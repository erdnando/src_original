package com.netro.garantias;

import com.netro.exception.NafinException;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "GarantiasEJB", mappedName = "GarantiasEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class GarantiasBean implements Garantias {

    //Variable para enviar mensajes al log.
    private final static Log log = ServiceLocator.getInstance().getLog(GarantiasBean.class);

    public static final int GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH = 2000;
    public static final int CLAVE_DEL_PROGRAMA_LENGTH = 5;
    public static final int GTI_CONTENIDO_ARCH_IC_LINEA_MAX = 99999;

    /**
     * Lee el archivo especificado, el cual debe cumplir con el layout de
     * carga de Alta de Garant�as.
     * @param strDirectorio Directorio donde se encuentra el archivo a procesar
     * @param esNombreArchivo Nombre del archivo a procesar, puede ser
     * 	un archivo de texto(.txt) o un archivo comprimido(.zip)
     * @param num_registros Numero de registros contenidos en el archivo a procesar
     * @param sum_montos Sumatoria del monto de los registros del archivo a procesar
     * @param ic_if Clave del intermediario financiero
     * @return Objeto ResultadosGar con el resultado del procesamiento.
     * @throws com.netro.exception.NafinException
     */
    public ResultadosGar procesarAltaGarantia(String strDirectorio, String esNombreArchivo, String num_registros,
                                              String sum_montos, String ic_if) throws NafinException {
        log.info("GarantiasBean::procesarAltaGarantia(E)");
        ResultadosGar retorno = new ResultadosGar(ResultadosGar.CARGA_MASIVA);
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        try {

            ReglasPrograma reglasValidacionParametros = getReglasValidacionParametrosPorPrograma(ic_if);
            retorno.setReglasValidacionParametros(reglasValidacionParametros);

            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());

            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);

            con.conexionDB();

            qrySentencia = "select ic_if_siag from comcat_if" + " where ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();

            retorno.setIcProceso(calculaIcProceso());
            qrySentencia =
                "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,IG_PROGRAMA,CG_CONTENIDO)" +
                "values(?,?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    BigDecimal tmpMonto = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    lsLinea = lsLinea.trim();
                    if (lsLinea.length() == 0) {
                        continue;
                    } else if (lsLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
                        lsLinea = lsLinea.substring(0, GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH);
                        ctrl_linea++;
                    } else {
                        ctrl_linea++;
                    }
                    vt = new VectorTokenizer(lsLinea, "@");
                    lovDatos = vt.getValuesVector();

                    this.validarAltaGarantias(ifSiag, numLinea, lsLinea, retorno);
                    try {
                        tmpMonto = new BigDecimal(((String) lovDatos.get(23)).trim());
                    } catch (Throwable e) {
                        tmpMonto = new BigDecimal(0);
                    }

                    if (!retorno.getErrorLinea()) {
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de financiamiento:\n");
                        }
                        //retorno.appendCorrectos("Linea:"+numLinea+" Monto del Financiamiento="+Comunes.formatoDecimal((String)lovDatos.get(23),2)+" Acreditado="+lovDatos.get(0)+".\n");
                        retorno.appendCorrectos("" + (String) lovDatos.get(9) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumRegOk(tmpMonto.doubleValue());
                        //insercion en tabla temporal
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, (String) lovDatos.get(33));
                            ps.setString(4, lsLinea);
                            ps.execute();
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }
                        }
                    } else {
                        retorno.incNumRegErr();
                        retorno.addSumRegErr(tmpMonto.doubleValue());
                    }
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                    iNumRegTrans++;
                } // for
            }
            ps.close();
            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            if (dbl_sum_montos.doubleValue() != ctrl_sum_montos.doubleValue()) {
                log.info("La sumatoria de los montos = " + dbl_sum_montos.doubleValue());
                log.info("La cifra de control capturada = " + ctrl_sum_montos.doubleValue());
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }
            //log.info("Numero de lineas = "+lineas);
        } catch (Exception e) {
            log.info("GarantiasBean::procesarAltaGarantia(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0)
                    con.terminaTransaccion(false);
                else
                    con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::procesarAltaGarantia(S)");
        }
        return retorno;
    }


    /**
     * Validaciones de alta de garantias.
     * PRECAUCION: Las validaciones son comunes tanto a la carga por archivo
     * como a la carga de garantias autom�ticas.
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * ResultadosGar, el cual llevara el registro de los errores de validaci�n.
     *
     * @param ifSiag clave SIAG del Intermediario Financiero
     * @param numLinea Numero de linea que se est� validando
     * @param strLinea linea de valores del layout de alta de garantias.
     * @param retorno Objeto de ResultadosGar, que contiene los detalles de la validaci�n
     *
     * @author Gilberto Aparicio
     * @author Salim Hernandez ( 25/01/2013 05:03:55 p.m. )
     * @author Salim Hernandez ( 07/02/2013 04:12:52 p.m. )
     * @author Salim Hernandez ( 16/07/2013 01:39:30 p.m. )
     */
    private void validarAltaGarantias(int ifSiag, int numLinea, String strLinea, ResultadosGar retorno) {

        if (strLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " El tama�o del registro es demasiado grande para ser procesado. \n");
            retorno.setErrorLinea(true);
            return;
        }

        VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
        List layout = vt.getValuesVector();

        String tipoPrograma = null;
        int reglaProgramaCreditoEducativo = ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO;
        int reglaProgramaSHFCreditoHipotecario = ResultadosGar.NO_OPERA_SHF_CRED_HIPOTECARIO;

        // Validar Numero de Campos
        if (retorno.getTipoCarga() == ResultadosGar.CARGA_INDIVIDUAL) {

            //if(layout.size()<42 + 1){		//+1 es por la arroba del correo
            if (layout.size() <
                43) { //+1 es por la arroba del correo y por el ultimo arroba que lleva al final de cada registro
                retorno.appendErrores("Error en la linea:" + numLinea +
                                      " El numero de campos es incorrecto. Esperados(42) Recibidos(" +
                                      (layout.size() - 1) + ") \n");
                retorno.setErrorLinea(true);
                return;
            }

        } else if (retorno.getTipoCarga() == ResultadosGar.CARGA_MASIVA) {

            tipoPrograma = layout.size() >= 34 ? (String) layout.get(33) : null;
            tipoPrograma = tipoPrograma == null ? "" : tipoPrograma.trim();

            if (tipoPrograma.length() > CLAVE_DEL_PROGRAMA_LENGTH) {
                tipoPrograma = ""; // tipoPrograma.substring(0,CLAVE_DEL_PROGRAMA_LENGTH);
            }

            reglaProgramaCreditoEducativo = retorno.getReglaValidacionParametroCreditoEducativo(tipoPrograma);
            reglaProgramaSHFCreditoHipotecario = retorno.getReglaValidacionSHFCreditoHipotecario();

            // Determinar posicion de los campos faltantes
            boolean existeCampoCorreo = layout.size() >= 41;
            boolean correoVacio = existeCampoCorreo ? esCampoVacioV2((String) layout.get(40)) : true;

            // Determinar el numero de elementos esperados
            int numElementosEsperados = 0;
            int recibidos = 0;

            /*

				Fodea 006 - 2012
				Nota: OPERA_SHF_CRED_HIPOTECARIO y  REQUERIDOS_IMSS son excluyentes,
				Actualmente OPERA_SHF_CRED_HIPOTECARIO tiene precedencia sobre REQUERIDOS_IMSS

				Fodea ### - 2013 (Credito Educativo 2da Fase)
				OPERA_CREDITO_EDUCATIVO puede trabajar en conjunto con uno de los dos
				programas anteriores.

				Fodea 024 - 2013
				Se suprime el uso del Programa Genera Empleo.

			*/
            if (reglaProgramaSHFCreditoHipotecario == ResultadosGar.OPERA_SHF_CRED_HIPOTECARIO) {

                numElementosEsperados =
                    45 + 1 +
                    (existeCampoCorreo && !correoVacio ? 1 :
                     0); //+1  y +1: es por la arroba del correo dominio del correo y por el ultimo arroba que lleva al final de cada registro
                recibidos = layout.size() - 1 - (existeCampoCorreo && !correoVacio ? 1 : 0);

                if (layout.size() != numElementosEsperados) {
                    retorno.appendErrores("Error en la linea:" + numLinea +
                                          " El numero de campos es incorrecto. Esperados(45) Recibidos(" + recibidos +
                                          ") \n");
                    retorno.setErrorLinea(true);
                    return;
                }

            } else { // => F024 - 2013 -- PROGRAMA GENERAL DE ALTA DE GARANTIAS

                numElementosEsperados =
                    44 + 1 +
                    (existeCampoCorreo && !correoVacio ? 1 :
                     0); //+1  y +1: es por la arroba del correo dominio del correo y por el ultimo arroba que lleva al final de cada registro
                recibidos = layout.size() - 1 - (existeCampoCorreo && !correoVacio ? 1 : 0);

                if (layout.size() != numElementosEsperados) {
                    retorno.appendErrores("Error en la linea:" + numLinea +
                                          " El numero de campos es incorrecto. Esperados(44) Recibidos(" + recibidos +
                                          ") \n");
                    retorno.setErrorLinea(true);
                    return;
                }

            }

        }

        if ((strLinea.charAt(strLinea.length() - 1)) != '@') {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " La cadena termina incorrectamente, debe llevar '@' al final \n");
            retorno.setErrorLinea(true);
            return;
        }

        int longitudClaveFinanciamiento =
            (reglaProgramaSHFCreditoHipotecario == ResultadosGar.OPERA_SHF_CRED_HIPOTECARIO) ? 30 : 20;

        // VALIDACIONES COMUNES
        //----------------------------------------------------------------------------------------------------------------
        boolean campoValido = true;

        // 1.  Nombre o Raz�n Social del acreditado										CHAR	   80						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(0), "Nombre o Raz�n Social del acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(0), "Nombre o Raz�n Social del acreditado", retorno, 80);

        // 2.  Registro Federal de Causante del acreditado								CHAR	   13						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(1), "Registro Federal de Causante del acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(1), "Registro Federal de Causante del acreditado",
                                retorno, 13);
        if (campoValido)
            validaRFC_v2(numLinea, (String) layout.get(1), "Registro Federal de Causante del acreditado", retorno);

        // 3.  Calle y N�mero del acreditado												CHAR	   80						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(2), "Calle y N�mero del acreditado", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(2), "Calle y N�mero del acreditado", retorno, 80);

        // 4.  Colonia del acreditado															CHAR	   80						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(3), "Colonia del acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(3), "Colonia del acreditado", retorno, 80);

        // 5.  C�digo Postal																		CHAR		 5						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(4), "C�digo Postal", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(4), "C�digo Postal", retorno, 5);

        // 6.  Ciudad																				CHAR		80						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(5), "Ciudad", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(5), "Ciudad", retorno, 80);

        // 7.  Clave del Municipio o Delegaci�n del acreditado						NUMBER	 4		Cat�logo A	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(6),
                                       "Clave del Municipio o Delegaci�n del acreditado", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(6), "Clave del Municipio o Delegaci�n del acreditado",
                                retorno, 4);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(6), "Clave del Municipio o Delegaci�n del acreditado", retorno);

        // 8.  Clave del Estado o Entidad Federativa del acreditado					NUMBER	 4		Cat�logo B	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(7),
                                       "Clave del Estado o Entidad Federativa del acreditado", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(7),
                                "Clave del Estado o Entidad Federativa del acreditado", retorno, 4);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(7), "Clave del Estado o Entidad Federativa del acreditado",
                         retorno);

        // 9.  Giro de la Actividad a la que se dedica el acreditado				CHAR	  300						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(8),
                                       "Giro de la Actividad a la que se dedica el acreditado", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(8),
                                "Giro de la Actividad a la que se dedica el acreditado", retorno, 300);

        //10.  Clave del financiamiento														CHAR	   20/30(para SHF)	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(9), "Clave del financiamiento", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(9), "Clave del financiamiento", retorno,
                                longitudClaveFinanciamiento);

        //11.  Sector de Actividad Econ�mica												CHAR		 8		Cat�logo C	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(10), "Sector de Actividad Econ�mica", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(10), "Sector de Actividad Econ�mica", retorno, 8);

        //12.  Solicita Fondeo de NAFIN														CHAR		 1		S o N	      Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(11), "Solicita Fondeo de NAFIN", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(11), "Solicita Fondeo de NAFIN", retorno, 1);
        if (campoValido)
            validaSN(numLinea, (String) layout.get(11), "Solicita Fondeo de NAFIN", retorno);

        //13.  Solicita la Participaci�n en el Riesgo de NAFIN						CHAR		 1		S o N	      Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(12),
                                       "Solicita la Participaci�n en el Riesgo de NAFIN", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(12), "Solicita la Participaci�n en el Riesgo de NAFIN",
                                retorno, 1);
        if (campoValido)
            validaSN(numLinea, (String) layout.get(12), "Solicita la Participaci�n en el Riesgo de NAFIN", retorno);

        //14.  Porcentaje de Participaci�n en el Riesgo Solicitado.					NUMBER	 5,2					Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(13),
                                       "Porcentaje de Participaci�n en el Riesgo Solicitado", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(13),
                                       "Porcentaje de Participaci�n en el Riesgo Solicitado", retorno, 5, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(13), "Porcentaje de Participaci�n en el Riesgo Solicitado",
                             retorno);

        //15.  Personal Ocupado (n�mero)														NUMBER	10						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(14), "Personal Ocupado (n�mero)", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(14), "Personal Ocupado (n�mero)", retorno, 10);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(14), "Personal Ocupado (n�mero)", retorno);

        //16.  Promedio de Ventas Anual (en pesos)										NUMBER 13,2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(15), "Promedio de Ventas Anual (en pesos)",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(15), "Promedio de Ventas Anual (en pesos)",
                                       retorno, 13, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(15), "Promedio de Ventas Anual (en pesos)", retorno);

        //17.  Prop�sito del Proyecto															CHAR	  2*N	   Cat�logo D	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(16), "Prop�sito del Proyecto", retorno);
        if (campoValido)
            campoValido =
                validaLongitudMultiplo(numLinea, (String) layout.get(16), "Prop�sito del Proyecto", retorno, 2);
        if (campoValido)
            validaProposito(numLinea, (String) layout.get(16), "Prop�sito del Proyecto", retorno);

        //18.  Clave del Tipo de financiamiento											NUMBER	 6		Cat�logo E	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(17), "Clave del Tipo de financiamiento", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(17), "Clave del Tipo de financiamiento", retorno, 6);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(17), "Clave del Tipo de financiamiento", retorno);

        //19. Garant�a Otorgada por Acreditado y Monto de la Garant�a Otorgada.
        //19a. Clave Garant�a Otorgada por el Acreditado								NUMBER	 2		Cat�logo F	Obligatorio
        //19b. Monto de la garant�a Otorgada												NUMBER 12,2
        // Nota: Como es un campo compuesto no se permiten espacios al inicio o al final.
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(18), "Clave y Monto de la garantia otorgada",
                                       retorno);
        if (campoValido)
            campoValido =
                validaLongitudMultiplo(numLinea, (String) layout.get(18), "Clave y Monto de la garantia otorgada",
                                       retorno, 14);
        if (campoValido)
            validaClaveMonto(numLinea, (String) layout.get(18), "Clave y Monto de la garantia otorgada", retorno);

        //20.  Destino de los Recursos.
        //20a. Clave del Tipo de Destino de los Recursos								NUMBER	 2		Cat�logo G	Obligatorio
        //20b. Porcentaje Parcial																NUMBER	 3
        //20c. Porcentaje de origen nacional												NUMBER	 3
        //20d. Porcentaje de importaci�n														NUMBER	 3
        // Nota: Como es un campo compuesto no se permiten espacios al inicio o al final
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(19), "Destino de los Recursos", retorno);
        if (campoValido)
            campoValido =
                validaLongitudMultiplo(numLinea, (String) layout.get(19), "Destino de los Recursos", retorno, 11);
        if (campoValido)
            validaDestino(numLinea, (String) layout.get(19), retorno);

        //21.  Porcentaje de la Producci�n destinada al Mercado Interno			NUMBER  5,2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(20),
                                       "Porcentaje de la Producci�n destinada al Mercado Interno", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(20),
                                       "Porcentaje de la Producci�n destinada al Mercado Interno", retorno, 5, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(20),
                             "Porcentaje de la Producci�n destinada al Mercado Interno", retorno);

        //22.  Porcentaje de la Producci�n destinada al Mercado Externo			NUMBER  5,2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(21),
                                       "Porcentaje de la Producci�n destinada al Mercado Externo", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(21),
                                       "Porcentaje de la Producci�n destinada al Mercado Externo", retorno, 5, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(21),
                             "Porcentaje de la Producci�n destinada al Mercado Externo", retorno);

        //23.  Forma de Amortizaci�n															NUMBER	 2		Cat�logo H	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(22), "Forma de Amortizaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(22), "Forma de Amortizaci�n", retorno, 2);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(22), "Forma de Amortizaci�n", retorno);

        //24.  Monto del Financiamiento														NUMBER 17,2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(23), "Monto del Financiamiento", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(23), "Monto del Financiamiento", retorno, 17, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(23), "Monto del Financiamiento", retorno);

        //25.  Fecha de Disposici�n y de Participaci�n en el Riesgo					DATE	    8						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(24),
                                       "Fecha de Disposici�n y de Participaci�n en el Riesgo", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(24),
                                "Fecha de Disposici�n y de Participaci�n en el Riesgo", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(24), "Fecha de Disposici�n y de Participaci�n en el Riesgo",
                        retorno);

        //26.  Importe de Disposici�n															NUMBER 17,2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(25), "Importe de Disposici�n", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(25), "Importe de Disposici�n", retorno, 17, 2);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(25), "Importe de Disposici�n", retorno);

        //27.  Moneda																				CHAR	    7		Cat�logo I	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(26), "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(26), "Moneda", retorno, 7);
        if (campoValido)
            validaMoneda(numLinea, (String) layout.get(26), retorno);

        //28.  N�mero de Meses de Plazo														NUMBER	 6						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(27), "N�mero de Meses de Plazo", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(27), "N�mero de Meses de Plazo", retorno, 6);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(27), "N�mero de Meses de Plazo", retorno);

        //29.  N�mero de Meses de la Gracia del Financiemiento						NUMBER	 6						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(28),
                                       "N�mero de Meses de la Gracia del Financiemiento", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(28), "N�mero de Meses de la Gracia del Financiemiento",
                                retorno, 6);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(28), "N�mero de Meses de la Gracia del Financiemiento", retorno);

        //30.  Sobretasa																			NUMBER  7,4						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(29), "Sobretasa", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, (String) layout.get(29), "Sobretasa", retorno, 7, 4);
        if (campoValido)
            validaFlotanteV2(numLinea, (String) layout.get(29), "Sobretasa", retorno);

        //31.  Clave del Funcionario Facultado del Intermediario Financiero 		NUMBER	 8		Cat�logo J	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(30),
                                       "Clave del Funcionario Facultado del Intermediario Financiero", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(30),
                                "Clave del Funcionario Facultado del Intermediario Financiero", retorno, 8);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(30),
                         "Clave del Funcionario Facultado del Intermediario Financiero", retorno);

        //32.  Clave del Intermediario Financiero.										NUMBER	 5		Cat�logo K	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(31), "Clave del Intermediario Financiero",
                                       retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(31), "Clave del Intermediario Financiero", retorno, 5);
        if (campoValido)
            validaIntermediario(numLinea, (String) layout.get(31), "Clave del Intermediario Financiero", ifSiag,
                                retorno);

        //33.  Tipo de Autorizaci�n															NUMBER	 6		Cat�logo L	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(32), "Tipo de Autorizaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(32), "Tipo de Autorizaci�n", retorno, 6);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(32), "Tipo de Autorizaci�n", retorno);

        //34.  Tipo Programa																		NUMBER	 5		Cat�logo M	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(33), "Tipo Programa", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(33), "Tipo Programa", retorno,
                                CLAVE_DEL_PROGRAMA_LENGTH); // CLAVE_DEL_PROGRAMA_LENGTH = 5
        if (campoValido)
            campoValido =
                validaEntero(numLinea, (String) layout.get(33), "Tipo Programa", retorno); //FODEA 017 - 2009 ACF
        if (campoValido)
            campoValido =
                validaTipoPrograma(numLinea, (String) layout.get(33), "Tipo Programa", reglaProgramaCreditoEducativo,
                                   retorno);

        //35.  Tipo Tasa																			CHAR	    6		Cat�logo N	Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(34), "Tipo Tasa", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(34), "Tipo Tasa", retorno, 6);

        //36.  Plazo en n�mero de d�as														NUMBER	 2						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(35), "Plazo en n�mero de d�as", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(35), "Plazo en n�mero de d�as", retorno, 2);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(35), "Plazo en n�mero de d�as", retorno);

        //37.  Calificaci�n Inicial															CHAR	    4						Obligatorio
        campoValido = true;
        if (reglaProgramaSHFCreditoHipotecario == ResultadosGar.NO_OPERA_SHF_CRED_HIPOTECARIO) {
            if (campoValido)
                campoValido =
                    validaCampoObligatorio(numLinea, (String) layout.get(36), "Calificaci�n Inicial", retorno);
            if (campoValido)
                campoValido = validarLongitud(numLinea, (String) layout.get(36), "Calificaci�n Inicial", retorno, 4);
            // Para el programa SHF se requiere que este campo venga vac�o.
        } else {
            //validarCampoRequeridoVacio(						numLinea, (String)layout.get(36),"Calificaci�n Inicial",											retorno		);
            if (campoValido)
                campoValido = validarLongitud(numLinea, (String) layout.get(36), "Calificaci�n Inicial", retorno, 4);
        }

        //38.  Antig�edad como cliente en meses											NUMBER	 4						Obligatorio
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(37), "Antig�edad como cliente en meses", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(37), "Antig�edad como cliente en meses", retorno, 4);
        if (campoValido)
            validaEntero(numLinea, (String) layout.get(37), "Antig�edad como cliente en meses", retorno);

        //39.  Nombre del Contacto y/o Representante										CHAR	   80
        if (!esVacio((String) layout.get(38))) {
            campoValido = true;
            if (campoValido)
                campoValido =
                    validarLongitud(numLinea, (String) layout.get(38), "Nombre del Contacto y/o Representante", retorno,
                                    80);
            // validarNombreContacto(     numLinea, (String)layout.get(38), "Nombre del Contacto y/o Representante",             retorno     );
        }

        //40. Tel�fonos del Contacto.
        //40a. Clave del Tipo de Tel�fono													NUMBER	 2	   Cat�logo O
        //40b. Tel�fono con clave lada														NUMBER	10
        //40c. Extensi�n																			NUMBER	 6
        //Nota: Se requiere al menos un telefono de oficina y un telefono celular.
        //Nota: Como es un campo compuesto no se permiten espacios al inicio o al final.
        if (!esVacio((String) layout.get(39))) {
            campoValido = true;
            if (campoValido)
                campoValido =
                    validaLongitudMultiplo(numLinea, (String) layout.get(39), "Tel�fonos del Contacto", retorno, 18);
            if (campoValido)
                validarTelefonoPyme(numLinea, (String) layout.get(39), "Tel�fonos del Contacto", retorno);
        }

        if (retorno.getTipoCarga() == ResultadosGar.CARGA_INDIVIDUAL) {

            if (layout.size() == 43) {

                //41.  Correo Electr�nico																CHAR	   40
                if (!esVacio((String) layout.get(40))) {
                    campoValido = true;
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(40), "Correo Electr�nico", retorno, 40);
                    if (campoValido)
                        validarCorreo(numLinea, (String) layout.get(40), "Correo Electr�nico", retorno);
                }

                //validaEntero(numLinea,(String)layout.get(41),"Entidad",retorno);

                //42.  Dependencia a la que vende													NUMBER	 4	   Cat�logo	P
                if (!esVacio((String) layout.get(41))) {
                    campoValido = true;
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(41), "Dependencia a la que vende", retorno,
                                            4);
                    if (campoValido)
                        validarDependencia(numLinea, (String) layout.get(41), "Dependencia a la que vende", retorno);
                }

            } else if (layout.size() == 44) {

                //41.  Correo Electr�nico																CHAR	   40
                if (!esVacio((String) layout.get(40) + "@" + (String) layout.get(41))) {
                    campoValido = true;
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(40) + "@" + (String) layout.get(41),
                                            "Correo Electr�nico", retorno, 40);
                    if (campoValido)
                        validarCorreo(numLinea, (String) layout.get(40) + "@" + (String) layout.get(41),
                                      "Correo Electr�nico", retorno);
                }
                //validaEntero(numLinea,(String)layout.get(42),"Entidad",retorno);

                //42.  Dependencia a la que vende													NUMBER	 4	   Cat�logo	P
                if (!esVacio((String) layout.get(42))) {
                    campoValido = true;
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(42), "Dependencia a la que vende", retorno,
                                            4);
                    if (campoValido)
                        validarDependencia(numLinea, (String) layout.get(42), "Dependencia a la que vende", retorno);
                }

            }
            //fin modificacion f059-2009

        } else if (retorno.getTipoCarga() == ResultadosGar.CARGA_MASIVA) {

            //41.  Correo Electr�nico																CHAR	   40
            int offset = 0;
            if (!esVacio((String) layout.get(40))) {
                // Si el campo 40 viene vac�o, se asume que no se especific� correo, en caso contrario,
                // Se asume que el campo 40 y 41 son corresponden al correo
                campoValido = true;
                if (campoValido)
                    campoValido =
                        validarLongitud(numLinea, (String) layout.get(40) + "@" + (String) layout.get(41),
                                        "Correo Electr�nico", retorno, 40);
                if (campoValido)
                    validarCorreo(numLinea, (String) layout.get(40) + "@" + (String) layout.get(41),
                                  "Correo Electr�nico", retorno);
                offset = 1;
            }

            //42.  Dependencia a la que vende													NUMBER	 4	   Cat�logo	P
            if (!esVacio((String) layout.get(41 + offset))) {
                campoValido = true;
                if (campoValido)
                    campoValido =
                        validarLongitud(numLinea, (String) layout.get(41 + offset), "Dependencia a la que vende",
                                        retorno, 4);
                if (campoValido)
                    validarDependencia(numLinea, (String) layout.get(41 + offset), "Dependencia a la que vende",
                                       retorno);
            }

            /*

				Fodea 006 - 2012
				Nota: OPERA_SHF_CRED_HIPOTECARIO y REQUERIDOS_IMSS son excluyentes,
				Actualmente OPERA_SHF_CRED_HIPOTECARIO tiene precedencia sobre REQUERIDOS_IMSS

				Fodea ### - 2013 (Credito Educativo 2da Fase)
				OPERA_CREDITO_EDUCATIVO puede trabajar en conjunto con uno de los dos
				programas anteriores.

				Fodea 024 - 2013
				Se suprime el uso del Programa Genera Empleo.

			*/
            if (reglaProgramaSHFCreditoHipotecario == ResultadosGar.OPERA_SHF_CRED_HIPOTECARIO) {
                boolean personaFisica = false;

                //43.  Tipo de Persona																	CHAR		 1		F o M			Obligatorio (Temporalmente NO Obligatorio)
                if (!esVacio((String) layout.get(42 +
                                                 offset))) { // Se inhabilita la obligatoriedad a peticion del usuario
                    campoValido = true;
                    //if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(42+offset),"Tipo de Persona", retorno    ); // Se inhabilita la obligatoriedad a peticion del usuario
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(42 + offset), "Tipo de Persona", retorno, 1);
                    if (campoValido)
                        validaTipoPersona(numLinea, (String) layout.get(42 + offset), "Tipo de Persona", retorno);
                    personaFisica = esPersonaFisica((String) layout.get(42 + offset)); // Se asume por default
                }

                //44.  Clave �nica de Registro de Poblaci�n										CHAR	   18						Obligatorio para personas fisicas
                String rfcAcreditado = (String) layout.get(1);
                campoValido = true;
                // Campo obligatorio solo para personas fisicas
                if (personaFisica) {
                    if (campoValido)
                        campoValido =
                            validaCampoObligatorio(numLinea, (String) layout.get(43 + offset),
                                                   "Clave �nica de Registro de Poblaci�n", retorno);
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(43 + offset),
                                            "Clave �nica de Registro de Poblaci�n", retorno, 18);
                    if (campoValido)
                        campoValido =
                            validaCURP(numLinea, (String) layout.get(43 + offset),
                                       "Clave �nica de Registro de Poblaci�n", retorno, personaFisica);
                    // if(campoValido) campoValido = validaCURPContraRFC(      		numLinea, (String)layout.get(43+offset),"Clave �nica de Registro de Poblaci�n", retorno, rfcAcreditado );
                } /*else {
					 if(campoValido)               validarCampoRequeridoVacio( 		numLinea, (String)layout.get(43+offset),"Clave �nica de Registro de Poblaci�n", retorno                );
				 }*/

                //45. Validar G�nero del Acreditado
                // Campo obligatorio s�lo para personas f�sicas (Temporalmente NO Obligatorio)
                if (!esVacio((String) layout.get(44 +offset))) { // Se inhabilita la obligatoriedad a peticion del usuario
                    if (personaFisica) {
                        /*if (campoValido)
                            campoValido =
                                validaCampoObligatorio(numLinea, (String) layout.get(44 + offset),
                                                       "G�nero del Acreditado",
                                                       retorno);Se inhabilita la obligatoriedad a peticion del usuario*/
                        if (campoValido)
                            campoValido =
                                validarLongitud(numLinea, (String) layout.get(44 + offset), "G�nero del Acreditado",
                                                retorno, 1);
                        if (campoValido)
                            campoValido =
                                validaGeneroAcreditado(numLinea, (String) layout.get(44 + offset),
                                                       "G�nero del Acreditado", retorno);
                    }else { //Personas morales tienen genero (Representante legal)
                        if (campoValido)
                            validarCampoRequeridoVacio(numLinea, (String) layout.get(44 + offset),
                                                       "G�nero del Acreditado", retorno);
                    }
                }

            } else { // => F024 - 2013 -- PROGRAMA GENERAL DE ALTA DE GARANTIAS

                //43.  Tipo de Persona																	CHAR		 1		F o M			Obligatorio (Temporalmente NO Obligatorio)
                if (!esVacio((String) layout.get(42 +
                                                 offset))) { // Se inhabilita la obligatoriedad a peticion del usuario
                    campoValido = true;
                    //if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(42+offset),"Tipo de Persona", retorno    );// Se inhabilita la obligatoriedad a peticion del usuario
                    if (campoValido)
                        campoValido =
                            validarLongitud(numLinea, (String) layout.get(42 + offset), "Tipo de Persona", retorno, 1);
                    if (campoValido)
                        validaTipoPersona(numLinea, (String) layout.get(42 + offset), "Tipo de Persona", retorno);
                }
                //44. Validar G�nero del Acreditado
                // Campo obligatorio s�lo para personas f�sicas (Temporalmente NO Obligatorio)
                

                //validaCampoObligatorio(numLinea, (String)layout.get(43+offset),"G�nero del Acreditado", retorno);

                if (!esVacio((String) layout.get(43 + offset))) { // Se inhabilita la obligatoriedad a peticion del usuario
                    boolean personaFisica = esPersonaFisica((String) layout.get(42 + offset)); // Se asume por default
                    if (personaFisica) {
                        //if(campoValido) campoValido = validaCampoObligatorio(numLinea, (String)layout.get(43+offset),"G�nero del Acreditado", retorno);// Se inhabilita la obligatoriedad a peticion del usuario
                        if (campoValido)
                            campoValido =
                                validarLongitud(numLinea, (String) layout.get(43 + offset), "G�nero del Acreditado", retorno, 1);
                        if (campoValido)
                            campoValido =
                                validaGeneroAcreditado(numLinea, (String) layout.get(43 + offset),
                                                       "G�nero del Acreditado", retorno);
                    }else { //Personas morales tienen genero (Representante legal)
                        if (campoValido)
                            validarCampoRequeridoVacio(numLinea, (String) layout.get(43 + offset),"G�nero del Acreditado", retorno);
                    }
                }

            }

        }


    }

    /**
     * Lee el archivo especificado, el cual debe cumplir con el layout de
     * carga de Alta de Recuperaciones.
     * @since F056-2009
     * @param strDirectorio Directorio donde se encuentra el archivo a procesar
     * @param esNombreArchivo Nombre del archivo a procesar, puede ser
     * 	un archivo de texto(.txt) o un archivo comprimido(.zip)
     * @param num_registros Numero de registros contenidos en el archivo a procesar
     * @param sum_montos Sumatoria del monto de los registros del archivo a procesar
     * @param ic_if Clave del intermediario financiero
     * @return Objeto ResultadosGar con el resultado del procesamiento.
     * @throws com.netro.exception.NafinException
     */
    public ResultadosGar procesarAltaRecuperaciones(String strDirectorio, String esNombreArchivo, String num_registros,
                                                    String sum_montos, String ic_if) {
        log.info("procesarAltaRecuperaciones(E)");
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        try {
            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());

            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);

            con.conexionDB();

            qrySentencia = " SELECT ic_if_siag " + " FROM comcat_if " + " WHERE ic_if = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();

            retorno.setIcProceso(this.calculaIcProceso());
            qrySentencia =
                " INSERT INTO gtitmp_contenido_arch " + " (IC_PROCESO,IC_LINEA,CG_CONTENIDO) " + " VALUES(?,?,?) ";
            ps = con.queryPrecompilado(qrySentencia);
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    BigDecimal tmpMonto = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    lsLinea = lsLinea.trim();
                    if (lsLinea.length() == 0)
                        continue;
                    else
                        ctrl_linea++;
                    vt = new VectorTokenizer(lsLinea, "@");
                    lovDatos = vt.getValuesVector();

                    this.validarAltaRecuperaciones(ifSiag, numLinea, lsLinea, retorno);

                    try {
                        tmpMonto = new BigDecimal(((String) lovDatos.get(5)).trim());
                    } catch (Throwable e) {
                        tmpMonto = new BigDecimal(0);
                    }

                    if (!retorno.getErrorLinea()) {
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de la garant�a:\n");
                        }
                        retorno.appendCorrectos("" + (String) lovDatos.get(1) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumRegOk(tmpMonto.doubleValue());
                        //insercion en tabla temporal
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }
                        }
                    } else {
                        retorno.incNumRegErr();
                        retorno.addSumRegErr(tmpMonto.doubleValue());
                    }
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                    iNumRegTrans++;
                } // for
            }
            ps.close();
            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            if (dbl_sum_montos.doubleValue() != ctrl_sum_montos.doubleValue()) {
                log.debug("procesarAltaRecuperaciones()::La sumatoria de los montos = " + dbl_sum_montos.doubleValue());
                log.debug("procesarAltaRecuperaciones()::La cifra de control capturada = " +
                          ctrl_sum_montos.doubleValue());
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }
            //log.debug("procesarAltaRecuperaciones()::Numero de lineas = "+lineas);
        } catch (Exception e) {
            throw new AppException("Error inesperado en el procesamiento del Carga de Recuperaciones", e);
        } finally {
            if (con.hayConexionAbierta()) {
                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0) {
                    con.terminaTransaccion(false);
                } else {
                    con.terminaTransaccion(true);
                }
                con.cierraConexionDB();
            }
            log.info("procesarAltaRecuperaciones(S)");
        }
        return retorno;
    }

    /**
     * Lee el archivo especificado, el cual debe cumplir con el layout de carga de
     * Desembolsos Masivos.
     *
     * @since F030-2010
     * @param strDirectorio   Directorio donde se encuentra el archivo a procesar
     * @param esNombreArchivo Nombre del archivo a procesar, puede ser
     * 	                    un archivo de texto(.txt) o un archivo comprimido(.zip)
     * @param num_registros   Numero de registros contenidos en el archivo a procesar
     * @param sum_montos      Sumatoria del monto de los registros del archivo a procesar
     * @param ic_if           Clave del intermediario financiero
     * @return                Objeto ResultadosGar con el resultado del procesamiento.
     * @throws com.netro.exception.NafinException
     *
     */
    public ResultadosGar procesarAltaDesembolsosMasivos(String strDirectorio, String esNombreArchivo,
                                                        String num_registros, String sum_montos,
                                                        String ic_if) throws AppException {

        log.info("procesarAltaDesembolsosMasivos(E)");

        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";

        String qrySentencia2 = "";
        PreparedStatement ps2 = null;

        try {
            // Copiar Numeros de Control
            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            // Validar el formato del archivo proporcionado
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            // Obtener el conteo de garantias repetidas
            HashMap listaGarantias = cuentaRepeticionesClavesGarantias(esNombreArchivo);

            // Abrir archivo
            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);

            con.conexionDB();
            // Obtener Clave SIAG del IF cuyo ID es ic_if
            qrySentencia = " SELECT ic_if_siag " + " FROM   comcat_if  " + " WHERE  ic_if = ?  ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();
            // Insertar lineas del archivo proporcionado en la tabla: GTITMP_CONTENIDO_ARCH
            retorno.setIcProceso(this.calculaIcProceso());
            qrySentencia =
                " INSERT INTO gtitmp_contenido_arch  " + " (IC_PROCESO,IC_LINEA,CG_CONTENIDO) " + " VALUES(?,?,?) ";
            // Insertar lineas del archivo proporcionado en la tabla: GTITMP_SOLICITUD_PAGO
            ps = con.queryPrecompilado(qrySentencia);
            qrySentencia2 =
                "INSERT INTO 											" + "	GTITMP_SOLICITUD_PAGO 							" + "		( 													" +
                "			ic_proceso,									" + "			cc_garantia,								" + "			df_incumplimiento,						" +
                "			cg_causas_incumplimiento,				" + "			cg_funcionario_if,						" + "			cg_tel_funcionario,						" +
                "			cg_ext_funcionario,						" + "			cg_mail_funcionario,						" +
                "			cg_localizacion_supervision,			" + "			cg_ciudad_supervision,					" +
                "			cg_estado_supervision,					" + "			ic_periodicidad_pago,					" + "			df_fecha_segundo_incum,					" +
                "			fn_saldo_insoluto_estado_cuent,		" + "			fn_capital_vigente,						" +
                "			fn_capital_vencido,						" + "			fn_interes_ordinario,					" + "			fn_interes_moratorio,					" +
                "			df_fecha_ultimo_pago_capital,			" + "			df_fecha_ultimo_pago_interes,			" +
                "			fn_interes_recup_post_prim_inc,		" + "			ig_tasa_fondeo_nafin,					" +
                "			df_fecha_liquidacion_fondeo,			" + "			ig_num_prestamo_sirac,					" +
                "			cg_colonia_supervision,					" + "			ic_estado,									" + "			ic_municipio,								" +
                "			fn_monto_original_credito,				" + "			df_fecha_disposicion,					" + "			cs_fondeo_nafin,							" +
                "			cg_rfc_acreditado,						" + "			cg_nombre_acreditado,               " +
                "			ig_num_solicitud               		" + "		) 													" + "	VALUES 												" +
                "		( 													" + "			?, 											" + "			?, 											" + "			to_date(?,'yyyymmdd'), 					" +
                "			?, 											" + "			?, 											" + "			?, 											" + "			?, 											" +
                "			?, 											" + "			?, 											" + "			?, 											" + "			?, 											" +
                "			?, 											" + "			to_date(?,'yyyymmdd'), 					" + "			?, 											" + "			?, 											" +
                "			?, 											" + "			?, 											" + "			?, 											" + "			to_date(?,'yyyymmdd'), 					" +
                "			to_date(?,'yyyymmdd'), 					" + "			?, 											" + "			?, 											" +
                "			to_date(?,'yyyymmdd'), 					" + "			?, 											" + "			?, 											" + "			?, 											" +
                "			?, 											" + "			?, 											" + "			to_date(?,'yyyymmdd'), 					" + "			?,  											" +
                "			?,  											" + "			?,  											" + "			?  											" +
                "		) 													";
            ps2 = con.queryPrecompilado(qrySentencia2);
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    // Inicializar variables
                    BigDecimal tmpMonto = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    // Copiar contenido de la linea
                    lsLinea = lsLinea.trim();
                    // Validar que la linea no venga vacia
                    if (lsLinea.length() == 0)
                        continue;
                    else
                        ctrl_linea++;
                    // Extraer campos de la linea leida
                    vt = new VectorTokenizer(lsLinea, "@");
                    lovDatos = vt.getValuesVector();
                    // Validar cada uno de los campos leidos
                    this.validarAltaDesembolsosMasivos(ifSiag, numLinea, lsLinea, retorno, listaGarantias, ic_if);
                    // Leer monto especificado en la linea....
                    try {
                        // Monto de Control: Saldo Insoluto del Estado de Cuenta
                        tmpMonto = new BigDecimal(((String) lovDatos.get(6)).trim());
                    } catch (Throwable e) {
                        tmpMonto = new BigDecimal(0);
                    }
                    // Si no hubo ningun error en el registro leido
                    if (!retorno.getErrorLinea()) {
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de Financiamiento:\n");
                        }
                        retorno.appendCorrectos("" + (String) lovDatos.get(1) + "\n"); // Clave del Intermediario
                        retorno.incNumRegOk(); // Linea Exitosa
                        retorno.addSumRegOk(tmpMonto.doubleValue()); //Sumar Monto
                        //Si no hay errores insertar en tabla temporal: GTITMP_CONTENIDO_ARCH
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();
                        }
                        //Si no hay errores insertar en tabla temporal: GTITMP_SOLICITUD_PAGO
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            // Leer parametros
                            VectorTokenizer vt2 = new VectorTokenizer(lsLinea, "@");
                            List layout = vt2.getValuesVector();
                            // Insertar parametros leidos
                            ps2.setInt(1, retorno.getIcProceso());
                            ps2.setString(2,
                                          getCadena((String) layout.get(1)).trim()); // CC_GARANTIA								,2
                            if (layout.get(15) == null || "".equals((String) layout.get(15))) {
                                ps2.setNull(3, Types.DATE);
                            } else {
                                ps2.setString(3,
                                              getCadena((String) layout.get(15)).trim()); // DF_INCUMPLIMIENTO						,16*
                            }
                            ps2.setString(4, ""); // CG_CAUSAS_INCUMPLIMIENTO			,""
                            ps2.setString(5,
                                          getCadena((String) layout.get(21)).trim()); // CG_FUNCIONARIO_IF						,22
                            ps2.setString(6,
                                          getCadena((String) layout.get(28)).trim()); // CG_TEL_FUNCIONARIO					,29
                            ps2.setInt(7,
                                       getInt((String) layout.get(29))); // CG_EXT_FUNCIONARIO					,30
                            ps2.setString(8,
                                          getCadena((String) layout.get(22)).trim() + "@" +
                                          getCadena((String) layout.get(23)).trim()); // CG_MAIL_FUNCIONARIO		,23y24
                            ps2.setString(9,
                                          getCadena((String) layout.get(24)).trim()); // CG_LOCALIZACION_SUPERVISION		,25
                            ps2.setString(10, ""); // CG_CIUDAD_SUPERVISION				,""
                            ps2.setString(11, ""); // CG_ESTADO_SUPERVISION				,""
                            ps2.setInt(12,
                                       getInt((String) layout.get(14))); // IC_PERIODICIDAD_PAGO					,15
                            if (layout.get(16) == null || "".equals((String) layout.get(16))) {
                                ps2.setNull(13, Types.DATE);
                            } else {
                                ps2.setString(13,
                                              getCadena((String) layout.get(16)).trim()); // DF_FECHA_SEGUNDO_INCUM				,17*
                            }
                            ps2.setBigDecimal(14,
                                              getBigDecimal((String) layout.get(6))); // FN_SALDO_INSOLUTO_ESTADO_CUENT	,7
                            ps2.setBigDecimal(15,
                                              getBigDecimal((String) layout.get(7))); // FN_CAPITAL_VIGENTE					,8
                            ps2.setBigDecimal(16,
                                              getBigDecimal((String) layout.get(8))); // FN_CAPITAL_VENCIDO					,9
                            ps2.setBigDecimal(17,
                                              getBigDecimal((String) layout.get(9))); // FN_INTERES_ORDINARIO					,10
                            ps2.setBigDecimal(18,
                                              getBigDecimal((String) layout.get(10))); // FN_INTERES_MORATORIO					,11
                            ps2.setString(19,
                                          getCadena((String) layout.get(11)).trim()); // DF_FECHA_ULTIMO_PAGO_CAPITAL		,12
                            ps2.setString(20,
                                          getCadena((String) layout.get(12)).trim()); // DF_FECHA_ULTIMO_PAGO_INTERES		,13
                            ps2.setBigDecimal(21,
                                              getBigDecimal((String) layout.get(13))); // FN_INTERES_RECUP_POST_PRIM_INC	,14
                            if (layout.get(18) == null || "".equals((String) layout.get(18))) {
                                ps2.setNull(22, Types.NUMERIC);
                            } else {
                                ps2.setBigDecimal(22,
                                                  getBigDecimal((String) layout.get(18))); // IG_TASA_FONDEO_NAFIN					,19*
                            }
                            if (layout.get(19) == null || "".equals((String) layout.get(19))) {
                                ps2.setNull(23, Types.DATE);
                            } else {
                                ps2.setString(23,
                                              getCadena((String) layout.get(19)).trim()); // DF_FECHA_LIQUIDACION_FONDEO		,20*
                            }
                            if (layout.get(20) == null || "".equals((String) layout.get(20))) {
                                ps2.setNull(24, Types.NUMERIC);
                            } else {
                                ps2.setBigDecimal(24,
                                                  getBigDecimal((String) layout.get(20))); // IG_NUM_PRESTAMO_SIRAC				,21*
                            }
                            ps2.setString(25,
                                          getCadena((String) layout.get(25)).trim()); // CG_COLONIA_SUPERVISION				,26
                            ps2.setInt(26,
                                       getInt((String) layout.get(26))); // IC_ESTADO								,27
                            ps2.setInt(27,
                                       getInt((String) layout.get(27))); // IC_MUNICIPIO							,28
                            ps2.setBigDecimal(28,
                                              getBigDecimal((String) layout.get(4))); // FN_MONTO_ORIGINAL_CREDITO			,5
                            ps2.setString(29,
                                          getCadena((String) layout.get(5)).trim()); // DF_FECHA_DISPOSICION					,6
                            ps2.setString(30,
                                          getCadena((String) layout.get(17)).trim()); // CS_FONDEO_NAFIN						,18

                            ps2.setString(31,
                                          getCadena((String) layout.get(2)).trim()); // CG_RFC_ACREDITADO						,3
                            ps2.setString(32,
                                          getCadena((String) layout.get(3)).trim()); // CG_NOMBRE_ACREDITADO					,4
                            ps2.setInt(33, numLinea); // Numero de Linea

                            ps2.execute();

                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }
                        }
                    } else {
                        retorno.incNumRegErr(); // Linea con error
                        retorno.addSumRegErr(tmpMonto.doubleValue()); // Agregar a la suma de montos con error
                    }
                    // Agregar a la Sumatoria de Montos
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                    iNumRegTrans++;
                } // for
            }
            ps.close();
            ps2.close();
            // Si el numero de registros procesados no corresponde con el numero de registros especificados por el
            // usuario
            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            // Si la Sumatoria de Montos de todas las lineas procesadas no corresponde con el especificado por el
            // usuario
            if (dbl_sum_montos.doubleValue() != ctrl_sum_montos.doubleValue()) {
                log.debug("procesarAltaDesembolsosMasivos()::La sumatoria de los montos    = " +
                          dbl_sum_montos.doubleValue());
                log.debug("procesarAltaDesembolsosMasivos()::La cifra de control capturada = " +
                          ctrl_sum_montos.doubleValue());
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }
            //log.debug("procesarDesembolsosMasivos()::Numero de lineas = "+lineas);
        } catch (Exception e) {
            log.error("procesarAltaDesembolsosMasivos(Exception)");
            log.debug("procesarAltaDesembolsosMasivos.strDirectorio   = <" + strDirectorio + ">");
            log.debug("procesarAltaDesembolsosMasivos.esNombreArchivo = <" + esNombreArchivo + ">");
            log.debug("procesarAltaDesembolsosMasivos.num_registros   = <" + num_registros + ">");
            log.debug("procesarAltaDesembolsosMasivos.sum_montos      = <" + sum_montos + ">");
            log.debug("procesarAltaDesembolsosMasivos.ic_if           = <" + ic_if + ">");
            e.printStackTrace();
            throw new AppException("Error inesperado en la Carga de Archivo de Desembolsos Masivos", e);
        } finally {

            if (con.hayConexionAbierta()) {
                // Si se detectaron registros con errores cancelar insercion en tabla: GTITMP_CONTENIDO_ARCH
                if ((retorno.getErrores().length() > 0) || (retorno.getCifras().length() > 0)) {
                    con.terminaTransaccion(false);
                } else {
                    con.terminaTransaccion(true);
                }
                con.cierraConexionDB();
            }

            log.info("procesarAltaDesembolsosMasivos(S)");
        }

        return retorno;
    }

    public boolean obtenerCondonacionParcial(String claveIF, String tipo) throws AppException {

        log.info("obtenerCondonacionParcial(E)");

        if (claveIF == null || claveIF.trim().equals("")) {
            log.info("obtenerCondonacionParcial(S)");
            throw new AppException("Ocurrio un Error al consultar la parametrizacion ");
        }

        AccesoDB con = new AccesoDB();
        StringBuffer qrySentencia = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean condonacionParcial = false;

        try {

            con.conexionDB();

            // Revisar si el programa de la SHF "Credito Hipotecario" esta parametrizado

            qrySentencia = new StringBuffer();
            if (tipo.equals("P")) {
                qrySentencia.append("	SELECT 																	" +
                                    "	IF_SIAG.CS_CONDONACION_PARCIAL AS CS_CONDONACION_PARCIAL	" +
                                    "	FROM 																	" + "	COMCAT_IF INTERMEDIARIO, 										" +
                                    "	GTI_IF_SIAG IF_SIAG 												" + "	WHERE 																	" +
                                    "	INTERMEDIARIO.IC_IF       		= ? 							" +
                                    "	AND INTERMEDIARIO.IC_IF_SIAG 	= IF_SIAG.IC_IF_SIAG 	");
            } else if (tipo.equals("V")) {
                qrySentencia.append("	SELECT 																	" +
                                    "	CS_CONDONACION_PARCIAL  AS CS_CONDONACION_PARCIAL	" + "	FROM 																	" +
                                    "	gti_if_siag											" + "	WHERE 																	" +
                                    "	IC_IF_SIAG = ?	");

            }
            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            if (rs.next()) {
                condonacionParcial = "S".equals(rs.getString("CS_CONDONACION_PARCIAL")) ? true : false;
            }

        } catch (Exception e) {

            log.error("obtenerCondonacionParcial(Exception)");
            log.error("obtenerCondonacionParcial.claveIF = <" + claveIF + ">");
            e.printStackTrace();
            throw new AppException("Ocurrio un Error al consultar la parametrizacion del programa \"Credito Hipotecario\" de la SHF.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("obtenerCondonacionParcial(S)");
        }

        return condonacionParcial;

    }


    /**
     * Validaciones de alta de recuperaciones.
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * ResultadosGar, el cual llevara el registro de los errores de validaci�n.
     *
     * @param ifSiag clave SIAG del Intermediario Financiero
     * @param numLinea Numero de linea que se est� validando
     * @param strLinea linea de valores del layout de alta de garantias.
     * @param retorno Objeto de ResultadosGar, que contiene los detalles de la validaci�n
     *
     * @author Gilberto Aparicio
     * @author Salim Hernandez ( 24/01/2013 07:31:02 p.m., 11/03/2013 07:23:35 p.m. )
     *
     */
    private void validarAltaRecuperaciones(int ifSiag, int numLinea, String strLinea, ResultadosGar retorno) {
        VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
        List layout = vt.getValuesVector();

        if (strLinea.charAt(strLinea.length() - 1) != '@') {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " La linea debe finalizar con un caracter de arroba\n");
            retorno.setErrorLinea(true);
            return;
        }
        int numCamposRequeridos = 0;
        boolean getCondonacion = this.obtenerCondonacionParcial(String.valueOf(ifSiag), "V");
        if (getCondonacion == true) {
            numCamposRequeridos = 20;
        } else {
            numCamposRequeridos = 19;
        }

        //Dado que en el layout SIEMPRE ponen una arroba al final de la linea,
        //el cual es detectado como un campo mas por la clase VectorTokenizer,
        //se le suma 1
        if (layout.size() != numCamposRequeridos + 1) {
            retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto. Esperados(" +
                                  numCamposRequeridos + ") Recibidos(" + (layout.size() - 1) + ") \n");
            //layout.size()-1 se les resta uno, debido a que como en el layout agregan una @ al final, VectorTokenizer cuenta un campo de m�s.
            retorno.setErrorLinea(true);
            return;
        }

        boolean campoValido = false;
        boolean campo16Valido = false;
        boolean campo17Valido = false;
        final String RECUPERACION_NORMAL = "\\s*1\\s*";
        // No. CAMPO                                       	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................
        //  1  Clave del intermediario  								NUMBER 		    5 	OBLIGATORIO
        //     OBSERVACIONES: N�mero de identificaci�n del intermediario
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(0), "Clave del intermediario", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(0), "Clave del intermediario", retorno, 5);
        if (campoValido)
            validaIntermediario(numLinea, (String) layout.get(0), "Clave del intermediario", ifSiag, retorno);
        //  2  Clave de la garant�a  									CHAR 			   20 	OBLIGATORIO
        //     OBSERVACIONES: No. de pr�stamo o garant�a
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(1), "Clave de la garant�a", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(1), "Clave de la garant�a", retorno, 20);
        //  3  Nombre del acreditado  								CHAR 			   80 	OBLIGATORIO
        //     OBSERVACIONES: Nombre completo del cliente
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(2), "Nombre del acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(2), "Nombre del acreditado", retorno, 80);
        //  4  Fecha honrada  											DATE 			    8 	OBLIGATORIO
        //     OBSERVACIONES: Formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(3), "Fecha honrada", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(3), "Fecha honrada", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(3), "Fecha honrada", retorno);
        //  5  Monto honrado  											NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Total pagado
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(4), "Monto honrado", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(4), "Monto honrado", retorno, 20,
                                       2); // Antes: 12,2
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(4), "Monto honrado", retorno);
        //  6  Monto total de la recuperaci�n del acreditado  NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Monto pagado por el cliente
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(5),
                                       "Monto total de la recuperaci�n del acreditado", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(5),
                                       "Monto total de la recuperaci�n del acreditado", retorno, 20, 2); // Antes: 12,2
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(5),
                              "Monto total de la recuperaci�n del acreditado", retorno);
        //  7  Moneda  													CHAR 				 7 	OBLIGATORIO
        //     OBSERVACIONES: Tipo de Moneda
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(6), "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(6), "Moneda", retorno, 7);
        if (campoValido)
            validaMoneda(numLinea, (String) layout.get(6), retorno);
        //  8  Tipo de recuperaci�n  									NUMBER 		    1 	OBLIGATORIO
        //     OBSERVACIONES: 1) Recuperaci�n normal y 2) Recuperaci�n por reestructura
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno, 1);
        if (campoValido)
            campoValido = validaEntero(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno);
        if (campoValido)
            validaRangoNumero(numLinea, ((String) layout.get(7)).trim(), "Tipo de recuperaci�n", retorno, "1",
                              "2"); //De estar entre 1 y 2
        //  9  Estatus de recuperaci�n  								NUMBER 		    1 	OBLIGATORIO
        //     OBSERVACIONES: 1) Recuperaci�n parcial y 2) Recuperaci�n total
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(8), "Estatus de recuperaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(8), "Estatus de recuperaci�n", retorno, 1);
        if (campoValido)
            campoValido = validaEntero(numLinea, (String) layout.get(8), "Estatus de recuperaci�n", retorno);
        if (campoValido)
            validaRangoNumero(numLinea, ((String) layout.get(8)).trim(), "Estatus de recuperaci�n", retorno, "1",
                              "2"); //De estar entre 1 y 2
        // 10  Gastos de juicio  										NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Monto de los gastos de juicio generados. Este campo ser� obligatorio cuando se trate de una recuperaci�n normal, en caso contrario ser� requerido vac�o.
        String campoTipoRecuperacion = (String) layout.get(7);
        campoTipoRecuperacion = campoTipoRecuperacion == null ? "" : campoTipoRecuperacion;
        campoValido = true;
        if (campoTipoRecuperacion.matches(RECUPERACION_NORMAL)) {
            if (campoValido)
                campoValido = validaCampoObligatorio(numLinea, (String) layout.get(9), "Gastos de juicio", retorno);
            if (campoValido)
                campoValido =
                    validaPrecisionDecimal(numLinea, (String) layout.get(9), "Gastos de juicio", retorno, 20,
                                           2); // Antes: 12,2: 10 enteros y 2 decimales
            if (campoValido)
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(9), "Gastos de juicio",
                                  retorno);
        } else {
            if (campoValido)
                validarCampoRequeridoVacio(numLinea, (String) layout.get(9), "Gastos de juicio", retorno);
        }
        // 11  Capital reembolsado por el acreditado  			NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Recuperaci�n que corresponde a capital
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(10), "Capital reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(10), "Capital reembolsado por el acreditado",
                                       retorno, 20, 2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(10),
                              "Capital reembolsado por el acreditado", retorno);
        // 12  Inter�s reembolsado por el acreditado  			NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Recuperaci�n que corresponde a inter�s ordinario
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(11), "Inter�s reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(11), "Inter�s reembolsado por el acreditado",
                                       retorno, 20, 2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(11),
                              "Inter�s reembolsado por el acreditado", retorno);
        // 13  Moratorios reembolsado por el acreditado  		NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Recuperaci�n que corresponde a moratorios
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(12), "Moratorios reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(12), "Moratorios reembolsado por el acreditado",
                                       retorno, 20, 2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(12),
                              "Moratorios reembolsado por el acreditado", retorno);
        // 14  Fecha reembolso por el acreditado  				DATE 			    8 	OBLIGATORIO
        //     OBSERVACIONES: Fecha en que el Intermediario recibi� recursos por parte del acreditado,
        //     formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(13), "Fecha reembolso por el acreditado", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, (String) layout.get(13), "Fecha reembolso por el acreditado", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(13), "Fecha reembolso por el acreditado", retorno);
        // 15  Importe del Reembolso de Recuperaci�n a NAFIN  NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Importe de la recuperaci�n a NAFIN
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(14),
                                       "Importe del Reembolso de Recuperaci�n a NAFIN", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(14),
                                       "Importe del Reembolso de Recuperaci�n a NAFIN", retorno, 20,
                                       2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(14),
                              "Importe del Reembolso de Recuperaci�n a NAFIN", retorno);
        // 16  Importe de intereses generados  					NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Importe de los intereses generados para pago a NAFIN; si el valor de este
        //     campo es mayor a cero, se requiere que el valor del campo 17 sea cero.
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(15), "Importe de intereses generados", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(15), "Importe de intereses generados", retorno, 20,
                                       2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            campoValido =
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(15),
                                  "Importe de intereses generados", retorno);
        campo16Valido = campoValido;
        // 17  Penalizaci�n  											NUMBER 		 20,2 	OBLIGATORIO
        //     OBSERVACIONES: Importe de la penalizaci�n para pago a NAFIN; si el valor de este campo es
        //     mayor a cero, se requiere que el valor del campo 16 sea cero.
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(16), "Penalizaci�n", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(16), "Penalizaci�n", retorno, 20,
                                       2); // Antes: 12,2: 10 enteros y 2 decimales
        if (campoValido)
            campoValido =
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(16), "Penalizaci�n", retorno);
        campo17Valido = campoValido;
        // 18  IVA  														NUMBER 		 20,2 	NO OBLIGATORIO
        //     OBSERVACIONES: Impuesto al Valor Agregado
        if (!esVacio((String) layout.get(17))) {
            campoValido = true;
            if (campoValido)
                campoValido =
                    validaPrecisionDecimal(numLinea, (String) layout.get(17), "IVA", retorno, 20,
                                           2); // Antes: 12,2: 10 enteros y 2 decimales
            if (campoValido)
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(17), "IVA", retorno);
        }
        // 19  Fecha de reembolso a NAFIN  							DATE 			    8 	OBLIGATORIO
        //     OBSERVACIONES: Formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(18), "Fecha del Reembolso", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(18), "Fecha del Reembolso", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(18), "Fecha del Reembolso", retorno);
        if (getCondonacion == true) {
            // 20  Importe de Condonaci�n Parcial							NUMBER 			    20,2 	OBLIGATORIO
            //     OBSERVACIONES: Importe de Condonaci�n Parcial del IF
            campoValido = true;
            if (campoValido)
                campoValido =
                    validaCampoObligatorio(numLinea, (String) layout.get(19), "Importe de Condonaci�n Parcial",
                                           retorno);
            if (campoValido)
                campoValido =
                    validaSerNumero(numLinea, (String) layout.get(19), "Importe de Condonaci�n Parcial", retorno);
            if (campoValido)
                campoValido =
                    validaSerPositivo(numLinea, (String) layout.get(19), "Importe de Condonaci�n Parcial", retorno);
            if (campoValido)
                campoValido =
                    validaPrecisionDecimalDifMe(numLinea, (String) layout.get(19), "Importe de Condonaci�n Parcial",
                                                retorno, 20, 2);


        }
        // VALIDACIONES ESPECIALES
        //.......................................................................................................
        if (campo16Valido && campo17Valido) {
            validaCamposExcluyentes(numLinea, (String) layout.get(15), (String) layout.get(16),
                                    "Los campos 16 y 17 son excluyentes, por lo que se requiere que el valor de uno de ellos sea cero.",
                                    retorno);
        }

    }

    /**
     *  Este metodo cuenta cuantas veces aparece la misma Clave de Garantia en un archivo
     *  especificado. Fodea 030 - 2010
     *
     *  @param filePath Ruta del Archivo
     *
     *  @return HashMap con el conteo de las garantias.
     *
     */
    private HashMap cuentaRepeticionesClavesGarantias(String filePath) {

        HashMap listaGarantias = new HashMap();
        BigInteger uno = new BigInteger("1");
        BigInteger numeroGarantias = null;
        BufferedReader bufferedReader = null;
        String strLinea = null;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "ISO-8859-1"));

            while ((strLinea = bufferedReader.readLine()) != null) {
                VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
                List layout = vt.getValuesVector();

                if (strLinea.charAt(strLinea.length() - 1) != '@') {
                    continue;
                }
                //Dado que en el layout SIEMPRE ponen una arroba al final de la linea,
                //el cual es detectado como un campo mas por la clase VectorTokenizer,
                //se le suma 1
                //if(layout.size() != numCamposRequeridos + 1){
                if (layout.size() < 3) {
                    continue;
                }
                String claveGarantia = (String) layout.get(1);
                if (claveGarantia == null) {
                    continue;
                }
                if (claveGarantia.trim().equals("")) {
                    continue;
                }

                numeroGarantias = (BigInteger) listaGarantias.get(claveGarantia.trim());
                if (numeroGarantias == null) {
                    listaGarantias.put(claveGarantia.trim(), new BigInteger("1"));
                } else {
                    listaGarantias.put(claveGarantia.trim(), numeroGarantias.add(uno));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return listaGarantias;
    }

    /**
     * Validaciones de alta de Desembolsos Masivos.
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * ResultadosGar, el cual llevara el registro de los errores de validaci�n.
     *
     * @param ifSiag    clave SIAG del Intermediario Financiero
     * @param numLinea  Numero de linea que se est� validando
     * @param strLinea  linea de valores del layout de alta de garantias.
     * @param retorno   Objeto de ResultadosGar, que contiene los detalles de la validaci�n
     *
     * @author Salim Hernandez
     *
     */
    private void validarAltaDesembolsosMasivos(int ifSiag, int numLinea, String strLinea, ResultadosGar retorno,
                                               HashMap listaGarantias, String claveIF) {
        VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
        List layout = vt.getValuesVector();

        if (strLinea.charAt(strLinea.length() - 1) != '@') {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " La linea debe finalizar con un caracter de arroba\n");
            retorno.setErrorLinea(true);
            return;
        }

        // Verificar si el campo de correo viene vacio
        /*
		boolean 	correoVacio 	= false;
		boolean 	faltaDominio 	= false;
		int 		ajustaIndice	= 0;
		if( layout.size() ==  30 ){


			String correo 	= (String) layout.get(22);
			if(correo == null || correo.trim().equals("")){
				correoVacio 	= true;
				ajustaIndice 	= -1;
				faltaDominio 	= true;
			}
			if(!correoVacio){
				String usuario = (String) layout.get(22);
				String dominio = (String) layout.get(23);
				usuario = (usuario == null?"":usuario);
				dominio = (dominio == null?"":dominio);
				if(!ValidadorDesembolsosMasivos.validarCorreo("Correo electr�nico", usuario+"@"+dominio, numLinea, null)){
					ajustaIndice = -1;
					faltaDominio = true;
				}
			}

		}
		*/

        int numCamposRequeridos = 29 + 1; // Se le suma 1 debido a que el campo del correo cuenta por dos
        //Dado que en el layout SIEMPRE ponen una arroba al final de la linea,
        //el cual es detectado como un campo mas por la clase VectorTokenizer,
        //se le suma 1: numCamposRequeridos + 1
        if (layout.size() !=
            (numCamposRequeridos +
             1)) { //&& !(correoVacio && layout.size() == 30 ) &&  !(!correoVacio && layout.size() == 30 )){
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " El n�mero de separadores de campo es incorrecto. \n");
            //layout.size()-1 se les resta uno, debido a que como en el layout agregan una @ al final, VectorTokenizer cuenta un campo de m�s. se le resta uno


            // mas debido a que los campos 23 y 24 (nombre usuario y dominio) son tratados como un solo campo.
            retorno.setErrorLinea(true);
            return;
        }

        //1.  Validar Clave del Intermediario
        ValidadorDesembolsosMasivos.existeCampo("Clave del Intermediario", (String) layout.get(0), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Clave del Intermediario", (String) layout.get(0), numLinea,
                                                            retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Clave del Intermediario", (String) layout.get(0), 5, 0,
                                                             numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaIntermediario((String) layout.get(0), String.valueOf(ifSiag),
                                                            "Clave del Intermediario", numLinea, retorno);
        }
        //2.  Validar Clave de la garantia
        ValidadorDesembolsosMasivos.existeCampo("Clave de la garantia", (String) layout.get(1), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Clave de la garantia", (String) layout.get(1), 20,
                                                              numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaGarantiaRepetida("Clave de la garantia", (String) layout.get(1), numLinea,
                                                               retorno, listaGarantias);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaGarantiaRegistrada("Clave de la garantia", (String) layout.get(1),
                                                                 numLinea, retorno, claveIF);
        }
        //3.  Validar RFC del Acreditado
        ValidadorDesembolsosMasivos.existeCampo("RFC del Acreditado", (String) layout.get(2), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaRFC("RFC del Acreditado", (String) layout.get(2), numLinea, retorno);
        }
        //4.  Validar Nombre del Adreditado
        ValidadorDesembolsosMasivos.existeCampo("Nombre del Acreditado", (String) layout.get(3), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Nombre del Acreditado", (String) layout.get(3), 80,
                                                              numLinea, retorno);
        }
        //5.  Validar Monto Original del Credito
        ValidadorDesembolsosMasivos.existeCampo("Monto original del cr�dito", (String) layout.get(4), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Monto original del cr�dito",
                                                                     (String) layout.get(4), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Monto original del cr�dito", (String) layout.get(4), 19,
                                                             2, numLinea, retorno);
        }
        //6.  Validar Fecha de Disposicion
        ValidadorDesembolsosMasivos.existeCampo("Fecha de disposici�n", (String) layout.get(5), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaFecha("Fecha de disposici�n", (String) layout.get(5), numLinea, retorno);
        }
        //7.  Validar Saldo Insoluto del Estado de Cuenta
        ValidadorDesembolsosMasivos.existeCampo("Saldo insoluto del estado de cuenta", (String) layout.get(6), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Saldo insoluto del estado de cuenta",
                                                                     (String) layout.get(6), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Saldo insoluto del estado de cuenta",
                                                             (String) layout.get(6), 19, 2, numLinea, retorno);
        }
        //8.  Validar Capital Vigente
        ValidadorDesembolsosMasivos.existeCampo("Capital vigente", (String) layout.get(7), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Capital vigente", (String) layout.get(7),
                                                                     numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Capital vigente", (String) layout.get(7), 19, 2, numLinea,
                                                             retorno);
        }
        //9.  Validar Capital Vencido
        ValidadorDesembolsosMasivos.existeCampo("Capital vencido", (String) layout.get(8), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Capital vencido", (String) layout.get(8),
                                                                     numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Capital vencido", (String) layout.get(8), 19, 2, numLinea,
                                                             retorno);
        }
        //10. Validar Interes Ordinario
        ValidadorDesembolsosMasivos.existeCampo("Inter�s ordinario", (String) layout.get(9), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Inter�s ordinario", (String) layout.get(9),
                                                                     numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Inter�s ordinario", (String) layout.get(9), 19, 2,
                                                             numLinea, retorno);
        }
        //11. Validar Interes Moratorio
        ValidadorDesembolsosMasivos.existeCampo("Inter�s moratorio", (String) layout.get(10), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Inter�s moratorio", (String) layout.get(10),
                                                                     numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Inter�s moratorio", (String) layout.get(10), 19, 2,
                                                             numLinea, retorno);
        }
        //12. Validar Fecha Ultimo Pago de Capital
        ValidadorDesembolsosMasivos.existeCampo("Fecha �ltimo pago de capital", (String) layout.get(11), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaFecha("Fecha �ltimo pago de capital", (String) layout.get(11), numLinea,
                                                    retorno);
        }
        //13. Validar Fecha Ultimo Pago de Interes
        ValidadorDesembolsosMasivos.existeCampo("Fecha �ltimo pago de inter�s", (String) layout.get(12), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaFecha("Fecha �ltimo pago de inter�s", (String) layout.get(12), numLinea,
                                                    retorno);
        }
        //14. Validar Intereses Recuperados Posteriores al Primer Incumplimiento
        ValidadorDesembolsosMasivos.existeCampo("Intereses recuperados posteriores al primer incumplimiento",
                                                (String) layout.get(13), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Intereses recuperados posteriores al primer incumplimiento",
                                                                     (String) layout.get(13), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Intereses recuperados posteriores al primer incumplimiento",
                                                             (String) layout.get(13), 19, 2, numLinea, retorno);
        }
        //15. Validar Periodicidad de Pago
        ValidadorDesembolsosMasivos.existeCampo("Periodicidad de pago", (String) layout.get(14), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Periodicidad de pago", (String) layout.get(14), numLinea,
                                                            retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Periodicidad de pago", (String) layout.get(14), 2, 0,
                                                             numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaClavePeriodicidadPago("Periodicidad de pago", (String) layout.get(14),
                                                                    numLinea, retorno);
        }
        //16. Validar Fecha de Primer Incumplimiento
        ValidadorDesembolsosMasivos.existeCampo("Fecha de primer incumplimiento", (String) layout.get(15), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaFecha("Fecha de primer incumplimiento", (String) layout.get(15), numLinea,
                                                    retorno);
        }
        //17. Validar Fecha de Segundo Incumplimiento
        if (ValidadorDesembolsosMasivos.esPeriodicidadMensual((String) layout.get(14))) {
            ValidadorDesembolsosMasivos.existeCampo("Fecha de segundo incumplimiento", (String) layout.get(16),
                                                    numLinea, retorno);
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validaFecha("Fecha de segundo incumplimiento", (String) layout.get(16),
                                                        numLinea, retorno);
            }
        } else {
            ValidadorDesembolsosMasivos.campoNoRequerido("Fecha de segundo incumplimiento", (String) layout.get(16),
                                                         numLinea, retorno);
        }
        //18. Validar Fondeo Nafin
        ValidadorDesembolsosMasivos.existeCampo("Fondeo Nafin", (String) layout.get(17), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.esSoN("Fondeo Nafin", (String) layout.get(17), numLinea, retorno);
        }
        //19. Validar Tasa de Fondeo Nafin
        if (ValidadorDesembolsosMasivos.fondeoNafinHabilitado((String) layout.get(17))) {
            ValidadorDesembolsosMasivos.existeCampo("Tasa de fondeo Nafin", (String) layout.get(18), numLinea, retorno);
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validaNumeroFlotantePositivo("Tasa de fondeo Nafin",
                                                                         (String) layout.get(18), numLinea, retorno);
            }
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validaLongitudNumero("Tasa de fondeo Nafin", (String) layout.get(18), 7, 2,
                                                                 numLinea, retorno);
            }
        } else {
            ValidadorDesembolsosMasivos.campoNoRequerido("Tasa de fondeo Nafin", (String) layout.get(18), numLinea,
                                                         retorno);
        }
        //20. Validar Fecha de Liquidacion de Fondeo
        if (ValidadorDesembolsosMasivos.fondeoNafinHabilitado((String) layout.get(17))) {
            ValidadorDesembolsosMasivos.existeCampo("Fecha de liquidaci�n de fondeo", (String) layout.get(19), numLinea,
                                                    retorno);
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validaFecha("Fecha de liquidaci�n de fondeo", (String) layout.get(19),
                                                        numLinea, retorno);
            }
        } else {
            ValidadorDesembolsosMasivos.campoNoRequerido("Fecha de liquidaci�n de fondeo", (String) layout.get(19),
                                                         numLinea, retorno);
        }
        //21. Validar Numero Prestamo SIRAC
        if (ValidadorDesembolsosMasivos.fondeoNafinHabilitado((String) layout.get(17))) {
            ValidadorDesembolsosMasivos.existeCampo("N�mero pr�stamo SIRAC", (String) layout.get(20), numLinea,
                                                    retorno);
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validarNumeroEntero("N�mero pr�stamo SIRAC", (String) layout.get(20),
                                                                numLinea, retorno);
            }
            if (!retorno.getErrorLinea()) {
                ValidadorDesembolsosMasivos.validaLongitudNumero("N�mero pr�stamo SIRAC", (String) layout.get(20), 12,
                                                                 0, numLinea, retorno);
            }
        } else {
            ValidadorDesembolsosMasivos.campoNoRequerido("N�mero pr�stamo SIRAC", (String) layout.get(20), numLinea,
                                                         retorno);
        }
        //22. Validar Ejecutivo Responsable del Expediente de Credito
        ValidadorDesembolsosMasivos.existeCampo("Ejecutivo responsable del expediente de cr�dito",
                                                (String) layout.get(21), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Ejecutivo responsable del expediente de cr�dito",
                                                              (String) layout.get(21), 100, numLinea, retorno);
        }
        //23. Validar Correo Electronico (abarca dos campos el 23 y 24
        ValidadorDesembolsosMasivos.existeCampo("Correo electr�nico",
                                                (String) layout.get(22) + "@" + (String) layout.get(23), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Correo electr�nico",
                                                              (String) layout.get(22) + "@" + (String) layout.get(23),
                                                              40, numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarCorreo("Correo electr�nico",
                                                      (String) layout.get(22) + "@" + (String) layout.get(23), numLinea,
                                                      retorno);
        }
        //24. Validar Ubicacion del Expediente Calle y Numero
        ValidadorDesembolsosMasivos.existeCampo("Ubicaci�n del expediente calle y n�mero", (String) layout.get(24),
                                                numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Ubicaci�n del expediente calle y n�mero",
                                                              (String) layout.get(24), 150, numLinea, retorno);
        }
        //25. Validar Ubicacion del Expediente Colonia
        ValidadorDesembolsosMasivos.existeCampo("Ubicaci�n del expediente colonia", (String) layout.get(25), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarLongitudCadena("Ubicaci�n del expediente colonia",
                                                              (String) layout.get(25), 80, numLinea, retorno);
        }
        //26. Validar Clave del Estado o Entidad Federativa
        ValidadorDesembolsosMasivos.existeCampo("Clave del Estado o Entidad Federativa", (String) layout.get(26),
                                                numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Clave del Estado o Entidad Federativa",
                                                            (String) layout.get(26), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Clave del Estado o Entidad Federativa",
                                                             (String) layout.get(26), 4, 0, numLinea, retorno);
        }
        //27. Validar Clave del Municipio o Delegacion
        ValidadorDesembolsosMasivos.existeCampo("Clave del Municipio o Delegaci�n", (String) layout.get(27), numLinea,
                                                retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Clave del Municipio o Delegaci�n", (String) layout.get(27),
                                                            numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Clave del Municipio o Delegaci�n",
                                                             (String) layout.get(27), 4, 0, numLinea, retorno);
        }
        //28. Validar Telefono
        ValidadorDesembolsosMasivos.existeCampo("Tel�fono", (String) layout.get(28), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Tel�fono", (String) layout.get(28), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Tel�fono", (String) layout.get(28), 10, 0, numLinea,
                                                             retorno);
        }
        //29. Validar Extension
        ValidadorDesembolsosMasivos.existeCampo("Extensi�n", (String) layout.get(29), numLinea, retorno);
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validarNumeroEntero("Extensi�n", (String) layout.get(29), numLinea, retorno);
        }
        if (!retorno.getErrorLinea()) {
            ValidadorDesembolsosMasivos.validaLongitudNumero("Extensi�n", (String) layout.get(29), 6, 0, numLinea,
                                                             retorno);
        }
    }

    /**
     * Procesa la carga de Saldos. Lee el archivo y realiza validaciones basicas
     * y posteriormente lo almacena de manera temporal (gtitmp_contenido_arch)
     * si no hay errores.
     *
     *
     *
     *
     * @param strDirectorio Ruta del archivo
     * @param esNombreArchivo Nombre del archivo
     * @param num_registros Cifras de control. Numero de registros
     * @param sum_montos Cifras de control. Monto total
     * @param aniomes Cifras de control. A�o y mes
     * @param ic_if Clave del intermediario financiero
     * @return ???
     *
     */
    public ResultadosGar procesarCargaSaldos(String strDirectorio, String esNombreArchivo, String num_registros,
                                             String sum_montos, String aniomes, String ic_if) throws NafinException {
        log.info("GarantiasBean::procesarCargaSaldos(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());

        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        try {
            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());

            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            String cs_valida_tarjeta = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            int reg = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);
            List clavesFinan = new ArrayList();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            con.conexionDB();

            qrySentencia = "select ic_if_siag from comcat_if" + " where ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();
            retorno.setIcProceso(calculaIcProceso());

            //Busqueda par�metro cs_valida_tarjeta SMJ 06/07/2006

            qrySentencia = "SELECT cs_valida_tarjeta " + " FROM gti_if_siag " + " WHERE ic_if_siag=? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, (ifSiag));
            rs = ps.executeQuery();
            if (rs.next()) {
                cs_valida_tarjeta = rs.getString("cs_valida_tarjeta");
            }
            rs.close();
            ps.close();

            log.info("Query valida tarjeta" + qrySentencia);

            int longitudClaveFinanciamiento = operaProgramaSHFCreditoHipotecario(ic_if) ? 30 : 20;

            qrySentencia = "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" + "values(?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    BigDecimal tmpMonto = new BigDecimal(0);
                    BigDecimal totPagar = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    lsLinea = lsLinea.trim();

                    if (lineas < 700) {
                        if (sbInClavesFinan.length() > 0)
                            sbInClavesFinan.append(",");
                        sbInClavesFinan.append("?");
                    }

                    if (lsLinea.length() == 0) {
                        clavesFinan.add("0");
                        continue;
                    } else
                        ctrl_linea++;
                    try {
                        vt = new VectorTokenizer(lsLinea, "@");
                        lovDatos = vt.getValuesVector();


                        //Valida el parametro

                        if ("S".equals(cs_valida_tarjeta)) {
                            reg = 12 + 3;
                        } else {
                            reg = 9 + 3 + 3;
                        }
                        log.info("reg" + reg);

                        if (lovDatos.size() < reg) {
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  " El numero de campos es incorrecto." + lovDatos.size() + " \n");
                            retorno.setErrorLinea(true);
                            clavesFinan.add("0");
                            retorno.incNumRegErr();
                            continue;
                        }
                    } catch (Exception e) {
                        retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto." +
                                              lovDatos.size() + " \n");
                        retorno.setErrorLinea(true);
                        clavesFinan.add("0");
                        retorno.incNumRegErr();
                        continue;
                    }

                    validaFlotante(numLinea, (String) lovDatos.get(6), "Saldo al ultimo dia del mes", retorno);
                    if (!retorno.getErrorLinea()) {
                        tmpMonto = new BigDecimal(((String) lovDatos.get(6)).trim());
                    }

                    clavesFinan.add(lovDatos.get(1));

                    //validaEntero(numLinea,(String)lovDatos.get(1),"Clave del Financiamiento",retorno);
                    validaIntermediario(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(0),
                                        "Clave del Intermediario Financiero", ifSiag, retorno);
                    validarLongitudCadena(numLinea, (String) lovDatos.get(1), "Clave del Financiamiento", retorno,
                                          longitudClaveFinanciamiento);
                    validaFechaFechaSaldo(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(2), aniomes,
                                          retorno);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(3), "Monto comision",
                                   retorno);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(4), "Monto IVA", retorno);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(5), "Total a pagar",
                                   retorno);
                    try {
                        totPagar = new BigDecimal(((String) lovDatos.get(5)).trim());
                    } catch (Exception e) {
                        retorno.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" +
                                              (String) lovDatos.get(1) +
                                              ". El campo total a pagar no es un numero valido\n");
                        retorno.setErrorLinea(true);
                    }
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(6),
                                   "Saldo del acreditado al ultimo dia del mes", retorno);
                    //if(!"".equals(lovDatos.get(7)))
                    validaEntero(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(7), "Mora en dias", retorno);


                    reg = 0;
                    if ("S".equals(cs_valida_tarjeta)) {
                        reg = 12 + 3;
                    } else {
                        reg = 9 + 3 + 3;
                    }
                    log.info("reg" + reg);

                    if (lovDatos.size() > reg) {
                        retorno.setErrorLinea(true);
                        retorno.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" +
                                              (String) lovDatos.get(1) +
                                              ".  El numero de campos es mayor al debido. \n");
                    }

                    //Valida el parametro para leer los campos 9,10,11 y 12,13,14
                    if ("S".equals(cs_valida_tarjeta)) {
                        validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(8), "Saldo al Corte",
                                        retorno);
                        validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(9),
                                        "Suma de Disposiciones", retorno);
                        try {
                            double saldoCorte = Double.parseDouble(lovDatos.get(8).toString().trim());
                            double sumaDisp = Double.parseDouble(lovDatos.get(9).toString().trim());
                            if (saldoCorte != 0 || sumaDisp != 0) {
                                validarLongitudCadena(numLinea, (String) lovDatos.get(10),
                                                      "Fecha de Otorgamiento de la Linea", retorno, 8);
                                if (!retorno.getErrorLinea())
                                    validaFecha(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(10),
                                                "Fecha de Otorgamiento de la Linea", retorno);
                            } else if (!"".equals(lovDatos.get(10).toString().trim())) {
                                validarLongitudCadena(numLinea, (String) lovDatos.get(10),
                                                      "Fecha de Otorgamiento de la Linea", retorno, 8);
                                if (!retorno.getErrorLinea())
                                    validaFecha(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(10),
                                                "Fecha de Otorgamiento de la Linea", retorno);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        try {
                            validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(8),
                                            "Saldo al Corte", retorno);
                            validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(9),
                                            "Suma de Disposiciones", retorno);
                            if (!retorno.getErrorLinea()) {
                                double saldoCorte = Double.parseDouble(lovDatos.get(8).toString().trim());
                                if (saldoCorte != 0) {
                                    retorno.setErrorLinea(true);
                                    retorno.appendErrores("Error en la linea:" + numLinea +
                                                          ", El Saldo al Corte debe ser 0.00. \n");
                                }
                            }
                            if (!retorno.getErrorLinea()) {
                                double sumaDisp = Double.parseDouble(lovDatos.get(9).toString().trim());
                                if (sumaDisp != 0) {
                                    retorno.setErrorLinea(true);
                                    retorno.appendErrores("Error en la linea:" + numLinea +
                                                          ", La Suma de Disposiciones debe ser 0.00. \n");
                                }
                            }
                            if (lovDatos.get(10) != null && !"".equals((String) lovDatos.get(10))) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores("Error en la linea:" + numLinea +
                                                      ", La Fecha de Otorgamiento de la Linea debe venir vac�a. \n");
                            }
                        } catch (Exception e) {
                        }
                    }

                    // Fodea 035 - 2010.
                    try {
                        // Fodea 035 - 2010. Validar Campo: Calificacion Variable. Como es tipo CHAR no se valida el contenido
                        validarLongitudCadena(numLinea, (String) lovDatos.get(11), "Calificacion Variable", retorno, 5);
                        // Fodea 035 - 2010. Validar Campo: Estatus de Garant�a Cancelaci�n/Liquidaci�n
                        boolean hayDatosEstatusDeGarantia = false;
                        if (lovDatos.get(12) != null && !"".equals((String) lovDatos.get(12))) {
                            hayDatosEstatusDeGarantia = true;
                            validarLongitudCadena(numLinea, (String) lovDatos.get(12),
                                                  "Estatus de Garant�a Cancelaci�n/Liquidaci�n", retorno, 1);
                            if (!retorno.getErrorLinea() && !("1".equals((String) lovDatos.get(12))) &&
                                !("2".equals((String) lovDatos.get(12)))) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores("Error en la linea:" + numLinea +
                                                      ", El valor del Estatus de garant�a debe ser 1 o 2. \n");
                            }
                        }
                        // Fodea 035 - 2010. Validar Campo: Fecha de Cancelaci�n/Liquidaci�n
                        if (hayDatosEstatusDeGarantia &&
                            (lovDatos.get(13) == null || "".equals((String) lovDatos.get(13)))) {
                            retorno.setErrorLinea(true);
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  ", El valor de la Fecha de cancelaci�n/liquidaci�n es requerido cuando se env�a valor en Estatus de garant�a.");
                        }
                        if (!hayDatosEstatusDeGarantia &&
                            (lovDatos.get(13) != null && !"".equals((String) lovDatos.get(13)))) {
                            retorno.setErrorLinea(true);
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  ", El valor de la Fecha de Cancelaci�n/Liquidaci�n no es requerido cuando no se env�a valor en Estatus de garant�a.");
                        }
                        if (!retorno.getErrorLinea() && hayDatosEstatusDeGarantia) {
                            validarLongitudCadena(numLinea, (String) lovDatos.get(13),
                                                  "Fecha de Cancelaci�n/Liquidaci�n", retorno, 8);
                        }
                        if (!retorno.getErrorLinea() && hayDatosEstatusDeGarantia) {
                            validaFecha(numLinea, (String) lovDatos.get(13), "Fecha de Cancelaci�n/Liquidaci�n",
                                        retorno);
                        }

                    } catch (Exception e) {
                    }

                    if (!retorno.getErrorLinea()) {
                        //retorno.appendCorrectos("Linea:"+numLinea+" Clave del Financiamiento: "+lovDatos.get(1)+" Saldo al Fin de mes="+Comunes.formatoDecimal((String)lovDatos.get(06),2)+".\n");
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de Financiamiento:\n");
                        }
                        retorno.appendCorrectos(lovDatos.get(1) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumRegOk(tmpMonto);
                        retorno.addSumTotPag(totPagar);
                        //insercion en tabla temporal solo si no hay errores
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc(); //invocacion al garbage collector para optimizar memoria
                            }
                        }
                    } else {
                        retorno.incNumRegErr();
                        retorno.addSumRegErr(tmpMonto.doubleValue());
                    }
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                    iNumRegTrans++;
                } // for
            }
            ps.close();

            if (sbInClavesFinan.length() > 0) {
                int ind1 = 0;
                int ind2 = 0;
                qrySentencia =
                    "select gs.ic_folio,gs.cc_garantia" + " from gti_garant_saldos gs" + " ,gti_estatus_solic es" +
                    " where gs.ic_folio = es.ic_folio" + " and gs.ic_if_siag = es.ic_if_siag" +
                    " and es.ic_situacion not in(5,7)" + " and gs.ic_if_siag = ?" + " and gs.cg_anio_mes =  ?" +
                    " and gs.cc_garantia in(" + sbInClavesFinan.toString() + ")";
                log.info(qrySentencia);
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, ifSiag);
                ps.setString(2, aniomes);
                while (ind2 < clavesFinan.size()) {
                    if (clavesFinan.size() - ind2 < 700) {
                        sbInClavesFinan = new StringBuffer();
                        for (int ind3 = 0; ind3 < clavesFinan.size() - ind2; ind3++) {
                            if (sbInClavesFinan.length() > 0)
                                sbInClavesFinan.append(",");
                            sbInClavesFinan.append("?");
                        }
                        qrySentencia =
                            "select gs.ic_folio,gs.cc_garantia" + " from gti_garant_saldos gs" +
                            " ,gti_estatus_solic es" + " where gs.ic_folio = es.ic_folio" +
                            " and gs.ic_if_siag = es.ic_if_siag" + " and es.ic_situacion not in(5,7)" +
                            " and gs.ic_if_siag = ?" + " and gs.cg_anio_mes =  ?" + " and gs.cc_garantia in(" +
                            sbInClavesFinan.toString() + ")";
                        log.info(qrySentencia);
                        ps = con.queryPrecompilado(qrySentencia);
                        ps.setInt(1, ifSiag);
                        ps.setString(2, aniomes);
                    }
                    for (ind1 = 0; ind2 < clavesFinan.size() && ind1 < 700; ind1++, ind2++) {
                        log.info("EL VALOR DE IND1=" + ind1);
                        ps.setString(ind1 + 3, clavesFinan.get(ind2).toString());
                    }
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        retorno.appendCifras("La clave: " + rs.getString("cc_garantia") +
                                             " ya se encuentra registrada este mes en el folio:" +
                                             rs.getString("ic_folio") + ".\n");
                    }
                } //while
                ps.close();
            }


            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            if (dbl_sum_montos.doubleValue() != ctrl_sum_montos.doubleValue()) {
                log.info("La sumatoria de los montos = " + dbl_sum_montos.doubleValue());
                log.info("La cifra de control capturada = " + ctrl_sum_montos.doubleValue());
                retorno.appendCifras("La sumatoria de los saldos no corresponde al contenido en el archivo.\n");
            }
            if (retorno.getCifras().length() ==
                0) {
                //Si no hay errores de cifras de control... establece claves de financiamiento

                //------------------------ Inicio mensajes para debug ---------------------------
                log.info("procesarCargaSaldos(" + _fechaMsg + "_" + retorno.getIcProceso() +
                                   ")::Cifra de Control OK. clavesFinan.size()=" + clavesFinan.size());
                //-------------------------- Fin mensajes para debug ----------------------------

                retorno.setClavesFinan(clavesFinan);
            }
            //log.info("Numero de lineas = "+lineas);
        } catch (Exception e) {
            log.info("GarantiasBean::procesarCargaSaldos(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0)
                    con.terminaTransaccion(false);
                else
                    con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::procesarCargaSaldos(S)");
        }
        return retorno;
    }

    public ResultadosGar procesarCargaSaldos(String strDirectorio, String esNombreArchivo, String num_registros,
                                             String sum_montos, String aniomesIni, String aniomesFin,
                                             String ic_if) throws NafinException {
        log.info("GarantiasBean::procesarCargaSaldos(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());

        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        try {
            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());

            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            String cs_valida_tarjeta = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            int reg = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);
            List clavesFinan = new ArrayList();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            con.conexionDB();

            qrySentencia = "select ic_if_siag from comcat_if" + " where ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();
            retorno.setIcProceso(calculaIcProceso());

            //Busqueda par�metro cs_valida_tarjeta SMJ 06/07/2006

            qrySentencia = "SELECT cs_valida_tarjeta " + " FROM gti_if_siag " + " WHERE ic_if_siag=? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, (ifSiag));
            rs = ps.executeQuery();
            if (rs.next()) {
                cs_valida_tarjeta = rs.getString("cs_valida_tarjeta");
            }
            rs.close();
            ps.close();

            log.info("Query valida tarjeta" + qrySentencia);

            int longitudClaveFinanciamiento = operaProgramaSHFCreditoHipotecario(ic_if) ? 30 : 20;

            qrySentencia = "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" + "values(?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            TreeMap<String, SaldosPagosComisionesVo> saldosPagosComisionesTMap = new TreeMap<>();
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    BigDecimal tmpMonto = new BigDecimal(0);
                    BigDecimal totPagar = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    lsLinea = lsLinea.trim();

                    if (lineas < 700) {
                        if (sbInClavesFinan.length() > 0)
                            sbInClavesFinan.append(",");
                        sbInClavesFinan.append("?");
                    }

                    if (lsLinea.length() == 0) {
                        clavesFinan.add("0");
                        continue;
                    } else
                        ctrl_linea++;
                    try {
                        vt = new VectorTokenizer(lsLinea, "@");
                        lovDatos = vt.getValuesVector();


                        //Valida el parametro

                        if ("S".equals(cs_valida_tarjeta)) {
                            reg = 12 + 3;
                        } else {
                            reg = 9 + 3 + 3;
                        }
                        log.info("reg" + reg);

                        if (lovDatos.size() < reg) {
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  " El numero de campos es incorrecto." + lovDatos.size() + " \n");
                            retorno.setErrorLinea(true);
                            clavesFinan.add("0");
                            retorno.incNumRegErr();
                            continue;
                        }
                    } catch (Exception e) {
                        retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto." +
                                              lovDatos.size() + " \n");
                        retorno.setErrorLinea(true);
                        clavesFinan.add("0");
                        retorno.incNumRegErr();
                        continue;
                    }

                    validaFlotante(numLinea, (String) lovDatos.get(6), "Saldo al ultimo dia del mes", retorno);
                    if (!retorno.getErrorLinea()) {
                        tmpMonto = new BigDecimal(((String) lovDatos.get(6)).trim());
                    }

                    clavesFinan.add(lovDatos.get(1));

                    //validaEntero(numLinea,(String)lovDatos.get(1),"Clave del Financiamiento",retorno);
                    validaIntermediario(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(0),
                                        "Clave del Intermediario Financiero", ifSiag, retorno);
                    validarLongitudCadena(numLinea, (String) lovDatos.get(1), "Clave del Financiamiento", retorno,
                                          longitudClaveFinanciamiento);
                    validaFechaFechaSaldoRango(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(2), aniomesIni, (String)lovDatos.get(12),
                                               retorno, saldosPagosComisionesTMap);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(3), "Monto comision",
                                   retorno);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(4), "Monto IVA", retorno);
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(5), "Total a pagar",
                                   retorno);
                    try {
                        totPagar = new BigDecimal(((String) lovDatos.get(5)).trim());
                    } catch (Exception e) {
                        retorno.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" +
                                              (String) lovDatos.get(1) +
                                              ". El campo total a pagar no es un numero valido\n");
                        retorno.setErrorLinea(true);
                    }
                    validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(6),
                                   "Saldo del acreditado al ultimo dia del mes", retorno);
                    //if(!"".equals(lovDatos.get(7)))
                    validaEntero(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(7), "Mora en dias", retorno);


                    reg = 0;
                    if ("S".equals(cs_valida_tarjeta)) {
                        reg = 12 + 3;
                    } else {
                        reg = 9 + 3 + 3;
                    }
                    log.info("reg" + reg);

                    if (lovDatos.size() > reg) {
                        retorno.setErrorLinea(true);
                        retorno.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" +
                                              (String) lovDatos.get(1) +
                                              ".  El numero de campos es mayor al debido. \n");
                    }

                    //Valida el parametro para leer los campos 9,10,11 y 12,13,14
                    if ("S".equals(cs_valida_tarjeta)) {
                        validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(8), "Saldo al Corte",
                                        retorno);
                        validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(9),
                                        "Suma de Disposiciones", retorno);
                        try {
                            double saldoCorte = Double.parseDouble(lovDatos.get(8).toString().trim());
                            double sumaDisp = Double.parseDouble(lovDatos.get(9).toString().trim());
                            if (saldoCorte != 0 || sumaDisp != 0) {
                                validarLongitudCadena(numLinea, (String) lovDatos.get(10),
                                                      "Fecha de Otorgamiento de la Linea", retorno, 8);
                                if (!retorno.getErrorLinea())
                                    validaFecha(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(10),
                                                "Fecha de Otorgamiento de la Linea", retorno);
                            } else if (!"".equals(lovDatos.get(10).toString().trim())) {
                                validarLongitudCadena(numLinea, (String) lovDatos.get(10),
                                                      "Fecha de Otorgamiento de la Linea", retorno, 8);
                                if (!retorno.getErrorLinea())
                                    validaFecha(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(10),
                                                "Fecha de Otorgamiento de la Linea", retorno);
                            }
                        } catch (Exception e) {
                        }
                    } else {
                        try {
                            validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(8),
                                            "Saldo al Corte", retorno);
                            validaFlotanteP(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(9),
                                            "Suma de Disposiciones", retorno);
                            if (!retorno.getErrorLinea()) {
                                double saldoCorte = Double.parseDouble(lovDatos.get(8).toString().trim());
                                if (saldoCorte != 0) {
                                    retorno.setErrorLinea(true);
                                    retorno.appendErrores("Error en la linea:" + numLinea +
                                                          ", El Saldo al Corte debe ser 0.00. \n");
                                }
                            }
                            if (!retorno.getErrorLinea()) {
                                double sumaDisp = Double.parseDouble(lovDatos.get(9).toString().trim());
                                if (sumaDisp != 0) {
                                    retorno.setErrorLinea(true);
                                    retorno.appendErrores("Error en la linea:" + numLinea +
                                                          ", La Suma de Disposiciones debe ser 0.00. \n");
                                }
                            }
                            if (lovDatos.get(10) != null && !"".equals((String) lovDatos.get(10))) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores("Error en la linea:" + numLinea +
                                                      ", La Fecha de Otorgamiento de la Linea debe venir vac�a. \n");
                            }
                        } catch (Exception e) {
                        }
                    }

                    // Fodea 035 - 2010.
                    try {
                        // Fodea 035 - 2010. Validar Campo: Calificacion Variable. Como es tipo CHAR no se valida el contenido
                        validarLongitudCadena(numLinea, (String) lovDatos.get(11), "Calificacion Variable", retorno, 5);
                        // Fodea 035 - 2010. Validar Campo: Estatus de Garant�a Cancelaci�n/Liquidaci�n
                        boolean hayDatosEstatusDeGarantia = false;
                        if (lovDatos.get(12) != null && !"".equals((String) lovDatos.get(12))) {
                            hayDatosEstatusDeGarantia = true;
                            validarLongitudCadena(numLinea, (String) lovDatos.get(12),
                                                  "Estatus de Garant�a Cancelaci�n/Liquidaci�n", retorno, 1);
                            if (!retorno.getErrorLinea() && !("1".equals((String) lovDatos.get(12))) &&
                                !("2".equals((String) lovDatos.get(12)))) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores("Error en la linea:" + numLinea +
                                                      ", El valor del Estatus de garant�a debe ser 1 o 2. \n");
                            }
                        }
                        // Fodea 035 - 2010. Validar Campo: Fecha de Cancelaci�n/Liquidaci�n
                        if (hayDatosEstatusDeGarantia &&
                            (lovDatos.get(13) == null || "".equals((String) lovDatos.get(13)))) {
                            retorno.setErrorLinea(true);
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  ", El valor de la Fecha de cancelaci�n/liquidaci�n es requerido cuando se env�a valor en Estatus de garant�a.");
                        }
                        if (!hayDatosEstatusDeGarantia &&
                            (lovDatos.get(13) != null && !"".equals((String) lovDatos.get(13)))) {
                            retorno.setErrorLinea(true);
                            retorno.appendErrores("Error en la linea:" + numLinea +
                                                  ", El valor de la Fecha de Cancelaci�n/Liquidaci�n no es requerido cuando no se env�a valor en Estatus de garant�a.");
                        }
                        if (!retorno.getErrorLinea() && hayDatosEstatusDeGarantia) {
                            validarLongitudCadena(numLinea, (String) lovDatos.get(13),
                                                  "Fecha de Cancelaci�n/Liquidaci�n", retorno, 8);
                        }
                        if (!retorno.getErrorLinea() && hayDatosEstatusDeGarantia) {
                            validaFecha(numLinea, (String) lovDatos.get(13), "Fecha de Cancelaci�n/Liquidaci�n",
                                        retorno);
                        }

                    } catch (Exception e) {
                    }

                    if (!retorno.getErrorLinea()) {
                        //retorno.appendCorrectos("Linea:"+numLinea+" Clave del Financiamiento: "+lovDatos.get(1)+" Saldo al Fin de mes="+Comunes.formatoDecimal((String)lovDatos.get(06),2)+".\n");
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de Financiamiento:\n");
                        }
                        retorno.appendCorrectos(lovDatos.get(1) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumRegOk(tmpMonto);
                        retorno.addSumTotPag(totPagar);
                        //insercion en tabla temporal solo si no hay errores
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc(); //invocacion al garbage collector para optimizar memoria
                            }
                        }
                    } else {
                        retorno.incNumRegErr();
                        retorno.addSumRegErr(tmpMonto.doubleValue());
                    }
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                    iNumRegTrans++;

                } // for
            }
            ps.close();
            validaFechasConsecutivo(retorno, saldosPagosComisionesTMap, aniomesIni);

            if (sbInClavesFinan.length() > 0) {
                int ind1 = 0;
                int ind2 = 0;
                
                
                qrySentencia =
                    "select gs.ic_folio,gs.cc_garantia " + " from gti_garant_saldos gs" + " ,gti_estatus_solic es" +
                    " where gs.ic_folio = es.ic_folio" + " and gs.ic_if_siag = es.ic_if_siag" +
                    " and es.ic_situacion not in(5,7)" + " and gs.ic_if_siag = ?" + 
                    " and gs.cg_anio_mes >  ?" +
                    " and gs.cg_anio_mes <=  ?" +
                    " and gs.cc_garantia in(" + sbInClavesFinan.toString() + ")";
                log.info(qrySentencia);
                // se supone formato aniomes: aaaamm, ej: 201801
                int anioMesFinal = Integer.valueOf(aniomesIni);
                int anioMesInicial = anioMesFinal-100;
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, ifSiag);
                ps.setString(2, anioMesInicial+"");
                ps.setString(3, anioMesFinal+"");
                
                
                while (ind2 < clavesFinan.size()) {
                    if (clavesFinan.size() - ind2 < 700) {
                        sbInClavesFinan = new StringBuffer();
                        for (int ind3 = 0; ind3 < clavesFinan.size() - ind2; ind3++) {
                            if (sbInClavesFinan.length() > 0)
                                sbInClavesFinan.append(",");
                            sbInClavesFinan.append("?");
                        }
                        qrySentencia =
                            "select gs.ic_folio,gs.cc_garantia " + " from gti_garant_saldos gs" +
                            " ,gti_estatus_solic es" + " where gs.ic_folio = es.ic_folio" +
                            " and gs.ic_if_siag = es.ic_if_siag" + " and es.ic_situacion not in(5,7)" +
                            " and gs.ic_if_siag = ?" + 
                            " and gs.cg_anio_mes >  ?" +
                            " and gs.cg_anio_mes <=  ?" +
                            " and gs.cc_garantia in(" +
                            sbInClavesFinan.toString() + ")";
                        log.info(qrySentencia);
                        ps = con.queryPrecompilado(qrySentencia);
                        ps.setInt(1, ifSiag);
                        ps.setString(2, anioMesInicial+"");
                        ps.setString(3, anioMesFinal+"");
                    }
                    for (ind1 = 0; ind2 < clavesFinan.size() && ind1 < 700; ind1++, ind2++) {
                        log.info("EL VALOR DE IND1=" + ind1);
                        ps.setString(ind1 + 4, clavesFinan.get(ind2).toString());
                    }
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        retorno.appendCifras("La clave: " + rs.getString("cc_garantia") +
                                             " ya se encuentra registrada este mes en el folio:" +
                                             rs.getString("ic_folio")  +".\n");
                    }
                } //while
                ps.close();
            }


            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            if (dbl_sum_montos.doubleValue() != ctrl_sum_montos.doubleValue()) {
                log.info("La sumatoria de los montos = " + dbl_sum_montos.doubleValue());
                log.info("La cifra de control capturada = " + ctrl_sum_montos.doubleValue());
                retorno.appendCifras("La sumatoria de los saldos no corresponde al contenido en el archivo.\n");
            }
            if (retorno.getCifras().length() ==
                0) {
                //Si no hay errores de cifras de control... establece claves de financiamiento

                //------------------------ Inicio mensajes para debug ---------------------------
                log.info("procesarCargaSaldos(" + _fechaMsg + "_" + retorno.getIcProceso() +
                                   ")::Cifra de Control OK. clavesFinan.size()=" + clavesFinan.size());
                //-------------------------- Fin mensajes para debug ----------------------------

                retorno.setClavesFinan(clavesFinan);
            }
            //log.info("Numero de lineas = "+lineas);
        } catch (Exception e) {
            log.info("GarantiasBean::procesarCargaSaldos(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0)
                    con.terminaTransaccion(false);
                else
                    con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::procesarCargaSaldos(S)");
        }
        return retorno;
    }

    /**
		@deprecated A partir del F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n este m�todo ha quedado deprecado.
		@modified_by jshernandez
		@modified_since F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n; 20/11/2013 11:41:00 a.m.
     */
    public ResultadosGar procesarAltaCalif(String strDirectorio, String esNombreArchivo, String anio_trim, String trim,
                                           String num_registros, String sum_montos_cub, String sum_montos_exp,
                                           String ic_if) throws NafinException {
        log.info("GarantiasBean::procesarAltaCalif(E)");
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        try {
            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos_cub = new BigDecimal(sum_montos_cub);
            BigDecimal ctrl_sum_montos_exp = new BigDecimal(sum_montos_exp);

            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());

            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            BigDecimal dbl_sum_montos_cub = new BigDecimal(0.00);
            BigDecimal dbl_sum_montos_exp = new BigDecimal(0.00);
            con.conexionDB();

            qrySentencia = "select ic_if_siag from comcat_if" + " where ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();

            retorno.setIcProceso(calculaIcProceso());

            qrySentencia = "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" + "values(?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {
                    BigDecimal tmpMontoCub = new BigDecimal(0);
                    BigDecimal tmpMontoExp = new BigDecimal(0);
                    retorno.setErrorLinea(false);
                    lsLinea = lsLinea.trim();
                    if (lsLinea.length() == 0)
                        continue;
                    else
                        ctrl_linea++;
                    vt = new VectorTokenizer(lsLinea, "@");
                    lovDatos = vt.getValuesVector();
                    if (lovDatos.size() < 10) {
                        retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto." +
                                              lovDatos.size() + " \n");
                        retorno.setErrorLinea(true);
                        continue;
                    }
                    validaFlotante(numLinea, (String) lovDatos.get(10), "Importe de Reservas Cubiertas", retorno);
                    if (!retorno.getErrorLinea()) {
                        tmpMontoCub = new BigDecimal(((String) lovDatos.get(10)).trim());
                    }
                    validaFlotante(numLinea, (String) lovDatos.get(11), "Importe de Reservas Expuestas", retorno);
                    if (!retorno.getErrorLinea()) {
                        tmpMontoExp = new BigDecimal(((String) lovDatos.get(11)).trim());
                    }
                    validaIntermediario(numLinea, (String) lovDatos.get(0), "Clave del Intermediario Financiero",
                                        ifSiag, retorno);
                    validaAnioTrim(numLinea, (String) lovDatos.get(1), anio_trim, "A�o trimestre calificacion",
                                   retorno);
                    validaAnioTrim(numLinea, (String) lovDatos.get(2), trim, "Trimestre calificacion", retorno);
                    validaRFC(numLinea, (String) lovDatos.get(4), retorno);

                    validaFlotante(numLinea, (String) lovDatos.get(8), "Porcentaje de reserva cubierto", retorno);
                    if (!retorno.getErrorLinea()) {
                        validaLongitudNumero(numLinea, (String) lovDatos.get(8), "Porcentaje de reserva cubierto",
                                             retorno, 5, 2);
                        validaRangoNumero(numLinea, (String) lovDatos.get(8), "Porcentaje de reserva cubierto", retorno,
                                          "0.00", "100.00");
                    }

                    validaFlotante(numLinea, (String) lovDatos.get(9), "Porcentaje de reserva expuesto", retorno);
                    if (!retorno.getErrorLinea()) {
                        validaLongitudNumero(numLinea, (String) lovDatos.get(9), "Porcentaje de reserva expuesto",
                                             retorno, 5, 2);
                        validaRangoNumero(numLinea, (String) lovDatos.get(9), "Porcentaje de reserva expuesto", retorno,
                                          "0.00", "100.00");
                    }

                    if (!retorno.getErrorLinea()) {
                        //retorno.appendCorrectos("Linea:"+numLinea+" Numero de Credito="+lovDatos.get(3)+".\n");

                        if (lineas == 0) {
                            retorno.appendCorrectos("Numeros de Credito:\n");
                        }
                        retorno.appendCorrectos(lovDatos.get(3) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumMontosCubOk(tmpMontoCub.doubleValue());
                        retorno.addSumMontosExpOk(tmpMontoExp.doubleValue());
                        //insercion en tabla temporal
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }
                        }
                    } else {
                        retorno.incNumRegErr();
                        retorno.addSumMontosCubErr(tmpMontoCub.doubleValue());
                        retorno.addSumMontosExpErr(tmpMontoExp.doubleValue());
                    }
                    dbl_sum_montos_cub = dbl_sum_montos_cub.add(tmpMontoCub);
                    dbl_sum_montos_exp = dbl_sum_montos_exp.add(tmpMontoExp);
                    iNumRegTrans++;
                } // for
            }
            ps.close();
            if (ctrl_linea != ctrl_num_registros)
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            if (dbl_sum_montos_cub.doubleValue() != ctrl_sum_montos_cub.doubleValue()) {
                log.info("La sumatoria de Importes de Reservas Cubiertas  = " +
                                   dbl_sum_montos_cub.doubleValue());
                log.info("La cifra de control Imp Res Cub  = " + ctrl_sum_montos_cub.doubleValue());
                log.info("La sumatoria de Importes de Reservas Cubiertas no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("La sumatoria de Importes de Reservas Cubiertas no corresponde al contenido en el archivo.\n");
            }
            if (dbl_sum_montos_exp.doubleValue() != ctrl_sum_montos_exp.doubleValue()) {
                log.info("La sumatoria de Importes de Reservas Expuestas  = " +
                                   dbl_sum_montos_exp.doubleValue());
                log.info("La cifra de control Imp Res Exp = " + ctrl_sum_montos_exp.doubleValue());
                log.info("La sumatoria de Importes de Reservas Expuestas no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("La sumatoria de Importes de Reservas Expuestas no corresponde al contenido en el archivo.\n");
            }
            //log.info("Numero de lineas = "+lineas);
        } catch (Exception e) {
            log.info("GarantiasBean::procesarAltaCalif(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0)
                    con.terminaTransaccion(false);
                else
                    con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::procesarAltaCalif(S)");
        }
        return retorno;
    }

    /**
     * 13/11/2013 04:29:55 p.m.
     */
    public ResultadosGar procesarAltaCalif(String strDirectorio, String esNombreArchivo, String anioTrimestreControl,
                                           String trimestreControl, String numeroRegistrosControl,
                                           String montoExposicionIncumplimientoControl,
                                           String claveIF) throws NafinException {

        log.info("procesarAltaCalif(E)");

        ResultadosGar retorno = new ResultadosGar();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        boolean exito = true;

        String lsLinea = "";
        int numLinea = 1;
        int ifSiag = 0;

        try {

            int controlNumeroRegistros = Integer.parseInt(numeroRegistrosControl);
            BigDecimal controlMontoExposicionIncumplimiento = new BigDecimal(montoExposicionIncumplimientoControl);

            // En caso de que el archivo tenga extension ZIP, descomprimirlo
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 3, esNombreArchivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            java.io.File lofArchivo = new java.io.File(esNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            VectorTokenizer vt = null;
            Vector lovDatos = null;

            int lineas = 0;
            int numeroRegistros = 0;
            BigDecimal sumatoriaMontoExposicionIncumplimiento = null;
            int iNumRegTrans = 0;
            BigDecimal montoExposicionIncumplimiento = null;

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave del Intermediario en el SIAG
            qrySentencia = " SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("IC_IF_SIAG");
            }
            rs.close();
            ps.close();

            // Obtener ID del Proceso
            retorno.setIcProceso(calculaIcProceso());

            qrySentencia =
                "INSERT INTO                                 " + "   GTITMP_CONTENIDO_ARCH                    " +
                "      (                                     " + "         IC_PROCESO, IC_LINEA, CG_CONTENIDO " +
                "      )                                     " + "   VALUES                                   " +
                "      (                                     " + "         ?,?,?                              " +
                "      )                                     ";
            ps = con.queryPrecompilado(qrySentencia);

            // Validar Cada uno de los Registros
            if (retorno.getErrores().length() == 0) {

                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; lineas++, numLinea++) {

                    // Resetear Flag de Error por l�nea
                    retorno.setErrorLinea(false);

                    // Verificar que la L�nea sea validable
                    lsLinea = lsLinea.trim();
                    if (lsLinea.length() == 0) {
                        continue;
                    } else if (lsLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
                        lsLinea = lsLinea.substring(0, GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH);
                    }

                    // Actualizar conteo de l�neas
                    numeroRegistros++;

                    // Validar que no se exceda el n�mero m�ximo de lineas que pueden ser guardadas en la base de datos
                    if (numeroRegistros > GTI_CONTENIDO_ARCH_IC_LINEA_MAX) {
                        retorno.appendErrores("Error: Se excedi� el n�mero m�ximo de registros por carga: " +
                                              GTI_CONTENIDO_ARCH_IC_LINEA_MAX + "\n");
                        break;
                    }

                    // Validar Registro
                    this.validarAltaCalificacionCartera(ifSiag, numLinea, lsLinea, retorno, anioTrimestreControl,
                                                        trimestreControl);

                    // Contabilizar Monto de Exposici�n al Incumplimiento
                    try {
                        vt = new VectorTokenizer(lsLinea, "@");
                        lovDatos = vt.getValuesVector();
                        montoExposicionIncumplimiento = new BigDecimal(((String) lovDatos.get(8)).trim());
                    } catch (Throwable e) {
                        montoExposicionIncumplimiento = new BigDecimal(0);
                    }
                    retorno.addSumTotPag(montoExposicionIncumplimiento);

                    // La validacion del registro fue exitosa
                    if (!retorno.getErrorLinea()) {

                        if (lineas == 0) {
                            retorno.appendCorrectos("Numeros de Credito:\n");
                        }

                        retorno.appendCorrectos(lovDatos.get(3) + "\n");

                        retorno.incNumRegOk();
                        retorno.addSumRegOk(montoExposicionIncumplimiento);

                        // Insertar registro en tabla temporal
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {

                            ps.clearParameters();
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();

                            // Dar Commit cada 3500 registros
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }

                        }

                        // La validacion present� errores
                    } else {

                        retorno.incNumRegErr();
                        retorno.addSumRegErr(montoExposicionIncumplimiento);

                    }

                    iNumRegTrans++;

                } // for

            }
            ps.close();

            if (numeroRegistros != controlNumeroRegistros) {
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            }

            sumatoriaMontoExposicionIncumplimiento = retorno.getValorSumTotPag();
            // if( !sumatoriaMontoExposicionIncumplimiento.equals(controlMontoExposicionIncumplimiento) ){
            if (retorno.getSumTotPag() != controlMontoExposicionIncumplimiento.doubleValue()) {
                log.info("procesarAltaCalif: La sumatoria de Exposici�n al Incumplimiento = " + retorno.getSumTotPag() +
                         ", Control = " + controlMontoExposicionIncumplimiento.doubleValue());
                log.info("procesarAltaCalif: La sumatoria de Exposici�n al Incumplimiento (EI) no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("La sumatoria de Exposici�n al Incumplimiento (EI) no corresponde al contenido en el archivo.\n");
            }

            //log.info("Numero de lineas = "+lineas);

        } catch (Exception e) {

            exito = false;

            log.error("procesarAltaCalif(Exception)");
            log.error("procesarAltaCalif.strDirectorio                        = <" + strDirectorio + ">");
            log.error("procesarAltaCalif.esNombreArchivo                      = <" + esNombreArchivo + ">");
            log.error("procesarAltaCalif.anioTrimestreControl                 = <" + anioTrimestreControl + ">");
            log.error("procesarAltaCalif.trimestreControl                     = <" + trimestreControl + ">");
            log.error("procesarAltaCalif.numeroRegistrosControl               = <" + numeroRegistrosControl + ">");
            log.error("procesarAltaCalif.montoExposicionIncumplimientoControl = <" +
                      montoExposicionIncumplimientoControl + ">");
            log.error("procesarAltaCalif.claveIF                              = <" + claveIF + ">");
            log.error("procesarAltaCalif.lsLinea(local)                       = <" + lsLinea + ">");
            log.error("procesarAltaCalif.numLinea(local)                      = <" + numLinea + ">");
            log.error("procesarAltaCalif.ifSiag(local)                        = <" + ifSiag + ">");
            e.printStackTrace();

            throw new NafinException("SIST0001");

        } finally {

            if (con.hayConexionAbierta()) {

                if (retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0) {
                    exito = false;
                }
                con.terminaTransaccion(exito);
                con.cierraConexionDB();

            }

            log.info("procesarAltaCalif(S)");

        }

        return retorno;

    }

    private String descomprimirArchivo(String strDirectorio, String esNombreArchivo) throws Exception {
        InputStream inStream = new FileInputStream(esNombreArchivo);
        ZipInputStream zipInStream = new ZipInputStream(inStream);
        ZipEntry zipEntry = null;
        byte arrByte[] = new byte[4096];
        OutputStream outstream = null;
        while ((zipEntry = zipInStream.getNextEntry()) != null) {
            if (zipEntry.isDirectory()) {
                continue; /*Ignora Directorios*/
            }
            String rutaArchivo = null;
            rutaArchivo = zipEntry.getName();
            if (outstream == null) {
                outstream = new BufferedOutputStream(new FileOutputStream(strDirectorio + rutaArchivo));
                esNombreArchivo = strDirectorio + rutaArchivo;
            }
            int i = 0;
            while ((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
                outstream.write(arrByte, 0, i);
            }
            outstream.close();
        }
        zipInStream.close();
        return esNombreArchivo;
    }


    /**
     * Realiza el alta de garantias, las cuales ya han sido
     * previamente validadas.
     *
     * @param icProceso Clave del proceso de carga
     * @param ic_if clave del IF
     * @param num_registros Numero de registros cargados
     * @param impte_total Importe total cargado
     * @param ic_usuario clave del usuario
     * @return ???
     * @throws com.netro.exception.NafinException
     */
    public ResultadosGar transmitirAltaGarantia(String icProceso, String ic_if, String num_registros,
                                                String impte_total, String ic_usuario) throws NafinException {
        log.info("GarantiasBean::transmitirAltaGarantia(E)");
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qrySentencia = "";
        boolean ok = true;
        try {
            con.conexionDB();
            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            //int consecutivo				= 0;
            //int ic_if_siag				= 0;
            //int ic_nafin_electronico	= 0;
            String fecha = "";
            String hora = "";
            //String folio				= "";
            String anio = "";
            String trim_calif = "";
            int ic_contragarante = 0;

            Registros reg = this.getDatosFolio(ic_if);
            /*				qrySentencia =
					"SELECT SEQ_GTI_ESTATUS_SOLIC.NEXTVAL as CONSECUTIVO"+
					" ,cn.ic_nafin_electronico"+
					" ,i.ic_if_siag"+
					" ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA"+
					" ,TO_CHAR(sysdate,'HH24:MI') as HORA"+
					" ,TO_CHAR(sysdate,'yyyymmdd') as FECFOLIO"+
					" ,TO_CHAR(sysdate,'yyyy') as ANIO"+
					" ,case"+
					"	when to_char(sysdate,'mm') in('01','02','03') then 1"   +
					"	when to_char(sysdate,'mm') in('04','05','06') then 2"   +
					"	when to_char(sysdate,'mm') in('07','08','09') then 3"   +
					"	when to_char(sysdate,'mm') in('10','11','12') then 4"   +
					"  end as TRIM_CALIF"  +
					" FROM COMREL_NAFIN CN"+
					" ,COMCAT_IF I"+
					" WHERE CN.ic_epo_pyme_if = i.ic_if"+
					" and CN.cg_tipo = 'I'"+
					" and CN.ic_epo_pyme_if = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
				rs = ps.executeQuery();
				if(rs.next()){
*/

            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));

                try {
                    ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                } catch (Exception e) {
                    ic_if_siag = new Integer(0);
                }
                fecha = reg.getString("fecha");
                hora = reg.getString("hora");
                anio = reg.getString("anio");
                trim_calif = reg.getString("trimestre");
            }
            //				ps.close();
            /*			//FODEA 002 - 2011 ACF
				qrySentencia =
					"SELECT ic_contragarante"+
					" FROM GTI_SERVICIOS S"+
					" WHERE S.IC_SERVICIO = 1";
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				if(rs.next()){
					ic_contragarante = rs.getInt(1);
				}
				ps.close();
*/
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            qrySentencia =
                "insert into gti_estatus_solic" + "(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
                " ,in_registros,fn_impte_total,ic_usuario_facultado,ic_situacion" +
                " ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante)" +
                " values(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,?" + " ,?,?,?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha + " " + hora);
            ps.setString(5, num_registros);
            ps.setDouble(6, Double.parseDouble(impte_total));
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1);
            ps.setInt(9, 0);
            ps.setInt(10, 1);
            ps.setInt(11, ic_contragarante);
            ps.execute();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirAltaGarantia(" + _fechaMsg + "_" + icProceso +
                               ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia =
                "insert into gti_contenido_arch" + "(ic_folio,ic_if_siag,ic_linea,ig_programa,cg_contenido)" +
                " select ?,?,ic_linea,ig_programa,cg_contenido" + " from gtitmp_contenido_arch" +
                " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setString(3, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();
            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirAltaGarantia(" + _fechaMsg + "_" + icProceso +
                               ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia = "delete gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
        } catch (Exception e) {
            ok = false;
            log.info("GarantiasBean::transmitirAltaGarantia(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::transmitirAltaGarantia(S)");
        }
        return retorno;
    }

    /**
     * Realiza el alta de recuperaciones, las cuales ya han sido
     * previamente validadas.
     * @since F056-2009.
     * @param icProceso Clave del proceso de carga
     * @param ic_if clave del IF
     * @param num_registros Numero de registros cargados
     * @param impte_total Importe total cargado
     * @param ic_usuario clave del usuario
     * @param reciboFirma Recibo obtenido al realizar la autenticaci�n de la firma digital
     *
     * @return Objeto ResultadosGar con la informaci�n de la ejecuci�n
     */
    public ResultadosGar transmitirAltaRecuperaciones(String icProceso, String ic_if, String num_registros,
                                                      String impte_total, String ic_usuario, String reciboFirma) {

        log.info("transmitirAltaRecuperaciones(E)");
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;

        String qrySentencia = "";
        boolean ok = true;
        try {
            con.conexionDB();
            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            String fecha = "";
            String fecha_valor = "";
            String hora = "";
            String anio = "";
            int ic_contragarante = 0;

            boolean usarCorreccionDeFecha =
                false; // Se asigna a falso para que no ajuste a fecha valor hasta que se solicite en FODEA
            Registros reg = this.getDatosFolio(ic_if, usarCorreccionDeFecha, "4");

            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                //fecha_valor = reg.getString("FECHA_VALOR");
                fecha_valor =
                    reg.getString("fecha"); // Se asigna la misma fecha para que no ajuste a fecha valor hasta que se solicite en FODEA
                hora = reg.getString("hora");
                anio = reg.getString("anio");
                fecha_valor = (fecha_valor.equals(fecha)) ? fecha_valor + " " + hora : fecha_valor + " 00:00";
                //trim_calif = reg.getString("trimestre");
            }


            /*
			qrySentencia =
					"SELECT s.ic_contragarante "+
					" FROM gti_servicios s "+
					" WHERE s.ic_servicio = 1";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_contragarante = rs.getInt(1);
			}
			ps.close();
*/
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            qrySentencia =
                " INSERT INTO gti_estatus_solic " + " (ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora " +
                " ,in_registros,fn_impte_total,ic_usuario_facultado,ic_situacion " +
                " ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante, cg_recibo_firma,df_registro) " +
                " VALUES(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,? " +
                " ,?,?,?,?,?,?,to_date(?,'dd/mm/yyyy HH24:MI')) ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha_valor);
            ps.setString(5, num_registros);
            ps.setDouble(6, Double.parseDouble(impte_total));
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1);
            ps.setInt(9, 0);
            ps.setInt(10, 4); //4= Alta de Recuperaciones Automaticas
            ps.setInt(11, ic_contragarante);
            ps.setString(12, reciboFirma);
            ps.setString(13, fecha + " " + hora);

            ps.executeUpdate();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.debug("transmitirAltaRecuperaciones(" + _fechaMsg + "_" + icProceso +
                      ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia =
                " INSERT INTO gti_contenido_arch " + "(ic_folio,ic_if_siag,ic_linea,ig_programa,cg_contenido,ic_if) " +
                " SELECT ?,?,ic_linea,ig_programa,cg_contenido,? " + " FROM gtitmp_contenido_arch " +
                " WHERE ic_proceso = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setInt(3, Integer.parseInt(ic_if));
            ps.setString(4, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();
            //------------------------ Inicio mensajes para debug ---------------------------
            log.debug("transmitirAltaRecuperaciones(" + _fechaMsg + "_" + icProceso +
                      ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia = " DELETE FROM gtitmp_contenido_arch " + " WHERE ic_proceso = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
            retorno.setFechaValor(fecha_valor);
        } catch (Exception e) {
            ok = false;
            throw new AppException("Error al transmitir el alta de recuperaciones", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("transmitirAltaRecuperaciones(S)");
        }
        return retorno;
    }


    public ResultadosGar transmitirCalifCartera(String icProceso, String ic_if, String num_registros,
                                                String impte_total_cub, String impte_total_exp, String anio,
                                                String trim_calif, String ic_usuario) throws NafinException {
        log.info("GarantiasBean::transmitirCalifCartera(E)");
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qrySentencia = "";
        boolean ok = true;
        try {
            con.conexionDB();
            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            //int consecutivo				= 0;
            //int ic_if_siag				= 0;
            //int ic_nafin_electronico	= 0;
            String fecha = "";
            String hora = "";
            //String folio				= "";
            int ic_contragarante = 0;

            Registros reg = this.getDatosFolio(ic_if);

            /*qrySentencia =
					"SELECT SEQ_GTI_ESTATUS_SOLIC.NEXTVAL as CONSECUTIVO"+
					" ,cn.ic_nafin_electronico"+
					" ,i.ic_if_siag"+
					" ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA"+
					" ,TO_CHAR(sysdate,'HH24:MI') as HORA"+
					" ,TO_CHAR(sysdate,'yyyymmdd') as FECFOLIO"+
					" FROM COMREL_NAFIN CN"+
					" ,COMCAT_IF I"+
					" WHERE CN.ic_epo_pyme_if = i.ic_if"+
					" and CN.cg_tipo = 'I'"+
					" and CN.ic_epo_pyme_if = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
				rs = ps.executeQuery();
				*/
            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                hora = reg.getString("hora");
            }
            /*
				qrySentencia =
					"SELECT ic_contragarante"+
					" FROM GTI_SERVICIOS S"+
					" WHERE S.IC_SERVICIO = 1";
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				if(rs.next()){
					ic_contragarante = rs.getInt(1);
				}
				ps.close();
*/
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            qrySentencia =
                "insert into gti_estatus_solic" + "(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
                " ,in_registros,fn_impte_total,ic_usuario_facultado,ic_situacion" +
                " ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante,ig_anio_trim_calif,ig_trim_calif" +
                " ,fn_reservas_cubiertas,fn_reservas_expuestas)" + " values(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,?" +
                " ,?,?,?,?,?,?,?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha + " " + hora);
            ps.setString(5, num_registros);
            ps.setDouble(6, Double.parseDouble(impte_total_cub) + Double.parseDouble(impte_total_exp));
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1);
            ps.setInt(9, 0);
            ps.setInt(10, 5);
            ps.setInt(11, ic_contragarante);
            ps.setString(12, anio);
            ps.setString(13, trim_calif);
            ps.setDouble(14, Double.parseDouble(impte_total_cub));
            ps.setDouble(15, Double.parseDouble(impte_total_exp));
            ps.execute();
            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCalifCartera(" + _fechaMsg + "_" + icProceso +
                               ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia =
                "insert into gti_contenido_arch" + "(ic_folio,ic_if_siag,ic_linea,cg_contenido)" +
                " select ?,?,ic_linea,cg_contenido" + " from gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setString(3, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCalifCartera(" + _fechaMsg + "_" + icProceso +
                               ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia = "delete gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
        } catch (Exception e) {
            ok = false;
            log.info("GarantiasBean::transmitirCalifCartera(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::transmitirCalifCartera(S)");
        }
        return retorno;
    }

    /**
		@deprecated A partir del F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n este m�todo ha quedado deprecado.
		@modified_by jshernandez
		@modified_since F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n; 20/11/2013 11:43:44 a.m.
     */
    public ResultadosGar transmitirCalifCartera(String icProceso, String ic_if, String num_registros,
                                                String montoExposicionIncumplimientoControl, String anio,
                                                String trim_calif, String ic_usuario) throws NafinException {
        log.info("GarantiasBean::transmitirCalifCartera(E)");
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qrySentencia = "";
        boolean ok = true;
        String impte_total_cub =
            "0"; // F036 - 2013. Se suprime el env�o de la Sumatoria de importes de reservas cubiertas
        String impte_total_exp =
            "0"; // F036 - 2013. Se suprime el env�o de la Sumatoria de importes de reservas expuestas
        try {
            con.conexionDB();
            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            //int consecutivo				= 0;
            //int ic_if_siag				= 0;
            //int ic_nafin_electronico	= 0;
            String fecha = "";
            String hora = "";
            //String folio				= "";
            int ic_contragarante = 0;

            Registros reg = this.getDatosFolio(ic_if);

            /*qrySentencia =
					"SELECT SEQ_GTI_ESTATUS_SOLIC.NEXTVAL as CONSECUTIVO"+
					" ,cn.ic_nafin_electronico"+
					" ,i.ic_if_siag"+
					" ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA"+
					" ,TO_CHAR(sysdate,'HH24:MI') as HORA"+
					" ,TO_CHAR(sysdate,'yyyymmdd') as FECFOLIO"+
					" FROM COMREL_NAFIN CN"+
					" ,COMCAT_IF I"+
					" WHERE CN.ic_epo_pyme_if = i.ic_if"+
					" and CN.cg_tipo = 'I'"+
					" and CN.ic_epo_pyme_if = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
				rs = ps.executeQuery();
				*/
            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                hora = reg.getString("hora");
            }
            /*
				qrySentencia =
					"SELECT ic_contragarante"+
					" FROM GTI_SERVICIOS S"+
					" WHERE S.IC_SERVICIO = 1";
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				if(rs.next()){
					ic_contragarante = rs.getInt(1);
				}
				ps.close();
*/
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            qrySentencia =
                "insert into gti_estatus_solic" + "(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
                " ,in_registros,fn_impte_total,ic_usuario_facultado,ic_situacion" +
                " ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante,ig_anio_trim_calif,ig_trim_calif" +
                " ,fn_reservas_cubiertas,fn_reservas_expuestas)" + " values(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,?" +
                " ,?,?,?,?,?,?,?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha + " " + hora);
            ps.setString(5, num_registros);
            ps.setBigDecimal(6,
                             new BigDecimal(montoExposicionIncumplimientoControl)); // F036 - 2013. Se agrega el monto de Exposici�n al Incumplimiento
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1);
            ps.setInt(9, 0);
            ps.setInt(10, 5);
            ps.setInt(11, ic_contragarante);
            ps.setString(12, anio);
            ps.setString(13, trim_calif);
            ps.setDouble(14,
                         Double.parseDouble(impte_total_cub)); // F036 - 2013. Se suprime el env�o de la Sumatoria de importes de reservas cubiertas
            ps.setDouble(15,
                         Double.parseDouble(impte_total_exp)); // F036 - 2013. Se suprime el env�o de la Sumatoria de importes de reservas expuestas
            ps.execute();
            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCalifCartera(" + _fechaMsg + "_" + icProceso +
                               ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia =
                "insert into gti_contenido_arch" + "(ic_folio,ic_if_siag,ic_linea,cg_contenido)" +
                " select ?,?,ic_linea,cg_contenido" + " from gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setString(3, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCalifCartera(" + _fechaMsg + "_" + icProceso +
                               ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia = "delete gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
        } catch (Exception e) {
            ok = false;
            log.info("GarantiasBean::transmitirCalifCartera(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::transmitirCalifCartera(S)");
        }
        return retorno;
    }


    /**
     * Transmite los datos de la carga de saldos.
     * @param icProceso Numero de proceso (para obtener datos de la tabla temporal)
     * @param ic_if Clave del IF
     * @param num_registros Numero de registros
     * @param total_saldos ???
     * @param total_pag ???
     * @param aniomes A�o y Mes de los saldos
     * @param clavesFinan Lista de claves de financiamiento
     * @paran ic_usuario Clave del usuario
     *
     * @return ???
     *
     */
    public ResultadosGar transmitirCargaSaldos(String icProceso, String ic_if, String num_registros,
                                               String total_saldos, String total_pag, String aniomes, List clavesFinan,
                                               String ic_usuario, String accion) throws NafinException {

        log.info("GarantiasBean::transmitirCargaSaldos(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        boolean ok = true;


        try {
            con.conexionDB();
            Integer ic_if_siag = null;
            BigDecimal ic_nafin_electronico = null;
            String fecha = "";
            String hora = "";
            BigDecimal folio = null;
            String anio = "";
            String trim_calif = "";
            int ic_contragarante = 0;
            String fechaCorte = "";
            java.util.Date dFechaCorte = null;
            Calendar cFechaCorte = Calendar.getInstance();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            boolean existeClave = false;

            //for(int i=0;i<clavesFinan.size()&&i<700;i++){
            for (int i = 0; i < clavesFinan.size(); i++) {
                if (sbInClavesFinan.length() > 0)
                    sbInClavesFinan.append(",");
                sbInClavesFinan.append("?");
            }

            Registros reg = this.getDatosFolio(ic_if);

            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                hora = reg.getString("hora");
                anio = reg.getString("anio");
                trim_calif = reg.getString("trimestre");
            }

            qrySentencia = " SELECT to_char(to_date(?,'yyyymm'),'dd/mm/yyyy') as FECHA_CORTE " + " FROM DUAL ";

            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, aniomes);
            rs = ps.executeQuery();
            if (rs.next()) {
                fechaCorte = rs.getString("FECHA_CORTE");
            }
            rs.close();
            ps.close();
            dFechaCorte = Comunes.parseDate(fechaCorte);
            cFechaCorte.setTime(dFechaCorte);
            cFechaCorte.set(Calendar.DAY_OF_MONTH, cFechaCorte.getActualMaximum(Calendar.DAY_OF_MONTH));
            dFechaCorte = cFechaCorte.getTime();
            fechaCorte = new SimpleDateFormat("dd/MM/yyyy").format(dFechaCorte);

            if (!"reprocesar".equals(accion) && !"".equals(accion)) {
                if (sbInClavesFinan.length() > 0) {
                    int ind1 = 0;
                    int ind2 = 0;
                    int ind3 = 0;
                    qrySentencia =
                        "select gs.ic_folio,gs.cc_garantia" + " from gti_garant_saldos gs" + " ,gti_estatus_solic es" +
                        " where gs.ic_folio = es.ic_folio" + " and gs.ic_if_siag = es.ic_if_siag" +
                        " and es.ic_situacion not in(?,?)" + " and gs.ic_if_siag = ?" + " and gs.cg_anio_mes =  ?" +
                        " and gs.cc_garantia in(" + sbInClavesFinan.toString() + ")";


                    ps = con.queryPrecompilado(qrySentencia);
                    ps.setInt(1, 5);
                    ps.setInt(2, 7);
                    ps.setInt(3, ic_if_siag.intValue());
                    ps.setString(4, aniomes);

                    for (ind1 = 0; ind2 < clavesFinan.size(); ind1++, ind2++) {
                        ind3 = ind1 + 5;
                        ps.setString(ind3, clavesFinan.get(ind2).toString());
                        log.info("EL VALOR  *** =" + ind3 + "---" + clavesFinan.get(ind2).toString());
                    }
                    rs = ps.executeQuery();

                    if (rs.next()) {
                        existeClave = true;
                    }
                    rs.close();
                }

            } //finaliza if(reproceso)


            if (existeClave) {
                throw new NafinException("DSCT0025"); //ERROR POR CONCURRENCIA
            }

            /*
			qrySentencia =
				"SELECT ic_contragarante"+
				" FROM GTI_SERVICIOS S"+
				" WHERE S.IC_SERVICIO = 1";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_contragarante = rs.getInt(1);
			}
			ps.close();
*/
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            //log.debug("qrySentencia   "+qrySentencia);
            qrySentencia =
                "insert into gti_estatus_solic" + "(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
                " ,in_registros,fn_saldo_fin_mes,fn_impte_total,ic_usuario_facultado,ic_situacion" +
                " ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante,ig_anio_carga,ig_mes_carga,df_fecha_corte)" +
                " values(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,?" + " ,?,?,?,?,?,?,?,?,to_date(?,'dd/mm/yyyy'))";
            //" ,?,?,01,00,02,?,?,?,to_date(?,'dd/mm/yyyy'))";

            //log.debug("qrySentencia   "+qrySentencia);

            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha + " " + hora);
            ps.setString(5, num_registros);
            ps.setDouble(6, Double.parseDouble(total_saldos));
            ps.setDouble(7, Double.parseDouble(total_saldos));
            ps.setString(8, ic_usuario);
            ps.setInt(9, 1);
            ps.setInt(10, 0);
            ps.setInt(11, 2);
            ps.setInt(12, ic_contragarante);
            ps.setString(13, aniomes.substring(0, 4));
            ps.setString(14, aniomes.substring(4, 6));
            ps.setString(15, fechaCorte);

            ps.execute();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCargaSaldos(" + _fechaMsg + "_" + icProceso +
                               ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia =
                " insert into gti_garant_saldos" + " (ic_folio,ic_if_siag,ic_linea,cc_garantia,cg_anio_mes)" +
                " values(?,?,?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);
            for (int i = 0; i < clavesFinan.size(); i++) {
                ps.setBigDecimal(1, folio);
                ps.setInt(2, ic_if_siag.intValue());
                ps.setInt(3, (i + 1));
                ps.setString(4, clavesFinan.get(i).toString());
                ps.setString(5, aniomes);
                ps.execute();
            }
            ps.close();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCargaSaldos(" + _fechaMsg + "_" + icProceso +
                               ")::Inserciones gti_garant_saldos...OK(" + clavesFinan.size() + ")");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia =
                "insert into gti_contenido_arch" + "(ic_folio,ic_if_siag,ic_linea,cg_contenido)" +
                " select ?,?,ic_linea,cg_contenido" + " from gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setString(3, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();


            log.info("qrySentencia_contenido_Arch>>" + qrySentencia + ">>>" + folio.toPlainString() + "--" +
                               ic_if_siag.intValue() + "--" + icProceso);
            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirCargaSaldos(" + _fechaMsg + "_" + icProceso +
                               ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------


            qrySentencia = "delete gtitmp_contenido_arch" + " where ic_proceso = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();
            ps.close();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);

        } catch (NafinException ne) {
            ok = false;
            throw ne;
        } catch (Exception e) {
            ok = false;
            log.info("GarantiasBean::transmitirCargaSaldos(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::transmitirCargaSaldos(S)");
        }
        return retorno;
    }

    /**
     * Calcula el siguiente numero de proceso.
     * Anteriormente, el valor se calculaba con una max+1, pero con concurrencia
     * podia regresar el mismo valor. Se ajusta para que use una secuencia,
     * la cual al llegar a su valor maximo volver� a comenzar, por lo cual
     * es importante que la tabla se depure adecuadamente.
     * @author Gilberto Aparicio
     * @return Numero de proceso
     * @throws com.netro.exception.NafinException
     */
    private int calculaIcProceso() {
        log.info("getIcProceso(E)");
        AccesoDB con = new AccesoDB();
        int retorno = 0;
        boolean exito = true;
        try {
            con.conexionDB();
            PreparedStatement ps = null;
            ResultSet rs = null;
            String qrySentencia = " SELECT seq_gtitmp_contenido_arch.nextval " + " FROM dual ";
            ps = con.queryPrecompilado(qrySentencia);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt(1);
            }
            rs.close();
            ps.close();
            log.debug("retorno--->" + retorno);
            return retorno;
        } catch (Exception e) {
            exito = false;
            throw new AppException("getIcProceso(Exception):: Error al calcular el numero de proceso.", e);
        } finally {
            if (con.hayConexionAbierta()) {
                //con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
            log.debug("getIcProceso(S)");
        }
    }


    private void validaFechaFechaSaldo(int numLinea, String claveGarant, String valor, String aniomes,
                                       ResultadosGar rg) {
        try {
            String fechaFinAnt = "";
            boolean errorLinea = rg.getErrorLinea();
            rg.setErrorLinea(false);
            if (valor.length() % 33 != 0) {
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ". La longitud del contenido del tercer campo es incorrecta.\n");
                rg.setErrorLinea(true);
            }
            if (valor.length() == 0) {
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ". El tercer campo esta vacio.\n");
                rg.setErrorLinea(true);
            }
            for (int i = 0; i < valor.length(); i += 33) {
                String fechaIni = valor.substring(i, i + 8);
                String fechaFin = valor.substring(i + 8, i + 16);
                String monto = valor.substring(i + 16, i + 33);
                validaFecha(numLinea, claveGarant, fechaIni, "Fecha inicial del per&iacute;odo de saldo insoluto", rg);
                validaFecha(numLinea, claveGarant, fechaFin, "Fecha final del per&iacute;odo de saldo insoluto", rg);
                validaFlotante(numLinea, claveGarant, monto,
                               "Monto del Saldo Insoluto en el per�odo " + fechaIni + " a " + fechaFin, rg);
                if (!fechaIni.substring(0, 6).equals(aniomes)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha inicial no corresponde al mes y a&ntilde;o capturados.\n");
                    rg.setErrorLinea(true);
                }
                if (!fechaFin.substring(0, 6).equals(aniomes)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha final no corresponde al mes y a&ntilde;o capturados.\n");
                    rg.setErrorLinea(true);
                }
                if (fechaIni.equals(fechaFinAnt)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha final es la misma que la fecha inicial del periodo anterior.\n");
                    rg.setErrorLinea(true);
                }

                if (rg.getErrorLinea()) {
                    break;
                }
                fechaFinAnt = fechaFin;
            } // for
            if (errorLinea) {
                rg.setErrorLinea(true);
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                             ". El contenido del tercer campo es incorrecto.\n");
            rg.setErrorLinea(true);
        }

    }

    public void validaFechaFechaSaldoRango(int numLinea, String claveGarant, String valor, String aniomes,
                                           String cancelaLiquida, ResultadosGar rg, TreeMap saldosPagosComisionesTMap) {
        try {
            String fechaFinAnt = "";
            boolean errorLinea = rg.getErrorLinea();
            rg.setErrorLinea(false);
            if (valor.length() % 33 != 0) {
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ". La longitud del contenido del tercer campo es incorrecta.\n");
                rg.setErrorLinea(true);
            }
            if (valor.length() == 0) {
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ". El tercer campo esta vacio.\n");
                rg.setErrorLinea(true);
            }
            String fechaIniRenglon = valor.substring(0, 6);
            String fechaSigRenglon = null;
            for (int i = 0; i < valor.length(); i += 33) {
                //log.info("fechaIniRenglon: "+fechaIniRenglon);
                fechaSigRenglon = valor.substring(i, i + 6);
                ;
                //log.info("fechaSigRenglon: "+fechaSigRenglon);
                if (!fechaIniRenglon.equals(fechaSigRenglon)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". El periodo de fechas no estan dentro del a�o mes " + fechaIniRenglon + ".\n");
                    rg.setErrorLinea(true);
                }


                String fechaIni = valor.substring(i, i + 8);
                String fechaFin = valor.substring(i + 8, i + 16);
                String monto = valor.substring(i + 16, i + 33);
                validaFecha(numLinea, claveGarant, fechaIni, "Fecha inicial del per&iacute;odo de saldo insoluto", rg);
                validaFecha(numLinea, claveGarant, fechaFin, "Fecha final del per&iacute;odo de saldo insoluto", rg);
                validaFlotante(numLinea, claveGarant, monto,
                               "Monto del Saldo Insoluto en el per�odo " + fechaIni + " a " + fechaFin, rg);

                Date dateFechaIni = new SimpleDateFormat("yyyyMMdd").parse(fechaIni);
                Date dateFechaFin = new SimpleDateFormat("yyyyMMdd").parse(fechaFin);
                String strFechaPrDiaMes = aniomes + "01";
                String strFechaUlDiaMes = Fecha.calculaUltimoDiaDelMes(aniomes + "01", "yyyyMMdd", 0);
                //log.info("strFechaPrDiaMes: "+strFechaPrDiaMes);
                //log.info("strFechaUlDiaMes: "+strFechaUlDiaMes);
                Date dateFechaPrDiaMes = new SimpleDateFormat("yyyyMMdd").parse(strFechaPrDiaMes);
                Date dateFechaUlrDiaMes = new SimpleDateFormat("yyyyMMdd").parse(strFechaUlDiaMes);

                //Valida Rango de Fechas
                /*if (!((dateFechaIni.after(dateFechaPrDiaMes)||dateFechaIni.equals(dateFechaPrDiaMes)) && (dateFechaIni.before(dateFechaUlrDiaMes)||dateFechaIni.equals(dateFechaUlrDiaMes)))){
                    log.info("La fecha " + dateFechaIni +" "+dateFechaPrDiaMes+" "+dateFechaUlrDiaMes+
                    " esta fuera del rango");
                }else{
                     log.info("La fecha " + dateFechaIni +
                     " esta dentro del rango");
                }
                if (!((dateFechaIni.after(dateFechaPrDiaMes)||dateFechaIni.equals(dateFechaPrDiaMes)) && (dateFechaIni.before(dateFechaUlrDiaMes)||dateFechaIni.equals(dateFechaUlrDiaMes)))) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha inicial no corresponde al mes y a&ntilde;o capturados.\n");
                    rg.setErrorLinea(true);
                }*/

                /*if (!((dateFechaFin.after(dateFechaPrDiaMes)||dateFechaFin.equals(dateFechaPrDiaMes)) && (dateFechaFin.before(dateFechaUlrDiaMes)||dateFechaFin.equals(dateFechaUlrDiaMes)))) {
                    log.info("La fecha " + dateFechaFin +" "+dateFechaPrDiaMes+" "+dateFechaUlrDiaMes+
                    " esta fuera del rango");
                }else{
                     log.info("La fecha " + dateFechaFin +
                     " esta dentro del rango");
                }*/

                if (!(dateFechaFin.before(dateFechaUlrDiaMes) || dateFechaFin.equals(dateFechaUlrDiaMes))) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha final no corresponde al mes y a&ntilde;o capturados.\n");
                    rg.setErrorLinea(true);
                }
                //Valida Rango de Fechas FIN

                if (fechaIni.equals(fechaFinAnt)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". La fecha final es la misma que la fecha inicial del periodo anterior.\n");
                    rg.setErrorLinea(true);
                }

                if (saldosPagosComisionesTMap.get(claveGarant) == null) {
                    ArrayList<SaldosPagosComisionesVo> saldosPagosComisionesVoAL =
                        new ArrayList<SaldosPagosComisionesVo>();
                    SaldosPagosComisionesVo saldosPagosComisionesVo = new SaldosPagosComisionesVo();
                    saldosPagosComisionesVo.setLinea(numLinea);
                    saldosPagosComisionesVo.setFechaInicialPeriodoSaldoInsoluto(dateFechaIni);
                    saldosPagosComisionesVo.setFechaFinalPeriodoSaldoInsoluto(dateFechaFin);
                    saldosPagosComisionesVo.setClaveFinanciamiento(claveGarant);
                    if(cancelaLiquida == null && cancelaLiquida.equals("")){
                        saldosPagosComisionesVo.setCancelacionLiquidacion(true);
                    }else{
                        saldosPagosComisionesVo.setCancelacionLiquidacion(false);
                    }
                    
                    saldosPagosComisionesVoAL.add(saldosPagosComisionesVo);
                    saldosPagosComisionesTMap.put(claveGarant, saldosPagosComisionesVoAL);
                } else {
                    ArrayList<SaldosPagosComisionesVo> saldosPagosComisionesVoAL =
                        (ArrayList<SaldosPagosComisionesVo>) saldosPagosComisionesTMap.get(claveGarant);
                    SaldosPagosComisionesVo saldosPagosComisionesVo = new SaldosPagosComisionesVo();
                    saldosPagosComisionesVo.setLinea(numLinea);
                    saldosPagosComisionesVo.setFechaInicialPeriodoSaldoInsoluto(dateFechaIni);
                    saldosPagosComisionesVo.setFechaFinalPeriodoSaldoInsoluto(dateFechaFin);
                    saldosPagosComisionesVo.setClaveFinanciamiento(claveGarant);
                    if(cancelaLiquida == null && cancelaLiquida.equals("")){
                        saldosPagosComisionesVo.setCancelacionLiquidacion(true);
                    }else{
                        saldosPagosComisionesVo.setCancelacionLiquidacion(false);
                    }

                    saldosPagosComisionesVoAL.add(saldosPagosComisionesVo);
                    saldosPagosComisionesTMap.put(claveGarant, saldosPagosComisionesVoAL);
                }


                if (rg.getErrorLinea()) {
                    break;
                }
                fechaFinAnt = fechaFin;
            } // for
            if (errorLinea) {
                rg.setErrorLinea(true);
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                             ". El contenido del tercer campo es incorrecto.\n");
            rg.setErrorLinea(true);
        }

    }


    /**
     * Valida que el campo proveniente de una linea de archivo, este entre
     * los valores especificados por el valor superior y el inferior (incluyente)
     *
     * El m�todo asume que el valor del campo a validar es un numero valido.
     *
     * Los valores Minimo y Maximo se especifican como cadenas para que al momento
     * de desplegar el mensaje de error aparezcan con el formato adecuado
     * (numero de decimales) pero dicha cadenas debe contener un numero valido
     *
     * @param numLinea Numero de linea en el archivo
     * @param campo Valor del campo a validar (El valor debe caber en un double)
     * @param nombreCampo Descripcion del campo que se valida
     * @param rg objeto que almacena los resultados del procesamiento
     * @param valorMin rango Inferior
     * 		Si es null solo se toma el limite superior
     * @param valorMax rango Superior
     * 		Si es null solo se toma el limite inferior
     *
     */

    private void validaRangoNumero(int numLinea, String campo, String nombreCampo, ResultadosGar rg, String strValorMin,
                                   String strValorMax) {

        Double valorMin = (strValorMin != null) ? new Double(strValorMin) : null;
        Double valorMax = (strValorMax != null) ? new Double(strValorMax) : null;

        double valorCampo = Double.parseDouble(campo);
        String msg = null;

        if (valorMin != null && valorMax != null) { // a < x <b
            if (valorCampo < valorMin.doubleValue() || valorCampo > valorMax.doubleValue()) {
                msg = " debe estar entre " + strValorMin + " y " + strValorMax;
            }
        } else if (valorMin != null && valorMax == null) { // a < x
            if (valorCampo < valorMin.doubleValue()) {
                msg = " debe ser mayor que " + strValorMin;
            }
        } else if (valorMin == null && valorMax != null) { // a < x
            if (valorCampo > valorMax.doubleValue()) {
                msg = " debe ser menor que " + strValorMax;
            }
        }

        if (msg !=
            null) { //si hay error:

            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo + " " + msg +
                             " \n");
            rg.setErrorLinea(true);
        }
    }


    private void validaRFC(int numLinea, String rfc, ResultadosGar rg) {
        try {
            rfc = rfc.trim();
            if (rfc.indexOf('-') > 0 || rfc.indexOf(' ') > 0) {
                rg.appendErrores("Error en la linea:" + numLinea +
                                 " El RFC del acreditado no debe tener guiones ni espacios en blanco. \n");
                rg.setErrorLinea(true);
            }

        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El RFC del acreditado es incorrecto. \n");
            rg.setErrorLinea(true);
        }
    }

    private boolean validaRFC_v2(int numLinea, String rfc, String nombreCampo, ResultadosGar rg) {
        boolean error = false;
        try {
            if (rfc.indexOf('-') > 0 || rfc.indexOf(' ') > 0) {
                rg.appendErrores("Error en la linea:" + numLinea + " El campo " + nombreCampo +
                                 " no debe tener guiones ni espacios en blanco. \n");
                rg.setErrorLinea(true);
                error = true;
            }

        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El campo " + nombreCampo + " es incorrecto. \n");
            rg.setErrorLinea(true);
            error = true;
        }
        return !error;
    }

    /**
     * Valida que un campo, obtenido de la linea de un archivo de carga,
     * 		sea flotante.
     * @param numLinea Numero de linea del archivo
     * @param campo Valor del campo a validar
     * @param nombreCampo Nombre del campo que se valida
     * @param rg Objeto que contiene los resultados del procesamiento
     *
     */
    private void validaFlotante(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        try {
            Double.parseDouble(campo.trim());
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }
    }

    /**
     *
     * Valida que un campo, obtenido de la linea de un archivo de carga,
     * sea flotante; no permite notacion cientifica.
     *
     * @param numLinea    Numero de linea del archivo
     * @param campo       Valor del campo a validar
     * @param nombreCampo Nombre del campo que se valida
     * @param rg          Objeto que contiene los resultados del procesamiento
     *
     */
    private boolean validaFlotanteV2(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean error = false;
        try {
            campo = campo.trim();
            if (!campo.matches("[\\-]?[\\d]+[\\.]?") && !campo.matches("[\\-]?[\\d]*[\\.][\\d]+")) {
                error = true;
            }
        } catch (Exception e) {
            error = true;
        }
        if (error) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }
        return !error;

    }

    /**
     *
     * Valida que un campo, obtenido de la linea de un archivo de carga,
     * sea flotante y no negativo; no permite notacion cientifica.
     *
     * @param numLinea    Numero de linea del archivo
     * @param campo       Valor del campo a validar
     * @param nombreCampo Nombre del campo que se valida
     * @param rg          Objeto que contiene los resultados del procesamiento
     *
     * @author jshernandez
     * @since 04/06/2014 01:24:41 p.m.
     */
    private boolean validaFlotantePV2(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean error = false;

        try {

            campo = campo.trim();

            if (campo.matches("[\\-][\\d]*[\\.][\\d]+") || campo.matches("[\\-][\\d]+[\\.]?")) {

                if (campo.matches("[\\-][0]*[\\.]?[0]*")) {
                    error = false; // Se deja pasar -0, como en la versi�n original
                } else {
                    error = true;
                    rg.appendErrores("Error en la linea:");
                    rg.appendErrores(String.valueOf(numLinea));
                    rg.appendErrores(" El valor del campo ");
                    rg.appendErrores(nombreCampo);
                    rg.appendErrores(" debe ser mayor o igual a 0. \n");
                    rg.setErrorLinea(true);
                }

            } else if (!campo.matches("[\\d]*[\\.][\\d]+") && !campo.matches("[\\d]+[\\.]?")) {
                error = true;
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(" El valor del campo '");
                rg.appendErrores(nombreCampo);
                rg.appendErrores("'  es incorrecto. \n");
                rg.setErrorLinea(true);
            }

        } catch (Exception e) {
            error = true;
            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo '");
            rg.appendErrores(nombreCampo);
            rg.appendErrores("'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }

        return !error;

    }

    /**
     *
     * Valida que un campo, obtenido de la linea de un archivo de carga,
     * sea flotante; no permite notacion cientifica.
     *
     * @param numLinea    Numero de linea del archivo
     * @param campo       Valor del campo a validar
     * @param nombreCampo Nombre del campo que se valida
     * @param rg          Objeto que contiene los resultados del procesamiento
     *
     */
    private boolean validaFlotantePV2(int numLinea, String claveGarant, String campo, String nombreCampo,
                                      ResultadosGar rg) {

        boolean error = false;
        try {

            campo = campo.trim();
            if (campo.matches("[\\-][0]+[\\.]?") || campo.matches("[\\-][0]*[\\.][0]+")) {
                error = false; // Se deja pasar -0, como en la versi�n original
            } else if (campo.matches("[\\-][\\d]+[\\.]?") || campo.matches("[\\-][\\d]*[\\.][\\d]+")) {
                error = true;
                rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo +
                                 " debe ser mayor o igual a 0. \n");
                rg.setErrorLinea(true);
            } else if (!campo.matches("[\\d]+[\\.]?") && !campo.matches("[\\d]*[\\.][\\d]+")) {
                error = true;
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ". El valor del campo '" + nombreCampo + "'  es incorrecto. \n");
                rg.setErrorLinea(true);
            }

        } catch (Exception e) {
            error = true;
            rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                             ". El valor del campo '" + nombreCampo + "'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }

        return !error;

    }

    private void validaFlotante(int numLinea, String claveGarant, String campo, String nombreCampo, ResultadosGar rg) {
        try {
            Double.parseDouble(campo.trim());
        } catch (Exception e) {
            rg.appendErrores("La  Clave del Financiamiento:" + claveGarant + ". El valor del campo '" + nombreCampo +
                             "'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }
    }

    //Agrego Valida Flotante Para Numeros Positivos
    private void validaFlotanteP(int numLinea, String claveGarant, String campo, String nombreCampo, ResultadosGar rg) {
        double auxCampo;
        try {
            auxCampo = Double.parseDouble(campo.trim());

            if (auxCampo < 0) {
                rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo +
                                 " debe ser mayor o igual a 0. \n");
                rg.setErrorLinea(true);
            }

        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                             ". El valor del campo '" + nombreCampo + "'  es incorrecto. \n");
            rg.setErrorLinea(true);
        }
    }


    /**
     * Valida la longitud de un campo entero o decimal proveniente de una linea de archivo
     *
     * Nota: Una precision de 5 y una escala de 2, permiten tener 3 enteros y 2 decimales
     *
     * @param numLinea Numero de linea en el archivo
     * @param campo Valor del campo a Validar
     * @param nombreCampo Descripcion del campo que se valida
     * @param rg objeto que almacena los resultados del procesamiento
     * @param precision Numero de digitos que conforman el valor
     * @param decimales Numero de digitos permitidos como decimales
     *
     */
    private void validaLongitudNumero(int numLinea, String campo, String nombreCampo, ResultadosGar rg, int precision,
                                      int decimales) {
        if (!Comunes.precisionValida(campo, precision, decimales)) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo +
                             " no cumple con la longitud requerida \n");
            rg.setErrorLinea(true);
        }
    }

    /**
     * Valida la longitud de un campo de caracteres proveniente de una linea de archivo
     *
     * @param numLinea Numero de linea en el archivo
     * @param campo Valor del campo a Validar
     * @param nombreCampo Descripcion del campo que se valida
     * @param rg objeto que almacena los resultados del procesamiento
     * @param longitudMax Longitud M�xima de la cadena
     *
     */
    private void validarLongitudCadena(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                                       int longitudMax) {
        if (campo != null && campo.length() > longitudMax) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo +
                             " no cumple con la longitud m�xima permitida \n");
            rg.setErrorLinea(true);
        }
    }


    private boolean validaEntero(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        boolean error = false;
        try {
            Long.parseLong(campo.trim());
        } catch (Exception e) {
            error = true;
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  debe ser un n�mero entero. \n");
            rg.setErrorLinea(true);
        }
        return !error;
    }

    private boolean validaSerNumero(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        boolean error = false;
        try {
            Double.parseDouble(campo.trim());
        } catch (Exception e) {
            error = true;
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  debe ser num�rico. \n");
            rg.setErrorLinea(true);
        }
        return !error;
    }

    private void validaEntero(int numLinea, String claveGarant, String campo, String nombreCampo, ResultadosGar rg) {
        try {
            Long.parseLong(campo.trim());
        } catch (Exception e) {
            rg.appendErrores(" La Clave del Financiamiento:" + claveGarant + ". El valor del campo '" + nombreCampo +
                             "'  debe ser un numero entero. \n");
            rg.setErrorLinea(true);
        }
    }


    private boolean validaIntermediario(int numLinea, String campo, String nombreCampo, int ifSiag, ResultadosGar rg) {
        boolean error = false;
        try {
            if (Integer.parseInt(campo.trim()) != ifSiag) {
                rg.appendErrores("Error en la linea:" + numLinea +
                                 " La clave del intermediario financiero es incorrecta.\n");
                rg.setErrorLinea(true);
                error = true;
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  debe ser un numero entero. \n");
            rg.setErrorLinea(true);
            error = true;
        }
        return !error;
    }

    private void validaFechasConsecutivo(ResultadosGar rg, TreeMap registros, String aniomesFin) {
        String strFechaUlrDiaMes = null;
        int numLinea = 0;
        try {
            Iterator it = registros.keySet().iterator();
            while (it.hasNext()) {
                String claveGarant = (String) it.next();
                ArrayList<SaldosPagosComisionesVo> saldosPagosComisionesVoAL =
                    (ArrayList<SaldosPagosComisionesVo>) registros.get(claveGarant);
                Iterator<SaldosPagosComisionesVo> iterator = saldosPagosComisionesVoAL.iterator();
                Calendar calFechaAnterior = null;
                Calendar calFechaOri = null;
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                boolean cancelacionLiquidacion=false;
                while (iterator.hasNext()) {
                    SaldosPagosComisionesVo saldosPagosComisionesVoTMP = iterator.next();
                    calFechaOri = Calendar.getInstance();
                    calFechaOri.setTime(saldosPagosComisionesVoTMP.getFechaInicialPeriodoSaldoInsoluto());
                    //log.info("getFechaInicialPeriodoSaldoInsoluto: "+format1.format(saldosPagosComisionesVoTMP.getFechaInicialPeriodoSaldoInsoluto().getTime()));


                    if (calFechaAnterior == null) {
                        calFechaAnterior = Calendar.getInstance();
                        calFechaAnterior.setTime((Fecha.sumarRestarDiasFecha(saldosPagosComisionesVoTMP.getFechaInicialPeriodoSaldoInsoluto(),
                                                                             Calendar.MONTH, -1)));
                    }
                    if (((calFechaOri.get(Calendar.MONTH) - calFechaAnterior.get(Calendar.MONTH)) > 1) &&
                        !((calFechaOri.get(Calendar.MONTH) - calFechaAnterior.get(Calendar.MONTH)) == 1)) {
                        rg.appendErrores("Error en la linea:" + saldosPagosComisionesVoTMP.getLinea() +
                                         ", Clave del Financiamiento:" + claveGarant +
                                         ". La fecha no es consecutiva.\n");
                        rg.setErrorLinea(true);
                    }
                    calFechaAnterior = calFechaOri;
                    numLinea = saldosPagosComisionesVoTMP.getLinea();
                    strFechaUlrDiaMes =
                        new SimpleDateFormat("yyyyMM").format(saldosPagosComisionesVoTMP.getFechaInicialPeriodoSaldoInsoluto().getTime());
                }
                if ( cancelacionLiquidacion && !aniomesFin.equals(strFechaUlrDiaMes)) {
                    rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                     ". Mes Final, no cubre el rango completo de las fechas seleccionadas.\n");
                    rg.setErrorLinea(true);
                }

            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + ". El contenido del tercer campo es incorrecto.\n");
            rg.setErrorLinea(true);
        }

    }

    private void validaIntermediario(int numLinea, String claveGarant, String campo, String nombreCampo, int ifSiag,
                                     ResultadosGar rg) {
        try {
            if (Integer.parseInt(campo.trim()) != ifSiag) {
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ".  La clave del intermediario financiero es incorrecta.\n");
                rg.setErrorLinea(true);
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "'  debe ser un numero entero. \n");
            rg.setErrorLinea(true);
        }
    }

    private void validaSN(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        try {
            if (!"S".equals(campo) && !"N".equals(campo)) {
                rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                                 "'  debe ser S o N. \n");
                rg.setErrorLinea(true);
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "' es incorrecto. \n");
            rg.setErrorLinea(true);
        }
    }

    private void validaProposito(int numLinea, String valor, String nombreCampo, ResultadosGar rg) {
        boolean error = false;
        String numeros = "0123456789";
        try {

            for (int i = 0; i < valor.length(); i += 2) {
                String tmp = valor.substring(i, i + 2);
                if (numeros.indexOf(tmp.charAt(0)) < 0 && tmp.charAt(0) != ' ') {
                    error = true;
                    break;
                }
                if (numeros.indexOf(tmp.charAt(1)) < 0) {
                    error = true;
                    break;
                }
            }

        } catch (Exception e) {
            error = true;
        }
        if (error) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "' es incorrecto. \n");
        }
    }


    private void validaClaveMonto(int numLinea, String valor, String nombreCampo, ResultadosGar rg) {
        boolean error = false;
        String numeros = "0123456789";
        try {

            for (int i = 0; i < valor.length(); i += 14) {

                String tmp = valor.substring(i, i + 2);
                String monto = valor.substring(i + 2, i + 14);

                monto = monto.replaceFirst("^[0]+", ""); // Suprimir los ceros a la izquierda que no nos interesan,

                //errores.append("clave = "+tmp);
                //errores.append("\nmonto = "+monto+"\n");
                if (numeros.indexOf(tmp.charAt(0)) < 0 && tmp.charAt(0) != ' ') {
                    error = true;
                    break;
                }
                if (numeros.indexOf(tmp.charAt(1)) < 0) {
                    error = true;
                    break;
                }

                // Validar que el monto sea un numero valido
                if (!monto.matches("[\\d]+[\\.]?") && !monto.matches("[\\d]*[\\.][\\d]+")) {
                    error = true;
                    break;
                }

                // Validar precision del monto
                String[] componentes = monto.split("\\.", -1);
                String parteEntera = componentes.length > 0 ? componentes[0] : "";
                String parteDecimal = componentes.length > 1 ? componentes[1] : "";
                if (parteDecimal.length() > 2) { // 2 decimales
                    //Error. El numero de decimales es mayor al especificado
                    error = true;
                    break;
                } else if (parteEntera.length() > 11 - 2) { // 9 enteros y no 10 porque se toma uno para el punto decimal
                    //Error. El numero de enteros es mayor al permitido
                    error = true;
                    break;
                }


            }

        } catch (Exception e) {
            error = true;
        }
        if (error) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "' es incorrecto. \n");
        }

    }


    private void validaDestino(int numLinea, String valor, ResultadosGar rg) {

        try {

            for (int i = 0; i < valor.length(); i += 11) {

                String claveTipoDestino = valor.substring(i, i + 2);
                String porcentajeParcial = valor.substring(i + 2, i + 5);
                String porcentajeNacional = valor.substring(i + 5, i + 8);
                String porcentajeImportacion = valor.substring(i + 8, i + 11);

                // Suprimir los guion a la izquierda
                claveTipoDestino = claveTipoDestino.replaceFirst("^[ ]", "");

                // Validar Clave Tipo Destino
                if (!claveTipoDestino.matches("[\\d]+")) {
                    rg.setErrorLinea(true);
                    ;
                    rg.appendErrores("Error en la linea:" + numLinea +
                                     " La clave del destino de los recursos es incorrecta \n");
                    break;
                }
                // Validar Porcentaje Parcial
                if (!porcentajeParcial.matches("[\\d]+")) {
                    rg.setErrorLinea(true);
                    ;
                    rg.appendErrores("Error en la linea:" + numLinea +
                                     " El valor del campo Destino de los recursos es incorrecto. \n");
                    break;
                }
                // Validar Porcentaje de origen nacional
                if (!porcentajeNacional.matches("[\\d]+")) {
                    rg.setErrorLinea(true);
                    ;
                    rg.appendErrores("Error en la linea:" + numLinea +
                                     " El valor del campo Destino de los recursos es incorrecto. \n");
                    break;
                }
                // Validar porcentaje de importacion
                if (!porcentajeImportacion.matches("[\\d]+")) {
                    rg.setErrorLinea(true);
                    ;
                    rg.appendErrores("Error en la linea:" + numLinea +
                                     " El valor del campo Destino de los recursos es incorrecto. \n");
                    break;
                }
                // Validar que la suma de porcentajes sea 100
                int sumatoriaPorcentajes =
                    Integer.parseInt(porcentajeNacional) + Integer.parseInt(porcentajeImportacion);
                if (sumatoriaPorcentajes != 100) {
                    log.info("El valor de la suma de Porc Nacional y de Importacion = " +
                                       sumatoriaPorcentajes);
                    rg.setErrorLinea(true);
                    ;
                    rg.appendErrores("Error en la linea:" + numLinea +
                                     " La suma de Porcentaje Nacional y Porcentaje Importaci�n debe ser 100. \n");
                    break;
                }

            }

        } catch (Exception e) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea +
                             " El valor del campo Destino de los recursos es incorrecto. \n");
        }
    }


    /**
     * Realiza la validacion de que el valor de &quot;campo&quot; sea una fecha
     * en el formato AAAAMMDD
     * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
     * @param campo Valor a validar
     * @param nombreCampo nombre del campo a validar
     * @param rg Objeto de resultado de garantias.
     */
    private boolean validaFecha(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        int dia = 0, mes = 0, ano = 0;
        boolean error = false;
        try {
            ano = Integer.parseInt(campo.substring(0, 4));
            mes = Integer.parseInt(campo.substring(4, 6)) - 1;
            dia = Integer.parseInt(campo.substring(6, 8));
            Calendar fecha = new GregorianCalendar(ano, mes, dia);
            int year = fecha.get(Calendar.YEAR);
            int month = fecha.get(Calendar.MONTH);
            int day = fecha.get(Calendar.DAY_OF_MONTH);
            //log.info(year+" "+month+" "+day);

            if (campo.length() > 8) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                                 "'  debe ser una fecha valida. \n");
                error = true;
            } else if (!Comunes.esNumero(campo)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                                 "'  debe ser una fecha valida. \n");
                error = true;
            }

            if ((dia != day) || (mes != month) || (ano != year)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                                 "'  debe ser una fecha valida. \n");
                error = true;
            }
        } catch (Exception e) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                             "'  debe ser una fecha valida  en formato AAAAMMDD. \n");
            error = true;
        }

        return !error;
    }

    private void validaFecha(int numLinea, String claveGarant, String campo, String nombreCampo, ResultadosGar rg) {
        int dia = 0, mes = 0, ano = 0;
        try {


            ano = Integer.parseInt(campo.substring(0, 4));
            mes = Integer.parseInt(campo.substring(4, 6)) - 1;
            dia = Integer.parseInt(campo.substring(6, 8));
            Calendar fecha = new GregorianCalendar(ano, mes, dia);
            int year = fecha.get(Calendar.YEAR);
            int month = fecha.get(Calendar.MONTH);
            int day = fecha.get(Calendar.DAY_OF_MONTH);

            if (campo.length() > 8) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                                 "'  debe ser una fecha valida. \n");
            } else if (!Comunes.esNumero(campo)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                                 "'  debe ser una fecha valida. \n");
            }

            if ((dia != day) || (mes != month) || (ano != year)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                                 ".  El campo '" + nombreCampo + "'  debe ser una fecha valida. \n");
            }
        } catch (Exception e) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + ", Clave del Financiamiento:" + claveGarant +
                             ". El campo '" + nombreCampo + "'  debe ser una fecha valida  en formato AAAAMMDD. \n");
        }
    }

    private boolean validaMoneda(int numLinea, String campo, ResultadosGar rg) {
        boolean error = false;
        try {
            if (!"PESOS".equalsIgnoreCase(campo) && !"DOLARES".equalsIgnoreCase(campo)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea +
                                 " El valor de la moneda debe ser PESOS o DOLARES. \n");
                error = true;
            }
        } catch (Exception e) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + " El valor de la moneda debe ser PESOS o DOLARES. \n");
            error = true;
        }
        return !error;
    }

    private void validaAnioTrim(int numLinea, String campo, String cifraCont, String nombreCampo, ResultadosGar rg) {
        try {
            campo = campo.trim();
            if (!cifraCont.equals(campo)) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:" + numLinea + " El valor del " + nombreCampo +
                                 " no corresponde a lo capturado en cifras de control. \n");
            }
        } catch (Exception e) {
            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del " + nombreCampo + " es incorrecto. \n");
        }
    }

    // Valida Campos Obligatorios SMJ 06/07/2006
    // Modified By JSHD 15/01/2013
    private boolean validaCampoObligatorio(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean valido = true;
        if (campo == null || campo.matches("\\s*")) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "' es obligatorio. \n");
            rg.setErrorLinea(true);
            valido = false;
        }
        return valido;

    }


    /**
     * Realiza la cancelaci�n de una solicitud.
     * @param ic_folio Clave del folio de la garantia a cancelar
     * @throws com.netro.exception.NafinException
     */
    public void setSolicitudCancelada(String ic_folio) throws NafinException {
        log.info("::setSolicitudCancelada(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        PreparedStatement ps = null;
        boolean bOk = true;
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                " UPDATE gti_estatus_solic" + "    SET ic_situacion = 7," + "        ic_sit_transfer = 0" +
                "  WHERE ic_folio = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setLong(1, new Long(ic_folio).longValue());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("::setSolicitudCancelada(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(bOk);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("::setSolicitudCancelada(S)");
        }
    } //setSolicitudCancelada

    public List getSolicitudesPorAutorizar(String ic_if, String ic_folio, String df_fecha_hora) throws NafinException {
        log.info("::getSolicitudesPorAutorizar(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        String condicion = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean bOk = true;
        List columnas = null;
        List renglones = new ArrayList();
        try {
            con = new AccesoDB();
            con.conexionDB();
            if (!"".equals(ic_folio))
                condicion += " AND sol.ic_folio = ? ";
            if (!"".equals(df_fecha_hora))
                condicion +=
                    " AND sol.df_fecha_hora >= to_date(?, 'dd/mm/yyyy hh24:mi') AND sol.df_fecha_hora < (to_date(?, 'dd/mm/yyyy hh24:mi')+1/(24*60)) ";
            qrySentencia =
                " SELECT sol.ic_folio, tip.cg_descripcion," +
                "        TO_CHAR (sol.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS df_fecha_hora," +
                "        con.in_aceptados_mn, con.in_aceptados_usd, con.fn_comision_mn," +
                "        con.fn_comision_usd" + "   FROM gti_estatus_solic sol," +
                " 		 (select ic_folio,ic_if_siag,sum(aceptados_mn) as in_aceptados_mn,sum(rechazados_mn) as in_rechazados_mn,sum(impte_mn) as fn_comision_mn,sum(aceptados_usd) as in_aceptados_usd,sum(rechazados_usd) as in_rechazados_usd,sum(impte_usd) as fn_comision_usd" +
                " 	 	 from (" + " 			select ic_folio" + "				,ic_if_siag" + " 		    	,in_aceptados as aceptados_mn" +
                " 		    	,in_rechazados as rechazados_mn" + " 		    	,fn_impte_autoriza as impte_mn" +
                " 		    	,0 as aceptados_usd" + " 		    	,0 as rechazados_usd" + " 		    	,0 as impte_usd" +
                " 			from gti_detalle_saldos " + " 			where ic_moneda = 1 " + " 			union all " + " 			select ic_folio" +
                "				,ic_if_siag" + " 		    	,0 as aceptados_mn" + " 		    	,0 as rechazados_mn" +
                " 		    	,0 as impte_mn" + " 		    	,in_aceptados as aceptados_usd" +
                " 		    	,in_rechazados as rechazados_usd" + " 		    	,fn_impte_autoriza as impte_usd" +
                " 			from gti_detalle_saldos" + " 			where ic_moneda = 54" + " 		 )" +
                " 		 group by ic_folio,ic_if_siag) con, " + "        comcat_if cif," +
                "        gticat_tipooperacion tip" + "  WHERE sol.ic_folio = con.ic_folio" +
                "    AND sol.ic_if_siag = con.ic_if_siag" + "    AND cif.ic_if_siag = sol.ic_if_siag" +
                "    AND sol.ic_tipo_operacion = tip.ic_tipo_operacion" +
                "	 AND con.in_aceptados_mn+con.in_aceptados_usd > 0" + "    AND sol.ic_tipo_operacion = 2" +
                "    AND sol.ic_situacion = 3" + "    AND cif.ic_if = ?" + condicion;
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.parseInt(ic_if));
            int cont = 1;
            if (!"".equals(ic_folio)) {
                cont++;
                ps.setLong(cont, new Long(ic_folio).longValue());
            }
            if (!"".equals(df_fecha_hora)) {
                cont++;
                ps.setString(cont, df_fecha_hora);
                cont++;
                ps.setString(cont, df_fecha_hora);
            }
            rs = ps.executeQuery();
            ps.clearParameters();
            while (rs.next()) {
                columnas = new ArrayList();
                String rs_folio = rs.getString("ic_folio") == null ? "" : rs.getString("ic_folio");
                String rs_tipo_operacion = rs.getString("cg_descripcion") == null ? "" : rs.getString("cg_descripcion");
                String rs_fecha = rs.getString("df_fecha_hora") == null ? "" : rs.getString("df_fecha_hora");
                String rs_aceptados_mn =
                    rs.getString("in_aceptados_mn") == null ? "0" : rs.getString("in_aceptados_mn");
                String rs_aceptados_usd =
                    rs.getString("in_aceptados_usd") == null ? "0" : rs.getString("in_aceptados_usd");
                String rs_comision_mn = rs.getString("fn_comision_mn") == null ? "0" : rs.getString("fn_comision_mn");
                String rs_comision_usd =
                    rs.getString("fn_comision_usd") == null ? "0" : rs.getString("fn_comision_usd");
                columnas.add(rs_folio);
                columnas.add(rs_tipo_operacion);
                columnas.add(rs_fecha);
                columnas.add(rs_aceptados_mn);
                columnas.add(rs_aceptados_usd);
                columnas.add(rs_comision_mn);
                columnas.add(rs_comision_usd);
                renglones.add(columnas);
            } //while(rs.next())
            rs.close();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("::getSolicitudesPorAutorizar(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("::getSolicitudesPorAutorizar(S)");
        }
        return renglones;
    } //getSolicitudesPorAutorizar


    public List getFoliosFechas(String ic_if) throws NafinException {
        log.info("::getSolicitudesPorAutorizar(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean bOk = true;
        List renglones = new ArrayList();
        Set sFolios = new TreeSet();
        Set sFechas = new TreeSet();
        List lFolios = new ArrayList();
        List lFechas = new ArrayList();
        try {
            con = new AccesoDB();
            con.conexionDB();

            qrySentencia =
                " SELECT sol.ic_folio, TO_CHAR (sol.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS df_fecha_hora" +
                "   FROM gti_estatus_solic sol," +
                " 		 (select ic_folio,ic_if_siag,sum(aceptados_mn) as in_aceptados_mn,sum(rechazados_mn) as in_rechazados_mn,sum(impte_mn) as fn_comision_mn,sum(aceptados_usd) as in_aceptados_usd,sum(rechazados_usd) as in_rechazados_usd,sum(impte_usd) as fn_comision_usd" +
                " 	 	 from (" + " 			select ic_folio" + "				,ic_if_siag" + " 		    	,in_aceptados as aceptados_mn" +
                " 		    	,in_rechazados as rechazados_mn" + " 		    	,fn_impte_autoriza as impte_mn" +
                " 		    	,0 as aceptados_usd" + " 		    	,0 as rechazados_usd" + " 		    	,0 as impte_usd" +
                " 			from gti_detalle_saldos " + " 			where ic_moneda = 1 " + " 			union all " + " 			select ic_folio" +
                "				,ic_if_siag" + " 		    	,0 as aceptados_mn" + " 		    	,0 as rechazados_mn" +
                " 		    	,0 as impte_mn" + " 		    	,in_aceptados as aceptados_usd" +
                " 		    	,in_rechazados as rechazados_usd" + " 		    	,fn_impte_autoriza as impte_usd" +
                " 			from gti_detalle_saldos" + " 			where ic_moneda = 54" + " 		 )" +
                " 		 group by ic_folio,ic_if_siag) con, " + "        comcat_if cif," +
                "        gticat_tipooperacion tip" + "  WHERE sol.ic_folio = con.ic_folio" +
                "    AND sol.ic_if_siag = con.ic_if_siag" + "    AND cif.ic_if_siag = sol.ic_if_siag" +
                "    AND sol.ic_tipo_operacion = tip.ic_tipo_operacion" +
                "	 AND con.in_aceptados_mn+con.in_aceptados_usd > 0" + "    AND sol.ic_tipo_operacion = 2" +
                "    AND sol.ic_situacion = 3" + "    AND cif.ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.parseInt(ic_if));

            rs = ps.executeQuery();
            ps.clearParameters();
            while (rs.next()) {
                sFolios.add(rs.getString("IC_FOLIO"));
                sFechas.add(rs.getString("DF_FECHA_HORA"));
            } //while(rs.next())
            lFolios.addAll(sFolios);
            lFechas.addAll(sFechas);
            renglones.add(lFolios);
            renglones.add(lFechas);

            rs.close();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("::getSolicitudesPorAutorizar(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("::getSolicitudesPorAutorizar(S)");
        }
        return renglones;
    } //getSolicitudesPorAutorizar


    /**
	 * Obtiene los datos de Clave Siag del IF y Numero de Nafin Electronico
	 * @throws com.netro.exception.NafinException
	 * @return Objeto Registros
	 */
    /*	private Registros getDatosIF(String claveIF) throws NafinException {
		log.info("GarantiasBean::getDatosIF(E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia =
				" SELECT cn.ic_nafin_electronico, "+
				" i.ic_if_siag "+
				" FROM COMREL_NAFIN CN, "+
				" COMCAT_IF I "+
				" WHERE CN.ic_epo_pyme_if = i.ic_if "+
				" and CN.cg_tipo = 'I' " +
				" and CN.ic_epo_pyme_if = ? ";
		List varBind = new ArrayList();
		varBind.add(new Integer(claveIF));
		try {
			con.conexionDB();
			Registros reg = con.consultarDB(qrySentencia, varBind);
			return reg;
		}catch(Exception e) {
			log.info("GarantiasBean::getDatosIF(). Exception: " + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("GarantiasBean::getDatosIF(S)");
		}
	}
*/

    /**
     * Obtiene el numero consecutivo del proceso de alta de garantias
     * @throws com.netro.exception.NafinException
     * @return
     */
    public String getNumeroProcesoAltaPagoGarantia() throws NafinException {
        log.info("GarantiasBean::getNumeroProcesoAltaPagoGarantia(E)");
        AccesoDB con = new AccesoDB();
        String qrySentencia = " SELECT seq_gti_proceso_solpago.nextval AS numProceso" + " FROM dual ";
        String numeroProceso = null;
        try {
            con.conexionDB();
            Registros reg = con.consultarDB(qrySentencia);
            reg.next();
            numeroProceso = reg.getString("numProceso");
        } catch (Exception e) {
            log.info("GarantiasBean::getNumeroProcesoAltaPagoGarantia(). Exception: " + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }

        log.info("GarantiasBean::getNumeroProcesoAltaPagoGarantia(S)");
        return numeroProceso;
    }


    /**
     * Obtiene el numero de folio a partir de la clave de if especificada
     * Tambien obtiene la clave siag para el IF, y el numero electronico
     * @throws com.netro.exception.NafinException
     * @return Objeto Registros con los datos del folio, N@E, SIAG, Fecha, Hora, A�o, trimestre
     */
    private Registros getDatosFolio(String claveIF) throws NafinException {
        return getDatosFolio(claveIF, false, "0");
    }

    /**
     * Obtiene el numero de folio a partir de la clave de if especificada
     * Tambien obtiene la clave siag para el IF, y el numero electronico.
     * @param usarCorreccionDeFecha 	indica si la fecha sera modificada
     *											para que se tome la del dia siguiente,
     *											en caso de que la fecha del sistema a la
     *											hora de realizar la consulta supere la
     *											hora parametrizada por tipo de operacion.
     * @throws com.netro.exception.NafinException
     * @return Objeto Registros con los datos del folio, N@E, SIAG, Fecha, Hora, A�o, trimestre
     *
     */
    private Registros getDatosFolio(String claveIF, boolean usarCorreccionDeFecha,
                                    String tipoOperacion) throws NafinException {
        log.info("GarantiasBean::getFolio(E)");
        AccesoDB con = new AccesoDB();
        Hashtable htFechas = new Hashtable();
        String qrySentencia = "";

        try {
            if (usarCorreccionDeFecha) {
                htFechas = getFechaConAdecuaciones(tipoOperacion);
                qrySentencia =
                    "SELECT cn.ic_nafin_electronico " +
                    " ,cn.ic_nafin_electronico || TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyymmdd') || ? AS FOLIO" +
                    " ,i.ic_if_siag" + " ,? as FECHA_VALOR" + " ,? as HORA" +
                    " ,TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyy') as ANIO_VALOR" + " ,case" +
                    "	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('01','02','03') then 1" +
                    "	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('04','05','06') then 2" +
                    "	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('07','08','09') then 3" +
                    "	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('10','11','12') then 4" + "  end as TRIMESTRE" +
                    " ,? as FECHA" + " ,TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyy') as ANIO" + " FROM " +
                    "	COMREL_NAFIN 	CN, " + " 	COMCAT_IF 		I   " + " WHERE " + "	CN.IC_EPO_PYME_IF = I.IC_IF 	AND " +
                    " 	CN.CG_TIPO 			= ? 			AND " +
                    "  CN.IC_EPO_PYME_IF = ?";
            } else {
                qrySentencia =
                    "SELECT cn.ic_nafin_electronico " +
                    " ,cn.ic_nafin_electronico || TO_CHAR(sysdate,'yyyymmdd') || ? AS FOLIO" + " ,i.ic_if_siag" +
                    " ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA" + " ,TO_CHAR(sysdate,'HH24:MI') as HORA" +
                    " ,TO_CHAR(sysdate,'yyyy') as ANIO" + " ,case" +
                    "	when to_char(sysdate,'mm') in('01','02','03') then 1" +
                    "	when to_char(sysdate,'mm') in('04','05','06') then 2" +
                    "	when to_char(sysdate,'mm') in('07','08','09') then 3" +
                    "	when to_char(sysdate,'mm') in('10','11','12') then 4" + "  end as TRIMESTRE" +
                    " FROM COMREL_NAFIN CN" + " ,COMCAT_IF I" + " WHERE CN.ic_epo_pyme_if = i.ic_if" +
                    " and CN.cg_tipo = ?" + " and CN.ic_epo_pyme_if = ?";
            }


            log.debug("qrySentencia--->" + qrySentencia);


            con.conexionDB();

            List varBind = new ArrayList();

            if (usarCorreccionDeFecha) {
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(this.getConsecutivo(htFechas.get("FECHA_VALOR").toString()));
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("HORA_CARGA").toString());
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("FECHA_VALOR").toString());
                varBind.add(htFechas.get("FECHA_CARGA").toString());
                varBind.add(htFechas.get("FECHA_CARGA").toString());
                varBind.add("I");
                varBind.add(new Integer(claveIF));
            } else {
                varBind.add(this.getConsecutivo());
                varBind.add("I");
                varBind.add(new Integer(claveIF));
            }

            Registros reg = con.consultarDB(qrySentencia, varBind);

            log.debug("varBind--->" + varBind);

            return reg;
        } catch (Exception e) {
            log.info("GarantiasBean::getFolio(). Exception: " + e.getMessage());
            throw new AppException("Error al obtener el folio de la solicitud", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::getFolio(S)");
        }
    }


    /** Esta funcion devuelve la fecha y hora del dia actual, si a la hora de hacer
     *  la consulta esta es menor o igual a la hora parametrizada (siag_cat_tipooperacion.hora_limite_desem), en caso contrario
     *  devolvera la fecha del dia siguiente con la hora actual.
     *	 @throws com.netro.exception.NafinException
     *	 @return Una cadena de texto con el siguiente formato de fechas de Oracle:
     *   			'DD/MM/YYYY HH24:MI'
     *  @author JSHD
     */
    public Hashtable getFechaConAdecuaciones(String sTipoOperacion) throws NafinException {
        log.info("GarantiasBean::getFechaConAdecuaciones(E)");

        String FECHA = "";
        boolean calcularDiaHabilSiguiente = false;
        Hashtable htFechas = new Hashtable();

        AccesoDB con = new AccesoDB();
        String qrySentencia =
            "SELECT 										" + "	CASE WHEN 									" +
            "		SYSDATE > to_date(to_char((select sysdate from DUAL_SIAG), 'dd/mm/yyyy ') || to_char(hora_limite_desem, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')  	" +
            "	THEN 										" + "		'true' 									" + "  ELSE											" + "		'false'									" + "	END 										" +
            "		AS	CALCULAR_DIA_HABIL_SIGUIENTE,	" + "  TO_CHAR(SYSDATE,'DD/MM/YYYY')			" + "		AS FECHA, 							" +
            "  TO_CHAR(SYSDATE,'HH24:MI')				" + "		AS HORA 							" + " FROM siag_cat_tipooperacion  				" +
            " WHERE TIPO_OPERACION = ?";

        try {
            con.conexionDB();
            List varBind = new ArrayList();
            varBind.add(sTipoOperacion);
            Registros reg = con.consultarDB(qrySentencia, varBind);

            if (reg != null && reg.next()) {
                calcularDiaHabilSiguiente =
                    (reg.getString("CALCULAR_DIA_HABIL_SIGUIENTE") != null &&
                     reg.getString("CALCULAR_DIA_HABIL_SIGUIENTE").equals("true")) ? true : false;
                FECHA = reg.getString("FECHA");

                if (calcularDiaHabilSiguiente) {
                    FECHA = Fecha.sumaFechaDiasHabilesSIAG(FECHA, "dd/MM/yyyy", 1);
                }

                //FECHA_Y_HORA = FECHA + " " + HORA;
                htFechas.put("FECHA_CARGA", reg.getString("FECHA"));
                htFechas.put("HORA_CARGA", reg.getString("HORA"));
                htFechas.put("FECHA_VALOR", FECHA);
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getFechaConAdecuaciones(Exception)");
            throw new AppException("Error al obtener la fecha valor", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::getFechaConAdecuaciones(S)");
        }
        return htFechas;
    }

    public List getSolicitudesSeleccionadas(String ic_if, String ic_folio[]) throws NafinException {
        log.info("GarantiasBean::getSolicitudesSeleccionadas(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        String condicion = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean bOk = true;
        List columnas = null;
        List renglones = new ArrayList();
        try {
            con = new AccesoDB();
            con.conexionDB();
            for (int i = 0; i < ic_folio.length; i++) {
                if (i > 0)
                    condicion += ",";
                condicion += "?";
            }
            qrySentencia =
                " SELECT sol.ic_folio, tip.cg_descripcion," +
                "        TO_CHAR (sol.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS df_fecha_hora," +
                "        con.in_aceptados_mn, con.in_aceptados_usd, con.fn_comision_mn," +
                "        con.fn_comision_usd" + "   FROM gti_estatus_solic sol," +
                " 		 (select ic_folio,ic_if_siag,sum(aceptados_mn) as in_aceptados_mn,sum(rechazados_mn) as in_rechazados_mn,sum(impte_mn) as fn_comision_mn,sum(aceptados_usd) as in_aceptados_usd,sum(rechazados_usd) as in_rechazados_usd,sum(impte_usd) as fn_comision_usd" +
                " 	 	 from (" + " 			select ic_folio" + "				,ic_if_siag" + " 		    	,in_aceptados as aceptados_mn" +
                " 		    	,in_rechazados as rechazados_mn" + " 		    	,fn_impte_autoriza as impte_mn" +
                " 		    	,0 as aceptados_usd" + " 		    	,0 as rechazados_usd" + " 		    	,0 as impte_usd" +
                " 			from gti_detalle_saldos " + " 			where ic_moneda = 1 " + " 			union all " + " 			select ic_folio" +
                "				,ic_if_siag" + " 		    	,0 as aceptados_mn" + " 		    	,0 as rechazados_mn" +
                " 		    	,0 as impte_mn" + " 		    	,in_aceptados as aceptados_usd" +
                " 		    	,in_rechazados as rechazados_usd" + " 		    	,fn_impte_autoriza as impte_usd" +
                " 			from gti_detalle_saldos" + " 			where ic_moneda = 54" + " 		 )" +
                " 		 group by ic_folio,ic_if_siag) con, " + "        comcat_if cif," +
                "        gticat_tipooperacion tip" + "  WHERE sol.ic_folio = con.ic_folio" +
                "    AND sol.ic_if_siag = con.ic_if_siag" + "    AND cif.ic_if_siag = sol.ic_if_siag" +
                "    AND sol.ic_tipo_operacion = tip.ic_tipo_operacion" + "    AND sol.ic_tipo_operacion = 2" +
                "    AND sol.ic_situacion = 3" + "    AND cif.ic_if = ?" + "    AND sol.ic_folio in (" + condicion +
                ") ";

            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.parseInt(ic_if));
            int cont = 1;
            for (int i = 0; i < ic_folio.length; i++) {
                cont++;
                ps.setLong(cont, new Long(ic_folio[i]).longValue());
            }
            rs = ps.executeQuery();
            ps.clearParameters();
            while (rs.next()) {
                columnas = new ArrayList();
                String rs_folio = rs.getString("ic_folio") == null ? "" : rs.getString("ic_folio");
                String rs_tipo_operacion = rs.getString("cg_descripcion") == null ? "" : rs.getString("cg_descripcion");
                String rs_fecha = rs.getString("df_fecha_hora") == null ? "" : rs.getString("df_fecha_hora");
                String rs_aceptados_mn =
                    rs.getString("in_aceptados_mn") == null ? "0" : rs.getString("in_aceptados_mn");
                String rs_aceptados_usd =
                    rs.getString("in_aceptados_usd") == null ? "0" : rs.getString("in_aceptados_usd");
                String rs_comision_mn = rs.getString("fn_comision_mn") == null ? "0" : rs.getString("fn_comision_mn");
                String rs_comision_usd =
                    rs.getString("fn_comision_usd") == null ? "0" : rs.getString("fn_comision_usd");
                columnas.add(rs_folio);
                columnas.add(rs_tipo_operacion);
                columnas.add(rs_fecha);
                columnas.add(rs_aceptados_mn);
                columnas.add(rs_aceptados_usd);
                columnas.add(rs_comision_mn);
                columnas.add(rs_comision_usd);
                renglones.add(columnas);
            } //while(rs.next())
            rs.close();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("GarantiasBean::getSolicitudesSeleccionadas(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            //con.terminaTransaccion(bOk);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getSolicitudesSeleccionadas(S)");
        }
        return renglones;
    }

    public List getSolicitudesConfirmadas(String ic_if, String ic_folio[]) throws NafinException {
        log.info("GarantiasBean::getSolicitudesConfirmadas(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        String condicion = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean bOk = true;
        List columnas = null;
        List renglones = new ArrayList();
        try {
            con = new AccesoDB();
            con.conexionDB();
            for (int i = 0; i < ic_folio.length; i++) {
                if (i > 0)
                    condicion += ",";
                condicion += "?";
            }
            qrySentencia =
                " SELECT sol.ic_folio, tip.cg_descripcion," +
                "        TO_CHAR (sol.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS df_fecha_hora," +
                "        con.in_aceptados_mn, con.in_aceptados_usd, con.fn_comision_mn," +
                "        con.fn_comision_usd" + "   FROM gti_estatus_solic sol," +
                " 		 (select ic_folio,ic_if_siag,sum(aceptados_mn) as in_aceptados_mn,sum(rechazados_mn) as in_rechazados_mn,sum(impte_mn) as fn_comision_mn,sum(aceptados_usd) as in_aceptados_usd,sum(rechazados_usd) as in_rechazados_usd,sum(impte_usd) as fn_comision_usd" +
                " 	 	 from (" + " 			select ic_folio" + "				,ic_if_siag" + " 		    	,in_aceptados as aceptados_mn" +
                " 		    	,in_rechazados as rechazados_mn" + " 		    	,fn_impte_autoriza as impte_mn" +
                " 		    	,0 as aceptados_usd" + " 		    	,0 as rechazados_usd" + " 		    	,0 as impte_usd" +
                " 			from gti_detalle_saldos " + " 			where ic_moneda = 1 " + " 			union all " + " 			select ic_folio" +
                "				,ic_if_siag" + " 		    	,0 as aceptados_mn" + " 		    	,0 as rechazados_mn" +
                " 		    	,0 as impte_mn" + " 		    	,in_aceptados as aceptados_usd" +
                " 		    	,in_rechazados as rechazados_usd" + " 		    	,fn_impte_autoriza as impte_usd" +
                " 			from gti_detalle_saldos" + " 			where ic_moneda = 54" + " 		 )" +
                " 		 group by ic_folio,ic_if_siag) con, " + "        comcat_if cif," +
                "        gticat_tipooperacion tip" + "  WHERE sol.ic_folio = con.ic_folio" +
                "    AND sol.ic_if_siag = con.ic_if_siag" + "    AND cif.ic_if_siag = sol.ic_if_siag" +
                "    AND sol.ic_tipo_operacion = tip.ic_tipo_operacion" + "    AND sol.ic_tipo_operacion = 2" +
                "    AND sol.ic_situacion = 4" + "    AND cif.ic_if = ?" + "    AND sol.ic_folio in (" + condicion +
                ") ";

            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.parseInt(ic_if));
            int cont = 1;
            for (int i = 0; i < ic_folio.length; i++) {
                cont++;
                ps.setLong(cont, new Long(ic_folio[i]).longValue());
            }
            rs = ps.executeQuery();
            ps.clearParameters();
            while (rs.next()) {
                columnas = new ArrayList();
                String rs_folio = rs.getString("ic_folio") == null ? "" : rs.getString("ic_folio");
                String rs_tipo_operacion = rs.getString("cg_descripcion") == null ? "" : rs.getString("cg_descripcion");
                String rs_fecha = rs.getString("df_fecha_hora") == null ? "" : rs.getString("df_fecha_hora");
                String rs_aceptados_mn =
                    rs.getString("in_aceptados_mn") == null ? "0" : rs.getString("in_aceptados_mn");
                String rs_aceptados_usd =
                    rs.getString("in_aceptados_usd") == null ? "0" : rs.getString("in_aceptados_usd");
                String rs_comision_mn = rs.getString("fn_comision_mn") == null ? "0" : rs.getString("fn_comision_mn");
                String rs_comision_usd =
                    rs.getString("fn_comision_usd") == null ? "0" : rs.getString("fn_comision_usd");
                columnas.add(rs_folio);
                columnas.add(rs_tipo_operacion);
                columnas.add(rs_fecha);
                columnas.add(rs_aceptados_mn);
                columnas.add(rs_aceptados_usd);
                columnas.add(rs_comision_mn);
                columnas.add(rs_comision_usd);
                renglones.add(columnas);
            } //while(rs.next())
            rs.close();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("GarantiasBean::getSolicitudesConfirmadas(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            //con.terminaTransaccion(bOk);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getSolicitudesConfirmadas(S)");
        }
        return renglones;
    }

    public void setConfirmarSolicitudes(String fn_autorizacion_cargo, String ic_usuario_autoriza, String ic_if,
                                        String ic_folio[]) throws NafinException {
        log.info("GarantiasBean::setConfirmarSolicitudes(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        String condicion = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean bOk = true;
        try {
            con = new AccesoDB();
            con.conexionDB();

            qrySentencia = " SELECT ic_if_siag" + "   FROM comcat_if" + "  WHERE ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.parseInt(ic_if));
            rs = ps.executeQuery();
            ps.clearParameters();
            String ic_if_siag = "";
            if (rs.next())
                ic_if_siag = rs.getString(1) == null ? "" : rs.getString(1);
            rs.close();
            ps.close();


            for (int i = 0; i < ic_folio.length; i++) {
                if (i > 0)
                    condicion += ",";
                condicion += "?";
            }
            qrySentencia =
                " UPDATE gti_estatus_solic" + "    SET ic_situacion = 4," + "        ic_sit_transfer = 0," +
                "        fn_autorizacion_cargo = ?," + "        ic_usuario_autoriza = ?" + "  WHERE ic_if_siag = ? " +
                "    AND ic_folio IN (" + condicion + ")";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, fn_autorizacion_cargo);
            ps.setString(2, ic_usuario_autoriza);
            ps.setInt(3, Integer.parseInt(ic_if_siag));
            int cont = 3;
            for (int i = 0; i < ic_folio.length; i++) {
                cont++;
                ps.setLong(cont, new Long(ic_folio[i]).longValue());
            }
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            bOk = false;
            log.info("GarantiasBean::setConfirmarSolicitudes(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(bOk);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::setConfirmarSolicitudes(S)");
        }
    }

    public List getTrimestresEnRango(String anio) throws NafinException {
        log.info("GarantiasBean::getTrimestresEnRango(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lRetorno = new ArrayList();
        Set setAnios = new TreeSet();
        Set setTrims = new TreeSet();
        List lTrim = new ArrayList();
        List lAnios = new ArrayList();
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                "   select fecha, to_char(fecha,'yyyy') as ic_anio_cali,ic_trim_cali from" + "    gti_parametro p" +
                "    ,(" + "       select 1 as ic_trim_cali,to_date('31/03','dd/mm') as fecha from dual" +
                "       union all" + "       select 2 as ic_trim_cali,to_date('30/06','dd/mm') as fecha from dual" +
                "       union all" +
                "       select 3 as ic_trim_cali,case when to_date('30/09','dd/mm')>sysdate then to_date('30/09'||to_char(sysdate-365,'yyyy'),'dd/mm/yyyy') else to_date('30/09','dd/mm') end as fecha from dual" +
                "       union all" +
                "       select 4 as ic_trim_cali,case when to_date('31/12','dd/mm')>sysdate then to_date('31/12'||to_char(sysdate-365,'yyyy'),'dd/mm/yyyy') else to_date('31/12','dd/mm') end as fecha from dual" +
                "   ) v" + "   where p.ic_parametro = 1" + "   and fecha + p.ig_dias_plazo >= trunc(sysdate)" +
                "   and (fecha < sysdate )";
            ps = con.queryPrecompilado(qrySentencia);
            rs = ps.executeQuery();
            while (rs.next()) {
                setAnios.add(rs.getString("ic_anio_cali"));
                if (anio != null && anio.equals(rs.getString("ic_anio_cali"))) {
                    setTrims.add(rs.getString("ic_trim_cali"));
                }
            }
            ps.close();

            lAnios.addAll(setAnios);
            lTrim.addAll(setTrims);
            lRetorno.add(lAnios);
            lRetorno.add(lTrim);
            if (anio != null && !anio.equals("") && lTrim.size() == 0) {
                throw new NafinException("GARA0001");
            }

        } catch (NafinException ne) {
            throw ne;
        } catch (Exception e) {
            log.info("GarantiasBean::getTrimestresEnRango(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getTrimestresEnRango(S)");
        }
        return lRetorno;
    }

    /**
     * Almacena la solicitud de pago de garantia capturada en el sistema.
     *
     * @param pagoGarantia Bean con los datos del pago de garantia
     * @return Bean con los datos del Acuse del Pago de Garant�a
     * @throws com.netro.exception.NafinException
     *
     */
    public AcusePagoGarantia guardarPagoGarantiasIF(PagoGarantiasIF pagoGarantia) throws NafinException {
        log.info("guardarPagoGarantiasIF(E)");
        //**********************************Validaci�n de parametros:*****************************
        try {
            if (pagoGarantia == null) {
                throw new Exception("Los parametros son requeridos");
            }
        } catch (Exception e) {
            log.info("guardarPagoGarantiasIF()::Error en los parametros establecidos. " + e.getMessage() +
                               " pagoGarantia=" + pagoGarantia);
            throw new NafinException("SIST0001");
        }
        //****************************************************************************************

        AcusePagoGarantia acuse = new AcusePagoGarantia();

        boolean exito = true;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();


            /*
			 * Validacion de seguridad
			 */
            Seguridad s = new Seguridad();
            char getReceipt = 'Y';
            String receipt = "";
            String folioAutencicacion =
                "03PG" + pagoGarantia.getClaveIF() + new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
            boolean autenticarMensaje =
                s.autenticar(folioAutencicacion, pagoGarantia.getSerialCertificado(), pagoGarantia.getPkcs7(),
                             pagoGarantia.getTextoFirmado(), getReceipt);

            if (autenticarMensaje) {
                receipt = s.getAcuse();
            } else {
                log.info(s.mostrarError());
                throw new NafinException("GRAL0021");
            }

            boolean usarCorreccionDeFecha = true;
            Registros reg = this.getDatosFolio(pagoGarantia.getClaveIF(), usarCorreccionDeFecha, "3");
            reg.next();

            BigDecimal folio = new BigDecimal(reg.getString("folio"));
            BigDecimal ne = new BigDecimal(reg.getString("ic_nafin_electronico"));
            log.debug("------------------");
            if (reg.getString("ic_if_siag") == null || reg.getString("ic_if_siag").equals("")) {
                throw new AppException("  El Numero de clave SIAG no se encuentra parametrizado en Nafin Electronico");
            }
            Integer ifSiag = new Integer(reg.getString("ic_if_siag"));
            String fecha = reg.getString("fecha");
            String fecha_valor = reg.getString("FECHA_VALOR");
            String hora = reg.getString("hora");

            acuse.setFolioSolicitud(reg.getString("folio"));
            acuse.setClaveGarantia(pagoGarantia.getClaveGarantia());
            acuse.setFechaValor(fecha_valor);
            acuse.setFechaCarga(fecha);
            acuse.setHoraCarga(hora);

            fecha_valor = (fecha_valor.equals(fecha)) ? fecha_valor + " " + hora : fecha_valor + " 00:00";

            String qrySentencia =
                "INSERT INTO gti_estatus_solic" + "(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
                " ,fn_impte_total, ic_contragarante " + " ,in_registros, ic_usuario_facultado,ic_situacion" +
                " ,ic_sit_transfer,ic_tipo_operacion, df_registro)" +
                " values(?,?,?,to_date(?,'dd/mm/yyyy hh24:mi'), " + " ?,?, ?,?,?, ?,?,to_date(?,'dd/mm/yyyy hh24:mi'))";

            List varBindSolic = new ArrayList();
            varBindSolic.add(folio);
            varBindSolic.add(ifSiag);
            varBindSolic.add(ne);
            varBindSolic.add(fecha_valor);

            //fn_impte_total Se almacena 0 porque no se tiene el valor y es requerido
            varBindSolic.add(new Integer(0));
            //ic_contragarante no viene en las especificaciones. Pero es requerido en
            //la tabla, por lo cual en este campo se carga ?????.
            //varBindSolic.add(new Integer(this.getClaveContragarante()));
            varBindSolic.add(new Integer(this.getClaveContragarante(pagoGarantia.getClaveIF()))); //FODEA 002 - 2011 ACF

            varBindSolic.add(new Integer(1)); //Num Registros = 1
            varBindSolic.add(pagoGarantia.getClaveUsuario());
            varBindSolic.add(new Integer(1)); //Situacion 1 :: Enviado para validacion y carga
            varBindSolic.add(new Integer(0)); //Sit. Transf. 0 :: Pendiente de enviar
            varBindSolic.add(new Integer(3)); //Tipo de operacion 3 :: Pago de Garantia
            varBindSolic.add(fecha + " " + hora);

            con.ejecutaUpdateDB(qrySentencia, varBindSolic);
            /*
			String strSQL =
					"INSERT INTO gti_solicitud_pago(" +
					" ic_if_siag, ic_folio, cc_garantia, ic_nafin_electronico, df_fecha_hora, " +
					" df_incumplimiento, " +
					" cg_causas_incumplimiento, cg_funcionario_if, cg_tel_funcionario, " +
					" cg_ext_funcionario,cg_mail_funcionario,cg_localizacion_supervision, "+
					" cg_ciudad_supervision,cg_estado_supervision) " +
					" VALUES (?,?,?,?,TO_DATE(?,'dd/mm/yyyy HH24:MI'), TO_DATE(?,'dd/mm/yyyy'), " +
					" ?,?,?, ?,?,?, ?,? )";
*/
            //FODEA 017 - 2009 ACF (I)
            String periodicidadPago = pagoGarantia.getPeriodicidadPago();
            String icPeriodicidadPago = "";
            String flagPeriodicidadPago = "";
            if (periodicidadPago != null && !periodicidadPago.equals("")) {
                if (periodicidadPago.indexOf("-") != -1) {
                    StringTokenizer tokenizer = new StringTokenizer(periodicidadPago, "-");
                    icPeriodicidadPago = tokenizer.nextToken();
                    flagPeriodicidadPago = tokenizer.nextToken();
                }
            }
            String strSQL =
                "INSERT INTO gti_solicitud_pago(" +
                " ic_if_siag, ic_folio, cc_garantia, ic_nafin_electronico, df_fecha_hora, " + " df_incumplimiento, " +
                " cg_causas_incumplimiento, cg_funcionario_if, cg_tel_funcionario, " +
                " cg_ext_funcionario,cg_mail_funcionario,cg_localizacion_supervision, " +
                " cg_ciudad_supervision,cg_estado_supervision, ic_periodicidad_pago";
            if (!flagPeriodicidadPago.equals("N")) {
                strSQL += ",df_fecha_segundo_incum";
            }
            strSQL += ")";
            strSQL +=
                " VALUES (?,?,?,?,TO_DATE(?,'dd/mm/yyyy HH24:MI'), TO_DATE(?,'dd/mm/yyyy'), " + " ?,?,?, ?,?,?, ?,?, ?";
            if (!flagPeriodicidadPago.equals("N")) {
                strSQL += ",TO_DATE(?,'dd/mm/yyyy')";
            }
            strSQL += ")";
            //	private String proceso;
            //FODEA 017 - 2009 ACF (F)
            List varBind = new ArrayList();
            varBind.add(ifSiag);
            varBind.add(folio);
            varBind.add(pagoGarantia.getClaveGarantia());
            varBind.add(ne);
            //varBind.add(fecha+" "+hora);
            varBind.add(fecha_valor);


            varBind.add(pagoGarantia.getFechaPrimerInclumplimiento()); //FODEA 017 - 2009 ACF

            varBind.add(pagoGarantia.getCausasIncumplimiento());
            varBind.add(pagoGarantia.getNombreFuncionarioIF());
            varBind.add(pagoGarantia.getTelefonoFuncionario());

            varBind.add(pagoGarantia.getExtensionFuncionario());
            varBind.add(pagoGarantia.getMailFuncionario());
            varBind.add(pagoGarantia.getLocalizacionSupervision());

            varBind.add(pagoGarantia.getCiudadSupervision());
            varBind.add(pagoGarantia.getEstadoSupervision());

            varBind.add(icPeriodicidadPago); //FODEA 017 - 2009 ACF

            //FODEA 017 - 2009 ACF (I)
            if (!flagPeriodicidadPago.equals("N")) {
                varBind.add(pagoGarantia.getFechaSegundoInclumplimiento());
            }
            //FODEA 017 - 2009 ACF (F)


            con.ejecutaUpdateDB(strSQL, varBind);


            strSQL =
                " INSERT INTO gti_imagen_solpago(" + " ic_imagen_solpago, ic_if_siag, ic_folio, " +
                " cc_garantia, ic_tipo_imagen, " + " bi_imagen, cg_content_type, cg_extension) " +
                " SELECT seq_gti_imagen_solpago.nextval, ?, ?, " +
                " ?, ic_tipo_imagen, bi_imagen, cg_content_type, cg_extension " + " FROM gtitmp_imagen_solpago " +
                " WHERE ic_proceso = ? ";
            List varBindImg = new ArrayList();
            varBindImg.add(ifSiag);
            varBindImg.add(folio);
            varBindImg.add(pagoGarantia.getClaveGarantia());
            varBindImg.add(pagoGarantia.getProceso());

            con.ejecutaUpdateDB(strSQL, varBindImg);

            return acuse;
        } catch (NafinException ne) {
            exito = false;
            ne.printStackTrace();
            throw ne;
        } catch (Exception e) {
            exito = false;
            e.printStackTrace();
            throw new AppException("Error al guardar la solicitud de pago de garantias", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
            log.info("guardarPagoGarantiasIF(S)");
        }

    }


    /**
     * Obtiene el siguiente valor consecutivo para formar el folio de la solicitud
     * La funcionalidad, ser� EQUIVALENTE a la usada mediante secuencias de Oracle.
     * Es decir, una vez que se regrese el valor por medio de este metodo en la
     * tabla ser� actualizado el valor, con la finalidad de que no sea
     * reusado el numero
     *
     * Esta implementaci�n maneja los consecutivos por d�a, es decir cada
     * d�a empezara con el numero de folio 1
     *
     * @return Cadena con el numero de consecutivo del d�a.
     */
    private String getConsecutivo() throws Exception {
        return getConsecutivo("");

    }

    private String getConsecutivo(String fecha) throws Exception {
        AccesoDB con = new AccesoDB();
        boolean exito = false;

        try {
            con.conexionDB();
            String strSQL = " SELECT MAX(ig_consecutivo) + 1 as consecutivo " + " FROM gti_consecutivo_folio ";
            if ("".equals(fecha)) {
                strSQL += " WHERE dc_consecutivo = trunc(sysdate) ";
            } else {
                strSQL += " WHERE dc_consecutivo = to_date('" + fecha + "', 'dd/mm/yyyy') ";
            }

            String consecutivo = "";
            Registros reg = con.consultarDB(strSQL);
            if (reg.next()) {
                consecutivo = reg.getString("consecutivo");
            }
            if (consecutivo.equals("")) {
                String strSQLInsert = " INSERT INTO gti_consecutivo_folio (dc_consecutivo, ig_consecutivo) ";
                if ("".equals(fecha)) {
                    strSQLInsert += " VALUES (trunc(sysdate),?) ";
                } else {
                    strSQLInsert += " VALUES (to_date('" + fecha + "', 'dd/mm/yyyy'),?) ";
                }
                consecutivo = "1";
                List varBind = new ArrayList();
                varBind.add(new BigDecimal(consecutivo));
                con.ejecutaUpdateDB(strSQLInsert, varBind);
            } else {
                String strSQLUpdate = " UPDATE gti_consecutivo_folio " + " SET ig_consecutivo = ? ";
                if ("".equals(fecha)) {
                    strSQLUpdate += " WHERE dc_consecutivo = trunc(sysdate) ";
                } else {
                    strSQLUpdate += " WHERE dc_consecutivo = to_date('" + fecha + "', 'dd/mm/yyyy') ";
                }
                List varBind = new ArrayList();
                varBind.add(new BigDecimal(consecutivo));
                con.ejecutaUpdateDB(strSQLUpdate, varBind);
            }
            exito = true;
            return consecutivo;

        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }

    }

    public ArrayList getPortafolioACalificar(String claveIF) throws NafinException {
        log.info("GarantiasBean::getPortafolioACalificar(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();
        try {
            con.conexionDB();
            qrySentencia =
                "SELECT   " + "    I.ANOTRIM                                                    ANIO,   " +
                "    I.TRIMESTRE                                                  TRIMESTRE,  " +
                "    I.VIGENTES                                                	TOTAL_GARANTIAS,  " +
                "    I.RECIBIDAS                                                	NUMERO_GARANTIAS_RECIBIDAS,  " +
                "    I.NO_RECIBIDAS                                             	NUMERO_GARANTIAS_NO_RECIBIDAS,  " +
                "    DECODE(I.SITUACION_RECEPCION_IF,01,'Abierta',02,'Cerrada')   SITUACION_RECEPCION,  " +
                "    DECODE(I.RECEPCION_PORTAFOLIO,01,'false',02,'true')          PORTAFOLIO_EXTRAIDO,   " +
                "	  TO_CHAR(I.FECHA_CORTE,'dd/mm/yyyy')			           			FECHA_CORTE  " + "FROM   " +
                "    SIAG_IFIN I  " + "WHERE  " + "    I.SITUACION_RECEPCION_IF     = ?                AND  " +
                "    I.INDICADOR_TIPOCALI         = ? 	              AND  " + "    I.CLAVE = (   " +
                "                   SELECT   " + "                       IC_IF_SIAG  " + "                   FROM   " +
                "                       COMCAT_IF   " + "                   WHERE   " +
                "                       ROWNUM  = ?    AND   " + "                       IC_IF   = ?   " +
                "              ) ";

            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();

                registro.put("A�O", registros.getString("ANIO"));
                registro.put("TRIMESTRE", registros.getString("TRIMESTRE"));
                registro.put("TOTAL_GARANTIAS",
                             registros.getString("TOTAL_GARANTIAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("TOTAL_GARANTIAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_RECIBIDAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_NO_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS"), 0, true));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
                registro.put("PORTAFOLIO_EXTRAIDO", registros.getString("PORTAFOLIO_EXTRAIDO"));
                registro.put("FECHA_CORTE", registros.getString("FECHA_CORTE"));


                // Verificar que hayan detalles del resumen de la informacion mostrada
                String anio = registros.getString("ANIO");
                String trimestre = registros.getString("TRIMESTRE");
                String fechaCorte = registros.getString("FECHA_CORTE");
                String mostrarExtraccionDetallada = "false";

                if (!anio.equals("") && !trimestre.equals("") && !fechaCorte.equals("")) {

                    qrySentencia =
                        "SELECT                                " +
                        "   DECODE(COUNT(1),0,'false','true') MOSTRAR_EXTRACCION_DETALLADA " + "FROM   " +
                        "   SIAG_CALI_IF C  " + "WHERE  " + "   C.ANOTRIM      = ?         AND  " +
                        "   C.TRIMESTRE    = ?         AND  " + "   C.FECHA_CORTE  >= TO_DATE(?,'dd/mm/yyyy')   AND " +
                        "   C.FECHA_CORTE  <  TO_DATE(?,'dd/mm/yyyy')+? AND " + "   C.CLAVE = (   " +
                        "                SELECT   " + "                    IC_IF_SIAG  " + "                FROM   " +
                        "                    COMCAT_IF   " + "                WHERE   " +
                        "                    ROWNUM  = ?    AND   " + "                    IC_IF   = ?   " +
                        "             ) ";


                    lVarBind.clear();
                    lVarBind.add(anio);
                    lVarBind.add(trimestre);
                    lVarBind.add(fechaCorte);
                    lVarBind.add(fechaCorte);
                    lVarBind.add(new Integer(1));
                    lVarBind.add(new Integer(1));
                    lVarBind.add(new Integer(claveIF));

                    Registros _registros = con.consultarDB(qrySentencia, lVarBind, false);
                    con.terminaTransaccion(true);

                    if (_registros.next()) {
                        mostrarExtraccionDetallada = _registros.getString("MOSTRAR_EXTRACCION_DETALLADA");
                    }

                }

                registro.put("MOSTRAR_EXTRACCION_DETALLADA", mostrarExtraccionDetallada);
                lista.add(registro);
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getPortafolioACalificar(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getPortafolioACalificar(S)");
        }
        return lista;
    }


    public HashMap getPortafolioACalificarPorRegistro(String anio, String trimestre, String fecha_corte,
                                                      String claveIF) throws NafinException {

        log.info("GarantiasBean::getPortafolioACalificarPorRegistro(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        HashMap registro = null;

        try {
            con.conexionDB();
            qrySentencia =
                "  SELECT  " + "    I.ANOTRIM                                                        ANIO,  " +
                "    I.TRIMESTRE                                                      TRIMESTRE, " +
                "		TO_CHAR(I.FECHA_CORTE,'dd/mm/yyyy')										  FECHA_CORTE, " +
                "    I.VIGENTES                                                		  TOTAL_GARANTIAS, " +
                "    I.RECIBIDAS                                                		  NUMERO_GARANTIAS_RECIBIDAS, " +
                "    I.NO_RECIBIDAS                                             		  NUMERO_GARANTIAS_NO_RECIBIDAS, " +
                "		TO_CHAR(I.FECHA_CIERRE,'dd/mm/yyyy')									  FECHA_CIERRE, " +
                "    DECODE(I.ACUSE,01,'En proceso',02,'Acuse de recibo')       		  ACUSE, " +
                "    DECODE(I.RECEPCION_PORTAFOLIO,01,'No extraido',02,'Extraido')    SITUACION_RECEPCION_PORTAFOLIO, " +
                "    DECODE(I.SITUACION_RECEPCION_IF,01,'Abierta',02,'Cerrada')       SITUACION_RECEPCION " +
                "  FROM  " + "    SIAG_IFIN I  " + "  WHERE " + "		I.INDICADOR_TIPOCALI		 = ? 		AND " +
                "    I.ANOTRIM                = ?     AND " + "    I.TRIMESTRE              = ?     AND " +
                "    I.FECHA_CORTE            >= 		TO_DATE(?,'dd/mm/yyyy')   AND " +
                "    I.FECHA_CORTE            < 		TO_DATE(?,'dd/mm/yyyy')+1  AND " + "    I.CLAVE = (  " +
                "                 SELECT  " + "                   IC_IF_SIAG " + "                 FROM  " +
                "                   COMCAT_IF  " + "                 WHERE  " +
                "                   ROWNUM  = ?    AND  " + "                   IC_IF   = ?  " + "              ) ";
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(anio));
            lVarBind.add(new Integer(trimestre));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                registro = new HashMap();
                registro.put("A�O", registros.getString("ANIO"));
                registro.put("TRIMESTRE", registros.getString("TRIMESTRE"));
                registro.put("FECHA_CORTE", registros.getString("FECHA_CORTE"));
                registro.put("TOTAL_GARANTIAS",
                             registros.getString("TOTAL_GARANTIAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("TOTAL_GARANTIAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_RECIBIDAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_NO_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS"), 0, true));
                registro.put("FECHA_CIERRE", registros.getString("FECHA_CIERRE"));
                registro.put("ACUSE", registros.getString("ACUSE"));
                registro.put("SITUACION_RECEPCION_PORTAFOLIO", registros.getString("SITUACION_RECEPCION_PORTAFOLIO"));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
            }

        } catch (Exception e) {
            log.debug("GarantiasBean::getPortafolioACalificarPorRegistro(Exception) " + e);
            e.printStackTrace();

            log.debug("qrySentencia:" + qrySentencia); // Debug info
            log.debug("anio:" + anio); // Debug info
            log.debug("trimestre:" + trimestre); // Debug info
            log.debug("fecha_corte:" + fecha_corte); // Debug info
            log.debug("claveIF:" + claveIF); // Debug info

            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getPortafolioACalificarPorRegistro(S)");
        }
        return registro;
    }

    public void actualizaStatusRecepcionPortafolioCalificacion(String clave, String anioTrim, String trimestre,
                                                               String fechaCorte, String status) throws NafinException {

        log.info("GarantiasBean::actualizaStatusRecepcionPortafolioCalificacion(E)");

        AccesoDB con = new AccesoDB();
        CallableStatement cs = null;
        boolean lbOk = true;


        try {
            con.conexionDB();
            // Extraer clave de ic_if_siag
            Registros registros = null;
            String clave_ic_if_siag = null;
            String query =
                "SELECT                " + "  IC_IF_SIAG          " + "FROM                  " +
                "	COMCAT_IF           " + "WHERE                 " + "	ROWNUM  = ?    AND  " +
                "	IC_IF   = ?         ";

            ArrayList lVarBind = new ArrayList();
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(clave));

            registros = con.consultarDB(query, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                clave_ic_if_siag = registros.getString("IC_IF_SIAG");
            } else {
                throw new Exception("No se encontro el ic_if_siag para el ic_if: " + clave);
            }

            // Actualizar Estatus
            cs = con.ejecutaSP("SP_ESTATUS_RECEP_PORT_CAL(?,?,?,TO_DATE(?,'dd/mm/yyyy'),?)");
            cs.setInt(1, Integer.parseInt(clave_ic_if_siag));
            cs.setInt(2, Integer.parseInt(anioTrim));
            cs.setInt(3, Integer.parseInt(trimestre));
            cs.setString(4, fechaCorte);
            cs.setInt(5, Integer.parseInt(status));

            cs.execute();
            con.terminaTransaccion(true);

        } catch (Exception e) {
            log.debug("GarantiasBean::actualizaStatusRecepcionPortafolioCalificacion(Exception)");
            e.printStackTrace();
            lbOk = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOk);
            if (cs != null)
                try {
                    cs.close();
                } catch (Exception e) {
                }
            ;
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::actualizaStatusRecepcionPortafolioCalificacion(S)");
        }

    }

    public ArrayList getDetallePortafolioACalificarPorRegistro(String anio, String trimestre, String fecha_corte,
                                                               String claveIF) throws NafinException {

        log.info("GarantiasBean::getDetallePortafolioACalificarPorRegistro(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        HashMap registro = null;
        ArrayList lista = new ArrayList();
        ResultSet rs = null;
        PreparedStatement ps = null;


        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "	ANOTRIM                                           						ANIO, " +
                "	TRIMESTRE                                         						TRIMESTRE, " +
                "	TO_CHAR(FECHA_CORTE,'dd/mm/yyyy')                 						FECHA_CORTE, " +
                "	NUMCREDITO                                        						NUM_CREDITO, " +
                "	RFC                                               						RFC, " +
                //"	CALIFDEUDOR_IF                                    						CALIFICACION_DEUDOR_IF, "  +


                //"	CALIFCUB_IF                                       						CALIFICACION_CUBIERTA_IF, "  +
                //"	CALIFEXP_IF                                       						CALIFICACION_EXPUESTA_IF, "  +
                //"	PORCUB_IF                                         						PORC_RESERVA_CUBIERTO_IF, "  +
                //"	POREXP_IF                                         						PORC_RESERVA_EXPUESTO_IF, "  +
                //"	IMPRESCUB_IF                                      						RESERVA_CUBIERTA_IF, "  +
                //"	IMPRESEXP_IF                                      						RESERVA_EXPUESTA_IF, "  +
                "	DECODE(SITUACION_RECEPCION_IF,01,'Recibida',02,'No recibida')   	SITUACION_RECEPCION_REGISTRO, " +
                "	to_char(FECHA_RECEPCION,'dd/mm/yyyy hh24:mi:ss')  						FECHA_RECEPCION_INFORMACION, " +
                "	TIPO_CARTERA																		TIPO_CARTERA, " +
                "	PROBABILIDAD_INCUMPLIMIENTO													PROB_INCUMP, " +
                "	SEVERIDAD_PERDIDA																	SEV_PERDIDA, " +
                "	EXPOSICION_INCUMP																	EXPOS_INCUMP, " + "	PRCTJE_RES																			PORC_RESERV, " +
                "	NIVEL_RIESGO 																		NIVEL_RIESGO " + "FROM     " + "    SIAG_CALI_IF " + "WHERE " +
                "    ANOTRIM        = ? AND " + "    TRIMESTRE      = ? AND " +
                "    FECHA_CORTE    >= TO_DATE(?,'dd/mm/yyyy') AND " +
                "    FECHA_CORTE    <  TO_DATE(?,'dd/mm/yyyy')+? AND " + "    CLAVE          = (  " +
                "                        SELECT   " + "                          IC_IF_SIAG  " +
                "                        FROM   " + "                          COMCAT_IF   " +
                "                        WHERE   " + "                          ROWNUM  = ?    AND   " +
                "                          IC_IF   = ?   " + "                     ) ";

            ps = con.queryPrecompilado(qrySentencia);

            ps.setInt(1, Integer.parseInt(anio));
            ps.setInt(2, Integer.parseInt(trimestre));
            ps.setString(3, fecha_corte);
            ps.setString(4, fecha_corte);
            ps.setInt(5, 1);
            ps.setInt(6, 1);
            ps.setInt(7, Integer.parseInt(claveIF));

            rs = ps.executeQuery();
            con.terminaTransaccion(true);

            while (rs != null && rs.next()) {
                registro = new HashMap();

                String year = rs.getString("ANIO") == null ? "" : rs.getString("ANIO");
                String trimester = rs.getString("TRIMESTRE") == null ? "" : rs.getString("TRIMESTRE");
                String fechaCorte = rs.getString("FECHA_CORTE") == null ? "" : rs.getString("FECHA_CORTE");
                String numCredito = rs.getString("NUM_CREDITO") == null ? "" : rs.getString("NUM_CREDITO");
                String rfc = rs.getString("RFC") == null ? "" : rs.getString("RFC");
                //String	calificacionDeudorIF 		= rs.getString("CALIFICACION_DEUDOR_IF") 			== null?"": rs.getString("CALIFICACION_DEUDOR_IF");
                //String	calificacionCubiertaIF 		= rs.getString("CALIFICACION_CUBIERTA_IF") 		== null?"": rs.getString("CALIFICACION_CUBIERTA_IF");
                //String	calificacionExpuestaIF 		= rs.getString("CALIFICACION_EXPUESTA_IF") 		== null?"": rs.getString("CALIFICACION_EXPUESTA_IF");
                //String	porcReservaCubiertoIF 		= rs.getString("PORC_RESERVA_CUBIERTO_IF") 		== null?"": rs.getString("PORC_RESERVA_CUBIERTO_IF");
                //String   porcReservaExpuestoIF 		= rs.getString("PORC_RESERVA_EXPUESTO_IF") 		== null?"": rs.getString("PORC_RESERVA_EXPUESTO_IF");
                //String	reservaCubiertaIF 			= rs.getString("RESERVA_CUBIERTA_IF") 				== null?"": rs.getString("RESERVA_CUBIERTA_IF");
                //String	reservaExpuestaIF 			= rs.getString("RESERVA_EXPUESTA_IF") 				== null?"": rs.getString("RESERVA_EXPUESTA_IF");
                String situacionRecepcionRegistro =
                    rs.getString("SITUACION_RECEPCION_REGISTRO") == null ? "" :
                    rs.getString("SITUACION_RECEPCION_REGISTRO");
                String fechaRecepcionInformacion =
                    rs.getString("FECHA_RECEPCION_INFORMACION") == null ? "" :
                    rs.getString("FECHA_RECEPCION_INFORMACION");

                registro.put("A�O", year);
                registro.put("TRIMESTRE", trimester);
                registro.put("FECHA_CORTE", fechaCorte);
                registro.put("NUM_CREDITO", numCredito);
                registro.put("RFC", rfc);
                //registro.put("CALIFICACION_DEUDOR_IF",			 calificacionDeudorIF);
                //registro.put("CALIFICACION_CUBIERTA_IF",		 calificacionCubiertaIF);
                //registro.put("CALIFICACION_EXPUESTA_IF",		 calificacionExpuestaIF);
                //registro.put("PORC_RESERVA_CUBIERTO_IF",		 Comunes.formatoDecimal(porcReservaCubiertoIF.equals("")?"0.00":porcReservaCubiertoIF,2,true));
                //registro.put("PORC_RESERVA_EXPUESTO_IF",		 Comunes.formatoDecimal(porcReservaExpuestoIF.equals("")?"0.00":porcReservaExpuestoIF,2,true));
                //registro.put("RESERVA_CUBIERTA_IF",				 Comunes.formatoDecimal(reservaCubiertaIF.equals("")?"0.00":reservaCubiertaIF,2,true));
                //registro.put("RESERVA_EXPUESTA_IF",				 Comunes.formatoDecimal(reservaExpuestaIF.equals("")?"0.00":reservaExpuestaIF,2,true));
                registro.put("TIPO_CARTERA",
                             (rs.getString("TIPO_CARTERA") == null ? " " : rs.getString("TIPO_CARTERA")));
                registro.put("PROB_INCUMP", (rs.getString("PROB_INCUMP") == null ? " " : rs.getString("PROB_INCUMP")));
                registro.put("SEV_PERDIDA", (rs.getString("SEV_PERDIDA") == null ? " " : rs.getString("SEV_PERDIDA")));
                registro.put("EXPOS_INCUMP",
                             (rs.getString("EXPOS_INCUMP") == null ? " " : rs.getString("EXPOS_INCUMP")));
                registro.put("PORC_RESERV", (rs.getString("PORC_RESERV") == null ? " " : rs.getString("PORC_RESERV")));
                registro.put("NIVEL_RIESGO",
                             (rs.getString("NIVEL_RIESGO") == null ? " " : rs.getString("NIVEL_RIESGO")));
                registro.put("SITUACION_RECEPCION_REGISTRO", situacionRecepcionRegistro);
                registro.put("FECHA_RECEPCION_INFORMACION", fechaRecepcionInformacion);

                lista.add(registro);
            }
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();

        } catch (Exception e) {
            log.debug("GarantiasBean::getDetallePortafolioACalificarPorRegistro(Exception)");
            e.printStackTrace();

            log.debug("qrySentencia: " + qrySentencia); // Debug info
            log.debug("anio:         " + anio); // Debug info
            log.debug("trimestre:    " + trimestre); // Debug info
            log.debug("fecha_corte:  " + fecha_corte); // Debug info
            log.debug("claveIF:      " + claveIF); // Debug info

            throw new NafinException("SIST0001");
        } finally {

            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.debug("GarantiasBean::getDetallePortafolioACalificarPorRegistro(S)");
        }

        return lista;
    }

    public ArrayList getResultadosCierreRecepcionCalificaciones(String claveIF) throws NafinException {

        log.info("GarantiasBean::getResultadosCierreRecepcionCalificaciones(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();
        try {
            con.conexionDB();
            qrySentencia =
                "SELECT " + "    I.ANOTRIM                                                     ANIO, " +
                "    I.TRIMESTRE                                                   TRIMESTRE, " +
                "    I.VIGENTES                                                	 TOTAL_GARANTIAS, " +
                "    I.RECIBIDAS                                                	 NUMERO_GARANTIAS_RECIBIDAS, " +
                "    I.NO_RECIBIDAS                                             	 NUMERO_GARANTIAS_NO_RECIBIDAS, " +
                "    DECODE(I.SITUACION_RECEPCION_IF,01,'Abierta',02,'Cerrada')    SITUACION_RECEPCION, " +
                "    DECODE(I.ACUSE,01,'false',02,'true')              			 	 HAY_ACUSE_DE_RECIBO, " +
                "	  TO_CHAR(I.FECHA_CORTE,'dd/mm/yyyy')			                	 FECHA_CORTE  " + "FROM  " +
                "    SIAG_IFIN I " + "WHERE " + "    I.SITUACION_RECEPCION_IF = ? AND " + "    I.CLAVE = (   " +
                "                 SELECT " + "                   IC_IF_SIAG " + "                 FROM  " +
                "                   COMCAT_IF " + "                 WHERE  " +
                "                   ROWNUM  = ?    AND " + "                   IC_IF   = ? " + "              ) ";
            lVarBind.add(new Integer(2));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();

                registro.put("A�O", registros.getString("ANIO"));
                registro.put("TRIMESTRE", registros.getString("TRIMESTRE"));
                registro.put("TOTAL_GARANTIAS",
                             registros.getString("TOTAL_GARANTIAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("TOTAL_GARANTIAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_RECIBIDAS"), 0, true));
                registro.put("NUMERO_GARANTIAS_NO_RECIBIDAS",
                             registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS").equals("") ? "0" :
                             Comunes.formatoDecimal(registros.getString("NUMERO_GARANTIAS_NO_RECIBIDAS"), 0, true));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
                registro.put("HAY_ACUSE_DE_RECIBO", registros.getString("HAY_ACUSE_DE_RECIBO"));
                registro.put("FECHA_CORTE", registros.getString("FECHA_CORTE"));

                // Verificar que hayan detalles del resumen de la informacion mostrada
                String anio = registros.getString("ANIO");
                String trimestre = registros.getString("TRIMESTRE");
                String fechaCorte = registros.getString("FECHA_CORTE");
                String mostrarExtraccionDetallada = "false";

                if (!anio.equals("") && !trimestre.equals("") && !fechaCorte.equals("")) {

                    qrySentencia =
                        "SELECT " + "	DECODE(COUNT(1),0,'false','true')     MOSTRAR_EXTRACCION_DETALLADA " +
                        "FROM " + "	SIAG_INCUMPLIMIENTOS " + "WHERE " + "	CLAVE  = ( " + "               SELECT " +
                        "                   IC_IF_SIAG " + "               FROM " + "                   COMCAT_IF " +
                        "               WHERE " + "         	       ROWNUM  = ?  AND " + // 1
                        "                   IC_IF  = ? " + // ic_if
                        "            )";
                    lVarBind.clear();
                    lVarBind.add(new Integer(1));
                    lVarBind.add(new Integer(claveIF));

                    Registros _registros = con.consultarDB(qrySentencia, lVarBind, false);
                    con.terminaTransaccion(true);

                    if (_registros.next()) {
                        mostrarExtraccionDetallada = _registros.getString("MOSTRAR_EXTRACCION_DETALLADA");
                    }
                }

                registro.put("MOSTRAR_EXTRACCION_DETALLADA", mostrarExtraccionDetallada);
                lista.add(registro);
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getResultadosCierreRecepcionCalificaciones(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getResultadosCierreRecepcionCalificaciones(S)");
        }
        return lista;
    }

    public void actualizaStatusAcuseResultadoCalificacion(String clave, String anioTrim, String trimestre,
                                                          String fechaCorte, String status) throws NafinException {

        log.info("GarantiasBean::actualizaStatusAcuseResultadoCalificacion(E)");

        AccesoDB con = new AccesoDB();
        CallableStatement cs = null;
        boolean lbOk = true;

        try {
            con.conexionDB();

            // Extraer clave de ic_if_siag
            Registros registros = null;
            String clave_ic_if_siag = null;
            String query =
                "SELECT                " + "  IC_IF_SIAG          " + "FROM                  " +
                "	COMCAT_IF           " + "WHERE                 " + "	ROWNUM  = ?    AND  " +
                "	IC_IF   = ?         ";

            ArrayList lVarBind = new ArrayList();
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(clave));

            registros = con.consultarDB(query, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                clave_ic_if_siag = registros.getString("IC_IF_SIAG");
            } else {
                throw new Exception("No se encontro el ic_if_siag para el ic_if: " + clave);
            }

            cs = con.ejecutaSP("SP_ESTATUS_ACUSE_RESULT_CAL(?,?,?,TO_DATE(?,'dd/mm/yyyy'),?)");
            cs.setInt(1, Integer.parseInt(clave_ic_if_siag));
            cs.setInt(2, Integer.parseInt(anioTrim));
            cs.setInt(3, Integer.parseInt(trimestre));
            cs.setString(4, fechaCorte);
            cs.setInt(5, Integer.parseInt(status));

            cs.execute();
            //con.terminaTransaccion(true); SE REQUIERE ???

        } catch (Exception e) {
            log.info("GarantiasBean::actualizaStatusAcuseResultadoCalificacion(Exception)");
            e.printStackTrace();
            lbOk = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOk);
            if (cs != null)
                try {
                    cs.close();
                } catch (Exception e) {
                }
            ;
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::actualizaStatusAcuseResultadoCalificacion(S)");
        }

    }


    public HashMap getDatosCierreRecepcionCalificacionesPorRegistro(String anio, String trimestre, String claveIF,
                                                                    String fecha_corte) throws NafinException {

        log.info("GarantiasBean::getDatosCierreRecepcionCalificacionesPorRegistro(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        HashMap registro = null;

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "    D.CG_RAZON_SOCIAL                                           INTERMEDIARIO,  " +
                "    I.ANOTRIM                                                   ANIO, " +
                "    I.TRIMESTRE                                                 TRIMESTRE, " +
                "    I.VIGENTES                                           			TOTAL_GARANTIAS, " +
                "    I.RECIBIDAS                                           		NUM_GARANTIAS_RECIBIDAS, " +
                "    I.NO_RECIBIDAS                                        		NUM_GARANTIAS_NO_RECIBIDAS, " +
                "    DECODE(I.SITUACION_RECEPCION_IF,01,'Abierta',02,'Cerrada')  SITUACION_RECEPCION " + "FROM  " +
                "    SIAG_IFIN I, COMCAT_IF D  " + "WHERE " +
                "    I.SITUACION_RECEPCION_IF = 	?                          AND " +
                "    I.ANOTRIM                = 	?                          AND " +
                "    I.TRIMESTRE              =  	?                          AND " +
                "    I.CLAVE   					 =    D.IC_IF_SIAG               AND " +
                "    I.FECHA_CORTE            >=   TO_DATE(?,'dd/mm/yyyy')    AND " +
                "    I.FECHA_CORTE            <    TO_DATE(?,'dd/mm/yyyy')+?  AND " +
                "    D.IC_IF                  =    ?                              ";

            lVarBind.add(new Integer(2));
            lVarBind.add(new Integer(anio));
            lVarBind.add(new Integer(trimestre));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                registro = new HashMap();
                registro.put("INTERMEDIARIO", registros.getString("INTERMEDIARIO"));
                registro.put("A�O", registros.getString("ANIO"));
                registro.put("TRIMESTRE", registros.getString("TRIMESTRE"));
                registro.put("TOTAL_GARANTIAS", registros.getString("TOTAL_GARANTIAS"));
                registro.put("NUM_GARANTIAS_RECIBIDAS", registros.getString("NUM_GARANTIAS_RECIBIDAS"));
                registro.put("NUM_GARANTIAS_NO_RECIBIDAS", registros.getString("NUM_GARANTIAS_NO_RECIBIDAS"));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getDatosCierreRecepcionCalificacionesPorRegistro(Exception) " + e);
            e.printStackTrace();

            log.info("qrySentencia:" + qrySentencia); // Debug info
            log.info("anio:" + anio); // Debug info
            log.info("trimestre:" + trimestre); // Debug info
            log.info("claveIF:" + claveIF); // Debug info

            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getDatosCierreRecepcionCalificacionesPorRegistro(S)");
        }
        return registro;
    }

    /*
	public TipoOperacion getTipoOperacion (int claveTipoOperacion) {
		log.info("getTipoOperacion(E)");
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String query =
					" SELECT "
		}catch (Exception e) {

		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getTipoOperacion(S)");
		}
	}
*/

    public ArrayList getCalificacionesRecibidas(String anio, String trimestre, String claveIF,
                                                String fecha_corte) throws NafinException {

        log.info("GarantiasBean::getCalificacionesRecibidas(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "    NUMCREDITO                                                       NUMERO_CREDITO, " +
                "    RFC                                                              RFC, " +
                //"    CALIFDEUDOR_IF                                                   CALIFICACION_DEUDOR, "  +


                //"    CALIFCUB_IF                                                      CALIFICACION_CUBIERTA, "  +
                //"    CALIFEXP_IF                                                      CALIFICACION_EXPUESTA, "  +
                //"    PORCUB_IF                                                        PORC_CALIFICACION_CUBIERTA, "  +
                //"    POREXP_IF                                                        PORC_CALIFICACION_EXPUESTA, "  +
                //"    IMPRESCUB_IF                                                     IMPORTE_RESERVA_CUBIERTA, "  +
                //"    IMPRESEXP_IF                                                     IMPORTE_RESERVA_EXPUESTA, "  +
                "    DECODE(SITUACION_RECEPCION_IF,01,'Recibida',02,'No recibida')    SITUACION_RECEPCION, " +
                "	   TIPO_CARTERA																		TIPO_CARTERA, " +
                "	   PROBABILIDAD_INCUMPLIMIENTO													PROB_INCUMP, " +
                "	   SEVERIDAD_PERDIDA																	SEV_PERDIDA, " +
                "	   EXPOSICION_INCUMP																	EXPOS_INCUMP, " +
                "	   PRCTJE_RES																			PORC_RESERV, " + "	   NIVEL_RIESGO 																		NIVEL_RIESGO " +
                "FROM " + "    SIAG_CALI_IF " + "WHERE     " +
                "  SITUACION_RECEPCION_IF =   ?                          AND " +
                "  ANOTRIM                =   ?                          AND " +
                "  TRIMESTRE              =   ?                          AND " +
                "  FECHA_CORTE            >=  TO_DATE(?,'dd/mm/yyyy')    AND " +
                "  FECHA_CORTE            <   TO_DATE(?,'dd/mm/yyyy')+?  AND " + "  CLAVE                  = (   " +
                "                             SELECT   " + "                                  IC_IF_SIAG  " +
                "                             FROM   " + "                                  COMCAT_IF   " +
                "                             WHERE   " + "                                  ROWNUM  = ?    AND   " +
                "                                 IC_IF    = ?          " + "                           )";
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(anio));
            lVarBind.add(new Integer(trimestre));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();
                registro.put("NUMERO_CREDITO", registros.getString("NUMERO_CREDITO"));
                registro.put("RFC", registros.getString("RFC"));
                //registro.put("CALIFICACION_DEUDOR", 			registros.getString("CALIFICACION_DEUDOR"));
                //registro.put("CALIFICACION_CUBIERTA", 			registros.getString("CALIFICACION_CUBIERTA"));
                //registro.put("CALIFICACION_EXPUESTA", 			registros.getString("CALIFICACION_EXPUESTA"));
                //registro.put("PORC_CALIFICACION_CUBIERTA", 	Comunes.formatoDecimal(registros.getString("PORC_CALIFICACION_CUBIERTA").equals("")	?"0.00":registros.getString("PORC_CALIFICACION_CUBIERTA"),	2,true));
                //registro.put("PORC_CALIFICACION_EXPUESTA", 	Comunes.formatoDecimal(registros.getString("PORC_CALIFICACION_EXPUESTA").equals("")	?"0.00":registros.getString("PORC_CALIFICACION_EXPUESTA"),	2,true));
                //registro.put("IMPORTE_RESERVA_CUBIERTA", 		Comunes.formatoDecimal(registros.getString("IMPORTE_RESERVA_CUBIERTA").equals("")	?"0.00":registros.getString("IMPORTE_RESERVA_CUBIERTA"),		2,true));
                //registro.put("IMPORTE_RESERVA_EXPUESTA", 		Comunes.formatoDecimal(registros.getString("IMPORTE_RESERVA_EXPUESTA").equals("")	?"0.00":registros.getString("IMPORTE_RESERVA_EXPUESTA"),		2,true));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
                registro.put("TIPO_CARTERA",
                             (registros.getString("TIPO_CARTERA") == null ? "" : registros.getString("TIPO_CARTERA")));
                registro.put("PROB_INCUMP",
                             (registros.getString("PROB_INCUMP") == null ? "" : registros.getString("PROB_INCUMP")));
                registro.put("SEV_PERDIDA",
                             (registros.getString("SEV_PERDIDA") == null ? "" : registros.getString("SEV_PERDIDA")));
                registro.put("EXPOS_INCUMP",
                             (registros.getString("EXPOS_INCUMP") == null ? "" : registros.getString("EXPOS_INCUMP")));
                registro.put("PORC_RESERV",
                             (registros.getString("PORC_RESERV") == null ? "" : registros.getString("PORC_RESERV")));
                registro.put("NIVEL_RIESGO",
                             (registros.getString("NIVEL_RIESGO") == null ? "" : registros.getString("NIVEL_RIESGO")));

                lista.add(registro);
            }


        } catch (Exception e) {
            log.debug("GarantiasBean::getCalificacionesRecibidas(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.debug("GarantiasBean::getCalificacionesRecibidas(S)");
        }
        return lista;
    }

    public ArrayList getCalificacionesNoRecibidas(String anio, String trimestre, String claveIF,
                                                  String fecha_corte) throws NafinException {

        log.info("GarantiasBean::getCalificacionesNoRecibidas(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "    NUMCREDITO                                                       NUMERO_CREDITO, " +
                "    RFC                                                              RFC, " +
                //"    CALIFDEUDOR_IF                                                   CALIFICACION_DEUDOR, "  +


                //"    CALIFCUB_IF                                                      CALIFICACION_CUBIERTA, "  +
                //"    CALIFEXP_IF                                                      CALIFICACION_EXPUESTA, "  +
                //"    PORCUB_IF                                                        PORC_CALIFICACION_CUBIERTA, "  +
                //"    POREXP_IF                                                        PORC_CALIFICACION_EXPUESTA, "  +
                //"    IMPRESCUB_IF                                                     IMPORTE_RESERVA_CUBIERTA, "  +
                //"    IMPRESEXP_IF                                                     IMPORTE_RESERVA_EXPUESTA, "  +
                "    DECODE(SITUACION_RECEPCION_IF,01,'Recibida',02,'No recibida')    SITUACION_RECEPCION, " +
                "	   TIPO_CARTERA																		TIPO_CARTERA, " +
                "	   PROBABILIDAD_INCUMPLIMIENTO													PROB_INCUMP, " +
                "	   SEVERIDAD_PERDIDA																	SEV_PERDIDA, " +
                "	   EXPOSICION_INCUMP																	EXPOS_INCUMP, " +
                "	   PRCTJE_RES																			PORC_RESERV, " + "	   NIVEL_RIESGO 																		NIVEL_RIESGO " +
                "FROM " + "    SIAG_CALI_IF " + "WHERE     " + "  SITUACION_RECEPCION_IF = 	?      							 AND " +
                "  ANOTRIM                = 	?     							 AND " + "  TRIMESTRE              = 	?      							 AND " +
                "  FECHA_CORTE            >=  TO_DATE(?,'dd/mm/yyyy')    AND " +
                "  FECHA_CORTE            <   TO_DATE(?,'dd/mm/yyyy')+?  AND " + "  CLAVE                  = (   " +
                "                             SELECT   " + "                                  IC_IF_SIAG  " +
                "                             FROM   " + "                                  COMCAT_IF   " +
                "                             WHERE   " + "                                  ROWNUM  = ?    AND   " +
                "                                  IC_IF   = ?   " + "                           )";
            lVarBind.add(new Integer(2));
            lVarBind.add(new Integer(anio));
            lVarBind.add(new Integer(trimestre));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();
                registro.put("NUMERO_CREDITO", registros.getString("NUMERO_CREDITO"));
                registro.put("RFC", registros.getString("RFC"));
                //registro.put("CALIFICACION_DEUDOR", 			registros.getString("CALIFICACION_DEUDOR"));
                //registro.put("CALIFICACION_CUBIERTA", 			registros.getString("CALIFICACION_CUBIERTA"));
                //registro.put("CALIFICACION_EXPUESTA", 			registros.getString("CALIFICACION_EXPUESTA"));
                //registro.put("PORC_CALIFICACION_CUBIERTA", 	Comunes.formatoDecimal(registros.getString("PORC_CALIFICACION_CUBIERTA").equals("")	?"0.00":registros.getString("PORC_CALIFICACION_CUBIERTA"),	2,true));
                //registro.put("PORC_CALIFICACION_EXPUESTA", 	Comunes.formatoDecimal(registros.getString("PORC_CALIFICACION_EXPUESTA").equals("")	?"0.00":registros.getString("PORC_CALIFICACION_EXPUESTA"),	2,true));
                //registro.put("IMPORTE_RESERVA_CUBIERTA", 		Comunes.formatoDecimal(registros.getString("IMPORTE_RESERVA_CUBIERTA").equals("")	?"0.00":registros.getString("IMPORTE_RESERVA_CUBIERTA"),		2,true));
                //registro.put("IMPORTE_RESERVA_EXPUESTA", 		Comunes.formatoDecimal(registros.getString("IMPORTE_RESERVA_EXPUESTA").equals("")	?"0.00":registros.getString("IMPORTE_RESERVA_EXPUESTA"),		2,true));
                registro.put("SITUACION_RECEPCION", registros.getString("SITUACION_RECEPCION"));
                registro.put("TIPO_CARTERA",
                             (registros.getString("TIPO_CARTERA") == null ? "" : registros.getString("TIPO_CARTERA")));
                registro.put("PROB_INCUMP",
                             (registros.getString("PROB_INCUMP") == null ? "" : registros.getString("PROB_INCUMP")));
                registro.put("SEV_PERDIDA",
                             (registros.getString("SEV_PERDIDA") == null ? "" : registros.getString("SEV_PERDIDA")));
                registro.put("EXPOS_INCUMP",
                             (registros.getString("EXPOS_INCUMP") == null ? "" : registros.getString("EXPOS_INCUMP")));
                registro.put("PORC_RESERV",
                             (registros.getString("PORC_RESERV") == null ? "" : registros.getString("PORC_RESERV")));
                registro.put("NIVEL_RIESGO",
                             (registros.getString("NIVEL_RIESGO") == null ? "" : registros.getString("NIVEL_RIESGO")));

                lista.add(registro);
            }

        } catch (Exception e) {
            log.debug("GarantiasBean::getCalificacionesNoRecibidas(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getCalificacionesNoRecibidas(S)");
        }
        return lista;
    }

    public HashMap getDescripcionIntermediario(String claveIF) throws NafinException {

        log.info("GarantiasBean::getDescripcionIntermediario(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        HashMap registro = null;

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "    D.CG_RAZON_SOCIAL   INTERMEDIARIO  " + "FROM  " + "    COMCAT_IF D  " + "WHERE " +
                "    D.IC_IF   = ?  ";

            lVarBind.add(new Integer(claveIF));


            registros = con.consultarDB(qrySentencia, lVarBind, false);

            if (registros != null && registros.next()) {
                registro = new HashMap();
                registro.put("INTERMEDIARIO", registros.getString("INTERMEDIARIO"));
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getDescripcionIntermediario(Exception) " + e);
            e.printStackTrace();

            log.info("qrySentencia:" + qrySentencia); // Debug info
            log.info("claveIF:" + claveIF); // Debug info

            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getDescripcionIntermediario(S)");
        }
        return registro;
    }

    public ArrayList getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(String claveIF) throws NafinException {

        log.info("GarantiasBean::getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "   NUMCREDITO                                                  NUM_GARANTIA, " +
                "   ANOTRIM_INCUMPLIDO || '/' || TRIMESTRE_INCUMPLIDO           ANIO_TRIMESTRE_PRMR_INC, " +
                "   TO_CHAR(FECHA_PRIMER_INCUMPLIMIENTO,'dd/mm/yyyy')           FECHA_PRIMER_INCUMPLIMIENTO, " +
                "   TO_CHAR(FECHA_ULTIMO_INCUMPLIMIENTO,'dd/mm/yyyy')           FECHA_ULTIMO_INCUMPLIMIENTO, " +
                "   PERIODOS_CSC                                                PERIODO_CONSEC_INCUMPLIMIENTO, " +
                "   TO_CHAR(FECHA_BAJA_GTIA,'dd/mm/yyyy')                       FECHA_SOLICITUD_BAJA, " +
                "   DECODE(SITUACION_REGISTRO,01,'Por Regularizar',02,'Baja en SIAG' )   SITUACION_INCUMPLIMIENTO " +
                "FROM  " + "   SIAG_INCUMPLIMIENTOS " + "WHERE  " + "   SITUACION_REGISTRO  = ?    AND " +
                "   CLAVE               = (  " + "                            SELECT  " +
                "                               IC_IF_SIAG " + "                            FROM  " +
                "                               COMCAT_IF  " + "                            WHERE  " +
                "                               ROWNUM  = ?    AND  " + "                               IC_IF   = ? " +
                "                         )";
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();
                registro.put("NUM_GARANTIA", registros.getString("NUM_GARANTIA"));
                registro.put("ANIO_TRIMESTRE_PRMR_INC", registros.getString("ANIO_TRIMESTRE_PRMR_INC"));
                registro.put("FECHA_PRIMER_INCUMPLIMIENTO", registros.getString("FECHA_PRIMER_INCUMPLIMIENTO"));
                registro.put("FECHA_ULTIMO_INCUMPLIMIENTO", registros.getString("FECHA_ULTIMO_INCUMPLIMIENTO"));
                registro.put("PERIODO_CONSEC_INCUMPLIMIENTO", registros.getString("PERIODO_CONSEC_INCUMPLIMIENTO"));
                registro.put("FECHA_SOLICITUD_BAJA", registros.getString("FECHA_SOLICITUD_BAJA"));
                registro.put("SITUACION_INCUMPLIMIENTO", registros.getString("SITUACION_INCUMPLIMIENTO"));

                lista.add(registro);
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(Exception) " +
                               e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(S)");
        }
        return lista;
    }

    public ArrayList getGarantiasConSituacionDeBajaDelPortafolioGarantizado(String claveIF) throws NafinException {

        log.info("GarantiasBean::getGarantiasConSituacionDeBajaDelPortafolioGarantizado(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList lista = new ArrayList();

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT  " + "   NUMCREDITO                                                  NUM_GARANTIA, " +
                "   ANOTRIM_INCUMPLIDO || '/' || TRIMESTRE_INCUMPLIDO           ANIO_TRIMESTRE_PRMR_INC, " +
                "   TO_CHAR(FECHA_PRIMER_INCUMPLIMIENTO,'dd/mm/yyyy')           FECHA_PRIMER_INCUMPLIMIENTO, " +
                "   TO_CHAR(FECHA_ULTIMO_INCUMPLIMIENTO,'dd/mm/yyyy')           FECHA_ULTIMO_INCUMPLIMIENTO, " +
                "   PERIODOS_CSC                                                PERIODO_CONSEC_INCUMPLIMIENTO, " +
                "   TO_CHAR(FECHA_BAJA_GTIA,'dd/mm/yyyy')                       FECHA_SOLICITUD_BAJA, " +
                "   DECODE(SITUACION_REGISTRO,01,'Activo',02,'Baja en SIAG' )   SITUACION_INCUMPLIMIENTO " + "FROM  " +
                "   SIAG_INCUMPLIMIENTOS " + "WHERE  " + "   SITUACION_REGISTRO  = ?    AND " +
                "   CLAVE               = (  " + "                            SELECT  " +
                "                               IC_IF_SIAG " + "                            FROM  " +
                "                               COMCAT_IF  " + "                            WHERE  " +
                "                               ROWNUM  = ?    AND  " + "                               IC_IF   = ? " +
                "                         ) ";
            lVarBind.add(new Integer(2));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            while (registros != null && registros.next()) {
                HashMap registro = new HashMap();
                registro.put("NUM_GARANTIA", registros.getString("NUM_GARANTIA"));
                registro.put("ANIO_TRIMESTRE_PRMR_INC", registros.getString("ANIO_TRIMESTRE_PRMR_INC"));
                registro.put("FECHA_PRIMER_INCUMPLIMIENTO", registros.getString("FECHA_PRIMER_INCUMPLIMIENTO"));
                registro.put("FECHA_ULTIMO_INCUMPLIMIENTO", registros.getString("FECHA_ULTIMO_INCUMPLIMIENTO"));
                registro.put("PERIODO_CONSEC_INCUMPLIMIENTO", registros.getString("PERIODO_CONSEC_INCUMPLIMIENTO"));
                registro.put("FECHA_SOLICITUD_BAJA", registros.getString("FECHA_SOLICITUD_BAJA"));
                registro.put("SITUACION_INCUMPLIMIENTO", registros.getString("SITUACION_INCUMPLIMIENTO"));

                lista.add(registro);
            }

        } catch (Exception e) {
            log.info("GarantiasBean::getGarantiasConSituacionDeBajaDelPortafolioGarantizado(Exception) " + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getGarantiasConSituacionDeBajaDelPortafolioGarantizado(S)");
        }
        return lista;
    }

    public boolean esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(String claveIF) throws NafinException {

        log.info("GarantiasBean::esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(E)");

        // Extraer fecha actual
        SimpleDateFormat simpleDate = new SimpleDateFormat("MM/yyyy");
        String[] fecha = simpleDate.format(Fecha.getSysDate()).split("\\/");
        int mes = Integer.parseInt(fecha[0]);
        int anio = Integer.parseInt(fecha[1]);
        int trimestre = getTrimestre(mes);
        int anioTrimestreAnterior = 0;
        int trimestreAnterior = 0;
        int anioTrimestreAnteAnterior = 0;
        int trimestreAnteAnterior = 0;

        // Calcular anio y trimestre anteriores
        if (trimestre == 1) {
            anioTrimestreAnterior = anio - 1;
            trimestreAnterior = 4;
        } else {
            anioTrimestreAnterior = anio;
            trimestreAnterior = trimestre - 1;
        }

        // Calcular anio y trimestre anteanteriores
        if (trimestreAnterior == 1) {
            anioTrimestreAnteAnterior = anioTrimestreAnterior - 1;
            trimestreAnteAnterior = 4;
        } else {
            anioTrimestreAnteAnterior = anioTrimestreAnterior;
            trimestreAnteAnterior = trimestreAnterior - 1;
        }

        String fecha_corte =
            Fecha.getUltimoDiaDelMes("01/" + (trimestreAnteAnterior * 3) + "/" + anioTrimestreAnteAnterior,
                                     "dd/MM/yyyy");

        // Realizar consulta
        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        boolean esStatusAcuseValido = false;
        try {
            con.conexionDB();
            qrySentencia =
                "  SELECT  " + "    DECODE(I.ACUSE,01,'false',02,'true') HAY_ACUSE_DE_RECIBO " + "  FROM  " +
                "    SIAG_IFIN I " + "  WHERE " + "    I.ANOTRIM                = ?   AND " +
                "    I.TRIMESTRE              = ?   AND " + "		I.FECHA_CORTE				 >= 	 TO_DATE(?,'dd/mm/yyyy') 	AND " +
                "		I.FECHA_CORTE				 < 	 TO_DATE(?,'dd/mm/yyyy')+? AND " + "    I.CLAVE = (  " +
                "                 SELECT  " + "                   IC_IF_SIAG " + "                 FROM  " +
                "                   COMCAT_IF  " + "                 WHERE  " +
                "                   ROWNUM  = ?    AND  " + "                   IC_IF   = ?  " + "              ) ";
            lVarBind.add(new Integer(anioTrimestreAnteAnterior));
            lVarBind.add(new Integer(trimestreAnteAnterior));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                //log.info("\nEntro a extraer registro"); // Debug info salim
                esStatusAcuseValido = registros.getString("HAY_ACUSE_DE_RECIBO").equals("true") ? true : false;
                //log.info("Contenido del Registro: "+registros.getString("HAY_ACUSE_DE_RECIBO")+"\n"); // Debug info salim


            } else {
                //log.info("\nNo encontro dato alguno para trimestre anterior\n"); // Debug info salim
                esStatusAcuseValido = true;
                // NOTA: Esta linea la agregue para el caso en el que no haya haya registro del trimestre anterior,
                // como sucede cuando es la primera vez que hay registros, y asi poder permitir la carga de datos
            }

        } catch (Exception e) {
            log.debug("GarantiasBean::esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(S)");
        }

        return esStatusAcuseValido;
    }

    public boolean esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(String claveIF) throws NafinException {

        log.info("GarantiasBean::esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(E)");

        // Calcular trimestre y anio actual
        SimpleDateFormat simpleDate = new SimpleDateFormat("MM/yyyy");
        String[] fecha = simpleDate.format(Fecha.getSysDate()).split("\\/");
        int mes = Integer.parseInt(fecha[0]);
        int anio = Integer.parseInt(fecha[1]);
        int trimestre = getTrimestre(mes);
        int anioTrimestreAnterior = 0;
        int trimestreAnterior = 0;

        // Realizar consulta
        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        Registros registros = null;
        List lVarBind = new ArrayList();
        boolean esStatusDeRecepcionValido = false;

        // Calcular anio y trimestre anteriores
        if (trimestre == 1) {
            anioTrimestreAnterior = anio - 1;
            trimestreAnterior = 4;
        } else {
            anioTrimestreAnterior = anio;
            trimestreAnterior = trimestre - 1;
        }

        String fecha_corte =
            Fecha.getUltimoDiaDelMes("01/" + (trimestreAnterior * 3) + "/" + anioTrimestreAnterior, "dd/MM/yyyy");

        try {
            con.conexionDB();
            qrySentencia =
                "  SELECT  " + "    DECODE(I.RECEPCION_PORTAFOLIO,01,'false',02,'true') PORTAFOLIO_EXTRAIDO " +
                "  FROM  " + "    SIAG_IFIN I " + "  WHERE " + "    I.ANOTRIM                = ?  AND " +
                "    I.TRIMESTRE              = ?  AND " + "		I.FECHA_CORTE				 >= 	TO_DATE(?,'dd/mm/yyyy')   AND " +
                "		I.FECHA_CORTE				 < 	TO_DATE(?,'dd/mm/yyyy')+? AND " + "    I.CLAVE = (  " +
                "                 SELECT  " + "                   IC_IF_SIAG " + "                 FROM  " +
                "                   COMCAT_IF  " + "                 WHERE  " +
                "                   ROWNUM  = ?    AND  " + "                   IC_IF   = ?  " + "              ) ";
            lVarBind.add(new Integer(anioTrimestreAnterior));
            lVarBind.add(new Integer(trimestreAnterior));
            lVarBind.add(fecha_corte);
            lVarBind.add(fecha_corte);
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(1));
            lVarBind.add(new Integer(claveIF));

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            con.terminaTransaccion(true);

            if (registros != null && registros.next()) {
                esStatusDeRecepcionValido = registros.getString("PORTAFOLIO_EXTRAIDO").equals("true") ? true : false;
            }

        } catch (Exception e) {
            log.debug("GarantiasBean::esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(S)");
        }

        //return esStatusDeRecepcionValido;
        return true;

    }

    private int getTrimestre(int mes) {

        int trimestre = 0;

        switch (mes) {
        case 1:
        case 2:
        case 3:
            trimestre = 1;
            break;
        case 4:
        case 5:
        case 6:
            trimestre = 2;
            break;
        case 7:
        case 8:
        case 9:
            trimestre = 3;
            break;
        case 10:
        case 11:
        case 12:
            trimestre = 4;
            break;
        }

        return trimestre;
    }

    /**
     * Generaci�n de Garant�as autom�ticas.
     * Este proceso est� pensado en generar garantias autom�ticas para IF=FIDE
     * a partir de las solicitudes de credito electr�nico.
     * @param ic_if Clave del IF a procesar
     * @since F010-2009
     * @author Gilberto Aparicio
     * @return Regresa objeto ResultadosGar, que contiene el detalle de la carga
     * @throws com.netro.exception.NafinException
     */
    public ResultadosGar generarGarantiaAutomatica(String ic_if, String baseOperacion) throws NafinException {
        log.info("generarGarantiaAutomatica(E)");
        //**********************************Validaci�n de parametros:*****************************
        try {
            if (ic_if == null) {
                throw new Exception("Los parametros son requeridos");
            }
        } catch (Exception e) {
            log.info("generarGarantiaAutomatica()::Error en los parametros establecidos. " + e.getMessage() +
                               " ic_if=" + ic_if);
            throw new NafinException("SIST0001");
        }
        //****************************************************************************************

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        ResultadosGar retorno = new ResultadosGar();
        List ErroresGral = new ArrayList();
        PreparedStatement psErrorInsert = null;
        try {
            con.conexionDB();

            String qrySIAG = " SELECT ic_if_siag " + " FROM comcat_if " + " WHERE ic_if = ? ";
            PreparedStatement psSIAG = con.queryPrecompilado(qrySIAG);
            psSIAG.setInt(1, Integer.parseInt(ic_if));
            ResultSet rsSIAG = psSIAG.executeQuery();
            int ifSiag = 0;
            if (rsSIAG.next()) {
                ifSiag = rsSIAG.getInt("ic_if_siag");
            }
            rsSIAG.close();
            psSIAG.close();


            String strSQL =
                " SELECT /*+ INDEX (sp IN_COM_SOLIC_PORTAL_05_NUK) use_nl(sp p d c i)*/  " +
                " 	CASE WHEN p.cs_tipo_persona = 'F' THEN " +
                " 		p.cg_appat || ' ' || p.cg_apmat || ' ' || p.cg_nombre " +
                " 	ELSE p.cg_razon_social END AS cg_razon_social,  " + " 	REPLACE(p.cg_rfc,'-','') as cg_rfc,  " +
                " 	d.cg_calle,  " + " 	d.cg_colonia,  " + " 	d.cn_cp,  " + " 	d.ic_estado,  " +
                " 	d.ic_municipio,  " +
                //" 	d.ic_estado, " +
                " 	c.cd_nombre AS nombreClase,  " + " 	sp.ig_numero_prestamo,  " + " 	p.ic_sector_econ,  " +
                " 	pg.cs_solic_fondeo AS solicitaFondeoNafin,  " + //FODEA 011-2010 FVR
                " 	pg.cs_solic_parti_riesgo AS solicitaParticipRiesgoNAFIN, " + //FODEA 011-2010 FVR
                " 	pg.cg_porcentaje_parti AS porcParticipacionSolicitado,  " + //FODEA 011-2010 FVR
                " 	p.in_numero_emp,  " + " 	p.fn_ventas_net_tot, " +
                " 	pg.cg_proposito_proyec AS propositoProyecto,  " + //FODEA 011-2010 FVR
                " 	pg.in_clave_tipo_cred AS claveTipoFinanciamiento, " + //FODEA 011-2010 FVR
                " 	pg.cg_clave_garantia AS param19, " + //FODEA 011-2010 FVR
                " 	pg.cg_porcen_parti_finan AS param20, " + //FODEA 011-2010 FVR
                " 	pg.in_porcen_prod_mer_inter AS porcProdDestMercadoInterno, " + //FODEA 011-2010 FVR
                " 	pg.in_porcen_prod_mer_exter AS porcProdDestMercadoExterno, " + //FODEA 011-2010 FVR
                " 	pg.in_forma_amortizacion AS formaAmortizacion, " + //FODEA 011-2010 FVR
                " 	sp.fn_importe_dscto, " + " 	TO_CHAR(sp.df_operacion, 'yyyymmdd') AS df_operacion, " +
                " 	sp.fn_importe_dscto, " + " 	pg.cg_modena AS moneda, " + //FODEA 011-2010 FVR
                " 	sp.in_numero_amort*sp.ic_periodicidad_c AS numMesesPlazo,  " +
                " 	pg.in_num_meses_gracia_finan AS numMesesGraciaFinan, " + //FODEA 011-2010 FVR
                " 	sp.FG_STUF, " + " 	pg.in_clave_funcionario_facul_if AS claveFuncionarioFacultadoIF, " + //FODEA 011-2010 FVR
                " 	i.ic_if_siag, " + " 	pg.in_tipo_autorizacion AS tipoAutorizacion, " + //FODEA 011-2010 FVR
                " 	pg.in_tipo_programa AS tipoPrograma, " + //FODEA 011-2010 FVR
                " 	DECODE(pg.cs_tipo_tasa, 'V', IC_TASAUF, pg.cs_tipo_tasa) AS tipoTasa, " + //FODEA 011-2010 FVR // FODEA 016-2013 EGB
                " 	pg.in_plazo_dias AS plazoNumDias, " + //FODEA 011-2010 FVR
                " 	pg.cg_calificacion_final AS calificacionInicial, " + //FODEA 011-2010 FVR
                " 	pg.in_antiguedad_client_meses AS antiguedadClienteMeses, " + //FODEA 011-2010 FVR
                "	  (t.cg_nombre||' '||t.cg_appat||' '||t.cg_apmat) as nombreContacto, " +
                "    d.cg_telefono1 as telefonoPyme, " + "    p.cg_email as correoPyme, " + " 	'' AS entidad, " +
                //MODIFICACION F059-2009 FVR
                "	p.CG_EXTENSION as extension, " + "  p.CS_TIPO_PERSONA as tipoPersona,  " +
                "  decode(p.CG_SEXO,'M','H','F','M') as generoAcreditado " +
                " FROM com_solic_portal sp, comcat_pyme p, com_domicilio d, comcat_clase c, comcat_if i, com_contacto t, com_param_garantias pg " +
                " WHERE  " + " 	sp.ic_estatus_solic = ? " + //3
                " 	AND sp.cs_alta_automatica_garantia = ? " + //'S'
                " 	AND sp.ic_if = ? " + //FIDE=103
                " 	AND sp.df_operacion >= TRUNC(SYSDATE-15) AND sp.df_operacion < TRUNC(SYSDATE) " + //--FODEA 047-2010 Se quito el +1 para que tome los registros con fecha de Operaci�n del d�a anterior y no las del d�a actual
                //--El SYSDATE-15 es para que en caso de que (por alguna razon) no corra el proceso un dia, a la sig. ejecucion procese los pendientes de hasta 15 dias de antig�edad
                " 	AND sp.df_envio_garantia IS NULL " + " 	AND sp.ig_clave_sirac = p.in_numero_sirac (+) " +
                " 	AND p.ic_pyme = d.ic_pyme(+) " + " 	AND d.cs_fiscal(+) = ? " + " 	AND p.ic_clase = c.ic_clase(+) " +
                " 	AND p.ic_rama = c.ic_rama(+) " + " 	AND p.ic_subsector = c.ic_subsector(+) " +
                " 	AND p.ic_sector_econ = c.ic_sector_econ(+) " + " 	AND sp.ic_if = i.ic_if " +
                "	AND p.ic_pyme = t.ic_pyme(+) " + "	AND sp.ig_base_operacion = pg.ig_base_operacion " + //FODEA 011-2010 FVR INI
                "	AND sp.ic_if = pg.ic_if "; //FODEA 011-2010 FVR INI
            if (!baseOperacion.equals("")) {
                strSQL += "	AND sp.ig_base_operacion in ( " + baseOperacion + ")"; //FODEA 011-2010 FVR INI
            }
            strSQL += "	AND t.cs_primer_contacto(+) = 'S' " + " ORDER BY sp.ig_numero_prestamo ";


            log.info("strSQL>>>>garantias_automatica>>>" + strSQL);
            PreparedStatement ps = con.queryPrecompilado(strSQL);
            ps.setInt(1, 3); //ic_estatus_solic=3
            ps.setString(2, "S"); //cs_alta_automatica_garantia=S
            ps.setString(3, ic_if); //ic_if = 103
            ps.setString(4, "S"); //cs_fiscal=S

            ResultSet rs = ps.executeQuery();

            retorno.setIcProceso(calculaIcProceso());

            String qryErrorInsert =
                " INSERT INTO gti_error_alta_garant " + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO,ERROR)" + "values(?,?,?,?)";
            psErrorInsert = con.queryPrecompilado(qryErrorInsert);

            String qryInsert =
                " INSERT INTO gtitmp_contenido_arch " + "(IC_PROCESO,IC_LINEA,IG_PROGRAMA,CG_CONTENIDO)" +
                "values(?,?,?,?)";
            PreparedStatement psInsert = con.queryPrecompilado(qryInsert);
            int numLinea = 0;

            //BigDecimal tmpMonto = new BigDecimal(0);
            StringBuffer strLineaLayout = new StringBuffer();
            while (rs.next()) {
                numLinea++;
                retorno.setErrorLinea(false);
                strLineaLayout.delete(0, strLineaLayout.length());

                String nombreContacto = rs.getString("nombreContacto");
                String telefonoPyme = rs.getString("telefonoPyme");
                String extension = rs.getString("extension");

                if (nombreContacto == null || "".equals(nombreContacto.trim()))
                    nombreContacto = "SIN CONTACTO";

                if (telefonoPyme != null && !"".equals(telefonoPyme)) {
                    telefonoPyme = " 1" + telefonoPyme;

                    if (extension != null && !"".equals(extension)) {
                        telefonoPyme = telefonoPyme + extension;
                    }

                    while (telefonoPyme.length() < 18) {
                        telefonoPyme = telefonoPyme + " ";
                    }
                }

                strLineaLayout.append(((rs.getString("cg_razon_social") != null) ? rs.getString("cg_razon_social") :
                                       "") + "@" + ((rs.getString("cg_rfc") != null) ? rs.getString("cg_rfc") : "") +
                                      "@" + ((rs.getString("cg_calle") != null) ? rs.getString("cg_calle") : "") + "@" +
                                      ((rs.getString("cg_colonia") != null) ? rs.getString("cg_colonia") : "") + "@" +
                                      ((rs.getString("cn_cp") != null) ? rs.getString("cn_cp") : "") + "@" +
                                      ((rs.getString("ic_estado") != null) ? rs.getString("ic_estado") : "") + "@" +
                                      ((rs.getString("ic_municipio") != null) ? rs.getString("ic_municipio") : "") +
                                      "@" + ((rs.getString("ic_estado") != null) ? rs.getString("ic_estado") : "") +
                                      "@" + ((rs.getString("nombreClase") != null) ? rs.getString("nombreClase") : "") +
                                      "@" +
                                      /*9*/((rs.getString("ig_numero_prestamo") != null) ?
                                            rs.getString("ig_numero_prestamo") : "") + "@" +
                                      ((rs.getString("ic_sector_econ") != null) ? rs.getString("ic_sector_econ") : "") +
                                      "@" +
                                      ((rs.getString("solicitaFondeoNafin") != null) ?
                                       rs.getString("solicitaFondeoNafin") : "") + "@" +
                                      ((rs.getString("solicitaParticipRiesgoNAFIN") != null) ?
                                       rs.getString("solicitaParticipRiesgoNAFIN") : "") + "@" +
                                      ((rs.getString("porcParticipacionSolicitado") != null) ?
                                       rs.getString("porcParticipacionSolicitado") : "") + "@" +
                                      ((rs.getString("in_numero_emp") != null) ? rs.getString("in_numero_emp") : "") +
                                      "@" +
                                      ((rs.getString("fn_ventas_net_tot") != null) ? rs.getString("fn_ventas_net_tot") :
                                       "") + "@" +
                                      ((rs.getString("propositoProyecto") != null) ? rs.getString("propositoProyecto") :
                                       "") + "@" +
                                      ((rs.getString("claveTipoFinanciamiento") != null) ?
                                       rs.getString("claveTipoFinanciamiento") : "") + "@" +
                                      ((rs.getString("param19") != null) ? rs.getString("param19") : "") + "@" +
                                      ((rs.getString("param20") != null) ? rs.getString("param20") : "") + "@" +
                                      ((rs.getString("porcProdDestMercadoInterno") != null) ?
                                       rs.getString("porcProdDestMercadoInterno") : "") + "@" +
                                      ((rs.getString("porcProdDestMercadoExterno") != null) ?
                                       rs.getString("porcProdDestMercadoExterno") : "") + "@" +
                                      ((rs.getString("formaAmortizacion") != null) ? rs.getString("formaAmortizacion") :
                                       "") +
                                      "@" +
                                      /*23*/((rs.getString("fn_importe_dscto") != null) ?
                                             rs.getString("fn_importe_dscto") : "") + "@" +
                                      ((rs.getString("df_operacion") != null) ? rs.getString("df_operacion") : "") +
                                      "@" +
                                      /*25*/((rs.getString("fn_importe_dscto") != null) ?
                                             rs.getString("fn_importe_dscto") : "") + "@" +
                                      ((rs.getString("moneda") != null) ? rs.getString("moneda") : "") + "@" +
                                      ((rs.getString("numMesesPlazo") != null) ? rs.getString("numMesesPlazo") : "") +
                                      "@" +
                                      ((rs.getString("numMesesGraciaFinan") != null) ?
                                       rs.getString("numMesesGraciaFinan") : "") + "@" +
                                      ((rs.getString("fg_stuf") != null) ? rs.getString("fg_stuf") : "") + "@" +
                                      ((rs.getString("claveFuncionarioFacultadoIF") != null) ?
                                       rs.getString("claveFuncionarioFacultadoIF") : "") + "@" +
                                      ((rs.getString("ic_if_siag") != null) ? rs.getString("ic_if_siag") : "") + "@" +
                                      ((rs.getString("tipoAutorizacion") != null) ? rs.getString("tipoAutorizacion") :
                                       "") + "@" +
                                      ((rs.getString("tipoPrograma") != null) ? rs.getString("tipoPrograma") : "") +
                                      "@" + ((rs.getString("tipoTasa") != null) ? rs.getString("tipoTasa") : "") + "@" +
                                      ((rs.getString("plazoNumDias") != null) ? rs.getString("plazoNumDias") : "") +
                                      "@" +
                                      ((rs.getString("calificacionInicial") != null) ?
                                       rs.getString("calificacionInicial") : "") + "@" +
                                      ((rs.getString("antiguedadClienteMeses") != null) ?
                                       rs.getString("antiguedadClienteMeses") : "") + "@" +
                                      //MODIFICACION FODEA 059-2009
                                      ((nombreContacto != null) ? nombreContacto : "") + "@" +
                                      ((telefonoPyme != null) ? telefonoPyme : "") + "@" +
                                      ((rs.getString("correoPyme") != null) ? rs.getString("correoPyme") : "") + "@" +
                                      ((rs.getString("entidad") != null) ? rs.getString("entidad") : "") + "@" +
                                      ((rs.getString("tipoPersona") != null) ? rs.getString("tipoPersona") : "") + "@" +
                                      ((rs.getString("generoAcreditado") != null) ? rs.getString("generoAcreditado") :
                                       "") + "@");
                ResultadosGar errorGar = new ResultadosGar();
                this.validarAltaGarantias(ifSiag, numLinea, strLineaLayout.toString(), retorno);
                this.validarAltaGarantias(ifSiag, numLinea, strLineaLayout.toString(), errorGar);

                if (!retorno.getErrorLinea()) { //Si no hay errores
                    if (numLinea == 0) {
                        retorno.appendCorrectos("Claves de financiamiento:\n");
                    }
                    //retorno.appendCorrectos("Linea:"+numLinea+" Monto del Financiamiento="+Comunes.formatoDecimal((String)lovDatos.get(23),2)+" Acreditado="+lovDatos.get(0)+".\n");
                    retorno.appendCorrectos("" + rs.getString("ig_numero_prestamo") + "\n");
                    retorno.incNumRegOk();
                    retorno.addSumRegOk(rs.getDouble("fn_importe_dscto"));
                    //insercion en tabla temporal solo si no hay ningun error hasta el momento
                    //porque ya no tiene caso insertar si se va realizar un rollback m�s abajo
                    //por los errores encontrados.
                    if (retorno.getErrores().length() == 0) {
                        psInsert.setInt(1, retorno.getIcProceso()); //ic_proceso
                        psInsert.setInt(2, numLinea); //ig_linea
                        psInsert.setString(3, rs.getString("tipoPrograma")); //programa
                        psInsert.setString(4, strLineaLayout.toString()); //linea con layout de carga
                        psInsert.executeUpdate();
                    }
                } else {
                    List ErroresReg = new ArrayList();
                    retorno.appendErrores("linea:" + numLinea + ":" + strLineaLayout + "\n");
                    retorno.incNumRegErr();
                    retorno.addSumRegErr(rs.getDouble("fn_importe_dscto"));

                    ErroresReg.add(String.valueOf(numLinea));
                    ErroresReg.add(strLineaLayout.toString());
                    ErroresReg.add(errorGar.getErrores());
                    ErroresGral.add(ErroresReg);

                }
                //dbl_sum_montos = dbl_sum_montos.add(tmpMonto);

            } //fin while
            rs.close();
            ps.close();
            psInsert.close();

            if (retorno.getErrores().length() == 0 && retorno.getNumRegOk() > 0) {
                //Si no hay ningun error y hay registros procesados,
                //hace commit en la tabla temporal
                //y envia para su alta en la tabla definitiva
                con.terminaTransaccion(true);
                ResultadosGar retornoTrasmitirAltaGarantia =
                    this.transmitirAltaGarantia(String.valueOf(retorno.getIcProceso()), ic_if,
                                                String.valueOf(retorno.getNumRegOk()),
                                                String.valueOf(retorno.getSumRegOk()), "proc_aut");
                retorno.setFolio(retornoTrasmitirAltaGarantia.getFolio());
                retorno.setFecha(retornoTrasmitirAltaGarantia.getFecha());
                retorno.setHora(retornoTrasmitirAltaGarantia.getHora());
                //Si ya aseguramos que se mand� la informaci�n y se proceso correctamente,
                //Se realiza la actualizaci�n de fecha de envio a garantias
                //para evitar su reenvio.
                String qryUpdate =
                    " UPDATE com_solic_portal " + " SET df_envio_garantia = sysdate " + " WHERE ic_estatus_solic = ? " + //3
                    " 	AND cs_alta_automatica_garantia = ? " + //'S'
                    " 	AND ic_if = ? " + //FIDE=103
                    " 	AND df_operacion >= TRUNC(SYSDATE-15) AND df_operacion < TRUNC(SYSDATE)  " + //--El SYSDATE-15 es para que en caso de que (por alguna razon) no corra el proceso un dia, a la sig. ejecucion procese los pendientes de hasta 15 dias de antig�edad
                    " 	AND df_envio_garantia IS NULL ";
                if (!baseOperacion.equals("")) { //Correccion media cochina por ser urgente.. GEAG
                    qryUpdate += "	AND ig_base_operacion in ( " + baseOperacion + ")";
                }
                PreparedStatement psUpdate = con.queryPrecompilado(qryUpdate);
                psUpdate.setInt(1, 3); //ic_estatus_solic=3
                psUpdate.setString(2, "S"); //cs_alta_automatica_garantia=S
                psUpdate.setString(3, ic_if); //ic_if = 103
                psUpdate.executeUpdate();
                psUpdate.close();
            }

            return retorno;
        } catch (Exception e) {
            exito = false;
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                if (exito && retorno.getErrores().length() == 0) {
                    con.terminaTransaccion(true);
                } else {
                    con.terminaTransaccion(false);
                    try {
                        if (ErroresGral != null && ErroresGral.size() > 0) {
                            for (int x = 0; x < ErroresGral.size(); x++) {
                                List Errores = (List) ErroresGral.get(x);
                                String rempplazo = "Error en la linea:" + (String) Errores.get(0);
                                String errores = ((String) Errores.get(2)).replaceAll(rempplazo, "");
                                errores = errores.replaceAll("\n", " ");
                                psErrorInsert.setInt(1, retorno.getIcProceso()); //ic_proceso
                                psErrorInsert.setInt(2, Integer.parseInt((String) Errores.get(0))); //ig_linea
                                psErrorInsert.setString(3, (String) Errores.get(1)); //programa
                                psErrorInsert.setString(4, errores); //linea con layout de carga
                                psErrorInsert.executeUpdate();
                            }
                            if (psErrorInsert != null)
                                psErrorInsert.close();
                            con.terminaTransaccion(true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                con.cierraConexionDB();
            }
            log.info("generarGarantiaAutomatica(S)");
        }
    }


    /******************************************************************/
    public ResultadosGar procesarCargaSaldosRegistrados(String ic_if) throws NafinException {

        log.info(" procesarCargaSaldosRegistrados(E)");

        AccesoDB con = new AccesoDB();
        ResultadosGar retorno = new ResultadosGar();
        ResultadosGar retorno2 = new ResultadosGar();
        PreparedStatement ps = null;
        ResultadosGar retorno1 = new ResultadosGar();

        ResultSet rs = null;
        ResultSet rs2 = null;
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        String qrySentencia = "";
        String qrySentenciaDetalle = "";
        PreparedStatement psD = null;
        PreparedStatement psE = null;
        PreparedStatement psF = null;
        PreparedStatement psZ = null;
        ResultSet rsF = null;
        ResultSet rsA = null;
        PreparedStatement psA = null;
        ResultSet rsI = null;

        PreparedStatement psH = null;
        PreparedStatement psJ = null;
        ResultSet rsH = null;

        PreparedStatement psL = null;
        ResultSet rsL = null;

        try {

            VectorTokenizer vt = null;
            Vector lovDatos = null;
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            int reg = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);
            List clavesFinan = new ArrayList();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            int iNumRegXproceso = 0;
            String aniomes = "";
            String s_contenido = "";
            String ic_det = "";
            int totalRechazados = 0;
            int totalAceptados = 0;
            int totalRegistros = 0;
            String claveFinancia = "";
            String d_aniomes = "";
            String in_enc = "";
            int ic_enc = 0;
            CallableStatement cs = null;

            con.conexionDB();

            /*  Procedimiento almacenado que toma la informaci�n resultado de las tablas del SIAG
        para reportar al FIDE los errores en el   procesamiento de los envios de Saldos*/

            cs = con.ejecutaSP("SP_ACT_ERR_SALDOS_FIDE()");
            try {
                cs.execute();

            } catch (SQLException sqle) {
                sqle.printStackTrace();
                throw new NafinException("SIST0001" + sqle);

            }
            if (cs != null)
                cs.close();
            con.terminaTransaccion(true);


            qrySentencia = "select ic_if_siag from comcat_if" + " where ic_if = ?";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();

            retorno1.setIcProceso(calculaIcProceso());

            log.info("retorno1.getIcProceso()>>>>>>>>>>>>>>>>>>>>>" + retorno1.getIcProceso());
            log.info("ifSiag>>>>>>>>>>>>>>>>>>>>>" + ifSiag);


            // SE OBTIENEN TODOS LOS ACUSES YA LEIDOS POR EL FIDE PARA PROCEDER A SU DEPURACION
            qrySentencia = " select ic_folio from  COMTMP_SALDOS_ALTA_ACUSE_FIDE " + " where ig_estatus = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, 4);
            rs = ps.executeQuery();
            while (rs.next()) {
                String folio = rs.getString("ic_folio");

                //Borra todos los registros del acuse que tienen estatus 4-Para Depurar
                qrySentencia = " DELETE FROM COMTMP_SAL_ALTA_ACUSE_DET_FIDE where ic_folio = ? ";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setString(1, folio);
                ps.execute();
                ps.close();

                //Actualiza el acuse con el estatus 5-Depurado
                qrySentencia =
                    "  UPDATE  COMTMP_SALDOS_ALTA_ACUSE_FIDE " + " SET ig_estatus  = ? " + " where ic_folio = ? ";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, 5);
                ps.setString(2, folio);
                ps.execute();
                ps.close();

            }
            rs.close();
            ps.close();
            con.terminaTransaccion(true);

            //borrar los datos capturados anteriormente para no tener  historial

            qrySentencia = "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" + "values(?,?,?)";
            ps = con.queryPrecompilado(qrySentencia);

            //inserta los registros con error o sin error
            qrySentenciaDetalle =
                " insert into COMTMP_SAL_ALTA_ACUSE_DET_FIDE  " +
                " (IC_FOLIO, IC_DET, CG_DATOS_SALDO, CG_ERROR, IG_ESTATUS, CG_PERIODO )" + " values( ?,?,?,?,?,?) ";
            psD = con.queryPrecompilado(qrySentenciaDetalle);


            if (retorno.getErrores().length() ==
                0) {
                //inicia ciclo de almacenamiento

                /***encabezado de tabla temporal*/
                String qrySentenciaI =
                                      " select ic_enc, cg_periodo  from comtmp_saldos_alta_fide_enc  where ig_estatus = 0";

                //VALIDAD SI EXTEN  O NO DATOS EN LAS SIGUIENTES TABLAS
                String existeAcuse = "";
                String existeDetalle = "";
                String qrySentenciaZ = " DELETE FROM COMTMP_SALDOS_ALTA_FIDE_DET where ic_enc = ? ";
                String qrySentenciaK = " DELETE FROM COMTMP_SALDOS_ALTA_FIDE_ENC where ic_enc = ? ";
                String qrySentenciaDet = " DELETE FROM COMTMP_SAL_ALTA_ACUSE_DET_FIDE where ic_folio = ? ";

                rsI = con.queryDB(qrySentenciaI);

                //Inicia proceso de registros
                while (rsI.next()) {

                    ic_enc = rsI.getInt("ic_enc");
                    in_enc = rsI.getString("ic_enc") == null ? "" : rsI.getString("ic_enc");

                    log.debug("encabezado  " + in_enc);
                    //verifico si ya existe un acuse con el folio a procesar
                    qrySentencia = "select ic_folio from COMTMP_SALDOS_ALTA_ACUSE_FIDE  where ic_folio = ? "; //acuse
                    psH = con.queryPrecompilado(qrySentencia);
                    psH.setInt(1, ic_enc);
                    rsH = psH.executeQuery();
                    if (rsH.next()) {
                        existeAcuse = rsH.getString("ic_folio") == null ? "" : rsH.getString("ic_folio");
                    } else {
                        existeAcuse = "";
                    }
                    rsH.close();
                    psH.close();
                    if (!"".equals(existeAcuse)) {
                        log.debug("Caso extrano, ya que el folio ya se habia procesado anteriormente, pero no se habia borrado del encabezado por lo que se procede a borrar el encabezado y detalle del encabezado:  " +
                                  in_enc);
                        log.debug("y pasar al siguiente registro.");

                        psJ = con.queryPrecompilado(qrySentenciaZ);
                        psJ.setInt(1, ic_enc);
                        psJ.execute();
                        psJ.close();

                        psJ = con.queryPrecompilado(qrySentenciaK);
                        psJ.setInt(1, ic_enc);
                        psJ.execute();
                        psJ.close();
                        log.debug("qrySentencia   " + qrySentenciaK);
                        con.terminaTransaccion(true);
                        continue;
                    }


                    //verifico si ya existen registros procesados en esta tabla de detalle de acuse con el folio a procesar
                    qrySentencia = "select count(ic_folio) from COMTMP_SAL_ALTA_ACUSE_DET_FIDE where ic_folio= ? ";
                    psL = con.queryPrecompilado(qrySentencia);
                    psL.setString(1, in_enc);
                    rsL = psL.executeQuery();
                    if (rsL.next()) {
                        existeDetalle = rsL.getInt(1) != 0 ? in_enc : "";
                    }
                    rsL.close();
                    psL.close();
                    if (!"".equals(existeDetalle)) {
                        log.debug("Caso extrano, seguramente porque se trono el proceso y se quedo en proceso el folio. Se borra y reinicia el procesamiento del encabezado:" +
                                  in_enc);
                        psJ = con.queryPrecompilado(qrySentenciaDet);
                        psJ.setInt(1, ic_enc);
                        psJ.execute();
                        psJ.close();
                        log.debug("qrySentenciaDet   " + qrySentenciaDet);
                        con.terminaTransaccion(true);
                    }

                    log.debug("existeAcuse  " + existeAcuse);
                    log.debug("existeDetalle  " + existeDetalle);


                    in_enc = rsI.getString("ic_enc");
                    aniomes = rsI.getString("cg_periodo");

                    qrySentencia =
                        " select ic_det, cg_datos_saldo from comtmp_saldos_alta_fide_det  where ic_enc = '" + in_enc +
                        "'";
                    rs2 = con.queryDB(qrySentencia);
                    log.debug("qrySentencia " + qrySentencia);

                    String qrySentencia9 = " select to_char(to_date(?,'mmyy'),'yyyymm') from dual ";
                    psA = con.queryPrecompilado(qrySentencia9);
                    psA.setString(1, aniomes);
                    rsA = psA.executeQuery();
                    if (rsA.next()) {
                        d_aniomes = rsA.getString(1);
                    }
                    rsA.close();
                    psA.close();


                    if (rs2 != null) {
                        lineas = -1;
                        numLinea = 0;
                        while (rs2.next()) { //**********************************************************************
                            retorno = new ResultadosGar();
                            s_contenido = rs2.getString("cg_datos_saldo");
                            ic_det = rs2.getString("ic_det");

                            lineas++;
                            numLinea++;
                            iNumRegXproceso++;
                            log.debug("Entra a ciclo de insertado en gtitmp_contenido_arch  linea:  " + numLinea +
                                      " ic_det: " + ic_det + " encabezado " + in_enc);

                            BigDecimal tmpMonto = new BigDecimal(0);
                            BigDecimal totPagar = new BigDecimal(0);

                            retorno.setErrorLinea(false);


                            if (sbInClavesFinan.length() > 0)
                                sbInClavesFinan.append(",");
                            sbInClavesFinan.append("?");


                            ctrl_linea++;

                            try {

                                vt = new VectorTokenizer(s_contenido, "@");
                                lovDatos = vt.getValuesVector();
                                log.info(lovDatos.toString());
                                reg = 15;

                                if (lovDatos.size() < reg) {
                                    retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() +
                                                          " \n");
                                    retorno.setErrorLinea(true);
                                    clavesFinan.add("0");
                                    retorno.incNumRegErr();
                                    continue;
                                }
                            } catch (Exception e) {
                                retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() + " \n");
                                retorno.setErrorLinea(true);
                                clavesFinan.add("0");
                                retorno.incNumRegErr();
                                continue;
                            }

                            if (!retorno.getErrorLinea()) {
                                tmpMonto = new BigDecimal(((String) lovDatos.get(6)).trim());
                            }

                            clavesFinan.add(lovDatos.get(1));
                            claveFinancia = ((String) lovDatos.get(1)).trim();

                            try {
                                totPagar = new BigDecimal(((String) lovDatos.get(5)).trim());
                            } catch (Exception e) {
                                retorno.appendErrores(" Clave del Financiamiento:" + (String) lovDatos.get(1) +
                                                      ". El campo total a pagar no es un numero valido\n");
                                retorno.setErrorLinea(true);
                            }

                            validaFlotante(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(6),
                                           "Saldo del acreditado al ultimo dia del mes", retorno);
                            validaEntero(numLinea, (String) lovDatos.get(1), (String) lovDatos.get(7), "Mora en dias",
                                         retorno);

                            reg = 0;

                            if (lovDatos.size() < reg) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores(" Clave del Financiamiento: " + (String) lovDatos.get(1) +
                                                      ".  El numero de campos es mayor al debido. \n");
                            }

                            //valida que  no exista la garantia en la tabla de  gti_estatus_solic
                            //Seccion temporalmente comentada en lo que EGB ve que hacer con el query

                            /*

								String qrySentencia2 =
									"select gs.ic_folio as ic_folio,gs.cc_garantia as cc_garantia "+
									" from gti_garant_saldos gs"+
									" ,gti_estatus_solic es"+
									" where gs.ic_folio = es.ic_folio"+
									" and gs.ic_if_siag = es.ic_if_siag"+
									" and es.ic_situacion not in(?,?)"+
									" and gs.ic_if_siag = ?"+
									" and gs.cg_anio_mes =  ?"+
									" and gs.cc_garantia =   ?";


								psF = con.queryPrecompilado(qrySentencia2);
								psF.setInt(1,5);
								psF.setInt(2,7);
								psF.setInt(3,ifSiag);
								psF.setString(4,d_aniomes);
								psF.setString(5,claveFinancia);
								rsF = psF.executeQuery();
								//psF.clearParameters();
								if(rsF.next()){
									log.debug("La clave: "+rsF.getString("cc_garantia")+" ya se encuentra registrada este mes en el folio:"+rsF.getString("ic_folio")+".\n");
									retorno.appendErrores("La clave: "+rsF.getString("cc_garantia")+" ya se encuentra registrada este mes en el folio:"+rsF.getString("ic_folio")+".\n");
									retorno.setErrorLinea(true);
								}
								rsF.close();
						    psF.close();
              */


                            if (!retorno.getErrorLinea()) {
                                if (lineas == 0) {
                                    retorno.appendCorrectos("Claves de Financiamiento:\n");
                                }
                                retorno1.appendCorrectos(lovDatos.get(1) + "\n");
                                retorno1.incNumRegOk();
                                retorno1.addSumRegOk(tmpMonto.doubleValue());
                                retorno1.addSumTotPag(totPagar);


                                //guada en la tabla de gtitmp_contenido_arch
                                ps.clearParameters();
                                ps.setInt(1, retorno1.getIcProceso());
                                ps.setInt(2, numLinea);
                                ps.setString(3, s_contenido);
                                ps.execute();


                                //guarda todos los detalles en la tabla de COMTMP_SAL_ALTA_ACUSE_DET_FIDE
                                String vacio = "";
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, vacio);
                                psD.setInt(5, 0); //estatus 0 Ok
                                psD.setString(6, aniomes);

                                psD.execute();


                                /*if(iNumRegTrans>=3500){
										con.terminaTransaccion(true);
										iNumRegTrans = 0;
										System.gc();	//invocacion al garbage collector para optimizar memoria
									}*/

                                totalAceptados++;

                            } else {


                                //guarda todos los detalles en la tabla de COMTMP_SAL_ALTA_ACUSE_DET_FIDE
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, retorno.getErrores());
                                psD.setInt(5, 1); // 1 Error
                                psD.setString(6, aniomes);
                                psD.execute();

                                retorno1.incNumRegErr();
                                retorno1.addSumRegErr(tmpMonto.doubleValue());

                                totalRechazados++;
                            }

                            dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                            iNumRegTrans++;

                            totalRegistros = totalAceptados + totalRechazados;


                        } //while(rs2)
                    } //if(rs2)
                    rs2.close();


                    //Condicion para mandar informacion por el metodo "transmitirCargaSaldos"
                    //siempre y cuando por lo menos haya un registros Aceptado

                    if (numLinea != totalRechazados) {
                        retorno2 = new ResultadosGar();
                        String c_aniomes = "";
                        con.terminaTransaccion(true);
                        String qrySentencia1 = " select to_char(to_date(?,'mmyy'),'yyyymm') from dual ";
                        log.info(qrySentencia1);


                        psF = con.queryPrecompilado(qrySentencia1);
                        psF.setString(1, aniomes);
                        rsF = psF.executeQuery();
                        if (rsF.next()) {
                            c_aniomes = rsF.getString(1);
                        }
                        rsF.close();
                        psF.close();

                        retorno2 =
                            transmitirCargaSaldos(String.valueOf(retorno1.getIcProceso()), ic_if,
                                                  String.valueOf(totalAceptados),
                                                  String.valueOf(retorno1.getSumRegOk()), "", c_aniomes, clavesFinan,
                                                  "proc_aut");

                        resumenCargaSaldosRegistrados(retorno2, aniomes, totalRegistros,
                                                      new BigDecimal(retorno1.getSumRegOk()), totalAceptados,
                                                      totalRechazados, in_enc);

                    } else {

                        log.debug(" cuando todos los registros tienen errores ");

                        retorno2 = new ResultadosGar();
                        String c_aniomes = "";
                        con.terminaTransaccion(true);
                        String qrySentencia1 = " select to_char(to_date(?,'mmyy'),'yyyymm') from dual ";

                        psF = con.queryPrecompilado(qrySentencia1);
                        psF.setString(1, aniomes);
                        rsF = psF.executeQuery();
                        if (rsF.next()) {
                            c_aniomes = rsF.getString(1);
                        }
                        rsF.close();
                        psF.close();


                        String fecha = (new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
                        String hora = (new SimpleDateFormat("HH:mm")).format(new java.util.Date());

                        retorno2.setFecha(fecha);
                        retorno2.setHora(hora);
                        retorno2.setFolio("");

                        resumenCargaSaldosRegistrados(retorno2, aniomes, totalRegistros,
                                                      new BigDecimal(retorno1.getSumRegOk()), totalAceptados,
                                                      totalRechazados, in_enc);

                    }

                    //tablas que se borraran al final del proceso
                    String qrySentenciaT = " delete  from COMTMP_SALDOS_ALTA_FIDE_DET  where ic_enc  = ?";

                    psE = con.queryPrecompilado(qrySentenciaT);
                    psE.setInt(1, ic_enc);
                    psE.execute();
                    psE.close();


                    qrySentenciaT = " delete  from COMTMP_SALDOS_ALTA_FIDE_ENC where ic_enc  = ? ";
                    psZ = con.queryPrecompilado(qrySentenciaZ);
                    psZ.setInt(1, ic_enc);
                    psZ.execute();
                    psZ.close();
                    con.terminaTransaccion(true);


                    log.debug("reseteo de variables que se llenan dentro del ciclo while");
                    iNumRegXproceso = 0;
                    clavesFinan = new ArrayList();
                    dbl_sum_montos = new BigDecimal(0.00);
                    sbInClavesFinan = new StringBuffer();
                    retorno1 = new ResultadosGar();
                    retorno1.setIcProceso(calculaIcProceso());
                    lineas = -1;
                    numLinea = 0;
                    totalAceptados = 0;
                    totalRechazados = 0;
                }
                rsI.close();

            } //inicia ciclo de almacenamiento


            if (retorno.getCifras().length() ==
                0) {
                //Si no hay errores de cifras de control... establece claves de financiamiento
                //------------------------ Inicio mensajes para debug ---------------------------
                log.info("procesarCargaSaldosRegistrados(" + _fechaMsg + "_" + retorno1.getIcProceso() +
                                   ")::Cifra de Control OK. clavesFinan.size()=" + clavesFinan.size());
                //-------------------------- Fin mensajes para debug ----------------------------
                retorno.setClavesFinan(clavesFinan);
            }


        } catch (Exception e) {
            log.info("GarantiasBean::procesarCargaSaldosRegistrados(Exception)");
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("GarantiasBean::procesarCargaSaldosRegistrados(S)");
        }
        return retorno;
    }

    /******************************************************************/

    public void resumenCargaSaldosRegistrados(ResultadosGar resultado, String aniomes, int numReg, BigDecimal montoReg,
                                              int totalAceptados, int totalRechazados,
                                              String in_enc) throws NafinException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String querySentence = "";
        String estatus = "1";

        try {
            con.conexionDB();

            log.info("Entrada:::GarantiasEJB.resumenCargaSaldosRegistrados");


            querySentence =
                " insert into COMTMP_SALDOS_ALTA_ACUSE_FIDE " +
                " (IC_FOLIO, CG_FECHA, CG_HORA, CG_ANIO_MES, IN_TOTAL_REG, FN_TOTAL_MONTO, " +
                " FN_TOTAL_RECHAZADOS , FN_TOTAL_ACEPTADOS, IC_FOLIO_GTI, IG_ESTATUS ) " +
                " values (?,?,?,?,?,?,?,?,?,?) ";

            log.debug("querySentence " + querySentence);
            log.debug("String.valueOf(resultado.getFolio() " + String.valueOf(resultado.getFolio()));
            log.debug("resultado.getFecha() " + resultado.getFecha());
            log.debug("resultado.getHora() " + resultado.getHora());
            log.debug("aniomes " + aniomes);
            log.debug("numReg " + numReg);
            log.debug("montoReg " + montoReg.toPlainString());
            log.debug("totalRechazados " + totalRechazados);
            log.debug("totalAceptados " + totalAceptados);
            log.debug("in_enc " + in_enc);
            if (totalRechazados == numReg) {
                estatus = "3";
            }

            ps = con.queryPrecompilado(querySentence);
            ps.setString(1, in_enc);
            ps.setString(2, resultado.getFecha());
            ps.setString(3, resultado.getHora());
            ps.setString(4, aniomes);
            ps.setInt(5, numReg);
            ps.setBigDecimal(6, montoReg);
            ps.setInt(7, totalRechazados);
            ps.setInt(8, totalAceptados);
            ps.setString(9, String.valueOf(resultado.getFolio()));
            ps.setString(10, estatus);

            ps.execute();


            log.info("Salida:::GarantiasEJB.resumenCargaSaldosRegistrados");
        } catch (Exception e) {
            con.terminaTransaccion(false);
            log.info("GarantiasBean::resumenCargaSaldosRegistrados(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.terminaTransaccion(true);
            con.cierraConexionDB();
        }
    }

    public ResultadosGar reprocesarSaldosRegConError(String ic_folio, String ic_if) throws NafinException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null, ps2 = null;
        ResultSet rs = null;
        ResultadosGar retorno = new ResultadosGar();
        ResultadosGar retorno2 = new ResultadosGar();
        String querySentence = "", querySentence2 = "";
        String c_aniomes = "";
        VectorTokenizer vt = null;
        int linea = 0;
        boolean transaccion = true;
        int totalAceptados = 0;
        int totalRechazados = 0;
        String in_enc = "";
        try {
            con.conexionDB();
            BigDecimal folio = new BigDecimal(ic_folio);
            List clavesFinan = new ArrayList();

            retorno.setIcProceso(calculaIcProceso());

            //query para asociar folio generado en el reproceso con el folio principal
            querySentence2 = " update gti_estatus_solic set in_folio_reproceso = ? " + " where ic_folio = ? ";
            //se obtiene el anio y mes de la carga
            querySentence =
                " select to_char(df_fecha_corte,'yyyymm') aniomes from gti_estatus_solic " + " where ic_folio = ? ";
            ps = con.queryPrecompilado(querySentence);
            ps.setBigDecimal(1, folio);
            rs = ps.executeQuery();
            if (rs != null && rs.next())
                c_aniomes = rs.getString("aniomes");
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();

            //se obtiene registros que fueron rechazados en algun proceso
            querySentence =
                " select ic_if_siag, ic_linea, cg_contenido " + " from gti_arch_rechazo " + " where ic_folio = ? " +
                " order by ic_linea ";

            ps = con.queryPrecompilado(querySentence);
            ps.setBigDecimal(1, folio);
            rs = ps.executeQuery();

            //se realiza insert en tabla temporal de los registros que fueron rechazados en algun proceso
            while (rs != null && rs.next()) {
                vt = new VectorTokenizer(rs.getString("cg_contenido"), "@");
                Vector lovDatos = vt.getValuesVector();
                if (lovDatos != null && lovDatos.size() >= 7) {
                    linea++;
                    retorno.addSumRegOk(new BigDecimal(lovDatos.get(6).toString()));
                    clavesFinan.add(lovDatos.get(1));

                    querySentence =
                        " insert into gtitmp_contenido_arch(ic_proceso, ic_linea, cg_contenido) " + " values (?,?,?) ";
                    ps2 = con.queryPrecompilado(querySentence);
                    ps2.setInt(1, retorno.getIcProceso());
                    ps2.setInt(2, linea);
                    ps2.setString(3, rs.getString("cg_contenido"));

                    ps2.executeUpdate();
                    if (ps2 != null)
                        ps2.close();
                }

            } //finaliza while()
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();

            con.terminaTransaccion(true);
            retorno2 =
                transmitirCargaSaldos(String.valueOf(retorno.getIcProceso()), ic_if, String.valueOf(linea),
                                      String.valueOf(retorno.getSumRegOk()), "", c_aniomes, clavesFinan, "proc_aut",
                                      "reprocesar");
            resumenCargaSaldosRegistrados(retorno2, c_aniomes, linea, new BigDecimal(retorno.getSumRegOk()),
                                          totalAceptados, totalRechazados, in_enc);

            //se asocia folio principal con folios generados por reproceso
            ps2 = con.queryPrecompilado(querySentence2);
            ps2.setBigDecimal(1, new BigDecimal(retorno2.getFolio()));
            ps2.setBigDecimal(2, folio);
            ps2.executeUpdate();
            if (ps2 != null)
                ps2.close();
            linea = 0;

            retorno.setFolio(retorno2.getFolio());
            retorno.setFecha(retorno2.getFecha());
            retorno.setHora(retorno2.getHora());

        } catch (Exception e) {
            transaccion = false;
            e.printStackTrace();
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(transaccion);
                con.cierraConexionDB();
            }
        }
        return retorno;
    }

    public ResultadosGar transmitirCargaSaldos(String icProceso, String ic_if, String num_registros,
                                               String total_saldos, String total_pag, String aniomes, List clavesFinan,
                                               String ic_usuario) throws NafinException {


        return transmitirCargaSaldos(icProceso, ic_if, num_registros, total_saldos, total_pag, aniomes, clavesFinan,
                                     ic_usuario, "");

    }

    public ResultadosGar reprocesarAltaGarantiasRegConError(String ic_folio, String ic_if) throws NafinException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null, ps2 = null;
        ResultSet rs = null;
        ResultadosGar retorno = new ResultadosGar();
        String querySentence = "", querySentence2 = "";
        String c_aniomes = "";
        VectorTokenizer vt = null;
        int linea = 0;
        boolean transaccion = true;

        try {
            con.conexionDB();
            BigDecimal folio = new BigDecimal(ic_folio);

            retorno.setIcProceso(calculaIcProceso());

            //query para asociar folio generado en el reproceso con el folio principal
            querySentence2 = " update gti_estatus_solic set in_folio_reproceso = ? " + " where ic_folio = ? ";
            //se obtiene el anio y mes de la carga
            querySentence =
                " select to_char(df_fecha_corte,'yyyymm') aniomes from gti_estatus_solic " + " where ic_folio = ? ";
            ps = con.queryPrecompilado(querySentence);
            ps.setBigDecimal(1, folio);
            rs = ps.executeQuery();
            if (rs != null && rs.next())
                c_aniomes = rs.getString("aniomes");
            if (ps != null)
                ps.close();
            if (rs != null)
                rs.close();

            //se obtiene registros que fueron rechazados en algun proceso
            querySentence =
                "select ca.ic_if_siag ic_if_siag, ca.ic_linea ic_linea, ca.cg_contenido cg_contenido, ca.ig_programa ig_programa " +
                "from gti_contenido_arch ca, gti_arch_rechazo ar " + "where ca.ic_folio = ar.ic_folio " +
                "and ca.ic_if_siag = ar.ic_if_siag " + "and ca.ic_linea = ar.ic_linea " + "and ar.ic_folio = ? " +
                "order by ca.ic_linea ";


            ps = con.queryPrecompilado(querySentence);
            ps.setBigDecimal(1, folio);
            rs = ps.executeQuery();

            //se realiza insert en tabla temporal de los registros que fueron rechazados en algun proceso
            while (rs != null && rs.next()) {
                vt = new VectorTokenizer(rs.getString("cg_contenido"), "@");
                Vector lovDatos = vt.getValuesVector();

                if (lovDatos != null && lovDatos.size() >= 7) {
                    linea++;
                    retorno.incNumRegOk();
                    retorno.addSumRegOk(new BigDecimal(lovDatos.get(23).toString()));

                    querySentence =
                        " insert into gtitmp_contenido_arch(ic_proceso, ic_linea, cg_contenido, ig_programa) " +
                        " values (?,?,?,?) ";
                    ps2 = con.queryPrecompilado(querySentence);
                    ps2.setInt(1, retorno.getIcProceso());
                    ps2.setInt(2, linea);
                    ps2.setString(3, rs.getString("cg_contenido"));
                    ps2.setString(4, rs.getString("ig_programa"));

                    ps2.executeUpdate();
                    if (ps2 != null)
                        ps2.close();
                }

            } //finaliza while()
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();

            con.terminaTransaccion(true);


            ResultadosGar retornoTrasmitirAltaGarantia =
                this.transmitirAltaGarantia(String.valueOf(retorno.getIcProceso()), ic_if,
                                            String.valueOf(retorno.getNumRegOk()),
                                            String.valueOf(retorno.getSumRegOk()), "proc_aut");

            //se asocia folio principal con folios generados por reproceso
            ps2 = con.queryPrecompilado(querySentence2);
            ps2.setBigDecimal(1, new BigDecimal(retornoTrasmitirAltaGarantia.getFolio()));
            ps2.setBigDecimal(2, folio);
            ps2.executeUpdate();
            if (ps2 != null)
                ps2.close();
            linea = 0;

            retorno.setFolio(retornoTrasmitirAltaGarantia.getFolio());
            retorno.setFecha(retornoTrasmitirAltaGarantia.getFecha());
            retorno.setHora(retornoTrasmitirAltaGarantia.getHora());

        } catch (Exception e) {
            transaccion = false;
            e.printStackTrace();
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(transaccion);
                con.cierraConexionDB();
            }
        }
        return retorno;
    }

    /**
     * Validacion del telefono de la pyme en formato de 18 digitos por cada telefono.
     * (espacio 1 � 2 mas 10 digitos del telefono mas 6 digitos de la extension)
     * @param numLinea - numero de la linea que se esta validando
     * @param campo - valor que se validara
     * @param nombreCampo - nombre del campo a validar
     * @param ResultadosGar - objeto que guarda info de la validacion
     * @since F059-2009
     * @author Fabian Valenzuela/Gilberto Aparicio/Salim Hernandez
     */
    private void validarTelefonoPyme(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        try {

            log.info("validarTelefonoPyme(E)");

            final String TELEFONO_OFICINA = "1";
            final String TELEFONO_CELULAR = "2";

            int numTelefonos = campo.length() / 18;
            boolean hayTelefonoFijo = false;
            boolean hayTelefonoCelular = false;

            for (int i = 0; i < numTelefonos; i++) {

                String claveTipoTelefono = campo.substring(i * 18, i * 18 + 2);
                String telefono = campo.substring(i * 18 + 2, i * 18 + 12);
                String extension = campo.substring(i * 18 + 12, i * 18 + 18);

                // Suprimir los ' ' a la izquierda.
                claveTipoTelefono = claveTipoTelefono.replaceFirst("^[ ]+", "");
                telefono = telefono.replaceFirst("^[ ]+", "");
                extension = extension.replaceFirst("^[ ]+", "");

                // Revisar el tipo de telefono especificado
                if (TELEFONO_OFICINA.equals(claveTipoTelefono)) {
                    hayTelefonoFijo = true;
                } else if (TELEFONO_CELULAR.equals(claveTipoTelefono)) {
                    hayTelefonoCelular = true;
                } else {
                    throw new Exception("El identificador del telefono solo permite 1(Oficina) y 2(Celular)");
                }

                if (!claveTipoTelefono.matches("[\\d]+")) {
                    throw new Exception("Error de formato.");
                } else if (!telefono.matches("[\\d]+")) {
                    throw new Exception("Error de formato.");
                } else if (TELEFONO_CELULAR.equals(claveTipoTelefono) && !"".equals(extension)) {
                    throw new Exception("Para los telefonos celulares, se requiere que la extension venga en \"blanco\".");
                } else if (!extension.matches("[\\d]*")) {
                    throw new Exception("Error de formato.");
                }

            }

            if (!hayTelefonoFijo && !hayTelefonoCelular) {
                throw new Exception("Se debe registrar al menos un Tel�fono, ya sea de Oficina � Celular.");
            }

        } catch (Exception e) {
            log.error("validarTelefonoPyme(Exception)");
            rg.appendErrores("Error en la linea:" + numLinea + " el campo '" + nombreCampo + "' no es correcto. " +
                             e.getMessage() + "\n");
            rg.setErrorLinea(true);
        } finally {
            log.info("validarTelefonoPyme(S)");
        }

    }


    /**
     * Validacion del formato del correo electronico de la pyme.
     * @param numLinea - numero de la linea que se esta validando
     * @param campo - valor que se validara
     * @param nombreCampo - nombre del campo a validar
     * @param ResultadosGar - objeto que guarda info de la validacion
     * @since F059-2009
     * @author Fabian Valenzuela
     * @author Salim Hernandez ( 28/01/2013 06:04:05 p.m. )
     */
    private void validarCorreo(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {
        try {
            log.info("validarCorreo(E)");
            if (!Comunes.validaEmail(campo)) {
                throw new Exception();
            }
        } catch (Exception e) {
            log.error("validarCorreo(Exception)");
            rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo + "' no es valido  \n");
            rg.setErrorLinea(true);
        } finally {
            log.info("validarCorreo(S)");
        }
    }


    /**
     * Validacion de la dependencia.
     * @param numLinea - numero de la linea que se esta validando
     * @param campo - valor que se validara
     * @param nombreCampo - nombre del campo a validar
     * @param ResultadosGar - objeto que guarda info de la validacion
     * @since F025-2011
     * @author Salim Hernandez
     */
    private void validarDependencia(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        log.info("validarDependencia(E)");
        try {

            campo = campo.trim();
            Long.parseLong(campo);

        } catch (Exception e) {

            log.error("validarDependencia(Exception)");
            rg.appendErrores("Error en la linea:" + numLinea + " El campo '" + nombreCampo +
                             "' no es un numero valido \n");
            rg.setErrorLinea(true);

        } finally {
            log.info("validarDependencia(S)");
        }

    }

    public ResultadosGar transmitirDesembolsosMasivos(String icProceso, String ic_if, String num_registros,
                                                      String impte_total, String ic_usuario,
                                                      String reciboFirma) throws AppException {

        log.info("transmitirDesembolsosMasivos(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qrySentencia = "";
        boolean ok = true;

        try {

            con.conexionDB();

            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            String fecha = "";
            String fecha_valor = "";
            String hora = "";
            String anio = "";
            int ic_contragarante = 0;

            boolean usarCorreccionDeFecha = true;
            Registros reg = this.getDatosFolio(ic_if, usarCorreccionDeFecha, "6");

            // Obtener el Folio
            log.debug(" ic_if  " + ic_if);

            if (reg.next()) {
                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                fecha_valor = reg.getString("FECHA_VALOR");
                hora = reg.getString("hora");
                anio = reg.getString("anio");
                fecha_valor = (fecha_valor.equals(fecha)) ? fecha_valor + " " + hora : fecha_valor + " 00:00";
            }


            log.debug(" folio  " + folio.toPlainString());
            log.debug(" ic_nafin_electronico  " + ic_nafin_electronico.toPlainString());
            log.debug(" ic_if_siag  " + ic_if_siag);
            log.debug(" ic_nafin_electronico  " + ic_nafin_electronico.toPlainString());
            log.debug(" fecha  " + fecha);
            log.debug(" fecha_valor  " + fecha_valor);
            log.debug(" hora  " + hora);
            log.debug(" anio  " + anio);
            log.debug(" fecha_valor  " + fecha_valor);


            // OBTENER LA CLAVE DEL CONTRAGARANTE
            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF
            /*
			qrySentencia =
					" SELECT 						"  +
					"		s.ic_contragarante 	"	+
					" FROM 							"  +
					"		gti_servicios s 		"	+
					" WHERE 							"  +
					"		s.ic_servicio = 1		";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_contragarante = rs.getInt(1);
			}
			ps.close();
			*/
            // REGISTRAR EN El GTI_ESTATUS_SOLIC EL ENCABEZADO y N�MERO DE FOLIO DE LAS OPERACIONES
            // DE LA NUEVA TRANSACCI�N: (06) DESEMBOLSOS MASIVOS
            qrySentencia =
                " INSERT INTO 							" + "		gti_estatus_solic 			" + " 		( 									" + "			ic_folio,					" +
                "			ic_if_siag,					" + "			ic_nafin_electronico,	" + "			df_fecha_hora, 			" +
                " 			in_registros,				" + "			fn_impte_total,			" + "			ic_usuario_facultado,	" +
                "			ic_situacion, 				" + " 			ic_sit_transfer, 			" + "			ic_tipo_operacion, 		" +
                "			ic_contragarante, 		" + "			cg_recibo_firma,   		" + "			df_registro  				" + "		) 									" +
                " VALUES									" + "		(									" + "			?,								" + "			?,								" + "			?,								" +
                "			to_date(?,'dd/mm/yyyy HH24:MI'), " + "			?,								" + "			?,								" + " 			?,								" +
                "			?,								" + "			?,								" + "			?,								" + "			?,								" + "			?,								" +
                "			to_date(?,'dd/mm/yyyy HH24:MI')  " + "		) ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha_valor);
            ps.setString(5, num_registros);
            ps.setDouble(6, Double.parseDouble(impte_total));
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1); // SITUACION      				= 1: ENVIO PARA VALIDACION Y CARGA
            ps.setInt(9, 0); // SITUACION TRANSFERENCIA 	= 0: PENDIENTE DE ENVIAR
            ps.setInt(10, 6); // TIPO OPERACION 				= 6: DESEMBOLSOS MASIVOS
            ps.setInt(11, ic_contragarante);
            ps.setString(12, reciboFirma);
            log.info("---> reciboFirma = <" + reciboFirma + ">");
            ps.setString(13, fecha + " " + hora);
            ps.executeUpdate();

            // Imprmir mensaje de Depuracion
            log.debug("transmitirDesembolsosMasivos(" + _fechaMsg + "_" + icProceso +
                      ")::Insercion gti_estatus_solic...OK");

            // ALMACENAR CON UN MISMO N�MERO DE FOLIO EN LA TABLA GTI_CONTENIDO_ARCH EL DETALLE DEL ARCHIVO
            // ORIGINAL
            qrySentencia =
                " INSERT INTO 					" + "		gti_contenido_arch 	" + "		( 							" + "			ic_folio, 			" +
                "			ic_if_siag, 		" + "			ic_linea, 			" + "			ig_programa, 		" + "			cg_contenido, 		" +
                "			ic_if 				" + "		) 							" + " 		SELECT 					" + "			?, 					" + "			?, 					" +
                "			ic_linea, 			" + "			ig_programa, 		" + "			cg_contenido, 		" + "			? 						" + " 		FROM 						" +
                "			gtitmp_contenido_arch " + " 		WHERE 					" +
                "			ic_proceso = ? 	";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setInt(3, Integer.parseInt(ic_if));
            ps.setString(4, icProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();

            // Imprmir mensaje de Depuracion
            log.debug("transmitirDesembolsosMasivos(" + _fechaMsg + "_" + icProceso +
                      ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");

            // ALMACENAR EL DETALLE CORRESPONDIENTE A CADA L�NEA CON UN MISMO N�MERO DE FOLIO
            // EN LA TABLA	GTI_SOLICITUD_PAGO
            qrySentencia =
                "INSERT INTO 											" + "	gti_solicitud_pago 								" + "		( 													" +
                " 			ic_if_siag, 								" + "			ic_folio, 									" + "			cc_garantia, 								" +
                "			ic_nafin_electronico, 					" + "			df_fecha_hora, 							" + " 			df_incumplimiento, 						" +
                " 			cg_causas_incumplimiento, 				" + "			cg_funcionario_if, 						" +
                "			cg_tel_funcionario, 						" + " 			cg_ext_funcionario, 						" + "			cg_mail_funcionario, 					" +
                "			cg_localizacion_supervision, 			" + " 			cg_ciudad_supervision, 					" +
                "			cg_estado_supervision, 					" + "			ic_periodicidad_pago, 					" +
                "			df_fecha_segundo_incum,					" + "			fn_saldo_insoluto_estado_cuent,		" +
                "			fn_capital_vigente,						" + "			fn_capital_vencido,						" + "			fn_interes_ordinario,					" +
                "			fn_interes_moratorio,					" + "			df_fecha_ultimo_pago_capital,			" +
                "			df_fecha_ultimo_pago_interes,			" + "			fn_interes_recup_post_prim_inc,		" +
                "			ig_tasa_fondeo_nafin,					" + "			df_fecha_liquidacion_fondeo,			" +
                "			ig_num_prestamo_sirac,					" + "			cg_colonia_supervision,					" + "			ic_estado,									" +
                "			ic_municipio,								" + "			fn_monto_original_credito,				" + "			df_fecha_disposicion,					" +
                "			cs_fondeo_nafin,							" + "			cg_rfc_acreditado,						" + "			cg_nombre_acreditado,					" +
                "        ig_num_solicitud							" + " 		)  												" + " SELECT 												" +
                " 			?, 											" + // ic_if_siag
                "			?,												" + // folio
                "			cc_garantia,								" + "			?,												" + // ic_nafin_electronico
                "			to_date(?,'dd/mm/yyyy HH24:MI'), 	" + // fecha_valor
                " 			df_incumplimiento, 						" + " 			cg_causas_incumplimiento, 				" +
                "			cg_funcionario_if, 						" + "			cg_tel_funcionario, 						" + " 			cg_ext_funcionario, 						" +
                "			cg_mail_funcionario, 					" + "			cg_localizacion_supervision, 			" +
                " 			cg_ciudad_supervision, 					" + "			cg_estado_supervision, 					" +
                "			ic_periodicidad_pago, 					" + "			df_fecha_segundo_incum,					" +
                "			fn_saldo_insoluto_estado_cuent,		" + "			fn_capital_vigente,						" +
                "			fn_capital_vencido,						" + "			fn_interes_ordinario,					" + "			fn_interes_moratorio,					" +
                "			df_fecha_ultimo_pago_capital,			" + "			df_fecha_ultimo_pago_interes,			" +
                "			fn_interes_recup_post_prim_inc,		" + "			ig_tasa_fondeo_nafin,					" +
                "			df_fecha_liquidacion_fondeo,			" + "			ig_num_prestamo_sirac,					" +
                "			cg_colonia_supervision,					" + "			ic_estado,									" + "			ic_municipio,								" +
                "			fn_monto_original_credito,				" + "			df_fecha_disposicion,					" + "			cs_fondeo_nafin,							" +
                "			cg_rfc_acreditado,						" + "			cg_nombre_acreditado,					" + "        ig_num_solicitud							" +
                " 		FROM 												" + "			GTITMP_SOLICITUD_PAGO 					" + " 		WHERE 											" +
                "			ic_proceso = ? 							";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, ic_if_siag.intValue());
            ps.setBigDecimal(2, folio);
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha_valor);
            ps.setString(5, icProceso);

            int numRegistrosInsertadosSolicitudPago = ps.executeUpdate();
            ps.close();

            // Imprmir mensaje de Depuracion
            log.debug("transmitirDesembolsosMasivos(" + _fechaMsg + "_" + icProceso +
                      ")::Inserciones gtitmp_solicitud_pago ...OK(" + numRegistrosInsertadosSolicitudPago + ")");

            // BORRAR DE GTITMP_CONTENIDO_ARCH LA INFORMACION TEMPORAL
            qrySentencia = " DELETE FROM gtitmp_contenido_arch " + " WHERE IC_PROCESO = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();
            ps.close();

            // BORRAR DE GTITMP_SOLICITUD_PAGO LA INFORMACION TEMPORAL
            qrySentencia = " DELETE FROM gtitmp_solicitud_pago " + " WHERE IC_PROCESO = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);
            ps.execute();
            ps.close();

            // Establecer los valores de retorno
            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
            retorno.setFechaValor(fecha_valor);

        } catch (Exception e) {
            ok = false;
            log.error("transmitirDesembolsosMasivos(Exception)");
            log.debug("transmitirDesembolsosMasivos.icProceso     = <" + icProceso + ">");
            log.debug("transmitirDesembolsosMasivos.ic_if         = <" + ic_if + ">");
            log.debug("transmitirDesembolsosMasivos.num_registros = <" + num_registros + ">");
            log.debug("transmitirDesembolsosMasivos.impte_total   = <" + impte_total + ">");
            log.debug("transmitirDesembolsosMasivos.ic_usuario    = <" + ic_usuario + ">");
            log.debug("transmitirDesembolsosMasivos.reciboFirma   = <" + reciboFirma + ">");
            e.printStackTrace();
            throw new AppException("Error al transmitir el alta de Desembolsos Masivos", e);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            ;
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("transmitirDesembolsosMasivos(S)");
        }

        return retorno;
    }

    private int getInt(String valor) {
        return (valor == null ? 0 : Integer.parseInt(valor.trim()));
    }

    private String getCadena(String valor) {
        return (valor == null ? "" : valor);
    }

    private BigDecimal getBigDecimal(String valor) {
        return (valor == null ? new BigDecimal("0.00") : new BigDecimal(valor.trim()));
    }

    /**
     * M�todo que regresa los correos parametrizados para NAFIN e IF.
     * @return lista
     * @throws NafinException Excepcion lanzada cuando ocurre un error.
     * @since FODEA 048 - 2010 GTIAS - Parametrizaci�n y Notificaciones de Aviso.
     * @author JRSW
     */

    public HashMap getParametrosDeNotificacion(String claveIF, String claveTransaccion,
                                               String claveEstatus) throws NafinException {

        log.info("GarantiasBean::getParametrosDeNotificacion(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        String condicion = null;
        List lVarBind = new ArrayList();
        Registros registros = null;
        HashMap resultado = new HashMap();

        String cuentas = null;
        String[] cuentasDeCorreoNAFIN = null;
        String[] cuentasDeCorreoIF = null;

        try {

            log.info("claveIF: " + claveIF + ", claveTransaccion: " + claveTransaccion + ", claveEstatus: " +
                               claveEstatus);

            con.conexionDB();

            if (claveIF != null && !claveIF.equals("")) {
                condicion = " WHERE ic_if = ? ";
                lVarBind.add(new Integer(claveIF));

                if (claveTransaccion != null && !claveTransaccion.equals("")) {
                    condicion = condicion + " AND ic_tipo_operacion = ? ";
                    lVarBind.add(new Integer(claveTransaccion));
                } else {
                    condicion = condicion + " AND ic_tipo_operacion = ? ";
                    lVarBind.add(new Integer(0));
                }

                if (claveEstatus != null && !claveEstatus.equals("")) {
                    condicion = condicion + " AND ic_estatus_notif = ? ";
                    lVarBind.add(new Integer(claveEstatus));
                }

                qrySentencia =
                    "SELECT cg_mail_cuentas_nafin as cuentasNafin, " + " cg_mail_cuentas_if as cuentasIf " +
                    "FROM gti_correo_notif " + condicion;

                log.info("qrySentencia: " + qrySentencia);
                log.info("lVarBind: " + lVarBind);
                registros = con.consultarDB(qrySentencia, lVarBind, false);

                if (registros != null && registros.next()) {

                    cuentas = registros.getString("cuentasNafin");
                    cuentasDeCorreoNAFIN = cuentas.equals("") ? null : cuentas.split(",");
                    cuentas = registros.getString("cuentasIf");
                    cuentasDeCorreoIF = cuentas.equals("") ? null : cuentas.split(",");

                }

                resultado.put("cuentasNafin", cuentasDeCorreoNAFIN);
                resultado.put("cuentasIf", cuentasDeCorreoIF);
            }
        } catch (Exception e) {
            log.info("GarantiasBean::getParametrosDeNotificacion(Exception)");
            log.info("claveIF: " + claveIF + ", claveTransaccion:" + claveTransaccion + ", claveEstatus: " +
                               claveEstatus);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getParametrosDeNotificacion(S)");
        }
        return resultado;
    }

    /**
     * Este metodo inserta en la tabla GTI_CORREO_NOTIF y GTI_CUENTA_CORREO las cuentas de correo de NAFIN e IF
     * para la notificaci�n de operaciones Rechazadas, Autorizaci�n IF � Ambas seg�n corresponda al tipo de operaci�n.
     * @return
     * @throws NafinException Excepcion lanzada cuando ocurre un error.
     * @since FODEA 048 - 2010 GTIAS - Parametrizaci�n y Notificaciones de Aviso.
     * @author JRSW
     */
    public void setParametrosDeNotificacion(String claveIf, String claveTransaccion, String claveEstatus,
                                            String listaDeCuentasNAFIN, String listaDeCuentasIF) throws NafinException {

        log.info("GarantiasBean::setParametrosDeNotificacion(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        List lVarBind = new ArrayList();
        boolean lbOK = true;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            con.conexionDB();

            if (!existeRegistroDeIntermediario(claveIf, claveTransaccion, claveEstatus)) {

                qrySentencia =
                    "INSERT INTO gti_correo_notif " +
                    "(ic_if, ic_tipo_operacion, ic_estatus_notif, cg_mail_cuentas_nafin, cg_mail_cuentas_if" + ") " +
                    " VALUES (?, ?, ?, ?, ? " + ") ";

                lVarBind.add(new Integer(claveIf));
                lVarBind.add(new Integer(claveTransaccion));
                lVarBind.add(new Integer(claveEstatus));
                lVarBind.add(listaDeCuentasNAFIN);
                lVarBind.add(listaDeCuentasIF);

            } else {

                qrySentencia =
                    "UPDATE gti_correo_notif " + "SET cg_mail_cuentas_nafin = ?, " + " cg_mail_cuentas_if = ? " +
                    "WHERE " + "	ic_if = ? AND " + " ic_tipo_operacion = ? AND " + " ic_estatus_notif = ? ";

                lVarBind.add(listaDeCuentasNAFIN);
                lVarBind.add(listaDeCuentasIF);
                lVarBind.add(new Integer(claveIf));
                lVarBind.add(new Integer(claveTransaccion));
                lVarBind.add(new Integer(claveEstatus));

            }

            log.info("qrySentencia  = " + qrySentencia);
            con.ejecutaUpdateDB(qrySentencia, lVarBind);

            if (claveTransaccion.equals("0")) {
                qrySentencia = "";

                qrySentencia = "SELECT MAX (ic_tipo_operacion) AS maxtipo " + "	FROM gticat_tipooperacion";

                log.info("qrySentencia  = " + qrySentencia);
                ps = con.queryPrecompilado(qrySentencia.toString());
                rs = ps.executeQuery();

                int MaxTipo = 0;
                if (rs != null && rs.next()) {
                    MaxTipo = rs.getInt("maxtipo");
                    MaxTipo = MaxTipo + 1;
                }

                ps.close();
                rs.close();

                for (int i = 0; i < MaxTipo; i++) {

                    claveTransaccion = String.valueOf(i);
                    qrySentencia = "";
                    lVarBind = new ArrayList();

                    log.error("claveIf: " + claveIf + ", claveTransaccion: " + claveTransaccion +
                                       ", claveEstatus: " + claveEstatus);

                    if (!existeRegistroDeIntermediario(claveIf, claveTransaccion, claveEstatus)) {

                        qrySentencia =
                            "INSERT INTO gti_correo_notif " +
                            "(ic_if, ic_tipo_operacion, ic_estatus_notif, cg_mail_cuentas_nafin, cg_mail_cuentas_if" +
                            ") " + " VALUES (?, ?, ?, ?, ? " + ") ";

                        lVarBind.add(new Integer(claveIf));
                        lVarBind.add(new Integer(claveTransaccion));
                        lVarBind.add(new Integer(claveEstatus));
                        lVarBind.add(listaDeCuentasNAFIN);
                        lVarBind.add(listaDeCuentasIF);

                        log.info("qrySentencia  = " + qrySentencia);
                        con.ejecutaUpdateDB(qrySentencia, lVarBind);
                    } else {

                        qrySentencia =
                            "UPDATE gti_correo_notif " + "SET cg_mail_cuentas_nafin = ?, " +
                            " cg_mail_cuentas_if = ? " + "WHERE " + "	ic_if = ? AND " + " ic_tipo_operacion = ? AND " +
                            " ic_estatus_notif = ? ";

                        lVarBind.add(listaDeCuentasNAFIN);
                        lVarBind.add(listaDeCuentasIF);
                        lVarBind.add(new Integer(claveIf));
                        lVarBind.add(new Integer(claveTransaccion));
                        lVarBind.add(new Integer(claveEstatus));

                        log.info("qrySentencia  = " + qrySentencia);
                        con.ejecutaUpdateDB(qrySentencia, lVarBind);
                    }
                }
            }

        } catch (Exception e) {
            log.info("GarantiasBean::setParametrosDeNotificacion(Exception)");
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");

        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::setParametrosDeNotificacion(S)");
        }
    }

    /**
     * Este metodo se encarga de revisar si ya se habian parametrizado datos
     * dispersion para el IF seleccionado.
     */
    public boolean existeRegistroDeIntermediario(String cveIf, String cveTransaccion,
                                                 String cveEstatus) throws NafinException {

        log.info("GarantiasBean::existeRegistroDeIntermediario(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        List lVarBind = new ArrayList();
        Registros registros = null;
        boolean existeRegistro = false;

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT " + "	DECODE (COUNT (1), 0, 'false', 'true') AS existe_parametrizacion " + "FROM " +
                "	gti_correo_notif " + "WHERE " + "	ic_if = ? AND " + " ic_tipo_operacion = ? AND " +
                " ic_estatus_notif = ? ";

            lVarBind.add(cveIf);
            lVarBind.add(cveTransaccion);
            lVarBind.add(cveEstatus);

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            if (registros != null && registros.next()) {
                existeRegistro = registros.getString("existe_parametrizacion").equals("true") ? true : false;
            }

        } catch (Exception e) {
            log.info("GarantiasBean::existeRegistroDeIntermediario(Exception)");
            log.info("cveIf = " + cveIf);
            log.info("cveTransaccion = " + cveTransaccion);
            log.info("cveEstatus = " + cveEstatus);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::existeRegistroDeIntermediario(S)");
        }
        return existeRegistro;
    }

    /**
     * M�todo que se encarga de obtener de las tablas del SIAG la clave de contragarante
     * asociado a un intermediario financiero.
     * @param claveIf Clave interna del intermediario financiero.
     * @return claveContragarante Clave del contragarante.
     * @throws AppException Cuando ocurre un error en la consulta.
     */
    private int getClaveContragarante(String claveIf) throws AppException {
        log.info("getClaveContragarante(E) ::..");
        AccesoDB con = new AccesoDB();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer strSQL = new StringBuffer();
        List varBind = new ArrayList();
        boolean transactionOk = true;
        int claveContragarante = 0;
        try {
            con.conexionDB();

            strSQL.append(" SELECT fso_clave AS clave_contragarante");
            strSQL.append(" FROM siag_fiso_intermediarios sfi");
            strSQL.append(" , comcat_if cif");
            strSQL.append(" WHERE sfi.clave = cif.ic_if_siag");
            strSQL.append(" And cif.ic_if = ?");

            varBind.add(new Integer(claveIf));

            log.debug("..:: strSQL: " + strSQL.toString());
            log.debug("..:: varBind: " + varBind);

            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            rst = pst.executeQuery();
            if (rst.next()) {
                claveContragarante = rst.getInt("clave_contragarante");
            }
            rst.close();
            pst.close();
        } catch (Exception e) {
            transactionOk = false;
            e.printStackTrace();
            throw new AppException("getClaveContragarante(ERROR) ::..", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(transactionOk);
                con.cierraConexionDB();
            }
            log.info("getClaveContragarante(S) ::..");
        }
        return claveContragarante;
    }

    /**
     * M�todo que regresa la Parametrizaci�n de Campos para el Alta Autom�tica de Garant�as
     * @return lista
     * @throws NafinException Excepcion lanzada cuando ocurre un error.
     * @since FODEA 008 - 2011 GTIAS - Alta Param�tricos.
     * @author JRSW
     */

    public HashMap getCamposParametricos(String claveIF, String claveBO) throws NafinException {

        log.info("GarantiasBean::getCamposParametricos(E)");
        log.info("claveIF: " + claveIF + ", claveBO: " + claveBO);

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        String condicion = null;
        List lVarBind = new ArrayList();
        Registros registros = null;
        HashMap resultado = new HashMap();
        int countReg = 0;

        String solicFondeo = null;
        String solicPartiRiesgo = null;
        String porcentajeParti = null;
        String propositoProyec = null;
        String claveTipoCred = null;
        String claveGarantia = null;
        String porcenPartiFinan = null;
        String porcenProdMerInter = null;
        String porcenProdMerExter = null;
        String formaAmortizacion = null;
        String moneda = null;
        String numMesesGraciaFinan = null;
        String claveFuncionarioFaculIf = null;
        String tipoAutorizacion = null;
        String tipoPrograma = null;
        String tipoTasa = null;
        String plazoDias = null;
        String cgCalificacionFinal = null;
        String antiguedadClientMeses = null;

        try {
            con.conexionDB();

            if (claveIF != null && !claveIF.equals("")) {
                condicion = " WHERE ic_if = ? ";
                lVarBind.add(new Integer(claveIF));

                if (claveBO != null && !claveBO.equals("")) {
                    condicion = condicion + " AND ig_base_operacion = ? ";
                    lVarBind.add(new Integer(claveBO));
                }
            }

            qrySentencia =
                "SELECT cs_solic_fondeo, cs_solic_parti_riesgo, cg_porcentaje_parti, " +
                "cg_proposito_proyec, in_clave_tipo_cred, cg_clave_garantia, " +
                "cg_porcen_parti_finan, in_porcen_prod_mer_inter, " +
                "in_porcen_prod_mer_exter, in_forma_amortizacion, cg_modena, " +
                "in_num_meses_gracia_finan, in_clave_funcionario_facul_if, " +
                "in_tipo_autorizacion, in_tipo_programa, cs_tipo_tasa, in_plazo_dias, " +
                "cg_calificacion_final, in_antiguedad_client_meses " + "FROM com_param_garantias " + condicion;

            log.info("qrySentencia: " + qrySentencia);
            log.info("lVarBind: " + lVarBind);

            registros = con.consultarDB(qrySentencia, lVarBind, false);

            if (registros != null && registros.next()) {
                solicFondeo =
                    (registros.getString("cs_solic_fondeo")).equals(null) ? "" : registros.getString("cs_solic_fondeo");
                solicPartiRiesgo =
                    (registros.getString("cs_solic_parti_riesgo")).equals(null) ? "" :
                    registros.getString("cs_solic_parti_riesgo");
                porcentajeParti =
                    (registros.getString("cg_porcentaje_parti")).equals(null) ? "" :
                    registros.getString("cg_porcentaje_parti");
                propositoProyec =
                    (registros.getString("cg_proposito_proyec")).equals(null) ? "" :
                    registros.getString("cg_proposito_proyec");
                claveTipoCred =
                    (registros.getString("in_clave_tipo_cred")).equals(null) ? "" :
                    registros.getString("in_clave_tipo_cred");
                claveGarantia =
                    (registros.getString("cg_clave_garantia")).equals(null) ? "" :
                    registros.getString("cg_clave_garantia");
                porcenPartiFinan =
                    (registros.getString("cg_porcen_parti_finan")).equals(null) ? "" :
                    registros.getString("cg_porcen_parti_finan");
                porcenProdMerInter =
                    (registros.getString("in_porcen_prod_mer_inter")).equals(null) ? "" :
                    registros.getString("in_porcen_prod_mer_inter");
                porcenProdMerExter =
                    (registros.getString("in_porcen_prod_mer_exter")).equals(null) ? "" :
                    registros.getString("in_porcen_prod_mer_exter");
                formaAmortizacion =
                    (registros.getString("in_forma_amortizacion")).equals(null) ? "" :
                    registros.getString("in_forma_amortizacion");
                moneda = (registros.getString("cg_modena")).equals(null) ? "" : registros.getString("cg_modena");
                numMesesGraciaFinan =
                    (registros.getString("in_num_meses_gracia_finan")).equals(null) ? "" :
                    registros.getString("in_num_meses_gracia_finan");
                claveFuncionarioFaculIf =
                    (registros.getString("in_clave_funcionario_facul_if")).equals(null) ? "" :
                    registros.getString("in_clave_funcionario_facul_if");
                tipoAutorizacion =
                    (registros.getString("in_tipo_autorizacion")).equals(null) ? "" :
                    registros.getString("in_tipo_autorizacion");
                tipoPrograma =
                    (registros.getString("in_tipo_programa")).equals(null) ? "" :
                    registros.getString("in_tipo_programa");
                tipoTasa =
                    (registros.getString("cs_tipo_tasa")).equals(null) ? "" : registros.getString("cs_tipo_tasa");
                plazoDias =
                    (registros.getString("in_plazo_dias")).equals(null) ? "" : registros.getString("in_plazo_dias");
                cgCalificacionFinal =
                    (registros.getString("cg_calificacion_final")).equals(null) ? "" :
                    registros.getString("cg_calificacion_final");
                antiguedadClientMeses =
                    (registros.getString("in_antiguedad_client_meses")).equals(null) ? "" :
                    registros.getString("in_antiguedad_client_meses");
                countReg++;
            }

            resultado.put("solicFondeo", solicFondeo);
            resultado.put("solicPartiRiesgo", solicPartiRiesgo);
            resultado.put("porcentajeParti", porcentajeParti);
            resultado.put("propositoProyec", propositoProyec);
            resultado.put("claveTipoCred", claveTipoCred);
            resultado.put("claveGarantia", claveGarantia);
            resultado.put("porcenPartiFinan", porcenPartiFinan);
            resultado.put("porcenProdMerInter", porcenProdMerInter);
            resultado.put("porcenProdMerExter", porcenProdMerExter);
            resultado.put("formaAmortizacion", formaAmortizacion);
            resultado.put("moneda", moneda);
            resultado.put("numMesesGraciaFinan", numMesesGraciaFinan);
            resultado.put("claveFuncionarioFaculIf", claveFuncionarioFaculIf);
            resultado.put("tipoAutorizacion", tipoAutorizacion);
            resultado.put("tipoPrograma", tipoPrograma);
            resultado.put("tipoTasa", tipoTasa);
            resultado.put("plazoDias", plazoDias);
            resultado.put("cgCalificacionFinal", cgCalificacionFinal);
            resultado.put("antiguedadClientMeses", antiguedadClientMeses);
            resultado.put("numeroRegistros", Integer.toString(countReg));
        } catch (Exception e) {
            log.info("GarantiasBean::getCamposParametricos(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::getCamposParametricos(S)");
        }
        return resultado;
    }

    /**
     * M�todo que genera el combo de IF de la pantalla Parametrizaci�n de Campos para el Alta Autom�tica de Garant�as
     * @return comboIf Lista de objetos ElementoCatalogo con los IFs con alta de garant�as.
     * @throws AppException Cuando ocurre un error en la consulta.
     */
    public List getComboIf() throws AppException {
        log.info("getComboIf(E) ::..");
        AccesoDB con = new AccesoDB();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer strSQL = new StringBuffer();
        List varBind = new ArrayList();
        List comboIf = new ArrayList();

        try {
            con.conexionDB();

            strSQL.append(" SELECT DISTINCT cboc.ic_if AS clave, ");
            strSQL.append(" ci.cg_razon_social AS descripcion");
            strSQL.append(" FROM com_base_op_credito cboc, comcat_if ci");
            strSQL.append(" WHERE cs_alta_automatica_garantia = ?");
            strSQL.append(" AND cboc.ic_if = ci.ic_if");
            strSQL.append(" ORDER BY ci.cg_razon_social");

            varBind.add("S");

            log.debug("..:: strSQL: " + strSQL.toString());
            log.debug("..:: varBind: " + varBind);

            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            rst = pst.executeQuery();

            while (rst.next()) {
                ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
                elementoCatalogo.setClave(rst.getString("clave") == null ? "" : rst.getString("clave"));
                elementoCatalogo.setDescripcion(rst.getString("descripcion") == null ? "" :
                                                rst.getString("descripcion"));
                comboIf.add(elementoCatalogo);
            }

            rst.close();
            pst.close();
        } catch (Exception e) {
            log.info("getComboIf(ERROR) ::..");
            throw new AppException("Error al consultar base de datos: ", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getComboIf(S) ::..");
        }
        return comboIf;
    }

    /**
     * M�todo que genera el combo de Bases de Operaci�n de la pantalla
     * Parametrizaci�n de Campos para el Alta Autom�tica de Garant�as
     * @return comboBO Lista de objetos ElementoCatalogo con las Bases de Operaci�n
     * del IF seleccionado.
     * @throws AppException Cuando ocurre un error en la consulta.
     */
    public List getComboBO(String claveIf) throws AppException {
        log.info("getComboBO(E) ::..");
        AccesoDB con = new AccesoDB();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuffer strSQL = new StringBuffer();
        List varBind = new ArrayList();
        List comboBO = new ArrayList();

        try {
            con.conexionDB();

            strSQL.append(" SELECT DISTINCT cbo.ig_codigo_base AS clave, ");
            strSQL.append(" 			   cbo.cg_descripcion AS descripcion ");
            strSQL.append("     FROM comcat_base_operacion cbo, com_base_op_credito cboc ");
            strSQL.append("    WHERE cbo.ig_codigo_base = cboc.ig_codigo_base ");
            strSQL.append("      AND cboc.cs_alta_automatica_garantia = ? ");
            strSQL.append("      AND cboc.ic_if = ? ");
            strSQL.append(" ORDER BY cbo.cg_descripcion ");

            varBind.add("S");
            varBind.add(new Integer(claveIf));

            log.debug("..:: strSQL: " + strSQL.toString());
            log.debug("..:: varBind: " + varBind);

            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            rst = pst.executeQuery();

            while (rst.next()) {
                ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
                elementoCatalogo.setClave(rst.getString("clave") == null ? "" : rst.getString("clave"));
                elementoCatalogo.setDescripcion((rst.getString("clave") == null ? "" : rst.getString("clave")) + " - " +
                                                (rst.getString("descripcion") == null ? "" :
                                                 rst.getString("descripcion")));
                comboBO.add(elementoCatalogo);
            }

            rst.close();
            pst.close();
        } catch (Exception e) {
            log.info("getComboBO(ERROR) ::..");
            throw new AppException("Error al consultar base de datos: ", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getComboBO(S) ::..");
        }
        return comboBO;
    }

    /**
     * Este metodo inserta/actualiza en la tabla COM_PARAM_GARANTIAS los Campos de la pantalla
     * Parametrizaci�n de Campos para el Alta Autom�tica de Garant�as.
     * @return
     * @throws NafinException Excepcion lanzada cuando ocurre un error.
     * @since FODEA 008 - 2011 GTIAS - Alta Parametricos.
     * @author JRSW
     */
    public void setcamposParam(String claveIf, String claveBO, String csSolicFondeo, String csSolicPartiRiesgo,
                               String cgPorcentajeParti, String cgPropositoProyec, String inClaveTipoCred,
                               String cgClaveGarantia, String cgPorcenPartiFinan, String inPorcenProdMerInter,
                               String inPorcenProdMerExter, String inFormaAmortizacion, String cgMoneda,
                               String inNumMesesGraciaFinan, String inClaveFuncionarioFaculIf,
                               String inTipoAutorizacion, String inTipoPrograma, String csTipoTasa, String inPlazoDias,
                               String cgCalificacionFinal, String inAntiguedadClientMeses) throws NafinException {

        log.info("GarantiasBean::setcamposParam(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        boolean lbOK = true;
        PreparedStatement ps = null;

        try {
            con.conexionDB();

            if (!existeIntermediarioParam(claveIf, claveBO)) {

                qrySentencia =
                    "INSERT INTO com_param_garantias " + "(ic_if, ig_base_operacion, cs_solic_fondeo, " +
                    " cs_solic_parti_riesgo, cg_porcentaje_parti, cg_proposito_proyec, " +
                    " in_clave_tipo_cred, cg_clave_garantia, cg_porcen_parti_finan, " +
                    " in_porcen_prod_mer_inter, in_porcen_prod_mer_exter, " +
                    " in_forma_amortizacion, cg_modena, in_num_meses_gracia_finan, " +
                    " in_clave_funcionario_facul_if, in_tipo_autorizacion, " +
                    " in_tipo_programa, cs_tipo_tasa, in_plazo_dias, " +
                    " cg_calificacion_final, in_antiguedad_client_meses " + " ) " + "VALUES (?, ?, ?, ?, ?, ?, ?, " +
                    " 			 ?, ?, ?, ?, ?, ?, ?, " +
                    " 			 ?, ?, ?, ?, ?, ?, ?)";

                ps = con.queryPrecompilado(qrySentencia);

                ps.setInt(1, claveIf.equals("") ? 0 : Integer.parseInt(claveIf));
                ps.setInt(2, claveBO.equals("") ? 0 : Integer.parseInt(claveBO));
                ps.setString(3, csSolicFondeo);
                ps.setString(4, csSolicPartiRiesgo);
                ps.setString(5, cgPorcentajeParti);
                ps.setString(6, cgPropositoProyec);
                ps.setInt(7, inClaveTipoCred.equals("") ? 0 : Integer.parseInt(inClaveTipoCred));
                ps.setString(8, cgClaveGarantia);
                ps.setString(9, cgPorcenPartiFinan);
                ps.setString(10, inPorcenProdMerInter);
                ps.setString(11, inPorcenProdMerExter);
                ps.setInt(12, inFormaAmortizacion.equals("") ? 0 : Integer.parseInt(inFormaAmortizacion));
                ps.setString(13, cgMoneda);
                ps.setInt(14, inNumMesesGraciaFinan.equals("") ? 0 : Integer.parseInt(inNumMesesGraciaFinan));
                ps.setInt(15, inClaveFuncionarioFaculIf.equals("") ? 0 : Integer.parseInt(inClaveFuncionarioFaculIf));
                ps.setInt(16, inTipoAutorizacion.equals("") ? 0 : Integer.parseInt(inTipoAutorizacion));
                ps.setInt(17, inTipoPrograma.equals("") ? 0 : Integer.parseInt(inTipoPrograma));
                ps.setString(18, csTipoTasa);
                ps.setInt(19, inPlazoDias.equals("") ? 0 : Integer.parseInt(inPlazoDias));
                ps.setString(20, cgCalificacionFinal);
                ps.setInt(21, inAntiguedadClientMeses.equals("") ? 0 : Integer.parseInt(inAntiguedadClientMeses));
            } else {
                qrySentencia =
                    "UPDATE com_param_garantias " + " SET cs_solic_fondeo = ?, " + " cs_solic_parti_riesgo = ?, " +
                    " cg_porcentaje_parti = ?, " + " cg_proposito_proyec = ?, " + " in_clave_tipo_cred = ?, " +
                    " cg_clave_garantia = ?, " + " cg_porcen_parti_finan = ?, " + " in_porcen_prod_mer_inter = ?, " +
                    " in_porcen_prod_mer_exter = ?, " + " in_forma_amortizacion = ?, " + " cg_modena = ?, " +
                    " in_num_meses_gracia_finan = ?, " + " in_clave_funcionario_facul_if = ?, " +
                    " in_tipo_autorizacion = ?, " + " in_tipo_programa = ?, " + " cs_tipo_tasa = ?, " +
                    " in_plazo_dias = ?, " + " cg_calificacion_final = ?, " + " in_antiguedad_client_meses = ? " +
                    " WHERE ic_if = ? AND ig_base_operacion = ?";

                ps = con.queryPrecompilado(qrySentencia);

                ps.setString(1, csSolicFondeo);
                ps.setString(2, csSolicPartiRiesgo);
                ps.setString(3, cgPorcentajeParti);
                ps.setString(4, cgPropositoProyec);
                ps.setInt(5, inClaveTipoCred.equals("") ? 0 : Integer.parseInt(inClaveTipoCred));
                ps.setString(6, cgClaveGarantia);
                ps.setString(7, cgPorcenPartiFinan);
                ps.setString(8, inPorcenProdMerInter);
                ps.setString(9, inPorcenProdMerExter);
                ps.setInt(10, inFormaAmortizacion.equals("") ? 0 : Integer.parseInt(inFormaAmortizacion));
                ps.setString(11, cgMoneda);
                ps.setInt(12, inNumMesesGraciaFinan.equals("") ? 0 : Integer.parseInt(inNumMesesGraciaFinan));
                ps.setInt(13, inClaveFuncionarioFaculIf.equals("") ? 0 : Integer.parseInt(inClaveFuncionarioFaculIf));
                ps.setInt(14, inTipoAutorizacion.equals("") ? 0 : Integer.parseInt(inTipoAutorizacion));
                ps.setInt(15, inTipoPrograma.equals("") ? 0 : Integer.parseInt(inTipoPrograma));
                ps.setString(16, csTipoTasa);
                ps.setInt(17, inPlazoDias.equals("") ? 0 : Integer.parseInt(inPlazoDias));
                ps.setString(18, cgCalificacionFinal);
                ps.setInt(19, inAntiguedadClientMeses.equals("") ? 0 : Integer.parseInt(inAntiguedadClientMeses));
                ps.setInt(20, claveIf.equals("") ? 0 : Integer.parseInt(claveIf));
                ps.setInt(21, claveBO.equals("") ? 0 : Integer.parseInt(claveBO));
            }
            log.info("qrySentencia  = " + qrySentencia);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            log.info("GarantiasBean::setcamposParam(Exception)");
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");

        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::setcamposParam(S)");
        }
    }

    /**
     * Este metodo se encarga de revisar si ya se habian parametrizado datos
     * de garant�as para el Alta de campos parametricos. FODEA 008-2011 JRSW
     */
    public boolean existeIntermediarioParam(String cveIf, String cveBO) throws NafinException {

        log.info("GarantiasBean::existeIntermediarioParam(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        List lVarBind = new ArrayList();
        Registros registros = null;
        boolean existeRegistro = false;

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT DECODE (COUNT (1), 0, 'false', 'true') AS existe_parametrizacion " +
                " FROM com_param_garantias " + " WHERE ic_if = ? AND ig_base_operacion = ? ";

            lVarBind.add(cveIf);
            lVarBind.add(cveBO);

            log.info("qrySentencia  = " + qrySentencia);
            log.info("lVarBind  = " + lVarBind);

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            if (registros != null && registros.next()) {
                existeRegistro = registros.getString("existe_parametrizacion").equals("true") ? true : false;
            }

        } catch (Exception e) {
            log.info("GarantiasBean::existeIntermediarioParam(Exception)");
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("GarantiasBean::existeIntermediarioParam(S)");
        }
        return existeRegistro;
    }

    /**
     *  Esta funcion devuelve las Reglas de Validacion de Parametros para todos los programas con los que trabaja
     *  el IF.
     *
     *  @throws netropology.utilerias.AppException
     *  @param 	claveIF <tt>String</tt> con la clave del IF
     *  @since  Fodea 025 - 2011
     *	 @return Un objeto de tipo <tt>ReglasProgramaEmpleo</tt> con las reglas de validacion.
     *  @author JSHD
     */
    public ReglasPrograma getReglasValidacionParametrosPorPrograma(String claveIF) throws AppException {

        log.info("getReglasValidacionParametrosPorPrograma(E)");
        ReglasPrograma reglas = new ReglasPrograma();

        if (claveIF == null || claveIF.trim().equals("")) {
            log.info("getReglasValidacionParametrosPorPrograma(E)");
            return reglas;
        }

        AccesoDB con = new AccesoDB();
        StringBuffer qrySentencia = new StringBuffer();
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean programaGeneraEmpleoParametrizado = false;
        boolean programaSHFCreditoHipotecario = false;
        boolean programaCreditoEducativoParametrizado = false;
        try {

            con.conexionDB();

            // 1. Revisar si el programa generar empleo esta parametrizado en SIAG
            programaGeneraEmpleoParametrizado = false;
            // Nota F024 - 2013. Se deshabilita el Programa Genera Empleo.
            /*
			qrySentencia.append(
				"SELECT 																	"  +
				"	IF_SIAG.CS_GENERA_EMPLEO AS PROGRAMA_PARAMETRIZADO		"  +
				"FROM 																	"  +
				"	COMCAT_IF INTERMEDIARIO, 										"  +
				"	GTI_IF_SIAG IF_SIAG 												"  +
				"WHERE 																	"  +
				"	INTERMEDIARIO.IC_IF       		= ? 							"  +
				"	AND INTERMEDIARIO.IC_IF_SIAG 	= IF_SIAG.IC_IF_SIAG 	"
			);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,Integer.parseInt(claveIF));
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				programaGeneraEmpleoParametrizado = "S".equals(rs.getString("PROGRAMA_PARAMETRIZADO"))?true:false;
			}

			rs.close();
			ps.close();
			*/
            reglas.setProgramaGeneraEmpleoParametrizado(programaGeneraEmpleoParametrizado);

            HashMap programasParametrosIMSSHabilitados =
                new HashMap(); // GTI_PARAMETROS_PROGRAMAS.CS_GENERA_EMPLEO = 'S'
            // Nota F024 - 2013. Se deshabilita el Programa Genera Empleo.
            /*
			if(programaGeneraEmpleoParametrizado){

				qrySentencia.setLength(0);
				qrySentencia.append(
					"SELECT 									"  +
					"	IC_PROGRAMA AS CLAVE_PROGRAMA	"  +
					"FROM 									"  +
					"	GTI_PARAMETROS_PROGRAMAS 		"  +
					"WHERE 									"  +
					"	CS_GENERA_EMPLEO = ?				" // 'S'
				);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setString(1,"S");
				rs = ps.executeQuery();

				while(rs != null && rs.next()){
					String clavePrograma = rs.getString("CLAVE_PROGRAMA");
					programasParametrosIMSSHabilitados.put(String.valueOf(clavePrograma),"true");
				}

				rs.close();
				ps.close();
			}
			*/
            reglas.setProgramasParametrosIMSSHabilitados(programasParametrosIMSSHabilitados);

            // 2. Revisar si el programa de la SHF "Credito Hipotecario" esta parametrizado en SIAG
            qrySentencia.setLength(0);
            qrySentencia.append("SELECT 																	" + "	IF_SIAG.CS_SHF AS PROGRAMA_PARAMETRIZADO					" +
                                "FROM 																	" + "	COMCAT_IF INTERMEDIARIO, 										" +
                                "	GTI_IF_SIAG IF_SIAG 												" + "WHERE 																	" +
                                "	INTERMEDIARIO.IC_IF       		= ? 							" +
                                "	AND INTERMEDIARIO.IC_IF_SIAG 	= IF_SIAG.IC_IF_SIAG 	");
            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            if (rs != null && rs.next()) {
                programaSHFCreditoHipotecario = "S".equals(rs.getString("PROGRAMA_PARAMETRIZADO")) ? true : false;
            }
            rs.close();
            ps.close();

            reglas.setProgramaSHFCreditoHipotecario(programaSHFCreditoHipotecario);

            // 3. Revisar si el programa Credito Educativo est� parametrizado (de forma global) en SIAG
            // ( Esto es para se coherente con la estructura de la parametrizacion de los otros programas )
            // Nota: Se asume por default que el programa credito educativo siempre estar� parametrizado en el SIAG.
            programaCreditoEducativoParametrizado = true;
            reglas.setProgramaCreditoEducativoParametrizado(programaCreditoEducativoParametrizado);

            // Obtener todas las Claves de los Programas (Campo 34 - Tipo Programa) que operan con Credito Educativo
            HashMap programasParametroCreditoEducativoHabilitado = new HashMap();
            if (programaGeneraEmpleoParametrizado) {

                qrySentencia.setLength(0);
                qrySentencia.append("SELECT                                " +
                                    "  IC_CLAVE AS CLAVE_PROGRAMA          " +
                                    "FROM                                  " +
                                    "  CECAT_BASES_OPERA                   " +
                                    "WHERE                                 " +
                                    "  CS_OPERA_CREDITO = 'S'              ");
                ps = con.queryPrecompilado(qrySentencia.toString());
                rs = ps.executeQuery();

                while (rs != null && rs.next()) {
                    String clavePrograma = rs.getString("CLAVE_PROGRAMA");
                    programasParametroCreditoEducativoHabilitado.put(String.valueOf(clavePrograma), "true");
                }

            }

            reglas.setProgramasParametroCreditoEducativoHabilitado(programasParametroCreditoEducativoHabilitado);

        } catch (Exception e) {
            log.error("getReglasValidacionParametrosPorPrograma(Exception)");
            log.error("getReglasValidacionParametrosPorPrograma.claveIF = <" + claveIF + ">");
            e.printStackTrace();
            throw new AppException("Ocurrio un Error al obtener las Reglas de Validacion de Parametros.");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getReglasValidacionParametrosPorPrograma(S)");
        }

        return reglas;

    }

    /**
     * Revisa si el String proporcionado en el parametro: <tt>campo</tt> viene vacio.
     * @param campo <tt>String</tt> a validar.
     * @return <tt>boolean</tt> con valor <tt>true</tt> si el campo viene vacio o tiene espacios en blanco;
     *         regresa <tt>false</tt> en caso contrario.
     * @since Fodea 025 - 2011
     * @author Salim Hernandez
     */
    private boolean esCampoVacio(String campo) {
        log.info("esCampoVacio(E)");
        boolean resultado = false;
        if (campo == null || campo.trim().equals("")) {
            resultado = true;
        }
        log.info("esCampoVacio(S)");
        return resultado;
    }

    /**
     * Revisa si el String proporcionado en el parametro: <tt>campo</tt> viene vacio.
     * @param campo <tt>String</tt> a validar.
     * @return <tt>boolean</tt> con valor <tt>true</tt> si el campo viene vacio o tiene espacios en blanco;
     *         regresa <tt>false</tt> en caso contrario.
     * @since Fodea 025 - 2011
     * @author Salim Hernandez
     */
    private boolean esCampoVacioV2(String campo) {
        log.info("esCampoVacio(E)");
        boolean resultado = false;
        if (campo == null || campo.matches("\\s*")) {
            resultado = true;
        }
        log.info("esCampoVacio(S)");
        return resultado;
    }

    /**
     *
     * Revisa si el String proporcionado en el parametro: <tt>campo</tt> corresponde a un espacio en blanco o
     * a una cadena vac�a.
     *
     * @param campo <tt>String</tt> a validar.
     * @return <tt>boolean</tt> con valor <tt>true</tt> si se trata de un campo vac�o;
     *         regresa <tt>false</tt> en caso contrario.
     *
     * @since Fodea 004 - 2013 ( 11/03/2013 06:19:12 p.m. )
     * @author Salim Hernandez
     *
     */
    private boolean esVacio(String campo) {
        log.info("esVacio(E)");
        boolean espacioEnBlanco = false;
        if (campo == null || campo.matches("\\s*")) {
            espacioEnBlanco = true;
        }
        log.info("esVacio(S)");
        return espacioEnBlanco;
    }


    /** Este metodo valida el Tipo de Persona proporcionado en el parametro: <tt>tipoPersona</tt>.
     *  Las posibles errores a reportar son:
     *  3. El campo Tipo de Persona no es un valor valido. Valor esperado "F" � "M"
     * @param campo <tt>String</tt> con la clave del Tipo de Persona.
     * @since Fodea 025 - 2011
     * @author Salim Hernandez
     * @author Salim Hernandez; F024-2013 (17/07/2013 11:33:35 a.m.)
     */
    private boolean validaTipoPersona(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        log.info("validaTipoPersona(E)");

        String msg_error = "";
        boolean hayError = false;

        if (!"F".equals(campo) && !"M".equals(campo)) {
            msg_error = "El campo '" + nombreCampo + "' no es un valor v�lido. Valor esperado \"F\" � \"M\". ";
            hayError = true;
        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validaTipoPersona(S)");
        return !hayError;

    }

    /**
     *
     * Este metodo valida el G�nero del Acreditado proporcionado en el parametro: <tt>campo</tt>.
     * El error reportado es:
     *    El campo G�nero del Acreditado es incorrecto. Valor esperado "H" (Hombre) � "M" (Mujer).
     *
     * @param numLinea       <tt>int</tt> con el n�mero de la l�nea.
     * @param campo          <tt>String</tt> con la clave del G�nero del Acreditado.
     * @param nombreCampo    <tt>String</tt> con el nombre del campo.
     * @param ResultadosGar  objeto que guarda info de la validaci�n.
     *
     * @return <tt>true</tt> si la validacion es exitosa y <tt>false</tt> en caso contrario.
     *
     * @since Fodea 024 - 2013
     * @author jshernandez ( 16/07/2013 04:15:44 p.m. )
     *
     */
    private boolean validaGeneroAcreditado(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        log.info("validaGeneroAcreditado(E)");

        String msg_error = "";
        boolean hayError = false;

        if (!"H".equals(campo) && !"M".equals(campo)) {
            msg_error =
                "El campo '" + nombreCampo + "' es incorrecto. Valor esperado: \"H\" (Hombre) � \"M\" (Mujer). ";
            hayError = true;
        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validaGeneroAcreditado(S)");

        return !hayError;

    }

    /**
     * Revisa si el String proporcionado en el parametro: <tt>campo</tt> hace referencia a
     * una persona fisica.
     * @param campo <tt>String</tt> a validar.
     * @return <tt>boolean</tt> con valor <tt>true</tt> si es una persona fisica;
     *         regresa <tt>false</tt> en caso contrario.
     * @since Fodea 025 - 2011
     * @author Salim Hernandez
     */
    private boolean esPersonaFisica(String campo) {
        log.info("esPersonaFisica(E)");
        boolean resultado = false;
        if ("F".equals(campo)) {
            resultado = true;
        }
        log.info("esPersonaFisica(S)");
        return resultado;
    }

    /**
     * Este metodo valida la CURP proporcionada en el parametro: <tt>curp</tt>.
     * Las posibles errores a reportar son:
     * 1. Campo obligatorio �nicamente para personas F�sicas.
     * 2. La Longitud de la CURP es incorrecta. Longitud esperada "18".
     * @param campo <tt>String</tt> con la clave CURP.
     * @return <tt>true</tt> si la validacion es exitosa, y <tt>false</tt> en caso contrario.
     * @since Fodea 025 - 2011
     * @author Salim Hernandez
     */
    private boolean validaCURP(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                               boolean personaFisica) {

        log.info("validaCURP(E)");

        String msg_error = "";
        boolean hayError = false;

        boolean campoVacio = esCampoVacio(campo);

        if (!personaFisica && !campoVacio) {
            msg_error = "El campo " + nombreCampo + " s�lo es requerido para personas F�sicas ";
            hayError = true;
        } else if (personaFisica && campoVacio) {
            msg_error = "El campo " + nombreCampo + " es obligatorio";
            hayError = true;
        } else if (personaFisica && !campoVacio && campo.length() != 18) {
            msg_error = "La longitud del campo " + nombreCampo + " es incorrecta. Longitud esperada \"18\" ";
            hayError = true;
        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validaCURP(S)");
        return !hayError;

    }

    /**
     * Este metodo valida que los primeros 10 caracteres del RFC proporcionado coincida con los primeros
     * diez caracteres de la CURP.
     * @param numLinea <tt>int</tt> con el numero de la linea que se est� validando.
     * @param campo <tt>String</tt> con la clave CURP.
     * @param nombreCampo <tt>String</tt> con el nombre del campo.
     * @param rg <tt>ResultadosGar</tt> objeto que guarda el resultado de la validacion.
     * @param rfc <tt>String</tt> con la clave del RFC.
     * @return <tt>true</tt> si la validacion es exitosa, y <tt>false</tt> en caso contrario.
     * @since Fodea ### - 2013
     * @author Salim Hernandez
     */
    private boolean validaCURPContraRFC(int numLinea, String campo, String nombreCampo, ResultadosGar rg, String rfc) {

        log.info("validaCURPContraRFC(E)");

        String msg_error = "";
        boolean hayError = false;

        if (campo != null && campo.length() >= 10) { // Se cumple la condicion para realizar esta validacion

            if (rfc == null || rfc.length() < 10) {

                msg_error = "Los 10 primeros caracteres de la CURP no coinciden con los del RFC ";
                hayError = true;

            } else {

                String subRFC = rfc.substring(0, 10);
                String subCURP = campo.substring(0, 10);
                if (!subCURP.equals(subRFC)) {
                    msg_error = "Los 10 primeros caracteres de la CURP no coinciden con los del RFC ";
                    hayError = true;
                }

            }

        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validaCURPContraRFC(S)");

        return !hayError;

    }

    /**
     *
     * Este m�todo valida que el valor del campo proporcionado venga vac�o:
     * con cadena vac�a o con espacios en blanco.
     *
     * @param campo <tt>String</tt> con el contenido del campo a validar.
     * @since Fodea 003,004 - 2013 ( 12/03/2013 03:54:32 p.m. )
     * @author Salim Hernandez
     *
     */
    private void validarCampoRequeridoVacio(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        log.info("validarCampoRequeridoVacio(E)");

        String msg_error = "";
        boolean hayError = false;

        if (campo != null && !campo.matches("\\s*")) {
            msg_error = "El campo " + nombreCampo + " no requiere informaci�n, por lo que debe venir vac�o. ";
            hayError = true;
        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validarCampoRequeridoVacio(S)");

    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ*/

    /*>>>>>>>>>>Fodea 028-2011*/
    public ResultadosGar procesarDesembolsosMasivos() throws NafinException {

        log.info(" procesarDesembolsosMasivos (E)");

        AccesoDB con = new AccesoDB();
        ResultadosGar retorno = new ResultadosGar();
        ResultadosGar retorno2 = new ResultadosGar();
        PreparedStatement ps = null;
        ResultadosGar retorno1 = new ResultadosGar();

        ResultSet rs = null;
        ResultSet rs2 = null;
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        String qrySentencia = "";
        String qrySentenciaDetalle = "";
        PreparedStatement psD = null;
        PreparedStatement psE = null;
        PreparedStatement psF = null;
        PreparedStatement psZ = null;
        PreparedStatement psX = null;
        ResultSet rsF = null;
        ResultSet rsI = null;

        PreparedStatement psH = null;
        PreparedStatement psJ = null;
        ResultSet rsH = null;

        PreparedStatement psL = null;
        ResultSet rsL = null;

        try {

            VectorTokenizer vt = null;
            Vector lovDatos = null;
            int ifSiag = 0;
            String ic_if = "0";
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            int reg = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);
            List clavesFinan = new ArrayList();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            int iNumRegXproceso = 0;
            String s_contenido = "";
            String ic_det = "";
            int totalRechazados = 0;
            int totalAceptados = 0;
            int totalRegistros = 0;
            String claveFinancia = "";
            String d_aniomes = "";
            String in_enc = "";
            int ic_enc = 0;
            CallableStatement cs = null;
            List datos = new ArrayList();
            List registros = new ArrayList();

            con.conexionDB();

            /*  Procedimiento almacenado que toma la informaci�n resultado de las tablas del SIAG
			para reportar al FIDE los errores en el   procesamiento de los envios de Saldos*/
            cs = con.ejecutaSP("SP_ACT_ERR_FIDE()");
            try {
                cs.execute();
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                throw new NafinException("SIST0001" + sqle);
            }
            if (cs != null)
                cs.close();
            con.terminaTransaccion(true);

            //OBTIENE EL NUMERO DE FOLIO
            retorno1.setIcProceso(calculaIcProceso());
            log.debug("retorno1.getIcProceso()>>>>>>>>>>>>>>>>>>>>>" + retorno1.getIcProceso());

            // SE OBTIENEN TODOS LOS ACUSES YA LEIDOS POR EL FIDE PARA PROCEDER A SU DEPURACION
            qrySentencia =
                " select ic_folio from  COM_PROC_ACUSE_FIDE " + " WHERE IC_PROCESO = ? " + " AND IG_ESTATUS = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, 2);
            ps.setInt(2, 4);
            rs = ps.executeQuery();
            while (rs.next()) {
                String folio = rs.getString("ic_folio");
                //Borra todos los registros del acuse
                qrySentencia = " DELETE FROM COM_PROC_ACUSE_DET_FIDE  where ic_folio = ? and ic_proceso = ?";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setString(1, folio);
                ps.setInt(2, 2);
                ps.execute();
                ps.close();

                //Actualiza el acuse con el estatus 5-Depurado
                qrySentencia =
                    "  UPDATE  COM_PROC_ACUSE_FIDE " + " SET ig_estatus  = ? " + " where ic_folio = ? " +
                    " and IC_PROCESO = ? ";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, 5);
                ps.setString(2, folio);
                ps.setInt(3, 2);
                ps.execute();
                ps.close();
            }
            rs.close();
            ps.close();
            con.terminaTransaccion(true);

            //log.debug(" retorno.getErrores()" +retorno.getErrores());

            if (retorno.getErrores().length() == 0) {
                //inicia ciclo de almacenamiento

                String existeAcuse = "";
                String existeDetalle = "";

                //Inicia proceso de registros
                /***encabezado de tabla temporal*/
                String qrySentenciaI =
                    " select ic_enc from COMTMP_ALTA_ENC_PROC_FIDE  where ic_proceso = 2 and ig_estatus = 0 ";
                rsI = con.queryDB(qrySentenciaI);
                while (rsI.next()) {

                    ic_enc = rsI.getInt("ic_enc");
                    in_enc = rsI.getString("ic_enc") == null ? "" : rsI.getString("ic_enc");

                    //verifico si ya existe un acuse con el folio a procesar
                    qrySentencia =
                        "select ic_folio from COM_PROC_ACUSE_FIDE  where ic_folio = ? and ic_proceso = ?"; //acuse
                    psH = con.queryPrecompilado(qrySentencia);
                    psH.setInt(1, ic_enc);
                    psH.setInt(2, 2);
                    rsH = psH.executeQuery();
                    if (rsH.next()) {
                        existeAcuse = rsH.getString("ic_folio") == null ? "" : rsH.getString("ic_folio");
                    } else {
                        existeAcuse = "";
                    }
                    rsH.close();
                    psH.close();

                    if (!"".equals(existeAcuse)) {
                        //log.debug("Caso extrano, ya que el folio ya se habia procesado anteriormente, pero no se habia borrado del encabezado por lo que se procede a borrar el encabezado y detalle del encabezado:  " + in_enc);
                        String qrySentenciaZ = " DELETE FROM COM_PROC_ACUSE_FIDE where ic_folio = ? and ic_proceso = ?";
                        psJ = con.queryPrecompilado(qrySentenciaZ);
                        psJ.setInt(1, ic_enc);
                        psJ.setInt(2, 2);
                        psJ.execute();
                        psJ.close();
                        con.terminaTransaccion(true);
                    }

                    //verifico si ya existen registros procesados en esta tabla de detalle de acuse con el folio a procesar
                    qrySentencia =
                        "select count(ic_folio) from COM_PROC_ACUSE_DET_FIDE where ic_folio= ? and ic_proceso = ?";
                    psL = con.queryPrecompilado(qrySentencia);
                    psL.setString(1, in_enc);
                    psL.setInt(2, 2);
                    rsL = psL.executeQuery();
                    if (rsL.next()) {
                        existeDetalle = rsL.getInt(1) != 0 ? in_enc : "";
                    }
                    rsL.close();
                    psL.close();

                    if (!"".equals(existeDetalle)) {
                        // log.debug("Caso extrano, seguramente porque se trono el proceso y se quedo en proceso el folio. Se borra y reinicia el procesamiento del encabezado:" + in_enc );
                        String qrySentenciaDet =
                            " DELETE FROM COM_PROC_ACUSE_DET_FIDE where ic_folio = ? and ic_proceso = ? ";
                        psJ = con.queryPrecompilado(qrySentenciaDet);
                        psJ.setInt(1, ic_enc);
                        psJ.setInt(2, 2);
                        psJ.execute();
                        psJ.close();
                        con.terminaTransaccion(true);
                    }

                    // obtiene los datos  que deposito el FIDE en la tabla COMTMP_ALTA_ENC_PROC_DET_FIDE
                    qrySentencia =
                        " select ic_det, CG_DATOS from COMTMP_ALTA_ENC_PROC_DET_FIDE  where ic_enc = " + in_enc +
                        " and ic_proceso = 2";
                    rs2 = con.queryDB(qrySentencia);

                    //PARA OBTENER  LA LISTA DE  GARANTIAS  Y PODER VERIFICAR SI ESTAN O NO REPETIDAS
                    HashMap listaGarantias = new HashMap();

                    if (rs2 != null) {
                        while (rs2.next()) {
                            s_contenido = rs2.getString("CG_DATOS");
                            vt = new VectorTokenizer(s_contenido, "@");

                            lovDatos = vt.getValuesVector();
                            String claveGarantia = ((String) lovDatos.get(1)).trim();

                            BigInteger uno = new BigInteger("1");
                            BigInteger numeroGarantias = null;
                            numeroGarantias = (BigInteger) listaGarantias.get(claveGarantia.trim());
                            if (numeroGarantias == null) {
                                listaGarantias.put(claveGarantia.trim(), new BigInteger("1"));
                            } else {
                                listaGarantias.put(claveGarantia.trim(), numeroGarantias.add(uno));
                            }
                        }
                    }
                    rs2.close();
                    // SE EJECUTA NUEVAMENTE EL
                    rs2 = con.queryDB(qrySentencia);

                    if (rs2 != null) {
                        lineas = -1;
                        numLinea = 0;
                        while (rs2.next()) {
                            retorno = new ResultadosGar();
                            s_contenido = rs2.getString("CG_DATOS");
                            ic_det = rs2.getString("ic_det");

                            lineas++;
                            numLinea++;
                            iNumRegXproceso++;
                            log.debug("Entra a ciclo de insertado en gtitmp_contenido_arch  linea:  " + numLinea +
                                      " ic_det: " + ic_det + " encabezado " + in_enc);

                            BigDecimal tmpMonto = new BigDecimal(0);
                            BigDecimal totPagar = new BigDecimal(0);

                            retorno.setErrorLinea(false);

                            if (sbInClavesFinan.length() > 0)
                                sbInClavesFinan.append(",");
                            sbInClavesFinan.append("?");

                            ctrl_linea++;
                            try {

                                vt = new VectorTokenizer(s_contenido, "@");
                                lovDatos = vt.getValuesVector();

                                reg = 29;

                                //log.debug("Numero de campos "+lovDatos.size());

                                if (lovDatos.size() < reg) {
                                    retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() +
                                                          " \n");
                                    retorno.setErrorLinea(true);
                                    clavesFinan.add("0");
                                    retorno.incNumRegErr();
                                    continue;
                                }

                            } catch (Exception e) {
                                retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() + " \n");
                                retorno.setErrorLinea(true);
                                clavesFinan.add("0");
                                retorno.incNumRegErr();
                                continue;
                            }
                            clavesFinan.add(lovDatos.get(1));
                            claveFinancia = ((String) lovDatos.get(1)).trim(); //clave de la garantia


                            String errorIF = "";
                            String ifSiag3 = ((String) lovDatos.get(0)).trim(); // clave del Intermediario
                            this.validaLongitudNumero(numLinea, ifSiag3, "Clave del Intermediario", retorno, 5,
                                                      0); //5 enteros
                            if (retorno.getErrorLinea()) {
                                errorIF = "Error";
                            }
                            if (errorIF.equals("")) {
                                ifSiag = Integer.parseInt(ifSiag3);
                            }

                            //log.debug("  listaGarantias "+listaGarantias);
                            // Validar cada uno de los campos leidos
                            this.validarAltaDesembolsosMasivos(ifSiag, numLinea, s_contenido, retorno, listaGarantias,
                                                               ic_if);
                            log.debug("retorno.getErrorLinea() ---" + retorno.getErrorLinea());
                            if (!retorno.getErrorLinea()) {
                                tmpMonto = new BigDecimal(((String) lovDatos.get(6)).trim());
                            }

                            qrySentencia = " select ic_if from comcat_if  where ic_if_siag = ? ";
                            ps = con.queryPrecompilado(qrySentencia);
                            ps.setInt(1, ifSiag);
                            rs = ps.executeQuery();
                            if (rs.next()) {
                                ic_if = String.valueOf(rs.getInt("ic_if"));
                            }
                            rs.close();
                            ps.close();

                            log.debug("ic_if --->" + ic_if);
                            log.debug("ifSiag --->" + ifSiag);
                            log.debug("claveFinancia --->" + claveFinancia);

                            try {
                                totPagar =
                                    new BigDecimal(((String) lovDatos.get(4)).trim()); // Monto Original del Credito
                                log.debug("totPagar --->" + totPagar.toPlainString());
                            } catch (Exception e) {
                                retorno.appendErrores(" Clave del Financiamiento:" + (String) lovDatos.get(1) +
                                                      ". El campo total a pagar no es un numero valido\n");
                                retorno.setErrorLinea(true);
                            }

                            reg = 0;

                            if (lovDatos.size() < reg) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores(" Clave del Financiamiento: " + (String) lovDatos.get(1) +
                                                      ".  El numero de campos es mayor al debido. \n");
                            }

                            //valida que  no exista la garantia en la tabla de  gti_estatus_solic
                            d_aniomes = (new SimpleDateFormat("yyyyMMdd")).format(new java.util.Date());
                            String qrySentencia2 =
                                "select gs.ic_folio as ic_folio,gs.cc_garantia as cc_garantia " +
                                " from gti_garant_saldos gs" + " ,gti_estatus_solic es" +
                                " where gs.ic_folio = es.ic_folio" + " and gs.ic_if_siag = es.ic_if_siag" +
                                " and es.ic_situacion not in(?,?)" + " and gs.ic_if_siag = ?" +
                                " and gs.cg_anio_mes =  ?" + " and gs.cc_garantia =   ?";
                            psF = con.queryPrecompilado(qrySentencia2);
                            psF.setInt(1, 5);
                            psF.setInt(2, 7);
                            psF.setInt(3, ifSiag);
                            psF.setString(4, d_aniomes.substring(0, 6));
                            psF.setString(5, claveFinancia);
                            rsF = psF.executeQuery();
                            //psF.clearParameters();
                            if (rsF.next()) {
                                log.debug("La clave: " + rsF.getString("cc_garantia") +
                                          " ya se encuentra registrada este mes en el folio:" +
                                          rsF.getString("ic_folio") + ".\n");
                                retorno.appendErrores("La clave: " + rsF.getString("cc_garantia") +
                                                      " ya se encuentra registrada este mes en el folio:" +
                                                      rsF.getString("ic_folio") + ".\n");
                                retorno.setErrorLinea(true);
                            }
                            rsF.close();
                            psF.close();

                            //cuando no  hay errores
                            if (!retorno.getErrorLinea()) {
                                log.debug("cuando no  hay errores --->");

                                datos = new ArrayList();
                                datos.add((String) lovDatos.get(0));
                                datos.add((String) lovDatos.get(1));
                                datos.add((String) lovDatos.get(2));
                                datos.add((String) lovDatos.get(3));
                                datos.add((String) lovDatos.get(4));
                                datos.add((String) lovDatos.get(5));
                                datos.add((String) lovDatos.get(6));
                                datos.add((String) lovDatos.get(7));
                                datos.add((String) lovDatos.get(8));
                                datos.add((String) lovDatos.get(9));
                                datos.add((String) lovDatos.get(10));
                                datos.add((String) lovDatos.get(11));
                                datos.add((String) lovDatos.get(12));
                                datos.add((String) lovDatos.get(13));
                                datos.add((String) lovDatos.get(14));
                                datos.add((String) lovDatos.get(15));
                                datos.add((String) lovDatos.get(16));
                                datos.add((String) lovDatos.get(17));
                                datos.add((String) lovDatos.get(18));
                                datos.add((String) lovDatos.get(19));
                                datos.add((String) lovDatos.get(20));
                                datos.add((String) lovDatos.get(21));
                                datos.add((String) lovDatos.get(22));
                                datos.add((String) lovDatos.get(23));
                                datos.add((String) lovDatos.get(24));
                                datos.add((String) lovDatos.get(25));
                                datos.add((String) lovDatos.get(26));
                                datos.add((String) lovDatos.get(27));
                                datos.add((String) lovDatos.get(28));
                                datos.add(String.valueOf(ifSiag));
                                registros.add(datos);


                                if (lineas == 0) {
                                    retorno.appendCorrectos("Claves de Financiamiento:\n");
                                }
                                retorno1.appendCorrectos(lovDatos.get(1) + "\n");
                                retorno1.incNumRegOk();
                                retorno1.addSumRegOk(tmpMonto.doubleValue());
                                retorno1.addSumTotPag(totPagar);


                                /*//guada en la tabla de gtitmp_contenido_arch	*/
                                qrySentencia =
                                    "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" +
                                    " values(?,?,?)";
                                ps = con.queryPrecompilado(qrySentencia);
                                ps.clearParameters();
                                ps.setInt(1, retorno1.getIcProceso());
                                ps.setInt(2, numLinea);
                                ps.setString(3, s_contenido);
                                ps.execute();
                                ps.close();

                                // Insertar lineas del archivo proporcionado en la tabla: GTITMP_SOLICITUD_PAGO
                                String qrySentenciaX =
                                    "INSERT INTO 											" + "	GTITMP_SOLICITUD_PAGO 							" +
                                    "		( 													" + "			ic_proceso,									" + "			cc_garantia,								" +
                                    "			df_incumplimiento,						" + "			cg_causas_incumplimiento,				" +
                                    "			cg_funcionario_if,						" + "			cg_tel_funcionario,						" +
                                    "			cg_ext_funcionario,						" + "			cg_mail_funcionario,						" +
                                    "			cg_localizacion_supervision,			" + "			cg_ciudad_supervision,					" +
                                    "			cg_estado_supervision,					" + "			ic_periodicidad_pago,					" +
                                    "			df_fecha_segundo_incum,					" + "			fn_saldo_insoluto_estado_cuent,		" +
                                    "			fn_capital_vigente,						" + "			fn_capital_vencido,						" +
                                    "			fn_interes_ordinario,					" + "			fn_interes_moratorio,					" +
                                    "			df_fecha_ultimo_pago_capital,			" + "			df_fecha_ultimo_pago_interes,			" +
                                    "			fn_interes_recup_post_prim_inc,		" + "			ig_tasa_fondeo_nafin,					" +
                                    "			df_fecha_liquidacion_fondeo,			" + "			ig_num_prestamo_sirac,					" +
                                    "			cg_colonia_supervision,					" + "			ic_estado,									" +
                                    "			ic_municipio,								" + "			fn_monto_original_credito,				" +
                                    "			df_fecha_disposicion,					" + "			cs_fondeo_nafin,							" +
                                    "			cg_rfc_acreditado,						" + "			cg_nombre_acreditado,               " +
                                    "			ig_num_solicitud               		" + "		) 													" +
                                    "	VALUES 												" + "		( 													" + "			?, 											" +
                                    "			?, 											" + "			to_date(?,'yyyymmdd'), 					" + "			?, 											" +
                                    "			?, 											" + "			?, 											" + "			?, 											" +
                                    "			?, 											" + "			?, 											" + "			?, 											" +
                                    "			?, 											" + "			?, 											" + "			to_date(?,'yyyymmdd'), 					" +
                                    "			?, 											" + "			?, 											" + "			?, 											" +
                                    "			?, 											" + "			?, 											" + "			to_date(?,'yyyymmdd'), 					" +
                                    "			to_date(?,'yyyymmdd'), 					" + "			?, 											" + "			?, 											" +
                                    "			to_date(?,'yyyymmdd'), 					" + "			?, 											" + "			?, 											" +
                                    "			?, 											" + "			?, 											" + "			?, 											" +
                                    "			to_date(?,'yyyymmdd'), 					" + "			?,  											" + "			?,  											" +
                                    "			?,  											" + "			?  											" +
                                    "		) 													";

                                psX = con.queryPrecompilado(qrySentenciaX);
                                // Insertar parametros leidos
                                psX.setInt(1, retorno1.getIcProceso());
                                psX.setString(2,
                                              getCadena((String) lovDatos.get(1)).trim()); // CC_GARANTIA								,2
                                if (lovDatos.get(15) == null || "".equals((String) lovDatos.get(15))) {
                                    psX.setNull(3, Types.DATE);
                                } else {
                                    psX.setString(3,
                                                  getCadena((String) lovDatos.get(15)).trim()); // DF_INCUMPLIMIENTO						,16*
                                }
                                psX.setString(4, ""); // CG_CAUSAS_INCUMPLIMIENTO			,""
                                psX.setString(5,
                                              getCadena((String) lovDatos.get(21)).trim()); // CG_FUNCIONARIO_IF						,22
                                psX.setString(6,
                                              getCadena((String) lovDatos.get(28)).trim()); // CG_TEL_FUNCIONARIO					,29
                                psX.setInt(7,
                                           getInt((String) lovDatos.get(29))); // CG_EXT_FUNCIONARIO					,30
                                psX.setString(8,
                                              getCadena((String) lovDatos.get(22)).trim() + "@" +
                                              getCadena((String) lovDatos.get(23)).trim()); // CG_MAIL_FUNCIONARIO		,23y24
                                psX.setString(9,
                                              getCadena((String) lovDatos.get(24)).trim()); // CG_LOCALIZACION_SUPERVISION		,25
                                psX.setString(10, ""); // CG_CIUDAD_SUPERVISION				,""
                                psX.setString(11, ""); // CG_ESTADO_SUPERVISION				,""
                                psX.setInt(12,
                                           getInt((String) lovDatos.get(14))); // IC_PERIODICIDAD_PAGO					,15
                                if (lovDatos.get(16) == null || "".equals((String) lovDatos.get(16))) {
                                    psX.setNull(13, Types.DATE);
                                } else {
                                    psX.setString(13,
                                                  getCadena((String) lovDatos.get(16)).trim()); // DF_FECHA_SEGUNDO_INCUM				,17*
                                }
                                psX.setBigDecimal(14,
                                                  getBigDecimal((String) lovDatos.get(6))); // FN_SALDO_INSOLUTO_ESTADO_CUENT	,7
                                psX.setBigDecimal(15,
                                                  getBigDecimal((String) lovDatos.get(7))); // FN_CAPITAL_VIGENTE					,8
                                psX.setBigDecimal(16,
                                                  getBigDecimal((String) lovDatos.get(8))); // FN_CAPITAL_VENCIDO					,9
                                psX.setBigDecimal(17,
                                                  getBigDecimal((String) lovDatos.get(9))); // FN_INTERES_ORDINARIO					,10
                                psX.setBigDecimal(18,
                                                  getBigDecimal((String) lovDatos.get(10))); // FN_INTERES_MORATORIO					,11
                                psX.setString(19,
                                              getCadena((String) lovDatos.get(11)).trim()); // DF_FECHA_ULTIMO_PAGO_CAPITAL		,12
                                psX.setString(20,
                                              getCadena((String) lovDatos.get(12)).trim()); // DF_FECHA_ULTIMO_PAGO_INTERES		,13
                                psX.setBigDecimal(21,
                                                  getBigDecimal((String) lovDatos.get(13))); // FN_INTERES_RECUP_POST_PRIM_INC	,14
                                if (lovDatos.get(18) == null || "".equals((String) lovDatos.get(18))) {
                                    psX.setNull(22, Types.NUMERIC);
                                } else {
                                    psX.setBigDecimal(22,
                                                      getBigDecimal((String) lovDatos.get(18))); // IG_TASA_FONDEO_NAFIN					,19*
                                }
                                if (lovDatos.get(19) == null || "".equals((String) lovDatos.get(19))) {
                                    psX.setNull(23, Types.DATE);
                                } else {
                                    psX.setString(23,
                                                  getCadena((String) lovDatos.get(19)).trim()); // DF_FECHA_LIQUIDACION_FONDEO		,20*
                                }
                                if (lovDatos.get(20) == null || "".equals((String) lovDatos.get(20))) {
                                    psX.setNull(24, Types.NUMERIC);
                                } else {
                                    psX.setBigDecimal(24,
                                                      getBigDecimal((String) lovDatos.get(20))); // IG_NUM_PRESTAMO_SIRAC				,21*
                                }
                                psX.setString(25,
                                              getCadena((String) lovDatos.get(25)).trim()); // CG_COLONIA_SUPERVISION				,26
                                psX.setInt(26,
                                           getInt((String) lovDatos.get(26))); // IC_ESTADO								,27
                                psX.setInt(27,
                                           getInt((String) lovDatos.get(27))); // IC_MUNICIPIO							,28
                                psX.setBigDecimal(28,
                                                  getBigDecimal((String) lovDatos.get(4))); // FN_MONTO_ORIGINAL_CREDITO			,5
                                psX.setString(29,
                                              getCadena((String) lovDatos.get(5)).trim()); // DF_FECHA_DISPOSICION					,6
                                psX.setString(30,
                                              getCadena((String) lovDatos.get(17)).trim()); // CS_FONDEO_NAFIN						,18
                                psX.setString(31,
                                              getCadena((String) lovDatos.get(2)).trim()); // CG_RFC_ACREDITADO						,3
                                psX.setString(32,
                                              getCadena((String) lovDatos.get(3)).trim()); // CG_NOMBRE_ACREDITADO					,4
                                psX.setInt(33, numLinea); // Numero de Linea
                                psX.execute();
                                psX.close();
                                log.debug(" qrySentenciaX--->" + qrySentenciaX);


                                //guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE
                                //inserta los registros sin error
                                qrySentenciaDetalle =
                                    " insert into COM_PROC_ACUSE_DET_FIDE  " +
                                    " (IC_FOLIO, IC_DET, CG_DATOS, CG_ERROR,  IG_ESTATUS, IC_PROCESO )" +
                                    " values( ?,?,?,?,?,?) ";
                                psD = con.queryPrecompilado(qrySentenciaDetalle);
                                String vacio = "";
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, vacio);
                                psD.setInt(5, 0); //estatus 0 Ok  no hay errores
                                psD.setInt(6, 2);
                                psD.execute();
                                psD.close();
                                //log.debug(" qrySentenciaDetalle--->"+qrySentenciaDetalle);
                                totalAceptados++;

                            } else {

                                log.debug(" guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE cuando hay error ");
                                log.debug(" qrySentenciaDetalle--->" + qrySentenciaDetalle);
                                /*guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE cuando hay error */
                                qrySentenciaDetalle =
                                    " insert into COM_PROC_ACUSE_DET_FIDE  " +
                                    " (IC_FOLIO, IC_DET, CG_DATOS, CG_ERROR,  IG_ESTATUS, IC_PROCESO )" +
                                    " values( ?,?,?,?,?,?) ";
                                psD = con.queryPrecompilado(qrySentenciaDetalle);
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, retorno.getErrores());
                                psD.setInt(5, 1); // 1 Error
                                psD.setInt(6, 2);
                                psD.execute();
                                psD.close();

                                retorno1.incNumRegErr();
                                retorno1.addSumRegErr(tmpMonto.doubleValue());
                                totalRechazados++;
                            }

                            dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                            iNumRegTrans++;
                            totalRegistros = totalAceptados + totalRechazados;

                        } //while(rs2)
                    } //if(rs2)
                    rs2.close();
                    //Condicion para mandar informacion por el metodo "transmitirDesembolsoMasivo"
                    //siempre y cuando por lo menos haya un registros Aceptado
                    if (numLinea != totalRechazados) {
                        retorno2 = new ResultadosGar();
                        con.terminaTransaccion(true);

                        //gti_estatus_solic,  gti_contenido_arch
                        retorno2 =
                            transmitirDesembolsosMasivos(String.valueOf(retorno1.getIcProceso()), ic_if,
                                                         String.valueOf(totalAceptados),
                                                         String.valueOf(retorno1.getSumRegOk()), "proc_aut", "");

                        //guarda en la tabla COM_PROC_ACUSE_FIDE
                        resumenDesembolsoMasivo(retorno2, totalRegistros, new BigDecimal(retorno1.getSumRegOk()),
                                                totalAceptados, totalRechazados, in_enc, 2);

                    } else {

                        log.debug(" cuando todos los registros tienen errores ");
                        retorno2 = new ResultadosGar();
                        String c_aniomes = (new SimpleDateFormat("yyyyMMdd")).format(new java.util.Date());
                        con.terminaTransaccion(true);
                        String qrySentencia1 = " select to_char(to_date(?,'yyyymm'),'yyyymm') from dual ";
                        psF = con.queryPrecompilado(qrySentencia1);
                        psF.setString(1, c_aniomes.substring(0, 6));
                        rsF = psF.executeQuery();
                        if (rsF.next()) {
                            c_aniomes = rsF.getString(1);
                        }
                        rsF.close();
                        psF.close();

                        String fecha = (new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
                        String hora = (new SimpleDateFormat("HH:mm")).format(new java.util.Date());
                        retorno2.setFecha(fecha);
                        retorno2.setHora(hora);
                        retorno2.setFolio("");

                        //guarda en la tabla COM_PROC_ACUSE_FIDE
                        resumenDesembolsoMasivo(retorno2, totalRegistros, new BigDecimal(retorno1.getSumRegOk()),
                                                totalAceptados, totalRechazados, in_enc, 2);

                    }

                    String qrySentenciaY =
                        " delete  from COMTMP_ALTA_ENC_PROC_DET_FIDE where ic_enc  = ? and ic_proceso = ?";
                    psZ = con.queryPrecompilado(qrySentenciaY);
                    psZ.setInt(1, ic_enc);
                    psZ.setInt(2, 2);
                    psZ.execute();
                    psZ.close();

                    //tablas que se borraran al final del proceso
                    String qrySentenciaT =
                        " delete  from COMTMP_ALTA_ENC_PROC_FIDE  where ic_enc  = ? and ic_proceso = ? ";
                    psE = con.queryPrecompilado(qrySentenciaT);
                    psE.setInt(1, ic_enc);
                    psE.setInt(2, 2);
                    psE.execute();
                    psE.close();
                    con.terminaTransaccion(true);

                    //log.debug("reseteo de variables que se llenan dentro del ciclo while");
                    iNumRegXproceso = 0;
                    clavesFinan = new ArrayList();
                    dbl_sum_montos = new BigDecimal(0.00);
                    sbInClavesFinan = new StringBuffer();
                    retorno1 = new ResultadosGar();
                    retorno1.setIcProceso(calculaIcProceso());
                    lineas = -1;
                    numLinea = 0;
                    totalAceptados = 0;
                    totalRechazados = 0;
                }
                rsI.close();

            } //inicia ciclo de almacenamiento

            if (retorno.getCifras().length() ==
                0) {
                //Si no hay errores de cifras de control... establece claves de financiamiento
                //------------------------ Inicio mensajes para debug ---------------------------
                log.debug("procesarDesembolsosMasivos(" + _fechaMsg + "_" + retorno1.getIcProceso() +
                          ")::Cifra de Control OK. clavesFinan.size()=" + clavesFinan.size());
                //-------------------------- Fin mensajes para debug ----------------------------
                retorno.setClavesFinan(clavesFinan);
            }


        } catch (Exception e) {
            log.error("procesarDesembolsosMasivos(Exception)");
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("procesarDesembolsosMasivos(S)");
        }
        return retorno;
    }


    /** @throws com.netro.exception.NafinException
     * @param in_enc
     * @param totalRechazados
     * @param totalAceptados
     * @param montoReg
     * @param numReg
     * @param resultado
     */

    public void resumenDesembolsoMasivo(ResultadosGar resultado, int numReg, BigDecimal montoReg, int totalAceptados,
                                        int totalRechazados, String in_enc, int ic_proceso) throws NafinException {

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String querySentence = "";
        String estatus = "1";

        try {
            con.conexionDB();

            log.info("resumenDesembolsoMasivo(E)");

            querySentence =
                " INSERT INTO COM_PROC_ACUSE_FIDE  " + " (IC_FOLIO, IC_PROCESO,  CG_FECHA_VALOR	,  " +
                " CG_HORA_VALOR, IN_TOTAL_REG	, " + " FN_TOTAL_MONTO, FN_TOTAL_RECHAZADOS , FN_TOTAL_ACEPTADOS, " +
                " IC_FOLIO_GTI, IG_ESTATUS) " + " VALUES (?,?,?,?,?,?,?,?,?,?) ";

            /*
			if(totalAceptados == numReg){
				estatus = "2";  // cuando todos estan bien
			}	else  if (totalRechazados == numReg ) {
				estatus = "3";  // cuando ninguno esta bien
			}else{
				estatus = "1";  // cuando al menos uno esta bien
			}
			*/
            if (totalRechazados == numReg) {
                estatus = "3";
            }

            ps = con.queryPrecompilado(querySentence);
            ps.setString(1, in_enc);
            ps.setInt(2, ic_proceso); //2 Desmbolsos 3 Recuperacion
            ps.setString(3, resultado.getFecha());
            ps.setString(4, resultado.getHora());
            ps.setInt(5, numReg);
            ps.setBigDecimal(6, montoReg);
            ps.setInt(7, totalRechazados);
            ps.setInt(8, totalAceptados);
            ps.setString(9, String.valueOf(resultado.getFolio()));
            ps.setString(10, estatus);
            ps.execute();

        } catch (Exception e) {
            con.terminaTransaccion(false);
            log.error("resumenDesembolsoMasivo(Exception)" + e);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.terminaTransaccion(true);
            con.cierraConexionDB();
        }
        log.info("resumenDesembolsoMasivo(F)");

    }


    public ResultadosGar procesarRecuperacionDesembolsos() throws NafinException {

        log.info(" procesarRecuperacionDesembolsos (E)");

        AccesoDB con = new AccesoDB();
        ResultadosGar retorno = new ResultadosGar();
        ResultadosGar retorno2 = new ResultadosGar();
        PreparedStatement ps = null;
        ResultadosGar retorno1 = new ResultadosGar();

        ResultSet rs = null;
        ResultSet rs2 = null;
        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        String qrySentencia = "";
        String qrySentenciaDetalle = "";
        PreparedStatement psD = null;
        PreparedStatement psE = null;
        PreparedStatement psF = null;
        PreparedStatement psZ = null;
        ResultSet rsF = null;
        ResultSet rsI = null;

        PreparedStatement psH = null;
        PreparedStatement psJ = null;
        ResultSet rsH = null;
        PreparedStatement psL = null;
        ResultSet rsL = null;

        try {

            VectorTokenizer vt = null;
            Vector lovDatos = null;
            int ifSiag = 0;
            String ic_if = "0";
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            int reg = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);
            List clavesFinan = new ArrayList();
            StringBuffer sbInClavesFinan = new StringBuffer("");
            int iNumRegXproceso = 0;
            String s_contenido = "";
            String ic_det = "";
            int totalRechazados = 0;
            int totalAceptados = 0;
            int totalRegistros = 0;
            String claveFinancia = "";
            String d_aniomes = "";
            String in_enc = "";
            int ic_enc = 0;
            CallableStatement cs = null;

            con.conexionDB();

            /*  Procedimiento almacenado que toma la informaci�n resultado de las tablas del SIAG
			para reportar al FIDE los errores en el   procesamiento de los envios de Saldos*/

            cs = con.ejecutaSP("SP_ACT_ERR_FIDE()");
            try {
                cs.execute();
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                throw new NafinException("SIST0001" + sqle);
            }
            if (cs != null)
                cs.close();
            con.terminaTransaccion(true);

            //OBTIENE EL NUMERO DE FOLIO
            retorno1.setIcProceso(calculaIcProceso());

            log.debug("retorno1.getIcProceso()>>>>>>>>>>>>>>>>>>>>>" + retorno1.getIcProceso());

            // SE OBTIENEN TODOS LOS ACUSES YA LEIDOS POR EL FIDE PARA PROCEDER A SU DEPURACION
            qrySentencia =
                " select ic_folio from  COM_PROC_ACUSE_FIDE " + " WHERE IC_PROCESO = ? " + " AND IG_ESTATUS = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, 3);
            ps.setInt(2, 4);
            rs = ps.executeQuery();
            while (rs.next()) {
                String folio = rs.getString("ic_folio");

                //Borra todos los registros del acuse
                qrySentencia = " DELETE FROM COM_PROC_ACUSE_DET_FIDE  where ic_folio = ? and ic_proceso = ?";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setString(1, folio);
                ps.setInt(2, 3);
                ps.execute();
                ps.close();

                //Actualiza el acuse con el estatus 5-Depurado
                qrySentencia =
                    "  UPDATE  COM_PROC_ACUSE_FIDE " + " SET ig_estatus  = ? " + " where ic_folio = ? " +
                    " and IC_PROCESO = ? ";
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, 5);
                ps.setString(2, folio);
                ps.setInt(3, 3);
                ps.execute();
                ps.close();

            }
            rs.close();
            ps.close();
            con.terminaTransaccion(true);

            //log.debug(" retorno.getErrores()" +retorno.getErrores());

            if (retorno.getErrores().length() == 0) {
                //inicia ciclo de almacenamiento
                String existeAcuse = "";
                String existeDetalle = "";
                //Inicia proceso de registros
                /***encabezado de tabla temporal*/
                String qrySentenciaI =
                    " select ic_enc from COMTMP_ALTA_ENC_PROC_FIDE  where ic_proceso = 3 and ig_estatus = 0 ";
                rsI = con.queryDB(qrySentenciaI);
                while (rsI.next()) {
                    ic_enc = rsI.getInt("ic_enc");
                    in_enc = rsI.getString("ic_enc") == null ? "" : rsI.getString("ic_enc");
                    //verifico si ya existe un acuse con el folio a procesar
                    qrySentencia =
                        "select ic_folio from COM_PROC_ACUSE_FIDE  where ic_folio = ? and ic_proceso = ?"; //acuse
                    psH = con.queryPrecompilado(qrySentencia);
                    psH.setInt(1, ic_enc);
                    psH.setInt(2, 3);
                    rsH = psH.executeQuery();
                    if (rsH.next()) {
                        existeAcuse = rsH.getString("ic_folio") == null ? "" : rsH.getString("ic_folio");
                    } else {
                        existeAcuse = "";
                    }
                    rsH.close();
                    psH.close();

                    if (!"".equals(existeAcuse)) {
                        //log.debug("Caso extrano, ya que el folio ya se habia procesado anteriormente, pero no se habia borrado del encabezado por lo que se procede a borrar el encabezado y detalle del encabezado:  " + in_enc);
                        String qrySentenciaZ = " DELETE FROM COM_PROC_ACUSE_FIDE where ic_folio = ? and ic_proceso = ?";
                        psJ = con.queryPrecompilado(qrySentenciaZ);
                        psJ.setInt(1, ic_enc);
                        psJ.setInt(2, 3);
                        psJ.execute();
                        psJ.close();
                        con.terminaTransaccion(true);
                    }

                    //verifico si ya existen registros procesados en esta tabla de detalle de acuse con el folio a procesar
                    qrySentencia =
                        "select count(ic_folio) from COM_PROC_ACUSE_DET_FIDE where ic_folio= ? and ic_proceso = ?";
                    psL = con.queryPrecompilado(qrySentencia);
                    psL.setString(1, in_enc);
                    psL.setInt(2, 3);
                    rsL = psL.executeQuery();
                    if (rsL.next()) {
                        existeDetalle = rsL.getInt(1) != 0 ? in_enc : "";
                    }
                    rsL.close();
                    psL.close();

                    if (!"".equals(existeDetalle)) {
                        // log.debug("Caso extrano, seguramente porque se trono el proceso y se quedo en proceso el folio. Se borra y reinicia el procesamiento del encabezado:" + in_enc );
                        String qrySentenciaDet =
                            " DELETE FROM COM_PROC_ACUSE_DET_FIDE where ic_folio = ? and ic_proceso = ?";
                        psJ = con.queryPrecompilado(qrySentenciaDet);
                        psJ.setInt(1, ic_enc);
                        psJ.setInt(2, 3);
                        psJ.execute();
                        psJ.close();
                        con.terminaTransaccion(true);
                    }

                    // obtiene los datos  que deposito el FIDE en la tabla COMTMP_ALTA_ENC_PROC_DET_FIDE
                    qrySentencia =
                        " select ic_det, CG_DATOS from COMTMP_ALTA_ENC_PROC_DET_FIDE  where ic_enc = " + in_enc +
                        " and ic_proceso = 3";
                    rs2 = con.queryDB(qrySentencia);

                    if (rs2 != null) {
                        lineas = -1;
                        numLinea = 0;
                        while (rs2.next()) {
                            retorno = new ResultadosGar();
                            s_contenido = rs2.getString("CG_DATOS");
                            ic_det = rs2.getString("ic_det");

                            lineas++;
                            numLinea++;
                            iNumRegXproceso++;
                            log.debug("Entra a ciclo de insertado en gtitmp_contenido_arch  linea:  " + numLinea +
                                      " ic_det: " + ic_det + " encabezado " + in_enc);

                            BigDecimal tmpMonto = new BigDecimal(0);
                            BigDecimal totPagar = new BigDecimal(0);

                            retorno.setErrorLinea(false);

                            if (sbInClavesFinan.length() > 0)
                                sbInClavesFinan.append(",");
                            sbInClavesFinan.append("?");

                            ctrl_linea++;

                            try {

                                vt = new VectorTokenizer(s_contenido, "@");
                                lovDatos = vt.getValuesVector();

                                reg = 18;

                                log.debug("Numero de campos " + lovDatos.size());

                                if (lovDatos.size() < reg) {
                                    retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() +
                                                          " \n");
                                    retorno.setErrorLinea(true);
                                    clavesFinan.add("0");
                                    retorno.incNumRegErr();
                                    continue;
                                }

                            } catch (Exception e) {
                                retorno.appendErrores(" El numero de campos es incorrecto." + lovDatos.size() + " \n");
                                retorno.setErrorLinea(true);
                                clavesFinan.add("0");
                                retorno.incNumRegErr();
                                continue;
                            }
                            String errorIF = "";
                            String ifSiag3 = ((String) lovDatos.get(0)).trim(); // clave del Intermediario
                            this.validaLongitudNumero(numLinea, ifSiag3, "Clave del Intermediario", retorno, 5,
                                                      0); //5 enteros

                            log.info("retorno.getErrorLinea() " + retorno.getErrorLinea());
                            if (retorno.getErrorLinea()) {
                                errorIF = "Error";
                            }

                            if (errorIF.equals("")) {
                                ifSiag = Integer.parseInt(ifSiag3);
                            }
                            // valida los registro
                            this.validarAltaRecuperaciones(ifSiag, numLinea, s_contenido, retorno);

                            //-------------------------------------------------------------------
                            if (!retorno.getErrorLinea()) {
                                tmpMonto = new BigDecimal(((String) lovDatos.get(4)).trim());
                            }

                            clavesFinan.add(lovDatos.get(1));
                            claveFinancia = ((String) lovDatos.get(1)).trim(); //clave de la garantia

                            qrySentencia = " select ic_if from comcat_if  where ic_if_siag = ? ";
                            ps = con.queryPrecompilado(qrySentencia);
                            ps.setInt(1, ifSiag);
                            rs = ps.executeQuery();
                            if (rs.next()) {
                                ic_if = String.valueOf(rs.getInt("ic_if"));
                            }
                            rs.close();
                            ps.close();

                            log.debug("ic_if --->" + ic_if);
                            log.debug("ifSiag --->" + ifSiag);
                            log.debug("claveFinancia --->" + claveFinancia);

                            try {
                                totPagar =
                                    new BigDecimal(((String) lovDatos.get(4)).trim()); // Monto Original del Credito
                            } catch (Exception e) {
                                retorno.appendErrores(" Clave del Financiamiento:" + (String) lovDatos.get(1) +
                                                      ". El campo total a pagar no es un numero valido\n");
                                retorno.setErrorLinea(true);
                            }
                            reg = 0;
                            if (lovDatos.size() < reg) {
                                retorno.setErrorLinea(true);
                                retorno.appendErrores(" Clave del Financiamiento: " + (String) lovDatos.get(1) +
                                                      ".  El numero de campos es mayor al debido. \n");
                            }

                            //valida que  no exista la garantia en la tabla de  gti_estatus_solic
                            d_aniomes = (new SimpleDateFormat("yyyyMMdd")).format(new java.util.Date());
                            String qrySentencia2 =
                                "select gs.ic_folio as ic_folio,gs.cc_garantia as cc_garantia " +
                                " from gti_garant_saldos gs" + " ,gti_estatus_solic es" +
                                " where gs.ic_folio = es.ic_folio" + " and gs.ic_if_siag = es.ic_if_siag" +
                                " and es.ic_situacion not in(?,?)" + " and gs.ic_if_siag = ?" +
                                " and gs.cg_anio_mes =  ?" + " and gs.cc_garantia =   ?";

                            psF = con.queryPrecompilado(qrySentencia2);
                            psF.setInt(1, 5);
                            psF.setInt(2, 7);
                            psF.setInt(3, ifSiag);
                            psF.setString(4, d_aniomes.substring(0, 6));
                            psF.setString(5, claveFinancia);
                            rsF = psF.executeQuery();
                            log.debug("qrySentencia2  " + qrySentencia2);

                            //psF.clearParameters();
                            if (rsF.next()) {
                                log.debug("La clave: " + rsF.getString("cc_garantia") +
                                          " ya se encuentra registrada este mes en el folio:" +
                                          rsF.getString("ic_folio") + ".\n");
                                retorno.appendErrores("La clave: " + rsF.getString("cc_garantia") +
                                                      " ya se encuentra registrada este mes en el folio:" +
                                                      rsF.getString("ic_folio") + ".\n");
                                retorno.setErrorLinea(true);
                            }
                            rsF.close();
                            psF.close();

                            if (!retorno.getErrorLinea()) {
                                if (lineas == 0) {
                                    retorno.appendCorrectos("Claves de Financiamiento:\n");
                                }
                                retorno1.appendCorrectos(lovDatos.get(1) + "\n");
                                retorno1.incNumRegOk();
                                retorno1.addSumRegOk(tmpMonto.doubleValue());
                                retorno1.addSumTotPag(totPagar);


                                /*//guada en la tabla de gtitmp_contenido_arch	*/
                                qrySentencia =
                                    "insert into gtitmp_contenido_arch" + "(IC_PROCESO,IC_LINEA,CG_CONTENIDO)" +
                                    " values(?,?,?)";
                                ps = con.queryPrecompilado(qrySentencia);
                                ps.clearParameters();
                                ps.setInt(1, retorno1.getIcProceso());
                                ps.setInt(2, numLinea);
                                ps.setString(3, s_contenido);
                                ps.execute();
                                ps.close();
                                log.debug(" qrySentencia--->" + qrySentencia);

                                //guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE
                                //inserta los registros sin error
                                qrySentenciaDetalle =
                                    " insert into COM_PROC_ACUSE_DET_FIDE  " +
                                    " (IC_FOLIO, IC_DET, CG_DATOS, CG_ERROR,  IG_ESTATUS, IC_PROCESO )" +
                                    " values( ?,?,?,?,?,?) ";
                                psD = con.queryPrecompilado(qrySentenciaDetalle);
                                String vacio = "";
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, vacio);
                                psD.setInt(5, 0); //estatus 0 Ok  no hay errores
                                psD.setInt(6, 3);
                                psD.execute();
                                log.debug(" qrySentenciaDetalle--->" + qrySentenciaDetalle);
                                totalAceptados++;

                            } else {

                                log.debug(" guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE cuando hay error ");
                                log.debug(" qrySentenciaDetalle--->" + qrySentenciaDetalle);
                                /*guarda todos los detalles en la tabla de COM_PROC_ACUSE_DET_FIDE cuando hay error */
                                qrySentenciaDetalle =
                                    " insert into COM_PROC_ACUSE_DET_FIDE  " +
                                    " (IC_FOLIO, IC_DET, CG_DATOS, CG_ERROR,  IG_ESTATUS, IC_PROCESO )" +
                                    " values( ?,?,?,?,?,?) ";
                                psD = con.queryPrecompilado(qrySentenciaDetalle);
                                psD.clearParameters();
                                psD.setString(1, in_enc);
                                psD.setString(2, ic_det);
                                psD.setString(3, s_contenido);
                                psD.setString(4, retorno.getErrores());
                                psD.setInt(5, 1); // 1 Error
                                psD.setInt(6, 3);
                                psD.execute();

                                retorno1.incNumRegErr();
                                retorno1.addSumRegErr(tmpMonto.doubleValue());

                                totalRechazados++;
                            }

                            dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
                            iNumRegTrans++;
                            totalRegistros = totalAceptados + totalRechazados;

                        } //while(rs2)
                    } //if(rs2)
                    rs2.close();

                    //Condicion para mandar informacion por el metodo "transmitirDesembolsoMasivo"
                    //siempre y cuando por lo menos haya un registros Aceptado
                    if (numLinea != totalRechazados) {
                        retorno2 = new ResultadosGar();
                        con.terminaTransaccion(true);

                        retorno2 =
                            transmitirAltaRecuperaciones(String.valueOf(retorno1.getIcProceso()), ic_if,
                                                         String.valueOf(totalAceptados),
                                                         String.valueOf(retorno1.getSumRegOk()), "proc_aut", "");

                        //guarda en la tabla COM_PROC_ACUSE_FIDE
                        resumenDesembolsoMasivo(retorno2, totalRegistros, new BigDecimal(retorno1.getSumRegOk()),
                                                totalAceptados, totalRechazados, in_enc, 3);


                    } else {

                        log.debug(" cuando todos los registros tienen errores ");
                        retorno2 = new ResultadosGar();
                        String c_aniomes = (new SimpleDateFormat("yyyyMMdd")).format(new java.util.Date());
                        con.terminaTransaccion(true);
                        String qrySentencia1 = " select to_char(to_date(?,'yyyymm'),'yyyymm') from dual ";
                        psF = con.queryPrecompilado(qrySentencia1);
                        psF.setString(1, c_aniomes.substring(0, 6));
                        rsF = psF.executeQuery();
                        if (rsF.next()) {
                            c_aniomes = rsF.getString(1);
                        }
                        rsF.close();
                        psF.close();


                        String fecha = (new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
                        String hora = (new SimpleDateFormat("HH:mm")).format(new java.util.Date());
                        retorno2.setFecha(fecha);
                        retorno2.setHora(hora);
                        retorno2.setFolio("");

                        //guarda en la tabla COM_PROC_ACUSE_FIDE
                        resumenDesembolsoMasivo(retorno2, totalRegistros, new BigDecimal(retorno1.getSumRegOk()),
                                                totalAceptados, totalRechazados, in_enc, 3);

                    }

                    //tablas que se borraran al final del proceso

                    String qrySentenciaY =
                        " delete  from COMTMP_ALTA_ENC_PROC_DET_FIDE where ic_enc  = ? and ic_proceso = ?";
                    psZ = con.queryPrecompilado(qrySentenciaY);
                    psZ.setInt(1, ic_enc);
                    psZ.setInt(2, 3);
                    psZ.execute();
                    psZ.close();

                    String qrySentenciaT =
                        " delete  from COMTMP_ALTA_ENC_PROC_FIDE  where ic_enc  = ? and ic_proceso = ?";
                    psE = con.queryPrecompilado(qrySentenciaT);
                    psE.setInt(1, ic_enc);
                    psE.setInt(2, 3);
                    psE.execute();
                    psE.close();

                    con.terminaTransaccion(true);

                    //log.debug("reseteo de variables que se llenan dentro del ciclo while");
                    iNumRegXproceso = 0;
                    clavesFinan = new ArrayList();
                    dbl_sum_montos = new BigDecimal(0.00);
                    sbInClavesFinan = new StringBuffer();
                    retorno1 = new ResultadosGar();
                    retorno1.setIcProceso(calculaIcProceso());
                    lineas = -1;
                    numLinea = 0;
                    totalAceptados = 0;
                    totalRechazados = 0;
                }
                rsI.close();

            } //inicia ciclo de almacenamiento

            if (retorno.getCifras().length() ==
                0) {
                //Si no hay errores de cifras de control... establece claves de financiamiento
                //------------------------ Inicio mensajes para debug ---------------------------
                log.debug("procesarRecuperacionDesembolsos(" + _fechaMsg + "_" + retorno1.getIcProceso() +
                          ")::Cifra de Control OK. clavesFinan.size()=" + clavesFinan.size());
                //-------------------------- Fin mensajes para debug ----------------------------
                retorno.setClavesFinan(clavesFinan);
            }


        } catch (Exception e) {
            log.error("procesarRecuperacionDesembolsos(Exception)");
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("procesarRecuperacionDesembolsos(S)");
        }
        return retorno;
    }

    /**
     *  Esta funcion determina si un intermediario financiero opera con el programa
     *  "Credito Hipotecario" de la SHF.
     *
     *  @throws netropology.utilerias.AppException
     *  @param 	claveIF <tt>String</tt> con la clave del IF
     *  @since  Fodea 006 - 2012
     *	 @return Un objeto de tipo <tt>boolean</tt> con <tt>true</tt> si el IF opera con el programa o
     *          con <tt>false</tt> en caso contrario.
     *  @author JSHD
     */
    public boolean operaProgramaSHFCreditoHipotecario(String claveIF) throws AppException {

        log.info("operaProgramaSHFCreditoHipotecario(E)");

        if (claveIF == null || claveIF.trim().equals("")) {
            log.info("operaProgramaSHFCreditoHipotecario(S)");
            throw new AppException("Ocurrio un Error al consultar la parametrizacion del programa \"Credito Hipotecario\" de la SHF.");
        }

        AccesoDB con = new AccesoDB();
        StringBuffer qrySentencia = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean operaPrograma = false;

        try {

            con.conexionDB();

            // Revisar si el programa de la SHF "Credito Hipotecario" esta parametrizado
            qrySentencia = new StringBuffer();
            qrySentencia.append("SELECT 																	" + "	IF_SIAG.CS_SHF AS PROGRAMA_PARAMETRIZADO					" +
                                "FROM 																	" + "	COMCAT_IF INTERMEDIARIO, 										" +
                                "	GTI_IF_SIAG IF_SIAG 												" + "WHERE 																	" +
                                "	INTERMEDIARIO.IC_IF       		= ? 							" +
                                "	AND INTERMEDIARIO.IC_IF_SIAG 	= IF_SIAG.IC_IF_SIAG 	");
            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            if (rs.next()) {
                operaPrograma = "S".equals(rs.getString("PROGRAMA_PARAMETRIZADO")) ? true : false;
            }

        } catch (Exception e) {

            log.error("operaProgramaSHFCreditoHipotecario(Exception)");
            log.error("operaProgramaSHFCreditoHipotecario.claveIF = <" + claveIF + ">");
            e.printStackTrace();
            throw new AppException("Ocurrio un Error al consultar la parametrizacion del programa \"Credito Hipotecario\" de la SHF.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("operaProgramaSHFCreditoHipotecario(S)");
        }

        return operaPrograma;

    }


    /**
     *
     * Revisa si el Intermediario Financiero consultado tiene asignada una clave de
     * intermediario en el SIAG.
     *
     * @throws AppException
     *
     * @param claveIF  <tt>String</tt> con la clave del intermediario financiero, como
     *						 aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     *
     * @return <tt>true</tt> en caso de que el intermediario financiero tenga una clave SIAG
     *			  asignada y <tt>false</tt>, en caso contrario.
     *
     * @since          Fodea 006 - 2012, 15/06/2012 12:50:41 p.m.
     * @author         jshernandez
     *
     */
    public boolean existeClaveSIAG(String claveIF) throws AppException {

        log.info("existeClaveSIAG(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        PreparedStatement ps = null;
        ResultSet rs = null;

        boolean existeClaveSIAG = false;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Extraer clave de Intermediario Financiero en el SIAG
            query.append("SELECT                " +
                         "  DECODE( IC_IF_SIAG, NULL, 'false', 'true') AS EXISTE_CLAVE_SIAG " +
                         "FROM                  " + "	COMCAT_IF           " + "WHERE                 " +
                         "	IC_IF = ?           ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            if (rs.next()) {
                existeClaveSIAG = "true".equals(rs.getString("EXISTE_CLAVE_SIAG")) ? true : false;
            } else {
                existeClaveSIAG = false;
            }

        } catch (Exception e) {

            log.error("existeClaveSIAG(Exception)");
            log.error("existeClaveSIAG.claveIF = <" + claveIF + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al validar la Clave SIAG del Intermediario Financiero.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("existeClaveSIAG(S)");

        }

        return existeClaveSIAG;

    }

    /**
     *
     * Devuelve el Resumen de la Comsiones para el Programa SHF para un mes y a�o
     * especificos y para un Intemediario Financiero en Particular.
     *
     * Nota: se asume que en la tabla solo existe un registro para una Fecha de Corte
     * e Intermediario Financiero especifico, en caso contrario se lanza una exception
     * indicando que hay demasiados registros.
     *
     * @throws AppException
     *
     * @param claveIF  	<tt>String</tt> con la clave del intermediario financiero, como
     *						 	aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param claveFiso  <tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param claveMes 	<tt>String</tt> clave del Mes para el cual se solicita el reporte
     *						 	con formato AAAADD.
     *
     * @return         	<tt>List</tt> de <tt>HashMap</tt> con los siguientes elementos:
     *	                	claveMes, fiso, fechaCorte, fechaComision, numeroComisionesExtraer,
     *						 	y extraccionFirmada.
     *
     *
     * @since          Fodea 006 - 2012, 13/06/2012 06:07:02 p.m.
     * @author         jshernandez
     *
     */
    public List getResumenComisiones(String claveIF, String claveFiso, String claveMes) throws AppException {

        log.info("getResumenComisiones(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        ResultSet rs = null;
        PreparedStatement ps = null;

        ArrayList lista = new ArrayList();
        int ctaRegistros = 0;

        try {

            // Obtener la fecha de corte
            String anio = claveMes.substring(0, 4);
            String numeroMes = claveMes.substring(4, 6);
            String fechaCorte = Fecha.getUltimoDiaDelMes("01/" + numeroMes + "/" + anio, "dd/MM/yyyy");

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Realizar Consulta
            query.append("SELECT  " + "   TO_CHAR(FECHA_CORTE,'YYYYMM')                AS CLAVE_MES,                 " +
                         "   FISO                                         AS FISO,                      " +
                         "   TO_CHAR(FECHA_CORTE,'DD/MM/YYYY')            AS FECHA_CORTE,               " +
                         "   TO_CHAR(FECHA_CORTE,'MM')                    AS NUMERO_MES_COMISION,       " +
                         // "   TO_NUMBER(TO_CHAR(FECHA_CORTE,'MM'),'99')    AS NUMERO_MES_COMISION,       "  +
                         "   TO_CHAR(FECHA_CORTE,'YYYY') 		             AS ANIO_COMISION,             " +
                         "   NVL(TOTAL_EXTRAIDAS, 0) 			             AS NUMERO_COMISIONES_EXTRAER, " +
                         "	DECODE(RECEPCION_CALCULO, 2, 'true', 'false') AS EXTRACCION_FIRMADA         " +
                         "FROM                                                  " +
                         "    SIAG_IF_COMISIONES_SHF                            " +
                         "WHERE                                                 " +
                         "    FECHA_CORTE    >= TO_DATE(?,'dd/mm/yyyy')     AND " +
                         "    FECHA_CORTE    <  TO_DATE(?,'dd/mm/yyyy') + ? AND " +
                         "    INTER_CLAVE    = (                                " +
                         "                        SELECT                        " +
                         "                          IC_IF_SIAG                  " +
                         "                        FROM                          " +
                         "                          COMCAT_IF                   " +
                         "                        WHERE                         " +
                         "                          IC_IF   = ?                 " +
                         "                     )                            AND " +
                         "	  FISO           = ?                                ");

            ps = con.queryPrecompilado(query.toString());
            ps.setString(1, fechaCorte);
            ps.setString(2, fechaCorte);
            ps.setInt(3, 1);
            ps.setInt(4, Integer.parseInt(claveIF));
            ps.setLong(5, Long.parseLong(claveFiso));
            rs = ps.executeQuery();

            // Extraer resultados
            while (rs.next()) {

                // Revisar que solo haya un registro para el IF y Fecha de Corte especificados, en caso
                // contrario enviar excepcion.
                if (++ctaRegistros > 1) {
                    throw new AppException("Error de integridad, se encontr� m�s de un registro.");
                }

                String claveMesRegistro = (rs.getString("CLAVE_MES") == null) ? "" : rs.getString("CLAVE_MES");
                String fiso = (rs.getString("FISO") == null) ? "" : rs.getString("FISO");
                String fechaCorteRegistro = (rs.getString("FECHA_CORTE") == null) ? "" : rs.getString("FECHA_CORTE");
                int numeroMesComision = rs.getInt("NUMERO_MES_COMISION");
                String anioComision = (rs.getString("ANIO_COMISION") == null) ? "" : rs.getString("ANIO_COMISION");
                String numeroComisionesExtraer =
                    (rs.getString("NUMERO_COMISIONES_EXTRAER") == null) ? "" :
                    rs.getString("NUMERO_COMISIONES_EXTRAER");
                String extraccionFirmada =
                    (rs.getString("EXTRACCION_FIRMADA") == null) ? "" : rs.getString("EXTRACCION_FIRMADA");

                String fechaComision = Fecha.getNombreDelMes(numeroMesComision) + " - " + anioComision;

                HashMap registro = new HashMap();
                registro.put("claveMes", claveMesRegistro);
                registro.put("fiso", fiso);
                registro.put("fechaCorte", fechaCorteRegistro);
                registro.put("fechaComision", fechaComision);
                registro.put("numeroComisionesExtraer", numeroComisionesExtraer);
                registro.put("extraccionFirmada", extraccionFirmada);

                lista.add(registro);

            }

        } catch (Exception e) {

            log.error("getResumenComisiones(Exception)");
            log.error("getResumenComisiones.claveIF    = <" + claveIF + ">");
            log.error("getResumenComisiones.claveFiso  = <" + claveFiso + ">");
            log.error("getResumenComisiones.claveMes   = <" + claveMes + ">");
            e.printStackTrace();

            if (e instanceof AppException) {
                throw (AppException) e;
            } else {
                throw new AppException("Ocurri� un error al obtener el Resumen de las Comisiones.");
            }

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true); // Se trata de una tabla remota
                con.cierraConexionDB();
            }
            log.info("getResumenComisiones(S)");

        }

        return lista;

    }

    /**
     *
     * Devuelve el Query que se utilizar para consultar el Detalle de la Comsiones correspondientes
     * al Programa SHF para un mes y a�o especificos y para un Intemediario Financiero
     * en Particular.
     *
     * @throws AppException
     *
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param claveFiso   <tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param fechaCorte  <tt>String</tt> con la fecha de corte, con formato <tt>dd/mm/aaaaa</tt>.
     *
     * @return            <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
     *                    los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     *
     * @since             Fodea 006 - 2012, 13/06/2012 06:10:26 p.m.
     * @author            jshernandez
     *
     */
    public HashMap getQueryDetalleComisiones(String claveIF, String claveFiso, String fechaCorte) throws AppException {

        log.info("getQueryDetalleComisiones(E)");

        HashMap query = new HashMap();
        StringBuffer text = new StringBuffer();
        List parameters = new ArrayList();

        try {

            text.append("SELECT                                           " +
                        "	NOMBRE,                                        " +
                        "	CURP,                                          " +
                        "	GAR_CLAVE_SHF,                                 " +
                        "	MONTO_CREDITO,                                 " +
                        "	INTER_CLAVE,                                   " +
                        "	MONTO_COMISION,                                " +
                        "	MONTO_IVA,                                     " +
                        "	NVL(MONTO_COMISION,0) + NVL(MONTO_IVA,0)  AS MONTO_COBRAR, " +
                        "	TO_CHAR(FECHA_PAGO,'DD/MM/YYYY') AS FECHA_PAGO,            " +
                        "	MONTO_PAGADO,                                  " +
                        "  INTER_CLAVE AS CLAVE_IF_SIAG                   " +
                        "FROM                                             " +
                        " SIAG_COMISIONES_SHF                             " +
                        "WHERE                                            " +
                        "  FISO        =  ?  AND                          " +
                        "  INTER_CLAVE = (                                " +
                        "                  SELECT                         " +
                        "                    IC_IF_SIAG                   " +
                        "                  FROM                           " +
                        "                    COMCAT_IF                    " +
                        "                  WHERE                          " +
                        "                    IC_IF   = ?                  " +
                        "                )   AND                          " +
                        "  FECHA_CORTE >= TO_DATE(?,'DD/MM/YYYY') AND     " +
                        "  FECHA_CORTE <  TO_DATE(?,'DD/MM/YYYY') + ?     " +
                        "ORDER BY NOMBRE ASC, CURP ASC                    ");

            parameters.add(new Long(claveFiso));
            parameters.add(new Integer(claveIF));
            parameters.add(fechaCorte);
            parameters.add(fechaCorte);
            parameters.add(new Integer("1"));

            query.put("text", text.toString());
            query.put("parameters", parameters);

        } catch (Exception e) {

            log.error("getQueryDetalleComisiones(Exception)");
            log.error("getQueryDetalleComisiones.claveIF    = <" + claveIF + ">");
            log.error("getQueryDetalleComisiones.claveFiso  = <" + claveFiso + ">");
            log.error("getQueryDetalleComisiones.fechaCorte = <" + fechaCorte + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar Consulta del Detalle de las Comisiones");

        } finally {

            log.info("getQueryDetalleComisiones(S)");

        }

        return query;

    }

    /**
     *
     * Devuelve el Query que se utilizar para consultar los Totales del Detalle de la Comsiones correspondientes
     * al Programa SHF para un mes y a�o especificos y para un Intemediario Financiero
     * en Particular.
     *
     * @throws AppException
     *
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param claveFiso   <tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param fechaCorte  <tt>String</tt> con la fecha de corte, con formato <tt>dd/mm/aaaaa</tt>.
     *
     * @return            <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
     *                    los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     *
     * @since             Fodea 006 - 2012, 18/06/2012 07:53:11 p.m.
     * @author            jshernandez
     *
     */
    public HashMap getQueryTotalesDetalleComisiones(String claveIF, String claveFiso,
                                                    String fechaCorte) throws AppException {

        log.info("getQueryTotalesDetalleComisiones(E)");

        HashMap query = new HashMap();
        StringBuffer text = new StringBuffer();
        List parameters = new ArrayList();

        try {

            text.append("SELECT                                                      " +
                        "	SUM(MONTO_CREDITO)               AS TOTAL_MONTO_CREDITO,  " +
                        "	SUM(MONTO_COMISION)              AS TOTAL_MONTO_COMISION, " +
                        "	SUM(MONTO_IVA)                   AS TOTAL_MONTO_IVA,      " +
                        "	SUM( NVL(MONTO_COMISION,0) + NVL(MONTO_IVA,0) ) AS TOTAL_MONTO_COBRAR, " +
                        "	SUM(MONTO_PAGADO)                AS TOTAL_MONTO_PAGADO    " +
                        "FROM                                             " +
                        " SIAG_COMISIONES_SHF                             " +
                        "WHERE                                            " +
                        "  FISO        =  ?  AND                          " +
                        "  INTER_CLAVE = (                                " +
                        "                  SELECT                         " +
                        "                    IC_IF_SIAG                   " +
                        "                  FROM                           " +
                        "                    COMCAT_IF                    " +
                        "                  WHERE                          " +
                        "                    IC_IF   = ?                  " +
                        "                )   AND                          " +
                        "  FECHA_CORTE >= TO_DATE(?,'DD/MM/YYYY') AND     " +
                        "  FECHA_CORTE <  TO_DATE(?,'DD/MM/YYYY') + ?     ");

            parameters.add(new Long(claveFiso));
            parameters.add(new Integer(claveIF));
            parameters.add(fechaCorte);
            parameters.add(fechaCorte);
            parameters.add(new Integer("1"));

            query.put("text", text.toString());
            query.put("parameters", parameters);

        } catch (Exception e) {

            log.error("getQueryTotalesDetalleComisiones(Exception)");
            log.error("getQueryTotalesDetalleComisiones.claveIF    = <" + claveIF + ">");
            log.error("getQueryTotalesDetalleComisiones.claveFiso  = <" + claveFiso + ">");
            log.error("getQueryTotalesDetalleComisiones.fechaCorte = <" + fechaCorte + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar Consulta de los Totales del Detalle de las Comisiones");

        } finally {

            log.info("getQueryTotalesDetalleComisiones(S)");

        }

        return query;

    }

    /**
     *
     * Delvuelve la clave del estatus de recepcion de comsiones por parte del Intermediario Financiero, para
	�* un Fiso y un mes en particular ( Fecha de Corte ).
     *
     * @throws AppException
     *
     * @param claveFiso   <tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param fechaCorte  <tt>String</tt> con la fecha de corte, con formato <tt>dd/mm/aaaaa</tt>.
     *
     * @return <tt>String</tt> con la clave del estatus de recepcion de comisiones.
     *
     * @since             Fodea 006 - 2012, 15/06/2012 01:52:44 p.m.
     * @author            jshernandez
     *
     */
    public String getEstatusRecepcionComisiones(String claveFiso, String claveIF,
                                                String fechaCorte) throws AppException {

        log.info("getEstatusRecepcionComisiones(E)");

        String claveEstatus = "";

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {

            // Conectarse a la Base de datos
            con.conexionDB();

            // Realizar Consulta
            query.append("SELECT                                               " +
                         "   RECEPCION_CALCULO                                 " +
                         "FROM                                                 " +
                         "   SIAG_IF_COMISIONES_SHF                            " +
                         "WHERE                                                " +
                         "   FISO           = ?                            AND " +
                         "   INTER_CLAVE    = (                                " +
                         "                        SELECT                       " +
                         "                          IC_IF_SIAG                 " +
                         "                        FROM                         " +
                         "                          COMCAT_IF                  " +
                         "                        WHERE                        " +
                         "                          IC_IF   = ?                " +
                         "                     )                           AND " +
                         "   FECHA_CORTE    >= TO_DATE(?,'dd/mm/yyyy')     AND " +
                         "   FECHA_CORTE    <  TO_DATE(?,'dd/mm/yyyy') + ?     ");

            ps = con.queryPrecompilado(query.toString());
            ps.setLong(1, Long.parseLong(claveFiso));
            ps.setInt(2, Integer.parseInt(claveIF));
            ps.setString(3, fechaCorte);
            ps.setString(4, fechaCorte);
            ps.setInt(5, 1);
            rs = ps.executeQuery();

            if (rs.next()) {
                claveEstatus = (rs.getString("RECEPCION_CALCULO") == null) ? "" : rs.getString("RECEPCION_CALCULO");
            }

        } catch (Exception e) {

            log.error("getEstatusRecepcionComisiones(Exception)");
            log.error("getEstatusRecepcionComisiones.claveFiso  = <" + claveFiso + ">");
            log.error("getEstatusRecepcionComisiones.claveIF    = <" + claveIF + ">");
            log.error("getEstatusRecepcionComisiones.fechaCorte = <" + fechaCorte + ">");

            e.printStackTrace();

            throw new AppException("Ocurri� un error al consultar el estatus de recepci�n de las comisiones.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true); // Se trata de una tabla remota
                con.cierraConexionDB();
            }

            log.info("getEstatusRecepcionComisiones(S)");

        }

        return claveEstatus;

    }

    /**
     *
     * Actualiza el estatus de recepcion de comsiones por parte del Intermediario Financiero, correspondientes
	�* un Fiso y un mes en particular ( Fecha de Corte ).
     *
     * @throws AppException
     *
     * @param claveFiso   <tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param fechaCorte  <tt>String</tt> con la fecha de corte, con formato <tt>dd/mm/aaaaa</tt>.
     * @param estatus     <tt>String</tt> con la clave del estatus de recepcion de comisiones a asignar.
     *
     * @since             Fodea 006 - 2012, 15/06/2012 01:52:56 p.m.
     * @author            jshernandez
     *
     */
    public void actualizaStatusRecepcionComisiones(String claveFiso, String claveIF, String fechaCorte,
                                                   String estatus) throws AppException {

        log.info("actualizaStatusRecepcionComisiones(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        PreparedStatement ps = null;

        boolean exito = true;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Extraer clave de Intermediario Financiero en el SIAG
            query.append("UPDATE                                        " +
                         "   SIAG_IF_COMISIONES_SHF                     " +
                         "SET                                           " +
                         "   RECEPCION_CALCULO = ?                      " +
                         "WHERE                                         " +
                         "   FISO        = ? AND                        " +
                         "   INTER_CLAVE = (                            " +
                         "       SELECT                                 " +
                         "          IC_IF_SIAG                          " +
                         "       FROM                                   " +
                         "	        COMCAT_IF                           " +
                         "       WHERE                                  " +
                         "	        IC_IF = ?                           " +
                         "   )               AND                        " +
                         "  FECHA_CORTE >= TO_DATE(?,'DD/MM/YYYY')  AND " +
                         "  FECHA_CORTE <  TO_DATE(?,'DD/MM/YYYY')  + 1 ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(estatus));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIF));
            ps.setString(4, fechaCorte);
            ps.setString(5, fechaCorte);
            ps.executeUpdate();

        } catch (Exception e) {

            log.error("actualizaStatusRecepcionComisiones(Exception)");
            log.error("actualizaStatusRecepcionComisiones.claveFiso  = <" + claveFiso + ">");
            log.error("actualizaStatusRecepcionComisiones.claveIF    = <" + claveIF + ">");
            log.error("actualizaStatusRecepcionComisiones.fechaCorte = <" + fechaCorte + ">");
            log.error("actualizaStatusRecepcionComisiones.estatus    = <" + estatus + ">");
            e.printStackTrace();

            exito = false;
            throw new AppException("Ocurri� un error al actualizar el estatus de la recepci�n de comisiones.");

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

            log.info("actualizaStatusRecepcionComisiones(S)");

        }

    }

    /**
     *
     * M�todo que se encarga de consultar de las tablas del SIAG la clave de contragarante (numero FISO)
     * asociado a un intermediario financiero.
     * @throws AppException
     *
     * @param	claveIF	Clave del Intermediario Financiero, como aparece en la tabla <tt>COMCAT_IF.IC_IF</tt>
     * @return  Clave del FISO ( contragarante ).
     *
     */
    public String getClaveFISO(String claveIF) throws AppException {

        log.info("getClaveFISO(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        String claveFISO = "";

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            query.append(" SELECT                             " + "    FSO_CLAVE AS CLAVE_FISO         " +
                         " FROM                               " + "    SIAG_FISO_INTERMEDIARIOS SFI,   " +
                         "    COMCAT_IF                CIF    " + " WHERE                              " +
                         "    SFI.CLAVE = CIF.IC_IF_SIAG AND  " + "    CIF.IC_IF = ?                   ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            int cuentaRegistros = 0;
            while (rs.next()) {
                cuentaRegistros++;
                claveFISO = rs.getString("CLAVE_FISO") == null ? "" : rs.getString("CLAVE_FISO");
            }

            if (cuentaRegistros > 1) {
                throw new AppException("Error de integridad, se encontr� m�s de una Clave de Contragarante");
            }

        } catch (Exception e) {

            log.error("getClaveFISO(Exception)");
            log.error("getClaveFISO.claveIF = <" + claveIF + ">");
            e.printStackTrace();

            if (e instanceof AppException) {
                throw (AppException) e;
            } else {
                throw new AppException("Ocurri� un error al consultar la clave del contragarante.");
            }

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true); // Se trata de una tabla remota
                con.cierraConexionDB();
            }
            log.info("getClaveFISO(S)");

        }

        return claveFISO;

    }

    /**
     *
     * Devuelve el Resumen del Estatus de Obtencion de Comisiones de del Programa SHF para un intermediario financiero
     * en especifico y para un rango de meses, comenzando por el mes: <tt>claveMesInicial</tt> y terminando con el mes:
     * <tt>claveMesFinal</tt>.
     *
     * @throws AppException
     *
     * @param claveIF  			<tt>String</tt> con la clave del intermediario financiero, como
     *						 			aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param claveFiso  		<tt>String</tt> clave del Fiso con el cual opera el IF.
     * @param claveMesInicial 	<tt>String</tt> clave del Mes Inicial con formato AAAADD.
     * @param claveMesFinal 	<tt>String</tt> clave del Mes Final con formato AAAADD.
     *
     * @return         			<tt>HashMap</tt> con los siguientes elementos:
     *	                			claveMes(llave), extraccionFirmada(valor).
     *
     * @since   Fodea 006 - 2012, 22/06/2012 01:41:05 p.m.
     * @author  jshernandez
     *
     */
    public HashMap getEstatusObtencionComisiones(String claveIF, String claveFiso, String claveMesInicial,
                                                 String claveMesFinal) throws AppException {

        log.info("getEstatusObtencionComisiones(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        ResultSet rs = null;
        PreparedStatement ps = null;

        HashMap estatus = new HashMap();

        try {

            // Obtener la Fecha de Corte Inicial
            int anioInicial = Integer.parseInt(claveMesInicial.substring(0, 4));
            int mesInicial = Integer.parseInt(claveMesInicial.substring(4, 6));

            int anioFinal = Integer.parseInt(claveMesFinal.substring(0, 4));
            int mesFinal = Integer.parseInt(claveMesFinal.substring(4, 6));

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Realizar Consulta
            query.append("SELECT  " + "   TO_CHAR(FECHA_CORTE,'YYYYMM')                AS CLAVE_MES,                 " +
                         "	DECODE(RECEPCION_CALCULO, 2, 'true', 'false') AS EXTRACCION_FIRMADA         " +
                         "FROM                                                  " +
                         "    SIAG_IF_COMISIONES_SHF                            " +
                         "WHERE                                                 " +
                         "    INTER_CLAVE    = (                                " +
                         "                        SELECT                        " +
                         "                          IC_IF_SIAG                  " +
                         "                        FROM                          " +
                         "                          COMCAT_IF                   " +
                         "                        WHERE                         " +
                         "                          IC_IF   = ?                 " +
                         "                     )                            AND " +
                         "	  FISO           = ?  AND ");

            query.append("( ");
            int cuentaCondiciones = 0;
            for (int anio = anioInicial; anio <= anioFinal; anio++) {

                int primerMes = 0;
                int ultimoMes = 0;
                if (anioInicial == anioFinal) {
                    primerMes = mesInicial;
                    ultimoMes = mesFinal;
                } else if (anioInicial < anioFinal && anio == anioInicial) {
                    primerMes = mesInicial;
                    ultimoMes = 12;
                } else if (anioInicial < anioFinal && anio == anioFinal) {
                    primerMes = 1;
                    ultimoMes = mesFinal;
                } else if (anioInicial < anioFinal) {
                    primerMes = 1;
                    ultimoMes = 12;
                }

                for (int mes = primerMes; mes <= ultimoMes; mes++) {
                    if (cuentaCondiciones > 0)
                        query.append(" OR ");
                    query.append(" ( ");
                    query.append("    FECHA_CORTE    >= TO_DATE(?,'dd/mm/yyyy')     AND ");
                    query.append("    FECHA_CORTE    <  TO_DATE(?,'dd/mm/yyyy') + ?     ");
                    query.append(" ) ");
                    cuentaCondiciones++;
                }

            }
            query.append(" ) ");

            ps = con.queryPrecompilado(query.toString());
            int indice = 1;
            ps.setInt(indice++, Integer.parseInt(claveIF));
            ps.setLong(indice++, Long.parseLong(claveFiso));
            for (int anio = anioInicial; anio <= anioFinal; anio++) {

                int primerMes = 0;
                int ultimoMes = 0;
                if (anioInicial == anioFinal) {
                    primerMes = mesInicial;
                    ultimoMes = mesFinal;
                } else if (anioInicial < anioFinal && anio == anioInicial) {
                    primerMes = mesInicial;
                    ultimoMes = 12;
                } else if (anioInicial < anioFinal && anio == anioFinal) {
                    primerMes = 1;
                    ultimoMes = mesFinal;
                } else if (anioInicial < anioFinal) {
                    primerMes = 1;
                    ultimoMes = 12;
                }

                for (int mes = primerMes; mes <= ultimoMes; mes++) {

                    String numeroMes = (mes < 10 ? "0" : "") + mes;
                    String fechaCorte = Fecha.getUltimoDiaDelMes("01/" + numeroMes + "/" + anio, "dd/MM/yyyy");

                    ps.setString(indice++, fechaCorte);
                    ps.setString(indice++, fechaCorte);
                    ps.setInt(indice++, 1);

                }

            }
            rs = ps.executeQuery();

            // Extraer resultados
            while (rs.next()) {

                String claveMes = (rs.getString("CLAVE_MES") == null) ? "" : rs.getString("CLAVE_MES");
                String extraccionFirmada =
                    (rs.getString("EXTRACCION_FIRMADA") == null) ? "false" : rs.getString("EXTRACCION_FIRMADA");

                estatus.put(claveMes, extraccionFirmada);

            }

        } catch (Exception e) {

            log.error("getEstatusObtencionComisiones(Exception)");
            log.error("getEstatusObtencionComisiones.claveIF          = <" + claveIF + ">");
            log.error("getEstatusObtencionComisiones.claveFiso        = <" + claveFiso + ">");
            log.error("getEstatusObtencionComisiones.claveMesInicial  = <" + claveMesInicial + ">");
            log.error("getEstatusObtencionComisiones.claveMesFinal    = <" + claveMesFinal + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al obtener el Resumen de las Comisiones.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true); // Se trata de una tabla remota
                con.cierraConexionDB();
            }
            log.info("getEstatusObtencionComisiones(S)");

        }

        return estatus;

    }

    /**
     *
     * Revisa si para el mes-a�o especificados se han enviado registros con estatus:
     * "env�o para validaci�n y carga".
     * @throws AppException
     *
     * @param claveIF   <tt>String</tt> con la clave del intermediario financiero, como
     *						  aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param anioCarga <tt>String</tt> con el n�mero del anio de la carga.
     * @param mesCarga  <tt>String</tt> con el n�mero del mes de la carga.
     *
     * @return <tt>true</tt> en caso de que el intermediario financiero tenga una clave SIAG
     *			  asignada y <tt>false</tt>, en caso contrario.
     *
     * @since          Fodea 006 - 2012, 25/06/2012 12:47:27 a.m.
     * @author         jshernandez
     *
     */
    public boolean existeEnvioComisionesParaValidacionYCarga(String claveIF, String anioCarga,
                                                             String mesCarga) throws AppException {

        log.info("existeEnvioComisionesParaValidacionYCarga(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer query = new StringBuffer();
        PreparedStatement ps = null;
        ResultSet rs = null;

        boolean existeEnvioValidacion = false;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Extraer clave de Intermediario Financiero en el SIAG
            query.append("SELECT                   " + "  DECODE( IC_SITUACION, 1, 'true', 'false') AS EXISTE_ENVIO " +
                         "FROM                     " + "	GTI_ESTATUS_SOLIC      " + "WHERE                    " +
                         "  IC_TIPO_OPERACION = 9 AND  " + "	IG_ANIO_CARGA     = ? AND  " +
                         "  IG_MES_CARGA      = ? AND  " + "  IC_IF_SIAG        = (  " + "  	SELECT              " +
                         "     	IC_IF_SIAG       " + "  	FROM                " + "     	COMCAT_IF        " +
                         "  	WHERE               " + "     	IC_IF       = ?  " + "  ) ");
            // TODO: PENDIENTE REVISAR LLAVE PRIMARIA DE GTI_ESTATUS_SOLIC

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(anioCarga));
            ps.setInt(2, Integer.parseInt(mesCarga));
            ps.setInt(3, Integer.parseInt(claveIF));
            rs = ps.executeQuery();

            if (rs.next()) {
                existeEnvioValidacion = "true".equals(rs.getString("EXISTE_ENVIO")) ? true : false;
            } else {
                existeEnvioValidacion = false;
            }

        } catch (Exception e) {

            log.error("existeEnvioComisionesParaValidacionYCarga(Exception)");
            log.error("existeEnvioComisionesParaValidacionYCarga.claveIF   = <" + claveIF + ">");
            log.error("existeEnvioComisionesParaValidacionYCarga.anioCarga = <" + anioCarga + ">");
            log.error("existeEnvioComisionesParaValidacionYCarga.mesCarga  = <" + mesCarga + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al revisar que no se hayan enviado registros previamente para validaci�n y carga.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("existeEnvioComisionesParaValidacionYCarga(S)");

        }

        return existeEnvioValidacion;

    }

    /**
     * Lee el archivo especificado, el cual debe cumplir con el layout de comisiones.
     * @throws AppException
     *
     * @param directorioTemporal    <tt>String</tt> con el directorio donde se encuentra el archivo a procesar
     * @param archivo               <tt>String</tt> con la ruta del archivo a procesar, puede ser
     * 	                          un archivo de texto(.txt) o un archivo comprimido(.zip)
     * @param numeroRegistros       <tt>String</tt> con las cifras de control. Numero de registros.
     * @param montoTotalComisiones  <tt>String</tt> con las cifras de control. Monto total.
     * @param claveIF               <tt>String</tt> con la clave del Intermediario Financiero.
     * @param claveMes 	           <tt>String</tt> clave del Mes para el cual se solicita el reporte
     *						 	           con formato AAAAMM.
     *
     * @return                      Objeto <tt>ResultadosGar</tt> con el resultado de las validaciones.
     *
     * @since          Fodea 006 - 2012, 25/06/2012 12:33:26 p.m.
     * @author         jshernandez
     *
     */
    public ResultadosGar procesarCargaComisiones(String directorioTemporal, String archivo, String numeroRegistros,
                                                 String montoTotalComisiones, String claveIF,
                                                 String claveMes) throws AppException {

        log.info("procesarCargaComisiones(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer(256);

        ResultadosGar retorno = new ResultadosGar(ResultadosGar.CARGA_MASIVA);
        boolean errorInesperado = false;

        List clavesFinanciamientoSHF = new ArrayList();
        BufferedReader buffer = null;
        try {

            // Inicializar variables de control
            int controlNumeroRegistros = Integer.parseInt(numeroRegistros);
            BigDecimal controlMontoTotalComisiones = new BigDecimal(montoTotalComisiones);

            // Obtener extension del archivos de datos
            String extension = archivo.substring(archivo.length() - 3, archivo.length());

            // Si el archivo proporcionado tiene extension ZIP, extraer archivo TXT.
            if ("zip".equalsIgnoreCase(extension)) {
                try {
                    archivo = descomprimirArchivo(directorioTemporal, archivo);
                    extension = archivo.substring(archivo.length() - 3, archivo.length());
                    if (!"txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es TXT.\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es TXT.\n");
                }
            }

            // Inicializacion de variables
            VectorTokenizer vt = null;
            Vector campos = null;
            String contenidoLinea = "";
            int claveIFSIAG = 0;
            int lineas = 0;
            int numLinea = 1;
            int cuentaRegistros = 0;
            int iNumRegTrans = 0;
            BigDecimal sumatoriaMontoTotalComisiones = new BigDecimal(0.00);

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del Intermediario Financiero
            query.setLength(0);
            query.append("SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ?");
            ps = con.queryPrecompilado(query.toString());
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveIFSIAG = rs.getInt("IC_IF_SIAG");
            }
            rs.close();
            ps.close();

            // Obtener ID del Proceso de Carga
            retorno.setIcProceso(calculaIcProceso());

            // Preparar Sentencia para Insertar registros en la tabla temporal GTITMP_CONTENIDO_ARCH
            query.setLength(0);
            query.append("INSERT INTO               " + " GTITMP_CONTENIDO_ARCH    " + "   (                      " +
                         "      IC_PROCESO, IC_LINEA, CG_CONTENIDO " + "   )                      " +
                         " VALUES                   " + "   (                      " + "    ?,?,?                 " +
                         "   )                      ");
            ps = con.queryPrecompilado(query.toString());

            // Realizar validacion de los registros si no se ha presentado ningun error previamente
            if (retorno.getErrores().length() == 0) {

                // Abrir archivo de datos para su lectura
                java.io.File file = new java.io.File(archivo);
                buffer = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));

                for (lineas = 0, numLinea = 1; (contenidoLinea = buffer.readLine()) != null; lineas++, numLinea++) {

                    // Inicializar variables
                    BigDecimal montoTotalPorRegistro = null;
                    retorno.setErrorLinea(false);

                    contenidoLinea = contenidoLinea.trim();

                    // Si la linea viene vac�a no hacerle caso
                    if (contenidoLinea.length() == 0) {
                        continue;
                        // Si el registro excede la longitud maxima, recortarlo.
                    } else if (contenidoLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
                        contenidoLinea = contenidoLinea.substring(0, GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH);
                        cuentaRegistros++;
                        // Incrementar conteo de lineas.
                    } else {
                        cuentaRegistros++;
                    }

                    // Extraer los campos separados por "@"
                    vt = new VectorTokenizer(contenidoLinea, "@");
                    campos = vt.getValuesVector();

                    // Validar registros de comisiones
                    this.validarRegistroComisiones(claveIFSIAG, numLinea, contenidoLinea, retorno,
                                                   clavesFinanciamientoSHF);

                    // Obtener Monto Total a cobrar.
                    try {
                        montoTotalPorRegistro =
                            new BigDecimal(((String) campos.get(9)).trim()); // Monto Pagado a la SHF
                    } catch (Throwable e) {
                        montoTotalPorRegistro = new BigDecimal(0);
                    }

                    // Si no se present� ningun error en el registro de comsiones
                    if (!retorno.getErrorLinea()) {

                        // Agregar registro a la lista de registros sin error
                        retorno.appendCorrectos("" + (String) campos.get(2) + "\n");
                        retorno.incNumRegOk();
                        retorno.addSumRegOk(montoTotalPorRegistro);

                        //Insertar registro en tabla temporal
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {

                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, contenidoLinea);
                            ps.execute();

                            // Dar commit cada 3500 registros
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }

                        }

                        //	El registro proporcionado present� errores
                    } else {

                        // Agregar registro a la lista de registros con error.
                        retorno.incNumRegErr();
                        retorno.addSumRegErr(montoTotalPorRegistro);

                    }

                    // Actualizar sumatoria total de montos
                    sumatoriaMontoTotalComisiones = sumatoriaMontoTotalComisiones.add(montoTotalPorRegistro);

                    // Actualizar conteo de registros en espera de "commit"
                    iNumRegTrans++;

                } // for

            }
            ps.close();

            // Revisar que el n�mero de registros validados coincida con el n�mero de registros especificados por el usuario.
            if (cuentaRegistros != controlNumeroRegistros) {
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            }

            // Revisar que el monto total a cobrar coincida con la sumatoria de los montos de todos los registros validados
            sumatoriaMontoTotalComisiones = sumatoriaMontoTotalComisiones.setScale(2, BigDecimal.ROUND_HALF_UP);
            controlMontoTotalComisiones = controlMontoTotalComisiones.setScale(2, BigDecimal.ROUND_HALF_UP);
            if (sumatoriaMontoTotalComisiones.compareTo(controlMontoTotalComisiones) != 0) {
                log.info("procesarCargaComisiones: La sumatoria de los montos    = " +
                         sumatoriaMontoTotalComisiones.toPlainString());
                log.info("procesarCargaComisiones: La cifra de control capturada = " +
                         controlMontoTotalComisiones.toPlainString());
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }

            //Si no hay errores de cifras de control... establece claves de financiamiento
            if (retorno.getCifras().length() == 0) {
                //------------------------ Inicio mensajes para debug ---------------------------
                String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
                log.info("procesarCargaComisiones(" + _fechaMsg + "_" + retorno.getIcProceso() +
                         ")::Cifra de Control OK. clavesFinanciamientoSHF.size()=" + clavesFinanciamientoSHF.size());
                //-------------------------- Fin mensajes para debug ----------------------------

                retorno.setClavesFinan(clavesFinanciamientoSHF);
            }

        } catch (OutOfMemoryError om) { // Se acabo la memoria

            log.error("procesarCargaComisiones(Exception)");
            log.error("procesarCargaComisiones.directorioTemporal   = <" + directorioTemporal + ">");
            log.error("procesarCargaComisiones.archivo              = <" + archivo + ">");
            log.error("procesarCargaComisiones.numeroRegistros      = <" + numeroRegistros + ">");
            log.error("procesarCargaComisiones.montoTotalComisiones = <" + montoTotalComisiones + ">");
            log.error("procesarCargaComisiones.claveIF              = <" + claveIF + ">");
            log.error("procesarCargaComisiones::Free Memory         = <" + Runtime.getRuntime().freeMemory() + ">");

            errorInesperado = true;

            om.printStackTrace();

            throw new AppException("Se acab� la memoria.");

        } catch (Exception e) {

            log.error("procesarCargaComisiones(Exception)");
            log.error("procesarCargaComisiones.directorioTemporal   = <" + directorioTemporal + ">");
            log.error("procesarCargaComisiones.archivo              = <" + archivo + ">");
            log.error("procesarCargaComisiones.numeroRegistros      = <" + numeroRegistros + ">");
            log.error("procesarCargaComisiones.montoTotalComisiones = <" + montoTotalComisiones + ">");
            log.error("procesarCargaComisiones.claveIF              = <" + claveIF + ">");
            e.printStackTrace();

            errorInesperado = true;

            if (e instanceof AppException) {
                throw (AppException) e;
            } else {
                throw new AppException("Ocurri� un error al validar las comisiones");
            }

        } finally {

            if (buffer != null) {
                try {
                    buffer.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {

                if (errorInesperado || retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0) {
                    con.terminaTransaccion(false);
                } else {
                    con.terminaTransaccion(true);
                }
                con.cierraConexionDB();

            }
            log.info("procesarCargaComisiones(S)");

        }

        return retorno;

    }

    /**
     * Validaciones de la Carga de Comisiones.
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * ResultadosGar, el cual llevara el registro de los errores de validaci�n.
     * @throws AppException
     *
     * @param claveIFSIAG    <tt>String</tt> con la Clave SIAG del Intermediario Financiero.
     * @param numeroLinea    <tt>int</tt> con el numero de linea que se est� validando.
     * @param contenidoLinea <tt>String</tt> con los campos delimitados por el caracter <tt>'@'</tt>.
     * @param retorno        Objeto de <tt>ResultadosGar</tt>, donde se agrega el resultado de la
     *                       validaci�n.
     * @param clavesFinanciamientoSHF <tt>List</tt> donde se almacena las clave de financiamiento de SHF que
     *								 pasaron las validaciones de forma exitosa.
     *
     * @since   Fodea 006 - 2012, 26/06/2012 12:19:29 a.m.
     * @author  jshernandez
     *
     */
    public void validarRegistroComisiones(int claveIFSIAG, int numeroLinea, String contenidoLinea,
                                          ResultadosGar retorno, List clavesFinanciamientoSHF) throws AppException {

        log.info("validarRegistroComisiones(E)");

        try {

            // Validar que el registro no exceda el tama�o m�ximo permitido
            if (contenidoLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El tama�o del registro es demasiado grande para ser procesado. \n");
                retorno.setErrorLinea(true);
                return;
            }

            // Extraer campos
            VectorTokenizer vt = new VectorTokenizer(contenidoLinea, "@");
            List campos = vt.getValuesVector();

            // Validar que el �ltimo caracter sea la '@'
            if ((contenidoLinea.charAt(contenidoLinea.length() - 1)) != '@') {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " La cadena termina incorrectamente, debe llevar '@' al final \n");
                retorno.setErrorLinea(true);
                return;
            }

            // Determinar el numero de elementos esperados
            int numElementosEsperados = 10;
            int recibidos = campos.size() - 1;

            // Validar que se hayan enviado todos los campos
            if (recibidos != numElementosEsperados) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El numero de campos es incorrecto. Esperados(" + numElementosEsperados +
                                      ") Recibidos(" + recibidos + ") \n");
                retorno.setErrorLinea(true);
                return;
            }

            // VALIDACIONES
            String campo = null;
            /*
						1	Nombre o Raz�n Social del Acreditado		-El valor del Nombre o Raz�n Social del Acreditado es requerido.
																					-La longitud del Nombre o Raz�n Social del Acreditado es incorrecta.
			*/
            campo = (String) campos.get(0);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Nombre o Raz�n Social del Acreditado' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else {
                validarLongitudCadena(numeroLinea, campo, "Nombre o Raz�n Social del Acreditado", retorno, 240);
            }
            /*
						2	CURP													-El valor del CURP del acreditado es requerido.
																					-El valor del CURP del acreditado es incorrecto.
			*/
            validaCURP(numeroLinea, (String) campos.get(1), "CURP", retorno, true);
            /*
						3	Clave del Financiamiento						-El valor de la clave del financiamiento es requerido.
																					-La longitud de la clave del financiamiento es incorrecta.
			*/
            campo = (String) campos.get(2);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Clave del Financiamiento' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (campo.length() > 30) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Clave del Financiamiento' no cumple con la longitud m�xima permitida \n");
                retorno.setErrorLinea(true);
            } else {
                clavesFinanciamientoSHF.add(campo);
            }
            /*
						4	Monto del Financiamiento						-El valor del Monto del financiamiento es requerido.
																					-La longitud del Monto del financiamiento es incorrecta.
																					-El valor del Monto del financiamiento no es un n�mero v�lido.
			*/
            campo = (String) campos.get(3);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto del Financiamiento' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esDecimal(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto del Financiamiento' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 17, 2)) { // 17,2 ( => 15 enteros, 2 decimales )
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto del Financiamiento' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            }
            /*
						5	Clave del Intermediario Financiero			-El valor de la clave del intermediario es requerido.
																					-La clave del intermediario financiero es incorrecta.
																					-La longitud de la clave del intermediario es incorrecta. ( OMITIDA )
																					-El valor de la clave del intermediario debe ser un n�mero entero.
			*/
            campo = (String) campos.get(4);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Clave del Intermediario Financiero' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esNumero(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Clave del Intermediario Financiero' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 5, 0)) { // 5 enteros
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Clave del Intermediario Financiero' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            } else {
                validaIntermediario(numeroLinea, campo, "Clave del Intermediario Financiero", claveIFSIAG, retorno);
            }
            /*
						6	Monto Comisi�n a Cobrar							-El valor del Monto comisi�n a Cobrar es requerido.
																					-La longitud del Monto comisi�n a Cobrar es incorrecta.
																					-El valor del Monto comisi�n a Cobrar no es un n�mero v�lido.
			*/
            campo = (String) campos.get(5);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Comisi�n a Cobrar' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esDecimal(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Comisi�n a Cobrar' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 26, 6)) { // 26,6 ( => 20 enteros, 6 decimales )
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Comisi�n a Cobrar' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            }
            /*
						7	Monto IVA											-El valor del Monto IVA es requerido.
																					-La longitud del Monto IVA es incorrecta.
																					-El valor del Monto IVA no es un n�mero v�lido.
			*/
            campo = (String) campos.get(6);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto IVA' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esDecimal(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto IVA' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 26, 6)) { // 26,6 ( => 20 enteros, 6 decimales )
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto IVA' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            }
            /*
						8	Monto Total a Cobrar								-El valor del Total monto a cobrar es requerido.
																					-La longitud del Total monto a cobrar es incorrecta.
																					-El valor del Total monto a cobrar  no es un n�mero v�lido.
			*/
            campo = (String) campos.get(7);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Total a Cobrar' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esDecimal(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Total a Cobrar' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 26, 6)) { // 26,6 ( => 20 enteros, 6 decimales )
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Total a Cobrar' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            }
            /*
						9	Fecha de pago de la comisi�n (AAAAMMDD)	-El valor de la Fecha de pago de la comisi�n es requerido.
																					-El valor de la Fecha de pago de la comisi�n es incorrecto, formato AAAAMMDD
			*/
            campo = (String) campos.get(8);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Fecha de pago de la comisi�n (AAAAMMDD)' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else {
                validaFecha(numeroLinea, campo, "Fecha de pago de la comisi�n (AAAAMMDD)", retorno);
            }
            /*
						10	Monto Pagado a la SHF							-El valor del Monto Pagado a la SHF es requerido.
																					-La longitud del Monto Pagado a la SHF es incorrecta.
																					-El valor del Monto Pagado a la SHF no es un n�mero v�lido.
			*/
            campo = (String) campos.get(9);
            if (campo == null || "".equals(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Pagado a la SHF' es obligatorio. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.esDecimal(campo)) {
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Pagado a la SHF' es incorrecto. \n");
                retorno.setErrorLinea(true);
            } else if (!Comunes.precisionValida(campo, 26, 6)) { // 26,6 ( => 20 enteros, 6 decimales )
                retorno.appendErrores("Error en la linea:" + numeroLinea +
                                      " El valor del campo 'Monto Pagado a la SHF' no cumple con la longitud requerida. \n");
                retorno.setErrorLinea(true);
            }

        } catch (OutOfMemoryError om) { // Se acabo la memoria

            log.error("validarRegistroComisiones(Exception)");
            log.error("validarRegistroComisiones.claveIFSIAG    = <" + claveIFSIAG + ">");
            log.error("validarRegistroComisiones.numeroLinea    = <" + numeroLinea + ">");
            log.error("validarRegistroComisiones.contenidoLinea = <" + contenidoLinea + ">");
            log.error("validarRegistroComisiones.retorno        = <" + retorno + ">");
            log.error("validarRegistroComisiones::Free Memory   = <" + Runtime.getRuntime().freeMemory() + ">");

            om.printStackTrace();

            throw new AppException("Se acab� la memoria.");

        } catch (Exception e) {

            log.error("validarRegistroComisiones(Exception)");
            log.error("validarRegistroComisiones.claveIFSIAG    = <" + claveIFSIAG + ">");
            log.error("validarRegistroComisiones.numeroLinea    = <" + numeroLinea + ">");
            log.error("validarRegistroComisiones.contenidoLinea = <" + contenidoLinea + ">");
            log.error("validarRegistroComisiones.retorno        = <" + retorno + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al validar uno de los registros.");

        } finally {

            log.info("validarRegistroComisiones(S)");

        }

    }

    /**
     *
     * Realiza la transmision de las comisiones, las cuales ya han sido previamente validadas.
     * @throws AppException
     *
     * @param claveProceso          <tt>String</tt> con la clave del proceso de carga.
     * @param claveIF               <tt>String</tt> con la clave del Intermediario Financiero.
     * @param numeroRegistros       <tt>String</tt> con las cifras de control. Numero de registros.
     * @param montoTotalComisiones  <tt>String</tt> con las cifras de control. Monto total.
     * @param claveMes 	           <tt>String</tt> clave del Mes para el cual se solicita el reporte
     *						 	           con formato AAAAMM.
     * @param loginUsuario          <tt>String</tt> con el login del usuario.
     * @param acuseFirmaDigital	  <tt>String</tt> con el numero de acuse de la firma digital.
     *
     * @param return                Objeto de <tt>ResultadosGar</tt>, donde se agrega el resultado de la
     *                              validaci�n.
     *
     * @since   Fodea 006 - 2012, 26/06/2012 11:42:33 p.m.
     * @author  jshernandez
     *
     */
    public ResultadosGar transmitirComisiones(String claveProceso, String claveIF, String numeroRegistros,
                                              String montoTotalComisiones, String claveMes, String loginUsuario,
                                              String acuseFirmaDigital) throws AppException {

        log.info("transmitirComisiones(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer(256);

        boolean exito = true;
        ResultadosGar resultados = new ResultadosGar();

        final int COMISIONES = 9;
        final int ENVIO_VALIDACION_Y_CARGA = 1;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            BigDecimal folio = null;
            BigDecimal numeroNafinElectronico = null;
            Integer claveIFSIAG = null;

            String fecha = "";
            String hora = "";
            String fechaValor = "";
            String anio = "";
            int claveContragarante = 0;

            // Obtener el a�o de la carga
            String anioCarga = claveMes.substring(0, 4);
            // Obtener el mes de la carga
            String mesCarga = claveMes.substring(4, 6);
            // Obtener la fecha de corte
            String fechaCorte = Fecha.getUltimoDiaDelMes("01/" + mesCarga + "/" + anioCarga, "dd/MM/yyyy");
            // Generar� un n�mero de folio para todas las solicitudes que sean registradas
            Registros registros = this.getDatosFolio(claveIF);
            if (registros.next()) {

                folio = new BigDecimal(registros.getString("folio"));
                numeroNafinElectronico = new BigDecimal(registros.getString("ic_nafin_electronico"));

                try {
                    claveIFSIAG = new Integer(registros.getString("ic_if_siag"));
                } catch (Exception e) {
                    claveIFSIAG = new Integer(0);
                }

                fecha = registros.getString("fecha");
                hora = registros.getString("hora");
                anio = registros.getString("anio");
                fechaValor = fecha + " " + hora; // FECHA_VALOR

            }
            // Obtener la Clave del Contragarante
            claveContragarante = this.getClaveContragarante(claveIF);

            query.setLength(0);
            query.append("INSERT INTO                               " + "   GTI_ESTATUS_SOLIC                      " +
                         "      (                                   " + "         IC_FOLIO,                        " +
                         "         IC_IF_SIAG,                      " + "         IC_NAFIN_ELECTRONICO,            " +
                         "         DF_FECHA_HORA,                   " + "         IN_REGISTROS,                    " +
                         "         FN_IMPTE_TOTAL,                  " + "         IC_TIPO_OPERACION,               " +
                         "         IC_MONEDA,                       " + "         IC_USUARIO_FACULTADO,            " + // Antes: IC_USUARIO_AUTORIZA
                         "         DF_AUTORIZACION,                 " + "         IC_SITUACION,                    " +
                         "         IC_CONTRAGARANTE,                " + "         IG_ANIO_CARGA,                   " +
                         "         IG_MES_CARGA,                    " + "         DF_FECHA_CORTE,                  " +
                         "         CG_RECIBO_FIRMA,                 " + "         IC_SIT_TRANSFER                  " +
                         "      )                                   " + "  VALUES                                  " +
                         "      (                                   " + "         ?,                               " +
                         "         ?,                               " + "         ?,                               " +
                         "         TO_DATE(?,'DD/MM/YYYY HH24:MI'), " + "         ?,                               " +
                         "         ?,                               " + "         ?,                               " +
                         "         ?,                               " + "         ?,                               " +
                         "         TO_DATE(?,'DD/MM/YYYY HH24:MI'), " + "         ?,                               " +
                         "         ?,                               " + "         ?,                               " +
                         "         ?,                               " + "         TO_DATE(?,'DD/MM/YYYY'),         " +
                         "         ?,                               " + "         ?                                " +
                         "      )                                   ");

            ps = con.queryPrecompilado(query.toString());
            ps.setBigDecimal(1, folio);
            ps.setInt(2, claveIFSIAG.intValue());
            ps.setBigDecimal(3, numeroNafinElectronico);
            ps.setString(4, fechaValor);
            ps.setString(5, numeroRegistros);
            ps.setBigDecimal(6, new BigDecimal(montoTotalComisiones));
            ps.setInt(7, COMISIONES);
            ps.setNull(8, Types.INTEGER);
            ps.setString(9, loginUsuario);
            ps.setNull(10, Types.VARCHAR);
            ps.setInt(11, ENVIO_VALIDACION_Y_CARGA);
            ps.setInt(12, claveContragarante);
            ps.setInt(13, Integer.parseInt(anioCarga));
            ps.setInt(14, Integer.parseInt(mesCarga));
            ps.setString(15, fechaCorte);
            ps.setString(16, acuseFirmaDigital);
            ps.setInt(17, 0);
            ps.execute();
            ps.close();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirComisiones(" + _fechaMsg + "_" + claveProceso + ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------

            query.setLength(0);
            query.append("INSERT INTO               " + "   GTI_CONTENIDO_ARCH     " + "      (                   " +
                         "         IC_FOLIO,        " + " 			 IC_IF,           " + "         IC_IF_SIAG,      " +
                         "         IC_LINEA,        " + "         CG_CONTENIDO     " + "      )                   " +
                         "SELECT                    " + "   ?,                     " + "	 ?,                     " +
                         "   ?,                     " + "   IC_LINEA,              " + "   CG_CONTENIDO           " +
                         "FROM                      " + "   GTITMP_CONTENIDO_ARCH  " + "WHERE                     " +
                         "   IC_PROCESO = ?         ");
            ps = con.queryPrecompilado(query.toString());
            ps.setBigDecimal(1, folio);
            ps.setInt(2, Integer.parseInt(claveIF));
            ps.setInt(3, claveIFSIAG.intValue());
            ps.setString(4, claveProceso);
            int numRegistrosInsertadosContArch = ps.executeUpdate();
            ps.close();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.info("transmitirComisiones(" + _fechaMsg + "_" + claveProceso +
                     ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------

            query.setLength(0);
            query.append("delete gtitmp_contenido_arch where ic_proceso = ?");
            ps = con.queryPrecompilado(query.toString());
            ps.setString(1, claveProceso);
            ps.execute();
            ps.close();

            resultados.setFolio(folio.toPlainString());
            resultados.setFecha(fecha);
            resultados.setHora(hora);
            resultados.setFechaValor(fechaValor);

        } catch (Exception e) {

            log.error("transmitirComisiones(Exception)");
            log.error("transmitirComisiones.claveProceso         = <" + claveProceso + ">");
            log.error("transmitirComisiones.claveIF              = <" + claveIF + ">");
            log.error("transmitirComisiones.numeroRegistros      = <" + numeroRegistros + ">");
            log.error("transmitirComisiones.montoTotalComisiones = <" + montoTotalComisiones + ">");
            log.error("transmitirComisiones.claveMes             = <" + claveMes + ">");
            log.error("transmitirComisiones.loginUsuario         = <" + loginUsuario + ">");
            e.printStackTrace();

            exito = false;

            throw new AppException("Ocurri� un error al transmitir las comisiones");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
            log.info("transmitirComisiones(S)");

        }

        return resultados;

    }

    /**
     *
     * Consulta el estatus de la carga de comisiones
     * @throws AppException
     *
     * @param claveIF          <tt>String</tt> con la clave del Intermediario Financiero como aparece en la tabla <tt>COMCAT_IF.IC_IF</tt>.
     * @param fechaOperacionDe <tt>String</tt> con la fecha de operacion inicial (con formato <tt>DD/MM/AAAA</tt> )
     * @param fechaOperacionA  <tt>String</tt> con la fecha de operacion final   (con formato <tt>DD/MM/AAAA</tt> )
     *
     * @return <tt>List</tt> de <tt>HashMap</tt> con el resultado de la consulta.
     *
     * @since   Fodea 006 - 2012, 27/06/2012 06:27:59 p.m.
     * @author  jshernandez
     *
     */
    public List getResultadosCargaComisiones(String claveIF, String fechaOperacionDe,
                                             String fechaOperacionA) throws AppException {

        log.info("getResultadosCargaComisiones(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer(128);

        int claveIFSIAG = 0;

        List resultados = new ArrayList();
        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del Intermediario Financiero
            query.setLength(0);
            query.append("SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ?");
            ps = con.queryPrecompilado(query.toString());
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveIFSIAG = rs.getInt("IC_IF_SIAG");
            }
            rs.close();
            ps.close();

            // Determinar que fechas han sido enviadas
            boolean hayFechaInicial = fechaOperacionDe == null || "".equals(fechaOperacionDe) ? false : true;
            boolean hayFechaFinal = fechaOperacionA == null || "".equals(fechaOperacionA) ? false : true;

            // Construir query
            query.setLength(0);
            query.append("SELECT                                                                                    " +
                         "  TO_CHAR(ESTATUS.DF_FECHA_HORA,'DD/MM/YYYY HH24:MI:SS' )       AS FECHA_HORA_OPERACION,  " +
                         "  ESTATUS.IC_FOLIO                                              AS FOLIO,       			 " +
                         "  SITUACION.CG_DESCRIPCION                                      AS DESCRIPCION_SITUACION, " +
                         "  NVL(ESTATUS.IN_REGISTROS_ACEP,0)                              AS REGISTROS_ACEPTADOS,   " +
                         "  NVL(ESTATUS.IN_REGISTROS_RECH,0)                              AS REGISTROS_RECHAZADOS,  " +
                         "  NVL(ESTATUS.IN_REGISTROS,0)                                   AS REGISTROS_TOTAL,       " +
                         "  ESTATUS.IC_SITUACION                                          AS CLAVE_SITUACION,       " +
                         "  DECODE( ESTATUS.IC_SITUACION,  1,  'false',    'true' )       AS ARCHIVO_RESULTADOS,    " +
                         "  CASE                                                                                    " +
                         "    WHEN ESTATUS.IC_SITUACION              = 1 THEN 'false'                               " +
                         "    WHEN NVL(ESTATUS.IN_REGISTROS_RECH ,0) = 0 THEN 'false'                               " +
                         "    ELSE                                             'true'                               " +
                         "  END                                                           AS ARCHIVO_ERRORES,       " +
                         "  'true'                                                        AS ARCHIVO_ORIGEN         " +
                         "FROM                                                                                      " +
                         "  GTI_ESTATUS_SOLIC ESTATUS,                                                              " +
                         "  GTICAT_SITUACION  SITUACION                                                             " +
                         "WHERE                                                                                     " +
                         "  ESTATUS.IC_IF_SIAG         = ?                               AND                        " +
                         "  ESTATUS.IC_TIPO_OPERACION  = 9                               AND                        ");
            if (hayFechaInicial) {
                query.append("	ESTATUS.DF_FECHA_HORA      >= TO_DATE(?,'DD/MM/YYYY')        AND                        ");
            }
            if (hayFechaFinal) {
                query.append("	ESTATUS.DF_FECHA_HORA      <  TO_DATE(?,'DD/MM/YYYY')+1      AND                        ");
            }
            query.append("  ESTATUS.IC_TIPO_OPERACION  = SITUACION.IC_TIPO_OPERACION  AND                           " +
                         "  ESTATUS.IC_SITUACION       = SITUACION.IC_SITUACION                                     " +
                         "ORDER BY                                                                                  " +
                         "  ESTATUS.DF_FECHA_HORA DESC                                                              ");

            ps = con.queryPrecompilado(query.toString());
            int idx = 1;
            ps.setInt(idx++, claveIFSIAG);
            if (hayFechaInicial) {
                ps.setString(idx++, fechaOperacionDe);
            }
            if (hayFechaFinal) {
                ps.setString(idx++, fechaOperacionA);
            }
            rs = ps.executeQuery();

            while (rs.next()) {

                String fechaHoraOperacion =
                    (rs.getString("FECHA_HORA_OPERACION") == null) ? "" : rs.getString("FECHA_HORA_OPERACION");
                String folioOperacion = (rs.getString("FOLIO") == null) ? "" : rs.getString("FOLIO");
                String situacion =
                    (rs.getString("DESCRIPCION_SITUACION") == null) ? "" : rs.getString("DESCRIPCION_SITUACION");
                String numeroOperacionesAceptadas =
                    (rs.getString("REGISTROS_ACEPTADOS") == null) ? "" : rs.getString("REGISTROS_ACEPTADOS");
                String numeroOperacionesConErrores =
                    (rs.getString("REGISTROS_RECHAZADOS") == null) ? "" : rs.getString("REGISTROS_RECHAZADOS");
                String totalOperaciones =
                    (rs.getString("REGISTROS_TOTAL") == null) ? "" : rs.getString("REGISTROS_TOTAL");

                String extraccionArchivoResultados =
                    (rs.getString("ARCHIVO_RESULTADOS") == null) ? "" : rs.getString("ARCHIVO_RESULTADOS");
                String extraccionArchivoErrores =
                    (rs.getString("ARCHIVO_ERRORES") == null) ? "" : rs.getString("ARCHIVO_ERRORES");
                String extraccionArchivoOrigen =
                    (rs.getString("ARCHIVO_ORIGEN") == null) ? "" : rs.getString("ARCHIVO_ORIGEN");

                HashMap resultado = new HashMap();
                resultado.put("fechaHoraOperacion", fechaHoraOperacion);
                resultado.put("folioOperacion", folioOperacion);
                resultado.put("situacion", situacion);
                resultado.put("numeroOperacionesAceptadas", numeroOperacionesAceptadas);
                resultado.put("numeroOperacionesConErrores", numeroOperacionesConErrores);
                resultado.put("totalOperaciones", totalOperaciones);
                resultado.put("extraccionArchivoResultados", extraccionArchivoResultados);
                resultado.put("extraccionArchivoErrores", extraccionArchivoErrores);
                resultado.put("extraccionArchivoOrigen", extraccionArchivoOrigen);

                resultados.add(resultado);

            }

        } catch (Exception e) {

            log.error("getResultadosCargaComisiones(Exception)");
            log.error("getResultadosCargaComisiones.claveIF          = <" + claveIF + ">");
            log.error("getResultadosCargaComisiones.fechaOperacionDe = <" + fechaOperacionDe + ">");
            log.error("getResultadosCargaComisiones.fechaOperacionA  = <" + fechaOperacionA + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al realizar la b�squeda.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getResultadosCargaComisiones(S)");

        }

        return resultados;

    }

    /**
     * Devuelve el Query que se utiliza para consultar el contenido original del archivo de comisiones.
     * @throws AppException
     *
     * @param claveIF     		<tt>String</tt> con la clave del intermediario financiero, como
     *						    		aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param folioOperacion   <tt>String</tt> con el numero del folio.
     *
     * @return            <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
     *                    los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     *
     * @since             Fodea 006 - 2012, 28/06/2012 02:09:34 p.m.
     * @author            jshernandez
     */
    public HashMap getQueryComisionesArchivoOriginal(String claveIF, String folioOperacion) throws AppException {

        log.info("getQueryComisionesArchivoOriginal(E)");

        HashMap query = new HashMap();
        StringBuffer text = new StringBuffer();
        List parameters = new ArrayList();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer buffer = new StringBuffer(64);

        int claveIFSIAG = 0;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del Intermediario Financiero
            buffer.setLength(0);
            buffer.append("SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ?");
            ps = con.queryPrecompilado(buffer.toString());
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveIFSIAG = rs.getInt("IC_IF_SIAG");
            }

            text.append("SELECT                    " + "   CG_CONTENIDO           " + "FROM                      " +
                        "   GTI_CONTENIDO_ARCH     " + "WHERE                     " + "   IC_FOLIO    = ?  AND   " +
                        "   IC_IF_SIAG  = ?        " + "ORDER BY IC_LINEA ASC     ");

            parameters.add(new BigDecimal(folioOperacion));
            parameters.add(new Integer(claveIFSIAG));

            query.put("text", text.toString());
            query.put("parameters", parameters);

        } catch (Exception e) {

            log.error("getQueryComisionesArchivoOriginal(Exception)");
            log.error("getQueryComisionesArchivoOriginal.claveIF         = <" + claveIF + ">");
            log.error("getQueryComisionesArchivoOriginal.folioOperacion  = <" + folioOperacion + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar la consulta");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getQueryComisionesArchivoOriginal(S)");

        }

        return query;

    }

    /**
     * Devuelve el Query que se utiliza para consultar el detalle de la carga del archivo de comisiones.
     * @throws AppException
     *
     * @param claveIF     		<tt>String</tt> con la clave del intermediario financiero, como
     *						    		aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param folioOperacion   <tt>String</tt> con el numero del folio.
     *
     * @return            		<tt>HashMap</tt> con de queries. Donde cada query es un <tt>HashMap</tt> con el atributo:
     *                         <tt>text</tt> y con los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     *
     * @since             Fodea 006 - 2012, 28/06/2012 02:13:17 p.m.
     * @author            jshernandez
     */
    public HashMap getQueriesComisionesArchivoResultados(String claveIF, String folioOperacion) throws AppException {

        log.info("getQueriesComisionesArchivoResultados(E)");

        HashMap queries = new HashMap();
        StringBuffer text = null;
        List parameters = null;

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer buffer = new StringBuffer(64);

        int claveIFSIAG = 0;
        String nombreIF = "";
        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del Intermediario Financiero
            buffer.setLength(0);
            buffer.append("SELECT IC_IF_SIAG,CG_RAZON_SOCIAL FROM COMCAT_IF WHERE IC_IF = ?");
            ps = con.queryPrecompilado(buffer.toString());
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveIFSIAG = rs.getInt("IC_IF_SIAG");
                nombreIF = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
            }

            // DEFINIR QUERY RESULTADOS SOLICITUD
            HashMap queryResultados = new HashMap();
            text = new StringBuffer(1408);
            text.append("SELECT                                                                           " +
                        "  ESTATUS.IC_FOLIO                                   AS FOLIO_OPERACION,         " +
                        "  ?                                                  AS INTERMEDIARIO_FINANCIERO," +
                        "  NVL(ESTATUS.IN_REGISTROS_ACEP,0)                   AS REGISTROS_SIN_ERRORES,   " +
                        "  NVL(ESTATUS.IN_REGISTROS_RECH,0)                   AS REGISTROS_CON_ERRORES,   " +
                        "  NVL(ESTATUS.IN_REGISTROS,0)                        AS TOTAL_REGISTROS,         " +
                        "  TO_CHAR(ESTATUS.DF_VALIDACION_SIAG, 'DD/MM/YYYY' ) AS FECHA_OPERACION_FIRME    " +
                        "FROM                                                                             " +
                        "  GTI_ESTATUS_SOLIC ESTATUS                                                      " +
                        "WHERE                                                                            " +
                        "  ESTATUS.IC_IF_SIAG         =  ? AND                                            " +
                        "  ESTATUS.IC_FOLIO           =  ?                                                ");

            parameters = new ArrayList();
            parameters.add(nombreIF);
            parameters.add(new Integer(claveIFSIAG));
            parameters.add(new BigDecimal(folioOperacion));

            queryResultados.put("text", text.toString());
            queryResultados.put("parameters", parameters);

            queries.put("QUERY_RESULTADOS", queryResultados);

            // DEFINIR QUERY DETALLE COMISIONES
            HashMap queryDetalle = new HashMap();
            text = new StringBuffer(1408);
            text.append("SELECT                                                                                     " +
                        "  NVL(MONEDA.CD_NOMBRE,DETALLE.IC_MONEDA)                        AS NOMBRE_MONEDA,         " +
                        "  NVL(DETALLE.IN_REG_ACEPTADOS,  0)                              AS REGISTROS_SIN_ERRORES, " +
                        "  NVL(DETALLE.IN_REG_RECHAZADOS, 0)                              AS REGISTROS_CON_ERRORES, " +
                        "  NVL(DETALLE.IN_REGISTROS,      0)                              AS TOTAL_REGISTROS,       " +
                        "  NVL(DETALLE.FN_MONTO_COMISION,0) + NVL(DETALLE.FN_MONTO_IVA,0) AS IMPORTE_COMISION_E_IVA " +
                        "FROM                                                                                       " +
                        "  GTI_DETCOMISIONES_SHF DETALLE,                                                           " +
                        "  COMCAT_MONEDA         MONEDA                                                             " +
                        "WHERE                                                                                      " +
                        "  DETALLE.IC_IF_SIAG =  ? AND                                                              " +
                        "  DETALLE.IC_FOLIO   =  ? AND                                                              " +
                        "  DETALLE.IC_MONEDA  = MONEDA.IC_MONEDA(+)                                                 " +
                        "ORDER BY DETALLE.IC_MONEDA                                                                 ");

            parameters = new ArrayList();
            parameters.add(new Integer(claveIFSIAG));
            parameters.add(new BigDecimal(folioOperacion));

            queryDetalle.put("text", text.toString());
            queryDetalle.put("parameters", parameters);

            queries.put("QUERY_DETALLE", queryDetalle);

            // DEFINIR QUERY DETALLE ERRORES GENERADOS
            HashMap queryErrores = new HashMap();
            text = new StringBuffer(640);
            text.append("SELECT                                                " +
                        "   ERRORES.CG_CONTENIDO  AS DETALLE_ERROR         	 " +
                        "FROM                                                  " +
                        "   GTI_ARCH_RESULTADO ERRORES                         " +
                        "WHERE                                                 " +
                        "   ERRORES.IC_IF_SIAG = ? AND                         " +
                        "   ERRORES.IC_FOLIO   = ?                             " +
                        "ORDER BY                                              " +
                        "  ERRORES.IC_LINEA,                                   " +
                        "  ERRORES.IC_NUM_ERROR                                ");

            parameters = new ArrayList();
            parameters.add(new Integer(claveIFSIAG));
            parameters.add(new BigDecimal(folioOperacion));

            queryErrores.put("text", text.toString());
            queryErrores.put("parameters", parameters);

            queries.put("QUERY_ERRORES", queryErrores);

        } catch (Exception e) {

            log.error("getQueriesComisionesArchivoResultados(Exception)");
            log.error("getQueriesComisionesArchivoResultados.claveIF         = <" + claveIF + ">");
            log.error("getQueriesComisionesArchivoResultados.folioOperacion  = <" + folioOperacion + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar la consulta");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getQueriesComisionesArchivoResultados(S)");

        }

        return queries;

    }

    /**
     * Devuelve el Query que se utiliza para consultar los registros originales del archivo de comisiones que
     * presentaron errores.
     * @throws AppException
     *
     * @param claveIF     		<tt>String</tt> con la clave del intermediario financiero, como
     *						    		aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param folioOperacion   <tt>String</tt> con el numero del folio.
     *
     * @return            <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
     *                    los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     * @since             Fodea 006 - 2012, 28/06/2012 02:14:34 p.m.
     * @author            jshernandez
     */
    public HashMap getQueryComisionesArchivoErrores(String claveIF, String folioOperacion) throws AppException {

        log.info("getQueryComisionesArchivoErrores(E)");

        HashMap query = new HashMap();
        StringBuffer text = new StringBuffer();
        List parameters = new ArrayList();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer buffer = new StringBuffer(64);

        int claveIFSIAG = 0;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del Intermediario Financiero
            buffer.setLength(0);
            buffer.append("SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ?");
            ps = con.queryPrecompilado(buffer.toString());
            ps.setString(1, claveIF);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveIFSIAG = rs.getInt("IC_IF_SIAG");
            }

            text.append("SELECT                    " + "   CG_CONTENIDO           " + "FROM                      " +
                        "   GTI_ARCH_RECHAZO       " + "WHERE                     " + "   IC_FOLIO    = ?  AND   " +
                        "   IC_IF_SIAG  = ?        " + "ORDER BY IC_LINEA ASC     ");

            parameters.add(new BigDecimal(folioOperacion));
            parameters.add(new Integer(claveIFSIAG));

            query.put("text", text.toString());
            query.put("parameters", parameters);

        } catch (Exception e) {

            log.error("getQueryComisionesArchivoErrores(Exception)");
            log.error("getQueryComisionesArchivoErrores.claveIF         = <" + claveIF + ">");
            log.error("getQueryComisionesArchivoErrores.folioOperacion  = <" + folioOperacion + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar la consulta");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getQueryComisionesArchivoErrores(S)");

        }

        return query;

    }

    /**
     *
     * Valida la longitud de un campo entero o decimal proveniente de una linea de archivo
     *
     * Nota: Una precision de 5 y una escala de 2, permiten tener 3 enteros y 2 decimales
     *
     * @debugInfo Se consideran dentro de la parte entera los signos: '-' '+', lo podr�a
     * ocasionar una validacion erronea de la precion
     *
     * @param numLinea    Numero de linea en el archivo
     * @param campo       Valor del campo a Validar
     * @param nombreCampo Descripcion del campo que se valida
     * @param rg          objeto que almacena los resultados del procesamiento
     * @param precision   Numero de digitos que conforman el valor
     * @param decimales   Numero de digitos permitidos como decimales
     *
     * @return            <tt>false</tt> si cualquiera de las partes del n�mero excede la precisi�n
     *                    especificada. <tt>true</tt> en caso contrario.
     *
     */
    private boolean validaPrecisionDecimal(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                                           int precision, int decimales) {

        boolean validacion = true;
        campo = campo == null ? "" : campo;

        String[] componentes = campo.split("\\.", -1);

        String parteEntera = componentes.length > 0 ? componentes[0] : "";
        String parteDecimal = componentes.length > 1 ? componentes[1] : "";

        if (parteDecimal.length() > decimales) {
            //Error. El numero de decimales es mayor al especificado
            validacion = false;
        } else if (parteEntera.length() > precision - decimales) {
            //Error. El numero de enteros es mayor al permitido
            validacion = false;
        }

        if (!validacion) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            rg.appendErrores(" no cumple con la longitud requerida. \n");
            rg.setErrorLinea(true);
            validacion = false;

        }
        return validacion;

    }

    private boolean validaSerPositivo(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean validacion = true;
        campo = campo == null ? "" : campo;

        String[] componentes = campo.split("\\.", -1);

        String parteEntera = componentes.length > 0 ? componentes[0] : "";
        //String   parteDecimal = componentes.length > 1?componentes[1]:"";
        if (parteEntera.substring(0, 1).equals("-")) {
            validacion = false;
        }

        if (!validacion) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores("'" + nombreCampo + "'");
            rg.appendErrores(" no puede ser negativo. \n");
            rg.setErrorLinea(true);
            validacion = false;

        }
        return validacion;

    }

    private boolean validaPrecisionDecimalDifMe(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                                                int precision, int decimales) {

        boolean validacion = true;
        campo = campo == null ? "" : campo;

        String[] componentes = campo.split("\\.", -1);

        String parteEntera = componentes.length > 0 ? componentes[0] : "";
        String parteDecimal = componentes.length > 1 ? componentes[1] : "";
        String auxNegativo = parteEntera;
        if (parteEntera.substring(0, 1).equals("-")) {
            String enteraNumNeg = auxNegativo.substring(1, auxNegativo.length());
            parteEntera = enteraNumNeg;
        }
        if (parteDecimal.length() > decimales) {
            //Error. El numero de decimales es mayor al especificado
            validacion = false;
        } else if (parteEntera.length() > precision) {
            //Error. El numero de enteros es mayor al permitido
            validacion = false;
        }

        if (!validacion) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores("'" + nombreCampo + "'");
            rg.appendErrores(" no cumple con la longitud m�xima de " + precision + "," + decimales + ". \n");
            rg.setErrorLinea(true);
            validacion = false;

        }
        return validacion;

    }

    /**
     *
     * Valida la longitud de un campo de caracteres proveniente de una linea de archivo
     *
     * @param numLinea     Numero de linea en el archivo
     * @param campo        Valor del campo a Validar
     * @param nombreCampo  Descripcion del campo que se valida
     * @param rg           objeto que almacena los resultados del procesamiento
     * @param longitudMax  Longitud M�xima de la cadena
     *
     * @return            <tt>false</tt> si la cadena proporcionada no cumple con la
     *                    longitud maxima permitida. <tt>true</tt> en caso contrario.
     *
     */
    private boolean validarLongitud(int numLinea, String campo, String nombreCampo, ResultadosGar rg, int longitudMax) {

        boolean validacion = true;
        if (campo != null && campo.length() > longitudMax) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            rg.appendErrores(" no cumple con la longitud m�xima permitida: " + longitudMax + ". \n");
            rg.setErrorLinea(true);
            validacion = false;

        }
        return validacion;

    }


    /**
     *
     * Valida que la longitud de un campo de caracteres proveniente de una linea de archivo
     * sea multiplo del numero indicado en el campo: <tt>longitud</tt>.
     *
     * @param numLinea     		Numero de linea en el archivo.
     * @param campo        		Valor del campo a Validar.
     * @param nombreCampo  		Descripcion del campo que se valida.
     * @param rg           		Objeto que almacena los resultados del procesamiento.
     * @param longitud 			La longitud del campo debe ser multiplo de este valor.
     *
     * @return            <tt>false</tt> si la cadena proporcionada no cumple con la
     *                    longitud o un multiplo de esta. <tt>true</tt> en caso contrario.
     *
     */
    private boolean validaLongitudMultiplo(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                                           int longitudMultiplo) {

        boolean validacion = true;
        if (campo == null || campo.length() == 0 || (campo.length() % longitudMultiplo) != 0) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            rg.appendErrores(" no cumple con la longitud requerida");
            //rg.appendErrores(" no cumple con la longitud requerida o no es un multiplo de longitud ");
            //rg.appendErrores(String.valueOf(longitudMultiplo));
            rg.appendErrores(". \n");
            rg.setErrorLinea(true);
            validacion = false;

        }
        return validacion;

    }

    private boolean validaCamposExcluyentes(int numLinea, String campo1, String campo2, String mensajeError,
                                            ResultadosGar rg) {

        log.info("validaCamposExcluyentes(E)");
        boolean sonExcluyentes = true;

        boolean campo1EsCero = false;
        boolean campo2EsCero = false;

        try {

            campo1 = campo1.trim();
            campo2 = campo2.trim();

            // Validar si el campo 1 es cero
            if (campo1.matches("[\\-]?[0]+[\\.]?") || campo1.matches("[\\-]?[0]*[\\.][0]+")) {
                campo1EsCero = true;
            }
            // Validar si el campo 2 es cero
            if (campo2.matches("[\\-]?[0]+[\\.]?") || campo2.matches("[\\-]?[0]*[\\.][0]+")) {
                campo2EsCero = true;
            }

            // Realizar la validaciones requerida.
            if (!campo1EsCero && !campo2EsCero) {
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(". ");
                rg.appendErrores(mensajeError);
                rg.appendErrores("\n");
                rg.setErrorLinea(true);
                sonExcluyentes = false;
            }

        } catch (Exception e) {

            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:" + numLinea + ". Error al validar campos excluyentes: " +
                             e.getMessage() + ". \n");
            sonExcluyentes = false;

        }

        log.info("validaCamposExcluyentes(S)");
        return sonExcluyentes;

    }

    /**
     *
     *  Este m�todo valida que el Tipo Programa ( Clave del Programa), no opere con Cr�dito Educativo,
     *  en caso contrario, el error que reporta es:
     *
     *  <tt>"El valor del campo Tipo Programa (Base de Operaci�n) opera el programa de Cr�dito Educativo."</tt>
     *
     *  @param numLinea    N�mero de linea en el archivo.
     *  @param campo       Valor del campo.
     *  @param nombreCampo Descripci�n del campo que se valida.
     *  @param reglaProgramaCreditoEducativo  Devuelve <tt>ResultadosGar.OPERA_CREDITO_EDUCATIVO </tt>, si el Programa ( Tipo Programa ) opera con Cr�dito Educativo;
     *                     en caso contrario devuelve <tt>ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO</tt>.
     *  @param rg          Objeto que almacena los resultados del procesamiento.
     *
     *  @since Fodea ### - 2013. Credito Educativo 2da Fase.
     *  @author Salim Hernandez ( 08/02/2013 07:09:58 p.m. )
     *
     */
    private boolean validaTipoPrograma(int numLinea, String campo, String nombreCampo,
                                       int reglaProgramaCreditoEducativo, ResultadosGar rg) {

        log.info("validaTipoPrograma(E)");

        String msg_error = "";
        boolean hayError = false;

        if (reglaProgramaCreditoEducativo != ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO) {
            msg_error =
                "El valor del campo " + nombreCampo + " (Base de Operaci�n) opera el programa de Cr�dito Educativo.";
            hayError = true;
        }

        if (hayError) {
            rg.appendErrores("Error en la linea:" + numLinea + " " + msg_error + "\n");
            rg.setErrorLinea(true);
        }

        log.info("validaTipoPrograma(S)");
        return !hayError;

    }

    /**
     *
     * Valida que el Tipo de Recuperaci&oacute;n coincida con la clave 3 ( Recuperaci&oacute;n
     * por reinstalaci&oacute;n )
     *
     * @param numLinea     		Numero de linea en el archivo.
     * @param campo        		Valor del campo a Validar.
     * @param nombreCampo  		Descripcion del campo que se valida.
     * @param rg           		Objeto que almacena los resultados del procesamiento.
     *
     * @return            		<tt>true</tt> si la clave proporcionada coincide con la
     *                         clave de Recuperaci�n por Reinstalaci�n. <tt>false</tt>
     *                         en caso contrario.
     *
     * @author Salim Hernandez ( 21/03/2013 01:43:33 p.m. )
     * @since  F007 - 2013
     *
     */
    private boolean validaRecuperacionPorReinstalacion(int numLinea, String campo, String nombreCampo,
                                                       ResultadosGar rg) {

        boolean valido = true;
        if (!"3".equals(campo)) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo " + nombreCampo +
                             " debe ser 3. \n");
            rg.setErrorLinea(true);
            valido = false;
        }
        return valido;

    }

    /**
     *
     * Validaciones de alta de Reinstalaciones de Garant�as.
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * ResultadosGar, el cual llevara el registro de los errores de validaci�n.
     *
     * @param ifSiag    Clave SIAG del Intermediario Financiero
     * @param numLinea  Numero de linea que se est� validando
     * @param strLinea  L�nea de valores del layout de Alta de Reinstalaciones.
     * @param retorno   Objeto de ResultadosGar, que contiene los detalles de la validaci�n.
     *
     * @author Salim Hernandez ( 19/03/2013 06:12:52 p.m. )
     * @since  F007 - 2013, 19/03/2013 06:12:52 p.m.
     *
     */
    private void validarAltaReinstalaciones(int ifSiag, int numLinea, String strLinea, ResultadosGar retorno) {

        // Validar que el registro no exceda el tama�o m�ximo permitido
        if (strLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " El tama�o del registro es demasiado grande para ser procesado. \n");
            retorno.setErrorLinea(true);
            return;
        }

        // I. CADA CAMPO DENTRO DE UN REGISTRO DEBER� TERMINAR, INVARIABLEMENTE, CON EL S�MBOLO @ (ARROBA).
        if (strLinea.charAt(strLinea.length() - 1) != '@') {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " La linea debe finalizar con un caracter de arroba\n");
            retorno.setErrorLinea(true);
            return;
        }

        VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
        List layout = vt.getValuesVector();

        // II. VALIDAR QUE SE HAYA ENVIADO EL NUMERO DE CAMPOS REQUERIDO
        int numCamposRequeridos = 20;
        //Dado que en el layout SIEMPRE ponen una arroba al final de la linea,
        //el cual es detectado como un campo mas por la clase VectorTokenizer,
        //se le suma 1
        if (layout.size() != numCamposRequeridos + 1) {
            retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto. Esperados(" +
                                  numCamposRequeridos + ") Recibidos(" + (layout.size() - 1) + ") \n");
            //layout.size()-1 se les resta uno, debido a que como en el layout agregan una @ al final, VectorTokenizer cuenta un campo de m�s.
            retorno.setErrorLinea(true);
            return;
        }

        // III. REALIZAR VALIDACIONES POR CAMPO.
        boolean campoValido = false;
        boolean campo17Valido = false;
        boolean campo18Valido = false;
        // No. 	CAMPO                                       	      	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................
        // 1. 	Clave del intermediario											NUMBER		5,0		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de identificaci�n del intermediario
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(0), "Clave del intermediario", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(0), "Clave del intermediario", retorno, 5);
        if (campoValido)
            validaIntermediario(numLinea, (String) layout.get(0), "Clave del intermediario", ifSiag, retorno);

        // 2. 	Clave de la garant�a												CHAR			20			OBLIGATORIO
        //    	OBSERVACIONES: No. de pr�stamo o garant�a
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(1), "Clave de la garant�a", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(1), "Clave de la garant�a", retorno, 20);

        // 3. 	Nombre del acreditado											CHAR			80			OBLIGATORIO
        //    	OBSERVACIONES: Nombre completo del cliente
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(2), "Nombre del acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(2), "Nombre del acreditado", retorno, 80);

        // 4. 	Fecha honrada														DATE			8			OBLIGATORIO
        //    	OBSERVACIONES: Formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(3), "Fecha honrada", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(3), "Fecha honrada", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(3), "Fecha honrada", retorno);

        // 5. 	Monto honrado														NUMBER		20,2		OBLIGATORIO
        //   		OBSERVACIONES: Total Pagado
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(4), "Monto honrado", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, (String) layout.get(4), "Monto honrado", retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(4), "Monto honrado", retorno);

        // 6. 	Monto total de la reinstalaci�n del acreditado 			NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Monto pagado por el cliente
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(5),
                                       "Monto total de la reinstalaci�n del acreditado", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(5),
                                       "Monto total de la reinstalaci�n del acreditado", retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(5),
                              "Monto total de la reinstalaci�n del acreditado", retorno);

        // 7. 	Moneda																CHAR			7			OBLIGATORIO
        //    	OBSERVACIONES: Tipo de Moneda: PESOS, DOLARES.
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(6), "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(6), "Moneda", retorno, 7);
        if (campoValido)
            validaMoneda(numLinea, (String) layout.get(6), retorno);

        // 8. 	Tipo de recuperaci�n												NUMBER		1			OBLIGATORIO
        //    	OBSERVACIONES: 3) Recuperaci�n por reinstalaci�n
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno, 1);
        if (campoValido)
            campoValido = validaEntero(numLinea, (String) layout.get(7), "Tipo de recuperaci�n", retorno);
        if (campoValido)
            campoValido =
                validaRecuperacionPorReinstalacion(numLinea, ((String) layout.get(7)).trim(), "Tipo de recuperaci�n",
                                                   retorno);

        // 9. 	Devoluci�n de Comisi�n de Aniversario						NUMBER		20,2		OBLIGATORIO
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(8), "Devoluci�n de Comisi�n de Aniversario",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(8), "Devoluci�n de Comisi�n de Aniversario",
                                       retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(8),
                              "Devoluci�n de Comisi�n de Aniversario", retorno);

        //10. 	Devoluci�n de IVA de Comisi�n de Aniversario				NUMBER		20,2		OBLIGATORIO
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(9), "Devoluci�n de IVA de Comisi�n de Aniversario",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(9), "Devoluci�n de IVA de Comisi�n de Aniversario",
                                       retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(9),
                              "Devoluci�n de IVA de Comisi�n de Aniversario", retorno);

        //11. 	Total de Devoluci�n de Comisi�n e IVA de Aniversario	NUMBER		20,2		OBLIGATORIO
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(10),
                                       "Total de Devoluci�n de Comisi�n e IVA de Aniversario", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(10),
                                       "Total de Devoluci�n de Comisi�n e IVA de Aniversario", retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(10),
                              "Total de Devoluci�n de Comisi�n e IVA de Aniversario", retorno);

        //12. 	Capital reembolsado por el acreditado						NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Recuperaci�n que corresponde a capital
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(11), "Capital reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(11), "Capital reembolsado por el acreditado",
                                       retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(11),
                              "Capital reembolsado por el acreditado", retorno);

        //13. 	Inter�s reembolsado por el acreditado						NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Recuperaci�n que corresponde a inter�s ordinario
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(12), "Inter�s reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(12), "Inter�s reembolsado por el acreditado",
                                       retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(12),
                              "Inter�s reembolsado por el acreditado", retorno);

        //14. 	Moratorios reembolsado por el acreditado					NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Recuperaci�n que corresponde a moratorios
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(13), "Moratorios reembolsado por el acreditado",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(13), "Moratorios reembolsado por el acreditado",
                                       retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(13),
                              "Moratorios reembolsado por el acreditado", retorno);

        //15. 	Fecha del Dep�sito												DATE			8			OBLIGATORIO
        //    	OBSERVACIONES: Formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(14), "Fecha del Dep�sito", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(14), "Fecha del Dep�sito", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(14), "Fecha del Dep�sito", retorno);

        //16. 	Importe del Reembolso de Reinstalaci�n de NAFIN			NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Importe del reembolso a NAFIN
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(15),
                                       "Importe del Reembolso de Reinstalaci�n de NAFIN", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(15),
                                       "Importe del Reembolso de Reinstalaci�n de NAFIN", retorno, 20, 2);
        if (campoValido)
            validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(15),
                              "Importe del Reembolso de Reinstalaci�n de NAFIN", retorno);

        //17. 	Importe de Costo Financiero									NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Importe de Costo Financiero para pago a NAFIN; si el valor de este
        //    	campo es mayor a cero, se requiere que el valor del campo 18 sea cero.
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(16), "Importe de Costo Financiero", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, (String) layout.get(16), "Importe de Costo Financiero", retorno, 20,
                                       2);
        if (campoValido)
            campoValido =
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(16),
                                  "Importe de Costo Financiero", retorno);
        campo17Valido = campoValido;

        //18. 	Penalizaci�n														NUMBER		20,2		OBLIGATORIO
        //    	OBSERVACIONES: Importe de la penalizaci�n para pago a NAFIN; si el valor de este
        //    	campo es mayor a cero, se requiere que el valor del campo 17 sea cero.
        campoValido = true;
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, (String) layout.get(17), "Penalizaci�n", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, (String) layout.get(17), "Penalizaci�n", retorno, 20, 2);
        if (campoValido)
            campoValido =
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(17), "Penalizaci�n", retorno);
        campo18Valido = campoValido;

        //19. 	IVA generado por Costos Financieros / Penalizaci�n		NUMBER		20,2		NO OBLIGATORIO
        campoValido = true;
        if (!esVacio((String) layout.get(18))) {
            if (campoValido)
                campoValido =
                    validaPrecisionDecimal(numLinea, (String) layout.get(18),
                                           "IVA generado por Costos Financieros / Penalizaci�n", retorno, 20, 2);
            if (campoValido)
                validaFlotantePV2(numLinea, (String) layout.get(1), (String) layout.get(18),
                                  "IVA generado por Costos Financieros / Penalizaci�n", retorno);
        }

        //20. 	Fecha de reembolso a NAFIN										DATE			8			OBLIGATORIO
        //    	OBSERVACIONES: Formato: AAAAMMDD
        campoValido = true;
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, (String) layout.get(19), "Fecha de reembolso a NAFIN", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, (String) layout.get(19), "Fecha de reembolso a NAFIN", retorno, 8);
        if (campoValido)
            validaFecha(numLinea, (String) layout.get(19), "Fecha de reembolso a NAFIN", retorno);

        // VALIDACIONES ESPECIALES
        //.......................................................................................................
        if (campo17Valido && campo18Valido) {
            validaCamposExcluyentes(numLinea, (String) layout.get(16), (String) layout.get(17),
                                    "Los campos 17 y 18 son excluyentes, por lo que se requiere que el valor de uno de ellos sea cero.",
                                    retorno);
        }

    }

    /**
     *
     * Lee el archivo especificado, el cual debe cumplir con el layout de
     * carga de Alta de Reinstalaciones de Garant�as.
     * @throws AppException
     *
     * @param strDirectorio 	Directorio donde se encuentra el archivo a procesar
     * @param esNombreArchivo 	Nombre del archivo a procesar, puede ser
     * 								un archivo de texto(.txt) o un archivo comprimido(.zip)
     * @param num_registros 	Numero de registros contenidos en el archivo a procesar
     * @param sum_montos 		Sumatoria del monto de los registros del archivo a procesar
     * @param ic_if 				Clave del intermediario financiero
     *
     * @return Objeto ResultadosGar con el resultado del procesamiento.
     *
     * @author Salim Hernandez ( 19/03/2013 06:16:33 p.m. )
     * @since  F007 - 2013, 19/03/2013 06:16:33 p.m.
     *
     */
    public ResultadosGar procesarAltaReinstalaciones(String strDirectorio, String esNombreArchivo, String num_registros,
                                                     String sum_montos, String ic_if) throws AppException {

        log.info("procesarAltaReinstalaciones(E)");

        ResultadosGar retorno = new ResultadosGar();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";

        boolean hayErrorInesperado = false;

        java.io.File lofArchivo = null;
        BufferedReader br = null;

        try {

            int ctrl_num_registros = Integer.parseInt(num_registros);
            BigDecimal ctrl_sum_montos = new BigDecimal(sum_montos);
            String extension = esNombreArchivo.substring(esNombreArchivo.length() - 4, esNombreArchivo.length());

            // En caso de enviar un archivo zip, validar que el archivo empaquetado tenga extension txt.
            if (".zip".equalsIgnoreCase(extension)) {
                try {
                    esNombreArchivo = descomprimirArchivo(strDirectorio, esNombreArchivo);
                    extension = esNombreArchivo.substring(esNombreArchivo.length() - 4, esNombreArchivo.length());
                    if (!".txt".equalsIgnoreCase(extension)) {
                        retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                    }
                } catch (Exception e) {
                    retorno.appendErrores("Error: El archivo comprimido no es txt\n");
                }
            }

            // Abrir archivo para su lectura
            lofArchivo = new java.io.File(esNombreArchivo);
            br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));

            // Inicializar variables
            VectorTokenizer vt = null;
            Vector lovDatos = null;
            String lsLinea = "";
            int ifSiag = 0;
            int lineas = 0;
            int numLinea = 1;
            int ctrl_linea = 0;
            int iNumRegTrans = 0;
            BigDecimal dbl_sum_montos = new BigDecimal(0.00);

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del IF
            qrySentencia =
                " SELECT        " + "    ic_if_siag " + " FROM          " + "    comcat_if  " + " WHERE         " +
                "    ic_if = ?  ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, ic_if);
            rs = ps.executeQuery();
            if (rs.next()) {
                ifSiag = rs.getInt("ic_if_siag");
            }
            rs.close();
            ps.close();

            // Generar ID del Proceso de Carga
            retorno.setIcProceso(this.calculaIcProceso());

            // Preparar sentencia para insertar los registros en la tabla temporal
            qrySentencia =
                " INSERT INTO              " + "    gtitmp_contenido_arch " + "    (                     " +
                "       IC_PROCESO,        " + "       IC_LINEA,          " + "       CG_CONTENIDO       " +
                "    )                     " + " VALUES                   " + "    (                     " +
                "       ?,                 " + "       ?,                 " + "       ?                  " +
                "    )                     ";
            ps = con.queryPrecompilado(qrySentencia);

            if (retorno.getErrores().length() == 0) {
                for (lineas = 0, numLinea = 1; (lsLinea = br.readLine()) != null; numLinea++) {

                    // Inicializar Variables
                    BigDecimal tmpMonto = new BigDecimal(0);
                    retorno.setErrorLinea(false);

                    // "Formatear" registro
                    lsLinea =
                        lsLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH ?
                        lsLinea.substring(0, GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH) : lsLinea.trim();

                    // No hacer nada si la linea viene vac�a
                    if (lsLinea.matches("\\s*"))
                        continue;
                    else
                        ctrl_linea++;

                    // Validar que no se exceda el n�mero m�ximo de lineas que pueden ser guardadas en la base de datos
                    if (ctrl_linea > GTI_CONTENIDO_ARCH_IC_LINEA_MAX) {
                        break;
                    }

                    // Extraer campos
                    vt = new VectorTokenizer(lsLinea, "@");
                    lovDatos = vt.getValuesVector();

                    // Validar Campos de la linea leida
                    this.validarAltaReinstalaciones(ifSiag, numLinea, lsLinea, retorno);

                    // Obtener valor del monto de reinstalacion
                    try {
                        tmpMonto = new BigDecimal(((String) lovDatos.get(5)).trim());
                    } catch (Throwable e) {
                        tmpMonto = new BigDecimal(0);
                    }

                    // El registro no pesent� errores
                    if (!retorno.getErrorLinea()) {

                        // Agregar cabecera de Claves de las Garant�as
                        if (lineas == 0) {
                            retorno.appendCorrectos("Claves de la garant�a:\n");
                        }

                        // Agregar clave de la garant�a a lista de Registros correctos
                        retorno.appendCorrectos("" + (String) lovDatos.get(1) + "\n");
                        // Incrementar conteo de registros correctos
                        retorno.incNumRegOk();
                        lineas++;
                        // A�adir monto de la reinstalacion a la sumatoria de montos de registros sin error
                        retorno.addSumRegOk(tmpMonto);

                        // Insertar registro sin errores en tabla temporal ( siempre y cuando se haya presentado ningun error )
                        if (retorno.getErrores().length() == 0 && retorno.getCifras().length() == 0) {

                            // Insertar registro reinstalacion
                            ps.setInt(1, retorno.getIcProceso());
                            ps.setInt(2, numLinea);
                            ps.setString(3, lsLinea);
                            ps.execute();

                            // Actualizar contador de registros transmitidos (insertados)
                            iNumRegTrans++;

                            // Dar commit cada 3500 registros
                            if (iNumRegTrans >= 3500) {
                                con.terminaTransaccion(true);
                                iNumRegTrans = 0;
                                System.gc();
                            }

                        }

                        // El registro pesent� errores
                    } else {

                        // Incrementar conteo de registros con error
                        retorno.incNumRegErr();
                        // A�adir monto de la reinstalacion a la sumatoria de montos de registros con error
                        retorno.addSumRegErr(tmpMonto);

                    }

                    // Actualizar sumatoria del total de montos de reinstalacion.
                    dbl_sum_montos = dbl_sum_montos.add(tmpMonto);

                } // for

            }
            ps.close();

            // Revisar que el n�mero de registros validados, coincida con el n�mero de registros
            // capturados por el usuario.
            if (ctrl_linea != ctrl_num_registros) {
                retorno.appendCifras("El n�mero de registros capturado no corresponde al contenido en el archivo.\n");
            }
            // Revisar que el total de suma de montos de reinstalacion coincida con el monto de
            // reinstalacion capturado por el usuario.
            if (dbl_sum_montos.compareTo(ctrl_sum_montos) != 0) {
                log.debug("procesarAltaReinstalaciones()::La sumatoria de los montos    = <" +
                          dbl_sum_montos.toPlainString() + ">");
                log.debug("procesarAltaReinstalaciones()::La cifra de control capturada = <" +
                          ctrl_sum_montos.toPlainString() + ">");
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }
            //log.debug("procesarAltaReinstalaciones()::Numero de lineas correctas = "+lineas);

        } catch (Exception e) {

            log.error("procesarAltaReinstalaciones(Exception)");
            log.error("procesarAltaReinstalaciones.strDirectorio   = <" + strDirectorio + ">");
            log.error("procesarAltaReinstalaciones.esNombreArchivo = <" + esNombreArchivo + ">");
            log.error("procesarAltaReinstalaciones.num_registros   = <" + num_registros + ">");
            log.error("procesarAltaReinstalaciones.sum_montos      = <" + sum_montos + ">");
            log.error("procesarAltaReinstalaciones.ic_if           = <" + ic_if + ">");
            e.printStackTrace();
            hayErrorInesperado = true;
            throw new AppException("Error inesperado en el procesamiento del Carga de Recuperaciones", e);

        } finally {

            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                }
            }
            ;
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            ;
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            ;
            if (con.hayConexionAbierta()) {
                if (hayErrorInesperado || retorno.getErrores().length() > 0 || retorno.getCifras().length() > 0) {
                    con.terminaTransaccion(false);
                } else {
                    con.terminaTransaccion(true);
                }
                con.cierraConexionDB();
            }
            log.info("procesarAltaReinstalaciones(S)");

        }

        return retorno;

    }

    /**
     *
     * Realiza el alta de Reinstalaciones de Garant�as, las cuales ya han sido
     * previamente validadas.
     * @throws AppException
     *
     * @param icProceso 		Clave del proceso de carga
     * @param ic_if 			Clave del IF
     * @param num_registros Numero de registros cargados
     * @param impte_total 	Importe total cargado
     * @param ic_usuario 	Clave del usuario
     * @param reciboFirma 	Recibo obtenido al realizar la autenticaci�n de la firma digital
     *
     * @return Objeto ResultadosGar con la informaci�n de la ejecuci�n
     *
     * @author Salim Hernandez ( 20/03/2013 10:36:07 a.m. )
     * @since  F007 - 2013, 20/03/2013 10:36:07 a.m.
     *
     */
    public ResultadosGar transmitirAltaReinstalaciones(String icProceso, String ic_if, String num_registros,
                                                       String impte_total, String ic_usuario,
                                                       String reciboFirma) throws AppException {

        log.info("transmitirAltaReinstalaciones(E)");

        String _fechaMsg = (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new java.util.Date());
        ResultadosGar retorno = new ResultadosGar();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qrySentencia = "";

        boolean ok = true;

        try {

            con.conexionDB();

            BigDecimal folio = null;
            BigDecimal ic_nafin_electronico = null;
            Integer ic_if_siag = null;

            String fecha = "";
            String fecha_valor = "";
            String hora = "";
            String anio = "";
            int ic_contragarante = 0;

            boolean usarCorreccionDeFecha =
                false; // Se asigna a falso para que no ajuste a fecha valor hasta que se solicite en FODEA
            Registros reg = this.getDatosFolio(ic_if, usarCorreccionDeFecha, "10");

            if (reg.next()) {

                folio = new BigDecimal(reg.getString("folio"));
                ic_nafin_electronico = new BigDecimal(reg.getString("ic_nafin_electronico"));
                ic_if_siag = new Integer(reg.getString("ic_if_siag"));
                fecha = reg.getString("fecha");
                //fecha_valor 			= reg.getString("FECHA_VALOR");
                fecha_valor =
                    reg.getString("fecha"); // Se asigna la misma fecha para que no ajuste a fecha valor hasta que se solicite en FODEA
                hora = reg.getString("hora");
                anio = reg.getString("anio");
                fecha_valor = (fecha_valor.equals(fecha)) ? fecha_valor + " " + hora : fecha_valor + " 00:00";
                //trim_calif = reg.getString("trimestre");

            }

            ic_contragarante = this.getClaveContragarante(ic_if); //FODEA 002 - 2011 ACF

            qrySentencia =
                " INSERT INTO  " + " gti_estatus_solic  " + " (  " + "     ic_folio, " + "     ic_if_siag, " +
                "     ic_nafin_electronico, " + "     df_fecha_hora,  " + "     in_registros, " +
                "     fn_impte_total, " + "     ic_usuario_facultado, " + "     ic_situacion,  " +
                "     ic_sit_transfer, " + "     ic_tipo_operacion, " + "     ic_contragarante, " +
                "     cg_recibo_firma, " + "     df_registro " + " )  " + " VALUES " + " (  " + "     ?, " +
                "     ?, " + "     ?, " + "     to_date(?,'dd/mm/yyyy HH24:MI'), " + "     ?, " + "     ?, " +
                "     ?, " + "     ?, " + "     ?, " + "     ?, " + "     ?, " + "     ?, " +
                "     to_date(?,'dd/mm/yyyy HH24:MI') " + " ) ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setBigDecimal(3, ic_nafin_electronico);
            ps.setString(4, fecha_valor);
            ps.setString(5, num_registros);
            ps.setBigDecimal(6, new BigDecimal(impte_total)); // Debug info: este campo deber�a ser un bigdecimal
            ps.setString(7, ic_usuario);
            ps.setInt(8, 1); // 1: Envio para valiadaci�n y carga
            ps.setInt(9, 0);
            ps.setInt(10, 10); // 10 = Alta de Reinstalaci�n de Garant�as
            ps.setInt(11, ic_contragarante);
            ps.setString(12, reciboFirma);
            ps.setString(13, fecha + " " + hora);

            ps.executeUpdate();
            ps.close();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.debug("transmitirAltaReinstalaciones(" + _fechaMsg + "_" + icProceso +
                      ")::Insercion gti_estatus_solic...OK");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia =
                " INSERT INTO " + " gti_contenido_arch " + " ( " + "     ic_folio, " + "     ic_if_siag, " +
                "     ic_linea, " + "     ig_programa, " + "     cg_contenido, " + "     ic_if " + " ) " + " SELECT " +
                "     ?, " + "     ?, " + "     ic_linea, " + "     ig_programa, " + "     cg_contenido, " + "     ? " +
                " FROM " + "     gtitmp_contenido_arch " + " WHERE " + "     ic_proceso = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setBigDecimal(1, folio);
            ps.setInt(2, ic_if_siag.intValue());
            ps.setInt(3, Integer.parseInt(ic_if));
            ps.setString(4, icProceso);

            int numRegistrosInsertadosContArch = ps.executeUpdate();
            ps.close();

            //------------------------ Inicio mensajes para debug ---------------------------
            log.debug("transmitirAltaReinstalaciones(" + _fechaMsg + "_" + icProceso +
                      ")::Inserciones gti_contenido_arch...OK(" + numRegistrosInsertadosContArch + ")");
            //-------------------------- Fin mensajes para debug ----------------------------

            qrySentencia = " DELETE FROM " + "   gtitmp_contenido_arch " + " WHERE " + "   ic_proceso = ? ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, icProceso);

            ps.execute();

            retorno.setFolio(folio.toPlainString());
            retorno.setFecha(fecha);
            retorno.setHora(hora);
            retorno.setFechaValor(fecha_valor);

        } catch (Exception e) {

            log.error("transmitirAltaReinstalaciones(Exception)");
            log.error("transmitirAltaReinstalaciones.icProceso 		= <" + icProceso + ">");
            log.error("transmitirAltaReinstalaciones.ic_if 				= <" + ic_if + ">");
            log.error("transmitirAltaReinstalaciones.num_registros 	= <" + num_registros + ">");
            log.error("transmitirAltaReinstalaciones.impte_total 		= <" + impte_total + ">");
            log.error("transmitirAltaReinstalaciones.ic_usuario 		= <" + ic_usuario + ">");
            log.error("transmitirAltaReinstalaciones.reciboFirma 		= <" + reciboFirma + ">");
            e.printStackTrace();
            ok = false;
            throw new AppException("Error al transmitir el alta de Reinstalaciones de Garant�as", e);

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            ;
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            log.info("transmitirAltaReinstalaciones(S)");

        }

        return retorno;

    }

    /**
     *
     * Devuelve un query que se utiliza para consultar el detalle de los registros con solicitud de
     * reinstalaciones de las garantias para un folio especifico.
     * @throws AppException
     *
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>)
     * @param folio		 <tt>String</tt> con el numero de folio de la solicitud, tal como
     *                    aparece en la tabla GTI_CONTENIDO_ARCH
     *
     * @return            <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
     *                    los parametros de consulta en el atributo: <tt>parameters</tt>.
     *
     * @author            Salim Hernandez ( 25/03/2013 04:36:36 p.m. )
     * @since             Fodea 007 - 2013, 25/03/2013 04:36:36 p.m.
     *
     */
    public HashMap getQueryDetalleSolicitudReinstalaciones(String claveIF, String folio) throws AppException {

        log.info("getQueryDetalleSolicitudReinstalaciones(E)");

        HashMap query = new HashMap();
        StringBuffer text = new StringBuffer();
        List parameters = new ArrayList();

        String claveSIAG = null;

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String queryClaveSIAG = null;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Obtener Clave SIAG del IF
            queryClaveSIAG =
                " SELECT        " + "    ic_if_siag " + " FROM          " + "    comcat_if  " + " WHERE         " +
                "    ic_if = ?  ";
            ps = con.queryPrecompilado(queryClaveSIAG);
            ps.setInt(1, Integer.parseInt(claveIF));
            rs = ps.executeQuery();
            if (rs.next()) {
                claveSIAG = rs.getString("ic_if_siag");
            }

            // Preparar query para consultar el detalle
            text.append(" SELECT                     " + "  CG_CONTENIDO AS REGISTRO  " +
                        " FROM                       " + "  GTI_CONTENIDO_ARCH        " +
                        " WHERE                      " + "  IC_IF_SIAG = ? AND        " +
                        "  IC_FOLIO   = ?            " + " ORDER BY IC_LINEA ASC      ");

            parameters.add(new Integer(claveSIAG));
            parameters.add(new BigDecimal(folio));

            query.put("text", text.toString());
            query.put("parameters", parameters);

        } catch (Exception e) {

            log.error("getQueryDetalleSolicitudReinstalaciones(Exception)");
            log.error("getQueryDetalleSolicitudReinstalaciones.claveIF = <" + claveIF + ">");
            log.error("getQueryDetalleSolicitudReinstalaciones.folio   = <" + folio + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al preparar Consulta del Detalle de la Solicitud de Reinstalacion de Garant�as.");

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            ;
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            ;
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getQueryDetalleSolicitudReinstalaciones(S)");

        }

        return query;

    }

    /**
     * Devuelve la Clave SIAG del IF consultado
     * @throws AppException
     *
     * @param claveIF     <tt>String</tt> con la clave del intermediario financiero, como
     *						    aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>).
     *
     * @return Regresa un <tt>int</tt> con la clave siag del IF
     *
     * @author jshernandez
     * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 10/10/2013 12:32:19 p.m.
     *
     */
    public int getClaveIfSiag(String claveIF) throws AppException {

        log.info("getClaveIfSiag(E)");

        StringBuffer query = new StringBuffer();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;

        int claveIfSiag = 0;

        try {

            // Conectarse a la base de datos
            con.conexionDB();

            query.append(" SELECT IC_IF_SIAG FROM COMCAT_IF WHERE IC_IF = ? ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveIF));

            rs = ps.executeQuery();
            if (rs.next()) {
                claveIfSiag = rs.getInt("IC_IF_SIAG");
            }

        } catch (Exception e) {

            log.error("getClaveIfSiag(Exception)");
            log.error("getClaveIfSiag.claveIF = <" + claveIF + ">");

            e.printStackTrace();

            throw new AppException("No se pudo obtener la Clave IF SIAG: " + e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getClaveIfSiag(S)");

        }

        return claveIfSiag;

    }

    /**
     * Verifica que el valor del parametro &quot;campo&quot;, pertenezca a una clave de estatus
     * de la siguiente lista: 'VIG', 'VEN', 'CAD', 'LIQ', 'CAN', 'REC' � 'CXS'.
     *
     * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
     * @param campo Valor a validar
     * @param nombreCampo nombre del campo a validar
     * @param rg Objeto de resultado de garantias.
     *
     * @return <tt>true</tt> si la validacion fue exitosa y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 10/10/2013 05:11:03 p.m.
     *
     */
    private boolean validaEstatusGarantia(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean esValido = false;

        try {

            if ("VIG".equals(campo)) {
                esValido = true;
            } else if ("VEN".equals(campo)) {
                esValido = true;
            } else if ("CAD".equals(campo)) {
                esValido = true;
            } else if ("LIQ".equals(campo)) {
                esValido = true;
            } else if ("CAN".equals(campo)) {
                esValido = true;
            } else if ("REC".equals(campo)) {
                esValido = true;
            } else if ("CXS".equals(campo)) {
                esValido = true;
            }

            if (!esValido) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(" El valor del campo ");
                rg.appendErrores(nombreCampo);
                rg.appendErrores(" debe ser alguno de estos: VIG, VEN, CAD, LIQ, CAN, REC � CXS. \n");
            }

        } catch (Exception e) {

            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            rg.appendErrores(" debe ser alguno de estos: VIG, VEN, CAD, LIQ, CAN, REC � CXS. \n");
            esValido = false;

        }

        return esValido;

    }

    /**
     * Verifica que el valor del parametro &quot;campo&quot;, pertenezca a una clave de estatus
     * de la siguiente lista: 'EP', 'RT', 'EP/AR', 'RT/AR' � 'AC'.
     *
     * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
     * @param campo Valor a validar
     * @param nombreCampo nombre del campo a validar
     * @param rg Objeto de resultado de garantias.
     *
     * @return <tt>true</tt> si la validacion fue exitosa y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 05:49:56 p.m.
     *
     */
    private boolean validaEstatusRecuperacion(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        boolean esValido = false;

        try {

            if ("EP".equals(campo)) {
                esValido = true;
            } else if ("RT".equals(campo)) {
                esValido = true;
            } else if ("EP/AR".equals(campo)) {
                esValido = true;
            } else if ("RT/AR".equals(campo)) {
                esValido = true;
            } else if ("AC".equals(campo)) {
                esValido = true;
            }

            if (!esValido) {
                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(" El valor del campo ");
                rg.appendErrores(nombreCampo);
                rg.appendErrores(" debe ser alguno de estos: EP, RT, EP/AR, RT/AR � AC. \n");
            }

        } catch (Exception e) {

            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            rg.appendErrores(" debe ser alguno de estos: EP, RT, EP/AR, RT/AR � AC. \n");
            esValido = false;

        }

        return esValido;

    }

    /**
     * Realiza la validacion de que el valor de &quot;campo&quot; sea una fecha
     * en el formato AAAAMM
     * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
     * @param campo Valor a validar
     * @param nombreCampo nombre del campo a validar
     * @param rg Objeto de resultado de garantias.
     *
     * @author jshernandez
     * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 10/10/2013 05:10:55 p.m.
     *
     */
    private boolean validaAnioMes(int numLinea, String campo, String nombreCampo, ResultadosGar rg) {

        String mes = "";
        String anio = "";
        boolean error = false;

        try {

            anio = campo.length() > 3 ? campo.substring(0, 4) : "";
            mes = campo.length() > 5 ? campo.substring(4, 6) : "";

            if (!anio.matches("[1-2]\\d{3}")) { // Nota: Esta validacion solo es v�lida para 2000 a�os contando a partir de a�o 1000 :)

                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(" El valor del campo '");
                rg.appendErrores(nombreCampo);
                rg.appendErrores("' debe ser una fecha valida con formato AAAAMM. \n");
                error = true;

            } else if (!mes.matches("0[1-9]") && !mes.matches("1[0-2]")) {

                rg.setErrorLinea(true);
                rg.appendErrores("Error en la linea:");
                rg.appendErrores(String.valueOf(numLinea));
                rg.appendErrores(" El valor del campo '");
                rg.appendErrores(nombreCampo);
                rg.appendErrores("' debe ser una fecha valida con formato AAAAMM. \n");
                error = true;

            }

        } catch (Exception e) {

            rg.setErrorLinea(true);
            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El campo '");
            rg.appendErrores(nombreCampo);
            rg.appendErrores("' debe ser una fecha valida en formato AAAAMM. \n");
            error = true;

        }

        return !error;

    }

    /**
     *
     * Valida que el valor campo proporcionado no se encuentre en la lista de claves repetidas.
     *
     * @param numLinea        	 Numero de linea en el archivo
     * @param campo           	 Valor del campo a Validar
     * @param nombreCampo     	 Descripcion del campo que se valida
     * @param rg           	  	 Objeto que almacena los resultados del procesamiento
     * @param clavesRepetidas	 Lista de Claves Repetidas
     * @param incluyeValorCampo Indica si se incluira <tt>true</tt> o no <tt>false</tt> el valor del
     *                          campo en el mensaje de error.
     *
     * @return            <tt>false</tt> si el campo proporcionado se encuentra en la lista
     *                    de claves repetidas. <tt>true</tt> en caso contrario.
     *
     */
    private boolean validaNoRepetido(int numLinea, String campo, String nombreCampo, ResultadosGar rg,
                                     List clavesRepetidas, boolean incluyeValorCampo) {

        boolean validacion = true;

        if (clavesRepetidas != null && campo != null && clavesRepetidas.contains(campo)) {

            rg.appendErrores("Error en la linea:");
            rg.appendErrores(String.valueOf(numLinea));
            rg.appendErrores(" El valor del campo ");
            rg.appendErrores(nombreCampo);
            if (incluyeValorCampo)
                rg.appendErrores(campo);
            rg.appendErrores(" se encuentra repetido.\n");
            rg.setErrorLinea(true);
            validacion = false;

        }

        return validacion;

    }

    /**
     *
     * Devuelve la lista de "Portafolios" de Conciliacion Automatica para un IF en espec�fico.
     * Solo trae los registros con mes a conciliar anteriores al mes en curso.
     *
     * Este m�todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
     *
     * @throws AppException
     *
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     * @param clavesSituacionConciliacion <tt>String</tt> con las claves de situacion conciliacion separadas por comas.
     * @param claveIf <tt>String</tt> con la Clave del Intermediario Financiero, como aparece en COMCAT_IF.IC_IF.
     *
     * @return <tt>List</tt> con los portafolios consultados.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 25/10/2013 11:54:04 a.m.
     *
     */
    public List getListaPortafoliosConciliacionAutomatica(String claveTipoConciliacion,
                                                          String clavesSituacionConciliacion,
                                                          String claveIf) throws AppException {

        log.info("getListaPortafoliosConciliacionAutomatica(E)");

        List registros = new ArrayList();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        try {

            String claveFiso = getClaveFISO(claveIf);
            String claveIfSiag = String.valueOf(getClaveIfSiag(claveIf));

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Consultar el mes Actual
            int mesActual = 0;
            query.setLength(0);
            query.append("SELECT TO_CHAR(sysdate,'YYYYMM') AS MES_ACTUAL FROM DUAL");

            ps = con.queryPrecompilado(query.toString());
            rs = ps.executeQuery();

            if (rs.next()) {
                mesActual = rs.getInt("MES_ACTUAL");
            }
            rs.close();
            ps.close();

            // Preparar arreglo de variables bind con las situaciones de conciliacion
            String[] situacionesConciliacion = clavesSituacionConciliacion.split(",", -1);

            StringBuffer variablesBind = new StringBuffer();
            for (int i = 0; i < situacionesConciliacion.length; i++) {
                if (i > 0)
                    variablesBind.append(",");
                variablesBind.append("?");
            }

            // Consultar registros
            query.setLength(0);
            query.append("SELECT                                                     " +
                         "   IC_MES_CONCILIAR,                                       " +
                         "   IC_FISO,                                                " +
                         "   IC_IF_SIAG,                                             " +
                         "   IC_TIPO_CONCILIACION,                                   " +
                         "   TO_CHAR(DF_FECHA_DESCARGA,'DD/MM/YYYY') AS DF_FECHA_DESCARGA, " +
                         "   CG_USUARIO_DESCARGA,                                    " +
                         "   CASE WHEN BI_ARCHIVO_PORTAFOLIO_NAFIN IS NOT NULL THEN  " +
                         "    'true'                                                 " +
                         "   ELSE                                                    " +
                         "    'false'                                                " +
                         "   END AS EXISTE_PORTAFOLIO_NAFIN                          " +
                         "FROM                                                       " +
                         "   GTI_CONC_ARCHIVO_PORTAFOLIO                             " +
                         "WHERE                                                      " +
                         "   IC_TIPO_CONCILIACION = ? AND                            " +
                         "   IC_SITUACION_CONCILIACION IN ( ");
            query.append(variablesBind);
            query.append(") AND                                                      " +
                         "  IC_FISO               = ? AND                            " +
                         "  IC_IF_SIAG            = ? AND                            " +
                         "  IC_MES_CONCILIAR      < ?                                " +
                         "ORDER BY                                                   " +
                         "   IC_MES_CONCILIAR                                        " +
                         "DESC                                                       ");

            ps = con.queryPrecompilado(query.toString());
            int idx = 1;
            ps.setInt(idx++, Integer.parseInt(claveTipoConciliacion));
            for (int i = 0; i < situacionesConciliacion.length; i++) {
                ps.setInt(idx++, Integer.parseInt(situacionesConciliacion[i]));
            }
            ps.setLong(idx++, Long.parseLong(claveFiso));
            ps.setInt(idx++, Integer.parseInt(claveIfSiag));
            ps.setInt(idx++, mesActual);
            rs = ps.executeQuery();

            HashMap registro = null;
            while (rs.next()) {

                String icMesConciliar =
                    (rs.getString("IC_MES_CONCILIAR") == null) ? "" : rs.getString("IC_MES_CONCILIAR");
                String icFiso = (rs.getString("IC_FISO") == null) ? "" : rs.getString("IC_FISO");
                String icIfSiag = (rs.getString("IC_IF_SIAG") == null) ? "" : rs.getString("IC_IF_SIAG");
                String icTipoConciliacion =
                    (rs.getString("IC_TIPO_CONCILIACION") == null) ? "" : rs.getString("IC_TIPO_CONCILIACION");
                String fechaDescarga =
                    (rs.getString("DF_FECHA_DESCARGA") == null) ? "" : rs.getString("DF_FECHA_DESCARGA");
                String usuarioDescarga =
                    (rs.getString("CG_USUARIO_DESCARGA") == null) ? "" : rs.getString("CG_USUARIO_DESCARGA");
                String existePortafolioNafin =
                    (rs.getString("EXISTE_PORTAFOLIO_NAFIN") == null) ? "false" :
                    rs.getString("EXISTE_PORTAFOLIO_NAFIN");

                registro = new HashMap();
                registro.put("IC_MES_CONCILIAR", icMesConciliar);
                registro.put("IC_FISO", icFiso);
                registro.put("IC_IF_SIAG", icIfSiag);
                registro.put("IC_TIPO_CONCILIACION", icTipoConciliacion);
                registro.put("DF_FECHA_DESCARGA", fechaDescarga);
                registro.put("CG_USUARIO_DESCARGA", usuarioDescarga);
                registro.put("EXISTE_PORTAFOLIO_NAFIN",
                             "true".equals(existePortafolioNafin) ? new Boolean(true) : new Boolean(false));

                registros.add(registro);

            }

        } catch (Exception e) {

            log.error("getListaPortafoliosConciliacionAutomatica(Exception)");
            log.error("getListaPortafoliosConciliacionAutomatica.claveTipoConciliacion       = <" +
                      claveTipoConciliacion + ">");
            log.error("getListaPortafoliosConciliacionAutomatica.clavesSituacionConciliacion = <" +
                      clavesSituacionConciliacion + ">");
            log.error("getListaPortafoliosConciliacionAutomatica.claveIf                     = <" + claveIf + ">");
            e.printStackTrace();

            throw new AppException("Fall� la consulta de portafolios de Conciliaci�n Autom�tica: " + e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getListaPortafoliosConciliacionAutomatica(S)");

        }

        return registros;

    }

    /**
     * Obtiene el numero m�ximo de dias permitidos para realizar la carga de portafolios de conciliaci�n de garant�as
     * o para firmar la extracci�n del SIAG de los mismos para el mes actual.
     *
     * @throws AppException
     *
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     *
     * @return <tt>int</tt> con el n�mero m�ximo de d�as h�biles permitidos.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliaci�n Autom�tica de Garant�as; 15/01/2014 05:07:39 p.m.
     *
     */
    public int getNumeroDiasHabilesPermitidosConciliacionAutomatica(String claveTipoConciliacion) throws AppException {

        log.info("getNumeroDiasHabilesPermitidosConciliacionAutomatica(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        int numeroDiasHabiles = 0;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Determinar si la cedula de conciliaci�n est� sin extraer
            query.append("SELECT                                             " +
                         "   IN_PRIMEROS_DIAS_HABILES AS NUMERO_DIAS_HABILES " +
                         "FROM                                               " +
                         "  GTICAT_TIPO_CONCILIACION                         " +
                         "WHERE                                              " +
                         "  IC_TIPO_CONCILIACION = ?                         ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveTipoConciliacion));

            rs = ps.executeQuery();
            if (rs.next()) {
                numeroDiasHabiles = rs.getInt("NUMERO_DIAS_HABILES");
            }

        } catch (Exception e) {

            log.error("getNumeroDiasHabilesPermitidosConciliacionAutomatica(Exception)");
            log.error("getNumeroDiasHabilesPermitidosConciliacionAutomatica.claveTipoConciliacion = <" +
                      claveTipoConciliacion + ">");
            e.printStackTrace();

            throw new AppException("Fall� la consulta del numero maximo de dias habiles permitidos: " + e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getNumeroDiasHabilesPermitidosConciliacionAutomatica(E)");

        }

        return numeroDiasHabiles;

    }

    /**
     * Determina si el portafolio a extraer cumple con las condiciones para que su extracci�n pueda
     * ser firmada digitalmente.
     *
     * Este m�todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
     *
     * @throws AppException
     *
     *	@param claveMesConciliar <tt>String</tt> con el n�mero del Mes a Conciliar. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     *
     * @return <tt>true</tt> en caso de que se requiera la firma y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 24/10/2013 04:44:49 p.m.
     *
     */
    public boolean requiereFirmaDescargaPortafolioConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                         String claveIfSiag,
                                                                         String claveTipoConciliacion) throws AppException {

        log.info("requiereFirmaDescargaPortafolioConciliacionAutomatica(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        boolean requiereFirma = false;

        try {

            // Obtener n�mero de d�as h�biles permitidos
            int primerosDiasHabilesPermitidos =
                getNumeroDiasHabilesPermitidosConciliacionAutomatica(claveTipoConciliacion);

            // Variables Auxiliares

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            Calendar aux = dateFormat.getCalendar();

            // Determinar Fecha Actual

            String fechaActual = Fecha.getFechaActual();
            dateFormat.parse(fechaActual);
            Calendar fechaActual01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));

            // Determinar el Primer D�a del Mes Anterior al mes actual

            dateFormat.parse(fechaActual);
            Calendar primerDiaDelMesAnterior01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), 1);
            primerDiaDelMesAnterior01.add(Calendar.MONTH, -1);

            // 1. VALIDAR QUE EL MES A CONCILIAR CORRESPONDA AL MES ANTERIOR DE LA FECHA ACTUAL, EN CASO CONTRARIO REGRESAR false

            // Determinar el Primer D�a del Mes a Conciliar

            String primerDiaDelMesConciliar =
                "01/" + claveMesConciliar.substring(4, 6) + "/" + claveMesConciliar.substring(0, 4);
            dateFormat.parse(primerDiaDelMesConciliar);
            Calendar primerDiaDelMesConciliar01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));

            if (!primerDiaDelMesConciliar01.equals(primerDiaDelMesAnterior01)) {
                requiereFirma = false;
                return requiereFirma;
            }

            // 2. VALIDAR QUE LA FECHA ACTUAL SE ENCUENTRE DENTRO DE LOS PRIMEROS DIEZ DIAS HABILES DEL MES EN CURSO

            // Determinar Primer Dia H�bil del Mes

            String primerDiaDelMesActual = Fecha.getPrimerDiaHabilDelMes(fechaActual, "dd/MM/yyyy");

            // Determinar ultimo d�a h�bil de periodo de gracia

            String ultimoDiaHabilPermitidoMesActual =
                Fecha.sumaFechaDiasHabiles(primerDiaDelMesActual, "dd/MM/yyyy", primerosDiasHabilesPermitidos - 1);
            dateFormat.parse(ultimoDiaHabilPermitidoMesActual);
            Calendar ultimoDiaHabilPermitidoMesActual01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));

            if (fechaActual01.after(ultimoDiaHabilPermitidoMesActual01)) {
                requiereFirma = false;
                return requiereFirma;
            }

            // 3. VALIDAR QUE AUN NO SE HAYA EXTRAIDO EL PORTAFOLIO PROPORCIONADO POR NAFIN

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Determinar si el portafolio esta sin extraer
            query.append("SELECT                                " +
                         "   DECODE( COUNT(1),0,'false','true' ) AS  PORTAFOLIO_SIN_FIRMAR " +
                         "FROM                                  " + "  GTI_CONC_ARCHIVO_PORTAFOLIO         " +
                         "WHERE                                 " + "  IC_MES_CONCILIAR          = ? AND   " +
                         "  IC_FISO                   = ? AND   " + "  IC_IF_SIAG                = ? AND   " +
                         "  IC_TIPO_CONCILIACION      = ? AND   " + "  IC_SITUACION_CONCILIACION = 1	      " // CLAVE 1: GENERACION DEL PORTAFOLIO NAFIN
                         );

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveMesConciliar));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIfSiag));
            ps.setInt(4, Integer.parseInt(claveTipoConciliacion));

            rs = ps.executeQuery();
            boolean portafolioSinFirmar = false;
            if (rs.next()) {
                portafolioSinFirmar = "true".equals(rs.getString("PORTAFOLIO_SIN_FIRMAR")) ? true : false;
            }

            if (portafolioSinFirmar) {
                requiereFirma = true;
            }

        } catch (Exception e) {

            log.error("requiereFirmaDescargaPortafolioConciliacionAutomatica(Exception)");
            log.error("requiereFirmaDescargaPortafolioConciliacionAutomatica.claveMesConciliar     = <" +
                      claveMesConciliar + ">");
            log.error("requiereFirmaDescargaPortafolioConciliacionAutomatica.claveFiso             = <" + claveFiso +
                      ">");
            log.error("requiereFirmaDescargaPortafolioConciliacionAutomatica.claveIfSiag           = <" + claveIfSiag +
                      ">");
            log.error("requiereFirmaDescargaPortafolioConciliacionAutomatica.claveTipoConciliacion = <" +
                      claveTipoConciliacion + ">");
            e.printStackTrace();

            throw new AppException("Fall� la consulta para determinar si la descarga de archivo requiere firmado digital: " +
                                   e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("requiereFirmaDescargaPortafolioConciliacionAutomatica(E)");

        }

        return requiereFirma;

    }

    /**
     * Registra el firmado digital de la extracci&oacute;n del portafolio NAFIN por parte del IF, adem&aacute;s actualiza el
     * estatus de la situaci&oacute;n transferencia a 2: "RECEPCION DEL PORTAFOLIO NAFIN POR PARTE DEL IF".
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
     *
     * @throws AppException
     *
     *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     * @param acuseFirmaPki <tt>String</tt> con el numero resultante del firmado digital de la descarga del portafolio.
     * @param loginUsuario <tt>String</tt> con el login del usuario que firm&oacute; la extracci&oacute;n.
     *
     * @return <tt>true</tt> en caso de que la operaci&oacute;n sea exitosa y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 28/10/2013 01:25:25 p.m.
     *
     */
    public boolean registraFirmaExtraccionPortafolioConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                           String claveIfSiag,
                                                                           String claveTipoConciliacion,
                                                                           String acuseFirmaPki,
                                                                           String loginUsuario) throws AppException {

        log.info("registraFirmaExtraccionPortafolioConciliacionAutomatica(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        StringBuffer sentence = new StringBuffer();

        boolean exito = true;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            sentence.append("UPDATE                                 " + "  GTI_CONC_ARCHIVO_PORTAFOLIO          " +
                            "SET                                    " + "  IG_PKI_PORTAFOLIO_NF      = ?,       " +
                            "  IC_SITUACION_CONCILIACION = 2,       " + // CLAVE 2: RECEPCION DEL PORTAFOLIO NAFIN POR PARTE DEL IF
                            "  CG_USUARIO_ACTUALIZA      = ?,       " + "  DF_FECHA_ACTUALIZA        = SYSDATE, " +
                            "  CG_USUARIO_DESCARGA       = ?,       " + "  DF_FECHA_DESCARGA         = SYSDATE  " +
                            "WHERE                                  " + "  IC_MES_CONCILIAR          = ? AND    " +
                            "  IC_FISO                   = ? AND    " + "  IC_IF_SIAG                = ? AND    " +
                            "  IC_TIPO_CONCILIACION      = ? AND    " + "  IC_SITUACION_CONCILIACION = 1        " // CLAVE 1:	GENERACION DEL PORTAFOLIO NAFIN
                            );

            ps = con.queryPrecompilado(sentence.toString());
            ps.setBigDecimal(1, new BigDecimal(acuseFirmaPki));
            ps.setString(2, loginUsuario);
            ps.setString(3, loginUsuario);
            ps.setInt(4, Integer.parseInt(claveMesConciliar));
            ps.setLong(5, Long.parseLong(claveFiso));
            ps.setInt(6, Integer.parseInt(claveIfSiag));
            ps.setInt(7, Integer.parseInt(claveTipoConciliacion));
            ps.executeUpdate();

        } catch (Exception e) {

            exito = false;

            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica(Exception)");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.claveMesConciliar     = <" +
                      claveMesConciliar + ">");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.claveFiso             = <" + claveFiso +
                      ">");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.claveIfSiag           = <" +
                      claveIfSiag + ">");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.claveTipoConciliacion = <" +
                      claveTipoConciliacion + ">");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.acuseFirmaPki         = <" +
                      acuseFirmaPki + ">");
            log.error("registraFirmaExtraccionPortafolioConciliacionAutomatica.loginUsuario          = <" +
                      loginUsuario + ">");

            e.printStackTrace();

            throw new AppException("No se pudo guardar registro de la firma digital: " + e.getMessage());

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

            log.info("registraFirmaExtraccionPortafolioConciliacionAutomatica(S)");
        }

        return exito;

    }

    /**
     * Devuelve la fecha en la que se realiz&oacute; la firma digital de la extracci&oacute;n del portafolio, tambi&eacute;n devuelve el login del
     * usuario que realiz&oacute; la extracci&oacute;n.
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
     *
     * @throws AppException
     *
     *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     *
     * @return <tt>HashMap</tt> con el detalle de la consulta.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 28/10/2013 04:26:24 p.m.
     *
     */
    public HashMap getDatosFirmaExtraccionConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                 String claveIfSiag,
                                                                 String claveTipoConciliacion) throws AppException {

        log.info("getDatosFirmaExtraccionConciliacionAutomatica(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        HashMap datos = new HashMap();
        String usuarioDescarga = "";
        String fechaDescarga = "";

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Preparar Query
            query.append("SELECT                                                            " +
                         "   TO_CHAR(DF_FECHA_DESCARGA, 'DD/MM/YYYY') AS DF_FECHA_DESCARGA, " +
                         "   CG_USUARIO_DESCARGA                                            " +
                         "FROM                                                              " +
                         "   GTI_CONC_ARCHIVO_PORTAFOLIO                                    " +
                         "WHERE                                                             " +
                         "  IC_MES_CONCILIAR     = ? AND                                    " +
                         "  IC_FISO              = ? AND                                    " +
                         "  IC_IF_SIAG           = ? AND                                    " +
                         "  IC_TIPO_CONCILIACION = ?                                        ");

            // Realizar la consulta
            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveMesConciliar));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIfSiag));
            ps.setInt(4, Integer.parseInt(claveTipoConciliacion));

            rs = ps.executeQuery();
            if (rs.next()) {
                fechaDescarga = (rs.getString("DF_FECHA_DESCARGA") == null) ? "" : rs.getString("DF_FECHA_DESCARGA");
                usuarioDescarga =
                    (rs.getString("CG_USUARIO_DESCARGA") == null) ? "" : rs.getString("CG_USUARIO_DESCARGA");
            }

            datos.put("fechaDescarga", fechaDescarga);
            datos.put("usuarioDescarga", usuarioDescarga);

        } catch (Exception e) {

            log.error("getDatosFirmaExtraccionConciliacionAutomatica(Exception)");
            log.error("getDatosFirmaExtraccionConciliacionAutomatica.claveMesConciliar     = <" + claveMesConciliar +
                      ">");
            log.error("getDatosFirmaExtraccionConciliacionAutomatica.claveFiso             = <" + claveFiso + ">");
            log.error("getDatosFirmaExtraccionConciliacionAutomatica.claveIfSiag           = <" + claveIfSiag + ">");
            log.error("getDatosFirmaExtraccionConciliacionAutomatica.claveTipoConciliacion = <" +
                      claveTipoConciliacion + ">");

            e.printStackTrace();

            throw new AppException("Fall� la consulta del detalle de la extracci�n del portafolio nafin: " +
                                   e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getDatosFirmaExtraccionConciliacionAutomatica(S)");

        }

        return datos;

    }

    /**
     * Determina si la fecha actual se encuentra dentro de los primeros d&iacute;as h&aacute;biles del mes.
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @throws AppException
     *
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
     *
     * @return <tt>true</tt> en caso de que la fecha actual se encuentre dentro de los primeros 10 d&iacute;as h&aacute;biles
     * y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 29/10/2013 04:47:35 p.m.
     *
     */
    public boolean primerosDiasHabilesMesConciliacionAutomatica(String claveTipoConciliacion) throws AppException {

        log.info("primerosDiasHabilesMesConciliacionAutomatica(E)");

        boolean primerosDiasHabiles = false;

        try {

            // Obtener n�mero de d�as h�biles permitidos
            int primerosDiasHabilesPermitidos =
                getNumeroDiasHabilesPermitidosConciliacionAutomatica(claveTipoConciliacion);

            // Variables Auxiliares

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            Calendar aux = dateFormat.getCalendar();

            // Determinar Fecha Actual

            String fechaActual = Fecha.getFechaActual();
            dateFormat.parse(fechaActual);
            Calendar fechaActual01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));
            // Determinar Primer D�a H�bil del Mes

            String primerDiaDelMesActual = Fecha.getPrimerDiaHabilDelMes(fechaActual, "dd/MM/yyyy");
            // Determinar ultimo d�a h�bil de periodo de gracia

            String ultimoDiaHabilPermitidoMesActual =
                Fecha.sumaFechaDiasHabiles(primerDiaDelMesActual, "dd/MM/yyyy", primerosDiasHabilesPermitidos - 1);
            dateFormat.parse(ultimoDiaHabilPermitidoMesActual);
            Calendar ultimoDiaHabilPermitidoMesActual01 =
                new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));

            primerosDiasHabiles = fechaActual01.after(ultimoDiaHabilPermitidoMesActual01) ? false : true;

        } catch (Exception e) {

            log.error("primerosDiasHabilesMesConciliacionAutomatica(Exception)");
            e.printStackTrace();

            throw new AppException("Fall� la validaci�n de d�as h�biles: " + e.getMessage());

        } finally {

            log.info("primerosDiasHabilesMesConciliacionAutomatica(E)");

        }

        return primerosDiasHabiles;

    }

    /**
     * Devuelve la clave en la que se encuentra la situaci&oacute;n de la conciliaci&oacute;n autom&aacute;tica de garant&iacute;as para un tipo de conciliaci&oacute;n
     * el cual se especifica en el par&aacute;metro: <tt>claveTipoConciliacion</tt>.
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @throws AppException
     *
     *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliaci&oacute;n. Para ver los tipos de conciliaci&oacute;n
     *                              usar tabla GTICAT_TIPO_CONCILIACION.
     *
     * @return <tt>int</tt> con la clave de la situaci&oacute;n de la conciliaci&oacute;n. <tt>0</tt> en caso de que el registro de conciliaci&oacute;n
     * autom�tica no exista.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 29/10/2013 06:12:11 p.m.
     *
     */
    public int getClaveSituacionConciliacionAutomatica(String claveMesConciliar, String claveFiso, String claveIfSiag,
                                                       String claveTipoConciliacion) throws AppException {

        log.info("getClaveSituacionConciliacionAutomatica(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        int claveSituacionConciliacion = 0;

        try {

            // En caso de que algun parametro no haya sido especificado, se regresa 0
            if (claveMesConciliar == null || claveMesConciliar.matches("\\s*")) {
                return claveSituacionConciliacion;
            } else if (claveFiso == null || claveFiso.matches("\\s*")) {
                return claveSituacionConciliacion;
            } else if (claveIfSiag == null || claveIfSiag.matches("\\s*")) {
                return claveSituacionConciliacion;
            } else if (claveTipoConciliacion == null || claveTipoConciliacion.matches("\\s*")) {
                return claveSituacionConciliacion;
            }

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Preparar Query
            query.append("SELECT                              " + "   IC_SITUACION_CONCILIACION        " +
                         "FROM                                " + "   GTI_CONC_ARCHIVO_PORTAFOLIO      " +
                         "WHERE                               " + "  IC_MES_CONCILIAR          = ? AND " +
                         "  IC_FISO                   = ? AND " + "  IC_IF_SIAG                = ? AND " +
                         "  IC_TIPO_CONCILIACION      = ?     ");

            // Realizar la consulta
            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveMesConciliar));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIfSiag));
            ps.setInt(4, Integer.parseInt(claveTipoConciliacion));

            rs = ps.executeQuery();
            if (rs.next()) {
                claveSituacionConciliacion = rs.getInt("IC_SITUACION_CONCILIACION");
            }

        } catch (Exception e) {

            log.error("getClaveSituacionConciliacionAutomatica(Exception)");
            log.error("getClaveSituacionConciliacionAutomatica.claveMesConciliar          = <" + claveMesConciliar +
                      ">");
            log.error("getClaveSituacionConciliacionAutomatica.claveFiso                  = <" + claveFiso + ">");
            log.error("getClaveSituacionConciliacionAutomatica.claveIfSiag                = <" + claveIfSiag + ">");
            log.error("getClaveSituacionConciliacionAutomatica.claveTipoConciliacion      = <" + claveTipoConciliacion +
                      ">");

            e.printStackTrace();

            throw new AppException("Fall� la consulta de la situaci�n de la conciliaci�n autom�tica: " +
                                   e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getClaveSituacionConciliacionAutomatica(S)");

        }

        return claveSituacionConciliacion;

    }

    /**
     * Devuelve la clave y el nombre del mes anterior al mes en curso del cual se realizar&aacute; la
     * conciliaci&oacute;n autom&aacute;tica.
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @throws AppException
     *
     * @return <tt>HashMap</tt> con la clave y nombre del mes a conciliar.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 30/10/2013 12:02:22 p.m.
     *
     */
    public HashMap getMesConciliacionAutomatica() throws AppException {

        log.info("getMesConciliacionAutomatica(E)");

        HashMap mesConciliacion = new HashMap();
        String claveMes = null;
        String nombreMes = null;

        try {

            // Variables Auxiliares
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            Calendar aux = dateFormat.getCalendar();

            // Determinar Fecha Actual
            String fechaActual = Fecha.getFechaActual();

            // Determinar el Primer D�a del Mes a Conciliar ( mes anterior al mes en curso )
            dateFormat.parse(fechaActual);
            Calendar primerDiaMesConciliar = new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), 1);
            primerDiaMesConciliar.add(Calendar.MONTH, -1);

            int numeroMes = primerDiaMesConciliar.get(Calendar.MONTH) + 1;
            int anioMes = primerDiaMesConciliar.get(Calendar.YEAR);

            // Determinar clave del mes
            claveMes = String.valueOf(anioMes) + (numeroMes < 10 ? "0" : "") + String.valueOf(numeroMes);

            // Determinar nombre del mes
            switch (numeroMes) {
            case 1:
                nombreMes = "Enero " + String.valueOf(anioMes);
                break;
            case 2:
                nombreMes = "Febrero " + String.valueOf(anioMes);
                break;
            case 3:
                nombreMes = "Marzo " + String.valueOf(anioMes);
                break;
            case 4:
                nombreMes = "Abril " + String.valueOf(anioMes);
                break;
            case 5:
                nombreMes = "Mayo " + String.valueOf(anioMes);
                break;
            case 6:
                nombreMes = "Junio " + String.valueOf(anioMes);
                break;
            case 7:
                nombreMes = "Julio " + String.valueOf(anioMes);
                break;
            case 8:
                nombreMes = "Agosto " + String.valueOf(anioMes);
                break;
            case 9:
                nombreMes = "Septiembre " + String.valueOf(anioMes);
                break;
            case 10:
                nombreMes = "Octubre " + String.valueOf(anioMes);
                break;
            case 11:
                nombreMes = "Noviembre " + String.valueOf(anioMes);
                break;
            case 12:
                nombreMes = "Diciembre " + String.valueOf(anioMes);
                break;
            }

            // Enviar resultado
            mesConciliacion.put("CLAVE_MES", claveMes);
            mesConciliacion.put("NOMBRE_MES", nombreMes);

        } catch (Exception e) {

            log.error("getMesConciliacionAutomatica(Exception)");
            e.printStackTrace();

            throw new AppException("No se pudo determinar el Mes a Conciliar: " + e.getMessage());

        } finally {

            log.info("getMesConciliacionAutomatica(S)");

        }

        return mesConciliacion;

    }

    /**
	 * NOTA: EL SIGUIENTE METODO FUE MOVIDO A LA CLASE ejb/GarantiasEJB/src/com/netro/garantias/AuxiliarConciliacionAutomatica.java
	 * DEBIDO A LAS RESTRICCION DE LOS EJB EN EL MANEJO DE ARCHIVO, POR LO QUE SE DEJA AQUI PARA REFERENCIA. NO BORRAR.
	 *
	 * M&eacute;todo que se encarga de transmitir la carga del portafolio IF de la Conciliaci&oacute;n Autom&aacute;tica de Garant&iacute;as.
	 * Actualiza el estatus de la situacion de la conciliaci&oacute;n a: 3 ( Carga del Portafolio Del IF ).
	 *
	 * Este m&eacute;todo se ocupa en las siguientes pantallas:
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
	 *
	 * @throws AppException
	 *
	 *	@param directorioTemporal  <tt>String</tt> con la ruta del directorio donde se subio el archivo a transmitir.
	 *	@param loginUsuario <tt>String</tt> con el login del usuario que realiza la carga.
	 *	@param nombreArchivo <tt>String</tt> con el nombre el nombre del archivo a "transmitir".
	 *	@param acuseFirmaPki <tt>String</tt> con el numero resultante del firmado digital de la transmici&oacute;n del portafolio IF.
	 *	@param numeroRegistros <tt>String</tt> con el n&uacute;mero de Registros cargados por el IF.
	 *	@param montoCarga <tt>String</tt> con el monto de la carga que especific&oacute; el IF.
	 *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
	 * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
	 * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
	 * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliaci&oacute;n. Para ver los tipos de conciliaci&oacute;n
	 *                              usar tabla GTICAT_TIPO_CONCILIACION.
	 *
	 * @return <tt>int</tt> con la clave de la situaci&oacute;n de la conciliaci&oacute;n. <tt>0</tt> en caso de que el registro de conciliaci&oacute;n
	 * autom�tica no exista.
	 *
	 * @author jshernandez
	 * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 29/10/2013 06:12:11 p.m.
	 *
	 */
    // public static ResultadosGar transmitirCargaPortafolioIfConciliacionAutomatica(
    // 	String directorioTemporal,
    // 	String loginUsuario,
    // 	String nombreArchivo,
    // 	String acuseFirmaPki,
    // 	String numeroRegistros,
    // 	String montoCarga,
    // 	String claveMesConciliar,
    // 	String claveFiso,
    // 	String claveIfSiag,
    // 	String claveTipoConciliacion
    // 	);

    /**
     *
     * Realiza la validacion de que el valor de &quot;campo&quot; coincida con la clave del mes a conciliar.
     *
     * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
     * @param campo Valor a validar
     * @param nombreCampo nombre del campo a validar
     * @param claveMesConciliar clave del mes a conciliar, en formato: <tt>AAAAMM</tt>.
     * @param rg Objeto de resultado de garantias.
     *
     * @author jshernandez
     * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 01/11/2013 04:27:39 p.m.
     *
     */
    private boolean validaMesConciliar(int numLinea, String campo, String nombreCampo, String claveMesConciliar,
                                       ResultadosGar rg) {
        boolean error = false;
        try {
            if (!campo.equals(claveMesConciliar)) {
                rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                                 "' debe coincidir con el mes de conciliaci�n.\n");
                rg.setErrorLinea(true);
                error = true;
            }
        } catch (Exception e) {
            rg.appendErrores("Error en la linea:" + numLinea + " El valor del campo '" + nombreCampo +
                             "' debe coincidir con el mes de conciliaci�n.\n");
            rg.setErrorLinea(true);
            error = true;
        }
        return !error;
    }

    /**
     *
     * Valida un registro del Portafolio IF de Saldos Insolutos correspondiente al M�dulo de Conciliaci�n Autom�tica.
     *
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual llevara el registro de los errores de validaci�n.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
     *
     * @param numLinea  Numero de linea que se est� validando.
     * @param registro  Objeto de tipo <tt>SaldoInsolutoConciliacionAutomaticaValidator</tt>, con el contenido del registro a validar.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 01:39:02 p.m.
     *
     */
    public void validarAltaPortafolioIfSaldosInsolutos(int numLinea,
                                                       SaldoInsolutoConciliacionAutomaticaValidator registro) {

        ResultadosGar retorno = registro.getResultadosGar();
        int ifSiag = registro.getClaveIfSiag();
        int longitudClaveFinanciamiento = registro.getLongitudClaveFinanciamiento();
        String claveMesConciliar = registro.getClaveMesConciliar();
        String claveFinanciamiento = null;
        BigDecimal saldoInsolutoFinMes = null;
        List clavesFinanciamientoRepetidas = registro.getClavesFinanciamientoRepetidas();

        // I. CADA CAMPO DENTRO DE UN REGISTRO DEBER� TERMINAR, INVARIABLEMENTE, CON EL S�MBOLO @ (ARROBA).
        int lastChar = registro.getLastChar();
        if (lastChar != -1 && '@' != (char) lastChar) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" La linea debe finalizar con un caracter de arroba.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // II. VALIDAR QUE SE HAYA ENVIADO EL NUMERO DE CAMPOS REQUERIDO
        if (registro.getCampoExtra().length() > 0) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" El numero de campos es incorrecto.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // III. REALIZAR VALIDACIONES POR CAMPO.
        boolean campoValido = false;
        String valorCampo = null;

        // No. 	CAMPO                                       	      	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................
        //  1. 	Clave del Intermediario Financiero SIAG						NUMBER		5		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de identificaci�n del intermediario en SIAG
        campoValido = true;
        valorCampo = registro.getClaveIntermediarioFinancieroSIAG().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno, 5);
        if (campoValido)
            campoValido =
                validaIntermediario(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", ifSiag, retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  2. 	Clave del Financiamiento											CHAR		   20/30	OBLIGATORIO
        //    	OBSERVACIONES: No. de pr�stamo o garant�a (Sociedad  Hipotecaria Federal)
        campoValido = true;
        valorCampo = registro.getClaveFinanciamiento().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Clave del Financiamiento", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "Clave del Financiamiento", retorno, longitudClaveFinanciamiento);
        if (campoValido)
            campoValido =
                validaNoRepetido(numLinea, valorCampo, "con \"Clave del Financiamiento\": ", retorno,
                                 clavesFinanciamientoRepetidas, true);
        if (!campoValido) {
            registro.setHayError(true);
        } else {
            claveFinanciamiento = valorCampo;
        }

        //  3. 	Nombre del acreditado												CHAR			80		OBLIGATORIO
        //    	OBSERVACIONES: Nombre completo del cliente
        campoValido = true;
        valorCampo = registro.getNombreAcreditado().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Nombre del acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Nombre del acreditado", retorno, 80);
        if (!campoValido)
            registro.setHayError(true);

        //  4. 	RFC																		CHAR			13		OBLIGATORIO
        //    	OBSERVACIONES: RFC completo del cliente sin espacios ni guiones.
        campoValido = true;
        valorCampo = registro.getRfc().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "RFC", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "RFC", retorno, 13);
        if (campoValido)
            campoValido = validaRFC_v2(numLinea, valorCampo, "RFC", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  5. 	Saldo Insoluto Fin de Mes											NUMBER		17,2	OBLIGATORIO
        //    	OBSERVACIONES: Saldo registrado al cierre del mes
        campoValido = true;
        valorCampo = registro.getSaldoInsolutoFinMes().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Saldo Insoluto Fin de Mes", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Saldo Insoluto Fin de Mes", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Saldo Insoluto Fin de Mes", retorno);
        if (!campoValido) {
            registro.setHayError(true);
        } else {
            saldoInsolutoFinMes = new BigDecimal(valorCampo);
        }

        //  6. 	Fecha de Disposici�n													DATE			8		OBLIGATORIO
        //    	OBSERVACIONES: Se reporta en formato texto y se conforma "aaaammdd"
        campoValido = true;
        valorCampo = registro.getFechaDisposicion().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Fecha de Disposici�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Fecha de Disposici�n", retorno, 8);
        if (campoValido)
            campoValido = validaFecha(numLinea, valorCampo, "Fecha de Disposici�n", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  7. 	Fecha de Vencimiento													DATE			8		OBLIGATORIO
        //    	OBSERVACIONES: Se reporta en formato texto y se conforma "aaaammdd"
        campoValido = true;
        valorCampo = registro.getFechaVencimiento().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Fecha de Vencimiento", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Fecha de Vencimiento", retorno, 8);
        if (campoValido)
            campoValido = validaFecha(numLinea, valorCampo, "Fecha de Vencimiento", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  8. 	Estatus de la garant�a												CHAR			3		OBLIGATORIO
        //    	OBSERVACIONES: Se indica �nicamente en 3 letras de acuerdo al estatus correspondiente (VIG, VEN, CAD, CAN, LIQ)
        campoValido = true;
        valorCampo = registro.getEstatusGarantia().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Estatus de la Garant�a", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Estatus de la Garant�a", retorno, 3);
        if (campoValido)
            campoValido = validaEstatusGarantia(numLinea, valorCampo, "Estatus de la Garant�a", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  9. 	Moneda																	CHAR			7		OBLIGATORIO
        //    	OBSERVACIONES: Se indica con la palabra PESOS/DOLARES
        campoValido = true;
        valorCampo = registro.getMoneda().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Moneda", retorno, 7);
        if (campoValido)
            campoValido = validaMoneda(numLinea, valorCampo, retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  10. 	Porcentaje Garantizado												NUMBER		5,2	OBLIGATORIO
        //    	OBSERVACIONES: �nicamente en n�mero y  dos decimales.
        campoValido = true;
        valorCampo = registro.getPorcentajeGarantizado().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Porcentaje Garantizado", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Porcentaje Garantizado", retorno, 5, 2);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Porcentaje Garantizado", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  11. 	Tipo de Cr�dito														NUMBER		6		OBLIGATORIO
        //    	OBSERVACIONES: �nicamente en n�mero y sin decimales.
        campoValido = true;
        valorCampo = registro.getTipoCredito().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Tipo de Cr�dito", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Tipo de Cr�dito", retorno, 6);
        if (campoValido)
            campoValido = validaEntero(numLinea, valorCampo, "Tipo de Cr�dito", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  12. 	Mes a Conciliar														NUMBER		6		OBLIGATORIO
        //    	OBSERVACIONES: Fecha del mes que se est�n conciliando, formato texto "aaaamm"
        campoValido = true;
        valorCampo = registro.getMesConciliar().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Mes a Conciliar", retorno, 6);
        if (campoValido)
            campoValido = validaAnioMes(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validaMesConciliar(numLinea, valorCampo, "Mes a Conciliar", claveMesConciliar, retorno);
        if (!campoValido)
            registro.setHayError(true);

        // ACTUALIZAR ESTADISTICAS DE LA CARGA
        // ......................................................................................................
        if (registro.getHayError()) {

            retorno.incNumRegErr();
            if (saldoInsolutoFinMes != null)
                retorno.addSumRegErr(saldoInsolutoFinMes);

        } else {

            // AGREGAR CLAVE DEL FINANCIAMIENTO SEGUN CORRESPONDA
            if (retorno.getNumRegOk() == 0) {

                retorno.appendCorrectos("Claves de la garant�a:\n");
                retorno.appendCorrectos(claveFinanciamiento);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() < 1000) {

                retorno.appendCorrectos(claveFinanciamiento);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() == 1000) {

                retorno.appendCorrectos("S�lo se muestran los primeros 1000 registros.\n");

            }

            // ACTUALIZAR CONTEO DE REGISTROS
            retorno.incNumRegOk();
            if (saldoInsolutoFinMes != null)
                retorno.addSumRegOk(saldoInsolutoFinMes);

        }

    }

    /**
     *
     * Valida las Cifras de Control del Alta de Conciliaci�n de Saldos.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual es un atributo de la clase <tt>FileValidator</tt>.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
     *
     * @param fileValidator  Objeto de tipo SaldoInsolutoConciliacionAutomaticaValidator, con el contenido de las cifras de control y las
     *                       calculadas a partir del archivo.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 01:47:53 p.m.
     *
     */
    public void validarCifrasControlPortafolioIfSaldosInsolutos(SaldoInsolutoConciliacionAutomaticaValidator fileValidator) {

        log.info("validarCifrasControlPortafolioIfSaldosInsolutos(E)");

        ResultadosGar retorno = fileValidator.getResultadosGar();

        try {

            int totalRegistros = fileValidator.getTotalRegistros(); // Cifras de Control
            int totalRegistrosArchivo = retorno.getNumRegOk() + retorno.getNumRegErr();

            if (totalRegistrosArchivo != totalRegistros) {
                log.debug("El numero de registros capturado no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            }

            /*

			// Nota: Se comenta esta seccion debido a la perdida de precision para numeros muy grandes
			// que tiene metodo Comunes.formatoDecimal

			BigDecimal sumatoriaMontosSinError = retorno.getValorSumRegOk();
			BigDecimal sumatoriaMontosConError = retorno.getValorSumRegErr();

			BigDecimal sumatoriaMontos			  = fileValidator.getSumatoriaMontos(); // Cifras de Control
			BigDecimal sumatoriaMontosArchivo  = sumatoriaMontosSinError.add(sumatoriaMontosConError);

			if( sumatoriaMontosArchivo.compareTo(sumatoriaMontos) != 0 ){
				log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo.toString() );
				log.debug("La cifra de control capturada = " + sumatoriaMontos.toString()        );
				retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
			}

			*/

            double sumatoriaMontosSinError = retorno.getSumRegOk();
            double sumatoriaMontosConError = retorno.getSumRegErr();

            double sumatoriaMontos = fileValidator.getSumatoriaMontos().doubleValue(); // Cifras de Control
            double sumatoriaMontosArchivo = sumatoriaMontosSinError + sumatoriaMontosConError;

            if (sumatoriaMontosArchivo != sumatoriaMontos) {
                log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo);
                log.debug("La cifra de control capturada = " + sumatoriaMontos);
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }

        } catch (Exception e) {

            log.error("validarCifrasControlPortafolioIfSaldosInsolutos(Exception)");
            log.error("validarCifrasControlPortafolioIfSaldosInsolutos.fileValidator = <" + fileValidator + ">");
            log.error("validarCifrasControlPortafolioIfSaldosInsolutos.retorno       = <" + retorno + ">");
            e.printStackTrace();

            retorno.appendCifras("Ocurri� un error inesperado al validar las cifras de control: ");
            retorno.appendCifras(e.getMessage());
            retorno.appendCifras("\n");

        }

        log.info("validarCifrasControlPortafolioIfSaldosInsolutos(S)");

    }

    /**
     *
     * Valida un registro del Portafolio IF de Saldos Pendientes por Recuperar correspondiente al M�dulo de Conciliaci�n Autom�tica.
     *
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual llevara el registro de los errores de validaci�n.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
     *
     * @param numLinea  Numero de linea que se est� validando.
     * @param registro  Objeto de tipo <tt>SaldoRecuperacionConciliacionAutomaticaValidator</tt>, con el contenido del registro a validar.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 05:10:35 p.m.
     *
     */
    public void validarAltaPortafolioIfSaldosRecuperaciones(int numLinea,
                                                            SaldoRecuperacionConciliacionAutomaticaValidator registro) {

        ResultadosGar retorno = registro.getResultadosGar();
        int ifSiag = registro.getClaveIfSiag();
        int longitudClaveFinanciamiento = registro.getLongitudClaveFinanciamiento();
        String claveMesConciliar = registro.getClaveMesConciliar();
        String claveFinanciamiento = null;
        BigDecimal saldoRecuperar = null;
        List clavesFinanciamientoRepetidas = registro.getClavesFinanciamientoRepetidas();

        // I. CADA CAMPO DENTRO DE UN REGISTRO DEBER� TERMINAR, INVARIABLEMENTE, CON EL S�MBOLO @ (ARROBA).
        int lastChar = registro.getLastChar();
        if (lastChar != -1 && '@' != (char) lastChar) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" La linea debe finalizar con un caracter de arroba.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // II. VALIDAR QUE SE HAYA ENVIADO EL NUMERO DE CAMPOS REQUERIDO
        if (registro.getCampoExtra().length() > 0) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" El numero de campos es incorrecto.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // III. REALIZAR VALIDACIONES POR CAMPO.
        boolean campoValido = false;
        String valorCampo = null;

        // No. 	CAMPO                                       	      	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................

        //  1. 	Clave del Intermediario Financiero SIAG						NUMBER		5		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de identificaci�n del intermediario
        campoValido = true;
        valorCampo = registro.getClaveIntermediarioFinancieroSIAG().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno, 5);
        if (campoValido)
            campoValido =
                validaIntermediario(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", ifSiag, retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  2. 	Clave del Financiamiento											CHAR			20/30	OBLIGATORIO
        //    	OBSERVACIONES: No. de pr�stamo o garant�a
        campoValido = true;
        valorCampo = registro.getClaveFinanciamiento().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Clave del Financiamiento", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "Clave del Financiamiento", retorno, longitudClaveFinanciamiento);
        if (campoValido)
            campoValido =
                validaNoRepetido(numLinea, valorCampo, "con \"Clave del Financiamiento\": ", retorno,
                                 clavesFinanciamientoRepetidas, true);
        if (!campoValido) {
            registro.setHayError(true);
        } else {
            claveFinanciamiento = valorCampo;
        }

        //  3. 	Nombre del Acreditado												CHAR			80		OBLIGATORIO
        //    	OBSERVACIONES: Nombre completo del cliente
        campoValido = true;
        valorCampo = registro.getNombreAcreditado().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Nombre del Acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Nombre del Acreditado", retorno, 80);
        if (!campoValido)
            registro.setHayError(true);

        //  4. 	RFC																		CHAR			13		OBLIGATORIO
        //    	OBSERVACIONES: RFC completo del cliente sin espacios ni guiones.
        campoValido = true;
        valorCampo = registro.getRfc().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "RFC", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "RFC", retorno, 13);
        if (campoValido)
            campoValido = validaRFC_v2(numLinea, valorCampo, "RFC", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  5. 	Moneda																	CHAR			7		OBLIGATORIO
        //    	OBSERVACIONES: Se indica con la palabra PESOS / DOLARES
        campoValido = true;
        valorCampo = registro.getMoneda().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Moneda", retorno, 7);
        if (campoValido)
            campoValido = validaMoneda(numLinea, valorCampo, retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  6. 	Total Honrado															NUMBER		17,2	OBLIGATORIO
        //    	OBSERVACIONES: Total pagado
        campoValido = true;
        valorCampo = registro.getTotalHonrado().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Total Honrado", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Total Honrado", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Total Honrado", retorno);

        //  7. 	Acumulado de Recuperaciones										NUMBER		17,2	OBLIGATORIO
        //    	OBSERVACIONES: Monto acumulado que ha pagado el cliente
        campoValido = true;
        valorCampo = registro.getAcumuladoRecuperaciones().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Acumulado de Recuperaciones", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Acumulado de Recuperaciones", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Acumulado de Recuperaciones", retorno);

        //  8. 	Estatus de Recuperaci�n												CHAR			5		OBLIGATORIO
        //    	OBSERVACIONES: EP = Recuperaci�n en Proceso, RT = Recuperaci�n Total, EP/AR = Aplicada a Reservas en Proceso de Recuperaci�n, RT/AR = Aplicada a Reservas en Recuperada Totalmente, AC= Aplicada a Castigos
        campoValido = true;
        valorCampo = registro.getEstatusRecuperacion().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Estatus de Recuperaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Estatus de Recuperaci�n", retorno, 5);
        if (campoValido)
            campoValido = validaEstatusRecuperacion(numLinea, valorCampo, "Estatus de Recuperaci�n", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  9. 	Saldo por recuperar													NUMBER		17,2	OBLIGATORIO
        //    	OBSERVACIONES: Saldo pendiente por recuperar
        campoValido = true;
        valorCampo = registro.getSaldoRecuperar().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Saldo por Recuperar", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Saldo por Recuperar", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Saldo por Recuperar", retorno);
        if (!campoValido) {
            registro.setHayError(true);
        } else {
            saldoRecuperar = new BigDecimal(valorCampo);
        }

        //  10. 	Mes a conciliar														NUMBER		6		OBLIGATORIO
        //    	OBSERVACIONES: Fecha del mes que se est�n conciliando, formato texto "aaaamm"
        campoValido = true;
        valorCampo = registro.getMesConciliar().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Mes a Conciliar", retorno, 6);
        if (campoValido)
            campoValido = validaAnioMes(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validaMesConciliar(numLinea, valorCampo, "Mes a Conciliar", claveMesConciliar, retorno);
        if (!campoValido)
            registro.setHayError(true);

        // ACTUALIZAR ESTADISTICAS DE LA CARGA
        // ......................................................................................................
        if (registro.getHayError()) {

            retorno.incNumRegErr();
            if (saldoRecuperar != null)
                retorno.addSumRegErr(saldoRecuperar);

        } else {

            // AGREGAR CLAVE DEL FINANCIAMIENTO SEGUN CORRESPONDA
            if (retorno.getNumRegOk() == 0) {

                retorno.appendCorrectos("Claves de la garant�a:\n");
                retorno.appendCorrectos(claveFinanciamiento);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() < 1000) {

                retorno.appendCorrectos(claveFinanciamiento);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() == 1000) {

                retorno.appendCorrectos("S�lo se muestran los primeros 1000 registros.\n");

            }

            // ACTUALIZAR CONTEO DE REGISTROS
            retorno.incNumRegOk();
            if (saldoRecuperar != null)
                retorno.addSumRegOk(saldoRecuperar);

        }

    }

    /**
     *
     * Valida las Cifras de Control del Portafolio IF de Saldos Pendientes por Recuperar correspondiente
     * al M�dulo de Conciliaci�n Autom�tica.
     *
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual es un atributo de la clase <tt>FileValidator</tt>.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
     *
     * @param fileValidator  Objeto de tipo SaldoRecuperacionConciliacionAutomaticaValidator, con el contenido de las cifras de control y las
     *                       calculadas a partir del archivo.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 06:09:19 p.m.
     *
     */
    public void validarCifrasControlPortafolioIfSaldosRecuperaciones(SaldoRecuperacionConciliacionAutomaticaValidator fileValidator) {

        log.info("validarCifrasControlPortafolioIfSaldosRecuperaciones(E)");

        ResultadosGar retorno = fileValidator.getResultadosGar();

        try {

            int totalRegistros = fileValidator.getTotalRegistros(); // Cifras de Control
            int totalRegistrosArchivo = retorno.getNumRegOk() + retorno.getNumRegErr();

            if (totalRegistrosArchivo != totalRegistros) {
                log.debug("El numero de registros capturado no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            }

            /*

			// Nota: Se comenta esta seccion debido a la perdida de precision para numeros muy grandes
			// que tiene metodo Comunes.formatoDecimal

			BigDecimal sumatoriaMontosSinError = retorno.getValorSumRegOk();
			BigDecimal sumatoriaMontosConError = retorno.getValorSumRegErr();

			BigDecimal sumatoriaMontos			  = fileValidator.getSumatoriaMontos(); // Cifras de Control
			BigDecimal sumatoriaMontosArchivo  = sumatoriaMontosSinError.add(sumatoriaMontosConError);

			if( sumatoriaMontosArchivo.compareTo(sumatoriaMontos) != 0 ){
				log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo.toString() );
				log.debug("La cifra de control capturada = " + sumatoriaMontos.toString()        );
				retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
			}

			*/

            double sumatoriaMontosSinError = retorno.getSumRegOk();
            double sumatoriaMontosConError = retorno.getSumRegErr();

            double sumatoriaMontos = fileValidator.getSumatoriaMontos().doubleValue(); // Cifras de Control
            double sumatoriaMontosArchivo = sumatoriaMontosSinError + sumatoriaMontosConError;

            if (sumatoriaMontosArchivo != sumatoriaMontos) {
                log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo);
                log.debug("La cifra de control capturada = " + sumatoriaMontos);
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }

        } catch (Exception e) {

            log.error("validarCifrasControlPortafolioIfSaldosRecuperaciones(Exception)");
            log.error("validarCifrasControlPortafolioIfSaldosRecuperaciones.fileValidator = <" + fileValidator + ">");
            log.error("validarCifrasControlPortafolioIfSaldosRecuperaciones.retorno       = <" + retorno + ">");
            e.printStackTrace();

            retorno.appendCifras("Ocurri� un error inesperado al validar las cifras de control: ");
            retorno.appendCifras(e.getMessage());
            retorno.appendCifras("\n");

        }

        log.info("validarCifrasControlPortafolioIfSaldosRecuperaciones(S)");

    }

    /**
     *
     * Valida un registro del Portafolio IF de Comisiones Cobradas correspondiente al M�dulo de Conciliaci�n Autom�tica.
     *
     * Son las validaciones de los campos que componen una linea en el archivo de carga.
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual llevara el registro de los errores de validaci�n.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @param numLinea  Numero de linea que se est� validando.
     * @param registro  Objeto de tipo <tt>ComisionesCobradasConciliacionAutomaticaValidator</tt>, con el contenido del registro a validar.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 07:03:53 p.m.
     *
     */
    public void validarAltaPortafolioIfComisionesCobradas(int numLinea,
                                                          ComisionesCobradasConciliacionAutomaticaValidator registro) {

        ResultadosGar retorno = registro.getResultadosGar();
        int ifSiag = registro.getClaveIfSiag();
        //int 				longitudClaveFinanciamiento		= registro.getLongitudClaveFinanciamiento();
        String claveMesConciliar = registro.getClaveMesConciliar();
        String claveIntermediarioFinancieroSIAG = "";
        BigDecimal montoComisionMensual = null;

        // I. CADA CAMPO DENTRO DE UN REGISTRO DEBER� TERMINAR, INVARIABLEMENTE, CON EL S�MBOLO @ (ARROBA).
        int lastChar = registro.getLastChar();
        if (lastChar != -1 && '@' != (char) lastChar) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" La linea debe finalizar con un caracter de arroba.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // II. VALIDAR QUE SE HAYA ENVIADO EL NUMERO DE CAMPOS REQUERIDO
        if (registro.getCampoExtra().length() > 0) {
            retorno.appendErrores("Error en la linea:");
            retorno.appendErrores(String.valueOf(numLinea));
            retorno.appendErrores(" El numero de campos es incorrecto.\n");
            retorno.setErrorLinea(true);
            registro.setHayError(true);
            return;
        }

        // III. REALIZAR VALIDACIONES POR CAMPO.
        boolean campoValido = false;
        String valorCampo = null;

        // No. 	CAMPO                                       	      	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................

        //  1. 	Clave del Intermediario Financiero SIAG					NUMBER			5		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de identificaci�n del intermediario
        campoValido = true;
        valorCampo = registro.getClaveIntermediarioFinancieroSIAG().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno, 5);
        if (campoValido)
            campoValido =
                validaIntermediario(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", ifSiag, retorno);
        if (campoValido)
            claveIntermediarioFinancieroSIAG = valorCampo;
        if (!campoValido)
            registro.setHayError(true);

        //  2. 	Moneda																CHAR				7		OBLIGATORIO
        //    	OBSERVACIONES: Moneda de origen
        campoValido = true;
        valorCampo = registro.getMoneda().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Moneda", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Moneda", retorno, 7);
        if (campoValido)
            campoValido = validaMoneda(numLinea, valorCampo, retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  3. 	Monto Comisi�n Mensual											NUMBER		17,2		OBLIGATORIO
        //    	OBSERVACIONES: Monto pagado por concepto de Comisi�n Mensual
        campoValido = true;
        valorCampo = registro.getMontoComisionMensual().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Monto Comisi�n Mensual", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Monto Comisi�n Mensual", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Monto Comisi�n Mensual", retorno);
        if (!campoValido) {
            registro.setHayError(true);
        } else {
            montoComisionMensual = new BigDecimal(valorCampo);
        }

        //  4. 	Monto Comisi�n Aniversario										NUMBER		17,2		OBLIGATORIO
        //    	OBSERVACIONES: Monto pagado por concepto de Comisi�n Aniversario
        campoValido = true;
        valorCampo = registro.getMontoComisionAniversario().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Monto Comisi�n Aniversario", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Monto Comisi�n Aniversario", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Monto Comisi�n Aniversario", retorno);

        //  5. 	Monto Comisi�n Subasta											NUMBER		17,2		OBLIGATORIO
        //    	OBSERVACIONES: Monto pagado por concepto de Comisi�n Subasta
        campoValido = true;
        valorCampo = registro.getMontoComisionSubasta().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Monto Comisi�n Subasta", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Monto Comisi�n Subasta", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Monto Comisi�n Subasta", retorno);

        //  6. 	Monto Comisi�n Extempor�nea									NUMBER		17,2		OBLIGATORIO
        //    	OBSERVACIONES: Monto pagado por concepto de Comisi�n Extempor�nea
        campoValido = true;
        valorCampo = registro.getMontoComisionExtemporanea().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Monto Comisi�n Extempor�nea", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Monto Comisi�n Extempor�nea", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotantePV2(numLinea, valorCampo, "Monto Comisi�n Extempor�nea", retorno);

        //  7. 	Mes a conciliar													NUMBER			6		OBLIGATORIO
        //    	OBSERVACIONES: Fecha del mes que se est�n conciliando, formato texto "aaaamm"

        campoValido = true;
        valorCampo = registro.getMesConciliar().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Mes a Conciliar", retorno, 6);
        if (campoValido)
            campoValido = validaAnioMes(numLinea, valorCampo, "Mes a Conciliar", retorno);
        if (campoValido)
            campoValido = validaMesConciliar(numLinea, valorCampo, "Mes a Conciliar", claveMesConciliar, retorno);
        if (!campoValido)
            registro.setHayError(true);

        // ACTUALIZAR ESTADISTICAS DE LA CARGA
        // ......................................................................................................
        if (registro.getHayError()) {

            retorno.incNumRegErr();
            if (montoComisionMensual != null)
                retorno.addSumRegErr(montoComisionMensual);

        } else {

            // AGREGAR CLAVES DE INTEMEDIARIO FINANCIERO SEGUN CORRESPONDA
            if (retorno.getNumRegOk() == 0) {

                retorno.appendCorrectos("Claves de la garant�a:\n");
                retorno.appendCorrectos(claveIntermediarioFinancieroSIAG);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() < 1000) {

                retorno.appendCorrectos(claveIntermediarioFinancieroSIAG);
                retorno.appendCorrectos("\n");

            } else if (retorno.getNumRegOk() == 1000) {

                retorno.appendCorrectos("S�lo se muestran los primeros 1000 registros.\n");

            }

            // ACTUALIZAR CONTEO DE REGISTROS
            retorno.incNumRegOk();
            if (montoComisionMensual != null)
                retorno.addSumRegOk(montoComisionMensual);

        }

    }

    /**
     *
     * Valida las Cifras de Control del Portafolio IF de Comisiones Cobradas correspondiente
     * al M�dulo de Conciliaci�n Autom�tica.
     *
     * El m�todo no regresa ningun valor pero se modifica el estado del objeto
     * <tt>ResultadosGar</tt>, el cual es un atributo de la clase <tt>FileValidator</tt>.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @param fileValidator  Objeto de tipo ComisionesCobradasConciliacionAutomaticaValidator, con el contenido de las cifras de control y las
     *                       calculadas a partir del archivo.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 07:13:42 p.m.
     *
     */
    public void validarCifrasControlPortafolioIfComisionesCobradas(ComisionesCobradasConciliacionAutomaticaValidator fileValidator) {

        log.info("validarCifrasControlPortafolioIfComisionesCobradas(E)");

        ResultadosGar retorno = fileValidator.getResultadosGar();

        try {

            int totalRegistros = fileValidator.getTotalRegistros(); // Cifras de Control
            int totalRegistrosArchivo = retorno.getNumRegOk() + retorno.getNumRegErr();

            if (totalRegistrosArchivo != totalRegistros) {
                log.debug("El numero de registros capturado no corresponde al contenido en el archivo.\n");
                retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
            }

            /*

			// Nota: Se comenta esta seccion debido a la perdida de precision para numeros muy grandes
			// que tiene metodo Comunes.formatoDecimal

			BigDecimal sumatoriaMontosSinError = retorno.getValorSumRegOk();
			BigDecimal sumatoriaMontosConError = retorno.getValorSumRegErr();

			BigDecimal sumatoriaMontos			  = fileValidator.getSumatoriaMontos(); // Cifras de Control
			BigDecimal sumatoriaMontosArchivo  = sumatoriaMontosSinError.add(sumatoriaMontosConError);

			if( sumatoriaMontosArchivo.compareTo(sumatoriaMontos) != 0 ){
				log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo.toString() );
				log.debug("La cifra de control capturada = " + sumatoriaMontos.toString()        );
				retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
			}

			*/

            double sumatoriaMontosSinError = retorno.getSumRegOk();
            double sumatoriaMontosConError = retorno.getSumRegErr();

            double sumatoriaMontos = fileValidator.getSumatoriaMontos().doubleValue(); // Cifras de Control
            double sumatoriaMontosArchivo = sumatoriaMontosSinError + sumatoriaMontosConError;

            if (sumatoriaMontosArchivo != sumatoriaMontos) {
                log.debug("La sumatoria de los montos    = " + sumatoriaMontosArchivo);
                log.debug("La cifra de control capturada = " + sumatoriaMontos);
                retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
            }

        } catch (Exception e) {

            log.error("validarCifrasControlPortafolioIfComisionesCobradas(Exception)");
            log.error("validarCifrasControlPortafolioIfComisionesCobradas.fileValidator = <" + fileValidator + ">");
            log.error("validarCifrasControlPortafolioIfComisionesCobradas.retorno       = <" + retorno + ">");
            e.printStackTrace();

            retorno.appendCifras("Ocurri� un error inesperado al validar las cifras de control: ");
            retorno.appendCifras(e.getMessage());
            retorno.appendCifras("\n");

        }

        log.info("validarCifrasControlPortafolioIfComisionesCobradas(S)");

    }

    /**
     *
     * Genera una lista con los �ltimos meses conciliados, cuyo n�mero se indica en el par�metro:
     * <tt>numeroMesesConciliados</tt>. Cada uno de los elementos de la lista corresponde es un
     * <tt>HashMap</tt> con los siguientes valores: "clave" (cuyo valor tiene el formato: "AAAAMM")
     * y "descripcion" ( con valor describiendo el nombre del mes, seguido del numero del a�o ).
     *
     * @throws AppException
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
     *
     * @param numeroMesesConciliados  <tt>int</tt> con el n�mero de los �ltimos meses conciliados.
     *
     * @return <tt>List</tt> con la lista de los �ltimos meses conciliados.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 05/11/2013 01:26:10 p.m.
     *
     */
    public List getCatalogoMesesConciliados(int numeroMesesConciliados) throws AppException {

        log.info("getCatalogoMesesConciliados(E)");

        List listaMesesConciliados = new ArrayList();

        try {

            // Variables Auxiliares

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            Calendar aux = dateFormat.getCalendar();

            // Determinar Fecha Actual

            String fechaActual = Fecha.getFechaActual();

            // Determinar el Primer D�a del Mes Anterior al mes actual

            dateFormat.parse(fechaActual);
            Calendar mesAnterior01 = new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), 1);

            String claveMes = null;
            String nombreMes = null;
            for (int i = 0; i < numeroMesesConciliados; i++) {

                // Obtener Fecha del Mes Anterior
                mesAnterior01.add(Calendar.MONTH, -1);

                int numeroMes = mesAnterior01.get(Calendar.MONTH) + 1;
                int anioMes = mesAnterior01.get(Calendar.YEAR);

                // Determinar clave del mes
                claveMes = String.valueOf(anioMes) + (numeroMes < 10 ? "0" : "") + String.valueOf(numeroMes);

                // Determinar nombre del mes
                switch (numeroMes) {
                case 1:
                    nombreMes = "Enero " + String.valueOf(anioMes);
                    break;
                case 2:
                    nombreMes = "Febrero " + String.valueOf(anioMes);
                    break;
                case 3:
                    nombreMes = "Marzo " + String.valueOf(anioMes);
                    break;
                case 4:
                    nombreMes = "Abril " + String.valueOf(anioMes);
                    break;
                case 5:
                    nombreMes = "Mayo " + String.valueOf(anioMes);
                    break;
                case 6:
                    nombreMes = "Junio " + String.valueOf(anioMes);
                    break;
                case 7:
                    nombreMes = "Julio " + String.valueOf(anioMes);
                    break;
                case 8:
                    nombreMes = "Agosto " + String.valueOf(anioMes);
                    break;
                case 9:
                    nombreMes = "Septiembre " + String.valueOf(anioMes);
                    break;
                case 10:
                    nombreMes = "Octubre " + String.valueOf(anioMes);
                    break;
                case 11:
                    nombreMes = "Noviembre " + String.valueOf(anioMes);
                    break;
                case 12:
                    nombreMes = "Diciembre " + String.valueOf(anioMes);
                    break;
                }

                // Preparar resultado
                HashMap mesConciliado = new HashMap();
                mesConciliado.put("clave", claveMes);
                mesConciliado.put("descripcion", nombreMes);

                // Agregar Mes Conciliado a la lista
                listaMesesConciliados.add(mesConciliado);

            }

        } catch (Exception e) {

            log.error("getCatalogoMesesConciliados(Exception)");
            log.error("getCatalogoMesesConciliados.numeroMesesConciliados = <" + numeroMesesConciliados + ">");
            e.printStackTrace();

            throw new AppException("No se pudo obtener la lista de los meses conciliados: " + e.getMessage());

        } finally {

            log.info("getCatalogoMesesConciliados(S)");

        }

        return listaMesesConciliados;

    }

    /**
     *
     * Devuelve la lista de C&eacute;dulas Conciliadas Autom&aacute;ticamente para un IF en espec&iacute;fico.
     *
     * Este m�todo se ocupa en la siguiente pantalla:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
     *
     * @throws AppException
     *
     *	@param claveMesConciliado <tt>String</tt> con el n&uacute;mero del Mes Conciliado. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param claveMoneda <tt>String</tt> con la Clave de la Moneda.
     * @param clavesSituacionConciliacion <tt>String</tt> con las claves de situacion conciliacion separadas por comas.
     * @param numeroRangoMeses <tt>int</tt> con el n�mero de los �ltimos meses conciliados a consultar.
     *
     * @return <tt>List</tt> con las C&eacute;dulas Conciliadas Autom&aacute;ticamente.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 05/11/2013 06:16:14 p.m.
     *
     */
    public List getListaDetalleCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                 String claveIfSiag, String claveMoneda,
                                                                 String clavesSituacionConciliacion,
                                                                 int numeroRangoMeses) throws AppException {

        log.info("getListaDetalleCedulasConciliadasAutomaticamente(E)");

        List registros = new ArrayList();

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Consultar el mes Actual
            int mesActual = 0;
            query.setLength(0);
            query.append("SELECT TO_CHAR(sysdate,'YYYYMM') AS MES_ACTUAL FROM DUAL");

            ps = con.queryPrecompilado(query.toString());
            rs = ps.executeQuery();

            if (rs.next()) {
                mesActual = rs.getInt("MES_ACTUAL");
            }
            rs.close();
            ps.close();

            // Consultar mes inicial de las cedulas a conciliar
            int mesInicial = 0;
            query.setLength(0);
            query.append("SELECT TO_CHAR(ADD_MONTHS(sysdate,?),'YYYYMM') AS MES_INICIAL FROM DUAL");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, -numeroRangoMeses);
            rs = ps.executeQuery();

            if (rs.next()) {
                mesInicial = rs.getInt("MES_INICIAL");
            }
            rs.close();
            ps.close();

            // Preparar arreglo de variables bind con las situaciones de conciliacion
            String[] situacionesConciliacion = clavesSituacionConciliacion.split(",", -1);

            StringBuffer variablesBind = new StringBuffer();
            for (int i = 0; i < situacionesConciliacion.length; i++) {
                if (i > 0)
                    variablesBind.append(",");
                variablesBind.append("?");
            }

            // Determinar si es especifico la clave del mes a conciliar
            boolean existeClaveMesConciliado =
                claveMesConciliado == null || claveMesConciliado.matches("\\s*") ? false : true;

            // Determinar si es especifico la clave de la moneda cuyos registros seran consultados
            boolean existeClaveMoneda = claveMoneda == null || claveMoneda.matches("\\s*") ? false : true;

            // Consultar registros
            query.setLength(0);
            query.append("SELECT                                                                        " +
                         "   CONC.IC_MES_CONCILIAR                        AS IC_MES_CONCILIAR,          " +
                         "   CONC.IC_FISO                                 AS IC_FISO,                   " +
                         "   CONC.IC_IF_SIAG                              AS IC_IF_SIAG,                " +
                         "   CONC.IC_MONEDA                               AS IC_MONEDA,                 " +
                         "   MONEDA.CD_NOMBRE                             AS MONEDA,	                   " +
                         "   TO_CHAR(CONC.DF_FECHA_DESCARGA,'DD/MM/YYYY') AS DF_FECHA_DESCARGA,         " +
                         "   CONC.CG_USUARIO_DESCARGA                     AS CG_USUARIO_DESCARGA,       " +
                         "   CASE WHEN CONC.BI_ARCH_CEDULA_CONCILIACIONES IS NOT NULL THEN              " +
                         "    'true'                                                                    " +
                         "   ELSE                                                                       " +
                         "    'false'                                                                   " +
                         "   END                                          AS EXISTE_CEDULA_CONCILIACION " +
                         "FROM                                                                          " +
                         "   GTI_CONC_CEDULA CONC,                                                      " +
                         "   COMCAT_MONEDA   MONEDA,                                                    " +
                         "   (                                                                          " +
                         "      SELECT                                                                  " +
                         "        PORT.IC_MES_CONCILIAR,                                                " +
                         "        PORT.IC_FISO,                                                         " +
                         "        PORT.IC_IF_SIAG                                                       " +
                         "      FROM                                                                    " +
                         "        GTI_CONC_ARCHIVO_PORTAFOLIO PORT                                      " +
                         "      WHERE                                                                   " +
                         "        PORT.IC_MES_CONCILIAR          <  ? AND                               " +
                         "        PORT.IC_MES_CONCILIAR          >= ? AND                               " +
                         "        PORT.IC_FISO                   =  ? AND                               " +
                         "        PORT.IC_IF_SIAG                =  ? AND                               " +
                         "        PORT.IC_SITUACION_CONCILIACION IN ( ");
            query.append(variablesBind);
            query.append("        )                                                                     " +
                         "      GROUP BY                                                                " +
                         "        PORT.IC_MES_CONCILIAR,                                                " +
                         "        PORT.IC_FISO,                                                         " +
                         "        PORT.IC_IF_SIAG                                                       " +
                         "   ) PORTAFOLIO                                                               " +
                         "WHERE                                                                         " +
                         "	 CONC.IC_MONEDA        = MONEDA.IC_MONEDA            AND                    " +
                         "   CONC.IC_MES_CONCILIAR = PORTAFOLIO.IC_MES_CONCILIAR AND                    " +
                         "   CONC.IC_FISO          = PORTAFOLIO.IC_FISO          AND                    " +
                         "   CONC.IC_IF_SIAG       = PORTAFOLIO.IC_IF_SIAG       AND                    " +
                         "	 CONC.IC_FISO          = ? AND                                              " +
                         "	 CONC.IC_IF_SIAG       = ?                                                  ");
            if (existeClaveMesConciliado) {
                query.append("	 AND CONC.IC_MES_CONCILIAR = ? ");
            }
            if (existeClaveMoneda) {
                query.append("	 AND CONC.IC_MONEDA        = ? ");
            }
            query.append("ORDER BY                                                                      " +
                         "   CONC.IC_MES_CONCILIAR                                                      " +
                         "DESC                                                                          ");

            ps = con.queryPrecompilado(query.toString());
            int idx = 1;
            ps.setInt(idx++, mesActual);
            ps.setInt(idx++, mesInicial);
            ps.setLong(idx++, Long.parseLong(claveFiso));
            ps.setInt(idx++, Integer.parseInt(claveIfSiag));
            for (int i = 0; i < situacionesConciliacion.length; i++) {
                ps.setInt(idx++, Integer.parseInt(situacionesConciliacion[i]));
            }
            ps.setLong(idx++, Long.parseLong(claveFiso));
            ps.setInt(idx++, Integer.parseInt(claveIfSiag));
            if (existeClaveMesConciliado) {
                ps.setInt(idx++, Integer.parseInt(claveMesConciliado));
            }
            if (existeClaveMoneda) {
                ps.setInt(idx++, Integer.parseInt(claveMoneda));
            }
            rs = ps.executeQuery();
            log.info(query);

            HashMap registro = null;
            while (rs.next()) {

                String icMesConciliar =
                    (rs.getString("IC_MES_CONCILIAR") == null) ? "" : rs.getString("IC_MES_CONCILIAR");
                String icFiso = (rs.getString("IC_FISO") == null) ? "" : rs.getString("IC_FISO");
                String icIfSiag = (rs.getString("IC_IF_SIAG") == null) ? "" : rs.getString("IC_IF_SIAG");
                String icMoneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
                String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
                String dfFechaDescarga =
                    (rs.getString("DF_FECHA_DESCARGA") == null) ? "" : rs.getString("DF_FECHA_DESCARGA");
                String cgUsuarioDescarga =
                    (rs.getString("CG_USUARIO_DESCARGA") == null) ? "" : rs.getString("CG_USUARIO_DESCARGA");
                String existeCedulaConciliacion =
                    (rs.getString("EXISTE_CEDULA_CONCILIACION") == null) ? "false" :
                    rs.getString("EXISTE_CEDULA_CONCILIACION");

                registro = new HashMap();
                registro.put("IC_MES_CONCILIAR", icMesConciliar);
                registro.put("IC_FISO", icFiso);
                registro.put("IC_IF_SIAG", icIfSiag);
                registro.put("IC_MONEDA", icMoneda);
                registro.put("MONEDA", moneda);
                registro.put("DF_FECHA_DESCARGA", dfFechaDescarga);
                registro.put("CG_USUARIO_DESCARGA", cgUsuarioDescarga);
                registro.put("EXISTE_CEDULA_CONCILIACION",
                             "true".equals(existeCedulaConciliacion) ? new Boolean(true) : new Boolean(false));
                registros.add(registro);

            }

        } catch (Exception e) {

            log.error("getListaDetalleCedulasConciliadasAutomaticamente(Exception)");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.claveMesConciliado 	       = <" +
                      claveMesConciliado + ">");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.claveFiso 				       = <" + claveFiso +
                      ">");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.claveIfSiag        	       = <" + claveIfSiag +
                      ">");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.claveMoneda        	       = <" + claveMoneda +
                      ">");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.clavesSituacionConciliacion = <" +
                      clavesSituacionConciliacion + ">");
            log.error("getListaDetalleCedulasConciliadasAutomaticamente.numeroRangoMeses            = <" +
                      numeroRangoMeses + ">");
            e.printStackTrace();

            throw new AppException("Fall� la consulta de C�dulas Conciliadas Autom�ticamente: " + e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getListaDetalleCedulasConciliadasAutomaticamente(S)");

        }

        return registros;

    }

    /**
     *
     * Determina si la c�dula de conciliaci�n a extraer cumple con las condiciones para
     * que su descarga pueda ser firmada digitalmente.
     *
     * Este m�todo se ocupa en la siguiente pantalla:
     *		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
     *
     * @throws AppException
     *
     *	@param claveMesConciliar <tt>String</tt> con el n�mero del Mes Conciliado. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     *
     * @return <tt>true</tt> en caso de que se requiera la firma y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 06/11/2013 04:31:46 p.m.
     *
     */
    public boolean requiereFirmarDescargaCedulasConciliadasAutomaticamente(String claveMesConciliar, String claveFiso,
                                                                           String claveIfSiag) throws AppException {

        log.info("requiereFirmarDescargaCedulasConciliadasAutomaticamente(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        boolean requiereFirma = false;

        try {

            // VALIDAR QUE AUN NO SE HAYA DESCARGADO LA CEDULA CONCILIADA PROPORCIONADA POR NAFIN.

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Determinar si la cedula de conciliaci�n est� sin extraer
            query.append("SELECT                                                     " +
                         "   DECODE( COUNT(1),0,'false','true' ) AS  FIRMAR_DESCARGA " +
                         "FROM                                                       " +
                         "  GTI_CONC_ARCHIVO_PORTAFOLIO                              " +
                         "WHERE                                                      " +
                         "  IC_MES_CONCILIAR          = ? AND                        " +
                         "  IC_FISO                   = ? AND                        " +
                         "  IC_IF_SIAG                = ? AND                        " +
                         "  IC_SITUACION_CONCILIACION = 6                            " + //  CLAVE 6:	GENERACION DE LA CEDULA DE CONCILIACION
                         "GROUP BY                                                   " +
                         "  IC_MES_CONCILIAR,                                        " +
                         "  IC_FISO,                                                 " +
                         "  IC_IF_SIAG                                               ");

            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveMesConciliar));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIfSiag));

            rs = ps.executeQuery();
            boolean firmarDescarga = false;
            if (rs.next()) {
                firmarDescarga = "true".equals(rs.getString("FIRMAR_DESCARGA")) ? true : false;
            }

            if (firmarDescarga) {
                requiereFirma = true;
            }

        } catch (Exception e) {

            log.error("requiereFirmarDescargaCedulasConciliadasAutomaticamente(Exception)");
            log.error("requiereFirmarDescargaCedulasConciliadasAutomaticamente.claveMesConciliar     = <" +
                      claveMesConciliar + ">");
            log.error("requiereFirmarDescargaCedulasConciliadasAutomaticamente.claveFiso             = <" + claveFiso +
                      ">");
            log.error("requiereFirmarDescargaCedulasConciliadasAutomaticamente.claveIfSiag           = <" +
                      claveIfSiag + ">");
            e.printStackTrace();

            throw new AppException("Fall� la consulta para determinar si la descarga de la cedula(s) conciliada(s) requiere firmado digital: " +
                                   e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("requiereFirmarDescargaCedulasConciliadasAutomaticamente(E)");

        }

        return requiereFirma;

    }

    /**
     * Registra l firmado digital de la descarga de la(s) C&eacute;dula(s) Conciliada(s) por parte del IF, adem&aacute;s actualiza el
     * estatus de la situaci&oacute;n transferencia a 7: "FIRMA DE LA CEDULA POR PARTE DEL IF".
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
     *
     * @throws AppException
     *
     *	@param claveMesConciliado <tt>String</tt> con el n&uacute;mero del Mes Conciliado. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     * @param acuseFirmaPki <tt>String</tt> con el numero resultante del firmado digital de la descarga de la(s) C�dula(s) Conciliada(s).
     * @param loginUsuario <tt>String</tt> con el login del usuario que firm&oacute; la extracci&oacute;n.
     *
     * @return <tt>true</tt> en caso de que la operaci&oacute;n sea exitosa y <tt>false</tt> en caso contrario.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 06/11/2013 06:32:47 p.m.
     *
     */
    public boolean registraFirmaDescargaCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                          String claveIfSiag, String acuseFirmaPki,
                                                                          String loginUsuario) throws AppException {

        log.info("registraFirmaDescargaCedulasConciliadasAutomaticamente(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        StringBuffer sentence = new StringBuffer();

        boolean exito = true;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            sentence.append("UPDATE                                 " + "  GTI_CONC_ARCHIVO_PORTAFOLIO          " +
                            "SET                                    " + "  IC_SITUACION_CONCILIACION = 7,       " + // CLAVE 7: "FIRMA DE LA CEDULA POR PARTE DEL IF".
                            "  CG_USUARIO_ACTUALIZA      = ?,       " + "  DF_FECHA_ACTUALIZA        = SYSDATE  " +
                            "WHERE                                  " + "  IC_MES_CONCILIAR          = ? AND    " +
                            "  IC_FISO                   = ? AND    " + "  IC_IF_SIAG                = ?        ");

            ps = con.queryPrecompilado(sentence.toString());
            ps.setString(1, loginUsuario);
            ps.setInt(2, Integer.parseInt(claveMesConciliado));
            ps.setLong(3, Long.parseLong(claveFiso));
            ps.setInt(4, Integer.parseInt(claveIfSiag));
            ps.executeUpdate();
            ps.close();

            sentence.setLength(0);
            sentence.append("UPDATE                                 " + "  GTI_CONC_CEDULA                      " +
                            "SET                                    " + "  IG_PKI_CEDULA             = ?,       " +
                            "  CG_USUARIO_ACTUALIZA      = ?,       " + "  DF_FECHA_ACTUALIZA        = SYSDATE, " +
                            "  CG_USUARIO_DESCARGA       = ?,       " + "  DF_FECHA_DESCARGA         = SYSDATE  " +
                            "WHERE                                  " + "  IC_MES_CONCILIAR          = ? AND    " +
                            "  IC_FISO                   = ? AND    " + "  IC_IF_SIAG                = ?        ");

            ps = con.queryPrecompilado(sentence.toString());
            ps.setBigDecimal(1, new BigDecimal(acuseFirmaPki));
            ps.setString(2, loginUsuario);
            ps.setString(3, loginUsuario);
            ps.setInt(4, Integer.parseInt(claveMesConciliado));
            ps.setLong(5, Long.parseLong(claveFiso));
            ps.setInt(6, Integer.parseInt(claveIfSiag));
            ps.executeUpdate();

        } catch (Exception e) {

            exito = false;

            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente(Exception)");
            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente.claveMesConciliado    = <" +
                      claveMesConciliado + ">");
            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente.claveFiso             = <" + claveFiso +
                      ">");
            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente.claveIfSiag           = <" + claveIfSiag +
                      ">");
            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente.acuseFirmaPki         = <" +
                      acuseFirmaPki + ">");
            log.error("registraFirmaDescargaCedulasConciliadasAutomaticamente.loginUsuario          = <" +
                      loginUsuario + ">");

            e.printStackTrace();

            throw new AppException("No se pudo guardar registro de la firma digital: " + e.getMessage());

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

            log.info("registraFirmaDescargaCedulasConciliadasAutomaticamente(S)");
        }

        return exito;

    }

    /**
     * Devuelve la fecha en la que se realiz&oacute; la firma digital de la descarga de la(s) c&eacute;dula(s) conciliada(s)
     * autom&aacute;ticamente, tambi&eacute;n devuelve el login del usuario que realiz&oacute; la descarga.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
     *
     * @throws AppException
     *
     *	@param claveMesConciliado <tt>String</tt> con el n&uacute;mero del Mes Conciliado. Formato AAAAMM.
     * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
     * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
     *
     * @return <tt>HashMap</tt> con el detalle de la consulta.
     *
     * @author jshernandez
     * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 06/11/2013 07:07:05 p.m.
     *
     */
    public HashMap getDatosFirmaDescargaCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                          String claveIfSiag) throws AppException {

        log.info("getDatosFirmaDescargaCedulasConciliadasAutomaticamente(E)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer query = new StringBuffer();

        HashMap datos = new HashMap();
        String usuarioDescarga = "";
        String fechaDescarga = "";

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Preparar Query
            query.append("SELECT                                                            " +
                         "   TO_CHAR(DF_FECHA_DESCARGA, 'DD/MM/YYYY') AS DF_FECHA_DESCARGA, " +
                         "   CG_USUARIO_DESCARGA                                            " +
                         "FROM                                                              " +
                         "   GTI_CONC_CEDULA                                                " +
                         "WHERE                                                             " +
                         "  IC_MES_CONCILIAR     = ? AND                                    " +
                         "  IC_FISO              = ? AND                                    " +
                         "  IC_IF_SIAG           = ?                                        ");

            // Realizar la consulta
            ps = con.queryPrecompilado(query.toString());
            ps.setInt(1, Integer.parseInt(claveMesConciliado));
            ps.setLong(2, Long.parseLong(claveFiso));
            ps.setInt(3, Integer.parseInt(claveIfSiag));

            rs = ps.executeQuery();
            if (rs.next()) {
                fechaDescarga = (rs.getString("DF_FECHA_DESCARGA") == null) ? "" : rs.getString("DF_FECHA_DESCARGA");
                usuarioDescarga =
                    (rs.getString("CG_USUARIO_DESCARGA") == null) ? "" : rs.getString("CG_USUARIO_DESCARGA");
            }

            datos.put("fechaDescarga", fechaDescarga);
            datos.put("usuarioDescarga", usuarioDescarga);

        } catch (Exception e) {

            log.error("getDatosFirmaDescargaCedulasConciliadasAutomaticamente(Exception)");
            log.error("getDatosFirmaDescargaCedulasConciliadasAutomaticamente.claveMesConciliado    = <" +
                      claveMesConciliado + ">");
            log.error("getDatosFirmaDescargaCedulasConciliadasAutomaticamente.claveFiso             = <" + claveFiso +
                      ">");
            log.error("getDatosFirmaDescargaCedulasConciliadasAutomaticamente.claveIfSiag           = <" + claveIfSiag +
                      ">");

            e.printStackTrace();

            throw new AppException("Fall� la consulta del detalle de la extracci�n del portafolio nafin: " +
                                   e.getMessage());

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getDatosFirmaDescargaCedulasConciliadasAutomaticamente(S)");

        }

        return datos;

    }

    /**
     * Validaciones de alta de Calificaci&oacute;n de Cartera usando la Nueva Metodolog&iacute;a ( definida en el F036-2013 ).
     * Son las validaciones de los campos que componen una l&iacute;nea en el archivo de carga.
     *
     * El m&eacute;todo no regresa ning&uacute;n valor pero se modifica el estado del objeto ResultadosGar, el cual llevar&aacute; el
     * registro de los errores de validaci&oacute;n.
     *
     * Este m&eacute;todo se ocupa en la siguiente pantalla:
     *		PANTALLA: ADMIN IF GARANT - PROGRAMA DE GARANTIA AUTOMATICA - SERVICIOS DE TRANSACCION - CARGA DE CALIFICACION CARTERA - CARGA DE ARCHIVO
     *
     * @param ifSiag <tt>int</tt> con la Clave SIAG del Intermediario Financiero.
     * @param numLinea <tt>int</tt> con el N&uacute;mero de l&iacute;nea que se est&aacute; validando.
     * @param strLinea <tt>String</tt> con el contenido del registro a validar.
     * @param retorno Objeto de <tt>ResultadosGar</tt>, que contiene los detalles de la validaci&oacute;n.
     * @param anioTrimestreControl <tt>String</tt> con el n&uacute;mero del a&ntilde;o para el cual se realiza la carga de la calificaci&oacute;n de cartera.
     * @param trimestreControl <tt>String</tt> con el n&uacute;mero del trimestre para el cual se realiza la carga de la calificaci&oacute;n de cartera.
     *
     * @author jshernandez
     * @since F036 - 2013 -- GARANTIAS - Nueva metodolog&iacute;a calificaci&oacute;n; 13/11/2013 01:46:24 p.m.
     *
     */
    private void validarAltaCalificacionCartera(int ifSiag, int numLinea, String strLinea, ResultadosGar retorno,
                                                String anioTrimestreControl, String trimestreControl) {

        VectorTokenizer vt = new VectorTokenizer(strLinea, "@");
        List layout = vt.getValuesVector();

        // I. CADA CAMPO DENTRO DE UN REGISTRO DEBER� TERMINAR, INVARIABLEMENTE, CON EL S�MBOLO @ (ARROBA).

        if (strLinea.charAt(strLinea.length() - 1) != '@') {
            retorno.appendErrores("Error en la linea:" + numLinea +
                                  " La linea debe finalizar con un caracter de arroba\n");
            retorno.setErrorLinea(true);
            return;
        }

        // II. VALIDAR QUE SE HAYA ENVIADO EL NUMERO DE CAMPOS REQUERIDO

        int numCamposRequeridos = 11;
        //Dado que en el layout SIEMPRE ponen una arroba al final de la linea,
        //el cual es detectado como un campo mas por la clase VectorTokenizer,
        //se le suma 1
        if (layout.size() != numCamposRequeridos + 1) {
            retorno.appendErrores("Error en la linea:" + numLinea + " El numero de campos es incorrecto. Esperados(" +
                                  numCamposRequeridos + ") Recibidos(" + (layout.size() - 1) + ") \n");
            //layout.size()-1 se les resta uno, debido a que como en el layout agregan una @ al final, VectorTokenizer cuenta un campo de m�s.
            retorno.setErrorLinea(true);
            return;
        }


        // III. REALIZAR VALIDACIONES POR CAMPO.

        boolean campoValido = false;
        String valorCampo = null;
        int longitudClaveFinanciamiento =
            20; //(reglaProgramaSHFCreditoHipotecario == ResultadosGar.OPERA_SHF_CRED_HIPOTECARIO)?30:20;
        // A indicaci�n de lmrojas se sobreescribe la longitud de la clave de financiamiento a 20 caracteres.

        // No. 	CAMPO                                       	      	TIPO DATO  LONG.MAX	OBLIGATORIO
        //........................................................................................................

        //  1. 	Clave del Intermediario Financiero SIAG					NUMBER			5		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de identificaci�n del Intermediario
        campoValido = true;
        valorCampo = (String) layout.get(0);
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", retorno,
                                CLAVE_DEL_PROGRAMA_LENGTH); // CLAVE_DEL_PROGRAMA_LENGTH = 5
        if (campoValido)
            campoValido =
                validaIntermediario(numLinea, valorCampo, "Clave del Intermediario Financiero SIAG", ifSiag, retorno);

        //  2. 	A�o Trimestre de Calificaci�n									NUMBER			4		OBLIGATORIO
        //    	OBSERVACIONES: Ninguna
        campoValido = true;
        valorCampo = (String) layout.get(1);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "A�o Trimestre de Calificaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "A�o Trimestre de Calificaci�n", retorno, 4);
        if (campoValido)
            campoValido = validaEntero(numLinea, valorCampo, "A�o Trimestre de Calificaci�n", retorno);
        if (campoValido)
            validaAnioTrim(numLinea, valorCampo, anioTrimestreControl, "A�o Trimestre de Calificaci�n", retorno);

        //  3. 	Trimestre de Calificaci�n										NUMBER			1		OBLIGATORIO
        //    	OBSERVACIONES: Ninguna
        campoValido = true;
        valorCampo = (String) layout.get(2);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Trimestre de Calificaci�n", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Trimestre de Calificaci�n", retorno, 1);
        if (campoValido)
            campoValido = validaEntero(numLinea, valorCampo, "Trimestre de Calificaci�n", retorno);
        if (campoValido)
            validaAnioTrim(numLinea, valorCampo, trimestreControl, "Trimestre de Calificaci�n", retorno);

        //  4. 	Clave del Cr�dito													  CHAR		  20		OBLIGATORIO
        //    	OBSERVACIONES: N�mero de pr�stamo
        campoValido = true;
        valorCampo = (String) layout.get(3);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Clave del Cr�dito", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "Clave del Cr�dito", retorno, longitudClaveFinanciamiento);

        //  5. 	RFC del Acreditado												  CHAR		  13		OBLIGATORIO
        //    	OBSERVACIONES: RFC completo del cliente, sin espacios ni guiones
        campoValido = true;
        valorCampo = (String) layout.get(4);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "RFC del Acreditado", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "RFC del Acreditado", retorno, 13);
        if (campoValido)
            campoValido = validaRFC_v2(numLinea, valorCampo, "RFC del Acreditado", retorno);

        //  6. 	Tipo de Cartera													NUMBER			3		OBLIGATORIO
        //    	OBSERVACIONES: Ninguna
        campoValido = true;
        valorCampo = (String) layout.get(5);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Tipo de Cartera", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Tipo de Cartera", retorno, 3);
        if (campoValido)
            campoValido = validaEntero(numLinea, valorCampo, "Tipo de Cartera", retorno);

        //  7. 	Probabilidad de Incumplimiento (%PI)							NUMBER		 9,6		OBLIGATORIO
        //    	OBSERVACIONES: Porcentaje redondeado a seis decimales; debe estar entre de 0 a 100
        campoValido = true;
        valorCampo = (String) layout.get(6);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Probabilidad de Incumplimiento (%PI)", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo, "Probabilidad de Incumplimiento (%PI)", retorno, 9, 6);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Probabilidad de Incumplimiento (%PI)", retorno);
        if (campoValido)
            validaRangoNumero(numLinea, valorCampo, "Probabilidad de Incumplimiento (%PI)", retorno, "0", "100");

        //  8. 	Severidad de la P�rdida (%SP)									NUMBER		 9,6		OBLIGATORIO
        //    	OBSERVACIONES: Porcentaje redondeado a seis decimales; debe estar entre de 0 a 100
        campoValido = true;
        valorCampo = (String) layout.get(7);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Severidad de la P�rdida (%SP)", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Severidad de la P�rdida (%SP)", retorno, 9, 6);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Severidad de la P�rdida (%SP)", retorno);
        if (campoValido)
            validaRangoNumero(numLinea, valorCampo, "Severidad de la P�rdida (%SP)", retorno, "0", "100");

        //  9. 	Exposici�n al Incumplimiento (EI)							NUMBER		17,2		OBLIGATORIO
        //    	OBSERVACIONES: Se redondea a dos decimales
        campoValido = true;
        valorCampo = (String) layout.get(8);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Exposici�n al Incumplimiento (EI)", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo, "Exposici�n al Incumplimiento (EI)", retorno, 17, 2);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Exposici�n al Incumplimiento (EI)", retorno);

        //  10. 	Porcentaje de Reservas (%PE)									NUMBER		 9,6		OBLIGATORIO
        //    	OBSERVACIONES: Porcentaje redondeado a seis decimales; debe estar entre de 0 a 100
        campoValido = true;
        valorCampo = (String) layout.get(9);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Porcentaje de Reservas (%PE)", retorno);
        if (campoValido)
            campoValido = validaPrecisionDecimal(numLinea, valorCampo, "Porcentaje de Reservas (%PE)", retorno, 9, 6);
        if (campoValido)
            campoValido = validaFlotanteV2(numLinea, valorCampo, "Porcentaje de Reservas (%PE)", retorno);
        if (campoValido)
            validaRangoNumero(numLinea, valorCampo, "Porcentaje de Reservas (%PE)", retorno, "0", "100");

        //  11. 	Nivel de Riesgo													  CHAR			4		OBLIGATORIO
        //    	OBSERVACIONES: Ninguna
        campoValido = true;
        valorCampo = (String) layout.get(10);
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "Nivel de Riesgo", retorno);
        if (campoValido)
            campoValido = validarLongitud(numLinea, valorCampo, "Nivel de Riesgo", retorno, 4);

    }

    /**
     *
     * Verifica, a traves de los atributos: archivoZIPContieneDirectorio y archivoZIPContieneMasDeUnArchivo, que el archivo ZIP
     * no contenga directorios ni tampoco m�s de un archivo.
     * El m�todo no regresa ning�n valor pero se modifica el estado del objeto:
     * <tt>ResultadosGar</tt>, el cual es un atributo de la clase <tt>FileValidator</tt>.
     *
     * Este m&eacute;todo se ocupa en las siguientes pantallas:
     *    PANTALLA: ADMIN IF GARANT - CONCILIACI�N AUTOM�TICA - SALDO INSOLUTO - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACI�N AUTOM�TICA - SALDO PENDIENTE POR RECUPERAR - CARGA DE ARCHIVO
     *    PANTALLA: ADMIN IF GARANT - CONCILIACI�N AUTOM�TICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
     *
     * @param fileValidator  Objeto de tipo <tt>FileValidator</tt>, con el contenido de las cifras de control y las
     *                       calculadas a partir del archivo.
     *
     * @author jshernandez
     * @since F002 - 2014 -- GARANT - Mantenimiento al Modulo de  Conciliacion; 05/02/2014 04:12:20 p.m.
     *
     */
    public void validarArchivoZIPConciliacionAutomatica(FileValidator fileValidator) {

        log.info("validarArchivoZIPConciliacionAutomatica(E)");

        ResultadosGar retorno = fileValidator.getResultadosGar();

        try {

            if (fileValidator.getArchivoZIPContieneDirectorio()) {
                log.debug("Error: El Archivo ZIP no puede contener ningun subdirectorio.");
                retorno.appendCifras("Error: El Archivo ZIP no puede contener ningun subdirectorio.\n");
            }

            if (fileValidator.getArchivoZIPContieneMasDeUnArchivo()) {
                log.debug("Error: En el Archivo ZIP, se encontro mas de un archivo.");
                retorno.appendCifras("Error: En el Archivo ZIP, se encontro mas de un archivo.\n");
            }

        } catch (Exception e) {

            log.error("validarArchivoZIPConciliacionAutomatica(Exception)");
            log.error("validarArchivoZIPConciliacionAutomatica.fileValidator = <" + fileValidator + ">");
            log.error("validarArchivoZIPConciliacionAutomatica.retorno       = <" + retorno + ">");
            e.printStackTrace();

            retorno.appendCifras("Ocurrio un error inesperado al validar propiedades del Archivo ZIP: ");
            retorno.appendCifras(e.getMessage());
            retorno.appendCifras("\n");

        }

        log.info("validarArchivoZIPConciliacionAutomatica(S)");

    }

    /**
     * Devuelve la lista de correos de usuarios NAFIN y de usuarios IF (parametrizados en <tt>GTI_CORREO_NOTIF</tt>)
     * que ser�n notificados por los siguientes procesos:
     * <br>
     *   <tt>ERRORES_SIAG</tt> (Proceso Java CorreoErroresSIAG): Se usa para notificar los registros con error
     *   que fueron enviados al SIAG.
     * <br>
     *   <tt>AUTORIZACION_IF_SIAG</tt> (Proceso Java CorreoAutorizacionIfSIAG): Se usa para notificar los registros
     *   que tienen Autorizacion IF y que fueron enviados al SIAG.
     * <br>
     * @throws Exception
     *
     * @param folio    		String con el n�mero de folio de la operaci�n String.<br>
     * @param icIfSiag 		String con la clave SIAG del IF.<br>
     * @param tipoProceso   String con el tipo de proceso que consulta la parametrizaci�n:<br>
     *								ERRORES_SIAG          Proceso Java CorreoErroresSIAG.<br>
     *								AUTORIZACION_IF_SIAG  Proceso Java CorreoAutorizacionIfSIAG.<br>
     *
     * @return List con la listas de correos a notificar, agrupados por tipo de usuario.
     *
     * @author jshernandez
     * @since  Ajuste: Actualizar Proceso CorreoErroresSIAG. 03/08/2015 11:48:37 a.m.
     *
     */
    public List getCorreosParametrizadosPorProcesoSiag(String folio, String icIfSiag,
                                                       String tipoProceso) throws Exception {

        log.info("getCorreosParametrizadosPorProcesoSiag(E)");

        AccesoDB con = new AccesoDB();
        StringBuffer qrySentencia = new StringBuffer();
        List lVarBind = new ArrayList();
        Registros registros = null;

        String direcciones = null;
        List correos = new ArrayList();

        try {

            con.conexionDB();

            qrySentencia.append("SELECT                               " + "   n.cg_mail_cuentas_nafin,          " +
                                "   n.cg_mail_cuentas_if              " + "FROM                                 " +
                                "   gti_estatus_solic s,              " + "   comrel_nafin      rel,            " +
                                "   gti_correo_notif  n               " + "WHERE                                " +
                                "       s.ic_folio             =  ?   " + "   AND s.ic_if_siag           =  ?   ");

            if ("ERRORES_SIAG".equals(tipoProceso)) {

                qrySentencia.append("   AND s.ic_correo_env        =  1                        " +
                    // 1 (REGISTROS CON ERROR)
                    "   AND s.ic_nafin_electronico =  rel.ic_nafin_electronico " +
                    "   AND s.ic_tipo_operacion    =  n.ic_tipo_operacion      " +
                    "   AND rel.ic_epo_pyme_if     =  n.ic_if                  " + "   AND n.ic_estatus_notif     in (1,3)                    " // 1 = RECHAZADA y 3 = AMBOS
                    );

            } else if ("AUTORIZACION_IF_SIAG".equals(tipoProceso)) {

                qrySentencia.append("   AND s.ic_correo_env         =  0                        " +
                    // 0 (REGISTROS SIN ERROR)
                    "   AND s.ic_tipo_operacion     =  2                        " +
                    // 2 = CARGA DE SALDOS Y PAGO COMISIONES
                    "   AND s.ic_situacion          =  4                        " + // 4 = AUTORIZACI�N IF
                    "   AND s.ic_nafin_electronico  =  rel.ic_nafin_electronico " +
                    "   AND s.ic_tipo_operacion     =  n.ic_tipo_operacion      " +
                    "   AND rel.ic_epo_pyme_if      =  n.ic_if                  " + "   AND n.ic_estatus_notif      in (2,3)                    " // 2 = AUTORIZADA IF y 3 = AMBOS
                    );

            } else {
                throw new IllegalArgumentException("El argumento tipoProceso no es v�lido.");
            }

            qrySentencia.append("ORDER BY                 " + "   n.ic_estatus_notif    " // Para dar preferencia a la parametrizaci�n
                                // espec�fica primero: 1 = RECHAZADA � 2 = AUTORIZADA IF
                                );

            lVarBind.add(folio);
            lVarBind.add(icIfSiag);

            log.debug("getCorreosParametrizadosPorProcesoSiag.FOLIO      = " + folio);
            log.debug("getCorreosParametrizadosPorProcesoSiag.IC_IF_SIAG = " + icIfSiag);

            registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
            if (registros != null && registros.next()) {

                direcciones = registros.getString("cg_mail_cuentas_nafin");
                if (!Comunes.esVacio(direcciones)) {
                    correos.add(direcciones);
                }

                direcciones = registros.getString("cg_mail_cuentas_if");
                if (!Comunes.esVacio(direcciones)) {
                    correos.add(direcciones);
                }

            }

            log.debug("getCorreosParametrizadosPorProcesoSiag.tipoProceso  = " + tipoProceso);
            log.debug("getCorreosParametrizadosPorProcesoSiag.correos      = " + correos);

        } catch (Exception e) {

            log.error("getCorreosParametrizadosPorProcesoSiag(Exception)");
            log.error("getCorreosParametrizadosPorProcesoSiag.folio        = <" + folio + ">");
            log.error("getCorreosParametrizadosPorProcesoSiag.icIfSiag     = <" + icIfSiag + ">");
            log.error("getCorreosParametrizadosPorProcesoSiag.tipoProceso  = <" + tipoProceso + ">");
            log.error("getCorreosParametrizadosPorProcesoSiag.qrySentencia = <" + qrySentencia +
                      ">"); // Para detectar problemas de compilaci�n en Strings.
            log.error("getCorreosParametrizadosPorProcesoSiag.exception:", e);

            e.printStackTrace();

            throw new Exception("Error al consultar las direcciones de correo parametrizadas por tipo de proceso.");

        } finally {

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("getCorreosParametrizadosPorProcesoSiag(S)");

        }

        return correos;

    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo
     * @throws Exception
     */
    public void sendError() throws Exception {

        log.info("send(E)");
        try {

            log.info("Obteniendo GarantiasEJB");

            // Obtener Registros
            ArrayList registros = getRegistrosConErrores();
            UtilUsr utilUsr = new UtilUsr();
            for (int i = 0; i < registros.size(); i++) {

                HashMap registro = (HashMap) registros.get(i);

                String idUsuario = (String) registro.get("ID_USUARIO");
                String folio = (String) registro.get("FOLIO");
                String numeroRegistros = (String) registro.get("NUM_REGISTROS");
                String operacion = (String) registro.get("OPERACION");
                String icIfSiag = (String) registro.get("IC_IF_SIAG");

                try {

                    // obtiene correo Parametrizado
                    List email = this.getCorreosParametrizadosPorProcesoSiag(folio, icIfSiag, "ERRORES_SIAG");
                    // En caso de que no haya ninguna direcci�n de correo parametrizada usar la que tiene registrada
                    // en el OID el usuario facultado.
                    if (email.size() == 0) {

                        log.debug("\nALERTA:");
                        log.debug(" No es posible notificar a los usuarios NAFIN que la transaccion con Numero de Folio: " +
                                  folio);
                        log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene " + numeroRegistros +
                                  ("1".equals(numeroRegistros) ? " operacion rechazada, " :
                                   " operaciones rechazadas,"));
                        log.debug(" debido a que el IF no esta parametrizado en GTI_CORREO_NOTIF.");

                        // Obtener correo del usuario
                        String emailUsuario = null;
                        try {
                            emailUsuario = utilUsr.getUsuario(idUsuario).getEmail();
                        } catch (Exception e) {
                            emailUsuario = null;
                        }

                        if (Comunes.esVacio(emailUsuario)) {
                            log.debug("\nALERTA:");
                            log.debug(" El usuario con ID: " + idUsuario +
                                      " no tiene ninguna direccion de correo registrada");
                            log.debug(" por lo que no es posible notificarle que la transaccion con Numero de Folio: " +
                                      folio);
                            log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene " + numeroRegistros +
                                      ("1".equals(numeroRegistros) ? " operacion rechazada." :
                                       " operaciones rechazadas."));
                        }

                        // Agregar direcci�n a la lista de correos.
                        if (!Comunes.esVacio(emailUsuario)) {
                            email.add(emailUsuario);
                        }

                    }

                    String emailDestinatarios = null;
                    boolean exitoEnvioCorreos = email.size() > 0 ? true : false;
                    for (int j = 0; j < email.size(); j++) {

                        emailDestinatarios = (String) email.get(j);

                        if (!Comunes.esVacio(emailDestinatarios)) {
                            try {
                                // Enviar correo de notificaci�n
                                enviarCorreoDeNotificacionError(emailDestinatarios, folio, numeroRegistros, operacion);
                            } catch (Exception e) {
                                exitoEnvioCorreos = false;
                                log.debug("\nALERTA:");
                                log.debug(" No fue posible notificar a: " + emailDestinatarios +
                                          " que la transaccion con Numero de Folio: " + folio);
                                log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene " + numeroRegistros +
                                          ("1".equals(numeroRegistros) ? " operacion rechazada." :
                                           " operaciones rechazadas."));
                                log.error("\nALERTA:" + " No fue posible notificar a: " + emailDestinatarios +
                                          " que la transaccion con Numero de Folio: " + folio + " (e IC_IF_SIAG = " +
                                          icIfSiag + ") tiene " + numeroRegistros +
                                          ("1".equals(numeroRegistros) ? " operacion rechazada." :
                                           " operaciones rechazadas."), e);
                            }
                        }

                    }

                    // Actualizar en tabla que se envio el correo con estatus 2
                    if (exitoEnvioCorreos) {
                        try {
                            setCorreoEnviadoError(folio, icIfSiag);
                        } catch (Exception e) {
                            log.debug("\nALERTA:");
                            log.debug(" No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: " +
                                      folio + " (e IC_IF_SIAG = " + icIfSiag + ").");
                            log.error("\nALERTA:" +
                                      " No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: " +
                                      folio + " (e IC_IF_SIAG = " + icIfSiag + ").", e);
                        }
                    }

                } catch (Exception e) {

                    log.debug("\nALERTA:");
                    log.debug(" No fue posible notificar que la transaccion con Numero de Folio: " + folio);
                    log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene " + numeroRegistros +
                              ("1".equals(numeroRegistros) ? " operacion rechazada." : " operaciones rechazadas."));
                    log.error("\nALERTA:" + " No fue posible notificar que la transaccion con Numero de Folio: " +
                              folio + " (e IC_IF_SIAG = " + icIfSiag + ") tiene " + numeroRegistros +
                              ("1".equals(numeroRegistros) ? " operacion rechazada." : " operaciones rechazadas."), e);

                }

            }
        } catch (Exception e) {
            log.error("send(Exception)", e);
            e.printStackTrace();
            throw e;
        } finally {
            log.info("send(S)");
        }
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo
     * @return
     * @throws Exception
     */
    private ArrayList getRegistrosConErrores() throws Exception {

        log.info("getRegistrosConErrores(E)");

        AccesoDBOracle con = new AccesoDBOracle();
        StringBuffer qrySentencia = new StringBuffer();
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList resultado = new ArrayList();

        try {

            con.disableMessages();
            con.conexionDB();

            qrySentencia.append("SELECT                              	               " +
                                "  ST.IC_USUARIO_FACULTADO AS ID_USUARIO,             " +
                                "  ST.IC_FOLIO             AS FOLIO,                  " +
                                "  ST.IN_REGISTROS_RECH    AS NUM_REGISTROS,          " +
                                "  TIPO.CG_DESCRIPCION     AS OPERACION,              " +
                                "  ST.IC_IF_SIAG           AS IC_IF_SIAG              " +
                                "FROM                                                 " +
                                "  GTI_ESTATUS_SOLIC       ST,                        " +
                                "  GTICAT_TIPOOPERACION    TIPO                       " +
                                "WHERE                                                " +
                                "  ST.IC_TIPO_OPERACION = TIPO.IC_TIPO_OPERACION  AND " +
                                "  ST.IC_CORREO_ENV     = ?                           ");

            lVarBind.add(new Integer("1")); // -- 1 (REGISTROS CON ERROR)

            registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
            while (registros != null && registros.next()) {

                HashMap registro = new HashMap();

                String idUsuario = registros.getString("ID_USUARIO");
                String folio = registros.getString("FOLIO");
                String numRegistros = registros.getString("NUM_REGISTROS");
                String operacion = registros.getString("OPERACION");
                String icIfSiag = registros.getString("IC_IF_SIAG");

                operacion = codificaAcentos(operacion);

                registro.put("ID_USUARIO", idUsuario);
                registro.put("FOLIO", folio);
                registro.put("NUM_REGISTROS", numRegistros);
                registro.put("OPERACION", operacion);
                registro.put("IC_IF_SIAG", icIfSiag);

                resultado.add(registro);
            }

        } catch (Exception e) {
            log.error("getRegistrosConErrores(Exception)");
            log.error("getRegistrosConErrores.qrySentencia = <" + qrySentencia + ">");
            log.error("getRegistrosConErrores.exception:", e);
            e.printStackTrace();
            throw new Exception("Error consultar Registros con Errores.");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("getRegistrosConErrores(S)");
        }

        return resultado;
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo
     * @param folio
     * @param icIfSiag
     * @throws Exception
     */
    private void setCorreoEnviadoError(String folio, String icIfSiag) throws Exception {

        log.info("setCorreoEnviado(E)");

        if (Comunes.esVacio(folio)) {
            log.info("setCorreoEnviado(S)");
            return;
        }

        if (Comunes.esVacio(icIfSiag)) {
            log.info("setCorreoEnviado(S)");
            return;
        }

        AccesoDBOracle con = new AccesoDBOracle();
        PreparedStatement ps = null;
        boolean bOk = true;
        StringBuffer strSQL = new StringBuffer();

        try {
            con.disableMessages();
            con.conexionDB();

            strSQL.append("UPDATE                           " + "   GTI_ESTATUS_SOLIC             " +
                          "SET                              " + "   IC_CORREO_ENV = 2             " + // 2 (CORREO DE NOTIFICACION ENVIADO)
                          "WHERE                            " + "   IC_FOLIO            = ? AND   " +
                          "   IC_IF_SIAG 			= ?       ");

            ps = con.queryPrecompilado(strSQL.toString());
            ps.setString(1, folio);
            ps.setString(2, icIfSiag);
            ps.executeUpdate();

        } catch (Exception e) {
            log.error("setCorreoEnviado(Exception)");
            log.error("setCorreoEnviado.folio    = <" + folio + ">");
            log.error("setCorreoEnviado.icIfSiag = <" + icIfSiag + ">");
            log.error("setCorreoEnviado.strSQL   = <" + strSQL + ">");
            log.error("setCorreoEnviado.exception:", e);
            bOk = false;
            e.printStackTrace();
            throw new Exception("Error al actualizar estatus de envio de correo");
        } finally {
            log.info("setCorreoEnviado(S)");
            if (ps != null)
                ps.close();
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(bOk);
                con.cierraConexionDB();
            }
        }

    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo
     * @param destinatario
     * @param folio
     * @param numeroRegistros
     * @param operacion
     * @throws Exception
     */
    private void enviarCorreoDeNotificacionError(String destinatario, String folio, String numeroRegistros,
                                                 String operacion) throws Exception {

        log.info("enviarCorreoDeNotificacion(E)");

        // Agregar remitente
        String remitente = "no_response@nafin.gob.mx";
        // Agregar copia de carbon para los siguientes destinatarios
        String ccDestinatarios = null;
        // Agregar titulo del mensaje
        String tituloMensaje = "Notificacion Electronica de Archivos con Error";
        // Agregar mensaje
        StringBuffer mensajeCorreo = getMensajeError(folio, numeroRegistros, operacion);
        // Agregar Archivos de Imagenes

        ArrayList listaDeImagenes = new ArrayList();

        HashMap imagenNafin = new HashMap();
        java.net.URL imgURL = getClass().getResource("nafinsa.gif");
        if (imgURL != null) {
            imagenNafin.put("FILE_FULL_PATH", imgURL.getPath());
            imagenNafin.put("FILE_ID", "nafin_id");
            listaDeImagenes.add(imagenNafin);
        }
        // Agregar archivo adjunto
        ArrayList listaDeArchivos = null;
        // Enviar correo
        Correo correo = new Correo();
        correo.disableDebug();
        correo.enableUseAccesoDBOracle();
        correo.enviaCorreoConDatosAdjuntos(remitente, destinatario, ccDestinatarios, tituloMensaje,
                                           mensajeCorreo.toString(), listaDeImagenes, listaDeArchivos);

        log.info("enviarCorreoDeNotificacion(S)");
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo
     * @param folio
     * @param numeroRegistros
     * @param operacion
     * @return
     */
    private StringBuffer getMensajeError(String folio, String numeroRegistros, String operacion) {

        log.info("getMensaje(E)");

        StringBuffer mensaje = new StringBuffer();
        mensaje.append("<html>" + "	<head>" + "		<title>" + "			Correo con Notificacion de Errores SIAG." +
                       "		</title>" + "	</head>" + "	<body>" + "		<table width=\"600px\" border=\"0\">" + "			<tr>" +
                       "				<td height=\"30px\">&nbsp;</td>" + "			</tr>" + "			<tr>" +
                       "				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "				Estimado usuario," + "				</td>" + "			</tr>" + "			<tr>" +
                       "				<td height=\"35px\">&nbsp;</td>" + "			</tr>	" + "			<tr>" +
                       "				<td style=\"text-align:justify;font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "					Le informamos que la transacci&oacute;n de <span style=\"font-weight:bold\">" + operacion +
                       "</span>, " + "					identificada con el n&uacute;mero de folio " + folio +
                       " fue atendido y presenta " + numeroRegistros + " " + "					" +
                       ("1".equals(numeroRegistros) ? "operaci&oacute;n rechazada" : "operaciones rechazadas") +
                       ".<br>" + "					&nbsp;<br>" +
                       "					Por lo anterior, lo invitamos a que ingrese a <a href=\"http://cadenas.nafin.com.mx\">http://cadenas.nafin.com.mx</a> en la " +
                       "					Consulta de Resultados del M�dulo de Transacciones, para conocer " +
                       "					los motivos de rechazos." + "				</td>" + "			</tr>" + "			<tr>" +
                       "				<td height=\"50px\">&nbsp;</td>" + "			</tr>" + "			<tr>" +
                       "				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "				Atentamente,<br>" + "				Administraci&oacute;n de usuarios." + "				</td>" + "			</tr>" +
                       "			<tr>" + "				<td>&nbsp;</td>" + "			</tr>" + "			<tr>" + "				<td>" +
                       "					<img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> " +
                       "				</td>" + "			</tr>" + "		</table>	" + "	</body>" +
                       "</html>");

        log.info("getMensaje(S)");

        return mensaje;
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo de Autorizaci�n
     * @throws Exception
     */
    public void sendAutorizacionIfSIAG() throws Exception {

        log.info("send(E)");
        try {

            log.info("Obteniendo GarantiasEJB");

            ArrayList registros = getRegistrosConAutorizacionIf();
            UtilUsr utilUsr = new UtilUsr();
            for (int i = 0; i < registros.size(); i++) {

                HashMap registro = (HashMap) registros.get(i);

                String idUsuario = (String) registro.get("ID_USUARIO");
                String folio = (String) registro.get("FOLIO");
                String numeroRegistros = (String) registro.get("NUM_REGISTROS");
                String operacion = (String) registro.get("OPERACION");
                String icIfSiag = (String) registro.get("IC_IF_SIAG");

                try {

                    // obtiene correo Parametrizado
                    List email = this.getCorreosParametrizadosPorProcesoSiag(folio, icIfSiag, "AUTORIZACION_IF_SIAG");
                    // En caso de que no haya ninguna direcci�n de correo parametrizada usar la que tiene registrada
                    // en el OID el usuario facultado.
                    if (email.size() == 0) {

                        log.debug("\nALERTA:");
                        log.debug(" No es posible notificar a los usuarios NAFIN que la transaccion con Numero de Folio: " +
                                  folio);
                        log.debug(" (e IC_IF_SIAG = " + icIfSiag +
                                  ") tiene 1 Autorizacion IF, debido a que el IF no esta ");
                        log.debug(" parametrizado en GTI_CORREO_NOTIF.");

                        // Obtener correo del usuario
                        String emailUsuario = null;
                        try {
                            emailUsuario = utilUsr.getUsuario(idUsuario).getEmail();
                        } catch (Exception e) {
                            emailUsuario = null;
                        }

                        if (Comunes.esVacio(emailUsuario)) {
                            log.debug("\nALERTA:");
                            log.debug(" El usuario con ID: " + idUsuario +
                                      " no tiene una direccion de correo registrada");
                            log.debug(" por lo que no es posible notificarle que la transaccion con Numero de Folio: " +
                                      folio);
                            log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene 1 Autorizacion IF.");
                        }

                        // Agregar direcci�n a la lista de correos.
                        if (!Comunes.esVacio(emailUsuario)) {
                            email.add(emailUsuario);
                        }

                    }

                    String emailDestinatarios = null;
                    boolean exitoEnvioCorreos = email.size() > 0 ? true : false;
                    for (int j = 0; j < email.size(); j++) {

                        emailDestinatarios = (String) email.get(j);

                        if (!Comunes.esVacio(emailDestinatarios)) {
                            try {
                                // Enviar correo de notificaci�n
                                enviarCorreoDeNotificacion(emailDestinatarios, folio, numeroRegistros, operacion);
                            } catch (Exception e) {
                                exitoEnvioCorreos = false;
                                log.debug("\nALERTA:");
                                log.debug(" No fue posible notificar a: " + emailDestinatarios +
                                          " que la transaccion ");
                                log.debug(" con Numero de Folio: " + folio + " (e IC_IF_SIAG = " + icIfSiag +
                                          ") tiene 1 Autorizacion IF.");
                                log.error("\nALERTA:" + " No fue posible notificar a: " + emailDestinatarios +
                                          " que la transaccion" + " con Numero de Folio: " + folio +
                                          " (e IC_IF_SIAG = " + icIfSiag + ") tiene 1 Autorizacion IF:", e);
                            }
                        }

                    }

                    // Actualizar en tabla que se envio el correo con estatus 2
                    if (exitoEnvioCorreos) {
                        try {
                            setCorreoEnviado(folio, icIfSiag);
                        } catch (Exception e) {
                            log.info("\nALERTA:");
                            log.info(" No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: " +
                                               folio + " (e IC_IF_SIAG = " + icIfSiag + ").");
                            log.error("\nALERTA:" +
                                      " No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: " +
                                      folio + " (e IC_IF_SIAG = " + icIfSiag + ").", e);
                        }
                    }

                } catch (Exception e) {

                    log.debug("\nALERTA:");
                    log.debug(" No fue posible notificar que la transaccion con Numero de Folio: " + folio);
                    log.debug(" (e IC_IF_SIAG = " + icIfSiag + ") tiene 1 Autorizacion IF.");
                    log.error("\nALERTA:" + " No fue posible notificar que la transaccion con Numero de Folio: " +
                              folio + " (e IC_IF_SIAG = " + icIfSiag + ") tiene 1 Autorizacion IF:", e);

                }

            } //for


        } catch (Exception e) {
            log.error("send(Exception)", e);
            e.printStackTrace();
            throw e;
        } finally {
            log.info("send(S)");
        }
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo de Autorizaci�n
     * @return
     * @throws Exception
     */
    private ArrayList getRegistrosConAutorizacionIf() throws Exception {

        log.info("getRegistrosConAutorizacionIf(E)");

        AccesoDBOracle con = new AccesoDBOracle();
        StringBuffer qrySentencia = new StringBuffer();
        Registros registros = null;
        List lVarBind = new ArrayList();
        ArrayList resultado = new ArrayList();

        try {
            con.disableMessages();
            con.conexionDB();

            qrySentencia.append("SELECT                              	               " +
                                "  ST.IC_USUARIO_FACULTADO AS ID_USUARIO,             " +
                                "  ST.IC_FOLIO             AS FOLIO,                  " +
                                "  ST.IN_REGISTROS_RECH    AS NUM_REGISTROS,          " +
                                "  TIPO.CG_DESCRIPCION     AS OPERACION,              " +
                                "  ST.IC_IF_SIAG           AS IC_IF_SIAG              " +
                                "FROM                                                 " +
                                "  GTI_ESTATUS_SOLIC       ST,                        " +
                                "  GTICAT_TIPOOPERACION    TIPO                       " +
                                "WHERE                                                " +
                                "  ST.IC_TIPO_OPERACION = TIPO.IC_TIPO_OPERACION  AND " +
                                "  ST.IC_CORREO_ENV     = ? AND                       " +
                                "	ST.IC_TIPO_OPERACION = ? AND 								" +
                                "  ST.IC_SITUACION      = ? 									");

            lVarBind.add(new Integer("0")); // 0 - (REGISTROS SIN ERROR)
            lVarBind.add(new Integer("2")); // 2 - CARGA DE SALDOS Y PAGO COMISIONES
            lVarBind.add(new Integer("4")); // 4 - AUTORIZACI�N IF

            registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
            while (registros != null && registros.next()) {

                HashMap registro = new HashMap();

                String idUsuario = registros.getString("ID_USUARIO");
                String folio = registros.getString("FOLIO");
                String numRegistros = registros.getString("NUM_REGISTROS");
                String operacion = registros.getString("OPERACION");
                String icIfSiag = registros.getString("IC_IF_SIAG");

                operacion = codificaAcentos(operacion);

                registro.put("ID_USUARIO", idUsuario);
                registro.put("FOLIO", folio);
                registro.put("NUM_REGISTROS", numRegistros);
                registro.put("OPERACION", operacion);
                registro.put("IC_IF_SIAG", icIfSiag);

                resultado.add(registro);
            }

        } catch (Exception e) {
            log.error("getRegistrosConAutorizacionIf(Exception)");
            log.error("getRegistrosConAutorizacionIf.qrySentencia = <" + qrySentencia + ">");
            log.error("getRegistrosConAutorizacionIf.exception:", e);
            e.printStackTrace();
            throw new Exception("Error al consultar Registros con Autorizaci�n IF.");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("getRegistrosConAutorizacionIf(S)");
        }

        return resultado;
    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo de Autorizaci�n
     * @param folio
     * @param icIfSiag
     * @throws Exception
     */
    private void setCorreoEnviado(String folio, String icIfSiag) throws Exception {

        log.info("setCorreoEnviado(E)");

        if (Comunes.esVacio(folio)) {
            log.info("setCorreoEnviado(S)");
            return;
        }

        if (Comunes.esVacio(icIfSiag)) {
            log.info("setCorreoEnviado(S)");
            return;
        }

        AccesoDBOracle con = new AccesoDBOracle();
        PreparedStatement ps = null;
        boolean bOk = true;
        StringBuffer strSQL = new StringBuffer();

        try {
            con.disableMessages();
            con.conexionDB();

            strSQL.append("UPDATE                           " + "   GTI_ESTATUS_SOLIC             " +
                          "SET                              " + "   IC_CORREO_ENV = 2             " + // 2 (CORREO DE NOTIFICACION ENVIADO)
                          "WHERE                            " + "   IC_FOLIO        		= ? AND   " +
                          "   IC_IF_SIAG 			= ?       ");

            ps = con.queryPrecompilado(strSQL.toString());
            ps.setString(1, folio);
            ps.setString(2, icIfSiag);
            ps.executeUpdate();

        } catch (Exception e) {
            log.error("setCorreoEnviado(Exception)");
            log.error("setCorreoEnviado.folio    = <" + folio + ">");
            log.error("setCorreoEnviado.icIfSiag = <" + icIfSiag + ">");
            log.error("setCorreoEnviado.strSQL   = <" + strSQL + ">");
            log.error("setCorreoEnviado.exception:", e);
            bOk = false;
            e.printStackTrace();
            throw new Exception("Error al actualizar estatus de envio de correo");
        } finally {
            log.info("setCorreoEnviado(S)");
            if (ps != null)
                ps.close();
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(bOk);
                con.cierraConexionDB();
            }
        }

    }

    /**
     * Metodo que se usa en el proceso de SincGarantias.sh para envio de correo de Autorizaci�n
     * @param destinatario
     * @param folio
     * @param numeroRegistros
     * @param operacion
     * @throws Exception
     */
    private void enviarCorreoDeNotificacion(String destinatario, String folio, String numeroRegistros,
                                            String operacion) throws Exception {

        log.info("enviarCorreoDeNotificacion(E)");

        // Agregar remitente
        String remitente = "no_response@nafin.gob.mx";
        // Agregar copia de carbon para los siguientes destinatarios
        String ccDestinatarios = null;
        // Agregar titulo del mensaje
        String tituloMensaje = "Notificacion Electronica de Archivos con Autorizaci�n IF";
        // Agregar mensaje
        StringBuffer mensajeCorreo = getMensaje(folio, numeroRegistros, operacion);
        // Agregar Archivos de Imagenes

        ArrayList listaDeImagenes = new ArrayList();

        HashMap imagenNafin = new HashMap();
        java.net.URL imgURL = getClass().getResource("nafinsa.gif");
        if (imgURL != null) {
            imagenNafin.put("FILE_FULL_PATH", imgURL.getPath());
            imagenNafin.put("FILE_ID", "nafin_id");
            listaDeImagenes.add(imagenNafin);
        }
        // Agregar archivo adjunto
        ArrayList listaDeArchivos = null;
        // Enviar correo
        Correo correo = new Correo();
        correo.disableDebug();
        correo.enableUseAccesoDBOracle();
        correo.enviaCorreoConDatosAdjuntos(remitente, destinatario, ccDestinatarios, tituloMensaje,
                                           mensajeCorreo.toString(), listaDeImagenes, listaDeArchivos);

        log.info("enviarCorreoDeNotificacion(S)");
    }

    private StringBuffer getMensaje(String folio, String numeroRegistros, String operacion) {

        log.info("getMensaje(E)");

        StringBuffer mensaje = new StringBuffer();
        mensaje.append("<html>" + "	<head>" + "		<title>" +
                       "			Notificaci&oacute;n electr&oacute:nica de Archivos con Error." + "		</title>" + "	</head>" +
                       "	<body>" + "		<table width=\"600px\" border=\"0\">" + "			<tr>" +
                       "				<td height=\"30px\">&nbsp;</td>" + "			</tr>" + "			<tr>" +
                       "				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "				Estimado usuario," + "				</td>" + "			</tr>" + "			<tr>" +
                       "				<td height=\"35px\">&nbsp;</td>" + "			</tr>	" + "			<tr>" +
                       "				<td style=\"text-align:justify;font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "					Le informamos que la transacci&oacute;n de <span style=\"font-weight:bold\">" + operacion +
                       "</span>, " + "					identificada con el n&uacute;mero de folio " + folio +
                       " fue atendido y se encuentra en estatus Aurorizaci&oacute;n IF." +
                       "					Por lo anterior, lo invitamos a que ingrese a <a href=\"http://cadenas.nafin.com.mx\">http://cadenas.nafin.com.mx</a> en la " +
                       "					Autorizaci&oacute;n de Pago de Comisiones del M�dulo de Transacciones, para que dichas operaciones se autoricen de " +
                       " 				manera oportuna. " + "				</td>" + "			</tr>" + "			<tr>" +
                       "				<td height=\"50px\">&nbsp;</td>" + "			</tr>" + "			<tr>" +
                       "				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">" +
                       "				Atentamente,<br>" + "				Administraci&oacute;n de usuarios." + "				</td>" + "			</tr>" +
                       "			<tr>" + "				<td>&nbsp;</td>" + "			</tr>" + "			<tr>" + "				<td>" +
                       "					<img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> " +
                       "				</td>" + "			</tr>" + "		</table>	" + "	</body>" +
                       "</html>");

        log.info("getMensaje(S)");

        return mensaje;
    }


    /**
     * @param texto
     * @return
     */
    private String codificaAcentos(String texto) {
        log.info("codificaAcentos(S)");

        if (texto == null || texto.trim().equals("")) {
            log.info("codificaAcentos(S)");
            return "";
        }

        texto = texto.replaceAll("�", "&Aacute;");
        texto = texto.replaceAll("�", "&Eacute;");
        texto = texto.replaceAll("�", "&Iacute;");
        texto = texto.replaceAll("�", "&Oacute;");
        texto = texto.replaceAll("�", "&Uacute;");

        texto = texto.replaceAll("�", "&aacute;");
        texto = texto.replaceAll("�", "&eacute;");
        texto = texto.replaceAll("�", "&iacute;");
        texto = texto.replaceAll("�", "&oacute;");
        texto = texto.replaceAll("�", "&uacute;");

        texto = texto.replaceAll("�", "&Ntilde;");
        texto = texto.replaceAll("�", "&ntilde;");

        log.info("codificaAcentos(S)");
        return texto;
    }

}// Fin del Bean
