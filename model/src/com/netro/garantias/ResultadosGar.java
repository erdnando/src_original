package com.netro.garantias;

import java.math.*;
import java.io.*;
import java.util.*;

public class ResultadosGar implements Serializable {

	private StringBuffer	errores = new StringBuffer("");
	private StringBuffer	correctos = new StringBuffer("");
	private StringBuffer	cifras	= new StringBuffer("");
	private boolean			errorLinea = false;
	private int 			num_reg_ok		= 0;
	private int				num_reg_err		= 0;
	private int				num_reg_err_bis	= 0;	
	private int				num_err_cifras	= 0;	
	private BigDecimal		sum_reg_ok	= new BigDecimal(0);
	private BigDecimal		sum_reg_err	= new BigDecimal(0);
	private BigDecimal		sum_tot_pag	= new BigDecimal(0);	
	private int				ic_proceso = 0;
	private String			fecha	= "";
	private String			hora	= "";
	private String			folio	= "";
	private List			clavesFinan = null;
	//para calif cartera
	private BigDecimal		sum_montos_cub_ok	= new BigDecimal(0);
	private BigDecimal		sum_montos_exp_ok	= new BigDecimal(0);
	private BigDecimal		sum_montos_cub_err	= new BigDecimal(0);
	private BigDecimal		sum_montos_exp_err	= new BigDecimal(0);
	private String fechaValor;
	private int 				tipoCarga;
	private ReglasPrograma	reglasValidacionParametros;
	
	private int					numeroMaximoErrores = 500;
	private boolean			usaConteoErrorEnMetodoSetErrorLinea = false;
	
	public static final int CARGA_MASIVA 		= 1;
	public static final int CARGA_INDIVIDUAL 	= 2;
	
	public static final int NO_ENCONTRADO 				= 100;
	public static final int REQUERIDOS_IMSS 			= 101;
	public static final int REQUERIDOS_VACIOS_IMSS 	= 102;
	public static final int NO_REQUERIDOS_IMSS 		= 103;
 
	public static final int NO_OPERA_SHF_CRED_HIPOTECARIO = 201;
	public static final int OPERA_SHF_CRED_HIPOTECARIO		= 202;
	
	public static final int NO_OPERA_CREDITO_EDUCATIVO 	= 301;
	public static final int OPERA_CREDITO_EDUCATIVO			= 302;
 
	public ResultadosGar(){
		this.tipoCarga = ResultadosGar.CARGA_INDIVIDUAL;
	}
	
	public ResultadosGar(int tipoCarga){
		this.tipoCarga = tipoCarga;
	}
	
	public void addSumMontosCubErr(double valor){
		this.sum_montos_cub_err = this.sum_montos_cub_err.add(new BigDecimal(valor));
	}
	public void addSumMontosExpErr(double valor){
		this.sum_montos_exp_err = this.sum_montos_exp_err.add(new BigDecimal(valor));
	}
	public void addSumMontosCubOk(double valor){
		this.sum_montos_cub_ok = this.sum_montos_cub_ok.add(new BigDecimal(valor));
	}
	public void addSumMontosExpOk(double valor){
		this.sum_montos_exp_ok = this.sum_montos_exp_ok.add(new BigDecimal(valor));
	}

	public void addSumTotPag(double valor){
		this.sum_tot_pag = this.sum_tot_pag.add(new BigDecimal(valor));
	}
	public void addSumTotPag(BigDecimal valor){
		this.sum_tot_pag = this.sum_tot_pag.add(valor);
	}	
	public void addSumRegErr(double valor){
		this.sum_reg_err = this.sum_reg_err.add(new BigDecimal(valor));
	}	
	public void addSumRegErr(BigDecimal valor){
		this.sum_reg_err = this.sum_reg_err.add(valor);
	}
	public void addSumRegOk(double valor){
		this.sum_reg_ok = this.sum_reg_ok.add(new BigDecimal(valor));
	}
	public void addSumRegOk(BigDecimal valor){
		this.sum_reg_ok = this.sum_reg_ok.add(valor);
	}
	public void appendErrores(String valor){

		if(       num_reg_err_bis <  numeroMaximoErrores ){
			errores.append(valor);
		}else if( num_reg_err_bis == numeroMaximoErrores ){
			errores.append("\n\n\nFavor de revisar el archivo, contiene demasiados errores");
			this.usaConteoErrorEnMetodoSetErrorLinea = false;
		}
		
		if( !usaConteoErrorEnMetodoSetErrorLinea ){
			num_reg_err_bis++;
		}
		
	}
	public void appendCorrectos(String valor){
		correctos.append(valor);
	}
	public void appendCifras(String valor){
		if(num_err_cifras < 200){
			cifras.append(valor);
		}else if(num_err_cifras==200){
			cifras.append("\n\n\nEl archivo contiene demasiados registros repetidos, favor de revisarlo.");
		}
		num_err_cifras++;
	}
	public void incNumRegOk(){
		this.num_reg_ok ++;
	}
	public void incNumRegErr(){
		this.num_reg_err ++;
	}	
	public void setClavesFinan(List clavesF){
		this.clavesFinan = clavesF;
	}
	public void setErrorLinea(boolean valor){
		this.errorLinea = valor;
		if( this.errorLinea && this.usaConteoErrorEnMetodoSetErrorLinea ){
			num_reg_err_bis++;
		}
	}
	public void setFecha(String valor){
		this.fecha = valor;
	}
	public void setFolio(String valor){
		this.folio = valor;
	}	
	public void setHora(String valor){
		this.hora = valor;
	}	
	public void setIcProceso(int valor){
		this.ic_proceso = valor;
	}	
	public List	getClavesFinan(){
		return this.clavesFinan;
	}
	public String getCorrectos(){
		return this.correctos.toString();
	}
	public String getCifras(){
		return this.cifras.toString();
	}
	public String getErrores(){
		System.err.println("errores = <" + errores+ ">");
		return this.errores.toString();
	}
	public boolean getErrorLinea(){
		return this.errorLinea;
	}
	public String getFecha(){
		return this.fecha;
	}
	public String getFolio(){
		return this.folio;
	}
	public String getHora(){
		return this.hora;
	}	
	public int getIcProceso(){
		return this.ic_proceso;
	}
	public int	getNumRegOk(){
		return this.num_reg_ok;
	}
	public int	getNumRegErr(){
		return this.num_reg_err;
	}
	public double getSumRegOk(){
		return this.sum_reg_ok.doubleValue();
	}
	public double getSumRegErr(){
		return this.sum_reg_err.doubleValue();
	}
	public double getSumTotPag(){
		return this.sum_tot_pag.doubleValue();
	}
	public BigDecimal getValorSumTotPag(){
		return this.sum_tot_pag;
	}

	public double getSumMontosCubErr(){
		return this.sum_montos_cub_err.doubleValue();
	}
	public double getSumMontosExpErr(){
		return this.sum_montos_exp_err.doubleValue();
	}
	public double getSumMontosCubOk(){
		return this.sum_montos_cub_ok.doubleValue();
	}
	public double getSumMontosExpOk(){
		return this.sum_montos_exp_ok.doubleValue();
	}	

	public String getFechaValor() {
		return fechaValor;
	}

	public void setFechaValor(String fechaValor) {
		this.fechaValor = fechaValor;
	}
	
	public int getTipoCarga(){
		return this.tipoCarga;
	}

	public void setTipoCarga(int tipoCarga){
		this.tipoCarga = tipoCarga;
	}

	public ReglasPrograma getReglasValidacionParametros(){
		
		if (this.reglasValidacionParametros == null){
			this.reglasValidacionParametros = new ReglasPrograma();
		}
		return this.reglasValidacionParametros;
		
	}

	public void setReglasValidacionParametros(ReglasPrograma reglasValidacionParametros){
		this.reglasValidacionParametros = reglasValidacionParametros;
	}

	/**
	 *
	 * Devuelve la regla de validacion con la que opera el programa (<tt>clavePrograma</tt>).
	 * NOTA: POR EL MOMENTO, ESTE METODO SOLO ESTA PENSADO PARA UTILIZARSE EN LA CARGA MASIVA.
	 *
	 * @param clavePrograma <tt>String</tt> con el n�mero del programa.
	 *
	 * @return <tt>ResultadosGar.OPERA_CREDITO_EDUCATIVO</tt> si el programa opera con Cr�dito
	 *         Educativo; devuelve <tt>ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO</tt> en caso 
	 *         contrario.
	 *
	 * @author Salim Hernandez ( 08/02/2013 07:33:55 p.m. )
	 */
	public int getReglaValidacionParametroCreditoEducativo(String clavePrograma){
		
		ReglasPrograma 	reglas 	= getReglasValidacionParametros();
		return reglas.getReglaValidacionParametroCreditoEducativo(clavePrograma);
		
	}
	
	/**
	 * NOTA: Por el momento, este metodo solo esta pensado para utilizarse en la carga masiva
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public int getReglaValidacionParametrosIMSS(String clavePrograma){
		
		ReglasPrograma 	reglas 	= getReglasValidacionParametros();
		return reglas.getReglaValidacionParametrosIMSS(clavePrograma);
		
	}
	
	/**
	 * NOTA: Por el momento, este metodo solo esta pensado para utilizarse en la carga masiva
	 */
	public int getReglaValidacionSHFCreditoHipotecario(){
		
		ReglasPrograma 	reglas 	= getReglasValidacionParametros();
		return reglas.getReglaValidacionSHFCreditoHipotecario();
		
	}
	
	public BigDecimal getValorSumRegOk(){
		return this.sum_reg_ok;
	}
	
	public BigDecimal getValorSumRegErr(){
		return this.sum_reg_err;
	}

	public int getNumeroMaximoErrores() {
		return numeroMaximoErrores;
	}

	public void setNumeroMaximoErrores(int numeroMaximoErrores) {
		this.numeroMaximoErrores = numeroMaximoErrores;
	}

	public boolean getUsaConteoErrorEnMetodoSetErrorLinea() {
		return usaConteoErrorEnMetodoSetErrorLinea;
	}

	public void setUsaConteoErrorEnMetodoSetErrorLinea(boolean usaConteoErrorEnMetodoSetErrorLinea) {
		this.usaConteoErrorEnMetodoSetErrorLinea = usaConteoErrorEnMetodoSetErrorLinea;
	}

}// Fin de la clase
