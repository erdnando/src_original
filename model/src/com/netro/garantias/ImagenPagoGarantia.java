package com.netro.garantias;
import com.netro.exception.NafinException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;

public class ImagenPagoGarantia {

	private String proceso; //Clave temporal del proceso de carga
	private String claveImagen; //Clave de la imagen
	private String claveTipoImagen;	//Clave del tipo de imagen que representa
	
	private String contentType; //Tipo de contenido del archivo
	private String extension; //Extension del archivo
	private int size; //Tama�o del archivo
	
	private String folio; //Clave del folio del pago de garantia. Para imagenes definitivas

	public ImagenPagoGarantia()
	{
	}

	/**
	 * Guarda la imagen del pago de garantias en tabla temporal
	 * @param is Stream con los datos del archivo origen
	 * 
	 */
	
	public void guardarImagenPagoGarantiaTmp(InputStream is)
			throws NafinException {
		System.out.println("GarantiasBean::guardarImagenPagoGarantiaTmp(E)");
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		int tamanio = 0;
		try {
			if (this.proceso == null || this.proceso.equals("") || is == null) {
				throw new Exception("Los parametros son requeridos");
			}
			proceso = Long.parseLong(this.proceso);
			tamanio = this.size;
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					" this.proceso=" + this.proceso + "\n" +
					" is=" + is);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		boolean exito = false;
		PreparedStatement ps = null;
		AccesoDB con = new AccesoDB();
		
		String strSQL = null;
		

		try {
			con.conexionDB();

			strSQL = 
					" SELECT seq_gtitmp_imagen_solpago.nextval AS consecutivo " +
					" FROM dual ";

			Registros reg = con.consultarDB(strSQL);
			reg.next();
			long consecutivo = Long.parseLong(reg.getString("consecutivo"));
			
			strSQL = "INSERT INTO gtitmp_imagen_solpago(" +
					" ic_proceso, ic_imagen_solpago, ic_tipo_imagen, bi_imagen, cg_extension, cg_content_type) " +
					" VALUES (?, ?, ?, empty_blob(), ?, ?) ";
			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, proceso);
			ps.setLong(2, consecutivo);
			ps.setInt(3, Integer.parseInt(this.claveTipoImagen));
			ps.setString(4, this.extension);
			ps.setString(5, this.contentType);
			ps.executeUpdate();
			ps.close();
	
			strSQL = 
					" UPDATE gtitmp_imagen_solpago " +
					" SET bi_imagen = ? " +
					" WHERE ic_proceso = ? " + 
					" AND ic_imagen_solpago = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, is, tamanio);
			ps.setLong(2, proceso);
			ps.setLong(3, consecutivo);
	
			ps.executeUpdate();
			ps.close();
			is.close();
			
			exito = true;
		} catch(Exception e)  {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			System.out.println("GarantiasBean::guardarImagenPagoGarantiaTmp(S)");
		}
	}


	/**
	 * Obtiene los datos de las imagenes cargadas en la tabla temporal con
	 * respecto al pago de garantias.
	 * @throws com.netro.exception.NafinException
	 * @return Objeto Registros con los datos de las imagenes cargadas.
	 */
	public Registros consultarImagenesPagoGarantiaTmp()
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		try {
			if (this.proceso == null || this.proceso.equals("")) {
				throw new Exception("El proceso no esta establecido");
			}
			proceso = Long.parseLong(this.proceso);
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					" this.proceso=" + this.proceso );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String strSQL = 
				" SELECT img.ic_imagen_solpago, img.ic_tipo_imagen, tipo.cg_descripcion " +
				" FROM gtitmp_imagen_solpago img, gticat_tipo_imagen tipo " +
				" WHERE img.ic_tipo_imagen = tipo.ic_tipo_imagen " +
				" AND ic_proceso = ? ";
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			List varBind = new ArrayList();
			varBind.add(new Long(proceso));
			Registros reg = con.consultarDB(strSQL, varBind);
			return reg;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene la imagen almacenada en tablas temporales y genera un 
	 * archivo temporal para consultarla
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Cadena con el nombre del archivo generado con la imagen
	 * @throws com.netro.exception.NafinException
	 */

	public String consultarImagenPagoGarantiaTmp(String rutaDestino) 
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		long clave = 0;
		try {
			if (this.proceso == null || this.proceso.equals("") || 
					rutaDestino == null || rutaDestino.equals("")) {
				throw new Exception("Los parametros son obligatorios");
			}
			proceso = Long.parseLong(this.proceso);
			if (this.claveImagen == null || this.claveImagen.equals("")) {
				throw new Exception("La clave de la imagen debe ser especificada");
			}
			clave = Long.parseLong(this.claveImagen);
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					"this.proceso=" + this.proceso + "\n" +
					"rutaDestino=" + rutaDestino + "\n" +
					"this.claveImagen=" + this.claveImagen);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String strSQL = 
				" SELECT cg_extension, bi_imagen " +
				" FROM gtitmp_imagen_solpago " +
				" WHERE ic_proceso = ? " +
				" AND ic_imagen_solpago = ? ";
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, proceso);
			ps.setLong(2, clave);
			ResultSet rs = ps.executeQuery();
			String nombreArchivoTmp = null;
			if (rs.next()) {
				String extension = rs.getString("cg_extension");
				InputStream inStream = rs.getBinaryStream("bi_imagen");
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.make(inStream, rutaDestino, extension);
				nombreArchivoTmp = creaArchivo.getNombre();
			}
			rs.close();
			ps.close();
			return nombreArchivoTmp;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene la imagen almacenada en tablas definitivas y genera un 
	 * archivo temporal para consultarla
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Cadena con el nombre del archivo generado con la imagen
	 * @throws com.netro.exception.NafinException
	 */

	public String consultarImagenPagoGarantia(String rutaDestino) 
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long clave = 0;
		try {
			if (rutaDestino == null || rutaDestino.equals("")) {
				throw new Exception("Los parametros son obligatorios");
			}
			if (this.claveImagen == null || this.claveImagen.equals("")) {
				throw new Exception("La clave de la imagen debe ser especificada");
			}
			clave = Long.parseLong(this.claveImagen);
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					"rutaDestino=" + rutaDestino + "\n" +
					"this.claveImagen=" + this.claveImagen);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String strSQL = 
				" SELECT cg_extension, bi_imagen " +
				" FROM gti_imagen_solpago " +
				" WHERE " +
				" ic_imagen_solpago = ? ";
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, clave);
			ResultSet rs = ps.executeQuery();
			String nombreArchivo = null;
			if (rs.next()) {
				String extension = rs.getString("cg_extension");
				InputStream inStream = rs.getBinaryStream("bi_imagen");
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.make(inStream, rutaDestino, extension);
				nombreArchivo = creaArchivo.getNombre();
			}
			rs.close();
			ps.close();
			return nombreArchivo;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}





	/**
	 * Obtiene los datos de las imagenes cargadas en la tabla definitiva con
	 * respecto al pago de garantias x folio.
	 * @throws com.netro.exception.NafinException
	 * @return Objeto Registros con los datos de las imagenes cargadas.
	 */
	public Registros consultarImagenesPagoGarantia()
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		BigDecimal folio = null;
		try {
			if (this.folio == null || this.folio.equals("")) {
				throw new Exception("El folio no esta establecido");
			}
			folio = new BigDecimal(this.folio);
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					" this.folio=" + this.folio );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String strSQL = 
				" SELECT img.ic_imagen_solpago, tipo.cg_descripcion " +
				" FROM gti_imagen_solpago img, gticat_tipo_imagen tipo " +
				" WHERE img.ic_tipo_imagen = tipo.ic_tipo_imagen " +
				" AND ic_folio = ? ";
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			List varBind = new ArrayList();
			varBind.add(folio);
			Registros reg = con.consultarDB(strSQL, varBind);
			return reg;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Determina si el tipo de imagen ya existe para el proceso especificado
	 * @return true si existen o false de lo contrario
	 * @throws com.netro.exception.NafinException
	 */
	public boolean existeTipoImagenPagoGarantiaTmp()
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		try {
			if (this.proceso == null || this.proceso.equals("") || 
				this.claveTipoImagen == null || this.claveTipoImagen.equals("")) {
				throw new Exception("Los parametros/atributos son requeridos");
			}
			proceso = Long.parseLong(this.proceso);
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					" this.proceso=" + this.proceso );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL = 
						" SELECT count(*) as existe " +
						" FROM gtitmp_imagen_solpago " +
						" WHERE ic_proceso = ? " +
						" AND ic_tipo_imagen = ? ";
			List varBind = new ArrayList();
			varBind.add(new Long(this.proceso));
			varBind.add(new Integer(this.claveTipoImagen));
			Registros reg = con.consultarDB(strSQL, varBind);
			reg.next();
			return (reg.getString("existe").equals("0"))?false:true;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Determina si existen imagenes cargadas
	 * @return true si existen o false de lo contrario
	 * @throws com.netro.exception.NafinException
	 */
/*	public boolean existenImagenesPagoGarantiaTmp()
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		try {
			if (this.proceso == null || this.proceso.equals("")) {
				throw new Exception("El proceso no esta establecido");
			}
			proceso = Long.parseLong(this.proceso);
		} catch(Exception e) {
			System.out.println("existenImagenesPagoGarantiaTmp()::Error en los parametros/atributos establecidos. " + e.getMessage() +
					" this.proceso=" + this.proceso );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL = 
						" SELECT count(*) as existe " +
						" FROM gtitmp_imagen_solpago " +
						" WHERE ic_proceso = ? ";
			List varBind = new ArrayList();
			varBind.add(new Long(this.proceso));
			Registros reg = con.consultarDB(strSQL, varBind);
			reg.next();
			return (reg.getString("existe").equals("0"))?false:true;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
*/

	/**
	 * Elimina la imagen almacenada almacenada  en tablas temporales junto con sus datos
	 * asociados
	 * @throws com.netro.exception.NafinException
	 */

	public void eliminarImagenPagoGarantiaTmp() throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		long proceso = 0;
		long clave = 0;
		try {
			if (this.proceso == null || this.proceso.equals("")) {
				throw new Exception("Los parametros son obligatorios");
			}
			proceso = Long.parseLong(this.proceso);
			if (this.claveImagen == null || this.claveImagen.equals("")) {
				throw new Exception("La clave de la imagen debe ser especificada");
			}
			clave = Long.parseLong(this.claveImagen);
		
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " + e.getMessage() +
					"this.proceso=" + this.proceso + "\n" +
					"this.claveImagen=" + this.claveImagen);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String strSQL = 
				" DELETE FROM gtitmp_imagen_solpago " +
				" WHERE ic_proceso = ? " +
				" AND ic_imagen_solpago = ? ";
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, proceso);
			ps.setLong(2, clave);
			ps.executeUpdate();
			ps.close();
			exito = true;
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}




	public String getClaveImagen()
	{
		return claveImagen;
	}

	public void setClaveImagen(String claveImagen)
	{
		this.claveImagen = claveImagen;
	}

	public String getClaveTipoImagen()
	{
		return claveTipoImagen;
	}

	public void setClaveTipoImagen(String claveTipoImagen)
	{
		this.claveTipoImagen = claveTipoImagen;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getExtension()
	{
		return extension;
	}

	public void setExtension(String extension)
	{
		this.extension = extension;
	}

	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}

	public String getProceso()
	{
		return proceso;
	}

	public void setProceso(String proceso)
	{
		this.proceso = proceso;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


}