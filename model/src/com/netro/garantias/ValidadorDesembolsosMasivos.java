package com.netro.garantias;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ValidadorDesembolsosMasivos{
  
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ValidadorDesembolsosMasivos.class);
	
	public static boolean existeCampo(String nombreCampo,String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		log.info("existeCampo(E)");
		boolean existe = true;
		if(contenidoCampo == null || contenidoCampo.trim().equals("")){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor de '"+nombreCampo+"' es requerido. \n");
			resultados.setErrorLinea(true);
			existe = false;
		}
		log.info("existeCampo(S)");
		return(existe);
	}
	
	public static boolean campoNoRequerido(String nombreCampo,String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		log.info("campoNoRequerido(E)");
		boolean noRequerido = true;
		/*
		if( contenidoCampo != null && (contenidoCampo.trim().equals("") &&  !contenidoCampo.equals(" ")) ){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor del campo '"+nombreCampo+"' no es requerido, por lo que se tendr� que indicar forzosamente con un espacio en blanco que no es requerido. \n");
			resultados.setErrorLinea(true);
			noRequerido = false;	
		}else	if( contenidoCampo != null &&  !contenidoCampo.equals(" ")){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor del campo '"+nombreCampo+"' no es requerido, por lo que se tendr� que indicar forzosamente con un espacio en blanco que no es requerido. \n");
			resultados.setErrorLinea(true);
			noRequerido = false;
		}*/
		if( contenidoCampo != null && !contenidoCampo.equals("") ){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El campo '"+nombreCampo+"' no es requerido. \n");
			resultados.setErrorLinea(true);
			noRequerido = false;	
		}
		log.info("campoNoRequerido(S)");
		return(noRequerido);
	}
		
	public static boolean validaLongitudNumero(String nombreCampo,String contenidoCampo,int precision, int decimales,int numeroLinea, ResultadosGar resultados){
		log.info("validaLongitudNumero(E)");
		boolean longitudValida = true;
		if(!Comunes.precisionValida(contenidoCampo, precision, decimales)){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " La longitud de '"+nombreCampo+"' es incorrecta. \n");
			resultados.setErrorLinea(true);
			longitudValida = false;
		}
		log.info("validaLongitudNumero(S)");
		return(longitudValida);
	}
	
	public static boolean validarNumeroEntero(String nombreCampo,String contenidoCampo,int numeroLinea, ResultadosGar resultados){
		log.info("validarNumeroEntero(E)");
		boolean		enteroValido = true;
		BigInteger 	numeroEntero = null;
		BigInteger	cero			 = new BigInteger("0");
		try{
			numeroEntero = new BigInteger(contenidoCampo);
			if(numeroEntero.compareTo(cero) == -1){// Si es menor que cero
				resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' debe ser mayor de 0. \n");
				resultados.setErrorLinea(true);
				enteroValido = false;
			}
		}catch(Exception e){
			resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' debe ser un n�mero entero. \n");
			resultados.setErrorLinea(true);
			enteroValido = false;
		}
		log.info("validarNumeroEntero(S)");
		return(enteroValido);
	}
	
	public static boolean validaIntermediario(String claveSIAGCampo,String claveSIAG,String nombreCampo,int numeroLinea, ResultadosGar resultados){
		log.info("validaIntermediario(E)");
		boolean claveValida = true;
		try{
			BigInteger numeroClaveSIAGCampo 	= new BigInteger(claveSIAGCampo);
			BigInteger numeroClaveSIAG			= new BigInteger(claveSIAG); 
			if(numeroClaveSIAG.compareTo(numeroClaveSIAGCampo) != 0){
				resultados.appendErrores("Error en la linea: "+numeroLinea+" La clave del intermediario financiero es incorrecta. \n");
				resultados.setErrorLinea(true);
				claveValida = false;
			}
		}catch(Exception e){
			resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' debe ser un n�mero entero. \n");
			resultados.setErrorLinea(true);
			claveValida = false;
		}
		log.info("validaIntermediario(S)");
		return(claveValida);
	}

	public static boolean validarLongitudCadena(String nombreCampo,String contenidoCampo, int longitudMax,int numeroLinea, ResultadosGar resultados){
		log.info("validarLongitudCadena(E)");
		boolean longitudValida = true;
		if (contenidoCampo != null && contenidoCampo.length() > longitudMax) {
			resultados.appendErrores("Error en la linea: " + numeroLinea +" El valor del campo '" + nombreCampo + "' no cumple con la longitud m�xima permitida. \n");
			resultados.setErrorLinea(true);
			longitudValida = false;
		}
		log.info("validarLongitudCadena(S)");
		return(longitudValida);
	}
	
	public static boolean validarLongitudRFC(String nombreCampo,String contenidoCampo,int numeroLinea, ResultadosGar resultados){
		log.info("validarLongitudRFC(E)");
		boolean longitudValida = true;
		if (contenidoCampo != null && (contenidoCampo.length() != 12) && (contenidoCampo.length() != 13)) {
			resultados.appendErrores("Error en la linea: " + numeroLinea +" El valor del '" + nombreCampo + "' es incorrecto; XXXXAAMMDDXXX para Persona F�sica y XXXDDMMAAXXX para Persona Moral. \n");
			resultados.setErrorLinea(true);
			longitudValida = false;
		}
		log.info("validarLongitudRFC(S)");
		return(longitudValida);
	}
	
	public static boolean validaNumeroFlotantePositivo(String nombreCampo,String contenidoCampo,int numeroLinea, ResultadosGar resultados){
		
		log.info("validaNumeroFlotantePositivo(E)");
		boolean flotanteValido = true;
		try{
			
			BigDecimal flotante 	= new BigDecimal(contenidoCampo.trim());
			BigDecimal cero		= new BigDecimal("0");

			if(flotante.compareTo(cero) == -1){// Numero Flotante menor a Cero
				resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' no puede ser negativo. \n");
				resultados.setErrorLinea(true);
				flotanteValido = false;
			}

		}catch(Exception e){
			resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' no es un n�mero valido. \n");
			resultados.setErrorLinea(true);
			flotanteValido = false;
		}
		log.info("validaNumeroFlotantePositivo(S)");
		return(flotanteValido);
		
	}
	
	public static boolean validaFecha(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		
		log.info("validaFecha(E)");
		
		boolean 	fechaValida = true;
	   int 		dia 			= 0;
		int 		mes 			= 0; 
		int 		ano 			= 0;    
		boolean 	isFecha 		= false;
		
	   try {
			
			String strFecha = (contenidoCampo == null?"":contenidoCampo.trim());
			
			ano = Integer.parseInt(strFecha.substring(0,4));
	      mes = Integer.parseInt(strFecha.substring(4,6))-1;
	      dia = Integer.parseInt(strFecha.substring(6,strFecha.length()));
			
	      Calendar fecha = new GregorianCalendar(ano, mes, dia);
	      int 		year 	= fecha.get(Calendar.YEAR);
	      int 		month = fecha.get(Calendar.MONTH);
	      int 		day 	= fecha.get(Calendar.DAY_OF_MONTH);
	      
	      if((dia!=day) || (mes!=month) || (ano!=year)){
				resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' debe ser una fecha valida, formato AAAAMMDD. \n");
				resultados.setErrorLinea(true);
				fechaValida = false;
			}
			
	   }catch(Exception e) {
			resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '"+nombreCampo+"' es incorrecto, formato AAAAMMDD. \n");
			resultados.setErrorLinea(true);
			fechaValida = false;
		}
		
		log.info("validaFecha(S)");
		return(fechaValida);
	}
	
	public static boolean esSoN(String nombreCampo,String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		log.info("esSoN(E)");
		boolean valorValido = true;
		if(contenidoCampo != null && !contenidoCampo.equals("S") && !contenidoCampo.equals("N")){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor del campo '"+nombreCampo+"' debe ser S o N. \n");
			resultados.setErrorLinea(true);
			valorValido = false;
		}
		if(contenidoCampo == null){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor del campo '"+nombreCampo+"' debe ser S o N. \n");
			resultados.setErrorLinea(true);
			valorValido = false;			
		}	
		log.info("esSoN(S)");
		return(valorValido);
	}
	
	public static boolean validarCorreo(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		
		log.info("validarCorreo(E)");
		boolean correoValido = true;
		try {
			if(contenidoCampo != null && !"".equals(contenidoCampo.trim()) ) {
				if(!Comunes.validaEmail(contenidoCampo))
					throw new Exception();
			}
		} catch (Exception e) {
			if(resultados != null) resultados.appendErrores("Error en la linea: "+numeroLinea+" El formato del '"+nombreCampo+"' es incorrecto. \n");
			if(resultados != null) resultados.setErrorLinea(true);
			correoValido = false;
		} finally {
			log.info("validarCorreo(S)");
		}
		return(correoValido);
		
	}
	
	public static boolean validaRFC(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		
		log.info("validaRFC(E)");
		boolean rfcValido = true;
		try{
			String rfc = contenidoCampo.trim();
			// Agregar guiones al RFC para validarlo
			if( rfc != null && rfc.length() > 13 ){
				resultados.appendErrores("Error en la linea: "+numeroLinea+" El valor del campo '" + nombreCampo + "' no cumple con la longitud m�xima permitida. \n");
				resultados.setErrorLinea(true);
				rfcValido = false;
			}
 		}catch(Exception e){
      e.printStackTrace();
			resultados.appendErrores("Error en la linea: "+numeroLinea+" El '"+nombreCampo+"' del acreditado es incorrecto; XXXXAAMMDDXXX para Persona F�sica y XXXDDMMAAXXX para Persona Moral. \n");
			resultados.setErrorLinea(true);
			rfcValido = false;
		}
		log.info("validaRFC(S)");
		return(rfcValido);
		
	}
	
	public static boolean fondeoNafinHabilitado(String valorFondeo){
		log.info("fondeoNafinHabilitado(E)");
		boolean habilitado = false;
		if( valorFondeo != null && valorFondeo.equals("S") ){
			habilitado = true;
		}
		log.info("fondeoNafinHabilitado(S)");
		return(habilitado);
	}	
	
	public static boolean esPeriodicidadMensual(String periodicidad){
		log.info("esPeriodicidadMensual(E)");
		boolean esMensual = false;
		if( periodicidad != null && periodicidad.equals("2") ){
			esMensual = true;
		}
		log.info("esPeriodicidadMensual(S)");
		return(esMensual);
	}

	public static boolean esPeriodicidadAlVencimiento(String periodicidad){
		log.info("esPeriodicidadAlVencimiento(E)");
		boolean esAlVencimiento = false;
		if( periodicidad != null && periodicidad.equals("1") ){
			esAlVencimiento = true;
		}
		log.info("esPeriodicidadAlVencimiento(S)");
		return(esAlVencimiento);
	}		
	
	public static boolean esGarantiaRepetida(String claveGarantia,HashMap garantias){
		log.info("esGarantiaRepetida(E)");
		boolean repetida = false;
		if(claveGarantia != null){
			BigInteger 	numRepeticiones 	= (BigInteger) garantias.get(claveGarantia.trim());
			BigInteger  uno					= new BigInteger("1");
			if(numRepeticiones != null && (numRepeticiones.compareTo(uno) == 1) ){ //si numRepeticiones > 1
				repetida = true;
			}
		}
		log.info("esGarantiaRepetida(S)");
		return(repetida);
	}
	
	public static  boolean esGarantiaRegistrada(){
		log.info("esGarantiaRegistrada(E)");
		boolean registrada = false;
		log.info("esGarantiaRegistrada(S)");
		return(registrada);
	}
	
	public static boolean validaGarantiaRepetida(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados, HashMap listaGarantias){
		log.info("validaGarantiaRepetida(E)");
		boolean garantiaValida = true;
		if(esGarantiaRepetida(contenidoCampo,listaGarantias)){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " El valor del campo '"+nombreCampo+"' se encuentra repetido. \n");
			resultados.setErrorLinea(true);
			garantiaValida = false;
		}
		log.info("validaGarantiaRepetida(S)");
		return(garantiaValida);
	}
	
	public static boolean validaClavePeriodicidadPago(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados){
		log.info("validaClavePeriodicidadPago(E)");
		boolean claveValida = true;
		if(!esPeriodicidadAlVencimiento(contenidoCampo) && !esPeriodicidadMensual(contenidoCampo) ){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " La clave de '"+nombreCampo+"' es incorrecta. \n");
			resultados.setErrorLinea(true);
			claveValida = false;
		}
		log.info("validaClavePeriodicidadPago(S)");
		return(claveValida);
	}
	
	/**
	 * Determina si ya existe una clave de garantia duplicada.
	 * Para Desembolso Masivo (tipoOperacion=6) se considera que ya 
	 * esta registrado si no est� en estatus Aceptado (5) o (8) Rechazado, es
	 * decir cuando aun no ha sido revisada y procesada por el BACK no se puede
	 * volver a registrar el numero de garantia.
	 * 
	 * @return cadena con el Folio en caso de que exista o cadena 
	 * 		vacia de lo contrario
	 * @throws AppException
	 */
	public static String existeGarantia(String claveGarantia,String claveIF) throws AppException  {
		
		log.info("existeGarantia(E)");
		
		AccesoDB con 	= new AccesoDB();
		boolean 	exito = false;
		String 	folio = "";
		
		try {
			con.conexionDB();
			String qrySentencia = 
					" SELECT sp.ic_folio " +
					" FROM gti_solicitud_pago sp, " +
					" 	gti_estatus_solic es, comcat_if i " +
					" WHERE sp.ic_folio = es.ic_folio " +
					" 	AND sp.ic_if_siag = es.ic_if_siag "+
					" 	AND sp.ic_if_siag = i.ic_if_siag " +
					" 	AND es.ic_situacion not in(5,8) "+
					" 	AND i.ic_if = ? "+
					" 	AND sp.cc_garantia = ? ";

			List varBind = new ArrayList();
			varBind.add(new Integer(claveIF));
			varBind.add(claveGarantia);

			Registros reg = con.consultarDB(qrySentencia, varBind);
 
			if(reg.next()) {
				folio = (reg.getString("ic_folio") == null )?"":reg.getString("ic_folio");
			}
			
		} catch(Exception e) {
			log.error("existeGarantia(Exception)");
			log.error("existeGarantia.claveGarantia 	= <"+claveGarantia+">");
			log.error("existeGarantia.claveIF			= <"+claveIF+">");
			e.printStackTrace();
			throw new AppException("Error al consultar clave de garantia");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("existeGarantia(S)");
		return folio;
	}
	
	public static boolean validaGarantiaRegistrada(String nombreCampo, String contenidoCampo, int numeroLinea, ResultadosGar resultados, String claveIF) throws AppException{
		log.info("validaGarantiaRegistrada(E)");
		boolean garantiaRegistrada = true;
		String 	folio = existeGarantia(contenidoCampo,claveIF);
		if(folio != null && !folio.equals("")){
			resultados.appendErrores("Error en la linea: " + numeroLinea + " La clave: '"+contenidoCampo+"' ya se encuentra registrada y en proceso, con el folio: '"+folio+"'. \n");
			resultados.setErrorLinea(true);
			garantiaRegistrada = false;
		}
		log.info("validaGarantiaRegistrada(S)");
		return(garantiaRegistrada);
	}
	
}