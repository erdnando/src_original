package com.netro.garantias;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.ejb.Remote;

import netropology.utilerias.AppException;

@Remote
public interface Garantias {

    public ResultadosGar procesarAltaGarantia(String strDirectorio, String esNombreArchivo, String num_registros,
                                              String sum_montos, String ic_if) throws NafinException;

    public ResultadosGar procesarAltaRecuperaciones(String strDirectorio, String esNombreArchivo, String num_registros,
                                                    String sum_montos, String ic_if);

    public ResultadosGar procesarCargaSaldos(String strDirectorio, String esNombreArchivo, String num_registros,
                                             String sum_montos, String aniomes, String ic_if) throws NafinException;

    public ResultadosGar procesarAltaCalif(String strDirectorio, String esNombreArchivo, String anio_trim, String trim,
                                           String num_registros, String sum_montos_cub, String sum_montos_exp,
                                           String ic_if) throws NafinException; // @deprecated since F036 - 2013 -- GARANTIAS - Nueva metodología calificación

    public ResultadosGar procesarAltaCalif(String strDirectorio, String esNombreArchivo, String anioTrimestreControl,
                                           String trimestreControl, String numeroRegistrosControl,
                                           String montoExposicionIncumplimientoControl,
                                           String claveIF) throws NafinException;

    public ResultadosGar transmitirAltaGarantia(String icProceso, String ic_if, String num_registros,
                                                String impte_total, String ic_usuario) throws NafinException;

    public ResultadosGar transmitirAltaRecuperaciones(String icProceso, String ic_if, String num_registros,
                                                      String impte_total, String ic_usuario, String reciboFirma);

    public ResultadosGar transmitirCargaSaldos(String icProceso, String ic_if, String num_registros, String impte_total,
                                               String total_pag, String aniomes, List clavesFinan, String ic_usuario,
                                               String accion) throws NafinException;

    public ResultadosGar transmitirCalifCartera(String icProceso, String ic_if, String num_registros,
                                                String impte_total_cub, String impte_total_exp, String anio,
                                                String trim_calif,
                                                String ic_usuario) throws NafinException; // @deprecated since F036 - 2013 -- GARANTIAS - Nueva metodología calificación

    public ResultadosGar transmitirCalifCartera(String icProceso, String ic_if, String num_registros,
                                                String montoExposicionIncumplimientoControl, String anio,
                                                String trim_calif, String ic_usuario) throws NafinException;

    public void setSolicitudCancelada(String ic_folio) throws NafinException;

    public String getNumeroProcesoAltaPagoGarantia() throws NafinException;

    public List getSolicitudesPorAutorizar(String ic_if, String ic_folio, String df_fecha_hora) throws NafinException;

    public List getSolicitudesSeleccionadas(String ic_if, String ic_folio[]) throws NafinException;

    public List getSolicitudesConfirmadas(String ic_if, String ic_folio[]) throws NafinException;

    public AcusePagoGarantia guardarPagoGarantiasIF(PagoGarantiasIF pagoGarantia) throws NafinException;

    public abstract void setConfirmarSolicitudes(String fn_autorizacion_cargo, String ic_usuario_autoriza, String ic_if,
                                                 String ic_folio[]) throws NafinException;

    public List getTrimestresEnRango(String anio) throws NafinException;

    public List getFoliosFechas(String ic_if) throws NafinException;

    public ArrayList getPortafolioACalificar(String claveIF) throws NafinException;

    public HashMap getPortafolioACalificarPorRegistro(String anio, String trimestre, String fecha_corte,
                                                      String claveIF) throws NafinException;

    public void actualizaStatusRecepcionPortafolioCalificacion(String clave, String anioTrim, String trimestre,
                                                               String fechaCorte, String status) throws NafinException;

    public ArrayList getDetallePortafolioACalificarPorRegistro(String anio, String trimestre, String fecha_corte,
                                                               String claveIF) throws NafinException;

    public ArrayList getResultadosCierreRecepcionCalificaciones(String claveIF) throws NafinException;

    public void actualizaStatusAcuseResultadoCalificacion(String clave, String anioTrim, String trimestre,
                                                          String fechaCorte, String status) throws NafinException;

    public HashMap getDatosCierreRecepcionCalificacionesPorRegistro(String anio, String trimestre, String claveIF,
                                                                    String fecha_corte) throws NafinException;

    public ArrayList getCalificacionesRecibidas(String anio, String trimestre, String claveIF,
                                                String fecha_corte) throws NafinException;

    public ArrayList getCalificacionesNoRecibidas(String anio, String trimestre, String claveIF,
                                                  String fecha_corte) throws NafinException;

    public HashMap getDescripcionIntermediario(String claveIF) throws NafinException;

    public ArrayList getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(String claveIF) throws NafinException;

    public ArrayList getGarantiasConSituacionDeBajaDelPortafolioGarantizado(String claveIF) throws NafinException;

    public boolean esStatusDeAcuseDeReciboDelTrimestreAnteAnteriorValido(String claveIF) throws NafinException;

    public boolean esStatusDeRecepcionDePortafolioDelTrimestreAnteriorValido(String claveIF) throws NafinException;

    public ResultadosGar generarGarantiaAutomatica(String ic_if, String baseOperacion) throws NafinException;

    public ResultadosGar procesarCargaSaldosRegistrados(String ic_if) //FODEA 012-2009
        throws NafinException;

    public void resumenCargaSaldosRegistrados(ResultadosGar resultado, String aniomes, int numReg, BigDecimal montoReg,
                                              int totalAceptados, int totalRechazados, String in_enc) //FODEA 012-2009
        throws NafinException;

    public ResultadosGar reprocesarSaldosRegConError(String ic_folio, String ic_if) throws NafinException;

    public ResultadosGar transmitirCargaSaldos(String icProceso, String ic_if, String num_registros, String impte_total,
                                               String total_pag, String aniomes, List clavesFinan,
                                               String ic_usuario) throws NafinException;

    public Hashtable getFechaConAdecuaciones(String sTipoOperacion) throws NafinException;

    public ResultadosGar reprocesarAltaGarantiasRegConError(String ic_folio, String ic_if) throws NafinException;

    public ResultadosGar procesarAltaDesembolsosMasivos(String strDirectorio, String esNombreArchivo,
                                                        String num_registros, String sum_montos,
                                                        String ic_if) throws AppException;

    public ResultadosGar transmitirDesembolsosMasivos(String icProceso, String ic_if, String num_registros,
                                                      String impte_total, String ic_usuario,
                                                      String reciboFirma) throws AppException;

    public HashMap getParametrosDeNotificacion(String claveIf, String claveTransaccion,
                                               String claveEstatus) throws NafinException; //FODEA 048 - 2010 JRSW	

    public void setParametrosDeNotificacion(String claveIf, String claveTransaccion, String claveEstatus,
                                            String listaDeCuentasNAFIN,
                                            String listaDeCuentasIF) throws NafinException; //FODEA 048 - 2010 JRSW

    public HashMap getCamposParametricos(String claveIF, String claveBO) throws NafinException; //FODEA 008 - 2011 JRSW

    public List getComboIf() throws AppException; //FODEA 008 - 2011 JRSW

    public List getComboBO(String claveIf) throws AppException; //FODEA 008 - 2011 JRSW

    public void setcamposParam(String claveIf, String claveBO, String csSolicFondeo, String csSolicPartiRiesgo,
                               String cgPorcentajeParti, String cgPropositoProyec, String inClaveTipoCred,
                               String cgClaveGarantia, String cgPorcenPartiFinan, String inPorcenProdMerInter,
                               String inPorcenProdMerExter, String inFormaAmortizacion, String cgMoneda,
                               String inNumMesesGraciaFinan, String inClaveFuncionarioFaculIf,
                               String inTipoAutorizacion, String inTipoPrograma, String csTipoTasa, String inPlazoDias,
                               String cgCalificacionFinal,
                               String inAntiguedadClientMeses) throws NafinException; //FODEA 008 - 2011 JRSW

    public ReglasPrograma getReglasValidacionParametrosPorPrograma(String claveIF) throws AppException; // Fodea 025 - 2011

    public ResultadosGar procesarDesembolsosMasivos() throws NafinException; //Fodea 028-2011

    public ResultadosGar procesarRecuperacionDesembolsos() throws NafinException; //Fodea 028-2011

    public void resumenDesembolsoMasivo(ResultadosGar resultado, int numReg, BigDecimal montoReg, int totalAceptados,
                                        int totalRechazados, String in_enc, int ic_proceso) throws NafinException;

    public boolean operaProgramaSHFCreditoHipotecario(String claveIF) throws AppException; // Fodea 006-2012

    public boolean existeClaveSIAG(String claveIF) throws AppException; // Fodea 006-2012

    public List getResumenComisiones(String claveIF, String claveFiso,
                                     String claveMes) throws AppException; // Fodea 006-2012

    public HashMap getQueryDetalleComisiones(String claveIF, String claveFiso,
                                             String fechaCorte) throws AppException; // Fodea 006-2012

    public HashMap getQueryTotalesDetalleComisiones(String claveIF, String claveFiso,
                                                    String fechaCorte) throws AppException; // Fodea 006-2012

    public String getEstatusRecepcionComisiones(String claveFiso, String claveIF,
                                                String fechaCorte) throws AppException; // Fodea 006-2012

    public void actualizaStatusRecepcionComisiones(String claveFiso, String claveIF, String fechaCorte,
                                                   String estatus) throws AppException; // Fodea 006-2012

    public String getClaveFISO(String claveIF) throws AppException; // Fodea 006-2012

    public HashMap getEstatusObtencionComisiones(String claveIF, String claveFiso, String claveMesInicial,
                                                 String claveMesFinal) throws AppException; // Fodea 006-2012

    public boolean existeEnvioComisionesParaValidacionYCarga(String claveIF, String anioCarga,
                                                             String mesCarga) throws AppException; // Fodea 006-2012

    public ResultadosGar procesarCargaComisiones(String directorioTemporal, String nombreArchivo,
                                                 String numeroRegistros, String montoTotalComisiones, String claveIF,
                                                 String claveMes) throws AppException; // Fodea 006-2012

    public void validarRegistroComisiones(int claveIFSIAG, int numeroLinea, String contenidoLinea,
                                          ResultadosGar retorno,
                                          List clavesFinanciamientoSHF) throws AppException; // Fodea 006-2012

    public ResultadosGar transmitirComisiones(String claveProceso, String claveIF, String numeroRegistros,
                                              String montoTotalComisiones, String claveMes, String loginUsuario,
                                              String acuseFirmaDigital) throws AppException; // Fodea 006-2012

    public List getResultadosCargaComisiones(String claveIF, String fechaOperacionDe,
                                             String fechaOperacionA) throws AppException; // Fodea 006-2012

    public HashMap getQueryComisionesArchivoOriginal(String claveIF,
                                                     String folioOperacion) throws AppException; // Fodea 006-2012

    public HashMap getQueriesComisionesArchivoResultados(String claveIF,
                                                         String folioOperacion) throws AppException; // Fodea 006-2012

    public HashMap getQueryComisionesArchivoErrores(String claveIF,
                                                    String folioOperacion) throws AppException; // Fodea 006-2012

    public ResultadosGar procesarAltaReinstalaciones(String strDirectorio, String esNombreArchivo, String num_registros,
                                                     String sum_montos,
                                                     String ic_if) throws AppException; // Fodea 007-2013

    public ResultadosGar transmitirAltaReinstalaciones(String icProceso, String ic_if, String num_registros,
                                                       String impte_total, String ic_usuario,
                                                       String reciboFirma) throws AppException; // Fodea 007-2013

    public HashMap getQueryDetalleSolicitudReinstalaciones(String claveIfSiag,
                                                           String folio) throws AppException; // Fodea 007-2013

    public int getClaveIfSiag(String claveIF) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public List getListaPortafoliosConciliacionAutomatica(String claveTipoConciliacion,
                                                          String clavesSituacionConciliacion,
                                                          String claveIf) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public int getNumeroDiasHabilesPermitidosConciliacionAutomatica(String claveTipoConciliacion) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean requiereFirmaDescargaPortafolioConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                         String claveIfSiag,
                                                                         String claveTipoConciliacion) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean registraFirmaExtraccionPortafolioConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                           String claveIfSiag,
                                                                           String claveTipoConciliacion,
                                                                           String acuseFirmaPki,
                                                                           String loginUsuario) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public HashMap getDatosFirmaExtraccionConciliacionAutomatica(String claveMesConciliar, String claveFiso,
                                                                 String claveIfSiag,
                                                                 String claveTipoConciliacion) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean primerosDiasHabilesMesConciliacionAutomatica(String claveTipoConciliacion) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public int getClaveSituacionConciliacionAutomatica(String claveMesConciliar, String claveFiso, String claveIfSiag,
                                                       String claveTipoConciliacion) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public HashMap getMesConciliacionAutomatica() throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public List getCatalogoMesesConciliados(int numeroMesesConciliados) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public List getListaDetalleCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                 String claveIfSiag, String claveMoneda,
                                                                 String clavesSituacionConciliacion,
                                                                 int numeroRangoMeses) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean requiereFirmarDescargaCedulasConciliadasAutomaticamente(String claveMesConciliar, String claveFiso,
                                                                           String claveIfSiag) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean registraFirmaDescargaCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                          String claveIfSiag, String acuseFirmaPki,
                                                                          String loginUsuario) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public HashMap getDatosFirmaDescargaCedulasConciliadasAutomaticamente(String claveMesConciliado, String claveFiso,
                                                                          String claveIfSiag) throws AppException; // F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias

    public boolean obtenerCondonacionParcial(String claveIF,
                                             String tipo) throws AppException; // Fodea 005-2015-GTIAS-Conciliación

    public List getCorreosParametrizadosPorProcesoSiag(String folio, String icIfSiag,
                                                       String tipoProceso) throws Exception; // Ajuste: Actualizar Proceso CorreoErroresSIAG

    public void sendError() throws Exception;

    public void sendAutorizacionIfSIAG() throws Exception;

    ResultadosGar procesarCargaSaldos(String strDirectorio, String esNombreArchivo, String num_registros,
                                      String sum_montos, String aniomesIni, String aniomesFin,
                                      String ic_if) throws NafinException;
}
