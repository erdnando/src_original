package com.netro.garantias;

import netropology.utilerias.Comunes;
import java.util.*;
import java.io.Serializable;

public class ReglasPrograma implements Serializable {
	
	public static final long serialVersionUID = 20110526L;
 
	/*
	
		Fodea 006 - 2012
		Nota: OPERA_SHF_CRED_HIPOTECARIO y  REQUERIDOS_IMSS son excluyentes,
		Actualmente OPERA_SHF_CRED_HIPOTECARIO tiene precedencia sobre REQUERIDOS_IMSS
		
		Fodea ### - 2013 (Credito Educativo 2da Fase)
		OPERA_CREDITO_EDUCATIVO puede trabajar en conjunto con uno de los dos
		programas anteriores.
		
	*/
	
	// Variables para el Programa: Credito Educativo
	private boolean 	programaCreditoEducativoParametrizado;
	private HashMap	programasParametroCreditoEducativoHabilitado;
	// Variables para el Programa: Genera Empleo
	private boolean 	programaGeneraEmpleoParametrizado;
	private HashMap	programasParametrosIMSSHabilitados;
	// Variables para el Programa: SHF Credito Hipotecario
	private boolean 	programaSHFCreditoHipotecario;
	
	public ReglasPrograma(){
		this.programaCreditoEducativoParametrizado			= false;
		this.programasParametroCreditoEducativoHabilitado	= null;
		this.programaGeneraEmpleoParametrizado 				= false;
		this.programasParametrosIMSSHabilitados				= null;
		this.programaSHFCreditoHipotecario						= false;
	}

	public void    setProgramaCreditoEducativoParametrizado(boolean programaCreditoEducativoParametrizado) {
		this.programaCreditoEducativoParametrizado = programaCreditoEducativoParametrizado;
	}

	
	public boolean getProgramaCreditoEducativoParametrizado() {
		return this.programaCreditoEducativoParametrizado;
	}

 
	public void	   setProgramasParametroCreditoEducativoHabilitado( HashMap	programasParametroCreditoEducativoHabilitado) {
		this.programasParametroCreditoEducativoHabilitado = programasParametroCreditoEducativoHabilitado;
	}

	
	public HashMap	getProgramasParametroCreditoEducativoHabilitado() {
		return this.programasParametroCreditoEducativoHabilitado;
	}

 
	public int     getReglaValidacionParametroCreditoEducativo(String clavePrograma){
		
		// El IF no esta parametrizado para operar con el programa de Credito Educativo.
		if(!this.programaCreditoEducativoParametrizado){
			return ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO;
		}
 
		// El Programa Credito Educativo esta parametrizado "globalmente" para el Intermediario en Cuestion,
		// Revisar si  para el programa especificado se tiene habilitada la operacion con credito educativo
		if( "true".equals( (String) programasParametroCreditoEducativoHabilitado.get(clavePrograma) )){
			return ResultadosGar.OPERA_CREDITO_EDUCATIVO;	
		}
      
      // Para el programa especificado NO se tiene habilitada la operacion con credito educativo.
		return ResultadosGar.NO_OPERA_CREDITO_EDUCATIVO;	
		
	}
	
	/**
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public void setProgramaGeneraEmpleoParametrizado(boolean programaGeneraEmpleoParametrizado){
		this.programaGeneraEmpleoParametrizado = programaGeneraEmpleoParametrizado;
	}
	
	/**
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public boolean getProgramaGeneraEmpleoParametrizado(){
		return this.programaGeneraEmpleoParametrizado;	
	}
	
	/**
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public void setProgramasParametrosIMSSHabilitados(HashMap programasParametrosIMSSHabilitados){
		this.programasParametrosIMSSHabilitados = programasParametrosIMSSHabilitados;
	}
	
	/**
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public HashMap getProgramasParametrosIMSSHabilitados(){
		return this.programasParametrosIMSSHabilitados;
	}
	
	/**
	 * NOTA: Este m�todo ha quedado sin uso. F024 - 2013
	 */
	public int getReglaValidacionParametrosIMSS(String clavePrograma){
		
		// El Programa Genera Empleo no esta parametrizado
		if(!programaGeneraEmpleoParametrizado){
			return ResultadosGar.NO_REQUERIDOS_IMSS;
		}
		
		// La clave del programa no es valida
		if(!Comunes.esNumeroEnteroPositivo(clavePrograma)){
			return ResultadosGar.NO_ENCONTRADO;
		}
 
		// El Programa Genera Empleo esta parametrizado para el Intermediario en Cuestion,
		// Revisar si  para el programa especificado se tiene habilitada la recepcion de campos IMSS
		if( "true".equals( (String) programasParametrosIMSSHabilitados.get(clavePrograma) )){
			return ResultadosGar.REQUERIDOS_IMSS;	
		}
      
      //Los campos IMSS son requeridos vacios
		return ResultadosGar.REQUERIDOS_VACIOS_IMSS;	
 
	}
 
	public boolean getProgramaSHFCreditoHipotecario() {

		return programaSHFCreditoHipotecario;

	}


 
	public void setProgramaSHFCreditoHipotecario(boolean programaSHFCreditoHipotecario) {

		this.programaSHFCreditoHipotecario = programaSHFCreditoHipotecario;

	}

	
	public int getReglaValidacionSHFCreditoHipotecario(){
		
		// El IF esta parametrizado para operar con el programa de la
		// SHF "Credito Hipotecario"
		if(this.programaSHFCreditoHipotecario){
			return ResultadosGar.OPERA_SHF_CRED_HIPOTECARIO;
		}
		
		return ResultadosGar.NO_OPERA_SHF_CRED_HIPOTECARIO;
		
	}
	
}
