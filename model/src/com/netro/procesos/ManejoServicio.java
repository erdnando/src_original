/*************************************************************************************
*
* Nombre de Clase o de Archivo: ManejoServicio
*
* Versi�n: 0.1
*
* Fecha Creaci�n: 22/01/2002
*
* Autor: Gilberto E. Aparicio
*
* Fecha Ult. Modificaci�n: 03/06/2004  //FODA 050-2004
*
* Descripci�n de Clase:
*
*************************************************************************************/

package com.netro.procesos;

import java.util.*;
import com.netro.exception.*;
import java.sql.*;
import netropology.utilerias.AppException;
//import javax.servlet.http.*;
import javax.ejb.Remote;

@Remote
public interface ManejoServicio {
	public int cambiarEstatusDoctos()
		throws NafinException;

	public int cambiarEstatusPedidos()
		throws NafinException;

	public int cambiarEstatusDisp()
		throws NafinException;

	public int cambiarEstatusDoctosDist()
		throws NafinException;
	
	public String getBanderaPymes()
		throws NafinException;
	
	public boolean validarDiaHabilSig(int num_dias)
		throws NafinException;
	
	public boolean validarTasasDiaSig()
		throws NafinException;
		
	public String[] getEstatusServicio()
		throws NafinException;

	public boolean[]  getEstatusServicio(int liProducto)
		throws NafinException;

	public Vector getInfoProducto(String ic_producto_nafin)
		throws NafinException;
		
	public boolean getDescuentoElectronico() 
		throws NafinException;
		
	public List getHorariosDescuentoElec()
		throws NafinException;
		
	public List getProductosById(String idProducto);
	
	public boolean validarHorariosDiaDescuento();
		
	public Vector getDoctosSelPyme()
		throws NafinException;

	public Vector getDispSelPyme()
		throws NafinException;

	public Vector getPedidosSelPyme()
		throws NafinException;

	public Vector getDoctosSelPymeDist()
		throws NafinException;

	public boolean hayDoctosSelecIF()
		throws NafinException;

	public boolean hayPedidosSelecIF()
		throws NafinException;

	public boolean haySolicSelecIF()
		throws NafinException;

	public boolean haySolicSelecIFDist()
		throws NafinException;

	public boolean hayDoctosSelecPyme()
		throws NafinException;

	public boolean hayDisposicionSelecPyme()
		throws NafinException;

	public boolean hayPedidosSelecPyme()
		throws NafinException;

	public boolean hayDoctosSelecPymeDist()
		throws NafinException;

	public void setEstatusServicio(String estatusServicioPyme, String estatusServicioIF,
			String estatusServicioNafin )
		throws NafinException;

	public void setEstatusServicioPyme(String estatusServicioPyme)
		throws NafinException;

	public boolean abrirServicio()
		throws NafinException;

	public boolean abrirServicio(Vector vProcesos, boolean bverificaTasas)
		throws NafinException;

	public abstract boolean abrirServicio(String[] productos,String cs_cierre_servicio)
		throws NafinException;

	public boolean fnChAperturaPyme (int liProducto)
		throws NafinException;

	public boolean fnChAperturaIF ()
		throws NafinException;

	//METODOS AGREGADOS POR EGB FODA 050-2004
	public ArrayList getHorariosxProducto(String lsIcProducto)
		throws NafinException;

	public void setHorariosGenerales(String sProducto, String sAPymeM, String sCPymeM, String sAIFM,
			String sCIFM, String sANafinM, String sCNafinM)
		throws NafinException;

	public ArrayList getHorariosxProductoEpo(String lsIcProducto, String lsIcEpo)
		throws NafinException;

	public void setEstatusServicioPyme(String[] productos,String cs_cierre_servicio)
		throws NafinException;

	public void setHorariosXEpo(String sProducto, String sAPymeM, String sCPymeM, String sAIFM,
	String sCIFM, String sIcEpo, String sCsEspecial)
		throws NafinException;

	// M�todo usado en la pantalla de Administraci�n - Horarios de Servicio - Horarios Cr�dito Electr�nico. Foda 1 del 2005, Hugoro.
	public ArrayList getHorarioPorIfCE(String sNoIF) throws NafinException;
	public ArrayList getHorarioIfGralCE() throws NafinException;
	public void setHorarioGralCE(String sHoraInicioIF, String sHoraFinalIF) throws NafinException;
	public void setHorarioPorIfCE(ArrayList alHorariosIf) throws NafinException;
	public void setBorrarHorarioPorIf(String sNoIf) throws NafinException;
	// M�todo usado en la pantalla de IF - Cr�dito Electr�nico - Registro de Operaciones (Individual y Masiva).
	public void getEstadoDelServicioIfCE(String sNoIf) throws NafinException;
	
	public void cambiarEstatusDoctosFact24h() throws AppException;//FODEA 005 - 2009 ACF
	public boolean existenDoctosProgramadosPyme() throws AppException;//FODEA 005 - 2009 ACF
	public List getHorariosEpoxIf(String cveProducto, String cveEpo, String cveIf) throws AppException;
	public void setHorariosEpoXif(List lstHoraEpoxIf) throws AppException;

	public Map getValidaciones();
	
	public boolean fnChkCierre24Hrs ();
	public boolean validarDiaSiguientHabil(int num_dias) throws AppException;
	public boolean revisarTasasDiaHabilSiguient()throws AppException;
	public boolean fnChkCierrePymeDescuentoE ();
	public boolean abrirServicio24hrs(String[] productos,String cs_cierre_servicio) throws AppException;
}

