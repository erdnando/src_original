package com.netro.procesos;

import com.netro.exception.NafinException;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/*************************************************************************************
 *
 * Nombre de Clase o de Archivo: ManejoServicioBean
 *
 * Versi�n: 0.1
 *
 * Fecha Creaci�n: 22/01/2002
 *
 * Autor: Gilberto E. Aparicio
 *
 * Fecha Ult. Modificaci�n: 25/01/2002
 *
 * Descripci�n de Clase:
 *
 *************************************************************************************/
@Stateless(name = "ManejoServicioEJB" , mappedName = "ManejoServicioEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ManejoServicioBean implements ManejoServicio {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ManejoServicioBean.class);

	public int cambiarEstatusDoctos()
		throws NafinException {

		AccesoDB 		con  					= new AccesoDB();
		String 			qrySentencia 		= "";
		boolean 			hayError 			= false;
		ResultSet 		rs 					= null;
		int 				numRegAfectados 	= 0;
		List 				listaDocumentos 	= new ArrayList();
		boolean			hayDocumentos		= false;

		try {
			con.conexionDB();

			qrySentencia=" SELECT   d.ic_documento, hxe.cg_horario_fin_if " +
				" FROM (SELECT ie.ic_if, ie.ic_epo,  " +
				"			  NVL (hxei.cg_horario_fin_if,  " +
				"						 pn.cg_horario_fin_if  " +
				"					) cg_horario_fin_if  " +
				"			FROM comrel_if_epo ie,  " +
				"				  comcat_producto_nafin pn,  " +
				"				  comrel_horario_epo_x_if hxei   " +
				"		  WHERE ie.ic_if = hxei.ic_if(+) AND ie.ic_epo = hxei.ic_epo(+)   " +
				"				  AND pn.ic_producto_nafin = 1) hxe,  " +
				"		com_documento d  " +
				" WHERE hxe.ic_epo = d.ic_epo  " +
				" AND hxe.ic_if = d.ic_if   " +
				"  AND d.ic_estatus_docto = 3   " +
				"  AND d.cs_dscto_especial != 'C'  " +
				"  AND TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy ') || cg_horario_fin_if,  " +
				"					'dd/mm/yyyy hh24:mi'   " +
				"				  ) < SYSDATE   " +
				" GROUP BY d.ic_documento, hxe.cg_horario_fin_if   " ;

			System.out.println(" qrySentencia "+qrySentencia);

			rs = con.queryDB(qrySentencia);
			for(int i=0;rs.next();i++) {

				String icDocumento = rs.getString("IC_DOCUMENTO");
				qrySentencia=
				   " update com_documento "  +
					" set ic_estatus_docto = 2"  +	//Negociable
					" where ic_documento = "  + icDocumento +
					" and ic_estatus_docto = 3 ";  //Seleccionado Pyme
				con.ejecutaSQL(qrySentencia);

				qrySentencia = "insert into comhis_cambio_estatus"+
					" (dc_fecha_cambio, ic_documento, ic_cambio_estatus, ct_cambio_motivo)"+
					" values(sysdate,"+rs.getString("IC_DOCUMENTO")+
					" ,2,'Sin autorizacion IF en horario de servicio')";
				con.ejecutaSQL(qrySentencia);

				listaDocumentos.add(icDocumento);

				hayDocumentos		= true;
				numRegAfectados++;
			}
			rs.close();
			con.cierraStatement();

			// Regresar a Negocible todas las Notas de Credito que fueron aplicadas
			// Borrar registros relacionados de tabla: comhis_cambio_estatus
			if(hayDocumentos){
				actualizaEstatusNegociableNotasCredito(listaDocumentos,con);
			}

			// Indicar que los documentos se regresaron a Negociables porque no fueron autorizados
			// por el IF.
/*			qrySentencia="select d.ic_DOCUMENTO " +
				"from comrel_horario_x_epo hxe, com_documento d " +
				"where hxe.ic_epo = d.ic_epo " +
				"	and hxe.ic_producto_nafin = 1 " +
				"   and d.ic_Estatus_docto = 3 " +
			 	"	and to_date(to_char(sysdate, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < sysdate " +
				"	group by d.ic_epo, d.ic_if, D.IC_DOCUMENTO";

			rs = con.queryDB(qrySentencia);
			for(int i=0;rs.next();i++) {
			}
			rs.close();
			con.cierraStatement();*/

			return numRegAfectados;

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en ManejoServicioBean.cambiarEstatusDoctos(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (!hayError) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
		}
	}

	private void actualizaEstatusNegociableNotasCredito(List documentos, AccesoDB lodbConexion) throws AppException {

		log.info("actualizaEstatusNegociableNotasCredito(E)");

		String 				lsQry 	= "";
		ResultSet 			rs1, rs2 = null;
		PreparedStatement ps 		= null;

		StringBuffer 		esClaves							 			= null;

		boolean				hayDocumentos								= false;
		final int 			NUM_MAXIMO_DOCUMENTOS					= 900;
		int 					numeroTotalDocumentos					= 0;
		int 					indiceDocumento 							= 0;

		try {

			try {

				// REGRESAR MONTOS DE AQUELLOS DOCUMENTOS QUE UTILIZAN NOTAS DE CREDITO TRADICIONAL
				// BORRAR DEL HISTORIAL LOS CAMBIOS REGISTRADOS COMO CONSECUENCIA DE LA SELECCION
				// DE LOS DOCUMENTOS
				numeroTotalDocumentos	= documentos.size();
				indiceDocumento 			= 0;

				while( indiceDocumento < numeroTotalDocumentos ){

					StringBuffer listaDocumentos = new StringBuffer(2048);

					// 0. PROCESAR 900 DOCUMENTOS
					int documentosLeidos = 0;
					while( indiceDocumento < numeroTotalDocumentos ){

						if(documentosLeidos>0){ listaDocumentos.append(","); }
						listaDocumentos.append( documentos.get( indiceDocumento ) );

						indiceDocumento++;
						documentosLeidos++;

						// Se lleg� al n�mero m�ximo de documentos que se pueden
						// procesar en cada iteraci�n.
						if( documentosLeidos == NUM_MAXIMO_DOCUMENTOS ){
							break;
						}

					}

					// 1. OBTENER AQUELLOS DOCUMENTOS SIN NOTA DE CREDITO Y AQUELLOS
					//    QUE UTILIZAN NOTAS DE CREDITO DE FORMA TRADICIONAL.
					hayDocumentos								= false;
					esClaves							 			= new StringBuffer();
					lsQry = "SELECT           "  +
							  "	IC_DOCUMENTO  "  +
							  "FROM             "  +
							  "   COM_DOCUMENTO "  +
							  "WHERE            "  +
							  "    IC_DOCUMENTO IN ( "+listaDocumentos.toString()+" ) AND "  +
							  "    IC_DOCUMENTO NOT IN ( "  +
							  "      SELECT DISTINCT IC_DOCUMENTO FROM COMREL_NOTA_DOCTO "  +
							  " ) ";
					rs1 = lodbConexion.queryDB(lsQry);
					for(int k=0;rs1.next();k++){
						if(k>0) esClaves.append(",");
						esClaves.append(rs1.getString("IC_DOCUMENTO"));
						hayDocumentos = true;
					}
					rs1.close();
					lodbConexion.cierraStatement();

					if(hayDocumentos){

						VectorTokenizer 	vt 		= new VectorTokenizer(esClaves.toString(),",");
						Vector 				doctos 	= vt.getValuesVector();
						int 					existen 	= 0;

						for(int i=0;i<doctos.size();i++) {
							String ic_documento = doctos.get(i).toString();
							lsQry = " select count(1) from comhis_cambio_estatus "+
									" where ic_documento = ? "+
									" and ic_cambio_estatus = 8 ";
							ps = lodbConexion.queryPrecompilado(lsQry);
							ps.setInt(1,Integer.parseInt(ic_documento));
							rs1 = ps.executeQuery();
							existen = 0;
							if(rs1.next())
								existen = rs1.getInt(1);
							rs1.close();
							if(ps!=null) ps.close();
							if(existen==0) {
								lsQry =
									" UPDATE com_documento"   +
									"    SET cs_cambio_importe = 'N'"   +
									"  WHERE ic_documento = ?"  ;
								ps = lodbConexion.queryPrecompilado(lsQry);
								ps.setInt(1,Integer.parseInt(ic_documento));
								ps.executeUpdate();
								if(ps!=null) ps.close();
							}//if(existen=0)
						}//for(int i=0;i<vt.size();i++)

						// Borra historial de los documentos que cambiaron de monto.
						lsQry = " delete comhis_cambio_estatus "+
								" where ic_documento in ("+esClaves.toString()+") "+
								" and ic_cambio_estatus = 28 ";

						lodbConexion.ejecutaSQL(lsQry);

						// Borra del historial las Notas de Credito
						lsQry =
								" delete comhis_cambio_estatus "  +
								" where ic_documento in ( select ic_documento "  +
								"                         from com_documento "  +
								"								  where ic_docto_asociado in ("  + esClaves.toString() +") )";
						lodbConexion.ejecutaSQL(lsQry);

						// Borra los documentos "notas de credito" que fueron seleccionadas.
						lsQry =
								" delete com_docto_seleccionado "+
								" where ic_documento in ( select ic_documento "+
								"							from com_documento "+
								"							where ic_docto_asociado in ("+esClaves.toString()+") )";
						lodbConexion.ejecutaSQL(lsQry);

						// Ahora se actualiza el monto del documento.
						lsQry = " select sum(fn_monto) as monto_docto, ic_docto_asociado "+
								" from com_documento "+
								" where ic_docto_asociado in ("+esClaves.toString()+")"+
								" group by ic_docto_asociado";
						rs1 = lodbConexion.queryDB(lsQry);

						while(rs1.next()) {
							lsQry =
									" select fn_monto+"+rs1.getString("monto_docto")+" as monto from com_documento "+
									" where ic_documento = "+rs1.getString("ic_docto_asociado");
							rs2 = lodbConexion.queryDB(lsQry);
							while(rs2.next()) {
								lsQry = " update com_documento "+
										" set fn_monto = "+rs2.getString("monto")+
										" where ic_documento = "+rs1.getString("ic_docto_asociado");
								lodbConexion.ejecutaSQL(lsQry);
							}
							rs2.close();
						}
						rs1.close();
						lodbConexion.cierraStatement();

						// Se actualizan los valores de la nota de credito.
						lsQry = " update com_documento "+
								" set ic_estatus_docto = 2 "+
								" , ic_if = null "+
								" , df_fecha_venc = null "+
								" , ic_docto_asociado = null "+
								" where ic_docto_asociado in ("+esClaves.toString()+")"+
								" and cs_dscto_especial = 'C' "+
								" and ic_estatus_docto in (3)";// 23,24
						lodbConexion.ejecutaSQL(lsQry);

					}

				}

				// 2. DOCUMENTOS A LOS CUALES SE LES APLICO NOTAS DE CREDITO MULTIPLE
				numeroTotalDocumentos = documentos.size();
				indiceDocumento 		 = 0;

				while( indiceDocumento < numeroTotalDocumentos ){

					StringBuffer listaDocumentos = new StringBuffer(2048);

					// 0. PROCESAR 900 DOCUMENTOS
					int documentosLeidos = 0;
					while( indiceDocumento < numeroTotalDocumentos ){

						if(documentosLeidos>0){ listaDocumentos.append(","); }
						listaDocumentos.append( documentos.get( indiceDocumento ) );

						indiceDocumento++;
						documentosLeidos++;

						// Se lleg� al n�mero m�ximo de documentos que se pueden
						// procesar en cada iteraci�n.
						if( documentosLeidos == NUM_MAXIMO_DOCUMENTOS ){
							break;
						}

					}

					// 1. TRAER AQUELLOS DOCUMENTOS A LOS CUALES SE LES APLICO NOTAS DE CREDITO MULTIPLE
					hayDocumentos								= false;
					esClaves							 			= new StringBuffer();
					lsQry = 	"SELECT                "  +
								"  DISTINCT			     "  +
								"   IC_DOCUMENTO       "  +
								"FROM                  "  +
								"   COMREL_NOTA_DOCTO  "  +
								"WHERE                 "  +
								"   IC_DOCUMENTO IN (  "  +
								"    " + listaDocumentos.toString() + "  "  +
								"   ) ";
					rs1 = lodbConexion.queryDB(lsQry);
					for(int k=0;rs1.next();k++){
						if(k>0) esClaves.append(",");
						esClaves.append(rs1.getString("IC_DOCUMENTO"));
						hayDocumentos = true;
					}
					rs1.close();
					lodbConexion.cierraStatement();

					if(hayDocumentos){

						VectorTokenizer 	vt 		= new VectorTokenizer(esClaves.toString(),",");
						Vector 				doctos 	= vt.getValuesVector();
						int 					existen 	= 0;

						for(int i=0;i<doctos.size();i++) {
							String ic_documento = doctos.get(i).toString();
							lsQry = " select count(1) from comhis_cambio_estatus "+
									" where ic_documento = ? "+
									" and ic_cambio_estatus = 8 ";
							ps = lodbConexion.queryPrecompilado(lsQry);
							ps.setInt(1,Integer.parseInt(ic_documento));
							rs1 = ps.executeQuery();
							existen = 0;
							if(rs1.next())
								existen = rs1.getInt(1);
							rs1.close();
							if(ps!=null) ps.close();
							if(existen==0) {
								lsQry =
									" UPDATE com_documento"   +
									"    SET cs_cambio_importe = 'N'"   +
									"  WHERE ic_documento = ?"  ;
								ps = lodbConexion.queryPrecompilado(lsQry);
								ps.setInt(1,Integer.parseInt(ic_documento));
								ps.executeUpdate();
								if(ps!=null) ps.close();
							}//if(existen=0)
						}//for(int i=0;i<vt.size();i++)

						// Borra historial de los documentos que cambiaron de monto.
						lsQry = " delete comhis_cambio_estatus "+
								" where ic_documento in ("+esClaves.toString()+") "+
								" and ic_cambio_estatus = 28 ";

						lodbConexion.ejecutaSQL(lsQry);

						// Fodea 002 - 2010
						// Borra del historial las Notas de Credito
						lsQry =
								" delete comhis_cambio_estatus "  +
								" where ic_documento in ( select distinct ic_nota_credito "  +
								"                         from comrel_nota_docto "  +
								"								  where ic_documento in ("  + esClaves.toString() +") )";
						lodbConexion.ejecutaSQL(lsQry);

						// Borra los documentos "notas de credito" que fueron seleccionadas.
						lsQry =
								" delete com_docto_seleccionado "  +
								" where ic_documento in ( select distinct ic_nota_credito "  +
								"                         from comrel_nota_docto "  +
								"								  where ic_documento in ("  + esClaves.toString() +") )";
						lodbConexion.ejecutaSQL(lsQry);

						// Ahora se actualiza el monto del documento.
						lsQry = " select sum(fn_monto_aplicado) as monto_docto, ic_documento as ic_docto_asociado "  +
								  " from comrel_nota_docto "  +
								  " where ic_documento in ("  + esClaves.toString() + ")" +
								  " group by ic_documento";
						rs1 = lodbConexion.queryDB(lsQry);
						while(rs1.next()) {

							lsQry = " select fn_monto+"+rs1.getString("monto_docto")+" as monto from com_documento "+
									" where ic_documento = "+rs1.getString("ic_docto_asociado");
							rs2 = lodbConexion.queryDB(lsQry);
							while(rs2.next()) {
								lsQry = " update com_documento "+
										" set fn_monto = "+rs2.getString("monto")+
										" where ic_documento = "+rs1.getString("ic_docto_asociado");
								lodbConexion.ejecutaSQL(lsQry);
							}
							rs2.close();
						}
						rs1.close();
						lodbConexion.cierraStatement();

						// Se actualizan los valores de la nota de credito.
						lsQry =
								" update com_documento "+
								" set ic_estatus_docto = 2 "+
								" , ic_if = null "+
								" , df_fecha_venc = null "+
								" , ic_docto_asociado = null "+
								" where ic_documento in ( select distinct ic_nota_credito from comrel_nota_docto where ic_documento in ("+esClaves.toString()+") )"+
								" and cs_dscto_especial = 'C' "+
								" and ic_estatus_docto in (3)"; // Debug info:  revisar estatus docto: 23,24

						lodbConexion.ejecutaSQL(lsQry);

						// Fodea 002 - 2010. Borrar de Comrel Nota Docto toda referencia a Notas de Credito Multiple
						lsQry = " delete comrel_nota_docto "+
								  " where ic_documento in ("+esClaves.toString()+") ";
						lodbConexion.ejecutaSQL(lsQry);

					}

				}

			} catch(SQLException sqle) {
				log.error("actualizaEstatusNegociableNotasCredito(Exception)");
				log.error("lsQry: "+lsQry);
				sqle.printStackTrace();
				throw new AppException("Error al retornar documentos a negociable.");
			}

		} catch (Exception e) {
			log.error("actualizaEstatusNegociableNotasCredito(Exception)");
			e.printStackTrace();
			throw new AppException("El Sistema esta experimentando dificultades tecnicas, intente mas tarde-");
		}finally{
			log.info("actualizaEstatusNegociableNotasCredito(S)");
		}

	}

	public int cambiarEstatusPedidos()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		boolean hayError = false;
		ResultSet rs = null;
		int numRegAfectados = 0;

		try {
			con.conexionDB();


			qrySentencia = "SELECT P.IC_PEDIDO "+
				"FROM comrel_horario_x_epo hxe, " +
				"	com_pedido p, " +
				"	com_pedido_seleccionado ps, " +
				"	com_linea_credito lc " +
				"WHERE hxe.ic_epo = p.ic_epo " +
				"	AND hxe.ic_producto_nafin = p.ic_producto_nafin " +
				"	AND p.ic_Estatus_pedido = 3 " +
			 	"	AND to_date(to_char(sysdate, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < sysdate " +
				"	AND p.ic_pedido = ps.ic_pedido " +
				"	AND ps.ic_linea_credito = lc.ic_linea_credito " +
				"group by p.ic_epo, lc.ic_if, P.IC_PEDIDO";

			rs = con.queryDB(qrySentencia);
			while (rs.next()) {

				qrySentencia = "insert into com_cambio_estatus_pedido " +
					" (dc_fecha_cambio, ic_pedido, ic_cambio_estatus, ct_cambio_motivo)" +
					" values(sysdate,"+rs.getString("IC_PEDIDO")+",2,'Sin autorizacion IF en horario de servicio')";
				con.ejecutaSQL(qrySentencia);

				qrySentencia = " update com_pedido " +
					" set ic_estatus_pedido = 2" +
					" where ic_pedido = " + rs.getString("IC_PEDIDO") +
					" and ic_estatus_pedido = 3";
				con.ejecutaSQL(qrySentencia);

				numRegAfectados++;
			}
			rs.close();
			con.cierraStatement();
			return numRegAfectados;

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en ManejoServicioBean.cambiarEstatusPedidos(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (!hayError) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
		}
	}


	public int cambiarEstatusDisp()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		boolean hayError = false;
		ResultSet rs = null;
		int numRegAfectados = 0;

		try {
			con.conexionDB();


			qrySentencia= " SELECT d.cc_disposicion " +
			" FROM comrel_horario_x_epo hxe, inv_disposicion d " +
			" WHERE hxe.ic_epo = d.ic_epo " +
			" 	AND hxe.ic_producto_nafin = d.ic_producto_nafin " +
			"    AND d.ic_estatus_disposicion = 1 " +
			" 	AND TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy ')|| hxe.cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < SYSDATE " +
			" 	GROUP BY d.ic_epo, d.cc_disposicion ";


			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				qrySentencia="update inv_disposicion "+
					" set ic_estatus_disposicion = 2 "+	//Rechazada
					" where cc_disposicion = "+ rs.getString("CC_DISPOSICION") +
					" and ic_estatus_disposicion = 1 ";  //Seleccionado Pyme
				con.ejecutaSQL(qrySentencia);
				numRegAfectados ++;
			}
			rs.close();
			con.cierraStatement();
			return numRegAfectados ;

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en ManejoServicioBean.cambiarEstatusDisp(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (!hayError) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
		}
	}


	public int cambiarEstatusDoctosDist()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		boolean hayError = false;
		ResultSet rs = null;
		int numRegAfectados = 0;

		try {
			con.conexionDB();


			qrySentencia= " SELECT d.ic_documento " +
			" FROM comrel_horario_x_epo hxe, dis_documento d " +
			" WHERE hxe.ic_epo = d.ic_epo " +
			" 	AND hxe.ic_producto_nafin = d.ic_producto_nafin " +
			"    AND d.ic_estatus_docto = 3 " +
			" 	AND TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy ')|| hxe.cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < SYSDATE " +
			" 	GROUP BY d.ic_epo, d.ic_documento ";


			rs = con.queryDB(qrySentencia);
			while (rs.next()) {

				qrySentencia = "insert into dis_cambio_estatus"+
					" (dc_fecha_cambio, ic_documento, ic_cambio_estatus, ct_cambio_motivo)"+
					" values(sysdate,"+rs.getString("IC_DOCUMENTO")+
					" ,2,'Sin autorizacion IF en horario de servicio')";
				con.ejecutaSQL(qrySentencia);
				qrySentencia="update dis_documento"+
					" set ic_estatus_docto = 2"+	//Negociable
					" where ic_documento = " + rs.getString("IC_DOCUMENTO") +
					" and ic_estatus_docto = 3 ";  //Seleccionado Pyme
				con.ejecutaSQL(qrySentencia);
				numRegAfectados ++;
			}
			rs.close();
			con.cierraStatement();
			return numRegAfectados ;

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en ManejoServicioBean.cambiarEstatusDoctosDist(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (!hayError) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
		}
	}


	public String[] getEstatusServicio()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;
		String [] registro = new String[3];

		try {
			con.conexionDB();
			qrySentencia = "select cs_cierre_servicio, cs_cierre_servicio_if"+
				" , cs_cierre_servicio_nafin from comcat_producto_nafin"+
				" where"+
				" ic_producto_nafin = 1";

			rs = con.queryDB(qrySentencia);

			if (rs.next()) {
				numRegistros++;

				registro[0] = (rs.getString("CS_CIERRE_SERVICIO") == null)?"":rs.getString("CS_CIERRE_SERVICIO");
				registro[1] = (rs.getString("CS_CIERRE_SERVICIO_IF") == null)?"":rs.getString("CS_CIERRE_SERVICIO_IF");
				registro[2] = (rs.getString("CS_CIERRE_SERVICIO_NAFIN") == null)?"":rs.getString("CS_CIERRE_SERVICIO_NAFIN");
			}
			rs.close();
			con.cierraStatement();
			if (numRegistros > 0) {
				return registro;
			} else {
				throw new NafinException("GRAL0018");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en ManejoServicioBean.getEstatusServicio(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getDoctosSelPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia ="select d.ig_numero_docto, p.cg_razon_social as nombrePyme"+
				" , e.cg_razon_social as nombreEpo, i.cg_razon_social as nombreIf"+
				" from com_documento d, comcat_epo e, comcat_if i, comcat_pyme p"+
				" where d.ic_estatus_docto = 3" +
				" and d.ic_pyme = p.ic_pyme" +
				" and d.ic_epo = e.ic_epo" +
				" and d.ic_if = i.ic_if " +
				" AND D.ic_if in (select distinct d.ic_if " +
				"					from comrel_horario_x_epo hxe, com_documento d " +
				"					where hxe.ic_epo = d.ic_epo " +
				"					    and hxe.ic_producto_nafin = 1 " +
				"					    and d.ic_Estatus_docto = 3 " +
				"					    and d.ic_epo = hxe.ic_epo " +
				"					    and to_date(to_char(sysdate, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < sysdate " +
				"					group by d.ic_epo, d.ic_if)";

			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(4);

				registro.add(0, (rs.getString("IG_NUMERO_DOCTO") == null)?"":rs.getString("IG_NUMERO_DOCTO") );
				registro.add(1, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(2, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(3, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en ManejoServicioBean.getDoctosSelPyme(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getDispSelPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();


			qrySentencia = "SELECT d.cc_disposicion, p.cg_razon_social AS nombrePyme " +
			      " , e.cg_razon_social AS nombreEpo, i.cg_razon_social AS nombreIf " +
			      " FROM inv_disposicion d, comcat_epo e, comcat_if i, comcat_pyme p, com_linea_credito lc " +
			      " WHERE d.ic_estatus_disposicion = 1 " +
			      " AND d.ic_pyme = p.ic_pyme " +
			      " AND d.ic_epo = e.ic_epo " +
			      " AND d.ic_linea_credito = lc.ic_linea_credito " +
			      " AND lc.ic_if = i.ic_if  " +
			      " AND lc.ic_if IN (SELECT DISTINCT lc.ic_if  " +
			      "					FROM comrel_horario_x_epo hxe, inv_disposicion d, com_linea_credito lc  " +
			      "					WHERE hxe.ic_epo = d.ic_epo  " +
			      "					    AND hxe.ic_producto_nafin = 5 " +
			      "					    AND d.ic_estatus_disposicion = 1  " +
			      "					    AND d.ic_epo = hxe.ic_epo " +
			      "						AND d.ic_linea_credito = lc.ic_linea_credito   " +
			      "					    AND TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < SYSDATE  " +
			      "					GROUP BY d.ic_epo, lc.ic_if) " ;



			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(4);

				registro.add(0, (rs.getString("CC_DISPOSICION") == null)?"":rs.getString("CC_DISPOSICION") );
				registro.add(1, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(2, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(3, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en ManejoServicioBean.getDispSelPyme(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getPedidosSelPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "select pe.ig_numero_pedido, py.cg_razon_social as nombrePyme , e.cg_razon_social as nombreEpo " +
				"	from com_pedido pe, " +
				"          comcat_epo e, " +
				"          comcat_if i, " +
				"          comcat_pyme py, " +
				"          com_pedido_seleccionado ps, " +
				"          com_linea_credito lc " +
				"    where pe.ic_estatus_pedido = 3 " +
				"		 and pe.ic_pyme = py.ic_pyme " +
				"		 and pe.ic_epo = e.ic_epo " +
				"        and pe.ic_pedido = ps.ic_pedido " +
				"        and ps.ic_linea_credito = lc.ic_linea_credito " +
				"        and lc.ic_if = i.ic_if " +
				"        and i.ic_if in (SELECT distinct lc.ic_if " +
				"							FROM comrel_horario_x_epo hxe, "+
				"							    com_pedido p, " +
				"							    com_pedido_seleccionado ps, " +
				"							    com_linea_credito lc " +
				"							WHERE hxe.ic_epo = p.ic_epo " +
				"							    AND hxe.ic_producto_nafin = p.ic_producto_nafin " +
				"							    AND p.ic_Estatus_pedido = 3 " +
				"							    AND p.ic_epo = hxe.ic_epo " +
				"							    AND to_date(to_char(sysdate, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < sysdate " +
				"							    AND p.ic_pedido = ps.ic_pedido " +
				"							    AND ps.ic_linea_credito = lc.ic_linea_credito " +
				"								group by p.ic_epo, lc.ic_if " +
				"						 )";

			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(3);

				registro.add(0, (rs.getString("IG_NUMERO_PEDIDO") == null)?"":rs.getString("IG_NUMERO_PEDIDO") );
				registro.add(1, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );
				registro.add(2, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en ManejoServicioBean.getPedidosSelPyme(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getDoctosSelPymeDist()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();


			qrySentencia = " SELECT d.ic_documento, p.cg_razon_social AS nombrePyme  "+
						"	       , e.cg_razon_social AS nombreEpo, i.cg_razon_social AS nombreIf  "+
						"	       FROM dis_documento d, comcat_epo e, comcat_if i, comcat_pyme p, com_linea_credito lc  "+
						"	       WHERE d.ic_estatus_docto = 3 "+
						"	       AND d.ic_pyme = p.ic_pyme  "+
						"	       AND d.ic_epo = e.ic_epo  "+
						"	       AND d.ic_linea_credito = lc.ic_linea_credito  "+
						"	       AND lc.ic_if = i.ic_if   "+
						"	       AND lc.ic_if IN (SELECT DISTINCT lc.ic_if   "+
						"	      					FROM comrel_horario_x_epo hxe, dis_documento d, com_linea_credito lc   "+
						"	      					WHERE hxe.ic_epo = d.ic_epo   "+
						"	      					    AND hxe.ic_producto_nafin = d.ic_producto_nafin  "+
						"	      					    AND d.ic_estatus_docto = 3   "+
						"	      						AND d.ic_linea_credito = lc.ic_linea_credito    "+
						"	      					    AND TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < SYSDATE   "+
						"	      					GROUP BY d.ic_epo, lc.ic_if)  "+
						"	UNION ALL "+
						"	 SELECT d.ic_documento, p.cg_razon_social AS nombrePyme  "+
						"	       , e.cg_razon_social AS nombreEpo, i.cg_razon_social AS nombreIf  "+
						"	       FROM dis_documento d, comcat_epo e, comcat_if i, comcat_pyme p, dis_linea_credito_dm lc  "+
						"	       WHERE d.ic_estatus_docto = 3 "+
						"	       AND d.ic_pyme = p.ic_pyme  "+
						"	       AND d.ic_epo = e.ic_epo  "+
						"	       AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
						"	       AND lc.ic_if = i.ic_if   "+
						"	       AND lc.ic_if IN (SELECT DISTINCT lc.ic_if   "+
						"	      					FROM comrel_horario_x_epo hxe, dis_documento d, dis_linea_credito_dm lc   "+
						"	      					WHERE hxe.ic_epo = d.ic_epo   "+
						"	      					    AND hxe.ic_producto_nafin = d.ic_producto_nafin  "+
						"	      					    AND d.ic_estatus_docto = 3   "+
						"	      						AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
						"	      					    AND TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy ')|| cg_horario_fin_if, 'dd/mm/yyyy hh24:mi') < SYSDATE  "+
						"	      					GROUP BY d.ic_epo, lc.ic_if) " ;

			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(4);

				registro.add(0, (rs.getString("IC_DOCUMENTO") == null)?"":rs.getString("IC_DOCUMENTO") );
				registro.add(1, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(2, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(3, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();
			return registros;

		} catch (Exception e) {
			System.out.println("Error en ManejoServicioBean.getDoctosSelPymeDist(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayDoctosSelecIF()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numRegSelIf"+
				" from com_solicitud where ic_estatus_solic = 1"+	//Seleccionadas IF
				" and ic_bloqueo = 1"; //Solicitud CANCELADA por el proceso de interfase de nafin electronico

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("NUMREGSELIF");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en hayDoctosSelecIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean haySolicSelecIF()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numSolSelIf "+
				" from inv_solicitud where ic_estatus_solic = 1 ";	//Seleccionadas IF


			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("numSolSelIf");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en haySolicSelecIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean haySolicSelecIFDist()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numSolSelIf "+
				" from dis_solicitud where ic_estatus_solic = 1 ";	//Seleccionadas IF
			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("numSolSelIf");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en haySolicSelecIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	public boolean hayPedidosSelecIF()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numPedSelIf"+
				" from com_anticipo where ic_estatus_antic = 1"+ //Seleccionadas IF
				" and ic_bloqueo = 1"; //Solicitud CANCELADA por el proceso de interfase de nafin electronico

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("NUMPEDSELIF");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en hayPedidosSelecIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayDoctosSelecPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numRegSelPyme"+
			" from com_documento where ic_estatus_docto = 3"; //Seleccionada Pyme

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("NUMREGSELPYME");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en hayDoctosSelecPyme: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayPedidosSelecPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = " select count(*) as numPedSelPyme "+
			" from com_pedido"+
			" where ic_estatus_pedido = 3";	//Seleccionada Pyme

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("NUMPEDSELPYME");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en hayPedidosSelecPyme: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayDisposicionSelecPyme()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = " select count(*) as numDispSelPyme "+
			" from inv_disposicion "+
			" where ic_estatus_disposicion = 1" +
			" and ic_producto_nafin = 5 ";	//Seleccionada Pyme

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("numDispSelPyme");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en haydisposicionSelecPyme: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayDoctosSelecPymeDist()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();

			qrySentencia = "select count(*) as numRegSelPyme"+
			" from dis_documento where ic_estatus_docto = 3"; //Seleccionada Pyme

			rs = con.queryDB(qrySentencia);
			rs.next();
			numRegistros = rs.getInt("NUMREGSELPYME");
			rs.close();
			con.cierraStatement();

			if (numRegistros > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error en hayDoctosSelecPymeDist: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public void setEstatusServicio(String estatusServicioPyme, String estatusServicioIF,
			String estatusServicioNafin )
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";

		estatusServicioPyme = (estatusServicioPyme == null)?"":estatusServicioPyme;
		estatusServicioIF = (estatusServicioIF == null)?"":estatusServicioIF;
		estatusServicioNafin = (estatusServicioNafin == null)?"":estatusServicioNafin;

		try {
			con.conexionDB();
				if(!estatusServicioPyme.equals("")) {
					qrySentencia="update comcat_producto_nafin"+
						" set cs_cierre_servicio='"+estatusServicioPyme+"'"+
						" where ic_producto_nafin in(1,2,4,5)";
					con.ejecutaSQL(qrySentencia);
					con.terminaTransaccion(true);
				}
				if(!estatusServicioIF.equals("")) {
					qrySentencia="update comcat_producto_nafin"+
						" set cs_cierre_servicio_if='"+estatusServicioIF+"'"+
						" where ic_producto_nafin in(1,2,4,5)";
					con.ejecutaSQL(qrySentencia);
					con.terminaTransaccion(true);
				}
				if(!estatusServicioNafin.equals("")) {
					qrySentencia="update comcat_producto_nafin"+
						" set cs_cierre_servicio_nafin='"+estatusServicioNafin+"'"+
						" where ic_producto_nafin in(1,2,4,5)";
					con.ejecutaSQL(qrySentencia);
					con.terminaTransaccion(true);
				}

		} catch (Exception e) {
			System.out.println("Error en setEstatusServicio: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public void setEstatusServicioPyme(String[] productos,String cs_cierre_servicio)
			throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		String ic_productos = "";
		for(int i=0;i<productos.length;i++){
			if(!"".equals(ic_productos))
				ic_productos += ",";
			ic_productos += productos[i];
		}
		try {
			con.conexionDB();
			qrySentencia=
				" UPDATE comcat_producto_nafin"   +
				"    SET cs_cierre_servicio = '"+cs_cierre_servicio+"' "   +
				"  WHERE ic_producto_nafin in ("+ic_productos+") "  ;
System.out.println("\n************************************* \n qrySentencia: "+qrySentencia);
			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);
		} catch (Exception e) {
			System.out.println("Error en setEstatusServicio: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public void setEstatusServicioPyme(String estatusServicioPyme )	throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		try {
			con.conexionDB();
			if(!estatusServicioPyme.equals("")) {
				qrySentencia="update comcat_producto_nafin"+
					" set cs_cierre_servicio='"+estatusServicioPyme+"'"+
					" where ic_producto_nafin in (1,2,4,5)";
				con.ejecutaSQL(qrySentencia);
				con.terminaTransaccion(true);
			}
		} catch (Exception e) {
			System.out.println("Error en setEstatusServicio: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/*Agregado EGB - 31/08/205 Permite la ejecuci�n de los procesos y la apertura del servicio */
	public boolean abrirServicio(Vector vProcesos, boolean bverificaTasas)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		CallableStatement cs=null;
		boolean bOktransaccion = true;
		boolean bServicio = true;

		try 	{
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado("ALTER SESSION SET NLS_TERRITORY = MEXICO");
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);
log.info("bverificaTasas " + bverificaTasas);

			for(int i=0; i<vProcesos.size(); i++){
				System.out.println("Ejecutando proceso: " + vProcesos.get(i).toString());
				cs = con.ejecutaSP(vProcesos.get(i).toString());
				cs.execute();
				cs.close();
				System.out.println("Terminando Ejecuci�n proceso: " + vProcesos.get(i).toString());
				// Agregado por si termin� su ejecucion, haga el commit por proceso
				con.terminaTransaccion(bOktransaccion);
			}
			if (bverificaTasas == true) {
				bServicio = abrirServicio();
			}
			return bServicio;

		} catch (NafinException ne) {
			bOktransaccion = false;
			throw ne;
		} catch (Exception e) {
			bOktransaccion = false;
			System.out.println("Error en abrirServicio (Vector, boolean): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOktransaccion);
				con.cierraConexionDB();
			}
		}
	}

	public boolean abrirServicio()
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "";
		ResultSet rs = null;
		int numTasas = 0;
		int numTasasRequeridas = 0;
		try {
			con.conexionDB();
			qrySentencia=
				" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM com_mant_tasa mt, comcat_tasa t"   +
				"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
				"    AND t.ic_tasa (+) = mt.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			//rs = con.queryDB(qrySentencia);
			rs.next();
			numTasas = rs.getInt("NUMTASAS");
			rs.close();
			if(ps != null) ps.close();
			qrySentencia=
				" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM comcat_tasa"   +
				"  WHERE cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			rs.next();
			numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
			rs.close();
			if(ps != null) ps.close();
			//this.cambiarEstatusDoctos32();//FODEA 037-2010 - cambia de estatus de Programado Pyme a Seleccionado Pyme
			if ((numTasasRequeridas == numTasas)) {//FODEA 005 - 2009 ACF
			  this.cambiarEstatusDoctosFact24h();//FODEA 005 - 2009 ACF
			}

			if ((numTasasRequeridas == numTasas) && !this.existenDoctosProgramadosPyme()) {//FODEA 005 - 2009 ACF
				setEstatusServicio("N","N","N");	//Apertura de servicio
				return true;
			} else {
				return false;
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en setEstatusServicio: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public boolean abrirServicio(String[] productos,String cs_cierre_servicio)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "";
		ResultSet rs = null;
		int numTasas = 0;
		int numTasasRequeridas = 0;
		try {
			con.conexionDB();
			qrySentencia=
				" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM com_mant_tasa mt, comcat_tasa t"   +
				"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
				"    AND t.ic_tasa (+) = mt.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			//rs = con.queryDB(qrySentencia);
			if(rs.next())
				numTasas = rs.getInt("NUMTASAS");
			rs.close();
			if(ps != null) ps.close();
			qrySentencia=
				" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM comcat_tasa"   +
				"  WHERE cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next())
				numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
			rs.close();
			if(ps != null) ps.close();
			if (numTasasRequeridas == numTasas) {
				setEstatusServicioPyme(productos,cs_cierre_servicio);	//Apertura de servicio
				return true;
			} else {
				return false;
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en setEstatusServicio: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Metodo que permite obtener una bandera para saber si el servicio esta
	 * abierto o cerrado
	 * @return banderaCerrada Devuelve 'S' si esta cerrada o 'N' si esta
	 * abierta
	 */
	public String getBanderaPymes(){
		log.info("getBanderaPymes(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement 	ps = null;
		ResultSet			rs = null;
		String banderaCerrada = "";

		String query = "SELECT cs_cierre_servicio AS bandera " +
				"FROM comcat_producto_nafin WHERE ic_producto_nafin = 1";
		try{
			con.conexionDB();
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();

			while(rs.next()){
				banderaCerrada = rs.getString("bandera");
			}

		}catch (Exception e)	{
			log.error("Error en ManejoServicioBean.getBanderaPymes(): "+e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getBanderaPymes(S)");
		return banderaCerrada;
	}

	/**
	 * Metodo que permite validar si el siguiente es un dia es habil
	 * @return validarDia Verdadero si el siguiente dia es habil
	 * @param num_dias Los dias a sumar a la fecha actual para validar el
	 * siguiente dia habil
	 */
	public boolean validarDiaHabilSig(int num_dias){
		log.info("validarDiaHabilSig(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement 	ps = null;
		ResultSet			rs = null;
		Calendar cal = Calendar.getInstance();
		String qry_dia_inhabil = "";
		String qry_dia_inhabil_x_anio = "";
		boolean validarDia = true;

		try{
			con.conexionDB();
			qry_dia_inhabil_x_anio = "SELECT count(*) as diaInhabil " +
			"FROM comcat_dia_inhabil "+
			"WHERE df_dia_inhabil = TO_DATE(TO_CHAR(sysdate+"+num_dias+",'dd/mm/yyyy'),'dd/mm/yyyy')";
			ps = con.queryPrecompilado(qry_dia_inhabil_x_anio);
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt("diaInhabil") != 0){
				log.info("Lo sentimos el siguiente es un dia inhabil");
				validarDia = false;
			}
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			qry_dia_inhabil = "SELECT count(*) as diaInhabil " +
			"FROM comcat_dia_inhabil "+
			"WHERE cg_dia_inhabil = TO_CHAR(sysdate+"+num_dias+",'dd/mm')";
			ps = con.queryPrecompilado(qry_dia_inhabil);
			rs = ps.executeQuery();
			rs.next();
			if(rs.getInt("diaInhabil") != 0){
				log.info("Lo sentimos el siguiente es un dia inhabil");
				validarDia = false;
			}
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			cal.add(Calendar.DATE, num_dias);
			int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
			if(diaSemana ==1 || diaSemana ==7){
				log.info("Lo sentimos el siguiente es un dia inhabil S-D");
				validarDia = false;
			}
		}catch (Exception e)	{
			log.error("Error en ManejoServicioBean.validarDiaHabilSig(): "+e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("validarDiaHabilSig(S)");
		return validarDia;
	}


	/**
	 * Metodo que permite validar la existencia de tasas en el siguiente dia
	 * habil
	 * @return validarTasas Verdadero en caso de existir tasas
	 */
	public boolean validarTasasDiaSig(){
		log.info("validarTasasDiaSig(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement 	ps = null;
		ResultSet			rs = null;
		int numDias = 1;
		int numTasas = 0;
		int numTasasRequeridas = 0;
		String qryTasas = "";
		String qryTasasRequeridas = "";
		boolean validarTasas = false;

		while(!validarDiaHabilSig(numDias)){
			numDias++;
		}

		try{
			con.conexionDB();
			qryTasas = "SELECT count(*) as numtasas " +
			"FROM com_mant_tasa mt, comcat_tasa t " +
			"WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = " +
			"TO_DATE (TO_CHAR (SYSDATE + "+numDias+", 'dd/mm/yyyy'), 'dd/mm/yyyy') " +
			"AND t.ic_tasa (+) = mt.ic_tasa " +
			"AND t.cs_disponible = 'S'";
			ps = con.queryPrecompilado(qryTasas);
			rs = ps.executeQuery();
			rs.next();
			numTasas = rs.getInt("numtasas");
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			qryTasasRequeridas = "SELECT count(*) as numtasasreq " +
			"FROM comcat_tasa " +
			"WHERE cs_disponible = 'S'";
			ps = con.queryPrecompilado(qryTasasRequeridas);
			rs = ps.executeQuery();
			rs.next();
			numTasasRequeridas = rs.getInt("numtasasreq");
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			if(numTasas == numTasasRequeridas)
				validarTasas = true;

		}catch (Exception e)	{
			log.error("Error en ManejoServicioBean.validarTasasDiaSig(): "+e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("validarTasasDiaSig(S)");
		return validarTasas;
	}

	/**
	 * Metodo que valida que este activo un proceso para horario de servicio
	 * @return descElectronico Verdadero si existe el proceso
	 */
	public boolean getDescuentoElectronico(){
		log.info("getDescuentotoElectronico(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement 	ps = null;
		ResultSet			rs = null;
		boolean descElectronico = false;
		String qryProcesoVencApe =
										"SELECT to_char(df_fecha,'HH24:MI') as hora , CG_NOMBRE_PROCESO "+
										"  FROM bit_ejecuta_proceso "+
										" WHERE TO_CHAR (df_fecha, 'dd/mm/yyyy') = TO_CHAR (SYSDATE, 'dd/mm/yyyy') "+
										"   AND cg_nombre_proceso LIKE '%spcom_neg_venc%' "+
										"    order by df_fecha desc, cg_nombre_proceso desc ";

		try{
			con.conexionDB();
			ps = con.queryPrecompilado(qryProcesoVencApe);
			rs = ps.executeQuery();

			if(rs.next())
				descElectronico = true;
		}catch (Exception e)	{
			log.error("Error en ManejoServicioBean.getEstatusServicio(): "+e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getDescuentoElectronico(E)");
		return descElectronico;
	}

	public boolean[] getEstatusServicio(int liProducto)
		throws NafinException {
		log.info("getEstatusServicio(E)");
		boolean[] registro = new boolean[3];
		try {

			registro[0] = fnChkCierrePyme(liProducto);
			if(liProducto == 1){
				boolean bandera = getDescuentoElectronico();
				if(!bandera){
					registro[0] = true;
				}
			}
			registro[1] = fnChkCierreIF(liProducto);
			registro[2] = fnChkCierreNafin(liProducto);

		}
		catch (Exception e)	{
			System.out.println("Error en ManejoServicioBean.getEstatusServicio(): "+e);
			throw new NafinException("SIST0001");
		}
		log.info("getEstatusServicio(S)");
		return registro;
	}

	/**
	*	Consulta para obtener una lista de productos
	*  @param idProducto Cadena en la cual se indica el id del producto
	*  que se desea buscar, en caso de ser "" regresa todos los productos.
	*  @return producto Un vector de vectores en los que se incluye el id y
	*  nombre del producto
	*/

	public Vector getInfoProducto(String ic_producto_nafin)
		throws NafinException {
		log.info("getInfoProducto(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement 	ps = null;
		ResultSet			rs = null;
		Vector 				producto = new Vector();
		try {
			con.conexionDB();
			String condicion = "";
			if("".equals(ic_producto_nafin))
				condicion =  "  WHERE ic_producto_nafin in (1,4)";
			else
				condicion =  "  WHERE ic_producto_nafin = ?";
			String qrySentencia =
				" SELECT ic_producto_nafin,ic_nombre"   +
				"   FROM comcat_producto_nafin"+condicion+" ORDER BY 1";
			ps = con.queryPrecompilado(qrySentencia);
			if(!"".equals(ic_producto_nafin))
				ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			rs = ps.executeQuery();
			while(rs.next()) {
				Vector renglones = new Vector();
				renglones.addElement(new Integer(rs.getInt(1)));
				renglones.addElement(rs.getString(2));
				producto.addElement(renglones);
			}
			rs.close();
			if(ps!=null) ps.close();
		}
		catch (Exception e)	{
			System.out.println("Error en ManejoServicioBean.getEstatusServicio(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getInfoProducto(S)");
		return producto;
	}

	/**
	 * Metodo que permite validar si Descuento electronico se encuentra dentro
	 * de los horarios de servicio
	 * @return valido Verdadero si todos los registros se encuentran dentro
	 * del horario de servicio
	 */
	public boolean validarHorariosDiaDescuento(){
		log.info("validarHorariosDiaDescuento(E)");
		String qry_Total = "";
		String qry_Dentro_Hr = "";
		int numTotal = 0;
		int numDentro = 0;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean valido = true;

		try{
			con.conexionDB();
			qry_Total = "SELECT count(*) AS contador " +
			"FROM COMREL_HORARIO_EPO_X_IF " +
			"WHERE ic_producto_nafin = 1";

			qry_Dentro_Hr ="SELECT count(*) AS contador " +
			"FROM COMREL_HORARIO_EPO_X_IF " +
			"WHERE ic_producto_nafin = 1 " +
			"AND TO_DATE(to_char(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
			"BETWEEN TO_DATE(cg_horario_inicio, 'hh24:mi') " +
			"AND TO_DATE(cg_horario_fin, 'hh24:mi')";

			ps = con.queryPrecompilado(qry_Total);
			rs = ps.executeQuery();
			rs.next();
			numTotal = rs.getInt("contador");
			System.out.println("QueryTotal: "+ qry_Total);
			System.out.println("Total en Desc E: " +numTotal);
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			ps = con.queryPrecompilado(qry_Dentro_Hr);
			rs = ps.executeQuery();
			rs.next();
			numDentro = rs.getInt("contador");
			System.out.println("QueryDentro: "+ qry_Dentro_Hr);
			System.out.println("Total dentro de horario en Desc E: " +numDentro);
			ps.clearParameters();
			rs.close();
			if(ps != null)
				ps.close();

			if(numTotal > numDentro)
				valido = false;

		}catch (Exception e) {
			log.error("Error en ManejoServicio.validarHorariosDiaDescuento: "+e);
			throw new AppException("Error en consulta para validaciones", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("validarHorariosDiaDescuento(S)");
		}
		return valido;
	}

	/**
	 * Metodo que retorna el producto de Descuento electronico ademas de
	 * las validaciones pertinentes al apartado horario de servicio 24 hrs,
	 * valida que se encuentre dentro de los horarios de servicio, tenga
	 * tasas del siguiente dia habil y validacion de la bandera.
	 * @return productos
	 */
	public List getHorariosDescuentoElec(){
		log.info("getHorariosDescuentoElec(E)");
		boolean validaHorario = false;
		boolean validarTasas = false;
		Vector infoProd = null;
		List productos = new ArrayList();
		String todosDentroHr = "";
		String bandera = "";
		String tasas = "";
		String servicioDesc = "";
		try{
			infoProd = this.getInfoProducto("1");
			for(int i = 0; i < infoProd.size(); i++){
				Vector aux = (Vector)infoProd.get(i);
				validaHorario = validarHorariosDiaDescuento();
				validarTasas = validarTasasDiaSig();
				bandera = getBanderaPymes();
				todosDentroHr 	= (validaHorario)?"S":"N";
				tasas = (validarTasas)?"S":"N";
				if(!validaHorario && validarTasas){
					servicioDesc 	= (bandera.equals("S"))?"Servicio Cerrado":"Servicio Abierto";
				}else{
					servicioDesc = "Servicio Cerrado";
				}
				HashMap mapProductos = new HashMap();
				mapProductos.put("ID", aux.get(0));
				mapProductos.put("NOM_PRODUCTO", aux.get(1));
				mapProductos.put("TODOS_DENTRO", todosDentroHr);
				mapProductos.put("SER_DESC", servicioDesc);
				mapProductos.put("BANDERA", bandera);
				mapProductos.put("HAY_TASAS", tasas);

				productos.add(mapProductos);
			}
		}catch(Exception e){
			log.error("Error en ManejoServicioBean.getHorariosDescuentoElec(): " + e);
			throw new AppException("Error al determinar el estatus de Descuento Electronico 24 hrs", e);
		}finally{
			log.info("getHorariosDescuentoElec(S)");
		}
		return productos;
	}

	/**
	*	LLena una Lista de Listas con los productos obtenidos de una consulta por
	*  Id del producto, a dicha lista se le agrega ademas, las validaciones para
	*  ver si existen documentos relacionados a los distintos productos obtenidos
	*  @param idProducto Cadena en la cual se indica el id del producto
	*  que se desea buscar, en caso de ser "" regresa todos los productos.
	*  @return productos Un list de hashmaps con los detalles de los productos
	*  obtenidos en la consulta.
	*/
	public List getProductosById(String idProducto) {

		log.info("getProductosById(E)");
		boolean[] estatusServicio = new boolean[3];
		boolean servicioPymes = false;
		boolean hayDoctosSelPyme = false;
		boolean hayDoctosSelPymeDist 	= false;
		boolean hayDoctosSelIf 	= false;
		boolean haySolicSelIfDist = false;

		String cierrePyme = "";
		String servicioPyme = "";
		String cierreIf = "";
		String servicioIf = "";
		String cierreNafin = "";
		String servicioNafin = "";
		String hayDocSelPyme = "";
		String hayDocSelPymeDist = "";
		String hayDocSelIf = "";
		String hayDocSelIfDist = "";
		String serPymes = "";

		Vector infoProd = null;
		List productos = new ArrayList();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idProducto == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" idProducto=" + idProducto);
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//***************************************************************************************


		try{
			infoProd = this.getInfoProducto(idProducto);
			int totalProductos = infoProd.size();
			boolean[] bcierrepyme 			= new boolean[totalProductos];
			boolean[] bcierrnafin 			= new boolean[totalProductos];
			boolean[] bcierreif 			= new boolean[totalProductos];

			for(int i = 0; i < totalProductos; i++){
				Vector aux = (Vector)infoProd.get(i);
				String rs_producto_nafin = aux.get(0).toString();
				estatusServicio = getEstatusServicio(((Integer)aux.get(0)).intValue());
				bcierrepyme[i] = estatusServicio[0];
				bcierreif[i]	= estatusServicio[1];
				bcierrnafin[i] = estatusServicio[2];
				cierrePyme 		= (bcierrepyme[i])?"S":"N";
				cierreIf 		= (bcierreif[i])?"S":"N";
				cierreNafin 	= (bcierrnafin[i])?"S":"N";
				servicioPyme 	= (bcierrepyme[i])?"Servicio Cerrado":"Servicio Abierto";
				servicioIf 		= (bcierreif[i])?"Servicio Cerrado":"Servicio Abierto";
				servicioNafin 	= (bcierrnafin[i])?"Servicio Cerrado":"Servicio Abierto";

				if(bcierrepyme[i] && !bcierreif[i]){
					if("1".equals(rs_producto_nafin)){
						hayDoctosSelPyme = hayDoctosSelecPyme();
					}
					else if("4".equals(rs_producto_nafin)){
						hayDoctosSelPymeDist = hayDoctosSelecPymeDist();
					}
				}
				else if (bcierrepyme[i] && bcierreif[i] && !bcierrnafin[i]){
					if("1".equals(rs_producto_nafin)){
						hayDoctosSelIf = hayDoctosSelecIF();
					}
					else if("4".equals(rs_producto_nafin)){
						haySolicSelIfDist = haySolicSelecIFDist();
					}
				}

				if(!bcierrepyme[i] && !servicioPymes)
					servicioPymes = true;

				hayDocSelPyme = (hayDoctosSelPyme)?"S":"N";
				hayDocSelPymeDist = (hayDoctosSelPymeDist)?"S":"N";
				hayDocSelIf = (hayDoctosSelIf)?"S":"N";
				hayDocSelIfDist = (haySolicSelIfDist)?"S":"N";
				serPymes = (servicioPymes)?"S":"N";

				HashMap mapProductos = new HashMap();
				mapProductos.put("ID", aux.get(0));
				mapProductos.put("NOM_PRODUCTO", aux.get(1));
				mapProductos.put("CIERRE_PYME", cierrePyme);
				mapProductos.put("CIERRE_IF", cierreIf);
				mapProductos.put("CIERRE_NAFIN", cierreNafin);
				mapProductos.put("SERVICIO_PYME", servicioPyme);
				mapProductos.put("SERVICIO_IF", servicioIf);
				mapProductos.put("SERVICIO_NAFIN", servicioNafin);
				mapProductos.put("HAY_DOC_SEL_PYME", hayDocSelPyme);
				mapProductos.put("HAY_DOC_SEL_PYME_DIST", hayDocSelPymeDist);
				mapProductos.put("HAY_DOC_SEL_IF", hayDocSelIf);
				mapProductos.put("HAY_DOC_SEL_IF_DIST", hayDocSelIfDist);
				mapProductos.put("BTN_CERRAR_ACTIVO", serPymes);

				productos.add(mapProductos);
			}
		}catch(Exception e){
			log.error("Error en ManejoServicioBean.getProductoById(): " + e);
			throw new AppException("Error al determinar el estatus de los productos", e);
		}finally{
			log.info("getProductosById(S)");
		}
		return productos;
	}

	/** Agregado
	 * Descripcion: M�todo que se encarga de realizar una consulta y a partir de los resultado obtenidos
	 *              asigna un valor a las variables locales que seran enviadas en el Map. Ayudar�n a determinar
	 *              los errores en apertura de servicio.
	 * @return Mapa con informaci�n de procesos derivados de la consulta.
	 */
	public Map getValidaciones() {

		log.info("getValidaciones(E)");

		boolean bDesctoElectronico         = true;
		boolean bProcesoVencidosOperar     = true;
		boolean bProcesoVencidosOperarEjec = false;
		String  horaInicioProceso = "";
		HashMap datos = new HashMap();
		AccesoDB con = null;

		try {
			con = new AccesoDB();
			PreparedStatement ps3 = null;
			ResultSet rsDoctos3 = null;
			String descUltimoProceso = "",descPenultimoProceso = "";
			int numReg = 0;

			con.conexionDB();

			String qryProcesoVencApe =
											"SELECT to_char(df_fecha,'HH24:MI') as hora , CG_NOMBRE_PROCESO "+
											"  FROM bit_ejecuta_proceso "+
											" WHERE TO_CHAR (df_fecha, 'dd/mm/yyyy') = TO_CHAR (SYSDATE, 'dd/mm/yyyy') "+
											"   AND cg_nombre_proceso LIKE '%spcom_neg_venc%' "+
											"    order by df_fecha desc, cg_nombre_proceso desc ";

			System.out.println("qryProcesoVencApe:"+qryProcesoVencApe+"*");
			ps3 = con.queryPrecompilado(qryProcesoVencApe);
			rsDoctos3 = ps3.executeQuery();

			if(rsDoctos3.next()){
				descUltimoProceso = (rsDoctos3.getString("CG_NOMBRE_PROCESO")==null)?"":rsDoctos3.getString("CG_NOMBRE_PROCESO");
				//Obteniendo la hora Para el caso en el que no se ha completado el proceso
				horaInicioProceso = (rsDoctos3.getString("hora")==null)?"":rsDoctos3.getString("hora");
				numReg++;
			}

			if(rsDoctos3.next()){
				descPenultimoProceso = (rsDoctos3.getString("CG_NOMBRE_PROCESO")==null)?"":rsDoctos3.getString("CG_NOMBRE_PROCESO");
				numReg++;
			}

			if(numReg == 0)//No se ejecuto el proceso de Vencidos sin operar
				bProcesoVencidosOperar = false;

			rsDoctos3.close();
			if(ps3 != null) ps3.close();

			if( numReg == 0 ){ //No se ha ejecutado el proceso el dia de hoy
				bDesctoElectronico = false;
			}else{
				//El Proceso se esta ejecutando aun o no se completo correctamente
				if( !descUltimoProceso.equals("spcom_neg_venc(S)") || !descPenultimoProceso.equals("spcom_neg_venc(E)") ){
					bDesctoElectronico = false;
					if(descUltimoProceso.equals("spcom_neg_venc(E)")){
						bProcesoVencidosOperarEjec = true;
					}

				}
			}

			datos.put("bProcesoVencidosOperarEjec",""+bProcesoVencidosOperarEjec);
			datos.put("bProcesoVencidosOperar",""+bProcesoVencidosOperar);
			datos.put("bDesctoElectronico",""+bDesctoElectronico);
			datos.put("horaInicioProceso",""+horaInicioProceso);

			return datos;

		} catch (Exception e) {
			log.error("Error en getValidaciones: "+e);
			throw new AppException("Error en consulta para validaciones", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getValidaciones(S)");
		}
	}

	/**
	 * Descripcion: M�todo que se encarga de realizar consultas para determinar d�as inhabiles
	 * @return boolean que indica si el siguiente d�a es h�bil
	 */

	public boolean validarDiaSiguientHabil(int num_dias){
		log.info("validarDiaSiguientHabil(E)");
		String qry_dia_inhabil = "";
		String qry_dia_inhabil_x_anio = "";
		AccesoDB conexion = new AccesoDB();
		Calendar cal = Calendar.getInstance();
		PreparedStatement pstmt = null;
		ResultSet rs_dia_inhabil = null;
		boolean dia_valido = true;

		try	{
			conexion.conexionDB();

			qry_dia_inhabil_x_anio  = "select count(*) as diaInhabil   , 'ManejoServicioEJB::validarDiaSiguientHabil()' as ORIGENQUERY from comcat_dia_inhabil where df_dia_inhabil = TO_DATE(TO_CHAR(sysdate + ?,'dd/mm/yyyy'),'dd/mm/yyyy')";
			qry_dia_inhabil  = "select count(*) as diaInhabil   , 'ManejoServicioEJB::validarDiaSiguientHabil()' as ORIGENQUERY from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate + ?,'dd/mm')";
			pstmt = conexion.queryPrecompilado(qry_dia_inhabil_x_anio);
			pstmt.setInt(1,num_dias);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0) {
					dia_valido=false;
				}
			}

			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			pstmt = conexion.queryPrecompilado(qry_dia_inhabil);
			pstmt.setInt(1,num_dias);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0) {
					dia_valido=false;
				}
			}

			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			log.info("proceso query(BIND):"+qry_dia_inhabil);
			cal.add(Calendar.DATE,num_dias);

			int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
			if (diaSemana == 7 || diaSemana == 1) {
				// Verifica que no sea s�bado o domingo
				dia_valido=false;
				}
		}
		catch (Exception e)	{
			log.error("Error en validarDiaSigHabil : " + e);
			throw new AppException("Error en consulta para validar d�a h�bil", e);
		}
		finally	{
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
		}
		log.info("ValidarDiaSiguientHabil(S)");
		return dia_valido;
	}//cierre metodo



	/**
	 * Descripcion: M�todo que se encarga de realizar consultas para determinar si se encuentran
	 * 				capturadas las tasas para el operar en el d�a h�bil siguiente.
	 * @return boolean que indica true si las tasas del d�a siguiente estan capturadas.
	 */

	public boolean revisarTasasDiaHabilSiguient(){
		log.info("revisarTasasDiaHabilSiguient(E)");
		AccesoDB con  = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "";
		ResultSet rs = null;
		int numTasas = 0;
		int numTasasRequeridas = 0;
		int num_dias =1;
		boolean bandera = true;

		try {
			con.conexionDB();

			while (!validarDiaSiguientHabil(num_dias)){
						num_dias += 1;
			}

			qrySentencia=
				" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::revisarTasasDiaHabilSiguient()'"   +
				"   FROM com_mant_tasa mt, comcat_tasa t"   +
				"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE + ?, 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
				"    AND t.ic_tasa (+) = mt.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,num_dias);
			log.info("QueryTasas : "+qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			rs.next();
			numTasas = rs.getInt("NUMTASAS");
			rs.close();
			if(ps != null) ps.close();

			qrySentencia=
				" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM comcat_tasa"   +
				"  WHERE cs_disponible = 'S'"  ;

			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			rs.next();
			numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
			rs.close();
			if(ps != null) ps.close();

			if (numTasasRequeridas == numTasas) {
				bandera = true;
			} else {
				bandera = false;
			}
		} catch (Exception e) {
			log.error("Error en getValidaciones: "+e);
			throw new AppException("Error en consulta para validaciones", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("revisarTasasDiaHabilSiguient(S)");
		return bandera;
	}


	/**
	 * Descripcion: M�todo que determina si todas las Pymes estan dentro del horario de servicio.
	 * @return boolean que retorna true si todas estan dentro del horario, false si al menos una esta fuera.
	 */

	public boolean fnChkCierre24Hrs (){
		log.info("fnChkCierre24Hrs(E)");
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;

		try	{
			con.conexionDB();
			String lsQryHorarios = "SELECT COUNT(CG_HORARIO_FIN) FROM COMREL_HORARIO_EPO_X_IF WHERE "+
			"TO_DATE(CG_HORARIO_INICIO, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') "+
			"OR TO_DATE (CG_HORARIO_FIN, 'hh24:mi') < TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
			"AND IC_PRODUCTO_NAFIN = 1";

			ResultSet rsHorarios = con.queryDB( lsQryHorarios);

			log.info("QueryfnChkCierre24Hrs: "+ lsQryHorarios );
			if ( rsHorarios.next() ){
				if ( rsHorarios.getInt(1) == 0 )
					lbCerrado = true;
			}

			rsHorarios.close();
			con.cierraStatement();

		} catch (Exception err)	{
			lbCerrado = false;
			log.error("Error en ManejoServicioBean.fnChkCierre24Hrs(): "+err);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("fnChkCierre24Hrs(S)");
		return lbCerrado;
	}


	/**
	 * Descripcion: M�todo que se encarga de realizar consultas para determinar si se encuentran
	 * 				capturadas las tasas para el operar en el d�a h�bil siguiente.
	 * @return boolean que indica true si las tasas del d�a siguiente est�n capturadas.
	 */

	public boolean fnChkCierrePymeDescuentoE()	{
		log.info("fnChkCierrePymeDescuentoE(E)");
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;
		try{
			con.conexionDB();
			String lsQryBaneraCierrePyme = "SELECT cs_cierre_servicio, 'ManejoServicioEJB::fnChkCierrePyme()' FROM comcat_producto_nafin WHERE ic_producto_nafin = 1 ";
			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(lsQryBaneraCierrePyme);
			ResultSet rsCierrePyme = pstmt.executeQuery();

			if ( rsCierrePyme.next() )	{
				if ( rsCierrePyme.getString("cs_cierre_servicio").equals("S") )
					lbCerrado = true;
			}

			rsCierrePyme.close();
			con.cierraStatement();
		}catch(Exception e){
			log.error("Error en ManejoServicioBean.fnChkCierrePymeDescuentoE(): "+e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("fnChkCierrePymeDescuentoE(S)");
		return lbCerrado;
	}//fin metodo


	/**
	 * Descripcion: M�todo que primero determina si las tasas del d�a siguiente est�n capturadas y posteriormente en caso positivo coloca
	 * 				 la bandera de cs_cierre_servicio en N, lo que indicar� que el servicio se ha abierto.
	 * @return boolean que retorna true si la operaci�n se realiz�, false si no se realiz� y adem�s determina que las tasas no est�n capturadas.
	 */

	public boolean abrirServicio24hrs(String[] productos,String cs_cierre_servicio){
		AccesoDB con  = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "";
		ResultSet rs = null;
		int numTasas = 0;
		int numTasasRequeridas = 0;
		int num_dias = 1 ;
		boolean bandera = false;

		try {
			con.conexionDB();
						while (!validarDiaSiguientHabil(num_dias)){
						num_dias += 1;
			}
			qrySentencia=
				" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM com_mant_tasa mt, comcat_tasa t"   +
				"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE + ? , 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
				"    AND t.ic_tasa (+) = mt.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,num_dias);
			log.info("QueryAbrirServicio24hrs : "+qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();

			if(rs.next())
				numTasas = rs.getInt("NUMTASAS");
			rs.close();
			if(ps != null) ps.close();
			qrySentencia=
				" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
				"   FROM comcat_tasa"   +
				"  WHERE cs_disponible = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next())
				numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
			rs.close();
			if(ps != null) ps.close();
			if (numTasasRequeridas == numTasas) {
				setEstatusServicioPyme(productos,cs_cierre_servicio);	//Apertura de servicio
				bandera = true;
			} else {
				bandera = false;
			}
		} catch (AppException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error en abrirServicio24: "+e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return bandera;
	}



	/*********************************************************************************************************************
	*	Verifica los horarios pyme para determinar si al menos uno se encuentre abierto el servicio
	*********************************************************************************************************************/

	public boolean fnChkCierrePyme (int liProducto)	throws NafinException 	{
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;
		try	{
			con.conexionDB();
			String lsQryNoHrPymesActivos = "" +
				"SELECT COUNT(cg_horario_inicio) " +
				"    FROM(SELECT cg_horario_inicio, cg_horario_fin FROM comrel_horario_x_epo " +
				"            WHERE TO_DATE(cg_horario_inicio, 'hh24:mi') <= TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"                AND TO_DATE(cg_horario_fin, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"                AND ic_producto_nafin = " + liProducto +
				"         UNION ALL " +
				"         SELECT cg_horario_inicio, cg_horario_fin FROM comcat_producto_nafin " +
				"            WHERE TO_DATE(cg_horario_inicio, 'hh24:mi') <= TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"	            AND TO_DATE(cg_horario_fin, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"   	         AND ic_producto_nafin = " + liProducto + ")";
			ResultSet rsHorarios = con.queryDB( lsQryNoHrPymesActivos );
			if ( rsHorarios.next() )	{
				if ( rsHorarios.getInt(1) == 0 )
					lbCerrado = true;
				else	{

					String lsQryBaneraCierrePyme = "SELECT cs_cierre_servicio, 'ManejoServicioEJB::fnChkCierrePyme()' FROM comcat_producto_nafin WHERE ic_producto_nafin = ? ";
					PreparedStatement pstmt = null;
					pstmt = con.queryPrecompilado(lsQryBaneraCierrePyme);
					pstmt.setInt(1,liProducto);
					ResultSet rsCierrePyme = pstmt.executeQuery();
					pstmt.clearParameters();

					/*String lsQryBaneraCierrePyme = "SELECT cs_cierre_servicio FROM comcat_producto_nafin WHERE ic_producto_nafin = " + liProducto;
					ResultSet rsCierrePyme = con.queryDB( lsQryBaneraCierrePyme );
					*/

					if ( rsCierrePyme.next() )	{
						if ( rsCierrePyme.getString("cs_cierre_servicio").equals("S") )
							lbCerrado = true;
					}
					rsCierrePyme.close();
					con.cierraStatement();
				}
			}
			rsHorarios.close();
			con.cierraStatement();
		} catch (Exception err)	{
			lbCerrado = false;
			System.out.print("\nError en el metodo ManejoServicioBean.fnChkCierrePyme().\n" + err );
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return lbCerrado;
	}


	/*********************************************************************************************************************
	*	Verifica que al menos un IF continue abierto su servicio
	*********************************************************************************************************************/

	public boolean fnChkCierreIF()	throws NafinException 	{
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;
		try	{
			con.conexionDB();
			String lsQryNoHrIFsActivos = "" +
				"SELECT COUNT(cg_horario_inicio_if)  " +
				"    FROM(SELECT cg_horario_inicio_if, cg_horario_fin_if FROM comrel_horario_x_epo " +
				"            WHERE TO_DATE(cg_horario_inicio_if, 'hh24:mi') <= TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"                AND TO_DATE(cg_horario_fin_if, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"         UNION ALL " +
				"         SELECT cg_horario_inicio_if, cg_horario_fin_if FROM comcat_producto_nafin " +
				"            WHERE TO_DATE(cg_horario_inicio_if, 'hh24:mi') <= TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
				"	            AND TO_DATE(cg_horario_fin_if, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') )";
			ResultSet rsHorarios = con.queryDB( lsQryNoHrIFsActivos );
			if ( rsHorarios.next() )	{
				if ( rsHorarios.getInt(1) == 0 )	lbCerrado = true;
				if ( lbCerrado )	{
					/****** CHECANDO SI HAY DOCUMENTOS PENDIENTES ******/
					String lsQryDoctos = "" +
						"SELECT SUM(doctos) FROM " +
						"	( SELECT COUNT(1) AS doctos FROM com_documento WHERE ic_estatus_docto = 3 " +
						"     UNION ALL " +
						"	  SELECT COUNT(1) AS doctos FROM com_pedido WHERE ic_estatus_pedido = 3 " +
			            "     UNION ALL " +
			            "	  SELECT COUNT(1) AS doctos FROM inv_disposicion WHERE ic_estatus_disposicion = 1 " +
			            "     UNION ALL " +
			            "	  SELECT COUNT(1) AS doctos FROM dis_documento WHERE ic_estatus_docto = 3 " +
						"    )";
						ResultSet rsDoctos = con.queryDB( lsQryDoctos );
						if ( rsDoctos.next() )	{
							if ( rsDoctos.getInt(1) > 0 )	lbCerrado = false;
						}
						rsDoctos.close();
						con.cierraStatement();
				}
			}
			rsHorarios.close();
			con.cierraStatement();
		} catch (Exception err)	{
			lbCerrado = false;
			System.out.print("\nError en el metodo ManejoServicioBean.fnChkCierreIF().\n" + err);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return lbCerrado;
	}

	private boolean fnChkCierreIF(int ic_producto_nafin)	throws NafinException 	{
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;
		try	{
			con.conexionDB();
			String lsQryNoHrIFsActivos =
				" SELECT COUNT (cg_horario_inicio_if)"   +
				"   FROM (SELECT cg_horario_inicio_if, cg_horario_fin_if"   +
				"           FROM comrel_horario_x_epo"   +
				"          WHERE TO_DATE (cg_horario_inicio_if, 'hh24:mi') <= TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"            AND TO_DATE (cg_horario_fin_if, 'hh24:mi') > TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"            AND ic_producto_nafin = "+ic_producto_nafin+" "   +
				"         UNION ALL"   +
				"         SELECT cg_horario_inicio_if, cg_horario_fin_if"   +
				"           FROM comcat_producto_nafin"   +
				"          WHERE TO_DATE (cg_horario_inicio_if, 'hh24:mi') <= TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"            AND TO_DATE (cg_horario_fin_if, 'hh24:mi') > TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"            AND ic_producto_nafin = "+ic_producto_nafin+")"  ;
			ResultSet rsHorarios = con.queryDB( lsQryNoHrIFsActivos );
			if ( rsHorarios.next() )	{
				if ( rsHorarios.getInt(1) == 0 )	lbCerrado = true;
				if ( lbCerrado )	{
					/****** CHECANDO SI HAY DOCUMENTOS PENDIENTES ******/
					String lsquery = "";
					if(ic_producto_nafin == 1)
						lsquery = " SELECT COUNT(1) AS doctos FROM com_documento WHERE ic_estatus_docto = 3 ";
					if(ic_producto_nafin == 2){
						if(!"".equals(lsquery))
							lsquery += " UNION ALL ";
						lsquery += " SELECT COUNT(1) AS doctos FROM com_pedido WHERE ic_estatus_pedido = 3 ";
					}
					if(ic_producto_nafin == 5){
						if(!"".equals(lsquery))
							lsquery += " UNION ALL ";
						lsquery += " SELECT COUNT(1) AS doctos FROM inv_disposicion WHERE ic_estatus_disposicion = 1 ";
					}
					if(ic_producto_nafin == 4){
						if(!"".equals(lsquery))
							lsquery += " UNION ALL ";
						lsquery += " SELECT COUNT(1) AS doctos FROM dis_documento WHERE ic_estatus_docto = 3 ";
					}

					String lsQryDoctos = "" +
						"SELECT SUM(doctos) FROM " +
						"	( "+lsquery+" )";
					ResultSet rsDoctos = con.queryDB( lsQryDoctos );
					if ( rsDoctos.next() )	{
						if ( rsDoctos.getInt(1) > 0 )	lbCerrado = false;
					}
					rsDoctos.close();
					con.cierraStatement();
				}
			}
			rsHorarios.close();
			con.cierraStatement();
		} catch (Exception err)	{
			lbCerrado = false;
			System.out.print("\nError en el metodo ManejoServicioBean.fnChkCierreIF().\n" + err);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return lbCerrado;
	}





	/*********************************************************************************************************************
	*	Verifica los horarios pyme para determinar si al menos uno se encuentre abierto el servicio (Obsoleto usar metodo getEstatusServicio() )
	*********************************************************************************************************************/

	public boolean fnChAperturaPyme (int liProducto)	throws NafinException{
		boolean  lbAbierto = false;
		return lbAbierto;
	}




	/*********************************************************************************************************************
	*	Verifica que al menos un IF continue abierto su servicio 	(Obsoleto usar metodo getEstatusServicio() )
	*********************************************************************************************************************/

	public boolean fnChAperturaIF ()	throws NafinException{
		boolean  lbAbierto = false;
		return lbAbierto;
	}



	/*********************************************************************************************************************
	*	Verifica que ya se encuentre abierto el servicio para NAFIN ( Obsoleto usar metodo getEstatusServicio() )
	*********************************************************************************************************************/

	public boolean fnChAperturaNafin() throws NafinException	{
		boolean  lbCerrado = false;
		return lbCerrado;
	}




	/***************************************************************************
	*															METODOS PRIVADOS
	****************************************************************************/

	private boolean fnChkCierreNafin(int ic_producto_nafin)	throws NafinException 	{
		AccesoDB con = new AccesoDB();
		boolean  lbCerrado = false;
		PreparedStatement ps = null;
		ResultSet rsDoctos = null;

		try	{
			con.conexionDB();
			String lsQryNoHrNafinActivos =
				" SELECT COUNT (cg_horario_inicio_nafin)"   +
				"   FROM comcat_producto_nafin"   +
				"  WHERE TO_DATE (cg_horario_inicio_nafin, 'hh24:mi') <= TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"    AND TO_DATE (cg_horario_fin_nafin, 'hh24:mi') > TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')" +
				"	 AND ic_producto_nafin = "+ic_producto_nafin+" ";

			ResultSet rsHorarios = con.queryDB( lsQryNoHrNafinActivos );
			if ( rsHorarios.next() )	{
				if ( rsHorarios.getInt(1) == 0 )
        			lbCerrado = true;
				if (lbCerrado)	{
					String lsquery = "";
					switch(ic_producto_nafin) {
						case 1:
							lsquery =
								" SELECT COUNT (s.ic_folio) AS numero"   +
								"   FROM comcat_producto_nafin pn, com_solicitud s"   +
								"  WHERE pn.ic_producto_nafin = 1"   +
								"    AND s.ic_estatus_solic = 1"   +
								"    AND s.ic_bloqueo = 1"   +
								"    AND TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy ') || pn.cg_horario_fin_nafin, 'dd/mm/yyyy hh24:mi') < SYSDATE"  ;
							break;
						case 2:
							lsquery =
								" SELECT COUNT (a.ic_pedido) AS numero"   +
								"   FROM comcat_producto_nafin pn, com_anticipo a"   +
								"  WHERE pn.ic_producto_nafin = 2"   +
								"    AND a.ic_estatus_antic = 1"   +
								"    AND a.ic_bloqueo = 1"   +
								"    AND TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy ') || pn.cg_horario_fin_nafin, 'dd/mm/yyyy hh24:mi') < SYSDATE"  ;
							break;
						case 5:
							lsquery =
								" SELECT COUNT (a.cc_disposicion) AS numero"   +
								"   FROM comcat_producto_nafin pn, inv_solicitud a"   +
								"  WHERE pn.ic_producto_nafin = 5"   +
								"    AND a.ic_estatus_solic = 1"   +
								"    AND TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy ') || pn.cg_horario_fin_nafin, 'dd/mm/yyyy hh24:mi') < SYSDATE"  ;
							break;
						case 4:
							lsquery =
								" SELECT COUNT (s.ic_documento) AS numero"   +
								"   FROM comcat_producto_nafin pn, dis_solicitud s"   +
								"  WHERE pn.ic_producto_nafin = 4"   +
								"    AND s.ic_estatus_solic = 1"   +
								"    AND TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yyyy ') || pn.cg_horario_fin_nafin, 'dd/mm/yyyy hh24:mi') < SYSDATE"  ;
							break;

					}
					String lsQryDoctos =
						"SELECT SUM(numero), 'ManejoServicioEJB:fnChkCierreNafin()' " +
						"FROM ( "+lsquery+" )" ;
					//System.out.print("lsQryDoctos::: "+lsQryDoctos);
					ps = con.queryPrecompilado(lsQryDoctos);
            		rsDoctos = ps.executeQuery();
					//ResultSet rsDoctos = con.queryDB( lsQryDoctos );
					if ( rsDoctos.next() ) {
          				if( rsDoctos.getInt(1) > 0 )
            			lbCerrado = false;
					}
					if(ps != null) ps.close();
					//rsDoctos.close();con.cierraStatement();
				 }
			}
			rsHorarios.close();
			con.cierraStatement();
		} catch (Exception err)	{
			lbCerrado = false;
			System.out.print("\nError en el metodo ManejoServicioBean.fnChkCierreNafin().\n" + err );
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return lbCerrado;
	}

	//METODOS AGREGADOS POR EGB FODA 050-2004

	public ArrayList getHorariosxProducto(String lsIcProducto) throws NafinException {
		ArrayList oalHorarios = new ArrayList();
		AccesoDB con = new AccesoDB();
		try {
			System.out.print("ManejoServicioBean.getHorariosxProducto(E)" );
			String condicion = "";

			con.conexionDB();

			if("T".equals(lsIcProducto))
				condicion = " WHERE ic_producto_nafin in (1,4) ";
			else
				condicion = " WHERE ic_producto_nafin in (1,2,4,5) ";

			String qryHorariosNafin  = "SELECT ic_producto_nafin, ic_nombre, cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if, cg_horario_inicio_nafin, " +
				"cg_horario_fin_nafin,'ManejoServicioBean.getHorariosxProducto()'  FROM comcat_producto_nafin  " +condicion+
				"	and cg_horario_inicio is not null" +
				"	and cg_horario_fin is not null" +
				"	and cg_horario_inicio_if is not null" +
				"	and cg_horario_fin_if is not null";
			if (!lsIcProducto.equals("") && !"T".equals(lsIcProducto)) {
				qryHorariosNafin += " and ic_producto_nafin = " + lsIcProducto;
			}

			PreparedStatement psHorarios = con.queryPrecompilado(qryHorariosNafin);
			ResultSet rsHorarios = psHorarios.executeQuery();
			Vector vDatos = null;
			while (rsHorarios.next())	{
				vDatos = new Vector();
				vDatos.add(0, rsHorarios.getString("ic_producto_nafin"));
				vDatos.add(1, rsHorarios.getString("ic_nombre"));
				vDatos.add(2, rsHorarios.getString("cg_horario_inicio"));
				vDatos.add(3, rsHorarios.getString("cg_horario_fin"));
				vDatos.add(4, rsHorarios.getString("cg_horario_inicio_if"));
				vDatos.add(5, rsHorarios.getString("cg_horario_fin_if"));
				vDatos.add(6, rsHorarios.getString("cg_horario_inicio_nafin"));
				vDatos.add(7, rsHorarios.getString("cg_horario_fin_nafin"));
				oalHorarios.add(vDatos);
			}
		} catch (Exception err)	{
			System.out.print("Error en el metodo ManejoServicioBean.getHorariosxProducto(Exception)" + err );
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.print("ManejoServicioBean.getHorariosxProducto(S)" );
		}
		return oalHorarios;
	}


	public void setHorariosGenerales(String sProducto, String sAPymeM, String sCPymeM, String sAIFM,
			String sCIFM, String sANafinM, String sCNafinM ) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		try {
			System.out.print("ManejoServicioBean.setHorariosGenerales(E)" );
			con.conexionDB();

			ArrayList loHorarios = getHorariosxProducto(sProducto);

			String lsAPyme = "", lsCPyme = "", lsAIF = "", lsCIF = "";
			Vector vDatos = (Vector) loHorarios.get(0);
			lsAPyme = vDatos.get(2).toString();
			lsCPyme = vDatos.get(3).toString();
			lsAIF   = vDatos.get(4).toString();
			lsCIF   = vDatos.get(5).toString();

			StringBuffer sbSql = new StringBuffer();
			sbSql.append("UPDATE comcat_producto_nafin " );
			sbSql.append(" SET cg_horario_inicio = ?, cg_horario_fin = ? , cg_horario_inicio_if = ?, ");
			sbSql.append(" cg_horario_fin_if = ?, cg_horario_inicio_nafin = ?, cg_horario_fin_nafin = ?");
			sbSql.append(" WHERE ic_producto_nafin = ?");

			//System.out.println(sbSql.toString());
			PreparedStatement psUpdate = con.queryPrecompilado(sbSql.toString());

			psUpdate.setString(1, sAPymeM);
			psUpdate.setString(2, sCPymeM);
			psUpdate.setString(3, sAIFM);
			psUpdate.setString(4, sCIFM);
			psUpdate.setString(5, sANafinM);
			psUpdate.setString(6, sCNafinM);
			psUpdate.setInt(7, Integer.parseInt(sProducto));
			try {
				psUpdate.execute();

				psUpdate.clearParameters();
				//psUpdate.close();
				sbSql.delete(0, sbSql.length());

				sbSql.append("UPDATE comrel_horario_x_epo SET " );
				sbSql.append("	cg_horario_inicio    = ?" );
				sbSql.append("	, cg_horario_fin       = ?" );
				sbSql.append("  , cg_horario_inicio_if = ?" );
				sbSql.append("  , cg_horario_fin_if    = ?" );
				sbSql.append("  , cs_especial    = 'N' " );
				sbSql.append(" WHERE " );
				sbSql.append("	cg_horario_inicio    	 = ?" );
				sbSql.append("	and cg_horario_fin       = ?" );
				sbSql.append("  and cg_horario_inicio_if = ?" );
				sbSql.append("  and cg_horario_fin_if    = ?" );
				sbSql.append("  and ic_producto_nafin 	 = ?" );
				//System.out.println(sbSql.toString());

				psUpdate = con.queryPrecompilado(sbSql.toString());

				psUpdate.setString(1, sAPymeM);
				psUpdate.setString(2, sCPymeM);
				psUpdate.setString(3, sAIFM);
				psUpdate.setString(4, sCIFM);
				psUpdate.setString(5, lsAPyme);
				psUpdate.setString(6, lsCPyme);
				psUpdate.setString(7, lsAIF);
				psUpdate.setString(8, lsCIF);
				psUpdate.setInt(9, Integer.parseInt(sProducto));

				try {
					psUpdate.execute();
				}catch(SQLException sqle){
					System.out.println(sqle);
					bOk = false;
					throw new NafinException("HORA0002");
				}

				psUpdate.clearParameters();
				//psUpdate.close();
				sbSql.delete(0, sbSql.length());

				sbSql.append("UPDATE comrel_horario_epo_x_if SET " );
				sbSql.append("	cg_horario_inicio    = ?" );
				sbSql.append("	, cg_horario_fin       = ?" );
				sbSql.append("  , cg_horario_inicio_if = ?" );
				sbSql.append("  , cg_horario_fin_if    = ?" );
				sbSql.append("  , cs_especial    = ? " );
				sbSql.append(" WHERE " );
				sbSql.append("	cs_especial = ? " );
				sbSql.append("  and ic_producto_nafin 	 = ? " );
				//System.out.println(sbSql.toString());

				psUpdate = con.queryPrecompilado(sbSql.toString());

				psUpdate.setString(1, sAPymeM);
				psUpdate.setString(2, sCPymeM);
				psUpdate.setString(3, sAIFM);
				psUpdate.setString(4, sCIFM);
				psUpdate.setString(5, "N");
				psUpdate.setString(6, "N");
				psUpdate.setInt(7, Integer.parseInt(sProducto));

				try {
					psUpdate.execute();
				}catch(SQLException sqle){
					System.out.println(sqle);
					bOk = false;
					throw new NafinException("HORA0002");
				}


			} catch (SQLException sqle){
				System.out.println(sqle);
				bOk = false;
				throw new NafinException("HORA0001");
			}

			if(psUpdate!=null) psUpdate.close();

		} catch (NafinException ne)	{
			System.out.print("Error en el metodo ManejoServicioBean.setHorariosGenerales(NafinException)" + ne.getMsgError() );
			throw ne;
		} catch (Exception err)	{
			System.out.print("Error en el metodo ManejoServicioBean.setHorariosGenerales(Exception)" + err );
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.print("ManejoServicioBean.setHorariosGenerales(S)" );
		}
	}


	public ArrayList getHorariosxProductoEpo(String lsIcProducto, String lsIcEpo) throws NafinException {
		ArrayList oalHorarios = new ArrayList();
		AccesoDB con = new AccesoDB();
		try {
			System.out.print("ManejoServicioBean.getHorariosxProductoEpo(E)" );

			con.conexionDB();
			String condicion = "";
			if("T".equals(lsIcProducto))
				condicion = " WHERE he.ic_producto_nafin in (4) ";
			else
				condicion = " WHERE he.ic_producto_nafin in (1,2,4,5) ";

			String qryHorarios  = "SELECT he.ic_producto_nafin, pn.ic_nombre, e.ic_epo, e.cg_razon_social, he.cg_horario_inicio, he.cg_horario_fin, he.cg_horario_inicio_if, he.cg_horario_fin_if, 'ManejoServicioBean.getHorariosxProductoEpo()' " +
				" FROM comrel_horario_x_epo he, comcat_epo e, comcat_producto_nafin pn " + condicion+
				" and he.ic_epo = e.ic_epo and he.ic_producto_nafin = pn.ic_producto_nafin ";
			if (!lsIcProducto.equals("") && !"T".equals(lsIcProducto)) {
				qryHorarios += " and he.ic_producto_nafin = " + lsIcProducto;
			}
			if (lsIcEpo.equals("T")) {
				qryHorarios += " and he.cs_especial = 'S'";
			} else if (!lsIcEpo.equals("")) {
				qryHorarios += " and he.ic_epo = " + lsIcEpo;
			}

			qryHorarios += " Order by e.ic_epo, he.ic_producto_nafin ";

			System.out.println("QUERY " + qryHorarios);
			PreparedStatement psHorarios = con.queryPrecompilado(qryHorarios);
			ResultSet rsHorarios = psHorarios.executeQuery();
			Vector vDatos = null;
			while (rsHorarios.next())	{
				vDatos = new Vector();
				vDatos.add(0, rsHorarios.getString("ic_producto_nafin"));
				vDatos.add(1, rsHorarios.getString("ic_nombre"));
				vDatos.add(2, rsHorarios.getString("ic_epo"));
				vDatos.add(3, rsHorarios.getString("cg_razon_social"));
				vDatos.add(4, rsHorarios.getString("cg_horario_inicio"));
				vDatos.add(5, rsHorarios.getString("cg_horario_fin"));
				vDatos.add(6, rsHorarios.getString("cg_horario_inicio_if"));
				vDatos.add(7, rsHorarios.getString("cg_horario_fin_if"));
				oalHorarios.add(vDatos);
			}
		} catch (Exception err)	{
			System.out.print("Error en el metodo ManejoServicioBean.getHorariosxProductoEpo(Exception)" + err );
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.print("ManejoServicioBean.getHorariosxProductoEpo(S)" );
		}
		return oalHorarios;
	}


	public void setHorariosXEpo(String sProducto, String sAPymeM, String sCPymeM, String sAIFM,
			String sCIFM, String sIcEpo, String sCsEspecial) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		try {
			System.out.print("ManejoServicioBean.setHorariosXEpo(E)" );
			con.conexionDB();
			if (sCsEspecial.equals("N")) {
				ArrayList loHorarios = getHorariosxProducto(sProducto);

				Vector vDatos = (Vector) loHorarios.get(0);
				sAPymeM = vDatos.get(2).toString();
				sCPymeM = vDatos.get(3).toString();
				sAIFM   = vDatos.get(4).toString();
				sCIFM   = vDatos.get(5).toString();
			}

			StringBuffer sbSql = new StringBuffer();
			sbSql.append("UPDATE comrel_horario_x_epo " );
			sbSql.append(" SET cg_horario_inicio = ?, cg_horario_fin = ? , cg_horario_inicio_if = ?, cg_horario_fin_if = ? , cs_especial = ? ");
			sbSql.append(" WHERE ic_producto_nafin = ? and ic_epo = ? ");

			System.out.println(sbSql.toString());
			PreparedStatement psUpdate = con.queryPrecompilado(sbSql.toString());

			psUpdate.setString(1, sAPymeM);
			psUpdate.setString(2, sCPymeM);
			psUpdate.setString(3, sAIFM);
			psUpdate.setString(4, sCIFM);
			psUpdate.setString(5, sCsEspecial);
			psUpdate.setInt(6, Integer.parseInt(sProducto));
			psUpdate.setInt(7, Integer.parseInt(sIcEpo));
			try {
				psUpdate.execute();

			} catch (SQLException sqle){
				System.out.println(sqle);
				bOk = false;
				throw new NafinException("HORA0003");
			}

		} catch (NafinException ne)	{
			System.out.print("Error en el metodo ManejoServicioBean.setHorariosXEpo(NafinException)" + ne.getMsgError() );
			throw ne;
		} catch (Exception err)	{
			System.out.print("Error en el metodo ManejoServicioBean.setHorariosXEpo(Exception)" + err );
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.print("ManejoServicioBean.setHorariosXEpo(S)" );
		}
	}

	/* M�todo usado para obtener los horarios de apertura y cierre de servicio
	 * de cada intermediario para Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public ArrayList getHorarioPorIfCE(String sNoIF) throws NafinException {
	AccesoDB con = new AccesoDB();
	ArrayList alHorarioPorIf = new ArrayList();
		try {
			con.conexionDB();

			String sQuery = " select I.ic_if as noIF, I.cg_razon_social as nombreIF, "+
							" HI.cg_horario_inicio as horaInicio, HI.cg_horario_fin as horaFinal "+
							" from comrel_horario_x_if HI, comcat_if I "+
							" where I.ic_if = HI.ic_if(+) "+
							(sNoIF.equals("T")||sNoIF.equals("")?" and HI.cg_horario_inicio is not null and HI.cg_horario_fin is not null ":" and I.ic_if(+) = ? ")+
							" order by I.cg_razon_social ";
			try {
				PreparedStatement ps = con.queryPrecompilado(sQuery);
				if(!sNoIF.equals("T") && !sNoIF.equals(""))
					ps.setString(1, sNoIF);

				ResultSet rs = ps.executeQuery();
				Vector vDatos = null;
				while (rs.next())	{
					vDatos = new Vector();
					vDatos.add(0, rs.getString("noIF"));
					vDatos.add(1, rs.getString("nombreIF"));
					vDatos.add(2, (rs.getString("horaInicio")==null?"":rs.getString("horaInicio")));
					vDatos.add(3, (rs.getString("horaFinal")==null?"":rs.getString("horaFinal")));
					alHorarioPorIf.add(alHorarioPorIf.size(), vDatos);
				}
				rs.close();
				if(ps!=null) ps.close();
			} catch (SQLException sqle){
				System.out.println(sqle);
				throw new NafinException("HORA0004");
			}
		} catch (Exception e)	{
			System.out.print("Error en el metodo ManejoServicioBean.getHorarioPorIfCE()" + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alHorarioPorIf;
	}

	/* M�todo usado para obtener los horarios de apertura y cierre de servicio
	 * general de los intermediarios para Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public ArrayList getHorarioIfGralCE() throws NafinException {
	AccesoDB con = new AccesoDB();
	ArrayList alHorarioGralIf = new ArrayList();
		try {
			con.conexionDB();

			String sQuery = " select cg_horario_inicio as horaInicio, "+
							" cg_horario_fin as horaFinal "+
							" from com_param_gral ";
			try {
				PreparedStatement ps = con.queryPrecompilado(sQuery);
				ResultSet rs = ps.executeQuery();
				while (rs.next())	{
					alHorarioGralIf.add(0, (rs.getString("horaInicio")==null?"":rs.getString("horaInicio")));
					alHorarioGralIf.add(1, (rs.getString("horaFinal")==null?"":rs.getString("horaFinal")));
				}
				rs.close();
				if(ps!=null) ps.close();
			} catch (SQLException sqle){
				System.out.println(sqle);
				throw new NafinException("HORA0005");
			}
		} catch (Exception e)	{
			System.out.print("Error en el metodo ManejoServicioBean.getHorarioIfGralCE()" + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alHorarioGralIf;
	}

	/* M�todo usado para guardar los horarios de apertura y cierre de servicio.
	 * general de los intermediarios para Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public void setHorarioGralCE(String sHoraInicioIF, String sHoraFinalIF) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOk = true;
		try {
			con.conexionDB();

			String sQuery = " update com_param_gral "+
							" set cg_horario_inicio = ?, "+
							" cg_horario_fin = ? ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sHoraInicioIF);
			ps.setString(2, sHoraFinalIF);
			try {
				ps.execute();

			} catch (SQLException sqle){
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("HORA0006");
			}
			if(ps!=null) ps.close();

		} catch (NafinException ne)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setHorarioGralCE()" + ne.getMsgError());
			throw ne;
		} catch (Exception e)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setHorarioGralCE()" + e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/* M�todo usado para guardar los horarios de apertura y cierre de servicio.
	 * de cada intermediario para Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public void setHorarioPorIfCE(ArrayList alHorariosIf) throws NafinException {
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	String sQuery = "";
	boolean bOk = true;
		try {
			con.conexionDB();

			for(int i=0; i<alHorariosIf.size(); i++) {
				Vector vecHorario = (Vector)alHorariosIf.get(i);

				try {
					sQuery = "select count(1) from comrel_horario_x_if where ic_if = ? ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, vecHorario.get(0).toString());
					ResultSet rs = ps.executeQuery();
					if(rs.next()) {
						if(rs.getInt(1) == 0) {
							sQuery =" insert into comrel_horario_x_if(cg_horario_inicio, "+
									" cg_horario_fin, ic_if) values(?, ?, ?) ";
						} else {
							sQuery =" update comrel_horario_x_if "+
									" set cg_horario_inicio = ?, "+
									" cg_horario_fin = ? "+
									" where ic_if = ? ";
						}
						ps = con.queryPrecompilado(sQuery);
						ps.setString(1, vecHorario.get(1).toString());
						ps.setString(2, vecHorario.get(2).toString());
						ps.setString(3, vecHorario.get(0).toString());

						ps.execute();
					}
				} catch (SQLException sqle) {
					bOk = false;
					sqle.printStackTrace();
					throw new NafinException("HORA0007");
				}

			} // for
			if(ps!=null) ps.close();

		} catch (NafinException ne)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setHorarioPorIfCE()" + ne.getMsgError());
			throw ne;
		} catch (Exception e)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setHorarioPorIfCE()" + e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/* M�todo usado para eliminar el horario de apertura y cierre de servicio.
	 * del intermediario para Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public void setBorrarHorarioPorIf(String sNoIf) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOk = true;
		try {
			con.conexionDB();

			String sQuery = " delete comrel_horario_x_if "+
							" where ic_if = ? ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoIf);
			try {
				ps.execute();

			} catch (SQLException sqle){
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("HORA0008");
			}
			if(ps!=null) ps.close();

		} catch (NafinException ne)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setBorrarHorarioPorIf()" + ne.getMsgError());
			throw ne;
		} catch (Exception e)	{
			bOk = false;
			System.out.print("Error en el metodo ManejoServicioBean.setBorrarHorarioPorIf()" + e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/* M�todo usado para indicar si el horario esta abierto o cerrado
	 * para el IF o en general para los intermediarios de Cr�dito Electr�nico.
	 * Realizado por Hugoro Foda 1 del 2005	*/
	public void getEstadoDelServicioIfCE(String sNoIf) throws NafinException {
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select cg_horario_inicio, cg_horario_fin "+
							" from comrel_horario_x_if "+
							" where ic_if = ? "+
							" union all "+
							" select cg_horario_inicio, cg_horario_fin "+
							" from com_param_gral ";
			try {
				PreparedStatement ps = con.queryPrecompilado(sQuery);
				ps.setString(1, sNoIf);
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					sQuery =" SELECT decode(count(1), 0, 'Cerrado', 'Abierto') as servicio "+
							" FROM DUAL "+
							" WHERE TO_NUMBER(TO_CHAR(SYSDATE, 'HH24MI')) "+
							" BETWEEN TO_NUMBER( ? ) "+
							" AND TO_NUMBER( ? ) ";
					ps = con.queryPrecompilado(sQuery);
					String sHoraIni = rs.getString("cg_horario_inicio");
					String sHoraFin = rs.getString("cg_horario_fin");
					ps.setString(1, sHoraIni.substring(0,2)+""+sHoraIni.substring(3,5));
					ps.setString(2, sHoraFin.substring(0,2)+""+sHoraFin.substring(3,5));
					rs = ps.executeQuery();
					if(rs.next()) {
						if(rs.getString(1).equals("Cerrado"))
							throw new NafinException("DSCT0048");
					}

				} else
					throw new NafinException("DSCT0048");
				rs.close();
				if(ps!=null) ps.close();

			} catch (SQLException sqle) {
				sqle.printStackTrace();
				System.out.println(sqle);
				throw new NafinException("HORA0009");
			}
		} catch (NafinException ne)	{
			System.out.print("Error en el metodo ManejoServicioBean.getEstadoDelServicioIfCE()" + ne.getMsgError());
			throw ne;
		} catch (Exception e)	{
			e.printStackTrace();
			System.out.print("Error en el metodo ManejoServicioBean.getEstadoDelServicioIfCE()" + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/**
	 * M�todo que se encarga de regresar el monto comprometido al monto limite utilizado
	 * que se emplea cuando una pyme selecciona documentos fuera del horario de servicio,
	 * cambiando el estatus de los documentos de Programado Pyme a Seleccionado Pyme.
	 * @throws AppException Cuando ocurre un error al completar la actualizaci�n.
	 * @author Alberto Cruz Flores
	 * @since FODEA 005 - 2009 Factoraje 24h
	 */
	public void cambiarEstatusDoctosFact24h() throws AppException {
		log.info("ManejoServicioBean::cambiarEstatusDoctosFact24h(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		boolean trans_op = true;
		String qrySentencia ="";
		ResultSet rs 					= null;
    PreparedStatement ps = null;
		String icEpo = "";
		String icDocumento ="";
		String exception = "";
		String cc_acuse = "";

		try{
			con.conexionDB();


		qrySentencia=" SELECT cc_acuse, ic_epo,ic_documento  from COM_DOCUMENTO " +
								 " WHERE ic_estatus_docto = 26  " +
								 " order by ic_epo ";

			log.debug(" qrySentencia::: "+qrySentencia);


			ps = con.queryPrecompilado(qrySentencia);
      rs = ps.executeQuery();


      while(rs.next()) {

					 icEpo         = rs.getString("ic_epo");
					 icDocumento =   rs.getString("ic_documento");
					 cc_acuse  = rs.getString("cc_acuse");

				  log.trace("..:: icEpo : "+icEpo);
					log.trace("..:: icDocumento : "+icDocumento);


						try{
							log.debug(" Cuando  al cambiar el estatus a Seleccionado Pyme::: ");

						  strSQL = new StringBuffer();
							//cambia de estatus
							strSQL.append(" UPDATE com_documento");
							strSQL.append(" SET ic_estatus_docto = 3"); //seleccionado pyme
							strSQL.append(" WHERE ic_estatus_docto = 26"); //programado pyme
							strSQL.append(" AND ic_EPO = "+icEpo);
							strSQL.append(" AND ic_documento = "+icDocumento);


							log.debug("..:: strSQL : "+strSQL.toString());


							pst = con.queryPrecompilado(strSQL.toString());
							pst.executeUpdate();
							pst.close();

							// Inserta en la bitacora de cambios de estatus
							 // como (32) Programado Pyme a Seleccionado Pyme
							strSQL = new StringBuffer();
							strSQL.append(" insert into comhis_cambio_estatus");
							strSQL.append(" (dc_fecha_cambio, ic_documento, ic_cambio_estatus, ct_cambio_motivo , cg_nombre_usuario)");
							strSQL.append(" values(sysdate,"+icDocumento+" ,32,'Apertura del servicio','Proceso autom�tico Manejo Servicio')");

							log.debug("..:: strSQL : "+strSQL.toString());

							pst = con.queryPrecompilado(strSQL.toString());
							pst.executeUpdate();
							pst.close();





					} catch(Exception e) {

						log.error("Cuando hay error al cambiar el estatus a Seleccionado Pyme");
						log.error("exception:::::"+exception);

                        exception = e.getMessage();
                        String exception1 = exception.replace(',',' ');
                        exception1 = exception1.replace('\'',' ');
                        if(exception1.length()>260)
                            exception1 = exception1.substring(0, 260);

						strSQL = new StringBuffer();
						//cambia de estatus
						  strSQL.append(" UPDATE com_documento");
							strSQL.append(" SET ic_estatus_docto = 2"); //Negociable
							strSQL.append(" WHERE ic_estatus_docto = 26"); //programado pyme
							strSQL.append(" AND ic_EPO = "+icEpo);
							strSQL.append(" AND ic_documento = "+icDocumento);


						log.error("..:: strSQL : "+strSQL.toString());


						pst = con.queryPrecompilado(strSQL.toString());
						pst.executeUpdate();
						pst.close();

						// Inserta en la bitacora de cambios de estatus
						// como (32) Programado Pyme a Negociable
						strSQL = new StringBuffer();

						strSQL.append(" insert into comhis_cambio_estatus");
						strSQL.append(" (dc_fecha_cambio, ic_documento, ic_cambio_estatus, ct_cambio_motivo , cg_nombre_usuario)");
						strSQL.append(" values(sysdate,"+icDocumento+" ,33,'"+exception1+"','Proceso autom�tico Manejo Servicio')");

						log.debug("..:: strSQL : "+strSQL.toString());

						pst = con.queryPrecompilado(strSQL.toString());
						pst.executeUpdate();
						pst.close();

				} finally {
					//if(con.hayConexionAbierta()){ 	con.terminaTransaccion(trans_op); 	con.cierraConexionDB();	}
				}

					//esto corresponde al Fodea 037-2010	 Reporte IFS de Programada PYME
          // Se actualizara la fecha de seleccion del documento en la tabla de
          // com_docto_seleccionado

          qrySentencia = " update com_docto_seleccionado  "  +
                          " set df_fecha_seleccion = sysdate "  +
                          " where ic_documento = "+icDocumento;

           log.debug("qrySentencia   "+qrySentencia);

           con.ejecutaSQL(qrySentencia);


          //Se genera el Acuse  en la tabla  com_acuse2

         qrySentencia = " update com_acuse2  "  +
                          " set df_fecha_hora = sysdate "  +
                          " where cc_acuse = '"+cc_acuse+"'"+
													" and ic_producto_nafin = 1";

          log.debug("qrySentencia   "+qrySentencia);
          con.ejecutaSQL(qrySentencia);



			}//termina while
			rs.close();
      ps.close();

			//se estable el monto comprometido a NULL cuando
			//el estatus fue cambiado a seleccionado pyme

			strSQL = new StringBuffer();
			strSQL.append(" UPDATE comrel_limite_pyme_if_x_epo");
			strSQL.append(" SET fn_monto_comprometido = NULL");
			strSQL.append(" WHERE fn_monto_comprometido IS NOT NULL");

			log.debug("..:: strSQL : "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();

			strSQL.append(" UPDATE comrel_if_epo");
			strSQL.append(" SET fn_monto_comprometido = NULL");
			strSQL.append(" WHERE fn_monto_comprometido IS NOT NULL");

			log.debug("..:: strSQL : "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			strSQL.append(" UPDATE comrel_producto_if");
			strSQL.append(" SET fn_monto_comprometido = NULL");
			strSQL.append(" WHERE fn_monto_comprometido IS NOT NULL");

			log.debug("..:: strSQL : "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.executeUpdate();
			pst.close();

        } catch(Exception e) {
			log.error("cambiarEstatusDoctosFact24h(Exception)::::: "+e);
			trans_op = false;
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
	    log.info("ManejoServicioBean::cambiarEstatusDoctosFact24h(S)");
	}

	/**
	 * M�todo que se encarga de verificar si existen documentos con el estatus Programado Pyme.
	 * @return docto_prg true Si existen documentos con estatus Programado Pyme, false en caso contrario.
	 * @throws AppException Cuando ocurre un error al realizar la consulta.
	 * @author Alberto Cruz Flores
	 * @since FODEA 005 - 2009 Factoraje 24h
	 */
	public boolean existenDoctosProgramadosPyme() throws AppException{
		log.info("exitenDoctosProgramadosPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		int num_documentos = 0;
		boolean docto_prg = false;
		try{
			con.conexionDB();

			strSQL.append(" SELECT COUNT(ic_documento) AS num_documentos");
			strSQL.append(" FROM com_documento");
			strSQL.append(" WHERE ic_estatus_docto = ?");

			varBind.add(new Integer(26));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);

			rst = pst.executeQuery();

			while(rst.next()){num_documentos = rst.getInt("num_documentos");}

			rst.close();
			pst.close();

			if(num_documentos > 0){docto_prg = true;}else{docto_prg = false;}
		}catch(Exception e){
			log.info("exitenDoctosProgramadosPyme(ERROR)");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("exitenDoctosProgramadosPyme(S)");
		return docto_prg;
	}

  /**FODEA 037 Reporte IFS de Programada PYME
 *@Metodo que cambia el estatus de los documentos cuando estos tienen el estatus
 *@Programado Pyme  (26) a Programado Pyme a Seleccionado Pyme(32)
 *@y guardara en la tabla de bitacora comhis_cambio_estatus  los cambios
 *@realizados al documento
 *
 */
  public void cambiarEstatusDoctos32() 	throws NafinException {
		log.info(	"cambiarEstatusDoctos32 (E)");
		
		AccesoDB 	con  					= new AccesoDB();
		String qrySentencia 	= "";
		String qrySentencia1	= "";
		String qrySentencia2	= "";
		String qrySentencia3  ="";
		String qrySentencia4  ="";
		boolean 	hayError 		= false;
		ResultSet rs 					= null;
		PreparedStatement pst = null;
		try {
			con.conexionDB();


  qrySentencia="select d.cc_acuse, d.ic_DOCUMENTO " +
				"from comrel_horario_x_epo hxe, com_documento d " +
				"where hxe.ic_epo = d.ic_epo " +
				"	and hxe.ic_producto_nafin = 1 " +
				"   and d.ic_Estatus_docto = 26 " +
				"	group by d.ic_epo, d.ic_if, D.IC_DOCUMENTO, d.cc_acuse";

			pst = con.queryPrecompilado(qrySentencia);
      rs = pst.executeQuery();

			log.debug(" qrySentencia"+qrySentencia);

        while(rs.next()) {

          String icDocumento = rs.getString("IC_DOCUMENTO");
          String cc_acuse = rs.getString("cc_acuse");

          System.out.println("icDocumento   "+icDocumento);
          System.out.println("cc_acuse   "+cc_acuse);

          /* Inserta en la bitacora de cambios de estatus
           * como (32) Programado Pyme a Seleccionado Pyme
           */
          qrySentencia2 = " insert into comhis_cambio_estatus"+
                          " (dc_fecha_cambio, ic_documento, ic_cambio_estatus, ct_cambio_motivo , cg_nombre_usuario)"+
                          " values(sysdate,"+icDocumento+" ,32,'Proceso matutino','Proceso autom�tico Manejo Servicio')";

           System.out.println("qrySentencia2   "+qrySentencia2);

           con.ejecutaSQL(qrySentencia2);


           /* Se actualizara la fecha de seleccion del documento en la tabla de
            * com_docto_seleccionado
           */
          qrySentencia3 = " update com_docto_seleccionado  "  +
                          " set df_fecha_seleccion = sysdate "  +
                          " where ic_documento = "+icDocumento;

           System.out.println("qrySentencia3   "+qrySentencia3);

           con.ejecutaSQL(qrySentencia3);


          //Se genera el Acuse  en la tabla  com_acuse2


         qrySentencia4 = " update com_acuse2  "  +
                          " set df_fecha_hora = sysdate "  +
                          " where cc_acuse = '"+cc_acuse+"'"+
													" and ic_producto_nafin = 1";

          System.out.println("qrySentencia4   "+qrySentencia4);
          con.ejecutaSQL(qrySentencia4);


       //cambia el estatus de (26)Programado Pyme a  (3)Seleccionado Pyme

          qrySentencia1= " update com_documento "  +
                         " set ic_estatus_docto = 3"  +	//Seleccionado Pyme
                         " where ic_documento = "+ icDocumento +" "+
                         " and ic_estatus_docto = 26 ";  //Programado Pyme
         System.out.println("qrySentencia1   "+qrySentencia1);
          con.ejecutaSQL(qrySentencia1);

        }

			rs.close();
      pst.close();
			//con.cierraStatement();



		} catch (Exception e) {
			hayError = true;
			log.error("Error en cambiarEstatusDoctos32(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (!hayError) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
       log.info(	"cambiarEstatusDoctos32 (S)");
		}
	}

	public List getHorariosEpoxIf(String cveProducto, String cveEpo, String cveIf) throws AppException {
		List lsHorarios = new ArrayList();
		AccesoDB con = new AccesoDB();
		try {
			log.info("ManejoServicioBean.getHorariosEpoxIf(E)");
			con.conexionDB();

			String qryHorarios  = "SELECT   cie.ic_producto_nafin cveproducto, pn.ic_nombre nombreproducto, " +
						"         e.ic_epo cveepo, e.cg_razon_social nombreepo, ci.ic_if cveif, " +
						"         ci.cg_razon_social nombreif, nvl(he.cg_horario_inicio,pn.cg_horario_inicio) aperturapyme, " +
						"         nvl(he.cg_horario_fin,pn.cg_horario_fin) cierrepyme,  " +
						"         nvl(he.cg_horario_inicio_if, pn.cg_horario_inicio_if) aperturaif, " +
						"         nvl(he.cg_horario_fin_if, pn.cg_horario_fin_if) cierreif,  " +
						"         nvl(he.cs_especial, 'N') csespecial, " +
						"         decode(cie.cs_tipo_fondeo,'P','S',decode(cie.cs_tipo_fondeo,'M','S','N')) fondeopropio " +
						"    FROM  " +
						"        comrel_if_epo_x_producto cie, " +
						"        comcat_producto_nafin pn, " +
						"        comcat_epo e, " +
						"        comcat_if ci, " +
						"        comrel_horario_epo_x_if he " +
						"   WHERE cie.ic_epo = e.ic_epo " +
						"     AND cie.ic_if = ci.ic_if " +
						"     AND cie.ic_producto_nafin = pn.ic_producto_nafin " +
						"     AND cie.ic_producto_nafin = he.ic_producto_nafin(+) " +
						"     AND cie.ic_epo = he.ic_epo (+) " +
						"     AND cie.ic_if = he.ic_if (+) ";

			if (!cveProducto.equals("")) {
				qryHorarios += " and cie.ic_producto_nafin = ? ";
			}else{
				qryHorarios += " and cie.ic_producto_nafin in (1) ";
			}
			if (cveEpo.equals("T")) {
				qryHorarios += " and he.cs_especial = ? ";
			} else if (!cveEpo.equals("")) {
				qryHorarios += " and cie.ic_epo = ? ";
			}
			if (!cveIf.equals("")) {
				qryHorarios += " and cie.ic_if = ? ";
			}

			qryHorarios += " Order by e.ic_epo, cie.ic_producto_nafin ";

			System.out.println("QUERY " + qryHorarios);
			PreparedStatement psHorarios = con.queryPrecompilado(qryHorarios);

			int p = 0;
			if (!cveProducto.equals("")) {
				psHorarios.setLong(++p, Long.parseLong(cveProducto));
			}
			if (cveEpo.equals("T")) {
				psHorarios.setString(++p, "S");
			} else if (!cveEpo.equals("")) {
				psHorarios.setLong(++p, Long.parseLong(cveEpo));
			}
			if (!cveIf.equals("")) {
				psHorarios.setLong(++p, Long.parseLong(cveIf));
			}

			ResultSet rsHorarios = psHorarios.executeQuery();
			HashMap mpDatos = null;
			while (rsHorarios.next())	{
				mpDatos = new HashMap();
				mpDatos.put("CVEPRODUCTO", rsHorarios.getString("cveproducto"));
				mpDatos.put("NOMBREPRODUCTO", rsHorarios.getString("nombreproducto"));
				mpDatos.put("CVEEPO", rsHorarios.getString("cveepo"));
				mpDatos.put("NOMBREEPO", rsHorarios.getString("nombreepo"));
				mpDatos.put("CVEIF", rsHorarios.getString("cveif"));
				mpDatos.put("NOMBREIF", rsHorarios.getString("nombreif"));
				mpDatos.put("APERTURAPYME", rsHorarios.getString("aperturapyme"));
				mpDatos.put("CIERREPYME", rsHorarios.getString("cierrepyme"));
				mpDatos.put("APERTURAIF", rsHorarios.getString("aperturaif"));
				mpDatos.put("CIERREIF", rsHorarios.getString("cierreif"));
				mpDatos.put("USAHORARIONAF", rsHorarios.getString("csespecial"));
				mpDatos.put("OPERFONDEO", rsHorarios.getString("fondeopropio"));

				lsHorarios.add(mpDatos);
			}
		} catch (Throwable err)	{
			log.error("Error en el metodo ManejoServicioBean.getHorariosxProductoEpo(Exception)" + err );
			throw new AppException("Error al consulta horarios Epo por If");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("ManejoServicioBean.getHorariosEpoxIf(S)");
		}
		return lsHorarios;
	}

	public void setHorariosEpoXif(List lstHoraEpoxIf) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		HashMap hmHorarios = new HashMap();
		try {
			log.info("ManejoServicioBean.setHorariosEpoXif(E)" );
			con.conexionDB();
			int conteo = 0;
			String sAPymeM = "";
			String sCPymeM = "";
			String sAIFM = "";
			String sCIFM = "";
			String sCsEspecial = "";
			String sProducto = "";
			String sIcEpo = "";
			String sIcIf = "";
			String sOpFondeo = "";

			StringBuffer sbSql = null;

			if(lstHoraEpoxIf!=null && lstHoraEpoxIf.size()>0){

				for(int x=0; x<lstHoraEpoxIf.size();x++){
					hmHorarios = (HashMap)lstHoraEpoxIf.get(x);

					sAPymeM		= hmHorarios.get("APPYME").toString();
					sCPymeM		= hmHorarios.get("CIPYME").toString();
					sAIFM			= hmHorarios.get("APIF").toString();
					sCIFM			= hmHorarios.get("CIIF").toString();
					sCsEspecial = hmHorarios.get("CSESPECIAL").toString();
					sProducto	= hmHorarios.get("CVEPROD").toString();
					sIcEpo		= hmHorarios.get("CVEEPO").toString();
					sIcIf			= hmHorarios.get("CVEIF").toString();
					sOpFondeo	= hmHorarios.get("OPFONDEO").toString();

					if (sCsEspecial.equals("N")) {
						ArrayList loHorarios = getHorariosxProducto(sProducto);
						Vector vDatos = (Vector) loHorarios.get(0);

						sAPymeM = vDatos.get(2).toString();
						sCPymeM = vDatos.get(3).toString();
						sAIFM   = vDatos.get(4).toString();
						sCIFM   = vDatos.get(5).toString();
					}

					//se valida si ya esxiste el registro
					sbSql = new StringBuffer();
					sbSql.append("SELECT count(1) conteo ");
					sbSql.append("FROM comrel_horario_epo_x_if ");
					sbSql.append("WHERE ic_producto_nafin = ? AND ic_epo = ? AND ic_if = ? ");

					ps = con.queryPrecompilado(sbSql.toString());
					ps.setInt(1, Integer.parseInt(sProducto));
					ps.setInt(2, Integer.parseInt(sIcEpo));
					ps.setInt(3, Integer.parseInt(sIcIf));
					rs = ps.executeQuery();
					if(rs!=null && rs.next()){
						conteo = rs.getInt("conteo");
					}
					rs.close();
					ps.close();

					if(conteo>0){
						sbSql = new StringBuffer();
						sbSql.append("UPDATE comrel_horario_epo_x_if " );
						sbSql.append(" SET cg_horario_inicio = ?, cg_horario_fin = ? , cg_horario_inicio_if = ?, cg_horario_fin_if = ? , cs_especial = ? ");
						sbSql.append(" WHERE ic_producto_nafin = ? and ic_epo = ? and ic_if = ? ");

						ps = con.queryPrecompilado(sbSql.toString());
						ps.setString(1, sAPymeM);
						ps.setString(2, sCPymeM);
						ps.setString(3, sAIFM);
						ps.setString(4, sCIFM);
						ps.setString(5, sCsEspecial);
						ps.setInt(6, Integer.parseInt(sProducto));
						ps.setInt(7, Integer.parseInt(sIcEpo));
						ps.setInt(8, Integer.parseInt(sIcIf));
						ps.executeUpdate();
						ps.close();
					}else{
						sbSql = new StringBuffer();
						sbSql.append("INSERT INTO comrel_horario_epo_x_if ");
						sbSql.append(" (cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, ");
						sbSql.append(" cg_horario_fin_if, cs_especial, ic_producto_nafin, ic_epo, ic_if ");
						sbSql.append(" ) ");
						sbSql.append(" VALUES (?, ?, ?, ");
						sbSql.append(" ?, ?, ?, ?, ? ");
						sbSql.append(" ) ");

						ps = con.queryPrecompilado(sbSql.toString());
						ps.setString(1, sAPymeM);
						ps.setString(2, sCPymeM);
						ps.setString(3, sAIFM);
						ps.setString(4, sCIFM);
						ps.setString(5, sCsEspecial);
						ps.setInt(6, Integer.parseInt(sProducto));
						ps.setInt(7, Integer.parseInt(sIcEpo));
						ps.setInt(8, Integer.parseInt(sIcIf));
						ps.executeUpdate();
						ps.close();
					}
				}
			}

		} catch (Throwable t)	{
			commit = false;
			log.error("ManejoServicioBean.setHorariosEpoXif(Error)" );
			throw new AppException("Error al establecer horarios Epo x If",t);
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("ManejoServicioBean.setHorariosEpoXif(S)" );
		}
	}

}	//fin de clase


