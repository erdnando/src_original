package com.netro.fianza;
import java.io.*;

public class ParamConsBean implements Serializable {
	
	// PARAMETROS PARA CONSULTAS 
	private String icEpo;
	private String icAfianzadora;
	private String icFiado;
	private String icEstatus;
	private String numFianza;
	private String numContrato;
	private String dfSolicIni;
	private String dfSolicFin;
	private String dfVencIni;
	private String dfVencFin;
	private String dfPublicIni;
	private String dfPublicFin;
	private String usuario;
	private String autorizador;
	
	public ParamConsBean() {
		icEpo			= "";	icAfianzadora	= "";
		icFiado		= "";	icEstatus		= "";
		numFianza	= "";	numContrato 	= "";
		dfSolicIni	= "";	dfSolicFin		= "";
		dfVencIni	= "";	dfVencFin		= "";
		dfPublicIni	= "";	dfPublicFin		= "";
		usuario		= "";
		autorizador		= "";
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setIcAfianzadora(String icAfianzadora) {
		this.icAfianzadora = icAfianzadora;
	}


	public String getIcAfianzadora() {
		return icAfianzadora;
	}


	public void setIcFiado(String icFiado) {
		this.icFiado = icFiado;
	}


	public String getIcFiado() {
		return icFiado;
	}


	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}


	public String getIcEstatus() {
		return icEstatus;
	}


	public void setNumFianza(String numFianza) {
		this.numFianza = numFianza;
	}


	public String getNumFianza() {
		return numFianza;
	}


	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}


	public String getNumContrato() {
		return numContrato;
	}


	public void setDfSolicIni(String dfSolicIni) {
		this.dfSolicIni = dfSolicIni;
	}


	public String getDfSolicIni() {
		return dfSolicIni;
	}


	public void setDfSolicFin(String dfSolicFin) {
		this.dfSolicFin = dfSolicFin;
	}


	public String getDfSolicFin() {
		return dfSolicFin;
	}


	public void setDfVencIni(String dfVencIni) {
		this.dfVencIni = dfVencIni;
	}


	public String getDfVencIni() {
		return dfVencIni;
	}


	public void setDfVencFin(String dfVencFin) {
		this.dfVencFin = dfVencFin;
	}


	public String getDfVencFin() {
		return dfVencFin;
	}


	public void setDfPublicIni(String dfPublicIni) {
		this.dfPublicIni = dfPublicIni;
	}


	public String getDfPublicIni() {
		return dfPublicIni;
	}


	public void setDfPublicFin(String dfPublicFin) {
		this.dfPublicFin = dfPublicFin;
	}


	public String getDfPublicFin() {
		return dfPublicFin;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}


	public String getAutorizador() {
		return autorizador;
	}
}