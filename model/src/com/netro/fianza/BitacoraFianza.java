package com.netro.fianza;
import com.netro.fianza.DatosFianzaWS;
import java.sql.*;
import java.util.*;
import java.util.Map.*;
import netropology.utilerias.*;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
import org.apache.commons.logging.Log;

public class BitacoraFianza  {
	public BitacoraFianza() {
	}
	
	private final static Log log = ServiceLocator.getInstance().getLog(BitacoraFianza.class);
	
	public static void agregarRegBitacora(AccesoDB con,
			String claveUsuario, String cveFianza, HashMap infoFianzaAnterior, 
			HashMap infoFianzaActual) 
			throws AppException {
		log.info("BitacoraFianza::agregarRegBitacora(E)");

		String icPyme = "";
		String icEpo = "";
		String icAfianzadora = "";
		String ccEstatus = "";
		String idMoviemiento = "";
		String cgNumFianza = "";
		
		
		//**********************************Validación de parametros:*****************************
		try {
			if (con == null || claveUsuario == null ||
					 infoFianzaAnterior == null || infoFianzaActual == null) {
				throw new AppException("Los parametros no pueden ser nulos");
			}
			
			/*if ( infoFianzaAnterior.size()<=0 ) {
				//Si no hubo cambios no realiza el registro en bitácora
				return;
			}*/

			if (!con.hayConexionAbierta()) {
				throw new AppException("La conexion a BD no es valida ");
			}

			
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" con=" + con + "\n" +
					//" tipoAfiliado=" + tipoAfiliado + "\n" +
					//" claveNafinElectronico=" + claveNafinElectronico + "\n" +
					" claveUsuario=" + claveUsuario + "\n" +
					" infoFianzaAnterior=" + infoFianzaAnterior + "\n" +
					" infoFianzaActual=" + infoFianzaActual);
			throw new AppException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		
		String nombreCompletoUsuario = "";
		try {
			UtilUsr utilUsr = new UtilUsr();
			Usuario usuario = utilUsr.getUsuario(claveUsuario);
			nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno() + " " + usuario.getNombre();
		} catch (Exception e) {
			throw new AppException("Error al obtener los datos del usuario");
		}
		
		
		//Comparacion de datos para detectar cambios
		StringBuffer strCamposModif = new StringBuffer();
		StringBuffer strDatAnterior = new StringBuffer();
		StringBuffer strDatActual = new StringBuffer();
		if(infoFianzaAnterior.size()>0 && infoFianzaActual.size()>0){
			Iterator it = infoFianzaAnterior.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry)it.next();
				String key = (String)e.getKey();
				String value = e.getValue()==null?"":(String)e.getValue();
				//strDatActual.append(key+" = "+infoFianzaActual.get(key).toString()+"\n");
				if(infoFianzaActual.containsKey(key)){
					if(!value.equals(infoFianzaActual.get(key)==null?"":infoFianzaActual.get(key).toString())){
						strDatAnterior.append(key+" = "+value+"\n");
						strCamposModif.append(key+" = "+(infoFianzaActual.get(key)==null?"":infoFianzaActual.get(key).toString())+"\n");
					}
				}
				//campos;
			}
		}
		//se obtienen claves para bitacora
		if(infoFianzaActual.size()>0){
			icEpo = infoFianzaActual.get("Clave_EPO").toString();
			icPyme = infoFianzaActual.get("Clave_Fiado").toString();
			icAfianzadora = infoFianzaActual.get("Clave_Afianzadora").toString();
			ccEstatus = infoFianzaActual.get("Estatus_Fianza").toString();
			idMoviemiento = infoFianzaActual.get("Id_Movimiento").toString();
			cgNumFianza = infoFianzaActual.get("Numero_Fianza").toString();
		}
		
		String strSQLBitacora = "";
				
		try {
			if(strCamposModif.length()>0 || (infoFianzaAnterior.size()<=0 && infoFianzaActual.size()>0)){
				PreparedStatement ps = null;
				ResultSet rs = null;
				strSQLBitacora = 
						" INSERT INTO BIT_FIANZA_ELECTRONICA ( " +
						" ic_bit_fianza, ic_fianza, ic_epo, " +
						" ic_fiado, ic_afianzadora,  " +
						" cc_estatus, cg_numero_fianza, " +
						" ig_idmovimiento, df_fecha, ic_usuario, cg_anterior, cg_actual "+
						" ) VALUES(SEQ_BIT_FIANZA_ELECTRONICA.nextval, "+
						" ?, ?, ?, ?, ?, ?, ?, sysdate, ?, ?, ? ) ";
						
				if(infoFianzaAnterior.size()<=0 && infoFianzaActual.size()>0)
					strCamposModif.append("--ALTA--\n");
					
				ps = con.queryPrecompilado(strSQLBitacora);
				int p = 0;
				ps.setLong(++p,Long.parseLong(cveFianza));
				ps.setLong(++p,Long.parseLong(icEpo));
				ps.setLong(++p,Long.parseLong(icPyme));
				ps.setLong(++p,Long.parseLong(icAfianzadora));
				ps.setString(++p,ccEstatus);
				ps.setString(++p,cgNumFianza);
				ps.setLong(++p,Long.parseLong(idMoviemiento));
				ps.setString(++p,claveUsuario);
				ps.setString(++p,strDatAnterior.toString());
				ps.setString(++p,strCamposModif.toString());
				ps.executeUpdate();
				ps.close();
			}

		} catch(SQLException e) {
			throw new AppException("Error al guardar datos en Bitacora de Fianza", e);
		}finally{
			log.info("BitacoraFianza::agregarRegBitacora(S)");	
		}
	}
	
	public HashMap getInfoFianza(AccesoDB con, String cveFianza){
		
		log.info("BitacoraFianza::getInfoFianza(E)");	
		String query = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap hmFianza = new HashMap();
		
		try{
			
			query = "SELECT ff.ic_fianza, ff.ic_epo, ff.ic_fiado, ff.ic_afianzadora, ff.cc_estatus, " +
					"       ff.cg_numero_contrato, " +
					"       TO_CHAR (ff.df_vencimiento, 'dd/mm/yyyy hh:mi:ss') as df_vencimiento, " +
					"       TO_CHAR (ff.df_publicacion, 'dd/mm/yyyy hh:mi:ss') as df_publicacion, ff.cg_xml, " +
					"       ff.bi_pdf, ff.cg_version_esq, ff.cg_num_cia_afianza, ff.cg_serie, " +
					"       ff.cg_folio, ff.cg_numero_fianza, ff.cg_num_inclusion, " +
					"       ff.cg_num_version, ff.ig_idpoliza, ff.ig_idmovimiento, " +
					"       ff.ig_idsolicitud, ff.cg_movimiento, ff.fg_monto, ff.cg_monto_letra, " +
					"       ff.fg_monto_movimiento, ff.cg_monto_mov_letra, ff.cc_ramo, " +
					"       ff.cc_subramo, ff.cg_tipo_fianza, ff.cg_moneda, " +
					"       TO_CHAR (ff.df_ini_vig_fianza, 'dd/mm/yyyy hh:mi:ss') as df_ini_vig_fianza, " +
					"       TO_CHAR (ff.df_fin_vig_fianza, 'dd/mm/yyyy hh:mi:ss') as df_fin_vig_fianza, " +
					"       TO_CHAR (ff.df_solicitud, 'dd/mm/yyyy hh:mi:ss') as df_solicitud, " +
					"       TO_CHAR (ff.df_autorizacion, 'dd/mm/yyyy hh:mi:ss') as df_autorizacion, " +
					"       ff.cg_af_rfc, ff.cg_af_nombre, ff.cg_af_calle, " +
					"       ff.cg_af_noexterior, ff.cg_af_nointerior, ff.cg_af_colonia, " +
					"       ff.cg_af_localidad, ff.cg_af_referencia, ff.cg_af_municipio, " +
					"       ff.cg_af_estado, ff.cg_af_pais, ff.cg_af_cp, ff.cg_suc_calle, " +
					"       ff.cg_suc_noexterior, ff.cg_suc_nointerior, ff.cg_suc_colonia, " +
					"       ff.cg_suc_localidad, ff.cg_suc_referencia, ff.cg_suc_municipio, " +
					"       ff.cg_suc_estado, ff.cg_suc_pais, ff.cg_suc_cp, ff.cg_fiado_rfc, " +
					"       ff.cg_fiado_nmombre, ff.cg_fiado_calle, ff.cg_fiado_noexterior, " +
					"       ff.cg_fiado_nointerior, ff.cg_fiado_colonia, ff.cg_fiado_localidad, " +
					"       ff.cg_fiado_referencia, ff.cg_fiado_municipio, ff.cg_fiado_estado, " +
					"       ff.cg_fiado_pais, ff.cg_fiado_cp, ff.cg_benf_rfc, ff.cg_benf_nombre, " +
					"       ff.cg_agc_rfc, ff.cg_agc_nombre, ff.fg_prima, ff.fg_bonificacion, " +
					"       ff.fg_derechos, ff.fg_gastos_exped, ff.fg_gastos_invest, ff.fg_iva, " +
					"       ff.fg_subtotal, ff.fg_prima_total, ff.cg_txt_fianza, " +
					"       ff.cg_firma_nombre, ff.cg_firma_puesto, ff.cg_txt_restriccion, " +
					"       ff.cg_txt_norm_cnsf, ff.ig_benf_nafin_dest, ff.cg_benf_nombre_usr, " +
					"       ff.cg_benf_correo_usr, ff.cc_benf_idramo, ff.cc_benf_idsubramo, " +
					"       ff.cg_rl_nombre, ff.cg_rl_correo, ff.cg_rl_telefono, " +
					"       ff.cg_seg_certificado, ff.cg_seg_sello_dig, ff.cg_observaciones, ff.cg_autorizador " +
					"  FROM fe_fianza ff " +
					" WHERE ff.ic_fianza= ? ";
					
			cveFianza = (cveFianza==null||"".equals(cveFianza))?"0":cveFianza;
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFianza));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				hmFianza.put("Clave_EPO", rs.getString("ic_epo"));
				hmFianza.put("Clave_Fiado", rs.getString("ic_fiado"));
				hmFianza.put("Clave_Afianzadora", rs.getString("ic_afianzadora"));
				hmFianza.put("Estatus_Fianza", rs.getString("cc_estatus"));
				hmFianza.put("Version_Esquema", rs.getString("cg_version_esq"));
				hmFianza.put("Numero_Cia_Afianzadora", rs.getString("cg_num_cia_afianza"));
				hmFianza.put("Serie", rs.getString("cg_serie"));
				hmFianza.put("Folio", rs.getString("cg_folio"));
				hmFianza.put("Numero_Fianza", rs.getString("cg_numero_fianza"));
				hmFianza.put("Numero_Inclusion", rs.getString("cg_num_inclusion"));
				hmFianza.put("Numero_Version", rs.getString("cg_num_version"));
				hmFianza.put("Id_Poliza", rs.getString("ig_idpoliza"));
				hmFianza.put("Id_Movimiento", rs.getString("ig_idmovimiento"));
				hmFianza.put("Id_Solicitud", rs.getString("ig_idsolicitud"));
				hmFianza.put("Movimiento", rs.getString("cg_movimiento"));
				hmFianza.put("Monto_Afianzado", rs.getString("fg_monto"));
				hmFianza.put("Monto_Afianzado_Letras", rs.getString("cg_monto_letra"));
				hmFianza.put("Monto_Afianzado_Mov", rs.getString("fg_monto_movimiento"));
				hmFianza.put("Monto_Afianzado_Mov_Letra", rs.getString("cg_monto_mov_letra"));
				hmFianza.put("Ramo", rs.getString("cc_ramo"));
				hmFianza.put("SubRamo", rs.getString("cc_subramo"));
				hmFianza.put("Tipo", rs.getString("cg_tipo_fianza"));
				hmFianza.put("Moneda", rs.getString("cg_moneda"));
				hmFianza.put("Ini_Vig_Fianza", rs.getString("df_ini_vig_fianza"));
				hmFianza.put("Fin_Vig_Fianza", rs.getString("df_fin_vig_fianza"));
				hmFianza.put("Fecha_Solicitada", rs.getString("df_solicitud"));
				hmFianza.put("Fecha_Autorizacion", rs.getString("df_autorizacion"));
				hmFianza.put("Documento_Fuente", rs.getString("cg_numero_contrato"));
				hmFianza.put("Afianzadora_Rfc", rs.getString("cg_af_rfc"));
				hmFianza.put("Afianzadora_Nombre", rs.getString("cg_af_nombre"));
				hmFianza.put("Afianzadora_Calle", rs.getString("cg_af_calle"));
				hmFianza.put("Afianzadora_NoExterior", rs.getString("cg_af_noexterior"));
				hmFianza.put("Afianzadora_NoInterior", rs.getString("cg_af_nointerior"));
				hmFianza.put("Afianzadora_Colonia", rs.getString("cg_af_colonia"));
				hmFianza.put("Afianzadora_Localidad", rs.getString("cg_af_localidad"));
				hmFianza.put("Afianzadora_Referencia", rs.getString("cg_af_referencia"));
				hmFianza.put("Afianzadora_Municipio", rs.getString("cg_af_municipio"));
				hmFianza.put("Afianzadora_Estado", rs.getString("cg_af_estado"));
				hmFianza.put("Afianzadora_Pais", rs.getString("cg_af_pais"));
				hmFianza.put("Afianzadora_CP", rs.getString("cg_af_cp"));
				hmFianza.put("Suc_Calle", rs.getString("cg_suc_calle"));
				hmFianza.put("Suc_NoExterior", rs.getString("cg_suc_noexterior"));
				hmFianza.put("Suc_NoInterior", rs.getString("cg_suc_nointerior"));
				hmFianza.put("Suc_Colonia", rs.getString("cg_suc_colonia"));
				hmFianza.put("Suc_Localidad", rs.getString("cg_suc_localidad"));
				hmFianza.put("Suc_Referencia", rs.getString("cg_suc_referencia"));
				hmFianza.put("Suc_Municipio", rs.getString("cg_suc_municipio"));
				hmFianza.put("Suc_Estado", rs.getString("cg_suc_estado"));
				hmFianza.put("Suc_Pais", rs.getString("cg_suc_pais"));
				hmFianza.put("Suc_CP", rs.getString("cg_suc_cp"));
				hmFianza.put("Fiado_Rfc", rs.getString("cg_fiado_rfc"));
				hmFianza.put("Fiado_Nombre", rs.getString("cg_fiado_nmombre"));
				hmFianza.put("Fiado_Calle", rs.getString("cg_fiado_calle"));
				hmFianza.put("Fiado_NoExterior", rs.getString("cg_fiado_noexterior"));
				hmFianza.put("Fiado_NoInterior", rs.getString("cg_fiado_nointerior"));
				hmFianza.put("Fiado_Colonia", rs.getString("cg_fiado_colonia"));
				hmFianza.put("Fiado_Localidad", rs.getString("cg_fiado_localidad"));
				hmFianza.put("Fiado_Referencia", rs.getString("cg_fiado_referencia"));
				hmFianza.put("Fiado_Municipio", rs.getString("cg_fiado_municipio"));
				hmFianza.put("Fiado_Estado", rs.getString("cg_fiado_estado"));
				hmFianza.put("Fiado_Pais", rs.getString("cg_fiado_pais"));
				hmFianza.put("Fiado_CP", rs.getString("cg_fiado_cp"));
				hmFianza.put("Benef_Rfc", rs.getString("cg_benf_rfc"));
				hmFianza.put("Benef_Nombre", rs.getString("cg_benf_nombre"));
				hmFianza.put("Ag_Corredor_Rfc", rs.getString("cg_agc_rfc"));
				hmFianza.put("Ag_Corredor_Nombre", rs.getString("cg_agc_nombre"));
				hmFianza.put("Prima", rs.getString("fg_prima"));
				hmFianza.put("Bonificacion", rs.getString("fg_bonificacion"));
				hmFianza.put("Derechos", rs.getString("fg_derechos"));
				hmFianza.put("GastosExpedicion", rs.getString("fg_gastos_exped"));
				hmFianza.put("GastosInvestigacion", rs.getString("fg_gastos_invest"));
				hmFianza.put("Iva", rs.getString("fg_iva"));
				hmFianza.put("SubTotal", rs.getString("fg_subtotal"));
				hmFianza.put("PrimaTotal", rs.getString("fg_prima_total"));
				//hmFianza.put("textoFianza",rs.getClob("cg_txt_fianza")); //SE DEJA PENDIENTE LA OBTENCION DE LOS CAMPOS CLOB
				hmFianza.put("Ver_Impresa_Nombre", rs.getString("cg_firma_nombre"));
				hmFianza.put("Ver_Impresa_Puesto", rs.getString("cg_firma_puesto"));
				//hmFianza.put("textoRestriccion",rs.getClob("cg_txt_restriccion")); //SE DEJA PENDIENTE LA OBTENCION DE LOS CAMPOS CLOB
				//hmFianza.put("textoNormCNSF",rs.getClob("cg_txt_norm_cnsf"));	 //SE DEJA PENDIENTE LA OBTENCION DE LOS CAMPOS CLOB
				hmFianza.put("Benef_Destinatario", rs.getString("ig_benf_nafin_dest"));
				hmFianza.put("Benef_NombreUsuario", rs.getString("cg_benf_nombre_usr"));
				hmFianza.put("Benef_CorreoUsuario", rs.getString("cg_benf_correo_usr"));
				hmFianza.put("Benef_IdRamo", rs.getString("cc_benf_idramo"));
				hmFianza.put("Benef_IdSubRamo", rs.getString("cc_benf_idsubramo"));
				hmFianza.put("RepLegal_Nombre", rs.getString("cg_rl_nombre"));
				hmFianza.put("RepLegal_Correo", rs.getString("cg_rl_correo"));
				hmFianza.put("RepLegal_Telefono", rs.getString("cg_rl_telefono"));
				hmFianza.put("Certificado", rs.getString("cg_seg_certificado"));
				hmFianza.put("SelloDigital", rs.getString("cg_seg_sello_dig"));
				hmFianza.put("Observaciones", rs.getString("cg_observaciones"));
				hmFianza.put("Autorizador", rs.getString("cg_autorizador"));
				

			}

			return hmFianza;			
		}catch(Throwable t){
			throw new AppException("Error al obtener la info actual de la Fianza",t);
		}finally{
			log.info("BitacoraFianza::getInfoFianza(S)");	
		}
	}
	
	
}