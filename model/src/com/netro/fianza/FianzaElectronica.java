package com.netro.fianza;

import com.netro.fianza.ws.FianzaWSInfo;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;

@Remote
public interface FianzaElectronica {

    public List getConsCatalogo(String catalogo ,  String clave, String claveRamo, String descripcion) throws AppException;//FODEA ###-2011

    public String  getValidaCatalogo(String catalogo, String clave, String claveRamo) throws AppException;//FODEA ###-2011

    public String  getAgregarCatalogo(String catalogo, String clave, String claveRamo, String descripcion) throws AppException;//FODEA ###-2011

    public String  getUpdateCatalogo(String catalogo, String clave, String claveRamo, String descripcion) throws AppException;//FODEA ###-2011

    public String  getDeleteCatalogo(String catalogo, String clave, String claveRamo) throws AppException;//FODEA ###-2011

    public List  ConsParametrosEPO(String epo ) throws AppException;//FODEA ###-2011

    public String  AgregarParametrosEPO(String epo, List datos ) throws AppException;//FODEA ###-2011

    public String  getMonitorEXT(String noAfianzadora, String noEpo, String noPyme) throws AppException;//FODEA ###-2011

    public FianzaWSInfo procesarFianzasWS (String claveUsuario, String password, String pkcs7, String serial, String claveAfianzadora, byte[] xmlFianza, byte[] pdfFianza );

    public FianzaWSInfo consultarFianzaWS (String claveUsuario, String password, String claveAfianzadora, String folio, String numeroFianza, String idPoliza, String rfcProveedor , String fechaAutorizacionInicial, String fechaAutorizacionFinal, String estatus);

    public List getAutorizadoresUsuariosxEpo(String claveAfiliado, String tipoAfiliado, String perfil)throws AppException;

    public List getFianzas(ParamConsBean params) throws AppException;

    public String getPDFfianzas(String cveFianza, String rutaDestino)throws AppException;

    public String getPDFfianzasErrores(String cveFianza, String rutaDestino)throws AppException;

    public void setEnvioFianzasBandeja(List lstClaves, String usuario, AcuseFianza acuse)throws AppException;

    public List getPreAcuseConcFianzas(List lstClaves)throws AppException;

    public List getAcuseConcFianzas(String acuse)throws AppException;

    public void setRevisionFianzas(List lstClaves, String cveEpo, String usuario, AcuseFianza acuse)throws AppException;

    public boolean validaFirmaMancomunada(String cveEpo) throws AppException;

    public void setEnvioCorreoFianzas(String acuse, String usuario, String perfil,String cliente) throws AppException;

    public void setAutorizacionFianzas(List lstClaves, String firmaMan, String usuario, AcuseFianza acuse)throws AppException;

    public String getTipoComisionFE()throws AppException;

    public HashMap getComisionFija()throws AppException;

    public void setComisionFija(String montoComision, String iva)throws AppException;

    public List getComisionPorRangos()throws AppException;

    public void setComisionPorRangos(List lstRangos, String iva)throws AppException;

    public List getConsComisiones(String cveAfianza, String mesIni, String mesFin, String anio)throws AppException;

    public void actuzalizaFiado( String cveFiado, String noUsuario, HashMap mDataFiado)throws AppException;

    public boolean borrarFiado(String cveFiado, String noUsuario) throws AppException;

    public String consultaTerminos(String cveFiado) throws AppException;

    public String capturaTerminos(String terminos, String versionPDF, String cveFiado) throws AppException;

}