package com.netro.fianza;

import com.netro.fianza.ws.FianzaWSInfo;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Reader;

import java.math.BigDecimal;

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "FianzaElectronicaEJB" , mappedName = "FianzaElectronicaEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class FianzaElectronicaBean implements FianzaElectronica {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(FianzaElectronicaBean.class);

	/**
	 *  metodo para consultar los diferentes tipos de catalogos
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param descripcion
	 * @param claveRamo
	 * @param clave
	 * @param catalogo
	 */
	public List getConsCatalogo(String catalogo, String clave, String claveRamo, String descripcion ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List datos = new ArrayList();
		List resultado = new ArrayList();
		StringBuffer 	SQL = new StringBuffer();
		boolean commit = true;
		log.info("getConsCatalogo (E)");

		try{
			con.conexionDB();
			//Tipo de Movimiento
			if(catalogo.equals("1")) {
				SQL = new StringBuffer();
				SQL.append(" select cc_tipo_movimiento as   clave, ");
				SQL.append("  cg_descripcion as  descripcion ");
				SQL.append(" from FECAT_TIPO_MOVIMIENTO  ");
				if(!clave.equals("") && descripcion.equals("") ){
					SQL.append(" WHERE  cc_tipo_movimiento ='"+clave+"'");
				}
				if(clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cg_descripcion LIKE '"+descripcion+"%'");
				}
				if(!clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cc_tipo_movimiento ='"+clave+"'");
					SQL.append(" AND  cg_descripcion LIKE '"+descripcion+"%'");
				}
				SQL.append(" order by cc_tipo_movimiento  ");

				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					datos = new ArrayList();
					datos.add(rs.getString("clave")==null?"":rs.getString("clave"));
					datos.add(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
					resultado.add(datos);
				}
				rs.close();
				ps.close();
			}
			//FE Ramo
			if(catalogo.equals("2")) {
				SQL = new StringBuffer();
				SQL.append(" select cc_ramo as   clave, ");
				SQL.append("  cg_descripcion as  descripcion ");
				SQL.append(" from FECAT_RAMO  ");
				if(!clave.equals("") && descripcion.equals("") ){
					SQL.append(" WHERE  cc_ramo ='"+clave+"'");
				}
				if(clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cg_descripcion LIKE '"+descripcion+"%'");
				}
				if(!clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cc_ramo ='"+clave+"'");
					SQL.append(" AND  cg_descripcion LIKE '"+descripcion+"%'");
				}
				SQL.append(" order by cc_ramo  ");

				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					datos = new ArrayList();
					datos.add(rs.getString("clave")==null?"":rs.getString("clave"));
					datos.add(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
					resultado.add(datos);
				}
				rs.close();
				ps.close();
			}

			// FE Sub-Ramo
			if(catalogo.equals("3")) {
				SQL = new StringBuffer();
				SQL.append(" select s.cc_ramo as  claveRamo, ");
				SQL.append(" s.cc_subramo as  claveSubRamo, ");
				SQL.append(" r.cg_descripcion as  descripcionRamo, ");
				SQL.append(" s.cg_descripcion as  descripcionSubRamo  ");
				SQL.append(" from FECAT_SUBRAMO s , FECAT_RAMO r ");
				SQL.append(" where s.cc_ramo =r.cc_ramo ");
				if(!clave.equals("") && descripcion.equals("")  && claveRamo.equals("")){
					SQL.append(" and  s.cc_subramo ='"+clave+"'");
				}
				if(clave.equals("") && !descripcion.equals("")  && claveRamo.equals("")){
					SQL.append(" and  s.cg_descripcion LIKE '"+descripcion+"%'");
				}
				if(clave.equals("") && descripcion.equals("")  && !claveRamo.equals("")){
					SQL.append(" and  s.cc_ramo ='"+claveRamo+"'");
				}
				if(!clave.equals("") && !descripcion.equals("")  && claveRamo.equals("")){
					SQL.append(" and  s.cc_ramo ='"+claveRamo+"'");
					SQL.append(" AND  s.cg_descripcion LIKE '"+descripcion+"%'");
				}
				if(!clave.equals("") && descripcion.equals("")  && !claveRamo.equals("")){
					SQL.append(" and  s.cc_subramo ='"+clave+"'");
					SQL.append(" AND  s.cc_ramo ='"+claveRamo+"'");
				}
				if(clave.equals("") && !descripcion.equals("")  && !claveRamo.equals("")){
					SQL.append(" and  s.cc_ramo ='"+claveRamo+"'");
					SQL.append(" AND  s.cg_descripcion LIKE '"+descripcion+"%'");
				}
				SQL.append(" order by s.cc_ramo, s.cc_subramo  asc ");


				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					datos = new ArrayList();
					datos.add(rs.getString("claveRamo")==null?"":rs.getString("claveRamo"));
					datos.add(rs.getString("claveSubRamo")==null?"":rs.getString("claveSubRamo"));
					datos.add(rs.getString("descripcionRamo")==null?"":rs.getString("descripcionRamo"));
					datos.add(rs.getString("descripcionSubRamo")==null?"":rs.getString("descripcionSubRamo"));
					resultado.add(datos);
				}
				rs.close();
				ps.close();
			}

			//FE Estatus
			if(catalogo.equals("4")) {
				SQL = new StringBuffer();
				SQL.append(" select cc_estatus as   clave, ");
				SQL.append(" cg_descripcion as  descripcion ");
				SQL.append(" from FECAT_ESTATUS  ");
				if(!clave.equals("") && descripcion.equals("") ){
					SQL.append(" WHERE  cc_estatus ='"+clave+"'");
				}
				if(clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cg_descripcion LIKE '"+descripcion+"%'");
				}
				if(!clave.equals("") && !descripcion.equals("") ){
					SQL.append(" WHERE  cc_estatus ='"+claveRamo+"'");
					SQL.append(" AND  cg_descripcion LIKE '"+descripcion+"%'");
				}
				SQL.append(" order by cc_estatus  ");

				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					datos = new ArrayList();
					datos.add(rs.getString("clave")==null?"":rs.getString("clave"));
					datos.add(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
					resultado.add(datos);
				}
				rs.close();
				ps.close();
			}

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getConsAfianzadora "+e);
			throw new AppException("Error al getConsAfianzadora",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getConsAfianzadora (S)");
		}
			return resultado;
	}

	/**
	 * metodo que valida si ya existe la clave del catalogo que se va a
	 * insertar
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param claveRamo
	 * @param clave
	 * @param catalogo
	 */
	public String  getValidaCatalogo(String catalogo, String clave, String claveRamo ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();
		String resultado ="";
		String total="0";
		boolean commit = true;
		log.info("getValidaCatalogo (E)");

		try{
			con.conexionDB();
			//Tipo de Movimiento
			if(catalogo.equals("1")) {
				SQL = new StringBuffer();
				SQL.append(" select count(*) as total FROM FECAT_TIPO_MOVIMIENTO ");
				SQL.append(" WHERE cc_tipo_movimiento  = ? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				rs = ps.executeQuery();
				while(rs.next()){
					total =rs.getString("total")==null?"":rs.getString("total");
				}
				rs.close();
				ps.close();
			}

			//FE Ramo
			if(catalogo.equals("2")) {
				SQL = new StringBuffer();
				SQL.append(" select count(*) as total FROM FECAT_RAMO   ");
				SQL.append(" WHERE cc_ramo =? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				rs = ps.executeQuery();
				while(rs.next()){
					total =rs.getString("total")==null?"":rs.getString("total");
				}
				rs.close();
				ps.close();
			}

			// FE Sub-Ramo
			if(catalogo.equals("3")) {
				SQL = new StringBuffer();
				SQL.append(" select count(*) as total FROM  FECAT_SUBRAMO  ");
				SQL.append(" WHERE cc_subramo = '"+clave+"'");
				SQL.append(" AND cc_ramo = '"+claveRamo+"'");
				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					total =rs.getString("total")==null?"":rs.getString("total");
				}
				rs.close();
				ps.close();
			}

			//FE Estatus
			if(catalogo.equals("4")) {
				SQL = new StringBuffer();
				SQL.append(" select count(*) as total FROM   FECAT_ESTATUS  ");
				SQL.append(" WHERE cc_estatus =? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				rs = ps.executeQuery();
				while(rs.next()){
					total =rs.getString("total")==null?"":rs.getString("total");
				}
				rs.close();
				ps.close();
			}
			resultado =total;

		}catch(Exception e){
			 commit = false;
			e.printStackTrace();
			log.error("Error getValidaCatalogo "+e);
			throw new AppException("Error al getValidaCatalogo",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getValidaCatalogo (S)");
		}
			return resultado;
	}

	/**
	 * metodo agrega los registros a los respectivos catalogos
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param descripcion
	 * @param claveSubRamo
	 * @param clave
	 * @param catalogo
	 */
	public String  getAgregarCatalogo(String catalogo, String clave, String claveRamo, String descripcion  ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer 	SQL = new StringBuffer();
		String resultado ="";
		boolean commit = true;
		log.info("getAgregarCatalogo (E)");

		try{
			con.conexionDB();

			//Tipo de Movimiento
			if(catalogo.equals("1")) {
				SQL = new StringBuffer();
				SQL.append(" INSERT INTO  FECAT_TIPO_MOVIMIENTO ");
				SQL.append(" (cc_tipo_movimiento , cg_descripcion) ");
				SQL.append(" VALUES(?,?) ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				ps.setString(2, descripcion);
				ps.executeUpdate();
				ps.close();
			}

			//FE Ramo
			if(catalogo.equals("2")) {
				SQL = new StringBuffer();
				SQL.append(" INSERT INTO  FECAT_RAMO   ");
				SQL.append(" (cc_ramo,  cg_descripcion) ");
				SQL.append(" VALUES(? ,?)  ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				ps.setString(2, descripcion);
				ps.executeUpdate();
				ps.close();

			}

			// FE Sub-Ramo
			if(catalogo.equals("3")) {
				SQL = new StringBuffer();
				SQL.append(" INSERT INTO  FECAT_SUBRAMO  ");
				SQL.append(" (cc_ramo, cc_subramo,  cg_descripcion) ");
				SQL.append(" VALUES (?,?,?) ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, claveRamo);
				ps.setString(2, clave);
				ps.setString(3, descripcion);
				ps.executeUpdate();
				ps.close();
			}
			//FE Estatus
			if(catalogo.equals("4")) {
				SQL = new StringBuffer();
				SQL.append(" INSERT INTO FECAT_ESTATUS  ");
				SQL.append(" ( cc_estatus, cg_descripcion )  ");
				SQL.append(" VALUES(?,?) ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				ps.setString(2, descripcion);
				ps.executeUpdate();
				ps.close();
			}
			resultado ="A";

		}catch(Exception e){
			commit = true;
			e.printStackTrace();
			log.error("Error getAgregarCatalogo "+e);
			throw new AppException("Error al getConsAfianzadora",e);
		}finally{
		con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getAgregarCatalogo (S)");
		}
			return resultado;
	}

	/**
	 *metodod que modifica los registros de los respectivos catalogos de Fianza
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param descripcion
	 * @param claveSubRamo
	 * @param clave
	 * @param catalogo
	 */
	public String  getUpdateCatalogo(String catalogo, String clave, String claveRamo, String descripcion  ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer 	SQL = new StringBuffer();
		String resultado ="";
		boolean commit = true;
		log.info("getUpdateCatalogo (E)");

		try{
			con.conexionDB();

			//Tipo de Movimiento
			if(catalogo.equals("1")) {
				SQL = new StringBuffer();
				SQL.append(" UPDATE FECAT_TIPO_MOVIMIENTO ");
				SQL.append(" SET cg_descripcion  = ?  ");
				SQL.append(" WHERE cc_tipo_movimiento  = ? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, descripcion);
				ps.setString(2, clave);
				ps.executeUpdate();
				ps.close();
			}

			//FE Ramo
			if(catalogo.equals("2")) {
				SQL = new StringBuffer();
				SQL.append(" UPDATE FECAT_RAMO   ");
				SQL.append(" SET cg_descripcion =? ");
				SQL.append(" WHERE cc_ramo =?  ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, descripcion);
				ps.setString(2, clave);
				ps.executeUpdate();
				ps.close();
			}

			// FE Sub-Ramo
			if(catalogo.equals("3")) {
				SQL = new StringBuffer();
				SQL.append(" UPDATE  FECAT_SUBRAMO  ");
				SQL.append(" SET cc_ramo =? , ");
				SQL.append("  cg_descripcion =?  ");
				SQL.append(" WHERE cc_subramo = ? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, claveRamo);
				ps.setString(2, descripcion);
				ps.setString(3, clave);
				ps.executeUpdate();
				ps.close();
			}

			//FE Estatus
			if(catalogo.equals("4")) {
				SQL = new StringBuffer();
				SQL.append(" UPDATE  FECAT_ESTATUS  ");
				SQL.append(" SET cg_descripcion =?   ");
				SQL.append(" WHERE cc_estatus =? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, descripcion);
				ps.setString(2, clave);
				ps.executeUpdate();
				ps.close();
			}

			resultado ="M";

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getUpdateCatalogo "+e);
			throw new AppException("Error al getUpdateCatalogo",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getUpdateCatalogo (S)");
		}
			return resultado;
	}


	/**
	 *elimina el registros  con respecto al catalogo
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param claveSubRamo
	 * @param clave
	 * @param catalogo
	 */
	public String  getDeleteCatalogo(String catalogo, String clave, String claveRamo ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();
		String resultado ="";
		boolean commit = true;
		log.info("getDeleteCatalogo (E)");

		try{
			con.conexionDB();

			//Tipo de Movimiento
			if(catalogo.equals("1")) {
				SQL = new StringBuffer();
				SQL.append(" DELETE  FROM FECAT_TIPO_MOVIMIENTO ");
				SQL.append(" WHERE cc_tipo_movimiento  = ? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				ps.executeUpdate();
				ps.close();
				resultado ="E";
			}

			//FE Ramo
			if(catalogo.equals("2")) {
					String valida ="0";
				//valido  si tiene el ramo tiene relacion con la tabla de fe_fianza
					SQL = new StringBuffer();
					SQL.append(" select count(*) as total ");
					SQL.append(" FROM fe_fianza ");
					SQL.append(" where cc_ramo  ='"+clave+"'");
					ps = con.queryPrecompilado(SQL.toString());
					rs = ps.executeQuery();
					while(rs.next()){
						valida  = rs.getString("total")==null?"0":rs.getString("total");
					}
					rs.close();
					ps.close();

					if(valida.equals("0")) {
						SQL = new StringBuffer();
						SQL.append(" DELETE FROM  FECAT_SUBRAMO  ");
						SQL.append(" WHERE cc_ramo = ? ");
						ps = con.queryPrecompilado(SQL.toString());
						ps.setString(1, clave);
						ps.executeUpdate();
						ps.close();

						SQL = new StringBuffer();
						SQL.append(" DELETE FROM FECAT_RAMO   ");
						SQL.append(" WHERE cc_ramo =? ");
						ps = con.queryPrecompilado(SQL.toString());
						ps.setString(1,clave);
						ps.executeUpdate();
						ps.close();

						resultado ="E";

					}else{
						resultado ="N";
					}
			}
			// FE Sub-Ramo
			if(catalogo.equals("3")) {
				String valida ="0";
				//valido  si tiene el Subramo tiene relacion con la tabla de fe_fianza
				SQL = new StringBuffer();
				SQL.append(" select count(*) as total ");
				SQL.append(" FROM fe_fianza ");
				SQL.append(" where cc_subramo  ='"+clave+"'");
				SQL.append(" and cc_ramo  ='"+claveRamo+"'");
				ps = con.queryPrecompilado(SQL.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					valida  = rs.getString("total")==null?"0":rs.getString("total");
				}
				rs.close();
				ps.close();

				if(valida.equals("0")) {
					SQL = new StringBuffer();
					SQL.append(" DELETE FROM  FECAT_SUBRAMO  ");
					SQL.append(" WHERE cc_subramo = ? ");
					SQL.append(" AND cc_ramo =?  ");
					ps = con.queryPrecompilado(SQL.toString());
					ps.setString(1, clave);
					ps.setString(2, claveRamo);
					ps.executeUpdate();
					ps.close();

					resultado ="E";
				}else{
					resultado ="N";
				}

			}

			//FE Estatus
			if(catalogo.equals("4")) {
				SQL = new StringBuffer();
				SQL.append(" DELETE FROM  FECAT_ESTATUS  ");
				SQL.append(" WHERE cc_estatus =? ");
				ps = con.queryPrecompilado(SQL.toString());
				ps.setString(1, clave);
				ps.executeUpdate();
				ps.close();

				resultado ="E";
			}

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getDeleteCatalogo "+e);
			throw new AppException("Error al getDeleteCatalogo",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDeleteCatalogo (S)");
		}
		return resultado;
	}

	/**
	* @throws netropology.utilerias.AppException
	 * @return
	 * @param datos
	 * @param epo
	 */
	public String  AgregarParametrosEPO(String epo, List datos ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer 	SQL = new StringBuffer();
		String resultado ="";
		boolean commit = true;

		log.info("AgregarParametrosEPO (E)");

		try{
			con.conexionDB();

			String FIRMA_MANCOMUNADA = (String)datos.get(0);

			SQL = new StringBuffer();
			SQL.append(" DELETE com_parametrizacion_epo ");
			SQL.append(" Where  CC_PARAMETRO_EPO in( 'FIRMA_MANCOMUNADA') ");
			SQL.append(" AND IC_EPO = ?");
			ps = con.queryPrecompilado(SQL.toString());
			ps.setString(1, epo);
			ps.executeUpdate();
			ps.close();

			SQL = new StringBuffer();
			SQL.append(" INSERT INTO com_parametrizacion_epo ");
			SQL.append(" (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) ");
			SQL.append(" values (?,?,?) ");
			ps = con.queryPrecompilado(SQL.toString());
			ps.setString(1, epo);
			ps.setString(2, "FIRMA_MANCOMUNADA");
			ps.setString(3, FIRMA_MANCOMUNADA );
			ps.executeUpdate();
			ps.close();

			resultado ="A";

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error AgregarParametrosEPO "+e);
			throw new AppException("Error al AgregarParametrosEPO",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("AgregarParametrosEPO (S)");
		}
		return resultado;
	}

	/**
	 * metodo que resliza la consultas para saber que parametros
	 * tiene la epo seleccionada
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param epo
	 */

	public List  ConsParametrosEPO(String epo ) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();
		List resultado = new ArrayList();
		List datos = new ArrayList();
		boolean commit = true;
		log.info("ConsParametrosEPO (E)");

		try{
			con.conexionDB();

			SQL = new StringBuffer();
			SQL.append(" Select CC_PARAMETRO_EPO  AS PARAMETRO , CG_VALOR  AS  VALOR");
			SQL.append(" FROM com_parametrizacion_epo ");
			SQL.append(" WHERE cc_parametro_epo ='FIRMA_MANCOMUNADA' ");
			SQL.append(" AND IC_EPO = ? ");
			ps = con.queryPrecompilado(SQL.toString());
			ps.setInt(1, Integer.parseInt(epo));
			rs = ps.executeQuery();

			while(rs.next()){
				datos = new ArrayList();
				datos.add(rs.getString("PARAMETRO")==null?"":rs.getString("PARAMETRO"));
				datos.add(rs.getString("VALOR")==null?"":rs.getString("VALOR"));
				resultado.add(datos);
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error ConsParametrosEPO "+e);
			throw new AppException("Error al ConsParametrosEPO",e);
		}finally{
				con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ConsParametrosEPO (S)");
		}
		return resultado;
	}


	 /**metodo para mostrar  las Monitor  de Fianzas
	 * @throws netropology.utilerias.AppException
	 * @return regresa  una variable String definido como un  TreeLoader en EXT JS
	 * @param noPyme
	 * @param noEpo
	 * @param noAfianzadora
	 */
	public String  getMonitorEXT(String noAfianzadora, String noEpo, String nofiado) throws AppException{

		log.debug("getMonitor (E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs3 = null;
		PreparedStatement ps4 = null;
		ResultSet rs4 = null;
		StringBuffer 	SQL = new StringBuffer();
		List resultado = new ArrayList();
		List subramo = new ArrayList();
		List info = new ArrayList();
		List nombre = new ArrayList();
		List datos = new ArrayList();
		String nombreRamo ="";
		boolean commit = true;
		String ic_estrato  = "", cd_nombre  = "",
		cc_estatus  = "", desEstatus  = "";
		String AlbolRamo = "";
		List estrato = new ArrayList();
		List estatus = new ArrayList();
		List totales = new ArrayList();
		try{
			con.conexionDB();

			//Obtengo los ramos
			SQL = new StringBuffer();
			SQL.append(" select cc_ramo as ramo, cg_descripcion as descripcion ");
			SQL.append(" FROM FECAT_RAMO ");
			SQL.append(" order by cg_descripcion ");

			ps = con.queryPrecompilado(SQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				subramo = new ArrayList();
				nombre = new ArrayList();
				info = new ArrayList();
				estrato = new ArrayList();
				totales = new ArrayList();

				String ramo1 = 	rs.getString("ramo")==null?"":rs.getString("ramo");
				nombreRamo =rs.getString("descripcion")==null?"":rs.getString("descripcion");

				//SubRamos
				SQL = new StringBuffer();
				SQL.append(" select cc_ramo as cc_ramo , cc_subramo as cc_subramo, cg_descripcion as subramo  ");
				SQL.append(" FROM FECAT_SUBRAMO ");
				SQL.append(" where cc_ramo  ='"+ramo1+"'");
				SQL.append(" order by cg_descripcion ");

				ps1 = con.queryPrecompilado(SQL.toString());
				rs1 = ps1.executeQuery();
				while(rs1.next()){
					datos = new ArrayList();
					datos.add(rs1.getString("subramo")==null?"":rs1.getString("subramo"));
					String ramo2 = rs1.getString("cc_ramo")==null?"":rs1.getString("cc_ramo");
					String subramo2 = rs1.getString("cc_subramo")==null?"":rs1.getString("cc_subramo");


					//OBTENGO LOS ESTRATOS
					SQL = new StringBuffer();
					SQL.append(" SELECT ic_estrato , cd_nombre ");
					SQL.append("  from comcat_estrato ");


					ps2 = con.queryPrecompilado(SQL.toString());
					rs2 = ps2.executeQuery();
					while(rs2.next()){
						estrato = new ArrayList();
						ic_estrato  = rs2.getString("ic_estrato")==null?"":rs2.getString("ic_estrato");
						cd_nombre  = rs2.getString("cd_nombre")==null?"":rs2.getString("cd_nombre");
						estrato.add(cd_nombre);
						//datos.add(estrato);


						SQL = new StringBuffer();
						SQL.append(" select cc_estatus, cg_descripcion ");
						SQL.append(" from FECAT_ESTATUS  ");
						SQL.append("  order by  cg_descripcion asc  ");

						ps3 = con.queryPrecompilado(SQL.toString());
						rs3 = ps3.executeQuery();
						while(rs3.next()){
							estatus = new ArrayList();
							 cc_estatus  = rs3.getString("cc_estatus")==null?"":rs3.getString("cc_estatus");
							 desEstatus  = rs3.getString("cg_descripcion")==null?"":rs3.getString("cg_descripcion");
							 estatus.add(desEstatus);

							//Total  en la tabla de Fianza
							SQL = new StringBuffer();
							SQL.append(" Select count(*)  total  ");
							SQL.append(" ,f.cg_numero_fianza as numeroFianza ");
							SQL.append(" FROM fe_fianza f,  fe_fiado fi ");
							SQL.append(" where f.CC_BENF_IDRAMO  ='"+ramo2+"'");
							SQL.append(" and f.CC_BENF_IDSUBRAMO  ='"+subramo2+"'");
							SQL.append(" and f.ic_fiado = fi.ic_fiado ");
							SQL.append(" and fi.ic_estrato = "+ic_estrato);
							SQL.append(" and f.cc_estatus ='"+cc_estatus+"'");
							if(!noEpo.equals("")){
							SQL.append(" and f.ic_epo =  "+noEpo);
							}
							if(!nofiado.equals("")){
							SQL.append(" and f.ic_fiado =  "+nofiado);
							}
							if(!noAfianzadora.equals("")){
							SQL.append(" and f.ic_afianzadora =  "+noAfianzadora);
							}

							SQL.append(" group by f.cg_numero_fianza ");

							System.out.println("SQL -->"+SQL);

							ps4 = con.queryPrecompilado(SQL.toString());
							rs4 = ps4.executeQuery();
							int total = 0;
							String numeroFianza =  "0";
							while(rs4.next()){
								total ++;
								numeroFianza  += rs4.getString("numeroFianza")==null?"":rs4.getString("numeroFianza")+",";
							}
							estatus.add(String.valueOf(total));
							estatus.add(ramo2);
							estatus.add(subramo2);
							estatus.add(ic_estrato);
							estatus.add(cc_estatus);
							rs4.close();
							ps4.close();

						estrato.add(estatus);
						}
						rs3.close();
						ps3.close();


					datos.add(estrato);
					}
					rs2.close();
					ps2.close();


					subramo.add(datos);

				}
				rs1.close();
				ps1.close();

				if(subramo.size()>0){
					info.add(nombreRamo);
					info.add(subramo);
					resultado.add(info);
				}
			}
			rs.close();
			ps.close();

			AlbolRamo = " [  ";
			for(int k=0;k<resultado.size();k++){
				List ramo1 = (ArrayList)resultado.get(k);
				List subramo2 = (ArrayList)ramo1.get(1);
				String ramo2 = (String)ramo1.get(0);
				if(k>0){  AlbolRamo +=	", ";  }
					AlbolRamo +=" { RAMO: 'Ramo: "+ramo2+"', "+
											"  iconCls:'task-folder', " +
											"  expanded: true, "+
											" children:[ ";

				for(int z=0;z<subramo2.size();z++){
				List subramo3 = (ArrayList)subramo2.get(z);
				String sub = (String)subramo3.get(0);

					if(z==0){ AlbolRamo +=	"{ "; }
					AlbolRamo +=" RAMO:' SubRamo: "+sub+"', " +
								" iconCls:'task-folder', " +
								"  expanded: false,  "+
								" children:[ " ;

					//los estratos
					for(int n=1;n<subramo3.size();n++){
						List estrato1 = (ArrayList)subramo3.get(n);
						String estra = (String)estrato1.get(0);
						AlbolRamo +=" { RAMO: ' Estrato: "+estra+"', "+
									"  iconCls:'task-folder', " +
									"  expanded: false,  "+
									"  children: [ ";


					for(int e=1;e<estrato1.size();e++){
						List estatus1 = (ArrayList)estrato1.get(e);
						String estatus2 = (String)estatus1.get(0);
						String total = (String)estatus1.get(1);
						String ramo3 = (String)estatus1.get(2);
						String subramo4 = (String)estatus1.get(3);
						String ic_estrato2 = (String)estatus1.get(4);
						String noEstatus = (String)estatus1.get(5);

						AlbolRamo +=" { " +
												" RAMO: ' Estatus: "+estatus2+"', "+
												" NOFIANZA: "+total+", "+
												" NORAMO: '"+ramo3+"', "+
												" NOSUBRAMO: '"+subramo4+"', "+
												" NOESTRATO: '"+ic_estrato2+"', "+
												" NOESTATUS: '"+noEstatus+"', "+
												" iconCls: 'task', " +
												" leaf: true  ";

						if(e<estrato1.size()){	 AlbolRamo +=	" }  "; 	}
						if(e<estrato1.size()-1){	AlbolRamo +=	" ,  ";	}

					}//for estatus
					AlbolRamo +=	"  ] ";

					if(n<subramo3.size()){	AlbolRamo +=	" }  "; 	}
					if(n<subramo3.size()-1){	AlbolRamo +=	" ,  ";	}


					}//for de estratos

					AlbolRamo +=	"  ] ";
					if(z<subramo2.size()-1){ 	 AlbolRamo +=	"    } ,  { ";	}//Estrato

				}//for de subramos
				if(k<=resultado.size()){ 	 AlbolRamo +=	"   } ]  } "; 	}//Sub Ramo

			}//for de ramos
			AlbolRamo +=	 " ]";

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getMonitor "+e);
			throw new AppException("Error al getMonitor",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.debug("getMonitor (S)");
		}

		return	AlbolRamo;


	}


	/**
	 * Metodo de WS que realiza la carga de la fianza enviada via WS
	 * @return FianzaWSInfo contiene la informacion del resultada de la carga
	 * @param pdfFianza
	 * @param xmlFianza
	 * @param idRamo
	 * @param claveAfianzadora
	 * @param serial
	 * @param pkcs7
	 * @param password
	 * @param claveUsuario
	 */
	public FianzaWSInfo procesarFianzasWS (String claveUsuario,
			String password, String pkcs7, String serial, String claveAfianzadora,
			byte[] xmlFianza, byte[] pdfFianza ) {

		log.info("procesarFianzasWS(E)");

		FianzaWSInfo resultadoProceso = new FianzaWSInfo();
		boolean commit = true;

		int iCveAfianzadora = 0;
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa
		StringBuffer erroresEsperados = new StringBuffer(); //almacena errores esperados
		String claveFianza = "";

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveAfianzadora == null || claveAfianzadora.equals("")  ||
					xmlFianza == null || pdfFianza==null ||
					pkcs7 == null || pkcs7.equals("") ||
					serial == null || serial.equals("") ) {
				throw new AppException("Los parametros son requeridos");
			}
			iCveAfianzadora = Integer.parseInt(claveAfianzadora);

		} catch(Exception e) {
			log.error("procesarFianzasWS(Error) ", e);
			// Como no entr� al ciclo, se regresan todos los documentos recibidos (si existen)
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveAfianzadora +
					"," + pkcs7 + "," + serial + ")" + ". Error=" + e.getMessage());
			return resultadoProceso;
		}
		//****************************************************************************************

		AccesoDB con = null;
		boolean envSolic = true;
		String acuse = "";

		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new AppException("Error al generar el objeto de acceso a BD", t);
			}
			//Validacion del usuario y Password y que sea del IF especificado
			UtilUsr utilUsr = new UtilUsr();
			List cuentasAfianzadora = null;
			try {
				cuentasAfianzadora = utilUsr.getUsuariosxAfiliado(claveAfianzadora, "A");
			} catch(Throwable t) {
				throw new AppException("Error al obtener la lista de usuarios de la Afianzadora: " + claveAfianzadora + ".", t);
			}
			log.debug("procesarFianzasWS::Validando claveAfianzadora/usuario");

			if (!cuentasAfianzadora.contains(claveUsuario)) {

				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para la Afianzadora especificada");
				return resultadoProceso; //Termina Proceso
			}

			log.debug("procesarFianzasWS::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					return resultadoProceso; //Termina Proceso
				}
			} catch(Throwable t) {
				throw new AppException("Error en el componente de validacion de usuario. ", t);
			}

			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new AppException ("Error al realizar la conexion a la BD. ", t);
			}

			//-------------------------- VALIDACION De FIRMA PKCS7 ----------------------------

			log.info("procesarFianzasWS()::Validando que el certificado sea del usuario especificado");
			PreparedStatement psSerial = null;
			String strSerial = "";

			try {
				String qryTraeSerial =
						" SELECT dn_user " +
						" FROM users_seguridata "+
						" WHERE UPPER(trim(table_uid)) = UPPER(?) ";
				psSerial = con.queryPrecompilado(qryTraeSerial);
				psSerial.setString(1, claveUsuario);
				ResultSet rsSerial = psSerial.executeQuery();

				if(rsSerial.next()) {
					strSerial = rsSerial.getString(1).trim();
				}
				rsSerial.close();
				psSerial.close();
			} catch(Throwable t) {
				throw new AppException("Error al intentar verificar el numero de serie del certificado.",t);
			} finally {
				con.terminaTransaccion(true); //Se realiza un commit dado que se accesa una tabla remota
			}

			if (!serial.equals(strSerial)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|El certificado con numero de serie " + serial + " no corresponde al usuario " + claveUsuario);
				return resultadoProceso;
			}
			String cadenaOriginalFirmada = "";

			try {
				cadenaOriginalFirmada = new String(xmlFianza);//pendienet verificar tama�o maximo de la cadena a enviar
			} catch(Throwable t) {
				throw new AppException("Error al intentar obtener la cadena original antes de la firmar.",t);
			}
			Seguridad s = null;

			try {
				s = new Seguridad();
			} catch(Throwable t) {
				throw new AppException("Error al inicializar el componente de Seguridad.",t);
			}
			char getReceipt = 'Y';
			String receipt = "";

			//Se realiza la creacion de un acuse vacio y se inserta definitivamente (GeneraAcuse da commit)
			AcuseFianza acuseFianza = new AcuseFianza(AcuseFianza.ACUSE_CONCENTRADO, con);
			acuseFianza.setClaveUsuario(claveUsuario);
			try {

				acuse = acuseFianza.getAcuse();
				//la autenticacion del mensaje requiere este numero de acuse
			} catch(Throwable t) {
				throw new AppException("Error al generar el acuse de la operacion.",t);
			}

			log.info("procesarFianzasWS()::Autenticando Firma Digital");
			boolean autenticarMensaje = false;
			try {
				autenticarMensaje =
						s.autenticar(acuse, serial, pkcs7,
								cadenaOriginalFirmada, getReceipt);
				if (autenticarMensaje) {
					receipt = s.getAcuse();
					acuseFianza.setReciboElectronico(receipt);
				} else {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del mensaje");

					return resultadoProceso; //Termina Proceso
				}
			} catch(Throwable t) {
				throw new AppException("Error en el proceso de autenticacion del mensaje.",t);
			}


			//-----------------Inicia ciclo de validacion del archivo XML -------------
			//Se obtiene informacion del XML y se genera Obj DatosFianzaWS
			//que contiene la info de la Fianza Enviada
			FianzaXMLtoObject  xmlObj = new FianzaXMLtoObject();
			DatosFianzaWS dat = xmlObj.parseDocument(xmlFianza);
			dat.setCcEstatus("001");
			acuseFianza.setEstatusFianza(dat.getCcEstatus());
			//SE REALIZA VALIDACION DE LA INFO DE LA FIANZA
			try{

				erroresEsperados.append(validaInfoFianza(dat));

				if(erroresEsperados.length()>1){
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion(erroresEsperados.toString());
					return resultadoProceso; //Termina Proceso
				}
			}catch(Throwable t){
				erroresInesperados.append("ERROR INESPERADO|Error en la validacion de la info. de la Fianza");
				throw new AppException("Error en la validacion de la info. de la Fianza.",t);
			}

			//SE REALIZA VALIDACION DE CONDICIONES EXTRAS Y EXISTENCIA DE LA MISMA EN N@E
			try{
				erroresEsperados.append(validacionesExtrasFianza(dat,con));
				if(erroresEsperados.length()>1){
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion(erroresEsperados.toString());
					return resultadoProceso; //Termina Proceso
				}
			}catch(Throwable t){
				erroresInesperados.append("ERROR INESPERADO|Error en la validacion de la info. de la Fianza con NE");
				throw new AppException("Error en la validacion de la info. de la Fianza con NE.",t);
			}

			try{
			//se verifica si ya existe el fiado
				PreparedStatement psFiado = null;
				ResultSet rsFiado = null;
				int numFiado  = 0;
				String queryFiado = "SELECT count(1) " +
						"  FROM fe_fiado " +
						" WHERE cg_rfc = ? ";

				psFiado = con.queryPrecompilado(queryFiado);
				psFiado.setString(1, dat.getFiadoRfc());
				rsFiado = psFiado.executeQuery();
				if(rsFiado!=null && rsFiado.next()){
					numFiado  = rsFiado.getInt(1);
					if(numFiado>0)	envSolic = false;
				}
				rsFiado.close();
				psFiado.close();
			}catch(Throwable t){
				erroresInesperados.append("Error en la verificaci�n de si ya existe el fiado \n");
				throw new AppException("Error en la verificaci�n de si ya existe el fiado", t);
			}

			//SE REALIZA ALTA DEL FIADO
			try{
				dat.setIcPyme(afiliaFiadoWS(con,dat,claveUsuario));
			}catch(Throwable t){
				erroresInesperados.append("Error en el alta del Fiado.\n");
				throw new AppException("Error en el alta del Fiado.", t);
			}

			//SE REALIZA GUARDADO DE LA FIANZA
			dat.setCgXmlClob(xmlFianza);
			dat.setBiPdfBlob(pdfFianza);
			try{
				claveFianza = guardaFianzaWS(dat, claveUsuario, acuseFianza.getAcuse(), con);
			}catch(Throwable t){
				erroresInesperados.append("Error en el guardado de la Fianza\n");
				throw new AppException("Error en el guardado de la Fianza", t);
			}

			try{
				List lstFianza = new ArrayList();
				HashMap mapFianza = new HashMap();
				mapFianza.put("CVEFIANZA", claveFianza);
				mapFianza.put("ESTATUS", dat.getCcEstatus());
				lstFianza.add(mapFianza);

				guardaAcuseFianza(con, acuseFianza, lstFianza , "S");

			}catch(Throwable t){
				erroresInesperados.append("Error en el guardado del Acuse generado para la Fianza enviada\n");
				throw new AppException("Error en el guardado del Acuse generado para la Fianza enviada", t);
			}

			try{
				setComisonFianza(claveFianza, con);
			}catch(Throwable t){
				erroresInesperados.append("Error en el calculo de la comision por fianza\n");
				throw new AppException("Error en el calculo de la comision por fianza Fianza enviada", t);
			}


			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
			resultadoProceso.setResumenEjecucion("PROCESO FINALIZADO\n" + "FIANZA CON NUMERO = "+dat.getNumeroFianza()+" CARGADA CON EXITO");

			con.terminaTransaccion(commit);
			if(envSolic){
				setSolicitudUsuario(dat.getIcPyme(),"F",dat, "CONS FIADO", false, true);
			}
			setEnvioCorreoFianzas(acuseFianza.getAcuse(),claveUsuario,"webservice","");
			log.info("procesarFianzasWS(S)");
		}catch(Throwable t){//en este catch caen todos los errores inesperados
			commit = false;
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
			log.error("ERROR INESPERADO|"+erroresInesperados.toString());
			t.printStackTrace();
			return resultadoProceso;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
		return resultadoProceso;
	}


	/**
	 * Metodo del WS que rrealiza la consulta de fianzas que ya hayan sido cargadas
	 * en base a la afianzadora y a los parametros enviados.
	 * @return FianzaWSInfo contiene el arreglo de xml y la informacion del resultado de la consulta
	 * @param estatus
	 * @param fechaAutorizacionFinal
	 * @param fechaAutorizacionInicial
	 * @param rfcProveedor
	 * @param idPoliza
	 * @param numeroFianza
	 * @param folio
	 * @param claveAfianzadora
	 * @param password
	 * @param claveUsuario
	 */
	public FianzaWSInfo consultarFianzaWS (String claveUsuario,
			String password, String claveAfianzadora, String folio, String numeroFianza, String idPoliza, String rfcProveedor , String fechaAutorizacionInicial,
			String fechaAutorizacionFinal, String estatus) {

		FianzaWSInfo resultadoProceso = new FianzaWSInfo();
		StringBuffer query = new StringBuffer();
		String icAfianzadora = "";

		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveAfianzadora == null || claveAfianzadora.equals("")) {
				throw new AppException("Los parametros son requeridos");
			}
			Integer.parseInt(claveAfianzadora);

		} catch(Exception e) {
			log.error("consultarFianzaWS(Error) ", e);
			// Como no entr� al ciclo, se regresan todos los documentos recibidos (si existen)
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveAfianzadora +
					 ")" + ". Error=" + e.getMessage());
			return resultadoProceso;
		}

			AccesoDB con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			int param = 0;
			try{

				try {
					con = new AccesoDB();
				} catch(Throwable t) {
					throw new AppException("Error al generar el objeto de acceso a BD", t);
				}
				//Validacion del usuario y Password y que sea del IF especificado
				UtilUsr utilUsr = new UtilUsr();
				List cuentasAfianzadora = null;
				try {
					cuentasAfianzadora = utilUsr.getUsuariosxAfiliado(claveAfianzadora, "A");
				} catch(Throwable t) {
					throw new AppException("Error al obtener la lista de usuarios de la Afianzadora: " + claveAfianzadora + ".", t);
				}
				log.debug("procesarFianzasWS::Validando claveAfianzadora/usuario");

				if (!cuentasAfianzadora.contains(claveUsuario)) {

					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para la Afianzadora especificada");
					return resultadoProceso; //Termina Proceso
				}

				log.debug("procesarFianzasWS::Validando usuario/passwd");
				try {
					if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
						//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
						resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
						resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
						return resultadoProceso; //Termina Proceso
					}
				} catch(Throwable t) {
					throw new AppException("Error en el componente de validacion de usuario. ", t);
				}

				try{
					Usuario user = utilUsr.getUsuario(claveUsuario);
					icAfianzadora = user.getClaveAfiliado();
					Long.parseLong(icAfianzadora);
				}catch(Throwable t){
					throw new AppException("Error al obtener la clave del afiliado. ", t);
				}

				try {
					con.conexionDB();
				} catch(Throwable t) {
					throw new AppException ("Error al realizar la conexion a la BD. ", t);
				}

				//**********SE LLEVA A CABO LA CONSULTA DE LAS FIANZAS
				query.append(" SELECT ff.ic_fianza, ff.ic_epo, ff.ic_fiado, ff.cg_xml " );
				query.append(" FROM fe_fianza ff " );
				query.append(" WHERE ff.ic_afianzadora = ? ");
				if(folio!=null && !"".equals(folio))
					query.append(" AND ff.cg_folio = ? " );
				if(numeroFianza!=null && !"".equals(numeroFianza))
					query.append(" AND ff.cg_numero_fianza = ? " );
				if(idPoliza!=null && !"".equals(idPoliza))
					query.append(" AND ff.ig_idpoliza = ? " );
				if(rfcProveedor!=null && !"".equals(rfcProveedor))
					query.append(" AND ff.df_ini_vig_fianza >= trunc(to_date(?,'dd/mm/yyyy')) " );
				if(fechaAutorizacionInicial!=null && !"".equals(fechaAutorizacionInicial))
					query.append(" AND ff.df_fin_vig_fianza < trunc(to_date(?,'dd/mm/yyyy')+1) " );
				if(fechaAutorizacionFinal!=null && !"".equals(fechaAutorizacionFinal))
					query.append(" AND ff.cc_ramo = ? " );
				if(estatus!=null && !"".equals(fechaAutorizacionFinal))
					query.append(" AND ff.cc_estatus = ? " );

				ps = con.queryPrecompilado(query.toString());
				ps.setLong(++param,Long.parseLong(icAfianzadora));
				if(folio!=null && !"".equals(folio))
					ps.setString(++param, folio);
				if(numeroFianza!=null && !"".equals(numeroFianza))
					ps.setString(++param, numeroFianza);
				if(idPoliza!=null && !"".equals(idPoliza))
					ps.setLong(++param, Long.parseLong(idPoliza));
				if(rfcProveedor!=null && !"".equals(rfcProveedor))
					ps.setString(++param, rfcProveedor);
				if(fechaAutorizacionInicial!=null && !"".equals(fechaAutorizacionInicial))
					ps.setString(++param,fechaAutorizacionInicial);
				if(fechaAutorizacionFinal!=null && !"".equals(fechaAutorizacionFinal))
					ps.setString(++param, fechaAutorizacionFinal);
				if(estatus!=null && !"".equals(estatus))
					ps.setString(++param, estatus);

				rs = ps.executeQuery();

				//se obtiene arrglo de archivos XML
				List lstXml = new ArrayList();
				String[] arrayXml;
				while(rs!=null && rs.next()){
					StringBuffer sbXml =  new StringBuffer();
					String line = "";
					Clob cXml = rs.getClob("cg_xml");
					Reader reader = cXml.getCharacterStream();
					BufferedReader bread = new BufferedReader(reader);
					while((line = bread.readLine()) != null){
						sbXml.append(line);
					}
					if(sbXml.length()>0){
						lstXml.add(sbXml.toString());
					}
				}
				rs.close();
				ps.close();
				if(lstXml!=null && lstXml.size()>0){
					arrayXml = new String[lstXml.size()];
					for(int x=0; x<lstXml.size(); x++){
						arrayXml[x]	= (String)lstXml.get(x);
					}
				}else{
					arrayXml = null;
				}

				resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
				resultadoProceso.setResumenEjecucion("PROCESO FINALIZADO\n" + "Numero de Fianzaz encontradas "+ (arrayXml==null?0:arrayXml.length));
				resultadoProceso.setXmls(arrayXml);

			}catch(Throwable t){
				resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
				return resultadoProceso;
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}

			return resultadoProceso;
	}


	/**
	 * LLeva acabop validaciones basicas sobre la informacion
	 * de la Fianza enviada por el WS
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param dat
	 */
	private String validaInfoFianza(DatosFianzaWS dat)throws AppException{

		StringBuffer msgError = new StringBuffer();
		//Extraccion de datos del bean de fianza
		/*01*/String versionEsquema = dat.getVersionEsquema()==null?"":dat.getVersionEsquema();
		/*02*/String numeroCompaniaAfianzadora = dat.getNumeroCompaniaAfianzadora()==null?"":dat.getNumeroCompaniaAfianzadora();
		/*03*/String serie = dat.getSerie()==null?"":dat.getSerie();
		/*04*/String folio = dat.getFolio()==null?"":dat.getFolio();
		/*05*/String numeroFianza = dat.getNumeroFianza()==null?"":dat.getNumeroFianza();
		/*06*/String numeroInclusion = dat.getNumeroInclusion()==null?"":dat.getNumeroInclusion();
		/*07*/String numeroVersion = dat.getNumeroVersion()==null?"":dat.getNumeroVersion();
		/*08*/String idPoliza = dat.getIdPoliza()==null?"":dat.getIdPoliza();
		/*09*/String idMovimiento = dat.getIdMovimiento()==null?"":dat.getIdMovimiento();
		/*10*/String idSolicitud = dat.getIdSolicitud()==null?"":dat.getIdSolicitud();
		/*11*/String movimiento = dat.getMovimiento()==null?"":dat.getMovimiento();
		/*12*/String montoAfianzado = dat.getMontoAfianzado()==null?"":dat.getMontoAfianzado();
		/*13*/String montoAfianzadoLetras = dat.getMontoAfianzadoLetras()==null?"":dat.getMontoAfianzadoLetras();
		/*14*/String montoAfianzadoMovimiento = dat.getMontoAfianzadoMovimiento()==null?"":dat.getMontoAfianzadoMovimiento();
		/*15*/String montoAfianzadoMovimientoLetra = dat.getMontoAfianzadoMovimientoLetra()==null?"":dat.getMontoAfianzadoMovimientoLetra();
		/*16*/String ramo = dat.getRamo()==null?"":dat.getRamo();
		/*17*/String subRamo = dat.getSubRamo()==null?"":dat.getSubRamo();
		/*18*/String tipo = dat.getTipo()==null?"":dat.getTipo();
		/*19*/String moneda = dat.getMoneda()==null?"":dat.getMoneda();
		/*20*/String inicioVigenciaFianza = dat.getInicioVigenciaFianza()==null?"":dat.getInicioVigenciaFianza();
		/*21*/String finVigenciaFianza = dat.getFinVigenciaFianza()==null?"":dat.getFinVigenciaFianza();
		/*22*/String fechaSolicitada = dat.getFechaSolicitada()==null?"":dat.getFechaSolicitada();
		/*23*/String fechaAutorizacion = dat.getFechaAutorizacion()==null?"":dat.getFechaAutorizacion();
		/*24*/String documentoFuente = dat.getDocumentoFuente()==null?"":dat.getDocumentoFuente();
		/*25*/String afianzaRfc = dat.getAfianzaRfc()==null?"":dat.getAfianzaRfc();
		/*26*/String afianzaNombre = dat.getAfianzaNombre()==null?"":dat.getAfianzaNombre();
		/*27*/String afianzaCalle = dat.getAfianzaCalle()==null?"":dat.getAfianzaCalle();
		/*28*/String afianzaNoExterior = dat.getAfianzaNoExterior()==null?"":dat.getAfianzaNoExterior();
		/*29*/String afianzaNoInterior = dat.getAfianzaNoInterior()==null?"":dat.getAfianzaNoInterior();
		/*30*/String afianzaColonia = dat.getAfianzaColonia()==null?"":dat.getAfianzaColonia();
		/*31*/String afianzaLocalidad = dat.getAfianzaLocalidad()==null?"":dat.getAfianzaLocalidad();
		/*32*/String afianzaReferencia = dat.getAfianzaReferencia()==null?"":dat.getAfianzaReferencia();
		/*33*/String afianzaMunicipio = dat.getAfianzaMunicipio()==null?"":dat.getAfianzaMunicipio();
		/*34*/String afianzaEstado = dat.getAfianzaEstado()==null?"":dat.getAfianzaEstado();
		/*35*/String afianzaPais = dat.getAfianzaPais()==null?"":dat.getAfianzaPais();
		/*36*/String afianzaCodigoPostal = dat.getAfianzaCodigoPostal()==null?"":dat.getAfianzaCodigoPostal();
		/*37*/String sucCalle = dat.getSucCalle()==null?"":dat.getSucCalle();
		/*38*/String sucNoExterior = dat.getSucNoExterior()==null?"":dat.getSucNoExterior();
		/*39*/String sucNoInterior = dat.getSucNoInterior()==null?"":dat.getSucNoInterior();
		/*40*/String sucColonia = dat.getSucColonia()==null?"":dat.getSucColonia();
		/*41*/String sucLocalidad = dat.getSucLocalidad()==null?"":dat.getSucLocalidad();
		/*42*/String sucReferencia = dat.getSucReferencia()==null?"":dat.getSucReferencia();
		/*43*/String sucMunicipio = dat.getSucMunicipio()==null?"":dat.getSucMunicipio();
		/*44*/String sucEstado = dat.getSucEstado()==null?"":dat.getSucEstado();
		/*45*/String sucPais = dat.getSucPais()==null?"":dat.getSucPais();
		/*46*/String sucCodigoPostal = dat.getSucCodigoPostal()==null?"":dat.getSucCodigoPostal();
		/*47*/String fiadoRfc = dat.getFiadoRfc()==null?"":dat.getFiadoRfc();
		/*48*/String fiadoNombre = dat.getFiadoNombre()==null?"":dat.getFiadoNombre();
		/*49*/String fiadoCalle = dat.getFiadoCalle()==null?"":dat.getFiadoCalle();
		/*50*/String fiadoNoExterior = dat.getFiadoNoExterior()==null?"":dat.getFiadoNoExterior();
		/*51*/String fiadoNoInterior = dat.getFiadoNoInterior()==null?"":dat.getFiadoNoInterior();
		/*52*/String fiadoColonia = dat.getFiadoColonia()==null?"":dat.getFiadoColonia();
		/*53*/String fiadoLocalidad = dat.getFiadoLocalidad()==null?"":dat.getFiadoLocalidad();
		/*54*/String fiadoReferencia = dat.getFiadoReferencia()==null?"":dat.getFiadoReferencia();
		/*55*/String fiadoMunicipio = dat.getFiadoMunicipio()==null?"":dat.getFiadoMunicipio();
		/*56*/String fiadoEstado = dat.getFiadoEstado()==null?"":dat.getFiadoEstado();
		/*57*/String fiadoPais = dat.getFiadoPais()==null?"":dat.getFiadoPais();
		/*58*/String fiadoCodigoPostal = dat.getFiadoCodigoPostal()==null?"":dat.getFiadoCodigoPostal();
		/*59*/String benefRfc = dat.getBenefRfc()==null?"":dat.getBenefRfc();
		/*60*/String benefNombre = dat.getBenefNombre()==null?"":dat.getBenefNombre();
		/*61*/String agenteCoRfc = dat.getAgenteCoRfc()==null?"":dat.getAgenteCoRfc();
		/*62*/String agenteCoNombre = dat.getAgenteCoNombre()==null?"":dat.getAgenteCoNombre();
		/*63*/String prima = dat.getPrima()==null?"":dat.getPrima();
		/*64*/String bonificacion = dat.getBonificacion()==null?"":dat.getBonificacion();
		/*65*/String derechos = dat.getDerechos()==null?"":dat.getDerechos();
		/*66*/String gastosExpedicion = dat.getGastosExpedicion()==null?"":dat.getGastosExpedicion();
		/*67*/String gastosInvestigacion = dat.getGastosInvestigacion()==null?"":dat.getGastosInvestigacion();
		/*68*/String iva = dat.getIva()==null?"":dat.getIva();
		/*69*/String subTotal = dat.getSubTotal()==null?"":dat.getSubTotal();
		/*70*/String primaTotal = dat.getPrimaTotal()==null?"":dat.getPrimaTotal();
		/*71*/String fianzaTexto = dat.getFianzaTexto()==null?"":dat.getFianzaTexto();
		/*72*/String verImpresaNombre = dat.getVerImpresaNombre()==null?"":dat.getVerImpresaNombre();
		/*73*/String verImpresaPuesto = dat.getVerImpresaPuesto()==null?"":dat.getVerImpresaPuesto();
		/*74*/String restriccionTexto = dat.getRestriccionTexto()==null?"":dat.getRestriccionTexto();
		/*75*/String normCNSFTexto = dat.getNormCNSFTexto()==null?"":dat.getNormCNSFTexto();
		/*76*/String benefnafinDestinatario = dat.getBenefnafinDestinatario()==null?"":dat.getBenefnafinDestinatario();
		/*77*/String benefNombreUsuario = dat.getBenefNombreUsuario()==null?"":dat.getBenefNombreUsuario();
		/*78*/String benefCorreoUsuario = dat.getBenefCorreoUsuario()==null?"":dat.getBenefCorreoUsuario();
		/*79*/String benefIdRamo = dat.getBenefIdRamo()==null?"":dat.getBenefIdRamo();
		/*80*/String benefIdSubRamo = dat.getBenefIdSubRamo()==null?"":dat.getBenefIdSubRamo();
		/*81*/String correoRepLegal = dat.getCorreoRepLegal()==null?"":dat.getCorreoRepLegal();
		/*82*/String telefonoRepLegal = dat.getTelefonoRepLegal()==null?"":dat.getTelefonoRepLegal();
		/*83*/String ventasAnuales = dat.getVentasAnuales()==null?"":dat.getVentasAnuales();
		/*84*/String numeroEmpleados = dat.getNumeroEmpleados()==null?"":dat.getNumeroEmpleados();
		/*85*/String sector = dat.getSector()==null?"":dat.getSector();
		/*86*/String certificado = dat.getCertificado()==null?"":dat.getCertificado();
		/*87*/String selloDigital = dat.getSelloDigital()==null?"":dat.getSelloDigital();
		/*88*/String nombreRepLegal = dat.getNombreRepLegal()==null?"":dat.getNombreRepLegal();

		System.out.println("versionEsquema == " + versionEsquema );
		System.out.println("numeroCompaniaAfianzadora == " + numeroCompaniaAfianzadora );
		System.out.println("serie == " + serie );
		System.out.println("folio == " + folio );
		System.out.println("numeroFianza == " + numeroFianza );
		System.out.println("numeroInclusion == " + numeroInclusion );
		System.out.println("numeroVersion == " + numeroVersion );
		System.out.println("idPoliza == " + idPoliza );
		System.out.println("idMovimiento == " + idMovimiento );
		System.out.println("idSolicitud == " + idSolicitud );
		System.out.println("movimiento == " + movimiento );
		System.out.println("montoAfianzado == " + montoAfianzado );
		System.out.println("montoAfianzadoLetras == " + montoAfianzadoLetras );
		System.out.println("montoAfianzadoMovimiento == " + montoAfianzadoMovimiento );
		System.out.println("montoAfianzadoMovimientoLetra == " + montoAfianzadoMovimientoLetra );
		System.out.println("ramo == " + ramo );
		System.out.println("subRamo == " + subRamo );
		System.out.println("tipo == " + tipo );
		System.out.println("moneda == " + moneda );
		System.out.println("inicioVigenciaFianza == " + inicioVigenciaFianza );
		System.out.println("finVigenciaFianza == " + finVigenciaFianza );
		System.out.println("fechaSolicitada == " + fechaSolicitada );
		System.out.println("fechaAutorizacion == " + fechaAutorizacion );
		System.out.println("documentoFuente == " + documentoFuente );
		System.out.println("afianzaRfc == " + afianzaRfc );
		System.out.println("afianzaNombre == " + afianzaNombre );
		System.out.println("afianzaCalle == " + afianzaCalle );
		System.out.println("afianzaNoExterior == " + afianzaNoExterior );
		System.out.println("afianzaNoInterior == " + afianzaNoInterior );
		System.out.println("afianzaColonia == " + afianzaColonia );
		System.out.println("afianzaLocalidad == " + afianzaLocalidad );
		System.out.println("afianzaReferencia == " + afianzaReferencia );
		System.out.println("afianzaMunicipio == " + afianzaMunicipio );
		System.out.println("afianzaEstado == " + afianzaEstado );
		System.out.println("afianzaPais == " + afianzaPais );
		System.out.println("afianzaCodigoPostal == " + afianzaCodigoPostal );
		System.out.println("sucCalle == " + sucCalle );
		System.out.println("sucNoExterior == " + sucNoExterior );
		System.out.println("sucNoInterior == " + sucNoInterior );
		System.out.println("sucColonia == " + sucColonia );
		System.out.println("sucLocalidad == " + sucLocalidad );
		System.out.println("sucReferencia == " + sucReferencia );
		System.out.println("sucMunicipio == " + sucMunicipio );
		System.out.println("sucEstado == " + sucEstado );
		System.out.println("sucPais == " + sucPais );
		System.out.println("sucCodigoPostal == " + sucCodigoPostal );
		System.out.println("fiadoRfc == " + fiadoRfc );
		System.out.println("fiadoNombre == " + fiadoNombre );
		System.out.println("fiadoCalle == " + fiadoCalle );
		System.out.println("fiadoNoExterior == " + fiadoNoExterior );
		System.out.println("fiadoNoInterior == " + fiadoNoInterior );
		System.out.println("fiadoColonia == " + fiadoColonia );
		System.out.println("fiadoLocalidad == " + fiadoLocalidad );
		System.out.println("fiadoReferencia == " + fiadoReferencia );
		System.out.println("fiadoMunicipio == " + fiadoMunicipio );
		System.out.println("fiadoEstado == " + fiadoEstado );
		System.out.println("fiadoPais == " + fiadoPais );
		System.out.println("fiadoCodigoPostal == " + fiadoCodigoPostal);
		System.out.println("benefRfc == " + benefRfc );
		System.out.println("benefNombre == " + benefNombre );
		System.out.println("agenteCoRfc == " + agenteCoRfc );
		System.out.println("agenteCoNombre == " + agenteCoNombre );
		System.out.println("prima == " + prima );
		System.out.println("bonificacion == " + bonificacion );
		System.out.println("derechos == " + derechos );
		System.out.println("gastosExpedicion == " + gastosExpedicion );
		System.out.println("gastosInvestigacion == " + gastosInvestigacion );
		System.out.println("iva == " + iva );
		System.out.println("subTotal == " + subTotal );
		System.out.println("primaTotal == " + primaTotal );
		System.out.println("fianzaTexto == " + fianzaTexto );
		System.out.println("verImpresaNombre == " + verImpresaNombre );
		System.out.println("verImpresaPuesto == " + verImpresaPuesto );
		System.out.println("restriccionTexto == " + restriccionTexto );
		System.out.println("normCNSFTexto == " + normCNSFTexto );
		System.out.println("benefnafinDestinatario == " + benefnafinDestinatario );
		System.out.println("benefNombreUsuario == " + benefNombreUsuario );
		System.out.println("benefCorreoUsuario == " + benefCorreoUsuario );
		System.out.println("benefIdRamo == " + benefIdRamo );
		System.out.println("benefIdSubRamo == " + benefIdSubRamo );
		System.out.println("correoRepLegal == " + correoRepLegal );
		System.out.println("telefonoRepLegal == " + telefonoRepLegal );
		System.out.println("certificado == " + certificado );
		System.out.println("selloDigital == " + selloDigital );
		System.out.println("nombreRepLegal == " + nombreRepLegal );


		try{
			//validaciones generales por cada campo de la fianza
			if( versionEsquema.equals("")){
				msgError.append("ERROR|La Versi�n del esquema es requerida.\n");
			}else if(versionEsquema.length()> 5 ){
				msgError.append("ERROR|Para la Versi�n del esquema la longitud m�xima es 5.\n");
			}

			if(numeroCompaniaAfianzadora.equals("")){
				msgError.append("ERROR|El No. de Afianzadora es requerido. \n");
			}else if(numeroCompaniaAfianzadora.length()> 20 ){
				msgError.append("ERROR|Para el No. de Afianzadora la longitud m�xima es 20.\n");
			}

			if(serie.length()> 2){
				msgError.append("ERROR|Para la Serie la longitud m�xima es 2.\n");
			}else if(!"".equals(serie) && !serie.equals("FE")){
				msgError.append("ERROR|Para el campo Serie el valor esperado es FE.\n");
			}

			if(folio.equals("")){
				msgError.append("ERROR|El Folio es requerido.\n");
			}else if(folio.length()> 50){
				msgError.append("ERROR|Para el Folio la longitud m�xima es 50.\n");
			}else if(!Comunes.esNumero(folio)){
				msgError.append("ERROR|El Folio debe ser un n�mero entero.\n");
			}

			if(numeroFianza.equals("")){
				msgError.append("ERROR|El N�mero de Fianza es requerido. \n");
			}else if(numeroFianza.length()> 50 ){
				msgError.append("ERROR|Para el N�mero de Fianza la longitud m�xima es 50.\n");
			}else{
				/*PENDIENTDE DE VALIDAR ESTE CASO*
				msgError.append("ERROR|El N�mero de Fianza ya se encuentra registrado para el Movimiento 11 (Expedici�n). \n");
				*/



			}

			if(numeroInclusion.length()> 10 ){
				msgError.append("ERROR|Para el N�mero de Inclusi�n la longitud m�xima es 10.\n");
			}

			if(numeroVersion.equals("") ){
				msgError.append("ERROR|El N�mero de Versi�n es requerido. \n");
			}else if(numeroVersion.length()> 3 ){
				msgError.append("ERROR|Para el N�mero de Versi�n la longitud m�xima es 3.\n");
			}

			if(idPoliza.equals("")){
				msgError.append("ERROR|El Id de P�liza es requerido. \n");
			}else if(idPoliza.length()> 10 ){
				msgError.append("ERROR|Para el Id de P�liza la longitud m�xima es 10.\n");
			}

			if(idMovimiento.equals("") ){
				msgError.append("ERROR|El Id de Movimiento es requerido. \n");
			}else if(idMovimiento.length()> 5 ){
				msgError.append("ERROR|Para el Id de Movimiento la longitud m�xima es 5.\n");
			}else{
				/*VALIDACION PENDIENTE
				msgError.append("ERROR|El Id del Movimiento no existe en el cat�logo.\n");
				*/
			}

			if(idSolicitud.length()> 10 ){
				msgError.append("ERROR|Para el Id de Solicitud la longitud m�xima es 10.\n");
			}

			if(movimiento.equals("") ){
				msgError.append("ERROR|El Movimiento es requerido. \n");
			}else if(movimiento.length()> 50 ){
				msgError.append("ERROR|Para el Movimiento la longitud m�xima es 50.\n");
			}

			if(montoAfianzado.equals("")){
				msgError.append("ERROR|El Monto Afianzado Total es requerido. \n");
			}else if(!Comunes.esDecimal(montoAfianzado)){
				msgError.append("ERROR|El Monto Afianzado Total debe ser un dato num�rico.\n");
			}else if(!Comunes.precisionValida(montoAfianzado,21,2)){
				msgError.append("ERROR|Para el Monto Afianzado Total la longitud m�xima es 19,2.\n");
			}

			if(montoAfianzadoLetras.length()> 500 ){
				msgError.append("ERROR|Para el Monto Afianzado en Letras la longitud m�xima es 500.\n");
			}

			if(montoAfianzadoMovimiento.equals("")){
				msgError.append("ERROR|El Monto Afianzado del movimiento es requerido. \n");
			}else if(!Comunes.esDecimal(montoAfianzadoMovimiento) ){
				msgError.append("ERROR|El Monto Afianzado del movimiento debe ser un dato num�rico.\n");
			}else if(!Comunes.precisionValida(montoAfianzadoMovimiento,21,2)){
				msgError.append("ERROR|Para el Monto Afianzado del movimiento la longitud m�xima es 19,2.\n");
			}

			if(montoAfianzadoMovimientoLetra.length()> 500 ){
				msgError.append("ERROR|Para el Monto afianzado del movimiento la longitud m�xima es 500.\n");
			}

			if(ramo.equals("")){
				msgError.append("ERROR|El Ramo es requerido. \n");
			}else if(ramo.length()> 50 ){
				msgError.append("ERROR|Para el Ramo la longitud m�xima es 50.\n");
			}

			if(subRamo.equals("") ){
				msgError.append("ERROR|El Subramo es requerido. \n");
			}else if(subRamo.length()> 50 ){
				msgError.append("ERROR|Para el Subramo la longitud m�xima es 50.\n");
			}

			if(tipo.equals("")){
				msgError.append("ERROR|El Tipo de Fianza es requerido. \n");
			}else if(tipo.length()> 50 ){
				msgError.append("ERROR|Para el Tipo de Fianza la longitud m�xima es 50.\n");
			}

			if(moneda.equals("")){
				msgError.append("ERROR|La Moneda es requerida. \n");
			}else if(moneda.length()> 3 ){
				msgError.append("ERROR|Para la Moneda la longitud m�xima es 3.\n");
			}

			if(inicioVigenciaFianza.length()>19 ){
				msgError.append("ERROR|Para el Inicio de Vigencia de la Fianza la longitud m�xima es 19. \n");
			}else if(!"".equals(inicioVigenciaFianza) && !Comunes.esFechaValida(inicioVigenciaFianza,"yyyy-mm-dd'T'HH:mm:ss")){
				msgError.append("ERROR|Para el Inicio de Vigencia de la Fianza el formato esperado es: AAAA-MM-DDTHH:MM:SS \n");
			}

			if(finVigenciaFianza.length()> 19 ){
				msgError.append("ERROR|Para el Fin de Vigencia de la Fianza la longitud m�xima es 19. \n");
			}else if(!"".equals(finVigenciaFianza) && !Comunes.esFechaValida(finVigenciaFianza,"yyyy-mm-dd'T'HH:mm:ss")){
				msgError.append("ERROR|Para el Fin de Vigencia de la Fianza el formato esperado es: AAAA-MM-DDTHH:MM:SS\n");
			}

			if(fechaSolicitada.equals("")){
				msgError.append("ERROR|La Fecha en que se Solicita la fianza es requerida.\n");
			}else if(fechaSolicitada.length()> 19 ){
				msgError.append("ERROR|Para la Fecha en que se Solicita la fianza la longitud m�xima es 19. \n");
			}else if(!Comunes.esFechaValida(fechaSolicitada,"yyyy-mm-dd'T'HH:mm:ss")){
				msgError.append("ERROR|Para la Fecha en que se Solicita la fianza el formato esperado es: AAAA-MM-DDTHH:MM:SS\n");
			}

			if(fechaAutorizacion.equals("")){
				msgError.append("ERROR|La Fecha de Autorizaci�n de la fianza es requerida.\n");
			}else if(fechaAutorizacion.length()> 19 ){
				msgError.append("ERROR|Para la Fecha de Autorizaci�n de la fianza la longitud m�xima es 19.\n");
			}else if(!Comunes.esFechaValida(fechaAutorizacion,"yyyy-mm-dd'T'HH:mm:ss")){
				msgError.append("ERROR|Para la Fecha de Autorizaci�n de la fianza el formato esperado es: AAAA-MM-DDTHH:MM:SS\n");
			}

			if(documentoFuente.equals("")){
				msgError.append("ERROR|El Identificador del documento fuente es requerido. \n");
			}else if(documentoFuente.length()> 30 ){
				msgError.append("ERROR|Para el Identificador del documento fuente la longitud m�xima es 30.\n");
			}

			if(afianzaRfc.equals("")){
				msgError.append("ERROR|El RFC de la Afianzadora es requerido. \n");
			}else if(afianzaRfc.length()> 15 ){
				msgError.append("ERROR|Para el RFC de la Afianzadora la longitud m�xima es 15.\n");
			}else if(!Comunes.validaRFC(afianzaRfc,'F') && !Comunes.validaRFC(afianzaRfc,'M')){
				msgError.append("ERROR|Para el RFC de la Afianzadora el formato esperado es P.F. XXXX-AAMMDD-XXX / P.M. XXX-AAMMDD-XXX\n");
			}else{
				//VALIDACION PENDIENTE PARA VALIDAR SI EXISTE LA AFIANZADORA
				//msgError.append("ERROR|El RFC de la Afianzadora no existe en N@E favor de verificar.\n");
			}

			if(afianzaNombre.equals("")){
				msgError.append("ERROR|El Nombre de la Afianzadora es requerido.\n");
			}else if(afianzaNombre.length()> 100 ){
				msgError.append("ERROR|Para el Nombre de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaCalle.equals("")){
				msgError.append("ERROR|La calle de la Afianzadora es requerida. \n");
			}else if(afianzaCalle.length()> 100 ){
				msgError.append("ERROR|Para la calle de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaNoExterior.length()> 22 ){
				msgError.append("ERROR|Para el No. Exterior de la Afianzadora la longitud m�xima es 22.\n");
			}

			if(afianzaNoInterior.length()> 22 ){
				msgError.append("ERROR|Para el No. Interior de la Afianzadora la longitud m�xima es 22.\n");
			}

			if(afianzaColonia.length()> 100 ){
				msgError.append("ERROR|Para la Colonia de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaLocalidad.length()> 100 ){
				msgError.append("ERROR|Para la Localidad de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaReferencia.length()> 100 ){
				msgError.append("ERROR|Para la Referencia de la ubicaci�n de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaMunicipio.equals("")){
				msgError.append("ERROR|El Municipio de la Afianzadora es requerido. \n");
			}else if(afianzaMunicipio.length()> 100 ){
				msgError.append("ERROR|Para el Municipio de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaEstado.equals("")){
				msgError.append("ERROR|El Estado de la Afianzadora es requerido. \n");
			}if(afianzaEstado.length()> 100 ){
				msgError.append("ERROR|Para el Estado de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaPais.equals("")){
				msgError.append("ERROR|El Pa�s de la Afianzadora es requerido. \n");
			}else if(afianzaPais.length()> 100 ){
				msgError.append("ERROR|Para el Pa�s de la Afianzadora la longitud m�xima es 100.\n");
			}

			if(afianzaCodigoPostal.equals("")){
				msgError.append("ERROR|El C�digo Postal de la Afianzadora es requerido. \n");
			}else if(afianzaCodigoPostal.length()> 5 ){
				msgError.append("ERROR|Para el C�digo Postal de la Afianzadora la longitud m�xima es 5.\n");
			}

			if(sucCalle.equals("")){
				msgError.append("ERROR|La calle de la Sucursal es requerida. \n");
			}else if(sucCalle.length()> 100 ){
				msgError.append("ERROR|Para la calle de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucNoExterior.length()> 100 ){
				msgError.append("ERROR|Para el No. Exterior de la Sucursal la longitud m�xima es 22.\n");
			}

			if(sucNoInterior.length()> 100 ){
				msgError.append("ERROR|Para el No. Interior de la Sucursal la longitud m�xima es 22.\n");
			}

			if(sucColonia.length()> 100 ){
				msgError.append("ERROR|Para la Colonia de la sucursal la longitud m�xima es 100.\n");
			}

			if(sucLocalidad.length()> 100 ){
				msgError.append("ERROR|Para la Localidad de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucReferencia.length()> 100 ){
				msgError.append("ERROR|Para la Referencia de la ubicaci�n de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucMunicipio.length()> 100 ){
				msgError.append("ERROR|Para el Municipio de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucEstado.length()> 100 ){
				msgError.append("ERROR|Para el Estado de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucPais.equals("")){
				msgError.append("ERROR|El Pa�s de la Sucursal es requerido. \n");
			}else if(sucPais.length()> 100 ){
				msgError.append("ERROR|Para el Pa�s de la Sucursal la longitud m�xima es 100.\n");
			}

			if(sucCodigoPostal.length()> 5 ){
				msgError.append("ERROR|Para el C�digo Postal de la Afianzadora la longitud m�xima es 5.\n");
			}

			if(fiadoRfc.equals("")){
				msgError.append("ERROR|El RFC del Fiado es requerido. \n");
			}else if(fiadoRfc.length()> 100 ){
				msgError.append("ERROR|Para el RFC del Fiado la longitud m�xima es 15.\n");
			}else if(!Comunes.validaRFC(fiadoRfc,'F') && !Comunes.validaRFC(fiadoRfc,'M')){
				msgError.append("ERROR|Para el RFC del Fiado el formato esperado es P.F. XXXX-AAMMDD-XXX o P.M. XXX-AAMMDD-XXX\n");
			}

			if(fiadoNombre.equals("")){
				msgError.append("ERROR|El Nombre del Fiado es requerido.\n");
			}else if(fiadoNombre.length()> 150 ){
				msgError.append("ERROR|Para el Nombre del Fiado la longitud esperada es 150.\n");
			}

			if(fiadoCalle.equals("")){
				msgError.append("ERROR|La calle del Fiado es requerida. \n");
			}else if(fiadoCalle.length()> 100 ){
				msgError.append("ERROR|Para la calle del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoNoExterior.length()> 22 ){
				msgError.append("ERROR|Para el No. Exterior del Fiado la longitud m�xima es 22.\n");
			}

			if(fiadoNoInterior.length()> 100 ){
				msgError.append("ERROR|Para el No. Interior del Fiado la longitud m�xima es 22.\n");
			}

			if(fiadoColonia.length()> 100 ){
				msgError.append("ERROR|Para la Colonia del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoLocalidad.length()>100){
				msgError.append("ERROR|Para la Localidad del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoReferencia.length()>100){
				msgError.append("ERROR|Para la Referencia del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoMunicipio.length()>100){
				msgError.append("ERROR|Para el Municipio del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoEstado.length()>100){
				msgError.append("ERROR|Para el Estado del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoPais.length()>100){
				msgError.append("ERROR|Para el Pa�s del Fiado la longitud m�xima es 100.\n");
			}

			if(fiadoCodigoPostal.equals("")){
				msgError.append("ERROR|Para el C�digo Postal del Fiado es requerido.\n");
			}else if(fiadoCodigoPostal.length()> 5 ){
				msgError.append("ERROR|Para el C�digo Postal del Fiado la longitud m�xima es 5.\n");
			}

			if(benefRfc.length()> 15 ){
				msgError.append("ERROR|Para el RFC del Beneficiario la longitud m�xima es 15.\n");
			}
			if(benefNombre.equals("")){
				msgError.append("ERROR|El Nombre del Beneficiario es requerido.\n");
			}else if(benefNombre.length()> 150 ){
				msgError.append("ERROR|Para el Nombre del Beneficiario la longitud m�xima es 150.\n");
			}

			if(agenteCoRfc.length()> 15 ){
				msgError.append("ERROR|Para el RFC del Agente ela longitud m�xima es 15.\n");
			}else if(!"".equals(agenteCoRfc) && !"".equals(agenteCoRfc) && !Comunes.validaRFC(agenteCoRfc,'F') && !Comunes.validaRFC(agenteCoRfc,'M')){
				msgError.append("ERROR|Para el RFC del Agente el formato esperado es P.F. XXXX-AAMMDD-XXX\n");
			}


			if(agenteCoNombre.equals("")){
				msgError.append("ERROR|El Nombre del Agente Corredor es requerido para fianzas Administrativas.\n");
			}else if(agenteCoNombre.length()> 100 ){
				msgError.append("ERROR|Para el Nombre del Agente corredor la longitud m�xima es 100.\n");
			}

			if(!"".equals(prima) && !Comunes.esDecimal(prima)){
				msgError.append("ERROR|Para la Prima el valor debe ser num�rico.\n");
			}else if(!"".equals(prima) && !Comunes.precisionValida(prima,21,2)){
				msgError.append("ERROR|Para la Prima la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(bonificacion) && !Comunes.esDecimal(bonificacion)){
				msgError.append("ERROR|Para la Bonificaci�n el valor debe ser num�rico.\n");
			}else if(!"".equals(bonificacion) && !Comunes.precisionValida(bonificacion,21,2)){
				msgError.append("ERROR|Para la Bonificaci�n la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(derechos) && !Comunes.esDecimal(derechos)){
				msgError.append("ERROR|Para los Derechos el valor debe ser num�rico.\n");
			}else if(!"".equals(derechos) && !Comunes.precisionValida(derechos,21,2)){
				msgError.append("ERROR|Para los Derechos la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(gastosExpedicion) && !Comunes.esDecimal(gastosExpedicion)){
				msgError.append("ERROR|Para los Gastos de Expedici�n el valor debe ser num�rico.\n");
			}else if(!"".equals(gastosExpedicion) && !Comunes.precisionValida(gastosExpedicion,21,2)){
				msgError.append("ERROR|Para los Gastos de Expedici�n la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(gastosInvestigacion) && !Comunes.esDecimal(gastosInvestigacion)){
				msgError.append("ERROR|Para los Gastos de Investigaci�n el valor debe ser num�rico.\n");
			}else if(!"".equals(gastosInvestigacion) && !Comunes.precisionValida(gastosInvestigacion,21,2)){
				msgError.append("ERROR|Para los Gastos de Investigaci�n la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(iva) && !Comunes.esDecimal(iva)){
				msgError.append("ERROR|Para el IVA el valor debe ser num�rico.\n");
			}else if(!"".equals(iva) && !Comunes.precisionValida(iva,21,2)){
				msgError.append("ERROR|Para el IVA la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(subTotal) && !Comunes.esDecimal(subTotal)){
				msgError.append("ERROR|Para el Subtotal el valor debe ser num�rico.\n");
			}else if(!"".equals(subTotal) && !Comunes.precisionValida(subTotal,21,2)){
				msgError.append("ERROR|Para el Subtotal la longitud m�xima es 19,2.\n");
			}

			if(primaTotal.equals("")){
				msgError.append("ERROR|La Prima Total es un valor requerido.\n");
			}else if(!Comunes.esDecimal(primaTotal)){
				msgError.append("ERROR|Para la Prima Total el valor debe ser num�rico.\n");
			}else if(!Comunes.precisionValida(primaTotal,21,2)){
				msgError.append("ERROR|Para la Prima Total la longitud m�xima es 19,2.\n");
			}

			if(fianzaTexto.equals("")){
				msgError.append("ERROR|El Texto de la Fianza es requerido.\n");
			}else if(fianzaTexto.length()>100){
				//msgError.append("ERROR|El Texto de la Fianza rebasa la longitud m�xima de 2048 MB.\n");
				//PENIENTE VALIDAR TAMA�O DE LA CADENA CLOB
			}

			if(verImpresaNombre.equals("")){
				msgError.append("ERROR|El Nombre de la persona que firma la versi�n impresa es requerido.\n");
			}else if(verImpresaNombre.length()> 100 ){
				msgError.append("ERROR|Para el Nombre de la persona que firma la versi�n impresa la longitud m�xima es 100.\n");
			}

			if(verImpresaPuesto.equals("")){
				msgError.append("ERROR|El Puesto de la persona que firma la versi�n impresa es requerido.\n");
			}else if(verImpresaPuesto.length()> 100 ){
				msgError.append("ERROR|Para el Puesto de la persona que firma la versi�n impresa la longitud m�xima es 100.\n");
			}

			if(restriccionTexto.equals("")){
				msgError.append("ERROR|El Texto de la L�nea es requerido.\n");
			}else if(restriccionTexto.length()> 100 ){
				//msgError.append("ERROR|Para el Texto de la L�nea la longitud m�xima es 2048 MB.\n");
				//falta validacion de tama�o de texto clob
			}

			if(normCNSFTexto.equals("") ){
				msgError.append("ERROR|El Texto de normatividad de la fianza es requerido.\n");
			}else if(normCNSFTexto.length()> 100 ){
				//msgError.append("ERROR|Para el Texto de normatividad de la fianza la longitud m�xima es 2048 MB.\n");
				//falta valiadacion del tama�o del texto clob
			}

			if(benefnafinDestinatario.equals("")){
				msgError.append("ERROR|La clave del Beneficiario en Nafin Electr�nico es requerida.\n");
			}else if(benefnafinDestinatario.length()>20){
				msgError.append("ERROR|La longitud m�xima de la clave del Beneficiario en Nafin Electr�nico es 20.\n");
			}else{
				//FALTA VALIDACION DE LA CLAVE DEL BENEFICIARIO (EPO) EN NAE
				//msgError.append("ERROR|La clave del Beneficiario no existe en Nafin Electr�nico.\n");
			}

			if(benefNombreUsuario.equals("") ){
				msgError.append("ERROR|El Nombre del usuario al que se dirige la Fianza es requerido.  \n");
			}else if(benefNombreUsuario.length()> 100 ){
				msgError.append("ERROR|Para el Nombre del usuario al que se dirige la Fianza la longitud m�xima es 100.\n");
			}

			if(benefCorreoUsuario.equals("")){
				msgError.append("ERROR|El Correo electr�nico del usuario al que se dirige la Fianza es requerido.  \n");
			}else if(benefCorreoUsuario.length()>100){
				msgError.append("ERROR|Para el Correo electr�nico del usuario al que se dirige la Fianza la longitud m�xima es 100.\n");
			}else if(!Comunes.validaEmail(benefCorreoUsuario)){
				msgError.append("ERROR|Para el Correo electr�nico del usuario al que se dirige la Fianza el formato esperado es USUARIO@DOMINIO\n");
			}

			if(benefIdRamo.equals("")){
				msgError.append("ERROR|La clave del Ramo es requerida.\n");
			}else if(benefIdRamo.length()> 3 ){
				msgError.append("ERROR|La longitud m�xima de la clave del Ramo es 3.\n");
			}else{
				//FALTA VALIDACION DE LA EXISTENCIA DE LA CLAVE EN EN EL CATALOGO
				//msgError.append("ERROR|La clave del Ramo no existe en el cat�logo.\n");
			}

			if(benefIdSubRamo.equals("")){
				msgError.append("ERROR|La clave del SubRamo es requerida.\n");
			}else if(benefIdSubRamo.length()> 3 ){
				msgError.append("ERROR|La longitud m�xima de la clave del SubRamo es 3.\n");
			}else{
				//FALTA VALIDACION DE LA CLAVE DEL SUBRAMO EN EL CATALOGO
				//msgError.append("ERROR|La clave del SubRamo no existe en el cat�logo.\n");
			}

			if(nombreRepLegal.equals("")){
				msgError.append("ERROR|El nombre del contacto del Fiado es requerido.\n");
			}if(nombreRepLegal.length()> 100 ){
				msgError.append("ERROR|La longitud m�xima del nombre del contacto del Fiado es 100.\n");
			}

			if(correoRepLegal.equals("")){
				msgError.append("ERROR|El correo electr�nico del contacto del Fiado es requerido.\n");
			}else if(correoRepLegal.length()> 100 ){
				msgError.append("ERROR|La longitud m�xima del correo electr�nico del contacto del Fiado es 100.\n");
			}else if(!Comunes.validaEmail(correoRepLegal)){
				msgError.append("ERROR|Para el Correo electr�nico del Fiado el formato esperado USUARIO@DOMINIO\n");
			}

			if(telefonoRepLegal.equals("")){
				msgError.append("ERROR|El tel�fono del contacto del Fiado es requerido.\n");
			}else if(telefonoRepLegal.length()> 20 ){
				msgError.append("ERROR|La longitud m�xima tel�fono del contacto del Fiado es 20.\n");
			}




			if(!"".equals(ventasAnuales) && !Comunes.esDecimal(ventasAnuales)){
				msgError.append("ERROR|Las Ventas Anuales debe ser un dato num�rico.\n");
			}else if(!Comunes.precisionValida(ventasAnuales,21,2)){
				msgError.append("ERROR|Para las Ventas Anuales la longitud m�xima es 19,2.\n");
			}

			if(!"".equals(numeroEmpleados) && !Comunes.esNumero(numeroEmpleados)){
				msgError.append("ERROR|El Numero de Empleados debe ser un dato num�rico.\n");
			}else if(numeroEmpleados.length()>8){
				msgError.append("ERROR|El Numero de Empleados la longitud m�xima es 8.\n");
			}

			if(!"".equals(sector) && !Comunes.esNumero(sector)){
				msgError.append("ERROR|El Sector debe ser un dato num�rico.\n");
			}else if(sector.length()>1){
				msgError.append("ERROR|Para el Sector la longitud m�xima es 1.\n");
			}

			if(certificado.equals("")){
				msgError.append("ERROR|El Certificado de la Afianzadora es requerido.\n");
			}else if(certificado.length()> 2500 ){
				msgError.append("ERROR|La longitud m�xima para el Certificado de la Afianzadora es 250.\n");
			}

			if(selloDigital.equals("")){
				msgError.append("ERROR|El Sello Digital de la Afianzadora es requerido.\n");
			}else if(selloDigital.length()> 250 ){
				msgError.append("ERROR|La longitud m�xima para el Sello Digital de la Afianzadora es 250.\n");
			}
		}catch(Throwable t){
			throw new AppException("Error en la validacion de la info. de la Fianza... ",t);
		}

		return msgError.toString();

	}//fin

	/**
	 * LLeva a acabo validaciones no tan comunes sobre la fianza que fue enviada via WS
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param con
	 * @param dat
	 */
	private String validacionesExtrasFianza(DatosFianzaWS dat, AccesoDB con) throws AppException{
		String query = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		int existeReg = 0;
		StringBuffer msgError = new StringBuffer();
		try{
			/*VALIDACION PENDIENTE
			msgError.append("ERROR|El Id del Movimiento no existe en el cat�logo.\n");
			*/
			query = "SELECT COUNT (*) " +
						"  FROM fecat_tipo_movimiento " +
						" WHERE cc_tipo_movimiento = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1, dat.getIdMovimiento());
			rs = ps.executeQuery();
			if(rs!=null && rs.next()) existeReg = rs.getInt(1);
			if(existeReg<=0){
				msgError.append("ERROR|El Id del Movimiento no existe en el cat�logo.\n");
			}
			rs.close();
			ps.close();

			//VALIDACION PENDIENTE PARA VALIDAR SI EXISTE LA AFIANZADORA
			//msgError.append("ERROR|El RFC de la Afianzadora no existe en N@E favor de verificar.\n");

			query = "SELECT ic_afianzadora " +
						"  FROM fe_afianzadora " +
						" WHERE cg_rfc = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,dat.getAfianzaRfc().toUpperCase());
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				dat.setIcAfianzadora(rs.getString("ic_afianzadora"));
			}else{
				msgError.append("ERROR|El RFC de la Afianzadora no existe en N@E favor de verificar.\n");
			}
			rs.close();
			ps.close();

			//FALTA VALIDACION DE LA CLAVE DEL BENEFICIARIO (EPO) EN NAE
			//msgError.append("ERROR|La clave del Beneficiario no existe en Nafin Electr�nico.\n");
			query = "SELECT ce.ic_epo " +
						" FROM comcat_epo ce, comrel_nafin cp  " +
						" WHERE ce.ic_epo = cp.ic_epo_pyme_if " +
						" AND cp.cg_tipo  = ? " +
						" AND cp.ic_nafin_electronico = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,"E");
			ps.setLong(2,Long.parseLong(dat.getBenefnafinDestinatario()));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				dat.setIcEpo(rs.getString("ic_epo"));
			}else{
				msgError.append("ERROR|La clave del Destinatario(EPO) no existe en Nafin Electr�nico.\n");
			}
			rs.close();
			ps.close();

			//FALTA VALIDACION DE LA EXISTENCIA DE LA CLAVE EN EN EL CATALOGO
			//
			query = "SELECT count(1) " +
					"  FROM fecat_ramo " +
					" WHERE cc_ramo = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,dat.getBenefIdRamo().toUpperCase());
			rs = ps.executeQuery();
			if(rs!=null && rs.next())existeReg = rs.getInt(1);
			if(existeReg<=0){
				msgError.append("ERROR|La clave del Ramo no existe en el cat�logo.\n");
			}
			rs.close();
			ps.close();


			//VALIDACION DE LA CLAVE DEL SUBRAMO EN EL CATALOGO
			query = "SELECT COUNT (1) " +
					"  FROM fecat_subramo " +
					" WHERE cc_ramo = ? AND cc_subramo = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,dat.getBenefIdRamo().toUpperCase());
			ps.setString(2,dat.getBenefIdSubRamo().toUpperCase());
			rs = ps.executeQuery();
			if(rs!=null && rs.next())existeReg = rs.getInt(1);
			if(existeReg<=0){
				msgError.append("ERROR|La clave del SubRamo no existe en el cat�logo.\n");
			}
			rs.close();
			ps.close();

			//VALIDACION DEL TIPO DE SECTOR EN EL CATALOGO COMCAT_TIPO_SECTOR
			if("".equals(dat.getSector())){
				query = "SELECT COUNT (1) " +
						"  FROM comcat_tipo_sector " +
						" WHERE ic_tipo_sector = ?  ";

				ps = con.queryPrecompilado(query);
				ps.setInt(1,Integer.parseInt(dat.getSector()));
				rs = ps.executeQuery();
				if(rs!=null && rs.next())existeReg = rs.getInt(1);
				if(existeReg<=0){
					msgError.append("ERROR|La Clave del Sector no existe en el cat�logo.\n");
				}
				rs.close();
				ps.close();
			}

			//Para esta Fase del Web Service, �nicamente se podr�n cargar Fianzas del Ramo "Administrativas" (clave 004).
			if(!"004".equals(dat.getBenefIdRamo())){
				msgError.append("ERROR|No se permiten Fianzas que no sean del Ramo 'Adiministrativas' (clave 004) \n");
			}

			//Se validar� que el tipo de Movimiento 11 �Expedici�n� sea enviado una sola vez.
			if("11".equals(dat.getIdMovimiento())){
         /*
				query = "select count(1) "+
							" from bit_fianza_electronica "+//se valida si ya existe
                        " where CG_NUMERO_FIANZA = ? "+
				//			" and IG_IDMOVIMIENTO = ? " +
                     " and cc_estatus!='003'" ; // FODEA 028-2012: SE AGREGA PARA VALIDAR SI LA FIANZA TIENE ESTATUS RECHAZADA
         */
            query = "SELECT COUNT (1) " +
                    " FROM fe_fianza " +
                    " WHERE cg_folio = ?  " +
                    "  AND cg_numero_fianza = ? " +
                    "  AND cc_estatus != '003'";

				ps = con.queryPrecompilado(query);

            ps.setString(1,dat.getFolio().toUpperCase());
            ps.setString(2,dat.getNumeroFianza().toUpperCase());

				rs = ps.executeQuery();
				if(rs!=null && rs.next())existeReg = rs.getInt(1);
				if(existeReg>0){
					//msgError.append("ERROR|La Fianza no puede ser procesada, ya existe la Fianza con movimiento de Expedici�n.\n");
               //FODEA 028-2012: SE CAMBIA EL MENSAJE DE ERROR
               msgError.append("ERROR|El n�mero de Fianza ya se encuentra registrado en el Sistema.\n");
				}
				rs.close();
				ps.close();
			}
			//(SE QUEDO PENDIENTE) El resto de los Movimientos (del cat�logo de movimientos) se podr�n reenviar �n� veces, con la particularidad de que el n�mero de versi�n deber� ser un consecutivo.
		}catch(Throwable t){
			throw new AppException("Error en la validacion Extra de la info. de la Fianza... ",t);
		}
		return msgError.toString();
	}

	/**
	 * Realiza el guardado de la Fianza enviada via WS
	 * @throws netropology.utilerias.AppException
	 * @return int -  claveFianza clave de la fianza generada o que ya existia
	 * @param con
	 * @param claveUsuario
	 * @param dat
	 */
	private String guardaFianzaWS(DatosFianzaWS dat, String claveUsuario, String acuse, AccesoDB con) throws AppException{
		String query = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String claveFianza = "";
		HashMap datAnteriores = new HashMap();
		HashMap datActuales = new HashMap();

		try{
			BitacoraFianza bitacora = new BitacoraFianza();

			query = "select IC_FIANZA from FE_FIANZA where CG_NUMERO_FIANZA = ? and CG_AF_RFC = ? ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,dat.getNumeroFianza());
			ps.setString(2,dat.getAfianzaRfc().toString());
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				claveFianza = rs.getString("IC_FIANZA")==null?"":rs.getString("IC_FIANZA");
			}
			rs.close();
			ps.close();

			datAnteriores = bitacora.getInfoFianza(con, claveFianza);

			Clob cXmlFianza = null;
			Clob cTtxtFianza = null;
			Clob cTxtRest = null;
			Clob cTxtCNSF = null;

			if("".equals(claveFianza)){
				query = "select seq_fe_fianza.nextval as icfianza from dual ";
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					claveFianza = rs.getString("icfianza")==null?"":rs.getString("icfianza");
				}
				rs.close();
				ps.close();

				query = "insert into FE_FIANZA( "+
							"	IC_FIANZA,  IC_EPO,  IC_FIADO,  CC_ESTATUS,  CG_NUMERO_CONTRATO,   " +
							"	DF_VENCIMIENTO,  DF_PUBLICACION,  CG_VERSION_ESQ,  CG_NUM_CIA_AFIANZA,  CG_SERIE,  " +
							"	CG_FOLIO,  CG_NUMERO_FIANZA,  CG_NUM_INCLUSION, CG_NUM_VERSION,  IG_IDPOLIZA,  " +
							"	IG_IDMOVIMIENTO,  IG_IDSOLICITUD,  CG_MOVIMIENTO, FG_MONTO,  CG_MONTO_LETRA,  " +
							"	FG_MONTO_MOVIMIENTO, CG_MONTO_MOV_LETRA,  CC_RAMO, CC_SUBRAMO,  CG_TIPO_FIANZA, " +
							"	CG_MONEDA,  DF_INI_VIG_FIANZA,  DF_FIN_VIG_FIANZA, DF_SOLICITUD,  DF_AUTORIZACION, " +
							"	CG_AF_RFC,  CG_AF_NOMBRE, CG_AF_CALLE,  CG_AF_NOEXTERIOR,  CG_AF_NOINTERIOR, " +
							"	CG_AF_COLONIA,  CG_AF_LOCALIDAD, CG_AF_REFERENCIA,  CG_AF_MUNICIPIO,  CG_AF_ESTADO, " +
							"	CG_AF_PAIS, CG_AF_CP, CG_SUC_CALLE,  CG_SUC_NOEXTERIOR,  CG_SUC_NOINTERIOR, " +
							"	CG_SUC_COLONIA,  CG_SUC_LOCALIDAD, CG_SUC_REFERENCIA,  CG_SUC_MUNICIPIO,  CG_SUC_ESTADO, " +
							"	CG_SUC_PAIS,  CG_SUC_CP, CG_FIADO_RFC,  CG_FIADO_NMOMBRE,  CG_FIADO_CALLE, " +
							"	CG_FIADO_NOEXTERIOR,  CG_FIADO_NOINTERIOR, CG_FIADO_COLONIA,  CG_FIADO_LOCALIDAD,  CG_FIADO_REFERENCIA, "+
							"	CG_FIADO_MUNICIPIO,  CG_FIADO_ESTADO, CG_FIADO_PAIS,  CG_FIADO_CP,  CG_BENF_RFC, " +
							"	CG_BENF_NOMBRE,  CG_AGC_RFC, CG_AGC_NOMBRE,  FG_PRIMA,  FG_BONIFICACION, " +
							"	FG_DERECHOS,  FG_GASTOS_EXPED, FG_GASTOS_INVEST,  FG_IVA,  FG_SUBTOTAL, " +
							"	FG_PRIMA_TOTAL, CG_FIRMA_NOMBRE,  CG_FIRMA_PUESTO,  IG_BENF_NAFIN_DEST, CG_BENF_NOMBRE_USR, " +
							"	CG_BENF_CORREO_USR,  CC_BENF_IDRAMO,  CC_BENF_IDSUBRAMO,  CG_RL_NOMBRE, CG_RL_CORREO,   " +
							"	CG_RL_TELEFONO,  CG_SEG_CERTIFICADO,  CG_SEG_SELLO_DIG,  CG_OBSERVACIONES, IC_AFIANZADORA, "+
							"	CC_ACUSE, CG_TXT_FIANZA, CG_TXT_RESTRICCION,  CG_TXT_NORM_CNSF, CG_XML,  BI_PDF ) " +
							" values(?, ?, ?, ?, ?, "+
							" 	 to_date(?,'yyyy-mm-dd HH24:mi:ss'), sysdate, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, "+
							"	 ?, ?, ?, ?, ?,  " +
							"        ?, ?, ?, ?, ?, " +
							"	 ?, to_date(?,'yyyy-mm-dd HH24:mi:ss'), to_date(?,'yyyy-mm-dd HH24:mi:ss'), to_date(?,'yyyy-mm-dd HH24:mi:ss'), to_date(?,'yyyy-mm-dd HH24:mi:ss'), " +
							"	 ?, ?, ?, ?, ?, " +
							"	 ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, ?, ?, ?, ?, " +
							"        ?, empty_clob(), empty_clob(), empty_clob(), empty_clob(), empty_blob()) ";

				int p = 0;
				ps = con.queryPrecompilado(query);
				/*01*/ps.setLong(++p,Long.parseLong(claveFianza));
				/*02*/ps.setLong(++p,Long.parseLong(dat.getIcEpo()));
				/*03*/ps.setLong(++p,Long.parseLong(dat.getIcPyme()));
				/*04*/ps.setString(++p,dat.getCcEstatus());//001
				/*05*/ps.setString(++p,dat.getDocumentoFuente());

				/*06*/ps.setString(++p,dat.getFinVigenciaFianza().replace('T',' '));//
				/*07*/ps.setString(++p,dat.getVersionEsquema());
				/*08*/ps.setString(++p,dat.getNumeroCompaniaAfianzadora());
				/*09*/ps.setString(++p,dat.getSerie());

				/*10*/ps.setString(++p,dat.getFolio());
				/*11*/ps.setString(++p,dat.getNumeroFianza());
				/*12*/ps.setString(++p,dat.getNumeroInclusion());
				/*13*/ps.setString(++p,dat.getNumeroVersion());
				/*14*/ps.setString(++p,dat.getIdPoliza());	//no se controla el valor vacio ya que es obligatorio el parametro

				/*15*/ps.setLong(++p,Long.parseLong(dat.getIdMovimiento())); //no se controla el valor vacio ya que es obligatorio el parametro
				/*16*/ps.setString(++p,dat.getIdSolicitud());
				/*17*/ps.setString(++p,dat.getMovimiento());
				/*18*/ps.setBigDecimal(++p,new BigDecimal(dat.getMontoAfianzado()));//no se controla el valor vacio ya que es obligatorio el parametro
				/*19*/ps.setString(++p,dat.getMontoAfianzadoLetras());

				/*20*/ps.setBigDecimal(++p,new BigDecimal(dat.getMontoAfianzadoMovimiento()));//no se controla el valor vacio ya que es obligatorio el parametro
				/*21*/ps.setString(++p,dat.getMontoAfianzadoMovimientoLetra());
				/*22*/ps.setString(++p,dat.getRamo());
				/*23*/ps.setString(++p,dat.getSubRamo());
				/*24*/ps.setString(++p,dat.getTipo());
				/*25*/ps.setString(++p,dat.getMoneda());
				/*26*/ps.setString(++p,dat.getInicioVigenciaFianza().replace('T',' '));
				/*27*/ps.setString(++p,dat.getFinVigenciaFianza().replace('T',' '));
				/*28*/ps.setString(++p,dat.getFechaSolicitada().replace('T',' '));
				/*29*/ps.setString(++p,dat.getFechaAutorizacion().replace('T',' '));
				//ps.setString(31,"cg_docto_fuente");//es el  mismo campo que cg_numero_contrato
				ps.setString(++p,dat.getAfianzaRfc());
				ps.setString(++p,dat.getAfianzaNombre());
				ps.setString(++p,dat.getAfianzaCalle());
				ps.setString(++p,dat.getAfianzaNoExterior());
				ps.setString(++p,dat.getAfianzaNoInterior());
				ps.setString(++p,dat.getAfianzaColonia());
				ps.setString(++p,dat.getAfianzaLocalidad());
				ps.setString(++p,dat.getAfianzaReferencia());
				ps.setString(++p,dat.getAfianzaMunicipio());
				ps.setString(++p,dat.getAfianzaEstado());
				ps.setString(++p,dat.getAfianzaPais());
				ps.setString(++p,dat.getAfianzaCodigoPostal());
				ps.setString(++p,dat.getSucCalle());
				ps.setString(++p,dat.getSucNoExterior());
				ps.setString(++p,dat.getSucNoInterior());
				ps.setString(++p,dat.getSucColonia());
				ps.setString(++p,dat.getSucLocalidad());
				ps.setString(++p,dat.getSucReferencia());
				ps.setString(++p,dat.getSucMunicipio());
				ps.setString(++p,dat.getSucEstado());
				ps.setString(++p,dat.getSucPais());
				ps.setString(++p,dat.getSucCodigoPostal());
				ps.setString(++p,dat.getFiadoRfc());
				ps.setString(++p,dat.getFiadoNombre());
				ps.setString(++p,dat.getFiadoCalle());
				ps.setString(++p,dat.getFiadoNoExterior());
				ps.setString(++p,dat.getFiadoNoInterior());
				ps.setString(++p,dat.getFiadoColonia());
				ps.setString(++p,dat.getFiadoLocalidad());
				ps.setString(++p,dat.getFiadoReferencia());
				ps.setString(++p,dat.getFiadoMunicipio());
				ps.setString(++p,dat.getFiadoEstado());
				ps.setString(++p,dat.getFiadoPais());
				ps.setString(++p,dat.getFiadoCodigoPostal());
				ps.setString(++p,dat.getBenefRfc());
				ps.setString(++p,dat.getBenefNombre());
				ps.setString(++p,dat.getAgenteCoRfc());
				ps.setString(++p,dat.getAgenteCoNombre());

				if(dat.getPrima()!=null && !"".equals(dat.getPrima()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getPrima()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getBonificacion()!=null && !"".equals(dat.getBonificacion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getBonificacion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getDerechos()!=null && !"".equals(dat.getDerechos()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getDerechos()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getGastosExpedicion()!=null && !"".equals(dat.getGastosExpedicion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getGastosExpedicion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getGastosInvestigacion()!=null && !"".equals(dat.getGastosInvestigacion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getGastosInvestigacion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getIva()!=null && !"".equals(dat.getIva()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getIva()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getSubTotal()!=null && !"".equals(dat.getSubTotal()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getSubTotal()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getPrimaTotal()!=null && !"".equals(dat.getPrimaTotal()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getPrimaTotal()));
				else ps.setNull(++p,Types.DOUBLE);


				ps.setString(++p,dat.getVerImpresaNombre());
				ps.setString(++p,dat.getVerImpresaPuesto());

				ps.setLong(++p,Long.parseLong(dat.getBenefnafinDestinatario()));
				ps.setString(++p,dat.getBenefNombreUsuario());
				ps.setString(++p,dat.getBenefCorreoUsuario());
				ps.setString(++p,dat.getBenefIdRamo());
				ps.setString(++p,dat.getBenefIdSubRamo());
				ps.setString(++p,dat.getNombreRepLegal());
				ps.setString(++p,dat.getCorreoRepLegal());
				ps.setString(++p,dat.getTelefonoRepLegal());
				ps.setString(++p,dat.getCertificado());
				ps.setString(++p,dat.getSelloDigital());
				ps.setString(++p,dat.getObservaciones());
				ps.setLong(++p, Long.parseLong(dat.getIcAfianzadora()));
				ps.setString(++p,acuse);
				//ps.setBinaryStream(++p, new ByteArrayInputStream(dat.getRestriccionTexto().getBytes()),dat.getRestriccionTexto().getBytes().length);//pendiente de guardar como clob
				//ps.setBinaryStream(++p, new ByteArrayInputStream(dat.getNormCNSFTexto().getBytes()),dat.getNormCNSFTexto().getBytes().length);//pendiente de guardar como clob
				//ps.setBinaryStream(++p, new ByteArrayInputStream(dat.getFianzaTexto().getBytes()),dat.getFianzaTexto().getBytes().length);//pendiente de guardar como clob
				//ps.setBinaryStream(++p, new ByteArrayInputStream(dat.getCgXmlClob()),dat.getCgXmlClob().length);// guardado clob
				//ps.setBinaryStream(++p,new ByteArrayInputStream(dat.getBiPdfBlob()),dat.getBiPdfBlob().length);//guardado blob
				ps.executeUpdate();
				ps.close();

				query = "SELECT cg_txt_fianza, cg_txt_restriccion,  cg_txt_norm_cnsf, cg_xml  "+
						  " from fe_fianza " +
						  " where ic_fianza  = ? ";
				ps = con.queryPrecompilado(query);
				ps.setLong(1,Long.parseLong(claveFianza));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					cXmlFianza = rs.getClob("cg_xml");
					cTtxtFianza = rs.getClob("cg_txt_fianza");
					cTxtRest = rs.getClob("cg_txt_restriccion");
					cTxtCNSF = rs.getClob("cg_txt_norm_cnsf");
				}
				rs.close();
				ps.close();

				cXmlFianza.setString(1,new String(dat.getCgXmlClob()));
				cTtxtFianza.setString(1,dat.getFianzaTexto());
				cTxtRest.setString(1,dat.getRestriccionTexto());
				cTxtCNSF.setString(1,dat.getNormCNSFTexto());

				query = "UPDATE fe_fianza " +
							"   SET cg_xml = ?, " +
							"       bi_pdf = ?, " +
							"		  cg_txt_fianza = ?, " +
							"		  cg_txt_restriccion = ?, " +
							"		  cg_txt_norm_cnsf = ? " +
							" WHERE ic_fianza = ? ";

				ps = con.queryPrecompilado(query);
				//ps.setBinaryStream(1, new ByteArrayInputStream(dat.getCgXmlClob()),(int)dat.getCgXmlClob().length);// guardado clob
				ps.setClob(1, cXmlFianza);// guardado clob
				ps.setBinaryStream(2,new ByteArrayInputStream(dat.getBiPdfBlob()),dat.getBiPdfBlob().length);//guardado blob
				ps.setClob(3, cTtxtFianza);// guardado clob
				ps.setClob(4, cTxtRest);// guardado clob
				ps.setClob(5, cTxtCNSF);// guardado clob
				ps.setLong(6,Long.parseLong(claveFianza));
				ps.executeUpdate();
				ps.close();

			}else{
				query = "SELECT cg_txt_fianza, cg_txt_restriccion,  cg_txt_norm_cnsf, cg_xml  "+
						  " from fe_fianza " +
						  " where ic_fianza  = ? FOR UPDATE ";
				ps = con.queryPrecompilado(query);
				ps.setLong(1,Long.parseLong(claveFianza));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					cXmlFianza = rs.getClob("cg_xml");
					cTtxtFianza = rs.getClob("cg_txt_fianza");
					cTxtRest = rs.getClob("cg_txt_restriccion");
					cTxtCNSF = rs.getClob("cg_txt_norm_cnsf");
				}
				rs.close();
				ps.close();

				cXmlFianza.setString(1,new String(dat.getCgXmlClob()));
				cTtxtFianza.setString(1,dat.getFianzaTexto());
				cTxtRest.setString(1,dat.getRestriccionTexto());
				cTxtCNSF.setString(1,dat.getNormCNSFTexto());

				query = "update  FE_FIANZA set " +
							" IC_EPO = ?,  IC_FIADO = ?,  CC_ESTATUS = ?,  CG_NUMERO_CONTRATO = ?, DF_VENCIMIENTO = to_date(?,'yyyy-mm-dd HH24:mi:ss'), " +
							"	DF_PUBLICACION = sysdate, CG_VERSION_ESQ = ?, CG_NUM_CIA_AFIANZA  = ?,  CG_SERIE  = ?,  CG_FOLIO = ?, " +
							"	CG_NUMERO_FIANZA = ?,  CG_NUM_INCLUSION = ?, CG_NUM_VERSION = ?,  IG_IDPOLIZA = ?,  IG_IDMOVIMIENTO = ?,  " +
							"	IG_IDSOLICITUD = ?,  CG_MOVIMIENTO = ?, FG_MONTO = ?,  CG_MONTO_LETRA = ?,  FG_MONTO_MOVIMIENTO = ?, " +
							"	CG_MONTO_MOV_LETRA = ?,  CC_RAMO = ?, CC_SUBRAMO = ?,  CG_TIPO_FIANZA = ?,  CG_MONEDA = ?, "+
							"	DF_INI_VIG_FIANZA = to_date(?,'yyyy-mm-dd HH24:mi:ss'),  DF_FIN_VIG_FIANZA =  to_date(?,'yyyy-mm-dd HH24:mi:ss'), "+
							"	DF_SOLICITUD = to_date(?,'yyyy-mm-dd HH24:mi:ss'),  DF_AUTORIZACION = to_date(?,'yyyy-mm-dd HH24:mi:ss'), CG_AF_RFC = ?, " +
							"	CG_AF_NOMBRE = ?, CG_AF_CALLE = ?,  CG_AF_NOEXTERIOR = ?,  CG_AF_NOINTERIOR = ?,  CG_AF_COLONIA = ?,  " +
							"	CG_AF_LOCALIDAD = ?, CG_AF_REFERENCIA = ?,  CG_AF_MUNICIPIO = ?,  CG_AF_ESTADO = ?,  CG_AF_PAIS = ?,  " +
							"	CG_AF_CP = ?, CG_SUC_CALLE = ?,  CG_SUC_NOEXTERIOR = ?,  CG_SUC_NOINTERIOR = ?,  CG_SUC_COLONIA = ?, " +
							"	CG_SUC_LOCALIDAD = ?, CG_SUC_REFERENCIA = ?,  CG_SUC_MUNICIPIO = ?,  CG_SUC_ESTADO = ?,  CG_SUC_PAIS = ?, " +
							"	CG_SUC_CP = ?, CG_FIADO_RFC = ?,  CG_FIADO_NMOMBRE = ?,  CG_FIADO_CALLE = ?,  CG_FIADO_NOEXTERIOR = ?, " +
							"	CG_FIADO_NOINTERIOR = ?, CG_FIADO_COLONIA = ?,  CG_FIADO_LOCALIDAD = ?,  CG_FIADO_REFERENCIA = ?,  CG_FIADO_MUNICIPIO = ?, " +
							"	CG_FIADO_ESTADO = ?, CG_FIADO_PAIS = ?,  CG_FIADO_CP = ?,  CG_BENF_RFC = ?,  CG_BENF_NOMBRE = ?, " +
							"	CG_AGC_RFC = ?, CG_AGC_NOMBRE = ?,  FG_PRIMA = ?,  FG_BONIFICACION = ?,  FG_DERECHOS = ?, " +
							"	FG_GASTOS_EXPED = ?, FG_GASTOS_INVEST = ?,  FG_IVA = ?,  FG_SUBTOTAL = ?,  FG_PRIMA_TOTAL = ?, " +
							"	CG_FIRMA_NOMBRE = ?,  CG_FIRMA_PUESTO = ?, IG_BENF_NAFIN_DEST = ?, CG_BENF_NOMBRE_USR = ?,  CG_BENF_CORREO_USR = ?, " +
							"	CC_BENF_IDRAMO = ?,  CC_BENF_IDSUBRAMO = ?,  CG_RL_NOMBRE = ?, CG_RL_CORREO = ?, CG_RL_TELEFONO = ?, " +
							"	CG_SEG_CERTIFICADO = ?,  CG_SEG_SELLO_DIG = ?,  CG_OBSERVACIONES = ?, IC_AFIANZADORA = ?, CC_ACUSE = ?, CG_TXT_FIANZA = ?, " +
							"	CG_TXT_RESTRICCION = ?,  CG_TXT_NORM_CNSF = ?, CG_XML = ?,  BI_PDF = ?  "+
							"where IC_FIANZA = ?   ";

				int p = 0;
				ps = con.queryPrecompilado(query);
				/*02*/ps.setLong(++p,Long.parseLong(dat.getIcEpo()));
				/*03*/ps.setLong(++p,Long.parseLong(dat.getIcPyme()));
				/*04*/ps.setString(++p,dat.getCcEstatus());//001
				/*05*/ps.setString(++p,dat.getDocumentoFuente());

				/*06*/ps.setString(++p,dat.getFinVigenciaFianza().replace('T',' '));//
				/*07*/ps.setString(++p,dat.getVersionEsquema());
				/*08*/ps.setString(++p,dat.getNumeroCompaniaAfianzadora());
				/*09*/ps.setString(++p,dat.getSerie());

				/*10*/ps.setString(++p,dat.getFolio());
				/*11*/ps.setString(++p,dat.getNumeroFianza());
				/*12*/ps.setString(++p,dat.getNumeroInclusion());
				/*13*/ps.setString(++p,dat.getNumeroVersion());
				/*14*/ps.setString(++p,dat.getIdPoliza());	//no se controla el valor vacio ya que es obligatorio el parametro

				/*15*/ps.setLong(++p,Long.parseLong(dat.getIdMovimiento())); //no se controla el valor vacio ya que es obligatorio el parametro
				/*16*/ps.setString(++p,dat.getIdSolicitud());
				/*17*/ps.setString(++p,dat.getMovimiento());
				/*18*/ps.setBigDecimal(++p,new BigDecimal(dat.getMontoAfianzado()));//no se controla el valor vacio ya que es obligatorio el parametro
				/*19*/ps.setString(++p,dat.getMontoAfianzadoLetras());

				/*20*/ps.setBigDecimal(++p,new BigDecimal(dat.getMontoAfianzadoMovimiento()));//no se controla el valor vacio ya que es obligatorio el parametro
				/*21*/ps.setString(++p,dat.getMontoAfianzadoMovimientoLetra());
				/*22*/ps.setString(++p,dat.getRamo());
				/*23*/ps.setString(++p,dat.getSubRamo());
				/*24*/ps.setString(++p,dat.getTipo());
				/*25*/ps.setString(++p,dat.getMoneda());
				/*26*/ps.setString(++p,dat.getInicioVigenciaFianza().replace('T',' '));
				/*27*/ps.setString(++p,dat.getFinVigenciaFianza().replace('T',' '));
				/*28*/ps.setString(++p,dat.getFechaSolicitada().replace('T',' '));
				/*29*/ps.setString(++p,dat.getFechaAutorizacion().replace('T',' '));
				//ps.setString(31,"cg_docto_fuente");//es el  mismo campo que cg_numero_contrato
				ps.setString(++p,dat.getAfianzaRfc());
				ps.setString(++p,dat.getAfianzaNombre());
				ps.setString(++p,dat.getAfianzaCalle());
				ps.setString(++p,dat.getAfianzaNoExterior());
				ps.setString(++p,dat.getAfianzaNoInterior());
				ps.setString(++p,dat.getAfianzaColonia());
				ps.setString(++p,dat.getAfianzaLocalidad());
				ps.setString(++p,dat.getAfianzaReferencia());
				ps.setString(++p,dat.getAfianzaMunicipio());
				ps.setString(++p,dat.getAfianzaEstado());
				ps.setString(++p,dat.getAfianzaPais());
				ps.setString(++p,dat.getAfianzaCodigoPostal());
				ps.setString(++p,dat.getSucCalle());
				ps.setString(++p,dat.getSucNoExterior());
				ps.setString(++p,dat.getSucNoInterior());
				ps.setString(++p,dat.getSucColonia());
				ps.setString(++p,dat.getSucLocalidad());
				ps.setString(++p,dat.getSucReferencia());
				ps.setString(++p,dat.getSucMunicipio());
				ps.setString(++p,dat.getSucEstado());
				ps.setString(++p,dat.getSucPais());
				ps.setString(++p,dat.getSucCodigoPostal());
				ps.setString(++p,dat.getFiadoRfc());
				ps.setString(++p,dat.getFiadoNombre());
				ps.setString(++p,dat.getFiadoCalle());
				ps.setString(++p,dat.getFiadoNoExterior());
				ps.setString(++p,dat.getFiadoNoInterior());
				ps.setString(++p,dat.getFiadoColonia());
				ps.setString(++p,dat.getFiadoLocalidad());
				ps.setString(++p,dat.getFiadoReferencia());
				ps.setString(++p,dat.getFiadoMunicipio());
				ps.setString(++p,dat.getFiadoEstado());
				ps.setString(++p,dat.getFiadoPais());
				ps.setString(++p,dat.getFiadoCodigoPostal());
				ps.setString(++p,dat.getBenefRfc());
				ps.setString(++p,dat.getBenefNombre());
				ps.setString(++p,dat.getAgenteCoRfc());
				ps.setString(++p,dat.getAgenteCoNombre());

				if(dat.getPrima()!=null && !"".equals(dat.getPrima()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getPrima()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getBonificacion()!=null && !"".equals(dat.getBonificacion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getBonificacion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getDerechos()!=null && !"".equals(dat.getDerechos()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getDerechos()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getGastosExpedicion()!=null && !"".equals(dat.getGastosExpedicion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getGastosExpedicion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getGastosInvestigacion()!=null && !"".equals(dat.getGastosInvestigacion()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getGastosInvestigacion()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getIva()!=null && !"".equals(dat.getIva()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getIva()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getSubTotal()!=null && !"".equals(dat.getSubTotal()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getSubTotal()));
				else ps.setNull(++p,Types.DOUBLE);

				if(dat.getPrimaTotal()!=null && !"".equals(dat.getPrimaTotal()))
					ps.setBigDecimal(++p,new BigDecimal(dat.getPrimaTotal()));
				else ps.setNull(++p,Types.DOUBLE);


				ps.setString(++p,dat.getVerImpresaNombre());
				ps.setString(++p,dat.getVerImpresaPuesto());

				ps.setLong(++p,Long.parseLong(dat.getBenefnafinDestinatario()));
				ps.setString(++p,dat.getBenefNombreUsuario());
				ps.setString(++p,dat.getBenefCorreoUsuario());
				ps.setString(++p,dat.getBenefIdRamo());
				ps.setString(++p,dat.getBenefIdSubRamo());
				ps.setString(++p,dat.getNombreRepLegal());
				ps.setString(++p,dat.getCorreoRepLegal());
				ps.setString(++p,dat.getTelefonoRepLegal());
				ps.setString(++p,dat.getCertificado());
				ps.setString(++p,dat.getSelloDigital());
				ps.setString(++p,dat.getObservaciones());
				ps.setLong(++p, Long.parseLong(dat.getIcAfianzadora()));
				ps.setString(++p,acuse);
				ps.setClob(++p, cTtxtFianza);// guardado clob
				ps.setClob(++p, cTxtRest);// guardado clob
				ps.setClob(++p, cTxtCNSF);// guardado clob
				ps.setClob(++p, cXmlFianza);// guardado clob
				ps.setBinaryStream(++p,new ByteArrayInputStream(dat.getBiPdfBlob()),dat.getBiPdfBlob().length);//guardado blob

				ps.setLong(++p, Long.parseLong(claveFianza));
				//ps.setString(95,dat.getNumeroFianza());
				ps.executeUpdate();
				ps.close();
			}

			datActuales = bitacora.getInfoFianza(con, claveFianza);
			BitacoraFianza.agregarRegBitacora(con, claveUsuario, claveFianza, datAnteriores, datActuales);

			return claveFianza;

		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error en el guardado de la Fianza",t);
		}
	}//fin metodo

	/**
	 * Afilia a los fiados involucrados en caada fianza enviada por el WS en N@E
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param con conexion de la bd
	 * @param datfianza objeto que contien la info de la fianza
	 */
	private String afiliaFiadoWS(AccesoDB con, DatosFianzaWS datfianza, String claveUsuario)throws AppException{
		log.info("afiliaFiadoWS(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String icFiado = "";
		String nafElecFiado = "";
		int contProv = 0;
		int param = 0;
		try{
			//se verifica si el fiado existe como provedor
			query = "SELECT COUNT (1) AS contador " +
			"  FROM comcat_pyme cp, comrel_pyme_epo ce " +
			" WHERE cp.ic_pyme = ce.ic_pyme " +
			"   AND cp.cg_rfc = UPPER (?) " +
			"   AND ce.cs_aceptacion IN (?, ?) " +
			"	 AND cp.cs_habilitado = ? ";
			ps = con.queryPrecompilado(query);
			ps.setString(1, datfianza.getFiadoRfc());
			ps.setString(2, "H");
			ps.setString(3, "R");
			ps.setString(4, "S");
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				contProv  = rs.getInt("contador");
			}
			rs.close();
			ps.close();


			//se verifica si ya existe el fiado
			query = "SELECT ic_fiado " +
					"  FROM fe_fiado " +
					" WHERE cg_rfc = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1, datfianza.getFiadoRfc());
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				icFiado  = rs.getString("ic_fiado")==null?"":rs.getString("ic_fiado");
			}
			rs.close();
			ps.close();

			if(contProv>0){
				query = " UPDATE comcat_pyme cp " +
						"   SET cp.cs_fianza_electronica = ? " +
						" WHERE cp.cg_rfc = UPPER (?) AND cp.cs_habilitado = ? ";

				ps = con.queryPrecompilado(query);
				ps.setString(1,"S");
				ps.setString(2,datfianza.getFiadoRfc());
				ps.setString(3,"S");
				ps.executeUpdate();
				ps.close();
			}

			if("".equals(icFiado)){
				//insercion de un nuevo fiado
				query = "select seq_fe_fiado.NEXTVAL as icfiado from dual ";
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					icFiado = rs.getString("icfiado");
				}
				rs.close();
				ps.close();


				double puntaje = 0.0;
				String ic_estrato = "";
				String numEmpleados = (datfianza.getNumeroEmpleados()!=null && !"".equals(datfianza.getNumeroEmpleados()))?datfianza.getNumeroEmpleados():"";
				String ventasAnuales = (datfianza.getVentasAnuales()!=null && !"".equals(datfianza.getVentasAnuales()))?datfianza.getVentasAnuales():"";
				String sector = (datfianza.getSector()!=null && !"".equals(datfianza.getSector()))?datfianza.getSector():"";
				if(!"".equals(numEmpleados) && !"".equals(ventasAnuales) && !"".equals(sector) ){
					puntaje = ((( Double.parseDouble(numEmpleados) * 10)/100) + ((Double.parseDouble(ventasAnuales)/1000000 * 90)/100));

					if("1".equals(sector)){
						ic_estrato = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=95)?"2":(puntaje>95 && puntaje<=250)?"3":puntaje>250?"4":"";
					}
					if("2".equals(sector)){
						ic_estrato = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=93)?"2":(puntaje>93 && puntaje<=235)?"3":puntaje>235?"4":"";
					}
					if("3".equals(sector)){
						ic_estrato = (puntaje<=4.6)?"1":(puntaje>4.6 && puntaje<=93)?"2":(puntaje>93 && puntaje<=235)?"3":puntaje>235?"4":"";
					}
				}

				query = "INSERT INTO fe_fiado " +
						"            (ic_fiado, cg_rfc, cg_nombre, cg_calle, cg_noexterior, " +
						"             cg_nointerior, cg_colonia, cg_localidad, cg_referencia, " +
						"             cg_municipio, cg_estado, cg_pais, cg_cp, cg_rl_nombre, " +
						"             cg_rl_correo, cg_rl_telefono, fg_ventas_anuales, ig_numero_epleados, ic_sector, ic_estrato " +
						"            ) " +
						"     VALUES (?, ?, ?, ?, ?, " +
						"             ?, ?, ?, ?, " +
						"             ?, ?, ?, ?, ?, " +
						"             ?, ?, ?, ?, ?, ?) ";

				ps = con.queryPrecompilado(query);
				ps.setLong(++param, Long.parseLong(icFiado));
				ps.setString(++param, datfianza.getFiadoRfc());
				ps.setString(++param, datfianza.getFiadoNombre() );
				ps.setString(++param, datfianza.getFiadoCalle());
				ps.setString(++param, datfianza.getFiadoNoExterior());
				ps.setString(++param, datfianza.getFiadoNoInterior());
				ps.setString(++param, datfianza.getFiadoColonia());
				ps.setString(++param, datfianza.getFiadoLocalidad());
				ps.setString(++param, datfianza.getFiadoReferencia());
				ps.setString(++param, datfianza.getFiadoMunicipio());
				ps.setString(++param, datfianza.getFiadoEstado());
				ps.setString(++param, datfianza.getFiadoPais());
				ps.setString(++param, datfianza.getFiadoCodigoPostal());
				ps.setString(++param, datfianza.getNombreRepLegal());
				ps.setString(++param, datfianza.getCorreoRepLegal() );
				ps.setString(++param, datfianza.getTelefonoRepLegal());

				if(!"".equals(ventasAnuales)) ps.setDouble(++param, Double.parseDouble(ventasAnuales));
				else ps.setNull(++param, Types.DOUBLE);

				if(!"".equals(numEmpleados)) ps.setLong(++param, Long.parseLong(numEmpleados));
				else ps.setNull(++param, Types.INTEGER);

				if(!"".equals(sector)) ps.setLong(++param, Long.parseLong(sector));
				else ps.setNull(++param, Types.INTEGER);

				ps.setString(++param, ic_estrato);

				ps.executeUpdate();
				ps.close();

				//Genera e inseta Nafin Electronico para fiados()
				nafElecFiado = insertaNoNafinElectronico(con, icFiado, "F");
				log.info("Numero Electronico generado para el Fiado: "+nafElecFiado);

				//genera cuenta de FIADO
				//setSolicitudUsuario(icFiado,"F",datfianza, "CONS FIADO", false, true);

				Bitacora.grabarEnBitacora(con, "WSFREGAFIL","F", nafElecFiado, claveUsuario,"", "ALTA-FIADO");

			}

			log.info("afiliaFiadoWS(S)");
			return icFiado;
		}catch(Throwable t){
			throw new AppException("Error en el alta del Fiado",t);
		}
	}

	/**
	 * Asigna numero de nafin electronico para el nuevo Fiado que
	 * es afiliado en N@E via WS
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param tipo - tipo de afiliado
	 * @param clave - clave del fiado
	 * @param con - conexion a la bd
	 */
	private String insertaNoNafinElectronico(AccesoDB con,String clave, String tipo)
			throws AppException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		String query  = null;
		String nafinElec = "";

		try {
			query = "select nvl(max(ic_nafin_electronico),0)+1 from comrel_nafin";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				nafinElec = rs.getString(1)==null?"":rs.getString(1);
			}
			rs.close();
			ps.close();

			query  ="insert into comrel_nafin(IC_NAFIN_ELECTRONICO,IC_EPO_PYME_IF,CG_TIPO) "+
				" values( ?, ?, ?) ";
			log.debug("query inerta en comrel_nafin = " + query);

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(nafinElec));
			ps.setLong(2, Long.parseLong(clave));
			ps.setString(3, tipo);
			ps.executeUpdate();
			ps.close();

			return nafinElec;
		} catch (Throwable t) {
			throw new AppException("Error al generar Nafin Electronico ", t);
		}
	}


	/**
	 * Realiza la solicitude para la generacion de la cuentas de usuario
	 * para el nuevo fiado que se genero via WS
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param bPreAutorizacion
	 * @param bBloqueada
	 * @param sPerfilU
	 * @param datfianza
	 * @param sTipoAfiliadoU
	 * @param sCveAfiliadoU
	 */
	private String setSolicitudUsuario(String sCveAfiliadoU, String sTipoAfiliadoU, DatosFianzaWS datfianza,
					String sPerfilU, boolean bBloqueada, boolean bPreAutorizacion) throws AppException {

		log.info("setSolicitudUsuario(E)");
		String login = null;
		try {
			Usuario usuario = new Usuario();
			SolicitudArgus solicitud = new SolicitudArgus();
			UtilUsr utilUsr = new UtilUsr();

			usuario.setClaveAfiliado(sCveAfiliadoU);
			usuario.setTipoAfiliado(sTipoAfiliadoU);
			usuario.setEmail(datfianza.getCorreoRepLegal());
			usuario.setApellidoPaterno("No especificado");
			usuario.setApellidoMaterno("No especificado");
			usuario.setNombre(datfianza.getFiadoNombre().toUpperCase());
			usuario.setPerfil(sPerfilU);

			solicitud.setCompania(datfianza.getFiadoNombre().toUpperCase());
			solicitud.setRFC(datfianza.getFiadoRfc().toUpperCase());
			//solicitud.setDireccion( sDireccionS.toUpperCase());
			solicitud.setDireccion(datfianza.getFiadoCalle().toUpperCase());
			solicitud.setColonia(datfianza.getFiadoColonia().toUpperCase());
			solicitud.setMunicipio(datfianza.getFiadoMunicipio());
			solicitud.setEstado(datfianza.getFiadoEstado());
			solicitud.setPais(datfianza.getFiadoPais());
			solicitud.setCodigoPostal(datfianza.getFiadoCodigoPostal());
			solicitud.setTelefono(datfianza.getTelefonoRepLegal());

			solicitud.setBloqueo(bBloqueada);
			solicitud.setPreautorizacion(bPreAutorizacion);
			solicitud.setUsuario(usuario);


			login = utilUsr.registrarSolicitudAltaUsuario(solicitud);

		} catch(Throwable t) {
			log.error("setSolicitudUsuario(Error)");
			t.printStackTrace();
			throw new AppException("Problemas al generarse la Solicitud del Usuario, para el OID. ", t);
		}
		log.info("setSolicitudUsuario(S)");
		return login;
	}

	/**
	 * Obtiene informacion de usuarios en base a los criterios enviados
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param perfil - nombre del perfil o perfiles separados por coma que se desea obtener (si es vacio no filtra por perfil)
	 * @param tipoAfiliado - identificador del tipo de afiliado
	 * @param claveAfiliado - usuario del acual se obtendran la cuentas asociadas
	 */
	public List getAutorizadoresUsuariosxEpo( String claveAfiliado, String tipoAfiliado, String perfil)throws AppException{
		log.info("getAutorizadoresUsuariosxEpo(E)");
		Usuario usuario = new Usuario();
		UtilUsr utilUsr = new UtilUsr();
		List lstUsersXepo = new ArrayList();
		List lstUsersInfo = new ArrayList();
		List lstPerfiles = new ArrayList();

		try{
			lstUsersXepo = utilUsr.getUsuariosxAfiliado(claveAfiliado, tipoAfiliado);
			log.debug("lstUsersXepo.size()====="+lstUsersXepo.size());
			perfil = perfil==null?"":perfil;
			if(!"".equals(perfil))
						lstPerfiles = Comunes.explode(",",perfil,String.class);

			if(lstUsersXepo.size()>0){
				for(int x = 0; x<lstUsersXepo.size();x++){
					HashMap hpUsauarios = new HashMap();
					boolean esperfil = false;

					//SE OBTIENE USUARIO DE CADA LOGIN
					usuario = utilUsr.getUsuario((String)lstUsersXepo.get(x));
					if(!"".equals(perfil)){
						for(int i=0;i<lstPerfiles.size();i++){
								if( usuario.getPerfil().equals((String)lstPerfiles.get(i)) ){
									log.debug("usuario,perfil --- "+(String)lstUsersXepo.get(x)+" , "+(String)lstPerfiles.get(i));
									esperfil = true;
								}
						}
					}

					if("".equals(perfil) || esperfil){
						log.debug("perfil=="+perfil + "---usuario=="+(String)lstUsersXepo.get(x)+ "---esperfil=="+esperfil);
						//usuario = utilUsr.getUsuario((String)lstUsersXepo.get(x));
						log.debug("usuario obtenido = "+usuario.getPerfil());
						hpUsauarios.put("NOMBREUSUARIO", usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno() );
						hpUsauarios.put("CORREOELEC", usuario.getEmail());
						hpUsauarios.put("USUARIO", usuario.getLogin());
						hpUsauarios.put("PERFIL", usuario.getPerfil());
						lstUsersInfo.add(hpUsauarios);
					}
				}
			}

			return lstUsersInfo;

		}catch(Throwable t){
			log.info("getAutorizadoresUsuariosxEpo(Error)");
			throw new AppException("Error en la obtencion de usuarios Autorizadores x Epo ", t);
		}finally{
			log.info("getAutorizadoresUsuariosxEpo(S)");
		}
	}//FIN METODO

	/**
	 * Consulta la fianzas existentes en la tabla fe_fianza en base a criterios seleccionados
	 * @throws netropology.utilerias.AppException
	 * @return registros - Lista de los registros obtenidos en la consulta
	 * @param params - bean que contiene los parametros para la consulta
	 */
	public List getFianzas(ParamConsBean params) throws AppException {
		log.info("getFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer();
		List registros = new ArrayList();
		try{
			con.conexionDB();

			query.append("SELECT ff.ic_fianza cvefianza, fa.ic_afianzadora cveafianza, " +
				"       fe.cc_estatus estatus, fr.cc_ramo ramos, fs.cc_subramo subramo, " +
				"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza, " +
				"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal, " +
				"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo, " +
				"       ff.cg_tipo_fianza tipofianza, to_char(ff.df_autorizacion,'dd/mm/yyyy hh24:mi:ss') fecautoriza, " +
				"       to_char(ff.df_fin_vig_fianza,'dd/mm/yyyy hh24:mi:ss') fecvigencia, fd.cg_nombre nomfiado, " +
				"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef, " +
				"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza, " +
				"       ff.bi_pdf pdf, ff.cg_observaciones observaciones " +
				"  FROM fe_fianza ff, " +
				"       fe_afianzadora fa, " +
				"       fe_fiado fd, " +
				"       comcat_epo ce, " +
				"       fecat_estatus fe, " +
				"       fecat_ramo fr, " +
				"       fecat_subramo fs " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora " +
				"   AND ff.ic_fiado = fd.ic_fiado " +
				"   AND ff.ic_epo = ce.ic_epo " +
				"   AND ff.cc_estatus = fe.cc_estatus " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo " +
				"   AND fr.cc_ramo = fs.cc_ramo "
				);


			if(!"".equals(params.getIcEpo()))
				query.append(" AND ff.ic_epo = ? ");
			if(!"".equals(params.getUsuario()))
				query.append(" AND ff.cg_usuario = ? ");
			if(!"".equals(params.getIcAfianzadora()))
				query.append(" AND ff.ic_afianzadora = ? ");
			if(!"".equals(params.getIcFiado()))
				query.append(" AND fd.ic_fiado = ? ");
			if(!"".equals(params.getIcEstatus()))
				query.append(" AND fe.cc_estatus = ? ");
			if(!"".equals(params.getNumFianza()))
				query.append(" AND ff.cg_numero_fianza = ? ");
			if(!"".equals(params.getNumContrato()))
				query.append(" AND ff.cg_numero_contrato = ? ");
			if(!"".equals(params.getDfSolicIni()))
				query.append(" AND ff.df_solicitud >= to_date(?,'dd/mm/yyyy') ");
			if(!"".equals(params.getDfSolicFin()))
				query.append(" AND ff.df_solicitud < to_date(?,'dd/mm/yyyy')+1 ");
			if(!"".equals(params.getDfVencIni()))
				query.append(" AND ff.df_vencimiento >= to_date(?,'dd/mm/yyyy') ");
			if(!"".equals(params.getDfVencFin()))
				query.append(" AND ff.df_vencimiento < to_date(?,'dd/mm/yyyy')+1 ");
			if(!"".equals(params.getDfPublicIni()))
				query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			if(!"".equals(params.getDfPublicFin()))
				query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			if(!"".equals(params.getAutorizador()))
				query.append(" AND (ff.cg_autorizador = ? OR ff.cg_autorizador = 'T')");  //F028-2012

			int p = 0;
			ps = con.queryPrecompilado(query.toString());
			if(!"".equals(params.getIcEpo()))
				ps.setLong(++p,Long.parseLong(params.getIcEpo()));
			if(!"".equals(params.getUsuario()))
				ps.setString(++p,params.getUsuario());
			if(!"".equals(params.getIcAfianzadora()))
				ps.setLong(++p,Long.parseLong(params.getIcAfianzadora()));
			if(!"".equals(params.getIcFiado()))
				ps.setLong(++p,Long.parseLong(params.getIcFiado()));
			if(!"".equals(params.getIcEstatus()))
				ps.setString(++p,params.getIcEstatus());
			if(!"".equals(params.getNumFianza()))
				ps.setString(++p,params.getNumFianza());
			if(!"".equals(params.getNumContrato()))
				ps.setString(++p,params.getNumContrato());
			if(!"".equals(params.getDfSolicIni()))
				ps.setString(++p,params.getDfSolicIni());
			if(!"".equals(params.getDfSolicFin()))
				ps.setString(++p,params.getDfSolicFin());
			if(!"".equals(params.getDfVencIni()))
				ps.setString(++p,params.getDfVencIni());
			if(!"".equals(params.getDfVencFin()))
				ps.setString(++p,params.getDfVencFin());
			if(!"".equals(params.getDfPublicIni()))
				ps.setString(++p,params.getDfPublicIni());
			if(!"".equals(params.getDfPublicFin()))
				ps.setString(++p,params.getDfPublicFin());
			//if(!"".equals(params.getAutorizador()))
			if(!"".equals(params.getAutorizador()) && !"T".equals(params.getAutorizador()) ) //F028-2012
				ps.setString(++p,params.getAutorizador());


			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				mp.put("CVEFIANZA", rs.getString("cvefianza")==null?"":rs.getString("cvefianza") );
				mp.put("CVEAFIANZA", rs.getString("cveafianza")==null?"":rs.getString("cveafianza"));
				mp.put("ESTATUS", rs.getString("estatus")==null?"":rs.getString("estatus") );
				mp.put("RAMOS", rs.getString("ramos")==null?"":rs.getString("ramos") );
				mp.put("SUBRAMO", rs.getString("subramo")==null?"":rs.getString("subramo"));
				mp.put("NOMAFINZA", rs.getString("nomafinza")==null?"":rs.getString("nomafinza") );
				mp.put("NUMFIANZA", rs.getString("numfianza")==null?"":rs.getString("numfianza"));
				mp.put("NUMCONTRATO", rs.getString("numcontrato")==null?"":rs.getString("numcontrato") );
				mp.put("MONTOTOTAL", rs.getString("montototal")==null?"":rs.getString("montototal"));
				mp.put("NOMRAMO", rs.getString("nomramo")==null?"":rs.getString("nomramo") );
				mp.put("NOMSUBRAMO", rs.getString("nomsubramo")==null?"":rs.getString("nomsubramo"));
				mp.put("TIPOFIANZA", rs.getString("tipofianza")==null?"":rs.getString("tipofianza") );
				mp.put("FECAUTORIZA", rs.getString("fecautoriza")==null?"":rs.getString("fecautoriza"));
				mp.put("FECVIGENCIA", rs.getString("fecvigencia")==null?"":rs.getString("fecvigencia") );
				mp.put("NOMFIADO", rs.getString("nomfiado")==null?"":rs.getString("nomfiado"));
				mp.put("NOMDEST", rs.getString("nomdest")==null?"":rs.getString("nomdest") );
				mp.put("NOMBENEF", rs.getString("nombenef")==null?"":rs.getString("nombenef"));
				mp.put("NOMESTATUS", rs.getString("nomestatus")==null?"":rs.getString("nomestatus") );
				mp.put("URLAFIANZA", rs.getString("urlafianza")==null?"":rs.getString("urlafianza"));
				//mp.put("PDF", rs.getString("pdf")==null?"":rs.getString("pdf") );
				mp.put("OBSERVACIONES", rs.getString("observaciones")==null?"":rs.getString("observaciones"));
				registros.add(mp);
			}
			rs.close();
			ps.close();

			return registros;
		}catch(Throwable t){
			throw new AppException("Error al consultar las fianzas en el Concentrador de Fianzas ", t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFianzas(S)");
		}
	}

	/**
	 * Extrae archivo pdf de la bd de una Fianza en especifico
	 * @throws netropology.utilerias.AppException
	 * @return String - nombre del archivo pdf
	 * @param rutaDestino - ruta donde se generara el archivo
	 * @param cveFianza - clave de la fianza
	 */
	public String getPDFfianzas(String cveFianza, String rutaDestino)throws AppException{
		log.info("getPDFfianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String nombreArchivo = "";
		try{
			con.conexionDB();
			query = " select bi_pdf from fe_fianza where ic_fianza = ? ";
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFianza));
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_pdf");
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, rutaDestino, ".pdf")) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
				nombreArchivo = creaArchivo.getNombre();
				inStream.close();
			}
			rs.close();
			ps.close();
			return nombreArchivo;

		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al obetener arreglo de bytes del PDF de la fianza");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Extrae archivo pdf de la bd de una Fianza en especifico PDF ERROES
    * FODEA 028-2012: garellano
    * @date 18/12/2012
	 * @throws netropology.utilerias.AppException
	 * @return String - nombre del archivo pdf
	 * @param rutaDestino - ruta donde se generara el archivo
	 * @param cveFianza - clave de la fianza
	 */
	public String getPDFfianzasErrores(String cveFianza, String rutaDestino)throws AppException{
		log.info("getPDFfianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String nombreArchivo = "";
		try{
			con.conexionDB();
			query = " select bi_pdf_correcciones from fe_fianza where ic_fianza = ? and cc_estatus='003'"; //estatus rechazada
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFianza));
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_pdf_correcciones");
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, rutaDestino, ".pdf")) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
				nombreArchivo = creaArchivo.getNombre();
				inStream.close();
			}
			rs.close();
			ps.close();
			return nombreArchivo;

		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al obetener arreglo de bytes del PDF de la fianza");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene las fianzas que seran envidas a la bandeja de cada usuario.
	 * @throws netropology.utilerias.AppException
	 * @return List regresa arreglo de fianzas en Objetos HashMap
	 * @param lstClaves
	 */
	public List getPreAcuseConcFianzas(List lstClaves)throws AppException{
		log.info("getPreAcuseConcFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List registros = new ArrayList();
		StringBuffer query = new StringBuffer();
		try{
			con.conexionDB();

			query.append("SELECT ff.ic_fianza cvefianza, fa.ic_afianzadora cveafianza, " +
				"       fe.cc_estatus estatus, fr.cc_ramo ramos, fs.cc_subramo subramo, " +
				"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza, " +
				"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal, " +
				"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo, " +
				"       ff.cg_tipo_fianza tipofianza, to_char(ff.df_autorizacion,'dd/mm/yyyy hh24:mi:ss') fecautoriza, " +
				"       to_char(ff.df_fin_vig_fianza,'dd/mm/yyyy hh24:mi:ss') fecvigencia, fd.cg_nombre nomfiado, " +
				"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef, " +
				"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza, " +
				"       ff.bi_pdf pdf, ff.cg_observaciones observaciones " +
				"  FROM fe_fianza ff, " +
				"       fe_afianzadora fa, " +
				"       fe_fiado fd, " +
				"       comcat_epo ce, " +
				"       fecat_estatus fe, " +
				"       fecat_ramo fr, " +
				"       fecat_subramo fs " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora " +
				"   AND ff.ic_fiado = fd.ic_fiado " +
				"   AND ff.ic_epo = ce.ic_epo " +
				"   AND ff.cc_estatus = fe.cc_estatus " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo " +
				"   AND fr.cc_ramo = fs.cc_ramo "
				);
			for(int x = 0; x<lstClaves.size();x++){
				if(x==0)query.append(" AND ff.ic_fianza in ( ? ");
				else query.append(",? ");
				if(x==(lstClaves.size()-1)) query.append(" ) ");
			}

			int p = 0;
			ps = con.queryPrecompilado(query.toString());
			for(int x = 0; x<lstClaves.size();x++){
				String cveFianza = (String)lstClaves.get(x);
				ps.setLong(++p,Long.parseLong(cveFianza));
			}
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				mp.put("CVEFIANZA", rs.getString("cvefianza")==null?"":rs.getString("cvefianza") );
				mp.put("CVEAFIANZA", rs.getString("cveafianza")==null?"":rs.getString("cveafianza"));
				mp.put("ESTATUS", rs.getString("estatus")==null?"":rs.getString("estatus") );
				mp.put("RAMOS", rs.getString("ramos")==null?"":rs.getString("ramos") );
				mp.put("SUBRAMO", rs.getString("subramo")==null?"":rs.getString("subramo"));
				mp.put("NOMAFINZA", rs.getString("nomafinza")==null?"":rs.getString("nomafinza") );
				mp.put("NUMFIANZA", rs.getString("numfianza")==null?"":rs.getString("numfianza"));
				mp.put("NUMCONTRATO", rs.getString("numcontrato")==null?"":rs.getString("numcontrato") );
				mp.put("MONTOTOTAL", rs.getString("montototal")==null?"":rs.getString("montototal"));
				mp.put("NOMRAMO", rs.getString("nomramo")==null?"":rs.getString("nomramo") );
				mp.put("NOMSUBRAMO", rs.getString("nomsubramo")==null?"":rs.getString("nomsubramo"));
				mp.put("TIPOFIANZA", rs.getString("tipofianza")==null?"":rs.getString("tipofianza") );
				mp.put("FECAUTORIZA", rs.getString("fecautoriza")==null?"":rs.getString("fecautoriza"));
				mp.put("FECVIGENCIA", rs.getString("fecvigencia")==null?"":rs.getString("fecvigencia") );
				mp.put("NOMFIADO", rs.getString("nomfiado")==null?"":rs.getString("nomfiado"));
				mp.put("NOMDEST", rs.getString("nomdest")==null?"":rs.getString("nomdest") );
				mp.put("NOMBENEF", rs.getString("nombenef")==null?"":rs.getString("nombenef"));
				mp.put("NOMESTATUS", rs.getString("nomestatus")==null?"":rs.getString("nomestatus") );
				mp.put("URLAFIANZA", rs.getString("urlafianza")==null?"":rs.getString("urlafianza"));
				mp.put("OBSERVACIONES", rs.getString("observaciones")==null?"":rs.getString("observaciones"));
				registros.add(mp);
			}
			rs.close();
			ps.close();

			return registros;
		}catch(Throwable t){
			log.error("getPreAcuseConcFianzas(ERROR)");
			throw new AppException("Error en la consulta par agenerar preacuse de envio de fianzas ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getPreAcuseConcFianzas(S)");
		}
	}

	/**
	 * Cambia de estatus(Por Revisar) a las fianzas seleccionadas, asignandoles
	 * el usuario que hizo el envio.
	 * @throws netropology.utilerias.AppException
	 * @param acuse - objetio que contiene la informacion del acuse generado
	 * @param usuario - numero de usuario que hizo el envio
	 * @param lstClaves - listado de claves de las fianzas
	 */
	public void setEnvioFianzasBandeja(List lstClaves, String usuario, AcuseFianza acuse)throws AppException{
		log.info("setEnvioFianzasBandeja(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean commit = true;
		String qry = "";
		try{
			con.conexionDB();
			BitacoraFianza bitacora = new BitacoraFianza();

			for(int x=0; x<lstClaves.size();x++){
				HashMap map = new HashMap();
				map = (HashMap)lstClaves.get(x);

				//Se obtine maps para generar registro en bitacora
				HashMap datAnteriores = new HashMap();
				HashMap datActuales = new HashMap();
				datAnteriores = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));

				qry = "UPDATE fe_fianza " +
				"   SET cc_estatus = ?, " +
				"       cc_acuse = ?, " +
				"	cg_usuario = ? " +
				" WHERE ic_fianza = ? AND cc_estatus = ? ";

				ps = con.queryPrecompilado(qry);
				int p = 0;
				ps.setString(++p,(String)map.get("ESTATUS"));
				ps.setString(++p,acuse.getAcuse());
				ps.setString(++p,usuario);
				ps.setLong(++p,Long.parseLong((String)map.get("CVEFIANZA")));
				ps.setString(++p,"001");
				ps.executeUpdate();
				ps.close();

				//SE GENRA REGISTRO EN BITACORA
				datActuales = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));
				BitacoraFianza.agregarRegBitacora(con, usuario, (String)map.get("CVEFIANZA"),datAnteriores,datActuales);
			}
			acuse.setClaveUsuario(usuario);
			guardaAcuseFianza(con, acuse, lstClaves, "N");

		}catch(Throwable t){
			log.error("setEnvioFianzasBandeja(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al enviar la fianza a la bandeja");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setEnvioFianzasBandeja(S)");
		}
	}

	/**
	 * Obtiene las fianzas que seran envidas a la bandeja de cada usuario.
	 * @throws netropology.utilerias.AppException
	 * @return List regresa arreglo de fianzas en Objetos HashMap
	 * @param lstClaves
	 */
	public List getAcuseConcFianzas(String acuse)throws AppException{
		log.info("getAcuseConcFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List registros = new ArrayList();
		StringBuffer query = new StringBuffer();
		try{
			con.conexionDB();

			query.append("SELECT ff.ic_fianza cvefianza, fa.ic_afianzadora cveafianza, " +
				"       fe.cc_estatus estatus, fr.cc_ramo ramos, fs.cc_subramo subramo, " +
				"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza, " +
				"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal, " +
				"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo, " +
				"       ff.cg_tipo_fianza tipofianza, to_char(ff.df_autorizacion,'dd/mm/yyyy hh24:mi:ss') fecautoriza, " +
				"       to_char(ff.df_fin_vig_fianza,'dd/mm/yyyy hh24:mi:ss') fecvigencia, fd.cg_nombre nomfiado, " +
				"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef, " +
				"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza, " +
				"       ff.bi_pdf pdf, ff.cg_observaciones observaciones, ff.cg_autorizador autorizador " +
				"  FROM fe_fianza ff, " +
				"       fe_afianzadora fa, " +
				"       fe_fiado fd, " +
				"       comcat_epo ce, " +
				"       fecat_estatus fe, " +
				"       fecat_ramo fr, " +
				"       fecat_subramo fs " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora " +
				"   AND ff.ic_fiado = fd.ic_fiado " +
				"   AND ff.ic_epo = ce.ic_epo " +
				"   AND ff.cc_estatus = fe.cc_estatus " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo " +
				"   AND fr.cc_ramo = fs.cc_ramo "+
				"   AND ff.cc_acuse = ? ");


			log.debug("getAcuseConcFianzas::query: "+query.toString());
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,acuse);
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				mp.put("CVEFIANZA", rs.getString("cvefianza")==null?"":rs.getString("cvefianza") );
				mp.put("CVEAFIANZA", rs.getString("cveafianza")==null?"":rs.getString("cveafianza"));
				mp.put("ESTATUS", rs.getString("estatus")==null?"":rs.getString("estatus") );
				mp.put("RAMOS", rs.getString("ramos")==null?"":rs.getString("ramos") );
				mp.put("SUBRAMO", rs.getString("subramo")==null?"":rs.getString("subramo"));
				mp.put("NOMAFINZA", rs.getString("nomafinza")==null?"":rs.getString("nomafinza") );
				mp.put("NUMFIANZA", rs.getString("numfianza")==null?"":rs.getString("numfianza"));
				mp.put("NUMCONTRATO", rs.getString("numcontrato")==null?"":rs.getString("numcontrato") );
				mp.put("MONTOTOTAL", rs.getString("montototal")==null?"":rs.getString("montototal"));
				mp.put("NOMRAMO", rs.getString("nomramo")==null?"":rs.getString("nomramo") );
				mp.put("NOMSUBRAMO", rs.getString("nomsubramo")==null?"":rs.getString("nomsubramo"));
				mp.put("TIPOFIANZA", rs.getString("tipofianza")==null?"":rs.getString("tipofianza") );
				mp.put("FECAUTORIZA", rs.getString("fecautoriza")==null?"":rs.getString("fecautoriza"));
				mp.put("FECVIGENCIA", rs.getString("fecvigencia")==null?"":rs.getString("fecvigencia") );
				mp.put("NOMFIADO", rs.getString("nomfiado")==null?"":rs.getString("nomfiado"));
				mp.put("NOMDEST", rs.getString("nomdest")==null?"":rs.getString("nomdest") );
				mp.put("NOMBENEF", rs.getString("nombenef")==null?"":rs.getString("nombenef"));
				mp.put("NOMESTATUS", rs.getString("nomestatus")==null?"":rs.getString("nomestatus") );
				mp.put("URLAFIANZA", rs.getString("urlafianza")==null?"":rs.getString("urlafianza"));
				mp.put("NEWOBSERVACIONES", rs.getString("observaciones")==null?"":rs.getString("observaciones"));
				mp.put("AUTORIZADOR", rs.getString("autorizador")==null?"":rs.getString("autorizador"));
				registros.add(mp);
			}
			rs.close();
			ps.close();

			return registros;
		}catch(Throwable t){
			log.error("getAcuseConcFianzas(ERROR)");
			throw new AppException("Error en la consulta par agenerar preacuse de envio de fianzas ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getAcuseConcFianzas(S)");
		}
	}

	/**
	 * Guarda el acuse de las distintas etapas del proceso de la fianza
	 * @throws netropology.utilerias.AppException
	 * @param cs_carga_ws
	 * @param lstFianzas
	 * @param acuse
	 * @param con
	 */
	private void guardaAcuseFianza(AccesoDB con, AcuseFianza acuse, List lstFianzas, String cs_carga_ws)throws AppException{
		log.info("guardaAcuseFianza(E)");
		PreparedStatement ps = null;
		String query  = "";
		try{
			cs_carga_ws = (cs_carga_ws==null || "".equals(cs_carga_ws))?"":cs_carga_ws;
			query = " INSERT INTO fe_acuse " +
				"            (cc_acuse, cg_usuario, ig_total_fianzas, cg_recibo_electronico, ic_producto_nafin, "+
				"				  df_fecha, cg_hora, cs_carga_ws, ig_total_autorizadas, ig_total_rechazadas, ig_total_retornadas ) " +
				"     VALUES (?, ?, ?, ?, ?, to_date(?,'dd/mm/yyyy'), ?, ?, ?, ?, ?) ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,acuse.getAcuse());
			ps.setString(2,acuse.getClaveUsuario());
			ps.setInt(3,lstFianzas.size());
			ps.setString(4,acuse.getReciboElectronico());
			ps.setString(5,acuse.getIcProductoNafin());
			ps.setString(6,acuse.getFecha());
			ps.setString(7,acuse.getHora());
			ps.setString(8,cs_carga_ws);
			if(!"".equals(acuse.getTotalAutorizadas()))
				ps.setLong(9,Long.parseLong(acuse.getTotalAutorizadas()));
			else
				ps.setNull(9, Types.INTEGER);
			if(!"".equals(acuse.getTotalRechazadas()))
				ps.setLong(10,Long.parseLong(acuse.getTotalRechazadas()));
			else
				ps.setNull(10, Types.INTEGER);
			if(!"".equals(acuse.getTotalRetornadas()))
				ps.setLong(11,Long.parseLong(acuse.getTotalRetornadas()));
			else
				ps.setNull(11, Types.INTEGER);
			ps.executeUpdate();

			for(int x=0; x<lstFianzas.size();x++){
				HashMap map = (HashMap)lstFianzas.get(x);
				String cveFianza = (String)map.get("CVEFIANZA");
				String estatus = (String)map.get("ESTATUS");
				query = "INSERT INTO ferel_acuse_fianza " +
					"            (cc_acuse, ic_fianza, cc_estatus) " +
					"     VALUES (?, ?, ?) ";
				ps = con.queryPrecompilado(query);
				ps.setString(1,acuse.getAcuse());
				ps.setLong(2,Long.parseLong(cveFianza));
				ps.setString(3, estatus);
				ps.executeUpdate();
			}

			log.info("guardaAcuseFianza(S)");
		}catch(Throwable t){
			log.error("guardaAcuseFianza(Error al guardar acuse de la fianza procesada)");
			throw new AppException("Error al guardar acuse de la fianza procesada", t);
		}
	}

	/**
	 * Cambia de estatus dependiendo de la accion asignada en la revision de fianzas
	 * @throws netropology.utilerias.AppException
	 * @param acuse - objeto que contiene la informacion del acuse generado
	 * @param usuario - numero de usuario que hizo el envio
	 * @param lstClaves - listado de claves de las fianzas
	 */
	public void setRevisionFianzas(List lstClaves, String firmaMan, String usuario, AcuseFianza acuse)throws AppException{
		log.info("setRevisionFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qry = "";
		boolean commit = true;

		try{
			con.conexionDB();
			BitacoraFianza bitacora = new BitacoraFianza();

			for(int x=0; x<lstClaves.size();x++){
				HashMap map = new HashMap();
				map = (HashMap)lstClaves.get(x);

				//Se obtine maps para generar registro en bitacora
				HashMap datAnteriores = new HashMap();
				HashMap datActuales = new HashMap();
				datAnteriores = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));
				String estatus = (String)map.get("ESTATUS");
				qry = "UPDATE fe_fianza " +
				"   SET cc_estatus = ?, " +
				"       cc_acuse = ?, " +
				"	cg_observaciones = ?, " +
				"	cg_autorizador = ? " +
				(("003".equals(estatus))?" ,df_rechazada = sysdate ":"")+
				(("005".equals(estatus))?" ,df_autorizada = sysdate ":"")+
				" WHERE ic_fianza = ? AND cc_estatus = ? ";

				ps = con.queryPrecompilado(qry);
				int p = 0;


				ps.setString(++p,estatus);
				ps.setString(++p,acuse.getAcuse());
				ps.setString(++p,(String)map.get("OBSERVACIONES"));
				ps.setString(++p,(String)map.get("AUTORIZADOR"));
				ps.setLong(++p,Long.parseLong((String)map.get("CVEFIANZA")));
				ps.setString(++p,"002");
				ps.executeUpdate();
				ps.close();


				//SE GENERA REGISTRO EN BITACORA
				datActuales = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));
				BitacoraFianza.agregarRegBitacora(con, usuario, (String)map.get("CVEFIANZA"),datAnteriores,datActuales);
			}
			acuse.setClaveUsuario(usuario);
			guardaAcuseFianza(con, acuse, lstClaves, "N");

		}catch(Throwable t){
			log.error("setRevisionFianzas(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error en la revisi�n de fianzas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setRevisionFianzas(S)");
		}
	}

	/**
	 * Se encarga de autorizar, rechazar o retornar las fianzas
	 * segun el autorizador o administrador lo desee
	 * @throws netropology.utilerias.AppException
	 * @param acuse - objeto que contiene la informacion del acuse generado
	 * @param usuario - numero de usuario que hizo el envio
	 * @param lstClaves - listado de claves de las fianzas
	 */
	public void setAutorizacionFianzas(List lstClaves, String firmaMan, String usuario, AcuseFianza acuse)throws AppException{
		log.info("setAutorizacionFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qry = "";
		boolean commit = true;

		try{
			con.conexionDB();
			BitacoraFianza bitacora = new BitacoraFianza();

			for(int x=0; x<lstClaves.size();x++){
				HashMap map = new HashMap();
				map = (HashMap)lstClaves.get(x);

				//Se obtine maps para generar registro en bitacora
				HashMap datAnteriores = new HashMap();
				HashMap datActuales = new HashMap();
				datAnteriores = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));
				String estatus = (String)map.get("ESTATUS");

				qry = "UPDATE fe_fianza " +
				"   SET cc_estatus = ?, " +
				"       cc_acuse = ?, " +
				"	cg_observaciones = ? " +
				(("003".equals(estatus))?" ,df_rechazada = sysdate ":"")+
				(("005".equals(estatus))?" ,df_autorizada = sysdate ":"")+
				//"	cg_autorizador = ? " +
				" WHERE ic_fianza = ? AND cc_estatus = ? ";

				ps = con.queryPrecompilado(qry);
				int p = 0;

				ps.setString(++p,estatus);
				ps.setString(++p,acuse.getAcuse());
				ps.setString(++p,(String)map.get("OBSERVACIONES"));
				ps.setLong(++p,Long.parseLong((String)map.get("CVEFIANZA")));
				ps.setString(++p,"004");
				ps.executeUpdate();
				ps.close();

				//SE GENERA REGISTRO EN BITACORA
				datActuales = bitacora.getInfoFianza(con, (String)map.get("CVEFIANZA"));
				BitacoraFianza.agregarRegBitacora(con, usuario, (String)map.get("CVEFIANZA"),datAnteriores,datActuales);
			}
			acuse.setClaveUsuario(usuario);
			guardaAcuseFianza(con, acuse, lstClaves, "N");

		}catch(Throwable t){
			log.error("setAutorizacionFianzas(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error en la revisi�n de fianzas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setAutorizacionFianzas(S)");
		}
	}

	public boolean validaFirmaMancomunada(String cveEpo) throws AppException{
		log.info("validaFirmaMancomunada(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String valor = "";
		boolean existe = false;
		try{
			con.conexionDB();
			query = "SELECT cp.cg_valor " +
				"  FROM com_parametrizacion_epo cp " +
				"  WHERE cp.ic_epo = ?  " +
				"  AND cp.cc_parametro_epo = ? ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(cveEpo));
			ps.setString(2, "FIRMA_MANCOMUNADA");
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				valor = rs.getString("cg_valor");
				existe = ("S".equals(valor))?true:false;
			}

			rs.close();
			ps.close();

			return existe;

		}catch(Throwable t){
			log.error("validaFirmaMancomunada(Error)");
			t.printStackTrace();
			throw new AppException("Error en la validacion de la firma mancomunada para la EPO", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("validaFirmaMancomunada(S)");
		}
	}


public void setEnvioCorreoFianzas(String acuse, String numusuario, String etapa,String numcliente) throws AppException{
		log.info("setEnvioCorreoFianzas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String asunto = "";//mggarciar
		String estimadoUsr = "";//mggarciar
		try{
			con.conexionDB();
			String correoBenef = "";
			String correoFiado = "";
			String correoAfianzadora = "";
			Usuario usuario = new Usuario();
			UtilUsr utilUsr = new UtilUsr();
			boolean siEnvio = false;
			String estatus = "";
			query = "SELECT ff.ic_fianza cvefianza,   " +
					"       fe.cc_estatus estatus,   " +
					"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza,  " +
					"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal,  " +
					"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo,  " +
					"       ff.cg_tipo_fianza tipofianza,   " +
					"       fd.cg_nombre nomfiado,  " +
					"		  ff.cg_rl_telefono telefonofiado,	"+
					"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef,  " +
					"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza,  " +
					"       ff.cg_observaciones observaciones, ff.cg_benf_correo_usr correobenef, " +
					"		  ff.cg_rl_correo correofiado, ff.cg_benf_nombre_usr nombenefusr, " +
					"		  ff.cg_autorizador autorizador, " +
					"		  dom.cg_email correo_afianzadora " +
					"  FROM fe_fianza ff,  " +
					"       fe_afianzadora fa,  " +
					"       fe_fiado fd,  " +
					"       comcat_epo ce,  " +
					"       fecat_estatus fe,  " +
					"       fecat_ramo fr,  " +
					"       fecat_subramo fs,  " +
					"		  com_domicilio dom	" +
					" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
					"	 AND fa.ic_afianzadora = dom.ic_afianzadora " +
					"   AND dom.cs_fiscal='S' " +
					"   AND ff.ic_fiado = fd.ic_fiado  " +
					"   AND ff.ic_epo = ce.ic_epo  " +
					"   AND ff.cc_estatus = fe.cc_estatus  " +
					"   AND ff.cc_benf_idramo = fr.cc_ramo  " +
					"   AND ff.cc_benf_idsubramo = fs.cc_subramo  " +
					"   AND fr.cc_ramo = fs.cc_ramo " +
					"   AND ff.cc_acuse = ?  ";


			ps = con.queryPrecompilado(query);
			ps.setString(1,acuse);
			rs  = ps.executeQuery();

			while(rs!=null && rs.next()){
				StringBuffer mensaje = new StringBuffer();
				String correos = "";
				String copiaCo = "";
				correoBenef = rs.getString("correobenef");
				correoFiado = rs.getString("correoFiado");
				correoAfianzadora = rs.getString("correo_afianzadora");
				estatus = rs.getString("estatus");
				asunto = "FIANZA "+rs.getString("nomestatus")+"/"+rs.getString("nomdest")+"/"+rs.getString("nomfiado");
				siEnvio = false;

				if("001".equals(estatus) && "webservice".equals(etapa)){
					if(correoBenef!=null && !"".equals(correoBenef))
							correos += (correos.length()>1)?(","+correoFiado):correoBenef;
					if(correoFiado!=null && !"".equals(correoFiado))
							copiaCo += (copiaCo.length()>1)?(","+correoFiado):correoFiado;

					estimadoUsr = rs.getString("nombenefusr");
					siEnvio = true;
				}else if("004".equals(estatus) && "revision".equals(etapa)){

					String autorizador = rs.getString("autorizador");
					if (autorizador.equals("T"))
						usuario = utilUsr.getUsuario(numusuario);

					else
						usuario = utilUsr.getUsuario(autorizador);

					estimadoUsr = usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno();

					if(usuario.getEmail()!=null && !"".equals(usuario.getEmail()))
						correos += (correos.length()>1)?(","+usuario.getEmail()):usuario.getEmail();

					List lstUsuarioDat = new ArrayList();
					String correo_admin = "";
					lstUsuarioDat = getAutorizadoresUsuariosxEpo(numcliente,"E","ADMIN AUTO EPO");
					//lstUsuarioDat = getAutorizadoresUsuariosxEpo(numusuario,"E","ADMIN AUTO EPO");
					if(lstUsuarioDat!=null && lstUsuarioDat.size()>0){
						for(int x=0; x<lstUsuarioDat.size();x++){
							HashMap map = new HashMap();
							map = (HashMap)lstUsuarioDat.get(x);
							if(map.get("CORREOELEC")!=null && !"".equals((String)map.get("CORREOELEC"))) {
								//copiaCo += (copiaCo.length()>1)?(","+(String)map.get("CORREOELEC")):(String)map.get("CORREOELEC");
								correo_admin = (copiaCo.length()>1)?(","+(String)map.get("CORREOELEC")):(String)map.get("CORREOELEC");
								if (!(correo_admin.equals(usuario.getEmail())))
									copiaCo += correo_admin;
							}
						}
					}
					siEnvio = true;
				}
				else if("003".equals(estatus) || "005".equals(estatus)){
					if( "revision".equals(etapa)){
						estimadoUsr = rs.getString("nomfiado");

						if(correoBenef!=null && !"".equals(correoBenef))
							correos += (correos.length()>1)?(","+correoFiado):correoBenef;
						if(correoFiado!=null && !"".equals(correoFiado))
								copiaCo += (copiaCo.length()>1)?(","+correoFiado):correoFiado;
					}
					if( "autorizacion".equals(etapa)){
						estimadoUsr = rs.getString("nomfiado");
						if(correoBenef!=null && !"".equals(correoBenef))
							copiaCo += (copiaCo.length()>1)?(","+correoFiado):correoBenef;
						if(correoFiado!=null && !"".equals(correoFiado))
								correos += (correos.length()>1)?(","+correoFiado):correoFiado;

						//F028-2012
						//***
						if(correoAfianzadora!=null && !"".equals(correoAfianzadora))
							copiaCo += (copiaCo.length()>1)?(","+correoAfianzadora):correoAfianzadora;

						String autorizador = rs.getString("autorizador");
						if (autorizador.equals("T"))
							usuario = utilUsr.getUsuario(numusuario);

						else
							usuario = utilUsr.getUsuario(autorizador);

						if(usuario.getEmail()!=null && !"".equals(usuario.getEmail()))
							copiaCo += (copiaCo.length()>1)?(","+usuario.getEmail()):usuario.getEmail();

						List lstUsuarioDat = new ArrayList();
						//lstUsuarioDat = getAutorizadoresUsuariosxEpo(numusuario,"E","ADMIN AUTO EPO");
						lstUsuarioDat = getAutorizadoresUsuariosxEpo(numcliente,"E","ADMIN AUTO EPO");
						if(lstUsuarioDat!=null && lstUsuarioDat.size()>0){
							for(int x=0; x<lstUsuarioDat.size();x++){
								HashMap map = new HashMap();
								map = (HashMap)lstUsuarioDat.get(x);
								if(map.get("CORREOELEC")!=null && !"".equals((String)map.get("CORREOELEC")))
									copiaCo += (copiaCo.length()>1)?(","+(String)map.get("CORREOELEC")):(String)map.get("CORREOELEC");
							}
						}
log.info("/*---------------------------------------------------------------------*/" );
log.info("estimadoUsr---------------------->" + estimadoUsr);
log.info("correoBenef---------------------->" + correoBenef);
log.info("correoFiado---------------------->" + correoFiado);
log.info("correoAfianzadora---------------------->" + correoAfianzadora);
log.info("autorizador---------------------->" + autorizador);
log.info("usuario.getEmail()---------------------->" + usuario.getEmail());
log.info("*************************************************" );
log.info("copiaCo----------------------->" + copiaCo);
log.info("*************************************************" );
log.info("/*---------------------------------------------------------------------*/" );
					}
					siEnvio = true;
				}//fin if("003".equals(estatus) || "005".equals(estatus))

				if(siEnvio){
					mensaje.append("<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>");
					mensaje.append("<p><b>Estimado usuario " + estimadoUsr +"</b></p>");
					if("001".equals(estatus) && "webservice".equals(etapa)){
						mensaje.append("<p>Por medio del presente se le informa que ha recibido fianzas en su buz&oacute;n concentrador, de la siguiente empresa:</p>");
					}else if("004".equals(estatus) && "revision".equals(etapa)){
						mensaje.append("<p>Por medio del presente se le informa que ha recibido fianzas para su autorizaci&oacute;n.</p>");
					}else if("005".equals(estatus)){
						mensaje.append("<p>Por medio del presente se le informa que se Autoriz&oacute; la siguiente Fianza:</p>");
					}else if("003".equals(estatus)){
						mensaje.append("<p>Por medio del presente se le informa que se Rechaz&oacute; la siguiente Fianza, usted puede consultar las causas de rechazo detalladas en el PDF cargado en el sistema mostrado en el campo \"PDF Errores\" y/o en el campo \"Observaciones\".</p>");
					}
					mensaje.append("<ul>");
					mensaje.append("<li><b>Afianzadora:</b> " 				+obtenerValor(rs,"nomafinza", 		"") + "</li>");
					mensaje.append("<li><b>No. Fianza:</b> " 					+obtenerValor(rs,"numfianza", 		"") + "</li>");
					mensaje.append("<li><b>No. Contrato:</b> " 				+obtenerValor(rs,"numcontrato", 		"") + "</li>");
					mensaje.append("<li><b>Monto:</b> $"						+(rs.getString("montototal") == null?"":Comunes.formatoDecimal(rs.getString("montototal"),2,true))+ "</li>");
					mensaje.append("<li><b>Ramo:</b> " 							+obtenerValor(rs,"nomramo", 			"") + "</li>");
					mensaje.append("<li><b>Sub Ramo:</b> " 					+obtenerValor(rs,"nomsubramo", 		"") + "</li>");
					mensaje.append("<li><b>Tipo Fianza:</b> " 				+obtenerValor(rs,"tipofianza", 		"") + "</li>");
					mensaje.append("<li><b>Fiado:</b> " 						+obtenerValor(rs,"nomfiado", 			"") + "</li>");
					mensaje.append("<li><b>Tel&eacute;fono Fiado:</b> " 	+obtenerValor(rs,"telefonofiado", 	"") + "</li>");
					mensaje.append("<li><b>Destinatario:</b> " 				+obtenerValor(rs,"nomdest", 			"") + "</li>");
					mensaje.append("<li><b>Beneficiario:</b> " 				+obtenerValor(rs,"nombenef", 			"") + "</li>");
					mensaje.append("<li><b>Estatus:</b> " 						+obtenerValor(rs,"nomestatus", 		"") + "</li>");
					if(!"001".equals(estatus) && !"webservice".equals(etapa)){
						mensaje.append("<li><b>Observaciones:</b> " 			+obtenerValor(rs,"observaciones", 	"NINGUNA") + "</li>");
					}
					mensaje.append("</ul>");
					mensaje.append("<br>");
					mensaje.append("<p>Con el proposito de dar seguimientos a sus fianzas lo invitamos a ingresar al portal ");
					mensaje.append("de Cadenas Productivas (http://cadenas.nafin.com.mx) en el modulo de Fianza Electr&oacute;nica.</p>");
					mensaje.append("<p>En el evento de que esta informaci&oacute; no le corresponda favor de hacer caso omiso al mismo. </p> ");
					mensaje.append("<br>");
					mensaje.append("<p>ATENTAMENTE<br><b>NACIONAL FINANCIERA, S.N.C.</b></p> ");
					mensaje.append("<br>");
					mensaje.append("<p><b>Nota:</b> Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar ");
					mensaje.append("correos electr&oacute;nicos de entrada. Por favor no responder a este mensaje.</p>");
					mensaje.append("</form>");

					Correo correo = new Correo();
					log.debug("Cuentas de correo: "+correos);
					if(correos!=null && !correos.equals(""))
						correo.enviarTextoHTML("NO_RESPONSE@NAFIN.GOB.MX", correos,asunto, mensaje.toString(),copiaCo);
				}

			}
			rs.close();
			ps.close();


		}catch(Throwable t){
			log.error("setEnvioCorreoFianzas(Error)");
			t.printStackTrace();
			throw new AppException("Error en el envio de correo de fianzas",t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEnvioCorreoFianzas(S)");
		}
	}




	/**
	 * Obtiene la parametrizacion del tipo comision a aplicar para el
	 * modulo de Fianza Electronica (F-Fija o R-Rangos)
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public String getTipoComisionFE()throws AppException{
		log.info("getTipoComisionFE(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String cgTipoComision = "";
		try{
			con.conexionDB();
			query = "SELECT cg_tipo_comision " +
					"  FROM com_param_gral ";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				cgTipoComision = rs.getString("cg_tipo_comision")==null?"":rs.getString("cg_tipo_comision");
			}
			rs.close();
			ps.close();

			return cgTipoComision;
		}catch(Throwable t){
			log.info("getTipoComisionFE(Error)",t);
			t.printStackTrace();
			throw new AppException("Error al consultar el tipo de comision para fianza electronica", t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTipoComisionFE(S)");
		}
	}

	/**
	 * Obtiene monto comision e iva de la Comison Fija para Fianza Electronica
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public HashMap getComisionFija()throws AppException{
		log.info("getComisionFija(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap map = new HashMap();
		String query="";
		try{
			con.conexionDB();
			query = "SELECT fg_monto_comision, fg_iva " +
					"  FROM fe_comision " +
					" WHERE cg_tipo_comision = ?  " +
					" AND cs_habilitada = ? ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,"F");
			ps.setString(2,"S");
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				map.put("MONTO",rs.getString("fg_monto_comision")==null?"":rs.getString("fg_monto_comision"));
				map.put("IVA",rs.getString("fg_iva")==null?"":rs.getString("fg_iva"));
			}
			rs.close();
			ps.close();

			return map;

		}catch(Throwable t){
			log.info("getComisionFija(Error)");
			t.printStackTrace();
			throw new AppException("Error al obtener la comision fija para las fianzas",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getComisionFija(S)");
		}
	}

	/**
	 * Establece el monto de la Comision Fija para las Fianzas Electronicas
	 * @throws netropology.utilerias.AppException
	 * @param iva - porcentaje de iva
	 * @param montoComision - monto de la comision que se aplicara por fianza
	 */
	public void setComisionFija(String montoComision, String iva)throws AppException{
		log.info("setComisionFija(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String icComision = "";
		boolean commit = true;
		try{
			con.conexionDB();
			query = "SELECT seq_fe_comision.NEXTVAL FROM dual ";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				icComision = rs.getString(1);
			}
			rs.close();
			ps.close();

			query = "INSERT INTO fe_comision " +
					"            (ic_comision, cg_tipo_comision, fg_monto_comision, fg_iva " +
					"            ) " +
					"     VALUES (?, ?, ?, ? " +
					"            ) ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(icComision));
			ps.setString(2,"F");
			ps.setDouble(3,Double.parseDouble(montoComision));
			ps.setDouble(4,Double.parseDouble(iva));
			ps.executeUpdate();
			ps.close();

			query = "UPDATE fe_comision " +
					"   SET cs_habilitada = ? " +
					" WHERE cg_tipo_comision = ? "+
					" AND ic_comision != ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,"N");
			ps.setString(2,"F");
			ps.setLong(3,Long.parseLong(icComision));
			ps.executeUpdate();
			ps.close();

			query = "UPDATE com_param_gral " +
					"   SET cg_tipo_comision = ? ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,"F");
			ps.executeUpdate();
			ps.close();


		}catch(Throwable t){
			log.info("setComisionFija(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error en el guardado de la comision fija",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setComisionFija(S)");
		}
	}

	/**
	 * Se obtiene la comison por rangos vigente para fianza electronica
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public List getComisionPorRangos()throws AppException{
		log.info("getComisionPorRangos(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstRegistros = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			query= " SELECT  f.ic_comision, f.fg_monto_ini,  " +
					"		f.fg_monto_fin, f.fg_porcentaje, f.fg_iva " +
					"    FROM fe_comision f " +
					"   WHERE f.cs_habilitada = ? " +
					"   AND f.cg_tipo_comision = ? " +
					" ORDER BY f.ic_comision ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,"S");
			ps.setString(2,"R");
			rs = ps.executeQuery();
			while(rs!=null && rs.next()){
				HashMap map = new HashMap();
				map.put("CVECOMISION",rs.getString("ic_comision"));
				map.put("MONTOINI",rs.getString("fg_monto_ini"));
				map.put("MONTOFIN",rs.getString("fg_monto_fin"));
				map.put("PORCENTAJE",rs.getString("fg_porcentaje"));
				map.put("IVA",rs.getString("fg_iva"));
				lstRegistros.add(map);
			}

			rs.close();
			ps.close();

			return lstRegistros;

		}catch(Throwable t){
			log.info("getComisionPorRangos(Error)");
			t.printStackTrace();
			throw new AppException("Error en la obtencion de comsion por rangos", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getComisionPorRangos(S)");
		}
	}

	/**
	 * Estable la comison de fianzas electronicas por rangos
	 * @throws netropology.utilerias.AppException
	 * @param iva - valor del iva
	 * @param lstRangos - lista de rangos a establecer
	 */
	public void setComisionPorRangos(List lstRangos, String iva)throws AppException{
		log.info("setComisionPorRangos(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		String icGrupoRango = "";
		String icComision = "";
		String query = " select SEQ_FE_COMISION_RANGOS.NEXTVAL from dual ";
		try{
			con.conexionDB();
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()) icGrupoRango = rs.getString(1);
			rs.close();
			ps.close();

			for(int x=0;x<lstRangos.size();x++){
				Map map = (Map)lstRangos.get(x);
				query = " select seq_fe_comision.nextval from dual ";
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()) icComision = rs.getString(1);
				rs.close();
				ps.close();

				query = "INSERT INTO fe_comision " +
						"            (ic_comision, cg_tipo_comision, fg_monto_ini, fg_monto_fin, " +
						"             fg_porcentaje, fg_iva, ig_grupo_rangos " +
						"            ) " +
						"     VALUES (?, ?, ?, ?, " +
						"             ?, ?, ? ) ";

				ps = con.queryPrecompilado(query);
				ps.setLong(1, Long.parseLong(icComision));
				ps.setString(2, "R");
				ps.setDouble(3, Double.parseDouble(String.valueOf(map.get("MONTOINI"))));
				ps.setDouble(4, Double.parseDouble(String.valueOf(map.get("MONTOFIN"))));
				ps.setDouble(5, Double.parseDouble(String.valueOf(map.get("PORCENTAJE"))));
				ps.setDouble(6, Double.parseDouble(iva));
				ps.setLong(7, Long.parseLong(icGrupoRango));
				ps.executeUpdate();
				ps.close();
			}

			query = "UPDATE fe_comision " +
					"   SET cs_habilitada = ? " +
					" WHERE cg_tipo_comision = ?  " +
					" AND ig_grupo_rangos not in(?) ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,"N");
			ps.setString(2,"R");
			ps.setLong(3, Long.parseLong(icGrupoRango));
			ps.executeUpdate();
			ps.close();

			query = "UPDATE com_param_gral " +
					"   SET cg_tipo_comision = ? ";
			ps = con.queryPrecompilado(query);
			ps.setString(1,"R");
			ps.executeUpdate();
			ps.close();

		}catch(Throwable t){
			log.info("setComisionPorRangos(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error en la asignacion de Comision por Rangos", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}

			log.info("setComisionPorRangos(S)");
		}
	}

	/**
	 * Realilza la consulta de la comision apliacada a las fianzas electronicas
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param anio
	 * @param mesFin
	 * @param mesIni
	 * @param cveAfianza
	 */
	public List getConsComisiones(String cveAfianza, String mesIni, String mesFin, String anio) throws AppException{
		log.info("getComisiones(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstConsComi = new ArrayList();
		StringBuffer query = new StringBuffer();
		try{
			con.conexionDB();
			query.append("SELECT   SUM (fr.fg_monto) montocomi, SUM (fr.fg_monto_iva) montoiva, " +
				"         SUM (fr.fg_monto + fr.fg_monto_iva) montotot, " +
				"         CASE WHEN MAX (fc.fg_iva) IS NULL THEN '0' END AS iva, " + // MAX (fc.fg_iva) iva,
				"         fr.ig_mes mes, fr.ig_anio anio " +
				"    FROM fe_fianza ff, fe_comision fc, ferel_comision_fianza fr " +
				"   WHERE ff.ic_fianza = fr.ic_fianza " +
				"     AND fr.ic_comision = fc.ic_comision ");
				if(cveAfianza!=null && !cveAfianza.equals(""))
					query.append("     AND ff.ic_afianzadora = ? ");
				if(mesIni!=null && !mesIni.equals(""))
					query.append("     AND fr.ig_mes >= ? ");
				if(mesFin!=null && !mesFin.equals(""))
					query.append("     AND fr.ig_mes <= ? ");
				if(anio!=null && !anio.equals(""))
					query.append("     AND fr.ig_anio = ? ");

				query.append("GROUP BY fr.ig_mes, fr.ig_anio  ");

			int p = 0;
			ps = con.queryPrecompilado(query.toString());
			if(cveAfianza!=null && !cveAfianza.equals(""))
				ps.setLong(++p,Long.parseLong(cveAfianza));
			if(mesIni!=null && !mesIni.equals(""))
				ps.setLong(++p,Long.parseLong(mesIni));
			if(mesFin!=null && !mesFin.equals(""))
				ps.setLong(++p,Long.parseLong(mesFin));
			if(anio!=null && !anio.equals(""))
				ps.setLong(++p,Long.parseLong(anio));

			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				HashMap map = new HashMap();
				map.put("MES", rs.getString("mes"));
				map.put("ANIO",rs.getString("anio"));
				map.put("MONTOCOMISION",rs.getString("montocomi"));
				map.put("IVA",rs.getString("iva"));
				map.put("MONTOIVA",rs.getString("montoiva"));
				map.put("MONTOTOTAL",rs.getString("montotot"));
				lstConsComi.add(map);
			}

			return lstConsComi;

		}catch(Throwable t){
			log.info("getComisiones(Error)");
			t.printStackTrace();
			throw new AppException("Error al realizas la consulta de Comisiones por Fianzas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getComisiones(S)");
		}
	}


	/**
	 * Calcula la comison por fianza y la registra
	 * @throws netropology.utilerias.AppException
	 * @param con - Objeto conexion de BD
	 * @param cveFianza - clave de la fianza
	 */
	private void setComisonFianza(String cveFianza, AccesoDB con)throws AppException{
		log.info("setComisonFianza(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		String query = "";
		try{

			String montoFianza = "";
			String montoIva = "";
			String idMovimiento = "";

			query = "SELECT ff.ic_fianza, ff.fg_monto, ff.fg_monto_movimiento, ff.ig_idmovimiento " +
					"  FROM fe_fianza ff " +
					" WHERE ff.ic_fianza = ? ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFianza));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				montoFianza = rs.getString("fg_monto_movimiento");
				idMovimiento = rs.getString("ig_idmovimiento");
			}
			rs.close();
			ps.close();

			if("11".equals(idMovimiento) || "12".equals(idMovimiento) || "13".equals(idMovimiento) || "52".equals(idMovimiento)){

				//SE OBTIENE EL TIPO DE COMSION QUE ESTE PARAMETRIZADO
				String tipoComision = "";
				String icComision = "";
				String strMontoComision = "";

				query = "SELECT cg_tipo_comision " +
						  "  FROM com_param_gral ";

				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					tipoComision = rs.getString("cg_tipo_comision");
				}
				rs.close();
				ps.close();

				//SE OBTIENE COMISION A APLICAR
				if(tipoComision!=null && !"".equals(tipoComision)){
					query = "SELECT fc.ic_comision, fc.fg_monto_comision, fc.fg_monto_ini,  " +
							"	   fc.fg_monto_fin, fc.fg_iva, fc.fg_porcentaje " +
							"  FROM fe_comision fc " +
							" WHERE fc.cg_tipo_comision = ? AND fc.cs_habilitada = ? ";

					ps = con.queryPrecompilado(query);

					if("F".equals(tipoComision)){
						ps.setString(1,"F");
						ps.setString(2,"S");
					}else if("R".equals(tipoComision)){
						ps.setString(1,"R");
						ps.setString(2,"S");
					}
					rs = ps.executeQuery();

					double montoComision = 0.0;
					double montoIni = 0.0;
					double montoFin = 0.0;
					double iva = 0.0;
					double porcentaje = 0.0;

					while(rs!=null && rs.next()){


						if("F".equals(tipoComision)){
							montoComision = rs.getDouble("fg_monto_comision");
							strMontoComision = Comunes.formatoDecimal(String.valueOf(montoComision),2,false);
							iva = rs.getDouble("fg_iva");
							icComision = rs.getString("ic_comision");
							montoIva = Comunes.formatoDecimal(String.valueOf(montoComision * ((iva/100))),2,false);;
						}else if("R".equals(tipoComision)){
							montoIni = rs.getDouble("fg_monto_ini");
							montoFin = rs.getDouble("fg_monto_fin");
							iva = rs.getDouble("fg_iva");
							porcentaje = rs.getDouble("fg_porcentaje");

							if(Double.parseDouble(montoFianza)>=montoIni && Double.parseDouble(montoFianza)<=montoFin){
								icComision = rs.getString("ic_comision");
								montoComision = Double.parseDouble(montoFianza)*(porcentaje/100);
								strMontoComision = Comunes.formatoDecimal(String.valueOf(montoComision),2,false);
								montoIva = Comunes.formatoDecimal(String.valueOf((montoComision)*((iva/100))),2,false);
							}
						}
					}
					rs.close();
					ps.close();

					//SE REALIZA REGISTRO DE LA COMISION GENERADA POR FIANZA
					if(!"".equals(icComision)){
						query = "INSERT INTO ferel_comision_fianza " +
							"            (ic_comision_fianza, ic_comision, ic_fianza, fg_monto, " +
							"             fg_monto_iva " +
							"            ) " +
							"     VALUES (seq_ferel_comision_fianza.nextval, ?, ?, ?, ?) ";

						ps = con.queryPrecompilado(query);
						ps.setLong(1, Long.parseLong(icComision));
						ps.setLong(2, Long.parseLong(cveFianza));
						ps.setDouble(3, Double.parseDouble(strMontoComision));
						ps.setDouble(4, Double.parseDouble(montoIva));
						ps.executeUpdate();
						ps.close();
					}
				}
			}

		}catch(Throwable t){
			log.info("setComisonFianza(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error en el calculo y asignacion de la comsion por fianza enviada");
		}finally{
			log.info("setComisonFianza(S)");
		}
	}

	/**
	 * Obtiene el numero de nafin electronico del fiado
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param tipoAfiliado
	 * @param con
	 * @param claveAfiliado
	 */
	private String sgetNoNafinElectronico(String claveAfiliado,AccesoDB con, String tipoAfiliado)throws AppException {
		log.info("sgetNoNafinElectronico(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			String numNafinElectronico = null;
			String strSQL =
					" SELECT ic_nafin_electronico " +
					" FROM comrel_nafin " +
					" WHERE ic_epo_pyme_if = ? " +
					" 	AND cg_tipo = ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveAfiliado));
			ps.setString(2, tipoAfiliado);
			rs = ps.executeQuery();
			if (rs.next()) {
					numNafinElectronico = rs.getString("ic_nafin_electronico");
			}
			rs.close();
			ps.close();

			return numNafinElectronico;
		}catch(Throwable t){
			log.info("sgetNoNafinElectronico(Error)");
			t.printStackTrace();
			throw new AppException("Error en la consulta del Nafin Electronico");
		}finally{
			log.info("sgetNoNafinElectronico(S)");
		}

	}

	/**
	 * Obtiene datos del Fiado especificado por si clave
	 * @return
	 * @param tipoAfil - tipo de afiliado
	 * @param cveFiado - Clave del fiado
	 * @param con - conexion BD
	 */
	private List getDatosFiado(AccesoDB con, String cveFiado, String tipoAfil)throws AppException{
		log.info("getDatosFiado(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		List lstRegistro = new ArrayList();
		try{

			String nombreFiado;
			String rfcFiado;
			String calleFiado;
			String noExteriorFiado;
			String noIntFiado;
			String colFiado;
			String locFiado;
			String refFiado;
			String municFiado;
			String estadoFiado;
			String paisFiado;
			String cpFiado;
			String nombreRL;
			String emailRL;
			String telefonoRL;
			String ventFiado;
			String numEmpFiado;
			String sectorEcFiado;
			String estratoFiado;

			query = "SELECT ff.ic_fiado , ff.cg_nombre nombreFiado, ff.cg_rfc rfcFiado, ff.cg_calle calleFiado, " +
				"       ff.cg_referencia refFiado, ff.cg_noexterior noExteriorFiado, ff.cg_municipio municFiado, ff.cg_nointerior noIntFiado, " +
				"       ff.cg_estado estadoFiado, ff.cg_colonia colFiado, ff.cg_pais paisFiado, ff.cg_localidad locFiado, ff.cg_cp cpFiado, " +
				"       ff.cg_rl_nombre nombreRL, ff.cg_rl_telefono telefonoRL, ff.cg_rl_correo emailRL, " +
				"       ff.fg_ventas_anuales ventFiado, ff.ic_sector sectorEcFiado, ff.ig_numero_epleados numEmpFiado, " +
				"       ff.ic_estrato cveEstratoFiado, ce.cd_nombre estratoFiado" +
				"  FROM fe_fiado ff, comrel_nafin cn, comcat_estrato ce " +
				" WHERE ff.ic_fiado = cn.ic_epo_pyme_if " +
				"   AND ce.ic_estrato = ff.ic_estrato " +
				"   AND cn.cg_tipo = ? " +
				"   AND ff.ic_fiado = ? ";

			ps = con.queryPrecompilado(query);
			ps.setString(1,tipoAfil);
			ps.setLong(2,Long.parseLong(cveFiado));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				nombreFiado = rs.getString("nombreFiado")==null?"":rs.getString("nombreFiado");
				rfcFiado = rs.getString("rfcFiado")==null?"":rs.getString("rfcFiado");
				calleFiado = rs.getString("calleFiado")==null?"":rs.getString("calleFiado");
				noExteriorFiado = rs.getString("noExteriorFiado")==null?"":rs.getString("noExteriorFiado");
				noIntFiado = rs.getString("noIntFiado")==null?"":rs.getString("noIntFiado");
				colFiado = rs.getString("colFiado")==null?"":rs.getString("colFiado");
				locFiado = rs.getString("locFiado")==null?"":rs.getString("locFiado");
				refFiado = rs.getString("refFiado")==null?"":rs.getString("refFiado");
				municFiado = rs.getString("municFiado")==null?"":rs.getString("municFiado");
				estadoFiado = rs.getString("estadoFiado")==null?"":rs.getString("estadoFiado");
				paisFiado = rs.getString("paisFiado")==null?"":rs.getString("paisFiado");
				cpFiado = rs.getString("cpFiado")==null?"":rs.getString("cpFiado");
				nombreRL = rs.getString("nombreRL")==null?"":rs.getString("nombreRL");
				emailRL = rs.getString("emailRL")==null?"":rs.getString("emailRL");
				telefonoRL = rs.getString("telefonoRL")==null?"":rs.getString("telefonoRL");
				ventFiado = rs.getString("ventFiado")==null?"":rs.getString("ventFiado");
				numEmpFiado = rs.getString("numEmpFiado")==null?"":rs.getString("numEmpFiado");
				sectorEcFiado = rs.getString("sectorEcFiado")==null?"":rs.getString("sectorEcFiado");
				estratoFiado = rs.getString("estratoFiado")==null?"":rs.getString("estratoFiado");

				lstRegistro.add(0, nombreFiado);
				lstRegistro.add(1, rfcFiado);
				lstRegistro.add(2, calleFiado);
				lstRegistro.add(3, noExteriorFiado);
				lstRegistro.add(4, noIntFiado);
				lstRegistro.add(5, colFiado);
				lstRegistro.add(6, locFiado);
				lstRegistro.add(7, refFiado);
				lstRegistro.add(8, municFiado);
				lstRegistro.add(9, estadoFiado);
				lstRegistro.add(10, paisFiado);
				lstRegistro.add(11, cpFiado);
				lstRegistro.add(12, nombreRL);
				lstRegistro.add(13, emailRL);
				lstRegistro.add(14, telefonoRL);
				lstRegistro.add(15, ventFiado);
				lstRegistro.add(16, numEmpFiado);
				lstRegistro.add(17, sectorEcFiado);
				lstRegistro.add(18, estratoFiado);
			}
			rs.close();
			ps.close();
			return lstRegistro;
		}catch(Throwable t){
			log.info("getDatosFiado(Error)");
			t.printStackTrace();
			throw new AppException("Error al consultar info de FIADO",t);
		}finally{
			log.info("getDatosFiado(S)");
		}
	}

	/**
	 * Se realiza la actualizaci�n de informacion del Fiado y genera registro en bitacora general
	 * @throws netropology.utilerias.AppException
	 * @param mDataFiado
	 * @param noUsuario
	 * @param cveFiado
	 */
	public void actuzalizaFiado( String cveFiado, String noUsuario, HashMap mDataFiado)throws AppException{
		log.info("actuzalizaFiado(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String query = "";
		boolean commit = true;
		List datosAnteriores = new ArrayList();
		List datosActuales = new ArrayList();
		StringBuffer valoresAnteriores = new StringBuffer();
		StringBuffer valoresActuales = new StringBuffer();
		try{
			con.conexionDB();

			datosAnteriores = getDatosFiado(con, cveFiado,"F");

			//---------------------------------------------------------------------
			query = "UPDATE fe_fiado " +
				"   SET cg_nombre = ?, " +
				"       cg_rfc = ?, " +
				"       cg_calle = ?, " +
				"       cg_referencia = ?, " +
				"       cg_noexterior = ?, " +
				"       cg_municipio = ?, " +
				"       cg_nointerior = ?, " +
				"       cg_estado = ?, " +
				"       cg_colonia = ?, " +
				"       cg_pais = ?, " +
				"       cg_localidad = ?, " +
				"       cg_cp = ?, " +
				"       cg_rl_nombre = ?, " +
				"       cg_rl_telefono = ?, " +
				"       cg_rl_correo = ?, " +
				"       fg_ventas_anuales = ?, " +
				"       ic_sector = ?, " +
				"       ig_numero_epleados = ?, " +
				"       ic_estrato = ? " +
				" WHERE ic_fiado = ? ";

			int p = 0;
			ps = con.queryPrecompilado(query);
			ps.setString(++p,(String)mDataFiado.get("nombreFiado"));
			ps.setString(++p,(String)mDataFiado.get("rfcFiado"));
			ps.setString(++p,(String)mDataFiado.get("calleFiado"));
			ps.setString(++p,(String)mDataFiado.get("refFiado"));
			ps.setString(++p,(String)mDataFiado.get("noExteriorFiado"));
			ps.setString(++p,(String)mDataFiado.get("municFiado"));
			ps.setString(++p,(String)mDataFiado.get("noIntFiado"));
			ps.setString(++p,(String)mDataFiado.get("estadoFiado"));
			ps.setString(++p,(String)mDataFiado.get("colFiado"));
			ps.setString(++p,(String)mDataFiado.get("paisFiado"));
			ps.setString(++p,(String)mDataFiado.get("locFiado"));
			ps.setString(++p,(String)mDataFiado.get("cpFiado"));
			ps.setString(++p,(String)mDataFiado.get("nombreRL"));
			ps.setString(++p,(String)mDataFiado.get("telefonoRL"));
			ps.setString(++p,(String)mDataFiado.get("emailRL"));
			if(mDataFiado.get("ventFiado")!=null && !"".equals((String)mDataFiado.get("ventFiado")) )
				ps.setDouble(++p,Double.parseDouble((String)mDataFiado.get("ventFiado")));
			else
				ps.setNull(++p, Types.DOUBLE);

			if(mDataFiado.get("sectorEcFiado")!=null && !"".equals((String)mDataFiado.get("sectorEcFiado")) )
				ps.setLong(++p,Long.parseLong((String)mDataFiado.get("sectorEcFiado")));
			else
				ps.setNull(++p, Types.INTEGER);

			if(mDataFiado.get("numEmpFiado")!=null && !"".equals((String)mDataFiado.get("numEmpFiado")) )
				ps.setLong(++p,Long.parseLong((String)mDataFiado.get("numEmpFiado")));
			else
				ps.setNull(++p, Types.INTEGER);

			ps.setString(++p,(String)mDataFiado.get("estratoFiado"));
			ps.setLong(++p,Long.parseLong(cveFiado));
			ps.executeUpdate();
			ps.close();

			datosActuales = getDatosFiado(con, cveFiado,"F");

			String[] nombres = {"Nombre", "RFC", "Calle", "No. Exterior", "No. Interior",
									"Colonia", "Localidad", "Referencia", "Municipio", "Estado",
									"Pais", "C.P.", "Nombre Rep. Legal", "Email Rep. Legal",
									"Telefono Rep. Legal", "Ventas Anuales", "No. Empleados",
									"Sector Economico", "Estrato"};

			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));


			Bitacora.grabarEnBitacora(con, "CONSAFIL", "F", sgetNoNafinElectronico(cveFiado, con, "F"), noUsuario, valoresAnteriores.toString(), valoresActuales.toString());

		}catch(Throwable t){
			log.info("actuzalizaFiado(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al actualizar Fiado",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("actuzalizaFiado(S)");
		}
	}

	/**
	 * Realiza borrado de Fiados
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param noUsuario
	 * @param cveFiado
	 */
	public boolean borrarFiado(String cveFiado, String noUsuario) throws AppException{
		log.info("borrarFiado(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		boolean existFianza = false;
		boolean borrado = false;
		String query = "";
		try{
			con.conexionDB();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer("BAJA");
			String[] nombres = {"Nombre", "RFC", "Calle", "No. Exterior", "No. Interior",
									"Colonia", "Localidad", "Referencia", "Municipio", "Estado",
									"Pais", "C.P.", "Nombre Rep. Legal", "Email Rep. Legal",
									"Telefono Rep. Legal", "Ventas Anuales", "No. Empleados",
									"Sector Economico", "Estrato"};


			query = "SELECT COUNT (1) " +
					"  FROM fe_fianza " +
					" WHERE ic_fiado = ? ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFiado));
			rs = ps.executeQuery();

			if(rs!=null && rs.next()){
				if(rs.getInt(1)>0)existFianza= true;
			}
			rs.close();
			ps.close();

			if(!existFianza){

				String nafElec = sgetNoNafinElectronico(cveFiado, con, "F");
				List datosAnteriores = getDatosFiado(con, cveFiado,"F");
				valoresAnteriores.append(Bitacora.getValoresBitacora(datosAnteriores, nombres));

				query = "DELETE comrel_nafin WHERE ic_epo_pyme_if = ? and cg_tipo = ? ";
				ps = con.queryPrecompilado(query);
				ps.setLong(1,Long.parseLong(cveFiado));
				ps.setString(2,"F");
				ps.executeUpdate();
				ps.close();

				query = "DELETE fe_fianza WHERE ic_fiado = ? ";
				ps = con.queryPrecompilado(query);
				ps.setLong(1,Long.parseLong(cveFiado));
				ps.executeUpdate();
				ps.close();

				Bitacora.grabarEnBitacora(con, "CONSAFIL", "F", nafElec, noUsuario, valoresAnteriores.toString(), valoresActuales.toString());

				borrado = true;
			}

			return borrado;
		}catch(Throwable t){
			log.info("borrarFiado(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al borrar Fiado",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("borrarFiado(S)");
		}
	}

	/**
	 * Dado un <tt>ResultSet</tt> lee el parametro indicado cuyo nombre es <tt>nombreCampo</tt>
	 * y en el caso que este parametro venga vacio, lo reemplaza por el <tt>valorDefault</tt>
	 *
	 * @throws Exception
	 * @author jshernandez
	 *
	 * @param ResultSet Objeto <tt>ResultSet</tt> con el resultado de la consulta
	 * @param nombreCampo <tt>String</tt> con el nombre del campo a consultar
	 * @param valorDefault <tt>String</tt> con el valor default que se enviara en caso
	 *                     de que el valor del campo leido sea <tt>null</tt> o venga vacio.
	 *
	 * @return Cadena de Texto con los caracteres especiales de HTML codificados
	 *
	 */
	private String obtenerValor(ResultSet rs,String nombreCampo, String valorDefault)
      throws Exception{
		String valor = rs.getString(nombreCampo);
		valor = valor == null || valor.trim().equals("")?valorDefault:valor;
		return Comunes.escapaCaracteresEspeciales(valor);
	}


	/**
	 *  metodo para saber si el Fiado ya acepto los Terminos y Condiciones
	 *  para el acceso al m�dulo de Fianza Electr�nica
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cveFiado
	 */
	public String consultaTerminos(String cveFiado) throws AppException{
		log.info("consultaTerminos(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		String query = "";
		String terminos  ="N";
		try{
			con.conexionDB();
			query =  "SELECT CS_ACEPTACION_TERMINOS  "+
							 "FROM FE_FIADO WHERE  ic_fiado = ?  ";
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveFiado));
			rs = ps.executeQuery();

			if(rs.next()){
				terminos  = rs.getString("CS_ACEPTACION_TERMINOS")==null?"N":rs.getString("CS_ACEPTACION_TERMINOS");
			}
			rs.close();
			ps.close();



		}catch(Throwable t){
			log.info("consultaTerminos(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error consultaTerminos",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}

			log.info("consultaTerminos(S)");
		}
		return terminos;
	}


	/**
	 *  Metodo que guarda las parametrizaciones de los Terminos y Condiciones
	 *  para el acceso al m�dulo de Fianza Electr�nica
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cveFiado
	 * @param versionPDF
	 * @param terminos
	 */
	public String capturaTerminos(String terminos, String versionPDF, String cveFiado) throws AppException{
		log.info("capturaTerminos(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean commit = true;
		String query = "";
		String Acepterminos  ="S";
		try{
			con.conexionDB();
			query =  "UPDATE  FE_FIADO "+
							 " SET CS_ACEPTACION_TERMINOS = ?, "+
							 " CG_VERSION_TERMINOS = ?,  "+
							 " DF_ACEPTA_TERMINOS = Sysdate"+
							 " WHERE IC_FIADO  = ?  ";
			ps = con.queryPrecompilado(query);
			ps.setString (1,terminos);
			ps.setString(2,versionPDF);
			ps.setLong(3,Long.parseLong(cveFiado));
			ps.executeUpdate();
			ps.close();

		}catch(Throwable t){
			log.info("capturaTerminos(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error capturaTerminos",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}

			log.info("capturaTerminos(S)");
		}
		return Acepterminos;
	}



}