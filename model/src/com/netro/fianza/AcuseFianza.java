package com.netro.fianza;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class AcuseFianza implements java.io.Serializable {
	public static final int ACUSE_CONCENTRADO=1;
	public static final int ACUSE_REVISION=2;
	public static final int ACUSE_AUTORIZACION=3;
	
	private String acuse;
	private String claveUsuario;
	private String fecha;
	private String hora;
	private String reciboElectronico;
	private String estatusFianza;
	private String icProductoNafin;
	private String totalRechazadas;
	private String totalAutorizadas;
	private String totalRetornadas;
	
	private final static Log log = ServiceLocator.getInstance().getLog(FianzaElectronicaBean.class);
	
	public AcuseFianza(int tipoAcuse ) {
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			icProductoNafin = "10";
			this.setAcuse(tipoAcuse, con);
		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error en generacion de acuse ", t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
	
	public AcuseFianza(int tipoAcuse, AccesoDB con) {
		icProductoNafin = "10";
		this.setAcuse(tipoAcuse, con);
	}
	
	
	private void setAcuse(int tipoAcuse, AccesoDB con) throws AppException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String consecutivo;
		String tmp;
		java.util.Date date = new java.util.Date();
		String fechaAcu = (new SimpleDateFormat ("ddMMyy")).format(date);
		String qrySentencia = "";
		String strTipoAcuse = "";
		String estatus = "";
		String prefijo = "";
		totalRechazadas = "";
		totalAutorizadas = "";
		totalRetornadas = "";
		
		char alfanumericos[] = {'A', 'B', 'C', 'D', 'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
		int digitos = 5;
		int rnd;
		int tipoAcuseTabla = 0;
		int numDigitos = 5;  
		

		rnd = (new Integer((int)(Math.random()*alfanumericos.length))).intValue();
		char aleatorio1 = alfanumericos[rnd];
		rnd = (new Integer((int)(Math.random()*alfanumericos.length))).intValue();
		char aleatorio2 = alfanumericos[rnd];
		
		fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(date);
		hora = (new SimpleDateFormat ("HH:mm:ss")).format(date);

		try {

			if (tipoAcuse == 1 || tipoAcuse == 2 || tipoAcuse == 3 ) {
				strTipoAcuse = String.valueOf(tipoAcuse);
				if (tipoAcuse == 1) prefijo = "FIA1";
				if (tipoAcuse == 2) prefijo = "FIA2";
				if (tipoAcuse == 3) prefijo = "FIA3";
			}
 			
			qrySentencia="select count(1)+1 as consecutivo"+
				" from fe_acuse " +
				" where cc_acuse like '?' " +
				" and ic_producto_nafin= ? ";

			log.debug(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, prefijo+"%");
			ps.setInt(1, 10);
			rs = ps.executeQuery();
			if (rs.next()) {
				consecutivo=rs.getString("CONSECUTIVO");
				if (consecutivo == null) {
					consecutivo="1";
				}
			} else {
				consecutivo="1";
			}
			rs.close();
			ps.close();
			
			tmp=consecutivo;
			for (int i=1;i<=digitos-consecutivo.length();i++)	{
				tmp="0"+tmp;
			}
			consecutivo=tmp;

			acuse = prefijo+strTipoAcuse+fechaAcu+aleatorio1+aleatorio2+consecutivo;

		} catch (Throwable e) {
			log.error("Error al generar el acuse de la fianza");
			throw new AppException("Error al generar el acuse de la fianza ", e);
		}
	}


	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}


	public String getAcuse() {
		return acuse;
	}


	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}


	public String getClaveUsuario() {
		return claveUsuario;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getFecha() {
		return fecha;
	}


	public void setReciboElectronico(String reciboElectronico) {
		this.reciboElectronico = reciboElectronico;
	}


	public String getReciboElectronico() {
		return reciboElectronico;
	}


	public void setEstatusFianza(String estatusFianza) {
		this.estatusFianza = estatusFianza;
	}


	public String getEstatusFianza() {
		return estatusFianza;
	}


	public void setIcProductoNafin(String icProductoNafin) {
		this.icProductoNafin = icProductoNafin;
	}


	public String getIcProductoNafin() {
		return icProductoNafin;
	}


	public void setHora(String hora) {
		this.hora = hora;
	}


	public String getHora() {
		return hora;
	}


	public void setTotalRechazadas(String totalRechazadas) {
		this.totalRechazadas = totalRechazadas;
	}


	public String getTotalRechazadas() {
		return totalRechazadas;
	}


	public void setTotalAutorizadas(String totalAutorizadas) {
		this.totalAutorizadas = totalAutorizadas;
	}


	public String getTotalAutorizadas() {
		return totalAutorizadas;
	}


	public void setTotalRetornadas(String totalRetornadas) {
		this.totalRetornadas = totalRetornadas;
	}


	public String getTotalRetornadas() {
		return totalRetornadas;
	}

}