package com.netro.fianza;
import com.netro.fianza.DatosFianzaWS;
import java.io.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.*;

public class FianzaXMLtoObject extends DefaultHandler  {
	private DatosFianzaWS tempFianza;
	private String tempVal;
	private boolean domEmisor;
	private boolean domFiado;
	private boolean textFianza;
	private boolean textRestric;
	private boolean textNormCNSF;
	
	public FianzaXMLtoObject() {
	}
	
	public  DatosFianzaWS  parseDocument( byte[] bytesXml) {

		//get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {

			//Se obtiene nueva instancia de parser
			SAXParser sp = spf.newSAXParser();

			//se parsea el arreglo de bytes y se pasa la clase como el manejador de eventos
			ByteArrayInputStream bais = new ByteArrayInputStream(bytesXml);
			InputSource is = new InputSource(bais);
			is.setEncoding("UTF-8");
			sp.parse(is, this);
			

		}catch(SAXException se) {
			se.printStackTrace();
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch (IOException ie) {
			ie.printStackTrace();
		}
		return tempFianza;
	}
	      
	
	//Event Handlers
	public void startElement(String uri, String localName, String qName,
		Attributes attributes) throws SAXException {
		//reset
		tempVal = "";
		if(qName.equalsIgnoreCase("Fianza")) {
			//crea nueva instancia para Datos de la Fianza
			tempFianza = new DatosFianzaWS();
			tempFianza.setVersionEsquema(attributes.getValue("versionEsquema"));
			tempFianza.setNumeroCompaniaAfianzadora(attributes.getValue("numeroCompaniaAfianzadora"));
			tempFianza.setSerie(attributes.getValue("serie"));
			tempFianza.setFolio(attributes.getValue("folio"));
			tempFianza.setNumeroFianza(attributes.getValue("numeroFianza"));
			tempFianza.setNumeroInclusion(attributes.getValue("numeroInclusion"));
			tempFianza.setNumeroVersion(attributes.getValue("numeroVersion"));
			tempFianza.setIdPoliza(attributes.getValue("IdPoliza"));
			//tempFianza.setIdMovimiento(attributes.getValue("IdMovimiento"));
			// FODEA 028-2012 : SE COMENTA Y SE OBTIENE DEL AddendaAfianzadora con el paramtro IdMovimientoFianza
         //tempFianza.setIdMovimiento("11");//MODIFICACION TEMPORAL A PETICION DE ALEJANDRO AGUIRRE PARA ASUMIR QUE SIEMPRE ESTAN ENVIANDO FIANZAS CON TIPO DE MOVIMIENTO 11 (27/11/2012)
			tempFianza.setIdSolicitud(attributes.getValue("IdSolicitud"));
			tempFianza.setMovimiento(attributes.getValue("movimiento"));
			tempFianza.setMontoAfianzado(attributes.getValue("montoAfianzado"));
			tempFianza.setMontoAfianzadoLetras(attributes.getValue("montoAfianzadoLetras"));
			tempFianza.setMontoAfianzadoMovimiento(attributes.getValue("montoAfianzadoMovimiento"));
			tempFianza.setMontoAfianzadoMovimientoLetra(attributes.getValue("montoAfianzadoMovimientoLetras"));
			tempFianza.setRamo(attributes.getValue("ramo"));
			tempFianza.setSubRamo(attributes.getValue("subRamo"));
			tempFianza.setTipo(attributes.getValue("tipo"));
			tempFianza.setMoneda(attributes.getValue("moneda"));
			tempFianza.setInicioVigenciaFianza(attributes.getValue("inicioVigenciaFianza"));
			tempFianza.setFinVigenciaFianza(attributes.getValue("finVigenciaFianza"));
			tempFianza.setFechaSolicitada(attributes.getValue("fechaSolicitada"));
			tempFianza.setFechaAutorizacion(attributes.getValue("fechaAutorizacion"));
			tempFianza.setDocumentoFuente(attributes.getValue("documentoFuente"));
		}
		if(qName.equalsIgnoreCase("Emisor")) {
			//Datos de la afianzadora
			tempFianza.setAfianzaRfc(formatRFC(attributes.getValue("rfc")));
			tempFianza.setAfianzaNombre(attributes.getValue("nombre"));
			
			domEmisor = true;
		}	
		if(qName.equalsIgnoreCase("DomicilioFiscal") && domEmisor) {
			//Datos del domicilio de la afianzadora
			tempFianza.setAfianzaCalle(attributes.getValue("calle"));
			tempFianza.setAfianzaNoExterior(attributes.getValue("noExterior"));
			tempFianza.setAfianzaNoInterior(attributes.getValue("noInterior"));
			tempFianza.setAfianzaColonia(attributes.getValue("colonia"));
			tempFianza.setAfianzaLocalidad(attributes.getValue("localidad"));
			tempFianza.setAfianzaReferencia(attributes.getValue("referencia"));
			tempFianza.setAfianzaMunicipio(attributes.getValue("municipio"));
			tempFianza.setAfianzaEstado(attributes.getValue("estado"));
			tempFianza.setAfianzaPais(attributes.getValue("pais"));
			tempFianza.setAfianzaCodigoPostal(attributes.getValue("codigoPostal"));
		}
		if(qName.equalsIgnoreCase("ExpedidoEn") && domEmisor) {
			tempFianza.setSucCalle(attributes.getValue("calle"));
			tempFianza.setSucNoExterior(attributes.getValue("noExterior"));
			tempFianza.setSucNoInterior(attributes.getValue("noInterior"));
			tempFianza.setSucColonia(attributes.getValue("colonia"));
			tempFianza.setSucLocalidad(attributes.getValue("localidad"));
			tempFianza.setSucReferencia(attributes.getValue("referencia"));
			tempFianza.setSucMunicipio(attributes.getValue("municipio"));
			tempFianza.setSucEstado(attributes.getValue("estado"));
			tempFianza.setSucPais(attributes.getValue("pais"));
			tempFianza.setSucCodigoPostal(attributes.getValue("codigoPostal"));
		}
		if(qName.equalsIgnoreCase("Fiado")) {
			//Datos del Fiado(Pyme)
			tempFianza.setFiadoRfc(formatRFC(attributes.getValue("rfc")));
			tempFianza.setFiadoNombre(attributes.getValue("nombre"));
			domFiado = true;
		}
		if(qName.equalsIgnoreCase("DomicilioFiscal") && domFiado) {
			//Datos del Fiado(Pyme)
			tempFianza.setFiadoCalle(attributes.getValue("calle"));
			tempFianza.setFiadoNoExterior(attributes.getValue("noExterior"));
			tempFianza.setFiadoNoInterior(attributes.getValue("noInterior"));
			tempFianza.setFiadoColonia(attributes.getValue("colonia"));
			tempFianza.setFiadoLocalidad(attributes.getValue("localidad"));
			tempFianza.setFiadoReferencia(attributes.getValue("referencia"));
			tempFianza.setFiadoMunicipio(attributes.getValue("municipio"));
			tempFianza.setFiadoEstado(attributes.getValue("estado"));
			tempFianza.setFiadoPais(attributes.getValue("pais"));
			tempFianza.setFiadoCodigoPostal(attributes.getValue("codigoPostal"));
		}
		if(qName.equalsIgnoreCase("Beneficiario")) {
			//Datos del beneficiario
			tempFianza.setBenefRfc(formatRFC(attributes.getValue("rfc")));
			tempFianza.setBenefNombre(attributes.getValue("nombre"));
		}
		if(qName.equalsIgnoreCase("Agente")) {
			//Datos del Agente Corredor
			tempFianza.setAgenteCoRfc(formatRFC(attributes.getValue("rfc")));
			tempFianza.setAgenteCoNombre(attributes.getValue("nombreAgenteCorredor"));
		}
		if(qName.equalsIgnoreCase("Primas")) {// faltan atributos en el xml
			//Datos de las primas de la fianza;
			tempFianza.setPrima(attributes.getValue("prima"));
			tempFianza.setBonificacion(attributes.getValue("bonificacion"));
			tempFianza.setDerechos(attributes.getValue("derechos"));
			tempFianza.setGastosExpedicion(attributes.getValue("gastosExpedicion"));
			tempFianza.setGastosInvestigacion(attributes.getValue("gastosInvestigacion"));
			tempFianza.setIva(attributes.getValue("iva"));
			tempFianza.setSubTotal(attributes.getValue("subTotal"));
			tempFianza.setPrimaTotal(attributes.getValue("primaTotal"));
		}
		
		if(qName.equalsIgnoreCase("Texto")) {
			textFianza = true;
		}
		if(textFianza && qName.equalsIgnoreCase("Linea")){
			String txt = tempFianza.getFianzaTexto()==null?"":tempFianza.getFianzaTexto()+attributes.getValue("texto");
			tempFianza.setFianzaTexto(txt);	
		}
		
		if(qName.equalsIgnoreCase("VersionImpresaNombre")) {//no esta en el xml
			//Datos de la persona que firma la version impresa
			tempFianza.setVerImpresaNombre(attributes.getValue("nombre"));
			tempFianza.setVerImpresaPuesto(attributes.getValue("puesto"));
		}
		
		if(qName.equalsIgnoreCase("Restricciones")) {
			textRestric = true;
		}
		if(textRestric && qName.equalsIgnoreCase("Linea")){
			String txt = tempFianza.getRestriccionTexto()==null?"":tempFianza.getRestriccionTexto()+attributes.getValue("texto");
			tempFianza.setRestriccionTexto(txt);	
		}
		
		if(qName.equalsIgnoreCase("NormatividadCNSF")) {
			textNormCNSF = true;
		}
		if(textNormCNSF && qName.equalsIgnoreCase("Linea")){
			String txt = tempFianza.getNormCNSFTexto()==null?"":tempFianza.getNormCNSFTexto()+attributes.getValue("texto");
			tempFianza.setNormCNSFTexto(txt);	
		}   
		
		if(qName.equalsIgnoreCase("AddendaBeneficiario")) {// no esta en el xml
			//Datos del domicilio fiscal del Beneficiario(EPO)
			tempFianza.setBenefnafinDestinatario(attributes.getValue("nafinDestinatario"));
			tempFianza.setBenefNombreUsuario(attributes.getValue("nombreUsuario"));
			tempFianza.setBenefCorreoUsuario(attributes.getValue("correoUsuario"));
			tempFianza.setBenefIdRamo(attributes.getValue("idRamo"));
			tempFianza.setBenefIdSubRamo(attributes.getValue("idSubRamo"));
		}
		if(qName.equalsIgnoreCase("AddendaFiado")) {//no esta en el xml
			//Datos adicionales del Fiado
			tempFianza.setNombreRepLegal(attributes.getValue("nombreRepLegal")); 
			tempFianza.setCorreoRepLegal(attributes.getValue("correoRepLegal"));
			tempFianza.setTelefonoRepLegal(attributes.getValue("telefonoRepLegal"));
			
			tempFianza.setVentasAnuales(attributes.getValue("ventasAnuales"));
			tempFianza.setNumeroEmpleados(attributes.getValue("numeroEmpleados"));
			tempFianza.setSector(attributes.getValue("sector"));
		}  
      if(qName.equalsIgnoreCase("AddendaAfianzadora")){ //FODEA 028-2012: se agrega el campo IdMovimientoFianza que sustituye a idMovimiento de FIANZA
         tempFianza.setIdMovimiento(attributes.getValue("IdMovimientoFianza"));
      }
		if(qName.equalsIgnoreCase("Seguridad")) {
			//Datos de seguridad para la carga de la Fianza
			tempFianza.setCertificado(attributes.getValue("certificado"));
			tempFianza.setSelloDigital(attributes.getValue("selloDigital"));
		}
		
		
	}


	public void characters(char[] ch, int start, int length) throws SAXException {
		tempVal = new String(ch,start,length);
		//System.out.println("tempVal characters = "+tempVal);
	}

	public void endElement(String uri, String localName,
		String qName) throws SAXException {

		if(qName.equalsIgnoreCase("Emisor")) {
			domEmisor = false;
			System.out.println("cierre de datos del emisor");
		}
		
		if(qName.equalsIgnoreCase("Fiado")) {
			domFiado = false;
			System.out.println("cierre de datos del fiado");
		}
		
		if(qName.equalsIgnoreCase("Texto")) { 
			textFianza = false;
		}
		
		if(qName.equalsIgnoreCase("Restricciones")) { 
			textRestric = false;
		}
		
		if(qName.equalsIgnoreCase("NormatividadCNSF")) { 
			textNormCNSF = false;
		}

	}
	
	private String formatRFC(String rfc){
		String tipoPersona = "";
		String rfcAux = rfc;
		
		if(rfc!=null){
			rfcAux = (rfc!=null)?rfc.replaceAll("[-]",""):rfc;
			if(rfcAux.matches("[A-Z]{4}[0-9]{6}[A-Z0-9]{3}")){//VALIDA PERSONA FISICA
				if(rfcAux.length()==13){
						String clvNombre = rfcAux.substring(0,4);
						String fecha = rfcAux.substring(4,10);
						String homoclave = rfcAux.substring(10,13);
						rfc = clvNombre+"-"+fecha+"-"+homoclave;
				}
			}
			else if(rfcAux.matches("[A-Z]{3}[0-9]{6}[A-Z0-9]{3}")){//VALIDA PERSONA MORAL
				if(rfcAux.length()==12){
						String clvNombre = rfcAux.substring(0,3);
						String fecha = rfcAux.substring(3,9);
						String homoclave = rfcAux.substring(9,12);
						rfc = clvNombre+"-"+fecha+"-"+homoclave;
				}
			}
		}

		return rfc;
	}


}