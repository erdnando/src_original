package com.netro.fianza.ws;

/**
 * La clase contiene la informaci�n arrojada por los procesos del 
 * Web Service de Carga de Fianzas WS
 * @author Fabian Valenzuela
 */
public class FianzaWSInfo implements java.io.Serializable  {
	private Integer codigoEjecucion;
	private String resumenEjecucion;
	private String[] xmls;

	public FianzaWSInfo() {
	}

	/**
	 * Obtiene el c�digo de ejecuci�n resultante.
	 * @return Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public Integer getCodigoEjecucion() {
		return codigoEjecucion;
	}

	/**
	 * Establece el c�digo de ejecuci�n resultante.
	 * @param codigoEjecucion Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public void setCodigoEjecucion(Integer codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}

	/**
	 * Obtiene el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de FianzasWS
	 * @return Cadena con el resumen o detalle de ejecuci�n
	 */
	public String getResumenEjecucion() {
		return resumenEjecucion;
	}

	/**
	 * Establece el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de FianzasWS
	 * @param resumenEjecucion Cadena con el resumen de ejecuci�n
	 */
	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}


	public void setXmls(String[] xmls) {
		this.xmls = xmls;
	}


	public String[] getXmls() {
		return xmls;
	}

}