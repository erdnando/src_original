package com.netro.fianza.ws;

/**
 * La clase contiene la informaci�n arrojada por los procesos del 
 * Web Service de Carga de Fianzas WS
 * @author Fabian Valenzuela
 */
public class ProcesoFianzaWSInfo implements java.io.Serializable  {
	private Integer codigoEjecucion;
	private String resumenEjecucion;

	public ProcesoFianzaWSInfo() {
	}

	/**
	 * Obtiene el c�digo de ejecuci�n resultante.
	 * @return Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public Integer getCodigoEjecucion() {
		return codigoEjecucion;
	}

	/**
	 * Establece el c�digo de ejecuci�n resultante.
	 * @param codigoEjecucion Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public void setCodigoEjecucion(Integer codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}

	/**
	 * Obtiene el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @return Cadena con el resumen o detalle de ejecuci�n
	 */
	public String getResumenEjecucion() {
		return resumenEjecucion;
	}

	/**
	 * Establece el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @param resumenEjecucion Cadena con el resumen de ejecuci�n
	 */
	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}

}