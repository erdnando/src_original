package com.netro.fianza;

public class DatosFianzaWS implements java.io.Serializable {
	
	//Datos Obtenidos
	private String icEpo;
	private String icPyme;
	private String ccEstatus;
	private String icAfianzadora;
	private String observaciones;
	
	//Datos de la Fianza
	private String versionEsquema;
	private String numeroCompaniaAfianzadora;
	private String serie;
	private String folio;	
	private String numeroFianza;
	private String numeroInclusion;
	private String numeroVersion;
	private String IdPoliza;
	private String IdMovimiento;
	private String IdSolicitud;
	private String movimiento;
	private String montoAfianzado;
	private String montoAfianzadoLetras;
	private String montoAfianzadoMovimiento;
	private String montoAfianzadoMovimientoLetra;
	private String ramo;
	private String subRamo;
	private String tipo;
	private String moneda;
	private String inicioVigenciaFianza;
	private String finVigenciaFianza;
	private String fechaSolicitada;
	private String fechaAutorizacion;
	private String documentoFuente;
	
	//Datos de la afianzadora
	private String afianzaRfc;
	private String afianzaNombre;
	
	//Datos del domicilio de la afianzadora
	private String afianzaCalle;
	private String afianzaNoExterior;
	private String afianzaNoInterior;
	private String afianzaColonia;
	private String afianzaLocalidad;
	private String afianzaReferencia;
	private String afianzaMunicipio;
	private String afianzaEstado;
	private String afianzaPais;
	private String afianzaCodigoPostal;
	
	//Datos de la Sucursal
	private String sucCalle;
	private String sucNoExterior;
	private String sucNoInterior;
	private String sucColonia;
	private String sucLocalidad;
	private String sucReferencia;
	private String sucMunicipio;
	private String sucEstado;
	private String sucPais;
	private String sucCodigoPostal;
	
	//Datos del Fiado(Pyme)
	private String fiadoRfc;
	private String fiadoNombre;
	//Datos del domicilio fiscal del Fiado(Pyme)
	private String fiadoCalle;
	private String fiadoNoExterior;
	private String fiadoNoInterior;
	private String fiadoColonia;
	private String fiadoLocalidad;
	private String fiadoReferencia;
	private String fiadoMunicipio;
	private String fiadoEstado;
	private String fiadoPais;
	private String fiadoCodigoPostal;
	
	//Datos del Beneficiario
	private String benefRfc;
	private String benefNombre;
	
	//Datos del Agente Corredor
	private String agenteCoRfc;
	private String agenteCoNombre;
	
	//Datos de las primas de la fianza;
	private String prima;
	private String bonificacion;
	private String derechos;
	private String gastosExpedicion;
	private String gastosInvestigacion;
	private String iva;
	private String subTotal;
	private String primaTotal;
	
	//PENDIENTE TEXTOS PARAM 71
	private String fianzaTexto;
	
	//Datos de la persona que firma la version impresa
	private String verImpresaNombre;
	private String verImpresaPuesto;
	
	//Datos del Complementode la Fianza
	private String restriccionTexto;
	private String normCNSFTexto;
	
	//Datos del domicilio fiscal del Beneficiario(EPO)
	private String benefnafinDestinatario;
	private String benefNombreUsuario;
	private String benefCorreoUsuario;
	private String benefIdRamo;
	private String benefIdSubRamo;
	
	//Datos adicionales del Fiado
	private String nombreRepLegal;
	private String correoRepLegal;
	private String telefonoRepLegal;
	private String ventasAnuales;
	private String numeroEmpleados;
	private String sector;
	
	//Datos de seguridad para la carga de la Fianza
	private String certificado;
	private String selloDigital;
	
   private byte[] cgXmlClob;
	private byte[] biPdfBlob;

	
	public DatosFianzaWS() {
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}


	public String getIcPyme() {
		return icPyme;
	}


	public void setCcEstatus(String ccEstatus) {
		this.ccEstatus = ccEstatus;
	}


	public String getCcEstatus() {
		return ccEstatus;
	}
	
	
	public void setIcAfianzadora(String icAfianzadora) {
		this.icAfianzadora = icAfianzadora;
	}


	public String getIcAfianzadora() {
		return icAfianzadora;
	}
	
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public String getObservaciones() {
		return observaciones;
	}
	
	
	public void setVersionEsquema(String versionEsquema) {
		this.versionEsquema = versionEsquema;
	}


	public String getVersionEsquema() {
		return versionEsquema;
	}


	public void setNumeroCompaniaAfianzadora(String numeroCompaniaAfianzadora) {
		this.numeroCompaniaAfianzadora = numeroCompaniaAfianzadora;
	}


	public String getNumeroCompaniaAfianzadora() {
		return numeroCompaniaAfianzadora;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public String getSerie() {
		return serie;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


	public void setNumeroFianza(String numeroFianza) {
		this.numeroFianza = numeroFianza;
	}


	public String getNumeroFianza() {
		return numeroFianza;
	}


	public void setNumeroInclusion(String numeroInclusion) {
		this.numeroInclusion = numeroInclusion;
	}


	public String getNumeroInclusion() {
		return numeroInclusion;
	}


	public void setNumeroVersion(String numeroVersion) {
		this.numeroVersion = numeroVersion;
	}


	public String getNumeroVersion() {
		return numeroVersion;
	}


	public void setIdPoliza(String IdPoliza) {
		this.IdPoliza = IdPoliza;
	}


	public String getIdPoliza() {
		return IdPoliza;
	}


	public void setIdMovimiento(String IdMovimiento) {
		this.IdMovimiento = IdMovimiento;
	}


	public String getIdMovimiento() {
		return IdMovimiento;
	}


	public void setIdSolicitud(String IdSolicitud) {
		this.IdSolicitud = IdSolicitud;
	}


	public String getIdSolicitud() {
		return IdSolicitud;
	}


	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}


	public String getMovimiento() {
		return movimiento;
	}


	public void setMontoAfianzado(String montoAfianzado) {
		this.montoAfianzado = montoAfianzado;
	}


	public String getMontoAfianzado() {
		return montoAfianzado;
	}


	public void setMontoAfianzadoLetras(String montoAfianzadoLetras) {
		this.montoAfianzadoLetras = montoAfianzadoLetras;
	}


	public String getMontoAfianzadoLetras() {
		return montoAfianzadoLetras;
	}


	public void setMontoAfianzadoMovimiento(String montoAfianzadoMovimiento) {
		this.montoAfianzadoMovimiento = montoAfianzadoMovimiento;
	}


	public String getMontoAfianzadoMovimiento() {
		return montoAfianzadoMovimiento;
	}


	public void setMontoAfianzadoMovimientoLetra(String montoAfianzadoMovimientoLetra) {
		this.montoAfianzadoMovimientoLetra = montoAfianzadoMovimientoLetra;
	}


	public String getMontoAfianzadoMovimientoLetra() {
		return montoAfianzadoMovimientoLetra;
	}


	public void setRamo(String ramo) {
		this.ramo = ramo;
	}


	public String getRamo() {
		return ramo;
	}


	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}


	public String getSubRamo() {
		return subRamo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getTipo() {
		return tipo;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setInicioVigenciaFianza(String inicioVigenciaFianza) {
		this.inicioVigenciaFianza = inicioVigenciaFianza;
	}


	public String getInicioVigenciaFianza() {
		return inicioVigenciaFianza;
	}


	public void setFinVigenciaFianza(String finVigenciaFianza) {
		this.finVigenciaFianza = finVigenciaFianza;
	}


	public String getFinVigenciaFianza() {
		return finVigenciaFianza;
	}


	public void setFechaSolicitada(String fechaSolicitada) {
		this.fechaSolicitada = fechaSolicitada;
	}


	public String getFechaSolicitada() {
		return fechaSolicitada;
	}


	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}


	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}


	public void setDocumentoFuente(String documentoFuente) {
		this.documentoFuente = documentoFuente;
	}


	public String getDocumentoFuente() {
		return documentoFuente;
	}


	public void setAfianzaRfc(String afianzaRfc) {
		this.afianzaRfc = afianzaRfc;
	}


	public String getAfianzaRfc() {
		return afianzaRfc;
	}


	public void setAfianzaNombre(String afianzaNombre) {
		this.afianzaNombre = afianzaNombre;
	}


	public String getAfianzaNombre() {
		return afianzaNombre;
	}


	public void setAfianzaCalle(String afianzaCalle) {
		this.afianzaCalle = afianzaCalle;
	}


	public String getAfianzaCalle() {
		return afianzaCalle;
	}


	public void setAfianzaNoExterior(String afianzaNoExterior) {
		this.afianzaNoExterior = afianzaNoExterior;
	}


	public String getAfianzaNoExterior() {
		return afianzaNoExterior;
	}


	public void setAfianzaNoInterior(String afianzaNoInterior) {
		this.afianzaNoInterior = afianzaNoInterior;
	}


	public String getAfianzaNoInterior() {
		return afianzaNoInterior;
	}


	public void setAfianzaColonia(String afianzaColonia) {
		this.afianzaColonia = afianzaColonia;
	}


	public String getAfianzaColonia() {
		return afianzaColonia;
	}


	public void setAfianzaLocalidad(String afianzaLocalidad) {
		this.afianzaLocalidad = afianzaLocalidad;
	}


	public String getAfianzaLocalidad() {
		return afianzaLocalidad;
	}


	public void setAfianzaReferencia(String afianzaReferencia) {
		this.afianzaReferencia = afianzaReferencia;
	}


	public String getAfianzaReferencia() {
		return afianzaReferencia;
	}


	public void setAfianzaMunicipio(String afianzaMunicipio) {
		this.afianzaMunicipio = afianzaMunicipio;
	}


	public String getAfianzaMunicipio() {
		return afianzaMunicipio;
	}


	public void setAfianzaEstado(String afianzaEstado) {
		this.afianzaEstado = afianzaEstado;
	}


	public String getAfianzaEstado() {
		return afianzaEstado;
	}


	public void setAfianzaPais(String afianzaPais) {
		this.afianzaPais = afianzaPais;
	}


	public String getAfianzaPais() {
		return afianzaPais;
	}


	public void setAfianzaCodigoPostal(String afianzaCodigoPostal) {
		this.afianzaCodigoPostal = afianzaCodigoPostal;
	}


	public String getAfianzaCodigoPostal() {
		return afianzaCodigoPostal;
	}


	public void setSucCalle(String sucCalle) {
		this.sucCalle = sucCalle;
	}


	public String getSucCalle() {
		return sucCalle;
	}


	public void setSucNoExterior(String sucNoExterior) {
		this.sucNoExterior = sucNoExterior;
	}


	public String getSucNoExterior() {
		return sucNoExterior;
	}


	public void setSucNoInterior(String sucNoInterior) {
		this.sucNoInterior = sucNoInterior;
	}


	public String getSucNoInterior() {
		return sucNoInterior;
	}


	public void setSucColonia(String sucColonia) {
		this.sucColonia = sucColonia;
	}


	public String getSucColonia() {
		return sucColonia;
	}


	public void setSucLocalidad(String sucLocalidad) {
		this.sucLocalidad = sucLocalidad;
	}


	public String getSucLocalidad() {
		return sucLocalidad;
	}


	public void setSucReferencia(String sucReferencia) {
		this.sucReferencia = sucReferencia;
	}


	public String getSucReferencia() {
		return sucReferencia;
	}


	public void setSucMunicipio(String sucMunicipio) {
		this.sucMunicipio = sucMunicipio;
	}


	public String getSucMunicipio() {
		return sucMunicipio;
	}


	public void setSucEstado(String sucEstado) {
		this.sucEstado = sucEstado;
	}


	public String getSucEstado() {
		return sucEstado;
	}


	public void setSucPais(String sucPais) {
		this.sucPais = sucPais;
	}


	public String getSucPais() {
		return sucPais;
	}


	public void setSucCodigoPostal(String sucCodigoPostal) {
		this.sucCodigoPostal = sucCodigoPostal;
	}


	public String getSucCodigoPostal() {
		return sucCodigoPostal;
	}


	public void setFiadoRfc(String fiadoRfc) {
		this.fiadoRfc = fiadoRfc;
	}


	public String getFiadoRfc() {
		return fiadoRfc;
	}


	public void setFiadoNombre(String fiadoNombre) {
		this.fiadoNombre = fiadoNombre;
	}


	public String getFiadoNombre() {
		return fiadoNombre;
	}


	public void setFiadoCalle(String fiadoCalle) {
		this.fiadoCalle = fiadoCalle;
	}


	public String getFiadoCalle() {
		return fiadoCalle;
	}


	public void setFiadoNoExterior(String fiadoNoExterior) {
		this.fiadoNoExterior = fiadoNoExterior;
	}


	public String getFiadoNoExterior() {
		return fiadoNoExterior;
	}


	public void setFiadoNoInterior(String fiadoNoInterior) {
		this.fiadoNoInterior = fiadoNoInterior;
	}


	public String getFiadoNoInterior() {
		return fiadoNoInterior;
	}


	public void setFiadoColonia(String fiadoColonia) {
		this.fiadoColonia = fiadoColonia;
	}


	public String getFiadoColonia() {
		return fiadoColonia;
	}
	
	public void setFiadoLocalidad(String fiadoLocalidad) {
		this.fiadoLocalidad = fiadoLocalidad;
	}


	public String getFiadoLocalidad() {
		return fiadoLocalidad;
	}


	public void setFiadoReferencia(String fiadoReferencia) {
		this.fiadoReferencia = fiadoReferencia;
	}


	public String getFiadoReferencia() {
		return fiadoReferencia;
	}


	public void setFiadoMunicipio(String fiadoMunicipio) {
		this.fiadoMunicipio = fiadoMunicipio;
	}


	public String getFiadoMunicipio() {
		return fiadoMunicipio;
	}


	public void setFiadoEstado(String fiadoEstado) {
		this.fiadoEstado = fiadoEstado;
	}


	public String getFiadoEstado() {
		return fiadoEstado;
	}


	public void setFiadoPais(String fiadoPais) {
		this.fiadoPais = fiadoPais;
	}


	public String getFiadoPais() {
		return fiadoPais;
	}


	public void setFiadoCodigoPostal(String fiadoCodigoPostal) {
		this.fiadoCodigoPostal = fiadoCodigoPostal;
	}


	public String getFiadoCodigoPostal() {
		return fiadoCodigoPostal;
	}


	public void setBenefRfc(String benefRfc) {
		this.benefRfc = benefRfc;
	}


	public String getBenefRfc() {
		return benefRfc;
	}


	public void setBenefNombre(String benefNombre) {
		this.benefNombre = benefNombre;
	}


	public String getBenefNombre() {
		return benefNombre;
	}


	public void setAgenteCoRfc(String agenteCoRfc) {
		this.agenteCoRfc = agenteCoRfc;
	}


	public String getAgenteCoRfc() {
		return agenteCoRfc;
	}


	public void setAgenteCoNombre(String agenteCoNombre) {
		this.agenteCoNombre = agenteCoNombre;
	}


	public String getAgenteCoNombre() {
		return agenteCoNombre;
	}


	public void setPrima(String prima) {
		this.prima = prima;
	}


	public String getPrima() {
		return prima;
	}


	public void setBonificacion(String bonificacion) {
		this.bonificacion = bonificacion;
	}


	public String getBonificacion() {
		return bonificacion;
	}


	public void setDerechos(String derechos) {
		this.derechos = derechos;
	}


	public String getDerechos() {
		return derechos;
	}


	public void setGastosExpedicion(String gastosExpedicion) {
		this.gastosExpedicion = gastosExpedicion;
	}


	public String getGastosExpedicion() {
		return gastosExpedicion;
	}


	public void setGastosInvestigacion(String gastosInvestigacion) {
		this.gastosInvestigacion = gastosInvestigacion;
	}


	public String getGastosInvestigacion() {
		return gastosInvestigacion;
	}


	public void setIva(String iva) {
		this.iva = iva;
	}


	public String getIva() {
		return iva;
	}


	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}


	public String getSubTotal() {
		return subTotal;
	}


	public void setPrimaTotal(String primaTotal) {
		this.primaTotal = primaTotal;
	}


	public String getPrimaTotal() {
		return primaTotal;
	}


	public void setFianzaTexto(String fianzaTexto) {
		this.fianzaTexto = fianzaTexto;
	}


	public String getFianzaTexto() {
		return fianzaTexto;
	}


	public void setVerImpresaNombre(String verImpresaNombre) {
		this.verImpresaNombre = verImpresaNombre;
	}


	public String getVerImpresaNombre() {
		return verImpresaNombre;
	}


	public void setVerImpresaPuesto(String verImpresaPuesto) {
		this.verImpresaPuesto = verImpresaPuesto;
	}


	public String getVerImpresaPuesto() {
		return verImpresaPuesto;
	}


	public void setRestriccionTexto(String restriccionTexto) {
		this.restriccionTexto = restriccionTexto;
	}


	public String getRestriccionTexto() {
		return restriccionTexto;
	}


	public void setNormCNSFTexto(String normCNSFTexto) {
		this.normCNSFTexto = normCNSFTexto;
	}


	public String getNormCNSFTexto() {
		return normCNSFTexto;
	}


	public void setBenefnafinDestinatario(String benefnafinDestinatario) {
		this.benefnafinDestinatario = benefnafinDestinatario;
	}


	public String getBenefnafinDestinatario() {
		return benefnafinDestinatario;
	}


	public void setBenefNombreUsuario(String benefNombreUsuario) {
		this.benefNombreUsuario = benefNombreUsuario;
	}


	public String getBenefNombreUsuario() {
		return benefNombreUsuario;
	}


	public void setBenefCorreoUsuario(String benefCorreoUsuario) {
		this.benefCorreoUsuario = benefCorreoUsuario;
	}


	public String getBenefCorreoUsuario() {
		return benefCorreoUsuario;
	}


	public void setBenefIdRamo(String benefIdRamo) {
		this.benefIdRamo = benefIdRamo;
	}


	public String getBenefIdRamo() {
		return benefIdRamo;
	}


	public void setBenefIdSubRamo(String benefIdSubRamo) {
		this.benefIdSubRamo = benefIdSubRamo;
	}


	public String getBenefIdSubRamo() {
		return benefIdSubRamo;
	}


	public void setNombreRepLegal(String nombreRepLegal) {
		this.nombreRepLegal = nombreRepLegal;
	}


	public String getNombreRepLegal() {
		return nombreRepLegal;
	}


	public void setCorreoRepLegal(String correoRepLegal) {
		this.correoRepLegal = correoRepLegal;
	}


	public String getCorreoRepLegal() {
		return correoRepLegal;
	}


	public void setTelefonoRepLegal(String telefonoRepLegal) {
		this.telefonoRepLegal = telefonoRepLegal;
	}


	public String getTelefonoRepLegal() {
		return telefonoRepLegal;
	}
	
	
	public void setVentasAnuales(String ventasAnuales) {
		this.ventasAnuales = ventasAnuales;
	}


	public String getVentasAnuales() {
		return ventasAnuales;
	}


	public void setNumeroEmpleados(String numeroEmpleados) {
		this.numeroEmpleados = numeroEmpleados;
	}


	public String getNumeroEmpleados() {
		return numeroEmpleados;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public String getSector() {
		return sector;
	}


	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}


	public String getCertificado() {
		return certificado;
	}


	public void setSelloDigital(String selloDigital) {
		this.selloDigital = selloDigital;
	}


	public String getSelloDigital() {
		return selloDigital;
	}


	public void setCgXmlClob(byte[] cgXmlClob) {
		this.cgXmlClob = cgXmlClob;
	}


	public byte[] getCgXmlClob() {
		return cgXmlClob;
	}


	public void setBiPdfBlob(byte[] biPdfBlob) {
		this.biPdfBlob = biPdfBlob;
	}


	public byte[] getBiPdfBlob() {
		return biPdfBlob;
	}

	
}