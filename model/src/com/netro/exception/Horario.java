package com.netro.exception;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Calendar;
import java.util.Vector;

import netropology.utilerias.AccesoDB;

/**
 * Clase de validacion de horarios
 */
public class Horario
{
	public static final int PRODUCTO_DSCTO = 1;
	public static final int PRODUCTO_ANTIC = 2;
	
	public static void validarDia(String strTipoUsuario, String lsEpo) throws NafinException, Exception	{
		String qry_dia_inhabil = "";
		String qry_dia_inhabil_x_anio = "";
		AccesoDB conexion = new AccesoDB();
		Calendar cal = Calendar.getInstance();
		PreparedStatement pstmt = null;
		ResultSet rs_dia_inhabil = null;
		long numero1;
		try	{
			conexion.conexionDB();

			qry_dia_inhabil_x_anio  = "select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comcat_dia_inhabil where df_dia_inhabil = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') UNION ";
			qry_dia_inhabil_x_anio += " select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comrel_dia_inhabil_x_epo where df_dia_inhabil = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') and ic_epo = ? ";
			
			qry_dia_inhabil  = "select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			qry_dia_inhabil += " select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = ? ";
			//qry_dia_inhabil  = "select count(*) as diaInhabil from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			//qry_dia_inhabil += " select count(*) as diaInhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = " + lsEpo;

			pstmt = conexion.queryPrecompilado(qry_dia_inhabil_x_anio);
			numero1=Long.parseLong(lsEpo);
			pstmt.setLong(1,numero1);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0 && (strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN"))) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					throw new NafinException("DSCT0046");
				}
			}
			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry_dia_inhabil_x_anio);
			
			pstmt = conexion.queryPrecompilado(qry_dia_inhabil);
			numero1=Long.parseLong(lsEpo);
			pstmt.setLong(1,numero1);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0 && (strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN"))) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					throw new NafinException("DSCT0046");
				}
			}
			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry_dia_inhabil);

			int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
			if (diaSemana == 7 || diaSemana == 1) {
				// Verifica que no sea s�bado o domingo
				System.out.println("Lo sentimos. Hoy es un d�a inhabil S-D");
				throw new NafinException("DSCT0046");
			}

		}
		catch (NafinException ne)	{
			System.out.println("HorarioException: "+ne);
			throw ne;
		}
		catch (Exception e)	{
			System.out.println("Exception: "+e);
			throw e;
		}
		finally	{
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
		}

	}



	public static void validarHorario(int numero, String strTipoUsuario, String lsEpo)
		throws NafinException, Exception
	{
		validarDia(strTipoUsuario, lsEpo);

		String lsCampos = "", lsCampoCierre = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		System.out.println("\n\n\n La hora es : " + liHora + ":" + liMinutos + "  \n\n\n");

		ResultSet rsHorario;
 		//String  lsQuery = "SELECT cg_horario_inicio, cg_horario_fin FROM comrel_horario_x_epo WHERE ic_producto_nafin = "+numero+" AND ic_epo = " + lsEpo,
 		String  lsQuery = "",
				lsHorarioIni = "",
				lsHorarioFin = "",
				lsCierre     = "";

		AccesoDB conexion = new AccesoDB();
		try	{
			conexion.conexionDB();
			//rsHorario = conexion.queryDB(lsQuery);
			PreparedStatement pstmt = conexion.queryPrecompilado("SELECT cg_horario_inicio, cg_horario_fin  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY FROM comrel_horario_x_epo WHERE ic_producto_nafin = ? AND ic_epo = ?");
			pstmt.setInt(1,numero);
			long numero1=Long.parseLong(lsEpo);
			pstmt.setLong(2,numero1);
			rsHorario = pstmt.executeQuery();
			pstmt.clearParameters();



			if(rsHorario.next())	{
				lsHorarioIni = rsHorario.getString(1);
				lsHorarioFin = rsHorario.getString(2);

				System.out.print("lsHorarioIni: " + lsHorarioIni + "\n");
				System.out.print("lsHorarioFin: " + lsHorarioFin + "\n");
				
				lsQuery = "SELECT UPPER(cs_cierre_servicio)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM COMCAT_PRODUCTO_NAFIN WHERE ic_producto_nafin = ? ";
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();

				if(rsHorario.next())
					lsCierre     = rsHorario.getString(1);
			}
			else	{
				//lsQuery = "SELECT UPPER(cs_cierre_servicio), cg_horario_inicio, cg_horario_fin FROM comcat_producto_nafin WHERE ic_producto_nafin = " + numero;
				lsQuery = "SELECT UPPER(cs_cierre_servicio), cg_horario_inicio, cg_horario_fin  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin WHERE ic_producto_nafin = ? ";
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();

				if(rsHorario.next())	{
					lsHorarioIni = rsHorario.getString(2);
					lsHorarioFin = rsHorario.getString(3);
					lsCierre     = rsHorario.getString(1);

					if ( strTipoUsuario.equals("IF") )	{	// SE ACTUALIZA LA TABLA comrel_horaio_x_epo SI NO TIENE HORARIO PARAMETRIZADO
						//lsQuery = "SELECT cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if FROM comcat_producto_nafin  WHERE ic_producto_nafin = " + numero;
						lsQuery = "SELECT cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin  WHERE ic_producto_nafin = ? ";
						pstmt = conexion.queryPrecompilado( lsQuery );
						pstmt.setInt(1,numero);
						ResultSet rs = pstmt.executeQuery();
						pstmt.clearParameters();

						if ( rs.next() )	{
							lsQuery = "INSERT INTO comrel_horario_x_epo " +
								"	(ic_producto_nafin, ic_epo, cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if) " +
								"VALUES " +
								"	("+numero+",'"+lsEpo+"', '"+ rs.getString("cg_horario_inicio") +"', '"+rs.getString("cg_horario_fin")+"', '"+rs.getString("cg_horario_inicio_if")+"', '"+rs.getString("cg_horario_fin_if")+"')";
							conexion.ejecutaSQL(lsQuery);
		    				conexion.terminaTransaccion(true);
						}
						rs.close();
						if(pstmt != null) pstmt.close();
						System.out.println("proceso query(BIND):"+lsQuery);
					}

					System.out.print("lsHorarioIni: " + lsHorarioIni + "\n");
					System.out.print("lsHorarioFin: " + lsHorarioFin + "\n");
				}
				rsHorario.close();
				if(pstmt != null) pstmt.close();
				System.out.println("proceso query(BIND):"+lsQuery);
			}
			conexion.cierraConexionDB();




			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				boolean lbError = false;
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp)	lbError = true;
				if (liHora == liHoraIniCmp)	if (liMinutos < liMinutoIniCmp)	lbError = true;

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				if (liHora > liHoraFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				System.out.println("Horario valido = " + !lbError);


				if (lbError && (strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN")))	{
					System.out.println("Checando Pyme NUEVO");
					throw new NafinException("DSCT0048");
				}
				else	{
					/**** LA BANDERA NO SE VALIDA CON IF, SOLO CON PYME  ****/
					if ( strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN") )	{
						if ( lsCierre.equals("S") )	{
							System.out.print("cs_cierre_servicio = (" + lsCierre + ")\n");
							throw new NafinException("DSCT0049");
						}
					}
				}
			}

		}
		catch (NafinException ne)	{
			System.out.println("HorarioException: "+ne);
			throw ne;
		}
		catch (Exception e)	{
			System.out.println("Exception: "+e);
			throw e;
		}
		finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraStatement();
				conexion.cierraConexionDB();
			}
		}
	}




	/**
	 * Valida el horario basandose en ???
	 * @param numero ???
	 * @param strTipoUsuario ???
	 * @param lsEpo Clave de la Epo
	 * @param lsIf ??? No se usa
	 * @throws com.netro.exception.NafinException
	 */
	public static void validarHorario(int numero, String strTipoUsuario, 
			String lsEpo, String lsIf) throws NafinException {
		System.out.println("Horario.validarHorario(E):IF");

		String lsCampos = "", lsCampoCierre = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		System.out.println("Horario.validarHorario():IF:La hora es : " + liHora + ":" + liMinutos +"\n");

		ResultSet rsHorario;
		String    lsQuery = "SELECT cg_horario_inicio_if, cg_horario_fin_if  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comrel_horario_x_epo WHERE ic_producto_nafin = ? AND ic_epo = ? " ,
				lsHorarioIni = "",
				lsHorarioFin = "",
				lsCierre     = "";

		AccesoDB conexion = new AccesoDB();
		try	{
			validarDia(strTipoUsuario, lsEpo);
			conexion.conexionDB();
			PreparedStatement pstmt = conexion.queryPrecompilado(lsQuery);
			pstmt.setInt(1,numero);
			long numero1=Long.parseLong(lsEpo);
			pstmt.setLong(2,numero1);
			rsHorario = pstmt.executeQuery();
			pstmt.clearParameters();

			if(rsHorario.next())	{
				lsHorarioIni = rsHorario.getString("CG_HORARIO_INICIO_IF");
				lsHorarioFin = rsHorario.getString("CG_HORARIO_FIN_IF");

				System.out.print("Horario.validarHorario():IF:lsHorarioIni: " + lsHorarioIni +"\n");
				System.out.print("Horario.validarHorario():IF:lsHorarioFin: " + lsHorarioFin +"\n");

			} else {
				System.out.print("Horario.validarHorario():IF: Sin horario x EPO \n");
				lsQuery = "SELECT cg_horario_inicio_if, cg_horario_fin_if, cg_horario_inicio, cg_horario_fin, 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin WHERE ic_producto_nafin = ? " ;
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();
				if(rsHorario.next()) {
					lsHorarioIni = rsHorario.getString("CG_HORARIO_INICIO_IF");
					lsHorarioFin = rsHorario.getString("CG_HORARIO_FIN_IF");

					// SE ACTUALIZA LA TABLA comrel_horario_x_epo SI NO TIENE HORARIO PARAMETRIZADO
					lsQuery = "INSERT INTO comrel_horario_x_epo " +
							"	(ic_producto_nafin, ic_epo, cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if) " +
							" VALUES " +
							"	("+numero+",'"+lsEpo+"', '"+ rsHorario.getString("CG_HORARIO_INICIO") +"', '"+rsHorario.getString("CG_HORARIO_FIN")+"', '"+lsHorarioIni+"', '"+lsHorarioFin+"')";
					conexion.ejecutaSQL(lsQuery);
		    		conexion.terminaTransaccion(true);
					System.out.print("Horario.validarHorario():IF:lsHorarioIni: " + lsHorarioIni +"\n");
					System.out.print("Horario.validarHorario():IF:lsHorarioFin: " + lsHorarioFin +"\n");
				}
			}
			rsHorario.close();
			if(pstmt != null) {
				pstmt.close();
			}


			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Horario.validarHorario():IF:Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Horario.validarHorario():IF:Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp)	 {
					throw new NafinException("DSCT0048");
				}
				if (liHora == liHoraIniCmp)
					if (liMinutos < liMinutoIniCmp)
						throw new NafinException("DSCT0048");

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbTiempoTranscurrido = true;
				}
				if (liHora > liHoraFinCmp)	{
					lbTiempoTranscurrido = true;
				}
				//System.out.print("Horario valido = " + !lbError);

				// CHECANDO DOCUMENTOS SI ES USUARIO IF
				if (lbTiempoTranscurrido && strTipoUsuario.equals("IF"))	{
					if (numero == 1)
						lsQuery = "SELECT /*+index (com_documento in_com_documento_04_nuk)*/  COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM com_documento WHERE ic_estatus_docto = 3 AND ic_epo = ? ";
					else if (numero == 2)
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM com_pedido WHERE ic_estatus_pedido = 3 AND ic_epo = ? ";
					else if (numero == 4)
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM dis_documento WHERE ic_estatus_docto = 3 AND ic_epo = ? ";
					else if (numero == 5){
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM inv_disposicion WHERE ic_estatus_disposicion = 1 AND ic_epo = ? ";
					}

					pstmt = conexion.queryPrecompilado( lsQuery );
					pstmt.setString(1, lsEpo);
					ResultSet rsEstatus = pstmt.executeQuery();
					pstmt.clearParameters();

					if(rsEstatus.next())	{
						if( rsEstatus.getInt(1) != 0 )	{
							System.out.println("Horario.validarHorario():IF:Hay documentos pendientes");
						} else {
							throw new NafinException("DSCT0048");
						}
					}
					rsEstatus.close();
					if(pstmt != null) pstmt.close();
				}
			}
		}
		catch (NafinException ne)	{
			System.out.println("Horario.validarHorario(HorarioException): "+ne.getMsgError());
			throw ne;
		}
		catch (Exception e)	{
			System.out.println("Horario.validarHorario(Exception): "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
			System.out.println("Horario.validarHorario(S):IF");
		}
	}




	public static void validarHorarioA(int productoNafin, String strTipoUsuario, String epo)
		throws NafinException, Exception
	{
		String qry_dia_inhabil = "";
		ResultSet rsHorario;
		String querySesion = "";
		String servicioPyme = "";
		String servicioIf = "";

		AccesoDB conexion = new AccesoDB();
		try
		{
			conexion.conexionDB();

			/*qry_dia_inhabil  = "select count(*) as diaInhabil from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			qry_dia_inhabil += " select count(*) as diaInhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo =" + epo;
			ResultSet rs_dia_inhabil = conexion.queryDB(qry_dia_inhabil);
            */

			qry_dia_inhabil  = "select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarHorarioA()' as ORIGENQUERY from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			qry_dia_inhabil += " select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarHorarioA()' as ORIGENQUERY from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = ? ";
			//qry_dia_inhabil  = "select count(*) as diaInhabil from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			//qry_dia_inhabil += " select count(*) as diaInhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = " + lsEpo;

			PreparedStatement pstmt = conexion.queryPrecompilado(qry_dia_inhabil);
			long numero1=Long.parseLong(epo);
			pstmt.setLong(1,numero1);
			ResultSet rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();



			while(rs_dia_inhabil.next()){
				if(rs_dia_inhabil.getInt(1) != 0 && strTipoUsuario.equals("PYME")) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					throw new NafinException("DSCT0046");
				}
			}
			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry_dia_inhabil);

			querySesion  = "SELECT " +
				" UPPER(C.cs_cierre_servicio), UPPER(C.cs_cierre_servicio_if) "+
				" FROM COMCAT_PRODUCTO_NAFIN C WHERE C.ic_producto_nafin = " + productoNafin;
				//System.out.println(querySesion);
			rsHorario = conexion.queryDB(querySesion);
			if(rsHorario.next())
			{
				// Se asigna el horario valido
				servicioPyme = rsHorario.getString(1);
				servicioIf = rsHorario.getString(2);
				Calendar fecha = Calendar.getInstance();

				int dia = fecha.get(Calendar.DAY_OF_WEEK);
				if (dia == Calendar.SUNDAY || dia == Calendar.SATURDAY)
				{
					// Es d�a que no hay labores
					if (productoNafin == PRODUCTO_DSCTO) {
						throw new NafinException("DSCT0001");
					} else if (productoNafin == PRODUCTO_ANTIC) {
						throw new NafinException("ANTI0001");
					} else {
						throw new NafinException("GRAL0001");
					}
				}

				//Validamos que no se encuentre cerrado el servicio
				if (strTipoUsuario.equals("PYME")) {
					if (servicioPyme.equals("S")) {
						if (productoNafin == PRODUCTO_DSCTO) {
							System.out.print("Servicio Pyme Cerrado");
							throw new NafinException("DSCT0002");
						} else if (productoNafin == PRODUCTO_ANTIC) {
							throw new NafinException("ANTI0002");
						} else {
							throw new NafinException("GRAL0002");
						}
					}
				} else if (strTipoUsuario.equals("IF")) {
					if (servicioIf.equals("S")) {
						if (productoNafin == PRODUCTO_DSCTO) {
							throw new NafinException("DSCT0003");
						} else if (productoNafin == PRODUCTO_ANTIC) {
							throw new NafinException("ANTI0003");
						} else {
							throw new NafinException("GRAL0003");
						}
					}
				}
			}

		} catch (NafinException ne) {
			System.out.println("HorarioException: "+ne);
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception: "+e);
			throw e;
		} finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraStatement();
				conexion.cierraConexionDB();
			}
		}
	}

	public static void validarHorario(int productoNafin, String strTipoUsuario)
			throws NafinException, Exception
	{
		ResultSet rsHorario;
		String querySesion = "";
		String servicioPyme = "";
		String servicioIf = "";

		AccesoDB conexion = new AccesoDB();
		try {
			conexion.conexionDB();
			querySesion  = "SELECT " +
				" UPPER(C.cs_cierre_servicio), UPPER(C.cs_cierre_servicio_if) "+
				" FROM COMCAT_PRODUCTO_NAFIN C WHERE C.ic_producto_nafin = " + productoNafin;
//System.out.println(querySesion);

			rsHorario = conexion.queryDB(querySesion);

			if(rsHorario.next())
			{
				// Se asigna el horario valido
				servicioPyme = rsHorario.getString(1);
				servicioIf = rsHorario.getString(2);
				Calendar fecha = Calendar.getInstance();

				int dia = fecha.get(Calendar.DAY_OF_WEEK);
				if (dia == Calendar.SUNDAY || dia == Calendar.SATURDAY)
				{
					// Es d�a que no hay labores
					if (productoNafin == PRODUCTO_DSCTO) {
						throw new NafinException("DSCT0001");
					} else if (productoNafin == PRODUCTO_ANTIC) {
						throw new NafinException("ANTI0001");
					} else {
						throw new NafinException("GRAL0001");
					}
				}

				//Validamos que no se encuentre cerrado el servicio
				if (strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN")) {
					if (servicioPyme.equals("S")) {
						if (productoNafin == PRODUCTO_DSCTO) {
							System.out.print("Servicio Pyme Cerrado");
							throw new NafinException("DSCT0049");
						} else if (productoNafin == PRODUCTO_ANTIC) {
							throw new NafinException("ANTI0002");
						} else {
							throw new NafinException("GRAL0002");
						}
					}
				} else if (strTipoUsuario.equals("IF")) {
					if (servicioIf.equals("S")) {
						if (productoNafin == PRODUCTO_DSCTO) {
							throw new NafinException("DSCT0003");
						} else if (productoNafin == PRODUCTO_ANTIC) {
							throw new NafinException("ANTI0003");
						} else {
							throw new NafinException("GRAL0003");
						}
					}
				}
			}

		} catch (NafinException ne) {
			System.out.println("HorarioException: "+ne);
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception: "+e);
			throw e;
		} finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
		}
	}

	/* Valida el Horario para la dispersi�n diaria, creado por HDG.	*/
	public static void validarHorarioDispersion(int iProductoNafin, String lsEpo, String lsIf)
												throws NafinException, Exception {
	AccesoDB conexion = new AccesoDB();
	String qry = "";
	ResultSet rs = null;
		try {
			conexion.conexionDB();

			qry = "select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarHorarioDispersion()' as ORIGENQUERY  from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION "+
				" select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarHorarioDispersion()' as ORIGENQUERY  from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = ? ";
			//qry_dia_inhabil  = "select count(*) as diaInhabil from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') UNION ";
			//qry_dia_inhabil += " select count(*) as diaInhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm') and ic_epo = " + lsEpo;

			PreparedStatement pstmt = conexion.queryPrecompilado(qry);
			long numero1=Long.parseLong(lsEpo);
			pstmt.setLong(1,numero1);
			rs = pstmt.executeQuery();
			while(rs.next()){
				if(rs.getInt(1) != 0) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					throw new NafinException("DSCT0046");
				}
			}
			rs.close();
			pstmt.clearParameters();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry);

			qry = "SELECT UPPER(cs_cierre_servicio) FROM comcat_producto_nafin "+
				" WHERE ic_producto_nafin = " + iProductoNafin;
			rs = conexion.queryDB(qry);
			if(rs.next()) {
				// Validamos que no se encuentre cerrado el servicio
				if (rs.getString(1).equals("N")) {
					if (iProductoNafin == PRODUCTO_DSCTO) {
						System.out.print("Servicio Pyme Cerrado");
						throw new NafinException("DSCT0002");
					}
				}
			}
			conexion.cierraStatement();

			// Validaci�n del N�mero de Documentos estatus seleccionada pyme, seleccionada if y en proceso.
			if (iProductoNafin == 1) {
				qry = "select a.tot_doc + b.tot_solic from "+
					" (select count(ic_documento) as tot_doc FROM com_documento WHERE ic_estatus_docto = 3 AND ic_epo = "+lsEpo+") a, "+
					" (select count(S.ic_folio) as tot_solic from com_documento D, com_docto_seleccionado DS, com_solicitud S "+
					" where D.ic_documento = DS.ic_documento "+
					" and DS.ic_documento = S.ic_documento "+
					" and D.ic_epo = DS.ic_epo "+
					" and S.ic_estatus_solic in (1,2) "+
					" and trunc(S.df_operacion) = trunc(SYSDATE) "+
					" and DS.ic_epo = "+lsEpo+
					" and DS.ic_if = "+lsIf+") b";
				rs = conexion.queryDB(qry);
				if(rs.next()) {
					if(rs.getInt(1) != 0 )	{
						throw new NafinException("DSCT0048");
					}
				}
				conexion.cierraStatement();
			}

		} catch (NafinException ne) {
			System.out.println("HorarioException: "+ne);
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception: "+e);
			throw e;
		} finally {
			if (conexion.hayConexionAbierta()) conexion.cierraConexionDB();
		}
	} // validarHorarioDispersion


 	public static Vector ovgetHorarioServicio(String lsProducto, String lsTipoUsuario, String lsiNoCliente)
		throws NafinException {
		System.out.println("Horario.ovgetHorarioServicio(E)");
		AccesoDB lodbConexion = new AccesoDB();
		Vector lovHorario = new Vector();
		ResultSet rsHorario = null;
		PreparedStatement pstmtHorario = null;
		try {
				lodbConexion.conexionDB();
			StringBuffer lsQryHorario = new StringBuffer();
			if ("NAFIN".equals(lsTipoUsuario)) {
				lsQryHorario.append("select cg_horario_inicio_nafin as horario_inicio, cg_horario_fin_nafin  as horario_fin from comcat_producto_nafin where ic_producto_nafin = ? " );
				pstmtHorario = lodbConexion.queryPrecompilado(lsQryHorario.toString());
				pstmtHorario.setString(1, lsProducto);
			} else if ("IF".equals(lsTipoUsuario)) {
				lsQryHorario.append("select cg_horario_inicio_if as horario_inicio, cg_horario_fin_if as horario_fin from comcat_producto_nafin where ic_producto_nafin = ? " );
				pstmtHorario = lodbConexion.queryPrecompilado(lsQryHorario.toString());
					pstmtHorario.setString(1, lsProducto);
			} else if ("PYME".equals(lsTipoUsuario) || "EPO".equals(lsTipoUsuario)) {
				//lsQryHorario.append("select cg_horario_inicio as horario_inicio, cg_horario_fin as horario_fin from comrel_horario_x_epo where ic_producto_nafin = ? and ic_epo = ? union ALL ");
				lsQryHorario.append("select cg_horario_inicio as horario_inicio, cg_horario_fin as horario_fin from comcat_producto_nafin where ic_producto_nafin = ? ");
				pstmtHorario = lodbConexion.queryPrecompilado(lsQryHorario.toString());
				//pstmtHorario.setString(1, lsProducto);
				//pstmtHorario.setString(2, lsiNoCliente);
				pstmtHorario.setString(1, lsProducto);
			}
			//System.out.println("Proceso query(BIND):"+lsQryHorario.toString());
			//System.out.println("EPO: " + lsiNoCliente);
			rsHorario = pstmtHorario.executeQuery();
			if(rsHorario.next()){
				lovHorario.add(rsHorario.getString("HORARIO_INICIO"));
				lovHorario.add(rsHorario.getString("HORARIO_FIN"));
			} else {
				throw new NafinException("GRAL0020");
			}
			rsHorario.close();
			pstmtHorario.clearParameters();
			if(pstmtHorario != null) pstmtHorario.close();
			//return lovHorario;
		} catch (NafinException ne) {
			System.out.println("Horario.ovgetHorarioServicio(NafinException): " + ne);
			throw ne;
		} catch (Exception e) {
			System.out.println("Horario.ovgetHorarioServicio(Exception): " + e);
			throw new NafinException("GRAL0020");
		} finally {
			if (lodbConexion.hayConexionAbierta() == true)
					lodbConexion.cierraConexionDB();
			System.out.println("Horario.ovgetHorarioServicio(S)");
		}
		return lovHorario;
	}
	
	/*************************************************/
	/**********FODEA 005-2009 FVR*********************/
	/*************************************************/
	public static String validarHorarioDescElec(int lsProducto, String lsTipoUsuario, String icEpo) throws NafinException, Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentencia = "";
		String cierre_servicio = "";
		
		
		//boolean desc_dia_sig = false;
		String estado_servicio = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		int num_dias =1;
		String lsHorarioIni="";
		String lsHorarioFin="";
		boolean lbError = false;
		
		//validarDia(lsTipoUsuario, icEpo);
		
		try{
			con.conexionDB();
			qrySentencia = "SELECT cg_horario_inicio, cg_horario_fin, " +
					"       'com/netro/exception/Horario::validarHorario()' AS origenquery " +
					"  FROM comrel_horario_x_epo " +
					" WHERE ic_producto_nafin = ? AND ic_epo = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,lsProducto);
			long numero1=Long.parseLong(icEpo);
			ps.setLong(2,numero1);
			rs = ps.executeQuery();
			if(rs.next())	{
				lsHorarioIni = rs.getString(1);
				lsHorarioFin = rs.getString(2);
			}else{
				
				if(rs!=null)rs.close();
				if(ps!=null)ps.close();
				
				qrySentencia = "SELECT UPPER (cs_cierre_servicio), cg_horario_inicio, cg_horario_fin, " +
							"       'com/netro/exception/Horario::validarHorario()' AS origenquery " +
							"  FROM comcat_producto_nafin " +
							" WHERE ic_producto_nafin = ?  ";

				ps = con.queryPrecompilado( qrySentencia );
				ps.setInt(1,lsProducto);
				rs = ps.executeQuery();
				ps.clearParameters();

				if(rs.next())	{
					lsHorarioIni = rs.getString(2);
					lsHorarioFin = rs.getString(3);
					//lsCierre     = rs.getString(1);
				}

			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();
			
			/*SE DETERMINARA SI EL HORARIO DE SERVICIO YA TERMINO O AUN NO COMIENZA*/
			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				lbError = false;
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp){	lbError = true; num_dias=0;}
				if (liHora == liHoraIniCmp)	if (liMinutos < liMinutoIniCmp){	lbError = true; num_dias=0;}

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				if (liHora > liHoraFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
			}
			/*-----*/
			if(!lbError){
				try{			
					/*SE VALIDA SI ESTA ABIERTO EL SERVICIO EL DIA ACTUAL*/
					validarHorario(lsProducto ,lsTipoUsuario ,icEpo);
					estado_servicio="AB";
		
				}catch (NafinException ne){
					//desc_dia_sig = true;
					//estado_servicio="CE";
					throw ne;
				}	
			}else{//else de if(!lbError)
				/*SI NO ESTA EN EL HORARIO INDICADO, SE OBTIENE SIGUIENET DIA HABIL*/
				//if(desc_dia_sig){
					int numTasas = 0;
					int numTasasRequeridas = 0;
					
					validarDia(lsTipoUsuario,icEpo);//SE AGREGA POR OBSERVACION DEL F056-2010 FVR
					
					while (!validarDiaSigHabil(lsTipoUsuario ,icEpo,num_dias)){
						num_dias += 1;
					}
					
					/*SE VALIDA SI HAY TASAS OPERATIVAS PARA EL SIGUIENTE DIA HABIL*/
					qrySentencia=
							" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::abrirServicio()'"   +
							"   FROM com_mant_tasa mt, comcat_tasa t"   +
							"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE + "+num_dias+", 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
							"    AND t.ic_tasa (+) = mt.ic_tasa"   +
							"    AND t.cs_disponible = 'S'"  ;
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					
					if(rs.next())
						numTasas = rs.getInt("NUMTASAS");
					rs.close();
					if(ps != null) ps.close();
					
					/*SE OBTIENE NUMERO DE TASAS REQUERIDAS*/
					qrySentencia=
						" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
						"   FROM comcat_tasa"   +
						"  WHERE cs_disponible = 'S'"  ;
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					if(rs.next())
						numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
					rs.close();
					if(ps != null) ps.close();
					
					/*SE COMPARA TASAS OPERATIVAS DEL DIA HABIL CON LAS TASA REQUERIDAS*/
					if (numTasasRequeridas == numTasas) 
						estado_servicio="SH";
					else
						throw new NafinException("DSCT0102");
						//estado_servicio="CE";
					
					
				//}
			}
			return estado_servicio;
		}catch(NafinException e){
			e.printStackTrace();
			throw e;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}//cierre metodo
	
	public static boolean validarDiaSigHabil(String strTipoUsuario, String lsEpo, int num_dias)throws NafinException{
		String qry_dia_inhabil = "";
		String qry_dia_inhabil_x_anio = "";
		AccesoDB conexion = new AccesoDB();
		Calendar cal = Calendar.getInstance();
		PreparedStatement pstmt = null;
		ResultSet rs_dia_inhabil = null;
		boolean dia_valido = true;
		long numero1;
		try	{
			conexion.conexionDB();

			qry_dia_inhabil_x_anio  = "select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comcat_dia_inhabil where df_dia_inhabil = TO_DATE(TO_CHAR(sysdate+"+num_dias+",'dd/mm/yyyy'),'dd/mm/yyyy') UNION ";
			qry_dia_inhabil_x_anio += " select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comrel_dia_inhabil_x_epo where df_dia_inhabil = TO_DATE(TO_CHAR(sysdate+"+num_dias+",'dd/mm/yyyy'),'dd/mm/yyyy') and ic_epo = ? ";
			
			qry_dia_inhabil  = "select count(*) as diaInhabil   , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comcat_dia_inhabil where cg_dia_inhabil = TO_CHAR(sysdate+"+num_dias+",'dd/mm') UNION ";
			qry_dia_inhabil += " select count(*) as diaInhabil  , 'com/netro/exception/Horario::validarDia()' as ORIGENQUERY from comrel_dia_inhabil_x_epo where cg_dia_inhabil = TO_CHAR(sysdate+"+num_dias+",'dd/mm') and ic_epo = ? ";

			pstmt = conexion.queryPrecompilado(qry_dia_inhabil_x_anio);
			numero1=Long.parseLong(lsEpo);
			pstmt.setLong(1,numero1);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					dia_valido=false;
					//throw new NafinException("DSCT0046");
				}
			}
			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry_dia_inhabil_x_anio);
			
			pstmt = conexion.queryPrecompilado(qry_dia_inhabil);
			numero1=Long.parseLong(lsEpo);
			pstmt.setLong(1,numero1);
			rs_dia_inhabil = pstmt.executeQuery();
			pstmt.clearParameters();

			while(rs_dia_inhabil.next())	{
				if(rs_dia_inhabil.getInt(1) != 0) {
					System.out.println("Lo sentimos. Hoy es un d�a inhabil");
					dia_valido=false;
					//throw new NafinException("DSCT0046");
				}
			}
			rs_dia_inhabil.close();
			if(pstmt != null) pstmt.close();
			System.out.println("proceso query(BIND):"+qry_dia_inhabil);

			cal.add(Calendar.DATE,num_dias);
			int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
			if (diaSemana == 7 || diaSemana == 1) {
				// Verifica que no sea s�bado o domingo
				System.out.println("Lo sentimos. Hoy es un d�a inhabil S-D");
				dia_valido=false;
				//throw new NafinException("DSCT0046");
			}
			
		}		
		catch (Exception e)	{
			System.out.println("Exception: "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
		}
		return dia_valido;
	}//cierre metodo
	
	
	public static String validarHorarioDescElec(int lsProducto, String lsTipoUsuario, String icEpo, String icIf) throws NafinException, Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentencia = "";
		String cierre_servicio = "";
		
		
		//boolean desc_dia_sig = false;
		String estado_servicio = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		int num_dias =1;
		String lsHorarioIni="";
		String lsHorarioFin="";
		boolean lbError = false;
		
		//validarDia(lsTipoUsuario, icEpo);
		
		try{
			con.conexionDB();
			qrySentencia = "SELECT cg_horario_inicio, cg_horario_fin, " +
					"       'com/netro/exception/Horario::validarHorarioDescElec()' AS origenquery " +
					"  FROM comrel_horario_epo_x_if " +
					" WHERE ic_producto_nafin = ? AND ic_epo = ? and ic_if = ?";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,lsProducto);
			long numero1=Long.parseLong(icEpo);
			ps.setLong(2,numero1);
			ps.setLong(3,Long.parseLong(icIf));
			rs = ps.executeQuery();
			if(rs.next())	{
				lsHorarioIni = rs.getString(1);
				lsHorarioFin = rs.getString(2);
			}else{
				
				if(rs!=null)rs.close();
				if(ps!=null)ps.close();
				
				qrySentencia = "SELECT UPPER (cs_cierre_servicio), cg_horario_inicio, cg_horario_fin, " +
							"       'com/netro/exception/Horario::validarHorario()' AS origenquery " +
							"  FROM comcat_producto_nafin " +
							" WHERE ic_producto_nafin = ?  ";

				ps = con.queryPrecompilado( qrySentencia );
				ps.setInt(1,lsProducto);
				rs = ps.executeQuery();
				ps.clearParameters();

				if(rs.next())	{
					lsHorarioIni = rs.getString(2);
					lsHorarioFin = rs.getString(3);
					//lsCierre     = rs.getString(1);
				}

			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();
			
			/*SE DETERMINARA SI EL HORARIO DE SERVICIO YA TERMINO O AUN NO COMIENZA*/
			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				lbError = false;
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp){	lbError = true; num_dias=0;}
				if (liHora == liHoraIniCmp)	if (liMinutos < liMinutoIniCmp){	lbError = true; num_dias=0;}

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				if (liHora > liHoraFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
			}
			/*-----*/
			if(!lbError){
				try{			
					/*SE VALIDA SI ESTA ABIERTO EL SERVICIO EL DIA ACTUAL*/
					validarHorarioEPOxIF(lsProducto ,lsTipoUsuario ,icEpo, icIf);
					estado_servicio="AB";
		
				}catch (NafinException ne){
					//desc_dia_sig = true;
					//estado_servicio="CE";
					throw ne;
				}	
			}else{//else de if(!lbError)
				/*SI NO ESTA EN EL HORARIO INDICADO, SE OBTIENE SIGUIENET DIA HABIL*/
				//if(desc_dia_sig){
					int numTasas = 0;
					int numTasasRequeridas = 0;
					
					validarDia(lsTipoUsuario,icEpo);//SE AGREGA POR OBSERVACION DEL F056-2010 FVR
					
					while (!validarDiaSigHabil(lsTipoUsuario ,icEpo,num_dias)){
						num_dias += 1;
					}
					
					/*SE VALIDA SI HAY TASAS OPERATIVAS PARA EL SIGUIENTE DIA HABIL*/
					qrySentencia=
							" SELECT COUNT (*) AS numtasas, 'ManejoServicioEJB::abrirServicio()'"   +
							"   FROM com_mant_tasa mt, comcat_tasa t"   +
							"  WHERE TO_DATE (TO_CHAR (dc_fecha, 'dd/mm/yyyy'), 'dd/mm/yyyy') = TO_DATE (TO_CHAR (SYSDATE + "+num_dias+", 'dd/mm/yyyy'), 'dd/mm/yyyy')"   +
							"    AND t.ic_tasa (+) = mt.ic_tasa"   +
							"    AND t.cs_disponible = 'S'"  ;
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					
					if(rs.next())
						numTasas = rs.getInt("NUMTASAS");
					rs.close();
					if(ps != null) ps.close();
					
					/*SE OBTIENE NUMERO DE TASAS REQUERIDAS*/
					qrySentencia=
						" SELECT COUNT (*) AS numtasasrequeridas, 'ManejoServicioEJB::abrirServicio()'"   +
						"   FROM comcat_tasa"   +
						"  WHERE cs_disponible = 'S'"  ;
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					if(rs.next())
						numTasasRequeridas = rs.getInt("NUMTASASREQUERIDAS");
					rs.close();
					if(ps != null) ps.close();
					
					/*SE COMPARA TASAS OPERATIVAS DEL DIA HABIL CON LAS TASA REQUERIDAS*/
					if (numTasasRequeridas == numTasas) 
						estado_servicio="SH";
					else
						throw new NafinException("DSCT0102");
						//estado_servicio="CE";
					
					
				//}
			}
			return estado_servicio;
		}catch(NafinException e){
			e.printStackTrace();
			throw e;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}//cierre metodo
	
	
	public static void validarHorarioEPOxIF(int numero, String strTipoUsuario, String lsEpo, String icIf)
		throws NafinException, Exception
	{
		validarDia(strTipoUsuario, lsEpo);

		String lsCampos = "", lsCampoCierre = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		System.out.println("\n\n\n La hora es : " + liHora + ":" + liMinutos + "  \n\n\n");

		ResultSet rsHorario;
 		//String  lsQuery = "SELECT cg_horario_inicio, cg_horario_fin FROM comrel_horario_x_epo WHERE ic_producto_nafin = "+numero+" AND ic_epo = " + lsEpo,
 		String  lsQuery = "",
				lsHorarioIni = "",
				lsHorarioFin = "",
				lsCierre     = "";

		AccesoDB conexion = new AccesoDB();
		try	{
			conexion.conexionDB();
			//rsHorario = conexion.queryDB(lsQuery);
			String qryHorario = " SELECT cg_horario_inicio, cg_horario_fin  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY " +
									  " FROM comrel_horario_epo_x_if " +
									  " WHERE ic_producto_nafin = ? " +
									  " AND ic_epo = ? "+ 
									  " AND ic_if = ? ";
			PreparedStatement pstmt = conexion.queryPrecompilado(qryHorario);
			pstmt.setInt(1,numero);
			long numero1=Long.parseLong(lsEpo);
			pstmt.setLong(2,numero1);
			pstmt.setLong(3,Long.parseLong(icIf));
			rsHorario = pstmt.executeQuery();
			pstmt.clearParameters();



			if(rsHorario.next())	{
				lsHorarioIni = rsHorario.getString(1);
				lsHorarioFin = rsHorario.getString(2);

				System.out.print("lsHorarioIni: " + lsHorarioIni + "\n");
				System.out.print("lsHorarioFin: " + lsHorarioFin + "\n");
				
				lsQuery = "SELECT UPPER(cs_cierre_servicio)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM COMCAT_PRODUCTO_NAFIN WHERE ic_producto_nafin = ? ";
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();

				if(rsHorario.next())
					lsCierre     = rsHorario.getString(1);
			}
			else	{
				//lsQuery = "SELECT UPPER(cs_cierre_servicio), cg_horario_inicio, cg_horario_fin FROM comcat_producto_nafin WHERE ic_producto_nafin = " + numero;
				lsQuery = "SELECT UPPER(cs_cierre_servicio), cg_horario_inicio, cg_horario_fin  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin WHERE ic_producto_nafin = ? ";
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();

				if(rsHorario.next())	{
					lsHorarioIni = rsHorario.getString(2);
					lsHorarioFin = rsHorario.getString(3);
					lsCierre     = rsHorario.getString(1);

					if ( strTipoUsuario.equals("IF") )	{	// SE ACTUALIZA LA TABLA comrel_horaio_x_epo SI NO TIENE HORARIO PARAMETRIZADO
						//lsQuery = "SELECT cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if FROM comcat_producto_nafin  WHERE ic_producto_nafin = " + numero;
						lsQuery = "SELECT cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin  WHERE ic_producto_nafin = ? ";
						pstmt = conexion.queryPrecompilado( lsQuery );
						pstmt.setInt(1,numero);
						ResultSet rs = pstmt.executeQuery();
						pstmt.clearParameters();

						if ( rs.next() )	{
							lsQuery = "INSERT INTO comrel_horario_epo_x_if " +
								"	(ic_producto_nafin, ic_epo, ic_if, cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if) " +
								"VALUES " +
								"	("+numero+",'"+lsEpo+"', '"+icIf+"', '"+ rs.getString("cg_horario_inicio") +"', '"+rs.getString("cg_horario_fin")+"', '"+rs.getString("cg_horario_inicio_if")+"', '"+rs.getString("cg_horario_fin_if")+"')";
							conexion.ejecutaSQL(lsQuery);
		    				conexion.terminaTransaccion(true);
						}
						rs.close();
						if(pstmt != null) pstmt.close();
						System.out.println("proceso query(BIND):"+lsQuery);
					}

					System.out.print("lsHorarioIni: " + lsHorarioIni + "\n");
					System.out.print("lsHorarioFin: " + lsHorarioFin + "\n");
				}
				rsHorario.close();
				if(pstmt != null) pstmt.close();
				System.out.println("proceso query(BIND):"+lsQuery);
			}
			conexion.cierraConexionDB();




			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				boolean lbError = false;
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp)	lbError = true;
				if (liHora == liHoraIniCmp)	if (liMinutos < liMinutoIniCmp)	lbError = true;

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				if (liHora > liHoraFinCmp)	{
					lbError = lbTiempoTranscurrido = true;
				}
				System.out.println("Horario valido = " + !lbError);


				if (lbError && (strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN")))	{
					System.out.println("Checando Pyme NUEVO");
					throw new NafinException("DSCT0048");
				}
				else	{
					/**** LA BANDERA NO SE VALIDA CON IF, SOLO CON PYME  ****/
					if ( strTipoUsuario.equals("PYME") || strTipoUsuario.equals("NAFIN") )	{
						if ( lsCierre.equals("S") )	{
							System.out.print("cs_cierre_servicio = (" + lsCierre + ")\n");
							throw new NafinException("DSCT0049");
						}
					}
				}
			}

		}
		catch (NafinException ne)	{
			System.out.println("HorarioException: "+ne);
			throw ne;
		}
		catch (Exception e)	{
			System.out.println("Exception: "+e);
			throw e;
		}
		finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraStatement();
				conexion.cierraConexionDB();
			}
		}
	}
	
	
	public static void validarHorarioIF(int numero, String strTipoUsuario, 
			String lsEpo, String lsIf) throws NafinException {
		System.out.println("Horario.validarHorario(E):IF");

		String lsCampos = "", lsCampoCierre = "";

		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());

		int liHora     = cal.get(Calendar.HOUR_OF_DAY);
		int liMinutos  = cal.get(Calendar.MINUTE);
		System.out.println("Horario.validarHorario():IF:La hora es : " + liHora + ":" + liMinutos +"\n");

		ResultSet rsHorario;
		String    lsQuery = "SELECT cg_horario_inicio_if, cg_horario_fin_if  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comrel_horario_epo_x_if WHERE ic_producto_nafin = ? AND ic_epo = ?  AND ic_if = ? " ,
		lsHorarioIni = "",
		lsHorarioFin = "",
		lsCierre     = "";

		AccesoDB conexion = new AccesoDB();
		try	{
			validarDia(strTipoUsuario, lsEpo);
			conexion.conexionDB();
			PreparedStatement pstmt = conexion.queryPrecompilado(lsQuery);
			pstmt.setInt(1,numero);
			long numero1=Long.parseLong(lsEpo);
			pstmt.setLong(2,numero1);
			pstmt.setLong(3,Long.parseLong(lsIf));
			rsHorario = pstmt.executeQuery();
			pstmt.clearParameters();

			if(rsHorario.next())	{
				lsHorarioIni = rsHorario.getString("CG_HORARIO_INICIO_IF");
				lsHorarioFin = rsHorario.getString("CG_HORARIO_FIN_IF");

				System.out.print("Horario.validarHorario():IF:lsHorarioIni: " + lsHorarioIni +"\n");
				System.out.print("Horario.validarHorario():IF:lsHorarioFin: " + lsHorarioFin +"\n");

			} else {
				System.out.print("Horario.validarHorario():IF: Sin horario x EPO \n");
				lsQuery = "SELECT cg_horario_inicio_if, cg_horario_fin_if, cg_horario_inicio, cg_horario_fin, 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM comcat_producto_nafin WHERE ic_producto_nafin = ? " ;
				pstmt = conexion.queryPrecompilado( lsQuery );
				pstmt.setInt(1,numero);
				rsHorario = pstmt.executeQuery();
				pstmt.clearParameters();
				if(rsHorario.next()) {
					lsHorarioIni = rsHorario.getString("CG_HORARIO_INICIO_IF");
					lsHorarioFin = rsHorario.getString("CG_HORARIO_FIN_IF");

					// SE ACTUALIZA LA TABLA comrel_horario_x_epo SI NO TIENE HORARIO PARAMETRIZADO
					lsQuery = "INSERT INTO comrel_horario_epo_x_if " +
							"	(ic_producto_nafin, ic_epo, ic_if,  cg_horario_inicio, cg_horario_fin, cg_horario_inicio_if, cg_horario_fin_if) " +
							" VALUES " +
							"	("+numero+",'"+lsEpo+"', '"+lsIf+"', '"+ rsHorario.getString("CG_HORARIO_INICIO") +"', '"+rsHorario.getString("CG_HORARIO_FIN")+"', '"+lsHorarioIni+"', '"+lsHorarioFin+"')";
					conexion.ejecutaSQL(lsQuery);
		    		conexion.terminaTransaccion(true);
					System.out.print("Horario.validarHorario():IF:lsHorarioIni: " + lsHorarioIni +"\n");
					System.out.print("Horario.validarHorario():IF:lsHorarioFin: " + lsHorarioFin +"\n");
				}
			}
			rsHorario.close();
			if(pstmt != null) {
				pstmt.close();
			}


			if ( !lsHorarioIni.equals("") && !lsHorarioFin.equals(""))	{
				int 	liHoraIniCmp   = Integer.parseInt( lsHorarioIni.substring(0, lsHorarioIni.indexOf(":")) ),
						liHoraFinCmp   = Integer.parseInt( lsHorarioFin.substring(0, lsHorarioFin.indexOf(":")) ),
						liMinutoIniCmp = Integer.parseInt( lsHorarioIni.substring(lsHorarioIni.indexOf(":")+1) ),
						liMinutoFinCmp = Integer.parseInt( lsHorarioFin.substring(lsHorarioFin.indexOf(":")+1) );

				System.out.print("Horario.validarHorario():IF:Inicial   horas a checar: " + liHoraIniCmp + ":" + liMinutoIniCmp + "\n");
				System.out.print("Horario.validarHorario():IF:Final     horas a checar: " + liHoraFinCmp + ":" + liMinutoFinCmp + "\n");

				//	SI TODAVIA NO ES HORA
				if (liHora < liHoraIniCmp)	 {
					throw new NafinException("DSCT0048");
				}
				if (liHora == liHoraIniCmp)
					if (liMinutos < liMinutoIniCmp)
						throw new NafinException("DSCT0048");

				// SI YA PASO LA HORA
				boolean lbTiempoTranscurrido = false;
				if (liHora == liHoraFinCmp)	if (liMinutos > liMinutoFinCmp)	{
					lbTiempoTranscurrido = true;
					throw new NafinException("DSCT0048");
				}
				if (liHora > liHoraFinCmp)	{
					lbTiempoTranscurrido = true;
					throw new NafinException("DSCT0048");
				}
				//System.out.print("Horario valido = " + !lbError);

				// CHECANDO DOCUMENTOS SI ES USUARIO IF
				
				//if (lbTiempoTranscurrido && strTipoUsuario.equals("IF"))	{
				//	if (numero == 1)
				//		lsQuery = "SELECT /*+index (com_documento in_com_documento_04_nuk)*/  COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM com_documento WHERE ic_estatus_docto = 3 AND ic_epo = ? ";
				/*	else if (numero == 2)
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM com_pedido WHERE ic_estatus_pedido = 3 AND ic_epo = ? ";
					else if (numero == 4)
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM dis_documento WHERE ic_estatus_docto = 3 AND ic_epo = ? ";
					else if (numero == 5){
						lsQuery = "SELECT COUNT(1)  , 'com/netro/exception/Horario::validarHorario()' as ORIGENQUERY  FROM inv_disposicion WHERE ic_estatus_disposicion = 1 AND ic_epo = ? ";
					}

					pstmt = conexion.queryPrecompilado( lsQuery );
					pstmt.setString(1, lsEpo);
					ResultSet rsEstatus = pstmt.executeQuery();
					pstmt.clearParameters();

					if(rsEstatus.next())	{
						if( rsEstatus.getInt(1) != 0 )	{
							System.out.println("Horario.validarHorario():IF:Hay documentos pendientes");
						} else {
							throw new NafinException("DSCT0048");
						}
					}
					rsEstatus.close();
					if(pstmt != null) pstmt.close();
				}*/
			}
		}
		catch (NafinException ne)	{
			System.out.println("Horario.validarHorario(HorarioException): "+ne.getMsgError());
			throw ne;
		}
		catch (Exception e)	{
			System.out.println("Horario.validarHorario(Exception): "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
			System.out.println("Horario.validarHorario(S):IF");
		}
	}
	
	
	

}