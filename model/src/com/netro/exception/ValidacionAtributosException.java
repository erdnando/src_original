package com.netro.exception;


/**
 * Esta clase representa los errores que se detectan durante el 
 * proceso de validación de los atributos de una clase (usualmente un bean)
 * @author Gilberto Aparicio
 *
 */
public class ValidacionAtributosException extends Exception {

	public ValidacionAtributosException() {}

	public ValidacionAtributosException(String mensajeError) {
		super(mensajeError);
	}
}