package com.netro.exception;


import netropology.utilerias.ServiceLocator;


/**
 * Manejo de mensajes de error.
 * @author
 * @author Gilberto Aparicio
 *
 */
public class NafinException extends Exception {
	String	m_sCodigo;
	String 	m_sMsgError;
	String 	m_sMsgErrorIngles;

	/**
	 * Constructor. De manera predeterminada obtiene los mensajes
	 * del idioma espa�ol
	 * @param vsCodigoError C�digo de error 
	 *
	 */

	public NafinException(String vsCodigoError) {

		m_sMsgError = "Mensaje Inesperado";
		m_sMsgErrorIngles = "Unexpected Error";

		m_sCodigo = vsCodigoError;

		try {
			NafinetException objExc = 
					ServiceLocator.getInstance().lookup("NafinetExceptionEJB", com.netro.exception.NafinetException.class);
			//Mensaje en Espa�ol
			m_sMsgError = objExc.getMensajeError(vsCodigoError, "ES");
			//Mensaje en Ingl�s
			m_sMsgErrorIngles = objExc.getMensajeError(vsCodigoError, "EN");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public NafinException(String vsCodigoError, String MsgExt) {
		this(vsCodigoError);
		m_sMsgError += ". "+MsgExt;
		m_sMsgErrorIngles += ". "+MsgExt;
	}

	/****************************************************************************
	*	int getCodError
	*****************************************************************************/
	public String getCodError() {
		return m_sCodigo;
	}

	/****************************************************************************
	*	String getMsgError
	*****************************************************************************/
	public String getMsgError() {
		return getMessage();
	}

	public String getMessage() {
		return m_sMsgError;
	}

	public String toString() {
		return "Clave Error: " + getCodError() + ". " + getMessage();
	}

	public String getMsgError(String idioma) {
		return getMessage(idioma);
	}

	public String getMessage(String idioma) {
		if (idioma.equals("EN")) {
			return m_sMsgErrorIngles;
		} else {
			return m_sMsgError;
		}
		
	}
}