package com.netro.exception;

import javax.ejb.Remote;

@Remote
public interface NafinetException {

    public void cargarDatos();
    public String getMensajeError(String vsCodigo);
    public String getMensajeError(String vsCodigo, String idioma);

}