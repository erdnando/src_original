package com.netro.exception;

import java.sql.ResultSet;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;

/****************************************************************************
 *
 *	clase NafinetExceptionBean
 *
 *****************************************************************************/
@Stateless(name = "NafinetExceptionEJB" , mappedName = "NafinetExceptionEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class NafinetExceptionBean implements NafinetException {

	static Map m_htException;

	public void cargarDatos() {

		m_htException = new HashMap();

		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String sentenciaSQL =
					" SELECT cc_modulo||ic_error as clave, " +
					" cg_descripcion, " +
					" 	cg_descripcion_ing " +
					" FROM comcat_error ";
			ResultSet rs = con.queryDB(sentenciaSQL);
			while (rs.next()) {
				m_htException.put(rs.getString("clave") + "ES",
						 rs.getString("cg_descripcion"));
				m_htException.put(rs.getString("clave") + "EN",
						 rs.getString("cg_descripcion_ing"));
			}
			rs.close();
			con.cierraStatement();
			//System.out.println(m_htException);
		} catch (Exception e) {
			throw new EJBException(e);
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/**
	 * Obtiene el mensaje de error de acuerdo al c�digo recibido
	 * @param vsCodigo c�digo de error del tipo SIST0001
	 * @return Cadena con mensaje de error.
	 */
	public String getMensajeError(String vsCodigo) {
		try {
			return getMensajeError(vsCodigo, "ES");
		} catch (Exception e) {
			throw new EJBException(e);
		}
	}


	/**
	 * Obtiene el mensaje de error de acuerdo al c�digo recibido
	 * @param vsCodigo c�digo de error del tipo SIST0001
	 * @param idioma Idioma ES Espa�ol EN Ingl�s
	 * @return Cadena con mensaje de error.
	 */
	public String getMensajeError(String vsCodigo, String idioma) {
		try {

			String lsCodigo = "";
			if (m_htException == null) {
				cargarDatos();
			}
			lsCodigo = (String) m_htException.get(vsCodigo + idioma);
			return lsCodigo;
		} catch (Exception e) {
			throw new EJBException(e);
		}
	}

}