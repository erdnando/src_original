/*************************************************************************************
*
* Nombre de Clase o de Archivo: AutorizacionDescuento
*
* Versi�n: 0.1
*
* Fecha Creaci�n: 20/02/2002
*
* Autor: Gilberto E. Aparicio
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase:
*
*************************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 28/10/2002
 *
 *  Agregar los m�todos para Tasas Preferenciales/Negociables.
 *
***************************************************************************/

package com.netro.descuento;
 
import java.util.*;
import com.netro.exception.*;
import java.sql.*;
import netropology.utilerias.*;
import javax.ejb.Remote;

@Remote
public interface AutorizacionTasas {

	public Vector getTasaBase (String claveEPO)
		throws NafinException;

	public Vector getTasaBase (String claveEPO, String claveIF, String voboIF)
		throws NafinException;

	public Vector getTasaTipificada (String claveEPO, String [] fechaTasa)
		throws NafinException;

	public Vector getTasaAutIF (String claveEPO, String claveIF, String moneda, String operaFideicomico )
		throws NafinException;

	public boolean esFactorajeVencido (String claveEpo)
		throws NafinException;

	public Vector getTasasDisponibles ()
		throws NafinException;

	public Vector getTasasDisponibles (String claveEPO)
		throws NafinException;

	public Vector getTasasDisponibles (String claveEPO, String claveMoneda)
		throws NafinException;

	public Vector getTasasPorClasificacion (String claveEPO)
		throws NafinException;

	public String getUltimaClaveTasaProductoEpo(String ic_epo,
			String ic_producto_nafin, String ic_moneda)
			throws NafinException;

	public Vector getIFAsociados (String claveEPO)
		throws NafinException;

	public void setTasas (String claveEPO, Vector vDatosTasaBase,
			Vector vDatosTasaIF)
		throws NafinException;

	public Vector getIFXRelacionar (String claveEPO)
		throws NafinException;

    // Agregado 28/10/2002	--CARP
    public Vector getProveedor(String esCveIf, String esCveMoneda, String esCadenaEPO)
		throws NafinException;

    public Vector getPlazo(String esCveMoneda)
		throws NafinException;

    // Agregado 28/10/2002	--CARP
    public boolean guardarTasas(String esCveIf, String esCveMoneda,
    						   String esCvePyme, String esTipoTasa,
                               String[] esCadenaTasas, String[] esValorTasas,
							   String cc_acuse,String icUsuario,String _acuse, String nombreUsuario, String lsPuntos, String aplicaFloating)
		throws NafinException;

    // Agregado 28/10/2002	--CARP
    public Vector procesarTasaNegociada(String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo,String tipoTasa)
		throws NafinException;

    // Agregado 28/10/2002	--CARP
    public Vector procesarTasaPreferencial(String esCveIf, String esCveMoneda,
    									   String esCadenaEPO, String esCvePyME,
                                           double edPuntos,String tipoTasa ,String aplicaFloating )
		throws NafinException;

	public void setRelacion (String claveEPO, String claveIF, String[] fechaHoraTasa, List vDatos)
		throws NafinException;

	public void actualizarTasaBase (String claveEPO, String relTasaBase,
			String puntosTasaBase, String fechaTasaBase)
		throws NafinException;

	public void guardarTasaProductoEpo(String ic_producto_nafin,
			String ic_epo, String ic_tasa, String ic_plazo)
		throws NafinException;

	public void actualizarTasaProductoEpo(String ic_tasa_producto_epo,
			String ic_tasa, String ic_plazo)
		throws NafinException;

	public void eliminarTasaProductoEpo(String ic_tasa_producto_epo)
			 throws NafinException;

	public void setEstatusTasaAutorizadaIF (String claveIF,
			String claveEPO, String fechaTasa, String voboEPO,
			String voboIF, String voboNAFIN, String relmat_tasa, String puntos)
		throws NafinException;

	public void setEstatusTasaAutorizadaIF (String claveIF,
			String claveEPO, String fechaTasa, String voboEPO,
			String voboIF, String voboNAFIN, String relmat_tasa, String puntos,
			String plazoEspecifico)
		throws NafinException;
		
	public void actualizarTipificacion (String fechaTasaTipificacion,
			 String relMatTipificacion, String puntosTipificacion,
			 String claveCalificacion, String voboNAFIN)
		throws NafinException;

	// Autorizaci�n de las tasas por la IF
	public void autorizarTasasEPO (String claveIF,
			String claveEPO, String[] fechasTasa)
		throws NafinException;

	// Autorizaci�n de las tasas por NAFIN
	public void autorizarTasasEPOIF (String claveEPO, Vector vDatosTasas)
		throws NafinException;

	public void setEstatusTasasBase (String claveEPO, Vector vDatosTasasBase)
		throws NafinException;

	public void setEstatusTasasTipificadas (Vector vDatosTasasTipificadas)
		throws NafinException;

	public void autorizarTodasLasTasas (String claveEPO, String[] arrFechasTasasBase,
			String[] arrDatosTasasTipificadas, String[] arrDatosTasasAutorizadasIF)
		throws NafinException;

	//new 08/11/2004
	public abstract Vector consultaTasaBase(String ic_epo, String fecha,String sesIdiomaUsuario)
		throws NafinException;
	public abstract Vector consultaTasaBase(String ic_epo, String fecha)
		throws NafinException;

	public abstract Vector consultaTasaAutoIf(String ic_epo, String fecha, String ic_IF, String sesIdiomaUsuario)
		throws NafinException;
	public abstract Vector consultaTasaAutoIf(String ic_epo, String fecha, String ic_IF)
		throws NafinException;

	public abstract Vector consultaTraeMoneda(String _sEPO, String _sIF, String _cveMoneda)
		throws NafinException;

    public abstract int operaFactorajeVencido(String ic_epo)
    	throws NafinException;

    public abstract Vector consultaHistTasa(String ic_epo, String fecha)
    	throws NafinException;
	//end new 08/11/2004
	
    public Vector obtenerTasas(String esCveIf, String esCveMoneda, String esCadenaEPO, String esCvePyME, String esCvePlazo)
    	throws NafinException;

	public Vector getMonedaRelacionadaIF (String claveEPO, String claveIF) throws NafinException;
	
	//FODEA 046-2009 FVR - INI
	
	public boolean guardarTasasMandato(String esCveIf, String esCveMoneda,
    						   String esCvePyme, String esTipoTasa,
                               String[] esCadenaTasas, String[] esValorTasas,
							   String cc_acuse,String ic_usuario,String _acuse)
	throws NafinException;//Agregado 10/10/2009 -- FVR
	
	
   public Vector procesarTasaNegociada(String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo, String tiop_factoraje,String tipoTasa)
	throws NafinException;//Agregado 10/10/2009 -- FVR
	
	public void eliminarTasasMandato(List lstParamTasas)throws NafinException;//Agregado 19/10/2009 -- FVR
	/*------------------*/
	public ArrayList getMandante(String esCadenaEPO)throws NafinException;//Agregado 08/11/2009 -- FVR
	
	public Vector procesarTasaPreferencialMandante(String esCveIf, String esCveMoneda,
    									   String esCadenaEPO, String esCvePyME, double edPuntos )
	throws NafinException;//Agregado 08/11/2009 -- FVR
	
	public Vector  procesarTasaNegociadaMandante(String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo)
	throws NafinException;//Agregado 08/11/2009 -- FVR
	/*------------------*/
	//FODEA 046-2009 FVR - FIN
		 public String eliminarTasas(String esCveIf, 
														   String esCveMoneda,
    						               String esCvePyme, 
															 String esTipoTasa,
                               String[] esCadenaTasas, 
															 String[] esValorTasas,
							                 String nombreUsuario,
															 String lsPuntos,
															 String ldTotal)
		throws NafinException;
		
		public Vector getIntermediario(String esCveMoneda, String esCadenaEPO) 	throws NafinException;
	  public Vector obtenerTasasNafin(	String lsTipoTasa, String lsNoEpo, String lsMoneda,  String lsIF, String lsProveedor,
		String lsPuntos, String lsPlazo, double edPuntos, String tipoTasa )	throws NafinException;
	  public List obtenerTasasNafinGuardadas( String lsCadenaEpo, String lstIF, String lsCveMoneda, String lsPYME, String lsTipoTasa, String lstPlazo, String acuse ) 
	  	throws AppException; 

	
	// Fodea 036 - 2010 JSHD
	public ResultadosValidacionTasas procesarCargaMasivaTasas( String strDirectorio, String esNombreArchivo, boolean esTasaNegociada, String claveMoneda, String claveEPO, String claveIF, String clavePlazo)
		throws AppException;
		
	public ArrayList getRegistrosValidos(ArrayList lineas, int idProceso)
		throws AppException;
		
	public ArrayList procesarTasasPreferenciales( String esCveIf, String esCveMoneda, String esCadenaEPO, String esCvePlazo, String icProceso, String lsLineasOk )
		throws AppException;
		
	public ArrayList procesarTasasNegociadas( String esCveIf, String esCveMoneda, String esCadenaEPO, String esCvePlazo, String icProceso, String lsLineasOk )
	 	throws AppException;

	public boolean guardaModificacionesCargaMasiva( String esCveIf, String esCveMoneda, String lsCveEpo, String esTipoTasa, String cc_acuse, String ic_usuario, String _acuse, String[] lsListaTasasInsertar, String[] lsListaTasasEliminar, String nombreUsuario);
	
	public HashMap obtenerParametrizacionPuntosLimite() throws AppException;//FODEA 049 - 2010 ACF
	public void guardarParametrizacionPuntosLimite(HashMap parametrosPuntosLimite) throws AppException;//FODEA 049 - 2010 ACF
	public HashMap obtenerParametrizacionPuntosLimitePorEpo(String claveEpo, String csParametroLimite) throws AppException;//FODEA 049 - 2010 ACF
	public void cambiarParametrizacionPuntosLimite(String claveEpo, String csPuntosLimite)  throws AppException;//FODEA 049 - 2010 ACF
	
 public String consultaHistTasaEXT(String ic_epo, String fecha) throws AppException;
 public String getTasaAutIFEXT(String claveEPO, String claveIF, String cveMoneda)throws AppException;
 public String consultaTasaAutoIfEXT(String ic_epo, String fecha, String ic_IF, String sesIdiomaUsuario) throws AppException;
 public String IfparaTasas(String ic_epo, String ic_pyme) throws AppException;
 public String nombreEPO(String ic_epo)  throws AppException;
 
	//F031-2011 FVR
	public List getTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda) throws AppException;
	public List getPymesOferTasas(String sesCveIf, String cboEpo, String cboMoneda, String numElecPyme, String csNoParam) throws AppException;
	public void setTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException;
	public void deleteTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException;
	public void cambiaEstatusTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException;
	public void setRelPymeOfertaTasas(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, List lstPymesAsign, Acuse acuse, String usuario) throws AppException;

	public boolean tipodeTasas(String ic_pyme, String cg_rfc) 	throws AppException;
	public List consTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, String nafinEle) throws AppException;
	
	public List getPymesOferTasasCons(String sesCveIf, String cboEpo, String cboMoneda, String numElecPyme, String csNoParam, String razonSocial, String rfc) throws AppException;
	//Migraci�n IF  
	public Vector  consultaTasaNegociada(String ic_epo,  String  ic_if, String ic_moneda, String ic_pyme , String tipoTasa, String lstPlazo ,	String acuse ) throws NafinException;
	public boolean guardaModificacionesCargaMasivaExt( String esCveIf, String esCveMoneda, String lsCveEpo, String esTipoTasa, String cc_acuse, String ic_usuario, String _acuse, String[] lsListaTasasInsertar, String[] lsListaTasasEliminar, String nombreUsuario,String  estatus);     		
	public Registros getRegistrosValidosExt(List lineas, int idProceso);  
	public Vector  consultaTasasMandato(String ic_epo,  String  ic_if, String ic_moneda, String ic_mandante ,  String tipoTasa, String lstPlazo ,String acuse )throws NafinException;
	public Vector  consultaTasasMandatoC(String ic_epo,  String  ic_if,	String ic_moneda, String ic_mandante, 	String tipoTasa ) throws NafinException;
	public String  getTasaIFDescHis (String  lsEposEnc, String lstEPO) throws NafinException;
	public List  getTasaIFDesc (String claveEPOs, String claveIF, String cveMoneda, String lsCadenaPyme, String lsTipoTasa, String  aplicaFloating) throws NafinException;
	public List verificaNafinElectronico(String ic_nafin,String ic_epo) throws AppException;
	public List busquedaProveedor(String ic_nafin) throws AppException;
	public List busquedaAvanzadaProveedor(String val_nombre, String val_rfc) throws NafinException; //FODEA 011 - 2009 ACF
	
	public HashMap buscaProveedorTasasPreferencialesNegociadas( String claveIF, String claveMoneda, String claveEPO, String numeroNafinElectronico) 
		 throws AppException;
			
	public String operaFideicomiso(String cveIf) throws AppException;
	
	/**
	 * metodo para verificar si la pyme es suceptible a tasa floating 
	 * QC-2018
	 * @param clavePyme
	 * @param rfc
	 * @return
	 * @throws AppException
	 */
	public String suceptibleFloating(String clavePyme, String rfc) throws AppException;
	

}   