/*************************************************************************************
*
* Nombre de Clase o Archivo: IMantenimiento.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 13/febrero/2002
*
* Autor:          
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase: Interface remota del EJB de Mantenimiento en Descuento Electronico.
*
*************************************************************************************/

package com.netro.descuento;

import java.util.*;
import netropology.utilerias.Acuse;
import com.netro.exception.*;
import netropology.utilerias.AppException;
import javax.ejb.Remote;

@Remote
public interface IMantenimiento {

	public void vvalidaFV( String esClaveEpo )
		throws NafinException;

	public Vector ovgetDocumentosMant(String esClavePyme, String esNumeroDocto, String esFechaDocumento, 
			String esFechaVencimiento, String esClaveMoneda, String esMontoMinimo, String esMontoMaximo, 
			String esEstatus, String esAforo, String esClaveEpo, boolean ebOperaFactorajeVencido, 
			String esDoctosSeleccionados[],	boolean ebInicial, String esAforoDL, String sesIdiomaUsuario) throws NafinException;

	public Vector ovgetEstatusAsignar(String esEstatuDeConsulta, String sesIdiomaUsuario) throws NafinException;
	
	public void vrealizarCambioEstatusDocto( String esClaveNuevoEstatus, String esDoctosSeleccionados[], 
											String esCambioMotivo, String esEstatusConsulta, String usuario )//FODEA 015 - 2009 ACF
		throws NafinException;

	public Vector ovgetDocumentosSelPyme(String esClaveEPO, String esNumeroDocto, String esFechaVencimiento, 
			String esClaveMoneda, String esMontoMinimo, String esMontoMaximo, String esEstatus,
			String esClaveIF, String esDoctosSeleccionados[], boolean ebInicial, String nafinPyme)
		throws NafinException;

	public Vector ovgetDocumentosSelPyme(String esClaveEPO, String esNumeroDocto, String esFechaVencimiento, 
			String esClaveMoneda, String esMontoMinimo, String esMontoMaximo, String esEstatus,
			String esClaveIF, String esDoctosSeleccionados[], String estatusDoctos[], boolean ebInicial, String nafinPyme)
		throws NafinException;		
		
	public Vector ovgetDocumentosPagosVto(String esClaveEPO, String esClavePyme, String esFechaVencimiento,
											String esEstatusSolicitud, String esNoClienteIf, 
											String esTipoSolicitud) throws NafinException;
	public Hashtable ohactualizaDoctoSolicitud(String esNuevoEstatusSolicitud,
												Vector evOrigenClave) throws NafinException;
	public Vector ovgetDocumentosCambioEstatus(String esClaveIf, String esFolios, 
												String esDocumentos) throws NafinException;
												
	public Vector ovgetDocumentosCambioEstatusNeg(String esNoClienteEpo, String esClavePyme, 
												String esClaveIf, String esNoDocto,
												String esFechaVenc, String esClaveMoneda,
												String esMontoMin, String esMontoMax, String tipofactoraje) throws NafinException;
	public Vector ovgetDocumentosSeleccionadosPyme(String esNoClienteEpo, String esClaves) throws NafinException;

	public List osactualizaHistoricoCambioEstatus(String seleccionados[], String estatus[],String ct_cambio_motivo, String usuario) throws NafinException;//FODEA 015 - 2009 ACF
	public boolean bactualizaEstatusNegociable(String esClaves) throws NafinException;

	public Vector ovgetPreNegociables(String ic_epo,String cc_acuse,String fechaCargaIni, String fechaCargaFin,String ic_usuario, String ic_estatus) throws NafinException;
	public Vector mostrarDocumentosPreNegociables(String sAforo,String sNoCliente,String sEstatus,String sAforoDL, String cc_acuse) throws NafinException;
	public void eliminaPrenegociables(String ic_epo, String cc_acuse) throws NafinException;
	public boolean bactualizaEstatusPreNegociable(String ic_epo, String cc_acuse,String ic_estatus, String usuario) throws NafinException;
	public List mostrarDocumentosReasignados(String sepo, String sno_electronico_prov, String sNoproveedor, String srfc, String snodoctos, String susuario_reasigno, String df_asigna_i, String df_asigna_f, String rutaDir) throws NafinException;
	
	public void guardarAcuseReasignacion(List datos, List lDatos) throws NafinException;
	//**************************************************************************** FODEA 050 - 2008 
	public boolean tieneParamEpoPef(String icEpo) throws NafinException;
	public List getDoctosMantFechaRecepcion(String icDocumento, String icEpo, String icPyme) throws NafinException;
	//**************************************************************************** FODEA 050 - 2008 
	public String getUsuarioCambioEstatus(String icDocumento) throws NafinException;//FODEA 015 - 2009 ACF
	
	/*FODEA 053-2009 FVR*/
	public Vector ovgetPreNegociables(String ic_epo, String cc_acuse, String fechaCargaIni, String fechaCargaFin, String ic_usuario, String ic_estatus, String numDoctos) 
	throws NafinException;
	
	public Vector mostrarDocumentosPreNegociables(String sAforo,String sNoCliente,String sEstatus,String sAforoDL, String cc_acuse, String numDoctos)
	throws NafinException;
	
	public boolean bactualizaEstatusPreNegociable(String ic_epo, String cc_acuse,String ic_estatus, String numDoctos, String usuario)
	throws NafinException;
	
	public Vector ovgetPendientes(String ic_epo, String cc_acuse, String fechaCargaIni, String fechaCargaFin, String ic_usuario, String ic_estatus, String numDoctos)
	throws NafinException;
	
	public ArrayList ovgetDetallePendientes(String ic_epo, String cc_acuse, String numDoctos)
	throws NafinException;
	
	public void eliminaPendientes(String ic_epo, String cc_acuse)
	throws NafinException;
	
	public boolean bactualizaEstatusPendiente(String ic_epo, String cc_acuse,String ic_estatus, String numDoctos)
	throws NafinException;
	
	public Vector mostrarDocumentosPendientes(String sAforo, String sNoCliente, String sEstatus, String sAforoDL, String cc_acuse, String numDoctos)
	throws NafinException;
	
	public ArrayList ovgetDetallePreNegociables(String ic_epo, String cc_acuse, String numDoctos)
	throws NafinException;
	
	public boolean bactualizaEstatusNegociable(String esClaves,String ic_epo) 
	throws NafinException;
	
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[],String estatusDoctos[])
	throws AppException;
	
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[],String estatusDoctos[],String estatusEsperado)
	throws AppException; // Fodea 039 - 2011
	
	/*-*/
	public List ConsulCambioEstatusNaf(String noIf, String noDocumento)	throws AppException; //DLHC 2010
	public List ConsulCambioEstatusPre(String noDocumento, String estatus) throws AppException; //DLHC 2010
	public String  getCambioEstatus(List datos ) throws AppException; //DLHC 2010
	
	public boolean bactualizaEstatusSeleccionadasPyme(String clavesDocumento) // Fodea 039 - 2011 By JSHD
		throws NafinException;
		
	public List getEstatusAsignar(String esEstatuDeConsulta) throws AppException;
  
  public String  getArchivoBajaProveedores(String cveEpo, String strDirectorioTmp) throws AppException;
  public String cargaDatosTmpBajaProveedores (String rutaArchivo, String rutaPdf) throws AppException;
  public void validaCargaProveedores(String numProceso, String cveEpo) throws AppException;
  public List getListBajaProvValidados(String numProceso)throws AppException;
  public String setBajaProveedores(String numProceso, String cveEpo, String rutaZip, String rutaPdf, String pkcs7, String textoFirmado, String serial ,String folio, String usuario) throws AppException;
  public String getDescargaArchivoBajaProv(String rutaDestino, String cveCambio, String tipoArchivo) throws AppException;

}