package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class IMSSEnlace extends EpoPEF implements EpoEnlace,Serializable { 


	public String getTablaDocumentos()	{	return "comtmp_doctos_pub_imss";	}
	public String getTablaAcuse()		{	return "com_doctos_pub_acu_imss";	}
	public String getTablaErrores()		{	return "com_doctos_err_pub_imss";	}
	
	public String getDocuments(){
		return 	"select ig_numero_docto" +
				" ,cg_pyme_epo_interno" +
				" ,df_fecha_docto" +
				" ,df_fecha_venc "+
				" ,ic_moneda" +
				" ,fn_monto" +
				" ,cs_dscto_especial" +
				" ,ct_referencia" +
				" ,cg_campo1" +
				" ,cg_campo2" +
				" ,cg_campo3" +
				" ,cg_campo4" +
				" ,cg_campo5 " +
				" ,'' as ic_if"+
				" ,'' as ic_nafin_electronico"+getCamposPEF()+
				" from "+getTablaDocumentos();
	}
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into com_doctos_err_pub_imss (ig_numero_docto, ig_numero_error, cg_error, " + getCamposDigitoIdentificador() + ") "+
				" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"'"+ ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+")";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update com_doctos_pub_acu_imss set IN_TOTAL_PROC="+acu.getInTotalProc()+",FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoAcepMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+",IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+",FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoRechMn()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+",IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+"' where CC_ACUSE='"+acu.getCcAcuse()+"'";
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into com_doctos_pub_acu_imss(CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN, "+
				"IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL, "+
				"IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoAcepMn()+","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+
				","+acu.getFnTotalMontoRechMn()+","+acu.getInTotalAcepDl()+","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}	
	
	public String getCondicionQuery(){
		return "";
	}
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }
	
}
