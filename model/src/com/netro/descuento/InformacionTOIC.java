package com.netro.descuento;
import com.netro.exception.NafinException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface InformacionTOIC {
	  //Fodea 049 - 2008 EBA
		public HashMap getDatosCtaPdf(String fechaInicio, String fechaFin, String ic_epo)throws SQLException, Exception;
	  //FODEA 049 - VALC - 10/2008
		public List getResumenCargaFFON(String fecha_proceso_de, String fecha_proceso_a) throws NafinException;
		public List getDetalleCarga(String tipo, String ic_carga) throws NafinException;
		/*******FODEA 049-2008 - FVR*******/
		public void setCargaTmpMasivaSIAFF(String Ruta, int numeroProceso, String tipo_carga) throws NafinException;//FODEA 034 - 2009 ACF
		public void procesarCargaSIAFF(int numeroProceso, int numero_carga_siaff, String claveUsuario, String nombreArchivo) throws NafinException;//FODEA 034 - 2009 ACF
		public List getCargaSIAFF(String fec_ini, String fec_fin) throws NafinException;
		public List getCargaSIAFFxEpo(String fec_ini, String fec_fin, String ic_epo) throws NafinException;
		public boolean getExisteParametroEpo(String numParametro, String sEpo)throws NafinException;
		//Fodea 049 - 2008 EBA			
		public boolean getExisteReporte   (String fechaInicioMensual, String sEpo) throws NafinException ;
		public boolean getExisteReporteDia(String fechaActual, String sEpo) throws NafinException ;
		public HashMap getDatosCtaPdfNAE  (String fechaInicioMensual, String sEpo, String tipoConsulta ) throws NafinException ;	
		public void   setDatosCtaPdf             (HashMap ArDatosPDFMen, HashMap ArDatosPDFAnu, HashMap ArDatosPDFAcu, String fechaInicioMensual, String sEpo, boolean flag ) throws NafinException ;		
		public boolean getExisteParametrizacionEpo(String sEpo) throws NafinException ;
		//RESUMEN PYME
		public HashMap getResumenPymes(String ic_epo, String df_publicacion_de, String df_publicacion_a) throws NafinException;
		//REPORTE ANALITICO DE DOCUMENTOS
		public HashMap getReporteAnaliticoDoctos(String ic_epo, String df_publicacion_de, String df_publicacion_a) throws NafinException;
		/*FODEA 034 - 2009 ACF*/
		public int getNumeroProcesoCargaSIAFF() throws NafinException;
		public List getRegistrosErroneosCargaSIAFF(int ic_carga_siaff) throws NafinException;
		public int getNumeroCargaSIAFF() throws NafinException;
		public boolean existenErroresCargaSIAFF(int ic_carga_siaff) throws NafinException;
		public void eliminarCargaSIAFF(int ic_carga_siaff) throws NafinException;
		public String getTipoEpo(String sEpo) throws NafinException;
		public List getInfoDiversa(List informacion)  throws NafinException; //Fodea 017-2011
		//Migración EPO 2012	
		public String getArchivoPlanoSIAFF(String strTipoUsuario, String iNoCliente, String fini, String ffin, String cargaSiaff, String flag);

		//Migración Nafin 2013
		public String leeArchivo(String rutaArchivo)	 throws NafinException; 
		public HashMap validaArchivo(int  numeroProceso) throws NafinException; 
		

}