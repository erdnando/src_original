package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class FonacotEnlace extends EpoPEF implements EpoEnlace,Serializable { 

	private String ic_epo = "";
	public FonacotEnlace(String ic_epo){
		this.ic_epo = ic_epo;
	}
	
	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_FONACOT";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_FONACOT";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_FONACOT";	}
	
	public String getDocuments(){
	
		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" ,'' as cg_campo2"+
				" ,'' as cg_campo3"+
				" ,'' as cg_campo4"+
				" ,'' as cg_campo5"+								
				" ,'' as ic_if"+
				" , IC_NAFIN_ELECTRONICO "+getCamposPEF()+
				" from "+getTablaDocumentos()+" "+
				" where IC_EPO = "+ic_epo;
	}
	public String getInsertaErrores(ErroresEnl err){
		return 	"INSERT INTO "+getTablaErrores()+" " +
				"            (ic_epo, ig_numero_docto, ig_numero_error, cg_error, cg_pyme_epo_interno, fn_monto, df_fecha_venc, " +
				"             cc_acuse, ct_referencia, " + getCamposDigitoIdentificador() + ") "+
				"   SELECT ic_epo, ig_numero_docto, "+err.getIgNumeroError()+", '"+err.getCgError()+"', cg_pyme_epo_interno, fn_monto, df_fecha_venc, " +
				"          '"+err.getCcAcuse()+"', ct_referencia " + ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+
				"     FROM "+getTablaDocumentos()+" " +
				"    WHERE ic_epo = "+ic_epo+" " +
				"      AND ig_numero_docto = '"+err.getIgNumeroDocto()+"' "+
				"      AND cg_pyme_epo_interno = '"+err.getCg_pyme_epo_interno()+"' ";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+",FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+",IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+",FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+",IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+"' where CC_ACUSE='"+acu.getCcAcuse()+"' and ic_epo ="+ic_epo;
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IC_EPO, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+ic_epo+","+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}	

	public String getCondicionQuery(){
		return " where ic_epo = "+ic_epo;
	}
  public void addErrores(ErroresEnl err){}
	public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }
  
}
