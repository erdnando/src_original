package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class liverpoolEnlace implements EpoEnlace, EpoEnlaceDetalle, Serializable {

	private String ic_epo = "";
	public liverpoolEnlace(String ic_epo){
		this.ic_epo = ic_epo;
	}
	
	public String getTablaDocumentos() 			{	return "comtmp_doctos_pub_l01";	}
	public String getTablaAcuse()				{	return "com_doctos_pub_acu_l01"; }
	public String getTablaErrores()				{	return "com_doctos_err_pub_l01"; }
	public String getTablaDetalles()			{	return "comtmp_doctosd_pub_l01"; }
	
	public String getDocuments(){
		return 	"select ig_numero_docto" +
				" ,cg_pyme_epo_interno" +
				" ,df_fecha_docto" +
				" ,df_fecha_venc" +
				" ,ic_moneda " +
				" ,fn_monto" +
				" ,cs_dscto_especial" +
				" ,ct_referencia" +
				" ,cg_campo1" +
				" ,cg_campo2" +
				" ,cg_campo3" +
				" ,cg_campo4 " +
				" ,cg_campo5 "+ 
				" ,ic_if"+
				" ,'' AS IC_NAFIN_ELECTRONICO "+
				" from " + getTablaDocumentos()+" "+
				" where IC_EPO = "+ic_epo;// F053 - 2010
	}
	
	public String getDetalles(String ic_epo){
		return 	"select l01.ig_numero_docto, TO_CHAR(l01.df_fecha_docto,'DD/MM/YYYY'), " +
				" l01.ic_moneda, l01.ic_consecutivo, l01.cg_campo1, l01.cg_campo2, l01.cg_campo3, " +
				" l01.cg_campo4, l01.cg_campo5, l01.cg_campo6, l01.cg_campo7, l01.cg_campo8, " +
				" l01.cg_campo9, l01.cg_campo10, d.ic_documento " +
				" from "+getTablaDetalles()+" l01, com_documento d " +
				" where l01.ig_numero_docto = d.ig_numero_docto (+) " +
				" and l01.ic_moneda = d.ic_moneda (+) " +
				" and to_char(l01.df_fecha_docto,'yyyy') = to_char(d.df_fecha_docto (+),'yyyy') " +
				" and d.ic_epo (+) = "+ic_epo+" "+
				" and l01.ic_epo = " +ic_epo+" ";// F053 - 2010
	}
	
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+" (ic_epo, ig_numero_docto,ig_numero_error,cg_error) values ('"+ic_epo+"','"+err.getIgNumeroDocto()+"','"+err.getIgNumeroError()+"','"+err.getCgError()+"')";// F053 - 2010
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return 	"update com_doctos_pub_acu_l01 set IN_TOTAL_PROC="+acu.getInTotalProc()+", IN_TOTAL_PROC_DETALLE="+acu.getInTotalProcDetalle()+", "+
				"IN_TOTAL_PROC_MN_NEG="+acu.getInTotalprocMnNeg()+", FN_TOTAL_MONTO_MN_NEG="+acu.getFnTotalMontoMnNeg()+", "+
				"IN_TOTAL_PROC_MN_NONEG="+acu.getInTotalProcMnNoneg()+", FN_TOTAL_MONTO_MN_NONEG="+acu.getFnTotalMontoMnNoneg()+", "+
				"IN_TOTAL_PROC_DL_NEG="+acu.getInTotalProcDlNeg()+", FN_TOTAL_MONTO_DL_NEG="+acu.getFnTotalMontoDlNeg()+", "+
				"IN_TOTAL_PROC_DL_NONEG="+acu.getInTotalProcDlNoneg()+", FN_TOTAL_MONTO_DL_NONEG="+acu.getFnTotalMontoDlNoneg()+", "+
				"CS_ESTATUS='"+acu.getCsEstatus()+"' where CC_ACUSE='"+acu.getCcAcuse()+"' and ic_epo ="+ic_epo; // F053 - 2010
	}
	
	public String getInsertAcuse(AcuseEnl acu){
		return 	"insert into "+getTablaAcuse()+"(CC_ACUSE, IC_EPO, IN_TOTAL_PROC, IN_TOTAL_PROC_DETALLE, "+ // F053 - 2010
				"IN_TOTAL_PROC_MN_NEG, FN_TOTAL_MONTO_MN_NEG, IN_TOTAL_PROC_MN_NONEG, FN_TOTAL_MONTO_MN_NONEG, "+
				"IN_TOTAL_PROC_DL_NEG, FN_TOTAL_MONTO_DL_NEG, IN_TOTAL_PROC_DL_NONEG, FN_TOTAL_MONTO_DL_NONEG, "+
				"DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+ic_epo+","+acu.getInTotalProc()+","+acu.getInTotalProcDetalle()+","+ // F053 - 2010
				acu.getInTotalprocMnNeg()+","+acu.getFnTotalMontoMnNeg()+","+acu.getInTotalProcMnNoneg()+","+acu.getFnTotalMontoMnNoneg()+","+
				acu.getInTotalProcDlNeg()+","+acu.getFnTotalMontoDlNeg()+","+acu.getInTotalProcDlNoneg()+","+acu.getFnTotalMontoDlNoneg()+","+
				"SYSDATE,'"+((acu.getCsEstatus()).equals("null")?"":acu.getCsEstatus())+"')";
	}
	
	public String getCondicionQuery() {
		return " where ic_epo = "+ic_epo;// F053 - 2010
	}
	
	public void addErrores(ErroresEnl err) {}
	
	public List getErrores() {
		ArrayList lista = new ArrayList();
		return lista;
	}
	
}
