package com.netro.descuento;

import java.io.Serializable;

public class DetallePagoIFNB implements Serializable {

	public DetallePagoIFNB() {}
	
	/**
	 * Establece la subaplicacion
	 * @param strSubaplicacion Subaplicacion 
	 */
	public void setSubaplicacion(String strSubaplicacion){
		subaplicacion = strSubaplicacion;
	}

	/**
	 * Establece el numero de prestamo
	 * @param strPrestamo Numero de prestamo 
	 */
	public void setPrestamo(String strPrestamo) {
		prestamo = strPrestamo;
	}
	
	/**
	 * Establece la clave Sirac del cliente
	 * @param strClaveSiracCliente Clave Sirac del cliente
	 */
	public void setClaveSiracCliente(String strClaveSiracCliente) {
		claveSiracCliente = strClaveSiracCliente;
	}
	
	/**
	 * Establece el capital
	 * @param strCapital Capital
	 */
	public void setCapital(String strCapital) {
		capital = strCapital;
	}
	
	/**
	 * Establece los intereses
	 * @param strIntereses Intereses
	 */
	public void setIntereses(String strIntereses) {
		intereses = strIntereses;
	}
	
	/**
	 * Establece los intereses moratorio
	 * @param strMoratorios Interes moratorio
	 */
	public void setMoratorios(String strMoratorios) {
		moratorios = strMoratorios;
	}
	
	/**
	 * Establece el subsidio
	 * @param strSubsidio Subsidio
	 */
	public void setSubsidio(String strSubsidio) {
		subsidio = strSubsidio;
	}
	
	/**
	 * Establece la comision
	 * @param strComision comision
	 */
	public void setComision(String strComision) {
		comision = strComision;
	}
	
	/**
	 * Establece el IVA
	 * @param strIVA IVA
	 */
	public void setIVA(String strIVA) {
		iva = strIVA;
	}
	
	/**
	 * Establece el importe de pago
	 * @param strImportePago Importe de pago
	 */
	public void setImportePago(String strImportePago) {
		importePago = strImportePago;
	}
	
	/**
	 * Establece el comcepto de pago
	 * @param strConceptoPago Concepto de pago
	 */
	public void setConceptoPago(String strConceptoPago) {
		conceptoPago = strConceptoPago;
	}
	
	/**
	 * Establece el origen de pago
	 * @param strOrigenPago Origen de pago
	 */
	public void setOrigenPago(String strOrigenPago) {
		origenPago = strOrigenPago;
	}
	
	/**
	 * Establece la sucursal
	 * @param strSucursal Sucursal
	 */
	public void setSucursal(String strSucursal) {
		sucursal = strSucursal;
	}
	
	/**
	 * Establece el acreditado
	 * @param strAcreditado Acreditado
	 */
	public void setAcreditado(String strAcreditado) {
		acreditado = strAcreditado;
	}
	
	/**
	 * Establece la fecha de operacion
	 * @param strFechaOperacion FechaOperacion
	 */
	public void setFechaOperacion(String strFechaOperacion) {
		fechaOperacion = strFechaOperacion;
	}
	
	/**
	 * Establece la fecha de vencimiento
	 * @param strFechaVencimiento FechaVencimiento
	 */
	public void setFechaVencimiento(String strFechaVencimiento) {
		fechaVencimiento = strFechaVencimiento;
	}
	
	/**
	 * Establece la fecha pago
	 * @param strFechaPago FechaPago
	 */
	public void setFechaPago(String strFechaPago) {
		fechaPago = strFechaPago;
	}
	
	/**
	 * Establece el saldo insoluto
	 * @param strSaldoInsoluto SaldoInsoluto
	 */
	public void setSaldoInsoluto(String strSaldoInsoluto) {
		saldoInsoluto = strSaldoInsoluto;
	}
	
	/**
	 * Establece la tasa
	 * @param strTasa tasa
	 */
	public void setTasa(String strTasa) {
		tasa = strTasa;
	}
	
	/**
	 * Establece los dias
	 * @param strDias dias
	 */
	public void setDias(String strDias) {
		dias = strDias;
	}

	/**
	 * Obtiene la subaplicacion
	 * @return Subaplicacion
	 */
	public String getSubaplicacion(){
		return subaplicacion;
	}

	/**
	 * Obtiene el numero de prestamo
	 * @return Numero de prestamo
	 */
	public String getPrestamo() {
		return prestamo;
	}
	
	/**
	 * Obtiene la clave sirac del cliente
	 * @return Clave Sirac del cliente
	 */

	public String getClaveSiracCliente() {
		return claveSiracCliente;
	}
	
	/**
	 * Obtiene el capital
	 * @return Capital
	 */

	public String getCapital() {
		return capital;
	}
	
	/**
	 * Obtiene los intereses
	 * @return Intereses
	 */
	public String getIntereses() {
		return intereses;
	}

	/**
	 * Obtiene los intereses moratorios
	 * @return Intereses Moratorios
	 */
	public String getMoratorios() {
		return moratorios;
	}

	/**
	 * Obtiene el subsidio
	 * @return Subsisdio
	 */
	public String getSubsidio() {
		return subsidio;
	}
	
	/**
	 * Obtiene la comision
	 * @return Comision
	 */
	public String getComision() {
		return comision;
	}
	
	/**
	 * Obtiene el IVA
	 * @return IVA
	 */
	public String getIVA() {
		return iva;
	}
	
	/**
	 * Obtiene el importe de pago
	 * @return Importe de pago
	 */

	public String getImportePago() {
		return importePago;
	}

	/**
	 * Obtiene Concepto de pago
	 * @return Concepto de pago
	 */
	public String getConceptoPago() {
		return conceptoPago;
	}
	
	/**
	 * Obtiene el origen de pago
	 * @return Origen de pago
	 */
	public String getOrigenPago() {
		return origenPago;
	}
	
	/**
	 * Obtiene la sucursal
	 * @return sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}
	
	/**
	 * Obtiene el acreditado
	 * @return acreditado
	 */
	public String getAcreditado() {
		return acreditado;
	}
	
	/**
	 * Obtiene la fecha de operacion
	 * @return fechaOperacion
	 */
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	
	/**
	 * Obtiene la fecha de vencimiento
	 * @return fechaVencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	
	/**
	 * Obtiene la fecha pago
	 * @return fechaPago
	 */
	public String getFechaPago() {
		return fechaPago;
	}
	
	/**
	 * Obtiene el saldo insoluto
	 * @return fechaPago
	 */
	public String getSaldoInsoluto() {
		return saldoInsoluto;
	}
	
	/**
	 * Obtiene la tasa
	 * @return tasa
	 */
	public String getTasa() {
		return tasa;
	}
	
	/**
	 * Obtiene los dias
	 * @return dias
	 */
	public String getDias() {
		return dias;
	}

	/**
	 * Obtiene el query de insercion para el detalle
	 * @param strClaveDetalle Clave del Detalle (PK)
	 * @param strClaveEncabezado Clave del encabezado (PK)
	 * @return Cadena con la instruccion de SQL para insertar
	 * 		el registro.
	 */
	 
	public String getSQLInsert(String strClaveDetalle, 
			String strClaveEncabezado) {
		
		StringBuffer campos = new StringBuffer();
		campos.append("ic_detalle_pago");
		campos.append(",ic_encabezado_pago");
		
		StringBuffer valoresCampos = new StringBuffer();
		valoresCampos.append(strClaveDetalle);
		valoresCampos.append("," + strClaveEncabezado);
		
		
		if (subaplicacion != null) {
			campos.append(",ig_subaplicacion");
			valoresCampos.append("," + this.subaplicacion);
		}
		if (prestamo != null) {
			campos.append(",ig_prestamo");
			valoresCampos.append("," + this.prestamo);
		}
		if (claveSiracCliente != null) {
			campos.append(",ig_cliente");
			valoresCampos.append("," + this.claveSiracCliente);
		}
		if (capital != null) {
			campos.append(",fg_amortizacion");
			valoresCampos.append("," + this.capital);
		}
		if (intereses != null) {
			campos.append(",fg_interes");
			valoresCampos.append("," + this.intereses);
		}
		if (moratorios != null) {
			campos.append(",fg_interes_mora");
			valoresCampos.append(","  + this.moratorios);
		}
		if (subsidio != null) {
			campos.append(",fg_subsidio");
			valoresCampos.append("," + this.subsidio);
		}
		if (comision != null) {
			campos.append(",fg_comision");
			valoresCampos.append("," + this.comision);
		}
		if (iva != null) {
			campos.append(",fg_iva");
			valoresCampos.append("," + this.iva);
		}
		if (importePago != null) {
			campos.append(",fg_totalexigible");
			valoresCampos.append("," + this.importePago);
		}
		if (conceptoPago != null) {
			campos.append(",cg_concepto_pago");
			valoresCampos.append(",'" + this.conceptoPago + "'");
		}
		if (origenPago != null) {
			campos.append(",cg_origen_pago");
			valoresCampos.append(",'" + this.origenPago + "'");
		}
		
		if (sucursal != null) {
			campos.append(",ig_sucursal");
			valoresCampos.append("," + this.sucursal + "");
		}
		if (acreditado != null) {
			campos.append(",cg_acreditado");
			valoresCampos.append(",'" + this.acreditado + "'");
		}
		if (fechaOperacion != null) {
			campos.append(",df_operacion");
			valoresCampos.append(",to_date('" + this.fechaOperacion + "','dd/mm/yyyy')");
		}
		if (fechaVencimiento != null) {
			campos.append(",df_vencimiento");
			valoresCampos.append(",to_date('" + this.fechaVencimiento + "','dd/mm/yyyy')");
		}
		if (fechaPago != null) {
			campos.append(",df_pago");
			valoresCampos.append(",to_date('" + this.fechaPago + "','dd/mm/yyyy')");
		}
		if (saldoInsoluto != null) {
			campos.append(",fn_saldo_insoluto");
			valoresCampos.append("," + this.saldoInsoluto);
		}
		if (tasa != null) {
			campos.append(",ig_tasa");
			valoresCampos.append("," + this.tasa);
		}
		if (dias != null) {
			campos.append(",ig_dias");
			valoresCampos.append("," + this.dias);
		}

		String strSQL = 
				" INSERT INTO com_detalle_pago (" + campos + ")" +
				" VALUES (" + valoresCampos + ")";
		
		return strSQL;		
	}



	private String subaplicacion;
	private String prestamo;
	private String claveSiracCliente;
	private String capital;
	private String intereses;
	private String moratorios;
	private String subsidio;
	private String comision;
	private String iva;
	private String importePago;
	private String conceptoPago;
	private String origenPago;
	
	private String sucursal;
	private String acreditado;
	private String fechaOperacion;
	private String fechaVencimiento;
	private String fechaPago;
	private String saldoInsoluto;
	private String tasa;
	private String dias;

}