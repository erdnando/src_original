package com.netro.descuento;

import java.util.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface LimitesPorEPO {

	public boolean bactualizaLimites(String esLimiteEPO[], String esIcEPO[], 
									String esNoCliente, String esCsActivaLimite[],
									String esFecVencLim[], String esAvisoVencLim[],
									String esFecCamAdmon[], String esAvisoCamAdmon[],
									String claveUsuario, String csActivaProgramado[], 
									String csActivaProgramado2[], String csAcuseRecibo, String lsFechaLimite2[] ) throws NafinException;
	public void bactualizaLimitesExt(
			List registrosModificados, String claveIF,
			String claveUsuario, String csAcuseRecibo, String usuarioBloqueo, String dependenciaBloqueo);
	public Vector ovgetLimites(String esNoCliente) throws NafinException;
	public Registros ovgetLimitesExt(String esNoCliente);
	public Vector ovgetLimitesPyME(String esNoCliente,String ic_epo,String ic_pyme) throws NafinException;										
	public boolean bactualizaLimites_Pyme(String esLimitePYME[], String esIcEPO, 
					String esNoCliente, String esCsActivaLimite[], String esIcPyme[], String csActivaProgramado[], String csActivaProgramado2[]) throws NafinException;
	public Vector ovgetLimitesPyME_C(String ic_if,String ic_epo,String ic_pyme) throws NafinException;	
	public void liberarLimitesxif(String valores, String fechaIni, String fechaFin)  throws NafinException;
	public void bitacoraLimitesxif(String valores, String claveNombre, String noAcuse)  throws NafinException;
	
	public HashMap  validaMonedas(String ic_epo , String ic_if) throws NafinException;

} 