package com.netro.descuento;

import com.netro.exception.NafinException;

import javax.ejb.Remote;

@Remote
public interface Notificacion {
    
    public void procesosaEjecutar(String ruta)throws NafinException;
}
