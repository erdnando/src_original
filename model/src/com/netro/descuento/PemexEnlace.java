package com.netro.descuento;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class PemexEnlace extends EpoPEF implements EpoEnlace,Serializable {

	private String ic_epo = "";
	public PemexEnlace(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_PEMEX_PEP";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_PEMEX_PEP";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_PEMEX_PEP";	}
	
	public String getDocuments(){

		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" , CG_CAMPO2"+
				" , cg_campo3"+
				" , cg_campo4"+
				" , cg_campo5"+
				" , '' as ic_if"+
				" , IC_NAFIN_ELECTRONICO "+getCamposPEF()+			
				
				" , FN_PORC_BENEFICIARIO "+  //2017_003 
				" , CG_NUM_COPADE "+  //2017_003 
				" , CG_NUM_CONTRATO "+  //2017_003 

				" from " + getTablaDocumentos()+
				"  where IC_EPO = "+ic_epo
				;
				
	}
	
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+" (IC_EPO, IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR, " + getCamposDigitoIdentificador() + ", IC_NAFIN_ELECTRONICO , CG_NUM_COPADE , CG_NUM_CONTRATO , CS_DSCTO_ESPECIAL    ) "+
		     " values("+ic_epo+",'"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"'"+ ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+",'"+ 
			err.getIcNafinElectronico()+"','"+err.getNumCopade()+"','"+err.getNumContrato()+"'"+((err.getCsDsctoEspecial()==null)?",'":",'"+err.getCsDsctoEspecial())+"')";
		
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+
				" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
				" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
				" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
				" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+ "'"+
				" ,CC_CONTROL='"+acu.getCcControl()+ "'"+
				" WHERE CC_ACUSE='"+acu.getCcAcuse()+"' and ic_epo ="+ic_epo;
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IC_EPO, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+ic_epo+","+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		return " where ic_epo = "+ic_epo;
	}
	
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }

}
