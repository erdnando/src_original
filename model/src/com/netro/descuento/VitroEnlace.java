package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class VitroEnlace implements EpoEnlace, EpoEnlaceDetalle, Serializable {
	
	private String ic_epo = "";
	public VitroEnlace(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_VITRO";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_VITRO";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_VITRO";	}
	public String getTablaDetalles()	{	return "COMTMP_DOCTOSD_PUB_VITRO"; }

	public String getDocuments(){
		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1 "+
				" , CG_CAMPO2 "+
				" , CG_CAMPO3 "+
				" , CG_CAMPO4 "+
				" , CG_CAMPO5 "+
				" , '' as ic_if"+
				" , '' AS IC_NAFIN_ELECTRONICO "+
				" from " + getTablaDocumentos()+
				" where IC_EPO = "+ic_epo;
	}
	
	public String getDetalles(String ic_epo) {
		return 	"select l01.ig_numero_docto, TO_CHAR(l01.df_fecha_docto,'DD/MM/YYYY'), " +
				" l01.ic_moneda, l01.ic_consecutivo, l01.cg_campo1, l01.cg_campo2, l01.cg_campo3, " +
				" l01.cg_campo4, l01.cg_campo5, l01.cg_campo6, l01.cg_campo7, l01.cg_campo8, " +
				" l01.cg_campo9, l01.cg_campo10, d.ic_documento " +
				" from  ( select * from "+getTablaDetalles()+" where ic_epo = "+ic_epo+" ) l01, com_documento d " +
				" where l01.ig_numero_docto = d.ig_numero_docto (+) " +
				" and l01.ic_moneda = d.ic_moneda (+) " +
				" and to_char(l01.df_fecha_docto,'yyyy') = to_char(d.df_fecha_docto (+),'yyyy') " +
				" and d.ic_epo (+) = "+ic_epo+" ";
	}
	
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+ getTablaErrores()+" (IC_EPO, IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR) "+
				" values("+ic_epo+" ,'"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"')";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return 	" update "+getTablaAcuse()+" set " + 
				"  IN_TOTAL_PROC="+acu.getInTotalProc()+
				" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
				" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
				" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
				" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,DF_FECHA_CARGA = SYSDATE " +
				" ,DF_HORA_CARGA = SYSDATE " + //TO_CHAR(SYSDATE,'hh24:mm:ss') " + // DF_HORA_CARGA
				" ,CS_ESTATUS='"+acu.getCsEstatus()+"'"+
				" where CC_ACUSE='"+acu.getCcAcuse()+"' "+
 				" and ic_epo ="+ic_epo;
	}
	
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IC_EPO, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHA_CARGA, DF_HORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"', "+ic_epo+","+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE, SYSDATE, "+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		return " where ic_epo = "+ic_epo;
	}
	
	public void addErrores(ErroresEnl err){}
	
	public List getErrores() {
		ArrayList lista = new ArrayList();
		return lista;
	}

}
