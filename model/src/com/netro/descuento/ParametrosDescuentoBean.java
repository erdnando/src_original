package com.netro.descuento;

import com.nafin.descuento.ws.CuentasPymeIFWS;

import com.netro.afiliacion.Afiliacion;
import com.netro.dispersion.DispersionBean;
import com.netro.exception.NafinException;
import com.netro.parametrosgrales.ParametrosGrales;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse2;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

/*************************************************************************************
 *
 * Nombre de Clase o Archivo: ParametrosDescuentoBean.java
 *
 * Versi�n: 1.0
 *
 * Fecha Creaci�n: 15/Noviembre/2004
 *
 * Autor: Hugo D�az Garc�a
 *
 * Descripci�n de Clase: Clase que implementa los m�todos para el EJB de Parametros de Descuento Electr�nico.
 *cs_factoraje_automatico

 *************************************************************************************/
@Stateless(name = "ParametrosDescuentoEJB" , mappedName = "ParametrosDescuentoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrosDescuentoBean implements ParametrosDescuento {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ParametrosDescuentoBean.class);


	/**
	 * Actualiza los datos del Clausulado Electr�nico configurado para
	 * la epo especificada en la BD
	 *
	 * @since F087-2006
	 * @param contrato Datos del contrato
	 * @param rutaArchivo Ruta del archivo PDF, PPT o DOC con el contenido
	 * 		del clausulado electr�nico o null si este no es modificado
	 */
	public void actualizarClausulado(ContratoEpo contrato,
			String rutaArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		int ic_epo = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (contrato==null || contrato.getClaveEpo() == null ||
					contrato.getConsecutivo() == null ||
					contrato.getTituloAviso() == null ||
					contrato.getContenidoAviso() == null ||
					contrato.getBotonAviso() == null ||
					contrato.getTextoAceptacion() == null ||
					contrato.getBotonAceptacion() == null ||
					contrato.getMostrar() == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_epo = Integer.parseInt(contrato.getClaveEpo());
			ic_consecutivo = Long.parseLong(contrato.getConsecutivo());

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" contrato=" + contrato + "\n" +
					" rutaArchivo = " + rutaArchivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" UPDATE com_contrato_epo " +
					" SET cg_titulo_aviso = ?, " +
					" 	cg_contenido_aviso = ?, " +
					" 	cg_boton_aviso = ?, " +
					" 	bi_documento = empty_blob(), " +
					" 	cg_texto_aceptacion = ?, " +
					" 	cg_boton_aceptacion = ?, " +
					" 	cs_mostrar = ?, " +
					" 	df_alta = sysdate " +
					" WHERE ic_epo = ? " +
					" 	AND ic_consecutivo = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, contrato.getTituloAviso());
			ps.setString(2, contrato.getContenidoAviso());
			ps.setString(3, contrato.getBotonAviso());
			ps.setString(4, contrato.getTextoAceptacion());
			ps.setString(5, contrato.getBotonAceptacion());
			ps.setString(6, contrato.getMostrar());
			ps.setInt(7, ic_epo);
			ps.setLong(8, ic_consecutivo);

			ps.executeUpdate();
			ps.close();

			if (rutaArchivo != null) {
				contrato.getContratoArchivo().guardarArchivoContratoEpo(rutaArchivo, con);
			}

		} catch(Exception e) {
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}


	}

	/* Creado por Hugoro, Foda 95, 15/11/2004
	 * Obtiene todas las Epos consideradas medianas por que se tiene registrada como Epo y Pyme. */
	public ArrayList getEpoMediana() throws NafinException {
	ArrayList alEpoPyme = new ArrayList();
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select E.ic_epo as noEpo, P.ic_pyme as noPyme, "+
							" E.cg_razon_social as nombreEpo, P.cg_razon_social as nombrePyme "+
							" from comcat_pyme P, comcat_epo E "+
							" where P.cg_rfc = E.cg_rfc";
			Vector vecDatos = null;
			try {
				ResultSet rs = con.queryDB(sQuery);
				while (rs.next()) {
					vecDatos = new Vector();
					vecDatos.addElement(rs.getString("noEpo"));
					vecDatos.addElement(rs.getString("noPyme"));
					vecDatos.addElement(rs.getString("nombreEpo"));
					vecDatos.addElement(rs.getString("nombrePyme"));
					alEpoPyme.add(alEpoPyme.size(), vecDatos);
				}
				rs.close();
				con.cierraStatement();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0087");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alEpoPyme;
	} // Fin del m�todo getComboEpoPyme()

	/* Creado por Hugoro, Foda 95, 16/11/2004
	 * Obtiene todos los intermediarios relacionados con la EPO. */
	public ArrayList getIntermediarioPorEpo(String sNoEpo) throws NafinException {
	ArrayList alIntermediarios = new ArrayList();
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select I.cg_razon_social "+
							" from comrel_if_epo IE, comcat_if I "+
							" where IE.ic_if = I.ic_if "+
							" and IE.ic_epo = ? "+
							" and IE.cs_vobo_nafin = 'S' "+
							" and IE.cs_aceptacion = 'S' ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoEpo);
			try {
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					alIntermediarios.add(alIntermediarios.size(), rs.getString(1));
				}
				rs.close();
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0088");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alIntermediarios;
	} // Fin del m�todo getIntermediarioPorEpo()

	/* Creado por Hugoro, Foda 95, 16/11/2004
	 * Obtiene todos las epos-pymes-ifs relacionadas con la (EPO)"-PyMe" Mediana. */
	public ArrayList getEpoPymeIfDeEpoMediana(String sNoEpo, String sNoPyme) throws NafinException {
	ArrayList alEpoPymeIf = new ArrayList();
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select V.ic_epo_pyme_if, V.nombreEpo, V.nombrePyme, V.nombreIf, "   +
							" PP.cs_pignoracion, PP.in_dias_venc_pignora "   +
							" from "   +
							" ( "   +
							"	select distinct(E.ic_epo||'-'||P.ic_pyme||'-'||I.ic_if) as ic_epo_pyme_if, "   +
							"	E.cg_razon_social as nombreEpo, P.cg_razon_social as nombrePyme, I.cg_razon_social as nombreIf, "   +
							"	E.ic_epo as ic_epo, P.ic_pyme as ic_pyme, I.ic_if as ic_if "   +
							"	from comrel_pyme_if PI, comrel_if_epo IE, comrel_cuenta_bancaria CB, "   +
							"	comrel_pyme_epo PE, comcat_epo E, comcat_pyme P, comcat_if I "   +
							"	where IE.ic_if = PI.ic_if "   +
							"	and PI.ic_epo = E.ic_epo "   +
							"	and PI.ic_if = I.ic_if "   +
							"	and PI.ic_cuenta_bancaria = CB.ic_cuenta_bancaria "   +
							"	and CB.ic_pyme = P.ic_pyme "   +
							"	and PE.ic_epo = PI.ic_epo "   +
							"	and PE.ic_pyme = CB.ic_pyme "   +
							"	and IE.ic_epo = ? "   +
							"	and CB.ic_pyme = ? "   +
							"	and PI.cs_vobo_if = 'S' "   +
							"	and PI.cs_borrado = 'N' "   +
							"	and CB.cs_borrado = 'N' "   +
							" ) V, com_param_pignora PP "   +
							" where V.ic_epo = PP.ic_epo(+) "   +
							" and V.ic_pyme = PP.ic_pyme(+) "   +
							" and V.ic_if = PP.ic_if(+) "   +
							" order by V.nombreEpo, V.nombrePyme, V.nombreIf ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoEpo);
			ps.setString(2, sNoPyme);
			try {
				ResultSet rs = ps.executeQuery();
				Vector vecDatos = null;
				while(rs.next()) {
					vecDatos = new Vector();
					vecDatos.addElement(rs.getString("ic_epo_pyme_if"));
					vecDatos.addElement(rs.getString("nombreEpo"));
					vecDatos.addElement(rs.getString("nombrePyme"));
					vecDatos.addElement(rs.getString("nombreIf"));
					vecDatos.addElement((rs.getString("cs_pignoracion")==null?"":rs.getString("cs_pignoracion")));
					vecDatos.addElement((rs.getString("in_dias_venc_pignora")==null?"":rs.getString("in_dias_venc_pignora")));
					alEpoPymeIf.add(alEpoPymeIf.size(), vecDatos);
				}
				rs.close();
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0089");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alEpoPymeIf;
	} // Fin del m�todo getIntermediarioPorEpo()

	/* Creado por Hugoro, Foda 95, 17/11/2004
	 * Realiza la parametrizaci�n de las epos-pymes-ifs seleccionadas. */
	public void setParametrosPignoracion(String[] sAutoPigEPI, String sDiasVenc[],
										String sNoEpoMed) throws NafinException {
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	boolean bOk = true;
	String sQuery = "";
	int iNoRegistros = 0, i=0;
		try {
			con.conexionDB();

			// Esto es para obtener unicamente los d�as para metrizados, por que se env�an todos (con y sin valor).
			ArrayList alDiasVenc = new ArrayList();
			for(i=0; i<sDiasVenc.length; i++) {
				if(sDiasVenc[i]!=null && !sDiasVenc[i].equals(""))
					alDiasVenc.add(alDiasVenc.size(), sDiasVenc[i]);
			}

			try {
				// Por default se deja a todas las parametrizaciones que existen sin poder pignoraci�n.
				sQuery =" update com_param_pignora " +
						" set cs_pignoracion = 'N' " +
						" , in_dias_venc_pignora = null " +
						" where ic_epo is not null " +
						" and ic_pyme is not null " +
						" and ic_if is not null " +
						" and ic_epo_med = "+sNoEpoMed;
				ps = con.queryPrecompilado(sQuery);
				iNoRegistros += ps.executeUpdate();

				// Posteriormente se parametrizan para pignoraci�n c/u de las EPO-PYME-IF seleccionadas.
				VectorTokenizer vt = null;
				for(i=0; i < sAutoPigEPI.length; i++) {
					if(sAutoPigEPI[i]!=null) {
						vt = new VectorTokenizer(sAutoPigEPI[i], "-");
						Vector vecDatos = vt.getValuesVector();
						String sNoEpo = vecDatos.get(0).toString();
						String sNoPyme = vecDatos.get(1).toString();
						String sNoIf = vecDatos.get(2).toString();

						sQuery = "select count(1) from com_param_pignora " +
								" where ic_epo = ? " +
								" and ic_pyme = ? " +
								" and ic_if = ? " ;
						ps = con.queryPrecompilado(sQuery);
						ps.setString(1, sNoEpo);
						ps.setString(2, sNoPyme);
						ps.setString(3, sNoIf);
						ResultSet rs = ps.executeQuery();
						ps.clearParameters();
						if(rs.next()) {
							if(rs.getInt(1)==0) {
								sQuery =" insert into com_param_pignora(cs_pignoracion, in_dias_venc_pignora, "+
										" ic_epo_med, ic_epo, ic_pyme, ic_if) values ( 'S', ?, ?, ?, ?, ? ) ";
							} else {
								sQuery =" update com_param_pignora " +
										" set cs_pignoracion = 'S' " +
										" , in_dias_venc_pignora = ? " +
										" , ic_epo_med = ? " +
										" where ic_epo = ? " +
										" and ic_pyme = ? "+
										" and ic_if = ? ";
							}
							ps = con.queryPrecompilado(sQuery);
							ps.setObject(1, alDiasVenc.get(i));
							ps.setString(2, sNoEpoMed);
							ps.setString(3, sNoEpo);
							ps.setString(4, sNoPyme);
							ps.setString(5, sNoIf);
							iNoRegistros += ps.executeUpdate();
							ps.clearParameters();
						} // if
					} // if
				} // for

			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DSCT0090");
			}

		} catch (NafinException ne) {
			bOk = false;
			throw ne;
		} catch (Exception e) {
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/* Creado por Hugoro, Foda 99, 10/12/2004
	 * Realiza la parametrizaci�n por epo para operar notas de cr�dito. */
	public void setOperaNotasCredito(String sNoEPO, String sOperaNotasCredito,
												int iNoProducto) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOk = true;
		try {
			con.conexionDB();
			try {
				String sQuery = "update comrel_producto_epo "+
								"set cs_opera_notas_cred = ? "+
								"where ic_epo = ? "+
								"and ic_producto_nafin = ? ";
				PreparedStatement ps = con.queryPrecompilado(sQuery);
				ps.setString(1, sOperaNotasCredito);
				ps.setString(2, sNoEPO);
				ps.setInt(3, iNoProducto);
				ps.executeUpdate();
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DSCT0092");
			}

		} catch (NafinException ne) {
			bOk = false;
			throw ne;
		} catch (Exception e) {
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/* Creado por Hugoro, Foda 99, 10/12/2004
	 * Obtiene la parametrizaci�n de la epo para saber si o no opera notas de cr�dito. */
	public String getOperaNotasCredito(String sNoEPO, int iNoProducto) throws NafinException {
	String sOperaNotasCredito = new String("");
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select cs_opera_notas_cred "+
							" from comrel_producto_epo "+
							" where ic_epo = ? "+
							" and ic_producto_nafin = ? ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoEPO);
			ps.setInt(2, iNoProducto);
			try {
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					sOperaNotasCredito = (rs.getString("cs_opera_notas_cred")==null)?"":rs.getString("cs_opera_notas_cred").trim();
				}
				rs.close();
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0093");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return sOperaNotasCredito;
	}


	public ArrayList getParamEPO(String sNoEPO, int iNoProducto) throws NafinException {
		System.out.println("ParametrosDescuentoBean::getParamEPO(E)");
	    AccesoDB con = new AccesoDB();
	    ArrayList alParametros = new ArrayList();
		try {
			con.conexionDB();
			alParametros = getParamEPO(sNoEPO, iNoProducto, con);
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error al obtener Parametros EPO",e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParamEPO(S)");
		}
		return alParametros;
	}	// Fin del metodo getParamEPO

    /**********************/
    /* MPCS FODA 021-2005 */
	/**********************/
	public ArrayList getParamEPO(String sNoEPO, int iNoProducto, AccesoDB con) throws NafinException {
	/* Regresa:
		0.- Opera notas de credito
		1.- Opera con descuento automatico obligatorio
		2.-  Monto m�nimo resultante para la aplicaci�n de Notas de Cr�dito (Agregado Foda 53 Modificaciones Bimbo SMJ 08/08/2005)
		3.-  FODA 44 SMJ 19/06/2006 SE AGREGA LOS PARAMETROS FIRMA MANCOMUNADA Y CRITERIO HASH*/

		System.out.println("ParametrosDescuentoBean::getParamEPO(E)");
	    ArrayList alParametros = new ArrayList();
//	    AccesoDB con = new AccesoDB();
		try {
//			con.conexionDB();
			String sQuery =
				" SELECT cs_opera_notas_cred, cs_dscto_auto_obligatorio, fn_monto_minimo,"   +
				"        cg_descto_dinam, cs_limite_pyme, cs_pub_firma_manc, cs_pub_hash,"   +
				"        cs_fecha_venc_pyme, cs_pub_docto_venc, ig_plazo_pago_fvp,       "   +
				"        ig_plazo_max1_fvp, ig_plazo_max2_fvp, cs_preafiliacion,         "   +
				"			cs_desc_auto_pyme, cs_publicacion_epo_pef                       "   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_producto_nafin = ?"  ;
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoEPO);
			ps.setInt(2, iNoProducto);
			System.out.println(":::Consulta Parametros:::"+ sQuery);
			try {
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					alParametros.add(0,(rs.getString("cs_opera_notas_cred")==null)?"":rs.getString("cs_opera_notas_cred").trim());
					alParametros.add(1,(rs.getString("cs_dscto_auto_obligatorio")==null)?"":rs.getString("cs_dscto_auto_obligatorio").trim());
					alParametros.add(2,(rs.getString("fn_monto_minimo")==null)?"":rs.getString("fn_monto_minimo").trim());
					alParametros.add(3,(rs.getString("cg_descto_dinam")==null)?"":rs.getString("cg_descto_dinam").trim());
					alParametros.add(4,(rs.getString("cs_limite_pyme")==null)?"":rs.getString("cs_limite_pyme").trim()); //Foda 37-SMJ 03/05/2006
					alParametros.add(5,(rs.getString("cs_pub_firma_manc")==null)?"":rs.getString("cs_pub_firma_manc").trim()); //Foda 37-SMJ 03/05/2006
					alParametros.add(6,(rs.getString("cs_pub_hash")==null)?"":rs.getString("cs_pub_hash").trim()); //Foda 44-SMJ 19/06/2006
					alParametros.add(7,(rs.getString("cs_fecha_venc_pyme")==null)?"":rs.getString("cs_fecha_venc_pyme").trim());
					alParametros.add(8,(rs.getString("cs_pub_docto_venc")==null)?"":rs.getString("cs_pub_docto_venc").trim());//Foda 75-ACS 16/01/2007

					alParametros.add(9,(rs.getString("ig_plazo_pago_fvp")==null)?"":rs.getString("ig_plazo_pago_fvp").trim());//Foda 88-ACS 20/02/2007
					alParametros.add(10,(rs.getString("ig_plazo_max1_fvp")==null)?"":rs.getString("ig_plazo_max1_fvp").trim());//Foda 88-ACS 20/02/2007
					alParametros.add(11,(rs.getString("ig_plazo_max2_fvp")==null)?"":rs.getString("ig_plazo_max2_fvp").trim());//Foda 88-ACS 20/02/2007
					alParametros.add(12,(rs.getString("cs_preafiliacion")==null)?"":rs.getString("cs_preafiliacion").trim());//Foda Preafiliacion SMJ 15/05/2007
					alParametros.add(13,(rs.getString("cs_desc_auto_pyme")==null)?"":rs.getString("cs_desc_auto_pyme").trim());//F014 - 2008 JSHD 26/06/2008
					alParametros.add(14,(rs.getString("cs_publicacion_epo_pef")==null)?"":rs.getString("cs_publicacion_epo_pef").trim());//F045 - 2008 JSHD 01/08/2008

				}
				rs.close();
				if(ps!=null) ps.close();


				if(!sNoEPO.equals("")){
					sQuery =
						"SELECT pe.cc_parametro_epo, pz.cg_valor " +
						"  FROM com_parametrizacion_epo pz, comcat_parametro_epo pe " +
						" WHERE pz.cc_parametro_epo(+) = pe.cc_parametro_epo " +
						"and pe.CC_PARAMETRO_EPO in( " +
						"'PUB_EPO_PEF_FECHA_RECEPCION', " +
						"'PUB_EPO_PEF_FECHA_ENTREGA', " +
						"'PUB_EPO_PEF_TIPO_COMPRA', " +
						"'PUB_EPO_PEF_CLAVE_PRESUPUESTAL', " +
						"'PUB_EPO_PEF_PERIODO', " +
						"'PUB_EPO_PEF_USA_SIAFF') "+
						" AND pz.ic_epo(+) = ? ORDER BY 1 ";

					System.out.println(":::Consulta Parametros:2::"+ sQuery);
					System.out.println(":::Bind::"+ sNoEPO);
					ps = con.queryPrecompilado(sQuery);
					ps.setInt(1, Integer.parseInt(sNoEPO));

					rs = ps.executeQuery();
					int k = 15;
					while(rs.next()) {
						alParametros.add(k,(rs.getString("cc_parametro_epo")==null)?"":rs.getString("cc_parametro_epo").trim());
						k++;
						alParametros.add(k,(rs.getString("cg_valor")==null)?"":rs.getString("cg_valor").trim());
						k++;
					}
					rs.close();
					if(ps!=null) ps.close();
				}
					System.out.println("::resultado antes de salir del bean::"+alParametros.toString());
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0095");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error al obtener Parametros EPO",e);
			throw new NafinException("SIST0001");
		} finally {
			//if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParamEPO(S)");
		}
		return alParametros;
	}	// Fin del metodo getParamEPO

  /**********************/
    /* EGB 031-2009 */
	/**********************/
	public Hashtable getParametrosEPO(String sNoEPO, int iNoProducto) throws NafinException {
		 log.info("getParametrosEPO(E)");
	    AccesoDB con = new AccesoDB();
	    Hashtable alParametros = new Hashtable();
		try {
			con.conexionDB();
			alParametros = getParametrosEPO(sNoEPO, iNoProducto, con);
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error al obtener Parametros EPO",e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		log.info("getParametrosEPO(S)");
		return alParametros;
	}	// Fin del metodo getParamEPO

  /**********************/
    /* EGB 031-2009 */
	/**********************/
	public Hashtable getParametrosEPO(String sNoEPO, int iNoProducto, AccesoDB con) throws NafinException {
	/* Regresa:
		0.- Un objeto Hashtable que contiene las llaves y los valores de los valores parametrizados por EPO
  */
    Hashtable alParametros = new Hashtable();
		log.info("getParametrosEPO(E)");
    try {
    String sQuery = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
        sQuery =
          " SELECT " +
          " NVL (CS_FACTORAJE_IF, 'N') AS FACTORAJE_IF, " + // FODEA 026-2012: Agregar Tipo Factoraje Autom�tico
          " NVL(CS_OPERA_NOTAS_CRED,'N') AS OPERA_NOTAS_CRED, " +
          " NVL(CS_DSCTO_AUTO_OBLIGATORIO,'N') AS DSCTO_AUTO_OBLIGATORIO, " +
          " FN_MONTO_MINIMO AS MONTO_MINIMO, "   +
          " NVL(CG_DESCTO_DINAM,'N') AS DESCTO_DINAM, "  +
          " NVL(CS_LIMITE_PYME,'N') AS LIMITE_PYME, " +
          " NVL(CS_PUB_FIRMA_MANC,'N') AS PUB_FIRMA_MANC, " +
          " NVL(CS_PUB_HASH,'N') AS PUB_HASH,"   +
          " NVL(CS_FECHA_VENC_PYME,'N') AS FECHA_VENC_PYME, " +
          " NVL(CS_PUB_DOCTO_VENC,'N') AS PUB_DOCTO_VENC, " +
          " IG_PLAZO_PAGO_FVP AS PLAZO_PAGO_FVP, "   +
          " IG_PLAZO_MAX1_FVP AS PLAZO_MAX1_FVP, " +
          " IG_PLAZO_MAX2_FVP AS PLAZO_MAX2_FVP, " +
          " NVL(CS_PREAFILIACION,'N') AS PREAFILIACION, "   +
          "	NVL(CS_DESC_AUTO_PYME,'N') AS DESC_AUTO_PYME, " +
          " NVL(CS_PUBLICACION_EPO_PEF,'N') AS PUBLICACION_EPO_PEF,  "   +
          "	NVL(CS_FACTORAJE_VENCIDO,'N') AS PUB_EPO_FACTORAJE_VENCIDO, " +
          "	NVL(CS_FACTORAJE_DISTRIBUIDO,'N') AS PUB_EPO_FACTORAJE_DISTRIBUIDO " +
			    //"	,NVL(PUB_EPO_FACT_24HRS,'N') AS PUB_EPO_FACT_24HRS " +
         "	,NVL(cs_factoraje_vencido,'N') AS cs_factoraje_vencido " + //Fodea 018-2010
         " ,CS_FACTORAJE_IF as FACTORAJE_IF "+
			//" ,CS_CONVENIO_UNICO as CS_CONVENIO_UNICO "+
			 " ,CS_OPERA_FIDEICOMISO as CS_OPERA_FIDEICOMISO "+
			 " ,CG_VALIDA_DUPLIC_DOCTO " +  //Fodea 38-2014
			 " ,CG_EMAIL_DUPLIC_DOCTO " +	  //Fodea 38-2014

          "   FROM comrel_producto_epo"   +
          "  WHERE ic_epo = ?"   +
          "    AND ic_producto_nafin = ?"  ;
        ps = con.queryPrecompilado(sQuery);
        ps.setString(1, sNoEPO);
        ps.setInt(2, iNoProducto);
		  log.debug("sNoEPO :::"+sNoEPO);
		  log.debug("iNoProducto :::"+iNoProducto);
        log.debug(":::Consulta Parametros:::"+ sQuery);
				rs = ps.executeQuery();
				if(rs.next()) {
					alParametros.put("OPERA_NOTAS_CRED", rs.getString("OPERA_NOTAS_CRED")==null?"":rs.getString("OPERA_NOTAS_CRED"));
					alParametros.put("DSCTO_AUTO_OBLIGATORIO", rs.getString("DSCTO_AUTO_OBLIGATORIO")==null?"":rs.getString("DSCTO_AUTO_OBLIGATORIO"));
					alParametros.put("DESCTO_DINAM", rs.getString("DESCTO_DINAM")==null?"":rs.getString("DESCTO_DINAM"));
					alParametros.put("LIMITE_PYME", rs.getString("LIMITE_PYME")==null?"":rs.getString("LIMITE_PYME"));
					alParametros.put("PUB_FIRMA_MANC", rs.getString("PUB_FIRMA_MANC")==null?"":rs.getString("PUB_FIRMA_MANC"));
					alParametros.put("PUB_HASH", rs.getString("PUB_HASH")==null?"":rs.getString("PUB_HASH"));
					alParametros.put("FECHA_VENC_PYME", rs.getString("FECHA_VENC_PYME")==null?"":rs.getString("FECHA_VENC_PYME"));
					alParametros.put("PUB_DOCTO_VENC", rs.getString("PUB_DOCTO_VENC")==null?"":rs.getString("PUB_DOCTO_VENC"));
					alParametros.put("PREAFILIACION", rs.getString("PREAFILIACION")==null?"":rs.getString("PREAFILIACION"));
					alParametros.put("DESC_AUTO_PYME", rs.getString("DESC_AUTO_PYME")==null?"":rs.getString("DESC_AUTO_PYME"));
					alParametros.put("PUBLICACION_EPO_PEF", rs.getString("PUBLICACION_EPO_PEF")==null?"":rs.getString("PUBLICACION_EPO_PEF"));
					alParametros.put("MONTO_MINIMO", rs.getString("MONTO_MINIMO")==null?"":rs.getString("MONTO_MINIMO"));
					alParametros.put("PLAZO_PAGO_FVP", rs.getString("PLAZO_PAGO_FVP")==null?"":rs.getString("PLAZO_PAGO_FVP"));
					alParametros.put("PLAZO_MAX1_FVP", rs.getString("PLAZO_MAX1_FVP")==null?"":rs.getString("PLAZO_MAX1_FVP"));
					alParametros.put("PLAZO_MAX2_FVP", rs.getString("PLAZO_MAX2_FVP")==null?"":rs.getString("PLAZO_MAX2_FVP"));
					alParametros.put("PUB_EPO_FACTORAJE_VENCIDO", rs.getString("PUB_EPO_FACTORAJE_VENCIDO")==null?"":rs.getString("PUB_EPO_FACTORAJE_VENCIDO"));
					alParametros.put("PUB_EPO_FACTORAJE_DISTRIBUIDO", rs.getString("PUB_EPO_FACTORAJE_DISTRIBUIDO")==null?"":rs.getString("PUB_EPO_FACTORAJE_DISTRIBUIDO"));
					alParametros.put("cs_factoraje_vencido", rs.getString("cs_factoraje_vencido")==null?"":rs.getString("cs_factoraje_vencido"));//fodea 018-2010
					alParametros.put("FACTORAJE_IF", rs.getString("FACTORAJE_IF")==null?"":rs.getString("FACTORAJE_IF")); // FODEA 026-2012: Agregar Tipo Factoraje Autom�tico
					//alParametros.put("CS_CONVENIO_UNICO", rs.getString("CS_CONVENIO_UNICO")==null?"":rs.getString("CS_CONVENIO_UNICO")); // FODEA 010-2013
					alParametros.put("CS_OPERA_FIDEICOMISO", rs.getString("CS_OPERA_FIDEICOMISO")==null?"":rs.getString("CS_OPERA_FIDEICOMISO")); // FODEA 017-2013
					alParametros.put("VALIDA_DUPLIC", rs.getString("CG_VALIDA_DUPLIC_DOCTO")==null?"":rs.getString("CG_VALIDA_DUPLIC_DOCTO")); // FODEA 38-2014
					alParametros.put("EMAIL_DUPLIC", rs.getString("CG_EMAIL_DUPLIC_DOCTO")==null?"":rs.getString("CG_EMAIL_DUPLIC_DOCTO")); // FODEA 38-2014

				}
				rs.close();
				if(ps!=null) ps.close();

				if(!sNoEPO.equals("")){
					sQuery =
						"SELECT pe.cc_parametro_epo, NVL(pz.cg_valor, 'N') CG_VALOR " +
						"  FROM com_parametrizacion_epo pz, comcat_parametro_epo pe " +
						" WHERE pz.cc_parametro_epo(+) = pe.cc_parametro_epo " +
						" AND pz.ic_epo(+) = ? ORDER BY 1 ";

					log.debug(":::Consulta Parametros:2::"+ sQuery);
					log.debug(":::Bind::"+ sNoEPO);
					ps = con.queryPrecompilado(sQuery);
					ps.setInt(1, Integer.parseInt(sNoEPO));

					rs = ps.executeQuery();
					while(rs.next()) {
						alParametros.put(rs.getString("cc_parametro_epo").trim(), rs.getString("cg_valor"));
					}
					rs.close();
					if(ps!=null) ps.close();
				}
					log.debug("::resultado antes de salir del bean::"+alParametros.toString());
    } catch(SQLException sqle) {
      sqle.printStackTrace();
      throw new NafinException("DSCT0095");
		} catch (Exception e) {
      e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			//if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		log.info("getParametrosEPO(S)");
		return alParametros;
	}	// Fin del metodo getParametrosEPO

/*----------------------------------------------------------------------------*/
	/**
	 * Este m�todo guarda las opciones seleccionadas de "Par�metros por EPO", esta basado en setParamEPO,
	 * la diferencia es el manejo de Hashtable, eliminando asi el uso de posiciones para las sentencias sql.
	 * @author Ivan Almaguer
	 * @param sNoEPO	Clave de la EPO
	 * @param alParametros Parametros seleccionados de "Par�metros por EPO"
	 * @param iNoProducto Numero de producto al que esta accesando
	 * @param claveUsuario Clave del usuario
	 * @since FODEA-023/2009 Factoraje con Mandato
	 * @throws NafinException Cuando ocurre alg�n error en las sentencias sql.
	 */

		public void setParametrosEPO(String sNoEPO, Hashtable alParametros, int iNoProducto, String claveUsuario) throws NafinException {
	/* Almacena los datos de los par�metros e invalida la parametrizacion de la PyME
	   para obligar a autorizar descuento automatico */
	log.info("setParametrosEPO(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	String sQuery = "";
	ResultSet rs = null;
	boolean bOk = true;
		try {
			con.conexionDB();
			String[] nombres = {
				"Opera Notas de Credito",
				"Monto Minimo para aplicacion de NC",
				"Descuento Automatico Obligatorio",
				"Descuento Automatico Dinamico",
				"Opera limite de linea por PyME",
				"Publicacion con Firma Mancomunada",
				"Publicacion con Criterio Hash",
				"Opera Fecha Vencimiento Proveedor",
				"Plazo Dias Maximos adicionales para el Proveedor",
				"Plazo Pyme y Documento Menor al Parametro 8.1",
				"Plazo Pyme y Documento Mayor al Parametro 8.1",
				"Publicacion Documentos Vencidos",
				"Opera Pre-Afiliacion a Descuento PYMES",
				"Permitir Descuento Automatico a PYMES",
				"Publicacion EPO PEF (Clave SIAFF)",
				"Utiliza SIAFF",
				"Publicacion EPO PEF",
				"Fecha de Recepcion de Bienes y Servicios",
				"Tipo de Compra (procedimiento)",
				"Clasificador por Objeto del Gasto",
				"Plazo Maximo",
				"Opera Factoraje con Mandato",
				"Opera Factoraje Vencimiento Infonavit",
				"Opera Solicitud de Consentimiento Cesion de derechos",
				"Opera Factoraje Vencido",
				"Opera Factoraje Distribuido",
				"Opera Factoraje 24 hrs",
				"Aplicar una Nota de Cr�dito a varios Documentos",
				"Descuento Automatico Factoraje Vencido",
				"Descuento Automatico al Vencimiento",  //FODEA 024-2010 FVR
				//"Factoraje Distribuido",//FODEA 032-2010 FVR
				"Autorizaci�n cuentas bancarias de entidades",//FODEA 032-2010 FVR
				"Opera Tasas Especiales",	// Fodea 036 - 2010
				"Tipo Tasa", 					// Fodea 036 - 2010
				"Hora Envio Operaciones IF",			// Fodea 026 - 2011
				"Mostrar Fecha de Autorizacion IF en Info. de Documentos", // Fodea 040 - 2011
				"Factoraje IF",//FODEA 026-2012
				"Convenio Unico ",//FODEA 026-2012
				"Opera Fideicomiso de Desarrollo de Proveedores ",//FODEA 017-2013
				"Tasa Contraprestaci�n (pago a NAFIN)  ",//FODEA 017-2013
				"Validar duplicidad de documentos por monto ",//FODEA 016-2015
				"Correo(S) a notificar "//FODEA 016-2015
			};

			Hashtable datosAnteriores = new Hashtable();
			Hashtable datosActuales = new Hashtable();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer();

			datosAnteriores = getParametrosEPO(sNoEPO, iNoProducto, con);

			String iNoNafin = "";
			sQuery =
				" SELECT ic_nafin_electronico"   +
				"   FROM comrel_nafin"   +
				"  WHERE ic_epo_pyme_if = ?"   +
				"    AND cg_tipo = 'E'"  ;

			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(sNoEPO));
			rs = ps.executeQuery();
			if(rs.next()) {
				iNoNafin = (rs.getString(1)==null)?"":rs.getString(1);
			}
			rs.close();ps.close();



		    if (alParametros == null || alParametros.size()<3 ) {
				log.info("ERROR::ParametrosDescuentoBean::setParamEPO::No se proporcionaron los parametros a almacenar.");
				throw new NafinException("DSCT0096");
			}

			try {
				sQuery =
					" UPDATE comrel_producto_epo"   +
					"    SET cs_opera_notas_cred = ?,"   +
					"        cs_dscto_auto_obligatorio = ?,"   +
					"        fn_monto_minimo = ? ,"   +
					"        cg_descto_dinam = ? ,"   +
					"        cs_limite_pyme = ?,"   +  //Foda 37-SMJ 03/05/2006
					"        cs_pub_firma_manc = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_pub_hash = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_fecha_venc_pyme = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_pub_docto_venc = ?,"   +  //Foda 75-ACS 16/01/2007
					"        ig_plazo_pago_fvp = ?,"   +  //Foda 88- 22/02/2007
					"        ig_plazo_max1_fvp = ?,"   +  //Foda 88- 22/02/2007
					"        ig_plazo_max2_fvp = ?,"   +  //Foda 88- 22/02/2007
					"		   cs_preafiliacion=?, " +  //Foda AfiliacionElectronica- SMJ 15/05/2007
					"			cs_desc_auto_pyme=?, "+ // Fodea 14 - 2008 - JSHD
					"			cs_publicacion_epo_pef=?, "+ // Fodea 45 - 2008 - JSHD
					"     	cs_factoraje_vencido = ?, "+ // Fodea 018 - 2010
					"			cs_factoraje_distribuido = ?, "+// Fodea 032-2010 FVR
					"			cs_factoraje_if = ?,  "+// Fodea 026-2012
					" 			cs_convenio_unico = ?,  "+ //Fodea 010-2013
					" 			CS_OPERA_FIDEICOMISO = ?, "+ //Fodea 017-2013
					" 			CG_VALIDA_DUPLIC_DOCTO = ?, "+ //Fodea 38-2014
					" 			CG_EMAIL_DUPLIC_DOCTO = ? "+ //Fodea 38-2014
					"  WHERE ic_epo = ?"   +
					"    AND ic_producto_nafin = ?"  ;

				List lVarBind = new ArrayList();

				lVarBind.add((String)alParametros.get("OPERA_NOTAS_CRED"));
				lVarBind.add((String)alParametros.get("DSCTO_AUTO_OBLIGATORIO"));

				String auxMontoMinimo = alParametros.get("MONTO_MINIMO").toString().trim();
				if(!"".equals(auxMontoMinimo)) {
					lVarBind.add(auxMontoMinimo);
				}else  {
					lVarBind.add("");
				}
				String auxDesctoDinam = alParametros.get("DESCTO_DINAM").toString().trim();
				if(!"".equals(auxDesctoDinam)) {
					lVarBind.add(auxDesctoDinam);
				}else  {
					lVarBind.add("");
				}

				lVarBind.add((String)alParametros.get("LIMITE_PYME")); 	//Foda 37-SMJ 03/05/2006
				lVarBind.add((String)alParametros.get("PUB_FIRMA_MANC")); 	//Foda 44-SMJ 16/06/2006
				lVarBind.add((String)alParametros.get("PUB_HASH")); 	//Foda 44-SMJ 16/06/2006
				lVarBind.add((String)alParametros.get("FECHA_VENC_PYME")); 	//Foda 44-SMJ 16/06/2006

				String auxPubDoctosVenc = alParametros.get("PUB_DOCTO_VENC").toString().trim();
				if(!"".equals(auxPubDoctosVenc)) {
					lVarBind.add(auxPubDoctosVenc);
				}else  {
					lVarBind.add("");
				} //Foda 75-ACS 16/01/2007

				String auxDiasMax = alParametros.get("PLAZO_PAGO_FVP").toString().trim();
				if(!"".equals(auxDiasMax)) {
					lVarBind.add(auxDiasMax);
				}else  {
					lVarBind.add("");
				}

				String auxPlazo1 = alParametros.get("PLAZO_MAX1_FVP").toString().trim();
				if(!"".equals(auxPlazo1)) {
					lVarBind.add(auxPlazo1);
				}else  {
					lVarBind.add("");
				}

				String auxPlazo2 = alParametros.get("PLAZO_MAX2_FVP").toString().trim();
				if(!"".equals(auxPlazo2)) {
					lVarBind.add(auxPlazo2);
				}else  {
					lVarBind.add("");
				}

				lVarBind.add((String)alParametros.get("PREAFILIACION")); 	//Foda AfiliacionElectronica- SMJ 15/05/2007
				lVarBind.add((String)alParametros.get("DESC_AUTO_PYME")); 	//F014 - 2008 JSHD
				lVarBind.add((String)alParametros.get("PUBLICACION_EPO_PEF")); 	//F045 - 2008 JSHD
				lVarBind.add((String)alParametros.get("cs_factoraje_vencido")); 	//F018 - 2010
				lVarBind.add((String)alParametros.get("PUB_EPO_FACTORAJE_DISTRIBUIDO")); //F032-2010 FVR
				lVarBind.add((String)alParametros.get("CS_FACTORAJE_IF")); //F026-2012
				lVarBind.add((String)alParametros.get("CS_CONVENIO_UNICO")); //F026-2012
				lVarBind.add((String)alParametros.get("CS_OPERA_FIDEICOMISO")); //F017-2013
				lVarBind.add((String)alParametros.get("VALIDA_DUPLIC")); //Fodea 38-2014
				lVarBind.add((String)alParametros.get("EMAIL_DUPLIC")); //Fodea 38-2014

				lVarBind.add(sNoEPO);
				lVarBind.add(String.valueOf(iNoProducto));

				System.out.println(":::Update Parametros sQuery:::"+ sQuery);
				System.out.println(":::Update Parametros lVarBind:::"+ lVarBind);

				ps = con.queryPrecompilado(sQuery, lVarBind);
				int iNoRegistros = ps.executeUpdate();
				ps.clearParameters();
				// Actualiza la informaci�n de las pymes si el descuento es obligatorio
				if ("S".equals((String)alParametros.get("DSCTO_AUTO_OBLIGATORIO"))) {
					sQuery =
						" UPDATE comcat_pyme p"   +
						"    SET cs_dscto_automatico = 'S'"   +
						"  WHERE p.ic_pyme IN ("   +
						"           SELECT cb.ic_pyme"   +
						"             FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb"   +
						"            WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
						"              AND pi.cs_borrado = 'N'"   +
						"              AND cb.cs_borrado = 'N'"   +
						"              AND pi.cs_vobo_if = 'S'"   +
						"              AND pi.cs_opera_descuento = 'S'"   +
						"              AND pi.ic_epo = ?)"   +
						"    AND cs_dscto_automatico = 'N'"  ;
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					iNoRegistros = ps.executeUpdate();
				}
				if(ps!=null) ps.close();


				//Siempre borrar los checks (borrar lo anterior y solo grabar lo nuevo, si l flag es N se queda borrado)
				sQuery = " DELETE com_parametrizacion_epo Where "+
				" CC_PARAMETRO_EPO in( " +
				"'PUB_EPO_PEF_FECHA_RECEPCION', " +
				"'PUB_EPO_PEF_FECHA_ENTREGA', " +
				"'PUB_EPO_PEF_TIPO_COMPRA', " +
				"'PUB_EPO_OPERA_MANDATO', "+
				"'PUB_EPO_VENC_INFONAVIT', "+
				"'PUB_EPO_PEF_CLAVE_PRESUPUESTAL', " +
				"'OPER_SOLIC_CONS_CDER', " +
				"'PUB_EPO_PEF_PERIODO', " +
				"'PUB_EPO_PEF_USA_SIAFF',"+//MODIFICACION F049-2008
				"'PUB_EPO_FACT_24HRS', "+
        "'PUB_EPO_DESCAUTO_FACTVENCIDO',"+// F018-2010
				"'CS_APLICAR_NOTAS_CRED',"+
				"'DESC_AUT_ULTIMO_DIA', "+//FODEA 024-2010 FVR
				//"'OPER_FACTORAJE_DIST', " +//FODEA 032-2010 FVR
				"'AUTORIZA_CTAS_BANC_ENTIDADES', "+//FODEA 032-2010 FVR
				"'OPER_TASAS_ESPECIALES', "+
				"'TIPO_TASA', " +
				"'HORA_ENV_OPE_IF', " +
				"'CS_SHOW_FCHA_AUT_IF_INFO_DCTOS', " + // FODEA 040 - 2011 BY JSHD
				"'FACTORAJE_IF',  " + // FODEA 026 - 2012
				"'CS_CONVENIO_UNICO', " + // FODEA 010 - 2013
				"'CS_OPERA_FIDEICOMISO',  " + // FODEA 017 - 2013
				"'TASA_OPERACION' " + // FODEA 017 - 2013
				") "+
				" AND IC_EPO = " + sNoEPO;
				ps = con.queryPrecompilado(sQuery);
				iNoRegistros = ps.executeUpdate();

        log.debug(
         "\n  13.- PUB_EPO_PEF_FECHA_RECEPCION: "+alParametros.get("PUB_EPO_PEF_FECHA_RECEPCION")
        +"\n13.1.- PUB_EPO_PEF_FECHA_ENTREGA: "+alParametros.get("PUB_EPO_PEF_FECHA_ENTREGA")
        +"\n13.2.- PUB_EPO_PEF_TIPO_COMPRA: "+alParametros.get("PUB_EPO_PEF_TIPO_COMPRA")
        +"\n13.3.- PUB_EPO_PEF_CLAVE_PRESUPUESTAL: "+alParametros.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL")
        +"\n13.4.- PUB_EPO_PEF_PERIODO: "+alParametros.get("PUB_EPO_PEF_PERIODO")
        +"\n");

				//Insertar los checks si la flag es S
				log.debug("Insertar los checks si la flag es S");
				if(((String)alParametros.get("PUB_EPO_PEF_FECHA_RECEPCION")).equals("S")){

					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_RECEPCION");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_FECHA_RECEPCION") );
					iNoRegistros = ps.executeUpdate();

					log.debug("insert 1");

					ps.clearParameters();
					//guardando docto 1 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_ENTREGA");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_FECHA_ENTREGA") );
					iNoRegistros = ps.executeUpdate();

					log.debug("insert 2");

					ps.clearParameters();
					//guardando docto 2 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_TIPO_COMPRA");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_TIPO_COMPRA") );
					iNoRegistros = ps.executeUpdate();

					log.debug("insert 3");

						ps.clearParameters();
					//guardando docto 3 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_CLAVE_PRESUPUESTAL");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL") );
					iNoRegistros = ps.executeUpdate();

					log.debug("insert 4");

					ps.clearParameters();
					//guardando docto 4 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_PERIODO");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_PERIODO") );
					iNoRegistros = ps.executeUpdate();

					log.debug("insert 5");

				}else{
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_RECEPCION");
					ps.setString(3, "N" );
					iNoRegistros = ps.executeUpdate();
				}

				if(((String)alParametros.get("PUB_EPO_OPERA_MANDATO")).equals("S")){
				ps.clearParameters();
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_OPERA_MANDATO");
					ps.setString(3, (String)alParametros.get("PUB_EPO_OPERA_MANDATO") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 6");
				}

				if(((String)alParametros.get("PUB_EPO_VENC_INFONAVIT")).equals("S")){
				ps.clearParameters();
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_VENC_INFONAVIT");
					ps.setString(3, (String)alParametros.get("PUB_EPO_VENC_INFONAVIT") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 7");
				}

				if(((String)alParametros.get("OPER_SOLIC_CONS_CDER")).equals("S")){
				ps.clearParameters();
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "OPER_SOLIC_CONS_CDER");
					ps.setString(3, (String)alParametros.get("OPER_SOLIC_CONS_CDER") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 8");
				}


				if(((String)alParametros.get("PUB_EPO_PEF_USA_SIAFF")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_USA_SIAFF");
					ps.setString(3, (String)alParametros.get("PUB_EPO_PEF_USA_SIAFF") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 9");
				}

				//FODEA 016-2010 FVR
				if(alParametros.get("PUB_EPO_FACT_24HRS")!=null &&
				(((String)alParametros.get("PUB_EPO_FACT_24HRS")).equals("S") || ((String)alParametros.get("PUB_EPO_FACT_24HRS")).equals("N") )){
				ps.clearParameters();
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_FACT_24HRS");
					ps.setString(3, (String)alParametros.get("PUB_EPO_FACT_24HRS") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 10");
				}

				// Actualizar lista de parametros
				if(((String)alParametros.get("CS_APLICAR_NOTAS_CRED")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "CS_APLICAR_NOTAS_CRED");
					ps.setString(3, (String)alParametros.get("CS_APLICAR_NOTAS_CRED") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 11");
				}

        //Fodea 018-2010
        if(((String)alParametros.get("PUB_EPO_DESCAUTO_FACTVENCIDO")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_DESCAUTO_FACTVENCIDO");
					ps.setString(3, (String)alParametros.get("PUB_EPO_DESCAUTO_FACTVENCIDO") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 18");
				}

			//FODEA 024-2010 FVR
        if(((String)alParametros.get("DESC_AUT_ULTIMO_DIA")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "DESC_AUT_ULTIMO_DIA");
					ps.setString(3, (String)alParametros.get("DESC_AUT_ULTIMO_DIA") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 19");
				}

			//FODEA 032-2010 FVR
			/*
			if(((String)alParametros.get("OPER_FACTORAJE_DIST")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "OPER_FACTORAJE_DIST");
					ps.setString(3, (String)alParametros.get("OPER_FACTORAJE_DIST") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 20");
				}
			*/

			//FODEA 032-2010 FVR
			if(((String)alParametros.get("AUTORIZA_CTAS_BANC_ENTIDADES")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "AUTORIZA_CTAS_BANC_ENTIDADES");
					ps.setString(3, (String)alParametros.get("AUTORIZA_CTAS_BANC_ENTIDADES") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 20.1");
				}

				// Fodea 036 - 2010
        System.err.println(" OPER_TASAS_ESPECIALES = <"+alParametros.get("OPER_TASAS_ESPECIALES")+">");
				if(((String)alParametros.get("OPER_TASAS_ESPECIALES")).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "OPER_TASAS_ESPECIALES");
					ps.setString(3, (String)alParametros.get("OPER_TASAS_ESPECIALES") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 21");
				}

				// Fodea 036 - 2010
        System.out.println("TIPO_TASA = <"+alParametros.get("TIPO_TASA")+">");
				if( ((String)alParametros.get("TIPO_TASA")).equals("P") || ((String)alParametros.get("TIPO_TASA")).equals("NG") || ((String)alParametros.get("TIPO_TASA")).equals("A")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "TIPO_TASA");
					ps.setString(3, (String)alParametros.get("TIPO_TASA") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 21.1");
				}

				// Fodea 026-2011
        System.err.println(" HORA_ENV_OPE_IF = <"+alParametros.get("HORA_ENV_OPE_IF")+">");
				if(!((String)alParametros.get("HORA_ENV_OPE_IF")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "HORA_ENV_OPE_IF");
					ps.setString(3, (String)alParametros.get("HORA_ENV_OPE_IF") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 22");
				}

				// Fodea 040 - 2011
			System.err.println(" CS_SHOW_FCHA_AUT_IF_INFO_DCTOS = <"+alParametros.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")+">");
				if(!((String)alParametros.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "CS_SHOW_FCHA_AUT_IF_INFO_DCTOS");
					ps.setString(3, (String)alParametros.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 23");
				}

				// Fodea 026 - 2012
			System.err.println("FACTORAJE_IF = <"+alParametros.get("CS_FACTORAJE_IF")+">");
				if(!((String)alParametros.get("CS_FACTORAJE_IF")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					System.out.println("sQuery" + sQuery);
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "FACTORAJE_IF");
					ps.setString(3, (String)alParametros.get("CS_FACTORAJE_IF") );

					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 24");
				}

						// Fodea 010 - 2013
			System.err.println("CS_CONVENIO_UNICO = <"+alParametros.get("CS_CONVENIO_UNICO")+">");
				if(!((String)alParametros.get("CS_CONVENIO_UNICO")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					System.out.println("sQuery" + sQuery);
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "CS_CONVENIO_UNICO");
					ps.setString(3, (String)alParametros.get("CS_CONVENIO_UNICO") );

					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 25");
				}
				// Fodea 017 - 2013
				System.err.println("CS_OPERA_FIDEICOMISO = <"+alParametros.get("CS_OPERA_FIDEICOMISO")+">");
				if(!((String)alParametros.get("CS_OPERA_FIDEICOMISO")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					System.out.println("sQuery" + sQuery);
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "CS_OPERA_FIDEICOMISO");
					ps.setString(3, (String)alParametros.get("CS_OPERA_FIDEICOMISO") );

					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 26");
				}

				if(!((String)alParametros.get("TASA_OPERACION")).equals("")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					System.out.println("sQuery" + sQuery);
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "TASA_OPERACION");
					ps.setString(3, (String)alParametros.get("TASA_OPERACION") );

					iNoRegistros = ps.executeUpdate();
					log.debug("insert parametro 26.1");
				}


			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DSCT0096");
			}

			datosActuales = getParametrosEPO(sNoEPO, iNoProducto, con);
			log.debug("\n\ndatosAnteriores : "+datosAnteriores.toString());
			log.debug("\n\ndatosActuales : "+datosActuales);
			log.debug("\n\n");


			List datosAnterioresList = new ArrayList();
			List datosActualesList = new ArrayList();


			/*
			 * Se obtienen los registros de las Hashtable datosAnteriores y datosActuales se van guardando los
			 * valores en las listas datosAnterioresList y datosActualesList para enviarlas al
			 * metodo getCambiosBitacora y se hagan las comparaciones de que parametro fue modificado.
			 */
			datosAnterioresList.add((String)datosAnteriores.get("OPERA_NOTAS_CRED"));
			  datosActualesList.add((String)datosActuales.get("OPERA_NOTAS_CRED"));

			datosAnterioresList.add((String)datosAnteriores.get("MONTO_MINIMO"));
			  datosActualesList.add((String)datosActuales.get("MONTO_MINIMO"));

			datosAnterioresList.add((String)datosAnteriores.get("DSCTO_AUTO_OBLIGATORIO"));
			  datosActualesList.add((String)datosActuales.get("DSCTO_AUTO_OBLIGATORIO"));

			datosAnterioresList.add((String)datosAnteriores.get("DESCTO_DINAM"));
			  datosActualesList.add((String)datosActuales.get("DESCTO_DINAM"));

			datosAnterioresList.add((String)datosAnteriores.get("LIMITE_PYME"));
			  datosActualesList.add((String)datosActuales.get("LIMITE_PYME"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_FIRMA_MANC"));
			  datosActualesList.add((String)datosActuales.get("PUB_FIRMA_MANC"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_HASH"));
			  datosActualesList.add((String)datosActuales.get("PUB_HASH"));

			datosAnterioresList.add((String)datosAnteriores.get("FECHA_VENC_PYME"));
			  datosActualesList.add((String)datosActuales.get("FECHA_VENC_PYME"));

			datosAnterioresList.add((String)datosAnteriores.get("PLAZO_PAGO_FVP"));
			  datosActualesList.add((String)datosActuales.get("PLAZO_PAGO_FVP"));

			datosAnterioresList.add((String)datosAnteriores.get("PLAZO_MAX1_FVP"));
			  datosActualesList.add((String)datosActuales.get("PLAZO_MAX1_FVP"));

			datosAnterioresList.add((String)datosAnteriores.get("PLAZO_MAX2_FVP"));
			  datosActualesList.add((String)datosActuales.get("PLAZO_MAX2_FVP"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_DOCTO_VENC"));
			  datosActualesList.add((String)datosActuales.get("PUB_DOCTO_VENC"));

			datosAnterioresList.add((String)datosAnteriores.get("PREAFILIACION"));
			  datosActualesList.add((String)datosActuales.get("PREAFILIACION"));

			datosAnterioresList.add((String)datosAnteriores.get("DESC_AUTO_PYME"));
			  datosActualesList.add((String)datosActuales.get("DESC_AUTO_PYME"));

			datosAnterioresList.add((String)datosAnteriores.get("PUBLICACION_EPO_PEF"));
			  datosActualesList.add((String)datosActuales.get("PUBLICACION_EPO_PEF"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_USA_SIAFF"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_USA_SIAFF"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_FECHA_RECEPCION"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_FECHA_RECEPCION"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_FECHA_ENTREGA"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_FECHA_ENTREGA"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_TIPO_COMPRA"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_TIPO_COMPRA"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_PEF_PERIODO"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_PEF_PERIODO"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_OPERA_MANDATO"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_OPERA_MANDATO"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_VENC_INFONAVIT"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_VENC_INFONAVIT"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_FACTORAJE_DISTRIBUIDO"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_FACTORAJE_DISTRIBUIDO"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_FACTORAJE_VENCIDO"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_FACTORAJE_VENCIDO"));

			datosAnterioresList.add((String)datosAnteriores.get("OPER_SOLIC_CONS_CDER"));
			  datosActualesList.add((String)datosActuales.get("OPER_SOLIC_CONS_CDER"));

			datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_FACT_24HRS"));
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_FACT_24HRS"));

			datosAnterioresList.add((String)datosAnteriores.get("CS_APLICAR_NOTAS_CRED"));
			  datosActualesList.add((String)datosActuales.get("CS_APLICAR_NOTAS_CRED"));

         datosAnterioresList.add((String)datosAnteriores.get("PUB_EPO_DESCAUTO_FACTVENCIDO")); //Fodea 018-2010
			  datosActualesList.add((String)datosActuales.get("PUB_EPO_DESCAUTO_FACTVENCIDO"));

			datosAnterioresList.add((String)datosAnteriores.get("DESC_AUT_ULTIMO_DIA")); //FODEA 024-2010
			  datosActualesList.add((String)datosActuales.get("DESC_AUT_ULTIMO_DIA"));

			//datosAnterioresList.add((String)datosAnteriores.get("OPER_FACTORAJE_DIST")); //FODEA 032-2010 FVR
			  //datosActualesList.add((String)datosActuales.get("OPER_FACTORAJE_DIST"));

			datosAnterioresList.add((String)datosAnteriores.get("AUTORIZA_CTAS_BANC_ENTIDADES")); //FODEA 032-2010 FVR
			  datosActualesList.add((String)datosActuales.get("AUTORIZA_CTAS_BANC_ENTIDADES"));

			datosAnterioresList.add((String)datosAnteriores.get("OPER_TASAS_ESPECIALES")); //FODEA 036 - 2010 JSHD
			  datosActualesList.add((String)datosActuales.get("OPER_TASAS_ESPECIALES"));

			 datosAnterioresList.add((String)datosAnteriores.get("TIPO_TASA")); //FODEA 036 - 2010 JSHD
			  datosActualesList.add((String)datosActuales.get("TIPO_TASA"));

			datosAnterioresList.add((String)datosAnteriores.get("HORA_ENV_OPE_IF")); //FODEA 026-2011
			  datosActualesList.add((String)datosActuales.get("HORA_ENV_OPE_IF"));

			datosAnterioresList.add((String)datosAnteriores.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")); //FODEA 040-2011
			  datosActualesList.add((String)datosActuales.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS"));

			datosAnterioresList.add((String)datosAnteriores.get("FACTORAJE_IF")); //FODEA 06-2012
			  datosActualesList.add((String)datosActuales.get("FACTORAJE_IF"));
			datosAnterioresList.add((String)datosAnteriores.get("CS_CONVENIO_UNICO")); //FODEA 010-2013
			datosActualesList.add((String)datosActuales.get("CS_CONVENIO_UNICO"));
			datosAnterioresList.add((String)datosAnteriores.get("CS_OPERA_FIDEICOMISO")); //FODEA 017-2013
			datosActualesList.add((String)datosActuales.get("CS_OPERA_FIDEICOMISO"));
			datosAnterioresList.add((String)datosAnteriores.get("TASA_OPERACION")); //FODEA 017-2013
			datosActualesList.add((String)datosActuales.get("TASA_OPERACION"));

			datosAnterioresList.add((String)datosAnteriores.get("VALIDA_DUPLIC")); //FODEA 016-2015
			datosActualesList.add((String)datosActuales.get("VALIDA_DUPLIC"));
			datosAnterioresList.add((String)datosAnteriores.get("EMAIL_DUPLIC")); //FODEA 016-2015
			datosActualesList.add((String)datosActuales.get("EMAIL_DUPLIC"));


			log.debug("\n");
			log.debug("Numero de nombres :::: "+nombres.length);//FODEA 016-2010 PENDIENTE REGISTRO EN BITACORA
			log.debug("Numero datosAnterioresList :::: "+datosAnterioresList.size());
			log.debug("Numero datosActualesList   :::: "+datosActualesList.size());
			log.debug("datosAnterioresList :::: "+datosAnterioresList);
			log.debug("datosActualesList  :::: "+datosActualesList);
			log.debug("\n");

			List cambios =	Bitacora.getCambiosBitacora(datosAnterioresList, datosActualesList, nombres);

			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));

			Bitacora.grabarEnBitacora(con, "PARAMDSCTO", "E",
			String.valueOf(iNoNafin), claveUsuario,
			valoresAnteriores.toString(), valoresActuales.toString());

		} catch (NafinException ne) {
			ne.printStackTrace();
			bOk = false;
			throw ne;
		} catch (Exception e) {
			e.printStackTrace();
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		log.info("setParametrosEPO(S)");
	} // Fin del metodo setParametrosEPO
/*---------------------------------------------------------------------------*/

	public void setParamEPO(String sNoEPO, ArrayList alParametros, int iNoProducto, String claveUsuario) throws NafinException {
	/* Almacena los datos de los par�metros e invalida la parametrizacion de la PyME
	   para obligar a autorizar descuento automatico */
	System.out.println("ParametrosDescuentoBean::setParamEPO(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	String sQuery = "";
	ResultSet rs = null;
	boolean bOk = true;
		try {
			con.conexionDB();
			//--------------------------------
			String[] nombres = {
				"Opera Notas de Cr�dito", "Monto M�nimo para aplicacion de NC",
				"Descuento Autom�tico Obligatorio", "Descuento Autom�tico Din�mico", "Opera l�mite de l�nea por PyME",
				"Publicaci�n con Firma Mancomunada","Publicacion con Criterio Hash", "Opera Fecha Vencimiento PyME",
				"Plazo D�as M�ximos adicionales para la Pyme", "Plazo Pyme y Documento Menor al Parametro 8.1",
				"Plazo Pyme y Documento Menor al Parametro 8.1", "Publicacion Documentos Vencidos", "Opera Pre-Afiliaci�n a Descuento PYMES",
				"Permitir Descuento Autom�tico a PYMES", "Publicaci�n EPO PEF (Clave SIAFF)",
				"PUB_EPO_PEF_CLAVE_PRESUPUESTAL", "PUB_EPO_PEF_CLAVE_PRESUPUESTAL",
				"PUB_EPO_PEF_FECHA_ENTREGA", "PUB_EPO_PEF_FECHA_ENTREGA",
				"PUB_EPO_PEF_FECHA_RECEPCION", " PUB_EPO_PEF_FECHA_RECEPCION",
				"PUB_EPO_PEF_PERIODO", " PUB_EPO_PEF_PERIODO",
				"PUB_EPO_PEF_TIPO_COMPRA", " PUB_EPO_PEF_TIPO_COMPRA",
				"PUB_EPO_PEF_USA_SIAFF", "PUB_EPO_PEF_USA_SIAFF"
			};
			List datosAnteriores = new ArrayList();
			List datosActuales = new ArrayList();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer();

			datosAnteriores = getParamEPO(sNoEPO, iNoProducto, con);

			String iNoNafin = "";
			sQuery =
				" SELECT ic_nafin_electronico"   +
				"   FROM comrel_nafin"   +
				"  WHERE ic_epo_pyme_if = ?"   +
				"    AND cg_tipo = 'E'"  ;

			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(sNoEPO));
			rs = ps.executeQuery();
			if(rs.next()) {
				iNoNafin = (rs.getString(1)==null)?"":rs.getString(1);
			}
			rs.close();ps.close();
			//--------------------------------


		    if (alParametros == null || alParametros.size()<3 ) {
				System.out.println("ERROR::ParametrosDescuentoBean::setParamEPO::No se proporcionaron los parametros a almacenar.");
				throw new NafinException("DSCT0096");
			}

			try {
				sQuery =
					" UPDATE comrel_producto_epo"   +
					"    SET cs_opera_notas_cred = ?,"   +
					"        cs_dscto_auto_obligatorio = ?,"   +
					"        fn_monto_minimo = ? ,"   +
					"        cg_descto_dinam = ? ,"   +
					"        cs_limite_pyme = ?,"   +  //Foda 37-SMJ 03/05/2006
					"        cs_pub_firma_manc = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_pub_hash = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_fecha_venc_pyme = ?,"   +  //Foda 44-SMJ 16/06/2006
					"        cs_pub_docto_venc = ?,"   +  //Foda 75-ACS 16/01/2007
					"        ig_plazo_pago_fvp = ?,"   +  //Foda 88- 22/02/2007
					"        ig_plazo_max1_fvp = ?,"   +  //Foda 88- 22/02/2007
					"        ig_plazo_max2_fvp = ?,"   +  //Foda 88- 22/02/2007
					"		   cs_preafiliacion=?, " +  //Foda AfiliacionElectronica- SMJ 15/05/2007
					"			cs_desc_auto_pyme=?, "+ // Fodea 14 - 2008 - JSHD
					"			cs_publicacion_epo_pef=? "+ // Fodea 45 - 2008 - JSHD
					"  WHERE ic_epo = ?"   +
					"    AND ic_producto_nafin = ?"  ;
				ps = con.queryPrecompilado(sQuery);
				int cont = 0;

				cont++;ps.setString(cont, (String)alParametros.get(0));
				cont++;ps.setString(cont, (String)alParametros.get(1));

				String auxMontoMinimo = alParametros.get(2).toString().trim();
				if(!"".equals(auxMontoMinimo)) {
					cont++;ps.setDouble(cont, Double.parseDouble(auxMontoMinimo));
				} else {
					cont++;ps.setNull(cont, Types.DOUBLE);
				}


				String auxDesctoDinam = alParametros.get(3).toString().trim();
				if(!"".equals(auxDesctoDinam)) {
					cont++;ps.setString(cont, auxDesctoDinam);
				} else {
					cont++;ps.setNull(cont, Types.VARCHAR);
				}

				cont++;ps.setString(cont, (String)alParametros.get(4)); 	//Foda 37-SMJ 03/05/2006
				cont++;ps.setString(cont, (String)alParametros.get(5)); 	//Foda 44-SMJ 16/06/2006
				cont++;ps.setString(cont, (String)alParametros.get(6)); 	//Foda 44-SMJ 16/06/2006
				cont++;ps.setString(cont, (String)alParametros.get(7)); 	//Foda 44-SMJ 16/06/2006

				String auxPubDoctosVenc = alParametros.get(8).toString().trim();
				if(!"".equals(auxPubDoctosVenc)) {
					cont++;ps.setString(cont, auxPubDoctosVenc);
				} else {
					cont++;ps.setNull(cont, Types.VARCHAR);
				} //Foda 75-ACS 16/01/2007

				String auxDiasMax = alParametros.get(9).toString().trim();
				if(!"".equals(auxDiasMax)) {
					cont++;ps.setString(cont, auxDiasMax);
				} else {
					cont++;ps.setNull(cont, Types.VARCHAR);
				}

				String auxPlazo1 = alParametros.get(10).toString().trim();
				if(!"".equals(auxPlazo1)) {
					cont++;ps.setString(cont, auxPlazo1);
				} else {
					cont++;ps.setNull(cont, Types.VARCHAR);
				}

				String auxPlazo2 = alParametros.get(11).toString().trim();
				if(!"".equals(auxPlazo2)) {
					cont++;ps.setString(cont, auxPlazo2);
				} else {
					cont++;ps.setNull(cont, Types.VARCHAR);
				}

				cont++;ps.setString(cont, (String)alParametros.get(12)); 	//Foda AfiliacionElectronica- SMJ 15/05/2007
				cont++;ps.setString(cont, (String)alParametros.get(13)); 	//F014 - 2008 JSHD
				cont++;ps.setString(cont, (String)alParametros.get(14)); 	//F045 - 2008 JSHD

				cont++;ps.setString(cont, sNoEPO);
				cont++;ps.setInt(cont, iNoProducto);

				//System.out.println(":::Update Parametros:::"+ sQuery);

				int iNoRegistros = ps.executeUpdate();
				ps.clearParameters();
				// Actualiza la informaci�n de las pymes si el descuento es obligatorio
				if ("S".equals((String)alParametros.get(1))) {
					sQuery =
						" UPDATE comcat_pyme p"   +
						"    SET cs_dscto_automatico = 'S'"   +
						"  WHERE p.ic_pyme IN ("   +
						"           SELECT cb.ic_pyme"   +
						"             FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb"   +
						"            WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
						"              AND pi.cs_borrado = 'N'"   +
						"              AND cb.cs_borrado = 'N'"   +
						"              AND pi.cs_vobo_if = 'S'"   +
						"              AND pi.cs_opera_descuento = 'S'"   +
						"              AND pi.ic_epo = ?)"   +
						"    AND cs_dscto_automatico = 'N'"  ;
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					iNoRegistros = ps.executeUpdate();
				}
				if(ps!=null) ps.close();


				//Siempre borrar los checks (borrar lo anterior y solo grabar lo nuevo, si l flag es N se queda borrado)
				sQuery = " DELETE com_parametrizacion_epo Where "+
				" CC_PARAMETRO_EPO in( " +
				"'PUB_EPO_PEF_FECHA_RECEPCION', " +
				"'PUB_EPO_PEF_FECHA_ENTREGA', " +
				"'PUB_EPO_PEF_TIPO_COMPRA', " +
				"'PUB_EPO_PEF_CLAVE_PRESUPUESTAL', " +
				"'PUB_EPO_PEF_PERIODO', " +
				"'PUB_EPO_PEF_USA_SIAFF') "+//MODIFICACION F049-2008
				" AND IC_EPO = " + sNoEPO;
				ps = con.queryPrecompilado(sQuery);
				iNoRegistros = ps.executeUpdate();

				//Insertar los checks si la flag es S
				System.out.println("Insertar los checks si la flag es S");
				if(((String)alParametros.get(15)).equals("S")){

					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_RECEPCION");
					ps.setString(3, (String)alParametros.get(15) );
					iNoRegistros = ps.executeUpdate();

					System.out.println("insert 1");

					ps.clearParameters();
					//guardando docto 1 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_ENTREGA");
					ps.setString(3, (String)alParametros.get(16) );
					iNoRegistros = ps.executeUpdate();

					System.out.println("insert 2");

					ps.clearParameters();
					//guardando docto 2 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_TIPO_COMPRA");
					ps.setString(3, (String)alParametros.get(17) );
					iNoRegistros = ps.executeUpdate();

					System.out.println("insert 3");

						ps.clearParameters();
					//guardando docto 3 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_CLAVE_PRESUPUESTAL");
					ps.setString(3, (String)alParametros.get(18) );
					iNoRegistros = ps.executeUpdate();

					System.out.println("insert 4");

					ps.clearParameters();
					//guardando docto 4 flag
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_PERIODO");
					ps.setString(3, (String)alParametros.get(19) );
					iNoRegistros = ps.executeUpdate();

					System.out.println("insert 5");

				}else{
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_FECHA_RECEPCION");
					ps.setString(3, "N" );
					iNoRegistros = ps.executeUpdate();
				}
				if(((String)alParametros.get(14)).equals("S")){
					ps.clearParameters();
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoEPO);
					ps.setString(2, "PUB_EPO_PEF_USA_SIAFF");
					ps.setString(3, (String)alParametros.get(20) );
					iNoRegistros = ps.executeUpdate();
				}

			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DSCT0096");
			}

			datosActuales = getParamEPO(sNoEPO, iNoProducto, con);

System.out.println("\n\ndatosAnteriores : "+datosAnteriores.toString());

System.out.println("\n\ndatosActuales : "+datosActuales.toString());

System.out.println("\n\n");

			List cambios =	Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);

			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));

			Bitacora.grabarEnBitacora(con, "PARAMDSCTO", "E",
					String.valueOf(iNoNafin), claveUsuario,
					valoresAnteriores.toString(), valoresActuales.toString());

		} catch (NafinException ne) {
			ne.printStackTrace();
			bOk = false;
			throw ne;
		} catch (Exception e) {
			e.printStackTrace();
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::setParamEPO(S)");
		}
	} // Fin del metodo setParamEPO
	/*********************/
	/* FIN FODA 021-2005 */
	/*********************/

	public List getIntermediarios(String ic_epo) throws NafinException {
		System.out.println("ParametrosDescuentoBean::getIntermediarios(E)");
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		List				columnas		= null;
		List				renglones		= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT i.cg_razon_social, i.cs_tipo, ie.cg_banco, ie.cg_num_cuenta, pl.cd_descripcion, ie.ig_orden, i.ic_if"   +
				"   FROM comrel_if_epo_x_producto iep,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_if i,"   +
				"        comcat_plaza pl"   +
				"  WHERE ie.ic_if = i.ic_if"   +
				"    AND iep.ic_if = ie.ic_if"   +
				"    AND iep.ic_epo = ie.ic_epo"   +
				"    AND ie.ic_plaza = pl.ic_plaza (+)"   +
				"    AND i.cs_habilitado = 'S'"   +
				"    AND ie.cs_vobo_nafin = 'S'"   +
				"    AND iep.cs_habilitado = 'S'"   +
				"    AND ie.ic_epo = ?"   +
				"    AND iep.ic_producto_nafin = 1"   +
				"  ORDER BY 1"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			while(rs.next()) {
				String rs_if = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				String rs_tipo = rs.getString("cs_tipo")==null?"":rs.getString("cs_tipo");
				String rs_banco = rs.getString("cg_banco")==null?"":rs.getString("cg_banco");
				String rs_cuenta = rs.getString("cg_num_cuenta")==null?"":rs.getString("cg_num_cuenta");
				String rs_plaza = rs.getString("cd_descripcion")==null?"":rs.getString("cd_descripcion");
				String rs_orden = rs.getString("ig_orden")==null?"":rs.getString("ig_orden");
				String rs_ifc = rs.getString("ic_if")==null?"":rs.getString("ic_if");
				columnas = new ArrayList();
				columnas.add(rs_if);
				columnas.add(rs_tipo);
				columnas.add(rs_banco);
				columnas.add(rs_cuenta);
				columnas.add(rs_plaza);
				columnas.add(rs_orden);
				columnas.add(rs_ifc);
				renglones.add(columnas);
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getIntermediarios(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getIntermediarios(S)");
		}
		return renglones;
	}

	public void setOrdenAplicacionIF(String ic_epo, String aplicacion[]) throws NafinException {
		System.out.println("ParametrosDescuentoBean::setOrdenAplicacionIF(E)");
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		StringTokenizer 	vTokCves 		= null;
		String 				ic_if 			= "";
		String 				ig_orden		= "";
		boolean				bOk				= true;
		try {
			con.conexionDB();

			for(int i=0;i<aplicacion.length;i++) {
				vTokCves = new StringTokenizer(aplicacion[i],"|");
				if(vTokCves.hasMoreTokens()){
					ic_if = vTokCves.nextToken().toString();
					ig_orden = vTokCves.nextToken().toString();
				}
				qrySentencia =
					" UPDATE comrel_if_epo"   +
					"    SET ig_orden = ?"   +
					"  WHERE ic_epo = ?"   +
					"    AND ic_if = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(ig_orden));
				ps.setInt(2, Integer.parseInt(ic_epo));
				ps.setInt(3, Integer.parseInt(ic_if));
				ps.executeUpdate();
				if(ps!=null) ps.close();
			}//for(int i=0;i<aplicacion.lenght;i++)
		} catch(Exception e){
			System.out.println("ParametrosDescuentoBean::setOrdenAplicacionIF(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::setOrdenAplicacionIF(S) ");
		}
	}

	public Map guardaParametrosCobMan(String productos[],String minCierre) throws NafinException {
		System.out.println("ParametrosDescuentoBean::guardaParametrosCobMan(E)");
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		Map					mParam			= new Hashtable();
		boolean				ok				= true;
		try {
			con.conexionDB();
			qrySentencia =
				" update comcat_productos_api"+
				" set cs_cobranza_manual = 'N'";
			ps = con.queryPrecompilado(qrySentencia);
			ps.execute();
			ps.close();
			if(productos!=null&&productos.length>0){
				qrySentencia =
					" update comcat_productos_api"+
					" set cs_cobranza_manual = 'S'"+
					" where cc_producto = ?";
				ps = con.queryPrecompilado(qrySentencia);
				for(int i=0;i<productos.length;i++){
					ps.setString(1,productos[i]);
					ps.execute();
				}
				if(ps!=null) ps.close();
			}

			qrySentencia =
				" update com_param_gral"+
				" set ig_min_cierre_cm = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,minCierre);
			ps.execute();

		} catch(Exception e){
			System.out.println("ParametrosDescuentoBean::guardaParametrosCobMan(Exception) "+e);
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("ParametrosDescuentoBean::guardaParametrosCobMan(S)");
		}
		return mParam;
	}



	/**
	 * Almacena los datos del Clausulado Electr�nico configurado para
	 * la epo especificada.
	 *
	 * @since F087-2006
	 * @param contrato Datos del contrato
	 * @param rutaArchivo Ruta del archivo PDF, PPT o DOC con el contenido
	 * 		del clausulado electr�nico
	 * @return Cadena con el consecutivo asignado al contrato.
	 */
	public String guardarClausulado(ContratoEpo contrato,
			String rutaArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		int ic_epo = 0;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (contrato==null || contrato.getClaveEpo() == null ||
					contrato.getTituloAviso() == null ||
					contrato.getContenidoAviso() == null ||
					contrato.getBotonAviso() == null ||
					contrato.getTextoAceptacion() == null ||
					contrato.getBotonAceptacion() == null ||
					contrato.getMostrar() == null || rutaArchivo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_epo = Integer.parseInt(contrato.getClaveEpo());

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" contrato=" + contrato + "\n" +
					" rutaArchivo = " + rutaArchivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" SELECT NVL(MAX(ic_consecutivo),0) + 1 as consecutivo" +
					" FROM com_contrato_epo " +
					" WHERE ic_epo = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_epo);
			ResultSet rs = ps.executeQuery();
			rs.next();
			long consecutivo = rs.getLong("consecutivo");
			rs.close();
			ps.close();

			strSQL =
					" UPDATE com_contrato_epo " +
					" SET cs_mostrar = ? " +
					" WHERE ic_epo = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "N");
			ps.setInt(2, ic_epo);

			ps.executeUpdate();
			ps.close();

			strSQL = "INSERT INTO com_contrato_epo(" +
					" ic_epo, ic_consecutivo, cg_titulo_aviso, cg_contenido_aviso, " +
					" cg_boton_aviso, bi_documento, " +
					" cg_texto_aceptacion, cg_boton_aceptacion, " +
					" cs_mostrar, df_alta ) " +
					" VALUES (?, ?, ?, ?, ?, empty_blob(), ?, ?, ?, sysdate) ";

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_epo);
			ps.setLong(2, consecutivo);
			ps.setString(3, contrato.getTituloAviso());
			ps.setString(4, contrato.getContenidoAviso());
			ps.setString(5, contrato.getBotonAviso());
			ps.setString(6, contrato.getTextoAceptacion());
			ps.setString(7, contrato.getBotonAceptacion());
			ps.setString(8, contrato.getMostrar());

			ps.executeUpdate();
			ps.close();

			contrato.setConsecutivo(String.valueOf(consecutivo));
			contrato.getContratoArchivo().guardarArchivoContratoEpo(rutaArchivo, con);

			return String.valueOf(consecutivo);
		} catch(Exception e) {
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}


	}

	/**
	 * Obtiene los datos del Clausulado Electr�nico configurado para
	 * la epo especificada. El archivo del contrato (pdf,ppt o doc) no
	 * se obtiene debido a que es un campo binario.
	 * Si el consecutivo es null obtiene todos los contratos configurados para
	 * la epo especificada.
	 *
	 * @since F087-2006
	 * @param claveEpo Clave de la epo
	 * @param consecutivo Consecutivo
	 * @return Lista de objetos ContratoEpo con los datos del contrato. Si no
	 * 		hay datos regresa una lista vac�a.
	 *
	 */
	public List getDatosClausulado(String claveEpo, String consecutivo)
			throws NafinException {
		AccesoDB con = new AccesoDB();

		int ic_epo = 0;
		long ic_consecutivo = 0;

		List registros = new ArrayList();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveEpo==null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_epo = Integer.parseInt(claveEpo);
			if (consecutivo != null) {
				ic_consecutivo = Long.parseLong(consecutivo);
			}

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" claveEpo=" + claveEpo + "\n" +
					" consecutivo = " + consecutivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();


			String strSQL =
					" SELECT e.ic_epo, e.cg_razon_social, " +
					" 	ce.ic_consecutivo, ce.cg_titulo_aviso, " +
					" 	ce.cg_contenido_aviso, ce.cg_boton_aviso, " +
					" 	ce.cg_ext_documento, " +
					" 	ce.cg_texto_aceptacion, ce.cg_boton_aceptacion, " +
					" 	ce.cs_mostrar, ce.df_alta,  " +
					" 	CASE WHEN ic_consecutivo = " +
					" 		(SELECT max(ic_consecutivo) FROM com_contrato_epo WHERE ic_epo = ?) THEN " +
					" 		'S' ELSE 'N' END AS ultimo " +
					" FROM comcat_epo e, com_contrato_epo ce " +
					" WHERE ce.ic_epo = e.ic_epo " +
					" AND ce.ic_epo = ? " +
					((consecutivo!=null)?"AND ce.ic_consecutivo = ?":"") +
					" ORDER BY ce.ic_consecutivo " ;
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_epo);
			ps.setInt(2, ic_epo);
			if (consecutivo!=null) {
				ps.setLong(3, ic_consecutivo);
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ContratoEpo contrato = new ContratoEpo();

				contrato.setClaveEpo(rs.getString("ic_epo"));
				contrato.setConsecutivo(rs.getString("ic_consecutivo"));
				contrato.setTituloAviso(rs.getString("cg_titulo_aviso"));
				contrato.setContenidoAviso(rs.getString("cg_contenido_aviso"));
				contrato.setBotonAviso(rs.getString("cg_boton_aviso"));
				contrato.setTextoAceptacion(rs.getString("cg_texto_aceptacion"));
				contrato.setBotonAceptacion(rs.getString("cg_boton_aceptacion"));
				contrato.getContratoArchivo().setExtension(rs.getString("cg_ext_documento"));
				contrato.setMostrar(rs.getString("cs_mostrar"));
				contrato.setFechaAlta(rs.getTimestamp("df_alta"));

				contrato.setNombreEpo(rs.getString("cg_razon_social"));
				contrato.setUltimo( ( (rs.getString("ultimo").equals("S"))?true:false ) );

				registros.add(contrato);
			}

			rs.close();
			ps.close();

			return registros;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene los datos del ultimo Clausulado Electr�nico configurado para
	 * la epo especificada. El archivo del contrato (pdf,ppt o doc) no
	 * se obtiene debido a que es un campo binario.
	 *
	 * @since F087-2006
	 * @param claveEpo Clave de la epo
	 * @return Objeto de ContratoEpo con los datos del contrato.
	 *
	 */
	public ContratoEpo getDatosUltimoClausulado(String claveEpo)
			throws NafinException {
		AccesoDB con = new AccesoDB();

		int ic_epo = 0;

		ContratoEpo contrato = new ContratoEpo();
		String consecutivo = "";

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveEpo==null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_epo = Integer.parseInt(claveEpo);

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" claveEpo=" + claveEpo );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();


			String strSQL =
					" SELECT max(ic_consecutivo) as consecutivo" +
					" FROM com_contrato_epo " +
					" WHERE ic_epo = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_epo);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				consecutivo = rs.getString("consecutivo");
			}

			rs.close();
			ps.close();

			List l = this.getDatosClausulado(claveEpo, consecutivo);
			if (l.size() > 0) {
				contrato = (ContratoEpo)l.get(0);
			}

			return contrato;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	public Map getParametrosCobMan() throws NafinException {
		System.out.println("ParametrosDescuentoBean::getParametrosCobMan(E)");
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Map					mParam			= new Hashtable();
		List				columnas		= null;
		List				renglones		= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia =
				" select cc_producto"   +
				"  ,cg_nombre"   +
				"  ,cs_cobranza_manual "   +
				" from comcat_productos_api"   +
				" where ig_sub_aplicacion is not null"   +
				" order by ig_orden"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new ArrayList();
/*0*/			columnas.add(rs.getString("CC_PRODUCTO")==null?"":rs.getString("CC_PRODUCTO"));
/*1*/			columnas.add(rs.getString("CG_NOMBRE")==null?"":rs.getString("CG_NOMBRE"));
/*2*/			columnas.add(rs.getString("CS_COBRANZA_MANUAL")==null?"":rs.getString("CS_COBRANZA_MANUAL"));
				renglones.add(columnas);
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();
			mParam.put("PRODUCTOS",renglones);
			qrySentencia =
				" select nvl(ig_min_cierre_cm,0) as ig_min_cierre_cm "   +
				" from com_param_gral"   +
				" where rownum = 1"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				mParam.put("MIN_CIERRE",rs.getString("IG_MIN_CIERRE_CM")==null?"":rs.getString("IG_MIN_CIERRE_CM"));
			}else{
				mParam.put("MIN_CIERRE","0");
			}
			rs.close();
			ps.close();
		} catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getParametrosCobMan(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("ParametrosDescuentoBean::getParametrosCobMan(S)");
		}
		return mParam;
	}

	public List getPymesSinDsctoAuto(String ic_epo, String ic_moneda) throws NafinException {
		System.out.println("ParametrosDescuentoBean::getPymesSinDsctoAuto(E)");
		AccesoDB			con 			= null;
		String				qrySentencia	= "";
		ResultSet			rs				= null;
		PreparedStatement 	ps 				= null;
		boolean				bOK				= true;
		List				renglones 		= new ArrayList();
		List				columnas 		= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT   /*+index(pi) use_nl(cb py pi pe e if m)*/"   +
				"           e.ic_epo, e.cg_razon_social AS nombre_epo,"   +
				"           py.ic_pyme, py.cg_razon_social AS nombrepyme, IF.cg_razon_social AS nombre_if,"   +
				"           IF.ic_if, cb.ic_cuenta_bancaria, cb.cg_banco, cb.cg_numero_cuenta,"   +
				"           cb.cg_sucursal, m.cd_nombre as moneda,"   +
				"           pe.cs_dscto_auto_obligatorio obligatorio,"   +
				"           pi.cs_dscto_automatico_dia, cpi.ig_orden, '13dsctoautmas.jsp' AS pantalla"   +
				"     FROM comrel_cuenta_bancaria cb,"   +
				"          comcat_pyme py,"   +
				"          comrel_pyme_if pi,"   +
				"          comrel_producto_epo pe,"   +
				"          comrel_if_epo cpi,"   +
				"          comcat_epo e,"   +
				"          comcat_if IF,"   +
				"          comcat_moneda m"   +
				"    WHERE py.ic_pyme = cb.ic_pyme"   +
				"      AND pe.ic_epo = e.ic_epo"   +
				"      AND pi.ic_epo = e.ic_epo"   +
				"      AND pi.ic_if = IF.ic_if"   +
				"      AND pi.ic_if = cpi.ic_if"   +
				"      AND pi.ic_epo = cpi.ic_epo"   +
				"      AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
				"      AND cb.ic_moneda = m.ic_moneda"   +
				"      AND pe.ic_producto_nafin = 1"   +
				"      AND pi.cs_borrado = 'N'"   +
				"      AND cb.cs_borrado = 'N'"   +
				"      AND pi.cs_vobo_if = 'S'"   +
				"      AND pi.cs_opera_descuento = 'S'"   +
				"      AND e.cs_habilitado = 'S'"   +
				"      AND pi.cs_dscto_automatico != 'S'"   +
				"      AND py.cs_dscto_automatico != 'S'"   +
				"      AND pe.ic_epo = ?"   +
				"      AND m.ic_moneda = ?"   +
				" ORDER BY py.cg_razon_social,py.ic_pyme, m.ic_moneda, cpi.ig_orden"  ;
//			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				String rs_ic_epo 	= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String rs_epo 		= rs.getString("nombre_epo")==null?"":rs.getString("nombre_epo");
				String rs_ic_pyme	= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_pyme		= rs.getString("nombrepyme")==null?"":rs.getString("nombrepyme");
				String rs_ic_if		= rs.getString("ic_if")==null?"":rs.getString("ic_if");
				String rs_if		= rs.getString("nombre_if")==null?"":rs.getString("nombre_if");
				String rs_cuenta	= rs.getString("ic_cuenta_bancaria")==null?"":rs.getString("ic_cuenta_bancaria");
				String rs_banco		= rs.getString("cg_banco")==null?"":rs.getString("cg_banco");
				String rs_num_cta	= rs.getString("cg_numero_cuenta")==null?"":rs.getString("cg_numero_cuenta");
				String rs_sucursal	= rs.getString("cg_sucursal")==null?"":rs.getString("cg_sucursal");
				String rs_moneda	= rs.getString("moneda")==null?"":rs.getString("moneda");
				String rs_dscta_obl	= rs.getString("obligatorio")==null?"":rs.getString("obligatorio");
				String rs_dscta_dia	= rs.getString("cs_dscto_automatico_dia")==null?"":rs.getString("cs_dscto_automatico_dia");
				columnas = new ArrayList();
				columnas.add(rs_ic_epo);
				columnas.add(rs_epo);
				columnas.add(rs_ic_pyme);
				columnas.add(rs_pyme);
				columnas.add(rs_ic_if);
				columnas.add(rs_if);
				columnas.add(rs_cuenta);
				columnas.add(rs_banco);
				columnas.add(rs_num_cta);
				columnas.add(rs_sucursal);
				columnas.add(rs_moneda);
				columnas.add(rs_dscta_obl);
				columnas.add(rs_dscta_dia);
				renglones.add(columnas);
			}//while
			rs.close();
			if(ps != null) ps.close();
		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getPymesSinDsctoAuto(Exception) "+e);
			bOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
				System.out.println("ParametrosDescuentoBean::getPymesSinDsctoAuto(S)");
			}
		}
		return renglones;
	}//getPymesSinDsctoAuto

	public int getNumeroLineas(String ic_epo, String ic_pyme, String ic_moneda) throws NafinException {
		System.out.println("ParametrosDescuentoBean::getNumeroLineas(E)");
		AccesoDB			con 			= null;
		String				qrySentencia	= "";
		ResultSet			rs				= null;
		PreparedStatement 	ps 				= null;
		boolean				bOK				= true;
		int 				rs_numero_monedas = 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT   pi.ic_epo AS epo, COUNT (1) AS nummonedas, cb.ic_moneda,"   +
				"          SUM (DECODE (pi.cs_dscto_automatico, 'S', 1, 0)) marca"   +
				"     FROM comrel_cuenta_bancaria cb, comrel_pyme_if pi"   +
				"    WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
				"      AND pi.cs_vobo_if = 'S'"   +
				"      AND pi.cs_borrado = 'N'"   +
				"      AND cb.cs_borrado = 'N'"   +
				"      AND pi.cs_opera_descuento = 'S'"   +
        "      AND pi.cs_dscto_automatico != 'S' " +
				"      AND pi.ic_epo = ?"   +
				"      AND cb.ic_pyme = ?"   +
				"      AND cb.ic_moneda = ?"   +
				" GROUP BY pi.ic_epo, cb.ic_moneda"  ;
//			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				rs_numero_monedas = rs.getInt("nummonedas");
			}//while
			rs.close();
			if(ps != null) ps.close();
		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getNumeroLineas(Exception) "+e);
			bOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
				System.out.println("ParametrosDescuentoBean::getNumeroLineas(S)");
			}
		}
		return rs_numero_monedas;
	}//getPymesSinDsctoAuto

	public List getParamSubsidioEpo(String ic_epo, String ic_epo_dest, String ic_pyme) throws NafinException {
		System.out.println("ParametrosDescuentoBean::getParamSubsidioEpo(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List				lRenglones		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT sub.cs_activo, sub.fg_puntos, sub.fg_limite,"   +
				"        sub.fg_acumulado, TO_CHAR(sub.df_inicio_apoyo, 'dd/mm/yyyy') df_inicio_apoyo"   +
				"   FROM comrel_subsidio_epo sub"   +
				"  WHERE sub.ic_producto_nafin = 1"   +
				"    AND sub.ic_epo = ?"   +
				"    AND sub.ic_epo_dest = ?"   +
				"    AND sub.ic_pyme = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_epo_dest));
			ps.setInt(3, Integer.parseInt(ic_pyme));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				String rs_activo		= rs.getString(1)==null?"":rs.getString(1);
				String rs_puntos		= rs.getString(2)==null?"":rs.getString(2);
				String rs_limite		= rs.getString(3)==null?"":rs.getString(3);
				String rs_acumulado		= rs.getString(4)==null?"":rs.getString(4);
				String rs_inicio_apoyo	= rs.getString(5)==null?"":rs.getString(5);
				List lColumnas = new ArrayList();
				lColumnas.add(rs_activo);
				lColumnas.add(rs_puntos);
				lColumnas.add(rs_limite);
				lColumnas.add(rs_acumulado);
				lColumnas.add(rs_inicio_apoyo);
				lRenglones.add(lColumnas);
			}//if(rs.next())
			rs.close();ps.close();
		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getParamSubsidioEpo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParamSubsidioEpo(S)");
		}
		return lRenglones;
	}//getParamSubsidioEpo

	public void setParamSubsidioEpo(String ic_epo, String ic_epo_dest, String ic_pyme, String cs_activo, String fg_puntos, String fg_limite, String df_inicio_apoyo) throws NafinException {
		System.out.println("ParametrosDescuentoBean::setParamSubsidioEpo(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		String				condicion	 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		int					existe			= 0;
		boolean				bOk				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comrel_subsidio_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND ic_epo = ?"   +
				"    AND ic_epo_dest = ?"   +
				"    AND ic_pyme = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_epo_dest));
			ps.setInt(3, Integer.parseInt(ic_pyme));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())
				existe = rs.getInt(1);
			rs.close();ps.close();

			if("".equals(cs_activo))
				cs_activo = "N";

			if(existe==0) {
				qrySentencia =
					" INSERT INTO comrel_subsidio_epo"   +
					"             ("   +
					"              ic_producto_nafin, ic_epo, ic_epo_dest, ic_pyme, cs_activo,"   +
					"              fg_puntos, fg_limite, fg_acumulado, df_inicio_apoyo"   +
					"             )"   +
					"      VALUES ("   +
					"              1, ?, ?, ?, ?,"   +
					"              ?, ?, 0, TO_DATE(?, 'dd/mm/yyyy')"   +
					"             )"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(ic_epo));
				ps.setInt(2, Integer.parseInt(ic_epo_dest));
				ps.setInt(3, Integer.parseInt(ic_pyme));
				ps.setString(4, cs_activo);
				ps.setDouble(5, Double.parseDouble(fg_puntos));
				ps.setDouble(6, Double.parseDouble(fg_limite));
				ps.setString(7, df_inicio_apoyo);
			} else {
				if(!"".equals(fg_puntos))
					condicion += "        ,fg_puntos = ?";
				if(!"".equals(fg_limite))
					condicion += "        ,fg_limite = ?";
				if(!"".equals(df_inicio_apoyo))
					condicion += "        ,df_inicio_apoyo = TO_DATE (?, 'dd/mm/yyyy')";

				qrySentencia =
					" UPDATE comrel_subsidio_epo"   +
					"    SET cs_activo = ?"   +
					condicion+
					"  WHERE ic_producto_nafin = 1"   +
					"    AND ic_epo = ?"   +
					"    AND ic_epo_dest = ?"   +
					"    AND ic_pyme = ?";
				ps = con.queryPrecompilado(qrySentencia);
				int cont = 0;
				cont++;ps.setString(cont, cs_activo);
				if(!"".equals(fg_puntos)) {
					cont++;ps.setDouble(cont, Double.parseDouble(fg_puntos));
				}
				if(!"".equals(fg_limite)) {
					cont++;ps.setDouble(cont, Double.parseDouble(fg_limite));
				}
				if(!"".equals(df_inicio_apoyo)) {
					cont++;ps.setString(cont, df_inicio_apoyo);
				}
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo_dest));
				cont++;ps.setInt(cont, Integer.parseInt(ic_pyme));
			}
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			bOk = false;
			System.out.println("ParametrosDescuentoBean::setParamSubsidioEpo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::setParamSubsidioEpo(S)");
		}
	}//setParamSubsidioEpo


/**********************/
/* ACS FODA 075-2007 */
/**********************/


	public ArrayList getParamPorEPO(String sNoEPO, String icBancoFondeo) throws NafinException {  //FODEA 008 - 2009 ACF
		System.out.println("ParametrosDescuentoBean::getParamPorEPO(E)");
		AccesoDB con = new AccesoDB();
		ArrayList alParametros = new ArrayList();
		try {
			con.conexionDB();
			alParametros = getParamPorEPO(sNoEPO, icBancoFondeo, con);  //FODEA 008 - 2009 ACF
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParamPorEPO(S)");
		}
		return alParametros;
	}	// Fin del metodo getParamPorEPO

	public ArrayList getParamPorEPO(String sNoEPO, String icBancoFondeo, AccesoDB con) throws NafinException { //FODEA 008 -2009 ACF
	/* Regresa:
		Lista de EPOS con sus parametros asociados
	*/

		System.out.println("ParametrosDescuentoBean::getParamPorEPO(E)");
		ArrayList alListaEpo = new ArrayList();
		HashMap hmParametros = null;
		String condicion="";
		if(!sNoEPO.equals("0")){
			condicion= "    AND rel.ic_epo = ?" ;
		}
//	    AccesoDB con = new AccesoDB();
		try {
//			con.conexionDB();

			String sQuery =
			" SELECT cat.cg_razon_social nombre, rel.ig_dias_minimo as dmin, rel.ig_dias_maximo as dmax, rel.fn_aforo as fecha_aforo, rel.fn_aforo_dl as fecha_aforo_dol,"   +
			"        rel.cs_factoraje_vencido as fact_venc, rel.cs_factoraje_distribuido as fact_distr"   +
			"   FROM comrel_producto_epo rel,"   +
			"		comcat_epo cat" +
			"  WHERE rel.ic_epo=cat.ic_epo"+
			"    AND rel.ic_producto_nafin = ?"+
      "    AND cat.ic_banco_fondeo = ?"   ; //FODEA 008 - 2009
			sQuery += condicion +
			" ORDER BY cat.cg_razon_social ";
      //FODEA 00 8 -2009 ACF (I)
			PreparedStatement ps = con.queryPrecompilado(sQuery);
      ps.setInt(1, 1);
      ps.setInt(2, Integer.parseInt(icBancoFondeo));
			if(!sNoEPO.equals("0")){
				ps.setString(3, sNoEPO);
			}
      //FODEA 00 8 -2009 ACF (F)
			System.out.println(":::Consulta Parametros:::"+ sQuery);
			try {
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					hmParametros = new HashMap();
					hmParametros.put("nombre",(rs.getString("nombre")==null)?"":rs.getString("nombre").trim());
					hmParametros.put("dmin",(rs.getString("dmin")==null)?"":rs.getString("dmin").trim());
					hmParametros.put("dmax",(rs.getString("dmax")==null)?"":rs.getString("dmax").trim());
					hmParametros.put("fecha_aforo",(rs.getString("fecha_aforo")==null)?"":rs.getString("fecha_aforo").trim());
					hmParametros.put("fecha_aforo_dol",(rs.getString("fecha_aforo_dol")==null)?"":rs.getString("fecha_aforo_dol").trim());
					hmParametros.put("fact_venc",(rs.getString("fact_venc")==null)?"":rs.getString("fact_venc").trim());
					hmParametros.put("fact_distr",(rs.getString("fact_distr")==null)?"":rs.getString("fact_distr").trim());
					alListaEpo.add(hmParametros);

				}
				rs.close();
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DSCT0095");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			//if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParamPorEPO(S)");
		}
		return alListaEpo;
	}	// Fin del metodo getParamPorEPO

/*********************/
/* FIN FODA 075-2007 */
/*********************/
//==============================================================================================================================================================>>
	public String setParamEpoPymeIf() throws NafinException{
		System.out.println("ParametrosDescuentoBean::setParamEpoPymeIf()");
		AccesoDB con          = null;
		boolean bOK     			= true;
		String qrySentencia   = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		java.text.SimpleDateFormat formatterYear = new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

		contenidoArchivo.append("****** OPERACIONES EFECTUADAS ****** "+new java.util.Date()+"\n");

		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				"SELECT   cpe.ic_epo, cpe.ic_pyme, ccb.ic_moneda, MIN (cpi.df_vobo_if), COUNT (1) " +
				"    FROM comrel_pyme_epo cpe, " +
				"         comrel_producto_epo pe, " +
				"         comrel_pyme_if cpi, " +
				"         comrel_cuenta_bancaria ccb, " +
				"         comcat_pyme pym " +
				"   WHERE cpe.ic_epo = cpi.ic_epo " +
				"     AND cpe.ic_epo = pe.ic_epo " +
				"     AND cpe.ic_pyme = ccb.ic_pyme " +
				"     AND cpe.ic_pyme = pym.ic_pyme " +
				"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
				"     AND pe.ic_producto_nafin = ? " +
				"     AND cpi.cs_vobo_if = ? " +
				"     AND ccb.cs_borrado = ? " +
				"     AND cpi.cs_borrado = ? " +
				"     AND cpi.cs_opera_descuento = ? " +
				"     AND cpe.cs_aceptacion = ? " +
				"     AND ccb.ic_moneda IN (?, ?) " +
				"     AND pym.ic_version_convenio in  (?) " +
				//"     AND pym.ic_version_convenio in  (?,?) " +
				"     AND pym.cs_dscto_auto_proc = ? " +
				"     AND (pe.cs_factoraje_vencido = ? OR pe.cs_factoraje_vencido IS NULL) " +
				"     AND (pe.cs_factoraje_distribuido = ? OR pe.cs_factoraje_distribuido IS NULL) " +
				"GROUP BY cpe.ic_epo, cpe.ic_pyme, ccb.ic_moneda " +
				"ORDER BY MIN (cpi.df_vobo_if) ";

			List lVarBind = new ArrayList();
			lVarBind.add(new Integer(1));		//pe.ic_producto_nafin
			lVarBind.add("S");					//cpi.cs_vobo_if
			lVarBind.add("N");					//ccb.cs_borrado
			lVarBind.add("N");					//cpi.cs_borrado
			lVarBind.add("S");					//cpi.cs_opera_descuento
			lVarBind.add("H");					//cpe.cs_aceptacion
			lVarBind.add(new Integer(1));		//ccb.ic_moneda IN
			lVarBind.add(new Integer(54));		//ccb.ic_moneda IN
			lVarBind.add(new Integer(3));		//pym.ic_version_convenio
			//lVarBind.add(new Integer(4));		//pym.ic_version_convenio
			lVarBind.add("S");					//pym.cs_dscto_auto_proc
			lVarBind.add("N");					//pe.cs_factoraje_vencido
			lVarBind.add("N");					//pe.cs_factoraje_distribuido

			System.out.println("\n qrySentencia: "+qrySentencia);
			System.out.println("\n lVarBind: "+lVarBind);

			Registros regEpoPymeMoneda = con.consultarDB(qrySentencia, lVarBind);
			while(regEpoPymeMoneda.next()) {
				String rs_ic_epo	= regEpoPymeMoneda.getString("ic_epo");
				String rs_ic_pyme	= regEpoPymeMoneda.getString("ic_pyme");
				String rs_ic_moneda	= regEpoPymeMoneda.getString("ic_moneda");
				java.util.Date startTime = new java.util.Date();

				contenidoArchivo.append(
					"\n"+formatterYear.format(startTime)+
					",PyME,"+rs_ic_pyme+
					",EPO,"+rs_ic_epo+
					",Moneda,"+rs_ic_moneda);

				qrySentencia =
					"SELECT   cpe.ic_epo, cpe.ic_pyme, cpi.ic_if, cpi.ic_cuenta_bancaria, MIN (cpi.df_vobo_if) " +
					"    FROM comrel_pyme_epo cpe, comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_pyme pym " +
					"   WHERE cpe.ic_epo = cpi.ic_epo " +
					"     AND cpe.ic_pyme = ccb.ic_pyme " +
					"     AND cpe.ic_pyme = pym.ic_pyme " +
					"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
					"     AND cpi.cs_vobo_if = ? " +
					"     AND ccb.cs_borrado = ? " +
					"     AND cpi.cs_borrado = ? " +
					"     AND cpi.cs_opera_descuento = ? " +
					"     AND cpe.cs_aceptacion = ? " +
					"     AND cpe.ic_epo = ? " +
					"     AND cpe.ic_pyme = ? " +
					"     AND ccb.ic_moneda = ? " +
					"     AND pym.ic_version_convenio in (?) " +
					//"     AND pym.ic_version_convenio in (?,?) " +
					"     AND pym.cs_dscto_auto_proc = ? " +
					"GROUP BY cpe.ic_epo, cpe.ic_pyme, cpi.ic_if, cpi.ic_cuenta_bancaria " +
					"ORDER BY MIN (cpi.df_vobo_if) ";

				lVarBind = new ArrayList();
				lVarBind.add("S");							//cpi.cs_vobo_if
				lVarBind.add("N");							//ccb.cs_borrado
				lVarBind.add("N");							//cpi.cs_borrado
				lVarBind.add("S");							//cpi.cs_opera_descuento
				lVarBind.add("H");							//cpe.cs_aceptacion
				lVarBind.add(new Long(rs_ic_epo));			//cpe.ic_epo
				lVarBind.add(new Long(rs_ic_pyme));			//cpe.ic_pyme
				lVarBind.add(new Integer(rs_ic_moneda));	//ccb.ic_moneda
				lVarBind.add(new Integer(3));				//pym.ic_version_convenio
				//lVarBind.add(new Integer(4));				//pym.ic_version_convenio
				lVarBind.add("S");							//pym.cs_dscto_auto_proc

				System.out.println("\n qrySentencia: "+qrySentencia);
				System.out.println("\n lVarBind: "+lVarBind);

				Registros regEpoPymeMonedaIf = con.consultarDB(qrySentencia, lVarBind);
				if(regEpoPymeMonedaIf.next()) {
					String rs_ic_if					= regEpoPymeMonedaIf.getString("ic_if");
					String rs_ic_cuenta_bancaria	= regEpoPymeMonedaIf.getString("ic_cuenta_bancaria");

					qrySentencia =
						" UPDATE comcat_pyme " +
						"SET cs_dscto_automatico = ?, cs_dscto_auto_proc = ? " +
						"  WHERE ic_pyme = ? ";
					lVarBind = new ArrayList();
					lVarBind.add("S");
					lVarBind.add("N");
					lVarBind.add(new Long(rs_ic_pyme));
					System.out.println("\n qrySentencia: "+qrySentencia);
					System.out.println("\n lVarBind: "+lVarBind);
					int cont = con.ejecutaUpdateDB(qrySentencia, lVarBind);

					qrySentencia =
						" UPDATE comrel_pyme_if " +
						" SET cs_dscto_automatico = ?, cs_dscto_automatico_dia = ?, " +
						" ig_orden = ?, cs_dscto_automatico_proc = ? " +
						"  WHERE ic_cuenta_bancaria = ? " +
						"    AND ic_if = ? " +
						"    AND ic_epo = ? ";
					lVarBind = new ArrayList();
					lVarBind.add("S");
					lVarBind.add("P");
					lVarBind.add(new Integer(1));
					lVarBind.add("S");
					lVarBind.add(new Long(rs_ic_cuenta_bancaria));
					lVarBind.add(new Long(rs_ic_if));
					lVarBind.add(new Long(rs_ic_epo));
					System.out.println("\n qrySentencia: "+qrySentencia);
					System.out.println("\n lVarBind: "+lVarBind);
					cont = con.ejecutaUpdateDB(qrySentencia, lVarBind);

					qrySentencia =
						" INSERT  " +
						"   INTO bit_dscto_automatico  " +
						"    (  " +
						"        ic_usuario, ic_cuenta_bancaria, ic_if      ,  " +
						"        ic_epo    , df_registro       , cs_autorizo  " +
						"    )  " +
						"    VALUES  " +
						"    (  " +
						"        ?, ?, ?, " +
						"        ?, SYSDATE, ? " +
						"    ) " ;
					lVarBind = new ArrayList();
					lVarBind.add("proc_aut");
					lVarBind.add(new Long(rs_ic_cuenta_bancaria));
					lVarBind.add(new Integer(Integer.parseInt(rs_ic_if)));

					lVarBind.add(new Integer(Integer.parseInt(rs_ic_epo)));
					lVarBind.add("S");
					System.out.println("\n qrySentencia: "+qrySentencia);
					System.out.println("\n lVarBind: "+lVarBind);
					cont = con.ejecutaUpdateDB(qrySentencia, lVarBind);

					contenidoArchivo.append(
						"\n"+formatterYear.format(startTime)+
						",EPO,"+rs_ic_epo+
						",PyME,"+rs_ic_pyme+
						",Cuenta,"+rs_ic_cuenta_bancaria+
						",Moneda,"+rs_ic_moneda+
						",IF,"+rs_ic_if);

				}//if(regEpoPymeMonedaIf.next())
			}//while(regEpoPymeMoneda.next())
		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::setParamEpoPymeIf(Exception) "+e);
			bOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
				System.out.println("ParametrosDescuentoBean::setParamEpoPymeIf(S)");
			}
		}
		return contenidoArchivo.toString();
	}//setParamEpoPymeIf

	/**
	 * Actualiza los datos del Clausulado Electr�nico configurado para la IF especificada en la BD
	 * @since F012-2008
	 * @param contrato Datos del contrato
	 * @param rutaArchivo Ruta del archivo PDF, PPT o DOC con el contenido
	 * 		del clausulado electr�nico o null si este no es modificado
	 */
	public void actualizarClausuladoIF(ContratoIF contrato, String rutaArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		int ic_if = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (contrato==null || contrato.getClaveIF() == null ||
					contrato.getConsecutivo() == null ||
					contrato.getTituloAviso() == null ||
					contrato.getContenidoAviso() == null ||
					contrato.getBotonAviso() == null ||
					contrato.getTextoAceptacion() == null ||
					contrato.getBotonAceptacion() == null ||
					contrato.getMostrar() == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(contrato.getClaveIF());
			ic_consecutivo = Long.parseLong(contrato.getConsecutivo());

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" contrato=" + contrato + "\n" +
					" rutaArchivo = " + rutaArchivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" UPDATE com_contrato_if " +
					" SET cg_titulo_aviso = ?, " +
					" 	cg_contenido_aviso = ?, " +
					" 	cg_boton_aviso = ?, " +
					" 	bi_documento = empty_blob(), " +
					" 	cg_texto_aceptacion = ?, " +
					" 	cg_boton_aceptacion = ?, " +
					" 	cs_mostrar = ?, " +
					" 	df_alta = sysdate " +
					" WHERE ic_if = ? " +
					" 	AND ic_consecutivo = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, contrato.getTituloAviso());
			ps.setString(2, contrato.getContenidoAviso());
			ps.setString(3, contrato.getBotonAviso());
			ps.setString(4, contrato.getTextoAceptacion());
			ps.setString(5, contrato.getBotonAceptacion());
			ps.setString(6, contrato.getMostrar());
			ps.setInt(7, ic_if);
			ps.setLong(8, ic_consecutivo);

			ps.executeUpdate();
			ps.close();

			if (rutaArchivo != null) {
				contrato.getContratoArchivo().guardarArchivoContratoIF(rutaArchivo, con);
			}

		} catch(Exception e) {
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Almacena los datos del Clausulado Electr�nico configurado para la if especificada.
	 * @since F012-2008
	 * @param contrato Datos del contrato
	 * @param rutaArchivo Ruta del archivo PDF, PPT o DOC con el contenido
	 * 		del clausulado electr�nico
	 * @return Cadena con el consecutivo asignado al contrato.
	 */
	public String guardarClausuladoIF(ContratoIF contrato, String rutaArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		int ic_if = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (contrato==null || contrato.getClaveIF() == null ||
					contrato.getTituloAviso() == null ||
					contrato.getContenidoAviso() == null ||
					contrato.getBotonAviso() == null ||
					contrato.getTextoAceptacion() == null ||
					contrato.getBotonAceptacion() == null ||
					contrato.getMostrar() == null || rutaArchivo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(contrato.getClaveIF());

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" contrato=" + contrato + "\n" +
					" rutaArchivo = " + rutaArchivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" SELECT NVL(MAX(ic_consecutivo),0) + 1 as consecutivo" +
					" FROM com_contrato_if " +
					" WHERE ic_if = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ResultSet rs = ps.executeQuery();
			rs.next();
			long consecutivo = rs.getLong("consecutivo");
			rs.close();
			ps.close();

			strSQL =
					" UPDATE com_contrato_if " +
					" SET cs_mostrar = ? " +
					" WHERE ic_if = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "N");
			ps.setInt(2, ic_if);

			ps.executeUpdate();
			ps.close();

			strSQL = "INSERT INTO com_contrato_if(" +
					" ic_if, ic_consecutivo, cg_titulo_aviso, cg_contenido_aviso, " +
					" cg_boton_aviso, bi_documento, " +
					" cg_texto_aceptacion, cg_boton_aceptacion, " +
					" cs_mostrar, df_alta ) " +
					" VALUES (?, ?, ?, ?, ?, empty_blob(), ?, ?, ?, sysdate) ";

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setLong(2, consecutivo);
			ps.setString(3, contrato.getTituloAviso());
			ps.setString(4, contrato.getContenidoAviso());
			ps.setString(5, contrato.getBotonAviso());
			ps.setString(6, contrato.getTextoAceptacion());
			ps.setString(7, contrato.getBotonAceptacion());
			ps.setString(8, contrato.getMostrar());

			ps.executeUpdate();
			ps.close();

			contrato.setConsecutivo(String.valueOf(consecutivo));
			contrato.getContratoArchivo().guardarArchivoContratoIF(rutaArchivo, con);

			return String.valueOf(consecutivo);
		} catch(Exception e) {
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene los datos del Clausulado Electr�nico configurado para
	 * la if especificada. El archivo del contrato (pdf,ppt o doc) no
	 * se obtiene debido a que es un campo binario.
	 * Si el consecutivo es null obtiene todos los contratos configurados para
	 * la if especificada.
	 *
	 * @since F012-2008
	 * @param claveIF Clave de la IF
	 * @param consecutivo Consecutivo
	 * @return Lista de objetos ContratoIF con los datos del contrato. Si no
	 * 		hay datos regresa una lista vac�a.
	 *
	 */
	public List getDatosClausuladoIF(String claveIF, String consecutivo)
			throws NafinException {
		AccesoDB con = new AccesoDB();

		int ic_if = 0;
		long ic_consecutivo = 0;

		List registros = new ArrayList();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveIF==null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(claveIF);
			if (consecutivo != null) {
				ic_consecutivo = Long.parseLong(consecutivo);
			}

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" claveIF=" + claveIF + "\n" +
					" consecutivo = " + consecutivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" SELECT e.ic_if, e.cg_razon_social, " +
					" 	ce.ic_consecutivo, ce.cg_titulo_aviso, " +
					" 	ce.cg_contenido_aviso, ce.cg_boton_aviso, " +
					" 	ce.cg_ext_documento, " +
					" 	ce.cg_texto_aceptacion, ce.cg_boton_aceptacion, " +
					" 	ce.cs_mostrar, ce.df_alta,  " +
					" 	CASE WHEN ic_consecutivo = " +
					" 		(SELECT max(ic_consecutivo) FROM com_contrato_if WHERE ic_if = ?) THEN " +
					" 		'S' ELSE 'N' END AS ultimo " +
					" FROM comcat_if e, com_contrato_if ce " +
					" WHERE ce.ic_if = e.ic_if " +
					" AND ce.ic_if = ? " +
					((consecutivo!=null)?"AND ce.ic_consecutivo = ?":"") +
					" ORDER BY ce.ic_consecutivo " ;
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_if);
			if (consecutivo!=null) {
				ps.setLong(3, ic_consecutivo);
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ContratoIF contrato = new ContratoIF();

				contrato.setClaveIF(rs.getString("ic_if"));
				contrato.setConsecutivo(rs.getString("ic_consecutivo"));
				contrato.setTituloAviso(rs.getString("cg_titulo_aviso"));
				contrato.setContenidoAviso(rs.getString("cg_contenido_aviso"));
				contrato.setBotonAviso(rs.getString("cg_boton_aviso"));
				contrato.setTextoAceptacion(rs.getString("cg_texto_aceptacion"));
				contrato.setBotonAceptacion(rs.getString("cg_boton_aceptacion"));
				contrato.getContratoArchivo().setExtension(rs.getString("cg_ext_documento"));
				contrato.setMostrar(rs.getString("cs_mostrar"));
				contrato.setFechaAlta(rs.getTimestamp("df_alta"));
				contrato.setNombreIF(rs.getString("cg_razon_social"));
				contrato.setUltimo( ( (rs.getString("ultimo").equals("S"))?true:false ) );

				registros.add(contrato);
			}

			rs.close();
			ps.close();

			return registros;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene los datos del ultimo Clausulado Electr�nico configurado para
	 * la if especificada. El archivo del contrato (pdf,ppt o doc) no
	 * se obtiene debido a que es un campo binario.
	 *
	 * @since F012-2008
	 * @param claveIF Clave de la if
	 * @return Objeto de ContratoIF con los datos del contrato.
	 *
	 */
	public ContratoIF getDatosUltimoClausuladoIF(String claveIF)
			throws NafinException {
		AccesoDB con = new AccesoDB();

		int ic_if = 0;

		ContratoIF contrato = new ContratoIF();
		String consecutivo = "";

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveIF==null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(claveIF);

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" claveIf=" + claveIF );
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" SELECT max(ic_consecutivo) as consecutivo" +
					" FROM com_contrato_if " +
					" WHERE ic_if = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				consecutivo = rs.getString("consecutivo");
			}

			rs.close();
			ps.close();

			List l = this.getDatosClausuladoIF(claveIF, consecutivo);
			if (l.size() > 0) {
				contrato = (ContratoIF)l.get(0);
			}

			return contrato;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
//==============================================================================================================================================================>>
	/**
	 * Verifica si la PYME cuya clave se proporciona en <tt>icPyme</tt>, que trabaja
	 * con la EPO cuyo id es <tt>icEpo</tt>, se encuentra bajo la version cuatro del convenio
	 *
	 * @since F014-2008
	 * @param icEpo Clave de la Epo
	 * @param icPyme Clave de la Pyme
	 * @return boolean <tt>true</tt> si la pyme esta bajo el esquema del convenio
	 *         4, <tt>false</tt> en caso contrario
	 *
	 */

	public boolean esConvenioCuatro(String icEpo, String icPyme)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::esConvenioCuatro(E)");

		if(icEpo == null || icEpo.equals("")){
			System.out.println("ParametrosDescuentoBean::esConvenioCuatro(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			hayConvenioCuatro		= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                              									"  +
					"     DECODE(COUNT(1),0,'false','true') AS ES_CONVENIO_CUATRO  "  +
					"FROM                                                          "  +
					"    comrel_pyme_epo cpe,                                      "  +
					"    comrel_pyme_if cpi,                                    	"  +
					"    comrel_cuenta_bancaria ccb,                            	"  +
					"    comcat_pyme pym, comcat_if cif                         	"  +
					"WHERE                                                      	"  +
					"    cpe.ic_epo               = cpi.ic_epo             AND  	"  +
					"    cpe.ic_pyme              = pym.ic_pyme            AND  	"  +
					"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  	"  +
					"    cpi.ic_if                = cif.ic_if              AND  	"  +
					"    cpe.ic_pyme              = ccb.ic_pyme            AND  	"  +
					"    ccb.cs_borrado           = ?                      AND  	"  + // 'N'
					"    ccb.ic_moneda            = ?                      AND  	"  + // 1
					"    cpi.cs_vobo_if           = ?                      AND  	"  + // 'S'
					"    cpi.cs_borrado           = ?                      AND  	"  + // 'N'
					"    cpi.cs_opera_descuento   = ?                      AND  	"  + // 'S'
					"    cpe.cs_aceptacion        = ?                      AND  	"  + // 'H'
					"    pym.ic_version_convenio  = ?                      AND  	"  + // 4
					"    cpe.ic_epo               = ?                      AND  	"  + // 	EPO
					"    pym.ic_pyme              = ? 								   	"  ; //  PYME

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(4));
				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(icPyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					hayConvenioCuatro = registros.getString("ES_CONVENIO_CUATRO").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::esConvenioCuatro(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::esConvenioCuatro(S)");
		}

		return hayConvenioCuatro;
	}

	/**
	 * Verifica si la PYME cuya clave se proporciona en <tt>icPyme</tt>, que trabaja
	 * con la EPO cuyo id es <tt>icEpo</tt>, se encuentra bajo la version tres del convenio
	 *
	 * @since F014-2008
	 * @param icEpo Clave de la Epo
	 * @param icPyme Clave de la Pyme
	 * @return boolean <tt>true</tt> si la pyme esta bajo el esquema del convenio
	 *         3, <tt>false</tt> en caso contrario
	 *
	 */

	public boolean esConvenioTres(String icEpo, String icPyme)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::esConvenioTres(E)");

		if(icEpo == null || icEpo.equals("")){
			System.out.println("ParametrosDescuentoBean::esConvenioTres(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			hayConvenioTres		= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                              									"  +
					"     DECODE(COUNT(1),0,'false','true') AS ES_CONVENIO_TRES    "  +
					"FROM                                                          "  +
					"    comrel_pyme_epo cpe,                                      "  +
					"    comrel_pyme_if cpi,                                    	"  +
					"    comrel_cuenta_bancaria ccb,                            	"  +
					"    comcat_pyme pym, comcat_if cif                         	"  +
					"WHERE                                                      	"  +
					"    cpe.ic_epo               = cpi.ic_epo             AND  	"  +
					"    cpe.ic_pyme              = pym.ic_pyme            AND  	"  +
					"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  	"  +
					"    cpi.ic_if                = cif.ic_if              AND  	"  +
					"    cpe.ic_pyme              = ccb.ic_pyme            AND  	"  +
					"    ccb.cs_borrado           = ?                      AND  	"  + // 'N'
					"    ccb.ic_moneda            = ?                      AND  	"  + // 1
					"    cpi.cs_vobo_if           = ?                      AND  	"  + // 'S'
					"    cpi.cs_borrado           = ?                      AND  	"  + // 'N'
					"    cpi.cs_opera_descuento   = ?                      AND  	"  + // 'S'
					"    cpe.cs_aceptacion        = ?                      AND  	"  + // 'H'
					"    pym.ic_version_convenio  = ?                      AND  	"  + // 3
					"    cpe.ic_epo               = ?                      AND  	"  + // 	EPO
					"    pym.ic_pyme              = ? 								   	"  ; //  PYME

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(3));
				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(icPyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					hayConvenioTres = registros.getString("ES_CONVENIO_TRES").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::esConvenioTres(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::esConvenioTres(S)");
		}

		return hayConvenioTres;
	}

	/**
	 * Verifica si la PYME cuya clave se proporciona en <tt>icPyme</tt>, que trabaja
	 * con la EPO cuyo id es <tt>icEpo</tt>, ya ha establecido un orden de operacion
	 * con sus intermediarios financieros; esto aplica solo para PYMEs bajo el convenio
	 * de version 3.
	 *
	 * @since F014-2008
	 * @param icEpo Clave de la Epo
	 * @param icPyme Clave de la Pyme
	 * @return boolean <tt>true</tt> si la pyme ya ha especificado el orden de operacion
	 *         con sus intermediarios financieros, <tt>false</tt> en caso contrario
	 *
	 */

	public boolean hayOrdenDeOperacion(String icEpo, String icPyme)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::hayOrdenDeOperacion(E)");

		if(icEpo == null || icEpo.equals("")){
			System.out.println("ParametrosDescuentoBean::hayOrdenDeOperacion(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			hayOrden					= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                              									    "  +
					"     DECODE(COUNT(1),0,'false','true') AS HAY_ORDEN_DE_OPERACION  "  +
					"FROM                                                              "  +
					"    comrel_pyme_epo cpe,                                          "  +
					"    comrel_pyme_if cpi,                                    	    "  +
					"    comrel_cuenta_bancaria ccb,                            	    "  +
					"    comcat_pyme pym, comcat_if cif                         	    "  +
					"WHERE                                                      	    "  +
					"    cpe.ic_epo               = cpi.ic_epo             AND  	    "  +
					"    cpe.ic_pyme              = pym.ic_pyme            AND  	    "  +
					"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  	    "  +
					"    cpi.ic_if                = cif.ic_if              AND  	    "  +
					"    cpe.ic_pyme              = ccb.ic_pyme            AND  	    "  +
					"    ccb.cs_borrado           = ?                      AND  	    "  + // 'N'
					"    ccb.ic_moneda            = ?                      AND  	    "  + //  1
					"    cpi.cs_vobo_if           = ?                      AND  	    "  + // 'S'
					"    cpi.cs_borrado           = ?                      AND  	    "  + // 'N'
					"    cpi.cs_opera_descuento   = ?                      AND  	    "  + // 'S'
					"    cpe.cs_aceptacion        = ?                      AND  	    "  + // 'H'
					"    pym.ic_version_convenio  = ?                      AND  	    "  + //  3 //4
					"	  cpi.ig_orden             is not null              AND         "  + //  null
					"    cpe.ic_epo               = ?                      AND  	    "  + //  EPO
					"    pym.ic_pyme              = ? 								   	    "  ; //  PYME

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(3));// anterior 4
				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(icPyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					hayOrden = registros.getString("HAY_ORDEN_DE_OPERACION").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::hayOrdenDeOperacion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::hayOrdenDeOperacion(S)");
		}

		return hayOrden;
	}

	/**
	 * Este metodo devuelve el nombre de la PYME.
	 *
	 * @since F014-2008
	 * @param icPyme Clave de la Pyme
	 * @return String con el nombre de la pyme, tanto sea esta fisica o moral,
	 *         <tt>null</tt> en caso contrario
	 */

	public String getNombrePyme(String icPyme)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::getNombrePyme(E)");

		if(icPyme == null || icPyme.equals("")){
			System.out.println("ParametrosDescuentoBean::getNombrePyme(S)");
			return null;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			nombrePyme				= null;
		try {
				con.conexionDB();
				qrySentencia =
						"SELECT 										"  +
						"	CG_RAZON_SOCIAL AS NOMBRE_PYME 	"  +
						"FROM 										"  +
						"	COMCAT_PYME 							"  +
						"WHERE 										"  +
						"	IC_PYME = ?								"; // 	PYME

				lVarBind.add(new Integer(icPyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					nombrePyme = registros.getString("NOMBRE_PYME");
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getNombrePyme(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getNombrePyme(S)");
		}

		return nombrePyme;
	}

	/**
	 * Este metodo devuelve una lista de intermediarios financieros con los que
	 * la PYME tiene una cuenta autorizada.
	 *
	 * @since 	F014-2008
	 * @param 	icEpo Clave de la EPO con la que trabaja la PYME
	 * @param 	icPyme Clave de la PYME
	 * @return 	List con los intermediarios financieros, lista vacia en
	 *				caso contrario.
	 *
	 */
	public List getListaDeIntermediariosFinancieros(String icEpo,String icPyme)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::getListaDeIntermediariosFinancieros(E)");
		List lista = new ArrayList();

		if(icEpo == null || icEpo.equals("")){
			System.out.println("ParametrosDescuentoBean::getListaDeIntermediariosFinancieros(S)");
			return lista;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
						"SELECT  "  +
						"    cif.ic_if                                  AS IC_IF,  "  +
						"    cif.cg_razon_social                        AS NOMBRE_IF, "  +
						"    DECODE(TO_CHAR(cpi.df_vobo_if,'MM'), "  +
						"              1,   'Enero',        2, 'Febrero',    3,'Marzo', "  +
						"              4,   'Abril',        5, 'Mayo',       6,'Junio', "  +
						"              7,   'Julio',        8, 'Agosto',     9,'Septiembre', "  +
						"              10,  'Octubre',     11, 'Noviembre', 12,'Diciembre')  "  +
						"    || TO_CHAR(cpi.df_vobo_if,' DD, YYYY hh24:mi') AS FECHA_Y_HORA_LIBERACION, "  +
						"		cpi.ig_orden AS ORDEN, " +
						"		ccb.ic_cuenta_bancaria AS CUENTA_BANCARIA " +
						"FROM  "  +
						"    comrel_pyme_epo cpe,  "  +
						"    comrel_pyme_if cpi,  "  +
						"    comrel_cuenta_bancaria ccb,  "  +
						"    comcat_pyme pym,  "  +
						"    comcat_if cif "  +
						"WHERE  "  +
						"    cpe.ic_epo               = cpi.ic_epo             AND  "  +
						"    cpe.ic_pyme              = pym.ic_pyme            AND  "  +
						"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  "  +
						"    cpi.ic_if                = cif.ic_if              AND  "  +
						"    cpe.ic_pyme              = ccb.ic_pyme            AND  "  +
						"    ccb.cs_borrado           = ?                      AND  "  + // 'N'
						"    ccb.ic_moneda            = ?                      AND  "  + // 1
						"    cpi.cs_vobo_if           = ?                      AND  "  + // 'S'
						"    cpi.cs_borrado           = ?                      AND  "  + // 'N'
						"    cpi.cs_opera_descuento   = ?                      AND  "  + // 'S'
						"    cpe.cs_aceptacion        = ?                      AND  "  + // 'H'
						"    pym.ic_version_convenio  = ?                      AND  "  + // 3 //4
						"    cpe.ic_epo               = ?                      AND  "  + // EPO
						"    pym.ic_pyme              = ?                           "  + // PYME
						"ORDER BY cpi.df_vobo_if asc ";

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(3)); // Anterior 4
				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(icPyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					registro.put("IC_IF",						registros.getString("IC_IF"));
					registro.put("NOMBRE_IF",					registros.getString("NOMBRE_IF"));
					registro.put("FECHA_Y_HORA_LIBERACION",registros.getString("FECHA_Y_HORA_LIBERACION"));
					registro.put("ORDEN",						registros.getString("ORDEN"));
					registro.put("CUENTA_BANCARIA",			registros.getString("CUENTA_BANCARIA"));
					lista.add(registro);
				}

		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getListaDeIntermediariosFinancierosException)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getListaDeIntermediariosFinancieros(S)");
		}
		return lista;
	}

	/**
	 * Este metodo devuelve el nombre del IF cuya clave se especifica en <tt>claveIF</tt>
	 *
	 * @since 	F014-2008
	 * @param 	claveIF Clave del IF.
	 * @return 	String con el nombre del intermediario financiero.
	 *
	 */
	public String getNombreIF(String claveIF)
		throws NafinException {

		System.out.println("ParametrosDescuentoBean::getNombreIF(E)");
		String nombreIF = "";

		if(claveIF == null || claveIF.equals("")){
			System.out.println("ParametrosDescuentoBean::getNombreIF(S)");
			return nombreIF;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
						"SELECT " +
						"	CG_RAZON_SOCIAL AS NOMBRE_IF " +
						"FROM " +
						"	COMCAT_IF " +
						"WHERE " +
						"	IC_IF           = ? "; // ic_if

				lVarBind.add(new Integer(claveIF));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					nombreIF = registros.getString("NOMBRE_IF");
				}

		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getNombreIF(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getNombreIF(S)");
		}
		return nombreIF;
	}
	/**
	 * Valida la contrase�a proporcionada por la PYME
	 *
	 * @since 	F014-2008
	 * @param 	cgLogin 		Clave del Usuario.
	 * @param 	cgPassword  Contrase�a
	 * @param	epo			Clave de la EPO
	 * @return 	boolean con valor igual a <tt>true</tt> si la contrase�a y clave
	 *				usuario son correctas, <tt>false</tt> en caso contrario.
	 */
	 public String validaContrasenna(String cgLogin, String cgPassword,  String epo, String iNoCliente , String sesIdiomaUsuario, String tipoUsuario,  String remoteAddress, String remoteHost, String sesCveSistema)
	 	throws NafinException {

		System.out.println("ParametrosDescuentoBean::validaCotrasenna(E)");

		String 	resultado			= "true";
		boolean	actualizarMensaje	= true;

		try {
			ISeleccionDocumento 		BeanSeleccionDocumento 	= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

			try{
				if("PYME".equals(tipoUsuario)){
					BeanSeleccionDocumento.vvalidarUsuario(iNoCliente, cgLogin, cgPassword, remoteAddress, remoteHost, sesCveSistema);
				}
			}catch(NafinException ne){
				resultado 			= "intento_fallido";
				actualizarMensaje = false;
				throw ne;
			}

			// La clave de sesion de derechos solo se usa en la seleccion de documentos
			/*if(!BeanSegFacultad.vvalidarCveCesion(cgLogin)){
				resultado 			= "false";
				actualizarMensaje = false;
			}*/

		}catch(NafinException ne){
			System.out.println("ParametrosDescuentoBean::validaCotrasenna(Exception)");
			if(actualizarMensaje){
				resultado = ne.getMessage(sesIdiomaUsuario);
			}
		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::validaCotrasenna(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}

		System.out.println("ParametrosDescuentoBean::validaCotrasenna(S)");
		return resultado;
	}

	 /**
	 * Este metodo se encarga de actualizar el orden de prioridad de los
	 * intermediarios financieron para una una pyme que trabaja con una
	 * EPO en particular.
	 *
	 * @since 	F014-2008
	 * @param 	icEpo Clave de la EPO con la que trabaja la PYME
	 * @param 	icPyme Clave de la PYME
	 * @param	listaIntermediarios	lista de HashMaps que continen las llaves "CLAVE", "VALOR"
	 *          que almacenan la "Clave del IF" y la "orden" especificado por la PYME.
	 * @return 	void
	 */
	public void actualizaOrdenDeIntermediariosFinancieros(String icEpo,String icPyme, List listaIntermediarios)
		throws NafinException {
		System.out.println("ParametrosDescuentoBean::actualizaOrdenDeIntermediariosFinancieros(E)");

		AccesoDB		con				= new AccesoDB();
      String		qrySentencia   = "";
		List 			lVarBind			= new ArrayList();
		boolean		lbOk				= true;

		try {
			con.conexionDB();

			String fechaActualizacion = getFechaDeActualizacion();

			for(int i=0;i<listaIntermediarios.size();i++){
				Map intermediario = (Map) listaIntermediarios.get(i);

				String claveIF 			= (String) intermediario.get("CLAVE");
				String orden 				= (String) intermediario.get("ORDEN");
				String cuentaBancaria	= (String) intermediario.get("CUENTA_BANCARIA");

				System.out.println(" +++++++++++claveIF 			" + (String) intermediario.get("CLAVE"));// Debug info
				System.out.println(" +++++++++++orden 			   " + (String) intermediario.get("ORDEN"));// Debug info
				System.out.println(" +++++++++++cuentaBancaria	" + (String) intermediario.get("CUENTA_BANCARIA"));// Debug info

				qrySentencia =
					"UPDATE                          										" +
					"	COMREL_PYME_IF                										" +
					"SET                             										" +
					"	IG_ORDEN = ?,                 										" + // ORDEN
					"	DF_PARAMETRIZACION_ORDEN = TO_DATE(?,'DD/MM/YYYY HH:MI:SS') " + // FECHA PARAMETRIZACION
					"WHERE                           										" +
					"	IC_EPO               = ?  AND 										" + // icEpo
					"	IC_IF                = ?  AND 										" + // claveIF
					"	CS_VOBO_IF           = ?  AND 										" + // 'S'
					"	CS_BORRADO           = ?  AND 										" + // 'N'
					"	CS_OPERA_DESCUENTO   = ?  AND 										" + // 'S'
					"	IC_CUENTA_BANCARIA   = ?      										" ; // cuentaBancaria

				lVarBind.clear();
				lVarBind.add(new Integer(orden));
				lVarBind.add(fechaActualizacion);
				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(claveIF));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add(cuentaBancaria);

				con.ejecutaUpdateDB(qrySentencia, lVarBind);



			}

			qrySentencia =
				"UPDATE                        " +
				"	COMCAT_PYME                 " +
				"SET                           " +
				"  CS_DSCTO_AUTOMATICO	= 'S', " +
				"	CS_DSCTO_AUTO_PROC	= 'N'	 " +
				"WHERE                         " +
				"	IC_PYME = ?                 " ;

			lVarBind.clear();
			lVarBind.add(new Integer(icPyme));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			lbOk = false;
			System.out.println("ParametrosDescuentoBean::actualizaOrdenDeIntermediariosFinancieros(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(lbOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::actualizaOrdenDeIntermediariosFinancieros(S)");
		}
	}

	//////////////////////////////////////
	/**
	 * Este metodo se encarga de buscar una pyme de acuerdo al formulario
	 * pudiendo existir b�squedas genericas
	 *
	 * @since 	F014-2008
	 * @param 	nae N@E de la PYME
	 * @param 	rfc Rfc de la PYME que puede contener * para busqueda generica
	 * @param	nombre Nombre de la pyme que puede contener *
	 * @param	sirac	Numero de Sirac
	 * @return 	lista con los registros de las pyme encontradas
	 */
	public List getPymeDescuento(String nae,String rfc, String nombre, String sirac)
		throws NafinException {
		System.out.println("ParametrosDescuentoBean::getPymeDescuento(E)");

		List 				lista 					= new ArrayList();
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String				condicion			= "";

		// Realizar consulta
		try {
				con.conexionDB();
				qrySentencia =
						"SELECT          																				" +
						"	/*+ use_nl(p pe crn crp)*/ 	" +
						" 	DISTINCT p.ic_pyme AS IC_PYME, '' AS CG_PYME_EPO_INTERNO, 					" +
						"	p.cg_razon_social AS RAZONSOCIAL, 													" +
						" 	crn.ic_nafin_electronico || ' ' || p.cg_razon_social DESPLIEGA, 			" +
						" 	crn.ic_nafin_electronico NAE 															" +
						"FROM 																							" +
						"	comrel_pyme_epo pe, 																		" +
						//" 	com_documento d, 																		" +
						" 	comrel_nafin crn, 																		" +
						" 	comcat_pyme p, 																			" +
						" 	comrel_producto_epo crp 																" +
						"WHERE 																							" +
						"	p.ic_pyme 					= pe.ic_pyme 			AND								" +
						//" 	d.ic_pyme 					= pe.ic_pyme 		AND								" +
						//" 	d.ic_epo 					= pe.ic_epo 		AND								" +
						"  pe.ic_pyme 					= crn.ic_epo_pyme_if AND 								" +
						" 	crn.cg_tipo 				= ? 						AND								" +
						" 	pe.cs_habilitado 			= ? 						AND								" +
						//"  pe.cg_pyme_epo_interno IS NOT NULL 			AND								" +
						" 	p.cs_habilitado 			= ? 						AND								" +
						" 	pe.ic_epo 					= crp.ic_epo 			AND								" +
                  " 	crp.cs_desc_auto_pyme 	= ? 						AND								" +
						"  p.ic_version_convenio	= ? 															";

					lVarBind.add("P");
					lVarBind.add("S");
					lVarBind.add("S");
					lVarBind.add("S");
					lVarBind.add(new Integer("3")); // Anterior 4

							if(!"".equals(nae)){
							condicion += "	AND crn.ic_nafin_electronico = ?";
							lVarBind.add(nae);}
							if(!"".equals(rfc)){
							//condicion += "	AND UPPER(cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
							condicion += " AND cg_rfc LIKE nvl(replace(upper(?), '*', '%'), '%') ";
							lVarBind.add(rfc);}
							if(!"".equals(nombre)){
							condicion += "	AND UPPER(cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
							lVarBind.add(nombre);}
							if(!"".equals(sirac)){
							condicion += "	AND p.in_numero_sirac = ?";
							lVarBind.add(sirac);}

						qrySentencia += condicion  + " " +
							" ORDER BY p.cg_razon_social " ;

				//System.out.println("::::qrySentencia:::" + qrySentencia);
				//System.out.println("::::lVarBind:::" + lVarBind);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					registro.put("IC_PYME",						registros.getString("IC_PYME"));
					registro.put("CG_PYME_EPO_INTERNO",		registros.getString("CG_PYME_EPO_INTERNO"));
					registro.put("RAZONSOCIAL",				registros.getString("RAZONSOCIAL"));
					registro.put("DESPLIEGA",					registros.getString("DESPLIEGA"));
					registro.put("NAE",							registros.getString("NAE"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getPymeDescuento(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getPymeDescuento(S)");
		}
		return lista;
	}

	/**
	 * Devuelve la lista de las EPO con el par�metro
	 * Permitir el descuento autom�tico a PYMES
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_pyme - Llave de la pyme a buscar
	 */
	public List getEpoParamDescAut(String ic_pyme)throws NafinException {
		System.out.println("ParametrosDescuentoBean::getEpoParamDescAut(E)");

		List 				lista 					= new ArrayList();
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		// Realizar consulta
		try {
			con.conexionDB();
			qrySentencia =
					" SELECT E.ic_epo AS CLAVE, E.cg_razon_social as DESCRIPCION " +
					" FROM comcat_epo E, comrel_producto_epo PE, comrel_pyme_epo P, comcat_pyme PY " +
					" WHERE E.cs_habilitado= ? " +
					" AND PE.cs_desc_auto_pyme = ? " +
					" AND E.ic_epo = PE.ic_epo " +
					" AND P.ic_epo = E.ic_epo " +
					" AND PE.ic_epo = P.ic_epo " +
					" AND PY.ic_pyme = p.ic_pyme " +
					" AND PY.ic_pyme = ? " ;

			lVarBind.add("S");
			lVarBind.add("S");
			lVarBind.add(ic_pyme);

			qrySentencia += " ORDER BY E.cg_razon_social " ;

			registros = con.consultarDB(qrySentencia, lVarBind, false);

			while(registros != null && registros.next()){
				HashMap registro = new HashMap();
				registro.put("CLAVE",						registros.getString("CLAVE"));
				registro.put("DESCRIPCION",				registros.getString("DESCRIPCION"));
				lista.add(registro);
			}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getEpoParamDescAut(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getEpoParamDescAut(S)");
		}
		return lista;
	}//getEpoParamDescAut

	/**
	 * Este metodo verifica si la EPO en cuestion tiene habilitado el numero SIAFF
	 *
	 * @since 	F045-2008
	 * @param 	icEPO Clave de la EPO
	 * @return 	devuelve un <tt>boolean</tt>, <tt>true</tt> si el numero SIAFF
	 *				se encuentra habilitado; <tt>false</tt> en caso contrario.
	 */
	public boolean estaHabilitadoNumeroSIAFF(String icEpo)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::estaHabilitadoNumeroSIAFF(E)");

		if(icEpo == null || icEpo.equals("")){
			System.out.println("ParametrosDescuentoBean::estaHabilitadoNumeroSIAFF(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			hayParametrizacion	= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 										" +
					"	DECODE(CS_PUBLICACION_EPO_PEF,'S','true','false' ) AS MOSTRAR_PARAMETRO " +
					"FROM  										" +
					"	COMREL_PRODUCTO_EPO 					" +
					"WHERE 										" +
					"	IC_EPO 					= ?    AND 	" +
					"	IC_PRODUCTO_NAFIN 	= ?			";  //  DESCUENTO ELECTRONICO: 1

				lVarBind.add(new Integer(icEpo));
				lVarBind.add(new Integer(1));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					hayParametrizacion = registros.getString("MOSTRAR_PARAMETRO").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::estaHabilitadoNumeroSIAFF(Exception)");
			System.out.println("  icEpo = " + icEpo);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::estaHabilitadoNumeroSIAFF(S)");
		}

		return hayParametrizacion;
	}

	/**
	 * Este metodo devuelve la direccion de la Pyme seleccionada
	 *
	 * @since 	F014-2008
	 * @param 	ic_pyme Clave de la PYME
	 * @return 	resultados con los registros de las pyme encontradas
	 */
	public HashMap getDomicilioPyme(String ic_pyme)
	  throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();

		System.out.println("ParametrosDescuentoBean::getDomicilioPyme(E)");
		try
		{
			con.conexionDB();
			qrySentencia = " SELECT P.cg_rfc as rfcPyme, D.cg_calle||' '||D.cg_numero_ext||' '||D.cg_numero_int as domicilio, " +
						" D.cg_colonia as colonia, D.cn_cp as codigoPostal, D.cg_telefono1 as telefono1 "+
						" FROM comcat_pyme P, com_domicilio D "+
						" WHERE P.ic_pyme = D.ic_pyme "+
						" AND P.ic_pyme = ? " ;
			lVarBind.add(ic_pyme);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("rfcPyme",			(registros.getString("rfcPyme")==null?"":registros.getString("rfcPyme")));
				resultados.put("domicilio",		(registros.getString("domicilio")==null?"":registros.getString("domicilio")));
				resultados.put("colonia",			(registros.getString("colonia")==null?"":registros.getString("colonia")));
				resultados.put("codigoPostal",	(registros.getString("codigoPostal")==null?"":registros.getString("codigoPostal")));
				resultados.put("telefono1",		(registros.getString("telefono1")==null?"":registros.getString("telefono1")));
			}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getDomicilioPyme(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getDomicilioPyme(S)");
		}
		return resultados;
	}//getDomicilioPyme

	/**
	 * Este metodo se encarga de buscar las EPO de acuerdo a un banco de fondeo
	 *
	 * @since 	F014-2008
	 * @param 	ic_banco_fondeo Banco de fondeo
	 * @return 	lista con los registros de las EPO encontradas
	 */
	public List getEPOxBancoFondeo(String ic_banco_fondeo)
		throws NafinException {
		System.out.println("ParametrosDescuentoBean::getEPOxBancoFondeo(E)");

		List 				lista 					= new ArrayList();
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		// Realizar consulta
		try {
				con.conexionDB();
				qrySentencia =
						" SELECT E.ic_epo, E.cg_razon_social " +
						" FROM COMCAT_EPO E, COMREL_PRODUCTO_EPO PE " +
						" WHERE E.IC_EPO = PE.IC_EPO " +
						"		AND PE.IC_PRODUCTO_NAFIN = 1 " +
						"		AND e.cs_habilitado='S' ";
						if(!"".equals(ic_banco_fondeo)&&ic_banco_fondeo!=null)
						{
						qrySentencia +=	"		AND E.ic_banco_fondeo = ?" ;
							lVarBind.add(ic_banco_fondeo);
						}

						qrySentencia +=" ORDER BY 2 " ;

				//System.out.println("::::qrySentencia:::" + qrySentencia);
				//System.out.println("::::lVarBind:::" + lVarBind);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					registro.put("IC_EPO",				registros.getString("ic_epo"));
					registro.put("CG_RAZON_SOCIAL",	registros.getString("cg_razon_social"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getEPOxBancoFondeo(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getEPOxBancoFondeo(S)");
		}
		return lista;
	}


	/**
	 * Este metodo devuelve los datos de la Pyme que se presentaran en la consulta
	 *
	 * @since 	F014-2008
	 * @param   ic_epo Clave de la EPO
	 * @param 	nae    Numero electronico de la Pyme
	 * @return 	resultados con los registros de las pyme encontradas
	 */
	public HashMap getConsultaDescuentoIf(String ic_epo, String nae)
	  throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();

		System.out.println("ParametrosDescuentoBean::getConsultaDescuentoIf(E)");
		try
		{
			con.conexionDB();
			qrySentencia = " SELECT DISTINCT cn.ic_nafin_electronico as NAE, cp.cg_razon_social as RAZON_SOCIAL, " +
								" 		to_char(cpi.df_parametrizacion_orden, 'DD/MM/YYYY hh24:mi') as FECHA_PARAMETRIZACION " +
								" FROM comcat_pyme cp, comrel_pyme_epo cpe, comrel_nafin cn, comrel_pyme_if cpi, comrel_cuenta_bancaria ccb " +
								" WHERE cpe.ic_pyme = cp.ic_pyme " +
								"		AND cn.ic_epo_pyme_if = cp.ic_pyme " +
								"		AND cpi.ic_epo = cpe.ic_epo " +
								"		AND cpi.df_parametrizacion_orden IS NOT NULL " +
								" 		AND cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria " +
								" 		AND cpe.ic_epo= ? " +
								"		AND cn.ic_nafin_electronico = ? " ;
			lVarBind.add(ic_epo);
			lVarBind.add(nae);

			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("NAE",							(registros.getString("NAE")==null?"":registros.getString("NAE")));
				resultados.put("RAZON_SOCIAL",				(registros.getString("RAZON_SOCIAL")==null?"":registros.getString("RAZON_SOCIAL")));
				resultados.put("FECHA_PARAMETRIZACION",	(registros.getString("FECHA_PARAMETRIZACION")==null?"":registros.getString("FECHA_PARAMETRIZACION")));
			}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getConsultaDescuentoIf(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getConsultaDescuentoIf(S)");
		}
		return resultados;
	}//getConsultaDescuentoIf

	/**
	 * Este metodo actualiza los cambios del orden de los bancos hechos por Nafin en la bitacora
	 *
	 * @since 	F014-2008
	 * @param   ic_epo Clave de la EPO
	 * @param 	nae    Numero electronico de la Pyme
	 * @return 	resultados con los registros de las pyme encontradas
	 */
	public String actualizaBitacoraOrdenIF(String ic_epo, String iNoUsuario, String ic_pyme, List listaIntermediariosAnteriores, List listaIntermediariosActuales)
	 throws NafinException {
		System.out.println("ParametrosDescuentoBean::actualizaBitacoraOrdenIF(E)");
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer valoresAnt = new StringBuffer();
		StringBuffer valoresAct = new StringBuffer();
		List valoresActuales = new ArrayList();
		List valoresAnteriores = new ArrayList();
		int i = 0;
		boolean lbOK = true;
		try
		{
			con.conexionDB();

			System.out.println("ic_epo 													= " + ic_epo ); // Debug info
			System.out.println("ic_pyme 													= " + ic_pyme ); // Debug info
			System.out.println("sgetNoNafinElectronico(ic_pyme, con, \"P\") 	= " + sgetNoNafinElectronico(ic_pyme, con, "P") ); // Debug info

			int tam = listaIntermediariosActuales.size();
			System.out.println("listaIntermediariosActuales.size() = " + listaIntermediariosActuales.size() ); // Debug info
			//Obtener los valores anteriores
			String [] nombresAnteriores = new String[tam];
			Iterator regn = listaIntermediariosAnteriores.iterator();
			while(regn.hasNext())
			{
				HashMap registrosAnteriores = (HashMap)regn.next();
				System.out.println(" registrosAnteriores.get(\"ORDEN\") = " + (String)registrosAnteriores.get("ORDEN") ); // Debug info
				System.out.println(" registrosAnteriores.get(\"CLAVE\") = " + (String)registrosAnteriores.get("CLAVE") ); // Debug info
				valoresAnteriores.add("Orden : " + (String)registrosAnteriores.get("ORDEN") + " Clave : " + (String)registrosAnteriores.get("CLAVE"));
				nombresAnteriores[i] = getNombreIF((String)registrosAnteriores.get("CLAVE")) + " ";
				i++;
			}

			System.out.println("------------------------------");					// Debug info
			for(int indice=0;indice<valoresAnteriores.size();indice++){			// Debug info
				String valor = (String)valoresAnteriores.get(indice);				// Debug info
				System.out.println("valoresAnteriores["+indice+"] = " + valor);// Debug info
			}																						// Debug info
			System.out.println("------------------------------");					// Debug info
			for(int indice=0;indice<nombresAnteriores.length;indice++){			// Debug info
				System.out.println("nombresAnteriores["+indice+"] = " + nombresAnteriores[indice]);	// Debug info
			}																						// Debug info

			valoresAnt.append(Bitacora.getValoresBitacora(
					valoresAnteriores, nombresAnteriores));
			System.out.println("VAnteriores " + valoresAnt.toString());// Debug info

			//Obtener los valores actuales
			i = 0;
			String [] nombresActuales = new String[tam];
			Iterator reg = listaIntermediariosActuales.iterator();
			while(reg.hasNext())
			{
				Map registros = (Map)reg.next();
				valoresActuales.add("Orden : " + (String)registros.get("ORDEN") + " Clave : " + (String)registros.get("CLAVE"));
				nombresActuales[i] = getNombreIF((String)registros.get("CLAVE")) + " ";
				i++;
			}

			valoresAct.append(Bitacora.getValoresBitacora(
					valoresActuales, nombresActuales));
			System.out.println("VActuales " + valoresAct.toString());// Debug info

			//Insertar en la bitacora de cambios
			Bitacora.grabarEnBitacora(con, "DESCORDIF", "P", sgetNoNafinElectronico(ic_pyme, con, "P"),
					iNoUsuario, valoresAnt.toString(), valoresAct.toString());

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::actualizaBitacoraOrdenIF(Exception) "+e);
			e.printStackTrace();
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			System.out.println("ParametrosDescuentoBean::actualizaBitacoraOrdenIF(S)");
		}
		return "";
	 }

	 /**
	 * Obtiene el n�mero de N@E asociado al afiliado
	 * @param claveAfiliado Clave de afiliado
	 * @param con Conexion a la BD.
	 * @param tipoAfiliado Tipo de Afiliado: E Epo P Pyme I If... etc.
	 */
	private String sgetNoNafinElectronico(String claveAfiliado, AccesoDB con, String tipoAfiliado)
			throws SQLException {

		String numNafinElectronico = null;
		String strSQL =
				" SELECT ic_nafin_electronico " +
				" FROM comrel_nafin " +
				" WHERE ic_epo_pyme_if = ? " +
				" 	AND cg_tipo = ? ";
		PreparedStatement ps = con.queryPrecompilado(strSQL);
		ps.setInt(1, Integer.parseInt(claveAfiliado));
		ps.setString(2, tipoAfiliado);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
				numNafinElectronico = rs.getString("ic_nafin_electronico");
		}
		rs.close();
		ps.close();
		return numNafinElectronico;
	}

	public List getBancosOrden(String ic_epo, String ic_pyme)
		throws NafinException {
		AccesoDB 		con						= new AccesoDB();
		List 				lista 					= new ArrayList();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		// Realizar consulta
		try {
			con.conexionDB();
			qrySentencia = "SELECT  "  +
						"    nvl(cif.ic_if, '')                                AS IC_IF,  "  +
						"	  nvl(cpi.ig_orden, '') AS ORDEN " +
						"FROM  "  +
						"    comrel_pyme_epo cpe,  "  +
						"    comrel_pyme_if cpi,  "  +
						"    comrel_cuenta_bancaria ccb,  "  +
						"    comcat_pyme pym,  "  +
						"    comcat_if cif "  +
						"WHERE  "  +
						"    cpe.ic_epo               = cpi.ic_epo             AND  "  +
						"    cpe.ic_pyme              = pym.ic_pyme            AND  "  +
						"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  "  +
						"    cpi.ic_if                = cif.ic_if              AND  "  +
						"    cpe.ic_pyme              = ccb.ic_pyme            AND  "  +
						"    ccb.cs_borrado           = ?                      AND  "  + // 'N'
						"    ccb.ic_moneda            = ?                      AND  "  + // 1
						"    cpi.cs_vobo_if           = ?                      AND  "  + // 'S'
						"    cpi.cs_borrado           = ?                      AND  "  + // 'N'
						"    cpi.cs_opera_descuento   = ?                      AND  "  + // 'S'
						"    cpe.cs_aceptacion        = ?                      AND  "  + // 'H'
						"    pym.ic_version_convenio  = ?                      AND  "  + // 3 // 4
						"    cpe.ic_epo               = ?                      AND  "  + // EPO
						"    pym.ic_pyme              = ?                           "  + // PYME
						"ORDER BY cpi.df_vobo_if desc ";

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(3)); // Anterior 4
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer(ic_pyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					registro.put("CLAVE",						registros.getString("IC_IF"));
					registro.put("ORDEN",						registros.getString("ORDEN"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getEPO(Exception)");
			e.printStackTrace();
		}finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		System.out.println("ParametrosDescuentoBean::getEPO(S)");
		return lista;
	}



	private String	getFechaDeActualizacion()
		throws NafinException {
		System.out.println("ParametrosDescuentoBean::getFechaDeActualizacion(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			resultado				= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 	" +
					"	TO_CHAR(SYSDATE,'DD/MM/YYYY HH:MI:SS') AS FECHA " +
					"FROM 	" +
					"	DUAL 	";

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					resultado = registros.getString("FECHA");
				}

		}catch(Exception e){
			System.out.println("ParametrosDescuentoBean::getFechaDeActualizacion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getFechaDeActualizacion(S)");
		}

		return resultado;
	}

	public HashMap autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente)
		throws NafinException{

		System.out.println("ParametrosDescuentoBean::autenticaFirmaDigital(E)");

		pkcs7					= (pkcs7 			== null?"":pkcs7);
		textoFirmado		= (textoFirmado 	== null?"":textoFirmado);
		numeroCliente		= (numeroCliente 	== null?"":numeroCliente);
		serial				= (serial 			== null?"":serial);

		String 		folioCertificado 	= null;
		netropology.utilerias.Seguridad	seguridad			= null;
		char 			getReceipt 			= 'Y';
		HashMap  	resultado			= new HashMap();

		if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") && !numeroCliente.equals("") ) {
			folioCertificado 	= "01CC"+numeroCliente+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
			seguridad 			= new Seguridad();

			if (!seguridad.autenticar(folioCertificado, serial, pkcs7, textoFirmado, getReceipt)) {
				resultado.put("VERIFICACION_EXITOSA","false");
				resultado.put("MENSAJE_ERROR","La autentificacion no se llevo a cabo: \n"+seguridad.mostrarError());
			}else{
				resultado.put("VERIFICACION_EXITOSA","true");
				resultado.put("RECIBO",seguridad.getAcuse());
				resultado.put("FOLIO",folioCertificado);
			}

		}else{
			System.out.println("ParametrosDescuentoBean::autenticaFirmaDigital(Exception)");
			System.out.println("Uno o mas parametros vienen vacios: ");
			System.out.println("      textoFirmado   = " + textoFirmado);
			System.out.println("      serial         = " + serial);
			System.out.println("      numeroCliente  = " + numeroCliente);
			System.out.println("      pkcs7          = " + pkcs7);
			throw new NafinException("GRAL0021");
		}

		System.out.println("ParametrosDescuentoBean::autenticaFirmaDigital(S)");
		return resultado;
	}

	public boolean haSidoRechazadoElDescuentoAutomatico(String icPyme)
		throws NafinException {

		System.out.println("ParametrosDescuentoBean::haSidoRechazadoElDescuentoAutomatico(E)");

		if(icPyme == null || icPyme.equals("")){
			System.out.println("ParametrosDescuentoBean::haSidoRechazadoElDescuentoAutomatico(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			estaDeshabilitado		= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																	" +
               "	DECODE(COUNT(1),0,'false','true') AS ESTA_RECHAZADO 	" +
               "FROM 																	" +
               "  COMCAT_PYME 														" +
               "WHERE 																	" +
               "	IC_PYME 					= ? 	AND			 					" +
               "	CS_DSCTO_AUTOMATICO	= ? 										" ; // 'R' = rechazado


				lVarBind.add(new Integer(icPyme));
				lVarBind.add("R");

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					estaDeshabilitado = registros.getString("ESTA_RECHAZADO").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::haSidoRechazadoElDescuentoAutomatico(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::haSidoRechazadoElDescuentoAutomatico(S)");
		}
		return estaDeshabilitado;
	}

	/**
	 * Este metodo se encarga de rechazar(deshabilitar) el descuento automatico
	 *
	 * @since 	F014-2008
	 * @param 	icEpo Clave de la EPO con la que trabaja la PYME
	 * @param 	icPyme Clave de la PYME
	 * @param	listaIntermediarios	lista de HashMaps que continen las llaves "CLAVE", "VALOR"
	 *          que almacenan la "Clave del IF" y la "orden" especificado por la PYME.
	 * @return 	void
	 */
	public void rechazarDescuentoAutomatico(String icPyme)
		throws NafinException {

		System.out.println("ParametrosDescuentoBean::rechazarDescuentoAutomatico(E)");

		AccesoDB		con				= new AccesoDB();
      String		qrySentencia   = "";
		List 			lVarBind			= new ArrayList();
		boolean		lbOk				= true;

		try {
			con.conexionDB();

			// Borrar orden de los intermediarios financieros
			qrySentencia =
				"UPDATE 												" +
				"	COMREL_PYME_IF                    		" +
				"SET 													" +
				"	IG_ORDEN 						= NULL, 		" +
				"	DF_PARAMETRIZACION_ORDEN 	= SYSDATE  	" +
				"WHERE 												" +
				"	IC_CUENTA_BANCARIA IN						" +
				"		( 												" +
            "      	SELECT 									" +
				"				IC_CUENTA_BANCARIA 				" +
				"			FROM 										" +
				"				COMREL_CUENTA_BANCARIA			" +
				"			WHERE 									" +
				"				IC_PYME = ?							" + // icPyme
				"		)	AND										" +
				"		IG_ORDEN IS NOT NULL 					" ;

			lVarBind.add(new Integer(icPyme));
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

			// Poner bandera de descuento automatico en R = "Rechazado"
			qrySentencia =
				"UPDATE                        " +
				"	COMCAT_PYME                 " +
				"SET                           " +
				"  CS_DSCTO_AUTOMATICO	= 'R', " +
				"	CS_DSCTO_AUTO_PROC	= 'N'	 " +
				"WHERE                         " +
				"	IC_PYME = ?                 " ;

			lVarBind.clear();
			lVarBind.add(new Integer(icPyme));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			lbOk = false;
			System.out.println("ParametrosDescuentoBean::rechazarDescuentoAutomatico(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(lbOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::rechazarDescuentoAutomatico(S)");
		}
	}

//	FODEA 002 - 2009 (I)
	/**
	 * M�todo que verifica si la EPO seleccionada tiene campos adicionales parametrizados,
	 * as� como el n�mero de estos.
	 * @since FODEA 002 - 2009
	 * @param icEpo Clave de la EPO seleccionada.
	 * @return camposAdicionales N�mero de campos adicionales parametrizados.
	 * @throws NafinException Cuando ocurre un error durante la ejecuci�n del m�todo.
	 */
	public int getNumeroCamposAdicionales(String icEpo) throws NafinException{
		log.info("getNumeroCamposAdicionales(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		int camposAdicionales = 0;

		try {
			con.conexionDB();

			strSQL.append("SELECT COUNT(ic_no_campo) FROM comrel_visor WHERE ic_epo = ? AND ic_producto_nafin = ?");
			varBind.add(new Long(icEpo));
			varBind.add(new Integer(1));
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);

			rst = pst.executeQuery();

			while(rst.next()){camposAdicionales = rst.getInt(1);}

			rst.close();
			pst.close();
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getNumeroCamposAdicionales(S)");
		}
		return camposAdicionales;
	}

	/**
	 * M�todo que obtiene los nombres de los campos adicionales parametrizados de la EPO.
	 * @since FODEA 002 - 2009
	 * @param icEpo Clave de la EPO seleccionada.
	 * @return camposAdicionales Nombres de los campos adicionales parametrizados.
	 * @throws NafinException Cuando ocurre un error durante la ejecuci�n del m�todo.
	 */
	public List getNombresCamposAdicionales(String icEpo) throws NafinException{
		log.info("getNombresCamposAdicionales(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List camposAdicionales = new ArrayList();

		try {
			con.conexionDB();

			strSQL.append(" SELECT cg_nombre_campo FROM comrel_visor WHERE ic_epo = ? AND ic_producto_nafin = ?");
			varBind.add(new Long(icEpo));
			varBind.add(new Integer(1));
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);

			rst = pst.executeQuery();

			while(rst.next()){camposAdicionales.add(rst.getString(1) == null?"":rst.getString(1));}

			rst.close();
			pst.close();
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getNombresCamposAdicionales(S)");
		}
		return camposAdicionales;
	}

	/**
	 * M�todo que obtiene los valores de los campos adicionales parametrizados de la EPO.
	 * @since FODEA 002 - 2009
	 * @param icDocumento Clave del documento seleccionado.
	 * @param icEpo Clave de la EPO seleccionada.
	 * @param numeroCampos N�mero de campos adicionales parametrizados.
	 * @return camposAdicionales Datos contenidos dentro de los campos adicionales parametrizados.
	 * @throws NafinException Cuando ocurre un error durante la ejecuci�n del m�todo.
	 */
	public List getDatosCamposAdicionales(String icDocumento, String icEpo, int numeroCampos) throws NafinException{
		log.info("getDatosCamposAdicionales(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List camposAdicionales = new ArrayList();

		try {
			con.conexionDB();

			if(numeroCampos > 0){
				strSQL.append(" SELECT cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5");
				strSQL.append(" FROM com_documento");
				strSQL.append(" WHERE ic_documento = ?");
				strSQL.append(" AND ic_epo = ?");
				varBind.add(new Integer(icDocumento));
				varBind.add(new Long(icEpo));
				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind.toString());
				pst = con.queryPrecompilado(strSQL.toString(), varBind);

				rst = pst.executeQuery();

				while(rst.next()){
					camposAdicionales.add(rst.getString(1) == null?"":rst.getString(1));
					camposAdicionales.add(rst.getString(2) == null?"":rst.getString(2));
					camposAdicionales.add(rst.getString(3) == null?"":rst.getString(3));
					camposAdicionales.add(rst.getString(4) == null?"":rst.getString(4));
					camposAdicionales.add(rst.getString(5) == null?"":rst.getString(5));
				}
				rst.close();
				pst.close();
			}
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getDatosCamposAdicionales(S)");
		}
		return camposAdicionales;
	}
//	FODEA 002 - 2009 (F)

	/**
	 * Este metodo devuelve la clave de la pyme (IC_PYME), numero nafin electronico de la
	 * pyme, y su razon social
	 *
	 * @since 	F002-2009
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @param 	claveEPO                       Clave de la EPO con la que trabaja la PYME
	 * @return 	HashMap con los valores mencionados en la descripcion del metodo, en dado caso
	 *          que alguno de los parametros venga vacio devuelve <tt>null</tt>.
	 */
	public HashMap getParametrosPYME(String numeroDeProveedor, String claveEPO)
	  throws NafinException {

		System.out.println("ParametrosDescuentoBean::getParametrosPYME(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= null;

		if(numeroDeProveedor == null || numeroDeProveedor.equals("")){
			return null;
		}

		if(claveEPO == null || claveEPO.equals("")){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia =
				"SELECT " +
				"	/*+ ORDERED USE_NL(P,D) */ " +
				"	P.IC_PYME 					AS IC_PYME, 			" +
				"	P.CG_RAZON_SOCIAL			AS DESCRIPCION, 		" +
				"	PE.CG_PYME_EPO_INTERNO 	AS NUMERO_PROVEEDOR	" +
				"FROM 								" +
				"	COMREL_PYME_EPO 		PE, 	" +
				"	COMCAT_PYME 		  	P, 	" +
				" 	COM_DOCUMENTO 			D		" +
				"WHERE " +
				"	P.IC_PYME 					= 	PE.IC_PYME 	AND " +
				"	PE.ic_epo					=	? 				AND " +
				"	PE.cs_habilitado 			= 	?  			AND " + // 'S'
				"	P.cs_habilitado 			= 	?  			AND " + // 'S'
				"	P.cs_invalido 				!= ?  			AND " + // 'S'
				"	PE.IC_PYME 					= 	D.IC_PYME  	AND " +
				"	PE.IC_EPO 					= 	D.IC_EPO		AND " +
				"	PE.CG_PYME_EPO_INTERNO 	=  ? 					 " +
				"GROUP BY 						" +
				"	P.IC_PYME, 					" +
				"	P.CG_RAZON_SOCIAL, 		" +
				"	PE.CG_PYME_EPO_INTERNO 	" +
				"ORDER BY 						" +
				"	P.CG_RAZON_SOCIAL			";


			System.out.println("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			System.out.println("claveEPO = " + claveEPO ); // Debug info

			lVarBind.add(new Integer(claveEPO));
			lVarBind.add("S");
			lVarBind.add("S");
			lVarBind.add("S");
			lVarBind.add(numeroDeProveedor);

			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next()){
				resultados		= new HashMap();

				resultados.put("IC_PYME",				(registros.getString("IC_PYME")			  == null?"":registros.getString("IC_PYME")));
				resultados.put("DESCRIPCION",			(registros.getString("DESCRIPCION")		  == null?"":registros.getString("DESCRIPCION")));
				resultados.put("NUMERO_PROVEEDOR",	(registros.getString("NUMERO_PROVEEDOR") == null?"":registros.getString("NUMERO_PROVEEDOR")));
			}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getParametrosPYME(Exception)");
			System.out.println("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			System.out.println("claveEPO          = " + claveEPO ); // Debug info
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDescuentoBean::getParametrosPYME(S)");
		}
		return resultados;
	}

	public List getBancosOrden(String ic_pyme)
		throws NafinException {

		System.out.println("ParametrosDescuentoBean::getBancosOrden(E)");

		AccesoDB 		con						= new AccesoDB();
		List 				lista 					= new ArrayList();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		// Realizar consulta
		try {
			con.conexionDB();
			qrySentencia = "SELECT  "  +
						"    nvl(cif.ic_if, '')                                AS IC_IF,  "  +
						"	  nvl(cpi.ig_orden, '') AS ORDEN " +
						"FROM  "  +
						"    comrel_pyme_epo cpe,  "  +
						"    comrel_pyme_if cpi,  "  +
						"    comrel_cuenta_bancaria ccb,  "  +
						"    comcat_pyme pym,  "  +
						"    comcat_if cif "  +
						"WHERE  "  +
						"    cpe.ic_epo               = cpi.ic_epo             AND  "  +
						"    cpe.ic_pyme              = pym.ic_pyme            AND  "  +
						"    cpi.ic_cuenta_bancaria   = ccb.ic_cuenta_bancaria AND  "  +
						"    cpi.ic_if                = cif.ic_if              AND  "  +
						"    cpe.ic_pyme              = ccb.ic_pyme            AND  "  +
						"    ccb.cs_borrado           = ?                      AND  "  + // 'N'
						"    ccb.ic_moneda            = ?                      AND  "  + // 1
						"    cpi.cs_vobo_if           = ?                      AND  "  + // 'S'
						"    cpi.cs_borrado           = ?                      AND  "  + // 'N'
						"    cpi.cs_opera_descuento   = ?                      AND  "  + // 'S'
						"    cpe.cs_aceptacion        = ?                      AND  "  + // 'H'
						"    pym.ic_version_convenio  = ?                      AND  "  + // 3 // 4
						"    pym.ic_pyme              = ?                           "  + // PYME
						"ORDER BY cpi.df_vobo_if desc ";

				lVarBind.add("N");
				lVarBind.add(new Integer(1));
				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add("S");
				lVarBind.add("H");
				lVarBind.add(new Integer(3)); // Anterior 4
				lVarBind.add(new Integer(ic_pyme));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					registro.put("CLAVE",						registros.getString("IC_IF"));
					registro.put("ORDEN",						registros.getString("ORDEN"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ParametrosDescuentoBean::getBancosOrden(Exception)");
			e.printStackTrace();
		}finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		System.out.println("ParametrosDescuentoBean::getBancosOrden(S)");
		return lista;
	}

	public String setParamEpoPymeIf( List cveEpoPymeIfCta, String cs_tipo_val) throws NafinException{
		System.out.println("ParametrosDescuentoBean :: setParamEpoPymeIf(p,p,p)");
		AccesoDB con = new AccesoDB();
		PreparedStatement psi	= null, ps = null;
		String qrySentencia   = "";
		String qryCondicion = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		int cont = 0;
		int ordenIf =0;
		contenidoArchivo.append("****** OPERACIONES EFECTUADAS ****** "+new java.util.Date()+"\n");

		try {
			con.conexionDB();

			if(cveEpoPymeIfCta!=null && cveEpoPymeIfCta.size()>0){
				for(int x=0;x<cveEpoPymeIfCta.size();x++){
					List regCveEpoPymeIfCta = (List)cveEpoPymeIfCta.get(x);
					if(x==0)
						qryCondicion = " AND ((cpe.ic_pyme = "+(String)regCveEpoPymeIfCta.get(0)+" AND cpe.ic_epo = "+(String)regCveEpoPymeIfCta.get(1)+
											" AND cpi.ic_if = "+(String)regCveEpoPymeIfCta.get(2)+" AND cpi.ic_cuenta_bancaria = "+(String)regCveEpoPymeIfCta.get(3)+") ";
					else
						qryCondicion +=" OR (cpe.ic_pyme = "+(String)regCveEpoPymeIfCta.get(0)+" AND cpe.ic_epo = "+(String)regCveEpoPymeIfCta.get(1)+
											" AND cpi.ic_if = "+(String)regCveEpoPymeIfCta.get(2)+" AND cpi.ic_cuenta_bancaria = "+(String)regCveEpoPymeIfCta.get(3)+") ";
				}
				if(!"".equals(qryCondicion))
					qryCondicion +=") ";


				qrySentencia =
					"SELECT   cpe.ic_epo, cpe.ic_pyme, cpi.ic_if, cpi.ic_cuenta_bancaria, MIN (cpi.df_vobo_if)  " +
					"    FROM comrel_pyme_epo cpe, comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_pyme pym,  " +
					"         comrel_producto_epo pe " +
					"   WHERE cpe.ic_epo = cpi.ic_epo  " +
					"     AND cpe.ic_epo = pe.ic_epo " +
					"     AND cpe.ic_pyme = ccb.ic_pyme  " +
					"     AND cpe.ic_pyme = pym.ic_pyme  " +
					"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria  " +
					"     AND cpi.cs_vobo_if = ?  " +//S
					"     AND ccb.cs_borrado = ? " +//N
					"     AND cpi.cs_borrado = ?  " +//N
					"     AND cpi.cs_opera_descuento = ?  " +//S
					"     AND cpe.cs_aceptacion = ?  " +//H
					"     AND pym.ic_version_convenio in (?)  " +
					"     AND pe.ic_producto_nafin = ? " +
					"     AND (pe.cs_factoraje_vencido = ? OR pe.cs_factoraje_vencido IS NULL) " +
					"     AND (pe.cs_factoraje_distribuido = ? OR pe.cs_factoraje_distribuido IS NULL) " +
					qryCondicion+
					" GROUP BY cpe.ic_epo, cpe.ic_pyme, cpi.ic_if, cpi.ic_cuenta_bancaria  "+
					" ORDER BY MIN (cpi.df_vobo_if) ";

				psi = con.queryPrecompilado(qrySentencia);
				psi.setString(1,"S");
				psi.setString(2,"N");
				psi.setString(3,"N");
				psi.setString(4,"S");
				psi.setString(5,"H");
				psi.setInt(6,3);
				psi.setInt(7,1);
				psi.setString(8,"N");
				psi.setString(9,"N");
				System.out.println("\n qrySentencia: "+qrySentencia);
				ResultSet regEpoPymeMonedaIf = psi.executeQuery();

				//if(regEpoPymeMonedaIf.next()) {
				while(regEpoPymeMonedaIf.next()) {
					ordenIf = 0;
					String rs_ic_pyme = regEpoPymeMonedaIf.getString("ic_pyme");
					String rs_ic_epo  = regEpoPymeMonedaIf.getString("ic_epo");
					String rs_ic_if					= regEpoPymeMonedaIf.getString("ic_if");
					String rs_ic_cuenta_bancaria	= regEpoPymeMonedaIf.getString("ic_cuenta_bancaria");

					qrySentencia = "SELECT   NVL(MAX(cpi.ig_orden),0)  max_orden " +
									"    FROM comrel_pyme_epo cpe, comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_pyme pym  " +
									"   WHERE cpe.ic_epo = cpi.ic_epo  " +
									"     AND cpe.ic_pyme = ccb.ic_pyme  " +
									"     AND cpe.ic_pyme = pym.ic_pyme  " +
									"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria  " +
									"     AND cpi.cs_vobo_if = ?  " +
									"     AND ccb.cs_borrado = ? " +
									"     AND cpi.cs_borrado = ?  " +
									"     AND cpi.cs_opera_descuento = ?  " +
									"     AND cpe.cs_aceptacion = ?  " +
									"     AND cpe.ic_epo = ?  " +
									"     AND cpe.ic_pyme = ?     " +
									"     AND ccb.ic_moneda in (SELECT ic_moneda "+
									"					FROM comrel_cuenta_bancaria "+
									"					WHERE ic_cuenta_bancaria = ?)" +
									"     AND pym.ic_version_convenio in (?)  ";
									if(validaParamPymeDsctoAut(rs_ic_pyme))
										qrySentencia += "     AND pym.cs_dscto_automatico = ? ";
									//"     AND pym.cs_dscto_auto_proc = ? ";

					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,"S");
					ps.setString(2,"N");
					ps.setString(3,"N");
					ps.setString(4,"S");
					ps.setString(5,"H");
					ps.setLong(6,Long.parseLong(rs_ic_epo));
					ps.setLong(7,Long.parseLong(rs_ic_pyme));
					ps.setLong(8,Long.parseLong(rs_ic_cuenta_bancaria));
					ps.setInt(9,3);
					if(validaParamPymeDsctoAut(rs_ic_pyme))
						ps.setString(10,"S");


					System.out.println("\n qrySentencia: "+qrySentencia);

					ResultSet regOrdenIf = ps.executeQuery();
					if(regOrdenIf!=null && regOrdenIf.next())
						ordenIf = Integer.parseInt(regOrdenIf.getString("max_orden"));
					regOrdenIf.close();
					if(ps!=null)ps.close();

					System.out.println("cve_pyme ::::::::::::::: "+rs_ic_pyme);
					System.out.println("cve_epo ::::::::::::::: "+rs_ic_epo);
					System.out.println("cve_if ::::::::::::::: "+rs_ic_if);
					System.out.println("cve_cuenta ::::::::::::::: "+rs_ic_cuenta_bancaria);
					System.out.println("cve_orden ::::::::::::::: "+(ordenIf+1));

					if(!validaParamPymeDsctoAut(rs_ic_pyme)){
						qrySentencia =
							" UPDATE comcat_pyme " +
							"SET cs_dscto_automatico = ?, cs_dscto_auto_proc = ? " +
							"  WHERE ic_pyme = ? ";
						ps = con.queryPrecompilado(qrySentencia);
						ps.setString(1,"S");
						ps.setString(2,"N");
						ps.setLong(3,Long.parseLong(rs_ic_pyme));

						System.out.println("\n qrySentencia: "+qrySentencia);
						cont = ps.executeUpdate();
						if(ps!=null)ps.close();
					}
					qrySentencia =
						" UPDATE comrel_pyme_if " +
						" SET cs_dscto_automatico = ?, cs_dscto_automatico_dia = ?, " +
						" ig_orden = ?, cs_dscto_automatico_proc = ? " +
						"  WHERE ic_cuenta_bancaria = ? " +
						"    AND ic_if = ? " +
						"    AND ic_epo = ? ";

					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,"S");
					ps.setString(2,"P");
					ps.setInt(3,ordenIf+1);
					ps.setString(4,"S");
					ps.setLong(5,Long.parseLong(rs_ic_cuenta_bancaria));
					ps.setLong(6,Long.parseLong(rs_ic_if));
					ps.setLong(7,Long.parseLong(rs_ic_epo));
					System.out.println("\n qrySentencia: "+qrySentencia);
					cont = ps.executeUpdate();
					if(ps!=null)ps.close();

					qrySentencia =
						" INSERT  " +
						"   INTO bit_dscto_automatico  " +
						"    (  " +
						"        ic_usuario, ic_cuenta_bancaria, ic_if      ,  " +
						"        ic_epo    , df_registro       , cs_autorizo  " +
						"    )  " +
						"    VALUES  " +
						"    (  " +
						"        ?, ?, ?, " +
						"        ?, SYSDATE, ? " +
						"    ) " ;

					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,"proc_aut");
					ps.setLong(2,Long.parseLong(rs_ic_cuenta_bancaria));
					ps.setLong(3,Long.parseLong(rs_ic_if));
					ps.setLong(4,Long.parseLong(rs_ic_epo));
					ps.setString(5,"S");

					System.out.println("\n qrySentencia: "+qrySentencia);
					cont = ps.executeUpdate();
					if(ps!=null)ps.close();
					//System.out.println("\n lVarBind: "+lVarBind);
					//cont = con.ejecutaUpdateDB(qrySentencia, lVarBind);


				}//while(regEpoPymeMonedaIf.next())
				if(regEpoPymeMonedaIf!=null)regEpoPymeMonedaIf.close();
				if(psi!=null)psi.close();
			}
			//}//while(rs.next())
			//if(rs!=null)rs.close();
		} catch(Exception e) {
			con.terminaTransaccion(false);
			System.out.println("ParametrosDescuentoBean::setParamEpoPymeIf(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("ParametrosDescuentoBean::setParamEpoPymeIf(P,P,P,P)(S)");
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		return contenidoArchivo.toString();
	}//setParamEpoPymeIf


	private boolean validaParamPymeDsctoAut(String ic_pyme) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentencia = "";
		String existe_param = "0";
		boolean exist_param = false;
		try {
			con.conexionDB();
			qrySentencia =
				"select count(1) existe_param from comcat_pyme  " +
				"where  cs_dscto_automatico = ? " +
				"and cs_dscto_auto_proc = ?  " +
				"and ic_pyme = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,"S");
			ps.setString(2,"N");
			ps.setLong(3,Long.parseLong(ic_pyme));

			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				existe_param = rs.getString("existe_param");
				if(!"0".equals(existe_param))
					exist_param = true;
			}

			return exist_param;

		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	/**
	 *	Devuelve <tt>true</tt> o <tt>false</tt>, dependiendo si la pyme tiene expediente en el
	 * Sistema EFile.
	 * @param rfc Cadena de Texto con el RFC de la PYME
	 * @return <tt>boolean</tt>. <tt>true</tt> si tiene expediente. <tt>false</tt> en caso contrario.
	 */
	private boolean tieneExpedienteEnEfile(String rfc, AccesoDB con)
		throws AppException{

			log.info("tieneExpedienteEnEfile(E)");

			if(rfc == null || rfc.trim().equals("")) return false;

			//AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				//con.conexionDB();
/*				qrySentencia =
					"SELECT "  +
					"	DECODE(COUNT(1),0,'false','true') AS HAY_EXPEDIENTE "  +
					"FROM "  +
					"	WWW_DOCUMENT "  +
					"WHERE "  +
					"	TP_CATALOG 	= ? AND "  + // 2
					"	RFC_PYME 	= ? ";       // RFC PYME

				lVarBind.add(new Integer("2"));
				lVarBind.add(rfc);
*/

         qrySentencia =
					"SELECT "  +
					"	DECODE(COUNT(1),0,'false','true') AS HAY_EXPEDIENTE "  +
					"FROM "  +
					"	VM_PYMES_EFILE "  +
					"WHERE CG_RFC 	= ? ";       // RFC PYME

        log.debug("tieneExpedienteEnEfile(query): "+ qrySentencia);
				lVarBind.add(rfc);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("HAY_EXPEDIENTE").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("tieneExpedienteEnEfile(Exception)");
			log.debug("tieneExpedienteEnEfile.rfc = <"+rfc+">");
			e.printStackTrace();
			throw new AppException("Error al consultar si hay expediente para la pyme en el Sistema EFile");
		}finally{
			con.terminaTransaccion(true);
			//if(con.hayConexionAbierta())
			//	con.cierraConexionDB();
			log.info("tieneExpedienteEnEfile(S)");
		}

		return resultado;
	}

	/**
	 * Sirve para indicar si la pyme cuyo rfc se proporciona en el parametro <tt>rfc</tt>,
	 * tiene su expediente cargado en el Sistema Efile
	 * @param rfc Cadena de Texto con el RFC de la PYME
	 * @param estatus <tt>true</tt> para indicar que hay expediente. <tt>false</tt> en caso contrario.
	 */
	public void setEstatusDelExpedienteEfileEnTablaPyme(String rfc, boolean estatus)
			throws AppException {

		log.info("setEstatusDelExpedienteEfileEnTablaPyme(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;

		try {

			con.conexionDB();

			qrySentencia =
				" UPDATE "  +
				"	COMCAT_PYME "  +
				" SET "  +
				"	CS_EXPEDIENTE_EFILE = ? "  +
				" WHERE "  +
				"	CG_RFC = ? ";

			lVarBind = new ArrayList();
			lVarBind.add((estatus?"S":"N"));
			lVarBind.add(rfc);

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusDelExpedienteEfileEnTablaPyme(Exception)"+e);
			log.debug("setEstatusDelExpedienteEfileEnTablaPyme.rfc     = <"+rfc+">");
			log.debug("setEstatusDelExpedienteEfileEnTablaPyme.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al actualizar el estatus de existencia de expediente en el Sistema Efile");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusDelExpedienteEfileEnTablaPyme(S)");
		}
	}

	/**
	 *	Determina si se encuentra disponible el sistema Efile
	 *
	 * @return <tt>boolean</tt>. <tt>true</tt> si el sistema esta disponible.
	 * 		  <tt>false</tt> en caso contrario.
	 */
	public boolean estaDisponibleSistemaEfile(){

			log.info("estaDisponibleSistemaEfile(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= true;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT 				"  +
					"	sysdate  			"  +
					"FROM 				"  +
					"	dual@ORADOC 	";

				registros = con.consultarDB(qrySentencia, lVarBind, false);

		}catch(Exception e){
			log.debug("estaDisponibleSistemaEfile(Exception)");
			resultado = false;
		}finally{
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("estaDisponibleSistemaEfile(S)");
		}

		return resultado;
	}

	/**
	 *	 Cuando se realice una consulta mediante un paginador, este metodo verifica si una o m�s pymes
	 *  no tienen activada la bandera CS_EXPEDIENTE_EFILE, y en caso de que el expediente si exista
	 *  en el Sistema Efile activa la bandera CS_EXPEDIENTE_EFILE;
	 *
	 *  @param query Cadena de Texto con el query para traer las pymes que todavia no tienen expediente
	 *               en efile.
	 */
	public void actualizaPymesConExpedientesCargadosEnEfile(String query) throws AppException{ // String actualizaciones.getDocumentQuery(request);

		log.info("actualizaPymesConExpedientesCargadosEnEfile(E)");
		if(query == null || query.trim().equals("")) return;

		AccesoDB 		con 				= new AccesoDB();
		ResultSet		rs					= null;
		String 			rfc 				= null;

		try {

			con.conexionDB();
			rs 					= con.queryDB(query,1000);
      log.debug("ParametrosDescuentoBean.actualizaPymesConExpedientesCargadosEnEfile" + query);

			while(rs != null && rs.next()){
				rfc = rs.getString("CG_RFC");  //Llenamos el vector de llaves primarias
				if(rfc != null && !(rfc.trim().equals("")) ){
					if(tieneExpedienteEnEfile(rfc, con)){
						setEstatusDelExpedienteEfileEnTablaPyme(rfc,true);
					}
				}
			}
      con.cierraStatement();

		}catch(Exception e){
			log.error("actualizaPymesConExpedientesCargadosEnEfile(Exception)");
			log.error("actualizaPymesConExpedientesCargadosEnEfile.query = <"+query+">");
			log.error("actualizaPymesConExpedientesCargadosEnEfile.rfc   = <"+rfc  +">");
			e.printStackTrace();
			throw new AppException("Error al actualizar estatus de las Pymes con expedientes cargados en E-File");
		}finally{
			if(rs != null) try { rs.close(); }catch(Exception a){};
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("actualizaPymesConExpedientesCargadosEnEfile(S)");
		}

	}

  /**
	 * M�todo que envia un String con la consulta de las  PyMES con cs_expediente_efile = 'N'
   * para posteriormente ejecutar metodos y verificar existencia de expediente EFILE
	 * @author Ivan Almaguer
	 * @since  FODEA 057 - 2009 Mejoras 3
	 */
	 public void getPymesSinExpEfile(String ic_if) throws AppException {
    StringBuffer qrySQL = new StringBuffer();
    try{

        log.info("getPymesSinExpEfile (E)");
        qrySQL.append(	" SELECT /*+ use_nl(cif cctabanc pyme)*/"+
                        "   pyme.cg_rfc AS CG_RFC " +
                        "   FROM comrel_pyme_if cif  " +
                        "        , comrel_cuenta_bancaria cctabanc " +
                        "        , comcat_pyme pyme " +
                        "  WHERE pyme.ic_pyme = cctabanc.ic_pyme " +
                        "    AND cctabanc.ic_cuenta_bancaria = cif.ic_cuenta_bancaria " +
                        "    AND cctabanc.cs_borrado = 'N' " +
                        "    AND cif.cs_borrado = 'N' " +
                        "    AND pyme.cs_expediente_efile = 'N' " +
                        "    AND cif.cs_estatus is null " +
                        "    AND cif.df_vobo_if  >= trunc(sysdate-60) " + // SOLO ACTUALIZAR� LOS REGISTROS QUE SE PARAMETRIZARON LOS ULTIMOS 60 DIAS
                        "    AND cif.cs_vobo_if = 'N' " +
                        "    AND cif.ic_if = "+ic_if+" "+
                        "  GROUP BY pyme.cg_rfc, cif.DF_VOBO_IF" +
                        "  ORDER BY cif.DF_VOBO_IF DESC");
        actualizaPymesConExpedientesCargadosEnEfile(qrySQL.toString());

      }catch(Exception e){
        e.printStackTrace();
        throw new AppException("Unexpected Error");
      }
   }

	/**
	 * Devuelve el nombre de la pyme a partir de su numero nafin electronico
	 */
	public  String getNombrePyme(String noNafinElectronico,String claveEpo)
		throws AppException {

		log.info("getNombrePyme(E)");

		if( noNafinElectronico == null || noNafinElectronico.trim().equals("") ) return "";

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			nombrePyme				= "";
		boolean			hayEpoEspecificada	= false;

		if( claveEpo != null && !claveEpo.trim().equals("")) hayEpoEspecificada = true;

		try {
				con.conexionDB();
				qrySentencia =
					"  SELECT                                 "  +
					"    PYME.CG_RAZON_SOCIAL  AS NOMBRE_PYME "  +
					"  FROM                                   "  +
					"    COMREL_NAFIN    REL,                 "  +
					"    COMCAT_PYME     PYME                 "  +
					(hayEpoEspecificada?",    COMREL_PYME_EPO PYME_EPO ":"")  +
					"  WHERE  "  +
					"    REL.CG_TIPO                 = 'P'              AND "  +
					"    REL.IC_NAFIN_ELECTRONICO    = ?                AND "  + // NUMERO NAFIN ELECTRONICO DE LA PYME
					"    REL.IC_EPO_PYME_IF          = PYME.IC_PYME         "  +
					(hayEpoEspecificada?" AND  PYME.IC_PYME                = PYME_EPO.IC_PYME ":"")  +
					(hayEpoEspecificada?" AND  PYME_EPO.IC_EPO             = ?                ":""); // CLAVE DE LA EPO

				lVarBind.add(noNafinElectronico);
				if(hayEpoEspecificada) lVarBind.add(claveEpo);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					nombrePyme = registros.getString("NOMBRE_PYME");
				}

		}catch(Exception e){
			log.debug("getNombrePyme(Exception)");
			log.debug("getNombrePyme.noNafinElectronico = <"+noNafinElectronico+">");
			log.debug("getNombrePyme.claveEpo           = <"+claveEpo+">");
			e.printStackTrace();
			throw new AppException("Error al obtener el nombre de la pyme");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getNombrePyme(S)");
		}

		return nombrePyme;
	}


	/*********************************************************************************************/
	/*********************************************************************************************/
	public Hashtable getParametrosIF(String sNoIF, int iNoProducto) throws NafinException {
		 log.info("getParametrosIF(E)");
	    AccesoDB con = new AccesoDB();
	    Hashtable alParametros = new Hashtable();
		try {
			con.conexionDB();
			alParametros = getParametrosIF(sNoIF, iNoProducto, con);
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		log.info("getParametrosIF(S)");
		return alParametros;
	}	// Fin del metodo getParamEPO


	private Hashtable getParametrosIF(String sNoIF, int iNoProducto, AccesoDB con) throws NafinException {
	/* Regresa:
		0.- Un objeto Hashtable que contiene las llaves y los valores de los valores parametrizados por EPO
	*/
	Hashtable alParametros = new Hashtable();
	log.info("getParametrosIF(E)");
	try {
	String sQuery = null;
	PreparedStatement ps = null;
	ResultSet rs = null;


	if(!sNoIF.equals("")){
		sQuery =
		"SELECT pe.cc_parametro_if, NVL(pz.cg_valor, '') CG_VALOR " +
		"  FROM com_parametrizacion_if pz, comcat_parametro_if pe " +
		" WHERE pz.cc_parametro_if(+) = pe.cc_parametro_if " +
		" AND pz.ic_if(+) = ?";


		if(iNoProducto==0 ){
		 sQuery +=" and pe.cc_parametro_if = 'FISO_CORTES_CREDELEC' ";
		}

		sQuery +="ORDER BY 1 ";


		log.debug(":::Consulta Parametros:2::"+ sQuery);
		log.debug(":::Bind::"+ sNoIF);
		ps = con.queryPrecompilado(sQuery);
		ps.setInt(1, Integer.parseInt(sNoIF));

		rs = ps.executeQuery();
		while(rs.next()) {
			alParametros.put(rs.getString("cc_parametro_if").trim(), rs.getString("cg_valor")==null?"":rs.getString("cg_valor"));
		}
		rs.close();
		if(ps!=null) ps.close();
	}
		log.debug("::resultado antes de salir del bean::"+alParametros.toString());
	} catch(SQLException sqle) {
		sqle.printStackTrace();
		throw new NafinException("DSCT0095");
	} catch (Exception e) {
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		//if(con.hayConexionAbierta()) con.cierraConexionDB();
	}
	log.info("getParametrosIF(S)");
	return alParametros;
}	// Fin del metodo getParametrosIF

/*----------------------------------------------------------------------------*/
	/**
	* Este m�todo guarda las opciones seleccionadas de "Par�metros por EPO", esta basado en setParamEPO,
	* la diferencia es el manejo de Hashtable, eliminando asi el uso de posiciones para las sentencias sql.
	* @author Ivan Almaguer
	* @param sNoIF	Clave de la EPO
	* @param alParametros Parametros seleccionados de "Par�metros por EPO"
	* @param iNoProducto Numero de producto al que esta accesando
	* @param claveUsuario Clave del usuario
	* @since FODEA-023/2009 Factoraje con Mandato
	* @throws NafinException Cuando ocurre alg�n error en las sentencias sql.
	*/

	public void setParametrosIF(String sNoIF, Hashtable alParametros, int iNoProducto, String claveUsuario) throws NafinException {
		log.info("setParametrosIF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String sQuery = "";
		ResultSet rs = null;
		boolean bOk = true;
		int iNoRegistros = 0;

		try {
			con.conexionDB();
			String[] nombres = {"Opera Factoraje 24 hrs", "Tipo Moneda"};

			Hashtable datosAnteriores = new Hashtable();
			Hashtable datosActuales = new Hashtable();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer();

			datosAnteriores = getParametrosIF(sNoIF, iNoProducto, con);

			String iNoNafin = "";
			sQuery =
				" SELECT ic_nafin_electronico"   +
				"   FROM comrel_nafin"   +
				"  WHERE ic_epo_pyme_if = ?"   +
				"    AND cg_tipo = 'I'"  ;

			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(sNoIF));
			rs = ps.executeQuery();
			if(rs.next()) {
				iNoNafin = (rs.getString(1)==null)?"":rs.getString(1);
			}
			rs.close();ps.close();

			if(iNoProducto==0) { // Credito Electronico

				//Siempre borrar los checks (borrar lo anterior y solo grabar lo nuevo, si l flag es N se queda borrado)
					sQuery = " DELETE com_parametrizacion_if Where "+
					" CC_PARAMETRO_IF in( " +
					"'FISO_CORTES_CREDELEC' )"+//F017-2015
					" AND IC_IF = " + sNoIF;

			}else  {
			//Siempre borrar los checks (borrar lo anterior y solo grabar lo nuevo, si l flag es N se queda borrado)
				sQuery = " DELETE com_parametrizacion_if Where "+
				" CC_PARAMETRO_IF in( " +
				"'PUB_IF_FACT_24HRS',"+//MODIFICACION F049-2008
				"'TIPO_CONSULTA',"+// F004-2011
				"'TIPO_MONEDA', "+//MODIFICACION F049-2008
				"'CORREO_NOTIFICACION', "+//2012
				"'TASA_FONDEO', "+//F017-2013
					"'NOTIFICACION_DOCTOS', "+
					"'MAIL_NOTIFICACION_DOCTOS', "+
				"'FISO_CORTES')"+//F00-2015
				" AND IC_IF = " + sNoIF;

			}

				ps = con.queryPrecompilado(sQuery);
				iNoRegistros = ps.executeUpdate();
				ps.close();

			log.debug(" borrar Parametros  "+sQuery );


			try {

				if(iNoProducto !=0) {

				log.debug("PUB_IF_FACT_24HRS: "+alParametros.get("PUB_IF_FACT_24HRS")+"\n");
				log.debug("TIPO_MONEDA: "+alParametros.get("TIPO_MONEDA")+"\n");
				log.debug("Insertar los checks si la flag es S");

				if(alParametros.get("PUB_IF_FACT_24HRS")!=null &&
					(((String)alParametros.get("PUB_IF_FACT_24HRS")).equals("S") || ((String)alParametros.get("PUB_IF_FACT_24HRS")).equals("N"))){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "PUB_IF_FACT_24HRS");
					ps.setString(3, (String)alParametros.get("PUB_IF_FACT_24HRS") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 1");
				}

				if(alParametros.get("TIPO_MONEDA") != null &&
				((String)alParametros.get("TIPO_MONEDA")).equals("MN") ||
				((String)alParametros.get("TIPO_MONEDA")).equals("DA") ||
				((String)alParametros.get("TIPO_MONEDA")).equals("A")){
					//guardando flag
					sQuery = " INSERT INTO com_parametrizacion_if (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "TIPO_MONEDA");
					ps.setString(3, (String)alParametros.get("TIPO_MONEDA") );
					iNoRegistros = ps.executeUpdate();
					log.debug("insert 2");
				}

				//FODEA 014-2011 (E)
				if(alParametros.get("TIPO_CONSULTA")!=null &&
					(((String)alParametros.get("TIPO_CONSULTA")).equals("G") || ((String)alParametros.get("TIPO_CONSULTA")).equals("D"))){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "TIPO_CONSULTA");
					ps.setString(3, (String)alParametros.get("TIPO_CONSULTA") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 3");
				}

				if(alParametros.get("CORREO_NOTIFICACION")!=null ){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "CORREO_NOTIFICACION");
					ps.setString(3, (String)alParametros.get("CORREO_NOTIFICACION") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 4  Correos de Notificaci�n");
				}
				//FODEA 014-2011 (S)

				//Fodea 017-2013
				if(alParametros.get("TASA_FONDEO")!=null ){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "TASA_FONDEO");
					ps.setString(3, (String)alParametros.get("TASA_FONDEO") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug(" Insert Tasa por FONDEO de Fideicomiso para Desarrollo de Proveedores");
				}

				if(alParametros.get("FISO_CORTES")!=null &&
					(((String)alParametros.get("FISO_CORTES")).equals("S") || ((String)alParametros.get("FISO_CORTES")).equals("N"))){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "FISO_CORTES");
					ps.setString(3, (String)alParametros.get("FISO_CORTES") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 5");
				}

				if(alParametros.get("NOTIFICACION_DOCTOS") != null){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "NOTIFICACION_DOCTOS");
					ps.setString(3, (String)alParametros.get("NOTIFICACION_DOCTOS"));
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 6");
				}

				if(alParametros.get("NOTIFICACION_DOCTOS") != null && ((String)alParametros.get("NOTIFICACION_DOCTOS")).equals("S")){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "MAIL_NOTIFICACION_DOCTOS");
					ps.setString(3, (String)alParametros.get("MAIL_NOTIFICACION_DOCTOS"));
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 6.1");
				}

			}else  {

				log.debug("Credito Electronico ");  //
				log.debug("insert inicia 6 ===   " +(String)alParametros.get("FISO_CORTES_CREDELEC"));

				if(alParametros.get("FISO_CORTES_CREDELEC")!=null &&
					(((String)alParametros.get("FISO_CORTES_CREDELEC")).equals("S") || ((String)alParametros.get("FISO_CORTES_CREDELEC")).equals("N"))){
					sQuery = " INSERT INTO com_parametrizacion_if  (IC_IF, CC_PARAMETRO_IF, CG_VALOR) values (?,?,?) ";
					ps = con.queryPrecompilado(sQuery);
					ps.setString(1, sNoIF);
					ps.setString(2, "FISO_CORTES_CREDELEC");
					ps.setString(3, (String)alParametros.get("FISO_CORTES_CREDELEC") );
					iNoRegistros = ps.executeUpdate();
					ps.close();
					log.debug("insert 6");
				}
			}



			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DSCT0096");
			}

			datosActuales = getParametrosIF(sNoIF, iNoProducto, con);
			log.debug("\n\ndatosAnteriores : "+datosAnteriores.toString());
			log.debug("\n\ndatosActuales : "+datosActuales);
			log.debug("\n\n");


			List datosAnterioresList = new ArrayList();
			List datosActualesList = new ArrayList();


			/*
			* Se obtienen los registros de las Hashtable datosAnteriores y datosActuales se van guardando los
			* valores en las listas datosAnterioresList y datosActualesList para enviarlas al
			* metodo getCambiosBitacora y se hagan las comparaciones de que parametro fue modificado.
			*/

			datosAnterioresList.add((String)datosAnteriores.get("PUB_IF_FACT_24HRS"));
			datosActualesList.add((String)datosActuales.get("PUB_IF_FACT_24HRS"));

			datosAnterioresList.add((String)datosAnteriores.get("TIPO_MONEDA"));
			datosActualesList.add((String)datosActuales.get("TIPO_MONEDA"));

			log.debug("\n");
			log.debug("Numero de nombres :::: "+nombres.length);//FODEA 016-2010 PENDIENTE REGISTRO EN BITACORA
			log.debug("Numero datosAnterioresList :::: "+datosAnterioresList.size());
			log.debug("Numero datosActualesList   :::: "+datosActualesList.size());
			log.debug("datosAnterioresList :::: "+datosAnterioresList);
			log.debug("datosActualesList  :::: "+datosActualesList);
			log.debug("\n");

			List cambios =	Bitacora.getCambiosBitacora(datosAnterioresList, datosActualesList, nombres);

			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));

			Bitacora.grabarEnBitacora(con, "PARAMDSCTO", "E",
			String.valueOf(iNoNafin), claveUsuario,
			valoresAnteriores.toString(), valoresActuales.toString());

		} catch (NafinException ne) {
			ne.printStackTrace();
			bOk = false;
			throw ne;
		} catch (Exception e) {
			e.printStackTrace();
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
			log.info("setParametrosIF(S)");
	} // Fin del metodo setParametrosIF


	/**
	 *	Este metodo sirve para consultar si la EPO, cuyo ID se proporciona en el parametro <tt>ic_epo</tt>,
	 * Opera Notas de Credito para el Producto: Descuento Electronico.
	 * @Fodea 002 - 2010
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si opera notas de credito. <tt>false</tt> en caso contrario.
	 */
	public boolean operaNotasDeCreditoParaDescuentoElectronico(String ic_epo)
		throws AppException{

		log.info("operaNotasDeCreditoParaDescuentoElectronico(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"	 NVL(CS_OPERA_NOTAS_CRED,'N') AS ESTATUS_PARAMETRO "  +
					"FROM "  +
					"	 COMREL_PRODUCTO_EPO "  +
					"WHERE "  +
					"	 IC_EPO            = ? AND "  +
					"	 IC_PRODUCTO_NAFIN = ? ";

				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer(1)); // Producto: Descuento Electronico

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("S")?true:false;
				}

		}catch(Exception e){
			log.debug("operaNotasDeCreditoParaDescuentoElectronico(Exception)");
			log.debug("operaNotasDeCreditoParaDescuentoElectronico.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar el estatus.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("operaNotasDeCreditoParaDescuentoElectronico(S)");
		}

		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si la EPO, cuyo ID se proporciona en el parametro <tt>ic_epo</tt>,
	 * tiene habilitada la Opcion de Aplicar Notas de Credito a Varios Documentos.
	 * @Fodea 002 - 2010
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitada. <tt>false</tt> en caso contrario.
	 */
	public boolean hasAplicarNotaDeCreditoAVariosDoctosEnabled(String ic_epo)
		throws AppException{

		log.info("hasAplicarNotaDeCreditoAVariosDoctosEnabled(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"  NVL(pz.cg_valor, 'N') AS ESTATUS_PARAMETRO "  +
					"FROM "  +
					"  com_parametrizacion_epo pz, "  +
					"  comcat_parametro_epo    pe "  +
					"WHERE "  +
					"  PE.CC_PARAMETRO_EPO    = ? AND "  + // 'CS_APLICAR_NOTAS_CRED'
					"  pz.cc_parametro_epo(+) = pe.cc_parametro_epo AND "  +
					"  pz.ic_epo(+)           = ? ";

				lVarBind.add("CS_APLICAR_NOTAS_CRED");
				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("S")?true:false;
				}

		}catch(Exception e){
			log.debug("hasAplicarNotaDeCreditoAVariosDoctosEnabled(Exception)");
			log.debug("hasAplicarNotaDeCreditoAVariosDoctosEnabled.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar el estatus.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasAplicarNotaDeCreditoAVariosDoctosEnabled(S)");
		}

		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si la Nota de Credito Aplicada es una Nota de Credito Multiple
	 * @Fodea 002 - 2010
	 * @param ic_nota_credito Clave de la Nota de Credito.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitada. <tt>false</tt> en caso contrario.
	 */
	public boolean existeReferenciaEnComrelNotaDocto(String ic_nota_credito)
		throws AppException{

		log.info("existeReferenciaEnComrelNotaDocto(E)");

			if(ic_nota_credito == null || ic_nota_credito.equals("")){
				log.info("existeReferenciaEnComrelNotaDocto(S)");
				return false;
			}

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {

				con.conexionDB();
				qrySentencia =
					"SELECT 																	"  +
					"	DECODE(COUNT(1),0,'false','true') as HAY_REFERENCIA 	"  +
					"FROM 																	"  +
					"	COMREL_NOTA_DOCTO 												"  +
					"WHERE 																	"  +
					"	IC_NOTA_CREDITO = ?												";

				lVarBind.add(new Integer(ic_nota_credito));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("HAY_REFERENCIA").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("existeReferenciaEnComrelNotaDocto(Exception)");
			log.debug("existeReferenciaEnComrelNotaDocto.ic_nota_credito = <"+ic_nota_credito+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar la Nota de Credito Multiple.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("existeReferenciaEnComrelNotaDocto(S)");
		}

		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si a un documento en especifico se le ha aplicado
	 * una nota de credito multiple.
	 * @Fodea 002 - 2010
	 * @param ic_documento Clave del Decumento.
	 * @return <tt>boolean</tt>. <tt>true</tt> si tiene notas de credito multiples aplicadas.
	 *         <tt>false</tt> en caso contrario.
	 */
	public boolean esDocumentoConNotaDeCreditoMultipleAplicada(String ic_documento)
		throws AppException{

		log.info("esDocumentoConNotaDeCreditoMultipleAplicada(E)");

			if(ic_documento == null || ic_documento.equals("")){
				log.info("esDocumentoConNotaDeCreditoMultipleAplicada(S)");
				return false;
			}

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {

				con.conexionDB();
				qrySentencia =
					"SELECT 																	"  +
					"	DECODE(COUNT(1),0,'false','true') as HAY_REFERENCIA 	"  +
					"FROM 																	"  +
					"	COMREL_NOTA_DOCTO 												"  +
					"WHERE 																	"  +
					"	IC_DOCUMENTO = ?												";

				lVarBind.add(new Integer(ic_documento));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("HAY_REFERENCIA").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("esDocumentoConNotaDeCreditoMultipleAplicada(Exception)");
			log.debug("esDocumentoConNotaDeCreditoMultipleAplicada.ic_documento = <"+ic_documento+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("esDocumentoConNotaDeCreditoMultipleAplicada(S)");
		}

		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si la Nota de Credito Simple fue aplicada a un Documento
	 * @Fodea 002 - 2010
	 * @param ic_nota_credito Clave de la Nota de Credito.
	 * @return <tt>boolean</tt>. <tt>true</tt> si fue aplicada. <tt>false</tt> en caso contrario.
	 */
	public boolean existeDocumentoAsociado(String ic_nota_credito)
		throws AppException{

		log.info("existeDocumentoAsociado(E)");

			if(ic_nota_credito == null || ic_nota_credito.equals("")){
				log.info("existeDocumentoAsociado(S)");
				return false;
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 			qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT                                                          "  +
					"	decode(IC_DOCTO_ASOCIADO,null,'false','true') AS EXISTE_DOCTO "  +
					"FROM                                                            "  +
					"	COM_DOCUMENTO                                                 "  +
					"WHERE                                                           "  +
					"	IC_DOCUMENTO = ?                                              ");

				lVarBind.add(new Integer(ic_nota_credito));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("EXISTE_DOCTO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("existeDocumentoAsociado(Exception)");
			log.debug("existeDocumentoAsociado.ic_nota_credito = <"+ic_nota_credito+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar la Nota de Credito Simple.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("existeDocumentoAsociado(S)");
		}

		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si a un documento en especifico se le ha aplicado
	 * una nota de credito simple.
	 * @Fodea 002 - 2010
	 * @param ic_documento Clave del Decumento.
	 * @return <tt>boolean</tt>. <tt>true</tt> si tiene una nota de credito simple aplicada.
	 *         <tt>false</tt> en caso contrario.
	 */
	public boolean esDocumentoConNotaDeCreditoSimpleAplicada(String ic_documento)
		throws AppException{

		log.info("esDocumentoConNotaDeCreditoSimpleAplicada(E)");

			if(ic_documento == null || ic_documento.equals("")){
				log.info("esDocumentoConNotaDeCreditoSimpleAplicada(S)");
				return false;
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT                                              "  +
					"	DECODE(COUNT(1),0,'false','true') AS EXISTE_DOCTO "  +
					"FROM                                                "  +
					"	COM_DOCUMENTO                                     "  +
					"WHERE                                               "  +
					"	IC_DOCTO_ASOCIADO = ?                             ");

				lVarBind.add(new Integer(ic_documento));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("EXISTE_DOCTO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("esDocumentoConNotaDeCreditoSimpleAplicada(Exception)");
			log.debug("esDocumentoConNotaDeCreditoSimpleAplicada.ic_documento = <"+ic_documento+">");
			e.printStackTrace();
			throw new AppException("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("esDocumentoConNotaDeCreditoSimpleAplicada(S)");
		}

		return resultado;
	}
  //FODEA 028 - 2010 ACF (I)
  /**
   * Este m�todo obtiene el ic_pyme a partir de dos parametros, el N�mero de proveedor asignado por la EPO
   * y la clave de esta �ltima.
   * @param numeroProveedor es el n�mero de proveedor asignado por la EPO a la Pyme.
   * @param claveEpo es la clave interna de la epo.
   * @return clavePyme es la clave interna de la pyme.
   * @throws AppException Cuando ocurre un error en la consulta.
   */
  public String obtenerPymeConNumeroDeProveedor(String numeroProveedor, String claveEpo) throws AppException {
    log.info("obtenerPymeConNumeroDeProveedor(E) ::..");
    AccesoDB con = new AccesoDB();
    ResultSet rst = null;
    PreparedStatement pst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    String clavePyme = "";
    try {
      con.conexionDB();

      strSQL.append("SELECT ic_pyme FROM comrel_pyme_epo WHERE ic_epo = ? AND cg_pyme_epo_interno = ?");
      varBind.add(new Long(claveEpo));
      varBind.add(numeroProveedor);

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
        clavePyme = rst.getString("ic_pyme")==null?"":rst.getString("ic_pyme");
      }

      rst.close();
      pst.close();
    } catch(Exception e) {
      log.info("obtenerPymeConNumeroDeProveedor(ERROR)");
      e.printStackTrace();
    } finally{
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
    }
    log.info("obtenerPymeConNumeroDeProveedor(S) ::..");
    return clavePyme;
  }
  //FODEA 028 - 2010 ACF (F)
  //FODEA 032 - 2010 ACF (I)
  /**
   * Este m�todo obtiene el correo electr�nico del representante legal del IF Benficiario.
   * @param claveBeneficiario es la clave interna del IF Beneficiario.
   * @return emailRepresentante correo electr�nico del representante legal.
   * @throws AppException Cuando ocurre un error en la consulta.
   */
  public String obtenerCorreoRepresentanteLegalBenef(String claveBeneficiario) throws AppException {
    log.info("obtenerCorreoRepresentanteLegalBenef(E) ::..");
    AccesoDB con = new AccesoDB();
    ResultSet rst = null;
    PreparedStatement pst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    String correoRepresentanteLegalBenef = "";
    try {
      con.conexionDB();

      strSQL.append(" SELECT cg_email AS email_representante FROM com_domicilio WHERE ic_if = ? AND cs_fiscal = ?");
      varBind.add(new Long(claveBeneficiario));
      varBind.add("S");

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
        correoRepresentanteLegalBenef = rst.getString("email_representante")==null?"":rst.getString("email_representante");
      }

      rst.close();
      pst.close();
    } catch(Exception e) {
      log.info("obtenerCorreoRepresentanteLegalBenef(ERROR)");
      e.printStackTrace();
    } finally{
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
    }
    log.info("obtenerCorreoRepresentanteLegalBenef(S) ::..");
    return correoRepresentanteLegalBenef;
  }

  /**
   * Este m�todo obtiene el correo electr�nico del representante legal del IF Benficiario.
   * @param claveBeneficiario es la clave interna del IF Beneficiario.
   * @return emailRepresentante correo electr�nico del representante legal.
   * @throws AppException Cuando ocurre un error en la consulta.
   */
  public String obtenerEstatusAvisoPublicacionBenef(String claveBeneficiario) throws AppException {
    log.info("obtenerEstatusAvisoPublicacionBenef(E) ::..");
    AccesoDB con = new AccesoDB();
    ResultSet rst = null;
    PreparedStatement pst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    String anteriorParametrizacion = "";
		String icAvisoPubBenef = "";
    try {
      con.conexionDB();

      strSQL.append(" SELECT MAX(ic_aviso_pub_benef) AS ic_aviso_pub_benef FROM com_aviso_pub_benef WHERE ic_beneficiario = ?");
      varBind.add(new Long(claveBeneficiario));

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
        icAvisoPubBenef = rst.getString("ic_aviso_pub_benef")==null?"":rst.getString("ic_aviso_pub_benef");
      }

      rst.close();
      pst.close();

			if (!icAvisoPubBenef.equals("")) {
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_activo");
				strSQL.append(" FROM com_aviso_pub_benef");
				strSQL.append(" WHERE ic_aviso_pub_benef = ?");
				varBind.add(new Long(icAvisoPubBenef));

				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);

				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();

				while (rst.next()) {
					anteriorParametrizacion = rst.getString("cs_activo")==null?"":rst.getString("cs_activo");
				}

				rst.close();
				pst.close();
			}
    } catch(Exception e) {
      log.info("obtenerEstatusAvisoPublicacionBenef(ERROR)");
      e.printStackTrace();
    } finally{
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
    }
    log.info("obtenerEstatusAvisoPublicacionBenef(S) ::..");
    return anteriorParametrizacion;
  }

  /**
   * Este m�todo guarda la parametrizaci�n del servicio de aviso de publicaci�n
	 * por parte de la EPO al IF Beneficiario.
   * @param claveBeneficiario es la clave interna del IF Beneficiario.
	 * @param loginUsuario es la clave de acceso al sistema del usuario que realiza la parametrizaci�n.
	 * @param csAvisoPublicacion es el valor de la parametrizaci�n del servicio, S Activo, N Inactivo.
   * @throws AppException Cuando ocurre un error en la consulta.
   */
  public void guardarEstatusAvisoPublicacionBenef(String claveBeneficiario, String loginUsuario, String csAvisoPublicacion) throws AppException {
    log.info("guardarEstatusAvisoPublicacionBenef(E) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
		boolean transOk = true;
    try {
      con.conexionDB();

      strSQL.append(" INSERT INTO com_aviso_pub_benef (ic_aviso_pub_benef, ic_beneficiario, cs_activo, cg_usuario)");
      strSQL.append(" VALUES ((SELECT NVL(MAX(ic_aviso_pub_benef), 0) + 1 AS ic_aviso_pub_benef FROM com_aviso_pub_benef), ?, ?, ?)");

      varBind.add(new Long(claveBeneficiario));
			varBind.add(csAvisoPublicacion);
			varBind.add(loginUsuario);

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      pst.executeUpdate();
      pst.close();
    } catch(Exception e) {
			transOk = false;
      log.info("guardarEstatusAvisoPublicacionBenef(ERROR)");
      e.printStackTrace();
    } finally{
      if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transOk);
        con.cierraConexionDB();
      }
    }
    log.info("guardarEstatusAvisoPublicacionBenef(S) ::..");
  }

  /**
   * Este m�todo obtiene el correo electr�nico del representante legal del IF Benficiario.
   * @param claveBeneficiario es la clave interna del IF Beneficiario.
   * @return emailRepresentante correo electr�nico del representante legal.
   * @throws AppException Cuando ocurre un error en la consulta.
   */
  public HashMap obtenerBitacoraAvisoPublicacionBenef(String claveBeneficiario) throws AppException {
    log.info("obtenerBitacoraAvisoPublicacionBenef(E) ::..");
    AccesoDB con = new AccesoDB();
    ResultSet rst = null;
    PreparedStatement pst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    HashMap bitacoraAvisoPubBenef = new HashMap();
		int countReg = 0;
    try {
      con.conexionDB();

			strSQL.append(" SELECT DECODE(cs_activo, 'S', 'Activo', 'N', 'Inactivo') AS accion,");
			strSQL.append(" cg_usuario AS cg_usuario,");
			strSQL.append(" TO_CHAR(df_activacion, 'dd/mm/yyyy') AS fecha_activacion");
			strSQL.append(" FROM com_aviso_pub_benef");
			strSQL.append(" WHERE ic_beneficiario = ?");
			strSQL.append(" ORDER BY df_activacion DESC");

      varBind.add(new Long(claveBeneficiario));

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
				HashMap registroBit = new HashMap();
				registroBit.put("accion", rst.getString("accion")==null?"":rst.getString("accion"));
				registroBit.put("usuario", rst.getString("cg_usuario")==null?"":rst.getString("cg_usuario"));
				registroBit.put("fecha_activacion", rst.getString("fecha_activacion")==null?"":rst.getString("fecha_activacion"));
				bitacoraAvisoPubBenef.put("registroBit" + countReg, registroBit);
				countReg++;
      }

			bitacoraAvisoPubBenef.put("numeroRegistros", Integer.toString(countReg));

      rst.close();
      pst.close();
    } catch(Exception e) {
      log.info("obtenerBitacoraAvisoPublicacionBenef(ERROR)");
      e.printStackTrace();
    } finally{
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
    }
    log.info("obtenerBitacoraAvisoPublicacionBenef(S) ::..");
    return bitacoraAvisoPubBenef;
  }
  //FODEA 32 - 2010 ACF (F)

	/**
	 * Fodea 060-2010
	 * @throws netropology.utilerias.AppException
	 * @return regresa   'S' si la epo esta parametrizada com
	 * Publicaci�n con Firma Mancomunada
	 * @param iNoProducto
	 * @param sNoEPO
	 */
	public String getOperaFirmaMancomunada(String sNoEPO, int iNoProducto) 	throws AppException {
	String firmaMancomunada = "";
	AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			String sQuery = " select NVL (cs_pub_firma_manc, 'N') AS pub_firma_manc "+
							" from comrel_producto_epo "+
							" where ic_epo = ? "+
							" and ic_producto_nafin = ? ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(sNoEPO));
			ps.setInt(2, iNoProducto);
			ResultSet rs = ps.executeQuery();
  		if(rs.next()) {
				firmaMancomunada = (rs.getString("pub_firma_manc")==null)?"":rs.getString("pub_firma_manc").trim();
			}
			rs.close();
		  ps.close();


		} catch (Exception e) {
			throw new AppException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
			con.cierraConexionDB();
			}
		}
	return firmaMancomunada;
	}

/**
	 * Fodea 022-2011
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param sNoIF
	 * @param sNoEPO
	 */
public String getValidarFechaLimite(String sNoEPO, String sNoIF) 	throws AppException {
	String ic_epo = "";
	AccesoDB con = new AccesoDB();
	String CS_FECHA_LIMITE ="";
	StringBuffer sQuery	= new StringBuffer();
	try {
		con.conexionDB();

			sQuery.append( " select distinct  CS_FECHA_LIMITE " );
			sQuery.append( " from comrel_if_epo  ");
			sQuery.append( " where ic_epo = ? ");
			sQuery.append( " and ic_if =  ? ");

			PreparedStatement ps1 = con.queryPrecompilado(sQuery.toString());
			ps1.setInt(1, Integer.parseInt(sNoEPO));
			ps1.setInt(2, Integer.parseInt(sNoIF));
			ResultSet rs1 = ps1.executeQuery();
			if(rs1.next()) {
				CS_FECHA_LIMITE = (rs1.getString("CS_FECHA_LIMITE")==null)?"":rs1.getString("CS_FECHA_LIMITE").trim();
			}
			rs1.close();
			ps1.close();

			if(CS_FECHA_LIMITE.equals("S")	){
				sQuery	= new StringBuffer();

				sQuery.append( " select distinct ic_epo  ");
				sQuery.append( " from comrel_if_epo   ");
				sQuery.append( " where CS_FECHA_LIMITE = 'S'  ");
				sQuery.append( " AND df_venc_linea <= TRUNC (SYSDATE)  ");
				sQuery.append( " and ic_epo = ? ");
        sQuery.append( " and ic_if = ? ");
				sQuery.append( " UNION ");
				sQuery.append( "  select distinct ic_epo    ");
				sQuery.append( "  from comrel_if_epo   ");
				sQuery.append( "  where CS_FECHA_LIMITE = 'S'  ");
				sQuery.append( "  and  df_cambio_admon <= TRUNC (SYSDATE)  ");
				sQuery.append( "  and ic_epo = ? ");
				sQuery.append( "  and ic_if = ? ");

				PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
				ps.setInt(1, Integer.parseInt(sNoEPO));
				ps.setInt(2, Integer.parseInt(sNoIF));
				ps.setInt(3, Integer.parseInt(sNoEPO));
				ps.setInt(4, Integer.parseInt(sNoIF));

				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					ic_epo = "S";   //cuando  las fechas estan Vencidas
				}else{
					ic_epo ="N"; //cuando  las fechas no  estan Vencidas
				}
				rs.close();
				ps.close();
			}else{
				ic_epo = "A"; //cuando no esta activado el Check de validar Fecha Limite
			}

		//log.debug("ic_epo "+ic_epo);



	} catch (Exception e) {
		throw new AppException("SIST0001");
	} finally {
		if(con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return ic_epo;
}

	/**
	 * lista de Epos  con l�nea de Cr�dito Vencida
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param sNoIF
	 */
	public List avisoLimiteLineaC(String sNoIF) 	throws AppException {
	AccesoDB con = new AccesoDB();
	StringBuffer sQuery	= new StringBuffer();
	List datos = new ArrayList();
	List listaEpos = new ArrayList();

		try {
			con.conexionDB();

			 sQuery.append("  SELECT   /*+index(cpe) index(ie)*/ ");
			 sQuery.append("  e.cg_razon_social AS nombre_epo ");
			 sQuery.append("  ,i.cg_razon_social AS nombre_if ");
			 sQuery.append("   ,TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea  ");
			 sQuery.append("   , TO_char(ie.df_cambio_admon, 'dd/mm/yyyy')	AS fechacambio   ");
			 sQuery.append("   FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe , comcat_if i ");
			 sQuery.append("   WHERE ie.ic_epo = e.ic_epo ");
			 sQuery.append("   AND UPPER (ie.cs_vobo_nafin) = 'S' " );
			 sQuery.append("   AND ie.in_limite_epo <> 0 ");
			 sQuery.append("   AND e.cs_habilitado = 'S' ");
			 sQuery.append("   AND cpe.ic_epo = e.ic_epo ");
			 sQuery.append("   AND cpe.ic_epo = ie.ic_epo ");
			 sQuery.append("   AND cpe.ic_producto_nafin = 1 ");
			 sQuery.append("    AND cpe.cs_limite_pyme != 'S' ");
			 sQuery.append("   and ie.CS_FECHA_LIMITE = 'S'  ");
			 sQuery.append("   and i.ic_if = ie.ic_if  ");
			 sQuery.append("   AND ie.df_venc_linea <= TRUNC (SYSDATE)   ");
			 if(!sNoIF.equals("0")){
				 sQuery.append("   AND ie.ic_if = ? ");
				}
			 sQuery.append("  UNION ");
			 sQuery.append("  SELECT   /*+index(cpe) index(ie)*/ ");
			 sQuery.append("  e.cg_razon_social AS nombre_epo ");
			 sQuery.append("  ,i.cg_razon_social AS nombre_if ");
			 sQuery.append("   ,TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea  ");
			 sQuery.append("   , TO_char(ie.df_cambio_admon, 'dd/mm/yyyy')	AS fechacambio   ");
			 sQuery.append("   FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe, comcat_if i  ");
			 sQuery.append("   WHERE ie.ic_epo = e.ic_epo ");
			 sQuery.append("   AND UPPER (ie.cs_vobo_nafin) = 'S' " );
			 sQuery.append("   AND ie.in_limite_epo <> 0 ");
			 sQuery.append("   AND e.cs_habilitado = 'S' ");
			 sQuery.append("   AND cpe.ic_epo = e.ic_epo ");
			 sQuery.append("   AND cpe.ic_epo = ie.ic_epo ");
			 sQuery.append("   AND cpe.ic_producto_nafin = 1 ");
			 sQuery.append("    AND cpe.cs_limite_pyme != 'S' ");
			 sQuery.append("   and ie.CS_FECHA_LIMITE = 'S'  ");
			 sQuery.append("   and i.ic_if = ie.ic_if  ");
			 sQuery.append("	 and ie.df_cambio_admon <= trunc(Sysdate)   ");

			 if(!sNoIF.equals("0")){
				 sQuery.append("   AND ie.ic_if = ? ");
			 }

			log.debug("sNoIF--->"+sNoIF);
			 log.debug(sQuery.toString());

			 PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			 if(!sNoIF.equals("0")){
				ps.setInt(1, Integer.parseInt(sNoIF));
				ps.setInt(2, Integer.parseInt(sNoIF));
			 }
			 ResultSet rs = ps.executeQuery();

			 while (rs.next()) {
				datos = new ArrayList();
				datos.add( (rs.getString("nombre_epo")==null)?"":rs.getString("nombre_epo").trim());
				datos.add( (rs.getString("nombre_if")==null)?"":rs.getString("nombre_if").trim());
				datos.add( (rs.getString("fechalinea")==null)?"":rs.getString("fechalinea").trim());
				datos.add( (rs.getString("fechacambio")==null)?"":rs.getString("fechacambio").trim());
				listaEpos.add(datos);
			}
			rs.close();
		  ps.close();

			//log.debug("listaEpos---->"+listaEpos);

		} catch (Exception e) {
			throw new AppException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
			con.cierraConexionDB();
			}
		}
		return listaEpos;
	}


	/**
	 * Se inserta registros de cuentas bancarias a procesar
	 * en tabla temporal para posteriormenete pasar a ser validados
	 * @param cveif
	 * @param procesoId
	 * @param ruta
	 */
	public void setDatosLiberMasivaPyme(String Ruta, String procesoId, String cveif)throws AppException{
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		String 				qry 				= "";
		String 				lineaf 			= "";
		int 					contregistros 	= 0;
		int 					numeroLinea 	= 1;
		int 					params 			= 0;
		boolean 				lbOK 				= true;
		VectorTokenizer 	vtd				= null;
		Vector 				vecdet			= null;

		String strCveEpo				="";
		String strRFCpyme				="";
		String strCveMoneda			="";
		String strTipoMov				="";
		String strCompDomFiscal		="";
		String strCompAltaHac		="";
		String strCedulaRFC			="";
		String strIdeOfiVig			="";
		String strEdoCtaCheuqes		="";
		String strConvPymeNafin		="";
		String strOtrDictisFact		="";
		String strContratoIf			="";
		String strNotas				="";

		try{
			con.conexionDB();

			// Insertar registros en COMTMP_AFILIA_PYME_MASIVA
			java.io.File   f	= new java.io.File(Ruta);
			BufferedReader br	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));
			while ((lineaf=br.readLine())!=null){
				//contregistros++;
				if(contregistros>0){
					vtd		= new VectorTokenizer(lineaf,"|");
					vecdet	= vtd.getValuesVector();
					System.out.println("vecdet.size()==="+vecdet.size());
					params 			= 0;

					// Obtener los campos individuales de cada uno de los registros del archivo
					//strCveIf				=(vecdet.size()>=1)?vecdet.get(0).toString().trim():"";
					strCveEpo			=(vecdet.size()>=1)?vecdet.get(0).toString().trim().toUpperCase():"";
					strRFCpyme			=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
					strCveMoneda		=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
					strTipoMov			=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"";
					strCompDomFiscal	=(vecdet.size()>=5)?vecdet.get(4).toString().trim().toUpperCase():"N";
					strCompAltaHac		=(vecdet.size()>=6)?vecdet.get(5).toString().trim().toUpperCase():"N";
					strCedulaRFC		=(vecdet.size()>=7)?vecdet.get(6).toString().trim().toUpperCase():"N";
					strIdeOfiVig		=(vecdet.size()>=8)?vecdet.get(7).toString().trim().toUpperCase():"N";
					strEdoCtaCheuqes	=(vecdet.size()>=9)?vecdet.get(8).toString().trim().toUpperCase():"N";
					strConvPymeNafin	=(vecdet.size()>=10)?vecdet.get(9).toString().trim().toUpperCase():"N";
					strOtrDictisFact	=(vecdet.size()>=11)?vecdet.get(10).toString().trim().toUpperCase():"N";
					strContratoIf		=(vecdet.size()>=12)?vecdet.get(11).toString().trim():"N";
					strNotas				=(vecdet.size()>=13)?vecdet.get(12).toString().trim():"";

					// Quitar comillas simples y dobles
					//strCveIf				=Comunes.quitaComitasSimplesyDobles(strCveIf);
					strCveEpo			=Comunes.quitaComitasSimplesyDobles(strCveEpo);
					strRFCpyme			=Comunes.quitaComitasSimplesyDobles(strRFCpyme);
					strCveMoneda		=Comunes.quitaComitasSimplesyDobles(strCveMoneda);
					strTipoMov			=Comunes.quitaComitasSimplesyDobles(strTipoMov);
					strCompDomFiscal	=Comunes.quitaComitasSimplesyDobles(strCompDomFiscal);
					strCompAltaHac		=Comunes.quitaComitasSimplesyDobles(strCompAltaHac);
					strCedulaRFC		=Comunes.quitaComitasSimplesyDobles(strCedulaRFC);
					strIdeOfiVig		=Comunes.quitaComitasSimplesyDobles(strIdeOfiVig);
					strEdoCtaCheuqes	=Comunes.quitaComitasSimplesyDobles(strEdoCtaCheuqes);
					strConvPymeNafin	=Comunes.quitaComitasSimplesyDobles(strConvPymeNafin);
					strOtrDictisFact	=Comunes.quitaComitasSimplesyDobles(strOtrDictisFact);
					strContratoIf		=Comunes.quitaComitasSimplesyDobles(strContratoIf);
					strNotas				=Comunes.quitaComitasSimplesyDobles(strNotas);

					// Recortar los caracteres de los campos en caso que excedan el tama�o permitido

					//strCveIf				=((strCveIf.length()>6)?strCveIf.substring(0,7):strCveIf);
					strCveEpo			=((strCveEpo.length()>6)?strCveEpo.substring(0,7):strCveEpo);
					strRFCpyme			=((strRFCpyme.length()>20)?strRFCpyme.substring(0,21):strRFCpyme);
					strCveMoneda		=((strCveMoneda.length()>4)?strCveMoneda.substring(0,5):strCveMoneda);
					strTipoMov			=((strTipoMov.length()>1)?strTipoMov.substring(0,2):strTipoMov);
					strCompDomFiscal	=((strCompDomFiscal.length()>1)?strCompDomFiscal.substring(0,2):strCompDomFiscal);
					strCompAltaHac		=((strCompAltaHac.length()>1)?strCompAltaHac.substring(0,2):strCompAltaHac);
					strCedulaRFC		=((strCedulaRFC.length()>1)?strCedulaRFC.substring(0,2):strCedulaRFC);
					strIdeOfiVig		=((strIdeOfiVig.length()>1)?strIdeOfiVig.substring(0,2):strIdeOfiVig);
					strEdoCtaCheuqes	=((strEdoCtaCheuqes.length()>1)?strEdoCtaCheuqes.substring(0,2):strEdoCtaCheuqes);
					strConvPymeNafin	=((strConvPymeNafin.length()>1)?strConvPymeNafin.substring(0,2):strConvPymeNafin);
					strOtrDictisFact	=((strOtrDictisFact.length()>1)?strOtrDictisFact.substring(0,2):strOtrDictisFact);
					strContratoIf		=((strContratoIf.length()>1)?strContratoIf.substring(0,2):strContratoIf);
					strNotas				=((strNotas.length()>1000)?strNotas.substring(0,1001):strNotas);

					if(!Comunes.esNumero(strCveEpo))strCveEpo="";
					if(!Comunes.esNumero(strCveMoneda))strCveMoneda="";

					System.out.println("strCveEpo==="+strCveEpo);
					System.out.println("strRFCpyme==="+strRFCpyme);
					System.out.println("strCveMoneda==="+strCveMoneda);
					System.out.println("strTipoMov==="+strTipoMov);
					System.out.println("strCompDomFiscal==="+strCompDomFiscal);
					System.out.println("strCompAltaHac==="+strCompAltaHac);
					System.out.println("strCedulaRFC==="+strCedulaRFC);
					System.out.println("strIdeOfiVig==="+strIdeOfiVig);
					System.out.println("strEdoCtaCheuqes==="+strEdoCtaCheuqes);
					System.out.println("strConvPymeNafin==="+strConvPymeNafin);
					System.out.println("strOtrDictisFact==="+strOtrDictisFact);
					System.out.println("strContratoIf==="+strContratoIf);
					System.out.println("strNotas==="+strNotas);

					// Insertar los datos en la tabla temporal COMTMP_AFILIA_PYME_MASIVA
					qry = "INSERT INTO comtmp_ctasbanc_pyme_masiva " +
							"            (ic_numero_proceso, ic_numero_linea, ic_if, ic_epo, cg_rfc_pyme, " +
							"             ic_moneda, cg_tipo_mov, cs_comp_domicilio_fiscal, " +
							"             cs_comp_alta_hacienda, cs_cedula_rfc, cs_ident_oficial_vig, " +
							"             cs_edo_cta_cheques, cs_convenio_pyme_nafin, " +
							"             cs_otros_doctos_factoraje, cs_contrato_if, cg_notas, cg_error " +
							"            ) " +
							"     VALUES (?, ?, ?, ?, ?, " +
							"             ?, ?, ?, " +
							"             ?, ?, ?, " +
							"             ?, ?, " +
							"             ?, ?, ?, ? " +
							"            ) ";
					ps = con.queryPrecompilado(qry);
					ps.clearParameters();

					ps.setLong(++params,Long.parseLong(procesoId));
					ps.setInt(++params,numeroLinea);

					if(cveif==null || cveif.equals(""))
						ps.setNull(++params,Types.INTEGER);
					else
						ps.setLong(++params,Long.parseLong(cveif));

					if(strCveEpo==null || strCveEpo.equals(""))
						ps.setNull(++params,Types.INTEGER);
					else
						ps.setLong(++params,Long.parseLong(strCveEpo));

					ps.setString(++params,strRFCpyme);

					if(strCveMoneda==null || strCveMoneda.equals(""))
						ps.setNull(++params,Types.INTEGER);
					else
						ps.setLong(++params,Long.parseLong(strCveMoneda));

					ps.setString(++params,strTipoMov);
					ps.setString(++params,strCompDomFiscal);
					ps.setString(++params,strCompAltaHac);
					ps.setString(++params,strCedulaRFC);
					ps.setString(++params,strIdeOfiVig);
					ps.setString(++params,strEdoCtaCheuqes);
					ps.setString(++params,strConvPymeNafin);
					ps.setString(++params,strOtrDictisFact);
					ps.setString(++params,strContratoIf);
					ps.setString(++params,strNotas);
					ps.setString(++params,"");
					ps.executeUpdate();
					ps.close();

					//Realizar un COMMIT cada 100 registros
					if (numeroLinea % 100 == 0)
						con.terminaTransaccion(true);

					numeroLinea++;
				}

				contregistros++;
			}
		}catch (IOException ioe){
			lbOK = false;
			ioe.printStackTrace();
			throw new AppException("Error al cargar reg en tabla temporal",ioe);
		}catch (Exception e){
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Error al cargar reg en tabla temporal",e);
		}finally{
			con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}


	/**
	 * Realiza la validacion del archivo cargado para la autorizacion o rechazo masivo
	 * de las cuentas PYMEs por autorizar
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param ruta
	 */
	public void validaCargaLiberMasiva(String procesoId, String cveIf) throws AppException{
		//HashMap hmap = new HashMap();
		List lstRegErroneos = new ArrayList();
		List lstRegCorrectos = new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		boolean commit = true;
		try{
			con.conexionDB();
			qry = "SELECT   ic_numero_proceso, ic_numero_linea, ic_if, ic_epo, cg_rfc_pyme, " +
					"         ic_moneda, cg_tipo_mov, cs_comp_domicilio_fiscal, " +
					"         cs_comp_alta_hacienda, cs_cedula_rfc, cs_ident_oficial_vig, " +
					"         cs_edo_cta_cheques, cs_convenio_pyme_nafin, " +
					"         cs_otros_doctos_factoraje, cs_contrato_if, cg_notas, cg_error " +
					"    FROM comtmp_ctasbanc_pyme_masiva " +
					"   WHERE ic_numero_proceso = ? AND ic_if = ? " +
					"ORDER BY ic_numero_linea ";
			ps = con.queryPrecompilado(qry);
			ps.setLong(1,Long.parseLong(procesoId));
			ps.setLong(2,Long.parseLong(cveIf));
			rs = ps.executeQuery();

			while(rs.next()){
				HashMap hmCuentaBanc = new HashMap();
				List lstErrores = new ArrayList();
				List lstCorrectos = new ArrayList();
				StringBuffer cgError = new StringBuffer();
				String cvePyme = "";
				String cveCtaBanc = "";
				String csExpEfile = "";
				String csPendAut = "";
				String csConvUnico = "";

				String ic_numero_proceso = rs.getString("ic_numero_proceso")==null?"":rs.getString("ic_numero_proceso");
				String ic_numero_linea = rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea");
				String ic_if = rs.getString("ic_if")==null?"":rs.getString("ic_if");
				String ic_epo = rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String cg_rfc_pyme = rs.getString("cg_rfc_pyme")==null?"":rs.getString("cg_rfc_pyme");
				String ic_moneda =rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String cg_tipo_mov = rs.getString("cg_tipo_mov")==null?"":rs.getString("cg_tipo_mov");
				String cs_comp_domicilio_fiscal = rs.getString("cs_comp_domicilio_fiscal")==null?"":rs.getString("cs_comp_domicilio_fiscal");
				String cs_comp_alta_hacienda = rs.getString("cs_comp_alta_hacienda")==null?"":rs.getString("cs_comp_alta_hacienda");
				String cs_cedula_rfc = rs.getString("cs_cedula_rfc")==null?"":rs.getString("cs_cedula_rfc");
				String cs_ident_oficial_vig = rs.getString("cs_ident_oficial_vig")==null?"":rs.getString("cs_ident_oficial_vig");
				String cs_edo_cta_cheques = rs.getString("cs_edo_cta_cheques")==null?"":rs.getString("cs_edo_cta_cheques");
				String cs_convenio_pyme_nafin =rs.getString("cs_convenio_pyme_nafin")==null?"":rs.getString("cs_convenio_pyme_nafin");
				String cs_otros_doctos_factoraje = rs.getString("cs_otros_doctos_factoraje")==null?"":rs.getString("cs_otros_doctos_factoraje");
				String cs_contrato_if = rs.getString("cs_contrato_if")==null?"":rs.getString("cs_contrato_if");
				String cg_notas = rs.getString("cg_notas")==null?"":rs.getString("cg_notas");
				//String cg_error = rs.getString("cg_error")==null?"":rs.getString("cg_error");

				//INICIAN VALIDACIONES.
				//1.- validacion rfc
				cvePyme = validaRfcLiberMasivaPyme(cg_rfc_pyme);
				if("".equals(cvePyme)){
					cgError.append("El RFC no existe\\");
				}else{
					//8. se valida formato del campo ic_epo
					if (ic_epo.equals("") || !Comunes.esNumero(ic_epo) ){
						cgError.append("El formato del ic_epo es incorrecto\\");
					}
					//9. se valida formato del rfc de la pyme
					if (!cg_rfc_pyme.equals("") && !Comunes.validaRFC(cg_rfc_pyme,'F') && !Comunes.validaRFC(cg_rfc_pyme,'M')){
						cgError.append("El formato del RFC es incorrecto\\");
					}
					//10. se valida clave de moneda
					if (ic_moneda.equals("") || !Comunes.esNumero(ic_moneda) ){
						cgError.append("La clave de la moneda no es correcta\\");
					}else{
						if(!"1".equals(ic_moneda) && !"54".equals(ic_moneda))
							cgError.append("La clave de la moneda no es correcta\\");
					}
					//10. se valida tipo de movimiento
					if(!"R".equals(cg_tipo_mov) && !"A".equals(cg_tipo_mov)){
						cgError.append("El dato permitido para el campo Movimiento es (A)utorizaci�n � (R)echazo\\");
					}

					if(!"".equals(ic_epo) && !"".equals(ic_if) && !"".equals(ic_moneda)){
						hmCuentaBanc = validaExisteCtaBancPymeIf(cvePyme,ic_if,ic_epo,ic_moneda);
						cveCtaBanc = hmCuentaBanc.get("ic_cuenta_bancaria")==null?"":(String)hmCuentaBanc.get("ic_cuenta_bancaria");
						csPendAut  = hmCuentaBanc.get("cs_estatus")==null?"":(String)hmCuentaBanc.get("cs_estatus");
						csExpEfile = hmCuentaBanc.get("exp_efile")==null?"":(String)hmCuentaBanc.get("exp_efile");
						csConvUnico = hmCuentaBanc.get("cs_convenio_unico")==null?"":(String)hmCuentaBanc.get("cs_convenio_unico");

						//2.-validacion cuentas banc relacion pyme-if
						if("".equals(cveCtaBanc)){
							cgError.append("El Proveedor no est� parametrizado para esta Cadena\\");
						}
						//3.-valida que la cuenta bancaria este pendiente por autorizar
						if(!"".equals(csPendAut)){
							cgError.append("La cuenta bancaria debe estar en estatus pendiente por autorizar\\");
						}
						//4.-valida que la pyme tenga expediente digital
						if(!"S".equals(csExpEfile)){
							cgError.append("No se cuenta con expediente digital, contacte al Administrador\\");
						}
						//5. valida que no exista otra cuenta igual enla carga
						if(validaCuentaBancDuplicada(ic_numero_proceso, ic_epo, ic_if, cg_rfc_pyme, ic_moneda, ic_numero_linea)){
							cgError.append("No se puede autorizar dos cuentas bancarias iguales\\");
						}

						//6. se valida que la info, este completa para el caso de los registros a rechazar
						if("R".equals(cg_tipo_mov)){
							if( (ic_epo.equals("")) ||
								 (cg_rfc_pyme.equals(""))||
								 (ic_moneda.equals(""))||
								 (cs_comp_domicilio_fiscal.equals("")) ||
								 (cs_comp_alta_hacienda.equals(""))||
								 (cs_cedula_rfc.equals(""))||
								 (cs_ident_oficial_vig.equals(""))||
								 (cs_edo_cta_cheques.equals(""))||
								 (cs_convenio_pyme_nafin.equals(""))||
								 (cs_otros_doctos_factoraje.equals(""))||
								 (cs_contrato_if.equals(""))||
								 (cg_notas.equals(""))
								)
								cgError.append("El registro no contiene el n�mero m�nimo de campos\\");
						}

						//7. se valida que la info este completa para el caso de los registros a autorizar
						if("A".equals(cg_tipo_mov)){
							if( (ic_epo.equals("")) || (cg_rfc_pyme.equals(""))|| (ic_moneda.equals("")))
								cgError.append("El registro no contiene el n�mero m�nimo de campos\\");
						}
						//validaciones de formato

						//11.

						//12
						if(!"S".equals(cs_comp_domicilio_fiscal) && !"N".equals(cs_comp_domicilio_fiscal)){
							cgError.append("El dato permitido para el campo  COMPROBANTE_DOMICILIO_FISCAL es S � N\\");
						}
						//13
						if(!"S".equals(cs_comp_alta_hacienda) && !"N".equals(cs_comp_alta_hacienda)){
							cgError.append("El dato permitido para el campo COMPROBANTE_ALTA_HACIENDA es S � N\\");
						}
						//14
						if(!"S".equals(cs_cedula_rfc) && !"N".equals(cs_cedula_rfc)){
							cgError.append("El dato permitido para el campo  CEDULA_RFC es S � N\\");
						}
						//15
						if(!"S".equals(cs_ident_oficial_vig) && !"N".equals(cs_ident_oficial_vig)){
							cgError.append("El dato permitido para el campo  IDENTIFICACION_OFICIAL_VIGENTE es S � N\\");
						}
						//16
						if(!"S".equals(cs_edo_cta_cheques) && !"N".equals(cs_edo_cta_cheques)){
							cgError.append("El dato permitido para el campo  ESTADO_CUENTA_CHEQUES es S � N\\");
						}
						//17
						if(!"S".equals(cs_convenio_pyme_nafin) && !"N".equals(cs_convenio_pyme_nafin)){
							cgError.append("El dato permitido para el campo  CONVENIO_PYME_NAFIN es S � N\\");
						}
						//18
						if(!"S".equals(cs_otros_doctos_factoraje) && !"N".equals(cs_otros_doctos_factoraje)){
							cgError.append("El dato permitido para el campo  OTROS_DOCTOS_FACTORAJE es S � N\\");
						}
						//19
						if(!"S".equals(cs_contrato_if) && !"N".equals(cs_contrato_if)){
							cgError.append("El dato permitido para el campo  CONTRATO_IF es S � N\\");
						}

						//20 se omite valiadcion de los mi caracteres para las notas ya que se limitan anteriormente
						//21. se valida que exista al menos una causa de rechazo (S)
						if("R".equals(cg_tipo_mov)){
							if( !(cs_comp_domicilio_fiscal.equals("S")) &&
								 !(cs_comp_alta_hacienda.equals("S"))&&
								 !(cs_cedula_rfc.equals("S"))&&
								 !(cs_ident_oficial_vig.equals("S"))&&
								 !(cs_edo_cta_cheques.equals("S"))&&
								 !(cs_convenio_pyme_nafin.equals("S"))&&
								 !(cs_otros_doctos_factoraje.equals("S"))&&
								 !(cs_contrato_if.equals("S"))
								)
								cgError.append("Es necesario especificar el documento causa del Rechazo\\");
						}
						//22. se valida que existan notas para cuando se efectua un rechazo
						if("R".equals(cg_tipo_mov)){
							if(cg_notas==null || cg_notas.equals(""))
								cgError.append("Es necesario especificar las Notas para la causa del Rechazo\\");
						}
						//23. se valida que no tenga causas de rechazo cuando se efectua una autorizacionm
						if("A".equals(cg_tipo_mov)){
							if( !(cs_comp_domicilio_fiscal.equals("N")) |
								 !(cs_comp_alta_hacienda.equals("N"))||
								 !(cs_cedula_rfc.equals("N"))||
								 !(cs_ident_oficial_vig.equals("N"))||
								 !(cs_edo_cta_cheques.equals("N"))||
								 !(cs_convenio_pyme_nafin.equals("N"))||
								 !(cs_otros_doctos_factoraje.equals("N"))||
								 !(cs_contrato_if.equals("N"))
								)
								cgError.append("Para las autorizaciones los documentos se deben enviar con el valor N\\");
						}
					}
				}//finaliza if-else

				if(cgError.length()>1){
					/*0*/lstErrores.add(ic_numero_proceso);
					/*1*/lstErrores.add(ic_numero_linea);
					/*2*/lstErrores.add(cgError.toString());
					lstRegErroneos.add(lstErrores);
				}else{
					/*0*/lstCorrectos.add(ic_numero_proceso);
					/*1*/lstCorrectos.add(ic_numero_linea);
					/*2*/lstCorrectos.add(cveCtaBanc);
					/*3*/lstCorrectos.add(csConvUnico);
					lstRegCorrectos.add(lstCorrectos);
				}

			}//finaliza while()

			rs.close();
			ps.close();

			//Se guardan registros con errores defectuosos
			if(lstRegErroneos!=null && lstRegErroneos.size()>0){
				qry = " UPDATE comtmp_ctasbanc_pyme_masiva " +
						"   SET cg_error = ? " +
						" WHERE ic_numero_proceso = ? AND ic_numero_linea = ? ";

				List lstReg = new ArrayList();
				for(int x=0; x<lstRegErroneos.size();x++){
					lstReg = (List)lstRegErroneos.get(x);

					ps = con.queryPrecompilado(qry);
					ps.setString(1, (String)lstReg.get(2));
					ps.setLong(2, Long.parseLong((String)lstReg.get(0)));
					ps.setLong(3, Long.parseLong((String)lstReg.get(1)));
					ps.executeUpdate();
					ps.close();

				}//finaliza::for(;;)
			}//finaliza::if(lstRegErroneos!=null && lstRegErroneos.size()>0)

			if(lstRegCorrectos!=null && lstRegCorrectos.size()>0){
				qry = " UPDATE comtmp_ctasbanc_pyme_masiva " +
						"   SET ic_cuenta_bancaria = ?, cs_convenio_unico = ?  " +
						" WHERE ic_numero_proceso = ? AND ic_numero_linea = ? ";

				List lstReg = new ArrayList();
				for(int x=0; x<lstRegCorrectos.size();x++){
					lstReg = (List)lstRegCorrectos.get(x);

					ps = con.queryPrecompilado(qry);
					ps.setString(1, (String)lstReg.get(2));
					ps.setString(2, (String)lstReg.get(3));
					ps.setLong(3, Long.parseLong((String)lstReg.get(0)));
					ps.setLong(4, Long.parseLong((String)lstReg.get(1)));
					ps.executeUpdate();
					ps.close();

				}//finaliza::for(;;)
			}//finaliza::if(lstRegErroneos!=null && lstRegErroneos.size()>0)


		}catch(Exception e){
			commit= false;
			e.printStackTrace();
			throw new AppException("Error al validar registros masivos de Cuentas Banc Pymes",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
		//return hmap;
	}


	private String validaRfcLiberMasivaPyme(String rfc) throws AppException{
		log.info("validaRfcLiberMasivaPyme(S)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();
			String cvePyme = "";

			//Se valida si existe la pyme a validar
			qry = "select ic_pyme from comcat_pyme where cg_rfc = ? and cs_habilitado = ? ";
			ps = con.queryPrecompilado(qry);
			ps.setString(1, rfc);
			ps.setString(2, "S");
			rs = ps.executeQuery();
			if(rs.next()){
				cvePyme = rs.getString("ic_pyme");
			}
			rs.close();
			ps.close();

			return cvePyme;
		}catch(Exception e){
			log.info("validaRfcLiberMasivaPyme(Error):::");
			e.printStackTrace();
			throw new AppException("Error al validar si existe RFC pyme en liberacion masiva de ctas banc",e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("validaRfcLiberMasivaPyme (S)");
		}
	}

	private HashMap validaExisteCtaBancPymeIf(String cvePyme, String cveIf, String cveEpo, String cveMoneda){
		log.info("validaExisteCtaBancPymeIf(S)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();
			HashMap map = new HashMap();

			//Se valida si existe cuenta bancaria de la pyme con el if
			if(cvePyme!=null && !"".equals(cvePyme)){
				qry=" SELECT cpi.ic_cuenta_bancaria ic_cuenta_bancaria, " +
					" cpi.cs_estatus cs_estatus, cp.cs_expediente_efile exp_efile, cp.cs_convenio_unico "+
					"  FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,  comcat_pyme cp " +
					" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
					"	 AND ccb.ic_pyme = cp.ic_pyme " +
					"   AND ccb.ic_pyme = ? " +
					"   AND ccb.cs_borrado = ? " +
					"   AND ccb.ic_moneda = ? " +
					"   AND cpi.cs_borrado = ? " +
					"   AND cpi.cs_vobo_if = ? " +
					"	 AND cpi.ic_if = ? "+
					"	 AND cpi.ic_epo = ? "+
					"   AND cpi.cs_estatus IS NULL " +
					"   AND cpi.df_autorizacion IS NULL ";

				ps = con.queryPrecompilado(qry);
				ps.setLong(1, Long.parseLong(cvePyme));
				ps.setString(2, "N");
				ps.setLong(3, Long.parseLong(cveMoneda));
				ps.setString(4, "N");
				ps.setString(5, "N");
				ps.setLong(6, Long.parseLong(cveIf));
				ps.setLong(7, Long.parseLong(cveEpo));
				rs = ps.executeQuery();
				if(rs.next()){
					//cveCuentaBanc = rs.getString("ic_cuenta_bancaria");
					map.put("ic_cuenta_bancaria",rs.getString("ic_cuenta_bancaria"));
					map.put("cs_estatus",rs.getString("cs_estatus"));
					map.put("exp_efile",rs.getString("exp_efile"));
					map.put("cs_convenio_unico",rs.getString("cs_convenio_unico"));

				}
				rs.close();
				ps.close();
			}

			return map;
		}catch(Exception e){
			log.info("validaExisteCtaBancPymeIf(Error):::");
			e.printStackTrace();
			throw new AppException("Error al validar si existe Cuenta Banc pyme-if en liberacion masiva de ctas banc",e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("validaExisteCtaBancPymeIf (S)");
		}
	}

	private boolean validaCuentaBancDuplicada(String procesoId, String cveEPO, String cveIf, String rfc, String cveMoneda, String numLinea) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		boolean existe = false;
		try{
			con.conexionDB();
			qry = "SELECT COUNT (1) ctarepetida " +
					"  FROM comtmp_ctasbanc_pyme_masiva " +
					" WHERE ic_numero_proceso = ? " +
					"   AND ic_if = ? " +
					"   AND ic_epo = ? " +
					"   AND cg_rfc_pyme = ? " +
					"   AND ic_moneda = ? " +
					"   AND ic_numero_linea < ? ";
			ps = con.queryPrecompilado(qry);
			ps.setLong(1,Long.parseLong(procesoId));
			ps.setLong(2,Long.parseLong(cveIf));
			ps.setLong(3,Long.parseLong(cveEPO));
			ps.setString(4,rfc);
			ps.setLong(5,Long.parseLong(cveMoneda));
			ps.setLong(6,Long.parseLong(numLinea));
			rs = ps.executeQuery();
			if(rs.next()){
				if(rs.getInt(1)== 0)
					existe = false;
				else
					existe = true;
			}

			return existe;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error al validar duplicidad en carga de liberacion masiva de cuentas banc",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene resumen de la carga masiva de cuentas bancarias pyme a liberar
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param procesoId
	 */
	public HashMap getResLiberMasivaCtasPymes(String procesoId) throws AppException{
		log.info("getResLiberMasivaCtasPymes(E)");
		List lstRegOk = new ArrayList();
		List lstRegX = new ArrayList();
		HashMap mapResumen= new HashMap();
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();

			qry = " SELECT ic_numero_linea, cg_rfc_pyme, ic_epo, ic_moneda, cg_error " +
					"  FROM comtmp_ctasbanc_pyme_masiva " +
					" WHERE ic_numero_proceso = ? AND cg_error IS  NULL "+
					" ORDER BY ic_numero_linea ";
			ps = con.queryPrecompilado(qry);
			ps.setLong(1, Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				List lstReg = new ArrayList();
				lstReg.add(rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea"));
				lstReg.add(rs.getString("cg_rfc_pyme")==null?"":rs.getString("cg_rfc_pyme"));
				lstReg.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
				lstReg.add(rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda"));
				lstReg.add(rs.getString("cg_error")==null?"":rs.getString("cg_error"));
				lstRegOk.add(lstReg);
			}
			rs.close();
			ps.close();

			//REGISTROS ERRORNEOS

			qry = " SELECT ic_numero_linea, cg_rfc_pyme, ic_epo, ic_moneda, cg_error " +
					"  FROM comtmp_ctasbanc_pyme_masiva " +
					" WHERE ic_numero_proceso = ? AND cg_error IS NOT NULL "+
					" ORDER BY ic_numero_linea ";
			ps = con.queryPrecompilado(qry);
			ps.setLong(1, Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				List lstReg = new ArrayList();
				lstReg.add(rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea"));
				lstReg.add(rs.getString("cg_rfc_pyme")==null?"":rs.getString("cg_rfc_pyme"));
				lstReg.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
				lstReg.add(rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda"));
				lstReg.add(rs.getString("cg_error")==null?"":rs.getString("cg_error"));
				lstRegX.add(lstReg);
			}
			rs.close();
			ps.close();

			mapResumen.put("correctos",lstRegOk);
			mapResumen.put("errores",lstRegX);

			return mapResumen;

		}catch(Exception e){
			log.info("getResLiberMasivaCtasPymes(Error)");
			e.printStackTrace();
			throw new AppException("Error al obtener resumen de carga de archivo de liberacion masiva",e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getResLiberMasivaCtasPymes(S)");
		}
	}

	public List getPreAcuseLiberMasivaPymes(String procesoId) throws AppException{
		log.info("getPreAcuseLiberMasivaPymes(E)");
		List lstRegPreAcuse = new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();
			qry = "SELECT   ccp.ic_numero_proceso, ccp.ic_numero_linea, ccp.ic_if ic_if,  " +
					"		 	 ce.cg_razon_social nomEpo, " +
					"         cp.cg_razon_social nomPyme,  " +
					"         cm.cd_nombre nomMoneda,  " +
					"         DECODE(ccp.cg_tipo_mov,'A','Autorizar','R','Rechazar') cg_tipo_mov, " +
					"         DECODE(NVL(ccp.cs_comp_domicilio_fiscal,'N'),'N','No','S','Si') cs_comp_domicilio_fiscal,  " +
					"         DECODE(NVL(ccp.cs_comp_alta_hacienda,'N'),'N','No','S','Si') cs_comp_alta_hacienda, " +
					"         DECODE(NVL(ccp.cs_cedula_rfc,'N'),'N','No','S','Si') cs_cedula_rfc, " +
					"         DECODE(NVL(ccp.cs_ident_oficial_vig,'N'),'N','No','S','Si') cs_ident_oficial_vig, " +
					"         DECODE(NVL(ccp.cs_edo_cta_cheques,'N'),'N','No','S','Si') cs_edo_cta_cheques, " +
					"         DECODE(NVL(ccp.cs_convenio_pyme_nafin,'N'),'N','No','S','Si') cs_convenio_pyme_nafin, " +
					"         DECODE(NVL(ccp.cs_otros_doctos_factoraje,'N'),'N','No','S','Si') cs_otros_doctos_factoraje, " +
					"         DECODE(NVL(ccp.cs_contrato_if,'N'),'N','No','S','Si') cs_contrato_if, " +
					"         DECODE(ccp.cg_tipo_mov,'A','-','R','Rechazado IF') causaRechazo, " +
					"         ccp.cg_notas          " +
					"    FROM comtmp_ctasbanc_pyme_masiva ccp, " +
					"         comcat_epo ce, " +
					"         comcat_pyme cp, " +
					"         comcat_moneda cm " +
					"   WHERE ccp.ic_epo = ce.ic_epo " +
					"     AND ccp.ic_moneda = cm.ic_moneda " +
					"     AND ccp.cg_rfc_pyme = cp.cg_rfc " +
					"     AND ic_numero_proceso = ? " +
					"     AND cg_error IS NULL " +
					"ORDER BY ic_numero_linea  ";

			ps = con.queryPrecompilado(qry);
			ps.setLong(1, Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				List lstReg = new ArrayList();
				/*00*/lstReg.add(rs.getString("ic_numero_proceso")==null?"":rs.getString("ic_numero_proceso"));
				/*01*/lstReg.add(rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea"));
				/*02*/lstReg.add(rs.getString("ic_if")==null?"":rs.getString("ic_if"));
				/*03*/lstReg.add(rs.getString("nomEpo")==null?"":rs.getString("nomEpo"));
				/*04*/lstReg.add(rs.getString("nomPyme")==null?"":rs.getString("nomPyme"));
				/*05*/lstReg.add(rs.getString("nomMoneda")==null?"":rs.getString("nomMoneda"));
				/*06*/lstReg.add(rs.getString("cg_tipo_mov")==null?"":rs.getString("cg_tipo_mov"));
				/*07*/lstReg.add(rs.getString("cs_comp_domicilio_fiscal")==null?"":rs.getString("cs_comp_domicilio_fiscal"));
				/*08*/lstReg.add(rs.getString("cs_comp_alta_hacienda")==null?"":rs.getString("cs_comp_alta_hacienda"));
				/*09*/lstReg.add(rs.getString("cs_cedula_rfc")==null?"":rs.getString("cs_cedula_rfc"));
				/*10*/lstReg.add(rs.getString("cs_ident_oficial_vig")==null?"":rs.getString("cs_ident_oficial_vig"));
				/*11*/lstReg.add(rs.getString("cs_edo_cta_cheques")==null?"":rs.getString("cs_edo_cta_cheques"));
				/*12*/lstReg.add(rs.getString("cs_convenio_pyme_nafin")==null?"":rs.getString("cs_convenio_pyme_nafin"));
				/*13*/lstReg.add(rs.getString("cs_otros_doctos_factoraje")==null?"":rs.getString("cs_otros_doctos_factoraje"));
				/*14*/lstReg.add(rs.getString("cs_contrato_if")==null?"":rs.getString("cs_contrato_if"));
				/*15*/lstReg.add(rs.getString("cg_notas")==null?"":rs.getString("cg_notas"));
				/*16*/lstReg.add(rs.getString("causaRechazo")==null?"":rs.getString("causaRechazo"));
				lstRegPreAcuse.add(lstReg);
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();
			return  lstRegPreAcuse;
		}catch(Exception e){
			log.info("getPreAcuseLiberMasivaPymes(Error)");
			e.printStackTrace();
			throw new AppException("Error al obtener pre acuse de  liberacion masiva ctas pymes",e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getPreAcuseLiberMasivaPymes(S)");
		}
	}

	public void confirmaLiberMasivaCtasPyme(String procesoId, String cveIf, String strLogin, String iNoNafinElectronico, String strNombreUsuario, String strNombre) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		boolean commit = true;
		try{
			con.conexionDB();
			String sIfConvenioUnico = "";
			String causasR = "", nombreMoneda ="", nombrePyme ="", nombreEpo ="", nun_elecPyme ="";
			String qryRelIFPYMEOk = "", SQLExec = "", eliminaRegistroComhis_cambios_cta = "";
			UtilUsr utils = new UtilUsr();
			int total =0;
			int contadorPymeNuev = 0;
			Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

			qry = " SELECT CS_CONVENIO_UNICO " +
					"  FROM comcat_if " +
					" WHERE ic_if = " + cveIf;

				ps = con.queryPrecompilado(qry);
				rs = ps.executeQuery();
				if(rs.next()){
					sIfConvenioUnico = rs.getString("CS_CONVENIO_UNICO");
				}
		System.out.println("sIfConvenioUnico  "+sIfConvenioUnico);

				qry= "SELECT  count(*) as total "+
					"    FROM comtmp_ctasbanc_pyme_masiva ccp, " +
					"         comcat_epo ce, " +
					"         comcat_pyme cp, " +
					"         comcat_moneda cm " +
					"   WHERE ccp.ic_epo = ce.ic_epo " +
					"     AND ccp.ic_moneda = cm.ic_moneda " +
					"     AND ccp.cg_rfc_pyme = cp.cg_rfc " +
					"     AND ic_numero_proceso = ? " +
					"     AND cg_error IS NULL " +
					"ORDER BY ic_numero_linea ";

					ps = con.queryPrecompilado(qry);
					ps.setLong(1,Long.parseLong(procesoId));
					rs = ps.executeQuery();
					if(rs.next()){
						total = rs.getInt("total");
					}

			System.out.println("total  "+total);

				// obtengo las EPos que operan Factoraje Vencido
				Hashtable alParamEPO1 = new Hashtable();
				List epoFactV = new ArrayList();
				StringBuffer sepoFactV	= 	new StringBuffer();
				String qryE= "SELECT    ccp.ic_epo "  +
					"    FROM comtmp_ctasbanc_pyme_masiva ccp, " +
					"         comcat_epo ce, " +
					"         comcat_pyme cp, " +
					"         comcat_moneda cm " +
					"   WHERE ccp.ic_epo = ce.ic_epo " +
					"     AND ccp.ic_moneda = cm.ic_moneda " +
					"     AND ccp.cg_rfc_pyme = cp.cg_rfc " +
					"     AND ic_numero_proceso = ? " +
					"     AND cg_error IS NULL " +
					"ORDER BY ic_numero_linea ";

			ps = con.queryPrecompilado(qryE);
			ps.setLong(1,Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){

				String ic_epo = rs.getString("ic_epo")==null?"":rs.getString("ic_epo");

				alParamEPO1 = this.getParametrosEPO(ic_epo,1);
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();

				if(bOperaFactorajeVencido.equals("S"))  {
					epoFactV.add(ic_epo);
				}
				sepoFactV.append(ic_epo+",");
			}
			sepoFactV.deleteCharAt(sepoFactV.length()-1);


				System.out.println("sepoFactV  "+sepoFactV);

			qry= "SELECT   ccp.ic_numero_proceso, ccp.ic_numero_linea, ccp.ic_if, ccp.ic_epo, " +
					"         ccp.cg_rfc_pyme, ccp.ic_moneda, ccp.cg_tipo_mov, " +
					"         ccp.cs_comp_domicilio_fiscal, ccp.cs_comp_alta_hacienda, " +
					"         ccp.cs_cedula_rfc, ccp.cs_ident_oficial_vig, ccp.cs_edo_cta_cheques, " +
					"         ccp.cs_convenio_pyme_nafin, ccp.cs_otros_doctos_factoraje, " +
					"         ccp.cs_contrato_if, ccp.cg_notas, ccp.cg_error, " +
					"			 cp.ic_pyme, ccp.ic_cuenta_bancaria, ccp.cs_convenio_unico,  " +
					" 			 cm.cd_nombre as nombreMoneda , "+
					" 			 ce.cg_razon_social as nombreEPO,   "+
               " 		    cp.cg_razon_social as nombrePYME, " +
					"         crn.ic_nafin_electronico nun_elecPyme  "+
					"    FROM comtmp_ctasbanc_pyme_masiva ccp, " +
					"         comcat_epo ce, " +
					"         comcat_pyme cp, " +
					"         comcat_moneda cm, " +
					"			 comrel_nafin crn "+
					"   WHERE ccp.ic_epo = ce.ic_epo " +
					"     AND ccp.ic_moneda = cm.ic_moneda " +
					"     AND ccp.cg_rfc_pyme = cp.cg_rfc " +
					" 		AND cp.ic_pyme = crn.ic_epo_pyme_if "+
					" 		AND crn.cg_tipo = 'P'  "+
					"     AND ic_numero_proceso = ? " +
					"     AND cg_error IS NULL " +
					"ORDER BY ic_numero_linea ";

			ps = con.queryPrecompilado(qry);
			ps.setLong(1,Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				causasR = "";
				String ic_numero_proceso = rs.getString("ic_numero_proceso")==null?"":rs.getString("ic_numero_proceso");
				String ic_numero_linea = rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea");
				String ic_if = rs.getString("ic_if")==null?"":rs.getString("ic_if");
				String ic_epo = rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String cg_rfc_pyme = rs.getString("cg_rfc_pyme")==null?"":rs.getString("cg_rfc_pyme");
				String ic_moneda =rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String cg_tipo_mov = rs.getString("cg_tipo_mov")==null?"":rs.getString("cg_tipo_mov");
				String cs_comp_domicilio_fiscal = rs.getString("cs_comp_domicilio_fiscal")==null?"":rs.getString("cs_comp_domicilio_fiscal");
				String cs_comp_alta_hacienda = rs.getString("cs_comp_alta_hacienda")==null?"":rs.getString("cs_comp_alta_hacienda");
				String cs_cedula_rfc = rs.getString("cs_cedula_rfc")==null?"":rs.getString("cs_cedula_rfc");
				String cs_ident_oficial_vig = rs.getString("cs_ident_oficial_vig")==null?"":rs.getString("cs_ident_oficial_vig");
				String cs_edo_cta_cheques = rs.getString("cs_edo_cta_cheques")==null?"":rs.getString("cs_edo_cta_cheques");
				String cs_convenio_pyme_nafin =rs.getString("cs_convenio_pyme_nafin")==null?"":rs.getString("cs_convenio_pyme_nafin");
				String cs_otros_doctos_factoraje = rs.getString("cs_otros_doctos_factoraje")==null?"":rs.getString("cs_otros_doctos_factoraje");
				String cs_contrato_if = rs.getString("cs_contrato_if")==null?"":rs.getString("cs_contrato_if");
				String cg_notas = rs.getString("cg_notas")==null?"":rs.getString("cg_notas");
				//String cg_error = rs.getString("cg_error")==null?"":rs.getString("cg_error");
				String ic_pyme = rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String ic_cuenta_bancaria = rs.getString("ic_cuenta_bancaria")==null?"":rs.getString("ic_cuenta_bancaria");
				String cs_convenio_unico = rs.getString("cs_convenio_unico")==null?"":rs.getString("cs_convenio_unico");
				nombreMoneda  = rs.getString("nombreMoneda")==null?"":rs.getString("nombreMoneda");
				nombreEpo  = rs.getString("nombreEPO")==null?"":rs.getString("nombreEPO");
				nombrePyme  = rs.getString("nombrePYME")==null?"":rs.getString("nombrePYME");
				nun_elecPyme  = rs.getString("nun_elecPyme")==null?"":rs.getString("nun_elecPyme");


				if("A".equals(cg_tipo_mov)){//CODIGO PARA LA AUTORIZACION

					//validacion de si la pyme es nueva o vieja
					String PymeNuevaquery = "";
					PreparedStatement pstPymNueva = null;
					ResultSet 				rstPymNueva = null;

					PymeNuevaquery = " SELECT COUNT (1) " +
										"  FROM comrel_pyme_epo " +
										" WHERE cs_aceptacion = ? " +
										"  AND ic_pyme = ? ";
					pstPymNueva = con.queryPrecompilado(PymeNuevaquery);
					pstPymNueva.setString(1,"H");
					pstPymNueva.setLong(2,Long.parseLong(ic_pyme));
					rstPymNueva = pstPymNueva.executeQuery();

					if(rstPymNueva.next()){
						contadorPymeNuev = rstPymNueva.getInt(1);
					}
					rstPymNueva.close();
					pstPymNueva.close();

					//se determina si es o no es un cambio de cuenta
					String qryCambioCuenta =
							" SELECT NVL(cs_cambio_cuenta,'N') FROM comrel_pyme_if " +
							" WHERE ic_if = "+cveIf+" AND ic_epo = "+ic_epo+" AND ic_cuenta_bancaria = "+ic_cuenta_bancaria;
					ResultSet rsCambioCuenta = con.queryDB(qryCambioCuenta);

					String dfvoboif = ", df_vobo_if = SYSDATE";
					if(rsCambioCuenta.next()){
						if(rsCambioCuenta.getString(1).equals("S")){
							dfvoboif = "";
						}
					}
					rsCambioCuenta.close();

					String relacionConvenioUnico = "";
					if (sIfConvenioUnico.equals("S") && "S".equals(cs_convenio_unico)){
						relacionConvenioUnico = " , CS_CONVENIO_UNICO = 'S' ";
					}

					//query de actualizacion de la informacion
					qryRelIFPYMEOk = "UPDATE comrel_pyme_if SET cs_vobo_if = 'S' "+dfvoboif + relacionConvenioUnico +
					" WHERE ic_if = " + cveIf + " AND ic_epo = " + ic_epo +
					" AND ic_cuenta_bancaria = " + ic_cuenta_bancaria;

					SQLExec = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H', " +
					" df_aceptacion = SYSDATE WHERE ic_epo = " + ic_epo +
					" AND ic_pyme = " + ic_pyme;

					eliminaRegistroComhis_cambios_cta = "UPDATE COMHIS_CAMBIOS_CTA SET CS_AUTORIZA_IF = 'S' " +
															"WHERE IC_IF = " + cveIf + " " +
															"AND IC_EPO = " + ic_epo + " " +
															"AND IC_CUENTA_BANCARIA = '" + ic_cuenta_bancaria + "'";

					StringBuffer ValBitquery = new StringBuffer();
					ArrayList varValBitNueva = new ArrayList();
					PreparedStatement pstValBit = null;
					ResultSet 				rstValBit = null;
					//este contador indicara si ya hubo un guardado de esa misma relacion anteriormente,
					//puede ser que la cuenta se borre y entonces vendra una segunda autorizacion,
					//asi que con esto se valida no mas insert con misma pyme/if en bitacora ni actualizacion de fecha.
					int contadorValBit = 0;
					ValBitquery.append(" SELECT COUNT (1) " +
														"  FROM BIT_CONVENIO_UNICO " +
														" WHERE IC_PYME  = ? " +
														"  AND IC_IF = ?  ");
					varValBitNueva.add(new Long(ic_pyme));
					varValBitNueva.add(new Long(cveIf));
					pstValBit = con.queryPrecompilado(ValBitquery.toString(), varValBitNueva);
					rstValBit = pstValBit.executeQuery();

					if(rstValBit.next()){
						contadorValBit = rstValBit.getInt(1);
					}

					if(rstValBit!=null) rstValBit.close();
					if(pstValBit!=null) pstValBit.close();

					//SACANDO EL CONVENIO UNICO DE LA PYME
					StringBuffer strSQL = new StringBuffer();
					List varBind = new ArrayList();
					ResultSet 				rstCU = null;
					PreparedStatement pstCU = null;

					//Si los dos manejan convenio unico
					if (sIfConvenioUnico.equals("S") && "S".equals(cs_convenio_unico) && contadorValBit==0){

						//La validacion de este contador si la pyme es nueva o vieja se hace antes de actualizar su cs_aceptacion a H
						if(contadorPymeNuev==0){
							//LA PYME ES NUEVA SI EL CONTADOR ES 0
							// a)
							System.out.println("inciso A : ");
							StringBuffer insertBitacora = new StringBuffer();
							insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
																		"( " +
																		"  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
																		") VALUES " +
																		"( " +cveIf+ ","+ic_pyme+ ",SYSDATE,SYSDATE,'"+strLogin+ "') ");
							System.out.println("insertBitacora : "+insertBitacora.toString());
							con.ejecutaSQL(insertBitacora.toString());

						}else{
							//SI EL CONTADOR NO ES CERO SE TRATA DE UNA PYME QUE ES VIEJA
							//validar si es la primera vez de esa pyme en la bitacora.
							int contadorExisteEnCU = 0;
							strSQL = new StringBuffer();
							varBind = new ArrayList();
							strSQL.append("SELECT COUNT(1) " +
														"  FROM bit_convenio_unico " +
														" WHERE ic_pyme = ? ");
							varBind.add(new Long(ic_pyme));
							pstCU = con.queryPrecompilado(strSQL.toString(), varBind);
							rstCU = pstCU.executeQuery();

							if(rstCU.next()){
								contadorExisteEnCU = rstCU.getInt(1);
							}

							if(rstCU!=null)rstCU.close();
							if(pstCU!=null)pstCU.close();

							if(contadorExisteEnCU==0){
							//VALIDANDO SI YA HAY BITACORA DE ESA RELACION
								//NO HAY BITACORA
								// B)
								System.out.println("inciso B : ");
								StringBuffer insertBitacora = new StringBuffer();
								insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
																			"( " +
																			"  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
																			") VALUES " +
																			"( " +cveIf+ ","+ic_pyme+ ",SYSDATE,TO_DATE((SELECT TO_CHAR(DF_FIRMA_CONV_UNICO,'DD/MM/YYYY hh24:mi:ss') FROM COMCAT_PYME WHERE IC_PYME = "+ic_pyme+"),'DD/MM/YYYY hh24:mi:ss'),'"+strLogin+ "') ");
								System.out.println("insertBitacora : "+insertBitacora.toString());
								con.ejecutaSQL(insertBitacora.toString());

							}	else{
								//SI HAY BITACORA
								//RESPETAR FECHA PRIMER IF
								// c) Actualiza la fecha
								System.out.println("inciso C : ");
								StringBuffer insertBitacora = new StringBuffer();
								insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
																			"( " +
																			"  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
																			") VALUES " +
																			"( " +cveIf+ ","+ic_pyme+ ",SYSDATE,TO_DATE((SELECT TO_CHAR(MIN(DF_ACEPTACION_PYME),'DD/MM/YYYY hh24:mi:ss') FROM BIT_CONVENIO_UNICO WHERE IC_PYME = "+ic_pyme+"),'DD/MM/YYYY hh24:mi:ss'),'"+strLogin+ "') ");
								System.out.println("insertBitacora : "+insertBitacora.toString());
								con.ejecutaSQL(insertBitacora.toString());

							} //fin de else si hay relacion, respetar fecha

						if(rstPymNueva!=null) rstPymNueva.close();
						if(pstPymNueva!=null) pstPymNueva.close();

						} // fin de if count

						if(rstCU!=null) rstCU.close();
						if(pstCU!=null) pstCU.close();

					} //if (sIfConvenioUnico.equals("S") && iPymeConvenioUnico.equals("S")){

					con.ejecutaSQL(qryRelIFPYMEOk);
					con.ejecutaSQL(SQLExec);
					con.ejecutaSQL(eliminaRegistroComhis_cambios_cta);

					log.debug("qryRelIFPYMEOk : "+qryRelIFPYMEOk);
					log.debug("SQLExec : "+SQLExec);
					log.debug("eliminaRegistroComhis_cambios_cta : "+eliminaRegistroComhis_cambios_cta);

					/* F056-2005
					 * El cambio de perfil a la pyme se realiza en el momento que se marcan los requisitos 7 y 8
					 */
					//System.out.print("15admparamifpymealta::ic_if=" + iNoCliente + "::Cambio de Perfil o desbloqueo");

					List lstLogin = utils.getUsuariosxAfiliado(ic_pyme, "P");

					if(!lstLogin.isEmpty()) {	//Si tiene una cuenta ya existente en el OID de -----N@E-----
						String loginUsuario = lstLogin.get(0).toString();
						utils.setPerfilUsuario(loginUsuario, "ADMIN PYME");
					} else {
						//NO existe la cuenta en el OID de N@E.

						if (utils.getExisteSolicitudAltaUsuario(ic_pyme, "P")) {
							//Si no existe la cuenta en el OID pero ya hay una solicitud de alta de usuario en el ARGUS, ajusta el perfil
							utils.setPerfilSolicitudAltaUsuario(ic_pyme, "P", "ADMIN PYME");
						} else {
							//No existe la cuenta de N@E en OID y no hay solicitud de alta de usuario

							SolicitudArgus solicitud = new SolicitudArgus();
							Usuario usr = new Usuario();
							usr.setClaveAfiliado(ic_pyme);
							usr.setTipoAfiliado("P");
							solicitud.setUsuario(usr);

							String loginAsociado = utils.getCuentaUsuarioAsociada(ic_pyme);
							if (loginAsociado != null && !loginAsociado.equals("") ) {
								//Existe una cuenta (del Sistema de compras) en el OID que
								//est� preasociada al proveedor, solo se le agregan el perfil, tipo de afiliado
								//y clave de afiliado a la cuenta de manera que pueda ingresar a N@E.
								usr.setLogin(loginAsociado);
								utils.actualizarUsuario(solicitud);
								//La actualizacion del usuario no contempla la actualizacion del perfil.
								//por lo cual se hace en una instrucci�n aparte.
								utils.setPerfilUsuario(loginAsociado, "ADMIN PYME");
							} else {
								//Si no hay cuenta ni de N@E ni compras que pueda usarse, se genera la solicitud

								usr.setPerfil("ADMIN PYME");
								utils.complementarSolicitudArgus(solicitud, "P", ic_pyme);

								/*
								Context context = ContextoJNDI.getInitialContext();
								AfiliacionHome afiliacionHome = (AfiliacionHome) context.lookup("AfiliacionEJB");
								Afiliacion beanAfiliacion = afiliacionHome.create();
								*/

								Registros reg = beanAfiliacion.getDatosContacto(solicitud.getRFC(), "");
								if (reg.next()) {
									usr.setNombre(reg.getString("contacto_nombre"));
									usr.setApellidoPaterno(reg.getString("contacto_appat"));
									usr.setApellidoMaterno(reg.getString("contacto_apmat"));
									usr.setEmail(reg.getString("contacto_email"));
								}

								utils.registrarSolicitudAltaUsuario(solicitud);
							}
						}
					}//finaliza:: if(!lstLogin.isEmpty()) - else

					//FODEA-036 MEJORAS II
					StringBuffer qryNEif = new StringBuffer();
					ResultSet	rsNEif = null;
					PreparedStatement	psNEif = null;
					List variableBind = new ArrayList();

					String nafElecIF = "";
					qryNEif = new StringBuffer();
					variableBind = new ArrayList();
					qryNEif.append( " SELECT ic_nafin_electronico  " +
										"   FROM comrel_nafin  " +
										"  WHERE ic_epo_pyme_if = ? " );
					variableBind.add(new Long(cveIf));
					psNEif = con.queryPrecompilado(qryNEif.toString(), variableBind);
					rsNEif = psNEif.executeQuery();

					if(rsNEif.next()){
						nafElecIF = rsNEif.getString(1);
					}

					if(psNEif!=null) psNEif.close();
					if(rsNEif!=null) rsNEif.close();

					StringBuffer insertBitacoraGral = new StringBuffer();
					insertBitacoraGral.append(
					"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
					"\n IC_CAMBIO,  " +
					"\n CC_PANTALLA_BITACORA,  " +
					"\n CG_CLAVE_AFILIADO, " +
					"\n IC_NAFIN_ELECTRONICO, " +
					"\n DF_CAMBIO, " +
					"\n IC_USUARIO, " +
					"\n CG_NOMBRE_USUARIO, " +
					"\n CG_ANTERIOR, " +
					"\n CG_ACTUAL, " +
					"\n IC_GRUPO_EPO, " +
					"\n IC_EPO ) " +
					"\n VALUES(  " +
					"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
					"\n 'CUENTABANCIF', " +
					"\n 'P', " +
					"\n '"+iNoNafinElectronico+"', " +
					"\n SYSDATE, " +
					"\n '"+strLogin+"',  " +
					"\n '"+strNombreUsuario+"', " +
					"\n '',  " +
					"\n 'Cuenta Autorizada = "+strNombre+" "+nafElecIF+" ', " +
					"\n '', " +
					"\n '' ) " );

					log.debug("insertBitacoraGral : "+insertBitacoraGral.toString());
					con.ejecutaSQL(insertBitacoraGral.toString());

				}else if("R".equals(cg_tipo_mov)){//CODIGO PARA EL RECHAZO
					log.debug("realiza rechazo masivo n veces");

					if(Comunes.validaRFC(cg_rfc_pyme,'F')){
						causasR += "S".equals(cs_comp_domicilio_fiscal)?"214,":"";
						causasR += "S".equals(cs_comp_alta_hacienda)?"215,":"";
						causasR += "S".equals(cs_cedula_rfc)?"216,":"";
						causasR += "S".equals(cs_ident_oficial_vig)?"217,":"";
						causasR += "S".equals(cs_edo_cta_cheques)?"218,":"";
						causasR += "S".equals(cs_convenio_pyme_nafin)?"219,":"";
						causasR += "S".equals(cs_otros_doctos_factoraje)?"228,":"";//en sepera de definicion
						causasR += "S".equals(cs_contrato_if)?"230,":"";////en sepera de definicion

					}
					if(Comunes.validaRFC(cg_rfc_pyme,'M')){
						causasR += "S".equals(cs_comp_domicilio_fiscal)?"222,":"";
						causasR += "S".equals(cs_comp_alta_hacienda)?"223,":"";
						causasR += "S".equals(cs_cedula_rfc)?"224,":"";
						causasR += "S".equals(cs_ident_oficial_vig)?"225,":"";
						causasR += "S".equals(cs_edo_cta_cheques)?"226,":"";
						causasR += "S".equals(cs_convenio_pyme_nafin)?"227,":"";
						causasR += "S".equals(cs_otros_doctos_factoraje)?"229,":"";//en sepera de definicion
						causasR += "S".equals(cs_contrato_if)?"231,":"";////en sepera de definicion
					}

					realizaRechazoLiberMasiva(ic_cuenta_bancaria,cveIf,ic_epo,cg_notas,causasR,con);
				}

				PreparedStatement	psRegProcOk = null;
				String qryRegProcOk = "INSERT INTO com_ctasbanc_pyme_masiva " +
											"   SELECT   ic_numero_proceso, ic_numero_linea, ic_if, ic_epo, cg_rfc_pyme, " +
											"            ic_moneda, cg_tipo_mov, cs_comp_domicilio_fiscal, " +
											"            cs_comp_alta_hacienda, cs_cedula_rfc, cs_ident_oficial_vig, " +
											"            cs_edo_cta_cheques, cs_convenio_pyme_nafin, " +
											"            cs_otros_doctos_factoraje, cs_contrato_if, cg_notas, " +
											"            ic_cuenta_bancaria, cs_convenio_unico, sysdate " +
											"       FROM comtmp_ctasbanc_pyme_masiva " +
											"      WHERE ic_numero_proceso = ? AND ic_numero_linea = ? AND cg_error IS NULL " +
											"   ORDER BY ic_numero_linea ";

				psRegProcOk = con.queryPrecompilado(qryRegProcOk);
				psRegProcOk.setLong(1, Long.parseLong(ic_numero_proceso));
				psRegProcOk.setLong(2, Long.parseLong(ic_numero_linea));
				psRegProcOk.executeUpdate();
				psRegProcOk.close();



				//*******INICIA ************** FODEA DESCUENTO AUTOMATICO***************************


				ArrayList tipoFactoraje  =  new ArrayList();
				alParamEPO1 = new Hashtable();
				String orden ="";
				alParamEPO1 = this.getParametrosEPO(ic_epo,1);
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
				String nombreFactoraje ="";
				tipoFactoraje = new ArrayList();
				tipoFactoraje.add("N");// Factoraje Normal
				if(bOperaFactorajeVencido.equals("S"))  {
					tipoFactoraje.add("V"); // Factoraje Vencido
				}


				StringBuffer SQL	= 	new StringBuffer();
				SQL.append("	INSERT INTO COMREL_DSCTO_AUT_PYME	");
				SQL.append("		(											");
				SQL.append("			IC_PYME								");
				SQL.append("			,IC_EPO								");
				SQL.append("			,IC_IF								");
				SQL.append("			,IC_MONEDA							");
				SQL.append("			,CS_ORDEN							");
				SQL.append("			,CS_DSCTO_AUTOMATICO_DIA		");
				SQL.append("			,CS_DSCTO_AUTOMATICO_PROC		");
				SQL.append("			,CC_TIPO_FACTORAJE				");
				SQL.append("			,IC_USUARIO							");
				SQL.append("			,CG_NOMBRE_USUARIO				");
				SQL.append("			,CC_ACUSE							");
				SQL.append("			,DF_FECHAHORA_OPER				");
				SQL.append("		)											");
				SQL.append("		VALUES (									");
				SQL.append("			?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, SYSDATE    ) ");

				StringBuffer insertBitacoraGral = new StringBuffer();
				insertBitacoraGral.append(
								"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
								"\n IC_CAMBIO,  " +
								"\n CC_PANTALLA_BITACORA,  " +
								"\n CG_CLAVE_AFILIADO, " +
								"\n IC_NAFIN_ELECTRONICO, " +
								"\n DF_CAMBIO, " +
								"\n IC_USUARIO, " +
								"\n CG_NOMBRE_USUARIO, " +
								"\n CG_ANTERIOR, " +
								"\n CG_ACTUAL, " +
								"\n IC_GRUPO_EPO, " +
								"\n IC_EPO ) " +
								"\n VALUES(  " +
								"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
								"\n 'DESAUTOMA', " +  // Descuento Automatico - Nueva Version
								"\n 'P', " +
								"\n  ?  , " +
								"\n SYSDATE, " +
								"\n    ? ,  " +
								"\n   ? , " +
								"\n '',  " +
								"\n   ?,  " +
								"\n '', " +
								"\n '' ) " );

					StringBuffer SQLU	= 	new StringBuffer();
					SQLU.append(" UPDATE COMCAT_PYME 				");
					SQLU.append(" SET CS_DSCTO_AUTOMATICO = 'S' ");
					SQLU.append(" WHERE IC_PYME = ? 				");

					List variables = new ArrayList();
					PreparedStatement pst		= 	null;

				log.debug("contadorPymeNuev  "+contadorPymeNuev);
				boolean existeParam 	= false;
				if (contadorPymeNuev==0 )  {

					variables = new ArrayList();
					variables.add(ic_pyme);
					pst = con.queryPrecompilado(SQLU.toString(), variables);
					pst.executeUpdate();
					pst.close();

					for(int y=0; y<tipoFactoraje.size(); y++)  {
						String mensajeOrden = "";
						String valorFact =  (String)tipoFactoraje.get(y);
						if(valorFact.equals("N"))  {  orden = "1";  nombreFactoraje = "Normal";  mensajeOrden= "Orden= "+orden+"\n";   }
						if(valorFact.equals("V"))  {  orden = "0";  nombreFactoraje = "Vencido";    }
						String actual =   mensajeOrden+
											  " Modalidad= Primer dia desde su publicacion "+"\n"+
											  " EPO="+nombreEpo+"\n"+
											  " PYME="+nombrePyme+"\n"+
											  " IF="+strNombre	+"\n"+
											  " Moneda= "+nombreMoneda+	"\n"+
										     " Pantalla= Cuentas Bancarias PyME "+"\n"+
											  " Tipo Factoraje =" +nombreFactoraje+"\n"+
											  " Descuento Autom�tico = S " ;
						//valido si existe ya la parametrizaci�n
						existeParam= existeParametrizacion(ic_epo, ic_if, ic_pyme, ic_moneda, valorFact, con);
						if(!existeParam){

							// insert en COMREL_DSCTO_AUT_PYME
							variables = new ArrayList();
							variables.add(new Integer(ic_pyme));	//PYME
							variables.add( new Integer(ic_epo));	//EPO
							variables.add( new Integer(ic_if)); //IC_IF
							variables.add( new Integer(ic_moneda)); //MONEDA
							variables.add(orden);						//ORDEN
							variables.add("P");							//MODALIDAD p -> primer dia...
							variables.add("S");							//S - APLICA DSCTO AUTOMATICO
							variables.add(valorFact);				//TIPO FACTIRAJE
							variables.add(strLogin);					//IC_USUARIO
							variables.add(strNombreUsuario);		//NOMBRE USUARIO
							variables.add("0");	    //ACUSE
							pst = con.queryPrecompilado(SQL.toString(), variables);
							pst.executeUpdate();
							pst.close();

							log.debug("SQL.toString()  "+SQL.toString());
							log.debug("variables  "+variables);

								//insert en BIT_CAMBIOS_GRAL
							variables = new ArrayList();
							variables.add(nun_elecPyme);	//nun_elecPyme
							variables.add( strLogin);	//EPO
							variables.add( strNombreUsuario); //IC_IF
							variables.add( actual); //MONEDA
							pst = con.queryPrecompilado(insertBitacoraGral.toString(), variables);
							pst.executeUpdate();
							pst.close();
							log.debug("insertBitacoraGral  "+insertBitacoraGral.toString());
							log.debug("variables  "+variables);
						}
					}//for
				}
					//*******FINALIZA ************** FODEA DESCUENTO AUTOMATICO***************************


			}//finaliza::while()
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

		}catch(Exception e){
			commit= false;
			e.printStackTrace();
			throw new AppException("Error al confirmar carga de liberacion masiva de ctas pyme",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}

		}
	}

	/**
 	 * Este metodo se encarga de actualizar las causas de rechazo en una cuenta bancaria.
	 * @param ic_cuenta numero de la cuenta bancaria
	 * @param ic_if numero del if
	 * @param ic_epo numero de la epo
 	 * @param notaIF Notas del Banco
	 * @param causas causas de rechazo concatenadas con comas ejemplo 14,21,32, no importa el orden y al final siempre hay una coma,
	 * si no viene nada estaria como cadena vacia ""
	 *
	 * @return booleano que indica si el proceso fue exitoso
 	 */
	private boolean realizaRechazoLiberMasiva(
			String ic_cuenta,
			String ic_if,
			String ic_epo,
			String notaIF,
			String causas,
			AccesoDB con) throws AppException {

		log.info("realizaRechazo(E)");

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_cuenta == null || ic_if==null || ic_epo==null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() );
			throw new AppException("Error en los parametros recibidos. " + e.getMessage(),e);
		}
		//***************************************************************************************

		boolean exito = true;
		try {
			//borrando las causas de rechazo siempre.
			String strSQL = " delete comrel_pymeif_causa " +
											" where IC_CUENTA_BANCARIA = ? and  IC_IF = ? and  IC_EPO = ?   ";

			System.out.println("strSQL : "+strSQL);
			PreparedStatement ps =  con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_cuenta));
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.setInt(3, Integer.parseInt(ic_epo));
			ps.executeUpdate();
			ps.close();

			if(causas==null||"".equals(causas)){

				//Este caso no se debe de ejecutar ya que siempre debe de haber causas de rechazo

				//Si no hay causas nuevas se borra la bandera de Rechazado y se guarda la nota
				strSQL = " update comrel_pyme_if set CS_RECHAZO = 'N', CG_NOTA = ? " +
												" where IC_CUENTA_BANCARIA = ? and  IC_IF = ? and  IC_EPO = ? ";
				System.out.println("strSQL : "+strSQL);
				ps =  con.queryPrecompilado(strSQL);
				ps.setString(1, notaIF);
				ps.setInt(2, Integer.parseInt(ic_cuenta));
				ps.setInt(3, Integer.parseInt(ic_if));
				ps.setInt(4, Integer.parseInt(ic_epo));
				ps.executeUpdate();
				ps.close();

			}else{

				//Si hay causas

				//se agrega la bandera de que esta rechazado y su nota
				strSQL = "update comrel_pyme_if set CS_RECHAZO = 'S', CG_NOTA = ?, DF_ULT_CAUSA = SYSDATE " +
				"where IC_CUENTA_BANCARIA = ? and  IC_IF = ? and  IC_EPO = ? ";
				System.out.println("strSQL : "+strSQL);

				ps =  con.queryPrecompilado(strSQL);
				ps.setString(1, notaIF);
				ps.setInt(2, Integer.parseInt(ic_cuenta));
				ps.setInt(3, Integer.parseInt(ic_if));
				ps.setInt(4, Integer.parseInt(ic_epo));
				ps.executeUpdate();
				ps.close();


				String[] sAcausa = causas.split(",");

				for (int k = 0 ; k < sAcausa.length ; k++ ){

					//se agrega la bandera de que esta rechazado y su nota
					strSQL = " insert into comrel_pymeif_causa (IC_CUENTA_BANCARIA,IC_IF,IC_EPO,IC_CAUSA) " +
													" values (?,?,?,?) ";
					System.out.println("strSQL : "+strSQL);
					ps =  con.queryPrecompilado(strSQL);
					ps.setInt(1, Integer.parseInt(ic_cuenta));
					ps.setInt(2, Integer.parseInt(ic_if));
					ps.setInt(3, Integer.parseInt(ic_epo));
					ps.setString(4, sAcausa[k]);

					System.out.println("sAcausa[k] : "+sAcausa[k]);

					ps.executeUpdate();
					ps.close();

				} //fin de for

			}//fin de else


		} catch(Exception e) {
			log.info("realizaRechazo(Error)");
			exito = false;
			e.printStackTrace();
			throw new AppException("Error en el rechazo masivo de ctas pyme::", e);
		} finally {
			log.info("realizaRechazo(E)");
		}

		return exito;
	}

	/**
	 * Se obtiene acuse de la liberacion masiva de ctas banc pyme por numero de proceso
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param procesoId numero de proceso de la carga a obtener
	 */
	public List getAcuseLiberMasivaPymes(String procesoId)throws AppException{
		log.info("getAcuseLiberMasivaPymes(E)");
		List lstRegPreAcuse = new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();

			qry = "SELECT   ccp.ic_numero_proceso, ccp.ic_numero_linea, ccp.ic_if ic_if,  " +
					"		 	 ce.cg_razon_social nomEpo, " +
					"         cp.cg_razon_social nomPyme,  " +
					"         cm.cd_nombre nomMoneda,  " +
					"         DECODE(ccp.cg_tipo_mov,'A','Autorizar','R','Rechazar') cg_tipo_mov, " +
					"         DECODE(NVL(ccp.cs_comp_domicilio_fiscal,'N'),'N','No','S','Si') cs_comp_domicilio_fiscal,  " +
					"         DECODE(NVL(ccp.cs_comp_alta_hacienda,'N'),'N','No','S','Si') cs_comp_alta_hacienda, " +
					"         DECODE(NVL(ccp.cs_cedula_rfc,'N'),'N','No','S','Si') cs_cedula_rfc, " +
					"         DECODE(NVL(ccp.cs_ident_oficial_vig,'N'),'N','No','S','Si') cs_ident_oficial_vig, " +
					"         DECODE(NVL(ccp.cs_edo_cta_cheques,'N'),'N','No','S','Si') cs_edo_cta_cheques, " +
					"         DECODE(NVL(ccp.cs_convenio_pyme_nafin,'N'),'N','No','S','Si') cs_convenio_pyme_nafin, " +
					"         DECODE(NVL(ccp.cs_otros_doctos_factoraje,'N'),'N','No','S','Si') cs_otros_doctos_factoraje, " +
					"         DECODE(NVL(ccp.cs_contrato_if,'N'),'N','No','S','Si') cs_contrato_if, " +
					"         DECODE(ccp.cg_tipo_mov,'A','-','R','Rechazado IF') causaRechazo, " +
					"         ccp.cg_notas          " +
					"    FROM com_ctasbanc_pyme_masiva ccp, " +
					"         comcat_epo ce, " +
					"         comcat_pyme cp, " +
					"         comcat_moneda cm " +
					"   WHERE ccp.ic_epo = ce.ic_epo " +
					"     AND ccp.ic_moneda = cm.ic_moneda " +
					"     AND ccp.cg_rfc_pyme = cp.cg_rfc " +
					"     AND ic_numero_proceso = ? " +
					"ORDER BY ic_numero_linea  ";

			ps = con.queryPrecompilado(qry);
			ps.setLong(1, Long.parseLong(procesoId));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				List lstReg = new ArrayList();
				/*00*/lstReg.add(rs.getString("ic_numero_proceso")==null?"":rs.getString("ic_numero_proceso"));
				/*01*/lstReg.add(rs.getString("ic_numero_linea")==null?"":rs.getString("ic_numero_linea"));
				/*02*/lstReg.add(rs.getString("ic_if")==null?"":rs.getString("ic_if"));
				/*03*/lstReg.add(rs.getString("nomEpo")==null?"":rs.getString("nomEpo"));
				/*04*/lstReg.add(rs.getString("nomPyme")==null?"":rs.getString("nomPyme"));
				/*05*/lstReg.add(rs.getString("nomMoneda")==null?"":rs.getString("nomMoneda"));
				/*06*/lstReg.add(rs.getString("cg_tipo_mov")==null?"":rs.getString("cg_tipo_mov"));
				/*07*/lstReg.add(rs.getString("cs_comp_domicilio_fiscal")==null?"":rs.getString("cs_comp_domicilio_fiscal"));
				/*08*/lstReg.add(rs.getString("cs_comp_alta_hacienda")==null?"":rs.getString("cs_comp_alta_hacienda"));
				/*09*/lstReg.add(rs.getString("cs_cedula_rfc")==null?"":rs.getString("cs_cedula_rfc"));
				/*10*/lstReg.add(rs.getString("cs_ident_oficial_vig")==null?"":rs.getString("cs_ident_oficial_vig"));
				/*11*/lstReg.add(rs.getString("cs_edo_cta_cheques")==null?"":rs.getString("cs_edo_cta_cheques"));
				/*12*/lstReg.add(rs.getString("cs_convenio_pyme_nafin")==null?"":rs.getString("cs_convenio_pyme_nafin"));
				/*13*/lstReg.add(rs.getString("cs_otros_doctos_factoraje")==null?"":rs.getString("cs_otros_doctos_factoraje"));
				/*14*/lstReg.add(rs.getString("cs_contrato_if")==null?"":rs.getString("cs_contrato_if"));
				/*15*/lstReg.add(rs.getString("cg_notas")==null?"":rs.getString("cg_notas"));
				/*16*/lstReg.add(rs.getString("causaRechazo")==null?"":rs.getString("causaRechazo"));
				lstRegPreAcuse.add(lstReg);
			}

			rs.close();
			ps.close();

			return  lstRegPreAcuse;

		}catch(Exception e){
			log.info("getAcuseLiberMasivaPymes(Error)");
			e.printStackTrace();
			throw new AppException("Error al generar acuse de liberacion masiva de ctas banc pyme::",e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getAcuseLiberMasivaPymes(S)");
		}
	}

	/**
	 * Valida que la hora de envio de operacios de IFs se cumpla (F026-2011 FVR)
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cveEpo
	 */
	public boolean validaHoraEnvOpeIFs(String cveEpo) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sQuery = "";
		boolean horaValida = true;
		try{
			con.conexionDB();
			String horaEnvOpeIf = "";
			Calendar cal = Calendar.getInstance();
			cal.setTime(new java.util.Date());

			int iHora     = cal.get(Calendar.HOUR_OF_DAY);
			int iMinutos  = cal.get(Calendar.MINUTE);

			if(!cveEpo.equals("")){
					sQuery =
						"SELECT pe.cc_parametro_epo, cg_valor  " +
						"  FROM com_parametrizacion_epo pz, comcat_parametro_epo pe  " +
						" WHERE pz.cc_parametro_epo = pe.cc_parametro_epo  " +
						" AND pz.ic_epo = ?  " +
						" AND pz.cc_parametro_epo = ? ";


					log.debug(":::Consulta Parametros:2::"+ sQuery);
					log.debug(":::Bind::"+ cveEpo);
					ps = con.queryPrecompilado(sQuery);
					ps.setInt(1, Integer.parseInt(cveEpo));
					ps.setString(2,"HORA_ENV_OPE_IF");

					rs = ps.executeQuery();
					if(rs!=null && rs.next()) {
						horaEnvOpeIf = rs.getString("cg_valor")==null?"":(String)rs.getString("cg_valor");
						//se realiza validacion de horario
						if(!"".equals(horaEnvOpeIf)){
							int horaEnv   = Integer.parseInt( horaEnvOpeIf.substring(0, horaEnvOpeIf.indexOf(":")) );
							int minEnv   = Integer.parseInt( horaEnvOpeIf.substring(horaEnvOpeIf.indexOf(":")+1) );

							if (iHora < horaEnv){
								horaValida = false;
							}else if (iHora == horaEnv && iMinutos < minEnv){
								horaValida = false;
							}

						}
					}
					rs.close();
					if(ps!=null) ps.close();
				}
			return horaValida;
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("error al validar horario de envio de operaciones a IFs",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
//---------------------------------------------------------------------------->>
  /**
   * M�todo que obtiene la consulta de los par�metros por EPO y devuelve la informaci�n como una cadena con formato JSON.
   * @param claveEpo <code>String</code> con la clave interna de la EPO.
   * @param claveProducto <code>String</code> con la clave interna del Producto.
   * @return cadenaJSONparametrosPorEpo <code>String</code> con el JSON de los par�metros por EPO.
   * @throws AppException al ocurrir un error durante la ejecuci�n del m�todo.
   */
	public String obtenerJSONparametrosPorEpo(String claveEpo, String claveProducto) throws AppException {
    log.info("obtenerJSONparametrosPorEpo(E) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    StringBuffer cadenaJSONparametrosPorEpo = new StringBuffer();
    Hashtable parametrosPorEpo = new Hashtable();
    HashMap parametroEpo = new HashMap();
    List varBind = new ArrayList();
    JSONArray jsonArr = new JSONArray();

    try {
      con.conexionDB();

      strSQL.append(" SELECT pe.cc_parametro_epo AS parametro_epo,");
      strSQL.append(" NVL(pz.cg_valor, 'N') AS cg_valor");
      strSQL.append(" FROM com_parametrizacion_epo pz");
      strSQL.append(", comcat_parametro_epo pe");
      strSQL.append(" WHERE pz.cc_parametro_epo(+) = pe.cc_parametro_epo");
      strSQL.append(" AND pz.ic_epo(+) = ?");
      strSQL.append(" ORDER BY 1");

      varBind.add(new Integer(claveEpo));

      log.debug("..:: strSQL: "+strSQL);
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
        parametrosPorEpo.put(rst.getString("parametro_epo").trim(), rst.getString("cg_valor"));
      }

      rst.close();
      pst.close();

      strSQL = new StringBuffer();
      varBind = new ArrayList();

      strSQL.append(" SELECT NVL(CS_OPERA_NOTAS_CRED, 'N') AS OPERA_NOTAS_CRED,");			//1
      strSQL.append(" NVL(FN_MONTO_MINIMO, 0) AS MONTO_MINIMO,");									//2
		strSQL.append(" NVL(CS_DSCTO_AUTO_OBLIGATORIO,'N') AS DSCTO_AUTO_OBLIGATORIO,");		//3
      strSQL.append(" NVL(CG_DESCTO_DINAM,'N') AS DESCTO_DINAM,");								//4
      strSQL.append(" NVL(CS_LIMITE_PYME,'N') AS LIMITE_PYME,");									//5
      strSQL.append(" NVL(CS_PUB_FIRMA_MANC,'N') AS PUB_FIRMA_MANC,");							//6
      strSQL.append(" NVL(CS_PUB_HASH,'N') AS PUB_HASH,");											//7
      strSQL.append(" NVL(CS_FECHA_VENC_PYME,'N') AS FECHA_VENC_PYME,");						//8
      strSQL.append(" NVL(CS_PUB_DOCTO_VENC,'N') AS PUB_DOCTO_VENC,");							//8.1
      strSQL.append(" NVL(IG_PLAZO_PAGO_FVP, 0) AS PLAZO_PAGO_FVP,");							//8.1
      strSQL.append(" NVL(IG_PLAZO_MAX1_FVP, 0) AS PLAZO_MAX1_FVP,");							//8.2
      strSQL.append(" NVL(IG_PLAZO_MAX2_FVP, 0) AS PLAZO_MAX2_FVP,");							//8.3
      strSQL.append(" NVL(CS_PREAFILIACION,'N') AS PREAFILIACION,");								//10
      strSQL.append(" NVL(CS_DESC_AUTO_PYME,'N') AS DESC_AUTO_PYME,");						//11
      strSQL.append(" NVL(CS_PUBLICACION_EPO_PEF,'N') AS PUBLICACION_EPO_PEF,");				//12
		strSQL.append(" NVL(CS_FACTORAJE_VENCIDO,'N') AS CS_FACTORAJE_VENCIDO,");				//18
      strSQL.append(" NVL(CS_FACTORAJE_DISTRIBUIDO,'N') AS PUB_EPO_FACTORAJE_DISTRIBUIDO");	//20
      strSQL.append(" FROM comrel_producto_epo");
      strSQL.append(" WHERE ic_epo = ?");
      strSQL.append(" AND ic_producto_nafin = ?");

      varBind.add(new Integer(claveEpo));
      varBind.add(new Integer(claveProducto));

      log.debug("..:: strSQL: "+strSQL);
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      if (rst.next()) {
        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "1");
        parametroEpo.put("DESCRIPCION", "Opera Notas de Cr�dito");
        parametroEpo.put("VALOR", rst.getString("OPERA_NOTAS_CRED").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

		  if (parametrosPorEpo.get("OPERA_NOTAS_CRED") != null && parametrosPorEpo.get("OPERA_NOTAS_CRED").equals("S")) {
			  parametroEpo = new HashMap();
			  parametroEpo.put("ORDEN", "1.1");
			  parametroEpo.put("DESCRIPCION", "Aplicar una Nota de Cr�dito a varios Documentos");
			  parametroEpo.put("VALOR", parametrosPorEpo.get("CS_APLICAR_NOTAS_CRED")!=null?(parametrosPorEpo.get("CS_APLICAR_NOTAS_CRED").equals("S")?"SI":"NO"):" ");
			  jsonArr.add(JSONObject.fromObject(parametroEpo));
		  }

		  parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "2");
        parametroEpo.put("DESCRIPCION", "Monto m�nimo resultante para la aplicaci�n de Notas de Cr�dito");
        parametroEpo.put("VALOR", rst.getString("MONTO_MINIMO").equals("0")?" ":rst.getString("MONTO_MINIMO"));
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "3");
        parametroEpo.put("DESCRIPCION", "Descuento Autom�tico Obligatorio");
        parametroEpo.put("VALOR", rst.getString("DSCTO_AUTO_OBLIGATORIO").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "4");
        parametroEpo.put("DESCRIPCION", "Descuento Autom�tico Din�mico");
        parametroEpo.put("VALOR", rst.getString("DESCTO_DINAM").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "5");
        parametroEpo.put("DESCRIPCION", "Opera L�mite de L�nea por PyME");
        parametroEpo.put("VALOR", rst.getString("LIMITE_PYME").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "6");
        parametroEpo.put("DESCRIPCION", "Publicaci�n con Firma Mancomunada");
        parametroEpo.put("VALOR", rst.getString("PUB_FIRMA_MANC").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "7");
        parametroEpo.put("DESCRIPCION", "Publicaci�n con Criterio Hash");
        parametroEpo.put("VALOR", rst.getString("PUB_HASH").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "8");
        parametroEpo.put("DESCRIPCION", "Operaci�n con Fecha Vencimiento Proveedor");
        parametroEpo.put("VALOR", rst.getString("FECHA_VENC_PYME").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "8.1");
        parametroEpo.put("DESCRIPCION", "Plazo para determinar d�as m�ximos adicionales a la fecha de vencimiento del proveedor");
        parametroEpo.put("VALOR", rst.getString("PLAZO_PAGO_FVP").equals("0")?" ":rst.getString("PLAZO_PAGO_FVP"));
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "8.2");
        parametroEpo.put("DESCRIPCION", "Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicaci�n sea menor al par�metro 8.1");
        parametroEpo.put("VALOR", rst.getString("PLAZO_MAX1_FVP").equals("0")?" ":rst.getString("PLAZO_MAX1_FVP"));
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "8.3");
        parametroEpo.put("DESCRIPCION", "Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicaci�n sea mayor  al par�metro 8.1");
        parametroEpo.put("VALOR", rst.getString("PLAZO_MAX2_FVP").equals("0")?" ":rst.getString("PLAZO_MAX2_FVP"));
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "9");
        parametroEpo.put("DESCRIPCION", "Permitir publicaci�n de documentos vencidos");
        parametroEpo.put("VALOR", rst.getString("PUB_DOCTO_VENC").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "10");
        parametroEpo.put("DESCRIPCION", "Opera Pre-Afiliaci�n a Descuento PYMES");
        parametroEpo.put("VALOR", rst.getString("PREAFILIACION").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "11");
        parametroEpo.put("DESCRIPCION", "Permitir Descuento Autom�tico a PYMES");
        parametroEpo.put("VALOR", rst.getString("DESC_AUTO_PYME").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "12");
        parametroEpo.put("DESCRIPCION", "Publicaci�n EPO PEF (D�gito Identificador)");
        parametroEpo.put("VALOR", rst.getString("PUBLICACION_EPO_PEF").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        if (rst.getString("PUBLICACION_EPO_PEF").equals("S")) {
          parametroEpo = new HashMap();
          parametroEpo.put("ORDEN", "12.1");
          parametroEpo.put("DESCRIPCION", "Utiliza SIAFF");
          parametroEpo.put("VALOR", rst.getString("PUBLICACION_EPO_PEF").equals("S")?"SI":"NO");
          jsonArr.add(JSONObject.fromObject(parametroEpo));
        }

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "13");
        parametroEpo.put("DESCRIPCION", "Publicaci�n EPO PEF");
        parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_PEF_USA_SIAFF")!=null?(parametrosPorEpo.get("PUB_EPO_PEF_USA_SIAFF").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        if (parametrosPorEpo.get("PUB_EPO_PEF_USA_SIAFF") != null && parametrosPorEpo.get("PUB_EPO_PEF_USA_SIAFF").equals("S")) {
          parametroEpo.put("ORDEN", "13.1");
          parametroEpo.put("DESCRIPCION", "Fecha de Recepci�n de Bienes y Servicios");
          parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_PEF_FECHA_ENTREGA")!=null?(parametrosPorEpo.get("PUB_EPO_PEF_FECHA_ENTREGA").equals("S")?"SI":"NO"):" ");
          jsonArr.add(JSONObject.fromObject(parametroEpo));

          parametroEpo.put("ORDEN", "13.2");
          parametroEpo.put("DESCRIPCION", "Tipo de Compra (procedimiento)");
          parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_PEF_TIPO_COMPRA")!=null?(parametrosPorEpo.get("PUB_EPO_PEF_TIPO_COMPRA").equals("S")?"SI":"NO"):" ");
          jsonArr.add(JSONObject.fromObject(parametroEpo));

          parametroEpo.put("ORDEN", "13.3");
          parametroEpo.put("DESCRIPCION", "Clasificador por Objeto del Gasto");
          parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL")!=null?(parametrosPorEpo.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL").equals("S")?"SI":"NO"):" ");
          jsonArr.add(JSONObject.fromObject(parametroEpo));

          parametroEpo.put("ORDEN", "13.4");
          parametroEpo.put("DESCRIPCION", "Plazo M�ximo");
          parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_PEF_PERIODO")!=null?(parametrosPorEpo.get("PUB_EPO_PEF_PERIODO").equals("S")?"SI":"NO"):" ");
          jsonArr.add(JSONObject.fromObject(parametroEpo));
        }

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "14");
        parametroEpo.put("DESCRIPCION", "Opera Factoraje con Mandato");
        parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_OPERA_MANDATO")!=null?(parametrosPorEpo.get("PUB_EPO_OPERA_MANDATO").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "15");
        parametroEpo.put("DESCRIPCION", "Opera Factoraje Venciminto Infonavit");
        parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_VENC_INFONAVIT")!=null?(parametrosPorEpo.get("PUB_EPO_VENC_INFONAVIT").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "16");
        parametroEpo.put("DESCRIPCION", "Opera Solicitud de Consentimiento de Cesi�n de Derechos");
        parametroEpo.put("VALOR", parametrosPorEpo.get("OPER_SOLIC_CONS_CDER")!=null?(parametrosPorEpo.get("OPER_SOLIC_CONS_CDER").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "17");
        parametroEpo.put("DESCRIPCION", "Opera Factoraje 24hrs");
        parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_FACT_24HRS")!=null?(parametrosPorEpo.get("PUB_EPO_FACT_24HRS").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "18");
        parametroEpo.put("DESCRIPCION", "Opera Factoraje Vencido");
        parametroEpo.put("VALOR", rst.getString("CS_FACTORAJE_VENCIDO").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "18.1");
        parametroEpo.put("DESCRIPCION", "Opera Descuento Autom�tico � Factoraje Vencido");
        parametroEpo.put("VALOR", parametrosPorEpo.get("PUB_EPO_DESCAUTO_FACTVENCIDO")!=null?(parametrosPorEpo.get("PUB_EPO_DESCAUTO_FACTVENCIDO").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "19");
        parametroEpo.put("DESCRIPCION", "Opera Descuento Autom�tico - �ltimo d�a antes del vencimiento");
        parametroEpo.put("VALOR", parametrosPorEpo.get("DESC_AUT_ULTIMO_DIA")!=null?(parametrosPorEpo.get("DESC_AUT_ULTIMO_DIA").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "20");
        parametroEpo.put("DESCRIPCION", "Factoraje Distribuido");
        parametroEpo.put("VALOR", rst.getString("PUB_EPO_FACTORAJE_DISTRIBUIDO").equals("S")?"SI":"NO");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "20.1");
        parametroEpo.put("DESCRIPCION", "Autorizaci�n cuentas bancarias de entidades");
        parametroEpo.put("VALOR", parametrosPorEpo.get("AUTORIZA_CTAS_BANC_ENTIDADES")!=null?(parametrosPorEpo.get("AUTORIZA_CTAS_BANC_ENTIDADES").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "21");
        parametroEpo.put("DESCRIPCION", "Opera Tasas Especiales");
        parametroEpo.put("VALOR", parametrosPorEpo.get("OPER_TASAS_ESPECIALES")!=null?(parametrosPorEpo.get("OPER_TASAS_ESPECIALES").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

        StringBuffer strBuffTipoTasa = new StringBuffer();
        strBuffTipoTasa.append((parametrosPorEpo.get("TIPO_TASA").toString().equalsIgnoreCase("P")?"SI":"NO") + " - Preferencial\n");
        strBuffTipoTasa.append((parametrosPorEpo.get("TIPO_TASA").toString().equalsIgnoreCase("NG")?"SI":"NO") + " - Negociada\n");
        strBuffTipoTasa.append((parametrosPorEpo.get("TIPO_TASA").toString().equalsIgnoreCase("A")?"SI":"NO") + " - Ambas\n");

        parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "21.1");
        parametroEpo.put("DESCRIPCION", "Tipo Tasa");
        parametroEpo.put("VALOR", strBuffTipoTasa.toString());
        jsonArr.add(JSONObject.fromObject(parametroEpo));


		  parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "22");
        parametroEpo.put("DESCRIPCION", "Hora para env�o de operaci�n a IF� s");
		 parametroEpo.put("VALOR", parametrosPorEpo.get("HORA_ENV_OPE_IF").equals("N") ?" ":parametrosPorEpo.get("HORA_ENV_OPE_IF"));
        jsonArr.add(JSONObject.fromObject(parametroEpo));

		  parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "23");
        parametroEpo.put("DESCRIPCION", "Mostrar Fecha de Autorizaci�n IF en Info. de Documentos");
        parametroEpo.put("VALOR", parametrosPorEpo.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")!=null?(parametrosPorEpo.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));

		  parametroEpo = new HashMap();
        parametroEpo.put("ORDEN", "24");
        parametroEpo.put("DESCRIPCION", "Factoraje IF");
        parametroEpo.put("VALOR", parametrosPorEpo.get("FACTORAJE_IF")!=null?(parametrosPorEpo.get("FACTORAJE_IF").equals("S")?"SI":"NO"):" ");
        jsonArr.add(JSONObject.fromObject(parametroEpo));


      }

      rst.close();
      pst.close();

      cadenaJSONparametrosPorEpo.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + "})");
    } catch(Exception e) {
      log.error("obtenerJSONparametrosPorEpo(Error) ::..");
      throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("obtenerJSONparametrosPorEpo(S) ::..");
		}
		return cadenaJSONparametrosPorEpo.toString();
	}

  /**
   * Este m�todo se ejecuta cuando es necesario obtener los datos de la EPO, que
   * est�n almacenados en la tabla comcat_epo.
   * @param claveEpo <code>String</code> con el valor de la clave interna de la EPO.
   * @return informacionEpo <code>HashMap</code> que contiene el resultado de la consulta.
   * @throws AppException Cuando ocurre un error en la ejecuci�n del m�todo.
   */
  public HashMap obtenerInformacionEpo(String claveEpo) throws AppException {
    log.info("obtenerInformacionEpo(E) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    HashMap informacionEpo = new HashMap();
    List varBind = new ArrayList();

    try {
      con.conexionDB();

      strSQL.append(" SELECT ic_epo AS clave_epo,");
      strSQL.append(" cg_razon_social AS nombre_epo,");
      strSQL.append(" cg_rfc AS rfc_epo");
      strSQL.append(" FROM comcat_epo");
      strSQL.append(" WHERE ic_epo = ?");

      varBind.add(new Integer(claveEpo));

      log.debug("..:: strSQL: "+strSQL);
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while (rst.next()) {
        informacionEpo.put("claveEpo", rst.getString("clave_epo"));
        informacionEpo.put("nombreEpo", rst.getString("nombre_epo"));
        informacionEpo.put("rfcEpo", rst.getString("rfc_epo"));
      }

      rst.close();
      pst.close();
    } catch (Exception e) {
      log.error("obtenerInformacionEpo(Error) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("obtenerInformacionEpo(S) ::..");
    }
    return informacionEpo;
  }
//---------------------------------------------------------------------------->>


  public String getParamPorEPOEXTJS(String sNoEPO, String icBancoFondeo) throws AppException {
    log.info("getParamPorEPOEXTJS(E) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
		JSONArray alListaEpo = new JSONArray();
		HashMap hmParametros = null;

    try {
      con.conexionDB();

			 String sQuery =
			" SELECT cat.cg_razon_social nombre, rel.ig_dias_minimo as dmin, rel.ig_dias_maximo as dmax, rel.fn_aforo as fecha_aforo, rel.fn_aforo_dl as fecha_aforo_dol,"   +
			"        rel.cs_factoraje_vencido as fact_venc, rel.cs_factoraje_distribuido as fact_distr"   +
			"   FROM comrel_producto_epo rel,"   +
			"		comcat_epo cat" +
			"  WHERE rel.ic_epo=cat.ic_epo"+
			"    AND rel.ic_producto_nafin = ?"+
      "    AND cat.ic_banco_fondeo = ?"  ;
			if(!sNoEPO.equals("0")){
				sQuery += "    AND rel.ic_epo = ?" ;
			}
			sQuery +=" ORDER BY cat.cg_razon_social ";

			ps = con.queryPrecompilado(sQuery);
      ps.setInt(1, 1);
      ps.setInt(2, Integer.parseInt(icBancoFondeo));
			if(!sNoEPO.equals("0")){
				ps.setString(3, sNoEPO);
			}
		  rs = ps.executeQuery();

      	while(rs.next()) {
					hmParametros = new HashMap();
					hmParametros.put("nombre",(rs.getString("nombre")==null)?"":rs.getString("nombre").trim());
					hmParametros.put("dmin",(rs.getString("dmin")==null)?"":rs.getString("dmin").trim());
					hmParametros.put("dmax",(rs.getString("dmax")==null)?"":rs.getString("dmax").trim());
					hmParametros.put("fecha_aforo",(rs.getString("fecha_aforo")==null)?"":rs.getString("fecha_aforo").trim());
					hmParametros.put("fecha_aforo_dol",(rs.getString("fecha_aforo_dol")==null)?"":rs.getString("fecha_aforo_dol").trim());
					hmParametros.put("fact_venc",(rs.getString("fact_venc")==null)?"":rs.getString("fact_venc").trim());
					hmParametros.put("fact_distr",(rs.getString("fact_distr")==null)?"":rs.getString("fact_distr").trim());
					alListaEpo.add(hmParametros);
				}
				rs.close();
				if(ps!=null) ps.close();

				return "({\"success\": true, \"total\": \"" +
				alListaEpo.size() + "\", \"registros\": " + alListaEpo.toString()+"})";


    } catch (Exception e) {
      log.error("getParamPorEPOEXTJS(Error) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("getParamPorEPOEXTJS(S) ::..");
    }

  }

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param iNoCliente
    * FODEA 026-2012
	 */
	public boolean getOperaFactorajeAutomaticoGral(int ic_pyme) throws AppException {
		log.info("getOperaFactorajeAutomaticoGral");
		boolean factorajeAutomaticoGral = false;
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT COUNT (1)"   +
					"  FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
					"	WHERE ic_pyme = ?  AND pe.ic_epo = e.ic_epo"   +
					"  AND e.ic_producto_nafin = 1"   +
					"  AND cs_factoraje_if = 'S'");
			log.debug("qrySentencia" +qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, ic_pyme);
			rs = ps.executeQuery();
			ps.clearParameters();
			if (rs.next()) { if(rs.getInt(1)>0) factorajeAutomaticoGral = true; }
			rs.close();
			ps.close();
			log.debug("getOperaFactorajeAutomaticoGral" +factorajeAutomaticoGral);
		} catch (Exception e) {
			log.error("getOperaFactorajeAutomaticoGral(Error) ::..");
		throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getOperaFactorajeAutomaticoGral(S) ::..");
		}
		return factorajeAutomaticoGral;
	}
	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param iNoCliente
	 */
	public boolean getOperaFactorajeVencidoGral(int ic_pyme) throws AppException {
		log.info("getOperaFactorajeVencidoGral");
		boolean factorajeVencidoGral = false;
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT COUNT (1)"   +
					"  FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
					"	WHERE ic_pyme = ?  AND pe.ic_epo = e.ic_epo"   +
					"  AND e.ic_producto_nafin = 1"   +
					"  AND cs_factoraje_vencido = 'S'");
			log.debug("qrySentencia" +qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, ic_pyme);
			rs = ps.executeQuery();
			ps.clearParameters();
			if (rs.next()) { if(rs.getInt(1)>0) factorajeVencidoGral = true; }
			rs.close();
			ps.close();
			log.debug("factorajeVencidoGral" +factorajeVencidoGral);
		} catch (Exception e) {
			log.error("getOperaFactorajeVencidoGral(Error) ::..");
		throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getOperaFactorajeVencidoGral(S) ::..");
		}
		return factorajeVencidoGral;
	}
	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param iNoCliente
	 */
	public boolean getOperaFactorajeDistribuidoGral(int ic_pyme) throws AppException {
		log.info("getOperaFactorajeDistribuidoGral");
		boolean factorajeDistribuidoGral = false;
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT COUNT (1) FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
									"	WHERE ic_pyme = ? AND pe.ic_epo = e.ic_epo AND e.ic_producto_nafin = 1 AND cs_factoraje_distribuido = 'S'");
			log.debug("qrySentencia" +qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, ic_pyme);
			rs = ps.executeQuery();
			ps.clearParameters();
			if (rs.next()) { if(rs.getInt(1)>0) factorajeDistribuidoGral = true; }
			rs.close();
			ps.close();
			log.debug("factorajeDistribuidoGral" +factorajeDistribuidoGral);
		} catch (Exception e) {
			log.error("factorajeDistribuidoGral(Error) ::..");
		throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("factorajeDistribuidoGral(S) ::..");
		}
		return factorajeDistribuidoGral;
	}
		/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param iNoCliente
	 */
	public boolean getOperaNotasCreditoGral(int ic_pyme) throws AppException {
		log.info("getOperaNotasCreditoGral");
		boolean operaNotasCreditoGral = false;
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT COUNT (1)	FROM comrel_pyme_epo pe, comrel_producto_epo e"+
									  " WHERE ic_pyme = ?	AND pe.ic_epo = e.ic_epo AND e.ic_producto_nafin = 1 AND cs_opera_notas_cred = 'S'");
			log.debug("qrySentencia" +qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, ic_pyme);
			rs = ps.executeQuery();
			ps.clearParameters();
			if (rs.next()) { if(rs.getInt(1)>0) operaNotasCreditoGral = true; }
			rs.close();
			ps.close();
			log.debug("getOperaNotasCreditoGral" +operaNotasCreditoGral);
		} catch (Exception e) {
			log.error("getOperaNotasCreditoGral(Error) ::..");
		throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getOperaNotasCreditoGral(S) ::..");
		}
		return operaNotasCreditoGral;
	}

		/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param icEPO
	 * @param iNoCliente
	 */
	public boolean mostrarOpcionPignorado(String icEpo, String icPyme) throws AppException {
		log.info("mostrarOpcionPignorado");
		boolean opcionPignorado = false;
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append("SELECT * FROM com_param_pignora WHERE ic_pyme=? AND ic_epo=? AND cs_pignoracion='S'");
			log.debug("qrySentencia" +qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1, icEpo);
			ps.setString(2, icPyme);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())	{opcionPignorado=true;}
			rs.close();
			ps.close();
			log.debug("MostrarOpcionPignorado" +opcionPignorado);
		} catch (Exception e) {
			log.error("mostrarOpcionPignorado(Error) ::..");
		throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("mostrarOpcionPignorado(S) ::..");
		}
		return opcionPignorado;
	}

	/**
	 * Obtiene los parametros necesarios para inicializar el m�dulo de
	 * descuento
	 * @param claveAfiliado Clave del Afiliado (ic_pyme, ic_epo, etc.)
	 * @param tipoAfiliado Tipo de Afiliado (P Pyme, E Epo, I If)
	 * @param claveEpoRelacionada Clave de la epo relacionada. (aplica si tipoAfiliado=P)
	 */
	public Map getParametrosModuloDescuento(String claveAfiliado,
			String tipoAfiliado, String claveEpoRelacionada) {
		log.info("getParametrosModuloDescuento(E)");
		Map mParamsDscto = new HashMap();


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveAfiliado==null || claveAfiliado.equals("") ||
					tipoAfiliado == null || tipoAfiliado.equals("") ) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(claveAfiliado);
			if (claveEpoRelacionada != null && !claveEpoRelacionada.equals("")) {
				Integer.parseInt(claveEpoRelacionada);
			}

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//***************************************************************************************


		AccesoDB con = new AccesoDB();
		List params = null;
		try {
			con.conexionDB();
			String strSQLAforo =
					" SELECT MIN (x.fn_aforo) / 100, MIN (x.fn_aforo_dl) / 100"   +
					"   FROM (SELECT fn_aforo, fn_aforo_dl"   +
					"           FROM comrel_producto_epo"   +
					"          WHERE ic_producto_nafin = ? AND ic_epo = ?"   +
					"         UNION"   +
					"         SELECT fn_aforo, fn_aforo_dl"   +
					"           FROM comcat_producto_nafin"   +
					"          WHERE ic_producto_nafin = ?) x"  ;

			if (tipoAfiliado.equals("P")) {
				params = new ArrayList();
				params.add(new Integer(1));
				params.add(new Integer(claveEpoRelacionada));
				params.add(new Integer(1));
				Registros reg = con.consultarDB(strSQLAforo,params);
				if (reg.next()) {
					mParamsDscto.put("strAforo", reg.getString(1));
					mParamsDscto.put("strAforoDL", reg.getString(2));
				}

				String strSQL =
						" SELECT cs_aceptacion " +
						" FROM comrel_pyme_epo " +
						" WHERE ic_epo = ? AND ic_pyme = ? " ;
				params = new ArrayList();
				params.add(new Integer(claveEpoRelacionada));
				params.add(new Integer(claveAfiliado));

				reg = con.consultarDB(strSQL,params);
				if (reg.next()) {
					mParamsDscto.put("cs_aceptacion", reg.getString("cs_aceptacion"));
				}

				if ("H".equals(mParamsDscto.get("cs_aceptacion")) ) {
					strSQL =
							" SELECT COUNT(*) " +
							" FROM comrel_pyme_epo " +
							" WHERE ic_pyme = ? " ;
					params = new ArrayList();
					params.add(new Integer(claveAfiliado));

					reg = con.consultarDB(strSQL,params);
					if (reg.next()) {
						mParamsDscto.put("bPymeRelacionada", ((Integer.parseInt(reg.getString(1))>1)?"S":"N"));
					}
				}

			} //Fin pyme
			else if (tipoAfiliado.equals("E")) {
				String strSQL =
						" SELECT cs_opera_descuento " +
						" FROM comcat_epo " +
						" WHERE ic_epo = ? ";
				params = new ArrayList();
				params.add(new Integer(claveAfiliado));

				String operaDescuento = "";
				Registros reg = con.consultarDB(strSQL,params);
				if (reg.next()) {
					operaDescuento = reg.getString("cs_opera_descuento");
				}

				strSQL =
					" SELECT cpe.cs_opera_notas_cred, cpe.cs_fecha_venc_pyme"   +
					"   FROM comrel_producto_epo cpe"   +
					"  WHERE cpe.ic_producto_nafin = ?"   +
					"    AND cpe.ic_epo = ?"  ;
				params = new ArrayList();
				params.add(new Integer(1));
				params.add(new Integer(claveAfiliado));

				reg = con.consultarDB(strSQL,params);
				if (reg.next()) {
					String operaNC = reg.getString("cs_opera_notas_cred")==null?"N":reg.getString("cs_opera_notas_cred");
					String operaFVPyme = reg.getString("cs_fecha_venc_pyme")==null?"N":reg.getString("cs_fecha_venc_pyme");

					mParamsDscto.put("operaNC", operaNC);
					mParamsDscto.put("operaFVPyme", operaFVPyme);
				}

				if("S".equals(operaDescuento)){
					params = new ArrayList();
					params.add(new Integer(1));
					params.add(new Integer(claveAfiliado));
					params.add(new Integer(1));
					reg = con.consultarDB(strSQLAforo,params);
					if (reg.next()) {
						mParamsDscto.put("strAforo", reg.getString(1));
						mParamsDscto.put("strAforoDL", reg.getString(2));
					}
				}
			} //fin Epo
			return mParamsDscto;
		} catch(Exception e) {
			throw new AppException("Error al inicializar el modulo de Descuento");
		} finally {
			log.info("getParametrosModuloDescuento(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene los datos para crear campos dinamicos en comrel_visor
	 * @param ic_epo  La clave epo proveniente al seleccionar el combo de Epo
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getCamposAdicionales(String ic_epo) {

		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT ic_no_campo, ic_epo, INITCAP(cg_nombre_campo) AS nombre_campo, cg_tipo_dato, ig_longitud,cg_tipo_objeto, cs_obligatorio " +
							" FROM comrel_visor " +
							" WHERE ic_epo= ? AND ic_no_campo BETWEEN ? AND ? "+
							" AND ic_producto_nafin = ? " +
							" ORDER BY ic_no_campo");
			List params = new ArrayList();
			params.add(new Integer(ic_epo));
			params.add(new Integer(1));
			params.add(new Integer(5));
			params.add(new Integer(1));	//1 = Descuento Electronico
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getCamposAdicionales(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos dinamicos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getCamposAdicionales(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para crear campos dinamicos en comrel_visor_detalle
	 * @param ic_epo  La clave epo proveniente al seleccionar el combo de Epo
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getCamposAdicionalesDetalle(String ic_epo) {

		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT ic_epo, ic_no_campo, cg_nombre_campo, cg_tipo_dato, ig_longitud " +
							" FROM comrel_visor_detalle " +
							" WHERE ic_epo= ? " +
							" AND ic_no_campo BETWEEN ? AND ?" +
							" AND ic_producto_nafin=? ORDER BY ic_no_campo");
			List params = new ArrayList();
			params.add(new Integer(ic_epo));
			params.add(new Integer(1));
			params.add(new Integer(10));
			params.add(new Integer(1));	//1 = Descuento Electronico
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getCamposAdicionales(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos dinamicos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getCamposAdicionales(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para los valores dblPorciento dblPorcientoDL y  colocarlos en variables de session AforoPantalla
	 * @param ic_epo  La clave epo proveniente al seleccionar el combo de Epo
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getTipoAforoPantalla(String ic_epo) {

		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT MIN (x.fn_aforo) / 100, MIN (x.fn_aforo_dl) / 100, '13descuento/index.jsp' origenquery"   +
							" FROM (SELECT fn_aforo, fn_aforo_dl"   +
							" 			FROM comrel_producto_epo"   +
							" 			WHERE ic_producto_nafin = ? AND ic_epo = ?"   +
							" 			UNION"   +
							" 			SELECT fn_aforo, fn_aforo_dl"   +
							" 			FROM comcat_producto_nafin"   +
							" 			WHERE ic_producto_nafin = ?) x");
			List params = new ArrayList();
			params.add(new Integer(1));
			params.add(new Integer(ic_epo));
			params.add(new Integer(1));	//1 = Descuento Electronico
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getTipoAforoPantalla(Error) ::..");
			throw new AppException("Ocurrio un error al obtener query AforoPantalla.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getTipoAforoPantalla(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar informacion detalle documentos
	 * @param ic_docto,ic_epo  La clave epo proveniente al seleccionar el combo de Epo
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getDatosDocumentos(String ic_epo, String ic_docto) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT p.cg_razon_social " +
							"    , p.cg_rfc " +
							"    , do.cg_calle " +
							"    , do.cg_colonia " +
							"    , do.cn_cp " +
							"    , e.cd_nombre AS cg_estado " +
							"    , do.cg_municipio " +
							"    ,d.ig_numero_docto " +
							"    , to_char(d.df_fecha_docto,'dd/mm/yyyy') AS fecha_docto " +
							"    , to_char(d.df_fecha_venc,'dd/mm/yyyy') AS fecha_venc " +
							"    , d.fn_monto " +
							"    , m.cd_nombre AS moneda " +
							"    , d.cg_campo1 " +
							"    , d.cg_campo2 " +
							"    , d.cg_campo3 " +
							"    , d.cg_campo4 " +
							"    , d.cg_campo5 " +
							"    FROM com_documento d " +
							"    , comcat_moneda m " +
							"    , comcat_pyme p  " +
							"    , com_domicilio do " +
							"    , comcat_estado e " +
							"    WHERE d.ic_documento = ? " +
							"    AND d.ic_moneda = m.ic_moneda " +
							"    AND d.ic_pyme = p.ic_pyme " +
							"    AND p.ic_pyme = do.ic_pyme " +
							"    AND do.cs_fiscal = 'S' " +
							"    AND do.ic_estado = e.ic_estado");
			List params = new ArrayList();
			params.add(new Integer(ic_docto));
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getDatosDocumentos(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los datos del documento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getDatosDocumentos(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar informacion detalle documentos
	 * @param ic_docto,ic_epo  La clave epo proveniente al seleccionar el combo de Epo
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getDatosDocumentosDetalle(String ic_docto) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT DD.ic_documento, DD.ic_docto_detalle, DD.cg_campo1, DD.cg_campo2, DD.cg_campo3, " +
                        "DD.cg_campo4, DD.cg_campo5, DD.cg_campo6, DD.cg_campo7, DD.cg_campo8, DD.cg_campo9, " +
                        "DD.cg_campo10, D.ig_numero_docto " +
                        "FROM com_documento_detalle DD, com_documento D " +
                        "WHERE DD.ic_documento = ? "	+
                        "AND D.ic_documento = ? "	+
                        "AND DD.ic_documento = D.ic_documento " +
                        "ORDER BY DD.ic_docto_detalle");
			List params = new ArrayList();
			params.add(new Integer(ic_docto));
			params.add(new Integer(ic_docto));
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getDatosDocumentosDetalle(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los datos detalle del documento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getDatosDocumentosDetalle(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar informacion de acuse en informacion de documentos
	 * @param ccAcuse el numero de acuse proveniente al seleccionar en el grid el acuse
	 * @return tipo de datos Registro.
	 */
	public Registros getDatosAcuse(String ccAcuse) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append(" SELECT I.cg_razon_social AS NOMBRE_IF"+
					"	,E.cg_razon_social AS nombre_epo"+
					"	,D.ig_numero_docto"+
					"	,M.cd_nombre"+
					"	,D.fn_monto"+
					"	,D.fn_porc_anticipo"+
					"	,D.fn_monto_dscto"+
					"	,DS.in_importe_interes"+
					"	,DS.in_importe_recibir"+
					"	, '13PYME13CONSULTA2D' AS pantalla"+
					" FROM com_docto_seleccionado DS"+
					"	,comcat_epo E"+
					"	,comcat_if I"+
					"	,com_documento D"+
					"	,comcat_moneda M"+
					" WHERE DS.cc_acuse = ? "+
					"	AND E.ic_epo = DS.ic_epo"+
					"	AND I.ic_if = DS.ic_if"+
					"	AND D.ic_documento = DS.ic_documento"+
					"	AND M.ic_moneda = D.ic_moneda"+
					"	AND D.FN_MONTO_DSCTO IS NOT NULL ");
			List params = new ArrayList();
			params.add(ccAcuse);
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getDatosAcuse(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los datos detalle del documento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getDatosAcuse(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar informacion de acuse en informacion de documentos
	 * @param ccAcuse el numero de acuse proveniente al seleccionar en el grid el acuse
	 * @return tipo de datos Registro.
	 */
	public Registros getDatosTotalesAcuse(String ccAcuse) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append(" SELECT E.cg_razon_social AS nombre_epo"+
				"	,M.ic_moneda"+
				"	,M.cd_nombre"+
				"	,sum(D.fn_monto) AS total_monto"+
				"	,sum(D.fn_monto_dscto) AS total_monto_dscto"+
				"	,sum(DS.in_importe_interes) AS total_interes"+
				"	,sum(DS.in_importe_recibir) AS total_recibir"+
				"	,'13PYME13CONSULTA2D' AS pantalla"+
				" FROM com_docto_seleccionado DS"+
				"	,comcat_epo E"+
				"	,com_documento D"+
				"	,comcat_moneda M"+
				" WHERE DS.cc_acuse = ? "+
				"	AND E.ic_epo = DS.ic_epo"+
				"	AND D.ic_documento = DS.ic_documento"+
				"	AND M.ic_moneda = D.ic_moneda"+
				"	AND D.FN_MONTO_DSCTO IS NOT NULL "+
				" GROUP BY E.cg_razon_social,M.ic_moneda,M.cd_nombre"+
				" ORDER BY M.ic_moneda ");
			List params = new ArrayList();
			params.add(ccAcuse);
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getDatosTotalesAcuse(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los datos detalle del documento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getDatosTotalesAcuse(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar informacion de acuse en informacion de documentos
	 * @param ccAcuse el numero de acuse proveniente al seleccionar en el grid el acuse
	 * @return tipo de datos Registro.
	 */
	public Registros getDatosAcuse2(String ccAcuse) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append(" SELECT TO_CHAR(df_fecha_hora,'dd/mm/yyyy') AS fecha_carga"+
					"	,TO_CHAR(df_fecha_hora,'hh:mi') AS hora_carga"+
					"	,A.ic_usuario "+
					" FROM com_acuse2 A"+
					" WHERE A.cc_acuse = ? ");
			List params = new ArrayList();
			params.add(ccAcuse);
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getDatosAcuse2(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los datos detalle del documento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getDatosAcuse2(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para mostrar un pop-pup en informacion de documentos (modificacion de montos y/o fechas)
	 * @param sFechaVencPyme y el numero de documentos proveniente al seleccionar en el grid (modificacion de montos y/o fechas)
	 * @return tipo de datos Registro.
	 */
	public Registros getModificacionMontos(String sFechaVencPyme, String ic_docto) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;
		String aux	=	"";

		try {
			con.conexionDB();
			query = new StringBuffer();
			if(!"".equals(sFechaVencPyme) && sFechaVencPyme != null) { aux="p_";}
			query.append(" SELECT to_char(C.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_cambio, "
                           +  " fn_monto_nuevo, fn_monto_anterior, ct_cambio_motivo, "
                           +  " C.cc_acuse, TO_CHAR(C.df_fecha_venc_"+aux+"nueva,'dd/mm/yyyy') AS fec_venc_nueva, "
									+  " TO_CHAR(C.df_fecha_venc_"+aux+"anterior,'dd/mm/yyyy') AS fec_venc_anterior "
                           +  " FROM com_documento D, comhis_cambio_estatus C "
                           +  " WHERE D.ic_documento=?"
                           +  " AND D.ic_documento=C.ic_documento AND C.ic_cambio_estatus IN (8,28) "
                           +  " ORDER BY C.dc_fecha_cambio ");
			List params = new ArrayList();
			params.add(ic_docto);
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;

		} catch (Exception e) {
			log.error("getModificacionMontos(Error) ::..");
			throw new AppException("Ocurrio un error al obtener la modificacion de montos y/o Fechas.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getModificacionMontos(S) ::..");
		}
	}

	/**
	 *
	 * Modifica los parametros de descuento automatico configurados para la pyme en cuestion.
	 *
    * @param autorizacionDsctoAutomatico indica si se autorizo el descuento automatico <tt>"S"</tt> o no <tt>"N"</tt>.
    * @param clavePyme clave de la pyme a la que se le modifica su parametrizacion.
    * @param claveEpo clave de la EPO.
    * @param iNoUsuario login del usuario.
    * @param if_cta <tt>String</tt> array con la clave y cuentas de los intermediarios seleccionados.
    * @param hidHuboCambio Indica con <tt>"S"</tt> si hubo cambio en la seleccion del intermediario e indica con <tt>"N"</tt> que no hubo.
    * @param hidHuboCambioDia Indica con <tt>"S"</tt> si hubo cambio en el combo de modalidad e indica con <tt>"N"</tt> que no hubo.
    * @param hidDsctoAutomaticoDia <tt>String</tt> array con la clave de la modalidad seleccionada.
    * @param hidIcEpo clave de la EPO involucrada en la parametrizacion.
    * @param hidIcMoneda clave de la Moneda.
	 *
	 * @return <tt>HashMap</tt> con el resultado de la operacion.
	 *
	 */
	public HashMap setParametrosDsctoAutomatico(
			String 	autorizacionDsctoAutomatico,
			String 	clavePyme,
			String 	claveEpo,
			String 	iNoUsuario,
			String[] if_cta,
			String[] hidHuboCambio,
			String[] hidHuboCambioDia,
			String[] hidDsctoAutomaticoDia,
			String[] hidIcEpo,
			String[] hidIcMoneda
		){

		log.info("setParametrosDsctoAutomatico(E)");

		AccesoDB con 				= new AccesoDB();
		HashMap  respuesta		= new HashMap();

		Acuse2 	acuse 			= null;

		boolean	ok					= true;
		boolean 	ejecutaCommit 	= false;

		// NOTA: Cuando se haga la parte de usuarios nafin descomentar los siguiente
		// (ver tambien nafin-web/13descuento/15dsctoaut.jspf):
		/*
 		if (strTipoUsuario.equals("NAFIN")) {
			acuse = autenticarUsuarioNafin(request, iNoUsuario);
			qrySentenciaAcuse =
					" insert into com_acuse2 (CC_ACUSE, " +
					" DF_FECHA_HORA, IC_USUARIO, " +
					" CG_RECIBO_ELECTRONICO, IC_PRODUCTO_NAFIN) "+
					" VALUES ('" + acuse.getAcuse() + "'," +
					" TO_DATE('"+ acuse.getFechaHora() + "','dd/mm/yyyy hh24:mi:ss'), " +
					" '" + acuse.getClaveUsuario() + "'," +
					" '" + acuse.getReciboElectronico() + "'," +
					" 1)";
			con.ejecutaSQL(qrySentenciaAcuse);
		}
		*/

		String mensaje 		= "Los datos han sido actualizados";
		String strAcuse 		= (acuse == null)?"null":"'" + acuse.getAcuse() + "'";
		String qrySentencia	= null;

		if("N".equals(autorizacionDsctoAutomatico)){

			try{

				con.conexionDB();

				qrySentencia =	" update comcat_pyme "+
									" set cs_dscto_automatico = 'N', cs_dscto_auto_proc = 'N' "+
									" where ic_pyme = "+clavePyme;
				con.ejecutaSQL(qrySentencia);


				qrySentencia =	" insert into bit_dscto_automatico"+
									"(ic_usuario,ic_cuenta_bancaria,ic_if,ic_epo,df_registro,cs_autorizo, cc_acuse)"+
									" 		select '"+iNoUsuario+"',pi.ic_cuenta_bancaria,pi.ic_if,pi.ic_epo,sysdate," +
									" 'N', " + strAcuse +
									"		from comrel_pyme_if pi,comrel_cuenta_bancaria cb "+
									" 		where cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"+
									"			and cb.ic_pyme = "+clavePyme+
									"			and pi.cs_borrado = 'N'"+
									"			and cb.cs_borrado = 'N'"+
									"			and pi.cs_dscto_automatico = 'S'";
				con.ejecutaSQL(qrySentencia);

				qrySentencia =	" update comrel_pyme_if"+
									" set cs_dscto_automatico = 'N'"+
									" where ic_cuenta_bancaria in("+
									"		select ic_cuenta_bancaria"+
									"		from comrel_cuenta_bancaria"+
									"		where ic_pyme = "+clavePyme+
									"		and cs_borrado = 'N')"+
									" and cs_borrado = 'N'"+
									" and cs_dscto_automatico = 'S'";
				con.ejecutaSQL(qrySentencia);

			} catch(Exception e) {

				e.printStackTrace();
				mensaje 	= "Error al actualizar los datos. Intente mas tarde";
				ok 		= false;

			}finally{

				con.terminaTransaccion(ok);

				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}

			}

		} else if("S".equals(autorizacionDsctoAutomatico)){

			String Epo 					= "";
			String EpoAnt 				= "";

			String hidIcIf				= "";
			String hidCtaBancaria 	= "";

			try {

				con.conexionDB();

				for(int i=0;i<if_cta.length;i++){

					VectorTokenizer 	vt 		= new VectorTokenizer(if_cta[i],"|");
					Vector 				vecdat	= vt.getValuesVector();

					if("".equals(if_cta[i])){

						hidIcIf				= "";
						hidCtaBancaria 	= "";

					}else{

						hidIcIf				= (String)vecdat.get(0);
						hidCtaBancaria 	= (String)vecdat.get(1);

					}

					if( "S".equals(hidHuboCambio[i]) || "S".equals(hidHuboCambioDia[i]) ){

						ejecutaCommit = true;

						try{

							if("S".equals(hidHuboCambio[i])){

								Epo 		= hidIcEpo[i];

								if(!Epo.equals(EpoAnt)) {

									// Se inserta en Bitacora por EPO.
									qrySentencia =	" insert into bit_dscto_automatico"+
														"(ic_usuario,ic_cuenta_bancaria,ic_if,ic_epo,df_registro,cs_autorizo, cc_acuse)"+
														" 		select '"+iNoUsuario+"',pi.ic_cuenta_bancaria,pi.ic_if,pi.ic_epo,sysdate," +
														" 'N'," + strAcuse +
														"		from comrel_pyme_if pi,comrel_cuenta_bancaria cb "+
														" 		where cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"+
														"			and cb.ic_pyme = "+clavePyme+
														"			and pi.cs_borrado = 'N'"+
														"			and cb.cs_borrado = 'N'"+
														"			and pi.cs_dscto_automatico = 'S'"+
														"			and pi.ic_epo = "+hidIcEpo[i];
									con.ejecutaSQL(qrySentencia);
								}

								EpoAnt = Epo;
								qrySentencia =	" update comrel_pyme_if"+
													" set cs_dscto_automatico = 'N'"+
													" where ic_cuenta_bancaria in("+
													"		select ic_cuenta_bancaria"+
													"		from comrel_cuenta_bancaria"+
													"		where ic_pyme = "+clavePyme+
													"		and cs_borrado = 'N' "+
													" 		and ic_moneda = "+hidIcMoneda[i]+")"+
													" and cs_borrado = 'N'"+
													" and ic_epo="+hidIcEpo[i]+
													" and cs_dscto_automatico = 'S'";
								con.ejecutaSQL(qrySentencia);

								qrySentencia =	" update comcat_pyme"+
													" set cs_dscto_automatico = 'S', cs_dscto_auto_proc = 'N'"+
													" where ic_pyme ="+clavePyme;
								con.ejecutaSQL(qrySentencia);

								if(!"".equals(hidIcIf)){

									qrySentencia =	" update comrel_pyme_if"+
														" set cs_dscto_automatico = 'S'"+
														"	where ic_cuenta_bancaria ="+hidCtaBancaria+
														"	and ic_if = "+hidIcIf+
														"	and ic_epo = "+hidIcEpo[i]+
														"	and cs_borrado = 'N'";
									con.ejecutaSQL(qrySentencia);

									qrySentencia =	" insert into bit_dscto_automatico"+
														"(ic_usuario,ic_cuenta_bancaria,ic_if,ic_epo,df_registro,cs_autorizo, cc_acuse)"+
														" values('"+iNoUsuario+"',"+hidCtaBancaria+","+hidIcIf+","+hidIcEpo[i]+
														",sysdate,'S'," +strAcuse + ")";
									con.ejecutaSQL(qrySentencia);
								}

							}	//if(S.equals(hidHuboCambio))

							if("S".equals(hidHuboCambioDia[i])){

								qrySentencia =	" update comrel_pyme_if"+
													" set cs_dscto_automatico_dia = '"+hidDsctoAutomaticoDia[i]+"'"+
													"	where ic_cuenta_bancaria ="+hidCtaBancaria+
													"	and ic_if = "+hidIcIf+
													"	and ic_epo = "+hidIcEpo[i]+
													"	and cs_dscto_automatico = 'S'"+
													"	and cs_borrado = 'N'";
								con.ejecutaSQL(qrySentencia);

							}

						}catch(Exception e){

							System.out.println("\n"+qrySentencia+"\n");// Debug info
							e.printStackTrace();
							ok = false;
							throw e;

						}

					} //if

				} //for

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				if (!ok) {
					mensaje = "Error al actualizar los datos. Intente mas tarde";
				} else {
					mensaje = "Los datos han sido actualizados";
				}

				if(ok && ejecutaCommit ) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}

				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}

			}

			if(!ejecutaCommit){
				mensaje = "No se realiz� ning�n cambio";
				//No hubo cambios -> no genera acuse!...
				//response.sendRedirect("15dsctoaut.jsp");
			}

		}

		respuesta.put("success", new Boolean(ok));
      respuesta.put("msg",     mensaje );

      log.info("setParametrosDsctoAutomatico(S)");

      return respuesta;

	}

	/**
	 * Determina si la epo a la que pertenece la pyme tiene parametrizado
	 * un contrato y esta activo
	 * @param claveEpo Clave de la epo
	 */
	public boolean contratoParametrizado(String claveEpo) {
		boolean contratoParametrizado = false;

		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();

			String strSQL =
					" SELECT count(*) as numContratos " +
					" FROM com_contrato_epo " +
					" WHERE cs_mostrar = 'S' " +
					" AND ic_epo = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEpo));

			ResultSet rs = ps.executeQuery();
			rs.next();
			contratoParametrizado = (rs.getInt("numContratos") > 0)?true:false;
			rs.close();
			ps.close();
			return contratoParametrizado;
		} catch(Exception e) {
			throw new AppException("Error al determinar si hay contrato parametrizado", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Determina si la pyme ha aceptado o no el ultimo contrato especifico de la EPO
	 * @param clavePyme Clave de la pyme
	 * @param claveEpo Clave de la epo
	 */
	public boolean contratoAceptado(String clavePyme, String claveEpo) {
		boolean contratoAceptado = false;

		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();

			//--determina si tiene autorizado el ultimo contrato parametrizado por la epo.
			String strSQL =
					" SELECT COUNT(*) as contratoAceptado " +
					" FROM com_aceptacion_contrato " +
					" WHERE ic_epo = ? "+
					" AND ic_consecutivo = ( " +
					" 		SELECT MAX(ic_consecutivo) " +
					" 		FROM com_contrato_epo ce " +
					" 		WHERE ce.ic_epo = ?  ) " +
					" AND ic_pyme = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEpo));
			ps.setInt(2, Integer.parseInt(claveEpo));
			ps.setInt(3, Integer.parseInt(clavePyme));

			ResultSet rs = ps.executeQuery();
			rs.next();
			contratoAceptado = (rs.getInt("contratoAceptado") > 0)?true:false;

			return contratoAceptado;
		} catch(Exception e) {
			throw new AppException("Error al determinar si tiene aceptacion el",e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////////
	/**
	 * Determina si la IF a la que pertenece la pyme tiene parametrizado
	 * un contrato y esta activo
	 * @param clavePyme Clave de la pyme
	 */
	public boolean contratoParametrizadoIF(String clavePyme) {
		boolean contratoParametrizadoIF = false;
	  List clavesIF = new ArrayList();
	  int contratos = 0;

		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();

		 String strSQL = " SELECT DISTINCT cpi.ic_if"+
							  " FROM comrel_pyme_if cpi,"+
							  "      comrel_cuenta_bancaria ccb"+
							  " WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
							  "   AND cpi.cs_vobo_if = ?"+
							  "   AND cpi.cs_borrado = ?"+
							  "   AND ccb.cs_borrado = ?   "+
							  "   AND ccb.ic_pyme = ?";

		 PreparedStatement ps = con.queryPrecompilado(strSQL);
		 ps.setString(1, "S");
		 ps.setString(2, "N");
		 ps.setString(3, "N");
		 ps.setInt(4, Integer.parseInt(clavePyme));

		 ResultSet rs = ps.executeQuery();

		 while(rs.next()){
			clavesIF.add(rs.getString(1));
		 }

		 Iterator itClaveIf = clavesIF.iterator();

		 while(itClaveIf.hasNext()){
			String claveIF = (String)itClaveIf.next();

			strSQL =
					" SELECT count(*) as numContratos " +
					" FROM com_contrato_if " +
					" WHERE cs_mostrar = 'S' " +
					" AND ic_if = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveIF));

			rs = ps.executeQuery();
			rs.next();
			contratos += rs.getInt("numContratos");
		 }

		 contratoParametrizadoIF = ( contratos > 0)?true:false;
		 System.out.println("..:: HAY "+contratos+" CONTRATOS PARAMETRIZADOS PARA LA PYME "+clavePyme);
			rs.close();
			ps.close();
			return contratoParametrizadoIF;
		} catch(Exception e) {
			throw new AppException("Error al determinar si existen contratos IF");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Determina si la pyme ha aceptado o no el ultimo contrato especifico de la IF
	 * @param clavePyme Clave de la pyme
	 */
	public boolean contratoAceptadoIF(String clavePyme) {
		boolean contratoAceptado = true;
		List clavesIF = new ArrayList();
	  int contratoP = 0;
	  int contratoA = 0;

		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
	//-->Determina las claves de IF que tienen relacion con la pyme
		 String strSQL = " SELECT DISTINCT cpi.ic_if"+
							  " FROM comrel_pyme_if cpi,"+
							  "      comrel_cuenta_bancaria ccb"+
							  " WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
							  "   AND cpi.cs_vobo_if = ?"+
							  "   AND cpi.cs_borrado = ?"+
							  "   AND ccb.cs_borrado = ?   "+
							  "   AND ccb.ic_pyme = ?";

		 PreparedStatement ps = con.queryPrecompilado(strSQL);
		 ps.setString(1, "S");
		 ps.setString(2, "N");
		 ps.setString(3, "N");
		 ps.setInt(4, Integer.parseInt(clavePyme));

		 ResultSet rs = ps.executeQuery();

		 while(rs.next()){
			clavesIF.add(rs.getString(1));
		 }

		 if(clavesIF.size() > 0){
			Iterator itClaveIf = clavesIF.iterator();

			while(itClaveIf.hasNext()){
			  String claveIF = (String)itClaveIf.next();
	//-->Determina si la IF tiene un contrato parametrizado
			  strSQL =
				 " SELECT count(*) as numContratos " +
				 " FROM com_contrato_if " +
				 " WHERE cs_mostrar = 'S' " +
				 " AND ic_if = ? ";

			  ps = con.queryPrecompilado(strSQL);
			  ps.setInt(1, Integer.parseInt(claveIF));

			  rs = ps.executeQuery();

			  rs.next();
			  contratoP = rs.getInt("numContratos");

			  if(contratoP == 1){
				 strSQL =
					" SELECT count(*) as contratoAceptado"+
					" FROM com_aceptacion_contrato_if"+
					" WHERE ic_if = ?"+
					" AND ic_consecutivo = ("+
					"        SELECT MAX(ic_consecutivo)"+
					"        FROM com_contrato_if"+
					"        WHERE ic_if = ?"+
					" ) "+
					" AND ic_pyme = ?";

				 ps = con.queryPrecompilado(strSQL);
				 ps.setInt(1, Integer.parseInt(claveIF));
				 ps.setInt(2, Integer.parseInt(claveIF));
				 ps.setInt(3, Integer.parseInt(clavePyme));

				 rs = ps.executeQuery();

				 rs.next();
				 contratoA = rs.getInt("contratoAceptado");

				 if(contratoA == 0){
					contratoAceptado = false;
					System.out.println("..:: HAY UN CONTRATO PARAMETRIZADO SIN ACEPTAR DE LA PYME "+clavePyme+" CON LA IF "+claveIF);
					break;
				 }
			  }
			}
		 }
			return contratoAceptado;
		} catch(Exception e) {
			throw new AppException("Error al determinar si hay contrato IF aceptado",e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


/**
 * FODEA 032 - 2008 DESCUENTO ELECTRONICO - PROMOCION A PYMES
 * Determina si la PYME ha autorizado el env�o de promociones de los productos de N@E
 * en su cuenta de email y/o en su telefono celular. En caso de que la PYME a�n no confirme
 * indique si desea o no recibir informaci�n sobre los productos de N@E se presenta la pantalla
 * correspondiente para que confirme su decisi�n.
 * @author Alberto Cruz Flores
 * @param
 */
	public boolean aceptaPromocion(String ic_pyme, String iNoEPO) {
		boolean recibePromocion = false;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();

			String strSQL = " SELECT ic_banco_fondeo FROM comcat_epo WHERE ic_epo = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(iNoEPO));

			ResultSet rs = ps.executeQuery();

			while(rs.next()){
				String bancoFondeo = rs.getString(1);

				strSQL = " SELECT cs_factoraje_movil"+
								 " FROM com_promo_pyme"+
								 " WHERE ic_pyme = ?"+
								 " AND ic_banco_fondeo = ?";

				PreparedStatement pst = con.queryPrecompilado(strSQL);
				pst.setLong(1, Long.parseLong(ic_pyme));
				pst.setInt(2, Integer.parseInt(bancoFondeo));

				ResultSet rst = pst.executeQuery();

				String factoraje = "";

				while(rst.next()){
					factoraje = rst.getString(1) == null?"":rst.getString(1);
				}

				if(!factoraje.equals("") && (factoraje.equals("S") || factoraje.equals("N"))){
					recibePromocion = true;
				}else{
					recibePromocion = false;
				}

				rst.close();
				pst.close();
			}

			rs.close();
			ps.close();

			return recibePromocion;
		} catch(Exception e) {
			throw new AppException("Error al obtener la configuracion de la recepcion de promociones", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Determina si hay cuentas bancarias pendientes de autorizar a la pyme, por
	 * parte de algun IF.
	 * @param clavePyme Clave de la pyme
	 * @return true Si hay cuentas pendientes o false de lo contrario
	 */
	public boolean hayCuentasPendientesPyme(String clavePyme) {
		boolean hayCuentasPendientes = false;
		AccesoDB con = new AccesoDB();
		int iCuentasPendientes = 0;

		try {
			con.conexionDB();

			  /*Obtener datos para saber las cuentas pendientes que la pyme espera a que le autorizen*/
			String verifCuentasPendPYME =
					" SELECT COUNT(*) total_cuentas_pendientes_pyme " +
					" FROM comcat_pyme pyme, comhis_cambios_cta camb_cta " +
					" WHERE camb_cta.ic_pyme = pyme.ic_pyme " +
					" AND camb_cta.ic_pyme = ? " +
					" AND camb_cta.cs_autoriza_if = ? ";
			List params = new ArrayList();
			params.add(clavePyme);
			params.add("N");
			Registros registros = con.consultarDB(verifCuentasPendPYME, params, false);
			if(registros.next()) {
					iCuentasPendientes = Integer.parseInt(registros.getString("TOTAL_CUENTAS_PENDIENTES_PYME"));
					if(iCuentasPendientes > 0){
						hayCuentasPendientes = true;
					}
			}
			return hayCuentasPendientes;
		} catch(Exception e) {
			throw new AppException("Error al obtener la cuentas pendientes PyME", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene los mensajes adicionales para mostrar.
	 * @param claveAfiliado Clave del afiliado
	 * @param tipoAfiliado Tipo de afiliado: PYME, EPO, etc.
	 * @param claveEpoRelacionada Clave de la epo relacionada. Aplica cuando
	 * 		tipoAfiliado = PYME
	 * @return Lista de mapas que contienen CG_TITULO y CG_CONTENIDO
	 */
	public List getMensajesInicialesAdicionales(String claveAfiliado,
			String tipoAfiliado, String claveEpoRelacionada) {
		log.info("getMensajesInicialesAdicionales(E)");
		int iClaveAfiliado = 0;
		int iClaveEpoRelacionada = 0;
		// ---------------- Validacion de Parametros ------------------------
		try {
			if (claveAfiliado == null || claveAfiliado.equals("")) {
				throw new Exception("La clave de afiliado es requerida");
			}
			iClaveAfiliado = Integer.parseInt(claveAfiliado);
			if (tipoAfiliado == null || tipoAfiliado.equals("")) {
				throw new Exception("El tipo de afiliado es requerido");
			}
			if (claveEpoRelacionada != null && !claveEpoRelacionada.equals("")) {
				iClaveEpoRelacionada = Integer.parseInt(claveEpoRelacionada);
			}
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		// ------------------------------------------------------------------

		try {
			List mensajes = new ArrayList();

			if (tipoAfiliado.equals("PYME")) {
				if (this.hayCuentasPendientesPyme(claveAfiliado)) {
					Map mensaje = new HashMap();
					mensaje.put("CG_TITULO", "Aviso");
					mensaje.put("CG_CONTENIDO", "Su solicitud de cambio de cuenta bancaria se encuentra en proceso de autorizaci�n por el (los) Intermediario(s) Financiero(s) ");
					mensaje.put("CS_SESION", "N");
					mensajes.add(mensaje);
				}

				Map param = this.getParametrosModuloDescuento(claveAfiliado,
						"P", claveEpoRelacionada);
				String aceptacionDescuento = (String)param.get("cs_aceptacion");
				if ("R".equals(aceptacionDescuento)) {
					Map mensaje = new HashMap();
					mensaje.put("CG_TITULO", "Aviso");
					mensaje.put("CG_CONTENIDO",
							"Usted ya realiz� el tr�mite de formalizaci�n para efectuar operaciones electr�nicas,<br>" +
							"para habilitar su clave, por favor comun�quese al <span style=\"font-weight: bold;\">Centro de Atenci�n a Clientes</span><br>" +
							"al tel�fono 50-89-61-07 � del interior al tel�fono <span style=\"font-weight: bold;\">01-800-NAFINSA (01-800-6234672)</span>.<br>"
					);
					mensaje.put("CS_SESION", "N");
					mensajes.add(mensaje);
				}

				ISeleccionDocumento seleccionDocto = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

				if(seleccionDocto.hayOfertaDeTasas(claveAfiliado, "1", claveEpoRelacionada) || seleccionDocto.hayOfertaDeTasas(claveAfiliado, "54", claveEpoRelacionada)	){
					String nombreEpo = this.getNombreEPO(claveEpoRelacionada);
					Map mensaje = new HashMap();
					mensaje.put("CG_TITULO", "Oferta de Tasas");
					mensaje.put("CG_CONTENIDO",
							"<span style=\"font-weight: bold;\">Estimado PROVEEDOR</span>,<br>" +
							"Actualmente podr� descontar sus documentos con una tasa especial en beneficio de su econom�a.<br>" +
							"Esto consiste en seleccionar uno o m�s documentos que sumen el m�nimo autorizado por la Banca  para acceder al descuento inmediato con la siguiente Cadena: "+
							"<span style=\"font-weight: bold;\">"+nombreEpo+"</span>.<br><br>"+
							"Para conocer el detalle de su operaci�n, deber� entrar al m�dulo de Nafinet / Descuento Electr�nico/ Capturas/ Selecc. Doctos en Oferta de Tasa.<br>"+
							"<span style=\"font-weight: bold;\">Nacional Financiera, S.N.C.</span>"
					);
					mensaje.put("CS_SESION", "S");
					mensajes.add(mensaje);
				}

				String qrySentencia = "SELECT cmd.ic_pyme, cmd.ic_epo, cmd.ic_if, " +
											" cmd.ic_descontante, ci.cg_razon_social, cmd.cs_dscto_aut_irrev " +
											"  FROM com_solic_mand_doc cmd, comcat_if ci " +
											" WHERE cmd.ic_descontante = ci.ic_if " +
											" AND cmd.ic_estatus_man_doc = ? " +
											" AND cmd.ic_epo = ? AND cmd.ic_pyme = ? "+
											" AND rownum = 1 ";

				List params = new ArrayList();
				params.add(new Integer(2));
				params.add(new Integer(iClaveEpoRelacionada));
				params.add(new Integer(iClaveAfiliado));
				AccesoDB con = new AccesoDB();
				try {
					con.conexionDB();
					Registros reg = con.consultarDB(qrySentencia, params);
					if(reg.next()) {
						Map mensaje = new HashMap();
						mensaje.put("CG_TITULO", "Aviso Instrucci�n Irrevocable");
						mensaje.put("CG_CONTENIDO",
								"ESTA OPERANDO INSTRUCCI&Oacute;N IRREVOCABLE CON EL INTERMEDIARIO<br> " +
								" FINANCIERO " + reg.getString("cg_razon_social") + " <br> " +
								" Y TIENE DESCUENTO AUTOM&Aacute;TICO " +
								("N".equals(reg.getString("cs_dscto_aut_irrev"))?"DESACTIVADO":"ACTIVADO")
						);
						mensaje.put("CS_SESION", "S");
						mensajes.add(mensaje);
					}
				} catch(Exception e) {
					throw new AppException("Error al generar el Aviso de Instruccion Irrevocable", e);
				} finally {
					if (con.hayConexionAbierta()) {
						con.cierraConexionDB();
					}
				}

			}
			return mensajes;
		} catch (Exception e) {
			throw new AppException("Error al obtener los mensajes iniciales adicionales", e);
		} finally {
			log.info("getMensajesInicialesAdicionales(S)");
		}
	}
	/**
	 * Actualiza los Puntos Rebate de seg�n la EPO (IC_EPO) e IF (IC_IF) correspondiente y
	 * en donde el tipo de producto sea Descuento Electronico (1).
	 * @param clavePyme Clave de la pyme
	 * @param claveEPO Clave de la EPO
	 * @param claveIF Clave de la IF
	*/
	public boolean actualizaPuntosRebate(String claveEPO, String numPuntosRebate[]){
		log.info("actualizaPuntosRebate(E)");
		AccesoDB			con				= new AccesoDB();
		List				condiciones		= null;
							condiciones		= new ArrayList();
		boolean			hayExito			= true;
		try{
			con.conexionDB();
			StringTokenizer puntosRebateIF;
			int numElementos = numPuntosRebate.length;
			for(int i=0; i<numElementos;i++){
				StringBuffer qrySentencia	= new StringBuffer();
				String ic_if = "";
				String puntosRebate = "";

				puntosRebateIF = new StringTokenizer(numPuntosRebate[i],"|");

				if(puntosRebateIF.hasMoreTokens()){
					puntosRebate = puntosRebateIF.nextToken().toString();
					ic_if = puntosRebateIF.nextToken().toString();
				}
				qrySentencia.append("UPDATE comrel_if_epo_x_producto SET fg_puntos_rebate =");
				qrySentencia.append(puntosRebate);
				qrySentencia.append(" WHERE ic_producto_nafin = 1 AND ic_if =");
				qrySentencia.append(ic_if);
				qrySentencia.append(" AND ic_epo =");
				qrySentencia.append(claveEPO);
				try{
					hayExito=true;
					con.ejecutaSQL(qrySentencia.toString());
					log.debug("qrySentencia= " + qrySentencia.toString());
				}catch(SQLException errorSQL){
					hayExito = false;
					throw new AppException("Error al actualizar los datos");
				}
				con.terminaTransaccion(hayExito);
			}
		}catch(Exception e){
			log.error("actualizaPuntosRebate(Error)",e);
			hayExito = false;
			throw new AppException("Error al actualizar los datos");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(hayExito);
				con.cierraConexionDB();
			}
			log.info("actualizaRespuestaCredito(S)");
		}
		return hayExito;
	}


	/**
	 * Este metodo devuelve el nombre de la EPO cuya clave se especifica en claveEPO
	 *
	 * @since 	F031-2011
	 * @param 	claveEPO Clave de la EPO.
	 * @return 	String con el nombre de la EPO.
	 *
	 */
	public String getNombreEPO(String claveEPO)
		throws AppException {

		log.info("ParametrosDescuentoBean::getNombreEPO(E)");
		String nombreEPO = "";

		if(claveEPO == null || claveEPO.equals("")){
			log.info("ParametrosDescuentoBean::getNombreEPO() - claveEPO vacia");
			return nombreEPO;
		}

		// Realizar consulta
		AccesoDB con 				= new AccesoDB();
		PreparedStatement ps 	= null;
		ResultSet rs 				= null;
		String qrySentencia		= null;

		try {
				con.conexionDB();
				qrySentencia =
						"SELECT " +
						"	CG_RAZON_SOCIAL AS NOMBRE_EPO " +
						"FROM " +
						"	COMCAT_EPO " +
						"WHERE " +
						"	IC_EPO           = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setLong(1, Long.parseLong(claveEPO));
				rs = ps.executeQuery();

				if(rs != null && rs.next()){
					nombreEPO = rs.getString("NOMBRE_EPO");
				}

				rs.close();
				ps.close();

			return nombreEPO;
		}catch(Throwable e){
			log.info("ParametrosDescuentoBean::getNombreEPO(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener el nombre la EPO a partir de su clave", e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getNombreEPO(S)");
		}

	}
	/**
	 * Este m�todo actualiza porcentaje de descuento en M.N y d�lares, d�as l�mite para notificaci�n, d�as m�nimos y m�ximos
	 * para descuento autom�tico y cesi�n de derechos
	 *
	 * @since 	Migraci�n EPO 2012
	 * @param 	porcentajeM Porcentaje de descuento M.N.
	 * @param 	porcentajeD Porcentaje de descuento en d�lares.
	 * @param 	operaFactVenc Opera Factoraje Vencido.
	 * @param 	operaFactDist Opera Factoraje Distribuido.
	 * @param 	diasMin Clave D�as m�nimos para descuento.
	 * @param 	diasMax Clave D�as m�ximos para descuento.
	 * @param 	diasNotif D�as l�mite para Notificaci�n (Cesi�n de derechos).
	 * @param 	diasNotifReact D�as l�mite para Reactivaci�n (Cesi�n de derechos).
	 * @param 	sDesAutoFactVenci Opera descuento autom�tico (Factoraje vencido).
	 * @return 	HashMap Indicadores de actualizaci�n de par�metros.
	 *
	 */
	public HashMap actualizaOtrosParametros(String porcentajeM, String porcentajeD, String operaFactVenc, String operaFactDist, String diasMin, String diasMax, String diasNotif, String diasNotifReact, String claveEpo, String sDesAutoFactVenci, String operaDescAutoEPO ){
		AccesoDB con		= new AccesoDB();
		StringBuffer strSQL;
		HashMap map			= null;
		String bandera = "0";
		ResultSet rsSQL;
		StringBuffer qryPorciento;
		StringBuffer queryNafinDescuento;
		StringBuffer qryNafin;
		String strPorciento = "";
		String strPorcientoDL = "";
		String strAforo = "";
		String strAforoDL = "";
		boolean bOperaFactorajeVencido = false;
		try{
			map = new HashMap();
			con.conexionDB();
			strSQL = new StringBuffer();
			//Verifica si la EPO acepta "Factoraje Vencido"
			strSQL.append(" SELECT COUNT (*)");
			strSQL.append("   FROM comrel_producto_epo");
			strSQL.append("  WHERE cs_factoraje_vencido = 'S'");
			strSQL.append("    AND ic_producto_nafin = 1");
			strSQL.append("    AND ic_epo = ");
			strSQL.append(claveEpo);

			rsSQL = con.queryDB(strSQL.toString());
			bOperaFactorajeVencido = false;
			if (rsSQL.next()){
				if(rsSQL.getInt(1)>0)
					bOperaFactorajeVencido = true;
			}
			rsSQL.close();
			con.cierraStatement();
			log.info("strSQL = " + strSQL.toString());

			qryPorciento = new StringBuffer();
			qryPorciento.append(" SELECT fn_aforo, fn_aforo_dl");
			qryPorciento.append("   FROM comcat_producto_nafin");
			qryPorciento.append("  WHERE ic_producto_nafin = 1");

			ResultSet rs = con.queryDB(qryPorciento.toString());
			log.info("qryPorciento = " + qryPorciento.toString());
			if(rs.next()){
				strPorciento = (rs.getString("fn_aforo")!=null)?rs.getString("fn_aforo"):"";
				strPorcientoDL = (rs.getString("fn_aforo_dl")!=null)?rs.getString("fn_aforo_dl"):"";
			}
			rs.close();
			con.cierraStatement();

			Float fltPorcientoNafin = new Float(strPorciento);
			Float fltPorcientoNafinDL = new Float(strPorcientoDL);

			Float fltPorcientoNuevo = new Float(porcentajeM);
			Float fltPorcientoNuevoDL = new Float(porcentajeD);

			if (fltPorcientoNuevoDL.doubleValue() > fltPorcientoNafinDL.doubleValue())
				bandera = "5";
			if(!bandera.equals("5")){
				if(fltPorcientoNuevo.doubleValue() <= fltPorcientoNafin.doubleValue()){
					queryNafinDescuento = new StringBuffer();
					queryNafinDescuento.append(" SELECT COUNT (1)");
					queryNafinDescuento.append("   FROM comrel_producto_epo");
					queryNafinDescuento.append("  WHERE ic_producto_nafin = 1");
					queryNafinDescuento.append("    AND ic_epo = ");
					queryNafinDescuento.append(claveEpo);

					ResultSet rsExisteDesc = con.queryDB(queryNafinDescuento.toString());
					log.info("queryNafinDescuento = " + queryNafinDescuento.toString());

					int existeDesc = 0;
					if(rsExisteDesc.next())
						existeDesc = rsExisteDesc.getInt(1);
					rsExisteDesc.close();
					con.cierraStatement();
					qryNafin = new StringBuffer();
					if(existeDesc>0){
						if(!"".equals(diasMin)||!"".equals(diasMax)){
							qryNafin.append(" UPDATE comrel_producto_epo");
							qryNafin.append("    SET ig_dias_minimo = ");
							qryNafin.append(diasMin);
							qryNafin.append("        ,ig_dias_maximo = ");
							qryNafin.append(diasMax);
							qryNafin.append("        ,fn_aforo = ");
							qryNafin.append(porcentajeM);
							qryNafin.append("        ,fn_aforo_dl = ");
							qryNafin.append(porcentajeD);
							qryNafin.append("        ,cs_factoraje_vencido = ");
							qryNafin.append("'" + operaFactVenc + "'");
							qryNafin.append("        ,cs_factoraje_distribuido = ");
							qryNafin.append("'" + operaFactDist + "'");
                                                        qryNafin.append("        ,CS_DESC_AUTO_EPO = "); //2019_QC
                                                        qryNafin.append("'" + operaDescAutoEPO + "'"); //2019_QC	 					    
							qryNafin.append("  WHERE ic_producto_nafin = 1 ");
							qryNafin.append("    AND ic_epo = ");
							qryNafin.append(claveEpo);
						}
					}else{
						if(!"".equals(diasMin)||!"".equals(diasMax)){
							qryNafin.append(" INSERT INTO comrel_producto_epo");
							qryNafin.append("             (ic_producto_nafin, ic_epo, ig_dias_minimo, ig_dias_maximo, fn_aforo, fn_aforo_dl, cs_factoraje_vencido, cs_factoraje_distribuido, CS_DESC_AUTO_EPO)");
							qryNafin.append("      VALUES (1,");// ?, ?, ?, ?, ?, ?, ?)");
							qryNafin.append(claveEpo + ",");
							qryNafin.append(diasMin + ",");
							qryNafin.append(diasMax + ",");
							qryNafin.append(porcentajeM + ",");
							qryNafin.append(porcentajeD + ",");
							qryNafin.append("'" + operaFactVenc + "',");
							qryNafin.append("'" + operaFactDist + "',");
                                                        qryNafin.append("'" + operaDescAutoEPO + "')"); //2019_QC
						}
					}
/*************************************** Cesion de derechos **********************************/
/********************************************************************************************/

/****************************** inicio validacion factoraje vencido *************************/
					StringBuffer strFactoraje = new StringBuffer();
					boolean blnValido = true;
					if(bOperaFactorajeVencido && operaFactVenc!=null&&operaFactVenc.equals("N")){
						strFactoraje.append("SELECT count(*) FROM com_documento ");
						strFactoraje.append(" WHERE ic_epo = ");
						strFactoraje.append(claveEpo);
						strFactoraje.append(" AND cs_dscto_especial='V' ");
						strFactoraje.append(" AND ic_estatus_docto NOT IN(5, 6, 7, 9, 10, 11)");

						ResultSet rsFactoraje = con.queryDB(strFactoraje.toString());
						log.info("strFactoraje = " + strFactoraje.toString());

						if(rsFactoraje.next()){
							if(rsFactoraje.getInt(1)>0){
								blnValido = false;
								bandera = (bandera.equals("0"))?"4":bandera;
							}
						}else{
							blnValido = false;
							bandera = (bandera.equals("0"))?"4":bandera;
						}
						rs.close();
						con.cierraStatement();
					}
/****************************** termina validacion factoraje vencido *********************/
					StringBuffer qryAforo = new StringBuffer();
					if(blnValido){
						con.ejecutaSQL(qryNafin.toString());
						log.info("qryNafin = " + qryNafin.toString());

						//FODEA 037 - 2009 ACF
						/*if(qrySQLCesion != null && !qrySQLCesion.equals("")){
							con.ejecutaSQL(qrySQLCesion);
						}*/
						ParametrosGrales BeanParametrosGenerales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
						BeanParametrosGenerales.actualizaOtrosParametros(porcentajeM, porcentajeD, operaFactVenc, operaFactDist, diasNotif, diasNotifReact, claveEpo);

						//Actualiza la variable de ambiente strAforo.
						qryAforo.append(" SELECT MIN (x.fn_aforo) / 100, MIN (x.fn_aforo_dl) / 100, '15epo/15admotrosparamepoext.jsp'");
						qryAforo.append("   FROM (SELECT fn_aforo, fn_aforo_dl");
						qryAforo.append("           FROM comrel_producto_epo");
						qryAforo.append("          WHERE ic_epo = ");
						qryAforo.append(claveEpo);
						qryAforo.append("            AND ic_producto_nafin = 1");
						qryAforo.append("         UNION");
						qryAforo.append("         SELECT fn_aforo, fn_aforo_dl");
						qryAforo.append("           FROM comcat_producto_nafin");
						qryAforo.append("          WHERE ic_producto_nafin = 1) x");

						PreparedStatement pstmt = null;
						pstmt= con.queryPrecompilado(qryAforo.toString());
						ResultSet rsAforo = pstmt.executeQuery();
						pstmt.clearParameters();
						log.info("qryAforo= " + qryAforo.toString());

/*************************** Descuento Automatico  Fodea 018-2010*********************/
						String PUB_EPO_DESCAUTO_FACTVENCIDO = "PUB_EPO_DESCAUTO_FACTVENCIDO";
						StringBuffer qrySQLDAVencidoD = new StringBuffer();
						StringBuffer qrySQLDAVencidoI = new StringBuffer();
						//Checar sDesAutoFactVenci1
						if(sDesAutoFactVenci.equals("S")){
							qrySQLDAVencidoD.append(" DELETE com_parametrizacion_epo WHERE ");
							qrySQLDAVencidoD.append(" cc_parametro_epo IN( ");
							qrySQLDAVencidoD.append("'PUB_EPO_DESCAUTO_FACTVENCIDO' )");
							qrySQLDAVencidoD.append(" AND ic_epo = ");
							qrySQLDAVencidoD.append(claveEpo);
							con.ejecutaSQL(qrySQLDAVencidoD.toString());

							con.cierraStatement();
							qrySQLDAVencidoI.append("  INSERT INTO  com_parametrizacion_epo ");
							qrySQLDAVencidoI.append("  (ic_epo,cc_parametro_epo,cg_valor) ");
							qrySQLDAVencidoI.append(" VALUES (");
							qrySQLDAVencidoI.append(claveEpo + ",");
							qrySQLDAVencidoI.append("'" + PUB_EPO_DESCAUTO_FACTVENCIDO + "',");
							qrySQLDAVencidoI.append("'" + sDesAutoFactVenci + "')");

							con.ejecutaSQL(qrySQLDAVencidoI.toString());
							log.info("qrySQLDAVencidoI = " + qrySQLDAVencidoI.toString());

						}else if(sDesAutoFactVenci.equals("N")){//Checar despu�s  sDesAutoFactVenci
							qrySQLDAVencidoD.append(" DELETE com_parametrizacion_epo WHERE ");
							qrySQLDAVencidoD.append(" CC_PARAMETRO_EPO in( ");
							qrySQLDAVencidoD.append("'PUB_EPO_DESCAUTO_FACTVENCIDO' )");//F018-2010
							qrySQLDAVencidoD.append(" AND IC_EPO = ");
							qrySQLDAVencidoD.append(claveEpo);

							con.ejecutaSQL(qrySQLDAVencidoD.toString());
						}
						log.info("qrySQLDAVencidoD = " + qrySQLDAVencidoD.toString());
						/*Fodea 018-2010*/

						//FODEA 032-2010 next()(I)
						StringBuffer qrySQLFactDistAutCta = new StringBuffer();
						if(operaFactDist.equals("N")){
							qrySQLFactDistAutCta.append(" DELETE com_parametrizacion_epo WHERE ");
							qrySQLFactDistAutCta.append(" CC_PARAMETRO_EPO in( ");
							qrySQLFactDistAutCta.append("'AUTORIZA_CTAS_BANC_ENTIDADES' )");
							qrySQLFactDistAutCta.append(" AND IC_EPO = ");
							qrySQLFactDistAutCta.append(claveEpo);

							con.ejecutaSQL(qrySQLFactDistAutCta.toString());
							log.info("qrySQLFactDistAutCta = "+qrySQLFactDistAutCta);
						}

						//FODEA 032-2010 next()(F)
						if(rsAforo.next()){
							strAforo = (rsAforo.getString(1)!=null)?rsAforo.getString(1):"";
							strAforoDL = (rsAforo.getString(2)!=null)?rsAforo.getString(2):"";
						}
						rsAforo.close();
						if(pstmt!=null)
							pstmt.close();
						con.terminaTransaccion(true);
						bandera = "1";
					}
				}else
					bandera = "2";
			}
			map.put("bandera",bandera);
			map.put("strAforo",strAforo);
			map.put("strAforoDL",strAforoDL);
		}catch(Exception e){
			log.error("actualizaOtrosParametros(Error)",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return map;
	}
	/**
	 * Este m�todo obtiene porcentaje de descuento en M.N y d�lares, d�as l�mite para notificaci�n, d�as m�nimos y m�ximos
	 * para descuento autom�tico y cesi�n de derechos
	 *
	 * @since 	Migraci�n EPO 2012
	 * @param 	claveEpo Clave de la EPO.
	 * @param 	flagfvenc Bandera de validaci�n.
	 * @return 	HashMap Par�metros de descuento autom�tico y cesi�n de derechos.
	 *
	 */
	public HashMap datosOtrosParametros (String claveEpo, String flagfvenc){
		AccesoDB		con				=	new AccesoDB();
		StringBuffer queryPorciento;
		StringBuffer queryDatNafin;
		StringBuffer qryCesion;
		StringBuffer queryDatEpo;
		StringBuffer qrySQLDAVencido;
		HashMap			map				=  null;
		String diasMinNaf = "";
		String diasMaxNaf = "";
		String porciento = "";
		String strDiasMin = "";
		String strDiasMax = "";
		String strPorcientoActual = "";
		String strPorcientoActualDL = "";
		String operaFactVenc = "";
		String operaFactDist = "";
		String sDesAutoFactVenci = "";
		String strDiasNotif = "";
		String strDiasNotifReact = "";
		String strPorciento = "";
		String strPorcientoDL = "";
                String operaDescAutoEPO ="";
		try{
			con.conexionDB();
			queryPorciento = new StringBuffer();
			queryDatNafin = new StringBuffer();
			qryCesion = new StringBuffer();
			queryDatEpo = new StringBuffer();
			qrySQLDAVencido = new StringBuffer();
			map = new HashMap();

			queryPorciento.append(" SELECT fn_aforo, fn_aforo_dl");
			queryPorciento.append("   FROM comcat_producto_nafin");
			queryPorciento.append("  WHERE ic_producto_nafin = 1");
			ResultSet rs0 = con.queryDB(queryPorciento.toString());
			if(rs0.next()){
				strPorciento = (rs0.getString("fn_aforo") == null)?"":rs0.getString("fn_aforo");
				strPorcientoDL = (rs0.getString("fn_aforo_dl") == null)?"":rs0.getString("fn_aforo_dl");
			}
			rs0.close();
			con.cierraStatement();

			queryDatNafin.append(" SELECT in_dias_minimo, in_dias_maximo, fn_aforo FROM comcat_producto_nafin");
			queryDatNafin.append(" WHERE ic_producto_nafin = 1");
			ResultSet rs = con.queryDB(queryDatNafin.toString());
			if(rs.next()){
				diasMinNaf = (rs.getString("in_dias_minimo") == null)?"":rs.getString("in_dias_minimo");
				diasMaxNaf = (rs.getString("in_dias_maximo") == null)?"":rs.getString("in_dias_maximo");
				porciento = (rs.getString("fn_aforo") == null)?"":rs.getString("fn_aforo");
			}
			rs.close();
			con.cierraStatement();
			log.info("queryDatNafin = " + queryDatNafin.toString());

			qryCesion.append(" SELECT ig_dias_pnotificacion, ig_dias_pnotificacion_reactiv ");
			qryCesion.append("   FROM comrel_producto_epo");
			qryCesion.append("  WHERE ic_producto_nafin = 9 ");
			qryCesion.append("    AND ic_epo = ");
			qryCesion.append(claveEpo);
			rs = con.queryDB(qryCesion.toString());
			if(rs.next()){
				strDiasNotif  = (rs.getString("ig_dias_pnotificacion") == null)?"":rs.getString("ig_dias_pnotificacion");
				strDiasNotifReact = (rs.getString("ig_dias_pnotificacion_reactiv") == null)?"":rs.getString("ig_dias_pnotificacion_reactiv");
			}
			rs.close();
			con.cierraStatement();
			log.info("qryCesion = " + qryCesion.toString());

			queryDatEpo.append(" SELECT ig_dias_minimo, ig_dias_maximo, fn_aforo, fn_aforo_dl,");
			queryDatEpo.append("        cs_factoraje_vencido, cs_factoraje_distribuido, CS_DESC_AUTO_EPO  ");
			queryDatEpo.append("   FROM comrel_producto_epo");
			queryDatEpo.append("  WHERE ic_producto_nafin = 1 ");
			queryDatEpo.append("    AND ic_epo = ");
			queryDatEpo.append(claveEpo);
			ResultSet rs2 = con.queryDB(queryDatEpo.toString());
			if(rs2.next()){
				strDiasMin = (rs2.getString("ig_dias_minimo") == null)?"":rs2.getString("ig_dias_minimo");
				strDiasMax = (rs2.getString("ig_dias_maximo") == null)?"":rs2.getString("ig_dias_maximo");
				strPorcientoActual = (rs2.getString("fn_aforo") == null)?"":rs2.getString("fn_aforo");
				strPorcientoActualDL = (rs2.getString("fn_aforo_dl") == null)?"":rs2.getString("fn_aforo_dl");

				if(flagfvenc!=null && !flagfvenc.equals("1"))
					operaFactVenc = (rs2.getString("cs_factoraje_vencido")==null)?"N":rs2.getString("cs_factoraje_vencido").trim();
				operaFactDist = (rs2.getString("cs_factoraje_distribuido")==null)?"N":rs2.getString("cs_factoraje_distribuido").trim();
			    operaDescAutoEPO = (rs2.getString("CS_DESC_AUTO_EPO")==null)?"N":rs2.getString("CS_DESC_AUTO_EPO").trim();
			}
			rs2.close();
			con.cierraStatement();
			log.info("queryDatEpo = " + queryDatEpo.toString());

			/* Fodea 018-2010 */
			qrySQLDAVencido.append(" SELECT cg_valor FROM com_parametrizacion_epo ");
			qrySQLDAVencido.append(" WHERE cc_parametro_epo = 'PUB_EPO_DESCAUTO_FACTVENCIDO' ");
			qrySQLDAVencido.append(" AND  ic_epo = ");
			qrySQLDAVencido.append(claveEpo);

			ResultSet rs1 = con.queryDB(qrySQLDAVencido.toString());
			if(rs1.next()){
				sDesAutoFactVenci = (rs1.getString("cg_valor")==null)?"N":rs1.getString("cg_valor");
			}
			rs1.close();
			con.cierraStatement();
			/*Fodea 018-2010*/
			log.info("qrySQLDAVencido = " + qrySQLDAVencido.toString());
			map.put("strPorciento",strPorciento);
			map.put("strPorcientoDL",strPorcientoDL);
			map.put("diasMinNaf",diasMinNaf);
			map.put("diasMaxNaf",diasMaxNaf);
			map.put("porciento",porciento);
			map.put("strDiasNotif",strDiasNotif);
			map.put("strDiasNotifReact",strDiasNotifReact);
			map.put("strDiasMin",strDiasMin);
			map.put("strDiasMax",strDiasMax);
			map.put("strPorcientoActual",strPorcientoActual);
			map.put("strPorcientoActualDL",strPorcientoActualDL);
			map.put("operaFactVenc",operaFactVenc);
			map.put("operaFactDist",operaFactDist);
			map.put("sDesAutoFactVenci",sDesAutoFactVenci);
		        map.put("operaDescAutoEPO",operaDescAutoEPO);
		    
		}catch(Exception e){
			log.error("datosOtrosParametros(Error)",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return map;
	}


	/**
	 * Verifica si la epo acepta "Factoraje Vencido"
	 * se usa en la pantalla de Avisos de Notificacion
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param producto
	 * @param claveEPO
	 */
	public String getfacVencido(String claveEPO, String producto) 	throws AppException {
		log.info("getfacVencido(E)");
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String bOperaFactoraje  ="N";
		try {
			con.conexionDB();
			qrySentencia =	" SELECT count(1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = ? AND ic_epo =  ?"  +
				"	AND cs_factoraje_distribuido = 'S' ";

			lVarBind.add(new Integer(producto));
			lVarBind.add(new Integer(claveEPO));

			registros = con.consultarDB(qrySentencia, lVarBind, false);

			if(registros.next()){
				if(Integer.parseInt( registros.getString(1))>0)
					bOperaFactoraje = "S";
			}

		}catch(Throwable e){
			log.info(" getfacVencido  Exception "+e);
			e.printStackTrace();
			throw new AppException("Error ", e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getfacVencido (S)");
		}
		return bOperaFactoraje;
	}

	/**
	 * OPT Estos campos agregar al query me los llevar�a en el summary
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param producto
	 * @param claveEPO
	 */
	public List  getCamposAdicionales(String claveEPO, String producto) 	throws AppException {
		log.info("getCamposAdicionales(E)");

		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List 				lVarBind					= new ArrayList();
		List vCamposAdcionales = new ArrayList();
		List vca  = new ArrayList();
		String strCampos  ="", strNombreCampos ="";

		try {
			con.conexionDB();
			qrySentencia =	" SELECT   ic_no_campo, INITCAP (cg_nombre_campo) AS cg_nombre_campo"   +
					"  FROM comrel_visor"   +
					"    WHERE ic_epo = ?  AND ic_producto_nafin = ? "   +
					"          AND ic_no_campo BETWEEN 1 AND 5"   +
					" ORDER BY ic_no_campo"  ;
			lVarBind.add(new Integer(claveEPO));
			lVarBind.add(new Integer(producto));

			ps = con.queryPrecompilado(qrySentencia, lVarBind);
			rs = ps.executeQuery();

			while(rs.next()){
				vca = new Vector();
					strCampos = "";
					strNombreCampos ="";
				strCampos =  ",D.CG_CAMPO" + rs.getString("IC_NO_CAMPO") + " as CAMPO"+rs.getString("IC_NO_CAMPO");
				strNombreCampos = rs.getString("CG_NOMBRE_CAMPO");
				vca.add(strCampos);
				vca.add(strNombreCampos);
				vCamposAdcionales.add(vca);
			}
			rs.close();
			ps.close();

		}catch(Throwable e){
			log.info(" getCamposAdicionales  Exception "+e);
			e.printStackTrace();
			throw new AppException("Error ", e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getCamposAdicionales (S)");
		}

		return vCamposAdcionales;
	}
		/**
	 * Obtiene estado de factorajes
	 * @return hm HashMap con el estadod de los factorajes
	 * @param claveEPO
	 */
	public HashMap  getOperaFactoraje(String claveEPO){
		AccesoDB con = new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();
		List varBind = new ArrayList();
		HashMap hm = new HashMap();
		boolean bOperaFactorajeVencido = false;
		boolean bOperaFactorajeDistribuido = false;
		boolean bOperaFactorajeNotaDeCredito=false;
		try {
			con.conexionDB();
			qrySentencia.append(" SELECT COUNT (1)");
			qrySentencia.append("   FROM comrel_producto_epo");
			qrySentencia.append("  WHERE ic_producto_nafin = 1");
			qrySentencia.append("    AND cs_factoraje_vencido = 'S'");
			qrySentencia.append("    AND ic_epo = ? ");
			qrySentencia.append(" UNION ALL ");
			qrySentencia.append(" SELECT COUNT (1)");
			qrySentencia.append("   FROM comrel_producto_epo");
			qrySentencia.append("  WHERE ic_producto_nafin = 1");
			qrySentencia.append("    AND cs_factoraje_distribuido = 'S'");
			qrySentencia.append("    AND ic_epo = ? ");
			qrySentencia.append(" UNION ALL ");
			qrySentencia.append(" SELECT COUNT (1)");
			qrySentencia.append("   FROM comrel_producto_epo");
			qrySentencia.append("  WHERE ic_producto_nafin = 1");
			qrySentencia.append("    AND CS_OPERA_NOTAS_CRED = 'S'");
			qrySentencia.append("    AND ic_epo = ? ");
			varBind.add(claveEPO);
			varBind.add(claveEPO);
			varBind.add(claveEPO);

			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencido = true; }
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeDistribuido = true; }
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeNotaDeCredito = true; }
			ps.close();
			rs.close();
			hm.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
			hm.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
			hm.put("bOperaFactorajeNotaDeCredito", new Boolean(bOperaFactorajeNotaDeCredito));
		}catch(Throwable e){
			log.info(" getOperaFactoraje:: "+e);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getOperaFactoraje:: (S)");
		}
		return hm;
	}
	/**
	 *
	 * @return
	 * @param ic_epo
	 * @param num_electronico
	 */
	public List  datosPymes(String  num_electronico , String ic_epo ){

		List datos =new ArrayList();
		String 				qrySentencia 	= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		String ic_pyme ="", txtNombre="";

		try{
			con.conexionDB();

			if(!"".equals(num_electronico)) {

				qrySentencia =" SELECT distinct n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre "   +
								  " FROM comrel_nafin n, comcat_pyme p "   +
								  " WHERE n.ic_epo_pyme_if = p.ic_pyme "+
								  " AND ic_nafin_electronico =  ? " ;

			System.out.println(qrySentencia);

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(num_electronico));
				rs = ps.executeQuery();
				if(rs.next()){
					ic_pyme = rs.getString(1)==null?"":rs.getString(1);
					txtNombre = rs.getString(2)==null?"":rs.getString(2);
					datos.add(ic_pyme);
					datos.add(txtNombre);

				}
				rs.close();
				ps.close();
			}
	} catch(Exception e) {

		e.printStackTrace();
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
		return datos;
	}


	/**
	 *  metodo para la pantalla de  Descuento Electronico-ConsultasInformaci�n de Solicitudes
	 * @return
	 * @param ic_epo
	 * @param num_electronico
	 */

	public List  datosPymesInfSolic(String  num_electronico , String ic_epo ){

		List datos =new ArrayList();
		String 				qrySentencia 	= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		String ic_pyme ="", txtNombre="";

		try{
			con.conexionDB();

			if(!"".equals(num_electronico)) {

				qrySentencia = " SELECT pym.ic_pyme, pym.cg_razon_social, '13descuento/13if/13consulta1.jsp' as pantalla "   +
						"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym , comcat_epo E"   +
						"  WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
						"    AND cpe.ic_pyme = pym.ic_pyme"   +
						"    AND crn.ic_nafin_electronico = ?"   +
						"    AND crn.cg_tipo = 'P'"   +
						"    AND cpe.cs_habilitado = 'S'"   +
						"    AND cpe.cg_pyme_epo_interno IS NOT NULL"  +
						"	   AND cpe.ic_epo = E.ic_epo " +
						"    AND cpe.ic_epo = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(num_electronico));
				ps.setInt(2,Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				if(rs.next()){
					ic_pyme = rs.getString(1)==null?"":rs.getString(1);
					txtNombre = rs.getString(2)==null?"":rs.getString(2);
					datos.add(ic_pyme);
					datos.add(txtNombre);

				}
				rs.close();
				ps.close();
			}
	} catch(Exception e) {

		e.printStackTrace();
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
		return datos;
	}


	/**
	 *  *  metodo para la pantalla de  Descuento Electronico-ConsultasInformaci�n de Solicitudes
	 * @return
	 * @param ic_epo
	 * @param ic_if
	 */
	public Hashtable  parametrizacion(String  ic_if, String ic_epo ){

		 Hashtable alParametros = new Hashtable();
		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  bOperaFactorajeVencidoGral = "N", bOperaFactorajeAutomaticoGral = "N", bOperaFactorajeDistGral = "N",
		bOperaFactorajeMandatoGral = "N",  bOperaFactorajeVencINFOGral = "N",
		bOperaPubHash  ="N", bOperaNC="N";

		try{
			con.conexionDB();

			qrySentencia=	" SELECT COUNT (1)"   +
								"   FROM comrel_if_epo"   +
								"  WHERE cs_vobo_nafin = 'S'"   +
								"    AND ic_if = ?"   +
								"    AND ic_epo IN (SELECT ic_epo"   +
								"                     FROM comrel_producto_epo"   +
								"                    WHERE ic_producto_nafin = 1"   +
								"                      AND cs_factoraje_vencido = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = "S"; }
			rs.close();ps.close();

			qrySentencia=	" SELECT COUNT (1)"   +
								"   FROM comrel_if_epo"   +
								"  WHERE cs_vobo_nafin = 'S'"   +
								"    AND ic_if = ?"   +
								"    AND ic_epo IN (SELECT ic_epo"   +
								"                     FROM comrel_producto_epo"   +
								"                    WHERE ic_producto_nafin = 1"   +
								"                      AND cs_factoraje_if = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeAutomaticoGral = "S"; }
			rs.close();ps.close();

			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_if_epo"   +
				"  WHERE cs_vobo_nafin = 'S'"   +
				"    AND ic_if = ?"   +
				"    AND ic_epo IN (SELECT ic_epo"   +
				"                     FROM comrel_producto_epo"   +
				"                    WHERE ic_producto_nafin = 1 "+
				"						AND cs_factoraje_distribuido = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeDistGral = "S";}
			rs.close();ps.close();

			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_if_epo"   +
				"  WHERE cs_vobo_nafin = 'S'"   +
				"    AND ic_if = ?"   +
				"    AND ic_epo IN (SELECT ic_epo"   +
				"                     FROM com_parametrizacion_epo"   +
				"                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
				"                      AND CG_VALOR = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = "S"; }
			rs.close();ps.close();

			//MODIFCACION FODEA 042-2009 FVR
			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_if_epo"   +
				"  WHERE cs_vobo_nafin = 'S'"   +
				"    AND ic_if = ?"   +
				"    AND ic_epo IN (SELECT ic_epo"   +
				"                     FROM com_parametrizacion_epo"   +
				"                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_VENC_INFONAVIT'"   +
				"                      AND CG_VALOR = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencINFOGral = "S"; }
			rs.close();ps.close();



			if(!ic_epo.equals(""))  {
				qrySentencia=
					" SELECT  CS_PUB_hash"   +
					"   FROM comrel_producto_epo"   +
					"  WHERE ic_producto_nafin = 1"   +
					"    AND ic_epo = ? " ;

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_epo);
				rs = ps.executeQuery();
				if (rs.next()) {  bOperaPubHash  = rs.getString(1);  }
				rs.close();ps.close();

				qrySentencia=" SELECT NVL (cpe.cs_opera_notas_cred, 'N') " +
							"  FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe, comcat_if i " +
							" WHERE e.ic_epo = ie.ic_epo " +
							"	AND ie.ic_epo = cpe.ic_epo " +
							"	AND i.ic_if = ie.ic_if " +
							"	AND i.ic_if = ? " +
							"	AND UPPER (ie.cs_vobo_nafin) = 'S' " +
							"	AND e.cs_habilitado = 'S' " +
							"	AND cpe.ic_producto_nafin = 1 " +
							"	and e.ic_epo =? " +
							"	AND (cpe.ic_modalidad IS NULL OR cpe.ic_modalidad=1) " ;

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
				ps.setString(2,ic_epo);
				rs = ps.executeQuery();
				if (rs.next()) {  bOperaNC  = rs.getString(1);  }
				rs.close();ps.close();
			}

			alParametros.put("cs_factoraje_automatico", bOperaFactorajeAutomaticoGral);
			alParametros.put("cs_factoraje_vencido", bOperaFactorajeVencidoGral);
			alParametros.put("cs_factoraje_distribuido", bOperaFactorajeDistGral);
			alParametros.put("PUB_EPO_OPERA_MANDATO", bOperaFactorajeMandatoGral);
			alParametros.put("PUB_EPO_VENC_INFONAVIT", bOperaFactorajeVencINFOGral);
			alParametros.put("CS_PUB_hash", bOperaPubHash);
			alParametros.put("bOperaNC", bOperaNC);

		} catch(Exception e) {

			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
			return alParametros;
		}

	/**
	 *  Migracion IF
	 *   Metodo para la pantalla Descuento - reportes -de Doctos / Solic. por Estatus
	 * @return
	 * @param ic_if
	 */

	public Hashtable  parametrizacionIF(String  ic_if ){

		 Hashtable alParametros = new Hashtable();
		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  sOperaFactConMandato ="N", sOperaFactVencimientoInfonavit  ="N",
				  bOperaFactorajeVencido = "N",bOperaFactorajeAutomatico = "N", bOperaFactorajeDist = "N", bTipoFactoraje ="N";

		try{
			con.conexionDB();

			qrySentencia=	" SELECT COUNT (1) " +
								"  FROM comrel_if_epo ie, com_parametrizacion_epo pe " +
								" WHERE ie.ic_epo = pe.ic_epo " +
								"   AND CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO' " +
								"   AND CG_VALOR = 'S' " +
								"   AND ie.ic_if = ? "  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) sOperaFactConMandato = "S"; }
			rs.close();
			ps.close();


			qrySentencia=	" SELECT COUNT (1) " +
								"  FROM comrel_if_epo ie, com_parametrizacion_epo pe " +
								" WHERE ie.ic_epo = pe.ic_epo " +
								"   AND CC_PARAMETRO_EPO = 'PUB_EPO_VENC_INFONAVIT' " +
								"   AND CG_VALOR = 'S' " +
								"   AND ie.ic_if = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) { if(rs.getInt(1)>0) sOperaFactVencimientoInfonavit = "S"; }
			rs.close();
			ps.close();

			qrySentencia=	" SELECT COUNT (1)"   +
								"   FROM comrel_if_epo ie, comrel_producto_epo e"   +
								"  WHERE ie.ic_epo = e.ic_epo"   +
								"    AND e.ic_producto_nafin = 1"   +
								"    AND e.cs_factoraje_vencido = 'S'"   +
								"    AND ic_if = ? "+
								" UNION ALL"   +
								" SELECT COUNT (1)"   +
								"   FROM comrel_if_epo ie, comrel_producto_epo e"   +
								"  WHERE ie.ic_epo = e.ic_epo"   +
								"    AND e.ic_producto_nafin = 1"   +
								"    AND e.cs_factoraje_distribuido = 'S'"   +
								"    AND ic_if = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			ps.setString(2,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) {
				if(rs.getInt(1)>0) {
					bOperaFactorajeVencido = "S";
					bOperaFactorajeDist = "S";
				}
			}
			rs.close();
			ps.close();

			qrySentencia=	" SELECT COUNT (1)"   +
								"   FROM comrel_if_epo ie, comrel_producto_epo e"   +
								"  WHERE ie.ic_epo = e.ic_epo"   +
								"    AND e.ic_producto_nafin = 1"   +
								"    AND e.cs_factoraje_if = 'S'"   +
								"    AND ic_if = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) {
				if(rs.getInt(1)>0) {
               bOperaFactorajeAutomatico = "S";
				}
			}
			rs.close();
			ps.close();


			if(bOperaFactorajeAutomatico.equals("S") || bOperaFactorajeVencido.equals("S") || bOperaFactorajeDist.equals("S") || sOperaFactConMandato.equals("S") ||  sOperaFactVencimientoInfonavit.equals("S") ) {
				bTipoFactoraje =  "S";
			}

         alParametros.put("cs_factoraje_automatico", bOperaFactorajeAutomatico);
			alParametros.put("PUB_EPO_OPERA_MANDATO", sOperaFactConMandato);
			alParametros.put("PUB_EPO_VENC_INFONAVIT", sOperaFactVencimientoInfonavit);
			alParametros.put("bOperaFactorajeVencido", bOperaFactorajeVencido);
			alParametros.put("bOperaFactorajeDist", bOperaFactorajeDist);
			alParametros.put("bTipoFactoraje", bTipoFactoraje);

		} catch(Exception e) {

			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
			return alParametros;
		}


	/**
	 * metodo para saber si  el IF opera convenio Unico
	 * para la pantalla de Cuentas Bancarias PYMEs (Consulta)
	 * @return
	 * @param ic_if
	 */
	public String  sIfConvenioUnico(String  ic_if ){

		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  sIfConvenioUnico ="N";

		try{
			con.conexionDB();

			qrySentencia=	" SELECT CS_CONVENIO_UNICO " +
								"  FROM comcat_if " +
								" WHERE ic_if = ? " ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if (rs.next()) {
				sIfConvenioUnico = rs.getString("CS_CONVENIO_UNICO");
			}
			rs.close();
			ps.close();

		} catch(Exception e) {

			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
			return sIfConvenioUnico;
		}



/**metodo  para obtener las Pymes en la pantalla de Cuentas Bancarias Pymes -Autorizaci�n
	* @param ic_epo, ic_if
	*/

	public String  catalogoPyme(String  ic_epo, String ic_if ){
		log.info("catalogoPyme (E)");

		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		JSONArray registros = new JSONArray();
		HashMap info = new HashMap();
		String 	infoRegresar ="";

		try{
			con.conexionDB();
			ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			qrySentencia=
							" SELECT PYME.IC_PYME, PYME.CG_RAZON_SOCIAL, PYME.cs_expediente_efile, PYME.CG_RFC "+
							" FROM  COMREL_PYME_EPO PE,   COMCAT_PYME PYME, "+
							" COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF, COMCAT_MONEDA M "+
							" WHERE PE.IC_PYME = PYME.IC_PYME "+
							" AND   PYME.IC_PYME = RELCTABANC.IC_PYME "+
							" AND RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA "+
							" AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
							" AND RELPYMEIF.CS_BORRADO='N' "+
							" AND PE.cs_habilitado='S' "+
							" AND PYME.cs_habilitado = 'S' "+
							" AND RELPYMEIF.CS_VOBO_IF ='N' "+
							" AND RELPYMEIF.CS_ESTATUS IS NULL "+
							" AND PE.IC_EPO = ? "+
							" AND RELPYMEIF.IC_IF = ? " +
							" AND RELPYMEIF.IC_EPO = PE.IC_EPO "+
							" GROUP BY PYME.IC_PYME, PYME.CG_RAZON_SOCIAL, PYME.cs_expediente_efile, PYME.CG_RFC "+
							" ORDER BY 2 	";

			System.out.println("qrySentencia "+qrySentencia);
			System.out.println("ic_epo "+ic_epo+" ic_if   "+ic_if);


			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_if);
			rs = ps.executeQuery();
			while (rs.next()){
				info = new HashMap();
				if ("N".equals(rs.getString("CS_EXPEDIENTE_EFILE"))) {
					if(tieneExpedienteEnEfile(rs.getString("CG_RFC"))){
						parametrosDescuento.setEstatusDelExpedienteEfileEnTablaPyme(rs.getString("CG_RFC"),true);
						info.put("clave", rs.getString(1));
						info.put("descripcion", rs.getString(2));
					}
				} else {
					info.put("clave", rs.getString(1));
					info.put("descripcion", rs.getString(2));
				}
				registros.add(info);
			}
			rs.close();
			ps.close();

			infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


		} catch(Exception e) {

			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("catalogoPyme (S)");
		}
			return infoRegresar;

	}


	/**
	 *	Devuelve <tt>true</tt> o <tt>false</tt>, dependiendo si la pyme tiene expediente en el
	 * Sistema EFile.
	 * @param rfc Cadena de Texto con el RFC de la PYME
	 * @return <tt>boolean</tt>. <tt>true</tt> si tiene expediente. <tt>false</tt> en caso contrario.
	 */
	public boolean tieneExpedienteEnEfile(String rfc)throws AppException{
      log.info("tieneExpedienteEnEfile (E)");
		if(rfc == null || rfc.trim().equals("")) return false;
		String 			qrySentencia	= null;
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		boolean			resultado		= false;
		AccesoDB con =new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia =
				"SELECT 	DECODE(COUNT(1),0,'false','true') AS HAY_EXPEDIENTE "  +
				"	FROM VM_PYMES_EFILE "  +
				"	WHERE CG_RFC 	= ? ";       // RFC PYME
			lVarBind.add(rfc);

			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next()){
				resultado = registros.getString("HAY_EXPEDIENTE").equals("true")?true:false;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error al consultar si hay expediente para la pyme en el Sistema EFile");
		}finally{
			con.terminaTransaccion(true);
			con.cierraConexionDB();
			log.info("tieneExpedienteEnEfile (S)");
		}
		return resultado;
	}


	 /**
	 * Obtener datos para saber las cuentas pendientes de autorizar
	 * @return
	 * @param ic_if
	 */
	public int  verifCuentasPendResul(String  ic_if ){
		log.info("verifCuentasPendResul (E)");
		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		int numeroCuentasPendientes = 0;

		try{
			con.conexionDB();
			qrySentencia = " SELECT COUNT(*) TOTAL_CUENTAS_PENDIENTES " +
				            " FROM COMCAT_PYME , COMCAT_EPO, COMHIS_CAMBIOS_CTA " +
                        " WHERE COMHIS_CAMBIOS_CTA.IC_PYME = COMCAT_PYME.IC_PYME " +
                        " AND COMHIS_CAMBIOS_CTA.IC_EPO = COMCAT_EPO.IC_EPO " +
                        " AND COMHIS_CAMBIOS_CTA.IC_IF =  ?"+
								" AND COMHIS_CAMBIOS_CTA.CS_AUTORIZA_IF  = 'N' ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if(rs.next()){
				numeroCuentasPendientes = rs.getInt("TOTAL_CUENTAS_PENDIENTES");
        }
			rs.close();
			ps.close();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("verifCuentasPendResul (S)");
		}
			return numeroCuentasPendientes;
	}

	/**
	 * consulta de la pantalla autorizaci�n Cuentas Bancarias PYMEs
	 * @return
	 * @param hasta
	 * @param desde
	 * @param ic_pyme
	 * @param cboEPO
	 * @param nafinelect
	 * @param chkPymesConvenio
	 * @param ic_if
	 */
 	public String  consAutotizacion(List parametros){
		log.info("consAutotizacion (E)");
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String icpyme ="", respyme ="", resEpo ="", vpyme ="", consulta="";
		HashMap	info = new HashMap();
		JSONArray registros = new JSONArray();
		ResultSet	rsX;
		String  ic_if =  parametros.get(0).toString();
		String chkPymesConvenio = parametros.get(1).toString();
		String nafinelect  = parametros.get(2).toString();
		String cboEPO  = parametros.get(3).toString();
		String ic_pyme = parametros.get(4).toString();
		String desde = parametros.get(5).toString();
		String hasta = parametros.get(6).toString();

		try{
			con.conexionDB();

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(
				" SELECT  RELPYMEIF.CS_VOBO_IF as CS_VOBO_IF , "+
				" pyme.in_numero_sirac as NUMERO_SIRAC ,"+
				"  RELCTABANC.IC_CUENTA_BANCARIA   as IC_CUENTA_BANCARIA , " +
				" decode (PYME.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '|| PYME.CG_RAZON_SOCIAL as  NOMBRE_PYME, "+
				" RELCTABANC.CG_BANCO as BANCO_SERVICIO , " +
				" RELCTABANC.CG_NUMERO_CUENTA as  NO_CUENTA ,"+
				" M.CD_NOMBRE as NOMBRE_MONEDA , "+
				" M.IC_MONEDA  as IC_MONEDA , "+
				" RELPYMEIF.IC_EPO  AS IC_EPO ,"+
				" PYME.IC_PYME as IC_PYME , "+
				" relpymeif.ic_if as IC_IF  ,  "+
				" RELCTABANC.CG_SUCURSAL as SUCURSAL ,"+
				" EPO.cg_razon_social  as NOMBRE_EPO,  " +
				" PYME.CG_RFC as RFC , "+
				" nvl(PLAZA.CD_DESCRIPCION,'no tiene') as PLAZA,  " +
				" relctabanc.CG_CUENTA_CLABE as CUENTA_CLABE ,  " +
				" TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') FECHA_FIRMA_AUTO,  " +
				"  TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM_CU,  " +
				" DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO,  " +
				" crn.ic_nafin_electronico NUM_NAFIN_ELEC, " +//FODEA 018 - 2009 ACF
				" DECODE (RELPYMEIF.cs_cambio_cuenta, 'S', 'Cambio de Cuenta' )as CS_CAMBIO_CUENTA "+
				" FROM COMCAT_MONEDA M, COMCAT_PYME PYME, " +
				" COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF,comcat_epo EPO " +
				" ,    COMCAT_PLAZA PLAZA "+
				"       , comrel_pyme_epo pe "+
				"       , comrel_nafin crn "+//FODEA 018 - 2009 ACF
				" WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
				" AND PYME.IC_PYME = RELCTABANC.IC_PYME AND RELCTABANC.IC_MONEDA = M.IC_MONEDA AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
				"      AND RELCTABANC.IC_PLAZA = PLAZA.IC_PLAZA(+) " +
				" AND RELPYMEIF.IC_IF = " + ic_if +
				" AND pyme.ic_pyme = crn.ic_epo_pyme_if" +//FODEA 018 - 2009 ACF
				" AND crn.cg_tipo = 'P'" +//FODEA 018 - 2009 ACF
				" AND RELPYMEIF.CS_BORRADO='N' " +
				" AND pe.ic_pyme = PYME.IC_PYME " +
				" AND RELPYMEIF.IC_EPO = PE.IC_EPO "+
				" AND RELPYMEIF.CS_VOBO_IF = 'N' "+
				" AND PYME.IC_PYME  = ? "+
				" AND RELPYMEIF.IC_EPO = ? "+
				" AND pe.IC_EPO =  ? ");

			if(chkPymesConvenio.equals("S")) {
				sbQuery.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");
			}
			sbQuery.append(" ORDER BY 3, 4 ");

			log.debug("sbQuery >>>>"  +sbQuery.toString());


			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(sbQuery.toString());

			if (!nafinelect.equals("")){
				String qrySentenciaS =
					" SELECT ic_epo_pyme_if as pyme, p.cs_expediente_efile, p.cg_rfc " +
					" FROM comrel_nafin n " +
					" , comcat_pyme p " +
					" WHERE n.cg_tipo = 'P' " +
					" AND n.ic_epo_pyme_if = p.ic_pyme " +
					" AND n.ic_nafin_electronico=  " + nafinelect ;
            rs = con.queryDB(qrySentenciaS);
				if(rs.next()){
					icpyme = rs.getString("PYME");
               if ("N".equals(rs.getString("CS_EXPEDIENTE_EFILE"))) {
						if(tieneExpedienteEnEfile(rs.getString("CG_RFC"))){
							this.setEstatusDelExpedienteEfileEnTablaPyme(rs.getString("CG_RFC"),true);
                  }
                }
				}
			}
         con.cierraStatement();

			String qrySentenciaX =
						"SELECT ST.IC_PYME, ST.CG_RAZON_SOCIAL, ST.IC_EPO, ST.IN_NUMERO_SIRAC, ST.CG_RFC, NVL(VP.CS_DOC_VIGENTES,'N') as PRIORIDAD  " +
						" FROM vm_pymes_doc_vig VP, (SELECT distinct(PYME.IC_PYME) AS IC_PYME,  " +
						"		decode (PYME.cs_habilitado,'N','*','S',' ')||' '||PYME.CG_RAZON_SOCIAL AS CG_RAZON_SOCIAL,  " +
						"		RELPYMEIF.ic_epo AS IC_EPO,  " +
						"   	PYME.in_numero_sirac AS in_numero_sirac,  " +
						"		PYME.cg_rfc AS cg_rfc, " +
						"   	'15cadenas15mantenimiento15parametrizacion15if15admparamifpyme.jsp' as PANTALLA " +
						"		FROM COMCAT_MONEDA M, " +
						"		COMCAT_PYME PYME, " +
						"		COMREL_CUENTA_BANCARIA RELCTABANC, " +
						"		COMREL_PYME_IF RELPYMEIF, " +
						"		comcat_epo EPO " +
						"     , comrel_pyme_epo pe "+
						"		WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
						"		AND PYME.IC_PYME = RELCTABANC.IC_PYME " +
						"		AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
						"		AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
						"		AND EPO.cs_habilitado = 'S' " +
						"		AND RELPYMEIF.CS_VOBO_IF ='N' "+
						"		AND RELPYMEIF.IC_IF = " + ic_if +
						"		AND RELPYMEIF.CS_BORRADO='N' " +
						"		AND RELPYMEIF.CS_ESTATUS IS NULL " +
						"		AND pe.ic_pyme = PYME.IC_PYME " +
					   "   AND PYME.in_numero_sirac IS NOT NULL";

			if(!nafinelect.equals("") && !icpyme.equals("")) {
				qrySentenciaX += " AND PYME.IC_PYME  = " + icpyme;
			} else if (!nafinelect.equals("") ){
				qrySentenciaX += " AND PYME.IC_PYME  = 0";
			}
			if(!cboEPO.equals("")) {
				if(!cboEPO.equals("0")) {
					qrySentenciaX += " AND RELPYMEIF.IC_EPO = " + cboEPO;
					qrySentenciaX += "	  AND pe.IC_EPO = " + cboEPO;
				}
				if(!ic_pyme.equals("") &&  !ic_pyme.equals("0") )
					qrySentenciaX += " AND PYME.IC_PYME  = " + ic_pyme;
			}
			if(!desde.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if >= TO_DATE('"+desde+"', 'dd/mm/yyyy') ";
			}
			if(!hasta.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if <  TO_DATE('"+hasta+"', 'dd/mm/yyyy')+1 ";
			}
			qrySentenciaX += " AND PYME.CS_EXPEDIENTE_EFILE = 'S' ";
			qrySentenciaX += " ) ST " +
									" WHERE ST.IC_EPO = VP.IC_EPO(+) " +
									" AND ST.IC_PYME = VP.IC_PYME(+) " +
									"ORDER BY 2 ";

			log.debug("qrySentenciaX >>>>"  +qrySentenciaX.toString());

			log.debug("respyme >>>>"  +respyme);
			log.debug("resEpo >>>>"  +resEpo);


			rsX = con.queryDB(qrySentenciaX);
			 while (rsX.next()){
				respyme= rsX.getString(1);
				resEpo= rsX.getString(3);
				int numeroPyme=Integer.parseInt(respyme);
				int numeroEpo=Integer.parseInt(resEpo);
				pstmt.setInt(1,numeroPyme);
				pstmt.setInt(2,numeroEpo);
				pstmt.setInt(3,numeroEpo);

				//log.debug("numeroPyme >>>>"  +numeroPyme);
				//log.debug("numeroEpo >>>>"  +numeroEpo);
				//log.debug("numeroEpo >>>>"  +numeroEpo);


				ResultSet rsTraeIF = pstmt.executeQuery();
				pstmt.clearParameters();
				vpyme =	((rsX.getString(2)==null)?"":rsX.getString(2).replace(',',' '))  ;
				while(rsTraeIF.next()){

					String num_sirac  = (rsTraeIF.getString("NUMERO_SIRAC") == null) ? "" : rsTraeIF.getString("NUMERO_SIRAC");
					String  cs_vobo_if = (rsTraeIF.getString("CS_VOBO_IF") == null) ? "" : rsTraeIF.getString("CS_VOBO_IF");
					String clave_pyme = (rsTraeIF.getString("IC_PYME") == null) ? "" : rsTraeIF.getString("IC_PYME");
					String nombre_pyme = (rsTraeIF.getString("NOMBRE_PYME") == null) ? "" : rsTraeIF.getString("NOMBRE_PYME");
					String nombre_moneda = (rsTraeIF.getString("NOMBRE_MONEDA") == null) ? "" : rsTraeIF.getString("NOMBRE_MONEDA");
					String banco_servicio = (rsTraeIF.getString("BANCO_SERVICIO") == null) ? "" : rsTraeIF.getString("BANCO_SERVICIO");
					String no_cuenta = (rsTraeIF.getString("NO_CUENTA") == null) ? "" : rsTraeIF.getString("NO_CUENTA");
					String cuenta_clabe = (rsTraeIF.getString("CUENTA_CLABE") == null) ? "" : rsTraeIF.getString("CUENTA_CLABE");
					String sucursal = (rsTraeIF.getString("SUCURSAL") == null) ? "" : rsTraeIF.getString("SUCURSAL");
					String nombre_epo = (rsTraeIF.getString("NOMBRE_EPO") == null) ? "" : rsTraeIF.getString("NOMBRE_EPO");
					String rfc = (rsTraeIF.getString("RFC") == null) ? "" : rsTraeIF.getString("RFC");
					String plaza = (rsTraeIF.getString("PLAZA") == null) ? "" : rsTraeIF.getString("PLAZA");
					String opera_convenio_unico = (rsTraeIF.getString("OPERA_CONVENIO_UNICO") == null) ? "" : rsTraeIF.getString("OPERA_CONVENIO_UNICO");
					String fecha_firma_auto = (rsTraeIF.getString("FECHA_FIRMA_AUTO") == null) ? "" : rsTraeIF.getString("FECHA_FIRMA_AUTO");
					String fecha_param_cu = (rsTraeIF.getString("FECHA_PARAM_CU") == null) ? "" : rsTraeIF.getString("FECHA_PARAM_CU");
					String num_nafin_elec = (rsTraeIF.getString("NUM_NAFIN_ELEC") == null) ? "" : rsTraeIF.getString("NUM_NAFIN_ELEC");
					String prioridad = (rsX.getString("PRIORIDAD") == null) ? "" : rsX.getString("PRIORIDAD");
					String ic_cuenta_bancaria = (rsTraeIF.getString("IC_CUENTA_BANCARIA") == null) ? "" : rsTraeIF.getString("IC_CUENTA_BANCARIA");
					String ic_epo = (rsTraeIF.getString("IC_EPO") == null) ? "" : rsTraeIF.getString("IC_EPO");
					String clave_if = (rsTraeIF.getString("IC_IF") == null) ? "" : rsTraeIF.getString("IC_IF");
					String cs_cambio_cuenta = (rsTraeIF.getString("CS_CAMBIO_CUENTA") == null) ? "" : rsTraeIF.getString("CS_CAMBIO_CUENTA");
					String ic_moneda = (rsTraeIF.getString("IC_MONEDA") == null) ? "" : rsTraeIF.getString("IC_MONEDA");


					if(prioridad.equals("S")) { 	prioridad = "Si";
					}	else {	 		prioridad = "";	 	}

					info = new HashMap();
					info.put("CS_VOBO_IF",cs_vobo_if);
					info.put("IC_PYME",clave_pyme);
					info.put("IC_EPO",ic_epo);
					info.put("IC_IF",clave_if);
					info.put("IC_CUENTA_BANCARIA",ic_cuenta_bancaria);
					info.put("NUMERO_SIRAC",num_sirac);
					info.put("NOMBRE_PYME",nombre_pyme);
					info.put("NOMBRE_MONEDA",nombre_moneda);
					info.put("BANCO_SERVICIO",banco_servicio);
					info.put("NO_CUENTA",no_cuenta);
					info.put("CUENTA_CLABE", cuenta_clabe);
					info.put("SUCURSAL",sucursal);
					info.put("NOMBRE_EPO",nombre_epo);
					info.put("RFC",rfc);
					info.put("PLAZA",plaza);
					info.put("SELECCIONAR","");
					info.put("OPERA_CONVENIO_UNICO",opera_convenio_unico);
					info.put("FECHA_FIRMA_AUTO",fecha_firma_auto);
					info.put("FECHA_PARAM_CU",fecha_param_cu);
					info.put("NUM_NAFIN_ELEC",num_nafin_elec);
					info.put("PRIORIDAD",prioridad);
					info.put("CS_CAMBIO_CUENTA",cs_cambio_cuenta);
					info.put("IC_MONEDA",ic_moneda);
					registros.add(info);

				}
			}
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("consAutotizacion (S)");
		}
			return consulta;
	}



	/** generaci�n de Archivo de consulta de la pantalla autorizaci�n Cuentas Bancarias PYMEs  */
 	public String    archivoConsAutotizacion(List parametros){
		log.info("archivoConsAutotizacion (E)");

		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String icpyme ="", respyme ="", resEpo ="", vpyme ="";
		HashMap	info = new HashMap();
		ResultSet	rsX;
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		String  ic_if =  parametros.get(0).toString();
		String chkPymesConvenio = parametros.get(1).toString();
		String nafinelect  = parametros.get(2).toString();
		String cboEPO  = parametros.get(3).toString();
		String ic_pyme = parametros.get(4).toString();
		String desde = parametros.get(5).toString();
		String hasta = parametros.get(6).toString();
		String sIfConvenioUnico = parametros.get(7).toString();
		String ruta = parametros.get(8).toString();


		try{
			con.conexionDB();

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(
				" SELECT  RELPYMEIF.CS_VOBO_IF as CS_VOBO_IF , "+
				" pyme.in_numero_sirac as NUMERO_SIRAC ,"+
				"  RELCTABANC.IC_CUENTA_BANCARIA  , " +
				" decode (PYME.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '|| PYME.CG_RAZON_SOCIAL as  NOMBRE_PYME, "+
				" RELCTABANC.CG_BANCO as BANCO_SERVICIO , " +
				" RELCTABANC.CG_NUMERO_CUENTA as  NO_CUENTA ,"+
				" M.CD_NOMBRE as NOMBRE_MONEDA , "+
				" RELPYMEIF.IC_EPO ,"+
				" PYME.IC_PYME as IC_PYME , "+
				" RELCTABANC.CG_SUCURSAL as SUCURSAL ,"+
				" EPO.cg_razon_social  as NOMBRE_EPO,  " +
				" PYME.CG_RFC as RFC , "+
				" nvl(PLAZA.CD_DESCRIPCION,'no tiene') as PLAZA,  " +
				" relctabanc.CG_CUENTA_CLABE as CUENTA_CLABE ,  " +
				" TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') FECHA_FIRMA_AUTO,  " +
				"  TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM_CU,  " +
				" DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO,  " +
				" crn.ic_nafin_electronico NUM_NAFIN_ELEC " +//FODEA 018 - 2009 ACF
				" ,	DECODE (RELPYMEIF.cs_cambio_cuenta, 'S', 'Cambio de Cuenta' )as cs_cambio_cuenta "+
				" FROM COMCAT_MONEDA M, COMCAT_PYME PYME, " +
				" COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF,comcat_epo EPO " +
				" ,    COMCAT_PLAZA PLAZA "+
				"       , comrel_pyme_epo pe "+
				"       , comrel_nafin crn "+//FODEA 018 - 2009 ACF
				" WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
				" AND PYME.IC_PYME = RELCTABANC.IC_PYME AND RELCTABANC.IC_MONEDA = M.IC_MONEDA AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
				"      AND RELCTABANC.IC_PLAZA = PLAZA.IC_PLAZA(+) " +
				" AND RELPYMEIF.IC_IF = " + ic_if +
				" AND pyme.ic_pyme = crn.ic_epo_pyme_if" +//FODEA 018 - 2009 ACF
				" AND crn.cg_tipo = 'P'" +//FODEA 018 - 2009 ACF
				" AND RELPYMEIF.CS_BORRADO='N' " +
				" AND pe.ic_pyme = PYME.IC_PYME " +
				" AND RELPYMEIF.IC_EPO = PE.IC_EPO "+
				" AND RELPYMEIF.CS_VOBO_IF = 'N' "+
				" AND PYME.IC_PYME  = ? "+
				" AND RELPYMEIF.IC_EPO = ? "+
				" AND pe.IC_EPO =  ? ");

			if(chkPymesConvenio.equals("S")) {
				sbQuery.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");
			}
			sbQuery.append(" ORDER BY 3, 4 ");

			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(sbQuery.toString());


			if (!nafinelect.equals("")){
				String qrySentenciaS =
					" SELECT ic_epo_pyme_if as pyme, p.cs_expediente_efile, p.cg_rfc " +
					" FROM comrel_nafin n " +
					" , comcat_pyme p " +
					" WHERE n.cg_tipo = 'P' " +
					" AND n.ic_epo_pyme_if = p.ic_pyme " +
					" AND n.ic_nafin_electronico=  " + nafinelect ;
            rs = con.queryDB(qrySentenciaS);
				if(rs.next()){
					icpyme = rs.getString("PYME");
               if ("N".equals(rs.getString("CS_EXPEDIENTE_EFILE"))) {
						if(tieneExpedienteEnEfile(rs.getString("CG_RFC"))){
							this.setEstatusDelExpedienteEfileEnTablaPyme(rs.getString("CG_RFC"),true);
                  }
                }
				}
			}
         con.cierraStatement();

			String qrySentenciaX =
						"SELECT ST.IC_PYME, ST.CG_RAZON_SOCIAL, ST.IC_EPO, ST.IN_NUMERO_SIRAC, ST.CG_RFC, NVL(VP.CS_DOC_VIGENTES,'N') as PRIORIDAD  " +
						" FROM vm_pymes_doc_vig VP, (SELECT distinct(PYME.IC_PYME) AS IC_PYME,  " +
						"		decode (PYME.cs_habilitado,'N','*','S',' ')||' '||PYME.CG_RAZON_SOCIAL AS CG_RAZON_SOCIAL,  " +
						"		RELPYMEIF.ic_epo AS IC_EPO,  " +
						"   	PYME.in_numero_sirac AS in_numero_sirac,  " +
						"		PYME.cg_rfc AS cg_rfc, " +
						"   	'15cadenas15mantenimiento15parametrizacion15if15admparamifpyme.jsp' as PANTALLA " +
						"		FROM COMCAT_MONEDA M, " +
						"		COMCAT_PYME PYME, " +
						"		COMREL_CUENTA_BANCARIA RELCTABANC, " +
						"		COMREL_PYME_IF RELPYMEIF, " +
						"		comcat_epo EPO " +
						"     , comrel_pyme_epo pe "+
						"		WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
						"		AND PYME.IC_PYME = RELCTABANC.IC_PYME " +
						"		AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
						"		AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
						"		AND EPO.cs_habilitado = 'S' " +
						"		AND RELPYMEIF.CS_VOBO_IF ='N' "+
						"		AND RELPYMEIF.IC_IF = " + ic_if +
						"		AND RELPYMEIF.CS_BORRADO='N' " +
						"		AND RELPYMEIF.CS_ESTATUS IS NULL " +
						"		AND pe.ic_pyme = PYME.IC_PYME " +
					   "   AND PYME.in_numero_sirac IS NOT NULL";

			if(!nafinelect.equals("") && !icpyme.equals("")) {
				qrySentenciaX += " AND PYME.IC_PYME  = " + icpyme;
			} else if (!nafinelect.equals("") ){
				qrySentenciaX += " AND PYME.IC_PYME  = 0";
			}
			if(!cboEPO.equals("")) {
				if(!cboEPO.equals("0")) {
					qrySentenciaX += " AND RELPYMEIF.IC_EPO = " + cboEPO;
					qrySentenciaX += "	  AND pe.IC_EPO = " + cboEPO;
				}
				if(!ic_pyme.equals("") &&  !ic_pyme.equals("0") )
					qrySentenciaX += " AND PYME.IC_PYME  = " + ic_pyme;
			}
			if(!desde.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if >= TO_DATE('"+desde+"', 'dd/mm/yyyy') ";
			}
			if(!hasta.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if <  TO_DATE('"+hasta+"', 'dd/mm/yyyy')+1 ";
			}
			qrySentenciaX += " AND PYME.CS_EXPEDIENTE_EFILE = 'S' ";
			qrySentenciaX += " ) ST " +
									" WHERE ST.IC_EPO = VP.IC_EPO(+) " +
									" AND ST.IC_PYME = VP.IC_PYME(+) " +
									"ORDER BY 2 ";

			rsX = con.queryDB(qrySentenciaX);


			 while (rsX.next()){
				respyme= rsX.getString(1);
				resEpo= rsX.getString(3);
				int numeroPyme=Integer.parseInt(respyme);
				int numeroEpo=Integer.parseInt(resEpo);
				pstmt.setInt(1,numeroPyme);
				pstmt.setInt(2,numeroEpo);
				pstmt.setInt(3,numeroEpo);

				ResultSet rsTraeIF = pstmt.executeQuery();
				pstmt.clearParameters();
				vpyme =	((rsX.getString(2)==null)?"":rsX.getString(2).replace(',',' '))  ;

				contenidoArchivo.append("PyME,MONEDA,BANCO DE SERVICIO,NO. DE CUENTA,");
				if(sIfConvenioUnico.equals("S")){ 	contenidoArchivo.append("CUENTA CLABE,"); }
				contenidoArchivo.append("SUCURSAL,EPO,RFC,PLAZA, ");
				contenidoArchivo.append("AUTORIZACI�N IF,");

				if(sIfConvenioUnico.equals("S")){
					contenidoArchivo.append(",PYME OPERA CON CONVENIO �NICO, FECHA DE FIRMA AUT�GRAFA DE CU.,FECHA PARAMETRIZACI�N AUTOM�TICA CU");
				}
				contenidoArchivo.append(",N�mero SIRAC");
				contenidoArchivo.append(",N�m. Nafin Electr�nico");
				contenidoArchivo.append(",Prioridad");
				contenidoArchivo.append("\n");


				while(rsTraeIF.next()){
					info = new HashMap();
					String num_sirac  = (rsTraeIF.getString("NUMERO_SIRAC") == null) ? "" : rsTraeIF.getString("NUMERO_SIRAC");
					//String  cs_vobo_if = (rsTraeIF.getString("CS_VOBO_IF") == null) ? "" : rsTraeIF.getString("CS_VOBO_IF");
					//String clave_pyme = (rsTraeIF.getString("IC_PYME") == null) ? "" : rsTraeIF.getString("IC_PYME");
					String nombre_pyme = (rsTraeIF.getString("NOMBRE_PYME") == null) ? "" : rsTraeIF.getString("NOMBRE_PYME");
					String nombre_moneda = (rsTraeIF.getString("NOMBRE_MONEDA") == null) ? "" : rsTraeIF.getString("NOMBRE_MONEDA");
					String banco_servicio = (rsTraeIF.getString("BANCO_SERVICIO") == null) ? "" : rsTraeIF.getString("BANCO_SERVICIO");
					String no_cuenta = (rsTraeIF.getString("NO_CUENTA") == null) ? "" : rsTraeIF.getString("NO_CUENTA");
					String cuenta_clabe = (rsTraeIF.getString("CUENTA_CLABE") == null) ? "" : rsTraeIF.getString("CUENTA_CLABE");
					String sucursal = (rsTraeIF.getString("SUCURSAL") == null) ? "" : rsTraeIF.getString("SUCURSAL");
					String nombre_epo = (rsTraeIF.getString("NOMBRE_EPO") == null) ? "" : rsTraeIF.getString("NOMBRE_EPO");
					String rfc = (rsTraeIF.getString("RFC") == null) ? "" : rsTraeIF.getString("RFC");
					String plaza = (rsTraeIF.getString("PLAZA") == null) ? "" : rsTraeIF.getString("PLAZA");
					String opera_convenio_unico = (rsTraeIF.getString("OPERA_CONVENIO_UNICO") == null) ? "" : rsTraeIF.getString("OPERA_CONVENIO_UNICO");
					String fecha_firma_auto = (rsTraeIF.getString("FECHA_FIRMA_AUTO") == null) ? "" : rsTraeIF.getString("FECHA_FIRMA_AUTO");
					String fecha_param_cu = (rsTraeIF.getString("FECHA_PARAM_CU") == null) ? "" : rsTraeIF.getString("FECHA_PARAM_CU");
					String num_nafin_elec = (rsTraeIF.getString("NUM_NAFIN_ELEC") == null) ? "" : rsTraeIF.getString("NUM_NAFIN_ELEC");
					String prioridad = (rsX.getString("PRIORIDAD") == null) ? "" : rsX.getString("PRIORIDAD");
					String  cs_cambio_cuenta = (rsTraeIF.getString("cs_cambio_cuenta") == null) ? "" : rsTraeIF.getString("cs_cambio_cuenta");
					if(prioridad.equals("S")) {
						prioridad = "Si";
					}	else {
						prioridad = "";
					}

					contenidoArchivo.append(nombre_pyme.replaceAll(",","") +", ");
					contenidoArchivo.append(nombre_moneda.replaceAll(",","") +", ");
					contenidoArchivo.append(banco_servicio.replaceAll(",","") +", ");
					contenidoArchivo.append(no_cuenta.replaceAll(",","") +", ");
					if(sIfConvenioUnico.equals("S")){
						if(!cuenta_clabe.equals("")){
							contenidoArchivo.append("'"+cuenta_clabe.replaceAll(",","") +"',");
						}else {
							contenidoArchivo.append(",");
						}
					}
					contenidoArchivo.append(sucursal.replaceAll(",","") +", ");
					contenidoArchivo.append(nombre_epo.replaceAll(",","") +", ");
					contenidoArchivo.append(rfc.replaceAll(",","") +", ");
					contenidoArchivo.append(plaza.replaceAll(",","") +", ");
					contenidoArchivo.append(cs_cambio_cuenta.replaceAll(",","") +", ");

					if(sIfConvenioUnico.equals("S")){
					contenidoArchivo.append(opera_convenio_unico.replaceAll(",","") +", ");
					contenidoArchivo.append(fecha_firma_auto.replaceAll(",","") +", ");
					contenidoArchivo.append(fecha_param_cu.replaceAll(",","") +", ");
					}
					contenidoArchivo.append(num_sirac.replaceAll(",","") +", ");
					contenidoArchivo.append(num_nafin_elec.replaceAll(",","") +", ");

					contenidoArchivo.append(prioridad.replaceAll(",","") +", ");
					contenidoArchivo.append("\n");

				}
				contenidoArchivo.append(",\n");
			}

			creaArchivo.make(contenidoArchivo.toString(), ruta, ".csv");
			nombreArchivo = creaArchivo.getNombre();


		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("archivoConsAutotizacion (S)");
		}
			return nombreArchivo;
	}

	/** metodoque  actualiza el registro seleccionado
	de la pantalla Cuentas Bancarias PYMEs  -  Autorizaci�n  */

	public String  autorizacionCuenta(String  chkCtaBanc[] ,		String sIfConvenioUnico,
												String icIfActual, 	String strLogin , String iNoNafinElectronico,
												String strNombreUsuario, 	String strNombre ){
		log.info("autorizacionCuenta (E)");

		AccesoDB con =new AccesoDB();

		String qryRelIFPYMEOk = "", SQLExec = "", qry = "", eliminaRegistroComhis_cambios_cta = "";
		boolean bOk = false;
		String iCta = "", iEPO = "", iPyme = "", sPymeConvUnic = "", ic_moneda ="", nombre_moneda ="", orden ="",
				nombreEpo = "", nombrePyme="", nun_elecPyme ="";

		VectorTokenizer vtCtaEPOPyme = null;
		Vector vCtaEPOPyme = null;
		JSONArray registros = new JSONArray();
		HashMap info = new HashMap();
		String 	consulta ="";
		List epoFactV = new ArrayList();
		List tipoFactoraje = new ArrayList();
		Hashtable alParamEPO1 = new Hashtable();
		StringBuffer sepoS = new StringBuffer();

		try{
			con.conexionDB();

			UtilUsr utils = new UtilUsr();

			//PRIMERO  IDENTIFICO SI ALGUNA EPO ESTA PARAMETRIZADA CON FACTORAJE VENCIDO
			for(int i=0;i<chkCtaBanc.length;i++) {
				vtCtaEPOPyme = new VectorTokenizer(chkCtaBanc[i],"|");
				vCtaEPOPyme = vtCtaEPOPyme.getValuesVector();
				iEPO = (vCtaEPOPyme.size()>= 2)?(String)vCtaEPOPyme.get(1):"0";
				alParamEPO1 = new Hashtable();
				alParamEPO1 = this.getParametrosEPO(iEPO,1);
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
				if(bOperaFactorajeVencido.equals("S"))  {
					epoFactV.add(iEPO);
				}
				sepoS.append(iEPO+",");
			}
			sepoS.deleteCharAt(sepoS.length()-1);


			for(int i=0;i<chkCtaBanc.length;i++) {
				vtCtaEPOPyme = new VectorTokenizer(chkCtaBanc[i],"|");
				vCtaEPOPyme = vtCtaEPOPyme.getValuesVector();

				try{
					iCta = (vCtaEPOPyme.size() >= 1)?(String)vCtaEPOPyme.get(0):"0";
					iEPO = (vCtaEPOPyme.size()>= 2)?(String)vCtaEPOPyme.get(1):"0";
					iPyme = (vCtaEPOPyme.size() >= 3)?(String)vCtaEPOPyme.get(2):"0";
					sPymeConvUnic = (vCtaEPOPyme.size()  >= 4)?(String)vCtaEPOPyme.get(3):"0";
					ic_moneda = (vCtaEPOPyme.size()  >= 5)?(String)vCtaEPOPyme.get(4):"0";
					nombre_moneda  = (vCtaEPOPyme.size()  >= 6)?(String)vCtaEPOPyme.get(5):"0";
					nombreEpo = (vCtaEPOPyme.size()  >= 7)?(String)vCtaEPOPyme.get(6):"0";
					nombrePyme = (vCtaEPOPyme.size()  >= 8)?(String)vCtaEPOPyme.get(7):"0";
					nun_elecPyme =  (vCtaEPOPyme.size()  >= 9)?(String)vCtaEPOPyme.get(8):"0";

				} catch(ArrayIndexOutOfBoundsException aiobe){
					System.out.println(aiobe);
				}
				vCtaEPOPyme.removeAllElements();

				//validacion de si la pyme es nueva o vieja
				StringBuffer PymeNuevaquery = new StringBuffer();
				ArrayList varBindPymeNueva = new ArrayList();
				PreparedStatement pstPymNueva = null;
				ResultSet 				rstPymNueva = null;
				int contadorPymeNuev = 0;
				PymeNuevaquery.append(" SELECT COUNT (1) " +
													"  FROM comrel_pyme_epo " +
													" WHERE cs_aceptacion = ? " +
													"  AND ic_pyme = ?");
				varBindPymeNueva.add("H");

				log.debug("PymeNuevaquery::  "+PymeNuevaquery);
				log.debug("varBindPymeNueva::  "+varBindPymeNueva);

				varBindPymeNueva.add(new Long(iPyme));
				pstPymNueva = con.queryPrecompilado(PymeNuevaquery.toString(), varBindPymeNueva);
				rstPymNueva = pstPymNueva.executeQuery();
				if(rstPymNueva.next()){
					contadorPymeNuev = rstPymNueva.getInt(1);
				}

				log.debug("contadorPymeNuev::  "+contadorPymeNuev);
				//sessionvalidacion de si la pyme es nueva o vieja
				// fodea 034
				qry =	" SELECT NVL(cs_cambio_cuenta,'N') FROM comrel_pyme_if " +
						" WHERE ic_if = "+icIfActual+" AND ic_epo = "+iEPO+" AND ic_cuenta_bancaria = "+iCta;
				ResultSet rs = con.queryDB(qry);
				String dfvoboif = ", df_vobo_if = SYSDATE";
				if(rs.next()){
					if(rs.getString(1).equals("S")){
						dfvoboif = "";
					}
				}
				String relacionConvenioUnico = "";
				if (sIfConvenioUnico.equals("S") && sPymeConvUnic.equals("SI")){
					relacionConvenioUnico = " , CS_CONVENIO_UNICO = 'S' ";
				}
				qryRelIFPYMEOk = " UPDATE comrel_pyme_if SET cs_vobo_if = 'S' "+dfvoboif + relacionConvenioUnico +
						 			  " WHERE ic_if = " + icIfActual + " AND ic_epo = " + iEPO +
									  " AND ic_cuenta_bancaria = " + iCta;

				SQLExec = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H', " +
							" df_aceptacion = SYSDATE WHERE ic_epo  ="+iEPO+
							" AND ic_pyme = " + iPyme;

				eliminaRegistroComhis_cambios_cta = "UPDATE COMHIS_CAMBIOS_CTA SET CS_AUTORIZA_IF = 'S' " +
																" WHERE IC_IF = " + icIfActual + " " +
																" AND IC_EPO = " + iEPO + " " +
																" AND IC_CUENTA_BANCARIA = '" + iCta + "'";


				//VALIDAR PARA METER EN BITACORA
				//validar que esa relacion no exista en la bitacora anteriormente
				StringBuffer ValBitquery = new StringBuffer();
				ArrayList varValBitNueva = new ArrayList();
				PreparedStatement pstValBit = null;
				ResultSet 				rstValBit = null;
				//este contador indicara si ya hubo un guardado de esa misma relacion anteriormente,
				//puede ser que la cuenta se borre y entonces vendra una segunda autorizacion,
				//asi que con esto se valida no mas insert con misma pyme/if en bitacora ni actualizacion de fecha.
				int contadorValBit = 0;
				ValBitquery.append(" SELECT COUNT (1) " +
													"  FROM BIT_CONVENIO_UNICO " +
													" WHERE IC_PYME  = ? " +
													"  AND IC_IF = ?  ");
				varValBitNueva.add(new Long(iPyme));
				varValBitNueva.add(new Long(icIfActual));
				log.debug("ValBitquery.toString()::  "+ValBitquery.toString());
				log.debug("varValBitNueva::  "+varValBitNueva);
				pstValBit = con.queryPrecompilado(ValBitquery.toString(), varValBitNueva);
				rstValBit = pstValBit.executeQuery();

				if(rstValBit.next()){
					contadorValBit = rstValBit.getInt(1);
				}

				if(rstValBit!=null) rstValBit.close();
				if(pstValBit!=null) pstValBit.close();

				//SACANDO EL CONVENIO UNICO DE LA PYME
				StringBuffer strSQL = new StringBuffer();
				List varBind = new ArrayList();
				ResultSet 				rstCU = null;
				PreparedStatement pstCU = null;

				log.debug("sIfConvenioUnico::  "+sIfConvenioUnico);
				log.debug("sPymeConvUnic::  "+sPymeConvUnic);
				log.debug("contadorValBit::  "+contadorValBit);
				log.debug("contadorPymeNuev::  "+contadorPymeNuev);

				//Si los dos manejan convenio unico
				if (sIfConvenioUnico.equals("S") && sPymeConvUnic.equals("SI") && contadorValBit==0){
					//La validacion de este contador si la pyme es nueva o vieja se hace antes de actualizar su cs_aceptacion a H
					if(contadorPymeNuev==0){
						//LA PYME ES NUEVA SI EL CONTADOR ES 0
						log.debug("LA PYME ES NUEVA SI EL CONTADOR ES 0  ");
						// a)
						System.out.println("inciso A : ");
						StringBuffer insertBitacora = new StringBuffer();
						insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
													"( IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO  ) " +
													"VALUES ( " +icIfActual+ ","+iPyme+ ",SYSDATE,SYSDATE,'"+strLogin+ "') ");
						log.debug("insertBitacora : "+insertBitacora.toString());
						con.ejecutaSQL(insertBitacora.toString());

					}else{
						log.debug("SI EL CONTADOR NO ES CERO SE TRATA DE UNA PYME QUE ES VIEJA  ");
						//SI EL CONTADOR NO ES CERO SE TRATA DE UNA PYME QUE ES VIEJA
						//validar si es la primera vez de esa pyme en la bitacora.
						int contadorExisteEnCU = 0;
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append("SELECT COUNT(1) " +
										  "  FROM bit_convenio_unico " +
										  " WHERE ic_pyme = ? ");
						varBind.add(new Long(iPyme));
						pstCU = con.queryPrecompilado(strSQL.toString(), varBind);
						rstCU = pstCU.executeQuery();
						if(rstCU.next()){
							contadorExisteEnCU = rstCU.getInt(1);
						}

						if(contadorExisteEnCU==0){
						//VALIDANDO SI YA HAY BITACORA DE ESA RELACION
							//NO HAY BITACORA
							// B)
							System.out.println("inciso B : ");
							StringBuffer insertBitacora = new StringBuffer();
							insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
														 "(  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO  )"+
														 " VALUES  (" +icIfActual+ ","+iPyme+ ",SYSDATE,TO_DATE((SELECT TO_CHAR(DF_FIRMA_CONV_UNICO,'DD/MM/YYYY hh24:mi:ss') FROM COMCAT_PYME WHERE IC_PYME = "+iPyme+"),'DD/MM/YYYY hh24:mi:ss'),'"+strLogin+ "') ");
							log.debug("insertBitacora : "+insertBitacora.toString());
							con.ejecutaSQL(insertBitacora.toString());

						}	else{
							//SI HAY BITACORA
							//RESPETAR FECHA PRIMER IF
							// c) Actualiza la fecha
							log.debug("inciso C : ");
							StringBuffer insertBitacora = new StringBuffer();
							insertBitacora.append("INSERT INTO BIT_CONVENIO_UNICO " +
														 "(  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO  )"+
														 " VALUES ( " +icIfActual+ ","+iPyme+ ",SYSDATE,TO_DATE((SELECT TO_CHAR(MIN(DF_ACEPTACION_PYME),'DD/MM/YYYY hh24:mi:ss') FROM BIT_CONVENIO_UNICO WHERE IC_PYME = "+iPyme+"),'DD/MM/YYYY hh24:mi:ss'),'"+strLogin+ "') ");
							log.debug("insertBitacora : "+insertBitacora.toString());
							con.ejecutaSQL(insertBitacora.toString());

						} //fin de else si hay relacion, respetar fecha

						if(rstPymNueva!=null) rstPymNueva.close();
						if(pstPymNueva!=null) pstPymNueva.close();

					} // fin de if count

					if(rstCU!=null) rstCU.close();
					if(pstCU!=null) pstCU.close();

				} //if (sIfConvenioUnico.equals("S") && iPymeConvenioUnico.equals("S")){

				try{

					con.ejecutaSQL(qryRelIFPYMEOk);
					con.ejecutaSQL(SQLExec);
					con.ejecutaSQL(eliminaRegistroComhis_cambios_cta);
					log.debug("qryRelIFPYMEOk : "+qryRelIFPYMEOk);
					log.debug("SQLExec : "+SQLExec);
					log.debug("eliminaRegistroComhis_cambios_cta : "+eliminaRegistroComhis_cambios_cta);

					/* F056-2005
					 * El cambio de perfil a la pyme se realiza en el momento que se marcan los requisitos 7 y 8
					*/
					List lstLogin = utils.getUsuariosxAfiliado(iPyme, "P");
					if(!lstLogin.isEmpty()) {	//Si tiene una cuenta ya existente en el OID de -----N@E-----
						String loginUsuario = lstLogin.get(0).toString();
						utils.setPerfilUsuario(loginUsuario, "ADMIN PYME");
					} else {
						//NO existe la cuenta en el OID de N@E.
						if (utils.getExisteSolicitudAltaUsuario(iPyme, "P")) {
						//Si no existe la cuenta en el OID pero ya hay una solicitud de alta de usuario en el ARGUS, ajusta el perfil
						utils.setPerfilSolicitudAltaUsuario(iPyme, "P", "ADMIN PYME");
						} else {
							//No existe la cuenta de N@E en OID y no hay solicitud de alta de usuario						SolicitudArgus solicitud = new SolicitudArgus();

							Usuario usr = new Usuario();
							SolicitudArgus solicitud = new SolicitudArgus();
							usr.setClaveAfiliado(iPyme);
							usr.setTipoAfiliado("P");
							solicitud.setUsuario(usr);
							String loginAsociado = utils.getCuentaUsuarioAsociada(iPyme);
							if (loginAsociado != null && !loginAsociado.equals("") ) {
								//Existe una cuenta (del Sistema de compras) en el OID que
								//est� preasociada al proveedor, solo se le agregan el perfil, tipo de afiliado
								//y clave de afiliado a la cuenta de manera que pueda ingresar a N@E.
								usr.setLogin(loginAsociado);
								utils.actualizarUsuario(solicitud);
								//La actualizacion del usuario no contempla la actualizacion del perfil.
								//por lo cual se hace en una instrucci�n aparte.
								utils.setPerfilUsuario(loginAsociado, "ADMIN PYME");
							} else {
								//Si no hay cuenta ni de N@E ni compras que pueda usarse, se genera la solicitud
								usr.setPerfil("ADMIN PYME");
								utils.complementarSolicitudArgus(solicitud, "P", iPyme);

								Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

								Registros reg = beanAfiliacion.getDatosContacto(solicitud.getRFC(), "");
								if (reg.next()) {
									usr.setNombre(reg.getString("contacto_nombre"));
									usr.setApellidoPaterno(reg.getString("contacto_appat"));
									usr.setApellidoMaterno(reg.getString("contacto_apmat"));
									usr.setEmail(reg.getString("contacto_email"));
								}
								utils.registrarSolicitudAltaUsuario(solicitud);
							}
						}
					}
					//Fin F056-2005
					//FODEA-036 MEJORAS II
					StringBuffer qryNEif = new StringBuffer();
					ResultSet	rsNEif = null;
					PreparedStatement	psNEif = null;
					List variableBind = new ArrayList();
					String nafElecIF = "";
					qryNEif = new StringBuffer();
					variableBind = new ArrayList();
					qryNEif.append( " SELECT ic_nafin_electronico  " +
											"   FROM comrel_nafin  " +
											"  WHERE ic_epo_pyme_if = ? " );
					//variableBind.add(new Long(icIfActual));
					variableBind.add(new Long(iPyme));
					log.debug("qryNEif.toString()::  "+qryNEif.toString());
					log.debug("variableBind::  "+variableBind);
					psNEif = con.queryPrecompilado(qryNEif.toString(), variableBind);
					rsNEif = psNEif.executeQuery();
					if(rsNEif.next()){
						nafElecIF = rsNEif.getString(1);
					}
					if(psNEif!=null) psNEif.close();
					if(rsNEif!=null) rsNEif.close();
					StringBuffer insertBitacoraGral = new StringBuffer();
					insertBitacoraGral.append(
						"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
						"\n IC_CAMBIO,  " +
						"\n CC_PANTALLA_BITACORA,  " +
						"\n CG_CLAVE_AFILIADO, " +
						"\n IC_NAFIN_ELECTRONICO, " +
						"\n DF_CAMBIO, " +
						"\n IC_USUARIO, " +
						"\n CG_NOMBRE_USUARIO, " +
						"\n CG_ANTERIOR, " +
						"\n CG_ACTUAL, " +
						"\n IC_GRUPO_EPO, " +
						"\n IC_EPO ) " +
						"\n VALUES(  " +
						"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
						"\n 'CUENTABANCIF', " +
						"\n 'P', " +
						"\n '"+iNoNafinElectronico+"', " +
						"\n SYSDATE, " +
						"\n '"+strLogin+"',  " +
						"\n '"+strNombreUsuario+"', " +
						"\n '',  " +
						"\n 'Cuenta Autorizada = "+strNombre+" "+nafElecIF+" ', " +
						"\n '', " +
						"\n '' ) " );

					log.debug("insertBitacoraGral : "+insertBitacoraGral.toString());
					con.ejecutaSQL(insertBitacoraGral.toString());
					//PINTA ACUSE
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					ResultSet rst2 = null;
					PreparedStatement pst2 = null;
					strSQL = new StringBuffer();
					varBind = new ArrayList();


		//*******INICIA ************** FODEA DESCUENTO AUTOMATICO***************************

				alParamEPO1 = new Hashtable();
				alParamEPO1 = this.getParametrosEPO(iEPO,1);
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
				String nombreFactoraje ="";
				tipoFactoraje = new ArrayList();
				tipoFactoraje.add("N");// Factoraje Normal
				if(bOperaFactorajeVencido.equals("S"))  {
					tipoFactoraje.add("V"); // Factoraje Vencido
				}

				StringBuffer SQL	= 	new StringBuffer();
				SQL.append("	INSERT INTO COMREL_DSCTO_AUT_PYME	");
				SQL.append("		(											");
				SQL.append("			IC_PYME								");
				SQL.append("			,IC_EPO								");
				SQL.append("			,IC_IF								");
				SQL.append("			,IC_MONEDA							");
				SQL.append("			,CS_ORDEN							");
				SQL.append("			,CS_DSCTO_AUTOMATICO_DIA		");
				SQL.append("			,CS_DSCTO_AUTOMATICO_PROC		");
				SQL.append("			,CC_TIPO_FACTORAJE				");
				SQL.append("			,IC_USUARIO							");
				SQL.append("			,CG_NOMBRE_USUARIO				");
				SQL.append("			,CC_ACUSE							");
				SQL.append("			,DF_FECHAHORA_OPER				");
				SQL.append("		)											");
				SQL.append("		VALUES (									");
				SQL.append("			?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, SYSDATE    ) ");

				insertBitacoraGral = new StringBuffer();
				insertBitacoraGral.append(
								"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
								"\n IC_CAMBIO,  " +
								"\n CC_PANTALLA_BITACORA,  " +
								"\n CG_CLAVE_AFILIADO, " +
								"\n IC_NAFIN_ELECTRONICO, " +
								"\n DF_CAMBIO, " +
								"\n IC_USUARIO, " +
								"\n CG_NOMBRE_USUARIO, " +
								"\n CG_ANTERIOR, " +
								"\n CG_ACTUAL, " +
								"\n IC_GRUPO_EPO, " +
								"\n IC_EPO ) " +
								"\n VALUES(  " +
								"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
								"\n 'DESAUTOMA', " +  // Descuento Automatico - Nueva Version
								"\n 'P', " +
								"\n  ?  , " +
								"\n SYSDATE, " +
								"\n    ? ,  " +
								"\n   ? , " +
								"\n '',  " +
								"\n   ?,  " +
								"\n '', " +
								"\n '' ) " );

					StringBuffer SQLU	= 	new StringBuffer();
					SQLU.append(" UPDATE COMCAT_PYME 				");
					SQLU.append(" SET CS_DSCTO_AUTOMATICO = 'S' ");
					SQLU.append(" WHERE IC_PYME = ? 				");

					List variables = new ArrayList();
					PreparedStatement pst		= 	null;

					log.debug("contadorPymeNuev ----> "+contadorPymeNuev);
					 boolean existeParam 	= false;
					if (contadorPymeNuev==0)  {

					variables = new ArrayList();
					variables.add(iPyme);
					pst = con.queryPrecompilado(SQLU.toString(), variables);
					pst.executeUpdate();
					pst.close();

					for(int y=0; y<tipoFactoraje.size(); y++)  {
						String mensajeOrden ="";
						String valorFact =  (String)tipoFactoraje.get(y);
						if(valorFact.equals("N"))  {  orden = "1";  nombreFactoraje = "Normal"; mensajeOrden= "Orden= "+orden+"\n"; }
						if(valorFact.equals("V"))  {  orden = "0";  nombreFactoraje = "Vencido"; }
							String actual =  mensajeOrden+
											  " Modalidad= Primer dia desde su publicacion "+"\n"+
											  " EPO="+nombreEpo+"\n"+
											  " PYME="+nombrePyme+"\n"+
											  " IF="+strNombre	+"\n"+
											  " Moneda= "+nombre_moneda+	"\n"+
										     " Pantalla= Cuentas Bancarias PyME "+"\n"+
											  " Tipo Factoraje =" +nombreFactoraje+"\n"+
											  " Descuento Autom�tico = S " +
											  " Metodo (autorizacionCuenta) " ;

						//valido si existe ya la parametrizaci�n
						existeParam= existeParametrizacion(iEPO, icIfActual, iPyme, ic_moneda, valorFact, con);
						log.debug("existeParam ---> "+existeParam);

						if(!existeParam){

							// insert en COMREL_DSCTO_AUT_PYME
							variables = new ArrayList();
							variables.add(new Integer(iPyme));	//PYME
							variables.add( new Integer(iEPO));	//EPO
							variables.add( new Integer(icIfActual)); //IC_IF
							variables.add( new Integer(ic_moneda)); //MONEDA
							variables.add(orden);						//ORDEN
							variables.add("P");							//MODALIDAD p -> primer dia...
							variables.add("S");							//S - APLICA DSCTO AUTOMATICO
							variables.add(valorFact);				//TIPO FACTIRAJE
							variables.add(strLogin);					//IC_USUARIO
							variables.add(strNombreUsuario);		//NOMBRE USUARIO
							variables.add("0");	    //ACUSE

							log.debug("SQL.toString() ----> "+SQL.toString());
							log.debug("variables ----> "+variables);

							pst = con.queryPrecompilado(SQL.toString(), variables);
							pst.executeUpdate();
							pst.close();

								//insert en BIT_CAMBIOS_GRAL
							variables = new ArrayList();
							variables.add(nun_elecPyme);	//nun_elecPyme
							variables.add( strLogin);	//EPO
							variables.add( strNombreUsuario); //IC_IF
							variables.add( actual); //MONEDA

							log.debug("insertBitacoraGral.toString() ----> "+insertBitacoraGral.toString());
							log.debug("variables ----> "+variables);

							pst = con.queryPrecompilado(insertBitacoraGral.toString(), variables);
							pst.executeUpdate();
							pst.close();



						}
					}//for
				}

				// Fodea 010-2013 - Reafiliacion Automatica
				this.paramCUDescAuto(icIfActual,  strNombre, iPyme, nombrePyme, iEPO, nombreEpo,  ic_moneda,  strLogin ,  strNombreUsuario, "Cuentas Bancarias PyME", con);

					//*******FINALIZA ************** FODEA DESCUENTO AUTOMATICO***************************


					strSQL.append(" SELECT relpymeif.cs_vobo_if as cs_vobo_if , relctabanc.ic_cuenta_bancaria,");
					strSQL.append(" 	DECODE(pyme.cs_habilitado, 'N', '*', 'S', '')||' '||pyme.cg_razon_social as NOMBRE_PYME ,");
					strSQL.append(" 	relctabanc.cg_banco as BANCO_SERVICIO  , relctabanc.cg_numero_cuenta as NO_CUENTA  , mon.cd_nombre as  MONEDA , relpymeif.ic_epo,");
					strSQL.append(" 	pyme.ic_pyme, relctabanc.cg_sucursal as SUCURSAL , epo.cg_razon_social as NOMBRE_EPO, pyme.cg_rfc as  RFC,");
					strSQL.append(" 	NVL(plaza.cd_descripcion, 'No tiene') as PLAZA ,");
					strSQL.append(" 	relctabanc.CG_CUENTA_CLABE  as CUENTA_CLABE , ");
					strSQL.append(" 	TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') as FIRMA_AUTOGRAFA_CU,  ");
					strSQL.append(" 	TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') as FIRMA_PARAM_CU, ");
					strSQL.append(" 	DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO, ");
					strSQL.append(" 	pyme.in_numero_sirac as   NUMERO_SIRAC ,  ");
					strSQL.append(" 	crn.ic_nafin_electronico as NUM_NAFIN_ELECT,  ");//FODEA 018 - 2009 ACF
					strSQL.append(" 	'15cadenas15mantenimiento15parametrizacion15if15admparamifpyme.jsp' AS pantalla");
					strSQL.append(" FROM comcat_moneda mon,");
					strSQL.append(" 	comcat_pyme pyme,");
					strSQL.append(" 	comrel_cuenta_bancaria relctabanc,");
					strSQL.append(" 	comrel_pyme_if relpymeif,");
					strSQL.append(" 	comcat_epo epo,");
					strSQL.append(" 	comcat_plaza plaza,");
					strSQL.append(" 	comrel_nafin crn,");//FODEA 018 - 2009 ACF
					strSQL.append(" 	comrel_pyme_epo pe");
					strSQL.append(" WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria");
					strSQL.append(" 	AND pyme.ic_pyme = relctabanc.ic_pyme");
					strSQL.append(" 	AND pyme.ic_pyme = crn.ic_epo_pyme_if");
					strSQL.append(" 	AND crn.cg_tipo = ?");
					strSQL.append(" 	AND relctabanc.ic_moneda = mon.ic_moneda");
					strSQL.append(" 	AND epo.ic_epo = relpymeif.ic_epo");
					strSQL.append(" 	AND relctabanc.ic_plaza = plaza.ic_plaza(+)");
					strSQL.append(" 	AND relpymeif.ic_if = ?");
					strSQL.append(" 	AND relpymeif.cs_borrado = ?");
					strSQL.append(" 	AND pe.ic_pyme = pyme.ic_pyme");
					strSQL.append(" 	AND relpymeif.ic_epo = pe.ic_epo");
					strSQL.append(" 	AND relpymeif.cs_vobo_if = ?");
					strSQL.append(" 	AND pyme.ic_pyme  = ?");
					strSQL.append(" 	AND relpymeif.ic_epo = ?");
					strSQL.append(" 	AND pe.ic_epo =  ?");
					strSQL.append(" 	AND RELCTABANC.IC_CUENTA_BANCARIA = ?");
					strSQL.append(" ORDER BY 3, 4");

					varBind.add("P");//FODEA 018 - 2009 ACF
					varBind.add(new Long(icIfActual));
					varBind.add("N");
					varBind.add("S");
					varBind.add(new Long(iPyme));
					varBind.add(new Long(iEPO));
					varBind.add(new Long(iEPO));
					varBind.add(new Long(iCta));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
				rst2 = pst2.executeQuery();

				while(rst2.next()){
					String num_sirac =rst2.getString("NUMERO_SIRAC")==null?"":rst2.getString("NUMERO_SIRAC");
					String nombre_pyme =rst2.getString("NOMBRE_PYME")==null?"":rst2.getString("NOMBRE_PYME");
					String moneda =rst2.getString("MONEDA")==null?"":rst2.getString("MONEDA");
					String banco_servicio =rst2.getString("BANCO_SERVICIO")==null?"":rst2.getString("BANCO_SERVICIO");
					String num_cuenta =rst2.getString("NO_CUENTA")==null?"":rst2.getString("NO_CUENTA");
					String cuenta_clabe =rst2.getString("CUENTA_CLABE")==null?"":rst2.getString("CUENTA_CLABE");
					String sucursal  = rst2.getString("SUCURSAL")==null?"":rst2.getString("SUCURSAL");
					String nombre_epo = rst2.getString("NOMBRE_EPO")==null?"":rst2.getString("NOMBRE_EPO");
					String rfc = rst2.getString("RFC")==null?"":rst2.getString("RFC");
					String plaza  = rst2.getString("PLAZA")==null?"":rst2.getString("PLAZA");
					String autorizacionIF = "SI";
					String usuario = strNombreUsuario;
					String operac_convenio = rst2.getString("OPERA_CONVENIO_UNICO")==null?"":rst2.getString("OPERA_CONVENIO_UNICO");
					String fechaFirmaAutoCU = rst2.getString("FIRMA_AUTOGRAFA_CU")==null?"":rst2.getString("FIRMA_AUTOGRAFA_CU");
					String fechaparamCU =rst2.getString("FIRMA_PARAM_CU")==null?"":rst2.getString("FIRMA_PARAM_CU");
					String num_nafin =rst2.getString("NUM_NAFIN_ELECT")==null?"":rst2.getString("NUM_NAFIN_ELECT");
					info = new HashMap();
					info.put("NUMERO_SIRAC",num_sirac);
					info.put("NOMBRE_PYME",nombre_pyme);
					info.put("MONEDA",moneda);
					info.put("BANCO_SERVICIO",banco_servicio);
					info.put("NO_CUENTA",num_cuenta);
					info.put("CUENTA_CLABE",cuenta_clabe);
					info.put("SUCURSAL",sucursal);
					info.put("NOMBRE_EPO",nombre_epo);
					info.put("RFC",rfc);
					info.put("PLAZA",plaza);
					info.put("AUTORIZACION_IF",autorizacionIF);
					info.put("USUARIO_MODIFICO",usuario);
					info.put("OPERA_CONVENIO_UNICO",operac_convenio);
					info.put("FIRMA_AUTOGRAFA_CU",fechaFirmaAutoCU);
					info.put("FIRMA_PARAM_CU",fechaparamCU);
					info.put("NUM_NAFIN_ELECT",num_nafin);
					registros.add(info);
				}
				rst2.close();
				pst2.close();

				bOk = true;
			} catch(Exception e){
				e.printStackTrace();
				bOk = false;
			}

			con.terminaTransaccion(bOk);	//Se introduce un commit/rollback por cuenta, para evitar que dure demasiado tiempo la transacci�n.
		}

		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			log.info("autorizacionCuenta (S)");
		}
			return consulta;
	}


	/* generaci�n delArchivo de Acuse de la pantalla de Autorizaci�n  Cuentas Bancarias Pymes */
	public String  archivoAcuseAutorizacion(List parametros ){
		log.info("archivoAcuseAutorizacion (E)");

		String  icIfActual  = parametros.get(0).toString();
		String  iPyme  = parametros.get(1).toString();
	   String iEPO  = parametros.get(2).toString();
		String  iCta   = parametros.get(3).toString();
		String strNombreUsuario = parametros.get(4).toString();
		String  strDirectorioTemp  = parametros.get(5).toString();
		String  sIfConvenioUnico  = parametros.get(6).toString();
		String pais = parametros.get(7).toString();
		String nombre = parametros.get(8).toString();
		String nombreUsr = parametros.get(9).toString();
		String logo = parametros.get(10).toString();
		String strDirectorioPublicacion = parametros.get(11).toString();

		AccesoDB con =new AccesoDB();
		ResultSet rst2 = null;
		PreparedStatement pst2 = null;
		StringBuffer strSQL = new StringBuffer();
		ArrayList varBind = new ArrayList();
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo = creaArchivo.nombreArchivo()+".pdf";

		try{
			con.conexionDB();

			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


			pdfDoc.encabezadoConImagenes(pdfDoc,pais, icIfActual, icIfActual , nombre, nombreUsr, logo, strDirectorioPublicacion);
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.addText("","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Acuse de recibo de Autorizaci�n de Cuentas Bancaria(s) PYME","formas",ComunesPDF.CENTER);
			pdfDoc.addText("","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

			int columnas =13;
			if(sIfConvenioUnico.equals("S")){  		columnas+=3; 	}
			pdfDoc.setTable(columnas,100);
			pdfDoc.setCell("N�mero SIRAC","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("PyME","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco de servicio","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No. de Cuenta","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Clabe","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plaza","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Autorizaci�n IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Usuario-Modific�","celda01",ComunesPDF.CENTER);
			if(sIfConvenioUnico.equals("S")){
				pdfDoc.setCell("Pyme Opera con Convenio �nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Firma Aut�grafa de CU.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Parametrizaci�n Autom�tica CU","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Num. Nafin Electr�nico","celda01",ComunesPDF.CENTER);

			strSQL.append(" SELECT relpymeif.cs_vobo_if as cs_vobo_if , relctabanc.ic_cuenta_bancaria,");
			strSQL.append(" 	DECODE(pyme.cs_habilitado, 'N', '*', 'S', '')||' '||pyme.cg_razon_social as NOMBRE_PYME ,");
			strSQL.append(" 	relctabanc.cg_banco as BANCO_SERVICIO  , relctabanc.cg_numero_cuenta as NO_CUENTA  , mon.cd_nombre as  MONEDA , relpymeif.ic_epo,");
			strSQL.append(" 	pyme.ic_pyme, relctabanc.cg_sucursal as SUCURSAL , epo.cg_razon_social as NOMBRE_EPO, pyme.cg_rfc as  RFC,");
			strSQL.append(" 	NVL(plaza.cd_descripcion, 'No tiene') as PLAZA ,");
			strSQL.append(" 	relctabanc.CG_CUENTA_CLABE as CUENTA_CLABE , ");
			strSQL.append(" 	TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') as FIRMA_AUTOGRAFA_CU,  ");
			strSQL.append(" 	TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') as FIRMA_PARAM_CU, ");
			strSQL.append(" 	DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO, ");
			strSQL.append(" 	pyme.in_numero_sirac as   NUMERO_SIRAC ,  ");
			strSQL.append(" 	crn.ic_nafin_electronico as NUM_NAFIN_ELECT,  ");//FODEA 018 - 2009 ACF
			strSQL.append(" 	'15cadenas15mantenimiento15parametrizacion15if15admparamifpyme.jsp' AS pantalla");
			strSQL.append(" FROM comcat_moneda mon,");
			strSQL.append(" 	comcat_pyme pyme,");
			strSQL.append(" 	comrel_cuenta_bancaria relctabanc,");
			strSQL.append(" 	comrel_pyme_if relpymeif,");
			strSQL.append(" 	comcat_epo epo,");
			strSQL.append(" 	comcat_plaza plaza,");
			strSQL.append(" 	comrel_nafin crn,");//FODEA 018 - 2009 ACF
			strSQL.append(" 	comrel_pyme_epo pe");
			strSQL.append(" WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria");
			strSQL.append(" 	AND pyme.ic_pyme = relctabanc.ic_pyme");
			strSQL.append(" 	AND pyme.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" 	AND crn.cg_tipo = ?");
			strSQL.append(" 	AND relctabanc.ic_moneda = mon.ic_moneda");
			strSQL.append(" 	AND epo.ic_epo = relpymeif.ic_epo");
			strSQL.append(" 	AND relctabanc.ic_plaza = plaza.ic_plaza(+)");
			strSQL.append(" 	AND relpymeif.ic_if = ?");
			strSQL.append(" 	AND relpymeif.cs_borrado = ?");
			strSQL.append(" 	AND pe.ic_pyme = pyme.ic_pyme");
			strSQL.append(" 	AND relpymeif.ic_epo = pe.ic_epo");
			strSQL.append(" 	AND relpymeif.cs_vobo_if = ? ");
			strSQL.append(" 	AND pyme.ic_pyme in( "+iPyme+" )");
			strSQL.append(" 	AND relpymeif.ic_epo in( "+iEPO +")");
			strSQL.append(" 	AND pe.ic_epo  in( "+iEPO +")");
			strSQL.append(" 	AND RELCTABANC.IC_CUENTA_BANCARIA in( "+iCta +" )");
			strSQL.append(" ORDER BY 3, 4");

			varBind.add("P");//FODEA 018 - 2009 ACF
			varBind.add(new Long(icIfActual));
			varBind.add("N");
			varBind.add("S");

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
			rst2 = pst2.executeQuery();
			while(rst2.next()){
				String num_sirac =rst2.getString("NUMERO_SIRAC")==null?"":rst2.getString("NUMERO_SIRAC");
				String nombre_pyme =rst2.getString("NOMBRE_PYME")==null?"":rst2.getString("NOMBRE_PYME");
				String moneda =rst2.getString("MONEDA")==null?"":rst2.getString("MONEDA");
				String banco_servicio =rst2.getString("BANCO_SERVICIO")==null?"":rst2.getString("BANCO_SERVICIO");
				String num_cuenta =rst2.getString("NO_CUENTA")==null?"":rst2.getString("NO_CUENTA");
				String cuenta_clabe =rst2.getString("CUENTA_CLABE")==null?"":rst2.getString("CUENTA_CLABE");
				String sucursal  = rst2.getString("SUCURSAL")==null?"":rst2.getString("SUCURSAL");
				String nombre_epo = rst2.getString("NOMBRE_EPO")==null?"":rst2.getString("NOMBRE_EPO");
				String rfc = rst2.getString("RFC")==null?"":rst2.getString("RFC");
				String plaza  = rst2.getString("PLAZA")==null?"":rst2.getString("PLAZA");
				String autorizacionIF = "SI";
				String usuario = strNombreUsuario;
				String operac_convenio = rst2.getString("OPERA_CONVENIO_UNICO")==null?"":rst2.getString("OPERA_CONVENIO_UNICO");
				String fechaFirmaAutoCU = rst2.getString("FIRMA_AUTOGRAFA_CU")==null?"":rst2.getString("FIRMA_AUTOGRAFA_CU");
				String fechaparamCU =rst2.getString("FIRMA_PARAM_CU")==null?"":rst2.getString("FIRMA_PARAM_CU");
				String num_nafin =rst2.getString("NUM_NAFIN_ELECT")==null?"":rst2.getString("NUM_NAFIN_ELECT");


					pdfDoc.setCell(num_sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(banco_servicio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta_clabe,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sucursal,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plaza,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(autorizacionIF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);
					if(sIfConvenioUnico.equals("S")){
						pdfDoc.setCell(operac_convenio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaFirmaAutoCU,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaparamCU,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(num_nafin,"formas",ComunesPDF.CENTER);

				}
				rst2.close();
				pst2.close();


				pdfDoc.addTable();
				pdfDoc.endDocument();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("archivoAcuseAutorizacion (S)");
		}
			return nombreArchivo;
	}


	/**
	 * consulta para obtener las cuentas bancarias Pyme Pendientes  de autorizaci�n
	 * @return
	 * @param ic_if
	 */
	public String cuentasPedientes(String  ic_if ){

	String 	qrySentencia 	= null;
	PreparedStatement 	ps	= null;
	ResultSet	rs	= null;
	AccesoDB con =new AccesoDB();
	String consulta ="";
	HashMap	info = new HashMap();
	JSONArray registros = new JSONArray();
	try{
			con.conexionDB();
			qrySentencia = 	"SELECT COMCAT_PYME.CG_RFC RFC, COMCAT_PYME.CG_RAZON_SOCIAL PYME, COMCAT_EPO.CG_RAZON_SOCIAL EPO " +
				"FROM COMCAT_PYME , COMCAT_EPO, COMHIS_CAMBIOS_CTA " +
				"WHERE COMHIS_CAMBIOS_CTA.IC_PYME = COMCAT_PYME.IC_PYME " +
				"AND COMHIS_CAMBIOS_CTA.IC_EPO = COMCAT_EPO.IC_EPO " +
				"AND COMHIS_CAMBIOS_CTA.IC_IF = ? " +
				"AND COMHIS_CAMBIOS_CTA.CS_AUTORIZA_IF  = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			ps.setString(2,"N");
			rs = ps.executeQuery();
			while(rs.next()){

				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String pyme = (rs.getString("PYME") == null) ? "" : rs.getString("PYME");
				String epo = (rs.getString("EPO") == null) ? "" : rs.getString("EPO");

				info = new HashMap();
				info.put("RFC",rfc);
				info.put("NOMBRE_PYME",pyme);
				info.put("NOMBRE_EPO",epo);
				registros.add(info);
        }
			rs.close();
			ps.close();

			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
			return consulta;
	}

	/**
	* pantalla Cuentas Bancarias PYMEs -Autorizaci�n - Masiva
	* Generar Archivo de Carga
	*/
 	public String    archivoConsAutoCarga(List parametros){
		log.info("archivoConsAutoCarga (E)");
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String icpyme ="", respyme ="", resEpo ="", vpyme ="";
		ResultSet	rsX;
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		String  ic_if =  parametros.get(0).toString();
		String chkPymesConvenio = parametros.get(1).toString();
		String nafinelect  = parametros.get(2).toString();
		String cboEPO  = parametros.get(3).toString();
		String ic_pyme = parametros.get(4).toString();
		String desde = parametros.get(5).toString();
		String hasta = parametros.get(6).toString();
		//String sIfConvenioUnico = parametros.get(7).toString();
		String ruta = parametros.get(8).toString();


		try{
			con.conexionDB();
			ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(
				" SELECT  RELPYMEIF.CS_VOBO_IF as CS_VOBO_IF , "+
				" pyme.in_numero_sirac as NUMERO_SIRAC ,"+
				"  RELCTABANC.IC_CUENTA_BANCARIA  , " +
				" decode (PYME.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '|| PYME.CG_RAZON_SOCIAL as  NOMBRE_PYME, "+
				" RELCTABANC.CG_BANCO as BANCO_SERVICIO , " +
				" RELCTABANC.CG_NUMERO_CUENTA as  NO_CUENTA ,"+
				" M.CD_NOMBRE as NOMBRE_MONEDA , "+
				" RELPYMEIF.IC_EPO ,"+
				" PYME.IC_PYME as IC_PYME , "+
				" RELCTABANC.CG_SUCURSAL as SUCURSAL ,"+
				" EPO.cg_razon_social  as NOMBRE_EPO,  " +
				" PYME.CG_RFC as RFC , "+
				" nvl(PLAZA.CD_DESCRIPCION,'no tiene') as PLAZA,  " +
				" relctabanc.CG_CUENTA_CLABE as CUENTA_CLABE ,  " +
				" TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') FECHA_FIRMA_AUTO,  " +
				" TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM_CU,  " +
				" DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO,  " +
				" crn.ic_nafin_electronico NUM_NAFIN_ELEC , " +//FODEA 018 - 2009 ACF
				" m.ic_moneda cvemoneda " +
				" FROM COMCAT_MONEDA M, COMCAT_PYME PYME, " +
				" COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF,comcat_epo EPO " +
				" ,    COMCAT_PLAZA PLAZA "+
				"       , comrel_pyme_epo pe "+
				"       , comrel_nafin crn "+//FODEA 018 - 2009 ACF
				" WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
				" AND PYME.IC_PYME = RELCTABANC.IC_PYME AND RELCTABANC.IC_MONEDA = M.IC_MONEDA AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
				"      AND RELCTABANC.IC_PLAZA = PLAZA.IC_PLAZA(+) " +
				" AND RELPYMEIF.IC_IF = " + ic_if +
				" AND pyme.ic_pyme = crn.ic_epo_pyme_if" +//FODEA 018 - 2009 ACF
				" AND crn.cg_tipo = 'P'" +//FODEA 018 - 2009 ACF
				" AND RELPYMEIF.CS_BORRADO='N' " +
				" AND pe.ic_pyme = PYME.IC_PYME " +
				" AND RELPYMEIF.IC_EPO = PE.IC_EPO "+
				" AND RELPYMEIF.CS_VOBO_IF = 'N' "+
				" AND PYME.IC_PYME  = ? "+
				" AND RELPYMEIF.IC_EPO = ? "+
				" AND pe.IC_EPO =  ? ");

			if(chkPymesConvenio.equals("S")) {
				sbQuery.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");
			}
			sbQuery.append(" ORDER BY 3, 4 ");

			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(sbQuery.toString());


			if (!nafinelect.equals("")){
				String qrySentenciaS =
					" SELECT ic_epo_pyme_if as pyme, p.cs_expediente_efile, p.cg_rfc " +
					" FROM comrel_nafin n " +
					" , comcat_pyme p " +
					" WHERE n.cg_tipo = 'P' " +
					" AND n.ic_epo_pyme_if = p.ic_pyme " +
					" AND n.ic_nafin_electronico=  " + nafinelect ;
            rs = con.queryDB(qrySentenciaS);
				if(rs.next()){
					icpyme = rs.getString("PYME");
               if ("N".equals(rs.getString("CS_EXPEDIENTE_EFILE"))) {
						if(tieneExpedienteEnEfile(rs.getString("CG_RFC"))){
							parametrosDescuento.setEstatusDelExpedienteEfileEnTablaPyme(rs.getString("CG_RFC"),true);
                  }
                }
				}
			}
         con.cierraStatement();

			String qrySentenciaX =
						"SELECT ST.IC_PYME, ST.CG_RAZON_SOCIAL, ST.IC_EPO, ST.IN_NUMERO_SIRAC, ST.CG_RFC, NVL(VP.CS_DOC_VIGENTES,'N') as PRIORIDAD  " +
						" FROM vm_pymes_doc_vig VP, (SELECT distinct(PYME.IC_PYME) AS IC_PYME,  " +
						"		decode (PYME.cs_habilitado,'N','*','S',' ')||' '||PYME.CG_RAZON_SOCIAL AS CG_RAZON_SOCIAL,  " +
						"		RELPYMEIF.ic_epo AS IC_EPO,  " +
						"   	PYME.in_numero_sirac AS in_numero_sirac,  " +
						"		PYME.cg_rfc AS cg_rfc, " +
						"   	'15cadenas15mantenimiento15parametrizacion15if15admparamifpyme.jsp' as PANTALLA " +
						"		FROM COMCAT_MONEDA M, " +
						"		COMCAT_PYME PYME, " +
						"		COMREL_CUENTA_BANCARIA RELCTABANC, " +
						"		COMREL_PYME_IF RELPYMEIF, " +
						"		comcat_epo EPO " +
						"     , comrel_pyme_epo pe "+
						"		WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
						"		AND PYME.IC_PYME = RELCTABANC.IC_PYME " +
						"		AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
						"		AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
						"		AND EPO.cs_habilitado = 'S' " +
						"		AND RELPYMEIF.CS_VOBO_IF ='N' "+
						"		AND RELPYMEIF.IC_IF = " + ic_if +
						"		AND RELPYMEIF.CS_BORRADO='N' " +
						"		AND RELPYMEIF.CS_ESTATUS IS NULL " +
						"		AND pe.ic_pyme = PYME.IC_PYME " +
					   "   AND PYME.in_numero_sirac IS NOT NULL";

			if(!nafinelect.equals("") && !icpyme.equals("")) {
				qrySentenciaX += " AND PYME.IC_PYME  = " + icpyme;
			} else if (!nafinelect.equals("") ){
				qrySentenciaX += " AND PYME.IC_PYME  = 0";
			}
			if(!cboEPO.equals("")) {
				if(!cboEPO.equals("0")) {
					qrySentenciaX += " AND RELPYMEIF.IC_EPO = " + cboEPO;
					qrySentenciaX += "	  AND pe.IC_EPO = " + cboEPO;
				}
				if(!ic_pyme.equals("") &&  !ic_pyme.equals("0") )
					qrySentenciaX += " AND PYME.IC_PYME  = " + ic_pyme;
			}
			if(!desde.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if >= TO_DATE('"+desde+"', 'dd/mm/yyyy') ";
			}
			if(!hasta.equals("")){
				qrySentenciaX += " AND RELPYMEIF.df_vobo_if <  TO_DATE('"+hasta+"', 'dd/mm/yyyy')+1 ";
			}
			qrySentenciaX += " AND PYME.CS_EXPEDIENTE_EFILE = 'S' ";
			qrySentenciaX += " ) ST " +
									" WHERE ST.IC_EPO = VP.IC_EPO(+) " +
									" AND ST.IC_PYME = VP.IC_PYME(+) " +
									"ORDER BY 2 ";

			rsX = con.queryDB(qrySentenciaX);

			contenidoArchivo.append("EPO|PYME|TIPO_MONEDA|MOVIMIENTO|COMPROBANTE_DOMICILIO_FISCAL|COMPROBANTE_ALTA_HACIENDA|"+
											"CEDULA_RFC|IDENTIFICACION_OFICIAL_VIGENTE|ESTADO_CUENTA_CHEQUES|CONVENIO_PYME_NAFIN|"+
											"OTROS_DOCTOS_FACTORAJE|CONTRATO_IF|NOTAS| \n");
			 while (rsX.next()){
				respyme= rsX.getString(1);
				resEpo= rsX.getString(3);
				int numeroPyme=Integer.parseInt(respyme);
				int numeroEpo=Integer.parseInt(resEpo);
				pstmt.setInt(1,numeroPyme);
				pstmt.setInt(2,numeroEpo);
				pstmt.setInt(3,numeroEpo);

				ResultSet rsTraeIF = pstmt.executeQuery();
				pstmt.clearParameters();
				vpyme =	((rsX.getString(2)==null)?"":rsX.getString(2).replace(',',' '))  ;

				while(rsTraeIF.next()){

					 /*01*/contenidoArchivo.append(rsX.getString(3)+"|");
					 /*02*/contenidoArchivo.append(rsTraeIF.getString("RFC")+"|");
					 /*03*/contenidoArchivo.append(rsTraeIF.getString("cvemoneda")+"|");
					 /*04*/contenidoArchivo.append("A|");//?	MOVIMIENTO
					 /*05*/contenidoArchivo.append("N|");//?	COMPROBANTE_DOMICILIO_FISCAL
					 /*06*/contenidoArchivo.append("N|");//?	COMPROBANTE_ALTA_HACIENDA
					 /*07*/contenidoArchivo.append("N|");//?	CEDULA_RFC
					 /*08*/contenidoArchivo.append("N|");//?	IDENTIFICACION_OFICIAL_VIGENTE
					 /*09*/contenidoArchivo.append("N|");//?	ESTADO_CUENTA_CHEQUES
					 /*10*/contenidoArchivo.append("N|");//?	CONVENIO_PYME_NAFIN
					 /*11*/contenidoArchivo.append("N|");//?	OTROS_DOCTOS_FACTORAJE
					 /*12*/contenidoArchivo.append("N|");//?	CONTRATO_IF
					 /*01*/contenidoArchivo.append("|\n"); //?	Notas
				}
			}

			creaArchivo.make(contenidoArchivo.toString(), ruta, ".txt");
			nombreArchivo = creaArchivo.getNombre();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("archivoConsAutoCarga (S)");
		}
			return nombreArchivo;
	}


	/**
	*metodo para generar archivo de errores  en la pantalla de
	* Carga de Cuentas Bancarias Pymes
	 * @return
	 * @param strDirectorioTemp
	 * @param numeroProceso
	 */
	public String  generarArchError(String numeroProceso, String  strDirectorioTemp )  {

		AccesoDB con =new AccesoDB();
		CreaArchivo archivo 				= new CreaArchivo();
		StringBuffer contenidoArchivo 	= new StringBuffer();
		String 	nombreArchivo 		= null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";

		try{

			con.conexionDB();
			query =
				"SELECT  ic_numero_proceso, ic_numero_linea, ic_if, ic_epo, cg_rfc_pyme, " +
					"  ic_moneda, cg_tipo_mov, cs_comp_domicilio_fiscal, " +
					"  cs_comp_alta_hacienda, cs_cedula_rfc, cs_ident_oficial_vig, " +
					"  cs_edo_cta_cheques, cs_convenio_pyme_nafin, " +
					"  cs_otros_doctos_factoraje, cs_contrato_if, cg_notas, cg_error " +
					"  FROM comtmp_ctasbanc_pyme_masiva " +
					" WHERE ic_numero_proceso = ? AND cg_error IS NOT NULL "+
					" ORDER BY ic_numero_linea ";
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(numeroProceso));
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
				contenidoArchivo.append(rs.getString("ic_epo")+"|");
				contenidoArchivo.append(rs.getString("cg_rfc_pyme")+"|");
				contenidoArchivo.append(rs.getString("ic_moneda")+"|");
				contenidoArchivo.append(rs.getString("cg_tipo_mov")+"|");
				contenidoArchivo.append(rs.getString("cs_comp_domicilio_fiscal")+"|");
				contenidoArchivo.append(rs.getString("cs_comp_alta_hacienda")+"|");
				contenidoArchivo.append(rs.getString("cs_cedula_rfc")+"|");
				contenidoArchivo.append(rs.getString("cs_ident_oficial_vig")+"|");
				contenidoArchivo.append(rs.getString("cs_edo_cta_cheques")+"|");
				contenidoArchivo.append(rs.getString("cs_convenio_pyme_nafin")+"|");
				contenidoArchivo.append(rs.getString("cs_otros_doctos_factoraje")+"|");
				contenidoArchivo.append(rs.getString("cs_contrato_if")+"|");
				contenidoArchivo.append((rs.getString("cg_notas")==null?"":rs.getString("cg_notas"))+"|");
				contenidoArchivo.append("\n");

			}
			rs.close();
			ps.close();

			archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
			nombreArchivo = archivo.getNombre();


		} catch(Exception e) {
			e.printStackTrace();
		//	throw new NafinException("SIST0001");
		} finally {

			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
			return nombreArchivo;
	}

	/**
	 * generar CSV y PDF del acuse de la pantalla de Cuentas PYME, Liberaci�n Masiva
	 *  @return
	 * @param parametros
	 */


	public String  archivAcuseCSVyPDF(List parametros )  {

		AccesoDB con =new AccesoDB();
		CreaArchivo archivo 				= new CreaArchivo();
		StringBuffer contenidoArchivo 	= new StringBuffer();
		String 	nombreArchivo 		= null;
		ComunesPDF pdfDoc = new ComunesPDF();

		String  numeroProceso = parametros.get(0).toString();
		//String  recibo = parametros.get(1).toString();
		String  acuse = parametros.get(2).toString();
		String  fechaCarga = parametros.get(3).toString();
		String  horaCarga = parametros.get(4).toString();
		String  usuario = parametros.get(5).toString();
		String  strDirectorioTemp  = parametros.get(6).toString();
		String pais = parametros.get(7).toString();
		String nombre = parametros.get(8).toString();
		String nombreUsr = parametros.get(9).toString();
		String logo = parametros.get(10).toString();
		String strDirectorioPublicacion = parametros.get(11).toString();
		String  tipoArchivo = parametros.get(12).toString();
		String noCliente  = parametros.get(13).toString();

		try{

			con.conexionDB();

			if(tipoArchivo.equals("CSV")) {
				contenidoArchivo.append("EPO,PYME,MONEDA,MOVIMIENTO,COMPROBANTE DOMICILIO FISCAL,COMPROBANTE ALTA HACIENDA,CEDULA_RFC,"+
												"IDENTIFICACI�N OFICIAL VIGENTE,ESTADO CUENTA CHEQUES,CONVENIO PYME NAFIN,OTROS DOCTOS FACTORAJE,"+
												"CONTRATO_IF,NOTAS,CAUSA DE RECHAZO\n");
			}

			if(tipoArchivo.equals("PDF")) {
			   nombreArchivo = archivo.nombreArchivo()+".pdf";
				pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


				pdfDoc.encabezadoConImagenes(pdfDoc,pais, noCliente, noCliente , nombre, nombreUsr, logo, strDirectorioPublicacion);
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(2,40);
				pdfDoc.setCell("Cifras de Control","formasmen",ComunesPDF.CENTER,2);
				pdfDoc.setCell("N�mero de Acuse","formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(acuse,"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setCell("Fecha de Carga","formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(fechaCarga,"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setCell("Hora de Carga","formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(horaCarga,"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setCell("Usuario de Captura","formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(usuario,"formasmen",ComunesPDF.RIGHT);
				pdfDoc.addTable();

				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(14,100);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("MOVIMIENTO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("COMPROBANTE DOMICILIO FISCAL","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("COMPROBANTE ALTA HACIENDA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("CEDULA_RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IDENTIFICACI�N OFICIAL VIGENTE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("ESTADO CUENTA CHEQUES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("CONVENIO PYME NAFIN","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("OTROS DOCTOS FACTORAJE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("CONTRATO_IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("NOTAS","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("CAUSA DE RECHAZO","celda01",ComunesPDF.CENTER);

			}


			List lstAcuseLibMasiva = this.getAcuseLiberMasivaPymes(numeroProceso);
				if(lstAcuseLibMasiva!=null && lstAcuseLibMasiva.size()>0){
				for(int x=0;x<lstAcuseLibMasiva.size();x++){
					List lstRegProc = (List)lstAcuseLibMasiva.get(x);

					String nombre_epo = lstRegProc.get(3)==null?"":(String)lstRegProc.get(3);
					String nombre_pyme = lstRegProc.get(4)==null?"":(String)lstRegProc.get(4);
					String moneda = lstRegProc.get(5)==null?"":(String)lstRegProc.get(5);
					String movimiento = lstRegProc.get(6)==null?"":(String)lstRegProc.get(6);
					String domicilio= lstRegProc.get(7)==null?"":(String)lstRegProc.get(7);
					String alta_hacienda =lstRegProc.get(8)==null?"":(String)lstRegProc.get(8);
					String cedula_rfc = lstRegProc.get(9)==null?"":(String)lstRegProc.get(9);
					String identificacion= lstRegProc.get(10)==null?"":(String)lstRegProc.get(10);
					String estado_cuenta = lstRegProc.get(11)==null?"":(String)lstRegProc.get(11);
					String convenio_pyme_nafin =lstRegProc.get(12)==null?"":(String)lstRegProc.get(12);
					String otros_doctos_factoraje =lstRegProc.get(13)==null?"":(String)lstRegProc.get(13);
					String  contra_if  = lstRegProc.get(14)==null?"":(String)lstRegProc.get(14);
					String notas ="".equals((String)lstRegProc.get(15))?"":(String)lstRegProc.get(15);
					String rechazo =lstRegProc.get(16)==null?"":(String)lstRegProc.get(16);

					if(tipoArchivo.equals("CSV")) {
						contenidoArchivo.append(nombre_epo.replaceAll(",","") +", ");
						contenidoArchivo.append(nombre_pyme.replaceAll(",","") +", ");
						contenidoArchivo.append(moneda.replaceAll(",","") +", ");
						contenidoArchivo.append(movimiento.replaceAll(",","") +", ");
						contenidoArchivo.append(domicilio.replaceAll(",","") +", ");
						contenidoArchivo.append(alta_hacienda.replaceAll(",","") +", ");
						contenidoArchivo.append(cedula_rfc.replaceAll(",","") +", ");
						contenidoArchivo.append(identificacion.replaceAll(",","") +", ");
						contenidoArchivo.append(estado_cuenta.replaceAll(",","") +", ");
						contenidoArchivo.append(convenio_pyme_nafin.replaceAll(",","") +", ");
						contenidoArchivo.append(otros_doctos_factoraje.replaceAll(",","") +", ");
						contenidoArchivo.append(contra_if.replaceAll(",","") +", ");
						contenidoArchivo.append(notas.replaceAll(",","") +", ");
						contenidoArchivo.append(rechazo.replaceAll(",","") +"\n ");
					}


					if(tipoArchivo.equals("PDF")) {
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(movimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(alta_hacienda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cedula_rfc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(identificacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(estado_cuenta,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(convenio_pyme_nafin,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(otros_doctos_factoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(contra_if,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(notas,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(rechazo,"formas",ComunesPDF.CENTER);
					}
				}
			}

			if(tipoArchivo.equals("CSV")) {
				archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv");
				nombreArchivo = archivo.getNombre();
			}

			if(tipoArchivo.equals("PDF")) {
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}


		} catch(Exception e) {
			e.printStackTrace();
		//	throw new NafinException("SIST0001");
		} finally {

			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
			return nombreArchivo;
	}

	/**
	 * metodo para  Obtener datos para saber si existen pymes sin numero de proveedor relacionadas a la EPO
	 * Migraci�n EPO-2012
	 * @return
	 * @param claveEpo
	 */
	public int  verifPYMEsinNumProv(String claveEpo) {
		log.info("verifPYMEsinNumProv(E)");
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		List listaEPO = new ArrayList();
		int numeroPymesSinNumProv =0;
		try {
			con.conexionDB();

			String 	verifPYMEsinNumProv = 	"SELECT COUNT(*) PYMES_SIN_NUMERO " +
					"  FROM comcat_epo t7, " +
					"       comcat_pyme t2, " +
					"       comrel_pyme_epo t5, " +
					"       comcat_version_convenio t8, " +
					"       comrel_nafin t6, " +
					"       com_domicilio t1, " +
					"       comcat_estado t3 " +
					" WHERE t5.ic_pyme = t2.ic_pyme " +
					"   AND t1.ic_pyme = t2.ic_pyme " +
					"   AND t2.ic_pyme = t6.ic_epo_pyme_if " +
					"   AND t1.ic_estado = t3.ic_estado " +
					"   AND t5.ic_epo = t7.ic_epo " +
					"   AND t2.ic_version_convenio = t8.ic_version_convenio(+) " +
					"   AND t2.ic_tipo_cliente IN (1, 4) " +
					"   AND t6.cg_tipo = 'P' " +
					"   AND t2.cs_internacional in ('S','N') " +
					"   AND t5.cg_pyme_epo_interno IS NULL " +
					"   AND t5.cs_num_proveedor = 'S' " +
					"   AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') " +
					"   AND t1.cs_fiscal = 'S' "+
					"   AND t5.ic_epo = ? ";

				listaEPO.add(claveEpo);
				registros = con.consultarDB(verifPYMEsinNumProv, listaEPO, false);
				if(registros.next()) {
					numeroPymesSinNumProv = Integer.parseInt(registros.getString("PYMES_SIN_NUMERO"));
				}

		} catch(Exception e) {
			throw new AppException("Error al  Obtener datos para saber si existen pymes sin numero de proveedor relacionadas a la EPO", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("verifPYMEsinNumProv(S)");
		}
		return numeroPymesSinNumProv;
	}

	/**
	 * metodo para  Obtener datos para saber que tipos de factoraje estan parametrizados con esa EPO
	 * Migraci�n NAFIN-2013
	 * @return
	 * @param claveEpo
	 */
	public String getParametrosFactoraje(String ic_epo){
		StringBuffer tiposFactorajes= new StringBuffer("");
		PreparedStatement ps = null;//MODIFICACION FODEA 042-2009 FVR
		AccesoDB con = new AccesoDB();
		ResultSet rs;
		try{
		con.conexionDB();

		String qryParamEpo = "SELECT COUNT(1) FROM COM_PARAMETRIZACION_EPO WHERE IC_EPO = ? AND CC_PARAMETRO_EPO = ? AND CG_VALOR = ? ";
		ps = con.queryPrecompilado(qryParamEpo);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setString(2,"PUB_EPO_VENC_INFONAVIT");
		ps.setString(3,"S");
		rs = ps.executeQuery();
		if(rs!=null && rs.next())
			if(!"0".equals(rs.getString(1)))
				tiposFactorajes.append(",I");
		rs.close();
		ps.close();


		ps = con.queryPrecompilado(qryParamEpo);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setString(2,"PUB_EPO_OPERA_MANDATO");
		ps.setString(3,"S");
		rs = ps.executeQuery();
		if(rs!=null && rs.next())
			if(!"0".equals(rs.getString(1))){
				tiposFactorajes.append(",M");
      }
	//FACTORAJE_IF 00 2012
		ps = con.queryPrecompilado(qryParamEpo);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setString(2,"FACTORAJE_IF");
		ps.setString(3,"S");
		rs = ps.executeQuery();
		if(rs!=null && rs.next()) {
			if(!"0".equals(rs.getString(1))){
				tiposFactorajes.append(",A");
			}
		}
		rs.close();
		ps.close();

		}catch(Exception e){}
		finally {
				if (con.hayConexionAbierta()) con.cierraConexionDB();
			}
		return tiposFactorajes.toString();
	}


	/**
	 *  Pantalla de  Administraci�n/Parametrizaci�n/Otros Par�metros
	 *  Migraci�n Nafin
	 * @return
	 * @param cadena
	 */
	private String formateaCamposCorreo(String cadena){
		// Escapar caracteres especiales
		if(cadena == null) return null;
		cadena = cadena.replaceAll("'","''");
		cadena = cadena.replaceAll("[\r\n]+","");
		// Suprimir saltos de linea
		return cadena;
	}

	/**
	 *  Pantalla de  Administraci�n/Parametrizaci�n/Otros Par�metros
	 *  Migraci�n Nafin
	 * @return
	 * @param cadena
	 */
	private String agregaSaltosDeLinea(String cadena){
		// Escapar caracteres especiales
		if(cadena == null) return null;
		cadena = cadena.replaceAll(",",",\n");
		return cadena;
	}


	/**
	 *  Pantalla de  Administraci�n/Parametrizaci�n/Otros Par�metros
	 *  Migraci�n Nafin
	 * @return
	 */
   public HashMap creaFormulario(){
      AccesoDB con = new AccesoDB();
      ResultSet rs = null;
      HashMap hm = new HashMap();

      String query_dat_nafin =
			" SELECT in_dias_minimo, in_dias_maximo, fn_aforo, "   +
			"        in_dias_vig_edo_cuenta, in_dias_vig_vencimientos, in_dias_vig_operados,"   +
			"        ig_plazo_maximo_factoraje, ig_plazo_maximo_factoraje_dl, ig_base_oper_gral, "   +
			"        cg_email_contingencia, fn_aforo_dl, cg_hora_reportes_esepciales, "   +
			"			cg_email_notificaciones_nafin, cs_valida_cve_cder_oper_tel "  +
			"   FROM comcat_producto_nafin"   +
			"  WHERE ic_producto_nafin = 1"  ;

      try{
         con.conexionDB();
         rs = con.queryDB(query_dat_nafin);
         if (rs.next()) {
            hm.put("strDiasMin",					rs.getString("IN_DIAS_MINIMO"));
            hm.put("strDiasMax",					rs.getString("IN_DIAS_MAXIMO"));
            hm.put("strPorcientoActual",			rs.getString("FN_AFORO"));
            hm.put("strDiasEdoCuenta",				rs.getString("IN_DIAS_VIG_EDO_CUENTA"));
            hm.put("strDiasVencimiento",			rs.getString("IN_DIAS_VIG_VENCIMIENTOS"));
            hm.put("strDiasOperados",				rs.getString("IN_DIAS_VIG_OPERADOS"));
            hm.put("strPlazoMaxFactorajeMN",		rs.getString("IG_PLAZO_MAXIMO_FACTORAJE"));
            hm.put("strPlazoMaxFactorajeDL",		rs.getString("IG_PLAZO_MAXIMO_FACTORAJE_DL"));
            hm.put("strBaseOperGenerica",			rs.getString("IG_BASE_OPER_GRAL"));
            hm.put("strEmailContingencia",			rs.getString("CG_EMAIL_CONTINGENCIA"));
            hm.put("strPorcientoActualDL",			rs.getString("FN_AFORO_DL"));
            hm.put("strHoraReportesEspeciales",		rs.getString("CG_HORA_REPORTES_ESEPCIALES"));
            hm.put("strEmailNotificacionesNafin",	agregaSaltosDeLinea(rs.getString("CG_EMAIL_NOTIFICACIONES_NAFIN")));
			hm.put("optOperTel",					rs.getString("CS_VALIDA_CVE_CDER_OPER_TEL"));
         }
         rs.close();
         con.cierraStatement();
      }catch(Exception e){
         e.printStackTrace();
         throw new RuntimeException(e);
      }finally{
         if(con.hayConexionAbierta()){
            con.cierraConexionDB();
         }
      }
      return hm;
   }

	/**
	 * metodo para actualizar la pantalla de
	 * Administraci�n/Parametrizaci�n/Otros Par�metros
	 * Migraci�n Nafin
	 * @return
	 * @param parametros
	 */
   public String  actOtrosParametros( List parametros ){
      AccesoDB con = new AccesoDB();
      ResultSet rs = null;
      String mensaje ="", strPorciento ="", strPorcientoDL ="";
		int bandera = 0;

      try{
         con.conexionDB();

			String strDiasMin = (String)parametros.get(0);
			String strDiasMax = (String)parametros.get(1);
			String strPorcientoActual = (String)parametros.get(2);
			String strPorcientoActualDL = (String)parametros.get(3);
			String strDiasEdoCuenta = (String)parametros.get(4);
			String strDiasVencimiento = (String)parametros.get(5);
			String strDiasOperados = (String)parametros.get(6);
			String strPlazoMaxFactorajeMN = (String)parametros.get(7);
			String strPlazoMaxFactorajeDL = (String)parametros.get(8);
			String  strBaseOperGenerica = (String)parametros.get(9);
			String strEmailContingencia = (String)parametros.get(10);
			String strHoraReportesEspeciales = (String)parametros.get(11);
			String strEmailNotificacionesNafin = (String)parametros.get(12);
			String optOperTel = (String)parametros.get(13);


			String   query_Porciento =
				" SELECT MAX (fn_aforo), MAX (fn_aforo_dl)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"  ;
			rs = con.queryDB(query_Porciento);
			if (rs.next()) {
				strPorciento = rs.getString(1);
				strPorcientoDL = rs.getString(2);
			}
			con.cierraStatement();

			Float fltPorcientoEPO = new Float(strPorciento);
			Float fltPorcientoEPODL = new Float(strPorcientoDL);
			Float fltPorcientoNuevo = new Float(strPorcientoActual);
			Float fltPorcientoNuevoDL = new Float(strPorcientoActualDL);

			System.out.println("fltPorcientoNuevo "+fltPorcientoNuevo);
			System.out.println("fltPorcientoEPO "+fltPorcientoEPO);

				if (!(fltPorcientoNuevoDL.doubleValue() >= fltPorcientoEPODL.doubleValue())) {
					bandera = 4;
				}else		if (fltPorcientoNuevoDL.doubleValue() > 100) {
					bandera = 5;
				}
				if (bandera == 0) {
					if (fltPorcientoNuevo.doubleValue() >= fltPorcientoEPO.doubleValue()){

					strHoraReportesEspeciales 	= strHoraReportesEspeciales 	== null || strHoraReportesEspeciales.trim().equals("")   ?"15:00":strHoraReportesEspeciales;
					strEmailNotificacionesNafin = strEmailNotificacionesNafin 	== null || strEmailNotificacionesNafin.trim().equals("") ?"aaltamirano@nafin.gob.mx":strEmailNotificacionesNafin;
					strEmailNotificacionesNafin = formateaCamposCorreo(strEmailNotificacionesNafin);

				String query_nafin =
					" UPDATE comcat_producto_nafin"   +
					"    SET in_dias_minimo = " + strDiasMin + "," +
					"        in_dias_maximo = " + strDiasMax + "," +
					"        fn_aforo = " + strPorcientoActual +"," +
					"        in_dias_vig_edo_cuenta = "+strDiasEdoCuenta+","+
					"        in_dias_vig_vencimientos = "+strDiasVencimiento+","+
					"        in_dias_vig_operados = "+strDiasOperados+","+
					"        ig_plazo_maximo_factoraje = "+strPlazoMaxFactorajeMN+","+
					"        ig_plazo_maximo_factoraje_dl = "+strPlazoMaxFactorajeDL+","+
					"        ig_base_oper_gral = "+ strBaseOperGenerica +","+
					"        cg_email_contingencia = '"+strEmailContingencia+"', "+
					"        fn_aforo_dl = "+strPorcientoActualDL+", "+
					"        cg_hora_reportes_esepciales = '"+strHoraReportesEspeciales+"', "+
					"		 cg_email_notificaciones_nafin = '"+strEmailNotificacionesNafin+"', " +
					"		 cs_valida_cve_cder_oper_tel = '"+optOperTel+"' "+
					"  WHERE ic_producto_nafin = 1"  ;
				if (fltPorcientoNuevo.doubleValue() <= 100){
					con.ejecutaSQL(query_nafin);
					con.terminaTransaccion(true);
					bandera = 1;
				}	else  {
					bandera = 3;
				}
			} else  {
				bandera = 2;
			}
		}
			if (bandera == 1)
				mensaje = "Los datos han sido actualizados.";
			if (bandera == 2)
				mensaje = "El porcentaje de anticipo debe ser mayor a " +strPorciento;
			if (bandera == 3)
				mensaje ="El porcentaje no puede ser mayor del 100 %";
			if (bandera == 4)
				mensaje ="El porcentaje de anticipo en d�lares debe ser mayor a " +strPorcientoDL;
			if (bandera == 5)
				mensaje= "El porcentaje en d�lares no puede ser mayor del 100 %";

      }catch(Exception e){
         e.printStackTrace();
         throw new RuntimeException(e);
      }finally{
         if(con.hayConexionAbierta()){
            con.cierraConexionDB();
         }
      }
		return mensaje;
   }

		/**
	 * Este metodo devuelve la clave de la pyme (IC_PYME) y su razon social
	 *
	 * @since 	Migracion_Nafin-Febrero-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	Registros con el query resultante
	 */
	public Registros getParametrosPymeNafinSeleccionIF(String numeroDeProveedor){
		log.info("ParametrosDescuentoBean::getParametrosPymeNafin(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(numeroDeProveedor == null || numeroDeProveedor.equals("")){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				"SELECT P.ic_pyme as pyme, P.cg_razon_social as nombre,P.CG_RFC as rfc" +
				" FROM comcat_pyme P ,comrel_nafin n " +
				" WHERE P.ic_pyme = n.ic_epo_pyme_if  " +
				" AND n.cg_tipo = 'P' " +
				" AND n.ic_nafin_electronico = ? "

			);

			log.debug("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			lVarBind.add(numeroDeProveedor);

			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getParametrosPymeNafin(Exception)");
			log.info("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			e.printStackTrace();
			throw new AppException("Error al obtener el nombre del proveedor");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getParametrosPYME(S)");
		}
		return registros;
	}
	/**
	 * Este metodo devuelve la clave de la pyme (IC_PYME) y su razon social
	 *
	 * @since 	Migracion_Nafin-Febrero-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	Registros con el query resultante
	 */
	public Registros getParametrosPymeNafin(String numeroDeProveedor){
		log.info("ParametrosDescuentoBean::getParametrosPymeNafin(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(numeroDeProveedor == null || numeroDeProveedor.equals("")){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT n.ic_epo_pyme_if  as pyme,  p.cg_razon_social as nombre,p.cg_rfc as rfc  "+
				" FROM comrel_nafin n, comcat_pyme p "+
				" WHERE n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = ? "
			);

			log.debug("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			lVarBind.add(numeroDeProveedor);

			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getParametrosPymeNafin(Exception)");
			log.info("numeroDeProveedor = " + numeroDeProveedor ); // Debug info
			e.printStackTrace();
			throw new AppException("Error al obtener el nombre del proveedor");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getParametrosPYME(S)");
		}
		return registros;
	}

	/**
	 * Se sobrecarga este m�todo
	 * Este metodo devuelve la clave de la pyme (IC_PYME) y su razon social
	 *
	 * @since 	Migracion_Nafin-Febrero-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	Registros con el query resultante
	 * @autor Hugo Vargas Cervantes
	 */
	public Registros getParametrosPymeNafin(String numeroDeProveedor, String ic_banco_fondeo, String ic_epo){
		log.info("ParametrosDescuentoBean::getParametrosPymeNafin(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(numeroDeProveedor == null || numeroDeProveedor.equals("")	|| ic_epo == null ||
			ic_epo.equals("")	||	ic_banco_fondeo == null || ic_banco_fondeo.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT pym.ic_pyme, pym.cg_razon_social"   +
				"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym , comcat_epo E"   +
				"  WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
				"   AND cpe.ic_pyme = pym.ic_pyme"   +
				"   AND crn.ic_nafin_electronico = ?"   +
				"   AND crn.cg_tipo = ? "   +
				"   AND cpe.cs_habilitado = ? "   +
				"   AND cpe.cg_pyme_epo_interno IS NOT NULL"  +
				"	 AND cpe.ic_epo = E.ic_epo " +
				"	 AND E.ic_banco_fondeo = ? "+
				"   AND cpe.ic_epo = ? "
			);

			lVarBind.add(numeroDeProveedor);
			lVarBind.add("P");
			lVarBind.add("S");
			lVarBind.add(ic_banco_fondeo);
			lVarBind.add(ic_epo);

			log.debug("parametros = " + lVarBind ); // Debug info
			log.debug("query = " + qrySentencia.toString() ); // Debug info
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getParametrosPymeNafin(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener el nombre del proveedor");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getParametrosPYME(S)");
		}
		return registros;
	}

	/**
	 * Se sobrecarga este m�todo
	 * Este metodo devuelve la clave de la pyme (IC_PYME) y su razon social
	 *
	 * @since 	Migracion_Nafin-Febrero-2013
	 * @param 	id_usuario
	 * @return 	Registros con el query resultante
	 * @autor Hugo Vargas Cervantes
	 */
	public Registros getUsuarioCesion(String id_usuario){
		log.info("ParametrosDescuentoBean::getUsuarioCesion(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(id_usuario == null || id_usuario.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT ic_usuario, cs_estatus_mail, '13nafin/13forma16Ext.jsp' origen FROM seg_cve_cesion"+
				" WHERE ic_usuario= ? "
			);

			lVarBind.add(id_usuario);

			log.debug("parametros = " + lVarBind ); // Debug info
			log.debug("query = " + qrySentencia.toString() ); // Debug info
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getUsuarioCesion(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getUsuarioCesion(S)");
		}
		return registros;
	}

	/**
	 *
	 * Este metodo obtiene las cuentas bancarias relacionadas con una pyme
	 *
	 * @since 	Migracion_Nafin-marzo-2013
	 * @param 	pyme
	 * @return 	Registros con el query resultante
	 * @autor Gerardo Perez Garcia
	 */
		public Registros getCtasPyme(String ic_pyme){
		log.info("ParametrosDescuentoBean::getCtasPyme(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(ic_pyme == null || ic_pyme.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				"select cb.ic_cuenta_bancaria as clave, cb.ic_moneda, m.cd_nombre||'-'||  " +
					"cb.cg_banco||'-Cuenta:'|| cb.cg_numero_cuenta||''||   " +
					"(case when cb.cs_convenio_unico='S' then  ' Convenio Unico' else '' end) as descripcion   " +
					"from comrel_cuenta_bancaria cb, comcat_moneda m  " +
					"where cb.ic_pyme = ? " +
					" and cs_borrado = 'N'  " +
					"and m.ic_moneda = cb.ic_moneda  " +
					"and m.ic_moneda in (1,54)  " +
					"order by cb.ic_moneda "

			);

			lVarBind.add(ic_pyme);

			log.debug("parametros = " + lVarBind ); // Debug info
			log.debug("query = " + qrySentencia.toString() ); // Debug info
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getCtasPyme(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getCtasPyme(S)");
		}
		return registros;
	}
	public Registros obtenerCuentasBancarias(String icPyme,String moneda){
	log.info("ParametrosDescuentoBean::getCtasPyme(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(icPyme == null || icPyme.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT cb.cg_banco,"   +
				"     cb.cg_numero_cuenta,"   +
				"     cb.ic_cuenta_bancaria, "   +
				"     cb.cs_convenio_unico "   +
				" FROM comrel_cuenta_bancaria cb "   +
				" WHERE cb.ic_pyme = ? "   +
				" AND cb.cs_borrado = 'N' "   +
				" AND cb.ic_moneda = ? "   +
				" ORDER BY 1"

			);

			lVarBind.add(icPyme);
			lVarBind.add(moneda);
			log.debug("parametros = " + lVarBind ); // Debug info
			log.debug("query = " + qrySentencia.toString() ); // Debug info
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getCtasPyme(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getCtasPyme(S)");
		}
		return registros;
	}

	public boolean obtenerInstruccionIrrevocable(String icPyme,String cboIF,String Epo){
	log.info("ParametrosDescuentoBean::getCtasPyme(E)");

		boolean pymeExisteContratoActivo = false;
		AccesoDB 		con 				= new AccesoDB();
		PreparedStatement ps = null;
		if(icPyme == null || icPyme.equals("")	){
			return false;
		}

		try
		{
			con.conexionDB();

			 String qryContratoActivo = " SELECT COUNT(*) as CONTRATOS " +
                                    " FROM com_solic_mand_doc " +
                                    " WHERE ic_estatus_man_doc='2' " +  // Aceptado
                                    " AND ic_pyme=? " +
                                    " AND ic_if=? " +
                                    " AND ic_epo=? ";

         ps = con.queryPrecompilado(qryContratoActivo);
         ps.setString(1,icPyme);
         ps.setString(2,cboIF);
         ps.setString(3,Epo);
         ResultSet rs = ps.executeQuery();


         if(rs.next()){
            if(Integer.parseInt(rs.getString("CONTRATOS"))>0){
               pymeExisteContratoActivo = true;
            }
         }

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getCtasPyme(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getCtasPyme(S)");
		}
		return pymeExisteContratoActivo;
	}
  	public Registros obtenerCuentaSeleccionada(String icPyme,String moneda,String ic_if,String ic_epo){
	log.info("ParametrosDescuentoBean::getCtasPyme(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		if(icPyme == null || icPyme.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT pi.IC_CUENTA_BANCARIA, cs_vobo_if, " +
					" 	pi.cs_opera_descuento, '15admnafinparam02.jsp' as pantalla  " +
					" FROM COMREL_PYME_IF pi, comrel_cuenta_bancaria cb " +
					" WHERE pi.IC_CUENTA_BANCARIA = cb.IC_CUENTA_BANCARIA " +
					" 	AND cb.IC_PYME = ?  " +
					" 	AND cb.ic_moneda = ? " +
					" 	AND IC_IF =  ?  " +
					" 	AND IC_EPO = ?  " +
					" 	AND cb.cs_borrado = 'N'  " +
					" 	AND pi.CS_BORRADO = 'N'  " +
					" GROUP BY pi.IC_CUENTA_BANCARIA, cs_vobo_if, pi.cs_opera_descuento "

			);

			lVarBind.add(icPyme);
			lVarBind.add(moneda);
			lVarBind.add(ic_if);
			lVarBind.add(ic_epo);
			log.debug("parametros = " + lVarBind ); // Debug info
			log.debug("query = " + qrySentencia.toString() ); // Debug info
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getCtasPyme(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getCtasPyme(S)");
		}
		return registros;
	}


	public boolean cambiaCuentas(String clavesIF,String icPyme,String cuentaNueva,String iNoUsuario,String strNombreUsuario){

		boolean bandera=true;
		List params = new ArrayList();
		boolean bOk = true;
		boolean banderaPEF = false;
		boolean banderavobo = false;
		String qryACuentaIF = "";
		String qryTraeIF = "";
		String estaAutorizada="N";
		String qryAutorizada="";
		String qryReq8 = "";
		String operaDescuento = "";
		String qry = "";
		String nafinelec = "";
		String fechavoboorig = "";
		String camposvobo = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String tieneConvenioUnico = "N";//FODEA 032 - 2010 ACF

		AccesoDB con = new AccesoDB();

		String cambiosCuenta  = "";
		String existeHisCuenta = "";//agregados 12/08/2010
		String cgBancoAntQuery = "";
		String cgBancoNuevoQuery = "";
		String cgBancoAntDato = "";
		String cgBancoNuevoDato = "";

		String cgNumCuentAntQuery = "";
		String cgNumCuentNuevoQuery = "";
		String cgNumCuentaAntDato = "";
		String cgNumCuentaNuevoDato = "";
		String autorizaIfcambioCta = "N";
		StringBuffer cadenasIf= new StringBuffer(clavesIF);
		cadenasIf=cadenasIf.deleteCharAt(cadenasIf.length()-1);

		String ic_moneda="";
		String ic_cuentaAnterior="";
		String ic_if="";
		String ic_epo="";
		String cs_vobo="";
		String fechaVobo="";
		String queryGetCuentas="SELECT   pi.IC_IF,pi.IC_EPO,cb.IC_CUENTA_BANCARIA, pi.CS_VOBO_IF,cb.IC_MONEDA, " +
										"    TO_CHAR(pi.df_vobo_if,'dd/mm/yyyy hh24:mi:ss') as FECHA_VOBO"+
										"    FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb " +
										"   WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
										"     AND cb.cs_borrado = 'N'  " +
										"     AND pi.cs_borrado = 'N' " +
										"     AND cb.IC_MONEDA = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) " +
										"     AND cb.ic_cuenta_bancaria not in (?)  " +
										"     AND pi.ic_if in ("+cadenasIf+") " +
										"     AND cb.ic_pyme = ? ";
	try{
	con.conexionDB();

			params = new ArrayList();
			params.add(cuentaNueva);
			params.add(cuentaNueva);

			params.add(icPyme);
			Registros regCuentas = con.consultarDB(queryGetCuentas, params);

			while(regCuentas.next()){
			ic_if=regCuentas.getString("IC_IF");
			ic_moneda=regCuentas.getString("IC_MONEDA");
			ic_cuentaAnterior=regCuentas.getString("IC_CUENTA_BANCARIA");
			ic_epo=regCuentas.getString("IC_EPO");
			cs_vobo=regCuentas.getString("CS_VOBO_IF");
			fechaVobo=regCuentas.getString("FECHA_VOBO");
			fechaVobo="";

			if(cs_vobo.equals("S")){
				banderavobo = true;
				fechavoboorig=regCuentas.getString("FECHA_VOBO");
				}

					//BORRAMOS LA RELACION
					qryTraeIF =
							"UPDATE COMREL_PYME_IF SET CS_BORRADO = 'S' " +
							" WHERE IC_CUENTA_BANCARIA = " + ic_cuentaAnterior +
							" AND IC_IF = " + ic_if + " AND ic_epo = " + ic_epo;
					System.out.println("qryTraeIF::: "+qryTraeIF);
					con.ejecutaSQL(qryTraeIF);
					bOk = true;

					if(existeRegistroRelacionEpoPymeIf(ic_epo, cuentaNueva, ic_if)) {

						//QUE SE AUTO AUTORIZE SI YA ANTES TIENE LA MISMA PYME CON LA MISMA MONEDA CUENTAS AUTORIZADAS
						estaAutorizada = "N";
						qryAutorizada =
								"SELECT count(1), '15ADMNAFINPARAM02A' " 					+
								" FROM  COMREL_PYME_IF          PI, " 					+
								"       COMREL_CUENTA_BANCARIA  CB, " 					+
								"       COMREL_IF_EPO           IE  " 					+
								" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
								" AND   PI.IC_IF              = IE.IC_IF "      		+
								" AND   PI.IC_EPO             = IE.IC_EPO "     		+
								" AND   PI.IC_IF              = ? " + // txtaNoIF 		+
								" AND   CB.IC_PYME            = ? " + // Pyme  		+
								" AND   CB.IC_MONEDA          = ? " + // cboMoneda   		+
								" AND   PI.CS_VOBO_IF         = 'S' "           		+
								" AND   PI.CS_BORRADO         = 'N' "           		+
								" AND   CB.CS_BORRADO         = 'N' "           		+
								" AND   IE.CS_VOBO_NAFIN      = 'S' "           		+
								" AND   IE.CS_ACEPTACION      = 'S' " ;
						rs = null;
						System.out.println("\nqryAutorizada::: "+qryAutorizada);
						ps = con.queryPrecompilado(qryAutorizada);
						ps.setInt(1, Integer.parseInt(ic_if));
						ps.setInt(2, Integer.parseInt(icPyme));
						ps.setInt(3, Integer.parseInt(ic_moneda));
						rs = ps.executeQuery();
						if(rs.next()){
							if(rs.getInt(1)>0) {
								//estaAutorizada="S";
							}
							rs.close();
						}
						if(banderavobo){
							camposvobo = ", cs_cambio_cuenta = 'S', df_vobo_if = to_date('"+fechavoboorig+"','dd/mm/yyyy hh24:mi:ss')";
						}else{
						   camposvobo=" , df_vobo_if = SYSDATE , cs_cambio_cuenta = 'S'  ";  //se le coloco para activar el cambio de cuenta

						}
						//FODEA 032 - 2010 ACF (I)
						//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
						//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
						StringBuffer strSQL = new StringBuffer();
						strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
						System.out.println("..:: strSQL: "+strSQL.toString());
						System.out.println("..:: varBind: ["+ic_epo+"]");
						ps = con.queryPrecompilado(strSQL.toString());
						ps.setInt(1, Integer.parseInt(ic_epo));
						rs = ps.executeQuery();
						while (rs.next()) {
							tieneConvenioUnico = rs.getString("cs_convenio_unico") == null?"N":rs.getString("cs_convenio_unico");
						}
						rs.close();
						ps.close();
						//FODEA 032 - 2010 ACF (F)
						qryTraeIF =
								"UPDATE COMREL_PYME_IF SET CS_BORRADO = 'N' "+camposvobo+" , CS_VOBO_IF = 'N' " + operaDescuento + //", IG_ORDEN = null "+
								" WHERE IC_CUENTA_BANCARIA = " + cuentaNueva + " AND IC_IF = " + ic_if + " AND ic_epo = " + ic_epo;
						System.out.println("\nqryTraeIF::: "+qryTraeIF + "<br>");
						con.ejecutaSQL(qryTraeIF);
						bOk = true;

						if(ps != null) ps.close();
						if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S")) || banderaPEF) { //FODEA 032 - 2010 ACF
							qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H' WHERE ic_epo = " + ic_epo + " AND ic_pyme = " + icPyme;
							System.out.println("\nqryReq8::: "+qryReq8);
							con.ejecutaSQL(qryReq8);
							bOk = true;
						} else {
								bOk = true;
						}
					}else{ //Si no existe el registro de relacion epo-pyme-if

						//QUE SE AUTO AUTORIZE SI YA ANTES TIENE LA MISMA PYME CON LA MISMA MONEDA CUENTAS AUTORIZADAS
						estaAutorizada = "N";
						qryAutorizada =
								"SELECT count(1), '15ADMNAFINPARAM02A' " 					+
								" FROM  COMREL_PYME_IF          PI, " 					+
								"       COMREL_CUENTA_BANCARIA  CB, " 					+
								"       COMREL_IF_EPO           IE  " 					+
								" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
								" AND   PI.IC_IF              = IE.IC_IF "      		+
								" AND   PI.IC_EPO             = IE.IC_EPO "     		+
								" AND   PI.IC_IF              = ? " + //txtaNoIF 		+
								" AND   CB.IC_PYME            = ? " + //Pyme  		+
								" AND   CB.IC_MONEDA          = ? " + //cboMoneda   		+
								" AND   PI.CS_VOBO_IF         = 'S' "           		+
								" AND   PI.CS_BORRADO         = 'N' "           		+
								" AND   CB.CS_BORRADO         = 'N' "           		+
								" AND   IE.CS_VOBO_NAFIN      = 'S' "           		+
								" AND   IE.CS_ACEPTACION      = 'S' " ;
						System.out.println("\nqryAutorizada:: "+qryAutorizada);
						rs = null;
						ps = con.queryPrecompilado(qryAutorizada);
						ps.setInt(1, Integer.parseInt(ic_if));
						ps.setInt(2, Integer.parseInt(icPyme));
						ps.setInt(3, Integer.parseInt(ic_moneda));
						rs = ps.executeQuery();
						if(rs.next()){
							if(rs.getInt(1)>0) {
								//estaAutorizada="S";
							}
							rs.close();
						}
						if(ps != null) ps.close();
						//FODEA 032 - 2010 ACF (I)
						//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
						//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
						StringBuffer strSQL = new StringBuffer();
						strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
						System.out.println("..:: strSQL: "+strSQL.toString());
						System.out.println("..:: varBind: ["+ic_epo+"]");
						ps = con.queryPrecompilado(strSQL.toString());
						ps.setInt(1, Integer.parseInt(ic_epo));
						rs = ps.executeQuery();
						while (rs.next()) {
							tieneConvenioUnico = rs.getString("cs_convenio_unico") == null?"N":rs.getString("cs_convenio_unico");
						}
						rs.close();
						ps.close();

						qryACuentaIF =
								" INSERT INTO COMREL_PYME_IF (ic_cuenta_bancaria, " +
								" ic_epo, ic_if, cs_vobo_if, cs_borrado) " +
								" VALUES(?, ?, ?, ?, ?) ";

						params = new ArrayList();
						params.add(new Integer(cuentaNueva));
						params.add(new Integer(ic_epo));
						params.add(new Integer(ic_if));
						params.add("N");
						params.add("N");
						//params.add(operaDescuento);

						System.out.println("\ninserta 1::: "+qryACuentaIF + "  binds: " +  params);
						con.ejecutaUpdateDB(qryACuentaIF, params);
						bOk = true;

						if(banderavobo){
							camposvobo = " cs_cambio_cuenta = 'S', df_vobo_if = to_date('"+fechavoboorig+"','dd/mm/yyyy hh24:mi:ss')";
						}else{
							camposvobo="  df_vobo_if = SYSDATE ";
						}
							qryACuentaIF = "UPDATE COMREL_PYME_IF set "+ camposvobo +
							" where  IC_CUENTA_BANCARIA = " + cuentaNueva + " AND IC_IF = " + ic_if + " AND ic_epo = " + ic_epo;
							System.out.println("\nactualiza para mantener fecha::: "+qryACuentaIF);
							con.ejecutaSQL(qryACuentaIF);
							bOk = true;


						if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S")) || banderaPEF) { //FODEA 032 - 2010 ACF
							qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H' WHERE ic_epo = " + ic_epo + " AND ic_pyme = " + icPyme;
							System.out.println("\nqryReq8:: "+qryReq8);
							con.ejecutaSQL(qryReq8);
							bOk = true;
						}else {
							bOk = true;
						}
					} // fin if

			qry = "select ic_nafin_electronico from comrel_nafin where ic_epo_pyme_if = ? and cg_tipo = 'P'";
			ps = con.queryPrecompilado(qry);
			ps.setInt(1, Integer.parseInt(icPyme));
			rs = ps.executeQuery();
			if(rs.next()){
				nafinelec = rs.getString("ic_nafin_electronico");
				rs.close();
			}

					/*Obtener cgNumeroCuenta anterior*/
					cgNumCuentAntQuery =
							"SELECT CG_NUMERO_CUENTA CUENTA_ANTERIOR " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(ic_cuentaAnterior);
					Registros regCtaAnterior = con.consultarDB(cgNumCuentAntQuery, params);

					if(regCtaAnterior.next()){
						cgNumCuentaAntDato = regCtaAnterior.getString("CUENTA_ANTERIOR");
					}


					/*Obtener cgNumeroCuenta nueva*/
					cgNumCuentNuevoQuery =
							"SELECT CG_NUMERO_CUENTA CUENTA_NUEVA " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";

					params = new ArrayList();
					params.add(new Integer(cuentaNueva));

					Registros regCtaNueva = con.consultarDB(cgNumCuentNuevoQuery, params);

					if(regCtaNueva.next()){
						cgNumCuentaNuevoDato = regCtaNueva.getString("CUENTA_NUEVA");
					}

					/*Obtener cg_bancos anterior*/
					cgBancoAntQuery =
							" SELECT CG_BANCO BANCO_ANTERIOR " +
							" FROM COMREL_CUENTA_BANCARIA " +
							" WHERE IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(new Integer(ic_cuentaAnterior));
					Registros regBancoAnt = con.consultarDB(cgBancoAntQuery, params);

					//cgBancoAnt = con.queryDB(cgBancoAntQuery);

					if(regBancoAnt.next()){
						cgBancoAntDato = regBancoAnt.getString("BANCO_ANTERIOR");
					}

					/*Obtener cg_bancos nuevo*/
					cgBancoNuevoQuery = "SELECT CG_BANCO BANCO_NUEVO " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";

					params = new ArrayList();
					params.add(new Integer(cuentaNueva));
					Registros regBancoNuevo = con.consultarDB(cgBancoNuevoQuery, params);

					//cgBancoAnt = con.queryDB(cgBancoAntQuery);

					if(regBancoNuevo.next()){
						cgBancoNuevoDato = regBancoNuevo.getString("BANCO_NUEVO");
					}
			/*Se registra el cambio de cuenta en COMHIS_CAMBIOS_CTA*/
					String ic_usuario = iNoUsuario; //Variable de session que guarda el ic_usuario
					String cg_nombre_usuario=strNombreUsuario; //Variable de session que guarda el nombre completo del usuario

					int count=0;
					existeHisCuenta=
							" SELECT COUNT(1) FROM COMHIS_CAMBIOS_CTA "+
							" WHERE IC_EPO = ?  AND IC_IF = ? AND IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(new Integer(ic_epo));
					params.add(new Integer(ic_if));
					params.add(new Integer(cuentaNueva));
					Registros regCtaHis = con.consultarDB(existeHisCuenta, params);
					if(regCtaHis.next()) {
						count = Integer.parseInt(regCtaHis.getString(1));
					}

					if(count == 0){
						cambiosCuenta  =
								"INSERT INTO COMHIS_CAMBIOS_CTA(ic_if, ic_epo, df_cambio_cta, cg_cuenta_ant, cg_bancos_ant, cg_cuenta, cg_bancos, ic_pyme, ic_cuenta_bancaria, ic_usuario, cg_nombre_usuario, cs_autoriza_if) " +
								"VALUES(" + ic_if + ", " + ic_epo + ", SYSDATE, '" + cgNumCuentaAntDato + "','" + cgBancoAntDato + "', '" + cgNumCuentaNuevoDato + "', '" + cgBancoNuevoDato + "', '" + icPyme +"', " + cuentaNueva + ", '" + ic_usuario + "', '" + cg_nombre_usuario + "','"+autorizaIfcambioCta+"')";
					}else{
						cambiosCuenta  =
								"UPDATE COMHIS_CAMBIOS_CTA SET CS_AUTORIZA_IF='"+autorizaIfcambioCta+"', DF_CAMBIO_CTA = SYSDATE "+
								" WHERE IC_EPO = "+ ic_epo +" AND IC_IF = "+ic_if+" AND IC_CUENTA_BANCARIA = "+cuentaNueva;
					}

					System.out.println("Cambios cuenta " + cambiosCuenta );


					con.ejecutaSQL(cambiosCuenta);
			con.terminaTransaccion(true);

			guardaBit(nafinelec, iNoUsuario, ic_epo, ic_if, ic_cuentaAnterior, cuentaNueva);

			AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB",AutorizacionDescuento.class);

			//Fodea 017-2013 autoriza cuenta si opera fiso
			if(ic_moneda.equals("54")&&BeanAutDescuento.getOperaIFEPOFISO(ic_if,ic_epo)){
				CuentasPymeIFWS  cuentaConfirmar = new CuentasPymeIFWS();
				cuentaConfirmar.setRfcPyme(BeanAutDescuento.getRFCPyme(icPyme));
				cuentaConfirmar.setClaveEpo(ic_epo);
				cuentaConfirmar.setClaveMoneda(ic_moneda);
				BeanAutDescuento.confirmacionCuentasIFWS ( ic_if, iNoUsuario,new CuentasPymeIFWS[]{ cuentaConfirmar});
			}

		}
	} catch(Throwable t) {
			bOk = false;
			bandera=false;
			throw new AppException("Error al almacenar la parametrizacion de cuentas bancarias", t);
		}finally{
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
				return bandera;
	}


	public String cambioCuentaIndividual(String EjecutarCambioCtaCliente,String txtaNoIF,String chkIF,
	String chkOperaIF,String hidOperaIF,String txtCtaAnterior,String rdCta,String cboMoneda,
	String Epo,String Pyme,String iNoUsuario,String strNombreUsuario, String ic_nafin_electronico , String login ){
	log.debug("cambioCuentaIndividual (E)::");
		String cadenaRetorna="";
		List params = new ArrayList();
		boolean bOk = true;
		boolean banderaPEF = false;
		boolean banderavobo = false;

		String qryACuentaIF = "";
		String qryTraeIF = "";
		String estaAutorizada="N";
		String qryAutorizada="";
		String qryReq8 = "";
		String operaDescuento = "";
		String qry = "";
		String nafinelec = "";
		String mensajepef = "";
		String fechavoboorig = "";
		String camposvobo = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String tieneConvenioUnico = "N";//FODEA 032 - 2010 ACF

		AccesoDB con = new AccesoDB();

		/*Parametros y varaiable(s) para el cambio de Cambio Cta Cliente */
		/*Recepcion de parametros para el cambio de cuenta del cliente*/

		String cambiosCuenta  = "";
		String existeHisCuenta = "";//agregados 12/08/2010
		boolean cambioCtaCliente = false;
		String cgBancoAntQuery = "";
		String cgBancoNuevoQuery = "";
		String cgBancoAntDato = "";
		String cgBancoNuevoDato = "";

		String cgNumCuentAntQuery = "";
		String cgNumCuentNuevoQuery = "";
		String cgNumCuentaAntDato = "";
		String cgNumCuentaNuevoDato = "";
		String autorizaIfcambioCta = "N";
		if(EjecutarCambioCtaCliente.equals("si")) {
			cambioCtaCliente = true;
		}
		boolean CtaClabe = false;
	  String mensajeCtaClabe = "";


		try{
			con.conexionDB();

				//ENTRA SOLAMENTE SI SE SELECCIONO EL CHECKBOX
				if(chkIF.equals("1")){
					String qrypef = "SELECT ig_if_primer_piso FROM com_param_gral";
					System.out.println("\nqrypef1::: "+qrypef);
					Registros regpef = con.consultarDB(qrypef);
					//ResultSet rspef = con.queryDB(qrypef);
					regpef.next();
					int igifppiso = Integer.parseInt(regpef.getString(1));

					log.debug("igifppiso ::"+igifppiso);
					log.debug("txtaNoIF ::"+txtaNoIF);

					if(igifppiso == Integer.parseInt(txtaNoIF)){
						qrypef =
								" SELECT COUNT(*) FROM comcat_epo " +
								" WHERE ic_epo = ? " +
								" AND ic_tipo_organismo IS NOT NULL ";
						System.out.println("\nqrypef2::: "+qrypef);
						//ResultSet rspef1 = con.queryDB(qrypef);
						params = new ArrayList();
						params.add(new Integer(Epo));
						Registros regpef1 = con.consultarDB(qrypef, params);
						regpef1.next();
						if(Integer.parseInt(regpef1.getString(1))>0){
							banderaPEF = true;
							System.out.println(" ::::: EPO PEF ::::: ");
						}
					}

					log.debug("banderaPEF ::"+banderaPEF);
					log.debug("txtCtaAnterior ::"+txtCtaAnterior);
					log.debug("rdCta ::"+rdCta);
					log.debug("hidOperaIF ::"+hidOperaIF);
					log.debug("chkOperaIF ::"+chkOperaIF);

					if(txtCtaAnterior != null&&!txtCtaAnterior.equals("")){

						if(!txtCtaAnterior.equals(rdCta)){
							//fodea 034
							//revisamos si esta autorizada
							qry = " SELECT cs_vobo_if, TO_CHAR(df_vobo_if,'dd/mm/yyyy hh24:mi:ss') FROM comrel_pyme_if " +
									" WHERE IC_CUENTA_BANCARIA = ? AND IC_IF = ? AND ic_epo = ? ";

							//ResultSet rsvobo = con.queryDB(qry);
							params = new ArrayList();
							params.add(new Integer(txtCtaAnterior));
							params.add(new Integer(txtaNoIF));
							params.add(new Integer(Epo));
							Registros regvobo = con.consultarDB(qry, params);
							if(regvobo.next()){
								if(regvobo.getString(1).equals("S")){
									banderavobo = true;
									fechavoboorig = regvobo.getString(2);
								}
							}

							log.debug("banderavobo (*) ::"+banderavobo);
							log.debug("fechavoboorig df_vobo_if (*)   ::"+fechavoboorig);

							//BORRAMOS LA RELACION
							qryTraeIF =
									"UPDATE COMREL_PYME_IF SET CS_BORRADO = 'S' " +
									" WHERE IC_CUENTA_BANCARIA = " + txtCtaAnterior +
									" AND IC_IF = " + txtaNoIF + " AND ic_epo = " + Epo;
							System.out.println("qryTraeIF::: "+qryTraeIF);
							con.ejecutaSQL(qryTraeIF);
							bOk = true;

							if(existeRegistroRelacionEpoPymeIf(Epo, rdCta, txtaNoIF)) {
								log.debug("Si  existe el registro de relacion epo-pyme-if  (-)  ::");

								log.debug("chkOperaIF   (-) ::"+chkOperaIF);

								if(chkOperaIF.equals("1")){
									operaDescuento = ", CS_OPERA_DESCUENTO = 'S' ";
								} else {
									operaDescuento = ", CS_OPERA_DESCUENTO = 'N' ";
								}
								//QUE SE AUTO AUTORIZE SI YA ANTES TIENE LA MISMA PYME CON LA MISMA MONEDA CUENTAS AUTORIZADAS
								estaAutorizada = "N";
								qryAutorizada =
										"SELECT count(1), '15ADMNAFINPARAM02A' " 					+
										" FROM  COMREL_PYME_IF          PI, " 					+
										"       COMREL_CUENTA_BANCARIA  CB, " 					+
										"       COMREL_IF_EPO           IE  " 					+
										" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
										" AND   PI.IC_IF              = IE.IC_IF "      		+
										" AND   PI.IC_EPO             = IE.IC_EPO "     		+
										" AND   PI.IC_IF              = ? " + // txtaNoIF 		+
										" AND   CB.IC_PYME            = ? " + // Pyme  		+
										" AND   CB.IC_MONEDA          = ? " + // cboMoneda   		+
										" AND   PI.CS_VOBO_IF         = 'S' "           		+
										" AND   PI.CS_BORRADO         = 'N' "           		+
										" AND   CB.CS_BORRADO         = 'N' "           		+
										" AND   IE.CS_VOBO_NAFIN      = 'S' "           		+
										" AND   IE.CS_ACEPTACION      = 'S' " ;

								log.debug("\nqryAutorizada:::(-) "+qryAutorizada);
								log.debug("\txtaNoIF:::(-) "+txtaNoIF);
								log.debug("cboMoneda:: (-)  "+cboMoneda);
								log.debug("Pyme::  (-) "+Pyme);

								rs = null;
								ps = con.queryPrecompilado(qryAutorizada);
								ps.setInt(1, Integer.parseInt(txtaNoIF));
								ps.setInt(2, Integer.parseInt(Pyme));
								ps.setInt(3, Integer.parseInt(cboMoneda));
								rs = ps.executeQuery();
								if(rs.next()){
									if(rs.getInt(1)>0) {
										estaAutorizada="S";
									}
									rs.close();
								}

								log.debug("estaAutorizada:: (-) "+estaAutorizada);

								if(banderavobo){
									camposvobo = ", cs_cambio_cuenta = 'S', df_vobo_if = to_date('"+fechavoboorig+"','dd/mm/yyyy hh24:mi:ss')";
								}
								//FODEA 032 - 2010 ACF (I)
								//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
								//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
								StringBuffer strSQL = new StringBuffer();
								strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
								log.debug("..:: strSQL: (-) "+strSQL.toString());
								log.debug("..:: varBind: (-) ["+Epo+"]");
								ps = con.queryPrecompilado(strSQL.toString());
								ps.setInt(1, Integer.parseInt(Epo));
								rs = ps.executeQuery();
								while (rs.next()) {
									tieneConvenioUnico = rs.getString("cs_convenio_unico") == null?"N":rs.getString("cs_convenio_unico");
								}
								rs.close();
								ps.close();

								log.debug(" tieneConvenioUnico: (-) "+tieneConvenioUnico);
								//FODEA 032 - 2010 ACF (F)
								qryTraeIF =
										"UPDATE COMREL_PYME_IF SET CS_BORRADO = 'N' "+camposvobo+" , CS_VOBO_IF = 'N' " + operaDescuento + //", IG_ORDEN = null "+
										" WHERE IC_CUENTA_BANCARIA = " + rdCta + " AND IC_IF = " + txtaNoIF + " AND ic_epo = " + Epo;
								log.debug("\nqryTraeIF::: (-) "+qryTraeIF + "<br>");
								con.ejecutaSQL(qryTraeIF);
								bOk = true;

								if(ps != null) ps.close();
								if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S")) || banderaPEF) { //FODEA 032 - 2010 ACF
									qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H' WHERE ic_epo = " + Epo + " AND ic_pyme = " + Pyme;
									log.debug("\nqryReq8:::(-)  "+qryReq8);
									con.ejecutaSQL(qryReq8);
									bOk = true;
								} else {
										bOk = true;
								}
							}else{ //Si no existe el registro de relacion epo-pyme-if

								log.debug("Si no existe el registro de relacion epo-pyme-if  (*)  ::");
								log.debug("chkOperaIF:: (*)  "+chkOperaIF);

								if(chkOperaIF.equals("1")){
									operaDescuento = "S";
								} else {
									operaDescuento = "N";
								}

								log.debug("operaDescuento (*) ::  "+operaDescuento);

								//QUE SE AUTO AUTORIZE SI YA ANTES TIENE LA MISMA PYME CON LA MISMA MONEDA CUENTAS AUTORIZADAS
								estaAutorizada = "N";
								qryAutorizada =
										"SELECT count(1), '15ADMNAFINPARAM02A' " 					+
										" FROM  COMREL_PYME_IF          PI, " 					+
										"       COMREL_CUENTA_BANCARIA  CB, " 					+
										"       COMREL_IF_EPO           IE  " 					+
										" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
										" AND   PI.IC_IF              = IE.IC_IF "      		+
										" AND   PI.IC_EPO             = IE.IC_EPO "     		+
										" AND   PI.IC_IF              = ? " + //txtaNoIF 		+
										" AND   CB.IC_PYME            = ? " + //Pyme  		+
										" AND   CB.IC_MONEDA          = ? " + //cboMoneda   		+
										" AND   PI.CS_VOBO_IF         = 'S' "           		+
										" AND   PI.CS_BORRADO         = 'N' "           		+
										" AND   CB.CS_BORRADO         = 'N' "           		+
										" AND   IE.CS_VOBO_NAFIN      = 'S' "           		+
										" AND   IE.CS_ACEPTACION      = 'S' " ;

								log.debug("\nqryAutorizada:: "+qryAutorizada);

								rs = null;
								ps = con.queryPrecompilado(qryAutorizada);
								ps.setInt(1, Integer.parseInt(txtaNoIF));
								ps.setInt(2, Integer.parseInt(Pyme));
								ps.setInt(3, Integer.parseInt(cboMoneda));
								rs = ps.executeQuery();
								if(rs.next()){
									if(rs.getInt(1)>0) {
										estaAutorizada="S";
									}
									rs.close();
								}
								if(ps != null) ps.close();

								log.debug("estaAutorizada (*) ::  "+estaAutorizada);

								//FODEA 032 - 2010 ACF (I)
								//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
								//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
								StringBuffer strSQL = new StringBuffer();
								strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
								log.debug("..:: strSQL: "+strSQL.toString());
								log.debug("..:: varBind: ["+Epo+"]");
								ps = con.queryPrecompilado(strSQL.toString());
								ps.setInt(1, Integer.parseInt(Epo));
								rs = ps.executeQuery();
								while (rs.next()) {
									tieneConvenioUnico = rs.getString("cs_convenio_unico") == null?"N":rs.getString("cs_convenio_unico");
								}
								rs.close();
								ps.close();

								log.debug("tieneConvenioUnico (*) ::  "+tieneConvenioUnico);

								qryACuentaIF =
										" INSERT INTO COMREL_PYME_IF (ic_cuenta_bancaria, " +
										" ic_epo, ic_if, cs_vobo_if, cs_borrado, cs_opera_descuento) " +
										" VALUES(?, ?, ?, ?, ?, ?) ";

								params = new ArrayList();
								params.add(new Integer(rdCta));
								params.add(new Integer(Epo));
								params.add(new Integer(txtaNoIF));
								params.add("N");
								params.add("N");
								params.add(operaDescuento);

								log.debug("\ninserta 1::: "+qryACuentaIF + "  binds: " +  params);
								con.ejecutaUpdateDB(qryACuentaIF, params);
								bOk = true;

								log.debug("banderavobo (*) ::  "+banderavobo);
								if(banderavobo){
									qryACuentaIF = "UPDATE COMREL_PYME_IF set cs_cambio_cuenta = 'S', df_vobo_if = to_date('"+fechavoboorig+"','dd/mm/yyyy hh24:mi:ss') " +
									" where  IC_CUENTA_BANCARIA = " + rdCta + " AND IC_IF = " + txtaNoIF + " AND ic_epo = " + Epo;
									log.debug("\nactualiza para mantener fecha (*) ::: "+qryACuentaIF);
									con.ejecutaSQL(qryACuentaIF);
									bOk = true;
								}

								if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S")) || banderaPEF) { //FODEA 032 - 2010 ACF
									qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H' WHERE ic_epo = " + Epo + " AND ic_pyme = " + Pyme;
									log.debug("\nqryReq8:: "+qryReq8);
									con.ejecutaSQL(qryReq8);
									bOk = true;
								}else {
									bOk = true;
								}
							} // fin if
						}else if(!hidOperaIF.equals(chkOperaIF)){ // fin if(!txtCtaAnterior.equals(rdCta))
							//Es la misma cuenta, pero fue modificado el campo opera descuento
							log.debug("Es la misma cuenta, pero fue modificado el campo opera descuento::  ");

							if(chkOperaIF.equals("1")){
								operaDescuento = "S";
							} else {
								operaDescuento = "N";
							}
							qryTraeIF = "UPDATE COMREL_PYME_IF SET CS_OPERA_DESCUENTO = '"+operaDescuento+"' " +
								" WHERE IC_CUENTA_BANCARIA = " + txtCtaAnterior + " AND IC_IF = " + txtaNoIF + " AND ic_epo = " + Epo;
							log.debug("\nqryTraeIF:: "+qryTraeIF);
							con.ejecutaSQL(qryTraeIF);
							bOk = true;
						} // fin else if(!hidOperaIF.equals(chkOperaIF))
					}else{// Si no hay cuenta anterior autorizada

						log.debug("Si no hay cuenta anterior autorizada:: (2) ");

						if(chkOperaIF.equals("1")){
							operaDescuento = "S";
						} else {
							operaDescuento = "N";
						}
						//Fodea 019-2014(E)
						AutorizacionDescuentoBean BeanAutDescuento  = new AutorizacionDescuentoBean();
						String operaFide_Pyme =  this.getOperaFideicomisoPYME( Pyme );
						boolean OperaIFEPOFISO  =  BeanAutDescuento.getOperaIFEPOFISO(txtaNoIF,Epo);
						String vClabePYME =  this.getValidaCcuentaClablePyme(ic_nafin_electronico ) ; //Fodea 019-2014
						String  validaEpoRea=  	getValidaPymeEpoRea(Pyme, Epo ); // esta validacion es apra una reafiliaci�n de una pyme que opera fideicomiso en otra epo  que opera fideicomiso

						if( "S".equals(validaEpoRea) ) {

							if("S".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
								mensajeCtaClabe =  getcuentaClablePyme(ic_nafin_electronico, login, Epo, con );
								if(!mensajeCtaClabe.equals("")) {
									CtaClabe = true;
								}
							}else  if("N".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
								mensajeCtaClabe="La PYME no tiene parametrizada una Cuenta CLABE, favor de verificar";
								CtaClabe = true;

							}else  if("E".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
								mensajeCtaClabe="La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla";
								CtaClabe = true;
							}
								log.debug(" CtaClabe   " +CtaClabe +"   mensajeCtaClabe  :::::: "+mensajeCtaClabe);
						}

						//Fodea 019-2014(S)

						// codigo que se comenta porque la regla  no aplica
						//QUE SE AUTO AUTORIZE SI YA ANTES TIENE LA MISMA PYME CON LA MISMA MONEDA CUENTAS AUTORIZADAS
						/*estaAutorizada = "N";

						String cs_reafiliacion_auto = "N";
						//verifico , Si la relaci�n EPO-IF tiene parametrizado CS_REAFILIA_AUTO = N o N/A el proceso no realizar� la Reafiliaci�n Autom�tica.
						String query = "select  ri.CS_REAFILIA_AUTO  from COMREL_IF_EPO ri , com_reafiliacion_epo re "+
						" where ri.ic_epo = re.ic_epo  "+
						" and  re.CS_ESTATUS_REAFILIACION ='S'  "+
						"  and ri.ic_epo = ?  "+
						"  and ri.ic_if = ?  ";

						ps = con.queryPrecompilado(query);
						ps.clearParameters();
						ps.setInt(1, Integer.parseInt(Epo));
						ps.setInt(2, Integer.parseInt(txtaNoIF));
						rs = ps.executeQuery();
						if(rs!=null && rs.next()){
							cs_reafiliacion_auto =  rs.getString("CS_REAFILIA_AUTO").trim();
						}
						rs.close();
						ps.close();

						log.debug("cs_reafiliacion_auto   "+cs_reafiliacion_auto);
						if(cs_reafiliacion_auto.equals("S"))  {
						*/

						String  entidad_Gobierno =  getOpEntidadGobierno(Pyme );			//F034-2014

						if (!CtaClabe){ // SOLO SI NO HAY ERROR EN LA VALIDACION DE LA CUENTA CLABE REALIZA LA REAFILIACION

							qryAutorizada =
									"SELECT count(1), '15ADMNAFINPARAM02A' " 					+
									" FROM  COMREL_PYME_IF          PI, " 					+
									"       COMREL_CUENTA_BANCARIA  CB, " 					+
									"       COMREL_IF_EPO           IE  " 					;
								if(OperaIFEPOFISO ){
									qryAutorizada += " , comcat_if i ";
								}
								qryAutorizada +=" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
									" AND   PI.IC_IF              = IE.IC_IF "      		+
									" AND   PI.IC_EPO             = IE.IC_EPO "     		+
									" AND   CB.IC_PYME            = ? " + //Pyme  		+
									" AND   CB.IC_MONEDA          = ? " + //cboMoneda   		+
									" AND   PI.CS_VOBO_IF         = 'S' "           		+
									" AND   PI.CS_BORRADO         = 'N' "           		+
									" AND   CB.CS_BORRADO         = 'N' "           		+
									" AND   IE.CS_VOBO_NAFIN      = 'S' "           		+
									" AND   IE.CS_ACEPTACION      = 'S' " ;
									if(OperaIFEPOFISO  ){
									qryAutorizada +=" and i.CS_OPERA_FIDEICOMISO = 'S' "+
													" and pi.ic_if = i.ic_if ";
									}else {
										qryAutorizada +=" AND   PI.IC_IF              = ? " ; //txtaNoIF 		+
									}
							rs = null;
							ps = con.queryPrecompilado(qryAutorizada);
							ps.setInt(1, Integer.parseInt(Pyme));
							ps.setInt(2, Integer.parseInt(cboMoneda));
							if(!OperaIFEPOFISO ){
								ps.setInt(3, Integer.parseInt(txtaNoIF));
							}
							rs = ps.executeQuery();
							if(rs.next()){
								if(rs.getInt(1)>0){
									estaAutorizada="S";
									if (entidad_Gobierno.equals("S"))
										estaAutorizada="N";
								}
								rs.close();
							}
							if(ps != null) ps.close();
						//}
							log.debug("estaAutorizada:: (2)  "+estaAutorizada);
							//FODEA 032 - 2010 ACF (I)
							//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
							//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
							StringBuffer strSQL = new StringBuffer();
							strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
							log.debug("..:: strSQL: "+strSQL.toString());
							log.debug("..:: varBind: ["+Epo+"]");
							ps = con.queryPrecompilado(strSQL.toString());
							ps.setInt(1, Integer.parseInt(Epo));
							rs = ps.executeQuery();
							while (rs.next()) {
								tieneConvenioUnico = rs.getString("cs_convenio_unico") == null?"N":rs.getString("cs_convenio_unico");
							}
							rs.close();
							ps.close();

							log.debug("tieneConvenioUnico.:: "+tieneConvenioUnico);
							log.debug("banderaPEF.:: "+banderaPEF);
							log.debug("estaAutorizada.:: "+estaAutorizada);

							log.debug("operaFide_Pyme :::::  "+operaFide_Pyme);
							log.debug("OperaIFEPOFISO :::::  "+OperaIFEPOFISO);
							log.debug("vClabePYME :::::  "+vClabePYME);
							//Fodea 019-2014(S)

							if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S") ) || banderaPEF  )  { //FODEA 032 - 2010 ACF
								autorizaIfcambioCta = "S";
								log.debug("existeRegistroRelacionEpoPymeIf(Epo, rdCta, txtaNoIF):::.:: "+existeRegistroRelacionEpoPymeIf(Epo, rdCta, txtaNoIF));


								if(!existeRegistroRelacionEpoPymeIf(Epo, rdCta, txtaNoIF) ) {
										qryACuentaIF = "INSERT INTO COMREL_PYME_IF " +
												" (ic_cuenta_bancaria, ic_epo, ic_if, cs_vobo_if, " +
												" df_vobo_if, cs_borrado, cs_opera_descuento, DF_REASIGNO_CTA) " +
												" VALUES(?, ?, ?, ?, sysdate, ?, ?, sysdate)";
										params = new ArrayList();
										params.add(new Integer(rdCta));
										params.add(new Integer(Epo));
										params.add(new Integer(txtaNoIF));
										params.add("S");
										params.add("N");
										params.add(operaDescuento);

										log.debug("qryACuentaIF (1):: "+qryACuentaIF);
										log.debug("params (1):: "+params);

									} else {
										qryACuentaIF =
												" UPDATE COMREL_PYME_IF " +
												" SET " +
												" 	cs_vobo_if = ?, " +
												" 	df_vobo_if = SYSDATE, " +
												" 	cs_borrado = ?, " +
												" 	cs_opera_descuento = ?, " +
												" 	df_reasigno_cta = SYSDATE " +
												" WHERE ic_cuenta_bancaria = ? " +
												" 	AND ic_epo = ? " +
												" 	AND ic_if = ? ";
										params = new ArrayList();
										params.add("S");
										params.add("N");
										params.add(operaDescuento);
										params.add(new Integer(rdCta));
										params.add(new Integer(Epo));
										params.add(new Integer(txtaNoIF));

										log.debug("qryACuentaIF (1):: "+qryACuentaIF);
										log.debug("params (1):: "+params);

									}


							}else{ // if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S") ) || banderaPEF  )
								if(!existeRegistroRelacionEpoPymeIf(Epo, rdCta, txtaNoIF)) {
									qryACuentaIF =
											" INSERT INTO COMREL_PYME_IF (ic_cuenta_bancaria, " +
											" ic_epo, ic_if, cs_vobo_if, cs_borrado, " +
											" cs_opera_descuento, DF_REASIGNO_CTA) " +
											" VALUES(?, ?, ?, ?, ?, ?, "+("S".equals(estaAutorizada)?"sysdate":"null")+")";//FODEA 032 - 2010 ACF

									params = new ArrayList();
									params.add(new Integer(rdCta));
									params.add(new Integer(Epo));
									params.add(new Integer(txtaNoIF));
									params.add("N");
									params.add("N");
									params.add(operaDescuento);
									log.debug("qryACuentaIF (2) :: "+qryACuentaIF);
									log.debug("params (2):: "+params);

								} else {
									qryACuentaIF =
											" UPDATE COMREL_PYME_IF " +
											" SET " +
											" 	cs_vobo_if = ?, " +
											" 	cs_borrado = ?, " +
											" 	cs_opera_descuento = ?, " +
											" 	df_reasigno_cta = " + ("S".equals(estaAutorizada)?"SYSDATE":"NULL") +
											" WHERE ic_cuenta_bancaria = ? " +
											" 	AND ic_epo = ? " +
											" 	AND ic_if = ? ";
									params = new ArrayList();
									params.add("N");
									params.add("N");
									params.add(operaDescuento);
									params.add(new Integer(rdCta));
									params.add(new Integer(Epo));
									params.add(new Integer(txtaNoIF));

									log.debug("qryACuentaIF (2) :: "+qryACuentaIF);
									log.debug("params (2):: "+params);
								}

							} //if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S") ) || banderaPEF  )


						System.out.println("\ninserta ::: "+qryACuentaIF + "   binds: " + params);
						con.ejecutaUpdateDB(qryACuentaIF, params);
						bOk = true;
						} //if (!CtaClabe)

						//Fodea 019-2014(S)
						if (!CtaClabe) {
							//if("S".equals(estaAutorizada) || banderaPEF){
							if ((estaAutorizada.equals("S") && tieneConvenioUnico.equals("S")) || banderaPEF) { //FODEA 032 - 2010 ACF
									qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H' WHERE ic_epo = " + Epo + " AND ic_pyme = " + Pyme;
									log.debug("\nqryReq8::: "+qryReq8);
									con.ejecutaSQL(qryReq8);
									bOk = true;
							}else {

								bOk = true;
							}
						}else {
							bOk = false;
						}

						/////////

					}// fin if

					log.debug("bOk  :: "+bOk);
					log.debug("banderaPEF :: "+banderaPEF);

					if(bOk && banderaPEF){ // fodea 033
						//Codigo que busca, si la pyme ya existe, se le cambia el perfil, de lo contrario, se desbloquea la solicitud
						//para enviarle el correo
						try{
							log.debug("\n::::Validando para correo::::::");
							UtilUsr utils = new UtilUsr();
							List lstLogin = utils.getUsuariosxAfiliado(Pyme, "P");
							if(!lstLogin.isEmpty()) {
								log.debug("\n::::Cambiar perfil::::::");
								String loginUsuario = lstLogin.get(0).toString();
								utils.setPerfilUsuario(loginUsuario, "ADMIN PYME");
							} else {
								//NO existe la cuenta en el OID de N@E.

								if (utils.getExisteSolicitudAltaUsuario(Pyme, "P")) {
									//Si no existe la cuenta en el OID pero ya hay una solicitud de alta de usuario en el ARGUS, ajusta el perfil
									utils.setPerfilSolicitudAltaUsuario(Pyme, "P", "ADMIN PYME");
								} else {
									//No existe la cuenta de N@E en OID y no hay solicitud de alta de usuario

									SolicitudArgus solicitud = new SolicitudArgus();
									Usuario usr = new Usuario();
									usr.setClaveAfiliado(Pyme);
									usr.setTipoAfiliado("P");
									solicitud.setUsuario(usr);

									String loginAsociado = utils.getCuentaUsuarioAsociada(Pyme);
									if (loginAsociado != null && !loginAsociado.equals("") ) {
										//Existe una cuenta (del Sistema de compras) en el OID que
										//est� preasociada al proveedor, solo se le agregan el perfil, tipo de afiliado
										//y clave de afiliado a la cuenta de manera que pueda ingresar a N@E.
										usr.setLogin(loginAsociado);
										utils.actualizarUsuario(solicitud);
										//La actualizacion del usuario no contempla la actualizacion del perfil.
										//por lo cual se hace en una instrucci�n aparte.
										utils.setPerfilUsuario(loginAsociado, "ADMIN PYME");
									} else {
										//Si no hay cuenta ni de N@E ni compras que pueda usarse, se genera la solicitud

										usr.setPerfil("ADMIN PYME");
										utils.complementarSolicitudArgus(solicitud, "P", Pyme);

										Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

										Registros reg = beanAfiliacion.getDatosContacto(solicitud.getRFC(), "");
										if (reg.next()) {
											usr.setNombre(reg.getString("contacto_nombre"));
											usr.setApellidoPaterno(reg.getString("contacto_appat"));
											usr.setApellidoMaterno(reg.getString("contacto_apmat"));
											usr.setEmail(reg.getString("contacto_email"));
										}

										utils.registrarSolicitudAltaUsuario(solicitud);
									}
								}
							}
							//------------------------------------------ Este bloque
						}catch(Exception ex){
							bOk = false;
							mensajepef = "Ocurrio un error al intentar guardar los datos";
							ex.printStackTrace();
							}
					} // fin if(bOk && banderaPEF){




				}// fin if(chkIF.equals("1"))
			if(bOk){
				// Fodea 010-2013 - Reafiliacion Automatica
				this.paramCUDescAuto(txtaNoIF,  "", Pyme, "", Epo, "",  cboMoneda,  iNoUsuario ,  strNombreUsuario, "Selecci�n IF Individual", con);
				con.terminaTransaccion(true);
			}else {
				con.terminaTransaccion(false);
			}


			//Obtener el ic_nafin_electronico para guarda en la bitacora
			qry = "select ic_nafin_electronico from comrel_nafin where ic_epo_pyme_if = ? and cg_tipo = 'P'";
			ps = con.queryPrecompilado(qry);
			ps.setInt(1, Integer.parseInt(Pyme));
			rs = ps.executeQuery();
			if(rs.next()){
				nafinelec = rs.getString("ic_nafin_electronico");
				rs.close();
			}
			log.debug("nafinelec::: "+nafinelec);
			if(ps != null) ps.close();

			log.debug("cambioCtaCliente>>>>>>>>>"+cambioCtaCliente);


			if(bOk == true && cambioCtaCliente == false ){

				if(txtCtaAnterior != null){
					guardaBit(nafinelec, iNoUsuario, Epo, txtaNoIF, txtCtaAnterior, rdCta);
				} else {
					guardaBit(nafinelec, iNoUsuario, Epo, txtaNoIF, "", rdCta);
				}


				//alert("La cuenta ha sido seleccionada");
				cadenaRetorna="La cuenta ha sido seleccionada";
			} else if(bOk == false && cambioCtaCliente == false  && CtaClabe == false ){

				//alert("La cuenta ya se encuentra seleccionada");
				cadenaRetorna="La cuenta ya se encuentra seleccionada";
			} else if(bOk == true && cambioCtaCliente == true   ){
					System.out.println("ENTRA EN CONDICION DE CAMBIO DE CUENTA" + cambiosCuenta );

					/*Obtener cgNumeroCuenta anterior*/
					cgNumCuentAntQuery =
							"SELECT CG_NUMERO_CUENTA CUENTA_ANTERIOR " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(txtCtaAnterior);
					Registros regCtaAnterior = con.consultarDB(cgNumCuentAntQuery, params);

					if(regCtaAnterior.next()){
						cgNumCuentaAntDato = regCtaAnterior.getString("CUENTA_ANTERIOR");
					}


					/*Obtener cgNumeroCuenta nueva*/
					cgNumCuentNuevoQuery =
							"SELECT CG_NUMERO_CUENTA CUENTA_NUEVA " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";

					params = new ArrayList();
					params.add(new Integer(rdCta));
					log.debug("cgNumCuentAntQuery::  "+cgNumCuentAntQuery);
					log.debug("params::  "+params);

					Registros regCtaNueva = con.consultarDB(cgNumCuentNuevoQuery, params);

					if(regCtaNueva.next()){
						cgNumCuentaNuevoDato = regCtaNueva.getString("CUENTA_NUEVA");
					}

					/*Obtener cg_bancos anterior*/
					cgBancoAntQuery =
							" SELECT CG_BANCO BANCO_ANTERIOR " +
							" FROM COMREL_CUENTA_BANCARIA " +
							" WHERE IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(new Integer(txtCtaAnterior));
					log.debug("cgNumCuentNuevoQuery::  "+cgNumCuentNuevoQuery);
					log.debug("params::  "+params);
					Registros regBancoAnt = con.consultarDB(cgBancoAntQuery, params);

					//cgBancoAnt = con.queryDB(cgBancoAntQuery);

					if(regBancoAnt.next()){
						cgBancoAntDato = regBancoAnt.getString("BANCO_ANTERIOR");
					}

					/*Obtener cg_bancos nuevo*/
					cgBancoNuevoQuery = "SELECT CG_BANCO BANCO_NUEVO " +
							"FROM COMREL_CUENTA_BANCARIA " +
							"WHERE IC_CUENTA_BANCARIA = ? ";

					params = new ArrayList();
					params.add(new Integer(rdCta));
					Registros regBancoNuevo = con.consultarDB(cgBancoNuevoQuery, params);

					//cgBancoAnt = con.queryDB(cgBancoAntQuery);

					if(regBancoNuevo.next()){
						cgBancoNuevoDato = regBancoNuevo.getString("BANCO_NUEVO");
					}

					/*Se registra el cambio de cuenta en COMHIS_CAMBIOS_CTA*/
					String ic_usuario = iNoUsuario; //Variable de session que guarda el ic_usuario
					String cg_nombre_usuario=strNombreUsuario; //Variable de session que guarda el nombre completo del usuario

					int count=0;
					existeHisCuenta=
							" SELECT COUNT(1) FROM COMHIS_CAMBIOS_CTA "+
							" WHERE IC_EPO = ?  AND IC_IF = ? AND IC_CUENTA_BANCARIA = ? ";
					params = new ArrayList();
					params.add(new Integer(Epo));
					params.add(new Integer(txtaNoIF));
					params.add(new Integer(rdCta));
					log.debug("cgBancoAntQuery::  "+cgBancoAntQuery);
					log.debug("params::  "+params);
					Registros regCtaHis = con.consultarDB(existeHisCuenta, params);
					if(regCtaHis.next()) {
						count = Integer.parseInt(regCtaHis.getString(1));
					}

					if(count == 0){
						cambiosCuenta  =
								"INSERT INTO COMHIS_CAMBIOS_CTA(ic_if, ic_epo, df_cambio_cta, cg_cuenta_ant, cg_bancos_ant, cg_cuenta, cg_bancos, ic_pyme, ic_cuenta_bancaria, ic_usuario, cg_nombre_usuario, cs_autoriza_if) " +
								"VALUES(" + txtaNoIF + ", " + Epo + ", SYSDATE, '" + cgNumCuentaAntDato + "','" + cgBancoAntDato + "', '" + cgNumCuentaNuevoDato + "', '" + cgBancoNuevoDato + "', '" + Pyme +"', " + rdCta + ", '" + ic_usuario + "', '" + cg_nombre_usuario + "','"+autorizaIfcambioCta+"')";
					}else{
						cambiosCuenta  =
								"UPDATE COMHIS_CAMBIOS_CTA SET CS_AUTORIZA_IF='"+autorizaIfcambioCta+"', DF_CAMBIO_CTA = SYSDATE "+
								" WHERE IC_EPO = "+ Epo +" AND IC_IF = "+txtaNoIF+" AND IC_CUENTA_BANCARIA = "+rdCta;
					}

					log.debug("cgBancoNuevoQuery::  "+cgBancoNuevoQuery);
					log.debug("params::  "+params);


					con.ejecutaSQL(cambiosCuenta);

					if(txtCtaAnterior != null) {
						guardaBit(nafinelec, iNoUsuario, Epo, txtaNoIF, txtCtaAnterior, rdCta);
					} else {
						guardaBit(nafinelec, iNoUsuario, Epo, txtaNoIF, "", rdCta);
					}

					//alert("La cuenta ha sido cambiada");
					cadenaRetorna="La cuenta ha sido cambiada";
				} else if(bOk == false && cambioCtaCliente == true  && CtaClabe == false ){

					//alert("La cuenta ya se encuentra seleccionada");
					cadenaRetorna="La cuenta ya se encuentra seleccionada";
			} else if(bOk == false && CtaClabe == true ){
				cadenaRetorna = mensajeCtaClabe;
			}

		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al almacenar la parametrizacion de cuentas bancarias", t);
		}finally{
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
		log.debug("cambioCuentaIndividual (S)::");
		return cadenaRetorna;
	}


	public List getCuentasMasivasGrupo(String Pyme,String icCuenta,String icGrupo){
		AccesoDB con = new AccesoDB();
		List regReturn=new ArrayList();
		HashMap datos;
		boolean bOk=true;
		try{
		con.conexionDB();
		PreparedStatement psge        = null;
		ResultSet rsge       = null;
		String                  qryEposIF      = "";
		String                  qryRelacion    = "";
		int contador = 0;
		String   ic_epo = "", Tic_epo ="",  cg_razon_social_epo  = "", Tic_if ="",
		cg_razon_social_if = "",   ic_if = "", ic_cuenta_bancaria = "", cs_fideicomisoIF ="N", cs_fideicomisoEPO ="";
		StringBuffer         valuechecks = new StringBuffer();
		String operaFide_Pyme =   this.getOperaFideicomisoPYME( Pyme ); // Fodea 19-2014
		String operaFide_GrupoEPO =   this.getValidaFisoXGrupoEpos( icGrupo ); // Fodea 19-2014
		PreparedStatement       psIFs = null;

		String queryIFs =  " SELECT   distinct  ci.ic_if as claveIF,  ci.cg_razon_social  as nombreIF " +
										" FROM   comcat_epo e, " +
										"        comrel_pyme_epo pe, " +
										"        comrel_grupo_x_epo gpo, " +
										"        comrel_if_epo cie, " +
										"        comcat_if ci, " +
										"       COMREL_IF_EPO_MONEDA m , "+
										" (      select pi.ic_epo,pi.ic_if, cb.ic_cuenta_bancaria  " +
										" from comrel_pyme_if pi, comrel_cuenta_bancaria cb  " +
										" where pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
										" and    cb.ic_pyme =  ? " +
										" and    pi.cs_borrado = 'N' " +
										" and    cb.cs_borrado = 'N'  " +
										" and    cb.ic_moneda = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? )     ) cta  " +
										" WHERE pe.ic_epo = e.ic_epo " +
										" AND    cta.ic_epo(+) = cie.ic_epo " +
										" AND    cta.ic_if(+) = cie.ic_if " +
										" AND    e.ic_epo = cie.ic_epo " +
										" AND    cie.ic_if = ci.ic_if " +
										" AND    cie.cs_vobo_nafin = 'S' " +
										" AND    cie.cs_aceptacion = 'S' " +
	//============================================================================>> FODEA 002 - 2009 (I)
										" AND    cie.cs_bloqueo = 'N' " +
	//============================================================================>> FODEA 002 - 2009 (F)
										" AND    e.cs_habilitado = 'S' " +
										" AND    pe.ic_pyme = ? " +
										" AND    pe.cs_aceptacion IN ('R', 'H') " +
										" AND    gpo.ic_epo = e.ic_epo " +
										" AND    gpo.ic_grupo_epo = ? " +
										" AND cie.ic_epo = m.ic_epo "+
										" AND cie.ic_if = m.ic_if "+
										" AND m.ic_moneda =  (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) ";
										queryIFs+= " ORDER BY  ci.ic_if  desc  ";
				psIFs= con.queryPrecompilado(queryIFs);
				psIFs.setInt(1,Integer.parseInt(Pyme));
				psIFs.setInt(2,Integer.parseInt(icCuenta));
				psIFs.setInt(3,Integer.parseInt(Pyme));
				psIFs.setInt(4,Integer.parseInt(icGrupo));
				psIFs.setInt(5,Integer.parseInt(icCuenta));

				ResultSet         rsIFs = psIFs.executeQuery();


				String queryEpos =  " SELECT   e.ic_epo AS clave, e.cg_razon_social AS descripcion, " +
										"        ci.cg_razon_social, ci.ic_if, cta.ic_cuenta_bancaria " +
										" ,  ci.cs_opera_fideicomiso AS cs_opera_fideicomiso "+
										" FROM   comcat_epo e, " +
										"        comrel_pyme_epo pe, " +
										"        comrel_grupo_x_epo gpo, " +
										"        comrel_if_epo cie, " +
										"        comcat_if ci, " +
										"       COMREL_IF_EPO_MONEDA m , "+
										" (      select pi.ic_epo,pi.ic_if, cb.ic_cuenta_bancaria  " +
										" from comrel_pyme_if pi, comrel_cuenta_bancaria cb  " +
										" where pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
										" and    cb.ic_pyme =  ? " +
										" and    pi.cs_borrado = 'N' " +
										" and    cb.cs_borrado = 'N'  " +
										" and    cb.ic_moneda = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? )     ) cta  " +
										" WHERE pe.ic_epo = e.ic_epo " +
										" AND    cta.ic_epo(+) = cie.ic_epo " +
										" AND    cta.ic_if(+) = cie.ic_if " +
										" AND    e.ic_epo = cie.ic_epo " +
										" AND    cie.ic_if = ci.ic_if " +
										" AND    cie.cs_vobo_nafin = 'S' " +
										" AND    cie.cs_aceptacion = 'S' " +
	//============================================================================>> FODEA 002 - 2009 (I)
										" AND    cie.cs_bloqueo = 'N' " +
	//============================================================================>> FODEA 002 - 2009 (F)
										" AND    e.cs_habilitado = 'S' " +
										" AND    pe.ic_pyme = ? " +
										" AND    pe.cs_aceptacion IN ('R', 'H') " +
										" AND    gpo.ic_epo = e.ic_epo " +
										" AND    gpo.ic_grupo_epo = ? " +
										" AND cie.ic_epo = m.ic_epo "+
										" AND cie.ic_if = m.ic_if "+
										" AND m.ic_moneda =  (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) "+
										" and  ci.ic_if = ? "+
										" ORDER BY  ci.ic_if, e.ic_epo, ci.cs_opera_fideicomiso desc  ";

						valuechecks = new StringBuffer();


						while(rsIFs.next()){

							datos=new HashMap();
							valuechecks = new StringBuffer();

							ic_if =  rsIFs.getString("claveIF");
							cg_razon_social_if   = rsIFs.getString("nombreIF");

							//-----------------------------------------------------------------

							psge = con.queryPrecompilado(queryEpos);
							psge.setInt(1,Integer.parseInt(Pyme));
							psge.setInt(2,Integer.parseInt(icCuenta));
							psge.setInt(3,Integer.parseInt(Pyme));
							psge.setInt(4,Integer.parseInt(icGrupo));
							psge.setInt(5,Integer.parseInt(icCuenta));
							psge.setInt(6,Integer.parseInt(ic_if));
							rsge = psge.executeQuery();
							psge.clearParameters();

							contador=0;
                                                        boolean param=true;
							while(rsge.next()){
								ic_epo                     = rsge.getString(1);
								cg_razon_social_epo  = rsge.getString(2);
								ic_cuenta_bancaria   = rsge.getString(5)==null?"":rsge.getString(5);
								cs_fideicomisoIF  = rsge.getString(6)==null?"N":rsge.getString(6);

								cs_fideicomisoEPO=  this.getOperaFideicomisoEpo(ic_epo);
								if(ic_cuenta_bancaria.equals("") ){
                                                                        param=false;
                                                                        if(contador==0) {
										valuechecks.append(ic_if+","+ic_epo+","+icCuenta);
									}else {
										valuechecks.append("|"+ic_if+","+ic_epo+","+icCuenta);
									}
								}
								contador++;
								datos.put("VALORES",valuechecks.toString());
							}//fin de while rsge.next()
							rsge.close();
							psge.close();

							datos.put("INTERMEDIARIO",cg_razon_social_if);

							//-------------------------------------------
							/*if(ic_cuenta_bancaria.equals("") ){
								datos.put("PARAMETRIZACION",new Boolean(false));

							}else if(!ic_cuenta_bancaria.equals("") ){
								datos.put("PARAMETRIZACION",new Boolean(true));
							}*/

                                                        datos.put("PARAMETRIZACION",new Boolean(param));

							//1.Si PYME no opera Fiso y alguna de las EPOS del Grupo si opera FISO, al dar clic en "Parametrizar" en el grid se deben desplegar los IFS de las EPOS que NO operan Fideicomiso.
							if(operaFide_Pyme.equals("N")  &&  operaFide_GrupoEPO.equals("OPERA")  &&  cs_fideicomisoEPO.equals("N") )  {
								regReturn.add(datos);
							}
							//2.Si PYME opera Fiso y y alguna de las EPOS del Grupo si opera FISO, al dar clic en "Parametrizar" en el grid: para EPOS que no operan se muestra su listado de IFS y para la EPO que opera Fiso se debe desplegar solo su IF que opera fiso.
							if(operaFide_Pyme.equals("S")  &&  operaFide_GrupoEPO.equals("OPERA") )  {
								if(cs_fideicomisoEPO.equals("N"))  {
									regReturn.add(datos);
								}else if(cs_fideicomisoEPO.equals("S") &&  cs_fideicomisoIF.equals("S") )  {
									regReturn.add(datos);
								}
							}
							//3.Si PYME no opera fiso y todas las EPOS del grupo NO operan fiso , es una seleccionar normal y se debe conservar funcionalidad de desplegar sus IFS
							if(operaFide_Pyme.equals("N")  &&  operaFide_GrupoEPO.equals("NO_OPERA") )  {
								regReturn.add(datos);
							}
								//4.Si PYME opera Fiso y  todas las EPOS del grupo operan fiso, es una seleccionar normal y se debe conservar funcionalidad de desplegar sus IFS.4  (NO ES CORRECTA , solo debe de mostrar el if que opera fiso )
							if(operaFide_Pyme.equals("S")  &&  operaFide_GrupoEPO.equals("NORMAL") )  {
								if(cs_fideicomisoEPO.equals("S") &&  cs_fideicomisoIF.equals("S") )  {
									regReturn.add(datos);
								}
							}

						}
						rsIFs.close();
						psIFs.close();

						} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al almacenar la parametrizacion de cuentas bancarias", t);
		}finally{
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
		return regReturn;
	}


	public List getCuentasMasiva(String Epos[],String icPyme,String icCuenta){
	AccesoDB con = new AccesoDB();
	List reg=new ArrayList();
		try{
			con.conexionDB();
			String					 	qryEposIF 		= "";
			String					 	qryRelacion 	= "";
			PreparedStatement psei 					= null;
			PreparedStatement psrel					= null;
			ResultSet 				rsei 					= null;
			ResultSet 				rsrel					= null;
			boolean 					rel 					= false;
			int 							x 						= 0;
			HashMap	datos;

			for(x=0;x<Epos.length;x++){

//								System.out.println("Epos["+x+"]="+Epos[x]);
				Hashtable alParamEPO1 = this.getParametrosEPO(Epos[x].toString(),1);
				String 	bOperaFideicomisoEPO = alParamEPO1.get("CS_OPERA_FIDEICOMISO").toString();

				//-- seleccion de IF s asociadas a una EPO

				if(!"".equals(Epos[x].toString())){
					qryEposIF = "select i.cg_razon_social, e.cg_razon_social, i.ic_if, e.ic_epo " +
								"from comrel_if_epo ie, comcat_if i, comcat_epo e , COMREL_IF_EPO_MONEDA  m " +
								"where ie.ic_epo = ? " +
								"and ie.ic_epo = e.ic_epo " +
								"and ie.ic_if = i.ic_if " +
								"and ie.cs_vobo_nafin = 'S' " +
								"and ie.cs_aceptacion = 'S' "+
//============================================================================>> FODEA 002 - 2009 (I)
								"and ie.cs_bloqueo = 'N'"+
//============================================================================>> FODEA 002 - 2009 (F)
				"and ie.ic_epo  = m.ic_epo "+
								"and ie.ic_if  = m.ic_if "+
								"     AND m.IC_MONEDA = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) ";

						if(bOperaFideicomisoEPO.equals("S"))  {
							qryEposIF+=" AND i.CS_OPERA_FIDEICOMISO = 'S' ";
						}

							log.debug("queryEpos "+qryEposIF);
							log.debug("epo: "+Epos[x].toString());
							log.debug("epo: "+icCuenta);


					psei = con.queryPrecompilado(qryEposIF);
					psei.setInt(1, Integer.parseInt(Epos[x].toString()));
					psei.setInt(2, Integer.parseInt(icCuenta));
					rsei = psei.executeQuery();

					while(rsei.next()){
						datos=new HashMap();
						//-- seleccion de relacion de una IF con una CUENTA BANCARIA  con una EPO
						//-- si existe resultado quiere decir que hay una cuenta bancaria asociada
						datos.put("INTERMEDIARIO",rsei.getString(1));
						datos.put("EPO",rsei.getString(2));
						datos.put("IC_IF",rsei.getString(3));
						datos.put("IC_EPO",rsei.getString(4));
						qryRelacion = "select cb.ic_cuenta_bancaria " +
										"from comrel_pyme_if pi, comrel_cuenta_bancaria cb " +
										"where pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria " +
										"and pi.ic_if = ? " +
										"and pi.ic_epo = ? " +
										"and cb.ic_pyme = ? " +
										"and pi.cs_borrado = ? " +
										"and cb.cs_borrado = ? " +
										"  AND cb.IC_MONEDA = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) " ;
						psrel = con.queryPrecompilado(qryRelacion);
						psrel.setInt(1, Integer.parseInt(rsei.getString(3)));
						psrel.setInt(2, Integer.parseInt(Epos[x].toString()));
						psrel.setInt(3, Integer.parseInt(icPyme));
						psrel.setString(4,"N");
						psrel.setString(5,"N");
						psrel.setInt(6, Integer.parseInt(icCuenta));
						rsrel = psrel.executeQuery();
						if(rsrel.next()){
							rel = true;
							datos.put("PARAMETRIZACION",new Boolean(true));
						}
						else{
							datos.put("PARAMETRIZACION",new Boolean(false));
						}
						rsrel.close();
						psrel.close();
						reg.add(datos);
					}
				}



				rsei.close();
				psei.close();
			}

		}catch(Exception e){

		throw new AppException("Error al guardar la parametrizacion de cuenta bancaria", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return reg;

	}

	public List confirmarCuentasMasiva(String valores,String Epo,String Pyme, String strLogin , String strNombreUsuario, String ic_nafin_electronico  ){

		String tieneConvenioUnico="N";
		PreparedStatement ps 			= null;
		PreparedStatement psReq8 	= null;
		PreparedStatement psAut 	= null;
		boolean bUltimaAct = false;
		List listaRegistros = new ArrayList();
		HashMap datos = new HashMap();

		int iIF 							= 0;
		int iEPO 							= 0;
		int iCuentaBancaria		= 0;

		String 	ifTemp  	= "";
		List 		lstEpos   = new ArrayList();
		List		cveEpoPymeIfCta = new ArrayList();

		VectorTokenizer vt = null;
		boolean ok = true;
		AccesoDB con = new AccesoDB();


		try{
			con.conexionDB();
			StringTokenizer st = new StringTokenizer(valores,"|");
			String qry = "INSERT INTO COMREL_PYME_IF (ic_cuenta_bancaria, ic_epo, ic_if, cs_borrado, cs_opera_descuento, cs_vobo_if, df_vobo_if, DF_REASIGNO_CTA) " +
						 "VALUES(?,?,?,?,?,?, sysdate, ?)";
			ps = con.queryPrecompilado(qry);

			//FODEA 032 - 2010 ACF (I)
			//Si la epo NO opera con Convenio �nico, las cuentas deben ser autorizadas por el IF.
			//En caso de que la epo SI opere con Convenio �nico, la reafiliaci�n con el IF es autom�tica.
			if (!Epo.equals("")) {
				StringBuffer strSQL = new StringBuffer();
				strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
				System.out.println("..:: strSQL: "+strSQL.toString());
				System.out.println("..:: varBind: ["+Epo+"]");
				PreparedStatement pst = con.queryPrecompilado(strSQL.toString());
				pst.setInt(1, Integer.parseInt(Epo));
				ResultSet rst = pst.executeQuery();
				while (rst.next()) {
					tieneConvenioUnico = rst.getString("cs_convenio_unico") == null?"N":rst.getString("cs_convenio_unico");
				}
				rst.close();
				pst.close();
			}

			String qryReq8 = "UPDATE comrel_pyme_epo SET cs_aceptacion = ? WHERE ic_epo = ? AND ic_pyme = ? ";
			psReq8 = con.queryPrecompilado(qryReq8);
		//SE ELIMINA LA AUTORIZACION POR PRIMERA VEZ Y SE ALMACENA SIEMPRE UN SYSDATE
		/*	String qryUpdateFechas = " 	update comrel_pyme_if    " +
			" 	    set df_vobo_if =  " +
			" 	    ( " +
			" 	       SELECT min(if.DF_VOBO_IF) df_vobo_if " +
			" 	     FROM comrel_pyme_if IF, comrel_cuenta_bancaria cuen " +
			" 	    WHERE IF.ic_cuenta_bancaria = cuen.ic_cuenta_bancaria " +
			" 	      AND IF.ic_if = ? " +
			" 	      AND if.cs_vobo_if = ? " +
			" 	      AND cuen.ic_pyme = ? " +
			" 	    ) " +
			" 	 WHERE ic_cuenta_bancaria = ? AND ic_if = ? and df_vobo_if!= " +
			" 	      ( " +
			" 	       SELECT min(if.DF_VOBO_IF) df_vobo_if  " +
			" 	     FROM comrel_pyme_if IF, comrel_cuenta_bancaria cuen " +
			" 	    WHERE IF.ic_cuenta_bancaria = cuen.ic_cuenta_bancaria " +
			" 	      AND IF.ic_if = ? " +
			" 	      AND if.cs_vobo_if = ?" +
			" 	      AND cuen.ic_pyme = ? " +
			" 	      ) ";
		psFecha = con.queryPrecompilado(qryUpdateFediagonal*/

			try{
				while(st.hasMoreTokens()){
					String registros = st.nextToken();
					vt = new VectorTokenizer(registros,",");
					Vector vecCampos = vt.getValuesVector();

					iIF 						= Integer.parseInt(vecCampos.get(0).toString());
					iEPO 						= Integer.parseInt(vecCampos.get(1).toString());
					iCuentaBancaria = Integer.parseInt(vecCampos.get(2).toString());

					if (Epo.equals("")) {
						StringBuffer strSQL = new StringBuffer();
						strSQL.append(" SELECT cs_convenio_unico FROM comcat_epo WHERE ic_epo = ?");
						//System.out.println("..:: strSQL: "+strSQL.toString());
						//System.out.println("..:: varBind: ["+iEPO+"]");
						PreparedStatement pst = con.queryPrecompilado(strSQL.toString());
						pst.setInt(1, iEPO);
						ResultSet rst = pst.executeQuery();
						while (rst.next()) {
							tieneConvenioUnico = rst.getString("cs_convenio_unico") == null?"N":rst.getString("cs_convenio_unico");
						}
						rst.close();
						pst.close();
					}

					//FODEA 032 - 2010 ACF (I)
		/*			psAut.clearParameters();
					psAut.setInt(1, iEPO);
					System.out.println("..:: strSQL: "+strSQL.toString());
					System.out.println("..:: varBind: ["+iEPO+"]");
					ResultSet rsAut = psAut.executeQuery();
					String autorizacion = "N";
					while (rsAut.next()) {
						autorizacion = rsAut.getString("cs_convenio_unico") == null?"N":rsAut.getString("cs_convenio_unico");
					}
					rsAut.close();*/
					//FODEA 032 - 2010 ACF (F)
					//Determina si el vobo IF va ser "S" con base en que ya exista una cuenta autorizada en la moneda en cuestion para el if especificado

							//Fodea 019-2014	(E)
					AutorizacionDescuentoBean BeanAutDescuento  = new AutorizacionDescuentoBean();
					String operaFide_Pyme =  this.getOperaFideicomisoPYME( Pyme ); //Fodea 019-2014
					boolean OperaIFEPOFISO  =  BeanAutDescuento.getOperaIFEPOFISO(String.valueOf(iIF),String.valueOf(iEPO));
					String  entidad_Gobierno =  getOpEntidadGobierno(Pyme );			//F034-2014

					//FODEA 032 - 2010 ACF (F)
				String qryAutorizada =
							" SELECT COUNT (1) as numRegistros, '15admnafinparam02masa.jsp' " +
							"  FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb, comrel_if_epo ie " ;
							if(OperaIFEPOFISO ){
								qryAutorizada += " , comcat_if i ";
							}

					qryAutorizada +=" WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria " +
							"   AND pi.ic_if = ie.ic_if " +
							"   AND pi.ic_epo = ie.ic_epo " +
							"   AND cb.ic_pyme = ? " +
							"   AND cb.ic_moneda = (SELECT ic_moneda " +
							"                         FROM comrel_cuenta_bancaria " +
							"                        WHERE ic_cuenta_bancaria = ?) " +
							"   AND pi.cs_vobo_if = ? " +
							"   AND pi.cs_borrado = ? " +
							"   AND cb.cs_borrado = ? " +
							"   AND ie.cs_vobo_nafin = ? " +
							"   AND ie.cs_aceptacion = ? ";
					if(OperaIFEPOFISO  ){
						qryAutorizada +=" and i.CS_OPERA_FIDEICOMISO = 'S' "+
											" and pi.ic_if = i.ic_if ";
					}else {
						qryAutorizada +=" AND   PI.IC_IF   = ? " ; //txtaNoIF 		+
					}


					psAut = con.queryPrecompilado(qryAutorizada);

					psAut.clearParameters();

					psAut.setInt(1,Integer.parseInt(Pyme)); //Pyme
					psAut.setInt(2,iCuentaBancaria); // Cuenta Bancaria
					psAut.setString(3,"S");
					psAut.setString(4,"N");
					psAut.setString(5,"N");
					psAut.setString(6,"S");
					psAut.setString(7,"S");
					if(!OperaIFEPOFISO  ){
						psAut.setInt(8,iIF); //IF
					}
					ResultSet rsAut = psAut.executeQuery();
					rsAut.next();

					String autorizacion = "N";

					if (rsAut.getInt("numRegistros") == 0 ) {
						autorizacion = "N";
					} else {
						autorizacion = "S";
						if (entidad_Gobierno.equals("S"))
							autorizacion = "N";

						List regCveEpoPymeIfCta = new ArrayList();
						regCveEpoPymeIfCta.add(Pyme);
						regCveEpoPymeIfCta.add(String.valueOf(iEPO));
						regCveEpoPymeIfCta.add(String.valueOf(iIF));
						regCveEpoPymeIfCta.add(String.valueOf(iCuentaBancaria));
						cveEpoPymeIfCta.add(regCveEpoPymeIfCta);

					}
					rsAut.close();

					String vClabePYME =  this.getValidaCcuentaClablePyme(ic_nafin_electronico ) ; //Fodea 019-2014
					log.debug("autorizacion :::::  "+autorizacion);
					log.debug("tieneConvenioUnico :::::  "+tieneConvenioUnico);
					log.debug("operaFide_Pyme :::::  "+operaFide_Pyme);
					log.debug("OperaIFEPOFISO :::::  "+OperaIFEPOFISO);
					log.debug("vClabePYME :::::  "+vClabePYME);
					String mensajeCtaClabe ="";
					boolean 	CtaClabe = false;
					String  validaEpoRea=  	getValidaPymeEpoRea(Pyme, Epo ); // esta validacion es apra una reafiliaci�n de una pyme que opera fideicomiso en otra epo  que opera fideicomiso

					if( "S".equals(validaEpoRea) ) {

						if("S".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
							mensajeCtaClabe =  getcuentaClablePyme(ic_nafin_electronico, strLogin, String.valueOf(iEPO), con );
							if(!mensajeCtaClabe.equals("")) {
								CtaClabe = true;
							}
						}else  if("N".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
							mensajeCtaClabe="La PYME no tiene parametrizada una Cuenta CLABE, favor de verificar";
							CtaClabe = true;
						}else  if("E".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {
							mensajeCtaClabe="La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla";
							CtaClabe = true;
						}

						log.debug("mensajeCtaClabe :::::  "+mensajeCtaClabe);
					}

					//Fodea 019-2014	(S)

					if (!CtaClabe){ // SOLO SI NO HAY ERROR EN LA VALIDACION DE LA CUENTA CLABE REALIZA LA REAFILIACION

							//insertar la relacion pyme - if
							//INSERT INTO COMREL_PYME_IF
							ps.clearParameters();
							ps.setInt(1,iCuentaBancaria); //Cuenta Bancaria
							ps.setInt(2,iEPO); //Epo
							ps.setInt(3,iIF); //IF
							ps.setString(4,"N");
							ps.setString(5,"S");
							//ps.setString(6,autorizacion);
							if ("S".equals(autorizacion) && tieneConvenioUnico.equals("S")) {//FODEA 032 - 2010 ACF
								ps.setString(6,"S");
							} else {
								ps.setString(6,"N");
							}
						//if ("S".equals(autorizacion)){
						if ("S".equals(autorizacion) && tieneConvenioUnico.equals("S")) {//FODEA 032 - 2010 ACF
						  ps.setDate(7,new java.sql.Date(new java.util.Date().getTime()));
						} else {
						  ps.setString(7,null);
						}
							ps.executeUpdate();

						//UPDATE comrel_pyme_epo
						//if (autorizacion.equals("S")) { //si la cuenta ya va autorizada de manera predeterminada, tambi�n se activa el requisito 8 autom�ticamente
						if (autorizacion.equals("S") && tieneConvenioUnico.equals("S") ) {//FODEA 032 - 2010 ACF

							psReq8.clearParameters();
							psReq8.setString(1, "H");
							psReq8.setInt(2, iEPO); // Epo
							psReq8.setInt(3, Integer.parseInt(Pyme));	//Pyme
							psReq8.executeUpdate();

							if( ifTemp.equals("") || Integer.parseInt(ifTemp)==iIF ){
									lstEpos.add(new Integer(iEPO));
									ifTemp = ""+iIF;
							}
							else{

								/*System.out.println("----------qryUpdateFechas----------"+qryUpdateFechas);

								psFecha.clearParameters();
								psFecha.setInt(1,iIF); //IF
								psFecha.setString(2, "S");
								psFecha.setInt(3,Integer.parseInt(Pyme));
								psFecha.setInt(4,iCuentaBancaria);
								psFecha.setInt(5,iIF); //IF
								psFecha.setInt(6,iIF); //IF
								psFecha.setString(7, "S");
								psFecha.setInt(8,Integer.parseInt(Pyme));
								psFecha.executeUpdate();*/
								ifTemp = ""+iIF;
								lstEpos.clear();
							}
							bUltimaAct = true;

						}
					}

					String qryEposIF = "select i.cg_razon_social, e.cg_razon_social, i.ic_if, e.ic_epo " +
										"from comrel_if_epo ie, comcat_if i, comcat_epo e " +
										"where ie.ic_epo = ? " +
										"and ie.ic_if = ? " +
										"and ie.ic_epo = e.ic_epo " +
										"and ie.ic_if = i.ic_if " +
										"and ie.cs_vobo_nafin = 'S' " +
										"and ie.cs_aceptacion = 'S'";
					PreparedStatement psei = con.queryPrecompilado(qryEposIF);
					psei.setInt(1, iEPO );
					psei.setInt(2, iIF);
					ResultSet rsei = psei.executeQuery();
					while(rsei.next()){
							datos= new HashMap();
							datos.put("EPO",rsei.getString(2));
							datos.put("INTERMEDIARIO",rsei.getString(1));
							datos.put("MENSAJE",mensajeCtaClabe);
							listaRegistros.add(datos);

						} // fin while(rsei.next())
						rsei.close();
						psei.close();

				} //fin de while


				/*if(bUltimaAct){

					System.out.println("----------qryUpdateFechas----------"+qryUpdateFechas);

					psFecha.clearParameters();
					psFecha.setInt(1,iIF); //IF
					psFecha.setString(2, "S");
					psFecha.setInt(3,Integer.parseInt(Pyme));
					psFecha.setInt(4,iCuentaBancaria);
					psFecha.setInt(5,iIF); //IF
					psFecha.setInt(6,iIF); //IF
					psFecha.setString(7, "S");
					psFecha.setInt(8,Integer.parseInt(Pyme));
					psFecha.executeUpdate();

				}

				psFecha.close();*/

				psAut.close();
				ps.close();
				psReq8.close();

				//INI - MODIFICACION FODEA 036-2009 FVR DESC. AUTOMATICO.
				/*
				con.terminaTransaccion(ok);
				System.out.println("nuevo metodo para desc aut Entra XXXXXXXXXXXXXXXXX");
				Context context_a = ContextoJNDI.getInitialContext();
				ParametrosDescuentoHome parametrosDescuentoHome = (ParametrosDescuentoHome) context_a.lookup("ParametrosDescuentoEJB");
				ParametrosDescuento beanParametrosDescuento = parametrosDescuentoHome.create();
				beanParametrosDescuento.setParamEpoPymeIf(cveEpoPymeIfCta,"");
				System.out.println("nuevo metodo para desc aut Salio XXXXXXXXXXXXXXXXX");
				*/
				//FIN - MODIFICACION FODEA 036-2009 FVR DESC. AUTOMATICO.


					}catch(Exception e){
						ok = false;
						e.printStackTrace();
						throw new AppException("Error al guardar la parametrizacion de cuenta bancaria", e);
					}finally{
						con.terminaTransaccion(ok);
					}
			}catch(Exception e){
						ok = false;
						e.printStackTrace();
						throw new AppException("Error al guardar la parametrizacion de cuenta bancaria", e);
					}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return listaRegistros;
	}

	private void guardaBit(String nafinel, String usuario, String epo, String _if, String cuentAnt, String cuentNue){
	System.out.println("Entra Metodo Guarda Bitacora");
	AccesoDB con = new AccesoDB();
	boolean bOk = true;
	try{
		con.conexionDB();
		System.out.println("Entra Metodo Guarda Bitacora Primer Try");
		//Obtener datos de la cuenta anterior y de la nueva cuenta (NOMBRE PYME, NOMBRE EPO, TIPO MONENDA)
		String obtieneInfoCA = "";
		String obtieneInfoCN = "";
		List ListaObtieneInfo = new ArrayList();
		Registros registros = new Registros();
		Registros registros2 = new Registros();

		if(!cuentAnt.equals("")){
			System.out.println("Entra Metodo Guarda Bitacora Cuenta Anterior dif de vacio");
			obtieneInfoCA = 	"SELECT C_IF.CG_RAZON_SOCIAL || ' - ' || EPO.CG_RAZON_SOCIAL || ' - ' || MONEDA.CD_NOMBRE || ' - CUENTA : '|| REL_CUEN_BANC.CG_BANCO || ' (' || REL_CUEN_BANC.CG_NUMERO_CUENTA || ')' DESC_CUENTA " +
			"FROM COMREL_PYME_IF PYME_IF, COMCAT_EPO EPO, COMCAT_PYME PYME, COMREL_CUENTA_BANCARIA REL_CUEN_BANC, COMCAT_MONEDA MONEDA, COMCAT_IF C_IF " +
			"WHERE PYME_IF.IC_EPO = EPO.IC_EPO " +
			"AND PYME_IF.IC_CUENTA_BANCARIA = REL_CUEN_BANC.IC_CUENTA_BANCARIA " +
			"AND REL_CUEN_BANC.IC_PYME = PYME.IC_PYME " +
			"AND REL_CUEN_BANC.IC_MONEDA = MONEDA.IC_MONEDA " +
			"AND PYME_IF.IC_IF = C_IF.IC_IF " +
			"AND PYME_IF.IC_IF = ? " +
			"AND PYME_IF.IC_EPO = ? " +
			"AND PYME_IF.IC_CUENTA_BANCARIA = ? ";

			ListaObtieneInfo.add(_if);
			ListaObtieneInfo.add(epo);
			ListaObtieneInfo.add(cuentAnt);
			System.out.println(ListaObtieneInfo.toString());
			registros = con.consultarDB(obtieneInfoCA, ListaObtieneInfo, false); //Obtener los datos de la cuenta anterior
			ListaObtieneInfo.clear(); //Borrar la lista para usarla con otros datos
		}

		if(!cuentNue.equals("")){
			System.out.println("Entra Metodo Guarda Bitacora Cuenta Nueva dif de vacio");
			obtieneInfoCN = 	"SELECT C_IF.CG_RAZON_SOCIAL || ' - ' || EPO.CG_RAZON_SOCIAL || ' - ' || MONEDA.CD_NOMBRE || ' - CUENTA : '|| REL_CUEN_BANC.CG_BANCO || ' (' || REL_CUEN_BANC.CG_NUMERO_CUENTA || ')' DESC_CUENTA " +
			"FROM COMREL_PYME_IF PYME_IF, COMCAT_EPO EPO, COMCAT_PYME PYME, COMREL_CUENTA_BANCARIA REL_CUEN_BANC, COMCAT_MONEDA MONEDA, COMCAT_IF C_IF " +
			"WHERE PYME_IF.IC_EPO = EPO.IC_EPO " +
			"AND PYME_IF.IC_CUENTA_BANCARIA = REL_CUEN_BANC.IC_CUENTA_BANCARIA " +
			"AND REL_CUEN_BANC.IC_PYME = PYME.IC_PYME " +
			"AND REL_CUEN_BANC.IC_MONEDA = MONEDA.IC_MONEDA " +
			"AND PYME_IF.IC_IF = C_IF.IC_IF " +
			"AND PYME_IF.IC_IF = ? " +
			"AND PYME_IF.IC_EPO = ? " +
			"AND PYME_IF.IC_CUENTA_BANCARIA = ? ";

			ListaObtieneInfo.add(_if);
			ListaObtieneInfo.add(epo);
			ListaObtieneInfo.add(cuentNue);
			System.out.println(ListaObtieneInfo.toString());
			registros2 = con.consultarDB(obtieneInfoCN, ListaObtieneInfo, false); //Obtener los datos de la cuenta nueva
		}

		//Grabar los cambios en la bitacora siempre y cuando la cuenta bancaria haya sido cambiada
		//Bitacora.grabarEnBitacora(con, "SELECIF", "P",nafinel, usuario,txtCtaAnterior, rdCta);
		if(registros.next() && registros2.next()){
			System.out.println("Guardando en Bitacora");
			Bitacora.grabarEnBitacora(con, "SELECIF", "P",nafinel, usuario, registros.getString("DESC_CUENTA"), registros2.getString("DESC_CUENTA"));
		} else if(registros.next() == false && registros2.next() == true){
			System.out.println("Guardando en Bitacora");
			Bitacora.grabarEnBitacora(con, "SELECIF", "P",nafinel, usuario, "", registros2.getString("DESC_CUENTA"));
		}
		bOk = true;
		System.out.println("GrabarEnBitacora::: OK");
		//fin Bit�cora
	}catch(Exception e){
		bOk = false;
		System.out.println("Ocurrio un error al grabar en bitacora " + e.getMessage());
	}finally{
		if (con.hayConexionAbierta()) {
			con.terminaTransaccion(bOk);
			con.cierraConexionDB();
		}
	}
}
	private boolean existeRegistroRelacionEpoPymeIf(
		String claveEpo, String claveCuentaBancaria, String claveIf) {
	AccesoDB con = new AccesoDB();

	try{
		con.conexionDB();
		String strSQL =
				" SELECT COUNT(*) " +
				" FROM comrel_pyme_if " +
				" WHERE ic_cuenta_bancaria = ? " +
				" AND ic_epo = ? " +
				" AND ic_if = ? ";
		PreparedStatement ps = con.queryPrecompilado(strSQL);
		ps.setInt(1, Integer.parseInt(claveCuentaBancaria));
		ps.setInt(2, Integer.parseInt(claveEpo));
		ps.setInt(3, Integer.parseInt(claveIf));
		ResultSet rs = ps.executeQuery();
		rs.next();
		boolean existe = false;
		if (rs.getInt(1) == 0) {
			existe = false;
		} else {
			existe = true;
		}
		rs.close();
		ps.close();
		return existe;
	}catch(Exception e){
		throw new AppException("Error al guardar la parametrizacion de cuenta bancaria", e);
	}finally{
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
			}
		}
	}


	public boolean reasignarCuentasBancarias(String ic_if,String ic_pyme,String ic_epo_base,String ic_cuenta_bancaria,String [] clavesEpo ,String strLogin , String strNombreUsuario ){

	AccesoDB con = new AccesoDB();
	String strSQL = "";
	String strSQL1 = "";
	PreparedStatement ps = null;
	PreparedStatement ps1 = null;
	boolean exito = true;
	log.info("reasignarCuentasBancarias (E)");

	try {
		con.conexionDB();

		strSQL =
				" INSERT INTO comrel_pyme_if ( " +
				" 	ic_cuenta_bancaria,  " +
				" 	ic_if, " +
				" 	ic_epo,  " +
				" 	cs_vobo_if,  " +
				" 	cs_borrado, " +
				" 	df_vobo_if,  " +
				" 	cs_dscto_automatico,  " +
				" 	cs_dscto_automatico_dia,  " +
				" 	cs_opera_descuento) " +
				" SELECT ic_cuenta_bancaria, " +
				" 	ic_if, " +
				" 	? ," +
				" 	cs_vobo_if, " +
				" 	cs_borrado, " +
				" 	sysdate, " +
				" 	cs_dscto_automatico, " +
				" 	cs_dscto_automatico_dia, " +
				" 	cs_opera_descuento  " +
				" FROM comrel_pyme_if  " +
				" WHERE ic_cuenta_bancaria = ? " +
				" 	AND ic_if = ? " +
				" 	AND ic_epo = ? ";

			ps = con.queryPrecompilado(strSQL);

			strSQL1 =
					" UPDATE comrel_pyme_epo " +
					" SET cs_aceptacion = 'H' " +
					" WHERE ic_epo = ? " +
					" AND ic_pyme = ? ";

			ps1 = con.queryPrecompilado(strSQL1);

		for (int i = 0; i < clavesEpo.length; i++) {
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(clavesEpo[i]));
			ps.setInt(2, Integer.parseInt(ic_cuenta_bancaria));
			ps.setInt(3, Integer.parseInt(ic_if));
			ps.setInt(4, Integer.parseInt(ic_epo_base));
			ps.executeUpdate();

			//Marcado automatico del Requisito 8.
			ps1.clearParameters();
			ps1.setInt(1, Integer.parseInt(clavesEpo[i]));
			ps1.setInt(2, Integer.parseInt(ic_pyme));

			ps1.executeUpdate();
		}
		ps.close();
		ps1.close();

		// Fodea 010-2013 - Reafiliacion Automatica
			String ic_moneda  = "";
			String  query = "SELECT ic_moneda  FROM comrel_cuenta_bancaria   WHERE ic_cuenta_bancaria  = ? ";
			PreparedStatement ps2 = con.queryPrecompilado(query);
			ps2.clearParameters();
			ps2.setInt(1, Integer.parseInt(ic_cuenta_bancaria));
			ResultSet  rs2 = ps2.executeQuery();
			if(rs2!=null && rs2.next()){
				ic_moneda =  rs2.getString("ic_moneda").trim();
			}
			rs2.close();
			ps2.close();

			this.paramCUDescAuto(ic_if,  "", ic_pyme, "", ic_epo_base, "",  ic_moneda, strLogin,  strNombreUsuario, "Selecci�n IF Individual", con);




		exito=true;
			} catch (Exception e) {
			exito = false;
			throw new AppException("Error al reafilar la cuenta bancaria", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("reasignarCuentasBancarias (S)");
		}
		return exito;
	}


	/**
	 * Este metodo devuelve los datos de acuerdo a su numero electr�nico
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	Registros con el query resultante
	 */
	public Hashtable getDatosPymeNafinElec(String noNafinElec, boolean consulta){
		log.info("ParametrosDescuentoBean::getUsuarioCesion(E)");
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Hashtable		mapa				= new Hashtable();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			sTraeLineas		= "S";

		if(noNafinElec == null || noNafinElec.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			String ic_epo_pyme_if	= "", dato= "0", srfc ="", srazon_social ="";

			// -------------------------se Obtiene  el ic_pyme del nafin electronico  ----------------------

			qrySentencia.append(	"SELECT count(*), ic_epo_pyme_if FROM comrel_nafin where IC_NAFIN_ELECTRONICO = ? " +
				" AND CG_TIPO = ? GROUP BY ic_epo_pyme_if ");

			lVarBind.add(noNafinElec);
			lVarBind.add("P");
			log.debug("qrySentencia.toString() = " + qrySentencia.toString() );
			log.debug("lVarBind = " + lVarBind );

			registros		= null;
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			if (registros.next()){
				dato				= (registros.getString(1)==null?"0":registros.getString(1));
				ic_epo_pyme_if	= (registros.getString("ic_epo_pyme_if")==null?"":registros.getString("ic_epo_pyme_if"));
			}
			if(dato.equals("0")){
				sTraeLineas ="N";
			}

			mapa.put("sTraeLineas",		sTraeLineas);
			mapa.put("ic_epo_pyme_if",	ic_epo_pyme_if);

			// -------------------------Obtengo RFc y razon social de la pyme  ----------------------
			qrySentencia = new StringBuffer();
			qrySentencia.append("SELECT cg_razon_social,  cg_rfc from comcat_pyme where ic_pyme  =  ?  ");
			lVarBind = new ArrayList();
			lVarBind.add(ic_epo_pyme_if);
			log.debug("qrySentencia.toString() = " + qrySentencia.toString() );
			log.debug("lVarBind = " + lVarBind );

			registros		= null;
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			if (registros.next()){
				srfc	= (registros.getString("cg_rfc")==null?"":registros.getString("cg_rfc"));
				srazon_social	= (registros.getString("cg_razon_social")==null?"":registros.getString("cg_razon_social"));
			}

			mapa.put("srfc",	srfc);
			mapa.put("srazon_social",	srazon_social);
			// -------------------------Se Obtiene consulta del Gris--------------------------- ---

			if(sTraeLineas.equals("S") && consulta){

				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"SELECT cb.cg_banco, " +
					" cb.cg_sucursal, " +
					" m.cd_nombre, " +
					" cb.cg_numero_cuenta, " +
					" cb.ic_cuenta_bancaria, " +
					" cb.CG_PLAZA plaza,  " +
					" CB.cg_cuenta_clabe, " +
					" CB.cs_convenio_unico, " +
					" cb.ic_pyme " +
					" FROM comcat_moneda m, comrel_cuenta_bancaria cb" +
					" WHERE cb.ic_moneda = m.ic_moneda " +
					" AND cb.ic_pyme = ? "  +
					" AND cb.cs_borrado = ? ORDER BY 1"
				);

				lVarBind = new ArrayList();
				lVarBind.add(ic_epo_pyme_if);
				lVarBind.add("N");
				registros = new Registros();

				log.debug("qrySentencia.toString() = " + qrySentencia.toString() );
				log.debug("lVarBind = " + lVarBind );

				registros		= null;
				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				mapa.put("registros",	registros);

			}

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getUsuarioCesion(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos del usuario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getUsuarioCesion(S)");
		}
		return mapa;
	}

	/**
	 * Este metodo devuelve los datos de acuerdo a su numero electr�nico y cta_bancaria
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	Hashtable con datos a obtener
	 */
	public Hashtable getDatosPymeAmodificar(String sPyme, String txtIcCtaBanco ){
		log.info("ParametrosDescuentoBean::getDatosPymeAmodificar(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Hashtable		mapa				= new Hashtable();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			sTraeLineas		= "S";
		boolean			plaza				= false;
		PreparedStatement ps				= null;
		PreparedStatement ps1			= null;

		if(	sPyme == null || sPyme.equals("") || txtIcCtaBanco == null || txtIcCtaBanco.equals("")	){
			return null;
		}

		try
		{
			con.conexionDB();

			// LOOP PARA OBTENER LAS EPOS DE LA PYME
			qrySentencia.append("SELECT ic_epo FROM comrel_pyme_epo WHERE ic_pyme = ? ");

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.clearParameters();
			ps.setString(1, sPyme);
			ResultSet rsRelPyemEpo = ps.executeQuery();

			while (rsRelPyemEpo.next()){
				String sEpoAsociada = rsRelPyemEpo.getString(1);
				qrySentencia	= new StringBuffer();
				qrySentencia.append(" SELECT count(*) from com_linea_credito "   +
						" WHERE ic_if IN("   +
						" 	  SELECT E.ic_if"   +
						" 	  FROM comrel_cuenta_bancaria C, comrel_pyme_if E"   +
						" 	  WHERE C.ic_cuenta_bancaria = E.ic_cuenta_bancaria "   +
						" 	  AND E.cs_borrado = ? "   +
						" 	  AND E.ic_epo = ? " +
						" 	  AND C.ic_cuenta_bancaria = ? "+
						"	  AND C.ic_pyme = ? "+
						" 	  )"   +
						" AND ic_pyme = ? "+
						" AND ic_estatus_linea NOT IN(?,?)");

				ps1 = con.queryPrecompilado(qrySentencia.toString());
				ps1.clearParameters();
				ps1.setString(1, "N");
				ps1.setString(2, sEpoAsociada);
				ps1.setString(3, txtIcCtaBanco);
				ps1.setString(4, sPyme);
				ps1.setString(5, sPyme);
				ps1.setInt(6, 4);
				ps1.setInt(7, 6);
				ResultSet rs = ps1.executeQuery();

				if(rs.next()){
					if(rs.getInt(1)==0){
						sTraeLineas ="N";
						plaza = false;
						ps1.close();
						rs.close();

					}else{
						qrySentencia = new StringBuffer();
						qrySentencia.append(" SELECT COUNT(*) FROM com_linea_credito "   +
							" WHERE ic_if in("   +
							" 	  SELECT E.ic_if"   +
							" 	  FROM comrel_cuenta_bancaria C, comrel_pyme_if E"   +
							" 	  WHERE C.ic_cuenta_bancaria = E.ic_cuenta_bancaria "   +
							" 	  AND E.cs_borrado = ? "   +
							" 	  AND E.ic_epo = ? "+
							" 	  AND C.ic_cuenta_bancaria = ? " +
							"	  AND C.ic_pyme = ? "+
							"     AND C.ic_plaza IS NOT NULL " +
							" 	  )"   +
							" and ic_pyme = ? "+
							" and ic_estatus_linea not in(?,?)");

						ps1 = con.queryPrecompilado(qrySentencia.toString());
						ps1.clearParameters();
						ps1.setString(1, "N");
						ps1.setString(2, sEpoAsociada);
						ps1.setString(3, txtIcCtaBanco);
						ps1.setString(4, sPyme);
						ps1.setString(5, sPyme);
						ps1.setInt(6, 4);
						ps1.setInt(7, 6);
						ResultSet rs1 = ps1.executeQuery();

						if(rs1.next())
							if(rs1.getInt(1)==0)
						  {
							plaza = true;
							sTraeLineas ="N";
						  }
						ps1.close();
						rs1.close();
						}//else
					}
			}
			rsRelPyemEpo.close();
			mapa.put("sTraeLineas",		sTraeLineas						);
			mapa.put("plaza",				plaza?"true":"false"			);

			if(sTraeLineas.equals("N")){
				qrySentencia = new StringBuffer();
				qrySentencia.append("SELECT trim(cb.CG_BANCO) as CG_BANCO," +
					" cb.IC_MONEDA, " +
					" cb.CG_SUCURSAL, " +
					" cb.CG_NUMERO_CUENTA, " +
					" NA.IC_NAFIN_ELECTRONICO, " +
					" CB.CG_PLAZA, " +
					" CB.cg_cuenta_clabe, " +
					" CB.CG_CUENTA_CLABE_C , " +
					" NVL(CB.cs_convenio_unico, ? ) as cs_convenio_unico " +
					" FROM COMREL_CUENTA_BANCARIA  CB, comrel_nafin NA " +
					" WHERE cb.IC_CUENTA_BANCARIA = ? AND cb.IC_PYME = ? "+
					" AND CB.IC_PYME = NA.IC_EPO_PYME_IF AND NA.CG_TIPO = ? ");

				lVarBind.add("N");
				lVarBind.add(txtIcCtaBanco);
				lVarBind.add(sPyme);
				lVarBind.add("P");

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

				mapa.put("registros",	registros);

			}

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getDatosPymeAmodificar(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener datos para modificar");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getDatosPymeAmodificar(S)");
		}
		return mapa;
	}

	/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	String con resultado de actualizacion
	 */
	public String setDatosPymeAmodificar(String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,	String sPyme,
								String txtNE, String cboPlaza,	String txtCtaClabe,	String chkCU,	String txtIcCtaBanco, String iNoUsuario, String txtCtaClabeC ){
		log.info("ParametrosDescuentoBean::setDatosPymeAmodificar(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		String			strMensaje		= "";

		try
		{
			con.conexionDB();
			if ( "S".equals(chkCU)) {
				int CveCtaB = 0;
				qrySentencia.append("SELECT COUNT(IC_CUENTA_BANCARIA) AS regs FROM COMREL_CUENTA_BANCARIA "+
					" WHERE ic_pyme = ? "+
					" AND ic_moneda = ? "+
					" AND cs_borrado = ? "+
					" AND IC_CUENTA_BANCARIA != ? "+
					" AND cs_convenio_unico = ? "
				);

				PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
				ps1.clearParameters();
				ps1.setString(1,	sPyme);
				ps1.setString(2,	cboMoneda);
				ps1.setString(3,	"N");
				ps1.setString(4,	txtIcCtaBanco);
				ps1.setString(5, "S");
				ResultSet rsCveCtaB = ps1.executeQuery();

				if(rsCveCtaB.next())
					CveCtaB = rsCveCtaB.getInt("regs");
				rsCveCtaB.close();
				if (CveCtaB>0) {
					strMensaje = "La Pyme ya tiene parametrizada una cuenta con Convenio �nico en la moneda especificada";
				}
			}
			if ("".equals(strMensaje)){
				String[] nombres = {"Banco de Servicio","Sucursal","Moneda","No. de Cuenta","Pyme","Plaza","Cuenta Clabe", "Convenio Unico"};
				List datosAnteriores = new ArrayList();
				List datosActuales = new ArrayList();
				StringBuffer valoresAnteriores = new StringBuffer();
				StringBuffer valoresActuales = new StringBuffer();

			 try {
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					" SELECT CG_BANCO, CG_SUCURSAL, IC_MONEDA, " +
					" CG_NUMERO_CUENTA, IC_PYME, CG_PLAZA, NVL(CG_CUENTA_CLABE, '') as CG_CUENTA_CLABE, NVL(CS_CONVENIO_UNICO,'N') as CS_CONVENIO_UNICO "+
					" FROM COMREL_CUENTA_BANCARIA " +
					" WHERE IC_CUENTA_BANCARIA = ? "
				);
				PreparedStatement pstmt = con.queryPrecompilado(qrySentencia.toString());
				pstmt.setInt(1,Integer.parseInt(txtIcCtaBanco));
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()){
					datosAnteriores.add(0,rs.getString("CG_BANCO"));
					datosAnteriores.add(1,rs.getString("CG_SUCURSAL"));
					datosAnteriores.add(2,rs.getString("IC_MONEDA"));
					datosAnteriores.add(3,rs.getString("CG_NUMERO_CUENTA"));
					datosAnteriores.add(4,rs.getString("IC_PYME"));
					datosAnteriores.add(5,rs.getString("CG_PLAZA"));
					datosAnteriores.add(6,rs.getString("CG_CUENTA_CLABE"));
					datosAnteriores.add(7,rs.getString("CS_CONVENIO_UNICO"));
				}
			 }catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al obtener los datos anteriores";
			 }
			 try{

			 if(cboPlaza.equals("null") || cboPlaza ==null )  { cboPlaza = ""; }

				qrySentencia = new StringBuffer();
				qrySentencia.append(
					" UPDATE comrel_cuenta_bancaria  "+
					" SET cg_banco = '"+txtBanco+"' , " +
					" cg_sucursal = '"+txtSucursal+"' , " +
					" ic_moneda = "+cboMoneda+" , " +
					" cg_numero_cuenta = '"+txtCuenta+"' , " +
					" ic_pyme = "+sPyme+" , " +
					" cg_plaza =  '"+cboPlaza+"' , " +
					" cg_cuenta_clabe = '"+txtCtaClabe+"' , " +
					" cs_convenio_unico ='"+chkCU+"' , " +
					" CG_CUENTA_CLABE_C  ='"+txtCtaClabeC+"'  " +
					" WHERE ic_cuenta_bancaria = "+txtIcCtaBanco);

			log.info("qrySentencia.toString()  "+qrySentencia.toString());

				PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
				ps.executeUpdate();
				ps.close();

				con.terminaTransaccion(true);
				strMensaje = "La cuenta ha sido actualizada";
			 }catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al actualizar la cuenta";
			 }
			 try {
				datosActuales.add(0,txtBanco);
				datosActuales.add(1,txtSucursal);
				datosActuales.add(2,cboMoneda);
				datosActuales.add(3,txtCuenta);
				datosActuales.add(4,sPyme);
				datosActuales.add(5,cboPlaza);
				datosActuales.add(6,cboPlaza);
				datosActuales.add(7,cboPlaza);

			 }catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al obtener los datos actualizados";
			}
			try {
				List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
				valoresAnteriores.append(cambios.get(0));
				valoresActuales.append(cambios.get(1));

				Bitacora.grabarEnBitacora(con,"CUENTABANC","P",txtNE,iNoUsuario,
								valoresAnteriores.toString(),valoresActuales.toString());
				con.terminaTransaccion(true);
			 }catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al actualizar la bit�cora";
			 }
			}

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::setDatosPymeAmodificar(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::setDatosPymeAmodificar(S)");
		}
		return strMensaje;
	}

	/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	String con resultado de actualizacion
	 */
	public String eliminaDatosPymeAmodificar(String sPyme,	String txtNE, String txtIcCtaBanco, String iNoUsuario ){
		log.info("ParametrosDescuentoBean::eliminaDatosPymeAmodificar(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		String 			sTraeLineas		= "S";
		String			strMensaje		= "";

		try
		{
			con.conexionDB();
			// LOOP PARA OBTENER LAS EPOS DE LA PYME

			qrySentencia.append("SELECT ic_epo FROM comrel_pyme_epo WHERE ic_pyme = ? ");

			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
			ps.clearParameters();
			ps.setString(1, sPyme);
			ResultSet rsRelPyemEpo = ps.executeQuery();

			while (rsRelPyemEpo.next()){
				String sEpoAsociada = rsRelPyemEpo.getString(1);
				qrySentencia	= new StringBuffer();
				qrySentencia.append(
					" SELECT count(*) FROM com_linea_credito "   +
					" WHERE ic_if IN("   +
					" 	  SELECT E.ic_if"   +
					" 	  FROM comrel_cuenta_bancaria C, comrel_pyme_if E"   +
					" 	  WHERE C.ic_cuenta_bancaria = E.ic_cuenta_bancaria "   +
					" 	  AND E.cs_borrado = ? "   +
					" 	  AND E.ic_epo = ? " +
					" 	  AND C.ic_cuenta_bancaria = ? "+
					"	  AND C.ic_pyme = ? "+
					" 	  )"   +
					" AND ic_pyme = ? "+
					" AND ic_producto_nafin in (?,?) "+
					" AND ic_estatus_linea NOT IN(?,?)"+
					" AND ic_moneda = (SELECT ic_moneda FROM comrel_cuenta_bancaria WHERE ic_cuenta_bancaria = ? ) "
				);

				PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
				ps1.clearParameters();
				ps1.setString(1, "N");
				ps1.setString(2, sEpoAsociada);
				ps1.setString(3, txtIcCtaBanco);
				ps1.setString(4, sPyme);
				ps1.setString(5, sPyme);
				ps1.setInt(6, 2);
				ps1.setInt(7, 5);
				ps1.setInt(8, 4);
				ps1.setInt(9, 6);
				ps1.setString(10, txtIcCtaBanco);
				ResultSet rs = null;
				rs = ps1.executeQuery();
				if(rs.next()){
					if(rs.getInt(1)==0) {
						sTraeLineas ="N";
					}else {
						sTraeLineas ="S";
					}
					rs.close();
					ps1.close();
				}
				if (sTraeLineas.equals("S"))
					break;
			}
			rsRelPyemEpo.close();
			ps.close();

			if(sTraeLineas.equals("N")){
				try{
					qrySentencia = new StringBuffer();
					qrySentencia.append(
						" UPDATE COMREL_CUENTA_BANCARIA SET CS_BORRADO = ? " +
						" WHERE IC_CUENTA_BANCARIA = ? "
					);
					PreparedStatement ps3 = con.queryPrecompilado(qrySentencia.toString());
					ps3.setString(1,	"S");
					ps3.setString(2,	txtIcCtaBanco);
					ps3.executeUpdate();
					ps3.close();

					qrySentencia = new StringBuffer();
					qrySentencia.append(
						"UPDATE COMREL_PYME_IF SET CS_BORRADO = ? " +
						"WHERE IC_CUENTA_BANCARIA = ? "
					);
					ps3 = con.queryPrecompilado(qrySentencia.toString());
					ps3.setString(1,	"S");
					ps3.setString(2,	txtIcCtaBanco);
					ps3.executeUpdate();
					ps3.close();

					con.terminaTransaccion(true);
					strMensaje = "La cuenta ha sido borrada";
				}catch (Exception e){
					con.terminaTransaccion(false);
					strMensaje = "Hubo un error al borrar la cuenta";
				}
				try {
					String no_cuenta = "";
					qrySentencia = new StringBuffer();
					qrySentencia.append(
						"SELECT CG_NUMERO_CUENTA FROM COMREL_CUENTA_BANCARIA " +
						" WHERE IC_CUENTA_BANCARIA = ? "
					);
					PreparedStatement ps4 = con.queryPrecompilado(qrySentencia.toString());
					ps4.clearParameters();
					ps4.setString(1,	txtIcCtaBanco);
					ResultSet rs = ps4.executeQuery();

					if(rs.next()){	no_cuenta = rs.getString(1);	}
					Bitacora.grabarEnBitacora(con,"CUENTABANC","P",txtNE,iNoUsuario,"","BAJA DE CUENTA NO. "+no_cuenta);
					con.terminaTransaccion(true);
				}catch(Exception e){
					con.terminaTransaccion(false);
					strMensaje = "Hubo un error al actualizar la bit�cora";
				}
			}else{
				strMensaje = "No se puede borrar la cuenta porque tiene Lineas de Credito asociadas";
			}

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::eliminaDatosPymeAmodificar(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::eliminaDatosPymeAmodificar(S)");
		}
		return strMensaje;
	}

	/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	numeroNafinElectronicoDeLaPYME Numero Nafin Electronico de la PYME
	 * @return 	String con resultado de actualizacion
	 */
	public String addDatosPymeAmodificar(String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,	String sPyme,
												String txtNE, String cboPlaza,	String txtCtaClabe,	String chkCU,	String iNoUsuario,
												String confirtxtCuentaClabe ){
		log.info("ParametrosDescuentoBean::addDatosPymeAmodificar(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		String			strMensaje		= "";

		try
		{
			con.conexionDB();
			try{
				if ( "S".equals(chkCU)) {
					int CveCtaB = 0;
					qrySentencia.append("SELECT COUNT(ic_cuenta_bancaria) AS regs FROM comrel_cuenta_bancaria "+
						" WHERE ic_pyme = ? "+
						" AND ic_moneda = ? "+
						" AND cs_borrado = ? "+
						" AND cs_convenio_unico = ? "
					);

					PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
					ps1.clearParameters();
					ps1.setString(1,	sPyme);
					ps1.setString(2,	cboMoneda);
					ps1.setString(3,	"N");
					ps1.setString(4,	"S");
					ResultSet rsCveCtaB = ps1.executeQuery();

					if(rsCveCtaB.next())
						CveCtaB = rsCveCtaB.getInt("regs");
					rsCveCtaB.close();
					if (CveCtaB>0) {
						strMensaje = "La Pyme ya tiene parametrizada una cuenta con Convenio �nico en la moneda especificada";
					}
				}
				if ("".equals(strMensaje)){
					qrySentencia = new StringBuffer();
					qrySentencia.append(
						" INSERT INTO comrel_cuenta_bancaria " +
						" (ic_cuenta_bancaria, cg_banco, cg_sucursal, ic_moneda, cg_numero_cuenta, ic_pyme, cs_borrado, CG_PLAZA, cg_cuenta_clabe, cs_convenio_unico, CG_CUENTA_CLABE_C ) " +
						" VALUES(seq_comrel_cta_banc_ic_cb.NEXTVAL,'" + txtBanco + "','" + txtSucursal + "'," + cboMoneda + ",'" + txtCuenta + "'," + sPyme + ", 'N','"+cboPlaza+"','"+txtCtaClabe+ "','" +chkCU+"','" +confirtxtCuentaClabe+"')"	);

					log.info("qrySentencia  "+qrySentencia);
					PreparedStatement ps2 = con.queryPrecompilado(qrySentencia.toString());
					ps2.executeUpdate();
					ps2.close();

					con.terminaTransaccion(true);
					strMensaje = "La cuenta ha sido dada de alta";
				}
			}catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al dar de alta la cuenta";
				log.error(" addDatosPymeAmodificar(Exception)  " +e);
			}
			try {
				if ("".equals(strMensaje)){
					Bitacora.grabarEnBitacora(con,"CUENTABANC","P",txtNE,iNoUsuario,"","ALTA DE CUENTA NO. "+txtCuenta);
					con.terminaTransaccion(true);
				}
			}catch(Exception e){
				con.terminaTransaccion(false);
				strMensaje = "Hubo un error al dar de alta la cuenta en bit�cora";
				log.error(" addDatosPymeAmodificar(Exception)  " +e);
			}
		} catch(Exception e) {
			log.error("ParametrosDescuentoBean::addDatosPymeAmodificar(Exception) "+e);
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::addDatosPymeAmodificar(S)");
		}
		return strMensaje;
	}

	/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	Clave epo
	 * @return 	Registros
	 */
	public Registros getDatosPymeCatalogoIf(String ic_epo){
		log.info("ParametrosDescuentoBean::getDatosPymeCatalogoIf(E)");

		if(	ic_epo == null || ic_epo.equals("")	){
			return null;
		}

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros 		registros		= null;

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT cif.ic_if, cif.cg_razon_social, fin.ic_financiera "   +
				" FROM comcat_if cif, comrel_if_epo rie, "   +
				" comrel_if_epo_x_producto iep, comcat_financiera fin "   +
				" WHERE iep.ic_if = cif.ic_if "   +
				" AND iep.ic_if = cif.ic_if "   +
				" AND rie.ic_if = iep.ic_if "   +
				" AND iep.ic_epo = rie.ic_epo "   +
				" AND cif.ic_financiera = fin.ic_financiera "   +
				" AND rie.ic_epo = ? "   +
				" AND iep.ic_producto_nafin = ? "   +
				" AND rie.cs_distribuidores = ? "   +
				" ORDER BY 2"
			);

			lVarBind.add(ic_epo);
			lVarBind.add(new Integer(4));
			lVarBind.add("S");

			registros = con.consultarDB(qrySentencia.toString(), lVarBind);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getDatosPymeCatalogoIf(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getDatosPymeCatalogoIf(S)");
		}
		return registros;
	}

	/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	datos de formulario..
	 * @return 	String con resultado de actualizacion
	 */
	public Hashtable addDatosCtasDist(String ic_epo,	String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,
								String ic_if,	String txtNE, String cboPlaza,	String CtasCapturadas ){
		log.info("ParametrosDescuentoBean::addDatosCtasDist(E)");

		AccesoDB 		con 				= new AccesoDB();
		Hashtable		mapa				= new Hashtable();
		String			strMensaje		= "";

		try
		{
			con.conexionDB();
			String sPyme			= "";
			boolean existeCuenta	= false;
			boolean existeError	= true;
			PreparedStatement ps1= null;
			String qryNEValido2 =
				" SELECT crn.ic_epo_pyme_if"   +
				" FROM comrel_nafin crn, comrel_pyme_epo_x_producto pep"   +
				" WHERE crn.cg_tipo = 'P'"   +
				" AND crn.ic_epo_pyme_if = pep.ic_pyme"   +
				" AND pep.ic_epo = ? "   +
				" AND crn.ic_nafin_electronico = ? "   +
				" AND pep.ic_producto_nafin = ? "  ;
			ps1 = con.queryPrecompilado(qryNEValido2);
			ps1.setInt(1,Integer.parseInt(ic_epo));
			ps1.setInt(2,Integer.parseInt(txtNE));
			ps1.setInt(3,4);
			ResultSet rsNEValido = ps1.executeQuery();
			if(rsNEValido.next())
				sPyme = rsNEValido.getString(1);
			else
				strMensaje = "N�mero PYME inexistente o no esta afiliado a Financiamiento a Distribuidores";
			rsNEValido.close();
			if(ps1 != null) ps1.close();
			if (!"".equals(sPyme)) {
				ps1 = null;
				String qryTraeCuentas2 =
					" SELECT COUNT( * ),'15admnafinctabancpyme' "   +
					" FROM comrel_cuenta_bancaria_x_prod"   +
					" WHERE ic_moneda = ? "   +
					" AND ic_pyme = ? "   +
					" AND ic_epo = ? "   ;
				qryTraeCuentas2 += (!"".equals(ic_if))?" AND ic_if = ? ":" AND ic_financiera = ? ";
				qryTraeCuentas2 +=
					" AND ic_producto_nafin = ? "   +
					" AND cs_borrado = ? "  ;
				ps1 = con.queryPrecompilado(qryTraeCuentas2);
				ps1.setInt(1, Integer.parseInt(cboMoneda));
				ps1.setInt(2, Integer.parseInt(sPyme));
				ps1.setInt(3, Integer.parseInt(ic_epo));
				ps1.setInt(4, Integer.parseInt((!"".equals(ic_if))?ic_if:txtBanco));
				ps1.setInt(5, 4);
				ps1.setString(6,"N");
				ResultSet rsTraeCuentas2 = ps1.executeQuery();
				if(rsTraeCuentas2.next())
					if(rsTraeCuentas2.getInt(1)>0)
							existeCuenta = true;
				rsTraeCuentas2.close();
				if(ps1 != null) ps1.close();
				if (existeCuenta)
					strMensaje = "Ya existe una cuenta para ese Distribuidor";
				else {
					String CveCtaB = "0";
					String qryCveCtaB =
						" SELECT MAX(ic_cuenta_bancaria) AS cve "   +
						" FROM comrel_cuenta_bancaria_x_prod"  ;
					ResultSet rsCveCtaB = con.queryDB(qryCveCtaB);
					if(rsCveCtaB.next())
						CveCtaB = Integer.toString(rsCveCtaB.getInt(1) + 1);
					rsCveCtaB.close();
					if("".equals(CtasCapturadas))
						CtasCapturadas = CveCtaB;
					else
						CtasCapturadas += ","+CveCtaB;
					try {
						String qryAltaCuenta =
							" INSERT INTO comrel_cuenta_bancaria_x_prod ("   +
							"    ic_cuenta_bancaria,"   +
							"    ic_producto_nafin,"   +
							"    ic_epo,"   +
							"    ic_pyme,"   ;
						qryAltaCuenta += (!"".equals(ic_if))?"    ic_if,":"";
						qryAltaCuenta +=
							"    ic_financiera,"+
							"    cg_sucursal,"   +
							"    ic_moneda,"   +
							"    cg_numero_cuenta,"   +
							"    ic_plaza,"   +
							"    cs_borrado"   +
							" ) "   +
							" VALUES("   +
							"    "+CveCtaB+","   +
							"    4,"   +
							"    "+ic_epo+","   +
							"    "+sPyme+","   ;
						qryAltaCuenta += (!"".equals(ic_if))?"    "+ic_if+",":"";
						qryAltaCuenta +=
							"    "+txtBanco+","+
							"    '"+txtSucursal+"',"   +
							"    "+cboMoneda+","   +
							"    '"+txtCuenta+"',"   +
							"    "+cboPlaza+","   +
							"    'N'"   +
							" )"  ;
	//System.out.println("\n"+qryAltaCuenta);
						con.ejecutaSQL(qryAltaCuenta);
						con.terminaTransaccion(true);
						existeError = false;
						strMensaje = "La cuenta ha sido dada de alta";

					}catch(Exception e){
						con.terminaTransaccion(false);
						strMensaje = "Hubo un error al dar de alta la cuenta";
					}
				}//	if (existeCuenta)
			}//if (!"".equals(sPyme))

			mapa.put("strMensaje",		strMensaje							);
			mapa.put("existeError",		existeError?"true":"false"		);
			mapa.put("CtasCapturadas",	CtasCapturadas						);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::addDatosCtasDist(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::addDatosCtasDist(S)");
		}
		return mapa;
	}

		/**
	 * Este metodo devuelve el mensaje referente a la actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	datos de formulario..
	 * @return 	String con resultado de actualizacion
	 */
	 public boolean addDatosCtasEpo(String ic_epo,String ic_if,String txtBanco,String txtSucursal,String txtCuenta,String cboPlaza,String txtCuentaClabe){
		log.info("ParametrosDescuentoBean::addDatosCtasEpo(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		boolean			ok					= false;
		boolean			transOk			= false;

		try{
			con.conexionDB();

			log.debug("ic_if  "+ic_if);
			log.debug("ic_epo  "+ic_epo);


			//Verifico si la EPO e IF operan Fideicomiso Fodea 017-2013
			Hashtable alParamEPO1 = new Hashtable();
			alParamEPO1 = this.getParametrosEPO(ic_epo,1);
			//String 	bOperaFideicomisoEPO = alParamEPO1.get("CS_OPERA_FIDEICOMISO").toString();
			String operaFideicomisoIF=  this.getOperaFideicomiIF( ic_if);
			String operanFideicomisoIFEPO = "N";
			if(operaFideicomisoIF.equals("S") && operaFideicomisoIF.equals("S"))  {
				operanFideicomisoIFEPO =  "S";
			}

			ResultSet rs=null;
			qrySentencia.append(
				"SELECT COUNT(*) FROM comrel_if_epo WHERE ic_if = ? AND ic_epo = ? "+
				" AND cg_banco IS NOT NULL AND cg_num_cuenta IS NOT NULL AND ig_sucursal IS NOT NULL"
			);

			PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
			ps1.setInt(1,Integer.parseInt(ic_if));
			ps1.setInt(2,Integer.parseInt(ic_epo));
			rs = ps1.executeQuery();

			int reg=0,reg2=0;
			if(rs.next())
				reg=rs.getInt(1);

			if(reg==0){

				try{
					ResultSet rs1 = null;
					qrySentencia = new StringBuffer();
					qrySentencia.append("SELECT COUNT(*) FROM COMREL_IF_EPO WHERE IC_IF = ? AND IC_EPO = ? ");
					PreparedStatement ps2 = con.queryPrecompilado(qrySentencia.toString());
					ps2.setInt(1,Integer.parseInt(ic_if));
					ps2.setInt(2,Integer.parseInt(ic_epo));
					rs1 = ps2.executeQuery();

					if(rs1.next())
						reg2=rs1.getInt(1);
					con.cierraStatement();
					qrySentencia = new StringBuffer();
					PreparedStatement ps3 = null;

					log.debug("reg2  "+reg2);

					if(reg2==0){
						qrySentencia.append("INSERT INTO COMREL_IF_EPO "+
							"(IC_IF, IC_EPO, CG_BANCO, IG_SUCURSAL, CG_NUM_CUENTA, CS_ACEPTACION, DF_ACEPTACION, CS_VOBO_NAFIN, CG_CUENTA_CLABE,CS_AUTORIZACION_CLABE,IC_PLAZA) "+
							"VALUES(?, ?, ?, ?, ?, ?,NULL, ?,"
						);
						if(	txtCuentaClabe.equals("")	){
							qrySentencia.append("NULL,NULL,");
						}else{
							qrySentencia.append("?,?,");
						}
						if (!"".equals(cboPlaza))
							qrySentencia.append("? )");
						else
							qrySentencia.append("NULL)");

						ps3 = con.queryPrecompilado(qrySentencia.toString());
						ps3.setString(1,	ic_if);
						ps3.setString(2,	ic_epo);
						ps3.setString(3,	txtBanco);
						ps3.setString(4,	txtSucursal);
						ps3.setString(5,	txtCuenta);
						if(operanFideicomisoIFEPO.equals("N"))ps3.setString(6,	"N");
						if(operanFideicomisoIFEPO.equals("S"))ps3.setString(6,	"S"); // Fodea 017-2013

						ps3.setString(7,	"N");
						int next = 8;
						if(!"".equals(txtCuentaClabe)){
							ps3.setString(next++,	txtCuentaClabe);
							ps3.setString(next++,	"N");
						}
						if (!"".equals(cboPlaza))
							ps3.setString(next++,	cboPlaza);
						ps3.executeUpdate();
						ps3.close();

						log.debug("qrySentencia.toString()---->  "+qrySentencia.toString());


					}else{
						qrySentencia.append(
							" UPDATE COMREL_IF_EPO "+
							" SET CG_BANCO = ? "+
							" ,IG_SUCURSAL = ? "+
							" ,CG_NUM_CUENTA = ? "
						);
						if(!txtCuentaClabe.equals(""))
							qrySentencia.append(" ,CG_CUENTA_CLABE = ? ");

						qrySentencia.append(" ,IC_PLAZA = ? "+
										" ,CS_ACEPTACION = ? "+
										" ,CS_VOBO_NAFIN = ? "+
										" WHERE IC_IF = ? AND IC_EPO = ? ");

						log.debug("qrySentencia.toString()---->  "+qrySentencia.toString());

						ps3 = con.queryPrecompilado(qrySentencia.toString());
						ps3.setString(1,	txtBanco);
						ps3.setString(2,	txtSucursal);
						ps3.setString(3,	txtCuenta);
						int sigue = 4;
						if(!txtCuentaClabe.equals(""))
							ps3.setString(sigue++,	txtCuentaClabe);

						ps3.setString(sigue++,	cboPlaza);
						if(operanFideicomisoIFEPO.equals("N"))ps3.setString(sigue++,	"N");
						if(operanFideicomisoIFEPO.equals("S"))ps3.setString(sigue++,	"S");  // Fodea 017-2013
						ps3.setString(sigue++,	"N");
						ps3.setString(sigue++,	ic_if);
						ps3.setString(sigue++,	ic_epo);
						ps3.executeUpdate();
						ps3.close();

					}
					qrySentencia = new StringBuffer();
					qrySentencia.append(
						"INSERT INTO comrel_if_epo_x_producto "+
						" (ic_if,ic_epo,ic_producto_nafin,cs_habilitado,df_habilitado,cs_tipo_fondeo) "+
						" VALUES(?,?,?,?,SYSDATE,?) "
					);

					log.debug("qrySentencia.toString()---->  "+qrySentencia.toString());

					PreparedStatement ps4 = con.queryPrecompilado(qrySentencia.toString());
					ps4.setInt(1,		Integer.parseInt(ic_if));
					ps4.setInt(2,		Integer.parseInt(ic_epo));
					ps4.setInt(3,		1);
					ps4.setString(4,	"S");
					ps4.setString(5,	"N");
					ps4.executeUpdate();
					ps4.close();
					transOk = true;
					ok = true;
				}catch(Exception e){
					transOk = false;
				}
			}
		} catch(Exception e) {
			transOk = false;
			log.info("ParametrosDescuentoBean::addDatosCtasEpo(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			con.terminaTransaccion(transOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::addDatosCtasEpo(S)");
		}
		return ok;
	}

	/**
	 * Este metodo devuelve los registros de las ctas bancarias epo
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	Clave if, clave epo
	 * @return 	Registros
	 */
	public Registros getDatosCtasEpo(String ic_if,	String ic_epo){
		log.info("ParametrosDescuentoBean::getDatosCtasEpo(E)");

		if(	ic_if == null || ic_if.equals("") ||	ic_epo == null || ic_epo.equals("")	){
			return null;
		}

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros 		registros		= null;

		try
		{
			con.conexionDB();

			qrySentencia.append(
				" SELECT   epo.cg_razon_social, relifepo.cg_banco, relifepo.cg_num_cuenta,"   +
				"          relifepo.cs_vobo_nafin, relifepo.ic_epo, IF.ic_if,"   +
				"          DECODE (IF.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || IF.cg_razon_social,"   +
				"          relifepo.ig_sucursal, cpe.fn_aforo, pn.fn_aforo, relifepo.ic_plaza,"   +
				"          relifepo.CG_CUENTA_CLABE "+
				"     FROM comcat_epo epo,"   +
				"          comcat_if IF,"   +
				"          comrel_if_epo relifepo,"   +
				"          comcat_producto_nafin pn,"   +
				"          comrel_producto_epo cpe"   +
				"    WHERE epo.ic_epo = relifepo.ic_epo"   +
				"      AND IF.ic_if = relifepo.ic_if"   +
				"      AND cpe.ic_epo = epo.ic_epo"   +
				"      AND cpe.ic_producto_nafin = pn.ic_producto_nafin"   +
				"      AND pn.ic_producto_nafin = ? "   +
				"      AND epo.ic_epo = ? "+
				"      AND IF.ic_if = ? "+
				" ORDER BY 1, 6"
			);

			lVarBind.add(new Integer(1));
			lVarBind.add(ic_epo);
			lVarBind.add(ic_if);

			registros = con.consultarDB(qrySentencia.toString(), lVarBind);

		} catch(Exception e) {
			log.info("ParametrosDescuentoBean::getDatosCtasEpo(Exception)");
			e.printStackTrace();
			throw new AppException("Error al actualizar la informacion");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::getDatosCtasEpo(S)");
		}
		return registros;
	}

	/**
	 * Este metodo devuelve el resultado de actualizacion
	 *
	 * @since 	Migracion_Nafin-Marzo-2013
	 * @param 	Clave if, clave epo
	 * @return 	String
	 */
	public String setDatosCtasEpo(String icIF,String icEpo, String txtNumCuenta, String ic_num_cuenta_CLABE,String VoBoNafin,
					String Plaza, String Sucursal, String cmb_banco, String txtBanco, String txtPlazaAnt, String iNoUsuario ){
		log.info("ParametrosDescuentoBean::setDatosCtasEpo(E)");

		if(	icIF == null || icIF.equals("") ||	icEpo == null || icEpo.equals("")	){
			throw new AppException("Los parametros no deben ser nulos.");
		}

		AccesoDB 		con 				= new AccesoDB();
		String strMensaje = "";
		boolean transOk = false;

		//**variables bitacora
		List valAnt = new ArrayList();
		String [] nombresAnteriores = new String[5];
		List valAct = new ArrayList();
		String [] nombresActuales = new String[5];
		StringBuffer valAnteriores = new StringBuffer();
		StringBuffer valNuevos = new StringBuffer();
		String clabeAnt ="";
		//**

		try
		{
			con.conexionDB();
			String campo = "";
			String nafinElectronico = "";

			String query =
				" SELECT ic_nafin_electronico"   +
				"   FROM comrel_nafin"   +
				"  WHERE ic_epo_pyme_if = "+icEpo+
				"    AND cg_tipo = 'E'"  ;
			ResultSet rs = con.queryDB(query);
			if(rs.next()) {
				nafinElectronico = rs.getString(1);
			}
			rs.close();
			con.cierraStatement();

			if(!"".equals(cmb_banco)) { //BANCO NUEVO
				campo =
					"        ,cg_banco = '"+cmb_banco+"'";
			}

//***** Para insertar en bitacora
			String queryBuscaDatosAnt = "";
			if(txtPlazaAnt.equals("")){
				queryBuscaDatosAnt =
				" SELECT cn.cg_num_cuenta,'' plaza,cn.ig_sucursal,cn.cg_cuenta_clabe"+
				"   FROM comrel_if_epo cn"   +
				"  WHERE cn.ic_epo = "+icEpo+
				"		 AND cn.ic_if = "+icIF;

			}else{
			queryBuscaDatosAnt =
				" SELECT cn.cg_num_cuenta,cp.cd_descripcion,cn.ig_sucursal,cn.cg_cuenta_clabe"+
				"   FROM comrel_if_epo cn,comcat_plaza cp"   +
				"  WHERE cn.ic_plaza = cp.ic_plaza"+
				"    AND cn.ic_epo = "+icEpo+
				"		 AND cn.ic_if = "+icIF;
			}

			ResultSet rs1 = con.queryDB(queryBuscaDatosAnt);
			if(rs1.next()) {
				valAnt.add(rs1.getString(1)==null?"":rs1.getString(1));
				nombresAnteriores[0]="No Cuenta";
				valAnt.add(rs1.getString(2)==null?"":rs1.getString(2));
				nombresAnteriores[1]="Plaza";
				valAnt.add(rs1.getString(3)==null?"":rs1.getString(3));
				nombresAnteriores[2]="Sucursal";
				clabeAnt = rs1.getString(4)==null?"":rs1.getString(4);
				valAnt.add(clabeAnt);
				nombresAnteriores[3]="Cuenta CLABE";
				valAnt.add(txtBanco);
				nombresAnteriores[4]="BANCO ANTERIOR";
			}
			rs1.close();
			con.cierraStatement();

			//valores actuales
			String plazaNueva = "";
			if(Plaza.equals("")){
			String queryPlaza = " SELECT cd_descripcion FROM comcat_plaza WHERE ic_plaza='"+Plaza+"'";
			ResultSet rs2 = con.queryDB(queryPlaza);
			if(rs2.next()) {
				plazaNueva = rs2.getString(1);
			}
			rs2.close();
			con.cierraStatement();
			}

			valAct.add(txtNumCuenta);
			nombresActuales[0]="No Cuenta";
			valAct.add(plazaNueva);
			nombresActuales[1]="Plaza";
			valAct.add(Sucursal);
			nombresActuales[2]="Sucursal";
			if(!"".equals(ic_num_cuenta_CLABE)){
			valAct.add(ic_num_cuenta_CLABE);
			}else{
			valAct.add(clabeAnt);
			}
			nombresActuales[3]="Cuenta CLABE";
			if(!"".equals(cmb_banco)){
			valAct.add(cmb_banco);
			}else{
			valAct.add(txtBanco);
			}
			nombresActuales[4]="BANCO NUEVO";
			System.out.println("Anteriores:::: "+valAnt+" ::::"+nombresAnteriores);
			System.out.println("Nuevos:::: "+valAct+" ::::"+nombresActuales);

			valAnteriores.append(Bitacora.getValoresBitacora(
					valAnt, nombresAnteriores));

			valNuevos.append(Bitacora.getValoresBitacora(
					valAct, nombresActuales));

			Bitacora.grabarEnBitacora(con, "CTASBANE", "E",
				nafinElectronico, iNoUsuario,valAnteriores.toString(),valNuevos.toString());
//**************

		    String qryCCuenta =
				" UPDATE comrel_if_epo"   +
				"    SET cg_num_cuenta = '"+txtNumCuenta+"',"   +
				"        ic_plaza = '"+Plaza+"',"   +
				"        ig_sucursal = '"+Sucursal+"'";
    if(!"".equals(ic_num_cuenta_CLABE))
				{
					qryCCuenta +=	"        ,cg_cuenta_clabe = '"+ic_num_cuenta_CLABE+"',";
					qryCCuenta += "        cs_autorizacion_clabe = 'N'";
				}else{
					qryCCuenta +=	"        ,cg_cuenta_clabe = null,";
					qryCCuenta += "        cs_autorizacion_clabe = 'N'";
				}
			qryCCuenta +=
				campo+
				"  WHERE ic_epo = "+icEpo+
				"    AND ic_if = "+icIF;
			con.ejecutaSQL(qryCCuenta);

			transOk = true;
			strMensaje = "La cuenta ha sido actualizada";

		} catch(Exception e) {
			strMensaje = "Hubo un error al actualizar la cuenta";
			transOk = false;
			log.info("ParametrosDescuentoBean::setDatosCtasEpo(Exception)");
			e.printStackTrace();
			//throw new AppException("Error al actualizar la informacion");
		} finally {
			con.terminaTransaccion(transOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDescuentoBean::setDatosCtasEpo(S)");
		}
		return strMensaje;
	}




	/**  Fodea 010-2013 - Reafiliacion Automatica
	 * Adminitraci�n � Parametrizacion - Seleccion IF- Individudal y Masiva
	 * @throws com.netro.exception.NafinException
	 * @param strNombreUsuario
	 * @param strLogin
	 * @param sMoneda
	 * @param nombreEpo
	 * @param sEpo
	 * @param nombrePyme
	 * @param sPyme
	 * @param sIntermediario
	 * @param sIF
	 */
public void paramCUDescAuto(String sIF,  String sIntermediario,
										String sPyme, String nombrePyme,
										String sEpo, String nombreEpo,
										String sMoneda, String strLogin ,
										String strNombreUsuario, String pantalla, AccesoDB con) throws NafinException {
    List lVarBind = new ArrayList();
    PreparedStatement ps = null;
	 ResultSet rs = null;
	 int ic_version_convenio = 0;
    String query = "";
    boolean ok = false;
	 String descAuto ="N", nun_elecPyme ="", cs_aceptacion ="";
	 boolean existeParam 	= false;

	 log.info("paramCUDescAuto (E)");

    try   {

			log.debug("sEpo   "+sEpo);
			log.debug("sPyme   "+sPyme);
			log.debug("sIF   "+sIF);

			if(sIntermediario.equals("") && nombrePyme.equals("")  &&  nombreEpo.equals("") )  {
				//obtengo nombre PYME
				query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_pyme  WHERE ic_pyme = ? ";
				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				ps.setInt(1, Integer.parseInt(sPyme));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					nombrePyme =  rs.getString("CG_RAZON_SOCIAL").trim();
				}
				rs.close();
				ps.close();

				//obtengo nombre EPO
				query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_epo  WHERE ic_epo = ? ";
				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				ps.setInt(1, Integer.parseInt(sEpo));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					nombreEpo =  rs.getString("CG_RAZON_SOCIAL").trim();
				}
				rs.close();
				ps.close();

				//obtengo nombre del IF
				query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_if  WHERE ic_if= ? ";
				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				ps.setInt(1, Integer.parseInt(sIF));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					sIntermediario =  rs.getString("CG_RAZON_SOCIAL").trim();
				}
				rs.close();
				ps.close();
			}


			//verifica  sea una  reafiliaci�n
			query = "SELECT cs_aceptacion    " +
					"  FROM  comrel_pyme_epo  " +
					" WHERE ic_pyme = ? "+
					"  AND  ic_epo = ? ";

			ps = con.queryPrecompilado(query);
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(sPyme));
			ps.setInt(2, Integer.parseInt(sEpo));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				cs_aceptacion = rs.getString("cs_aceptacion")==null?"":rs.getString("cs_aceptacion");
			}
			rs.close();
			ps.close();


			log.debug("cs_aceptacion   "+cs_aceptacion);

			//if(cs_aceptacion.equals("H")  || cs_aceptacion.equals("R") )  {
			if(cs_aceptacion.equals("H") )  {
				//SE VERIFICA el tipo de convenio
				query = "SELECT ic_version_convenio   " +
						"  FROM  comcat_pyme " +
						" WHERE ic_pyme = ? ";

				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				ps.setInt(1, Integer.parseInt(sPyme));
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					ic_version_convenio = rs.getInt("ic_version_convenio");
				}
				rs.close();
				ps.close();

				log.debug("ic_version_convenio   "+ic_version_convenio);

				if(ic_version_convenio>=3){

				// verifico si la pyme esta parametrizada con descuento Automatico
				 String strQry = " select n.IC_NAFIN_ELECTRONICO AS NUM_NAFIN, "+
										" p.CS_DSCTO_AUTOMATICO AS DSCTO_AUT   "+
										" FROM  comrel_nafin n ,  "+
										" comcat_pyme p  "+
										" Where n.IC_EPO_PYME_IF = p.ic_pyme "+
										" and CG_TIPO = 'P'  "+
										" and p.ic_pyme = ? ";

					lVarBind		= new ArrayList();
					lVarBind.add(new Integer(sPyme));
					ps = con.queryPrecompilado(strQry,lVarBind );
					rs = ps.executeQuery();

					if(rs.next() && rs!=null){
						descAuto = rs.getString("DSCTO_AUT").trim();
						nun_elecPyme = rs.getString("NUM_NAFIN").trim();

					}
					rs.close();
					ps.close();

					log.debug("descAuto   "+descAuto);

					if(descAuto.equals("S")) {

						int desAutomatico	 =0;

						//reviso que parametrizacion tiene en la version nueva
						//NA aun no esta parametrizada en la tabla COMREL_DSCTO_AUT_PYME

						strQry =" SELECT COUNT (*) AS total  " +
								  " FROM (SELECT a.ic_pyme, a.ic_epo, a.ic_if,  "+
								  " DECODE (dap.cs_dscto_automatico_proc,  '', 'NA', dap.cs_dscto_automatico_proc    ) AS cs_dscto_automatico_proc  "+
								  " FROM (SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */  " +
								  " pi.ic_epo, cb.ic_pyme, pi.ic_if, cb.ic_moneda  "+
								  " FROM comrel_cuenta_bancaria cb,   "+
								  " comrel_pyme_if pi, "+
								  " comrel_pyme_epo rp "+
								  " WHERE cb.ic_moneda = ?  "+
								  " AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "+
								  " AND pi.cs_borrado = 'N'  "+
								  " AND cb.cs_borrado = 'N'  "+
								  " AND pi.cs_vobo_if = 'S'  "+
								  " AND pi.cs_opera_descuento = 'S'  "+
								  " AND cb.ic_pyme = ? "+
								  " AND cb.ic_pyme = rp.ic_pyme  "+
								  " AND rp.cs_aceptacion = 'H'  " +
								  " AND pi.ic_epo = rp.ic_epo  "+
								  " AND pi.ic_if = ?   "+
								  " AND pi.ic_epo !=    ?  ) a,  "+
								  " comrel_dscto_aut_pyme dap  "+
								  " WHERE a.ic_if = dap.ic_if(+)  "+
								  " AND a.ic_epo = dap.ic_epo(+)  "+
								  " AND a.ic_pyme = dap.ic_pyme(+)  "+
								  " AND a.ic_moneda = dap.ic_moneda(+)) pa  "+
								  " WHERE cs_dscto_automatico_proc != 'S'  ";

						lVarBind		= new ArrayList();
						lVarBind.add(new Integer(sMoneda));
						lVarBind.add(new Integer(sPyme));
						lVarBind.add(new Integer(sIF));
						lVarBind.add(new Integer(sEpo));
						ps = con.queryPrecompilado(strQry,lVarBind );
						log.debug("strQry::  "+strQry);
						log.debug("lVarBind::  "+lVarBind);
						rs = ps.executeQuery();
						if(rs.next() && rs!=null){
							desAutomatico = Integer.parseInt(rs.getString("total").trim());
						}
						rs.close();
						ps.close();

					  /*si desAutomatico == 0  entonces todas las parametrizaciones estan en "S"
						* por lo cual si realizare la parametrizaci�n del Descuento Automatico
					  */
					   log.debug("desAutomatico   "+desAutomatico);
						if(desAutomatico==0 )  {

							int desAutomaticoEPO	 =0;
							strQry ="";
							strQry =" SELECT COUNT (*) AS total  " +
									  " FROM (SELECT a.ic_pyme, a.ic_epo, a.ic_if,  "+
									  " DECODE (dap.cs_dscto_automatico_proc,  '', 'NA', dap.cs_dscto_automatico_proc    ) AS cs_dscto_automatico_proc  "+
									  " FROM (SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */  " +
									  " pi.ic_epo, cb.ic_pyme, pi.ic_if, cb.ic_moneda  "+
									  " FROM comrel_cuenta_bancaria cb,   "+
									  " comrel_pyme_if pi, "+
									  " comrel_pyme_epo rp "+
									  " WHERE cb.ic_moneda = ?  "+
									  " AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "+
									  " AND pi.cs_borrado = 'N'  "+
									  " AND cb.cs_borrado = 'N'  "+
									  " AND pi.cs_vobo_if = 'S'  "+
									  " AND pi.cs_opera_descuento = 'S'  "+
									  " AND cb.ic_pyme = ? "+
									  " AND cb.ic_pyme = rp.ic_pyme  "+
									  " AND rp.cs_aceptacion = 'H'  " +
									  " AND pi.ic_epo = rp.ic_epo  "+
									  " AND pi.ic_if != ?   "+
									  " AND pi.ic_epo =  ?  ) a,  "+
									  " comrel_dscto_aut_pyme dap  "+
									  " WHERE a.ic_if = dap.ic_if(+)  "+
									  " AND a.ic_epo = dap.ic_epo(+)  "+
									  " AND a.ic_pyme = dap.ic_pyme(+)  "+
									  " AND a.ic_moneda = dap.ic_moneda(+)) pa  "+
									  " WHERE cs_dscto_automatico_proc != 'S'  ";

							lVarBind		= new ArrayList();
							lVarBind.add(new Integer(sMoneda));
							lVarBind.add(new Integer(sPyme));
							lVarBind.add(new Integer(sIF));
							lVarBind.add(new Integer(sEpo));
							ps = con.queryPrecompilado(strQry,lVarBind );
							log.debug("strQry::  "+strQry);
							log.debug("lVarBind::  "+lVarBind);
							rs = ps.executeQuery();
							if(rs.next() && rs!=null){
								desAutomaticoEPO = Integer.parseInt(rs.getString("total").trim());
							}
								rs.close();
								ps.close();

						log.debug("desAutomaticoEPO::  "+desAutomaticoEPO);

						if(desAutomaticoEPO==0) {


							Hashtable alParamEPO1 = this.getParametrosEPO(String.valueOf(sEpo),1);
							String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
							String nombreFactoraje ="", orden ="0";

							List tipoFactoraje = new ArrayList();
							tipoFactoraje.add("N");// Factoraje Normal
							if(bOperaFactorajeVencido.equals("S"))  {
								tipoFactoraje.add("V"); // Factoraje Vencido
							}

							StringBuffer SQL	= 	new StringBuffer();
							SQL.append("	INSERT INTO COMREL_DSCTO_AUT_PYME	");
							SQL.append("		(											");
							SQL.append("			IC_PYME								");
							SQL.append("			,IC_EPO								");
							SQL.append("			,IC_IF								");
							SQL.append("			,IC_MONEDA							");
							SQL.append("			,CS_ORDEN							");
							SQL.append("			,CS_DSCTO_AUTOMATICO_DIA		");
							SQL.append("			,CS_DSCTO_AUTOMATICO_PROC		");
							SQL.append("			,CC_TIPO_FACTORAJE				");
							SQL.append("			,IC_USUARIO							");
							SQL.append("			,CG_NOMBRE_USUARIO				");
							SQL.append("			,CC_ACUSE							");
							SQL.append("			,DF_FECHAHORA_OPER				");
							SQL.append("		)											");
							SQL.append("		VALUES (									");
							SQL.append("			?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, SYSDATE    ) ");

							StringBuffer insertBitacora = new StringBuffer();
							insertBitacora.append(
											"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
											"\n IC_CAMBIO,  " +
											"\n CC_PANTALLA_BITACORA,  " +
											"\n CG_CLAVE_AFILIADO, " +
											"\n IC_NAFIN_ELECTRONICO, " +
											"\n DF_CAMBIO, " +
											"\n IC_USUARIO, " +
											"\n CG_NOMBRE_USUARIO, " +
											"\n CG_ANTERIOR, " +
											"\n CG_ACTUAL, " +
											"\n IC_GRUPO_EPO, " +
											"\n IC_EPO ) " +
											"\n VALUES(  " +
											"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
											"\n 'DESAUTOMA', " +
											"\n 'P', " +
											"\n  ?  , " +
											"\n SYSDATE, " +
											"\n    ? ,  " +
											"\n   ? , " +
											"\n '',  " +
											"\n   ?,  " +
											"\n '', " +
											"\n '' ) " );


							List variables = new ArrayList();
							PreparedStatement pst		= 	null;

							for(int y=0; y<tipoFactoraje.size(); y++)  {
								String mensajeOrden ="", moneda ="";
								String valorFact =  (String)tipoFactoraje.get(y);
								//valido si existe ya la parametrizaci�n
								existeParam= existeParametrizacion(sEpo, sIF, sPyme, sMoneda, valorFact, con);

								log.debug("existeParam   "+existeParam);
								if(!existeParam){

									if(valorFact.equals("N"))  {
										orden=  getOrden(sEpo, sPyme, sMoneda, valorFact);
										nombreFactoraje = "Normal";  mensajeOrden= "Orden= "+orden+"\n";
									}
									if(valorFact.equals("V"))  {  orden = "0";  nombreFactoraje = "Vencido"; }
									if(sMoneda.equals("54"))    {    moneda = "DOLAR AMERICANO"; }
									if(sMoneda.equals("1"))    {    moneda = "MONEDA NACIONAL"; }

									String actual =  mensajeOrden+
														  " Modalidad= Primer dia desde su publicacion "+"\n"+
														  " EPO="+nombreEpo+"\n"+
														  " PYME="+nombrePyme+"\n"+
														  " IF="+sIntermediario	+"\n"+
														  " Moneda= "+moneda+	"\n"+
														  " Pantalla= "+pantalla+"\n"+
														  " Tipo Factoraje =" +nombreFactoraje+"\n"+
														  " Descuento Autom�tico = S " +
														  " Metodo(paramCUDescAuto)";

									// insert en COMREL_DSCTO_AUT_PYME
									variables = new ArrayList();
									variables.add(new Integer(sPyme));	//PYME
									variables.add( new Integer(sEpo));	//EPO
									variables.add( new Integer(sIF)); //IC_IF
									variables.add( new Integer(sMoneda)); //MONEDA
									variables.add(orden);						//ORDEN
									variables.add("P");							//MODALIDAD p -> primer dia...
									variables.add("S");							//S - APLICA DSCTO AUTOMATICO
									variables.add(valorFact);				//TIPO FACTIRAJE
									variables.add(strLogin);					//IC_USUARIO
									variables.add(strNombreUsuario);		//NOMBRE USUARIO
									variables.add("0");	    //ACUSE

									log.debug("SQL.toString(  "+SQL.toString());
									log.debug("variables(  "+variables);


									pst = con.queryPrecompilado(SQL.toString(), variables);
									pst.executeUpdate();
									pst.close();

									//insert en BIT_CAMBIOS_GRAL
									variables = new ArrayList();
									variables.add(nun_elecPyme);	//nun_elecPyme
									variables.add( strLogin);	//EPO
									variables.add( strNombreUsuario); //IC_IF
									variables.add( actual); //MONEDA
									log.debug("SQL.toString(  "+SQL.toString());
									log.debug("variables(  "+variables);
									pst = con.queryPrecompilado(insertBitacora.toString(), variables);
									pst.executeUpdate();
									pst.close();

									log.debug("S� la �ltima acci�n para todas las EPO�s fue parametrizar el Descuento Autom�tico con el IF, el sistema SI parametrizar� el Descuento Autom�tico a la(s) EPO(s)  ");
								}

							}//for

							}

						}	else  {
							log.debug("S� la �ltima acci�n para alguna EPO fue Desparametrizar el Descuento Autom�tico con el IF  No se hace NADA ");
						}


					}else {
						log.debug("LA PYME NO ESTA PARAMETRIZADA CON DESCUENTO AUTOMATICO  ");
					}

				}else  {
					log.debug("LA PYME TIENE TIPO DE CONVENIO MENOR A 3  ");
				}

			}
	//	}else  {
	//		log.debug(" la relaci�n EPO-IF no tiene  parametrizado la reafiliaci�n Automatica  ");
	//	}

      ok = true;
    }catch(SQLException sqle){
      ok = false;
      log.error("paramCUDescAuto(SQLException)" + sqle.getMessage());
      sqle.printStackTrace();
      throw new NafinException("AFIL0045");
    }catch(Exception e){
      ok = false;
      log.error("paramCUDescAuto(Exception)" + e.getMessage());
      e.printStackTrace();
      throw new NafinException("AFIL0045");
    }finally{
			 // es responsabilidad de metodo donde es llamado el commit
     log.info("paramCUDescAuto:::::::::(S)  ");
    }
  }

  /**
	 * verifico si existe parametrizaci�n del Descuento Automatico
	 * Fodea 010-2013 - Reafiliacion Automatica
	 * @return
	 * @param cc_tipo_factoraje
	 * @param ic_moneda
	 * @param ic_pyme
	 * @param ic_if
	 * @param ic_epo
	 */
    private boolean existeParametrizacion(String ic_epo, String ic_if, String ic_pyme, String ic_moneda, String cc_tipo_factoraje , AccesoDB con){
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      boolean           exiteIf  =  false;
		List lVarBind		= new ArrayList();
		log.info("existeParametrizacion (E)");
		 try{


         String strQry = "SELECT count(1) AS existe_if  "  +
                        "  FROM comrel_dscto_aut_pyme   "  +
                        " WHERE ic_if = ?               "  +
                        "   AND ic_epo = ?              "  +
                        "   AND ic_pyme = ?             "  +
                        "   AND ic_moneda = ?           "  +
                        "   AND cc_tipo_factoraje = ?   ";
			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_if));
         lVarBind.add(new Integer(ic_epo));
         lVarBind.add(new Integer(ic_pyme));
         lVarBind.add(new Integer(ic_moneda));
         lVarBind.add(cc_tipo_factoraje);
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

			log.info("strQry "+  strQry);
			log.info("lVarBind "+  lVarBind);

         if(rs.next() && rs!=null){
            exiteIf = Integer.parseInt(rs.getString("existe_if").trim())>0?true:false;
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  existeParametrizacion (S)" +e);
         throw new RuntimeException(e);
      }finally{
			log.info("existeParametrizacion (S)");
      }

      return exiteIf;
    }


	/**
	 *  Para Obtener el Orden
	 * @return
	 * @param cc_tipo_factoraje
	 * @param ic_moneda
	 * @param ic_pyme
	 * @param ic_if
	 * @param ic_epo
	 */
    private String getOrden(String ic_epo, String ic_pyme, String ic_moneda, String cc_tipo_factoraje){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       orden  =  "0";
		List lVarBind		= new ArrayList();
		int ordenF =0;

		log.info("getOrden (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT MAX(CS_ORDEN)  as orden  "  +
                        "  FROM comrel_dscto_aut_pyme   "  +
                        " WHERE ic_epo = ?              "  +
                        "   AND ic_pyme = ?             "  +
                        "   AND ic_moneda = ?           "  +
                        "   AND cc_tipo_factoraje = ?   ";
			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_epo));
         lVarBind.add(new Integer(ic_pyme));
         lVarBind.add(new Integer(ic_moneda));
         lVarBind.add(cc_tipo_factoraje);
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
				orden = rs.getString("orden")==null?"0":rs.getString("orden");
         }
			rs.close();
       	ps.close();


			ordenF =  Integer.parseInt(orden)+1;
			orden  =  String.valueOf(ordenF);


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOrden (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getOrden (S)");
      return orden;
    }


	/**
	 * Se obtienen las calves de la EPOs de los sistemas externos asigandas por los IFS
	 * (ahorita solo hay de BANAMEX)
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public List consultaClavesEpoWSIF(String cveIF) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstClavesEpo = new ArrayList();
		String strQry= "";

		try{
			con.conexionDB();

			strQry = "SELECT cie.ic_if, cie.ic_epo, ce.cg_razon_social, cws.ig_epo_externa, " +
				"       NVL (cc_tipo_factoraje, 'V') cc_tipo_factoraje " +
				"  FROM comrel_if_epo_x_producto cie, " +
				"       comrel_producto_epo cpe, " +
				"       comrel_if_epo_wsif cws, " +
				"       comcat_epo ce " +
				" WHERE cie.ic_epo = cpe.ic_epo " +
				"   AND cie.ic_producto_nafin = cpe.ic_producto_nafin " +
				"   AND cie.ic_epo = ce.ic_epo " +
				"   AND cie.ic_if = cws.ic_if(+) " +
				"   AND cie.ic_epo = cws.ic_epo(+) " +
				"   AND cie.ic_producto_nafin = ? " +
				"   AND cpe.cs_factoraje_vencido = ? " +
				"   AND cie.ic_if = ? ";

			ps = con.queryPrecompilado(strQry);
			ps.setLong(1, Long.parseLong("1"));
			ps.setString(2, "S");
			ps.setLong(3, Long.parseLong(cveIF));

			rs = ps.executeQuery();

			HashMap hmEposFactVenc = null;
			while(rs.next()){
				hmEposFactVenc = new HashMap();
				hmEposFactVenc.put("ic_if", rs.getString("ic_if"));
				hmEposFactVenc.put("ic_epo", rs.getString("ic_epo"));
				hmEposFactVenc.put("cg_razon_social", rs.getString("cg_razon_social"));
				hmEposFactVenc.put("ig_epo_externa", rs.getString("ig_epo_externa"));
				hmEposFactVenc.put("cc_tipo_factoraje", rs.getString("cc_tipo_factoraje"));

				lstClavesEpo.add(hmEposFactVenc)	;
			}

			rs.close();
			ps.close();


			return lstClavesEpo;


		}catch(Throwable t){
			throw new AppException("Error al consultar claves de EPO asignada por Intemediario para WSIF", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	public void guardaCveEposWsIF(List lstClavesEpos) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String strQry = "";
		String strQryIns = "";
		boolean commit = true;

		try{
			con.conexionDB();
			String icEpo = "";
			String icIf = "";
			String igEpoExt = "";
			String tipoFact = "";

			strQry = "UPDATE comrel_if_epo_wsif " +
				"   SET ig_epo_externa = ? " +
				" WHERE ic_if = ? AND ic_epo = ? " +
				" AND cc_tipo_factoraje = ? ";

			strQryIns = "INSERT INTO comrel_if_epo_wsif " +
				"            (ic_if, ic_epo, ig_epo_externa, cc_tipo_factoraje " +
				"            ) " +
				"     VALUES (?, ?, ?, ? " +
				"            ) ";


			if(lstClavesEpos!=null && lstClavesEpos.size()>0){
				for(int x=0; x<lstClavesEpos.size(); x++){
					HashMap hm = (HashMap)lstClavesEpos.get(x);
					icEpo =  (String)hm.get("ic_epo");
					icIf = (String)hm.get("ic_if");
					igEpoExt = (String)hm.get("ig_epo_externa");
					tipoFact = (String)hm.get("cc_tipo_factoraje");

					ps = con.queryPrecompilado(strQry);
					if(igEpoExt==null || igEpoExt.equals(""))
						ps.setNull(1, Types.INTEGER);
					else
						ps.setLong(1, Long.parseLong(igEpoExt));

					ps.setLong(2, Long.parseLong(icIf));
					ps.setLong(3, Long.parseLong(icEpo));
					ps.setString(4, tipoFact);
					int nUpdate = ps.executeUpdate();
					ps.close();

					if(nUpdate==0 && igEpoExt!=null && !igEpoExt.equals("")){
						ps = con.queryPrecompilado(strQryIns);
						ps.setLong(1, Long.parseLong(icIf));
						ps.setLong(2, Long.parseLong(icEpo));
						ps.setLong(3, Long.parseLong(igEpoExt));
						ps.setString(4, tipoFact);
						ps.executeUpdate();
						ps.close();
					}
				}
			}


		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al gardar las calves extrenas de las EPOs por IF ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Metodo para saber si la el IF Opera Fideicomiso de Desarrollo de Proveedores
	 * Fodea 017-2013
	 * @return
	 * @param ic_if
	 * @param ic_pyme
	 */
	public String getOperaFideicomiIF(String ic_if){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       operaFide  =  "N";
		List lVarBind		= new ArrayList();
		log.info("getOperaFideicomiIF (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT CS_OPERA_FIDEICOMISO  "  +
                        "  FROM comcat_if    "  +
                        " WHERE ic_if = ?  ";

			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_if));
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
				operaFide= (rs.getString("CS_OPERA_FIDEICOMISO")==null?"N":rs.getString("CS_OPERA_FIDEICOMISO"));
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOperaFideicomiIF (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getOperaFideicomiIF (S)");
      return operaFide;
    }

	/**
	 * Fodea 017-2013
	 * @return
	 * @param ic_pyme
	 */
	 /*
	public int getOperaConevio4PYME(String ic_pyme){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      int       convenio4  =  0;
		List lVarBind		= new ArrayList();
		log.info("getOperaConevio4PYME (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT ic_version_convenio  "  +
                        "  FROM comcat_pyme    "  +
                        " WHERE ic_pyme = ?  ";

			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_pyme));
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next()){
				convenio4 = rs.getInt("ic_version_convenio");
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOperaConevio4PYME (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getOperaConevio4PYME (S)");
      return convenio4;
    }
	 */

	 /**
	 *  Para saber si la epo opera con fideicomiso
	 * @return parametrizacion de la epo opera_fideicomiso

	 * @param ic_epo
	 */

    public String getOperaFideicomisoEpo(String ic_epo){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       operaFide  =  "N";
		List lVarBind		= new ArrayList();
		log.info("getOperaFideicomisoEpo (E)");
		 try{
         con.conexionDB();

         String strQry = "select CG_VALOR as CS_OPERA_FIDEICOMISO from COM_PARAMETRIZACION_EPO  where ic_epo = ? and CC_PARAMETRO_EPO =  'CS_OPERA_FIDEICOMISO'  ";
			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_epo));
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
				operaFide= (rs.getString("CS_OPERA_FIDEICOMISO")==null?"N":rs.getString("CS_OPERA_FIDEICOMISO"));
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOperaFideicomisoEpo (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getOperaFideicomisoEpo (S)");
      return operaFide;
    }

	 /**
	 *  Fodea 019-2014
	 * @return
	 * @param ic_pyme
	 */

	public String  getOperaFideicomisoPYME(String ic_pyme ){
            AccesoDB 	      con   =  new AccesoDB();
            ResultSet 	      rs	   =  null;
            PreparedStatement ps    =  null;
            StringBuffer strQry = new StringBuffer();
            String operaFide_Pyme ="N";
            log.info("getOperaFideicomisoPYME (E)");
            
            try{
                con.conexionDB();
                // Vetifico si la  Pyme  opera Fideicomiso
                strQry = new StringBuffer();
                strQry.append(" SELECT  cs_opera_fideicomiso  "+
                                            " FROM comcat_pyme  "+
                                            " WHERE ic_pyme =  ?  ");
                
                ps = con.queryPrecompilado(strQry.toString());
                ps.setInt(1, Integer.parseInt(ic_pyme));
                rs = ps.executeQuery();
                
                if(rs.next() && rs!=null){
                    operaFide_Pyme= (rs.getString("cs_opera_fideicomiso")==null?"N":rs.getString("cs_opera_fideicomiso"));
                }
                rs.close();
                ps.close();        
            }catch(Exception e){
                e.printStackTrace();
                con.terminaTransaccion(false);
                log.error("Error en  getOperaFideicomisoPYME (S)" +e);
                throw new RuntimeException(e);
            }finally{
                if(con.hayConexionAbierta()) {
                    con.terminaTransaccion(true);
                    con.cierraConexionDB();
                }
            }
            log.info("getOperaFideicomisoPYME (S)");
            return operaFide_Pyme;
        }

	 /**
	 *  Metodo  para saber si la Epo e IF fueron parametrizados con Factoraje Vencido
	 *  en la pantalla Relaci�n EPO - IF Fodea 016-2014
	 * @return
	 * @param ic_if
	 * @param ic_epo
	 */

	public String  getOperaFactVencido(String ic_epo, String ic_if  ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		String operaFactVencido ="N";

		List lVarBind		= new ArrayList();
		log.info("getOperaFactVencido (E)");

		 try{

			con.conexionDB();

			strQry = new StringBuffer();
			strQry.append(" SELECT CS_FACTORAJE_VENCIMIENTO  as FAC_VENCIDO  "+
								" from comrel_if_epo  "+
								"	where  ic_if = ?  "+
								"	and ic_epo =   ?   ");

			lVarBind		= new ArrayList();
			lVarBind.add(new Integer(ic_if));
			lVarBind.add(new Integer(ic_epo));

			log.info("strQry  " +strQry);
			log.info("lVarBind  " +lVarBind);


			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			if(rs.next()){
				operaFactVencido= (rs.getString("FAC_VENCIDO")==null?"N":rs.getString("FAC_VENCIDO"));
         }
			rs.close();
       	ps.close();



      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOperaFactVencido (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getOperaFactVencido (S)");
      return operaFactVencido;
    }

	 /**
	 * Fodea 019-2014
	 * @return
	 * @param grupo
	 */
	public String  getValidaFisoXGrupoEpos(String grupo ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		String operaFide_grupoEpo ="NO_OPERA";

		List lVarBind		= new ArrayList();
		log.info("getValidaFisoXGrupoEpos (E)");
		int operanFiso =0, noOperanFiso=0;

		 try{

			con.conexionDB();
			// Vetifico si la  Pyme  opera Fideicomiso
			strQry = new StringBuffer();
			strQry.append(" SELECT  distinct cg_valor  AS cs_opera_fideicomiso "+
							  " FROM com_parametrizacion_epo  "+
							  " WHERE ic_epo in  (select ic_epo from  comrel_grupo_x_epo where IC_GRUPO_EPO  =  ?   )   "+
							  " AND cc_parametro_epo = ?   " );

			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(grupo));
			lVarBind.add("CS_OPERA_FIDEICOMISO");

			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			while (rs.next() && rs!=null){
				String opera= (rs.getString("cs_opera_fideicomiso")==null?"N":rs.getString("cs_opera_fideicomiso"));
				if(opera.equals("S")) {
					operanFiso++;
				}else if(opera.equals("N")) {
					noOperanFiso++;
				}
         }
			rs.close();
       	ps.close();

			log.info("noOperanFiso "+noOperanFiso);
			log.info("noOperanFiso "+noOperanFiso);



			 if(operanFiso==0 &&  noOperanFiso>0) {  //todas las EPOS del grupo NO operan fiso
				operaFide_grupoEpo ="NO_OPERA";
			}else  if(operanFiso>0 &&  noOperanFiso==0) { // todas las EPOS del grupo operan fiso
				operaFide_grupoEpo ="NORMAL";
			}else if(operanFiso>0) {  // alguna de las EPOS del Grupo si opera FISO,
				operaFide_grupoEpo ="OPERA";
			}


			log.info("operaFide_grupoEpo "+operaFide_grupoEpo);


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getValidaFisoXGrupoEpos (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getValidaFisoXGrupoEpos (S)");
      return operaFide_grupoEpo;
    }




	 /**
	 * Metodo que verifica si la Pyme tiene cuentas
	 * @return
	 * @param ic_nafin_electronico
	 */
	public String  getValidaCcuentaClablePyme(String ic_nafin_electronico  ){
		log.info("getValidaCcuentaClablePyme (E)");
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		String validaCcuentaClablePyme ="N", validaCcuentaClablePyme_1 ="N", valida = "N";

		StringBuffer qrySentencia1 = new StringBuffer();


		List lVarBind		= new ArrayList();
		List lVarBind1		= new ArrayList();

		 try{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" select 	count(*)    "+
			" from com_cuentas "+
			" where  ic_nafin_electronico = ?  "+
			" and IC_CUENTA  = (select  MAX(IC_CUENTA)   from com_cuentas  "+
			" where  ic_nafin_electronico = ?   and IC_ESTATUS_CECOBAN = ?  ) ");

			lVarBind		= new ArrayList();
			lVarBind.add(ic_nafin_electronico);
			lVarBind.add(ic_nafin_electronico);
			lVarBind.add("99");

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				if(rs.getInt(1)>0) {
					validaCcuentaClablePyme="S";
				}else{
					qrySentencia1.append(" select 	count(*)    "+
					" from com_cuentas "+
					" where  ic_nafin_electronico = ?  "+
					"  and IC_ESTATUS_CECOBAN is null  ");


					lVarBind1		= new ArrayList();
					lVarBind1.add(ic_nafin_electronico);
					log.debug("qrySentencia1.toString() "+qrySentencia1.toString());
					log.debug("varBind1 "+lVarBind1);

					PreparedStatement ps1 = con.queryPrecompilado(qrySentencia1.toString(), lVarBind1);
					ResultSet  rs1 = ps1.executeQuery();
					if(rs1.next()){
						if(rs1.getInt(1)>0) {
							validaCcuentaClablePyme_1="S";
						}
					}
					rs1.close();
					ps1.close();
				}
			}
			rs.close();
			ps.close();

			log.debug("validaCcuentaClablePyme "+validaCcuentaClablePyme);
			log.debug("validaCcuentaClablePyme_1 "+validaCcuentaClablePyme_1);
			if("S".equals(validaCcuentaClablePyme) && "N".equals(validaCcuentaClablePyme_1) )  {
				valida = "S";
			}else if("N".equals(validaCcuentaClablePyme) && "S".equals(validaCcuentaClablePyme_1) )  {
				valida = "E";  // La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla
			}

			log.debug("valida "+valida);

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getValidaCcuentaClablePyme (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getValidaCcuentaClablePyme (S)");
      return valida;
    }

	 /**Metodo para copiar la Cuenta CLABE de la PYME en la nueva Cadena Productiva
	  * y se registra con estatus Cuenta Correcta
	 * Fodea 019-2014
	 * @return
	 * @param grupo
	 */
	public String  getcuentaClablePyme(String ic_nafin_electronico, String login, String ic_epo, AccesoDB con  ){
       ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      PreparedStatement ps1    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		String respuesta ="";

		List lVarBind		= new ArrayList();
		log.info("getcuentaClablePyme (E)");

		String ic_producto_nafin ="",  ic_moneda= "",  ic_bancos_tef ="",
				cg_cuenta ="", df_transferencia ="",
				df_aplicacion="", ic_tipo_cuenta ="", cg_tipo_afiliado ="",  ic_estatus_cecoban ="",
				cg_plaza_swift ="",  cg_sucursal_swift ="";

		 try{

			DispersionBean disp  = new DispersionBean();
			String sSigLlave		= disp.sLlaveSiguiente();  //  ic_cuenta


			//Obtengo los datos de la Cuenta
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select "+
			"  ic_producto_nafin, ic_moneda , ic_bancos_tef,   df_ultima_mod,  "+
			" TO_CHAR (df_transferencia, 'dd/mm/yyyy') as df_transferencia  , "+
			" TO_CHAR ( df_aplicacion, 'dd/mm/yyyy') as df_aplicacion,  "+
			" ic_tipo_cuenta, cg_tipo_afiliado , ic_estatus_cecoban , "+
			" cg_plaza_swift,  cg_sucursal_swift,     "+
			" cg_cuenta  "+
			" from com_cuentas "+
			" where  ic_nafin_electronico = ?  "+
			" and IC_CUENTA  = (select  MAX(IC_CUENTA)   from com_cuentas  "+
			" where  ic_nafin_electronico = ?   and IC_ESTATUS_CECOBAN = ?  )  ");

			lVarBind		= new ArrayList();
			lVarBind.add(ic_nafin_electronico);
			lVarBind.add(ic_nafin_electronico);
			lVarBind.add("99");

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				ic_producto_nafin =(rs.getString("ic_producto_nafin")==null?"":rs.getString("ic_producto_nafin"));
				ic_moneda= (rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda"));
				ic_bancos_tef =(rs.getString("ic_bancos_tef")==null?"":rs.getString("ic_bancos_tef"));
				cg_cuenta =(rs.getString("cg_cuenta")==null?"":rs.getString("cg_cuenta"));
				df_transferencia =(rs.getString("df_transferencia")==null?"":rs.getString("df_transferencia"));
				df_aplicacion=(rs.getString("df_aplicacion")==null?"":rs.getString("df_aplicacion"));
				ic_tipo_cuenta =(rs.getString("ic_tipo_cuenta")==null?"":rs.getString("ic_tipo_cuenta"));
				cg_tipo_afiliado =(rs.getString("cg_tipo_afiliado")==null?"":rs.getString("cg_tipo_afiliado"));
				ic_estatus_cecoban =(rs.getString("ic_estatus_cecoban")==null?"":rs.getString("ic_estatus_cecoban"));
				cg_plaza_swift =(rs.getString("cg_plaza_swift")==null?"":rs.getString("cg_plaza_swift"));
				cg_sucursal_swift =(rs.getString("cg_sucursal_swift")==null?"":rs.getString("cg_sucursal_swift"));
			}
			rs.close();
			ps.close();

		  if(respuesta.equals("")) {
				qrySentencia = new StringBuffer();
				qrySentencia.append(" INSERT INTO com_cuentas ( "+
										" ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda,  ic_bancos_tef, "+
										" cg_cuenta, ic_usuario, df_ultima_mod,  df_registro, df_transferencia , "+
										" df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado , ic_epo, ic_estatus_cecoban ,"+
										" cg_plaza_swift,  cg_sucursal_swift  ) "+
										" VALUES ( "+sSigLlave+" ,"+ ic_nafin_electronico+","+ic_producto_nafin+","+ic_moneda +","+ic_bancos_tef+", '"+
														cg_cuenta+"','"+login+"', SYSDATE, SYSDATE, to_date('"+df_transferencia+"','dd/mm/yyyy') , "+
										"			 to_date( '"+df_aplicacion+"' ,'dd/mm/yyyy'), "+ic_tipo_cuenta+",'"+ cg_tipo_afiliado+"',"+ ic_epo+", 99 ,"+
										"'"+cg_plaza_swift+"','"+cg_sucursal_swift+"') ");


				log.error("qrySentencia  " +qrySentencia);
				ps1 = con.queryPrecompilado(qrySentencia.toString());
				ps1.executeUpdate();
				ps1.close();

		  }

      }catch(Exception e){
         e.printStackTrace();

			log.error("Error en  getValidaFisoXGrupoEpos (S)" +e);
         throw new RuntimeException(e);
      }finally{
      }
      log.info("getcuentaClablePyme (S)");
      return respuesta;
    }

	 /**
	 * Fodea 034-2014
	 * @return
	 * @param ic_pyme
	 */

	 public String  getOpEntidadGobierno(String ic_pyme ){
		log.info("getOpEntidadGobierno (E)");
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		String entidad_Gobierno ="N";
		List lVarBind		= new ArrayList();

		 try{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" select 	CS_ENTIDAD_GOBIERNO   "+
			" from COMCAT_PYME "+
			" where  ic_pyme =  ?   ");

			lVarBind		= new ArrayList();
			lVarBind.add(ic_pyme);

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				entidad_Gobierno =(rs.getString("CS_ENTIDAD_GOBIERNO")==null?"N":rs.getString("CS_ENTIDAD_GOBIERNO"));
			}
			rs.close();
			ps.close();
			log.debug("entidad_Gobierno "+entidad_Gobierno);

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getOpEntidadGobierno (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getValidaCcuentaClablePyme (S)");
      return entidad_Gobierno;
    }


	 /**
	 esta validacion es apra una reafiliaci�n de una pyme que opera fideicomiso en otra epo  que opera fideicomiso
	 * @return
	 * @param ic_epo
	 * @param ic_pyme
	 */
	public String  getValidaPymeEpoRea(String ic_pyme, String ic_epo  ){
		log.info("getValidaPymeEpoRea (E)");
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List lVarBind		= new ArrayList();
		String valida = "N";
		 try{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" select  count(*) as total    from " +
			" comrel_pyme_epo r , COM_PARAMETRIZACION_EPO  e , comcat_pyme p  "+
			" where  r.cs_aceptacion = ?   "+
			" and r.ic_pyme  =  ? "+
			" and r.ic_epo != ? "+
			" and e.CC_PARAMETRO_EPO = ?   "+
			" and e.CG_VALOR =  ?  "+
			" and r.ic_epo = e.ic_epo "+
			" and r.ic_pyme   = p.ic_pyme  "+
			" and p.CS_OPERA_FIDEICOMISO  = ?   ");

			lVarBind		= new ArrayList();
			lVarBind.add("H");
			lVarBind.add(ic_pyme);
			lVarBind.add(ic_epo);
			lVarBind.add("CS_OPERA_FIDEICOMISO");
			lVarBind.add("S");
			lVarBind.add("S");

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				if(rs.getInt(1)>0) {
					valida = "S";
				}
			}
			rs.close();
			ps.close();


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getValidaPymeEpoRea (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getValidaPymeEpoRea (S)");
      return valida;
    }

	/**
	 * Metodo que valida si la pyme tiene epos que operan dispersi�n
	 * Fodea 033-2014
	 * @return
	 * @param ic_pyme
	 */
	public String  getValidaEposDispersionPyme(String ic_pyme  ){
		log.info("getValidaPymeEpoRea (E)");
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List lVarBind		= new ArrayList();
		String valida = "N";
		 try{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT ic_epo, pe.cg_dispersion "+
										"  FROM comrel_producto_epo pe  "+
										" WHERE  pe.ic_producto_nafin = ?   "+
										" and pe.ic_epo in (  select IC_EPO from  comrel_pyme_epo "+
										" where ic_pyme  = ?  ) "+
										" and pe.cg_dispersion = ?  ");

			lVarBind		= new ArrayList();
			lVarBind.add("1");
			lVarBind.add(ic_pyme);
			lVarBind.add("S");

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				if(rs.getInt(1)>0) {
					valida = "S";
				}
			}
			rs.close();
			ps.close();


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getValidaPymeEpoRea (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getValidaPymeEpoRea (S)");
      return valida;
    }

	/**
	 * Metodo que obtiene el nombre de la moneda
	 *  Fodea 033-2014
	 * @return
	 * @param ic_moneda
	 */
	public String  getNombreMoneda(String ic_moneda  ){
		log.info("getNombreMoneda (E)");
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List lVarBind		= new ArrayList();
		String moneda = "";
		 try{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT cd_nombre  FROM comcat_moneda   WHERE  ic_moneda  =   ?  ");
			lVarBind		= new ArrayList();
			lVarBind.add(ic_moneda);
			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				moneda =(rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre"));
			}
			rs.close();
			ps.close();


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getNombreMoneda (E)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getNombreMoneda (S)");
      return moneda;
    }

	 /**
	 * Metodo que obtiene  todas las epos que operan Dispersi�n para la Pyme
	 *  Fodea 033-2014
	 * @return
	 * @param moneda
	 * @param nafinElectrico
	 * @param ic_pyme
	 */
	public Hashtable getDatosDispersion(String ic_pyme, String nafinElectrico, String moneda, String txtCuenta  ){
		log.info("getDatosDispersion (E)");
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Hashtable		mapa				= new Hashtable();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		int totalCuentas =0;

		try	{

			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append(" select count(*) as total from com_cuentas  "+
									  " where IC_NAFIN_ELECTRONICO =  ?  "+
									  " and IC_PRODUCTO_NAFIN =  ?   "+
									  " and IC_MONEDA = ?  ");

			lVarBind		= new ArrayList();
			lVarBind.add(nafinElectrico);
			lVarBind.add("1");
			lVarBind.add(moneda);

			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         while(rs.next()){
				totalCuentas +=rs.getInt(1);
			}
			rs.close();
			ps.close();


			qrySentencia = new StringBuffer();
			qrySentencia.append( "	SELECT 'Proveedor' AS tipo_afiliado, e.ic_epo AS ic_epo, "+
						" e.cg_razon_social AS nombre_epo, "+
						" b.ic_cuenta AS ic_cuenta, "+
						" banco AS banco, b.cg_cuenta AS cuenta_clabe, "+
						" b.CG_PLAZA  as PLAZA,    "+
					   " b. CG_SUCURSAL AS SUCURSAL,   "+
					   " b.IC_SWIFT as IC_SWIFT,  "+
					   " b.IC_ABA as IC_ABA , "+
						" 'N' AS seleccionar  "+
						" FROM comrel_producto_epo pe,  "+
						"  comcat_epo e,  "+
						" comrel_pyme_epo re,  "+

						" (SELECT c.ic_epo, c.ic_cuenta,  c.CG_CUENTA   , bt.CD_DESCRIPCION   as banco  ,  "+
						" c.CG_PLAZA_SWIFT as CG_PLAZA  ,   c.CG_SUCURSAL_SWIFT as CG_SUCURSAL  ,   "+
						" c.IC_SWIFT as IC_SWIFT , c.IC_ABA as IC_ABA  "+

						" FROM com_cuentas c , comcat_bancos_tef bt  "+
						" WHERE c.ic_moneda = ? "+
						" AND c.ic_nafin_electronico = ? "+
						" AND c.ic_producto_nafin = ? "+
						" and c.IC_BANCOS_TEF = bt.IC_BANCOS_TEF   ) b  "+

					   " WHERE pe.ic_producto_nafin = ?    "+
					   " 	AND pe.cg_dispersion = ?    "+
					   "	AND pe.ic_epo = e.ic_epo  "+
					   "	AND pe.ic_epo = re.ic_epo  "+
					   "	AND re.ic_pyme = ?  "+
					   "	and b.ic_epo(+) = pe.ic_epo    ");

						lVarBind = new ArrayList();
						lVarBind.add(moneda);
						lVarBind.add(nafinElectrico);
						lVarBind.add("1");
						lVarBind.add("1");
						lVarBind.add("S");
						lVarBind.add(ic_pyme);



				log.debug("qrySentencia.toString() = " + qrySentencia.toString() );
				log.debug("lVarBind = " + lVarBind );

				registros = new Registros();
				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				mapa.put("registros",	registros);
				mapa.put("totalCuentas",String.valueOf(totalCuentas));


		} catch(Exception e) {
			log.info("getDatosDispersion Exception "+e);
			e.printStackTrace();
			throw new AppException("Error al obtener datos de las EPos que Operan Dispersi�n");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDatosDispersion (S) ");
		}
		return mapa;
	}







	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_cuenta_bancarias
	 * @param ic_cuentas
	 * @param ic_epos
	 * @param txtBanco
	 * @param confirtxtCuentaClabe
	 * @param chkCU
	 * @param txtCtaClabe
	 * @param cboPlaza
	 * @param txtCuenta
	 * @param txtSucursal
	 * @param ic_aba
	 * @param ic_Swift
	 * @param cg_tipo_afiliado
	 * @param ic_tipo_cuenta
	 * @param cg_cuenta_cue
	 * @param txtSucursal_cue
	 * @param ic_bancos_tef
	 * @param ic_moneda
	 * @param ic_producto_nafin
	 * @param ic_nafin_electronico
	 * @param sPyme
	 * @param loginUsuario
	 */
	public String ovinsertarCuentaCBP(  String loginUsuario, String sPyme,   String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, String  ic_bancos_tef ,  String txtSucursal_cue,  String  txtPlaza_cue , String cg_cuenta_cue,
										String ic_tipo_cuenta, 	String cg_tipo_afiliado,  	String ic_Swift , String  ic_aba , 	String txtCtaClabe ,
										String ic_epos[], String ic_cuentas[] , String rfc )	throws NafinException {

		log.info("  ovinsertarCuentaCBP (E)");

		DispersionBean  dips = new DispersionBean();
		AccesoDB		con 			= null;
		ResultSet	rs				= null;
		PreparedStatement ps = null;
		boolean resultado = true;
		List 		lVarBind			= new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();

		String  strMensaje	= "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {
			con = new AccesoDB();
			con.conexionDB();


			for(int i=0;i<ic_epos.length;i++) {

				String ic_epo = ic_epos[i];
				String ic_cuenta  =ic_cuentas[i];

				log.debug("ic_epo ----------- "+ic_epo);
				log.debug("ic_cuenta -----------"+ic_cuenta);

				//VALIDA QUE NO TENGA MAS DE UNA CUENTA POR EPO, PROVEEDOR, MONEDA Y PRODUCTO.
				int existeCuenta = 0;
				qrySentencia = new StringBuffer();
				qrySentencia.append(" SELECT COUNT (1)"   +
											"	FROM com_cuentas"   +
											"	WHERE ic_nafin_electronico = ? "   +
											"  AND ic_producto_nafin    = ? "   +
											"  AND ic_tipo_cuenta       = ? "   +
											"	AND ic_epo = ?"  +
											" 	AND cg_tipo_afiliado = ?  ");

				lVarBind = new ArrayList();
				lVarBind.add(ic_nafin_electronico);
				lVarBind.add(ic_producto_nafin);
				lVarBind.add(ic_tipo_cuenta);
				lVarBind.add(ic_epo);
				lVarBind.add("P");

				log.debug("qrySentencia.toString() "+qrySentencia.toString());
				log.debug("varBind "+lVarBind);

				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();

				if(rs.next()) {
					existeCuenta = rs.getInt(1);
				}
				rs.close();
				ps.close();
				//if(existeCuenta>0)
					//throw new NafinException("DISP0017");


				//VALIDA QUE LA CUENTA NO ESTE ASIGNADA A OTRO USUARIO
				qrySentencia = new StringBuffer();
				qrySentencia.append(" SELECT   /*+index(c CP_COM_CUENTAS_PK) use_nl(crn c)*/ "   +
					"	crn.cg_tipo, crn.ic_epo_pyme_if "   +
					"  FROM com_cuentas c, comrel_nafin crn,  comcat_pyme p "   +
					"  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico "+
					"  	AND c.cg_cuenta = ? "   +
					"		AND c.ic_nafin_electronico != ? " +
					" 		AND ic_epo != ? " +
					"  	and crn.CG_TIPO =  ?  "+
					"		and p.ic_pyme = crn.ic_epo_pyme_if ");
					// NOTA: SE USARA LA CONDICION: AND ic_epo != ? DEBIDO A QUE NO PERMITE DETECTAR CORRECTAMENTE
					//  PROVEEDORES EN UNA MISMA EPO QUE TENGAN LA CUENTA REPETIDA:  AND ic_epo != ?

				lVarBind		= new ArrayList();
				lVarBind.add(cg_cuenta_cue);
				lVarBind.add(ic_nafin_electronico);
				lVarBind.add(ic_epo);
				lVarBind.add("P");

				log.debug("qrySentencia.toString() "+qrySentencia.toString());
				log.debug("varBind "+lVarBind);

				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();
				existeCuenta = 0;
				while(rs.next()){

					ResultSet			rs1				= null;
					String				qrySentencia1	= "";
					PreparedStatement	ps1				= null;

					//String rs_tipo = rs.getString(1)==null?"":rs.getString(1);
					String rs_epo_pyme_if = rs.getString(2)==null?"":rs.getString(2);
						qrySentencia1 =
							" SELECT COUNT (1)"   +
							"   FROM comcat_if"   +
							"  WHERE ic_if = ?"   +
							"    AND cg_rfc != ?"  ;
					//System.out.println("\nqrySentencia1: "+qrySentencia1);
					ps1 = con.queryPrecompilado(qrySentencia1);
					ps1.setInt(1, Integer.parseInt(rs_epo_pyme_if));
					ps1.setString(2, rfc);
					rs1 = ps1.executeQuery();
					ps1.clearParameters();
					if(rs1.next())
						existeCuenta += rs1.getInt(1);
					rs1.close();ps1.close();

				}//while
				rs.close();ps.close();
				if(existeCuenta>0)
					throw new NafinException("DISP0018");


				Calendar cFechaSigHabil = new GregorianCalendar();
				cFechaSigHabil.setTime(new java.util.Date());
				cFechaSigHabil.add(Calendar.DATE, 1);
				if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
					cFechaSigHabil.add(Calendar.DATE, 2);
				if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
					cFechaSigHabil.add(Calendar.DATE, 1);
				Vector vFecha = dips.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
				boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
				while(bInhabil) {
					vFecha = dips.bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
					bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
					if(!bInhabil)
						break;
					cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
				}
				String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
				log.debug("Fecha siguiente: "+sFechaSigHabil);


				// Si la cuenta a registrar es en dolares, autom�ticamente se almacenar�n con el estatus 99 (Cuenta Correcta).
				String estatusCuenta ="";
				if(ic_moneda.equals("54")){
					estatusCuenta 	= "99";
					txtCtaClabe = cg_cuenta_cue;
				}

				if(existeCuenta==0 ){

					log.debug("ic_cuenta ::: "+ic_cuenta );

					if ("".equals(ic_cuenta)){

						String 			sSigLlave ="";
						qrySentencia = new StringBuffer();
						qrySentencia.append(	" SELECT  NVL(MAX(ic_cuenta),0)+1 FROM com_cuentas ");
						System.out.print("DispersionBean :: sLlaveSiguiente :: Query "+qrySentencia);
						rs = con.queryDB(qrySentencia.toString());
						while(rs.next()){
								sSigLlave = rs.getString(1);
						}
						rs.close();


						qrySentencia = new StringBuffer();
						qrySentencia.append(	" INSERT INTO com_cuentas  ( "+
												" ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, "+
												" ic_bancos_tef, cg_cuenta, ic_usuario, df_ultima_mod,  "+
												" df_registro, df_transferencia, df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado , ic_epo , ic_estatus_cecoban ,"+
												" cg_plaza_swift, cg_sucursal_swift , IC_SWIFT  , IC_ABA   ) "+
						" VALUES ("+sSigLlave+","+ic_nafin_electronico+","+ic_producto_nafin+","+ic_moneda+","+ic_bancos_tef+", '"+txtCtaClabe+"','"+ loginUsuario+"', "+
						" SYSDATE,  SYSDATE, to_date( '" + sFechaSigHabil +"' ,'dd/mm/yyyy'), to_date( '" + sFechaSigHabil +"' ,'dd/mm/yyyy'), "+
						ic_tipo_cuenta+",'"+cg_tipo_afiliado+"',"+ic_epo+",'"+estatusCuenta+"','"+txtPlaza_cue+"','"+txtSucursal_cue+"', '"+ic_Swift+"','"+ic_aba+"'  )  ");

						log.debug("qrySentencia ::: "+qrySentencia.toString() );

						ps = con.queryPrecompilado(qrySentencia.toString() );
						ps.executeUpdate();
						ps.close();

					}else {

						qrySentencia = new StringBuffer();
						qrySentencia.append( " Update com_cuentas  "+
													" set ic_bancos_tef = "+ic_bancos_tef +", "+
													" cg_cuenta = '"+txtCtaClabe+"', "+
													" ic_usuario = '"+loginUsuario+"', "+
													" df_ultima_mod  =  Sysdate , "+
													" IC_SWIFT  = '"+ic_Swift+"' , "+
													" IC_ABA  =  '"+ic_aba+"' , "+
													" cg_plaza_swift  =  '"+txtPlaza_cue+"', "+
													" cg_sucursal_swift  = '"+txtSucursal_cue+"' ,  "+
													" ic_estatus_cecoban = '"+estatusCuenta+"' "+
													" where  ic_cuenta = "+ic_cuenta+
													" and ic_epo = "+ic_epo);

						log.debug("qrySentencia ::: "+qrySentencia.toString() );
						ps = con.queryPrecompilado(qrySentencia.toString() );
						ps.executeUpdate();
						ps.close();
					}
				}


			}	// For

			return strMensaje;
		}catch(NafinException ne){
			log.debug("ovinsertarCuentaCBP:: NafinException"+ ne);
			ne.printStackTrace();
			resultado = false;
			throw ne;
		}catch(Exception e){
			log.debug("ovinsertarCuentaCBP:: NafinException"+ e);
			e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.debug("ovinsertarCuentaCBP:: (S)");
		}
	} /*Fin del M�todo ovinsertarCuenta*/

	/**
	 * Obtiene los datos de la pantalla de Otros Parametros
	 * @return
	 */
	public boolean getValidaCveCder(){
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean valida = true;

		String query_dat_nafin =
			" SELECT  cs_valida_cve_cder_oper_tel "  +
			"   FROM comcat_producto_nafin"   +
			"  WHERE ic_producto_nafin = 1"  ;

		try{
			con.conexionDB();
			ps = con.queryPrecompilado(query_dat_nafin);
			rs = ps.executeQuery();
			if (rs.next()) {
				String csValida = rs.getString("cs_valida_cve_cder_oper_tel")==null?"S":rs.getString("cs_valida_cve_cder_oper_tel");
				valida = "N".equals(csValida)?false:true;
			}
			rs.close();
			ps.close();

			return valida;
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error al consultar si esta activo la validacion de la clave de cesion ",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene el nombre de la PyME que opera con factoraje distribuido
	 * @param numElectronico
	 * @return
	 */
	public List  datosPymes(String numElectronico){

		String              qrySentencia    = null;
		PreparedStatement   ps              = null;
		ResultSet           rs              = null;
		AccesoDB            con             = new AccesoDB();
		List                datos           = new ArrayList();
		String              icPyme          = "";
		String              txtNombre       = "";

		try{
			con.conexionDB();

			if(!"".equals(numElectronico)) {

				qrySentencia =  "SELECT distinct n.ic_epo_pyme_if as Pyme,  p.cg_razon_social as nombre "+
								" FROM comrel_nafin n, comcat_pyme p "   +
								" WHERE n.ic_epo_pyme_if = p.ic_pyme and p.CS_FACT_DISTRIBUIDO='S' "+
								" AND ic_nafin_electronico = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setBigDecimal(1, new BigDecimal(numElectronico));
				rs = ps.executeQuery();
				if(rs.next()){
					icPyme = rs.getString(1)==null?"":rs.getString(1);
					txtNombre = rs.getString(2)==null?"":rs.getString(2);
					datos.add(icPyme);
					datos.add(txtNombre);

				}
				rs.close();
				ps.close();
			}
	} catch(Exception e) {
		throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
		return datos;
	}

	/**
	 * Pantalla Administraci�n / Parametrizaci�n / Cuentas Bancarias Pymes - Dactoraje Distribuido.
	 * Obtiene los datos para generar el grid de contratos parametrizados
	 * @param id
	 * @param tabla
	 * @return
	 * @throws AppException
	 */
	@Override
	public String obtenerJSONContratosFactDistribuido(String id, String tabla) throws AppException {
		log.info("obtenerJSONContratosFactDistribuido(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuilder strSQL = new StringBuilder();
		StringBuilder cadenaJSONparametros = new StringBuilder();
		HashMap parametro = new HashMap();
		List varBind = new ArrayList();
		JSONArray jsonArr = new JSONArray();
		try {
			String nombreTabla = "COM_CONTRATOS_DISTRIBUIDO";
			if("TMP".equals(tabla)){
				nombreTabla = "COMTMP_CONTRATOS_DISTRIBUIDO";
			}
			con.conexionDB();
			strSQL.append("SELECT IC_CONSECUTIVO,CG_NUM_CONTRATO FROM "+nombreTabla+" WHERE IC_CONSECUTIVO=? ORDER BY CG_NUM_CONTRATO");
			varBind.add(new Long(id));
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			while(rst.next()) {
				parametro = new HashMap();
				parametro.put("IC_CONSECUTIVO", rst.getString("IC_CONSECUTIVO"));
				parametro.put("CG_NUM_CONTRATO", rst.getString("CG_NUM_CONTRATO"));
				jsonArr.add(JSONObject.fromObject(parametro));
			}
			rst.close();
			pst.close();
			cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + "})");
		} catch(Exception e) {
			log.error("obtenerJSONContratosFactDistribuido(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerJSONContratosFactDistribuido(S) ::..");
		}
		return cadenaJSONparametros.toString();
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Obtiene las cuentas que se agregan temporalmente para ser mostradas en un grid.
	 * @param ids Lista de icConsecutivo a consultar
	 * @param icConsecutivo Este solo lo traigo para regresarlo en el json
	 * @param tipoTabla Indica si consulta la tabla temporal o la tabla real
	 * @param usuario Este solo lo traigo para regresarlo en el json
	 * @return
	 * @throws AppException
	 */
	@Override
	public String obtenerJSONCuentasAgregadasTmp(List<String> ids, Long icConsecutivo, String tipoTabla, String usuario) throws AppException {
		log.info("obtenerJSONCuentasAgregadasTmp(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuilder strSQL = new StringBuilder();
		StringBuilder cadenaJSONparametros = new StringBuilder();
		HashMap parametro = new HashMap();
		List varBind = new ArrayList();
		JSONArray jsonArr = new JSONArray();
		StringBuilder idsStr = new StringBuilder();
		String fecha = "";
		String tabla ="COMREL_CTA_BAN_DISTRIBUIDO";
		if("TMP".equals(tipoTabla)) {
			tabla ="COMTMPREL_CTA_BAN_DISTRIBUIDO";
		}
		try {
			con.conexionDB();
			strSQL.append("SELECT A.IC_CONSECUTIVO\n"
			+",A.IC_IF\n"
			+",A.IC_EPO\n"
			+",A.IC_PYME\n"
			+",A.IC_BENEFICIARIO\n"
			+",A.IC_MONEDA\n"
			+",A.IC_NAFIN_ELECTRONICO\n"
			+",A.IC_BANCOS_TEF\n"
			+",A.CG_SUCURSAL\n"
			+",A.CG_NUMERO_CUENTA\n"
			+",A.CG_CUENTA_CLABE\n"
			+",A.CG_CUENTA_SPID\n"
			+",A.CG_SWIFT\n"
			+",A.CG_ABA\n"
			+",CASE WHEN A.CG_AUTORIZA_IF='S' THEN 'SI' ELSE 'NO' END AS CG_AUTORIZA_IF\n"
			+",CASE WHEN A.CG_CAMBIO_CUENTA='S' THEN 'SI' ELSE 'NO' END AS CG_CAMBIO_CUENTA\n"
			+",CASE WHEN A.CG_CAMBIO_CUENTA='N' OR A.CG_AUTORIZA_IF='N' THEN 'ROJO' ELSE '' END AS COLOR\n"
			+",B.CG_RAZON_SOCIAL AS NOMBRE_EPO\n"
			+",E.CG_RAZON_SOCIAL AS NOMBRE_PYME\n"
			+",C.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO\n"
			+",G.CD_DESCRIPCION AS BANCO_SERVICIO\n"
			+",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO\n"
			+",F.CD_NOMBRE AS NOMBRE_MONEDA\n"
			+",H.CD_DESCRIPCION AS CG_PLAZA\n");
			if("TMP".equals(tipoTabla)) {
				strSQL.append(",'' AS FECHA\n");
			} else {
				strSQL.append(",TO_CHAR(A.DF_FECHA_ALTA,'dd/mm/yyyy hh:mi:ss') AS FECHA\n");
			}
			strSQL.append(",'TMP' AS TABLA\n"
			+"FROM "+tabla+" A\n"
			+"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO\n"
			+"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n"
			+"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF\n"
			+"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME\n"
			+"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA\n"
			+"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF\n"
			+"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA\n"
			+"WHERE A.IC_CONSECUTIVO IN(");
			for (int i = 0; i < ids.size(); i++) {
				if(i > 0){
					strSQL.append(",");
					idsStr.append("A");
				}
				strSQL.append("?");
				varBind.add(Long.parseLong(ids.get(i)));
				idsStr.append(ids.get(i));
			}
			strSQL.append(") ORDER BY A.IC_CONSECUTIVO");
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			while(rst.next()) {
				parametro = new HashMap();
				parametro.put("NOMBRE_EPO",               rst.getString("NOMBRE_EPO"));
				parametro.put("NOMBRE_PYME",              rst.getString("NOMBRE_PYME"));
				parametro.put("IC_NAFIN_ELECTRONICO",     rst.getString("IC_NAFIN_ELECTRONICO"));
				parametro.put("INTERMEDIARIO_FINANCIERO", rst.getString("INTERMEDIARIO_FINANCIERO"));
				parametro.put("BANCO_SERVICIO",           rst.getString("BANCO_SERVICIO"));
				parametro.put("NOMBRE_BENEFICIARIO",      rst.getString("NOMBRE_BENEFICIARIO"));
				parametro.put("CG_SUCURSAL",              rst.getString("CG_SUCURSAL"));
				parametro.put("NOMBRE_MONEDA",            rst.getString("NOMBRE_MONEDA"));
				parametro.put("CG_NUMERO_CUENTA",         rst.getString("CG_NUMERO_CUENTA"));
				parametro.put("CG_CUENTA_CLABE",          rst.getString("CG_CUENTA_CLABE"));
				parametro.put("CG_CUENTA_SPID",           rst.getString("CG_CUENTA_SPID"));
				parametro.put("CG_SWIFT",                 rst.getString("CG_SWIFT"));
				parametro.put("CG_ABA",                   rst.getString("CG_ABA"));
				parametro.put("CG_PLAZA",                 rst.getString("CG_PLAZA"));
				parametro.put("IC_CONSECUTIVO",           rst.getString("IC_CONSECUTIVO"));
				fecha = rst.getString("FECHA");
				jsonArr.add(JSONObject.fromObject(parametro));
			}
			rst.close();
			pst.close();

			if("TMP".equals(tipoTabla)) {
				cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + ",\"ids\":\"" + idsStr.toString() + "\",\"id\":\""+icConsecutivo+"\"})");
			} else {
				cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + ",\"usuario\":\"" +usuario + "\",\"fecha\":\""+fecha+"\"})");
			}
		} catch(Exception e) {
			log.error("obtenerJSONCuentasAgregadasTmp(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerJSONCuentasAgregadasTmp(S) ::..");
		}
		return cadenaJSONparametros.toString();
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Agrega una cuenta y sus contratos a una tabla temporal
	 * @param numNePyme
	 * @param icEpo
	 * @param icIf
	 * @param benef
	 * @param moneda
	 * @param bancoServicio
	 * @param plaza
	 * @param sucursal
	 * @param cuenta
	 * @param cuentaClabe
	 * @param cuentaSpid
	 * @param swift
	 * @param aba
	 * @param icPyme
	 * @param cgContratos
	 * @return id del registro insertado
	 * @throws NafinException
	 */
	@Override
	public Long agregarCuentaTmp(String numNePyme, Long icEpo, Long icIf, Long benef, Long moneda, Long bancoServicio,
								 Long plaza, String sucursal, String cuenta, String cuentaClabe, String cuentaSpid, String swift,
								 String aba, Long icPyme, String[] cgContratos) throws NafinException {

		log.info("agregarCuentaTmp(E) ::..");
		Long icConsecutivo = 0L;
		boolean bOk = false;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rst = null;
		StringBuilder strSQL = new StringBuilder();
		StringBuilder insertSQL = new StringBuilder();
		try {
			con.conexionDB();
			//Obtengo el max(id)+1 de la tabla temporal para realizar la inserci�n
			strSQL.append("SELECT MAX(IC_CONSECUTIVO)+1 AS ID FROM COMTMPREL_CTA_BAN_DISTRIBUIDO");
			ps = con.queryPrecompilado(strSQL.toString());
			rst = ps.executeQuery();
			if(rst.next()) {
				icConsecutivo = rst.getLong("ID");
			}
			if(null==icConsecutivo || 0==icConsecutivo){
				icConsecutivo=1L;
			}
			ps.clearParameters();

			//Inserto los registros
			int cont=1;
			insertSQL.append("INSERT INTO COMTMPREL_CTA_BAN_DISTRIBUIDO(IC_CONSECUTIVO,IC_IF,IC_EPO,IC_PYME,IC_BENEFICIARIO,IC_MONEDA,IC_NAFIN_ELECTRONICO,CG_SUCURSAL,CG_NUMERO_CUENTA");
			if(null!=bancoServicio && bancoServicio>0){
				insertSQL.append(",IC_BANCOS_TEF");
			}
			if(null!=plaza && plaza>0){
				insertSQL.append(",IC_PLAZA");
			}
			if(null!=cuentaClabe && !"".equals(cuentaClabe)){
				insertSQL.append(",CG_CUENTA_CLABE");
			}
			if(null!=cuentaSpid && !"".equals(cuentaSpid)){
				insertSQL.append(",CG_CUENTA_SPID");
			}
			if(null!=swift && !"".equals(swift)){
				insertSQL.append(",CG_SWIFT");
			}
			if(null!=aba && !"".equals(aba)){
				insertSQL.append(",CG_ABA");
			}
			insertSQL.append(") VALUES(?,?,?,?,?,?,?,?,?");
			if(null!=bancoServicio && bancoServicio>0){
				insertSQL.append(",?");
			}
			if(null!=plaza && plaza>0){
				insertSQL.append(",?");
			}
			if(null!=cuentaClabe && !"".equals(cuentaClabe)){
				insertSQL.append(",?");
			}
			if(null!=cuentaSpid && !"".equals(cuentaSpid)){
				insertSQL.append(",?");
			}
			if(null!=swift && !"".equals(swift)){
				insertSQL.append(",?");
			}
			if(null!=aba && !"".equals(aba)){
				insertSQL.append(",?");
			}
			insertSQL.append(")");
			ps = con.queryPrecompilado(insertSQL.toString());
			ps.setLong(cont++, icConsecutivo);
			ps.setLong(cont++, icIf);
			ps.setLong(cont++, icEpo);
			ps.setLong(cont++, icPyme);
			ps.setLong(cont++, benef);
			ps.setLong(cont++, moneda);
			ps.setBigDecimal(cont++, new BigDecimal(numNePyme));
			ps.setString(cont++, sucursal);
			ps.setString(cont++, cuenta);
			if(null!=bancoServicio && bancoServicio>0){
				ps.setLong(cont++, bancoServicio);
			}
			if(null!=plaza && plaza>0){
				ps.setLong(cont++, plaza);
			}
			if(null!=cuentaClabe && !"".equals(cuentaClabe)){
				ps.setString(cont++, cuentaClabe);
			}
			if(null!=cuentaSpid && !"".equals(cuentaSpid)){
				ps.setString(cont++, cuentaSpid);
			}
			if(null!=swift && !"".equals(swift)){
				ps.setString(cont++, swift);
			}
			if(null!=aba && !"".equals(aba)){
				ps.setString(cont++, aba);
			}
			log.debug("..:: insertSQL: "+insertSQL);
			ps.executeUpdate();
			ps.clearParameters();

			//Inserto los contratos en la tabla temporal
			for(int i=0; i<cgContratos.length; i++){
				ps = con.queryPrecompilado("INSERT INTO COMTMP_CONTRATOS_DISTRIBUIDO(IC_CONSECUTIVO,CG_NUM_CONTRATO) VALUES(?,?)");
				ps.setLong(1, icConsecutivo);
				ps.setString(2, cgContratos[i]);
				ps.executeUpdate();
				ps.clearParameters();
			}

			rst.close();
			ps.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("agregarCuentaTmp(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("agregarCuentaTmp(S) ::..");
		}
		return icConsecutivo;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Agrega una cuenta y sus contratos.
	 * @param icConsecutivo id del registro de la tabla temporal
	 * @param usuario usuario que realiza la acci�n
	 * @return id del registro insertado
	 * @throws NafinException
	 */
	@Override
	public Long agregarCuenta(Long icConsecutivo, String usuario) throws NafinException {
		log.info("agregarCuenta(E) ::..");
		boolean bOk = false;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rst = null;
		StringBuilder insertSQL = new StringBuilder();
		Long ic=0L;
		Long icBenef = 0L;
		try {
			con.conexionDB();
			//1. Obtengo el max(id)+1 de la tabla para realizar la inserci�n
			ps = con.queryPrecompilado("SELECT MAX(IC_CONSECUTIVO)+1 AS ID FROM COMREL_CTA_BAN_DISTRIBUIDO");
			rst = ps.executeQuery();
			if(rst.next()) {
				ic = rst.getLong("ID");
			}
			if(null==ic || 0==ic){
				ic=1L;
			}
			ps.clearParameters();

			//2. Inserto los datos de la tabla temporal en la tabla original
			insertSQL.append("INSERT INTO COMREL_CTA_BAN_DISTRIBUIDO (IC_CONSECUTIVO,IC_IF,IC_EPO,IC_PYME,\n" + 
			"IC_BENEFICIARIO,IC_MONEDA,IC_PLAZA,IC_NAFIN_ELECTRONICO,IC_BANCOS_TEF,CG_SUCURSAL,\n" + 
			"CG_NUMERO_CUENTA,CG_CUENTA_CLABE,CG_CUENTA_SPID,CG_SWIFT,CG_ABA,DF_FECHA_ALTA,IC_USUARIO_ALTA)\n" + 
			"SELECT ?,IC_IF,IC_EPO,IC_PYME,\n" + 
			"IC_BENEFICIARIO,IC_MONEDA,IC_PLAZA,IC_NAFIN_ELECTRONICO,IC_BANCOS_TEF,CG_SUCURSAL,\n" + 
			"CG_NUMERO_CUENTA,CG_CUENTA_CLABE,CG_CUENTA_SPID,CG_SWIFT,CG_ABA,SYSDATE,?\n" + 
			"FROM COMTMPREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO=?");
			ps = con.queryPrecompilado(insertSQL.toString());
			ps.setLong(1, ic);
			ps.setString(2, usuario);
			ps.setLong(3, icConsecutivo);
			log.debug("..:: insertSQL: "+insertSQL);
			ps.executeUpdate();
			ps.clearParameters();

			//3. Inserto los contratos
			ps = con.queryPrecompilado("INSERT INTO COM_CONTRATOS_DISTRIBUIDO (IC_CONSECUTIVO,CG_NUM_CONTRATO)\n" + 
			"SELECT ?,CG_NUM_CONTRATO FROM COMTMP_CONTRATOS_DISTRIBUIDO\n" + 
			"WHERE IC_CONSECUTIVO=?");
			ps.setLong(1, ic);
			ps.setLong(2, icConsecutivo);
			ps.executeUpdate();
			ps.clearParameters();

			//4. elimino de las tablas temporales
			ps = con.queryPrecompilado("DELETE FROM COMTMP_CONTRATOS_DISTRIBUIDO WHERE IC_CONSECUTIVO=?");
			ps.setLong(1, icConsecutivo);
			ps.executeUpdate();
			ps.clearParameters();
			ps = con.queryPrecompilado("DELETE FROM COMTMPREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO=?");
			ps.setLong(1, icConsecutivo);
			ps.executeUpdate();
			ps.clearParameters();

			//5. Obtengo el IC_BENEFICIARIO, lo necesito para su numero de nafin electronico.
			ps = con.queryPrecompilado("SELECT IC_BENEFICIARIO FROM COMREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO=?");
			ps.setLong(1, ic);
			rst = ps.executeQuery();
			if(rst.next()) {
				icBenef = rst.getLong("IC_BENEFICIARIO");
			}

			//6. Registro en bit�cora
			String noNafin = getNumNafinElectronico(con, icBenef, "I");
			Bitacora.grabarEnBitacora(con,"CUENTABANC","N",noNafin,usuario,"","ALTA DE CUENTA NO. "+ic);

			rst.close();
			ps.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("agregarCuenta(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("agregarCuenta(S) ::..");
		}
		return ic;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Modifica una cuenta y sus contratos.
	 * @param icConsecutivo
	 * @param numNePyme
	 * @param icEpo
	 * @param icIf
	 * @param icPyme
	 * @param benef
	 * @param moneda
	 * @param bancoServicio
	 * @param plaza
	 * @param sucursal
	 * @param cuenta
	 * @param cuentaClabe
	 * @param cuentaSpid
	 * @param swift
	 * @param aba
	 * @param usuario
	 * @param cgContratos
	 * @return
	 * @throws NafinException
	 */
	@Override
	public boolean modificarCuenta(Long icConsecutivo, String numNePyme, Long icEpo, Long icIf, Long icPyme, Long benef, Long moneda,
								Long bancoServicio, Long plaza, String sucursal, String cuenta, String cuentaClabe, String cuentaSpid,
								String swift, String aba, String usuario, String[] cgContratos) throws NafinException {

		log.info("modificarCuenta(E) ::..");
		boolean bOk = false;
		boolean ctaModificada = false;
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement ps = null;
		StringBuilder selectSQL = new StringBuilder();
		StringBuilder insertSQL = new StringBuilder();
		Long ic = 0L;
		//Variables para la bit�cora
		List datosAnteriores = new ArrayList();
		List datosActuales = new ArrayList();
		StringBuilder valoresAnteriores = new StringBuilder();
		StringBuilder valoresActuales = new StringBuilder();
		String[] nombres = {
			",N�m. NE Proveedor",
			",EPO",
			",Intermediario Financier",
			",Beneficiario",
			",Moneda",
			",Banco de Servicio",
			",Plaza",
			",Sucursal",
			",No. de Cuenta",
			",No. Cuenta CLABE",
			",No. cuenta Spid",
			",Swift",
			",ABA",
			",Contratos autorizados"
		};
		try {
			con.conexionDB();

			//La siguiente consulta es para saber si se modificaron los campos de la cuenta o no.
			selectSQL.append("SELECT COUNT(*) AS ID FROM COMREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO=? AND CG_SUCURSAL=? AND CG_NUMERO_CUENTA=? ");
			if(null!=bancoServicio && bancoServicio>0){
				selectSQL.append("AND IC_BANCOS_TEF=? ");
			}
			if(null!=plaza && plaza>0){
				selectSQL.append("AND IC_PLAZA=? ");
			} else {
				selectSQL.append("AND IC_PLAZA IS NULL ");
			}
			if(moneda==1){
				if(null!=cuentaClabe && !"".equals(cuentaClabe)){
					selectSQL.append("AND CG_CUENTA_CLABE=? ");
				}
			} else if(moneda==54){
				if(null!=cuentaSpid && !"".equals(cuentaSpid)){
					selectSQL.append("AND CG_CUENTA_SPID=? ");
				}
				if(null!=swift && !"".equals(swift)){
					selectSQL.append("AND CG_SWIFT=? ");
				} else {
					selectSQL.append("AND CG_SWIFT IS NULL ");
				}
				if(null!=aba && !"".equals(aba)){
					selectSQL.append("AND CG_ABA=? ");
				} else{
					selectSQL.append("AND CG_ABA IS NULL ");
				}
			}
			int cont=1;
			ps = con.queryPrecompilado(selectSQL.toString());
			ps.setLong(cont++, icConsecutivo);
			ps.setString(cont++, sucursal);
			ps.setString(cont++, cuenta);
			if(null!=bancoServicio && bancoServicio>0){
				ps.setLong(cont++, bancoServicio);
			}
			if(null!=plaza && plaza>0){
				ps.setLong(cont++, plaza);
			}
			if(moneda==1){
				if(null!=cuentaClabe && !"".equals(cuentaClabe)){
					ps.setString(cont++, cuentaClabe);
				}
			} else if(moneda==54){
				if(null!=cuentaSpid && !"".equals(cuentaSpid)){
					ps.setString(cont++, cuentaSpid);
				}
				if(null!=swift && !"".equals(swift)){
					ps.setString(cont++, swift);
				}
				if(null!=aba && !"".equals(aba)){
					ps.setString(cont++, aba);
				}
			}
			rst = ps.executeQuery();
			log.debug("..:: selectSQL: "+selectSQL);
			if(rst.next()) {
				ic = rst.getLong("ID");
			}
			if(null==ic || 0==ic){
				ctaModificada = true;
			}
			ps.clearParameters();

			//Obtengo los datos anteriores
			datosAnteriores = this.getFactorajeDistribuido(con, icConsecutivo);

			if(ctaModificada==true) {
				//Actualizo los registros
				cont=1;
				insertSQL.append("UPDATE COMREL_CTA_BAN_DISTRIBUIDO SET CG_AUTORIZA_IF='N', CG_CAMBIO_CUENTA='S', CC_ACUSE=NULL, DF_FECHA_MODIF=SYSDATE, IC_USUARIO_MODIF=?, CG_SUCURSAL=?, CG_NUMERO_CUENTA=?");
				if(null!=bancoServicio && bancoServicio>0){
					insertSQL.append(",IC_BANCOS_TEF=?");
				}
				if(null!=plaza && plaza>0){
					insertSQL.append(",IC_PLAZA=?");
				} else {
					insertSQL.append(",IC_PLAZA=null");
				}
				if(moneda==1){
					if(null!=cuentaClabe && !"".equals(cuentaClabe)){
						insertSQL.append(",CG_CUENTA_CLABE=?");
					}
					insertSQL.append(",CG_CUENTA_SPID=null");
					insertSQL.append(",CG_SWIFT=null");
					insertSQL.append(",CG_ABA=null");
				} else if(moneda==54){
					if(null!=cuentaSpid && !"".equals(cuentaSpid)){
						insertSQL.append(",CG_CUENTA_SPID=?");
					}
					if(null!=swift && !"".equals(swift)){
						insertSQL.append(",CG_SWIFT=?");
					} else {
						insertSQL.append(",CG_SWIFT=null");
					}
					if(null!=aba && !"".equals(aba)){
						insertSQL.append(",CG_ABA=?");
					} else {
						insertSQL.append(",CG_ABA=null");
					}
					insertSQL.append(",CG_CUENTA_CLABE=null");
				}
				insertSQL.append(" WHERE IC_CONSECUTIVO=?");
				ps = con.queryPrecompilado(insertSQL.toString());
				ps.setString(cont++, usuario);
				ps.setString(cont++, sucursal);
				ps.setString(cont++, cuenta);
				if(null!=bancoServicio && bancoServicio>0){
					ps.setLong(cont++, bancoServicio);
				}
				if(null!=plaza && plaza>0){
					ps.setLong(cont++, plaza);
				}
				if(moneda==1){
					if(null!=cuentaClabe && !"".equals(cuentaClabe)){
						ps.setString(cont++, cuentaClabe);
					}
				} else if(moneda==54){
					if(null!=cuentaSpid && !"".equals(cuentaSpid)){
						ps.setString(cont++, cuentaSpid);
					}
					if(null!=swift && !"".equals(swift)){
						ps.setString(cont++, swift);
					}
					if(null!=aba && !"".equals(aba)){
						ps.setString(cont++, aba);
					}
				}
				ps.setLong(cont++, icConsecutivo);
				log.debug("..:: insertSQL: "+insertSQL);
				ps.executeUpdate();
				ps.clearParameters();

			}

			//Elimino los contratos anteriores.
			ps = con.queryPrecompilado("DELETE FROM COM_CONTRATOS_DISTRIBUIDO WHERE IC_CONSECUTIVO=?");
			ps.setLong(1, icConsecutivo);
			ps.executeUpdate();
			ps.clearParameters();
			//Inserto los contratos nuevos.
			for(int i=0; i<cgContratos.length; i++){
				ps = con.queryPrecompilado("INSERT INTO COM_CONTRATOS_DISTRIBUIDO(IC_CONSECUTIVO,CG_NUM_CONTRATO) VALUES(?,?)");
				ps.setLong(1, icConsecutivo);
				ps.setString(2, cgContratos[i]);
				ps.executeUpdate();
				ps.clearParameters();
			}

			//Obtengo los datos posteriores a la modificaci�n para guardar los cambios en bit�cora
			datosActuales = this.getFactorajeDistribuido(con, icConsecutivo);
			//Inserto en bitacora
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));
			String icNafElec = getNumNafinElectronico(con, benef, "I");
			Bitacora.grabarEnBitacora(con,"CUENTABANC","N",icNafElec,usuario,"ACTUALIZACION CUENTA BENEFICIARIO:" + valoresAnteriores.toString(), "ACTUALIZACION CUENTA BENEFICIARIO:" + valoresActuales.toString());

			ps.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("modificarCuenta(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("modificarCuenta(S) ::..");
		}
		return bOk;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Elimina una cuenta y sus cantratos
	 * @param icConsecutivo
	 * @param icBenef
	 * @param usuario
	 * @return
	 * @throws NafinException
	 */
	@Override
	public boolean eliminarCuenta(Long icConsecutivo, Long icBenef, String usuario) throws NafinException {
		log.info("eliminarCuenta(E) ::..");
		boolean bOk = false;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		String deleteContrato="DELETE FROM COM_CONTRATOS_DISTRIBUIDO WHERE IC_CONSECUTIVO=?";
		String deleteCuenta="DELETE FROM COMREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO=?";
		//Variables para la bit�cora
		List datosAnteriores = new ArrayList();
		List datosActuales = new ArrayList();
		StringBuilder valoresAnteriores = new StringBuilder();
		StringBuilder valoresActuales = new StringBuilder();
		String[] nombres = {
			"N�m. NE Proveedor",
			",EPO",
			",Intermediario Financier",
			",Beneficiario",
			",Moneda",
			",Banco de Servicio",
			",Plaza",
			",Sucursal",
			",No. de Cuenta",
			",No. Cuenta CLABE",
			",No. cuenta Spid",
			",Swift",
			",ABA",
			",Contratos autorizados"
		};
		try {
			con.conexionDB();
			//Obtengo los datos anteriores
			datosAnteriores = this.getFactorajeDistribuido(con, icConsecutivo);
			//Elimino los contratos asociados a la cuenta
			ps = con.queryPrecompilado(deleteContrato);
			ps.setLong(1, icConsecutivo);
			ps.executeUpdate();
			//Elimino la cuenta
			ps1 = con.queryPrecompilado(deleteCuenta);
			ps1.setLong(1, icConsecutivo);
			ps1.executeUpdate();
			//Obtengo los datos posteriores a la modificaci�n para guardar los cambios en bit�cora
			datosActuales = this.getFactorajeDistribuido(con, icConsecutivo);
			//Inserto en bitacora
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));
			String numNePyme = getNumNafinElectronico(con, icBenef, "I");
			Bitacora.grabarEnBitacora(con,"CUENTABANC","N",numNePyme,usuario,valoresAnteriores.toString(), "ELIMINACION DE CUENTA NO. " + icConsecutivo);

			ps.close();
			ps1.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("eliminarCuenta(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("eliminarCuenta(S) ::..");
		}
		return bOk;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Antes de agregar una cuenta, valida que no est� duplicada la relaci�n
	 * PROVEEDOR-EPO-IF-MONEDA para el beneficiario.
	 * @param ids
	 * @param icEpo
	 * @param icPyme
	 * @param icIf
	 * @param icMoneda
	 * @param icBenef
	 * @param tipoTabla indica si busca en la tabla final o en la tabla temporal
	 * @return TRUE: Existen registros. FALSE: No existen registros.
	 * @throws NafinException
	 */
	@Override
	public boolean existeCuentaDuplicada(List<String> ids, Long icEpo, Long icPyme, Long icIf, Long icMoneda, Long icBenef, String tipoTabla) throws NafinException {
		log.info("existeCuentaDuplicada(E) ::..");
		boolean bOk = false;
		int numReg=0;
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		List varBind = new ArrayList();
		StringBuilder strSQL = new StringBuilder();
		try {

			if("TMP".equals(tipoTabla)) {
				strSQL.append("SELECT COUNT(*) AS NUM_REG FROM COMTMPREL_CTA_BAN_DISTRIBUIDO WHERE IC_EPO=? AND IC_PYME=? AND IC_IF=? AND IC_MONEDA=? AND IC_BENEFICIARIO=?");
				varBind.add(icEpo);
				varBind.add(icPyme);
				varBind.add(icIf);
				varBind.add(icMoneda);
				varBind.add(icBenef);
				if(null!=ids && !ids.isEmpty()){
					strSQL.append(" AND IC_CONSECUTIVO IN(");
					for(int i=0; i<ids.size(); i++) {
						if(i>0){
							strSQL.append(",");
						}
						strSQL.append("?");
						varBind.add(Long.parseLong(ids.get(i)));
					}
					strSQL.append(")");
				}
			} else {
				strSQL.append("SELECT COUNT(*) AS NUM_REG FROM COMREL_CTA_BAN_DISTRIBUIDO WHERE IC_EPO=? AND IC_PYME=? AND IC_IF=? AND IC_MONEDA=? AND IC_BENEFICIARIO=?");
				varBind.add(icEpo);
				varBind.add(icPyme);
				varBind.add(icIf);
				varBind.add(icMoneda);
				varBind.add(icBenef);
			}
			con.conexionDB();
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if(rst.next()) {
				numReg = rst.getInt("NUM_REG");
			}
			if(numReg>0) {
				bOk = true;
			} else {
				bOk = false;
			}
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("existeCuentaDuplicada(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("existeCuentaDuplicada(S) ::..");
		}
		return bOk;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: Factoraje Distribuido. Antes de agregar una cuenta valida que la relaci�n 
	 * PROVEEDOR-EPO-IF-MONEDA ya se encuentra parametrizada en la pantalla Administraci�n / Parametrizaci�n / Selecci�n IF.
	 * Nota: Esta consulta se obtuvo de la acci�n del bot�n seleccionar de la pantalla mencionada (15admNafinParam02Ext.js)
	 * @param icEpo
	 * @param icPyme
	 * @param icIf
	 * @param icMoneda
	 * @return TRUE: Existen registros. FALSE: No existen registros.
	 * @throws NafinException
	 */
	@Override
	public boolean existeCuentaParametrizada(Long icEpo, Long icPyme, Long icIf, Long icMoneda) throws NafinException {
		log.info("existeCuentaParametrizada(E) ::..");
		boolean bOk = false;
		int numReg=0;
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		List varBind = new ArrayList();
		String strSQL="SELECT COUNT(PI.IC_CUENTA_BANCARIA) AS NUM_REG " + 
			"FROM COMREL_PYME_IF PI, comrel_cuenta_bancaria CB " + 
			"WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA " + 
			"AND CB.IC_PYME = ? " + 
			"AND CB.IC_MONEDA = ? " + 
			"AND IC_IF = ? " + 
			"AND IC_EPO = ? " + 
			"AND CB.CS_BORRADO = 'N' " + 
			"AND PI.CS_BORRADO = 'N'";
		try {
			varBind.add(icPyme);
			varBind.add(icMoneda);
			varBind.add(icIf);
			varBind.add(icEpo);
			con.conexionDB();
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if(rst.next()) {
				numReg = rst.getInt("NUM_REG");
			}
			if(numReg>0) {
				bOk = true;
			} else {
				bOk = false;
			}
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("existeCuentaParametrizada(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("existeCuentaParametrizada(S) ::..");
		}
		return bOk;
	}

	/**
	 * Pantalla Cuentas Bancarias Pymes: M�todo para eliminar las cuentas que se agregaron temporalmente pero que al final se 
	 * desea cancelar la inserci�n final.
	 * @param ids Lista de ic_consecutivos
	 * @return
	 * @throws NafinException
	 */
	@Override
	public boolean eliminarCuentasTemporales(List<String> ids) throws NafinException {
		log.info("eliminarCuentasTemporales(E) ::..");
		boolean bOk = false;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		List varBind = new ArrayList();
		List varBind1 = new ArrayList();
		StringBuilder deleteContrato=new StringBuilder();
		StringBuilder deleteCuenta=new StringBuilder();
		try {
			con.conexionDB();
			//Elimino los contratos asociados a la cuenta
			deleteContrato.append("DELETE FROM COMTMP_CONTRATOS_DISTRIBUIDO WHERE IC_CONSECUTIVO IN(");
			for (int i = 0; i < ids.size(); i++) {
				if(i > 0){
					deleteContrato.append(",");
				}
				deleteContrato.append("?");
				varBind.add(Long.parseLong(ids.get(i)));
			}
			deleteContrato.append(")");
			ps = con.queryPrecompilado(deleteContrato.toString(), varBind);
			ps.executeUpdate();
			//Elimino la cuenta
			deleteCuenta.append("DELETE FROM COMTMPREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO IN(");
			for (int i = 0; i < ids.size(); i++) {
				if(i > 0){
					deleteCuenta.append(",");
				}
				deleteCuenta.append("?");
				varBind1.add(Long.parseLong(ids.get(i)));
			}
			deleteCuenta.append(")");
			ps1 = con.queryPrecompilado(deleteCuenta.toString(), varBind1);
			ps1.executeUpdate();
			ps.close();
			ps1.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("eliminarCuentasTemporales(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("eliminarCuentasTemporales(S) ::..");
		}
		return bOk;
	}

	/**
	 * M�todo para que el intermediario pueda autorizar al Beneficiario por cada PYME asociada.
	 * @param cuentasPorAutorizar
	 * @param acuse
	 * @param recibo
	 * @param usuario
	 * @param numNafinElec
	 * @return
	 * @throws NafinException
	 */
	@Override
	public String autorizarCuentasBeneficiarios(String[] cuentasPorAutorizar, String acuse, String recibo, String usuario, Long numNafinElec) throws NafinException{
		log.info("autorizarCuentasBeneficiarios(E) ::..");
		String fechaAutorizacion = "";
		boolean bOk = false;
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement ps = null;
		List varBind = new ArrayList();
		StringBuilder sql1=new StringBuilder();
		StringBuilder sql2=new StringBuilder();
		StringBuilder sql3=new StringBuilder();
		try {
			con.conexionDB();

			//Inserto el registro en COM_ACUSE_DISTRIBUIDO
			sql2.append("INSERT INTO COM_ACUSE_DISTRIBUIDO(CC_ACUSE,CC_RECIBO,CG_USUARIO) VALUES(?,?,?)");
			varBind.add(acuse);
			varBind.add(recibo);
			varBind.add(usuario);
			ps = con.queryPrecompilado(sql2.toString(), varBind);
			ps.executeUpdate();
			ps.clearParameters();
			varBind = new ArrayList();

			//Actualizo el campo que indica que se autoriza
			sql1.append("UPDATE COMREL_CTA_BAN_DISTRIBUIDO SET CG_AUTORIZA_IF='S', CC_ACUSE=? WHERE IC_CONSECUTIVO IN(");
			varBind.add(acuse);
			for (int i = 0; i < cuentasPorAutorizar.length; i++) {
				if(i > 0){
					sql1.append(",");
				}
				sql1.append("?");
				varBind.add(Long.parseLong(cuentasPorAutorizar[i]));
			}
			sql1.append(")");
			ps = con.queryPrecompilado(sql1.toString(), varBind);
			ps.executeUpdate();
			ps.clearParameters();
			varBind = new ArrayList();

			//Actualizo en bitacora
			sql3.append("SELECT A.IC_NAFIN_ELECTRONICO \n" + 
			",A.CG_SUCURSAL \n" + 
			",A.CG_NUMERO_CUENTA \n" + 
			",A.CG_CUENTA_CLABE \n" + 
			",A.CG_CUENTA_SPID \n" + 
			",A.CG_SWIFT \n" + 
			",A.CG_ABA \n" + 
			",A.IC_IF \n" + 
			",B.CG_RAZON_SOCIAL AS NOMBRE_EPO \n" + 
			",C.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO \n" + 
			",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO \n" + 
			",F.CD_NOMBRE AS NOMBRE_MONEDA \n" + 
			",G.CD_DESCRIPCION AS BANCO_SERVICIO \n" + 
			",H.CD_DESCRIPCION AS CG_PLAZA \n" + 
			",TO_CHAR(I.DF_AUTORIZA_IF,'dd/mm/yyyy hh24:mi:ss') AS FECHA_AUTORIZACION \n" + 
			"FROM COMREL_CTA_BAN_DISTRIBUIDO A \n" + 
			"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO \n" + 
			"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF \n" + 
			"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF \n" + 
			"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA \n" + 
			"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF \n" + 
			"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA \n" + 
			"LEFT JOIN COM_ACUSE_DISTRIBUIDO I ON A.CC_ACUSE=I.CC_ACUSE \n" + 
			"WHERE IC_CONSECUTIVO IN(");
			for (int i = 0; i < cuentasPorAutorizar.length; i++) {
				if(i > 0){
					sql3.append(",");
				}
				sql3.append("?");
				varBind.add(Long.parseLong(cuentasPorAutorizar[i]));
			}
			sql3.append(")");
			ps = con.queryPrecompilado(sql3.toString(), varBind);
			String ne       ="";
			String epo      ="";
			String intFin   ="";
			String benef    ="";
			String moneda   ="";
			String bancServ ="";
			String plaza    ="";
			String sucursal ="";
			String cuenta   ="";
			String clabe    ="";
			String spid     ="";
			String swift    ="";
			String aba      ="";

			rst = ps.executeQuery();
			while(rst.next()) {
				ne                = rst.getString("IC_NAFIN_ELECTRONICO")     == null?"":rst.getString("IC_NAFIN_ELECTRONICO");
				epo               = rst.getString("NOMBRE_EPO")               == null?"":rst.getString("NOMBRE_EPO");
				intFin            = rst.getString("INTERMEDIARIO_FINANCIERO") == null?"":rst.getString("INTERMEDIARIO_FINANCIERO");
				benef             = rst.getString("NOMBRE_BENEFICIARIO")      == null?"":rst.getString("NOMBRE_BENEFICIARIO");
				moneda            = rst.getString("NOMBRE_MONEDA")            == null?"":rst.getString("NOMBRE_MONEDA");
				bancServ          = rst.getString("BANCO_SERVICIO")           == null?"":rst.getString("BANCO_SERVICIO");
				plaza             = rst.getString("CG_PLAZA")                 == null?"":rst.getString("CG_PLAZA");
				sucursal          = rst.getString("CG_SUCURSAL")              == null?"":rst.getString("CG_SUCURSAL");
				cuenta            = rst.getString("CG_NUMERO_CUENTA")         == null?"":rst.getString("CG_NUMERO_CUENTA");
				clabe             = rst.getString("CG_CUENTA_CLABE")          == null?"":rst.getString("CG_CUENTA_CLABE");
				spid              = rst.getString("CG_CUENTA_SPID")           == null?"":rst.getString("CG_CUENTA_SPID");
				swift             = rst.getString("CG_SWIFT")                 == null?"":rst.getString("CG_SWIFT");
				aba               = rst.getString("CG_ABA")                   == null?"":rst.getString("CG_ABA");
				fechaAutorizacion = rst.getString("FECHA_AUTORIZACION")       == null?"":rst.getString("FECHA_AUTORIZACION");
				//Armo la cadena de datos actuales
				StringBuilder valoresActuales = new StringBuilder();
				valoresActuales.append("AUTORIZACION CUENTA BENEFICIARIO"       +
										",N�m. NE Proveedor="        + ne       +
										",EPO="                      + epo      +
										",Intermediario Financiero=" + intFin   +
										",Beneficiario="             + benef    +
										",Moneda="                   + moneda   +
										",Banco de Servicio="        + bancServ +
										",Plaza="                    + plaza    +
										",Sucursal="                 + sucursal +
										",No. de Cuenta="            + cuenta   +
										",Cuenta CLABE="             + clabe    +
										",No. Cuenta SPID="          + spid     +
										",SWIFT="                    + swift    +
										",ABA="                      + aba);
				//Mando ainsertar en bitacora
				Bitacora.grabarEnBitacora(con,"AUTORIZABENEF","I",""+numNafinElec,usuario,"", valoresActuales.toString());
			}
			

			rst.close();
			ps.close();
			bOk = true;
		} catch(Exception e) {
			bOk = false;
			log.error("autorizarCuentasBeneficiarios(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("autorizarCuentasBeneficiarios(S) ::..");
		}
		return fechaAutorizacion;
	}

	/**
	 * Obtiene las cuentas autorizadas por el IF para llenar el grid del acuse
	 * @param acuse
	 * @return
	 * @throws AppException
	 */
	@Override
	public String obtenerJSONCuentasAutorizadas(String acuse) throws AppException {
		log.info("obtenerJSONCuentasAutorizadas(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuilder strSQL = new StringBuilder();
		StringBuilder cadenaJSONparametros = new StringBuilder();
		HashMap parametro = new HashMap();
		List varBind = new ArrayList();
		JSONArray jsonArr = new JSONArray();
		try {
			con.conexionDB();
			strSQL.append("SELECT A.IC_CONSECUTIVO\n"
			+",A.IC_IF\n"
			+",A.IC_EPO\n"
			+",A.IC_PYME\n"
			+",A.IC_BENEFICIARIO\n"
			+",A.IC_MONEDA\n"
			+",A.IC_NAFIN_ELECTRONICO\n"
			+",A.IC_BANCOS_TEF\n"
			+",A.CG_SUCURSAL\n"
			+",A.CG_NUMERO_CUENTA\n"
			+",A.CG_CUENTA_CLABE\n"
			+",A.CG_CUENTA_SPID\n"
			+",A.CG_SWIFT\n"
			+",A.CG_ABA\n"
			+",CASE WHEN A.CG_AUTORIZA_IF='S' THEN 'SI' ELSE 'NO' END AS CG_AUTORIZA_IF\n"
			+",CASE WHEN A.CG_CAMBIO_CUENTA='S' THEN 'SI' ELSE 'NO' END AS CG_CAMBIO_CUENTA\n"
			+",CASE WHEN A.CG_CAMBIO_CUENTA='N' OR A.CG_AUTORIZA_IF='N' THEN 'ROJO' ELSE '' END AS COLOR\n"
			+",B.CG_RAZON_SOCIAL AS NOMBRE_EPO\n"
			+",E.CG_RAZON_SOCIAL AS NOMBRE_PYME\n"
			+",C.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO\n"
			+",G.CD_DESCRIPCION AS BANCO_SERVICIO\n"
			+",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO\n"
			+",F.CD_NOMBRE AS NOMBRE_MONEDA\n"
			+",H.CD_DESCRIPCION AS CG_PLAZA\n"
			+"FROM COMREL_CTA_BAN_DISTRIBUIDO A\n"
			+"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO\n"
			+"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n"
			+"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF\n"
			+"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME\n"
			+"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA\n"
			+"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF\n"
			+"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA\n"
			+"WHERE A.CC_ACUSE =? \n"
			+"ORDER BY D.CG_RAZON_SOCIAL, B.CG_RAZON_SOCIAL, E.CG_RAZON_SOCIAL");
			varBind.add(acuse);
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			while(rst.next()) {
				parametro = new HashMap();
				parametro.put("NOMBRE_EPO",           rst.getString("NOMBRE_EPO"));
				parametro.put("NOMBRE_PYME",          rst.getString("NOMBRE_PYME"));
				parametro.put("IC_NAFIN_ELECTRONICO", rst.getString("IC_NAFIN_ELECTRONICO"));
				parametro.put("BANCO_SERVICIO",       rst.getString("BANCO_SERVICIO"));
				parametro.put("NOMBRE_BENEFICIARIO",  rst.getString("NOMBRE_BENEFICIARIO"));
				parametro.put("NOMBRE_MONEDA",        rst.getString("NOMBRE_MONEDA"));
				parametro.put("CG_NUMERO_CUENTA",     rst.getString("CG_NUMERO_CUENTA"));
				parametro.put("CG_CUENTA_CLABE",      rst.getString("CG_CUENTA_CLABE"));
				parametro.put("CG_CUENTA_SPID",       rst.getString("CG_CUENTA_SPID"));
				parametro.put("CG_SWIFT",             rst.getString("CG_SWIFT"));
				parametro.put("CG_ABA",               rst.getString("CG_ABA"));
				parametro.put("CG_PLAZA",             rst.getString("CG_PLAZA"));
				parametro.put("IC_CONSECUTIVO",       rst.getString("IC_CONSECUTIVO"));
				parametro.put("CG_CAMBIO_CUENTA",     rst.getString("CG_CAMBIO_CUENTA"));
				jsonArr.add(JSONObject.fromObject(parametro));
			}
			rst.close();
			pst.close();
			cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + "})");
		} catch(Exception e) {
			log.error("obtenerJSONCuentasAutorizadas(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerJSONCuentasAutorizadas(S) ::..");
		}

		return cadenaJSONparametros.toString();
	}

	/**
	 * Pantalla Administraci�n / Parametrizaci�n / Cuentas Bancarias Pymes / Autorizar Cuentas Beneficiarios
	 * Obtiene las cuentas con cambio de cuenta y pendientes de autorizar.
	 * @param numNafinElec
	 * @return
	 * @throws AppException
	 */
	@Override
	public String obtenerJSONCambioCuentasPorAutorizar(Long numNafinElec) throws AppException {
		log.info("obtenerJSONCambioCuentasPorAutorizar(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuilder strSQL = new StringBuilder();
		StringBuilder cadenaJSONparametros = new StringBuilder();
		HashMap parametro = new HashMap();
		List varBind = new ArrayList();
		JSONArray jsonArr = new JSONArray();
		try {
			con.conexionDB();
			strSQL.append("SELECT A.IC_CONSECUTIVO" +
			",A.IC_NAFIN_ELECTRONICO\n" + 
			",A.IC_EPO\n" + 
			",A.IC_BENEFICIARIO\n" + 
			",A.CG_CAMBIO_CUENTA\n" + 
			",E.CG_RFC\n" + 
			",E.CG_RAZON_SOCIAL AS NOMBRE_PYME\n" + 
			",B.CG_RAZON_SOCIAL AS NOMBRE_EPO\n" + 
			",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO\n" + 
			",F.CD_NOMBRE AS NOMBRE_MONEDA\n" + 
			"FROM COMREL_CTA_BAN_DISTRIBUIDO A\n" + 
			"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO\n" + 
			"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF\n" + 
			"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME\n" + 
			"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA\n" + 
			"WHERE A.CG_AUTORIZA_IF='N'\n" + 
			"AND A.CG_CAMBIO_CUENTA='S'\n" + 
			"AND A.IC_IF=(SELECT IC_EPO_PYME_IF FROM COMREL_NAFIN where CG_TIPO='I' and IC_NAFIN_ELECTRONICO=?)");
			varBind.add(numNafinElec);
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			while(rst.next()) {
				parametro = new HashMap();
				parametro.put("IC_CONSECUTIVO",       rst.getString("IC_CONSECUTIVO")      );
				parametro.put("IC_NAFIN_ELECTRONICO", rst.getString("IC_NAFIN_ELECTRONICO"));
				parametro.put("IC_EPO",               rst.getString("IC_EPO")              );
				parametro.put("IC_BENEFICIARIO",      rst.getString("IC_BENEFICIARIO")     );
				parametro.put("CG_CAMBIO_CUENTA",     rst.getString("CG_CAMBIO_CUENTA")    );
				parametro.put("CG_RFC",               rst.getString("CG_RFC")              );
				parametro.put("NOMBRE_PYME",          rst.getString("NOMBRE_PYME")         );
				parametro.put("NOMBRE_EPO",           rst.getString("NOMBRE_EPO")          );
				parametro.put("NOMBRE_BENEFICIARIO",  rst.getString("NOMBRE_BENEFICIARIO") );
				parametro.put("NOMBRE_MONEDA",        rst.getString("NOMBRE_MONEDA")       );
				jsonArr.add(JSONObject.fromObject(parametro));
			}
			rst.close();
			pst.close();
			cadenaJSONparametros.append("({\"success\": true, \"total\": " + jsonArr.size() + ", \"registros\": " + jsonArr.toString() + "})");
		} catch(Exception e) {
			log.error("obtenerJSONCambioCuentasPorAutorizar(Error) ::..");
			throw new AppException("Ocurri� un errror durante la ejecuci�n del m�todo:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerJSONCambioCuentasPorAutorizar(S) ::..");
		}

		return cadenaJSONparametros.toString();
	}

	/**
	 * M�todo para obtener los datos anteriores y posteriores a una modificacion para registrar cambios en bit�cora.
	 * @param con
	 * @param icConsecutivo
	 * @return
	 * @throws SQLException
	 * @throws NamingException
	 */
	private List getFactorajeDistribuido(AccesoDB con, Long icConsecutivo) throws SQLException, NamingException {
		ResultSet rs = null;
		StringBuilder strSQL1 = new StringBuilder();
		StringBuilder strSQL2 = new StringBuilder();
		StringBuilder contratos = new StringBuilder();
		List registro = new ArrayList();
		log.info("getFactorajeDistribuido(E)");
		String icNafinElectronico="";
		String icEpo             ="";
		String icIf              ="";
		String icBeneficiario    ="";
		String icMoneda          ="";
		String icBancoServicio   ="";
		String icPlaza           ="";
		String sucursal          ="";
		String noCuenta          ="";
		String noCuentaClabe     ="";
		String noCuentaSpid      ="";
		String swift             ="";
		String aba               ="";
		
		try {
			strSQL1.append("SELECT " +
				"IC_NAFIN_ELECTRONICO" +
				",IC_EPO" +
				",IC_IF" +
				",IC_BENEFICIARIO" +
				",IC_MONEDA" +
				",IC_BANCOS_TEF" +
				",IC_PLAZA" +
				",CG_SUCURSAL" +
				",CG_NUMERO_CUENTA" +
				",CG_CUENTA_CLABE" +
				",CG_CUENTA_SPID" +
				",CG_SWIFT" +
				",CG_ABA  " +
				"FROM COMREL_CTA_BAN_DISTRIBUIDO WHERE IC_CONSECUTIVO="+icConsecutivo);
			rs = con.queryDB(strSQL1.toString());
			if (rs.next()) {
				icNafinElectronico = rs.getString("IC_NAFIN_ELECTRONICO") ==null?"":rs.getString("IC_NAFIN_ELECTRONICO");
				icEpo              = rs.getString("IC_EPO")               ==null?"":rs.getString("IC_EPO");
				icIf               = rs.getString("IC_IF")                ==null?"":rs.getString("IC_IF");
				icBeneficiario     = rs.getString("IC_BENEFICIARIO")      ==null?"":rs.getString("IC_BENEFICIARIO");
				icMoneda           = rs.getString("IC_MONEDA")            ==null?"":rs.getString("IC_MONEDA");
				icBancoServicio    = rs.getString("IC_BANCOS_TEF")        ==null?"":rs.getString("IC_BANCOS_TEF");
				icPlaza            = rs.getString("IC_PLAZA")             ==null?"":rs.getString("IC_PLAZA");
				sucursal           = rs.getString("CG_SUCURSAL")          ==null?"":rs.getString("CG_SUCURSAL");
				noCuenta           = rs.getString("CG_NUMERO_CUENTA")     ==null?"":rs.getString("CG_NUMERO_CUENTA");
				noCuentaClabe      = rs.getString("CG_CUENTA_CLABE")      ==null?"":rs.getString("CG_CUENTA_CLABE");
				noCuentaSpid       = rs.getString("CG_CUENTA_SPID")       ==null?"":rs.getString("CG_CUENTA_SPID");
				swift              = rs.getString("CG_SWIFT")             ==null?"":rs.getString("CG_SWIFT");
				aba                = rs.getString("CG_ABA")               ==null?"":rs.getString("CG_ABA");
			}
			strSQL2.append("SELECT CG_NUM_CONTRATO FROM COM_CONTRATOS_DISTRIBUIDO WHERE IC_CONSECUTIVO="+icConsecutivo);
			rs = con.queryDB(strSQL2.toString());
			int cont=0;
			while(rs.next()) {
				if(cont>0){
					contratos.append(";");
				}
				contratos.append(rs.getString("CG_NUM_CONTRATO"));
			}
			registro.add(0, icNafinElectronico  );
			registro.add(1, icEpo               );
			registro.add(2, icIf                );
			registro.add(3, icBeneficiario      );
			registro.add(4, icMoneda            );
			registro.add(5, icBancoServicio     );
			registro.add(6, icPlaza             );
			registro.add(7, sucursal            );
			registro.add(8, noCuenta            );
			registro.add(9, noCuentaClabe       );
			registro.add(10,noCuentaSpid        );
			registro.add(11,swift               );
			registro.add(12,aba                 );
			registro.add(13,contratos.toString());
			rs.close();
			con.cierraStatement();
			return registro;
		} finally {
			log.info("getFactorajeDistribuido(S)");
		}
	}


	/**
	 * Obtiene el numero de nafin electronico
	 * @param con
	 * @param ic
	 * @param tipo
	 * @return
	 * @throws SQLException
	 * @throws NamingException
	 */
	private String getNumNafinElectronico(AccesoDB con, Long ic, String tipo) throws SQLException, NamingException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuilder strSQL = new StringBuilder();
		log.info("getNumNafinElectronico(E)");
		String icNafinElectronico="";
		
		try {
			strSQL.append("SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE CG_TIPO=? AND IC_EPO_PYME_IF=?");
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, tipo);
			ps.setLong(2, ic);
			rs = ps.executeQuery();
			if (rs.next()) {
				icNafinElectronico = rs.getString("IC_NAFIN_ELECTRONICO") ==null?"":rs.getString("IC_NAFIN_ELECTRONICO");
			}
			rs.close();
			ps.close();
			con.cierraStatement();
			return icNafinElectronico;
		} finally {
			log.info("getNumNafinElectronico(S)");
		}
	}

}// Fin de la Clase