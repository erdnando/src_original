package com.netro.descuento;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse3;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/**
 * Maneja la informaci�n referente a pagos a Intermediarios Financieros
 * No Bancarios
 * @author Gilberto Aparicio (GEAG)
 */
@Stateless(name = "PagosIFNBEJB" , mappedName = "PagosIFNBEJB")
@TransactionManagement(TransactionManagementType.BEAN)
 public class PagosIFNBBean implements PagosIFNB {

/********************** Metodos publicos ************************/

	/**
	 * Realiza la carga masiva de pagos realizados a IFNB.
	 * En una linea se encuentra el encabezado (Se distingue por tener una E en el primer campo)
	 * y en las siguientes se encuentran los detalles (Se distingue por la letra D en el
	 * primer campo) que corresponden a ese encabezado.
	 * Si la primera linea no comienza con E que indica un encabezado, el proceso de las lineas
	 * subsecuente se cancela. en cualquier otro caso, aunque existan errores sigue revisando
	 * registro por registro, almacenando los bloques que estan correctos. Genera un listado
	 * de las lineas con errores y tambi�n de las exitosas.
	 *
	 * @param strDirectorio	Ruta del directorio donde se encuentra el archivo a procesar
	 * @param strNombrearchivo	Nombre del archivo a procesar
	 * @return Lista con los siguientes valores:
	 * 		0 - Mensajes de error (String)
	 * 		1 - Lineas con errores (String)
	 * 		2 - Datos de los encabezados de los bloques sin error (String)
	 * 		3 - Numero de proceso (0 si no hubo ningun pago valido o no se proceso el archivo) (Integer)
	 */
	private String strTipoBanco = "NB";
	private final static Log log = ServiceLocator.getInstance().getLog(PagosIFNBBean.class);

	public List procesarArchivo(String strDirectorio, String strNombreArchivo, String TipoBanco)
			throws NafinException {

		this.strTipoBanco = TipoBanco;

		String ruta = strDirectorio + strNombreArchivo;
		List resultado = new ArrayList();

		boolean exito = true;
		ResultSet rs = null;
		String strSQL = null;
		AccesoDB con = new AccesoDB();

		StringBuffer lineasConErrores = new StringBuffer();
		StringBuffer mensajesDeError = new StringBuffer();
		StringBuffer mensajesDeExito = new StringBuffer();
		StringBuffer lineasDelBloque = null;
		int numProceso = 0;
		int cont = 0;
	   BufferedReader br = null;

		try {
			con.conexionDB();

			strSQL = "SELECT count(*) FROM com_encabezado_pago " +
					" WHERE cg_nombre_archivo = '" + strNombreArchivo + "'";
			rs = con.queryDB(strSQL);
			rs.next();
			boolean archivoPreviamenteProcesado = (rs.getInt(1)==0)?false:true;
			rs.close();
			con.cierraStatement();

			if (archivoPreviamenteProcesado) {
				mensajesDeError.append("Ya existen datos para el archivo que intenta enviar");
			} else { //Se procesa el archivo
				java.io.File f = new java.io.File(ruta);
				br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));

				String linea = br.readLine();

				int numLinea = 1;

				Encabezado:
				while(linea != null) {
					List arrDetalles = new ArrayList();
					boolean bloqueCorrecto = true;
					//System.out.println("E linea: "+linea);
					VectorTokenizer vt = new VectorTokenizer(linea,",");
					Vector vecdat = vt.getValuesVector();

					int numCampos = vecdat.size();
					if(numCampos == 0) {	//Linea Vacia -> Linea ignorada
						continue;
					}

					String tipoRegistro = vecdat.get(0).toString().trim();	//E Encabezado D Detalle

					String mensajeLineaError = 	"\nError en la linea " + numLinea + ": ";
			//
					//Variables para datos de Encabezado
					String strClaveIF = null;
					String strClaveDirEstatal = null;
					String strClaveMoneda = null;
					String strBancoServicio = null;
					String strFechaVencimiento = null;
					String strFechaProbablePago = null;
					String strFechaDeposito = null;
					String strImporteDeposito = null;
					String strReferenciaBanco = null;
					String strReferenciaInter	= null;

					EncabezadoPagoIFNB encabezado = null;

					boolean encabezadoCorrecto = true;
//System.out.println("ANTES");
					if (tipoRegistro.equals("E")) { //Encabezado
//System.out.println("E");
						lineasDelBloque = new StringBuffer();
						lineasDelBloque.append(linea + "\n");	//Agrega el encabezado al bloque
						int numeroCampos = 10;
						if("NB".equals(strTipoBanco))
							numeroCampos++;

//System.out.println("numCampos:"+numCampos);
//System.out.println("numeroCampos:"+numeroCampos);
						if (numCampos < numeroCampos) {
							encabezadoCorrecto = false;
							mensajesDeError.append(mensajeLineaError +
									"El registro no contiene el numero de campos requeridos para el Encabezado");
						} else {
//System.out.println("CAMPOS OK");
//System.out.println("E vecdat: "+vecdat);
							strClaveIF = Comunes.quitaComitasSimples(vecdat.get(1).toString()).trim();
							strClaveDirEstatal = Comunes.quitaComitasSimples(vecdat.get(2).toString()).trim();
							strClaveMoneda = Comunes.quitaComitasSimples(vecdat.get(3).toString()).trim();
							strBancoServicio = Comunes.quitaComitasSimples(vecdat.get(4).toString()).trim();
							cont = 4;
							if("NB".equals(strTipoBanco)) {
								cont++;
								strFechaVencimiento = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();
							}
							cont++;
							strFechaProbablePago = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();
							cont++;
							strFechaDeposito = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();
							cont++;
							strImporteDeposito = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();
							cont++;
							strReferenciaBanco = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();
							cont++;
							strReferenciaInter = Comunes.quitaComitasSimples(vecdat.get(cont).toString()).trim();

							System.out.println("LA REFERENCIA BANCO = "+strReferenciaBanco);
//System.out.println("strFechaProbablePago = "+strFechaProbablePago);

							Hashtable htResultado = validarEncabezado(mensajesDeError, numLinea,
									strClaveIF, strClaveDirEstatal,
									strClaveMoneda, strBancoServicio,
									strFechaVencimiento, strFechaProbablePago,
									strFechaDeposito, strImporteDeposito,
									strReferenciaBanco,strReferenciaInter,con);
							encabezadoCorrecto = new Boolean(htResultado.get("registroValido").toString()).booleanValue();
							strFechaProbablePago = htResultado.get("strFechaProbablePago").toString();
							System.out.println("strFechaProbablePago = "+strFechaProbablePago);

						}

						if (!encabezadoCorrecto) {
							bloqueCorrecto = false;
						} else {
//System.out.println("E OK");

							strSQL = "SELECT ic_referencia_inter FROM COMCAT_referencia_inter "+
									" WHERE cg_descripcion = '" + strReferenciaInter + "'";
							rs = con.queryDB(strSQL);
							String ic_referencia_inter = "";
							if(rs.next()){
								ic_referencia_inter = rs.getString("IC_REFERENCIA_INTER");
							}
							rs.close();
							con.cierraStatement();
							encabezado = new EncabezadoPagoIFNB();
							encabezado.setNumeroDeLinea(numLinea);
							encabezado.setNombreArchivo(strNombreArchivo);
							encabezado.setClaveIF(strClaveIF);
							encabezado.setClaveDirEstatal(strClaveDirEstatal);
							encabezado.setClaveMoneda(strClaveMoneda);
							encabezado.setBancoServicio(strBancoServicio);
							if("NB".equals(strTipoBanco))
								encabezado.setFechaVencimiento(strFechaVencimiento);
							encabezado.setFechaProbablePago(strFechaProbablePago);
							encabezado.setFechaDeposito(strFechaDeposito);
							encabezado.setImporteDeposito(strImporteDeposito);
							encabezado.setReferenciaBanco(strReferenciaBanco);
							encabezado.setReferenciaInter(ic_referencia_inter);
						}

						boolean detalleCorrecto = true;
						BigDecimal bdMontoTotalDetalles = new BigDecimal(0);
						StringBuffer observacionesDetalles = new StringBuffer();
						int numDetalles = 0;

						Detalle:
						while((linea = br.readLine()) != null) { //Detalles
							numLinea++;
//System.out.println("D linea"+numLinea+": "+linea);
							VectorTokenizer vtDetalle = new VectorTokenizer(linea,",");
							Vector vecdatDetalle = vtDetalle.getValuesVector();

							numCampos = vecdatDetalle.size();
							if(numCampos == 0) {	//Linea Vacia -> Linea ignorada
								continue Detalle;
							}

							tipoRegistro = vecdatDetalle.get(0).toString().trim();	//E Encabezado D Detalle

							mensajeLineaError = 	"\nError en la linea " + numLinea + ": ";

							//Variables para datos de Detalle
							String strSucursal = null;				//NB
							String strSubaplicacion = null;
							String strPrestamo = null;
							String strAcreditado = null;			//NB
							String strFechaOperacion = null;		//NB
							String strFechaVencimientoD = null;		//NB
							String strFechaPago = null;				//NB
							String strSaldoInsoluto = null;			//NB
							String strTasa = null;					//NB
							String strClaveSiracCliente = null;
							String strCapital = null;
							String strDias = null;					//NB
							String strIntereses = null;
							String strMoratorios = null;
							String strSubsidio = null;
							String strComision = null;
							String strIVA = null;
							String strImportePago = null;
							String strConceptoPago = null;
							String strOrigenPago = null;


							if (tipoRegistro.equals("D")) { //Detalle
//System.out.println("D");
								lineasDelBloque.append(linea + "\n");	//Agrega el detalle al bloque
								if("NB".equals(strTipoBanco))
									numDetalles = 13;
								else if("B".equals(strTipoBanco))
									numDetalles = 15;
								if (numCampos < numDetalles) {
									mensajesDeError.append(mensajeLineaError +
											"El registro no contiene el numero de campos requeridos para un Detalle");
									detalleCorrecto = false;
								} else {
//System.out.println("CAMPOS D OK");
//System.out.println("D vecdatDetalle: "+vecdatDetalle);
									if("NB".equals(strTipoBanco)) {
										strSubaplicacion = Comunes.quitaComitasSimples(vecdatDetalle.get(1).toString()).trim();
										strPrestamo = Comunes.quitaComitasSimples(vecdatDetalle.get(2).toString()).trim();
										strClaveSiracCliente = Comunes.quitaComitasSimples(vecdatDetalle.get(3).toString()).trim();
										strCapital = Comunes.quitaComitasSimples(vecdatDetalle.get(4).toString()).trim();
										strIntereses = Comunes.quitaComitasSimples(vecdatDetalle.get(5).toString()).trim();
										strMoratorios = Comunes.quitaComitasSimples(vecdatDetalle.get(6).toString()).trim();
										strSubsidio = Comunes.quitaComitasSimples(vecdatDetalle.get(7).toString()).trim();
										strComision = Comunes.quitaComitasSimples(vecdatDetalle.get(8).toString()).trim();
										strIVA = Comunes.quitaComitasSimples(vecdatDetalle.get(9).toString()).trim();
										strImportePago = Comunes.quitaComitasSimples(vecdatDetalle.get(10).toString()).trim();
										strConceptoPago = Comunes.quitaComitasSimples(vecdatDetalle.get(11).toString()).trim().toUpperCase();
										strOrigenPago = Comunes.quitaComitasSimples(vecdatDetalle.get(12).toString()).trim().toUpperCase();
										detalleCorrecto = validarDetalle(mensajesDeError, numLinea,
												strSubaplicacion, strPrestamo,
												strClaveSiracCliente, strCapital,
												strIntereses, strMoratorios,
												strSubsidio, strComision,
												strIVA, strImportePago,
												strConceptoPago, strOrigenPago);

									} else if("B".equals(strTipoBanco)) {
										strSucursal			= Comunes.quitaComitasSimples(vecdatDetalle.get( 1).toString()).trim();
										strSubaplicacion 	= Comunes.quitaComitasSimples(vecdatDetalle.get( 2).toString()).trim();
										strPrestamo 		= Comunes.quitaComitasSimples(vecdatDetalle.get( 3).toString()).trim();
										strAcreditado 		= Comunes.quitaComitasSimples(vecdatDetalle.get( 4).toString()).trim();
										strFechaOperacion 	= Comunes.quitaComitasSimples(vecdatDetalle.get( 5).toString()).trim();
										strFechaVencimientoD= Comunes.quitaComitasSimples(vecdatDetalle.get( 6).toString()).trim();
										strFechaPago 		= Comunes.quitaComitasSimples(vecdatDetalle.get( 7).toString()).trim();
										strSaldoInsoluto 	= Comunes.quitaComitasSimples(vecdatDetalle.get( 8).toString()).trim();
										strTasa 			= Comunes.quitaComitasSimples(vecdatDetalle.get( 9).toString()).trim();
										strCapital 			= Comunes.quitaComitasSimples(vecdatDetalle.get(10).toString()).trim();
										strDias 			= Comunes.quitaComitasSimples(vecdatDetalle.get(11).toString()).trim();
										strIntereses 		= Comunes.quitaComitasSimples(vecdatDetalle.get(12).toString()).trim();
										strComision 		= Comunes.quitaComitasSimples(vecdatDetalle.get(13).toString()).trim();
										strIVA 				= Comunes.quitaComitasSimples(vecdatDetalle.get(14).toString()).trim();
										strImportePago 		= Comunes.quitaComitasSimples(vecdatDetalle.get(15).toString()).trim();

										detalleCorrecto = validarDetalleB(mensajesDeError, numLinea,
												strSucursal, strSubaplicacion, strPrestamo,
												strAcreditado, strFechaOperacion ,strFechaVencimientoD,
												strFechaPago, strSaldoInsoluto, strTasa,
												strCapital, strDias, strIntereses,
												strComision, strIVA, strImportePago);
									}
								}

								//Si el detalle es incorrecto � si es correcto pero el registro ya existe el bloque es erroneo
//System.out.println("strFechaProbablePago = "+strFechaProbablePago);
								if (!detalleCorrecto) {
									bloqueCorrecto = false;
								} /*else if (detalleCorrecto &&
										existeDetalle(strPrestamo,
												strFechaVencimiento, strFechaProbablePago)) {
									bloqueCorrecto = false;
									mensajesDeError.append(mensajeLineaError +
											"Ya existen datos del prestamo, para las fechas " +
											"indicadas de vencimiento y pago");
								}*/


								//Solo si el bloque es correcto se guardan los detalles para
								//posteriormente insertarlos.
								if (bloqueCorrecto ) {
									bdMontoTotalDetalles = bdMontoTotalDetalles.add(new BigDecimal(strImportePago));

									DetallePagoIFNB detalle = new DetallePagoIFNB();
									if("NB".equals(strTipoBanco)) {
										detalle.setSubaplicacion(strSubaplicacion);
										detalle.setPrestamo(strPrestamo);
										detalle.setClaveSiracCliente(strClaveSiracCliente);
										detalle.setCapital(strCapital);
										detalle.setIntereses(strIntereses);
										detalle.setMoratorios(strMoratorios);
										detalle.setSubsidio(strSubsidio);
										detalle.setComision(strComision);
										detalle.setIVA(strIVA);
										detalle.setImportePago(strImportePago);
										detalle.setConceptoPago(strConceptoPago);
										detalle.setOrigenPago(strOrigenPago);
									}  else if("B".equals(strTipoBanco)) {
										detalle.setSucursal(strSucursal);
										detalle.setSubaplicacion(strSubaplicacion);
										detalle.setPrestamo(strPrestamo);
										detalle.setAcreditado(strAcreditado);
										detalle.setFechaOperacion(strFechaOperacion);
										detalle.setFechaVencimiento(strFechaVencimientoD);
										detalle.setFechaPago(strFechaPago);
										detalle.setSaldoInsoluto(strSaldoInsoluto);
										detalle.setTasa(strTasa);
										detalle.setCapital(strCapital);
										detalle.setDias(strDias);
										detalle.setIntereses(strIntereses);
										detalle.setComision(strComision);
										detalle.setIVA(strIVA);
										detalle.setImportePago(strImportePago);
									}

									arrDetalles.add(detalle);


									if (Double.parseDouble(strImportePago) <
											Double.parseDouble(strIntereses)) {
										observacionesDetalles.append(
												"\nEl monto a pagar del prestamo" + strPrestamo +
												" cubrira parcialmente solo intereses," +
												" no se aplicara a capital");
									}

								} else {
									//si se detecta un error en el bloque se borra todos los detalles
									//dado que no ser�n necesarios por no realizarse la inserci�n
									arrDetalles.clear();
								}
							} else if (tipoRegistro.equals("E")) {	//Tipo de registro = E ->  Termina el bloque anterior y comienza otro
								break;
							} else { //No est� especificado el tipo de registro o es diferente de E,D
								detalleCorrecto = false;
								lineasDelBloque.append(linea + "\n");	//Agrega el registro al bloque
								mensajesDeError.append(mensajeLineaError +
										"El registro no especifica un tipo valido");
							}
						} //fin del while del detalle

						if (bloqueCorrecto) { //Si hasta el momento el bloque va bien... se realiza las sig. validaciones
							if (numDetalles == 0) {	//Si no hay detalles el bloque es erroneo
								bloqueCorrecto = false;
								mensajesDeError.append("\nError en la linea " +
										encabezado.getNumeroDeLinea() + ": " +
										"El Encabezado debe ser seguido de al menos un detalle");
							}

							//if (bdMontoTotalDetalles.equals(new BigDecimal(encabezado.getImporteDeposito()))) {
							if (bdMontoTotalDetalles.doubleValue()!=new BigDecimal(encabezado.getImporteDeposito()).doubleValue()) {
								bloqueCorrecto = false;
								mensajesDeError.append("\nError en la linea " +
										encabezado.getNumeroDeLinea() + ": " +
										"El monto total de importes de deposito de los detalles " +
										"no concuerda con el monto especificado en el encabezado");

								//System.out.println("MONTO TOTAL DETALLES= "+bdMontoTotalDetalles.toString());
								//System.out.println("IMPORTE DEL DEPOSITO = "+new BigDecimal(encabezado.getImporteDeposito()).toString());
							}
						}


						if (bloqueCorrecto) { //El Bloque sigue siendo correcto despu�s de las ultimas validaciones
							if (numProceso == 0) { //No ha obtenido su numero de proceso
								strSQL = "SELECT " +
										" CASE WHEN MAX(ic_proc) is null " +
										" 		THEN 1 ELSE MAX(ic_proc) + 1 " +
										" END " +
										" FROM comtmp_encabezado_pago ";
								rs = con.queryDB(strSQL);
								rs.next();
								numProceso = rs.getInt(1);
								rs.close();
								con.cierraStatement();
							}

							grabarPagoIFNBTemporal(con, numProceso, encabezado, arrDetalles);

							String sMensaje = "";

							if("NB".equals(strTipoBanco))
								sMensaje = "\n  Fecha Vencimiento: " + encabezado.getFechaVencimiento();

							mensajesDeExito.append(
									"\nLinea " + encabezado.getNumeroDeLinea() + ":" +
									sMensaje+
									"\n  Fecha probable de Pago: " + encabezado.getFechaProbablePago() +
									"\n  Fecha de deposito: " + encabezado.getFechaDeposito() +
									"\n  Importe del deposito: " + encabezado.getImporteDeposito() +
									observacionesDetalles);

						} else { //Bloque incorrecto -> Guarda en errores
							lineasConErrores.append(lineasDelBloque);
						}
					} else {
						mensajesDeError.append("La primera linea no corresponde a un Encabezado." +
								" Se omite el procesamiento de las demas lineas");
						//System.out.println("GEAG::La primera linea no es E");
						break; //Sale si la primera linea no es "E"
					}
				} //fin del while del encabezado
			} //fin del procesamiento del archivo
			resultado.add(0, mensajesDeError.toString());
			resultado.add(1, lineasConErrores.toString());
			resultado.add(2, mensajesDeExito.toString());
			resultado.add(3, new Integer(numProceso));

			return resultado;

		} catch(FileNotFoundException e) {
			System.out.println("El archivo " + ruta + " no se encontro");
			throw new NafinException("SIST0001");
		} catch(IOException e) {
			System.out.println("Error de Entrada/Salida al leer el archivo " + ruta);
			throw new NafinException("SIST0001");
		} catch(NamingException e) {
			System.out.println("No se encontro el DataSource: " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(SQLException e) {
			exito = false;
			System.out.println("Error de SQL: " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch (Exception e ) {
			exito = false;
			e.printStackTrace();
			System.out.println("Error inesperado: " + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch(Throwable t) {
				log.error("procesarArchivo(Exception)::Error al cerrar el archivo", t);
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene la informaci�n de Vencimientos de SIRAC
	 * y los almacena en las tablas de N@E.
	 * @return Cadena con la descripci�n del estatus resultante de la ejecuci�n
	 * 		del proceso
	 */
	public String obtenerVencimientosSirac() throws NafinException{

		System.out.println("PagosIFNBBean::obtenerVencimientosSirac(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		//String fechaActual = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).
		//		format(new java.util.Date());
		ResultSet rs =null;
		int numRegistros = 0;
		int numIFs = 0;
		String strSQL =null;
		int num_reg_mn= 0;
		int num_reg_dl= 0;
		String strSQLBitacora="";
		String cs_tipo="";
		String fechaprobablepago="";
		String fechaHoy= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String ic_proc_vencimiento = "";
		//List vecfecha = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs2 =null;
		int numFecProxPago = 0;
		int numFecExistVenc = 0;
		boolean realizaNuevasValid = true;
		boolean realizaCarga = true;

			System.out.println("fechaHoy"+fechaHoy);

		try {
		 	con.conexionDB();

		 	strSQL =
		 			" SELECT ig_if_primer_piso " +
		 			" FROM com_param_gral " +
		 			" WHERE ic_param_gral = 1 ";
			rs = con.queryDB(strSQL);
			rs.next();
			String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");

			rs.close();
			con.cierraStatement();

				//System.out.println("if_if_primerpiso"+strSQL);

		 	strSQL =
		 			" SELECT ic_proc_vencimiento " +
					" FROM comhis_proc_vencimiento " +
					" WHERE df_fecha_hora >= TRUNC(SYSDATE) " +
					" AND df_fecha_hora < TRUNC(SYSDATE + 1) " +
					" AND cg_nombre_archivo IS NULL ";
			rs = con.queryDB(strSQL);
			//System.out.println("numregistros"+strSQL);

			if (rs.next()) {
			
				log.debug("Se ejecuta una actualizacion mismo dia");
				
				int ic_proc_venc = rs.getInt("ic_proc_vencimiento");
				System.out.println("..:: INICIA LA ELIMINACION EN LA TABLA DE VENCIMIENTOS DEL FIDE PARA EL FONDO JR ::..");
				
				CallableStatement callableSt = con.ejecutaSP("sp_canc_proceso(?, ?, ?)");
				callableSt.setInt(1, ic_proc_venc);
				callableSt.setString(2, "V");
				callableSt.setInt(3, 10000);
				callableSt.executeUpdate();
				callableSt.close();
				System.out.println("..:: TERMINA LA ELIMINACION EN LA TABLA DE VENCIMIENTOS DEL FIDE PARA EL FONDO JR ::..");
				//Ya existe un procesamiento para el dia actual registrado en bitacora
				//throw new Exception("Ya existe un procesamiento del dia actual: " + fechaActual);
				
				realizaNuevasValid = false;

			}
			rs.close();
			con.cierraStatement();
			
			if(realizaNuevasValid){
				StringBuffer msgFecNoRegistradas = new StringBuffer();
				StringBuffer msgFecSiRegistradas = new StringBuffer();
				String strSQLFecProxPago = "select distinct to_char(fecha_proxpagoint,'dd/mm/yyyy') fecha_proxpagoint from pr_vencimientos_rep ";
				rs = con.queryDB(strSQLFecProxPago);
				
				while(rs!=null && rs.next()){
					numFecProxPago++;
					
					
					String strSQLExistVenc = "SELECT COUNT (cv.df_periodofin) " +
											"  FROM com_vencimiento cv, comhis_proc_vencimiento cpv " +
											" WHERE cv.ic_proc_vencimiento = cpv.ic_proc_vencimiento " +
											" AND cv.df_periodofin = to_date(?,'dd/mm/yyyy') " +
											" AND cpv.CG_NOMBRE_ARCHIVO is null ";

					ps = con.queryPrecompilado(strSQLExistVenc);
					//ps.setDate(1, rs.getDate("fecha_proxpagoint"));
					ps.setString(1, rs.getString("fecha_proxpagoint"));
					rs2 = ps.executeQuery();
					if(rs2.next()){
						if(rs2.getInt(1)>0){
							numFecExistVenc++;
							if(numFecProxPago==1)msgFecSiRegistradas.append(rs.getString("fecha_proxpagoint"));
							else msgFecSiRegistradas.append(", "+ rs.getString("fecha_proxpagoint"));
						}else{
							msgFecNoRegistradas.append(rs.getString("fecha_proxpagoint") + " - Fecha no resgistrada en NAFINET\n");
						}
					}
					rs2.close();
					ps.close();
				}
				rs.close();
				con.terminaTransaccion(true);
	
				if(numFecProxPago>0){
					if(numFecProxPago==numFecExistVenc){
						log.info("No se ejecuta ning�n proceso\n. Las fechas ya estan registradas en NAFIN: "+msgFecSiRegistradas.toString());
						realizaCarga = false;
					}else if(numFecExistVenc>0 && numFecProxPago>numFecExistVenc){
						log.info(msgFecNoRegistradas);
						realizaCarga = false;
					}else if(numFecExistVenc==0){
						log.info("Se ejecuta una carga nueva\n");
						realizaCarga = true;
					}
				}
			}

			if(realizaCarga){
			String strSQLDepura = "DELETE FROM COM_VENCIMIENTO " +
					" WHERE com_fechaprobablepago <= " +
					" (SELECT sysdate-in_dias_vig_vencimientos " +
					" 	FROM COMCAT_PRODUCTO_NAFIN " +
					" 	WHERE ic_producto_nafin = 1) ";

			try {
				con.ejecutaSQL(strSQLDepura);
			} catch(SQLException e) {
				throw new Exception("No fue posible depurar la tabla COM_VENCIMIENTO");
			}

			/*String strSQLConsecutivoBit =
					" SELECT " +
					" CASE WHEN MAX(ic_proc_vencimiento) IS NULL THEN " +
					" 	1 " +
					" ELSE " +
					" 	MAX(ic_proc_vencimiento) + 1 " +
					" END AS ic_proc_vencimiento " +
					" FROM COMHIS_PROC_VENCIMIENTO ";*/

			String strSQLConsecutivoBit =
					" select SEQ_comhis_proc_vencimiento.nextval  as ic_proc_vencimiento "+
					" from dual ";


			rs = con.queryDB(strSQLConsecutivoBit);
			rs.next();

			//System.out.println("strSQLConsecutivoBit"+strSQLConsecutivoBit);

			ic_proc_vencimiento = rs.getString("ic_proc_vencimiento");
			rs.close();
			con.cierraStatement();

			/*String strSQLBitacora =
					" INSERT INTO COMHIS_PROC_VENCIMIENTO " +
					" (ic_proc_vencimiento) " +
					" VALUES (" + ic_proc_vencimiento + " ) ";

			con.ejecutaSQL(strSQLBitacora);*/


/*
			String  strSQLVencimientosSirac =
					" INSERT INTO com_vencimiento (ic_vencimiento, " +
					" com_fechaprobablepago, ic_if, ic_moneda, ig_baseoperacion, " +
					" ig_cliente, cg_nombrecliente, ig_sucursal, ig_prestamo, ig_subaplicacion, ig_cuotasemit, " +
					" ig_cuotasporvenc, ig_totalcuotas, df_fechaopera, ig_sancionado, ig_frecuenciacapital, " +
					" ig_frecuenciainteres, ig_tasabase, cg_esquematasas, fg_margen, fg_porcdescfop, fg_totdescfop, " +
					" fg_porcdescfinape, fg_totdescfinape, df_periodoinic, df_periodofin, ig_dias, fg_sdoinsoluto, " +
					" fg_amortizacion, fg_interes, fg_totalvencimiento, fg_pagotrad, fg_subsidio, fg_totalexigible, " +
					" fg_sdoinsnvo, fg_sdoinsbase,  " +
					" ic_proc_vencimiento) " +
					" SELECT seq_com_vencto_ic_vencto.NEXTVAL, " +
					" v.fecha_real_pago, nvl(i.ic_if," + claveIFPrimerPiso + ") as ic_if, v.codigo_moneda, v.codigo_base_operacion, " +
					" v.codigo_cliente, v.nombre_cliente, v.codigo_agencia, v.numero_prestamo, v.codigo_sub_aplicacion, v.ae, " +
					" v.av, v.ta, v.fecha_apertura, TO_NUMBER(TRIM(v.bloqueo)), TO_NUMBER(TRIM(v.frecuencia_capital)) " +
					" , TO_NUMBER(TRIM(v.frecuencia_interes)), v.tasa_base, v.esquema_tasa, v.margen, v.porc_desc_fopyme, " +
					" v.tot_desc_fopyme, v.porc_desc_finape, v.tot_desc_finape, v.fecha_inicio_per, v.fecha_fin_per, v.cf_dias, " +
					" v.saldo_insoluto, v.amortizacion, v.interes, v.total_vencimiento, v.pago_tradicional, v.subsidio, " +
					" v.total_exigible, v.saldo_insoluto_nuevo, v.saldo_insoluto_base, " +
					" " + ic_proc_vencimiento +
					" FROM pr_vencimientos_rep v, comcat_if i  " +
					" WHERE " +
					" v.codigo_financiera = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");
					//Si no hay parametrizaci�n de if de primer piso entonces no se realiza el outer join.

			String  strSQLVencimientosSirac =
					" INSERT INTO com_vencimiento (ic_vencimiento, " +
					" 	com_fechaprobablepago, ic_if, ic_moneda, " +
					" 	cg_modalidadpago, ig_baseoperacion, cg_descbaseoper, " +
					" 	ig_cliente, cg_nombrecliente, ig_sucursal, " +
					" 	ig_proyecto, ig_contrato, ig_prestamo, " +
					" 	ig_numelectronico, cg_bursatilizado, ig_disposicion, " +
					" 	ig_subaplicacion, ig_cuotasemit, ig_cuotaspornc, " +
					" 	ig_totalcuotas, cg_aniomodalidad, ig_tipoestrato, " +
					" 	df_fechaopera, ig_sancionado, ig_frecuenciacapital, " +
					" 	ig_frecuenciainteres, ig_tasabase, cg_esquematasas, " +
					" 	cg_relmat_1, fg_spread_1, cg_relmat_2, " +
					" 	fg_spread_2, cg_relmat_3, fg_spread_3, " +
					" 	cg_relmat_4, fg_margen, ig_tipogarantia, " +
					" 	fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, " +
					" 	fg_totdescfinape, fg_fcrecimiento, df_periodoinic, " +
					" 	df_periodofin, ig_dias, fg_comision, " +
					" 	fg_porcgarantia, fg_porccomision, fg_sdoinsoluto, " +
					" 	fg_amortizacion, fg_interes, fg_totalncimiento, " +
					" 	fg_pagotrad, fg_subsidio, fg_totalexigible, " +
					" 	fg_capitalncido, fg_interesncido, fg_totalcarn, " +
					" 	fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, " +
					" 	fg_sdoinsn, fg_sdoinsbase, fg_intcobradoxanticip, " +
					" 	ic_proc_vencimiento) " +
					" SELECT seq_com_vencto_ic_vencto.NEXTVAL, " +
					" 	v.fechaprobablepago, nvl(i.ic_if," + claveIFPrimerPiso + ") as ic_if, v.codmoneda, " +
					" 	v.modalidadpago, v.baseoperacion, v.descbaseoper, " +
					" 	v.cliente, v.nombre, v.sucursal, " +
					" 	v.proyecto, v.contrato, v.prestamo, " +
					" 	v.numelectronico, v.bursatilizado, v.disposicion, " +
					" 	v.subaplicacion, v.cuotasemit, v.cuotasporvenc, " +
					" 	v.totalcuotas, v.aniomodalidad, v.tipoestrato, " +
					" 	v.fechaopera, TO_NUMBER(TRIM(v.sancionado)), TO_NUMBER(TRIM(v.frecuenciacapital)), " +
					" 	TO_NUMBER(TRIM(v.frecuenciainteres)), v.tasabase, v.esquematasas, " +
					" 	v.relmat_1, v.spread_1, v.relmat_2, " +
					" 	v.spread_2, v.relmat_3, v.spread_3, " +
					" 	v.relmat_4, v.margen, TO_NUMBER(TRIM(v.tipogarantia)), " +
					" 	v.porcdescfop, v.totdescfop, v.porcdescfinape, " +
					" 	v.totdescfinape, v.fcrecimiento, v.periodoinic, " +
					" 	v.periodofin, v.dias, v.comision, " +
					" 	v.porcgarantia, v.porccomision, v.sdoinsoluto, " +
					" 	v.amortizacion, v.interes, v.totalvencimiento, " +
					" 	v.pagotrad, v.subsidio, v.totalexigible, " +
					" 	v.capitalvencido, v.interesvencido, v.totalcarven, " +
					" 	v.adeudototal, v.finadicotorgado, v.finadicrecup, " +
					" 	v.sdoinsnvo, v.sdoinsbase, v.intcobant "
					" " + ic_proc_vencimiento +
					" FROM pr_vencimientos_rep v, comcat_if i  " +
					" WHERE " +
					" v.codigo_financiera = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");
					//Si no hay parametrizaci�n de if de primer piso entonces no se realiza el outer join.*/
      System.out.println("..:: ENTRA AL INSERT - SELECT ::..");
			String  strSQLVencimientosSirac =
					" INSERT INTO com_vencimiento (ic_vencimiento, " +
					" com_fechaprobablepago, ic_if, ic_moneda, ig_baseoperacion, " +
					" ig_cliente, cg_nombrecliente, ig_sucursal, ig_prestamo, ig_subaplicacion, ig_cuotasemit, " +
					" ig_cuotasporvenc, ig_totalcuotas, df_fechaopera, ig_sancionado, ig_frecuenciacapital, " +
					" ig_frecuenciainteres, ig_tasabase, cg_esquematasas, fg_margen, fg_porcdescfop, fg_totdescfop, " +
					" fg_porcdescfinape, fg_totdescfinape, df_periodoinic, df_periodofin, ig_dias, fg_sdoinsoluto, " +
					" fg_amortizacion, fg_interes, fg_totalvencimiento, fg_pagotrad, fg_subsidio, fg_totalexigible, " +
					" fg_sdoinsnvo, fg_sdoinsbase, cg_modalidadpago, cg_descbaseoper, ig_proyecto, ig_contrato, " +
					" ig_numelectronico,cg_bursatilizado, ig_disposicion,cg_aniomodalidad, ig_tipoestrato,cg_relmat_1, " +
					" fg_spread_1,cg_relmat_2,fg_spread_2,cg_relmat_3,fg_spread_3,cg_relmat_4, ig_tipogarantia, " +
					" fg_fcrecimiento, fg_comision, fg_porcgarantia, fg_porccomision, fg_capitalvencido, fg_interesvencido, "+
					" fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, fg_intcobradoxanticip, " +
					//" ic_proc_vencimiento) " +
					" ic_proc_vencimiento, cg_fondeo_birf, cg_CUENTA_CLABE) " +//se agrega por peticion de SIRAC para identificar un programa nuevo que se contemplara en el fondo junior
					" SELECT seq_com_vencto_ic_vencto.NEXTVAL, " +
					" v.fecha_real_pago, nvl(i.ic_if," + claveIFPrimerPiso + ") as ic_if, v.codigo_moneda, v.codigo_base_operacion, " +
					" v.codigo_cliente, v.nombre_cliente, v.codigo_agencia, v.numero_prestamo, v.codigo_sub_aplicacion, v.ae, " +
					" v.av, v.ta, v.fecha_apertura, TO_NUMBER(TRIM(v.bloqueo)), TO_NUMBER(TRIM(v.frecuencia_capital)) " +
					" , TO_NUMBER(TRIM(v.frecuencia_interes)), v.tasa_base, v.esquema_tasa, v.margen, v.porc_desc_fopyme, " +
					" v.tot_desc_fopyme, v.porc_desc_finape, v.tot_desc_finape, v.fecha_cuota_ant, v.fecha_proxpagoint, v.cf_dias, " +
					" v.saldo_insoluto, v.amortizacion, v.interes, v.total_vencimiento, v.pago_tradicional, v.subsidio, " +
					" v.total_exigible, v.saldo_insoluto_nuevo, v.saldo_insoluto_base, v.modalidadpago, v.codigo_base_operacion, v.proyecto, " +
					" v.contrato,v.numelectronico, v.bursatilizado, v.disposicion, v.aniomodalidad, v.tipoestrato,v.relmat_1,v.spread_1, " +
					" v.relmat_2, v.spread_2, v.relmat_3, v.spread_3, v.relmat_4,to_number(trim(v.tipogarantia)), v.fcrecimiento, " +
					" v.comision, v.porcgarantia, v.porccomision, v.capitalvencido, v.interesvencido, v.totalcarven, v.adeudototal, " +
					" v.finadicotorgado, v.finadicrecup,v.intcobant, " +
					" " + ic_proc_vencimiento + ", v.fondeo_birf, " +//se agrega por peticion de SIRAC para identificar un programa nuevo que se contemplara en el fondo junior
					" CUENTA_CLABE "+
					" FROM pr_vencimientos_rep v, comcat_if i  " +
					" WHERE " +
					" v.codigo_financiera = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");
					//Si no hay parametrizaci�n de if de primer piso entonces no se realiza el outer join.

			con.ejecutaSQL(strSQLVencimientosSirac);
          System.out.println("..:: SALE DEL INSERT - SELECT ::..");
/*          System.out.println("..:: ENTRA AL UPDATE ::..");
			//FODEA 015 - 2009 ACF (I)
			CallableStatement callableSt = con.ejecutaSP("SP_ACTUALIZA_VENCIMIENTO");
			callableSt.executeUpdate();
			callableSt.close();
			//FODEA 015 - 2009 ACF (F)
			//System.out.println("strSQLVencimientosSirac"+strSQLVencimientosSirac);
          System.out.println("..:: SALE DEL UPDATE ::..");
*/
		 /*	strSQL =
		 			" SELECT COUNT(*)  as numRegistros, " +
		 			" COUNT(DISTINCT ic_if) as numIFs " +
		 			" FROM com_vencimiento " +
		 			" WHERE ic_proc_vencimiento = " + ic_proc_vencimiento;
			rs = con.queryDB(strSQL);
			rs.next();
			numRegistros = rs.getInt("numRegistros");
			numIFs = rs.getInt("numIFs");
			rs.close();
			con.cierraStatement();

			if (numRegistros == 0) {
				//No existen datos a transferir de sirac a N@E
				throw new Exception("No hay datos para transferir de Sirac a N@E");
			}

			strSQLBitacora =
					" UPDATE COMHIS_PROC_VENCIMIENTO " +
					" SET ig_num_registros = " + numRegistros + "," +
					" ig_num_ifs = " + numIFs +
					" WHERE ic_proc_vencimiento = " + ic_proc_vencimiento;

			con.ejecutaSQL(strSQLBitacora);

			return "Proceso ejecutado con exito";*/


				 strSQL = "	SELECT to_char(cv.com_fechaprobablepago,'dd/mm/yyyy') as fechaprobablepago, cif.cs_tipo, "+
       				  "	COUNT (distinct cv.ic_if) numifs, COUNT (1) numregistros, "+
       				  " SUM (DECODE (cv.ic_moneda, 1, 1, 0)) totmn, "+
       				  " SUM (DECODE (cv.ic_moneda, 54, 1, 0)) totdl, "+
       				  " SUM (DECODE (cv.ic_moneda, 1, 0, 54, 0, 1)) tot_otro "+						 
  					  " FROM  com_vencimiento cv, comcat_if cif "+
 					  " WHERE cv.ic_if = cif.ic_if "+
 					  " AND ic_proc_vencimiento = " + ic_proc_vencimiento +
 					  " GROUP BY cv.com_fechaprobablepago,  cif.cs_tipo" ;

			rs = con.queryDB(strSQL);

			//System.out.println("strSQL Obtiene los datos"+strSQL);
			int i=0;

			while (rs.next()){
				fechaprobablepago = rs.getString("fechaprobablepago");
				cs_tipo=rs.getString("cs_tipo");
				numIFs = rs.getInt("numIFs");
				num_reg_mn= rs.getInt("totmn");
			   num_reg_dl= rs.getInt("totdl");
			   numRegistros = rs.getInt("tot_otro");				
				//vecfecha.add(fechaprobablepago);
				//System.out.println("fechaprobablepago vencimiento::: "+fechaprobablepago);

				strSQLBitacora =
					" INSERT INTO comhis_proc_vencimiento (ic_proc_vencimiento , com_fechaprobablepago,cg_nombre_archivo, df_fecha_hora, ig_num_registros, "+
					" ig_num_ifs,ig_num_reg_mn,ig_num_reg_dl, cs_tipo) " +
					" VALUES ("+ic_proc_vencimiento+","+"to_date('"+fechaprobablepago+"','dd/MM/yyyy')"+ ","+null+","+ "to_date('"+fechaHoy+"','dd/mm/yyyy HH24:mi:ss')"+","+numRegistros+","+numIFs+","+num_reg_mn+","+num_reg_dl+",'"+cs_tipo+"')";

				con.ejecutaSQL(strSQLBitacora);

				//System.out.println("QUERY DEL INSERT"+strSQLBitacora);
				i++;

			}//fin-while rs
			rs.close();
			con.cierraStatement();

			//System.out.println("VALOR I"+i);

				if (i==0) {

				//No existen datos a transferir de sirac a N@E
					throw new Exception("No hay datos para transferir de Sirac a N@E");
				}

/*     System.out.println("..:: INICIA LA ACTUALIZACION DE LA TABLA DE VENCIMIENTOS DEL FIDE PARA EL FONDO JR ::..");
			//SIN FODEA, SE REALIZO AJUSTE PARA DEJARLO DE LA MISMA MANERA EN QUE ESTA EN PRODUCCION
      // EGB 2010
			callableSt = con.ejecutaSP("sp_copia_vencimientos_fide");
			callableSt.executeUpdate();
			callableSt.close();
      System.out.println("..:: TERMINA LA ACTUALIZACION DE LA TABLA DE VENCIMIENTOS DEL FIDE PARA EL FONDO JR ::..");
*/
			}
			return "Proceso ejecutado con exito";

		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
			//return "Error en el proceso de Vencimiento. " + e.getMessage();
		} finally {
			System.out.println("PagosIFNBBean::obtenerVencimientosSirac(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			/*if(exito){
				System.out.println("Antes de crearArchivosZip");
				String respuesta = crearArchivosZip(ic_proc_vencimiento,vecfecha,1,"");
				System.out.println("Despues de crearArchivosZip");
			}*/
		}
	}

	/**
	 * Obtiene la informaci�n de Operados de SIRAC
	 * y los almacena en las tablas de N@E.
	 * @return Cadena con la descripci�n del estatus resultante de la ejecuci�n
	 * 		del proceso
	 */
 	public String obtenerOperadosSirac() throws NafinException{

		System.out.println("PagosIFNBBean::obtenerOperadosSirac(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		//StringBuffer mensaje = new StringBuffer();
		String fechaActual = (new SimpleDateFormat("dd/MM/yyyy")).
				format(new java.util.Date());
		ResultSet rs =null;
		int numRegistros = 0;
		int numIFs= 0;
		//String ic_moneda = "";
		int num_reg_mn= 0;
		int num_reg_dl= 0;
		String strSQL =null;
		String df_operacion="";
		String strSQLBitacora="";
		String cs_tipo="";
		String fechaHoy= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());
		//List vecfecha = new ArrayList();
		String ic_proc_operado = "";
		System.out.println("Entra a Obtener Operados Sirac");
		//System.out.println("fechaHoy"+fechaHoy);
		try {
		 	con.conexionDB();

		 	strSQL =
		 			" SELECT ig_if_primer_piso " +
		 			" FROM com_param_gral " +
		 			" WHERE ic_param_gral = 1 ";
			rs = con.queryDB(strSQL);
			rs.next();
			System.out.println("strSQL::: "+strSQL);
			String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");

			rs.close();
			con.cierraStatement();

		 	strSQL =
		 			" SELECT COUNT(*) as numRegistros " +
					" FROM comhis_proc_operado " +
					" WHERE df_fecha_hora >= TRUNC(SYSDATE) " +
					" AND df_fecha_hora < TRUNC(SYSDATE + 1) ";
			rs = con.queryDB(strSQL);
			rs.next();
			System.out.println("strSQL Registros en Comhis"+strSQL);
			if (rs.getInt("numRegistros") > 0) {
				//Ya existe un procesamiento para el dia actual registrado en bitacora
				throw new Exception("Ya existe un procesamiento del dia actual: " + fechaActual);
			}
			rs.close();
			con.cierraStatement();

			String strSQLDepura = "DELETE FROM com_operado " +
					" WHERE df_fechaoperacion <= " +
					" (SELECT sysdate-in_dias_vig_operados " +
					" 	FROM comcat_producto_nafin " +
					" 	WHERE ic_producto_nafin = 1) ";

			try {
				con.ejecutaSQL(strSQLDepura);
			} catch(SQLException e) {
				throw new Exception("No fue posible depurar la tabla COM_OPERADO");
			}
			/*
			String strSQLConsecutivoBit =
					" SELECT " +
					" CASE WHEN MAX(ic_proc_operado) IS NULL THEN " +
					" 	1 " +
					" ELSE " +
					" 	MAX(ic_proc_operado) + 1 " +
					" END AS ic_proc_operado " +
					" FROM comhis_proc_operado ";*/
			/* Se cambia el valor por una Sequencia SMJ */

			String strSQLConsecutivoBit =
					" select SEQ_COMHIS_PROC_OPERADO.nextval  as ic_proc_operado "+
					" from dual ";

			rs = con.queryDB(strSQLConsecutivoBit);
			rs.next();
			ic_proc_operado = rs.getString("ic_proc_operado");
			rs.close();
			con.cierraStatement();
			System.out.println("strSQLConsecutivoBit"+strSQLConsecutivoBit);

			/*String strSQLBitacora =
					" INSERT INTO comhis_proc_operado " +
					" (ic_proc_operado) " +
					" VALUES (" + ic_proc_operado + ") ";

			con.ejecutaSQL(strSQLBitacora);*/


/*			String  strSQLOperadosSirac =
					" INSERT INTO COM_OPERADO " +
					" ( " +
					" 	ic_operado, " +
					" 	ic_if, ic_moneda, df_fechaoperacion, " +
					" 	ig_baseoperacion, ig_cliente, cg_nombrecliente, " +
					" 	ig_succliente, ig_prestamo, ig_subaplicacion, " +
					" 	df_fechapripagcap, ig_tasa, cg_relmat, " +
					" 	fg_spread, ig_freccap, ig_frecint, " +
					" 	ig_numcoutas, ig_sucinter, fg_montooper, " +
					" 	fg_montoprimcuot, fg_montoultcout, fg_comnodisp, " +
					" 	fg_netootorgado, fg_intcobradoxanticip, " +
					" 	ic_proc_operado) " +
					" SELECT  SEQ_COM_OPER_IC_OPER.nextval," +
					" 	NVL(i.ic_if," + claveIFPrimerPiso + " ) as ic_if, o.ic_moneda, o.df_fechaoperacion, " +
					" 	o.ig_baseoperacion, o.ig_cliente, o.cg_nombrecliente, " +
					" 	o.ig_succliente, o.ig_prestamo, o.ig_subaplicacion, " +
					" 	o.df_fechapripagcap, o.ig_tasa, o.cg_relmat, " +
					" 	o.fg_spread, o.ig_freccap, o.ig_frecint, " +
					" 	o.ig_numcuotas, o.ig_sucinter, o.fg_montooper, " +
					" 	o.fg_montoprimcuot, o.fg_montoultcuot, o.fg_comnodisp, " +
					" 	o.fg_netootorgado, o.fg_intcobradoxanticip, " +
					" " + ic_proc_operado +
					" FROM pr_operados_rep o, comcat_if i " +
					" WHERE " +
					" 	o.ic_if = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");
*/
	/*		String  strSQLOperadosSirac =
					" INSERT INTO COM_OPERADO " +
					" ( " +
					" 	ic_operado, " +
					" 	ic_if, ic_moneda, ig_estatus, " +
					" 	df_fechaoperacion, fg_tipocambio, cg_calificacion, " +
					" 	ig_baseoperacion, cg_descbaseop, ig_cliente, " +
					" 	cg_nombrecliente, ig_succliente, ig_proyecto, " +
					" 	ig_contrato, ig_prestamo, ig_numelectronico, " +
					" 	ig_disposicion, ig_estado, ig_municipio, " +
					" 	ig_subaplicacion, ig_suctram, df_fechapripagcap, " +
					" 	ig_tasa, cg_relmat, fg_spread, " +
					" 	fg_margen, cg_acteco, cg_aniomod, " +
					" 	ig_estrato, cg_afianz, ig_freccap, " +
					" 	ig_frecint, ig_numcoutas, ig_tipoamort, " +
					" 	cg_modpago, cg_centrofin, ig_sucinter, " +
					" 	fg_montooper, fg_montoprimcuot, fg_montoultcout, " +
					" 	fg_montorecal, fg_montoporgar, fg_comnodisp, " +
					" 	ig_tipogar, fg_netootorgado, cg_primabruta, " +
					" 	cg_gastos, fg_intcobradoxanticip, " +
					" 	ic_proc_operado) " +
					" SELECT  SEQ_COM_OPER_IC_OPER.nextval," +
					" 	NVL(i.ic_if," + claveIFPrimerPiso + " ) as ic_if, o.codmoneda, o.status,  " +
					" 	o.fechaoperacion, o.tipocambio, o.calificacion, " +
					" 	o.baseoperacion, o.descbaseop, o.cliente, " +
					" 	o.nombrecliente, TO_NUMBER(TRIM(o.succliente)), o.proyecto, " +
					" 	o.contrato, o.prestamo, o.numelectronico, " +
					" 	o.disposicion, o.estado, municipio, " +
					" 	o.subaplicacion, TO_NUMBER(TRIM(o.suctram)), o.fechapripagcap, " +
					" 	o.tasa, o.relmat, o.spread, " +
					" 	o.margen, o.acteco, o.aniomod, " +
					" 	o.estrato, o.afianz, TO_NUMBER(TRIM(o.freccap)), " +
					" 	TO_NUMBER(TRIM(o.frecint)), o.numcuotas, TO_NUMBER(TRIM(o.tipoamort)), " +
					" 	o.modpago, o.centrofin, TO_NUMBER(TRIM(o.sucinter)), " +
					" 	o.montooper, o.montoprimcuot, o.montoultcuota, " +
					" 	o.montorecal, o.comisionporgar, o.comnodisp, " +
					" 	o.tipogar, o.netootorgado, o.primabruta, " +
					" 	o.gastos, o.intcobant," +
					" " + ic_proc_operado +
					" FROM pr_operados_rep o, comcat_if i " +
					" WHERE " +
					" 	o.intermediario = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");*/

				String  strSQLOperadosSirac =
					" INSERT INTO COM_OPERADO " +
					" ( " +
					" 	ic_operado, " +
					" 	ic_if, ic_moneda, ig_estatus, " +
					" 	df_fechaoperacion, fg_tipocambio, cg_calificacion, " +
					" 	ig_baseoperacion, cg_descbaseop, ig_cliente, " +
					" 	cg_nombrecliente, ig_succliente, ig_proyecto, " +
					" 	ig_contrato, ig_prestamo, ig_numelectronico, " +
					" 	ig_disposicion, ig_estado, ig_municipio, " +
					" 	ig_subaplicacion, ig_suctram, df_fechapripagcap, " +
					" 	ig_tasa, cg_relmat, fg_spread, " +
					" 	fg_margen, cg_acteco, cg_aniomod, " +
					" 	ig_estrato, cg_afianz, ig_freccap, " +
					" 	ig_frecint, ig_numcoutas, ig_tipoamort, " +
					" 	cg_modpago, cg_centrofin, ig_sucinter, " +
					" 	fg_montooper, fg_montoprimcuot, fg_montoultcout, " +
					" 	fg_montorecal, fg_montoporgar, fg_comnodisp, " +
					" 	ig_tipogar, fg_netootorgado, cg_primabruta, " +
					" 	cg_gastos, fg_intcobradoxanticip, " +
					" 	ic_proc_operado) " +
					" SELECT  SEQ_COM_OPER_IC_OPER.nextval," +
					" 	NVL(i.ic_if," + claveIFPrimerPiso + " ) as ic_if, o.ic_moneda, o.status,  " +
					" 	o.df_fechaoperacion, o.tipocambio, o.calificacion,  " +
					" 	o.ing_baseoperacion, o.descbaseop, o.ing_cliente, " +
					" 	o.cg_nombrecliente, TO_NUMBER(TRIM(o.ing_succliente)), o.proyecto, " +
					" 	o.contrato, o.ing_prestamo, o.numelectronico, " +
					" 	o.disposicion, o.estado, municipio, " +
					" 	o.ing_subaplicacion, TO_NUMBER(TRIM(o.suctram)), o.df_fechapripagcap, " +
					" 	o.ig_tasa, o.cg_relmat, o.fg_spread, " +
					" 	o.margen, o.acteco, o.aniomod, " +
					" 	o.estrato, o.afianz, TO_NUMBER(TRIM(o.ig_freccap)),  " +
					" 	TO_NUMBER(TRIM(o.ig_frecint)), o.ig_numcuotas, TO_NUMBER(TRIM(o.tipoamort)), " +
					" 	o.modpago, o.centrofin, TO_NUMBER(TRIM(o.ig_sucinter)), " +
					" 	o.fg_montooper, o.fg_montoprimcuot, o.fg_montoultcuot,  " +
					" 	o.montorecal, o.comisionporgar, o.fg_comnodisp, " +
					" 	o.tipogar, o.fg_netootorgado, o.primabruta, " +
					" 	o.gastos, o.fg_intcobradoxanticip," +
					" " + ic_proc_operado +
					" FROM pr_operados_rep o, comcat_if i " +
					" WHERE " +
					" 	o.ic_if = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");

			con.ejecutaSQL(strSQLOperadosSirac);

		 System.out.println("strSQLOperadosSirac"+strSQLOperadosSirac);

		 	 strSQL = "	SELECT to_char(co.df_fechaoperacion,'dd/mm/yyyy') as df_fechaoperacion, cif.cs_tipo, "+
       				  "	COUNT (distinct co.ic_if) numifs, COUNT (1) numregistros, "+
       				  " SUM (DECODE (co.ic_moneda, 1, 1, 0)) totmn, "+
       				  " SUM (DECODE (co.ic_moneda, 54, 1, 0)) totdl, "+
       				  " SUM (DECODE (co.ic_moneda, 1, 0, 54, 0, 1)) tot_otro "+
  					  " FROM com_operado co, comcat_if cif "+
 					  " WHERE co.ic_if = cif.ic_if "+
 					  " AND ic_proc_operado ="+ ic_proc_operado +
 					  " GROUP BY co.df_fechaoperacion,  cif.cs_tipo" ;

			rs = con.queryDB(strSQL);

			System.out.println("strSQL Obtiene los datos"+strSQL);
			int i=0;
			while (rs.next()){
				df_operacion = rs.getString("df_fechaoperacion");
				cs_tipo=rs.getString("cs_tipo");
				numIFs = rs.getInt("numIFs");
				num_reg_mn= rs.getInt("totmn");
			    num_reg_dl= rs.getInt("totdl");
			    numRegistros = rs.getInt("tot_otro");
				//vecfecha.add(df_operacion);
				//System.out.println("df_operacion operados::: "+df_operacion);

				strSQLBitacora =
					" INSERT INTO comhis_proc_operado (ic_proc_operado, df_operacion,cg_nombre_archivo, df_fecha_hora, ig_num_registros, "+
					" ig_num_ifs,ig_num_reg_mn,ig_num_reg_dl, cs_tipo) " +
					" VALUES ("+ic_proc_operado+","+"to_date('"+df_operacion+"','dd/MM/yyyy')"+ ","+null+","+ "to_date('"+fechaHoy+"','dd/mm/yyyy HH24:mi:ss')"+","+numRegistros+","+numIFs+","+num_reg_mn+","+num_reg_dl+",'"+cs_tipo+"')";

				con.ejecutaSQL(strSQLBitacora);

				//System.out.println("QUERY DEL INSERT"+strSQLBitacora);
				i++;
			}//fin-while rs

			System.out.println("VALOR I"+i);

				if (i==0) {
				//No existen datos a transferir de sirac a N@E
					throw new Exception("No hay datos para transferir de Sirac a N@E");
				}

			rs.close();
			con.cierraStatement();



			return "Proceso ejecutado con exito";

		} catch(Exception e) {
			exito = false;
			//return "Error en el proceso de Operados. " + e.getMessage();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("PagosIFNBBean::obtenerOperadosSirac (S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			/*if(exito){
				System.out.println("Antes de crearArchivosZip");
				String respuesta = crearArchivosZip(ic_proc_operado,vecfecha,2,"");
				System.out.println("Despues de crearArchivosZip");
			}*/
		}
	}



	/**
	 * Obtiene la informaci�n de Estados de Cuenta de SIRAC
	 * y los almacena en las tablas de N@E.
	 * @return Cadena con la descripci�n del estatus resultante de la ejecuci�n
	 * 		del proceso*/


	public String obtenerEstadosDeCuentaSirac() throws NafinException {

		System.out.println("PagosIFNBBean::obtenerEstadosDeCuentaSirac(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		//StringBuffer mensaje = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Calendar cal = new GregorianCalendar();
		Calendar fecha = Calendar.getInstance();

		cal.set(Calendar.DATE, 1); //Primer dia del mes actual
		String fechaInicial = sdf.format(cal.getTime()); //Fecha con el primer dia del mes actual

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH)); //Ultimo dia del mes actual
		String fechaFinal = sdf.format(cal.getTime()); //Fecha con el ultimo dia del mes actual

		//Para obtener la fecha del mes anterior

		int ano = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH);
		int ic_mes_calculo = mes;
		int ic_anio_calculo = ano;
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, ic_anio_calculo);
		cal.set(Calendar.MONTH, ic_mes_calculo-1);	//Le restamos 1 al mes en curso
		//int ultimoDiaDelMesA = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		//String fechaInicialA = sdf.format(cal.getTime()); //Fecha con el primer dia del mes anterior
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DAY_OF_MONTH)); //Ultimo dia del mes anterior
		String fechaFinalA = sdf.format(cal.getTime()); //Fecha con el ultimo dia del mes anterior
		String fechaHoy= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

		System.out.println("fechaHoy"+fechaHoy);
		ResultSet rs =null;
		int numRegistros = 0;
		int numIFs = 0;
		String strSQL =null;
		int num_reg_mn= 0;
		int num_reg_dl= 0;
		String fecha_fin_mes="";
		String cs_tipo="";
		String strSQLBitacora="";
		String ic_proc_edo_cuenta = "";
                String ic_if="";
		//List vecfecha = new ArrayList();

		try {
		 	con.conexionDB();

		 	strSQL =
		 			" SELECT ig_if_primer_piso " +
		 			" FROM com_param_gral " +
		 			" WHERE ic_param_gral = 1 ";
			rs = con.queryDB(strSQL);
			rs.next();
			String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");

			rs.close();
			con.cierraStatement();

		 	strSQL =
		 			" SELECT COUNT(*)  as numRegistros, " +
		 			" COUNT(DISTINCT NVL(intermediario," + claveIFPrimerPiso + ")) as numIFs " +
		 			" FROM pr_edocta_rep " +
		 			" where fechafinmes = TO_DATE('" + fechaFinalA + "','dd/mm/yyyy') " ;


			rs = con.queryDB(strSQL);
							System.out.println ("Query para obtener registros: ::"+strSQL);


			rs.next();
			int numRegistros_pr=0;
			numRegistros_pr= rs.getInt("numRegistros");
			//numIFs = rs.getInt("numIFs");
			rs.close();
			con.cierraStatement();
			con.terminaTransaccion(true); //Debido a que pr_edocta_rep se consulta a traves de un link

			if (numRegistros_pr == 0) {
				//No existen datos a transferir de sirac a N@E
				throw new Exception("No hay datos para transferir de Sirac a N@E");
			}

		 	strSQL =
		 			" SELECT COUNT(*) as numRegistros " +
					" FROM comhis_proc_edo_cuenta " +
					" WHERE df_fecha_hora >= TO_DATE('" + fechaInicial + "','dd/mm/yyyy') " +
					" AND df_fecha_hora < TO_DATE('" + fechaFinal + "','dd/mm/yyyy') + 1 ";
			rs = con.queryDB(strSQL);
				System.out.println ("Query para obtener registros de comhis: ::"+strSQL);
			rs.next();
			if (rs.getInt("numRegistros") > 0) {
				//Ya existe un procesamiento para el dia actual registrado en bitacora
				throw new Exception("Ya existe un procesamiento en el mes actual");
			}
			rs.close();
			con.cierraStatement();


			String strSQLDepura =
					" DELETE FROM com_estado_cuenta " +
					" WHERE df_fechafinmes < " +
					" 	(SELECT ADD_MONTHS( " +
					" 			TO_DATE('" + fechaInicial + "','dd/mm/yyyy'),-in_dias_vig_edo_cuenta/30) " +
					" 	FROM comcat_producto_nafin " +
					" 	WHERE ic_producto_nafin = 1) ";

			try {
				con.ejecutaSQL(strSQLDepura);
			} catch(SQLException e) {
				throw new Exception("No fue posible depurar la tabla COM_ESTADO_CUENTA");
			}

			/*String strSQLConsecutivoBit =
					" SELECT " +
					" CASE WHEN MAX(ic_proc_edo_cuenta) IS NULL THEN " +
					" 	1 " +
					" ELSE " +
					" 	MAX(ic_proc_edo_cuenta) + 1 " +
					" END AS ic_proc_edo_cuenta " +
					" FROM comhis_proc_edo_cuenta ";*/

			String strSQLConsecutivoBit =
					" select SEQ_comhis_proc_edo_cuenta.nextval  as ic_proc_edo_cuenta "+
					" from dual ";

			rs = con.queryDB(strSQLConsecutivoBit);
			rs.next();
			ic_proc_edo_cuenta = rs.getString("ic_proc_edo_cuenta");
			rs.close();
			con.cierraStatement();

		/*	String strSQLBitacora =
					" INSERT INTO comhis_proc_edo_cuenta " +
					" (ic_proc_edo_cuenta) " +
					" VALUES (" + ic_proc_edo_cuenta + " ) ";

			con.ejecutaSQL(strSQLBitacora);*/



			String  strSQLIfsEdosCuentaSirac =
					" SELECT  distinct e.intermediario " +
					" FROM pr_edocta_rep e, comcat_if i " +
					" WHERE e.intermediario = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");

 			rs = con.queryDB(strSQLIfsEdosCuentaSirac);
			while (rs.next()) {
				String claveIntermediario = rs.getString("intermediario");
				String condicion = (claveIntermediario == null)?" AND e.intermediario IS NULL ":
						 " AND e.intermediario = " + claveIntermediario + " ";


				String  strSQLEdosCuentaSirac =
						" INSERT INTO com_estado_cuenta " +
						" ( " +
						" 	ic_estado_cuenta, " +
						" 	df_fechafinmes, ic_moneda, ic_if, " +
						" 	ig_codsucursal, cg_nomsucursal, ig_tipointermediario, " +
						" 	cg_tipointermediario_desc, cg_descmodpago, ig_prodbanco, " +
						" 	cg_prodbanco_desc, ig_subapl, cg_subapl_desc, " +
						" 	ig_cliente, cg_nombrecliente, ig_proyecto, " +
						" 	ig_contrato, ig_prestamo, ig_numero_electronico, " +
						" 	ig_disposicion, ig_frecinteres, ig_freccapital, " +
						" 	df_fechoperacion, df_fechvencimiento, ig_tasareferencial, " +
						" 	cg_descripciontasa, cg_relmat1, fg_spread, " +
						" 	fg_margen, fg_tasamoratoria, ig_sancion, " +
						" 	df_1acuotaven, ig_diavencimiento, ig_diaprovision, " +
						" 	fg_montooperado, fg_saldoinsoluto, fg_capitalvigente, " +
						" 	fg_capitalvencido, fg_interesprovi, fg_intcobradoxanticip, " +
						" 	fg_interesvencido, fg_interesmorat, fg_sobretasamor, " +
						" 	fg_otrosadeudos, fg_comisiongtia, fg_sobtasagtia_porc, " +
						" 	fg_finadicotorg, fg_finanadicrecup, fg_totalfinan, " +
						" 	fg_adeudototal, fg_capitalrecup, fg_interesrecup, " +
						" 	fg_morarecup, fg_saldonafin, fg_saldobursatil, " +
						" 	fg_valortasa, fg_tasatotal, cg_edocartera, " +
						" 	fg_interesgravprov, fg_ivaprov, fg_interesvencidogravado, " +
						" 	fg_ivavencido, fg_interesmoratgravado, fg_ivasobremoratorios, " +
						" 	fg_sobretasamorgravado, fg_comisiones, fg_interesrecupgravado, " +
						" 	fg_ivarecup, fg_subsidioaplicado, fg_morarecupgravado, " +
						" 	ic_proc_edo_cuenta) " +
						" SELECT  SEQ_COM_EDO_CUENTA_IC_CUENTA.nextval," +
						" 	e.fechafinmes, e.moneda, NVL(i.ic_if," + claveIFPrimerPiso + " ) as ic_if, " +
						" 	e.codsucursal, e.nomsucursal, e.tipintermediario, " +
						" 	e.descripcion, e.descmodpago, e.prodbanco, " +
						" 	e.descripcion_pbamco, e.subapl, e.descripcion_sapl, " +
						" 	e.cliente, e.nombrecliente, e.proyecto, " +
						" 	e.contrato, e.prestamo, TO_CHAR(e.numelectronico), " +
						" 	e.disposicion, TO_NUMBER(TRIM(e.frecinteres)), TO_NUMBER(TRIM(e.freccapital)), " +
						" 	e.fechoperacion, e.fechvencimiento, e.tasareferencial, " +
						" 	e.descripciontasa, e.relmat1, e.spread, " +
						" 	e.margen, e.tasamoratoria, TO_NUMBER(TRIM(e.sancion)), " +
						" 	e.facuotaven, TO_NUMBER(TRIM(e.diavencimiento)), TO_NUMBER(TRIM(e.diaprovision)), " +
						" 	e.montooperado, e.saldoinsoluto, e.capitalvigente, " +
						" 	e.capitalvencido, e.interesprovi, e.intcobant, " +
						" 	e.interesvencido, e.interesmorat, e.sobretasamor, " +
						" 	e.otrosadeudos, e.comisiongtia, TO_NUMBER(TRIM(e.sobtasagtia)), " +
						" 	e.finadicotorg, e.finanadicrecup, e.totalfinan, " +
						" 	e.adeudototal, e.capitalrecup, e.interesrecup, " +
						" 	e.morarecup, e.saldonafin, e.saldobursatil, " +
						" 	e.valortasa, e.tasatotal, e.edocartera, " +
						" 	e.interesgravprov, e.ivaprov, e.interesvencidogravado, " +
						" 	e.ivavencido, e.interesmoratgravado, e.ivasobremoratorios, " +
						" 	e.sobretasamorgravado, e.comisiones, e.interesrecupgravado, " +
						" 	e.iva_recup, e.subsidioaplicado, e.morarecupgravado, " +
						" " + ic_proc_edo_cuenta +
						" FROM pr_edocta_rep e, comcat_if i" +
						" WHERE e.intermediario = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)") +
						condicion;

				con.ejecutaSQL(strSQLEdosCuentaSirac);
				con.terminaTransaccion(true); //Se realiza el commit por bloques porque puede traer mucha informaci�n.
				//System.out.println ("El insert: ::"+strSQLEdosCuentaSirac);

			}
			rs.close();
			con.cierraStatement();




/*

			String  strSQLEdosCuentaSirac =
					" INSERT INTO com_estado_cuenta " +
					" ( " +
					" 	ic_estado_cuenta, " +
					" 	df_fechafinmes, ic_moneda, ic_if, " +
					" 	ig_codsucursal, cg_nomsucursal, ig_tipointermediario, " +
					" 	cg_tipointermediario_desc, cg_descmodpago, ig_prodbanco, " +
					" 	cg_prodbanco_desc, ig_subapl, cg_subapl_desc, " +
					" 	ig_cliente, cg_nombrecliente, ig_proyecto, " +
					" 	ig_contrato, ig_prestamo, ig_numero_electronico, " +
					" 	ig_disposicion, ig_frecinteres, ig_freccapital, " +
					" 	df_fechoperacion, df_fechvencimiento, ig_tasareferencial, " +
					" 	cg_descripciontasa, cg_relmat1, fg_spread, " +
					" 	fg_margen, fg_tasamoratoria, ig_sancion, " +
					" 	df_1acuotaven, ig_diavencimiento, ig_diaprovision, " +
					" 	fg_montooperado, fg_saldoinsoluto, fg_capitalvigente, " +
					" 	fg_capitalvencido, fg_interesprovi, fg_intcobradoxanticip, " +
					" 	fg_interesvencido, fg_interesmorat, fg_sobretasamor, " +
					" 	fg_otrosadeudos, fg_comisiongtia, fg_sobtasagtia_porc, " +
					" 	fg_finadicotorg, fg_finanadicrecup, fg_totalfinan, " +
					" 	fg_adeudototal, fg_capitalrecup, fg_interesrecup, " +
					" 	fg_morarecup, fg_saldonafin, fg_saldobursatil, " +
					" 	fg_valortasa, fg_tasatotal, cg_edocartera, " +
					" 	fg_interesgravprov, fg_ivaprov, fg_interesvencidogravado, " +
					" 	fg_ivavencido, fg_interesmoratgravado, fg_ivasobremoratorios, " +
					" 	fg_sobretasamorgravado, fg_comisiones, fg_interesrecupgravado, " +
					" 	fg_ivarecup, fg_subsidioaplicado, fg_morarecupgravado, " +
					" 	ic_proc_edo_cuenta) " +
					" SELECT  SEQ_COM_EDO_CUENTA_IC_CUENTA.nextval," +
					" 	e.fechafinmes, e.moneda, NVL(i.ic_if," + claveIFPrimerPiso + " ) as ic_if, " +
					" 	e.codsucursal, e.nomsucursal, e.tipintermediario, " +
					" 	e.descripcion, e.descmodpago, e.prodbanco, " +
					" 	e.descripcion_pbamco, e.subapl, e.descripcion_sapl, " +
					" 	e.cliente, e.nombrecliente, e.proyecto, " +
					" 	e.contrato, e.prestamo, TO_CHAR(e.numelectronico), " +
					" 	e.disposicion, TO_NUMBER(TRIM(e.frecinteres)), TO_NUMBER(TRIM(e.freccapital)), " +
					" 	e.fechoperacion, e.fechvencimiento, e.tasareferencial, " +
					" 	e.descripciontasa, e.relmat1, e.spread, " +
					" 	e.margen, e.tasamoratoria, TO_NUMBER(TRIM(e.sancion)), " +
					" 	e.facuotaven, TO_NUMBER(TRIM(e.diavencimiento)), TO_NUMBER(TRIM(e.diaprovision)), " +
					" 	e.montooperado, e.saldoinsoluto, e.capitalvigente, " +
					" 	e.capitalvencido, e.interesprovi, e.intcobant, " +
					" 	e.interesvencido, e.interesmorat, e.sobretasamor, " +
					" 	e.otrosadeudos, e.comisiongtia, TO_NUMBER(TRIM(e.sobtasagtia)), " +
					" 	e.finadicotorg, e.finanadicrecup, e.totalfinan, " +
					" 	e.adeudototal, e.capitalrecup, e.interesrecup, " +
					" 	e.morarecup, e.saldonafin, e.saldobursatil, " +
					" 	e.valortasa, e.tasatotal, e.edocartera, " +
					" 	e.interesgravprov, e.ivaprov, e.interesvencidogravado, " +
					" 	e.ivavencido, e.interesmoratgravado, e.ivasobremoratorios, " +
					" 	e.sobretasamorgravado, e.comisiones, e.interesrecupgravado, " +
					" 	e.iva_recup, e.subsidioaplicado, e.morarecupgravado, " +
					" " + ic_proc_edo_cuenta +
					" FROM pr_edocta_rep e, comcat_if i" +
					" WHERE e.intermediario = i.ic_financiera" + ((claveIFPrimerPiso.equals("NULL"))?"":"(+)");

			con.ejecutaSQL(strSQLEdosCuentaSirac);

		 	strSQL =
		 			" SELECT COUNT(*)  as numRegistros, " +
		 			" COUNT(DISTINCT ic_if) as numIFs " +
		 			" FROM com_estado_cuenta " +
		 			" WHERE ic_proc_edo_cuenta = " + ic_proc_edo_cuenta;
			rs = con.queryDB(strSQL);
			rs.next();
			numRegistros = rs.getInt("numRegistros");
			numIFs = rs.getInt("numIFs");
			rs.close();
			con.cierraStatement();

			if (numRegistros == 0) {
				//No existen datos a transferir de sirac a N@E
				throw new Exception("No hay datos para transferir de Sirac a N@E");
			}
		*/

/* SE COMENTA PARA AGREGAR LA NUEVA FUNCIONALIDAD  SMJ
			strSQLBitacora =
					" UPDATE comhis_proc_edo_cuenta " +
					" SET ig_num_registros = " + numRegistros + "," +
					" ig_num_ifs =  " + numIFs +
					" WHERE ic_proc_edo_cuenta =  " + ic_proc_edo_cuenta;
			con.ejecutaSQL(strSQLBitacora);

			return "Proceso ejecutado con exito";*/


			strSQL=
				" SELECT to_char(df_fechafinmes,'dd/mm/yyyy')AS fecha_finmes, cif.cs_tipo, "+
				" COUNT (distinct cec.ic_if) numifs, COUNT (1) numregistros,  "+
				" SUM (DECODE (cec.ic_moneda, 1, 1, 0)) totmn,  SUM (DECODE (cec.ic_moneda, 54, 1, 0)) totdl,  "+
				" SUM (DECODE (cec.ic_moneda, 1, 0, 54, 0, 1)) tot_otro  "+
				" FROM com_estado_cuenta cec, comcat_if cif  "+
				" WHERE cec.ic_if = cif.ic_if "+
				" AND ic_proc_edo_cuenta ="+ ic_proc_edo_cuenta +
				" GROUP BY cec.df_fechafinmes,  cif.cs_tipo";
			rs = con.queryDB(strSQL);

			//System.out.println("strSQL Obtiene los datos"+strSQL);

			int i=0;
			while (rs.next()){
				fecha_fin_mes = rs.getString("fecha_finmes");
				cs_tipo=rs.getString("cs_tipo");
				numIFs = rs.getInt("numIFs");
				num_reg_mn= rs.getInt("totmn");
			    num_reg_dl= rs.getInt("totdl");
			    numRegistros = rs.getInt("tot_otro");
			    //vecfecha.add(fecha_fin_mes);
			    //System.out.println("fecha_fin_mes estados de cuenta::: "+fecha_fin_mes);

			strSQLBitacora =
					" INSERT INTO comhis_proc_edo_cuenta (ic_proc_edo_cuenta, df_fechafinmes,cg_nombre_archivo, df_fecha_hora, ig_num_registros, "+
					" ig_num_ifs,ig_num_reg_mn,ig_num_reg_dl, cs_tipo) " +
					" VALUES ("+ic_proc_edo_cuenta+","+"to_date('"+fecha_fin_mes+"','dd/mm/yyyy')"+ ","+null+","+ "to_date('"+fechaHoy+"','dd/mm/yyyy HH24:mi:ss')"+","+numRegistros+","+numIFs+","+num_reg_mn+","+num_reg_dl+",'"+cs_tipo+"')";

				con.ejecutaSQL(strSQLBitacora);

				//System.out.println("QUERY DEL INSERT"+strSQLBitacora);
				i++;
			}//fin-while rs



			//System.out.println("VALOR I"+i);

				if (i==0) {
				//No existen datos a transferir de sirac a N@E
					throw new Exception("No hay datos para transferir de Sirac a N@E");
				}

			rs.close();
			con.cierraStatement();
                    
                    /*
                     * 002 2017 Generacion bitacora seguimiento publicacion Estados de cuenta por IF
                     * INICIO
                     * */
                    strSQL="SELECT TO_CHAR(df_fechafinmes,'dd/mm/yyyy')AS fecha_finmes," + 
                    "       cif.ic_if ic_if," + 
                    "       ic_proc_edo_cuenta" +
                    " FROM com_estado_cuenta cec," + 
                    "     comcat_if cif " + 
                    " WHERE cec.ic_if = cif.ic_if" + 
                    " AND cec.ic_proc_edo_cuenta NOT IN" + 
                    "  (SELECT BI_NOTIFICA_EDO_CTA.ic_proc_edo_cuenta FROM BI_NOTIFICA_EDO_CTA)" + 
                    " GROUP BY cec.df_fechafinmes," + 
                    "  cif.ic_if," + 
                    "  ic_proc_edo_cuenta " +
                    " ORDER BY cif.ic_if ASC ";
                    rs = con.queryDB(strSQL);

                    //System.out.println("strSQL Obtiene los datos"+strSQL);

                    i=0;
                    while (rs.next()){
                            fecha_fin_mes = rs.getString("fecha_finmes");
                            ic_if = rs.getString("ic_if");
                            ic_proc_edo_cuenta= rs.getString("ic_proc_edo_cuenta");

                    String strSQLBitacoraEdoCta =
                                    " INSERT INTO BI_NOTIFICA_EDO_CTA (IC_NOTIFICA, DF_FIN_MES, IC_IF, IC_PROC_EDO_CUENTA)" +
                                    " VALUES (SEQ_BI_NOTIFICA_EDO_CTA.nextval,"+"to_date('"+fecha_fin_mes+"','dd/mm/yyyy'),"+ic_if+","+ic_proc_edo_cuenta+")";

                            con.ejecutaSQL(strSQLBitacoraEdoCta);

                            System.out.println("QUERY DEL INSERT "+strSQLBitacoraEdoCta);
                            i++;
                    }//fin-while rs
                    rs.close();
                    con.cierraStatement();

                    /*
                     * 002 2017 FIN
                     * */


			System.out.println("ic_proc_edo_cuenta::: "+ic_proc_edo_cuenta);


			return "Proceso ejecutado con exito";

		} catch(Exception e) {
			exito = false;
			throw new NafinException("SIST0001");
			//return "Error en el proceso de Estados de cuenta. " + e.getMessage();
		} finally {
			System.out.println("PagosIFNBBean::obtenerEstadosDeCuentaSirac (S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			/*if(exito){
				System.out.println("Antes de crearArchivosZip");
				String respuesta = crearArchivosZip(ic_proc_edo_cuenta,vecfecha,3,fechaInicial);
				System.out.println("Despues de crearArchivosZip");
			}*/
		}
	}




	/**
	 * Acepta los pagos guardandolos definitivamente sobre las tablas correspondientes.
	 * @param numProceso Numero de proceso
	 * @param acuse Acuse con los datos necesarios para un acuse de la tabla com_acuse3
	 */
	public void transmitirPagos(String numProceso, Acuse3 acuse)
			throws NafinException {
		AccesoDB con = null;
		boolean exito = true;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (numProceso == null || acuse == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" numProceso=" + numProceso +
					" acuse = " + acuse);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT into com_acuse3 (cc_acuse, ic_producto_nafin," +
					" df_fecha_hora, cg_recibo_electronico, ic_usuario, " +
					" in_monto_mn, in_monto_dl) " +
					" values ('" + acuse.getAcuse() + "',1," +
					" TO_DATE('"+ acuse.getFechaHora() + "','dd/mm/yyyy hh24:mi:ss'), " +
					" '" + acuse.getReciboElectronico() + "'," +
					" '" + acuse.getClaveUsuario() + "'," +
					acuse.getMontoMN() + "," +
					acuse.getMontoDL() + ")";

			con.ejecutaSQL(strSQL);

			strSQL =
					" SELECT EP.IC_ENCABEZADO_PAGO_TMP, " +
					" 	EP.IC_IF, EP.IG_SUCURSAL, " +
					" 	EP.IC_MONEDA, " +
					" 	TO_CHAR(EP.DF_PERIODO_FIN, 'DD/MM/YYYY') as DF_PERIODO_FIN, " +
					" 	TO_CHAR(EP.DF_PROBABLE_PAGO, 'DD/MM/YYYY') as DF_PROBABLE_PAGO, " +
					" 	TO_CHAR(EP.DF_DEPOSITO, 'DD/MM/YYYY') as DF_DEPOSITO, " +
					" 	EP.IG_IMPORTE_DEPOSITO, " +
					" 	EP.IC_FINANCIERA, " +
					" 	EP.CG_REFERENCIA_BANCO, " +
					" 	EP.CG_NOMBRE_ARCHIVO, " +
					"	EP.ic_referencia_inter"+
					" FROM comtmp_encabezado_pago ep " +
					" WHERE ic_proc = " + numProceso +
					" ORDER BY ic_encabezado_pago_tmp ";

			//System.out.println("GEAG::" + strSQL);
			ResultSet rs = con.queryDB(strSQL);
			String ic_encabezado_pago_tmp = "";
			while (rs.next()) {	//Encabezado
				String strSQL1 = "SELECT " +
						" CASE WHEN MAX(ic_encabezado_pago) is null THEN 1 " +
						" ELSE MAX(ic_encabezado_pago) + 1 " +
						" END " +
						" FROM com_encabezado_pago ";
				ResultSet rs1 = con.queryDB(strSQL1);
				rs1.next();
				String ic_encabezado_pago = rs1.getString(1);
				rs1.close();
				con.cierraStatement();

				ic_encabezado_pago_tmp = rs.getString("IC_ENCABEZADO_PAGO_TMP");

				EncabezadoPagoIFNB encabezado = new EncabezadoPagoIFNB();
				encabezado.setNombreArchivo(rs.getString("CG_NOMBRE_ARCHIVO"));
				encabezado.setClaveIF(rs.getString("IC_IF"));
				encabezado.setClaveDirEstatal(rs.getString("IG_SUCURSAL"));
				encabezado.setClaveMoneda(rs.getString("IC_MONEDA"));
				encabezado.setBancoServicio(rs.getString("IC_FINANCIERA"));
				encabezado.setFechaVencimiento(rs.getString("DF_PERIODO_FIN"));
				encabezado.setFechaProbablePago(rs.getString("DF_PROBABLE_PAGO"));
				encabezado.setFechaDeposito(rs.getString("DF_DEPOSITO"));
				encabezado.setImporteDeposito(rs.getString("IG_IMPORTE_DEPOSITO"));
				encabezado.setReferenciaBanco(rs.getString("CG_REFERENCIA_BANCO"));
				encabezado.setReferenciaInter(rs.getString("IC_REFERENCIA_INTER"));
				encabezado.setAcuse(acuse.getAcuse());

				String strSQLInsertEncabezado = encabezado.getSQLInsert(ic_encabezado_pago);
				//System.out.println("GEAG::" + strSQLInsertEncabezado);

				con.ejecutaSQL(strSQLInsertEncabezado);

				String strSQLDet =
						" SELECT ig_subaplicacion, ig_prestamo, ig_cliente, fg_amortizacion, fg_interes,"   +
						"        fg_subsidio, fg_comision, fg_iva, fg_totalexigible, cg_concepto_pago,"   +
						"        cg_origen_pago, fg_interes_mora, ig_sucursal, cg_acreditado,"   +
						"        TO_CHAR (df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
						"        TO_CHAR (df_vencimiento, 'dd/mm/yyyy') AS df_vencimiento,"   +
						"        fn_saldo_insoluto, TO_CHAR (df_pago, 'dd/mm/yyyy') AS df_pago, ig_tasa,"   +
						"        ig_dias"   +
						"   FROM comtmp_detalle_pago"  +
						" WHERE " +
						" 	ic_encabezado_pago_tmp = " + ic_encabezado_pago_tmp +
						" ORDER BY ic_encabezado_pago_tmp ";
				//System.out.println("GEAG::Detalle Pago TMp: " + strSQLDet);
				ResultSet rsDet = con.queryDB(strSQLDet);

				while (rsDet.next()) {	//Detalle
					String strSQL2 =
							" SELECT " +
							" CASE WHEN MAX(ic_detalle_pago) is null THEN 1 " +
							" ELSE MAX(ic_detalle_pago) + 1 " +
							" END " +
							" FROM com_detalle_pago ";
					ResultSet rs2 = con.queryDB(strSQL2);
					rs2.next();
					String ic_detalle_pago = rs2.getString(1);
					rs2.close();
					con.cierraStatement();

					DetallePagoIFNB detalle = new DetallePagoIFNB();
					if("NB".equals(strTipoBanco)) {
//System.out.println("ENTRO NB");
						detalle.setSubaplicacion(rsDet.getString("ig_subaplicacion"));
						detalle.setPrestamo(rsDet.getString("ig_prestamo"));
						detalle.setClaveSiracCliente(rsDet.getString("ig_cliente"));
						detalle.setCapital(rsDet.getString("fg_amortizacion"));
						detalle.setIntereses(rsDet.getString("fg_interes"));
						detalle.setMoratorios(rsDet.getString("fg_interes_mora"));
						detalle.setSubsidio(rsDet.getString("fg_subsidio"));
						detalle.setComision(rsDet.getString("fg_comision"));
						detalle.setIVA(rsDet.getString("fg_iva"));
						detalle.setImportePago(rsDet.getString("fg_totalexigible"));
						detalle.setConceptoPago(rsDet.getString("cg_concepto_pago"));
						detalle.setOrigenPago(rsDet.getString("cg_origen_pago"));
					}  else if("B".equals(strTipoBanco)) {
//System.out.println("ENTRO B");
/*
						System.out.println(rsDet.getString("ig_sucursal"));
						System.out.println(rsDet.getString("ig_subaplicacion"));
						System.out.println(rsDet.getString("ig_prestamo"));
						System.out.println(rsDet.getString("cg_acreditado"));
						System.out.println(rsDet.getString("df_operacion"));
						System.out.println(rsDet.getString("df_vencimiento"));
						System.out.println(rsDet.getString("df_pago"));
						System.out.println(rsDet.getString("fn_saldo_insoluto"));
						System.out.println(rsDet.getString("ig_tasa"));
						System.out.println(rsDet.getString("fg_amortizacion"));
						System.out.println(rsDet.getString("ig_dias"));
						System.out.println(rsDet.getString("fg_interes"));
						System.out.println(rsDet.getString("fg_comision"));
						System.out.println(rsDet.getString("fg_iva"));
						System.out.println(rsDet.getString("fg_totalexigible"));
*/

						detalle.setSucursal(rsDet.getString("ig_sucursal"));
						detalle.setSubaplicacion(rsDet.getString("ig_subaplicacion"));
						detalle.setPrestamo(rsDet.getString("ig_prestamo"));
						detalle.setAcreditado(rsDet.getString("cg_acreditado"));
						detalle.setFechaOperacion(rsDet.getString("df_operacion"));
						detalle.setFechaVencimiento(rsDet.getString("df_vencimiento"));
						detalle.setFechaPago(rsDet.getString("df_pago"));
						detalle.setSaldoInsoluto(rsDet.getString("fn_saldo_insoluto"));
						detalle.setTasa(rsDet.getString("ig_tasa"));
						detalle.setCapital(rsDet.getString("fg_amortizacion"));
						detalle.setDias(rsDet.getString("ig_dias"));
						detalle.setIntereses(rsDet.getString("fg_interes"));
						detalle.setComision(rsDet.getString("fg_comision"));
						detalle.setIVA(rsDet.getString("fg_iva"));
						detalle.setImportePago(rsDet.getString("fg_totalexigible"));
					}

					String strSQLInsertDetalle = detalle.getSQLInsert(ic_detalle_pago, ic_encabezado_pago);
					//System.out.println("GEAG::" + strSQLInsertDetalle);

					con.ejecutaSQL(strSQLInsertDetalle);
				} //fin del ciclo del detalle
				rsDet.close();
				con.cierraStatement();
			} // fin del ciclo del encabezado
			rs.close();
			con.cierraStatement();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	/**
	 * Borra los datos de encabezados y detalles de las tablas temporales
	 * @param numProceso Numero de proceso de la carga masiva
	 *
	 */
	public void borrarPagosCargadosTmp(String numProceso) {
		AccesoDB con = null;
		boolean exito = true;

		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "DELETE FROM comtmp_detalle_pago " +
					" WHERE ic_encabezado_pago_tmp IN " +
					" 	(SELECT ic_encabezado_pago_tmp " +
					" 	FROM comtmp_encabezado_pago " +
					" 	WHERE ic_proc = " + numProceso + ")";
			con.ejecutaSQL(strSQL);

			strSQL = "DELETE FROM comtmp_encabezado_pago " +
					" 	WHERE ic_proc = " + numProceso;
			con.ejecutaSQL(strSQL);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			exito = false;
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	/**
	 * Elimina los datos pertenecientes al proceso(s) especificado(s)
	 *
	 * @param icProcVenc Arreglo de claves de proceso. (edo de cuenta, vencimientos u operados)
	 * @param tipoproceso Tipo de proceso E - Estados de cuenta, V vencimientos, O operados.
	 *
	 */

	public void cancelarProceso(List icProc, String tipoProceso, String numRegistros)
		throws NafinException{
		System.out.println("PagosIFNBBean::cancelarProceso (E)");


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (icProc == null || icProc.size() == 0 || tipoProceso == null) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
			if (!tipoProceso.equals("E") && !tipoProceso.equals("V") &&
					!tipoProceso.equals("O")) {
				throw new Exception("El tipo de proceso no es v�lido");
			}

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " +
					e.getMessage() + "\n" +
					" icProc=" + icProc + "\n" +
					" tipoProceso = " + tipoProceso);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		AccesoDB con			= null;
		CallableStatement cs=null; //-- CMO --//
		boolean resultado = true;

		try{
			con = new AccesoDB();
			con.conexionDB();

			//-- Modificacion para que se elimine la informacion por bloques a traves de un SP --//
      for(int i=0;i<icProc.size();i++){
        cs = con.ejecutaSP("SP_CANC_PROCESO(?,?,?)");
        //cs.setInt(1, (int)icProc);
        cs.setString(1,(String)icProc.get(i));
        cs.setString(2, tipoProceso);
        cs.setString(3, numRegistros);
        cs.execute();
        if (cs !=null) cs.close();
        con.cierraStatement();
      }

		}catch(Exception e){
			resultado = false;
			System.out.println("PagosIFNBBean::cancelarProceso Exception " + e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("PagosIFNBBean::cancelarProceso (S)");
		}
	}





	/**
	 * Obtiene los datos de los procesos de acuerdo a las fechas establecidas y
	 * el tipo de proceso
	 * @param fechaDe Fecha inicial del rango
 	 * @param fechaA Fecha final del rango
 	 * @param tipoProceso Tipo de proceso:
 	 * 		V Vencimientos, O Operados, E Estados de Cuenta
	 * @param csTipo Tipo de IF ("B", "NB")
	 * @return Lista de listas, que contienen la informaci�n del proceso:
	 *  0. Fecha del proceso
	 * 	1. Hora del proceso
	 * 	2. Clave del proceso
	 * 	3. Numero de Registros del proceso
	 * 	4. N�mero de Ifs, involucrados en el proceso
	 *	5. Dependiendo de V,O y E puede ser Fecha Probable de Pago, Fecha de Operaci�n o Fecha Fin de Mes
	 *	6. Moneda Nacional
	 *	7. Dolar
	 */
	public List consultarProcesos(String fechaDe, String fechaA,
			String tipoProceso, String csTipo) throws NafinException {
			return consultarProcesos(fechaDe, fechaA, "", "", tipoProceso, csTipo);

	}

		/**
	 * Obtiene los datos de los procesos de acuerdo a las fechas establecidas y
	 * el tipo de proceso
	 * @param fechaDe Fecha inicial del rango de ejecucion del proceso
 	 * @param fechaA Fecha final del rango de ejecucion del proceso
	 * @param fechaCargaDe Fecha inicial de carga del proceso
 	 * @param fechaCargaA Fecha final de carga del proceso
 	 * @param tipoProceso Tipo de proceso:
 	 * 		V Vencimientos, O Operados, E Estados de Cuenta
	 * @param csTipo Tipo de IF ("B", "NB")
	 * @return Lista de listas, que contienen la informaci�n del proceso:
	 *  0. Fecha del proceso
	 * 	1. Hora del proceso
	 * 	2. Clave del proceso
	 * 	3. Numero de Registros del proceso
	 * 	4. N�mero de Ifs, involucrados en el proceso
	 *	5. Dependiendo de V,O y E puede ser Fecha Probable de Pago, Fecha de Operaci�n o Fecha Fin de Mes
	 *	6. Moneda Nacional
	 *	7. Dolar
	 */
	public List consultarProcesos(String fechaDe, String fechaA,
			String fechaProcDe, String fechaProcA,
			String tipoProceso, String csTipo) throws NafinException {

		System.out.println("PagosIFNBBean::consultarProcesos (E)");

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (tipoProceso == null || csTipo == null) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
			if (!tipoProceso.equals("E") && !tipoProceso.equals("V") &&
					!tipoProceso.equals("O")) {
				throw new Exception("El tipo de proceso no es v�lido");
			}

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " +
					e.getMessage() + "\n" +
					" tipoProceso = " + tipoProceso + "\n" +
					" csTipo = " + csTipo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		AccesoDB con			= null;
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		StringBuffer qrySentencia = new StringBuffer();
		List vecFilas			= new ArrayList();
		List vecColumnas		= null;

		String postfijo = "";
		String condTipoProc = "";
		if (tipoProceso.equals("O")) {
			postfijo = "operado";
			condTipoProc = "df_operacion";
		} else if (tipoProceso.equals("V")) {
			postfijo = "vencimiento";
			condTipoProc = "com_fechaprobablepago";
		} else if (tipoProceso.equals("E")) {
			postfijo = "edo_cuenta";
			condTipoProc = "df_fechafinmes";
		}
		System.out.print("condTipoProc= "+condTipoProc);

		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia.append(
					" SELECT to_char(df_fecha_hora,'dd/mm/yyyy') as fecha, "+
					" to_char(df_fecha_hora,'HH:MI') as hora,"+
					//" ,cg_nombre_archivo"+
					" ic_proc_" + postfijo +
					" ,ig_num_registros, ig_num_ifs, to_char("+condTipoProc+",'dd/mm/yyyy') as fechaOVFM " +
					" ,ig_num_reg_mn, ig_num_reg_dl, cg_nombre_archivo");
			if (tipoProceso.equals("V")){
				qrySentencia.append(", to_char(df_periodofin,'dd/mm/yyyy') as fechaFin");
			}
			qrySentencia.append(" FROM comhis_proc_" + postfijo +
					" WHERE cs_tipo = ? ");
			if(!"".equals(fechaDe)&&!"".equals(fechaA)){
				qrySentencia.append(" AND df_fecha_hora >= to_date(?,'dd/mm/yyyy') " +
						" AND df_fecha_hora < to_date(?,'dd/mm/yyyy') + 1 ");
			}
			if(!"".equals(fechaProcDe)&&!"".equals(fechaProcA)){
				qrySentencia.append(" AND "+condTipoProc+" >= to_date(?,'dd/mm/yyyy') " +
						" AND "+condTipoProc+" < to_date(?,'dd/mm/yyyy') + 1 ");
			}
			qrySentencia.append(" ORDER BY df_fecha_hora desc ");
			System.out.println("QUERY***= "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia.toString());
			int parametro = 1;

			ps.setString(parametro,csTipo);
			parametro ++;
			if(!"".equals(fechaDe)&&!"".equals(fechaA)){
				ps.setString(parametro,fechaDe);
				parametro ++;
				ps.setString(parametro,fechaA);
				parametro ++;
			}
			if(!"".equals(fechaProcDe)&&!"".equals(fechaProcA)){
				ps.setString(parametro,fechaProcDe);
				parametro ++;
				ps.setString(parametro,fechaProcA);
				parametro ++;
			}
			rs = ps.executeQuery();
			while(rs.next()){
				System.out.println("vecColumnas***");
				vecColumnas = new ArrayList();
/*0*/			vecColumnas.add((rs.getString("fecha")==null)?"":rs.getString("fecha"));
/*1*/			vecColumnas.add((rs.getString("hora")==null)?"":rs.getString("hora"));
/*2*/			vecColumnas.add((rs.getString("ic_proc_" + postfijo)==null)?"":rs.getString("ic_proc_" + postfijo));
/*3*/			vecColumnas.add((rs.getString("ig_num_registros")==null)?"":rs.getString("ig_num_registros"));
/*4*/			vecColumnas.add((rs.getString("ig_num_ifs")==null)?"":rs.getString("ig_num_ifs"));
/*5*/			vecColumnas.add((rs.getString("fechaOVFM")==null)?"":rs.getString("fechaOVFM"));
/*6*/			vecColumnas.add((rs.getString("ig_num_reg_mn")==null)?"":rs.getString("ig_num_reg_mn"));
/*7*/			vecColumnas.add((rs.getString("ig_num_reg_dl")==null)?"":rs.getString("ig_num_reg_dl"));
/*8*/			vecColumnas.add((rs.getString("cg_nombre_archivo")==null)?"":rs.getString("cg_nombre_archivo"));
				if (tipoProceso.equals("V")){
/*9*/				vecColumnas.add((rs.getString("fechaFin")==null)?"":rs.getString("fechaFin"));
				}			
				vecFilas.add(vecColumnas);
			}
		}catch(Exception e){
			System.out.println("PagosIFNBBean::consultarProcesos Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("PagosIFNBBean::consultarProcesos (S)");
		}
		System.out.println("vecFilas "+vecFilas);
		return vecFilas;
	}





	private String getDiasInhabiles(AccesoDB con) throws NafinException {
	String sDiaMesInhabiles = "";
		try{
			String query = "select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null ";
			ResultSet rs = con.queryDB(query);
			while(rs.next())
				sDiaMesInhabiles += rs.getString(1).trim()+";";

			con.cierraStatement();
		}
		catch(Exception e) {
			System.out.println("Exception en getDiasInhabiles. "+e);
			throw new NafinException("SIST0001");
		}
	return sDiaMesInhabiles;
	}


	private Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil, AccesoDB lodbConexion)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		boolean lbExisteDiaInhabil = false;
		ResultSet lrsSel = null;
		Vector vFecha = new Vector();

		try {
			String lsCadenaSQL = "select * from comcat_dia_inhabil"+
								" where cg_dia_inhabil = '"+esFechaAplicacion.substring(0,5)+"'";
			try {
				lrsSel = lodbConexion.queryDB(lsCadenaSQL);
				if(lrsSel.next()) {
					lbExisteDiaInhabil = true;
					cFechaSigHabil.add(Calendar.DATE, 1);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
				} else {
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) { // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
						lbExisteDiaInhabil = true;
					} else if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) { // Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
						lbExisteDiaInhabil = true;
					} else
						lbExisteDiaInhabil = false;
				}

				lrsSel.close();
				lodbConexion.cierraStatement();

				vFecha.addElement(new Boolean(lbExisteDiaInhabil));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.YEAR)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.MONTH)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.DAY_OF_MONTH)));

			} catch (Exception error){
				throw new NafinException("DESC0041");
			}

			return vFecha;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");
		} finally {
			if(!lbOK) throw new NafinException(lsCodError);
			System.out.println(" PagosIFNBBeanEJB::bEsDiaInhabil(S)");
		}
	} // Fin bEsDiaInhabil


/********************** Metodos privados ************************/

	/**
	 * Realiza las validaciones necesarias a los campos del encabezado.
	 * @param mensajesDeError StringBuffer donde se almacenan los errores
	 * 		que se puedan presentar al relizar la validaci�n.
	 * @param numLinea Numero de linea dentro del archivo de carga masiva
	 * 		donde se ubica el detalle a validar
	 * @param strClaveIF Clave del IF
	 * @param strClaveDirEstatal Clave de la direccion estatal
	 * @param strClaveMoneda Clave de la moneda
	 * @param strBancoServicio Clave del Banco de servicio
	 * @param strFechaVencimiento Fecha de vencimiento
	 * @param strFechaProbablePago Fecha probable de pago
	 * @param strFechaDeposito Fecha de deposito
	 * @param strImporteDeposito Importe de deposito
	 * @param strReferenciaBanco Referencia de Banco
	 * @return true si no existio ningun problema en la validaci�n o false
	 * 		de lo contrario
	 */
	private Hashtable validarEncabezado(StringBuffer mensajesDeError, int numLinea,
			String strClaveIF, String strClaveDirEstatal,
			String strClaveMoneda, String strBancoServicio,
			String strFechaVencimiento, String strFechaProbablePago,
			String strFechaDeposito, String strImporteDeposito,
			String strReferenciaBanco, String strReferenciaInter,AccesoDB con) throws NafinException {

	String strSQL = "";
	ResultSet rs = null;
	Hashtable htResultados = new Hashtable();
	boolean registroValido = true;
	String mensajeLineaError = 	"\nError en la linea " + numLinea + ": ";
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");

		try {

			//Clave IF
			if (strClaveIF.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La clave del IF es un campo requerido");
				registroValido = false;
			} else {
				if (!Comunes.esNumero(strClaveIF)) {
					mensajesDeError.append(mensajeLineaError + " La clave del IF debe ser numerica");
					registroValido = false;
				} else {
					strSQL = "SELECT count(*) FROM COMCAT_IF WHERE IC_IF = " + strClaveIF + "";
					rs = con.queryDB(strSQL);
					rs.next();
					int numRegistros = rs.getInt(1);
					rs.close();
					con.cierraStatement();
					if (numRegistros == 0) {
						mensajesDeError.append(mensajeLineaError + " La clave del IF no existe");
						registroValido = false;
					}
				}
			}

			//Clave Direccion Estatal
			if (strClaveDirEstatal.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la direccion estatal es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strClaveDirEstatal)) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la Direcci�n Estatal debe ser numerica");
				registroValido = false;
			} else if (strClaveDirEstatal.length() > 5) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la Direcci�n Estatal excede la longitud permitida");
				registroValido = false;
			}

			//Clave moneda
			if (strClaveMoneda.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La clave de la moneda es un campo requerido");
				registroValido = false;
			} else {
				if (!Comunes.esNumero(strClaveMoneda)) {
					mensajesDeError.append(mensajeLineaError + " La clave de la moneda debe ser numerica");
					registroValido = false;
				} else {
					strSQL = "SELECT count(*) FROM COMCAT_MONEDA WHERE IC_MONEDA = " +
							strClaveMoneda + "";
					rs = con.queryDB(strSQL);
					rs.next();
					int numRegistros = rs.getInt(1);
					rs.close();
					con.cierraStatement();
					if (numRegistros == 0) {
						mensajesDeError.append(mensajeLineaError + " La clave de la moneda no existe");
						registroValido = false;
					}
				}
			}


			//Banco de servicio
			if (strBancoServicio.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La clave del banco de servicio es un campo requerido");
				registroValido = false;
			} else {
				if (!Comunes.esNumero(strBancoServicio)) {
					mensajesDeError.append(mensajeLineaError + " La clave del banco de servicio debe ser numerica");
					registroValido = false;
				} else {
					strSQL = "SELECT count(*) FROM COMCAT_FINANCIERA WHERE IC_FINANCIERA = " +
							strBancoServicio + "";
					rs = con.queryDB(strSQL);
					rs.next();
					int numRegistros = rs.getInt(1);
					rs.close();
					con.cierraStatement();
					if (numRegistros == 0) {
						mensajesDeError.append(mensajeLineaError + " La clave del banco de servicio no existe");
						registroValido = false;
					}
				}
			}

			if("NB".equals(strTipoBanco)) {
				//Fecha de vencimiento
				if (strFechaVencimiento.equals("")) {
					mensajesDeError.append(mensajeLineaError +
							" La fecha de vencimiento es un campo requerido");
					registroValido = false;
				} else if(!Comunes.checaFecha(strFechaVencimiento)) {
					mensajesDeError.append(mensajeLineaError +
							" La fecha de vencimiento no es una fecha valida");
					registroValido = false;
				}
			}

			//Fecha Probable de pago
			if (strFechaProbablePago.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La fecha probable de pago es un campo requerido");
				registroValido = false;
			} else {
				if(!Comunes.checaFecha(strFechaProbablePago)) {
					mensajesDeError.append(mensajeLineaError +
						" La fecha probable de pago no es una fecha valida");
					registroValido = false;
				} else {
					int iAnio = Integer.parseInt(strFechaProbablePago.substring(6,10));
					int iMes = Integer.parseInt(strFechaProbablePago.substring(3,5))-1;
					int iDia = Integer.parseInt(strFechaProbablePago.substring(0,2));
					Calendar cFechaSigHabil = new GregorianCalendar(iAnio, iMes, iDia);

					Vector vFecha = bEsDiaInhabil(strFechaProbablePago, cFechaSigHabil, con);
					boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
					boolean bFechaInhabil = bInhabil;

					cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
					while(bInhabil) {
						vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
						bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
						if(!bInhabil)
							break;
						cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
					}

					if(bFechaInhabil) {
						mensajesDeError.append(mensajeLineaError +
							" La fecha probable de pago "+strFechaProbablePago+" cae en d�a inh�bil, deber�a de ser: "+sdf.format(cFechaSigHabil.getTime()));
						registroValido = false;
					}

				}
			}

			//Fecha de deposito
			if (strFechaDeposito.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La fecha de deposito es un campo requerido");
				registroValido = false;
			} else {
				if(!Comunes.checaFecha(strFechaDeposito)) {
					mensajesDeError.append(mensajeLineaError +
						" La fecha de deposito no es una fecha valida");
					registroValido = false;
				} else {
					java.util.Date dFechaDeposito = Comunes.parseDate(strFechaDeposito);
					/* Validacion de la Fecha que sea un dia habil. */
					String sLineaDiaMes = getDiasInhabiles(con);
					if (sLineaDiaMes.length() > 0) {
						StringTokenizer st = new StringTokenizer(sLineaDiaMes,";");
						while(st.hasMoreElements()) {
							String sDiaMesInhabil = st.nextToken();
							if(strFechaDeposito.substring(0,5).equals(sDiaMesInhabil)) {
								mensajesDeError.append(mensajeLineaError + ", la fecha de deposito "+sdf.format(dFechaDeposito)+" del archivo, no es una fecha en d&iacute;a h&aacute;bil en el catalogo de d�as inh�biles. \n");
								registroValido = false;
							}
						}
					}
					/* Validacion de que la fecha si es un S�bado o Domingo. */
					Calendar calFecha = new GregorianCalendar();
					calFecha.setTime(dFechaDeposito);
					int noDiaSemana = calFecha.get(Calendar.DAY_OF_WEEK);
					if(noDiaSemana == 7 || noDiaSemana == 1) { // 7 Sabado y 1 Domingo.
						mensajesDeError.append(mensajeLineaError + ", la fecha de deposito "+sdf.format(dFechaDeposito)+" del archivo, no es una fecha en d&iacute;a h&aacute;bil. \n");
						registroValido = false;
					}

				}//else
			}

			//Importe del deposito
			if (strImporteDeposito.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El importe del deposito es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strImporteDeposito)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe del deposito debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strImporteDeposito,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe del deposito debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Referencia Banco
			if (strReferenciaBanco.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La clave de Referencia Banco es un campo requerido");
				registroValido = false;
			} else if (strReferenciaBanco.length() > 15) {
				mensajesDeError.append(mensajeLineaError +
						" La Referencia de Banco excede la longitud permitida");
				registroValido = false;
			}
			if(strReferenciaInter.equals("")){
				mensajesDeError.append(mensajeLineaError+ " La referencia del intermediario es un campo requerido");
				registroValido = false;
			}else{
				strSQL = "SELECT ic_referencia_inter FROM COMCAT_referencia_inter "+
						" WHERE cg_descripcion = '" + strReferenciaInter + "'";
				rs = con.queryDB(strSQL);
				String ic_referencia_inter = "";
				if(rs.next()){
					ic_referencia_inter = rs.getString("IC_REFERENCIA_INTER");
				}
				rs.close();
				con.cierraStatement();
				if ("".equals(ic_referencia_inter)||ic_referencia_inter==null) {
						mensajesDeError.append(mensajeLineaError + " La referencia del intermediario no existe");
						registroValido = false;
				}
			}

			htResultados.put("registroValido", new Boolean(registroValido));
			htResultados.put("strFechaProbablePago", strFechaProbablePago);

		} catch(SQLException e) {
			System.out.println("Error de SQL: " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(Exception e) {
			e.printStackTrace();
		}
	return htResultados;
	}


	/**
	 * Realiza las validaciones de los campos del detalle
	 * @param mensajesDeError StringBuffer donde se almacenan los errores
	 * 		que se puedan presentar al relizar la validaci�n.
	 * @param numLinea Numero de linea dentro del archivo de carga masiva
	 * 		donde se ubica el detalle a validar
	 * @param strSubaplicacion Subaplicacion
	 * @param strPrestamo Numero de Prestamo
	 * @param strClaveSiracCliente  Clave de cliente Sirac
	 * @param strCapital Capital
	 * @param strIntereses Intereses
	 * @param strMoratorios Intereses Moratorios
	 * @param strSubsidio Subsidio
	 * @param strComision Comision
	 * @param strIVA IVA
	 * @param strImportePago Importe de pago
	 * @param strConceptoPago Concepto de pago
	 * @param strOrigenPago Origen de pago
	 * @return true si no existio ningun problema en la validaci�n o false
	 * 		de lo contrario
	 */

	private boolean validarDetalle(StringBuffer mensajesDeError, int numLinea,
			String strSubaplicacion, String strPrestamo,
			String strClaveSiracCliente, String strCapital,
			String strIntereses, String strMoratorios,
			String strSubsidio, String strComision,
			String strIVA, String strImportePago,
			String strConceptoPago, String strOrigenPago)  throws NafinException {

		AccesoDB con = new AccesoDB();
		boolean registroValido = true;
		String mensajeLineaError = 	"\nError en la linea " + numLinea + ": ";

		try {
			con.conexionDB();
			//Subaplicacion
			if (strSubaplicacion.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strSubaplicacion)) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion debe ser numerica");
				registroValido = false;
			} else if (strSubaplicacion.length() > 5) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion excede la longitud permitida");
				registroValido = false;
			}

			//Numero de prestamo
			if (strPrestamo.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strPrestamo)) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo debe ser numerica");
				registroValido = false;
			} else if (strPrestamo.length() > 8) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo excede la longitud permitida");
				registroValido = false;
			}

			//Clave Sirac del Cliente
			if (strClaveSiracCliente.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La clave Sirac del cliente es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strClaveSiracCliente)) {
				mensajesDeError.append(mensajeLineaError +
						" La clave sirac del cliente debe ser numerica");
				registroValido = false;
			} else if (strClaveSiracCliente.length() > 10) {
				mensajesDeError.append(mensajeLineaError +
						" La clave sirac del cliente excede la longitud permitida");
				registroValido = false;
			}

			//Capital
			if (strCapital.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strCapital)) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strCapital,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Intereses
			if (strIntereses.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strIntereses)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strIntereses,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Intereses Moratorios
			if (strMoratorios.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes Moratorio es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strMoratorios)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes Moratorio debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strMoratorios,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes Moratorio debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Subsidio
			if (strSubsidio.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Subsidio es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strSubsidio)) {
				mensajesDeError.append(mensajeLineaError +
						" El Subsidio debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strSubsidio,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Subsidio debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Comision
			if (strComision.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La comision es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strComision)) {
				mensajesDeError.append(mensajeLineaError +
						" La comision debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strComision,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" La comision debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//IVA
			if (strIVA.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strIVA)) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strIVA,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Importe de pago
			if (strImportePago.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strImportePago)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strImportePago,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Concepto de pago
			if (strConceptoPago.equals("")) {
				mensajesDeError.append(mensajeLineaError + " El concepto de pago es un campo requerido");
				registroValido = false;
			} else if (strConceptoPago.length() > 2) {
				mensajesDeError.append(mensajeLineaError +
						" El concepto de pago excede la longitud permitida");
				registroValido = false;
			} else if (!strConceptoPago.equals("AN") &&
					!strConceptoPago.equals("VE") &&
					!strConceptoPago.equals("CV") &&
					!strConceptoPago.equals("CO")) {
				mensajesDeError.append(mensajeLineaError +
						" El concepto de pago no tiene un valor v�lido");
				registroValido = false;
			}

			// Origen de pago
			if (strOrigenPago.equals("")) {
				mensajesDeError.append(mensajeLineaError + " El origen de pago es un campo requerido");
				registroValido = false;
			} else if (strOrigenPago.length() > 2) {
				mensajesDeError.append(mensajeLineaError +
						" El origen de pago excede la longitud permitida");
				registroValido = false;
			} else if (!strOrigenPago.equals("I") &&
					!strOrigenPago.equals("A")) {
				mensajesDeError.append(mensajeLineaError +
						" El origen de pago no tiene un valor v�lido");
				registroValido = false;
			}

			return registroValido;
		} catch(NamingException e) {
			System.out.println("No se encontro el DataSource: " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(SQLException e) {
			System.out.println("Error de SQL: " + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	private boolean validarDetalleB(StringBuffer mensajesDeError, int numLinea,
			String strSucursal, String strSubaplicacion, String strPrestamo,
			String strAcreditado, String strFechaOperacion , String strFechaVencimiento,
			String strFechaPago, String strSaldoInsoluto, String strTasa,
			String strCapital, String strDias, String strIntereses,
			String strComision, String strIVA, String strImportePago)  throws NafinException {

		AccesoDB con = new AccesoDB();
		boolean registroValido = true;
		String mensajeLineaError = 	"\nError en la linea " + numLinea + ": ";

		try {
			con.conexionDB();
			//Sucursal
			if (strSucursal.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la sucursal es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strSucursal)) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la sucursal debe ser numerica");
				registroValido = false;
			} else if (strSucursal.length() > 5) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de la sucursal excede la longitud permitida");
				registroValido = false;
			}

			//Subaplicacion
			if (strSubaplicacion.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strSubaplicacion)) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion debe ser numerica");
				registroValido = false;
			} else if (strSubaplicacion.length() > 5) {
				mensajesDeError.append(mensajeLineaError +
						" La clave de subaplicacion excede la longitud permitida");
				registroValido = false;
			}

			//Numero de prestamo
			if (strPrestamo.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strPrestamo)) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo debe ser numerica");
				registroValido = false;
			} else if (strPrestamo.length() > 8) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de prestamo excede la longitud permitida");
				registroValido = false;
			}

			//Acreditado
			if (strAcreditado.equals("")) {
				mensajesDeError.append(mensajeLineaError + " El acreditado es un campo requerido");
				registroValido = false;
			} else if (strAcreditado.length() > 150) {
				mensajesDeError.append(mensajeLineaError +
						" El acreditado excede la longitud permitida");
				registroValido = false;
			}

			//Fecha de Operacion
			if (strFechaOperacion.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La fecha de operacion es un campo requerido");
				registroValido = false;
			} else if (strFechaOperacion.length() > 10) {
				mensajesDeError.append(mensajeLineaError +
						" La fecha de operacion excede la longitud permitida");
				registroValido = false;
			}

			//Fecha de Vencimiento
			if (strFechaVencimiento.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La fecha de vencimiento es un campo requerido");
				registroValido = false;
			} else if (strFechaVencimiento.length() > 10) {
				mensajesDeError.append(mensajeLineaError +
						" La fecha de vencimiento excede la longitud permitida");
				registroValido = false;
			}

			//Fecha Pago
			if (strFechaPago.equals("")) {
				mensajesDeError.append(mensajeLineaError + " La fecha de pago es un campo requerido");
				registroValido = false;
			} else if (strFechaPago.length() > 10) {
				mensajesDeError.append(mensajeLineaError +
						" La fecha de pago excede la longitud permitida");
				registroValido = false;
			}

			//Saldo Insoluto
			if (strSaldoInsoluto.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El saldo insoluto es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strSaldoInsoluto)) {
				mensajesDeError.append(mensajeLineaError +
						" El saldo insoluto debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strSaldoInsoluto,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El saldo insoluto debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Tasa
			if (strTasa.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La tasa es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strTasa)) {
				mensajesDeError.append(mensajeLineaError +
						" La tasa debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strTasa,11,4)) {
				mensajesDeError.append(mensajeLineaError +
						" La tasa debe tener maximo 7 digitos enteros y 4 decimales");
				registroValido = false;
			}

			//Capital
			if (strCapital.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strCapital)) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strCapital,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Capital debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Dias
			if (strDias.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de dias es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esNumero(strDias)) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de dias debe ser numerica");
				registroValido = false;
			} else if (strDias.length() > 5) {
				mensajesDeError.append(mensajeLineaError +
						" El numero de dias excede la longitud permitida");
				registroValido = false;
			}

			//Intereses
			if (strIntereses.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strIntereses)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strIntereses,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El Interes debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Comision
			if (strComision.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" La comision es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strComision)) {
				mensajesDeError.append(mensajeLineaError +
						" La comision debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strComision,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" La comision debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//IVA
			if (strIVA.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strIVA)) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strIVA,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El IVA debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			//Importe de pago
			if (strImportePago.equals("")) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago es un campo requerido");
				registroValido = false;
			} else if (!Comunes.esDecimal(strImportePago)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago debe ser numerico");
				registroValido = false;
			} else if (!Comunes.precisionValida(strImportePago,19,2)) {
				mensajesDeError.append(mensajeLineaError +
						" El importe de pago debe tener maximo 17 digitos enteros y 2 decimales");
				registroValido = false;
			}

			return registroValido;
		} catch(NamingException e) {
			System.out.println("No se encontro el DataSource: " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(SQLException e) {
			System.out.println("Error de SQL: " + e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	/**
	 * Graba los datos del Pago en tablas temporales.
	 * @param con Conexion
	 * @param numProceso Numero de proceso para distinguir los registros
	 * 		insertados durante esta carga masiva
	 * @param encabezado Encabezado del Pago al IFNB
	 * @param arrDetalles Lista de objetos de DetallePagoIFNB, los cuales contienen
	 * 		los detalles a asociarse al encabezado.
	 */
	private void grabarPagoIFNBTemporal(AccesoDB con, int numProceso,
			EncabezadoPagoIFNB encabezado, List arrDetalles) throws SQLException {

		String	strSQL = "SELECT " +
				" CASE WHEN MAX(IC_ENCABEZADO_PAGO_TMP) is null " +
				" 		THEN 1 ELSE MAX(IC_ENCABEZADO_PAGO_TMP) + 1 " +
				" END " +
				" FROM comtmp_encabezado_pago ";
		ResultSet rs = con.queryDB(strSQL);
		rs.next();
		int ic_encabezado_pago_tmp = rs.getInt(1);
		rs.close();
		con.cierraStatement();

		String condicion = "";
		String sFechaVenc = encabezado.getFechaVencimiento();
		if(sFechaVenc!=null&&!"".equals(sFechaVenc)) {
			condicion =
				", TO_DATE('" + encabezado.getFechaVencimiento() + "', 'DD/MM/YYYY')";
		} else
			condicion = ", NULL";

		strSQL = " INSERT INTO COMTMP_ENCABEZADO_PAGO " +
				" (IC_ENCABEZADO_PAGO_TMP, IC_PROC, " +
				" IC_IF, IG_SUCURSAL," +
				" IC_MONEDA, DF_PERIODO_FIN, " +
				" DF_PROBABLE_PAGO, DF_DEPOSITO, " +
				" IG_IMPORTE_DEPOSITO, " +
				" IC_FINANCIERA, " +
				" CG_NOMBRE_ARCHIVO, CG_REFERENCIA_BANCO,IC_REFERENCIA_INTER) " +
				" VALUES(" + ic_encabezado_pago_tmp +
				"," + numProceso +
				"," + encabezado.getClaveIF() +
				"," + encabezado.getClaveDirEstatal() +
				"," + encabezado.getClaveMoneda() +
				condicion+
				", TO_DATE('" + encabezado.getFechaProbablePago() + "', 'DD/MM/YYYY')" +
				", TO_DATE('" + encabezado.getFechaDeposito() + "', 'DD/MM/YYYY')" +
				"," + encabezado.getImporteDeposito() +
				"," + encabezado.getBancoServicio() +
				", '" + encabezado.getNombreArchivo() + "'" +
				", '" + encabezado.getReferenciaBanco() + "'"+
				", " + encabezado.getReferenciaInter() + ")";
//		System.out.println("GEAG::" + strSQL);
		con.ejecutaSQL(strSQL);


		Iterator it = arrDetalles.iterator();
		while(it.hasNext()) {
			DetallePagoIFNB detalle = (DetallePagoIFNB) it.next();

			strSQL = "SELECT " +
					" CASE WHEN MAX(IC_DETALLE_PAGO_TMP) is null " +
					" 		THEN 1 ELSE MAX(IC_DETALLE_PAGO_TMP) + 1 " +
					" END " +
					" FROM comtmp_detalle_pago ";
			rs = con.queryDB(strSQL);
			rs.next();
			int ic_detalle_pago_tmp = rs.getInt(1);
			rs.close();
			con.cierraStatement();

			if("NB".equals(strTipoBanco)) {
				strSQL =
					" INSERT INTO COMTMP_DETALLE_PAGO " +
					" (IC_DETALLE_PAGO_TMP, IC_ENCABEZADO_PAGO_TMP, " +
					" IG_SUBAPLICACION, IG_PRESTAMO, " +
					" IG_CLIENTE, FG_AMORTIZACION, " +
					" FG_INTERES, FG_SUBSIDIO, " +
					" FG_COMISION, FG_IVA, " +
					" FG_TOTALEXIGIBLE, CG_CONCEPTO_PAGO, " +
					" CG_ORIGEN_PAGO, FG_INTERES_MORA) " +
					" VALUES(" + ic_detalle_pago_tmp +
					"," + ic_encabezado_pago_tmp +
					"," + detalle.getSubaplicacion() +
					"," + detalle.getPrestamo() +
					"," + detalle.getClaveSiracCliente() +
					"," + detalle.getCapital() +
					"," + detalle.getIntereses() +
					"," + detalle.getSubsidio() +
					"," + detalle.getComision() +
					"," + detalle.getIVA() +
					"," + detalle.getImportePago() +
					",'" + detalle.getConceptoPago() + "'" +
					",'" + detalle.getOrigenPago() + "'" +
					"," + detalle.getMoratorios() + ")";
			} else if("B".equals(strTipoBanco)) {
				strSQL =
					" INSERT INTO COMTMP_DETALLE_PAGO " +
					" (IC_DETALLE_PAGO_TMP, IC_ENCABEZADO_PAGO_TMP " +
					" ,IG_SUCURSAL "+
					" ,IG_SUBAPLICACION "+
					" ,IG_PRESTAMO "+
					" ,CG_ACREDITADO "+
					" ,DF_OPERACION "+
					" ,DF_VENCIMIENTO "+
					" ,DF_PAGO "+
					" ,FN_SALDO_INSOLUTO "+
					" ,IG_TASA "+
					" ,FG_AMORTIZACION "+
					" ,IG_DIAS "+
					" ,FG_INTERES "+
					" ,FG_COMISION "+
					" ,FG_IVA "+
					" ,FG_TOTALEXIGIBLE) "+
					" VALUES(" + ic_detalle_pago_tmp +
					"," + ic_encabezado_pago_tmp +
					"," + detalle.getSucursal() +
					"," + detalle.getSubaplicacion() +
					"," + detalle.getPrestamo() +
					",'" + detalle.getAcreditado()+"'" +
					", to_date('" + detalle.getFechaOperacion()+"','dd/mm/yyyy') " +
					", to_date('" + detalle.getFechaVencimiento()+"','dd/mm/yyyy') " +
					", to_date('" + detalle.getFechaPago()+"','dd/mm/yyyy') " +
					"," + detalle.getSaldoInsoluto() +
					"," + detalle.getTasa() +
					"," + detalle.getCapital() +
					"," + detalle.getDias() +
					"," + detalle.getIntereses() +
					"," + detalle.getComision() +
					"," + detalle.getIVA() +
					"," + detalle.getImportePago() +")";

			}

//			System.out.println("GEAG::" + strSQL);
			con.ejecutaSQL(strSQL);
		}//fin while
	}


	/**
 	* Maneja la informaci�n referente a la captura individual de pagos
 	* @author JMPR
 	*/

	/**
	 * Guarda los datos de una carga individual de pagos.
	 * @param acuse Acuse con los datos necesarios para un acuse de la tabla com_acuse3
	 * @param encabezado Encabezado con los datos necesarios para llenar la tabla com_encabezado_pago
	 * @param al ArrayList que contiene los detalles del pago
	 */
	public void capturaIndividualPagos(Acuse3 acuse, EncabezadoPagoIFNB encabezado,
	            ArrayList al) throws NafinException {
		//**********************************Validaci�n de parametros:**************************
		try {
			if (acuse == null || encabezado == null || al == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {//
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" acuse=" + acuse +
					" encabezado=" + encabezado +
					" al = " + al);
			throw new NafinException("SIST0001");
		}
		//*************************************************************************************
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO COM_ACUSE3 (CC_ACUSE,IN_MONTO_MN,IN_MONTO_DL,"+
			                "DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN) "+
			                "VALUES('"+acuse.getAcuse()+"','"+acuse.getMontoMN()+
			                "','"+acuse.getMontoDL()+"',sysdate,'"+acuse.getClaveUsuario()+
			                "','"+acuse.getReciboElectronico()+"',1)";
			//System.out.println("query Acuse3:" + strSQL);
			con.ejecutaSQL(strSQL);


			String strSQL1 = "SELECT " +
						     " CASE WHEN MAX(ic_encabezado_pago) is null THEN 1 " +
						     " ELSE MAX(ic_encabezado_pago) + 1 " +
						     " END " +
						     " FROM com_encabezado_pago ";
			ResultSet rs1 = con.queryDB(strSQL1);
			rs1.next();
			String ic_encabezado_pago = rs1.getString(1);
			rs1.close();
			con.cierraStatement();

			String strSQLInsertEncabezado = encabezado.getSQLInsert(ic_encabezado_pago);
			//System.out.println("Inser Encabezado:" + strSQLInsertEncabezado);
			con.ejecutaSQL(strSQLInsertEncabezado);

			for(int i=0; i < al.size(); i++){
				ArrayList alTemp = new ArrayList();
				alTemp=(ArrayList)al.get(i);
				String strSQL2 = " SELECT " +
							     " CASE WHEN MAX(ic_detalle_pago) is null THEN 1 " +
							     " ELSE MAX(ic_detalle_pago) + 1 " +
							     " END " +
							     " FROM com_detalle_pago ";
				ResultSet rs2 = con.queryDB(strSQL2);
				rs2.next();
				String ic_detalle_pago = rs2.getString(1);
				rs2.close();
				con.cierraStatement();

				DetallePagoIFNB detalle = new DetallePagoIFNB();
				detalle.setSubaplicacion((String)alTemp.get(0));
				detalle.setPrestamo((String)alTemp.get(1));
				detalle.setClaveSiracCliente((String)alTemp.get(2));
				detalle.setCapital((String)alTemp.get(3));
				detalle.setIntereses((String)alTemp.get(4));
				detalle.setMoratorios((String)alTemp.get(5));
				detalle.setSubsidio((String)alTemp.get(6));
				detalle.setComision((String)alTemp.get(7));
				detalle.setIVA((String)alTemp.get(8));
				detalle.setImportePago((String)alTemp.get(9));
				detalle.setConceptoPago((String)alTemp.get(10));
				detalle.setOrigenPago((String)alTemp.get(11));

				String strSQLInsertDetalle = detalle.getSQLInsert(ic_detalle_pago, ic_encabezado_pago);
				//System.out.println("Inserta Detalle:" + strSQLInsertDetalle);
				con.ejecutaSQL(strSQLInsertDetalle);

			} //fin del for
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

	}//fin del metodo capturaIndividualPagos


	public void capturaIndividualPagosIFB(Acuse3 acuse, EncabezadoPagoIFNB encabezado,
	            ArrayList al) throws NafinException {
		//**********************************Validaci�n de parametros:**************************
		try {
			if (acuse == null || encabezado == null || al == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {//
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" acuse=" + acuse +
					" encabezado=" + encabezado +
					" al = " + al);
			throw new NafinException("SIST0001");
		}
		//*************************************************************************************
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO COM_ACUSE3 (CC_ACUSE,IN_MONTO_MN,IN_MONTO_DL,"+
			                "DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN) "+
			                "VALUES('"+acuse.getAcuse()+"','"+acuse.getMontoMN()+
			                "','"+acuse.getMontoDL()+"',sysdate,'"+acuse.getClaveUsuario()+
			                "','"+acuse.getReciboElectronico()+"',1)";
			//System.out.println("query Acuse3:" + strSQL);
			con.ejecutaSQL(strSQL);


			String strSQL1 = "SELECT " +
						     " CASE WHEN MAX(ic_encabezado_pago) is null THEN 1 " +
						     " ELSE MAX(ic_encabezado_pago) + 1 " +
						     " END " +
						     " FROM com_encabezado_pago ";
			ResultSet rs1 = con.queryDB(strSQL1);
			rs1.next();
			String ic_encabezado_pago = rs1.getString(1);
			rs1.close();
			con.cierraStatement();

			String strSQLInsertEncabezado = encabezado.getSQLInsert(ic_encabezado_pago);
			//System.out.println("Inser Encabezado:" + strSQLInsertEncabezado);
			con.ejecutaSQL(strSQLInsertEncabezado);

			for(int i=0; i < al.size(); i++){
				ArrayList alTemp = new ArrayList();
				alTemp=(ArrayList)al.get(i);
				String strSQL2 = " SELECT " +
							     " CASE WHEN MAX(ic_detalle_pago) is null THEN 1 " +
							     " ELSE MAX(ic_detalle_pago) + 1 " +
							     " END " +
							     " FROM com_detalle_pago ";
				ResultSet rs2 = con.queryDB(strSQL2);
				rs2.next();
				String ic_detalle_pago = rs2.getString(1);
				rs2.close();
				con.cierraStatement();

				DetallePagoIFNB detalle = new DetallePagoIFNB();
				detalle.setSucursal((String)alTemp.get(0));
				detalle.setSubaplicacion((String)alTemp.get(1));
				detalle.setPrestamo((String)alTemp.get(2));
				detalle.setAcreditado((String)alTemp.get(3));
				detalle.setFechaOperacion((String)alTemp.get(4));
				detalle.setFechaVencimiento((String)alTemp.get(5));
				detalle.setFechaPago((String)alTemp.get(6));
				detalle.setSaldoInsoluto((String)alTemp.get(7));
				detalle.setTasa((String)alTemp.get(8));
				detalle.setCapital((String)alTemp.get(9));
				detalle.setDias((String)alTemp.get(10));
				detalle.setIntereses((String)alTemp.get(11));
				detalle.setComision((String)alTemp.get(12));
				detalle.setIVA((String)alTemp.get(13));
				detalle.setImportePago((String)alTemp.get(14));
				detalle.setConceptoPago((String)alTemp.get(15));
				detalle.setOrigenPago("I");

				String strSQLInsertDetalle = detalle.getSQLInsert(ic_detalle_pago, ic_encabezado_pago);
				//System.out.println("Inserta Detalle:" + strSQLInsertDetalle);
				con.ejecutaSQL(strSQLInsertDetalle);

			} //fin del for
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

	}//fin del metodo capturaIndividualPagos


	/**
	 * Guarda los datos de la cedula de conciliaci�n
	 * @param cedula Contiene los datos necesarios para dar de alta una cedula
	 */
	public void cedulaConciliacion(CedulaConciliacionIFNB cedula)
	                                  throws NafinException {
		//**********************************Validaci�n de parametros:**************************
		try {
			if (cedula == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {//
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" cedula = " + cedula);
			throw new NafinException("SIST0001");
		}
		//*************************************************************************************
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL1 = "SELECT " +
						     " CASE WHEN MAX(ic_cedula_concilia) is null THEN 1 " +
						     " ELSE MAX(ic_cedula_concilia) + 1 " +
						     " END " +
						     " FROM com_cedula_concilia ";
			ResultSet rs1 = con.queryDB(strSQL1);
			rs1.next();
			String ic_cedula_concilia = rs1.getString(1);
			rs1.close();
			con.cierraStatement();

			String strSQLInsertCedula = cedula.getSQLInsert(ic_cedula_concilia);
			//System.out.println("Inser Cedula:" + strSQLInsertCedula);
			con.ejecutaSQL(strSQLInsertCedula);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

	}//fin del metodo cedulaConciliacion


	/**
	 * Borra los datos de encabezados y detalles de los pagos
	 * @param numEncabezado Numero del encabezado
	 *
	 */
	public void eliminaPagos(String numEncabezado) throws NafinException{
		AccesoDB con = null;
		boolean exito = true;
		System.out.println("PagosIFNBBean::eliminaPagos (E)");
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "DELETE FROM com_detalle_pago " +
					" WHERE ic_encabezado_pago = " + numEncabezado;
			con.ejecutaSQL(strSQL);

			strSQL = "DELETE FROM com_encabezado_pago " +
					" 	WHERE ic_encabezado_pago = " + numEncabezado;
			con.ejecutaSQL(strSQL);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			exito = false;
			System.out.println("PagosIFNBBean::eliminaPagos Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("PagosIFNBBean::eliminaPagos (S)");
		}
	}//fin del metodo eliminaPagos()

	
	public void eliminaCedula(String sClaveIF, String sClaveMoneda,
	                          String sFechaCorte, String sFechaEnvio)
	                          throws NafinException{
		
		eliminaCedula( sClaveIF, sClaveMoneda, sFechaCorte, sFechaEnvio, "", "");
	}
	
	/**
	 * Borra la cedula regitrada
	 * @param sClaveIF Clave del IF
	 * @param sClaveMoneda Clave de la moneda
	 * @param sFechaCorte Fecha de Corte
	 * @param sFechaEnvio Fecha de Envio
	 */
	public void eliminaCedula(String sClaveIF, String sClaveMoneda,
	                          String sFechaCorte, String sFechaEnvio, String tipoLinea, String tipoAfiliado)
	                          throws NafinException{
		AccesoDB con = null;
		boolean exito = true;
		System.out.println("PagosIFNBBean::eliminaCedula (E)");
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			
			if(!"C".equals(tipoLinea)){
				String strSQL = "DELETE FROM com_cedula_concilia " +
						" WHERE ic_if = " + sClaveIF +
						" and ic_moneda = " + sClaveMoneda +
						" and TRUNC(df_fecha_corte) = TO_DATE('"+sFechaCorte+"','DD/MM/YYYY') "+
						" and TRUNC(df_carga) = TO_DATE('"+sFechaEnvio+"','DD/MM/YYYY') " +
						" and in_numero_sirac is null " +
						" and cg_tipo_linea = '" + tipoLinea+"'" ;
				con.ejecutaSQL(strSQL);
			}else
			{
				if(!"CE".equals(tipoAfiliado)){
			String strSQL = "DELETE FROM com_cedula_concilia " +
					" WHERE ic_if = " + sClaveIF +
					" and ic_moneda = " + sClaveMoneda +
					" and TRUNC(df_fecha_corte) = TO_DATE('"+sFechaCorte+"','DD/MM/YYYY') "+
							" and TRUNC(df_carga) = TO_DATE('"+sFechaEnvio+"','DD/MM/YYYY') " +
							" and in_numero_sirac is not null " +
							" and cg_tipo_linea = '" + tipoLinea+"'" ;
			con.ejecutaSQL(strSQL);
				}else
				{
					String strSQL = "DELETE FROM com_cedula_concilia " +
							" WHERE ic_cliente_externo = " + sClaveIF +
							" and ic_moneda = " + sClaveMoneda +
							" and TRUNC(df_fecha_corte) = TO_DATE('"+sFechaCorte+"','DD/MM/YYYY') "+
							" and TRUNC(df_carga) = TO_DATE('"+sFechaEnvio+"','DD/MM/YYYY') " +
							" and in_numero_sirac is not null " +
							" and cg_tipo_linea = '" + tipoLinea+"'" ;
					con.ejecutaSQL(strSQL);
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			exito = false;
			System.out.println("PagosIFNBBean::eliminaCedula Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("PagosIFNBBean::eliminaCedula (S)");
		}
	}//fin del metodo eliminaCedula()


// Foda 029- Modificaci�n de pagos de Cartera 09/05/2005 smj //
	public Vector getEstadoCuenta(
				String df_fechafinmes,
				String ic_moneda,
				String ic_if,
				String txt_num_int)
			throws NafinException {

		return getEstadoCuenta(df_fechafinmes, ic_moneda, ic_if, txt_num_int, "","", "");
	
	}
	public Vector getEstadoCuenta(
				String df_fechafinmes,
				String ic_moneda,
				String ic_if,
				String txt_num_int, String tipoLinea, String tipoAfiliado, String numSirac)
			throws NafinException {
		System.out.println("PagosIFNBean BEAN::getEstadoCuenta (E)");
		AccesoDB con = new AccesoDB();
		String				condicion		= "";
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector 				renglones		= new Vector();
		Vector 				columnas		= null;

		try {
				con.conexionDB();

			if(!"C".equals(tipoLinea)){
			if(!"".equals(ic_if))
    			condicion += "    AND C.ic_if = ? ";
    		if(!"".equals(txt_num_int))
    			condicion += "    AND I.ic_financiera = ?";
			}else
			{
				condicion += "    AND C.ig_cliente is not null ";
				condicion += "    AND C.ic_if = ? ";
				if(!"".equals(numSirac))
					condicion += "    AND C.ig_cliente = ?";
				if("CE".equals(tipoAfiliado))
				{
					condicion += "    AND exists (select 1 from comcat_cli_externo where in_numero_sirac = c.ig_cliente ) ";
				}
			}


			qrySentencia =	"SELECT CG_SUBAPL_DESC TIPO_CREDITO "+
 	  						" ,SUM(C.FG_MONTOOPERADO) MONTO_OPERADO "+
 	  						" ,SUM(C.FG_CAPITALVIGENTE) CAPITAL_VIGENTE "+
 	  						" ,SUM(C.FG_CAPITALVENCIDO) CAPITAL_VENCIDO "+
	  						" ,SUM(C.FG_INTERESPROVI) INTERES_VIGENTE "+
					 	    " ,SUM(C.FG_INTERESVENCIDO) INTERES_VENCIDO "+
					 	    " ,SUM(C.FG_INTERESMORAT + C.FG_SOBRETASAMOR) MORAS "+
					 	    " ,SUM(C.FG_ADEUDOTOTAL) TOTAL_ADEUDO "+
					 	    " ,COUNT(C.IG_PRESTAMO) DESCUENTOS "+
					 	    " ,I.IC_IF ";
					if("".equals(numSirac)){
						qrySentencia +=	" ,I.cg_razon_social";
					}else
					{
						qrySentencia +=	" ,c.cg_nombrecliente as cg_razon_social";
					}
					
					qrySentencia += " FROM COM_ESTADO_CUENTA C "+
						 	"	  ,COMCAT_IF I "+
						 	" WHERE C.IC_IF=I.IC_IF " +
						 	"	  AND TRUNC(C.df_fechafinmes) = TO_DATE(?,'DD/MM/YYYY') "+
						 	"	  AND C.ic_moneda = ? " +
						 			condicion +
						 	" GROUP BY ";
					if("".equals(numSirac)){
						qrySentencia +=	" I.CG_RAZON_SOCIAL ";
					}else
					{
						qrySentencia += " c.cg_nombrecliente ";
					}
					
					qrySentencia += "	  ,CG_SUBAPL_DESC " +
						 	"	  ,I.IC_IF " ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,df_fechafinmes);
			ps.setInt(2,Integer.parseInt(ic_moneda));
			int numparam=3;
			
			if(!"C".equals(tipoLinea)){
			if(!"".equals(ic_if))
				ps.setInt(numparam++,Integer.parseInt(ic_if));
			if(!"".equals(txt_num_int))
				ps.setInt(numparam++,Integer.parseInt(txt_num_int));
			}else
			{
				
				ps.setInt(numparam++,Integer.parseInt("12"));
				if(!"".equals(numSirac))
					ps.setInt(numparam++,Integer.parseInt(numSirac));
			}

			System.out.println("*******QueryConsultaEdo:*********"+ qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();

				while(rs.next()) {
					columnas = new Vector();
					columnas.addElement((rs.getString(1)==null?"":rs.getString(1))); // 0 TIPO_CREDITO
					columnas.addElement((rs.getString(2)==null?"":rs.getString(2))); // 1 MONTO_OPERADO
					columnas.addElement((rs.getString(3)==null?"":rs.getString(3))); // 2 CAPITAL_VIGENTE
					columnas.addElement((rs.getString(4)==null?"":rs.getString(4))); // 3 CAPITAL_VENCIDO
					columnas.addElement((rs.getString(5)==null?"":rs.getString(5))); // 4 INTERES_VIGENTE
					columnas.addElement((rs.getString(6)==null?"":rs.getString(6))); // 5 INTERES_VENCIDO
					columnas.addElement((rs.getString(7)==null?"":rs.getString(7))); // 6 MORAS
					columnas.addElement((rs.getString(8)==null?"":rs.getString(8))); // 7 TOTAL_ADEUDO
					columnas.addElement((rs.getString(9)==null?"":rs.getString(9))); // 8 DESCUENTOS
					columnas.addElement((rs.getString(10)==null?"":rs.getString(10))); // 9 IC_IF
					columnas.addElement((rs.getString(11)==null?"":rs.getString(11))); // 10 nombre IF
					renglones.addElement(columnas);
				}
				rs.close();

			System.out.println("****Vectorenglones*****"+renglones);
			}catch(Exception e){
			System.out.println("PagosIFNBean BEAN::getEstadoCuenta (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("PagosIFNBean BEAN::getEstadoCuenta (E)");
		}
		return renglones;
	}//fin 	Vector getEstadoCuenta


	public void guardaDatosOBS(String df_fechafinmes, String ic_moneda, String ic_if, String cg_observaciones)throws NafinException
	{
		guardaDatosOBS(df_fechafinmes, ic_moneda, ic_if, cg_observaciones, "");
	}
	
	public void guardaDatosOBS(String df_fechafinmes, String ic_moneda, String ic_if, String cg_observaciones, String numSirac)

   	throws NafinException
   	{
		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;
		int 		num					= 0;

		try {
			con = new AccesoDB();
			con.conexionDB();
			// Verifico si existen registros con esas caracteristicas//

	       	qrySentencia =
				"SELECT COUNT (1)"+
				"  FROM COM_OBSERVACIONES_CEDULA"+
				" WHERE trunc(DF_FECHA_CORTE) = trunc(to_date('"+df_fechafinmes+"','dd/mm/yyyy')) "+
				" AND IC_MONEDA = "+ic_moneda;
				
			
			if(!"".equals(numSirac))
			{
				qrySentencia += " AND IC_IF = 12 "+
								" AND IG_CLIENTE = "+numSirac;
			}else
			{
				qrySentencia += " AND IC_IF= "+ic_if;
			}

	        rs = con.queryDB(qrySentencia);
	        System.out.println("Query Select:::"+qrySentencia);
		    if(rs.next())
				num = rs.getInt(1);
			con.cierraStatement();

			// Si existe registro

			System.out.println(":::::::N�mero de registros:::::::" + num);

			if (num>0) {

			System.out.println(":::::::Entra al insert o update:::::::" );

			qrySentencia =	"update COM_OBSERVACIONES_CEDULA " +
							"   set CG_OBSERVACIONES = '" + cg_observaciones + "'"+
							" where trunc(dF_FECHA_CORTE) = trunc(to_date('"+df_fechafinmes+"','dd/mm/yyyy'))"+
								"	AND IC_MONEDA = "+ic_moneda;
								
				if(!"".equals(numSirac))
				{
					qrySentencia += " AND IC_IF = 12 "+
									" AND IG_CLIENTE = "+numSirac;
				}else
				{
					qrySentencia += " AND IC_IF= "+ic_if;
				}

			}

			else {
			//Si no existe
				if(!"".equals(numSirac))
				{
					qrySentencia =	" INSERT INTO COM_OBSERVACIONES_CEDULA( " +
								"    DF_FECHA_CORTE,IC_MONEDA,IC_IF,CG_OBSERVACIONES, IG_CLIENTE)" +
								" VALUES( "+
								" TO_DATE('"+ df_fechafinmes+"','DD/MM/YYYY')" +","+ic_moneda+", 12,'"+cg_observaciones+"'"+","+numSirac+")";
				}else
				{
			qrySentencia =	" INSERT INTO COM_OBSERVACIONES_CEDULA( " +
							"    DF_FECHA_CORTE,IC_MONEDA,IC_IF,CG_OBSERVACIONES)" +
							" VALUES( "+
							" TO_DATE('"+ df_fechafinmes+"','DD/MM/YYYY')" +","+ic_moneda+","+ic_if+",'"+cg_observaciones+"'"+")";
				}
				

			}


			System.out.println(qrySentencia);
			con.ejecutaSQL(qrySentencia);
			//System.out.println(":::::::Sale del insert o update:::::::"+ qrySentencia);

		/*}catch(NafinException ne){
	      	resultado = false;
	      	throw ne;*/
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}

   }//fin-guardaDatosOBS



	public ArrayList getFechasCorte(int iDias, String sSigno, String tipoAfiliado) throws NafinException {
	AccesoDB con = new AccesoDB();
	ArrayList alFechas = new ArrayList();
		try {
			con.conexionDB();
			String sQuery = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.executeUpdate();
			if (ps!=null) ps.close();
			con.terminaTransaccion(true);

			sQuery = "select distinct(to_char(c.df_fechafinmes,'dd/mm/yyyy')) as fechaCorte "+
							"from com_estado_cuenta c, comcat_if i "+
							"where c.ic_if = i.ic_if " +
							"and c.df_fechafinmes < trunc(sigfechahabil(sysdate, ?, ?)) " +
							"and c.df_fechafinmes > trunc(sysdate - 360) ";
			if(tipoAfiliado!=null && !tipoAfiliado.equals(""))
			{
				sQuery += "and i.cs_tipo = ? ";
			}
			
			System.out.println("PagosIFNB.getFecharCorte() Query: " + sQuery);
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, iDias);
			ps.setString(2, sSigno);
			if(tipoAfiliado!=null && !tipoAfiliado.equals(""))
			{
				ps.setString(3, tipoAfiliado);
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				alFechas.add(alFechas.size(), rs.getString(1));
			}
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("PagosIFNBean::getFechasCorte() "+e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("PagosIFNBean BEAN::getEstadoCuenta (E)");
		}
		return alFechas;
	}


	/**
	 * Obtiene las fechas de corte disponibles
	 * @param iDias Dias
	 * @param sSigno Signo
	 * @param icIf Clave del intermediario financiero (comcat_if.ic_if)
	 * @param tipoLinea Tipo de linea (D Descuento, C Cr�dito)
	 * @param tipoAfiliado Tipo de afiliado. B Bancario, NB No Bancario, CE cliente Externo
	 * @param igCliente Cliente
	 * @return Lista con las fechas de corte
	 * @throws com.netro.exception.NafinException Si existe un error
	 */
	public ArrayList getFechasCorte(int iDias, String sSigno,String icIf, 
			String tipoLinea, String tipoAfiliado, String igCliente) throws NafinException {
		log.info("getFechasCorte(E)");
	AccesoDB con = new AccesoDB();
	ArrayList alFechas = new ArrayList();
		try {
			con.conexionDB();
			StringBuffer sQuery = new StringBuffer("ALTER SESSION SET NLS_TERRITORY = MEXICO");
			PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			ps.executeUpdate();
			if (ps!=null) ps.close();
			con.terminaTransaccion(true);

			sQuery = new StringBuffer(); 
			sQuery.append(
					" SELECT DISTINCT(TO_CHAR(c.df_fechafinmes,'dd/mm/yyyy')) AS fechaCorte "+
					" FROM com_estado_cuenta c "+
					" WHERE c.df_fechafinmes < TRUNC(sigfechahabil(SYSDATE, ?, ?)) "+
					" AND c.df_fechafinmes > TRUNC(SYSDATE - 360) "+
					" AND c.ic_if = ?");
			if(tipoLinea!=null && tipoLinea.equals("C")) {
				sQuery.append(" AND c.IG_CLIENTE IS NOT NULL ");
      }
      
			if(igCliente!=null && !igCliente.equals("")) {
				sQuery.append(" AND c.IG_CLIENTE = ? ");
      }
	  
			if(tipoAfiliado!=null && !tipoAfiliado.equals("")) {
		if(tipoAfiliado.equals("CE")){
					sQuery.append(
							" AND EXISTS  (" +
							"    SELECT   1 " +
							"    FROM comcat_cli_externo i " +
							"	  WHERE i.in_numero_sirac = c.IG_CLIENTE	)");
				} else {
					if (tipoLinea != null && tipoLinea.equals("C")) {
						sQuery.append(
								" AND EXISTS (" +
								"    SELECT   1 " +
					"    FROM comcat_if i " +
					"   WHERE i.cs_habilitado = 'S' " +
					"     AND i.cs_tipo = ? " +
					"     AND i.in_numero_sirac IS NOT NULL " +
								"	   AND i.in_numero_sirac = c.IG_CLIENTE )");
					}
		}
	  }
      
      
			log.debug("PagosIFNB.getFecharCorte(ic_if) Query: " + sQuery);
			int param = 3;
			ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, iDias);
			ps.setString(2, sSigno);
			ps.setString(3,icIf);
			if(igCliente!=null && !igCliente.equals("")) {
        ps.setLong(++param,Long.parseLong(igCliente));  
      }
	  
			if(tipoAfiliado != null && !tipoAfiliado.equals("") &&
					!tipoAfiliado.equals("CE") && "C".equals(tipoLinea)) {
		  ps.setString(++param,tipoAfiliado);
	  }
	  
	  
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				alFechas.add(alFechas.size(), rs.getString(1));
			}
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			log.error("getFechasCorte(Exception)",e);
			throw new NafinException("SIST0001");
		}finally{
			log.info("getFechasCorte(S)");
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return alFechas;
	}


	public void setBitacoraPagos(String ic_if, String datosBitacora, String sFechaVencIni, String sFechaVencFin, String sFechaProbIni) throws NafinException {
		String				qrySentencia	= "";
		ResultSet			rs				= null;
		PreparedStatement 	ps				= null;
		AccesoDB con = new AccesoDB();
		String sNombre1="", sFechaDel="";
		try {
			con.conexionDB();
			System.out.println("PagosIFNB::setBitacoraPagos(E) ");
			VectorTokenizer vt = new VectorTokenizer(datosBitacora, "|");
			Vector vecDatos = vt.getValuesVector();
			String cveMoneda = vecDatos.get(0).toString();
			String fechaVenc = vecDatos.get(1).toString();
			String importeDep = vecDatos.get(2).toString();
			if(!sFechaVencIni.equals("") && !sFechaVencFin.equals("")){//BUSQUEDA POR FECHA DE VENCIMIENTO
				sFechaDel=sFechaVencIni;
				sNombre1="V";
			} else { //BUSQUEDA POR FECHA PROBABLE DE PAGO
				sFechaDel=sFechaProbIni;
				sNombre1="P";
			}
		    String sNombre2 = Comunes.rellenaCeros(String.valueOf(ic_if),5);
			DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT,new Locale("ES","MX"));
		  	SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMyy");
		  	String sNombre3 = sdf1.format(df.parse(sFechaDel));
		  	java.util.Date date2 = new java.util.Date();
		  	SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
		  	String sNombre4 = sdf2.format(date2);
			String sLike=sNombre1+sNombre2+sNombre3+sNombre4;
			qrySentencia =
				"SELECT COUNT(1) CONSECUTIVO FROM com_encabezado_pago "+
				" WHERE ic_if = ? AND cg_nombre_archivo LIKE '%"+sLike+"%' ";
			//out.println("sQueryArchivo:"+sQueryArchivo+"<br>");
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, ic_if);
			rs = ps.executeQuery();
			String sConsecutivo="";
			while (rs.next()) {
				sConsecutivo  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
			}//fin while 3
			rs.close();
			if(ps!=null) ps.close();
			String sNombre5= (Integer.parseInt(sConsecutivo)==0)?"-1":"-"+String.valueOf(Integer.parseInt(sConsecutivo)+1);
			String sNombreTemporal="";
			sNombreTemporal = sNombre1+sNombre2+sNombre3+sNombre4+sNombre5+".csv";

			qrySentencia =
				" INSERT INTO bit_consulta_pago"   +
				"             (ic_if, df_consulta_pago, cg_nombre_archivo, ic_moneda, df_vencimiento, fn_total_pagar)"   +
				"      VALUES(?, SYSDATE, ?, ?, to_date(?,'dd/mm/yyyy'), ?)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_if));
			ps.setString(2, sNombreTemporal);
			ps.setInt(3,Integer.parseInt(cveMoneda));
			ps.setString(4, fechaVenc);
			ps.setDouble(5,Double.parseDouble(importeDep));
			ps.executeUpdate();
			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		} catch (Exception e) {
			con.terminaTransaccion(false);
			System.out.println("PagosIFNB::setBitacoraPagos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("PagosIFNB::setBitacoraPagos(S)");
		}
	}//setBitacoraPagos



	public ArrayList consultaDetalles(EncabezadoPagoIFNB encabezado)
		throws NafinException {
		ArrayList al = new ArrayList();
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();


			String qrySentencia =
				" SELECT /*+ use_nl(V) "	+
				"			index(V CP_COM_VENCIMIENTO_PK) */ "	+
				"			V.ig_subaplicaCion SUBAPLICACION"   +
				"			,V.ig_prestamo PRESTAMO"   +
				"			,V.ig_cliente CLAVE_SIRAC"   +
				"			,V.fg_amortizacion CAPITAL"   +
				"			,V.fg_interes INTERESES"   +
				"			,V.fg_subsidio SUBSIDIO"   +
				"			,V.fg_comision COMISION"   +
				"			,V.fg_totalexigible IMPORTE_PAGO"   +
				"   FROM COM_VENCIMIENTO V"   +
				" WHERE V.ic_if = ?"   +
				"     AND V.ic_moneda = ?"   +
				"     AND trunc(V.df_periodofin) = to_date(?,'dd/mm/yyyy')"   +
				"     AND trunc(V.com_fechaprobablepago) = to_date(?,'dd/mm/yyyy')"  ;
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,encabezado.getClaveIF());
			ps.setString(2,encabezado.getClaveMoneda());
			ps.setString(3,encabezado.getFechaVencimiento());
			ps.setString(4,encabezado.getFechaProbablePago());
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				ArrayList alTemp = new ArrayList();
				alTemp.add((rs.getString("SUBAPLICACION")==null)?"":rs.getString("SUBAPLICACION"));
				alTemp.add((rs.getString("PRESTAMO")==null)?"":rs.getString("PRESTAMO"));
				alTemp.add((rs.getString("CLAVE_SIRAC")==null)?"":rs.getString("CLAVE_SIRAC"));
				alTemp.add((rs.getString("CAPITAL")==null)?"":rs.getString("CAPITAL"));
				alTemp.add((rs.getString("INTERESES")==null)?"":rs.getString("INTERESES"));
				alTemp.add("0");	//moratorios
				alTemp.add((rs.getString("SUBSIDIO")==null)?"":rs.getString("SUBSIDIO"));
				alTemp.add((rs.getString("COMISION")==null)?"":rs.getString("COMISION"));
				alTemp.add("0");	//iva
				alTemp.add((rs.getString("IMPORTE_PAGO")==null)?"":rs.getString("IMPORTE_PAGO"));
				alTemp.add("VE");
				alTemp.add("I");
				al.add(alTemp);

			} //while
		rs.close();
		ps.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return al;

	}//fin del metodo capturaIndividualPagos




	/**
	 * Obtiene el desgloce del proceso por IF.
	 * @param icProc Clave del Proceso
	 * @param tipoProceso Tipo de Proceso
	 * 		E Estado de cuenta, V Vencimiento, O Operado
	 * @return lista de listas, conteniendo una relaci�n
	 * 		de if y el numero registros asociados en este proceso
	 *		0. Fecha de Corte
	 *		1. Tipo de Moneda
	 *		2. IF SIRAC
	 *		3. IF N@E
	 *		4. Razon Social IF
	 * 		5. Num de Registros MN
	 * 		6. Num de Registros DL
	 *
	 */
	public List consultarDetalleProceso(String icProc,
			String tipoProceso, String csTipo, String fechaOVFM) throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (icProc == null || tipoProceso == null || csTipo == null || fechaOVFM == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			if (!tipoProceso.equals("E") && !tipoProceso.equals("V") &&
					!tipoProceso.equals("O")) {
				throw new Exception("El tipo de proceso no es v�lido");
			}
			Integer.parseInt(icProc);

		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" icProc=" + icProc +
					" tipoProceso = " + tipoProceso +
					" csTipo = " + csTipo +
					" fechaOVFM = " +fechaOVFM);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		AccesoDB con = new AccesoDB();
		List detalle = new ArrayList();

		String tabla = "";
		String clave = "";
		String fechaDCorte = "";
		if (tipoProceso.equals("O")) {
			tabla = "com_operado";
			clave = "ic_proc_operado";
			fechaDCorte = "df_fechaoperacion";

		} else if (tipoProceso.equals("V")) {
			tabla = "com_vencimiento";
			clave = "ic_proc_vencimiento";
			fechaDCorte = "com_fechaprobablepago";

		} else if (tipoProceso.equals("E")) {
			tabla = "com_estado_cuenta";
			clave = "ic_proc_edo_cuenta";
			fechaDCorte = "df_fechafinmes";
		}
		try {
			con.conexionDB();

			String qrySentencia =

				" SELECT to_char(x." + fechaDCorte + ", 'dd/mm/yyyy') as fechaDCorte, x.ic_moneda, i.ic_financiera, i.ic_if, i.cg_razon_social, " +
				" SUM (DECODE (x.ic_moneda, 1, 1, 0)) totalmn,  SUM (DECODE (x.ic_moneda, 54, 1, 0)) totaldl " +
				" FROM " + tabla + " x, comcat_if i " +
				" WHERE x.ic_if = i.ic_if " +
				" AND x." + clave + " = ? " +
				" AND i.cs_tipo = ? " +
				" AND x."+fechaDCorte+" = to_date(?,'dd/mm/yyyy') " +
				" GROUP BY x." + fechaDCorte + ", x.ic_moneda, i.ic_financiera, i.ic_if, i.cg_razon_social " +
				" ORDER BY i.ic_financiera, i.ic_if, i.cg_razon_social ";

				log.debug("qrySentencia  "+qrySentencia);
			

			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(icProc));
			ps.setString(2,csTipo);
			ps.setString(3,fechaOVFM);
			ResultSet rs = ps.executeQuery();

			while(rs.next()){
				List registro = new ArrayList();
				registro.add((rs.getString("ic_financiera")==null)?"":rs.getString("ic_financiera"));
				registro.add((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
				registro.add((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				registro.add((rs.getString("totalmn")==null)?"":rs.getString("totalmn"));
				registro.add((rs.getString("totaldl")==null)?"":rs.getString("totaldl"));
				registro.add((rs.getString("fechaDCorte")==null)?"":rs.getString("fechaDCorte"));
				registro.add((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
				detalle.add(registro);
			} //while
			rs.close();
			ps.close();

			return detalle;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public ArrayList consultaDetallesIFB(EncabezadoPagoIFNB encabezado)
		throws NafinException {
		ArrayList al = new ArrayList();


		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String qrySentencia =
				" SELECT /*+ use_nl(V) "	+
				"			index(V CP_COM_VENCIMIENTO_PK) */ "	+
				"			V.ig_sucursal SUCURSAL"+
				"			,V.ig_subaplicaCion SUBAPLICACION"   +
				"			,V.ig_prestamo PRESTAMO"   +
				"			,v.cg_nombrecliente ACREDITADO"+
				"			,to_char(v.df_periodoinic,'dd/mm/yyyy') FECHA_OPERACION"+
				"			,to_char(v.df_periodofin,'dd/mm/yyyy') FECHA_VENCIMIENTO"+
				"			,to_char(v.com_fechaprobablepago,'dd/mm/yyyy') FECHA_PAGO"+
				"			,V.ig_tasabase TASA"+
				"			,V.fg_amortizacion CAPITAL"   +
				"			,v.ig_dias DIAS"+
				"			,V.fg_interes INTERESES"   +
				"			,V.fg_subsidio SUBSIDIO"   +
				"			,V.fg_comision COMISION"   +
				"			,V.fg_totalvencimiento IMPORTE_PAGO"   +
				"   FROM COM_VENCIMIENTO V"   +
				" WHERE V.ic_if = ?"   +
				"     AND V.ic_moneda = ?"   +
				"     AND trunc(V.df_periodofin) = to_date(?,'dd/mm/yyyy')"   +
				"     AND trunc(V.com_fechaprobablepago) = to_date(?,'dd/mm/yyyy')"  ;
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,encabezado.getClaveIF());			System.out.println(encabezado.getClaveIF());
			ps.setString(2,encabezado.getClaveMoneda());		System.out.println(encabezado.getClaveMoneda());
			ps.setString(3,encabezado.getFechaVencimiento());	System.out.println(encabezado.getFechaVencimiento());
			ps.setString(4,encabezado.getFechaProbablePago());	System.out.println(encabezado.getFechaProbablePago());
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				ArrayList alTemp = new ArrayList();
				alTemp.add((rs.getString("SUCURSAL")==null)?"":rs.getString("SUCURSAL"));
				alTemp.add((rs.getString("SUBAPLICACION")==null)?"":rs.getString("SUBAPLICACION"));
				alTemp.add((rs.getString("PRESTAMO")==null)?"":rs.getString("PRESTAMO"));
				alTemp.add((rs.getString("ACREDITADO")==null)?"":rs.getString("ACREDITADO"));
				alTemp.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
				alTemp.add((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
				alTemp.add((rs.getString("FECHA_PAGO")==null)?"":rs.getString("FECHA_PAGO"));
				alTemp.add("0.00");
				alTemp.add((rs.getString("TASA")==null)?"":rs.getString("TASA"));
				alTemp.add((rs.getString("CAPITAL")==null)?"":rs.getString("CAPITAL"));
				alTemp.add((rs.getString("DIAS")==null)?"":rs.getString("DIAS"));
				alTemp.add((rs.getString("INTERESES")==null)?"":rs.getString("INTERESES"));
				alTemp.add((rs.getString("COMISION")==null)?"":rs.getString("COMISION"));
				alTemp.add("0");	//iva
				alTemp.add((rs.getString("IMPORTE_PAGO")==null)?"":rs.getString("IMPORTE_PAGO"));
				alTemp.add("VE");
				al.add(alTemp);

			} //while
		rs.close();
		ps.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return al;

	}//fin del metodo capturaIndividualPagos



	public ArrayList cargaIndividualPagos(Acuse3 acuse, EncabezadoPagoIFNB encabezado)
		throws NafinException {
		ArrayList al = new ArrayList();
		//**********************************Validaci�n de parametros:**************************
		try {
			if (acuse == null || encabezado == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {//
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" acuse=" + acuse +
					" encabezado=" + encabezado +
					" al = " + al);
			throw new NafinException("SIST0001");
		}
		//*************************************************************************************
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO COM_ACUSE3 (CC_ACUSE,IN_MONTO_MN,IN_MONTO_DL,"+
			                "DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN) "+
			                "VALUES('"+acuse.getAcuse()+"','"+acuse.getMontoMN()+
			                "','"+acuse.getMontoDL()+"',sysdate,'"+acuse.getClaveUsuario()+
			                "','"+acuse.getReciboElectronico()+"',1)";
			//System.out.println("query Acuse3:" + strSQL);
			con.ejecutaSQL(strSQL);


			String strSQL1 = "SELECT " +
						     " CASE WHEN MAX(ic_encabezado_pago) is null THEN 1 " +
						     " ELSE MAX(ic_encabezado_pago) + 1 " +
						     " END " +
						     " FROM com_encabezado_pago ";
			ResultSet rs1 = con.queryDB(strSQL1);
			rs1.next();
			String ic_encabezado_pago = rs1.getString(1);
			rs1.close();
			con.cierraStatement();

			String strSQLInsertEncabezado = encabezado.getSQLInsert(ic_encabezado_pago);
			//System.out.println("Inser Encabezado:" + strSQLInsertEncabezado);
			con.ejecutaSQL(strSQLInsertEncabezado);

			String strSQL2 = " SELECT " +
						     " CASE WHEN MAX(ic_detalle_pago) is null THEN 0 " +
						     " ELSE MAX(ic_detalle_pago)" +
						     " END " +
						     " FROM com_detalle_pago ";
			ResultSet rs = con.queryDB(strSQL2);
			rs.next();
			String ic_detalle_pago = rs.getString(1);
			rs.close();
			con.cierraStatement();
			String qrySentencia =
				" SELECT /*+ use_nl(V) "	+
				"			index(V CP_COM_VENCIMIENTO_PK) */ "	+
				"			V.ig_subaplicaCion SUBAPLICACION"   +
				"			,V.ig_prestamo PRESTAMO"   +
				"			,V.ig_cliente CLAVE_SIRAC"   +
				"			,V.fg_amortizacion CAPITAL"   +
				"			,V.fg_interes INTERESES"   +
				"			,V.fg_subsidio SUBSIDIO"   +
				"			,V.fg_comision COMISION"   +
				"			,V.fg_totalexigible IMPORTE_PAGO"   +
				"			,?+rownum as ic_detalle_pago"+
				"   FROM COM_VENCIMIENTO V"   +
				" WHERE V.ic_if = ?"   +
				"     AND V.ic_moneda = ?"   +
				"     AND trunc(V.df_periodofin) = to_date(?,'dd/mm/yyyy')"   +
				"     AND trunc(V.com_fechaprobablepago) = to_date(?,'dd/mm/yyyy')"  ;
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_detalle_pago);
			ps.setString(2,encabezado.getClaveIF());
			ps.setString(3,encabezado.getClaveMoneda());
			ps.setString(4,encabezado.getFechaVencimiento());
			ps.setString(5,encabezado.getFechaProbablePago());
			rs = ps.executeQuery();
			while(rs.next()){
				ArrayList alTemp = new ArrayList();
				DetallePagoIFNB detalle = new DetallePagoIFNB();
				ic_detalle_pago = (rs.getString("IC_DETALLE_PAGO")==null)?"":rs.getString("IC_DETALLE_PAGO");
				alTemp.add((rs.getString("SUBAPLICACION")==null)?"":rs.getString("SUBAPLICACION"));
				alTemp.add((rs.getString("PRESTAMO")==null)?"":rs.getString("PRESTAMO"));
				alTemp.add((rs.getString("CLAVE_SIRAC")==null)?"":rs.getString("CLAVE_SIRAC"));
				alTemp.add((rs.getString("CAPITAL")==null)?"":rs.getString("CAPITAL"));
				alTemp.add((rs.getString("INTERESES")==null)?"":rs.getString("INTERESES"));
				alTemp.add("0");	//moratorios
				alTemp.add((rs.getString("SUBSIDIO")==null)?"":rs.getString("SUBSIDIO"));
				alTemp.add((rs.getString("COMISION")==null)?"":rs.getString("COMISION"));
				alTemp.add("0");	//iva
				alTemp.add((rs.getString("IMPORTE_PAGO")==null)?"":rs.getString("IMPORTE_PAGO"));
				alTemp.add("VE");
				alTemp.add("I");

				detalle.setSubaplicacion((String)alTemp.get(0));
				detalle.setPrestamo((String)alTemp.get(1));
				detalle.setClaveSiracCliente((String)alTemp.get(2));
				detalle.setCapital((String)alTemp.get(3));
				detalle.setIntereses((String)alTemp.get(4));
				detalle.setMoratorios((String)alTemp.get(5));
				detalle.setSubsidio((String)alTemp.get(6));
				detalle.setComision((String)alTemp.get(7));
				detalle.setIVA((String)alTemp.get(8));
				detalle.setImportePago((String)alTemp.get(9));
				detalle.setConceptoPago((String)alTemp.get(10));
				detalle.setOrigenPago((String)alTemp.get(11));

				String strSQLInsertDetalle = detalle.getSQLInsert(ic_detalle_pago, ic_encabezado_pago);
				//System.out.println("Inserta Detalle:" + strSQLInsertDetalle);
				con.ejecutaSQL(strSQLInsertDetalle);
				al.add(alTemp);

			} //while
		rs.close();
		ps.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return al;

	}//fin del metodo capturaIndividualPagos

	public ArrayList cargaIndividualPagosIFB(Acuse3 acuse, EncabezadoPagoIFNB encabezado)
		throws NafinException {
		ArrayList al = new ArrayList();
		//**********************************Validaci�n de parametros:**************************
		try {
			if (acuse == null || encabezado == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {//
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" acuse=" + acuse +
					" encabezado=" + encabezado +
					" al = " + al);
			throw new NafinException("SIST0001");
		}
		//*************************************************************************************
		AccesoDB con = null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO COM_ACUSE3 (CC_ACUSE,IN_MONTO_MN,IN_MONTO_DL,"+
			                "DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN) "+
			                "VALUES('"+acuse.getAcuse()+"','"+acuse.getMontoMN()+
			                "','"+acuse.getMontoDL()+"',sysdate,'"+acuse.getClaveUsuario()+
			                "','"+acuse.getReciboElectronico()+"',1)";
			//System.out.println("query Acuse3:" + strSQL);
			con.ejecutaSQL(strSQL);


			String strSQL1 = "SELECT " +
						     " CASE WHEN MAX(ic_encabezado_pago) is null THEN 1 " +
						     " ELSE MAX(ic_encabezado_pago) + 1 " +
						     " END " +
						     " FROM com_encabezado_pago ";
			ResultSet rs1 = con.queryDB(strSQL1);
			rs1.next();
			String ic_encabezado_pago = rs1.getString(1);
			rs1.close();
			con.cierraStatement();

			String strSQLInsertEncabezado = encabezado.getSQLInsert(ic_encabezado_pago);
			//System.out.println("Inser Encabezado:" + strSQLInsertEncabezado);
			con.ejecutaSQL(strSQLInsertEncabezado);

			String strSQL2 = " SELECT " +
						     " CASE WHEN MAX(ic_detalle_pago) is null THEN 0 " +
						     " ELSE MAX(ic_detalle_pago)" +
						     " END " +
						     " FROM com_detalle_pago ";
			ResultSet rs = con.queryDB(strSQL2);
			rs.next();
			String ic_detalle_pago = rs.getString(1);
			rs.close();
			con.cierraStatement();
			String qrySentencia =
				" SELECT /*+ use_nl(V) "	+
				"			index(V CP_COM_VENCIMIENTO_PK) */ "	+
				"			V.ig_sucursal SUCURSAL"+
				"			,V.ig_subaplicaCion SUBAPLICACION"   +
				"			,V.ig_prestamo PRESTAMO"   +
				"			,v.cg_nombrecliente ACREDITADO"+
				"			,to_char(v.df_periodoinic,'dd/mm/yyyy') FECHA_OPERACION"+
				"			,to_char(v.df_periodofin,'dd/mm/yyyy') FECHA_VENCIMIENTO"+
				"			,to_char(v.com_fechaprobablepago,'dd/mm/yyyy') FECHA_PAGO"+
				"			,V.ig_tasabase TASA"+
				"			,V.fg_amortizacion CAPITAL"   +
				"			,v.ig_dias DIAS"+
				"			,V.fg_interes INTERESES"   +
				"			,V.fg_subsidio SUBSIDIO"   +
				"			,V.fg_comision COMISION"   +
				"			,V.fg_totalvencimiento IMPORTE_PAGO"   +
				"			,?+rownum as ic_detalle_pago"+
				"   FROM COM_VENCIMIENTO V"   +
				" WHERE V.ic_if = ?"   +
				"     AND V.ic_moneda = ?"   +
				"     AND trunc(V.df_periodofin) = to_date(?,'dd/mm/yyyy')"   +
				"     AND trunc(V.com_fechaprobablepago) = to_date(?,'dd/mm/yyyy')"  ;
			System.out.println("ANTES");
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_detalle_pago);					System.out.println(ic_detalle_pago);
			ps.setString(2,encabezado.getClaveIF());			System.out.println(encabezado.getClaveIF());
			ps.setString(3,encabezado.getClaveMoneda());		System.out.println(encabezado.getClaveMoneda());
			ps.setString(4,encabezado.getFechaVencimiento());	System.out.println(encabezado.getFechaVencimiento());
			ps.setString(5,encabezado.getFechaProbablePago());	System.out.println(encabezado.getFechaProbablePago());
			rs = ps.executeQuery();
			System.out.println("DESPUES");
			while(rs.next()){
				ArrayList alTemp = new ArrayList();
				DetallePagoIFNB detalle = new DetallePagoIFNB();
				ic_detalle_pago = (rs.getString("IC_DETALLE_PAGO")==null)?"":rs.getString("IC_DETALLE_PAGO");

				alTemp.add((rs.getString("SUCURSAL")==null)?"":rs.getString("SUCURSAL"));
				alTemp.add((rs.getString("SUBAPLICACION")==null)?"":rs.getString("SUBAPLICACION"));
				alTemp.add((rs.getString("PRESTAMO")==null)?"":rs.getString("PRESTAMO"));
				alTemp.add((rs.getString("ACREDITADO")==null)?"":rs.getString("ACREDITADO"));
				alTemp.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
				alTemp.add((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
				alTemp.add((rs.getString("FECHA_PAGO")==null)?"":rs.getString("FECHA_PAGO"));
				alTemp.add("0.00");
				alTemp.add((rs.getString("TASA")==null)?"":rs.getString("TASA"));
				alTemp.add((rs.getString("CAPITAL")==null)?"":rs.getString("CAPITAL"));
				alTemp.add((rs.getString("DIAS")==null)?"":rs.getString("DIAS"));
				alTemp.add((rs.getString("INTERESES")==null)?"":rs.getString("INTERESES"));
				alTemp.add((rs.getString("COMISION")==null)?"":rs.getString("COMISION"));
				alTemp.add("0");	//iva
				alTemp.add((rs.getString("IMPORTE_PAGO")==null)?"":rs.getString("IMPORTE_PAGO"));
				alTemp.add("VE");

				detalle.setSucursal((String)alTemp.get(0));
				detalle.setSubaplicacion((String)alTemp.get(1));
				detalle.setPrestamo((String)alTemp.get(2));
				detalle.setAcreditado((String)alTemp.get(3));
				detalle.setFechaOperacion((String)alTemp.get(4));
				detalle.setFechaVencimiento((String)alTemp.get(5));
				detalle.setFechaPago((String)alTemp.get(6));
				detalle.setSaldoInsoluto((String)alTemp.get(7));
				detalle.setTasa((String)alTemp.get(8));
				detalle.setCapital((String)alTemp.get(9));
				detalle.setDias((String)alTemp.get(10));
				detalle.setIntereses((String)alTemp.get(11));
				detalle.setComision((String)alTemp.get(12));
				detalle.setIVA((String)alTemp.get(13));
				detalle.setImportePago((String)alTemp.get(14));
				detalle.setConceptoPago((String)alTemp.get(15));
				detalle.setOrigenPago("I");

				String strSQLInsertDetalle = detalle.getSQLInsert(ic_detalle_pago, ic_encabezado_pago);
				System.out.println("Inserta Detalle:" + strSQLInsertDetalle);
				con.ejecutaSQL(strSQLInsertDetalle);
				al.add(alTemp);

			} //while
		rs.close();
		ps.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return al;

	}//fin del metodo capturaIndividualPagos


public ArrayList getDatosPrestamos(String prestamo)
	throws NafinException{
	AccesoDB				con = new AccesoDB();
	PreparedStatement	ps = null;
	ResultSet			rs = null;
	String				qrySentencia	= "";
	ArrayList			alRet			= new ArrayList();
	try{
		con.conexionDB();
		qrySentencia =
			"select codigo_agencia as sucursal"+
			" ,codigo_sub_aplicacion as sub_aplicacion"+
			" from pr_prestamos"+
			" where numero_prestamo = ?";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,prestamo);
		rs = ps.executeQuery();
		if(rs.next()){
			alRet.add(rs.getString("sucursal"));
			alRet.add(rs.getString("sub_aplicacion"));
		}
		rs.close();
		ps.close();
	}catch(Exception e){
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
	}
	return alRet;
}


/* Este m�todo obtiene si la EPO con el IF seleccionado tiene Limite Activo.
	 * Foda 051, 03/08/2006
	 * Creado por SMJ	*/
	public String getTipoIF(String sNoIf) throws NafinException {
		System.out.println(" PagosIFNBBean::getTipoIF(E)");
		String 				cs_tipo	 	    = "";
		String				qrySentencia	= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		AccesoDB 			con 			= new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cs_tipo"   +
				"   FROM comcat_if"   +
				"  WHERE ic_if = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, sNoIf);
			rs = ps.executeQuery();

			System.out.println("qrySentencia "+qrySentencia);

			ps.clearParameters();
			if(rs.next())
				cs_tipo = rs.getString("cs_tipo");
			rs.close(); ps.close();
		} catch(SQLException sqle) {
			sqle.printStackTrace();
			throw new NafinException("DSCT0097");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("  PagosIFNBBean::getTipoIF(E)(Exception) "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("  PagosIFNBBean::getTipoIF(B,NB)");
		}
		return cs_tipo;
	}


/**
 * Generar archivo para vencimientos, operados y estados de cuenta
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ param opc Opciones de archivo: 1)vencimientos; 2)operados; 3)estados de cuenta
 *@ return regresa mensaje de como termino el proceso
 */

 public String crearArchivosZip(int opc, String ruta) throws NafinException{
 	ResultSet rs = null;
 	ResultSet rsVenc = null;
 	ResultSet rsOper = null;
 	ResultSet rsEdoC = null;
	StringBuffer strQuerys = new StringBuffer();
	Vector datosvencimiento;
	String slayout = "";
  String doctos_venc = "";//FODEA 015 - 2009
	String fvencimiento = "";
	String reg = "Generacion exitosa";
	boolean band = true;
	int total = 0;
	int ciclo = 0;
	int j = 0;
	int i = 0;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = new GregorianCalendar();
	cal.set(Calendar.DATE, 1); //Primer dia del mes actual
	String fechai = sdf.format(cal.getTime());
	List fecha = new ArrayList();
	AccesoDB con = new AccesoDB();
        AccesoDB conUpdate = new AccesoDB();
	boolean exito = true;
	GenerarArchivos archivo = new GenerarArchivos();
		try{
			System.out.println("crearArchivosZip");
			archivo.setrutaServer(ruta);
			strQuerys = strQuerys.delete(0,strQuerys.length());
			//strQuerys.append("Select cs_layout_completo_venc as layout_general From com_param_gral Where ic_param_gral = 1");
      strQuerys.append("Select cs_layout_completo_venc as layout_general, cs_layout_doctos_venc as doctos_venc From com_param_gral Where ic_param_gral = 1");//FODEA 015 - 2009 ACF

			con.conexionDB();
			rs = con.queryDB(strQuerys.toString());
			while (rs.next()){
				slayout  = rs.getString("layout_general");
        doctos_venc = rs.getString("doctos_venc");//FODEA 015 - 2009 ACF
      }
			rs.close();
			con.cierraStatement();
			switch(opc){

				case 1:

					strQuerys = strQuerys.delete(0,strQuerys.length());
					strQuerys.append("select CS_ARCHIVOS_ZIP as estatus ");
					strQuerys.append("from comhis_proc_vencimiento ");
					strQuerys.append("where ic_proc_vencimiento = (select max(ic_proc_vencimiento) from comhis_proc_vencimiento)");
					System.out.println("qry::: "+strQuerys.toString());
					rs = con.queryDB(strQuerys.toString());
					while (rs.next()){
						String estatus = rs.getString("estatus");
						if(estatus.equals("N")){
							band = false;
							break;
						}
					}
					rs.close();
					con.cierraStatement();

					if(!band){
						datosvencimiento = new Vector();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(com_fechaprobablepago,'dd/mm/yyyy') as fechaprobablepago ");
	 					strQuerys.append("FROM  comhis_proc_vencimiento ");
	 					strQuerys.append("WHERE ic_proc_vencimiento = (select max(ic_proc_vencimiento) from comhis_proc_vencimiento) ");
	 					strQuerys.append("GROUP BY com_fechaprobablepago ");
						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fecha.add(rs.getString("fechaprobablepago"));
						}
						rs.close();
						con.cierraStatement();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(sysdate-in_dias_vig_vencimientos,'dd/mm/yyyy') as fechav ");
						strQuerys.append("FROM COMCAT_PRODUCTO_NAFIN WHERE ic_producto_nafin = 1");
						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fvencimiento  = rs.getString("fechav");
							fvencimiento = fvencimiento.substring(0,2)+"_"+fvencimiento.substring(3,5)+"_"+fvencimiento.substring(6,10);
						}
						rs.close();
						con.cierraStatement();

						for(i=0;i<fecha.size();i++){
							strQuerys = strQuerys.delete(0,strQuerys.length());
							strQuerys.append("select /*+ index(v in_com_vencimiento_01_nuk)*/");
							strQuerys.append(" v.ic_if as ic_if, NVL(pxi.cs_layout_completo_venc, '"+ slayout +"') as layout_completo,");
              strQuerys.append(" NVL(pxi.cs_layout_doctos_venc, '" + doctos_venc + "') as doctos_venc,");//FODEA 015 - 2009 ACF
							strQuerys.append(" TO_CHAR(v.com_fechaprobablepago, 'dd/mm/yyyy') as fecha");
						 	strQuerys.append(" from com_vencimiento v, com_param_x_if pxi");
						 	strQuerys.append(" where v.COM_FECHAPROBABLEPAGO >= to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy')");
						 	strQuerys.append(" and v.COM_FECHAPROBABLEPAGO < to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy') + 1");
						 	strQuerys.append(" AND V.IC_IF = pxi.ic_if (+)");
							strQuerys.append(" group by v.ic_if, pxi.cs_layout_completo_venc, v.com_fechaprobablepago, pxi.cs_layout_doctos_venc");//FODEA 015 - 2009 ACF
							rsVenc = con.queryDB(strQuerys.toString());
							while (rsVenc.next()){
								datosvencimiento.addElement(rsVenc.getString("ic_if"));
								datosvencimiento.addElement(rsVenc.getString("layout_completo"));
								datosvencimiento.addElement(rsVenc.getString("fecha"));
                datosvencimiento.addElement(rsVenc.getString("doctos_venc"));//FODEA 015 - 2009 ACF
							}
							rsVenc.close();
							con.cierraStatement();
						}

						total = datosvencimiento.size();
						//ciclo = total/3;
            ciclo = total/4;//FODEA 015 - 2009 ACF

						for(j=0;j<ciclo;j++){
							//archivo.armarArchivoVencimientos(datosvencimiento.elementAt(3*j).toString(),datosvencimiento.elementAt(3*j+1).toString(),datosvencimiento.elementAt(3*j+2).toString(),fvencimiento);
              archivo.armarArchivoVencimientos(datosvencimiento.elementAt(4*j).toString(),datosvencimiento.elementAt(4*j+1).toString(),datosvencimiento.elementAt(4*j+2).toString(),fvencimiento,datosvencimiento.elementAt(4*j+3).toString());//FODEA 015 - 2009 ACF
						}

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("update comhis_proc_vencimiento ");
						strQuerys.append("set cs_archivos_zip = 'S' ");
						strQuerys.append("where ic_proc_vencimiento = (select max(ic_proc_vencimiento) from comhis_proc_vencimiento)");
                                                conUpdate.conexionDB();
                                                conUpdate.ejecutaSQL(strQuerys.toString());
					}else
						reg = "Ya se han generado los archivos";

				break;

				case 2:

					strQuerys = strQuerys.delete(0,strQuerys.length());
					strQuerys.append("select CS_ARCHIVOS_ZIP as estatus ");
					strQuerys.append("from comhis_proc_operado ");
					strQuerys.append("where ic_proc_operado = (select max(ic_proc_operado) from comhis_proc_operado)");
					rs = con.queryDB(strQuerys.toString());
					while (rs.next()){
						String estatus = rs.getString("estatus");
						if(estatus.equals("N")){
							band = false;
							break;
						}

					}
					rs.close();
					con.cierraStatement();

					if(!band){

						datosvencimiento = new Vector();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(df_operacion,'dd/mm/yyyy') as fechaoperado ");
						strQuerys.append("FROM  comhis_proc_operado ");
						strQuerys.append("WHERE ic_proc_operado = (select max(ic_proc_operado) from comhis_proc_operado) ");
						strQuerys.append("GROUP BY df_operacion");
						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fecha.add(rs.getString("fechaoperado"));
						}
						rs.close();
						con.cierraStatement();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(sysdate-in_dias_vig_operados,'dd/mm/yyyy') as fechao ");
						strQuerys.append("FROM comcat_producto_nafin WHERE ic_producto_nafin = 1");

						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fvencimiento  = rs.getString("fechao");
							fvencimiento = fvencimiento.substring(0,2)+"_"+fvencimiento.substring(3,5)+"_"+fvencimiento.substring(6,10);
						}
						rs.close();
						con.cierraStatement();

						for(i=0;i<fecha.size();i++){
							strQuerys = strQuerys.delete(0,strQuerys.length());
							strQuerys.append("select v.ic_if as ic_if, NVL(pxi.cs_layout_completo_venc, '"+ slayout +"') as layout_completo,");
							strQuerys.append(" TO_CHAR(v.df_fechaoperacion, 'dd/mm/yyyy') as fecha");
							strQuerys.append(" from com_operado v, com_param_x_if pxi");
							strQuerys.append(" where v.df_fechaoperacion >= to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy')");
							strQuerys.append(" and v.df_fechaoperacion < to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy') + 1");
							strQuerys.append(" AND V.IC_IF = pxi.ic_if(+)");
							strQuerys.append(" group by v.ic_if, pxi.cs_layout_completo_venc, v.df_fechaoperacion");
							rsOper = con.queryDB(strQuerys.toString());
							while (rsOper.next()){
								datosvencimiento.addElement(rsOper.getString("ic_if"));
								datosvencimiento.addElement(rsOper.getString("layout_completo"));
								datosvencimiento.addElement(rsOper.getString("fecha"));
							}
							rsOper.close();
							con.cierraStatement();
						}

						total = datosvencimiento.size();
						ciclo = total/3;

						for(j=0;j<ciclo;j++){
							archivo.armarArchivosOperados(datosvencimiento.elementAt(3*j).toString(),datosvencimiento.elementAt(3*j+1).toString(),datosvencimiento.elementAt(3*j+2).toString(),fvencimiento);
						}

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("update comhis_proc_operado ");
						strQuerys.append("set cs_archivos_zip = 'S' ");
						strQuerys.append("where ic_proc_operado = (select max(ic_proc_operado) from comhis_proc_operado)");
                                                conUpdate.conexionDB();
						conUpdate.ejecutaSQL(strQuerys.toString());
					}else
						reg = "Ya se han generado los archivos";

				break;

				case 3:

					strQuerys = strQuerys.delete(0,strQuerys.length());
					strQuerys.append("select CS_ARCHIVOS_ZIP as estatus ");
					strQuerys.append("from comhis_proc_edo_cuenta ");
					strQuerys.append("where ic_proc_edo_cuenta = (select max(ic_proc_edo_cuenta) from comhis_proc_edo_cuenta)");
					rs = con.queryDB(strQuerys.toString());
					while (rs.next()){
						String estatus = rs.getString("estatus");
						if(estatus.equals("N")){
							band = false;
							break;
						}

					}
					rs.close();
					con.cierraStatement();

					if(!band){

						datosvencimiento = new Vector();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(df_fechafinmes,'dd/mm/yyyy') as fechaedocuenta ");
						strQuerys.append("FROM  comhis_proc_edo_cuenta ");
						strQuerys.append("WHERE ic_proc_edo_cuenta = (select max(ic_proc_edo_cuenta) from comhis_proc_edo_cuenta) ");
						strQuerys.append("GROUP BY df_fechafinmes");
						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fecha.add(rs.getString("fechaedocuenta"));
						}
						rs.close();
						con.cierraStatement();

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("SELECT to_char(ADD_MONTHS( TO_DATE('" + fechai + "','dd/mm/yyyy'),-in_dias_vig_edo_cuenta/30)-1,'dd/mm/yyyy') as fechaec ");
						strQuerys.append("FROM comcat_producto_nafin WHERE ic_producto_nafin = 1");

						rs = con.queryDB(strQuerys.toString());
						while (rs.next()){
							fvencimiento  = rs.getString("fechaec");
							fvencimiento = fvencimiento.substring(0,2)+"_"+fvencimiento.substring(3,5)+"_"+fvencimiento.substring(6,10);
						}
						rs.close();
						con.cierraStatement();

						for(i=0;i<fecha.size();i++){
							strQuerys = strQuerys.delete(0,strQuerys.length());
							strQuerys.append("select ic_if as ic_if, TO_CHAR(df_fechafinmes, 'dd/mm/yyyy') as fecha");
							strQuerys.append(" from com_estado_cuenta");
							strQuerys.append(" where DF_FECHAFINMES >= to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy')");
							strQuerys.append(" and DF_FECHAFINMES < to_date('"+ fecha.get(i) +"', 'dd/mm/yyyy') + 1");
							strQuerys.append(" group by ic_if, df_fechafinmes");
							rsEdoC = con.queryDB(strQuerys.toString());
							while (rsEdoC.next()){
								datosvencimiento.addElement(rsEdoC.getString("ic_if"));
								datosvencimiento.addElement(rsEdoC.getString("fecha"));
							}
							rsEdoC.close();
							con.cierraStatement();
						}

						total = datosvencimiento.size();
						ciclo = total/2;

						for(j=0;j<ciclo;j++){
							archivo.armarArchivosEst_Cuenta(datosvencimiento.elementAt(2*j).toString(),datosvencimiento.elementAt(2*j+1).toString(),fvencimiento);
						}

						strQuerys = strQuerys.delete(0,strQuerys.length());
						strQuerys.append("update comhis_proc_edo_cuenta ");
						strQuerys.append("set cs_archivos_zip = 'S' ");
						strQuerys.append("where ic_proc_edo_cuenta = (select max(ic_proc_edo_cuenta) from comhis_proc_edo_cuenta)");
                                                conUpdate.conexionDB();
                                                conUpdate.ejecutaSQL(strQuerys.toString());
					}else
						reg = "Ya se han generado los archivos";
				break;

				default:
					System.out.println("*** Opcion erronea ***");
			}
			return reg;
		}catch(Exception e){
			exito = false;
			e.printStackTrace();
			//return "Error en el proceso de crearArchivosZip::: " + e.getMessage();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				System.out.println("crearArchivosZip finally");
				con.cierraConexionDB();
			}
                        if (conUpdate.hayConexionAbierta()){
                                conUpdate.terminaTransaccion(exito);
                                System.out.println("conUpdate crearArchivosZip finally");
                                conUpdate.cierraConexionDB();
                        }
			//En este finally no se cierra la conexion porque este metodo es invocado desde otro metodo, que es el que se
			//encarga de hacer dicha operacion
		}
	}

	/********************************************************************************************
	 *cada que se consulten pagos de cartera - informacion operativa if, se insertara en bitacora
	 *
	 *
	 *********************************************************************************************/
  public void insertarDatosConsultaPCIf(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, 
                                          String igprestamo, String icif, String cvecertificado, String pantalla) {
                                          
    insertarDatosConsultaPCIf(icusuario, cgnombreusuario, dfcorte,  icmoneda, igcliente, 
                                          igprestamo, icif, cvecertificado, pantalla, "","D");
  
  }
   
	public void insertarDatosConsultaPCIf(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, String igprestamo, String icif, String cvecertificado, String pantalla, String cveCliExterno, String tipoLinea) {
		System.out.println("insertarDatosConsultaPCIf (E)");
		AccesoDB con = null;
		PreparedStatement	ps	= null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO bit_consulta_pago " +
										"(ic_usuario, cg_nombre_usuario, df_consulta_pago, df_fecha_corte, ic_moneda, ig_cliente, ig_numero_prestamo, ic_if, ig_clave_certificado, cg_pantalla_consulta, ic_cliente_externo, cg_tipo_linea) " +
										"values (?,?,sysdate,to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?,?,?)";

			ps = con.queryPrecompilado(strSQL);

			ps.setString(1,icusuario);
			ps.setString(2,cgnombreusuario);
			if(!dfcorte.equals(""))
				ps.setString(3,dfcorte);
			else
				ps.setNull(3, Types.VARCHAR);
			if(!icmoneda.equals(""))
				ps.setInt(4,Integer.parseInt(icmoneda));
			else
				ps.setNull(4, Types.INTEGER);
			if(!igcliente.equals(""))
				ps.setInt(5,Integer.parseInt(igcliente));
			else
				ps.setNull(5, Types.INTEGER);
			if(!igprestamo.equals(""))
				ps.setInt(6,Integer.parseInt(igprestamo));
			else
				ps.setNull(6, Types.INTEGER);
      if(!icif.equals(""))
				ps.setInt(7,Integer.parseInt(icif));
			else
				ps.setNull(7, Types.INTEGER);
			if(!cvecertificado.equals(""))
				ps.setString(8,cvecertificado);
			else
				ps.setNull(8, Types.VARCHAR);
			ps.setString(9,pantalla);
      if(!cveCliExterno.equals(""))
				ps.setString(10,cveCliExterno);
			else
				ps.setNull(10, Types.INTEGER);
      if(!tipoLinea.equals(""))
				ps.setString(11,tipoLinea);
			else
				ps.setNull(11, Types.VARCHAR);
      
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			exito = false;
		} finally {
			System.out.println("insertarDatosConsultaPCIf (S)");
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * cada que se consulten pagos de cartera para Bancomext, se guarda registro en bitacora
	 * @param pantalla
	 * @param cvecertificado
	 * @param icif
	 * @param igprestamo
	 * @param igcliente
	 * @param icmoneda
	 * @param dfcorte
	 * @param cgnombreusuario
	 * @param icusuario
	 */
	public void insertarDatosConsultaPBmx(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, String igprestamo, String icif, String cvecertificado, String pantalla) {
		System.out.println("insertarDatosConsultaPBmx (E)");
		AccesoDB con = null;
		PreparedStatement	ps	= null;
		boolean exito = true;
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			String strSQL = "INSERT INTO bit_bmx_consulta_pago " +
										"(ic_usuario, cg_nombre_usuario, df_consulta_pago, df_fecha_corte, ic_moneda, ig_cliente, ig_numero_prestamo, ic_if, ig_clave_certificado, cg_pantalla_consulta) " +
										"values (?,?,sysdate,to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?)";

			ps = con.queryPrecompilado(strSQL);

			ps.setString(1,icusuario);
			ps.setString(2,cgnombreusuario);
			if(!dfcorte.equals(""))
				ps.setString(3,dfcorte);
			else
				ps.setNull(3, Types.VARCHAR);
			if(!icmoneda.equals(""))
				ps.setInt(4,Integer.parseInt(icmoneda));
			else
				ps.setNull(4, Types.INTEGER);
			if(!igcliente.equals(""))
				ps.setInt(5,Integer.parseInt(igcliente));
			else
				ps.setNull(5, Types.INTEGER);
			if(!igprestamo.equals(""))
				ps.setInt(6,Integer.parseInt(igprestamo));
			else
				ps.setNull(6, Types.INTEGER);
			ps.setString(7,icif);
			if(!cvecertificado.equals(""))
				ps.setString(8,cvecertificado);
			else
				ps.setNull(8, Types.VARCHAR);
			ps.setString(9,pantalla);
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			exito = false;
		} finally {
			System.out.println("insertarDatosConsultaPBmx (S)");
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
	
	public Registros getArchivoPagos(String ic_if){
	AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= " SELECT    "+
				"           v.ic_moneda clave_moneda, m.cd_nombre moneda, "+
				"           TO_CHAR (v.df_periodofin, 'DD/MM/YYYY') fecha_vencimiento, "+
				"           TO_CHAR (v.com_fechaprobablepago, 'DD/MM/YYYY') fecha_prob_pago, "+
				"           SUM (v.fg_totalexigible) importe_deposito "+
				"   FROM com_vencimiento v, "+
				"           comcat_moneda m, "+
				"           comcat_if i "+
				" WHERE v.ic_if = ? "+
				"     AND v.ic_if = i.ic_if "+
				"     AND v.ic_moneda = m.ic_moneda "+
				" GROUP BY v.ic_moneda, m.cd_nombre, "+
				" 		    TO_CHAR (v.df_periodofin, 'DD/MM/YYYY'), "+
				"           TO_CHAR (v.com_fechaprobablepago, 'DD/MM/YYYY') "+
				" ORDER BY fecha_vencimiento, fecha_prob_pago ";
		List 				lVarBind			= new ArrayList();
		Registros		registros		= new Registros();
		try{
		lVarBind.add(ic_if);
	
					con.conexionDB();
					registros = con.consultarDB(qrySentencia, lVarBind, false);
					
			
		} catch(Exception e) {
				e.printStackTrace();
		} finally {
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
						
		}
				return registros;
	}
	
	
	
	public List getPagosFecha(String icMoneda,String iNoCliente,String sFechaVencIni,String sFechaVencFin,String sFechaProbIni,String sFechaProbFin,String sTipoBanco,Integer totalReg,Double totalValor){
	   //setBitacoraPagos(iNoCliente, hidBitacora, sFechaVencIni, sFechaVencFin, sFechaProbIni);
		List regEncabezados=new ArrayList();
		List regDetalles=new ArrayList();
		List regTotales=new ArrayList();
		List registrosTotales = new ArrayList();
		AccesoDB con = new AccesoDB();
		try{
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String sQueryInterno = "";
		int iSubRegistros=0;
		con.conexionDB();
		HashMap	datos;
		String sCondicion="", sOrden="", sFechaDel="", sFechaAl="";
		if(!sFechaVencIni.equals("") && !sFechaVencFin.equals("")){//BUSQUEDA POR FECHA DE VENCIMIENTO
			sCondicion = " AND V.df_periodofin >= to_date(?,'dd/mm/yyyy') "   +
								" AND V.df_periodofin < to_date(?,'dd/mm/yyyy')+1 ";
			sOrden=" V.df_periodofin ";
			sFechaDel=sFechaVencIni;
			sFechaAl=sFechaVencFin;
		}else{//BUSQUEDA POR FECHA PROBABLE DE PAGO
			sCondicion = " AND V.com_fechaprobablepago >= to_date(?,'dd/mm/yyyy') "   +
								" AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1 ";
			sOrden=" V.com_fechaprobablepago ";
			sFechaDel=sFechaProbIni;
			sFechaAl=sFechaProbFin;
		}
		//out.println("Fecha del: "+sFechaDel+" al "+sFechaAl);
		String query = " SELECT "	+
		     
			    	           "			V.ic_if CLAVE_IF"   +
							   "			,V.ic_moneda CLAVE_MONEDA"	+
		      				   "			,M.cd_nombre MONEDA"		+
							   "			,TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
							   "			,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PROB_PAGO"   +
			  				   "			,SUM(V.fg_totalexigible) IMPORTE_DEPOSITO"	+
			                   "			,F.ic_estado CLAVE_ESTADO"	+
			                   "			,E.cd_nombre NOMBRE_ESTADO"		+
							   "			,V.df_periodofin"   +
							   "			,V.com_fechaprobablepago"   +
							   "   FROM com_vencimiento V"   +
							   "			,comcat_moneda M"   +
						 	   "			,comcat_if I"	+
		                	   "			,comcat_financiera F"	+
			                   "			,comcat_estado E"	+
							   " WHERE V.ic_moneda = M.ic_moneda"		+
			  				   "     AND V.ic_if = I.ic_if"		+
								
		               	       "     AND I.ic_financiera = F.ic_financiera"		+
			                   "     AND F.ic_estado = E.ic_estado"		+
								 "     AND V.ic_moneda = ?"   +
							   "     AND V.ic_if = ?"   +
							   sCondicion+
							   " GROUP BY V.ic_if"   +
							   "				,V.ic_moneda"	+
							   "				,M.cd_nombre"   +
							   "				,TO_CHAR(V.df_periodofin,'DD/MM/YYYY')"   +
							   "				,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY')"	+
							   "				,F.ic_estado"	+
							   "				,E.cd_nombre"	+
							   "				,V.df_periodofin"   +
							   "				,V.com_fechaprobablepago"   +
							   " ORDER BY"	+
							   sOrden;
		//out.println("query:"+query+"<br>");
		PreparedStatement ps = con.queryPrecompilado(query);
		ps.setString(1,icMoneda);
		ps.setString(2, iNoCliente);
		ps.setString(3, sFechaDel);
		ps.setString(4, sFechaAl);
		System.out.println(query);
		ResultSet rs = ps.executeQuery();
		int registros=0;
		String sClaveIF = "", sClaveMoneda = "", sMoneda = "", sFechaVencimiento = "", sFechaProbPago = "", sImporteDeposito = "", sClaveEstado = "", sEstado = "";
		double dImporteDepositoTotal=0;
		while (rs.next()) {
			regEncabezados=new ArrayList();
			regDetalles=new ArrayList();
			sClaveIF          = (rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim();
        	sClaveMoneda      = (rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim();
        	sMoneda           = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim();
        	sFechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim();
        	sFechaProbPago    = (rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim();
			sImporteDeposito  = (rs.getString("IMPORTE_DEPOSITO") == null) ? "" : rs.getString("IMPORTE_DEPOSITO").trim();
        	sClaveEstado      = (rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim();
        	sEstado           = (rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim();
			
			datos=new HashMap();
			datos.put("CLAVE_IF",(rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim());
			datos.put("CLAVE_MONEDA",(rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim());
			datos.put("MONEDA",(rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim());
			datos.put("FECHA_VENCIMIENTO",(rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim());
			datos.put("FECHA_PROB_PAGO",(rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim());
			datos.put("IMPORTE_DEPOSITO",(rs.getString("IMPORTE_DEPOSITO") == null) ? "" : rs.getString("IMPORTE_DEPOSITO").trim());
			datos.put("CLAVE_ESTADO",(rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim());
			datos.put("NOMBRE_ESTADO",(rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim());
			
			regEncabezados.add(datos);
			
			//SUBQUERY
			if(sTipoBanco.equals("NB")) {	// Si es IF no Bancario.
				sQueryInterno= " SELECT  "	+
										
										"			V.ig_subaplicaCion SUBAPLICACION"   +
										"			,V.ig_prestamo PRESTAMO"   +
										"			,V.ig_cliente CLAVE_SIRAC"   +
										"			,V.fg_amortizacion CAPITAL"   +
										"			,V.fg_interes INTERESES"   +
										"			,V.fg_subsidio SUBSIDIO"   +
										"			,V.fg_comision COMISION"   +
										"			,V.fg_totalexigible IMPORTE_PAGO"   +
										"   FROM COM_VENCIMIENTO V"   +
										" WHERE V.ic_if = ?"   +
										"     AND V.ic_moneda = ?"   +
										"     AND V.df_periodofin >= to_date(?,'dd/mm/yyyy')"   +
										"     AND V.df_periodofin < to_date(?,'dd/mm/yyyy')+1 "   +
										"     AND V.com_fechaprobablepago >= to_date(?,'dd/mm/yyyy')"  +
										"     AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1"  ;
				//out.println("sQueryInterno:"+sQueryInterno+"<br>");
				ps2 = con.queryPrecompilado(sQueryInterno);
				ps2.setString(1, sClaveIF);
				ps2.setString(2, sClaveMoneda);
				ps2.setString(3, sFechaVencimiento);
				ps2.setString(4, sFechaVencimiento);
				ps2.setString(5, sFechaProbPago);
				ps2.setString(6, sFechaProbPago);
				rs2 = ps2.executeQuery();
				System.out.println(sQueryInterno);
				while (rs2.next()) {
					datos=new HashMap();
					datos.put("SUBAPLICACION",(rs2.getString("SUBAPLICACION") == null) ? "" : rs2.getString("SUBAPLICACION").trim());
					datos.put("PRESTAMO",(rs2.getString("PRESTAMO") == null) ? "" : rs2.getString("PRESTAMO").trim());
					datos.put("CLAVE_SIRAC",(rs2.getString("CLAVE_SIRAC") == null) ? "" : rs2.getString("CLAVE_SIRAC").trim());
					datos.put("CAPITAL","$"+Comunes.formatoDecimal(rs2.getString("CAPITAL"),2));
					datos.put("INTERESES","$"+Comunes.formatoDecimal(rs2.getString("INTERESES"),2));
					datos.put("MORATORIOS","0.00");
					datos.put("SUBSIDIO","$"+Comunes.formatoDecimal(rs2.getString("SUBSIDIO"),2));
					datos.put("COMISION","$"+Comunes.formatoDecimal(rs2.getString("COMISION"),2));
					datos.put("IVA","0.00");
					datos.put("IMPORTE_PAGO","$"+Comunes.formatoDecimal(rs2.getString("IMPORTE_PAGO"),2));
					datos.put("CONCEPTO_PAGO","VE");
					datos.put("ORIGEN_PAGO","A");
					regDetalles.add(datos);
					iSubRegistros++;
				}//fin while 2
				
			} else if(sTipoBanco.equals("B")) {			// IF Bancario.
				sQueryInterno= " SELECT  "	+
										"			V.ig_sucursal SUCURSAL, "	+
										"			V.ig_subaplicaCion SUBAPLICACION, "	+
										"			V.ig_prestamo PRESTAMO, "	+
										"			V.cg_nombrecliente ACREDITADO, "	+
										"			TO_CHAR(V.df_periodoinic,'DD/MM/YYYY') FECHA_OPERACION, "	+
										"			TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO, "	+
										"			TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PAGO, "	+
										"			V.ig_tasabase TASA, "	+
										"			V.fg_amortizacion CAPITAL, "	+
										"			V.ig_dias DIAS, "	+
										"			V.fg_interes INTERESES, "	+
										"			V.fg_comision COMISION, "	+
										"			V.fg_totalexigible IMPORTE_PAGO "	+
										"   FROM COM_VENCIMIENTO V"   +
										" WHERE V.ic_if = ?"   +
										"     AND V.ic_moneda = ?"   +
										"     AND V.df_periodofin >= to_date(?,'dd/mm/yyyy')"   +
										"     AND V.df_periodofin < to_date(?,'dd/mm/yyyy')+1 "   +
										"     AND V.com_fechaprobablepago >= to_date(?,'dd/mm/yyyy')"  +
										"     AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1"  ;
				//out.println("sQueryInterno:"+sQueryInterno+"<br>");
				ps2 = con.queryPrecompilado(sQueryInterno);
				ps2.setString(1, sClaveIF);
				ps2.setString(2, sClaveMoneda);
				ps2.setString(3, sFechaVencimiento);
				ps2.setString(4, sFechaVencimiento);
				ps2.setString(5, sFechaProbPago);
				ps2.setString(6, sFechaProbPago);
				rs2 = ps2.executeQuery();
				System.out.println(sQueryInterno);
				while (rs2.next()) {
					datos=new HashMap();

					
					datos.put("SUCURSAL",(rs2.getString("SUCURSAL")==null?"":rs2.getString("SUCURSAL").trim()));	
					datos.put("SUBAPLICACION",(rs2.getString("SUBAPLICACION") == null) ? "" : rs2.getString("SUBAPLICACION").trim());
					datos.put("PRESTAMO",(rs2.getString("PRESTAMO")==null?"":rs2.getString("PRESTAMO").trim()));
					datos.put("ACREDITADO",(rs2.getString("ACREDITADO") == null) ? "" : rs2.getString("ACREDITADO").trim());
					datos.put("FECHA_OPERACION",(rs2.getString("FECHA_OPERACION") == null) ? "" : rs2.getString("FECHA_OPERACION").trim());
					datos.put("FECHA_VENCIMIENTO",(rs2.getString("FECHA_VENCIMIENTO") == null) ? "" : rs2.getString("FECHA_VENCIMIENTO").trim());
					datos.put("FECHA_PAGO",(rs2.getString("FECHA_PAGO") == null) ? "" : rs2.getString("FECHA_PAGO").trim());
					datos.put("SALDO","0.00");
					datos.put("TASA",(rs2.getString("TASA") == null) ? "" : rs2.getString("TASA").trim());
					datos.put("CAPITAL","$"+Comunes.formatoDecimal(rs2.getString("CAPITAL"),2));
					datos.put("DIAS",(rs2.getString("DIAS")==null?"":rs2.getString("DIAS").trim()));
					datos.put("INTERESES","$"+Comunes.formatoDecimal(rs2.getString("INTERESES"),2));
					datos.put("COMISION","$"+Comunes.formatoDecimal(rs2.getString("COMISION"),2));
					datos.put("IVA","0.00");
					datos.put("IMPORTE_PAGO","$"+Comunes.formatoDecimal(rs2.getString("IMPORTE_PAGO"),2));
					regDetalles.add(datos);
					iSubRegistros++;
				}//fin while 2
			}
			if(rs2!=null) rs2.close();
			if(ps2!=null) ps2.close();
			//FIN SUBQUERY
			dImporteDepositoTotal += Double.parseDouble(sImporteDeposito);
			registros++;
			registrosTotales.add(regDetalles);
			registrosTotales.add(regEncabezados);
			}
		totalReg=new Integer(iSubRegistros);
		totalValor=new Double(dImporteDepositoTotal);
		
		datos=new HashMap();
		datos.put("PRESTAMOS",totalReg);
		datos.put("TOTAL_IMPORTE",totalValor);
		regTotales.add(datos);
		
		System.out.println(iSubRegistros+"\n"+dImporteDepositoTotal+"\n"+totalReg+"\n"+totalValor);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		List regis = new ArrayList();
		regis.add(registrosTotales);
		regis.add(regTotales);
		return regis;
	}
	
	public String crearCustomFile(List rs,String path, String tipo){
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
		String SEP = ",";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contArchivoCVS = new StringBuffer();
		int numRegistros = 0;
		for(int i=0; i < rs.size(); i++){
			List vecColumnas = (List) rs.get(i);
			String numIfSirac = (String)vecColumnas.get(0);
			String numIfNafin = (String)vecColumnas.get(1);
			String razonSocial = (String)vecColumnas.get(2);
			String numRegMN = (String)vecColumnas.get(3);
			String numRegDLS = (String)vecColumnas.get(4);		
			if(i == 0){
				contArchivoCVS.append("N�m. IF SIRAC" + SEP + "N�m. IF N@E" +
											SEP + "Nombre del Intermediario" + SEP +
											"Num. Registros Moneda Nacional" + SEP + 
											"Num. Registros D�lares\n"); 
			}
			contArchivoCVS.append(numIfSirac.replaceAll(",","") + SEP +
									numIfNafin.replaceAll(",","") + SEP +
									razonSocial.replaceAll(",","") + SEP +
									numRegMN.replaceAll(",","") + SEP +
									numRegDLS.replaceAll(",","") + "\n");
			
			numRegistros++;
		}
		if(tipo.equals("CVS")){
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".cvs";
			if(numRegistros >= 1){
				if(!creaArchivo.make(contArchivoCVS.toString(),path, ".csv")){
					nombreArchivo="ERROR";
				}else{
					nombreArchivo = creaArchivo.nombre;
				}
			}	
		}
		
		return nombreArchivo;
		
	}
  
  /**
   * Consulta los datos de Numero Sirac e Ic_Financiera de un Intermediario
   * @throws netropology.utilerias.AppException
   * @return 
   * @param cveIf
   */
  public HashMap getSiracFinaciera(String cveIf)throws AppException
  {
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    String query = "";
    HashMap hmResult = new HashMap();
    try
    {
      con.conexionDB();
      query = "SELECT ic_financiera, in_numero_sirac " +
                   "  FROM comcat_if " +
                   " WHERE ic_if = ? ";
      
      ps = con.queryPrecompilado(query);
      ps.setInt(1, Integer.parseInt(cveIf));
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        hmResult.put("IC_FINANCIERA",rs.getString("ic_financiera")==null?"":rs.getString("ic_financiera"));
        hmResult.put("NUMERO_SIRAC",rs.getString("in_numero_sirac")==null?"":rs.getString("in_numero_sirac"));
      }
      rs.close();
      ps.close();
      
      return hmResult;
    }catch(Throwable t)
    {
      t.printStackTrace();
      throw new AppException("Error al consultar el ic_financiera y el numero sirac del Intermediario", t);
    }finally
    {
      if(con.hayConexionAbierta())
      {
        con.cierraConexionDB();
      }
    }
  }
  
  /**
   * Consulta  Numero Sirac  de los Cientes Externos con Perfil LINEA CREDITO
   * @throws netropology.utilerias.AppException
   * @return 
   * @param cveIf
   */
  public String getSiracLineaCredito(String cveLineaCred)throws AppException
  {
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    String query = "";
    String numSirac = "";
    try
    {
      con.conexionDB();
      query = "SELECT IN_NUMERO_SIRAC " +
              "  FROM comcat_cli_externo " +
              " WHERE ic_nafin_electronico = ? ";

      
      ps = con.queryPrecompilado(query);
      ps.setInt(1, Integer.parseInt(cveLineaCred));
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        numSirac = rs.getString("IN_NUMERO_SIRAC")==null?"":rs.getString("IN_NUMERO_SIRAC");
      }
      rs.close();
      ps.close();
      
      return numSirac;
    }catch(Throwable t)
    {
      t.printStackTrace();
      throw new AppException("Error al consultar el numero sirac de Cliente Externo(LINEA CREDITO)", t);
    }finally
    {
      if(con.hayConexionAbierta())
      {
        con.cierraConexionDB();
      }
    }
  }
  
  //------------------------------------------------------------------------WLogic
  /**
	 * Metodo que obtiene el archivo zip de la base de datos para ser descargado
	 * Los archivos descargados son los que se generan con el proceso PagosIFNBEJBCliente.sh
	 * @param nombreZip
	 * @param ruta
	 * @return
	 * @throws AppException
	 */
	public String consultaArchivosZip(String nombreZip, String strDirectorioTemp) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		File file = null;
		FileOutputStream fileOutputStream = null;
		ResultSet rs =  null;
		String query = "";
	    String pathname = "";
		try{
		    con.conexionDB();
			System.out.println("consultaArchivosZip");
			query = "SELECT bi_archivo " +
					"  FROM com_archivos_procesos " +
					" WHERE cg_nombre_archivo = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setString(1,nombreZip);
			rs = ps.executeQuery();
			
		    if(rs.next()){
				pathname = strDirectorioTemp + nombreZip;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rs.getBlob("bi_archivo");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
			}
			rs.close();
			ps.close();
			
			return pathname;
		}catch(Exception e){
			e.printStackTrace();
			//return "Error en el proceso de crearArchivosZip::: " + e.getMessage();
			throw new AppException("Error al obetener archivo zip de la Base de datos", e);
		} finally {
			if (con.hayConexionAbierta()){
			con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Se encarga solo de validar si el archivo Zip ya fue generado y guardado en la base
	 * Estos archivos son generados y guadados mediante el proceso PagosIFNBEJBCliente.sh
	 * @param nombreZip
	 * @return
	 * @throws AppException
	 */
	public boolean existeArchivosZip(String nombreZip) throws AppException{
		log.info("existeArchivosZip(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs =  null;
		String query = "";
		boolean existe = false;
		try{
			con.conexionDB();
			query = "SELECT count(*) contador" +
					"  FROM com_archivos_procesos " +
					" WHERE cg_nombre_archivo = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setString(1,nombreZip);
			rs = ps.executeQuery();
			
			if(rs.next()){
				int contador = rs.getInt("contador");
				if(contador>0) existe = true;

			}
			rs.close();
			ps.close();
			
			return existe;
		}catch(Exception e){
		    log.info("existeArchivosZip(Error)");
			e.printStackTrace();
			throw new AppException("Error al verificar si existe el archivo Zip generado por medio del proceso PagosIFNBEJBCliente ", e);
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		    log.info("existeArchivosZip(S)");
		}
	}
	
	/**
	 * Se encarga de guardar el zip generado contiene Informacion Operativa(Edo de cuanta, Vencimientos, Operados)
	 * @param nombreZip
	 * @param strDirectorioTemp
	 * @throws AppException
	 */
	public void guardarArchivosZip(String nombreZip, String strDirectorioTemp) throws AppException{
	    log.info("guardarArchivosZip(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		boolean commit = true;
		try{
			con.conexionDB();
			int contador = 0;
			query = "SELECT count(*) " +
					"  FROM com_archivos_procesos " +
					" WHERE cg_nombre_archivo = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setString(1,nombreZip);
			rs = ps.executeQuery();
			
			if(rs.next()){
				contador = rs.getInt(1);
			}
			rs.close();
			ps.close();
			
			if(contador>0){
        query = "DELETE com_archivos_procesos " +
                "  WHERE cg_nombre_archivo = ? ";
        
        ps = con.queryPrecompilado(query);
        ps.setString(1,nombreZip);
        ps.executeUpdate();
				ps.close();
			}
			
		    File fzip = new File(strDirectorioTemp+nombreZip);
		    FileInputStream fis = new FileInputStream(fzip);
			query = "INSERT INTO com_archivos_procesos (cg_nombre_archivo, bi_archivo, cg_origen) " +
					"     VALUES (?, ?, ?) ";

		    ps = con.queryPrecompilado(query);
			ps.setString(1, nombreZip);
			ps.setBinaryStream(2, fis, (int)fzip.length());
		    ps.setString(3, "PagosIFNBEJBCliente.sh");
			ps.executeUpdate();
			ps.close();
			
		}catch(Exception e){
		    log.info("guardarArchivosZip(Error)");
		    commit = false;
			e.printStackTrace();
			throw new AppException("Error al guardar el archivo zip genrado para la parte de Informacion operativa", e);
		} finally {
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		    log.info("guardarArchivosZip(S)");
		}
	}
	
	/**
	 * Se encarga de depurar Zips generados para Informacion Operativa(Edo de cuenta, Vencimientos, Operados)
	 * @param nombreZip
	 * @throws AppException
	 */
	public void depuraArchivosZip(String nombreZip) throws AppException{
		log.info("depuraArchivosZip(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		boolean commit = true;
		try{
			con.conexionDB();
			int contador = 0;
			query = "SELECT count(*) " +
					"  FROM com_archivos_procesos " +
					" WHERE cg_nombre_archivo = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setString(1,nombreZip);
			rs = ps.executeQuery();
			
			if(rs.next()){
				contador = rs.getInt(1);
			}
			rs.close();
			ps.close();
			
			if(contador>0){
				query = "DELETE com_archivos_procesos " +
						"  WHERE cg_nombre_archivo = ? ";
				
				ps = con.queryPrecompilado(query);
				ps.setString(1,nombreZip);
				ps.executeUpdate();
				ps.close();
			}
			
			
		}catch(Exception e){
			log.info("depuraArchivosZip(Error)");
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al depurar el archivo zip generado para la parte de Informacion Operativa", e);
		} finally {
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("depuraArchivosZip(S)");
		}
	}

} //fin-clase PagosIFNBBean

