package com.netro.descuento;
import java.util.*;
import java.sql.*;
import netropology.utilerias.*;

/*import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
*/

import java.math.*;

public class NotaCredito{
	
	public NotaCredito(){}
	
	public NotaCredito(int icDocto,double fnMonto,int icMoneda){
		this(icDocto,fnMonto,icMoneda,false);
	}
	
	public NotaCredito(int icDocto,double fnMonto,int icMoneda,boolean aplicarNotaCreditoAMultiplesDoctos){
		this.ic_documento 								= icDocto;
		this.fn_monto 										= fnMonto;
		this.ic_moneda 									= icMoneda;
		this.disponible 									= true;
		this.tmpDisp 										= true;
		// Fodea 002 - 2010. Aplicar Una Nota de Credito a Multiples Documentos
		this.aplicarNotaCreditoAMultiplesDoctos 	= aplicarNotaCreditoAMultiplesDoctos;
		this.tmpFnMontoDisponible 						= this.aplicarNotaCreditoAMultiplesDoctos?new BigDecimal(fnMonto):new BigDecimal("0.00");
		this.fnMontoDisponible 							= this.aplicarNotaCreditoAMultiplesDoctos?new BigDecimal(fnMonto):new BigDecimal("0.00");
				
	}
	
	private int 			ic_documento;
	private double 		fn_monto;
	private int 			ic_moneda;
	private boolean 		disponible;
	private boolean 		tmpDisp;
	// Fodea 002 - 2010.
	private boolean 		aplicarNotaCreditoAMultiplesDoctos;
	private BigDecimal	fnMontoDisponible;
	private BigDecimal	tmpFnMontoDisponible;
	
	public void setDisponible(boolean disp){
		this.tmpDisp = this.disponible;
		this.disponible = disp;
	}
	public int getIcDocumento(){
		return ic_documento;
	}
	public double getFnMonto(){
		return fn_monto;		
	}
	public int getIcMoneda(){
		return ic_moneda;
	}
	public boolean esDisponible(){
		return this.disponible;
	}
	public void retornaDisp(){	
		this.disponible 		= this.tmpDisp;
		// Fodea 002 - 2010
		if(this.aplicarNotaCreditoAMultiplesDoctos){
			this.fnMontoDisponible = this.tmpFnMontoDisponible;
		}
	}
	public void confirmaDisp(){	
		this.tmpDisp 				= this.disponible;
		// Fodea 002 - 2010
		if(this.aplicarNotaCreditoAMultiplesDoctos){
			if(!this.tmpDisp){
				this.fnMontoDisponible = new BigDecimal("0.00");
			}
			this.tmpFnMontoDisponible = this.fnMontoDisponible;
		}
	}
	// Fodea 002 - 2010	
	public void setFnMontoDisponible(BigDecimal fnMontoDisponible){		
		this.fnMontoDisponible = fnMontoDisponible;
	}
	public BigDecimal getFnMontoDisponible(){
		return this.fnMontoDisponible;
	}
////*****-----
	public HashMap getDatosDocumentos(HashSet listaDocumentos)
		throws Exception{
		
			System.out.println("13forma1_popm.jsp::getDatosDocumentos(E)");
		
			HashMap			resultado		= new HashMap();
			if(listaDocumentos == null || listaDocumentos.size() == 0){
				System.out.println("13forma1_popm.jsp::getDatosDocumentos(S)");
				return	resultado;
			}
				
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			ResultSet		registros		= null;
			List 				lVarBind			= new ArrayList();
			
			try {	
 
				con = new AccesoDB();
				con.conexionDB();

				String lista = "";
				Iterator it = listaDocumentos.iterator();
				int k=0;
				while(it.hasNext()) {
					if(k>0){
						lista += ",";
					}
					lista  += (String)it.next();
					k++;
				}	
        
        qrySentencia.append(	
			   "SELECT " +
				 " d.ic_documento," +
				 " TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS fechaEmision, "  + 
				 " m.cd_nombre AS moneda, " +
				 " f.cg_nombre AS tipoFactoraje, "  +
				 " d.ig_numero_docto "  +
				"FROM com_documento d, "  +
				" comcat_moneda m, " +
				" comcat_tipo_factoraje f "  +
				" WHERE ic_documento IN ( "+lista+" ) AND " +
				"  d.cs_dscto_especial = f.cc_tipo_factoraje AND " +
				" m.ic_moneda = d.ic_moneda");

				registros = con.queryDB(qrySentencia.toString());
				while(registros != null && registros.next()){
					
					String igNumeroDocto = registros.getString("ig_numero_docto");
					String icDocumento 	= registros.getString("ic_documento");
					String fechaEmision  = registros.getString("fechaEmision");
					String moneda			= registros.getString("moneda");
					String tipoFactoraje = registros.getString("tipoFactoraje");
					
					HashMap registro = new HashMap();
					registro.put("IG_NUMERO_DOCTO", 	igNumeroDocto);
					registro.put("IC_DOCUMENTO",     icDocumento);
					registro.put("FECHA_EMISION",		fechaEmision);
					registro.put("MONEDA",				moneda);
					registro.put("TIPO_FACTORAJE",	tipoFactoraje);
					 
					resultado.put(icDocumento,registro);
          
				}
				con.cierraStatement();
		}catch(Exception e){
			
			System.out.println("13forma1_popm.jsp::getDatosDocumentos(Exception)");
			System.out.println("13forma1_popm.jsp::getDatosDocumentos.listaClavesNotas = <"+listaDocumentos+">");
			e.printStackTrace();
			throw new Exception("Error al Consultar Notas de Credito");
			
		}finally{
			if(registros != null) registros.close();
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13forma1_popm.jsp::getDatosDocumentos(S)13");
		}

		return resultado;	
	}
	
	public String getPropiedad(String id,String propiedad,HashMap bd){
		
		if(id == null || id.equals("")) return "";
		if(propiedad == null) return "";
		if(bd == null) return "";
		
		HashMap registro = (HashMap) bd.get(id);
		String resultado = (String) registro.get(propiedad);
		if(resultado == null){
			return "";	
		}
		return resultado;
		
	}
	
	public HashMap getDatosNotasCredito(HashSet listaClavesNotas)
		throws Exception{
		
			System.out.println("13forma1_popm.jsp::getDatosNotasCredito(E)");
		
			HashMap			resultado		= new HashMap();
			if(listaClavesNotas == null || listaClavesNotas.size() == 0){
				System.out.println("13forma1_popm.jsp::getDatosNotasCredito(S)");
				return	resultado;
			}
				
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			ResultSet		registros		= null;
			List 				lVarBind			= new ArrayList();
			
			try {	
 
				con = new AccesoDB();
				con.conexionDB();

				String lista = "";
				Iterator it = listaClavesNotas.iterator();
				int k=0;
				while(it.hasNext()) {
					if(k>0){
						lista += ",";
					}
					lista  += (String)it.next();
					k++;
				}	
        
        qrySentencia.append(	
						"SELECT " + 
						"	ic_documento, "  +
						"  ig_numero_docto, "  +
						"  fn_monto "  +
						"FROM "  + 
						"  com_documento "  +
						"WHERE "  +
						"	ic_documento IN ( "+lista+" )");

				registros = con.queryDB(qrySentencia.toString());
				while(registros != null && registros.next()){
					
					String icDocumento 	= registros.getString("ic_documento");
					String igNumeroDocto = registros.getString("ig_numero_docto");
					String fnMonto			= registros.getString("fn_monto");
					
					HashMap registro = new HashMap();
					registro.put("IG_NUMERO_DOCTO", igNumeroDocto);
					registro.put("FN_MONTO",        fnMonto);
					 
					resultado.put(icDocumento,registro);
          
				}
				con.cierraStatement();
		}catch(Exception e){
			
			System.out.println("13forma1_popm.jsp::getDatosNotasCredito(Exception)");
			System.out.println("13forma1_popm.jsp::getDatosNotasCredito.listaClavesNotas = <"+listaClavesNotas+">");
			e.printStackTrace();
			throw new Exception("Error al Consultar Notas de Credito");
			
		}finally{
			if(registros != null) registros.close();
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13forma1_popm.jsp::getDatosNotasCredito(S)");
		}

		return resultado;	
	}
	
	public List getDetalleNotaAplicada(String icNotaCredito)
	throws Exception{
		ArrayList lista = new ArrayList();
		lista.add(icNotaCredito);
		return getDetalleNotaAplicada(lista);
	}
	
	public List getDetalleNotaAplicada(ArrayList listaIdNotaCredito)
    	throws Exception{
		
		System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada(E)");
		ArrayList lista = new ArrayList();
		
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		
		// Validar que la lista no venga vacia
		if(listaIdNotaCredito == null || (listaIdNotaCredito.size() == 0)){
			System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada(S)");
			return lista;
		}
		// Validar que haya al menos un parametro valido
		boolean hayParametrosValidos = false;
		for(int j=0;j<listaIdNotaCredito.size();j++){
				String icNotaCredito = (String) listaIdNotaCredito.get(j);
				if(icNotaCredito != null && !icNotaCredito.equals("")){
					hayParametrosValidos = true;
				}
		}
		if(!hayParametrosValidos){
			System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada(S)");
			return lista;
		}
		
		try{
 
			StringBuffer listaQuestionSign = new StringBuffer();
			for(int i=0;i<listaIdNotaCredito.size();i++){
				String icNotaCredito = (String) listaIdNotaCredito.get(i);
				if(icNotaCredito != null && !icNotaCredito.equals("")){
					if(i>0) listaQuestionSign.append(",");
					listaQuestionSign.append("?");
					lVarBind.add(icNotaCredito);
				}
			}
			
			con.conexionDB();
			qrySentencia.append(
					"SELECT                "  +
					"	IC_NOTA_CREDITO,    "  +
					"  IC_DOCUMENTO,       "  +
					"  FN_MONTO_APLICADO,  "  +
					"  FN_SALDO_DOCTO      "  +
					"FROM                  "  +
					"	COMREL_NOTA_DOCTO   "  +
					"WHERE                 "  +
					"	IC_NOTA_CREDITO in ("+listaQuestionSign.toString()+") ");
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next()){
				
				String idNotaCredito  	= registros.getString("IC_NOTA_CREDITO");
				String idDocumento 	  	= registros.getString("IC_DOCUMENTO");
				String montoAplicado		= registros.getString("FN_MONTO_APLICADO");
				String saldoDocto		  	= registros.getString("FN_SALDO_DOCTO");
				
				lista.add(
					new NotaCreditoMultiple(
						idNotaCredito,
						idDocumento,
						montoAplicado,
						saldoDocto
					)
				);
			}
			
		}catch(Exception e){
			System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada(Exception)");
			System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada.icNotaCredito = <"+listaIdNotaCredito+">");
			e.printStackTrace();
			throw new Exception("Error al consultar datos de las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13forma1_popm.jsp::getDetalleNotaAplicada(S)");
		}
		
		return lista;
	}
	
	public List getNotasDeCredito(String icDocumento)
     throws Exception{
		  
		 System.out.println("13forma1_popm.jsp::getNotasDeCredito(E)");
		 String []icDocumentos 	= new String[1];
		 icDocumentos[0] 			= icDocumento;
		 List 	lista 			= getNotasDeCredito(icDocumentos);
		 System.out.println("13forma1_popm.jsp::getNotasDeCredito(S)");
		 
		 return lista;
	}
	 
	public List getNotasDeCredito(String []icDocumentos)
     throws Exception{
		
		System.out.println("13forma1_popm.jsp::getNotasDeCredito(E)");
		ArrayList lista = new ArrayList();
		
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		
		if(icDocumentos == null || icDocumentos.length == 0){
			System.out.println("13forma1_popm.jsp::getNotasDeCredito(S)");
			return lista;
		}
		
		try{
			
			con.conexionDB();
			
			StringBuffer listaVariablesBind = new StringBuffer();
			
			for(int i=0;i<icDocumentos.length;i++){
				if(i>0) listaVariablesBind.append(",");
				listaVariablesBind.append("?");
			}
			
			qrySentencia.append(
					"SELECT                				"  +
					"	DISTINCT IC_NOTA_CREDITO    	"  +
					"FROM                  				"  +
					"	COMREL_NOTA_DOCTO   				"  +
					"WHERE                 				"  +
					"	IC_DOCUMENTO IN ("+listaVariablesBind.toString()+") ");

			for(int i=0;i<icDocumentos.length;i++){
				String icDocumento = (String)icDocumentos[i]; 
				lVarBind.add(icDocumento);
			}
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next()){
				
				String idNotaCredito  = registros.getString("IC_NOTA_CREDITO");
				lista.add(idNotaCredito);
			}
			
		}catch(Exception e){
			System.out.println("13forma1_popm.jsp::getNotasDeCredito(Exception)");
			System.out.println("13forma1_popm.jsp::getNotasDeCredito.icDocumento = <"+icDocumentos+">");
			e.printStackTrace();
			throw new Exception("Error al consultar datos de las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13forma1_popm.jsp::getNotasDeCredito(S)");
		}
		
		return lista;
	}
	/**
	 * Metodo que optiene informacion de Notas de credito a los documentos.
	 * @params  Recibe dos cadena, los documentos y las notas
	 * return Regresa informacion para mostrar un pop-pup en la captura Seleccion de Documentos.
	 **/
	public Registros getDatosNotas(String icDoctos[], String icNotas[]){
		AccesoDB con = new AccesoDB();
		StringBuffer visRel;
		StringBuffer query;
		int i = 0, j = 0;
		try {
			visRel = new StringBuffer();
			for(i=0;i<icDoctos.length;i++){
				String icDocto = icDoctos[i];
				if(!"".equals(icNotas[i])){
					Vector vecNotas = Comunes.explode(",",icNotas[i]);
					for(j=0;j<vecNotas.size();j++){
						if(!"".equals(visRel.toString()))
							visRel.append(" UNION ALL ");
						visRel.append(" SELECT "+icDocto+" AS ic_documento, "+vecNotas.get(j).toString()+" AS ic_nota FROM dual");
					}
				}
			}
			con.conexionDB();
			query = new StringBuffer();

			query.append("SELECT /*+use_nl(d,n,m,rel)*/"   +
							"	(d.ig_numero_docto || ' - Monto: $' || d.fn_monto) AS doctoAplicadoMonto"   +
							"	,n.ig_numero_docto AS numNota"   +
							"	,TO_CHAR(n.df_fecha_docto,'dd/mm/yyyy') AS fechaEmision"   +
							"	,m.cd_nombre AS moneda"   +
							"	,n.fn_monto AS montoNota"   +
							"	,'Nota de credito' AS tipoFactoraje"   +
							"	,d.ig_numero_docto AS doctoAplicado"   +
							"	,d.fn_monto AS montoDocto"   +
							"	,d.fn_monto - n.fn_monto AS saldo"   +
							" FROM com_documento d"   +
							"  ,com_documento n"   +
							"  ,comcat_moneda m"   +
							"  ,("+visRel.toString()+") rel"   +
							" WHERE d.ic_documento = rel.ic_documento"   +
							" AND n.ic_documento = rel.ic_nota"   +
							" AND n.ic_moneda = m.ic_moneda");
			Registros reg = con.consultarDB(query.toString());

			return reg;

		} catch (Exception e) {
			throw new AppException("Ocurrio un error al obtener la informacion de Notas de Credito.", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
		}
	}
}