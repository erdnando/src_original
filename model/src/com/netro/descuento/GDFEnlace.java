package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class GDFEnlace implements EpoEnlace,Serializable {


	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_GDF";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_GDF";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_GDF";	}

	public String getDocuments(){

		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" , CG_CAMPO2"+
				" , '' as cg_campo3"+
				" , '' as cg_campo4"+
				" , '' as cg_campo5"+
				" , '' as ic_if"+
				" , '' AS IC_NAFIN_ELECTRONICO "+
				" from " + getTablaDocumentos();
	}
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+
				" 		(IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR, "+
				"		cg_pyme_epo_interno, fn_monto, df_fecha_venc, " +
				"     cc_acuse, df_fecha_hora_carga, ct_referencia) " +
				"   SELECT ig_numero_docto, "+err.getIgNumeroError()+", '"+err.getCgError()+"',  " + 
				" 		cg_pyme_epo_interno, fn_monto, df_fecha_venc, " +
				"  	'"+err.getCcAcuse()+"', (select nvl(df_fechahora_carga,sysdate)  " + 
				"		from " + getTablaAcuse() + " where cc_acuse = '" +  err.getCcAcuse() + "') " + 
				"		,ct_referencia " +
				"     FROM "+getTablaDocumentos()+" d, " +getTablaAcuse()+" a "+
				"    WHERE ig_numero_docto = '"+err.getIgNumeroDocto()+"' "+
				"      AND cg_pyme_epo_interno = '"+err.getCg_pyme_epo_interno()+"' ";
				//" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"')";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+
				" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
				" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
				" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
				" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+
				"' where CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		return " ";
	}
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }
  

}
