package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class CFEEnlOperados extends EpoPEF implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

        /**
         * Se sobreescribe el metodo de EpoPEF, debido a que en este enlace es necesario
         * especificar el dblink ORALINK en la funci�n para que pueda
         * funcionar adecuadamente.
         * @return Cadena con la secci�n del query que determina el digito identificador.
         */
        public String getCamposDigitoIdentificadorCalc(){
                return "  rellenaCeros@ORANFE_ORALINK(d.ic_epo||'','0000') || rellenaCeros@ORANFE_ORALINK(d.ic_documento||'','00000000000') as " + getCamposDigitoIdentificador();
        }

	public CFEEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_cfe";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,5,6,7,9,16) " +
				" AND df_notificacion < (trunc(sysdate) - 14)";
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9)";
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7)";
	}

	public String getInsertaDoctosOpe(){
		return "insert into "+getTablaDoctosOpe()+" (ic_documento, ic_estatus_docto,"+
				" df_fecha_venc, cg_razon_social_pyme, ig_numero_docto, ic_moneda, fn_monto,"+
				" in_numero_proveedor, df_fecha_docto, cc_acuse, cg_razon_social_if,"+
				" ic_nafin_electronico,cg_campo1,cg_campo2,cg_campo3,cg_campo4" + getCamposPEFOper()+") ";
	}


	public String getDoctosOperados(Hashtable alParamEPO){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    
		return "  SELECT d.ic_documento, 4 AS ic_estatus_docto, d.df_fecha_venc,"   +
			"   SUBSTR(py.cg_razon_social, 1, 70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
			"   pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
			"   a.cc_acuse, SUBSTR(i.cg_razon_social,1,50) AS nombreIf,"   +
			"   TO_CHAR(rn.ic_nafin_electronico) AS ic_nafin_electronico"   +
			"   ,d.cg_campo1"   +
			"   ,d.cg_campo2"   +
			"   ,d.cg_campo3"   +
			"   ,d.cg_campo4 "+
      (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
      (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			"   FROM com_documento d, com_docto_seleccionado ds, com_solicitud s,"   +
			"   com_acuse2 a, comcat_if i, comrel_pyme_epo pe, comrel_nafin rn"   +
			"   ,comcat_pyme py"+
			"   WHERE a.cc_acuse = ds.cc_acuse"   +
			"   AND s.ic_documento = ds.ic_documento"   +
			"   AND ds.ic_documento = d.ic_documento"   +
			"   AND ds.ic_if = rn.ic_epo_pyme_if"   +
			"   AND d.ic_if = i.ic_if"   +
			"   AND d.ic_pyme = pe.ic_pyme"   +
			"   AND d.ic_epo = pe.ic_epo"   +
			"   AND d.ic_pyme = py.ic_pyme"+
			"   AND d.ic_estatus_docto IN (4,16)"   +
			"   AND rn.cg_tipo = 'I'"   +
			"   AND s.df_fecha_solicitud >= TRUNC(SYSDATE) "+
			"   AND s.df_fecha_solicitud < TRUNC(SYSDATE+1) "+
			"  AND d.ic_epo = "+ic_epo;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    
			return " SELECT d.ic_documento, d.ic_estatus_docto, d.df_fecha_venc,"   +
				"  SUBSTR(py.cg_razon_social, 1, 70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
				"  pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
				"  NULL AS acuse,NULL AS IF,NULL AS nafin_electronico"   +
				" ,d.cg_campo1"+
				" ,d.cg_campo2"+
				" ,d.cg_campo3"+
				" ,d.cg_campo4"+
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				" from com_documento d, comhis_cambio_estatus ce, comrel_pyme_epo pe "+
				" ,comcat_pyme py"+
				" where d.ic_documento = ce.ic_documento "+
				" and d.ic_pyme = pe.ic_pyme "+
				" and d.ic_epo = pe.ic_epo "+
				" and ce.dc_fecha_cambio >= TRUNC(SYSDATE)  AND ce.dc_fecha_cambio < TRUNC(SYSDATE) + 1 "+
				" and d.ic_pyme = py.ic_pyme"+
				" and d.ic_estatus_docto in (5,6,7) "+
				" and ce.ic_cambio_estatus in (4,5,6) "+
				" and d.ic_epo = "+ic_epo;
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		String condicion = "";
		//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+

		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-3)";
		} else {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-1)";
		}

		String qrySentencia = " SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"   +
					"  SUBSTR(p.cg_razon_social, 1, 70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
					"  pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
					"  NULL AS acuse, NULL AS razonsocialif,NULL AS ic_nafin_electronico"   +
					" ,d.cg_campo1"+
					" ,d.cg_campo2"+
					" ,d.cg_campo3"+
					" ,d.cg_campo4"+
          (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
          (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
					" from com_documento d, comcat_pyme p, comrel_pyme_epo pe "+
					" where d.ic_pyme = p.ic_pyme "+
					" and d.ic_pyme = pe.ic_pyme "+
					" and d.ic_epo = pe.ic_epo "+
					" and d.ic_estatus_docto = 9 "+
					condicion+
					//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+
					" and d.df_fecha_venc < TRUNC(SYSDATE+"+diasMinimos+")"+
					" and d.ic_epo = "+ic_epo+
					//FODEA 006-Feb2010 Rebos - Se agrego la notificaci�n de documentos publicados como vencidos
					" UNION " +
					" SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"   +
					"  SUBSTR(p.cg_razon_social, 1, 70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
					"  pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
					"  NULL AS acuse, NULL AS razonsocialif,NULL AS ic_nafin_electronico"   +
					" ,d.cg_campo1"+
					" ,d.cg_campo2"+
					" ,d.cg_campo3"+
					" ,d.cg_campo4"+
          (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
          (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
					" from com_documento d, comcat_pyme p, comrel_pyme_epo pe "+
					" where d.ic_pyme = p.ic_pyme "+
					" and d.ic_pyme = pe.ic_pyme "+
					" and d.ic_epo = pe.ic_epo "+
					" and d.ic_estatus_docto = 9 "+
					" and d.df_alta > TRUNC(SYSDATE-1)"+
		      " and d.ic_epo = "+ic_epo;										
		return qrySentencia;
	}

}
