package com.netro.descuento;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/**
 * Archivo con generacion de una solicitud en automatico y el documento aplicado a credito
 *
 * @author:	Edgar Gonzalez Bautista
 */
public class Automatico {
    
	private static final Log LOG = ServiceLocator.getInstance().getLog(Automatico.class);
    
	public static final String PRODUCTO_DSCTO = "1";
	private int proceso;
	private String mensaje;
	private double montoCobro;
	private int documento;
	private double montoDescuentoAcuse;
	private double montoInteresAcuse;
	private double montoRecibirAcuse;
	//Agregados 131202 	--CARP
	private String m_sMontoDscto;
	private String m_sMontoInteres;
	private double m_dMontoRecibir;
	private String m_sPorcAnticipo;
	private String m_sTasa;
	private int m_iPlazo;

	private String sDtosProc = "";

	/**
	 * Descuento Autom�tico de documentos para el pago de creditos en Pedidos
	 * @param epo Clave de la epo
	 * @param pyme Clave de la Pyme
	 * @param intFin Clave del IF
	 * @param Moneda Clave de la moneda
	 * @param claveUsuario Clave del usuario
	 * @param numContrato Numero de Pedido (ig_numero_pedido)
	 * 		Si el par�metro es nulo, la busqueda de documentos no se hace
	 * 		de manera referenciada
	 * @param numCampoAdicionalConReferencia Numero de campo adicional que
	 * 		contiene la referencia.
	 * 		Este par�metro s�lo es considerado si numContrato no es nulo.
	 * @param con Conexion a BD
	 *
	 */
	public Automatico(String epo, String pyme, String intFin, String Moneda,
			String claveUsuario, String numContrato,
			Integer numCampoAdicionalConReferencia, AccesoDB con) {
		proceso = 0;
		mensaje = null;
		montoCobro = 0;
		documento = 0;
		generaProceso(epo, pyme, intFin, Moneda,
				claveUsuario, numContrato,
				numCampoAdicionalConReferencia, con);
	}

	/**
	 * Descuento Autom�tico de documentos para el pago de disposiciones en Obra p�blica.
	 * @param epo Clave de la EPO
	 * @param pyme Clave de la Pyme
	 * @param intFin Clave del IF
	 * @param Moneda Clave de la moneda
	 * @param claveUsuario Clave del usuario con el cul se realiza el descuento autom�tico
	 * @param numContrato Clave de contrato con el que se tiene una referencia,
	 * 		o null si no es referenciado.
	 * @param conexion Conexion a la BD.
	 *
	 */

	public Automatico(String epo, String pyme, String intFin, String Moneda,
			String claveUsuario, String numContrato, AccesoDB con) {
		proceso = 0;
		mensaje = null;
		montoCobro = 0;
		documento = 0;
		Integer numCampoAdicionalConReferencia = null;
		generaProceso(epo, pyme, intFin, Moneda,
				claveUsuario, numContrato,
				numCampoAdicionalConReferencia, con);
	}



	// Agregado 131202	--CARP
	// Para Proceso de Pignoraci�n (Inventarios)
	public Automatico(String esEpo, String esPyme, String esIF, String esMoneda,
			String esFechaPago, String esTipoPiso, String esDiasMinimos,
			AccesoDB eoConexion) {
		proceso 		= 0;
		mensaje 		= null;
		montoCobro 		= 0;
		documento 		= 0;
		m_sMontoDscto 	= "0";
		m_sMontoInteres = "0";
		m_dMontoRecibir = 0;
		m_sPorcAnticipo = "0";
		m_sTasa 		= "0";
		generaProceso(esEpo, esPyme, esIF, esMoneda, esFechaPago, esTipoPiso, esDiasMinimos, eoConexion);
	}

	// Agregado 201202	--CARP
	// Para Proceso de Cobranza (Inventarios)
	public Automatico(String esEpo, String esPyme, String esIF, String esMoneda, AccesoDB eoConexion,
			String esCveUsuario, String esProducto) {
		proceso = 0;
		mensaje = null;
		montoCobro = 0;
		documento = 0;
		generaProceso(esEpo, esPyme, esIF, esMoneda, esCveUsuario, eoConexion, esProducto);
	}

	/**
	 * Se agrega constructor que llama a otro constructor para no afectar la funcionalidad actual
	 */
	public Automatico(String epo, String pyme, String intFin, String Moneda,
			String claveUsuario, String tipoFactoraje, String tipoPiso,
			AccesoDB con, String acuse, String diaDscto, String diasMinimos,
			String sDtosProcesados, Vector vFechasInhabiles,
			int orden, String lsCveProc, String lsAforo,ArrayList alNotas) {
			
		this( epo,  pyme,  intFin,  Moneda, claveUsuario,  tipoFactoraje,  tipoPiso, con,
				acuse,  diaDscto,  diasMinimos, sDtosProcesados,  vFechasInhabiles,
				orden,  lsCveProc,  lsAforo, alNotas,  "");
	}
	
	//Modificado Foda 99 21/12/2004 --JRFH
	public Automatico(String epo, String pyme, String intFin, String Moneda,
			String claveUsuario, String tipoFactoraje, String tipoPiso,
			AccesoDB con, String acuse, String diaDscto, String diasMinimos,
			String sDtosProcesados, Vector vFechasInhabiles,
			int orden, String lsCveProc, String lsAforo,ArrayList alNotas, String cveMandante) {
		//Automatico para el proceso de descuento automatico
		proceso = 0;
		mensaje = null;
		montoCobro = 0;
		documento = 0;
		montoDescuentoAcuse = 0;
		montoInteresAcuse = 0;
		montoRecibirAcuse = 0;   
		//System.out.println("Tamanio Vector Fechas:" + vFechasInhabiles.size());
		if ("N".equals(tipoFactoraje) ) {
			System.out.println("Descuento Normal");
			generaDescuentoNormal(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo,alNotas);
			System.out.println("Termina Normal\n");
		} else if ( "V".equals(tipoFactoraje) ) {
			System.out.println("Descuento Vencido");
			generaDescuentoVencido(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo,alNotas);
			System.out.println("Termina Vencido\n");
		} else if ( "D".equals(tipoFactoraje) ) {
			System.out.println("Descuento Distribuido");
			generaDescuentoDistribuido(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo);
			System.out.println("Termina Distribuido\n");
		} else if ( "I".equals(tipoFactoraje) ) {
			System.out.println("Descuento Vencido INFONAVIT");
			generaDescuentoVencidoINFO(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo);
			System.out.println("Termina Vencido INFONAVIT\n");
		} else if ( "M".equals(tipoFactoraje) ) {
			System.out.println("Descuento Factoraje con Mandato");
			generaDescuentoMandato(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo, cveMandante);//FODEA 046-2009 FVR
			System.out.println("Termina Factoraje con Mandato\n");
		} else if ( "A".equals(tipoFactoraje) ) {
			System.out.println("Descuento Factoraje Autom�tico ");
			generaDescuentoFacIF(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, orden, lsCveProc, lsAforo,alNotas);
			System.out.println("Termina Factoraje con Autom�tico \n");
		}
	}

	public Automatico(String epo, String pyme, String intFin, String Moneda,
			String claveUsuario, String tipoFactoraje, String tipoPiso,
			AccesoDB con, String acuse, String diaDscto, String diasMinimos,
			String sDtosProcesados, Vector vFechasInhabiles, String lsCveProc,
			String lsAforo) {
		//Automatico para el proceso de descuento automatico, Documentos Programados
		proceso = 0;
		mensaje = null;
		montoCobro = 0;
		documento = 0;
		montoDescuentoAcuse = 0;
		montoInteresAcuse = 0;
		montoRecibirAcuse = 0;
		//System.out.println("Tamanio Vector Fechas:" + vFechasInhabiles.size());
		if ("N".equals(tipoFactoraje) ) {
			System.out.println("Descuento Normal PROGRAM");
			generaDescuentoNormalProgram(epo, pyme, intFin, Moneda, claveUsuario, tipoPiso, con, acuse, diaDscto, diasMinimos, sDtosProcesados, vFechasInhabiles, lsCveProc, lsAforo);
			System.out.println("Termina Normal PROGRAM\n");
		}
	}

	private void generaDescuentoNormalProgram(String icEpo, String iNoCliente, String icIf,
								String icMoneda, String iNoUsuario, String tipoPiso,
								AccesoDB con, String acuse, String diaDscto,
								String diasMinimos, String sDtosProcesados,
								Vector vFechasInhabiles, String lsCveProc, String lsAforo) {
		try {
			System.out.println("Automatico::generaDescuentoNormalProgram(E)");
			/* Declaracion de variables */
			String montoBenef = null; ///* Se emplea en el metodo de seleccionar el documento */
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
		   	String aforo = obtenerAforo(icEpo, icIf, icMoneda, con);
			//System.out.println("Aforo: "+aforo);
			float valorTC = obtenerTipoCambio(icMoneda, con);
			//System.out.println("valor Tipo Cambio: "+valorTC);
			try {
				valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf);
			} catch (NafinException ne) {
				CSeleccionDocumentoBean.ovSinTasaProgram(lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"N",icIf,con);
			}
			String icDocumento="";
			if  (valoresTasas.size()>0) {
				try {
					Vector datosDocumento = obtenerDocumentoProgram(lsCveProc, diaDscto, icMoneda, aforo, valoresTasas, iNoCliente, icEpo, "N", tipoPiso, icIf, con, diasMinimos, sDtosProcesados);
					System.out.println("Tama�o del Vector: "+datosDocumento.size());
					if (datosDocumento.size() > 0) {
						String totalMonto 		= datosDocumento.get(2).toString();
						String totalDescuento 	= datosDocumento.get(3).toString();
						String totalIntereses 	= datosDocumento.get(4).toString();
						String totalRecibir 	= datosDocumento.get(5).toString();
						String fnMontoDscto		= datosDocumento.get(6).toString();
						String inImporteInteres = datosDocumento.get(7).toString();
						String inImporteRecibir	= datosDocumento.get(8).toString();
						icDocumento				= datosDocumento.get(9).toString();
						String fnPorcAnticipo	= datosDocumento.get(10).toString();
						String sfDsctoAuto		= datosDocumento.get(14).toString();
						String dcFechaTasa 		= datosDocumento.get(15).toString();
						String cgRelMat			= datosDocumento.get(16).toString();
						String tasaAceptada		= datosDocumento.get(17).toString();
						String cgPuntos			= datosDocumento.get(18).toString();
						String tipoTasa			= datosDocumento.get(19).toString();
						String puntosPref		= datosDocumento.get(20).toString();
						String montoAnt			= datosDocumento.get(21).toString();
						String icNotasAplicadas	= datosDocumento.get(22).toString();
						String fechaVenc		= datosDocumento.get(1).toString();

						//System.out.println("Total Monto: "+totalMonto);
						//System.out.println("Total Descuento: "+totalDescuento);
						//System.out.println("Total Intereses: "+totalIntereses);
						//System.out.println("Total Recibir: "+totalRecibir);
						//System.out.println("Monto Descuento: "+fnMontoDscto);
						//System.out.println("Importe Interes: "+inImporteInteres);
						//System.out.println("Importe Recibir: "+inImporteRecibir);
						//System.out.println("icDocumento: "+icDocumento);
						//System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
						//System.out.println("\n Fecha Dscto Automatico: "+sfDsctoAuto);
						//String acuse = obtenerAcuse(totalDescuento, totalIntereses, totalRecibir, icMoneda, iNoUsuario, con);
						//System.out.println("Acuse Generado: "+acuse);

//						if(diaDscto.equals("P") || ( diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto)) ) {

							seleccionarDocumento(
								lsCveProc,		diasMinimos,		diaDscto,		icEpo,				icIf,
								acuse, 			dcFechaTasa,		tasaAceptada, 	inImporteInteres,	inImporteRecibir,
								cgRelMat, 		cgPuntos, 			valorTC, 		icDocumento, 		fnMontoDscto,
								fnPorcAnticipo, tipoTasa, 			puntosPref, 	montoBenef,			montoAnt,
								totalMonto,		icNotasAplicadas,	fechaVenc,		iNoUsuario,			con);
/*
						} else if (vFechasInhabiles != null) {
							boolean bseleccionaDocto = false;
							for (int i=0; i < vFechasInhabiles.size(); i++) {
								if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
									seleccionarDocumento(
										lsCveProc,	diasMinimos,	diaDscto,	icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
													inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													fnPorcAnticipo, tipoTasa, puntosPref, montoBenef,montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con);
									bseleccionaDocto = true;
									break;
								}
							}
							if (!bseleccionaDocto){
								this.sDtosProc = icDocumento;
							} else {
								System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
							}
						} else {
							this.sDtosProc = icDocumento;
						}
*/
					}
				} catch(ArrayIndexOutOfBoundsException aioobe) { aioobe.printStackTrace(); }
			} else {
				mensaje = "No hay TASAS X PLAZO en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf ;
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			System.out.println("Automatico::generaDescuentoNormalProgram(Exception)");
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		} finally {
			System.out.println("Automatico::generaDescuentoNormalProgram(S)");
		}
	}//Fin generaDescuentoNormal


	private void generaDescuentoNormal(
								String icEpo, 			String iNoCliente, 		String icIf,
								String icMoneda, 		String iNoUsuario, 		String tipoPiso,
								AccesoDB con, 			String acuse, 			String diaDscto,
								String diasMinimos, 	String sDtosProcesados,	Vector vFechasInhabiles, 	
								int orden, 				String lsCveProc,		String lsAforo, 
								ArrayList alNotas) {
		try {
			/* Declaracion de variables */
			String montoBenef = null; ///* Se emplea en el metodo de seleccionar el documento */
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
		   	String aforo = obtenerAforo(icEpo, icIf, icMoneda, con);
			//System.out.println("Aforo: "+aforo);
			float valorTC = obtenerTipoCambio(icMoneda, con);
			//System.out.println("valor Tipo Cambio: "+valorTC);
			String existValIf = "";
			try {
				valoresTasas = selec.ovgetTasaAceptada(iNoCliente, icEpo, icMoneda, icIf, con);
			} catch (NafinException ne) {
				CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"N",icIf,con);
			}
			String icDocumento="";
			if  (valoresTasas.size()>0) {
				try {
					Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto, icMoneda, aforo, valoresTasas, iNoCliente, icEpo, "N", tipoPiso, icIf, con, diasMinimos, sDtosProcesados,alNotas);
					System.out.println("Tama�o del Vector: "+datosDocumento.size());
					if (datosDocumento.size() > 0) {
						String 		totalMonto 								= datosDocumento.get(2).toString();
						String 		totalDescuento 						= datosDocumento.get(3).toString();
						String 		totalIntereses 						= datosDocumento.get(4).toString();
						String 		totalRecibir 							= datosDocumento.get(5).toString();
						String 		fnMontoDscto							= datosDocumento.get(6).toString();
						String 		inImporteInteres 						= datosDocumento.get(7).toString();
						String 		inImporteRecibir						= datosDocumento.get(8).toString();
						icDocumento												= datosDocumento.get(9).toString();
						String 		fnPorcAnticipo							= datosDocumento.get(10).toString();
						String 		sfDsctoAuto								= datosDocumento.get(14).toString();
						String 		dcFechaTasa 							= datosDocumento.get(15).toString();
						String 		cgRelMat									= datosDocumento.get(16).toString();
						String 		tasaAceptada							= datosDocumento.get(17).toString();
						String 		cgPuntos									= datosDocumento.get(18).toString();
						String 		tipoTasa									= datosDocumento.get(19).toString();
						String 		puntosPref								= datosDocumento.get(20).toString();
						String 		montoAnt									= datosDocumento.get(21).toString();
						String 		icNotasAplicadas						= datosDocumento.get(22).toString();
						String 		fechaVenc								= datosDocumento.get(1).toString();
						ArrayList 	lsNotasDeCreditoMultiplesDoctos 	= (ArrayList)datosDocumento.get(23);

						System.out.println("Total Monto: "+totalMonto);
						System.out.println("Total Descuento: "+totalDescuento);
						System.out.println("Total Intereses: "+totalIntereses);
						System.out.println("Total Recibir: "+totalRecibir);
						System.out.println("Monto Descuento: "+fnMontoDscto);
						System.out.println("Importe Interes: "+inImporteInteres);
						System.out.println("Importe Recibir: "+inImporteRecibir);
						System.out.println("icDocumento: "+icDocumento);
						System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
						System.out.println("\n Fecha Dscto Automatico: "+sfDsctoAuto);
						//String acuse = obtenerAcuse(totalDescuento, totalIntereses, totalRecibir, icMoneda, iNoUsuario, con);
						System.out.println("Acuse Generado: "+acuse);

						System.out.println("diaDscto: "+diaDscto);
						System.out.println("sfDsctoAuto: "+sfDsctoAuto);
						System.out.println("icIf: "+icIf); 
						//existValIf = selec.operaEpoConMandato(iNoCliente,icEpo);//MODIFICACION FODEA 041-2009 FVR
						existValIf = selec.operaEpoConMandato(iNoCliente,icEpo,icMoneda);//FODEA 015 - 2011 ACF
						String operaMontoMenorIF =  selec.operaMontosMenoresIF(icIf );
						
						System.out.println("icEpo>>>>>>>>>"+icEpo);
						System.out.println("iNoCliente>>>>>>>>>"+iNoCliente);
						System.out.println("existValIf>>>>>>>>>"+existValIf);
						System.out.println("operaMontoMenorIF>>>>>>>>>"+operaMontoMenorIF);						
						
						
							
						  if( ("".equals(existValIf) && diaDscto.equals("P")) || (!"".equals(existValIf) && diaDscto.equals("P")) || ( diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto))  ||  "A".equals(diaDscto)  ) {
								
								if( "N".equals(operaMontoMenorIF)  && Double.parseDouble(inImporteInteres)<0.01 ){
										System.out.println(" icIf == "+icIf+"   Importe de Intereses es menor a 0.01  el documento no podra descontarse ");	
										this.sDtosProc = icDocumento;
								}else  {
								
									//    NO ES MANDATO Y SE TOMA PRIMER DIA            O   ES MANDATO Y TIENE DSCTO AUTOM ACTIVADO         O   ESTA PARAMETRIZADO COMO ULTIMO DIA O COMO MANDATO CON DSCTO AUTOM DESACTIVADO
									seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
													inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													fnPorcAnticipo, tipoTasa, puntosPref, montoBenef,montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con,lsNotasDeCreditoMultiplesDoctos);
								}
								
								} else if (vFechasInhabiles != null) {
									boolean bseleccionaDocto = false;
									for (int i=0; i < vFechasInhabiles.size(); i++) {
										if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
											seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
															inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
															fnPorcAnticipo, tipoTasa, puntosPref, montoBenef,montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con,lsNotasDeCreditoMultiplesDoctos);
											bseleccionaDocto = true;
											break;
										}
									}
									if (!bseleccionaDocto){
										this.sDtosProc = icDocumento;
									} else {
										System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
									}
								} else {
									this.sDtosProc = icDocumento;
								}						
					}
				} catch(ArrayIndexOutOfBoundsException aioobe) { aioobe.printStackTrace(); }
			} else {
				mensaje = "No hay TASAS X PLAZO en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf ;
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaDescuentoNormal



	private void generaDescuentoVencido(String icEpo, String iNoCliente, String icIf,
								String icMoneda, String iNoUsuario, String tipoPiso,
								AccesoDB con, String acuse, String diaDscto,
								String diasMinimos, String sDtosProcesados,
								Vector vFechasInhabiles, int orden, String lsCveProc, String lsAforo, 
							ArrayList alNotas) {
		try {
			//Declaracion de variables
		   	String aforo = "1";
			//System.out.println("Aforo: "+aforo);
			String montoBenef = null;
			float valorTC = obtenerTipoCambio(icMoneda, con);
			//System.out.println("valor Tipo Cambio: "+valorTC);
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
			try
			{	valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf,con);
			}	catch(NafinException ne)
			{	CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"V",icIf,con);
			}
			String icDocumento="";
			if (valoresTasas.size() > 0) {
				Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto,icMoneda, aforo, valoresTasas , iNoCliente, icEpo, "V", tipoPiso, icIf, con, diasMinimos, sDtosProcesados,alNotas);
				if (datosDocumento.size() > 0) {
					String totalMonto 		= datosDocumento.get(2).toString();
					//System.out.println("Total Monto: "+totalMonto);
					String totalDescuento 	= datosDocumento.get(3).toString();
					//System.out.println("Total Descuento: "+totalDescuento);
					String totalIntereses 	= datosDocumento.get(4).toString();
					//System.out.println("Total Intereses: "+totalIntereses);
					String totalRecibir 	= datosDocumento.get(5).toString();
					//System.out.println("Total Recibir: "+totalRecibir);
					String fnMontoDscto		= datosDocumento.get(6).toString();
					//System.out.println("Monto Descuento: "+fnMontoDscto);
					String inImporteInteres = datosDocumento.get(7).toString();
					//System.out.println("Importe Interes: "+inImporteInteres);
					String inImporteRecibir		= datosDocumento.get(8).toString();
					//System.out.println("Importe Recibir: "+inImporteRecibir);
					icDocumento		= datosDocumento.get(9).toString();
					//System.out.println("icDocumento: "+icDocumento);
					String fnPorcAnticipo	= datosDocumento.get(10).toString();
					//System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
					String sfDsctoAuto		= datosDocumento.get(14).toString();
					//System.out.println("Fecha Dscto Automatico: "+sfDsctoAuto);
					String dcFechaTasa 		= datosDocumento.get(15).toString();
					//System.out.println("Clave Tasa: "+dcFechaTasa);
					String cgRelMat			= datosDocumento.get(16).toString();
					//System.out.println("Rel. Mat.: "+cgRelMat);
					String tasaAceptada		= datosDocumento.get(17).toString();
					//System.out.println("Tasa Aceptada: "+tasaAceptada);
					String cgPuntos			= datosDocumento.get(18).toString();
					//System.out.println("Puntos: "+cgPuntos);
					String tipoTasa			= datosDocumento.get(19).toString();
					String puntosPref		= datosDocumento.get(20).toString();
					
					// Fodea 039 - 2010
					String 		montoAnt									= datosDocumento.get(21).toString();
					String 		icNotasAplicadas						= datosDocumento.get(22).toString();
					String 		fechaVenc								= datosDocumento.get(1).toString();
					ArrayList 	lsNotasDeCreditoMultiplesDoctos 	= (ArrayList)datosDocumento.get(23);
					
					if(diaDscto.equals("P") || (diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto)) ) {
					seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
										inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
										"100", "B", "0", montoBenef, montoAnt, totalMonto, icNotasAplicadas, fechaVenc, iNoUsuario, con, lsNotasDeCreditoMultiplesDoctos);
					} else if (vFechasInhabiles != null) {
						boolean bseleccionaDocto = false;
						for (int i=0; i < vFechasInhabiles.size(); i++) {
							if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
								seleccionarDocumento(lsCveProc, diasMinimos,diaDscto,icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
													inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													"100", "B", "0", montoBenef, montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con,lsNotasDeCreditoMultiplesDoctos);
								bseleccionaDocto = true;
								break;
							}
						}
						if (!bseleccionaDocto){
							this.sDtosProc = icDocumento;
						} else {
							System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
						}
					} else {
						this.sDtosProc = icDocumento;
					}
				}

			} else {
				mensaje = "No hay tasas por plazo en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf;
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaDescuentoVencido
  
	private void generaDescuentoMandato(String icEpo, String iNoCliente, String icIf,
								String icMoneda, String iNoUsuario, String tipoPiso,
								AccesoDB con, String acuse, String diaDscto,
								String diasMinimos, String sDtosProcesados,
								Vector vFechasInhabiles, int orden, String lsCveProc, String lsAforo, String cveMandante) {
		try {
			//Declaracion de variables
		   	String aforo = "1";
			//System.out.println("Aforo: "+aforo);
			String montoBenef = null;
			float valorTC = obtenerTipoCambio(icMoneda, con);
			//System.out.println("valor Tipo Cambio: "+valorTC);
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
			try
			{	valoresTasas = selec.ovgetTasaAceptada(cveMandante,icEpo,icMoneda,icIf,con,"M");
			}	catch(NafinException ne)
			{	CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"M",icIf,con);
			}
			String icDocumento="";
			if (valoresTasas.size() > 0) {
				Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto,icMoneda, aforo, valoresTasas , iNoCliente, icEpo, "M", tipoPiso, icIf, con, diasMinimos, sDtosProcesados);
				if (datosDocumento.size() > 0) {
					String totalMonto 		= datosDocumento.get(2).toString();
					//System.out.println("Total Monto: "+totalMonto);
					String totalDescuento 	= datosDocumento.get(3).toString();
					//System.out.println("Total Descuento: "+totalDescuento);
					String totalIntereses 	= datosDocumento.get(4).toString();
					//System.out.println("Total Intereses: "+totalIntereses);
					String totalRecibir 	= datosDocumento.get(5).toString();
					//System.out.println("Total Recibir: "+totalRecibir);
					String fnMontoDscto		= datosDocumento.get(6).toString();
					//System.out.println("Monto Descuento: "+fnMontoDscto);
					String inImporteInteres = datosDocumento.get(7).toString();
					//System.out.println("Importe Interes: "+inImporteInteres);
					String inImporteRecibir		= datosDocumento.get(8).toString();
					//System.out.println("Importe Recibir: "+inImporteRecibir);
					icDocumento		= datosDocumento.get(9).toString();
					//System.out.println("icDocumento: "+icDocumento);
					String fnPorcAnticipo	= datosDocumento.get(10).toString();
					//System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
					String sfDsctoAuto		= datosDocumento.get(14).toString();
					//System.out.println("Fecha Dscto Automatico: "+sfDsctoAuto);
					String dcFechaTasa 		= datosDocumento.get(15).toString();
					//System.out.println("Clave Tasa: "+dcFechaTasa);
					String cgRelMat			= datosDocumento.get(16).toString();
					//System.out.println("Rel. Mat.: "+cgRelMat);
					String tasaAceptada		= datosDocumento.get(17).toString();
					//System.out.println("Tasa Aceptada: "+tasaAceptada);
					String cgPuntos			= datosDocumento.get(18).toString();
					//System.out.println("Puntos: "+cgPuntos);
					String tipoTasa			= datosDocumento.get(19).toString();
					String puntosPref		= datosDocumento.get(20).toString();
					if(diaDscto.equals("P") || (diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto)) ) {
					seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
										inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
										"100", "B", "0", montoBenef, con);
					} else if (vFechasInhabiles != null) {
						boolean bseleccionaDocto = false;
						for (int i=0; i < vFechasInhabiles.size(); i++) {
							if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
								seleccionarDocumento(lsCveProc, diasMinimos,diaDscto,icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
													inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													"100", "B", "0", montoBenef, con);
								bseleccionaDocto = true;
								break;
							}
						}
						if (!bseleccionaDocto){
							this.sDtosProc = icDocumento;
						} else {
							System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
						}
					} else {
						this.sDtosProc = icDocumento;
					}
				}

			} else {
				mensaje = "No hay tasas por plazo en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf;
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaDescuentoMandato


	private void generaDescuentoDistribuido(String icEpo, String iNoCliente, String icIf,
								String icMoneda, String iNoUsuario, String tipoPiso,
								AccesoDB con, String acuse, String diaDscto,
								String diasMinimos, String sDtosProcesados,
								Vector vFechasInhabiles, int orden, String lsCveProc, String lsAforo)
	{
	  	try {
			//Declaracion de variables
		   	String aforo = "1";
			//System.out.println("Aforo: "+aforo);
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
						
			try
			{	valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf,con);
			} catch (NafinException ne)
			{  /*** REGISTRA EL ERROR DE QUE POR EPO,IF,MONEDA,PYME,TIPODESC NO HAY NINGUNA TASA ****/
				CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"D",icIf,con);
			}
			String icDocumento="";
			if (valoresTasas.size() > 0) {
				Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto,icMoneda, aforo, valoresTasas, iNoCliente, icEpo, "D", tipoPiso, icIf, con, diasMinimos, sDtosProcesados);
												
				if (datosDocumento.size() > 0) {
				    	String totalMonto 		= datosDocumento.get(2).toString();
					//System.out.println("Total Monto: "+totalMonto);
					String totalDescuento 	= datosDocumento.get(3).toString();
					//System.out.println("Total Descuento: "+totalDescuento);
					String totalIntereses 	= datosDocumento.get(4).toString();
					//System.out.println("Total Intereses: "+totalIntereses);
					String totalRecibir 	= datosDocumento.get(5).toString();
					//System.out.println("Total Recibir: "+totalRecibir);
					String fnMontoDscto		= datosDocumento.get(6).toString();
					//System.out.println("Monto Descuento: "+fnMontoDscto);
					String inImporteInteres = datosDocumento.get(7).toString();
					//System.out.println("Importe Interes: "+inImporteInteres);
					String inImporteRecibir	= datosDocumento.get(8).toString();
					//System.out.println("Importe Recibir: "+inImporteRecibir);
					icDocumento		= datosDocumento.get(9).toString();
					//System.out.println("icDocumento: "+icDocumento);
					String fnPorcAnticipo	= datosDocumento.get(10).toString();
					//System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
					String importeRecBenef	= datosDocumento.get(13).toString();
					//System.out.println("Monto Recibir Beneficiario: "+importeRecBenef);
					String sfDsctoAuto		= datosDocumento.get(14).toString();
					//System.out.println("Fecha Dscto Automatico: "+sfDsctoAuto);
					String dcFechaTasa 		= datosDocumento.get(15).toString();
					//System.out.println("Clave Tasa: "+dcFechaTasa);
					String cgRelMat			= datosDocumento.get(16).toString();
					//System.out.println("Rel. Mat.: "+cgRelMat);
					String tasaAceptada		= datosDocumento.get(17).toString();
					//System.out.println("Tasa Aceptada: "+tasaAceptada);
					String cgPuntos			= datosDocumento.get(18).toString();
					//System.out.println("Puntos: "+cgPuntos);
					String tipoTasa			= datosDocumento.get(19).toString();
					String puntosPref		= datosDocumento.get(20).toString();
					float valorTC = obtenerTipoCambio(icMoneda, con);
					
					String icBeneficiario = datosDocumento.get(24).toString();	 //2018_02
					String numContrato = datosDocumento.get(25).toString();	 //2018_02	
					
					//if( selec.getBenefAutoxIF(icEpo,  icIf, iNoCliente,  icBeneficiario,  con)) {   //NE_2018_002 se quita 
					String ctaAutoBeneIF = selec.getCtaAutoIFBene( icIf, icEpo, iNoCliente, icBeneficiario, icMoneda,  con);	//NE_2018_002					
					boolean exiContratoAuto = selec.getListContratosDist(icIf, icEpo, iNoCliente, icBeneficiario, icMoneda,  con, numContrato); //NE_2018_002
					
					LOG.debug ("pyme:: "+iNoCliente +" icBeneficiario::: "+icBeneficiario  +"  numContrato:: "+numContrato+"  ctaAutoBeneIF:: "+ctaAutoBeneIF+"  exiContratoAuto:: "+exiContratoAuto);
					LOG.debug ("inImporteInteres::::: "+inImporteInteres );
				    String operaMontoMenorIF =  selec.operaMontosMenoresIF(icIf );
					LOG.debug ("operaMontoMenorIF::::: "+operaMontoMenorIF );
										
					if( "N".equals(operaMontoMenorIF)  && Double.parseDouble(inImporteInteres)<0.01 ){
						System.out.println(" icIf == "+icIf+"   Importe de Intereses es menor a 0.01  el documento no podra descontarse ");						
						this.sDtosProc = icDocumento;
					}else  {
						if ("S".equals(ctaAutoBeneIF)) { //NE_2018_002
	
							if (!"".equals(numContrato)) { //NE_2018_002
	
								if (exiContratoAuto) { //NE_2018_002
	
									if (diaDscto.equals("P") ||
										 (diaDscto.equals("U") &&
										  ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto))) {
										seleccionarDocumento(lsCveProc, diasMinimos, diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada,
																	inImporteInteres, inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
																	fnPorcAnticipo, tipoTasa, puntosPref, importeRecBenef, con);
									} else if (vFechasInhabiles != null) {
										boolean bseleccionaDocto = false;
										for (int i = 0; i < vFechasInhabiles.size(); i++) {
											if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto)) {
												seleccionarDocumento(lsCveProc, diasMinimos, diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada,
																			inImporteInteres, inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
																			fnPorcAnticipo, tipoTasa, puntosPref, importeRecBenef, con);											
												bseleccionaDocto = true;
												break;
											}
										}
										if (!bseleccionaDocto) {
											this.sDtosProc = icDocumento;
											/* No selecciono el documento porque no era momento de cobrarse */
										} else {
											LOG.debug("TOMO EL DOCTO POR DIAS INHABILES");
										}
									} else {
										this.sDtosProc = icDocumento;
										/* No selecciono el documento porque no era momento de cobrarse */
									}
	
								} else {
									
									LOG.debug("El IF: " + icIf + " no ha autorizado al Beneficiario: " + icBeneficiario +
												 " el n�mero de contrato: " + numContrato + " del documento " + icDocumento + " que public� la EPO: " +
												 icEpo + " para la PYME: " + iNoCliente); //NE_2018_002	
									this.sDtosProc = icDocumento; 
								}
	
							} else {
								LOG.debug(" El registro que intenta seleccionar no tiene contrato asociado: del documento "+icDocumento+" que public� la EPO: "+icEpo+" para el Proveedor: "+iNoCliente+".");//NE_2018_002
							  this.sDtosProc = icDocumento;
							}
						} else {
							LOG.debug("El IF::  " + icIf + " EPO: " + icEpo + " PYME: " + iNoCliente + " y Beneficiario: " + icBeneficiario +
										 " no tienen relaci�n."); //NE_2018_002
							this.sDtosProc = icDocumento;
						}
					
					}
				}/*Fin de cuando se encuentra un documento*/

			} else {
				mensaje = "NO hay tasa en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf ;
				this.sDtosProc = icDocumento;
				LOG.debug(mensaje);
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaDescuentoDistribuido



	/**
	 * Obtiene el porcentaje de aforo.
	 * @param icEpo Clave de la epo
	 * @param icIf Clave del IF
	 * @param icMoneda Clave de la Moneda. Solo soporta dos valores: Moneda
	 * 		Nacional (1) y Dolares(54)
	 *
	 */
	private String obtenerAforo(String icEpo, String icIf, String icMoneda, AccesoDB con)
		throws Exception {
	  	try {
	  		PreparedStatement ps = null;
			ResultSet rsAforo = null;
			String aforo = "1";
			String sufijo = "";

			if (icMoneda.equals("1")) { //Moneda Nacional
				sufijo = "";
			} else if (icMoneda.equals("54")) { //D�lares
				sufijo = "_dl";
			}

			//Obtenemos el aforo de la EPO para la Pyme
			String qryAforo =
					" SELECT (in_porc_descuento"+sufijo+" / 100), 'SeleccionDocumentoEJB::obtenerAforo()'"   +
					"   FROM comrel_if_epo"   +
					"  WHERE ic_if = ?"   +
					"    AND ic_epo = ?"  ;					
			ps = con.queryPrecompilado(qryAforo);
			ps.setInt(1,Integer.parseInt(icIf));
			ps.setInt(2,Integer.parseInt(icEpo));
			rsAforo = ps.executeQuery();
			if(rsAforo.next()) {
				aforo = rsAforo.getString(1);
			} else {
				aforo = null;
			}
			rsAforo.close();ps.close();

			if(aforo == null || aforo.equals("1")) {
				qryAforo =
					" SELECT MIN (x.fn_aforo"+sufijo+") / 100, 'SeleccionDocumentoEJB::obtenerAforo()'"   +
					"   FROM (SELECT fn_aforo"+sufijo+" "   +
					"           FROM comrel_producto_epo"   +
					"          WHERE ic_epo = ?"   +
					"            AND ic_producto_nafin = ?"   +
					"         UNION"   +
					"         SELECT fn_aforo"+sufijo+" "   +
					"           FROM comcat_producto_nafin"   +
					"          WHERE ic_producto_nafin = ?) x"  ;
				ps = con.queryPrecompilado(qryAforo);
				long numero1=Long.parseLong(icEpo);
				ps.setLong(1,numero1);
				ps.setInt(2,1);
				ps.setInt(3,1);
				rsAforo = ps.executeQuery();
				ps.clearParameters();
				if (rsAforo.next()) {
					aforo = rsAforo.getString(1);
					System.out.println("AFORO="+rsAforo.getString(1));
				}
				rsAforo.close();ps.close();
			}
			return aforo;
		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerAforo


	private Vector obtenerTasaNP(String icEpo, String icIf, String icMoneda,
								String icPyme, String valorTasa, double puntos,
								double valorTasaDia, AccesoDB con)
		throws Exception
	{
	  	try {
			ResultSet rsTasaNP = null;
			Vector vTasaNP = new Vector();
			String tipoTasa = "B";
			double tasaRes = 0;
			String puntosPrefenciales="0";
			String cgrm = "";
			String cgPuntos = puntos+"";
			// Obtenemos la Tasa Negociada o Preferencial
			String qryTasaNP = "SELECT DISTINCT " +
							 "   TPI.cc_tipo_tasa, " +
							 "   TPI.fn_valor, " +
							 "   TPI.df_aplicacion " +
							 " FROM comrel_cuenta_bancaria CB, " +
							 "   comrel_pyme_if PI, " +
							 "   com_tasa_pyme_if TPI " +
							 " WHERE CB.ic_pyme = " + icPyme +
							 "   AND CB.ic_moneda = " + icMoneda +
							 "   AND CB.ic_cuenta_bancaria = PI.ic_cuenta_bancaria " +
							 "   AND PI.ic_epo = " + icEpo +
							 "   AND PI.ic_if = " + icIf +
							 "   AND UPPER(PI.cs_vobo_if) = UPPER('S') " +
							 "   AND UPPER(PI.cs_borrado) = UPPER('N') " +
							 "   AND UPPER(CB.cs_borrado) = 'N' " +
							 "   AND TPI.ic_epo = PI.ic_epo" +
							 "   AND TPI.ic_if = PI.ic_if" +
							 "   AND TPI.ic_cuenta_bancaria = PI.ic_cuenta_bancaria " +
							 "	 AND TRUNC(NVL(TPI.df_aplicacion, SYSDATE)) = TRUNC(SYSDATE) " +
							 " ORDER BY TPI.cc_tipo_tasa ASC";
			//System.out.println(qryTasaNP);
			rsTasaNP = con.queryDB(qryTasaNP);
			//La primer tasa que se encuentre es la que se toma
			if(rsTasaNP.next()) {
				tipoTasa = rsTasaNP.getString("CC_TIPO_TASA");
				if (tipoTasa.equals("N")) {
					valorTasa = rsTasaNP.getString("FN_VALOR");
					puntos= (double) (Math.round(rsTasaNP.getDouble("FN_VALOR")*100000) - Math.round(valorTasaDia*100000)) / 100000;
				} else if (tipoTasa.equals("P")) {
					double tasaOrig = Double.parseDouble(valorTasa);
					double puntosResta = rsTasaNP.getDouble("FN_VALOR");
					tasaRes = (double) (Math.round(tasaOrig*100000) - Math.round(puntosResta*100000))/100000;
					valorTasa = String.valueOf(tasaRes);
					puntosPrefenciales = rsTasaNP.getString("FN_VALOR");
					puntos = (double) (Math.round(puntos*100000) - Math.round(rsTasaNP.getDouble("FN_VALOR")*100000))/100000;
				}
				cgrm = (puntos>=0)?"+":"-";
				puntos = Math.abs(puntos);
			}
			rsTasaNP.close();
			con.cierraStatement();
			cgPuntos = puntos+"";
			vTasaNP.add(tipoTasa);
			vTasaNP.add(valorTasa);
			vTasaNP.add(puntosPrefenciales);
			vTasaNP.add(cgPuntos);
			vTasaNP.add(cgrm);
			return vTasaNP;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerTasaNP


	private String obtenerCalificacion(String icEpo, String iNoCliente, AccesoDB con)
		throws Exception
	{
	  	try {
			ResultSet rsCalificacion = null;
			String calificacion = "";



			String qryCal = "SELECT ic_calificacion FROM COMREL_PYME_EPO WHERE ic_pyme = ? AND ic_epo = ? " ;
			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(qryCal);
			long numero1=Long.parseLong(iNoCliente);
			pstmt.setLong(1,numero1);
			long numero2=Long.parseLong(icEpo);
			pstmt.setLong(2,numero2);
			rsCalificacion = pstmt.executeQuery();
			pstmt.clearParameters();



			/*
			String qryCal = "SELECT ic_calificacion FROM COMREL_PYME_EPO WHERE ic_pyme = " + iNoCliente + " AND ic_epo = " + icEpo;
			//System.out.println(qryCal);
			rsCalificacion = con.queryDB(qryCal);
			*/

			if(rsCalificacion.next())
			{
			   	calificacion = rsCalificacion.getString(1);
				//System.out.println(calificacion);
			}
			rsCalificacion.close();
			con.cierraStatement();

			return calificacion;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerCalificacion

	private Vector obtenerValoresTasa(String icEpo, String icMoneda, String icIf, String calificacion, AccesoDB con)
		throws Exception
	{
	  	try {
			ResultSet rsTasa = null;
			String dcFechaTasa = "", cgrmt = "";
			String tasaAceptada="", cgRelMat = "", cgPuntos = "",  icTasa ="";
			int tasas=0;
			double valor = 0, puntos = 0, fnValor = 0;
			Vector vDatos = new Vector();

			String qryTasa = "SELECT T.cd_nombre, "+
			"   TO_CHAR(TA.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS'), "+
			"   MT.fn_valor, "+
			"   TB.cg_rel_mat, "+
			"   TB.fn_puntos, "+
			"   TI.cg_rel_mat, "+
			"   TI.cg_puntos, "+
			"   TA.ic_if, " +
			"   TB.ic_tasa " +
			" FROM COMCAT_MONEDA M, "+
			"   COMCAT_TASA T, "+
			"   COM_MANT_TASA MT, "+
			"   COMREL_TASA_BASE TB, "+
			"   COMREL_TIPIFICACION TI, " +
			"   COMREL_TASA_AUTORIZADA TA " +
			" WHERE M.ic_moneda = T.ic_moneda "+
			"   AND T.ic_tasa = MT.ic_tasa "+
			"   AND MT.dc_fecha = (select max(dc_fecha) FROM COM_MANT_TASA WHERE ic_tasa = T.ic_tasa) " +
			"   AND UPPER(T.cs_disponible) = 'S' " +
			"   AND T.ic_tasa = TB.ic_tasa " +
			"   AND TB.dc_fecha_tasa = (select max(dc_fecha_tasa) FROM COMREL_TASA_BASE WHERE ic_epo = "+ icEpo +" AND ic_tasa = T.ic_tasa AND UPPER(cs_vobo_nafin) = 'S') " +
			"   AND UPPER(TB.cs_vobo_nafin) = 'S' "+
			"   AND TB.dc_fecha_tasa = TI.dc_fecha_tasa "+
			"   AND TI.ic_calificacion = " + calificacion +
			"   AND UPPER(TI.cs_vobo_nafin) = 'S' " +
			"   AND TB.dc_fecha_tasa = TA.dc_fecha_tasa "+
			"   AND UPPER(TA.cs_vobo_epo) = 'S' "+
			"   AND UPPER(TA.cs_vobo_if) = 'S' "+
			"   AND UPPER(TA.cs_vobo_nafin) = 'S' "+
			"   AND TA.ic_epo = "+ icEpo  +
			"   AND TB.ic_epo = "+icEpo+
			"   AND M.ic_moneda = " + icMoneda+
			"   AND TA.ic_if = "+ icIf;
			//System.out.println(qryTasa);

			rsTasa = con.queryDB(qryTasa);
			if (rsTasa.next()) {
				dcFechaTasa = rsTasa.getString(2).trim();
				icTasa = rsTasa.getString("IC_TASA");
				fnValor = rsTasa.getDouble(3);
				String cgrmb = rsTasa.getString(4).trim();
				double fnpb = rsTasa.getDouble(5);
				cgrmt = rsTasa.getString(6).trim();
				double fnpt = rsTasa.getDouble(7);
				//Calculamos el importe de la tasa a aplicar
				switch (cgrmb.charAt(0)){
					case '+':
					valor = fnValor + fnpb;break;
					case '-':
					valor = fnValor - fnpb;break;
					case '/':
					valor = fnValor / fnpb;break;
					case '*':
					valor = fnValor * fnpb;break;
				  }					   //fin switch
				switch (cgrmt.charAt(0)) {
					case '+':
						valor = valor + fnpt;
						puntos = fnpb + fnpt;
						break;
					case '-':
						valor = valor - fnpt;
						puntos = fnpb - fnpt;
						break;
					case '/':
						valor = valor / fnpt;
						break;
					case '*':
						valor = valor * fnpt;
						break;
				  }   //fin switch
				//puntos = fnpb + fnpt;
				if ("1".equals(calificacion)) {
					cgrmt = cgrmb;
					puntos = fnpb;
		  		}   // fin if "1".equals
				//System.out.println(puntos);
				tasas ++;
				tasaAceptada = ""+valor;
				cgRelMat = cgrmt;
				cgPuntos = ""+puntos;
				vDatos.add(0, dcFechaTasa);
				vDatos.add(1, tasaAceptada);
				vDatos.add(2, cgRelMat);
				vDatos.add(3, cgPuntos);
				vDatos.add(4, icTasa);
				vDatos.add(5, String.valueOf(fnValor) );

			} // fin if rsTasa.next()
			rsTasa.close();
			con.cierraStatement();
			return vDatos;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerValoresTasa

	private float obtenerTipoCambio(String icMoneda, AccesoDB con)
		throws Exception
	{
	  	try {
	  		PreparedStatement ps = null;
			ResultSet rsTC = null;
			float valorTC = 1;
			if (!icMoneda.equals("1")) {
				String qryTC = 
					" SELECT ROUND (fn_valor_compra, 2), 'SeleccionDocumentoEJB::obtenerTipoCambio()'"   +
					"   FROM com_tipo_cambio"   +
					"  WHERE ic_moneda = ?"   +
					"    AND dc_fecha = (SELECT MAX (tc.dc_fecha)"   +
					"                      FROM com_tipo_cambio tc"   +
					"                     WHERE tc.ic_moneda = ?)"  ;
				ps = con.queryPrecompilado(qryTC);
				ps.setInt(1, Integer.parseInt(icMoneda));
				ps.setInt(2, Integer.parseInt(icMoneda));
				rsTC = ps.executeQuery();
				if (rsTC.next()) {
					valorTC = rsTC.getFloat(1);
				}
				rsTC.close();ps.close();
			}	 //fin if icMoneda.equals("1")
			return valorTC;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerTipoCambio


   	/*********************************************************************************************
	*		Vector obtenerDocumento()
	* Obtiene los documentos para el proceso de Descuento Automatico (FODA 69)
	* Monitor de rechazos (Foda 66)
	*********************************************************************************************/

	private Vector obtenerDocumento(String numProc, String diaDesc, String icMoneda, String aforo, Vector valorTasas,
				String iNoCliente, String icEpo, String tipoFactoraje, String tipoPiso,
				String icIF, AccesoDB con, String diasMinimos, String sDtosProcesados)
			throws Exception	{
				ArrayList alNotas = new ArrayList();
				return obtenerDocumento(numProc,diaDesc,icMoneda,aforo,valorTasas,iNoCliente,icEpo,tipoFactoraje,tipoPiso,icIF,con,diasMinimos,sDtosProcesados,alNotas);

	}

   	/*********************************************************************************************
	*		Vector obtenerDocumento()
	* Obtiene los documentos para el proceso de Descuento Automatico (FODA 99)
	* Notas de credito (Foda 99)
	*********************************************************************************************/

	private Vector obtenerDocumento(String numProc, String diaDesc, String icMoneda, String aforo, Vector valorTasas,
				String iNoCliente, String icEpo, String tipoFactoraje, String tipoPiso,
				String icIF, AccesoDB con, String diasMinimos, String sDtosProcesados,ArrayList alNotas)
		throws Exception
	{  
		
			System.out.println("Automatico:: obtenerDocumento :obtenerDocumento(E) ");
      Vector               vDatos   = new Vector();
      PreparedStatement 	ps       = null;
		ResultSet 				rs       = null;
      
	  	try {
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
			String sFechaVencPyme = selec.operaFechaVencPyme(icEpo);

			
			ResultSet rsDoctos = null;
			Vector vIntereses = new Vector();
			String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", nombreEpo="", dfDsctoAuto="";
			String totalMonto = "", totalDescuento = "", totalIntereses = "", totalRecibir = "";
			String fnMontoDscto="", inImporteInteres="", inImporteRecibir="", fnPorcAnticipo="";
			String icDocumento="", importeBenef = "";
			String lsdcFechaTasa = "";
			String lscgRelMat = "";
			String lsinTasaAceptada = "";
			String lscgPuntos		= "";
			String lscgTipoTasa		= "";
			String lsfnPuntosPref	= "";
      String lsNumeroDocto = "";
			int notasAplicadas		= 0;
			int i = 0;
			String icNotasAplicadas	= "";
			String montoAnt			= "";
			BigDecimal lbMonto		= new BigDecimal(0.0);
			BigDecimal lbMontoDscto	= new BigDecimal(0.0);
			BigDecimal lbRecibir = new BigDecimal(0.0);
			String lsNumContrato ="";//NE_2018_002
			String lsBeneficiario ="";//NE_2018_002

			
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
			boolean operaNotasDeCredito 						= selec.operaNotasDeCreditoParaDescuentoElectronico(icEpo);
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
			boolean aplicarNotasDeCreditoAVariosDoctos 	= selec.hasAplicarNotaDeCreditoAVariosDoctosEnabled(icEpo);
				
			// Fodea 002 - 2010. Informacion para Depuracion de Notas de Credito
			if(operaNotasDeCredito && !aplicarNotasDeCreditoAVariosDoctos){
				System.out.println("La EPO: " + icEpo + ", tiene habilitado Operar Notas de Credito.");
			}else if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){
				System.out.print("La EPO: " + icEpo + ", tiene habilitado Operar Notas de Credito, y ademas");
				System.out.println(" tiene habilitado Aplicar una Nota de Credito a varios documentos.");
			}
			
			double montoMinimo = selec.getMontoMinino(icEpo, con, icMoneda);

			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

			/*String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);*/
			
			// Para Factoraje Normal y Factoraje Vencido
			if( "N".equals(tipoFactoraje) || "V".equals(tipoFactoraje) || "I".equals(tipoFactoraje) || "M".equals(tipoFactoraje)  || "A".equals(tipoFactoraje)){ // "I" => Factoraje Vencimiento INFONAVIT
				
				// Fodea 038 - 2011 By JSHD
				// Si los documentos estan publicados en dolares, revisar si la relacion EPO - IF opera con Fondeo Propio
				boolean hayFondeoPropio = false;
				if( "54".equals(icMoneda) ){
					
					String qrySentencia =
						" SELECT                      "  +
						"    CS_TIPO_FONDEO           "  +
						" FROM                        "  +
						"    COMREL_IF_EPO_X_PRODUCTO "  +
						" WHERE                       "  +
						"     IC_IF             = ?   "  +
						" AND IC_EPO            = ?   "  +
						" AND IC_PRODUCTO_NAFIN = ?   ";
						
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1, 	Integer.parseInt(icIF));
					ps.setInt(2, 	Integer.parseInt(icEpo));
					ps.setInt(3, 	1); // 1: Descuento electronico
					rs = ps.executeQuery();
					
					if(rs.next()) {
						hayFondeoPropio = "P".equals(rs.getString("CS_TIPO_FONDEO"))?true:false;
					}
	
					rs.close();ps.close();
				}
				
				// Fodea 038 - 2011 By JSHD:
				//	Si los documentos a seleccionar est�n publicados con moneda Dolar Americano y la relacion EPO � IF seleccionada opera con 
				// FONDEO PROPIO, el calculo del plazo para el descuento debera ser similar que el de los documentos seleccionados con moneda 
				// nacional; de manera que los documentos deberan ser fondeados el mismo dia en que se esta realizado la solicitud de descuento 
				// (dia en que se estan seleccionando los documentos).
				if( hayFondeoPropio && "54".equals(icMoneda) ){
					
					String fechaHoy = "";
					String qrySentencia =
						" SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS FECHA_HOY FROM DUAL";
						
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					if(rs.next()) {
						fechaHoy = rs.getString("FECHA_HOY");
					}
					rs.close();ps.close();
					
					fechaSigDol = fechaHoy;
					
				// Si los documentos estan publicados en dolar  americano  pero la relacion EPO � IF con la que fueron seleccionados opera 
				// con FONDEO NAFIN, se conserva la regla actual y el abono de los recursos sera el siguiente dia en que fueron seleccionados.
				}else{
					
					String qrySentencia =
						" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), ?, '+'), 'dd/mm/yyyy') fecha, 'SeleccionDocumentoEJB::obtenerDocumento()'"   +
						"   FROM dual"  ;
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1, Integer.parseInt(icEpo));
					ps.setInt(2, 1);
					rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
					}
					rs.close();ps.close();
					
				}
			
			// Para todos los demas tipos de factoraje	
			}else{
				
				String qrySentencia =
					" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), ?, '+'), 'dd/mm/yyyy') fecha, 'SeleccionDocumentoEJB::obtenerDocumento()'"   +
					"   FROM dual"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(icEpo));
				ps.setInt(2, 1);
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
				}
				rs.close();ps.close();
				
			}
			

	String qryDoctos =
				" SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK)*/ "+
				"          d.ig_numero_docto, TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_docto,"   +
				"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') fecha_vencto,"   +
				"          (d.df_fecha_venc"+sFechaVencPyme+" - DECODE (d.ic_moneda, 1, TRUNC (SYSDATE), 54, TO_DATE (?, 'dd/mm/yyyy'))) plazo,"   +
				"          d.ic_moneda, d.fn_monto, (d.fn_monto * ?), d.ic_documento, (? * 100) AS porcentaje,"   +
				"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd') AS diavencimiento, NULL AS cg_razon_social,";
			if ("D".equals(tipoFactoraje) || "I".equals(tipoFactoraje)) {//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
				qryDoctos += 
					" ROUND( (D.fn_monto * D.fn_porc_beneficiario)/100, 2)  as monto_beneficiario, ";
			}
			
			//Fodea 062-2010
			if(icMoneda.equals("54")){
			
			qryDoctos +=
				"          TO_CHAR ( ( (  d.df_fecha_venc"+sFechaVencPyme+" - ? ) -1), 'DD/MM/YYYY') AS fechadsctoauto,";
         
			}else{
				qryDoctos += " TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+" - ?, 'DD/MM/YYYY') AS fechadsctoauto,";
			}
			qryDoctos +=	" d.IC_BENEFICIARIO as BENEFICIARIO , d.CG_NUM_CONTRATO as NUMCONTRATO,  ";//NE_2018_002				
			qryDoctos +=	"          'SeleccionDocumentoEJB  obtenerDocumento()'"   +								
				"     FROM com_documento d"   +
				"    WHERE ROWNUM = ?"   +
				"      AND d.ic_moneda = ?"   +
				"      AND d.df_fecha_venc"+sFechaVencPyme+" > TRUNC (SYSDATE)"   +
				"      AND d.cs_dscto_especial = ?";
			if (!numProc.equals("")&&numProc!=null) {
				qryDoctos +=
					"   AND NOT EXISTS (SELECT 1 FROM COM_DSCTOERROR DER "+
					"					WHERE DER.IC_DOCUMENTO = D.IC_DOCUMENTO "+
					"					AND   DER.IC_IF = D.IC_IF "+
					"					AND   DER.IC_DSCTOERROR = ?) ";
			}
			qryDoctos +=
				"      AND d.ic_estatus_docto = ?";
         if ("V".equals(tipoFactoraje) || "I".equals(tipoFactoraje) || "M".equals(tipoFactoraje) || "A".equals(tipoFactoraje)) {////MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT) EGB (FACTORAJE CON MANDATO)
				qryDoctos +=
					"   AND D.ic_if = ?"; //icIF
			}
			qryDoctos +=
				"      AND d.ic_pyme = ?"   +
				"      AND d.ic_epo = ?";
			
			
			

			//System.out.println("Doctos que ya fueron procesados: "+this.sDtosProcesados);
			VectorTokenizer vtkDoctosPorc =  new VectorTokenizer(sDtosProcesados,",");
			Vector vecDoctosProc = vtkDoctosPorc.getValuesVector();
			String cadenaVars = "";
			for(i=0;i<vecDoctosProc.size();i++) {
				if(!"".equals(cadenaVars))
					cadenaVars += ",";
				cadenaVars += "?";
			}
			if(!"".equals(cadenaVars)) {
				qryDoctos +=
					" AND D.ic_documento not in("+cadenaVars+")";

			}
			qryDoctos +=
				" ORDER BY d.df_fecha_venc"+sFechaVencPyme+", d.fn_monto ";
						
			LOG.debug("\n Query para obtencion del documento::::"+qryDoctos);
			LOG.debug("\n fechaSigDol   "+fechaSigDol +" \n aforo   "+aforo +"\n diasMinimos   "+diasMinimos);
			LOG.debug("\n numProc   "+numProc);
			
			
			//rsDoctos = con.queryDB(qryDoctos);
			ps = con.queryPrecompilado(qryDoctos);
/**/		int cont = 1;
			ps.setString(cont++,fechaSigDol);
			
			ps.setDouble(cont++,Double.parseDouble(aforo));
			ps.setDouble(cont++,Double.parseDouble(aforo));
			ps.setInt(cont++,Integer.parseInt(diasMinimos));
			ps.setInt(cont++,1);								//ROWNUM
			ps.setInt(cont++,Integer.parseInt(icMoneda));
			ps.setString(cont++, tipoFactoraje);			
			if (!numProc.equals("")&&numProc!=null) {
				ps.setInt(cont++,Integer.parseInt(numProc));
			}
			ps.setInt(cont++,2);								//ic_estatus_docto
			if ("V".equals(tipoFactoraje) || "I".equals(tipoFactoraje) || "M".equals(tipoFactoraje)  || "A".equals(tipoFactoraje)) {//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT) EGB (FACTORAJE CON MANDATO) DLHC (FACTORAJE AUTOMATICO)
				ps.setInt(cont++,Integer.parseInt(icIF));
			}
			ps.setInt(cont++,Integer.parseInt(iNoCliente));
			ps.setInt(cont++,Integer.parseInt(icEpo));
			for(i=0;i<vecDoctosProc.size();i++) {
				ps.setInt(cont++,Integer.parseInt(vecDoctosProc.get(i).toString()));
			}

/**/
			double   	granTotalMontoDisponible			= 0; // Solo ocupado en Fodea 002 - 2010. Sumatoria de Montos Disponibles (Monto del Documento - Monto Minimo)
			ArrayList 	lsNotasDeCreditoMultiplesDoctos = new ArrayList();

			rsDoctos = ps.executeQuery();
			if (rsDoctos.next()) {
				
				int 		tmpNotasAplicadas				= 0;
				double 	montoDisponible 				= 0; // Solo ocupado en Fodea 002 - 2010. Monto del documento - Monto Minimo	
							
				dfFechaDocto	= rsDoctos.getString(2);
				dfFechaVenc		= rsDoctos.getString(3);
				totalMonto		= rsDoctos.getString(6);
				totalDescuento	= rsDoctos.getString(7);
				fnMontoDscto	= rsDoctos.getString(7);
				icDocumento		= rsDoctos.getString(8);
				fnPorcAnticipo	= rsDoctos.getString(9);
				diaVencimiento	= rsDoctos.getString("DIAVENCIMIENTO");
				nombreEpo		= rsDoctos.getString("CG_RAZON_SOCIAL");
				dfDsctoAuto		= rsDoctos.getString("FECHADSCTOAUTO");
				lbMonto 		= new BigDecimal(totalMonto);
        lsNumeroDocto = rsDoctos.getString("ig_numero_docto");
		   lsBeneficiario =  (rsDoctos.getString("BENEFICIARIO")==null)?"":rsDoctos.getString("BENEFICIARIO");//NE_2018_002
		   lsNumContrato = (rsDoctos.getString("NUMCONTRATO")==null)?"":rsDoctos.getString("NUMCONTRATO");//NE_2018_002
		  
			  
				/*Aplicacion de Notas de Credito*/

				// Fodea 002 - 2010. Si la EPO solo tiene Configurado Operar Notas de Credito 
				//                   se aplicaran las Notas de Credito en la Forma Tradicional
				if(operaNotasDeCredito && !aplicarNotasDeCreditoAVariosDoctos){
					
					for(i=0;i<alNotas.size();i++){
						NotaCredito nc = (NotaCredito)alNotas.get(i);
						System.out.println("El monto de la nota = "+nc.getFnMonto());
						System.out.println("El monto docto = "+lbMonto.doubleValue());
						System.out.println("Notas aplicadas = "+notasAplicadas);
						if(nc.esDisponible()&&((lbMonto.doubleValue()-nc.getFnMonto())>=montoMinimo)){
							lbMonto = lbMonto.subtract(new BigDecimal(nc.getFnMonto()));
							icNotasAplicadas += ("".equals(icNotasAplicadas)?"":",")+nc.getIcDocumento();
							nc.setDisponible(false);
							notasAplicadas++;
						}
						System.out.println("-----------------------------------");
						System.out.println("Notas aplicadas = "+notasAplicadas);
					}
				// Fodea 002 - 2010. Tiene Configurado Operar Notas de Credito y tambien Aplicar una
				//                   Nota de Credito a Varios Documentos
				}else if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){
									lsNotasDeCreditoMultiplesDoctos = new ArrayList();
									montoDisponible = 0;
									// Fodea 002 - 2010. Se tomaran  los documentos cuyo monto sea mayor al Monto Minimo
									// parametrizado.
									if(lbMonto.doubleValue()>montoMinimo){
										// Fodea 002 - 2010. El Monto Total del Documento = Monto del Documento - Monto Minimo
										//                   Parametrizado.
										montoDisponible = lbMonto.doubleValue() - montoMinimo;
										// Aplicar las notas de Credito que correspondan
										for(i=0;i<alNotas.size();i++){
											
											NotaCredito nc = (NotaCredito)alNotas.get(i);
		
											System.out.println("El monto de la nota       = "+nc.getFnMontoDisponible().toString());
											System.out.println("Clave Docto(ic_documento) = "+lsNumeroDocto);
											System.out.println("El monto docto            = "+lbMontoDscto.doubleValue());
 
											// 1ER CASO: Monto de Nota de Credito > (Monto del Documento - Monto Minimo)
											if(nc.esDisponible() && ( nc.getFnMontoDisponible().doubleValue() > (lbMonto.doubleValue()-montoMinimo))){
												// Calcular el restante de la nota de credito que todavia puede ser aplicado
												BigDecimal montoAplicado 		= lbMonto.subtract(new BigDecimal(montoMinimo));
												BigDecimal fnMontoDisponible	= nc.getFnMontoDisponible().subtract(montoAplicado);
												// Actualizar monto disponible de la nota de Credito
												nc.setFnMontoDisponible(fnMontoDisponible);
												// Indicar clave de la Nota de Credito que se aplico
												icNotasAplicadas 			+= ("".equals(icNotasAplicadas)?"":",")+nc.getIcDocumento();
												// Actualar monto del documento al monto minimo que este debe tener
												lbMonto 						= new BigDecimal(montoMinimo);
												// Registrar Montos de Nota de Credito al Documento
												lsNotasDeCreditoMultiplesDoctos.add(
													new NotaCreditoMultiple(
															String.valueOf(nc.getIcDocumento()),
															icDocumento,
															montoAplicado.toPlainString(),
															lbMonto.toPlainString()
													)
												);
												// Mostrar nota aplicada
												System.out.println("Nota aplicada             = "+nc.getIcDocumento());
												System.out.println("-----------------------------------");
												// Continuar con el documento que sigue
												break;
											// 2DO CASO: Monto Nota de Credito <= (Monto del Documento - Monto Minimo)	
											}else if(nc.esDisponible() && ( nc.getFnMontoDisponible().doubleValue() <= (lbMonto.doubleValue()-montoMinimo))){
												// Actualizar monto del documento
												BigDecimal	montoAplicado 	= nc.getFnMontoDisponible();
												lbMonto 							= lbMonto.subtract(nc.getFnMontoDisponible());
												// Indicar clave de la Nota de Credito que se aplico
												icNotasAplicadas 			+= ("".equals(icNotasAplicadas)?"":",")+nc.getIcDocumento();
												// Indicar que la Nota de Credito que se aplico ya no esta disponible
												nc.setDisponible(false);
												// Actualizar Ceros el monto disponible de la Nota de Credito
												nc.setFnMontoDisponible(new BigDecimal("0.00"));
												// Registrar Montos de Nota de Credito al Documento
												lsNotasDeCreditoMultiplesDoctos.add(
													new NotaCreditoMultiple(
														String.valueOf(nc.getIcDocumento()),
														icDocumento,
														montoAplicado.toPlainString(),
														lbMonto.toPlainString()
													)
												);
												// Mostrar nota aplicada
												System.out.println("Nota aplicada             = "+nc.getIcDocumento());
												System.out.println("-----------------------------------");
												// Continuar con el documento que sigue
												if( lbMonto.compareTo(new BigDecimal(montoMinimo)) == 0 ){
													break;
												}
											// 3ER CASO: La nota de credito seleccionada no esta disponible(=>ya fue utilizada) 
											}else{
												// Mostrar nota aplicada
												System.out.println("Nota aplicada             = NINGUNA ");
												System.out.println("-----------------------------------");
											}	

										}
 
									}
									// Agregar al Monto Total de los Documentos
									granTotalMontoDisponible += montoDisponible;
				}	
					
				montoAnt = totalMonto;
				totalMonto = lbMonto.toPlainString();
				lbMontoDscto = lbMonto.multiply(new BigDecimal(aforo));
				totalDescuento = lbMontoDscto.toPlainString();
				fnMontoDscto   = lbMontoDscto.toPlainString();

				LOG.debug("montoAnt  "+montoAnt +"\n totalMonto  "+totalMonto+" \n lbMontoDscto  "+lbMontoDscto);
				LOG.debug("totalDescuento  "+totalDescuento +"\n fnMontoDscto  "+fnMontoDscto);  
				
				/* Calculo de los intereses */
				try {
					vIntereses = selec.ovcalculaInteres(lbMontoDscto, rsDoctos.getInt("PLAZO"),valorTasas, tipoPiso );
					for(i=0;i<alNotas.size();i++){
						NotaCredito nc = (NotaCredito)alNotas.get(i);
						nc.confirmaDisp();
					}
				} catch (Exception e) {
					System.out.println("No se encontro tasa para el documento selecciondado");
					CSeleccionDocumentoBean.ovDocumentoRechazado(numProc,icDocumento,icIF,"86",fnPorcAnticipo,diasMinimos,diaDesc,icEpo,con);
					this.sDtosProc = icDocumento;
					icNotasAplicadas = "";
					for(i=0;i<alNotas.size();i++){
						NotaCredito nc = (NotaCredito)alNotas.get(i);
						nc.retornaDisp();
					}
				}
				if (vIntereses.size()>=10) {
					totalIntereses  = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"0":vIntereses.get(11).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					lbRecibir=lbMontoDscto.subtract(new BigDecimal(totalIntereses));
					totalRecibir	= lbRecibir.toPlainString();
					inImporteInteres= totalIntereses;
					inImporteRecibir= totalRecibir;
					montoInteresAcuse = Double.parseDouble(totalIntereses);
					montoRecibirAcuse = Double.parseDouble(totalRecibir);
					lsdcFechaTasa = vIntereses.get(0).toString();
					lscgRelMat = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"+":vIntereses.get(1).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					lsinTasaAceptada = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"0":vIntereses.get(3).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					lscgPuntos = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"0":vIntereses.get(4).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					lscgTipoTasa = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"B":vIntereses.get(7).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					lsfnPuntosPref = (tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))?"0":vIntereses.get(8).toString();//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
					/* Formacion del vector resultante */
					vDatos.add(0, dfFechaDocto);
					vDatos.add(1, dfFechaVenc);
					vDatos.add(2, totalMonto);
					vDatos.add(3, totalDescuento);
					vDatos.add(4, totalIntereses);
					vDatos.add(5, totalRecibir);
					vDatos.add(6, fnMontoDscto);
					vDatos.add(7, inImporteInteres);
					vDatos.add(8, inImporteRecibir);
					vDatos.add(9, icDocumento);
					vDatos.add(10, fnPorcAnticipo);
					vDatos.add(11, diaVencimiento);
					vDatos.add(12, nombreEpo);
					if ("D".equals(tipoFactoraje) || "I".equals(tipoFactoraje)) {//MODIFICACION FODEA 042-2009 FVR (SE AGREGA FACTORAJE VENC INFONAVIT)
						importeBenef = (lbRecibir.doubleValue() <= rsDoctos.getDouble("MONTO_BENEFICIARIO"))?totalRecibir:rsDoctos.getString("MONTO_BENEFICIARIO");
						vDatos.add(13, importeBenef);
					} else {
						vDatos.add(13, "");
					}
					vDatos.add(14, dfDsctoAuto);
					vDatos.add(15, lsdcFechaTasa);
					vDatos.add(16, lscgRelMat);
					vDatos.add(17, lsinTasaAceptada);
					vDatos.add(18, lscgPuntos);
					vDatos.add(19, lscgTipoTasa);
					vDatos.add(20, lsfnPuntosPref);
					vDatos.add(21, montoAnt);
					vDatos.add(22, icNotasAplicadas);
					vDatos.add(23, lsNotasDeCreditoMultiplesDoctos);
					
					vDatos.add(24, lsBeneficiario);//NE_2018_002					
					vDatos.add(25, lsNumContrato);//NE_2018_002

					montoDescuentoAcuse = rsDoctos.getDouble(7);
				} else
				{	System.out.println("Aqui hay un error: No se encontro tasa para el plazo del documento: "+icDocumento);
					/*****************************************************
						AQUI SE DEBE INSERTAR UN PROCEDIMIENTO QUE REGISTRE EN LA BITACORA DE ERRRORES EL ICDOCUMENTO
					*******************************************************/
					CSeleccionDocumentoBean.ovDocumentoRechazado(numProc,icDocumento,icIF,"86",fnPorcAnticipo,diasMinimos,diaDesc,icEpo,con);
					this.sDtosProc = icDocumento;
				}
			}
			if(rsDoctos!=null) {
				rsDoctos.close();
				rsDoctos=null;
			}
			
		} catch(Exception e) {  // fin try
			e.printStackTrace();
			//throw e;
			System.out.println("Error en el calculo de los intereses");
		}finally {
			//con.cierraStatement();
			if(rs!=null) try { rs.close(); }catch(Exception e){}
			if(ps!=null) try { ps.close(); }catch(Exception e){}
		}
		return vDatos;
		
	}//Fin obtenerDocumento

	private Vector obtenerDocumentoProgram(String numProc, String diaDesc, String icMoneda, String aforo, Vector valorTasas,
				String iNoCliente, String icEpo, String tipoFactoraje, String tipoPiso,
				String icIF, AccesoDB con, String diasMinimos, String sDtosProcesados)
		throws Exception
	{   Vector vDatos = new Vector();
	  	try {
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
	  		String sFechaVencPyme = selec.operaFechaVencPyme(icEpo);

			PreparedStatement ps = null;
			ResultSet rsDoctos = null;
			Vector vIntereses = new Vector();
			String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", nombreEpo="", dfDsctoAuto="";
			String totalMonto = "", totalDescuento = "", totalIntereses = "", totalRecibir = "";
			String fnMontoDscto="", inImporteInteres="", inImporteRecibir="", fnPorcAnticipo="";
			String icDocumento="", importeBenef = "";
			String lsdcFechaTasa = "";
			String lscgRelMat = "";
			String lsinTasaAceptada = "";
			String lscgPuntos		= "";
			String lscgTipoTasa		= "";
			String lsfnPuntosPref	= "";
			int notasAplicadas		= 0;
			int i = 0;
			String icNotasAplicadas	= "";
			String montoAnt			= "";
			BigDecimal lbMonto		= new BigDecimal(0.0);
			BigDecimal lbMontoDscto	= new BigDecimal(0.0);
			BigDecimal lbRecibir = new BigDecimal(0.0);
			String qryDoctos =
				"SELECT D.ig_numero_docto, "+ 															//1
				"   TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') FECHA_DOCTO, " + 							//2
				"   TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'DD/MM/YYYY') FECHA_VENCTO, " + 							//3
				"   (d.df_fecha_venc"+sFechaVencPyme+" - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')) PLAZO, " + //4
				"   D.ic_moneda, "+											//5
				"   D.fn_monto, "+											//6
				"   (D.fn_monto * ?), "+									//7 aforo
				"   D.ic_documento " +										//8 (10)
				"   , (? * 100) as PORCENTAJE "+							//9 (11) aforo
				"   , TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd') as  diaVencimiento "+	//10(12)
				"   , E.cg_razon_social ";									//11(13)
			qryDoctos +=
				"	, TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+" - ?,'DD/MM/YYYY') as fechaDsctoAuto"+ // diasMinimos
				"	FROM COM_DOCUMENTO D, COM_DOCTO_SELECCIONADO SEL, COMREL_PYME_EPO PE, COMCAT_EPO E " +
				"	WHERE  D.ic_documento = SEL.ic_documento "+
				"	AND D.ic_epo = PE.ic_epo " +
				"   AND D.ic_pyme = PE.ic_pyme " +
				"   AND PE.ic_epo = E.ic_epo " ;
			if (!numProc.equals("")&&numProc!=null)	// Asegura que no pueda tomar un documento ya rechazado
			{
				qryDoctos +=
					"   AND NOT EXISTS (SELECT 1 FROM COM_DSCTOERROR DER "+
					"					WHERE DER.IC_DOCUMENTO = D.IC_DOCUMENTO "+
					"					AND   DER.IC_DSCTOERROR = ?) "; // numProc
			}

			qryDoctos +=
				"   AND D.ic_pyme = ?" + //iNoCliente
				"   AND D.ic_epo = ?" + //icEpo
				"   AND D.ic_moneda = ?" + //icMoneda
				"   AND d.df_fecha_venc"+sFechaVencPyme+" > TRUNC(SYSDATE) " +
				"   AND SEL.df_programacion >= trunc(SYSDATE) "+
				"   AND SEL.df_programacion < trunc(SYSDATE+1) "+
				"   AND D.ic_estatus_docto = 26 "+
				"   AND D.cs_dscto_especial = ?"; //tipoFactoraje
			//System.out.println("Doctos que ya fueron procesados: "+this.sDtosProcesados);
			VectorTokenizer vtkDoctosPorc =  new VectorTokenizer(sDtosProcesados,",");
			Vector vecDoctosProc = vtkDoctosPorc.getValuesVector();
			String cadenaVars = "";
			for(i=0;i<vecDoctosProc.size();i++) {
				if(!"".equals(cadenaVars))
					cadenaVars += ",";
				cadenaVars += "?";
			}
			if(!"".equals(cadenaVars)) {
				qryDoctos +=
					" AND D.ic_documento not in("+cadenaVars+")";

			}
			qryDoctos +=
				"   AND rownum = 1 "+
				" ORDER BY d.df_fecha_venc"+sFechaVencPyme+" ";
			System.out.println("\n &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
			System.out.println("\n Query para obtencion del documento:"+qryDoctos);
/*			System.out.println("\n aforo: "+aforo);
			System.out.println("\n diasMinimos: "+diasMinimos);
			System.out.println("\n numProc: "+numProc);
			System.out.println("\n iNoCliente: "+iNoCliente);
			System.out.println("\n icEpo: "+icEpo);
			System.out.println("\n icMoneda: "+icMoneda);
			System.out.println("\n tipoFactoraje: "+tipoFactoraje);
			System.out.println("\n vecDoctosProc: "+vecDoctosProc);*/

			//rsDoctos = con.queryDB(qryDoctos);
			ps = con.queryPrecompilado(qryDoctos);
/**/		int cont = 3;
			ps.setDouble(1,Double.parseDouble(aforo));
			ps.setDouble(2,Double.parseDouble(aforo));
			ps.setInt(3,Integer.parseInt(diasMinimos));
			if (!numProc.equals("")&&numProc!=null) {
				cont++; ps.setInt(cont,Integer.parseInt(numProc));
			}
			cont++;ps.setInt(cont,Integer.parseInt(iNoCliente));
			cont++;ps.setInt(cont,Integer.parseInt(icEpo));
			cont++;ps.setInt(cont,Integer.parseInt(icMoneda));
			cont++;ps.setString(cont, tipoFactoraje);
			for(i=0;i<vecDoctosProc.size();i++) {
				cont++;ps.setInt(cont,Integer.parseInt(vecDoctosProc.get(i).toString()));
			}

/**/
			rsDoctos = ps.executeQuery();
			if (rsDoctos.next()) {
				dfFechaDocto	= rsDoctos.getString(2);
				dfFechaVenc		= rsDoctos.getString(3);
				totalMonto		= rsDoctos.getString(6);
				totalDescuento	= rsDoctos.getString(7);
				fnMontoDscto	= rsDoctos.getString(7);
				icDocumento		= rsDoctos.getString(8);
				fnPorcAnticipo	= rsDoctos.getString(9);
				diaVencimiento	= rsDoctos.getString("DIAVENCIMIENTO");
				nombreEpo		= rsDoctos.getString("CG_RAZON_SOCIAL");
				dfDsctoAuto		= rsDoctos.getString("FECHADSCTOAUTO");
				lbMonto 		= new BigDecimal(totalMonto);
				montoAnt = totalMonto;
				totalMonto = lbMonto.toPlainString();
				lbMontoDscto = lbMonto.multiply(new BigDecimal(aforo));
				totalDescuento = lbMontoDscto.toPlainString();
				fnMontoDscto   = lbMontoDscto.toPlainString();

				/* Calculo de los intereses */
				try {
					vIntereses = selec.ovcalculaInteres(lbMontoDscto, rsDoctos.getInt("PLAZO"),valorTasas, tipoPiso );
				} catch (Exception e) {
					System.out.println("No se encontro tasa para el documento selecciondado");
					CSeleccionDocumentoBean.ovDocumentoRechazado(numProc,icDocumento,icIF,"86",fnPorcAnticipo,diasMinimos,diaDesc,icEpo,con);
					this.sDtosProc = icDocumento;
					icNotasAplicadas = "";
				}
				if (vIntereses.size()>=10) {
					totalIntereses  = vIntereses.get(11).toString();
					lbRecibir=lbMontoDscto.subtract(new BigDecimal(totalIntereses));
					totalRecibir	= lbRecibir.toPlainString();
					inImporteInteres= totalIntereses;
					inImporteRecibir= totalRecibir;
					montoInteresAcuse = Double.parseDouble(totalIntereses);
					montoRecibirAcuse = Double.parseDouble(totalRecibir);
					lsdcFechaTasa = vIntereses.get(0).toString();
					lscgRelMat = vIntereses.get(1).toString();
					lsinTasaAceptada = vIntereses.get(3).toString();
					lscgPuntos = vIntereses.get(4).toString();
					lscgTipoTasa = vIntereses.get(7).toString();
					lsfnPuntosPref = vIntereses.get(8).toString();
					/* Formacion del vector resultante */
					vDatos.add(0, dfFechaDocto);
					vDatos.add(1, dfFechaVenc);
					vDatos.add(2, totalMonto);
					vDatos.add(3, totalDescuento);
					vDatos.add(4, totalIntereses);
					vDatos.add(5, totalRecibir);
					vDatos.add(6, fnMontoDscto);
					vDatos.add(7, inImporteInteres);
					vDatos.add(8, inImporteRecibir);
					vDatos.add(9, icDocumento);
					vDatos.add(10, fnPorcAnticipo);
					vDatos.add(11, diaVencimiento);
					vDatos.add(12, nombreEpo);
					if ("D".equals(tipoFactoraje)) {
						importeBenef = (lbRecibir.doubleValue() <= rsDoctos.getDouble("MONTO_BENEFICIARIO"))?totalRecibir:rsDoctos.getString("MONTO_BENEFICIARIO");
						vDatos.add(13, importeBenef);
					} else {
						vDatos.add(13, "");
					}
					vDatos.add(14, dfDsctoAuto);
					vDatos.add(15, lsdcFechaTasa);
					vDatos.add(16, lscgRelMat);
					vDatos.add(17, lsinTasaAceptada);
					vDatos.add(18, lscgPuntos);
					vDatos.add(19, lscgTipoTasa);
					vDatos.add(20, lsfnPuntosPref);
					vDatos.add(21, montoAnt);
					vDatos.add(22, icNotasAplicadas);

					montoDescuentoAcuse = rsDoctos.getDouble(7);
				} else
				{	System.out.println("Aqui hay un error: No se encontro tasa para el plazo del documento: "+icDocumento);
					/*****************************************************
						AQUI SE DEBE INSERTAR UN PROCEDIMIENTO QUE REGISTRE EN LA BITACORA DE ERRRORES EL ICDOCUMENTO
					*******************************************************/
					CSeleccionDocumentoBean.ovDocumentoRechazado(numProc,icDocumento,icIF,"86",fnPorcAnticipo,diasMinimos,diaDesc,icEpo,con);
					this.sDtosProc = icDocumento;
				}
			}
			if(rsDoctos!=null) {
				rsDoctos.close();
				rsDoctos=null;
			}
			//con.cierraStatement();
			if(ps!=null) ps.close();
		} catch(Exception e) {  // fin try
			e.printStackTrace();
			//throw e;
			System.out.println("Error en el calculo de los intereses");
		}
		return vDatos;
		
	}//Fin obtenerDocumento

   	/*********************************************************************************************
	*		Vector obtenerDocumento()
	* Version antigua que no considera las tasas por plazo
	*********************************************************************************************/
	private Vector obtenerDocumento(String icMoneda, String aforo, String valor,
				String iNoCliente, String icEpo, String tipoFactoraje, String tipoPiso,
				String icIF, AccesoDB con, String diasMinimos, String sDtosProcesados)
		throws Exception
	{
	  	try {
			ResultSet rsDoctos = null;
			Vector vDatos = new Vector();
			String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", nombreEpo="", dfDsctoAuto="";
			String totalMonto = "", totalDescuento = "", totalIntereses = "", totalRecibir = "";
			String fnMontoDscto="", inImporteInteres="", inImporteRecibir="", fnPorcAnticipo="";
			String icDocumento="", importeBenef = "";


			String qryDoctos = "SELECT D.ig_numero_docto, "+
							"   TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') FECHA_DOCTO, " +
							"   TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') FECHA_VENCTO, " +
							"   (D.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')) PLAZO, " +
							"   D.ic_moneda, "+
							"   D.fn_monto, "+
							"   (D.fn_monto * " + aforo + "), ";
			if (tipoPiso.equals("1")) {
				qryDoctos +="   ROUND( (((d.fn_monto * " + aforo + ") * ("+valor+" / 100)) / 360 ) ,2) * (d.df_fecha_venc - TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')) INTERESES, " +
							"   ((D.fn_monto * " + aforo + ") - ROUND( (((d.fn_monto * " + aforo + ") * ("+valor+"/ 100)) / 360 ) ,2) * (d.df_fecha_venc - TO_DATE (TO_CHAR (SYSDATE, 'DD/MM/YYYY'), 'DD/MM/YYYY')) ) RECIBIR, ";
			} else {
				qryDoctos +="	((((D.fn_monto * " + aforo + ") * ("+valor+"/100)) * (D.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')))/360) INTERESES, " +
							"	((D.fn_monto * " + aforo + ") - ((((D.fn_monto * " + aforo + ") * ("+valor+"/100)) * (D.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')))/360)) RECIBIR, ";
			}
			qryDoctos += "   D.ic_documento " +
						"   , (" + aforo + " * 100) as PORCENTAJE "+
						"   , TO_CHAR(D.df_fecha_venc,'dd') as  diaVencimiento "+
						"   , E.cg_razon_social ";
			if ("D".equals(tipoFactoraje)) {
				qryDoctos += "	, ROUND( (D.fn_monto * D.fn_porc_beneficiario)/100, 2)  as monto_beneficiario ";
			}
			qryDoctos +="	, TO_CHAR(D.df_fecha_venc - "+diasMinimos+",'DD/MM/YYYY') as fechaDsctoAuto"+
						" FROM COM_DOCUMENTO D, COMREL_PYME_EPO PE, COMCAT_EPO E " +
						" WHERE D.ic_estatus_docto = 2 "+
						"   AND D.ic_pyme = " + iNoCliente +
						"   AND D.ic_epo = " + icEpo +
						"   AND D.ic_epo = PE.ic_epo " +
						"   AND D.ic_pyme = PE.ic_pyme " +
						"   AND PE.ic_epo = E.ic_epo " +
						"   AND D.ic_moneda = " + icMoneda +
						"   AND D.df_fecha_venc > TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') " +
						"   AND D.cs_dscto_especial = '"+tipoFactoraje+"' ";
			if ("V".equals(tipoFactoraje)) {
				qryDoctos += "   AND D.ic_if = " + icIF;
			}
			//System.out.println("Doctos Procesados: "+this.sDtosProcesados);
			qryDoctos +=(sDtosProcesados.equals(""))?"":"   AND D.ic_documento not in ("+sDtosProcesados+")";
			qryDoctos +="   AND rownum = 1 "+
						" ORDER BY D.df_fecha_venc ";
			//System.out.println("\n"+qryDoctos);
			rsDoctos = con.queryDB(qryDoctos);
			if (rsDoctos.next()) {
				dfFechaDocto	= rsDoctos.getString(2);
				dfFechaVenc	 = rsDoctos.getString(3);
				totalMonto	  = rsDoctos.getString(6);
				totalDescuento  = rsDoctos.getString(7);
				totalIntereses  = rsDoctos.getString(8);
				totalRecibir	= rsDoctos.getString(9);
				fnMontoDscto	= rsDoctos.getString(7);
				inImporteInteres= rsDoctos.getString(8);
				inImporteRecibir= rsDoctos.getString(9);
				montoDescuentoAcuse = rsDoctos.getDouble(7);
				montoInteresAcuse = rsDoctos.getDouble(8);
				montoRecibirAcuse = rsDoctos.getDouble(9);
				icDocumento	 = rsDoctos.getString(10);
				fnPorcAnticipo  = rsDoctos.getString(11);
				diaVencimiento  = rsDoctos.getString("DIAVENCIMIENTO");
				nombreEpo	   = rsDoctos.getString("CG_RAZON_SOCIAL");
				dfDsctoAuto		= rsDoctos.getString("FECHADSCTOAUTO");

				vDatos.add(0, dfFechaDocto);
				vDatos.add(1, dfFechaVenc);
				vDatos.add(2, totalMonto);
				vDatos.add(3, totalDescuento);
				vDatos.add(4, totalIntereses);
				vDatos.add(5, totalRecibir);
				vDatos.add(6, fnMontoDscto);
				vDatos.add(7, inImporteInteres);
				vDatos.add(8, inImporteRecibir);
				vDatos.add(9, icDocumento);
				vDatos.add(10, fnPorcAnticipo);
				vDatos.add(11, diaVencimiento);
				vDatos.add(12, nombreEpo);
				if ("D".equals(tipoFactoraje)) {
					importeBenef = (rsDoctos.getDouble(9) <= rsDoctos.getDouble("MONTO_BENEFICIARIO"))?rsDoctos.getString(9):rsDoctos.getString("MONTO_BENEFICIARIO");
					vDatos.add(13, importeBenef);
				} else {
					vDatos.add(13, "");
				}
				vDatos.add(14, dfDsctoAuto);
			}
			if(rsDoctos!=null) {
				rsDoctos.close();
				rsDoctos=null;
			}
			con.cierraStatement();
			return vDatos;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerDocumento

   	/*********************************************************************************************
	*		Vector obtenerDocumento()
	* Obtiene los documentos para el proceso de Pignoracion
	*   considera las tasas por plazo
	*********************************************************************************************/
	// Modificado 13/12/2002	--CARP
	private Vector obtenerDocumento(String icMoneda, String aforo, Vector valor,
				String iNoCliente, String icEpo, String esDiasMinimos, String tipoPiso,
				String icIF, String esFechaPago, String esPlazoMaximo, AccesoDB con)
		throws Exception
	{
		System.out.println("\tCobroDocumentosBean::AutomaticoNuevo::obtenerDocumento()");
	  	try {
			ResultSet rsDoctos = null;
			Vector vDatos = new Vector();
			Vector vIntereses = new Vector();
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();;
			String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", nombreEpo="";
			String totalMonto = "", totalDescuento = "", totalIntereses = "", totalRecibir = "";
			String fnMontoDscto="", inImporteInteres="", inImporteRecibir="", fnPorcAnticipo="";
			String icDocumento="", importeBenef = "";
			// Agregado 261202	--CARP
			int liPlazo = 0;
			BigDecimal lbMontoDscto = new BigDecimal(0.0);
			BigDecimal lbRecibir = new BigDecimal(0.0);
			StringBuffer lsQueryDoctos = new StringBuffer();
			lsQueryDoctos.append(" SELECT (D.fn_monto * " + aforo + ") as Monto, "); //1
			/******* Sustituido por el metodo ovcalculaInteres del EJB de Seleccion de Documentos *********
			if (tipoPiso.equals("1")) {
				lsQueryDoctos.append(" ROUND( (((d.fn_monto * " + aforo + ") * ("+valor+" / 100)) / 360 ) ,2) * (d.df_fecha_venc - TO_DATE ('"+esFechaPago+"', 'DD/MM/YYYY')) INTERESES, " );
				lsQueryDoctos.append(" ((D.fn_monto * " + aforo + ") - ROUND( (((d.fn_monto * " + aforo + ") * ("+valor+"/ 100)) / 360 ) ,2) * (d.df_fecha_venc - TO_DATE ('"+esFechaPago+"','DD/MM/YYYY')) ) RECIBIR, ");
			} else {
				lsQueryDoctos.append(" ((((D.fn_monto * " + aforo + ") * ("+valor+"/100)) * (D.df_fecha_venc - TO_DATE('"+esFechaPago+"','DD/MM/YYYY')))/360) INTERESES, " );
				lsQueryDoctos.append(" 	((D.fn_monto * " + aforo + ") - ((((D.fn_monto * " + aforo + ") * ("+valor+"/100)) * (D.df_fecha_venc - TO_DATE('"+esFechaPago+"','DD/MM/YYYY')))/360)) RECIBIR, ");
			}
			*********************/
			lsQueryDoctos.append(" D.ic_documento " ); //2 (antes 4)
			lsQueryDoctos.append(" , (" + aforo + " * 100) as PORCENTAJE ");
			lsQueryDoctos.append(" ,  (D.df_fecha_venc - TO_DATE('"+esFechaPago+"', 'DD/MM/YYYY')) PLAZO " );
			lsQueryDoctos.append(" FROM COM_DOCUMENTO D, COMREL_PYME_EPO PE, COMCAT_EPO E " );
			lsQueryDoctos.append(" WHERE D.ic_estatus_docto = 2 ");
			lsQueryDoctos.append(" AND D.ic_pyme = " + iNoCliente );
			lsQueryDoctos.append(" AND D.ic_epo = " + icEpo );
			lsQueryDoctos.append(" AND D.ic_epo = PE.ic_epo " );
			lsQueryDoctos.append(" AND D.ic_pyme = PE.ic_pyme " );
			lsQueryDoctos.append(" AND PE.ic_epo = E.ic_epo " );
			lsQueryDoctos.append(" AND D.ic_moneda = " + icMoneda );
			lsQueryDoctos.append(" AND D.df_fecha_venc > TO_DATE('"+esFechaPago+"' ,'DD/MM/YYYY')+"+ esDiasMinimos );
			lsQueryDoctos.append(" AND D.cs_dscto_especial = 'N' ");
			lsQueryDoctos.append(" AND (D.df_fecha_venc - TO_DATE('"+esFechaPago+"', 'DD/MM/YYYY'))<= " + esPlazoMaximo);
			lsQueryDoctos.append("  AND rownum = 1 " );
			lsQueryDoctos.append(" ORDER BY D.df_fecha_venc asc");
			System.out.println(lsQueryDoctos.toString());
			rsDoctos = con.queryDB(lsQueryDoctos.toString());
			if (rsDoctos.next()) {
				fnMontoDscto	= rsDoctos.getString(1);
				icDocumento	 = rsDoctos.getString(2);
				fnPorcAnticipo  = rsDoctos.getString(3);
				liPlazo 		= rsDoctos.getInt(4);
				/* Calculo de los intereses */
				lbMontoDscto = new BigDecimal(fnMontoDscto);
				vIntereses = selec.ovcalculaInteres(lbMontoDscto, liPlazo ,valor, tipoPiso );
				if (vIntereses.size()>=10)
				{
					inImporteInteres= vIntereses.get(11).toString();
					lbRecibir=lbMontoDscto.subtract(new BigDecimal(inImporteInteres));
					inImporteRecibir= lbRecibir.toPlainString();
					String lsdcFechaTasa = vIntereses.get(0).toString();
					String lscgRelMat = vIntereses.get(1).toString();
					String lsinTasaAceptada = vIntereses.get(3).toString();
					String lscgPuntos = vIntereses.get(4).toString();
					String lscgTipoTasa = vIntereses.get(7).toString();
					String lsfnPuntosPref = vIntereses.get(8).toString();
					String lsinValorBase = vIntereses.get(5).toString();
					/* Formacion del vector resultante */
					vDatos.add(0, fnMontoDscto);
					vDatos.add(1, inImporteInteres);
					vDatos.add(2, inImporteRecibir);
					vDatos.add(3, icDocumento);
					vDatos.add(4, fnPorcAnticipo);
					vDatos.add(5, new Integer(liPlazo));
					vDatos.add(6, lsdcFechaTasa);
					vDatos.add(7, lscgRelMat);
					vDatos.add(8, lsinTasaAceptada);
					vDatos.add(9, lscgPuntos);
					vDatos.add(10,lscgTipoTasa);
					vDatos.add(11,lsfnPuntosPref);
					vDatos.add(12,lsinValorBase);
				} else
				{	System.out.println("No se encontraron tasas para el documento:"+icDocumento);

				} // Fin del if-else para calculo de intereses
			} // Fin del if de la busqueda de documentos
			if(rsDoctos!=null) {
				rsDoctos.close();
				rsDoctos=null;
			}
			con.cierraStatement();
			return vDatos;

		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerDocumento

	private String obtenerAcuse(String totalDescuento, String totalIntereses, String totalRecibir,
				String icMoneda, String iNoUsuario, AccesoDB con)
		throws Exception
	{
	  	try {
			//________________________GENERACION DEL NUMERO DE ACUSE PYME:___________________
			Acuse acuse = new Acuse(Acuse.ACUSE_PYME_AUT, PRODUCTO_DSCTO, con);
			String _acuse = "NULL";
			String qryAcuse="insert into com_acuse2 (cc_acuse, fn_total_monto_mn, fn_total_int_mn, fn_total_imp_mn, "+
			"fn_total_monto_dl, fn_total_int_dl, fn_total_imp_dl,"+
			"df_fecha_hora, ic_usuario, cg_recibo_electronico) values ('"+acuse+"', ";
			if (icMoneda.equals("1")) {
				qryAcuse += totalDescuento+", "+totalIntereses+", "+totalRecibir+", NULL, NULL, NULL ";
			} else {
				qryAcuse +=" NULL, NULL, NULL, "+totalDescuento+", "+totalIntereses+", "+totalRecibir;
			}
			qryAcuse +=", sysdate, '"+iNoUsuario+"','"+_acuse+"')";
			System.out.println("************\nInsercion Acuse Automatico PYME: " + qryAcuse + "\n******************\n");
			//System.out.print(qryAcuse);
			con.ejecutaSQL(qryAcuse);
			return acuse.toString();
		} catch(Exception e) {  // fin try
			throw e;
		}
	}//Fin obtenerAcuse

	private void seleccionarDocumento(String CveProc, String diasMinimos, String diaDscto,String icEpo, String icIf, String acuse, String dcFechaTasa,
				String tasaAceptada, String inImporteInteres, String inImporteRecibir,
				String cgRelMat, String cgPuntos, float valorTC, String icDocumento,
				String fnMontoDscto, String fnPorcAnticipo, String cgTipoTasa,
				String fnPuntosPref, String importeRecBenef,AccesoDB con)
		throws Exception{
			seleccionarDocumento(CveProc, diasMinimos, diaDscto,icEpo, icIf, acuse, dcFechaTasa,
				tasaAceptada, inImporteInteres, inImporteRecibir,
				cgRelMat, cgPuntos, valorTC, icDocumento,
				fnMontoDscto, fnPorcAnticipo, cgTipoTasa,
				fnPuntosPref, importeRecBenef,"","","","","",con);

	}

	private void seleccionarDocumento(
				String CveProc, 		String diasMinimos, 		String diaDscto,
				String icEpo, 			String icIf, 				String acuse, String dcFechaTasa,
				String tasaAceptada, 	String inImporteInteres, 	String inImporteRecibir,
				String cgRelMat, 		String cgPuntos, 			float valorTC,
				String icDocumento,		String fnMontoDscto, 		String fnPorcAnticipo,
				String cgTipoTasa,		String fnPuntosPref, 		String importeRecBenef,
				String montoAnt,		String montoNvo,			String icNotasCredito,
				String fechaVenc,		String esNoUsuario,			AccesoDB con) throws Exception{
	
		seleccionarDocumento(
				CveProc, diasMinimos, diaDscto, icEpo, icIf, acuse, dcFechaTasa,	tasaAceptada, 
				inImporteInteres, inImporteRecibir,	cgRelMat, cgPuntos, valorTC, icDocumento, 
				fnMontoDscto, fnPorcAnticipo,cgTipoTasa, fnPuntosPref, importeRecBenef, montoAnt, 
				montoNvo, icNotasCredito,fechaVenc,esNoUsuario,con, null);
	
	}
   /*********************************************************************************************
	*		Vector seleccionarDocumento()
	*    Selecciona documentos en el proceso de descuento autom�tico
	*   Sobrecargado para Notas de Credito (Foda 99)-- JRFH
	*********************************************************************************************/

	private void seleccionarDocumento(
				String CveProc, 		String diasMinimos, 			String diaDscto,
				String icEpo, 			String icIf, 					String acuse, String dcFechaTasa,
				String tasaAceptada, String inImporteInteres, 	String inImporteRecibir,
				String cgRelMat, 		String cgPuntos, 				float valorTC,
				String icDocumento,	String fnMontoDscto, 		String fnPorcAnticipo,
				String cgTipoTasa,	String fnPuntosPref, 		String importeRecBenef,
				String montoAnt,		String montoNvo,				String icNotasCredito,
				String fechaVenc,		String esNoUsuario,			AccesoDB con, 
				ArrayList 				lsNotasDeCreditoMultiplesDoctos) 
		throws Exception {
		System.out.println("Automatico::seleccionarDocumento(E)");
	  	try {
			String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			ResultSet rsBusca = null;
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
			// Fodea 002 - 2010
			StringBuffer lsMontosAplicados 	= new StringBuffer();
			StringBuffer lsSaldosDocto 		= new StringBuffer();
			ArrayList lVarBind	= new ArrayList();
			String qryBusca =
				" SELECT ds.ic_documento, d.ic_estatus_docto,"   +
				"        DECODE (epo.ic_banco_fondeo,"   +
				"                2, 'I',"   +
				"                DECODE (cpe.cs_limite_pyme, 'S', 'P', 'E')"   +
				"               ) tipolimite"   +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        comrel_producto_epo cpe,"   +
				"        comcat_epo epo"   +
				"  WHERE d.ic_documento = ds.ic_documento(+)"   +
				"    AND d.ic_epo = cpe.ic_epo"   +
				"    AND cpe.ic_epo = epo.ic_epo"   +
				"    AND cpe.ic_producto_nafin = ?"   +
				"    AND d.ic_documento = ?"  ;
				lVarBind	= new ArrayList();
				lVarBind.add("1");
				lVarBind.add(icDocumento);
			//System.out.println(qryBusca);
			ps = con.queryPrecompilado(qryBusca, lVarBind);
			rsBusca = ps.executeQuery();
			String qryDocto = "";
			Acuse no_acuse = null;
			no_acuse=new Acuse(Acuse.ACUSE_EPO_MODIF,"1");
			String acuse_his = "";

			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
			boolean operaNotasDeCredito 						= selec.operaNotasDeCreditoParaDescuentoElectronico(icEpo);
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
			boolean aplicarNotasDeCreditoAVariosDoctos 	= selec.hasAplicarNotaDeCreditoAVariosDoctosEnabled(icEpo);
 
			// Fodea 002 - 2010.
			if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos && lsNotasDeCreditoMultiplesDoctos != null){
				icNotasCredito = "";
				if(lsNotasDeCreditoMultiplesDoctos.size()>0){
					for(int j=0;j<lsNotasDeCreditoMultiplesDoctos.size();j++){
											
						NotaCreditoMultiple n = (NotaCreditoMultiple) lsNotasDeCreditoMultiplesDoctos.get(j);
											
						if(j>0){
							icNotasCredito 				+= ","; 
							lsMontosAplicados.append(",");	
							lsSaldosDocto.append(",");
						}
		 
						icNotasCredito += n.getIcNotaCredito();
						lsMontosAplicados.append(n.getMontoNotaAplicado());
						lsSaldosDocto.append(n.getSaldoDocto());
					}
				}
			}
			
			if (rsBusca.next()) { // Encontr� un registro, por lo que procedemos a la actualizaci�n de datos
				String 	lsExisteDocto 	= rsBusca.getString("ic_documento");
				int 	estatus 		= rsBusca.getInt("ic_estatus_docto");
				String 	tipoLimite		= rsBusca.getString("tipolimite");
				System.out.println("Automatico::seleccionarDocumento(Estatus antes de seleccion) " + estatus);
				if (lsExisteDocto != null) {
					if (estatus==2 || estatus==26) {
						qryDocto =
							"UPDATE COM_DOCTO_SELECCIONADO "+
							" SET ic_epo = ?, ic_if = ?, "+
							" dc_fecha_tasa = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"+
							" cc_acuse = ?, in_tasa_aceptada = ?, "+
							" in_importe_interes = ROUND(?,?), "+
							" in_importe_recibir = ROUND(?,?), "+
							" df_fecha_seleccion = to_date(?, 'dd/mm/yyyy'),"+
							" cs_dia_siguiente = ?, cg_rel_mat = ?, cg_puntos = ?"+
							" , fn_tipo_cambio = ?" +
							" , cg_tipo_tasa = ? " +
							" , fn_puntos_pref = ?" +
							" , fn_importe_recibir_benef = ?" +
							" , cg_tipo_limite = ?"+
							" WHERE ic_documento = ?";
							
						ps1 = con.queryPrecompilado(qryDocto);
						int cont = 1;
						ps1.setInt(cont++,Integer.parseInt(icEpo));
						ps1.setInt(cont++,Integer.parseInt(icIf));
						ps1.setString(cont++, dcFechaTasa);
						ps1.setString(cont++, acuse);
						ps1.setDouble(cont++,Double.parseDouble(tasaAceptada));
						ps1.setDouble(cont++,Double.parseDouble(inImporteInteres));
						ps1.setInt(cont++,2);
						ps1.setDouble(cont++,Double.parseDouble(inImporteRecibir));
						ps1.setInt(cont++,2);
						ps1.setString(cont++, fechaHoy);
						ps1.setString(cont++, "N");
						ps1.setString(cont++, cgRelMat);
						ps1.setDouble(cont++,Double.parseDouble(cgPuntos));	
						ps1.setDouble(cont++,new Float(valorTC).doubleValue());
						ps1.setString(cont++, cgTipoTasa);
						ps1.setDouble(cont++,Double.parseDouble(fnPuntosPref));
						if(importeRecBenef==null)
							ps1.setNull(cont++,Types.DOUBLE);
						else
							ps1.setDouble(cont++,Double.parseDouble(importeRecBenef));						
						ps1.setString(cont++, tipoLimite);
						ps1.setLong(cont++,new Long(icDocumento).longValue());
					} else {
						proceso = 2;
						mensaje = "Error el documento ya no es negociable"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
					}
				} else {
					if (estatus==2 || estatus==26) {
						qryDocto =
							" INSERT INTO com_docto_seleccionado("   +
							"              ic_documento, ic_epo, ic_if, dc_fecha_tasa, cc_acuse, in_tasa_aceptada, in_importe_interes,"   +
							"              in_importe_recibir, df_fecha_seleccion, cs_dia_siguiente, cg_rel_mat, cg_puntos, fn_tipo_cambio,"   +
							"              cg_tipo_tasa, fn_puntos_pref, fn_importe_recibir_benef, cg_tipo_limite)"   +
							"      VALUES ("   +
							"              ?, ?, ?, to_date(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, ROUND(?, ?),"   +
							"              ROUND(?, ?), to_date(?, 'DD/MM/YYYY'), ?, ?, ?, ?,"   +
							"              ?, ?, ?, ?)"  ;
						ps1 = con.queryPrecompilado(qryDocto);
						int cont = 1;
						ps1.setLong(cont++,new Long(icDocumento).longValue());
						ps1.setInt(cont++,Integer.parseInt(icEpo));
						ps1.setInt(cont++,Integer.parseInt(icIf));
						ps1.setString(cont++, dcFechaTasa);						
						ps1.setString(cont++, acuse);
						ps1.setDouble(cont++,Double.parseDouble(tasaAceptada));
						ps1.setDouble(cont++,Double.parseDouble(inImporteInteres));
						ps1.setInt(cont++,2);
						ps1.setDouble(cont++,Double.parseDouble(inImporteRecibir));
						ps1.setInt(cont++,2);
						ps1.setString(cont++, fechaHoy);
						ps1.setString(cont++, "N");
						ps1.setString(cont++, cgRelMat);
						ps1.setDouble(cont++,Double.parseDouble(cgPuntos));
						ps1.setDouble(cont++,new Float(valorTC).doubleValue());
						ps1.setString(cont++, cgTipoTasa);
						ps1.setDouble(cont++,Double.parseDouble(fnPuntosPref));
						if(importeRecBenef==null)
							ps1.setNull(cont++,Types.DOUBLE);
						else
							ps1.setDouble(cont++,Double.parseDouble(importeRecBenef));
						ps1.setString(cont++, tipoLimite);
					} else {
						CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"95",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
						proceso = 2;
						mensaje = "Error el documento ya no es negociable"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
					}
				}
				System.out.println("Automatico::seleccionarDocumento(qryDocto) " + qryDocto);
			} else {	 //FIN if rsBusca.next()
				CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"95",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
				proceso = 2;
				mensaje = "Error el documento ya no es negociable"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
			}
			rsBusca.close();ps.close();
			//System.out.println(qryDocto);
	  		try {
				ps1.executeUpdate();
				ps1.close();
				//actualizamos el estatus del documento a Seleccionado Pyme(3)
				qryDocto =
					"UPDATE COM_DOCUMENTO set ic_estatus_docto = ? "+
					",  ic_if = ? " +
					",  fn_monto_dscto = ROUND(?,?) "+
					",  fn_porc_anticipo = ?";
				if(!"".equals(icNotasCredito)){
					qryDocto +=
						" ,fn_monto=?"+
						" ,fn_monto_ant=?"+
						" ,cs_cambio_importe = ? ";
				}
				qryDocto += " WHERE ic_documento = ?";
//System.out.println("\n qryDocto: "+qryDocto);
				ps1 = con.queryPrecompilado(qryDocto);
				int cont = 1;
				ps1.setInt(cont++, 3);
				ps1.setInt(cont++, Integer.parseInt(icIf));
				ps1.setDouble(cont++, Double.parseDouble(fnMontoDscto));
				ps1.setInt(cont++, 2);
				ps1.setDouble(cont++, Double.parseDouble(fnPorcAnticipo));
				if(!"".equals(icNotasCredito)) {
					ps1.setDouble(cont++, Double.parseDouble(montoNvo));
					ps1.setDouble(cont++, Double.parseDouble(montoAnt));
					ps1.setString(cont++, "S");
				}
				ps1.setLong(cont++,new Long(icDocumento).longValue());
				//System.out.println(qryDocto);
				try {
					ps1.executeUpdate();
					ps1.close();
				} catch (SQLException sqle) {
					sqle.printStackTrace();
					PreparedStatement psE = null;
					qryDocto = "UPDATE COM_DOCTO_SELECCIONADO SET CC_ACUSE=? WHERE IC_DOCUMENTO=?";
					psE = con.queryPrecompilado(qryDocto);
					psE.setNull(1,Types.VARCHAR);
					psE.setLong(2,new Long(icDocumento).longValue());
					psE.executeUpdate();
					psE.close();

					System.out.println("ejecutaSQL(). Error SQL: " + sqle.getMessage());
					montoDescuentoAcuse=0;
					montoInteresAcuse=0;
					montoRecibirAcuse=0;
					proceso = 1;
					/********** FALLA POR LIMITE DE CREDITO
					*******************/
					if (sqle.getErrorCode()==20003) {
						// Error especifico por limite
						CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"87",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
					} else if (sqle.getErrorCode()==20004) {
						// Error especifico por limite SIRAC
						CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"89",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
					}else {
						CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"88",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
					}
					this.sDtosProc = icDocumento; //Se continua con el siguiente registro
				} catch (Exception e) {
					CSeleccionDocumentoBean.ovDocumentoRechazado(CveProc,icDocumento,icIf,"88",fnPorcAnticipo,diasMinimos,diaDscto,icEpo,con);
				}
								
				//se mueve codigo de las noptas de credito fuera de try que cacha la exception que se arroja cuando se sobrepasa el limite
				/*afectaciones por las notas de credito*/
					if(!"".equals(icNotasCredito)) {
						ps = null;
						PreparedStatement ps2 = null;
						PreparedStatement ps3 = null;
						Vector vNotas = Comunes.explode(",",icNotasCredito);
						//String notasAux = "";
						StringBuffer notasAux = new StringBuffer();
						ArrayList 	numerosNotas	= new ArrayList();
						ArrayList	montos			= new ArrayList();
						double		montoDoc		= Double.parseDouble(montoAnt);
						
						// Fodea 002 - 2010
						HashMap notaCreditoMultiple = new HashMap();
					
						//String numerosNotas  = "";
						for(int i =0;i<vNotas.size();i++) {
							if("".equals(notasAux.toString()))
								notasAux.append("?");
							else
								notasAux.append(",?");
							
						}
						qryDocto =
							" SELECT IG_NUMERO_DOCTO,fn_monto,IC_DOCUMENTO FROM COM_DOCUMENTO"+
							" WHERE IC_DOCUMENTO IN("+notasAux.toString()+")";
						ps = con.queryPrecompilado(qryDocto);
						for(int i =0;i<vNotas.size();i++) {
							ps.setLong(i+1, new Long(vNotas.get(i).toString()).longValue());
						}
						rsBusca = ps.executeQuery();
						ps.clearParameters();							
						while(rsBusca.next()){
							numerosNotas.add(rsBusca.getString("IG_NUMERO_DOCTO"));
							montos.add(new Double(rsBusca.getDouble("FN_MONTO")));
							
							// Fodea 002 - 2010
							if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){
								notaCreditoMultiple.put(rsBusca.getString("IC_DOCUMENTO"),rsBusca.getString("IG_NUMERO_DOCTO"));
							}
							
						}
						rsBusca.close();ps.close();
						
						if("".equals(acuse_his)) {
							acuse_his = no_acuse.toString();
							qryDocto =
								"insert into com_acuse4(cc_acuse, df_fecha_hora_carga, ic_usuario) "+
								"values(?, to_date(?, 'dd/mm/yyyy'), ?)";
							ps = con.queryPrecompilado(qryDocto);
							ps.setString(1, acuse_his);
							ps.setString(2, fechaHoy);
							ps.setString(3, esNoUsuario);							
							ps.executeUpdate();
							ps.close();
						}

						qryDocto =
							" INSERT INTO comhis_cambio_estatus"   +
							" (dc_fecha_cambio, "+
							"  ic_documento, "+
							"  ic_cambio_estatus, "+
							"  fn_monto_anterior, "+
							"  fn_monto_nuevo, "+
							"  ct_cambio_motivo, "+
							"  cc_acuse)"   +
							" VALUES(to_date(to_char(sysdate,'dd/mm/yyyy hh:mi:')||?,'dd/mm/yyyy hh:mi:ss'),?,28,?,?,?,?)"  ;
						ps = con.queryPrecompilado(qryDocto);
						
						// Opera Notas de Credito de la Forma Tradicional					
						if(operaNotasDeCredito && !aplicarNotasDeCreditoAVariosDoctos){
							for(int j=0;j<numerosNotas.size();j++){
								Double montoNota = (Double)montos.get(j);
								ps.setInt(1,j);
								ps.setString(2,icDocumento);
								ps.setDouble(3,montoDoc);
								montoDoc -= montoNota.doubleValue();
								ps.setDouble(4,montoDoc);
								ps.setString(5,"Aplicacion de Nota de Credito: "+numerosNotas.get(j));
								ps.setString(6,acuse_his);
								ps.execute();
								ps.clearParameters();
							}
						// Fodea 002 - 2010. Opera Nota de Credito a Multiples Documentos
						}else if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){
							
							// Realizar Insert en COMREL_NOTA_DOCTO
							qryDocto =
								"INSERT INTO comrel_nota_docto  "  +
								"	(                            "  +
								"		IC_NOTA_CREDITO,          "  +
								"		IC_DOCUMENTO,             "  +
								"		FN_MONTO_APLICADO,        "  +
								"		FN_SALDO_DOCTO,           "  +
								"     CC_ACUSE                  "  +
								"	)                            "  +
								"VALUES                         "  +
								"	(?,?,?,?,?) ";
						
							ps2 = con.queryPrecompilado(qryDocto);
							
							String notasCredito[]		= icNotasCredito.split(",");
							String montosAplicados[]	= lsMontosAplicados.toString().split(",");
							String saldosDocto[]			= lsSaldosDocto.toString().split(",");
	 
							for(int j=0;j<notasCredito.length;j++){
								String 		icNotaCredito					= notasCredito[j];
								String 		igNumeroDoctoDeNotaCredito = (String) notaCreditoMultiple.get(icNotaCredito);
								BigDecimal	montoAplicado 					= new BigDecimal(montosAplicados[j]);
								BigDecimal  saldoDocto						= new BigDecimal(saldosDocto[j]);
								BigDecimal  montoDocto						= montoAplicado.add(saldoDocto);
								// INSERTAR EN COMHIS_CAMBIO_ESTATUS
								ps.setInt(1,j);
								ps.setString(2,icDocumento);
								ps.setDouble(3,montoDocto.doubleValue());
								ps.setDouble(4,saldoDocto.doubleValue());
								ps.setString(5,"Aplicacion de Nota de Credito Multiple: "+ igNumeroDoctoDeNotaCredito);
								ps.setString(6,acuse_his);
								ps.execute();
								// INSERTAR EN COMREL_NOTA_DOCTO
								ps2.setString(1,icNotaCredito);
								ps2.setString(2,icDocumento);// IC_DOCUMENTO del Docto. al que se le aplico la Nota de Credito
								ps2.setDouble(3,montoAplicado.doubleValue());
								ps2.setDouble(4,saldoDocto.doubleValue());
								ps2.setString(5,acuse);
								ps2.execute();
							}
							ps2.close();
						}
						ps.close();
						
						qryDocto  =
							" UPDATE COM_DOCUMENTO"+
							" SET ic_estatus_docto = ?, "+
							"   ic_if = ?,"+
							"	df_fecha_venc = to_date(?,'dd/mm/yyyy'),"+
							"	ic_docto_asociado = ?"+
							" WHERE ic_documento = ?";
							ps = con.queryPrecompilado(qryDocto);
						qryDocto =
							" INSERT INTO COM_DOCTO_SELECCIONADO"+
							"(IC_DOCUMENTO, IC_IF, IC_EPO, DF_FECHA_SELECCION,CC_ACUSE)"+
							"VALUES(?,?,?,sysdate,?)";
						ps2 = con.queryPrecompilado(qryDocto);
						qryDocto =
							" UPDATE com_docto_seleccionado "+
							" SET " +
							" ic_if = ?, " +
							" ic_epo = ?, " +
							" df_fecha_seleccion = SYSDATE, " +
							" cc_acuse = ? "+
							" WHERE ic_documento = ? ";
						ps3 = con.queryPrecompilado(qryDocto);

						String strQueryExisteDoctoSelec = 
								" SELECT COUNT(*) " +
								" FROM com_docto_seleccionado " +
								" WHERE ic_documento = ? ";
						PreparedStatement psExisteDoctoSelec = con.queryPrecompilado(strQueryExisteDoctoSelec);
						
						for(int j=0;j<vNotas.size();j++){
							
							psExisteDoctoSelec.clearParameters();
							ps.clearParameters();
							ps2.clearParameters();
							ps3.clearParameters();
							
							String icNota = (String)vNotas.get(j);
							System.out.println("-- DENTRO IC NOTA = "+icNota);
							ps.setInt(1, 3);
							ps.setInt(2,Integer.parseInt(icIf));
							ps.setString(3,fechaVenc);
							// Notas de Credito Forma Tradicional
							if(operaNotasDeCredito && !aplicarNotasDeCreditoAVariosDoctos){// Version anterior
								ps.setLong(4,new Long(icDocumento).longValue());
							// Fodea 002 - 2010. Notas de Credito a Multiples Documentos
							}else if(operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){
								// Debido a que una Nota de Credito Multiple puede ser aplicada a varios doctos,
								// no se puede guardar la referencia del Docto al que se le aplico en el campo
								// com_documento.ic_docto_asociado, debido a que faltaria espacio; para saber a que
								// documentos se aplico utilizar, SOLO PARA ESTE CASO (NOTA DE CREDITO MULTIPLE), 
								// la tabla COMREL_NOTA_DOCTO.
								// Por lo que IC_DOCTO_ASOCIADO tendra valor NULL
								ps.setNull(4,java.sql.Types.NUMERIC);
							}	
							ps.setLong(5,new Long(icNota).longValue());
							ps.executeUpdate();
							

							psExisteDoctoSelec.setLong(1, Long.parseLong(icNota));
							ResultSet rsExisteDoctoSelec =  psExisteDoctoSelec.executeQuery();
							boolean existeDoctoSelec = false;
							if (rsExisteDoctoSelec.next()) {
								existeDoctoSelec = ( rsExisteDoctoSelec.getInt(1) > 0 )?true:false;
							}
							rsExisteDoctoSelec.close();
							//Si un documento se selecciona y luego se regresa a negociable, se queda el registro en
							//com_docto_seleccionado. Por tal motivo es necesario realizar la validaci�n
							//de si existe para determinar si se hace insert o update.
							if (existeDoctoSelec) {
								ps3.setString(1, icIf);
								ps3.setString(2, icEpo);
								ps3.setString(3, acuse);
								ps3.setString(4,icNota);
								ps3.executeUpdate();
							} else {
								ps2.setString(1,icNota);
								ps2.setString(2,icIf);
								ps2.setString(3,icEpo);
								ps2.setString(4,acuse);
								ps2.executeUpdate();
							}
						}
						ps.close();
						ps2.close();
						ps3.close();
						psExisteDoctoSelec.close();
					}//if(!"".equals(icNotasCredito))
	  		} catch (Exception sqle) {
				if (proceso != 2) {
					proceso = 2;
					mensaje = "Error en la insercion del documento seleccionado"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
				}
			}
		} catch(Exception e) {  // fin try
			System.out.println("Automatico::seleccionarDocumento(Exception)");
			e.printStackTrace();
			throw e;
		} finally {
			System.out.println("Automatico::seleccionarDocumento(S)");
		}
	}//Fin obtenerAcuse



	/**
	 * Proceso para cobranza general considerando que las tasas se
	 * obtienen por plazo.
	 * Proceso para Pedidos y Obra publica
	 */

	private void generaProceso(String icEpo, String iNoCliente, String icIf,
			String icMoneda, String iNoUsuario, String numContrato,
			Integer numCampoAdicionalConReferencia, AccesoDB con)	{
  	try {
		//Declaracion de variables
	   	String aforo = "1";
		String dcFechaTasa = "";
		double valor = 0, puntos = 0;
		String cgrmt = "";
		int datos = 0, tasas=0;
		float valorTC = 1;
		String tipoPiso = "";
		String tipoTasa = "B";
		String puntosPref = "0";
		double fnValor = 0;
		String condicionReferencia = "";

		//variables locales de para eliminar querys anidados
		boolean hayIF	 = false;
		boolean hayCD	 = false;
		boolean hayDoctos = false;
		int	 numDoctos = 0;
		AutorizacionDescuentoBean autDscto = new AutorizacionDescuentoBean();

		String tasaAceptada="", cgRelMat = "", cgPuntos = "";
		String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", icTasa ="";
		String icClaseDocto = "", qrySentencia= "", nombreEpo="";
		String calificacion = "";
		String dfFechaVencNva = "";
		String dFechaVencNva = "";
		String sPlazoNvo = "";

		String sufijo = "";
		if (icMoneda.equals("1")) { //Moneda Nacional
			sufijo = "";
		} else if (icMoneda.equals("54")) { //D�lares
			sufijo = "_dl";
		}


		ResultSet rsAforo = null, rsCalificacion =null,  rsIf = null, rsCD = null;
		ResultSet rsTC = null, rsDoctos = null, rsBusca = null;

		BigDecimal lbMontoDscto = new BigDecimal(0.0);
		Vector vIntereses = new Vector();

		//Obtenemos el aforo de la EPO para la Pyme
		String qryAforo = "select (in_porc_descuento" + sufijo + "/100) " +
				" from comrel_if_epo where ic_if = " + icIf + " AND ic_epo = " + icEpo;
		rsAforo = con.queryDB(qryAforo);
		if(rsAforo.next()) {
			aforo = rsAforo.getString(1);
		} else {
			aforo = null;
		}
		con.cierraStatement();

		if(aforo == null || aforo.equals("1")) {


			qryAforo = "Select min(x.fn_aforo)/100 from (select fn_aforo" + sufijo + " from comrel_producto_epo where ic_producto_nafin = 1 AND ic_epo= ? union Select fn_aforo" + sufijo + " from comcat_producto_nafin where ic_producto_nafin = 1) x";
			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(qryAforo);
			long numero1=Long.parseLong(icEpo);
			pstmt.setLong(1,numero1);
			rsAforo = pstmt.executeQuery();
			pstmt.clearParameters();

			if (rsAforo.next())
			{
				aforo = rsAforo.getString(1);
				System.out.println("AFORO="+rsAforo.getString(1));
			}
			rsAforo.close();
			con.cierraStatement();
		}

		/* A continuaci�n se obtendr� el vector de tasas para la pyme, epo, if. Estas eliminan el concepto de califiacion pero si consideran las tasas preferenciales */

		Vector valoresTasas = new Vector();
		Vector valoresTasa = new Vector();
		CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
		String lsPlazoMaximo="0";
		valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf);
		if (valoresTasas.size()>0) {
 		//  A PARTIR DE AQUI
 			/* Determina cual es el plazo maximo de  los documentos */
 			valoresTasa = (Vector) valoresTasas.get(valoresTasas.size()-1);
 			lsPlazoMaximo= valoresTasa.get(6).toString();
			/* Verificacion de la existencia de la relacion con la IF y de la cuenta bancaria */
			String qryIf = "SELECT DISTINCT PI.ic_if, "+
					  "	 I.cg_razon_social, "+
					  "	 CB.cg_numero_cuenta, "+
					  "	 CB.cg_banco, "+
					  "	 CB.cg_sucursal, " +
					  "	 IE.fn_limite_utilizado, "+
					  "	 IE.in_limite_epo, "+
					  "		I.ig_tipo_piso "+
					  " FROM comrel_cuenta_bancaria CB, "+
					  "	 comrel_pyme_if PI, "+
					  "	 comrel_if_epo IE, "+
					  "	 comcat_if I " +
					  " WHERE CB.ic_pyme = " + iNoCliente +
					  "	 AND CB.ic_moneda = " + icMoneda +
					  "	 AND CB.ic_cuenta_bancaria = PI.ic_cuenta_bancaria " +
					  "	 AND PI.ic_epo = " + icEpo +
					  "	 AND UPPER(PI.cs_vobo_if) = UPPER('S') "+
					  "	 AND UPPER(PI.cs_borrado) = UPPER('N') "+
					  "	 AND PI.ic_if = I. ic_if " +
					  "	 AND PI.ic_if = "+ icIf +
					  "	 AND UPPER(CB.cs_borrado) = 'N' " +
					  "	 AND PI.ic_epo = IE.ic_epo " +
					  "	 AND PI.ic_if = IE.ic_if"+
					  "	 AND PI.ic_if = "+ icIf;
			//System.out.println(qryIf);
			rsIf = con.queryDB(qryIf);
			if (rsIf.next()) {
				hayIF = true;
				tipoPiso = rsIf.getString(8);
			}
			if(rsIf!=null){
				rsIf.close();
				rsIf = null;
			}
			con.cierraStatement();
			if(hayIF) {
				datos ++;

				qrySentencia="select pd.ic_clase_docto"+
					" from comrel_producto_docto pd"+
					" where pd.ic_epo="+icEpo+
					"   and pd.ic_producto_nafin=1";

				rsCD = con.queryDB(qrySentencia);
				if (rsCD.next()) {
					icClaseDocto = rsCD.getString("IC_CLASE_DOCTO");
					hayCD = true;
				}
				if(rsCD!=null){
					rsCD.close();
					rsCD = null;
				}
				con.cierraStatement();
				if(hayCD) {
					if (!icMoneda.equals("1")) {
						String qryTC = "SELECT round(fn_valor_compra, 2) FROM COM_TIPO_CAMBIO " +
							" WHERE ic_moneda = " + icMoneda +
							" AND dc_fecha = (Select max(TC.dc_fecha) " +
							" FROM COM_TIPO_CAMBIO TC " +
							" WHERE TC.ic_moneda = " + icMoneda + ")";
						//System.out.println(qryTC);
						rsTC = con.queryDB(qryTC);
						if (rsTC.next()) {
							valorTC = rsTC.getFloat(1);
						}
						if(rsTC!=null) {
							rsTC.close();
							rsTC=null;
						}
						con.cierraStatement();
					}	 //fin if icMoneda.equals("1")

					/***** El vector anterior ya incluye los valores de tasas preferenciales  ******/


					if (numContrato != null ) {
						int numCampoReferencia = 0;
						//Para pedidos el numero de campo que contiene la referencia
						//se recibe como par�metro en numCampoAdicionalConReferencia
						//para obra p�blica se determina de la tabla comrel_visor
						if(numCampoAdicionalConReferencia != null) {
							numCampoReferencia = numCampoAdicionalConReferencia.intValue();
						} else {
							String strSQLCampoReferencia = "select ic_no_campo from comrel_visor " +
									" where ic_epo = " + icEpo +
									" and ic_producto_nafin = 1 " +
									" and cs_obligatorio = 'S'";
							ResultSet rsCampoReferencia = con.queryDB(strSQLCampoReferencia);
							if (rsCampoReferencia.next()) {
								numCampoReferencia = rsCampoReferencia.getInt("ic_no_campo");
							} else {
								System.out.println("ERROR: No se encontro el campo de referencia.");
							}
							rsCampoReferencia.close();
							con.cierraStatement();
						}

						condicionReferencia = " AND d.cg_campo" + numCampoReferencia +
								" = '" + numContrato +"' ";
					}

					String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
					PreparedStatement ps = con.queryPrecompilado(qrySentencia);
					ps.executeUpdate();
					ps.close();

					qrySentencia =
						" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
						"   FROM dual"  ;
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1, Integer.parseInt(icEpo));
					ResultSet rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
					}
					rs.close();ps.close();



					String qryDoctos = "SELECT D.ig_numero_docto, "+ //1
						"   TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') FECHA_DOCTO, " +
						"   TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') FECHA_VENCTO, " + //3
						"   (D.df_fecha_venc - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) PLAZO, " + //4
						"   D.ic_moneda, "+
						"   D.fn_monto, "+
						"   (D.fn_monto * " + aforo + "), " +
						"   D.ic_documento, " + //8
						"   (" + aforo + " * 100) as PORCENTAJE, "+ //9
						"   TO_CHAR(D.df_fecha_venc,'dd') as  diaVencimiento, "+
						"   E.cg_razon_social "+
						" FROM COM_DOCUMENTO D, COMREL_PYME_EPO PE, COMCAT_EPO E " +
						"  WHERE "   +
						"	D.ic_epo = PE.ic_epo"   +
						"	AND D.ic_pyme = PE.ic_pyme"   +
						"	AND PE.ic_epo = E.ic_epo"   +
						"	AND D.df_fecha_venc > trunc(SYSDATE)"   +
						"	AND (D.df_fecha_venc - trunc(SYSDATE))<=" + lsPlazoMaximo +
						"	AND D.cs_dscto_especial = 'N'"   +
						"	AND D.ic_moneda = " + icMoneda +
						"	AND D.ic_pyme = " + iNoCliente +
						"	AND D.ic_epo = " + icEpo +
						"	AND D.ic_estatus_docto = 2"   +
						condicionReferencia +
						"	AND rownum = 1"   +
						"  ORDER BY D.df_fecha_venc, D.fn_monto ASC"  ;
					//System.out.println("QueryDoctos: " + qryDoctos);
					rsDoctos = con.queryDB(qryDoctos);
					int i = 0;
					double totalMonto = 0, totalDescuento = 0, totalIntereses = 0, totalRecibir = 0;
					double fnMontoDscto=0, inImporteInteres=0, inImporteRecibir=0, fnPorcAnticipo=0;
					int icDocumento=0, igPlazo=0;
					if (rsDoctos.next()) {

						dfFechaDocto	= rsDoctos.getString(2);
						dfFechaVenc	 = rsDoctos.getString(3);
						totalMonto	  = rsDoctos.getDouble(6);
						totalDescuento  = rsDoctos.getDouble(7);
						fnMontoDscto	= rsDoctos.getDouble(7);
						icDocumento	 = rsDoctos.getInt(8);
						fnPorcAnticipo  = rsDoctos.getDouble(9);
						diaVencimiento  = rsDoctos.getString("DIAVENCIMIENTO");
						nombreEpo	   = rsDoctos.getString("CG_RAZON_SOCIAL");

						/*** Calculo de los intereses ***/
						lbMontoDscto = new BigDecimal(totalDescuento);
						vIntereses = selec.ovcalculaInteres(lbMontoDscto, rsDoctos.getInt("PLAZO"),valoresTasas, tipoPiso );
						if (vIntereses.size()>=10)
						{
							/*** Agregar las variables de las tasas ***/

							totalIntereses  = Double.parseDouble(vIntereses.get(11).toString());
							totalRecibir	= (lbMontoDscto.subtract(new BigDecimal(totalIntereses))).doubleValue();
							inImporteInteres= totalIntereses;
							inImporteRecibir= totalRecibir;
							dcFechaTasa = vIntereses.get(0).toString().trim();
							cgRelMat = vIntereses.get(1).toString();
							tasaAceptada = vIntereses.get(3).toString();
							cgPuntos = vIntereses.get(4).toString();
							tipoTasa = vIntereses.get(7).toString();;
							puntosPref = vIntereses.get(8).toString();
							icTasa = vIntereses.get(9).toString();

							hayDoctos = true;
						} else
						{ System.out.println("No se encontro tasa para el documento "+icDocumento+" por lo que no ser� tomado." );
						  hayDoctos = false;
						}
					}
					if(rsDoctos!=null) {
						rsDoctos.close();
						rsDoctos=null;
					}
					con.cierraStatement();

					if(hayDoctos) {
						//Se obtiene el tipo de financiamiento del IF, default Nafin
						String tipoFinanciamiento = "N", porcComision = "NULL", icEstatusSolic = "1";
						String qryTipoFin = "SELECT iexp.cs_tipo_fondeo, pn.fg_porc_comision_fondeo " +
							" FROM comrel_if_epo_x_producto iexp, comcat_producto_nafin pn " +
							" WHERE pn.ic_producto_nafin = iexp.ic_producto_nafin " +
							"	AND iexp.ic_producto_nafin = 1 " +
							"	AND iexp.ic_epo = " + icEpo +
							"	AND iexp.ic_if = " + icIf ;
						//System.out.println(qryTipoFin);
						rsIf = con.queryDB(qryTipoFin);
						if (rsIf.next()) {
							tipoFinanciamiento = rsIf.getString("CS_TIPO_FONDEO");
							porcComision = rsIf.getString("FG_PORC_COMISION_FONDEO");
						}
						if(rsIf!=null) {
							rsIf.close();
							rsIf = null;
						}
						con.cierraStatement();
						if (tipoFinanciamiento.equals("N")) {
							porcComision = "NULL";
						}else{
							icEstatusSolic = "10";
						}
						//System.out.println(icDocumento);
						//________________________GENERACION DEL NUMERO DE ACUSE PYME:___________________
						Acuse acuse = new Acuse(Acuse.ACUSE_PYME,PRODUCTO_DSCTO,con);
						String _acuse = "NULL";
						String qryAcuse="insert into com_acuse2 (cc_acuse, fn_total_monto_mn, fn_total_int_mn, fn_total_imp_mn, "+
							"fn_total_monto_dl, fn_total_int_dl, fn_total_imp_dl,"+
							"df_fecha_hora, ic_usuario, cg_recibo_electronico) values ('"+acuse+"', ";
						if (icMoneda.equals("1")) {
							qryAcuse += totalDescuento+", "+totalIntereses+", "+totalRecibir+", NULL, NULL, NULL ";
						} else {
							qryAcuse +=" NULL, NULL, NULL, "+totalDescuento+", "+totalIntereses+", "+totalRecibir;
						}
						qryAcuse +=", sysdate, '"+iNoUsuario+"','"+_acuse+"')";
						System.out.println("************\nInsercion Acuse Automatico PYME: " + qryAcuse + "\n******************\n");
						//System.out.print(qryAcuse);
						con.ejecutaSQL(qryAcuse);
						System.out.println("SI EJECUTO EL QUERY DE LA INSERSION");
						String qryBusca = "SELECT d.ic_documento, d.ic_estatus_docto FROM COM_DOCUMENTO d, COM_DOCTO_SELECCIONADO ds  WHERE d.ic_documento = ds.ic_documento and d.ic_documento = "+ icDocumento;
						//System.out.println(qryBusca);
						rsBusca = con.queryDB(qryBusca);
						String qryDocto = "";
						System.out.println("EL QUERY QUE TRAE EL ESTATUS "+qryBusca);
						if (rsBusca.next()) { // Encontr� un registro, por lo que procedemos a la actualizaci�n de datos
							int estatus = rsBusca.getInt("ic_estatus_docto");
							System.out.println("SI TRAJO RESULTADOS EL RSBUSCA Y EL ESTATUS ES = "+estatus);
							if (estatus==2) {
								qryDocto = "UPDATE COM_DOCTO_SELECCIONADO "+
									" SET ic_epo = "+icEpo+", ic_if = "+ icIf+", "+
									" dc_fecha_tasa = TO_DATE('"+dcFechaTasa+"','DD/MM/YYYY HH24:MI:SS'),"+
									" cc_acuse = '"+acuse+"', in_tasa_aceptada = "+tasaAceptada+", "+
									" in_importe_interes = ROUND("+inImporteInteres+",2), "+
									" in_importe_recibir = ROUND("+inImporteRecibir+",2), "+
									" df_fecha_seleccion = SYSDATE,"+
									" cs_dia_siguiente = 'N', cg_rel_mat = '"+cgRelMat+"', cg_puntos = "+cgPuntos+
									" , fn_tipo_cambio = " + valorTC +
									" , cg_tipo_tasa = '" + tipoTasa + "' " +
									" , fn_puntos_pref = " + puntosPref +
									" WHERE ic_documento = "+icDocumento;
									System.out.println("EFECTIVAMENTE EL ESTATUS FUE 2 Y ARMO EL UPDATE");
							} else {
								proceso = 2;
								mensaje = "Error el documento ya no es negociable"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
							}
						} else {	//FIN if rsBusca.next()
							qryDocto = "INSERT INTO COM_DOCTO_SELECCIONADO("+
								" ic_documento, "+
								" ic_epo, ic_if, "+
								" dc_fecha_tasa, "+
								" cc_acuse, "+
								" in_tasa_aceptada, "+
								" in_importe_interes, "+
								" in_importe_recibir, "+
								" df_fecha_seleccion, "+
								" cs_dia_siguiente, "+
								" cg_rel_mat, "+
								" cg_puntos, "+
								" fn_tipo_cambio, "+
								" cg_tipo_tasa, "+
								" fn_puntos_pref ) "+
								"VALUES("+
								icDocumento+
								", "+icEpo+
								", "+icIf+
								", TO_DATE('"+dcFechaTasa+"','DD/MM/YYYY HH24:MI:SS'), '"+
								acuse +
								"', "+tasaAceptada+
								", ROUND("+inImporteInteres+",2) "+
								", ROUND("+inImporteRecibir+",2) "+
								", SYSDATE" +
								", 'N' "+
								",'"+cgRelMat+"'"+
								", "+cgPuntos+
								", "+valorTC+
								", '" + tipoTasa + "'" +
								", " + puntosPref + ")";
								System.out.println("NO TRAJO REGISTROS Y SE TENDR� QUE INSERTAR EL ELEMENTO");
						}
						if(rsBusca!=null) {
							rsBusca.close();
							rsBusca = null;
						}
						con.cierraStatement();
						//System.out.println(qryDocto);
						try {
							con.ejecutaSQL(qryDocto);
						  }catch(Exception e){
						  	proceso = 2;
						  }
						//actualizamos el estatus del documento a Aplicado a Credito(16)
						qryDocto = "UPDATE COM_DOCUMENTO set ic_estatus_docto = 16 "+
							",  ic_if = " + icIf +
							",  fn_monto_dscto = ROUND("+ fnMontoDscto +",2) "+
							",  fn_porc_anticipo = " + fnPorcAnticipo +
							" WHERE ic_documento = "+icDocumento;
						//System.out.println(qryDocto);
						con.ejecutaSQL(qryDocto);


						//________________________GENERACION DEL NUMERO DE ACUSE IF:___________________
						String usuarioIf = "dscto_aut";
						acuse= new Acuse(Acuse.ACUSE_IF, PRODUCTO_DSCTO,con);

						qryAcuse="insert into com_acuse3 (cc_acuse, in_monto_mn, in_monto_int_mn"+
							" , in_monto_dscto_mn, in_monto_dl, in_monto_int_dl"+
							" , in_monto_dscto_dl, df_fecha_hora, ic_usuario,cg_recibo_electronico)"+
							" values ('"+acuse+"', ";
						if (icMoneda.equals("1")) {
							qryAcuse += totalDescuento+", "+totalIntereses+", "+totalRecibir+", NULL, NULL, NULL ";
						} else {
							qryAcuse +=" NULL, NULL, NULL, "+totalDescuento+", "+totalIntereses+", "+totalRecibir;
						}
						qryAcuse +=", sysdate, '"+usuarioIf+"','"+_acuse+"')";
						System.out.println("************\nInsercion Acuse Automatico IF: " + qryAcuse + "\n******************\n");
						con.ejecutaSQL(qryAcuse);

						/*Insercion de solicitud seleccionada IF*/
						qrySentencia="select count(*) from com_solicitud where ic_documento="+icDocumento;
						//System.out.println(qrySentencia);
						rsBusca = con.queryDB(qrySentencia);
						if(rsBusca.next()) {
							numDoctos = rsBusca.getInt(1);
						}
						if(rsBusca!=null) {
							rsBusca.close();
							rsBusca = null;
						}
						con.cierraStatement();
						if (numDoctos == 0) {  //El ic_documento no existe en solicitudes?

							//String tmpCliente = ""+(Integer.parseInt(icIf) + 1) ;
							//qrySentencia=" select /*+ index( s CP_COM_SOLICITUD_PK) */  substr(lpad(nvl('"+icIf+"','0'),3,'0'),1,3)||"   +
							/*		" 	   to_char(sysdate,'y')||"   +
									"		substr(lpad(nvl(max(to_number(substr(s.ic_folio,5,6)))+1,1),6,'0'),1,6) consecutivo "   +
									" from com_solicitud s"   +
									" where to_char(sysdate,'yyyy')=to_char(DF_FECHA_SOLICITUD,'yyyy')"   +
									" and   s.ic_folio between '" + Comunes.rellenaCeros(icIf,3) + "00000000' and '"+ Comunes.rellenaCeros(tmpCliente,3) +"00000000'";
									//" and   nvl(to_number('"+icIf+"'),0)=to_number(substr(s.ic_folio,1,3))"  ;
							System.out.println(qrySentencia);

							int consecutivo = 0;
							rsBusca = con.queryDB(qrySentencia);
							if(rsBusca.next()) {
								//MPCS FODA 095 consecutivo = rsBusca.getInt("CONSECUTIVO")+1;
								consecutivo = rsBusca.getInt("CONSECUTIVO");
							}
							if(rsBusca!=null) {
								rsBusca.close();
								rsBusca = null;
							}
							con.cierraStatement();*/

							String folio = autDscto.getFolioSolicitud(icIf, con);

							//MPCS FODA 085 Ajuste de los plazos por base de operacion
							qrySentencia=" select fecha fechav,to_char(to_date(fecha,'dd/mm/yyyy'),'dd') diav, TO_DATE(fecha,'dd/mm/yyyy')-trunc(sysdate) plazov"   +
									" from (select ajustaplazo("+icEpo+","+icMoneda+",2,"+tipoPiso+",'"+dfFechaVenc+"') fecha "   +
									"	   from dual)"  ;
							//System.out.println(qrySentencia);
							rsBusca = con.queryDB(qrySentencia);
							if(rsBusca.next()) {
								dfFechaVencNva = rsBusca.getString("FECHAV");
								dFechaVencNva = rsBusca.getString("DIAV");
								sPlazoNvo = rsBusca.getString("PLAZOV");
							}
							if(rsBusca!=null) {
								rsBusca.close();
								rsBusca = null;
							}
							con.cierraStatement();

							//String folio = Comunes.calculaFolio(consecutivo,10);

							qrySentencia="insert into com_solicitud ("+
						  			  " ic_folio, ic_documento, ic_esquema_amort, ic_tipo_credito,"+
									  " ic_oficina, cc_acuse, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap,"+
									  " cg_perio_pago_int, cg_desc_bienes, ic_clase_docto,"+
									  " cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif,"+
									  " ic_tabla_amort, in_numero_amort, df_ppc, df_ppi,"+
									  " df_v_documento, df_v_descuento, in_dia_pago,"+
									  " df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic,"+
									  " df_fecha_solicitud, ic_bloqueo, cs_tipo_solicitud,"+
									  " cs_dia_siguiente, cg_emisor, fg_porc_comision_fondeo)"+
									  " values ('"+folio+"', "+icDocumento+", 1, 1, 90, '"+acuse+"'"+
									  " , TO_NUMBER('"+sPlazoNvo+"')"+
									  " , 'D', 'Al Vto.', 'Al Vto.', ' ', "+icClaseDocto+", 'Mexico, D.F.'"+
									  " , "+icTasa+", '+', 0, 1, 1"+
									  " , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy') , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy')"+
									  " , TO_DATE('"+dfFechaVenc+"','dd/mm/yyyy') , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy')"+
									  " , "+dFechaVencNva+", TO_DATE('"+dfFechaDocto+"','dd/mm/yyyy'), 'Mexico, D.F.'"+
									  " , 0, "+icEstatusSolic+", sysdate, NULL, 'C', 'N','"+nombreEpo+"'," + porcComision +")";
								  //System.out.println(qrySentencia);
								  try {
									con.ejecutaSQL(qrySentencia);
			  						  System.out.println(new java.util.Date()+" Solicitud: "+folio+" insertada Automatica IF");
									//Se movio hacia adelante	  proceso = 1;
									montoCobro = inImporteRecibir;
									documento = icDocumento;
								  } catch(SQLException sqle) {
										proceso = 2;
										System.out.println(new java.util.Date()+" Error al insertar la solicitud  Automatica IF "+folio);
										System.out.println("Query fallido de insercion Automatica: "+qrySentencia);
								  }

									qrySentencia="insert into com_amortizacion (ic_amortizacion, ic_folio, in_numero"+
			  						  " , fn_importe, df_fecha)"+
			  						  " values (seq_com_amortizacion_ic_amor.NEXTVAL, '"+folio+"', 1,"+inImporteRecibir+","+
			  						  " TO_DATE('"+dfFechaVenc+"','dd/mm/yyyy'))";
							//System.out.println(qrySentencia);
			  					  con.ejecutaSQL(qrySentencia);
								  proceso = 1;
						  	   }//fin del if (ic_solicitud no existen en com_solicitud)
							   else
			  				   {
						  proceso = 2;
									mensaje = "Error en la transaccion IF. �Transaccion CANCELADA!";
			   				   }
				   		}	 // fin if hayDoctos antes --> if rsDoctos.next()
					else // No encontro documento posiblemente porque no hay tasa para el documento obtenido
					  {
					  mensaje = "No hay documentos en la moneda: " + icMoneda + "Epo: " + icEpo + "IF: " + icIf + "Pyme: " + iNoCliente;
					  }
				}	 //fin if hayCD   antes --> if rsCd.next()
			  else
				{
				mensaje = "No hay Clases de Doctos para el producto NAFIN: 1 y la EPO: " + icEpo;
				}
			  }	 //  fin if hayIF   antes  --> if rsIf.next()
			else
			  {
			  mensaje = "No existe una relacion PYME - IF en la Moneda y EPO seleccionada. Moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf + " Pyme: " + iNoCliente;
			  }
//			  con.cierraStatement();
		} else { // else hay tasas
			mensaje = "No hay tasa en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf + " Pyme: " + iNoCliente;
	  	}//   fin  If hay tasas


	} catch(Exception e) {  // fin try
		e.printStackTrace();
		mensaje = e.toString();
		proceso = 2;
	}
}//Fin generaProceso


   	/*********************************************************************************************
	*
	*		void generaProceso()
	*  Obtiene los documentos para inventarios en el proceso de pignoraci�n
	*	   considera las tasas por plazo (MPCS-10/jul/03)
	*********************************************************************************************/
	// Agregado 12/12/2002	--CARP
	private void generaProceso(String esCveEPO, String esCvePyme, String esCveIF,
								String esCveMoneda, String esFechaPago, String esTipoPiso,
								String esDiasMinimos, AccesoDB eoConDB)
	{
	  	try {
			//Declaracion de variables
			boolean ExistenDoctos = false;
		   	String aforo = obtenerAforo(esCveEPO, esCveIF, esCveMoneda, eoConDB);
			Vector valoresTasas = new Vector();
			Vector valoresTasa = new Vector();
			String lsPlazoMaximo ="0";
			CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
			try
			{	valoresTasas = selec.ovgetTasaAceptada(esCvePyme,esCveEPO,esCveMoneda,esCveIF);
			}	catch(NafinException ne) {}
			if  (valoresTasas.size()>0)
			{
				System.out.println("Se encontraron tasas para la epo,pyme, if y moneda");
				/* Busqueda del plazo maximo */
				valoresTasa=(Vector) valoresTasas.get(valoresTasas.size()-1);
				lsPlazoMaximo = valoresTasa.get(6).toString();
				float valorTC 		= obtenerTipoCambio(esCveMoneda, eoConDB);
				System.out.println("Obtiene el documento\n");
				Vector lovDatosDocumento = obtenerDocumento(esCveMoneda, aforo, valoresTasas, esCvePyme, esCveEPO,
															 esDiasMinimos, esTipoPiso, esCveIF, esFechaPago,lsPlazoMaximo,
															 eoConDB);
				if (lovDatosDocumento.size() > 0) {
					m_sMontoDscto 	= lovDatosDocumento.get(0).toString();
					m_sMontoInteres = lovDatosDocumento.get(1).toString();
					m_dMontoRecibir = Double.parseDouble(lovDatosDocumento.get(2).toString());
					documento = Integer.parseInt(lovDatosDocumento.get(3).toString());
					m_sPorcAnticipo = lovDatosDocumento.get(4).toString();
					m_sTasa = lovDatosDocumento.get(8).toString();
					m_iPlazo =  ((Integer) lovDatosDocumento.get(5)).intValue();
					/* Estas variables no se usan en el proceso subsecuente:
					String dcFechaTasa 	= lovDatosDocumento.get(6).toString();
					String tasaAceptada = lovDatosDocumento.get(8).toString();
					String cgRelMat 	= lovDatosDocumento.get(7).toString();
					String cgPuntos 	= lovDatosDocumento.get(9).toString();
					String valorTasaDia = lovDatosDocumento.get(12).toString();
					String tipoTasa 	= lovDatosDocumento.get(10).toString();
					String puntosPref 	= lovDatosDocumento.get(11).toString();*/

					ExistenDoctos = true;
				}
			} else {
				mensaje = "No hay tasa en la moneda: " + esCveMoneda + " Epo: " + esCveEPO + " IF: " + esCveIF ;
			}
			proceso = ExistenDoctos? 1 : -1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaProceso

   	/*********************************************************************************************
	*
	*		void generaProceso()
	*  Obtiene los documentos para inventarios en el proceso de cobranza
	*	   considera las tasas por plazo (MPCS-15/jul/03)
	*********************************************************************************************/
	// Modificado 20/12/2002	--CARP
	private void generaProceso(String icEpo, String iNoCliente, String icIf, String icMoneda, String iNoUsuario,
							   AccesoDB con, String esProducto)
	{
  	try {
		//Declaracion de variables
	   	String aforo = "1";
		String dcFechaTasa = "";
		double valor = 0, puntos = 0;
		String cgrmt = "";
		int datos = 0, tasas=0;
		float valorTC = 1;
		String tipoPiso = "";
		String tipoTasa = "B";
		String puntosPref = "0";
		double fnValor = 0;
		String sufijo = "";
		if (icMoneda.equals("1")) { //Moneda Nacional
			sufijo = "";
		} else if (icMoneda.equals("54")) { //D�lares
			sufijo = "_dl";
		}

		//variables locales  para eliminar querys anidados
		boolean hayIF	 = false;
		boolean hayCD	 = false;
		boolean hayDoctos = false;
		int	 numDoctos = 0;


		String tasaAceptada="", cgRelMat = "", cgPuntos = "";
		String dfFechaDocto="", dfFechaVenc="", diaVencimiento="", icTasa ="";
		String icClaseDocto = "", qrySentencia= "", nombreEpo="";
		String calificacion = "";
		String dfFechaVencNva = "";
		String dFechaVencNva = "";
		String sPlazoNvo = "";
		AutorizacionDescuentoBean autDscto = new AutorizacionDescuentoBean();

		ResultSet rsAforo = null, rsCalificacion =null, rsTasa = null, rsIf = null, rsCD = null;
		ResultSet rsTC = null, rsDoctos = null, rsBusca = null;

		BigDecimal lbMontoDscto = new BigDecimal(0.0);
		Vector vIntereses = new Vector();

		//Obtenemos el aforo de la EPO para la Pyme
		String qryAforo = "select (in_porc_descuento" + sufijo + "/100) from comrel_if_epo where ic_if = " + icIf + " AND ic_epo = " + icEpo;
		rsAforo = con.queryDB(qryAforo);
		if(rsAforo.next()) {
			aforo = rsAforo.getString(1);
		} else {
			aforo = null;
		}
		con.cierraStatement();

		if(aforo == null || aforo.equals("1")) {

			qryAforo = "Select min(x.fn_aforo" + sufijo + ")/100 "+
					" from ( " +
					" select fn_aforo" + sufijo +
					" from comrel_producto_epo where ic_epo= ? " +
					" and ic_producto_nafin = 1 " +
					" union " +
					" Select fn_aforo" + sufijo +
					" from comcat_producto_nafin " +
					" where ic_producto_nafin = 1) x";
			PreparedStatement pstmt = null;
			pstmt = con.queryPrecompilado(qryAforo);
			long numero1=Long.parseLong(icEpo);
			pstmt.setLong(1,numero1);
			rsAforo = pstmt.executeQuery();
			pstmt.clearParameters();


			if (rsAforo.next())
			{
				aforo = rsAforo.getString(1);
				System.out.println("AFORO="+rsAforo.getString(1));
			}
			con.cierraStatement();
		}

		/* A continuaci�n se obtendr� el vector de tasas para la pyme, epo, if. Estas eliminan el concepto de califiacion pero si consideran las tasas preferenciales */

		Vector valoresTasas = new Vector();
		Vector valoresTasa = new Vector();
		CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
		String lsPlazoMaximo="0";
		try
		{	valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf);
		}	catch (NafinException ne) {}
		if (valoresTasas.size()>0) {
		//  A PARTIR DE AQUI HAY QUE HACER LOS CAMBIOS JRFH
			valoresTasa = (Vector) valoresTasas.get(valoresTasas.size()-1);
			lsPlazoMaximo= valoresTasa.get(6).toString();
			String qryIf = "SELECT DISTINCT PI.ic_if, "+
					  "	 I.cg_razon_social, "+
					  "	 CB.cg_numero_cuenta, "+
					  "	 CB.cg_banco, "+
					  "	 CB.cg_sucursal, " +
					  "	 IE.fn_limite_utilizado, "+
					  "	 IE.in_limite_epo, "+
					  "		I.ig_tipo_piso "+
					  " FROM comrel_cuenta_bancaria CB, "+
					  "	 comrel_pyme_if PI, "+
					  "	 comrel_if_epo IE, "+
					  "	 comcat_if I " +
					  " WHERE CB.ic_pyme = " + iNoCliente +
					  "	 AND CB.ic_moneda = " + icMoneda +
					  "	 AND CB.ic_cuenta_bancaria = PI.ic_cuenta_bancaria " +
					  "	 AND PI.ic_epo = " + icEpo +
					  "	 AND UPPER(PI.cs_vobo_if) = UPPER('S') "+
					  "	 AND UPPER(PI.cs_borrado) = UPPER('N') "+
					  "	 AND PI.ic_if = I. ic_if " +
					  "	 AND PI.ic_if = "+ icIf +
					  "	 AND UPPER(CB.cs_borrado) = 'N' " +
					  "	 AND PI.ic_epo = IE.ic_epo " +
					  "	 AND PI.ic_if = IE.ic_if"+
					  "	 AND PI.ic_if = "+ icIf;
			//System.out.println(qryIf);
			rsIf = con.queryDB(qryIf);
			if (rsIf.next()) {
				hayIF = true;
				tipoPiso = rsIf.getString(8);
			}
			if(rsIf!=null){
				rsIf.close();
				rsIf = null;
			}
			con.cierraStatement();
			if(hayIF) {
				datos ++;

				qrySentencia="select pd.ic_clase_docto"+
					" from comrel_producto_docto pd"+
					" where pd.ic_epo="+icEpo+
					"   and pd.ic_producto_nafin=1";

				rsCD = con.queryDB(qrySentencia);
				if (rsCD.next()) {
					icClaseDocto = rsCD.getString("IC_CLASE_DOCTO");
					hayCD = true;
				}
				if(rsCD!=null){
					rsCD.close();
					rsCD = null;
				}
				con.cierraStatement();
				if(hayCD) {
					if (!icMoneda.equals("1")) {
						String qryTC = "SELECT round(fn_valor_compra, 2) FROM COM_TIPO_CAMBIO " +
							" WHERE ic_moneda = " + icMoneda +
							" AND dc_fecha = (Select max(TC.dc_fecha) " +
							" FROM COM_TIPO_CAMBIO TC " +
							" WHERE TC.ic_moneda = " + icMoneda + ")";
						//System.out.println(qryTC);
						rsTC = con.queryDB(qryTC);
						if (rsTC.next()) {
							valorTC = rsTC.getFloat(1);
						}
						if(rsTC!=null) {
							rsTC.close();
							rsTC=null;
						}
						con.cierraStatement();
					}	 //fin if icMoneda.equals("1")


					String qryDoctos = "SELECT D.ig_numero_docto, "+ //1
						"   TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') FECHA_DOCTO, " +
						"   TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') FECHA_VENCTO, " + //3
						"   (D.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')) PLAZO, " +
						"   D.ic_moneda, "+
						"   D.fn_monto, "+ //6
						"   (D.fn_monto * " + aforo + "), "+ //7
						"   D.ic_documento, " + //8 (10)
						"   (" + aforo + " * 100) as PORCENTAJE, "+
						"   TO_CHAR(D.df_fecha_venc,'dd') as  diaVencimiento, "+
						"   E.cg_razon_social "+ //11
						" FROM COM_DOCUMENTO D, COMREL_PYME_EPO PE, COMCAT_EPO E "+
						" WHERE D.ic_epo = PE.ic_epo " +
						"   AND D.ic_pyme = PE.ic_pyme " +
						"   AND PE.ic_epo = E.ic_epo " +
						"   AND D.df_fecha_venc > TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY'),'DD/MM/YYYY') " +
						"   AND (D.df_fecha_venc - trunc(SYSDATE))<=" + lsPlazoMaximo +
						"   AND D.ic_epo = " + icEpo +
						"   AND D.ic_moneda = " + icMoneda +
						"   AND D.ic_pyme = " + iNoCliente +
						"   AND D.cs_dscto_especial = 'N' " ;
	//Agregado 201202 		--CARP
						if (esProducto.equals("5"))
							qryDoctos += " AND D.ic_estatus_docto in (2,23) ";
						else
							qryDoctos += " AND D.ic_estatus_docto = 2 ";
	//--------------------
						qryDoctos += "   AND rownum = 1 " +
						" ORDER BY D.df_fecha_venc, D.fn_monto ASC  ";
					//System.out.println(qryDoctos);
					rsDoctos = con.queryDB(qryDoctos);
					int i = 0;
					double totalMonto = 0, totalDescuento = 0, totalIntereses = 0, totalRecibir = 0;
					double fnMontoDscto=0, inImporteInteres=0, inImporteRecibir=0, fnPorcAnticipo=0;
					int icDocumento=0, igPlazo=0;
					if (rsDoctos.next()) {
						dfFechaDocto	= rsDoctos.getString(2);
						dfFechaVenc	 = rsDoctos.getString(3);
						totalMonto	  = rsDoctos.getDouble(6);
						totalDescuento  = rsDoctos.getDouble(7);
						fnMontoDscto	= rsDoctos.getDouble(7);
						icDocumento	 = rsDoctos.getInt(8);
						fnPorcAnticipo  = rsDoctos.getDouble(9);
						diaVencimiento  = rsDoctos.getString("DIAVENCIMIENTO");
						nombreEpo	   = rsDoctos.getString("CG_RAZON_SOCIAL");
						/*** Calculo de los intereses ***/
						lbMontoDscto = new BigDecimal(totalDescuento);
						vIntereses = selec.ovcalculaInteres(lbMontoDscto, rsDoctos.getInt("PLAZO"),valoresTasas, tipoPiso );
						if (vIntereses.size()>=10)
						{	/* Variables que se asignan a partir del calculo de intereses */
							totalIntereses  = Double.parseDouble(vIntereses.get(11).toString());
							totalRecibir	= (lbMontoDscto.subtract(new BigDecimal(totalIntereses))).doubleValue();
							inImporteInteres= totalIntereses;
							inImporteRecibir= totalRecibir;
							dcFechaTasa = vIntereses.get(0).toString().trim();
							cgRelMat = vIntereses.get(1).toString();
							tasaAceptada = vIntereses.get(3).toString();
							cgPuntos = vIntereses.get(4).toString();
							tipoTasa = vIntereses.get(7).toString();;
							puntosPref = vIntereses.get(8).toString();
							icTasa = vIntereses.get(9).toString();
							hayDoctos = true;
						} else
						{ System.out.println("No se encontro tasa para el documento "+icDocumento+" por lo que no ser� tomado." );
						  hayDoctos = false;
						} // Validacion de tasas para el documento encontrado
					} // Se encontro un documento candidato
					if(rsDoctos!=null) {
						rsDoctos.close();
						rsDoctos=null;
					}
					con.cierraStatement();
					// Prosigue solo si encontro documento
					if(hayDoctos) {
						//System.out.println(icDocumento);
						//Se obtiene el tipo de financiamiento del IF, default Nafin
						String tipoFinanciamiento = "N", porcComision = "NULL", icEstatusSolic = "1";
						String qryTipoFin = "SELECT iexp.cs_tipo_fondeo, pn.fg_porc_comision_fondeo " +
							" FROM comrel_if_epo_x_producto iexp, comcat_producto_nafin pn " +
							" WHERE pn.ic_producto_nafin = iexp.ic_producto_nafin " +
							"	AND iexp.ic_producto_nafin = 1 " +
							"	AND iexp.ic_epo = " + icEpo +
							"	AND iexp.ic_if = " + icIf ;
						//System.out.println(qryTipoFin);
						rsIf = con.queryDB(qryTipoFin);
						if (rsIf.next()) {
							tipoFinanciamiento = rsIf.getString("CS_TIPO_FONDEO");
							porcComision = rsIf.getString("FG_PORC_COMISION_FONDEO");
						}
						if(rsIf!=null) {
							rsIf.close();
							rsIf = null;
						}
						con.cierraStatement();
						if (tipoFinanciamiento.equals("N")) {
							porcComision = "NULL";
						}else{
							icEstatusSolic = "10";
						}

						//________________________GENERACION DEL NUMERO DE ACUSE PYME:___________________
						Acuse acuse = new Acuse(Acuse.ACUSE_PYME,PRODUCTO_DSCTO,con);
						String _acuse = "NULL";
						String qryAcuse="insert into com_acuse2 (cc_acuse, fn_total_monto_mn, fn_total_int_mn, fn_total_imp_mn, "+
							"fn_total_monto_dl, fn_total_int_dl, fn_total_imp_dl,"+
							"df_fecha_hora, ic_usuario, cg_recibo_electronico) values ('"+acuse+"', ";
						if (icMoneda.equals("1")) {
							qryAcuse += totalDescuento+", "+totalIntereses+", "+totalRecibir+", NULL, NULL, NULL ";
						} else {
							qryAcuse +=" NULL, NULL, NULL, "+totalDescuento+", "+totalIntereses+", "+totalRecibir;
						}
						qryAcuse +=", sysdate, '"+iNoUsuario+"','"+_acuse+"')";
						System.out.println("************\nInsercion Acuse Automatico PYME: " + qryAcuse + "\n******************\n");
						//System.out.print(qryAcuse);
						con.ejecutaSQL(qryAcuse);

						String qryBusca = "SELECT d.ic_documento, d.ic_estatus_docto FROM COM_DOCUMENTO d, COM_DOCTO_SELECCIONADO ds  WHERE d.ic_documento = ds.ic_documento and d.ic_documento = "+ icDocumento;
						//System.out.println(qryBusca);
						rsBusca = con.queryDB(qryBusca);
						String qryDocto = "";
						if (rsBusca.next()) { // Encontr� un registro, por lo que procedemos a la actualizaci�n de datos
							int estatus = rsBusca.getInt("ic_estatus_docto");
							if (estatus==2) {
								qryDocto = "UPDATE COM_DOCTO_SELECCIONADO "+
									" SET ic_epo = "+icEpo+", ic_if = "+ icIf+", "+
									" dc_fecha_tasa = TO_DATE('"+dcFechaTasa+"','DD/MM/YYYY HH24:MI:SS'),"+
									" cc_acuse = '"+acuse+"', in_tasa_aceptada = "+tasaAceptada+", "+
									" in_importe_interes = ROUND("+inImporteInteres+",2), "+
									" in_importe_recibir = ROUND("+inImporteRecibir+",2), "+
									" df_fecha_seleccion = SYSDATE,"+
									" cs_dia_siguiente = 'N', cg_rel_mat = '"+cgRelMat+"', cg_puntos = "+cgPuntos+
									" , fn_tipo_cambio = " + valorTC +
									" , cg_tipo_tasa = '" + tipoTasa + "' " +
									" , fn_puntos_pref = " + puntosPref +
									" WHERE ic_documento = "+icDocumento;
							} else {
								proceso = 2;
								mensaje = "Error el documento ya no es negociable"; //Se cancela el proceso y se envia el mensaje de error de concurrencia
							}
						} else {	//FIN if rsBusca.next()
							qryDocto = "INSERT INTO COM_DOCTO_SELECCIONADO("+
								" ic_documento, "+
								" ic_epo, ic_if, "+
								" dc_fecha_tasa, "+
								" cc_acuse, "+
								" in_tasa_aceptada, "+
								" in_importe_interes, "+
								" in_importe_recibir, "+
								" df_fecha_seleccion, "+
								" cs_dia_siguiente, "+
								" cg_rel_mat, "+
								" cg_puntos, "+
								" fn_tipo_cambio, "+
								" cg_tipo_tasa, "+
								" fn_puntos_pref ) "+
								"VALUES("+
								icDocumento+
								", "+icEpo+
								", "+icIf+
								", TO_DATE('"+dcFechaTasa+"','DD/MM/YYYY HH24:MI:SS'), '"+
								acuse +
								"', "+tasaAceptada+
								", ROUND("+inImporteInteres+",2) "+
								", ROUND("+inImporteRecibir+",2) "+
								", SYSDATE" +
								", 'N' "+
								",'"+cgRelMat+"'"+
								", "+cgPuntos+
								", "+valorTC+
								", '" + tipoTasa + "'" +
								", " + puntosPref + ")";
						}
						if(rsBusca!=null) {
							rsBusca.close();
							rsBusca = null;
						}
						con.cierraStatement();
						//System.out.println(qryDocto);
						con.ejecutaSQL(qryDocto);
						//actualizamos el estatus del documento a Aplicado a Credito(16)
						qryDocto = "UPDATE COM_DOCUMENTO set ic_estatus_docto = 16 "+
							",  ic_if = " + icIf +
							",  fn_monto_dscto = ROUND("+ fnMontoDscto +",2) "+
							",  fn_porc_anticipo = " + fnPorcAnticipo +
							" WHERE ic_documento = "+icDocumento;
						//System.out.println(qryDocto);
						con.ejecutaSQL(qryDocto);


						//________________________GENERACION DEL NUMERO DE ACUSE IF:___________________
						String usuarioIf = "dscto_aut";
						acuse= new Acuse(Acuse.ACUSE_IF, PRODUCTO_DSCTO,con);

						qryAcuse="insert into com_acuse3 (cc_acuse, in_monto_mn, in_monto_int_mn"+
							" , in_monto_dscto_mn, in_monto_dl, in_monto_int_dl"+
							" , in_monto_dscto_dl, df_fecha_hora, ic_usuario,cg_recibo_electronico)"+
							" values ('"+acuse+"', ";
						if (icMoneda.equals("1")) {
							qryAcuse += totalDescuento+", "+totalIntereses+", "+totalRecibir+", NULL, NULL, NULL ";
						} else {
							qryAcuse +=" NULL, NULL, NULL, "+totalDescuento+", "+totalIntereses+", "+totalRecibir;
						}
						qryAcuse +=", sysdate, '"+usuarioIf+"','"+_acuse+"')";
						System.out.println("************\nInsercion Acuse Automatico IF: " + qryAcuse + "\n******************\n");
						con.ejecutaSQL(qryAcuse);

						/*Insercion de solicitud seleccionada IF*/
						qrySentencia="select count(1) from com_solicitud where ic_documento="+icDocumento;
						//System.out.println(qrySentencia);
						rsBusca = con.queryDB(qrySentencia);
						if(rsBusca.next()) {
							numDoctos = rsBusca.getInt(1);
						}
						if(rsBusca!=null) {
							rsBusca.close();
							rsBusca = null;
						}
						con.cierraStatement();
						if (numDoctos == 0) {  //El ic_documento no existe en solicitudes?

							//String tmpCliente = ""+(Integer.parseInt(icIf) + 1) ;
							//qrySentencia=" select /*+ index( s CP_COM_SOLICITUD_PK) */ substr(lpad(nvl('"+icIf+"','0'),3,'0'),1,3)||"   +
							/*		" 	   to_char(sysdate,'y')||"   +
									"		substr(lpad(nvl(max(to_number(substr(s.ic_folio,5,6)))+1,1),6,'0'),1,6) consecutivo "   +
									" from com_solicitud s"   +
									" where to_char(sysdate,'yyyy')=to_char(DF_FECHA_SOLICITUD,'yyyy')"   +
									" and   s.ic_folio between '" + Comunes.rellenaCeros(icIf,3) + "00000000' and '"+ Comunes.rellenaCeros(tmpCliente,3) +"00000000'";
									//" and   nvl(to_number('"+icIf+"'),0)=to_number(substr(s.ic_folio,1,3))"  ;
							System.out.println(qrySentencia);
							int consecutivo = 0;
							rsBusca = con.queryDB(qrySentencia);
							if(rsBusca.next()) {
								//MPCS FODA 095 consecutivo = rsBusca.getInt("CONSECUTIVO")+1;
								consecutivo = rsBusca.getInt("CONSECUTIVO");
							}
							if(rsBusca!=null) {
								rsBusca.close();
								rsBusca = null;
							}
							con.cierraStatement();

							*/

							String folio = autDscto.getFolioSolicitud(icIf, con);


							//MPCS FODA 085 Ajuste de los plazos por base de operacion
							qrySentencia=" select fecha fechav,to_char(to_date(fecha,'dd/mm/yyyy'),'dd') diav, TO_DATE(fecha,'dd/mm/yyyy')-trunc(sysdate) plazov"   +
									" from (select ajustaplazo("+icEpo+","+icMoneda+",5,"+tipoPiso+",'"+dfFechaVenc+"') fecha "   +
									"	   from dual)"  ;
							//System.out.println(qrySentencia);
							rsBusca = con.queryDB(qrySentencia);
							if(rsBusca.next()) {
								dfFechaVencNva = rsBusca.getString("FECHAV");
								dFechaVencNva = rsBusca.getString("DIAV");
								sPlazoNvo = rsBusca.getString("PLAZOV");
							}
							if(rsBusca!=null) {
								rsBusca.close();
								rsBusca = null;
							}
							con.cierraStatement();


							//String folio = Comunes.calculaFolio(consecutivo,10);

						qrySentencia="insert into com_solicitud ("+
						  " ic_folio, ic_documento, ic_esquema_amort, ic_tipo_credito,"+
									  " ic_oficina, cc_acuse, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap,"+
									  " cg_perio_pago_int, cg_desc_bienes, ic_clase_docto,"+
									  " cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif,"+
									  " ic_tabla_amort, in_numero_amort, df_ppc, df_ppi,"+
									  " df_v_documento, df_v_descuento, in_dia_pago,"+
									  " df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic,"+
									  " df_fecha_solicitud, ic_bloqueo, cs_tipo_solicitud,"+
									  " cs_dia_siguiente, cg_emisor, fg_porc_comision_fondeo)"+
									  " values ('"+folio+"', "+icDocumento+", 1, 1, 90, '"+acuse+"'"+
									  " , TO_NUMBER('"+sPlazoNvo+"')"+
									  " , 'D', 'Al Vto.', 'Al Vto.', ' ', "+icClaseDocto+", 'Mexico, D.F.'"+
									  " , "+icTasa+", '+', 0, 1, 1"+
									  " , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy') , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy')"+
									  " , TO_DATE('"+dfFechaVenc+"','dd/mm/yyyy') , TO_DATE('"+dfFechaVencNva+"','dd/mm/yyyy')"+
									  " , "+dFechaVencNva+", TO_DATE('"+dfFechaDocto+"','dd/mm/yyyy'), 'Mexico, D.F.'"+
									  " , 0, "+icEstatusSolic+", sysdate, NULL, 'C', 'N','"+nombreEpo+"'," + porcComision +")";
								  //System.out.println(qrySentencia);
						  try {
							con.ejecutaSQL(qrySentencia);
			  						  System.out.println(new java.util.Date()+" Solicitud: "+folio+" insertada Automatica IF");
									proceso = 1;
									montoCobro = inImporteRecibir;
									documento = icDocumento;
								  } catch (SQLException sqle) {
										proceso = 2;
										System.out.println(new java.util.Date()+" Error al insertar la solicitud  Automatica IF "+folio);
										System.out.println("Query fallido de insercion Automatica: "+qrySentencia);
								  }

									qrySentencia="insert into com_amortizacion (ic_amortizacion, ic_folio, in_numero"+
			  						  " , fn_importe, df_fecha)"+
			  						  " values (seq_com_amortizacion_ic_amor.NEXTVAL, '"+folio+"', 1,"+inImporteRecibir+","+
			  						  " TO_DATE('"+dfFechaVenc+"','dd/mm/yyyy'))";
							//System.out.println(qrySentencia);
			  					  con.ejecutaSQL(qrySentencia);
						  	   }//fin del if (ic_solicitud no existen en com_solicitud)
							   else
			  				   {
						  			proceso = 2;
									mensaje = "Error en la transaccion IF. �Transaccion CANCELADA!";
			   				   }
//					   con.cierraStatement();
/**/
				   		}	 // fin if hayDoctos antes --> if rsDoctos.next()
					else
					  {
					  mensaje = "No hay documentos en la moneda: " + icMoneda + "Epo: " + icEpo + "IF: " + icIf + "Pyme: " + iNoCliente;
					  }
//			   con.cierraStatement();
				}	 //fin if hayCD   antes --> if rsCd.next()
			  else
				{
				mensaje = "No hay Clases de Doctos para el producto NAFIN: 1 y la EPO: " + icEpo;
				}
//				con.cierraStatement();
			  }	 //  fin if hayIF   antes  --> if rsIf.next()
			else
			  {
			  mensaje = "No existe una relacion PYME - IF en la Moneda y EPO seleccionada. Moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf + " Pyme: " + iNoCliente;
			  }
//			  con.cierraStatement();
		} else { // else hay tasas
			mensaje = "No hay tasa en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf + " Pyme: " + iNoCliente;
	  	}//   fin  If valoresTasas traia vectores
	} catch(Exception e) {  // fin try
		e.printStackTrace();
		mensaje = e.toString();
		proceso = 2;
	}
}//Fin generaProceso
	
	private void generaDescuentoVencidoINFO(String icEpo, String iNoCliente, String icIf,
								String icMoneda, String iNoUsuario, String tipoPiso,
								AccesoDB con, String acuse, String diaDscto,
								String diasMinimos, String sDtosProcesados,
								Vector vFechasInhabiles, int orden, String lsCveProc, String lsAforo)
	{
	  	try {
			//Declaracion de variables
		   	String aforo = "1";
			//System.out.println("Aforo: "+aforo);
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec= new CSeleccionDocumentoBean();
			try
			{	valoresTasas = selec.ovgetTasaAceptada(iNoCliente,icEpo,icMoneda,icIf,con);
			} catch (NafinException ne)
			{  /*** REGISTRA EL ERROR DE QUE POR EPO,IF,MONEDA,PYME,TIPODESC NO HAY NINGUNA TASA ****/
				CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"I",icIf,con,tipoPiso);
			}
			String icDocumento="";
			if (valoresTasas.size() > 0) {
				Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto,icMoneda, aforo, valoresTasas, iNoCliente, icEpo, "I", tipoPiso, icIf, con, diasMinimos, sDtosProcesados);
				if (datosDocumento.size() > 0) {
					String totalMonto 		= datosDocumento.get(2).toString();
					//System.out.println("Total Monto: "+totalMonto);
					String totalDescuento 	= datosDocumento.get(3).toString();
					//System.out.println("Total Descuento: "+totalDescuento);
					String totalIntereses 	= datosDocumento.get(4).toString();
					//System.out.println("Total Intereses: "+totalIntereses);
					String totalRecibir 	= datosDocumento.get(5).toString();
					//System.out.println("Total Recibir: "+totalRecibir);
					String fnMontoDscto		= datosDocumento.get(6).toString();
					//System.out.println("Monto Descuento: "+fnMontoDscto);
					String inImporteInteres = datosDocumento.get(7).toString();
					//System.out.println("Importe Interes: "+inImporteInteres);
					String inImporteRecibir	= datosDocumento.get(8).toString();
					//System.out.println("Importe Recibir: "+inImporteRecibir);
					icDocumento		= datosDocumento.get(9).toString();
					//System.out.println("icDocumento: "+icDocumento);
					String fnPorcAnticipo	= datosDocumento.get(10).toString();
					//System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
					String importeRecBenef	= datosDocumento.get(13).toString();
					//System.out.println("Monto Recibir Beneficiario: "+importeRecBenef);
					String sfDsctoAuto		= datosDocumento.get(14).toString();
					//System.out.println("Fecha Dscto Automatico: "+sfDsctoAuto);
					String dcFechaTasa 		= datosDocumento.get(15).toString();
					//System.out.println("Clave Tasa: "+dcFechaTasa);
					String cgRelMat			= datosDocumento.get(16).toString();
					//System.out.println("Rel. Mat.: "+cgRelMat);
					String tasaAceptada		= datosDocumento.get(17).toString();
					//System.out.println("Tasa Aceptada: "+tasaAceptada);
					String cgPuntos			= datosDocumento.get(18).toString();
					//System.out.println("Puntos: "+cgPuntos);
					String tipoTasa			= datosDocumento.get(19).toString();
					String puntosPref		= datosDocumento.get(20).toString();
					float valorTC = obtenerTipoCambio(icMoneda, con);
					//System.out.println("valor Tipo Cambio: "+valorTC);
					if(diaDscto.equals("P") || (diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto)) ) {
					seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres, inImporteRecibir,
										cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
										fnPorcAnticipo, tipoTasa, puntosPref, importeRecBenef, con);
					} else if (vFechasInhabiles!= null) {
						boolean bseleccionaDocto = false;
						for (int i=0; i < vFechasInhabiles.size(); i++) {
							if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
								seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres, inImporteRecibir,
													cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													fnPorcAnticipo, tipoTasa, puntosPref, importeRecBenef, con);
								bseleccionaDocto = true;
								break;
							}
						}
						if (!bseleccionaDocto){
							this.sDtosProc = icDocumento;
							/* No selecciono el documento porque no era momento de cobrarse */
						} else {
							System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
						}
					} else {
						this.sDtosProc = icDocumento;
						/* No selecciono el documento porque no era momento de cobrarse */
					}

				}/*Fin de cuando se encuentra un documento*/

			} else {
				mensaje = "No hay tasa en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf ;
				this.sDtosProc = icDocumento;
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
		}
	}//Fin generaDescuentoVencidoINFO

 /**
	 * Para Factoraje IF  Fodea 2012
	 * @param alNotas
	 * @param lsAforo
	 * @param lsCveProc
	 * @param orden
	 * @param vFechasInhabiles
	 * @param sDtosProcesados
	 * @param diasMinimos
	 * @param diaDscto
	 * @param acuse
	 * @param con
	 * @param tipoPiso
	 * @param iNoUsuario
	 * @param icMoneda
	 * @param icIf
	 * @param iNoCliente
	 * @param icEpo
	 */
  private void generaDescuentoFacIF(
								String icEpo, 			String iNoCliente, 		String icIf,
								String icMoneda, 		String iNoUsuario, 		String tipoPiso,
								AccesoDB con, 			String acuse, 			String diaDscto,
								String diasMinimos, 	String sDtosProcesados,	Vector vFechasInhabiles, 	
								int orden, 				String lsCveProc,		String lsAforo, 
								ArrayList alNotas) {
		try {
			/* Declaracion de variables */
			String montoBenef = null; ///* Se emplea en el metodo de seleccionar el documento */
			Vector valoresTasas = new Vector();
			CSeleccionDocumentoBean selec = new CSeleccionDocumentoBean();
		   String aforo = obtenerAforo(icEpo, icIf, icMoneda, con);
			float valorTC = obtenerTipoCambio(icMoneda, con);
			String existValIf = "", icDocumento="";
			try {
				valoresTasas = selec.ovgetTasaAceptada(iNoCliente, icEpo, icMoneda, icIf, con);
			} catch (NafinException ne) {
				CSeleccionDocumentoBean.ovSinTasa(orden,lsCveProc,diasMinimos,lsAforo,icEpo,iNoCliente,icMoneda,"A",icIf,con); 
			}			
			if  (valoresTasas.size()>0) {
				try {
					Vector datosDocumento = obtenerDocumento(lsCveProc, diaDscto, icMoneda, aforo, valoresTasas, iNoCliente, icEpo, "A", tipoPiso, icIf, con, diasMinimos, sDtosProcesados,alNotas);
					System.out.println("Tama�o del Vector: "+datosDocumento.size());
					if (datosDocumento.size() > 0) {
						String 		totalMonto 								= datosDocumento.get(2).toString();
						String 		totalDescuento 						= datosDocumento.get(3).toString();
						String 		totalIntereses 						= datosDocumento.get(4).toString();
						String 		totalRecibir 							= datosDocumento.get(5).toString();
						String 		fnMontoDscto							= datosDocumento.get(6).toString();
						String 		inImporteInteres 						= datosDocumento.get(7).toString();
						String 		inImporteRecibir						= datosDocumento.get(8).toString();
						icDocumento												= datosDocumento.get(9).toString();
						String 		fnPorcAnticipo							= datosDocumento.get(10).toString();
						String 		sfDsctoAuto								= datosDocumento.get(14).toString();
						String 		dcFechaTasa 							= datosDocumento.get(15).toString();
						String 		cgRelMat									= datosDocumento.get(16).toString();
						String 		tasaAceptada							= datosDocumento.get(17).toString();
						String 		cgPuntos									= datosDocumento.get(18).toString();
						String 		tipoTasa									= datosDocumento.get(19).toString();
						String 		puntosPref								= datosDocumento.get(20).toString();
						String 		montoAnt									= datosDocumento.get(21).toString();
						String 		icNotasAplicadas						= datosDocumento.get(22).toString();
						String 		fechaVenc								= datosDocumento.get(1).toString();
						ArrayList 	lsNotasDeCreditoMultiplesDoctos 	= (ArrayList)datosDocumento.get(23);

						System.out.println("Total Monto: "+totalMonto);
						System.out.println("Total Descuento: "+totalDescuento);
						System.out.println("Total Intereses: "+totalIntereses);
						System.out.println("Total Recibir: "+totalRecibir);
						System.out.println("Monto Descuento: "+fnMontoDscto);
						System.out.println("Importe Interes: "+inImporteInteres);
						System.out.println("Importe Recibir: "+inImporteRecibir);
						System.out.println("icDocumento: "+icDocumento);
						System.out.println("Porcentaje Anticipo: "+fnPorcAnticipo);
						System.out.println("\n Fecha Dscto Automatico: "+sfDsctoAuto);
						System.out.println("diaDscto: "+diaDscto);
						System.out.println("sfDsctoAuto: "+sfDsctoAuto);
						
						existValIf = selec.operaEpoConMandato(iNoCliente,icEpo,icMoneda);
						System.out.println("icEpo>>>>>>>>>"+icEpo);
						System.out.println("iNoCliente>>>>>>>>>"+iNoCliente);
						System.out.println("existValIf>>>>>>>>>"+existValIf);
						if(!"".equals(existValIf) || diaDscto.equals("P") || ( diaDscto.equals("U") && ((new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date())).equals(sfDsctoAuto)) ) {
							seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
											inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
											fnPorcAnticipo, tipoTasa, puntosPref, montoBenef,montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con,lsNotasDeCreditoMultiplesDoctos);
						} else if (vFechasInhabiles != null) {
							boolean bseleccionaDocto = false;
							for (int i=0; i < vFechasInhabiles.size(); i++) {
								if (vFechasInhabiles.get(i).toString().equals(sfDsctoAuto) ) {
									seleccionarDocumento(lsCveProc,diasMinimos,diaDscto, icEpo, icIf, acuse, dcFechaTasa, tasaAceptada, inImporteInteres,
													inImporteRecibir, cgRelMat, cgPuntos, valorTC, icDocumento, fnMontoDscto,
													fnPorcAnticipo, tipoTasa, puntosPref, montoBenef,montoAnt,totalMonto,icNotasAplicadas,fechaVenc,iNoUsuario,con,lsNotasDeCreditoMultiplesDoctos);
									bseleccionaDocto = true;
									break;
								}
							}
							if (!bseleccionaDocto){
								this.sDtosProc = icDocumento;
							} else {
								System.out.println("TOMO EL DOCTO POR DIAS INHABILES");
							}
						} else {
							this.sDtosProc = icDocumento;    
						}
					}
				} catch(ArrayIndexOutOfBoundsException aioobe) { aioobe.printStackTrace(); }
			} else {
				mensaje = "No hay TASAS X PLAZO en la moneda: " + icMoneda + " Epo: " + icEpo + " IF: " + icIf ;   
			}

			proceso = 1;

		} catch(Exception e) {  // fin try
			e.printStackTrace();
			mensaje = e.toString();
			proceso = 2;
			System.out.println("mensaje "+mensaje);
		}
	}//Fin generaDescuentoFacIF
	
  
//Agregados 131202 	--CARP
		public String getMontoDscto()
		{
			return m_sMontoDscto;
		}
		public String getMontoInteres()
		{
			return m_sMontoInteres;
		}
		public double getMontoRecibir()
		{
			return m_dMontoRecibir;
		}
		public String getPorcAnticipo()
		{
			return m_sPorcAnticipo;
		}
		public String getTasa()
		{
			return m_sTasa;
		}
		public int getPlazo()
		{
			return m_iPlazo;
		}

//------------------------

	public int obtieneProceso()
	{
		return proceso;
	}

	public String obtieneMensaje()
	{
		return mensaje;
	}

	public double obtieneCobro()
	{
		return montoCobro;
	}

	public int obtieneDocumento()
	{
		return documento;
	}

	public double obtieneDescuento() {
		return montoDescuentoAcuse;
	}

	public double obtieneInteres() {
		return montoInteresAcuse;
	}

	public double obtieneRecibir() {
		return montoRecibirAcuse;
	}

	public String getDoctoProcesado() {
		return this.sDtosProc;
	}

}