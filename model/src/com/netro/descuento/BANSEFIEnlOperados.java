package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class BANSEFIEnlOperados extends EpoPEF implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

	public BANSEFIEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_bansefi";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe();

	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto = 9" ;		
	}
	public String getBorraDoctosBaja(){
		return "";
	}

	public String getInsertaDoctosOpe(){
		return 
			"INSERT INTO "+getTablaDoctosOpe()+" " +
			"            (cg_razon_social_if, df_fecha_aut, cc_acuse, in_numero_proveedor, " +
			"             cg_razon_social_pyme, ig_numero_docto, df_fecha_docto, " +
			"             df_fecha_venc, ic_moneda, fn_monto, cg_sucursal, cg_banco, " +
			"             cg_cuenta_bco, fn_porc_dscto, fn_monto_dscto, in_tasa_aceptada, " +
			"             ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, " +
			"             cg_campo5 " + getCamposPEFOper()+ ", ic_estatus_docto" +
			"            ) ";

	}


	public String getDoctosOperados(Hashtable alParamEPO){
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR

		return	
			"SELECT SUBSTR (i.cg_razon_social, 1, 50) cg_razon_social_if, a.df_fecha_hora, " +
			"       a.cc_acuse, pe.cg_pyme_epo_interno, " +
			"       SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
			"       d.ig_numero_docto, d.df_fecha_docto, d.df_fecha_venc, d.ic_moneda, " +
			"       d.fn_monto, cb.cg_sucursal, cb.cg_banco, cb.cg_numero_cuenta, " +
			"       d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_tasa_aceptada, " +
			"       d.ct_referencia, d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, " +
			"       d.cg_campo5 " + 
      (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
      (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			" 			, d.ic_estatus_docto " +
			"  FROM com_documento d, " +
			"       com_docto_seleccionado ds, " +
			"       com_solicitud s, " +
			"       com_acuse2 a, " +
			"       comcat_if i, " +
			"       comrel_pyme_epo pe, " +
			"       comrel_pyme_if pi, " +
			"       comrel_cuenta_bancaria cb, " +
			"       comcat_pyme py, " +
			"       comrel_if_epo ie " +
			" WHERE a.cc_acuse = ds.cc_acuse " +
			"   AND s.ic_documento = ds.ic_documento " +
			"   AND ds.ic_documento = d.ic_documento " +
			"   AND d.ic_if = i.ic_if " +
			"   AND d.ic_pyme = pe.ic_pyme " +
			"   AND d.ic_epo = pe.ic_epo " +
			"   AND d.ic_epo = pi.ic_epo " +
			"   AND ds.ic_if = pi.ic_if " +
			"   AND d.ic_moneda = cb.ic_moneda " +
			"   AND d.ic_pyme = cb.ic_pyme " +
			"   AND d.ic_pyme = py.ic_pyme " +
			"   AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
			"   AND d.ic_epo = ie.ic_epo " +
			"   AND d.ic_if = ie.ic_if " +
			"   AND pi.cs_vobo_if = 'S' " +
			"   AND pi.cs_borrado = 'N' " +
			"   AND cb.cs_borrado = 'N' " +
			"   AND d.ic_estatus_docto IN (4, 16) " +
			"   AND s.df_fecha_solicitud >= TRUNC (SYSDATE) " +
			"   AND s.df_fecha_solicitud < TRUNC (SYSDATE) + 1 " +
			"   AND d.ic_epo = " + ic_epo;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
		return "";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
	//FODEA 006-Feb2010 Rebos - Se agrego la notificaci�n de documentos vencidos
	 String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		String condicion = "";
		
		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-3)";
		} else {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-1)";
		}

		String qrySentencia =
	    "SELECT NULL AS cg_razon_social_if, NULL AS df_fecha_hora, " +
			"       NULL AS cc_acuse, pe.cg_pyme_epo_interno, " +
			"       SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
			"       d.ig_numero_docto, d.df_fecha_docto, d.df_fecha_venc, d.ic_moneda, " +
			"       d.fn_monto, NULL AS cg_sucursal, NULL AS cg_banco, NULL AS cg_numero_cuenta, " +
			"       NULL AS fn_porc_anticipo, NULL AS fn_monto_dscto, NULL AS in_tasa_aceptada, " +
			"       d.ct_referencia, d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, " +
			"       d.cg_campo5 " + 
      (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
      (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			" 			, d.ic_estatus_docto" +
			"  FROM com_documento d, " +
			"       comrel_pyme_epo pe, " +						
			"       comcat_pyme py " +			
			" WHERE d.ic_pyme = pe.ic_pyme " +
			"   AND d.ic_epo = pe.ic_epo " +			
			"   AND d.ic_pyme = py.ic_pyme " +
			"   AND d.ic_estatus_docto = 9 "+
			condicion+
			"   AND d.df_fecha_venc < TRUNC(SYSDATE+"+diasMinimos+")"+
			"   AND d.ic_epo = "+ic_epo +
			" UNION " +
	    "SELECT NULL AS cg_razon_social_if, NULL AS df_fecha_hora, " +
			"       NULL AS cc_acuse, pe.cg_pyme_epo_interno, " +
			"       SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
			"       d.ig_numero_docto, d.df_fecha_docto, d.df_fecha_venc, d.ic_moneda, " +
			"       d.fn_monto, NULL AS cg_sucursal, NULL AS cg_banco, NULL AS cg_numero_cuenta, " +
			"       NULL AS fn_porc_anticipo, NULL AS fn_monto_dscto, NULL AS in_tasa_aceptada, " +
			"       d.ct_referencia, d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, " +
			"       d.cg_campo5 " + 
      (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
      (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			" 			, d.ic_estatus_docto" +
			"  FROM com_documento d, " +
			"       comrel_pyme_epo pe, " +						
			"       comcat_pyme py " +			
			" WHERE d.ic_pyme = pe.ic_pyme " +
			"   AND d.ic_epo = pe.ic_epo " +			
			"   AND d.ic_pyme = py.ic_pyme " +
			"   AND d.ic_estatus_docto = 9 "+			
			"   AND d.df_alta > TRUNC(SYSDATE-1)"+
			"   AND d.ic_epo = "+ic_epo;	
		
		return qrySentencia;
	}
	
	/**
	 * Se sobreescribe el metodo de EpoPEF, debido a que en este enlace es necesario 
	 * especificar el dblink ORALINK en la funci�n para que pueda 
	 * funcionar adecuadamente.
	 * @return Cadena con la secci�n del query que determina el digito identificador.
	 */
	public String getCamposDigitoIdentificadorCalc(){ 
		return "  rellenaCeros@ORALINK(d.ic_epo||'','0000') || rellenaCeros@ORALINK(d.ic_documento||'','00000000000') as " + getCamposDigitoIdentificador();	
	}

	
}
