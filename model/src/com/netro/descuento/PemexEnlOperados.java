package com.netro.descuento;

import java.io.Serializable;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
    
public class PemexEnlOperados extends EpoPEF implements EpoEnlOperados,Serializable {

	//modificar esta clase 2017_003
	private String ic_epo = "";

	public PemexEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}
	
	public String getTablaDoctosOpe(){
		return "com_doctos_ope_pemex_pep";
	}
	
	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,16) and ic_epo = "+ic_epo;
	}

	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9) and ic_epo = "+ic_epo;
	}
	
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7) and ic_epo = "+ic_epo;
	}
	
	public String getInsertaDoctosOpe(){ 
		return
		"insert into "+getTablaDoctosOpe()+" (ic_documento, ic_estatus_docto,"+
					" df_fecha_venc, cg_razon_social_pyme, ig_numero_docto, ic_moneda, fn_monto,"+
					" in_numero_proveedor, df_fecha_docto, cc_acuse, cg_razon_social_if,"+
					" ic_nafin_electronico, cg_rfc, cg_sociedad, ic_epo, CG_CAMPO6 " + getCamposPEFOper()+
					", CG_NUM_CONTRATO, CS_DSCTO_ESPECIAL, CG_NUM_COPADE, CG_REFERENCIA ) ";  //2017_003  
	} 
	 
	public String getDoctosOperados(Hashtable alParamEPO){
		String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
		String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		return	"SELECT d.ic_documento, 4 AS ic_estatus_docto, d.df_fecha_venc,"+
				"	       SUBSTR (d.cg_campo1, 1, 70) AS nombrepyme, d.ig_numero_docto,"+
				"	       d.ic_moneda, d.fn_monto,"+
				"	       pe.cg_pyme_epo_interno AS numeroproveedor,"+
				"	       d.df_fecha_docto AS fechaemision, a.cc_acuse,"+
				"	       SUBSTR (i.cg_razon_social, 1, 50) AS nombreif,"+
				"	       ie.cg_num_cuenta AS ic_nafin_electronico,"+
				"	       SUBSTR (d.cg_campo3, 1, 14) AS cg_rfc, "+
				"	       SUBSTR (d.cg_campo4, 1, 4) AS cg_sociedad, "+
				"	       d.ic_epo, " +
                                "              null as CG_CAMPO6 " +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+			
				" , d.CG_NUM_CONTRATO, d.CS_DSCTO_ESPECIAL, d.CG_NUM_COPADE , ie2.CG_REFERENCIA "+  //2017_003    
				"	  FROM com_documento d,"+
				"	       com_docto_seleccionado ds,"+
				"	       com_solicitud s,"+
				"	       com_acuse2 a,"+
				"	       comcat_if i,"+
				"	       comrel_pyme_epo pe,"+
				"	       comrel_if_epo ie,"+ 
				"	       comrel_if_epo ie2 "+  //2017_003
				"	 WHERE a.cc_acuse = ds.cc_acuse"+
				"	   AND s.ic_documento = ds.ic_documento"+
				"	   AND ds.ic_documento = d.ic_documento"+
				"	   AND d.ic_if = i.ic_if"+
				"	   AND d.ic_pyme = pe.ic_pyme"+
				"	   AND d.ic_epo = pe.ic_epo"+
				"	   AND d.ic_epo = ie2.ic_epo(+)"+ //2017_003
				"	   AND d.ic_beneficiario = ie2.ic_if(+)"+ //2017_003
				"	   AND ds.ic_epo = ie.ic_epo"+
				"	   AND ds.ic_if = ie.ic_if"+
				"	   AND d.ic_epo = "+ic_epo+
				"	   AND d.ic_estatus_docto IN (4,16)"+
				"	   AND s.df_fecha_solicitud >= TRUNC(SYSDATE)"+
				"	   AND s.df_fecha_solicitud < TRUNC(SYSDATE+1)"	;
	}
	
	public String getDoctosEliminacion(Hashtable alParamEPO){
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		return " select d.ic_documento, d.ic_estatus_docto, d.df_fecha_venc,"+
				" substr(d.cg_campo1,1,70) as nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"+
				" pe.cg_pyme_epo_interno as numeroProveedor, d.df_fecha_docto as fechaEmision,"+
				" null as cc_acuse, null as cg_razon_social_if, null as ic_nafin_electronico, " +
				" substr(d.cg_campo3,1,14) as cg_rfc, "+
				" SUBSTR (d.cg_campo4, 1, 4) AS cg_sociedad, "+
				" d.ic_epo, " +
                                " null as CG_CAMPO6 " +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
		       " , d.CG_NUM_CONTRATO, d.CS_DSCTO_ESPECIAL, d.CG_NUM_COPADE, ie2.CG_REFERENCIA   "+  //2017_003    
				" from com_documento d, comhis_cambio_estatus ce, comrel_pyme_epo pe"+
		                " , comrel_if_epo ie2 "+ 
				" where d.ic_epo = "+ic_epo+
				" and ce.dc_fecha_cambio >= trunc(sysdate)"+
				" and ce.dc_fecha_cambio < trunc(sysdate+1)"+
				" and d.ic_estatus_docto in (5,6,7)"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.ic_pyme = pe.ic_pyme"+
				" and d.ic_epo = pe.ic_epo"+
				" and ce.IC_CAMBIO_ESTATUS IN (4,5,6) "+
		                " AND d.ic_epo = ie2.ic_epo (+)"+  //2017_003
				" AND d.ic_beneficiario = ie2.ic_if (+)";  //2017_003
	}
	
	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
		
		//FODEA 006-Feb2010 Rebos - Se agrego la notificaci�n de documentos vencidos
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		String condicion = "";

		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
			condicion = " and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-3)";
		} else {
			condicion = " and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-1)";
		}

		String qrySentencia =
			  "SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"+
				"	       SUBSTR (d.cg_campo1, 1, 70) AS nombrepyme, d.ig_numero_docto,"+
				"	       d.ic_moneda, d.fn_monto,"+
				"	       pe.cg_pyme_epo_interno AS numeroproveedor,"+
				"	       d.df_fecha_docto AS fechaemision, "+
				" 			 NULL AS acuse, NULL AS razonsocialif,NULL AS ic_nafin_electronico, "+
				"	       SUBSTR (d.cg_campo3, 1, 14) AS cg_rfc, "+
				"	       SUBSTR (d.cg_campo4, 1, 4) AS cg_sociedad, "+
				"	       d.ic_epo," +
                                "              null as CG_CAMPO6 " +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			  " , d.CG_NUM_CONTRATO, d.CS_DSCTO_ESPECIAL, d.CG_NUM_COPADE, ie2.CG_REFERENCIA   "+  //2017_003 
				"	  FROM com_documento d,"+
				"	       comrel_pyme_epo pe"+
			        " 		,comrel_if_epo ie2 "+//2017_003 
				"	 WHERE d.ic_pyme = pe.ic_pyme"+
				"	   AND d.ic_epo = pe.ic_epo"+
				"	   AND d.ic_epo = "+ic_epo+
				"	   AND d.ic_estatus_docto = 9"+
				condicion+
				" 	 AND d.df_fecha_venc < TRUNC(SYSDATE+"+diasMinimos+")"+
			      " AND d.ic_epo = ie2.ic_epo (+)" +  //2017_003 
			      " AND d.ic_beneficiario = ie2.ic_if (+)"+ //2017_003 
				" UNION " +
				"SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"+
				"	       SUBSTR (d.cg_campo1, 1, 70) AS nombrepyme, d.ig_numero_docto,"+
				"	       d.ic_moneda, d.fn_monto,"+
				"	       pe.cg_pyme_epo_interno AS numeroproveedor,"+
				"	       d.df_fecha_docto AS fechaemision, "+
				" 			 NULL AS acuse, NULL AS razonsocialif,NULL AS ic_nafin_electronico, "+
				"	       SUBSTR (d.cg_campo3, 1, 14) AS cg_rfc, "+
				"	       SUBSTR (d.cg_campo4, 1, 4) AS cg_sociedad, "+
				"	       d.ic_epo," +
                                "              null as CG_CAMPO6 " +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			  " , d.CG_NUM_CONTRATO, d.CS_DSCTO_ESPECIAL, d.CG_NUM_COPADE, ie2.CG_REFERENCIA "+  //2017_003  
				"	  FROM com_documento d,"+
				"	       comrel_pyme_epo pe"+
			        "         , comrel_if_epo ie2  "+//2017_003 
				"	 WHERE d.ic_pyme = pe.ic_pyme"+
				"	   AND d.ic_epo = pe.ic_epo"+
				"	   AND d.ic_epo = "+ic_epo+
				"	   AND d.ic_estatus_docto = 9"+
				"    AND d.df_alta > TRUNC(SYSDATE-1)"+
				"    AND d.ic_epo = ie2.ic_epo (+)"+ //2017_003 
				"    AND d.ic_beneficiario = ie2.ic_if (+)";//2017_003 

		return qrySentencia;
	}
 
	
	/**
	 * Se sobreescribe el metodo de EpoPEF, debido a que en este enlace es necesario
	 * especificar el dblink ORANFE_ORALINK en la funci�n para que pueda
	 * funcionar adecuadamente.
	 * @return Cadena con la secci�n del query que determina el digito identificador.
	 */
	public String getCamposDigitoIdentificadorCalc(){
	  return "  rellenaCeros@ORANFE_ORALINK(d.ic_epo||'','0000') || rellenaCeros@ORANFE_ORALINK(d.ic_documento||'','00000000000') as " + getCamposDigitoIdentificador();  
	}

}
