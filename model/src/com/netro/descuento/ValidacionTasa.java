package com.netro.descuento;

import javax.ejb.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import java.io.*;

public class ValidacionTasa implements Serializable{

	private static final long serialVersionUID = 012010L;
	
	private 	StringBuffer 	bufferRfc				= null;
	private 	StringBuffer 	puntosValorTasa		= null;
	private 	StringBuffer 	estatus					= null;
	private 	StringBuffer 	bufferErrorGenerico	= null;
	private 	boolean			errorRFC 				= false;
	private 	boolean			errorPuntosValorTasa = false;
	private 	boolean			errorEstatus 			= false;
	private 	boolean			errorGenerico 			= false;
	private 	int 				numeroLinea				= -1;
	private  String 			rfc						= null;
	private 	boolean			errorTipoTasa 				= false;
	private  StringBuilder 	tipoTasa						= null;
	
	public ValidacionTasa(String rfc,int numeroLinea){
		this.rfc 			= rfc;
		this.numeroLinea 	= numeroLinea;
	}
	
	public String getRFC(){
		return this.rfc;
	}
	
	public int getNumeroLinea(){
		return this.numeroLinea;
	}
	
	public void appendError(String campo,String descripcion){
		if("1".equals(campo)){
				errorRFC = true;
				if(bufferRfc == null) bufferRfc = new StringBuffer();
				bufferRfc.append(descripcion);
		}else if("2".equals(campo)){
				errorPuntosValorTasa = true;
				if(puntosValorTasa == null) puntosValorTasa = new StringBuffer();
				puntosValorTasa.append(descripcion);
		}else if("3".equals(campo)){
				errorEstatus = true;
				if(estatus == null) estatus = new StringBuffer();
				estatus.append(descripcion);
		}else if("4".equals(campo)){
				errorTipoTasa = true;
				if(tipoTasa == null){
					tipoTasa = new StringBuilder();
				}
				tipoTasa.append(descripcion);
		}else{ // Error Generico
			errorGenerico = true;
			if(bufferErrorGenerico == null) bufferErrorGenerico = new StringBuffer();
				bufferErrorGenerico.append(descripcion);
		}
	}
	
	public boolean hayError(){
		return (this.errorRFC || this.errorPuntosValorTasa || this.errorEstatus || this.errorTipoTasa || this.errorGenerico);	
	}
	
	public boolean hayErrorRFC(){
		return this.errorRFC;
	}
	public boolean hayErrorPuntosValorTasa(){
		return this.errorPuntosValorTasa;
	}
	public boolean hayErrorEstatus(){
		return this.errorEstatus;
	}
	public boolean hayErrorGenerico(){
		return this.errorGenerico;
	}
	
	
	public String getErroresRfc(){	
		if (bufferRfc == null) return "";
		return bufferRfc.toString();
	}		
	public String getErroresPuntosValorTasa(){
		if (puntosValorTasa == null) return "";
		return puntosValorTasa.toString();
	}
	public String getErroresEstatus(){
		if (estatus == null) return "";
		return estatus.toString();
	}
	public String getErrorGenerico(){
		if (bufferErrorGenerico == null) return "";
		return bufferErrorGenerico.toString();
	}
	/**
	 * error tipo tasa
	 * @return
	 */
	public boolean hayErrorTipoTasa() {
		return errorTipoTasa;
	}
	/**
	 * error tipo tasa
	 * @return
	 */
	public String getErrorTipoTasa(){
		if (tipoTasa == null) {
			return "";
		}
		return tipoTasa.toString();
	}
 
}
