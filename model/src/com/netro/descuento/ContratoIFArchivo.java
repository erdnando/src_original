package com.netro.descuento;

import java.io.*;
import java.util.*;
import java.sql.*;
import netropology.utilerias.*;
import com.netro.exception.*;


/**
 * Esta clase representa proporciona mecanismos para manipular
 * el archivo del contrato
 * @author Alberto Cruz
 * @since F012-2008
 *
 */
public class ContratoIFArchivo implements Serializable {
	private String claveIF;
	private String consecutivo;
	private String extension;

	public ContratoIFArchivo(){};


	/**
	 * Establece la clave de la if
	 * @param claveIF Clave de la if
	 *
	 */
	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	/**
	 * Obtiene la clave de la if
	 * @return Clave de la if
	 *
	 */
	public String getClaveIF() {
		return this.claveIF;
	}

	/**
	 * Establece el consecutivo
	 * @param consecutivo Consecutivo
	 *
	 */
	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	/**
	 * Obtiene el consecutivo
	 * @return Consecutivo
	 *
	 */
	public String getConsecutivo() {
		return this.consecutivo;
	}

	/**
	 * Establece la extensi�n del archivo
	 * @param extension Extension del archivo
	 *
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Obtiene la extensi�n del archivo
	 * @return extension
	 *
	 */
	public String getExtension() {
		return this.extension;
	}

	/**
	 * Genera el archivo del contrato a partir del contenido en BD.
	 * El nombre del archivo estar� conformado por la clave de la if, la
	 * el consecutivo y la extensi�n que corresponda. (Es decir llave primaria + extension)
	 * Por ejemplo 126_3.ppt  que corresponde a la IF 126, el contrato numero 3
	 * La clave de la if y el consecutivo deben ser establecidos previamente.
	 *
	 * Adicionalmente establece la propiedad extension, con la extension
	 * correspondiente al archivo guardado en BD
	 *
	 * @param rutaDestino Ruta destino donde se generara el archivo debe terminar con una diagonal /
	 *
	 * @return Cadena con el nombre del archivo creado
	 */

	public String getArchivoContratoIF(String rutaDestino)
			throws NafinException {

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] arrByte = new byte[4096];
		java.io.OutputStream outstream = null;

		InputStream inStream = null;
		int ic_if = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveIF == null || this.consecutivo == null ||
					rutaDestino == null) {
				throw new Exception("Los parametros o atributos no pueden estar vacios");
			}

			if (!rutaDestino.endsWith("/")){
				throw new Exception("La ruta destino debe terminar en diagonal /");
			}
			ic_if = Integer.parseInt(this.claveIF);
			ic_consecutivo = Long.parseLong(this.consecutivo);
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveIF=" + this.claveIF + "\n" +
					" consecutivo=" + this.consecutivo + "\n" +
					" rutaDestino=" + rutaDestino);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = "SELECT bi_documento, cg_ext_documento " +
					" FROM com_contrato_if " +
					" WHERE ic_if = ? " +
					" AND ic_consecutivo = ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setLong(2, ic_consecutivo);
			rs = ps.executeQuery();

			rs.next();

			this.extension = rs.getString("cg_ext_documento");

			inStream = rs.getBinaryStream("bi_documento");

			String nombreArchivo = claveIF + "_" + consecutivo + "." + extension;
			String rutaArchivo = rutaDestino + nombreArchivo;

			outstream = new BufferedOutputStream(new FileOutputStream(rutaArchivo));

			int i = 0;
			while((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
				outstream.write(arrByte, 0, i);
			}
			outstream.close();
			inStream.close();

			return nombreArchivo;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Guarda en la BD el archivo contiene el contrato de la IF especificada
	 * El commit es responsabilidad del que manda a llamar al m�todo
	 * @param rutaArchivoOrigen Ruta fisica del archivo origen contiene el contrato
	 * @param con Conexion de BD
	 */
	public void guardarArchivoContratoIF(String rutaArchivoOrigen, AccesoDB con) throws NafinException {
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;
		int ic_if = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveIF == null || this.consecutivo == null || rutaArchivoOrigen == null) {
				throw new Exception("La clave y la ruta de archivo no estan establecidos");
			}
			ic_if = Integer.parseInt(this.claveIF);
			ic_consecutivo = Long.parseLong(this.consecutivo);
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveIF=" + this.claveIF + "\n" +
					" consecutivo=" + this.consecutivo + "\n" +
					" rutaArchivoOrigen=" + rutaArchivoOrigen);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {

			this.extension = rutaArchivoOrigen.substring(rutaArchivoOrigen.length()-3).toLowerCase();
			String strSQL = "UPDATE com_contrato_if " +
					" SET cg_ext_documento = ?, bi_documento = ? " +
					" WHERE ic_if = ? and ic_consecutivo = ? ";

			//La extensi�n debe ser PDF, PPT o DOC por lo cual se consideran los
			//ultimos tres caracteres para la extensi�n
			File archivo = new File(rutaArchivoOrigen);
			FileInputStream fileinputstream = new FileInputStream(archivo);

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, this.extension);
			ps.setBinaryStream(2, fileinputstream, (int)archivo.length());
			ps.setInt(3, ic_if);
			ps.setLong(4, ic_consecutivo);

			ps.executeUpdate();
			ps.close();

			fileinputstream.close();
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
		}
	}



	public String toString() {
		return "claveIF=" + this.claveIF + "," + "\n" +
				"consecutivo=" + this.consecutivo + "," + "\n" +
				"extension=" + this.extension;
	}
	
	public String getClaveIfRelacion(String iNoCliente){
		List clavesIF = new ArrayList();
		int contratoP = 0;
		int contratoA = 0;
		String clave = "";
		StringBuffer strSQL;
		AccesoDB con = new AccesoDB();
		
		try {
			con.conexionDB();
		
			//-->Determina las claves de IF que tienen relacion con la pyme
			strSQL = new StringBuffer();
			strSQL.append(" SELECT DISTINCT cpi.ic_if"+
							  " FROM comrel_pyme_if cpi,"+
							  "      comrel_cuenta_bancaria ccb"+
							  " WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
							  "   AND cpi.cs_vobo_if = ?"+
							  "   AND cpi.cs_borrado = ?"+
							  "   AND ccb.cs_borrado = ?   "+
							  "   AND ccb.ic_pyme = ?");
		 
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, "S");
			ps.setString(2, "N");
			ps.setString(3, "N");
			ps.setInt(4, Integer.parseInt(iNoCliente));
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				clavesIF.add(rs.getString(1));
			}
			
			if(clavesIF.size() > 0){
				Iterator itClaveIf = clavesIF.iterator();
			
				while(itClaveIf.hasNext()){
				  String clvIF = (String)itClaveIf.next();
				//-->Determina si la IF tiene un contrato parametrizado
					strSQL = new StringBuffer();
					strSQL.append( " SELECT count(*) as numContratos " +
									" FROM com_contrato_if " +
									" WHERE cs_mostrar = 'S' " +
									" AND ic_if = ? ");
					 
					ps = con.queryPrecompilado(strSQL.toString());
					ps.setInt(1, Integer.parseInt(clvIF));
					
					rs = ps.executeQuery();
					
					rs.next();
					contratoP = rs.getInt("numContratos");
					
					if(contratoP == 1){
						strSQL = new StringBuffer();
						strSQL.append(
							" SELECT count(*) as contratoAceptado"+
							" FROM com_aceptacion_contrato_if"+
							" WHERE ic_if = ?"+
							" AND ic_consecutivo = ("+
							"        SELECT MAX(ic_consecutivo)"+
							"        FROM com_contrato_if"+
							"        WHERE ic_if = ?"+
							" ) "+
							" AND ic_pyme = ?");
					
						ps = con.queryPrecompilado(strSQL.toString());
						ps.setInt(1, Integer.parseInt(clvIF));
						ps.setInt(2, Integer.parseInt(clvIF));
						ps.setInt(3, Integer.parseInt(iNoCliente));
						
						rs = ps.executeQuery();
						
						rs.next();
						contratoA = rs.getInt("contratoAceptado");
						
						if(contratoA == 0){
							clave = clvIF;
							break;          
						}
					}
				}
			}
		} catch(Exception e) {
			throw new AppException("Ocurrio un error al obtener la claveIF y su relacion con Pyme", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return clave;
	}

}