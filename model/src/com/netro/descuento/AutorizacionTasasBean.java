package com.netro.descuento;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "AutorizacionTasasEJB" , mappedName = "AutorizacionTasasEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutorizacionTasasBean implements AutorizacionTasas {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AutorizacionTasasBean.class);

	public void actualizarTipificacion (String fechaTasaTipificacion,
			String relMatTipificacion, String puntosTipificacion,
			String claveCalificacion, String voboNAFIN)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			qrySentencia = " UPDATE COMREL_TIPIFICACION SET CG_REL_MAT='"+relMatTipificacion+"'," +
			" CG_PUNTOS="+puntosTipificacion+", CS_VOBO_NAFIN='"+voboNAFIN+"'" +
			" WHERE DC_FECHA_TASA = TO_DATE('"+fechaTasaTipificacion+"','DD/MM/YYYY HH24:MI:SS')" +
			" AND IC_CALIFICACION=" + claveCalificacion;

			System.out.println(qrySentencia);

			con.ejecutaSQL(qrySentencia);

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en actualizarTipificacion: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


	public void actualizarTasaBase (String claveEPO, String relTasaBase,
			String puntosTasaBase, String fechaTasaBase)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			qrySentencia = " UPDATE COMREL_TASA_BASE SET CG_REL_MAT='" +relTasaBase+ "'," +
			" FN_PUNTOS=" +puntosTasaBase+", CS_VOBO_NAFIN='N'" +
			" WHERE DC_FECHA_TASA = TO_DATE('" + fechaTasaBase + "','DD/MM/YYYY HH24:MI:SS')" +
			" AND IC_EPO=" + claveEPO;

			System.out.println(qrySentencia);

			con.ejecutaSQL(qrySentencia);

		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en actualizarTasaBase: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


	public void autorizarTasasEPO (String claveIF,
			String claveEPO, String[] fechasTasa)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			for(int i=0; i < fechasTasa.length; i++) {
				String fechaTasa = fechasTasa[i];

				qrySentencia = "UPDATE comrel_tasa_autorizada set CS_VOBO_IF='S'" +
					" WHERE IC_IF= " + claveIF +
					" AND IC_EPO=" + claveEPO +
					" AND dc_fecha_tasa = TO_DATE('" + fechaTasa + "','DD/MM/YYYY HH24:MI:SS')";

				System.out.println(qrySentencia);

				con.ejecutaSQL(qrySentencia);
			}	//for i
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en autorizarTasaEPO: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


	public void autorizarTasasEPOIF (String claveEPO, Vector vDatosTasas)
		throws NafinException {

		AccesoDB con = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			for(int i=0; i < vDatosTasas.size(); i++) {
				String datosTasa = vDatosTasas.get(i).toString();

				Vector vDatosTasa = Comunes.explode("|", datosTasa);
				String fechaTasa = (String)vDatosTasa.get(0);
				String claveIF = (String)vDatosTasa.get(1);

				qrySentencia = "update comrel_tasa_autorizada set CS_VOBO_NAFIN='S' " +
					" where IC_EPO=" + claveEPO + " and IC_IF=" + claveIF +
					" and DC_FECHA_TASA = TO_DATE('" + fechaTasa + "','DD/MM/YYYY HH24:MI:SS')";

				con.ejecutaSQL(qrySentencia);
			}	//for i
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en autorizarTasasEPOIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


		public void autorizarTodasLasTasas (String claveEPO, String[] arrFechasTasasBase,
			String[] arrDatosTasasTipificadas, String[] arrDatosTasasAutorizadasIF)
		throws NafinException {

		AccesoDB con = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;
		int numRegistrosTasaBase = (arrFechasTasasBase == null)?0:arrFechasTasasBase.length;
		int numRegistrosTasaTipificada = (arrDatosTasasTipificadas == null)?0:arrDatosTasasTipificadas.length;
		int numRegistrosTasaAutorizadaIF = (arrDatosTasasAutorizadasIF == null)?0:arrDatosTasasAutorizadasIF.length;


		try {
			con.conexionDB();

			for(int i=0; i < numRegistrosTasaBase; i++) {
				String fechaTasa = arrFechasTasasBase[i];

				qrySentencia = "update comrel_tasa_base set CS_VOBO_NAFIN='S' "+
							" where IC_EPO="+claveEPO+
							" and DC_FECHA_TASA = TO_DATE('"+fechaTasa+"','DD/MM/YYYY HH24:MI:SS')";

				con.ejecutaSQL(qrySentencia);
			}	//for i

			for(int i = 0; i < numRegistrosTasaTipificada; i++) {
				String datosTasaTipificada = arrDatosTasasTipificadas[i];

				Vector vDatosTasaTipificada = Comunes.explode("|", datosTasaTipificada);
				String claveCalificacion = vDatosTasaTipificada.get(0).toString();
				String fechaTasaTipificada = vDatosTasaTipificada.get(1).toString();

				qrySentencia = "update comrel_tipificacion set CS_VOBO_NAFIN='S' "+
					" where IC_CALIFICACION=" + claveCalificacion +
					" and DC_FECHA_TASA = TO_DATE('" + fechaTasaTipificada + "','DD/MM/YYYY HH24:MI:SS')";
				con.ejecutaSQL(qrySentencia);
			} //for

			for(int i = 0; i < numRegistrosTasaAutorizadaIF; i++) {
				String datosTasaAutorizadaIF = arrDatosTasasAutorizadasIF[i];

				Vector vDatosTasaAutorizadaIF = Comunes.explode("|", datosTasaAutorizadaIF);
				String fechaTasaAutorizada = vDatosTasaAutorizadaIF.get(0).toString();
				String claveIF = vDatosTasaAutorizadaIF.get(1).toString();

				qrySentencia = "update comrel_tasa_autorizada set CS_VOBO_NAFIN='S' " +
						" where IC_EPO=" + claveEPO + " and IC_IF=" + claveIF +
						" and DC_FECHA_TASA = TO_DATE('" + fechaTasaAutorizada + "','DD/MM/YYYY HH24:MI:SS')";
				con.ejecutaSQL(qrySentencia);
			}
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en autorizarTodasTasas: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}

	public boolean esFactorajeVencido(String claveEpo)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		try {
			con.conexionDB();

			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_vencido = 'S'"   +
				"    AND ic_epo = " + claveEpo;

			rs = con.queryDB(qrySentencia);
			rs.next();

			if(rs.getInt(1) > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getTasaBase (String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia =
				" SELECT DISTINCT t.cd_nombre AS nombretasa, mt.fn_valor, tb.cg_rel_mat, tb.fn_puntos, tb.cs_vobo_nafin,"   +
				"                 TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS fechatasa, tb.ic_tasa, m.cd_nombre AS nombremoneda,"   +
				"                 p.cg_descripcion AS rangoplazo"   +
				"   FROM comcat_tasa t, comrel_tasa_base tb, com_mant_tasa mt,"   +
				"        (SELECT ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"           FROM com_mant_tasa"   +
				"          GROUP BY ic_tasa) x, comcat_moneda m, comrel_if_epo_moneda iem, comcat_plazo p,"   +
				"        (SELECT tb2.ic_epo, tb2.ic_tasa, MAX (tb2.dc_fecha_tasa) AS dc_fecha_tasa"   +
				"           FROM comrel_tasa_base tb2"   +
				"          WHERE tb2.ic_epo = " +claveEPO+
				"          GROUP BY tb2.ic_epo,  tb2.ic_tasa) tbu"   +
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND mt.dc_fecha = x.dc_fecha"   +
				"    AND t.ic_tasa = mt.ic_tasa"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND t.ic_tasa = tb.ic_tasa"   +
				"    AND tb.ic_epo = tbu.ic_epo"   +
				"    AND tb.ic_tasa = tbu.ic_tasa"   +
				"    AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"    AND iem.ic_moneda = m.ic_moneda"   +
				"    AND iem.ic_epo = tb.ic_epo"   +
				"    AND tb.ic_epo = " +claveEPO+
				"    AND tb.CS_TASA_ACTIVA = 'S'"+
				"    AND t.cs_disponible = 'S'"   +
				"    AND tb.ic_plazo = p.ic_plazo"   +
				"  ORDER BY fechatasa"  ;


			/*"SELECT distinct t.cd_nombre as nombreTasa, mt.fn_valor"+
				" , tb.cg_rel_mat, tb.fn_puntos, tb.cs_vobo_nafin"+
				" , TO_CHAR(tb.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS') as fechaTasa"+
				" , tb.ic_tasa, m.cd_nombre as nombreMoneda, p.cg_descripcion as rangoPlazo "+
				" FROM COMCAT_TASA t, COMREL_TASA_BASE tb, com_mant_tasa mt, comrel_tasa_producto tp "+
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , COMCAT_MONEDA m, comrel_if_epo_moneda iem, comcat_plazo p "+
				" , (SELECT tb2.ic_epo, tb2.ic_tasa, MAX(tb2.dc_fecha_tasa) as dc_fecha_tasa"+
				" 	FROM comrel_tasa_base tb2 WHERE tb2.ic_epo = "+ claveEPO +
				" 	GROUP BY tb2.ic_epo, tb2.ic_tasa) tbu"+
				" WHERE t.ic_tasa = x.ic_tasa "+
				" AND mt.dc_fecha = x.dc_fecha "+
				" AND t.ic_tasa = mt.ic_tasa "+
				" AND t.ic_moneda=m.ic_moneda "+
				" AND t.ic_tasa = tb.ic_tasa "+
				" AND tb.ic_epo = tbu.ic_epo "+
				" AND tb.ic_tasa = tbu.ic_tasa "+
				" AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa "+
				" AND iem.ic_moneda = m.ic_moneda "+
				" AND iem.ic_epo = tb.ic_epo "+
				" AND t.ic_plazo = p.ic_plazo "+
				" AND t.ic_tasa = tp.ic_tasa "+
				" AND tp.ic_producto_nafin = 1 "+
				" AND tb.ic_epo = " +claveEPO+
				" AND t.cs_disponible='S' "+
				" ORDER BY fechaTasa ";*/
			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(9);
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("FN_VALOR") == null)?"":rs.getString("FN_VALOR") );
				registro.add(2, (rs.getString("CG_REL_MAT") == null)?"":rs.getString("CG_REL_MAT") );
				registro.add(3, (rs.getString("FN_PUNTOS") == null)?"":rs.getString("FN_PUNTOS") );
				registro.add(4, (rs.getString("CS_VOBO_NAFIN") == null)?"":rs.getString("CS_VOBO_NAFIN") );
				registro.add(5, (rs.getString("FECHATASA") == null)?"":rs.getString("FECHATASA") );
				registro.add(6, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );
				registro.add(7, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(8, (rs.getString("RANGOPLAZO") == null)?"":rs.getString("RANGOPLAZO") );
				registros.add(registro);
			}
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("Error en getTasaBase: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getTasaBase (String claveEPO, String claveIF, String voboIF)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		int numRegistros = 0;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = " SELECT T.cd_nombre as nombreTasa"+
				" , E.cg_razon_social as nombreEPO, TB.cg_rel_mat, TB.fn_puntos"+
				" , TO_CHAR(TA.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS') fechaTasa"+
				" , TA.cs_vobo_epo, TA.cs_vobo_if, TA.cs_vobo_nafin, TA.ic_epo"+
				" , TO_CHAR(TA.dc_fecha_tasa,'DD/MM/YYYY') fechaTasa2, M.cd_nombre as nombreMoneda"+
				" , mt.fn_valor, t.ic_tasa "+
				" FROM comrel_tasa_autorizada TA, comrel_tasa_base TB, comcat_tasa T, comrel_if_epo_moneda iem"+
				" , comcat_epo E, comcat_moneda M"+
				" , (select max(TB2.dc_fecha_tasa)as dc_fecha_tasa, TB2.ic_epo, ic_tasa "+
				" 		FROM COMREL_TASA_BASE TB2"+
				"	 		, COMREL_TASA_AUTORIZADA TA2"+
				"	 		where TB2.ic_epo = " + claveEPO + "	AND TB2.dc_fecha_tasa = TA2.dc_fecha_tasa"+
				"	 		AND TA2.ic_if = " + claveIF + " group by TB2.ic_epo, TB2.ic_tasa) TU"+
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , com_mant_tasa mt "+
				" WHERE TB.dc_fecha_tasa = TA.dc_fecha_tasa and TB.ic_epo = "+ claveEPO +
				" AND TB.ic_epo = TA.ic_epo and TA.ic_epo = " + claveEPO + " and TA.ic_if = " + claveIF +
				" AND TB.ic_tasa = T.ic_tasa and TB.ic_epo = E.ic_epo "+
				" AND T.ic_moneda = M.ic_moneda"+
				" AND t.cs_disponible = 'S'"+
				" AND TB.dc_fecha_tasa = TU.dc_fecha_tasa "+
				" AND TB.ic_epo = TU.ic_epo AND TB.ic_tasa = TU.ic_tasa "+
				" AND T.ic_tasa = x.ic_tasa AND mt.dc_fecha = x.dc_fecha"+
				" AND mt.ic_tasa = t.ic_tasa"+
				" AND iem.ic_epo = tb.ic_epo"+
				" AND iem.ic_moneda = m.ic_moneda"+
				" AND iem.ic_if = ta.ic_if"+
				" AND TA.cs_vobo_if = '"+voboIF+"'"+
				" ORDER by TA.dc_fecha_tasa";

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				Vector registro = new Vector(13);

				numRegistros++;
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(2, (rs.getString("CG_REL_MAT") == null)?"":rs.getString("CG_REL_MAT") );
				registro.add(3, (rs.getString("FN_PUNTOS") == null)?"":rs.getString("FN_PUNTOS") );
				registro.add(4, (rs.getString("FECHATASA") == null)?"":rs.getString("FECHATASA") );
				registro.add(5, (rs.getString("CS_VOBO_EPO") == null)?"":rs.getString("CS_VOBO_EPO") );
				registro.add(6, (rs.getString("CS_VOBO_IF") == null)?"":rs.getString("CS_VOBO_IF") );
				registro.add(7, (rs.getString("CS_VOBO_NAFIN") == null)?"":rs.getString("CS_VOBO_NAFIN") );
				registro.add(8, (rs.getString("IC_EPO") == null)?"":rs.getString("IC_EPO") );
				registro.add(9, (rs.getString("FECHATASA2") == null)?"":rs.getString("FECHATASA2") );
				registro.add(10, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(11, (rs.getString("FN_VALOR") == null)?"":rs.getString("FN_VALOR") );
				registro.add(12, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			if (numRegistros == 0) {
				throw new NafinException("DSCT0029");	//No hay tasas pendientes por autorizar
			}

			return registros;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en getTasaBase: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getTasaTipificada (String claveEPO, String [] fechaTasa)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();
		String fechas = "";

		try {
			con.conexionDB();

			for (int i=0; i < fechaTasa.length; i++) {
				if (i == 0) {
					fechas = "'" +fechaTasa[i]+ "'";
				} else {
					fechas += ",'" +fechaTasa[i]+ "'";
				}
			}

			qrySentencia = "SELECT "+
				" TO_CHAR(t.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS') as df_fecha_tasa"+
				" , ce.cd_descripcion as CALIFDESC" +
				" , t.cg_rel_mat, t.cg_puntos, t.cs_vobo_nafin"+
				" , c.ic_calificacion " +
				" FROM COMCAT_CALIFICACION c, COMREL_TIPIFICACION t"+
				" , COMREL_CALIF_EPO ce " +
				" WHERE c.ic_calificacion=t.ic_calificacion " +
				" AND ce.ic_calificacion = t.ic_calificacion"+
				" AND ce.ic_epo = " + claveEPO +
				" AND c.ic_calificacion > 0	" +
				" AND TO_CHAR(t.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS') in (" + fechas + ")"+
				" ORDER BY t.dc_fecha_tasa";
			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(6);
				registro.add(0, (rs.getString("DF_FECHA_TASA") == null)?"":rs.getString("DF_FECHA_TASA") );
				registro.add(1, (rs.getString("CALIFDESC") == null)?"":rs.getString("CALIFDESC") );
				registro.add(2, (rs.getString("CG_REL_MAT") == null)?"":rs.getString("CG_REL_MAT") );
				registro.add(3, (rs.getString("CG_PUNTOS") == null)?"":rs.getString("CG_PUNTOS") );
				registro.add(4, (rs.getString("CS_VOBO_NAFIN") == null)?"":rs.getString("CS_VOBO_NAFIN") );
				registro.add(5, (rs.getString("IC_CALIFICACION") == null)?"":rs.getString("IC_CALIFICACION") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasaTipificada: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getTasaAutIF (String claveEPO, String claveIF, String cveMoneda, String operaFideicomico)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia =
				" SELECT (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social"   +
				"        ) AS nombreif,"   +
				"        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin,"   +
				"        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS dc_fecha_tasa,"   +
				"        t.cd_nombre AS nombretasa, m.cd_nombre AS nombremoneda, i.ic_if"   +
				"		 ,p.cg_descripcion AS rangoplazo, ta.cg_rel_mat as relmat, ta.fn_puntos as ptos, mt.fn_valor as valor" +
				"		 , TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS fechatasa" +
				"		 , p.ic_plazo, p.in_plazo_inicio as plazo_dias_inicio, p.in_plazo_dias as plazo_dias_fin, ta.in_plazo_especifico as plazo_especifico "  + // F031-2013. JSHD
				"   FROM comcat_if i,"   +
				"        comrel_tasa_autorizada ta,"   +
				"        comcat_tasa t,"   +
				"        comcat_moneda m,"   +
				"        comrel_if_epo_moneda iem,"   +
				"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"             FROM com_mant_tasa"   +
				"         GROUP BY ic_tasa) x,"   +
				"        (SELECT   tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa"   +
				"             FROM comrel_tasa_base tb"   +
				"            WHERE tb.ic_epo = " + claveEPO+
				"			  AND tb.CS_TASA_ACTIVA = 'S'"+
				"         GROUP BY tb.ic_epo, tb.ic_tasa) tbu"   +
				"		 ,comrel_tasa_base tb,  comcat_plazo p, com_mant_tasa mt"+
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"   +
				"    AND ta.ic_epo = " + claveEPO+
				"    AND ta.ic_if = i.ic_if"   +
				"    AND ta.ic_epo = tbu.ic_epo"   +
				"    AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"    AND tbu.ic_tasa = t.ic_tasa"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND iem.ic_epo = ta.ic_epo"   +
				"    AND iem.ic_if = ta.ic_if"   +
				"    AND iem.ic_moneda = m.ic_moneda"   +
				"    AND i.cs_habilitado = 'S'" ;
				if(!claveIF.equals("")){
					qrySentencia += "    AND ta.ic_if in( "+claveIF+")";
				}
				
				qrySentencia += "    AND M.IC_MONEDA = "+cveMoneda+
				"	 AND tbu.ic_tasa = tb.ic_tasa"+
    			"	 AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa"+
    			" 	 AND tb.ic_plazo = p.ic_plazo"+
    			"	 AND mt.dc_fecha = x.dc_fecha"+
    			"	 AND t.ic_tasa = mt.ic_tasa";
				
				//Fodea 017-2013 
				if(!operaFideicomico.equals("")){  
					qrySentencia += " and i.CS_OPERA_FIDEICOMISO   = '"+operaFideicomico+"'"+
										"  and ta.CS_VOBO_EPO  = 'S'" +
										"  and ta.CS_VOBO_IF = 'S'  "+
										"  and ta.CS_VOBO_NAFIN  = 'S'";
				}

/*
			"SELECT "+
				" (decode(i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIF"+
				" , ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin"+
				" , TO_CHAR(ta.dc_fecha_tasa,'DD/MM/YYYY HH24:MI:SS') as dc_fecha_tasa"+
				" , t.cd_nombre as nombreTasa, m.cd_nombre as nombreMoneda, i.ic_if"+
				" FROM COMCAT_IF i, COMREL_TASA_AUTORIZADA ta, COMCAT_TASA t, COMCAT_MONEDA m, "+
				" comrel_if_epo_moneda iem, comrel_tasa_producto tp "+
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , (SELECT tb.ic_epo, tb.ic_tasa, MAX(tb.dc_fecha_tasa) as dc_fecha_tasa "+
				" 	FROM comrel_tasa_base tb WHERE tb.ic_epo = " + claveEPO +
				"		GROUP BY tb.ic_epo, tb.ic_tasa) tbu "+
				" WHERE t.ic_tasa = x.ic_tasa AND t.cs_disponible='S' "+
				" AND ta.ic_epo = " + claveEPO+
				" AND ta.ic_if = i.ic_if "+
				" AND ta.ic_epo = tbu.ic_epo "+
				" AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa "+
				" AND tbu.ic_tasa = t.ic_tasa"+
				" AND t.ic_moneda = m.ic_moneda"+
				" AND iem.ic_epo = ta.ic_epo"+
				" AND iem.ic_if = ta.ic_if"+
				" AND iem.ic_moneda = m.ic_moneda"+
				" AND i.cs_habilitado = 'S'"+
				" AND t.ic_tasa = tp.ic_tasa "+
				" AND tp.ic_producto_nafin = 1 "+
				" AND ta.ic_if ="+claveIF;*/
			rs = con.queryDB(qrySentencia);

			System.out.println("qrySentencia"+qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(13);
				registro.add(0, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(1, (rs.getString("CS_VOBO_EPO") == null)?"":rs.getString("CS_VOBO_EPO") );
				registro.add(2, (rs.getString("CS_VOBO_IF") == null)?"":rs.getString("CS_VOBO_IF") );
				registro.add(3, (rs.getString("CS_VOBO_NAFIN") == null)?"":rs.getString("CS_VOBO_NAFIN") );
				registro.add(4, (rs.getString("DC_FECHA_TASA") == null)?"":rs.getString("DC_FECHA_TASA") );
				registro.add(5, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(6, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(7, (rs.getString("IC_IF") == null)?"":rs.getString("IC_IF") );
				registro.add(8, (rs.getString("rangoplazo") == null)?"":rs.getString("rangoplazo") );
				registro.add(9, (rs.getString("relmat") == null)?"":rs.getString("relmat") );
				registro.add(10, (rs.getString("ptos") == null)?"":rs.getString("ptos") );
				registro.add(11, (rs.getString("valor") == null)?"":rs.getString("valor") );
				registro.add(12, (rs.getString("fechatasa") == null)?"":rs.getString("fechatasa") );
				registro.add(13, (rs.getString("ic_plazo") 				== null)?"":rs.getString("ic_plazo") 				); // F031-2013. JSHD
				registro.add(14, (rs.getString("plazo_dias_inicio") 	== null)?"":rs.getString("plazo_dias_inicio")	); // F031-2013. JSHD
				registro.add(15, (rs.getString("plazo_dias_fin") 		== null)?"":rs.getString("plazo_dias_fin") 		); // F031-2013. JSHD
				registro.add(16, (rs.getString("plazo_especifico") 	== null)?"":rs.getString("plazo_especifico") 	); // F031-2013. JSHD

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasaAutIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getTasasDisponibles ()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "SELECT t.cd_nombre as nombreTasa, mt.fn_valor"+
				" , t.ic_tasa, m.cd_nombre as nombreMoneda"+
				" FROM COMCAT_TASA t" +
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , com_mant_tasa mt, COMCAT_MONEDA m, COMREL_TASA_PRODUCTO TP"+
				" WHERE t.ic_tasa = x.ic_tasa AND mt.dc_fecha = x.dc_fecha"+
				" AND t.ic_tasa = mt.ic_tasa"+
				" AND t.cs_disponible='S' AND t.ic_moneda=m.ic_moneda  "+
		  	    " AND t.ic_tasa = TP.ic_tasa " +
				" AND TP.ic_producto_nafin = 1 " ;

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(4);
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("FN_VALOR") == null)?"":rs.getString("FN_VALOR") );
				registro.add(2, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );
				registro.add(3, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasasDisponibles: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getTasasDisponibles (String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "SELECT t.cd_nombre as nombreTasa, mt.fn_valor, t.ic_tasa"+
				" , m.cd_nombre as nombreMoneda, p.cg_descripcion as rangoPlazo, m.ic_moneda as claveMoneda"+
				" FROM COMCAT_TASA t" +
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , com_mant_tasa mt, COMCAT_MONEDA m, COMREL_TASA_PRODUCTO TP, comcat_plazo p"+
				" WHERE t.ic_tasa = x.ic_tasa AND mt.dc_fecha = x.dc_fecha"+
				" AND t.ic_tasa = mt.ic_tasa"+
				" AND t.cs_disponible='S' "+
				" AND t.ic_moneda=m.ic_moneda "+
		  	    " AND t.ic_tasa = TP.ic_tasa " +
				" AND t.ic_plazo = p.ic_plazo "+
				" AND TP.ic_producto_nafin = 1 " +
				" AND t.ic_moneda in ( select distinct rm.ic_moneda from COMREL_IF_EPO_MONEDA rm "+
				"                       where rm.ic_epo = " + claveEPO+
				" 						and   rm.ic_if  in ( select b.ic_if"+
				" 											  FROM COMCAT_IF a, COMREL_IF_EPO b"+
				" 											  WHERE b.ic_epo = " +claveEPO+
				" 											  AND a.ic_if=b.ic_if"+
				" 											  AND b.cs_vobo_nafin='S'"+
				" 											  AND a.cs_habilitado='S'"+
				" 											  AND b.ic_if in ( select distinct rm.ic_if from  COMREL_IF_EPO_MONEDA rm where rm.ic_epo = " + claveEPO+" ) "+
														  ") ) ";
			//System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				Vector registro = new Vector(4);
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("FN_VALOR") == null)?"":rs.getString("FN_VALOR") );
				registro.add(2, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );
				registro.add(3, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(4, (rs.getString("RANGOPLAZO") == null)?"":rs.getString("RANGOPLAZO") );
				registro.add(5, (rs.getString("CLAVEMONEDA") == null)?"":rs.getString("CLAVEMONEDA") );
				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasasDisponibles: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getTasasDisponibles (String claveEPO, String claveMoneda)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		String condicion = "";
		ResultSet rs = null;
		Vector registros = new Vector();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comrel_tasa_producto_epo"   +
				"  WHERE ic_epo = " +claveEPO;
			rs = con.queryDB(qrySentencia);
			int existeParam = 0;
			if(rs.next()) {
				existeParam = rs.getInt(1);
			}
			rs.close();
			con.cierraStatement();
			if(existeParam>0) {
				condicion = "    AND tpe.ic_epo = " +claveEPO;
			} else {
				condicion = "    AND tpe.ic_epo is null ";
			}

			qrySentencia =
				" SELECT t.cd_nombre AS nombretasa, "   +
				"        mt.fn_valor, "   +
				"        t.ic_tasa, "   +
				"        m.cd_nombre AS nombremoneda, "   +
				"        p.cg_descripcion AS rangoplazo,"   +
				"		 tpe.ic_plazo " +
				"   FROM comcat_tasa t, "   +
				"        com_mant_tasa mt, "   +
				"        comcat_moneda m, "   +
				"        comrel_tasa_producto_epo tpe,"   +
				"        comcat_plazo p,"   +
				"        (SELECT ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"           FROM com_mant_tasa"   +
				"          GROUP BY ic_tasa) x"   +
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND mt.dc_fecha = x.dc_fecha"   +
				"    AND t.ic_tasa = mt.ic_tasa"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND tpe.ic_tasa = t.ic_tasa"   +
				"    AND tpe.ic_plazo = p.ic_plazo"   +
				"    AND tpe.ic_producto_nafin = 1"   +
				condicion+
				"    AND t.cs_disponible = 'S'"   +
				"    AND m.ic_moneda = "+claveMoneda+
				"    AND t.ic_moneda IN (SELECT DISTINCT rm.ic_moneda"   +
				"                          FROM comrel_if_epo_moneda rm"   +
				"                         WHERE rm.ic_epo = " + claveEPO+
				"                           AND rm.ic_if IN (SELECT b.ic_if"   +
				"                                              FROM comcat_if a, comrel_if_epo b"   +
				"                                             WHERE b.ic_epo = " + claveEPO+
				"                                               AND a.ic_if = b.ic_if"   +
				"                                               AND b.cs_vobo_nafin = 'S'"   +
				"                                               AND a.cs_habilitado = 'S'"   +
				"                                               AND b.ic_if IN (SELECT DISTINCT rm.ic_if"   +
				"                                                                 FROM comrel_if_epo_moneda rm"   +
				"                                                                WHERE rm.ic_epo = "+claveEPO+")))"  ;
/*


				"SELECT t.cd_nombre as nombreTasa, mt.fn_valor, t.ic_tasa"+
				" , m.cd_nombre as nombreMoneda, p.cg_descripcion as rangoPlazo"+
				" FROM COMCAT_TASA t" +
				" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
				" , com_mant_tasa mt, COMCAT_MONEDA m, COMREL_TASA_PRODUCTO TP, comcat_plazo p"+
				" WHERE t.ic_tasa = x.ic_tasa AND mt.dc_fecha = x.dc_fecha"+
				" AND t.ic_tasa = mt.ic_tasa"+
				" AND t.cs_disponible='S' "+
				" AND t.ic_moneda = m.ic_moneda "+
		  	    " AND t.ic_tasa = TP.ic_tasa " +
				" AND t.ic_plazo = p.ic_plazo "+
				" AND TP.ic_producto_nafin = 1 " +
				" AND m.ic_moneda = "+claveMoneda+
				" AND t.ic_moneda in ( select distinct rm.ic_moneda from COMREL_IF_EPO_MONEDA rm "+
				"                       where rm.ic_epo = " + claveEPO+
				" 						and   rm.ic_if  in ( select b.ic_if"+
				" 											  FROM COMCAT_IF a, COMREL_IF_EPO b"+
				" 											  WHERE b.ic_epo = " +claveEPO+
				" 											  AND a.ic_if=b.ic_if"+
				" 											  AND b.cs_vobo_nafin='S'"+
				" 											  AND a.cs_habilitado='S'"+
				" 											  AND b.ic_if in ( select distinct rm.ic_if from  COMREL_IF_EPO_MONEDA rm where rm.ic_epo = " + claveEPO+" ) "+
														  ") ) ";
*/
//System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				Vector registro = new Vector(4);
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("FN_VALOR") == null)?"":rs.getString("FN_VALOR") );
				registro.add(2, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );
				registro.add(3, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(4, (rs.getString("RANGOPLAZO") == null)?"":rs.getString("RANGOPLAZO") );
				registro.add(5, (rs.getString("IC_PLAZO") == null)?"":rs.getString("IC_PLAZO") );
				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasasDisponibles: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Obtiene la clave de la ultima tasa capturada para la epo en el
	 * producto y moneda especificados
	 * @param ic_epo Clave de la epo o la cadena "TODAS" si aplica
	 * 		para la tasa general que aplica a las epos no parametrizadas.
	 * @param ic_producto_nafin Clave del producto
	 * @param ic_moneda Clave de la moneda. Esta clave se requiere dado que
	 * 		se puede tener una ultima tasa capturada para Moneda nacional y
	 * 		otra para Dolares, por ejemplo.
	 * @return Cadena vacia si no existe ninguna clave para la epo en el
	 * 		producto y moneda especificados o la clave de la
	 * 		Tasa x Producto x Epo (comrel_tasa_producto_epo.ic_tasa_producto_epo)
	 * @exception NafinException si ocurre un error durante el
	 * 		proceso de obtenci�n de la clave
	 */
	public String getUltimaClaveTasaProductoEpo(String ic_epo,
			String ic_producto_nafin, String ic_moneda) throws NafinException {
		AccesoDB con = new AccesoDB();
		String claveUltimaTasa = "";

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_epo == null || ic_producto_nafin == null ||
					ic_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			//Integer.parseInt(ic_epo);
			Integer.parseInt(ic_producto_nafin);
			Integer.parseInt(ic_moneda);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" ic_epo=" + ic_epo +
					" ic_producto_nafin=" + ic_producto_nafin +
					" ic_moneda=" + ic_moneda
					);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String condicion = "";
			if (ic_epo.equals("TODAS")) {
				condicion = " AND tpe.ic_epo is null ";
			} else {
				condicion = " AND tpe.ic_epo =" + ic_epo;
			}
			String strSQL = " SELECT MAX(tpe.ic_tasa_producto_epo) as claveUltimaTasa" +
				" FROM comrel_tasa_producto_epo tpe, comcat_moneda m, comcat_tasa t  " +
				" WHERE tpe.ic_tasa = t.ic_tasa  " +
				" 	AND t.ic_moneda = m.ic_moneda  " +
				condicion +
				" 	AND tpe.ic_producto_nafin = " + ic_producto_nafin +
				" 	AND t.ic_moneda = " + ic_moneda;
			ResultSet rs = con.queryDB(strSQL);
			if (rs.next()) {
				claveUltimaTasa = rs.getString("ClaveUltimaTasa");
			} else {
				claveUltimaTasa = "";
			}
			rs.close();
			con.cierraStatement();
			return claveUltimaTasa;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


		public Vector getTasasPorClasificacion (String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia =
				" SELECT t.cd_nombre AS nombretasa, t.ic_tasa, m.cd_nombre AS nombremoneda, cepo.cd_descripcion AS califdesc, cal.ic_calificacion"   +
				"   FROM comcat_tasa t, comcat_moneda m, comcat_calificacion cal, comrel_calif_epo cepo, comrel_tasa_producto_epo tpe"   +
				"  WHERE t.ic_moneda = m.ic_moneda"   +
				"    AND cepo.ic_calificacion = cal.ic_calificacion"   +
				"    AND t.ic_tasa = tpe.ic_tasa"   +
				"    AND tpe.ic_epo = cepo.ic_epo"   +
				"    AND cepo.ic_epo = " + claveEPO +
				"    AND cal.ic_calificacion > 1"   +
				"    AND t.cs_disponible = 'S'"   +
				"    AND tpe.ic_producto_nafin = 1"   +
				"  ORDER BY t.ic_tasa"  ;

/*
				"SELECT t.cd_nombre as nombreTasa, t.ic_tasa"+
				" , m.cd_nombre as nombreMoneda" +
				" , CEPO.cd_descripcion as CALIFDESC, CAL.ic_calificacion " +
				" FROM COMCAT_TASA t, COMCAT_MONEDA m, COMCAT_CALIFICACION CAL"+
				" , COMREL_CALIF_EPO CEPO, COMREL_TASA_PRODUCTO TP " +
				" WHERE t.cs_disponible='S' AND t.ic_moneda=m.ic_moneda " +
				" AND CEPO.ic_calificacion = CAL.ic_calificacion "+
				" AND CEPO.ic_epo = " + claveEPO +
				" AND CAL.ic_calificacion >1 "+
 			    " AND t.ic_tasa = TP.ic_tasa " +
				" AND TP.ic_producto_nafin = 1 " +
				" ORDER BY t.ic_tasa";*/


			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(5);
				registro.add(0, (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.add(1, (rs.getString("IC_TASA") == null)?"":rs.getString("IC_TASA") );
				registro.add(2, (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.add(3, (rs.getString("CALIFDESC") == null)?"":rs.getString("CALIFDESC") );
				registro.add(4, (rs.getString("IC_CALIFICACION") == null)?"":rs.getString("IC_CALIFICACION") );
				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getTasasPorClasificacion: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getIFAsociados (String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "SELECT (decode(a.cs_habilitado,'N','*','S',' ')||' '||a.cg_razon_social) as nombreIF"+
				" , b.ic_if"+
				" FROM COMCAT_IF a, COMREL_IF_EPO b, comrel_producto_if cpi "+
				" WHERE b.ic_epo = " +claveEPO+
				" AND a.ic_if=b.ic_if"+
				" AND b.cs_vobo_nafin='S'"+
				" AND a.cs_habilitado='S'"+
//============================================================================>> FODEA 002 - 2009 (I)
				" AND b.cs_bloqueo = 'N'"+
//============================================================================>> FODEA 002 - 2009 (F);
				" AND cpi.ic_if = a.ic_if " + 
        " AND cpi.ic_producto_nafin = 1 " +
				" AND b.ic_if in (select distinct rm.ic_if from  COMREL_IF_EPO_MONEDA rm where rm.ic_epo = " + claveEPO+" ) ";
			/*	"( "+
				"select distinct ta.ic_if "+
				"from comrel_tasa_autorizada ta, "+ 
				"(SELECT   tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa "+
				"             FROM comrel_tasa_base tb "+
				"            WHERE tb.ic_epo = " +claveEPO+
				"			  AND tb.CS_TASA_ACTIVA = 'S' "+
				"         GROUP BY tb.ic_epo, tb.ic_tasa) tbu "+
				"where ta.ic_if in (select distinct rm.ic_if from  COMREL_IF_EPO_MONEDA rm where rm.ic_epo = " +claveEPO+
				") and ta.dc_fecha_tasa = tbu.dc_fecha_tasa "+
				")";*/

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(2);
				registro.add(0, (rs.getString("IC_IF") == null)?"":rs.getString("IC_IF") );
				registro.add(1, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getIFAsociados: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getIFXRelacionar (String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "SELECT (decode(a.cs_habilitado,'N','*','S',' ')||' '||a.cg_razon_social) as nombreIF"+
						" , b.ic_if" +
						" FROM COMCAT_IF a, COMREL_IF_EPO b" +
						" WHERE b.ic_epo = "+ claveEPO +
						" AND a.ic_if=b.ic_if" +
						" AND b.cs_vobo_nafin='S'" +
						" AND a.cs_habilitado = 'S'"+
						" AND b.ic_if IN ( select distinct rm.ic_if from COMREL_IF_EPO_MONEDA rm where rm.ic_epo = "+ claveEPO +
                						" and rm.ic_if not in ( SELECT i.ic_if " +
															" FROM COMCAT_IF i, COMREL_TASA_AUTORIZADA ta, COMCAT_TASA t, COMCAT_MONEDA m,"+
															" COMREL_IF_EPO_MONEDA iem, comrel_tasa_producto tp"+
															" , (select ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) x"+
															" , (SELECT tb.ic_epo, tb.ic_tasa, MAX(tb.dc_fecha_tasa) as dc_fecha_tasa, cs_tasa_activa" +
															" 	FROM comrel_tasa_base tb WHERE tb.ic_epo = "+ claveEPO +
															" 	GROUP BY tb.ic_epo, tb.ic_tasa, cs_tasa_activa) tbu" +
															"	WHERE t.ic_tasa = x.ic_tasa "+
															"	AND t.cs_disponible='S'"+
															"  	AND ta.ic_epo = "+ claveEPO +
															"	AND ta.ic_if = i.ic_if" +
															" 	AND ta.ic_epo = tbu.ic_epo" +
															" 	AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa" +
															" 	AND tbu.ic_tasa = t.ic_tasa" +
															" 	AND t.ic_moneda = m.ic_moneda"+
															"	AND iem.ic_epo = ta.ic_epo "+
															"	AND iem.ic_if = ta.ic_if "+
															"	AND iem.ic_moneda = m.ic_moneda "+
															"	AND i.cs_habilitado = 'S'"+
															"	AND t.ic_tasa = tp.ic_tasa"+
															"	AND tp.ic_producto_nafin = 1"+
															"	AND tbu.cs_tasa_activa = 'S') )";
			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				Vector registro = new Vector(2);
				registro.add(0, (rs.getString("IC_IF") == null)?"":rs.getString("IC_IF") );
				registro.add(1, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();
			/*if(registros.size() == 0) {
				throw new NafinException("DSCT0027");	//No hay IFs por relacionar a la tasa, o no hay IFs asociadas a esta epo
			}*/

			return registros;
		/*} catch (NafinException ne) {
			throw ne;*/
		} catch (Exception e) {
			System.out.println("Error en getIFXRelacionar: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public void setTasas (String claveEPO, Vector vDatosTasaBase, Vector vDatosTasaIf)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		int numRegistrosTasaBase = vDatosTasaBase.size();
		int numRegistrosSelIF = vDatosTasaIf.size();

		try {
			System.out.println(":::::SET TASAS:::::");
			con.conexionDB();
			
			qrySentencia =
				" UPDATE comrel_tasa_base"   +
				"    SET cs_tasa_activa = 'N'"   +
				"  WHERE cs_tasa_activa = 'S' AND ic_epo = "+ claveEPO +" "  ;
//			System.out.println(qrySentencia);
			con.ejecutaSQL(qrySentencia);
						
			for (int i = 0; i < numRegistrosTasaBase; i++){
				System.out.println("]]]]] TASA BASE [[[[[::: "+i);
				String [] registroTasaBase = (String []) vDatosTasaBase.get(i);

				String claveTasaBase	= registroTasaBase[0];
				String relMatTasaBase	= registroTasaBase[1];
				String puntosTasaBase	= registroTasaBase[2];
				String plazoTasaBase	= registroTasaBase[3];
				
				Thread.sleep(1000);
				String fechaHora = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(new java.util.Date());
				
				qrySentencia = "INSERT INTO COMREL_TASA_BASE (dc_fecha_tasa, ic_epo," +
							" ic_tasa, cg_rel_mat, fn_puntos, cs_vobo_nafin, ic_plazo, cs_tasa_activa)" +
							" VALUES (TO_DATE('"+fechaHora+"','DD/MM/YYYY HH24:MI:SS'), "+ claveEPO +
							"," +claveTasaBase +",'"+ relMatTasaBase +"', "+ puntosTasaBase +",'S', "+plazoTasaBase+", 'S')";
//				System.out.println(qrySentencia);
				con.ejecutaSQL(qrySentencia);

				qrySentencia = "INSERT INTO COMREL_TIPIFICACION (ic_calificacion, " +
								"dc_fecha_tasa, cg_rel_mat, cg_puntos, cs_vobo_nafin) " +
								"VALUES (1, TO_DATE('"+ fechaHora +"','DD/MM/YYYY HH24:MI:SS'), " +
								"'+', 0, 'S')";
//				System.out.println(qrySentencia);
				con.ejecutaSQL(qrySentencia);
				
				for (int j = 0; j < numRegistrosSelIF; j++){
					String [] registroTasaIf = (String []) vDatosTasaIf.get(j);

					String cveIf	= registroTasaIf[0];
					String cvetasa	= registroTasaIf[1];
					String relMat	= registroTasaIf[2];
					String puntos	= registroTasaIf[3];
					
					if(claveTasaBase.equals(cvetasa)){
						
						qrySentencia = "INSERT INTO COMREL_TASA_AUTORIZADA (ic_if, ic_epo,"+
									" dc_fecha_tasa, cs_vobo_epo, cs_vobo_if, cs_vobo_nafin, cg_rel_mat, fn_puntos)"+
									" VALUES (" +cveIf+"," +claveEPO+","+
									" TO_DATE('"+fechaHora+"','DD/MM/YYYY HH24:MI:SS'),"+
									"'S', 'S', 'S','"+relMat+"',"+puntos+")";
						System.out.println(qrySentencia);
	
						// Validaci�n que la moneda exista palomeada en comrel_epo_if_moneda
						boolean palomeada=false;
						String qryTraeMoneda="SELECT e.IC_MONEDA, e.CD_NOMBRE, "+
											" nvl( ( SELECT to_char(i.DF_ALTA,'dd/mm/yyyy') " +
													"FROM COMREL_IF_EPO_MONEDA  i" +
													"	WHERE i.IC_EPO 	= "+claveEPO+
													"	AND   i.IC_IF 	= "+cveIf+
													"	AND   i.IC_MONEDA = e.IC_MONEDA"+
											"      ) , 'no asociada' ) "+
											" FROM COMCAT_MONEDA e, COMCAT_TASA t  "+
											" WHERE e.IC_MONEDA = t.IC_MONEDA "+
											" AND   t.IC_TASA = "+claveTasaBase;
	
						ResultSet rsTraeMoneda = con.queryDB(qryTraeMoneda);
						while(rsTraeMoneda.next()){
							//String sMoneda	= rsTraeMoneda.getString(1);
							String fechaAlta= rsTraeMoneda.getString(3);
							if(!fechaAlta.equals("no asociada"))
								palomeada=true;
						}
						rsTraeMoneda.close();
						con.cierraStatement();
						if (palomeada)
							con.ejecutaSQL(qrySentencia);						
					}					
				}				
			}	
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setTasas: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}

	public void setRelacion(String claveEPO, String claveIF, String[] fechaHoraTasa, List vDatos) throws NafinException {
	AccesoDB con  = new AccesoDB();
	String qrySentencia = null;
	String claveTasaBase	= "";
	String relMatTasaBase	= "";
	String puntosTasaBase	= "";
	boolean hayError = false;
	int numRegistros = vDatos.size();
		try {
			con.conexionDB();
			for (int i = 0; i < fechaHoraTasa.length; i++) {
				String fechaHora = fechaHoraTasa[i];
				//System.out.println("fechaHora:::: "+fechaHora);
				String cvetasa = fechaHora.substring(0,fechaHora.indexOf('|'));
				System.out.println("cvetasa:::: "+cvetasa);
				String fecha = fechaHora.substring(fechaHora.indexOf('|')+1,fechaHora.length());
				System.out.println("fecha:::: "+fecha);
				for (int j = 0; j < numRegistros; j++){
					String [] registroTasaBase = (String []) vDatos.get(j);
					claveTasaBase	= registroTasaBase[0];
					relMatTasaBase	= registroTasaBase[1];
					puntosTasaBase	= registroTasaBase[2];
					System.out.println("claveTasaBase::: "+claveTasaBase);					
					if(cvetasa.equals(claveTasaBase)){	
						System.out.println(" :::::Entro::::: ");					
						qrySentencia = "INSERT INTO COMREL_TASA_AUTORIZADA (ic_if, ic_epo,"+
								" dc_fecha_tasa, cs_vobo_epo, cs_vobo_if, cs_vobo_nafin, cg_rel_mat, fn_puntos)"+
								" VALUES ("+ claveIF +","+ claveEPO +","+
								" TO_DATE('"+fecha+"','DD/MM/YYYY HH24:MI:SS'),"+
								"'S', 'S', 'S','"+relMatTasaBase+"',"+puntosTasaBase+")";
					
						System.out.println(qrySentencia);
						con.ejecutaSQL(qrySentencia);
					}										
				}				
			}
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setRelacion: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * @deprecated
	 * Nota: Este m�todo ha sido deprecado, se deja para que la versi�n no migrada pueda seguir funcionando.
	 * @modifico jshernandez 
	 * @modificationDate 22/09/2014 06:14:22 p.m.
	 * @fodea F031 - 2014
	 */
	public void setEstatusTasaAutorizadaIF (String claveIF,
			String claveEPO, String fechaTasa, String voboEPO,
			String voboIF, String voboNAFIN, String relmat_tasa, String puntos)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			qrySentencia = "UPDATE COMREL_TASA_AUTORIZADA SET CS_VOBO_EPO='"+voboEPO+"', "+
				" CS_VOBO_IF='"+voboIF+"', CS_VOBO_NAFIN='"+voboNAFIN+"' "+
				" , CG_REL_MAT='"+relmat_tasa+"', FN_PUNTOS="+puntos+
				" WHERE DC_FECHA_TASA = TO_DATE('"+fechaTasa+"','DD/MM/YYYY HH24:MI:SS') "+
				" AND IC_EPO="+claveEPO+" AND IC_IF="+claveIF;

			con.ejecutaSQL(qrySentencia);
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setEstatusTasaAutorizadaIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Modifica par�metros de las tasa autorizada. Versi�n Migrada del m�todo setEstatusTasaAutorizadaIF sin el
	 * par�metro String plazoEspecifico.
	 * @throws NafinException 
	 *
	 * @since F031 - 2014
	 * @date 22/09/2014 06:21:59 p.m.
	 * @author jshernandez
	 */
	public void setEstatusTasaAutorizadaIF (String claveIF,
			String claveEPO, String fechaTasa, String voboEPO,
			String voboIF, String voboNAFIN, String relmat_tasa, String puntos,
			String plazoEspecifico)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		try {
			con.conexionDB();

			qrySentencia = "UPDATE COMREL_TASA_AUTORIZADA SET CS_VOBO_EPO='"+voboEPO+"', "+
				" CS_VOBO_IF='"+voboIF+"', CS_VOBO_NAFIN='"+voboNAFIN+"' "+
				" , CG_REL_MAT='"+relmat_tasa+"', FN_PUNTOS="+puntos+
				" , IN_PLAZO_ESPECIFICO = " + (plazoEspecifico == null || plazoEspecifico.matches("^\\s*$")?"NULL":plazoEspecifico) + " " +
				" WHERE DC_FECHA_TASA = TO_DATE('"+fechaTasa+"','DD/MM/YYYY HH24:MI:SS') "+
				" AND IC_EPO="+claveEPO+" AND IC_IF="+claveIF;

			con.ejecutaSQL(qrySentencia);
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setEstatusTasaAutorizadaIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}
	
	public void setEstatusTasasBase (String claveEPO, Vector vDatosTasasBase)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		int numRegistrosTasaBase = vDatosTasasBase.size();

		try {
			con.conexionDB();

			for (int i = 0; i < numRegistrosTasaBase; i++) {
				String tasaBase = vDatosTasasBase.get(i).toString();	//tasaBase contiene fechaTasa y estatus

				Vector vTasaBase = Comunes.explode("|", tasaBase);
				String fechaTasa = vTasaBase.get(0).toString();
				String estatus = vTasaBase.get(1).toString();

				qrySentencia = "update comrel_tasa_base set CS_VOBO_NAFIN = '"+estatus+"'"+
					" where IC_EPO = "+claveEPO+
					" and DC_FECHA_TASA = TO_DATE('"+fechaTasa+"','DD/MM/YYYY HH24:MI:SS')";
				con.ejecutaSQL(qrySentencia);
			}//for i
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setEstatusTasasBase: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


	public void setEstatusTasasTipificadas (Vector vDatosTasasTipificadas)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		boolean hayError = false;

		int numRegistrosTasaTipificada = vDatosTasasTipificadas.size();

		try {
			con.conexionDB();

			for (int i = 0; i < numRegistrosTasaTipificada; i++) {
				String tasaTipificada = vDatosTasasTipificadas.get(i).toString();	//tasaTipificada contiene fechaTasaTipificada, claveCalificacion y estatus

				Vector vTasaBase = Comunes.explode("|", tasaTipificada);
				String claveCalificacion = vTasaBase.get(0).toString();
				String fechaTasaTipificada = vTasaBase.get(1).toString();
				String estatus = vTasaBase.get(2).toString();

				qrySentencia = "update comrel_tipificacion set CS_VOBO_NAFIN='"+estatus+"'"+
						" where IC_CALIFICACION="+claveCalificacion+
						" and DC_FECHA_TASA = TO_DATE('"+fechaTasaTipificada+"','DD/MM/YYYY HH24:MI:SS')";

				con.ejecutaSQL(qrySentencia);
			}//for i
		} catch (Exception e) {
			hayError = true;
			System.out.println("Error en setEstatusTasasTipificadas: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if ( hayError) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}


	/**
	 *	    
	 *	    Vector getProveedor()
	 *	    
	 *	    @throws com.netro.exception.NafinException
	 *	    @return 
	 *	    @param esCadenaEPO
	 *	    @param esCveMoneda
	 *	    @param esCveIf
	 */
    // Agregado 28/10/2002	--CARP
    public Vector getProveedor(String esCveIf, String esCveMoneda, String esCadenaEPO)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
   		AccesoDB con  = new AccesoDB();

        String lsClave 		= "";
        String lsNombre 	= "";
        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();

        try{
        	con.conexionDB();

			lsQuery.delete(0, lsQuery.length());
			lsQuery.append(" SELECT distinct ccb.ic_pyme,  cp.cg_razon_social ");
			lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_pyme cp");
			lsQuery.append(" WHERE cpi.ic_if = " + esCveIf);
			lsQuery.append(" AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
			lsQuery.append(" AND ccb.ic_pyme = cp.ic_pyme");
			lsQuery.append(" AND ic_moneda = " + esCveMoneda);
			lsQuery.append(" AND cpi.ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
			lsQuery.append(" AND cpi.cs_borrado = 'N' ");
			lsQuery.append(" AND ccb.cs_borrado = 'N'");
			lsQuery.append(" ORDER BY cp.cg_razon_social ");

			lrsResultado = con.queryDB(lsQuery.toString());

			while (lrsResultado.next()) {
				lsClave = lrsResultado.getString(1);
				lsNombre = lrsResultado.getString(2);

                lovRegistro = new Vector();
                lovRegistro.addElement(lsClave);
                lovRegistro.addElement(lsNombre);

                lovDatos.addElement(lovRegistro);
		   }
		   lrsResultado.close();
	   	   con.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en getProveedor: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }

	/*********************************************************************************************
	*
	*	    Vector getPlazo()
	*
	*********************************************************************************************/
    // Agregado 04/07/2003	--HDG
    public Vector getPlazo(String esCveMoneda) throws NafinException {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
   		AccesoDB con  = new AccesoDB();
        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();
        try{
        	con.conexionDB();

			lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(" select t.ic_tasa, p.in_plazo_dias, p.ic_plazo ");
	   		lsQuery.append(" from comrel_tasa_producto tp, comcat_tasa t, comcat_plazo p ");
		   	lsQuery.append(" where tp.ic_tasa = t.ic_tasa ");
		   	lsQuery.append(" and t.ic_plazo = p.ic_plazo ");
	   		lsQuery.append(" and tp.ic_producto_nafin = 1 ");
		   	lsQuery.append(" and t.cs_disponible = 'S' ");
		   	lsQuery.append(" and t.ic_moneda = " + esCveMoneda);
			lsQuery.append(" order by p.ic_plazo ");
			lrsResultado = con.queryDB(lsQuery.toString());
			while (lrsResultado.next()) {
                lovRegistro = new Vector();
                lovRegistro.addElement(lrsResultado.getString(1));
                lovRegistro.addElement(lrsResultado.getString(2));
				lovRegistro.addElement(lrsResultado.getString(3));
                lovDatos.addElement(lovRegistro);
		   }
		   lrsResultado.close();
	   	   con.cierraStatement();
		} catch (Exception e) {
			System.out.println("Error en getPlazo: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return lovDatos;
    }

	/**
	 *	    
	 *	    Vector guardarTasas()
	 * @param esCveIf
	 * @param esCveMoneda
	 * @param esCvePyme
	 * @param esTipoTasa
	 * @param esCadenaTasas
	 * @param esValorTasas
	 * @param cc_acuse
	 * @param ic_usuario
	 * @param _acuse
	 * @param nombreUsuario
	 * @param lsPuntos
	 * @param aplicaFloating
	 * @return
	 * @throws NafinException
	 */
    public boolean guardarTasas(String esCveIf, String esCveMoneda,
    						   String esCvePyme, String esTipoTasa,
                               String[] esCadenaTasas, String[] esValorTasas,
							   String cc_acuse,String ic_usuario,String _acuse, String nombreUsuario,String lsPuntos, String aplicaFloating )
		throws NafinException {
			log.info(" guardarTasas Incia ");
			
		
    	StringBuffer lsQuery = new StringBuffer();
      ResultSet lrsResultado = null;
   		AccesoDB con  = new AccesoDB();
   		PreparedStatement ps = null;
   		String auxTipoTasa = esTipoTasa;
			StringBuffer lsQuery2 = new StringBuffer();
			String lsValorTasa ="";
			
   		if("R".equals(esTipoTasa))
   			esTipoTasa = "N";
   		else 
   			auxTipoTasa = "";

        String lsCveEpo 	= "";
        String lsTasa 		= "";
				String lsCuentaBancaria = "";
        String lsFecha 		= "";
				String lsPlazo		= "";
				String lsFechaIni	= "";		
				String lsFechaFin	= "NULL";	
				String lsNoTasa ="";

				int liRegistros = 0;
        boolean lbOK = true;

        Vector lovDatos 	= null;
				int ordenApp = 1;

			 ResultSet rs1 = null;
			 PreparedStatement ps1 = null;
			 int seqNEXTVAL	= 0;
		
        try{
        	con.conexionDB();
					
					
				lsQuery.append("SELECT SEQ_Bit_Tasas_Preferenciales.nextval FROM dual");
				ps1 = con.queryPrecompilado(lsQuery.toString());
				rs1 = ps1.executeQuery();
					if(rs1.next())
					{
						seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
					}
				ps1.close();
				rs1.close();
	
			log.debug("seqNEXTVAL--->"+lsQuery.toString());
			
        	if(!"".equals(_acuse)) {
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(
					" INSERT INTO COM_ACUSE3"+
					" (CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
					" VALUES(?,sysdate,?,?,1)");
				ps = con.queryPrecompilado(lsQuery.toString());
				ps.setString(1,cc_acuse);
				ps.setString(2,ic_usuario);
				ps.setString(3,_acuse);
				ps.execute();
				ps.close();
			}
				log.debug("lsQuery--->"+lsQuery.toString());
	
				String ic_epo = "";
			for(int i=0; i < esCadenaTasas.length; i++) {
				liRegistros = 0;
				lovDatos = new Vector();
				lovDatos = Comunes.explode("|",esCadenaTasas[i]);

				lsCveEpo = lovDatos.get(0).toString();
				if(!ic_epo.equals(lsCveEpo)) {
					ic_epo = lsCveEpo;
					if("P".equals(esTipoTasa) || "N".equals(esTipoTasa) ) {
						lsQuery.delete(0, lsQuery.length());
						lsQuery.append(
							" DELETE"   +
							"   FROM com_tasa_pyme_if"   +
							"  WHERE ic_epo = ?"   +
							"    AND ic_if = ?"   +
							"    AND cc_tipo_tasa = '" +esTipoTasa+"'"+
							"    AND ic_cuenta_bancaria IN (SELECT cpi.ic_cuenta_bancaria"   +
							"                                 FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb"   +
							"                                WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"   +
							"                                  AND cpi.cs_vobo_if = 'S'"   +
							"                                  AND cpi.cs_borrado = 'N'"   +
							"                                  AND ccb.cs_borrado = 'N'"   +
							"                                  AND cpi.ic_if = ?"   +
							"                                  AND cpi.ic_epo = ?"   +
							"                                  AND ccb.ic_moneda = ?"   +
							"                                  AND ccb.ic_pyme = ?)");
					
					 log.debug("lsQuery--->"+lsQuery.toString());
						
						ps = con.queryPrecompilado(lsQuery.toString());
						ps.setInt(1, Integer.parseInt(lsCveEpo));
						ps.setInt(2, Integer.parseInt(esCveIf));
						ps.setInt(3, Integer.parseInt(esCveIf));
						ps.setInt(4, Integer.parseInt(lsCveEpo));
						ps.setInt(5, Integer.parseInt(esCveMoneda));
						ps.setInt(6, Integer.parseInt(esCvePyme));
						ps.execute();
						ps.close();
					}//if("P".equals(esTipoTasa))
				}//if(!ic_epo.equals(lsCveEpo))
			}//for(int i=0; i < esCadenaTasas.length; i++)
				
			
			log.debug ("esCadenaTasas.length   "+esCadenaTasas.length);
			
			for(int i=0; i < esCadenaTasas.length; i++) {
				liRegistros = 0;
				lovDatos = new Vector();
				lovDatos = Comunes.explode("|",esCadenaTasas[i]);

				lsCveEpo = lovDatos.get(0).toString();
				//lsTasa   = lovDatos.get(1).toString();
				lsPlazo	 = lovDatos.get(2).toString();
				lsNoTasa =  lovDatos.get(3).toString();
				lsValorTasa = lovDatos.get(4).toString();
				
/*log.debug ("lsCveEpo---->"+lsCveEpo);
log.debug ("lsPlazo---->"+lsPlazo);
log.debug ("lsNoTasa---->"+lsNoTasa);
log.debug ("lsValorTasa---->"+lsValorTasa);
*/
				if("R".equals(auxTipoTasa)) {
					lsFechaIni = lovDatos.get(1).toString();
					lsFechaFin = lovDatos.get(3).toString();
				}

				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(" SELECT cpi.ic_cuenta_bancaria");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb");
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme = " + esCvePyme);

				//log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT1: "+lsQuery.toString());
				if ( lrsResultado.next()) {
					lsCuentaBancaria = lrsResultado.getString(1);
				} else
         lsCuentaBancaria = "";

				lrsResultado.close();
				con.cierraStatement();
				
				// busca si ya existe un registro para esta pyme
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(" SELECT count(1)");
				lsQuery.append(" FROM com_tasa_pyme_if");
				lsQuery.append(" WHERE ");
				lsQuery.append("  ic_if = "+ esCveIf);
				if(!lsCuentaBancaria.equals("")){
				lsQuery.append(" AND ic_cuenta_bancaria = " + lsCuentaBancaria);
				}
				lsQuery.append(" AND ic_epo =" + lsCveEpo);
				lsQuery.append(" AND cc_tipo_tasa = '"+ esTipoTasa + "'");
				lsQuery.append(" AND ic_plazo = "+lsPlazo);
				if(esTipoTasa.equals("N")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 1");
				else if(auxTipoTasa.equals("R"))
					lsQuery.append(" AND ic_orden = 2");
				else if(esTipoTasa.equals("P")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 3");
 
			//log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT2: "+lsQuery.toString());
				if ( lrsResultado.next()) {
					liRegistros = lrsResultado.getInt(1);
				}
				lrsResultado.close();
				con.cierraStatement();

				lsFecha = esTipoTasa.equals("P")? "null" : "sysdate";
				if("R".equals(auxTipoTasa)) {
					lsFecha = "to_date('"+lsFechaIni+"', 'dd/mm/yyyy')";
					lsFechaFin = "to_date('"+lsFechaFin+"', 'dd/mm/yyyy')";
				}

	/*********************************/
				if ( esTipoTasa.equals("N")){
					lsTasa = esValorTasas[i];
					lsValorTasa = lsTasa;
				}else {
					lsTasa = lsValorTasa;
				}
				System.out.println("lsTasa   "+lsTasa);
	/*********************************/

				// NO EXISTE EL REGISTRO POR LO QUE SE INSERTA
				//lsQuery.delete(0, lsQuery.length());
			
				if(auxTipoTasa.equals("R")){
					ordenApp = 2;
					lsTasa = lsTasa;
				}
				else if(esTipoTasa.equals("P")){
					ordenApp = 3;
					lsTasa = lsPuntos;				
				}
				
				System.out.println("lsPuntos--->"+lsPuntos);
				System.out.println("lsTasa--->"+lsTasa);
				System.out.println("lsValorTasa--->"+lsValorTasa); 
					
				if ( liRegistros == 0) {
					
					lsQuery = new StringBuffer();
					
					lsQuery.append(" INSERT INTO com_tasa_pyme_if");
					lsQuery.append(" (ic_cuenta_bancaria, ic_if, ic_epo, cc_tipo_tasa, fn_valor, ");
					lsQuery.append(" df_aplicacion, ic_plazo,cc_acuse, df_aplicacion_fin, ic_orden, CS_TASA_FLOATING) ");
					lsQuery.append(" VALUES("+ lsCuentaBancaria +","+ esCveIf +","+ lsCveEpo );
					lsQuery.append(" ,'" +esTipoTasa +"'," + lsTasa + ","+ lsFecha +", "+lsPlazo+",'"+cc_acuse+"', "+lsFechaFin+","+ordenApp+",'"+aplicaFloating+"' )");
				
				
					log.debug("com_tasa_pyme_if : "+lsQuery.toString());	 	
				
					ps = con.queryPrecompilado(lsQuery.toString());
					ps.executeUpdate();
					ps.close();
				
					lsQuery2 = new StringBuffer();
				 
					 String estatus= "A";
					 lsQuery2.append("	insert into Bit_Tasas_Preferenciales  "+
													 " (ic_Bitacora, ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
													" fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus, CS_TASA_FLOATING ) "+	
													" VALUES("+seqNEXTVAL+","+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
													esTipoTasa+"',"+esCveMoneda +","+	lsPuntos+","+
													lsValorTasa +",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"','"+aplicaFloating+"' )");
				
				log.debug("Bit_Tasas_Preferenciales : "+lsQuery2.toString());	
				
						ps = con.queryPrecompilado(lsQuery2.toString());
						ps.executeUpdate();
						ps.close();
					
				} 	// actualiza el Registro
				else {
					lsQuery = new StringBuffer();		
					lsQuery.append(" UPDATE com_tasa_pyme_if");
					lsQuery.append(" SET fn_valor = " + lsTasa);
					lsQuery.append(" ,cc_acuse = '" + cc_acuse+"'");
				
					if(auxTipoTasa.equals("R")) {
						lsQuery.append(" ,df_aplicacion = " + lsFecha);						
						lsQuery.append(" ,df_aplicacion_fin = " + lsFechaFin);
					} else
						lsQuery.append(" ,df_aplicacion = " + lsFecha);
					
					lsQuery.append(" , CS_TASA_FLOATING = '" + aplicaFloating+"'");
					
					lsQuery.append(" WHERE ic_cuenta_bancaria = " + lsCuentaBancaria );
					lsQuery.append(" AND ic_if = "+ esCveIf);
					lsQuery.append(" AND ic_epo =" + lsCveEpo);
					lsQuery.append(" AND cc_tipo_tasa = '"+ esTipoTasa + "'");
					lsQuery.append(" AND ic_plazo = "+lsPlazo);
					lsQuery.append(" AND ic_orden = " + ordenApp+"");
					
					log.debug("update -->: "+lsQuery.toString());	
					
						ps = con.queryPrecompilado(lsQuery.toString());
						ps.executeUpdate();
						ps.close();
			
						lsQuery2 = new StringBuffer();
						String estatus= "A";
					
						 lsQuery2.append("	insert into Bit_Tasas_Preferenciales  "+
													 " (ic_Bitacora, ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
													" fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus, CS_TASA_FLOATING) "+	
													" VALUES("+seqNEXTVAL+","+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
													esTipoTasa+"',"+esCveMoneda +","+	lsPuntos+","+
													lsValorTasa +",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"' ,'"+aplicaFloating+"' )");
		
							log.debug("Bit_Tasas_Preferenciales : "+lsQuery2.toString());		
							ps = con.queryPrecompilado(lsQuery2.toString());
							ps.executeUpdate();
							ps.close();
				}
				
			} //fin del for


		} catch (Exception e) {
        	lbOK = false;
        	e.printStackTrace();
			log.error("Error en guardarTasas: "+e);
			throw new NafinException("SIST0001");
		} finally {
	        con.terminaTransaccion(lbOK);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
			log.info("guardarTasas:  Termina");
    return lbOK;
    }


	/**
	 * Guarda la tasa por producto por epo. Cuando la clave de epo contiene
	 * "TODAS" entonces s�lo se guarda una  una vez, sin guardar la
	 * clave de la epo
	 * @param ic_producto_nafin Clave del producto nafin
	 * @param ic_epo Clave del producto epo o "TODAS" cuando se requiere asignar
	 * 		la tasa a todas las epos.
	 * @param ic_tasa Clave de la tasa
	 * @param ic_plazo Clave del plazo.
	 */
	public void guardarTasaProductoEpo(String ic_producto_nafin,
			String ic_epo, String ic_tasa, String ic_plazo) throws NafinException {

		AccesoDB con = new AccesoDB();
		String strSQL = "";
		boolean exito = true;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_epo == null || ic_producto_nafin == null ||
					ic_tasa == null || ic_plazo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			//Integer.parseInt(ic_epo);
			Integer.parseInt(ic_producto_nafin);
			Integer.parseInt(ic_tasa);
			Integer.parseInt(ic_plazo);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" ic_epo=" + ic_epo +
					" ic_producto_nafin=" + ic_producto_nafin +
					" ic_tasa=" + ic_tasa +
					" ic_plazo=" + ic_plazo
					);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			if (ic_epo.equals("TODAS")) {
				ic_epo = "null";
			}

			strSQL = " INSERT INTO comrel_tasa_producto_epo " +
					" (ic_tasa_producto_epo, ic_tasa, ic_producto_nafin, ic_plazo, ic_epo) " +
					" values ((SELECT Nvl(Max(ic_tasa_producto_epo) + 1, 1) " +
					" 		FROM comrel_tasa_producto_epo),	" +
						ic_tasa + "," +
						ic_producto_nafin + "," +
						ic_plazo + "," +
						ic_epo + " ) ";
			con.ejecutaSQL(strSQL);

			exito = true;
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Guarda la tasa por producto por epo.
	 * @param ic_producto_nafin Clave del producto nafin
	 * @param ic_epo Clave del producto epo o "TODAS" cuando se requiere asignar
	 * 		la tasa a todas las epos.
	 * @param ic_tasa Clave de la tasa
	 * @param ic_plazo Clave del plazo.
	 */
	public void actualizarTasaProductoEpo(String ic_tasa_producto_epo,
			String ic_tasa, String ic_plazo) throws NafinException {

		AccesoDB con = new AccesoDB();
		String strSQL = "";
		boolean exito = true;
		try {
			con.conexionDB();
			strSQL = " UPDATE comrel_tasa_producto_epo " +
					" SET ic_tasa = " + ic_tasa + "," +
					" ic_plazo = " + ic_plazo +
					" WHERE ic_tasa_producto_epo = " + ic_tasa_producto_epo;
			con.ejecutaSQL(strSQL);
			exito = true;
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Elimina la tasa por producto por epo.
	 * @param ic_producto_nafin Clave del producto nafin
	 * @param ic_epo Clave del producto epo o "TODAS" cuando se requiere asignar
	 * 		la tasa a todas las epos.
	 * @param ic_tasa Clave de la tasa
	 * @param ic_plazo Clave del plazo.
	 */
	public void eliminarTasaProductoEpo(String ic_tasa_producto_epo)
			 throws NafinException {

		AccesoDB con = new AccesoDB();
		String strSQL = "";
		boolean exito = true;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_tasa_producto_epo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(ic_tasa_producto_epo);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" ic_tasa_producto_epo=" + ic_tasa_producto_epo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			strSQL = " DELETE FROM comrel_tasa_producto_epo " +
					" 		WHERE ic_tasa_producto_epo = " + ic_tasa_producto_epo;
			con.ejecutaSQL(strSQL);
			exito = true;
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}
	
    public Vector obtenerTasas(String esCveIf, String esCveMoneda, String esCadenaEPO,
								String esCvePyME, String esCvePlazo) throws NafinException {
		System.out.println("AutorizacionTasasBean::procesarAltaGarantia(E)");
		AccesoDB con = new AccesoDB();
		Vector datos = new Vector();
		String tipoTasa ="";
		try {
			con.conexionDB();
			datos = obtenerTasas(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, esCvePlazo, con, tipoTasa);
		} catch(Exception e) { 
			System.out.println("::getSolicitudesSeleccionadas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally { 
			//con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("::getSolicitudesSeleccionadas(S)");
		}
		return datos;
	}

	/*********************************************************************************************
	*
	*	    Vector obtenerTasas()
	*
	*********************************************************************************************/
    // Agregado 28/10/2002	--CARP
    // Modificado 08/11/2004 -- HDG
    private Vector obtenerTasas(String esCveIf, String esCveMoneda, String esCadenaEPO,
								String esCvePyME, String esCvePlazo, AccesoDB eoConexionDB, String tipoTasa)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;

        String lsNombre 	= "";
        String lsCveEpo 	= "";
        String lsNomTasa 	= "";
        String lsValorTasa 	= "";
        String lsRelMatBase = "";
        String lsPuntosTasa = "";
        String lsRelMatClasif = "";
        String lsPuntosClasif = "";
        String lsCalificacion = "";
        String lsCadenaTasa = "";
        String lsTemp 		= "";
				String lsPlazo		= "";
				String lsDescripPlazo ="";
				String lsIc_Tasa ="";

        int liCalificacion 		= 0;
        double ldValorTotalTasa = 0;

        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();

        try{
			lsQuery.append(" SELECT distinct ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  ");
			lsQuery.append(" 	   cmt.fn_valor as ValorTasa, cta.cg_rel_mat as RelMatBase,  cta.fn_puntos as PuntosBase, ");
			lsQuery.append(" 	   cpe.ic_calificacion as Clasificacion, rt.cg_rel_mat as RelMatClasif, rt.cg_puntos as PuntosClasif  ");
			lsQuery.append(" 		, to_char(sysdate,'dd/mm/yyyy'), ctb.ic_plazo ");
			lsQuery.append(" 	,pl.CG_DESCRIPCION, ct.ic_tasa ");	
			//lsQuery.append(" ,pi.fn_valor as NoVaTasa ");
			 
			lsQuery.append(" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, ");
			
		//	lsQuery.append(" ,com_tasa_pyme_if pi  ,comrel_cuenta_bancaria ccb, ");
								
		/*	lsQuery.append(" 	 ( SELECT distinct ic_epo, max(dc_fecha_tasa) as fecha_tasa, ic_tasa ");
			lsQuery.append("	   FROM comrel_tasa_base  ");
			lsQuery.append(" 	   WHERE ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" 	   AND cs_vobo_nafin = 'S'");
			lsQuery.append(" 	   GROUP BY ic_epo, ic_tasa ) tb,");*/
			lsQuery.append(" 	 ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) mb,");
			lsQuery.append(" 	 com_mant_tasa cmt, comcat_epo ce, comrel_pyme_epo cpe, comrel_tipificacion rt ");
		  lsQuery.append(" 	,comcat_plazo  pl ");		
			lsQuery.append(" WHERE cta.ic_if = " + esCveIf);
			lsQuery.append(" AND cta.cs_vobo_if = 'S' ");
			lsQuery.append(" AND cta.cs_vobo_epo = 'S'");
			lsQuery.append(" AND cta.cs_vobo_nafin = 'S'");
			//lsQuery.append(" AND cta.ic_epo = tb.ic_epo");
			lsQuery.append(" AND cta.ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" AND cta.dc_fecha_tasa = ctb.dc_fecha_tasa");
			//lsQuery.append(" AND trunc(ctb.dc_fecha_tasa) = trunc(tb.fecha_tasa)");
			//lsQuery.append(" AND ctb.ic_epo = tb.ic_epo");
			//lsQuery.append(" AND ctb.ic_tasa = tb.ic_tasa");
			lsQuery.append(" AND ctb.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND ctb.ic_epo = ce.ic_epo ");
			lsQuery.append(" AND ctb.cs_tasa_activa = 'S' ");
			lsQuery.append(" AND cpe.ic_epo = ctb.ic_epo");
			lsQuery.append(" AND cpe.ic_pyme = " + esCvePyME);
			lsQuery.append(" AND cpe.ic_calificacion = rt.ic_calificacion");
			lsQuery.append(" AND rt.cs_vobo_nafin = 'S'");
			lsQuery.append(" AND rt.dc_fecha_tasa = ctb.dc_fecha_tasa");
			lsQuery.append(" AND ct.ic_moneda = " + esCveMoneda);
			lsQuery.append(" AND ct.ic_tasa = mb.ic_tasa");
			lsQuery.append(" AND cmt.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND ctb.ic_plazo  = pl.ic_plazo ");
			lsQuery.append(" AND trunc(cmt.dc_fecha) = trunc(mb.dc_fecha)");
			lsQuery.append((!esCvePlazo.equals("")?" AND ctb.ic_plazo = "+esCvePlazo:""));
			lsQuery.append("order by ct.ic_tasa");
					 
			
			lrsResultado = eoConexionDB.queryDB(lsQuery.toString());

			System.out.println("QUERY[[[[[ --->" + lsQuery);

			while ( lrsResultado.next()) {
				lsCveEpo 		= lrsResultado.getString(1);
				lsNombre 		= lrsResultado.getString(2);
				lsNomTasa 		= lrsResultado.getString(3);
				lsValorTasa 	= lrsResultado.getString(4);
				lsRelMatBase 	= lrsResultado.getString(5);
				lsPuntosTasa 	= lrsResultado.getString(6);
				liCalificacion 	= lrsResultado.getInt(7);
				lsRelMatClasif 	= lrsResultado.getString(8);
				lsPuntosClasif 	= lrsResultado.getString(9);
				lsPlazo			= lrsResultado.getString(11);
				lsDescripPlazo = lrsResultado.getString(12);
				lsIc_Tasa = lrsResultado.getString(13);
				//lsNoVaTasa = lrsResultado.getString(14);
				

				if ( lsValorTasa == null || lsValorTasa.equals(""))
					lsValorTasa = "0";
				if ( lsPuntosTasa == null || lsPuntosTasa.equals(""))
					lsPuntosTasa = "0";
				if ( lsPuntosClasif == null || lsPuntosClasif.equals(""))
					lsPuntosClasif = "0";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;

				lsTemp = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsTemp += lsRelMatClasif + lsPuntosClasif;
				}

                lovRegistro = new Vector();

                lovRegistro.addElement(lsCveEpo);
                lovRegistro.addElement(lsNombre);
                lovRegistro.addElement(lsCalificacion);
                lovRegistro.addElement(lsCadenaTasa);
                lovRegistro.addElement(new Double(ldValorTotalTasa));
                lovRegistro.addElement(lsTemp);
								lovRegistro.addElement(lsPlazo);
								lovRegistro.addElement(lsDescripPlazo);
								lovRegistro.addElement(lsIc_Tasa);
						//		lovRegistro.addElement(lsNoVaTasa);
                lovDatos.addElement(lovRegistro);
								
									System.out.println("lovDatos: "+lovDatos);
									
            }// Fin del While
            lrsResultado.close();
            eoConexionDB.cierraStatement();
		} catch (Exception e) {
			System.out.println("Error en obtenerTasas: "+e);
			throw new NafinException("SIST0001");
		} //finally {
        return lovDatos;
		//}
    }

	/*********************************************************************************************
	*
	*	    Vector procesarTasaNegociada()--metodo sobrecargado
	*      FODEA 046-2009 FVR -INI
	*
	*********************************************************************************************/
    // Agregado 10/10/2009	-- FVR
    public Vector procesarTasaNegociada (String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo, String tipoTasa)
		throws NafinException
    {
		return procesarTasaNegociada(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, esCvePlazo, "",tipoTasa);
	 }
	
	/*********************************************************************************************
	*
	*	    Vector procesarTasaNegociada()
	*
	*********************************************************************************************/
    // Agregado 28/10/2002	--CARP
    public Vector  procesarTasaNegociada(String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo, String tipo_factoraje, String tipoTasa)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
   		AccesoDB con  = new AccesoDB();

        String lsCveEpo 	= "";
        String lsCalificacion = "";
        String lsCadenaTasa = "";
		    String lsValorTasaP = "";
        String lsValorTasaN = "";
        String lsTipo 		= "";
        String lsTemp 		= "";
        String lsFechaHoy 	= "";
		    String lsPlazo		= "";
				String lsDescripPlazo ="";
				String lsIc_Tasa ="";

        int liExiste 	= 0;
		    double ldTotal 			= 0;
        double ldValorTotalTasa = 0;

        Vector lovTasas 	= null;
        Vector lovRegistro 	= null;
        Vector lovRegistro2	= null;
        Vector lovDatos 	= new Vector();

        try {
        	con.conexionDB();

			lovTasas = obtenerTasas(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, esCvePlazo, con, tipoTasa);

            for ( int i = 0; i < lovTasas.size(); i++) {
	            lsValorTasaP = lsValorTasaN = lsFechaHoy =  "";

            	lovRegistro = (Vector) lovTasas.elementAt(i);

				        lsCveEpo 	= (String) lovRegistro.elementAt(0);
                lsCadenaTasa 	= (String) lovRegistro.elementAt(3);
                ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
                lsTemp 			= (String) lovRegistro.elementAt(5);
				        lsPlazo			= lovRegistro.elementAt(6).toString();
								lsDescripPlazo = lovRegistro.elementAt(7).toString();
								lsIc_Tasa = lovRegistro.elementAt(8).toString();

	            lsQuery.delete(0,lsQuery.length());
                lsQuery.append(" SELECT ctpi.cc_tipo_tasa, NVL(ctpi.fn_valor,0),  ");
				lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
                lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,");
				//FODEA 046-2009 FVR -INI
				if(!"M".equals(tipo_factoraje))
					lsQuery.append(" com_tasa_pyme_if ctpi");
				else
					lsQuery.append(" com_tasa_pyme_if_mandato ctpi");
				//FODEA 046-2009 FVR -FIN
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_if = ctpi.ic_if");
				lsQuery.append(" AND cpi.ic_epo = ctpi.ic_epo");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme = " + esCvePyME);
				lsQuery.append(" AND ctpi.ic_plazo = " + lsPlazo);
			//	lsQuery.append(" AND ctpi.cc_tipo_tasa = '"+tipoTasa+"'");
				lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");

				System.out.println("Query 2 = " + lsQuery.toString());
				lrsResultado = con.queryDB(lsQuery.toString());

				while ( lrsResultado.next()) {
					lsTipo = lrsResultado.getString(1);
					liExiste = lrsResultado.getInt(3);
                    lsFechaHoy = lrsResultado.getString(4);

					if ( lsTipo.equals("N") && liExiste == 0)
						lsValorTasaN = lrsResultado.getString(2);
					else
						if ( lsTipo.equals("P"))
							lsValorTasaP = lrsResultado.getString(2);
				}
				lrsResultado.close();
				con.cierraStatement();

				lsCalificacion = (!lsValorTasaP.equals(""))? "PREFERENCIAL" : (String) lovRegistro.elementAt(2);

				if ( !lsValorTasaP.equals("")){
					lsCadenaTasa += "&nbsp;-&nbsp;" + lsValorTasaP;
                    lsTemp += "-"+ lsValorTasaP;
                }

				if ( lsValorTasaP.equals(""))
					lsValorTasaP = "0";
				if ( lsValorTasaP != null || !lsValorTasaP.equals("") )
					ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;

                lovRegistro2 = new Vector();

                lovRegistro2.add(0,lovRegistro.elementAt(0));
                lovRegistro2.add(1,lovRegistro.elementAt(1));
								lovRegistro2.add(2,lsCalificacion);
								lovRegistro2.add(3,lsCadenaTasa);
                lovRegistro2.add(4,new Double(ldTotal));
                lovRegistro2.add(5,lsValorTasaN);
                lovRegistro2.add(6,(lsFechaHoy.equals("")?(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date()):lsFechaHoy));
                lovRegistro2.add(7,lsTemp);
			        	lovRegistro2.add(8,lsPlazo);
								lovRegistro2.add(9,lsDescripPlazo);		
								lovRegistro2.add(10,lsIc_Tasa);		
								

                lovDatos.addElement(lovRegistro2);
            }// Fin del for

		} catch (Exception e) {
			System.out.println("Error en procesarTasaNegociada: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }

   	/*********************************************************************************************
	*
	*	    Vector procesarTasaPreferencial()
	*
	*********************************************************************************************/
    // Agregado 28/10/2002	--CARP
    public Vector procesarTasaPreferencial(String esCveIf, String esCveMoneda,
    									   String esCadenaEPO, String esCvePyME,
                                           double edPuntos, String tipoTasa, String aplicaFloating )
		throws NafinException
    {
    	/*StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
				 ResultSet lrsResultado1 = null;
   		AccesoDB con  = new AccesoDB();

		double ldTotal 			= 0;
        double ldValorTotalTasa = 0;

        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();
        Vector lovTasas 	= null;
			  String 	lsPlazo	="";
				String lsCveEpo ="";
				String lsValorTasaN ="";
        try{
        	con.conexionDB();

			lovTasas = obtenerTasas(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, "", con, tipoTasa);

            for ( int i = 0; i < lovTasas.size(); i++) {
						
					lsCveEpo 		= (String) lovRegistro.elementAt(0);					
					ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();					
					lsPlazo			= lovRegistro.elementAt(6).toString();
					
					lovRegistro = (Vector) lovTasas.elementAt(i);
						
					lsQuery.delete(0,lsQuery.length());
          lsQuery.append(" SELECT ctpi.cc_tipo_tasa, NVL(ctpi.fn_valor,0),  ");
					lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
          lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
					lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,");				
					lsQuery.append(" com_tasa_pyme_if ctpi");				
					lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
					lsQuery.append(" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria");
					lsQuery.append(" AND cpi.ic_if = ctpi.ic_if");
					lsQuery.append(" AND cpi.ic_epo = ctpi.ic_epo");
					lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
					lsQuery.append(" AND cpi.cs_borrado = 'N'");
					lsQuery.append(" AND ccb.cs_borrado = 'N' ");
					lsQuery.append(" AND cpi.ic_if = " + esCveIf);
					lsQuery.append(" AND cpi.ic_epo =  " + lsCveEpo);
					lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
					lsQuery.append(" AND ccb.ic_pyme = " + esCvePyME);
					lsQuery.append(" AND ctpi.ic_plazo = " + lsPlazo);
					lsQuery.append(" AND ctpi.cc_tipo_tasa = '"+tipoTasa+"'");
					lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");

				System.out.println("Query 2 = " + lsQuery.toString());
				
				lrsResultado = con.queryDB(lsQuery.toString());

				while ( lrsResultado1.next()) {
					lsValorTasaN = lrsResultado1.getString(2);
					
				}
				 lovRegistro.addElement(lsValorTasaN);
				
				lrsResultado1.close();
				con.cierraStatement();
						
            	

       ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();

       ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;

       lovRegistro.addElement(new Double(ldTotal));

       lovDatos.addElement(lovRegistro);
			 
       }// Fin del for*/
			 
			 
			StringBuffer lsQuery = new StringBuffer();
			ResultSet lrsResultado = null;
			AccesoDB con  = new AccesoDB();
			
			String lsCveEpo 	= "";
			String lsCalificacion = "";
			String lsCadenaTasa = "";
			String lsValorTasaP = "";
			String lsValorTasaN = "";
			String lsTipo 		= "";
			String lsTemp 		= "";
			String lsFechaHoy 	= "";
			String lsPlazo		= "";
			String lsDescripPlazo ="";
			String lsIc_Tasa ="";
			String lsNombre ="";
			
			int liExiste 	= 0;
			double ldTotal 			= 0;
			double ldValorTotalTasa = 0;
			
			Vector lovTasas 	= null;
			Vector lovRegistro 	= null;
			Vector lovRegistro2	= null;
			Vector lovDatos 	= new Vector();
			String  aplifloating = "";
			
			try {
				con.conexionDB();

				lovTasas = obtenerTasas(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, "", con, tipoTasa);

            for ( int i = 0; i < lovTasas.size(); i++) {
	            lsValorTasaP = lsValorTasaN = lsFechaHoy =  "";
            	lovRegistro = (Vector) lovTasas.elementAt(i);
					lsCveEpo 	= (String) lovRegistro.elementAt(0);
					lsNombre = (String) lovRegistro.elementAt(1);
					lsCalificacion =  (String) lovRegistro.elementAt(2);
					lsCadenaTasa 	= (String) lovRegistro.elementAt(3);
					ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
					lsTemp 			= (String) lovRegistro.elementAt(5);
					lsPlazo			= lovRegistro.elementAt(6).toString();
					lsDescripPlazo = lovRegistro.elementAt(7).toString();
					lsIc_Tasa = lovRegistro.elementAt(8).toString();


					lsQuery.delete(0,lsQuery.length());
					lsQuery.append(" SELECT ctpi.cc_tipo_tasa, NVL(ctpi.fn_valor,0),  ");
					lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
					lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
					lsQuery.append(" , ctpi.CS_TASA_FLOATING  AS TASAFLOATING");
					lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,");
					lsQuery.append(" com_tasa_pyme_if ctpi");	
					lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
					lsQuery.append(" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria");
					lsQuery.append(" AND cpi.ic_if = ctpi.ic_if");
					lsQuery.append(" AND cpi.ic_epo = ctpi.ic_epo");
					lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
					lsQuery.append(" AND cpi.cs_borrado = 'N'");
					lsQuery.append(" AND ccb.cs_borrado = 'N' ");
					lsQuery.append(" AND cpi.ic_if = " + esCveIf);
					lsQuery.append(" AND cpi.ic_epo = " + lsCveEpo);
					lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
					lsQuery.append(" AND ccb.ic_pyme = " + esCvePyME);
					lsQuery.append(" AND ctpi.ic_plazo = " + lsPlazo);
					
					if (!"".equals(aplicaFloating)  ) {
						lsQuery.append(" AND ctpi.CS_TASA_FLOATING = '" + aplicaFloating+ "'");
					}
					
					//lsQuery.append(" AND ctpi.cc_tipo_tasa = '"+tipoTasa+"'");
					lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");

					System.out.println("Query 2 = " + lsQuery.toString());
					lrsResultado = con.queryDB(lsQuery.toString());

					while ( lrsResultado.next()) {
						lsTipo = lrsResultado.getString(1);
						liExiste = lrsResultado.getInt(3);
						lsFechaHoy = lrsResultado.getString(4);

						if ( lsTipo.equals("N") && liExiste == 0)
							lsValorTasaN = lrsResultado.getString(2);
						else
							if ( lsTipo.equals("P"))
								lsValorTasaP = lrsResultado.getString(2);
												
						 aplifloating = (lrsResultado.getString("TASAFLOATING")==null)?"N":lrsResultado.getString("TASAFLOATING");
						
					}
					lrsResultado.close();
					con.cierraStatement();

					ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
			
					ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;

					if(lsValorTasaP.equals("")){
						lsValorTasaP ="0";
					}
					
					String floating = lsCalificacion;
					if ("S".equals(aplifloating)  ) {
						floating = "Preferencial / Floating";						
					}else  if ("N".equals(aplifloating)  ) {
						floating = "Preferencial";	
					}
				  
					lovRegistro2 = new Vector();               
					lovRegistro2.addElement(lsCveEpo);
               lovRegistro2.addElement(lsNombre);
               lovRegistro2.addElement(floating);
               lovRegistro2.addElement(lsCadenaTasa);
               lovRegistro2.addElement(new Double(ldValorTotalTasa));
               lovRegistro2.addElement(lsTemp);
					lovRegistro2.addElement(lsPlazo);
					lovRegistro2.addElement(lsDescripPlazo);
					lovRegistro2.addElement(lsIc_Tasa);
					lovRegistro2.addElement(lsValorTasaP);
					lovRegistro2.addElement(new Double(ldTotal));
	
               lovDatos.addElement(lovRegistro2);
            }// Fin del for

		} catch (Exception e) {
			System.out.println("Error en procesarTasaPreferencial: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }

public Vector consultaTasaBase(String ic_epo, String fecha, String sesIdiomaUsuario)
		throws NafinException {
		String				qrySentencia	= "";
		String				condicion		= "";
    	StringBuffer		lsQuery 		= new StringBuffer();
        ResultSet			lrsResultado	= null;
        PreparedStatement	ps				= null;
        String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

        Vector renglones = new Vector();
        Vector columnas;

   		AccesoDB con  = new AccesoDB();

        try{
        	con.conexionDB();
        	if(!fecha.equals("")) {
        		condicion =
					"          (SELECT tb2.ic_epo, tb2.ic_tasa, tb2.dc_fecha_tasa"   +
					"             FROM comrel_tasa_base tb2"   +
					"            WHERE tb2.ic_epo = ?"   +
					"              AND tb2.cs_vobo_nafin = 'S'"   +
					"              AND tb2.cs_tasa_activa = 'S'"   +
					"              AND TRUNC (tb2.dc_fecha_tasa) = TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) tbu";
        	} else {
        		condicion =
					"          (SELECT   tb2.ic_epo, tb2.ic_tasa,"   +
					"                    MAX (tb2.dc_fecha_tasa) AS dc_fecha_tasa"   +
					"               FROM comrel_tasa_base tb2"   +
					"              WHERE tb2.ic_epo = ? AND tb2.cs_vobo_nafin = 'S'"   +
					"				 AND tb2.cs_tasa_activa = 'S'"+
					"           GROUP BY tb2.ic_epo, tb2.ic_tasa) tbu";

        	}


        	qrySentencia =

				" SELECT   t.cd_nombre, mt.fn_valor, tb.cg_rel_mat, tb.fn_puntos,"   +
				"          tb.cs_vobo_nafin, TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS'),"   +
				"          tb.ic_epo, m.cd_nombre"+idioma+" as cd_nombre, p.cg_descripcion AS rangoplazo"   +
				"     FROM comcat_tasa t,"   +
				"          (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"               FROM com_mant_tasa"   +
				"           GROUP BY ic_tasa) x,"   +
				"          comrel_tasa_base tb,"   +
				"          com_mant_tasa mt,"   +
				"          comcat_moneda m,"   +
				"          comcat_plazo p,"   +

				condicion +

				"    WHERE t.ic_tasa = x.ic_tasa"   +
				"      AND mt.dc_fecha = x.dc_fecha"   +
				"      AND t.ic_tasa = mt.ic_tasa"   +
				"      AND t.ic_moneda = m.ic_moneda"   +
				"      AND tb.ic_plazo = p.ic_plazo"   +
				"      AND t.ic_tasa = tb.ic_tasa"   +
				"      AND tb.ic_epo = tbu.ic_epo"   +
				"      AND tb.ic_tasa = tbu.ic_tasa"   +
				"      AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"      AND t.ic_tasa = tb.ic_tasa"   +
				"      AND tb.ic_epo = ?"   +
				"      AND tb.cs_tasa_activa = 'S'"   +
				"      AND t.cs_disponible = 'S'"   +
				" ORDER BY m.ic_moneda"  ;

        	lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(qrySentencia);

			ps = con.queryPrecompilado(lsQuery.toString());
			int cont = 1;
			ps.setInt(cont, Integer.parseInt(ic_epo));
			if(!"".equals(fecha)) {
				cont++;ps.setString(cont, fecha);
			}
			cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			lrsResultado = ps.executeQuery();
			while(lrsResultado.next()){
				columnas = new Vector();
				columnas.addElement((lrsResultado.getString(1)==null?"":lrsResultado.getString(1)));
				columnas.addElement((lrsResultado.getString(2)==null?"":lrsResultado.getString(2)));
				columnas.addElement((lrsResultado.getString(3)==null?"":lrsResultado.getString(3)));
				columnas.addElement((lrsResultado.getString(4)==null?"":lrsResultado.getString(4)));
				columnas.addElement((lrsResultado.getString(5)==null?"":lrsResultado.getString(5)));
				columnas.addElement((lrsResultado.getString(6)==null?"":lrsResultado.getString(6)));
				columnas.addElement((lrsResultado.getString(7)==null?"":lrsResultado.getString(7)));
				columnas.addElement((lrsResultado.getString(8)==null?"":lrsResultado.getString(8)));
				columnas.addElement((lrsResultado.getString(9)==null?"":lrsResultado.getString(9)));
				renglones.addElement(columnas);
			}
			lrsResultado.close();
			if(ps!=null) ps.close();

		} catch (Exception e) {
			System.out.println("Error en consultaTasaBase: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return renglones;
    }
    
    
     public Vector consultaTasaBase(String ic_epo, String fecha)
		throws NafinException {
			
		return consultaTasaBase(ic_epo, fecha,"ES");
			
    }

    

    public Vector consultaTasaAutoIf(String ic_epo, String fecha, String ic_IF, String sesIdiomaUsuario)
		throws NafinException {
		String				qrySentencia	= "";
		String				condicion		= "";
    	StringBuffer		lsQuery 		= new StringBuffer();
        ResultSet			lrsResultado	= null;
        PreparedStatement	ps				= null;
        String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

        Vector renglones = new Vector();
        Vector columnas;

   		AccesoDB con  = new AccesoDB();

        try{
        	con.conexionDB();
        	if(!"".equals(fecha)) {
        		condicion =
					"        (SELECT tb2.ic_epo, tb2.ic_tasa, tb2.dc_fecha_tasa"   +
					"           FROM comrel_tasa_base tb2"   +
					"          WHERE tb2.ic_epo = ?"   +
					"            AND tb2.cs_vobo_nafin = 'S'"   +
				//	"            AND tb2.cs_tasa_activa = 'S'"   +
					"            AND TRUNC (tb2.dc_fecha_tasa) = TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) tbu";

        	} else {
        		condicion =
					"        (SELECT   tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa"   +
					"             FROM comrel_tasa_base tb"   +
					"            WHERE tb.ic_epo = ? AND tb.cs_vobo_nafin = 'S'"   +
				//	"            AND tb.cs_tasa_activa = 'S'"   +
					"         GROUP BY tb.ic_epo, tb.ic_tasa) tbu";
        	}

        	qrySentencia =
				" SELECT (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social),"   +
				"        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin,"   +
				"        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS'), t.cd_nombre,"   +
				"        m.cd_nombre"+idioma+" as cd_nombre, i.ic_if, m.ic_moneda, ta.cg_rel_mat as relacion, ta.fn_puntos as puntos, " +
        		"		 p.cg_descripcion as rangoplazo, mt.fn_valor as valor "   +
				"   FROM comcat_if i,"   +
				"        comrel_tasa_autorizada ta,"   +
				"        comcat_tasa t,"   +
				"        comcat_moneda m,"   +
				"		 comrel_tasa_base tb, " +
        		"		 comcat_plazo p, " +
        		"		 com_mant_tasa mt, " +
				"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"             FROM com_mant_tasa"   +
				"         GROUP BY ic_tasa) x,"   +
				condicion+
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"   +
				"    AND ta.ic_epo = ?"   +
				"    AND ta.ic_if = i.ic_if"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND ta.ic_epo = tbu.ic_epo"   +
				"    AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"    AND tbu.ic_tasa = t.ic_tasa"   +
                "    AND tbu.ic_tasa = tb.ic_tasa" +
 				"    AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa" +
				"    AND tb.ic_plazo = p.ic_plazo" +
				"    AND mt.dc_fecha = x.dc_fecha" +
				"    AND t.ic_tasa = mt.ic_tasa" +
				"    AND ta.cs_vobo_nafin = 'S'"  ;
            //new 08/11/2004
			if(!ic_IF.equals("0")) {
				 qrySentencia+=" AND ta.ic_if = " + ic_IF;
			}//if icIF
			qrySentencia+=" ORDER BY m.ic_moneda ";
			//end new 08/11/2004
        	lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(qrySentencia);		   	

 System.out.println("\n lsQuery.toString(): "+lsQuery.toString()); 

			ps = con.queryPrecompilado(lsQuery.toString());
			int cont = 1;
			ps.setInt(cont, Integer.parseInt(ic_epo));
			if(!"".equals(fecha)) {
				cont++;ps.setString(cont, fecha);
			}
			cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			lrsResultado = ps.executeQuery();
			while(lrsResultado.next()){
				columnas = new Vector();
				columnas.addElement((lrsResultado.getString(1)==null?"":lrsResultado.getString(1)));
				columnas.addElement((lrsResultado.getString(2)==null?"":lrsResultado.getString(2)));
				columnas.addElement((lrsResultado.getString(3)==null?"":lrsResultado.getString(3)));
				columnas.addElement((lrsResultado.getString(4)==null?"":lrsResultado.getString(4)));
				columnas.addElement((lrsResultado.getString(5)==null?"":lrsResultado.getString(5)));
				columnas.addElement((lrsResultado.getString(6)==null?"":lrsResultado.getString(6)));
				columnas.addElement((lrsResultado.getString(7)==null?"":lrsResultado.getString(7)));
				columnas.addElement((lrsResultado.getString(8)==null?"":lrsResultado.getString(8)));
				columnas.addElement((lrsResultado.getString(9)==null?"":lrsResultado.getString(9)));
				columnas.addElement((lrsResultado.getString(10)==null?"":lrsResultado.getString(10)));
				columnas.addElement((lrsResultado.getString(11)==null?"":lrsResultado.getString(11)));
				columnas.addElement((lrsResultado.getString(12)==null?"":lrsResultado.getString(12)));
				columnas.addElement((lrsResultado.getString(13)==null?"":lrsResultado.getString(13)));
				renglones.addElement(columnas);
			}
			lrsResultado.close();
			if(ps!=null) ps.close();

		} catch (Exception e) {
			System.out.println("Error en consultaTasaAutoIf: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return renglones;
    }
    
    
	public Vector consultaTasaAutoIf(String ic_epo, String fecha, String ic_IF)
		throws NafinException {    
		
		return consultaTasaAutoIf(ic_epo, fecha,ic_IF,"ES");
			
    }

	public Vector consultaTraeMoneda(String _sEPO, String _sIF, String _cveMoneda)
		throws NafinException {
		String				qrySentencia	= "";
    	StringBuffer		lsQuery 		= new StringBuffer();
        ResultSet			lrsResultado	= null;
        PreparedStatement	ps				= null;

        Vector renglones = new Vector();
        Vector columnas;

   		AccesoDB con  = new AccesoDB();

        try{
        	con.conexionDB();
	        qrySentencia="SELECT e.IC_MONEDA, e.CD_NOMBRE, "+
                            " nvl ( ( SELECT to_char(i.DF_ALTA,'dd/mm/yyyy') " +
    						" FROM COMREL_IF_EPO_MONEDA  i" +
							" WHERE i.IC_EPO =? " +
							" AND   i.IC_IF  =? " +
							" AND   i.IC_MONEDA = e.IC_MONEDA"+
							"     ) , 'no asociada' ) "+
							" FROM COMCAT_MONEDA e, COMREL_TASA_PRODUCTO rtp, COMCAT_TASA t  "+
							" WHERE e.IC_MONEDA = t.IC_MONEDA "+
							" AND t.ic_moneda = ? "+
							" AND rtp.IC_TASA = t.IC_TASA "+
							" AND rtp.IC_PRODUCTO_NAFIN = 1 "+
							" ORDER BY 2";
			lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(qrySentencia);

			ps = con.queryPrecompilado(lsQuery.toString());
			ps.setInt(1, Integer.parseInt(_sEPO));
			ps.setInt(2, Integer.parseInt(_sIF));
			ps.setInt(3,Integer.parseInt(_cveMoneda));

			lrsResultado = ps.executeQuery();
			while(lrsResultado.next()){
				columnas = new Vector();
				columnas.addElement((lrsResultado.getString(1)==null?"":lrsResultado.getString(1)));
				columnas.addElement((lrsResultado.getString(3)==null?"":lrsResultado.getString(3)));
				renglones.addElement(columnas);
			}
			lrsResultado.close();
			if(ps!=null) ps.close();
		} catch (Exception e) {
		  System.out.println("Error en consultaTraeMoneda: "+e);
		  throw new NafinException("SIST0001");
		} finally {
		if (con.hayConexionAbierta())
		    con.cierraConexionDB();
		}
	    return renglones;
}//consultaTraeMoneda



public int operaFactorajeVencido(String ic_epo)
		throws NafinException {
		String				qrySentencia	= "";
    	StringBuffer		lsQuery 		= new StringBuffer();
        ResultSet			lrsResultado	= null;
        PreparedStatement	ps				= null;

        int cuantos = 0;

   		AccesoDB con  = new AccesoDB();

        try{
        	con.conexionDB();

        	qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND CS_FACTORAJE_VENCIDO = 'S'"   +
				"    AND ic_epo = ?"  ;

        	lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(qrySentencia);

			ps = con.queryPrecompilado(lsQuery.toString());
			int cont = 1;
			ps.setInt(cont, Integer.parseInt(ic_epo));

			lrsResultado = ps.executeQuery();
			if(lrsResultado.next()){
				cuantos = lrsResultado.getInt(1);
			}
			lrsResultado.close();
			if(ps!=null) ps.close();

		} catch (Exception e) {
			System.out.println("Error en operaFactorajeVencido: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return cuantos;
 }//operaFactorajeVencido

  public Vector consultaHistTasa(String ic_epo, String fecha)
		throws NafinException {
		String				qrySentencia	= "";
    	StringBuffer		lsQuery 		= new StringBuffer();
        ResultSet			lrsResultado	= null;
        PreparedStatement	ps				= null;

        Vector renglones = new Vector();
        Vector columnas;

   		AccesoDB con  = new AccesoDB();

        try{
        	con.conexionDB();
        	if(fecha.equals("")) {
        		qrySentencia =
					" SELECT DISTINCT TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY'), tb.ic_epo"   +
					"            FROM (SELECT MAX (tb2.dc_fecha_tasa) AS fecha"   +
					"                    FROM comrel_tasa_base tb2"   +
					"                   WHERE tb2.ic_epo = ? AND tb2.cs_vobo_nafin = 'S') tbu,"   +
					"                 comrel_tasa_base tb"   +
					"           WHERE tb.ic_epo = ?"   +
					"             AND tb.cs_vobo_nafin = 'S'"   +
					"             AND TRUNC (tb.dc_fecha_tasa) != TRUNC (tbu.fecha)"  ;
        	} else {
        		qrySentencia =
					" SELECT DISTINCT TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY'), tb.ic_epo"   +
					"            FROM comrel_tasa_base tb"   +
					"           WHERE tb.ic_epo = ?"   +
					"             AND tb.cs_vobo_nafin = 'S'"   +
					"             AND TRUNC (tb.dc_fecha_tasa) != TRUNC (TO_DATE (?, 'DD/MM/YY'))"  ;
        	}
        	lsQuery.delete(0, lsQuery.length());
		   	lsQuery.append(qrySentencia);

			ps = con.queryPrecompilado(lsQuery.toString());
		
			if("".equals(fecha)) {
				ps.setInt(1, Integer.parseInt(ic_epo));
				ps.setInt(2, Integer.parseInt(ic_epo));
			}else {
				ps.setInt(1, Integer.parseInt(ic_epo));
				ps.setString(2, fecha);
			}
			
			lrsResultado = ps.executeQuery();
			while(lrsResultado.next()){
				columnas = new Vector();
				columnas.addElement((lrsResultado.getString(1)==null?"":lrsResultado.getString(1)));
				columnas.addElement((lrsResultado.getString(2)==null?"":lrsResultado.getString(2)));
				renglones.addElement(columnas);
			}
			lrsResultado.close();
			if(ps!=null) ps.close();

		} catch (Exception e) {
			System.out.println("Error en consultaHistTasa: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return renglones;
  }//consultaHistTasa
  
  public Vector getMonedaRelacionadaIF (String claveEPO, String claveIF) throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();
		try {
			con.conexionDB();

			qrySentencia = " SELECT ic_moneda " +
							"FROM COMREL_IF_EPO_MONEDA " +
							"WHERE IC_EPO =  " + claveEPO +
							"AND IC_IF = " + claveIF;

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				registros.add(rs.getString("ic_moneda"));
			}
			rs.close();
			con.cierraStatement();
			return registros;
		} catch (Exception e) {
			System.out.println("Error en getMonedaRelacionadaIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/*********************************************************************************************
	*
	*	    boolean guardarTasasMandato()
	*
	*********************************************************************************************/
    // Agregado 10/10/2009	-- FVR
    public boolean guardarTasasMandato(String esCveIf, String esCveMoneda,
    						   String esCvePyme, String esTipoTasa,
                               String[] esCadenaTasas, String[] esValorTasas,
							   String cc_acuse,String ic_usuario,String _acuse)
	throws NafinException 
	{
		StringBuffer lsQuery = new StringBuffer();
      ResultSet lrsResultado = null;
   	AccesoDB con  = new AccesoDB();
   	PreparedStatement ps = null;
   	String auxTipoTasa = esTipoTasa;
   	if("R".equals(esTipoTasa))
			esTipoTasa = "N";
   	else 
   		auxTipoTasa = "";

      String lsCveEpo 	= "";
      String lsTasa 		= "";
      String lsFecha 	= "";
		String lsPlazo		= "";
		String lsFechaIni	= "";		
		String lsFechaFin	= "NULL";		

  		int liRegistros = 0;
      boolean lbOK = true;

      Vector lovDatos 	= null;

      try{
			con.conexionDB();
			if(!"".equals(_acuse)) {
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(
					" INSERT INTO COM_ACUSE3"+
					" (CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
					" VALUES(?,sysdate,?,?,1)");
				ps = con.queryPrecompilado(lsQuery.toString());
				ps.setString(1,cc_acuse);
				ps.setString(2,ic_usuario);
				ps.setString(3,_acuse);
				ps.execute();
				ps.close();
			}

			String ic_epo = "";
			for(int i=0; i < esCadenaTasas.length; i++) {
				liRegistros = 0;
				lovDatos = new Vector();
				lovDatos = Comunes.explode("|",esCadenaTasas[i]);

				lsCveEpo = lovDatos.get(0).toString();
				if(!ic_epo.equals(lsCveEpo)) {
					ic_epo = lsCveEpo;
					if("P".equals(esTipoTasa)) {
						lsQuery.delete(0, lsQuery.length());
						lsQuery.append(
							" DELETE"   +
							"   FROM com_tasa_pyme_if_mandato"   +
							"  WHERE ic_epo = ?"   +
							"    AND ic_if = ?"   +
							"    AND cc_tipo_tasa = 'P'"   +
							"    AND ic_mandante = ? ");
							/*
							"    AND ic_cuenta_bancaria IN (SELECT cpi.ic_cuenta_bancaria"   +
							"                                 FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb"   +
							"                                WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"   +
							"                                  AND cpi.cs_vobo_if = 'S'"   +
							"                                  AND cpi.cs_borrado = 'N'"   +
							"                                  AND ccb.cs_borrado = 'N'"   +
							"                                  AND cpi.ic_if = ?"   +
							"                                  AND cpi.ic_epo = ?"   +
							"                                  AND ccb.ic_moneda = ?"   +
							"                                  AND ccb.ic_pyme = ?)");
							*/
						ps = con.queryPrecompilado(lsQuery.toString());
						ps.setInt(1, Integer.parseInt(lsCveEpo));
						ps.setInt(2, Integer.parseInt(esCveIf));
						ps.setInt(3, Integer.parseInt(esCvePyme));
						/*
						ps.setInt(3, Integer.parseInt(esCveIf));
						ps.setInt(4, Integer.parseInt(lsCveEpo));
						ps.setInt(5, Integer.parseInt(esCveMoneda));
						ps.setInt(6, Integer.parseInt(esCvePyme));
						*/
						ps.execute();
						ps.close();
					}//if("P".equals(esTipoTasa))
				}//if(!ic_epo.equals(lsCveEpo))
			}//for(int i=0; i < esCadenaTasas.length; i++)

			for(int i=0; i < esCadenaTasas.length; i++) {
				liRegistros = 0;
				lovDatos = new Vector();
				lovDatos = Comunes.explode("|",esCadenaTasas[i]);

				lsCveEpo = lovDatos.get(0).toString();
				lsTasa   = lovDatos.get(1).toString();
				lsPlazo	 = lovDatos.get(2).toString();
				if("R".equals(auxTipoTasa)) {
					lsFechaIni = lovDatos.get(1).toString();
					lsFechaFin = lovDatos.get(3).toString();
				}

				/*
				lsQuery.delete(0, lsQuery.length());
			
				lsQuery.append(" SELECT cpi.ic_cuenta_bancaria");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb");
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme = " + esCvePyme);
				
	

				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT1: "+lsQuery.toString());
				if ( lrsResultado.next()) {
					lsCuentaBancaria = lrsResultado.getString(1);
				} else
                	lsCuentaBancaria = "";

				lrsResultado.close();
				con.cierraStatement();
				*/
				// busca si ya existe un registro para esta pyme
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(" SELECT count(1)");
				lsQuery.append(" FROM com_tasa_pyme_if_mandato");
				lsQuery.append(" WHERE ic_mandante = " + esCvePyme);
				lsQuery.append(" AND ic_if = "+ esCveIf);
				lsQuery.append(" AND ic_epo =" + lsCveEpo);
				lsQuery.append(" AND cc_tipo_tasa = '"+ esTipoTasa + "'");
				lsQuery.append(" AND ic_plazo = "+lsPlazo);
				if(esTipoTasa.equals("N")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 1");
				else if(auxTipoTasa.equals("R"))
					lsQuery.append(" AND ic_orden = 2");
				else if(esTipoTasa.equals("P")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 3");

				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT2: "+lsQuery.toString());
				if ( lrsResultado.next()) {
					liRegistros = lrsResultado.getInt(1);
				}
				lrsResultado.close();
				con.cierraStatement();

				lsFecha = esTipoTasa.equals("P")? "null" : "sysdate";
				if("R".equals(auxTipoTasa)) {
					lsFecha = "to_date('"+lsFechaIni+"', 'dd/mm/yyyy')";
					lsFechaFin = "to_date('"+lsFechaFin+"', 'dd/mm/yyyy')";
				}

	/*********************************/
				if ( esTipoTasa.equals("N")){
					lsTasa = esValorTasas[i];
				}
	/*********************************/

				// NO EXISTE EL REGISTRO POR LO QUE SE INSERTA
				lsQuery.delete(0, lsQuery.length());
				int ordenApp = 1;
				if(auxTipoTasa.equals("R"))
					ordenApp = 2;
				else if(esTipoTasa.equals("P"))
					ordenApp = 3;
				lsQuery.delete(0, lsQuery.length());
				if ( liRegistros == 0) {
					lsQuery.append(" INSERT INTO com_tasa_pyme_if_mandato");
					lsQuery.append(" (ic_mandante, ic_if, ic_epo, cc_tipo_tasa, fn_valor, ");
					lsQuery.append(" df_aplicacion, ic_plazo,cc_acuse, df_aplicacion_fin, ic_orden, ic_moneda) ");
					lsQuery.append(" VALUES("+ esCvePyme +","+ esCveIf +","+ lsCveEpo );
					lsQuery.append(" ,'" +esTipoTasa +"'," + lsTasa + ","+ lsFecha +", "+lsPlazo+",'"+cc_acuse+"', "+lsFechaFin+","+ordenApp+","+esCveMoneda+")");
				} 	// actualiza el Registro
				else {
					lsQuery.append(" UPDATE com_tasa_pyme_if_mandato");
					lsQuery.append(" SET fn_valor = " + lsTasa);
					lsQuery.append(" ,cc_acuse = '" + cc_acuse+"'");
				
					if(auxTipoTasa.equals("R")) {
						lsQuery.append(" ,df_aplicacion = " + lsFecha);						
						lsQuery.append(" ,df_aplicacion_fin = " + lsFechaFin);
					} else
						lsQuery.append(" ,df_aplicacion = " + lsFecha);
						
					lsQuery.append(" WHERE ic_mandante = " + esCvePyme );
					lsQuery.append(" AND ic_if = "+ esCveIf);
					lsQuery.append(" AND ic_epo =" + lsCveEpo);
					lsQuery.append(" AND cc_tipo_tasa = '"+ esTipoTasa + "'");
					lsQuery.append(" AND ic_plazo = "+lsPlazo);
					lsQuery.append(" AND ic_orden = " + ordenApp+"");
				}
				System.out.println("Query GT3: "+lsQuery.toString());
				con.ejecutaSQL(lsQuery.toString());
			} //fin del for
		} catch (Exception e) {
        	lbOK = false;
        	e.printStackTrace();
			System.out.println("Error en guardarTasasMandato: "+e);
			throw new NafinException("SIST0001");
		} finally {
	        con.terminaTransaccion(lbOK);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lbOK;
    }
	 
	 
	 /**
	 * Elimina Tasas Preferenciales y Negociables para Mandato
	 * Created by FVR (19/10/2009)
	 * @throws com.netro.exception.NafinException
	 * @param lstParamTasas lista con datos de las tasas a eliminar
	 */
	 public void eliminarTasasMandato(List lstParamTasas)throws NafinException{
		 AccesoDB con = new AccesoDB();
		 PreparedStatement ps = null;
		 String sentencia = "";
		 String condicion = "";
		 boolean transaccion = true;
		 
		 try{
			 con.conexionDB();
			 sentencia = " delete com_tasa_pyme_if_mandato "+
							 " where cc_tipo_tasa = ? "+
							 " and ic_mandante = ? "+
							 " and ic_if = ? "+
							 " and ic_epo = ? "+
							 " and ic_plazo = ? "+
							 " and ic_orden = ? ";
							 
			if(lstParamTasas!=null && lstParamTasas.size()>0){
				for (int i=0; i<lstParamTasas.size();i++){
					Vector vtasaParam = (Vector)lstParamTasas.get(i);
					String tipo_tasa = (String)vtasaParam.get(0);
					String cuentaBancaria =  (String)vtasaParam.get(1);
					String cveIf  = (String)vtasaParam.get(2);
					String cveEpo = (String)vtasaParam.get(3);
					String plazo  = (String)vtasaParam.get(4);
					String orden  = (String)vtasaParam.get(5);
					
					if("N".equals(tipo_tasa))
						condicion = " and TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, df_aplicacion))  ";
					else if("P".equals(tipo_tasa))
						condicion = "	and TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, SYSDATE)) ";
					
					/*-----------------------------------
					 *Se realiza borrado de tasa por tasa
					 */
					ps = con.queryPrecompilado(sentencia+condicion);
					ps.setString(1,tipo_tasa);
					ps.setInt(2,Integer.parseInt(cuentaBancaria));
					ps.setInt(3,Integer.parseInt(cveIf));
					ps.setInt(4,Integer.parseInt(cveEpo));
					ps.setInt(5,Integer.parseInt(plazo));
					ps.setInt(6,Integer.parseInt(orden));
					
					ps.executeUpdate();
					
					if(ps!=null)ps.close();
				}
			}

		 }catch(Exception e){
			 transaccion = false;
			 //PENDIENTE DE MODIFICACION DE CODIGO PARA IMPLEMENTAR APPEXCEPTION
			 e.printStackTrace();
			 throw new NafinException("SIST0001");
		 }finally{
			 if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			 }
		 }
	 }
	 
	
	
	
	/*************************************************************************************************/
	/*************************PSEUDO DESARROLLO JAJAJA NO SE SI ESTA BIEN*****************************/
	/*************************************************************************************************/
	public ArrayList getMandante(String esCadenaEPO)
	throws NafinException
	{
		StringBuffer lsQuery = new StringBuffer();
		ResultSet rs = null;
		PreparedStatement ps = null;
		AccesoDB con  = new AccesoDB();

		String lsClave 		= "";
		String lsNombre 	= "";
		ArrayList lovRegistro 	= null;
		ArrayList lovDatos 	= new ArrayList();

        try{
        	con.conexionDB();

			lsQuery.delete(0, lsQuery.length());
			lsQuery.append(" SELECT cm.ic_mandante, cm.cg_razon_social ");
			lsQuery.append(" from comcat_mandante cm, comrel_mandante_epo cme ");
			lsQuery.append(" where cm.ic_mandante = cme.ic_mandante ");
			lsQuery.append(" and cme.ic_epo =  ? ");		   	
			lsQuery.append(" order by cm.cg_razon_social");
		   	

			ps = con.queryPrecompilado(lsQuery.toString());
			ps.setInt(1,Integer.parseInt(esCadenaEPO));
			rs = ps.executeQuery();

			while (rs.next()) {
				lsClave = rs.getString(1);
				lsNombre = rs.getString(2);

				lovRegistro = new ArrayList();
				lovRegistro.add(lsClave);
				lovRegistro.add(lsNombre);
				
				lovDatos.add(lovRegistro);
		   }
		   rs.close();
			ps.close();
	   	con.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en getProveedor: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }
	 
	 
	 	/*********************************************************************************************
	*
	*	    Vector procesarTasaPreferencial()
	*
	*********************************************************************************************/
    // Agregado 28/10/2002	--CARP
    public Vector procesarTasaPreferencialMandante(String esCveIf, String esCveMoneda,
    									   String esCadenaEPO, String esCvePyME,
                                           double edPuntos )
		throws NafinException
    {
   		AccesoDB con  = new AccesoDB();

		double ldTotal 			= 0;
        double ldValorTotalTasa = 0;

        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();
        Vector lovTasas 	= null;

        try{
        	con.conexionDB();

			lovTasas = obtenerTasasMandante(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, "", con);

            for ( int i = 0; i < lovTasas.size(); i++) {
            	lovRegistro = (Vector) lovTasas.elementAt(i);

                ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();

                ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;

                lovRegistro.addElement(new Double(ldTotal));

                lovDatos.addElement(lovRegistro);
            }// Fin del for

		} catch (Exception e) {
			System.out.println("Error en procesarTasaPreferencial: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }
	 
	 
	 private Vector obtenerTasasMandante(String esCveIf, String esCveMoneda, String esCadenaEPO,
								String esCvePyME, String esCvePlazo, AccesoDB eoConexionDB)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;

        String lsNombre 	= "";
        String lsCveEpo 	= "";
        String lsNomTasa 	= "";
        String lsValorTasa 	= "";
        String lsRelMatBase = "";
        String lsPuntosTasa = "";
        String lsRelMatClasif = "";
        String lsPuntosClasif = "";
        String lsCalificacion = "";
        String lsCadenaTasa = "";
        String lsTemp 		= "";
		String lsPlazo		= "";

        int liCalificacion 		= 0;
        double ldValorTotalTasa = 0;

        Vector lovRegistro 	= null;
        Vector lovDatos 	= new Vector();

        try{
			lsQuery.append(" SELECT distinct ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  ");
			lsQuery.append(" 	   cmt.fn_valor as ValorTasa, cta.cg_rel_mat as RelMatBase,  cta.fn_puntos as PuntosBase, ");
			lsQuery.append(" 	   rt.ic_calificacion as Clasificacion, rt.cg_rel_mat as RelMatClasif, rt.cg_puntos as PuntosClasif  ");
			lsQuery.append(" 		, to_char(sysdate,'dd/mm/yyyy'), ctb.ic_plazo ");
			lsQuery.append(" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, ");
		/*	lsQuery.append(" 	 ( SELECT distinct ic_epo, max(dc_fecha_tasa) as fecha_tasa, ic_tasa ");
			lsQuery.append("	   FROM comrel_tasa_base  ");
			lsQuery.append(" 	   WHERE ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" 	   AND cs_vobo_nafin = 'S'");
			lsQuery.append(" 	   GROUP BY ic_epo, ic_tasa ) tb,");*/
			lsQuery.append(" 	 ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) mb,");
			lsQuery.append(" 	 com_mant_tasa cmt, comcat_epo ce, comrel_mandante_epo cpe, comrel_tipificacion rt ");
			lsQuery.append(" WHERE cta.ic_if = " + esCveIf);
			lsQuery.append(" AND cta.cs_vobo_if = 'S' ");
			lsQuery.append(" AND cta.cs_vobo_epo = 'S'");
			lsQuery.append(" AND cta.cs_vobo_nafin = 'S'");
			//lsQuery.append(" AND cta.ic_epo = tb.ic_epo");
			lsQuery.append(" AND cta.ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" AND cta.dc_fecha_tasa = ctb.dc_fecha_tasa");
			//lsQuery.append(" AND trunc(ctb.dc_fecha_tasa) = trunc(tb.fecha_tasa)");
			//lsQuery.append(" AND ctb.ic_epo = tb.ic_epo");
			//lsQuery.append(" AND ctb.ic_tasa = tb.ic_tasa");
			lsQuery.append(" AND ctb.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND ctb.ic_epo = ce.ic_epo ");
			lsQuery.append(" AND ctb.cs_tasa_activa = 'S' ");
			lsQuery.append(" AND cpe.ic_epo = ctb.ic_epo");
			lsQuery.append(" AND cpe.ic_mandante = " + esCvePyME);
			lsQuery.append(" AND rt.ic_calificacion = 1 ");
			lsQuery.append(" AND rt.cs_vobo_nafin = 'S'");
			lsQuery.append(" AND rt.dc_fecha_tasa = ctb.dc_fecha_tasa");
			lsQuery.append(" AND ct.ic_moneda = " + esCveMoneda);
			lsQuery.append(" AND ct.ic_tasa = mb.ic_tasa");
			lsQuery.append(" AND cmt.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND trunc(cmt.dc_fecha) = trunc(mb.dc_fecha)");
			lsQuery.append((!esCvePlazo.equals("")?" AND ctb.ic_plazo = "+esCvePlazo:""));
			lrsResultado = eoConexionDB.queryDB(lsQuery.toString());

			//System.out.println("QUERY[[[[[ " + lsQuery);

			while ( lrsResultado.next()) {
				lsCveEpo 		= lrsResultado.getString(1);
				lsNombre 		= lrsResultado.getString(2);
				lsNomTasa 		= lrsResultado.getString(3);
				lsValorTasa 	= lrsResultado.getString(4);
				lsRelMatBase 	= lrsResultado.getString(5);
				lsPuntosTasa 	= lrsResultado.getString(6);
				liCalificacion 	= lrsResultado.getInt(7);
				lsRelMatClasif 	= lrsResultado.getString(8);
				lsPuntosClasif 	= lrsResultado.getString(9);
				lsPlazo			= lrsResultado.getString(11);

				if ( lsValorTasa == null || lsValorTasa.equals(""))
					lsValorTasa = "0";
				if ( lsPuntosTasa == null || lsPuntosTasa.equals(""))
					lsPuntosTasa = "0";
				if ( lsPuntosClasif == null || lsPuntosClasif.equals(""))
					lsPuntosClasif = "0";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;

				lsTemp = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsTemp += lsRelMatClasif + lsPuntosClasif;
				}

                lovRegistro = new Vector();

                lovRegistro.addElement(lsCveEpo);
                lovRegistro.addElement(lsNombre);
                lovRegistro.addElement(lsCalificacion);
                lovRegistro.addElement(lsCadenaTasa);
                lovRegistro.addElement(new Double(ldValorTotalTasa));
                lovRegistro.addElement(lsTemp);
				lovRegistro.addElement(lsPlazo);

                lovDatos.addElement(lovRegistro);
            }// Fin del While
            lrsResultado.close();
            eoConexionDB.cierraStatement();
		} catch (Exception e) {
			System.out.println("Error en obtenerTasas: "+e);
			throw new NafinException("SIST0001");
		} //finally {
        return lovDatos;
		//}
    }
	 
	  public Vector  procesarTasaNegociadaMandante(String esCveIf, String esCveMoneda,
    								    String esCadenaEPO, String esCvePyME, String esCvePlazo)
		throws NafinException
    {
			StringBuffer lsQuery = new StringBuffer();
			ResultSet lrsResultado = null;
			AccesoDB con  = new AccesoDB();
			
			String lsCveEpo 	= "";
			String lsCalificacion = "";
			String lsCadenaTasa = "";
			String lsValorTasaP = "";
			String lsValorTasaN = "";
			String lsTipo 		= "";
			String lsTemp 		= "";
			String lsFechaHoy 	= "";
			String lsPlazo		= "";
			
			int liExiste 	= 0;
			double ldTotal 			= 0;
			double ldValorTotalTasa = 0;
			
			Vector lovTasas 	= null;
			Vector lovRegistro 	= null;
			Vector lovRegistro2	= null;
			Vector lovDatos 	= new Vector();

        try {
        	con.conexionDB();

			lovTasas = obtenerTasasMandante(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, esCvePlazo, con);

            for ( int i = 0; i < lovTasas.size(); i++) {
					lsValorTasaP = lsValorTasaN = lsFechaHoy =  "";
					
					lovRegistro = (Vector) lovTasas.elementAt(i);
					
					lsCveEpo 		= (String) lovRegistro.elementAt(0);
					lsCadenaTasa 	= (String) lovRegistro.elementAt(3);
					ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
					lsTemp 			= (String) lovRegistro.elementAt(5);
					lsPlazo			= lovRegistro.elementAt(6).toString();

					lsQuery.delete(0,lsQuery.length());
					lsQuery.append(" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor,  ");
					lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
					lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
					lsQuery.append(" FROM comcat_mandante cm, comrel_mandante_epo cme, comcat_epo ce, ");
					lsQuery.append(" com_tasa_pyme_if_mandato ctpi");
					lsQuery.append(" WHERE cm.ic_mandante = cme.ic_mandante");
					lsQuery.append(" AND cme.ic_epo = ce.ic_epo");					
					lsQuery.append(" AND ctpi.ic_epo = cme.ic_epo");
					lsQuery.append(" AND ctpi.ic_mandante = cm.ic_mandante");
					lsQuery.append(" AND ctpi.ic_if = " + esCveIf);
					lsQuery.append(" AND ctpi.ic_epo = " + lsCveEpo);
					//lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
					lsQuery.append(" AND ctpi.ic_mandante = " + esCvePyME);
					lsQuery.append(" AND ctpi.ic_plazo = " + lsPlazo);
					lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");
	
					System.out.println("Query 2 = " + lsQuery.toString());
					lrsResultado = con.queryDB(lsQuery.toString());

				while ( lrsResultado.next()) {
					lsTipo = lrsResultado.getString(1);
					liExiste = lrsResultado.getInt(3);
                    lsFechaHoy = lrsResultado.getString(4);

					if ( lsTipo.equals("N") && liExiste == 0)
						lsValorTasaN = lrsResultado.getString(2);
					else
						if ( lsTipo.equals("P"))
							lsValorTasaP = lrsResultado.getString(2);
				}
				lrsResultado.close();
				con.cierraStatement();

				lsCalificacion = (!lsValorTasaP.equals(""))? "PREFERENCIAL" : (String) lovRegistro.elementAt(2);

				if ( !lsValorTasaP.equals("")){
					lsCadenaTasa += "&nbsp;-&nbsp;" + lsValorTasaP;
                    lsTemp += "-"+ lsValorTasaP;
                }

				if ( lsValorTasaP.equals(""))
					lsValorTasaP = "0";
				if ( lsValorTasaP != null || !lsValorTasaP.equals("") )
					ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;

                lovRegistro2 = new Vector();

                lovRegistro2.add(0,lovRegistro.elementAt(0));
                lovRegistro2.add(1,lovRegistro.elementAt(1));
				lovRegistro2.add(2,lsCalificacion);
				lovRegistro2.add(3,lsCadenaTasa);
                lovRegistro2.add(4,new Double(ldTotal));
                lovRegistro2.add(5,lsValorTasaN);
                lovRegistro2.add(6,(lsFechaHoy.equals("")?(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date()):lsFechaHoy));
                lovRegistro2.add(7,lsTemp);
				lovRegistro2.add(8,lsPlazo);

                lovDatos.addElement(lovRegistro2);
            }// Fin del for

		} catch (Exception e) {
			System.out.println("Error en procesarTasaNegociada: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
    return lovDatos;
    }
 
	 /**
	  * 
	  * Procesa la Carga Masiva de Tasas. Lee el archivo y realiza validaciones
	  * y posteriormente lo almacena de manera temporal (comtmp_tasas)
	  *
	  * @param strDirectorio Ruta del archivo
	  * @param esNombreArchivo Nombre del archivo
	  *
	  * @return Clave <tt>ResultadosValidacionTasas</tt> con los resultados
	  *         de la Validacion.
	  *
	  */
	 public ResultadosValidacionTasas procesarCargaMasivaTasas(
			String strDirectorio,
			String esNombreArchivo,
			boolean esTasaNegociada,
			String claveMoneda,
			String claveEPO,
			String claveIF,
			String clavePlazo
		)throws AppException{
	 
			log.info("procesarCargaMasivaTasas(E)");

			ResultadosValidacionTasas 	validacion 		= new ResultadosValidacionTasas();//ResultadosGar();
			AccesoDB 						con				= new AccesoDB();
			PreparedStatement 			ps					= null;
			ResultSet		  				rs 				= null;
			StringBuffer 					qrySentencia	= null;
			int   							NUM_CAMPOS		= 3;
			boolean							bOk				= true;
			
			try{
				
				// VALIDAR LA EXTENSION DEL ARCHIVO
				String extension = esNombreArchivo.substring(esNombreArchivo.length()-3,esNombreArchivo.length());
				if(!"txt".equalsIgnoreCase(extension)){
					validacion.appendErrores("Error: El archivo comprimido no es txt\n");
					// TODO: CODIGO RETURN
				}
 
				// ABRIR ARCHIVO CARGADO
				java.io.File 		lofArchivo 				= new java.io.File(esNombreArchivo);
				BufferedReader 	br 						= new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));
							
				// OBTENER ID DEL PROCESO DE CARGA
				con.conexionDB();
				int idProceso = 0;
				qrySentencia  = new StringBuffer();
				qrySentencia.append(" select seq_comtmp_tasas.nextval as id from dual ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();
				if(rs.next()){
					idProceso = rs.getInt("id");
				}
				rs.close();
				ps.close();
				validacion.setIcProceso(idProceso);

				// INSERTAR REGISTROS LEIDOS EN LA TABLA TEMPORAL COMTMP_TASAS
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"INSERT INTO                "  +
					"	COMTMP_TASAS             "  +
					"	(	                      "  +
					"		IC_NUMERO_PROCESO,    "  +
					"		IC_NUMERO_LINEA,      "  +
					"		CG_RFC,               "  +
					"		CG_PUNTOS_VALOR_TASA, "  +
					"		CG_ESTATUS ,          "  +
					"		CS_TASA_FLOATING      "  +					
					"	)                        "  +
					"  VALUES                   "  +
					"	(                        "  +
					"		?,                    "  +
					"		?,                    "  +
					"		?,                    "  +
					"		?,                    "  +
					"		?,                     "  +
					"		?                     "  +
					"	) ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				int 			lineas			= 0;
				int 			numLinea			= 1;
				String 		lsLinea 			= "";
				String [] 	campos			= null;
				int 			iNumRegTrans 	= 0;
				if(validacion.getErrores() == null || validacion.getErrores().length()==0){
					for (lineas = 0,numLinea = 1; (lsLinea = br.readLine()) != null; lineas++,numLinea++) {
 
						validacion.setErrorLinea(false);
						lsLinea = lsLinea.trim();
						System.out.println("linea["+numLinea+"]: " + lsLinea);	
						// VALIDAR EL NUMERO DE CAMPOS
						try{
	
							campos 		= lsLinea.split("\\|",-1);
              if( campos.length == 0){
              	System.out.println(numLinea+","+(campos.length == 0?"<>":campos[0]));
								validacion.agregaRegistro(numLinea,"");
								validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
								validacion.setErrorLinea(true);
								validacion.incNumRegErr();
								continue;
              } else if( campos.length < NUM_CAMPOS ){
								System.out.println(numLinea+","+campos[0]);
								validacion.agregaRegistro(numLinea,campos[0]);
								validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
								validacion.setErrorLinea(true);
								validacion.incNumRegErr();
								continue;
							}
							
						}catch(Exception e){
							System.out.println(numLinea+","+(campos.length == 0?"":campos[0]));
							e.printStackTrace();
							validacion.agregaRegistro(numLinea,(campos.length == 0?"":campos[0]));
							validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
							validacion.setErrorLinea(true);
							validacion.incNumRegErr();
							continue;
						}
 
						// VALIDAR CAMPO 1. RFC
						boolean 	rfcValido 		= true;
						int 		longMaximaRFC  = 15;
						String 	rfc 				= campos[0];
						validacion.agregaRegistro(numLinea,rfc);
						// Acondicionar cadena
						rfc = (rfc.length() > longMaximaRFC)?rfc.substring(0,longMaximaRFC+1):rfc;
						// Validar Longitud del RFC
						if(rfc.length() > longMaximaRFC){
							validacion.appendErrores(numLinea,"1","Excede la longitud m&aacute;xima permitida.");
							validacion.setErrorLinea(true);
							rfcValido = false;
						}
						// Validar que el RFC tenga el formato correcto
						if(rfcValido){
							 
							if(rfc.length() == 0 ){
								validacion.appendErrores(numLinea,"1","Campo Requerido.");
								validacion.setErrorLinea(true);
								rfcValido = false;
							} else if(rfc.length() < 14 ){
								validacion.appendErrores(numLinea,"1","No posee la longitud m&iacute;nima requerida.");
								validacion.setErrorLinea(true);
								rfcValido = false;
							}else if(rfc.length() == 14 && !Comunes.validaRFC(rfc, 'M')){ // Se trata de una persona Moral
								validacion.appendErrores(numLinea,"1","RFC inv&aacute;lido.");
								validacion.setErrorLinea(true);
								rfcValido = false;
							}else if(rfc.length() == 15 && !Comunes.validaRFC(rfc, 'F')){ // Se trata de una persona fisica
								validacion.appendErrores(numLinea,"1","RFC inv&aacute;lido.");
								validacion.setErrorLinea(true);
								rfcValido = false;
							}
							
						}
						// Validar que el RFC venga en mayusculas
						if(rfcValido && tieneMinusculas(rfc)){
							validacion.appendErrores(numLinea,"1","El RFC debe venir en may&uacute;sculas.");
							validacion.setErrorLinea(true);
							rfcValido = false;
						}	
						// Verificar que el RFC pertenezca a un proveedor
						if(rfcValido && !esProveedor(rfc)){
							validacion.appendErrores(numLinea,"1","El RFC de la PYME no pertenece a un proveedor.");
							validacion.setErrorLinea(true);
							rfcValido = false;
						}
						// Verificar que el RFC cargado exista en la cadena previamente seleccionada
						if(rfcValido && !rfcPymeExisteConlaCadena(rfc,claveEPO,claveMoneda)){
							validacion.appendErrores(numLinea,"1","El RFC de la PYME no existe para la cadena.");
							validacion.setErrorLinea(true);
							rfcValido = false;
						}
						// Verificar que la PYME se encuentre parametrizada con el intermediario financiero
						if(rfcValido && !estaLaPymeParametrizadaConElIF(rfc,claveIF,claveMoneda)){
							validacion.appendErrores(numLinea,"1","La PYME no se encuentra parametrizada con el IF.");
							validacion.setErrorLinea(true);
							rfcValido = false;	
						}
						// Verificar que la PYME se tenga una cuenta parametrizada con el intermediario financiero y la epo especificadas
						if(rfcValido && !tieneCuentaBancaria(claveIF,claveEPO,claveMoneda,rfc)){
							validacion.appendErrores(numLinea,"1","No existe una cuenta bancaria para la pyme.");
							validacion.setErrorLinea(true);
							rfcValido = false;	
						}
 
					//valida si la tasa Opera Oferta de tasas por Montos 
						if(esTasaNegociada==false ) {
							if(tipodeTasas("",rfc)==true){
								validacion.appendErrores(numLinea,"1","La Pyme opera con Oferta de Tasas por Montos, es necesario borrar su parametrizaci�n para poder aplicar Tasa Preferencial.");
								validacion.setErrorLinea(true);
								rfcValido = false;								
							}
						}
						
						// VALIDAR CAMPO 3. ESTATUS
						boolean 	estatusValido 		= true;
						int		longMaximaEstatus = 1;
						String 	estatus 				= campos[2];
						// Acondicionar Cadena
						estatus = (estatus.length() > longMaximaEstatus)?estatus.substring(0,longMaximaEstatus+1):estatus;
						// Validar la longitud del Estatus
						if(estatus.length() > longMaximaEstatus){
							validacion.appendErrores(numLinea,"3","Excede la longitud m&aacute;xima permitida.");
							validacion.setErrorLinea(true);
							estatusValido = false;
						}
						// Este campo debe tener uno de los siguientes valores: "A" y "E"
						if( estatusValido && !"A".equals(estatus) && !"E".equals(estatus) ){
							validacion.appendErrores(numLinea,"3","La clave no es correcta.");
							validacion.setErrorLinea(true);
							estatusValido = false;
						}	
						
						// VALIDAR CAMPO 2. PUNTOS / VALOR TASA
						boolean 	puntosValorTasaValido 			= true;
						int 		longMaximaValorTasa				= 8;
						int 		longMaximaDecimalesValorTasa	= 5;
						int 		longMaximaEnterosValorTasa		= 2;
						boolean	validarPuntosTasa					= true;
						String 	puntosValorTasa 					= campos[1];
						boolean	esPunto								= !esTasaNegociada?true:false;
						boolean  esTasa								= esTasaNegociada ?true:false;
						
						// Acondicionar Cadena
						//if(esPunto){
							//puntosValorTasa = (puntosValorTasa.length() > longMaximaPuntos)?puntosValorTasa.substring(0,longMaximaPuntos+1):puntosValorTasa;
						//}else	if(esTasa){
							//puntosValorTasa = (puntosValorTasa.length() > longMaximaValorTasa)?puntosValorTasa.substring(0,longMaximaValorTasa+1):puntosValorTasa;
						//}
						// No sera obligatorio tener un valor si el estatus (campo 3) es 'E'
						if( "E".equals(estatus) && puntosValorTasa.equals("")){
							validarPuntosTasa = false;
						}	
						// Este campo es requerido cuando el registro venga con estatus "A"
						if( "A".equals(estatus) && puntosValorTasa.trim().equals("")){
							validacion.appendErrores(numLinea,"2","Campo Requerido.");
							validacion.setErrorLinea(true);
							puntosValorTasaValido = false;
						}
						// Validar Tipo y Longitud
						if(validarPuntosTasa && esPunto ){
							// Validar que los puntos sean decimal de 2 enteros y m�ximo 5 decimales
							if(puntosValorTasaValido && !Comunes.esDecimal(puntosValorTasa)){
								validacion.appendErrores(numLinea,"2","No es un n&uacute;mero decimal v&aacute;lido.");
								validacion.setErrorLinea(true);
								puntosValorTasaValido = false;	
							}
							// Validar que los puntos cuenten con la longitud requerida	
							if(puntosValorTasaValido ){
								String []datos =	puntosValorTasa.split("\\.");
                
								if(datos[0].length() > longMaximaEnterosValorTasa){
									validacion.appendErrores(numLinea,"2","Excede la longitud m&aacute;xima de enteros permitida.");
									validacion.setErrorLinea(true);
									puntosValorTasaValido = false;
                  puntosValorTasa = (puntosValorTasa.length() > longMaximaValorTasa)?puntosValorTasa.substring(0,longMaximaValorTasa+1):puntosValorTasa;
								}
                System.out.println("puntosValorTasa: " + puntosValorTasa);
                System.out.println("ldatos.size: " + datos.length);
								if(puntosValorTasaValido && (datos.length > 1) && (datos[1].length() > longMaximaDecimalesValorTasa) ){
									validacion.appendErrores(numLinea,"2","Excede la longitud m&aacute;xima de decimales permitida.");
									validacion.setErrorLinea(true);
									puntosValorTasaValido = false;
                  puntosValorTasa = (puntosValorTasa.length() > longMaximaValorTasa)?puntosValorTasa.substring(0,longMaximaValorTasa+1):puntosValorTasa;
								}
							}
						}else if(validarPuntosTasa && esTasa){
							// Validar que el valor de la Tasa sea decimal
							if(puntosValorTasaValido && !esFlotante(puntosValorTasa)){
								validacion.appendErrores(numLinea,"2","No es un n&uacute;mero decimal v&aacute;lido.");
								validacion.setErrorLinea(true);
								puntosValorTasaValido = false;
							}
							// Validar que el valor de la tasa cuente con la longitud requerida
							if(puntosValorTasaValido){
								String []datos =	puntosValorTasa.split("\\.");
								if(datos[0].length() > longMaximaEnterosValorTasa){
									validacion.appendErrores(numLinea,"2","Excede la longitud m&aacute;xima de enteros permitida.");
									validacion.setErrorLinea(true);
									puntosValorTasaValido = false;
								}
								if(puntosValorTasaValido && (datos.length > 1) && (datos[1].length() > longMaximaDecimalesValorTasa) ){
									validacion.appendErrores(numLinea,"2","Excede la longitud m&aacute;xima de decimales permitida.");
									validacion.setErrorLinea(true);
									puntosValorTasaValido = false;
								}
							}
						}	
						if(puntosValorTasa.length() > 9){
							validacion.appendErrores(numLinea,"1","Excede la longitud m&aacute;xima permitida.");
							validacion.setErrorLinea(true);
							puntosValorTasaValido = false;
							
						}
						// Si el Tipo de Tasa es Negociada esta no debera ser mayor al valor de la tasa actual �como obtener la tasa actual?
						if(validarPuntosTasa && puntosValorTasaValido && esTasaNegociada && rfcValido){
							
							double dblTasaActual 		= getValorTasaActual( claveIF, claveMoneda, claveEPO, rfc, clavePlazo, "" );
							double dblPuntosValorTasa 	= Double.parseDouble(puntosValorTasa);
							System.err.println("-----> dblTasaActual = <"+dblTasaActual+">");// Debug info
							if(dblPuntosValorTasa > dblTasaActual){
								validacion.appendErrores(numLinea,"2","La tasa no debe ser mayor al Valor de la Tasa Actual.");
								validacion.setErrorLinea(true);
								puntosValorTasaValido = false;
							}
							
						}						
						
						//System.err.println("-----> esTasaNegociada = <"+esTasaNegociada+">");// Debug info		
						
						//*******************valida tipo Tasa solo cuando es prefetencial
						String 	tipoTasa		=  "";						
						if(campos.length>3){											
							boolean 	estValidoPreFloating 		= true;	
							tipoTasa		= campos[3];								
							if(esTasaNegociada==false && !"".equals(tipoTasa) )  {
								
									if(  !"P-F".equals(tipoTasa)  &&  !"P".equals(tipoTasa)  ){									
										validacion.appendErrores(numLinea,"4","La clave no es correcta.");
										validacion.setErrorLinea(true);
										estValidoPreFloating = false;
									}
									
									if("P-F".equals(tipoTasa) )  {	
										
										String  operaPYMEFloating =  suceptibleFloating("",campos[0] );								
										if (estValidoPreFloating  && !"S".equals(operaPYMEFloating)  ) {
											validacion.appendErrores(numLinea,"4","La PYME no es suceptible a Floating.");
											validacion.setErrorLinea(true);
											estValidoPreFloating=false;		
										}
										
										if( estValidoPreFloating &&  tipoTasa.length() >3 ){
											validacion.appendErrores(numLinea,"4","Excede la longitud m&aacute;xima permitida.");
											tipoTasa  = 	tipoTasa.substring(0, 3);
											validacion.setErrorLinea(true);
											estValidoPreFloating=false;	   								
										}
									}									
									
								}							
						}
						
						System.err.println("-----> tipoTasa = <"+tipoTasa+">");// Debug info	
						
						if( esTasaNegociada==false  && "".equals(tipoTasa) )  {
							tipoTasa		=  "P";
						}else  if( esTasaNegociada==true && "".equals(tipoTasa) )  {
							tipoTasa		=  "N";
						}
										
						
						// ACTUALIZAR CONTEO DE REGISTROS CON Y SIN ERROR
						if(validacion.getErrorLinea()){
							validacion.incNumRegErr();
						}else{
							validacion.incNumRegOk();
						}
						System.err.println("-----> rfcValido = <"+rfcValido+">");// Debug info
						System.err.println("-----> puntosValorTasaValido = <"+puntosValorTasaValido+">");// Debug info
						System.err.println("-----> estatusValido = <"+estatusValido+">");// Debug info
						// INSERTAR EN LA TABLA TEMPORAL EL CONTENIDO DEL REGISTRO	
						if(rfcValido && puntosValorTasaValido && estatusValido  ) {
							ps.setInt(1,    idProceso);
							ps.setInt(2,    numLinea);
							ps.setString(3, rfc);
							ps.setString(4, puntosValorTasa);
							ps.setString(5, estatus);
							ps.setString(6, tipoTasa);   // Tasa Preferencial-Floating 							
							ps.execute();
							if(iNumRegTrans>=3500){
								con.terminaTransaccion(true);
								iNumRegTrans = 0;
								System.gc();	//Invocacion al garbage collector para "liberar" memoria
							}
						}
						
						iNumRegTrans++;
						
					} // for
				}
				con.terminaTransaccion(true);
				ps.close();

				// BUSCAR REGISTROS REPETIDOS
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"SELECT 										"  + 
					"	RFC 										"  +
					"FROM 										"  +
					"  (											"  +
					"		SELECT 								"  + 
					"			CG_RFC RFC, 					"  +
					"			COUNT(1) REPETICIONES 		"  +
					"		FROM 									"  +
					"			COMTMP_TASAS 					"  +
					"		WHERE  								"  +
					"			IC_NUMERO_PROCESO = ? 		"  +
					"		GROUP BY  							"  +
					"			IC_NUMERO_PROCESO,CG_RFC 	"  +
					" ) A 										"  +
					" WHERE  									"  +
					"	A.REPETICIONES > 1					");
				System.out.println(qrySentencia);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,idProceso);
				rs = ps.executeQuery();
				while(rs.next()){
					validacion.appendErrorRFCRepetido(rs.getString("RFC")," EL RFC esta Repetido.");
				}
				rs.close();
				ps.close();

			}catch(Exception e){
				log.error("procesarCargaMasivaTasas(Exception)");
				e.printStackTrace();
				bOk = false;
				throw new AppException("Error en la Validacion de la Carga Masiva de las Tasas");
			}finally{
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(bOk);
					con.cierraConexionDB();
				}
				log.info("procesarCargaMasivaTasas(S)");
			}
			return validacion;
	}
	
	private boolean tieneMinusculas(String cadena){
		
			if(cadena == null || cadena.trim().equals("")) return false;
			
			String letras = "abcdefghijklmn�opqrstuvwxyz������";
			
			for(int i=0;i<cadena.length();i++){
				char c = cadena.charAt(i);
				if(letras.indexOf(c) != -1){
					return true;
				}
			}
			return false;
	}
		 
	private boolean esProveedor(String rfc){
 
		log.info("esProveedor(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					proveedorValido	= false;
		String 					valor					= null; 
		
		if(rfc == null || rfc.trim().equals("")){
			log.info("esProveedor(S)");
			return proveedorValido;
		}	
		
		try {
			
			con.conexionDB();
			
			query.append(
				"SELECT 														"  + 
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 	"  +
				"FROM 														"  + 
				"	COMCAT_PYME 											"  +
				"WHERE 														"  + 
				"	CS_INVALIDO 	= ?  AND 							"  +
				"	CG_RFC        	= ?		                		" 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "N");
			ps.setString(2, rfc);				
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 				= rs.getString("EXISTE");
				proveedorValido 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("esProveedor(Exception)");
			log.debug("esProveedor.rfc    = <"+rfc+">");
			e.printStackTrace();
			throw new AppException("Error al validar RFC de Proveedor");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("esProveedor(S)");
		}
		
		return proveedorValido;
	}
	
	private boolean rfcPymeExisteConlaCadena(String rfc,String claveEPO, String claveMoneda){

		log.info("rfcPymeExisteConlaCadena(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					existeRelacion		= false;
		String 					valor					= null; 
 
		if(rfc 			== null || rfc.trim().equals("")) 			{ log.info("rfcPymeExisteConlaCadena(S)"); return existeRelacion; }
		if(claveEPO 	== null || claveEPO.trim().equals("")) 	{ log.info("rfcPymeExisteConlaCadena(S)"); return existeRelacion; }
		if(claveMoneda == null || claveMoneda.trim().equals("")) { log.info("rfcPymeExisteConlaCadena(S)"); return existeRelacion; }
		
		try {
			
			con.conexionDB();
			
			query.append(
				"SELECT 																	"  + 
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 				"  + 
				"FROM 																	"  +
				"  COMREL_CUENTA_BANCARIA  CTA, 									"  +
				"  COMCAT_PYME             PYME, 								"  +
				"  COMREL_PYME_IF          REL 									"  +
				"WHERE  																	"  +
				"  CTA.CS_BORRADO    = ?          	AND 						"  + // 'N'
				"  CTA.IC_MONEDA     = ?           	AND 						"  + // claveMoneda
				"  CTA.IC_PYME       = PYME.IC_PYME AND						"  + 
				"  PYME.CS_INVALIDO  = ?          	AND 						"  + // 'N'
				"  PYME.CG_RFC       = ?           	AND 						"  + // rfc
				"  REL.IC_CUENTA_BANCARIA = CTA.IC_CUENTA_BANCARIA AND 	"  + 
				"  REL.IC_EPO        = ?           	AND 						"  + // claveEPO
				"  REL.CS_VOBO_IF    = ?          	AND 						"  + // 'S'
				"  REL.CS_BORRADO    = ? 											"    // 'N' 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "N");
			ps.setString(2, claveMoneda);
			ps.setString(3, "N");
			ps.setString(4, rfc);
			ps.setString(5, claveEPO);
			ps.setString(6, "S");
			ps.setString(7, "N");
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 				= rs.getString("EXISTE");
				existeRelacion 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("rfcPymeExisteConlaCadena(Exception)");
			log.debug("rfcPymeExisteConlaCadena.rfc    		= <"+rfc+">");
			log.debug("rfcPymeExisteConlaCadena.claveEPO    = <"+claveEPO+">");
			log.debug("rfcPymeExisteConlaCadena.claveMoneda = <"+claveMoneda+">");
			e.printStackTrace();
			throw new AppException("Error al revisar si la PYME trabaja con la Cadena Seleccionada");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("rfcPymeExisteConlaCadena(S)");
		}
		return existeRelacion;
	}
	
	private boolean tieneCuentaBancaria(String claveIF,String claveEpo,String claveMoneda,String rfcPyme){
		
		log.info("tieneCuentaBancaria(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					tieneCuenta			= false;
		String 					valor					= null; 
 
		if(rfcPyme 			== null || rfcPyme.trim().equals("")) 		{ log.info("tieneCuentaBancaria(S)"); return tieneCuenta; }
		if(claveIF 			== null || claveIF.trim().equals("")) 		{ log.info("tieneCuentaBancaria(S)"); return tieneCuenta; }
		if(claveEpo 		== null || claveEpo.trim().equals("")) 	{ log.info("tieneCuentaBancaria(S)"); return tieneCuenta; }
		if(claveMoneda 	== null || claveMoneda.trim().equals("")) { log.info("tieneCuentaBancaria(S)"); return tieneCuenta; }
		
		try {
			
			con.conexionDB();
			
			query.append(
				" SELECT                                           "  +
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 	   "  + 
				" FROM                                             "  +
				"	COMREL_PYME_IF         CPI,                     "  +
				"	COMREL_CUENTA_BANCARIA CCB                      "  +
				" WHERE                                            "  +
				"	CPI.IC_CUENTA_BANCARIA = CCB.IC_CUENTA_BANCARIA "  +
				" AND CPI.CS_VOBO_IF  = ? "  +
				" AND CPI.CS_BORRADO  = ? "  +
				" AND CCB.CS_BORRADO  = ? "  +
				" AND CPI.IC_IF       = ? "  + 
				" AND CPI.IC_EPO      = ? "  + 
				" AND CCB.IC_MONEDA   = ? "  + 
				" AND CCB.IC_PYME IN ( SELECT IC_PYME FROM COMCAT_PYME WHERE CS_INVALIDO = ? AND CG_RFC = ? ) "
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "S");
			ps.setString(2, "N");
			ps.setString(3, "N");
			ps.setString(4, claveIF);
			ps.setString(5, claveEpo);
			ps.setString(6, claveMoneda);
			ps.setString(7, "N");
			ps.setString(8, rfcPyme);
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 			= rs.getString("EXISTE");
				tieneCuenta 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("tieneCuentaBancaria(Exception)");
			log.debug("tieneCuentaBancaria.rfcPyme     = <"+rfcPyme+">");
			log.debug("tieneCuentaBancaria.claveIF     = <"+claveIF+">");
			log.debug("tieneCuentaBancaria.claveEpo    = <"+claveEpo+">");
			log.debug("tieneCuentaBancaria.claveMoneda = <"+claveMoneda+">");
			e.printStackTrace();
			throw new AppException("Error al revisar si la PYME tiene parametrizada una cuenta bancaria con el if y epo indicados");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("tieneCuentaBancaria(S)");
		}
		return tieneCuenta;
	}
 
	private boolean estaLaPymeParametrizadaConElIF(String rfc, String claveIF, String claveMoneda){

		log.info("estaLaPymeParametrizadaConElIF(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					existeRelacion		= false;
		String 					valor					= null; 
 
		if(rfc 			== null || rfc.trim().equals("")) 			{ log.info("estaLaPymeParametrizadaConElIF(S)"); return existeRelacion; }
		if(claveIF 		== null || claveIF.trim().equals("")) 		{ log.info("estaLaPymeParametrizadaConElIF(S)"); return existeRelacion; }
		if(claveMoneda == null || claveMoneda.trim().equals("")) { log.info("estaLaPymeParametrizadaConElIF(S)"); return existeRelacion; }
		
		try {
			
			con.conexionDB();
			
			query.append(
				"SELECT 																	"  + 
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 				"  + 
				"FROM 																	"  +
				"  COMREL_CUENTA_BANCARIA  CTA, 									"  +
				"  COMCAT_PYME             PYME, 								"  +
				"  COMREL_PYME_IF          REL 									"  +
				"WHERE  																	"  +
				"  CTA.CS_BORRADO    = ?          	AND 						"  + // 'N'
				"  CTA.IC_MONEDA     = ?           	AND 						"  + // claveMoneda
				"  CTA.IC_PYME       = PYME.IC_PYME AND						"  + 
				"  PYME.CS_INVALIDO  = ?          	AND 						"  + // 'N'
				"  PYME.CG_RFC       = ?           	AND 						"  + // rfc
				"  REL.IC_CUENTA_BANCARIA = CTA.IC_CUENTA_BANCARIA AND 	"  + 
				"  REL.IC_IF        	= ?           	AND 						"  + // claveIF
				"  REL.CS_VOBO_IF    = ?          	AND 						"  + // 'S'
				"  REL.CS_BORRADO    = ? 											"    // 'N' 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "N");
			ps.setString(2, claveMoneda);
			ps.setString(3, "N");
			ps.setString(4, rfc);
			ps.setString(5, claveIF);
			ps.setString(6, "S");
			ps.setString(7, "N");
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 				= rs.getString("EXISTE");
				existeRelacion 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("estaLaPymeParametrizadaConElIF(Exception)");
			log.debug("estaLaPymeParametrizadaConElIF.rfc    		= <"+rfc+">");
			log.debug("estaLaPymeParametrizadaConElIF.claveIF    	= <"+claveIF+">");
			log.debug("estaLaPymeParametrizadaConElIF.claveMoneda = <"+claveMoneda+">");
			e.printStackTrace();
			throw new AppException("Error al revisar si la PYME esta parametrizada con un IF en especifico");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("estaLaPymeParametrizadaConElIF(S)");
		}
		return existeRelacion;
	}
 	
	public ArrayList getRegistrosValidos(ArrayList lineas, int idProceso) throws AppException{
			
		log.info("getRegistrosValidos(E)");
		
		AccesoDB 				con				= new AccesoDB();
    	PreparedStatement 	ps 				= null;
    	ResultSet 				rs 				= null;
		StringBuffer        	query 			= new StringBuffer();
		StringBuffer        	variablesBind 	= new StringBuffer();
		
		ArrayList				registros		= new ArrayList();
		
		if(lineas == null || lineas.size() == 0){
			log.info("getRegistrosValidos(S)");
			return registros;
		}	
		
		try {
			
			con.conexionDB();
			
			for(int i=0;i<lineas.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
			
			query.append(
				"SELECT                        							"  + 
				"  IC_NUMERO_LINEA,            							"  +
				"  CG_RFC,                     							"  +
				"  CG_PUNTOS_VALOR_TASA,       							"  +
				"  CG_ESTATUS                  							"  +
				"FROM                          							"  + 
				"  COMTMP_TASAS                                 	"  +
				"WHERE                                          	"  + 
				"  IC_NUMERO_PROCESO        =   ?               	"  +
				"  AND IC_NUMERO_LINEA      IN ("+variablesBind.toString()+")	"  +
				"ORDER BY IC_NUMERO_LINEA      							"		
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,idProceso);
			for(int i=0;i<lineas.size();i++){
				ps.setString(i+2, (String)lineas.get(i));	
			}			
			rs = ps.executeQuery();
 
			while(rs != null && rs.next()){
				
				HashMap registro = new HashMap();
				String valor = rs.getString("IC_NUMERO_LINEA");
				registro.put("LINEA", (valor == null)?"":valor);
				valor = rs.getString("CG_RFC");
				registro.put("RFC", 				(valor == null)?"":valor);
				valor = rs.getString("CG_PUNTOS_VALOR_TASA");
				registro.put("PUNTOS_TASA", 	(valor == null)?"":valor);
				valor = rs.getString("CG_ESTATUS");
				registro.put("ESTATUS", 		(valor == null)?"":valor);
				
				registros.add(registro);
				
			}
			
		}catch(Exception e){
			log.debug("getRegistrosValidos(Exception)");
			log.debug("getRegistrosValidos.lineas    = <"+lineas+">");
			log.debug("getRegistrosValidos.idProceso = <"+idProceso+">");
			e.printStackTrace();
			throw new AppException("getRegistrosValidos(Exception)");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getRegistrosValidos(S)");
		}
				
		return registros;	
	}
	
	private boolean esFlotante(String campo){
		boolean flotanteValido = true;
		try{
			double numero = Double.parseDouble(campo);
			if(numero < 0){
				flotanteValido = false;
			}
		}catch(Exception e){
			flotanteValido = false;
		}
		return flotanteValido;
	}
	
	/**
	 * Obtiene el Valor de la Tasa Actual
	 * 
	 * @param esCveIf 			Clave del IF
	 * @param esCveMoneda   	Clave de la Moneda 
	 * @param esCadenaEPO   	Clave de la EPO
	 * @param esRFCPyme 			RFC del Proveedor
	 * @param esCvePlazo 		Clave del Plazo
	 * @param tipo_factoraje   Clave Con el Tipo de Factoraje
	 *
	 * @return <tt>double</tt> con el valor de la Tasa solicitada
	 *
	 */
	private double getValorTasaActual(
			String esCveIf, 
			String esCveMoneda,
			String esCadenaEPO, 
			String esRFCPyme, 
			String esCvePlazo, 
			String tipo_factoraje
	)throws AppException {
	
		  log.info("getValorTasaActual(E)");
	
		  if(esRFCPyme == null || esRFCPyme.trim().equals("")){
			  log.info("getValorTasaActual(S)");
			  return 0.00;
		  }
	
		  StringBuffer 		lsQuery 			= new StringBuffer();		  
		  PreparedStatement 	ps 				= null;
		  ResultSet 			rs 				= null;
		  AccesoDB 				con  				= new AccesoDB();

        String lsCveEpo 			= "";
		  String esCvePyME			= "0";
        //String lsCalificacion 	= "";
        //String lsCadenaTasa 		= "";
		  String lsValorTasaP 		= "";
        String lsValorTasaN 		= "";
        String lsTipo 				= "";
        //String lsTemp 				= "";
        //String lsFechaHoy 			= "";
		  String lsPlazo				= "";
		  //String lsDescripPlazo 	= "";
		  //String lsIc_Tasa 			= "";

        int 	liExiste 			= 0;
		  double ldTotal 				= 0;
        double ldValorTotalTasa 	= 0;

        Vector lovTasas 			= null;
        Vector lovRegistro 		= null;
        //Vector lovRegistro2		= null;
        //Vector lovDatos 			= new Vector();

        try {
			  
        	con.conexionDB();

			lsQuery.delete(0,lsQuery.length());
			lsQuery.append(
				"SELECT 							"  + 
				"	IC_PYME AS CLAVE_PYME 	"  +
				"FROM 							"  + 
				"	COMCAT_PYME 				"  +
				"WHERE 							"  + 
				"	CS_INVALIDO 	= ?  AND "  +
				"	CG_RFC        	= ?      " 
			);
			ps = con.queryPrecompilado(lsQuery.toString());
			ps.setString(1, "N");
			ps.setString(2, esRFCPyme);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()) {
				esCvePyME 				= rs.getString("CLAVE_PYME");
				esCvePyME 				= (esCvePyME == null || esCvePyME.trim().equals(""))?"0":esCvePyME;	
			}
			rs.close();
			ps.close();
			
			lovTasas = obtenerTasas(esCveIf, esCveMoneda, esCadenaEPO, esCvePyME, esCvePlazo);

			if(lovTasas != null && lovTasas.size() > 0 ){
				int i					=0;
				lsValorTasaP 		= lsValorTasaN = "";//lsFechaHoy =  "";
	
				lovRegistro 		= (Vector) 	lovTasas.elementAt(i);
	
				lsCveEpo 			= (String) 	lovRegistro.elementAt(0);
				//lsCadenaTasa 		= (String) 	lovRegistro.elementAt(3);
				ldValorTotalTasa 	= ((Double) lovRegistro.elementAt(4)).doubleValue();
				//lsTemp 				= (String) 	lovRegistro.elementAt(5);
				lsPlazo				= lovRegistro.elementAt(6).toString();
				//lsDescripPlazo 	= lovRegistro.elementAt(7).toString();
				//lsIc_Tasa 			= lovRegistro.elementAt(8).toString();
	
				lsQuery.delete(0,lsQuery.length());
				lsQuery.append(" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor,  ");
				lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
				lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,");
				//FODEA 046-2009 FVR -INI
				if(!"M".equals(tipo_factoraje))
					lsQuery.append(" com_tasa_pyme_if ctpi");
				else
					lsQuery.append(" com_tasa_pyme_if_mandato ctpi");
				//FODEA 046-2009 FVR -FIN
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_cuenta_bancaria   = ctpi.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_if      = ctpi.ic_if");
				lsQuery.append(" AND cpi.ic_epo     = ctpi.ic_epo");
				lsQuery.append(" AND cpi.cs_vobo_if =  ? ");
				lsQuery.append(" AND cpi.cs_borrado =  ? ");
				lsQuery.append(" AND ccb.cs_borrado =  ? ");
				lsQuery.append(" AND cpi.ic_if      =  ? ");
				lsQuery.append(" AND cpi.ic_epo     =  ? ");
				lsQuery.append(" AND ccb.ic_moneda  =  ? ");
				lsQuery.append(" AND ccb.ic_pyme    =  ? ");
				lsQuery.append(" AND ctpi.ic_plazo  =  ? ");
				lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");
	
				System.out.println("Query 2 = " + lsQuery.toString());
				ps = con.queryPrecompilado(lsQuery.toString());
				ps.setString(1, "S");
				ps.setString(2, "N");
				ps.setString(3, "N");
				ps.setString(4, esCveIf);
				ps.setString(5, lsCveEpo);
				ps.setString(6, esCveMoneda);
				ps.setString(7, esCvePyME);
				ps.setString(8, lsPlazo);
				rs = ps.executeQuery();
				
				while( rs != null && rs.next()) {
					lsTipo 		= rs.getString(1);
					liExiste 	= rs.getInt(3);
					//lsFechaHoy 	= rs.getString(4);
	
					if ( "N".equals(lsTipo) && liExiste == 0 )
						lsValorTasaN = rs.getString(2);
					else if ( "P".equals(lsTipo) )
						lsValorTasaP = rs.getString(2);
				}
				rs.close();
				ps.close();
	
				// lsCalificacion = (!lsValorTasaP.equals(""))? "PREFERENCIAL" : (String) lovRegistro.elementAt(2);
	
				// if ( !lsValorTasaP.equals("")){
				//		lsCadenaTasa 	+= "&nbsp;-&nbsp;" + lsValorTasaP;
				//  	lsTemp 			+= "-"+ lsValorTasaP;
				// }
	
				if ( lsValorTasaP.equals("")){
					lsValorTasaP 	= "0";
				}
				if ( lsValorTasaP != null || !lsValorTasaP.equals("") ){
					ldTotal 			= (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;
				}	
         } 
		} catch (Exception e) {
			log.error("getValorTasaActual(Exception)");
			log.error("getValorTasaActual.esCveIf        = <"+esCveIf       +">"); 
			log.error("getValorTasaActual.esCveMoneda    = <"+esCveMoneda   +">");
			log.error("getValorTasaActual.esCadenaEPO    = <"+esCadenaEPO   +">");
			log.error("getValorTasaActual.esRFCPyme      = <"+esRFCPyme     +">");
			log.error("getValorTasaActual.esCvePlazo     = <"+esCvePlazo    +">");
			log.error("getValorTasaActual.tipo_factoraje = <"+tipo_factoraje+">");
			e.printStackTrace();
			throw new AppException("Error al obtener valor de la tasa Actual para el proveedor cuyo RFC es: "+esRFCPyme);
		} finally {
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();  
			log.info("getValorTasaActual(S)");
		}
		return ldTotal;
    }
	
	/**
	 * 
	 * @throws com.netro.exception.NafinException
	 * @param lstParamTasas
	 */
 	 public String eliminarTasas(String esCveIf, 
														   String esCveMoneda,
    						               String esCvePyme, 
															 String esTipoTasa,
                               String[] esCadenaTasas, 
															 String[] esValorTasas,
							                 String nombreUsuario,
															 String lsPuntos,
															 String ldTotal)
	 throws NafinException{
	
	 System.out.println("eliminarTasas Inicia");
	
	System.out.println("esCveIf  "+esCveIf);
	System.out.println("esCveMoneda  "+esCveMoneda);
	System.out.println("esCvePyme  "+esCvePyme);
	System.out.println("esTipoTasa  "+esTipoTasa);
	System.out.println("esCadenaTasas  "+ esCadenaTasas.length);
	System.out.println("nombreUsuario  "+nombreUsuario);
	System.out.println("lsPuntos  "+lsPuntos);
	System.out.println("ldTotal  "+ldTotal);
	
		 AccesoDB con = new AccesoDB();
		 PreparedStatement ps = null;
		 boolean transaccion = true;
		 String lsNoTasa ="";
		 int liRegistros = 0;
		 Vector lovDatos 	= new Vector();
		 String valor ="";
		 StringBuffer lsQuery2 = new StringBuffer();
		 String lsTasa ="";
		 String lsCveEpo ="";
		 StringBuffer lsQuery = new StringBuffer();
		 ResultSet lrsResultado = null;
		 String lsCuentaBancaria ="";
		 String lsPlazo ="";
		 int ordenApp = 1;
		 String lsTasaP ="";
		  
		 if (lsPuntos.equals("")){
		 lsPuntos ="0";
		 }
		 ResultSet rs1 = null;
		PreparedStatement ps1 = null;
		int seqNEXTVAL	= 0;
		
		 try{
			 con.conexionDB();
			
				 
			
			lsQuery.append("SELECT SEQ_Bit_Tasas_Preferenciales.nextval FROM dual");
			ps1 = con.queryPrecompilado(lsQuery.toString());
			rs1 = ps1.executeQuery();
				if(rs1.next())
				{
					seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
				}
			ps1.close();
   		rs1.close();
	
			log.debug("seqNEXTVAL--->"+lsQuery.toString());		
			
			for(int i=0; i < esCadenaTasas.length; i++) {
				liRegistros = 0;
				lovDatos = new Vector();
				lovDatos = Comunes.explode("|",esCadenaTasas[i]);		
				lsCveEpo = lovDatos.get(0).toString();
				lsPlazo	 = lovDatos.get(2).toString();
				lsNoTasa =  lovDatos.get(3).toString();
				lsTasaP =  lovDatos.get(4).toString();
				
				log.debug("lsTasa--->"+lsTasaP);
				
				if ( esTipoTasa.equals("N")){
					lsTasa = esValorTasas[i];
				}
				if ( esTipoTasa.equals("P")){
					lsTasa = lsTasaP;					
				}
				
				if ( lsTasa== null || lsTasa.equals("")){
					lsTasa = "0";
				}
				
				
				
				if(esTipoTasa.equals("R"))
					ordenApp = 2;
				else if(esTipoTasa.equals("P"))
					ordenApp = 3;
				
			
					if("P".equals(esTipoTasa) || "N".equals(esTipoTasa) ) {
						lsQuery.delete(0, lsQuery.length());
						
					}//if("P".equals(esTipoTasa))
				
				
				
				lsQuery = new StringBuffer();
				 
				lsQuery.append(" SELECT cpi.ic_cuenta_bancaria");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb");
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme = " + esCvePyme);

				log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
				
				
				if ( lrsResultado.next()) {
					lsCuentaBancaria = lrsResultado.getString(1);
				} else
         lsCuentaBancaria = "";

				lrsResultado.close();
				con.cierraStatement();
				
		
					lsQuery2 = new StringBuffer();
				 
						String estatus= "E"; //Elimiando
											
					lsQuery2.append("	insert into Bit_Tasas_Preferenciales  "+
													 " (ic_Bitacora, ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
													" fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus) "+	
													" VALUES("+seqNEXTVAL+","+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
													esTipoTasa+"',"+esCveMoneda +","+	lsPuntos+","+
													lsTasa +",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"'"+")");
													
						System.out.println("lsQuery2::: --->  "+lsQuery2);
			
					ps = con.queryPrecompilado(lsQuery2.toString());
					ps.executeUpdate();
					ps.close();
				
				
				lsQuery = new StringBuffer();
				lsQuery.append(
							" DELETE"   +
							"   FROM com_tasa_pyme_if"   +
							"  WHERE ic_epo =  "+lsCveEpo+  
							"    AND ic_if =  "+esCveIf+   
							"    AND cc_tipo_tasa = '"+esTipoTasa+"' "+
							"    AND IC_PLAZO = "+lsPlazo+
							"    AND IC_ORDEN = "+ordenApp+
							"    AND ic_cuenta_bancaria IN ( "+lsCuentaBancaria+" )");
					
					 log.debug("lsQuery--->"+lsQuery.toString());
						
						ps = con.queryPrecompilado(lsQuery.toString());
						ps.execute();
						ps.close();
						
				valor="E";
				
			}//se termina el for
			System.out.println("valor:::   "+valor);

		 }catch(Exception e){
			 transaccion = false;
				 e.printStackTrace();
			 throw new NafinException("SIST0001");
		 }finally{
			 if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			 }
		 }return valor;
	 }
	/**
	 *Fodea 036-2010 
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param esCadenaEPO
	 * @param esCveMoneda
	 */
	 public Vector getIntermediario(String esCveMoneda, String esCadenaEPO)
		throws NafinException
    {
    	System.out.println(" getIntermediario: Inicia");
				
			StringBuffer lsQuery = new StringBuffer();
      ResultSet lrsResultado = null;
   		AccesoDB con  = new AccesoDB();

      String lsClave 		= "";
      String lsNombre 	= "";
      Vector lovRegistro 	= null;
      Vector lovDatos 	= new Vector();

        try{
        	con.conexionDB();
			
			lsQuery.append(" SELECT distinct  cpi.ic_if, i.cg_razon_social ");
      lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_if i");
		//	lsQuery.append(" ,com_tasa_pyme_if p");
		  lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria ");
		//lsQuery.append(" and  p.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria ");		
			lsQuery.append(" AND cpi.ic_if = i.ic_if ");
			lsQuery.append(" AND ic_moneda = " + esCveMoneda);
			lsQuery.append(" AND cpi.ic_epo in ("+ esCadenaEPO +")");
			lsQuery.append(" AND cpi.cs_vobo_if = 'S' ");
			lsQuery.append(" AND cpi.cs_borrado = 'N'  ");
			lsQuery.append(" AND ccb.cs_borrado = 'N' ");
			lsQuery.append(" ORDER BY i.cg_razon_social  ");
 	
			System.out.println("lsQuery.toString():::"+lsQuery.toString());

			lrsResultado = con.queryDB(lsQuery.toString());

			while (lrsResultado.next()) {
				lsClave = lrsResultado.getString(1);
				lsNombre = lrsResultado.getString(2);

                lovRegistro = new Vector();
                lovRegistro.addElement(lsClave);
                lovRegistro.addElement(lsNombre);

                lovDatos.addElement(lovRegistro);
		   }
		   lrsResultado.close();
	   	   con.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en getIntermediario: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}            
		}
			System.out.println(" getIntermediario: Termina");
    return lovDatos;
    }

  /**
	 * Fodea 036-2010
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param edPuntos
	 * @param lsPlazo
	 * @param lsPuntos
	 * @param lsProveedor
	 * @param lsIF
	 * @param lsMoneda
	 * @param lsNoEpo
	 * @param lsTipoTasa
	 */
	 public Vector obtenerTasasNafin( String lsTipoTasa, String lsNoEpo, String lsMoneda,  String lsIF, String lsProveedor, String lsPuntos, String lsPlazo, double edPuntos, String tipoTasa)	
	 throws NafinException {

		log.info("obtenerTasasNafin Incia ");
			
		log.debug("obtenerTasasNafin.lsTipoTasa  = <" + lsTipoTasa  + ">");
		log.debug("obtenerTasasNafin.lsNoEpo     = <" + lsNoEpo     + ">");
		log.debug("obtenerTasasNafin.lsMoneda    = <" + lsMoneda    + ">");
		log.debug("obtenerTasasNafin.lsIF        = <" + lsIF        + ">");
		log.debug("obtenerTasasNafin.lsProveedor = <" + lsProveedor + ">");
		log.debug("obtenerTasasNafin.lsPuntos    = <" + lsPuntos    + ">");
		log.debug("obtenerTasasNafin.lsPlazo     = <" + lsPlazo     + ">");
		log.debug("obtenerTasasNafin.edPuntos     = <" + edPuntos     + ">");
		log.debug("obtenerTasasNafin.tipoTasa     = <" + tipoTasa     + ">");

      String lsNombre 			= "";
      String lsCveEpo 			= "";
      String lsNomTasa 			= "";
      String lsValorTasa 		= "";
      String lsRelMatBase 		= "";
      String lsPuntosTasa 		= "";
      String lsRelMatClasif 	= "";
      String lsPuntosClasif 	= "";
      String lsCalificacion 	= "";
      String lsCadenaTasa 		= "";
      String lsTemp 				= "";
      String lsDescripPlazo 	= "";
      String lsIc_Tasa 			= "";
      String lsFecha 			= "";
      String lsNuValor_TasaN 	= "";
      String lsNuValor_TasaP 	= "";
      int 	 liCalificacion 	= 0;
      double ldValorTotalTasa = 0;
      double ldTotal 			= 0;
      String lsTipo 			   = "";
      int 	 liExiste 			= 0;
 
      Vector 				lovRegistro 	= null;
      Vector 				lovDatos 		= new Vector();
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		lsQuery 			= new StringBuffer();
		StringBuffer      variablesBind 	= new StringBuffer();
		PreparedStatement ps0 				= null;
		ResultSet 			rs0 				= null;
		PreparedStatement ps1 				= null;
		ResultSet 			rs1 				= null;
		int 					idx 				= 0;
		String 				[]lineas			= null;
		  
      try{
				
			con.conexionDB();
				
			// 1. PREPARAR QUERY PRINCIPAL
			// ..........................................................................................................
			
			lineas = lsNoEpo == null || lsNoEpo.length() == 0?new String[0]:lsNoEpo.split(",");
			for(int i=0;i<lineas.length;i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
			
			lsQuery.append(" SELECT /*+  use_nl(cpe mb ce cmt cta ctb ct rt pl tmptasa)  */ ");
            lsQuery.append(" distinct ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  ");
			lsQuery.append(" 	   cmt.fn_valor as ValorTasa, cta.cg_rel_mat as RelMatBase,  cta.fn_puntos as PuntosBase, ");
			lsQuery.append(" 	   cpe.ic_calificacion as Clasificacion, rt.cg_rel_mat as RelMatClasif, rt.cg_puntos as PuntosClasif  ");
			lsQuery.append(" 		, to_char(sysdate,'dd/mm/yyyy'), ctb.ic_plazo ");
			lsQuery.append(" 	,pl.CG_DESCRIPCION, ct.ic_tasa ");	
	
			lsQuery.append(" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, ");
			lsQuery.append(" 	 ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) mb,");
			lsQuery.append(" 	comcat_epo ce, com_mant_tasa cmt, comrel_pyme_epo cpe, comrel_tipificacion rt, comcat_plazo pl ");		
		
			lsQuery.append(" WHERE cta.ic_if = ? "); // lsIF
			lsQuery.append(" AND cta.cs_vobo_if = 'S' ");
			lsQuery.append(" AND cta.cs_vobo_epo = 'S'");
			lsQuery.append(" AND cta.cs_vobo_nafin = 'S'");			
			lsQuery.append(" AND cta.ic_epo in ("+ variablesBind.toString() +")"); // lsNoEpo
			lsQuery.append(" AND cta.dc_fecha_tasa = ctb.dc_fecha_tasa");
			lsQuery.append(" AND ctb.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND ctb.ic_epo = ce.ic_epo ");
			lsQuery.append(" AND ctb.cs_tasa_activa = 'S' ");
			lsQuery.append(" AND cpe.ic_epo = ctb.ic_epo");
			lsQuery.append(" AND cpe.ic_pyme = ? "); // lsProveedor
			lsQuery.append(" AND cpe.ic_calificacion = rt.ic_calificacion");
			lsQuery.append(" AND rt.cs_vobo_nafin = 'S'");
			lsQuery.append(" AND rt.dc_fecha_tasa = ctb.dc_fecha_tasa");
			lsQuery.append(" AND ct.ic_moneda = ? "); // lsMoneda
			lsQuery.append(" AND ct.ic_tasa = mb.ic_tasa");
			lsQuery.append(" AND cmt.ic_tasa = ct.ic_tasa");
			lsQuery.append(" AND ctb.ic_plazo  = pl.ic_plazo ");
			lsQuery.append(" AND trunc(cmt.dc_fecha) = trunc(mb.dc_fecha)");
			lsQuery.append((!lsPlazo.equals("")?" AND ctb.ic_plazo = ? ":"")); // lsPlazo
			lsQuery.append("order by ct.ic_tasa");
 
				log.debug("lsQuery 1     = <" + lsQuery.toString()     + ">");
		  
			// Preparar parametros de consulta
			// ............................................................................................
			idx = 1;
			ps0 = con.queryPrecompilado(lsQuery.toString()); // lrsResultado
			ps0.setString(idx++,    lsIF       );
			for(int i=0;i<lineas.length;i++){
				ps0.setString(idx++, lineas[i]  );					
			}	
			ps0.setString(idx++,    lsProveedor);
			ps0.setString(idx++,    lsMoneda   );
			if( !lsPlazo.equals("") ){
				ps0.setString(idx++, lsPlazo    );
			}
 
			// Imprimir Informaci�n de Depuraci�n
			// ............................................................................................
			idx = 1;	
			log.debug("obtenerTasasNafin.Query(1)= " + lsQuery.toString()					 );
			log.debug("obtenerTasasNafin.Query(1).param["    + (idx++) + "]= <" + lsIF        + ">" );
			for(int i=0;i<lineas.length;i++){	
				log.debug("obtenerTasasNafin.Query(1).param[" + (idx++) + "]= <" + lineas[i]   + ">" );
			}	
			log.debug("obtenerTasasNafin.Query(1).param["    + (idx++) + "]= <" + lsProveedor + ">" );
			log.debug("obtenerTasasNafin.Query(1).param["    + (idx++) + "]= <" + lsMoneda    + ">" );
			if( !lsPlazo.equals("") ){
				log.debug("obtenerTasasNafin.Query(1).param[" + (idx++) + "]= <" + lsPlazo     + ">" );
			}
			
			
			ps1 = con.queryPrecompilado(lsQuery.toString()); // lrsResultado				
			
			rs0 = ps0.executeQuery();
			while ( rs0.next()) {
				
				lsCveEpo 		= rs0.getString(1);
				lsNombre 		= rs0.getString(2);
				lsNomTasa 		= rs0.getString(3);
				lsValorTasa 	= rs0.getString(4);
				lsRelMatBase 	= rs0.getString(5);
				lsPuntosTasa 	= rs0.getString(6);
				liCalificacion	= rs0.getInt(7);
				lsRelMatClasif	= rs0.getString(8);
				lsPuntosClasif	= rs0.getString(9);
				lsFecha 			= rs0.getString(10);
				lsPlazo			= rs0.getString(11);
				lsDescripPlazo = rs0.getString(12);
				lsIc_Tasa 		= rs0.getString(13);
				//lsNuValor_Tasa = rs0.getString(14);
				
				if ( lsValorTasa == null || lsValorTasa.equals(""))
					lsValorTasa    = "0";
				if ( lsPuntosTasa == null || lsPuntosTasa.equals(""))
					lsPuntosTasa   = "0";
				if ( lsPuntosClasif == null || lsPuntosClasif.equals(""))
					lsPuntosClasif = "0";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;

				lsTemp = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsTemp += lsRelMatClasif + lsPuntosClasif;
				}
          
				ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;
					
            lovRegistro = new Vector();

            lovRegistro.addElement(lsCveEpo); //0
            lovRegistro.addElement(lsNombre); //1
            lovRegistro.addElement(lsCalificacion); //2
            lovRegistro.addElement(lsCadenaTasa); //3
            lovRegistro.addElement(new Double(ldValorTotalTasa)); //4
            lovRegistro.addElement(lsTemp); //5
				lovRegistro.addElement(lsPlazo); //6
				lovRegistro.addElement(lsDescripPlazo); //7
				lovRegistro.addElement(lsIc_Tasa); //8
				lovRegistro.addElement(new Double(ldTotal)); //9
				lovRegistro.addElement(lsFecha); //10
				
				
				log.debug(" Preparar parametros de consulta)= " );
 
				// Preparar parametros de consulta
				// ............................................................................................
				
				lsQuery = new StringBuffer();							
				lsQuery.append(" SELECT ctpi.cc_tipo_tasa, NVL(ctpi.fn_valor,0),  ");
				lsQuery.append(" nvl(trunc(sysdate) - trunc(df_aplicacion),0), ");
				lsQuery.append(" to_char(sysdate,'dd/mm/yyyy')");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,");
				lsQuery.append(" com_tasa_pyme_if ctpi");						
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.ic_if = ctpi.ic_if");
				lsQuery.append(" AND cpi.ic_epo = ctpi.ic_epo");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if = ? ");     // lsIF
				lsQuery.append(" AND cpi.ic_epo = ? ");    // lsCveEpo
				lsQuery.append(" AND ccb.ic_moneda = ? "); // lsMoneda
				lsQuery.append(" AND ccb.ic_pyme = ? ");   // lsProveedor
				lsQuery.append((!lsPlazo.equals("")?" AND ctpi.ic_plazo = ? ":"")); // lsPlazo
				lsQuery.append(" ORDER BY ctpi.cc_tipo_tasa");
					
				log.debug("obtenerTasasNafin.Query(2)= " + lsQuery.toString());
				log.debug(" lsIF " + lsIF        + ">" );
				log.debug(" lsCveEpo " + lsCveEpo    + ">" );
				log.debug(" lsMoneda " + lsMoneda    + ">" );
				log.debug(" lsProveedor " + lsProveedor + ">" );
				log.debug(" lsPlazo " + lsPlazo     + ">" );
				
				ps1.clearParameters();
				ps1 = con.queryPrecompilado(lsQuery.toString() );
				ps1.setString(1, lsIF        );
				ps1.setString(2, lsCveEpo    );
				ps1.setString(3, lsMoneda    );
				ps1.setString(4, lsProveedor );
				if( !lsPlazo.equals("") ){
					ps1.setString(5, lsPlazo     );
				}							
				
				rs1 = ps1.executeQuery();		
				while ( rs1.next() ) { 
					
					lsTipo 	= rs1.getString(1);
					liExiste = rs1.getInt(3);
					//lsFechaHoy = rs0.getString(4);
					
					if(tipoTasa.equals("N")) {
						
						if ( lsTipo.equals("N") && liExiste == 0){
							lsNuValor_TasaN = rs1.getString(2);
							lovRegistro.addElement(lsNuValor_TasaN); //11	
							log.debug("obtenerTasasNafin.lsNuValor_TasaN"	+lsNuValor_TasaN);										
						}
									
						if ( lsTipo.equals("P")){
							lsNuValor_TasaP = rs1.getString(2);
							if(lsNuValor_TasaP.equals("")){
								lsNuValor_TasaP ="0";
								lovRegistro.addElement(lsNuValor_TasaP); //12	
							}else {										
								lovRegistro.addElement(lsNuValor_TasaP); //12	
							}
						}else{
							lsNuValor_TasaP ="0";
							lovRegistro.addElement(lsNuValor_TasaP); //12	
						}
								
					} //if(tipoTasa.equals("N")) {
							
					if(tipoTasa.equals("P")) {					
					
						if ( lsTipo.equals("P")){
							
							lsNuValor_TasaP = rs1.getString(2);
							lovRegistro.addElement(lsNuValor_TasaP); //11	
								
							if(lsNuValor_TasaP.equals("")){
								lsNuValor_TasaP ="0";
								lovRegistro.addElement(lsNuValor_TasaP); //11	
							}
									 
						}else{
							lsNuValor_TasaP = "0";
							lovRegistro.addElement(lsNuValor_TasaP); //11	
						}
							
					}
					
					/*
					// Nota: Se deja la siguiente seccion como referencia a la consulta
					if ( tipoTasa.equals("N") ){
						lsNuValor_TasaN = rs1.getString(2);
						lovRegistro.addElement(lsNuValor_TasaN); //11
					} else 	if ( tipoTasa.equals("P"))  {
						lsNuValor_TasaP = rs1.getString(2);
						lovRegistro.addElement(lsNuValor_TasaP); //11
						ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsNuValor_TasaP)*100000)) / 100000;
					}
					*/
					
			  }//while ( rs1.next()) 
			  rs1.close();		
			  log.debug("obtenerTasasNafin.lsNuValor_TasaP = <" + lsNuValor_TasaP + ">");	
				
			  if(lsNuValor_TasaP.equals("")){
			  	  lsNuValor_TasaP = "0";
			  	  lovRegistro.addElement(lsNuValor_TasaP); //11	
			  }
 
			  if(lsNuValor_TasaN.equals("")){
			  	  lsNuValor_TasaN = "0";
			  	  lovRegistro.addElement(lsNuValor_TasaP); //11	
			  }
					
			  lsNuValor_TasaP = "";
			  lsNuValor_TasaN = "";
 
			  log.debug("lovRegistro" + lovRegistro);
			  lovDatos.addElement(lovRegistro);
	
			}// Fin del While
			ps1.close();
			ps0.close();
         rs0.close();
         	
 	
		} catch (Exception e) {
			
			log.error("obtenerTasasNafin Exception ");
			log.error("Error en obtenerTasas: "+e);
			log.error("obtenerTasasNafin.lsTipoTasa  = <" + lsTipoTasa  + ">");
			log.error("obtenerTasasNafin.lsNoEpo     = <" + lsNoEpo     + ">");
			log.error("obtenerTasasNafin.lsMoneda    = <" + lsMoneda    + ">");
			log.error("obtenerTasasNafin.lsIF        = <" + lsIF        + ">");
			log.error("obtenerTasasNafin.lsProveedor = <" + lsProveedor + ">");
			log.error("obtenerTasasNafin.lsPuntos    = <" + lsPuntos    + ">");
			log.error("obtenerTasasNafin.lsPlazo     = <" + lsPlazo     + ">");
			log.error("obtenerTasasNafin.edPuntos    = <" + String.valueOf(edPuntos) + ">");
			log.error("obtenerTasasNafin.tipoTasa    = <" + tipoTasa    + ">");
			
			throw new NafinException("SIST0001");
			
		} finally {
			
			if (rs0 	!= null)	try { rs0.close(); } catch(SQLException e) {}
			if (ps0 	!= null)	try { ps0.close(); } catch(SQLException e) {}
			if (rs1 	!= null)	try { rs1.close(); } catch(SQLException e) {}
			if (ps1 	!= null)	try { ps1.close(); } catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("obtenerTasasNafin Termina ");
			
		}
      return lovDatos;
      
    }
	 
    /**
     * Obtiene el valor de las tasas que han sido guardadas, ya sean Prenegociables o Negociables.
     * @throws com.netro.exception.NafinException
     */
    public List obtenerTasasNafinGuardadas( 
    	String lsCadenaEpo, 
    	String lstIF, 
    	String lsCveMoneda,  
    	String lsPYME, 
    	String lsTipoTasa, 
    	String lstPlazo,
    	String acuse
    ) throws AppException {
	 	
		log.info("obtenerTasasNafinGuardadas(E)");
		
		List    				tasasGuardadas	= new ArrayList();
		HashMap 				detalleTasa 	= null;
		
		StringBuffer 		query 			= new StringBuffer();
		StringBuffer      variablesBind 	= new StringBuffer();
		PreparedStatement ps0 				= null;
		ResultSet			rs0   			= null;
		PreparedStatement ps1				= null;
		ResultSet			rs1   			= null;
		AccesoDB				con				= new AccesoDB();
		String 				[]lineas			= null;
		int 					idx				= 0;
		
		try {
			
			con.conexionDB();
			
			// 1. PREPARAR QUERY PRINCIPAL
			// ..........................................................................................................
			
			lineas = lsCadenaEpo == null || lsCadenaEpo.length() == 0?new String[0]:lsCadenaEpo.split(",");
			for(int i=0;i<lineas.length;i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
			
			query.append(
				" SELECT ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  "+
				" nvl(cmt.fn_valor,0) as ValorTasa, ctb.cg_rel_mat as RelMatBase,  nvl(cta.fn_puntos,0) as PuntosBase, "+
				" cpe.ic_calificacion as Clasificacion, nvl(ct.cg_rel_mat,' ') as RelMatClasif, nvl(ct.cg_puntos,0) as PuntosClasif, "+
				" to_char(sysdate,'dd/mm/yyyy'), cpe.ic_pyme, cp.cg_razon_social, pl.cg_descripcion as descPlazo "+
				" ,pl.ic_plazo "+
				" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, "+
				" ( SELECT distinct ic_epo, max(dc_fecha_tasa) as fecha_tasa, ic_tasa "+
				"   FROM comrel_tasa_base  "+
				"   WHERE ic_epo in ("+ variablesBind.toString() +")"+ // lsCadenaEpo
				"   AND cs_vobo_nafin = 'S'"+
				"   AND cs_tasa_activa = 'S'"+			
				"   GROUP BY ic_epo, ic_tasa ) tb,"+
				" ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa)  mb,"+
				" com_mant_tasa cmt, comcat_epo ce, comrel_pyme_epo cpe, "+
				" comrel_tipificacion ct, comcat_pyme cp, comcat_plazo pl "+
				" WHERE cta.ic_if       = ? " + // lstIF
				" AND cta.cs_vobo_if    = 'S' "+
				" AND cta.cs_vobo_epo   = 'S'"+
				" AND cta.cs_vobo_nafin = 'S'"+
				" AND cta.ic_epo        = tb.ic_epo"+
				" AND cta.ic_epo in ("+ variablesBind.toString() +")"+ // lsCadenaEpo
				" AND cta.dc_fecha_tasa = tb.fecha_tasa"+
				" AND ctb.dc_fecha_tasa = tb.fecha_tasa"+
				" AND ctb.ic_epo        = tb.ic_epo"+
				" AND ctb.ic_tasa       = tb.ic_tasa"+
				" AND ctb.ic_tasa       = ct.ic_tasa"+
				" AND ct.ic_moneda      = ? " + // lsCveMoneda+
				" AND ct.ic_tasa        = mb.ic_tasa"+
				" AND cmt.ic_tasa       = ct.ic_tasa"+
				" AND cmt.dc_fecha      = mb.dc_fecha"+
				" AND ctb.ic_epo        = ce.ic_epo "+
				" AND cpe.ic_epo        = ctb.ic_epo"+
				" AND cpe.ic_pyme in ( ? ) " + // lsPYME
				" AND cpe.ic_calificacion = ct.ic_calificacion"+
				" AND ct.dc_fecha_tasa  = tb.fecha_tasa"+
				" AND ct.cs_vobo_nafin  = 'S'"+
				" AND cp.ic_pyme        = cpe.ic_pyme"+
				" AND ctb.ic_plazo      = pl.ic_plazo"
			);
			// Esta seccion no se ocupa, pero se agrega por compatibilidad
			if ( lsTipoTasa.equals("B"))
				query.append(" AND cpe.ic_calificacion = 1");  
			// Agregar condicion de plazo
			if ( !lstPlazo.equals(""))
				query.append(" AND ctb.ic_plazo = ? "); // lstPlazo
			// Agregar ordenamiento del query
			query.append(" order by ct.ic_tasa");
			
			// Preparar parametros de consulta
			// ............................................................................................
			idx = 1;
			ps0 = con.queryPrecompilado(query.toString());
			for(int i=0;i<lineas.length;i++){
				ps0.setString(idx++, lineas[i]  	);	
			}	
			ps0.setString(idx++, lstIF );
			for(int i=0;i<lineas.length;i++){
				ps0.setString(idx++, lineas[i]  	);	
			}	
			ps0.setString(idx++, lsCveMoneda		);
			ps0.setString(idx++, lsPYME			);
			if ( !lstPlazo.equals("")){
				ps0.setString(idx++, lstPlazo		);
			}

			// Imprimir Informaci�n de Depuraci�n
			// ............................................................................................
			idx = 1;	
			log.debug("obtenerTasasNafinGuardadas.Query(1)= " + query.toString()					 );
			for(int i=0;i<lineas.length;i++){
				log.debug("obtenerTasasNafinGuardadas.Query(1).param[" + (idx++) + "]= <" + lineas[i]     + ">" );	
			}	
			log.debug("obtenerTasasNafinGuardadas.Query(1).param["    + (idx++) + "]= <" + lstIF         + ">" );
			for(int i=0;i<lineas.length;i++){
				log.debug("obtenerTasasNafinGuardadas.Query(1).param[" + (idx++) + "]= <" + lineas[i]     + ">" );
			}	
			log.debug("obtenerTasasNafinGuardadas.Query(1).param["    + (idx++) + "]= <" +  lsCveMoneda	+ ">" );
			log.debug("obtenerTasasNafinGuardadas.Query(1).param["    + (idx++) + "]= <" +  lsPYME			+ ">" );
			log.debug("obtenerTasasNafinGuardadas.Query(1).param["    + (idx++) + "]= <" + lstPlazo		+ ">" );
			
			// 2. PREPARAR SUBQUERY
			// ............................................................................................
			query.setLength(0);
			query.append(
				" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor, "+
				" nvl(trunc(sysdate) - trunc(df_aplicacion),0) "+
				" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,"+
				" 	 com_tasa_pyme_if ctpi"+
				" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
				" AND cpi.cs_vobo_if = 'S'"+
				" AND cpi.cs_borrado = 'N'"+
				" AND ccb.cs_borrado = 'N' "+
				" AND cpi.ic_if      = ? " + // lstIF 
				" AND cpi.ic_epo     = ? " + // lsCveEpo 
				" AND ccb.ic_moneda  = ? " + // lsCveMoneda 
				" AND ccb.ic_pyme    = ? " + // lsPYME 
				" AND ctpi.cc_acuse  = ? " + // acuse.toString()
				" AND ctpi.ic_plazo  = ? " + // lsPlazo9
				" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria"+
				" AND cpi.ic_if 		= ctpi.ic_if"+
				" AND cpi.ic_epo 		= ctpi.ic_epo"
			);
			ps1 = con.queryPrecompilado(query.toString());
			
			// 3. REALIZAR CONSULTA
			// ............................................................................................
			rs0 = ps0.executeQuery();
			while ( rs0.next() ) {
				
				// LEER RESULTADOS DE LA CONSULTA
				String lsCveEpo 			= rs0.getString(1);
				String lsNombre 			= rs0.getString(2);
				String lsNomTasa 			= rs0.getString(3);
				String lsValorTasa		= rs0.getString(4);
				String lsRelMatBase		= rs0.getString(5);
				String lsPuntosTasa		= rs0.getString(6);
				int 	 liCalificacion	= rs0.getInt(7);
				String lsRelMatClasif	= rs0.getString(8);
				String lsPuntosClasif	= rs0.getString(9);
				String lsFecha				= rs0.getString(10);
				String lsNomPYME			= rs0.getString(12); // lsDescripPlazo = rs0.getString(12);
				// lsIc_Tasa 		= rs0.getString(13);
				String lsPlazo				= rs0.getString("descPlazo");
				String lsPlazo9			= rs0.getString("ic_plazo");
			
				// OBTENER VALOR DE LOS PUNTOS DE LA TASA
				double ldValorTotalTasa = 0;
				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);
				
				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;
			
				// SE SUMA LA PARTE CORRESPONDIENTE A LA CLASIFICACION
				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);
			
				// SE VUELVE A FORMATEAR EL VALOR DE LA TASA
				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;
			
				// OBTENER LA CALIFICACION
				String lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci&oacute;n";
			
				// OBTENER NOMBRE DETALLADO DE LA TASA
				String lsCadenaTasa 	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;
				String lsCadTasaArch	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + " "      + lsPuntosTasa;
				// SI LA CALIFICACION ES: "Clasificacion", AGREGAR LA DESCRIPCION CORRESPONDIENTE
				if ( lsCalificacion.equals("Clasificaci&oacute;n")){
					lsCadenaTasa  += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsCadTasaArch += " "      + lsRelMatClasif + " "      + lsPuntosClasif;
				}
			
				// Se omite la linea siguiente
				// ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;
				
				/*************************************/
				// Para Tasas Preferenciales, Negociadas y TODAS
				String	lsValorTasaN 				= "";
				boolean	lbExisteTasaNegociada	= false;
				String 	lsValorTasaP				= "";
				String 	lsTipo						= "";
				double 	ldTotal 						= 0;
				
				// Preparar parametros de consulta
				// ............................................................................................
				idx = 1;
				ps1.clearParameters();
				ps1.setString(idx++,lstIF); 
				ps1.setString(idx++,lsCveEpo); 
				ps1.setString(idx++,lsCveMoneda); 
				ps1.setString(idx++,lsPYME); 
				ps1.setString(idx++,acuse);
				ps1.setString(idx++,lsPlazo9);
				rs1 = ps1.executeQuery();
				
				// Imprimir Informaci�n de Depuraci�n
				// ............................................................................................
				idx = 1;	
				log.debug("obtenerTasasNafinGuardadas.Query(2)= " + query.toString()					 );
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + lstIF 			+ ">");
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + lsCveEpo 		+ ">");
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + lsCveMoneda 	+ ">");
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + lsPYME 			+ ">");
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + acuse 			+ ">");
				log.debug("obtenerTasasNafinGuardadas.Query(2).param["    + (idx++) + "]= <" + lsPlazo9 		+ ">");
				
				while(rs1.next()){
					
					lsTipo 			= rs1.getString(1);
					//int liExiste	= rs1.getInt(3);
				
					//if ( lsTipo.equals("N") && liExiste == 0){ 
					// Independientemente del tipo de tasa se considera solo se toma el ultimo valor de esta
					if (        lsTipo.equals("N") ){ // SI ES TASA NEGOCIADA
						lsValorTasaN 				= rs1.getString(2);
						lbExisteTasaNegociada 	= true;
					} else if ( lsTipo.equals("P") ){ // SI ES TASA PREFERENCIAL
						lsValorTasaP 				= rs1.getString(2);
						// Actualizar valor total de la tasa
						ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;
					}
					
				}
				rs1.close();
				
				// Obtener la calificacion de la tasa... 
				lsCalificacion = (!lsValorTasaP.equals(""))? "Preferencial" : lsCalificacion; 
				lsCalificacion = (!lsValorTasaN.equals(""))? "Negociada"    : lsCalificacion; 
	 
				/*if ( !lsValorTasaP.equals("")){
					lsCadenaTasa += "&nbsp;-&nbsp;" + lsValorTasaP;
					lsCadTasaArch += " - " + lsValorTasaP;
					}*/
			
				/*
				if ( lsValorTasaP.equals(""))	lsValorTasaP = "0";
				if ( lsValorTasaP != null && !lsValorTasaP.equals("") )
					ldValorTotalTasa -= Double.parseDouble(lsValorTasaP);
				*/
				if ( !lbExisteTasaNegociada )
					lsFecha = lsValorTasaN = "";
			
				if ( 
					  ( lsTipoTasa.equals("P") && !lsCalificacion.equals("Preferencial") ) 
							|| 
					  ( lsTipoTasa.equals("N") && !lsCalificacion.equals("Negociada") 	) 
							||
					  ( lsTipoTasa.equals("B") && !lsCalificacion.equals("Base")			)  // Se deja por compatibilidad 
					  /*|| ( lsTipoTasa.equals("C") && !lsCalificacion.equals("Clasificaci&oacute;n") )*/ 
				){
					continue;		
				} 
				
				detalleTasa = new HashMap();
				detalleTasa.put("EPO_RELACIONADA",	lsNombre													);	
				detalleTasa.put("NOMBRE_PYME",		lsNomPYME												);
				detalleTasa.put("REFERENCIA",			lsCalificacion											);
				detalleTasa.put("TIPO_TASA",			lsCadenaTasa											);
				detalleTasa.put("PLAZO",				lsPlazo													);
				detalleTasa.put("VALOR",				Comunes.formatoDecimal(ldValorTotalTasa,5)	);
		
				if(!lsValorTasaN.equals("")){
					detalleTasa.put("VALOR_TASA",		Comunes.formatoDecimal(lsValorTasaN,5)	);
				} if(!lsValorTasaP.equals("")){ 
					detalleTasa.put("VALOR_TASA",		Comunes.formatoDecimal(ldTotal,5)		);
				}
				tasasGuardadas.add(detalleTasa);
			
			}
			
			ps1.close();
			
			rs0.close();
			ps0.close();
 
		} catch( Exception e){
			
			log.error("obtenerTasasNafinGuardadas(Exception)");
			log.error("obtenerTasasNafinGuardadas.lsCadenaEpo = <" + lsCadenaEpo + ">");
			log.error("obtenerTasasNafinGuardadas.lstIF       = <" + lstIF       + ">");
			log.error("obtenerTasasNafinGuardadas.lsCveMoneda = <" + lsCveMoneda + ">");
			log.error("obtenerTasasNafinGuardadas.lsPYME      = <" + lsPYME      + ">");
			log.error("obtenerTasasNafinGuardadas.lsTipoTasa  = <" + lsTipoTasa  + ">");
			log.error("obtenerTasasNafinGuardadas.lstPlazo    = <" + lstPlazo    + ">");
			log.error("obtenerTasasNafinGuardadas.acuse       = <" + acuse       + ">");
			e.printStackTrace();
    	
		} finally {
			
			if (rs0 	!= null)	try { rs0.close(); } catch(SQLException e) {}
			if (ps0 	!= null)	try { ps0.close(); } catch(SQLException e) {}
			if (rs1 	!= null)	try { rs1.close(); } catch(SQLException e) {}
			if (ps1 	!= null)	try { ps1.close(); } catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			
			log.info("obtenerTasasNafinGuardadas(S)");
			
		}
		
		
		return tasasGuardadas;
		
	 }
	 
	 public ArrayList procesarTasasPreferenciales(
	  		String esCveIf, 
			String esCveMoneda, 
			String esCadenaEPO,
			String esCvePlazo,
			String icProceso, 
			String lsLineasOk
	)throws AppException {
  
		log.info("procesarTasasPreferenciales(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
      	PreparedStatement 	ps1 					= null;
    	ResultSet 				rs1 					= null;
    	StringBuffer 			lsQuery 				= new StringBuffer();
		StringBuffer			lsVariablesBind	= new StringBuffer();
      
      String 			lsNombre 			= "";
		String 			lsNombrePyme 		= "";
      String 			lsCveEpo 			= "";
      String 			lsNomTasa 			= "";
      String 			lsValorTasa 		= "";
      String 			lsRelMatBase 		= "";
      String 			lsPuntosTasa 		= "";
      String 			lsRelMatClasif 	= "";
      String 			lsPuntosClasif 	= "";
      String 			lsCalificacion 	= "";
      String 			lsCadenaTasa 		= "";
      String 			lsTemp 				= "";
		String 			lsPlazo				= "";
		String 			lsDescripPlazo 	= "";
		String 			lsPuntosTasaNuevo	= "";
		String 			lsIc_Tasa 			= "";
		String         lsEstatus			= "";
		String 			lsCvePyme 			= "";
    String      tipo_factoraje  = "";
    String      lsValorTasaP    = "";
    String      lsValorTasaN    = "";
	 String floating ="";

      int 				liCalificacion 	= 0;
      double 			ldValorTotalTasa 	= 0; 
		double 			ldTotal 				= 0;
		double			edPuntos				= 0;
		
      Vector 			lovRegistro 		= null;
      ArrayList		lovDatos 			= new ArrayList();

      try{
			
			con.conexionDB();
			
			String [] lineas = lsLineasOk.split(","); 
			for(int i=0;i<lineas.length;i++){
				if(i>0) lsVariablesBind.append(",");
				lsVariablesBind.append("?");
			}
 
			lsQuery.append(
				" SELECT" +
                                " /*+ use_nl(pyme cpe mb ce cmt cta ctb ct rt pl tmptasa)  */ "  +
                                "          distinct                            		"  +
				" 	   ctb.ic_epo          as CveEpo,         		"  +
				" 	   ce.cg_razon_social  as NomEpo,         		"  +
				" 	   ct.cd_nombre        as tasa,           		"  +
				" 	   cmt.fn_valor        as ValorTasa,      		"  +
				" 	   cta.cg_rel_mat      as RelMatBase,     		"  + 
				" 	   cta.fn_puntos       as PuntosBase,     		"  +
				" 	   cpe.ic_calificacion as Clasificacion,  		"  +
				" 	   rt.cg_rel_mat       as RelMatClasif,   		"  +
				" 	   rt.cg_puntos        as PuntosClasif,   		"  +
				" 		to_char(sysdate,'dd/mm/yyyy'),         		"  +
				" 	   ctb.ic_plazo,                          		"  +
				" 		pl.CG_DESCRIPCION,                     		"  +
				" 	   ct.ic_tasa,                            		"  +	
				" 	   decode(tmptasa.cg_estatus,'E',0,tmptasa.cg_puntos_valor_tasa) as PuntosTasa,  "  +
				" 	   pyme.cg_razon_social as NomPyme,        		"  +
				"     tmptasa.cg_estatus   as Estatus,             "  +
				"		pyme.ic_pyme         as CvePyme        		"  +
				" FROM                            "  +
				"        comcat_pyme pyme,                          " + 
				"        comrel_pyme_epo cpe,                       " + 
				"        (SELECT ic_tasa, MAX(dc_fecha) AS dc_fecha FROM com_mant_tasa GROUP BY ic_tasa) mb," + 
				"        comcat_epo ce,                             " + 
				"        com_mant_tasa cmt,                         " + 
				"        comrel_tasa_autorizada cta,                " + 
				"        comrel_tasa_base ctb,                      " + 
				"        comcat_tasa ct,                            " + 
				"        comrel_tipificacion rt,                    " + 
				"        comcat_plazo pl,                           " + 
				"        comtmp_tasas tmptasa                       " +
				" WHERE                           "  +
				"     cta.ic_if            =  ?   "  +
				" AND cta.cs_vobo_if       =  ?   "  +
				" AND cta.cs_vobo_epo      =  ?   "  +
				" AND cta.cs_vobo_nafin    =  ?   "  +
				" AND cta.ic_epo           =  ?   "  +
				" AND cta.dc_fecha_tasa    = ctb.dc_fecha_tasa    "  +
				" AND ctb.ic_tasa          = ct.ic_tasa           "  +
				" AND ctb.ic_epo           = ce.ic_epo            "  +
				" AND ctb.cs_tasa_activa   = ?                    "  +
				" AND cpe.ic_epo           = ctb.ic_epo           "  +
				" AND cpe.ic_pyme         	= pyme.ic_pyme         "  +
				" and pyme.cg_rfc         	= tmptasa.cg_rfc       "  +
				" and tmptasa.ic_numero_proceso = ?               "  +
				" and tmptasa.ic_numero_linea in ("+lsVariablesBind.toString()+") "  +
				" AND cpe.ic_calificacion  = rt.ic_calificacion   "  +
				" AND rt.cs_vobo_nafin     = ?                    "  +
				" AND rt.dc_fecha_tasa     = ctb.dc_fecha_tasa    "  +
				" AND ct.ic_moneda         = ?                    "  +
				" AND ct.ic_tasa           = mb.ic_tasa           "  +
				" AND cmt.ic_tasa          = ct.ic_tasa           "  +
				" AND ctb.ic_plazo         = pl.ic_plazo          "  +
				" AND trunc(cmt.dc_fecha)  = trunc(mb.dc_fecha)   "  +
				(!esCvePlazo.equals("")?" AND ctb.ic_plazo = ? ":"") +
				" ORDER BY 15, 3 "
			);
			
			log.debug("lsQuery = {"+lsQuery.toString()+"}");
			log.debug("esCveIf "+esCveIf +" esCadenaEPO "+esCadenaEPO+" icProceso "+icProceso);
			
			ps = con.queryPrecompilado(lsQuery.toString());
			ps.setString(1, esCveIf);
			ps.setString(2, "S");
			ps.setString(3, "S");
			ps.setString(4, "S");
			ps.setString(5, esCadenaEPO);
			ps.setString(6, "S");
			ps.setString(7, icProceso);
			int i = 8;
			for(i=8;i<(lineas.length+8);i++){
				ps.setString(i, lineas[i-8]);
			}
			ps.setString(i++, "S");	
			ps.setString(i++, esCveMoneda);
			if(!esCvePlazo.equals("")) ps.setString(i++, esCvePlazo);

			rs = ps.executeQuery();
			
			String 	lsTipo 			   = "";
			int 		liExiste 			= 0;
			
			lsQuery.delete(0,lsQuery.length());
			lsQuery.append(
					" SELECT 															"  +
					"		ctpi.cc_tipo_tasa, 										"  + 
					"		ctpi.fn_valor,  											"  +
					" 		nvl(trunc(sysdate) - trunc(df_aplicacion),0), 	"  +
					" 		to_char(sysdate,'dd/mm/yyyy') 						"  +
					" FROM 																"  +
					"	comrel_pyme_if 			cpi, 								"  + 
					"	comrel_cuenta_bancaria 	ccb,								"  +
						(!"M".equals(tipo_factoraje)?" com_tasa_pyme_if ctpi ":" com_tasa_pyme_if_mandato ctpi ")  +
					" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria  "  +
					" AND cpi.ic_cuenta_bancaria   = ctpi.ic_cuenta_bancaria "  +
					" AND cpi.ic_if                = ctpi.ic_if  				"  +
					" AND cpi.ic_epo               = ctpi.ic_epo 				"  +
					" AND cpi.cs_vobo_if           = ? 							   "  +
					" AND cpi.cs_borrado           = ? 						   	"  +
					" AND ccb.cs_borrado           = ? 						   	"  +
					" AND cpi.ic_if                = ? 					   		"  +
					" AND cpi.ic_epo               = ? 						   	"  +
					" AND ccb.ic_moneda            = ? 					   		"  +
					" AND ccb.ic_pyme              = ? 					   		"  +
					" AND ctpi.ic_plazo            = ? 					   		"  +
					" AND ctpi.cc_tipo_tasa 		 = ? 								"  +
					" ORDER BY ctpi.cc_tipo_tasa "
			);	
			System.out.println("lsQuery = <"+lsQuery.toString()+">");
			ps1 = con.queryPrecompilado(lsQuery.toString());
			
			
			while(rs != null && rs.next()){
				
				lsCveEpo 			= rs.getString(1);
				lsNombre 			= rs.getString(2);
				lsNomTasa 			= rs.getString(3);
				lsValorTasa 		= rs.getString(4);
				lsRelMatBase 		= rs.getString(5);
				lsPuntosTasa 		= rs.getString(6);
				liCalificacion 	= rs.getInt(7);
				lsRelMatClasif 	= rs.getString(8);
				lsPuntosClasif 	= rs.getString(9);
				lsPlazo				= rs.getString(11);
				lsDescripPlazo 	= rs.getString(12);
				lsIc_Tasa 			= rs.getString(13);
				lsPuntosTasaNuevo	= rs.getString("PuntosTasa");
				lsNombrePyme		= rs.getString("NomPyme");
				lsEstatus			= rs.getString("Estatus");
				lsCvePyme			= rs.getString("CvePyme");
				//floating			= rs.getString("floating");

				if ( lsValorTasa == null    || lsValorTasa.equals(""))
					lsValorTasa = "0";
				if ( lsPuntosTasa == null   || lsPuntosTasa.equals(""))
					lsPuntosTasa = "0";
				if ( lsPuntosClasif == null || lsPuntosClasif.equals(""))
					lsPuntosClasif = "0";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa 	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;

				lsTemp 			= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsTemp       += lsRelMatClasif + lsPuntosClasif;
				}
				
				edPuntos				= Double.parseDouble(lsPuntosTasaNuevo);
            ldTotal 				= (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;
 
				
				ps1.clearParameters();
				ps1.setString(1, "S"        );
				ps1.setString(2, "N"        );
				ps1.setString(3, "N"        );
				ps1.setString(4, esCveIf    );
				ps1.setString(5, lsCveEpo   );
				ps1.setString(6, esCveMoneda);
				ps1.setString(7, lsCvePyme  );
				ps1.setString(8, lsPlazo    );
				ps1.setString(9, "P"        );
				rs1 = ps1.executeQuery();
				
				
				lsValorTasaP = "";
				while(rs1 != null && rs1.next()){	
					lsTipo = rs1.getString(1);
					liExiste = rs1.getInt(3);
					
					if ( lsTipo.equals("N") && liExiste == 0)
						lsValorTasaN = rs1.getString(2);
					else if ( lsTipo.equals("P"))
						lsValorTasaP = rs1.getString(2);
					
				}
				rs1.close();
				
				if(lsValorTasaP == null || lsValorTasaP.equals("")){
					lsValorTasaP ="0";
				}
 
            lovRegistro = new Vector();

            lovRegistro.addElement(lsCveEpo);
            lovRegistro.addElement(lsNombre);
            lovRegistro.addElement(lsCalificacion);
            lovRegistro.addElement(lsCadenaTasa);
            lovRegistro.addElement(new Double(ldValorTotalTasa));
            lovRegistro.addElement(lsTemp);
				lovRegistro.addElement(lsPlazo);
				lovRegistro.addElement(lsDescripPlazo);
				lovRegistro.addElement(lsIc_Tasa);
				lovRegistro.addElement(new Double(ldTotal));
				lovRegistro.addElement(lsNombrePyme);
				lovRegistro.addElement(lsPuntosTasaNuevo);
				lovRegistro.addElement(lsEstatus);
				lovRegistro.addElement(lsCvePyme);
				lovRegistro.addElement(lsValorTasaP);
				lovRegistro.addElement(floating);				
            lovDatos.add(lovRegistro);
					 
         }// Fin del While
				
		} catch (Exception e) {
			log.error("procesarTasasPreferenciales(Exception)");
			log.error("procesarTasasPreferenciales.esCveIf     = <"+esCveIf     +">");
			log.error("procesarTasasPreferenciales.esCveMoneda = <"+esCveMoneda +">");
			log.error("procesarTasasPreferenciales.esCadenaEPO = <"+esCadenaEPO +">");
			log.error("procesarTasasPreferenciales.esCvePlazo  = <"+esCvePlazo  +">");
			log.error("procesarTasasPreferenciales.icProceso   = <"+icProceso   +">");
			log.error("procesarTasasPreferenciales.lsLineasOk  = <"+lsLineasOk  +">");
			e.printStackTrace();
			throw new AppException("Error al Procesar Tasas Preferenciales de la Carga Masiva");
		} finally {
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (rs1 	!= null) 										try { rs1.close(); 	} catch(SQLException e) {}
			if (ps1 	!= null) 										try { ps1.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("procesarTasasPreferenciales(S)");
		}
		
		return lovDatos;
		
    }
	 
	 public ArrayList procesarTasasNegociadas(
	  		String esCveIf, 
			String esCveMoneda, 
			String esCadenaEPO,
			String esCvePlazo,
			String icProceso, 
			String lsLineasOk
	)throws AppException {
  
		log.info("procesarTasasNegociadas(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
    	StringBuffer 			lsQuery 				= new StringBuffer();
		StringBuffer			lsVariablesBind	= new StringBuffer();
      
      String 			lsNombre 			= "";
		String 			lsNombrePyme 		= "";
      String 			lsCveEpo 			= "";
      String 			lsNomTasa 			= "";
      String 			lsValorTasa 		= "";
      String 			lsRelMatBase 		= "";
      String 			lsPuntosTasa 		= "";
      String 			lsRelMatClasif 	= "";
      String 			lsPuntosClasif 	= "";
      String 			lsCalificacion 	= "";
      String 			lsCadenaTasa 		= "";
      String 			lsTemp 				= "";
		String 			lsPlazo				= "";
		String 			lsDescripPlazo 	= "";
		String 			lsPuntosTasaNuevo	= "";
		String 			lsIc_Tasa 			= "";
		String         lsEstatus			= "";
		String  			lsCvePyme			= "";

      int 				liCalificacion 	= 0;
      double 			ldValorTotalTasa 	= 0; 
		double 			ldTotal 				= 0;
		
      Vector 			lovRegistro 		= null;
      ArrayList		lovDatos 			= new ArrayList();
		ArrayList		lovDatos2 			= new ArrayList();

      try{
			
			con.conexionDB();
			
			String [] lineas = lsLineasOk.split(","); 
			for(int i=0;i<lineas.length;i++){
				if(i>0) lsVariablesBind.append(",");
				lsVariablesBind.append("?");
			}
 
			lsQuery.append(
				" SELECT " +
                                " /*+  use_nl(pyme cpe mb ce cmt cta ctb ct rt pl tmptasa)  */ "  +
                                " distinct                            		"  +
				" 	   ctb.ic_epo          as CveEpo,         		"  +
				" 	   ce.cg_razon_social  as NomEpo,         		"  +
				" 	   ct.cd_nombre        as tasa,           		"  +
				" 	   cmt.fn_valor        as ValorTasa,      		"  +
				" 	   cta.cg_rel_mat      as RelMatBase,     		"  + 
				" 	   cta.fn_puntos       as PuntosBase,     		"  +
				" 	   cpe.ic_calificacion as Clasificacion,  		"  +
				" 	   rt.cg_rel_mat       as RelMatClasif,   		"  +
				" 	   rt.cg_puntos        as PuntosClasif,   		"  +
				" 		to_char(sysdate,'dd/mm/yyyy'),         		"  +
				" 	   ctb.ic_plazo,                          		"  +
				" 		pl.CG_DESCRIPCION,                     		"  +
				" 	   ct.ic_tasa                             		"  +	
				" 	   ,decode(tmptasa.cg_estatus,'E',0,tmptasa.cg_puntos_valor_tasa) as PuntosTasa,  "  +
				" 	   pyme.cg_razon_social as NomPyme,        		"  +
				"     tmptasa.cg_estatus   as Estatus,             "  +
				"		pyme.ic_pyme			as CvePyme					"  +
				" FROM                            "  +
				" 	     comcat_pyme pyme,                          " + 
				"        comrel_pyme_epo cpe,                       " +
				" 	 	( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa) mb, "  +
				" 	 	 comcat_epo ce,                             " + 
				"        com_mant_tasa cmt,                         " + 
				"        comrel_tasa_autorizada cta,                " + 
				"        comrel_tasa_base ctb,                      " + 
				"        comcat_tasa ct,                            " + 
				"        comrel_tipificacion rt,                    " + 
				"        comcat_plazo pl,                           " + 
				"        comtmp_tasas tmptasa                       " +
				" WHERE                           "  +
				"     cta.ic_if            =  ?   "  +
				" AND cta.cs_vobo_if       =  ?   "  +
				" AND cta.cs_vobo_epo      =  ?   "  +
				" AND cta.cs_vobo_nafin    =  ?   "  +
				" AND cta.ic_epo           =  ?   "  +
				" AND cta.dc_fecha_tasa    = ctb.dc_fecha_tasa    "  +
				" AND ctb.ic_tasa          = ct.ic_tasa           "  +
				" AND ctb.ic_epo           = ce.ic_epo            "  +
				" AND ctb.cs_tasa_activa   = ?                    "  +
				" AND cpe.ic_epo           = ctb.ic_epo           "  +
				//
				" AND cpe.ic_pyme         	= pyme.ic_pyme         "  +
				" and pyme.cg_rfc         	= tmptasa.cg_rfc       "  +
				" and tmptasa.ic_numero_proceso = ?               "  +
				" and tmptasa.ic_numero_linea in ("+lsVariablesBind.toString()+") "  +
				//
				" AND cpe.ic_calificacion  = rt.ic_calificacion   "  +
				" AND rt.cs_vobo_nafin     = ?                    "  +
				" AND rt.dc_fecha_tasa     = ctb.dc_fecha_tasa    "  +
				" AND ct.ic_moneda         = ?                    "  +
				" AND ct.ic_tasa           = mb.ic_tasa           "  +
				" AND cmt.ic_tasa          = ct.ic_tasa           "  +
				" AND ctb.ic_plazo         = pl.ic_plazo          "  +
				" AND trunc(cmt.dc_fecha)  = trunc(mb.dc_fecha)   "  +
				(!esCvePlazo.equals("")?" AND ctb.ic_plazo = ? ":"") +
				" ORDER BY 15, 3 "
			);
			
			System.out.println("lsQuery = {"+lsQuery.toString()+"}");
			
			ps = con.queryPrecompilado(lsQuery.toString());
			ps.setString(1, esCveIf);
			ps.setString(2, "S");
			ps.setString(3, "S");
			ps.setString(4, "S");
			ps.setString(5, esCadenaEPO);
			ps.setString(6, "S");
			ps.setString(7, icProceso);
			int i = 8;
			for(i=8;i<(lineas.length+8);i++){
				ps.setString(i, lineas[i-8]);
			}
			ps.setString(i++, "S");	
			ps.setString(i++, esCveMoneda);
			if(!esCvePlazo.equals("")) ps.setString(i++, esCvePlazo);

			rs = ps.executeQuery();
			while(rs != null && rs.next()){
				
				lsCveEpo 			= rs.getString(1);
				lsNombre 			= rs.getString(2);
				lsNomTasa 			= rs.getString(3);
				lsValorTasa 		= rs.getString(4);
				lsRelMatBase 		= rs.getString(5);
				lsPuntosTasa 		= rs.getString(6);
				liCalificacion 	= rs.getInt(7);
				lsRelMatClasif 	= rs.getString(8);
				lsPuntosClasif 	= rs.getString(9);
				lsPlazo				= rs.getString(11);
				lsDescripPlazo 	= rs.getString(12);
				lsIc_Tasa 			= rs.getString(13);
				lsPuntosTasaNuevo	= rs.getString("PuntosTasa");
				lsNombrePyme		= rs.getString("NomPyme");
				lsEstatus			= rs.getString("Estatus");
				lsCvePyme			= rs.getString("CvePyme");

				if ( lsValorTasa == null    || lsValorTasa.equals(""))
					lsValorTasa = "0";
				if ( lsPuntosTasa == null   || lsPuntosTasa.equals(""))
					lsPuntosTasa = "0";
				if ( lsPuntosClasif == null || lsPuntosClasif.equals(""))
					lsPuntosClasif = "0";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa 	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "&nbsp;" + lsPuntosTasa;

				lsTemp 			= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "&nbsp;" + lsRelMatClasif + "&nbsp;" + lsPuntosClasif;
					lsTemp       += lsRelMatClasif + lsPuntosClasif;
				}
				
				//edPuntos				= Double.parseDouble(lsPuntosTasaNuevo);
            //ldTotal 				= (double) (Math.round(ldValorTotalTasa*100000) - Math.round(edPuntos*100000)) / 100000;
 
            lovRegistro = new Vector();

            lovRegistro.addElement(lsCveEpo);
            lovRegistro.addElement(lsNombre);
            lovRegistro.addElement(lsCalificacion);
            lovRegistro.addElement(lsCadenaTasa);
            lovRegistro.addElement(new Double(ldValorTotalTasa));
            lovRegistro.addElement(lsTemp);
				lovRegistro.addElement(lsPlazo);
				lovRegistro.addElement(lsDescripPlazo);
				lovRegistro.addElement(lsIc_Tasa);
				//lovRegistro.addElement(new Double(ldTotal));
				lovRegistro.addElement(lsNombrePyme);
				lovRegistro.addElement(lsPuntosTasaNuevo);
				lovRegistro.addElement(lsEstatus);
				lovRegistro.addElement(lsCvePyme);
				
            lovDatos.add(lovRegistro);
					 
         }// Fin del While
			
			String 	lsValorTasaP 		= "";
			String 	lsValorTasaN 		= "";
			String 	lsFechaHoy 		  	= "";
			String 	tipo_factoraje	  	= "";
      
			Vector 	lovRegistro2	   = null;
			String 	lsTipo 			   = "";
			int 		liExiste 			= 0;
			
			lsQuery.delete(0,lsQuery.length());
			lsQuery.append(
					" SELECT 															"  +
					"		ctpi.cc_tipo_tasa, 										"  + 
					"		ctpi.fn_valor,  											"  +
					" 		nvl(trunc(sysdate) - trunc(df_aplicacion),0), 	"  +
					" 		to_char(sysdate,'dd/mm/yyyy') 						"  +
					" FROM 																"  +
					"	comrel_pyme_if 			cpi, 								"  + 
					"	comrel_cuenta_bancaria 	ccb,								"  +
						(!"M".equals(tipo_factoraje)?" com_tasa_pyme_if ctpi ":" com_tasa_pyme_if_mandato ctpi ")  +
					" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria  "  +
					" AND cpi.ic_cuenta_bancaria   = ctpi.ic_cuenta_bancaria "  +
					" AND cpi.ic_if                = ctpi.ic_if  				"  +
					" AND cpi.ic_epo               = ctpi.ic_epo 				"  +
					" AND cpi.cs_vobo_if           = ? 							   "  +
					" AND cpi.cs_borrado           = ? 						   	"  +
					" AND ccb.cs_borrado           = ? 						   	"  +
					" AND cpi.ic_if                = ? 					   		"  +
					" AND cpi.ic_epo               = ? 						   	"  +
					" AND ccb.ic_moneda            = ? 					   		"  +
					" AND ccb.ic_pyme              = ? 					   		"  +
					" AND ctpi.ic_plazo            = ? 					   		"  +
					//" AND ctpi.cc_tipo_tasa 		 = ? 								"  +
					" ORDER BY ctpi.cc_tipo_tasa "
			);	
			System.out.println("lsQuery = <"+lsQuery.toString()+">");
			ps 	= con.queryPrecompilado(lsQuery.toString());
			
			for ( int j = 0; j < lovDatos.size(); j++) {
				lsValorTasaP = lsValorTasaN = lsFechaHoy =  "";
				
				lovRegistro 		= (Vector) lovDatos.get(j);

				lsCveEpo 			= (String) lovRegistro.elementAt(0);
            lsCadenaTasa 		= (String) lovRegistro.elementAt(3);
            ldValorTotalTasa 	= ((Double) lovRegistro.elementAt(4)).doubleValue();
            lsTemp 				= (String) lovRegistro.elementAt(5);
				lsPlazo				= lovRegistro.elementAt(6).toString();
				lsDescripPlazo 	= lovRegistro.elementAt(7).toString();
				lsIc_Tasa 			= lovRegistro.elementAt(8).toString();
				lsNombrePyme		= (String) lovRegistro.elementAt(9);
				lsPuntosTasaNuevo	= (String) lovRegistro.elementAt(10);
				lsEstatus			= (String) lovRegistro.elementAt(11);
				lsCvePyme 			= (String) lovRegistro.elementAt(12);
 
				ps.clearParameters();
				ps.setString(1, "S"        );
				ps.setString(2, "N"        );
				ps.setString(3, "N"        );
				ps.setString(4, esCveIf    );
				ps.setString(5, lsCveEpo   );
				ps.setString(6, esCveMoneda);
				ps.setString(7, lsCvePyme  );
				ps.setString(8, esCvePlazo );
				//ps.setString(9, "N"        );
				rs = ps.executeQuery();
				
				while(rs != null && rs.next()) {
					lsTipo 		= rs.getString(1);
					liExiste 	= rs.getInt(3);
					lsFechaHoy 	= rs.getString(4);
	
					if ( lsTipo.equals("N") && liExiste == 0)
						lsValorTasaN = rs.getString(2);
					else if ( lsTipo.equals("P"))
						lsValorTasaP = rs.getString(2);
				}
				rs.close();
				
				lsCalificacion = (!lsValorTasaP.equals(""))? "PREFERENCIAL" : (String) lovRegistro.elementAt(2);
				if ( !lsValorTasaP.equals("")){
					lsCadenaTasa 	+= "&nbsp;-&nbsp;" + lsValorTasaP;
					lsTemp 			+= "-"+ lsValorTasaP;
				}
				
				if ( lsValorTasaP.equals(""))
					lsValorTasaP = "0";
				if ( lsValorTasaP != null || !lsValorTasaP.equals("") )
					ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;
	
				lovRegistro2 = new Vector();
				
				lovRegistro2.add(0,lovRegistro.elementAt(0));
				lovRegistro2.add(1,lovRegistro.elementAt(1));
				lovRegistro2.add(2,lsCalificacion);
				lovRegistro2.add(3,lsCadenaTasa);
				lovRegistro2.add(4,new Double(ldTotal));
				lovRegistro2.add(5,lsValorTasaN);
				lovRegistro2.add(6,(lsFechaHoy.equals("")?(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date()):lsFechaHoy));
				lovRegistro2.add(7,lsTemp);
				lovRegistro2.add(8,lsPlazo);
				lovRegistro2.add(9,lsDescripPlazo);		
				lovRegistro2.add(10,lsIc_Tasa);
				lovRegistro2.add(11,lsPuntosTasaNuevo);
				lovRegistro2.add(12,lsEstatus);
				lovRegistro2.add(13,lsNombrePyme);
				lovRegistro2.add(14,lsCvePyme);
				lovRegistro2.add(15,new Double(ldValorTotalTasa));
	 
				lovDatos2.add(lovRegistro2);
 
			}
			ps.close();
				
		} catch (Exception e) {
			log.error("procesarTasasNegociadas(Exception)");
			log.error("procesarTasasNegociadas.esCveIf     = <"+esCveIf     +">");
			log.error("procesarTasasNegociadas.esCveMoneda = <"+esCveMoneda +">");
			log.error("procesarTasasNegociadas.esCadenaEPO = <"+esCadenaEPO +">");
			log.error("procesarTasasNegociadas.esCvePlazo  = <"+esCvePlazo  +">");
			log.error("procesarTasasNegociadas.icProceso   = <"+icProceso   +">");
			log.error("procesarTasasNegociadas.lsLineasOk  = <"+lsLineasOk  +">");
			e.printStackTrace();
			throw new AppException("Error al Procesar Tasas Negociadas de la Carga Masiva");
		} finally {
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("procesarTasasNegociadas(S)");
		}
		
		return lovDatos2;
		
    }

	 /**   
	 *
	 *	    guardarTasas()
	 *	    
	 *	    @throws com.netro.exception.NafinException
	 *	    @return 
	 *	    @param lsPuntos
	 *	    @param nombreUsuario
	 *	    @param _acuse
	 *	    @param ic_usuario
	 *	    @param cc_acuse
	 *	    @param esValorTasas
	 *	    @param esCadenaTasas
	 *	    @param esTipoTasa
	 *	    @param esCvePyme
	 *	    @param esCveMoneda
	 *	    @param esCveIf
	 */
    public boolean guardarTasasCargaMasiva(
		 	String 		esCveIf, 
			String 		esCveMoneda,
			String 		lsCveEpo,
			String 		esTipoTasa,
			String 		cc_acuse,
			String 		ic_usuario,
			String 		_acuse, 
			String[] 	lsListaTasas,
			String 		nombreUsuario,
			AccesoDB 	con)
	throws AppException {
	 
		log.info("guardarTasasCargaMasiva(E)");
			
		if(lsListaTasas == null || lsListaTasas.length == 0 ){
			log.info("guardarTasasCargaMasiva(S)");
			return false;	
		}
		
    	StringBuffer 		lsQuery 			= new StringBuffer();
      ResultSet 			lrsResultado 	= null;
   	//AccesoDB 			con  				= new AccesoDB();
   	PreparedStatement ps 				= null;
   	String 				auxTipoTasa 	= esTipoTasa;
		StringBuffer 		lsQuery2 		= new StringBuffer();
		String 				lsValorTasa 	= "";
			
   	if("R".equals(esTipoTasa))
   		esTipoTasa = "N";
   	else 
   		auxTipoTasa = "";

      //String 	lsCveEpo 			= "";
      String esCvePyme      = "";
      String 	lsTasa 				= "";
		String 	lsCuentaBancaria 	= "";
      String 	lsFecha 				= "";
		String 	lsPlazo				= "";
		String 	lsFechaFin			= "NULL";	
		String 	lsNoTasa 			= "";
    String  lsPuntos      = "";
		String lsFloating ="";
		int 		liRegistros 	= 0;
      boolean 	lbOK 				= true;

		int 		ordenApp 		= 1;
		
		String[] registro 		= null;

      try{
      	// con.conexionDB();
			
			// INSERTAR ACUSE
        	if(!"".equals(_acuse)){
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(
					" INSERT INTO COM_ACUSE3"+
					" (CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
					" VALUES(?,sysdate,?,?,1)");
				ps = con.queryPrecompilado(lsQuery.toString());
				ps.setString(1,cc_acuse);
				ps.setString(2,ic_usuario);
				ps.setString(3,_acuse);
				ps.execute();
				ps.close();
			}
			log.debug("lsQuery--->"+lsQuery.toString());
				
			for(int i=0; i < lsListaTasas.length; i++) {
				
				liRegistros 	= 0;
				// lovDatos 		= new Vector();
				// lovDatos 		= Comunes.explode("|",esCadenaTasas[i]);
				registro			= lsListaTasas[i].split("\\|");
				// log.debug ("lovDatos"+lovDatos);

				esCvePyme 		= registro[0];
				lsPlazo			= registro[1];
				lsTasa 			= esTipoTasa.equals("N")?registro[2]:"0";
				lsPuntos			= esTipoTasa.equals("P")?registro[3]:"0";
				lsNoTasa			= registro[4]; 
				lsValorTasa		= registro[5]; // esTipoTasa.equals("N")?lsTasa:(esTipoTasa.equals("P")?lsPuntos:"0"); 
				lsFloating = registro[6]; 
				
				log.debug("lsFloating--->"+lsFloating);
				
				//lsCveEpo 		= lovDatos.get(0).toString();
				//lsTasa   		= lovDatos.get(1).toString();
				//lsPlazo	 		= lovDatos.get(2).toString();
				//lsNoTasa 		= lovDatos.get(3).toString();
				//lsValorTasa 	= lovDatos.get(4).toString();
			
				/*if("R".equals(auxTipoTasa)) {
					lsFechaIni = lovDatos.get(1).toString();
					lsFechaFin = lovDatos.get(3).toString();
				}*/
				
				// OBTENER LA CLAVE DE LA CUENTA BANCARIA
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(" SELECT cpi.ic_cuenta_bancaria");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb");
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S' ");
				lsQuery.append(" AND cpi.cs_borrado = 'N' ");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if      = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo     = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda  = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme    = " + esCvePyme);

				log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT1: "+lsQuery.toString());
				
				if ( lrsResultado.next()) {
					lsCuentaBancaria = lrsResultado.getString(1);
				} else{
					lsCuentaBancaria = "";
				}	
				
				lrsResultado.close();
				con.cierraStatement();
				
				// BUSCAR SI YA EXISTE UN REGISTRO PARA ESTA PYME
				lsQuery.delete(0, lsQuery.length());
				lsQuery.append(" SELECT count(1)");
				lsQuery.append(" FROM com_tasa_pyme_if");
				lsQuery.append(" WHERE ic_cuenta_bancaria = " + lsCuentaBancaria);
				lsQuery.append(" AND ic_if                = " + esCveIf);
				lsQuery.append(" AND ic_epo               = " + lsCveEpo);
				lsQuery.append(" AND cc_tipo_tasa         = '"+ esTipoTasa + "'");
				lsQuery.append(" AND ic_plazo             = " + lsPlazo);
				if(esTipoTasa.equals("N")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 1");
				else if(auxTipoTasa.equals("R"))
					lsQuery.append(" AND ic_orden = 2");
				else if(esTipoTasa.equals("P")&&"".equals(auxTipoTasa))
					lsQuery.append(" AND ic_orden = 3");
 
				log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
				System.out.println("Query GT2: "+lsQuery.toString());
				if ( lrsResultado.next()) {
					liRegistros = lrsResultado.getInt(1);
				}
				lrsResultado.close();
				con.cierraStatement();

				lsFecha = esTipoTasa.equals("P")? "null" : "sysdate";

				if(esTipoTasa.equals("P")){
					ordenApp = 3;
					lsTasa = lsPuntos;
				}
				
				// NO EXISTE EL REGISTRO POR LO QUE SE INSERTA
				if ( liRegistros == 0) {
					
					// INSERTAR TABLA COM_TASA_PYME_IF
					lsQuery = new StringBuffer();
					lsQuery.append(" INSERT INTO com_tasa_pyme_if");
					lsQuery.append(" (ic_cuenta_bancaria, ic_if, ic_epo, cc_tipo_tasa, fn_valor, ");
					lsQuery.append(" df_aplicacion, ic_plazo,cc_acuse, df_aplicacion_fin, ic_orden, CS_TASA_FLOATING ) ");
					lsQuery.append(" VALUES("+ lsCuentaBancaria +","+ esCveIf +","+ lsCveEpo );
					lsQuery.append(" ,'" +esTipoTasa +"'," + lsTasa + ","+ lsFecha +", "+lsPlazo+",'"+cc_acuse+"', "+lsFechaFin+","+ordenApp+", '"+lsFloating+"' )");
					
					log.debug("com_tasa_pyme_if : "+lsQuery.toString());	 	
				
					ps = con.queryPrecompilado(lsQuery.toString());
					ps.executeUpdate();
					ps.close();
				
					// INSERTAR EN BITACORA REGISTRO DE LA TABLA QUE SE INSERTO
					lsQuery2 = new StringBuffer();
					String estatus= "A";
					lsQuery2.append(	"	insert into Bit_Tasas_Preferenciales  "+
											" (ic_bitacora,ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
											" fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus, CS_TASA_FLOATING ) "+	
											" VALUES(SEQ_BIT_TASAS_PREFERENCIALES.NEXTVAL,"+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
											esTipoTasa+"',"+esCveMoneda +","+lsPuntos+","+
											lsValorTasa 
											+",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"', '"+lsFloating+"' )");
					log.debug("Bit_Tasas_Preferenciales : "+lsQuery2.toString());	
				
					ps = con.queryPrecompilado(lsQuery2.toString());
					ps.executeUpdate();
					ps.close();
				
				// LA TASA YA EXISTE POR LO QUE SOLO SE ACTUALIZA	
				}else{
					// ACTUALIZAR TASA
					lsQuery = new StringBuffer();		
					lsQuery.append(" UPDATE com_tasa_pyme_if");
					lsQuery.append(" SET fn_valor  = " + lsTasa);
					lsQuery.append(" ,cc_acuse     = '" + cc_acuse+"'");
					if(auxTipoTasa.equals("R")) {
						lsQuery.append(" ,df_aplicacion = " + lsFecha);						
						lsQuery.append(" ,df_aplicacion_fin = " + lsFechaFin);
					} else {
						lsQuery.append(" ,df_aplicacion = " + lsFecha);
					}
					
					lsQuery.append(" ,CS_TASA_FLOATING = '" + lsFloating+"'");
					
					lsQuery.append(" WHERE ic_cuenta_bancaria = " + lsCuentaBancaria );
					lsQuery.append(" AND ic_if          = "+ esCveIf);
					lsQuery.append(" AND ic_epo         = " + lsCveEpo);
					lsQuery.append(" AND cc_tipo_tasa   = '"+ esTipoTasa + "'");
					lsQuery.append(" AND ic_plazo       = "+lsPlazo);
					lsQuery.append(" AND ic_orden       = " + ordenApp+"");
					
					log.debug("update -->: "+lsQuery.toString());	
					
					ps = con.queryPrecompilado(lsQuery.toString());
					ps.executeUpdate();
					ps.close();
			
					// ACTUALIZAR BITACORA CON LA INFORMACION DE LA TASA QUE SE ACTUALIZO
					lsQuery2 = new StringBuffer();
					String estatus= "A";
					lsQuery2.append(	"	insert into Bit_Tasas_Preferenciales  "+
											" (ic_bitacora,ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
											" fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus, CS_TASA_FLOATING) "+	
											" VALUES(SEQ_BIT_TASAS_PREFERENCIALES.NEXTVAL,"+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
											esTipoTasa+"',"+esCveMoneda +","+	lsPuntos+","+
											lsValorTasa +",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"' , '"+lsFloating+"' )");
				
					log.debug("Bit_Tasas_Preferenciales : "+lsQuery2.toString());		
					ps = con.queryPrecompilado(lsQuery2.toString());
					ps.executeUpdate();
					ps.close();
			
				}
 
			} //fin del for
 
		} catch (Exception e) {
			lbOK = false;
			log.error("guardarTasasCargaMasiva(Exception)");
			log.error("guardarTasasCargaMasiva.esCveIf       =<"+esCveIf+">");
			log.error("guardarTasasCargaMasiva.esCveMoneda   =<"+esCveMoneda+">");
			log.error("guardarTasasCargaMasiva.lsCveEpo      =<"+lsCveEpo+">");
			log.error("guardarTasasCargaMasiva.esTipoTasa    =<"+esTipoTasa+">");
			log.error("guardarTasasCargaMasiva.cc_acuse      =<"+cc_acuse+">");
			log.error("guardarTasasCargaMasiva.ic_usuario    =<"+ic_usuario+">");
			log.error("guardarTasasCargaMasiva._acuse        =<"+_acuse+">");
			log.error("guardarTasasCargaMasiva.lsListaTasas  =<"+lsListaTasas+">");
			log.error("guardarTasasCargaMasiva.nombreUsuario =<"+nombreUsuario+">");
			log.error("guardarTasasCargaMasiva.con           =<"+con+">");
        	e.printStackTrace();
			throw new AppException("Error al Guardar Tasas de la Carga Masiva");
		} finally {
	    /*    con.terminaTransaccion(lbOK);
			 if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			 }*/            
		}
		log.info("guardarTasasCargaMasiva(S)");
		return lbOK;
    }
 
	 /**
	 * 
	 * Elimina las Tasas Indicadas por la Carga Masiva
    *
	 */
 	 private boolean eliminarTasasCargaMasiva(
		 	String 		esCveIf, 
			String 		esCveMoneda,
			String 		lsCveEpo,
			String 		esTipoTasa,
			String 		cc_acuse,
			String 		ic_usuario,
			String 		_acuse, 
			String[] 	lsListaTasas,
			String 		nombreUsuario,
			AccesoDB 	con)
	throws AppException {
		 /*
		 	String esCveIf, 
			String esCveMoneda,
			String esCvePyme, 
			String esTipoTasa,
			String[] esCadenaTasas, 
			String[] esValorTasas,
			String nombreUsuario,
			String lsPuntos
		 )throws AppException{
		 */
		 log.info("eliminarTasasCargaMasiva(E)");
		 
		 if(lsListaTasas == null || lsListaTasas.length == 0 ){
			log.info("eliminarTasasCargaMasiva(S)");
			return false;	
		}
	
		 log.info("esCveIf        "+esCveIf);
		 log.info("esCveMoneda    "+esCveMoneda);
		 //log.info("esCvePyme      "+esCvePyme);
		 log.info("esTipoTasa     "+esTipoTasa);
		 //log.info("esCadenaTasas  "+esCadenaTasas.length);
		 log.info("nombreUsuario  "+nombreUsuario);
		// log.info("lsPuntos       "+lsPuntos);
 
		 PreparedStatement 	ps 					= null;
		 boolean 				transaccion 		= true;
		 String 					lsNoTasa 			= "";
		 int 						liRegistros 		= 0;
		 StringBuffer 			lsQuery2 			= new StringBuffer();
		 StringBuffer 			lsQuery 				= new StringBuffer();
		 ResultSet 				lrsResultado 		= null;
		 String 					lsCuentaBancaria 	= "";
		 String 					lsPlazo 				= "";
		 String 					lsValorTasa 		= "";
		 int 						ordenApp			 	= 1;
     String         esCvePyme = "";
     String         lsPuntos  = "";
     String[]       registro = null;
		 
		 try{
			 
			for(int i=0; i < lsListaTasas.length; i++) {
				
				liRegistros 	= 0;
				registro			= lsListaTasas[i].split("\\|");
				// log.debug ("lovDatos"+lovDatos);

				esCvePyme 		= registro[0];
				lsPlazo			= registro[1];
				lsValorTasa 	= registro[2];
				lsPuntos			= registro[3];
				lsNoTasa			= registro[4]; 
 
				//lovDatos = new Vector();
				//lovDatos = Comunes.explode("|",esCadenaTasas[i]);	
				
				//lsCveEpo = lovDatos.get(0).toString();
				//lsPlazo	= lovDatos.get(2).toString();
				//lsNoTasa = lovDatos.get(3).toString();
				
        /*
				if ( esTipoTasa.equals("N")){
					lsTasa = esValorTasas[i];
				}
				if ( esTipoTasa.equals("P")){
					lsTasa = null;
				}*/
 
				if(esTipoTasa.equals("R"))
					ordenApp = 2;
				else if(esTipoTasa.equals("P"))
					ordenApp = 3;
				
				// OBTENER LA CLAVE DE LA CUENTA BANCARIA
				lsQuery = new StringBuffer();
				lsQuery.append(" SELECT cpi.ic_cuenta_bancaria");
				lsQuery.append(" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb");
				lsQuery.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				lsQuery.append(" AND cpi.cs_vobo_if = 'S'");
				lsQuery.append(" AND cpi.cs_borrado = 'N'");
				lsQuery.append(" AND ccb.cs_borrado = 'N' ");
				lsQuery.append(" AND cpi.ic_if      = " + esCveIf);
				lsQuery.append(" AND cpi.ic_epo     = " + lsCveEpo);
				lsQuery.append(" AND ccb.ic_moneda  = " + esCveMoneda);
				lsQuery.append(" AND ccb.ic_pyme    = " + esCvePyme);

				log.debug("lsQuery--->"+lsQuery.toString());
 
				lrsResultado = con.queryDB(lsQuery.toString());
 
				if ( lrsResultado.next()) {
					lsCuentaBancaria = lrsResultado.getString(1);
				} else {
					lsCuentaBancaria = "-1";
				}

				lrsResultado.close();
				con.cierraStatement();
				
				// BORRAR TASA LA TASA ESPECIFICADA
				// IC_CUENTA_BANCARIA", "IC_IF", "IC_EPO", "CC_TIPO_TASA", "IC_PLAZO", "IC_ORDEN"
				lsQuery = new StringBuffer();	
				lsQuery.append(
						" DELETE                      "   +
						"   FROM com_tasa_pyme_if     "   +
						"  WHERE ic_epo 			=     "   + lsCveEpo +  
						"    AND ic_if 			=     "   + esCveIf  + 
						"    AND cc_tipo_tasa 	=     '"   + esTipoTasa +"'"+
						" 	  AND ic_plazo 		=     "   + lsPlazo +
						"    AND ic_cuenta_bancaria = "   + lsCuentaBancaria + 
						" AND IC_ORDEN          =     "   + ordenApp
				);
				System.out.println("lsQuery:::   "+lsQuery);
				ps = con.queryPrecompilado(lsQuery.toString());						
				ps.execute();
				ps.close();
 
				// INSERTAR EN BITACORA EL REGISTRO QUE SE HA ELIMINADO
				lsQuery2 = new StringBuffer();
				String estatus= "E"; //Elimiando						
				lsQuery2.append("	insert into Bit_Tasas_Preferenciales  "+
									 " (ic_bitacora, ic_plazo,  ic_orden, ic_tasa, ic_cuenta_bancaria, ic_epo, ic_pyme, ic_if, cg_tipoTasa, ic_moneda,  "+
									 " fn_puntos, fn_valor, df_fecha_hora, cg_usuario, cg_estatus) "+	
									 " VALUES(SEQ_BIT_TASAS_PREFERENCIALES.NEXTVAL,"+lsPlazo+","+ordenApp+","+lsNoTasa+","+lsCuentaBancaria+","+ lsCveEpo +","+ esCvePyme +","+ esCveIf  +",'"+
										esTipoTasa+"',"+esCveMoneda +","+	lsPuntos+","+
										lsValorTasa +",Sysdate,"+"'"+nombreUsuario+"','"+estatus+"'"+")");
				System.out.println("lsQuery2::: --->  " + lsQuery2);
				ps = con.queryPrecompilado(lsQuery2.toString());
				ps.executeUpdate();
				ps.close();
				
				
			}//se termina el for
 
		 }catch(Exception e){
			 log.info("eliminarTasasCargaMasiva(Exception)");
			 log.info("eliminarTasasCargaMasiva.esCveIf       = <"+esCveIf+">");
			 log.info("eliminarTasasCargaMasiva.esCveMoneda   = <"+esCveMoneda+">");
			 log.info("eliminarTasasCargaMasiva.lsCveEpo      = <"+lsCveEpo+">");
			 log.info("eliminarTasasCargaMasiva.esTipoTasa    = <"+esTipoTasa+">");
			 log.info("eliminarTasasCargaMasiva.cc_acuse      = <"+cc_acuse+">");
			 log.info("eliminarTasasCargaMasiva.ic_usuario    = <"+ic_usuario+">");
			 log.info("eliminarTasasCargaMasiva._acuse        = <"+_acuse+">");
			 log.info("eliminarTasasCargaMasiva.lsListaTasas  = <"+lsListaTasas+">");
			 log.info("eliminarTasasCargaMasiva.nombreUsuario = <"+nombreUsuario+">");
			 log.info("eliminarTasasCargaMasiva.con           = <"+con+">");
			 transaccion = false;
			 e.printStackTrace();
			 throw new AppException("Error al Eliminar Tasa de la Carga Masiva");
		 }finally{
			 /*
			 if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			 }
			 */
			 log.info("eliminarTasasCargaMasiva(S)");
		 }
		 return transaccion;
	 }	 
	 
   public boolean guardaModificacionesCargaMasiva(
    		String 		esCveIf, 
			String 		esCveMoneda,
			String 		lsCveEpo,
			String 		esTipoTasa,
			String 		cc_acuse,
			String 		ic_usuario,
			String 		_acuse, 
			String[] 	lsListaTasasInsertar,
			String[] 	lsListaTasasEliminar,
			String 		nombreUsuario){
      boolean lbOK = true;
      AccesoDB con =new AccesoDB();
      try {
        con.conexionDB();
        guardarTasasCargaMasiva( esCveIf, esCveMoneda, lsCveEpo, esTipoTasa, cc_acuse, ic_usuario, _acuse, 	lsListaTasasInsertar, nombreUsuario, con);
        eliminarTasasCargaMasiva( esCveIf, esCveMoneda, lsCveEpo, esTipoTasa, cc_acuse, ic_usuario, _acuse, lsListaTasasEliminar, nombreUsuario, con);
      }catch(Exception e) {
        lbOK = false; 
      }finally{
        if(con.hayConexionAbierta()){
					con.terminaTransaccion(lbOK);
					con.cierraConexionDB();
				}  
      }
      return lbOK;
   }
	 //FODEA 049 - 2010 ACF (I)
	 /**
		* M�todo que obtiene el valor de los puntos limite adicionales con los que se puede
		* realizar la captura de tasas.
		* @return parametrizacionPuntosLimite HashMap con los valores de la �ltima parametrizaci�n realizada.
		* @throws AppException Cuando ocurre un error en la consulta.
		* @author Alberto Cruz Flores.
		*/
		public HashMap obtenerParametrizacionPuntosLimite() throws AppException {
			log.info("obtenerParametrizacionPuntosLimite(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap parametrizacionPuntosLimite = new HashMap();
			try {
				con.conexionDB();
				
				strSQL.append(" SELECT NVL(MAX(ic_puntos_limite), 0) AS ic_puntos_limite");
				strSQL.append(" FROM com_puntos_limite");
				
				log.debug("..:: strSQL: "+strSQL.toString());
				
				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();
				
				while (rst.next()) {
					String icPuntosLimite = rst.getString("ic_puntos_limite")==null?"0":rst.getString("ic_puntos_limite");
					if (!icPuntosLimite.equals("0")) {
						strSQL = new StringBuffer();
						
						strSQL.append(" SELECT fn_puntos_limite,");
						strSQL.append(" cg_usuario,");
						strSQL.append(" TO_CHAR(df_modificacion, 'dd/mm/yyyy') df_modificacion");
						strSQL.append(" FROM com_puntos_limite");
						strSQL.append(" WHERE ic_puntos_limite = ?");
						
						varBind.add(new Integer(icPuntosLimite));
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
						ResultSet rst1 = pst1.executeQuery();
						
						while (rst1.next()) {
							parametrizacionPuntosLimite.put("valorPuntosLimite", rst1.getString("fn_puntos_limite")==null?"":rst1.getString("fn_puntos_limite"));
							parametrizacionPuntosLimite.put("usuarioModificacion", rst1.getString("cg_usuario")==null?"":rst1.getString("cg_usuario"));
							parametrizacionPuntosLimite.put("fechaModificacion", rst1.getString("df_modificacion")==null?"":rst1.getString("df_modificacion"));
						}

						rst1.close();
						pst1.close();
					}
				}
				
				rst.close();
				pst.close();
			} catch (Exception e) {
				log.info("obtenerParametrizacionPuntosLimite(ERROR) ::..");
				throw new AppException("Error al consultar base de datos: ", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
				log.info("obtenerParametrizacionPuntosLimite(S) ::..");
			}
			return parametrizacionPuntosLimite;
		}
		
	 /**
		* M�todo que guarda el valor de los puntos limite adicionales con los que se puede
		* realizar la captura de tasas.
		* @param parametrosPuntosLimite HashMap que contiene los parametros a guardar.
		* @throws AppException Cuando ocurre un error al guardar la parametrizaci�n.
		* @author Alberto Cruz Flores.
		*/
		public void guardarParametrizacionPuntosLimite(HashMap parametrosPuntosLimite) throws AppException {
			log.info("guardarParametrizacionPuntosLimite(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			boolean transOk = true;
			try {
				con.conexionDB();
				
				strSQL.append(" SELECT NVL(MAX(ic_puntos_limite), 0) + 1 AS ic_puntos_limite");
				strSQL.append(" FROM com_puntos_limite");
				
				log.debug("..:: strSQL: "+strSQL.toString());
				
				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();
				
				while (rst.next()) {
					String icPuntosLimite = rst.getString("ic_puntos_limite")==null?"0":rst.getString("ic_puntos_limite");
					String fnPuntosLimite = parametrosPuntosLimite.get("fnPuntosLimite")==null?"00.0000":(String)parametrosPuntosLimite.get("fnPuntosLimite");
					String cgUsuario = parametrosPuntosLimite.get("cgUsuario")==null?"":(String)parametrosPuntosLimite.get("cgUsuario");
					if (!icPuntosLimite.equals("0")) {
						strSQL = new StringBuffer();

						strSQL.append(" INSERT INTO com_puntos_limite (ic_puntos_limite, fn_puntos_limite, cg_usuario)");
						strSQL.append(" VALUES (?, ?, ?)");
						
						varBind.add(new Integer(icPuntosLimite));
						varBind.add(new BigDecimal(fnPuntosLimite));
						varBind.add(cgUsuario);
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
						pst1.executeUpdate();
						pst1.close();
					}
				}
				
				rst.close();
				pst.close();
			} catch (Exception e) {
				transOk = false;
				log.info("guardarParametrizacionPuntosLimite(ERROR) ::..");
				throw new AppException("Error al guardar en la base de datos: ", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(transOk);
					con.cierraConexionDB();
				}
				log.info("guardarParametrizacionPuntosLimite(S) ::..");
			}
		}
		
	 /**
		* M�todo que obtiene el valor de la parametrizaci�n por epo de los puntos limite adicionales con los que se puede
		* realizar la captura de tasas.
		* @return parametrizacionPuntosLimite HashMap con los valores de la �ltima parametrizaci�n realizada.
		* @throws AppException Cuando ocurre un error en la consulta.
		* @author Alberto Cruz Flores.
		*/
		public HashMap obtenerParametrizacionPuntosLimitePorEpo(String claveEpo, String csParametroLimite) throws AppException {
			log.info("obtenerParametrizacionPuntosLimitePorEpo(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap parametrizacionPuntosLimitePorEpo = new HashMap();
			try {
				con.conexionDB();
				
				strSQL.append(" SELECT epo.ic_epo AS clave_epo,");
				strSQL.append(" epo.cg_razon_social AS nombre_epo,");
				strSQL.append(" DECODE(pme.cg_valor, 'S', 'SI', 'N', 'NO') AS cs_puntos_limite");
				strSQL.append(" FROM comcat_epo epo");
				strSQL.append(", com_parametrizacion_epo pme");
				strSQL.append(" WHERE epo.ic_epo = pme.ic_epo");
				strSQL.append(" AND pme.cc_parametro_epo = ?");
				
				varBind.add("CS_PUNTOS_LIMITE");
				
				if (!claveEpo.equals("")) {
					strSQL.append(" AND pme.ic_epo = ?");
					varBind.add(new Integer(claveEpo));
				}

				if (!csParametroLimite.equals("")) {
					strSQL.append(" AND pme.cg_valor = ?");
					varBind.add(csParametroLimite);
				}
				
				strSQL.append(" ORDER BY epo.cg_razon_social");
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);

				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();
				
				int numeroRegistros = 0;
				
				while (rst.next()) {
					HashMap puntosLimitePorEpo = new HashMap();
					puntosLimitePorEpo.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
					puntosLimitePorEpo.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
					puntosLimitePorEpo.put("csPuntosLimite", rst.getString("cs_puntos_limite")==null?"":rst.getString("cs_puntos_limite"));
					parametrizacionPuntosLimitePorEpo.put("puntosLimitePorEpo"+numeroRegistros, puntosLimitePorEpo);
					numeroRegistros++;
				}

				rst.close();
				pst.close();

				parametrizacionPuntosLimitePorEpo.put("numeroRegistros", Integer.toString(numeroRegistros));
			} catch (Exception e) {
				log.info("obtenerParametrizacionPuntosLimitePorEpo(ERROR) ::..");
				throw new AppException("Error al consultar base de datos: ", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
				log.info("obtenerParametrizacionPuntosLimitePorEpo(S) ::..");
			}
			return parametrizacionPuntosLimitePorEpo;
		}
		
	 /**
		* M�todo que guarda el la parametrizaci�n por epo de los puntos limite para la 
		* captura de tasas.
		* @param parametrosPuntosLimite HashMap que contiene los parametros a guardar.
		* @throws AppException Cuando ocurre un error al guardar la parametrizaci�n.
		* @author Alberto Cruz Flores.
		*/
		public void cambiarParametrizacionPuntosLimite(String claveEpo, String csPuntosLimite) throws AppException {
			log.info("cambiarParametrizacionPuntosLimite(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			boolean transOk = true;
			try {
				con.conexionDB();

				strSQL.append(" UPDATE com_parametrizacion_epo");
				strSQL.append(" SET cg_valor = ?");
				strSQL.append(" WHERE cc_parametro_epo = ?");
				strSQL.append(" AND ic_epo = ?");
				
				varBind.add(csPuntosLimite);
				varBind.add("CS_PUNTOS_LIMITE");
				varBind.add(new Integer(claveEpo));
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				pst.executeUpdate();
				pst.close();
			} catch (Exception e) {
				transOk = false;
				log.info("cambiarParametrizacionPuntosLimite(ERROR) ::..");
				throw new AppException("Error al guardar en la base de datos: ", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(transOk);
					con.cierraConexionDB();
				}
				log.info("cambiarParametrizacionPuntosLimite(S) ::..");
			}
		}
		//FODEA 049 - 2010 ACF (F)
	//---------------------------------Fodea 017-2011---------------------------------------
	
	
	 public String getTasaAutIFEXT (String claveEPO, String claveIF, String cveMoneda) throws AppException {
    log.info("getTasaAutIFEXT(E) ::..");
    AccesoDB con = new AccesoDB();
    ResultSet rs = null;
    JSONArray registros = new JSONArray();
		HashMap registro = null;
		String tasa_aplicar = "";
		String valor_tasa = "";
		String relmat_tasa = "";
		String puntos_tasa = "";

    try {
      con.conexionDB();

			String  qrySentencia =
				" SELECT (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social"   +
				"        ) AS nombreif,"   +
				"        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin,"   +
				"        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS dc_fecha_tasa,"   +
				"        t.cd_nombre AS nombretasa, m.cd_nombre AS nombremoneda, i.ic_if"   +
				"		 ,p.cg_descripcion AS rangoplazo, ta.cg_rel_mat as relmat, ta.fn_puntos as ptos, mt.fn_valor as valor" +
				"		 , TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS fechatasa" +
				"   FROM comcat_if i,"   +
				"        comrel_tasa_autorizada ta,"   +
				"        comcat_tasa t,"   +
				"        comcat_moneda m,"   +
				"        comrel_if_epo_moneda iem,"   +
				"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"             FROM com_mant_tasa"   +
				"         GROUP BY ic_tasa) x,"   +
				"        (SELECT   tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa"   +
				"             FROM comrel_tasa_base tb"   +
				"            WHERE tb.ic_epo = " + claveEPO+
				"			  AND tb.CS_TASA_ACTIVA = 'S'"+
				"         GROUP BY tb.ic_epo, tb.ic_tasa) tbu"   +
				"		 ,comrel_tasa_base tb,  comcat_plazo p, com_mant_tasa mt"+
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"   +
				"    AND ta.ic_epo = " + claveEPO+
				"    AND ta.ic_if = i.ic_if"   +
				"    AND ta.ic_epo = tbu.ic_epo"   +
				"    AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"    AND tbu.ic_tasa = t.ic_tasa"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND iem.ic_epo = ta.ic_epo"   +
				"    AND iem.ic_if = ta.ic_if"   +
				"    AND iem.ic_moneda = m.ic_moneda"   +
				"    AND i.cs_habilitado = 'S'"   ; 
				if(!claveIF.equals("")){
					qrySentencia += "    AND ta.ic_if in( "+claveIF+")";
				}
				qrySentencia +="  AND M.IC_MONEDA in("+cveMoneda+")"+
				 "	 AND tbu.ic_tasa = tb.ic_tasa"+
    			"	 AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa"+
    			" 	 AND tb.ic_plazo = p.ic_plazo"+
    			"	 AND mt.dc_fecha = x.dc_fecha"+
    			"	 AND t.ic_tasa = mt.ic_tasa"+
					" order by i.cg_razon_social, t.cd_nombre ";
			rs = con.queryDB(qrySentencia);

			System.out.println("qrySentencia--->"+qrySentencia);
     
			while (rs.next()) {

				registro = new HashMap();
				registro.put("NOMBREIF", (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") ); 
				registro.put("NOMBREMONEDA", (rs.getString("NOMBREMONEDA") == null)?"":rs.getString("NOMBREMONEDA") );
				registro.put("NOMBRETASA", (rs.getString("NOMBRETASA") == null)?"":rs.getString("NOMBRETASA") );
				registro.put("RANGOPLAZO", (rs.getString("rangoplazo") == null)?"":rs.getString("rangoplazo") );
				registro.put("VALOR", (rs.getString("valor") == null)?"":rs.getString("valor") );
				registro.put("RELMAT", (rs.getString("relmat") == null)?"":rs.getString("relmat") );
				registro.put("PTOS", (rs.getString("ptos") == null)?"":rs.getString("ptos") );
				relmat_tasa = (rs.getString("relmat") == null)?"":rs.getString("relmat");
				puntos_tasa  = (rs.getString("ptos") == null)?"":rs.getString("ptos");
				valor_tasa = (rs.getString("VALOR") == null)?"":rs.getString("VALOR");
				tasa_aplicar = "";
				
				if(relmat_tasa.equals("+")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
				}
				if(relmat_tasa.equals("-")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
				}
				if(relmat_tasa.equals("/")) {
					if (!puntos_tasa.equals("0")) {
						tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
					}
				}
				if(relmat_tasa.equals("*")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
				}
				registro.put("TASA_APLICAR", tasa_aplicar );		
				registros.add(registro);				
			}

			rs.close();
			con.cierraStatement();		
	
			return "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			
								
				
    } catch (Exception e) {
      log.error("getParamPorEPOEXTJS(Error) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("getParamPorEPOEXTJS(S) ::..");
    }
   
  }
		
	public String consultaHistTasaEXT(String ic_epo, String fecha) throws AppException {
		log.info("consultaHistTasaEXT(E) ::..");
		String				qrySentencia	= "";
    ResultSet			lrsResultado	= null;
    PreparedStatement	ps				= null;     
		JSONArray alListaEpo = new JSONArray();			
		HashMap hmParametros = null;
   	AccesoDB con  = new AccesoDB();

    try {
      con.conexionDB();

			if(fecha.equals("")) {
        		qrySentencia =
					" SELECT DISTINCT TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY'), tb.ic_epo"   +
					"            FROM (SELECT MAX (tb2.dc_fecha_tasa) AS fecha"   +
					"                    FROM comrel_tasa_base tb2"   +
					"                   WHERE tb2.ic_epo =  ?  AND tb2.cs_vobo_nafin = 'S') tbu,"   +
					"                 comrel_tasa_base tb"   +
					"           WHERE tb.ic_epo = ?"   +
					"             AND tb.cs_vobo_nafin = 'S'"   +
					"             AND TRUNC (tb.dc_fecha_tasa) != TRUNC (tbu.fecha)"  ;
        	} else {
        		qrySentencia =
					" SELECT DISTINCT TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY'), tb.ic_epo"   +
					"            FROM comrel_tasa_base tb"   +
					"           WHERE tb.ic_epo = ?"   +
					"             AND tb.cs_vobo_nafin = 'S'"   +
					"             AND TRUNC (tb.dc_fecha_tasa) != TRUNC (TO_DATE (?, 'DD/MM/YY'))"  ;
        	}
        		  
			ps = con.queryPrecompilado(qrySentencia);
			if(fecha.equals("")) {
				ps.setInt(1, Integer.parseInt(ic_epo));
				ps.setInt(2, Integer.parseInt(ic_epo));
			}else{
				ps.setInt(1, Integer.parseInt(ic_epo));
				ps.setString(2, fecha);
			}
			lrsResultado = ps.executeQuery();
			while(lrsResultado.next()){			
				hmParametros = new HashMap();
				hmParametros.put("FECHA",(lrsResultado.getString(1)==null?"":lrsResultado.getString(1)));
				hmParametros.put("EPO",(lrsResultado.getString(2)==null?"":lrsResultado.getString(2)));
				alListaEpo.add(hmParametros);
			
			}
			lrsResultado.close();
			if(ps!=null) ps.close();

			return "({\"success\": true, \"total\": \"" + alListaEpo.size() + "\", \"registros\": " + alListaEpo.toString()+"})";
			
				
    } catch (Exception e) {
      log.error("consultaHistTasaEXT(Error) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("consultaHistTasaEXT(S) ::..");
    }
   
  }
	
	
	
	public String   consultaTasaAutoIfEXT(String ic_epo, String fecha, String ic_IF, String sesIdiomaUsuario) throws AppException {
		log.info("consultaTasaAutoIfEXT(E) ::..");
		
		String				qrySentencia	= "";
		String				condicion		= "";
    StringBuffer		lsQuery 		= new StringBuffer();
    ResultSet			lrsResultado	= null;
    PreparedStatement	ps				= null;
    String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";	
		AccesoDB con  = new AccesoDB();

    try {
      con.conexionDB();

			if(!"".equals(fecha)) {
				condicion =
					"        (SELECT tb2.ic_epo, tb2.ic_tasa, tb2.dc_fecha_tasa"   +
					"           FROM comrel_tasa_base tb2"   +
					"          WHERE tb2.ic_epo = "+ic_epo+
					"            AND tb2.cs_vobo_nafin = 'S'"   +
				//	"            AND tb2.cs_tasa_activa = 'S'"   +
					"            AND TRUNC (tb2.dc_fecha_tasa) = TRUNC (TO_DATE ('"+fecha+"', 'dd/mm/yyyy'))) tbu";

        	} else {
        		condicion =
					"        (SELECT   tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa"   +
					"             FROM comrel_tasa_base tb"   +
					"            WHERE tb.ic_epo = "+ic_epo+" AND tb.cs_vobo_nafin = 'S'"   +
				//	"            AND tb.cs_tasa_activa = 'S'"   +
					"         GROUP BY tb.ic_epo, tb.ic_tasa) tbu";
        	}

        	qrySentencia =
				" SELECT (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social),"   +
				"        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin,"   +
				"        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS'), t.cd_nombre,"   +
				"        m.cd_nombre"+idioma+" as cd_nombre, i.ic_if, m.ic_moneda, ta.cg_rel_mat as relacion, ta.fn_puntos as puntos, " +
        		"		 p.cg_descripcion as rangoplazo, mt.fn_valor as valor "   +
				"   FROM comcat_if i,"   +
				"        comrel_tasa_autorizada ta,"   +
				"        comcat_tasa t,"   +
				"        comcat_moneda m,"   +
				"		 comrel_tasa_base tb, " +
        		"		 comcat_plazo p, " +
        		"		 com_mant_tasa mt, " +
				"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
				"             FROM com_mant_tasa"   +
				"         GROUP BY ic_tasa) x,"   +
				condicion+
				"  WHERE t.ic_tasa = x.ic_tasa"   +
				"    AND t.cs_disponible = 'S'"   +
				"    AND ta.ic_epo = "+ic_epo+
				"    AND ta.ic_if = i.ic_if"   +
				"    AND t.ic_moneda = m.ic_moneda"   +
				"    AND ta.ic_epo = tbu.ic_epo"   +
				"    AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa"   +
				"    AND tbu.ic_tasa = t.ic_tasa"   +
                "    AND tbu.ic_tasa = tb.ic_tasa" +
 				"    AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa" +
				"    AND tb.ic_plazo = p.ic_plazo" +
				"    AND mt.dc_fecha = x.dc_fecha" +
				"    AND t.ic_tasa = mt.ic_tasa" +
				"    AND ta.cs_vobo_nafin = 'S'"  ;
            //new 08/11/2004
			if(!ic_IF.equals("")) {
				 qrySentencia+=" AND ta.ic_if in("+ic_IF+")";
			}//if icIF
			qrySentencia+=" ORDER BY i.cg_razon_social, m.cd_nombre ";
			//end new 08/11/2004
       	lsQuery.append(qrySentencia);		   	


	System.out.println("\n lsQuery.toString(): "+lsQuery.toString());

			ps = con.queryPrecompilado(lsQuery.toString());
			lrsResultado = ps.executeQuery();
			
			JSONArray renglones = new JSONArray();
			HashMap columnas = null;
			String relmat_tasa = "",  puntos_tasa = "", valor_tasa = "",  tasa_aplicar = "";		
			
			while(lrsResultado.next()){
				columnas = new HashMap();
				columnas.put("NOMIFV", lrsResultado.getString(1)==null?"":lrsResultado.getString(1) );
				columnas.put("NOMIFV", lrsResultado.getString(1)==null?"":lrsResultado.getString(1) );
				columnas.put("MONEDAV", lrsResultado.getString(7)==null?"":lrsResultado.getString(7) );
				columnas.put("TASABASEV", lrsResultado.getString(6)==null?"":lrsResultado.getString(6) );				
				columnas.put("PLAZOV", lrsResultado.getString(12)==null?"":lrsResultado.getString(12) ); 
				columnas.put("VALORV",lrsResultado.getString(13)==null?"":lrsResultado.getString(13));
				columnas.put("RETMATV", lrsResultado.getString(10)==null?"":lrsResultado.getString(10));
				columnas.put("PTOSV", lrsResultado.getString(11)==null?"":lrsResultado.getString(11));
				
				relmat_tasa = lrsResultado.getString(10)==null?"":lrsResultado.getString(10);				
				puntos_tasa = lrsResultado.getString(11)==null?"":lrsResultado.getString(11);
				valor_tasa = lrsResultado.getString(13)==null?"":lrsResultado.getString(13);
				tasa_aplicar = ""; 
				 
				if(relmat_tasa.equals("+")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
				}
				if(relmat_tasa.equals("-")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
				}
				if(relmat_tasa.equals("/")) {
					if (!puntos_tasa.equals("0")) {
						tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
					}
				}
				if(relmat_tasa.equals("*")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
				}
				columnas.put("TASA_APLICARV",tasa_aplicar );
				renglones.add(columnas);				
			}
			lrsResultado.close();
			if(ps!=null) ps.close();
			
	
			return "({\"success\": true, \"total\": \"" + renglones.size() + "\", \"registros\": " + renglones.toString()+"})";
			
				
    } catch (Exception e) {
      log.error("consultaTasaAutoIfEXT(Error) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("consultaTasaAutoIfEXT(S) ::..");
    }
   
  }
	
	
	/**
	 * se obtiene los IF's relacionados con la Pyme y EPO
	 * @return 
	 * @param ic_pyme
	 * @param ic_epo
	 */
	 public String IfparaTasas(String ic_epo, String ic_pyme) 	{    
		log.info("IfparaTasas (E) ");
    
	  ResultSet	rs				= null;
    PreparedStatement ps = null;
	  AccesoDB 	con				= null;
    boolean transaccion = true;
		String noIfs ="";
    
    try {
			con = new AccesoDB();
      con.conexionDB();
			String qrySentencia = " SELECT DISTINCT (pi.ic_if) as ic_if, i.cg_razon_social as cg_razon_social"   +
								"            FROM comrel_pyme_if pi, comcat_if i"   +
								"           WHERE ic_epo IN ("   +
								"                    SELECT rpe.ic_epo"   +
								"                      FROM comcat_epo e, comrel_pyme_epo rpe"   +
								"                     WHERE rpe.ic_epo = e.ic_epo"   +
								"                       AND e.cs_habilitado = 'S'"   +
								"                       AND rpe.ic_pyme = ?"   +
								"                       AND rpe.ic_epo = ? "+
								"             AND pi.ic_if = i.ic_if )"  ;
							
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_pyme));	
		ps.setInt(2,Integer.parseInt(ic_epo));	
		rs = ps.executeQuery();
		while(rs.next()){		
				noIfs +=  (rs.getString("ic_if")==null)?"":rs.getString("ic_if")+",";			
		}		
		rs.close();
		ps.close();
		
		if(!noIfs.equals("")){
				int tamanio2 =	noIfs.length();
				String valor =  noIfs.substring(tamanio2-1, tamanio2);
				if(valor.equals(",")){
					noIfs =noIfs.substring(0,tamanio2-1);
				}
			}
			
    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();     
      log.error("IfparaTasas "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
			
    }
		
    log.info("IfparaTasas (S) ");
		return noIfs;
				
	}
	


	/**
	 * se obtiene el nombre de la EPO
	 * @return 
	 * @param ic_epo
	 */
	 public String nombreEPO(String ic_epo) 	{    
		log.info("nomnbreEPO (E) ");
    
	  ResultSet	rs				= null;
    PreparedStatement ps = null;
	  AccesoDB 	con				= null;
    boolean transaccion = true;
		String nombreEPO ="";
    
    try {
			con = new AccesoDB();
      con.conexionDB();
			String qrySentencia = " SELECT cg_razon_social"   +
								"  FROM comcat_epo "   +
								"  WHERE ic_epo = ?"  ;
							
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_epo));	
		rs = ps.executeQuery();
		while(rs.next()){		
				nombreEPO =  (rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social");			
		}
		rs.close();
		ps.close();
			
    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();     
      log.error("nombreEPO "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
			
    }
		
    log.info("nombreEPO (S) ");
		return nombreEPO;
				
	}
	
	
	/**
	 * Se obtienen la oferta de tasas de la relacion if - epo - moneda
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	public List getTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda) throws AppException{
		log.info("getTasasOfertaIf(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lstTasas = new ArrayList();
		HashMap mp = new HashMap();
		try{
			con.conexionDB();
			String query = "";
			
			query = "SELECT co.ic_oferta_tasa cvetasa, co.ic_if cveif, co.ic_epo cveepo, co.cc_tipo_tasa tipotasa, " +
					"       co.fg_montodesde montodesde, co.fg_montohasta montohasta, co.fn_puntos puntospref, co.cg_estatus estatus, " +
					"       co.ic_moneda cvemoneda, co.cc_acuse acuse " +
					"  FROM com_oferta_tasa_if_epo co " +
					" WHERE co.ic_if = ? AND co.ic_epo = ? AND co.ic_moneda = ? "+
					" ORDER BY  co.ic_oferta_tasa ";

				
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(sesCveIf));
			ps.setLong(2,Long.parseLong(cboEpo));
			ps.setLong(3,Long.parseLong(cboMoneda));
			
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				mp = new HashMap();
				mp.put("CVETASA",rs.getString("cvetasa"));
				mp.put("CVEIF", rs.getString("cveif"));
				mp.put("CVEEPO", rs.getString("cveepo"));
				mp.put("TIPOTASA", rs.getString("tipotasa"));
				mp.put("MONTODESDE", rs.getString("montodesde"));
				mp.put("MONTOHASTA", rs.getString("montohasta"));
				mp.put("PUNTOSPREF", rs.getString("puntospref"));
				mp.put("ESTATUS", rs.getString("estatus"));
				mp.put("CVEMONEDA", rs.getString("cvemoneda"));
				mp.put("ACUSE", rs.getString("acuse"));
				lstTasas.add(mp);
			}
			
			rs.close();
			ps.close();
			
			return lstTasas;

		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al consultar Oferta de Tasas",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getTasasOfertaIf(S)");
		}
	}
	
	
	public void setTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException{
		log.info("setTasasOfertaIf(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		boolean commit = true;
		BitOfertaTasas bitacora  = new BitOfertaTasas();
		HashMap mapAnterior = new HashMap();
		HashMap mapActual = new HashMap();
		
		try{
			con.conexionDB();
			String query = "";
			
			if(lstTasasOfer!=null){
				for(int i=0; i<lstTasasOfer.size();i++){
					Map map = (Map)lstTasasOfer.get(i);
					String cvetasa = map.get("CVETASA")==null?"":String.valueOf(map.get("CVETASA"));
					if(!"".equals(cvetasa)){
						
						
						mapAnterior = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						
						//UPDATE
						query = "UPDATE com_oferta_tasa_if_epo " +
								"   SET fg_montodesde = ?, " +
								"       fg_montohasta = ?, " +
								"       fn_puntos = ?, " +
								"       cg_estatus = ?, " +
								"       cc_acuse = ? " +
								" WHERE ic_oferta_tasa = ? AND ic_if = ?  " +
								" AND ic_epo = ? AND ic_moneda = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setDouble(1, Double.parseDouble(String.valueOf(map.get("MONTODESDE"))));
						ps.setDouble(2, Double.parseDouble(String.valueOf(map.get("MONTOHASTA"))));
						ps.setDouble(3, Double.parseDouble(String.valueOf(map.get("PUNTOSPREF"))));
						ps.setString(4, String.valueOf(map.get("ESTATUS")));
						ps.setString(5, acuse.toString());
						ps.setLong(6, Long.parseLong(String.valueOf(map.get("CVETASA"))));
						ps.setLong(7, Long.parseLong(sesCveIf));
						ps.setLong(8, Long.parseLong(cboEpo));
						ps.setLong(9, Long.parseLong(cboMoneda));
						ps.executeUpdate();
						ps.close();
						
						mapActual = bitacora.getInfoOfertaTasas(con,cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA

						BitOfertaTasas.agregarRegBitacora(con, usuario, cvetasa, mapAnterior, mapActual, "", "");//CODIGO PARA GUARDAR INFO EN BITACORA

						
					}else{
						//INSERT
						String icOferTasa = "";
						query = "SELECT seq_com_oferta_tasa_if_epo.NEXTVAL from dual ";
						ps = con.queryPrecompilado(query);
						rs = ps.executeQuery();
						if(rs!=null && rs.next()){
							icOferTasa = rs.getString(1);
						}
						rs.close();
						ps.close();
						
						mapAnterior = bitacora.getInfoOfertaTasas(con, icOferTasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						
						query = "INSERT INTO com_oferta_tasa_if_epo " +
							"            (ic_oferta_tasa, ic_if, ic_epo, cc_tipo_tasa, fg_montodesde, " +
							"             fg_montohasta, fn_puntos,  ic_moneda, cc_acuse " +
							"            ) " +
							"     VALUES ( ? , ?, ?, ?, ?, ?, " +
							"             ?, ?, ? " +
							"            )";
						
						ps = con.queryPrecompilado(query);
						ps.setLong(1, Long.parseLong(icOferTasa));
						ps.setLong(2, Long.parseLong(sesCveIf));
						ps.setLong(3, Long.parseLong(cboEpo));
						ps.setString(4, "O");
						ps.setBigDecimal(5, new BigDecimal(String.valueOf(map.get("MONTODESDE"))));
						ps.setBigDecimal(6, new BigDecimal(String.valueOf(map.get("MONTOHASTA"))));
						ps.setBigDecimal(7, new BigDecimal(String.valueOf(map.get("PUNTOSPREF"))));
						ps.setLong(8, Long.parseLong(cboMoneda));
						ps.setString(9, acuse.toString());
						ps.executeUpdate();
						ps.close();
						
						mapActual = bitacora.getInfoOfertaTasas(con, icOferTasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						
						//se verifica si ya hay pymes asociadas a estas tasas para asignarle las nuevas tasas.
						query = "SELECT DISTINCT ctp.ic_cuenta_bancaria ctabancaria " +
									" FROM com_oferta_tasa_if_epo cot, comrel_oferta_tasa_pyme_if ctp " +
									"WHERE cot.ic_oferta_tasa = ctp.ic_oferta_tasa " +
									"  AND cot.ic_moneda = ? " +
									"  AND cot.ic_epo = ? " +
									"  AND cot.ic_if = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setLong(1, Long.parseLong(cboMoneda));
						ps.setLong(2, Long.parseLong(cboEpo));
						ps.setLong(3, Long.parseLong(sesCveIf));
						rs = ps.executeQuery();
						
						PreparedStatement ps2 = null;
						while(rs!=null && rs.next()){
							String ctaBancaria = rs.getString("ctabancaria");
							query = "INSERT INTO comrel_oferta_tasa_pyme_if " +
									"     (ic_cuenta_bancaria, ic_oferta_tasa) " +
									"     VALUES (?, ?)  ";
							ps2 = con.queryPrecompilado(query);
							ps2.setLong(1,Long.parseLong(ctaBancaria));
							ps2.setLong(2,Long.parseLong(icOferTasa));
							ps2.executeUpdate();
							ps2.close();
							
							BitOfertaTasas.agregarRegBitacora(con, usuario, icOferTasa, mapAnterior, mapActual, "", ctaBancaria);//CODIGO PARA GUARDAR INFO EN BITACORA
						}
						
						rs.close();
						ps.close();

					}
				}
			}//END - if(lstTasasOfer!=null)
			
		}catch(Throwable t){
			log.info("setTasasOfertaIf(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al realizar guardado de oderta de tasas ",t);
			
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setTasasOfertaIf(S)");
		}
	}
	
	/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param lstTasasOfer
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	public List getPymesOferTasas(String sesCveIf, String cboEpo, String cboMoneda, String numElecPyme, String csNoParam) throws AppException{
		log.info("getPymesOferTasas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap mp = null;
		List lstPymesTasas = new ArrayList();
		
		try{
			con.conexionDB();
			
			String query = 
						"SELECT DISTINCT cp.cg_rfc rfcpyme, ccb.ic_cuenta_bancaria cuentabancaria, ccb.ic_pyme cvepyme, " +
						"						cp.cg_razon_social razonsocial, nvl(cot.csparam,'N') paramtasas " +
						"    FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, " +
						"         comcat_pyme cp, com_tasa_pyme_if ct, " +
						"			(SELECT ctp.ic_cuenta_bancaria, cot.ic_epo, cot.ic_if, 'S' csparam " +
						"			 FROM com_oferta_tasa_if_epo cot, comrel_oferta_tasa_pyme_if ctp " +
						"				WHERE cot.ic_oferta_tasa = ctp.ic_oferta_tasa " +
						"  	      AND cot.ic_moneda = ? " +
						"        )cot, " +
						"			comrel_nafin cn " +
						"   WHERE cpi.ic_if = ? " +
						"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
						"     AND ccb.ic_pyme = cp.ic_pyme " +
						"		AND cp.ic_pyme = cn.ic_epo_pyme_if " +
						"     AND ct.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria " +
						"     AND ct.ic_epo(+) = cpi.ic_epo " +
						"     AND ct.ic_if(+) = cpi.ic_if " +
						"		AND cot.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria " +
						"     AND cot.ic_epo(+) = cpi.ic_epo " +
						"     AND cot.ic_if(+) = cpi.ic_if " +
						"     AND ccb.ic_moneda = ? " +
						"     AND cpi.ic_epo = ? " +
						"		AND cn.cg_tipo = ? " +
						"     AND cpi.cs_vobo_if = ? " +
						"     AND cpi.cs_borrado = ? " +
						"     AND ccb.cs_borrado = ? " +
						"     AND ct.cc_tipo_tasa(+) != ? ";
			
			if(numElecPyme!=null && !"".equals(numElecPyme))
				query += "		AND cn.ic_nafin_electronico = ? ";
			if(csNoParam != null && "S".equals(csNoParam))
				query += "		AND cot.csparam is null ";
			query += "ORDER BY cp.cg_razon_social ";
			
			log.debug("getPymesOferTasas:query = "+ query);
			
			int p=0;
			ps = con.queryPrecompilado(query);
			ps.setLong(++p, Long.parseLong(cboMoneda));
			ps.setLong(++p, Long.parseLong(sesCveIf) );
			ps.setLong(++p, Long.parseLong(cboMoneda));
			ps.setLong(++p, Long.parseLong(cboEpo));
			ps.setString(++p, "P");
			ps.setString(++p, "S");
			ps.setString(++p, "N");
			ps.setString(++p, "N");
			ps.setString(++p, "P");
			if(numElecPyme!=null && !"".equals(numElecPyme))
				ps.setLong(++p, Long.parseLong(numElecPyme));
				
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				mp = new HashMap();
				mp.put("CVEPYME", rs.getString("cvepyme"));
				mp.put("RFCPYME", rs.getString("rfcpyme"));
				mp.put("CTABANC", rs.getString("cuentabancaria"));
				mp.put("RAZONSOCIAL", rs.getString("razonsocial"));
				mp.put("PARAMTASAS", rs.getString("paramtasas"));
				lstPymesTasas.add(mp);
			}
			
			rs.close();
			ps.close();

			return lstPymesTasas;
		}catch(Throwable t){
			log.info("getPymesOferTasas(Error)");
			t.printStackTrace();
			throw new AppException("Error al obtener pymes para ofeerta de tasas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getPymesOferTasas(S)");
		}
	}
	
		
	/**
	 * Se eliminan rangos de oferta de tasas.
	 * @throws netropology.utilerias.AppException
	 * @param acuse
	 * @param lstTasasOfer
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	public void deleteTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException{
		log.info("deleteTasasOfertaIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean commit = true;
		
		BitOfertaTasas bitacora  = new BitOfertaTasas();
		HashMap mapAnterior = new HashMap();
		HashMap mapActual = new HashMap();
		
		try{
			con.conexionDB();
			String query = "";
			
			if(lstTasasOfer!=null){
				String oldAcuse = "";
				for(int i=0; i<lstTasasOfer.size();i++){
					Map map = (Map)lstTasasOfer.get(i);
					String cvetasa = map.get("CVETASA")==null?"":String.valueOf(map.get("CVETASA"));
					
					if(i==0){//SE ACTUALIZA ACUSE ACTUAL POR EL NUEVO ACUSE
						
						
						oldAcuse = map.get("ACUSE")==null?"":String.valueOf(map.get("ACUSE"));
						query = " UPDATE com_oferta_tasa_if_epo SET cc_acuse = ? " +
								" WHERE cc_acuse = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setString(1,acuse.toString());
						ps.setString(2,oldAcuse);
						ps.executeUpdate();
						ps.close();
						
						
					}
					
					if(!"".equals(cvetasa)){
						mapAnterior = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						BitOfertaTasas.agregarRegBitacora(con, usuario, cvetasa, mapAnterior, mapActual, "E", "");//CODIGO PARA GUARDAR INFO EN BITACORA

						//DELETE
						query = "DELETE comrel_oferta_tasa_pyme_if " +
								"	WHERE ic_oferta_tasa = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setLong(1, Long.parseLong(cvetasa));
						ps.close();
						
						query = "DELETE com_oferta_tasa_if_epo " +
									" WHERE ic_oferta_tasa = ? AND ic_if = ?  " +
								" AND ic_epo = ? AND ic_moneda = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setLong(1, Long.parseLong(cvetasa));
						ps.setLong(2, Long.parseLong(sesCveIf));
						ps.setLong(3, Long.parseLong(cboEpo));
						ps.setLong(4, Long.parseLong(cboMoneda));
						ps.executeUpdate();
						ps.close();

					}
				}
			}//END - if(lstTasasOfer!=null)
			
		}catch(Throwable t){
			log.info("deleteTasasOfertaIf(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al realizar guardado de oderta de tasas ",t);
			
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("deleteTasasOfertaIf(S)");
		}
	}
	
	
	/**
	 * Se activan o inactivan las ofertas de tasa definidas por el Intermediario.
	 * @throws netropology.utilerias.AppException
	 * @param acuse
	 * @param lstTasasOfer
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	public void cambiaEstatusTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, Acuse acuse, String usuario) throws AppException{
		log.info("cambiaEstatusTasasOfertaIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean commit = true;
		
		BitOfertaTasas bitacora  = new BitOfertaTasas();
		HashMap mapAnterior = new HashMap();
		HashMap mapActual = new HashMap();
		
		try{
			con.conexionDB();
			String query = "";
			
			if(lstTasasOfer!=null){
				String oldAcuse = "";
				for(int i=0; i<lstTasasOfer.size();i++){
					Map map = (Map)lstTasasOfer.get(i);
					String cvetasa = map.get("CVETASA")==null?"":String.valueOf(map.get("CVETASA"));
					if(i==0){//SE ACTUALIZA ACUSE ACTUAL POR EL NUEVO ACUSE
						oldAcuse = map.get("ACUSE")==null?"":String.valueOf(map.get("ACUSE"));
						query = " UPDATE com_oferta_tasa_if_epo SET cc_acuse = ? " +
								" WHERE cc_acuse = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setString(1,acuse.toString());
						ps.setString(2,oldAcuse);
						ps.executeUpdate();
						ps.close();
					}
					
					if(!"".equals(cvetasa)){
						mapAnterior = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						
						//UPDATE
						String estatus = String.valueOf(map.get("ESTATUS"));
						estatus = "S".equals(estatus)?"N":"S";
						query = "UPDATE com_oferta_tasa_if_epo " +
								"   SET cg_estatus = ? " +
								" WHERE ic_oferta_tasa = ? AND ic_if = ?  " +
								" AND ic_epo = ? AND ic_moneda = ? ";
						
						ps = con.queryPrecompilado(query);
						ps.setString(1, estatus);
						ps.setLong(2, Long.parseLong(String.valueOf(map.get("CVETASA"))));
						ps.setLong(3, Long.parseLong(sesCveIf));
						ps.setLong(4, Long.parseLong(cboEpo));
						ps.setLong(5, Long.parseLong(cboMoneda));
						ps.executeUpdate();
						ps.close();
						
						mapActual = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
						BitOfertaTasas.agregarRegBitacora(con, usuario, cvetasa, mapAnterior, mapActual, "", "");//CODIGO PARA GUARDAR INFO EN BITACORA
						
					}
				}
			}//END - if(lstTasasOfer!=null)
			
		}catch(Throwable t){
			log.info("cambiaEstatusTasasOfertaIf(Error)");
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al realizar la activacion o inactivacion de oderta de tasas ",t);
			
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("cambiaEstatusTasasOfertaIf(S)");
		}
	}
	
	/**
	 * Se establece la realcion de la oferta de tasas con las pymes seleccionadas
	 * @throws netropology.utilerias.AppException
	 * @param acuse
	 * @param lstPymesAsign
	 * @param lstTasasOfer
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	public void setRelPymeOfertaTasas(String sesCveIf, String cboEpo, String cboMoneda, List lstTasasOfer, List lstPymesAsign, Acuse acuse, String usuario)throws AppException{
		log.info("setRelPymeOfertaTasas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean commit = true;
		String queryDel = "";
		String queryIns = "";
		
		BitOfertaTasas bitacora  = new BitOfertaTasas();
		HashMap mapAnterior = new HashMap();
		HashMap mapActual = new HashMap();
		
		try{
			con.conexionDB();
			
			queryDel = "DELETE      comrel_oferta_tasa_pyme_if " +
						"	WHERE ic_cuenta_bancaria = ? AND ic_oferta_tasa = ? ";
			
			queryIns = "INSERT INTO comrel_oferta_tasa_pyme_if " +
						"     (ic_cuenta_bancaria, ic_oferta_tasa, cc_acuse) " +
						"     VALUES (?, ?, ?)  ";

			if(lstTasasOfer!=null){
				for(int i=0; i<lstTasasOfer.size();i++){
					Map map = (Map)lstTasasOfer.get(i);
					String cvetasa = map.get("CVETASA")==null?"":String.valueOf(map.get("CVETASA"));
					if(lstPymesAsign!=null){
						for(int y=0; y<lstPymesAsign.size();y++){

							Map mapPyme = (Map)lstPymesAsign.get(y);
							String paramtasas = mapPyme.get("PARAMTASAS")==null?"":String.valueOf(mapPyme.get("PARAMTASAS"));
							String ctabanc = mapPyme.get("CTABANC")==null?"":String.valueOf(mapPyme.get("CTABANC"));
							if("N".equals(paramtasas)){//SE INSERTA RELACION
								mapAnterior = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
								ps = con.queryPrecompilado(queryIns);
								ps.setLong(1, Long.parseLong(ctabanc));
								ps.setLong(2, Long.parseLong(cvetasa));
								ps.setString(3, acuse.toString());
								ps.executeUpdate();
								ps.close();
								mapActual = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
								BitOfertaTasas.agregarRegBitacora(con, usuario, cvetasa, mapAnterior, mapActual, "A", ctabanc);//CODIGO PARA GUARDAR INFO EN BITACORA
								
							}else if("S".equals(paramtasas)){// SE ELIMINA RELACION
								mapAnterior = bitacora.getInfoOfertaTasas(con, cvetasa);//CODIGO PARA GUARDAR INFO EN BITACORA
								BitOfertaTasas.agregarRegBitacora(con, usuario, cvetasa, mapAnterior, mapActual, "E", ctabanc);//CODIGO PARA GUARDAR INFO EN BITACORA
								
								ps = con.queryPrecompilado(queryDel);
								ps.setLong(1, Long.parseLong(ctabanc));
								ps.setLong(2, Long.parseLong(cvetasa));
								ps.executeUpdate();
								ps.close();
								
								
							}
							
						}
					}
				}
			}

		}catch(Throwable t){
			log.info("setRelPymeOfertaTasas(Error)");
			commit = false;
			throw new AppException("Error al asignar tasas de oferta a las pymes", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("setRelPymeOfertaTasas(S)");
		}
		
	}
	
	
	/**
	 *  Cuando se requiera hacer una parametrizaci�n  con Tasa Preferencial, 
	 *  se verifique que la PYME no tenga parametrizada Oferta de Tasas por Montos.
	 * @return 
	 * @param ic_pyme
	 * @param cg_rfc
	 */
	 public boolean tipodeTasas(String ic_pyme, String cg_rfc) 	{    
		log.info("tipodeTasas (E) ");
    
	  ResultSet	rs				= null;
    PreparedStatement ps = null;
	  AccesoDB 	con				= null;
    boolean transaccion = true;
    StringBuffer qrySentencia  = new StringBuffer();
		boolean sihayTasas = false;
    try {
			con = new AccesoDB();
      con.conexionDB();
			qrySentencia.append(" Select count(*) as sihay  from    "+
										" com_oferta_tasa_if_epo o,  "+
										"	comrel_oferta_tasa_pyme_if  op,"+
										" COMREL_CUENTA_BANCARIA c , "+
										" comcat_pyme p "+
										" where o.ic_oferta_tasa = op.ic_oferta_tasa "+
										" and  op.ic_cuenta_bancaria = c.ic_cuenta_bancaria"+
										" and CC_TIPO_TASA ='O' "+										
										" and p.ic_pyme = c.ic_pyme  ");										
			if(!ic_pyme.equals("")) {
				qrySentencia.append(" and c.IC_PYME = ? ");
			}
			if(!cg_rfc.equals("")) {
				qrySentencia.append(" and p.cg_rfc = ? ");
			}
		
		ps = con.queryPrecompilado(qrySentencia.toString());
		if(!ic_pyme.equals("")) {
		ps.setInt(1,Integer.parseInt(ic_pyme));	
		}
		if(!cg_rfc.equals("")) {
			ps.setString(1,cg_rfc);	
		}
		rs = ps.executeQuery();
		while(rs.next()){		
				sihayTasas = (rs.getInt("sihay") > 0)?true:false;	
		}
		rs.close();
		ps.close();
			
    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();     
      log.error("tipodeTasas "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}			
    }		
    log.info("tipodeTasas (S) ");
		return sihayTasas;				
	}
	
	/**
	 * metodo para la consulta de la  Oferta de Tasas por Montos
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param nafinEle
	 * @param cboMoneda
	 * @param cboEpo
	 * @param sesCveIf
	 */
	
	public List consTasasOfertaIf(String sesCveIf, String cboEpo, String cboMoneda, String nafinEle) throws AppException{
		log.info("consTasasOfertaIf(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lstTasas = new ArrayList();
		HashMap mp = new HashMap();
		try{
			con.conexionDB();
			StringBuffer query = new StringBuffer();
	
			
			query.append(" SELECT distinct  cpi.ic_epo , co.ic_oferta_tasa cvetasa, ");
			query.append(" co.ic_if cveif, co.ic_epo cveepo, co.cc_tipo_tasa tipotasa,  ");
      query.append(" co.fg_montodesde montodesde, co.fg_montohasta montohasta, co.fn_puntos puntospref,");
			query.append(" decode(co.cg_estatus, 'S', 'Activo', 'N', 'Inactivo') as estatus, ");
      query.append(" co.ic_moneda cvemoneda, co.cc_acuse acuse    ");
			query.append(" FROM com_oferta_tasa_if_epo co, ");
			query.append(" comrel_oferta_tasa_pyme_if ctp, ");
			query.append(" comrel_pyme_if cpi, ");
			query.append(" comrel_cuenta_bancaria ccb, ");
			query.append(" comrel_nafin cn, ");
			query.append(" com_tasa_pyme_if ct ");
			query.append(" WHERE co.ic_oferta_tasa = ctp.ic_oferta_tasa ");
			query.append(" AND ctp.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria  ");
			query.append(" and cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria  ");
			query.append(" AND ct.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria  ");
			query.append(" AND ct.ic_epo(+) = cpi.ic_epo  ");
			query.append(" AND ct.ic_if(+) = cpi.ic_if  ");
			query.append(" AND cpi.cs_vobo_if = 'S'  ");
			query.append(" AND cpi.cs_borrado = 'N' ");
			query.append(" AND ccb.cs_borrado = 'N' ");
			query.append(" AND cn.cg_tipo = 'P'  ");
			query.append(" AND co.ic_epo(+) = cpi.ic_epo ");
			query.append(" AND co.ic_if(+) = cpi.ic_if   ");
			query.append(" AND ct.cc_tipo_tasa(+) != 'P' ");
			
			if(!sesCveIf.equals("")) {
				query.append(" and cpi.ic_if = ? ");		
			}
			if(!cboEpo.equals("")) {
				query.append(" AND cpi.ic_epo = ? ");
			}
			if(!cboMoneda.equals("")) {
				query.append(" AND co.ic_moneda = ? ");	
				query.append("  AND ccb.ic_moneda =? ");
			}
			if(!nafinEle.equals("")) {
			query.append(" AND cn.ic_nafin_electronico  = ? ");
			}

			
			
			
			
			System.out.println("query "+query);
			System.out.println("sesCveIf "+sesCveIf);
			System.out.println("cboEpo "+cboEpo);
			System.out.println("cboMoneda "+cboMoneda);
			System.out.println("nafinEle "+nafinEle);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1,Long.parseLong(sesCveIf));
			ps.setLong(2,Long.parseLong(cboEpo));
			ps.setLong(3,Long.parseLong(cboMoneda));
			ps.setLong(4,Long.parseLong(cboMoneda));
			if(!nafinEle.equals("")) {
			ps.setLong(5,Long.parseLong(nafinEle));
			}			
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				mp = new HashMap();
				mp.put("CVETASA",rs.getString("cvetasa"));
				mp.put("CVEIF", rs.getString("cveif"));
				mp.put("CVEEPO", rs.getString("cveepo"));
				mp.put("TIPOTASA", rs.getString("tipotasa"));
				mp.put("MONTODESDE", rs.getString("montodesde"));
				mp.put("MONTOHASTA", rs.getString("montohasta"));
				mp.put("PUNTOSPREF", rs.getString("puntospref"));
				mp.put("ESTATUS", rs.getString("estatus"));
				mp.put("CVEMONEDA", rs.getString("cvemoneda"));
				mp.put("ACUSE", rs.getString("acuse"));
				lstTasas.add(mp);
			}
			
			rs.close();
			ps.close();
			
			return lstTasas;

		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al consultar Oferta de Tasas",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("consTasasOfertaIf(S)");
		}
	}
	
	
	public List getPymesOferTasasCons(String sesCveIf, String cboEpo, String cboMoneda, String numElecPyme, String csNoParam, String razonSocial, String rfc) throws AppException{
		log.info("getPymesOferTasasCons(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap mp = null;
		List lstPymesTasas = new ArrayList();
		
		try{
			con.conexionDB();
			
			String query = 
						"SELECT DISTINCT cn.ic_nafin_electronico AS nafinE, cp.cg_rfc rfcpyme, ccb.ic_cuenta_bancaria cuentabancaria, ccb.ic_pyme cvepyme, " +
						"						cp.cg_razon_social razonsocial, nvl(cot.csparam,'N') paramtasas " +
						"    FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, " +
						"         comcat_pyme cp, com_tasa_pyme_if ct, " +
						"			(SELECT ctp.ic_cuenta_bancaria, cot.ic_epo, cot.ic_if, 'S' csparam " +
						"			 FROM com_oferta_tasa_if_epo cot, comrel_oferta_tasa_pyme_if ctp " +
						"				WHERE cot.ic_oferta_tasa = ctp.ic_oferta_tasa ";
						if(!cboMoneda.equals("") )
							query +="  AND cot.ic_moneda = "+cboMoneda;	
						
					query +="      )cot, " +
						"			comrel_nafin cn " +
						"   WHERE cpi.ic_if = "+sesCveIf +
						"     AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
						"     AND ccb.ic_pyme = cp.ic_pyme " +
						"		AND cp.ic_pyme = cn.ic_epo_pyme_if " +
						"     AND ct.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria " +
						"     AND ct.ic_epo(+) = cpi.ic_epo " +
						"     AND ct.ic_if(+) = cpi.ic_if " +
						"		AND cot.ic_cuenta_bancaria(+) = cpi.ic_cuenta_bancaria " +
						"     AND cot.ic_epo(+) = cpi.ic_epo " +
						"     AND cot.ic_if(+) = cpi.ic_if " +						
						"     AND cpi.ic_epo = " +cboEpo+
						"		AND cn.cg_tipo = 'P' " +
						"     AND cpi.cs_vobo_if = 'S' " +
						"     AND cpi.cs_borrado = 'N' " +
						"     AND ccb.cs_borrado = 'N' " +
						"     AND ct.cc_tipo_tasa(+) != 'P' ";
					
			if(numElecPyme!=null && !"".equals(numElecPyme))
				query += "		AND cn.ic_nafin_electronico =  "+numElecPyme;
			if(csNoParam != null && "S".equals(csNoParam))
			query += "		AND cot.csparam ='S' ";
					
			if(!cboMoneda.equals("") )
			query +="  AND ccb.ic_moneda = "+cboMoneda ;		
			
			if(!razonSocial.equals("") )
			query += " AND cp.cg_razon_social LIKE NVL (REPLACE (UPPER ('"+razonSocial+"'), '*', '%'), '%') ";
					
			if(!rfc.equals("") )
			query += " AND cp.cg_rfc LIKE NVL (REPLACE (UPPER ('"+rfc+"'), '*', '%'), '%') ";
						
			query += "ORDER BY cp.cg_razon_social ";
			
			
			log.debug("getPymesOferTasas:query = "+ query);
			
		
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				mp = new HashMap();
				mp.put("CVEPYME", rs.getString("cvepyme"));
				mp.put("RFCPYME", rs.getString("rfcpyme"));
				mp.put("CTABANC", rs.getString("cuentabancaria"));
				mp.put("RAZONSOCIAL", rs.getString("razonsocial"));
				mp.put("PARAMTASAS", rs.getString("paramtasas"));
				mp.put("NAFINE", rs.getString("nafinE"));				
				lstPymesTasas.add(mp);
			}
			
			rs.close();
			ps.close();

			return lstPymesTasas;
		}catch(Throwable t){
			log.info("getPymesOferTasasCons(Error)");
			t.printStackTrace();
			throw new AppException("Error al obtener pymes para ofeerta de tasas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getPymesOferTasasCons(S)");
		}
	}
	// Migraci�n IF
	/**
	 * consulta para la pantala de Acuse de  Tasas Preferenciales /Negociables 
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param acuse
	 * @param lstPlazo
	 * @param tipoTasa
	 * @param ic_pyme
	 * @param ic_moneda    
	 * @param ic_if
	 * @param ic_epo   
	 */
	 public Vector  consultaTasaNegociada(String ic_epo,  String  ic_if, 
													String ic_moneda, String ic_pyme , 
													String tipoTasa, String lstPlazo ,
													String acuse )throws NafinException    {  
  
	System.out.println("   consultaTasaNegociada  (E)");
	AccesoDB con  = new AccesoDB();
	Vector registros = new Vector();
	Vector lovDatos = new Vector();
	StringBuffer lsQuery = new StringBuffer("");
	double ldTotal =0;
	
   try {
		con.conexionDB();
		
		lsQuery.append(" SELECT ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  "+
				" nvl(cmt.fn_valor,0) as ValorTasa, ctb.cg_rel_mat as RelMatBase,  nvl(cta.fn_puntos,0) as PuntosBase, "+
		   	" cpe.ic_calificacion as Clasificacion, nvl(ct.cg_rel_mat,' ') as RelMatClasif, nvl(cta.fn_puntos,0) as PuntosClasif, "+
				" to_char(sysdate,'dd/mm/yyyy'), cpe.ic_pyme, cp.cg_razon_social, pl.cg_descripcion as descPlazo "+
				" ,pl.ic_plazo "+
		   	" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, "+
		   	" ( SELECT distinct ic_epo, max(dc_fecha_tasa) as fecha_tasa, ic_tasa "+
		   	"   FROM comrel_tasa_base  "+
		   	"   WHERE ic_epo in ("+ ic_epo +")"+
		   	"   AND cs_vobo_nafin = 'S'"+
		   	"   AND cs_tasa_activa = 'S'"+			
		   	"   GROUP BY ic_epo, ic_tasa ) tb,"+
		   	" ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa)  mb,"+
		   	" com_mant_tasa cmt, comcat_epo ce, comrel_pyme_epo cpe, "+
		   	" comrel_tipificacion ct, comcat_pyme cp, comcat_plazo pl "+
		   	" WHERE cta.ic_if = " + ic_if+
		   	" AND cta.cs_vobo_if = 'S' "+
		   	" AND cta.cs_vobo_epo = 'S'"+
		   	" AND cta.cs_vobo_nafin = 'S'"+
		   	" AND cta.ic_epo = tb.ic_epo"+
		   	" AND cta.ic_epo in ("+ ic_epo+")"+
		   	" AND cta.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ctb.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ctb.ic_epo = tb.ic_epo"+
				" AND ctb.ic_tasa = tb.ic_tasa"+
		   	" AND ctb.ic_tasa = ct.ic_tasa"+
		   	" AND ct.ic_moneda = " + ic_moneda+
		   	" AND ct.ic_tasa = mb.ic_tasa"+
		   	" AND cmt.ic_tasa = ct.ic_tasa"+
		   	" AND cmt.dc_fecha = mb.dc_fecha"+
		   	" AND ctb.ic_epo = ce.ic_epo "+
		   	" AND cpe.ic_epo = ctb.ic_epo"+
		   	" AND cpe.ic_pyme in (" + ic_pyme + ")"+
		   	" AND cpe.ic_calificacion = ct.ic_calificacion"+
		   	" AND ct.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ct.cs_vobo_nafin = 'S'"+
				" AND cp.ic_pyme = cpe.ic_pyme"+
				" AND ctb.ic_plazo = pl.ic_plazo");
				if ( tipoTasa.equals("B"))
		   	lsQuery.append(" 	  AND cpe.ic_calificacion = 1"); // condici�n para tipo de tasa (Base) 

				if ( !lstPlazo.equals(""))
		   	lsQuery.append(" AND ctb.ic_plazo = "+lstPlazo+" ");
		
				lsQuery.append(" order by ct.ic_tasa");
			
				System.out.println("queryyyyy::: "+lsQuery);
				ResultSet	lrsResultado	= con.queryDB(lsQuery.toString());
				while (lrsResultado.next()) {
					
					String lsCveEpo 		= lrsResultado.getString(1);
					String lsNombre 		= lrsResultado.getString(2);
					String lsNomTasa 		= lrsResultado.getString(3);
					String lsValorTasa		= lrsResultado.getString(4);
					String lsRelMatBase		= lrsResultado.getString(5);
					String lsPuntosTasa		= lrsResultado.getString(6);
					int liCalificacion		= lrsResultado.getInt(7);
					String lsRelMatClasif	= lrsResultado.getString(8);
					String lsPuntosClasif	= lrsResultado.getString(9);
					String lsFecha			= lrsResultado.getString(10);
					String lsNomPYME		= lrsResultado.getString(12);
					String lsPlazo			= lrsResultado.getString("descPlazo");
					String lsPlazo9			= lrsResultado.getString("ic_plazo");
					double ldValorTotalTasa = 0;			
					if ( lsRelMatBase.equals("+"))
						ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
					if ( lsRelMatBase.equals("-"))
						ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);
					
					ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;
					
					if ( lsRelMatClasif.equals("+"))
						ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
					if ( lsRelMatClasif.equals("-"))
						ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);
				
					ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

					String lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";
					
					String lsCadenaTasa 	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "  " + lsPuntosTasa;
					String lsCadTasaArch	=  lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + " " + lsPuntosTasa;
						
					if ( lsCalificacion.equals("Clasificaci�n")){
						lsCadenaTasa += "   " + lsRelMatClasif + "  " + lsPuntosClasif;
						lsCadTasaArch += " " + lsRelMatClasif + " " + lsPuntosClasif;
					}
			
					/*************************************/
					// Para Tasas Preferenciales, Negociadas y TODAS
					String	lsValorTasaN = "";
					boolean	lbExisteTasaNegociada = false;
					String lsValorTasaP = "";
				
					lsQuery.delete(0, lsQuery.length());
					lsQuery.append(" SELECT ctpi.cc_tipo_tasa, nvl(ctpi.fn_valor,0) , "+
										" nvl(trunc(sysdate) - trunc(df_aplicacion),0) "+
										" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,"+
										" 	com_tasa_pyme_if ctpi"+					
									   "  WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
									   " AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria"+
										" and  cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria"+
										" AND cpi.cs_vobo_if = 'S'"+
										" AND cpi.cs_borrado = 'N'"+
										" AND ccb.cs_borrado = 'N' "+
										" AND cpi.ic_if = " + ic_if+
										" AND cpi.ic_epo = " + lsCveEpo+
										" AND ccb.ic_moneda = " + ic_moneda+
										" AND ccb.ic_pyme = " + ic_pyme+
									   " AND ctpi.cc_acuse = '"+acuse+"'"+
										" AND ctpi.ic_plazo = " + lsPlazo9+
										" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria"+
										" AND cpi.ic_if = ctpi.ic_if"+
										" AND cpi.ic_epo = ctpi.ic_epo"+
										" ORDER BY ctpi.cc_tipo_tasa");
			
					System.out.println("Query 2 = " + lsQuery.toString());
					ResultSet lrsResultado2 = con.queryDB(lsQuery.toString());
					String lsTipo = "";
						
					 while ( lrsResultado2.next()) {
						lsTipo = lrsResultado2.getString(1);
						int liExiste = lrsResultado2.getInt(3);
						if ( lsTipo.equals("N") && liExiste == 0){			
							lsValorTasaN = lrsResultado2.getString(2);
							lbExisteTasaNegociada = true;				
						}	
						if ( lsTipo.equals("P")){
							lsValorTasaP = lrsResultado2.getString(2);	
							ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;
						}						
					}//while ( lrsResultado2.next()) {
						
					lrsResultado2.close();
					con.cierraStatement();
					lsCalificacion = (!lsValorTasaP.equals(""))? "Preferencial" : lsCalificacion;
					lsCalificacion = (!lsValorTasaN.equals(""))? "Negociada" : lsCalificacion;
					if ( !lbExisteTasaNegociada)
					lsFecha = lsValorTasaN = "";
			
					if ( ( tipoTasa.equals("P") && !lsCalificacion.equals("Preferencial")) || ( tipoTasa.equals("N") && !lsCalificacion.equals("Negociada") ) ||
					( tipoTasa.equals("B") && !lsCalificacion.equals("Base")) )
					
				
					//continue;		
					System.out.println("lsPlazo::: "+lsPlazo);					
					registros = new Vector(); 
					registros.add(0,lsNombre);
					registros.add(1,lsNomPYME);
					registros.add(2,lsCalificacion);
					registros.add(3,lsCadenaTasa);
					registros.add(4,lsPlazo);
					registros.add(5, Comunes.formatoDecimal(ldValorTotalTasa,5) );														
					
					if(!lsValorTasaN.equals("")){
					registros.add(6,Comunes.formatoDecimal(lsValorTasaN,5));
					}else if(!lsValorTasaP.equals("")){
						registros.add(6,Comunes.formatoDecimal(ldTotal,5) );
					}
					if(lsValorTasaP.equals("")  && lsValorTasaN.equals("") ) {
						registros.add(6,"0" );
					}
					lovDatos.add(registros);
				}
				lrsResultado.close();
				con.cierraStatement();	
				System.out.println("lovDatos::: "+lovDatos);
	} catch (Exception e) {
		System.out.println("Error en consultaTasaNegociada: "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}            
	}
	System.out.println("   consultaTasaNegociada  (S)");
    return lovDatos;
}

	public boolean guardaModificacionesCargaMasivaExt(
    		String 		esCveIf, 
			String 		esCveMoneda,
			String 		lsCveEpo,
			String 		esTipoTasa,
			String 		cc_acuse,
			String 		ic_usuario,
			String 		_acuse, 
			String[] 	lsListaTasasInsertar,
			String[] 	lsListaTasasEliminar,
			String 		nombreUsuario, 
			String estatus){
      boolean lbOK = true;
      AccesoDB con =new AccesoDB(); 
      try {
        con.conexionDB();
		  if(estatus.equals("Alta")) {  
			guardarTasasCargaMasiva( esCveIf, esCveMoneda, lsCveEpo, esTipoTasa, cc_acuse, ic_usuario, _acuse, 	lsListaTasasInsertar, nombreUsuario, con);
		  }
		  if(estatus.equals("Eliminar")) {
			eliminarTasasCargaMasiva( esCveIf, esCveMoneda, lsCveEpo, esTipoTasa, cc_acuse, ic_usuario, _acuse, lsListaTasasEliminar, nombreUsuario, con);
		  }
	   }catch(Exception e) {
        lbOK = false; 
      }finally{
        if(con.hayConexionAbierta()){
					con.terminaTransaccion(lbOK);
					con.cierraConexionDB();
				}  
      }
      return lbOK;
   }
	

	public Registros getRegistrosValidosExt(List lineas, int idProceso) {  
			
		log.info("getRegistrosValidos(E)");
		
		AccesoDB 				con				= new AccesoDB();
		StringBuffer        	query 			= new StringBuffer();
		Registros registros = new Registros();
		
		if(lineas == null || lineas.size() == 0){
			log.info("getRegistrosValidos(S)");
			return registros;
		}	
		
		try {
			
			con.conexionDB();
			//Con la implementaci�n actual solo podria soportar la carga de maximo 1000 lineas correctas, debido a restricciones de Oracle para colocar valores en el IN
			query.append(
				"SELECT                        							"  + 
				"  IC_NUMERO_LINEA AS LINEA,            							"  +
				"  CG_RFC AS RFC,                     							"  +
				"  CG_PUNTOS_VALOR_TASA AS PUNTOS_TASA,       							"  +
				"  CG_ESTATUS AS ESTATUS,                  							"  +
				"  CS_TASA_FLOATING AS TIPOTASA "+
				"FROM                          							"  + 
				"  COMTMP_TASAS                                 	"  +
				"WHERE                                          	"  + 
				"  IC_NUMERO_PROCESO        =   ?               	"  +
				"  AND IC_NUMERO_LINEA      IN ("+ Comunes.repetirCadenaConSeparador("?", ",", lineas.size())+")	"  +
				"ORDER BY IC_NUMERO_LINEA      							"		
			);
			List variableBind = new ArrayList();
			variableBind.add(new Integer(idProceso));
			variableBind.addAll(lineas);
			log.debug("getRegistrosValidos:query:"+ query);
			log.debug("getRegistrosValidos:variableBind:"+ variableBind);
			registros = con.consultarDB(query.toString(), variableBind);
	
		}catch(Exception e){
			log.debug("getRegistrosValidos(Exception)");
			log.debug("getRegistrosValidos.lineas    = <"+lineas+">");
			log.debug("getRegistrosValidos.idProceso = <"+idProceso+">");
			throw new AppException("getRegistrosValidos(Exception)", e);
		}finally{
			log.info("getRegistrosValidos(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB(); 
			}
		}
		return registros;	
	}
	
	/**
	 *  para  la pantalla de Captura Tasas Preferenciales/Negociadas para Mandato
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param acuse
	 * @param lstPlazo
	 * @param tipoTasa
	 * @param ic_mandante
	 * @param ic_moneda
	 * @param ic_if  
	 * @param ic_epo
	 */
	 public Vector  consultaTasasMandato(String ic_epo,  String  ic_if, 
													String ic_moneda, String ic_mandante , 
													String tipoTasa, String lstPlazo ,
													String acuse )throws NafinException    {  
  
	System.out.println("   consultaTasasMandato  (E)");
	AccesoDB con  = new AccesoDB();
	Vector registros = new Vector();
	Vector lovDatos = new Vector();
	StringBuffer lsQuery = new StringBuffer("");
	
   try {
		con.conexionDB();
		
		lsQuery.append(" SELECT ctb.ic_epo as CveEpo, ce.cg_razon_social as NomEpo, ct.cd_nombre as tasa,  "+
		   	" nvl(cmt.fn_valor,0) as ValorTasa, ctb.cg_rel_mat as RelMatBase,  nvl(cta.fn_puntos,0) as PuntosBase, "+
		   	" ct.ic_calificacion as Clasificacion, nvl(ct.cg_rel_mat,' ') as RelMatClasif, nvl(ct.cg_puntos,0) as PuntosClasif, "+
			" to_char(sysdate,'dd/mm/yyyy'), cpe.ic_mandante, cp.cg_razon_social, pl.cg_descripcion as descPlazo "+
		   	" FROM comrel_tasa_autorizada cta, comrel_tasa_base ctb, comcat_tasa ct, "+
		   	" ( SELECT distinct ic_epo, max(dc_fecha_tasa) as fecha_tasa, ic_tasa "+
		   	"   FROM comrel_tasa_base  "+
		   	"   WHERE ic_epo in ("+ ic_epo +")"+
		   	"   AND cs_vobo_nafin = 'S'"+
		   	"   AND cs_tasa_activa = 'S'"+
		   	"   GROUP BY ic_epo, ic_tasa ) tb,"+
		   	" ( SELECT ic_tasa, max(dc_fecha) as dc_fecha from com_mant_tasa group by ic_tasa)  mb,"+
		   	" com_mant_tasa cmt, comcat_epo ce, comrel_mandante_epo cpe, "+
		   	" comrel_tipificacion ct, comcat_mandante cp, comcat_plazo pl "+
		   	" WHERE cta.ic_if = " + ic_if+
		   	" AND cta.cs_vobo_if = 'S' "+
		   	" AND cta.cs_vobo_epo = 'S'"+
		   	" AND cta.cs_vobo_nafin = 'S'"+
		   	" AND cta.ic_epo = tb.ic_epo"+
		   	" AND cta.ic_epo in ("+ ic_epo+")"+
		   	" AND cta.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ctb.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ctb.ic_epo = tb.ic_epo"+
			" AND ctb.ic_tasa = tb.ic_tasa"+
		   	" AND ctb.ic_tasa = ct.ic_tasa"+
		   	" AND ct.ic_moneda = " + ic_moneda+
		   	" AND ct.ic_tasa = mb.ic_tasa"+
		   	" AND cmt.ic_tasa = ct.ic_tasa"+
		   	" AND cmt.dc_fecha = mb.dc_fecha"+
		   	" AND ctb.ic_epo = ce.ic_epo "+
		   	" AND cpe.ic_epo = ctb.ic_epo"+
		   	" AND cpe.ic_mandante in (" + ic_mandante + ")"+
		   	" AND ct.ic_calificacion = 1 "+
		   	" AND ct.dc_fecha_tasa = tb.fecha_tasa"+
		   	" AND ct.cs_vobo_nafin = 'S'"+
				" AND cp.ic_mandante = cpe.ic_mandante"+
				" AND ctb.ic_plazo = pl.ic_plazo");	
				if ( !lstPlazo.equals(""))  {
					lsQuery.append(" AND ctb.ic_plazo = "+lstPlazo+" ");
				}
			
		System.out.println("queryyyyy::: "+lsQuery);
		ResultSet	lrsResultado	= con.queryDB(lsQuery.toString());
		lovDatos = new Vector();
		while (lrsResultado.next()) {
			String lsCveEpo 		= lrsResultado.getString(1);
			String lsNombre 		= lrsResultado.getString(2);
			String lsNomTasa 		= lrsResultado.getString(3);
			String lsValorTasa		= lrsResultado.getString(4);
			String lsRelMatBase		= lrsResultado.getString(5);
			String lsPuntosTasa		= lrsResultado.getString(6);
			int liCalificacion		= lrsResultado.getInt(7);
			String lsRelMatClasif	= lrsResultado.getString(8);
			String lsPuntosClasif	= lrsResultado.getString(9);
			String lsFecha			= lrsResultado.getString(10);
			String lsNomPYME		= lrsResultado.getString(12);
			String lsPlazo			= lrsResultado.getString("descPlazo");  

			double ldValorTotalTasa = 0;

			if ( lsRelMatBase.equals("+"))
				ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
			if ( lsRelMatBase.equals("-"))
				ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

			ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

			if ( lsRelMatClasif.equals("+"))
				ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
			if ( lsRelMatClasif.equals("-"))
				ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

			ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

			String lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

			String lsCadenaTasa 	= lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + " " + lsPuntosTasa;
			String lsCadTasaArch	=  lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + " " + lsPuntosTasa;
			if ( lsCalificacion.equals("Clasificaci�n")){
				lsCadenaTasa += lsRelMatClasif + lsPuntosClasif;
				lsCadTasaArch += " " + lsRelMatClasif + " " + lsPuntosClasif;
			}

			// Para Tasas Preferenciales, Negociadas y TODAS
			String	lsValorTasaN = "";
			boolean	lbExisteTasaNegociada = false;
			String lsValorTasaP = "";

			lsQuery.delete(0, lsQuery.length());
		   lsQuery.append(" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor, "+
					" nvl(trunc(sysdate) - trunc(df_aplicacion),0) "+
			   	" FROM comrel_mandante_epo cme, comcat_mandante cm,"+
			   	" 	 com_tasa_pyme_if_mandato ctpi"+
			   	" WHERE cme.ic_mandante = cm.ic_mandante"+
			   	" AND ctpi.ic_if = " + ic_if+
			   	" AND ctpi.ic_epo = " + lsCveEpo+
			   	" AND ctpi.ic_moneda = " + ic_moneda+
			   	" AND ctpi.ic_mandante = " + ic_mandante+
					" AND ctpi.cc_acuse = '"+acuse.toString()+"'");
			ResultSet lrsResultado2 = con.queryDB(lsQuery.toString());
			String lsTipo = "";
			while ( lrsResultado2.next()) {
				lsTipo = lrsResultado2.getString(1);
				int liExiste = lrsResultado2.getInt(3);

				if ( lsTipo.equals("N") && liExiste == 0){
					lsValorTasaN = lrsResultado2.getString(2);
					lbExisteTasaNegociada = true;
				}
				else
					if ( lsTipo.equals("P"))
						lsValorTasaP = lrsResultado2.getString(2);
			}
			lrsResultado2.close();
			con.cierraStatement();

			lsCalificacion = (!lsValorTasaP.equals(""))? "Preferencial" : lsCalificacion;

			lsCalificacion = (!lsValorTasaN.equals(""))? "Negociada" : lsCalificacion;


			if ( !lsValorTasaP.equals("")){
				lsCadenaTasa += "-" + lsValorTasaP;
				lsCadTasaArch += " - " + lsValorTasaP;
			}

			if ( lsValorTasaP.equals(""))	lsValorTasaP = "0";
			if ( lsValorTasaP != null && !lsValorTasaP.equals("") )
				ldValorTotalTasa -= Double.parseDouble(lsValorTasaP);

			if ( !lbExisteTasaNegociada)
				lsFecha = lsValorTasaN = "";

			if ( ( tipoTasa.equals("P") && !lsCalificacion.equals("Preferencial")) || ( tipoTasa.equals("N") && !lsCalificacion.equals("Negociada") ) ||
				( tipoTasa.equals("B") && !lsCalificacion.equals("Base")) /*|| ( lsTipoTasa.equals("C") && !lsCalificacion.equals("Clasificaci&oacute;n") )*/ )
			//	continue;
				registros = new Vector();
				registros.add(0,lsNombre);
				registros.add(1,lsNomPYME);
				registros.add(2,lsCalificacion);
				registros.add(3,lsCadenaTasa);
				registros.add(4,lsPlazo);
				registros.add(5, Comunes.formatoDecimal(ldValorTotalTasa,4) );														
				registros.add(6,lsValorTasaN);					
				lovDatos.add(registros);
		}
		lrsResultado.close();
		con.cierraStatement();
				
		
	} catch (Exception e) {
		System.out.println("Error en consultaTasasMandato: "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}            
	}
	System.out.println("   consultaTasasMandato  (S)");
    return lovDatos;
}

/**
	 * para la pantalla  de Consulta  Tasas Preferenciales/Negociadas para Mandato
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param tipoTasa
	 * @param ic_mandante
	 * @param ic_moneda
	 * @param ic_if
	 * @param ic_epo
	 */
 public Vector  consultaTasasMandatoC(String ic_epo,  String  ic_if, 
													String ic_moneda, String ic_mandante, 
													String tipoTasa	 )throws NafinException    {  
  
	System.out.println("   consultaTasasMandatoC  (E)");
	AccesoDB con  = new AccesoDB();
	Vector registros = new Vector();
	Vector lovDatos = new Vector();
	StringBuffer lsQuery = new StringBuffer("");
	String  lsFechaFin = "", lsCveEpo ="", lsNombre ="", 	lsNomTasa ="",  lsValorTasa = "", lsRelMatBase = "",
				lsPuntosTasa = "",   lsRelMatClasif ="", lsPuntosClasif = "", lsFecha = "", lsCalificacion ="",
				lsPYME = "", lsNomPYME 	= "", lsPlazo = "", lsIcPlazo	= "",  lsEposEnc 	= "", lsCadenaTasa ="", 
				lsAprobIF = "", lsAprobEPO = "",  lsAprobNafin = "", lsTipo = "", lsValorTasaN = "", lsValorTasaP  = "", lsSel ="";
	boolean lbExisteTasaNegociada = false;
	boolean existeTasaAborrar = false;
	double ldValorTotalTasa = 0;
	int liCalificacion;
	String condicion = "";	
	ResultSet lrsResultado;
	ResultSet lrsResultado2;

   try {
		con.conexionDB();
	
		if(!ic_mandante.equals("")) {
			condicion = "    AND cpe.ic_mandante IN (" + ic_mandante + ") ";		
		}
		
		lsQuery.delete(0, lsQuery.length());
	   	lsQuery.append(
			" SELECT ctb.ic_epo AS cveepo, ce.cg_razon_social AS nomepo, ct.cd_nombre AS tasa,"   +
			"        NVL (cmt.fn_valor, 0) AS valortasa, cta.cg_rel_mat AS relmatbase,"   +
			"        cta.fn_puntos AS puntosbase,"   +
			"        ct.ic_calificacion AS clasificacion,"   +
			"        NVL (ct.cg_rel_mat, ' ') AS relmatclasif,"   +
			"        NVL (ct.cg_puntos, 0) AS puntosclasif, TO_CHAR (SYSDATE, 'dd/mm/yyyy'),"   +
			"        cpe.ic_mandante, cp.cg_razon_social, pl.cg_descripcion AS descplazo,"   +
			"        pl.ic_plazo"   +
			"   FROM comrel_tasa_autorizada cta,"   +
			"        comrel_tasa_base ctb,"   +
			"        comcat_tasa ct,"   +
			"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
			"             FROM com_mant_tasa"   +
			"         GROUP BY ic_tasa) mb,"   +
			"        com_mant_tasa cmt,"   +
			"        comcat_epo ce,"   +
			"        comrel_mandante_epo cpe,"   +
			"        comrel_tipificacion ct,"   +
			"        comcat_mandante cp,"   +
			"        comcat_plazo pl"   +
			"  WHERE cta.ic_if = "+ic_if+
			"    AND cta.cs_vobo_if = 'S'"   +
			"    AND cta.cs_vobo_epo = 'S'"   +
			"    AND cta.cs_vobo_nafin = 'S'"   +
			"    AND cta.ic_epo IN ("+ic_epo+")"   +
			"    AND cta.dc_fecha_tasa = ctb.dc_fecha_tasa"   +
			"    AND ctb.ic_epo = cta.ic_epo"   +
			"    AND ctb.ic_tasa = ct.ic_tasa"   +
			"    AND ct.ic_moneda = "+ic_moneda+
			"    AND ct.ic_tasa = mb.ic_tasa"   +
			"    AND cmt.ic_tasa = ct.ic_tasa"   +
			"    AND cmt.dc_fecha = mb.dc_fecha"   +
			"    AND ctb.ic_epo = ce.ic_epo"   +
			"    AND cpe.ic_epo = ctb.ic_epo"   +
			condicion+
			"    AND ct.ic_calificacion = 1 "   +
			"    AND ct.dc_fecha_tasa = ctb.dc_fecha_tasa"   +
			"    AND ct.cs_vobo_nafin = 'S'"   +
			"    AND cp.ic_mandante = cpe.ic_mandante"   +
			"    AND ctb.ic_plazo = pl.ic_plazo"   +
			"    AND ctb.cs_tasa_activa = 'S'");
			
			lrsResultado = con.queryDB(lsQuery.toString());
			if ( lrsResultado.next()) {
			lovDatos = new Vector();
			do {
				registros = new Vector();
				lsFechaFin = "";
				lsCveEpo 	= lrsResultado.getString(1);
				lsNombre 	= lrsResultado.getString(2);
				lsNomTasa 	= lrsResultado.getString(3);
				lsValorTasa = lrsResultado.getString(4);
				lsRelMatBase = lrsResultado.getString(5);
				lsPuntosTasa = lrsResultado.getString(6);
				liCalificacion = lrsResultado.getInt(7);
				lsRelMatClasif = lrsResultado.getString(8);
				lsPuntosClasif = lrsResultado.getString(9);
				lsFecha 	= lrsResultado.getString(10);
				lsPYME 		= lrsResultado.getString(11);
				lsNomPYME 	= lrsResultado.getString(12);
				lsPlazo		= lrsResultado.getString("descPlazo");
				lsIcPlazo	= lrsResultado.getString("ic_plazo");

				if (lsEposEnc.indexOf(lsCveEpo) == -1 )
					lsEposEnc += lsCveEpo + ",";

				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);

				ldValorTotalTasa = (double) Math.round(ldValorTotalTasa*100000)/100000;

				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";

				lsCadenaTasa = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + "  " + lsPuntosTasa;

				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += "  " + lsRelMatClasif + "  " + lsPuntosClasif;
				}

				lsAprobIF = lsAprobEPO = lsAprobNafin = "S";
				lsTipo = "";
				/*************************************/
				// Para Tasas Preferenciales, Negociadas y TODAS
				lsValorTasaN = "&nbsp;";
				lbExisteTasaNegociada = false;
				lsValorTasaP = lsValorTasaN = "";

				lsQuery.delete(0, lsQuery.length());
			   	lsQuery.append(" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor, " +
							" TO_CHAR(df_aplicacion, 'dd/mm/yyyy') as df_aplicacion, " +
							" TO_CHAR(df_aplicacion_fin, 'dd/mm/yyyy') as df_aplicacion_fin, ");
					lsQuery.append(" ctpi.ic_mandante mandante, ctpi.ic_if cveIf,  ctpi.ic_epo cveEpo, ctpi.ic_plazo plazo, ctpi.ic_orden orden " );
			   	lsQuery.append(" FROM comrel_mandante_epo cme, comcat_mandante cm,");
			   	lsQuery.append(" 	 com_tasa_pyme_if_mandato ctpi");
			   	lsQuery.append(" WHERE cme.ic_mandante = cm.ic_mandante");			   	
			   	lsQuery.append(" AND ctpi.ic_if = " + ic_if);
			   	lsQuery.append(" AND ctpi.ic_epo = " + lsCveEpo);			   	
			   	lsQuery.append(" AND ctpi.ic_mandante = " + lsPYME);
					lsQuery.append(" AND ctpi.ic_plazo = " + lsIcPlazo);
			   	
					lsQuery.append(" AND ( " +
							"	(ctpi.cc_tipo_tasa = 'N' " +
							"	AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, df_aplicacion)) ) " +
							"	OR (ctpi.cc_tipo_tasa = 'P'  " +
							"	AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, SYSDATE)) ) " +
							"	)" +
							" ORDER BY ic_orden ");


				System.out.println("Query 2 = " + lsQuery.toString());
				lrsResultado2 = con.queryDB(lsQuery.toString());

				if (lrsResultado2.next()) {
					existeTasaAborrar = true;
					lsTipo = lrsResultado2.getString(1);
					String cadValTasaBorrar =	lrsResultado2.getString(1)+","+
														lrsResultado2.getString("mandante")+","+
														lrsResultado2.getString("cveIf")+","+
														lrsResultado2.getString("cveEpo")+","+
														lrsResultado2.getString("plazo")+","+
														lrsResultado2.getString("orden");
					if (lsTipo.equals("N") ){
						lsValorTasaN = lrsResultado2.getString(2);
						lbExisteTasaNegociada = true;
						lsFechaFin = (lrsResultado2.getString("DF_APLICACION_FIN")==null)?"":lrsResultado2.getString("DF_APLICACION_FIN");
					}
					else
						if ( lsTipo.equals("P"))
							lsValorTasaP = lrsResultado2.getString(2);
				
					
					registros.add(0,cadValTasaBorrar);	
				}else {
					registros.add(0,"");
				}
				lrsResultado2.close();
				con.cierraStatement();

				lsCalificacion = (!lsValorTasaP.equals(""))? "Preferencial" : lsCalificacion;

				lsCalificacion = (!lsValorTasaN.equals(""))? "Negociada" : lsCalificacion;

				if ( !lsValorTasaP.equals(""))
					lsCadenaTasa += "-" + lsValorTasaP;

				if ( lsValorTasaP.equals(""))	lsValorTasaP = "0";
				if ( lsValorTasaP != null && !lsValorTasaP.equals("") )
					ldValorTotalTasa -= Double.parseDouble(lsValorTasaP);

				if ( !lbExisteTasaNegociada)
					lsFecha = lsValorTasaN = " ";

				if ( lsCalificacion.equals("Negociada") || lsCalificacion.equals("Preferencial"))
					lsAprobEPO = lsAprobNafin = " ";

				if ( ( tipoTasa.equals("P") && !lsCalificacion.equals("Preferencial")) || ( tipoTasa.equals("N") && !lsCalificacion.equals("Negociada") ) ||
					( !lsTipo.equals("N") && !lsTipo.equals("P")) /*|| ( lsTipoTasa.equals("C") && !lsCalificacion.equals("Clasificaci&oacute;n") )*/ )
					//continue; 
				/************************************************/
				lsNombre = (lsSel.equals("PROVEEDOR"))? lsNomPYME : lsNombre;     
			
			String fecha  = lsFecha ;
			
			if(!"".equals(lsFechaFin)){
				fecha +=" al " + lsFechaFin;
			}		
							
			registros.add(1,lsNombre);
			registros.add(2,lsNomPYME);
			registros.add(3,lsCalificacion);
			registros.add(4,lsPlazo);
			registros.add(5,lsCadenaTasa);
			registros.add(6, Comunes.formatoDecimal(ldValorTotalTasa,4) );														
			registros.add(7,fecha);	
			registros.add(8,lsValorTasaN);
			registros.add(9,lsAprobEPO);	
			registros.add(10,lsAprobIF);	
			registros.add(11,lsAprobNafin);	
			lovDatos.add(registros); 		
		
		} while (lrsResultado.next()); //while
					lrsResultado.close();
					con.cierraStatement();
					if (lsEposEnc.length() > 0 )
					lsEposEnc = lsEposEnc.substring(0, lsEposEnc.length()-1);
		
		}			
		
	} catch (Exception e) {
		System.out.println("Error en consultaTasasMandatoC: "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}            
	}
	System.out.println("   consultaTasasMandatoC  (S)");
    return lovDatos;
}
	
	/**
	 * consulta tasas para el perfil iF
	 * @throws netropology.utilerias.AppException
	 * @param claveEPOs
	 * @param claveIF
	 * @param cveMoneda
	 * @param lsCadenaPyme
	 * @param lsTipoTasa
	 * @param aplicaFloating
	 * @return
	 * @throws AppException
	 */
public List  getTasaIFDesc (String claveEPOs, String claveIF, String cveMoneda, String lsCadenaPyme, String lsTipoTasa, String  aplicaFloating) throws AppException {
  
   AccesoDB con = new AccesoDB();
   PreparedStatement ps = null;
   ResultSet rs = null;
	PreparedStatement ps2 = null;
   ResultSet rs2 = null;
	ArrayList informacion = new ArrayList();
   JSONArray registros = new JSONArray();
	HashMap datos = null;
	
	StringBuffer lsQuery = new StringBuffer();
	StringBuffer lsQuery2 = new StringBuffer();
	String lsFechaFin = "", lsCveEpo = "", lsNombre = "", lsNomTasa = "",  lsValorTasa = "", lsRelMatBase = "", lsPuntosTasa = "",
			  lsRelMatClasif = "", lsPuntosClasif = "",  lsFecha = "", lsPYME = "", lsNomPYME 	= "",	 lsPlazo	= "",  lsIcPlazo	= "", 
			  lsEposEnc 	= "", lsCalificacion  ="", lsCadenaTasa  ="",lsValorTasaN = "", lsAprobIF =  "", lsAprobEPO = "", 
			  lsAprobNafin = "", lsValorTasaP  ="", lsTipo ="", consulta ="",  fecha_tasa  ="";
			 
	boolean lbExisteTasaNegociada = false;				
				
		
	int liCalificacion =0;
	double ldValorTotalTasa = 0;
	int numReg =0;
	List lVarBind = new ArrayList();
	String floating ="";
	
    try {
      con.conexionDB();
		
		lsQuery = new StringBuffer();			
		lsQuery.append( " SELECT ctb.ic_epo AS cveepo, ce.cg_razon_social AS nomepo, ct.cd_nombre AS tasa,"   +
			"        NVL (cmt.fn_valor, 0) AS valortasa, cta.cg_rel_mat AS relmatbase,"   +
			"        cta.fn_puntos AS puntosbase,"   +
			"        cpe.ic_calificacion AS clasificacion,"   +
			"        NVL (ct.cg_rel_mat, ' ') AS relmatclasif,"   +
			"        NVL (ct.cg_puntos, 0) AS puntosclasif, TO_CHAR (SYSDATE, 'dd/mm/yyyy'),"   +
			"        cpe.ic_pyme, cp.cg_razon_social, pl.cg_descripcion AS descplazo,"   +
			"        pl.ic_plazo"   +
			"   FROM comrel_tasa_autorizada cta,"   +
			"        comrel_tasa_base ctb,"   +
			"        comcat_tasa ct,"   +
			"        (SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha"   +
			"             FROM com_mant_tasa"   +
			"         GROUP BY ic_tasa) mb,"   +
			"        com_mant_tasa cmt,"   +
			"        comcat_epo ce,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comrel_tipificacion ct,"   +
			"        comcat_pyme cp,"   +
			"        comcat_plazo pl"   +
			"  WHERE cta.ic_if = ?  "+
			"    AND cta.cs_vobo_if = 'S'"   +
			"    AND cta.cs_vobo_epo = 'S'"   +
			"    AND cta.cs_vobo_nafin = 'S'"   +
			"    AND cta.ic_epo IN ( "+claveEPOs+" )"   +
			"    AND cta.dc_fecha_tasa = ctb.dc_fecha_tasa"   +
			"    AND ctb.ic_epo = cta.ic_epo"   +
			"    AND ctb.ic_tasa = ct.ic_tasa"   +
			"    AND ct.ic_moneda = ? " +
			"    AND ct.ic_tasa = mb.ic_tasa"   +
			"    AND cmt.ic_tasa = ct.ic_tasa"   +
			"    AND cmt.dc_fecha = mb.dc_fecha"   +
			"    AND ctb.ic_epo = ce.ic_epo"   +
			"    AND cpe.ic_epo = ctb.ic_epo"   +
			"    AND cpe.ic_calificacion = ct.ic_calificacion"   +
			"    AND ct.dc_fecha_tasa = ctb.dc_fecha_tasa"   +
			"    AND ct.cs_vobo_nafin = 'S'"   +
			"    AND cp.ic_pyme = cpe.ic_pyme"   +
			"    AND ctb.ic_plazo = pl.ic_plazo"   +
			"    AND ctb.cs_tasa_activa = 'S' "+
			"  AND cpe.cs_aceptacion IN ('R', 'S', 'H')  "); 
			
			lVarBind = new ArrayList();
			lVarBind.add(claveIF);
		 	lVarBind.add(cveMoneda);
		 
			if ( lsTipoTasa.equals("B")){
		   	lsQuery.append(" 	  AND cpe.ic_calificacion = ? " ); // condici�n para tipo de tasa (Base) 
				lVarBind.add("1");
			}	   	
		
			if(!"".equals(lsCadenaPyme)) {
				lsQuery.append(" AND cpe.ic_pyme IN (" + lsCadenaPyme + ") ");
			}
						
		
			log.debug ("lsQuery.toString()  "+lsQuery.toString()  );
		   log.debug ("lVarBind  "+lVarBind ); 
		 
			ps = con.queryPrecompilado(lsQuery.toString(), lVarBind);
		   rs = ps.executeQuery();			
					
    		while (rs.next()) {
				numReg++;
			 	lsFechaFin = "";
				floating = "";
				lsCveEpo 	= rs.getString(1);
				lsNombre 	= rs.getString(2);
				lsNomTasa 	= rs.getString(3);
				lsValorTasa = rs.getString(4);
				lsRelMatBase = rs.getString(5);
				lsPuntosTasa = rs.getString(6);
				liCalificacion = rs.getInt(7);
				lsRelMatClasif = rs.getString(8);
				lsPuntosClasif = rs.getString(9);
				lsFecha 	= rs.getString(10);
				lsPYME 		= rs.getString(11);
				lsNomPYME 	= rs.getString(12);
				lsPlazo		= rs.getString("descPlazo");
				lsIcPlazo	= rs.getString("ic_plazo");
				
				if (lsEposEnc.indexOf(lsCveEpo) == -1 )
					lsEposEnc += lsCveEpo + ",";
				
				if ( lsRelMatBase.equals("+"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) + Double.parseDouble(lsPuntosTasa);
				if ( lsRelMatBase.equals("-"))
					ldValorTotalTasa = Double.parseDouble(lsValorTasa) - Double.parseDouble(lsPuntosTasa);
				
				   ldValorTotalTasa = (ldValorTotalTasa*100000)/100000;
				
				if ( lsRelMatClasif.equals("+"))
					ldValorTotalTasa += Double.parseDouble(lsPuntosClasif);
				if ( lsRelMatClasif.equals("-"))
					ldValorTotalTasa -= Double.parseDouble(lsPuntosClasif);
				
				ldValorTotalTasa = (ldValorTotalTasa*100000)/100000;
				
				lsCalificacion = (liCalificacion == 1)? "Base" : "Clasificaci�n";
				
				lsCadenaTasa = lsNomTasa + "(" + lsValorTasa + ") " + lsRelMatBase + " " + lsPuntosTasa;
				
				if ( lsCalificacion.equals("Clasificaci�n")){
					lsCadenaTasa += " " + lsRelMatClasif + " " + lsPuntosClasif;
				}
				
				lsAprobIF = lsAprobEPO = lsAprobNafin = "S";
				/*************************************/
				// Para Tasas Preferenciales, Negociadas y TODAS
				lsValorTasaN = "";
				lbExisteTasaNegociada = false;
				lsValorTasaP = lsValorTasaN = "";
				 
				// INICIA SIGUIENTE QUERY	
				List lVarBind2 = new ArrayList();
				lsQuery2 = new StringBuffer();
				lsQuery2.append(" SELECT ctpi.cc_tipo_tasa, ctpi.fn_valor, " +
					" TO_CHAR(df_aplicacion, 'dd/mm/yyyy') as df_aplicacion, " +
					" TO_CHAR(df_aplicacion_fin, 'dd/mm/yyyy') as df_aplicacion_fin "+
					", ctpi.CS_TASA_FLOATING  as TASAFLOATING "+
			   	" FROM comrel_pyme_if cpi, comrel_cuenta_bancaria ccb,"+
			   	" 	 com_tasa_pyme_if ctpi" +
			   	" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria" +
			   	" AND cpi.cs_vobo_if = 'S' "+
			   	" AND cpi.cs_borrado = 'N' "+
			   	" AND ccb.cs_borrado = 'N' "+
			   	" AND cpi.ic_if =  ? " +
			   	" AND cpi.ic_epo = ? " +
			   	" AND ccb.ic_moneda = ? "+
			   	" AND ccb.ic_pyme =  ? "+
					" AND ctpi.ic_plazo = ? " + 
					" AND cpi.ic_cuenta_bancaria = ctpi.ic_cuenta_bancaria" +
			   	" AND cpi.ic_if = ctpi.ic_if"+
			   	" AND cpi.ic_epo = ctpi.ic_epo" +
					" AND ( " +
							"	(ctpi.cc_tipo_tasa = 'N' " +
							"	AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, df_aplicacion)) ) " +
							"	OR (ctpi.cc_tipo_tasa = 'P'  " +
							"	AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(df_aplicacion, SYSDATE)) AND TRUNC(NVL(df_aplicacion_fin, SYSDATE)) ) " +
							"	)" );
									 
					lVarBind2.add(claveIF);
					lVarBind2.add(lsCveEpo);
					lVarBind2.add(cveMoneda);
					lVarBind2.add(lsPYME);
					lVarBind2.add(lsIcPlazo);
				
					if (!"".equals(aplicaFloating)  ) {
						lsQuery2.append(" AND ctpi.CS_TASA_FLOATING = ? "); 
						lVarBind2.add(aplicaFloating);
					}	   
					lsQuery2.append(" ORDER BY ic_orden ");
				
				log.debug (" lsQuery2.toString()  "+lsQuery2.toString()  );
				log.debug (" lVarBind2  "+lVarBind2 );
				
				ps2 = con.queryPrecompilado(lsQuery2.toString(), lVarBind2);	
				rs2 = ps2.executeQuery();		
				
				if (rs2.next()) {
					lsTipo = rs2.getString(1);					
					if (lsTipo.equals("N") ){
						lsValorTasaN = rs2.getString(2);
						lbExisteTasaNegociada = true;
						lsFechaFin = (rs2.getString("DF_APLICACION_FIN")==null)?"":rs2.getString("DF_APLICACION_FIN");
					}
					else
						if ( lsTipo.equals("P"))
							lsValorTasaP = rs2.getString(2);
					
					String aplifloating = (rs2.getString("TASAFLOATING")==null)?"N":rs2.getString("TASAFLOATING");
					if ( aplifloating.equals("S")){
						floating = "/ Floating";						
					}
						
				} //if (rs2.next()) {
				rs2.close();
				con.cierraStatement();
				
				
				if (!"".equals(lsValorTasaP)  ) {
					lsCalificacion = "Preferencial "+floating;
				}else  {
					lsCalificacion = lsCalificacion;
				}						
				lsCalificacion = (!lsValorTasaN.equals(""))? "Negociada" : lsCalificacion;
				
				if ( !lsValorTasaP.equals(""))
					lsCadenaTasa += "-" + lsValorTasaP;
				
				if ( lsValorTasaP.equals(""))	lsValorTasaP = "0";
				if ( lsValorTasaP != null && !lsValorTasaP.equals("") )
					ldValorTotalTasa -= Double.parseDouble(lsValorTasaP);
				
				if ( !lbExisteTasaNegociada)
					lsFecha = lsValorTasaN = " ";
				
				if ( lsCalificacion.equals("Negociada") || lsCalificacion.equals("Preferencial"))
					lsAprobEPO = lsAprobNafin = "";
				
				if ( ( lsTipoTasa.equals("P") && !lsCalificacion.equals("Preferencial")) || ( lsTipoTasa.equals("N") && !lsCalificacion.equals("Negociada") ) ||
					( lsTipoTasa.equals("B") && !lsCalificacion.equals("Base"))  )
				//continue;					
				//	lsNombre = (lsSel.equals("PROVEEDOR"))? lsNomPYME : lsNombre;	 no se bien como funciona 
				if( !"".equals(lsFechaFin) ){  
					fecha_tasa  =  lsFecha +" al " + lsFechaFin +"";
				}else {
					fecha_tasa =lsFecha;
				}
										
				datos = new HashMap();
				datos.put("NOMBRE_EPO", lsNombre ); 
				datos.put("PROVEEDOR", lsNomPYME ); 
				datos.put("REFERENCIA", lsCalificacion ); 
				datos.put("PLAZO", lsPlazo ); 
				datos.put("TIPO_TASA", lsCadenaTasa ); 
				datos.put("VALOR", Double.toString (ldValorTotalTasa) ); 
				datos.put("FECHA", fecha_tasa ); 
				datos.put("VALOR_TASA", lsValorTasaN ); 
				datos.put("EPO", lsAprobEPO ); 
				datos.put("IF", lsAprobIF ); 
				datos.put("NAFIN", lsAprobNafin ); 
				registros.add(datos);	
				
			}// while (rs.next()) 
			
			rs.close();
			con.cierraStatement();
			if (lsEposEnc.length() > 0 ){
				lsEposEnc = lsEposEnc.substring(0, lsEposEnc.length()-1);
			}
	
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
				
			informacion.add(consulta);
			informacion.add(lsEposEnc);
			
			
			return informacion;					
				
    } catch (Exception e) {
      
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      
    }
   
  }
  
  
/**
	 *Historico de Tasas para el perfil IF  
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param lstEPO
	 * @param lsEposEnc
	 */

public String  getTasaIFDescHis (String  lsEposEnc, String lstEPO) throws AppException {
  
   AccesoDB con = new AccesoDB();
   ResultSet rs = null;
   JSONArray registros = new JSONArray();
	HashMap datos = null;	
	StringBuffer lsQuery = new StringBuffer();
	
    try {
      con.conexionDB();
		
		lsQuery = new StringBuffer();			
	
		lsQuery.append(" SELECT distinct TO_CHAR(tb.dc_fecha_tasa,'DD/MM/YYYY'), tb.ic_epo, ce.cg_razon_social ");
		lsQuery.append(" FROM (select max(tb2.dc_fecha_tasa) as fecha, ic_epo from comrel_tasa_base tb2 where tb2.ic_epo in ("+ lstEPO +") and tb2.cs_vobo_nafin='S' GROUP BY ic_epo) tbu, " );
		lsQuery.append("    comrel_tasa_base tb, ");
		lsQuery.append("	comcat_epo ce ");
		lsQuery.append(" WHERE tb.ic_epo in ("+ lsEposEnc +")");
		lsQuery.append(" AND tb.cs_vobo_nafin='S' ");
		lsQuery.append(" AND tbu.ic_epo = tb.ic_epo ");
		lsQuery.append(" AND trunc(tb.dc_fecha_tasa) != trunc(tbu.fecha) ");
		lsQuery.append(" AND ce.ic_epo = tb.ic_epo");
		System.out.println("Query 3 = " + lsQuery.toString());
		rs = con.queryDB(lsQuery.toString());
		
		while(rs.next()) {
			datos = new HashMap();
			datos.put("FECHA", rs.getString(1) ); 
			datos.put("IC_EPO", rs.getString(2) ); 
			datos.put("NOMBRE_EPO", rs.getString(3) ); 
			registros.add(datos);	
			
		}
		rs.close();
		con.cierraStatement();
	
		return   "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
						
    } catch (Exception e) {
      
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      
    }
   
  }

// ------ ***** ***** ***** ***** ***** **** **** ***** ***** ***** *****  ***** ------ //
// ------ ***** Migracion 2013 Bitacora de Tasa Preferenciales/Negociadas ***** ------ // 
/**
	 * Este m�todo VERIFICA si el NafinElectronico corresponde a una pyme
	 * @param ic_nafin_electronico numero Nafin Electronico de la EPO
	 * @param ic_epo clave de la EPO
	 * @return Lista con pymes
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */
	 public List verificaNafinElectronico(String ic_nafin,String ic_epo) throws AppException {
		log.info("verificaNafinElectronico (E)");
		String qrySentencia="";
		String ic_pyme="",txt_nombre="";
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		
		List listPyme = new ArrayList();
		HashMap mapPyme = new HashMap();
		
		AccesoDB con = new AccesoDB();
	
		try{
			con.conexionDB();		
			
			qrySentencia = " SELECT pym.ic_pyme, pym.cg_razon_social"   +
								"  FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym , comcat_epo E"   +
								"  WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
								"    AND cpe.ic_pyme = pym.ic_pyme"   +
								"    AND crn.ic_nafin_electronico = ?"   +
								"    AND crn.cg_tipo = 'P'"   +
								"    AND cpe.cs_habilitado = 'S'"   +
								"    AND cpe.cg_pyme_epo_interno IS NOT NULL"  +
								"	  AND cpe.ic_epo = E.ic_epo " +					
								"    AND cpe.ic_epo = ? ";
						
			log.debug(":::::QUERY::::::"+qrySentencia);
			log.debug("ic_nafin_electronico = "+ic_nafin);
			log.debug("ic_epo = "+ic_epo);
			try {
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(ic_nafin));					
				ps.setInt(2,Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				ps.clearParameters();
				
				if(rs.next()) {
					ic_pyme    = (rs.getString(1)==null)?"":rs.getString(1);
					txt_nombre = (rs.getString(2)==null)?"":rs.getString(2);
				   mapPyme.put("ic_pyme",ic_pyme);
				   mapPyme.put("txt_nombre",txt_nombre);
				} else{
				   mapPyme.put("ic_pyme","");
				   mapPyme.put("txt_nombre","");
				}
				listPyme.add(mapPyme);
				rs.close();
				if(ps!=null) 
					ps.close();
				
			} catch (SQLException sqle) { throw new AppException("No Existen Limites asociados a alguna EPO para este IF."); }
														//throw new NafinException("DSCT0032");
		} catch (Exception e) {
					log.info("verificaLimitesEPO -ERROR- ");
					e.printStackTrace();
		} finally {
				if (con.hayConexionAbierta()) con.cierraConexionDB();
		  }
		log.info("verificaNafinElectronico (S)");
		return listPyme;
	} // Fin Metodo verificaNafinElectronico
	
	
	/**
	 * M�todo que obtiene en base al numero Nafin Electronico el nombre del Proveedor.
	 * @param ic_nafin_electronico numero Nafin Electronico de la EPO
	 * @return Lista con pymes
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */
	public List busquedaProveedor(String ic_nafin) throws AppException {
		log.info("busquedaProveedor (E)");
		String qrySentencia="";
		String ic_pyme="",txt_nombre="";
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		String floating ="";
		List listPyme = new ArrayList();
		HashMap mapPyme = null;
		
		AccesoDB con = new AccesoDB();
	
		try{
			con.conexionDB();		
			
			qrySentencia = " SELECT  n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre , p.CS_TASA_FLOATING  as floating "+
								" FROM comrel_nafin n, comcat_pyme p "+
								" WHERE n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = "+ic_nafin;
										
			log.debug(":::::QUERY::::::"+qrySentencia);
			log.debug("ic_nafin = "+ic_nafin);
			
			try {
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				ps.clearParameters();
				
				while(rs.next()) 	{
					mapPyme = new HashMap();
					ic_pyme 			= (rs.getString("Pyme")==null)?"":rs.getString("Pyme");
					txt_nombre 		= (rs.getString("nombre")==null)?"":rs.getString("nombre"); 
					floating 		= (rs.getString("floating")==null)?"":rs.getString("floating"); 
					mapPyme.put("ic_pyme",ic_pyme);
					mapPyme.put("txt_nombre",txt_nombre);
					mapPyme.put("sucepFloating",floating);
					listPyme.add(mapPyme);
				}
				rs.close();
				if(ps!=null) 
					ps.close();
				
			} catch (SQLException sqle) { throw new AppException("No Existen Limites asociados a alguna EPO para este IF."); }
														//throw new NafinException("DSCT0032");
		} catch (Exception e) {
					log.info("busquedaProveedor -ERROR- ");
					e.printStackTrace();
		} finally {
				if (con.hayConexionAbierta()) con.cierraConexionDB();
		  }
		log.info("busquedaProveedor (S)");
		return listPyme;
	} // Fin Metodo verificaNafinElectronico
	
	
	
	/**
   * M�todo obtiene PROVEEDOR determinada pyme.
   * @param rfc_pyme RFC de la pyme
	* @param nombre_pyme Nombre de la pyme
   * @return IC_PYME la clave de la pyme.
	* 	CG_RAZON_SOCIAL nombre de la pyme.
	* 	DESPLIEGA la clave y nombre de la pyme.
	* 	IC_NAFIN_ELECTRONICO numero nafin electronico de la pyme.
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   * @since Migracion Nafin 2013
   * @author DBM
   */
   public List busquedaAvanzadaProveedor(String val_nombre, String val_rfc)throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
	 StringBuffer condicion		= new StringBuffer();	//condiciones del query
    List varBind = new ArrayList();
    List informacion_proveedores = new ArrayList();

    System.out.println("..:: AutorizacionTasasBean : busquedaAvanzadaProveedor(I) ::..");

    try{
      con.conexionDB();
		// ----------------------------------------------------------------------//
		String campoProveedor = "0"; //Geag
		String	condicionDoc = "";
		String tablaDoc = "";
		
		if(!val_nombre.equals("")){
			condicion.append( "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
			varBind.add(val_nombre);
		}
		if(!val_rfc.equals("")){
			condicion.append("    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
			varBind.add(val_rfc);
		}		
		
		condicionDoc = 
			"    AND d.ic_pyme = pe.ic_pyme"   +
			"    AND d.ic_epo = pe.ic_epo";
		tablaDoc = "com_documento d,";


		strSQL.append(" SELECT /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ " +
		//				"        p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social," +
						"        distinct p.ic_pyme, " + campoProveedor + " as cg_pyme_epo_interno, p.cg_razon_social," + //GEAG
						"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega," +
						"        crn.ic_nafin_electronico" +
						"   FROM comrel_pyme_epo pe, "+tablaDoc+"comrel_nafin crn, comcat_pyme p" +
						"  WHERE p.ic_pyme = pe.ic_pyme" +
						"    AND p.ic_pyme = crn.ic_epo_pyme_if" +
						condicionDoc+
						"    AND crn.cg_tipo = 'P' "+condicion+
						"    AND pe.cs_habilitado = 'S'"+
						"    AND p.cs_habilitado = 'S'"+
						"    AND pe.cg_pyme_epo_interno IS NOT NULL ");
						
		strSQL.append("   GROUP BY p.ic_pyme," +
				"           pe.cg_pyme_epo_interno," +
				"           p.cg_razon_social," +
				"           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social," +
				"           crn.ic_nafin_electronico" +
				"  ORDER BY p.cg_razon_social" );

		
		
		// ----------------------------------------------------------------------//
     
      System.out.println("..:: strSQL : "+strSQL.toString());
      System.out.println("..:: varBind : "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){ //pendiente
        HashMap datos_pyme = new HashMap();
        datos_pyme.put("IC_PYME",rst.getString("IC_PYME")==null?"":rst.getString("IC_PYME"));
        datos_pyme.put("CG_RAZON_SOCIAL",rst.getString("CG_RAZON_SOCIAL")==null?"":rst.getString("CG_RAZON_SOCIAL"));
		  datos_pyme.put("DESPLIEGA",rst.getString("DESPLIEGA")==null?"":rst.getString("DESPLIEGA"));
		  datos_pyme.put("IC_NAFIN_ELECTRONICO",rst.getString("IC_NAFIN_ELECTRONICO")==null?"":rst.getString("IC_NAFIN_ELECTRONICO"));
        informacion_proveedores.add(datos_pyme);
      }

      pst.close();
      rst.close();
    }catch(Exception e){
      System.out.println("..:: AutorizacionTasasBean : busquedaAvanzadaProveedor(ERROR) :");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
      System.out.println("..:: AutorizacionTasasBean : busquedaAvanzadaProveedor(F) ::..");
    }
    return informacion_proveedores;
   }
// ------ ***** ***** ***** ***** ***** **** **** ***** ***** ***** *****  ***** ------ //		
		
	/**
	 *
	 * Consulta el Nombre y ID para correspondiente al N�mero NAFIN Electr�nico proporcionado,
	 * y que opere con la EPO, IF y Moneda indicados.
	 *  
	 * @throws AppException
	 *
	 * @param claveIF						   <tt>String</tt> con el ID del IF
	 * @param claveMoneda 					<tt>String</tt> con el ID de la Moneda
	 * @param claveEPO    					<tt>String</tt> con el ID de la EPO
	 * @param numeroNafinElectronico	   <tt>String</tt> con el Numero NAFIN Electr�nico de la PYME
	 *
	 * @author Salim Hern�ndez
	 * @since  FXXX - 2013 -- ADMIN NAFIN - Migracion Descuento Electronico ( 13/03/2013 01:30:15 p.m. )
	 *
	 */
	public HashMap buscaProveedorTasasPreferencialesNegociadas( String claveIF, String claveMoneda, String claveEPO, String numeroNafinElectronico) 
		throws AppException{
			
		log.info("buscaProveedorTasasPreferencialesNegociadas(E)");
		
		AccesoDB 				con				= new AccesoDB();
    	PreparedStatement 	ps 				= null;
    	ResultSet 				rs 				= null;
		StringBuffer        	query 			= new StringBuffer();
 
		HashMap					busqueda			= new HashMap();
 
		try {
			
			con.conexionDB();
					
			query.append(
				" SELECT                                "  +
				"   distinct ccb.ic_pyme as CLAVE_PYME, "  +
				"   cp.cg_razon_social   as NOMBRE_PYME "  +
				" FROM                                  "  +
				"   comrel_pyme_if         cpi,         "  +
				"   comrel_cuenta_bancaria ccb,         "  +
				"   comcat_pyme            cp,          "  +
				"   comrel_nafin           n,           "  +
				"   comrel_pyme_epo        cpe          "  +
				" WHERE                                 "  +
				"     cpi.ic_if              = ?        "  + // claveIF
				" AND cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria "  +
				" AND ccb.ic_pyme            = cp.ic_pyme             "  +
				" AND ic_moneda              = ?        "  + // claveMoneda
				" AND cpi.ic_epo             in ( ? )   "  + // claveEPO
				" AND cpi.cs_vobo_if         = 'S'      "  +
				" AND cpi.cs_borrado         = 'N'      "  +
				" AND ccb.cs_borrado         = 'N'      "  +
				" AND n.ic_nafin_electronico = ?        "  + // numeroNafinElectronico
				" AND n.cg_tipo              = 'P'      "  +
				" AND cpe.cs_habilitado      = 'S'      "  +
				" AND cpe.cg_pyme_epo_interno IS NOT NULL  "  +
				" AND cpe.ic_epo             = cpi.ic_epo  "  +
				" AND n.IC_EPO_PYME_IF       = cp.ic_pyme  "  +
				" ORDER BY cp.cg_razon_social              "	
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveIF));
			ps.setInt(2,Integer.parseInt(claveMoneda));
			ps.setInt(3,Integer.parseInt(claveEPO));
			ps.setBigDecimal(4,new BigDecimal(numeroNafinElectronico));
			rs = ps.executeQuery();
 
			if(rs.next()){
				
				String clavePyme  = (rs.getString("CLAVE_PYME")	 == null)?"":rs.getString("CLAVE_PYME");
				String nombrePyme = (rs.getString("NOMBRE_PYME") == null)?"":rs.getString("NOMBRE_PYME");
				
				busqueda.put( "CLAVE_PYME",  clavePyme  );
				busqueda.put( "NOMBRE_PYME", nombrePyme );
				
			}
			
		}catch(Exception e){
			
			log.error("buscaProveedorTasasPreferencialesNegociadas(Exception)");
			log.error("buscaProveedorTasasPreferencialesNegociadas.claveIF                = <" + claveIF                + ">");
			log.error("buscaProveedorTasasPreferencialesNegociadas.claveMoneda            = <" + claveMoneda            + ">");
			log.error("buscaProveedorTasasPreferencialesNegociadas.claveEPO               = <" + claveEPO               + ">");
			log.error("buscaProveedorTasasPreferencialesNegociadas.numeroNafinElectronico = <" + numeroNafinElectronico + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al consultar detalle del proveedor.");
			
		}finally{
			
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("buscaProveedorTasasPreferencialesNegociadas(S)");
			
		}
				
		return busqueda;	
		
	}
	
	/**
	*	M�todo auxilia en fodea 17 para determinar si la IF opera fideicomiso.
	* @param cveIf	    Clave de la If	
	* @return bandera    String con el valor de opera fideicomiso.
	*/
	
	public String operaFideicomiso(String cveIf){
		log.info("operaFideicomiso(E)");
		AccesoDB con = null;
		PreparedStatement ps = null;
		String strSQL = null;
		String bandera = "";
		
		strSQL = " select  CS_OPERA_FIDEICOMISO   from  COMCAT_IF  where IC_IF  = ? ";
		log.info(strSQL);
			
		try{
			con = new AccesoDB();
			con.conexionDB();
	
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(cveIf));	
		
			ResultSet rs = ps.executeQuery();
			
			
			if(rs.next()){
				bandera = rs.getString("CS_OPERA_FIDEICOMISO");
			}
			
			rs.close();
			if(ps != null)
				ps.close();
			
			return bandera;
		}catch(Exception e){
			log.error("Error en operaFideicomiso: ",e);
			throw new AppException("Error en consulta para conocer si opera fideicomiso ", e);
		} finally{
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB();
			log.info("operaFideicomiso(S)");
		}
	}

	/**
	 * metodo para verificar si la pyme es suceptible a tasa floating 
	 * QC-2018
	 * @param clavePyme
	 * @param rfc
	 * @return
	 */
	public String suceptibleFloating(String clavePyme, String rfc ){
		log.info("suceptibleFloating(E)");
		AccesoDB con =  new AccesoDB();	
		String bandera = "";   
		StringBuilder        	strSQL 			= new StringBuilder();	
		List lVarBind		= new ArrayList();
		
		try{
			con = new AccesoDB();
			con.conexionDB();
	
			strSQL.append(" SELECT  CS_TASA_FLOATING  from  COMCAT_PYME  " );
			
			if (!"".equals(clavePyme)  ) {
				strSQL.append(" where ic_pyme =  ? ");	
				lVarBind.add(clavePyme);
			}
			if (!"".equals(rfc)  ) {
				strSQL.append(" where CG_RFC =  ? ");	
				lVarBind.add(rfc);
			}
			
			log.debug("strSQL  "+strSQL  +"lVarBind  "+lVarBind);
			
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), lVarBind);	
			ResultSet rs = ps.executeQuery();			
			if(rs.next()){
				bandera =  (rs.getString("CS_TASA_FLOATING") == null)?"N":rs.getString("CS_TASA_FLOATING");
			}
			rs.close();
			ps.close();			
			
		}catch(Exception e){
			log.error("Error en suceptibleFloating: ",e);
			throw new AppException("Error en consulta para conocer si la pyme es suceptible a tasa floating  ", e);
		} finally{
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB();
			log.info("suceptibleFloating(S)");
		}
		return bandera;
	}
	
} //fin de clase
