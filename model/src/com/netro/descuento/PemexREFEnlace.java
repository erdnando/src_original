	package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class PemexREFEnlace extends EpoPEF implements EpoEnlace,Serializable {


	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_PEMEX_REF";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_PEMEX_REF";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_PEMEX_REF";	}

	public String getDocuments(){

		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" , CG_CAMPO2"+
				" , cg_campo3"+
				" , cg_campo4"+
				" , cg_campo5"+
				" , '' as ic_if"+
				" , '' as IC_NAFIN_ELECTRONICO "+getCamposPEF()+
				" from " + getTablaDocumentos();
	}
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+" (IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR, IN_A_EJERCICIO, " + getCamposDigitoIdentificador() + ") "+
				" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"', "+ err.getInAEjercicio() + ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+")";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+
				" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
        " ,FN_TOTAL_MONTO_ACEP_MN="+acu.getFnTotalMontoAcepMn()+//FODEA 006 - 2010 ACF
				" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
        " ,FN_TOTAL_MONTO_RECH_MN="+acu.getFnTotalMontoRechMn()+//FODEA 006 - 2010 ACF
				" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
        " ,FN_TOTAL_MONTO_ACEP_DL="+acu.getFnTotalMontoAcepDl()+//FODEA 006 - 2010 ACF
				" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
        " ,FN_TOTAL_MONTO_RECH_DL="+acu.getFnTotalMontoRechDl()+//FODEA 006 - 2010 ACF
				" ,CS_ESTATUS='"+acu.getCsEstatus()+ "'"+
				" WHERE CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, FN_TOTAL_MONTO_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL, FN_TOTAL_MONTO_ACEP_DL"+//FODEA 006 - 2010 ACF
				", IN_TOTAL_RECH_DL, FN_TOTAL_MONTO_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+//FODEA 006 - 2010 ACF
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getFnTotalMontoAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoRechMn()+//FODEA 006 - 2010 ACF
        ","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+","+acu.getFnTotalMontoAcepDl()+//FODEA 006 - 2010 ACF
				","+acu.getInTotalRechDl()+","+acu.getFnTotalMontoRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";//FODEA 006 - 2010 ACF
	}

	public String getCondicionQuery(){
		return " ";
	}
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }

}
