package com.netro.descuento;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CargaDoctoDes {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CargaDoctoDes.class);	
	
	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario,		String sDfFechaVencPyme, String NoMandante,
					String fechaEntrega, String tipoCompra ,  String clavePresupuestaria , String periodo ,  
					boolean estaHabilitadaPublicacionEPOPEF,  AccesoDB con) throws NafinException {
	
		 insertaDocumentoCM(
					sIcPyme, sIgNumeroDocto, sDfFechaDocto, sDfFechaVenc, sIcMoneda, sFnMonto,
					sCsDsctoEspecial, sCtReferencia, sCampAd1, sCampAd2, sCampAd3, sCampAd4,
					sCampAd5, sNoBeneficiario, sPorcBeneficiario, sMontoBeneficiario, iNumProceso,
					sStatusRegistro, sMensajeError, iNumLinea, iNumCampos, sIcBeneficiario, 
					sDfFechaVencPyme, NoMandante, fechaEntrega, tipoCompra, clavePresupuestaria,
					periodo, estaHabilitadaPublicacionEPOPEF,  "", con);
	}
	
	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario,		String sDfFechaVencPyme, String NoMandante,
					String fechaEntrega, String tipoCompra ,  String clavePresupuestaria , String periodo ,  
					boolean estaHabilitadaPublicacionEPOPEF, String csCaracterEspecial, AccesoDB con) throws NafinException {
		boolean bOK = true;
		List varBind       = new ArrayList();	
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		log.info("CargaDocumentoEJB:: insertaDocumentoCM::(E)");
		log.debug("-------------Para Carga Masiva----------------");
				
		StringBuffer strSQL = new StringBuffer();
		
		try{
					
			strSQL.append("	INSERT INTO comtmp_carga_docto ("   +
				" cg_pyme_epo_interno, ig_numero_docto, df_fecha_docto,"   + 
				" df_fecha_venc, ic_moneda, fn_monto, cs_dscto_especial,"   +
				" ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				" cg_campo5, ic_if_benef, fn_porc_beneficiario,"   +
				" fn_monto_beneficiario, ic_carga_docto, cg_estatus, cg_error_reg,"   +
				" ic_linea, ig_numero_campos, ic_benef, df_fecha_venc_pyme, ic_mandante ");
			if(estaHabilitadaPublicacionEPOPEF) {
				strSQL.append(" ,DF_ENTREGA, CG_TIPO_COMPRA , CG_CLAVE_PRESUPUESTARIA ,CG_PERIODO  ");
			}			
			if("S".equals(csCaracterEspecial)){
				strSQL.append(" ,CS_CARACTER_ESPECIAL  ");
			}
			strSQL.append(" )");				
			strSQL.append(	"  VALUES ( ?, ? , ?, ?, ?, ? , ?, ? , ? , ? , ? , ?, ? , ? , ? , ? , ? , ?, ? , ? , ? , ? ,? , ?  " );
			
			if(estaHabilitadaPublicacionEPOPEF) {
				strSQL.append(" , ? ,  ?,  ?,  ? ");
			}
			if("S".equals(csCaracterEspecial)){
				strSQL.append(" , ? ");
			}
			strSQL.append(" )");

			varBind.add(sIcPyme);
			varBind.add(sIgNumeroDocto);
			varBind.add(sDfFechaDocto);
			varBind.add(sDfFechaVenc);
			varBind.add(sIcMoneda);
			varBind.add(sFnMonto);
			varBind.add(sCsDsctoEspecial);
			varBind.add(sCtReferencia);
			varBind.add(sCampAd1);
			varBind.add(sCampAd2);
			varBind.add(sCampAd3);
			varBind.add(sCampAd4);
			varBind.add(sCampAd5);
			varBind.add(sNoBeneficiario);
			varBind.add(sPorcBeneficiario);
			varBind.add(sMontoBeneficiario);
			varBind.add(String.valueOf(iNumProceso));
			varBind.add(sStatusRegistro);
			varBind.add(sMensajeError);
			varBind.add(String.valueOf(iNumLinea));
			varBind.add(String.valueOf(iNumCampos));
			varBind.add(sIcBeneficiario);
			varBind.add(sDfFechaVencPyme);
			varBind.add(NoMandante);
			if(estaHabilitadaPublicacionEPOPEF) {
				varBind.add(fechaEntrega);
				varBind.add(tipoCompra);
				varBind.add(clavePresupuestaria);
				varBind.add(periodo);			 
			}
			if("S".equals(csCaracterEspecial)){
				varBind.add(csCaracterEspecial);
			}
						
			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			ps.executeUpdate();
			ps.close(); 
					
		 // log.debug("-----strSQL.toString()-------"+strSQL.toString()); 
		  //log.debug("-----varBind-------"+varBind);		  
			
		} catch(Exception e) {
			bOK = false;
			log.error("CargaDocumentoEJB::insertaDocumentoCM(Exception): "+e);
			e.printStackTrace();
			throw new NafinException("DSCT0067");
		} finally {
			// quien manda invocar este metodo  se encarga de cerrar la conexi�n
			log.info("CargaDocumentoEJB:: insertaDocumentoCM::(S)");
		}
	}//fin insertaDocumentoCM
	
	
	

}
