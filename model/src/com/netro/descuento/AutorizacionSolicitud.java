/*************************************************************************************
*
* Nombre de Clase o de Archivo: AutorizacionSolicitud
*
* Versión: 0.1
*
* Fecha Creación: 22/01/2002
*
* Autor: Gilberto E. Aparicio
*
* Fecha Ult. Modificación: 07/02/2002
*
* Descripción de Clase:
*
*************************************************************************************/

package com.netro.descuento;

import com.netro.exception.NafinException;

import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.Registros;

@Remote
public interface AutorizacionSolicitud{

    
    
   	public boolean esIfBloqueado(String claveIf, String claveProducto) 
		throws NafinException;

    // Agregado 16/10/2002	--CARP
	public void borrarDoctosCargadosTmp( int eicProcSolic )
		throws NafinException;

	public String getArchivoDoctoAplicCred()
		throws NafinException;

	public Vector getDoctoAplicCredito()
		throws NafinException;

    // Agregado 15/10/2002	--CARP
	public Vector getDoctosCargados(String esAcuse, String esFolio,
									String esEstatus, String esIf,
									String esFechaOper, String esNoSirac, 
									String esNoPrestamo)
		throws NafinException;

    // Agregado 15/10/2002	--CARP
	public Vector getDoctosCargadosTmp( int eicProcSolic )
		throws NafinException;

	public Vector getSolicitudesManuales(String claveIF, String claveEPO,
			String claveMoneda, String tipoSolicitud,
			String folios, String estatusSolicitud)
		throws NafinException;

	public Vector getSolicOperadas(String folios)
		throws NafinException;

	public Vector getSolicProcesadas(String folios)
		throws NafinException;


	public void procesarSolicitudes(String folios)
		throws NafinException;

    // Agregado 15/10/2002	--CARP
	public Vector ovProcesarArchivo(String esNombreArchivo, String esDirectorio, String NoIf, String TipoPlazo)
		throws NafinException;

    // Agregado 01/11/2005	--MRZL
	public Vector ovProcesarArchivoXLS(List lDatos, String NoIf, String TipoPlazo)
		throws NafinException;

    // Agregado 01/11/2005	--MRZL
	public Vector sGetDatosBO(String NoIf, String TipoPiso)
		throws NafinException;

    // Agregado 16/10/2002	--CARP
	public Vector ovTransmitirDoctos( int eicProcSolic, String esAcuse )
		throws NafinException;
	
	// Agregado 15/01/2003  --HDG
	public Hashtable ohProcesaOperacSolicitudes(String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
												String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto, 
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios, 
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF, 
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI, 
												String NoAmortizaciones, String Amortizacion, String FechaPPC,  
												String DiaPago, String FechaPPI, String FechaVencDocto,  
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,  
												String AmortizacionAjuste, String ImporAmortAjuste, 
												String FechaAmortAjuste, String Acuse) 
												throws NafinException;
	// Agregado 15/01/2003  --HDG					
	public Vector getSolicitudes(String sNoSolicPortal) throws NafinException;


	public String getFechaMaxRecortado(String TipoPlazo, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda)
							throws NafinException;	


	public Hashtable ohProcesaOperacSolicTmp(String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
												String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto,
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios,
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF,
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI,
												String NoAmortizaciones, String Amortizacion, String FechaPPC,
												String DiaPago, String FechaPPI, String FechaVencDocto,
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,
												String AmortizacionAjuste, String ImporAmortAjuste,
												String FechaAmortAjuste,String tipoRenta,int numeroAmortRec,
												String fvencAmort[],String tipoAmort[],String montoAmort[],
												String sTipoPiso, String sSolicPYME, String sPyme,
												String sSolicTROYA, String sProducto, String codigoBaseOperacion, String destinoCredito ) throws NafinException;

	public String getFechaMaxRecortado(String TipoPlazo, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, String sProducto) throws NafinException;

	// Modificación para la generacion de archivos de origen, pantallas de Rechazos de Interfases (Prestamos, Disposiciones, Proyectos, Contratos)
	public String getDatosSolicitudCredito(String sNoSolicitud) throws NafinException;

	public String getDatosSolicitudCreditoxAcuse(String acuse)
		throws NafinException;
		
	public ArrayList getSolicitud (String claveIF, String sHorario)	
	   throws NafinException;

    public ArrayList getCamposcotPagos (String strsolicesp, String fechalimite)	
	   throws NafinException;
	   
	public Hashtable ohProcesaOperacSolicETmp (String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
											    String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto,
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios,
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF,
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI,
												String NoAmortizaciones, String Amortizacion, String FechaPPC,
												String DiaPago, String FechaPPI, String FechaVencDocto,
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,
												String AmortizacionAjuste, String ImporAmortAjuste,
												String FechaAmortAjuste,String tipoRenta,int numeroAmortRec,
												String fvencAmort[],String tipoAmort[],String montoAmort[],
												String sTipoPiso, String sSolicPYME, String sPyme,
												String sSolicTROYA, String sProducto) throws NafinException;  

	   
   
   public Vector ovTransmitirDoctosE( int eicProcSolic, String esAcuse )
     throws NafinException;
     
   public Map ovProcesarArchivoSiracCE(List lDatos)
     throws NafinException;
	 
   public Map confirmaSiracCredEle(String fpf[],String icUsuario)
     throws NafinException;
     
//>>>IHJ>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	public List getDatosCargaMasivaCE(String rutaFisica, String archivo, String ext)
     throws NafinException;

//>>>CARGA DETALLES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	public ArrayList getPendientes(String NoIf, String fecha_de, String fecha_a)
		throws NafinException;
	
	public String setDatosTmpDetSolic(String ic_proc_carga, List lRegistros)
		throws NafinException;
	
	public Registros getEstatisCargaSolicDet(String ic_proc_carga)
		throws NafinException;
	
	public Registros getRegistrosValidadosSolicDetalle(String ic_proc_carga, String cg_estatus, String ordenar)
	throws NafinException;
	
	public void setCargaDetalleMasiva(Registros registros)
		throws NafinException;
		
	public List detalleComprobarMonto(String ic_proc_carga)
		throws NafinException;

	public void setDatosCargaMasivaCE(
						String ig_clave_sirac,		String ig_sucursal,			String ic_moneda,			String ig_numero_docto,	
						String fn_importe_docto,	String fn_importe_dscto,	String ic_emisor,			String ic_tipo_credito, 
						String ic_periodicidad_c,	String ic_periodicidad_i,	String cg_bienes_servicios,	String ic_clase_docto, 
						String cg_domicilio_pago,	String ic_tasauf,			String cg_rmuf, 			String fg_stuf, 
						String ic_tasaif,			String in_numero_amort,		String ic_tabla_amort,		String df_ppc, 
						String df_ppi,				String in_dia_pago,			String df_v_documento,		String df_v_descuento,	
						String df_etc, 				String cg_lugar_firma,		String cs_amort_ajuste,		String cs_tipo_renta,
						String cg_destino_credito, String tipoPlazo,
						String ic_proc_solic,		List lRegistro, 			String cg_reg_ok,			String cg_error)
     throws NafinException;
     
	public List getTotalesCargaM(String ic_proc_solic)
     throws NafinException;
     
	public List getDetalleCarga(String ic_proc_solic)
     throws NafinException;

//<<<IHJ<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
/*     
public HashMap evaluarArchivoDeOperacionesEmergentes(String rutaArchivo,String tipoDeRegistro) 
	throws NafinException;  
*/

public String getArchivoConErroresDelRegOperacionesEmergentes(String strDirectorio, String rutaArchivo, String listaDocsConError)
	throws NafinException;   
/*
public String generaArchivoDeRegistroOperacionesEmergentes(String strDirectorio, String rutaArchivo, String listaDoctos)
	throws NafinException;

public String generaArchivoCSVConTablaDeAmortizaciones(String strDirectorio, String rutaArchivo, String listaDoctos,String tipoDeRegistro)
	throws NafinException;
*/
public String getCSVconDetallePorRegistroArchivosEnviados(String path,String numPrestamo, String icSolicPortal) throws Exception;
		
public List getDetalleArchivosEnviados(String ic_if)
		throws NafinException;  
		
public List getListaDeIntermediariosFinancierosNoBancarios()
		throws NafinException; 
		
public boolean depurarDetalles(String[] relaciones)
		throws NafinException; 
		
public List getDetalleAcuse(String carga)
		throws NafinException; 
		
public String generaArchivoAFirmarOperacionesExtemporaneas(String strDirectorio, String rutaArchivo, String listaDoctos)
		throws NafinException;
/*
public HashMap getResumenDeSolicitudesExtemporaneas(String strDirectorio, String nombreArchivo)
		throws NafinException;
*/		
public List getListaDeRegistrosExtemporaneosAFirmar(String strDirectorio, String nombreArchivo)
		throws NafinException;
		
public void subeArchivoCSVDeRegistroExtemporaneo(String nombreUsuario,String claveUsuario,String claveIF, String fechaDeActualizacion, String numeroTotalOperaciones, String montoTotalImporteDocumentos, String montoTotalImporteDescuento,String recibo, String directorio, String nombreArchivo,String hash,String tipoDePrograma) 
		throws NafinException;
		
public String getNumeroRecibo() 
		throws NafinException;

public HashMap autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente) 
		throws NafinException;
		
public String	getFechaDeActualizacion() 
		throws NafinException;
		
public boolean estaRepetidoHashMD5(String hash)
		throws NafinException;
		
//======================================================================>> Fodea 038 - extemporaneas BAZ
public HashMap consultarTablasEmergentes(String intermediario) throws NafinException;
public File getArchivoTablasEmergentes(String numeroSolicitud, String iFin, String directorio) throws NafinException;
public void eliminarTablasEmergentes(String numerosSol, String iFin) throws NafinException;
//======================================================================>> Fodea 038 - extemporaneas BAZ

public String getSecuenciaDeTiposDePrograma(List lista);
	
public List getTiposDePrograma(String tipoRegistro) 
		throws NafinException;
		
public String[] getClavesDeTipoDePrograma(String tipoRegistro)
		throws NafinException;
		
public boolean existeMasDeUnTipoDePrograma(String rutaArchivo, String listaDoctos)
		throws NafinException;
		
public String getTipoDePrograma(String rutaArchivo, String listaDoctos)
		throws NafinException;
		
public List getIFDescuento()
		throws NafinException;

public String getOperCredElecPDF(String strRutaTmp, String strRutaFisica, String cveSolicPortal) throws NafinException;

public List consultarTablasEmergentesExt(String intermediario) throws NafinException;
}

