package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class PemexREFEnlOperados extends EpoPEF implements EpoEnlOperados,Serializable {

	private String ic_epo = "";

	public PemexREFEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "COM_DOCTOS_OPE_PEMEX_REF";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,16)";
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9)";
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7)";
	}

	public String getInsertaDoctosOpe(){
		return
				"insert into "+getTablaDoctosOpe()+" (ic_documento, ic_estatus_docto,"+
						" df_fecha_venc, cg_razon_social_pyme, ig_numero_docto, fn_monto,"+
						" in_numero_proveedor, df_fecha_docto, cc_acuse, cg_razon_social_if,"+
						" in_a_ejercicio, ic_nafin_electronico, cg_rfc "+ getCamposPEFOper()+") ";
	}


	public String getDoctosOperados(Hashtable alParamEPO){
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR

		return	" SELECT d.ic_documento"   +
				" , 4 as ic_estatus_docto"   +
				" , d.df_fecha_venc"   +
				" , substr(d.cg_campo2,1,70) as nombrePyme"   +
				" , d.ig_numero_docto"   +
				" , d.fn_monto"   +
				" , pe.cg_pyme_epo_interno as numeroProveedor"   +
				" , d.df_fecha_docto as fechaEmision"   +
				" , a.cc_acuse"   +
				" , substr(i.cg_razon_social,1,50) as nombreIf"   +
				" , TO_NUMBER(substr(d.cg_campo3,1,4)) as in_a_ejercicio"   +
				" , TO_CHAR(rn.ic_nafin_electronico) as ic_nafin_electronico"   +
				" , substr(d.cg_campo1,1,16) as cg_rfc"   +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+this.getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"  FROM com_documento d"   +
				"  , com_docto_seleccionado ds"   +
				"  , com_solicitud s"   +
				"  , com_acuse2 a"   +
				"  , comcat_if i"   +
				"  , comrel_pyme_epo pe"   +
				"  , comrel_nafin rn"   +
				"  WHERE a.cc_acuse = ds.cc_acuse"   +
				"  and s.ic_documento = ds.ic_documento"   +
				"  and ds.ic_documento = d.ic_documento"   +
				"  and ds.ic_if = rn.ic_epo_pyme_if"   +
				"  and d.ic_if = i.ic_if"   +
				"  and d.ic_pyme = pe.ic_pyme"   +
				"  and d.ic_epo = pe.ic_epo"   +
				"  and d.ic_epo = " +ic_epo +
				"  and d.ic_estatus_docto in (4,16)"   +
				"  and s.df_fecha_solicitud >= trunc(sysdate)"   +
				"  and s.df_fecha_solicitud < trunc(sysdate+1)"   +
				"  and rn.cg_tipo = 'I'"  ;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		return "   SELECT d.ic_documento"   +
				"  , d.ic_estatus_docto"   +
				"  , d.df_fecha_venc"   +
				"  , substr(d.cg_campo2,1,70) as nombrePyme"   +
				"  , d.ig_numero_docto"   +
				"  , d.fn_monto"   +
				"  , pe.cg_pyme_epo_interno as numeroProveedor"   +
				"  , d.df_fecha_docto as fechaEmision"   +
				"  , null as cc_acuse"   +
				"  , null as nombreIf"   +
				"  , TO_NUMBER(substr(d.cg_campo3,1,4)) as in_a_ejercicio"   +
				"  , null as ic_nafin_electronico"   +
				"  , substr(d.cg_campo4,1,16) as cg_rfc"   +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+this.getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"  FROM com_documento d"   +
				"  , comhis_cambio_estatus ce"   +
				"  , comrel_pyme_epo pe"   +
				"  WHERE d.ic_documento = ce.ic_documento"   +
				"  and d.ic_pyme = pe.ic_pyme"   +
				"  and d.ic_epo = pe.ic_epo"   +
				"  and ce.dc_fecha_cambio >= TRUNC(sysdate)"   +
				"  and ce.dc_fecha_cambio < TRUNC(sysdate+1)"   +
				"  and d.ic_estatus_docto in (5,6,7)"   +
				"  and d.ic_epo = " +ic_epo +
				"  AND ce.IC_CAMBIO_ESTATUS IN (4,5,6) ";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    //25 Enero 2010 - ACF (i)
    String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    String	diaAct	= fechaHoy.substring(0, 2);
    String	mesAct	= fechaHoy.substring(3, 5);
    String	anyoAct	= fechaHoy.substring(6, 10);
    Calendar cal = new GregorianCalendar();
    cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));
    String condicion = "";

    if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
      condicion = " AND d.df_fecha_venc >= TRUNC (SYSDATE + NVL (cpe.ig_dias_minimo, cpn.in_dias_minimo) - 3)";
    } else {
      condicion = " AND d.df_fecha_venc >= TRUNC (SYSDATE + NVL (cpe.ig_dias_minimo, cpn.in_dias_minimo) - 1)";
    }
    //25 Enero 2010 - ACF (f)
		return 	" SELECT d.ic_documento, ic_estatus_docto, d.df_fecha_venc, SUBSTR (d.cg_campo2, 1, 70) AS nombrepyme,"   +
				"        d.ig_numero_docto, d.fn_monto, pe.cg_pyme_epo_interno AS numeroproveedor,"   +
				"        d.df_fecha_docto AS fechaemision, NULL AS cc_acuse, NULL AS nombreif,"   +
				"        TO_NUMBER (SUBSTR (d.cg_campo3, 1, 4)) AS in_a_ejercicio, NULL AS ic_nafin_electronico,"   +
				"        SUBSTR (d.cg_campo4, 1, 16) AS cg_rfc"   +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+this.getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"   FROM com_documento d, comcat_pyme p, comrel_pyme_epo pe, comrel_producto_epo cpe, comcat_producto_nafin cpn"   +
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_epo = cpe.ic_epo"   +
				"    AND cpe.ic_producto_nafin = cpn.ic_producto_nafin"   +
				"    AND cpn.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto = 9"   +
        //"    AND d.df_fecha_venc = sigfechahabilxepo (d.ic_epo, TRUNC (SYSDATE), "+diasMinimos+", '+')-1"   +
        //"    AND d.df_fecha_venc >= TRUNC (SYSDATE + NVL (cpe.ig_dias_minimo, cpn.in_dias_minimo))"   +
        condicion +//25 Enero 2010 - ACF (i)
				"    AND d.df_fecha_venc < TRUNC (SYSDATE + NVL (cpe.ig_dias_minimo, cpn.in_dias_minimo) + 1)"   +
				"    AND d.ic_epo = "+ic_epo+
		//FODEA 006-Feb2010 Rebos - Se agrego la notificaci�n de documentos publicados como vencidos
		" UNION " +
				" SELECT d.ic_documento, ic_estatus_docto, d.df_fecha_venc, SUBSTR (d.cg_campo2, 1, 70) AS nombrepyme,"   +
				"        d.ig_numero_docto, d.fn_monto, pe.cg_pyme_epo_interno AS numeroproveedor,"   +
				"        d.df_fecha_docto AS fechaemision, NULL AS cc_acuse, NULL AS nombreif,"   +
				"        TO_NUMBER (SUBSTR (d.cg_campo3, 1, 4)) AS in_a_ejercicio, NULL AS ic_nafin_electronico,"   +
				"        SUBSTR (d.cg_campo4, 1, 16) AS cg_rfc"   +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+this.getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"   FROM com_documento d, comcat_pyme p, comrel_pyme_epo pe, comrel_producto_epo cpe, comcat_producto_nafin cpn"   +
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_epo = cpe.ic_epo"   +
				"    AND cpe.ic_producto_nafin = cpn.ic_producto_nafin"   +
				"    AND cpn.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto = 9"   +
			  "    AND d.df_alta > TRUNC(SYSDATE-1)"+
		    "    AND d.ic_epo = "+ic_epo;
	}

	/**
	 * Se sobreescribe el metodo de EpoPEF, debido a que en este enlace es necesario
	 * especificar el dblink ORALINK en la funci�n para que pueda
	 * funcionar adecuadamente.
	 * @return Cadena con la secci�n del query que determina el digito identificador.
	 */
	public String getCamposDigitoIdentificadorCalc(){
		return "  rellenaCeros@ORALINK(d.ic_epo||'','0000') || rellenaCeros@ORALINK(d.ic_documento||'','00000000000') as " + super.getCamposDigitoIdentificador();
	}

}