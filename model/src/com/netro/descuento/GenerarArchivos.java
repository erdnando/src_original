package com.netro.descuento;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		GenerarArchivos.java
*
*	Proposito:	Clase GenerarArchivos
*
*	Lenguaje:	Java
*
*	Autor:		Salvador Saldivar Barajas
*
*	Fecha Creacion:	18/09/2006
*
****************************************************************************************************************/

import netropology.utilerias.*;
import java.sql.*;
import java.io.*;

import org.apache.commons.logging.Log;

public class GenerarArchivos{
	
    AccesoDB con = new AccesoDB();
    ResultSet rs = null;
    CreaArchivo archivo = new CreaArchivo();
    ArmaQueryFile queryf = new ArmaQueryFile();
    String contenidoArchivo = null;
    String QryArmado = null;
    StringBuffer qrySentencia = null;
    GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
    private String rutaServer;
    
    private final static Log log = ServiceLocator.getInstance().getLog(GenerarArchivos.class);
    
            
    public GenerarArchivos(){
    }
	
/**
 * Generar archivo zip para vencimientos
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ author Ricardo Jimenez Vargas
 *@ mod 8 de marzo de 2018 implementacion log4j
 *@ param ic_if identificador del cliente
 *@ param parametro Valor que nos indica si el formato del archivo sera grande o peque�o
 *@ param fechaCorte Fecha de probable pago
 *@ param fechaElim Fecha de archivos que se deben eliminar
 *@ return regresa mensaje de como termino el proceso
 */
	//public String armarArchivoVencimientos(String ic_if, String parametro, String FechaCorte, String fechaElim){				
    public String armarArchivoVencimientos(String ic_if, String parametro, String FechaCorte, String fechaElim, String flagNumDocto){//FODEA 015 - 2009 ACF

        log.info("GenerarArchivos:armarArchivoVencimientos(E)");
        try{
            
            QryArmado = queryf.regresaQueryVenc(ic_if,"","",FechaCorte,"");
            
            con.conexionDB();
            rs = con.queryDB(QryArmado);
            
            contenidoArchivo = conten.llenarContenido(rs, parametro, 1, flagNumDocto,"");//FODEA 015 - 2009
            
            FechaCorte = FechaCorte.substring(0,2)+"_"+FechaCorte.substring(3,5)+"_"+FechaCorte.substring(6,10);
            creartxt(parametro,ic_if,FechaCorte,1);
            depurarArchivosZip(1, fechaElim,ic_if,parametro);
            
            if(parametro.equals("N")){
                rs = con.queryDB(QryArmado);
                contenidoArchivo = conten.llenarContenido(rs, "S", 1, flagNumDocto,"");//FODEA 015 - 2009  ACF
                creartxt("S",ic_if,FechaCorte,1);
                depurarArchivosZip(1, fechaElim,ic_if,"S");
            }
            
            rs.close();
            con.cierraStatement();			
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if (con.hayConexionAbierta()){
                con.cierraConexionDB();
            }		
            log.info("GenerarArchivos:armarArchivoVencimientos(S)");
        }
            return "Generacion exitosa";				
    }
	
/**
 * Generar archivo zip para operados
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ author Ricardo Jimenez Vargas
 *@ mod 8 de marzo de 2018 implementacion log4j
 *@ param ic_if identificador del cliente
 *@ param parametro Valor que nos indica si el formato del archivo sera grande o peque�o
 *@ param fechaCorte Fecha de probable pago
 *@ param fechaElim Fecha de archivos que se deben eliminar
 *@ return regresa mensaje de como termino el proceso
*/	
	
    public String armarArchivosOperados(String ic_if, String parametro, String FechaCorte, String fechaElim){
        try{
            log.info("GenerarArchivos:armarArchivosOperados(E)");
            
            QryArmado = queryf.regresaQueryOper(ic_if,"","",FechaCorte,"");
            
            con.conexionDB();
            rs = con.queryDB(QryArmado);
            
            contenidoArchivo = conten.llenarContenido(rs, parametro, 2,"","");		  
            
            FechaCorte = FechaCorte.substring(0,2)+"_"+FechaCorte.substring(3,5)+"_"+FechaCorte.substring(6,10);
            creartxt(parametro,ic_if,FechaCorte,2);
            depurarArchivosZip(2, fechaElim,ic_if,parametro);
            
            if(parametro.equals("N")){
                rs = con.queryDB(QryArmado);
                contenidoArchivo = conten.llenarContenido(rs, "S", 2,"","");
                creartxt("S",ic_if,FechaCorte,2);
                depurarArchivosZip(2, fechaElim,ic_if,"S");
            }
            
            rs.close();
            con.cierraStatement();	
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if (con.hayConexionAbierta()){
                con.cierraConexionDB();
            }		
            log.info("GenerarArchivos:armarArchivosOperados(S)");
        }
        
        return "Generacion exitosa";		
    }
	
/**
 * Generar archivo zip para estados de cuenta
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ author Ricardo Jimenez Vargas
 *@ mod 8 de marzo de 2018 implementacion log4j
 *@ param ic_if identificador del cliente
 *@ param parametro Valor que nos indica si el formato del archivo sera grande o peque�o
 *@ param fechaCorte Fecha de probable pago
 *@ param fechaElim Fecha de archivos que se deben eliminar
 *@ return regresa mensaje de como termino el proceso
*/	
	
    public String armarArchivosEst_Cuenta(String ic_if, String FechaCorte, String fechaElim){		
        try{
            log.info("GenerarArchivos:armarArchivosEst_Cuenta(E)");
            //contenidoArchivo = new String();
            conten.setrutaServerc(this.rutaServer);
            
            contenidoArchivo = queryf.regresaQueryEdoCta(ic_if,"","",FechaCorte,"");
            
            con.conexionDB();
            rs = con.queryDB(contenidoArchivo);
            
            FechaCorte = FechaCorte.substring(0,2)+"_"+FechaCorte.substring(3,5)+"_"+FechaCorte.substring(6,10);
            
            contenidoArchivo = conten.llenarContenido(rs, "S", 3,ic_if,FechaCorte);
            
            rs.close();
            con.cierraStatement();
            
            creartxt("S",ic_if,FechaCorte,3);
            depurarArchivosZip(3, fechaElim,ic_if,"S");
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if(con.hayConexionAbierta()){
                con.cierraConexionDB();
            }		
            log.info("GenerarArchivos:armarArchivosEst_Cuenta(S)");
        }		
        return "Generacion exitosa";
    }
	
/**
 * Metodo que crea archivo txt y zip
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ author Ricardo Jimenez Vargas
 *@ mod 8 de marzo de 2018 implementacion log4j, y correccion de la generacion de los zip en los operados
 *@ param par Valor que nos indica parte del nombre del archivo
 *@ param icif Valor que nos indica parte del nombre del archivo
 *@ param FCorte Valor que nos indica parte del nombre del archivo
 *@ param fechaElim Fecha de archivos que se deben eliminar
 *@ param tipo Valor que indica si es vencimiento, operado o estado de cuenta
 *@ return void
*/	
	
    public void creartxt(String par, String icif, String FCorte, int tipo){
        CreaArchivo archivo = new CreaArchivo();
        GenerarArchivoZip zip = new GenerarArchivoZip();		
        
        boolean commit = true;
        try{
            PagosIFNB beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
            log.info("GenerarArchivos:creartxt(E)");
            
            String nombreArchZip = "";
            //String FechaCorte = FCorte.substring(0,2)+"/"+FCorte.substring(3,5)+"/"+FCorte.substring(6,10);
            
            switch(tipo){
                case 1:
                    if(!archivo.make(contenidoArchivo,this.rutaServer+"00tmp/13descuento/","archvenc_"+FCorte+"_"+icif+"_"+par, ".txt"))
                        log.error("GenerarArchivos:Error al generar el archivo");
                    else{
                        zip.generandoZip(this.rutaServer+"00tmp/13descuento/",this.rutaServer+"00tmp/13descuento/","archvenc_"+FCorte+"_"+icif+"_"+par);
                        File f = new File(this.rutaServer+"00tmp/13descuento/"+"archvenc_"+FCorte+"_"+icif+"_"+par+".txt");
                        nombreArchZip = "archvenc_"+FCorte+"_"+icif+"_"+par+".zip";
                        if(f.exists())
                        f.delete();
                        
                        beanPagos.guardarArchivosZip(nombreArchZip, this.rutaServer+"00tmp/13descuento/");
                    }						
                    break;
                case 2:				
                    if(!archivo.make(contenidoArchivo,this.rutaServer+"00tmp/13descuento/","archoper_"+FCorte+"_"+icif+"_"+par, ".txt"))
                        log.error("GenerarArchivos:Error al generar el archivo");
                    else{
                        zip.generandoZip(this.rutaServer+"00tmp/13descuento/",this.rutaServer+"00tmp/13descuento/","archoper_"+FCorte+"_"+icif+"_"+par);
                        File f = new File(this.rutaServer+"00tmp/13descuento/"+"archoper_"+FCorte+"_"+icif+"_"+par+".txt");
                        nombreArchZip = "archoper_"+FCorte+"_"+icif+"_S.zip";
                        if(f.exists())
                        f.delete();
                        
                        beanPagos.guardarArchivosZip(nombreArchZip, this.rutaServer+"00tmp/13descuento/");
                    }										
                    break;
                case 3:				
                    zip.generandoZip(this.rutaServer+"00tmp/13descuento/",this.rutaServer+"00tmp/13descuento/","archedocuenta_"+FCorte+"_"+icif+"_S");              
                    File f = new File(this.rutaServer+"00tmp/13descuento/"+"archedocuenta_"+FCorte+"_"+icif+"_S.txt");
                    nombreArchZip = "archedocuenta_"+FCorte+"_"+icif+"_S.zip";
                    if(f.exists())
                    f.delete();
                    
                    beanPagos.guardarArchivosZip(nombreArchZip, this.rutaServer+"00tmp/13descuento/");
                    
                    break;
                default:
                    log.error("GenerarArchivos:Opcion erronea");
            }
        }catch(Exception e){
            e.printStackTrace();
            commit=false;
        }finally{
            if(con.hayConexionAbierta()){
                con.terminaTransaccion(commit);
            }
            log.info("GenerarArchivos:creartxt(S)");
        }
    }
	
/**
 * Metodo que elimina archivo zip
 *@ author Salvador Saldivar Barajas
 *@ since 18 de septiembre de 2006
 *@ author Ricardo Jimenez Vargas
 *@ mod 8 de marzo de 2018 implementacion log4j
 *@ param tipo Valor que indica si es vencimiento, operado o estado de cuenta
 *@ param fechaElim Fecha de archivos que se deben eliminar
 *@ param iif Valor que nos indica parte del nombre del archivo
 *@ param p Valor que nos indica parte del nombre del archivo
 *@ return void
*/		
	
    public void depurarArchivosZip(int tipo, String fechaElim, String iif, String p){
    
        try{
            log.info("GenerarArchivos:depurarArchivosZip(E)");
            PagosIFNB beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
            switch(tipo){
                case 1:					
                    beanPagos.depuraArchivosZip("archvenc_"+fechaElim+"_"+iif+"_S.zip");
                    beanPagos.depuraArchivosZip("archvenc_"+fechaElim+"_"+iif+"_N.zip");
                    break;
                case 2:					
                    beanPagos.depuraArchivosZip("archoper_"+fechaElim+"_"+iif+"_S.zip");
                    beanPagos.depuraArchivosZip("archoper_"+fechaElim+"_"+iif+"_N.zip");
                    break;
                case 3:				
                    beanPagos.depuraArchivosZip("archedocuenta_"+fechaElim+"_"+iif+"_S.zip");
                    break;
                default:
                    break;
            }								
        }catch(Exception e){
            e.printStackTrace();
        }
        finally{
            log.info("GenerarArchivos:depurarArchivosZip(S)");
        }
    }

    /**
     * Metodo para asignar la ruta donde se procesan los archivos en el filesystem
     *@ author Salvador Saldivar Barajas
     *@ since 18 de septiembre de 2006
     *@ author Ricardo Jimenez Vargas
     *@ mod 8 de marzo de 2018 implementacion log4j
     *@ param ruta Parametro de la ruta fisica del filesystem
     *@ return void
    */              
	
    public void setrutaServer(String ruta){
        this.rutaServer = ruta;
        log.info("GenerarArchivos:rutaServer: "+rutaServer);
    }
	
}