package com.netro.descuento;
import java.sql.*;
import java.util.*;
import java.util.Map.*;
import netropology.utilerias.*;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
import org.apache.commons.logging.Log;


public class BitOfertaTasas  {
	public BitOfertaTasas() {
	}
	
	private final static Log log = ServiceLocator.getInstance().getLog(BitOfertaTasas.class);
	
	public static void agregarRegBitacora(AccesoDB con,
			String claveUsuario, String cveOferta, HashMap infoFianzaAnterior, 
			HashMap infoFianzaActual, String accion, String cveCtaBanc) 
			throws AppException {
		log.info("BitOfertaTasas::agregarRegBitacora(E)");

		String icOferta = "";
		String icIf = "";
		String icEpo = "";
		String icPyme = "";
		String icCuenta = "";
		String icMoneda = "";
		String acuseTasa = "";
		String acusePyme = "";
		String puntosPref = "";
		
		
		//**********************************Validación de parametros:*****************************
		try {
			if (con == null || claveUsuario == null || 
					(infoFianzaAnterior == null && infoFianzaActual == null)
				) {
				throw new AppException("Los parametros no pueden ser nulos");
			}

			if (!con.hayConexionAbierta()) {
				throw new AppException("La conexion a BD no es valida ");
			}

			
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" con=" + con + "\n" +
					" claveUsuario=" + claveUsuario + "\n" +
					" infoFianzaAnterior=" + infoFianzaAnterior + "\n" +
					" infoFianzaActual=" + infoFianzaActual);
			throw new AppException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		
		String nombreCompletoUsuario = "";
		try {
			UtilUsr utilUsr = new UtilUsr();
			Usuario usuario = utilUsr.getUsuario(claveUsuario);
			nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno() + " " + usuario.getNombre();
		} catch (Exception e) {
			throw new AppException("Error al obtener los datos del usuario");
		}
		
		
		//Comparacion de datos para detectar cambios
		StringBuffer strCamposModif = new StringBuffer();
		StringBuffer strDatAnterior = new StringBuffer();
		StringBuffer strDatActual = new StringBuffer();
		HashMap mpShowData = new HashMap();
		mpShowData.put("Monto_Desde", "Monto_Desde");
		mpShowData.put("Monto_Hasta", "Monto_Hasta");
		mpShowData.put("Puntos_Pref", "Puntos_Pref");
		mpShowData.put("Estatus", "Estatus");
		
		if(infoFianzaAnterior.size()>0 ){
			Iterator it = infoFianzaAnterior.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry)it.next();
				String key = (String)e.getKey();
				String value = e.getValue()==null?"":(String)e.getValue();
				
				if(mpShowData.containsKey(key)){
					strDatAnterior.append(key+" = "+value+"\n");
				}
				
				
			}
		}
		
		if(infoFianzaActual.size()>0){
			Iterator it = infoFianzaActual.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry)it.next();
				String key = (String)e.getKey();
				String value = e.getValue()==null?"":(String)e.getValue();
				if(mpShowData.containsKey(key)){
					strCamposModif.append(key+" = "+value+"\n");
				}
			}
		}


		if(infoFianzaAnterior.size()<=0 && infoFianzaActual.size()>0){
			accion = "A";
		}
		
		//se obtienen claves para bitacora
		if("E".equals(accion)){
			if(infoFianzaAnterior.size()>0){
	
				icOferta = infoFianzaAnterior.get("Clave_Oferta").toString();
				icIf = infoFianzaAnterior.get("Clave_If").toString();
				icEpo = infoFianzaAnterior.get("Clave_Epo").toString();
				//icPyme = infoFianzaActual.get("Clave_Pyme").toString();
				//icCuenta = infoFianzaActual.get("Clave_Cuenta").toString();
				icMoneda = infoFianzaAnterior.get("Clave_Moneda").toString();
				acuseTasa = infoFianzaAnterior.get("Acuse_Tasa").toString();
				//acusePyme = infoFianzaActual.get("Acuse_Pyme").toString();
				puntosPref = infoFianzaAnterior.get("Puntos_Pref").toString();
	
			}
		}else{
				if(infoFianzaActual.size()>0){
	
				icOferta = infoFianzaActual.get("Clave_Oferta").toString();
				icIf = infoFianzaActual.get("Clave_If").toString();
				icEpo = infoFianzaActual.get("Clave_Epo").toString();
				//icPyme = infoFianzaActual.get("Clave_Pyme").toString();
				//icCuenta = infoFianzaActual.get("Clave_Cuenta").toString();
				icMoneda = infoFianzaActual.get("Clave_Moneda").toString();
				acuseTasa = infoFianzaActual.get("Acuse_Tasa").toString();
				//acusePyme = infoFianzaActual.get("Acuse_Pyme").toString();
				puntosPref = infoFianzaActual.get("Puntos_Pref").toString();
	
			}	
		}
		
		String strSQLBitacora = "";
				
		try {
			if(strCamposModif.length()>0 || (infoFianzaAnterior.size()<=0 && infoFianzaActual.size()>0) || !"".equals(accion)){
				PreparedStatement ps = null;
				ResultSet rs = null;
				PreparedStatement ps2 = null;
				ResultSet rs2 = null;
				List lstPymes = new ArrayList();
				
				String query = "SELECT cp.ic_cuenta_bancaria cvecuenta, cc.ic_pyme cvepyme, cp.cc_acuse acusepyme " +
								"  FROM com_oferta_tasa_if_epo co, " +
								"       comrel_oferta_tasa_pyme_if cp, " +
								"       comrel_cuenta_bancaria cc " +
								" WHERE co.ic_oferta_tasa = cp.ic_oferta_tasa " +
								" 	 AND cp.ic_cuenta_bancaria = cc.ic_cuenta_bancaria " +
								"   AND co.ic_oferta_tasa = ? ";
				if(cveCtaBanc!=null && !"".equals(cveCtaBanc))
					query += "   AND cp.ic_cuenta_bancaria = ? ";
								
				ps2 = con.queryPrecompilado(query);
				ps2.setLong(1, Long.parseLong(cveOferta));
				if(cveCtaBanc!=null && !"".equals(cveCtaBanc))
					ps2.setLong(2, Long.parseLong(cveCtaBanc));
				rs2 = ps2.executeQuery();
				
				while(rs2!=null && rs2.next()){
				
					strSQLBitacora = 
							"INSERT INTO bit_tasas_pref_oferta " +
							"            (ic_bit_oferta, ic_oferta_tasa, ic_epo, ic_pyme, ic_if, " +
							"             ic_cuenta_bancaria, ic_moneda, cg_tipotasa, fn_puntos, " +
							"             cg_estatus, cg_usuario, cg_anterior, cg_actual " +
							"            ) " +
							"     VALUES (seq_bit_tasas_pref_oferta.nextval, ?, ?, ?, ?, " +
							"             ?, ?, ?, ?, " +
							"             ?, ?, ?, ? " +
							"            ) ";
	
					
					if("A".equals(accion)){
						//ALTA
						strDatAnterior = new StringBuffer();
						strDatAnterior.append("N/A \n");
					}else if("E".equals(accion)){
						//BAJA
						strCamposModif = new StringBuffer();
						strCamposModif.append("N/A \n");
					}else{
						accion = "M";
					}
						
					ps = con.queryPrecompilado(strSQLBitacora);
					int p = 0;
					ps.setLong(++p,Long.parseLong(icOferta));
					ps.setLong(++p,Long.parseLong(icEpo));
					//ps.setLong(++p,Long.parseLong(icPyme));
					ps.setLong(++p,Long.parseLong(rs2.getString("cvepyme")));
					ps.setLong(++p,Long.parseLong(icIf));
					//ps.setLong(++p,Long.parseLong(icCuenta));
					ps.setLong(++p,Long.parseLong(rs2.getString("cvecuenta")));
					ps.setLong(++p,Long.parseLong(icMoneda));
					ps.setString(++p,"O");
					ps.setDouble(++p,Double.parseDouble(puntosPref));
					ps.setString(++p,accion);//estatus
					ps.setString(++p,claveUsuario);//usuario
					ps.setString(++p,strDatAnterior.toString());
					ps.setString(++p,strCamposModif.toString());
					ps.executeUpdate();
					ps.close();
				
				}
				rs2.close();
				ps2.close();
			}

		} catch(SQLException e) {
			throw new AppException("Error al guardar datos en Bitacora de Oferta de Tasas", e);
		}finally{
			log.info("BitOfertaTasas::agregarRegBitacora(S)");	
		}
	}
	
	
	public HashMap getInfoOfertaTasas(AccesoDB con, String cveTasa){
		
		log.info("BitOfertaTasas::getInfoFianza(E)");	
		String query = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap hmFianza = new HashMap();
		
		try{
			
			query = "SELECT co.ic_oferta_tasa cveoferta, co.ic_if cveif, co.ic_epo cveepo, " +
					"       co.ic_moneda cvemoneda, co.fn_puntos puntos, " +
					"       co.cg_estatus estatus, co.fg_montodesde montodesde, " +
					"       co.fg_montohasta montohasta, co.cc_acuse acusetasa " +
					"  FROM com_oferta_tasa_if_epo co " +
					"   WHERE co.ic_oferta_tasa = ? " ;
					//"   AND cp.ic_cuenta_bancaria = ? ";

					
			cveTasa = (cveTasa==null||"".equals(cveTasa))?"0":cveTasa;
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveTasa));
			//ps.setLong(2,Long.parseLong(cveCuenta));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				hmFianza.put("Clave_Oferta", rs.getString("cveoferta"));
				hmFianza.put("Clave_If", rs.getString("cveif"));
				hmFianza.put("Clave_Epo", rs.getString("cveepo"));
				hmFianza.put("Clave_Moneda", rs.getString("cvemoneda"));
				hmFianza.put("Monto_Desde", rs.getString("montodesde"));
				hmFianza.put("Monto_Hasta", rs.getString("montohasta"));
				hmFianza.put("Puntos_Pref", rs.getString("puntos"));
				hmFianza.put("Estatus", rs.getString("estatus"));
				hmFianza.put("Acuse_Tasa", rs.getString("acusetasa"));
				//hmFianza.put("Acuse_Pyme", rs.getString("acusepyme"));
			}

			return hmFianza;			
		}catch(Throwable t){
			throw new AppException("Error al obtener la info actual de Oferta de Tasa",t);
		}finally{
			log.info("BitOfertaTasas::getInfoFianza(S)");	
		}
	}
	
	
	public HashMap getInfoOfertaTasas(AccesoDB con, String cveTasa, String cveCuenta){
		
		log.info("BitOfertaTasas::getInfoFianza(E)");	
		String query = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap hmFianza = new HashMap();
		
		try{
			
			query = "SELECT co.ic_oferta_tasa cveoferta, co.ic_if cveif, co.ic_epo cveepo, " +
					"       co.ic_moneda cvemoneda, cc.ic_pyme cvepyme, co.fn_puntos puntos, " +
					"       co.cg_estatus estatus, co.fg_montodesde montodesde, " +
					"       co.fg_montohasta montohasta, co.cc_acuse acusetasa, " +
					"       cp.ic_cuenta_bancaria cvecuenta, cp.cc_acuse acusepyme " +
					"  FROM com_oferta_tasa_if_epo co, " +
					"       comrel_oferta_tasa_pyme_if cp, " +
					"       comrel_cuenta_bancaria cc " +
					" WHERE co.ic_oferta_tasa = cp.ic_oferta_tasa " +
					"   AND cp.ic_cuenta_bancaria = cc.ic_cuenta_bancaria " +
					"   AND co.ic_oferta_tasa = ? " +
					"   AND cp.ic_cuenta_bancaria = ? ";

					
			cveTasa = (cveTasa==null||"".equals(cveTasa))?"0":cveTasa;
			ps = con.queryPrecompilado(query);
			ps.setLong(1,Long.parseLong(cveTasa));
			ps.setLong(2,Long.parseLong(cveCuenta));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				hmFianza.put("Clave_Oferta", rs.getString("cveoferta"));
				hmFianza.put("Clave_If", rs.getString("cveif"));
				hmFianza.put("Clave_Epo", rs.getString("cveepo"));
				hmFianza.put("Clave_Pyme", rs.getString("cvepyme"));
				hmFianza.put("Clave_Cuenta", rs.getString("cvecuenta"));
				hmFianza.put("Clave_Moneda", rs.getString("cvemoneda"));
				hmFianza.put("Puntos_Pref", rs.getString("puntos"));
				hmFianza.put("Estatus", rs.getString("estatus"));
				hmFianza.put("Monto_Desde", rs.getString("montodesde"));
				hmFianza.put("Monto_Hasta", rs.getString("montohasta"));
				hmFianza.put("Acuse_Tasa", rs.getString("acusetasa"));
				hmFianza.put("Acuse_Pyme", rs.getString("acusepyme"));
			}

			return hmFianza;			
		}catch(Throwable t){
			throw new AppException("Error al obtener la info actual de Oferta de Tasa",t);
		}finally{
			log.info("BitOfertaTasas::getInfoFianza(S)");	
		}
	}
	
	
}