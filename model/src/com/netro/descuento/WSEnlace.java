package com.netro.descuento;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WSEnlace extends EpoPEF implements EpoEnlace, Serializable { 

	private String ic_epo 		= "";
	private String cg_receipt 	= "";
	private String ic_usuario 	= "";
		
	public WSEnlace (String ic_epo, String cg_receipt, String ic_usuario) {
		this.ic_epo 		= ic_epo;
		this.cg_receipt 	= cg_receipt;
		this.ic_usuario 	= ic_usuario;
	}//WSEnlace
	
	public String getTablaDocumentos() {
		return "comtmp_doctos_pub_ws";
	}//getTablaDocumentos
	
	public String getTablaAcuse() {
		return "com_doctos_pub_acu_ws";
	}//getTablaAcuse
	
	public String getTablaErrores() {
		return "com_doctos_err_pub_ws";
	}//getTablaErrores
	
	public String getIc_usuario() {
		return this.ic_usuario;
	}//getIc_usuario
	
	public String getCg_receipt() {
		return this.cg_receipt;
	}//getIc_usuario
		
	public String getDocuments() {
		String querySentencia = 
			" SELECT ig_numero_docto, cg_pyme_epo_interno, df_fecha_docto, df_fecha_venc,"   +
			"        ic_moneda, fn_monto, cs_dscto_especial, ct_referencia, cg_campo1,"   +
			"        cg_campo2, cg_campo3, cg_campo4, cg_campo5, "   +
			"        '' ic_if, ic_nafin_electronico"   +getCamposPEF()+ ", cg_clave_mandante, cs_caracter_especial, CS_CADENA_ORIGINAL   " +
			"   FROM "+getTablaDocumentos()+" "   +
			getCondicionQuery();
		
		return querySentencia;
	}//getDocuments
	
	public String getInsertaErrores(ErroresEnl err) {
        int tamanioCampoError = 199;
        String cadena_original = err.getCadenaOriginal();
	    cadena_original = cadena_original.replace("'", "''");
        // Cortar mensajes de error cuando excede el permitido en la BD 
        String error200 = err.getCgError().length() > tamanioCampoError ? err.getCgError().substring(0, tamanioCampoError) : err.getCgError();
		String querySentencia = 
			" INSERT INTO "+getTablaErrores()+" "   +
			"             (ic_epo, cg_receipt, ig_numero_docto, ig_numero_error, cg_error,  CS_CADENA_ORIGINAL ,   " + getCamposDigitoIdentificador() + " ) "+
			"      VALUES ("+ic_epo+", '"+cg_receipt+"', '"+err.getIgNumeroDocto()+"', "+err.getIgNumeroError()+", '"+error200+"'"+",'"+cadena_original+"'"+ ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+")";
		
		return querySentencia;
	}//getInsertaErrores
	
	public String getInsertAcuse(AcuseEnl acu) {
		String querySentencia = 
			" INSERT INTO "+getTablaAcuse()+" ("   +
			"              cc_acuse, ic_epo, cg_receipt, in_total_proc, fn_total_monto_mn,"   +
			"              in_total_acep_mn, in_total_rech_mn, fn_total_monto_dl,"   +
			"              in_total_acep_dl, in_total_rech_dl, df_fechahora_carga, cs_estatus)"   +
			"      VALUES ("   +
			"              '"+acu.getCcAcuse()+"', "+ic_epo+", '"+cg_receipt+"', "+acu.getInTotalProc()+", "+acu.getFnTotalMontoMn()+","   +
			"              "+acu.getInTotalAcepMn()+", "+acu.getInTotalRechMn()+", "+acu.getFnTotalMontoDl()+","   +
			"              "+acu.getInTotalAcepDl()+", "+acu.getInTotalRechDl()+", SYSDATE, "+acu.getCsEstatus()+")"  ;
			
		return querySentencia;
	}//getInsertAcuse

	public String getUpdateAcuse(AcuseEnl acu) {
		String querySentencia = 
			" UPDATE "+getTablaAcuse()+" "   +
			"    SET in_total_proc 		= "+acu.getInTotalProc()+" ,"   +
			"        fn_total_monto_mn 	= "+acu.getFnTotalMontoMn()+" ,"   +
			"        in_total_acep_mn 	= "+acu.getInTotalAcepMn()+" ,"   +
			"        in_total_rech_mn 	= "+acu.getInTotalRechMn()+" ,"   +
			"        fn_total_monto_dl 	= "+acu.getFnTotalMontoDl()+" ,"   +
			"        in_total_acep_dl 	= "+acu.getInTotalAcepDl()+" ,"   +
			"        in_total_rech_dl 	= "+acu.getInTotalRechDl()+" ,"   +
			"        cs_estatus 		= '"+acu.getCsEstatus()+"'"   +
			getCondicionQuery()+
			"    AND cc_acuse = '"+acu.getCcAcuse()+"' "  ;
			
		return querySentencia;
	}//getUpdateAcuse
	
	public String getCondicionQuery() {
		String condicion = 
			"  WHERE ic_epo = "+ic_epo+" "   +
			"    AND cg_receipt = '"+cg_receipt+"' "  ;
		return condicion;
	}//getCondicionQuery
	public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }
  
}//WSEnlace
