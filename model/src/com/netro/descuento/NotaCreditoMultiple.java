package com.netro.descuento;

import java.io.Serializable;

public class NotaCreditoMultiple implements Serializable{

	private static final long serialVersionUID = 10L; 
	
	private String icNotaCredito;
	private String icDocumento;
	private String montoNotaAplicado;
	private String saldoDocto;
	
	public NotaCreditoMultiple( String icNotaCredito,String icDocumento, String montoNotaAplicado, String saldoDocto)
	{
			super();
			this.icNotaCredito		= icNotaCredito;
			this.icDocumento			= icDocumento;
			this.montoNotaAplicado	= montoNotaAplicado;
			this.saldoDocto			= saldoDocto;
	}
	
	public String getIcNotaCredito(){
		return this.icNotaCredito;
	}
	public String getIcDocumento(){
		return this.icDocumento;
	}
	public String getMontoNotaAplicado(){
		return this.montoNotaAplicado;
	}
	public String getSaldoDocto(){
		return this.saldoDocto;
	}
	
}
