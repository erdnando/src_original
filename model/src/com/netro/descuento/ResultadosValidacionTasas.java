package com.netro.descuento;

import javax.ejb.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import java.io.*;

public class ResultadosValidacionTasas implements Serializable{

	private static final long serialVersionUID = 022010L;
	
	private StringBuffer errorGeneral 	= null;
	private int 			icProceso		= -1;
	private StringBuffer errores			= null;
	private boolean		errorLinea		= false;
	private int 			numRegErr   	= 0; 
	private int 			numRegOk			= 0;
	private ArrayList		registros		= new ArrayList();
			
	public void appendErrores(String error){
			if(errorGeneral == null) errorGeneral = new StringBuffer();
			errorGeneral.append(error);
	}
	
	public StringBuffer getErrores(){
		return errorGeneral;
	}
 
	public void setIcProceso(int icProceso){
		this.icProceso = icProceso;
	}
	public int  getIcProceso(){
		return icProceso;
	}
 
	public void setErrorLinea(boolean errorLinea){
		this.errorLinea = errorLinea;
	}
	public boolean getErrorLinea(){
		return this.errorLinea;
	}
 
	public void appendErrores(int numeroLinea,String numeroCampo,String descripcionError){
		ValidacionTasa v = (ValidacionTasa) registros.get(numeroLinea-1);
		v.appendError(numeroCampo,descripcionError);
	}
	
	public void incNumRegErr(){
		this.numRegErr++;
	}
	public void decNumRegErr(){
		this.numRegErr--;
		if(this.numRegErr < 0) this.numRegErr = 0;
	}
	
	public void incNumRegOk(){
		this.numRegOk++;	
	}
	public void decNumRegOk(){
		this.numRegOk--;
		if(this.numRegOk < 0) this.numRegOk = 0;
	}
	
	public void appendErrorRFCRepetido(String rfc,String descripcion){
		if(rfc == null) return;
		for(int i=0;i<registros.size();i++){
			ValidacionTasa v = (ValidacionTasa) registros.get(i);
			if(rfc.equals(v.getRFC())){
				if(!v.hayError()){
					decNumRegOk();
					incNumRegErr();
				}
				v.appendError("1",descripcion);
			}
		}
	}
	
	public void agregaRegistro(int numeroLinea,String rfc){
		registros.add(new ValidacionTasa(rfc,numeroLinea));
	}

	public ArrayList getListaDeRegistrosExitosos(){
		
		ArrayList lista = new ArrayList();
		
		for(int i=0;i<registros.size();i++){
			ValidacionTasa v = (ValidacionTasa) registros.get(i);
			if(!v.hayError()){
				lista.add(String.valueOf(v.getNumeroLinea()));
			}
		}		
		return lista;

	}
	
	public ArrayList getRegistros(){
		return this.registros;	
	}
	
	public int getNumRegErr(){	
		return this.numRegErr;
	}
	
	public int getNumRegOk(){
		return this.numRegOk;	
	}
	
}
