package com.netro.descuento;

/**
 * Representa el Contrato parametrizado por Epo.
 * @since F087-2006 
 * @author Gilberto Aparicio
 */

public class ContratoEpo implements java.io.Serializable {

	public ContratoEpo() {}
	
	/**
	 * Establece la clave de la epo
	 * @param claveEpo Clave de la epo
	 *
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
		contratoArchivo.setClaveEpo(claveEpo);
	}

	/**
	 * Obtiene la clave de la epo
	 * @return Clave de la epo
	 *
	 */
	public String getClaveEpo() {
		return this.claveEpo;
	}

	/**
	 * Establece el consecutivo
	 * @param consecutivo Consecutivo
	 *
	 */
	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
		contratoArchivo.setConsecutivo(consecutivo);
	}

	/**
	 * Obtiene el consecutivo
	 * @return Consecutivo
	 *
	 */
	public String getConsecutivo() {
		return this.consecutivo;
	}

	/**
	 * Establece el titulo del aviso
	 * @param tituloAviso Titulo del aviso
	 *
	 */
	public void setTituloAviso(String tituloAviso) {
		this.tituloAviso = tituloAviso;
	}

	/**
	 * Establece el contenido del aviso
	 * @param contenidoAviso Contenido del aviso
	 *
	 */
	public void setContenidoAviso(String contenidoAviso) {
		this.contenidoAviso = contenidoAviso;
	}
	
	/**
	 * Establece el boton del aviso
	 * @param botonAviso Titulo del boton del aviso
	 *
	 */
	public void setBotonAviso(String botonAviso) {
		this.botonAviso = botonAviso;
	}
	
	/**
	 * Establece el texto de aceptaci�n
	 * @param textoAceptacion Texto que precede al boton de aceptaci�n
	 *
	 */
	public void setTextoAceptacion(String textoAceptacion) {
		this.textoAceptacion = textoAceptacion;
	}
	
	/**
	 * Establece el texto del boton de aceptaci�n
	 * @param botonAceptacion Texto del bot�n de aceptaci�n
	 *
	 */
	public void setBotonAceptacion(String botonAceptacion) {
		this.botonAceptacion = botonAceptacion;
	}

	/**
	 * Establece si el contrato ser� mostrado o no a las pymes de la EPO
	 * @param mostrar S para mostrar el contrato o N para no mostrarlo
	 *
	 */
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}

	/**
	 * Establece la fecha-hora de alta del contrato
	 * @param fechaAlta Fecha de alta del contrato
	 *
	 */
	public void setFechaAlta(java.util.Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


	/**
	 * Obtiene el titulo del aviso
	 * @return Titulo del aviso
	 *
	 */
	public String getTituloAviso() {
		return tituloAviso;
	}

	/**
	 * Obtiene el contenido del aviso
	 * @return Contenido del aviso
	 *
	 */
	public String getContenidoAviso() {
		return this.contenidoAviso;
	}
	
	/**
	 * Obtiene el texto del boton del aviso
	 * @return Titulo del boton del aviso
	 *
	 */
	public String getBotonAviso() {
		return this.botonAviso;
	}
	
	/**
	 * Obtiene el texto de aceptaci�n
	 * @return Texto que precede al boton de aceptaci�n
	 *
	 */
	public String getTextoAceptacion() {
		return this.textoAceptacion;
	}
	
	/**
	 * Obtiene el texto del boton de aceptaci�n
	 * @param botonAceptacion Texto del bot�n de aceptaci�n
	 *
	 */
	public String getBotonAceptacion() {
		return this.botonAceptacion;
	}

	/**
	 * Obtiene si el contrato ser� mostrado o no a las pymes de la EPO
	 * @return S para mostrar el contrato o N para no mostrarlo
	 *
	 */
	public String getMostrar() {
		return this.mostrar;
	}

	/**
	 * Establece la fecha-hora de alta del contrato
	 * @param fechaAlta Fecha de alta del contrato
	 *
	 */
	public java.util.Date getFechaAlta() {
		return this.fechaAlta;
	}


	/**
	 * Establece los datos del archivo del contrato
	 * @param contratoArchivo Objeto de ContratoEpoArchivo
	 *
	 */
	public void setContratoArchivo(ContratoEpoArchivo contratoArchivo) {
		this.contratoArchivo = contratoArchivo;
	}

	/**
	 * Obtiene los datos del archivo del contrato
	 * @return Objeto de ContratoEpoArchivo con la informaci�n del archivo
	 *
	 */
	public ContratoEpoArchivo getContratoArchivo() {
		return this.contratoArchivo;
	}


	
	/**
	 * Establece si es o no, el contrato m�s reciente
	 * @param claveEpo Clave de la epo
	 *
	 */
	public void setUltimo(boolean ultimo) {
		this.ultimo = ultimo;
	}

	/**
	 * Obtiene si es o no, el contrato m�s reciente
	 * @return true si es el m�s reciente o false de lo contrario
	 *
	 */
	public boolean esUltimo() {
		return this.ultimo;
	}


	/**
	 * Establece el nombre de la Epo
	 * @param nombreEpo Nombre de la Epo
	 *
	 */
	public void setNombreEpo(String nombreEpo) {
		this.nombreEpo = nombreEpo;
	}

	/**
	 * Obtiene el nombre de la Epo
	 * @return Nombre de la Epo
	 *
	 */
	public String getNombreEpo() {
		return this.nombreEpo;
	}



	
	public String toString() {
		return 
				"[" +
				"claveEpo=" + this.claveEpo + "," +
				"consecutivo=" + this.consecutivo + "," +
				"tituloAviso=" + this.tituloAviso + "," +
				"contenidoAviso=" + this.contenidoAviso + "," +
				"botonAviso=" +this. botonAviso + "," +
				"textoAceptacion=" + this.textoAceptacion + "," +
				"botonAceptacion=" + this.botonAceptacion + "," +
				"mostrar=" + this.mostrar + "," +
				"fechaAlta=" + this.fechaAlta +
				"contratoArchivo=" + this.contratoArchivo +
				"]";
	}
	
	private String claveEpo;
	private String consecutivo;
	private String tituloAviso;
	private String contenidoAviso;
	private String botonAviso;
	private ContratoEpoArchivo contratoArchivo = new ContratoEpoArchivo();
	private String textoAceptacion;
	private String botonAceptacion;
	private String mostrar;
	private java.util.Date fechaAlta;
	
	private boolean ultimo;
	private String nombreEpo;
	
	//private boolean ;

}// Fin de la Clase