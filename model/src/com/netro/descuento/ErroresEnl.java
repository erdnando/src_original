package com.netro.descuento;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class ErroresEnl {

	private	String	ig_numero_docto 		= null;
	private	String	cg_pyme_epo_interno		= null;
	private String	ig_numero_error			= null;
	private String	cg_error				= null;
	private String	in_a_ejercicio			= null;
	private String	in_sociedad				= null;
	private String	cc_acuse				= null;
	private String	cg_digito_identificador				= null;
	private String cadenaOriginal;
	
	private String icNafinElectronico = null; //2017_003	
	private String numCopade = null; // 2017_003
	private String numContrato = null; // 2017_003
	private String csDsctoEspecial = null; // 2017_003
		
	
	//getters
	public String getIgNumeroDocto() {
		return ig_numero_docto;
	}
	public String getCg_pyme_epo_interno() {
		return cg_pyme_epo_interno;
	}
	public String getIgNumeroError(){
		return ig_numero_error;
	}
	public String getCgError(){
		return cg_error;
	}
	public String getInAEjercicio(){
		return in_a_ejercicio;
	}
	public String getInSociedad(){
		return in_sociedad;
	}
	public String getCcAcuse(){
		return cc_acuse;
	}
	public String getCgDigitoIdentificador(){
		return cg_digito_identificador;
	}
	//setters
	public void setIgNumeroDocto(String ig_numero_docto){
		this.ig_numero_docto = ig_numero_docto;
	}
	public void setCg_pyme_epo_interno(String cg_pyme_epo_interno) {
		this.cg_pyme_epo_interno = cg_pyme_epo_interno;
	}
	public void setIgNumeroError(String ig_numero_error){
		this.ig_numero_error = ig_numero_error;
	}
	public void setCgError(String cg_error){
		this.cg_error = cg_error;
	}
	public void setInAEjercicio(String in_a_ejercicio){
		this.in_a_ejercicio = in_a_ejercicio;
	}
	public void setInSociedad(String in_sociedad){
		this.in_sociedad = in_sociedad;
	}
	public void setCcAcuse(String cc_acuse){
		this.cc_acuse = cc_acuse;
	}
	public void setCgDigitoIdentificador(String cg_digito_identificador){
		this.cg_digito_identificador = cg_digito_identificador;
	}

	public String getCadenaOriginal() {
		return cadenaOriginal;
	}

	public void setCadenaOriginal(String cadenaOriginal) {
		this.cadenaOriginal = cadenaOriginal;
	}
		
	public String getIcNafinElectronico() {
	    return icNafinElectronico;
	}
    
	public void setIcNafinElectronico(String icNafinElectronico) {
	    this.icNafinElectronico = icNafinElectronico;
	}
        
	public String getNumCopade() {
	    return numCopade;
	}
    
	public void setNumCopade(String numCopade) {
	    this.numCopade = numCopade;
	}
    
	public String getNumContrato() {
	    return numContrato;
	}
    
	public void setNumContrato(String numContrato) {
	    this.numContrato = numContrato;
	}
	
		
	public String getCsDsctoEspecial() {
	    return csDsctoEspecial;
	}
    
	public void setCsDsctoEspecial(String csDsctoEspecial) {
	    this.csDsctoEspecial = csDsctoEspecial;
	}
		
	
}
