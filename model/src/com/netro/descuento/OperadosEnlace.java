package com.netro.descuento;

import java.util.ArrayList;

public interface OperadosEnlace extends java.io.Serializable {
	
	String 		getTablaDocumentos();
	String 		getTablaErrores();
	String 		getQuery();
	String 		getClaveDeError();
	ArrayList  	getListaDeVariablesBind();
	
	void	setTablaDocumentos(String	tablaDocumentos);
	void	setTablaErrores(String	tablaErrores);
	void	setQuery(String queryDocumentos);
	void	setClaveDeError(String claveModuloError);
	void	setListaDeVariablesBind(ArrayList listaDeVariablesBind);
	
	void  generaQueryDeDocumentos();
	void	generaQueryDeDocumentosConError();
}//OperadosEnlace
