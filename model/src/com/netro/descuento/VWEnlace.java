package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class VWEnlace implements EpoEnlace,EpoEnlaceArch,Serializable {

	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_VW";	}
	public String getTablaAcuse()		{	return "COM_DOCTOS_PUB_ACU_VW";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_VW";	}

	public String getArchivoAcuse()		{	return "VW-PUB-ACU-";	}
	public String getArchivoErrores()	{	return "VW-PUB-ERR-";	}
	public String getArchivo()			{	return "VW-PUB-";	}
	public String getNombreEnlace()		{	return "VWEnlace";	}

	public String getDocuments(){
		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" , CG_CAMPO2"+
				" , CG_CAMPO3"+
				" , CG_CAMPO4"+
				" , CG_CAMPO5"+
				" ,'' as ic_if"+
				" ,'' as IC_NAFIN_ELECTRONICO"+
				" , DF_FECHA_VENC_PYME "+
				" from " + getTablaDocumentos();
	}

	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+" (IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR) "+
				" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"')";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+
				" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
				" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
				" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
				" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+
				"' where CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}

	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		return " ";
	}

	public String getInsertaDatos(){
		return "INSERT INTO "+getTablaDocumentos()+
				" (CG_PYME_EPO_INTERNO "+
				" , IG_NUMERO_DOCTO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , DF_FECHA_VENC_PYME "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1"+
				" , CG_CAMPO2"+
				" , CG_CAMPO3"+
				" , CG_CAMPO4"+
				" , CG_CAMPO5)"+
				" VALUES (?,?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?,?,?,?)";
	}

	public String getAcusePublicacion(){
		return "SELECT cc_acuse,in_total_proc,fn_total_monto_mn,in_total_acep_mn,in_total_rech_mn"+
				",fn_total_monto_dl,in_total_acep_dl,in_total_rech_dl,to_char(df_fechahora_carga,'dd/mm/yyyy') as fecha"+
				",to_char(df_fechahora_carga,'hh24:mi:ss') as hora,cs_estatus FROM com_doctos_pub_acu_vw";
	}

	public String getErroresPublicacion(){
		return "SELECT ig_numero_docto,ig_numero_error,cg_error FROM com_doctos_err_pub_vw";
	}

	public String getRutaArchivo(){
		return this.rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo  = rutaArchivo;
	}
	public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }
  
	private String rutaArchivo = null;
}