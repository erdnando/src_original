package com.netro.descuento;

import java.io.*;
import java.util.*;
import java.sql.*;
import netropology.utilerias.*;
import com.netro.exception.*;


/**
 * Esta clase representa proporciona mecanismos para manipular
 * el archivo del contrato
 * @author Gilberto Aparicio 22/02/2007
 * @since F087-2006
 *
 */
public class ContratoEpoArchivo implements Serializable {

	public ContratoEpoArchivo(){};


	/**
	 * Establece la clave de la epo
	 * @param claveEpo Clave de la epo
	 *
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	/**
	 * Obtiene la clave de la epo
	 * @return Clave de la epo
	 *
	 */
	public String getClaveEpo() {
		return this.claveEpo;
	}

	/**
	 * Establece el consecutivo
	 * @param consecutivo Consecutivo
	 *
	 */
	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
	}

	/**
	 * Obtiene el consecutivo
	 * @return Consecutivo
	 *
	 */
	public String getConsecutivo() {
		return this.consecutivo;
	}

	/**
	 * Establece la extensi�n del archivo
	 * @param extension Extension del archivo
	 *
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Obtiene la extensi�n del archivo
	 * @return extension
	 *
	 */
	public String getExtension() {
		return this.extension;
	}

	/**
	 * Genera el archivo del contrato a partir del contenido en BD.
	 * El nombre del archivo estar� conformado por la clave de la epo, la
	 * el consecutivo y la extensi�n que corresponda. (Es decir llave primaria + extension)
	 * Por ejemplo 126_3.ppt  que corresponde a la Epo 126, el contrato numero 3
	 * La clave de la epo y el consecutivo deben ser establecidos previamente.
	 *
	 * Adicionalmente establece la propiedad extension, con la extension
	 * correspondiente al archivo guardado en BD
	 *
	 * @param rutaDestino Ruta destino donde se generara el archivo debe terminar con una diagonal /
	 *
	 * @return Cadena con el nombre del archivo creado
	 */

	public String getArchivoContratoEpo(String rutaDestino)
			throws NafinException {

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] arrByte = new byte[4096];
		java.io.OutputStream outstream = null;

		InputStream inStream = null;
		int ic_epo = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null || this.consecutivo == null ||
					rutaDestino == null) {
				throw new Exception("Los parametros o atributos no pueden estar vacios");
			}

			if (!rutaDestino.endsWith("/")){
				throw new Exception("La ruta destino debe terminar en diagonal /");
			}
			ic_epo = Integer.parseInt(this.claveEpo);
			ic_consecutivo = Long.parseLong(this.consecutivo);
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveEpo=" + this.claveEpo + "\n" +
					" consecutivo=" + this.consecutivo + "\n" +
					" rutaDestino=" + rutaDestino);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = "SELECT bi_documento, cg_ext_documento " +
					" FROM com_contrato_epo " +
					" WHERE ic_epo = ? " +
					" AND ic_consecutivo = ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_epo);
			ps.setLong(2, ic_consecutivo);
			rs = ps.executeQuery();

			rs.next();

			this.extension = rs.getString("cg_ext_documento");

			inStream = rs.getBinaryStream("bi_documento");

			String nombreArchivo = claveEpo + "_" + consecutivo + "." + extension;
			String rutaArchivo = rutaDestino + nombreArchivo;

			outstream = new BufferedOutputStream(new FileOutputStream(rutaArchivo));

			int i = 0;
			while((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
				outstream.write(arrByte, 0, i);
			}
			outstream.close();
			inStream.close();

			return nombreArchivo;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Guarda en la BD el archivo contiene el contrato de la EPO especificada
	 * El commit es responsabilidad del que manda a llamar al m�todo
	 * @param rutaArchivoOrigen Ruta fisica del archivo origen contiene el contrato
	 * @param con Conexion de BD
	 */
	public void guardarArchivoContratoEpo(String rutaArchivoOrigen, AccesoDB con) throws NafinException {
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;
		int ic_epo = 0;
		long ic_consecutivo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null || this.consecutivo == null || rutaArchivoOrigen == null) {
				throw new Exception("La clave y la ruta de archivo no estan establecidos");
			}
			ic_epo = Integer.parseInt(this.claveEpo);
			ic_consecutivo = Long.parseLong(this.consecutivo);
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveEpo=" + this.claveEpo + "\n" +
					" consecutivo=" + this.consecutivo + "\n" +
					" rutaArchivoOrigen=" + rutaArchivoOrigen);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {

			this.extension = rutaArchivoOrigen.substring(rutaArchivoOrigen.length()-3).toLowerCase();
			String strSQL = "UPDATE com_contrato_epo " +
					" SET cg_ext_documento = ?, " +
					" bi_documento = ? " +
					" WHERE ic_epo = ? and ic_consecutivo = ? ";

			//La extensi�n debe ser PDF, PPT o DOC por lo cual se consideran los
			//ultimos tres caracteres para la extensi�n
			File archivo = new File(rutaArchivoOrigen);
			FileInputStream fileinputstream = new FileInputStream(archivo);

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, this.extension);
			ps.setBinaryStream(2, fileinputstream, (int)archivo.length());
			ps.setInt(3, ic_epo);
			ps.setLong(4, ic_consecutivo);

			ps.executeUpdate();
			ps.close();
			fileinputstream.close();
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
		}
	}



	public String toString() {
		return "claveEpo=" + this.claveEpo + "," + "\n" +
				"consecutivo=" + this.consecutivo + "," + "\n" +
				"extension=" + this.extension;
	}

	/**
	 * Inserta la aceptacion de contrato, para Epo o para Pyme.
	 * @param tipo {si es EPO o es IF}, la clave Pyme ,la clave EPO o Clave IF, Un consecutivo, el usuario
	 * @return "ok" si el resultado es correcto.
	 */
	public String insertaContrato(String tipo, String ic_pyme, String epo_o_pyme, String consecutivo, String usuario) {
		AccesoDB con = new AccesoDB();
		StringBuffer query;
		boolean bcommit = true;

		try {
			con.conexionDB();
			query = new StringBuffer();
			if (tipo.equals("EPO")){
				query.append("INSERT INTO com_aceptacion_contrato " +
								" ( ic_pyme, ic_epo, ic_consecutivo, ic_usuario, df_aceptacion ) " +
								" VALUES (?,?,?,?, sysdate)");
			}else if (tipo.equals("IF")){
				query.append(" INSERT INTO com_aceptacion_contrato_if " +
								" ( ic_pyme, ic_if, ic_consecutivo, ic_usuario, df_aceptacion ) " +
								" VALUES (?,?,?,?, sysdate) ");
			}
			PreparedStatement ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(ic_pyme));
			ps.setInt(2, Integer.parseInt(epo_o_pyme));
			ps.setLong(3, Long.parseLong(consecutivo));
			ps.setString(4, usuario);
			ps.executeUpdate();
			ps.close();
			return "S";
		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al insertar la aceptacion del contrato", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
	}


	private String claveEpo;
	private String consecutivo;
	private String extension;

}