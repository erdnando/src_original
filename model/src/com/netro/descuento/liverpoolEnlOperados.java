package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class liverpoolEnlOperados implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

	public liverpoolEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_l01";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,5,6,7)"+
		 		 " and ic_epo = "+ic_epo;// F053 - 2010
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9) "+
		" and ic_epo = "+ic_epo;// F053 - 2010
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7) "+
		" and ic_epo = "+ic_epo;// F053 - 2010
	}

	public String getInsertaDoctosOpe(){
		return "insert into "+getTablaDoctosOpe()+" (ic_documento, ic_estatus_docto, "+
					"df_fecha_venc, cg_razon_social_pyme, ig_numero_docto, fn_monto, "+
					"cg_pyme_epo_interno, df_fecha_docto, df_fecha_seleccion, "+
					"ic_nafin_electronico, cg_razon_social_if, in_tasa_aceptada, ic_epo)";
	}


	public String getDoctosOperados(Hashtable alParamEPO){
		return	" select d.ic_documento, 4 as ic_estatus_docto, d.df_fecha_venc, "+
				" SUBSTR (p.cg_razon_social, 0, 70)  as nombrepyme, d.ig_numero_docto, d.fn_monto, "+
				"pe.cg_pyme_epo_interno, d.df_fecha_docto, s.df_fecha_solicitud as df_fecha_seleccion, "+
				"rn.ic_nafin_electronico, i.cg_razon_social as nombreif, "+
				"ROUND(ds.in_tasa_aceptada, 4) in_tasa_aceptada "+
				"   ,"+ic_epo+" "+
				"from com_documento d, comcat_pyme p, comrel_pyme_epo pe, com_solicitud s, "+
				"comrel_nafin rn, com_docto_seleccionado ds, comcat_if i, com_acuse2 a "+
				"where d.ic_epo = "+ic_epo+
				" and d.ic_pyme = pe.ic_pyme "+
				"and d.ic_epo = pe.ic_epo "+
				"and pe.ic_pyme = p.ic_pyme "+
				"and ds.ic_if = rn.ic_epo_pyme_if "+
				"and rn.cg_tipo = 'I' "+
				"and d.ic_if = i.ic_if "+
				"and s.ic_documento = ds.ic_documento "+
				"and ds.ic_documento = d.ic_documento "+
				"and a.cc_acuse = ds.cc_acuse "+
				"and d.ic_estatus_docto in (4,16) "+
				"and s.df_fecha_solicitud >= TRUNC(SYSDATE)  AND s.df_fecha_solicitud < TRUNC(SYSDATE) + 1 ";
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
			return " select d.ic_documento, d.ic_estatus_docto, d.df_fecha_venc, "+
						" SUBSTR (p.cg_razon_social, 0, 70) as nombrepyme, d.ig_numero_docto, d.fn_monto, "+
						"pe.cg_pyme_epo_interno, d.df_fecha_docto, "+
            "null as df_fecha_seleccion, "+
					" null as ic_nafin_electronico,null as cg_razon_social_if,null as in_tasa_aceptada"+
						", "+ic_epo+" "+
						"from com_documento d, comcat_pyme p, comrel_pyme_epo pe, "+
						"comhis_cambio_estatus ce "+
						"where d.ic_epo = "+ic_epo+
						" and d.ic_pyme = pe.ic_pyme "+
						"and d.ic_epo = pe.ic_epo "+
						"and pe.ic_pyme = p.ic_pyme "+
						"and d.ic_estatus_docto in (5,6,7) "+
						"and d.ic_documento = ce.ic_documento "+
						"and ce.dc_fecha_cambio >= TRUNC(SYSDATE)  AND ce.dc_fecha_cambio < TRUNC(SYSDATE) + 1 "+
						"and ce.IC_CAMBIO_ESTATUS IN (4,5,6) ";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
    return "";
	}


}
