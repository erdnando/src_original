package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class VWEnlOperados implements EpoEnlOperados,EpoEnlOperadosArch,Serializable{

	public VWEnlOperados(){
	}

	public VWEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_vw";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe();
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9)";
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7)";
	}

	public String getInsertaDoctosOpe(){
		return
				"insert into "+getTablaDoctosOpe()+" (cg_razon_social_if,df_fechahora_aut,cc_acuse,"+
				"in_numero_proveedor,cg_razon_social_pyme,ig_numero_docto,df_fecha_docto,df_fecha_venc,"+
				"df_fecha_venc_prov,cg_nombre_moneda,fn_monto,cg_sucursal,cg_banco,cg_cuenta_bco,fn_porc_dscto,"+
				"fn_monto_dscto,in_tasa_aceptada,ct_referencia,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5)";
	}

	public String getDoctosOperados(Hashtable alParamEPO){
		return	" SELECT   /*+ use_nl(d ds s pe p a3 ie i i2 m)  */ "+
				" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as ifinanciero"+
				", to_char(A3.df_fecha_hora, 'DD/MM/YYYY') fecha_not "+
				", A3.cc_acuse as acuse_not"+
				", PE.cg_pyme_epo_interno as no_proveedor "+
				", (decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombre_proveedor"+
				", D.ig_numero_docto as no_docto "+
				", to_char(D.df_fecha_docto, 'DD/MM/YYYY') fecha_emision "+
				", to_char(D.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento "+
				", to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY')  df_fecha_venc_pyme "+
				", M.cd_nombre as moneda "+
				", D.fn_monto as monto "+
				", IE.ig_sucursal as sucursal "+
				", IE.cg_banco as banco "+
				", IE.cg_num_cuenta as no_cuenta "+
				", D.fn_porc_anticipo as porc_dscto "+
				", D.fn_monto_dscto as monto_desc "+
				", DS.in_tasa_aceptada as tasa_desc "+
				", D.CT_REFERENCIA as referencia "+
				", D.cg_campo1 as campo1 "+
				", D.cg_campo2 as campo2 "+
				", D.cg_campo3 as campo3 "+
				", D.cg_campo4 as campo4 "+
				", D.cg_campo5 as campo5 "+
				"FROM com_documento d, com_docto_seleccionado ds, com_solicitud s "+
				", comrel_pyme_epo pe, comcat_pyme p, com_acuse3 a3, comrel_if_epo ie "+
				", comcat_if i, comcat_if i2, comcat_moneda m "+
				"WHERE d.ic_documento = ds.ic_documento "+
				"AND ds.ic_documento = s.ic_documento "+
				"AND s.cc_acuse = a3.cc_acuse    AND ds.ic_if = ie.ic_if "+
				"AND ie.ic_if = i.ic_if    AND ie.ic_epo = pe.ic_epo "+
				"AND p.ic_pyme = pe.ic_pyme    AND d.ic_pyme = p.ic_pyme "+
				"AND d.ic_beneficiario = i2.ic_if (+)    AND d.ic_moneda = m.ic_moneda "+
				"AND ie.cs_vobo_nafin = 'S' "+
				"AND a3.df_fecha_hora >= TRUNC(SYSDATE) "+
				"AND a3.df_fecha_hora < TRUNC(SYSDATE+1) "+
				"AND ds.ic_epo= " + ic_epo + " AND d.ic_epo = " + ic_epo + " AND ie.ic_epo = " + ic_epo + " AND pe.ic_epo = " + ic_epo;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
		return " ";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
		return " ";
	}

	public String getNombreArchivo(){
		return "VW-Oper-";
	}

	public String getNombreEnlace(){
		return "VWEnlOperados";
	}

	public String getRutaArchivo(){
		return this.rutaArchivo;
	}
	
	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo  = rutaArchivo;
	}
	

	public void setEpo(String epo) {
		this.ic_epo  = epo;
	}
	
	private String rutaArchivo = null;
	private String ic_epo = "";
	
}