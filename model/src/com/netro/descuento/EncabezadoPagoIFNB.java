package com.netro.descuento;

import java.io.Serializable;

public class EncabezadoPagoIFNB implements Serializable {
	
	public EncabezadoPagoIFNB(){}
	
	/**
	 * Establece el numero de linea 
	 * @param numeroDeLinea N�mero de linea en el que se encontro 
	 * 		el encabezado dentro del archivo de carga masiva.
	 */
	public void setNumeroDeLinea(int numeroDeLinea){
		this.numeroDeLinea = numeroDeLinea;
	}

	/**
	 * Establece el nombre de archivo
	 * @param strNombreArchivo Nombre del archivo de carga masiva 
	 * donde se encontro el encabezado.
	 */
	public void setNombreArchivo(String strNombreArchivo){
		this.nombreArchivo = strNombreArchivo;
	}
	
	/**
	 * Establece la clave de Intermediario Financiero
	 * @param strClaveIF clave del IF
	 */
	public void setClaveIF(String strClaveIF) {
		claveIF = strClaveIF;
	}
	
	/**
	 * Establece la clave de la Direccion Estatal
	 * @param strClaveDirEstatal Clave de la direccion estatal
	 */
	public void setClaveDirEstatal(String strClaveDirEstatal) {
		claveDirEstatal = strClaveDirEstatal;
	}
	
	/**
	 * Establece la clave de la Moneda
	 * @param strClaveMoneda clave de la moneda
	 */
	public void setClaveMoneda(String strClaveMoneda) {
		claveMoneda = strClaveMoneda;
	}
	
	/**
	 * Establece el banco de servicio
	 * @param strBancoServicio Banco de servicio
	 */
	public void setBancoServicio(String strBancoServicio) {
		bancoServicio = strBancoServicio;
	}
	
	/**
	 * Establece la fecha de vencimiento
	 * @param strFechaVencimiento fecha de vencimiento
	 */
	public void setFechaVencimiento(String strFechaVencimiento) {
		fechaVencimiento = strFechaVencimiento;
	}
	
	/**
	 * Establece la Fecha probable de pago
	 * @param strFechaProbablePago Fecha probable de pago
	 */
	public void setFechaProbablePago(String strFechaProbablePago) {
		fechaProbablePago = strFechaProbablePago;
	}
	
	/**
	 * Establece la Fecha de deposito
	 * @param strFechaDeposito Fecha de deposito
	 */
	public void setFechaDeposito(String strFechaDeposito) {
		fechaDeposito = strFechaDeposito;
	}
	
	/**
	 * Establece el importe de deposito
	 * @param strImporteDeposito Importe de deposito
	 */
	public void setImporteDeposito(String strImporteDeposito) {
		importeDeposito = strImporteDeposito;
	}
	
	/**
	 * Establece la referencia de Banco
	 * @param strReferenciaBanco Referencia de Banco
	 */
	public void setReferenciaBanco(String strReferenciaBanco) {
		referenciaBanco = strReferenciaBanco;
	}

	/**
	 * Establece la referencia del Intermediario
	 * @param strReferenciaInter Referencia del Intermediario
	 */
	public void setReferenciaInter(String strReferenciaInter) {
		referenciaInter = strReferenciaInter;
	}
	
	/**
	 * Establece el acuse
	 * @param strAcuse Acuse
	 */
	public void setAcuse(String strAcuse) {
		acuse = strAcuse;
	}
	
	/**
	 * Obtiene el numero de linea
	 * @return 	 N�mero de linea en el que se encontro 
	 * 		el encabezado dentro del archivo de carga masiva.
	 */
	public int getNumeroDeLinea() {
		return numeroDeLinea;
	}

	/**
	 * Obtiene el nombre de archivo
	 * @return Nombre del archivo de carga masiva 
	 * 		donde se encontro el encabezado.
	 */
	public String getNombreArchivo(){
		return nombreArchivo;
	}

	/**
	 * Obtiene la clave de Intermediario Financiero
	 * @return clave del IF
	 */
	public String getClaveIF() {
		return claveIF;
	}
	
	/**
	 * Obtiene la clave de la Direccion Estatal
	 * @return Clave de la direccion estatal
	 */
	public String getClaveDirEstatal() {
		return claveDirEstatal;
	}

	/**
	 * Obtiene la clave de la Moneda
	 * @return Clave de la moneda
	 */
	public String getClaveMoneda() {
		return claveMoneda;
	}
	
	/**
	 * Obtiene el banco de servicio
	 * @return Banco de servicio
	 */
	public String getBancoServicio() {
		return bancoServicio;
	}
	
	/**
	 * Obtiene la fecha de vencimiento
	 * @return Fecha de vencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	
	/**
	 * Obtiene la Fecha probable de pago
	 * @return Fecha probable de pago
	 */	public String getFechaProbablePago() {
		return fechaProbablePago;
	}
	
	/**
	 * Obtiene la Fecha de deposito
	 * @return Fecha de deposito
	 */
	public String getFechaDeposito() {
		return fechaDeposito;
	}
	
	/**
	 * Obtiene el importe de deposito
	 * @return strImporteDeposito Importe de deposito
	 */
	public String getImporteDeposito() {
		return importeDeposito;
	}
	
	/**
	 * Obtiene la referencia de Banco
	 * @return Referencia de Banco
	 */
	public String getReferenciaBanco() {
		return referenciaBanco;
	}

	/**
	 * Obtiene la referencia del Intermediario
	 * @return Referencia del Intermediario
	 */
	public String getReferenciaInter() {
		return referenciaInter;
	}
	
	/**
	 * Obtiene el Acuse
	 * @return Acuse
	 */
	public String getAcuse() {
		return acuse;
	}

	
	
	/**
	 * Obtiene el query de insercion para el encabezado
	 * @param strClaveEncabezado Clave del encabezado (PK)
	 * @return Cadena con la instruccion de SQL para insertar
	 * 		el registro.
	 */
	 
	public String getSQLInsert(String strClaveEncabezado) {
		
		StringBuffer campos = new StringBuffer();
		campos.append("ic_encabezado_pago");
		
		StringBuffer valoresCampos = new StringBuffer();
		valoresCampos.append(strClaveEncabezado);
		
		
		campos.append(",df_registro");
		valoresCampos.append(",trunc(sysdate)");
		
		campos.append(",cc_acuse");
		valoresCampos.append(",'" + this.acuse + "'");
		
		if (claveIF != null) {
			campos.append(",ic_if");
			valoresCampos.append("," + this.claveIF);
		}
		if (claveDirEstatal != null) {
			campos.append(",ig_sucursal");
			valoresCampos.append("," + this.claveDirEstatal);
		}
		if (claveMoneda != null) {
			campos.append(",ic_moneda");
			valoresCampos.append("," + this.claveMoneda);
		}
		if (bancoServicio != null) {
			campos.append(",ic_financiera");
			valoresCampos.append("," + this.bancoServicio);
		}

		if (fechaVencimiento != null) {
			campos.append(",df_periodo_fin");
			valoresCampos.append(
					",TO_DATE('" + this.fechaVencimiento + "', 'DD/MM/YYYY')");
		}
		if (fechaProbablePago != null) {
			campos.append(",df_probable_pago");
			valoresCampos.append(
					",TO_DATE('" + this.fechaProbablePago + "', 'DD/MM/YYYY')");
		}
		if (fechaDeposito != null) {
			campos.append(",df_deposito");
			valoresCampos.append(
					",TO_DATE('"  + this.fechaDeposito + "', 'DD/MM/YYYY')");
		}
		if (importeDeposito != null) {
			campos.append(",ig_importe_deposito");
			valoresCampos.append("," + this.importeDeposito);
		}
		if (referenciaBanco != null) {
			campos.append(",cg_referencia_banco");
			valoresCampos.append(",'" + this.referenciaBanco + "'");
		}
		if (referenciaInter != null) {
			campos.append(",ic_referencia_inter");
			valoresCampos.append("," + this.referenciaInter);
		}
		if (nombreArchivo != null) {
			campos.append(",cg_nombre_archivo");
			valoresCampos.append(",'" + this.nombreArchivo + "'");
		}
		
		String strSQL = 
				"INSERT INTO com_encabezado_pago (" + campos + ")" +
				" VALUES (" + valoresCampos + ")";
		
		return strSQL;		
	}

	private String claveIF;
	private String claveDirEstatal;
	private String claveMoneda;
	private String bancoServicio;
	private String fechaVencimiento;
	private String fechaProbablePago;
	private String fechaDeposito;
	private String importeDeposito;
	private String referenciaBanco;
	private String referenciaInter;
	
	private String acuse;
	
	private String nombreArchivo;
	private int numeroDeLinea;

}
