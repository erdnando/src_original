package com.netro.descuento;

import com.nafin.ws.CargaDocumentoWSInfo;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.CRutaFisicaWS;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.CrearArchivosAcuses;
import netropology.utilerias.Fecha;
import netropology.utilerias.MensajeParam;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "CargaDocumentoEJB" , mappedName = "CargaDocumentoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CargaDocumentoBean implements CargaDocumento {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CargaDocumentoBean.class);

	AccesoDB conn = new AccesoDB();

	public void abreConexionDB() throws NafinException{
		try{
			log.info("CargaDocumentoBean::abreConexionDB(E) ");
			conn.conexionDB();
		}catch(Exception e){
			log.error("CargaDocumentoBean::ejbCreate(Error) "+e.toString());
			throw new NafinException("SIST0001");
		} finally {
			log.info("CargaDocumentoBean::abreConexionDB(S) ");
		}
	}

	public void cierraConexionDB(){
		log.info("CargaDocumentoBean::cierraConexionDB(E) ");
		if(conn.hayConexionAbierta()) conn.cierraConexionDB();
		log.info("CargaDocumentoBean::cierraConexionDB(S) ");
	}

	/* Informa si la epo tiene Campos Detalle definidos. */
	public int camposDetalleHabilitados(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	int iDefinioDetalles=0;
		try{
			con.conexionDB();
			String query = "select count(*) from comrel_visor_detalle "+
							" where ic_epo="+sNoCliente+
							" and ic_no_campo between 1 and 10 "+
							" and ic_producto_nafin=1";
			try{
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					iDefinioDetalles = (rs.getInt(1)>0)?rs.getInt(1):1;
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0035"); }
			con.cierraStatement();

		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en camposDetalleHabilitados. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return iDefinioDetalles;
	}

	/* Verifica si la epo acepta "Factoraje Vencido" */
	public boolean autorizadaFactorajeVencido(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOperaFactorajeVencido=false;
		try{
			con.conexionDB();
			String query =
				" SELECT COUNT (*)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_vencido = 'S'"   +
				"    AND ic_epo = "+sNoCliente;
			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bOperaFactorajeVencido=(rs.getInt(1)>0)?true:false;
				else
					bOperaFactorajeVencido=false;
			} catch(SQLException sqle) { throw new NafinException("DSCT0037"); }
			con.cierraStatement();

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en autorizadaFactorajeVencido. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bOperaFactorajeVencido;
	}

	/* Verifica si la epo acepta "Factoraje Vencido" */
	private boolean autorizadaFactorajeVencido(String sNoCliente,AccesoDB con) throws NafinException {
	boolean bOperaFactorajeVencido=false;
		try{
			String query =
				" SELECT COUNT (*)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_vencido = 'S'"   +
				"    AND ic_epo = "+sNoCliente;

			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bOperaFactorajeVencido=(rs.getInt(1)>0)?true:false;
				else
					bOperaFactorajeVencido=false;
			} catch(SQLException sqle) { throw new NafinException("DSCT0037"); }
			con.cierraStatement();

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en autorizadaFactorajeVencido. "+e);
			throw new NafinException("SIST0001");
		}
	return bOperaFactorajeVencido;
	}

	/* Verifica si la epo acepta "Factoraje Distribuido" */
	public boolean autorizadaFactorajeDistribuido(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOperaFactorajeDistribuido=false;
		try{
			con.conexionDB();
			String query =
				" SELECT COUNT (*)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_distribuido = 'S'"   +
				"    AND ic_epo = "+sNoCliente;
			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bOperaFactorajeDistribuido=(rs.getInt(1)>0)?true:false;
				else
					bOperaFactorajeDistribuido=false;
			} catch(SQLException sqle) { throw new NafinException("DSCT0050"); }
			con.cierraStatement();

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en autorizadaFactorajeDistribuido. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bOperaFactorajeDistribuido;
	}

	/* Verifica si la epo acepta "Notas de Credito" */
	public boolean autorizadaNotasCredito(String ic_epo) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean bOperaNotasCredito=false;
		try{
			con.conexionDB();
			String qrySentencia =
				" SELECT cs_opera_notas_cred"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_producto_nafin = 1"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ResultSet rs = ps.executeQuery();
			ps.clearParameters();
			String cs_opera_notas_cred = "";
			if(rs.next())
				cs_opera_notas_cred	= rs.getString(1)==null?"":rs.getString(1);
			rs.close();
			if(ps != null) ps.close();
			if("S".equals(cs_opera_notas_cred))
				bOperaNotasCredito = true;
		} catch(Exception e) {
			log.error("Exception en autorizadaNotasCredito. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return bOperaNotasCredito;
	}

	/* Verifica si la epo acepta "Factoraje Distribuido" */
	public boolean autorizadaFactorajeDistribuido(String sNoCliente,AccesoDB con) throws NafinException {
	boolean bOperaFactorajeDistribuido=false;
		try{
			String query =
				" SELECT COUNT (*)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_distribuido = 'S'"   +
				"    AND ic_epo = "+sNoCliente;
			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bOperaFactorajeDistribuido=(rs.getInt(1)>0)?true:false;
				else
					bOperaFactorajeDistribuido=false;
			} catch(SQLException sqle) { throw new NafinException("DSCT0050"); }
			con.cierraStatement();

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en autorizadaFactorajeDistribuido. "+e);
			throw new NafinException("SIST0001");
		}
	return bOperaFactorajeDistribuido;
	}


	/* Obtenemos el n�mero maximo de proceso del docto.	*/
	/*Modificado por EGB para utilizaci�n de la secuencia de ORACLE */
	public String getNumaxProcesoDocto() throws NafinException {
	AccesoDB con = new AccesoDB();
	String iProcesoDocto=null;
		try{
			con.conexionDB();
			//String query = "select (NVL(max(ic_proc_docto),0)+1) from comtmp_proc_docto";
			String query = "select SEQ_COMTMP_PROC_IC_PROC_D.NEXTVAL from dual";

			try{
				ResultSet rs = con.queryDB(query);
				if(rs.next())
					iProcesoDocto = rs.getString(1);
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0036"); }
			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getNumaxProcesoDocto. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return iProcesoDocto;
	}


	/**
	 * Obtiene los documentos que fueron dados de baja en la fecha especificada. Si esta no es especificada
	 * obtiene los de d�a actual.
	 * @param claveEPO Clave de la EPO
	 * @param usuario Login del usuario
	 * @param password Password del usuario
	 * @param fechaInicio Fecha inicial del rango (formato dd/mm/yyyy)
	 * @param fechaFin Fecha final del rango (formato dd/mm/yyyy)
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 * si no hay informaci�n que concuerde con los par�metros epecificados
	 * o "ERROR" cuando los par�metros no son v�lidos u ocurrio un error.
	 */
	public String getDocumentosBajaWS(String claveEPO, String usuario, String password,
			String fechaInicio, String fechaFin) {
		StringBuffer sbArchivo = new StringBuffer();
		String tituloMensaje = "Ejecucion getDocumentosBajaWS";

		String sPubEPOPEFFlagVal						= null;
		String sDigitoIdentificadorFlag    = null;
		EpoPEF epoPEF = new EpoPEF();
		boolean bOperaFactorajeConMandato = false;

		AccesoDB con = new AccesoDB();

		//**********************************Validaci�n de parametros:*****************************
		try {

			if (fechaInicio != null && !fechaInicio.equals("")) {
				Comunes.parseDate(fechaInicio);
			}
			fechaInicio = (fechaInicio == null || fechaInicio.equals(""))?"":fechaInicio;

			if (fechaFin != null && !fechaFin.equals("")) {
				Comunes.parseDate(fechaFin);
			}
			fechaFin = (fechaFin == null || fechaFin.equals(""))?"":fechaFin;

			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

			log.info("getDocumentosSinOperarWS::Validando claveEPO/usuario");

			if (!cuentasEPO.contains(usuario)) {
				throw new Exception("El usuario no es de la epo especificada");
			}

			log.info("getDocumentosSinOperarWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(usuario, password)) {
				throw new Exception("Fall� la autenticaci�n del usuario");
			}

		} catch(Exception e) {
			String error =
					"ERROR. Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"claveEPO=" + claveEPO + "\n" +
					"usuario=" + usuario + "\n" +
					"password=********" + "\n" +
					"fechaInicio=" + fechaInicio + "\n" +
					"fechaFin=" + fechaFin + "\n";

			log.error(error);
			try { //Si el env�o del correo falla se ignora.
				String to = this.getCuentasWS(claveEPO);
				if (to != null) {
					String from = this.getCuentaWSFrom();
					Correo.enviarMensajeTexto(to, from, tituloMensaje, error);
				}
			} catch(Throwable t) {
				log.error("Error al enviar correo " + t.getMessage());
			}


			return error;
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
				Hashtable alParamEPO = BeanParamDscto.getParametrosEPO(claveEPO, 1);
				sDigitoIdentificadorFlag    = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
				sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
				bOperaFactorajeConMandato = operaFactorajeConMandato(claveEPO); // Fodea 046 - 2009
			} catch (Throwable ne){
				throw new Exception("Error en obtencion de parametros ", ne);
			}

			StringBuffer strCampos = new StringBuffer();
			StringBuffer strNombreCampos = new StringBuffer();
			int numCampos = 0;
			String strSQL =
					" SELECT ic_no_campo, " +
					" 	INITCAP(cg_nombre_campo) AS cg_nombre_campo " +
					" FROM comrel_visor " +
					" WHERE ic_epo = ? AND ic_producto_nafin = 1 " +
					" 	AND ic_no_campo BETWEEN 1 AND 5 " +
					" ORDER BY ic_no_campo ";
			ps  = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEPO));
			rs = ps.executeQuery();
			while(rs.next()) {
				numCampos++;
				strCampos.append(",D.CG_CAMPO" + rs.getString("IC_NO_CAMPO"));
				strNombreCampos.append(" ," + rs.getString("CG_NOMBRE_CAMPO"));
			}
			rs.close();
			ps.close();

			//---------------------- Query de doctos dados de baja -------------------


			StringBuffer strSQLDoctos = new StringBuffer();

			strSQLDoctos.append(
					" SELECT d.ic_estatus_docto " +
					" 	, pe.cg_pyme_epo_interno " +
					" 	, d.ig_numero_docto " +
					" 	, TO_CHAR(d.DF_FECHA_DOCTO, 'dd/mm/yyyy') as df_fecha_docto " +
					" 	, TO_CHAR(d.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc " +
					" 	, d.ic_moneda " +
					" 	, d.fn_monto " +
					" 	, d.cs_dscto_especial " +
					" 	, d.ct_referencia " +
					strCampos +
          ("S".equals(sDigitoIdentificadorFlag)?","+epoPEF.getCamposDigitoIdentificadorCalc():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getCamposFormateadosPEF():"")+
			 		(bOperaFactorajeConMandato?", i.IC_NAFIN_ELECTRONICO     AS clave_if "			:"")+
					(bOperaFactorajeConMandato?", mndte.IC_NAFIN_ELECTRONICO AS clave_mandante "	:"")+
					" FROM com_documento d " +
					" 	, comhis_cambio_estatus ce " +
					" 	, comrel_pyme_epo pe  " +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'I' ) i "		:"") +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'M' ) mndte "	:"") +
					" WHERE d.ic_documento = ce.ic_documento  " +
					" 	 AND d.ic_pyme = pe.ic_pyme  " +
					" 	 AND d.ic_epo = pe.ic_epo  " +
					" 	 AND ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') " +
					" 	 AND ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 " +
					" 	 AND d.ic_estatus_docto IN (5)  " +	//5=Baja
					" 	 AND ce.ic_cambio_estatus IN (4)  " +  //4=De Negociable a Baja
					" 	 AND d.ic_epo = ?  " +
					(bOperaFactorajeConMandato?"   AND d.ic_if       = i.IC_EPO_PYME_IF(+)     ":"")+
					(bOperaFactorajeConMandato?"   AND d.ic_mandante = mndte.ic_epo_pyme_if(+) ":""));
			log.debug("getDocumentosBajaWS::" + strSQLDoctos);

			ps = con.queryPrecompilado(strSQLDoctos.toString());

			ps.setString(1, ( (fechaInicio.equals(""))?Fecha.getFechaActual():fechaInicio ) );
			ps.setString(2, ( (fechaFin.equals(""))?Fecha.getFechaActual():fechaFin ) );
			ps.setInt(3, Integer.parseInt(claveEPO));

			sbArchivo.append("Clave Estatus," +
					"Num. proveedor," +
					"Num. Documento," +
					"Fecha de  Emision," +
					"Fecha de Vencimiento," +
					"Clave de moneda," +
					"Monto," +
					"Tipo Factoraje," +
					"Referencia" +
					strNombreCampos +
          ("S".equals(sDigitoIdentificadorFlag)?epoPEF.getDescDigitoIdentificador():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getDescCamposPEF():"")+
			 		(bOperaFactorajeConMandato?",Clave del Intermediario Financiero":"")+
					(bOperaFactorajeConMandato?",Clave Mandante":"")+
					"\n");

			rs = ps.executeQuery();
			while(rs.next()) {


				String estatusDocto = (rs.getString("ic_estatus_docto") == null)?"":rs.getString("ic_estatus_docto");
				String numProveedor = (rs.getString("cg_pyme_epo_interno") == null)?"":rs.getString("cg_pyme_epo_interno");
				String numDocto = (rs.getString("ig_numero_docto") == null)?"":rs.getString("ig_numero_docto");
				String fechaDocto = (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO");
				String fechaVenc = (rs.getString("df_fecha_venc") == null)?"":rs.getString("df_fecha_venc");
				String moneda = (rs.getString("ic_moneda") == null)?"":rs.getString("ic_moneda");
				String monto = (rs.getString("fn_monto") == null)?"":rs.getString("fn_monto");
				String tipoFactoraje = (rs.getString("cs_dscto_especial") == null)?"":rs.getString("cs_dscto_especial");
				String referencia = (rs.getString("ct_referencia") == null)?"":rs.getString("ct_referencia");
        String digitoIdentificador = ("S".equals(sDigitoIdentificadorFlag)?rs.getString("CG_DIGITO_IDENTIFICADOR"):"");
        String fechaEntrega = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("DF_ENTREGA"):"");
        fechaEntrega = (fechaEntrega==null)?"":fechaEntrega;
        String tipoCompra = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_TIPO_COMPRA"):"");
        tipoCompra = (tipoCompra==null)?"":tipoCompra;
        String clavePresupuestaria = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_CLAVE_PRESUPUESTARIA"):"");
        clavePresupuestaria = (clavePresupuestaria==null)?"":clavePresupuestaria;
        String periodo = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_PERIODO"):"");
        periodo = (periodo==null)?"":periodo;


				StringBuffer sbValorCampos = new StringBuffer();
				for (int j = 1; j <= numCampos; j++) {
					String valor = (rs.getString("cg_campo" + j) == null)?
							"":rs.getString("cg_campo" + j).replace(',',' ');
					sbValorCampos.append("," + valor);
				}

				String claveIntermediarioFinanciero	= (bOperaFactorajeConMandato && (rs.getString("clave_if") 		!= null))?rs.getString("clave_if")			:"";
				String claveMandante						= (bOperaFactorajeConMandato && (rs.getString("clave_mandante")!= null))?rs.getString("clave_mandante")	:"";

				sbArchivo.append(
						estatusDocto + "," + numProveedor + "," +
						numDocto + "," + fechaDocto + "," +
						fechaVenc + "," + moneda + "," +
						monto + "," + tipoFactoraje + "," +
						referencia.replace(',',' ') + sbValorCampos +
            ("S".equals(sDigitoIdentificadorFlag)?","+digitoIdentificador:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+fechaEntrega:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+tipoCompra:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+clavePresupuestaria:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+periodo:"")+
				(bOperaFactorajeConMandato?","+claveIntermediarioFinanciero:"")+
				(bOperaFactorajeConMandato?","+claveMandante:"")+
            "\n" );

			} //fin while
			rs.close();
			ps.close();


		} catch (Exception e){
			sbArchivo.delete(0,sbArchivo.length());
			sbArchivo.append("ERROR. " + e.getMessage());
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		try { //Si el env�o del correo falla se ignora.
			String to = this.getCuentasWS(claveEPO);
			if (to != null) {
				String from = this.getCuentaWSFrom();
				Correo.enviarMensajeTexto(to, from, tituloMensaje, sbArchivo.toString());
			}
		} catch(Throwable t) {
			log.error("Error al enviar correo " + t.getMessage());
		}

		return sbArchivo.toString();

	}



	/**
	 * Obtiene un listado de los documentos sin operar
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 *
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 * si no hay informaci�n
	 * o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 *
	 */
	public String getDocumentosSinOperarWS(
			String claveEPO, String usuario, String password) {

		StringBuffer sbArchivo = new StringBuffer();
		String tituloMensaje = "Ejecucion getDocumentosSinOperarWS";
    String sPubEPOPEFFlagVal						= null;
    String sDigitoIdentificadorFlag    = null;
    EpoPEF epoPEF = new EpoPEF();
	 boolean bOperaFactorajeConMandato = false;

		AccesoDB con = new AccesoDB();

		//**********************************Validaci�n de parametros:*****************************
		try {
			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

			log.info("getDocumentosSinOperarWS::Validando claveEPO/usuario");

			if (!cuentasEPO.contains(usuario)) {
				throw new Exception("El usuario no es de la epo especificada");
			}

			log.info("getDocumentosSinOperarWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(usuario, password)) {
				throw new Exception("Fall� la autenticaci�n del usuario");
			}

		} catch(Exception e) {
			String error =
					"Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"claveEPO=" + claveEPO + "\n" +
					"usuario=" + usuario + "\n" +
					"password=********" + "\n";

			log.error(error);
			try { //Si el env�o del correo falla se ignora.
				String to = this.getCuentasWS(claveEPO);
				if (to != null) {
					String from = this.getCuentaWSFrom();
					Correo.enviarMensajeTexto(to, from, tituloMensaje, "("+claveEPO+" - "+usuario+")\n"+error);
				}
			} catch(Throwable t) {
				log.error("Error al enviar correo " + t.getMessage());
			}


			return error;
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;

      /*MODIFICACIONES PARA DETERMINAR SI SE TRATA DE UNA EPO PEF Y LOS PARAMETROS ACTIVOS*/

      try {
        ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
        Hashtable alParamEPO = BeanParamDscto.getParametrosEPO(claveEPO, 1);
        sDigitoIdentificadorFlag    = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
        sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
		  bOperaFactorajeConMandato = operaFactorajeConMandato(claveEPO); // Fodea 046 - 2009
      } catch (NafinException ne){
        ne.printStackTrace();
        throw new Exception("Error en obtencion de parametros ");
      }

			StringBuffer strCampos = new StringBuffer();
			StringBuffer strNombreCampos = new StringBuffer();
			int numCampos = 0;
			String strSQL =
					" SELECT ic_no_campo, " +
					" 	INITCAP(cg_nombre_campo) AS cg_nombre_campo " +
					" FROM comrel_visor " +
					" WHERE ic_epo = ? AND ic_producto_nafin = 1 " +
					" 	AND ic_no_campo BETWEEN 1 AND 5 " +
					" ORDER BY ic_no_campo ";
			ps  = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEPO));
			rs = ps.executeQuery();
			while(rs.next()) {
				numCampos++;
				strCampos.append(",D.CG_CAMPO" + rs.getString("IC_NO_CAMPO"));
				strNombreCampos.append(" ," + rs.getString("CG_NOMBRE_CAMPO"));
			}
			rs.close();
			ps.close();

			String strSQLDias =
					" SELECT NVL(pe.ig_dias_minimo,p.in_dias_minimo) as diasMinimos " +
					" FROM comrel_producto_epo pe, comcat_producto_nafin p " +
					" WHERE pe.ic_producto_nafin(+) = p.ic_producto_nafin " +
					" 	AND pe.ic_producto_nafin  = 1 " +
					" 	AND pe.ic_epo(+) = ? ";
			ps  = con.queryPrecompilado(strSQLDias);
			ps.setInt(1, Integer.parseInt(claveEPO));
			rs = ps.executeQuery();
			rs.next();
			String diasMinimos = rs.getString("diasMinimos");
			rs.close();
			ps.close();


			//---------------------- Query de doctos sin operar -------------------
			Calendar cal = new GregorianCalendar();


			StringBuffer strSQLDoctos = new StringBuffer();

			strSQLDoctos.append(
					" SELECT d.ic_estatus_docto " +
					" 	, pe.cg_pyme_epo_interno " +
					" 	, d.ig_numero_docto " +
					" 	, TO_CHAR(d.DF_FECHA_DOCTO, 'dd/mm/yyyy') as df_fecha_docto " +
					" 	, TO_CHAR(d.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc " +
					" 	, d.ic_moneda " +
					" 	, d.fn_monto " +
					" 	, d.cs_dscto_especial " +
					" 	, d.ct_referencia " +
					strCampos +
          ("S".equals(sDigitoIdentificadorFlag)?","+epoPEF.getCamposDigitoIdentificadorCalc():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getCamposFormateadosPEF():"")+
			 		(bOperaFactorajeConMandato?", i.IC_NAFIN_ELECTRONICO     AS clave_if "			:"")+
					(bOperaFactorajeConMandato?", mndte.IC_NAFIN_ELECTRONICO AS clave_mandante "	:"")+
					" FROM com_documento d " +
					" 	, comhis_cambio_estatus ce " +
					" 	, comrel_pyme_epo pe  " +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'I' ) i "		:"") +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'M' ) mndte "	:"") +
					" WHERE d.ic_documento = ce.ic_documento  " +
					" 	 AND d.ic_pyme = pe.ic_pyme  " +
					" 	 AND d.ic_epo = pe.ic_epo  " +
					" 	 AND ce.dc_fecha_cambio >= TRUNC(SYSDATE) " +
					" 	 AND ce.dc_fecha_cambio < TRUNC(SYSDATE + 1) " +
					" 	 AND d.ic_estatus_docto IN (5, 6, 7)  " +
					" 	 AND ce.ic_cambio_estatus IN (4, 5, 6)  " +
					" 	 AND d.ic_epo = ?  " +
					(bOperaFactorajeConMandato?"   AND d.ic_if       = i.IC_EPO_PYME_IF(+)     ":"")+
					(bOperaFactorajeConMandato?"   AND d.ic_mandante = mndte.ic_epo_pyme_if(+) ":"")+
					" UNION ALL " +
					" SELECT " + (bOperaFactorajeConMandato?" /*+ index (d IN_COM_DOCUMENTO_09_NUK)*/ ":"" )+
					"  d.ic_estatus_docto " +
					" 	, pe.cg_pyme_epo_interno " +
					" 	, d.ig_numero_docto " +
					" 	, TO_CHAR(d.DF_FECHA_DOCTO, 'dd/mm/yyyy') as df_fecha_docto " +
					" 	, TO_CHAR(d.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc " +
					" 	, d.ic_moneda " +
					" 	, d.fn_monto " +
					" 	, d.cs_dscto_especial " +
					" 	, d.ct_referencia " +
					strCampos +
          ("S".equals(sDigitoIdentificadorFlag)?","+epoPEF.getCamposDigitoIdentificadorCalc():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getCamposFormateadosPEF():"")+
			 		(bOperaFactorajeConMandato?", i.IC_NAFIN_ELECTRONICO     AS clave_if "			:"")+
					(bOperaFactorajeConMandato?", mndte.IC_NAFIN_ELECTRONICO AS clave_mandante "	:"")+
					"  FROM com_documento d, comcat_pyme p, comrel_pyme_epo pe " +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'I' ) i "		:"") +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'M' ) mndte "	:"") +
					"  WHERE d.ic_pyme = p.ic_pyme " +
					" 	 AND d.ic_pyme = pe.ic_pyme " +
					" 	 AND d.ic_epo = pe.ic_epo " +
					" 	 AND d.ic_estatus_docto = 9 " +
					" 	 AND d.ic_epo = ? " +
					" 	 AND d.df_fecha_venc < TRUNC(SYSDATE+?)  " + //Dias minimos	?
					((cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY)?
							" AND d.df_fecha_venc >= TRUNC(SYSDATE+?-3)":
									" AND d.df_fecha_venc >= TRUNC(SYSDATE+?-1)")+
					(bOperaFactorajeConMandato?"   AND d.ic_if       = i.IC_EPO_PYME_IF(+)     ":"")+
					(bOperaFactorajeConMandato?"   AND d.ic_mandante = mndte.ic_epo_pyme_if(+) ":"")+
					//FODEA 006-Feb2010 Rebos - Se agregaron los documentos publicados como vencidos
					" UNION " +
					" SELECT " + (bOperaFactorajeConMandato?" /*+ index (d IN_COM_DOCUMENTO_09_NUK)*/ ":"" )+
					"  d.ic_estatus_docto " +
					" 	, pe.cg_pyme_epo_interno " +
					" 	, d.ig_numero_docto " +
					" 	, TO_CHAR(d.DF_FECHA_DOCTO, 'dd/mm/yyyy') as df_fecha_docto " +
					" 	, TO_CHAR(d.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc " +
					" 	, d.ic_moneda " +
					" 	, d.fn_monto " +
					" 	, d.cs_dscto_especial " +
					" 	, d.ct_referencia " +
					strCampos +
          ("S".equals(sDigitoIdentificadorFlag)?","+epoPEF.getCamposDigitoIdentificadorCalc():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getCamposFormateadosPEF():"")+
			 		(bOperaFactorajeConMandato?", i.IC_NAFIN_ELECTRONICO     AS clave_if "			:"")+
					(bOperaFactorajeConMandato?", mndte.IC_NAFIN_ELECTRONICO AS clave_mandante "	:"")+
					"  FROM com_documento d, comcat_pyme p, comrel_pyme_epo pe " +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'I' ) i "		:"") +
					(bOperaFactorajeConMandato?",(SELECT * FROM comrel_nafin WHERE cg_tipo = 'M' ) mndte "	:"") +
					"  WHERE d.ic_pyme = p.ic_pyme " +
					" 	 AND d.ic_pyme = pe.ic_pyme " +
					" 	 AND d.ic_epo = pe.ic_epo " +
					" 	 AND d.ic_estatus_docto = 9 " +
					" 	 AND d.ic_epo = ? " +
					"    AND d.df_alta > TRUNC(SYSDATE-1)"
					);
			log.debug("getDocumentosSinOperarWS::" + strSQLDoctos);

			ps = con.queryPrecompilado(strSQLDoctos.toString());

			ps.setInt(1, Integer.parseInt(claveEPO));
			ps.setInt(2, Integer.parseInt(claveEPO));
			ps.setInt(3, Integer.parseInt(diasMinimos));
			ps.setInt(4, Integer.parseInt(diasMinimos));
			ps.setInt(5, Integer.parseInt(claveEPO));

			sbArchivo.append("Clave Estatus," +
					"Num. proveedor," +
					"Num. Documento," +
					"Fecha de  Emision," +
					"Fecha de Vencimiento," +
					"Clave de moneda," +
					"Monto," +
					"Tipo Factoraje," +
					"Referencia" +
					strNombreCampos +
          ("S".equals(sDigitoIdentificadorFlag)?epoPEF.getDescDigitoIdentificador():"")+
          ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getDescCamposPEF():"")+
			 		(bOperaFactorajeConMandato?",Clave del Intermediario Financiero":"")+
					(bOperaFactorajeConMandato?",Clave Mandante":"")+
					"\n");

			rs = ps.executeQuery();
			while(rs.next()) {


				String estatusDocto = (rs.getString("ic_estatus_docto") == null)?"":rs.getString("ic_estatus_docto");
				String numProveedor = (rs.getString("cg_pyme_epo_interno") == null)?"":rs.getString("cg_pyme_epo_interno");
				String numDocto = (rs.getString("ig_numero_docto") == null)?"":rs.getString("ig_numero_docto");
				String fechaDocto = (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO");
				String fechaVenc = (rs.getString("df_fecha_venc") == null)?"":rs.getString("df_fecha_venc");
				String moneda = (rs.getString("ic_moneda") == null)?"":rs.getString("ic_moneda");
				String monto = (rs.getString("fn_monto") == null)?"":rs.getString("fn_monto");
				String tipoFactoraje = (rs.getString("cs_dscto_especial") == null)?"":rs.getString("cs_dscto_especial");
				String referencia = (rs.getString("ct_referencia") == null)?"":rs.getString("ct_referencia");
        String digitoIdentificador = ("S".equals(sDigitoIdentificadorFlag)?rs.getString("CG_DIGITO_IDENTIFICADOR"):"");
        String fechaEntrega = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("DF_ENTREGA"):"");
        fechaEntrega = (fechaEntrega==null)?"":fechaEntrega;
        String tipoCompra = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_TIPO_COMPRA"):"");
        tipoCompra = (tipoCompra==null)?"":tipoCompra;
        String clavePresupuestaria = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_CLAVE_PRESUPUESTARIA"):"");
        clavePresupuestaria = (clavePresupuestaria==null)?"":clavePresupuestaria;
        String periodo = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_PERIODO"):"");
        periodo = (periodo==null)?"":periodo;


				StringBuffer sbValorCampos = new StringBuffer();
				for (int j = 1; j <= numCampos; j++) {
					String valor = (rs.getString("cg_campo" + j) == null)?
							"":rs.getString("cg_campo" + j).replace(',',' ');
					sbValorCampos.append("," + valor);
				}

				String claveIntermediarioFinanciero	= (bOperaFactorajeConMandato && (rs.getString("clave_if") 		!= null))?rs.getString("clave_if")			:"";
				String claveMandante						= (bOperaFactorajeConMandato && (rs.getString("clave_mandante")!= null))?rs.getString("clave_mandante")	:"";

				sbArchivo.append(
						estatusDocto + "," + numProveedor + "," +
						numDocto + "," + fechaDocto + "," +
						fechaVenc + "," + moneda + "," +
						monto + "," + tipoFactoraje + "," +
						referencia.replace(',',' ') + sbValorCampos +
            ("S".equals(sDigitoIdentificadorFlag)?","+digitoIdentificador:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+fechaEntrega:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+tipoCompra:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+clavePresupuestaria:"")+
            ("S".equals(sPubEPOPEFFlagVal)?","+periodo:"")+
				(bOperaFactorajeConMandato?","+claveIntermediarioFinanciero:"")+
				(bOperaFactorajeConMandato?","+claveMandante:"")+
            "\n" );

			} //fin while
			rs.close();
			ps.close();


		} catch (Exception e){
			sbArchivo.append("ERROR. " + e.getMessage());
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		try { //Si el env�o del correo falla se ignora.
			String to = this.getCuentasWS(claveEPO);
			if (to != null) {
				String from = this.getCuentaWSFrom();
				Correo.enviarMensajeTexto(to, from, tituloMensaje, "("+claveEPO+" - "+usuario+")\n"+sbArchivo.toString());
			}
		} catch(Throwable t) {
			log.error("Error al enviar correo " + t.getMessage());
		}

		return sbArchivo.toString();

	}





	/* Obtenemos los dias Inhabiles para validar la Fecha de Vencimiento. */
	public String getDiasInhabiles(String epo) throws NafinException {
	AccesoDB con = new AccesoDB();
	String sDiaMesInhabiles = "";
		try{
			con.conexionDB();
			String query = "select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null UNION select cg_dia_inhabil from comrel_dia_inhabil_x_epo where ic_epo = " + epo + " and cg_dia_inhabil is not null ";
			ResultSet rs = con.queryDB(query);
			while(rs.next())
				sDiaMesInhabiles += rs.getString(1).trim()+";";

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDiasInhabiles. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return sDiaMesInhabiles;
	}

	private String getDiasInhabiles(String epo,AccesoDB con) throws NafinException {
	String sDiaMesInhabiles = "";
		try{
			String query = "select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null UNION select cg_dia_inhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil is not null and ic_epo = " + epo;
			ResultSet rs = con.queryDB(query);
			while(rs.next())
				sDiaMesInhabiles += rs.getString(1).trim()+";";

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDiasInhabiles. "+e);
			throw new NafinException("SIST0001");
		}
	return sDiaMesInhabiles;
	}

	/* Obtencion de las Fechas Minimas y Maximas de Vencimiento
	 * Adem�s obtiene la parametrizaci�n */
	// iTipoDato  1 - Para regresar la fechas en String y 2 - de tipo Date.
	// Ambas fechas (Minima y Maxima) en un Vector.
		public Vector getFechasVencimiento(String sNoCliente, int iTipoDato) throws NafinException {
	AccesoDB con = new AccesoDB();
	String sFechaVencMin=null, sFechaVencMax=null, query="", spubdocto_venc="";
	String sFechaPlazoPagoFVP=null, sPlaxoMax1FVP = null, sPlaxoMax2FVP = null;
	ResultSet rs=null; Vector fv=new Vector();
		try{
			con.conexionDB();
			query =
				" SELECT TO_CHAR (SYSDATE + ig_dias_minimo, 'dd/mm/yyyy') AS fechavencmin "   +
				"        , TO_CHAR (SYSDATE + ig_dias_maximo, 'dd/mm/yyyy') AS fechavencmax"   +
				"        , cs_pub_docto_venc AS cs_pub_docto_venc"   +
				"        , TO_CHAR (TRUNC (SYSDATE) + NVL(ig_plazo_pago_fvp, 0), 'dd/mm/yyyy') as df_plazo_pago_fvp"   +
				"        , NVL(ig_plazo_max1_fvp, 0) AS ig_plazo_max1_fvp"   +
				"        , NVL(ig_plazo_max2_fvp, 0) AS ig_plazo_max2_fvp"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND ic_epo = "+ sNoCliente;
			try{
				rs = con.queryDB(query);
				if (rs.next()) {
					sFechaVencMin = rs.getString(1);
					sFechaVencMax = rs.getString(2);
					spubdocto_venc = rs.getString(3);
					sFechaPlazoPagoFVP = rs.getString(4);
					sPlaxoMax1FVP = rs.getString(5);
					sPlaxoMax2FVP = rs.getString(6);
				}

				// Si la epo no especifico valores de dias minimos y/o m�ximos se toman los default
				if (sFechaVencMin == null || sFechaVencMax == null) {
					query = "select TO_CHAR(sysdate + in_dias_minimo,'dd/mm/yyyy') as fechaVencMin, "+
							" TO_CHAR(sysdate + in_dias_maximo,'dd/mm/yyyy') as fechaVencMax "+
							" from comcat_producto_nafin "+
							" where ic_producto_nafin = 1";
					rs = con.queryDB(query);
					if (rs.next()) {
						if (sFechaVencMin == null)
							sFechaVencMin = rs.getString(1);
						if (sFechaVencMax == null)
							sFechaVencMax = rs.getString(2);
					}
					rs.close();
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0042"); }
			/* Agregamos al Vector las Fechas de Vencimiento Minima y Maxima */
			if(iTipoDato==1){
				fv.addElement(sFechaVencMin);
				fv.addElement(sFechaVencMax);
				fv.addElement(spubdocto_venc);
				fv.addElement(sFechaPlazoPagoFVP);
				fv.addElement(sPlaxoMax1FVP);
				fv.addElement(sPlaxoMax2FVP);
			}
			if(iTipoDato==2){
				fv.addElement(Comunes.parseDate(sFechaVencMin));
				fv.addElement(Comunes.parseDate(sFechaVencMax));
				fv.addElement(spubdocto_venc);
				fv.addElement(Comunes.parseDate(sFechaPlazoPagoFVP));
				fv.addElement(sPlaxoMax1FVP);
				fv.addElement(sPlaxoMax2FVP);
			}

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getFechasVencimiento. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return fv;
	}


	/* Obtencion de las Fechas Minimas y Maximas de Vencimiento */
	// iTipoDato  1 - Para regresar la fechas en String y 2 - de tipo Date.
	// Ambas fechas (Minima y Maxima) en un Vector.
	private Vector getFechasVencimiento(String sNoCliente, int iTipoDato,AccesoDB con) throws NafinException {
	String sFechaVencMin=null, sFechaVencMax=null, query="";
	ResultSet rs=null; Vector fv=new Vector();
		try{
			query =
				" SELECT TO_CHAR (SYSDATE + ig_dias_minimo, 'dd/mm/yyyy') AS fechavencmin, "   +
				"        TO_CHAR (SYSDATE + ig_dias_maximo, 'dd/mm/yyyy') AS fechavencmax"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND ic_epo = "+ sNoCliente;
			try{
				rs = con.queryDB(query);
				if (rs.next()) {
					sFechaVencMin = rs.getString(1);
					sFechaVencMax = rs.getString(2);
				}

				// Si la epo no especifico valores de dias minimos y/o m�ximos se toman los default
				if (sFechaVencMin == null || sFechaVencMax == null) {
					query = "select TO_CHAR(sysdate + in_dias_minimo,'dd/mm/yyyy') as fechaVencMin, "+
							" TO_CHAR(sysdate + in_dias_maximo,'dd/mm/yyyy') as fechaVencMax "+
							" from comcat_producto_nafin "+
							" where ic_producto_nafin = 1";
					rs = con.queryDB(query);
					if (rs.next()) {
						if (sFechaVencMin == null)
							sFechaVencMin = rs.getString(1);
						if (sFechaVencMax == null)
							sFechaVencMax = rs.getString(2);
					}
					rs.close();
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0042"); }
			/* Agregamos al Vector las Fechas de Vencimiento Minima y Maxima */
			if(iTipoDato==1){
				fv.addElement(sFechaVencMin);
				fv.addElement(sFechaVencMax);
			}
			if(iTipoDato==2){
				fv.addElement(Comunes.parseDate(sFechaVencMin));
				fv.addElement(Comunes.parseDate(sFechaVencMax));
			}

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getFechasVencimiento. "+e);
			throw new NafinException("SIST0001");
		}
	return fv;
	}


	/**
	 * Genera un archivo origen de carga de documentos, a partir del numero de acuse
	 * @param ic_epo Clave Epo
	 * @param cc_acuse Clave del acuse
	 * @return Cadena con el contenido para el archivo a generar
	 */
	public String generarArchivoOrigen(String ic_epo, String cc_acuse)
			throws NafinException {

		StringBuffer contenidoArchivo = new StringBuffer();
		AccesoDB con = new AccesoDB();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_epo == null || ic_epo.equals("") ||
					cc_acuse == null || cc_acuse.equals("") ) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
			Integer.parseInt(ic_epo);

		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" ic_epo=" + ic_epo + "\n" +
					" cc_acuse=" + cc_acuse);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		String strSQL =
				" SELECT d.cs_dscto_especial, " +
				" 	pe.cg_pyme_epo_interno, d.ig_numero_docto, " +
				" 	TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS df_fecha_docto, " +
				" 	TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc, " +
				"  d.ic_moneda, d.fn_monto, d.ct_referencia, " +
				" 	d.cg_campo1, d.cg_campo2, d.cg_campo3, " +
				" 	d.cg_campo4, d.cg_campo5, " +
				" 	n.ic_nafin_electronico AS ne_if, " +
				" 	n1.ic_nafin_electronico AS ne_if_benef, " +
				" 	d.fn_porc_beneficiario, " +
				" 	d.fn_monto * d.fn_porc_beneficiario / 100 AS monto_benef " +
				" FROM com_documento d, comrel_pyme_epo pe, " +
				" 	com_acuse1 a1, comrel_nafin n, " +
				" 	comrel_nafin n1 " +
				" WHERE " +
				" 	d.cc_acuse = ? " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_pyme = pe.ic_pyme " +
				" 	AND d.cc_acuse = a1.cc_acuse " +
				" 	AND d.ic_if = n.ic_epo_pyme_if (+) " +
				" 	AND n.cg_tipo (+) = 'I' " +
				" 	AND d.ic_beneficiario = n1.ic_epo_pyme_if (+) " +
				" 	AND n1.cg_tipo (+) = 'I' " +
				" ORDER BY d.cs_dscto_especial, " +
				" 	pe.cg_pyme_epo_interno, d.ig_numero_docto ";

		try {

			con.conexionDB();

			int numCamposAdicionales = getNumCamposAdic(ic_epo, con);
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String numProveedor = (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
				String numeroDocumento = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				String fechaDocumento = (rs.getString("DF_FECHA_DOCTO")==null)?"":rs.getString("DF_FECHA_DOCTO");
				String fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
				String claveMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
				String monto = (rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO");
				String tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
				String referencia = (rs.getString("CT_REFERENCIA")==null)?"":rs.getString("CT_REFERENCIA");
				String campoDinamico1 = (rs.getString("CG_CAMPO1")==null)?"":rs.getString("CG_CAMPO1");
				String campoDinamico2 = (rs.getString("CG_CAMPO2")==null)?"":rs.getString("CG_CAMPO2");
				String campoDinamico3 = (rs.getString("CG_CAMPO3")==null)?"":rs.getString("CG_CAMPO3");
				String campoDinamico4 = (rs.getString("CG_CAMPO4")==null)?"":rs.getString("CG_CAMPO4");
				String campoDinamico5 = (rs.getString("CG_CAMPO5")==null)?"":rs.getString("CG_CAMPO5");
				String neIf = (rs.getString("NE_IF")==null)?"":rs.getString("NE_IF");
				String neIfBeneficiario = (rs.getString("NE_IF_BENEF")==null)?"":rs.getString("NE_IF_BENEF");
				String porcentajeBeneficiario = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
				String montoBeneficiario = (rs.getString("MONTO_BENEF")==null)?"":rs.getString("MONTO_BENEF");



				contenidoArchivo.append(
						numProveedor + "|" + numeroDocumento + "|" +
						fechaDocumento + "|" +
						fechaVencimiento + "|" + claveMoneda + "|" +
						monto + "|" + tipoFactoraje + "|" +
						referencia);

				if (numCamposAdicionales>= 1) {
						contenidoArchivo.append("|" + campoDinamico1);
				}
				if (numCamposAdicionales>= 2) {
						contenidoArchivo.append("|" + campoDinamico2);
				}
				if (numCamposAdicionales>= 3) {
						contenidoArchivo.append("|" + campoDinamico3);
				}
				if (numCamposAdicionales>= 4) {
						contenidoArchivo.append("|" + campoDinamico4);
				}
				if (numCamposAdicionales>= 5) {
						contenidoArchivo.append("|" + campoDinamico5);
				}


				if (tipoFactoraje.equals("N") || tipoFactoraje.equals("C") ||
						tipoFactoraje.equals("X") ) {	//Normal o Notas de Credito � X
					contenidoArchivo.append("\n");
				} else if (tipoFactoraje.equals("V")) { //Vencido
					contenidoArchivo.append("|" + neIf + "\n" );
				} else if (tipoFactoraje.equals("D")) { //Distribuidor
					contenidoArchivo.append("||" + neIfBeneficiario + "|" +
							porcentajeBeneficiario + "|" + montoBeneficiario + "\n" );
				} else { //Si no est� soportado el tipo de factoraje lo trata como Normal
					contenidoArchivo.append("\n");
				}
			}//fin while
			rs.close();
			ps.close();
			return contenidoArchivo.toString();
		} catch(SQLException e) {
			log.error("Query::" + strSQL + " " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene los avisos de notificacion
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 * @param tipoAviso Tipo de aviso de notificaci�n
	 * 		"1".- Formato simple.
	 * 		"2".- Formato de exportaci�n de interfases.
	 * @param neIF Numero de Nafin Electronico del IF
	 * @param fechaNotificacionIni Fecha de Notificacion Inicial dd/mm/aaaa
	 * @param fechaNotificacionFin Fecha de Notificacion Final dd/mm/aaaa
	 * @param fechaVencimientoIni Fecha de Vencimiento Inicial dd/mm/aaaa
	 * @param fechaVencimientoFin Fecha de Vencimiento Final dd/mm/aaaa
	 * @param acuseNotificacion Acuse de notificaci�n
	 *
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 * si no hay informaci�n que concuerde con los par�metros epecificados
	 * o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 *
	 */

	public String getAvisosNotificacionWS(
			String claveEPO, String usuario, String password,
			String tipoAviso,
			String neIF,
			String fechaNotificacionIni,
			String fechaNotificacionFin,
			String fechaVencimientoIni,
			String fechaVencimientoFin,
			String acuseNotificacion) {

		StringBuffer sbArchivo = new StringBuffer();

		AccesoDB con = new AccesoDB();
		String tituloMensaje = "Ejecucion getAvisosNotificacionWS";
    String sPubEPOPEFFlagVal						= null;
    String sDigitoIdentificadorFlag    = null;
    EpoPEF epoPEF = new EpoPEF();
	 boolean bOperaFactorajeConMandato = false; // FODEA 046-2009

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (tipoAviso == null || tipoAviso.equals("")) {
				throw new Exception("El par�metro -Tipo de Aviso- debe ser especificado");
			}
			if (!tipoAviso.equals("1") && !tipoAviso.equals("2")) {
				throw new Exception("El valor del par�metro -Tipo de Aviso- no es valido");
			}
			if (neIF != null && !neIF.equals("")) {
				Integer.parseInt(neIF);
			}


			if (fechaNotificacionIni != null && !fechaNotificacionIni.equals("")) {
				Comunes.parseDate(fechaNotificacionIni);
			}
			if (fechaNotificacionFin != null && !fechaNotificacionFin.equals("")) {
				Comunes.parseDate(fechaNotificacionFin);
			}
			if (fechaVencimientoIni != null && !fechaVencimientoIni.equals("")) {
				Comunes.parseDate(fechaVencimientoIni);
			}
			if (fechaVencimientoFin != null && !fechaVencimientoFin.equals("")) {
				Comunes.parseDate(fechaVencimientoFin);
			}
			//if (acuseNotificacion != null && !acuseNotificacion.equals("")) {
			//	Comunes.parseDate(acuseNotificacion);
			//}

			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

			log.info("getAvisosNotificacionWS::Validando claveEPO/usuario");

			if (!cuentasEPO.contains(usuario)) {
				throw new Exception("El usuario no es de la epo especificada");
			}

			log.info("getAvisosNotificacionWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(usuario, password)) {
				throw new Exception("Fall� la autenticaci�n del usuario");
			}

		} catch(Exception e) {
			String error =
					"Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"claveEPO=" + claveEPO + "\n" +
					"usuario=" + usuario + "\n" +
					"password=********" + "\n" +
					"neIF=" + neIF + "\n" +
					"fechaNotificacionIni=" + fechaNotificacionIni + "\n" +
					"fechaNotificacionFin=" + fechaNotificacionFin + "\n" +
					"fechaVencimientoIni=" + fechaVencimientoIni + "\n" +
					"fechaVencimientoFin=" + fechaVencimientoFin + "\n" +
					"acuseNotificacion=" + acuseNotificacion + "\n";

			log.error(error);

			try { //Si el env�o del correo falla se ignora.
				String to = this.getCuentasWS(claveEPO);
				if (to != null) {
					String from = this.getCuentaWSFrom();
					Correo.enviarMensajeTexto(to, from, tituloMensaje, "("+claveEPO+" - "+usuario+")\n"+error);
				}
			} catch(Throwable t) {
				log.error("Error al enviar correo " + t.getMessage());
			}

			return error;
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;

      /*MODIFICACIONES PARA DETERMINAR SI SE TRATA DE UNA EPO PEF Y LOS PARAMETROS ACTIVOS*/

      try {
        ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
        Hashtable alParamEPO = BeanParamDscto.getParametrosEPO(claveEPO, 1);
        sDigitoIdentificadorFlag    = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
        sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
		  bOperaFactorajeConMandato = operaFactorajeConMandato(claveEPO);
      } catch (NafinException ne){
        ne.printStackTrace();
        throw new Exception("Error en obtencion de parametros ");
      }

			//Verifica si recibio par�metros o no
			boolean neIFVacio = (neIF == null || neIF.equals(""));
			boolean fechaNotificacionIniVacio = (fechaNotificacionIni == null || fechaNotificacionIni.equals(""));
			boolean fechaNotificacionFinVacio = (fechaNotificacionFin == null || fechaNotificacionFin.equals(""));
			boolean fechaVencimientoIniVacio = (fechaVencimientoIni == null || fechaVencimientoIni.equals(""));
			boolean fechaVencimientoFinVacio = (fechaVencimientoFin == null || fechaVencimientoFin.equals(""));
			boolean acuseNotificacionVacio = (acuseNotificacion == null || acuseNotificacion.equals(""));

			boolean parametrosVacios =
					neIFVacio && fechaNotificacionIniVacio && fechaNotificacionFinVacio &&
					fechaVencimientoIniVacio && fechaVencimientoFinVacio && acuseNotificacionVacio;

			int ic_if = 0;

			if (!neIFVacio) {
				String sqlNE =
						" SELECT n.ic_epo_pyme_if " +
						" FROM comrel_nafin n, comcat_if i, comrel_if_epo ie " +
						" WHERE n.ic_nafin_electronico = ? " +
						" AND n.cg_tipo = 'I' " +
						" AND n.ic_epo_pyme_if = i.ic_if " +
						" AND i.ic_if = ie.ic_if " +
						" AND i.cs_habilitado='S' " +
						" AND ie.ic_epo = ? " +
						" AND IE.cs_vobo_nafin = 'S' ";

				ps = con.queryPrecompilado(sqlNE);
				ps.setInt(1, Integer.parseInt(neIF));
				ps.setInt(2, Integer.parseInt(claveEPO));

				rs = ps.executeQuery();
				if (rs.next()) {
					ic_if = rs.getInt("ic_epo_pyme_if");
				}
				if (ic_if == 0) {
					throw new Exception("Error en el nafin electr�nico del IF");
				}
				rs.close();
				ps.close();

			}


			StringBuffer strCampos = new StringBuffer();
			StringBuffer strNombreCampos = new StringBuffer();
			int numCampos = 0;
			String strSQL =
					" SELECT ic_no_campo, " +
					" 	INITCAP(cg_nombre_campo) AS cg_nombre_campo " +
					" FROM comrel_visor " +
					" WHERE ic_epo = ? AND ic_producto_nafin = 1 " +
					" 	AND ic_no_campo BETWEEN 1 AND 5 " +
					" ORDER BY ic_no_campo ";
			ps  = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEPO));
			rs = ps.executeQuery();
			while(rs.next()) {
				numCampos++;
				strCampos.append(",D.CG_CAMPO" + rs.getString("IC_NO_CAMPO"));
				strNombreCampos.append(" ," + rs.getString("CG_NOMBRE_CAMPO"));
			}
			rs.close();
			ps.close();


			//---------------------- Query de Avisos de Notificaci�n -------------------



			StringBuffer sbColumnas = new StringBuffer(
				" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIF, " +
				" to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, " +
				" A3.cc_acuse, " +
				" (decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePyme, " +
				" D.ig_numero_docto, " +
				" to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				" to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, " +
				" M.cd_nombre, " +
				" D.fn_monto, " +
				" D.fn_porc_anticipo, " +
				" D.fn_monto_dscto, " +
				" IE.ig_sucursal, " +
				" IE.cg_banco, " +
				" IE.cg_num_cuenta, " +
				" PE.cg_pyme_epo_interno, " +
				//" D.ic_moneda, " +
				//" D.ic_documento, " +
				//" D.cs_detalle, " +
				" D.CT_REFERENCIA " +
				strCampos +
				//" , I2.cg_razon_social as beneficiario " +
				//" , d.fn_porc_beneficiario as por_beneficiario " +
				//" , DS.fn_importe_recibir_benef as importe_a_recibir " +
				" , DS.in_tasa_aceptada " +
				//" , replace(I.cg_rfc, '-', '') as CG_RFC_IF " +
				//" , d.cs_dscto_especial, " +
				//"  (select d2.ig_numero_docto from com_documento d2 where d2.ic_documento = d.ic_docto_asociado) as doctoasociado " +
				//" , D.fn_monto_ant " +
				" , 'CargaDocumentosBean::getAvisosNotificacionWS' AS PANTALLA " +
        ("S".equals(sDigitoIdentificadorFlag)?","+epoPEF.getCamposDigitoIdentificadorCalc():"")+
        ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getCamposFormateadosPEF():"")+
		  		", d.cs_dscto_especial " +
				(bOperaFactorajeConMandato?", mndte.cg_razon_social as mandante ":"")+ // Fodea 046 - 2009
				"   FROM "
        );

			StringBuffer sbHint = new StringBuffer(" ");
			StringBuffer sbTablas = new StringBuffer(" ");

			if(parametrosVacios || !fechaNotificacionIniVacio ||
					!fechaNotificacionFinVacio || !acuseNotificacionVacio ) {

				//Entra si no hay par�metros especificados o
				//s�lo estan especificados los de fecha de notificaci�n o el acuse

				// Caso en que la entrada se realizara por la tabla de acuse

				sbHint.append(" /*+ use_nl(s,ds,d,pe,p,ie,i,m) ordered */ ");

				sbTablas.append(
					" com_acuse3 a3," +
					" com_solicitud s," +
					" com_docto_seleccionado ds, " +
					" com_documento d, " +
					" (select * from comrel_pyme_epo where ic_epo=?) pe, " +
					" comcat_pyme p, " +
					" (select * from comrel_if_epo where ic_epo=?) ie, " +
					" comcat_if i, " +
					//" comcat_if i2, " +
					" comcat_moneda m ");

			} else { // Caso en que no se inicia con com_acuse

				sbHint.append(" /*+ use_nl(d ds s pe p a3 ie i m) "+(bOperaFactorajeConMandato?" index (mndte COMCAT_MANDANTE_PK) ":"")+" */ ");

				sbTablas.append(
						" com_documento d, " +
						" com_docto_seleccionado ds, " +
						" com_solicitud s, " +
						" comrel_pyme_epo pe, " +
						" comcat_pyme p, " +
						" com_acuse3 a3, " +
						" comrel_if_epo ie, " +
						" comcat_if i, " +
						//" comcat_if i2, " +
						" comcat_moneda m ");
			} // Fin de la creacion de Hints, Tablas y Condiciones Adicionales

			if(bOperaFactorajeConMandato){
				sbTablas.append(", comcat_mandante mndte ");
			}

			StringBuffer sbCondicion = new StringBuffer(
					"  WHERE d.ic_documento = ds.ic_documento " +
					"    AND ds.ic_documento = s.ic_documento " +
					"    AND s.cc_acuse = a3.cc_acuse " +
					"    AND ds.ic_if = ie.ic_if " +
					"    AND ie.ic_if = i.ic_if " +
					"    AND ie.ic_epo = pe.ic_epo " +
					"    AND p.ic_pyme = pe.ic_pyme " +
					"    AND d.ic_pyme = p.ic_pyme " +
					//"    AND d.ic_beneficiario = i2.ic_if (+) " +
					"    AND d.ic_moneda = m.ic_moneda " +
					"    AND ds.ic_epo = ? " +
					"    AND d.ic_epo = ? " +
					"    AND ie.ic_epo = ? " +
					"    AND pe.ic_epo = ? " +
					"    AND ie.cs_vobo_nafin = 'S' ");


			StringBuffer sbCondicionAdic = new StringBuffer(" ");
			if(parametrosVacios) {
				sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(SYSDATE) ");
				sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(SYSDATE+1) ");
			} else { //Se proporciono por lo menos un parametro
				if(!fechaNotificacionIniVacio && !fechaNotificacionFinVacio) {
					sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
					sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(TO_DATE(?,'dd/mm/yyyy')+1) ");
				}

				if(!fechaVencimientoIniVacio && !fechaVencimientoFinVacio) {
					sbCondicionAdic.append(" and D.df_fecha_venc >= TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
					sbCondicionAdic.append(" and D.df_fecha_venc < TRUNC(TO_DATE(?,'dd/mm/yyyy')+1) ");
				}

				if (!neIFVacio) {
					sbCondicionAdic.append(" AND DS.ic_if = ?  AND ie.ic_if = ? AND i.ic_if = ? ");
				}

				if (!acuseNotificacionVacio) {
					sbCondicionAdic.append(" AND A3.cc_acuse = ? ");
				}
			} //Fin del if-else por el numero de parametros

			if(bOperaFactorajeConMandato){
				sbCondicionAdic.append(" AND d.ic_mandante = mndte.ic_mandante(+) ");
			}

			String queryFile =
					"SELECT " + sbHint.toString() +
					sbColumnas.toString() +
					sbTablas.toString() +
					sbCondicion.toString() +
					sbCondicionAdic.toString();

	log.debug("CargaDocumento:getAvisosNotificacionWS:query:" + queryFile);

			ps = con.queryPrecompilado(queryFile);
			int i = 1;

		 	if(parametrosVacios || !fechaNotificacionIniVacio ||
					!fechaNotificacionFinVacio || !acuseNotificacionVacio ) {
				ps.setInt(i, Integer.parseInt(claveEPO));i++;
				ps.setInt(i, Integer.parseInt(claveEPO));i++;
			}

			ps.setInt(i, Integer.parseInt(claveEPO));i++;
			ps.setInt(i, Integer.parseInt(claveEPO));i++;
			ps.setInt(i, Integer.parseInt(claveEPO));i++;
			ps.setInt(i, Integer.parseInt(claveEPO));i++;

			if(!fechaNotificacionIniVacio && !fechaNotificacionFinVacio) {
				ps.setString(i, fechaNotificacionIni);i++;
				ps.setString(i, fechaNotificacionFin);i++;
			}

			if(!fechaVencimientoIniVacio && !fechaVencimientoFinVacio) {
				ps.setString(i, fechaVencimientoIni);i++;
				ps.setString(i, fechaVencimientoFin);i++;
			}

			if (!neIFVacio) {
				ps.setInt(i, ic_if);i++;
				ps.setInt(i, ic_if);i++;
				ps.setInt(i, ic_if);i++;
			}

			if (!acuseNotificacionVacio) {
				ps.setString(i, acuseNotificacion);i++;
			}



			if (tipoAviso.equals("1")) { //Formato: Archivo
				sbArchivo.append(
						"Intermediario financiero,Fecha de notificacion," +
						"Acuse de notificacion,Numero de Proveedor," +
						"Nombre del proveedor,Numero de documento," +
						"Fecha de emision,Fecha de vencimiento," +
						"Moneda,Monto," +
						"Sucursal,Banco," +
						"Numero de Cuenta" +
            ("S".equals(sDigitoIdentificadorFlag)?epoPEF.getDescDigitoIdentificador():"")+
            ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getDescCamposPEF():"")+
						", Tipo Factoraje"+
						(bOperaFactorajeConMandato?", Mandante":"")+ // Fodea 046 - 2009
            "\n");
			} else if (tipoAviso.equals("2")) { //Formato: Exportar Interfase
				sbArchivo.append(
						"Intermediario financiero,Fecha de notificacion," +
						"Acuse de notificacion,Numero de Proveedor," +
						"Nombre del proveedor,Numero de documento," +
						"Fecha de emision,Fecha de vencimiento," +
						"Moneda,Monto," +
						"Sucursal,Banco," +
						"Numero de Cuenta,Porcentaje Descuento," +
						"Monto de Descuento,Tasa de Descuento," +
						"Referencia " + ((numCampos > 0)?strNombreCampos.toString():"") +
            ("S".equals(sDigitoIdentificadorFlag)?epoPEF.getDescDigitoIdentificador():"")+
            ("S".equals(sPubEPOPEFFlagVal)?epoPEF.getDescCamposPEF():"")+
						(bOperaFactorajeConMandato?", Mandante":"")+
            "\n");
			}

			rs = ps.executeQuery();
			while(rs.next()) {
				String ifNombre = (rs.getString("nombreIF") == null)?"":rs.getString("nombreIF");
				String fechaNotificacion = (rs.getString("fecha_not") == null)?"":rs.getString("fecha_not");
				String acuse = (rs.getString("cc_acuse") == null)?"":rs.getString("cc_acuse");
				String pymeNombre = (rs.getString("nombrePyme") == null)?"":rs.getString("nombrePyme");
				String numeroDocto = (rs.getString("ig_numero_docto") == null)?"":rs.getString("ig_numero_docto");
				String fechaEmision = (rs.getString("fecha_emision") == null)?"":rs.getString("fecha_emision");
				String fechaVencimiento = (rs.getString("fecha_vencimiento") == null)?"":rs.getString("fecha_vencimiento");
				String monedaNombre = (rs.getString("cd_nombre") == null)?"":rs.getString("cd_nombre");
				String monto = (rs.getString("fn_monto") == null)?"0.00":rs.getString("fn_monto");
				String strPorcentaje = (rs.getString("fn_porc_anticipo") == null)?"0.00":rs.getString("fn_porc_anticipo");
				String strMontoDescuento = (rs.getString("fn_monto_dscto") == null)?"0.00":rs.getString("fn_monto_dscto");
				String sucursal = (rs.getString("ig_sucursal") == null)?"":rs.getString("ig_sucursal");
				String banco = (rs.getString("cg_banco") == null)?"":rs.getString("cg_banco");
				String noCuenta = (rs.getString("cg_num_cuenta") == null)?"":rs.getString("cg_num_cuenta");
				String noPyme = (rs.getString("cg_pyme_epo_interno") == null)?"":rs.getString("cg_pyme_epo_interno");
        String digitoIdentificador = ("S".equals(sDigitoIdentificadorFlag)?rs.getString("CG_DIGITO_IDENTIFICADOR"):"");
        String fechaEntrega = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("DF_ENTREGA"):"");
        fechaEntrega = (fechaEntrega==null)?"":fechaEntrega;
        String tipoCompra = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_TIPO_COMPRA"):"");
        tipoCompra = (tipoCompra==null)?"":tipoCompra;
        String clavePresupuestaria = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_CLAVE_PRESUPUESTARIA"):"");
        clavePresupuestaria = (clavePresupuestaria==null)?"":clavePresupuestaria;
        String periodo = ("S".equals(sPubEPOPEFFlagVal)?rs.getString("CG_PERIODO"):"");
        periodo = (periodo==null)?"":periodo;

//				String beneficiario = (rs.getString("beneficiario") == null)?"":rs.getString("beneficiario").trim();
//				String por_beneficiario = (rs.getString("por_beneficiario")  == null)?"":rs.getString("por_beneficiario").trim();
//				String importe_a_recibir = (rs.getString("importe_a_recibir") == null)?"":rs.getString("importe_a_recibir").trim();

				String tasaAceptada = (rs.getString("in_tasa_aceptada") == null)?"":rs.getString("in_tasa_aceptada");
				String strCT_REFERENCIA = (rs.getString("CT_REFERENCIA") == null)?"":rs.getString("CT_REFERENCIA");

//				String rs_docto_asociado = (rs.getString("doctoasociado") == null)?"":rs.getString("doctoasociado").trim();
//				String rs_tipo_factoraje = (rs.getString("cs_dscto_especial") == null)?"":rs.getString("cs_dscto_especial").trim();
//				String rs_monto_ant = (rs.getString("fn_monto_ant") == null)?"":rs.getString("fn_monto_ant").trim();
//				rs_tipo_factoraje =
//					"N".equals(rs_tipo_factoraje)?"Normal":
//					"V".equals(rs_tipo_factoraje)?"Vencido":"";
				String tipoFactoraje =	(rs.getString("cs_dscto_especial") == null)?"":rs.getString("cs_dscto_especial").trim();
				String mandante 	= 	(bOperaFactorajeConMandato &&  (rs.getString("MANDANTE") != null) )?rs.getString("MANDANTE"):"";


				StringBuffer sbValorCampos = new StringBuffer();
				for (int j = 1; j <= numCampos; j++) {
					String valor = (rs.getString("cg_campo" + j) == null)?
							"":rs.getString("cg_campo" + j).replace(',',' ');
					sbValorCampos.append("," + valor);
				}

				ifNombre = ifNombre.replace('*',' ').trim();
				pymeNombre = pymeNombre.replace('*',' ').trim();

				if (tipoAviso.equals("1")) {
					sbArchivo.append(
							ifNombre.replace(',',' ')+","+fechaNotificacion+","+
							acuse+"," + noPyme+","+
							pymeNombre.replace(',',' ')+","+numeroDocto+","+
							fechaEmision+","+fechaVencimiento+","+
							monedaNombre+"," +
							Comunes.formatoDecimal(monto,2,false)+","+sucursal+","+
							banco.replace(',',' ')+","+noCuenta+
              ("S".equals(sDigitoIdentificadorFlag)?","+digitoIdentificador:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+fechaEntrega:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+tipoCompra:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+clavePresupuestaria:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+periodo:"")+
				  ","+tipoFactoraje+
				  (bOperaFactorajeConMandato?","+mandante:"")+// Fodea 046 - 2009
              "\n");
				} else if (tipoAviso.equals("2")) {
					sbArchivo.append(
							ifNombre.replace(',',' ')+","+fechaNotificacion+","+
							acuse+"," +noPyme+"," +
							pymeNombre.replace(',',' ')+","+numeroDocto+","+
							fechaEmision+","+fechaVencimiento+"," +
							monedaNombre+","+ Comunes.formatoDecimal(monto,2,false)+","+
							sucursal+","+banco.replace(',',' ') +","+
							noCuenta+ "," + strPorcentaje+ "," +
							strMontoDescuento + "," + tasaAceptada + "," +
							strCT_REFERENCIA + sbValorCampos +
              ("S".equals(sDigitoIdentificadorFlag)?","+digitoIdentificador:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+fechaEntrega:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+tipoCompra:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+clavePresupuestaria:"")+
              ("S".equals(sPubEPOPEFFlagVal)?","+periodo:"")+
				  (bOperaFactorajeConMandato?","+mandante:"")+// Fodea 046 - 2009
              "\n");
				}
			} //fin while
			rs.close();
			ps.close();


		} catch (Exception e){
			sbArchivo.append("ERROR. " + e.getMessage());
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		try { //Si el env�o del correo falla se ignora.
			String to = this.getCuentasWS(claveEPO);
			if (to != null) {
				String from = this.getCuentaWSFrom();
				Correo.enviarMensajeTexto(to, from, tituloMensaje, "("+claveEPO+" - "+usuario+")\n"+sbArchivo.toString());
			}
		} catch(Throwable t) {
			log.error("Error al enviar correo " + t.getMessage());
		}

		return sbArchivo.toString();

	}













	/* Obtiene los campos adicionales definidos por la epo. */
	// Modificado 25/08/2004 FODA 061 - 2004 -- EGB
	public Vector getCamposAdicionales(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vCamposAdcionales = new Vector();
	Vector vca = null;
		try{
			con.conexionDB();
			String query = " select v.ic_no_campo"   +
						" , initcap(v.cg_nombre_campo)"   +
						" , v.cg_tipo_dato"   +
						" , v.ig_longitud"   +
						" , v.cs_obligatorio"   +
						" , DECODE(NVL(pe.cg_tipo_cobranza, 'N'), 'N', DECODE(NVL(pe2.cs_cobranza_referencial, 'N'), 'S', 'R', 'N' ), pe.cg_tipo_cobranza )  as tipo_cobranza"   +
						" , 'CargaDocumentoBean.getCamposAdicionales(Query)' as pantalla " +
						" from comrel_visor v"   +
						" , (select ic_epo, cg_tipo_cobranza"   +
						" from comrel_producto_epo"   +
						" where ic_epo = "   +sNoCliente+
						" and ic_producto_nafin = 6) pe"   +
						" , (select ic_epo, cs_cobranza_referencial"   +
						" from comrel_producto_epo"   +
						" where ic_epo = "   +sNoCliente+
						" and ic_producto_nafin = 2) pe2"   +
						" where v.ic_epo = "   +sNoCliente+
						" and v.ic_epo = pe.ic_epo (+)"   +
						" and v.ic_epo = pe2.ic_epo (+)"   +
						" and v.ic_producto_nafin = 1"   +
						" and v.ic_no_campo between 1 and 5"   +
						" order by v.ic_no_campo"  ;
			try{
				ResultSet rs = con.queryDB(query);
				while (rs.next()) {
					vca = new Vector();
					vca.add(rs.getString(1));
					vca.add(rs.getString(2).trim());
					vca.add(rs.getString(3).trim());
					vca.add(rs.getString(4));
					vca.add(rs.getString(5));
					vca.add(rs.getString(6));
					vCamposAdcionales.addElement(vca);
				}
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0043"); }
			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getCamposAdicionales. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vCamposAdcionales;
	}

	/* Obtiene los campos adicionales definidos por la epo. */
	private Vector getCamposAdicionales(String sNoCliente,AccesoDB con) throws NafinException {
	Vector vCamposAdcionales = new Vector();
	Vector vca = null;
		try{
			String query = "select ic_no_campo, initcap(cg_nombre_campo), "+
							" cg_tipo_dato, ig_longitud"+
							" from comrel_visor"+
							" where ic_epo = "+sNoCliente+
							" and ic_producto_nafin = 1"+
							" and ic_no_campo between 1 and 5"+
							" order by ic_no_campo";
			try{
				ResultSet rs = con.queryDB(query);
				while (rs.next()) {
					vca = new Vector();
					vca.add(rs.getString(1));
					vca.add(rs.getString(2).trim());
					vca.add(rs.getString(3).trim());
					vca.add(rs.getString(4));
					vCamposAdcionales.addElement(vca);
				}
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0043"); }
			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getCamposAdicionales. "+e);
			throw new NafinException("SIST0001");
		}
	return vCamposAdcionales;
	}

	public Vector getNombresCamposAdicionales(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vca = new Vector();
		try{
			con.conexionDB();
			String query = "select initcap(cg_nombre_campo) "+
							" from comrel_visor"+
							" where ic_epo = "+sNoCliente+
							" and ic_producto_nafin = 1"+
							" and ic_no_campo between 1 and 5"+
							" order by ic_no_campo";
			ResultSet rs = con.queryDB(query);
			while (rs.next())
				vca.add(rs.getString(1));

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getNombresCamposAdicionales. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vca;
	}

	public int existenDoctosCargados(String sNoCliente, String sNumeroDocto, String df_fecha_docto, String ic_moneda, String ic_pyme) throws NafinException {
	log.info("CargaDocumentoBean.existenDoctosCargados(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	int iConteoDocto=0;
		try{
			con.conexionDB();
			String query = "select count(1) from com_documento "+
					" where ic_epo=?"+//sNoCliente+
					" and ig_numero_docto=?"+//sNumeroDocto+"'"+
					" and to_char(df_fecha_docto,'yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'yyyy')"+
					" and ic_moneda = ?" + //+ic_moneda;
					" and ic_pyme = ?";
			try{
				log.debug("CargaDocumentoBean.existenDoctosCargados(ic_pyme)" + ic_pyme);
				ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(sNoCliente));
				ps.setString(2, sNumeroDocto);
				ps.setString(3, df_fecha_docto);
				ps.setInt(4, Integer.parseInt(ic_moneda));
				ps.setInt(5, Integer.parseInt(ic_pyme));
				ResultSet rs = ps.executeQuery();

				if(rs.next()){
					iConteoDocto = rs.getInt(1);
				}
				rs.close();
				if(ps != null) ps.close();
				if (iConteoDocto > 0) {
					throw new NafinException("DSCT0056");
				}
			} catch(SQLException sqle) {
				if(ps != null) ps.close();
				throw new NafinException("DSCT0039");
			}

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en existenDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			log.info("CargaDocumentoBean.existenDoctosCargados(S)");
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return iConteoDocto;
	}

	private int existenDoctosCargados(String sNoCliente, String sNumeroDocto,String df_fecha_docto,String ic_moneda,AccesoDB con) throws NafinException {
	int iConteoDocto=0;
		try{
			String query = "select count(*) from com_documento "+
					" where ic_epo="+sNoCliente+
					" and ig_numero_docto='"+sNumeroDocto+"'"+
					" and to_char(df_fecha_docto,'yyyy') = to_char(to_date('"+df_fecha_docto+"','dd/mm/yyyy'),'yyyy')"+
					" and ic_moneda = "+ic_moneda;
			try{
				ResultSet rs = con.queryDB(query);
				if(rs.next())
					iConteoDocto = rs.getInt(1);
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0039"); }

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en existenDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		}
	return iConteoDocto;
	}

	public int existenDoctosCargadosTmp(String sProcDocto, String sNumeroDocto, String df_fecha_docto, String ic_moneda, String ic_pyme) throws NafinException {
	log.info("CargaDocumentoBean.existenDoctosCargadosTmp(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	int iConteoDocto=0;
		try{

			con.conexionDB();
			String query = "select count(1) from comtmp_proc_docto "+
					" where ic_proc_docto= ? "+ //sProcDocto+
					" and ig_numero_docto=?"+ //sNumeroDocto+"'"+
					" and to_char(df_fecha_docto,'yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'yyyy')"+
					" and ic_moneda = ?"+//ic_moneda;
					" and ic_pyme = ?";
			try{
				log.debug("CargaDocumentoBean.existenDoctosCargadosTmp(ic_pyme)" + ic_pyme);
				ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(sProcDocto));
				ps.setString(2, sNumeroDocto);
				ps.setString(3, df_fecha_docto);
				ps.setInt(4, Integer.parseInt(ic_moneda));
				ps.setInt(5, Integer.parseInt(ic_pyme));
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					iConteoDocto = rs.getInt(1);
				}
				rs.close();
				if(ps != null) ps.close();
				if (iConteoDocto > 0) {
					throw new NafinException("DSCT0056");
				}
			} catch(SQLException sqle) {
				if(ps != null) ps.close();
				throw new NafinException("DSCT0040");
			}

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en existenDoctosCargadosTmp. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("CargaDocumentoBean.existenDoctosCargadosTmp(S)");
		}
	return iConteoDocto;
	}

	private int existenDoctosCargadosTmp(String sProcDocto, String sNumeroDocto, String df_fecha_docto,String ic_moneda, AccesoDB con) throws NafinException {
	int iConteoDocto=0;
		try{
			String query = "select count(1) from comtmp_proc_docto "+
					" where ic_proc_docto="+sProcDocto+
					" and ig_numero_docto='"+sNumeroDocto+"'"+
					" and to_char(df_fecha_docto,'yyyy') = to_char(to_date('"+df_fecha_docto+"','dd/mm/yyyy'),'yyyy')"+
					" and ic_moneda = "+ic_moneda;
			try{
				ResultSet rs = con.queryDB(query);
				if(rs.next())
					iConteoDocto = rs.getInt(1);
				rs.close();
				if (iConteoDocto > 0) {
					throw new NafinException("DSCT0056");
				}
			} catch(SQLException sqle) { throw new NafinException("DSCT0040"); }

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en existenDoctosCargadosTmp. "+e);
			throw new NafinException("SIST0001");
		}
	return iConteoDocto;
	}

	public String getPyme(String sNoCliente, String sPymeEpoInterno) throws NafinException {
	AccesoDB con = new AccesoDB();
	String sIcPyme = "";
		try{
			con.conexionDB();
			String query = "select ic_pyme from comrel_pyme_epo "+
							" where ic_epo="+sNoCliente+
							" and cg_pyme_epo_interno = '"+sPymeEpoInterno+"'"+
							" and cs_habilitado = 'S'";
			try{
				ResultSet rs = con.queryDB(query);
				sIcPyme = (rs.next())?rs.getString(1):"";
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0038"); }

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getPyme. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return sIcPyme;
	}


	private String getPyme(String sNoCliente, String sPymeEpoInterno,AccesoDB con) throws NafinException {
	String sIcPyme = "";
		try{
			String query = "select ic_pyme from comrel_pyme_epo "+
							" where ic_epo="+sNoCliente+
							" and cg_pyme_epo_interno = '"+sPymeEpoInterno+"'"+
							" and cs_habilitado = 'S'";
			try{
				ResultSet rs = con.queryDB(query);
				sIcPyme = (rs.next())?rs.getString(1):"";
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0038"); }

			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getPyme. "+e);
			throw new NafinException("SIST0001");
		}
	return sIcPyme;
	}

	public Hashtable procesarDocumentos(String sNoCliente, String sLineasDoctos, String ctxtmtomindoc,
										String ctxtmtomaxdoc, String ctxtmtomindocdol, String ctxtmtomaxdocdol,
										String ctxttotdoc, String ctxttotdocdol, String ctxtmtodoc,
										String ctxtmtodocdol, String ic_proc_docto) throws NafinException {
	AccesoDB con = new AccesoDB(); 	ResultSet rs = null;
	StringBuffer error=new StringBuffer();		StringBuffer lineasErrores=new StringBuffer();
	StringBuffer sinError=new StringBuffer();	StringBuffer errorCifras=new StringBuffer();
	boolean ok = true, registroOk = true, bBotonProcesar = false;
	String query = "", MSG_ERROR = "", sLineaDocto = "", no_pyme = "";
	String ic_pyme="", numero_docto="", fecha_docto="", fecha_venc="", ic_moneda="", fn_monto="";
	String dscto_especial="", ct_referencia="", campo_ad1="", campo_ad2="", campo_ad3="", campo_ad4="";
	String campo_ad5="", NombreIf="", habilitadoIF="";
	String NoBeneficiario="", PorcBeneficiario="", MontoBeneficiario="";
	String sEstatusDocto="";
	int iNoLinea=0, totdoc_mn=0, totdoc_dolar=0, ic_if=0, ic_beneficiario=0;
	int totdoc_mnOK = 0, totdoc_dolarOK = 0, totdoc_mnErr = 0, totdoc_dolarErr = 0;
	BigDecimal monto_mn = new BigDecimal("0");	BigDecimal monto_dolar = new BigDecimal("0");
	BigDecimal monto_mnNeg=new BigDecimal("0.0"); BigDecimal monto_dolarNeg=new BigDecimal("0.0");
	BigDecimal monto_mnNoNeg=new BigDecimal("0.0"); BigDecimal monto_dolarNoNeg=new BigDecimal("0.0");
	BigDecimal monto_tot_mnOK=new BigDecimal("0.0"); BigDecimal monto_tot_dolarOK=new BigDecimal("0.0");
	BigDecimal monto_tot_mnErr=new BigDecimal("0.0"); BigDecimal monto_tot_dolarErr=new BigDecimal("0.0");
	BigDecimal monto_tot_mnNegOK=new BigDecimal("0.0"); BigDecimal monto_tot_dolarNegOK=new BigDecimal("0.0");
	BigDecimal monto_tot_mnNoNegOK=new BigDecimal("0.0"); BigDecimal monto_tot_dolarNoNegOK=new BigDecimal("0.0");
	BigDecimal PorcentBenef=new BigDecimal("0.0");
	Hashtable hResultados = new Hashtable();
	try{
		con.conexionDB();
		VectorTokenizer vt=null; Vector vecdat=null;
		StringTokenizer stDocto = new StringTokenizer(sLineasDoctos,"\n");
		String let=""; int pipesporlinea=0;
		while(stDocto.hasMoreElements()) {
			sLineaDocto = stDocto.nextToken().trim();
			if(sLineaDocto.length()>0) {
				iNoLinea++;
				MSG_ERROR = "Error en la l&iacute;nea "+iNoLinea;
				for(int n=0; n<sLineaDocto.length(); n++) {
					let=sLineaDocto.substring(n,n+1);
					if(let.equals("|"))
						pipesporlinea++;
				}
				registroOk = true;
				//log.debug(pipesporlinea);
				vt = new VectorTokenizer(sLineaDocto,"|");
				vecdat = vt.getValuesVector();
				//log.debug("El tama�o del Vector: "+vecdat.size());

				ic_pyme=(vecdat.size()>=1)?Comunes.quitaComitasSimples(vecdat.get(0).toString()).trim():"";
				numero_docto=(vecdat.size()>=2)?Comunes.quitaComitasSimples(vecdat.get(1).toString()).trim():"";
				fecha_docto=(vecdat.size()>=3)?Comunes.quitaComitasSimples(vecdat.get(2).toString()).trim():"";
				fecha_venc=(vecdat.size()>=4)?Comunes.quitaComitasSimples(vecdat.get(3).toString()).trim():"";
				ic_moneda=(vecdat.size()>=5)?Comunes.quitaComitasSimples(vecdat.get(4).toString()).trim():"";
				fn_monto=(vecdat.size()>=6)?Comunes.quitaComitasSimples(vecdat.get(5).toString()).trim():"";
				dscto_especial=(vecdat.size()>=7)?Comunes.quitaComitasSimples(vecdat.get(6).toString()).trim():"";
				ct_referencia=(vecdat.size()>=8)?Comunes.quitaComitasSimples(vecdat.get(7).toString()).trim():"";
				campo_ad1=(vecdat.size()>=9)?Comunes.quitaComitasSimples(vecdat.get(8).toString()).trim():"";
				campo_ad2=(vecdat.size()>=10)?Comunes.quitaComitasSimples(vecdat.get(9).toString()).trim():"";
				campo_ad3=(vecdat.size()>=11)?Comunes.quitaComitasSimples(vecdat.get(10).toString()).trim():"";
				campo_ad4=(vecdat.size()>=12)?Comunes.quitaComitasSimples(vecdat.get(11).toString()).trim():"";
				campo_ad5=(vecdat.size()>=13)?Comunes.quitaComitasSimples(vecdat.get(12).toString()).trim():"";
				log.debug("Los Valores:" +ic_pyme+"-"+numero_docto+"-"+fecha_docto+"-"+fecha_venc+"-"+ic_moneda+"-"+fn_monto+"-"+dscto_especial+"-"+ct_referencia+"-"+campo_ad1+"-"+campo_ad2+"-"+campo_ad3+"-"+campo_ad4+"-"+campo_ad5);

				dscto_especial = dscto_especial.toUpperCase().trim();
				// Verifica Factoraje Vencido
				ic_if = 0; NombreIf = "";
				if(dscto_especial.equals("V")) {
					//boolean bOperaFactorajeVencido = ;
					if(!autorizadaFactorajeVencido(sNoCliente,con)) {
						error.append(MSG_ERROR+", la EPO no puede operar con Factoraje Vencido. \n");
						registroOk = ok = false;
					}
					else if(vecdat.size()>8) { // despues de "ct_referencia"
						NombreIf = vecdat.get(vecdat.size()-1).toString().trim();
						if(NombreIf.length()==0) {
							error.append(MSG_ERROR+", la clave de la IF no es correcta (Documento con tipo de factoraje \"vencido\"). \n");
							registroOk = ok = false;
						} else {
							query = "select ic_epo_pyme_if from comrel_nafin where cg_tipo='I' and ic_nafin_electronico="+NombreIf;
							rs = con.queryDB(query);
							if(rs.next()) ic_if = rs.getInt(1);
								con.cierraStatement();
							if(ic_if == 0) {
								error.append(MSG_ERROR+", el N&uacute;mero de IF "+NombreIf+" no existe. \n");
								registroOk = ok = false;
							} else {
								query = "select ie.ic_if, i.cs_habilitado from comrel_if_epo ie, comcat_if i "+
										" where ie.cs_vobo_nafin='S' and ie.ic_if="+ic_if+" and ie.ic_epo="+sNoCliente+
										" and ie.ic_if = i.ic_if";
								rs = con.queryDB(query);
								//out.println(query);
								if(rs.next()) {
									habilitadoIF = rs.getString(2).trim();
									if(habilitadoIF.equals("N")) {
										error.append(MSG_ERROR+", el N&uacute;mero de IF "+NombreIf+" se encuentra deshabilitado, para la EPO. \n");
										registroOk = ok = false;
									}
								} else {
									error.append(MSG_ERROR+", el N&uacute;mero de IF "+NombreIf+" no existe, para la EPO. \n");
									registroOk = ok = false;
								}
								con.cierraStatement();
							}
						}
					} else {
						error.append(MSG_ERROR+", la clave de la IF no es correcta (Documento con tipo de factoraje \"vencido\"). \n");
						registroOk = ok = false;
					}
				} // termina: Verifica Factoraje Vencido

				// Verifica Factoraje Distribuido
				PorcentBenef = new BigDecimal("0");
				NoBeneficiario = ""; PorcBeneficiario = ""; MontoBeneficiario = ""; ic_beneficiario=0;
				if(dscto_especial.equals("D")) {
					if(!autorizadaFactorajeDistribuido(sNoCliente,con)) {
						error.append(MSG_ERROR+", la EPO no puede operar con Factoraje Distribuido. \n");
						registroOk = ok = false;
					}
					else if(vecdat.size()>8) { // despues de "ct_referencia"
						NoBeneficiario = vecdat.get(vecdat.size()-3).toString().trim();
						PorcBeneficiario = vecdat.get(vecdat.size()-2).toString().trim();
						MontoBeneficiario = vecdat.get(vecdat.size()-1).toString().trim();
						if(NoBeneficiario.length()==0) {
							error.append(MSG_ERROR+", la clave del Beneficiario no es correcta (Documento con tipo de factoraje \"Distribuido\"). \n");
							registroOk = ok = false;
						} else {
							query = "select ic_epo_pyme_if from comrel_nafin where cg_tipo='I' and ic_nafin_electronico="+NoBeneficiario;
							rs = con.queryDB(query);
							if(rs.next()) ic_beneficiario = rs.getInt(1);
								con.cierraStatement();
							if(ic_beneficiario == 0) {
								error.append(MSG_ERROR+", el N&uacute;mero del Beneficiario "+NoBeneficiario+" no existe. \n");
								registroOk = ok = false;
							} else {
								query = "select ie.ic_if, i.cs_habilitado from comrel_if_epo ie, comcat_if i "+
										" where ie.cs_vobo_nafin='N' and ie.cs_aceptacion='N' "+
										" and ie.ic_if="+ic_beneficiario+" and ie.ic_epo="+sNoCliente+
										" and ie.ic_if = i.ic_if";
								rs = con.queryDB(query);
								//out.println(query);
								if(rs.next()) {
									if(rs.getString(2).trim().equals("N")) {
										error.append(MSG_ERROR+", el N&uacute;mero de Beneficiario "+NoBeneficiario+" se encuentra deshabilitado, para la EPO. \n");
										registroOk = ok = false;
									}
								} else {
									error.append(MSG_ERROR+", el N&uacute;mero de Beneficiario "+NoBeneficiario+" no existe, para la EPO. \n");
									registroOk = ok = false;
								}
								con.cierraStatement();
							}
						}
						if((PorcBeneficiario.equals("") && MontoBeneficiario.equals("")) || (!PorcBeneficiario.equals("") && !MontoBeneficiario.equals("")) ) {
							error.append(MSG_ERROR+", debe de Introducir un Porcentaje o un Monto para el Beneficiario, pero no Ambos (Documento con tipo de factoraje \"Distribuido\"). \n");
							registroOk = ok = false;
						}
						if(PorcBeneficiario.equals("") && !MontoBeneficiario.equals("")) {
							if(Comunes.esDecimal(MontoBeneficiario) && Comunes.esDecimal(fn_monto)) {
								if(((new BigDecimal(MontoBeneficiario)).compareTo(new BigDecimal(fn_monto))) >= 0) {
									error.append(MSG_ERROR+", el Monto del Beneficiario no pueder ser Mayor o Igual al Monto del Documento (Documento con tipo de factoraje \"Distribuido\"). \n");
									registroOk = ok = false;
								}
								else
									PorcentBenef = ((new BigDecimal(MontoBeneficiario)).divide((new BigDecimal(fn_monto)),5,2)).multiply(new BigDecimal("100"));
							} else {
								error.append(MSG_ERROR+", el Monto del Docto y del Beneficiario deben ser valores N&uacute;mericos (Documento con tipo de factoraje \"Distribuido\"). \n");
								registroOk = ok = false;
							}
						}

						if(!PorcBeneficiario.equals("") && MontoBeneficiario.equals("")) {
							if(Comunes.esDecimal(PorcBeneficiario)) {
								if( (new Double(PorcBeneficiario)).doubleValue() <= 100) {
									PorcentBenef = (new BigDecimal(PorcBeneficiario));
								} else {
									error.append(MSG_ERROR+", el Porcentaje del Beneficiario no puede ser mayor al 100% (Documento con tipo de factoraje \"Distribuido\"). \n");
									registroOk = ok = false;
								}
							} else {
								error.append(MSG_ERROR+", el Porcentaje del Beneficiario debe ser Valor N&uacute;merico (Documento con tipo de factoraje \"Distribuido\"). \n");
								registroOk = ok = false;
							}
						}
					} else {
						error.append(MSG_ERROR+", la clave del Beneficiario no es correcta (Documento con tipo de factoraje \"Distribuido\"). \n");
						error.append(MSG_ERROR+", el porcentaje del Beneficiario no es correcto (Documento con tipo de factoraje \"Distribuido\"). \n");
						error.append(MSG_ERROR+", el monto del Beneficiario no es correcto (Documento con tipo de factoraje \"Distribuido\"). \n");
						registroOk = ok = false;
					}
				} // termina: Factoraje Distribuido

				if(!dscto_especial.equals("N") && !dscto_especial.equals("V") && !dscto_especial.equals("D") && !dscto_especial.equals("X")) {
					error.append(MSG_ERROR+", el Tipo de Factoraje debe ser (N,n / V,v / D,d) o un documento No Negociable (X,x).\n");
					registroOk = ok = false;
				}

				//String habilitadoPyme="";
				if((ic_pyme.equals("")) || (ic_pyme.length()==0)) {
					error.append(MSG_ERROR+", el valor de la PYME no puede estar vacio, debe de tener un valor. \n");
					registroOk = ok = false;
				} else {
					// Revisa si el numero de pyme existe en la relacion pyme_epo y la obtine para insertarla en la tabla temporal.
					String sIcPyme = getPyme(sNoCliente, ic_pyme,con);
					if(sIcPyme.equals("")) {
						error.append(MSG_ERROR+", el N&uacute;mero de PYME "+ic_pyme+" no existe, para la EPO. \n");
						registroOk = ok = false;
					}
					else
						no_pyme = sIcPyme;
					/*
					query ="select ic_pyme, cs_habilitado from comrel_pyme_epo where IC_EPO="+sNoCliente+" and CG_PYME_EPO_INTERNO='"+ic_pyme+"'";
					//log.debug(query);
					rs = con.queryDB(query);
					if(rs.next()) {
						this.no_pyme = rs.getString(1);
						habilitadoPyme = rs.getString(2).trim();
						if(habilitadoPyme.equals("N")) {
							error.append(MSG_ERROR+", el N&uacute;mero de PYME "+ic_pyme+" no esta habilitado, para la EPO. \n"); ok=false;
						}
					} else {
						error.append(MSG_ERROR+", el N&uacute;mero de PYME "+ic_pyme+" no existe, para la EPO. \n"); ok=false;
					}
					con.cierraStatement();
					*/
				}

				//log.debug("El n�mero de ic_proc_docto Actual: "+ic_proc_docto);
				if((numero_docto.equals("")) || (numero_docto.length()==0)) {
					error.append(MSG_ERROR+", el valor de NUMERO DOCTO no puede estar vacio, debe de tener un valor. \n");
					registroOk = ok = false;
				}
				else {
					// Revisa si ya existe c/u de los documentos del archivo en la tabla com_documento, para evitar duplicados.
					try{
//						int	doc_conteo = existenDoctosCargados(sNoCliente, numero_docto,fecha_docto,ic_moneda);
						int	doc_conteo = existenDoctosCargados(sNoCliente, numero_docto,fecha_docto,ic_moneda,con);
//						doc_conteo = doc_conteo + existenDoctosCargadosTmp(ic_proc_docto, numero_docto,fecha_docto,ic_moneda); // Revisa si ya existe c/u de los documentos del archivo en la tabla comtmp_proc_docto, para evitar duplicados.
						doc_conteo = doc_conteo + existenDoctosCargadosTmp(ic_proc_docto, numero_docto,fecha_docto,ic_moneda,con); // Revisa si ya existe c/u de los documentos del archivo en la tabla comtmp_proc_docto, para evitar duplicados.
						if(doc_conteo >= 1) {
							error.append(MSG_ERROR+", el Documento "+numero_docto+" ya existe. \n");
							registroOk = ok = false;
						}
					} catch(NafinException ne) {
						error.append(MSG_ERROR+", el Documento "+numero_docto+" ya existe. \n");
						registroOk = ok = false;
					}
					//Para guardar todos los ig_numero_docto y despues compararlos con los ig del docto detalle.
					//this.ig_nums_doctos.addElement(numero_docto);
				}//else no_documento.

				java.util.Date fechaDocto=null;
				if((fecha_docto.equals("")) || (fecha_docto.length()==0)) {
					error.append(MSG_ERROR+", el valor de la Fecha del Documento no puede estar vac&iacute;o, debe de tener un valor. \n");
					registroOk = ok = false;
				} else {
					if(Comunes.checaFecha(fecha_docto)==false) {
						error.append(MSG_ERROR+", el valor en el campo Fecha del Documento del archivo, no es una fecha correcta. \n");
						registroOk = ok = false;
					} else {
						fechaDocto=Comunes.parseDate(fecha_docto);
						//log.debug(fechaDocto);
					}
				}

				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date fechaVenc=null;
				Calendar dia_fv = new GregorianCalendar();
				if((fecha_venc.equals("")) || (fecha_venc.length()==0)) {
					error.append(MSG_ERROR+", el valor de la Fecha de Vencimiento no puede estar vac&iacute;o, debe de tener un valor. \n");
					registroOk = ok = false;
				} else {
					if(Comunes.checaFecha(fecha_venc)==false) {
						error.append(MSG_ERROR+", el valor en el campo Fecha de Vencimiento del archivo, no es una fecha correcta. \n");
						registroOk = ok = false;
					} else {
						fechaVenc=Comunes.parseDate(fecha_venc);
						/* Validacion de la Fecha que sea un dia habil. */
//						String sLineaDiaMes = getDiasInhabiles(sNoCliente);
						String sLineaDiaMes = getDiasInhabiles(sNoCliente,con);
						if (sLineaDiaMes.length() > 0) {
							StringTokenizer st = new StringTokenizer(sLineaDiaMes,";");
							String sDiaMesInhabil = "";
							while(st.hasMoreElements()) {
								sDiaMesInhabil = st.nextToken();
								//log.debug(sDiaMesInhabil);
								if(fecha_venc.substring(0,5).equals(sDiaMesInhabil)) {
									error.append(MSG_ERROR+", la Fecha de Vencimiento "+sdf.format(fechaVenc)+" del archivo, no es una fecha en d&iacute;a h&aacute;bil. \n");
									registroOk = ok = false;
								}
							}
						}
						/* Validacion de que la fecha de Vencimiento que no sea Sabado o Domingo. */
						dia_fv.setTime(fechaVenc);
						int no_dia_semana=dia_fv.get(Calendar.DAY_OF_WEEK);
						if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
							error.append(MSG_ERROR+", la Fecha de Vencimiento "+sdf.format(fechaVenc)+" del archivo, no es una fecha en d&iacute;a h&aacute;bil. \n");
							registroOk = ok = false;
						}
					}//else
				}//else

				/* Validaciones de Fechas */
				if(fechaDocto!=null && fechaVenc!=null) {
					int comp_fecha;
					Vector vFechasVencimiento = getFechasVencimiento(sNoCliente, 2,con);
					java.util.Date FechaMin = (java.util.Date)vFechasVencimiento.get(0);
					java.util.Date FechaMax = (java.util.Date)vFechasVencimiento.get(1);
					java.util.Date FechaHoy = new java.util.Date();
					comp_fecha=fechaDocto.compareTo(fechaVenc); //log.debug(comp_fecha);
					if (comp_fecha > 0 && !sdf.format(fechaDocto).equals(sdf.format(fechaVenc)) ) {
						error.append(MSG_ERROR+", la Fecha de Vencimiento "+sdf.format(fechaVenc)+" es menor que la Fecha del Documento "+sdf.format(fechaDocto)+". \n");
						registroOk = ok = false;
					}
					if (!dscto_especial.equals("X")) {
						comp_fecha=fechaVenc.compareTo(FechaMin); //log.debug(comp_fecha);
						if (comp_fecha < 0 && !sdf.format(fechaVenc).equals(sdf.format(FechaMin)) ) {
							error.append(MSG_ERROR+", la Fecha de Vencimiento "+sdf.format(fechaVenc)+", no pasa el par&aacute;metro de los d&iacute;as m&iacute;nimos necesarios "+sdf.format(FechaMin)+", el Documento no puede ser Cargado. \n");
							registroOk = ok = false;
						}
					}
					comp_fecha=fechaVenc.compareTo(FechaMax); //log.debug(comp_fecha);
					if (comp_fecha > 0 && !sdf.format(fechaVenc).equals(sdf.format(FechaMax)) ) {
						error.append(MSG_ERROR+", la Fecha de Vencimiento "+sdf.format(fechaVenc)+", sobrepasa el par&aacute;metro de los dias m&aacute;ximo necesario "+sdf.format(FechaMax)+", el Documento no puede ser Cargado. \n");
						registroOk = ok = false;
					}
					comp_fecha=fechaDocto.compareTo(FechaHoy); //log.debug(comp_fecha);
					if (comp_fecha > 0 && !sdf.format(fechaDocto).equals(sdf.format(FechaHoy)) ) {
						error.append(MSG_ERROR+", la Fecha del Documento "+sdf.format(fechaDocto)+" es mayor a la Fecha de Hoy "+sdf.format(FechaHoy)+", el Documento no puede ser Cargado. \n");
						registroOk = ok = false;
					}
				}

				/* La Validacion para el Monto. */
				if((fn_monto.equals("")) || (fn_monto.length()==0)) {
					error.append(MSG_ERROR+", el valor del MONTO no puede estar vac&iacute;o, debe de tener un valor. \n");
					registroOk = ok = false;
				} else {
					if(Comunes.esDecimal(fn_monto)==false) {
						error.append(MSG_ERROR+", el valor en el campo MONTO del archivo, no es un valor decimal correcto. \n");
						registroOk = ok = false;
					}
				}

				/* La Validacion del Tipo de Moneda */
				BigDecimal monto=new BigDecimal(fn_monto);	int moneda=0;
				BigDecimal txtmtomindoc=new BigDecimal(ctxtmtomindoc);
				BigDecimal txtmtomaxdoc=new BigDecimal(ctxtmtomaxdoc);
				BigDecimal txtmtomindocdol=new BigDecimal(ctxtmtomindocdol);
				BigDecimal txtmtomaxdocdol=new BigDecimal(ctxtmtomaxdocdol);
				if((ic_moneda.equals("")) || (ic_moneda.length()==0)) {
					error.append(MSG_ERROR+", el valor de la MONEDA no puede estar vac&iacute;o, debe de tener un valor. \n");
					registroOk = ok = false;
				} else {
					if(Comunes.esNumero(ic_moneda)==false) {
						error.append(MSG_ERROR+", el valor en el campo MONEDA del archivo, debe de ser un valor num&eacute;rico. \n");
						registroOk = ok = false;
					} else {
						moneda=Integer.parseInt(ic_moneda);
						if((moneda != 1) && (moneda != 54)) {
							error.append(MSG_ERROR+", en el campo MONEDA el valor "+ic_moneda+", no existe este tipo de moneda por favor verif&iacute;quelo. \n");
							registroOk = ok = false;
						} else {
							if(moneda == 1) {   //Si es Moneda Nacional
								if(monto.compareTo(txtmtomindoc) < 0) {
									error.append(MSG_ERROR+", el Monto M&iacute;nimo de Entrada en M.N. "+txtmtomindoc.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n");
									registroOk = ok = false;
								} else if(monto.compareTo(txtmtomaxdoc) > 0) {
									error.append(MSG_ERROR+", el Monto Maximo de Entrada en M.N. "+txtmtomaxdoc.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n");
									registroOk = ok = false;
								} else if(!dscto_especial.equals("X")) {
									monto_mnNeg = monto_mnNeg.add(monto);
								} else {
									monto_mnNoNeg = monto_mnNoNeg.add(monto);
								}
								monto_mn = monto_mn.add(monto);
								totdoc_mn++;
							}
							if(moneda == 54) {  //Si son Dolares.
								if(monto.compareTo(txtmtomindocdol) < 0) {
									error.append(MSG_ERROR+", el Monto M&iacute;nimo de Entrada en D&oacute;lares "+txtmtomindocdol.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n");
									registroOk = ok = false;
								}
								else if(monto.compareTo(txtmtomaxdocdol) > 0) {
									error.append(MSG_ERROR+", el Monto M&aacute;ximo de Entrada en D&oacute;lares "+txtmtomaxdocdol.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n");
									registroOk = ok = false;
								} else if(!dscto_especial.equals("X")) {
									monto_dolarNeg = monto_dolarNeg.add(monto);
								} else {
									monto_dolarNoNeg = monto_dolarNoNeg.add(monto);
								}
								monto_dolar = monto_dolar.add(monto);
								totdoc_dolar++;
							}
							/* Este es para pasar la moneda sin ceros a la izquierda. */
							ic_moneda = (new Integer(moneda)).toString();
						}
					}
				}

				if((ct_referencia.equals("")) || (ct_referencia.length()==0)) { ct_referencia=null; }

				/* Comparacion de la existencia de los campos din�micos o adicionales en el Archivo. */
				// Revisa si la epo a definido campos dinamicos o adicionales.
//				Vector vCA = getCamposAdicionales(sNoCliente);
				Vector vCA = getCamposAdicionales(sNoCliente,con);
				if( (campo_ad1.length()>0) && (vCA.size()==0) ) {
					if( (dscto_especial != null)  &&  (! dscto_especial.equals("V")) ) {
						error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 1 no est&aacute definido por la EPO, por lo que no debe de tener un valor en el Archivo. \n");
						registroOk = ok = false;
					}
				}
				if((campo_ad2.length()>0) && (vCA.size()<=1)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 2 no est&aacute definido por la EPO, por lo que no debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad3.length()>0) && (vCA.size()<=2)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 3 no est&aacute definido por la EPO, por lo que no debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad4.length()>0) && (vCA.size()<=3)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 4 no est&aacute definido por la EPO, por lo que no debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad5.length()>0) && (vCA.size()<=4)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 5 no est&aacute definido por la EPO, por lo que no debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}

				if((campo_ad1.length()==0) && (vCA.size()>=1) && (pipesporlinea<8)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 1 est&aacute definido por la EPO, por lo que debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad2.length()==0) && (vCA.size()>=2) && (pipesporlinea<9)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 2 est&aacute definido por la EPO, por lo que debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad3.length()==0) && (vCA.size()>=3) && (pipesporlinea<10)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 3 est&aacute definido por la EPO, por lo que debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad4.length()==0) && (vCA.size()>=4) && (pipesporlinea<11)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 4 est&aacute definido por la EPO, por lo que debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}
				if((campo_ad5.length()==0) && (vCA.size()>=5) && (pipesporlinea<12)) {
					error.append(MSG_ERROR+", el Campo Din&aacute;mico No. 5 est&aacute definido por la EPO, por lo que debe de tener un valor en el Archivo. \n");
					registroOk = ok = false;
				}

				String sCampAd1=null, sCampAd2=null, sCampAd3=null, sCampAd4=null, sCampAd5=null;
				if(vCA.size()>=1) {
					String sCampo="";
					int iNoCampo, iLongitud; String sNombreCampo, sTipoDato;
					for(Enumeration e=vCA.elements(); e.hasMoreElements();) {
						Vector vca = (Vector)e.nextElement();
						iNoCampo = Integer.parseInt((String)vca.get(0));
						sNombreCampo = (String)vca.get(1);
						sTipoDato = (String)vca.get(2);
						iLongitud = Integer.parseInt((String)vca.get(3));
						//log.debug(iNoCampo+" "+sNombreCampo+" "+sTipoDato+" "+iLongitud);
						switch(iNoCampo) {
							case 1: sCampo=campo_ad1; sCampAd1=campo_ad1; break;
							case 2: sCampo=campo_ad2; sCampAd2=campo_ad2; break;
							case 3: sCampo=campo_ad3; sCampAd3=campo_ad3; break;
							case 4: sCampo=campo_ad4; sCampAd4=campo_ad4; break;
							case 5: sCampo=campo_ad5; sCampAd5=campo_ad5; break;
						}
						if(!sCampo.equals("")) {
							if(sTipoDato.equalsIgnoreCase("N")) {
								if(Comunes.esNumero(sCampo)==false) {
									error.append(MSG_ERROR+", el Tipo de Dato del Campo Din&aacute;mico No. "+iNoCampo+" "+sNombreCampo+" debe de ser Num&eacute;rico. \n");
									registroOk = ok = false;
								}
							}
							if(iLongitud < sCampo.length()) {
								error.append(MSG_ERROR+", el tama&ntilde;o del Campo Din&aacute;mico No. "+iNoCampo+" "+sNombreCampo+" excede la longitud definida por su epo que es de: "+iLongitud+", verif&iacute;quelo. \n");
								registroOk = ok = false;
							}
						}
					}//for
				}//if

				vecdat.removeAllElements();
				String sIf = (ic_if==0)?"null":String.valueOf(ic_if);
				PorcBeneficiario = (PorcentBenef.equals("0"))?"null":PorcentBenef.abs().toPlainString();
				String sBeneficiario = (ic_beneficiario==0)?"null":String.valueOf(ic_beneficiario);
				sEstatusDocto = (dscto_especial.equals("X"))?"1":"2";
				dscto_especial = (dscto_especial.equals("X"))?"N":dscto_especial;
				if(registroOk) {
/*					boolean bBien = insertaDocumento(ic_proc_docto, no_pyme, numero_docto,
									fecha_docto, fecha_venc, ic_moneda, fn_monto,
									dscto_especial, ct_referencia, sCampAd1,
									sCampAd2, sCampAd3, sCampAd4, sCampAd5, sIf,
									sNoCliente, PorcBeneficiario, sBeneficiario, sEstatusDocto);*/
							String NombreMandate = "";
					insertaDocumento(ic_proc_docto, no_pyme, numero_docto,
									fecha_docto, fecha_venc, ic_moneda, fn_monto,
									dscto_especial, ct_referencia, sCampAd1,
									sCampAd2, sCampAd3, sCampAd4, sCampAd5, sIf,
									sNoCliente, PorcBeneficiario, sBeneficiario, sEstatusDocto,con,NombreMandate);
					bBotonProcesar = true; // Hay datos que procesar
					if(moneda==1) {
						totdoc_mnOK++;
						monto_tot_mnOK = monto_tot_mnOK.add(monto);
						if (sEstatusDocto.equals("1"))
							monto_tot_mnNoNegOK = monto_tot_mnNoNegOK.add(monto);
						else
							monto_tot_mnNegOK = monto_tot_mnNegOK.add(monto);
					}
					else if(moneda==54) {
						totdoc_dolarOK++;
						monto_tot_dolarOK = monto_tot_dolarOK.add(monto);
						if (sEstatusDocto.equals("1"))
							monto_tot_dolarNoNegOK = monto_tot_dolarNoNegOK.add(monto);
						else
							monto_tot_dolarNegOK = monto_tot_dolarNegOK.add(monto);
					}
					sinError.append("Linea: "+iNoLinea+ " No. Documento: "+numero_docto+" - PYME: "+no_pyme+".\n");
				} //ok
				else {
					lineasErrores.append(sLineaDocto.replace('|','@')+"\n");	//Para el contenido del Archivo.
					if(moneda==1) {
						totdoc_mnErr++;
						monto_tot_mnErr = monto_tot_mnErr.add(monto);
					}
					else if(moneda==54) {
						totdoc_dolarErr++;
						monto_tot_dolarErr = monto_tot_dolarErr.add(monto);
					}
				}

			} //if de sLineaDocto > 0
		} // while

		//Comparacion de las Cifras de Control.
		if(totdoc_mn != Integer.parseInt(ctxttotdoc)) {
			errorCifras.append("El Total de Documentos de entrada en M.N. "+ctxttotdoc+" que se introdujo no coincide con el Total de Documentos en M.N. del Archivo que es de: "+totdoc_mn+". \n");
			ok = false;
		}
		if(totdoc_dolar != Integer.parseInt(ctxttotdocdol)) {
			errorCifras.append("El Total de Documentos de entrada en Dolares "+ctxttotdocdol+" que se introdujo no coincide con el Total de Documentos en D&oacute;lares del Archivo que es de: "+totdoc_dolar+". \n");
			ok = false;
		}
		//log.debug("Comparacion de los Montos: "+monto_mn.compareTo(txtmtodoc)+"\n");
		BigDecimal txtmtodoc = new BigDecimal(ctxtmtodoc);
		if((monto_mn.compareTo(txtmtodoc)< 0) || (monto_mn.compareTo(txtmtodoc)> 0) ) {
			errorCifras.append("El Monto Total de Documentos de entrada en M.N. "+txtmtodoc.toPlainString()+" no coincide con el Monto del Archivo de Carga que es de: "+Comunes.formatoDecimal(monto_mn,2,false)+". \n");
			ok = false;
		}
		BigDecimal txtmtodocdol = new BigDecimal(ctxtmtodocdol);
		if((monto_dolar.compareTo(txtmtodocdol)< 0) || (monto_dolar.compareTo(txtmtodocdol)> 0) ) {
			errorCifras.append("El Monto Total de Documentos de entrada en D&oacute;lares "+txtmtodocdol.toPlainString()+" no coincide con el Monto del Archivo de Carga que es de: "+Comunes.formatoDecimal(monto_dolar,2,false)+". \n");
			ok = false;
		}
		// Registro en  todas las epos que han cargado registros con errores.
		String sNumBitacora="";	boolean bBitacoraOk=true;
		if(totdoc_mnErr>0) {
			sNumBitacora = getNumMaxBitacora(sNoCliente);
			bBitacoraOk = insertaErroresDoctosBitacora(sNumBitacora, sNoCliente, 1, totdoc_mnErr, monto_tot_mnErr.toPlainString());
		}
		if(totdoc_dolarErr>0) {
			sNumBitacora = getNumMaxBitacora(sNoCliente);
			bBitacoraOk = insertaErroresDoctosBitacora(sNumBitacora, sNoCliente, 54, totdoc_dolarErr, monto_tot_dolarErr.toPlainString());
		}
	}
	catch(SQLException sqle) { log.error("El Mensaje: "+sqle.getMessage()); }
	catch(ArrayIndexOutOfBoundsException aioobe) { log.error(aioobe); }
	catch(NullPointerException npe) {
		error.append("Error Grave el Archivo no cumple con el Formato Requerido, por favor reviselo. \n");
		log.error("Exception en procesarDocumentos: "+npe);
	}
	catch(Exception e) {
		log.error("Exception en procesarDocumentos. "+e);
		throw new NafinException("SIST0001");
	}
	finally {
		if(con.hayConexionAbierta()) con.cierraConexionDB();
		//Se Pone los resultados de la carga en un Hashtable.
		hResultados.put("ok", new Boolean(ok));
		hResultados.put("error", error.toString());
		hResultados.put("bBotonProcesar", new Boolean(bBotonProcesar));
		hResultados.put("monto_tot_mnOK", monto_tot_mnOK.toPlainString());
		hResultados.put("monto_tot_dolarOK", monto_tot_dolarOK.toPlainString());
		hResultados.put("monto_tot_mnErr", monto_tot_mnErr.toPlainString());
		hResultados.put("monto_tot_dolarErr", monto_tot_dolarErr.toPlainString());
		hResultados.put("sinError", sinError.toString());
		hResultados.put("lineasErrores", lineasErrores.toString());
		hResultados.put("errorCifras", errorCifras.toString());
		hResultados.put("totdoc_mnOK", new Integer(totdoc_mnOK));
		hResultados.put("totdoc_dolarOK", new Integer(totdoc_dolarOK));
		hResultados.put("totdoc_mnErr", new Integer(totdoc_mnErr));
		hResultados.put("totdoc_dolarErr", new Integer(totdoc_dolarErr));
		hResultados.put("monto_mnNeg", monto_mnNeg.toPlainString());
		hResultados.put("monto_mnNoNeg", monto_mnNoNeg.toPlainString());
		hResultados.put("monto_dolarNeg", monto_dolarNeg.toPlainString());
		hResultados.put("monto_dolarNoNeg", monto_dolarNoNeg.toPlainString());
		hResultados.put("monto_tot_mnNegOK", monto_tot_mnNegOK.toPlainString());
		hResultados.put("monto_tot_mnNoNegOK", monto_tot_mnNoNegOK.toPlainString());
		hResultados.put("monto_tot_dolarNegOK", monto_tot_dolarNegOK.toPlainString());
		hResultados.put("monto_tot_dolarNoNegOK", monto_tot_dolarNoNegOK.toPlainString());
	}
	return hResultados;
	}
    
    private String convertStringToHex(String str) {
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int)chars[i]));
        }
        return hex.toString();
    }
    
	/**
	 * Realiza la carga de documentos.
	 * El proceso en la primera etapa, recibe el archivo (cadena) y
	 * verifica layout y que no haya lineas duplicadas.
	 * La segunda etapa es la carga en N@E pero �nicamente de aquellos
	 * documentos que cumplan con las reglas de negocio establecidas por Nafin
	 * (Se emplea un enlace)
	 * @param claveEPO Clave de la EPO que envia los documentos
	 * @param usuario Clave del usuario en N@E
	 * @param password Password del usuario en N@E
	 * @param cadenaArchivo Cadena con el contenido del archivo
	 * @param pkcs7 Cadena con el contenido del documento firmado
	 * @param String serial Serial del certificado.
	 *
	 * @return CargaDocumentoWSInfo que contiene informaci�n acerca de la
	 * 		ejecuci�n del proceso y de la carga de documentos
	 *		Para el estatus del proceso:
	 * 			1.-Sin errores
	 * 			2.-Error en el proceso
	 *		Para el estatus de la carga:
	 *			1.-Sin errores.
	 * 			2.-Carga con errores
	 * 			3.-Sin registros por procesar
   *   Modificado por EGB FODEA 031-2009 INTEGRAR AL WEBSERVICE LA PUBLICACION PEF
	 */

	public CargaDocumentoWSInfo procesarDocumentosWS(
			String claveEPO, String usuario, String password,
			String cadenaArchivo, String pkcs7, String serial) {

		log.info("CargaDocumento::procesarDocumentosWS(E)");

		boolean error = false;
		String receipt = null;
		AccesoDB con = new AccesoDB();
		boolean errorCarga = false;
		StringBuffer resumenEjecucion = null;
		int estatusCarga = 3;
		String tituloMensaje = "Ejecucion procesarDocumentosWS";
		String csCaracterEspecial = "N";

		SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");

		CargaDocumentoWSInfo info = new CargaDocumentoWSInfo();
		EpoPEF epoPEF = new EpoPEF();
	    String acuse ="";
	    StringBuffer qrySQL = new StringBuffer ();
		List lVarBind		= new ArrayList();
		
		
		try {
			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

			con.conexionDB();

			log.info("procesarDocumentosWS::Validando claveEPO/usuario");

			if (!error && !cuentasEPO.contains(usuario)) {
				error = true;
				info.setCodigoEjecucion(new Integer(2)); //Error del proceso
				info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
				info.setResumenEjecucion("El usuario no existe para la EPO especificada");
			}

			log.info("procesarDocumentosWS::Validando usuario/passwd");
			if (!error && !utilUsr.esUsuarioValido(usuario, password)) {
				error = true;
				info.setCodigoEjecucion(new Integer(2)); //Error del proceso
				info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
				info.setResumenEjecucion("Fallo la autenticaci�n del usuario");
			}

			if (!error) {
				log.info("procesarDocumentosWS::Validando que el certificado sea del usuario especificado");
				String qryTraeSerial =
						" SELECT dn_user " +
						" FROM users_seguridata "+
						" WHERE UPPER(trim(table_uid)) = UPPER(?) ";
				PreparedStatement psSerial = con.queryPrecompilado(qryTraeSerial);
				psSerial.setString(1, usuario);
				ResultSet rsSerial = psSerial.executeQuery();
				String strSerial = "";
				if(rsSerial.next()) {
					strSerial = rsSerial.getString(1).trim();
				}
				rsSerial.close();
				psSerial.close();
                
                log.trace("serial:"+serial.length());
			    if(serial.length()==20) {
                    serial = convertStringToHex(serial);
                }
			    log.trace("serial:"+serial.length());
			    log.trace("serial:"+serial);
			    log.trace("strSerial:"+strSerial);
                
				if (!serial.equals(strSerial)) {
					error = true;
					info.setCodigoEjecucion(new Integer(2)); //Error del proceso
					info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
					info.setResumenEjecucion("El certificado no corresponde al usuario");
				}
			}


			if (!error) {

				log.info("procesarDocumentosWS::Validando firma mensaje");

				Seguridad s = new Seguridad();
				char getReceipt = 'Y';


				String folio = claveEPO + "WS" + String.valueOf(new java.util.Date().getTime());


				boolean autenticarMensaje =
						s.autenticar(folio, serial, pkcs7,
								cadenaArchivo, getReceipt);

				if (autenticarMensaje) {
					receipt = s.getAcuse();
				} else {
					log.info(s.mostrarError() +"folio = "+folio);
					error = true;
					info.setCodigoEjecucion(new Integer(2)); //Error del proceso
					info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
					info.setResumenEjecucion("Fallo la autenticaci�n del mensaje");
				}
			}


			errorCarga = false;
			resumenEjecucion = new StringBuffer();

			if (!error) {
				log.info("procesarDocumentosWS::Inicia proceso de archivo");

				log.debug("Depuracion comtmp_doctos_pub_ws. ic_epo = " + claveEPO);
				String strSQLDelete = 
						" DELETE FROM comtmp_doctos_pub_ws " +
						" WHERE ic_epo = ?";
				PreparedStatement psDelete = con.queryPrecompilado(strSQLDelete);
				psDelete.setInt(1, Integer.parseInt(claveEPO));
				psDelete.executeUpdate();
				psDelete.close();
				
				log.debug("Depuracion comtmp_doctos_err_pub_ws. ic_epo = " + claveEPO);
				String strSQLDeleteErr = 
						" DELETE FROM com_doctos_err_pub_ws " +
						" WHERE ic_epo = ?";
				PreparedStatement psDeleteErr = con.queryPrecompilado(strSQLDeleteErr);
				psDeleteErr.setInt(1, Integer.parseInt(claveEPO));
				psDeleteErr.executeUpdate();
				psDeleteErr.close();
				con.terminaTransaccion(true);

				List registros = new VectorTokenizer(cadenaArchivo, "\n").getValuesVector();
				String strSQL = "INSERT INTO comtmp_doctos_pub_ws (" +
						" ic_epo, ig_numero_docto, " +
						" cg_pyme_epo_interno, " +
						" df_fecha_docto, df_fecha_venc, " +
						" ic_moneda, fn_monto, cs_dscto_especial, " +
						" ct_referencia, cg_campo1, cg_campo2, " +
						" cg_campo3, cg_campo4, cg_campo5, " +
						" ic_nafin_electronico, cg_receipt "+epoPEF.getCamposPEF()+", cg_clave_mandante, cs_caracter_especial , CS_CADENA_ORIGINAL) " +
						" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ? ) ";
				PreparedStatement ps = con.queryPrecompilado(strSQL);

				for(int i = 0; i < registros.size(); i++){
					String registroEnTurno = (String)registros.get(i);
					if (registroEnTurno.trim().equals("")) {
						//Linea vacia... Ignorar
						continue;
					}
					
					if(Comunes.tieneCaracterControlNoPermitidoEnXML1(registroEnTurno)){
						csCaracterEspecial = "S";
					}
					
					List camposRegistroEnTurno =
							new VectorTokenizer(registroEnTurno, "|").getValuesVector();

					if(camposRegistroEnTurno.size() < 7) {
						errorCarga = true; //con errores
						resumenEjecucion.append("Linea " + (i + 1) + ":" +
								"El registro no contiene el numero minimo de campos" + "\n");
						continue;
					}

					String cg_pyme_epo_interno = (camposRegistroEnTurno.size()>= 1)?((String)camposRegistroEnTurno.get(0)).replace('\'',' ').replace('"',' ').trim():"";
					String numero_docto	= (camposRegistroEnTurno.size()>= 2)?((String)camposRegistroEnTurno.get(1)).replace('\'',' ').replace('"',' ').trim():"";
					String fecha_docto = (camposRegistroEnTurno.size()>= 3)?((String)camposRegistroEnTurno.get(2)).replace('\'',' ').replace('"',' ').trim():"";
					String fecha_venc = (camposRegistroEnTurno.size()>= 4)?((String)camposRegistroEnTurno.get(3)).replace('\'',' ').replace('"',' ').trim():"";
					String ic_moneda = (camposRegistroEnTurno.size()>= 5)?((String)camposRegistroEnTurno.get(4)).replace('\'',' ').replace('"',' ').trim():"";
					String fn_monto = (camposRegistroEnTurno.size()>= 6)?((String)camposRegistroEnTurno.get(5)).replace('\'',' ').replace('"',' ').trim():"";
					String dscto_especial = (camposRegistroEnTurno.size()>= 7)?((String)camposRegistroEnTurno.get(6)).replace('\'',' ').replace('"',' ').trim():"";
					String ct_referencia = (camposRegistroEnTurno.size()>= 8)?((String)camposRegistroEnTurno.get(7)).replace('\'',' ').replace('"',' ').trim():"";
					String campo_ad1 = (camposRegistroEnTurno.size()>= 9)?((String)camposRegistroEnTurno.get(8)).replace('\'',' ').replace('"',' ').trim():"";
					String campo_ad2 = (camposRegistroEnTurno.size()>= 10)?((String)camposRegistroEnTurno.get(9)).replace('\'',' ').replace('"',' ').trim():"";
					String campo_ad3 = (camposRegistroEnTurno.size()>= 11)?((String)camposRegistroEnTurno.get(10)).replace('\'',' ').replace('"',' ').trim():"";
					String campo_ad4 = (camposRegistroEnTurno.size()>= 12)?((String)camposRegistroEnTurno.get(11)).replace('\'',' ').replace('"',' ').trim():"";
					String campo_ad5 = (camposRegistroEnTurno.size()>= 13)?((String)camposRegistroEnTurno.get(12)).replace('\'',' ').replace('"',' ').trim():"";

					String ic_nafin_electronico_if = (camposRegistroEnTurno.size()>= 14)?((String)camposRegistroEnTurno.get(13)).replace('\'',' ').replace('"',' ').trim():"";

					String fecha_entrega = (camposRegistroEnTurno.size()>= 15)?((String)camposRegistroEnTurno.get(14)).replace('\'',' ').replace('"',' ').trim():"";
					String tipo_compra = (camposRegistroEnTurno.size()>= 16)?((String)camposRegistroEnTurno.get(15)).replace('\'',' ').replace('"',' ').trim():"";
					String clave_presupuestaria = (camposRegistroEnTurno.size()>= 17)?((String)camposRegistroEnTurno.get(16)).replace('\'',' ').replace('"',' ').trim():"";
					String periodo = (camposRegistroEnTurno.size()>= 18)?((String)camposRegistroEnTurno.get(17)).replace('\'',' ').replace('"',' ').trim():"";
					String claveMandante = (camposRegistroEnTurno.size()>= 19)?((String)camposRegistroEnTurno.get(18)).replace('\'',' ').replace('"',' ').trim():"";

					try {

						ps.clearParameters();
						ps.setInt(1, Integer.parseInt(claveEPO));
						ps.setString(2, numero_docto);
						ps.setString(3, cg_pyme_epo_interno);

						ps.setDate(4, new java.sql.Date(Comunes.parseDate(fecha_docto).getTime()),Calendar.getInstance());
						ps.setDate(5, new java.sql.Date(Comunes.parseDate(fecha_venc).getTime()),Calendar.getInstance());
						ps.setInt(6, Integer.parseInt(ic_moneda));
						ps.setDouble(7, Double.parseDouble(fn_monto));
						ps.setString(8, dscto_especial);

						if (ct_referencia.equals("")) {
							ps.setNull(9, java.sql.Types.VARCHAR);
						} else {
							ps.setString(9, ct_referencia);
						}

						if (campo_ad1.equals("")) {
							ps.setNull(10, java.sql.Types.VARCHAR);
						} else {
							ps.setString(10, campo_ad1);
						}
						if (campo_ad2.equals("")) {
							ps.setNull(11, java.sql.Types.VARCHAR);
						} else {
							ps.setString(11, campo_ad2);
						}
						if (campo_ad3.equals("")) {
							ps.setNull(12, java.sql.Types.VARCHAR);
						} else {
							ps.setString(12, campo_ad3);
						}
						if (campo_ad4.equals("")) {
							ps.setNull(13, java.sql.Types.VARCHAR);
						} else {
							ps.setString(13, campo_ad4);
						}
						if (campo_ad5.equals("")) {
							ps.setNull(14, java.sql.Types.VARCHAR);
						} else {
							ps.setString(14, campo_ad5);
						}
						if (ic_nafin_electronico_if.equals("")) {
							ps.setNull(15, java.sql.Types.NUMERIC);
						} else {
							ps.setInt(15, Integer.parseInt(ic_nafin_electronico_if));
						}

						ps.setString(16, receipt);

						if (fecha_entrega.equals("")) {
							ps.setNull(17, java.sql.Types.DATE);
						} else {
							ps.setDate(17, new java.sql.Date(Comunes.parseDate(fecha_entrega).getTime()),Calendar.getInstance());
						}
						if (tipo_compra.equals("")) {
							ps.setNull(18, java.sql.Types.VARCHAR);
						} else {
							ps.setString(18, tipo_compra);
						}
						if (clave_presupuestaria.equals("")) {
							ps.setNull(19, java.sql.Types.VARCHAR);
						} else {
							ps.setString(19, clave_presupuestaria);
						}
						if (periodo.equals("")) {
							ps.setNull(20, java.sql.Types.NUMERIC);
						} else {
							ps.setInt(20, Integer.parseInt(periodo));
						}
						if (claveMandante.equals("")) {
							ps.setNull(21, java.sql.Types.VARCHAR);
						} else {
							ps.setString(21, claveMandante);
						}

						ps.setString(22, csCaracterEspecial);
						ps.setString(23, registroEnTurno);

						ps.executeUpdate();


					} catch(SQLException sqle){
						String fechaError = formatoHora.format(new java.util.Date());

						log.error(fechaError + "::" + sqle.getMessage());
						//sqle.printStackTrace();
						//Si no se puede insertar en la tabla
						error = true; //con errores
						resumenEjecucion.append("Linea " + (i + 1) + ":" +
								"Verifique layout del registro y asegurese " +
								"que el numero de documento no se repita." +
								" (" + fechaError + ")" + "\n");
					}
				}//fin for
				ps.close();

				//Si alguno de los registros del archivo no se pudo insertar en
				//la tabla temporal de publicaci�n de documentos, entonces se
				//realiza un rollback sobre toda la operaci�n.
				if (error) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}

				log.info("...");
				log.info("...");
				log.info("...");
				log.info("procesarDocumentosWS::Finaliza proceso de archivo");
			   
				if (!error) {

					log.info("...");
					log.info("...");
					log.info("...");
					log.info("procesarDocumentosWS::Inicia proceso de carga de documentos");

					this.proceso(claveEPO, new WSEnlace(claveEPO, receipt, usuario));

					String strSQLAcuse =
							" SELECT cc_acuse, in_total_proc, " +
							" 	fn_total_monto_mn, in_total_acep_mn, in_total_rech_mn, " +
							" 	fn_total_monto_dl, in_total_acep_dl, in_total_rech_dl, " +
							" 	df_fechahora_carga, cs_estatus " +
							" FROM com_doctos_pub_acu_ws " +
							" WHERE ic_epo = ? " +
							" 	AND cg_receipt = ? ";
					PreparedStatement psAcuse = con.queryPrecompilado(strSQLAcuse);

					psAcuse.setInt(1, Integer.parseInt(claveEPO));
					psAcuse.setString(2, receipt);
					ResultSet rsAcuse = psAcuse.executeQuery();
					StringBuffer sbAcuse = new StringBuffer();
				  
					while (rsAcuse.next()) {
						 acuse = rsAcuse.getString("cc_acuse");
						String totalProcesados = rsAcuse.getString("in_total_proc");
						String totalMontoMN = rsAcuse.getString("fn_total_monto_mn");
						String totalAceptadosMN = rsAcuse.getString("in_total_acep_mn");
						String totalRechazadosMN = rsAcuse.getString("in_total_rech_mn");
						String totalMontoDL = rsAcuse.getString("fn_total_monto_dl");
						String totalAceptadosDL = rsAcuse.getString("in_total_acep_dl");
						String totalRechazadosDL = rsAcuse.getString("in_total_rech_dl");
						String fechaHoraCarga = rsAcuse.getString("df_fechahora_carga");
						estatusCarga = Integer.parseInt(rsAcuse.getString("cs_estatus"));


						sbAcuse.append(
								"Acuse," + acuse + "\n" +
								"Numero Total de Documentos Procesados," + totalProcesados + "\n" +
								"Monto Total M.N.," + totalMontoMN + "\n" +
								"Numero Total de Documentos Aceptados M.N.," + totalAceptadosMN + "\n" +
								"Numero Total de Documentos Rechazados M.N.," + totalRechazadosMN + "\n" +
								"Monto Total DL," + totalMontoDL + "\n" +
								"Numero Total de Documentos Aceptados DL," + totalAceptadosDL + "\n" +
								"Numero Total de Documentos Rechazados DL," + totalRechazadosDL + "\n" +
								"Fecha y hora de carga," + fechaHoraCarga + "\n" );
					}
					rsAcuse.close();
					psAcuse.close();

					if (sbAcuse.length() > 0 ) {
						resumenEjecucion.append(sbAcuse + "\n\n\n");
					}

				//Se Verifica la parametrizaci�n de la EPO con respuesto a mostrar la cadena original 		   
				qrySQL = new StringBuffer ();
				qrySQL.append(" SELECT cg_valor FROM com_parametrizacion_epo ");
				qrySQL.append(" WHERE cc_parametro_epo = 'CADENA_ORIGINAL_WS' ");
				qrySQL.append(" AND  ic_epo =  ?  ");
				
				lVarBind		= new ArrayList();
			    lVarBind.add(claveEPO);   
				String paramCadenaOrigWS ="";
	
				PreparedStatement ps1 = con.queryPrecompilado(qrySQL.toString(),lVarBind );
				ResultSet rs = ps1.executeQuery();
				if(rs.next()){
					paramCadenaOrigWS = (rs.getString("cg_valor")==null)?"N":rs.getString("cg_valor");
				}
				rs.close();
				ps1.close();



					String strSQLError =
							" SELECT ig_numero_docto, ig_numero_error, " +
							" cg_error, cg_digito_identificador, CS_CADENA_ORIGINAL " +
							" FROM com_doctos_err_pub_ws " +
							" WHERE ic_epo = ? " +
							" 	AND cg_receipt = ? ";
					PreparedStatement psError = con.queryPrecompilado(strSQLError);

					psError.setInt(1, Integer.parseInt(claveEPO));
					psError.setString(2, receipt);
					ResultSet rsError = psError.executeQuery();
					StringBuffer sbError = new StringBuffer();
					String digitoIdentificador = "";
					String cadOriginal = "";
					while (rsError.next()) {
						String numDocto = rsError.getString("ig_numero_docto");
						String codigoError = rsError.getString("ig_numero_error");
						String errorDocto = rsError.getString("cg_error");
						digitoIdentificador = (rsError.getString("CG_DIGITO_IDENTIFICADOR")==null)?"":rsError.getString("CG_DIGITO_IDENTIFICADOR");
						cadOriginal = (rsError.getString("CS_CADENA_ORIGINAL")==null)?"":rsError.getString("CS_CADENA_ORIGINAL");

						errorCarga = true; //con errores
						sbError.append(numDocto + "," +
								codigoError + "," +
								errorDocto.replace(',',' ') +
								((!"".equals(digitoIdentificador))?","+digitoIdentificador:"")+
								(("S".equals(paramCadenaOrigWS))?","+cadOriginal:"")+
								"\n");
					}
					rsError.close();
					psError.close();

					if (sbError.length() > 0 ) {
						resumenEjecucion.append(
								"Numero de Documento, Codigo Error, " +
								"Descripcion Error" +
								((!"".equals(digitoIdentificador))?", D�gito Identificador":"")+
								(("S".equals(paramCadenaOrigWS))?", Cadena Original":"")+
								"\n" +
								sbError + "\n\n\n");
					}

					log.info("...");
					log.info("...");
					log.info("...");
					log.info("procesarDocumentosWS::Finaliza proceso de carga de documentos");
				}

				info.setCodigoEjecucion((!error)?new Integer(1):new Integer(2));
				info.setEstatusCarga(new Integer(estatusCarga));
				info.setResumenEjecucion(resumenEjecucion.toString());


        //consu.ta para registros epndientes de revisar duplicidad
        /*if(publicarDocDuplicados(claveEPO)){
          enviaCorreoDoctosDuplicados(claveEPO,receipt);
        }*/
			
		  CrearArchivosAcuses  arc = new CrearArchivosAcuses();
		  
			 arc.setAcuse(acuse );
			 arc.setUsuario(usuario );
			 arc.setTipoUsuario("EPO");
			 arc.setVersion("WEBSERVICE"); 
			 
			try {
			 //Crea y Guarda el XML de respuesta 
			 arc.setCodigoEjecucion( String.valueOf(info.getCodigoEjecucion())  );
			 arc.setEstatusCarga(  String.valueOf(estatusCarga)   );
			 arc.setResumenEjecucion( resumenEjecucion.toString()    );
			 arc.guardarArchivo();
			 
			} catch (Exception exception) {
				 java.io.StringWriter outSW = new java.io.StringWriter();
				 exception.printStackTrace(new java.io.PrintWriter(outSW));
				 String stackTrace = outSW.toString();              
				 arc.setDesError(stackTrace);  
				 arc.guardaBitacoraArch();          
			 
			}
			
				
			}

		} catch(Exception e) {
			info.setCodigoEjecucion(new Integer(2)); //Error del proceso
			info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
			String fechaError = formatoHora.format(new java.util.Date());
			log.error("CargaDocumentoBean::procesarDocumentosWS()::" +
					"ERROR INESPERADO " + fechaError);
			info.setResumenEjecucion("ERROR INESPERADO " + fechaError);
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}



		try { //Si el env�o del correo falla se ignora.
			String to = this.getCuentasWS(claveEPO);
			if (to != null) {
				String from = this.getCuentaWSFrom();
				Correo.enviarMensajeTexto(to, from, tituloMensaje, "("+claveEPO+" - "+usuario+")\n"+info.toString());
			}
		} catch(Throwable t) {
			log.error("Error al enviar correo " + t.getMessage());
		}

		log.info("CargaDocumento::procesarDocumentosWS(S)");
		return info;
	}

	private void validaFechaVencimiento(String fecha_venc,String sNoCliente,AccesoDB con)
		throws NafinException{
		log.info("CargaDocumentoBean::validaFechaVencimiento (E)");
		try{
			Calendar dia_fv = new GregorianCalendar();
			java.util.Date fechaVenc=Comunes.parseDate(fecha_venc);
			String sLineaDiaMes = getDiasInhabiles(sNoCliente,con);
			if (sLineaDiaMes.length() > 0) {
				StringTokenizer st = new StringTokenizer(sLineaDiaMes,";");
				String sDiaMesInhabil = "";
				while(st.hasMoreElements()) {
					sDiaMesInhabil = st.nextToken();
					//log.debug(sDiaMesInhabil);
					if(fecha_venc.substring(0,5).equals(sDiaMesInhabil)) {
						throw new NafinException("DIST0010");
					}
				}
			}
			/* Validacion de que la fecha de Vencimiento que no sea Sabado o Domingo. */
			dia_fv.setTime(fechaVenc);
			int no_dia_semana=dia_fv.get(Calendar.DAY_OF_WEEK);
			if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
				throw new NafinException("DIST0010");
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.error("CargaDocumentoBean::validaFechaVencimiento Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			log.info("CargaDocumentoBean::validaFechaVencimiento (S)");
		}

	}

	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto, String NombreMandate, String duplicado ) throws NafinException {

		log.info("CargaDocumentoBean::insertaDocumento (E)");

		log.debug("CargaDocumentoBean::---------------------");
		log.debug("CargaDocumentoBean::-NombreMandate::----"+NombreMandate);

		boolean bBien = true;
		try {
			bBien = insertaDocumento(
						sProcDocto,  	sIcPyme,  			sIgNumeroDocto,
						sDfFechaDocto,  sDfFechaVenc,  		sIcMoneda,
						sFnMonto,  		sCsDsctoEspecial,  	sCtReferencia,
						sCampAd1,  		sCampAd2,  			sCampAd3,
						sCampAd4,  		sCampAd5,  			sIf,
						sNoCliente,		sPorcBeneficiario,  sBeneficiario,
						sEstatusDocto,NombreMandate, "");

		}catch(Exception e){
			bBien = false;
			log.error("CargaDocumentoBean::validaFechaVencimiento Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			log.info("CargaDocumentoBean::validaFechaVencimiento (S)");
		}
		return bBien;
	}

	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto,	String sDfFechaVencPyme, String NombreMandate, String duplicado) throws NafinException {
		log.info("CargaDocumentoBean::insertaDocumento(E)");



		log.debug("CargaDocumentoBean::---------------2------");
		log.debug("CargaDocumentoBean::-NombreMandate::--2--"+NombreMandate);

		boolean bBien = true;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			sCtReferencia=(sCtReferencia==null || sCtReferencia.equals(""))?"NULL":"'"+sCtReferencia+"'";
			sCampAd1=(sCampAd1==null || sCampAd1.equals(""))?"NULL":"'"+sCampAd1+"'";
			sCampAd2=(sCampAd2==null || sCampAd2.equals(""))?"NULL":"'"+sCampAd2+"'";
			sCampAd3=(sCampAd3==null || sCampAd3.equals(""))?"NULL":"'"+sCampAd3+"'";
			sCampAd4=(sCampAd4==null || sCampAd4.equals(""))?"NULL":"'"+sCampAd4+"'";
			sCampAd5=(sCampAd5==null || sCampAd5.equals(""))?"NULL":"'"+sCampAd5+"'";

			String df_fecha_venc = "NULL";
			String df_fecha_venc_pyme = "NULL";
			if("C".equals(sCsDsctoEspecial)) {
				df_fecha_venc 		= "NULL";
				df_fecha_venc_pyme 	= "NULL";
			} else {
				df_fecha_venc 		= "TO_DATE('"+sDfFechaVenc+"','DD/MM/YYYY')";
				if(!"".equals(sDfFechaVencPyme))
					df_fecha_venc_pyme 	= "TO_DATE('"+sDfFechaVencPyme+"','DD/MM/YYYY')";
			}

			String query =
				" INSERT INTO comtmp_proc_docto ("   +
				"              ic_consecutivo, ic_proc_docto, ic_pyme, ig_numero_docto,"   +
				"              df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto,"   +
				"              cs_dscto_especial, fn_monto_dscto, ic_estatus_docto,"   +
				"              ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				"              cg_campo5, ic_if, ic_epo, fn_porc_beneficiario, ic_beneficiario,"   +
				"              df_fecha_venc_pyme, cg_duplicado )"   +
				"      VALUES ("   +
				"              seq_comtmp_proc_ic_consec.NEXTVAL, "+sProcDocto+", "+sIcPyme+", '"+sIgNumeroDocto+"',"   +
				"              TO_DATE('"+sDfFechaDocto+"','DD/MM/YYYY'), "+df_fecha_venc+", "+sIcMoneda+", "+sFnMonto+","   +
				"              '"+sCsDsctoEspecial+"', NULL, "+sEstatusDocto+","   +
				"              "+sCtReferencia+", "+sCampAd1+", "+sCampAd2+", "+sCampAd3+", "+sCampAd4+","   +
				"              "+sCampAd5+", "+sIf+", "+sNoCliente+", "+sPorcBeneficiario+", "+sBeneficiario+","   +
				"              "+df_fecha_venc_pyme+", '"+duplicado+"' )"  ;
			//log.debug(query);
			if(!"C".equals(sCsDsctoEspecial))
				validaFechaVencimiento(sDfFechaVenc,sNoCliente,con);
			try{

				log.debug("query::--"+query);
				con.ejecutaSQL(query);


			}catch(SQLException sqle) {
				throw new NafinException("DSCT0044");
			}
		}catch(NafinException ne) {
			throw ne;
		}catch(Exception e) {
			e.printStackTrace();
			log.error("CargaDocumentoBean::insertaDocumento(Exception) "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bBien);
			con.cierraConexionDB();
			log.info("CargaDocumentoBean::insertaDocumento(S)");
		}
		return bBien;
	}

	//FODEA 050 - VALC - 10/2008
	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto,	String sDfFechaVencPyme,
									String sDfEntrega,	String sTipoCompra,	String sClavePresupuestaria,
									String sPeriodo, String NombreMandate, String duplicado ) throws NafinException {

		log.info("CargaDocumentoBean::insertaDocumento(E)");

		log.debug("CargaDocumentoBean::---------------3------");
		log.debug("CargaDocumentoBean::-NombreMandate::--3--"+NombreMandate);

		String Mandante = "";
		if (NombreMandate.equals("")) {
		 Mandante = "NULL";
		}else {
		 Mandante = NombreMandate;
		}

		boolean bBien = true;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			sCtReferencia=(sCtReferencia==null || sCtReferencia.equals(""))?"NULL":"'"+sCtReferencia+"'";
			sCampAd1=(sCampAd1==null || sCampAd1.equals(""))?"NULL":"'"+sCampAd1+"'";
			sCampAd2=(sCampAd2==null || sCampAd2.equals(""))?"NULL":"'"+sCampAd2+"'";
			sCampAd3=(sCampAd3==null || sCampAd3.equals(""))?"NULL":"'"+sCampAd3+"'";
			sCampAd4=(sCampAd4==null || sCampAd4.equals(""))?"NULL":"'"+sCampAd4+"'";
			sCampAd5=(sCampAd5==null || sCampAd5.equals(""))?"NULL":"'"+sCampAd5+"'";

			String df_fecha_entrega		= "NULL";
			if(!"".equals(sDfEntrega))
				df_fecha_entrega= "TO_DATE('"+sDfEntrega+"','DD/MM/YYYY')";
			sTipoCompra=(sTipoCompra==null || sTipoCompra.equals(""))?"NULL":"'"+sTipoCompra+"'";
			sClavePresupuestaria=(sClavePresupuestaria==null || sClavePresupuestaria.equals(""))?"NULL":"'"+sClavePresupuestaria+"'";
			sPeriodo=(sPeriodo==null || sPeriodo.equals(""))?"NULL":sPeriodo;

			String df_fecha_venc = "NULL";
			String df_fecha_venc_pyme = "NULL";
			if("C".equals(sCsDsctoEspecial)) {
				df_fecha_venc 		= "NULL";
				df_fecha_venc_pyme 	= "NULL";
			} else {
				df_fecha_venc 		= "TO_DATE('"+sDfFechaVenc+"','DD/MM/YYYY')";
				if(!"".equals(sDfFechaVencPyme))
					df_fecha_venc_pyme 	= "TO_DATE('"+sDfFechaVencPyme+"','DD/MM/YYYY')";
			}

			String query =
				" INSERT INTO comtmp_proc_docto ("   +
				"              ic_consecutivo, ic_proc_docto, ic_pyme, ig_numero_docto,"   +
				"              df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto,"   +
				"              cs_dscto_especial, fn_monto_dscto, ic_estatus_docto,"   +
				"              ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				"              cg_campo5, ic_if, ic_epo, fn_porc_beneficiario, ic_beneficiario,"   +
				"              df_fecha_venc_pyme, df_entrega, cg_tipo_compra, cg_clave_presupuestaria, cg_periodo, ic_mandante, cg_duplicado )"   +
				"      VALUES ("   +
				"              seq_comtmp_proc_ic_consec.NEXTVAL, "+sProcDocto+", "+sIcPyme+", '"+sIgNumeroDocto+"',"   +
				"              TO_DATE('"+sDfFechaDocto+"','DD/MM/YYYY'), "+df_fecha_venc+", "+sIcMoneda+", "+sFnMonto+","   +
				"              '"+sCsDsctoEspecial+"', NULL, "+sEstatusDocto+","   +
				"              "+sCtReferencia+", "+sCampAd1+", "+sCampAd2+", "+sCampAd3+", "+sCampAd4+","   +
				"              "+sCampAd5+", "+sIf+", "+sNoCliente+", "+sPorcBeneficiario+", "+sBeneficiario+","   +
				"              "+df_fecha_venc_pyme+","+df_fecha_entrega+", "+sTipoCompra+", "+sClavePresupuestaria+", "+
									sPeriodo+","+Mandante+",'"+duplicado+"')"  ;

				log.debug(query);

			if(!"C".equals(sCsDsctoEspecial)){
				validaFechaVencimientoXanio(sDfFechaVenc,sNoCliente,con);
				validaFechaVencimiento(sDfFechaVenc,sNoCliente,con);
			}
			try{
				con.ejecutaSQL(query);
			}catch(SQLException sqle) {
				throw new NafinException("DSCT0044");
			}
		}catch(NafinException ne) {
			throw ne;
		}catch(Exception e) {
			e.printStackTrace();
			log.error("CargaDocumentoBean::insertaDocumento(Exception) "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bBien);
			con.cierraConexionDB();
			log.info("CargaDocumentoBean::insertaDocumento(S)");
		}
		return bBien;
	}


	private boolean insertaDocumento(String sProcDocto, String sIcPyme, String sIgNumeroDocto,
									String sDfFechaDocto, String sDfFechaVenc, String sIcMoneda,
									String sFnMonto, String sCsDsctoEspecial, String sCtReferencia,
									String sCampAd1, String sCampAd2, String sCampAd3,
									String sCampAd4, String sCampAd5, String sIf, String sNoCliente,
									String sPorcBeneficiario, String sBeneficiario,
									String sEstatusDocto,AccesoDB con,String NombreMandate) throws NafinException {

		log.debug("CargaDocumentoBean::---------------4------");
		log.debug("CargaDocumentoBean::-NombreMandate::--4--"+NombreMandate);

	boolean bBien = true;
		try{
			sCtReferencia=(sCtReferencia==null || sCtReferencia.equals(""))?"NULL":"'"+sCtReferencia+"'";
			sCampAd1=(sCampAd1==null || sCampAd1.equals(""))?"NULL":"'"+sCampAd1+"'";
			sCampAd2=(sCampAd2==null || sCampAd2.equals(""))?"NULL":"'"+sCampAd2+"'";
			sCampAd3=(sCampAd3==null || sCampAd3.equals(""))?"NULL":"'"+sCampAd3+"'";
			sCampAd4=(sCampAd4==null || sCampAd4.equals(""))?"NULL":"'"+sCampAd4+"'";
			sCampAd5=(sCampAd5==null || sCampAd5.equals(""))?"NULL":"'"+sCampAd5+"'";
			String query = "insert into comtmp_proc_docto(ic_consecutivo, ic_proc_docto, ic_pyme, "+
					"ig_numero_docto, df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto, "+
					"cs_dscto_especial, fn_monto_dscto, ic_estatus_docto, ct_referencia, "+
					"cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, ic_if, ic_epo, "+
					"fn_porc_beneficiario, ic_beneficiario) "+
					"values(seq_comtmp_proc_ic_consec.NEXTVAL, "+sProcDocto+", "+sIcPyme+", '"+sIgNumeroDocto+"', "+
					"TO_DATE('"+sDfFechaDocto+"','DD/MM/YYYY'), TO_DATE('"+sDfFechaVenc+"','DD/MM/YYYY'), "+
					sIcMoneda+", "+sFnMonto+", '"+sCsDsctoEspecial+"', null, "+ sEstatusDocto +", "+sCtReferencia+", "+
					sCampAd1+", "+sCampAd2+", "+sCampAd3+", "+sCampAd4+", "+sCampAd5+", "+sIf+", "+
					sNoCliente+", "+sPorcBeneficiario+", "+sBeneficiario+")";
		//log.debug(query);
			try{ con.ejecutaSQL(query); }
			catch(SQLException sqle) { throw new NafinException("DSCT0044"); }
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en insertaDocumento. "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		}
		finally {
			con.terminaTransaccion(bBien);
		}
	return bBien;
	}

	private String getNumMaxBitacora(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	String sNumBitacora = "";
		try {
			con.conexionDB();
			String sQry = "select max(ic_bitcarga) from com_bitcarga where ic_epo="+sNoCliente;
			try{
				ResultSet rs = con.queryDB(sQry);
				if (rs.next())
					sNumBitacora = new Integer(rs.getInt(1)+1).toString();
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0045"); }
			con.cierraStatement();
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en getNumMaxBitacora. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return sNumBitacora;
	}

	private boolean insertaErroresDoctosBitacora(String sNumBitacora, String sNoCliente, int iMoneda,
												int sTotalDoc, String sMontoTotal) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bBien = true;
		try{
			con.conexionDB();
			String sQry = "insert into com_bitcarga(ic_bitcarga, ic_epo, ic_moneda, "+
							"df_fecha_hora_carga, in_total_rechazados, fn_monto_rechazados) "+
							"values("+sNumBitacora+", "+sNoCliente+", "+iMoneda+", SYSDATE, "+
							sTotalDoc+", "+sMontoTotal+") ";
			con.ejecutaSQL(sQry);
		}
		catch(Exception e) {
			log.error("Exception en insertaErroresDoctosBitacora. "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bBien;
	}


	public boolean borraDoctosCargados(String sProcDoctoConsec, String sOrigen,
										String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bBien = true;
		try{
			con.conexionDB();
			String condicion = (sOrigen.equals("I"))?"ic_consecutivo":"ic_proc_docto";
			String query = "delete from comtmp_proc_docto where "+condicion+" = "+sProcDoctoConsec+
							" and ic_epo = "+sNoCliente;
			try { con.ejecutaSQL(query); }
			catch(SQLException sqle) {
				bBien = false;
				throw new NafinException("DSCT0051");
			}
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en borraDoctosCargados. "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return bBien;
	}

	public boolean borraDoctosCargados(String sProcDoctoConsec, String sOrigen )
									   throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean bBien = true;
			try{
				con.conexionDB();
				String condicion = (sOrigen.equals("I"))?"ic_consecutivo":"ic_carga_docto";
				String query = "delete from comtmp_proc_docto where "+condicion+" = "+sProcDoctoConsec;
				try { con.ejecutaSQL(query); }
				catch(SQLException sqle) {
					bBien = false;
					throw new NafinException("DSCT0051");
				}
			}
			catch(NafinException ne) { throw ne; }
			catch(Exception e) {
				log.error("Exception en borraDoctosCargados. "+e);
				bBien = false;
				throw new NafinException("SIST0001");
			}
			finally {
				con.terminaTransaccion(bBien);
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			}
			return bBien;
	}

	/*public Vector getNumsDoctosCargaMasiva() {
		return ig_nums_doctos;
	}*/

	/* Obtenemos el numero maximo del ic_proc_docto de la tabla comtmp_docto_detalle. */
	/*public String getNumaxProcesoCargaDoctoDetalle() throws NafinException {
	AccesoDB con = new AccesoDB();
	String iProcesoDocto="1";
		try{
			con.conexionDB();
			String query = "select max(ic_proc_docto)+1 from comtmp_docto_detalle";
			ResultSet rs = con.queryDB(query);
			if(rs.next())
				iProcesoDocto = (rs.getString(1)==null)?"1":rs.getString(1);

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getNumaxProcesoCargaDoctoDetalle. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return iProcesoDocto;
	}*/

	/* Obtenemos los campos detalle definidos por la epo. */
	public Vector getCamposDetalle(String sNoCliente) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vcd = null;
	Vector vCamposDetalle = new Vector();
		try{
			con.conexionDB();
			String query = "select ic_no_campo, initcap(cg_nombre_campo), "+
							" cg_tipo_dato, ig_longitud"+
							" from comrel_visor_detalle"+
							" where ic_epo = "+sNoCliente+
							" and ic_producto_nafin = 1"+
							" and ic_no_campo between 1 and 10"+
							" order by ic_no_campo";
			ResultSet rs = con.queryDB(query);
			while (rs.next()) {
				vcd = new Vector();
				vcd.add(rs.getString(1));
				vcd.add(rs.getString(2).trim());
				vcd.add(rs.getString(3).trim());
				vcd.add(rs.getString(4));
				vCamposDetalle.addElement(vcd);
			}
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getCamposDetalle. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vCamposDetalle;
	}

	public Hashtable procesarDocumentosDetalles(String sNoCliente, String sDoctosDetalle) throws NafinException {
	AccesoDB con = new AccesoDB();
	StringBuffer errord = new StringBuffer();
	boolean bOk = true, ok_det = true;
	Hashtable hResultados = new Hashtable();
	Vector vNumsDoctosDet = new Vector();
	VectorTokenizer vtd = null; Vector vecdet = null;
	String MSG_ERROR = "", sLineaDocto = "";
	int iNoLinea = 0;
	try{
		con.conexionDB();
		StringTokenizer stDetalles = new StringTokenizer(sDoctosDetalle,"\n");
		while(stDetalles.hasMoreElements()) {
			iNoLinea++;
			MSG_ERROR="Error en la l&iacute;nea "+iNoLinea;
			ok_det = true;
			sLineaDocto = stDetalles.nextToken().trim();
			if(sLineaDocto.length()==0)
				continue;

			vtd = new VectorTokenizer(sLineaDocto,"|");
			vecdet = vtd.getValuesVector();
			int no_campos_detalle = camposDetalleHabilitados(sNoCliente);
			if(vecdet.size() != (4 + no_campos_detalle)) { // 4 = NumDocto,FechaDocto,Moneda,Consecutivo.
				errord.append(MSG_ERROR +" no coincide el layout. Por favor verifiquelo.\n");
				bOk = ok_det = false;
			} else {
				/*
				if(no_campos_detalle > vecdet.size()) {
					int iNoPipes=no_campos_detalle-vecdet.size();
					errord.append(MSG_ERROR+", debe de poner "+iNoPipes+" separador(es) | si usted no desea poner un valor. \n");
					bOk = ok_det = false;
				}*/

				String ig_numero_docto=(vecdet.size()>=1)?Comunes.quitaComitasSimples((String)vecdet.get(0)).trim():"";
				String df_fecha_docto=(vecdet.size()>=2)?Comunes.quitaComitasSimples((String)vecdet.get(1)).trim():"";
				String ic_moneda=(vecdet.size()>=3)?Comunes.quitaComitasSimples((String)vecdet.get(2)).trim():"";
				String consecutivo=(vecdet.size()>=4)?Comunes.quitaComitasSimples((String)vecdet.get(3)).trim():"";
				String sCampo1=(vecdet.size()>=5)?Comunes.quitaComitasSimples((String)vecdet.get(4)).trim():"";
				String sCampo2=(vecdet.size()>=6)?Comunes.quitaComitasSimples((String)vecdet.get(5)).trim():"";
				String sCampo3=(vecdet.size()>=7)?Comunes.quitaComitasSimples((String)vecdet.get(6)).trim():"";
				String sCampo4=(vecdet.size()>=8)?Comunes.quitaComitasSimples((String)vecdet.get(7)).trim():"";
				String sCampo5=(vecdet.size()>=9)?Comunes.quitaComitasSimples((String)vecdet.get(8)).trim():"";
				String sCampo6=(vecdet.size()>=10)?Comunes.quitaComitasSimples((String)vecdet.get(9)).trim():"";
				String sCampo7=(vecdet.size()>=11)?Comunes.quitaComitasSimples((String)vecdet.get(10)).trim():"";
				String sCampo8=(vecdet.size()>=12)?Comunes.quitaComitasSimples((String)vecdet.get(11)).trim():"";
				String sCampo9=(vecdet.size()>=13)?Comunes.quitaComitasSimples((String)vecdet.get(12)).trim():"";
				String sCampo10=(vecdet.size()>=14)?Comunes.quitaComitasSimples((String)vecdet.get(13)).trim():"";
				//log.debug(ig_numero_docto+" "+consecutivo+" "+sCampo1+" "+sCampo2+" "+sCampo3+" "+sCampo4+" "+sCampo5+" "+sCampo6+" "+sCampo7+" "+sCampo8+" "+sCampo9+" "+sCampo10);
				vecdet.removeAllElements();
				//Se guardan los numeros de Documentos para saber cuantos se cargaron.
				vNumsDoctosDet.add(ig_numero_docto);

				// Validaci�n del N�mero de Documento
				if(ig_numero_docto.equals("")) {
					errord.append(MSG_ERROR+", el valor del N&uacute;mero Documento no puede estar vac&iacute;o, debe de tener un valor. \n");
					bOk = ok_det = false;
				} else if(ig_numero_docto.length()>15) {
					errord.append(MSG_ERROR+", el valor del N&uacute;mero Documento excede la longitud definida por su epo que es de 15. \n");
					bOk = ok_det = false;
				}

				// Validaci�n de la fecha del Documento.
				if((df_fecha_docto.equals("")) || (df_fecha_docto.length()==0)) {
					errord.append(MSG_ERROR+", el valor de la Fecha del Documento no puede estar vac&iacute;o, debe de tener un valor. \n");
					bOk = ok_det = false;
				} else {
					if(!Comunes.checaFecha(df_fecha_docto)) {
						errord.append(MSG_ERROR+", el valor en el campo Fecha del Documento del archivo, no es una fecha correcta. \n");
						bOk = ok_det = false;
					}
				}

				// Validaci�n del tipo de Moneda.
				if((ic_moneda.equals("")) || (ic_moneda.length()==0)) {
					errord.append(MSG_ERROR+", el valor de la Moneda no puede estar vac&iacute;o, debe de tener un valor. \n");
					bOk = ok_det = false;
				} else {
					if(!Comunes.esNumero(ic_moneda)) {
						errord.append(MSG_ERROR+", el valor en el campo Moneda del archivo, debe de ser un valor num&eacute;rico. \n");
						bOk = ok_det = false;
					} else {
						if((Integer.parseInt(ic_moneda) != 1) && (Integer.parseInt(ic_moneda) != 54)) {
							errord.append(MSG_ERROR+", en el campo Moneda el valor "+ic_moneda+", no existe por favor verif&iacute;quelo. \n");
							bOk = ok_det = false;
						}
					}
				}

				// La Validacion del valor del Consecutivo.
				if(consecutivo.equals("") || consecutivo.length()==0) {
					errord.append(MSG_ERROR+", el valor del CONSECUTIVO no puede estar vac&iacute;o, debe de tener un valor. \n");
					bOk = ok_det = false;
				} else {
					if (Comunes.esNumero(consecutivo)==false) {
						errord.append(MSG_ERROR+", el valor del CONSECUTIVO debe de ser Num&eacute;rico. \n");
						bOk = ok_det = false;
					}
				}

				// El noCamposDet va del valor 1 al 10 dependiendo de los campos dinamicos que haya definido la epo.
				Vector noCamposDet = getCamposDetalle(sNoCliente);
				int iNoCampo, iLongitud; String sNombreCampo, sTipoDato, sValCampo="";
				for(int d=0; d<noCamposDet.size(); d++) {
					Vector vcd = (Vector)noCamposDet.get(d);
					iNoCampo = Integer.parseInt((String)vcd.get(0));
					sNombreCampo = (String)vcd.get(1);
					sTipoDato = (String)vcd.get(2);
					iLongitud = Integer.parseInt((String)vcd.get(3));
					//log.debug(iNoCampo+" "+sNombreCampo+" "+sTipoDato+" "+iLongitud);
					switch(iNoCampo) {
						case 1: sValCampo=sCampo1; break;
						case 2: sValCampo=sCampo2; break;
						case 3: sValCampo=sCampo3; break;
						case 4: sValCampo=sCampo4; break;
						case 5: sValCampo=sCampo5; break;
						case 6: sValCampo=sCampo6; break;
						case 7: sValCampo=sCampo7; break;
						case 8: sValCampo=sCampo8; break;
						case 9: sValCampo=sCampo9; break;
						case 10: sValCampo=sCampo10; break;
					}
					if(sTipoDato.equalsIgnoreCase("N") && !sValCampo.equals("")) {
						//log.debug("Valor del campo actual: "+sValCampo);
						if (Comunes.esDecimal(sValCampo)==false) {
							errord.append(MSG_ERROR+", el Tipo de Dato del Campo "+sNombreCampo+" debe de ser Num&eacute;rico. \n");
							bOk = ok_det = false;
						}
					}
					if(iLongitud < sValCampo.length()) {
						errord.append(MSG_ERROR+", la Longitud del Campo "+sNombreCampo+" es mayor que la definida por su Epo, por favor rev&iacute;selo. \n");
						bOk = ok_det = false;
					}
				} //for


				if(ok_det) {
					int docsdet = existenDoctosCargados(sNoCliente, ig_numero_docto, df_fecha_docto, ic_moneda, con);					//log.debug("Cuantos Doctos Existen Cargados? "+docsdet);
					if(docsdet > 0) {
						String query = "select ic_documento from com_documento "+
										" where ic_epo="+sNoCliente+
										" and ig_numero_docto='"+ig_numero_docto+"'"+
										" and trunc(df_fecha_docto) = to_date('"+df_fecha_docto+"','dd/mm/yyyy') "+
										" and ic_moneda = "+ic_moneda;
	                    //log.debug(query);
						int ic_documento=0;
						boolean actualiza=false;
						ResultSet rs = con.queryDB(query);
						while(rs.next()) {
							ic_documento = rs.getInt(1);
							//log.debug(ic_documento);
							actualiza = existeDoctoDetalle(ic_documento, consecutivo, con);
							//log.debug("actualizo? "+actualiza+" inserto? "+actualiza);
							insertactualizaDoctosDetalles(sCampo1, sCampo2, sCampo3, sCampo4,
															sCampo5, sCampo6, sCampo7, sCampo8,
															sCampo9, sCampo10, ic_documento,
															consecutivo, actualiza, con);
	                    } //while
						con.cierraStatement();
	                } else {
	                    errord.append(MSG_ERROR+", no existe ning�n documento cargado para el n�mero de docto "+ig_numero_docto+", fecha docto. "+df_fecha_docto+" y moneda "+ic_moneda+", por favor reviselo. \n");
	                    bOk = false;
					} // else
				} // ok_det

			} // else
		} // while.
	} catch(NafinException error) {
		bOk = false;
		errord.append(error.getMsgError());
	} catch(NullPointerException npe) {
		bOk = false;
		errord.append("Error Grave el Archivo Detalle no cumple con el Formato Requerido, por favor reviselo. \n");
	} catch(Exception e) {
		bOk = false;
		log.error("Exception en procesarDocumentosDetalles. "+e);
		throw new NafinException("SIST0001");
	}
	finally {
		con.terminaTransaccion(bOk);
		hResultados.put("ok_det", new Boolean(bOk));
		hResultados.put("errord", errord.toString());
		hResultados.put("total", new Integer(iNoLinea));
		hResultados.put("vNumsDoctosDet", vNumsDoctosDet);
		if(con.hayConexionAbierta()) con.cierraConexionDB();
	}
	return hResultados;
	}

	public void insertaDocumentoDetalle(String sProcDoctoDet, String no_pyme, String ig_numero_docto,
											String consecutivo, String df_fecha_docto, String ic_moneda,
											String sCampo1, String sCampo2, String sCampo3,
											String sCampo4, String sCampo5,	String sCampo6, String sCampo7,
											String sCampo8, String sCampo9, String sCampo10) throws NafinException {
	boolean bBien = true;
	AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			String consecu=(consecutivo==null || consecutivo.equals(""))?"seq_comtmp_docto_det_ic_consec.NEXTVAL":consecutivo;
			String sCampoD1=(sCampo1==null || sCampo1.equals(""))?"NULL":"'"+sCampo1+"'";
			String sCampoD2=(sCampo2==null || sCampo2.equals(""))?"NULL":"'"+sCampo2+"'";
			String sCampoD3=(sCampo3==null || sCampo3.equals(""))?"NULL":"'"+sCampo3+"'";
			String sCampoD4=(sCampo4==null || sCampo4.equals(""))?"NULL":"'"+sCampo4+"'";
			String sCampoD5=(sCampo5==null || sCampo5.equals(""))?"NULL":"'"+sCampo5+"'";
			String sCampoD6=(sCampo6==null || sCampo6.equals(""))?"NULL":"'"+sCampo6+"'";
			String sCampoD7=(sCampo7==null || sCampo7.equals(""))?"NULL":"'"+sCampo7+"'";
			String sCampoD8=(sCampo8==null || sCampo8.equals(""))?"NULL":"'"+sCampo8+"'";
			String sCampoD9=(sCampo9==null || sCampo9.equals(""))?"NULL":"'"+sCampo9+"'";
			String sCampoD10=(sCampo10==null || sCampo10.equals(""))?"NULL":"'"+sCampo10+"'";
			String query = "insert into comtmp_docto_detalle(ic_proc_docto, ic_pyme, ig_numero_docto, "+
							"ic_consecutivo, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, "+
							"cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10, df_fecha_docto, ic_moneda) "+
							"values("+sProcDoctoDet+", "+no_pyme+", '"+ig_numero_docto+"', "+consecu+", "+
							sCampoD1+", "+sCampoD2+", "+sCampoD3+", "+sCampoD4+", "+sCampoD5+", "+sCampoD6+", "+
							sCampoD7+", "+sCampoD8+", "+sCampoD9+", "+sCampoD10+", to_date('"+df_fecha_docto+"','dd/mm/yyyy'), "+ic_moneda+")";
			//log.debug(query);
			try {
				con.ejecutaSQL(query);
			} catch(SQLException sqle) {
				bBien = false;
				throw new NafinException("DSCT0064");
			}
		}
		catch(NullPointerException npe) { log.error(npe); }
		catch(Exception e) {
			log.error("Exception en insertaDocumentoDetalle. "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	public void borraDoctosDetalleCargados(String sConseProcDoctoDet, String sOrigen) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bBien = true;
		try{
			con.conexionDB();
			String condicion = (sOrigen.equals("I"))?"ic_consecutivo":"ic_proc_docto";
			String query = "delete from comtmp_docto_detalle where "+condicion+" = "+sConseProcDoctoDet;
			con.ejecutaSQL(query);
		}
		catch(Exception e) {
			log.error("Exception en borraDoctosDetalleCargados. "+e);
			bBien = false;
			throw new NafinException("DSCT0065");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	public Vector mostrarDocumentosCargados(
			String sProcDocto,
			String sAforo,
			String sNoCliente,
			String sEstatus,
			String tipoCarga,
			String sAforoDL,
			String sesIdiomaUsuario, String doctoDuplicados ) throws NafinException {
		AccesoDB con = new AccesoDB();
		Vector vDoctos = new Vector();
		Vector vd = null;
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

		log.debug("mostrarDocumentosCargados ::sEstatus--::--"+sEstatus);

		try{
			con.conexionDB();
			String llave = "ic_proc_docto";
			if("M".equals(tipoCarga))
				llave = "ic_carga_docto";


	String query =
				" SELECT p.cg_razon_social, pd.ig_numero_docto,"   +
				"        TO_CHAR (pd.df_fecha_docto, 'DD/MM/YYYY') AS fecha_docto,"   +
				"        TO_CHAR (pd.df_fecha_venc, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        m.cd_nombre"+idioma+" AS cd_nombre, m.ic_moneda, pd.fn_monto,"   +
				"        pd.cs_dscto_especial,"   +
				"        100 * DECODE (pd.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+") AS porc_anticipo,"   +
				"        pd.fn_monto * DECODE (pd.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+") AS monto_dscto,"   +
				"        pd.ct_referencia, i.cg_razon_social AS nombre_if, pd.ic_consecutivo,"   +
				"        p.ic_pyme, ed.cd_descripcion"+idioma+" AS cd_descripcion, pd.cg_campo1,"   +
				"        pd.cg_campo2, pd.cg_campo3, pd.cg_campo4, pd.cg_campo5,"   +
				"        b.cg_razon_social AS nombre_beneficiario, pd.fn_porc_beneficiario,"   +
				"        (pd.fn_porc_beneficiario / 100) * pd.fn_monto AS monto_beneficiario,"   +
				"        pd.ic_estatus_docto, TO_CHAR (pd.df_fecha_venc_pyme, 'DD/MM/YYYY') AS fecha_venc_pyme, "   +
				"        TO_CHAR (pd.df_entrega, 'DD/MM/YYYY') AS fecha_entrega, "   +
				"        pd.cg_tipo_compra AS tipo_compra, pd.cg_clave_presupuestaria AS clave_presupuestaria, "+
				"        pd.cg_periodo AS periodo, ma.CG_RAZON_SOCIAL as mandante, tf.cg_nombre as TIPO_FACTORAJE , "   +
				"  		decode(pd.cg_duplicado, 'S', 'Si', 'N','No') as DUPLICADO "+
				"   FROM comtmp_proc_docto pd,"   +
				"        comcat_estatus_docto ed,"   +
				"        comcat_pyme p, "   +
				"        comcat_moneda m, "   +
				"        comcat_if i, "   +
				"        comcat_if b, "   +
				" 			COMCAT_MANDANTE ma, "+
				"         COMCAT_TIPO_FACTORAJE tf " +
				"  WHERE pd.ic_pyme = p.ic_pyme"   +
				"    AND pd.ic_moneda = m.ic_moneda"   +
				"    AND pd.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND pd.ic_if = i.ic_if(+)"   +
				"    AND pd.ic_beneficiario = b.ic_if(+)"   +
				"    AND ma.IC_MANDANTE(+) = pd.IC_MANDANTE "+
				"    AND pd.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"    AND pd."+llave+" = "+sProcDocto+" "   +
				"    AND pd.ic_epo = "+sNoCliente+" "  ;
			if (!sEstatus.equals("")) {
				query += " AND PD.ic_estatus_docto = " + sEstatus;
			}

			query += " and  pd.cg_duplicado in ("+doctoDuplicados+")";

			ResultSet rs = con.queryDB(query);
			log.debug("query-------mostrarDocumentosCargados--------- "+query);

			while(rs.next()) {
				vd = new Vector();
/*00*/			vd.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
/*01*/			vd.add(rs.getString("IG_NUMERO_DOCTO"));
/*02*/			vd.add(rs.getString("FECHA_DOCTO"));
/*03*/			vd.add(rs.getString("FECHA_VENC"));
/*04*/			vd.add(rs.getString("CD_NOMBRE"));
/*05*/			vd.add(rs.getString("IC_MONEDA"));
/*06*/			vd.add(rs.getString("FN_MONTO"));
/*07*/			vd.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
/*08*/			vd.add((rs.getString("PORC_ANTICIPO")==null?"":rs.getString("PORC_ANTICIPO")));
/*09*/			vd.add((rs.getString("MONTO_DSCTO")==null?"":rs.getString("MONTO_DSCTO")));
/*10*/			vd.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
/*11*/			vd.add((rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF")));
/*12*/			vd.add(rs.getString("IC_CONSECUTIVO"));
/*13*/			vd.add(rs.getString("IC_PYME"));
/*14*/			vd.add(rs.getString("CD_DESCRIPCION"));
/*15*/			vd.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
/*16*/			vd.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
/*17*/			vd.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
/*18*/			vd.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
/*19*/			vd.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
/*10*/			vd.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
/*21*/			vd.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
/*22*/			vd.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
/*23*/			vd.add(rs.getString("IC_ESTATUS_DOCTO"));
/*24*/			vd.add(rs.getString("FECHA_VENC_PYME"));

//FODEA 050 - VALC - 10/2008
/*25*/			vd.add((rs.getString("FECHA_ENTREGA")==null?"":rs.getString("FECHA_ENTREGA")));
/*26*/			vd.add((rs.getString("TIPO_COMPRA")==null?"":rs.getString("TIPO_COMPRA")));
/*27*/			vd.add((rs.getString("CLAVE_PRESUPUESTARIA")==null?"":rs.getString("CLAVE_PRESUPUESTARIA")));
/*28*/			vd.add((rs.getDouble("PERIODO")==0?"":rs.getString("PERIODO")));
/*29*/			vd.add((rs.getString("mandante")==null?"":rs.getString("mandante")));
/*30*/         vd.add((rs.getString("TIPO_FACTORAJE")==null?"":rs.getString("TIPO_FACTORAJE")));
/*31*/         vd.add((rs.getString("DUPLICADO")==null?"No":rs.getString("DUPLICADO")));


				vDoctos.add(vd);
				//log.debug("-mostrarDocumentosCargados--------- "+vDoctos);
			}
			con.cierraStatement();
		} catch(Exception e) {
			log.error("Exception en mostrarDocumentosCargados. "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	return vDoctos;
	}
/**
   *
   * @throws com.netro.exception.NafinException
   * @return
   * @param acuse
   * @param sesIdiomaUsuario
   * @param sAforoDL
   * @param tipoCarga
   * @param sEstatus
   * @param sNoCliente
   * @param sAforo
   * @param sProcDocto
   */
	public Vector mostrarDocumentosCargadosConIcDocumento(
			String sProcDocto,
			String sAforo,
			String sNoCliente,
			String sEstatus,
			String tipoCarga,
			String sAforoDL,
			String sesIdiomaUsuario, String acuse ) throws NafinException {

    log.info("mostrarDocumentosCargadosConIcDocumento.(E) ");

		AccesoDB con = new AccesoDB();
		Vector vDoctos = new Vector();
		Vector vd = null;
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try{
			con.conexionDB();
			String llave = "ic_proc_docto";
			if("M".equals(tipoCarga))
				llave = "ic_carga_docto";

				String query =
				" SELECT p.cg_razon_social, pd.ig_numero_docto,"   +
				"        TO_CHAR (pd.df_fecha_docto, 'DD/MM/YYYY') AS fecha_docto,"   +
				"        TO_CHAR (pd.df_fecha_venc, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        m.cd_nombre"+idioma+" AS cd_nombre, m.ic_moneda, pd.fn_monto,"   +
				"        pd.cs_dscto_especial,"   +
				"        100 * DECODE (pd.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+") AS porc_anticipo,"   +
				"        pd.fn_monto * DECODE (pd.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+") AS monto_dscto,"   +
				"        pd.ct_referencia, i.cg_razon_social AS nombre_if, pd.ic_consecutivo,"   +
				"        p.ic_pyme, ed.cd_descripcion"+idioma+" AS cd_descripcion, pd.cg_campo1,"   +
				"        pd.cg_campo2, pd.cg_campo3, pd.cg_campo4, pd.cg_campo5,"   +
				"        b.cg_razon_social AS nombre_beneficiario, pd.fn_porc_beneficiario,"   +
				"        (pd.fn_porc_beneficiario / 100) * pd.fn_monto AS monto_beneficiario,"   +
				"        pd.ic_estatus_docto, TO_CHAR (pd.df_fecha_venc_pyme, 'DD/MM/YYYY') AS fecha_venc_pyme, "   +
				"				 d.ic_documento as ic_documento, " +
				"			   d.ic_epo as ic_epo, " +
				"        TO_CHAR (pd.df_entrega, 'DD/MM/YYYY') AS fecha_entrega, "   +
				"        pd.cg_tipo_compra AS tipo_compra, pd.cg_clave_presupuestaria AS clave_presupuestaria, "+
				"			  pd.cg_periodo AS periodo, ma.CG_RAZON_SOCIAL as mandante, tf.cg_nombre as TIPO_FACTORAJE ,  "   +
				"  		decode(pd.cg_duplicado, 'S', 'Si', 'N','No')  as DUPLICADO "+

				"   FROM comtmp_proc_docto pd,"   +
				"        comcat_estatus_docto ed,"   +
				"        comcat_pyme p,"   +
				"        comcat_moneda m,"   +
				"        comcat_if i,"   +
				"        comcat_if b, "   +
				"			com_documento d,  " +
				"  		COMCAT_MANDANTE ma, "+
				" 			COMCAT_TIPO_FACTORAJE tf "+
				"  WHERE pd.ic_pyme = p.ic_pyme"   +
				"    AND pd.ic_moneda = m.ic_moneda"   +
				"    AND pd.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND pd.ic_if = i.ic_if(+)"   +
				"    AND pd.ic_beneficiario = b.ic_if(+)"   +
				"    AND ma.IC_MANDANTE(+) = pd.IC_MANDANTE"+
				"    AND pd.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"    AND pd."+llave+" = "+sProcDocto+" "   +
				"    AND pd.ic_epo = "+sNoCliente+" "  ;
			if (!sEstatus.equals("")) {
				query += " AND PD.ic_estatus_docto = " + sEstatus;
			}
			query +=
				" 		AND d.ic_epo 				= pd.ic_epo 			" +
				" 		AND d.ic_pyme 				= p.ic_pyme 			" +
				"		AND d.ig_numero_docto 	= pd.ig_numero_docto	"+
        "    AND d.cc_acuse = '"+acuse+"' "  ;
					ResultSet rs = con.queryDB(query);

			//log.debug("-mostrarDocumentosCargadosConIcDocumento--------- "+query);

			while(rs.next()) {
				vd = new Vector();
/*00*/			vd.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
/*01*/			vd.add(rs.getString("IG_NUMERO_DOCTO"));
/*02*/			vd.add(rs.getString("FECHA_DOCTO"));
/*03*/			vd.add(rs.getString("FECHA_VENC"));
/*04*/			vd.add(rs.getString("CD_NOMBRE"));
/*05*/			vd.add(rs.getString("IC_MONEDA"));
/*06*/			vd.add(rs.getString("FN_MONTO"));
/*07*/			vd.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
/*08*/			vd.add((rs.getString("PORC_ANTICIPO")==null?"":rs.getString("PORC_ANTICIPO")));
/*09*/			vd.add((rs.getString("MONTO_DSCTO")==null?"":rs.getString("MONTO_DSCTO")));
/*10*/			vd.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
/*11*/			vd.add((rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF")));
/*12*/			vd.add(rs.getString("IC_CONSECUTIVO"));
/*13*/			vd.add(rs.getString("IC_PYME"));
/*14*/			vd.add(rs.getString("CD_DESCRIPCION"));
/*15*/			vd.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
/*16*/			vd.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
/*17*/			vd.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
/*18*/			vd.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
/*19*/			vd.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
/*10*/			vd.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
/*21*/			vd.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
/*22*/			vd.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
/*23*/			vd.add(rs.getString("IC_ESTATUS_DOCTO"));
/*24*/			vd.add(rs.getString("FECHA_VENC_PYME"));
/*25*/			vd.add(rs.getString("IC_DOCUMENTO"));
/*26*/			vd.add(rs.getString("IC_EPO"));

//FODEA 050 - VALC - 10/2008
/*27*/			vd.add((rs.getString("FECHA_ENTREGA")==null?"":rs.getString("FECHA_ENTREGA")));
/*28*/			vd.add((rs.getString("TIPO_COMPRA")==null?"":rs.getString("TIPO_COMPRA")));
/*29*/			vd.add((rs.getString("CLAVE_PRESUPUESTARIA")==null?"":rs.getString("CLAVE_PRESUPUESTARIA")));
/*30*/			vd.add((rs.getDouble("PERIODO")==0?"":rs.getString("PERIODO")));
/*31*/			vd.add((rs.getString("mandante")==null?"":rs.getString("mandante")));
/*32*/			vd.add((rs.getString("TIPO_FACTORAJE")==null?"":rs.getString("TIPO_FACTORAJE")));
/*33*/			vd.add((rs.getString("DUPLICADO")==null?"No":rs.getString("DUPLICADO")));

				vDoctos.add(vd);
				//log.debug("mostrarDocumentosCargadosConIcDocumento:: "+vDoctos);
			}
			con.cierraStatement();
		} catch(Exception e) {
			log.error("Exception en mostrarDocumentosCargadosConIcDocumento. "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
    log.info("mostrarDocumentosCargadosConIcDocumento.(S) ");
	return vDoctos;
	}

	public Vector getComDoctosCargados(String sAcuse, String sAforo, String sNoCliente, String sEstatus, String sAforoDL, String sesIdiomaUsuario ) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vDoc = null;
	Vector vDocumento = new Vector();
	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try{
			con.conexionDB();
			String query =
				"select py.cg_razon_social, d.ig_numero_docto, "+
				" TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, "+
				" TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, "+
				" m.cd_nombre"+idioma + " as CD_NOMBRE,"+
				" m.ic_moneda, d.fn_monto, d.cs_dscto_especial,  "+
				" 100*DECODE (d.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+")  as porcentajeAnticipo, "+
				" d.fn_monto*DECODE (d.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+")  as montoDescuento, "+
				" d.ct_referencia, "+
				" I.cg_razon_social as NombreIF, d.fn_porc_beneficiario, "+
				" B.cg_razon_social as Nombre_Beneficiario, "+
				" (d.fn_porc_beneficiario/100)*d.fn_monto as Monto_Beneficiario, "+
				" d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, "+
				" TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme "+
				" from com_documento d, comcat_pyme py, comcat_moneda m, "+
				" comcat_if I, comcat_if B "+
				" where d.ic_pyme = py.ic_pyme "+
				" and d.ic_moneda = m.ic_moneda "+
				" and d.ic_if=I.ic_if(+) "+
				" and d.ic_beneficiario=B.ic_if(+)"+
				" and d.ic_epo ="+ sNoCliente+
				" and d.cc_acuse= '"+sAcuse+"'";
			if (!sEstatus.equals("")) {
				query += " and d.ic_estatus_docto = " + sEstatus ;
			}
			log.debug("getComDoctosCargados"+query);
			try {
				ResultSet rs = con.queryDB(query);
				while (rs.next()) {
					vDoc = new Vector();
/*00*/				vDoc.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
/*01*/				vDoc.add(rs.getString("IG_NUMERO_DOCTO"));
/*02*/				vDoc.add(rs.getString("DF_FECHA_DOCTO"));
/*03*/				vDoc.add(rs.getString("DF_FECHA_VENC"));
/*04*/				vDoc.add(rs.getString("CD_NOMBRE"));
/*05*/				vDoc.add(rs.getString("IC_MONEDA"));
/*06*/				vDoc.add(rs.getString("FN_MONTO"));
/*07*/				vDoc.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
/*08*/				vDoc.add((rs.getString("PORCENTAJEANTICIPO")==null?"":rs.getString("PORCENTAJEANTICIPO")));
/*09*/				vDoc.add((rs.getString("MONTODESCUENTO")==null?"":rs.getString("MONTODESCUENTO")));
/*10*/				vDoc.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
/*11*/				vDoc.add((rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF")));
/*12*/				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
/*13*/				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
/*14*/				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
/*15*/				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
/*16*/				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
/*17*/				vDoc.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
/*18*/				vDoc.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
/*19*/				vDoc.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
/*20*/ 				vDoc.add((rs.getString("DF_FECHA_VENC_PYME")==null?"":rs.getString("DF_FECHA_VENC_PYME")));
					vDocumento.addElement(vDoc);
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0052"); }
		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en getComDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDocumento;
	}

	public Vector getComDoctosCargadosConIcDocumento(String sAcuse, String sAforo, String sNoCliente, String sEstatus, String sAforoDL, String sesIdiomaUsuario ) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vDoc = null;
	Vector vDocumento = new Vector();
	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try{
			con.conexionDB();
			String query =
				"select py.cg_razon_social, d.ig_numero_docto, "+
				" TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, "+
				" TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, "+
				" m.cd_nombre"+idioma + " as CD_NOMBRE,"+
				" m.ic_moneda, d.fn_monto, d.cs_dscto_especial,  "+
				" 100*DECODE (d.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+")  as porcentajeAnticipo, "+
				" d.fn_monto*DECODE (d.ic_moneda, 1, "+sAforo+", 54, "+sAforoDL+")  as montoDescuento, "+
				" d.ct_referencia, "+
				" I.cg_razon_social as NombreIF, d.fn_porc_beneficiario, "+
				" B.cg_razon_social as Nombre_Beneficiario, "+
				" (d.fn_porc_beneficiario/100)*d.fn_monto as Monto_Beneficiario, "+
				" d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, "+
				" TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme, "+
				" d.ic_documento as ic_documento, " +
				" d.ic_epo as ic_epo, " +
				" TO_CHAR(d.df_entrega,'dd/mm/yyyy') as fecha_entrega, "+
				" d.cg_tipo_compra as tipo_compra, " +
				" d.cg_clave_presupuestaria as clave_presupuestaria, " +
				" d.cg_periodo as periodo , ma.CG_RAZON_SOCIAL as mandante, " +
				" tf.cg_nombre as TIPO_FACTORAJE,   "+
				" 	decode(d.cg_duplicado, 'S', 'Si', 'N','No')  as DUPLICADO  "+

				" from com_documento d, comcat_pyme py, comcat_moneda m, "+
				" comcat_if I, comcat_if B, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf "+
				" where d.ic_pyme = py.ic_pyme "+
				" and d.ic_moneda = m.ic_moneda "+
				" and d.ic_if=I.ic_if(+) "+
				" and d.ic_beneficiario=B.ic_if(+)"+
				" and ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				" AND d.cs_dscto_especial = tf.cc_tipo_factoraje  "+
				" and d.ic_epo ="+ sNoCliente+
				" and d.cc_acuse= '"+sAcuse+"'";

			if (!sEstatus.equals("")) {
				query += " and d.ic_estatus_docto = " + sEstatus ;
			}
			log.debug("getComDoctosCargadosConIcDocumento   Imprimir PDF ::"+query);
			try {
				ResultSet rs = con.queryDB(query);
				while (rs.next()) {
					vDoc = new Vector();
/*00*/				vDoc.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
/*01*/				vDoc.add(rs.getString("IG_NUMERO_DOCTO"));
/*02*/				vDoc.add(rs.getString("DF_FECHA_DOCTO"));
/*03*/				vDoc.add(rs.getString("DF_FECHA_VENC"));
/*04*/				vDoc.add(rs.getString("CD_NOMBRE"));
/*05*/				vDoc.add(rs.getString("IC_MONEDA"));
/*06*/				vDoc.add(rs.getString("FN_MONTO"));
/*07*/				vDoc.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
/*08*/				vDoc.add((rs.getString("PORCENTAJEANTICIPO")==null?"":rs.getString("PORCENTAJEANTICIPO")));
/*09*/				vDoc.add((rs.getString("MONTODESCUENTO")==null?"":rs.getString("MONTODESCUENTO")));
/*10*/				vDoc.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
/*11*/				vDoc.add((rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF")));
/*12*/				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
/*13*/				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
/*14*/				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
/*15*/				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
/*16*/				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
/*17*/				vDoc.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
/*18*/				vDoc.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
/*19*/				vDoc.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
/*20*/ 				vDoc.add((rs.getString("DF_FECHA_VENC_PYME")==null?"":rs.getString("DF_FECHA_VENC_PYME")));
/*21*/				vDoc.add((rs.getString("IC_DOCUMENTO")==null?"":rs.getString("IC_DOCUMENTO")));
/*22*/				vDoc.add((rs.getString("IC_EPO")==null?"":rs.getString("IC_EPO")));
//FODEA 050 - VALC - 10/2008
/*23*/				vDoc.add((rs.getString("FECHA_ENTREGA")==null?"":rs.getString("FECHA_ENTREGA")));
/*24*/				vDoc.add((rs.getString("TIPO_COMPRA")==null?"":rs.getString("TIPO_COMPRA")));
/*25*/ 				vDoc.add((rs.getString("CLAVE_PRESUPUESTARIA")==null?"":rs.getString("CLAVE_PRESUPUESTARIA")));
/*26*/				vDoc.add((rs.getString("PERIODO")==null?"":rs.getString("PERIODO")));
/*27*/				vDoc.add((rs.getString("mandante")==null?"":rs.getString("mandante")));
/*28*/				vDoc.add((rs.getString("TIPO_FACTORAJE")==null?"":rs.getString("TIPO_FACTORAJE")));
/*29*/   			vDoc.add((rs.getString("DUPLICADO")==null?"":rs.getString("DUPLICADO")));

					vDocumento.addElement(vDoc);
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0052"); }
		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en getComDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDocumento;
	}


	/* Inserta a la Tabla com_acuse1 el Acuse. */
	public boolean insertaAcuse1(String sNoAcuse, String sNoTotDoctosMn, String sMontoMn,
								 String sNoTotDoctosDol, String sMontoDol, String sNoUsuario,
								 String sAcuseRecibo) throws NafinException {
	boolean bBien = true;
	AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			String query = "insert into com_acuse1(cc_acuse, in_total_docto_mn, fn_total_monto_mn, "+
							"df_fechahora_carga, in_total_docto_dl, fn_total_monto_dl, ic_usuario, "+
							"cg_recibo_electronico) "+
							"values('"+sNoAcuse+"', "+sNoTotDoctosMn+", "+sMontoMn+", SYSDATE, "+
							sNoTotDoctosDol+", "+sMontoDol+", '"+sNoUsuario+"', '"+sAcuseRecibo+"')";
			//log.debug(query);
			con.ejecutaSQL(query);
		}
		catch(Exception e) {
			log.error("Exception en insertaAcuse1. "+e);
			//Error al Insertar los datos en la Tabla del Acuse com_acuse1, no existe el Usuario.
			bBien = false;
			throw new NafinException("SIST0001");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bBien;
	}

	public void insertaDoctosConDetalles(String ic_proc_docto, String ic_proc_docto_det,
										String ses_ic_epo, String no_acuse, String iNoUsuario , String strNombreUsuario) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bBien = true, actualiza=false;
		try{
			con.conexionDB();
			int ic_documento=0; String ic_consecutivod="";
			String ic_pyme="", ig_numero_docto="", df_fecha_docto="", df_fecha_venc="", df_fecha_venc_pyme="";
			String ic_moneda="", fn_monto="", cs_dscto_especial="", ic_estatus_docto="";
			String ic_if="", ct_referencia="", cg_campo1="", cg_campo2="", cg_campo3="", cg_campo4="", cg_campo5="", porc_benef="", no_benef="";
			String cg_campo1d="", cg_campo2d="", cg_campo3d="", cg_campo4d="", cg_campo5d="", cg_campo6d="", cg_campo7d="", cg_campo8d="", cg_campo9d="", cg_campo10d="";
			String df_entrega = "", cg_tipo_compra = "", cg_clave_presupuestaria = "", cg_periodo = "", ic_mandante = "", duplicado = "";

			// Obtiene los datos de la tabla temporal comtmp_proc_docto.
//			Vector vDoctos = getDoctosCargados(ic_proc_docto, "M");
			Vector vDoctos = getDoctosCargados(ic_proc_docto, "M", con);
			for(int i=0; i<vDoctos.size(); i++) {
				Vector vdm 			= (Vector)vDoctos.get(i);
				ic_pyme 			= vdm.get(0).toString();
				ig_numero_docto 	= vdm.get(1).toString();
				df_fecha_docto 		= vdm.get(2).toString();
				df_fecha_venc 		= vdm.get(3).toString();
				df_fecha_venc_pyme 	= vdm.get(17).toString();
				ic_moneda 			= vdm.get(4).toString();
				fn_monto 			= vdm.get(5).toString();
				cs_dscto_especial 	= vdm.get(6).toString();
				/*fn_monto_dscto = rs.getString();*/		//Siempre es nulo se calcula en el despliegue.
				ic_estatus_docto 	= (String)vdm.get(7);
				ct_referencia 		= vdm.get(8).toString();
				ic_if 				= vdm.get(9).toString();
				cg_campo1 			= vdm.get(10).toString();
				cg_campo2 			= vdm.get(11).toString();
				cg_campo3 			= vdm.get(12).toString();
				cg_campo4 			= vdm.get(13).toString();
				cg_campo5 			= vdm.get(14).toString();
				porc_benef 			= vdm.get(15).toString();
				no_benef 			= vdm.get(16).toString();

//FODEA 050 - VALC - 10/2008
				df_entrega 			= vdm.get(18).toString();
				cg_tipo_compra 	= vdm.get(19).toString();
				cg_clave_presupuestaria 			= vdm.get(20).toString();
				cg_periodo 			= vdm.get(21).toString();
				ic_mandante 	    = vdm.get(22).toString();
				duplicado 	    = vdm.get(23).toString();


				//log.debug("Valores: "+ic_pyme+"-"+ig_numero_docto+"-"+df_fecha_docto+"-"+df_fecha_venc+"-"+ic_moneda+"-"+fn_monto+"-"+cs_dscto_especial+"-"+fn_monto_dscto+"-"+ic_estatus_docto+"-"+ct_referencia <br>");

				// Obtenemos ic_documento maximo de la tabla com_documento
				ic_documento = getNumeroMaximoDocto(con);
				//log.debug("El numero max de Documento "+ic_documento);
				// Inserta los valores en com_documento
				insertaDoctosNegociables(
					ic_documento, 		ic_pyme, 			ses_ic_epo,
					no_acuse, 			ig_numero_docto, 	df_fecha_docto,
					df_fecha_venc, 		ic_moneda, 			fn_monto,
					cs_dscto_especial, 	ic_if, 				ct_referencia,
					cg_campo1, 			cg_campo2, 			cg_campo3,
					cg_campo4, 			cg_campo5, 			porc_benef,
					no_benef, 			ic_estatus_docto, 	df_fecha_venc_pyme,
					df_entrega, 	cg_tipo_compra, 	cg_clave_presupuestaria,	cg_periodo, ic_mandante, duplicado, con);

				//log.debug("Inserto en com_documento "+bOkInsertaDocto);

				// Valida que venga el ic_proc_docto_det para saber si va ha insertar documentos detalle.
				if(ic_proc_docto_det!=null && !ic_proc_docto_det.equals("") && !ic_proc_docto_det.equals("0")) {

					// Obtiene los datos de la tabla comtmp_docto_detalle.
					Vector vDoctosDet = getDoctosDetalles(ic_proc_docto_det, ig_numero_docto, df_fecha_docto, ic_moneda, ic_pyme, con);
					for(int d=0; d<vDoctosDet.size(); d++) {
						Vector vDet = (Vector)vDoctosDet.get(d);
						ic_consecutivod = (String)vDet.get(0);
						cg_campo1d = (String)vDet.get(1);
						cg_campo2d = (String)vDet.get(2);
						cg_campo3d = (String)vDet.get(3);
						cg_campo4d = (String)vDet.get(4);
						cg_campo5d = (String)vDet.get(5);
						cg_campo6d = (String)vDet.get(6);
						cg_campo7d = (String)vDet.get(7);
						cg_campo8d = (String)vDet.get(8);
						cg_campo9d = (String)vDet.get(9);
						cg_campo10d = (String)vDet.get(10);

						actualiza = existeDoctoDetalle(ic_documento, ic_consecutivod, con);
						//log.debug("Existe el Documento: "+actualiza);

						insertactualizaDoctosDetalles(cg_campo1d, cg_campo2d, cg_campo3d,
														cg_campo4d, cg_campo5d, cg_campo6d,
														cg_campo7d, cg_campo8d, cg_campo9d,
														cg_campo10d, ic_documento, ic_consecutivod,
														actualiza, con);
					}//fin for

				} else {// sin valor ic_proc_docto_det.
					//log.debug("El ig_numero_docto "+ig_numero_docto);
					seleccionaInsertaDoctosDetalles(ic_documento, ic_proc_docto, ig_numero_docto, df_fecha_docto, ic_moneda, ic_pyme, con);
					//log.debug("Inserto bien en insert update "+bSelecInsert);
				}

			} // for tabla temporal.
		} catch(SQLException sqle) {
			bBien = false; log.error("Error al Insertar los datos en la tabla de com_documento_detalle.");
		} catch (NafinException Error){
			log.error("Error al Insertar los datos en la tabla de com_documento. insertaDoctosConDetalles()");
			bBien = false;
			throw Error;
		} catch(Exception e) {
			log.error("Exception en insertaDoctosConDetalles. "+e);
			bBien = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}//metodo inserta.

	/* Obtiene los datos de la tabla temporal comtmp_proc_docto. */
	public Vector getDoctosCargados(String sConsecNoProc, String sOrigen) throws NafinException {
		AccesoDB con = new AccesoDB();
		Vector vDocumento = new Vector();
		try{
			con.conexionDB();
			vDocumento = getDoctosCargados(sConsecNoProc, sOrigen, con);
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			log.error("Exception en getDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return vDocumento;
	}

	/* Obtiene los datos de la tabla temporal comtmp_proc_docto. */
	public Vector getDoctosCargados(String sConsecNoProc, String sOrigen, AccesoDB con) throws NafinException {
	Vector vDoc = null;
	Vector vDocumento = new Vector();
		try{
			String query =
				" SELECT ic_pyme, ig_numero_docto,"   +
				"        TO_CHAR (df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto,"   +
				"        TO_CHAR (df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, ic_moneda,"   +
				"        fn_monto, cs_dscto_especial, ic_estatus_docto, ct_referencia, ic_if,"   +
				"        fn_porc_beneficiario, ic_beneficiario, cg_campo1, cg_campo2, cg_campo3,"   +
				"        cg_campo4, cg_campo5, TO_CHAR (df_fecha_venc_pyme, 'dd/mm/yyyy') AS df_fecha_venc_pyme,"   +
//FODEA 050 - VALC - 10/2008
				"			TO_CHAR (df_entrega, 'dd/mm/yyyy') AS df_fecha_entrega,  "  +
				"		 	cg_tipo_compra, cg_clave_presupuestaria, cg_periodo, ic_mandante ,  cg_duplicado " +
				"   FROM comtmp_proc_docto"  ;
			if(sOrigen.equals("I"))
				query +=" WHERE ic_consecutivo = "+sConsecNoProc;
			else if(sOrigen.equals("M"))
				query +=" WHERE ic_proc_docto = "+sConsecNoProc;


	log.debug("getDoctosCargados----------. "+query);


			try {
				ResultSet rs = con.queryDB(query);
				while (rs.next()) {
					vDoc = new Vector();
/*00*/				vDoc.add(rs.getString("IC_PYME"));
/*01*/				vDoc.add((rs.getString("IG_NUMERO_DOCTO")==null?"":rs.getString("IG_NUMERO_DOCTO")));
/*02*/				vDoc.add((rs.getString("DF_FECHA_DOCTO")==null?"":rs.getString("DF_FECHA_DOCTO")));
/*03*/				vDoc.add((rs.getString("DF_FECHA_VENC")==null?"":rs.getString("DF_FECHA_VENC")));
/*04*/				vDoc.add(rs.getString("IC_MONEDA"));
/*05*/				vDoc.add((rs.getString("FN_MONTO")==null?"":rs.getString("FN_MONTO")));
/*06*/				vDoc.add(rs.getString("CS_DSCTO_ESPECIAL"));
/*07*/				vDoc.add(rs.getString("IC_ESTATUS_DOCTO"));
/*08*/				vDoc.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
/*09*/				vDoc.add((rs.getString("IC_IF")==null?"":rs.getString("IC_IF")));
/*10*/				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
/*11*/				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
/*12*/				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
/*13*/				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
/*14*/				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
/*15*/				vDoc.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
/*16*/				vDoc.add((rs.getString("IC_BENEFICIARIO")==null?"":rs.getString("IC_BENEFICIARIO")));
/*17*/				vDoc.add((rs.getString("DF_FECHA_VENC_PYME")==null?"":rs.getString("DF_FECHA_VENC_PYME")));
//FODEA 050 - VALC - 10/2008
/*18*/				vDoc.add((rs.getString("DF_FECHA_ENTREGA")==null?"":rs.getString("DF_FECHA_ENTREGA")));
/*19*/				vDoc.add((rs.getString("CG_TIPO_COMPRA")==null?"":rs.getString("CG_TIPO_COMPRA")));
/*20*/				vDoc.add((rs.getString("CG_CLAVE_PRESUPUESTARIA")==null?"":rs.getString("CG_CLAVE_PRESUPUESTARIA")));
/*21*/				vDoc.add((rs.getString("CG_PERIODO")==null?"":rs.getString("CG_PERIODO")));
/*22*/				vDoc.add((rs.getString("ic_mandante")==null?"":rs.getString("ic_mandante")));
/*23*/				vDoc.add((rs.getString("cg_duplicado")==null?"":rs.getString("cg_duplicado")));

					vDocumento.addElement(vDoc);

		log.debug("getDoctosCargados----vDocumento------. "+vDocumento);
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0052"); }

		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en getDoctosCargados. "+e);
			throw new NafinException("SIST0001");
		}
	return vDocumento;
	}

	public Vector getDoctosDetalles(String sProcDocto, String sNumeroDocto,
									String df_fecha_docto, String ic_moneda, String ic_pyme) throws NafinException {
	log.info("CargaDocumentoBean.getDoctosDetalles(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	Vector vDoc = null;
	Vector vDocumento = new Vector();
		try{
			con.conexionDB();
			String query = "select ic_consecutivo, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"+
							" cg_campo5, cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10"+
							" from comtmp_docto_detalle"+
							" where ic_proc_docto = ?"+//sProcDocto+
							" and ig_numero_docto = ?"+//sNumeroDocto+"'"+
							" and trunc(df_fecha_docto) = to_date(?,'dd/mm/yyyy') "+
							" and ic_moneda = ?"+ //ic_moneda;
							" and ic_pyme = ?";
			//log.debug("CargaDocumentoBean.existenDoctosCargadosTmp(query)" + query);
			ps = con.queryPrecompilado(query);
			ps.setInt(1, Integer.parseInt(sProcDocto));
			ps.setString(2, sNumeroDocto);
			ps.setString(3, df_fecha_docto);
			ps.setInt(4, Integer.parseInt(ic_moneda));
			ps.setInt(5, Integer.parseInt(ic_pyme));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				vDoc = new Vector();
				vDoc.add((rs.getString("IC_CONSECUTIVO")==null?"":rs.getString("IC_CONSECUTIVO")));
				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
				vDoc.add((rs.getString("CG_CAMPO6")==null?"":rs.getString("CG_CAMPO6")));
				vDoc.add((rs.getString("CG_CAMPO7")==null?"":rs.getString("CG_CAMPO7")));
				vDoc.add((rs.getString("CG_CAMPO8")==null?"":rs.getString("CG_CAMPO8")));
				vDoc.add((rs.getString("CG_CAMPO9")==null?"":rs.getString("CG_CAMPO9")));
				vDoc.add((rs.getString("CG_CAMPO10")==null?"":rs.getString("CG_CAMPO10")));
				vDocumento.addElement(vDoc);
			}
			rs.close();
			if(ps != null) ps.close();

		}
		catch(Exception e) {
			log.error("Exception en getDoctosDetalles. "+e);
			throw new NafinException("SIST0035");
		}
		finally {
			log.info("CargaDocumentoBean.getDoctosDetalles(S)");
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vDocumento;
	}

	private Vector getDoctosDetalles(String sProcDocto, String sNumeroDocto,
									String df_fecha_docto, String ic_moneda, String ic_pyme, AccesoDB con) throws NafinException {
	log.info("CargaDocumentoBean.getDoctosDetalles(E)");
	PreparedStatement ps = null;
	Vector vDoc = null;
	Vector vDocumento = new Vector();
		try{
			String query = "select ic_consecutivo, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"+
							" cg_campo5, cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10"+
							" from comtmp_docto_detalle"+
							" where ic_proc_docto = ?"+//sProcDocto+
							" and ig_numero_docto = ?"+//sNumeroDocto+"'"+
							" and trunc(df_fecha_docto) = to_date(?,'dd/mm/yyyy') "+
							" and ic_moneda = ?"+//ic_moneda;
							" and ic_pyme ? ";
			//log.debug("CargaDocumentoBean.existenDoctosCargadosTmp(query)" + query);
			ps = con.queryPrecompilado(query);
			ps.setInt(1, Integer.parseInt(sProcDocto));
			ps.setString(2, sNumeroDocto);
			ps.setString(3, df_fecha_docto);
			ps.setInt(4, Integer.parseInt(ic_moneda));
			ps.setInt(5, Integer.parseInt(ic_pyme));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				vDoc = new Vector();
				vDoc.add((rs.getString("IC_CONSECUTIVO")==null?"":rs.getString("IC_CONSECUTIVO")));
				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
				vDoc.add((rs.getString("CG_CAMPO6")==null?"":rs.getString("CG_CAMPO6")));
				vDoc.add((rs.getString("CG_CAMPO7")==null?"":rs.getString("CG_CAMPO7")));
				vDoc.add((rs.getString("CG_CAMPO8")==null?"":rs.getString("CG_CAMPO8")));
				vDoc.add((rs.getString("CG_CAMPO9")==null?"":rs.getString("CG_CAMPO9")));
				vDoc.add((rs.getString("CG_CAMPO10")==null?"":rs.getString("CG_CAMPO10")));
				vDocumento.addElement(vDoc);
			}
			rs.close();
			if(ps != null) ps.close();

		}
		catch(Exception e) {
			log.error("Exception en getDoctosDetalles. "+e);
			throw new NafinException("SIST0035");
		}
		finally {
			log.info("CargaDocumentoBean.getDoctosDetalles(S)");
		}
	return vDocumento;
	}


	public Vector getDoctosDetalle(String sConsec) {
	AccesoDB con = new AccesoDB();
	Vector vDoc = new Vector();
	//Vector vDetalle = new Vector();
		try{
			con.conexionDB();
			String query = "select cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, "+
							" cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10 "+
							" from comtmp_docto_detalle "+
							" where ic_consecutivo = "+sConsec;

			ResultSet rs = con.queryDB(query);
			if (rs.next()) {
				vDoc.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1").trim()));
				vDoc.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2").trim()));
				vDoc.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3").trim()));
				vDoc.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4").trim()));
				vDoc.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5").trim()));
				vDoc.add((rs.getString("CG_CAMPO6")==null?"":rs.getString("CG_CAMPO6").trim()));
				vDoc.add((rs.getString("CG_CAMPO7")==null?"":rs.getString("CG_CAMPO7").trim()));
				vDoc.add((rs.getString("CG_CAMPO8")==null?"":rs.getString("CG_CAMPO8").trim()));
				vDoc.add((rs.getString("CG_CAMPO9")==null?"":rs.getString("CG_CAMPO9").trim()));
				vDoc.add((rs.getString("CG_CAMPO10")==null?"":rs.getString("CG_CAMPO10").trim()));
				//vDetalle.addElement(vDoc);
			}
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDoctosDetalle. "+e);
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDoc;
	}

	/* Obtenemos ic_documento maximo de la tabla com_documento */
	private int getNumeroMaximoDocto(AccesoDB con) throws NafinException {
	int iIcDocumento = 0;
		try{
//			String query = "select max(ic_documento) from com_documento";011107
			String query = "SELECT seq_com_documento.NEXTVAL FROM DUAL";
			ResultSet rs = con.queryDB(query);
			if (rs.next()) {
//				iIcDocumento = rs.getInt(1)+1;
				iIcDocumento = rs.getInt(1);
			}
			rs.close();
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getNumeroMaximoDocto. "+e);
			throw new NafinException("SIST0001");
		}
	return iIcDocumento;
	}

	/* Inserta los valores en com_documento */
	/*private void insertaDoctosNegociables(
					int ic_documento, 			String ic_pyme, 		String ses_ic_epo,
					String no_acuse, 			String ig_numero_docto, String df_fecha_docto,
					String df_fecha_venc, 		String ic_moneda, 		String fn_monto,
					String cs_dscto_especial, 	String ic_if, 			String ct_referencia,
					String cg_campo1, 			String cg_campo2, 		String cg_campo3,
					String cg_campo4, 			String cg_campo5, 		String porc_benef,
					String no_benef, 			String sStatusDocto, 	String df_fecha_venc_pyme,	AccesoDB con) throws NafinException {
		try{
			ic_if = (ic_if==null || ic_if.equals(""))?"NULL":ic_if;
			ct_referencia=(ct_referencia==null || ct_referencia.equals(""))?"NULL":"'"+ct_referencia+"'";
			cg_campo1=(cg_campo1==null || cg_campo1.equals(""))?"NULL":"'"+cg_campo1+"'";
			cg_campo2=(cg_campo2==null || cg_campo2.equals(""))?"NULL":"'"+cg_campo2+"'";
			cg_campo3=(cg_campo3==null || cg_campo3.equals(""))?"NULL":"'"+cg_campo3+"'";
			cg_campo4=(cg_campo4==null || cg_campo4.equals(""))?"NULL":"'"+cg_campo4+"'";
			cg_campo5=(cg_campo5==null || cg_campo5.equals(""))?"NULL":"'"+cg_campo5+"'";
			porc_benef=(porc_benef==null || porc_benef.equals(""))?"NULL":porc_benef;
			no_benef=(no_benef==null || no_benef.equals(""))?"NULL":no_benef;
			String query =
				" INSERT INTO com_documento ("   +
				"              ic_documento, ic_pyme, ic_epo, cc_acuse, ig_numero_docto,"   +
				"              df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto,"   +
				"              cs_dscto_especial, ic_if, fn_monto_dscto, ic_estatus_docto,"   +
				"              ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				"              cg_campo5, fn_porc_beneficiario, ic_beneficiario,"   +
				"              df_fecha_venc_pyme)"   +
				"      VALUES ("   +
				"              "+ic_documento+", "+ic_pyme+", "+ses_ic_epo+", '"+no_acuse+"', '"+ig_numero_docto+"',"   +
				"              TO_DATE('"+df_fecha_docto+"','DD/MM/YYYY'), TO_DATE('"+df_fecha_venc+"','DD/MM/YYYY'), "+ic_moneda+", "+fn_monto+","   +
				"              '"+cs_dscto_especial+"', "+ic_if+", NULL, "+sStatusDocto+","   +
				"              "+ct_referencia+", "+cg_campo1+", "+cg_campo2+", "+cg_campo3+", "+cg_campo4+","   +
				"              "+cg_campo5+", "+porc_benef+", "+no_benef+","   +
				"              TO_DATE('"+df_fecha_venc_pyme+"','DD/MM/YYYY'))"  ;

			log.debug("--com_documento--:: 1:--"+query);

			try { con.ejecutaSQL(query);
			} catch(SQLException sqle) {
				throw new NafinException("DSCT0034");
			}
		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en insertaDoctosNegociables. "+e);
			throw new NafinException("SIST0001");
		}
	}*/

//FODEA 050 - VALC - 10/2008
	/* Inserta los valores en com_documento */
	private void insertaDoctosNegociables(
					int ic_documento, 			String ic_pyme, 		String ses_ic_epo,
					String no_acuse, 			String ig_numero_docto, String df_fecha_docto,
					String df_fecha_venc, 		String ic_moneda, 		String fn_monto,
					String cs_dscto_especial, 	String ic_if, 			String ct_referencia,
					String cg_campo1, 			String cg_campo2, 		String cg_campo3,
					String cg_campo4, 			String cg_campo5, 		String porc_benef,
					String no_benef, 			String sStatusDocto, 	String df_fecha_venc_pyme,
					String df_entrega, 	String cg_tipo_compra, 	String cg_clave_presupuestaria, String cg_periodo,
					String ic_mandante, String duplicado,AccesoDB con) throws NafinException {

			String  mandante = "";

		try{
			ic_if = (ic_if==null || ic_if.equals(""))?"NULL":ic_if;
			ct_referencia=(ct_referencia==null || ct_referencia.equals(""))?"NULL":"'"+ct_referencia+"'";
			cg_campo1=(cg_campo1==null || cg_campo1.equals(""))?"NULL":"'"+cg_campo1+"'";
			cg_campo2=(cg_campo2==null || cg_campo2.equals(""))?"NULL":"'"+cg_campo2+"'";
			cg_campo3=(cg_campo3==null || cg_campo3.equals(""))?"NULL":"'"+cg_campo3+"'";
			cg_campo4=(cg_campo4==null || cg_campo4.equals(""))?"NULL":"'"+cg_campo4+"'";
			cg_campo5=(cg_campo5==null || cg_campo5.equals(""))?"NULL":"'"+cg_campo5+"'";
			porc_benef=(porc_benef==null || porc_benef.equals(""))?"NULL":porc_benef;
			no_benef=(no_benef==null || no_benef.equals(""))?"NULL":no_benef;

			String df_fecha_entrega		= "NULL";
			if(!"".equals(df_entrega))
				df_fecha_entrega= "TO_DATE('"+df_entrega+"','DD/MM/YYYY')";
			cg_tipo_compra=(cg_tipo_compra==null || cg_tipo_compra.equals(""))?"NULL":"'"+cg_tipo_compra+"'";
			cg_clave_presupuestaria=(cg_clave_presupuestaria==null || cg_clave_presupuestaria.equals(""))?"NULL":"'"+cg_clave_presupuestaria+"'";
			cg_periodo=(cg_periodo==null || cg_periodo.equals(""))?"NULL":cg_periodo;


			if (ic_mandante.equals("")) {
			  mandante  = "NULL";
			}else  {
			mandante = ic_mandante;
			}

			String query =
				" INSERT INTO com_documento ("   +
				"              ic_documento, ic_pyme, ic_epo, cc_acuse, ig_numero_docto,"   +
				"              df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto,"   +
				"              cs_dscto_especial, ic_if, fn_monto_dscto, ic_estatus_docto,"   +
				"              ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				"              cg_campo5, fn_porc_beneficiario, ic_beneficiario,"   +
				"              df_fecha_venc_pyme, df_entrega, cg_tipo_compra, " +
				"					cg_clave_presupuestaria, cg_periodo, ic_mandante, cg_duplicado)"   +
				"      VALUES ("   +
				"              "+ic_documento+", "+ic_pyme+", "+ses_ic_epo+", '"+no_acuse+"', '"+ig_numero_docto+"',"   +
				"              TO_DATE('"+df_fecha_docto+"','DD/MM/YYYY'), TO_DATE('"+df_fecha_venc+"','DD/MM/YYYY'), "+ic_moneda+", "+fn_monto+","   +
				"              '"+cs_dscto_especial+"', "+ic_if+", NULL, "+sStatusDocto+","   +
				"              "+ct_referencia+", "+cg_campo1+", "+cg_campo2+", "+cg_campo3+", "+cg_campo4+","   +
				"              "+cg_campo5+", "+porc_benef+", "+no_benef+","   +
				"              TO_DATE('"+df_fecha_venc_pyme+"','DD/MM/YYYY'), " +
				//"					" + df_fecha_entrega + "," + cg_tipo_compra + "," + cg_clave_presupuestaria +"," + cg_periodo + ")"  ;
				"					" + df_fecha_entrega + "," + cg_tipo_compra + "," + cg_clave_presupuestaria +"," + cg_periodo +"," + mandante + ",'" + duplicado+ "' )"  ;

			log.debug("--com_documento--:: 2:--"+query);

			try { con.ejecutaSQL(query);
			} catch(SQLException sqle) {
				throw new NafinException("DSCT0034");
			}
		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en insertaDoctosNegociables. "+e);
			throw new NafinException("SIST0001");
		}
	}

	private boolean existeDoctoDetalle(int ic_documento, String ic_consecutivod,
										AccesoDB con) throws NafinException {
	boolean bActualizaOInserta=false;
		try{
			String query = "select count(*) from com_documento_detalle "+
							"where ic_documento = "+ic_documento+
							" and ic_docto_detalle = "+ic_consecutivod;
			ResultSet rs = con.queryDB(query);
			if (rs.next())
				bActualizaOInserta = (rs.getInt(1)==0)?false:true;

			rs.close();
			con.cierraStatement();
		}
		catch(Exception e) {
			bActualizaOInserta=false;
			log.error("Exception en existeDoctoDetalle. "+e);
			throw new NafinException("SIST0001");
		}
	return bActualizaOInserta;
	}

	private void insertactualizaDoctosDetalles(String cg_campo1d, String cg_campo2d, String cg_campo3d,
											String cg_campo4d, String cg_campo5d, String cg_campo6d,
											String cg_campo7d, String cg_campo8d, String cg_campo9d,
											String cg_campo10d, int ic_documento, String ic_consecutivod,
											boolean bActualizaOInserta, AccesoDB con) throws NafinException {
	String query="";
	String query1="";

		try{
			String sCampoD1=(cg_campo1d==null || cg_campo1d.equals(""))?"NULL":"'"+cg_campo1d+"'";
			String sCampoD2=(cg_campo2d==null || cg_campo2d.equals(""))?"NULL":"'"+cg_campo2d+"'";
			String sCampoD3=(cg_campo3d==null || cg_campo3d.equals(""))?"NULL":"'"+cg_campo3d+"'";
			String sCampoD4=(cg_campo4d==null || cg_campo4d.equals(""))?"NULL":"'"+cg_campo4d+"'";
			String sCampoD5=(cg_campo5d==null || cg_campo5d.equals(""))?"NULL":"'"+cg_campo5d+"'";
			String sCampoD6=(cg_campo6d==null || cg_campo6d.equals(""))?"NULL":"'"+cg_campo6d+"'";
			String sCampoD7=(cg_campo7d==null || cg_campo7d.equals(""))?"NULL":"'"+cg_campo7d+"'";
			String sCampoD8=(cg_campo8d==null || cg_campo8d.equals(""))?"NULL":"'"+cg_campo8d+"'";
			String sCampoD9=(cg_campo9d==null || cg_campo9d.equals(""))?"NULL":"'"+cg_campo9d+"'";
			String sCampoD10=(cg_campo10d==null || cg_campo10d.equals(""))?"NULL":"'"+cg_campo10d+"'";
			if(bActualizaOInserta) {
				query = "UPDATE com_documento_detalle set CG_CAMPO1="+sCampoD1+", CG_CAMPO2="+sCampoD2+", "+
						"CG_CAMPO3="+sCampoD3+", CG_CAMPO4="+sCampoD4+", CG_CAMPO5="+sCampoD5+", "+
						"CG_CAMPO6="+sCampoD6+", CG_CAMPO7="+sCampoD7+", CG_CAMPO8="+sCampoD8+", "+
						"CG_CAMPO9="+sCampoD9+", CG_CAMPO10="+sCampoD10+" "+
						"where IC_DOCUMENTO="+ic_documento+" and IC_DOCTO_DETALLE="+ic_consecutivod;
			} else {
				query = "INSERT INTO com_documento_detalle(IC_DOCUMENTO, IC_DOCTO_DETALLE, CG_CAMPO1, CG_CAMPO2, "+
						"CG_CAMPO3, CG_CAMPO4, CG_CAMPO5, CG_CAMPO6, CG_CAMPO7, CG_CAMPO8, CG_CAMPO9, CG_CAMPO10) "+
						"VALUES ("+ic_documento+", "+ic_consecutivod+", "+sCampoD1+", "+sCampoD2+", "+
						sCampoD3+", "+sCampoD4+", "+sCampoD5+", "+sCampoD6+", "+
						sCampoD7+", "+sCampoD8+", "+sCampoD9+", "+sCampoD10+")";
				query1 = "UPDATE com_documento set cs_detalle = 'S' " +
						" WHERE ic_documento = " + ic_documento;

			}
			log.debug("insertactualizaDoctosDetalles   ::"+query);
			try {
				con.ejecutaSQL(query);
				if (!query1.equals("")) {
					con.ejecutaSQL(query1);
				}
			} catch(SQLException sqle) {
				log.error("insertactualizaDoctosDetalles tabla com_documento_detalle: "+sqle);
				throw new NafinException("DSCT0057");
			}
		} catch (NafinException error) {
			throw error;
		} catch(Exception e) {
			log.error("Exception en insertactualizaDoctosDetalles. "+e);
			throw new NafinException("SIST0001");
		}
	}

	private void seleccionaInsertaDoctosDetalles(int iClaveDocumento, String sProcDocto,
                                                            String sNumDocto,
                                                            String fechaDocto,
                                                            String icMoneda,
															String icPyme,
                                                            AccesoDB con) throws NafinException {
		try{
			String query = "insert into com_documento_detalle (ic_documento, ic_docto_detalle,"+
							" cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, "+
							" cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10)"+
							" select "+iClaveDocumento+" as ic_documento, ROWNUM, "+
							" cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, "+
							" cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10"+
							" from comtmp_docto_detalle"+
							" where ic_proc_docto = "+sProcDocto+
							" and ig_numero_docto = '"+sNumDocto+"'"+
							" and trunc(df_fecha_docto) = to_date('"+fechaDocto+"', 'dd/mm/yyyy') "+
							" and ic_moneda = "+icMoneda +
							" and ic_pyme = "+icPyme;
			String query1 = "UPDATE com_documento set cs_detalle = 'S' " +
					" WHERE ic_documento = " + iClaveDocumento +
					" AND exists " +
					" (select ic_documento " +
					" 	from com_documento_detalle " +
					" 	where ic_documento = " + iClaveDocumento + ")";

			//log.debug(query);
			try {
				con.ejecutaSQL(query);
				con.ejecutaSQL(query1);
			} catch(SQLException sqle) {
				log.error("Insert - Select en seleccionaInsertaDoctosDetalles "+sqle);
				throw new NafinException("DSCT0058");
			}
		} catch (NafinException error) {
			throw error;
		} catch(Exception e) {
			log.error("Exception en seleccionaInsertaDoctosDetalles. "+e);
			throw new NafinException("SIST0001");
		}
	}
	/**
	 *
	 * @param sNoCliente
	 * @param cbEPO
	 * @param factoraje
	 * @return
	 * @throws NafinException
	 */
	public Vector getComboBeneficiarios(String sNoCliente, String cbEPO, String factoraje) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vBeneficiarios = new Vector();
	Vector vb = null;
		try{
			log.info("CargaDocumentoEJB:getComboBeneficiarios(E) ");
			con.conexionDB();
			StringBuilder query = new StringBuilder(); 
			
			 query.append(" select ic_if, cg_razon_social from comcat_if "+
							"where cs_habilitado = 'S' and ic_if in " );
						
					if ("D".equals(factoraje)  ) {
											
						 query.append(	"(SELECT distinct d.IC_BENEFICIARIO " + 
							" FROM comrel_cta_ban_distribuido d, comrel_if_epo r " + 
							" WHERE d.ic_epo = " +cbEPO +
							"  AND d.ic_pyme = " +sNoCliente +
							"  AND d.cg_autoriza_if = 'S'" + 
							"  and r.ic_epo = d.ic_epo" + 
							"  and r.cs_beneficiario='S'" + 
							"  and d.IC_IF= r.IC_IF  )");
					}else  {
						query.append( "(select ic_if from comrel_if_epo where ic_epo="+cbEPO+" and cs_beneficiario='S')");	
					}
							query.append("order  by  cg_razon_social ");
			
	

			log.debug("query:====> "+query);
			try{
				ResultSet rs = con.queryDB(query.toString());
				while (rs.next()) {
					vb = new Vector();
					vb.add(rs.getString(1));
					vb.add(rs.getString(2).trim());
					vBeneficiarios.addElement(vb);
				}
				con.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0053"); }
		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("Exception en getComboBeneficiarios. "+e);
			throw new NafinException("SIST0001");
		} finally {
			log.info("CargaDocumentoEJB:getComboBeneficiarios(S) ");
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vBeneficiarios;
	}


	/* -------------------- Metodos para Mantenimiento de Documentos -------------------- */

	/* Obtenemos el numero maximo del ic_proceso_cambio de la tabla comtmp_cambio_importe. */
	public String getNumaxProcesoCambioImporte() throws NafinException {
	AccesoDB con = new AccesoDB();
	String sProcesoCambioImporte="1";
		try{
			con.conexionDB();
			String query = "select max(ic_proceso_cambio)+1 from comtmp_cambio_importe";
			ResultSet rs = con.queryDB(query);
			if(rs.next())
				sProcesoCambioImporte = (rs.getString(1)==null)?"1":rs.getString(1);

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getNumaxProcesoCambioImporte. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return sProcesoCambioImporte;
	}

public Hashtable procesarMantenimientoDocumentos(
						String sLineasDoctos,
						String ctxttotdoc, 			String ctxttotdocdol,
						String ctxtmtodoc, 			String ctxtmtodocdol,
						String ctxtmtomindoc, 		String ctxtmtomindocdol,
						String ctxtmtomaxdoc,		String ctxtmtomaxdocdol,
						String ic_proceso_cambio, 	String ic_epo,
						MensajeParam mensaje_param, String sesIdiomaUsuario,
						String sOperaFechaVencPyme) throws NafinException {
	AccesoDB con = new AccesoDB();
	BigDecimal txtmtomindoc=new BigDecimal(ctxtmtomindoc);
	BigDecimal txtmtomindocdol=new BigDecimal(ctxtmtomindocdol);
	BigDecimal txtmtomaxdoc=new BigDecimal(ctxtmtomaxdoc);
	BigDecimal txtmtomaxdocdol=new BigDecimal(ctxtmtomaxdocdol);
	Hashtable hResultado=new Hashtable();
	String query="", linea="", linea_aux="", lineasErrores="", msgerr = "";
	String cve_pyme="", numero_docto="", df_fecha_docto="", moneda="", tipo="", fn_monto_ant="", fn_monto_nvo="";
	String causa="", df_fecha_venc_ant="", df_fecha_venc_nvo="", df_fecha_venc_p_ant="", df_fecha_venc_p_nvo="";
	String desc_pyme="", habilitadoPyme="", estatus_nuevo="";
	//>>========================================================================== FODEA 050 - 2008
	String fechaEntrega = "";
	String tipoCompra = "";
	String clavePresupuestaria = "";
	String periodo = "";
	//==========================================================================<<
	StringBuffer error=new StringBuffer();
	StringBuffer errorOtros=new StringBuffer();
	StringBuffer sinError=new StringBuffer();
	ResultSet rs=null;
	int lineadocs=1, totdoc_mn=0, totdoc_dolar=0, totdoc_mnOK=0, totdoc_dolarOK=0;
	int ic_pyme=0, ic_documento=0, ic_moneda=0, negociable=2;
	double monto_tot_mnOK=0.0, monto_tot_dolarOK=0.0;
	BigDecimal monto_mn=new BigDecimal("0.0");
	BigDecimal monto_dolar=new BigDecimal("0.0");
	boolean gen_archivo=false, bBotonProcesar=false;
	boolean ok=true, registroOk=true, bMontoSumado=true, otrosErr=true;
	VectorTokenizer vt=null;	Vector vecdat=null;
	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
	
	boolean bFechaVenc  = false;
	boolean bFechaVencP = false;
	int ic_estatus_docto = 0;
	try{
		con.conexionDB();
		StringTokenizer stDocto = new StringTokenizer(sLineasDoctos,"\n");
		while(stDocto.hasMoreElements()) {
			linea = stDocto.nextToken().trim();
			registroOk = true;
			bMontoSumado = true;
			if(linea.length()==0)
				continue;
			linea_aux = linea;
			linea = linea.replace('\'',' ');
			vt = new VectorTokenizer(linea,"|");
			vecdat = vt.getValuesVector();
			msgerr = MensajeParam.getMensaje("13forma5.ErrorEnLinea",sesIdiomaUsuario)+lineadocs+", ";
			//log.debug("El tama�o del Vector: "+vecdat.size());
			int iLimCap = 10;
			if("S".equals(sOperaFechaVencPyme)) {
				iLimCap += 2;
			}
			//>>====================================================================== FODEA 050 - 2008
			 if(tieneParamEpoPef(ic_epo)){
				iLimCap += 4;
			 }
			//======================================================================<<
			if(vecdat.size() < 8 && vecdat.size() > iLimCap) {//Campo Min 8		Campo Max 10-12
				registroOk = ok = false;
				error.append(msgerr +MensajeParam.getMensaje("13forma5.NoCoincideLayout",sesIdiomaUsuario)+"\n\n");
			}
			else { // else 2
				try {
					cve_pyme			= (vecdat.size()>= 1)?Comunes.quitaComitasSimples(vecdat.get( 0).toString().trim()).trim():"";
					numero_docto		= (vecdat.size()>= 2)?Comunes.quitaComitasSimples(vecdat.get( 1).toString().trim()).trim():"";
					df_fecha_docto		= (vecdat.size()>= 3)?Comunes.quitaComitasSimples(vecdat.get( 2).toString().trim()).trim():"";
					moneda				= (vecdat.size()>= 4)?Comunes.quitaComitasSimples(vecdat.get( 3).toString().trim()).trim():"";
					tipo				= (vecdat.size()>= 5)?Comunes.quitaComitasSimples(vecdat.get( 4).toString().trim()).trim():"";
					fn_monto_ant		= (vecdat.size()>= 6)?Comunes.quitaComitasSimples(vecdat.get( 5).toString().trim()).trim():"";
					fn_monto_nvo		= (vecdat.size()>= 7)?Comunes.quitaComitasSimples(vecdat.get( 6).toString().trim()).trim():"";
					causa				= (vecdat.size()>= 8)?Comunes.quitaComitasSimples(vecdat.get( 7).toString().trim()).trim():"";
					df_fecha_venc_ant	= (vecdat.size()>= 9)?Comunes.quitaComitasSimples(vecdat.get( 8).toString().trim()).trim():"";
					df_fecha_venc_nvo	= (vecdat.size()>=10)?Comunes.quitaComitasSimples(vecdat.get( 9).toString().trim()).trim():"";
					df_fecha_venc_p_ant	= (vecdat.size()>=11)?Comunes.quitaComitasSimples(vecdat.get(10).toString().trim()).trim():"";
					df_fecha_venc_p_nvo	= (vecdat.size()>=12)?Comunes.quitaComitasSimples(vecdat.get(11).toString().trim()).trim():"";
					//>>================================================================== Obtenemos los valores de la parametrizaci�n epo pef
					fechaEntrega	= (vecdat.size()>= 13)?Comunes.quitaComitasSimples(vecdat.get(12).toString().trim()).trim():"";
					tipoCompra	= (vecdat.size()>=14)?Comunes.quitaComitasSimples(vecdat.get(13).toString().trim()).trim():"";
					clavePresupuestaria	= (vecdat.size()>=15)?Comunes.quitaComitasSimples(vecdat.get(14).toString().trim()).trim():"";
					periodo	= (vecdat.size()>=16)?Comunes.quitaComitasSimples(vecdat.get(15).toString().trim()).trim():"";
					//==================================================================<<
					log.debug("******:cve_pyme:			"+cve_pyme+"*");
					log.debug("******:numero_docto:		"+numero_docto+"*");
					log.debug("******:df_fecha_docto:		"+df_fecha_docto+"*");
					log.debug("******:moneda:				"+moneda+"*");
					log.debug("******:tipo:				"+tipo+"*");
					log.debug("******:fn_monto_ant:		"+fn_monto_ant+"*");
					log.debug("******:fn_monto_nvo:		"+fn_monto_nvo+"*");
					log.debug("******:causa:				"+causa+"*");
					log.debug("******:df_fecha_venc_ant:	"+df_fecha_venc_ant+"*");
					log.debug("******:df_fecha_venc_nvo:	"+df_fecha_venc_nvo+"*");
					log.debug("******:df_fecha_venc_p_ant:	"+df_fecha_venc_p_ant+"*");
					log.debug("******:df_fecha_venc_p_nvo:	"+df_fecha_venc_p_nvo+"*");

				} catch(ArrayIndexOutOfBoundsException aioobe) {
					log.error(msgerr + ": " + aioobe);
					break;
				}
				vecdat.removeAllElements();
				//log.debug("Los Valores:"+cve_pyme+"-"+numero_docto+"-"+tipo+"-"+fn_monto_ant+"-"+fn_monto_nvo+"-"+causa);

				if(tipo.length()==0 ) {
						error.append(msgerr +"el valor del Tipo de cambio no puede estar vacio "+"\n\n");
					registroOk = ok = false;
				}	else	{

						StringBuffer strSQL = new StringBuffer();
						String estatus = "";
						strSQL.append(" select ic_estatus_docto from com_documento where  IG_NUMERO_DOCTO = '"+numero_docto+"'"+
						" and  ic_epo =  "+ic_epo +
						" and DF_FECHA_DOCTO =trunc(to_date('"+df_fecha_docto+"','dd/mm/yyyy'))  ");

						System.out.println("strSQL   "+strSQL);

						ResultSet rsE = con.queryDB(strSQL.toString());
						if(rsE.next()){
							estatus = rsE.getString(1);
						}
						rsE.close();   
            con.cierraStatement();
						
						System.out.println("estatus "+estatus +"   tipo  "+tipo); 						  
						
						if( !estatus.equals("33")  && !tipo.equalsIgnoreCase("L") && !tipo.equalsIgnoreCase("R") && !tipo.equalsIgnoreCase("V") &&  !tipo.equalsIgnoreCase("M") && !tipo.equalsIgnoreCase("B") && !tipo.equalsIgnoreCase("F") && !tipo.equalsIgnoreCase("P")  ) {
							error.append(msgerr +"el valor del Tipo de cambio no es el correcto "+"\n\n");
							registroOk = ok = false;
						}else  if( estatus.equals("33") &&   ( !tipo.equalsIgnoreCase("D")  &&  !tipo.equalsIgnoreCase("B")  )  )  {
							error.append(msgerr +"el valor del Tipo de cambio no es el correcto    "+"\n\n");
							registroOk = ok = false;
						}
				}


				// ** validacion de longitud ** //
				// La Validacion para la pyme.
				if(cve_pyme.length()==0) {
					error.append(msgerr + MensajeParam.getMensaje("13forma5.PYMEnoVacio",sesIdiomaUsuario)+"\n\n");
					registroOk = ok = false;
				}
				// La Validacion para el numero de documento.
				if(numero_docto.length()==0) {
					error.append(msgerr + MensajeParam.getMensaje("13forma5.NumDocNoVacio",sesIdiomaUsuario)+"\n\n");
					registroOk = ok = false;
				}

				// La Validacion para el Monto anterior.
				if(fn_monto_ant.length()==0 ) {
					error.append(msgerr + MensajeParam.getMensaje("13forma5.MontAnteNoVacio",sesIdiomaUsuario)+"\n\n");
					fn_monto_ant="0";
					registroOk = ok = false;
				}
			  if(fn_monto_ant.length()!=0 ) {
					// valida el formato del monto
					if(Comunes.esDecimal(fn_monto_ant)==false) {
						error.append(msgerr + MensajeParam.getMensaje("13forma5.MontAnteValDecimal",sesIdiomaUsuario)+"\n\n");
						registroOk = ok = false;
					}
					// valida que el monto sea positivo
					if(Comunes.esDecimal(fn_monto_ant) && (new Double(fn_monto_ant).doubleValue()<0) ) {
						error.append(msgerr + MensajeParam.getMensaje("13forma5.MontAntePositivo",sesIdiomaUsuario)+"\n\n");
						registroOk = ok = false;
					}
				}

				// La Validacion para el Monto nuevo.
				if( fn_monto_nvo.length()==0||"".equals(fn_monto_nvo)) {
					error.append(msgerr + MensajeParam.getMensaje("13forma5.MontNuevoNoVacio",sesIdiomaUsuario)+"\n\n");
					fn_monto_nvo="0";
					registroOk = ok = false;
				} else {
					// valida el formato del monto
					if(Comunes.esDecimal(fn_monto_nvo)==false) {
						error.append(msgerr + MensajeParam.getMensaje("13forma5.MontNuevoValDecimal",sesIdiomaUsuario)+"\n\n");
						registroOk = ok = false;
					}
					// valida que el monto nuevo y el anterior sean diferentes
					if(!fn_monto_ant.equals("")){
					if(Comunes.esDecimal(fn_monto_ant) && Comunes.esDecimal(fn_monto_nvo) ) {
						if(tipo.equalsIgnoreCase("M") && new Double(fn_monto_ant).doubleValue() == new Double(fn_monto_nvo).doubleValue() ) {

							error.append(msgerr + MensajeParam.getMensaje("13forma5.MontNewMismoMontAnte",sesIdiomaUsuario)+"\n\n");
							registroOk = ok = false;
						}
						else if(!tipo.equalsIgnoreCase("M") && !fn_monto_nvo.equals(fn_monto_ant)){
							error.append(msgerr +MensajeParam.getMensaje("13forma5.MontNewIgualMontAnte",sesIdiomaUsuario) +"\n\n");
							registroOk = ok = false;
						}
						// valida que el monto sea positivo
						if(new Double(fn_monto_nvo).doubleValue() < 0) {
							error.append(msgerr + MensajeParam.getMensaje("13forma5.MontNewPositivo",sesIdiomaUsuario)+"\n\n");
							registroOk = ok = false;
						}
					}
				}
				}
				// La Validacion para la causa..
				if(causa.length()==0) {
					error.append(msgerr + MensajeParam.getMensaje("13forma5.CAUSANoVacio",sesIdiomaUsuario)+"\n\n");
					registroOk = ok = false;
				}

				// ** validacion de relaciones ** //
				if(cve_pyme.length()>0) {
					// Revisa si el numero de pyme existe en la relacion pyme_epo y la obtine para insertarla en la tabla temporal.
					query =" SELECT REL.ic_pyme, CAT.cg_razon_social, REL.cs_habilitado"+
							" FROM comrel_pyme_epo REL, comcat_pyme CAT "+
							" WHERE CAT.ic_pyme=REL.ic_pyme"+
							" AND REL.ic_epo="+ic_epo+
							" AND REL.cs_habilitado = 'S'"+
							" AND REL.cg_pyme_epo_interno='"+cve_pyme+"'";
					log.debug("*******relacion pyme_epo******:"+query);
					ic_pyme = 0;
					rs = con.queryDB(query);
					if(!rs.next()) {
						error.append(msgerr +cve_pyme+" "+ MensajeParam.getMensaje("13forma5.NumPymeNExisEPO",sesIdiomaUsuario)+" "+ic_epo+".\n\n");
						registroOk = ok = false;
						bMontoSumado = false;
					}
		            else { // else 3
						ic_pyme = rs.getInt(1);
						desc_pyme = rs.getString(2);
						habilitadoPyme = rs.getString(3);
						con.cierraStatement();
						if(habilitadoPyme.equals("N")) {
							error.append(msgerr + cve_pyme +" "+ MensajeParam.getMensaje("13forma5.NumPymeDeshaEPO",sesIdiomaUsuario)+" "+ic_epo+".\n\n");
							registroOk = ok = false;
							bMontoSumado = false;
						}
						else { // else 4
							ic_documento = 0;
							double monto = 0.0;
							ic_moneda = 0;
							//int ic_estatus_docto = 0;
							String fecha_venc = "";
							String fecha_venc_pyme = "";
							String dscto_especial = "";
							if(!Comunes.checaFecha(df_fecha_docto)){
							  	error.append(msgerr + MensajeParam.getMensaje("13forma5.DateEIncorrecta",sesIdiomaUsuario)+"\n\n");
								registroOk = ok = false;
								otrosErr = false;
							} else if(!moneda.equals("1") && !moneda.equals("54")){
								error.append(msgerr + MensajeParam.getMensaje("13forma5.MonedaIncorrecta",sesIdiomaUsuario)+"\n\n");
								registroOk = ok = false;
								otrosErr = false;
							} else {	//else 5
								// Revisa si el numero de documento existe para la pyme y la epo.
								query =
									" SELECT ic_documento, fn_monto, ic_moneda, ic_estatus_docto,"   +
									"        TO_CHAR (df_fecha_venc, 'DD/MM/YYYY'), cs_dscto_especial,"   +
									"        TO_CHAR (df_fecha_venc_pyme, 'DD/MM/YYYY') "   +
									"   FROM com_documento"   +
									"  WHERE ic_epo = "+ic_epo+" "   +
									"    AND ic_pyme = "+ic_pyme+" "   +
									"    AND ig_numero_docto = '"+numero_docto+"'"   +
									"    AND df_fecha_docto >= TO_DATE ('"+df_fecha_docto+"', 'dd/mm/yyyy')"   +
									"    AND df_fecha_docto < (TO_DATE ('"+df_fecha_docto+"', 'dd/mm/yyyy')+1)"   +
									"    AND ic_moneda = "+moneda+" ";
								log.debug("********numero de documento existe para la pyme y la epo**********:"+query);
								rs = con.queryDB(query);
								while(rs.next()) {
									ic_documento 		= rs.getInt(1);
									monto 				= rs.getDouble(2);
									ic_moneda 			= rs.getInt(3);
									ic_estatus_docto 	= rs.getInt(4);
									fecha_venc 			= rs.getString(5)==null?"":rs.getString(5);
									dscto_especial 		= rs.getString(6)==null?"":rs.getString(6);
									fecha_venc_pyme 	= rs.getString(7)==null?"":rs.getString(7);
								}//while(rs.next())
                				rs.close();
								con.cierraStatement();

								log.debug("ic_documento  "+ic_documento);
								log.debug("monto  "+monto);
								log.debug("ic_moneda  "+ic_moneda);
								log.debug("fecha_venc  "+fecha_venc);
								log.debug("dscto_especial  "+dscto_especial);
								log.debug("fecha_venc_pyme  "+fecha_venc_pyme);
								log.debug("ic_estatus_docto  "+ic_estatus_docto);

								// verifica que el documento exista para la relacion pyme-epo
								if(ic_documento == 0) {
									error.append(msgerr + MensajeParam.getMensaje("13forma5.ElDocumento",sesIdiomaUsuario)+" "+numero_docto+" "+MensajeParam.getMensaje("13forma5.NoExistePyME",sesIdiomaUsuario)+" "+cve_pyme+" "+MensajeParam.getMensaje("13forma5.laEPO",sesIdiomaUsuario)+" "+ic_epo+".\n\n");
									registroOk = ok = false;
								}
								// verifica que el estatus del documento sea negociable o bloqueado en caso de ser reactivacion
								else if(tipo.equalsIgnoreCase("R") && (ic_estatus_docto!=21 && ic_estatus_docto!=28)){
									error.append(msgerr + MensajeParam.getMensaje("13forma5.DocNoBloqueado",sesIdiomaUsuario)+"\n\n");
									registroOk = ok = false;
								}
								else if(!tipo.equalsIgnoreCase("R") && ic_estatus_docto!=negociable && ic_estatus_docto!=23 &&  ic_estatus_docto!=28  &&  ic_estatus_docto!=33) {
									error.append(msgerr + MensajeParam.getMensaje("13forma5.DocNegociable",sesIdiomaUsuario)+"\n\n");
									registroOk = ok = false;
								}
								/*else if((tipo.equalsIgnoreCase("B") || tipo.equalsIgnoreCase("F") ||  tipo.equalsIgnoreCase("P") ) && ic_estatus_docto==negociable) {
										error.append(msgerr +"El cambio de estatus aplica �nicamente para documentos Pre negociables."+"\n\n");
										registroOk = ok = false;
								}*/  //esta regla se quito ya que afecto a los cambios de Estatus de Negociables fodea 060-2010
								/*else if ( ic_estatus_docto!=28 ) {
											error.append(msgerr +"el documento no es Pre negociable."+"\n\n");
									registroOk = ok = false;
								}*/


								// verifica igualdad de montos (archivo - tabla)
								if( ic_documento!=0 && Comunes.esDecimal(fn_monto_nvo) && (monto != (new Double(fn_monto_ant).doubleValue())) ) {
									error.append(msgerr + MensajeParam.getMensaje("13forma5.MontoArchivo",sesIdiomaUsuario)+" "+Comunes.formatoMN(fn_monto_ant)+" "
												+MensajeParam.getMensaje("13forma5.MontoTabla",sesIdiomaUsuario)+" "
												+Comunes.formatoMN(new Double(monto).toString())+".\n\n");
									registroOk = ok = false;
								}

								if(Comunes.esDecimal(fn_monto_nvo)) { // compara montos
									if(ic_moneda==1) {     //Si es Moneda Nacional
										if(new BigDecimal(fn_monto_nvo).compareTo(txtmtomindoc) < 0) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.MontoMinimMN",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(txtmtomindoc.toPlainString())+" "
												+ MensajeParam.getMensaje("13forma5.MayorMontoArchivo",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(fn_monto_nvo.toString())+" "+MensajeParam.getMensaje("13forma5.DebeserMenor",sesIdiomaUsuario)+"\n\n");
											registroOk = ok = false;
										}
										else if(new BigDecimal(fn_monto_nvo).compareTo(txtmtomaxdoc) > 0) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.MontoMaximoMN",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(txtmtomaxdoc.toPlainString())+" "
												+ MensajeParam.getMensaje("13forma5.MenorMontoArchivo",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(fn_monto_nvo.toString())+" "+MensajeParam.getMensaje("13forma5.DebeserMayor",sesIdiomaUsuario)+" \n\n");
											registroOk = ok = false;
										}
										monto_mn = monto_mn.add(new BigDecimal(fn_monto_nvo));
										totdoc_mn++;
									}
									else if(ic_moneda==54) {    //Si son Dolares.
										if(new BigDecimal(fn_monto_nvo).compareTo(txtmtomindocdol) < 0) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.MinimoUSD",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(txtmtomindocdol.toPlainString())+" "
												+ MensajeParam.getMensaje("13forma5.MayorMontoArchivo",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(fn_monto_nvo.toString())+" "+MensajeParam.getMensaje("13forma5.DebeserMenor",sesIdiomaUsuario)+"\n\n");
											registroOk = ok = false;
										}
										else if(new BigDecimal(fn_monto_nvo).compareTo(txtmtomaxdocdol) > 0) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.MaxUSD",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(txtmtomaxdocdol.toPlainString())+" "
												+ MensajeParam.getMensaje("13forma5.MenorMontoArchivo",sesIdiomaUsuario)+" "
												+ Comunes.formatoMN(fn_monto_nvo.toString())+" "+MensajeParam.getMensaje("13forma5.DebeserMayor",sesIdiomaUsuario)+"\n\n");
											registroOk = ok = false;
										}
										monto_dolar = monto_dolar.add(new BigDecimal(fn_monto_nvo));
										totdoc_dolar++;
									}
									else {
										bMontoSumado = false;
									}
								} // if( Comunes.esDecimal(fn_monto_nvo) ) { // compara montos
								else {
									bMontoSumado = false;
								}
								// Validaci�n de la modificaci�n de la fecha de vencimiento.
								bFechaVenc  = false;
								bFechaVencP = false;
								if(ic_documento!=0 && tipo.equalsIgnoreCase("V")) {
									if("C".equals(dscto_especial)) {
										error.append(msgerr + MensajeParam.getMensaje("13forma5.NoCambiaFVParaNC",sesIdiomaUsuario)+"\n\n");
										registroOk = ok = false;
									} else {
										if((!df_fecha_venc_p_ant.equals("") || !df_fecha_venc_p_nvo.equals(""))&&"S".equals(sOperaFechaVencPyme)) {
											bFechaVencP = true;
										} else {
											bFechaVenc = true;
										}
									}
									if(bFechaVenc) {
										if(df_fecha_venc_ant.equals("") || df_fecha_venc_nvo.equals("")) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaVenNueFormato",sesIdiomaUsuario)+"\n\n");
											registroOk = ok = false;
										} else {
											if(!Comunes.checaFecha(df_fecha_venc_ant)){
												error.append(msgerr +MensajeParam.getMensaje("13forma5.FechaVenIncorret",sesIdiomaUsuario) +"\n\n");
												registroOk = ok = false;
											}
											if(!Comunes.checaFecha(df_fecha_venc_nvo)){
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaVenNueIncorret",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											}
											if(!fecha_venc.equals(df_fecha_venc_ant)) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FVAnteMismaFVDoc",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											} else if(fecha_venc.equals(df_fecha_venc_nvo)) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FVNMismaFVDoc",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											} else {
												// Validacion de la fecha de vencimiento que sea un dia habil con el cat�logo.
												String sLineaFec = getDiasInhabilesXanio(ic_epo, con);
												if(sLineaFec.length() > 0) {
													StringTokenizer st = new StringTokenizer(sLineaFec,";");
													while(st.hasMoreElements()) {
														//log.debug(st.nextToken());
														if(df_fecha_venc_nvo.equals(st.nextToken())) {
															error.append(msgerr +MensajeParam.getMensaje("13forma5.FVNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_nvo+"\n\n");
															registroOk = ok = false;
														}
													}
												}

												String sLineaDiaMes = getDiasInhabiles(ic_epo, con);
												if(sLineaDiaMes.length() > 0) {
													StringTokenizer st = new StringTokenizer(sLineaDiaMes,";");
													while(st.hasMoreElements()) {
														//log.debug(st.nextToken());
														if(df_fecha_venc_nvo.substring(0,5).equals(st.nextToken())) {
															error.append(msgerr +MensajeParam.getMensaje("13forma5.FVNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_nvo+"\n\n");
															registroOk = ok = false;
														}
													}
												}
												// Validacion de la fecha de Vencimiento que no sea un d�a inh�bil un S�bado o Domingo.
												Calendar gcDiaFecha = new GregorianCalendar();
												java.util.Date fechaVencNvo = Comunes.parseDate(df_fecha_venc_nvo);
												gcDiaFecha.setTime(fechaVencNvo);
												int no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
												if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
													error.append(msgerr +MensajeParam.getMensaje("13forma5.FVNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_nvo+"\n\n");
													registroOk = ok = false;
												}
												// Validaci�n de fechas de Venimiento d�as Minimos y M�ximos.
												int comp_fecha;
												SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
												Vector vFechasVencimiento = getFechasVencimiento(ic_epo, 2, con);
												java.util.Date FechaMin = (java.util.Date)vFechasVencimiento.get(0);
												java.util.Date FechaMax = (java.util.Date)vFechasVencimiento.get(1);
												java.util.Date fechaDocto = Comunes.parseDate(df_fecha_docto);
												comp_fecha=fechaDocto.compareTo(fechaVencNvo); //log.debug(comp_fecha);
												if (comp_fecha > 0 && !df_fecha_docto.equals(df_fecha_venc_nvo) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVNueva",sesIdiomaUsuario)+df_fecha_venc_nvo+" "+MensajeParam.getMensaje("13forma5.FVNMenorFDoc",sesIdiomaUsuario)+" "+df_fecha_docto+".\n\n");
													registroOk = ok = false;
												}
												comp_fecha=fechaVencNvo.compareTo(FechaMin); //log.debug(comp_fecha);
												if (comp_fecha < 0 && !df_fecha_venc_nvo.equals(sdf.format(FechaMin)) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVNueva",sesIdiomaUsuario)+df_fecha_venc_nvo+", "+ MensajeParam.getMensaje("13forma5.FVNNoParametroDias",sesIdiomaUsuario)+" "+sdf.format(FechaMin)+".\n\n");
													registroOk = ok = false;
												}
												comp_fecha=fechaVencNvo.compareTo(FechaMax); //log.debug(comp_fecha);
												if (comp_fecha > 0 && !df_fecha_venc_nvo.equals(sdf.format(FechaMax)) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVNueva",sesIdiomaUsuario)+df_fecha_venc_nvo+","+ MensajeParam.getMensaje("13forma5.FVNSobrepasaParamDia",sesIdiomaUsuario)+" "+sdf.format(FechaMax)+".\n\n");
													registroOk = ok = false;
												}
											}
										} // else
									} else if(bFechaVencP) {
										//df_fecha_venc_p_ant
										//df_fecha_venc_p_nvo
										if(df_fecha_venc_p_ant.equals("") || df_fecha_venc_p_nvo.equals("")) {
											error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaVenPNueFormato",sesIdiomaUsuario)+"\n\n");
											registroOk = ok = false;
										} else {
											if(!Comunes.checaFecha(df_fecha_venc_p_ant)){
												error.append(msgerr +MensajeParam.getMensaje("13forma5.FechaVenPIncorret",sesIdiomaUsuario) +"\n\n");
												registroOk = ok = false;
											}
											if(!Comunes.checaFecha(df_fecha_venc_p_nvo)){
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaVenPNueIncorret",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											}
											if(!fecha_venc_pyme.equals(df_fecha_venc_p_ant)) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FVPAnteMismaFVDoc",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											} else if(fecha_venc_pyme.equals(df_fecha_venc_p_nvo)) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FVPNMismaFVDoc",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											} else {
												// Validacion de la fecha de vencimiento del proveedor que sea un dia habil con el cat�logo.
												String sLineaFec = getDiasInhabilesXanio(ic_epo, con);
												if(sLineaFec.length() > 0) {
													StringTokenizer st = new StringTokenizer(sLineaFec,";");
													while(st.hasMoreElements()) {
														//log.debug(st.nextToken());
														if(df_fecha_venc_p_nvo.equals(st.nextToken())) {
															error.append(msgerr +MensajeParam.getMensaje("13forma5.FVNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_p_nvo+"\n\n");
															registroOk = ok = false;
														}
													}
												}

												String sLineaDiaMes = getDiasInhabiles(ic_epo, con);
												if(sLineaDiaMes.length() > 0) {
													StringTokenizer st = new StringTokenizer(sLineaDiaMes,";");
													while(st.hasMoreElements()) {
														//log.debug(st.nextToken());
														if(df_fecha_venc_p_nvo.substring(0,5).equals(st.nextToken())) {
															error.append(msgerr +MensajeParam.getMensaje("13forma5.FVPNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_p_nvo+"\n\n");
															registroOk = ok = false;
														}
													}
												}
												// Validacion de la fecha de Vencimiento del proveedor que no sea un d�a inh�bil un S�bado o Domingo.
												Calendar gcDiaFecha = new GregorianCalendar();
												java.util.Date fechaVencPNvo = Comunes.parseDate(df_fecha_venc_p_nvo);
												gcDiaFecha.setTime(fechaVencPNvo);
												int no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
												if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
													error.append(msgerr +MensajeParam.getMensaje("13forma5.FVPNNoDiaHabil",sesIdiomaUsuario)+" "+df_fecha_venc_p_nvo+"\n\n");
													registroOk = ok = false;
												}
												// Validaci�n de fechas de Venimiento d�as Minimos y M�ximos.
												int comp_fecha;
												SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
												Vector vFechasVencimiento = getFechasVencimiento(ic_epo, 2, con);
												java.util.Date FechaMin 		= (java.util.Date)vFechasVencimiento.get(0);
												java.util.Date FechaMax 		= (java.util.Date)vFechasVencimiento.get(1);
												java.util.Date fechaDocto 		= Comunes.parseDate(df_fecha_docto);
												java.util.Date fechaVencDocto 	= Comunes.parseDate(fecha_venc);
												comp_fecha=fechaDocto.compareTo(fechaVencPNvo); //log.debug(comp_fecha);
												if (comp_fecha > 0 && !df_fecha_docto.equals(df_fecha_venc_p_nvo) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVPNueva",sesIdiomaUsuario)+df_fecha_venc_p_nvo+" "+MensajeParam.getMensaje("13forma5.FVNMenorFDoc",sesIdiomaUsuario)+" "+df_fecha_docto+".\n\n");
													registroOk = ok = false;
												}
												comp_fecha=fechaVencPNvo.compareTo(FechaMin); //log.debug(comp_fecha);
												if (comp_fecha < 0 && !df_fecha_venc_p_nvo.equals(sdf.format(FechaMin)) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVPNueva",sesIdiomaUsuario)+df_fecha_venc_p_nvo+", "+ MensajeParam.getMensaje("13forma5.FVNNoParametroDias",sesIdiomaUsuario)+" "+sdf.format(FechaMin)+".\n\n");
													registroOk = ok = false;
												}
												comp_fecha=fechaVencPNvo.compareTo(FechaMax); //log.debug(comp_fecha);
												if (comp_fecha > 0 && !df_fecha_venc_p_nvo.equals(sdf.format(FechaMax)) ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FVPNueva",sesIdiomaUsuario)+df_fecha_venc_p_nvo+","+ MensajeParam.getMensaje("13forma5.FVNSobrepasaParamDia",sesIdiomaUsuario)+" "+sdf.format(FechaMax)+".\n\n");
													registroOk = ok = false;
												}
												comp_fecha=fechaVencPNvo.compareTo(fechaVencDocto); //log.debug(comp_fecha);
												if (comp_fecha > 0 || comp_fecha == 0 ) {
													error.append(msgerr + ","+MensajeParam.getMensaje("13forma5.FechaVenPNueMay",sesIdiomaUsuario)+" "+df_fecha_venc_p_nvo+".\n\n");
													registroOk = ok = false;
												}
											}
										} // else
									}//bFechaVencP
								} // if(tipo.equalsIgnoreCase("V")
//============================================================================>> verifica si tiene parametrizaci�n epo - pef
									/*Modifique la validaci�n en la que originalmente eran obligatorios los campos de la fecha de entrega, el tipo de compra,
									  la clave presupuestaria y el periodo, ahora la obligatoriedad de estos campos depende de la parametrizaci�n de la EPO.*/
									if(tieneParamEpoPef(ic_epo)){
										StringBuffer strSQL = new StringBuffer();
										List varBind = new ArrayList();
										PreparedStatement pst = null;
										ResultSet rst = null;
										String param_fecha_entrega = "";
										String param_tipo_compra = "";
										String param_clave_presupuestaria = "";
										String param_periodo = "";

										strSQL.append(" SELECT cg_valor FROM com_parametrizacion_epo WHERE ic_epo = ? AND cc_parametro_epo = ?");
										varBind.add(new Integer(ic_epo));
										varBind.add("PUB_EPO_PEF_FECHA_ENTREGA");
										pst = con.queryPrecompilado(strSQL.toString(), varBind);
										rst = pst.executeQuery();
										while(rst.next()){param_fecha_entrega = rst.getString(1)==null?"":rst.getString(1);}
										rst.close();
										pst.close();

										strSQL = new StringBuffer();
										varBind = new ArrayList();
										strSQL.append(" SELECT cg_valor FROM com_parametrizacion_epo WHERE ic_epo = ? AND cc_parametro_epo = ?");
										varBind.add(new Integer(ic_epo));
										varBind.add("PUB_EPO_PEF_TIPO_COMPRA");
										pst = con.queryPrecompilado(strSQL.toString(), varBind);
										rst = pst.executeQuery();
										while(rst.next()){param_tipo_compra = rst.getString(1)==null?"":rst.getString(1);}
										rst.close();
										pst.close();

										strSQL = new StringBuffer();
										varBind = new ArrayList();
										strSQL.append(" SELECT cg_valor FROM com_parametrizacion_epo WHERE ic_epo = ? AND cc_parametro_epo = ?");
										varBind.add(new Integer(ic_epo));
										varBind.add("PUB_EPO_PEF_CLAVE_PRESUPUESTAL");
										pst = con.queryPrecompilado(strSQL.toString(), varBind);
										rst = pst.executeQuery();
										while(rst.next()){param_clave_presupuestaria = rst.getString(1)==null?"":rst.getString(1);}
										rst.close();
										pst.close();

										strSQL = new StringBuffer();
										varBind = new ArrayList();
										strSQL.append(" SELECT cg_valor FROM com_parametrizacion_epo WHERE ic_epo = ? AND cc_parametro_epo = ?");
										varBind.add(new Integer(ic_epo));
										varBind.add("PUB_EPO_PEF_PERIODO");
										pst = con.queryPrecompilado(strSQL.toString(), varBind);
										rst = pst.executeQuery();
										while(rst.next()){param_periodo = rst.getString(1)==null?"":rst.getString(1);}
										rst.close();
										pst.close();

										if(param_fecha_entrega.equals("S")){
											if(fechaEntrega.equals("")){
												error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaEntregaVacio",sesIdiomaUsuario)+".\n\n");
												registroOk = ok = false;
											}else{
												if(!Comunes.checaFecha(fechaEntrega)){
													error.append(msgerr + MensajeParam.getMensaje("13forma5.FechaEntregaIncorrecta",sesIdiomaUsuario)+"\n\n");
													registroOk = ok = false;
												}
											}
										}

										if(param_tipo_compra.equals("S")){
											if(tipoCompra.equals("")) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.TipoCompraVacio",sesIdiomaUsuario)+".\n\n");
												registroOk = ok = false;
											}else{
												strSQL = new StringBuffer();
												strSQL.append(" SELECT cc_clave FROM comcat_tipo_compra");
												rst = con.queryDB(strSQL.toString());
												boolean compra_ok = false;
												while(rst.next()){
													if(tipoCompra.equals(rst.getString(1))){compra_ok = true;}
												}
                        						rst.close();
                        						con.cierraStatement();
												if(!compra_ok){
													error.append(msgerr + MensajeParam.getMensaje("13forma5.TipoCompraIncorrecta",sesIdiomaUsuario)+"\n\n");
													registroOk = ok = false;
												}
											}
										}

										if(param_clave_presupuestaria.equals("S")){
											if(clavePresupuestaria.equals("")) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.ClavePresupuestariaVacio",sesIdiomaUsuario)+".\n\n");
												registroOk = ok = false;
											}else{
												if(clavePresupuestaria.length() > 4){
													error.append(msgerr + MensajeParam.getMensaje("13forma5.ClavePresupuestariaIncorrecta",sesIdiomaUsuario)+"\n\n");
													registroOk = ok = false;
												}
											}
										}

										if(param_periodo.equals("S")){
											if(periodo.equals("")) {
												error.append(msgerr + MensajeParam.getMensaje("13forma5.PeriodoVacio",sesIdiomaUsuario)+"\n\n");
												registroOk = ok = false;
											}else{
												if(periodo.length() > 3){
													error.append(msgerr + MensajeParam.getMensaje("13forma5.PeriodoIncorrecto",sesIdiomaUsuario)+"\n\n");
													registroOk = ok = false;
												}
												if(!Comunes.esNumeroEnteroPositivo(periodo)){
													error.append(msgerr + MensajeParam.getMensaje("13forma5.PeriodoIncorrecto",sesIdiomaUsuario)+"\n\n");
													registroOk = ok = false;
												}
											}
										}
									}
//============================================================================>>
/*
								else {
									error.append(msgerr + " no se puede cambiar la fecha de vencimiento para una Nota de Cr&eacute;dito.\n\n");
									registroOk = ok = false;
								}
*/

							} // else 5
						} // else 4
					} // else 3
          			rs.close();
					con.cierraStatement();
				} // if(cve_pyme.length()>0)
				else {
					bMontoSumado = false;
				}

				// Verifica si el monto fue sumado y total documento incrementado
				if(bMontoSumado == false) {
					if(Comunes.esDecimal(fn_monto_nvo)) {
						monto_mn = monto_mn.add(new BigDecimal(fn_monto_nvo));
					}
					totdoc_mn++;
				}
				//log.debug("monto_mn: "+monto_mn+" total_mn: "+totdoc_mn);


//Fodea 060 DESC ELEC - Baja de Prenegociables
				String firmaMancomunada ="";
				String sQuery = " select NVL (cs_pub_firma_manc, 'N') AS pub_firma_manc "+
							" from comrel_producto_epo "+
							" where ic_epo = ? "+
							" and ic_producto_nafin = ? ";
			PreparedStatement ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, 1);
			rs = ps.executeQuery();
  		if(rs.next()) {
				firmaMancomunada = (rs.getString("pub_firma_manc")==null)?"":rs.getString("pub_firma_manc").trim();
			}
			rs.close();
		  ps.close();

			log.debug("firmaMancomunada "+firmaMancomunada);

			if(firmaMancomunada.equalsIgnoreCase("N") && ic_estatus_docto ==28 ){
				error.append(msgerr +"La Epo no  Opera Publicaci�n con Firma Mancomunada"+"\n\n");
				registroOk = ok = false;
			}


				if(registroOk) {
					log.info("****** comienza modificacion *********");
				if( ic_estatus_docto !=28){
						switch(tipo.charAt(0)){
							case 'M': case 'R': case 'V': case 'D':
								estatus_nuevo = "2";
							break;
							case 'B':
								estatus_nuevo = "5";
							break;
							case 'F':
								estatus_nuevo = "6";
							break;
							case 'L':
								estatus_nuevo = "21";
							break;
							case 'P':
								estatus_nuevo = "7";
							break;



							case 'm': case 'r': case 'v': case 'd':
								estatus_nuevo = "2";
							break;
							case 'b':
								estatus_nuevo = "5";
							break;
							case 'f':
								estatus_nuevo = "6";
							break;
							case 'l':
								estatus_nuevo = "21";
							break;
							case 'p':
								estatus_nuevo = "7";
							break;

						default:
								estatus_nuevo = "2";
							break;


						}
				}

				if( ic_estatus_docto ==28){
						switch(tipo.charAt(0)){
						case 'M': case 'R': case 'V':
								estatus_nuevo = "28";
							break;
							case 'B':
								estatus_nuevo = "5";
							break;
							case 'F':
								estatus_nuevo = "6";
							break;
							case 'L':
								estatus_nuevo = "28";
							break;
							case 'P':
								estatus_nuevo = "7";
							break;


							case 'm': case 'r': case 'v':
								estatus_nuevo = "28";
							break;
							case 'b':
								estatus_nuevo = "5";
							break;
							case 'f':
								estatus_nuevo = "6";
							break;
							case 'l':
								estatus_nuevo = "28";
							break;
							case 'p':
								estatus_nuevo = "7";
							break;
							default:
								estatus_nuevo = "28";
							break;


						}
				}


					try{
						String auxMntoAnt = fn_monto_ant;
						String auxMntoNvo = fn_monto_nvo;
						if(tipo.equalsIgnoreCase("V")) {
							auxMntoAnt			 		= "NULL";
							auxMntoNvo 					= "NULL";
							if(bFechaVenc) {
								df_fecha_venc_ant 		= "TO_DATE('"+df_fecha_venc_ant+"','DD/MM/YYYY')";
								df_fecha_venc_nvo 		= "TO_DATE('"+df_fecha_venc_nvo+"','DD/MM/YYYY')";
								df_fecha_venc_p_ant 	= "NULL";
								df_fecha_venc_p_nvo 	= "NULL";
							} else if(bFechaVencP) {
								df_fecha_venc_ant 		= "NULL";
								df_fecha_venc_nvo 		= "NULL";
								df_fecha_venc_p_ant 	= "TO_DATE('"+df_fecha_venc_p_ant+"','DD/MM/YYYY')";
								df_fecha_venc_p_nvo 	= "TO_DATE('"+df_fecha_venc_p_nvo+"','DD/MM/YYYY')";
							}
						} else {
							df_fecha_venc_ant 		= "NULL";
							df_fecha_venc_nvo 		= "NULL";
							df_fecha_venc_p_ant 	= "NULL";
							df_fecha_venc_p_nvo 	= "NULL";
						}
//============================================================================>> Si no tiene parametrizaci�n epo - pef se ponen los valores en null
						if(!tieneParamEpoPef(ic_epo)){
							fechaEntrega = "NULL";
							tipoCompra = "NULL";
							clavePresupuestaria = "NULL";
							periodo = "NULL";
						}else{
							fechaEntrega = "TO_DATE('"+fechaEntrega+"', 'DD/MM/YYYY')";
							tipoCompra = "'"+tipoCompra+"'";
							clavePresupuestaria = "'"+clavePresupuestaria+"'";
						}
//============================================================================>>
						query =
							" INSERT INTO comtmp_cambio_importe ("   +
							"              ic_documento, ic_proceso_cambio, fn_monto_anterior,"   +
							"              fn_monto_nuevo, ct_causa, ic_estatus_docto,"   +
							"              df_fecha_venc_anterior, df_fecha_venc_nueva,"   +
							"              df_fecha_venc_p_anterior, df_fecha_venc_p_nueva,"   +
							"              df_entrega, cg_tipo_compra,"+											//=====>>
							"              cg_clave_presupuestaria, cg_periodo, ic_estatus_anterior"+							//=====>>
							"             )"   +
							"      VALUES ("   +
							"              "+ic_documento+", "+ic_proceso_cambio+", "+auxMntoAnt+","   +
							"              "+auxMntoNvo+", '"+causa+"', "+estatus_nuevo+","   +
							"              "+df_fecha_venc_ant+", "+df_fecha_venc_nvo+","  +
							"              "+df_fecha_venc_p_ant+", "+df_fecha_venc_p_nvo+","+
							"							 "+fechaEntrega+", "+tipoCompra+", "+clavePresupuestaria+", "+periodo+","+ic_estatus_docto+")";//=====>>

							log.debug("========================>> query = "+query);
/*
						"insert into comtmp_cambio_importe(ic_documento, ic_proceso_cambio,"+
								" fn_monto_anterior, fn_monto_nuevo, ct_causa, ic_estatus_docto";
						query+=	(tipo.equals("V"))?", df_fecha_venc_anterior, df_fecha_venc_nueva":"";
						query+=	" ) values("+ic_documento+","+ic_proceso_cambio+","+(tipo.equals("V")?"null":fn_monto_ant)+","+
								(tipo.equals("V")?"null":fn_monto_nvo)+",'"+causa+"',"+estatus_nuevo;
						query+=	(tipo.equals("V"))?",TO_DATE('"+df_fecha_venc_ant+"','DD/MM/YYYY'),TO_DATE('"+df_fecha_venc_nvo+"','DD/MM/YYYY')":"";
						query+= ")";
*/
						//log.debug(query);
						con.ejecutaSQL(query);
					}catch(SQLException sqle) {
						ok = false;
						bBotonProcesar = false;
						throw new NafinException("DSCT0074");
					}

					bBotonProcesar = true; // Hay datos que procesar

					if(ic_moneda==1) {
						totdoc_mnOK++;
						monto_tot_mnOK += new Double(fn_monto_nvo).doubleValue();
					}
					else if(ic_moneda==54) {
						totdoc_dolarOK++;
						monto_tot_dolarOK += new Double(fn_monto_nvo).doubleValue();
					}

					sinError.append(MensajeParam.getMensaje("13forma5.Linea",sesIdiomaUsuario)+lineadocs+" "+MensajeParam.getMensaje("13forma5.NoDocto",sesIdiomaUsuario)+" "+numero_docto+" "+MensajeParam.getMensaje("13forma5.PYME",sesIdiomaUsuario)+" "+cve_pyme+" - "+desc_pyme+".\n\n");
				}
				else {
					gen_archivo = true;
					lineasErrores += linea_aux+"\n";
				}
			} // else 2
			lineadocs++;
		} // while.

		// Otros Errores (de sumarizacion)
		if(otrosErr){
			int txttotdoc=Integer.parseInt(ctxttotdoc);
			if(txttotdoc > 0 && totdoc_mn != txttotdoc) {
				errorOtros.append(MensajeParam.getMensaje("13forma5.TotalDocEnMN",sesIdiomaUsuario)+" "+txttotdoc
									+" "+MensajeParam.getMensaje("13forma5.DiferenteTotalDoctosMN",sesIdiomaUsuario)+" "+totdoc_mn+".\n\n");
				ok = false;
			}
			int txttotdocdol=Integer.parseInt(ctxttotdocdol);
			if(txttotdocdol > 0 && totdoc_dolar != txttotdocdol) {
				errorOtros.append(MensajeParam.getMensaje("13forma5.TotalDocUSD",sesIdiomaUsuario)+" "+txttotdocdol
									+" "+MensajeParam.getMensaje("13forma5.DiferenteTotalDoctosUSD",sesIdiomaUsuario)+" "+totdoc_dolar+".\n\n");
				ok = false;
			}
			//log.debug("Comparacion de los Montos: "+monto_mn.compareTo(txtmtodoc));
			BigDecimal txtmtodoc=new BigDecimal(ctxtmtodoc);
			if(txttotdoc > 0 && ( monto_mn.compareTo(txtmtodoc) < 0 || monto_mn.compareTo(txtmtodoc) > 0)  ) {
				errorOtros.append(MensajeParam.getMensaje("13forma5.TotalMontoEnMN",sesIdiomaUsuario)
									+" "+ Comunes.formatoMN(txtmtodoc.toPlainString())
									+" "+ MensajeParam.getMensaje("13forma5.NoCoincideFile",sesIdiomaUsuario)
									+" "+ Comunes.formatoMN(monto_mn.toPlainString()) + ".\n\n");
				ok = false;
			}
			BigDecimal txtmtodocdol=new BigDecimal(ctxtmtodocdol);
			if(txttotdocdol > 0 && (monto_dolar.compareTo(txtmtodocdol) < 0 || monto_dolar.compareTo(txtmtodocdol) > 0) ) {
				errorOtros.append(MensajeParam.getMensaje("13forma5.TotalMontoUSD",sesIdiomaUsuario)
									+" "+ Comunes.formatoMN(txtmtodocdol.toPlainString())
									+" "+ MensajeParam.getMensaje("13forma5.NoCoincideFile",sesIdiomaUsuario)
									+" "+ Comunes.formatoMN(monto_dolar.toPlainString()) + ".\n\n");
				ok = false;
			}
		}

		if(sinError.length()<1 || errorOtros.length()>0)
			bBotonProcesar = false;


		hResultado.put("ok", new Boolean(ok));
		//hResultado.put("gen_archivo", new Boolean(gen_archivo));
		hResultado.put("lineasErrores", lineasErrores);
		hResultado.put("bBotonProcesar", new Boolean(bBotonProcesar));
		hResultado.put("error", error.toString());
		hResultado.put("errorOtros", errorOtros.toString());
		hResultado.put("sinError", sinError.toString());
		hResultado.put("totdoc_mnOK", new Integer(totdoc_mnOK));
		hResultado.put("monto_tot_mnOK", new Double(monto_tot_mnOK));
		hResultado.put("totdoc_dolarOK", new Integer(totdoc_dolarOK));
		hResultado.put("monto_tot_dolarOK", new Double(monto_tot_dolarOK));
	} catch(NafinException ne) {
		//ne.printStackTrace();
		throw ne;
	} catch(Exception e) {
		e.printStackTrace();
		log.error("Exception en procesarMantenimientoDocumentos. "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (con.hayConexionAbierta()){
			con.terminaTransaccion(bBotonProcesar);
			con.cierraConexionDB();
		}
	}
	return hResultado;
	}

	public Vector getDoctosCambioImporteTmp(String sProcesoCambio) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vDoctosMant = new Vector();
	Vector vdm = null;
		try{
			con.conexionDB();
			String query =
				" SELECT   pe.cg_pyme_epo_interno, d.ig_numero_docto, fn_monto_anterior,"   +
				"          fn_monto_nuevo, ct_causa,"   +
				"          DECODE ("   +
				"             ed1.ic_estatus_docto,"   +
				"             23, ed3.cd_descripcion,"   +
				"             ed1.cd_descripcion"   +
				"          ),"   +
				"          ed2.cd_descripcion, TO_CHAR (i.df_fecha_venc_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_nueva, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_p_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_p_nueva, 'DD/MM/YYYY'),"   +
//>>============================================================================ FODEA 050 - 20008
				"          TO_CHAR (i.df_entrega, 'DD/MM/YYYY'),"   +
				//"        i.cg_tipo_compra, "   +
				"          tc.cg_descripcion, "   +
				"          i.cg_clave_presupuestaria,"   +
				"          i.cg_periodo"   +
//============================================================================<<
				"     FROM com_documento d,"   +
				"          comtmp_cambio_importe i,"   +
				"          comrel_pyme_epo pe,"   +
				"          comcat_estatus_docto ed1,"   +
				"          comcat_estatus_docto ed2,"   +
				"          comcat_estatus_docto ed3"   +
				"          ,comcat_tipo_compra tc"   +
				"    WHERE i.ic_proceso_cambio = "+sProcesoCambio+
				"      AND d.ic_documento = i.ic_documento"   +
				"      AND d.ic_pyme = pe.ic_pyme"   +
				"      AND d.ic_estatus_docto = ed1.ic_estatus_docto"   +
				"      AND i.ic_estatus_docto = ed2.ic_estatus_docto"   +
				"	 	 AND i.cg_tipo_compra = tc.cc_clave(+)"+
				"      AND ed3.ic_estatus_docto = 2"   +
				"      AND d.ic_epo = pe.ic_epo"   +
				" ORDER BY d.ic_moneda"  ;
			ResultSet rs = con.queryDB(query);
			while (rs.next()) {
				vdm = new Vector();
				vdm.add(rs.getString(1).trim());
				vdm.add(rs.getString(2).trim());
				vdm.add((rs.getString(3)==null?"":rs.getString(3).trim()));
				vdm.add((rs.getString(4)==null?"":rs.getString(4).trim()));
				vdm.add(rs.getString(5).trim());
				vdm.add(rs.getString(6).trim());
				vdm.add(rs.getString(7).trim());
				vdm.add((rs.getString(8)==null?"":rs.getString(8).trim()));
				vdm.add((rs.getString(9)==null?"":rs.getString(9).trim()));
				vdm.add((rs.getString(10)==null?"":rs.getString(10).trim()));
				vdm.add((rs.getString(11)==null?"":rs.getString(11).trim()));
//>>============================================================================ FODEA 050 - 2008
				vdm.add((rs.getString(12)==null?"":rs.getString(12).trim()));
				vdm.add((rs.getString(13)==null?"":rs.getString(13).trim()));
				vdm.add((rs.getString(14)==null?"":rs.getString(14).trim()));
				vdm.add((rs.getString(15)==null?"":rs.getString(15).trim()));
//==============================================================================<<
				vDoctosMant.addElement(vdm);
			}
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDoctosCambioImporteTmp. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vDoctosMant;
	}

	public boolean borraDoctosCambioImporte(String sProcDocto) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bBien = true;
		try{
			con.conexionDB();
			String query = "delete from comtmp_cambio_importe where ic_proceso_cambio="+sProcDocto;
			con.ejecutaSQL(query);
		}
		catch(Exception e) {
			bBien = false;
			log.error("Exception en borraDoctosCambioImporte. "+e);
			throw new NafinException("SIST0059");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bBien;
	}

	public void insertaDoctosCambioEstatus(String no_acuse, String totdoc, String mtodoc,
											String totdocdol, String mtodocdol,
											String noUsuario, String _acuse,
											String procDocto, String usuario) throws NafinException {//FODEA 015 - 2009 ACF
	AccesoDB con = new AccesoDB();
	String query="";
	boolean bBien = true;
		try{
			con.conexionDB();
			String ic_estatus_anterior ="";
			insertaAcuse4(no_acuse, totdoc, mtodoc, totdocdol, mtodocdol, noUsuario, _acuse, con);
			// Obtiene los datos de la tabla temporal comtmp_cambio_importe.
			query =
				" SELECT ic_documento, ct_causa, fn_monto_anterior, fn_monto_nuevo,"   +
				"        ic_estatus_docto, TO_CHAR (df_fecha_venc_anterior, 'DD/MM/YYYY'),"   +
				"        TO_CHAR (df_fecha_venc_nueva, 'DD/MM/YYYY'),"   +
				"        TO_CHAR (df_fecha_venc_p_anterior, 'DD/MM/YYYY'),"   +
				"        TO_CHAR (df_fecha_venc_p_nueva, 'DD/MM/YYYY'),"+
//>>============================================================================ FODEA 050 - 2008
				"        TO_CHAR (df_entrega, 'DD/MM/YYYY'),"+
				"        cg_tipo_compra,"+
				"        cg_clave_presupuestaria,"+
				"        cg_periodo"+
				"        ,ic_estatus_anterior"+
//>>==========================================================================<<
				"   FROM comtmp_cambio_importe"   +
				"  WHERE ic_proceso_cambio = "+procDocto+" "  ;
			log.debug("=========================>> query"+query);
			String ic_documento="", causa="", fn_monto_ant="", fn_monto_nvo="", ic_estatus_nuevo="", ic_cambio_estatus="", fecha_venc_anterior="", fecha_venc_nueva="", fecha_venc_p_anterior="", fecha_venc_p_nueva="";
//>>============================================================================ FODEA 050 - 2008
			String fechaEntrega = "";
			String tipoCompra = "";
			String clavePresupuestaria = "";
			String periodo = "";
//============================================================================<<
			ResultSet rs = con.queryDB(query);
			while (rs.next()) {
				ic_documento 			= rs.getString(1);
				causa 					= (rs.getString(2)==null)?"null":"'"+rs.getString(2)+"'";
				fn_monto_ant 			= (rs.getString(3)==null)?"":rs.getString(3);
				fn_monto_nvo 			= (rs.getString(4)==null)?"":rs.getString(4);
				ic_estatus_nuevo 		= (rs.getString(5)==null)?"":rs.getString(5);
				fecha_venc_anterior 	= (rs.getString(6)==null)?"":rs.getString(6);
				fecha_venc_nueva 		= (rs.getString(7)==null)?"":rs.getString(7);
				fecha_venc_p_anterior 	= (rs.getString(8)==null)?"":rs.getString(8);
				fecha_venc_p_nueva 		= (rs.getString(9)==null)?"":rs.getString(9);
//>>============================================================================ FODEA 050 - 2008
				fechaEntrega = (rs.getString(10)==null)?"":rs.getString(10);
				tipoCompra = (rs.getString(11)==null)?"":rs.getString(11);
				clavePresupuestaria = (rs.getString(12)==null)?"":rs.getString(12);
				periodo = (rs.getString(13)==null)?"":rs.getString(13);
				ic_estatus_anterior = (rs.getString(14)==null)?"":rs.getString(14);
//============================================================================<<


		log.debug("=========>> ic_estatus_anterior===================="+ic_estatus_anterior);
		log.debug("=========>> ic_estatus_nuevo===================="+ic_estatus_nuevo);


				if(ic_estatus_nuevo.equals("2") && (!fn_monto_ant.equals(fn_monto_nvo) || !fecha_venc_anterior.equals(fecha_venc_nueva) || !fecha_venc_p_anterior.equals(fecha_venc_p_nueva)) )	//Modificaci�n de Montos
					ic_cambio_estatus = "8";
				else if(ic_estatus_nuevo.equals("2"))	//Bloqueado a Negociable
					ic_cambio_estatus = "16";
				else if(ic_estatus_nuevo.equals("21"))	//Negociable a Bloqueado
					ic_cambio_estatus = "15";
				else if(ic_estatus_nuevo.equals("5"))	//Negociable a Baja
					ic_cambio_estatus = "4";
				else if(ic_estatus_nuevo.equals("6"))	//Negociable a Descuento F�sico
					ic_cambio_estatus = "5";
				else if(ic_estatus_nuevo.equals("7"))	//Negociable a Pagado Anticipado
					ic_cambio_estatus = "6";
				// Inserta los valores en comhis_cambio_estatus
				if(ic_estatus_anterior.equals("28") && ic_estatus_nuevo.equals("5")){
					ic_cambio_estatus = "37";
				}else	if(ic_estatus_anterior.equals("28") && ic_estatus_nuevo.equals("6")){
					ic_cambio_estatus = "38";
				}else	if(ic_estatus_anterior.equals("28") && ic_estatus_nuevo.equals("7")){
					ic_cambio_estatus = "39";
				}else if(ic_estatus_anterior.equals("28") && ic_estatus_nuevo.equals("28")){
					ic_cambio_estatus = "21";
				}else if(ic_estatus_anterior.equals("33") && ic_estatus_nuevo.equals("2")){
					ic_cambio_estatus = "45";
				}else if(ic_estatus_anterior.equals("33") && ic_estatus_nuevo.equals("5")){
					ic_cambio_estatus = "46";
				}


				fn_monto_ant 			= ("".equals(fn_monto_ant))?"NULL":fn_monto_ant;
				fn_monto_nvo 			= ("".equals(fn_monto_nvo))?"NULL":fn_monto_nvo;
				fecha_venc_anterior 	= ("".equals(fecha_venc_anterior))?"NULL":"TO_DATE ('"+fecha_venc_anterior+"', 'dd/mm/yyyy')";
				fecha_venc_nueva 		= ("".equals(fecha_venc_nueva))?"NULL":"TO_DATE ('"+fecha_venc_nueva+"', 'dd/mm/yyyy')";
				fecha_venc_p_anterior 	= ("".equals(fecha_venc_p_anterior))?"NULL":"TO_DATE ('"+fecha_venc_p_anterior+"', 'dd/mm/yyyy')";
				fecha_venc_p_nueva 		= ("".equals(fecha_venc_p_nueva))?"NULL":"TO_DATE ('"+fecha_venc_p_nueva+"', 'dd/mm/yyyy')";
//>>============================================================================ FODEA 050 - 2008
				fechaEntrega = fechaEntrega.equals("")?"NULL":"TO_DATE('"+fechaEntrega+"', 'DD/MM/YYYY')";
				tipoCompra = tipoCompra.equals("")?"NULL":"'"+tipoCompra+"'";
				clavePresupuestaria = clavePresupuestaria.equals("")?"NULL":"'"+clavePresupuestaria+"'";
				periodo = periodo.equals("")?"NULL":periodo;
//>>==========================================================================<<
				String auxCambioImp = "S";
				if(fn_monto_ant.equals(fn_monto_nvo) && fecha_venc_anterior.equals(fecha_venc_nueva) && fecha_venc_p_anterior.equals(fecha_venc_p_nueva)) {
					auxCambioImp = "N";
				}

				query =
					" INSERT INTO comhis_cambio_estatus ("   +
					"              dc_fecha_cambio, ic_documento, ic_cambio_estatus,"   +
					"              ct_cambio_motivo, cc_acuse, fn_monto_anterior, fn_monto_nuevo,"   +
					"              df_fecha_venc_anterior,"   +
					"              df_fecha_venc_nueva,"   +
					"              df_fecha_venc_p_anterior,"   +
					"              df_fecha_venc_p_nueva,"   +
          "              cg_nombre_usuario"   +//FODEA 015 - 2009 ACF
					"             )"   +
					"      VALUES ("   +
					"              SYSDATE, "+ic_documento+", "+ic_cambio_estatus+","   +
					"              "+causa+", '"+no_acuse+"', "+fn_monto_ant+", "+fn_monto_nvo+","   +
					"              "+fecha_venc_anterior+","   +
					"              "+fecha_venc_nueva+","   +
					"              "+fecha_venc_p_anterior+","   +
					"              "+fecha_venc_p_nueva+", "   +//FODEA 015 - 2009 ACF
          "              '"+usuario+"' "   +//FODEA 015 - 2009 ACF
					"             )"  ;
				log.debug("..:: query : "+query);
				con.ejecutaSQL(query);

				//update a la tabla com_documento

				String condicion = "";
				if(!"NULL".equals(fn_monto_nvo)) {
					condicion = " fn_monto = "+fn_monto_nvo+", ";
				}
				if(!"NULL".equals(fecha_venc_nueva)) {
					condicion = " df_fecha_venc = "+fecha_venc_nueva+", ";
				}
				if(!"NULL".equals(fecha_venc_p_nueva)) {
					condicion = " df_fecha_venc_pyme = "+fecha_venc_p_nueva+", ";
				}

				query =
					" UPDATE com_documento"   +
					"    SET "+condicion;
//>>============================================================================ FODEA 050 - 2008
					if(!fechaEntrega.equals("NULL")){query += " df_entrega = "+fechaEntrega+",";}
					if(!tipoCompra.equals("NULL")){query += " cg_tipo_compra = "+tipoCompra+",";}
					if(!clavePresupuestaria.equals("NULL")){query += " cg_clave_presupuestaria = "+clavePresupuestaria+",";}
					if(!periodo.equals("NULL")){query += " cg_periodo = "+periodo+",";}
//>>==========================================================================<<
					query += "        cs_cambio_importe = '"+auxCambioImp+"',"   +
					"        ic_estatus_docto = "+ic_estatus_nuevo+" "   +
					"  WHERE ic_documento = "+ic_documento+" "  ;
				log.debug(query);
				con.ejecutaSQL(query);
			}
			con.cierraStatement();

		}
		catch(Exception e) {
			bBien = false;
			log.error("Exception en insertaDoctosCambioEstatus. "+e);
			throw new NafinException("SIST0047");
		}
		finally {
			con.terminaTransaccion(bBien);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	//return bBien;
	}

	private boolean insertaAcuse4(String no_acuse, String totdoc, String mtodoc,
									String totdocdol, String mtodocdol, String noUsuario,
									String _acuse, AccesoDB con) throws NafinException {
	boolean bBien=true;
	try {
		String query = "insert into com_acuse4(cc_acuse, in_total_docto_mn, fn_total_monto_mn, "+
				"df_fecha_hora_carga, in_total_docto_dl, fn_total_monto_dl, ic_usuario, "+
				"cg_recibo_electronico) "+
				"values('"+no_acuse+"', "+totdoc+", "+mtodoc+", SYSDATE, "+totdocdol+", "+
				mtodocdol+", '"+noUsuario+"', '"+_acuse+"')";
		//log.debug(query);
		con.ejecutaSQL(query);
	} catch(Exception e) {
		bBien=false;
		log.error("Exception en insertaAcuse4. "+e);
		throw new NafinException("DSCT0010");
	}
	return bBien;
	}


	public Vector getDoctosCambioImporte(String no_acuse,String sesIdiomaUsuario) throws NafinException {
	AccesoDB con = new AccesoDB();
	Vector vDoctosMant = new Vector();
	Vector vdm = null;
	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try{
			con.conexionDB();
			String query =
				" SELECT   pe.cg_pyme_epo_interno, d.ig_numero_docto, fn_monto_anterior,"   +
				"          fn_monto_nuevo, ct_cambio_motivo, ce.cd_descripcion"+idioma+" AS cd_descripcion,"   +
				"          TO_CHAR (df_fecha_venc_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_nueva, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_p_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_p_nueva, 'DD/MM/YYYY'),"   +
//>>============================================================================ FODEA 050 - 2008
				"        	 TO_CHAR (d.df_entrega, 'DD/MM/YYYY'),"+
				" 				tc.cg_descripcion,"+
				//"        	 d.cg_tipo_compra,"+
				"        	 d.cg_clave_presupuestaria,"+
				"        	 d.cg_periodo,"+
//>>==========================================================================<<
				"        	 c.cg_nombre_usuario"+//FODEA 015 - 2009
				"     FROM com_documento d,"   +
				"          comhis_cambio_estatus c,"   +
				"          comrel_pyme_epo pe,"   +
				"          comcat_cambio_estatus ce"   +
				"			  ,comcat_tipo_compra tc"+
				"    WHERE c.cc_acuse = '"+no_acuse+"'"+
				"      AND d.ic_documento = c.ic_documento"   +
				"      AND d.ic_pyme = pe.ic_pyme"   +
				"      AND c.ic_cambio_estatus = ce.ic_cambio_estatus"   +
				"      AND d.ic_epo = pe.ic_epo"   +
				"		 AND d.cg_tipo_compra = tc.cc_clave(+)"+
				" ORDER BY d.ic_moneda"  ;
			ResultSet rs = con.queryDB(query);
			log.debug("QUERY getDoctosCambioImporte"+query);
			while (rs.next()) {
				vdm = new Vector();
				vdm.add(rs.getString(1).trim());
				vdm.add(rs.getString(2).trim());
				vdm.add((rs.getString(3)==null?"":rs.getString(3).trim()));
				vdm.add((rs.getString(4)==null?"":rs.getString(4).trim()));
				vdm.add((rs.getString(5)==null?"":rs.getString(5).trim()));
				vdm.add(rs.getString(6).trim());
				vdm.add((rs.getString(7)==null?"":rs.getString(7).trim()));
				vdm.add((rs.getString(8)==null?"":rs.getString(8).trim()));
				vdm.add((rs.getString(9)==null?"":rs.getString(9).trim()));
				vdm.add((rs.getString(10)==null?"":rs.getString(10).trim()));
//>>============================================================================ FODEA 050 - 2008
				vdm.add((rs.getString(11)==null?"":rs.getString(11).trim()));
				vdm.add((rs.getString(12)==null?"":rs.getString(12).trim()));
				vdm.add((rs.getString(13)==null?"":rs.getString(13).trim()));
				vdm.add((rs.getString(14)==null?"":rs.getString(14).trim()));
        vdm.add((rs.getString(15)==null?"":rs.getString(15).trim())); //FODEA 015 - 2009 ACF
//>>==========================================================================<<
				vDoctosMant.addElement(vdm);
			}
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDoctosCambioImporte. "+e);
			throw new NafinException("SIST0060");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vDoctosMant;
	}

       /*-- Metodos asignados a la clase para el nuevo proceso de Carga Masiva --*/


    ////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                    //
    //        Insertar un registro dentro de la tabla COMTMP_CARGA_DOCTO para las cargas  //
    //        Masivas Utilizando la conexion del metodo create().                                                                    //
    //                                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////

	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario, String NoMandante) throws NafinException {
		try{
			insertaDocumentoCM(
					sIcPyme, 			sIgNumeroDocto,		sDfFechaDocto,
					sDfFechaVenc, 		sIcMoneda,			sFnMonto,
					sCsDsctoEspecial, 	sCtReferencia,		sCampAd1,
					sCampAd2, 			sCampAd3,			sCampAd4,
					sCampAd5, 			sNoBeneficiario,	sPorcBeneficiario,
					sMontoBeneficiario, iNumProceso, 		sStatusRegistro,
					sMensajeError, 		iNumLinea, 			iNumCampos,
					sIcBeneficiario,	"", NoMandante);
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			log.error("Exception en insertaDocumento. "+e);
			throw new NafinException("SIST0001");
		}
	}

	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario,		String sDfFechaVencPyme, String NoMandante) throws NafinException {
		boolean bOK = true;
		AccesoDB con = new AccesoDB();
		List varBind       = new ArrayList();
		PreparedStatement ps = null;
		

		log.info("CargaDocumentoEJB:: insertaDocumentoCM::(E)");
	//	log.debug("-------------Para Carga Masiva----------------");

		StringBuffer strSQL = new StringBuffer();

		try{
			con.conexionDB();

			strSQL.append("	INSERT INTO comtmp_carga_docto ("   +
				" cg_pyme_epo_interno, ig_numero_docto, df_fecha_docto,"   +
				" df_fecha_venc, ic_moneda, fn_monto, cs_dscto_especial,"   +
				" ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
				" cg_campo5, ic_if_benef, fn_porc_beneficiario,"   +
				" fn_monto_beneficiario, ic_carga_docto, cg_estatus, cg_error_reg,"   +
				" ic_linea, ig_numero_campos, ic_benef, df_fecha_venc_pyme, ic_mandante )"+
				"  VALUES ( ?, ? , ?, ?, ?, ? , ?, ? , ? , ? , ? , ?, ? , ? , ? , ? , ? , ?, ? , ? , ? , ? ,? , ? ) " );

			varBind.add(sIcPyme);
			varBind.add(sIgNumeroDocto);
			varBind.add(sDfFechaDocto);
			varBind.add(sDfFechaVenc);
			varBind.add(sIcMoneda);
			varBind.add(sFnMonto);
			varBind.add(sCsDsctoEspecial);
			varBind.add(sCtReferencia);
			varBind.add(sCampAd1);
			varBind.add(sCampAd2);
			varBind.add(sCampAd3);
			varBind.add(sCampAd4);
			varBind.add(sCampAd5);
			varBind.add(sNoBeneficiario);
			varBind.add(sPorcBeneficiario);
			varBind.add(sMontoBeneficiario);
			varBind.add(String.valueOf(iNumProceso));
			varBind.add(sStatusRegistro);
			varBind.add(sMensajeError);
			varBind.add(String.valueOf(iNumLinea));
			varBind.add(String.valueOf(iNumCampos));
			varBind.add(sIcBeneficiario);
			varBind.add(sDfFechaVencPyme);
			varBind.add(NoMandante);

			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			ps.executeUpdate();
			ps.close();

		  //log.debug("-----strSQL.toString()-------"+strSQL.toString());
		  //log.debug("-----varBind-------"+varBind);

		} catch(Exception e) {
			bOK = false;
			log.error("CargaDocumentoEJB::insertaDocumentoCM(Exception): "+e);
			e.printStackTrace();
			throw new NafinException("DSCT0067");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CargaDocumentoEJB:: insertaDocumentoCM::(S)");
		}
	}//fin insertaDocumentoCM


    ////////////////////////////////////////////////////////////////////////////////////////
    //                                                                                    //
    //        M�todo sobrecarcado para cargar toda la informaci�n del archivo  			  //
    //        en la tabla COMTMP_CARGA_DOCTO.                                  			  //
    //                                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////
	public void insertaDocumentoCM(String esPathFile,int esNumProceso) throws NafinException {
		String sIcPyme = "", sIgNumeroDocto = "";
		String sDfFechaDocto = "", sDfFechaVenc = "", sIcMoneda = "";
		String sFnMonto = "", sCsDsctoEspecial = "", sCtReferencia = "";
		String sCampAd1 = "", sCampAd2 = "", sCampAd3 = "";
		String sCampAd4 = "", sCampAd5 = "", sNoBeneficiario = "", sPorcBeneficiario = "";
		String sMontoBeneficiario = "", sStatusRegistro = "", sMensajeError = "";
		int iNumLinea = 0,iNumCampos = 0,iNumProceso = 0;
		String sIcBeneficiario = "", query,linea = "",MensajeError = "",StatusRegistro = "";
		java.io.File f = new java.io.File(esPathFile);
		VectorTokenizer vt = null;
		Vector vecdat = null;
		AccesoDB con = new AccesoDB();

		log.info("CargaDocumentoEJB:: insertaDocumentoCM::(E)");

		  log.debug("----------------2-------------------------------------");
		try{
			con.conexionDB();
			log.info("Paso1");

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));

			log.info("Paso2");
			while((linea=br.readLine())!=null) {
				log.info("Paso3");
				try {
					/*-- Asignacion de la informacion del archivo a las variables --*/
					iNumLinea ++ ;
					vt = new VectorTokenizer(linea,"|");
					vecdat = vt.getValuesVector();
					sIcPyme = (vecdat.size()>=1)?Comunes.quitaComitasSimples(vecdat.get(0).toString()).trim():"";
					sIgNumeroDocto = (vecdat.size()>=2)?Comunes.quitaComitasSimples(vecdat.get(1).toString()).trim():"";
					sDfFechaDocto = (vecdat.size()>=3)?Comunes.quitaComitasSimples(vecdat.get(2).toString()).trim():"";
					sDfFechaVenc = (vecdat.size()>=4)?Comunes.quitaComitasSimples(vecdat.get(3).toString()).trim():"";
					sIcMoneda = (vecdat.size()>=5)?Comunes.quitaComitasSimples(vecdat.get(4).toString()).trim():"";
					sFnMonto = (vecdat.size()>=6)?Comunes.quitaComitasSimples(vecdat.get(5).toString()).trim():"";
					sCsDsctoEspecial = (vecdat.size()>=7)?Comunes.quitaComitasSimples(vecdat.get(6).toString()).trim():"";
					sCtReferencia = (vecdat.size()>=8)?Comunes.quitaComitasSimples(vecdat.get(7).toString()).trim():"";
					sCampAd1 = (vecdat.size()>=9)?Comunes.quitaComitasSimples(vecdat.get(8).toString()).trim():"";
					sCampAd2 = (vecdat.size()>=10)?Comunes.quitaComitasSimples(vecdat.get(9).toString()).trim():"";
					sCampAd3 = (vecdat.size()>=11)?Comunes.quitaComitasSimples(vecdat.get(10).toString()).trim():"";
					sCampAd4 = (vecdat.size()>=12)?Comunes.quitaComitasSimples(vecdat.get(11).toString()).trim():"";
					sCampAd5 = (vecdat.size()>=13)?Comunes.quitaComitasSimples(vecdat.get(12).toString()).trim():"";
					sNoBeneficiario = (vecdat.size()>=14)?Comunes.quitaComitasSimples(vecdat.get(13).toString()).trim():"";
					sIcBeneficiario = (vecdat.size()>=15)?Comunes.quitaComitasSimples(vecdat.get(14).toString()).trim():"";
					sPorcBeneficiario = (vecdat.size()>=16)?Comunes.quitaComitasSimples(vecdat.get(15).toString()).trim():"";
					sMontoBeneficiario = (vecdat.size()>=17)?Comunes.quitaComitasSimples(vecdat.get(16).toString()).trim():"";

					/*-- En caso de que el registro no contenga el numero minimo de campos solicitados --*/
					if(vecdat.size() < 7) {
						MensajeError = "El registro no contiene el numero minimo de campos";
						StatusRegistro = "ER";
					}
					else
						StatusRegistro = "OK";


					query = " INSERT INTO COMTMP_CARGA_DOCTO ( CG_PYME_EPO_INTERNO,IG_NUMERO_DOCTO,DF_FECHA_DOCTO, "   +
								" DF_FECHA_VENC,IC_MONEDA,FN_MONTO,CS_DSCTO_ESPECIAL,CT_REFERENCIA,CG_CAMPO1, "   +
								" CG_CAMPO2,CG_CAMPO3,CG_CAMPO4,CG_CAMPO5,IC_IF_BENEF,FN_PORC_BENEFICIARIO, "   +
								" FN_MONTO_BENEFICIARIO, IC_CARGA_DOCTO,CG_ESTATUS,CG_ERROR_REG, IC_LINEA, IG_NUMERO_CAMPOS, IC_BENEF )" +
								" VALUES ('" + sIcPyme + "','" + sIgNumeroDocto + "','" + sDfFechaDocto + "','" + sDfFechaVenc+ "','" + sIcMoneda + "','" + sFnMonto +
									"','" + sCsDsctoEspecial + "','" + sCtReferencia + "','" + sCampAd1 + "','" + sCampAd2 + "','" + sCampAd3 + "','" + sCampAd4 +
								"','" + sCampAd5 + "','" + sNoBeneficiario + "','" + sPorcBeneficiario + "','" + sMontoBeneficiario +
								"'," + iNumProceso + ",'" + sStatusRegistro + "','" + sMensajeError + "'," + iNumLinea + ","+iNumCampos+ ",'"+sIcBeneficiario+"')";

					try{
						con.ejecutaSQL(query);
						con.terminaTransaccion(true);
					}
					catch(SQLException sqle) {
						con.terminaTransaccion(false);
						throw new NafinException("DSCT0067");
					}
				} catch (StringIndexOutOfBoundsException sioobe) {
					log.error("Exception Linea sin valores. "+sioobe);
				}
			} // while.
		}
		catch (FileNotFoundException fnfe) {
			log.error("Exception del archivo. "+fnfe);
		}
		catch(NafinException ne) {
			throw ne;
		}
		catch(Exception e) {
			log.error("Exception en insertaDocumento. "+e);
			throw new NafinException("SIST0001");
		}
//****************************************************************************************
		finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
//****************************************************************************************
	}//fin insertaDocumentoCM




	////////////////////////////////////////////////////////////////////////////////////////
	//                                                                                    //
	//      Obtencion de informacion sobre una carga masiva en la tabla BIT_CARGA_DOCTO.  //
	//                                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////
	public Vector mostrarDocumentosBitacoraCM (int NumProceso, String doctoDuplicados  ) throws NafinException {
	AccesoDB con = new AccesoDB();
	
	Vector vd = null;
	log.info("ENTRA A  mostrarDocumentosBitacoraCM:::::");
	StringBuffer 	query 	= new StringBuffer();
	
		try{
			con.conexionDB();


			/*-- Obtencion del status del procesamiento del archivo por el Store Procedure --*/
			query.append(" SELECT CG_ESTATUS_PROC,IG_NUM_CORRECTOS,IG_NUM_ERRORES,CG_ERRORES_CTL,"   +

					"        FN_MONTO_TOTAL_NEG  , "+
					"        FN_MONTO_TOTAL_NEG_DL , " +

					"        FN_MONTO_TOTAL_NONEG , "+
					"        FN_MONTO_TOTAL_NONEG_DL, "+

					" 			FN_MONTO_TOTAL_ERR, " +
					"        FN_MONTO_TOTAL_ERR_DL, " +

					"        FN_MONTO_TOTAL_NEG + FN_MONTO_TOTAL_NONEG + FN_MONTO_TOTAL_VENC  AS TOTAL,"  +
					"        FN_MONTO_TOTAL_NEG_DL + FN_MONTO_TOTAL_NONEG_DL + FN_MONTO_TOTAL_VENC_DL   AS TOTALDL,  "   +

					"        IG_NUM_CORRECTOS_DL, "+

					"        FN_MONTO_TOTAL_VENC,  "+
					"        FN_MONTO_TOTAL_VENC_DL,  " );
					// F038-2014
					if(doctoDuplicados.equals("S")){
					query.append(" FN_MONTO_TOTAL_NEG  + FN_MONTO_TOTAL_DUP  AS FN_TNEGO_MN, "+
									 "  FN_MONTO_TOTAL_NEG_DL + FN_MONTO_TOTAL_DUP_DL  AS FN_TNEGO_DL, " +
					             " FN_MONTO_TOTAL_NEG + FN_MONTO_TOTAL_NONEG + FN_MONTO_TOTAL_VENC  + FN_MONTO_TOTAL_DUP  AS TOTAL_MN,"  +
									 " FN_MONTO_TOTAL_NEG_DL + FN_MONTO_TOTAL_NONEG_DL + FN_MONTO_TOTAL_VENC_DL    + FN_MONTO_TOTAL_DUP_DL  AS TOTAL_DL,  "   );

					}else 	if(doctoDuplicados.equals("N")){
						query.append(" FN_MONTO_TOTAL_NEG  AS FN_TNEGO_MN, "+
										 "  FN_MONTO_TOTAL_NEG_DL  AS FN_TNEGO_DL, " +
										 "  FN_MONTO_TOTAL_NEG + FN_MONTO_TOTAL_NONEG + FN_MONTO_TOTAL_VENC   AS TOTAL_MN,"  +
										 " FN_MONTO_TOTAL_NEG_DL + FN_MONTO_TOTAL_NONEG_DL + FN_MONTO_TOTAL_VENC_DL     AS TOTAL_DL,  "   );

					}

					query.append("  IG_NUM_DUPLICADO, IG_NUM_DUPLICADO_dl, FN_MONTO_TOTAL_DUP, FN_MONTO_TOTAL_DUP_DL   "+		// F038-2014

					" FROM BIT_CARGA_DOCTO"   +
					" WHERE IC_CARGA_DOCTO = " + NumProceso)   ;

			log.debug("Query bicatora:::::"+query);
			ResultSet rs = con.queryDB(query.toString());
			vd = new Vector();
			if (rs.next()) {
				vd.add(rs.getString("CG_ESTATUS_PROC")); /*0*/
				vd.add((rs.getString("IG_NUM_CORRECTOS")==null?"":rs.getString("IG_NUM_CORRECTOS")));  /*1*/
				vd.add((rs.getString("IG_NUM_ERRORES")==null?"0":rs.getString("IG_NUM_ERRORES")));   /*2*/
				vd.add((rs.getString("CG_ERRORES_CTL")==null?"":rs.getString("CG_ERRORES_CTL")));  /*3*/
				vd.add((rs.getString("FN_MONTO_TOTAL_NEG")==null?"0":rs.getString("FN_MONTO_TOTAL_NEG")));   /*4*/
				vd.add((rs.getString("FN_MONTO_TOTAL_NONEG")==null?"0":rs.getString("FN_MONTO_TOTAL_NONEG")));  /*5*/
				vd.add((rs.getString("FN_MONTO_TOTAL_NEG_DL")==null?"0":rs.getString("FN_MONTO_TOTAL_NEG_DL")));  /*6*/
				vd.add((rs.getString("FN_MONTO_TOTAL_NONEG_DL")==null?"0":rs.getString("FN_MONTO_TOTAL_NONEG_DL"))); /*7*/
				vd.add((rs.getString("FN_MONTO_TOTAL_ERR")==null?"0":rs.getString("FN_MONTO_TOTAL_ERR")));  /*8*/
				vd.add((rs.getString("FN_MONTO_TOTAL_ERR_DL")==null?"0":rs.getString("FN_MONTO_TOTAL_ERR_DL")));  /*9*/
				vd.add((rs.getString("TOTAL")==null?"0":rs.getString("TOTAL")));  /*10*/
				vd.add((rs.getString("TOTALDL")==null?"0":rs.getString("TOTALDL"))); /*11*/
				vd.add((rs.getString("IG_NUM_CORRECTOS_DL")==null?"0":rs.getString("IG_NUM_CORRECTOS_DL")));  /*12*/
				vd.add((rs.getString("FN_MONTO_TOTAL_VENC")==null?"0":rs.getString("FN_MONTO_TOTAL_VENC")));  /*13*/
				vd.add((rs.getString("FN_MONTO_TOTAL_VENC_DL")==null?"0":rs.getString("FN_MONTO_TOTAL_VENC_DL")));  /*14*/

					// F038-2014
				vd.add((rs.getString("IG_NUM_DUPLICADO")==null?"0":rs.getString("IG_NUM_DUPLICADO")));  /*15*/
				vd.add((rs.getString("IG_NUM_DUPLICADO_dl")==null?"0":rs.getString("IG_NUM_DUPLICADO_dl")));  /*16*/
				vd.add((rs.getString("FN_MONTO_TOTAL_DUP")==null?"0":rs.getString("FN_MONTO_TOTAL_DUP"))); /*16*/
				vd.add((rs.getString("FN_MONTO_TOTAL_DUP_DL")==null?"0":rs.getString("FN_MONTO_TOTAL_DUP_DL")));  /*18*/
				vd.add((rs.getString("FN_TNEGO_MN")==null?"0":rs.getString("FN_TNEGO_MN")));   /*19*/
				vd.add((rs.getString("FN_TNEGO_DL")==null?"0":rs.getString("FN_TNEGO_DL")));  /*20*/
				vd.add((rs.getString("TOTAL_MN")==null?"0":rs.getString("TOTAL_MN")));   /*21*/
				vd.add((rs.getString("TOTAL_DL")==null?"0":rs.getString("TOTAL_DL")));  /*22*/
				// F038-2014

				log.debug("Vector::::::"+vd);

			}
			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en mostrarDocumentosBitacoraCM. "+e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vd;

	}//Fin de mostrarDocumentosBitacoraCM


	/**
	 * Obtencion del resumen de la carga masiva por cada registro,
	 * asi como el archivo.
	 * @param iNumProceso Numero de proceso
	 * @param idiomaUsuario Idioma del usuario
	 * @return Lista con 3 elementos
	 * 		0 Lineas sin error
	 * 		1 Lineas con error
	 * 		2 Archivo de error
	 *
	 */
	public Vector ObtenerDetalleCargaM (int iNumProceso, String idiomaUsuario)
			throws NafinException {
		AccesoDB con = new AccesoDB();
		Vector vd = null;
		// Ejecucion del query para obtener los reg
		String sLinea = "", sPyme = "", sNumDocto = "", sEstatus = "", sError = "", slineasErrores = "", csCaracterEspecial = "";
		StringBuffer sLineasError    = new StringBuffer();
		StringBuffer sLineasSinError = new StringBuffer();
		StringBuffer sArchivoError   = new StringBuffer();
		StringBuffer sLineasDuplicadas = new StringBuffer();

		String query="";
		try {
			con.conexionDB();
			/*-- Obtencion del detalle de carga por registro --*/
			query = "  SELECT ic_linea, cg_pyme_epo_interno, ig_numero_docto, "   +
					"         cg_estatus, cg_error_reg,"   +
					" CG_PYME_EPO_INTERNO||'@'||IG_NUMERO_DOCTO||'@'||DF_FECHA_DOCTO||'@'||DF_FECHA_VENC||'@'||IC_MONEDA||'@'||" +
					"FN_MONTO||'@'||CS_DSCTO_ESPECIAL||'@'||CT_REFERENCIA ||'@'||CG_CAMPO1||'@'||CG_CAMPO2||'@'||CG_CAMPO3||'@'" +
					"||CG_CAMPO5||'@'||CG_CAMPO5||'@'||IC_IF_BENEF||'@'||FN_PORC_BENEFICIARIO||'@'||FN_MONTO_BENEFICIARIO" +
					",  cs_caracter_especial " +
					"  FROM comtmp_carga_docto"   +
					"  WHERE ic_carga_docto =" +iNumProceso +
					" ORDER BY ic_linea ";

			log.debug("---query ---"+query );
			ResultSet rs = con.queryDB(query);
			while(rs.next()) {
				sLinea    = rs.getString(1);
				sPyme     = rs.getString(2);
				sNumDocto = rs.getString(3);
				sEstatus  = rs.getString(4);
				sError    = rs.getString(5);
				csCaracterEspecial = rs.getString(7);
				slineasErrores = ("S".equals(csCaracterEspecial))?(rs.getString(6)+(sError.replaceAll(","," "))):(rs.getString(6));
				

				//log.debug(sLinea);

				if(sEstatus.equals("OK")) {
					sLineasSinError.append(
							(("EN".equals(idiomaUsuario))?"Line":"Linea") + " : "+sLinea+ " " +
							(("EN".equals(idiomaUsuario))?"Document No.":"No. Documento") + ": "+sNumDocto+" - " +
							(("EN".equals(idiomaUsuario))?"SME":"PYME") + ": "+sPyme+".\n");

				}else  if(sEstatus.equals("DU")) {
				
					//sLineasDuplicadas.append((("EN".equals(idiomaUsuario))?"Error in line":"Error en la Linea") + ": " + sLinea + " " + sError + ".\n" );
					sLineasDuplicadas.append((("EN".equals(idiomaUsuario))?"Check Line":"Verifique Linea") + ": " + sLinea + " " + sError + ".\n" );
					//sLineasDuplicadas.append(slineasErrores + "\n");

				}else  if(sEstatus.equals("ER")) {
					sLineasError.append(
							(("EN".equals(idiomaUsuario))?"Error in line":"Error en la Linea") + ": " + sLinea + " " + sError + ".\n" );
					sArchivoError.append(slineasErrores + "\n");
				}

			} //fin while
			rs.close();
			vd = new Vector();
			vd.add((sLineasSinError.toString()));
			vd.add((sLineasError.toString()));
			vd.add((sArchivoError.toString()));
			vd.add((sLineasDuplicadas.toString()));

			con.cierraStatement();
		} catch(Exception e) {
			log.error("Exception en ObtenerDetalleCargaM. " + e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return vd;
	}//Fin de ObtenerDetalleCargaM


	////////////////////////////////////////////////////////////////////////////////////////
	//                                                                                    //
	//      Actualizacion de la informacion a traves del SP SP_INSERTA_DOCTO.             //
	//                                                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////

	public void EjecutaSPInsertaDocto (
					int iIcProcDocto, 	String sFolio, 		int iTotDoc,
					double dMontoDoc, 	int iTotDocDol, 	double dMontoDocDol,
					String sNoUsuario, 	String sAcuse) throws NafinException {
		try{
			EjecutaSPInsertaDocto(
					iIcProcDocto, sFolio, iTotDoc, dMontoDoc,
					iTotDocDol, dMontoDocDol, sNoUsuario, sAcuse, "");
			log.info("Actualizacion de la informacion a traves del SP SP_INSERTA_DOCTO");

		} catch(Exception e) {
			throw new NafinException("SIST0001");
		}
	}//Fin de EjecutaSPInsertaDocto

	public void EjecutaSPInsertaDocto (
					int iIcProcDocto, 	String sFolio, 		int iTotDoc,
					double dMontoDoc, 	int iTotDocDol, 		double dMontoDocDol,
					String sNoUsuario, 	String sAcuse, 		String sHash )
	throws NafinException {
			try{
				EjecutaSPInsertaDocto(
					iIcProcDocto,	sFolio,		iTotDoc,
					dMontoDoc,		iTotDocDol,	dMontoDocDol,
					sNoUsuario,		sAcuse,		sHash,
					"",				"", "");
				log.info("Actualizacion de la informacion a traves del SP SP_INSERTA_DOCTO");
			} catch(Exception e) {
				throw new NafinException("SIST0001");
			}
	}

	public void EjecutaSPInsertaDocto (
					int iIcProcDocto, 	String sFolio, 		int iTotDoc,
					double dMontoDoc, 	int iTotDocDol, 	double dMontoDocDol,
					String sNoUsuario, 	String sAcuse, 		String sHash,
					String nombreUsuario, String correoUsuario, String doctoDuplicados)
	throws NafinException {
		AccesoDB con = new AccesoDB();
		CallableStatement cs=null;
		try{

		log.info("doctoDuplicados --------------->"+doctoDuplicados);

			con.conexionDB();
			cs=con.ejecutaSP("SP_INSERTA_DOCTO(?,?,?,?,?,?,?,?,?,?,?, ?)");
			cs.setInt(		1, iIcProcDocto);
			cs.setString(	2, sFolio);
			cs.setInt(		3, iTotDoc);
			cs.setDouble(	4, dMontoDoc);
			cs.setInt(		5, iTotDocDol);
			cs.setDouble(	6, dMontoDocDol);
			cs.setString(	7, sNoUsuario);
			cs.setString(	8, sAcuse);
			cs.setString(	9, sHash);
			cs.setString(	10, nombreUsuario);
			cs.setString(	11, correoUsuario);
			cs.setString(	12, doctoDuplicados); // N -solo se dan de alta los documentos no Duplicados F038-2014 , (N,S) todos los documentos
			try  {
				cs.execute();
			} catch (SQLException sqle){
				throw new NafinException("ANTI0020");
			}
			if (cs!=null) cs.close();

			log.info("Actualizacion de la informacion a traves del SP SP_INSERTA_DOCTO");

	   	} catch ( NafinException Error){
			throw Error;
		} catch(Exception e) {
			log.error("Exception en EjecutaSPInsertaDocto. "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}//Fin de EjecutaSPInsertaDocto

	////////////////////////////////////////////////////////////////////////////////////////
	//                                                                                    //
	//      Agregado por MPCS para Carga de Detalles de Documento   26/05/03.             //
	//                                                                                    //
    ///////////////////////////////////////////////////////////////////////////////////////


/* Informa el numero de campos adicionales que ha definido la EPO para detalles. */
public int getNumCamposDetalle(String sNoCliente) throws NafinException {
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	int NoCamposDetalle=0;
		try{
			con.conexionDB();
			String query = "select max(ic_no_campo) from comrel_visor_detalle "+
							" where ic_epo="+sNoCliente+
							" and ic_no_campo between 1 and 10 "+
							" and ic_producto_nafin=1";
			try{
				ResultSet rs = con.queryDB(query);
				if (rs.next()){
					NoCamposDetalle = rs.getInt(1);
				}
				rs.close();
			}catch(SQLException sqle){
				log.error("Error SQL en getNumCamposDetalle:"+sqle.getMessage());
				throw new NafinException("DSCT0035");
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.error("Exception en getNumCamposDetalle: "+e.toString());
			throw new NafinException("SIST0001");
		}
//****************************************************************************************
		finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
//****************************************************************************************
	return NoCamposDetalle;
}//Fin de getNumCamposDetalle()


/* Extrae el numero de carga de detalles de documento */
public int getNumProcesoDetalle() throws NafinException{
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	int NoSecuencia=0;
	try{
		con.conexionDB();
		String query="SELECT SEQ_COMTMP_CARGA_DOCTODET.NEXTVAL FROM DUAL ";
		ResultSet rs=con.queryDB(query);
		if (rs.next())
			NoSecuencia = rs.getInt(1);
		rs.close();
		//con.cierraStatement();
		if (NoSecuencia==0 )
			throw new NafinException("DSCT0075");
	}catch (SQLException sqle){
		log.error("Error SQL en getNumProcesoDetalle(): "+sqle.getMessage());
	  throw new NafinException("DSCT0075");
	}catch (NafinException ne){
		throw ne;
	}catch (Exception e){
		log.error("Error ocurrido en getNumProcesoDetalle():"+e.toString());
		throw new NafinException("SIST0001");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	return NoSecuencia;
} //Fin de getNumProcesoDetalle()


/* Inserta en la tabla temporal COMTMP_CARGA_DOCTODET utilizando el metodo abreConexionDB() */
public void insertDetallesTMP(int no_proceso, int numLinea, String strNoDocto, String strFechaDocto, String strMoneda,
					 String strConsecutivo, String strCampo1, String strCampo2, String strCampo3, String strCampo4,
					 String strCampo5, String strCampo6, String strCampo7, String strCampo8, String strCampo9,
					 String strCampo10, String EstatusReg, String ErrorReg, String strCgPymeEpoInterno) throws NafinException {
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	try{
		con.conexionDB();
		String query = " INSERT INTO COMTMP_CARGA_DOCTODET (IC_CARGA_DOCTODET,IC_LINEA,IG_NUMERO_DOCTO,DF_FECHA_DOCTO, "+
 						"				IC_MONEDA,IC_CONSECUTIVO,CG_CAMPO1,CG_CAMPO2,CG_CAMPO3,CG_CAMPO4,CG_CAMPO5, "+
 						"				CG_CAMPO6,CG_CAMPO7,CG_CAMPO8,CG_CAMPO9,CG_CAMPO10,CG_ESTATUS,CG_ERROR_REG, "+
						"				CG_PYME_EPO_INTERNO )" +
						" VALUES ("+no_proceso+","+numLinea+",'"+strNoDocto+"','"+strFechaDocto+"','"+strMoneda+"','"+
								strConsecutivo+"','"+strCampo1+"','"+strCampo2+"','"+strCampo3+"','"+strCampo4+"','"+
								strCampo5+"','"+strCampo6+"','"+strCampo7+"','"+strCampo8+"','"+strCampo9+"','"+
								strCampo10+"','"+EstatusReg+"','"+ErrorReg+"', '"+strCgPymeEpoInterno+"') ";
		//log.debug(query);
		try{
			//conn.ejecutaSQL(query);
			//conn.terminaTransaccion(true);
			con.ejecutaSQL(query);
			con.terminaTransaccion(true);

		}catch(SQLException sqle) {
			log.error("Error SQL en insertDetallesTMP():"+sqle.getMessage());
			//conn.terminaTransaccion(false);
			con.terminaTransaccion(false);
			throw new NafinException("DSCT0064");
		}
	}
	catch(NafinException ne) {
		throw ne;
	}
	catch(Exception e) {
		log.error("Error ocurrido en insertDetallesTMP():"+e.toString());
		throw new NafinException("SIST0001");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
}//Fin de insertDetallesTMP()

/*Obtencion de la informacion del estado de la carga de detalles*/
public Vector mostrarEstadoProcesoCMD (int NumProceso) throws NafinException {
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
Vector vd = null;
String query="";
	try{
		con.conexionDB();
		query = " SELECT CG_ESTATUS_PROC,IG_NUM_PROCESADOS, IG_NUM_CORRECTOS,IG_NUM_ERRORES " +
				" FROM BIT_CARGA_DOCTODET"   +
				" WHERE IC_CARGA_DOCTODET = " + NumProceso  ;
		//log.debug(query);
		ResultSet rs = con.queryDB(query);
		vd = new Vector();
		if (rs.next()) {
			vd.addElement((rs.getString("CG_ESTATUS_PROC")==null)?"PR":rs.getString("CG_ESTATUS_PROC"));
			vd.addElement((rs.getString("IG_NUM_PROCESADOS")==null)?"0":rs.getString("IG_NUM_PROCESADOS"));
			vd.addElement((rs.getString("IG_NUM_CORRECTOS")==null)?"0":rs.getString("IG_NUM_CORRECTOS"));
			vd.addElement((rs.getString("IG_NUM_ERRORES")==null)?"0":rs.getString("IG_NUM_ERRORES"));
		} else 	{
			vd.addElement("PR");
			vd.addElement("0");
			vd.addElement("0");
			vd.addElement("0");
		}
		rs.close();
		//conn.cierraStatement();
	}
	catch(SQLException sqle) {
		log.error("Error SQL en mostrarEstadoProcesoCMD(): "+sqle.getMessage());
		throw new NafinException("DSCT0077");
	}catch(Exception e) {
		log.error("Exception en mostrarEstadoProcesoCMD(): "+e.toString());
		throw new NafinException("SIST0001");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	return vd;
}//Fin de mostrarEstadoProcesoCMD


/* Regresa el resumen de los detalles cargados */
public Vector getResumenOkCMD(int NumProceso)	throws NafinException{
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	String strQuery="";
	Vector VResultados = new Vector();
	ResultSet rs = null;
	try{
		con.conexionDB();
		strQuery = "SELECT CDD.IC_DOCUMENTO CVE_DOCTO ,TMP.IG_NUMERO_DOCTO NUM_DOCTO, COUNT(*) NUM_DETALLES"+
					" FROM COM_DOCUMENTO_DETALLE CDD, "+
					"	 COMTMP_DOCTO_DETALLE TMP "+
					" WHERE TMP.IC_DOCUMENTO=CDD.IC_DOCUMENTO "+
					"  AND TMP.IC_CONSECUTIVO=CDD.IC_DOCTO_DETALLE "+
					"  AND TMP.IC_CARGA_DOCTODET= "+ NumProceso +
					" GROUP BY CDD.IC_DOCUMENTO,TMP.IG_NUMERO_DOCTO "+
					" ORDER BY TMP.IG_NUMERO_DOCTO  ";
		rs = con.queryDB(strQuery);
		while (rs.next())
		{	Vector VColumnas = new Vector ();
			VColumnas.addElement((rs.getString("CVE_DOCTO")==null)?"":rs.getString("CVE_DOCTO"));
			VColumnas.addElement((rs.getString("NUM_DOCTO")==null)?"":rs.getString("NUM_DOCTO"));
			VColumnas.addElement((rs.getString("NUM_DETALLES")==null)?"":rs.getString("NUM_DETALLES"));
			VResultados.addElement(VColumnas);
		}
		rs.close();
		//conn.cierraStatement();
	} catch (SQLException sqle){
		log.error("Error SQL en getResumenOkCMD():"+sqle.getMessage());
		throw new NafinException("DSCT0078");
	} catch (Exception e){
		log.error("Error en getResumenOkCMD();"+e.toString());
		throw new NafinException("DSCT0078");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	return VResultados;
} //  Fin de getResumenOkCMD()


/**
 * Regresa la cadena de error de la carga masiva de detalles
 * @param NumProceso Numero de proceso
 * @param idiomaUsuario Idioma del usuario
 * 		EN Ingl�s ES Espa�ol
 * @return Cadena con los errores generados
 */

public String getResultadoErrorCMD ( int NumProceso, String idiomaUsuario) throws NafinException{
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	String strQuery="";
	StringBuffer strBufResult=new StringBuffer("");
	ResultSet rs=null;
	try{
		con.conexionDB();
		strQuery=" SELECT " +
			"'" + (("EN".equals(idiomaUsuario))?"Line":"Linea") + " '" +
			"||IC_LINEA||' .- '||CG_ERROR_REG AS LINEA "+
			"   FROM COMTMP_CARGA_DOCTODET "+
			"  WHERE CG_ESTATUS='ER' "+
			"    AND IC_CARGA_DOCTODET= "+NumProceso+
			" ORDER BY IC_LINEA ";
		rs = con.queryDB(strQuery);
		while (rs.next()) {
			strBufResult.append((rs.getString("LINEA")==null)?"":rs.getString("LINEA")+"\n");
		}
		rs.close();
		//conn.cierraStatement();
	} catch (SQLException sqle){
		log.error("Error SQL en getResultadoErrorCMD():"+sqle.getMessage());
		throw new NafinException("DSCT0079");
	} catch (Exception e){
		log.error("Error en getResultadoErrorCMD();"+e.toString());
		throw new NafinException("DSCT0079");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	return strBufResult.toString();
} // Fin de getResultadoErrorCMD()



/**
 * Metodo para procesar la informaci�n de los enlaces.
 * Se agreg� el throws Exception con la finalidad de que el cliente
 * pueda determinar facilmente si existio un erro o no
 * @return Cadena (StringBuffer) con la infroamci�n del proceso
 * @throws Exception cuando existe un error inesperado.
 */
public StringBuffer proceso(String EPO, EpoEnlace epoEnl) throws SQLException{
	log.info("CargaDocumentoBean::proceso(E)************");
	log.info("************************************** " + epoEnl.getTablaDocumentos());
	boolean esEnlaceWS = (epoEnl instanceof WSEnlace);
	int noMoneda=0, totalcd=0, i=1;
	java.sql.Date fechaDocto, fechaVenc, fechaVencProv=null,fechaEntrega=null;
	java.util.Date FechaVenc;
	java.util.Date FechaVencProv = null;
	Calendar cal= Calendar.getInstance();
	Calendar caFV= Calendar.getInstance();
	BigDecimal monto;	Vector diames_inhabil=new Vector();
	String query="",noDocto="",noInterPymEpo="",descuento="",referencia="",cd1="",cd2="",cd3="",cd4="",cd5="",queri="",dm_inhabil="",ymd="",ymdProv="",diames="";
	String diamesanio="";
	String claveMandante = ""; // "No. de Nafin electr�nico del Mandante asignado."
	String icMandante 	= "";
	String sTipoCompra="",sClavePresup="",sPeriodo="";
	int iPeriodo = 0;
	PreparedStatement pstmt = null;
	String nombreCampo[]=new String[6]; String tipoDato[]=new String[6];
	int noCampo[]=new int[6]; int longitud[]=new int[6];

	String obligatorio[]=new String[6]; String tipoCobranza[]=new String[6];

	StringBuffer error=new StringBuffer(); StringBuffer erroreg=new StringBuffer();
  	StringBuffer erroraux = new StringBuffer();
	StringBuffer noError=new StringBuffer(); StringTokenizer st,st1, st2;
	ResultSet rs=null, rs1=null;
	Acuse acuse=new Acuse(Acuse.ACUSE_EPO);
	AcuseEnl	acuEnl	= new AcuseEnl();
	AccesoDB con = new AccesoDB();
	StringBuffer	sblog = new StringBuffer("");
	String noUsuario = "enlac_aut";
	String noRecibo = "";
	String cargaEnlace = "";
	String csCaracterEspecial 	= "";
	
	if(epoEnl instanceof WSEnlace) {
		noUsuario 	= ((WSEnlace)epoEnl).getIc_usuario();
		noRecibo 	= ((WSEnlace)epoEnl).getCg_receipt();
		cargaEnlace = "S";
	}
	String condicionDoctos = "";
	String tabla_detalles = "";
	boolean bProcesarDoctos = true;
	boolean bOperaFactorajeConMandato = false;
	String factorajeVencido = "";
	String fechaVencProveedor = "";
	String doctosVencidos = "";
	String sDigitoIdentificadorFlag = "";
	String 		sPubEPOPEFFlagVal = "",
				sPubEPOPEFDocto1Val = "",
				sPubEPOPEFDocto2Val = "",
				sPubEPOPEFDocto3Val = "",
				sPubEPOPEFDocto4Val = "";
	String valoresTipoCompra = "";
	//==============================================================================//FODEA 026 - 2009 ACF (I)

	String cuentaClabe = "";
	String icBancosTef = "";
	String icViaLiquidacion = "";
	boolean flagCadenaNafin = true;

	//==============================================================================//FODEA 026 - 2009 ACF (F)
	String ic_estatus_docto = "";
	String nafinElectronicoIF = "";
	String icIf				= null;
	java.util.Date FechaPlazoPagoFVP = null;
	int iPlazoMax1FVP = 0, iPlazoMax2FVP = 0, hayRegDet = 0, lineadoctosd=0;
	List lAlmacenErrores = new ArrayList(); //en caso de ser epo todo o nada, en estas listas se guardaran los errores
	
	Hashtable alParamEPO = null;
	ParametrosDescuento BeanParamDscto = null;

	Vector diames_inhabil_x_anio = new Vector();
	String factorajeIF ="";
	//DocumentoEnl doctfactorajeIFo = new DocumentoEnl();
	
	
	String sFactorajeDistribuido =""; //2017_003	
	String porcBeneficiario= ""; //2017_003
	String numCopade =  "";  //2017_003
	String numContrato = ""; //2017_003
	String icBeneficiario =""; //2017_003
	
	
	try {
		con.conexionDB();

		int NO_EPO=Integer.parseInt(EPO);
		bOperaFactorajeConMandato = operaFactorajeConMandato(EPO);
		///////********************* GENERAL INNECESARIO *************************////////
		// Sirve para validar que existan documentos.
		// En el caso de EPOS centralizadas, solo se ejecuta si hay datos la tabla, sino no ejecuta el proceso
		int hayreg=0;
		query =
			" SELECT COUNT (1), 'CargaDocumentoBean::proceso()' origenquery "   +
			"   FROM "+epoEnl.getTablaDocumentos()+" "   +
			epoEnl.getCondicionQuery();
		log.debug("query: " + query);
		rs = con.queryDB(query);
		if (rs.next()) {
			hayreg = rs.getInt(1);
		}
		rs.close(); con.cierraStatement();

		if(epoEnl instanceof EpoEnlaceDetalle) {

			tabla_detalles = ((EpoEnlaceDetalle)epoEnl).getTablaDetalles();

			query = "SELECT COUNT(1) FROM "+tabla_detalles+" " + epoEnl.getCondicionQuery();
	        rs = con.queryDB(query);
	        if(rs.next())
	          hayRegDet = rs.getInt(1);
	        rs.close();
	        con.cierraStatement();

	        if(hayreg>0 && hayRegDet>0)
	        	bProcesarDoctos = true;
	        else
	        	bProcesarDoctos = false;

		}

		if(epoEnl instanceof EpoCentralizada) {
			bProcesarDoctos = (hayreg == 0)?false:true;
		}
//-------------------->> aqui empiezan las validaciones para hacer el insert en COM_DOCUMENTO
		if (bProcesarDoctos) {
			///////********************* GENERAL *************************////////
			// Obtenemos los dias Inhabiles para validar la Fecha de Vencimiento.
			query =
				"SELECT cg_dia_inhabil " +
				"  FROM comcat_dia_inhabil " +
				" WHERE cg_dia_inhabil IS NOT NULL " +
				"UNION " +
				"SELECT cg_dia_inhabil " +
				"  FROM comrel_dia_inhabil_x_epo " +
				" WHERE cg_dia_inhabil IS NOT NULL AND ic_epo = " + NO_EPO;


			rs = con.queryDB(query);
			while(rs.next()) {
				diames_inhabil.addElement(rs.getString(1).trim());
			}//while(rs.next())
			rs.close(); con.cierraStatement();

			//FODEA 015-2012
			query =
				" SELECT to_char(df_dia_inhabil,'DD/MM/YYYY'), 'CargaDocumentoBean::proceso()' origenquery "   +
				" FROM comcat_dia_inhabil where df_dia_inhabil IS NOT NULL " +
				" UNION " +
				" SELECT to_char(df_dia_inhabil,'DD/MM/YYYY'), 'CargaDocumentoBean::proceso()' origenquery  " +
				" FROM comrel_dia_inhabil_x_epo " +
				" WHERE df_dia_inhabil IS NOT NULL AND ic_epo = " + NO_EPO;
			rs = con.queryDB(query);
			while(rs.next()) {
				diames_inhabil_x_anio.addElement(rs.getString(1).trim());
			}//while(rs.next())
			rs.close(); con.cierraStatement();

			///////********************* GENERAL *************************////////
			// Obtiene los D�as Maximos y M�nimos en los que puede Cargar documentos.
			int diaMin=0, mesMin=0, anoMin=0, diaMax=0, mesMax=0, anoMax=0;
			query =
				" SELECT TO_CHAR (SYSDATE + NVL (pe.ig_dias_minimo, p.in_dias_minimo), 'dd') fechavemind,"   +
				"        TO_CHAR (SYSDATE + NVL (pe.ig_dias_minimo, p.in_dias_minimo), 'mm') fechaveminm,"   +
				"        TO_CHAR (SYSDATE + NVL (pe.ig_dias_minimo, p.in_dias_minimo), 'yyyy') fechavemina,"   +
				"        TO_CHAR (SYSDATE + NVL (pe.ig_dias_maximo, p.in_dias_maximo), 'dd') fechavmaxd,"   +
				"        TO_CHAR (SYSDATE + NVL (pe.ig_dias_maximo, p.in_dias_maximo), 'mm') fechavmaxm,"   +
				"        TO_CHAR (SYSDATE + NVL (pe.ig_dias_maximo, p.in_dias_maximo), 'yyyy') fechavmaxa,"   +
				"        pe.cs_factoraje_vencido, NVL (pe.cs_fecha_venc_pyme, 'N') AS cs_fecha_venc_pyme,"   +
				"        NVL (pe.cs_pub_docto_venc, 'N') AS cs_pub_docto_venc "   +
				"        , TRUNC (SYSDATE) + NVL(ig_plazo_pago_fvp, 0) as df_plazo_pago_fvp"   +
				"        , NVL(ig_plazo_max1_fvp, 0) AS ig_plazo_max1_fvp"   +
				"        , NVL(ig_plazo_max2_fvp, 0) AS ig_plazo_max2_fvp"   +
				" 			,pe.CS_FACTORAJE_IF  as CS_FACTORAJE_IF  "+
				" 	     , 'CargaDocumentoBean::proceso()' origenquery"   +
				"  FROM comrel_producto_epo pe, comcat_producto_nafin p"   +
				"  WHERE pe.ic_producto_nafin(+) = p.ic_producto_nafin"   +
				"	 AND pe.ic_producto_nafin  = 1" +
				"   AND pe.ic_epo(+) = "+NO_EPO;
			rs=con.queryDB(query);
			if(rs.next()) {
				diaMin				= rs.getInt(1);
				mesMin				= rs.getInt(2);
				anoMin				= rs.getInt(3);
				diaMax				= rs.getInt(4);
				mesMax				= rs.getInt(5);
				anoMax				= rs.getInt(6);
				factorajeVencido 	= rs.getString("cs_factoraje_vencido");
				fechaVencProveedor	= rs.getString("cs_fecha_venc_pyme");
				doctosVencidos 		= rs.getString("cs_pub_docto_venc");
				FechaPlazoPagoFVP	= rs.getDate("df_plazo_pago_fvp");
				iPlazoMax1FVP 		= rs.getInt("ig_plazo_max1_fvp");
				iPlazoMax2FVP		= rs.getInt("ig_plazo_max2_fvp");
				factorajeIF = rs.getString("CS_FACTORAJE_IF");
			}//if(rs.next())
			rs.close(); con.cierraStatement();

			// NOTA: Debido a que en el query proporcinado al invocar el metodo getDocuments de la clase
			//       WSEnlace no se tiene referencia al parametro: cs_fecha_venc_pyme. Se deshabilito la
			// 		opcion fechaVencProveedor y con lo que se evita que la carga de documentos truene,
			//       cuando esta se realice mediante el Web Service.
			//			Linea Conflictiva: 6820
			//					fechaVencProv 	= rs.getDate("DF_FECHA_VENC_PYME");
			//			ESTA CONDICION DEBE SER CORREGIDA, PARA QUE EN LA CLASE WSENLACE SE CONSIDERE LA COLUMNA
			//			cs_fecha_venc_pyme
			if(epoEnl instanceof WSEnlace){
				fechaVencProveedor = "N";
			}

			try {
				BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
				alParamEPO = BeanParamDscto.getParametrosEPO(EPO, 1);
				sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
				sPubEPOPEFDocto1Val					= alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA").toString();
				sPubEPOPEFDocto2Val					= alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA").toString();
				sPubEPOPEFDocto3Val					= alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL").toString();
				sPubEPOPEFDocto4Val					= alParamEPO.get("PUB_EPO_PEF_PERIODO").toString();
				sDigitoIdentificadorFlag    = alParamEPO.get("PUBLICACION_EPO_PEF").toString();
				sFactorajeDistribuido    = alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString(); //2017_003
			    
			} catch (NafinException ne){
				String msg = "Error en la obtencion de los parametros por epo "+ne.getMsgError()+".\n";
				sblog.append(msg);
				if (esEnlaceWS) {
					throw new AppException(msg, ne);
				}
			}

			Calendar fechaMin = Calendar.getInstance();	Calendar fechaMax = Calendar.getInstance();
			fechaMin.set(anoMin,mesMin-1,diaMin,0,0,0);	fechaMax.set(anoMax,mesMax-1,diaMax,0,0,0);
			java.util.Date FechaMin=fechaMin.getTime();	java.util.Date FechaMax=fechaMax.getTime();
			java.util.Date FechaHoy=new java.util.Date();	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			Calendar dia_fv = new GregorianCalendar();
			sblog.append("Fecha de Venc Minima: "+sdf.format(FechaMin)+", Fecha de Venc Maxima: "+sdf.format(FechaMax)+", Fecha de Hoy: "+sdf.format(FechaHoy)+"\n");

			///////********************* GENERAL *************************////////
			/*query = "select ic_no_campo, cg_nombre_campo, cg_tipo_dato, ig_longitud, CS_OBLIGATORIO "+
					" from comrel_visor"+
					" where ic_epo= " +NO_EPO+
					" and ic_no_campo between 1 and 5 "+
					" and ic_producto_nafin = 1 "+
					" order by ic_no_campo";*/
			query =
				" SELECT   v.ic_no_campo, TRIM (INITCAP (v.cg_nombre_campo)) nombre_campo,"   +
				"          TRIM (UPPER (v.cg_tipo_dato)) tipo_dato, v.ig_longitud,"   +
				"          v.cs_obligatorio,"   +
				"          DECODE ("   +
				"             NVL (pe.cg_tipo_cobranza, 'N'),"   +
				"             'N', DECODE (NVL (pe2.cs_cobranza_referencial, 'N'), 'S', 'R', 'N'),"   +
				"             pe.cg_tipo_cobranza ) AS cg_tipo_cobranza,"   +
				"             'CargaDocumentoBean::proceso()' origenquery "   +
				"     FROM comrel_visor v,"   +
				"          (SELECT ic_epo, cg_tipo_cobranza"   +
				"             FROM comrel_producto_epo"   +
				"            WHERE ic_epo = "+NO_EPO+" "   +
				"              AND ic_producto_nafin = 6) pe,"   +
				"          (SELECT ic_epo, cs_cobranza_referencial"   +
				"             FROM comrel_producto_epo"   +
				"            WHERE ic_epo = "+NO_EPO+" "   +
				"              AND ic_producto_nafin = 2) pe2"   +
				"    WHERE v.ic_epo = "+NO_EPO+" "   +
				"      AND v.ic_epo = pe.ic_epo(+)"   +
				"      AND v.ic_epo = pe2.ic_epo(+)"   +
				"      AND v.ic_producto_nafin = 1"   +
				"      AND v.ic_no_campo BETWEEN 1 AND 5"   +
				" ORDER BY v.ic_no_campo"  ;
			rs=con.queryDB(query);
			while(rs.next()) {
				noCampo[i]		= rs.getInt(1);
				nombreCampo[i]	= rs.getString(2);
				tipoDato[i]		= rs.getString(3);
				longitud[i]		= rs.getInt(4);
				obligatorio[i]	= rs.getString(5);
				tipoCobranza[i]	= rs.getString(6);
				sblog.append(
						noCampo[i]+" "+
						nombreCampo[i]+" "+
						tipoDato[i]+" "+
						longitud[i]+" "+
						obligatorio[i]+" "+
						tipoCobranza[i]+"\n");
				i++;
				totalcd++;
			}//while(rs.next())
			rs.close(); con.cierraStatement();

			///////********************* GENERAL *************************////////
			// Inserta los datos del Acuse.
			acuEnl.setCcAcuse(acuse.toString());
			int totdocmn=0, totdocdol=0, linedoctos=0, itotacepmn=0, itotrechmn=0, itotacepdol=0, itotrechdol=0;
			BigDecimal mtodocmn=new BigDecimal(0.0); BigDecimal mtodocdol=new BigDecimal(0.0);
			query =
				" INSERT INTO com_acuse1"   +
				"             (cc_acuse, in_total_docto_mn, fn_total_monto_mn,"   +
				"              df_fechahora_carga, in_total_docto_dl, fn_total_monto_dl,"   +
				"              ic_usuario, cg_recibo_electronico, cs_carga_ws)"   +
				"      VALUES ('"+acuEnl.getCcAcuse()+"', "+acuEnl.getInTotalDoctoMn()+", "+acuEnl.getFnTotalMontoMn()+","   +
				"              SYSDATE, "+acuEnl.getInTotalDoctoDl()+", "+acuEnl.getFnTotalMontoDl()+","   +
				"              '"+noUsuario+"', '"+noRecibo+"', '"+cargaEnlace+"')"  ;
			log.debug(query);
			try {
				con.ejecutaSQL(query);
			} catch(Exception e) {
				//Si hay error al insertar el acuse, no debe continuar,
				//de lo contrario deja informaci�n inconsistente
				String msg = "Error al insertar a com_acuse1.\n";
				sblog.append(msg);
				throw new AppException(msg, e);
			}

			///////********************* GENERAL *************************////////
			// Antes de Insertar el acuse de la EPO se borran los anteriores.

			if(epoEnl instanceof EpoCentralizada) {
				try {
					query =
						" update "+epoEnl.getTablaDocumentos()+
						" set cc_acuse = '"+acuEnl.getCcAcuse()+"'" +
						" WHERE cc_acuse is null ";
					con.ejecutaSQL(query);

					query =
						" delete "+epoEnl.getTablaErrores()+
						" where cc_acuse in "+
						" 	(select cc_acuse from "+epoEnl.getTablaAcuse()+
						" 	where df_fechahora_carga < trunc(sysdate))";
					con.ejecutaSQL(query);

					query =
						" delete "+epoEnl.getTablaAcuse()+"_DET"+
						" where cc_acuse in "+
						" 	(select cc_acuse from "+epoEnl.getTablaAcuse()+
						" 	where df_fechahora_carga < trunc(sysdate))";
					con.ejecutaSQL(query);

					query =
						" delete "+epoEnl.getTablaAcuse() +
						" where df_fechahora_carga < trunc(sysdate)";
					con.ejecutaSQL(query);

				} catch(Exception e){
					e.printStackTrace();
					sblog.append("Error al actualizar tablas de dias anteriores \n"+query+"\n");
				}
				condicionDoctos = " WHERE CC_ACUSE = '"+acuEnl.getCcAcuse()+"'";

			} else {
				try {
					query =
						" DELETE "+epoEnl.getTablaAcuse() +
						epoEnl.getCondicionQuery();
					log.debug(query);
					con.ejecutaSQL(query);

					if(epoEnl instanceof WSEnlace) {
						query =
							" DELETE "+epoEnl.getTablaAcuse() +
							"  WHERE df_fechahora_carga < TRUNC(SYSDATE)";
						log.debug(query);
						con.ejecutaSQL(query);
					}
				}catch(Exception e){
					sblog.append("Error al borrar a "+epoEnl.getTablaAcuse()+".\n");
				}
			}

			String csestatus = "null";
			///////********************* GENERAL *************************////////
			// Inserta los datos del Acuse para la EPO
			query = epoEnl.getInsertAcuse(acuEnl);
			try{
				con.ejecutaSQL(query);
			}catch(Exception e){
				String msg ="Error al insertar a "+epoEnl.getTablaAcuse()+".\n";
				sblog.append(msg);
				if (esEnlaceWS) {
					throw new AppException(msg, e);
				}
			}

			if(epoEnl instanceof EpoEnlaceAll || epoEnl instanceof EpoEnlaceDetalle){
				con.terminaTransaccion(true);
			}

			///////********************* GENERAL INNECESARIO *************************////////
			// Sirve para validar que existan documentos.
			/*int hayreg=0;
			query = " select count(*) from "+epoEnl.getTablaDocumentos()  + epoEnl.getCondicionQuery();
			rs = con.queryDB(query);
	        if (rs.next())
				hayreg = rs.getInt(1);
			rs.close(); con.cierraStatement();*/

			///////********************* GENERAL *************************////////
			// Obtenemos el Aforo para calcular el Monto del Descuento
			BigDecimal fnAforoP = new BigDecimal("0");
			BigDecimal fnAforoPDL = new BigDecimal("0");
			query =
				" SELECT MIN (x.fn_aforo) / 100, MIN (x.fn_aforo_dl) / 100, 'CargaDocumentoBean::proceso()' origenquery "   +
				"   FROM (SELECT fn_aforo, fn_aforo_dl"   +
				"           FROM comrel_producto_epo"   +
				"          WHERE ic_producto_nafin = ? AND ic_epo = ?"   +
				"         UNION"   +
				"         SELECT fn_aforo, fn_aforo_dl"   +
				"           FROM comcat_producto_nafin"   +
				"          WHERE ic_producto_nafin = ?) x"  ;
			log.debug(query);
			pstmt = con.queryPrecompilado(query);
			pstmt.setInt(1, 1);
			pstmt.setInt(2, NO_EPO);
			pstmt.setInt(3, 1);
			rs = pstmt.executeQuery();
			if(rs.next()) {
	        	fnAforoP = new BigDecimal(rs.getString(1));
	        	fnAforoPDL = new BigDecimal(rs.getString(2));
			}
			rs.close();
			if(pstmt!=null) pstmt.close();

			///////********************* GENERAL *************************////////
			if(!(epoEnl instanceof EpoCentralizada)){
				sblog.append("Se entro para borrar la tabla de errores ");
				query = "DELETE "+epoEnl.getTablaErrores()+epoEnl.getCondicionQuery();
				try{
					sblog.append("query para borrar::: "+query);
					con.ejecutaSQL(query);
					sblog.append("Se Borro en "+epoEnl.getTablaErrores());
				}catch(Exception e){
					sblog.append("Error al Borrar en "+epoEnl.getTablaErrores());
				}
			}

			//para epos todo o nada, este commit sirve para borrar la tabla de errores
			if(epoEnl instanceof EpoEnlaceAll){
				con.terminaTransaccion(true);
			}

			if ("S".equals(sPubEPOPEFFlagVal)){
				query =
					" SELECT cc_clave "   +
					"   FROM comcat_tipo_compra ";
				//log.debug(":: " + query);
				rs1 = con.queryDB(query);
				int rsTipoCompra = 0;
				while(rs1.next()) {
					valoresTipoCompra += (rsTipoCompra == 0)?rs1.getString("CC_CLAVE"):", "+rs1.getString("CC_CLAVE");
					rsTipoCompra ++;
				}//if(rs1.next())
				rs1.close(); con.cierraStatement();
			}

			String msgErr="";
			int ic_pyme=0, no_dia_semana=0, iTotalAlta=0, iTotalError=0;
			BigDecimal PymeEpoInterno=null;
			boolean ok=true, okReg=true;
			boolean esPymeNoRegistrada = false; // Fodea 059 - 2010
			String cadenaOriginal =  "";
				
			if(hayreg > 0){
				///////********************* GENERAL POR PROCESO; ESPECIFICO POR EPO *************************////////
				queri = epoEnl.getDocuments() + condicionDoctos;
				log.debug(queri);
				rs = con.queryDB(queri);
				while(rs.next()) {

					ic_pyme 			= 0;
					noDocto 			= rs.getString(1).trim();
					noInterPymEpo 		= rs.getString(2).trim();
					fechaDocto 			= rs.getDate(3);
					fechaVenc 			= rs.getDate(4);
					cal.setTime(fechaVenc);
					FechaVenc			= cal.getTime();
					ymd					= fechaVenc.toString();
					noMoneda 			= rs.getInt(5);
					monto 				= rs.getBigDecimal(6);
					descuento 			= rs.getString(7).trim();
					referencia 			= (rs.getString( 8)==null)?"":rs.getString( 8);
					cd1 				= (rs.getString( 9)==null)?"":rs.getString( 9);
					cd2 				= (rs.getString(10)==null)?"":rs.getString(10);
					cd3 				= (rs.getString(11)==null)?"":rs.getString(11);
					cd4 				= (rs.getString(12)==null)?"":rs.getString(12);
					cd5 				= (rs.getString(13)==null)?"":rs.getString(13);
					nafinElectronicoIF 	= (rs.getString(15)==null)?"":rs.getString(15);
					icIf				= "NULL";
					ic_estatus_docto 	=  "2";
					icBeneficiario = "";
					esPymeNoRegistrada = false;  // Fodea 059 - 2010
					if (esEnlaceWS) {
						cadenaOriginal 		= rs.getString("CS_CADENA_ORIGINAL");
					}
					if ("S".equals(sPubEPOPEFFlagVal)){
						fechaEntrega 		= rs.getDate("DF_ENTREGA");
						sTipoCompra 		= rs.getString("CG_TIPO_COMPRA");
						sClavePresup 		= rs.getString("CG_CLAVE_PRESUPUESTARIA");
						sPeriodo 		    = rs.getString("CG_PERIODO");
						iPeriodo        = rs.getInt("CG_PERIODO");
					}

					if ( "S".equals(fechaVencProveedor)) {
						fechaVencProv 	= rs.getDate("DF_FECHA_VENC_PYME");
						caFV.setTime(fechaVencProv);
						FechaVencProv 	= caFV.getTime();
						ymdProv 		= fechaVencProv.toString();
					}
//==============================================================================//FODEA 026 - 2009 ACF (I)

					if (epoEnl instanceof SIOREnlace) {
						if (noMoneda == 1) {
							cuentaClabe = rs.getString("CG_CUENTA")==null?"":rs.getString("CG_CUENTA");
							icBancosTef = rs.getString("IC_BANCOS_TEF")==null?"":rs.getString("IC_BANCOS_TEF");
							icViaLiquidacion = rs.getString("CG_VIA_LIQ")==null?"":rs.getString("CG_VIA_LIQ");
							flagCadenaNafin = true;
						}
					}

	//==============================================================================//FODEA 026 - 2009 ACF (F)
			// Fodea 046 - 2009. Si el campo "Opera Factoraje con Mandato" es verdadero,
			// se realizara la carga del campo Clave Mandante.
					claveMandante = "";
					if( (epoEnl instanceof WSEnlace) && bOperaFactorajeConMandato && descuento.equalsIgnoreCase("M")){
						claveMandante 		    = rs.getString("CG_CLAVE_MANDANTE");
						if(claveMandante == null) claveMandante = "";
					}

					if (  epoEnl instanceof PemexEnlace  &&  "S".equalsIgnoreCase(sFactorajeDistribuido) && "D".equalsIgnoreCase(descuento)    ) {  // 2017_003  
					    porcBeneficiario = rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO");
					    numCopade = rs.getString("CG_NUM_COPADE")==null?"":rs.getString("CG_NUM_COPADE");
					    numContrato = rs.getString("CG_NUM_CONTRATO")==null?"":rs.getString("CG_NUM_CONTRATO");
					}
					
					sblog.append(
							noDocto+" "+
							noInterPymEpo+" "+
							fechaDocto+" "+
							fechaVenc+" "+
							noMoneda+" "+
							monto.toPlainString()+" "+
							descuento+" "+
							referencia+" "+
							cd1+" "+
							cd2+" "+
							cd3+" "+
							cd4+" "+
							cd5);

					if ("S".equals(sPubEPOPEFFlagVal)){
						sblog.append(" "+fechaEntrega+" "+
								sTipoCompra+" "+
								sClavePresup+" "+
								sPeriodo);
					}

					if( (epoEnl instanceof WSEnlace) && bOperaFactorajeConMandato && descuento.equalsIgnoreCase("M") ){
						sblog.append(" "+claveMandante);
					}
				    
					if (  epoEnl instanceof PemexEnlace  &&  "S".equalsIgnoreCase(sFactorajeDistribuido) && "D".equalsIgnoreCase(descuento)    ) {  // 2017_003  					
					    sblog.append( "" +nafinElectronicoIF +" "+porcBeneficiario+" "+ numCopade+" "+numContrato);
					}
				    
				   
				    
					linedoctos++;
					if(!(epoEnl instanceof WSEnlace)) {
						msgErr = "Error en el Registro "+ (linedoctos) +",";
					}
					okReg = true;

					//F0015-2015
					//Se integra validacion que determina si la informacion enviada por el WS de Distribuidores contiene carcateres especiales
					if( (epoEnl instanceof WSEnlace)){
						csCaracterEspecial 		    = rs.getString("cs_caracter_especial");
						if("S".equals(csCaracterEspecial)){
							error.append(msgErr + " Se detectaron caracteres de control no permitidos (excluyendo tabulador (\\t), salto de l�nea (\\n) y retorno de carro (\\r))\n");
							noError.append("81\n");
							ok = okReg = false;
						}
					}
					
					// Revisa que la clave del proveedor sea num�rico. CASO ESPECIAL
					if(epoEnl instanceof WMEnlace){
						try {
							//iPymeEpoInterno = Integer.parseInt(noInterPymEpo);
							PymeEpoInterno=new BigDecimal(noInterPymEpo);
							noInterPymEpo=PymeEpoInterno.toPlainString();
							log.info("noInterPymEpo" + noInterPymEpo);
						}
						catch(Exception e) {
							error.append(msgErr + " la clave de la PYME "+noInterPymEpo+" debe ser num�rico.\n");
							noError.append("19\n");
							ok = okReg = false;
							esPymeNoRegistrada = true; // Fodea 059 - 2010 By JSHD
						}
					}

					if(noInterPymEpo.length() > 25){
						error.append(msgErr + " la clave de la PYME "+noInterPymEpo+" excede la longitud permitida.\n");
						noError.append("20\n");
						ok = okReg = false;
						esPymeNoRegistrada = true; // Fodea 059 - 2010 By JSHD
					}

					// CASO ESPECIAL PARA EL ENLACE DE PEMEX PEP

					if (linedoctos == 1) {
						acuEnl.setCcControl(("".equals(cd3))?"0":cd3);
					}

					/* Revisa si el numero de pyme no existe en la relacion pyme_epo. */
					query =
						" SELECT ic_pyme, 'CargaDocumentoBean::proceso()' origenquery "   +
						"   FROM comrel_pyme_epo"   +
						"  WHERE ic_epo = "+NO_EPO+
						"    AND TRIM (cg_pyme_epo_interno) = '"+noInterPymEpo+"'";
	                //log.debug(":: " + query);
					rs1 = con.queryDB(query);
					if(rs1.next()) {
						ic_pyme = rs1.getInt(1);
					}//if(rs1.next())
					rs1.close(); con.cierraStatement();

					if(ic_pyme==0) {
						error.append(msgErr + " no existe una PYME para el "+noInterPymEpo+". \n");
						noError.append("1\n");
						ok = okReg = false;
						esPymeNoRegistrada = true; // Fodea 059 - 2010 By JSHD
					}

					// Fodea 059 - 2010. Registrar en la Bitacora de Error Publicacion aquellos registros que no tienen
					// cuyo numero de proveedor no fue encontrado en la tabla comrel_pyme_epo
					if(esPymeNoRegistrada){
						SimpleDateFormat formateaFecha =new SimpleDateFormat("dd/MM/yyyy");
						registraDoctoConErrorEnPublicacion(
							String.valueOf(NO_EPO),
							noInterPymEpo,
							noDocto,
							(fechaVenc != null?formateaFecha.format(fechaVenc):null),
							String.valueOf(noMoneda),
							(monto     != null?monto.toPlainString()               :null),
							descuento,
							"ProcAut",
							"N/A"
						);
						//INICIA CODIGO PARA VALIDAR PYME BLOQUEADA PARA LA EPO
					}else{
						System.out.println("String.valueOf(ic_pyme) === "+ String.valueOf(ic_pyme));
						System.out.println(" String.valueOf(NO_EPO) === "+  String.valueOf(NO_EPO));

						if(validaPymeXEpoBloqueda(String.valueOf(NO_EPO), String.valueOf(ic_pyme),"1" )){
							System.out.println(" SI esta bloqueda ");
							error.append(msgErr + " La Pyme con N�mero de Proveedor "+noInterPymEpo+" esta Bloqueda. \n");
							noError.append("79\n");
							ok = okReg = false;
						}else{
							System.out.println(" NO esta bloqueda ");
						}
					}
					//////////////////////////////////////////////////////////////
					//Esta validaci�n se hace en el TRIGGER -> TRG_CHECA_DOCUMENTO
					//////////////////////////////////////////////////////////////

					//Nota 30/06/2009: A solicitud de Ricardo Jimenez/Israel Herrera se descomenta
					//el siguiente bloque de c�digo.

					//Revisa si existen c/u de los doctos en la tabla com_documento, para evitar duplicados
          boolean csDocDuplicado = false;

          System.out.println("epoEnl.getTablaDocumentos() ==== " + epoEnl.getTablaDocumentos());

					query =
							" SELECT /*+ INDEX_FFS(com_documento in_com_documento_01_nuk) */ COUNT(*) " +
							" FROM com_documento " +
							" WHERE ig_numero_docto = ? " +
							" AND ic_epo = ? " +
							" AND TO_CHAR(df_fecha_docto,'yyyy') = ? " +
							" AND ic_moneda = ? " +
							" AND ic_pyme = ? ";

					PreparedStatement psRepetido = con.queryPrecompilado(query);
					psRepetido.setString(1, noDocto);
					psRepetido.setInt(2, NO_EPO);
					psRepetido.setString(3, (new SimpleDateFormat("yyyy")).format(fechaDocto));
					psRepetido.setInt(4, noMoneda);
					psRepetido.setInt(5, ic_pyme);

					rs1=psRepetido.executeQuery();
					rs1.next();
					if(rs1.getInt(1)>=1) {
						error.append(msgErr +" el Documento "+noDocto+" ya existe. \n");
						noError.append("2\n");
						ok = okReg = false;
            csDocDuplicado = true;
					}
					rs1.close();
					psRepetido.close();

          //--
          /*if(!csDocDuplicado){
            query =
                " SELECT  COUNT(*) " +
                " FROM  " +epoEnl.getTablaDocumentos()+
                " WHERE IG_NUMERO_DOCTO = ? " +
                " AND CG_PYME_EPO_INTERNO = ? " +
                " AND TO_CHAR(df_fecha_docto,'yyyy') = ? " +
                " AND ic_moneda = ? ";

            PreparedStatement psRepetido = con.queryPrecompilado(query);
            psRepetido.setString(1, noDocto);
            psRepetido.setString(2, noInterPymEpo);
            psRepetido.setString(3, (new SimpleDateFormat("yyyy")).format(fechaDocto));
            psRepetido.setInt(4, noMoneda);

            rs1=psRepetido.executeQuery();
            rs1.next();
            if(rs1.getInt(1)>1) {
              error.append(msgErr +" el Documento "+noDocto+" ya existe. \n");
              noError.append("2\n");
              ok = okReg = false;
              csDocDuplicado = true;
            }
            rs1.close();
            psRepetido.close();
          }*/

          if(!csDocDuplicado && publicarDocDuplicados(String.valueOf(NO_EPO)))
          {
             query = " select  count(*) as total  "+
										"	from  com_documento   "+
										"	where ic_epo  = ?  "+
										"	and to_char(df_fecha_docto,'dd/mm/yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'dd/mm/yyyy')  "+
										"	and ic_moneda = ?  "+
										"	and ic_pyme  =  ?  "+
										"	and fn_monto =  ?  "+
										"  and ic_estatus_docto  not in (1, 5 )  ";
              psRepetido = con.queryPrecompilado(query);
              psRepetido.setInt(1, NO_EPO);
              psRepetido.setString(2, (new SimpleDateFormat("dd/MM/yyyy")).format(fechaDocto));
              psRepetido.setInt(3, noMoneda);
              psRepetido.setInt(4, ic_pyme);
              psRepetido.setBigDecimal(5,monto);


              rs1=psRepetido.executeQuery();
              rs1.next();
              if(rs1.getInt(1)>=1) {
                //error.append(msgErr +" el Documento "+noDocto+" ya existe. \n");
                //noError.append("2\n");
                //ok = okReg = false;
                ic_estatus_docto = "33";
              }
              rs1.close();
              psRepetido.close();

              if(!ic_estatus_docto.equals("33")){
                query =
                " SELECT COUNT(*) " +
                " FROM  " +epoEnl.getTablaDocumentos()+
                " WHERE IG_NUMERO_DOCTO = ? " +
                " AND CG_PYME_EPO_INTERNO = ? " +
                " AND TO_CHAR(df_fecha_docto,'yyyy') = ? " +
                " AND ic_moneda = ? "+
                "	AND fn_monto =  ?  ";

                psRepetido = con.queryPrecompilado(query);
                psRepetido.setString(1, noDocto);
                psRepetido.setString(2, noInterPymEpo);
                psRepetido.setString(3, (new SimpleDateFormat("yyyy")).format(fechaDocto));
                psRepetido.setInt(4, noMoneda);
                psRepetido.setBigDecimal(5,monto);

                rs1=psRepetido.executeQuery();
                rs1.next();
                if(rs1.getInt(1)>1) {
                  ic_estatus_docto = "33";
                }
                rs1.close();
                psRepetido.close();
              }
          }

					//------------------------------------------------- Fin validacion de repetidos ------------------------------------------
					/* Validaciones de Fechas */
					int comp_fecha=0;
					comp_fecha=fechaDocto.compareTo(fechaVenc);
				 	if (comp_fecha > 0) {
						error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+" es menor que la Fecha del Documento "+sdf.format(fechaDocto)+". \n");
						noError.append("3\n");
				 		ok = okReg = false;
					}

					comp_fecha=FechaVenc.compareTo(FechaMin);
				 	if ( comp_fecha < 0 && (!FechaVenc.toString().equals(FechaMin.toString())) && "N".equals(doctosVencidos) ) {
						error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+", no pasa el par�metro de los dias m�nimos necesarios "+sdf.format(FechaMin)+", el Documento no puede ser Cargado. \n");
						noError.append("4\n");
			 			ok = okReg = false;
				 	} else if ( comp_fecha < 0 && (!FechaVenc.toString().equals(FechaMin.toString())) && "S".equals(doctosVencidos) ) {
						ic_estatus_docto = "9";
				 	}
					comp_fecha=fechaVenc.compareTo(FechaMax);
				 	if (comp_fecha > 0) {
			 			error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+", sobrepasa el par�metro de los dias m�ximo necesario "+sdf.format(FechaMax)+", el Documento no puede ser Cargado. \n");
						noError.append("5\n");
			 			ok = okReg = false;
				 	}
					comp_fecha=fechaDocto.compareTo(FechaHoy);
					if (comp_fecha > 0) {
				 		error.append(msgErr +" la Fecha del Documento "+sdf.format(fechaDocto)+" es mayor a la Fecha de Hoy "+sdf.format(FechaHoy)+", el Documento no puede ser Cargado. \n");
						noError.append("6\n");
				 		ok = okReg = false;
				 	}

				 	//validacion fodea 031
				 	DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				 	java.util.Date fechaMenor = myDateFormat.parse("01/01/2001");
				 	comp_fecha=fechaDocto.compareTo(fechaMenor);
					if (comp_fecha < 0) {
				 		error.append(msgErr +" la Fecha del Documento "+sdf.format(fechaDocto)+" es menor al a�o 2001, el Documento no puede ser Cargado. \n");
						noError.append("33\n");
				 		ok = okReg = false;
				 	}

					/* Validacion de la Fecha de Vencimiento que sea un dia habil. */
					//FODEA 015-2012
					boolean existDiaInhabil = false;
					diamesanio = sdf.format(fechaVenc);//ymd.substring(8,10)+"/"+ymd.substring(5,7)+"/"+ymd.substring(0,4);
					for(int fv=0; fv<diames_inhabil_x_anio.size(); fv++) {
						dm_inhabil=diames_inhabil_x_anio.get(fv).toString();
						System.out.println("dm_inhabil=="+dm_inhabil);
						System.out.println("diamesanio=="+diamesanio);
						if(diamesanio.equals(dm_inhabil)) {
							error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+" no es una fecha en dia habil. \n");
							noError.append("7\n");
							ok = okReg = false;
							existDiaInhabil=true;
						}
					}

					if(!existDiaInhabil){
						diames = ymd.substring(8,10)+"/"+ymd.substring(5,7);
						for(int fv=0; fv<diames_inhabil.size(); fv++) {
							dm_inhabil=diames_inhabil.get(fv).toString();
							if(diames.equals(dm_inhabil)) {
								error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+" no es una fecha en dia habil. \n");
								noError.append("7\n");
								ok = okReg = false;
							}
						}
					}

					/* Validacion de la fecha de Vencimiento que el dia no sea Sabado o Domingo. */
					dia_fv.setTime(FechaVenc);
					no_dia_semana=dia_fv.get(Calendar.DAY_OF_WEEK);
					if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
						error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+" no es una fecha en dia habil. \n");
						noError.append("7\n");
						ok = okReg = false;
					}

					/****************************************************************************/
					/*			INICIO VALIDACIONES FECHA VENCIMIENTO PROVEEDOR					*/
					/****************************************************************************/

					if ("S".equals(fechaVencProveedor)) {

						comp_fecha=fechaDocto.compareTo(fechaVencProv);
						if (comp_fecha > 0) {
							error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+" es menor que la Fecha del Documento "+sdf.format(fechaDocto)+". \n");
							noError.append("27\n");
							ok = okReg = false;
						}
						comp_fecha=FechaVencProv.compareTo(FechaMin);
						if (comp_fecha < 0 && (!FechaVencProv.toString().equals(FechaMin.toString())) ) {
							error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+", no pasa el par�metro de los dias m�nimos necesarios "+sdf.format(FechaMin)+", el Documento no puede ser Cargado. \n");
							noError.append("28\n");
							ok = okReg = false;
						}
						comp_fecha=fechaVencProv.compareTo(FechaMax);
						if (comp_fecha > 0) {
							error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+", sobrepasa el par�metro de los dias m�ximo necesario "+sdf.format(FechaMax)+", el Documento no puede ser Cargado. \n");
							noError.append("29\n");
							ok = okReg = false;
						}
						comp_fecha=fechaVencProv.compareTo(fechaVenc);
						if (comp_fecha > 0) {
							error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+", debe ser menor o igual a la fecha de vencimiento "+sdf.format(fechaVenc)+", el Documento no puede ser Cargado. \n");
							noError.append("30\n");
							ok = okReg = false;
						}

						/* Validacion de la Fecha de Vencimiento  Proveedor que sea un dia habil. */

						//FODEA 015-2012
						existDiaInhabil = false;
						diamesanio = sdf.format(fechaVencProv);//ymd.substring(8,10)+"/"+ymd.substring(5,7)+"/"+ymd.substring(0,4);
						for(int fv=0; fv<diames_inhabil_x_anio.size(); fv++) {
							dm_inhabil=diames_inhabil_x_anio.get(fv).toString();
							if(diamesanio.equals(dm_inhabil)) {
								error.append(msgErr +" la Fecha de Vencimiento "+sdf.format(fechaVenc)+" no es una fecha en dia habil. \n");
								noError.append("31\n");
								ok = okReg = false;
								existDiaInhabil=true;
							}
						}

						if(!existDiaInhabil){
							diames = ymdProv.substring(8,10)+"/"+ymdProv.substring(5,7);
							for(int fv=0; fv<diames_inhabil.size(); fv++) {
								dm_inhabil=diames_inhabil.get(fv).toString();
								if(diames.equals(dm_inhabil)) {
									error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+" no es una fecha en dia habil. \n");
									noError.append("31\n");
									ok = okReg = false;
								}
							}
						}

						/* Validacion de la fecha de Vencimiento que el dia no sea Sabado o Domingo. */
						dia_fv.setTime(FechaVencProv);
						no_dia_semana=dia_fv.get(Calendar.DAY_OF_WEEK);
						if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
							error.append(msgErr +" la Fecha de Vencimiento Proveedor "+sdf.format(fechaVencProv)+" no es una fecha en dia habil. \n");
							noError.append("31\n");
							ok = okReg = false;
						}


						comp_fecha=FechaPlazoPagoFVP.compareTo(FechaHoy);
						if (comp_fecha > 0) {
							comp_fecha=FechaVencProv.compareTo(FechaPlazoPagoFVP);
							int iDiasMaxFVP = (comp_fecha > 0)?iPlazoMax2FVP:iPlazoMax1FVP;

							caFV.add(Calendar.DATE, iDiasMaxFVP);
							java.util.Date FechaMaxFV = caFV.getTime();
							log.debug("iDiasMaxFVP" + iDiasMaxFVP + " FechaMaxFV "+ FechaMaxFV + " FechaVencProv" + FechaVencProv);

							comp_fecha=FechaVenc.compareTo(FechaMaxFV);
							if (comp_fecha > 0){
								error.append(msgErr +" la Fecha del Vencimiento "+sdf.format(FechaVenc)+" sobrepasa el parametro maximo entre Fecha Vencimiento Proveedor y Fecha Vencimiento. \n");
								noError.append("32\n");
								ok = okReg = false;
							}
						}
					}

					/* Validacion para el tipo de Moneda. */
					if(noMoneda!=1 && noMoneda!=54) {
						error.append(msgErr +" el Tipo de Moneda No Existe por favor verifiquelo. \n");
						noError.append("8\n");
						ok = okReg = false;
				 	}

					/* Validacion para el Monto. */
					if(monto == null || monto.doubleValue() <= 0) {
						error.append(msgErr +" El monto del documento debe ser mayor a 0. \n");
						noError.append("24\n");
						ok = okReg = false;
				 	}

						/*Para Factoraje Automatico(A)  */
					if(descuento.equalsIgnoreCase("A")){
						if(!"S".equals(factorajeIF)){
							error.append(msgErr +" la EPO no puede operar con factoraje IF. \n");
							noError.append("21\n");
							ok = okReg = false;
						}else if("".equals(nafinElectronicoIF)){
							error.append(msgErr +" Es factoraje IF y el nombre de la IF no fue proporcionado. \n");
							noError.append("22\n");
							ok = okReg = false;
						}else {


						query =" SELECT  distinct  pi.ic_if " +
							"  FROM  comrel_pyme_if pi , " +
							" comrel_nafin n , comcat_if  i " +
							" WHERE  i.ic_if = pi.ic_if  "+
							"  and pi.cs_borrado = ? " +
							" and pi.cs_opera_descuento = ?" +
							" and pi.cs_vobo_if = ? " +
							" AND n.cg_tipo = ? " +
							" and pi.ic_epo = ?" +
							" AND n.ic_nafin_electronico = ? " +
							"	AND pi.ic_if = n.ic_epo_pyme_if" ;
							pstmt = con.queryPrecompilado(query);
							pstmt.setString(1,"N");
							pstmt.setString(2,"S");
							pstmt.setString(3,"S");
							pstmt.setString(4,"I");
							pstmt.setInt(5,NO_EPO);
							pstmt.setInt(6,Integer.parseInt(nafinElectronicoIF));

							rs1 = pstmt.executeQuery();
							if(rs1.next()){
								icIf = rs1.getString(1);
							}else{
								error.append(msgErr +" La IF no se encuentra habilitada para la PYME. \n");
								noError.append("23\n");
								ok = okReg = false;
							}
							rs1.close();
							pstmt.close();

							query =
								" SELECT ie.ic_if, 'CargaDocumentoBean::proceso()' origenquery "   +
								"   FROM comrel_nafin n, comcat_if i, comrel_if_epo ie"   +
								"  WHERE i.ic_if = ie.ic_if"   +
								"    AND ie.ic_if = n.ic_epo_pyme_if"   +
								"    AND ie.cs_vobo_nafin = ? "   +
								"    AND i.cs_habilitado != ? "   +
								"    AND n.cg_tipo = ? "   +
								"    AND ie.ic_epo = ?"   +
								"    AND n.ic_nafin_electronico = ?"  ;
							pstmt = con.queryPrecompilado(query);
							pstmt.setString(1,"S");
							pstmt.setString(2,"N");
							pstmt.setString(3,"I");
							pstmt.setInt(4,NO_EPO);
							pstmt.setInt(5,Integer.parseInt(nafinElectronicoIF));

							rs1 = pstmt.executeQuery();
							if(rs1.next()){
								icIf = rs1.getString(1);
							}else{
								error.append(msgErr +" La IF no se encuentra habilitada para la EPO. \n");
								noError.append("23\n");
								ok = okReg = false;
							}
							rs1.close();
							pstmt.close();

						}
						log.info("Valor del ic_if="+icIf);
					/* Validacion para el Descuento Especial. */
					}else if(descuento.equalsIgnoreCase("V")){
						if(!"S".equals(factorajeVencido)){
							error.append(msgErr +" la EPO no puede operar con factoraje vencido. \n");
							noError.append("21\n");
							ok = okReg = false;
						}else if("".equals(nafinElectronicoIF)){
							error.append(msgErr +" Es factoraje vencido y el nombre de la IF no fue proporcionado. \n");
							noError.append("22\n");
							ok = okReg = false;
						}else{
							query =
								" SELECT ie.ic_if, 'CargaDocumentoBean::proceso()' origenquery "   +
								"   FROM comrel_nafin n, comcat_if i, comrel_if_epo ie"   +
								"  WHERE i.ic_if = ie.ic_if"   +
								"    AND ie.ic_if = n.ic_epo_pyme_if"   +
								"    AND ie.cs_vobo_nafin = ? "   +
								"    AND i.cs_habilitado != ? "   +
								"    AND n.cg_tipo = ? "   +
								"    AND ie.ic_epo = ?"   +
								"    AND n.ic_nafin_electronico = ?"  ;
							pstmt = con.queryPrecompilado(query);
							pstmt.setString(1,"S");
							pstmt.setString(2,"N");
							pstmt.setString(3,"I");
							pstmt.setInt(4,NO_EPO);
							pstmt.setInt(5,Integer.parseInt(nafinElectronicoIF));

							rs1 = pstmt.executeQuery();
							if(rs1.next()){
								icIf = rs1.getString(1);
							}else{
								error.append(msgErr +" La IF no se encuentra habilitada para la EPO. \n");
								noError.append("23\n");
								ok = okReg = false;
							}
							rs1.close();
							pstmt.close();
						}

						log.info("����������SI PASO POR AQU� Y EL VALOR DEL icIF ES = "+icIf);
						
						if(!"NULL".equals(icIf) ){
							//Fodea 016-2014
							ParametrosDescuentoBean param  = new ParametrosDescuentoBean();
							String operaFactVencidoEPO_IF = param.getOperaFactVencido(String.valueOf(NO_EPO), icIf );
							if(operaFactVencidoEPO_IF.equals("N")) {
								error.append(msgErr +" El Intermediario Financiero no opera Factoraje al Vencimiento. \n");
								noError.append("80\n");
								ok = okReg = false;
							}
						}else  {
							error.append(msgErr +" El Intermediario Financiero no opera Factoraje al Vencimiento. \n");
							noError.append("80\n");
							ok = okReg = false;	
						}
					}else if( (epoEnl instanceof WSEnlace) && descuento.equalsIgnoreCase("M")){ // Tipo M = Mandato. F046 - 2009.
						if(!bOperaFactorajeConMandato){
							// Se especifico Mandato como tipo de Factoraje pero la Epo no opera con este
							// tipo de factoraje.
							error.append(msgErr +" Tipo de Factoraje no soportado. \n");
							noError.append("70\n");
							ok = okReg = false;
						}else if("".equals(nafinElectronicoIF)){
							// El No. de Nafin electronico del Intermediario Financiero asignado es
							// Obligatorio si el Tipo de Factoraje es "M" (Mandato).
							error.append(msgErr +" El Tipo de Factoraje es M (Mandato) y el No. de Nafin Electronico del Intermediario Financiero no fue proporcionado. \n");
							noError.append("68\n");
							ok = okReg = false;
						}else{
							query =
								" SELECT ie.ic_if, 'CargaDocumentoBean::proceso()' origenquery "   +
								"   FROM comrel_nafin n, comcat_if i, comrel_if_epo ie"   +
								"  WHERE i.ic_if = ie.ic_if"   +
								"    AND ie.ic_if = n.ic_epo_pyme_if"   +
								"    AND ie.cs_vobo_nafin = ? "   +
								"    AND i.cs_habilitado != ? "   +
								"    AND n.cg_tipo = ? "   +
								"    AND ie.ic_epo = ?"   +
								"    AND n.ic_nafin_electronico = ?"  ;
							pstmt = con.queryPrecompilado(query);
							pstmt.setString(1,"S");
							pstmt.setString(2,"N");
							pstmt.setString(3,"I");
							pstmt.setInt(4,NO_EPO);
							pstmt.setInt(5,Integer.parseInt(nafinElectronicoIF));

							rs1 = pstmt.executeQuery();
							if(rs1.next()){
								icIf = rs1.getString(1);
							}else{
								error.append(msgErr +" La IF no se encuentra habilitada para la EPO. \n");
								noError.append("23\n");
								ok = okReg = false;
							}
							rs1.close();
							pstmt.close();
						}
						log.info("El valor del ic_if="+icIf);	
						
					}else if (  epoEnl instanceof PemexEnlace  && "D".equalsIgnoreCase(descuento)  ) { //2017_003
					    
					    if(!"S".equalsIgnoreCase(sFactorajeDistribuido)){						
						error.append(msgErr +" Tipo de Factoraje no soportado. \n");
						noError.append("88\n");
						ok = okReg = false;
					    }
					     
					}else if(!descuento.equalsIgnoreCase("N")) {
						error.append(msgErr +" el Tipo de Descuento No Existe por favor verifiquelo. \n");
						noError.append("9\n");
						ok = okReg = false;
					}

					/* Validacion de los campos Adicionales. */
					log.debug("\n totalcd: "+totalcd);
					if(totalcd>=1) {
						String camp="", rc=""; int num=0;
						for(int n=1; n<=totalcd; n++) {
							switch(noCampo[n]) {
								case 1: camp=cd1; num=1; break;
								case 2: camp=cd2; num=2; break;
								case 3: camp=cd3; num=3; break;
								case 4: camp=cd4; num=4; break;
								case 5: camp=cd5; num=5; break;
							}
							log.debug(noCampo[n]+" "+nombreCampo[n]+" "+tipoDato[n]+" "+longitud[n]);
							if(noCampo[n]==num && (!camp.equals(""))) {
								if(tipoDato[n].equals("N")) {
									if(esNumero(camp)==false) {
										error.append(msgErr +" el Tipo de Dato del Campo Din�mico No."+num+" "+nombreCampo[n]+" debe de ser Num�rico. \n");
										noError.append("15\n");
	                				ok = okReg = false;
									}
								}
								if(longitud[n] < camp.length()) {
									error.append(msgErr +" el tama�o del Campo Din�mico No."+num+" "+nombreCampo[n]+" excede la longitud definida por su epo que es de: "+longitud[n]+", verifiquelo. \n");
									noError.append("16\n");
	                			ok = okReg = false;
								}
								if(NO_EPO==1 && num == 5){
									cd5 = (!"".equals(cd5))?cd5.toUpperCase():cd5;
									String RefString="ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890";
									String TempChar = "";
									for(int Count=0;Count < camp.length(); Count++) {
										TempChar = camp.substring(Count,Count+1);
										if(RefString.indexOf(TempChar.toUpperCase(),0)==-1) {
											error.append(msgErr + " el Campo Din�mico No."+num+" "+nombreCampo[n]+" no acepta caracteres diferentes a letras y/o numeros. \n");
											noError.append("26\n");
											ok = okReg = false;
											break;
										}
									}
								}
							}
							// Foda 027 - 2006 EGB Cobranza Referenciada
							if ("".equals(camp)) {
								if("R".equals(tipoCobranza[n]) && "S".equals(obligatorio[n]) ) {
									error.append(msgErr +" el Campo Din�mico No."+num+" "+nombreCampo[n]+" debe ser capturado, verifiquelo. \n");
									noError.append("25\n");
	                			ok = okReg = false;
								}
							}
						} //for
						for(int a=1; a<=totalcd; a++) {
							switch(a) {
								case 1: rc=cd1; break;
								case 2: rc=cd2; break;
								case 3: rc=cd3; break;
								case 4: rc=cd4; break;
								case 5: rc=cd5; break;
							}
							if((totalcd<a) && (!rc.equals(""))) {
								error.append(msgErr +" la EPO no tiene un Campo Din�mico No."+a+" definido, por favor verifiquelo. \n");
								noError.append("17\n");
								ok = okReg = false;
							}
						} //for
					} else if(totalcd==0) {
						if(!cd1.equals("") || !cd2.equals("") || !cd3.equals("") || !cd4.equals("") || !cd5.equals("")){
							error.append(msgErr +" la Epo no ha definido Campos Din�micos, no pueden ser procesados. \n");
							noError.append("18\n");
							ok = okReg = false;
						}
					}

					if ("S".equals(sPubEPOPEFFlagVal) && "S".equals(sPubEPOPEFDocto1Val)){
						if (fechaEntrega == null){
							error.append(msgErr +" La fecha de recepci�n de bienes y servicios no puede omitirse. \n");
							noError.append("58\n");
							ok = okReg = false;
						}
					}
					if ("S".equals(sPubEPOPEFFlagVal) && "S".equals(sPubEPOPEFDocto2Val)){
						if (sTipoCompra == null){
							error.append(msgErr +" El tipo de compra (procedimiento) no puede omitirse. \n");
							noError.append("59\n");
							ok = okReg = false;
						}else{
							if (sTipoCompra.length() > 1){
								error.append(msgErr +" El tipo de compra (procedimiento) excede la longitud declarada que es de 1 car�cter. \n");
								noError.append("60\n");
								ok = okReg = false;
							}
							if (valoresTipoCompra.indexOf(sTipoCompra)==-1){
								error.append(msgErr +" El tipo de compra (procedimiento) no es v�lido. Valores validos: "+valoresTipoCompra+" \n");
								noError.append("61\n");
								ok = okReg = false;
							}
						}
					}

					if ("S".equals(sPubEPOPEFFlagVal) && "S".equals(sPubEPOPEFDocto3Val)){
						if (sClavePresup == null){
							error.append(msgErr +" El Clasificador por objeto del gasto no puede omitirse. \n");
							noError.append("62\n");
							ok = okReg = false;
						}else{
							if (sClavePresup.length() > 5){
								error.append(msgErr +" El Clasificador por objeto del gasto excede la longitud declarada que es de 5 caracteres. \n");
								noError.append("63\n");
								ok = okReg = false;
							}
						}
					}

					if ("S".equals(sPubEPOPEFFlagVal) && "S".equals(sPubEPOPEFDocto4Val)){
						if (sPeriodo == null){
							error.append(msgErr +" El plazo m�ximo no puede omitirse. \n");
							noError.append("64\n");
							ok = okReg = false;
						}else{
							if (sPeriodo.length() > 3){
								error.append(msgErr +" El plazo m�ximo excede la longitud declarada que es de 3 caracteres. \n");
								noError.append("65\n");
								ok = okReg = false;
							}
							if (iPeriodo > 999){
								error.append(msgErr +" El plazo m�ximo no es un n�mero v�lido (>999). \n");
								noError.append("66\n");
								ok = okReg = false;
							}
							if (iPeriodo < 0){
								error.append(msgErr +" El plazo m�ximo no puede ser negativo. \n");
								noError.append("67\n");
								ok = okReg = false;
							}
						}
					}

					/* Validar la Clave del Mandante. Fodea 046 - 2009 */
					icMandante = "";
					if( (epoEnl instanceof WSEnlace) && bOperaFactorajeConMandato && descuento.equalsIgnoreCase("M")){
						// Revisar que la Clave del Mandante sea un n�mero v�lido.
						if(!Comunes.esNumero(claveMandante)){
							error.append(msgErr +" La Clave del Mandante no es un numero valido. \n");
							noError.append("69\n");
							ok = okReg = false;
						}else{ // Obtener IC_MANDANTE
							icMandante = getIcMandante(claveMandante);
							if(icMandante.equals("")){
								error.append(msgErr +" La Clave del Mandante no existe por favor verifiquelo. \n");
								noError.append("71\n");
								ok = okReg = false;
							}
						}
					}
					//==============================================================================//FODEA 026 - 2009 ACF (I)

					if (epoEnl instanceof SIOREnlace) {
						if (noMoneda == 1) {
							//Valida la cuenta Clabe
							if (icViaLiquidacion.equals("TEF") || icViaLiquidacion.equals("SPEI")) {
								if (!cuentaClabe.equals("")) {
									if (!Comunes.esCuentaClabeValida(cuentaClabe)) {
										error.append(msgErr +" La cuenta CLABE no es valida. \n");
										noError.append("72\n");
										ok = okReg = false;
										flagCadenaNafin = false;
									}
								} else {
									error.append(msgErr +" La cuenta CLABE es un valor requerido. \n");
									noError.append("73\n");
									ok = okReg = false;
									flagCadenaNafin = false;
								}
								//Valida la clave del banco TEF
								if (!icBancosTef.equals("")) {
									StringBuffer strSQL = new StringBuffer();
									PreparedStatement pst = null;
									ResultSet rst = null;
									int esBancoTef = 0;
									strSQL.append(" SELECT COUNT(ic_bancos_tef) AS banco_tef FROM comcat_bancos_tef WHERE ic_bancos_tef = ?");
									pst = con.queryPrecompilado(strSQL.toString());
									pst.setInt(1, Integer.parseInt(icBancosTef));
									rst = pst.executeQuery();
									while (rst.next()) {
										esBancoTef = rst.getInt("banco_tef");
									}
									rst.close();
									pst.close();
									if (esBancoTef == 0) {
										error.append(msgErr +" La clave del banco TEF no es valido. \n");
										noError.append("74\n");
										ok = okReg = false;
										flagCadenaNafin = false;
									}
								} else {
									error.append(msgErr +" La clave del banco TEF es un valor requerido. \n");
									noError.append("75\n");
									ok = okReg = false;
									flagCadenaNafin = false;
								}
								//Valida el tipo de liquidaci�n
								if (!icViaLiquidacion.equals("")) {
									if (!icViaLiquidacion.equals("TEF") && !icViaLiquidacion.equals("SPEI")) {
										error.append(msgErr +" El tipo de liquidacion es incorrecto. \n");
										noError.append("76\n");
										ok = okReg = false;
										flagCadenaNafin = false;
									}
								} else {
									error.append(msgErr +" El tipo de liquidacion es un valor requerido. \n");
									noError.append("77\n");
									ok = okReg = false;
									flagCadenaNafin = false;
								}
							}
						}
					}

					if(!referencia.equals("")  ||  !referencia.equals("null") ){
						if (referencia.indexOf("'") > -1){
							error.append(msgErr +"La referencia tiene caracteres incorrectos. \n");
							noError.append("78\n");
							ok = okReg = false;
							flagCadenaNafin = false;
						}
					}
				//==============================================================================//FODEA 026 - 2009 ACF (F)
					
					//***********************   Inicia  Validaciones pemex factoraje distribuido  2017_003
				    
					log.info("sFactorajeDistribuido  "+sFactorajeDistribuido);
					log.info("descuento  "+descuento);
					log.info("nafinElectronicoIF  "+nafinElectronicoIF);
					log.info("numContrato  "+numContrato);
					log.info("numCopade  "+numCopade);
				        log.info("porc_beneficiario  "+porcBeneficiario);
				    
				    
					 if (  epoEnl instanceof PemexEnlace  &&  "S".equalsIgnoreCase(sFactorajeDistribuido) && "D".equalsIgnoreCase(descuento)    ) {  // 2017_003  
					
					    if(!"".equals(nafinElectronicoIF) ){   
					    
						int existe =  this.getExisteicIFxReferencia(NO_EPO,  nafinElectronicoIF );
						if(existe==0){
						    error.append(msgErr +"No existe una clave de Beneficiario o Cesionario.  \n");
						   noError.append("82\n");
						    ok = okReg = false;
						    flagCadenaNafin = false;   
						}else if(existe >1){
						    error.append(msgErr +"Favor de verificar la clave del beneficiario o Cesionario, ya que se encuentra registrada para varios IFs. \n");
						    noError.append("83\n");
						    ok = okReg = false;
						    flagCadenaNafin = false;   
						}else if(existe ==1){
						    icBeneficiario =   this.geticIFxReferencia(NO_EPO, nafinElectronicoIF );
						}													
					    }else  {
						error.append(msgErr +"La clave del Beneficiario o Cesionario, es un valor requerido. \n");
						noError.append("84\n");
						ok = okReg = false;
						flagCadenaNafin = false;   
					    }
					    
					    if("".equals(numContrato) ){    					     
					    	error.append(msgErr +"El N�mero de Contrato, es un valor requerido. \n");
						noError.append("85\n");
						ok = okReg = false;
						flagCadenaNafin = false;   
					    }					    
					    if("".equals(numCopade) ){ 
					    	error.append(msgErr +"El N�mero COPADE, es un valor requerido. \n");
						noError.append("86\n");
						ok = okReg = false;
						flagCadenaNafin = false;   
					    }
					    
					    if(!"".equals(porcBeneficiario) ){ 
						
						if(Integer.parseInt(porcBeneficiario)<=0)  { 
						     error.append(msgErr +"El valor del porcentaje del Beneficiario es incorrecto. Favor de verificarlo. \n");
						    noError.append("87\n");
						    ok = okReg = false;
						    flagCadenaNafin = false;   
						 }else if(Integer.parseInt(porcBeneficiario)>100)  { 
						     error.append(msgErr +"El valor del porcentaje del Beneficiario es incorrecto. Favor de verificarlo. \n");
						    noError.append("87\n");
						    ok = okReg = false;
						    flagCadenaNafin = false;   
						 }											 
						 
					    }
						 
					 }
					//***********************  Termina  Validaciones pemex factoraje distribuido  2017_003					

					if(noMoneda == 1) {
						mtodocmn = mtodocmn.add(monto);
						totdocmn++;
						if(okReg) itotacepmn++; else itotrechmn++;
					} else if(noMoneda == 54) {
						mtodocdol = mtodocdol.add(monto);
						totdocdol++;
						if(okReg) itotacepdol++; else itotrechdol++;
					}



					///////********************* GENERAL *************************////////
					// Inserta los Errores en com_doctos_err_pub_imss para IMSS
					if(error.length()>0) erroreg.append(error.toString());

					if(okReg) {
						// Se calcula el descuento del monto del documento.
						BigDecimal montoDscto=new BigDecimal("0.0");
						montoDscto=monto.multiply(fnAforoP);

						// Obtenemos ic_documento maximo de la tabla com_documento
						int ic_documento=0;
						query =
							" SELECT seq_com_documento.NEXTVAL FROM DUAL ";
//							" SELECT (NVL (MAX (ic_documento), 0) + 1), 'CargaDocumentoBean::proceso()' origenquery "   +011107
//							"   FROM com_documento"  ;
						rs1=con.queryDB(query);
						if(rs1.next()) {
							ic_documento = rs1.getInt(1);
						}
						rs1.close(); con.cierraStatement();

						// Inserta los Datos com_documento.
						referencia = (referencia.equals(""))?"null":"'"+referencia+"'";
						cd1 = (totalcd>=1)?(cd1.equals(""))?"null":"'"+cd1+"'":"null";
						cd2 = (totalcd>=2)?(cd2.equals(""))?"null":"'"+cd2+"'":"null";
						cd3 = (totalcd>=3)?(cd3.equals(""))?"null":"'"+cd3+"'":"null";
						cd4 = (totalcd>=4)?(cd4.equals(""))?"null":"'"+cd4+"'":"null";
						cd5 = (totalcd==5)?(cd5.equals(""))?"null":"'"+cd5+"'":"null";

						log.debug("������������TAMBIEN PASO POR AQUI");
						query =
							" INSERT INTO com_documento ("   +
							"              ic_documento, ic_pyme, ic_epo, cc_acuse, ig_numero_docto,"   +
							"              df_fecha_docto, df_fecha_venc, ic_moneda, fn_monto,"   +
							"              cs_dscto_especial, ic_estatus_docto, ct_referencia,"   +
							"              cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
							"              cg_campo5, ic_if"   + (("S".equals(fechaVencProveedor))?", df_fecha_venc_pyme":"") +
							(("S".equals(sPubEPOPEFFlagVal) && fechaEntrega != null)?", df_entrega":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sTipoCompra != null)?", cg_tipo_compra":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sClavePresup != null)?", cg_clave_presupuestaria":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sPeriodo != null)?", cg_periodo":"") +
							((bOperaFactorajeConMandato     && (epoEnl instanceof WSEnlace) && !icMandante.equals(""))?", ic_mandante":"") +							
							( (epoEnl instanceof PemexEnlace &&  "S".equalsIgnoreCase(sFactorajeDistribuido)  && "D".equalsIgnoreCase(descuento)  )?", FN_PORC_BENEFICIARIO, IC_BENEFICIARIO , CG_NUM_COPADE  ,  CG_NUM_CONTRATO ":"") + //2017_003					
							
							")" +
							"      VALUES ("   +
							"              "+ic_documento+", "+ic_pyme+", "+NO_EPO+", '"+acuse+"', '"+noDocto+"',"   +
							"              TO_DATE('"+sdf.format(fechaDocto)+"', 'dd/mm/yyyy'), TO_DATE('"+sdf.format(fechaVenc)+"', 'dd/mm/yyyy'), "+noMoneda+", "+monto.toPlainString()+","   +
							"              '"+descuento+"', "+ic_estatus_docto+", "+referencia+","   +
							"              "+cd1+", "+cd2+", "+cd3+", "+cd4+","+cd5+","   +
							"              "+icIf+ (("S".equals(fechaVencProveedor))?", to_date('"+sdf.format(fechaVencProv)+"', 'dd/mm/yyyy')":"") +
							(("S".equals(sPubEPOPEFFlagVal) && fechaEntrega != null)?", TO_DATE('"+ sdf.format(fechaEntrega)+"', 'dd/mm/yyyy')":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sTipoCompra != null)?", '"+ sTipoCompra+"'":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sClavePresup != null)?", '"+sClavePresup+"'":"") +
							(("S".equals(sPubEPOPEFFlagVal) && sPeriodo != null)?", '"+sPeriodo+"'":"") +
							((bOperaFactorajeConMandato     && (epoEnl instanceof WSEnlace) && !icMandante.equals(""))?", " + icMandante:"") +
							( (epoEnl instanceof PemexEnlace &&  "S".equalsIgnoreCase(sFactorajeDistribuido)  && "D".equalsIgnoreCase(descuento) )?","+porcBeneficiario+","+ icBeneficiario +",'"+ numCopade+"','"+ numContrato+"'":"") + //2017_003							
							")"  ;
						log.debug(query);
						ErroresEnl sErrores = new ErroresEnl();
						try{
							sErrores.setCadenaOriginal(cadenaOriginal);
							//log.debug("INSERTA");
							con.ejecutaSQL(query);
						    

						    
							//if(epoEnl instanceof FonacotEnlace || epoEnl instanceof FideEnlace || epoEnl instanceof GDFEnlace) {
							if(epoEnl instanceof EpoPEF || epoEnl instanceof GDFEnlace) {
							
								sErrores.setCcAcuse(acuEnl.getCcAcuse());
								sErrores.setIgNumeroDocto(noDocto);
								sErrores.setCg_pyme_epo_interno(noInterPymEpo);
								sErrores.setIgNumeroError("0");
								sErrores.setCsDsctoEspecial(descuento);
								
								if((epoEnl instanceof CFEEnlace) ) {
									sErrores.setInAEjercicio(cd1);
									sErrores.setInSociedad(cd2);
								}
								if((epoEnl instanceof PemexREFEnlace) ) {
									sErrores.setInAEjercicio(cd3);
								}
														    //2017_003	
							    if (epoEnl instanceof PemexEnlace  && "S".equalsIgnoreCase(sFactorajeDistribuido)  && "D".equalsIgnoreCase(descuento)   ) {  
								sErrores.setIcNafinElectronico(nafinElectronicoIF);	
								sErrores.setNumContrato(numContrato);
								sErrores.setNumCopade(numCopade);							   						    
							    }else  {
								sErrores.setIcNafinElectronico("");	
								sErrores.setNumContrato("");
								sErrores.setNumCopade("");    
							    }
								if((epoEnl instanceof FonacotEnlace) || (epoEnl instanceof GDFEnlace) ) {
									sErrores.setCg_pyme_epo_interno(noInterPymEpo);
								}
								if("33".equals(ic_estatus_docto))
                {
                  sErrores.setIgNumeroError("81");
                  sErrores.setCgError("Docto. publicado como Pendiente Duplicado por posible duplicidad en su informacion, ingrese a Nafinet para validarlo.");
                }else{
                  sErrores.setCgError("Registro publicado exitosamente");
                }
								if ("S".equals(sDigitoIdentificadorFlag)){
									sErrores.setCgDigitoIdentificador(Comunes.rellenaCeros(NO_EPO+"", 4)+Comunes.rellenaCeros(ic_documento+"", 11));
								}
								query = epoEnl.getInsertaErrores(sErrores);
								log.debug("Inserta Registro Exitoso:: " + query);
								con.ejecutaSQL(query);
							}
							
						       //==============================================================================//FODEA 026 - 2009 ACF (I)

							if (epoEnl instanceof SIOREnlace) {
								if (noMoneda == 1) {
									if (flagCadenaNafin && (icViaLiquidacion.equals("TEF") || icViaLiquidacion.equals("SPEI"))) {
										StringBuffer strSQL = new StringBuffer();
										PreparedStatement pst = null;
										ResultSet rst = null;
										String nafinElectronicoPyme = "";
										String icCuenta = "";
										//SE OBTIENE EL NAFIN ELECTRONICO DEL PROVEEDOR
										strSQL.append(" SELECT ic_nafin_electronico AS nafin_electronico FROM comrel_nafin WHERE ic_epo_pyme_if = ? AND cg_tipo = ?");
										log.debug("..:: strSQL: "+strSQL.toString());
										pst = con.queryPrecompilado(strSQL.toString());
										pst.setInt(1, ic_pyme);
										pst.setString(2, "P");
										rst = pst.executeQuery();
										while (rst.next()) {
											nafinElectronicoPyme = rst.getString("nafin_electronico")==null?"":rst.getString("nafin_electronico");
										}
										rst.close();
										pst.close();
										//SE VALIDA QUE EXISTE UNA CUENTA CLABE REGISTRADA PARA ESE PROVEEDOR CON LA EPO
										strSQL = new StringBuffer();
										strSQL.append(" SELECT ic_cuenta FROM com_cuentas WHERE ic_nafin_electronico = ? AND ic_tipo_cuenta = ? AND cg_cuenta = ? AND ic_epo = ?");
										log.debug("..:: strSQL: "+strSQL.toString());
										pst = con.queryPrecompilado(strSQL.toString());
										pst.setInt(1, Integer.parseInt(nafinElectronicoPyme));
										pst.setInt(2, 40);
										pst.setString(3, cuentaClabe);
										pst.setInt(4, NO_EPO);
										rst = pst.executeQuery();
										while (rst.next()) {
											icCuenta = rst.getString("ic_cuenta")==null?"":rst.getString("ic_cuenta");
										}
										rst.close();
										pst.close();

										if (!icCuenta.equals("")) {
											//SI EXISTE LA CUENTA REGISTRADA, SE AGREGA LA RELACION A LA TABLA COMREL_DOCTO_CUENTA
											strSQL = new StringBuffer();
											strSQL.append(" INSERT INTO comrel_docto_cuenta (ic_documento, ic_cuenta, cg_via_liq) VALUES (?, ?, ?)");
											log.debug("..:: strSQL: "+strSQL.toString());
											pst = con.queryPrecompilado(strSQL.toString());
											pst.setInt(1, ic_documento);
											pst.setLong(2, Long.parseLong(icCuenta));
											pst.setString(3, icViaLiquidacion);
											pst.executeUpdate();
											pst.close();
										} else {
											//SI NO EXISTE LA CUENTA PARA LA EPO, SE VALIDA ENTONCES QUE EXISTA PARA EL EL PROVEEDOR CON CUALQUIER OTRA EPO CON IC_ESTATUS_CECOBAN = 99
											strSQL = new StringBuffer();
											strSQL.append(
													" SELECT ic_cuenta " +
													" FROM com_cuentas " +
													" WHERE ic_nafin_electronico = ? " +
													" AND ic_tipo_cuenta = ? " +
													" AND ic_estatus_cecoban = ? " +
													" AND cg_cuenta = ? " +
													" AND ic_epo <> ? ");

											log.debug("..:: strSQL: "+strSQL.toString());
											pst = con.queryPrecompilado(strSQL.toString());
											pst.setInt(1, Integer.parseInt(nafinElectronicoPyme));
											pst.setInt(2, 40);
											pst.setInt(3, 99);
											pst.setString(4, cuentaClabe);
											pst.setInt(5, NO_EPO);
											rst = pst.executeQuery();
											if (rst.next()) {
												icCuenta = rst.getString("ic_cuenta")==null?"":rst.getString("ic_cuenta");
											}
											rst.close();
											pst.close();

											if (!icCuenta.equals("")) {
												//SI EXISTE LA CUENTA SE CREA UNA COPIA PARA LA EPO Y SE AGREGA LA RELACI�N A LA TABLA COMREL_DOCTO_CUENTA
												String nuevaIcCuenta = "";

												strSQL = new StringBuffer();
												strSQL.append(" SELECT NVL(MAX(ic_cuenta), 0) + 1 AS IC_CUENTA FROM com_cuentas");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												rst = pst.executeQuery();
												while (rst.next()) {
													nuevaIcCuenta = rst.getString("ic_cuenta")==null?"":rst.getString("ic_cuenta");
												}
												rst.close();
												pst.close();

												strSQL = new StringBuffer();
												strSQL.append(" INSERT INTO com_cuentas (ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, ic_bancos_tef, cg_cuenta, ic_usuario, ic_estatus_cecoban, ic_tipo_cuenta, ic_epo)");
												strSQL.append(" SELECT "+nuevaIcCuenta+", ic_nafin_electronico, ic_producto_nafin, ic_moneda, ic_bancos_tef, cg_cuenta, 'proc_aut', 99, ic_tipo_cuenta, "+NO_EPO);//AJUSTE_ENL_SIOR ACF 19-07-2010
												// A solicitud de Israel Herrera se modifica la insercion de la cuenta para que esta este autorizada por default (ic_estatus_cecoban = 99)
												//strSQL.append(" SELECT ("+nuevaIcCuenta+", ic_nafin_electronico, ic_producto_nafin, ic_moneda, ic_bancos_tef, cg_cuenta, 'proc_aut', ic_estatus_cecoban, ic_tipo_cuenta, "+NO_EPO+")");
												strSQL.append(" FROM com_cuentas");
												strSQL.append(" WHERE ic_cuenta = ?");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												pst.setLong(1, Long.parseLong(icCuenta));
												pst.executeUpdate();
												pst.close();

												strSQL = new StringBuffer();
												strSQL.append(" INSERT INTO comrel_docto_cuenta (ic_documento, ic_cuenta, cg_via_liq) VALUES (?, ?, ?)");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												pst.setInt(1, ic_documento);
												pst.setLong(2, Long.parseLong(nuevaIcCuenta));
												pst.setString(3, icViaLiquidacion);
												pst.executeUpdate();
												pst.close();
											} else {
												//SI NO EXISTE LA CUENTA CON OTRA CADENA, ENTONCES SE AGREGA UN NUEVO REGISTRO A LA TABLA COM_CUENTAS Y SE AGREGA LA RELACI�N A COMREL_DOCTO_CUENTAS
												strSQL = new StringBuffer();
												strSQL.append(" SELECT NVL(MAX(ic_cuenta), 0) + 1 AS IC_CUENTA FROM com_cuentas");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												rst = pst.executeQuery();
												while (rst.next()) {
													icCuenta = rst.getString("ic_cuenta")==null?"":rst.getString("ic_cuenta");
												}
												rst.close();
												pst.close();

												strSQL = new StringBuffer();
												strSQL.append(" INSERT INTO com_cuentas (ic_cuenta,");
												strSQL.append(" ic_nafin_electronico,");
												strSQL.append(" ic_producto_nafin,");
												strSQL.append(" ic_moneda,");
												strSQL.append(" cg_cuenta,");
												strSQL.append(" ic_bancos_tef,");
												strSQL.append(" ic_usuario,");
												strSQL.append(" ic_tipo_cuenta,");
												strSQL.append(" ic_epo,");
												strSQL.append(" cg_tipo_afiliado,");
												strSQL.append(" ic_estatus_cecoban)");
												strSQL.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												pst.setInt(1, Integer.parseInt(icCuenta));
												pst.setInt(2, Integer.parseInt(nafinElectronicoPyme));
												pst.setInt(3, 1);
												pst.setInt(4, noMoneda);
												pst.setString(5, cuentaClabe);
												pst.setInt(6, Integer.parseInt(icBancosTef));
												pst.setString(7, "proc_aut");
												pst.setInt(8, 40);
												pst.setInt(9, NO_EPO);
												pst.setString(10, "P");
												pst.setInt(11, 99);// A solicitud de Israel Herrera se modifica la insercion de la cuenta para que esta este autorizada por default (ic_estatus_cecoban = 99)
												pst.executeUpdate();
												pst.close();

												strSQL = new StringBuffer();
												strSQL.append(" INSERT INTO comrel_docto_cuenta (ic_documento, ic_cuenta, cg_via_liq) VALUES (?, ?, ?)");
												log.debug("..:: strSQL: "+strSQL.toString());
												pst = con.queryPrecompilado(strSQL.toString());
												pst.setInt(1, ic_documento);
												pst.setLong(2, Long.parseLong(icCuenta));
												pst.setString(3, icViaLiquidacion);
												pst.executeUpdate();
												pst.close();
											}
										}
									}
								}
							}

							//==============================================================================//FODEA 026 - 2009 ACF (F)
							iTotalAlta++;
						} catch(SQLException sqle) {
							//log.error("SQLException");
							sqle.printStackTrace();
							if (sqle.getErrorCode()==20002) {
								sblog.append("No se puede insertar en com_documento porque esta duplicado el numero de documento."+sqle+"\n");
								error.append(msgErr+" No se puede insertar en com_documento porque esta duplicado el numero de documento.\n");
								noError.append("34\n");
								ok = okReg = false;
								if(noMoneda == 1) {
									itotacepmn--;
									itotrechmn++;
								}
								else if(noMoneda == 54) {
									itotacepdol--;
									itotrechdol++;
								}
							} else {
								sblog.append("Error de Concurrencia al insertar en com_documento."+sqle+"\n");
								error.append(msgErr+" Concurrencia. \n");
								noError.append("35\n");
								ok = okReg = false;
							}
						} catch(Exception e) {
							//log.error("Exception");
							sblog.append("Error de Concurrencia al insertar en com_documento."+e+"\n");
							error.append(msgErr+" Concurrencia. "+e+" \n");
							noError.append("36\n");
							ok = okReg = false;
						}
						//log.debug("DESP TRY");

						if(epoEnl instanceof EpoCentralizada && okReg) {
							EpoPEF epoPEF = new EpoPEF();
							query =
									" INSERT INTO "+epoEnl.getTablaAcuse()+"_DET"+
									"             (ig_numero_docto, cg_pyme_epo_interno, df_fecha_docto,"   +
									"              df_fecha_venc, ic_moneda, fn_monto, cs_dscto_especial,"   +
									"              ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"   +
									"              cg_campo5, cc_acuse"+
									((epoEnl instanceof EpoPEF)?","+epoPEF.getCamposDigitoIdentificador():"")+
									"             )"   +
									"      VALUES (?, ?, ?,"   +
									"              ?, ?, ?, ?,"   +
									"              ?, ?, ?, ?, ?,"   +
									"              ?, ?"   +
									((epoEnl instanceof EpoPEF)?",?":"")+
									"             )"  ;
							pstmt = con.queryPrecompilado(query);
							pstmt.setString(	1, noDocto);
							pstmt.setString(	2, noInterPymEpo);
							pstmt.setDate(		3, fechaDocto, Calendar.getInstance());
							pstmt.setDate(		4, fechaVenc,Calendar.getInstance());
							pstmt.setInt(		5, noMoneda);
							pstmt.setBigDecimal(6, monto);
							pstmt.setString(	7, descuento);
							//log.debug(query);

							if(!"".equals(referencia))
								pstmt.setString(8, Comunes.quitaComitasSimples(referencia).trim());
							else
								pstmt.setNull(8, Types.VARCHAR);

							if(!"".equals(cd1))
								pstmt.setString(9, Comunes.quitaComitasSimples(cd1).trim());
							else
								pstmt.setNull(9, Types.VARCHAR);

							if(!"".equals(cd2))
								pstmt.setString(10, Comunes.quitaComitasSimples(cd2).trim());
							else
								pstmt.setNull(10, Types.VARCHAR);

							if(!"".equals(cd3))
								pstmt.setString(11, Comunes.quitaComitasSimples(cd3).trim());
							else
								pstmt.setNull(11, Types.VARCHAR);

							if(!"".equals(cd4))
								pstmt.setString(12, Comunes.quitaComitasSimples(cd4).trim());
							else
								pstmt.setNull(12, Types.VARCHAR);

							if(!"".equals(cd5))
								pstmt.setString(13, Comunes.quitaComitasSimples(cd5).trim());
							else
								pstmt.setNull(13, Types.VARCHAR);

							pstmt.setString(	14, acuse.toString());

							if (epoEnl instanceof EpoPEF){
								pstmt.setString(15, sErrores.getCgDigitoIdentificador() );
							}

							//log.debug("ANTES DEL INSERT");
							pstmt.executeUpdate();
							pstmt.clearParameters();
							pstmt.close();
						}//if(epoEnl instanceof EpoCentralizada)
					}//if(okReg)

					//acualiza datos del acuse segun doctos aceptados y rechazados
					//log.debug("\n monto: "+monto);
					//log.debug("\n noMoneda: "+noMoneda);
					//log.debug("\n okReg: "+okReg);
					//log.debug("\n descuento: "+descuento);
					acuEnl.add(monto.doubleValue(), noMoneda, okReg, descuento);
					erroraux = error;
					///////********************* GENERAL *************************////////
					if(!okReg) {
						String linea="";
						String numError = "";
						st=new StringTokenizer(error.toString(),"\n");
						st2=new StringTokenizer(noError.toString(),"\n");
						cd1 = (cd1.equals("")|| cd1.equals("null"))?"null":"'"+cd1+"'";
						cd2 = (cd2.equals("")|| cd2.equals("null"))?"null":"'"+cd2+"'";
						cd3 = (cd3.equals("")|| cd3.equals("null"))?"null":"'"+cd3+"'";
						cd4 = (cd4.equals("")|| cd4.equals("null"))?"null":"'"+cd4+"'";
						cd5 = (cd5.equals("")|| cd5.equals("null"))?"null":"'"+cd5+"'";

						while(st2.hasMoreTokens()) {
							ErroresEnl err = new ErroresEnl();
							err.setCadenaOriginal(cadenaOriginal);
							
							err.setCcAcuse(acuEnl.getCcAcuse());
							numError = st2.nextToken();
							iTotalError++;
							linea=st.nextToken().trim();
							//linea=(linea.length()>120)?linea.substring(0,120):linea.substring(0,linea.length());
							err.setIgNumeroDocto(noDocto);
							err.setIgNumeroError(numError);
							err.setCgError(linea);
							if((epoEnl instanceof CFEEnlace) ) {
								err.setInAEjercicio(cd1);
							}
							if((epoEnl instanceof PemexREFEnlace) ) {
								err.setInAEjercicio(cd3);
							}
							if((epoEnl instanceof FonacotEnlace) || (epoEnl instanceof GDFEnlace) ) {
								err.setCg_pyme_epo_interno(noInterPymEpo);
							}
							err.setInSociedad(cd2);
						    
						    //2017_003
						    if (epoEnl instanceof PemexEnlace  && "S".equalsIgnoreCase(sFactorajeDistribuido)  && "D".equalsIgnoreCase(descuento)   ) {  
							    
							    err.setCsDsctoEspecial(descuento);
							    err.setIcNafinElectronico(nafinElectronicoIF);	
							    err.setNumContrato(numContrato);
							    err.setNumCopade(numCopade);
						    }
							if(!(epoEnl instanceof EpoEnlaceAll)){
								query = epoEnl.getInsertaErrores(err);

								try{
									log.debug("Inserta Registro Con Error:: 111" + query);
									con.ejecutaSQL(query);
								}catch(Exception e){
									log.error("CargaDocumentoBean::proceso(Exception):\n\t\tError al insertar en "+epoEnl.getTablaErrores()+". Seccion 1.\n");
									e.printStackTrace();
									sblog.append("Error al insertar en "+epoEnl.getTablaErrores()+".\n");
								}
							}else{
								epoEnl.addErrores(err);
							}
							//if(!con.ejecutaSQL(query)) { log.error("Error al insertar en com_doctos_err_pub_imss.\n"); }
						}//while(st.hasMoreTokens())

					}//if(!okReg)
					error.delete(0,error.length());
					noError.delete(0,noError.length());

				} //while(rs.next()) epoEnl.getDocuments()
				rs.close(); con.cierraStatement();

				if(epoEnl instanceof EpoEnlaceDetalle){
					con.terminaTransaccion(true);
				}

				if(epoEnl instanceof EpoEnlaceAll){
					if(ok)
						con.terminaTransaccion(true);
					else
						con.terminaTransaccion(false);

					//inserta errores para epos todo o nada
					lAlmacenErrores = epoEnl.getErrores();
					for(int a=0;a<lAlmacenErrores.size();a++){
						ErroresEnl err = new ErroresEnl();
						err.setCadenaOriginal(cadenaOriginal);
						
						err = (ErroresEnl)lAlmacenErrores.get(a);
						query = epoEnl.getInsertaErrores(err);
						try{
							con.ejecutaSQL(query);
						}catch(Exception e){
							log.error("CargaDocumentoBean::proceso(Exception):\n\t\tError al insertar en "+epoEnl.getTablaErrores()+". Seccion 2.\n");
							e.printStackTrace();
							sblog.append("Error al insertar en "+epoEnl.getTablaErrores()+".\n");
						}
					}
				}
			} //if hayreg
			/*******************************************************
			 * codigo para epos con documentos detalle *
			 *******************************************************/

			if(epoEnl instanceof EpoEnlaceDetalle){
				// Para saber si tiene la epo Campos Detalle definidos.
				boolean okDet = true, okRegDet = true;
				StringBuffer errord=new StringBuffer(); StringBuffer noDoctosd=new StringBuffer();
				StringBuffer noErrord=new StringBuffer();
				int noCamposDetalle=0, iTotProcDetAlta=0, iTotalErrorDet=0;
				Vector noCampoDet=new Vector();	Vector nomCampoDet=new Vector();
				Vector tipoDatoDet=new Vector(); Vector longDet=new Vector();
				ResultSet rs2;
				String linea = "";
				// Obtenemos el No Campo, Nombre del Campo, Tipo de Dato y Longitud de cada Campo Detalle.
				query =
						" SELECT ic_no_campo, cg_nombre_campo, cg_tipo_dato, ig_longitud "+
						" FROM comrel_visor_detalle WHERE ic_epo="+NO_EPO+
						" AND ic_producto_nafin=1"+
						" AND ic_no_campo BETWEEN 1 and 10 ORDER BY ic_no_campo";
				rs = con.queryDB(query);
				while(rs.next()) {
					noCampoDet.addElement(new Integer(rs.getInt(1)));
					nomCampoDet.addElement(rs.getString(2).trim());
					tipoDatoDet.addElement(rs.getString(3).trim());
					longDet.addElement(new Integer(rs.getInt(4)));
					noCamposDetalle++;
				}
				con.cierraStatement();

				// Si tiene detalles definidos y existe detalles capturados se procesan.
				String fechaDocumento="", icConsecutivo="";
				String cgCampo1="", cgCampo2="", cgCampo3="", cgCampo4="", cgCampo5="";
				String cgCampo6="", cgCampo7="", cgCampo8="", cgCampo9="", cgCampo10="";
				log.debug("ANTES DETALLES");
				log.debug("\n noCamposDetalle: "+noCamposDetalle);
				log.debug("\n hayRegDet: "+hayRegDet);
				if(noCamposDetalle>0 && hayRegDet>0){
					log.debug("Procesando Detalles.");
					int iNoCampo=0, iLongitud=0;
					String msgErrD="", sNombreCampo="", sTipoDato="", sValCampo="";
					// Obtenemos los Registros detalle de Liverpool de comtmp_doctosd_pub_l01.

					query = "";

					query = ((EpoEnlaceDetalle)epoEnl).getDetalles(String.valueOf(NO_EPO));

					rs1 = con.queryDB(query);
					while(rs1.next()) {
						noDocto = (rs1.getString(1)==null)?"":rs1.getString(1).trim();
						fechaDocumento = (rs1.getString(2)==null)?"":rs1.getString(2).trim();
						noMoneda = rs1.getInt(3);
						icConsecutivo = (rs1.getString(4)==null)?"":rs1.getString(4).trim();
						cgCampo1 = (noCampoDet.size()>=1)?(rs1.getString(5)==null?"":rs1.getString(5).trim()):"";
						cgCampo2 = (noCampoDet.size()>=2)?(rs1.getString(6)==null?"":rs1.getString(6).trim()):"";
						cgCampo3 = (noCampoDet.size()>=3)?(rs1.getString(7)==null?"":rs1.getString(7).trim()):"";
						cgCampo4 = (noCampoDet.size()>=4)?(rs1.getString(8)==null?"":rs1.getString(8).trim()):"";
						cgCampo5 = (noCampoDet.size()>=5)?(rs1.getString(9)==null?"":rs1.getString(9).trim()):"";
						cgCampo6 = (noCampoDet.size()>=6)?(rs1.getString(10)==null?"":rs1.getString(10).trim()):"";
						cgCampo7 = (noCampoDet.size()>=7)?(rs1.getString(11)==null?"":rs1.getString(11).trim()):"";
						cgCampo8 = (noCampoDet.size()>=8)?(rs1.getString(12)==null?"":rs1.getString(12).trim()):"";
						cgCampo9 = (noCampoDet.size()>=9)?(rs1.getString(13)==null?"":rs1.getString(13).trim()):"";
						cgCampo10 = (noCampoDet.size()>=10)?(rs1.getString(14)==null?"":rs1.getString(14).trim()):"";
						log.debug("\n >>> DETALLES");
						log.debug("noDocto: "+noDocto);
						log.debug("fechaDocumento: "+fechaDocumento);
						log.debug("noMoneda: "+noMoneda);
						log.debug("icConsecutivo: "+icConsecutivo);
						log.debug("cgCampo 1: "+cgCampo1);
						log.debug("cgCampo 2: "+cgCampo2);
						log.debug("cgCampo 3: "+cgCampo3);
						log.debug("cgCampo 4: "+cgCampo4);
						log.debug("cgCampo 5: "+cgCampo5);
						log.debug("cgCampo 6: "+cgCampo6);
						log.debug("cgCampo 7: "+cgCampo7);
						log.debug("cgCampo 8: "+cgCampo8);
						log.debug("cgCampo 9: "+cgCampo9);
						log.debug("cgCampo10: "+cgCampo10);
						log.debug("\n <<< DETALLES");
						lineadoctosd++;
						msgErrD = "ErrorDetalle "+lineadoctosd+", NoDocto "+noDocto+"-"+fechaDocumento+"-"+noMoneda+", sec. "+icConsecutivo;
						okRegDet = true;

						if(noDocto.equals("")) {
							errord.append(msgErrD+", el valor del n�mero docto debe de tener un valor.\n");
							noErrord.append("50\n");
							noDoctosd.append(noDocto+"\n");
							okDet = okRegDet = false;
						}
						if(!checaFecha(fechaDocumento)){
							errord.append(msgErrD+", la fecha de emisi�n del documento es incorrecta.\n");
							noErrord.append("51\n");
							noDoctosd.append(noDocto+"\n");
							okDet = okRegDet = false;
						}
						if(noMoneda!=1 && noMoneda!=54){
							errord.append(msgErrD+", la clave de la moneda no es v�lida.\n");
							noErrord.append("52\n");
							noDoctosd.append(noDocto+"\n");
							okDet = okRegDet = false;
						}
						if(icConsecutivo.equals("")) {
							errord.append(msgErrD+", el valor del consecutivo debe de tener un valor.\n");
							noErrord.append("53\n");
							noDoctosd.append(noDocto+"\n");
							okDet = okRegDet = false;
						}

						// El valor 1 al 10 va dependiendo de los campos din�micos que haya definido la Epo.
						for(int d=0; d<noCampoDet.size(); d++) {
							iNoCampo = Integer.parseInt(noCampoDet.get(d).toString());
							sNombreCampo = nomCampoDet.get(d).toString();
							sTipoDato = tipoDatoDet.get(d).toString();
							iLongitud = Integer.parseInt(longDet.get(d).toString());
							switch(iNoCampo) {
								case 1: sValCampo=cgCampo1; break;
								case 2: sValCampo=cgCampo2; break;
								case 3: sValCampo=cgCampo3; break;
								case 4: sValCampo=cgCampo4; break;
								case 5: sValCampo=cgCampo5; break;
								case 6: sValCampo=cgCampo6; break;
								case 7: sValCampo=cgCampo7; break;
								case 8: sValCampo=cgCampo8; break;
								case 9: sValCampo=cgCampo9; break;
								case 10: sValCampo=cgCampo10; break;
							}
							if(sTipoDato.equalsIgnoreCase("N")) {
								if (!esNumero(sValCampo)) {
									errord.append(msgErrD+", el Tipo de Dato del Campo "+sNombreCampo+" debe de ser Num�rico.\n");
									noErrord.append("54\n");
									noDoctosd.append(noDocto+"\n");
									okDet = okRegDet = false;
								}
							}
							if(iLongitud < sValCampo.length()) {
								errord.append(msgErrD+", la Longitud del Campo "+sNombreCampo+" es mayor a la definida por su Epo.\n");
								noErrord.append("55\n");
								noDoctosd.append(noDocto+"\n");
								okDet = okRegDet = false;
							}
						} //for

						if(okRegDet){
							// Para saber si Liverpool tiene uno documento y poderle asignar los detalles.
							if (rs1.getString("IC_DOCUMENTO") != null) {
								int actualiza = 0;
								query = " SELECT count(*) FROM com_documento_detalle "+
										" WHERE ic_documento="+rs1.getString("IC_DOCUMENTO")+
										" AND ic_docto_detalle="+icConsecutivo;
								rs2 = con.queryDB(query);
								if(rs2.next()) {
									actualiza = rs2.getInt(1);
								}
								rs2.close();
								con.cierraStatement();

								cgCampo1 = (cgCampo1.equals(""))?"null":"'"+cgCampo1+"'";
								cgCampo2 = (cgCampo2.equals(""))?"null":"'"+cgCampo2+"'";
								cgCampo3 = (cgCampo3.equals(""))?"null":"'"+cgCampo3+"'";
								cgCampo4 = (cgCampo4.equals(""))?"null":"'"+cgCampo4+"'";
								cgCampo5 = (cgCampo5.equals(""))?"null":"'"+cgCampo5+"'";
								cgCampo6 = (cgCampo6.equals(""))?"null":"'"+cgCampo6+"'";
								cgCampo7 = (cgCampo7.equals(""))?"null":"'"+cgCampo7+"'";
								cgCampo8 = (cgCampo8.equals(""))?"null":"'"+cgCampo8+"'";
								cgCampo9 = (cgCampo9.equals(""))?"null":"'"+cgCampo9+"'";
								cgCampo10 = (cgCampo10.equals(""))?"null":"'"+cgCampo10+"'";

								if(actualiza > 0) {	// Si ya existe el Detalle se Actualiza.
									query = "update com_documento_detalle set CG_CAMPO1="+cgCampo1+", CG_CAMPO2="+cgCampo2+","+
														" CG_CAMPO3="+cgCampo3+", CG_CAMPO4="+cgCampo4+", CG_CAMPO5="+cgCampo5+", CG_CAMPO6="+cgCampo6+","+
														" CG_CAMPO7="+cgCampo7+", CG_CAMPO8="+cgCampo8+", CG_CAMPO9="+cgCampo9+", CG_CAMPO10="+cgCampo10+
														" where ic_documento="+rs1.getString("IC_DOCUMENTO")+" and ic_docto_detalle="+icConsecutivo;
								} else {				// Si no se inserta un Nuevo Detalle.
									query = "insert into com_documento_detalle(ic_documento, ic_docto_detalle, cg_campo1, "+
											"cg_campo2, cg_campo3, cg_campo4, cg_campo5, cg_campo6, cg_campo7, cg_campo8, "+
											"cg_campo9, cg_campo10) values("+rs1.getString("IC_DOCUMENTO")+","+icConsecutivo+","+
											cgCampo1+","+cgCampo2+","+cgCampo3+","+cgCampo4+","+cgCampo5+","+cgCampo6+","+
											cgCampo7+","+cgCampo8+","+cgCampo9+","+cgCampo10+")";
								} //else
								//log.debug("QUERY DETALLE: \n\n"+query);

		                  try{
									log.debug("query: "+query);
									con.ejecutaSQL(query);
									iTotProcDetAlta++;
		                  }catch(Exception e){
									log.error("Error al Actualizar o Insertar en com_documento_detalle.\n");
									sblog.append("Error al Actualizar o Insertar en com_documento_detalle.\n");
		                  }

								// ACTUALIZAR COM_DOCUMENTO.CS_DETALLE = 'S'
								query = "update com_documento set cs_detalle='S' where ic_documento="+rs1.getString("IC_DOCUMENTO");
								try{
									log.debug("query: "+query);
									con.ejecutaSQL(query);
		                  }catch(Exception e){
									log.error("Error al Actualizar com_documento.cs_detalle = 'S'.\n");
									sblog.append("Error al Actualizar com_documento.cs_detalle = 'S'.\n");
		                  }
							} else {
								errord.append(msgErrD+", No existe un Documento para asociar el Detalle.\n");
								noErrord.append("56\n");
								noDoctosd.append(noDocto+"\n");
								okDet = okRegDet = false;
							}
						} // okRegDet
					} // while rs1
					rs1.close();
					con.cierraStatement();
				} //if noCamposDetalle

				//Si se cargaron detalles y no tiene definidos se Genera un Error General de Detalles.
				if(noCamposDetalle==0 && hayRegDet>0) {
					okDet = false;
					noDoctosd.append("*");
					noErrord.append("57\n");
					errord.append("Error de definici�n, no tiene campos detalles definidos para poder realizar la Carga de Detalles.");
				}
				con.terminaTransaccion(true);

				if(!okDet) {
					// Inserta los Errores de Detalle en com_doctos_err_pub_l01 para Liverpool.
					linea="";
					st=new StringTokenizer(noDoctosd.toString(),"\n");
					st1=new StringTokenizer(errord.toString(),"\n");
					st2=new StringTokenizer(noErrord.toString(),"\n");
					while(st.hasMoreTokens()) {
						iTotalErrorDet++;
						linea=st1.nextToken().trim();
						//linea=(linea.length()>120)?linea.substring(0,120):linea;
            System.out.println("MENSAJE LINEA == "+linea);
						ErroresEnl err = new ErroresEnl();
						err.setCadenaOriginal(cadenaOriginal);
						err.setIgNumeroDocto(st.nextToken());
						err.setIgNumeroError(st2.nextToken());
						err.setCgError(linea);

						query = epoEnl.getInsertaErrores(err);

						try{
							con.ejecutaSQL(query);
						}catch(Exception e){
							log.error("Error al insertar en "+epoEnl.getTablaErrores()+".\n");
							e.printStackTrace();
							sblog.append("Error al insertar en "+epoEnl.getTablaErrores()+".\n");
						}
					}
				}
				errord.delete(0,errord.length());
				noErrord.delete(0,noErrord.length());
				noDoctosd.delete(0,noDoctosd.length());

				if(hayreg==0 && hayRegDet==0)
					csestatus="03";
				else // Con Errores en cualquier Proceso 02 y sin errores, es decir, todo bien con estatus 01.
					csestatus=(!ok || !okDet)?"02":"01";

				acuEnl.setInTotalProcDetalle(lineadoctosd);
			}

			/*******************************************************************
			* termina codigo para epos con documentos detalle
			**********************************************************************/

			if(!(epoEnl instanceof EpoEnlaceDetalle)){
				csestatus=(!ok)?"02":"01";
				if(hayreg==0){
					csestatus="03";
				}
			}

			///////********************* GENERAL *************************////////
			// Se actualizan las cifras.
			acuEnl.setCsEstatus(csestatus);

			if(!(epoEnl instanceof EpoEnlaceAll) || csestatus.equals("01")){
				query =
					" UPDATE com_acuse1"   +
					"    SET in_total_docto_mn = "+acuEnl.getInTotalAcepMn()+","   +
					"        fn_total_monto_mn = "+acuEnl.getFnTotalMontoAcepMn()+","   +
					"        in_total_docto_dl = "+acuEnl.getInTotalAcepDl()+","   +
					"        fn_total_monto_dl = "+acuEnl.getFnTotalMontoDl()+" "   +
					"  WHERE cc_acuse = '"+acuse+"'"  ;
				try{
					con.ejecutaSQL(query);
				}catch(Exception e){
					sblog.append("Error al actualizar en com_acuse1.\n");
				}
				query = epoEnl.getUpdateAcuse(acuEnl);
				try{
					con.ejecutaSQL(query);
				}catch(Exception e){
					sblog.append("Error al actualizar en "+epoEnl.getTablaAcuse()+".\n");
				}
			}else{
				if(epoEnl instanceof EpoEnlaceAll){
					query = "update "+epoEnl.getTablaAcuse()+" set CS_ESTATUS='"+csestatus+"' where CC_ACUSE='"+acuse+"'";
					try{
						con.ejecutaSQL(query);
					}catch(Exception e){
						sblog.append("Error al actualizar en "+epoEnl.getTablaAcuse()+".\n");
					}
				}
			}

			///////********************* GENERAL *************************////////
			// Se borran los documentos existentes.
			if(epoEnl instanceof EpoCentralizada){
				query =
						" DELETE "+epoEnl.getTablaDocumentos() +
						"  WHERE cc_acuse = '"+acuEnl.getCcAcuse()+"' "  ;
			}else{
				query =
						" DELETE "+epoEnl.getTablaDocumentos() +
						epoEnl.getCondicionQuery();
			}
			try{
				con.ejecutaSQL(query);
			}catch(Exception e){
				sblog.append("Error al borrar "+epoEnl.getTablaDocumentos()+".\n");
			}

			if(epoEnl instanceof EpoEnlaceDetalle) {

				query = "DELETE "+tabla_detalles+" "+epoEnl.getCondicionQuery();
				try{
					con.ejecutaSQL(query);
				}catch(Exception e){
					sblog.append("Error al borrar comtmp_doctosd_pub_l01.\n");
				}
			}


			//if(!con.ejecutaSQL(query)) { log.debug("Error al borrar "+epoEnl.getTablaErrores()+".\n"); }

			//Mandamos la salida al log.
			sblog.append(" Fecha de Ejecuci�n:\t\t\t"+ new SimpleDateFormat("yyyy/MM/dd").format(new java.util.Date())+"\n");
			sblog.append(" Hora de Ejecuci�n:\t\t\t"+ new SimpleDateFormat("HH:mm:ss").format(new java.util.Date())+"\n");
			sblog.append(" Registros dados de alta:\t\t"+iTotalAlta+"\n");
			sblog.append(" Total de Errores generados:\t\t"+iTotalError+"\n");
			sblog.append(" Estatus: proceso generado con: "+((ok)?"exito":"error")+"\n");
			sblog.append(" Clase Acuse = "+acuEnl.toString()+"\n");
			log.debug(sblog.toString());

      log.info("NO_EPO ========================== "+NO_EPO);
      log.info("acuEnl.getCcAcuse() ========================== "+acuEnl.getCcAcuse());

      if(publicarDocDuplicados(String.valueOf(NO_EPO))){
        enviaCorreoDoctosDuplicados(con, String.valueOf(NO_EPO),acuEnl.getCcAcuse());
      }
		
			if(!esEnlaceWS) {
				con.terminaTransaccion(true);
				//Crea y Guarda el CSV de respuesta 
				CrearArchivosAcuses  arc = new CrearArchivosAcuses();
				arc.setAcuse( acuEnl.getCcAcuse() );
				arc.setUsuario(noUsuario );
				arc.setVersion("ENLACES"); 
							
				try{
					arc.setTabla(epoEnl.getTablaErrores());
					arc.setIc_epo(String.valueOf(NO_EPO) );            
					arc.guardarArchivo();  
							
				} catch (Exception exception) {
					  
					java.io.StringWriter outSW = new java.io.StringWriter();
					exception.printStackTrace(new java.io.PrintWriter(outSW));
					String stackTrace = outSW.toString();
					arc.setDesError(stackTrace);  
					arc.guardaBitacoraArch();  					
				} 
			}
			
		} // End if (bProcesarDoctos)
	} catch(SQLException sqle) {
		sqle.printStackTrace();
		log.error(sqle.getMessage());
		throw sqle;
	} catch(Exception e) {
		e.printStackTrace();
		log.error(e.getMessage());
		// Si es publicacion via WS... si debe regresar la excepcion para que
		//el proceso pueda notificar que hubo un error.
		if (esEnlaceWS) {
			throw new AppException("Error Inesperado ", e);
		}
	} finally{
		con.terminaTransaccion(true);
		con.cierraConexionDB();
		log.info("CargaDocumentoBean::proceso(S)************");
	}
	return sblog;
}


 public StringBuffer procesoDocumentos(String EPO, EpoEnlOperados epoEnlOper) {
	 return procesoDocumentos(EPO, epoEnlOper, "");
 }

 /**
  * M�todo para obtener documentos operados, por proceso de enlace.
  * @param EPO clave de la epo
  * @param epoEnlOper clase que implementa EpoEnlOperados, contiene las
  * consultas necesarias para obtener la informaci�n requerida.
  * @param status Estatus
  * 	O Operados
  *   B Baja, Descuento Fisico y Pagado Anticipado
  *   V Vencidos
  *
  */
 public StringBuffer procesoDocumentos(String EPO, EpoEnlOperados epoEnlOper, String status) {
	log.info("CargaDocumentoBean::procesoDocumentos(E)************\n");
	AccesoDB con = new AccesoDB();
	String query = "", queryOper = "", queryBaja = "", queryVenc = "";
  String sDigitoIdentificadorFlag = "", sPubEPOPEFFlagVal = "";
	StringBuffer sblog = new StringBuffer();
	
	boolean bOk = true;
	PreparedStatement ps = null;
  ResultSet rs=null;
  Hashtable alParamEPO = new Hashtable();
	try {
		con.conexionDB();
		int claveEpo = Integer.parseInt(EPO);

      /*MODIFICACIONES PARA DETERMINAR SI SE TRATA DE UNA EPO PEF Y LOS PARAMETROS ACTIVOS*/

      try {
        ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
        alParamEPO = BeanParamDscto.getParametrosEPO(EPO, 1);
        sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
        sDigitoIdentificadorFlag    = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
      } catch (NafinException ne){
        sblog.append("Error en la obtencion de los parametros por epo "+ne.getMsgError()+".\n");
      }



		// Operados.

		if("O".equals(status) || "".equals(status)){
			sblog.append("Ejecutando proceso Operados \n");
			try {
				con.ejecutaSQL(epoEnlOper.getBorraDoctosOpe());
        queryOper = epoEnlOper.getInsertaDoctosOpe() + epoEnlOper.getDoctosOperados(alParamEPO);
			} catch (SQLException sqle){
				sblog.append("Error al borrar "+epoEnlOper.getBorraDoctosOpe()+".\n");
				throw sqle;
			}

			try {
				con.ejecutaSQL(queryOper);
			} catch (SQLException sqle){
				sblog.append("Error al Insertar "+queryOper+".\n");
				throw sqle;
			}
		}
		// Baja, Descuento Fisico y Pagado Anticipado.
		if("B".equals(status) || "".equals(status)){
			sblog.append("Ejecutando proceso Baja, Descuento Fisico y Pagado Anticipado \n");
			try {
				con.ejecutaSQL(epoEnlOper.getBorraDoctosBaja());
				queryBaja = epoEnlOper.getInsertaDoctosOpe() + epoEnlOper.getDoctosEliminacion(alParamEPO);
			} catch (SQLException sqle){
				sblog.append("Error al borrar "+epoEnlOper.getBorraDoctosBaja()+".\n");
				throw sqle;
			}
			try {
				con.ejecutaSQL(queryBaja);
			} catch (SQLException sqle){
				sblog.append("Error al Insertar "+queryBaja+".\n");
				throw sqle;
			}
		}
		// Vencidos
		if("V".equals(status) || "".equals(status)){
			sblog.append("Ejecutando proceso Vencidos sin Operar \n");
			try {
				con.ejecutaSQL(epoEnlOper.getBorraDoctosVenc());
				query = "select NVL((select ig_dias_minimo from comrel_producto_epo where ic_epo=? and ic_producto_nafin = ?), "+
						" (select in_dias_minimo from comcat_producto_nafin where ic_producto_nafin = ?)) as in_dias_minimo, 'CargaDocumentoBean.procesoDocumentos()' "+
						" from dual";
				ps = con.queryPrecompilado(query);
				ps.setInt(1,claveEpo);
				ps.setInt(2,1);
				ps.setInt(3,1);
				rs =  ps.executeQuery();
				int diasMinimos=0;
				if(rs.next())
					diasMinimos = rs.getInt("in_dias_minimo");
				rs.close();

				queryVenc = epoEnlOper.getInsertaDoctosOpe() + epoEnlOper.getDoctosVencidoSinOperar(alParamEPO, diasMinimos);
				log.debug("queryVenc: "+queryVenc);
			} catch (SQLException sqle){
				sblog.append("Error al borrar "+epoEnlOper.getBorraDoctosVenc()+".\n");
				throw sqle;
			} finally {
				if (ps != null)
					ps.close();
			}
			try {
				con.ejecutaSQL(queryVenc);
			} catch (SQLException sqle){
				sblog.append("Error al Insertar "+queryVenc+".\n");
				throw sqle;
			}
		}
		sblog.append("Proceso llevado a cabo con exito. Epo " + EPO + "\n");
		//

//	} catch(SQLException sqle) {
//		bOk = false;
//		sblog.append(sqle.getMessage());
//		sqle.printStackTrace();
	} catch(Exception e) {
		bOk = false;
		sblog.append(e.getMessage());
		log.error(sblog.toString(), e);
		throw new RuntimeException("CargaDocumentoBean::procesoDocumentos(Error): " + e.getMessage());
	} finally {
		con.terminaTransaccion(bOk);
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		log.debug(sblog.toString());
		log.info("CargaDocumentoBean::procesoDocumentos(S)************\n");
	}
	return sblog;
  }




 private static boolean esNumero(String numero) {
	boolean isNum = false;
		try {
			new BigDecimal(numero.trim());
			isNum = true;
	        }
		catch(NumberFormatException nfe) { isNum = false; }
	return isNum;
 }

 /*private static String dobleComitas(String frase) {
		StringBuffer sb=new StringBuffer(frase);
		Vector v=new Vector();
		for(int i=0; i<sb.length(); i++) {
	 		if(sb.substring(i,i+1).equals("'")) {
				v.addElement(Integer.toString(i));
			}
		}
		int indx=0;
		for(int h=v.size(); h>0; h--) {
			indx=Integer.parseInt(v.get(h-1).toString());
			sb.replace(indx, indx+1, "''");
		}
	return sb.toString().trim();
 }*/

 /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IHJ*/
 public String getResultadoErrorCMDProc ( int NumProceso)	throws NafinException {
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	log.info("CargaDocumentoBean::getResultadoErrorCMDProc (E)");
	String strQuery="";
	StringBuffer strBufResult=new StringBuffer("");
	ResultSet rs=null;
	try{
		con.conexionDB();
		strQuery=
			" SELECT cg_errores_ctl"   +
			"   FROM bit_carga_doctodet"   +
			"  WHERE ic_carga_doctodet = "+NumProceso+" "  ;
//log.debug("\n strQuery: "+strQuery);
		rs = con.queryDB(strQuery);
		if (rs.next())  {
			strBufResult.append((rs.getString(1)==null)?"":rs.getString(1)+"\n");
		}
		rs.close(); //conn.cierraStatement();
	} catch (Exception e) {
		log.error("Error en getResultadoErrorCMDProc();"+e.toString());
		throw new NafinException("DSCT0079");
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	log.info("CargaDocumentoBean::getResultadoErrorCMDProc (S)");
	return strBufResult.toString();
} // Fin de getResultadoErrorCMD()

public Vector getTotalesErrorCMD ( int NumProceso) throws NafinException {
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	Vector renglones = new Vector();
	Vector columnas = null;
	log.info("CargaDocumentoBean::getTotalesErrorCMD (E)");
	String strQuery="";
	ResultSet rs=null;
	try{
		con.conexionDB();
		strQuery=
			" SELECT ic_moneda, COUNT (1)"   +
			"   FROM comtmp_carga_doctodet"   +
			"  WHERE ic_carga_doctodet = "+NumProceso+" "+
			"    AND cg_estatus <> 'OK'"   +
			"  GROUP BY ic_moneda";
		log.debug("\n getTotalesErrorCMD::strQuery: "+strQuery);
		rs = con.queryDB(strQuery);
		while(rs.next())  {
			columnas = new Vector();
			String rs_moneda = (rs.getString(1)==null)?"":rs.getString(1);
			String rs_cuantos = (rs.getString(2)==null)?"":rs.getString(2);
			columnas.add(rs_moneda);
			columnas.add(rs_cuantos);
			renglones.add(columnas);
		}
		rs.close();// conn.cierraStatement();
	} catch (Exception e) {
		log.error("Error en getTotalesErrorCMD();"+e.toString());
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	log.info("CargaDocumentoBean::getTotalesErrorCMD (S)");
	return renglones;
} // Fin de getTotalesErrorCMD()

public Vector getTotalesValidosCMD ( int NumProceso) throws NafinException{
//****************************************************************************************
	AccesoDB con = new AccesoDB();
//****************************************************************************************
	Vector renglones = new Vector();
	Vector columnas = null;
	log.info("CargaDocumentoBean::getTotalesValidosCMD (E)");
	String strQuery="";
	ResultSet rs=null;
	try {
		con.conexionDB();
		strQuery=
			" SELECT ic_moneda, ig_numero_docto, COUNT (1)"   +
			"   FROM comtmp_carga_doctodet"   +
			"  WHERE ic_carga_doctodet = "+NumProceso+" "   +
			"    AND cg_estatus = 'OK'"   +
			"  GROUP BY ic_moneda,  ig_numero_docto"   +
			"  ORDER BY 2"  ;
		log.debug("\n getTotalesErrorCMD::strQuery: "+strQuery);
		rs = con.queryDB(strQuery);
		while(rs.next()){
			columnas = new Vector();
			String rs_moneda = (rs.getString(1)==null)?"":rs.getString(1);
			String rs_numero_docto = (rs.getString(2)==null)?"":rs.getString(2);
			String rs_cuantos = (rs.getString(3)==null)?"":rs.getString(3);
			columnas.add(rs_moneda);
			columnas.add(rs_numero_docto);
			columnas.add(rs_cuantos);
			renglones.add(columnas);
		}
		rs.close();// conn.cierraStatement();
	} catch (Exception e) {
		log.error("Error en getTotalesValidosCMD();"+e.toString());
	}
//****************************************************************************************
	finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
//****************************************************************************************
	log.info("CargaDocumentoBean::getTotalesValidosCMD (S)");
	return renglones;
} // Fin de getTotalesValidosCMD()

public void subsidioAutomaticoEpo(String strFechaVenc, String strFechaIni)
		throws NafinException {
	log.info(" CargaDocumentoEJB::subsidioAutomaticoEpo(E)");
	AccesoDB 			con 			= new AccesoDB();
	String				condicion		= "";
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
    Acuse				acuse			= null;

	try {
		con.conexionDB();

		setSubsidioHistoricoEpo(con);

	 	if("".equals(strFechaIni)) {
			condicion =
				"    AND sol.df_fecha_solicitud >= TRUNC (SYSDATE) "+
				"    AND sol.df_fecha_solicitud < TRUNC (SYSDATE+1) "+
				"    AND doc.ic_estatus_docto = 4";
		} else {
			condicion =
				"    AND sol.df_fecha_solicitud >= TRUNC (TO_DATE(?,'dd/mm/yyyy')) "+
				"    AND doc.ic_estatus_docto in (4, 11)";
		}
		if(!"".equals(strFechaVenc)) {
			condicion +=
				"    AND sol.df_fecha_solicitud < TRUNC (TO_DATE(?,'dd/mm/yyyy')+1) ";

		}

		qrySentencia =
			" SELECT   /*+ordered INDEX(s in_com_solicitud_13_nuk) */"   +
			"           doc.ic_documento, doc.ig_numero_docto, 'dscto_aut' AS ic_usuario,"   +
			"           epo.cg_razon_social AS nomepo, doc.ic_epo, doc.ic_pyme, doc.ic_moneda,"   +
			"           doc.fn_monto, doc.fn_monto_dscto, sel.in_importe_interes,"   +
			"           cif.ig_tipo_piso, sub.fg_puntos tasa,"   +
			"           (TRUNC(doc.df_fecha_venc) - TRUNC (sel.df_fecha_seleccion)) plazo,"   +
			"           sub.fg_limite, sub.ic_epo_dest, 'CargaDocumentoBean::subsidioAutomaticoEpo()' origen"   +
			"     FROM com_solicitud sol,"   +
			"          com_docto_seleccionado sel,"   +
			"          com_documento doc,"   +
			"          comrel_pyme_epo cpe,"   +
			"          comcat_epo epo,"   +
			"          comcat_if cif,"   +
			"          comrel_subsidio_epo sub"   +
			"    WHERE sol.ic_documento = sel.ic_documento"   +
			"      AND doc.ic_documento = sel.ic_documento"   +
			"      AND doc.ic_epo = epo.ic_epo"   +
			"      AND doc.ic_pyme = cpe.ic_pyme"   +
			"      AND doc.ic_epo = cpe.ic_epo"   +
			"      AND sel.ic_if = cif.ic_if"   +
			"      AND sub.ic_pyme = cpe.ic_pyme"   +
			"      AND sub.ic_epo = cpe.ic_epo"   +
			condicion+
			"      AND sol.df_fecha_solicitud >= sub.df_inicio_apoyo"   +
			"      AND sub.cs_activo = 'S'"   +
			"      AND sub.ic_producto_nafin = 1"   +
			"      AND doc.cs_subsidio IS NULL"   +
			" ORDER BY doc.ic_epo, sub.ic_epo_dest, doc.ic_pyme, doc.ic_documento"  ;
		//log.debug("qrySentencia: "+qrySentencia);

		ps = con.queryPrecompilado(qrySentencia);
		int cont = 0;
	 	if(!"".equals(strFechaIni)) {
	 		cont++;ps.setString(cont, strFechaIni);
	 	}
		if(!"".equals(strFechaVenc)) {
	 		cont++;ps.setString(cont, strFechaVenc);
		}
		rs = ps.executeQuery();
		int 	doctosinsert 	= 0;
		int 	total_docs_mn 	= 0;
		double 	total_monto_mn 	= 0;
		int 	total_docs_dl 	= 0;
		double 	total_monto_dl 	= 0;
		while(rs.next()) {
			String rs_documento		= rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
			String rs_numdocumento	= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
			String rs_usuario		= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
			String rs_nomepo		= rs.getString("nomepo")==null?"":rs.getString("nomepo");
			String rs_epo			= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
			String rs_pyme			= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
			String rs_moneda		= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
			String rs_monto			= rs.getString("fn_monto")==null?"":rs.getString("fn_monto");
			String rs_monto_dscto	= rs.getString("fn_monto_dscto")==null?"":rs.getString("fn_monto_dscto");
			String rs_montointeres	= rs.getString("in_importe_interes")==null?"":rs.getString("in_importe_interes");
			String rs_tipo_piso		= rs.getString("ig_tipo_piso")==null?"":rs.getString("ig_tipo_piso");
			String rs_tasa			= rs.getString("tasa")==null?"":rs.getString("tasa");
			String rs_plazo			= rs.getString("plazo")==null?"":rs.getString("plazo");
			String rs_lim_subsidio	= rs.getString("fg_limite")==null?"":rs.getString("fg_limite");
			String rs_epo_destino	= rs.getString("ic_epo_dest")==null?"":rs.getString("ic_epo_dest");

			double subsidioDoc	= 0;
			double interesRed	= 0;
			double tasaSubsidio	= Double.parseDouble(rs_tasa)/100;
			BigDecimal interesSub = new BigDecimal(0.0);

			if (rs_tipo_piso.equals("1")) {
				interesSub = new BigDecimal(rs_monto_dscto).multiply(new BigDecimal(tasaSubsidio));
				interesSub = interesSub.divide(new BigDecimal(360), 2);
				interesRed = (double)Math.round(interesSub.doubleValue()*100)/100;
				interesRed = interesRed * Double.parseDouble(rs_plazo);
				interesRed = (double)Math.round(interesRed*100)/100;
			} else {
				interesSub = new BigDecimal(rs_monto_dscto).multiply(new BigDecimal(tasaSubsidio));
				interesSub = interesSub.multiply(new BigDecimal( rs_plazo ));
				interesSub = interesSub.divide(new BigDecimal(360), 2);
				interesRed = (double)Math.round(interesSub.doubleValue()*100)/100;
			}

/*			interesSub = new BigDecimal(rs_monto_dscto).multiply(new BigDecimal(tasaSubsidio));
			interesSub = interesSub.divide(new BigDecimal(360.00), 2);
			interesRed = interesSub.doubleValue() * Double.parseDouble(rs_plazo);
			interesRed = (double)Math.round(interesRed*100)/100;*/

			subsidioDoc = interesRed;

			double subAcum = getSubsidioEpoAcum(rs_epo, rs_epo_destino, rs_pyme, con);
			subAcum += subsidioDoc;
			if(subAcum<Double.parseDouble(rs_lim_subsidio)) {
				int camposAdic = getNumCamposAdic(rs_epo_destino, con);
				if(camposAdic>=5) {
					if(doctosinsert==0) {
						acuse = new Acuse(Acuse.ACUSE_EPO, "1");
						creaAcuseSubsidioAuto(acuse.toString(), rs_usuario, con);
					}
					String fechaVenc;
					if("".equals(strFechaVenc))
						fechaVenc= getFechaVencNoNeg("", con);
					else
						fechaVenc= strFechaVenc;
					String igConsec = getNumConsecDoc(rs_epo_destino, con);
					setDocumentoNoNegociable(rs_pyme, rs_epo_destino, acuse.toString(), igConsec, fechaVenc, rs_moneda, String.valueOf(subsidioDoc), rs_nomepo, rs_tasa, rs_numdocumento, rs_monto, rs_montointeres, con);
					setSubsidioAcum(rs_epo, rs_epo_destino, rs_pyme, subAcum, con);
					doctosinsert++;
					if("1".equals(rs_moneda)) {
						total_docs_mn	++;
						total_monto_mn	+= Double.parseDouble(rs_monto);
					} else if("54".equals(rs_moneda)) {
						total_docs_dl	++;
						total_monto_dl	+= Double.parseDouble(rs_monto);
					}
				}//if(camposAdic==5)
			}//if(subAcum<Double.parseDouble(rs_lim_subsidio))
			setSubsidioAplicado(rs_documento, rs_epo_destino, con);
		}//while(rs.next())
		if(doctosinsert>0)
			setAcuseSubsidioAuto(acuse.toString(),total_docs_mn,total_monto_mn,total_docs_dl,total_monto_dl,con);
		rs.close();
		if(ps!=null) ps.close();
		con.terminaTransaccion(true);
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::subsidioAutomaticoEpo(Exception): "+e);
		e.printStackTrace();
		con.terminaTransaccion(false);
		throw new NafinException("SIST0001");
	} finally {
		if (con.hayConexionAbierta() == true)
			con.cierraConexionDB();
		log.info(" CargaDocumentoEJB::subsidioAutomaticoEpo(S)");
	}
}//public void subsidioAutomaticoEpo()

private void setSubsidioHistoricoEpo(AccesoDB con)
			throws NafinException {
	log.info(" CargaDocumentoEJB::setSubsidioHistoricoEpo(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	try {
		qrySentencia =
			" SELECT ic_producto_nafin, ic_epo, ic_epo_dest, ic_pyme, to_char(sysdate, 'yyyy') actual"   +
			"   FROM comrel_subsidio_epo"   +
			"  WHERE df_vigencia is null"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		ps.clearParameters();
		while(rs.next()) {
			String rs_producto_nafin 	= rs.getString(1)==null?"":rs.getString(1);
			String rs_epo				= rs.getString(2)==null?"":rs.getString(2);
			String rs_epo_dest			= rs.getString(3)==null?"":rs.getString(3);
			String rs_pyme 				= rs.getString(4)==null?"":rs.getString(4);
			String rs_actual 			= rs.getString(5)==null?"":rs.getString(5);
			qrySentencia =
				" UPDATE comrel_subsidio_epo"   +
				"    SET df_vigencia = TO_DATE (?, 'dd/mm/yyyy')"   +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_epo = ?"   +
				"    AND ic_epo_dest = ?"   +
				"    AND ic_pyme = ?"  ;
			//log.debug("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, "31/12/"+rs_actual);
			ps.setInt(2, Integer.parseInt(rs_producto_nafin));
			ps.setInt(3, Integer.parseInt(rs_epo));
			ps.setInt(4, Integer.parseInt(rs_epo_dest));
			ps.setInt(5, Integer.parseInt(rs_pyme));
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		}//while(rs.next())
		ps.close();
		rs.close();

		qrySentencia =
			" SELECT ic_producto_nafin, ic_epo, ic_epo_dest, ic_pyme, fg_acumulado,"   +
			"        TO_CHAR (df_vigencia, 'YYYY'), TO_CHAR (SYSDATE, 'yyyy')"   +
			"   FROM comrel_subsidio_epo"   +
			"  WHERE df_vigencia < TRUNC (SYSDATE)"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		ps.clearParameters();
		while(rs.next()) {
			String rs_producto_nafin	= (rs.getString(1)==null)?"":rs.getString(1);
			String rs_epo				= (rs.getString(2)==null)?"":rs.getString(2);
			String rs_epo_dest			= (rs.getString(3)==null)?"":rs.getString(3);
			String rs_pyme				= (rs.getString(4)==null)?"":rs.getString(4);
			String rs_acumulado 		= (rs.getString(5)==null)?"0":rs.getString(5);
			String rs_vigencia 			= (rs.getString(6)==null)?"":rs.getString(6);
			String rs_anyo_act 			= (rs.getString(7)==null)?"":rs.getString(7);

			qrySentencia =
				" INSERT INTO comhis_subsidio_epo"   +
				"             (ic_producto_nafin, ic_epo, ic_epo_dest, ic_pyme, fg_acumulado, df_subsidio)"   +
				"      VALUES (?, ?, ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'))"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(rs_producto_nafin));
			ps.setInt(2, Integer.parseInt(rs_epo));
			ps.setInt(3, Integer.parseInt(rs_epo_dest));
			ps.setInt(4, Integer.parseInt(rs_pyme));
			ps.setDouble(5, Double.parseDouble(rs_acumulado));
			ps.setString(6, "01/01/"+rs_vigencia);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

			qrySentencia =
				" UPDATE comrel_subsidio_epo"   +
				"    SET df_vigencia = TO_DATE (?, 'dd/mm/yyyy'),"   +
				"        fg_acumulado = NULL"   +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_epo = ?"   +
				"    AND ic_epo_dest = ?"   +
				"    AND ic_pyme = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, "31/12/"+rs_anyo_act);
			ps.setInt(2, Integer.parseInt(rs_producto_nafin));
			ps.setInt(3, Integer.parseInt(rs_epo));
			ps.setInt(4, Integer.parseInt(rs_epo_dest));
			ps.setInt(5, Integer.parseInt(rs_pyme));
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		}//while(rs.next())
		ps.close();
		rs.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setSubsidioHistoricoEpo(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setSubsidioHistoricoEpo(S)");
	}
}//setSubsidioHistoricoEpo

private double getSubsidioEpoAcum(String ic_epo, String ic_epo_dest, String ic_pyme, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::getSubsidioEpoAcum(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	double				subsidioacum	= 0;
	try {
		qrySentencia =
			" SELECT fg_acumulado"   +
			"   FROM comrel_subsidio_epo"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND ic_epo = ?"   +
			"    AND ic_epo_dest = ?"   +
			"    AND ic_pyme = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setInt(2,Integer.parseInt(ic_epo_dest));
		ps.setInt(3,Integer.parseInt(ic_pyme));
		rs = ps.executeQuery();
		if(rs.next())
			subsidioacum = rs.getDouble(1);
		rs.close();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::getSubsidioEpoAcum(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getSubsidioEpoAcum(S)");
	}
	return subsidioacum;
}//getSubsidioEpoAcum

private void setSubsidioAcum(String ic_epo, String ic_epo_destino, String ic_pyme, double subAcum, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::setSubsidioAcum(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	try {
		qrySentencia =
			" UPDATE comrel_subsidio_epo"   +
			"    SET fg_acumulado = ?"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND ic_epo = ?"  +
			"    AND ic_epo_dest = ?"  +
			"    AND ic_pyme = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setDouble(1,subAcum);
		ps.setInt(2,Integer.parseInt(ic_epo));
		ps.setInt(3,Integer.parseInt(ic_epo_destino));
		ps.setInt(4,Integer.parseInt(ic_pyme));
		ps.executeUpdate();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setSubsidioAcum(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setSubsidioAcum(S)");
	}
}

public String subsidioAutomatico(String ic_epo, String strFechaVenc, String strFechaIni)
		throws NafinException {
	String contenidoArchivo = "";
	try {
		contenidoArchivo = subsidioAutomatico(ic_epo, strFechaVenc, strFechaIni, true);
	} catch(Exception e) {
		e.printStackTrace();
		throw new NafinException("SIST0001");
	}
	return contenidoArchivo;
}

public String subsidioAutomatico(String ic_epo, String strFechaVenc, String strFechaIni, boolean bSave)
		throws NafinException {
	log.info(" CargaDocumentoEJB::subsidioAutomatico(E)");
	AccesoDB 			con 			= new AccesoDB();
	String				condicion		= "";
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
    Acuse				acuse			= null;
    boolean				bOK				= true;
	String 				contenidoArchivo= "";

	try {
		con.conexionDB();

		if(bSave) {
			setSubsidioHistorico(con);
		}

		qrySentencia =
			" SELECT ROUND (fn_valor_compra, 4)"   +
			"   FROM com_tipo_cambio"   +
			"  WHERE ic_moneda = 54"   +
			"    AND dc_fecha = (SELECT MAX (tc.dc_fecha)"   +
			"                      FROM com_tipo_cambio tc"   +
			"                     WHERE tc.ic_moneda = 54)"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		String tipo_cambio = "1";
		while(rs.next()) {
			tipo_cambio = rs.getString(1)==null?"1":rs.getString(1);
		}
		rs.close();
		ps.close();

	 	if("".equals(strFechaIni)) {
			condicion =
				"    AND sol.df_fecha_solicitud >= TRUNC (SYSDATE) "+
				"    AND sol.df_fecha_solicitud < TRUNC (SYSDATE+1) "+
				"    AND doc.ic_estatus_docto = 4";
		} else {
			condicion =
				"    AND sol.df_fecha_solicitud >= TRUNC (TO_DATE(?,'dd/mm/yyyy')) "+
				"    AND doc.ic_estatus_docto in (4, 11)";
		}

		if(!"".equals(strFechaVenc)) {
			condicion +=
				"    AND sol.df_fecha_solicitud < TRUNC (TO_DATE(?,'dd/mm/yyyy')+1) ";
		}

		if(bSave) {
			condicion +=
				"    AND sub.cs_activo = 'S'";
		}

		qrySentencia =
			" SELECT   /*+index(doc) index(sub)*/"   +
			"        doc.ic_documento, "   +
			"        doc.ig_numero_docto, "   +
			"        'dscto_aut' ic_usuario,"   +
			"        epo.cg_razon_social nomepo,"   +
			"        doc.ic_epo, "   +
			"        doc.ic_pyme, "   +
			"        doc.ic_moneda,"   +
			"        doc.fn_monto,"   +
			"        DECODE (doc.ic_moneda, 54, doc.fn_monto * ?, doc.fn_monto) fn_monto_tc,"   +
			"        sel.in_importe_interes,"   +
			"        ROUND ("   +
			"         DECODE (doc.ic_moneda, 54, sel.in_importe_interes * ?, sel.in_importe_interes), 2"   +
			"        ) in_importe_interes_tc,"   +
			"        DECODE ("   +
			"         doc.ic_moneda, 54, sel.in_importe_interes * ?, sel.in_importe_interes"   +
			"        ) * (sub.fg_porcentaje_descuento / 100) subsidio,"   +
			"        sub.fg_limite, "   +
			"        sub.fg_porcentaje_descuento,"   +
			"        pym.cg_razon_social nombrePyme,"   +
			"        TO_CHAR(sol.df_fecha_solicitud, 'dd/mm/yyyy') fechaSol"   +
			"   FROM com_documento doc,"   +
			"        com_docto_seleccionado sel,"   +
			"        com_solicitud sol,"   +
			"        comcat_pyme pym,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comcat_epo epo,"   +
			"        comrel_subsidio_producto sub"   +
			"  WHERE doc.ic_documento = sel.ic_documento"   +
			"    AND sel.ic_documento = sol.ic_documento"   +
			"    AND doc.ic_epo = epo.ic_epo"   +
			"    AND doc.ic_pyme = cpe.ic_pyme"   +
			"    AND doc.ic_epo = cpe.ic_epo"   +
			"    AND pym.ic_pyme = cpe.ic_pyme"   +
			"    AND sub.ic_pyme = cpe.ic_pyme"   +
			"    AND sol.df_fecha_solicitud >= sub.df_inicio_apoyo"   +
			condicion+
			"    AND sub.ic_producto_nafin = 1"   +
			"    AND doc.cs_subsidio IS NULL"   +
			"  ORDER BY doc.ic_epo, doc.ic_pyme, doc.ic_documento"  ;
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("tipo_cambio: "+tipo_cambio);
		log.debug("strFechaIni: "+strFechaIni);
		log.debug("tipo_cambio: "+tipo_cambio);
		ps = con.queryPrecompilado(qrySentencia);
		int cont = 0;
	 	cont++;ps.setDouble(cont, Double.parseDouble(tipo_cambio));
	 	cont++;ps.setDouble(cont, Double.parseDouble(tipo_cambio));
	 	cont++;ps.setDouble(cont, Double.parseDouble(tipo_cambio));
	 	if(!"".equals(strFechaIni)) {
	 		cont++;ps.setString(cont, strFechaIni);
	 	}
		if(!"".equals(strFechaVenc)) {
	 		cont++;ps.setString(cont, strFechaVenc);
	 	}
		rs = ps.executeQuery();
		int 	doctosinsert 	= 0;
		int 	total_docs_mn 	= 0;
		double 	total_monto_mn 	= 0;
		int 	total_docs_dl 	= 0;
		double 	total_monto_dl 	= 0;
		String 	auxPyme 		= "";
		double 	subAcum 		= 0;

		while(rs.next()) {
			String rs_documento			= rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
			String rs_numdocumento		= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
			String rs_usuario			= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
			String rs_nomepo			= rs.getString("nomepo")==null?"":rs.getString("nomepo");
			String rs_pyme				= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
			String rs_moneda			= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
			String rs_monto_tc			= rs.getString("fn_monto_tc")==null?"":rs.getString("fn_monto_tc");
			String rs_montointeres_tc	= rs.getString("in_importe_interes_tc")==null?"":rs.getString("in_importe_interes_tc");
			String rs_subsidio			= rs.getString("subsidio")==null?"":rs.getString("subsidio");
			String rs_lim_subsidio		= rs.getString("fg_limite")==null?"":rs.getString("fg_limite");
			String rs_porcdesc			= rs.getString("fg_porcentaje_descuento")==null?"":rs.getString("fg_porcentaje_descuento");
			String rs_nompyme			= rs.getString("nombrePyme")==null?"":rs.getString("nombrePyme");
			String rs_fechasol			= rs.getString("fechaSol")==null?"":rs.getString("fechaSol");

			if(bSave) {
				subAcum = getSubsidioAcum(rs_pyme, con);
			} else {
				if(!auxPyme.equals(rs_pyme)) {
					subAcum = getSubsidioAcum(rs_pyme, con);
					auxPyme = rs_pyme;
				}
			}

			subAcum += Double.parseDouble(rs_subsidio);
			if(subAcum<Double.parseDouble(rs_lim_subsidio)) {
				int camposAdic = getNumCamposAdic(ic_epo,con);
				if(camposAdic>=5) {
					if(bSave && doctosinsert==0) {
						acuse = new Acuse(Acuse.ACUSE_EPO, "1");
						creaAcuseSubsidioAuto(acuse.toString(), rs_usuario, con);
					}
					String fechaVenc;
					if("".equals(strFechaVenc))
						fechaVenc= getFechaVencNoNeg("S", con);
					else
						fechaVenc= strFechaVenc;

					if(bSave) {
						String igConsec = getNumConsecDoc("", con);
						setDocumentoNoNegociable(
								rs_pyme, ic_epo, acuse.toString(), igConsec,
								fechaVenc, "1", rs_subsidio, rs_nomepo,
								rs_porcdesc, rs_numdocumento, rs_monto_tc, rs_montointeres_tc,
								con);
						setSubsidioAcum(rs_pyme, subAcum, con);
					}

					if(doctosinsert==0) {
						contenidoArchivo +=
							"Nombre PyME,"+
							"Fecha Operaci�n,"+
							"Subsidio,"+
							"Nombre EPO,"+
							"Num. Doc.,"+
							"Porc. Desc.,"+
							"Monto Desc.,"+
							"Monto Interes\n";
					}

					contenidoArchivo +=
						rs_nompyme.replace(',',' ')+","+
						rs_fechasol+","+
						rs_subsidio+","+
						rs_nomepo.replace(',',' ')+","+
						rs_numdocumento+","+
						rs_porcdesc+","+
						rs_monto_tc+","+
						rs_montointeres_tc+"\n";

					doctosinsert++;
					if("1".equals(rs_moneda)) {
						total_docs_mn	++;
						total_monto_mn	+= Double.parseDouble(rs_monto_tc);
					} else if("54".equals(rs_moneda)) {
						total_docs_dl	++;
						total_monto_dl	+= Double.parseDouble(rs_monto_tc);
					}
				}//if(camposAdic==5)
			}//if(subAcum<Double.parseDouble(rs_lim_subsidio))
			if(bSave) {
				setSubsidioAplicado(rs_documento, ic_epo, con);
			}
		}//while(rs.next())
		if(bSave && doctosinsert>0) {
			setAcuseSubsidioAuto(acuse.toString(),total_docs_mn,total_monto_mn,total_docs_dl,total_monto_dl,con);
		}
		rs.close();
		if(ps!=null) ps.close();
	} catch (NafinException ne) {
		bOK = false;
		throw ne;
	} catch (Exception e) {
		bOK = false;
		log.error(" CargaDocumentoEJB::subsidioAutomatico(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		if(bSave) {
			con.terminaTransaccion(bOK);
		}
		if (con.hayConexionAbierta() == true)
			con.cierraConexionDB();
		log.info(" CargaDocumentoEJB::subsidioAutomatico(S)");
	}
	return contenidoArchivo;
}//public void subsidioAutomatico(String ic_epo)

private void setSubsidioHistorico(AccesoDB con)
			throws NafinException {
	log.info(" CargaDocumentoEJB::setSubsidioHistorico(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	try {
		qrySentencia =
			" SELECT ic_producto_nafin, ic_pyme, to_char(sysdate, 'yyyy') actual"   +
			"   FROM comrel_subsidio_producto"   +
			"  WHERE df_vigencia is null"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		ps.clearParameters();
		while(rs.next()) {
			String rs_producto_nafin 	= rs.getString(1)==null?"":rs.getString(1);
			String rs_pyme 				= rs.getString(2)==null?"":rs.getString(2);
			String rs_actual 			= rs.getString(3)==null?"":rs.getString(3);
			qrySentencia =
				" UPDATE comrel_subsidio_producto"   +
				"    SET df_vigencia = TO_DATE (?, 'dd/mm/yyyy')"   +
				"  WHERE ic_producto_nafin = ? AND ic_pyme = ?"  ;
			//log.debug("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, "31/12/"+rs_actual);
			ps.setInt(2, Integer.parseInt(rs_producto_nafin));
			ps.setInt(3, Integer.parseInt(rs_pyme));
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		}//while(rs.next())
		ps.close();
		rs.close();

		qrySentencia =
			" SELECT ic_producto_nafin, ic_pyme, fg_acumulado,"   +
			"        TO_CHAR (df_vigencia, 'YYYY'), TO_CHAR (SYSDATE, 'yyyy')"   +
			"   FROM comrel_subsidio_producto"   +
			"  WHERE df_vigencia < TRUNC (SYSDATE)"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		ps.clearParameters();
		while(rs.next()) {
			String rs_producto_nafin	= (rs.getString(1)==null)?"":rs.getString(1);
			String rs_pyme				= (rs.getString(2)==null)?"":rs.getString(2);
			String rs_acumulado 		= (rs.getString(3)==null)?"0":rs.getString(3);
			String rs_vigencia 			= (rs.getString(4)==null)?"":rs.getString(4);
			String rs_anyo_act 			= (rs.getString(5)==null)?"":rs.getString(5);

			qrySentencia =
				" INSERT INTO comhis_subsidio"   +
				"             (ic_producto_nafin, ic_pyme, fg_acumulado, df_subsidio"   +
				"             )"   +
				"      VALUES (?, ?, ?, TO_DATE(?, 'dd/mm/yyyy')"   +
				"             )"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(rs_producto_nafin));
			ps.setInt(2, Integer.parseInt(rs_pyme));
			ps.setDouble(3, Double.parseDouble(rs_acumulado));
			ps.setString(4, "01/01/"+rs_vigencia);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

			qrySentencia =
				" UPDATE comrel_subsidio_producto"   +
				"    SET df_vigencia = TO_DATE (?, 'dd/mm/yyyy'),"   +
				"        fg_acumulado = NULL"   +
				"  WHERE ic_producto_nafin = ? AND ic_pyme = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, "31/12/"+rs_anyo_act);
			ps.setInt(2, Integer.parseInt(rs_producto_nafin));
			ps.setInt(3, Integer.parseInt(rs_pyme));
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		}//while(rs.next())
		ps.close();
		rs.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setSubsidioHistorico(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getSubsidioAcum(S)");
	}
}//setSubsidioHistorico

private double getSubsidioAcum(String ic_pyme, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::getSubsidioAcum(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	double				subsidioacum	= 0;
	try {
		qrySentencia =
			" SELECT fg_acumulado"   +
			"   FROM comrel_subsidio_producto"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND ic_pyme = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_pyme));
		rs = ps.executeQuery();
		if(rs.next())
			subsidioacum = rs.getDouble(1);
		rs.close();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::getSubsidioAcum(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getSubsidioAcum(S)");
	}
	return subsidioacum;
}

private void setSubsidioAcum(String ic_pyme, double subAcum, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::setSubsidioAcum(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	
	try {
		qrySentencia =
			" UPDATE comrel_subsidio_producto"   +
			"    SET fg_acumulado = ?"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND ic_pyme = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setDouble(1,subAcum);
		ps.setInt(2,Integer.parseInt(ic_pyme));
		ps.executeUpdate();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setSubsidioAcum(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setSubsidioAcum(S)");
	}
}

private void setAcuseSubsidioAuto(String acuse, int total_docs_mn,double total_monto_mn,int total_docs_dl,double total_monto_dl,AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::setAcuseSubsidioAuto(E)");
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	
	try {
		qrySentencia =
			" UPDATE com_acuse1"   +
			"    SET in_total_docto_mn = ?,"   +
			"        fn_total_monto_mn = ?,"   +
			"        in_total_docto_dl = ?,"   +
			"        fn_total_monto_dl = ?"   +
			"   WHERE cc_acuse = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,total_docs_mn);
		ps.setDouble(2,total_monto_mn);
		ps.setInt(3,total_docs_dl);
		ps.setDouble(4,total_monto_dl);
		ps.setString(5,acuse);
		ps.executeUpdate();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setAcuseSubsidioAuto(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setAcuseSubsidioAuto(S)");
	}
}

private void setSubsidioAplicado(String ic_documento, String ic_epo, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::setSubsidioAplicado(E)");
	
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	
	try {
		qrySentencia =
			" UPDATE com_documento"   +
			"    SET cs_subsidio = 'S"+ic_epo+"'"+
			"  WHERE ic_documento = ?";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_documento));
		ps.executeUpdate();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setSubsidioAplicado(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setSubsidioAplicado(S)");
	}
}

private int getNumCamposAdic(String ic_epo, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::getNumCamposAdic(E)");
	
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	int					camposadic		= 0;
	try {
		qrySentencia =
			" SELECT COUNT (1)"   +
			"   FROM comrel_visor"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND ic_epo = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_epo));
		rs = ps.executeQuery();
		if(rs.next())
			camposadic = rs.getInt(1);
		rs.close();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::getNumCamposAdic(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getNumCamposAdic(S)");
	}
	return camposadic;
}

private String getNumConsecDoc(String ic_epo, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::getNumConsecDoc(E)");
	
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	String				numero			= "1";
	try {
		if("".equals(ic_epo)) {
			qrySentencia =
				" SELECT seq_subsidio_auto_desc.nextval"   +
				"   FROM dual"  ;
		} else {
			qrySentencia =
				" SELECT seq_subsidio_walmart.nextval"   +
				"   FROM dual"  ;
		}

		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		if(rs.next())
			numero = rs.getString(1);
		rs.close();
		if(ps!=null) ps.close();
		numero = "S-"+Comunes.formatoFijo(Comunes.formatoDecimal(numero,0,false),9,"0","");
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::getNumConsecDoc(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getNumConsecDoc(S)");
	}
	return numero;
}

private String getFechaVencNoNeg(String chk_ultimo, AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::getFechaVencNoNeg(E)");

	//AccesoDB 			con 			= new AccesoDB();

	String 				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	String fechaVenc = "";
	int existe = 0;
	try {
		//con.conexionDB();
		String		fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String		diaAct		= fechaHoy.substring(0, 2);
		String		mesAct		= fechaHoy.substring(3, 5);
		String		anyoAct		= fechaHoy.substring(6, 10);
		Calendar 	cal 		= new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		int diaMax = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		Calendar aux = new GregorianCalendar();
		aux.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, diaMax);
		if(aux.get(Calendar.DAY_OF_WEEK)==7) // S�bado
			diaMax--;
		else if(aux.get(Calendar.DAY_OF_WEEK)==1) // Domingo
			diaMax-=2;

		if(diaMax == Integer.parseInt(diaAct)&&"S".equals(chk_ultimo)) {
			cal.add(Calendar.MONTH,1);
			diaMax = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}

		fechaVenc = diaMax+"/"+new java.text.SimpleDateFormat("MM/yyyy").format(cal.getTime());

		boolean catalogo = false;
		boolean sabdom = false;

		diaAct	= fechaVenc.substring(0, 2);
		mesAct	= fechaVenc.substring(3, 5);
		anyoAct	= fechaVenc.substring(6, 10);
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		while(!(catalogo&&sabdom)) {
			fechaVenc = new java.text.SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
			diaAct	= fechaVenc.substring(0, 2);
			mesAct	= fechaVenc.substring(3, 5);
			anyoAct	= fechaVenc.substring(6, 10);

			catalogo = false;
			sabdom = false;
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comcat_dia_inhabil"   +
				"  WHERE cg_dia_inhabil = ? "  +
				"	AND cg_dia_inhabil is not null ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, diaAct+"/"+mesAct);
			rs = ps.executeQuery();
			existe = 0;
			if(rs.next())
				existe = rs.getInt(1);
			rs.close();
			if(ps!=null) ps.close();
			if(existe==0) {
				catalogo = true;
				if(cal.get(Calendar.DAY_OF_WEEK)==7) // S�bado
					cal.add(Calendar.DATE, -1);//resta 2 dias
				else {
					if(cal.get(Calendar.DAY_OF_WEEK)==1) // Domingo
						cal.add(Calendar.DATE, -2);//resta 1 dia
					else
						sabdom = true;
				} //else
			}//if(existe==0)
			else
				cal.add(Calendar.DATE, -1);//resta 1 dia
		}//while
		fechaVenc = new java.text.SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::getFechaVencNoNeg(Exception): "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::getFechaVencNoNeg(S)");
	}
	return fechaVenc;
}


private static void creaAcuseSubsidioAuto(String cc_acuse, String ic_usuario, AccesoDB con) throws NafinException {
	log.info(" CargaDocumentoEJB::creaAcuseSubsidioAuto(E)");
	
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	
	try {
		qrySentencia =
			" INSERT INTO com_acuse1"   +
			"             (cc_acuse, df_fechahora_carga, ic_usuario, ic_producto_nafin)"   +
			"      VALUES(?, SYSDATE, ?, 1)"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, cc_acuse);
		ps.setString(2, ic_usuario);
		ps.executeUpdate();
		if(ps!=null) ps.close();
	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::creaAcuseSubsidioAuto(Exception): "+e);
		throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::creaAcuseSubsidioAuto(S)");
	}
} //creaAcuseSubsidioAuto


private static void setDocumentoNoNegociable(
			String ic_pyme,
			String ic_epo,
			String cc_acuse,
			String ig_numero_docto,
			String df_fecha_venc,
			String ic_moneda,
			String fn_monto,
			String cg_campo1,
			String cg_campo2,
			String cg_campo3,
			String cg_campo4,
			String cg_campo5,
			AccesoDB con)
		throws NafinException {
	log.info(" CargaDocumentoEJB::setDocumentoNoNegociable(E) ic_epo: "+ic_epo+" ic_pyme: "+ic_pyme);
	
	String				qrySentencia	= "";
	PreparedStatement 	ps				= null;
	ResultSet			rs				= null;
	try {
		qrySentencia =
			" SELECT seq_com_documento.NEXTVAL FROM DUAL ";
//			" SELECT NVL (MAX (ic_documento) + 1, 1)"   +011107
//			"   FROM com_documento"  ;
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		String ic_documento = "";
		if(rs.next())
			ic_documento = rs.getString(1)==null?"1":rs.getString(1);
		rs.close();
		if(ps!=null) ps.close();


		qrySentencia =
			" INSERT INTO com_documento"   +
			"             (ic_documento,"   +
			"              ic_pyme,"   +
			"              ic_epo,"   +
			"              cc_acuse,"   +
			"              ig_numero_docto,"   +
			"              df_fecha_docto,"   +
			"              df_fecha_venc,"   +
			"              ic_moneda,"   +
			"              fn_monto,"   +
			"              ic_estatus_docto,"   +
			"              cg_campo1,"   +
			"              cg_campo2,"   +
			"              cg_campo3,"   +
			"              cg_campo4,"   +
			"              cg_campo5"   +
			"             )"   +
			"         VALUES(?, ?, ?, ?, ?,"   +
			"                SYSDATE, TO_DATE (?, 'dd/mm/yyyy'), ?, ?, 1,"   +
			"                ?, ?, ?, ?, ?)";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(ic_documento));
		ps.setInt(2, Integer.parseInt(ic_pyme));
		ps.setInt(3, Integer.parseInt(ic_epo));
		ps.setString(4, cc_acuse);
		ps.setString(5, ig_numero_docto);
		ps.setString(6, df_fecha_venc);
		ps.setInt(7, Integer.parseInt(ic_moneda));
		ps.setDouble(8, Double.parseDouble(fn_monto));
		ps.setString(9, cg_campo1);
		ps.setString(10, cg_campo2);
		ps.setString(11, cg_campo3);
		ps.setString(12, cg_campo4);
		ps.setString(13, cg_campo5);
		ps.executeUpdate();
		if(ps!=null) ps.close();

		log.debug(" setDocumentoNoNegociable----------- "+qrySentencia);

	} catch (Exception e) {
		log.error(" CargaDocumentoEJB::setDocumentoNoNegociable(Exception): "+e);
		//throw new NafinException("SIST0001");
	} finally {
		log.info(" CargaDocumentoEJB::setDocumentoNoNegociable(S)");
	}
} // fin del m�todo creaAcuseDstoAuto.

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< IHJ*/



/**
 * Obtiene las cuentas de correo parametrizadas para la epo especificada,
 * m�s las cuentas generales parametrizadas (ic_epo IS NULL)
 * Si no hay cuentas parametrizadas para el Web Service regresa null.
 * @param claveEpo Clave de la epo
 *
 * @return Cadena con las cuenta de correo parametrizadas, separadas
 * 		por coma en caso de que sean dos o m�s.
 *
 */
private String getCuentasWS(String claveEpo) {
	AccesoDB con = new AccesoDB();
	String cuentasCorreo = null;

	try {
		con.conexionDB();
		StringBuffer sb = new StringBuffer();
		String strSQL =
				" SELECT cg_mail " +
				" FROM com_correo_ws " +
				" WHERE ic_epo = ? OR ic_epo IS NULL ";
		PreparedStatement ps = con.queryPrecompilado(strSQL);
		ps.setInt(1, Integer.parseInt(claveEpo));
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			String correo = rs.getString("cg_mail");
			if (correo != null  && !correo.equals("")) {
				sb.append(((sb.length() > 0)?",":"") + correo);
			}
		}
		rs.close();
		ps.close();

		cuentasCorreo = (sb.length() > 0)?sb.toString():null;


	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return cuentasCorreo;
}



/**
 * Obtiene la cuenta a la cual ser�n atribuidos los correos enviados
 *
 * @return Cadena con la cuenta de correo
 *
 */
private String getCuentaWSFrom() {
	AccesoDB con = new AccesoDB();
	String cuentaCorreo = "postmaster@nafin.gob.mx";

	try {
		con.conexionDB();
		String strSQL =
				" SELECT cg_mail_ws_from " +
				" FROM com_param_gral " +
				" WHERE ic_param_gral = 1 ";

		ResultSet rs = con.queryDB(strSQL);
		if(rs.next()) {
			String correo = rs.getString("cg_mail_ws_from");
			cuentaCorreo = (correo == null  || correo.equals(""))?
					"postmaster@nafin.gob.mx":correo;
		}
		rs.close();
		con.cierraStatement();

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return cuentaCorreo;
}

public void insertaDatosTempEnlace(String qry, List lista){
	log.info("insertaDatosTempEnlace(E)");
		boolean ok = true;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		try{
			if (lista.size() > 0) {
				con.conexionDB();
				//log.debug("qry::: "+qry);
				log.debug("lista::: "+lista + "tama�o lista:" + lista.size() );
				ps = con.queryPrecompilado(qry);

				if (lista.size() > 0) {	ps.setString(1, lista.get(0).toString());
				} else { ps.setString(1, null);	}
				if (lista.size() > 1) {	ps.setString(2, lista.get(1).toString());
				} else { ps.setString(2, null);	}
				if (lista.size() > 2) {	ps.setString(3, lista.get(2).toString());
				} else { ps.setString(3, null);	}
				if (lista.size() > 3) {	ps.setString(4, lista.get(3).toString());
				} else { ps.setString(4, null);	}
				if (lista.size() > 4) {	ps.setString(5, lista.get(4).toString());
				} else { ps.setString(5, null);	}
				if (lista.size() > 5) {	ps.setInt(6, Integer.parseInt(lista.get(5).toString()));
				} else { ps.setNull(6, Types.INTEGER);	}
				if (lista.size() > 6) {	ps.setDouble(7, Double.parseDouble(lista.get(6).toString()));
				} else { ps.setNull(7, Types.DOUBLE);	}
				if (lista.size() > 7) {	ps.setString(8, lista.get(7).toString());
				} else { ps.setString(8, null);	}
				if (lista.size() > 8) {	ps.setString(9, lista.get(8).toString());
				} else { ps.setString(9, null);	}

				if (lista.size() > 9) {	ps.setString(10, lista.get(9).toString());
				} else { ps.setString(10, null);	}
				if (lista.size() > 10) { ps.setString(11, lista.get(10).toString());
				} else { ps.setString(11, null);	}
				if (lista.size() > 11) { ps.setString(12, lista.get(11).toString());
				} else { ps.setString(12, null);	}
				if (lista.size() > 12) { ps.setString(13, lista.get(12).toString());
				} else { ps.setString(13, null);	}
				if (lista.size() > 13) { ps.setString(14, lista.get(13).toString());
				} else { ps.setString(14, null);	}

				ps.executeUpdate();

				if(ps!=null) ps.close();
			}
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
		}finally{
			log.info("insertaDatosTempEnlace(S)");
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

public String getAcusePublicacionEnlace(String qry,String enlace){
	log.info("getAcusePublicacionEnlace(E)");
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	String regresa = "";
	try{
		con.conexionDB();
		rs = con.queryDB(qry);
		if(rs.next()){
			if(enlace.equals("VWEnlace")){
				regresa = rs.getString("cc_acuse")+"|"+rs.getString("in_total_proc")+"|"+
						rs.getString("fn_total_monto_mn")+"|"+rs.getString("in_total_acep_mn")+"|"+
						rs.getString("in_total_rech_mn")+"|"+rs.getString("fn_total_monto_dl")+"|"+
						rs.getString("in_total_acep_dl")+"|"+rs.getString("in_total_rech_dl")+"|"+
						rs.getString("fecha")+"|"+rs.getString("hora")+"|"+rs.getString("cs_estatus");
			}

		}
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		log.info("getAcusePublicacionEnlace(S)");
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
	return regresa;
}

public String getErroresPublicacionEnlace(String qry,String enlace){
	log.info("getErroresPublicacionEnlace(E)");
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	String regresa = "";
	try{
		con.conexionDB();
		rs = con.queryDB(qry);
		while(rs.next()){
			if(enlace.equals("VWEnlace")){
				regresa += rs.getString("ig_numero_docto")+"|"+rs.getString("ig_numero_error")+"|"+rs.getString("cg_error")+"\n";
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		log.info("getErroresPublicacionEnlace(S)");
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
	return regresa;
}

public String getOperadosEnlace(String tabla){
	log.info("getOperadosEnlace(E)");
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	ResultSetMetaData rsMD = null;
	String regresa = "";
	try{
		con.conexionDB();
		rs = con.queryDB("SELECT * from " + tabla);
		//log.debug("SELECT * from " + tabla);
		rsMD = rs.getMetaData();
		while(rs.next()){
			for (int j = 1; j <= rsMD.getColumnCount(); j++) {
				regresa += rs.getString(j) + ((j<rsMD.getColumnCount())?"|":"");

			}
			regresa += "\n";
			//log.debug(regresa);
		}
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		log.info("getOperadosEnlace(S)");
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
	return regresa;
}

/* Verifica si la epo acepta "Documentos Vencidos sin Operar" */

	public boolean publicarDocVencidos(String sNoEPO) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bPubVencDoctoSinOP=false;
		try{
			con.conexionDB();
			String query =
				" SELECT COUNT (*)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE CS_PUB_DOCTO_VENC = 'S'" +
				"   AND ic_epo = "+sNoEPO;
			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bPubVencDoctoSinOP=(rs.getInt(1)>0)?true:false;
				else
					bPubVencDoctoSinOP=false;
			} catch(SQLException sqle) { throw new NafinException("DSCT0037"); }
			con.cierraStatement();

		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			log.error("Exception en publicarDocVencidos. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bPubVencDoctoSinOP;
	}

  private static boolean checaFecha(String formato) {
        String date=formato;
        StringTokenizer st=new StringTokenizer(date,"/");
        int dia=0, mes=0, ano=0;    boolean isFecha = false;
        while(st.hasMoreTokens()){
            try {
                dia = Integer.parseInt(st.nextToken());
                mes = Integer.parseInt(st.nextToken())-1;
                ano = Integer.parseInt(st.nextToken());       //log.debug(dia+"/"+mes+"/"+ano);
                Calendar fecha = new GregorianCalendar(ano, mes, dia);
                int year = fecha.get(Calendar.YEAR);
                int month = fecha.get(Calendar.MONTH);
                int day = fecha.get(Calendar.DAY_OF_MONTH);
                //log.debug(year+" "+month+" "+day);
                if((dia!=day) || (mes!=month) || (ano!=year))
                    isFecha = false;
                else
                    isFecha = true;
            }
            catch(NoSuchElementException nse) { isFecha = false; }
            catch(NumberFormatException nf) { isFecha = false; }
        } //while
    return isFecha;
    }

	 public boolean estaHabilitadaLaPublicacionEPOPEF(String claveEPO)
	 	throws NafinException{

		log.info("CargaDocumentoBean::estaHabilitadaLaPublicacionEPOPEF(E)");

		if(claveEPO == null || claveEPO.equals("")){
			log.info("CargaDocumentoBean::estaHabilitadaLaPublicacionEPOPEF(S)");
			return false;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		boolean 			hayPublicacionEPOPEF	= false;
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																	  " +
					"	DECODE(COUNT(1),0,'false','true') AS HAY_PUB_EPO_PEF 	  " +
					"FROM 																	  " +
					"	COM_PARAMETRIZACION_EPO 										  " +
					"WHERE 																	  " +
					" 	CC_PARAMETRO_EPO    = ? 									 AND " + // 'PUB_EPO_PEF_FECHA_RECEPCION'
					"	IC_EPO              = ? 									 AND " + // claveEPO
					"	CG_VALOR            = ? 				                    " ; // 'S'

				lVarBind.add("PUB_EPO_PEF_FECHA_RECEPCION");
				lVarBind.add(new Integer(claveEPO));
				lVarBind.add("S");

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					hayPublicacionEPOPEF = registros.getString("HAY_PUB_EPO_PEF").equals("true")?true:false;
				}

		} catch(Exception e) {
			log.error("CargaDocumentoBean::estaHabilitadaLaPublicacionEPOPEF(Exception)");
			log.info(" claveEPO = " + claveEPO);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CargaDocumentoBean::estaHabilitadaLaPublicacionEPOPEF(S)");
		}

		return hayPublicacionEPOPEF;
	 }

	 public void agregaParametrosPublicacionEPOPEF(int numeroProceso, int numeroLinea, HashMap parametrosPublicacionEPOPEF)
		throws NafinException {

		log.info("CargaDocumentoBean::agregaParametrosPublicacionEPOPEF(E)");

		AccesoDB		con				= new AccesoDB();
      String		qrySentencia   = "";
		List 			lVarBind			= new ArrayList();
		boolean		lbOk				= true;

		String fechaEntrega 			= null;
		String tipoCompra 			= null;
		String clavePresupuestaria	= null;
		String periodo					= null;

		try {
			con.conexionDB();

			fechaEntrega 			= (String) parametrosPublicacionEPOPEF.get("FECHA_ENTREGA");
			tipoCompra 				= (String) parametrosPublicacionEPOPEF.get("TIPO_COMPRA");
			clavePresupuestaria	= (String) parametrosPublicacionEPOPEF.get("CLAVE_PRESUPUESTARIA");
			periodo					= (String) parametrosPublicacionEPOPEF.get("PERIODO");

			fechaEntrega 			= fechaEntrega 			== null?"":fechaEntrega;
			tipoCompra 				= tipoCompra				== null?"":tipoCompra;
			clavePresupuestaria	= clavePresupuestaria	== null?"":clavePresupuestaria;
			periodo					= periodo					== null?"":periodo;

			qrySentencia =
				"UPDATE                          		  	" +
				"	COMTMP_CARGA_DOCTO                	  	" +
				"SET                             		  	" +
				"	DF_ENTREGA 					= ?, 				" +
				"	CG_TIPO_COMPRA 			= UPPER(?),    " +
				"	CG_CLAVE_PRESUPUESTARIA = ?,          	" +
				"	CG_PERIODO 					= ?            " +
				"WHERE                           		  	" +
				"	IC_CARGA_DOCTO         	= ?  AND 		" +
				"	IC_LINEA          		= ?     			";


			lVarBind.add(fechaEntrega);
			lVarBind.add(tipoCompra);
			lVarBind.add(clavePresupuestaria);
			lVarBind.add(periodo);
			lVarBind.add(new Integer(numeroProceso));
			lVarBind.add(new Integer(numeroLinea));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			log.error("CargaDocumentoBean::agregaParametrosPublicacionEPOPEF(Exception)");
			lbOk = false;
			log.error(" numeroProceso 			= " + numeroProceso			);
			log.error(" numeroLinea				= " + numeroLinea 			);
			log.error(" fechaEntrega 			= " + fechaEntrega			);
			log.error(" tipoCompra 				= " + tipoCompra				);
			log.error(" clavePresupuestaria	= " + clavePresupuestaria	);
			log.error(" periodo					= " + periodo					);

			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(lbOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		log.info("CargaDocumentoBean::agregaParametrosPublicacionEPOPEF(S)");
	}
	/*********************************************************************************************
	*
	*	Este m�todo verifica si la epo maneja parametrizaci�n EPO-PEF.
	*
	*********************************************************************************************/
	public boolean tieneParamEpoPef(String icEpo) throws NafinException {
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List variablesBind = new ArrayList();
		PreparedStatement pst = null;
		ResultSet rst = null;
		boolean paramEpoPef = false;

		try{
			con.conexionDB();

			strSQL.append(" SELECT cg_valor");
			strSQL.append(" FROM com_parametrizacion_epo");
			strSQL.append(" WHERE ic_epo = ?");
			strSQL.append(" AND cc_parametro_epo = ?");

			variablesBind.add(new Long(icEpo));
			variablesBind.add("PUB_EPO_PEF_FECHA_RECEPCION");

			pst = con.queryPrecompilado(strSQL.toString(), variablesBind);

			rst = pst.executeQuery();

			String valor = "";

			while(rst.next()){
				valor = rst.getString(1)==null?"":rst.getString(1);
			}

			rst.close();
			pst.close();

			if(!valor.equals("") && valor.equals("S")){
				paramEpoPef = true;
			}else{
				paramEpoPef = false;
			}

			return paramEpoPef;
		}catch(Exception ne){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Genera un archivo origen de carga de documentos, a partir del numero de acuse
	 * @param ic_epo Clave Epo
	 * @param cc_acuse Clave del acuse
	 * @param bandera que indica si se van a mostrar los 4 campos de la parametrizacion
	 * 	(Fecha recepcion) en el documento de texto
	 * @return Cadena con el contenido para el archivo a generar
	 */
	public String generarArchivoOrigenconParam(String ic_epo, String cc_acuse, boolean bmostrarparams)
			throws NafinException {

		StringBuffer contenidoArchivo = new StringBuffer();
		AccesoDB con = new AccesoDB();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_epo == null || ic_epo.equals("") ||
					cc_acuse == null || cc_acuse.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
			Integer.parseInt(ic_epo);

		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" ic_epo=" + ic_epo + "\n" +
					" cc_acuse=" + cc_acuse);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************



		String strSQL =
				" SELECT d.cs_dscto_especial, " +
				" 	pe.cg_pyme_epo_interno, d.ig_numero_docto, " +
				" 	TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS df_fecha_docto, " +
				" 	TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc, " +
				"  d.ic_moneda, d.fn_monto, d.ct_referencia, " +
				" 	d.cg_campo1, d.cg_campo2, d.cg_campo3, " +
				" 	d.cg_campo4, d.cg_campo5, " +
				" 	n.ic_nafin_electronico AS ne_if, " +
				" 	n1.ic_nafin_electronico AS ne_if_benef, " +
				" 	d.fn_porc_beneficiario, " +
				" 	d.fn_monto * d.fn_porc_beneficiario / 100 AS monto_benef " +
				" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" FROM com_documento d, comrel_pyme_epo pe, " +
				" 	com_acuse1 a1, comrel_nafin n, " +
				" 	comrel_nafin n1 " +
				" WHERE " +
				" 	d.cc_acuse = ? " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_pyme = pe.ic_pyme " +
				" 	AND d.cc_acuse = a1.cc_acuse " +
				" 	AND d.ic_if = n.ic_epo_pyme_if (+) " +
				" 	AND n.cg_tipo (+) = 'I' " +
				" 	AND d.ic_beneficiario = n1.ic_epo_pyme_if (+) " +
				" 	AND n1.cg_tipo (+) = 'I' " +
				" ORDER BY d.cs_dscto_especial, " +
				" 	pe.cg_pyme_epo_interno, d.ig_numero_docto ";

		try {

			con.conexionDB();

			int numCamposAdicionales = getNumCamposAdic(ic_epo, con);
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				String numProveedor = (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
				String numeroDocumento = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				String fechaDocumento = (rs.getString("DF_FECHA_DOCTO")==null)?"":rs.getString("DF_FECHA_DOCTO");
				String fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
				String claveMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
				String monto = (rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO");
				String tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
				String referencia = (rs.getString("CT_REFERENCIA")==null)?"":rs.getString("CT_REFERENCIA");
				String campoDinamico1 = (rs.getString("CG_CAMPO1")==null)?"":rs.getString("CG_CAMPO1");
				String campoDinamico2 = (rs.getString("CG_CAMPO2")==null)?"":rs.getString("CG_CAMPO2");
				String campoDinamico3 = (rs.getString("CG_CAMPO3")==null)?"":rs.getString("CG_CAMPO3");
				String campoDinamico4 = (rs.getString("CG_CAMPO4")==null)?"":rs.getString("CG_CAMPO4");
				String campoDinamico5 = (rs.getString("CG_CAMPO5")==null)?"":rs.getString("CG_CAMPO5");
				String neIf = (rs.getString("NE_IF")==null)?"":rs.getString("NE_IF");
				String neIfBeneficiario = (rs.getString("NE_IF_BENEF")==null)?"":rs.getString("NE_IF_BENEF");
				String porcentajeBeneficiario = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
				String montoBeneficiario = (rs.getString("MONTO_BENEF")==null)?"":rs.getString("MONTO_BENEF");

				String fechaEntrega 				= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
				String tipoCompra 					= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
				String clavePresupuestaria 	= (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
				String periodo 							= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");

				contenidoArchivo.append(
						numProveedor + "|" + numeroDocumento + "|" +
						fechaDocumento + "|" +
						fechaVencimiento + "|" + claveMoneda + "|" +
						monto + "|" + tipoFactoraje + "|" +
						referencia);

				if (numCamposAdicionales>= 1) {
						contenidoArchivo.append("|" + campoDinamico1);
				}
				if (numCamposAdicionales>= 2) {
						contenidoArchivo.append("|" + campoDinamico2);
				}
				if (numCamposAdicionales>= 3) {
						contenidoArchivo.append("|" + campoDinamico3);
				}
				if (numCamposAdicionales>= 4) {
						contenidoArchivo.append("|" + campoDinamico4);
				}
				if (numCamposAdicionales>= 5) {
						contenidoArchivo.append("|" + campoDinamico5);
				}

				if(bmostrarparams) {
					contenidoArchivo.append("|"+fechaEntrega+"|"+tipoCompra+"|"+clavePresupuestaria+"|"+ periodo);
				}


				if (tipoFactoraje.equals("N") || tipoFactoraje.equals("C") ||
						tipoFactoraje.equals("X") ) {	//Normal o Notas de Credito � X
						contenidoArchivo.append("\n");
				} else if (tipoFactoraje.equals("V")) { //Vencido
					contenidoArchivo.append("|" + neIf );
					contenidoArchivo.append("\n");
				} else if (tipoFactoraje.equals("D")) { //Distribuidor
					contenidoArchivo.append("||" + neIfBeneficiario + "|" + porcentajeBeneficiario + "|" + montoBeneficiario );
					contenidoArchivo.append("\n");
				} else { //Si no est� contemplado el tipo de factoraje, lo trata como Normal
					contenidoArchivo.append("\n");
				}

				//Siempre se pone un enter
				contenidoArchivo.append("\n");

			}//fin while
			rs.close();
			ps.close();
			return contenidoArchivo.toString();
		} catch(SQLException e) {
			log.error("Query::" + strSQL + " " + e.getMessage());
			throw new NafinException("SIST0001");
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}	//generarArchivoOrigenconParam

	/**
	 * Este metodo valida si la EPO cuya clave se proporciona en el parametro <tt>claveEpo</tt>
	 * Opera Factoraje con Mandato.
	 *
	 * @param claveEpo Clave de la Epo.
	 * @return <tt>true</tt> si la Epo opera con mandato; <tt>false</tt>
	 *         en caso contrario.
	 */
	private boolean operaFactorajeConMandato(String claveEpo) throws Exception{

		log.info("CargaDocumentoBean::operaFactorajeConMandato(E)");

		AccesoDB 				con		= new AccesoDB();
    	PreparedStatement 	ps 		= null;
    	ResultSet 				rs 		= null;
		StringBuffer        	query 	= new StringBuffer();
		boolean					opera		= false;

		try {

			con.conexionDB();

			query.append(
				"SELECT                         "  +
				"  DECODE(NVL(pz.cg_valor, 'N'),'S','true','false') as OPERA_MANDATO "  +
				"FROM                           "  +
				"  com_parametrizacion_epo pz,  "  +
				"  comcat_parametro_epo pe      "  +
				"WHERE                          "  +
				"  pz.cc_parametro_epo(+)  = pe.cc_parametro_epo  and "  +
				"  pz.ic_epo(+)            = ?                    and "  + // ic_epo
				"  pe.cc_parametro_epo     = ?                        " // 'PUB_EPO_OPERA_MANDATO'
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		  Integer.parseInt(claveEpo));
			ps.setString(2, 	"PUB_EPO_OPERA_MANDATO");
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				String valor = rs.getString("OPERA_MANDATO");
				opera = (valor != null && valor.equals("true"))?true:false;
			}

		}catch(Exception e){
			log.debug("CargaDocumentoBean::operaFactorajeConMandato(Exception)");
			log.debug(" claveEpo = <"+claveEpo+">");
			e.printStackTrace();
			throw new AppException("CargaDocumentoBean::operaFactorajeConMandato(Exception)", e);
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta())  con.cierraConexionDB();
			log.info("CargaDocumentoBean::operaFactorajeConMandato(S)");
		}
		return opera;
	}

	/**
	 * Este metodo metodo devuelve el ic_mandante a partir de la clave Mandante
	 * (No. de Nafin electronico del Mandante).
	 *
	 * @param claveMandante No. de Nafin electronico del Mandante asignado.
	 * @return ic_mandante.
	 *
	 */
	private String getIcMandante(String claveMandante) throws Exception{

		log.info("CargaDocumentoBean::getIcMandante(E)");

		AccesoDB 				con			= new AccesoDB();
    	PreparedStatement 	ps 			= null;
    	ResultSet 				rs 			= null;
		StringBuffer        	query 		= new StringBuffer();
		String 					icMandante	= "";

		if(claveMandante == null || claveMandante.trim().equals("")){
			log.info("CargaDocumentoBean::getIcMandante(S)");
			return("");
		}

		try {

			con.conexionDB();

			query.append(
				"SELECT 									"  +
				"	IC_EPO_PYME_IF AS IC_MANDANTE "  +
				"FROM 									"  +
				"	COMREL_NAFIN 						"  +
				"WHERE 									"  +
				"	CG_TIPO					= ? AND 	"  + 	// 'M'
				"	IC_NAFIN_ELECTRONICO = ? 		" 		// N@E
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, 	"M");
			ps.setInt(2, 		Integer.parseInt(claveMandante));
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				String valor = rs.getString("IC_MANDANTE");
				icMandante = (valor == null)?"":valor;
			}

		}catch(Exception e){
			log.debug("CargaDocumentoBean::getIcMandante(Exception)");
			log.debug("  claveMandante = <"+claveMandante+">");
			e.printStackTrace();
			throw new AppException("CargaDocumentoBean::getIcMandante(Exception)", e);
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();
			log.info("CargaDocumentoBean::getIcMandante(S)");
		}

		return icMandante;
	}

	/**
    * Almacena en la Bitacora de Error en la Publicacion (BIT_ERROR_PUBLICACION), aquellos documentos que al intentar publicarlos desde la
	 * PANTALLA: ADMIN EPO - DESCUENTO ELECTRONICO � CARGA INDIVIDUAL DE DOCUMENTOS NEGOCIABLES presenta un error
	 *	del siguiente tipo: "La clave del proveedor no existe"
	 *
	 * @Fodea 059 - 2010
	 * @Author JSHD
	 *
	 * @param icEpo Clave de la Epo que publica el Documento
	 * @param cgNumeroProveedor N�mero de Proveedor asignado por la EPO
	 * @param igNumeroDocumento Numero de Documento asignado por la EPO
	 * @param dfVencimiento Fecha en la que Vence el Documento
	 * @param icMoneda Clave de la Moneda
	 * @param fnMonto Monto del Documento
	 * @param ccTipoFactoraje Tipo de Factoraje del documento
	 * @param cgUsuarioPublica Nombre del Usuario que reliza la publicacion
	 * @param cgEmail Correo del Usuario que realiza la publicacion
	 *
	 */
	public void registraDoctoConErrorEnPublicacion(
			String icEpo,
			String cgNumeroProveedor,
			String igNumeroDocumento,
			String dfVencimiento,
			String icMoneda,
			String fnMonto,
			String ccTipoFactoraje,
			String cgUsuarioPublica,
			String cgEmail)
	{
		log.info("registraDoctoConErrorEnPublicacion(E)");

		if(icEpo == null || icEpo.trim().equals("") ){
			log.info("registraDoctoConErrorEnPublicacion(S)");
			return;
		}

		icEpo	= icEpo 	!= null?icEpo.trim()	:icEpo;
		if(!Comunes.esNumeroEnteroPositivo(icEpo) || icEpo.length() > 4 ){
			log.info("registraDoctoConErrorEnPublicacion(S)");
			return;
		}

		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		strSQL 			= new StringBuffer();
		PreparedStatement ps 				= null;
		boolean 				exitoOperacion = true;

		cgUsuarioPublica	= cgUsuarioPublica   == null || cgUsuarioPublica.trim().equals("")?"No disponible"				:cgUsuarioPublica;

		cgNumeroProveedor	= cgNumeroProveedor 	!= null?cgNumeroProveedor.trim()	:cgNumeroProveedor;
		igNumeroDocumento	= igNumeroDocumento 	!= null?igNumeroDocumento.trim()	:igNumeroDocumento;
		dfVencimiento		= dfVencimiento 		!= null?dfVencimiento.trim()		:dfVencimiento;
		icMoneda				= icMoneda			   != null?icMoneda.trim()				:icMoneda;
		fnMonto				= fnMonto				!= null?fnMonto.trim()				:fnMonto;
		ccTipoFactoraje	= ccTipoFactoraje		!= null?ccTipoFactoraje.trim()	:ccTipoFactoraje;
		cgUsuarioPublica	= cgUsuarioPublica	!= null?cgUsuarioPublica.trim()	:cgUsuarioPublica;
		cgEmail				= cgEmail				!= null?cgEmail.trim()				:cgEmail;

		icEpo					= icEpo 				  != null && icEpo.length() 				 > 4  ?icEpo.substring(0,4)					:icEpo;
		cgNumeroProveedor	= cgNumeroProveedor != null && cgNumeroProveedor.length() > 25 ?cgNumeroProveedor.substring(0,25)	:cgNumeroProveedor;
		igNumeroDocumento	= igNumeroDocumento != null && igNumeroDocumento.length() > 15 ?igNumeroDocumento.substring(0,15)	:igNumeroDocumento;
		dfVencimiento		= dfVencimiento     != null && dfVencimiento.length()     > 10 ?dfVencimiento.substring(0,10)    	:dfVencimiento;
		icMoneda				= icMoneda			  != null && icMoneda.length()          > 3  ?icMoneda.substring(0,3)			   :icMoneda;
		fnMonto				= fnMonto			  != null && fnMonto.length()				 > 17 ?fnMonto.substring(0,17)				:fnMonto;
		ccTipoFactoraje	= ccTipoFactoraje   != null && ccTipoFactoraje.length()   > 1  ?ccTipoFactoraje.substring(0,1)		:ccTipoFactoraje;
		cgUsuarioPublica	= cgUsuarioPublica  != null && cgUsuarioPublica.length()  > 100?cgUsuarioPublica.substring(0,100)	:cgUsuarioPublica;
		cgEmail				= cgEmail			  != null && cgEmail.length()				 > 100?cgEmail.substring(0,100)				:cgEmail;

		try {

			con.conexionDB();
			strSQL.append(
				"INSERT INTO                "  +
				"	BIT_ERROR_PUBLICACION    "  +
				"	(                        "  +
				"		IC_ERROR_PUBLICACION, "  +
				"		IC_EPO,               "  +
				"		CG_NUMERO_PROVEEDOR,  "  +
				"		IG_NUMERO_DOCUMENTO,  "  +
				"		DF_PUBLICACION,       "  +
				"		DF_VENCIMIENTO,       "  +
				"		IC_MONEDA,            "  +
				"		FN_MONTO,             "  +
				"		CC_TIPO_FACTORAJE,    "  +
				"		CG_USUARIO_PUBLICA,   "  +
				"		CG_EMAIL              "  +
				"	)                        "  +
				"  VALUES                   "  +
				"	(                        "  +
				"	   SEQ_BIT_ERROR_PUBLICACION.NEXTVAL, "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	SYSDATE,					 "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	?, 						 "  +
				"  	?  						 "  +
				"  ) "
			);
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(icEpo));
			if(cgNumeroProveedor != null && !cgNumeroProveedor.trim().equals("")){
				ps.setString(2, 	cgNumeroProveedor);
			}else{
				ps.setNull(2, 	Types.VARCHAR);
			}
			if(igNumeroDocumento != null && !igNumeroDocumento.trim().equals("")){
				ps.setString(3, 	igNumeroDocumento);
			}else{
				ps.setNull(3, 	Types.VARCHAR);
			}
			if(dfVencimiento != null && !dfVencimiento.trim().equals("")){
				ps.setString(4, 	dfVencimiento);
			}else{
				ps.setNull(4, 	Types.VARCHAR);
			}
			if(icMoneda != null && !icMoneda.trim().equals("")){
				ps.setString(5, 	icMoneda);
			}else{
				ps.setNull(5, Types.VARCHAR);
			}
			if( fnMonto != null && !fnMonto.trim().equals("")){
				ps.setString(6,	fnMonto);
			}else{
				ps.setNull(6, Types.VARCHAR);
			}
			if(ccTipoFactoraje != null && !ccTipoFactoraje.trim().equals("")){
				ps.setString(7, 	ccTipoFactoraje);
			}else{
				ps.setNull(7,	Types.VARCHAR);
			}
			ps.setString(8,	cgUsuarioPublica);
			if( cgEmail != null && !cgEmail.trim().equals("")){
				ps.setString(9,	cgEmail);
			}else{
				ps.setNull(9, 	Types.VARCHAR);
			}
			ps.executeUpdate();
			ps.close();

		}catch(Exception e){
			log.error("registraDoctoConErrorEnPublicacion(Exception)");
			log.error("registraDoctoConErrorEnPublicacion. = <"+icEpo+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+cgNumeroProveedor+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+igNumeroDocumento+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+dfVencimiento+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+icMoneda+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+fnMonto+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+ccTipoFactoraje+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+cgUsuarioPublica+">");
			log.error("registraDoctoConErrorEnPublicacion. = <"+cgEmail+">");
			e.printStackTrace();
			exitoOperacion = false;
		}finally{
			if(ps != null) { try { ps.close();}catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exitoOperacion);
				con.cierraConexionDB();
			}
			log.info("registraDoctoConErrorEnPublicacion(S)");
		}
	}

	/**
	 * Borra en la Bitacora de Error en la Publicacion (BIT_ERROR_PUBLICACION) los documentos especificados en
	 * la lista <tt>listaRegistros</tt>
	 *
	 * @param listaRegistros <tt>ArrayList</tt> con la lista de los documentos a borrar.
	 *
	 */
	public void borraDoctosBitacoraErrorPublicacion(ArrayList listaRegistros)
		throws AppException{

		log.info("borraDoctosBitacoraErrorPublicacion(E)");

		if(listaRegistros == null || listaRegistros.size() == 0 ){
			log.info("borraDoctosBitacoraErrorPublicacion(S)");
			return;
		}

		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		strSQL 			= new StringBuffer();
		PreparedStatement ps 				= null;
		boolean 				exitoOperacion = true;

		StringBuffer 		variablesBind  = new StringBuffer();

		boolean hayVariableBindAgregada = false;
		for(int i=0;i<listaRegistros.size();i++){
			String id = (String) listaRegistros.get(i);
			if( id != null && !id.trim().equals("")){
				if(hayVariableBindAgregada) variablesBind.append(",");
				variablesBind.append("?");
				hayVariableBindAgregada = true;
			}
		}

		if(!hayVariableBindAgregada){
			log.info("No se tiene ningun registro con id valido que se pueda borrar. Se cancela la operacion de borrado. ");
			log.info("borraDoctosBitacoraErrorPublicacion(S)");
			return;
		}

		try {
			con.conexionDB();

			strSQL.append(
				"DELETE FROM                "  +
				"  BIT_ERROR_PUBLICACION    "  +
				"WHERE                      "  +
				"  IC_ERROR_PUBLICACION IN  "  +
				"   (                       "  +
				   variablesBind.toString()   +
				"   )  "
			);

			ps = con.queryPrecompilado(strSQL.toString());
			for(int i=0,indice=1;i<listaRegistros.size();i++){
				String id = (String) listaRegistros.get(i);
				if( id != null && !id.trim().equals("")){
					ps.setString(indice++, id);
				}
			}
			ps.executeUpdate();
			ps.close();

		}catch(Exception e){
			log.error("borraDoctosBitacoraErrorPublicacion(Exception)");
			log.error("borraDoctosBitacoraErrorPublicacion.listaRegistros = <"+listaRegistros+">");
			e.printStackTrace();
			exitoOperacion = false;
			throw new AppException("Error al borrar registros en la Bitacora de Error Publicacion.");
		}finally{
			if (ps != null) { try { ps.close();}catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exitoOperacion);
				con.cierraConexionDB();
			}
			log.info("borraDoctosBitacoraErrorPublicacion(S)");
		}

	}

	/**
	 * Obtiene el acuse con el resumen de carga.
	 * @param acuse Acuse
	 * @return Objeto Registros con los valores del acuse
	 *
	 */
	public Registros getDatosAcuse(String acuse) {

		log.info("getDatosAcuse(E)");
		AccesoDB con = new AccesoDB();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (acuse == null || acuse.equals("")) {
				throw new Exception("El acuse es requerido");
			}
		} catch(Exception e) {
			String error =
					"ERROR. Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"acuse=" + acuse + "\n";
			throw new AppException(error);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			
			String strSQL =
					" SELECT in_total_docto_mn, fn_total_monto_mn,  " +
					"       in_total_docto_dl, fn_total_monto_dl,  " +
					"       TO_CHAR(df_fechahora_carga,'dd/mm/yyyy') as fecha_carga, " +
					"       TO_CHAR(df_fechahora_carga,'hh24:mi:ss') as hora_carga, " +
					"       ic_usuario, " +
					"       cg_recibo_electronico,  " +
					"       cs_carga_ws " +
					" FROM com_acuse1 " +
					" WHERE cc_acuse = ? ";

			List params = new ArrayList();
			params.add(acuse);

			log.debug("getDatosAcuse:: strSQL=" + strSQL );
			log.debug("getDatosAcuse:: params=" + params );

			Registros reg = con.consultarDB(strSQL, params);

			return reg;
		} catch(Exception e) {
			throw new AppException("Error al obtener los datos de acuse", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getDatosAcuse(S)");
		}
	}


/**
	 * cambiara los documentos de estatus �En Proceso de Autorizaci�n� a  �Negociable�.
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public String  procesoCambioEstatus (String ruta, String claveEpos )	throws AppException	{
		log.info("procesoCambioEstatus (E)");
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		String respuesta ="", documentos = "", ic_documento ="", ic_estatus_docto="";
		String envioCorreo ="N";
		String ic_if ="";
		try {
			con.conexionDB();

			//obtengo el numero de documentos que van hacer cambiados a estatus Negociable
			//Fodea 021-2012-Se agrega que se consideren los doctos con estatus "Seleccionada Pyme"
			//Esta parte no implementa las reglas de negocio para los documentos con notas cr�dito
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select ic_documento,ic_estatus_docto from  com_documento ");
			qrySentencia.append(" where ic_epo in("+claveEpos+")  "); // Se incorpora el IMSS al proceso
			qrySentencia.append(" and  ic_estatus_docto in (24,3) ");
			log.debug("qrySentencia.toString()  "+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				documentos += rs.getString("ic_documento")==null?"":rs.getString("ic_documento")+",";
				ic_documento   = rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
				ic_estatus_docto   = rs.getString("ic_estatus_docto")==null?"":rs.getString("ic_estatus_docto");
				Integer.parseInt(ic_estatus_docto);

				// modifico el estatus de "En proceso de Autorizacion IF" a "Negociable"
				qrySentencia = new StringBuffer();
				qrySentencia.append(" update com_documento ");
				qrySentencia.append(" set  ic_estatus_docto = 2 ");
				qrySentencia.append(" where ic_estatus_docto in (24,3) ");
				qrySentencia.append(" and ic_documento  = ? ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();

				//registra el cambio de estatus
				qrySentencia = new StringBuffer();
				String cambio ="Retorno Autom�tico por horario";
				qrySentencia.append(" INSERT INTO  COMHIS_CAMBIO_ESTATUS ");
				qrySentencia.append(" (dc_fecha_cambio , ic_documento, ic_cambio_estatus, ct_cambio_motivo ) ");
				qrySentencia.append(" values(sysdate, ? , ? , ?) ");

				if (ic_estatus_docto.equals("24")){
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.setInt(2, 41);
				ps.setString(3, cambio);
				ps.executeUpdate();
				ps.close();
				} else{
					ps = con.queryPrecompilado(qrySentencia.toString());
					ps.setInt(1, Integer.parseInt(ic_documento));
					ps.setInt(2, 2);
					ps.setString(3, cambio);
					ps.executeUpdate();
					ps.close();
				}

				envioCorreo ="S";
			}
			rs.close();
			ps.close();
      if(	envioCorreo.equals("S")	 && !documentos.equals("")){
					// Se enviara  correo para  los IF's que tengas parametrizado  correos  en la tabla cg_email_noti_if
				if(!documentos.equals("")){
					int tamanio2 =	documentos.length();
					String valor =  documentos.substring(tamanio2-1, tamanio2);
					if(valor.equals(",")){
						documentos =documentos.substring(0,tamanio2-1);
					}
				}
				String correos ="";
				//se obtienes los correos de los If parametrizados para Notificar Documentos de Cambio de Estatus
				qrySentencia = new StringBuffer();
				qrySentencia.append("select ic_if, replace(cg_email_noti_if, '|', ',') as cg_email_noti_if  from com_email_noti_if ");
				qrySentencia.append(" where cg_notificar  = 'CE' ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();
				while (rs.next()) {
					correos =rs.getString("cg_email_noti_if")==null?"":rs.getString("cg_email_noti_if");
					ic_if =rs.getString("ic_if")==null?"":rs.getString("ic_if");

					HashMap archivoAdjunto 		 = new HashMap();
					Correo correo = new Correo();
					ArrayList listaDeImagenes = null;
					ArrayList listaDeArchivos = new ArrayList();
					String	dia	 = new java.text.SimpleDateFormat("dd").format(new java.util.Date());
					String	mes	 = new java.text.SimpleDateFormat("MM").format(new java.util.Date());
					String	anio	 = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
					String  nombreMes ="";
					if(mes.equals("01")){ nombreMes = "Enero"; }
					if(mes.equals("02")){ nombreMes = "Febrero"; }
					if(mes.equals("03")){ nombreMes = "Marzo"; }
					if(mes.equals("04")){ nombreMes = "Abril"; }
					if(mes.equals("05")){ nombreMes = "Mayo"; }
					if(mes.equals("06")){ nombreMes = "Junio"; }
					if(mes.equals("07")){ nombreMes = "Julio"; }
					if(mes.equals("08")){ nombreMes = "Agosto"; }
					if(mes.equals("09")){ nombreMes = "Septiembre"; }
					if(mes.equals("10")){ nombreMes = "Octubre"; }
					if(mes.equals("11")){ nombreMes = "Noviembre"; }
					if(mes.equals("12")){ nombreMes = "Diciembre"; }

					String strDirectorioTemp = ruta;
					String strDirectorioFisico = strDirectorioTemp+"/00tmp/13descuento/";
					//metodo que regresa el archivo
					String nombreArchivo = CorreoprocesoCambioEstatus(documentos, strDirectorioFisico, ic_if );

					if(!nombreArchivo.equals("")) {
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );
						String mensajeCorreo 	=" Relaci�n de documentos con modificaci�n de estatus En Proceso de autorizaci�n IF y Seleccionada Pyme a Negociable, "+dia+" de "+nombreMes+" del "+anio;
						listaDeArchivos.add(archivoAdjunto);
						String de ="cadenas@nafin.gob.mx";
						String tituloMensaje ="DOCUMENTOS CON CAMBIOS DE ESTATUS";
						String para = correos;

						try { //Si el env�o del correo falla se ignora.
							correo.enviaCorreoConDatosAdjuntos(de,para,"",tituloMensaje,mensajeCorreo,listaDeImagenes,listaDeArchivos);
						} catch(Throwable t) {
							log.error("Error al enviar correo " + t.getMessage());
						}
					}
				}
				rs.close();
				ps.close();
			}

		}catch(Exception e){
			resultado = false;
			log.error("procesoCambioEstatus (Exception) " + e);
			throw new AppException("procesoCambioEstatus(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("procesoCambioEstatus (S)");

		return respuesta;
	}

/**
	 *  metodo para obtener los documentos que fueron procesados  para su cambio de estatus
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param strDirectorioTemp
	 * @param documentos
	 */
	private String  CorreoprocesoCambioEstatus (String documentos, String strDirectorioTemp, String ic_if  )	throws AppException	{

		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		BufferedWriter 	out 								= null;
		String nombreArchivo = "";
		String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());
		OutputStreamWriter writer = null;

		log.info("CorreoprocesoCambioEstatus (E)");

		try {
			con.conexionDB();

			log.debug("ic_if "+ic_if +"documentos   "+documentos);

			// Obtengo ls registros que fueron modificados y los envio en un excel  por correo electronico
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select e.cg_razon_social as nombreEPO, ");
			qrySentencia.append(" p.cg_razon_social as nombrePYME, ");
			qrySentencia.append(" p.ic_pyme as noProveedor, ");
			qrySentencia.append(" d.ic_documento as noDocumento,  ");
			qrySentencia.append(" TO_CHAR(d.df_fecha_docto, 'dd/mm/yyyy')  as fechaEmision, ");
			qrySentencia.append(" TO_CHAR(d.df_fecha_venc, 'dd/mm/yyyy')   as fechaVencimiento, ");
			qrySentencia.append(" m.cd_nombre as moneda, ");
			qrySentencia.append(" d.fn_monto as monto, ");
			qrySentencia.append(" d.fn_porc_beneficiario as porcentajeDescuento,");
			qrySentencia.append(" d.fn_monto_dscto as montoDescontar,  ");
			qrySentencia.append(" ed.cd_descripcion as estatus, ");
			qrySentencia.append(" d.ct_referencia as referencia,");
			qrySentencia.append(" TO_CHAR(d.df_entrega, 'dd/mm/yyyy')  as fechaRecepcion,");
			qrySentencia.append(" d.cg_tipo_compra as tipoCompra,");
			qrySentencia.append(" d.cg_clave_presupuestaria as clasificador,");
			qrySentencia.append(" d.cg_periodo as plazoMaximo ");
			qrySentencia.append(" from ");
			qrySentencia.append(" com_documento  d,");
			qrySentencia.append(" comcat_epo e, ");
			qrySentencia.append(" comcat_pyme p, ");
			qrySentencia.append(" comcat_moneda m,");
			qrySentencia.append(" comcat_estatus_docto ed ");
			qrySentencia.append(" where d.ic_epo = e.ic_epo ");
			qrySentencia.append(" and d.ic_pyme = p.ic_pyme   ");
			qrySentencia.append(" and d.ic_moneda = m.ic_moneda ");
			qrySentencia.append(" and d.ic_estatus_docto  = ed.ic_estatus_docto ");
			qrySentencia.append(" and d.ic_documento  in( "+documentos+") ");
			qrySentencia.append(" and d.ic_if  = ?  ");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();


      nombreArchivo = strDirectorioTemp+"DocumentosCambioEstatus"+fecha+".csv";
			writer  = new OutputStreamWriter(new FileOutputStream(nombreArchivo),"Cp1252");
      out   = new BufferedWriter(writer);

				out.write("	Nombre de la EPO " +",");
				out.write("	Nombre del Proveedor " +",");
				out.write("	N�mero de Proveedor " +",");
				out.write("	N�mero de Documento " +",");
				out.write("	Fecha de Emisi�n " +",");
				out.write("	Fecha de Vencimiento " +",");
				out.write("	Moneda " +",");
				out.write("	Monto " +",");
				out.write("	Porcentaje de Descuento " +",");
				out.write("	Monto a Descontar " +",");
				out.write("	Estatus " +",");
				out.write("	Referencia " +",");
				out.write("	Fecha de Recepci�n de Bienes y Servicios " +",");
				out.write("	Tipo de Compra " +",");
				out.write("	Clasificador por Objeto del Gasto " +",");
				out.write("	Plazo M�ximo " +",");
				out.write("\r\n");

				int numero =0;
				while (rs.next()) {

					String nombreEPO = rs.getString("nombreEPO")==null?"":rs.getString("nombreEPO").replace(',',' ');
					String nombrePYME = rs.getString("nombrePYME")==null?"":rs.getString("nombrePYME").replace(',',' ');
					String noProveedor = rs.getString("noProveedor")==null?"":rs.getString("noProveedor").replace(',',' ');
					String noDocumento = rs.getString("noDocumento")==null?"":rs.getString("noDocumento").replace(',',' ');
					String fechaEmision = rs.getString("fechaEmision")==null?"":rs.getString("fechaEmision");
					String fechaVencimiento = rs.getString("fechaVencimiento")==null?"":rs.getString("fechaVencimiento");
					String moneda = rs.getString("moneda")==null?"":rs.getString("moneda").replace(',',' ');
					String monto = rs.getString("monto")==null?"":rs.getString("monto").replace(',',' ');
					String porcentajeDescuento =  rs.getString("porcentajeDescuento")==null?"":rs.getString("porcentajeDescuento");
					String montoDescontar  = rs.getString("montoDescontar")==null?"":rs.getString("montoDescontar").replace(',',' ');
					String estatus = rs.getString("estatus")==null?"":rs.getString("estatus").replace(',',' ');
					String referencia  = rs.getString("referencia")==null?"":rs.getString("referencia").replace(',',' ');
					String fechaRecepcion = rs.getString("fechaRecepcion")==null?"":rs.getString("fechaRecepcion");
					String tipoCompra =rs.getString("tipoCompra")==null?"":rs.getString("tipoCompra");
					String clasificador = rs.getString("clasificador")==null?"":rs.getString("clasificador");
					String plazoMaximo = rs.getString("plazoMaximo")==null?"":rs.getString("plazoMaximo");
					String tipoCompraDescripcion ="";
					if(tipoCompra.equals("L")){  tipoCompraDescripcion  = "Licitacion"; 		}
					if(tipoCompra.equals("I")){  tipoCompraDescripcion  = "Invitacion"; 		}
					if(tipoCompra.equals("C")){  tipoCompraDescripcion  = "CAAS CAAS delegado"; 	}
					if(tipoCompra.equals("D")){  tipoCompraDescripcion  = "Directa  Directa en el Extranjero Entidades Ampliacion Art. 52 y pago a Notarios"; 	}

					out.write(nombreEPO+",");
					out.write(nombrePYME+",");
					out.write(noProveedor+",");
					out.write(noDocumento+",");
					out.write(fechaEmision+",");
					out.write(fechaVencimiento+",");
					out.write(moneda+",");
					out.write("\"$"+Comunes.formatoDecimal(monto,2,true)+"\",");
					out.write(porcentajeDescuento+"%"+",");
					out.write("\"$"+Comunes.formatoDecimal(montoDescontar,2,true)+"\",");
					out.write(estatus+",");
					out.write(referencia+",");
					out.write(fechaRecepcion+",");
					out.write(tipoCompraDescripcion+",");
					out.write(clasificador+",");
					out.write(plazoMaximo+",");
					out.write("\r\n");
					numero++;
				}
				if(numero==0){
					nombreArchivo ="";
				}


		}catch(Exception e){
			log.error("CorreoprocesoCambioEstatus (Exception) " + e);
			throw new AppException("CorreoprocesoCambioEstatus(Exception) ", e);
		}finally{
			if (rs 	!= null) try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) try { ps.close(); 	} catch(SQLException e) {}
			if (out 	!= null )  try { out.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
			//log.info("archivo.getName"+archivo.getName());
		log.info("CorreoprocesoCambioEstatus (S)");
		//return archivo.getName();
		return nombreArchivo;
	}

/**
	 *metodo que valida el archivo  de los correos a cargar
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param archivo
	 */

	 public String  archivoCorreosNotiIF(String archivo) throws AppException{

		log.info("archivoCorreosNotiIF(E)");

		boolean ok = true;
		int noLinea = 0;
		String  mensaje = "";

		try{

			if(!archivo.equals("")){
				List LineaArchivo = new VectorTokenizer(archivo, "\n").getValuesVector();
				for(int i = 0; i < LineaArchivo.size()-1; i++) 	{
					noLinea = i+1;
					if(LineaArchivo.get(i)!=null){
						List camposLineaArchivo = new VectorTokenizer((String)LineaArchivo.get(i), "|").getValuesVector();
						for(int x = 0; x < camposLineaArchivo.size(); x++) 	{
							String campo  = (camposLineaArchivo.size()>= 1)?camposLineaArchivo.get(x)==null?"":((String)camposLineaArchivo.get(x)).trim():"";
						if (!this.isEmail(campo)){
							mensaje +="Error en la linea: "+noLinea+", el campo ["+campo+"] no es un email valido."+"\n";
						}

						}
					} //if
				}//for
			}//if


		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new AppException("Error archivoCorreosNotiIF ", e);
		}finally{
			log.info("archivoCorreosNotiIF(S)");
		}
		return mensaje;
	}



	private boolean isEmail(String correo){
	Pattern pat = null;
	Matcher mat = null;
	pat = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	mat = pat.matcher(correo);

	if (mat.find()) {
		System.out.println("[" + mat.group() + "]");
		return true;
	}else{
		return false;
	}
}

/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param nombreArchivo
	 * @param archivo
	 * @param usuario
	 * @param notificacion
	 * @param intermediario
	 */
	public List  cargaCorreosNotiIF(String intermediario, String notificacion, String usuario, String archivo, String nombreArchivo)  throws AppException{

		log.info("cargaCorreosNotiIF(E)");
		int nocorreos = 0;
		StringBuffer mensaje = new StringBuffer();
		StringBuffer qrySentencia = new StringBuffer();
		List datos = new ArrayList();
		PreparedStatement ps = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;

		try{
			con.conexionDB();

			//registra el cambio de estatus
			qrySentencia = new StringBuffer();
			qrySentencia.append(" delete  from  com_email_noti_if ");
			qrySentencia.append(" where ic_if  = ?  ");
			qrySentencia.append(" AND CG_NOTIFICAR   = ?  ");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,Integer.parseInt(intermediario) );
			ps.setString(2,notificacion );
			ps.executeUpdate();
			ps.close();

			if(!archivo.equals("")){
				List LineaArchivo = new VectorTokenizer(archivo, "\n").getValuesVector();
				for(int i = 0; i < LineaArchivo.size()-1; i++) 	{
					if(LineaArchivo.get(i)!=null){
						List camposLineaArchivo = new VectorTokenizer((String)LineaArchivo.get(i), "|").getValuesVector();
						for(int x = 0; x < camposLineaArchivo.size(); x++) 	{
							String campo  = (camposLineaArchivo.size()>= 1)?camposLineaArchivo.get(x)==null?"":((String)camposLineaArchivo.get(x)).trim():"";
							mensaje.append(campo+"|");
							nocorreos++;
						}
					} //if
				}//for
			}//if

			 /// insert
			String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());
			String	hora	 = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			//registra el cambio de estatus
			qrySentencia = new StringBuffer();
			qrySentencia.append(" INSERT INTO  com_email_noti_if ");
			qrySentencia.append(" (ic_email_noti_if , cg_email_noti_if, cg_notificar, cg_usuario, df_alta, ic_if ) ");
			qrySentencia.append(" values(SEQ_com_email_noti_if.NEXTVAL,?,?,? ,sysdate, ?) ");
			log.debug("qrySentencia "+qrySentencia.toString ());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1, mensaje.toString());
			ps.setString(2, notificacion);
			ps.setString(3, usuario);
			ps.setString(4, intermediario);
			ps.executeUpdate();
			ps.close();

			datos.add(nombreArchivo);
			datos.add(String.valueOf(nocorreos));
			datos.add(fecha);
			datos.add(hora);
			datos.add(usuario);

		}catch(Exception e){
			resultado = false;
			e.printStackTrace();
			throw new AppException("Error al guardar los correo de  Notificaciones IF", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}

			log.info("cargaCorreosNotiIF(S)");
		}
			return datos;
	}
/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param notificacion
	 * @param intermediario
	 */

	public String   ultimoArchNotIF(String intermediario, String notificacion )  throws AppException{

		log.info("ultimoArchNotIF(E)");

		StringBuffer qrySentencia = new StringBuffer();
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		String correos  ="";
		try{
			con.conexionDB();


			qrySentencia = new StringBuffer();
			qrySentencia.append(" select cg_email_noti_if as correos  from com_email_noti_if ");
			qrySentencia.append(" where ic_email_noti_if =  ");
			qrySentencia.append(" ( SELECT MAX  (ic_email_noti_if)  ");
			qrySentencia.append(" from com_email_noti_if ");
		  if(!intermediario.equals("") && !notificacion.equals("") )  {
					qrySentencia.append(" where  cg_notificar ='"+notificacion+"'");
					qrySentencia.append(" and ic_if = "+intermediario);
			}
			if(intermediario.equals("") && !notificacion.equals("") )  {
					qrySentencia.append(" where  cg_notificar ='"+notificacion+"'");
			}
			if(!intermediario.equals("") && notificacion.equals("") )  {
					qrySentencia.append(" where ic_if = "+intermediario);
			 }
			qrySentencia.append(" ) ");


			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
			correos  = rs.getString("correos")==null?"":rs.getString("correos").replace(',',' ');
			}
			rs.close();
			ps.close();


		}catch(Exception e){
			resultado = false;
			e.printStackTrace();
			throw new AppException("Error  obtener el ultimo archivo cargado ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("ultimoArchNotIF(S)");
		}
			return correos;
	}

/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param notificacion
	 * @param intermediario
	 */
	public String   EliminarNotIF(String intermediario, String notificacion )  throws AppException{

		log.info("EliminarNotIF(E)");
		StringBuffer qrySentencia = new StringBuffer();
		PreparedStatement ps = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		String elimino ="";
		try{
			con.conexionDB();

			//registra el cambio de estatus
			qrySentencia = new StringBuffer();
			qrySentencia.append(" delete  from  com_email_noti_if ");
			qrySentencia.append(" where ic_if  = ?  ");
			qrySentencia.append(" AND CG_NOTIFICAR   = ?  ");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,Integer.parseInt(intermediario) );
			ps.setString(2,notificacion );
			ps.executeUpdate();
			ps.close();

			elimino ="E";

		}catch(Exception e){
			resultado = false;
			e.printStackTrace();
			throw new AppException("Error se eliminar� los destinatarios cargados para el IF seleccionado ", e);
		}finally{

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("EliminarNotIF(S)");
		}
		return elimino;
	}

/**
	 * proceso para enviar correo con los documentos operados del Dia
	 * a los Intermediarios Financieros parametrizados para Notificar Operaciones del d�a	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param ruta
	 */
public String  procesoOperacionDia (String ruta)	throws AppException	{
		log.info("procesoOperacionDia (E)");
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		String respuesta ="", correos = "", ic_if ="";

		try {
			con.conexionDB();
			//se obtienes los correos de los If parametrizados para Notificar Operaciones del d�a
			qrySentencia = new StringBuffer();
			qrySentencia.append("select ic_if, replace(cg_email_noti_if, '|', ',') as cg_email_noti_if  from com_email_noti_if ");
			qrySentencia.append(" where cg_notificar  = 'OD' ");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				correos =rs.getString("cg_email_noti_if")==null?"":rs.getString("cg_email_noti_if");
				ic_if =rs.getString("ic_if")==null?"":rs.getString("ic_if");

					HashMap archivoAdjunto 		 = new HashMap();
					Correo correo = new Correo();
					ArrayList listaDeImagenes = null;
					ArrayList listaDeArchivos = new ArrayList();
					String	dia	 = new java.text.SimpleDateFormat("dd").format(new java.util.Date());
					String	mes	 = new java.text.SimpleDateFormat("MM").format(new java.util.Date());
					String	anio	 = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
					String  nombreMes ="";
					if(mes.equals("01")){ nombreMes = "Enero"; }
					if(mes.equals("02")){ nombreMes = "Febrero"; }
					if(mes.equals("03")){ nombreMes = "Marzo"; }
					if(mes.equals("04")){ nombreMes = "Abril"; }
					if(mes.equals("05")){ nombreMes = "Mayo"; }
					if(mes.equals("06")){ nombreMes = "Junio"; }
					if(mes.equals("07")){ nombreMes = "Julio"; }
					if(mes.equals("08")){ nombreMes = "Agosto"; }
					if(mes.equals("09")){ nombreMes = "Septiembre"; }
					if(mes.equals("10")){ nombreMes = "Octubre"; }
					if(mes.equals("11")){ nombreMes = "Noviembre"; }
					if(mes.equals("12")){ nombreMes = "Diciembre"; }

					String strDirectorioTemp = ruta;
					String strDirectorioFisico = strDirectorioTemp+"/00tmp/13descuento/";
					//metodo que regresa el archivo
					System.out.println("ic_if  "+ic_if);
					String nombreArchivo = CorreoOperacionesDia(ic_if,  strDirectorioFisico);
					System.out.println("nombreArchivo---->  "+nombreArchivo);
					if(!nombreArchivo.equals("")) {
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );

						String mensajeCorreo 	=" Relaci�n de los documentos Operados del D�a , "+dia+" de "+nombreMes+" del "+anio;
						listaDeArchivos.add(archivoAdjunto);

						String de ="cadenas@nafin.gob.mx";
						String tituloMensaje ="OPERACIONES DEL DIA, "+dia+"-"+mes+"-"+anio;
						String para = correos;

					try { //Si el env�o del correo falla se ignora.
						correo.enviaCorreoConDatosAdjuntos(de,para,"",tituloMensaje,mensajeCorreo,listaDeImagenes,listaDeArchivos);

					} catch(Throwable t) {
						log.error("Error al enviar correo " + t.getMessage());
					}
				}
			}
			rs.close();
			ps.close();



		}catch(Exception e){
			resultado = false;
			log.error("procesoOperacionDia (Exception) " + e);
			throw new AppException("procesoOperacionDia(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("procesoOperacionDia (S)");

		return respuesta;
	}


/**
	 * metodo para formar el archivo a enviar al Intermediario Financiero
	 * parametrizado en Notificar Operaciones del d�a
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param ic_if
	 * @param strDirectorioTemp
	 * @param documentos
	 */
	private String  CorreoOperacionesDia (String ic_if, String strDirectorioTemp  )	throws AppException	{

		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		BufferedWriter 	out 								= null;
		String nombreArchivo = "";
		OutputStreamWriter writer = null;
		log.info("CorreoOperacionesDia (E)");

		String	fechaActual	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

		log.debug("ic_if "+ic_if);
		log.debug("fecha "+fechaActual);
		try {
			con.conexionDB();

			// Obtengo ls registros  operados  del Dia
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') ||' '|| e.cg_razon_social ) as EPO ,  ");
			qrySentencia.append(" TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') as fecha_not,  ");
			qrySentencia.append(" a3.cc_acuse as acuseNoti, ");
			qrySentencia.append(" (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') ||' '|| p.cg_razon_social) as pyme, ");
			qrySentencia.append(" d.ig_numero_docto as numero_docto, ");
			qrySentencia.append(" TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision, ");
			qrySentencia.append(" TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento, ");
			qrySentencia.append(" m.cd_nombre as moneda, ");
			qrySentencia.append(" tf.cg_nombre as tipo_factoraje, ");
			qrySentencia.append(" d.fn_monto as monto,  ");
			qrySentencia.append(" d.fn_porc_anticipo as porcen_descuento, ");
			qrySentencia.append(" nvl(d.fn_monto,0) - nvl(d.fn_monto_dscto, 0) as recu_garantia , ");
			qrySentencia.append(" d.fn_monto_dscto as monto_descuento  ");
			qrySentencia.append("  FROM com_documento d, ");
			qrySentencia.append("  com_docto_seleccionado ds, ");
			qrySentencia.append("  comcat_epo e, ");
			qrySentencia.append("  com_acuse3 a3, ");
			qrySentencia.append("  comcat_pyme p, ");
			qrySentencia.append("  comcat_moneda m, ");
			qrySentencia.append("  comrel_if_epo ie, ");
			qrySentencia.append("  com_solicitud s, ");
			qrySentencia.append("  comcat_tipo_factoraje tf ");
			qrySentencia.append("  WHERE d.ic_documento = ds.ic_documento ");
			qrySentencia.append("  AND d.cs_dscto_especial != 'C' ");
			qrySentencia.append("  AND ds.ic_documento = s.ic_documento ");
			qrySentencia.append("  AND s.cc_acuse = a3.cc_acuse ");
			qrySentencia.append("  AND ds.ic_if = ie.ic_if ");
			qrySentencia.append("  AND ds.ic_epo = ie.ic_epo ");
			qrySentencia.append("  AND ie.cs_vobo_nafin = 'S' ");
			qrySentencia.append("  AND d.ic_epo = e.ic_epo ");
			qrySentencia.append("  AND e.cs_habilitado = 'S' ");
			qrySentencia.append("  AND d.ic_pyme = p.ic_pyme ");
			qrySentencia.append("  AND d.ic_moneda = m.ic_moneda ");
			qrySentencia.append("  AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
			qrySentencia.append("  and d.ic_estatus_docto =4 ");
			qrySentencia.append(" AND a3.df_fecha_hora BETWEEN TO_DATE ('" + fechaActual + " 00:00','dd/mm/yyyy hh24:mi')   ");
			qrySentencia.append(" AND TO_DATE ('" + fechaActual + " 23:59','dd/mm/yyyy hh24:mi') ");
			qrySentencia.append("  and ie.ic_if = ? ");

			log.debug("qrySentencia.toString() "+qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();


				String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());
				/*nombreArchivo = strDirectorioTemp+"OperacionesDelDia"+fecha+".csv";
				File file = new File(nombreArchivo);
				out = new BufferedWriter(new FileWriter(file));
				*/
			 nombreArchivo = strDirectorioTemp+"OperacionesDelDia"+fecha+".csv";
			 writer  = new OutputStreamWriter(new FileOutputStream(nombreArchivo),"Cp1252");
			 out   = new BufferedWriter(writer);


				out.write("	EPO " +",");
				out.write("	Fecha de notificaci�n " +",");
				out.write("	Acuse de notificaci�n " +",");
				out.write("	Nombre del proveedor " +",");
				out.write("	N�mero de Documento " +",");
				out.write("	Fecha de Emisi�n " +",");
				out.write("	Fecha de Vencimiento " +",");
				out.write("	Moneda " +",");
				out.write("	Tipo de Factoraje " +",");
				out.write("	Monto " +",");
				out.write("	Porcentaje de Descuento " +",");
				out.write("	Recurso Garantia " +",");
				out.write("	Monto Descuento " +",");
				out.write("\r\n");

				int numero = 0;
				while (rs.next()) {

					String nombreEPO = rs.getString("EPO")==null?"":rs.getString("EPO").replace(',',' ');
					String fechaNoti = rs.getString("fecha_not")==null?"":rs.getString("fecha_not").replace(',',' ');
					String acuseNoti = rs.getString("acuseNoti")==null?"":rs.getString("acuseNoti").replace(',',' ');
					String proveedor = rs.getString("pyme")==null?"":rs.getString("pyme").replace(',',' ');
					String documento = rs.getString("numero_docto")==null?"":rs.getString("numero_docto");
					String fechaEmision = rs.getString("fecha_emision")==null?"":rs.getString("fecha_emision");
					String fechaVenc = rs.getString("fecha_vencimiento")==null?"":rs.getString("fecha_vencimiento").replace(',',' ');
					String moneda = rs.getString("moneda")==null?"":rs.getString("moneda").replace(',',' ');
					String factoraje =  rs.getString("tipo_factoraje")==null?"":rs.getString("tipo_factoraje");
					String monto  = rs.getString("monto")==null?"":rs.getString("monto").replace(',',' ');
					String porDescuento = rs.getString("porcen_descuento")==null?"":rs.getString("porcen_descuento").replace(',',' ');
					String recurso  = rs.getString("recu_garantia")==null?"":rs.getString("recu_garantia").replace(',',' ');
					String montoDescuento = rs.getString("monto_descuento")==null?"":rs.getString("monto_descuento");

					out.write(nombreEPO+",");
					out.write(fechaNoti+",");
					out.write(acuseNoti+",");
					out.write(proveedor+",");
					out.write(documento+",");
					out.write(fechaEmision+",");
					out.write(fechaVenc+",");
					out.write(moneda+",");
					out.write(factoraje+",");
					out.write("\"$"+Comunes.formatoDecimal(monto,2,true)+"\",");
					out.write(porDescuento+"%"+",");
					out.write("\"$"+Comunes.formatoDecimal(recurso,2,true)+"\",");
					out.write("\"$"+Comunes.formatoDecimal(montoDescuento,2,true)+"\",");
					out.write("\r\n");
					numero++;
				}
				if(numero==0){
					nombreArchivo ="";
				}


		}catch(Exception e){
			log.error("CorreoOperacionesDia (Exception) " + e);
			throw new AppException("CorreoOperacionesDia(Exception) ", e);
		}finally{
			if (rs 	!= null) try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) try { ps.close(); 	} catch(SQLException e) {}
			if (out 	!= null )  try { out.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("CorreoOperacionesDia (S)");
		return nombreArchivo;
	}



	/**informa el numero de proceso para la carga Masiva de Documentos
	 * descuento electronico
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param sNoCliente
	 */
	public int noProcesoCM() throws NafinException {
		log.info("noProcesoCM (E)");
		AccesoDB con = new AccesoDB();
		int NumProceso=0;
			try{
				con.conexionDB();

				String query = "SELECT SEQ_COMTMP_PROC_DOCTO_IC_C_D.nextval from dual";

				ResultSet rs = con.queryDB(query);
				if (rs.next()) {
					NumProceso = rs.getInt(1);
				}
				rs.close();
				con.cierraStatement();

			}	catch(Exception e) {
				log.error("Exception en noProcesoCM. "+e);
				throw new NafinException("SIST0001");
			}
			finally {
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			}
			log.info("noProcesoCM (S)");
		return NumProceso;
	}

	/**
	 * regresal porcentaje de avance que tiene  la carga de documentos
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param NumProceso
	 */
	public List  porcentajeProcesoCarga(int NumProceso, String  NumLineas) throws NafinException {
	log.info("porcentajeProcesoCarga (E)");
	AccesoDB con = new AccesoDB();
	String  EstatusProceso="";
	int NumProcesados =0;
	double Porcentaje = 0;
	double NumeroLineas 	= Double.parseDouble(NumLineas);
	double ImageSize = 1;

	List registros = new ArrayList();

	try{

			con.conexionDB();
			String query =
			" SELECT cg_estatus_proc, NVL(ig_num_procesados,0)  " +
			" FROM BIT_CARGA_DOCTO " +
			" WHERE ic_carga_docto = " + NumProceso ;
		System.out.println("Obtencion del status del proceso:" +query);
			ResultSet rs = con.queryDB(query);
			if(rs.next()) {
				EstatusProceso = rs.getString(1);
				NumProcesados  = rs.getInt(2);
			}
		rs.close();
		con.cierraStatement();
		System.out.println("ESTATUS PROCESO JSP:" + EstatusProceso);
		System.out.println("NUMERO PROCESADOS JSP:" + NumProcesados);
		/*-- Obtencion del porcentaje de avance --*/
		Porcentaje = (double) ( NumProcesados * 100 ) / NumeroLineas ;
		ImageSize = (double) 380 * (Porcentaje/100);


		registros.add(EstatusProceso );
		registros.add(String.valueOf(NumProcesados) );
		registros.add(String.valueOf(NumeroLineas) );
		registros.add(String.valueOf(Porcentaje) );
		registros.add(String.valueOf(ImageSize) );


		}	catch(Exception e) {
			log.error("Exception en porcentajeProcesoCarga. "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		log.info("porcentajeProcesoCarga (S)");
	return registros;
	}

	public List getDoctosCambioImporteTmpExt(String sProcesoCambio, boolean limitado) throws AppException {
	AccesoDB con = new AccesoDB();
	List lstDoctosMant = new ArrayList();

		try{
			con.conexionDB();
			HashMap mp = new HashMap();

			String query =
				" SELECT   pe.cg_pyme_epo_interno, d.ig_numero_docto, fn_monto_anterior,"   +
				"          fn_monto_nuevo, ct_causa,"   +
				"          DECODE ("   +
				"             ed1.ic_estatus_docto,"   +
				"             23, ed3.cd_descripcion,"   +
				"             ed1.cd_descripcion"   +
				"          ),"   +
				"          ed2.cd_descripcion, TO_CHAR (i.df_fecha_venc_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_nueva, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_p_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_fecha_venc_p_nueva, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (i.df_entrega, 'DD/MM/YYYY'),"   +
				//"        i.cg_tipo_compra, "   +
				"          tc.cg_descripcion, "   +
				"          i.cg_clave_presupuestaria,"   +
				"          i.cg_periodo"   +
				"     FROM com_documento d,"   +
				"          comtmp_cambio_importe i,"   +
				"          comrel_pyme_epo pe,"   +
				"          comcat_estatus_docto ed1,"   +
				"          comcat_estatus_docto ed2,"   +
				"          comcat_estatus_docto ed3"   +
				"          ,comcat_tipo_compra tc"   +
				"    WHERE i.ic_proceso_cambio = "+sProcesoCambio+
				"      AND d.ic_documento = i.ic_documento"   +
				"      AND d.ic_pyme = pe.ic_pyme"   +
				"      AND d.ic_estatus_docto = ed1.ic_estatus_docto"   +
				"      AND i.ic_estatus_docto = ed2.ic_estatus_docto"   +
				"	 	 AND i.cg_tipo_compra = tc.cc_clave(+)"+
				"      AND ed3.ic_estatus_docto = 2"   +
				"      AND d.ic_epo = pe.ic_epo"   +
        (limitado?"      AND rownum <1001 ":"")   +
				" ORDER BY d.ic_moneda"  ;
			ResultSet rs = con.queryDB(query);
			while (rs.next()) {
				mp = new HashMap();

				mp.put("CVE_PROVEEDOR",rs.getString(1).trim());
				mp.put("NUM_DOCTO",rs.getString(2).trim());
				mp.put("MONTO_ANTERIOR",(rs.getString(3)==null?"":rs.getString(3).trim()));
				mp.put("MONTO_NUEVO",(rs.getString(4)==null?"":rs.getString(4).trim()));
				mp.put("CAUSA",rs.getString(5).trim());
				mp.put("ESTATUS_ANTERIOR",rs.getString(6).trim());
				mp.put("ESTATUS_NUEVO",rs.getString(7).trim());
				mp.put("FECVENC_ANTERIOR",(rs.getString(8)==null?"":rs.getString(8).trim()));
				mp.put("FECVENC_NUEVA",(rs.getString(9)==null?"":rs.getString(9).trim()));
				mp.put("FECVENC_PROVANTE",(rs.getString(10)==null?"":rs.getString(10).trim()));
				mp.put("FECVENC_PROVNUEVA",(rs.getString(11)==null?"":rs.getString(11).trim()));
//>>============================================================================ FODEA 050 - 2008
				mp.put("FEC_ENTREGA",(rs.getString(12)==null?"":rs.getString(12).trim()));
				mp.put("TIPO_COMPRA",(rs.getString(13)==null?"":rs.getString(13).trim()));
				mp.put("CVE_PRESUPUESTAL",(rs.getString(14)==null?"":rs.getString(14).trim()));
				mp.put("PERIODO",(rs.getString(15)==null?"":rs.getString(15).trim()));
//==============================================================================<<
				lstDoctosMant.add(mp);
			}
			con.cierraStatement();
		}
		catch(Throwable e) {
			log.error("Exception en getDoctosCambioImporteTmpExt. "+e);
			throw new AppException("Error al obtener documentos mantenimiento", e);
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return lstDoctosMant;
	}


	public List getDoctosCambioImporteExt(String no_acuse, boolean limitada) throws AppException {
		AccesoDB con = new AccesoDB();
		List lstDoctosMant = new ArrayList();
		HashMap mp = new HashMap();

		try{
			con.conexionDB();
			String query =
				" SELECT   pe.cg_pyme_epo_interno, d.ig_numero_docto, fn_monto_anterior,"   +
				"          fn_monto_nuevo, ct_cambio_motivo, ce.cd_descripcion AS cd_descripcion,"   +
				"          TO_CHAR (df_fecha_venc_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_nueva, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_p_anterior, 'DD/MM/YYYY'),"   +
				"          TO_CHAR (df_fecha_venc_p_nueva, 'DD/MM/YYYY'),"   +
				"        	 TO_CHAR (d.df_entrega, 'DD/MM/YYYY'),"+
				" 				tc.cg_descripcion,"+
				//"        	 d.cg_tipo_compra,"+
				"        	 d.cg_clave_presupuestaria,"+
				"        	 d.cg_periodo,"+
				"        	 c.cg_nombre_usuario"+
				"     FROM com_documento d,"   +
				"          comhis_cambio_estatus c,"   +
				"          comrel_pyme_epo pe,"   +
				"          comcat_cambio_estatus ce"   +
				"			  ,comcat_tipo_compra tc"+
				"    WHERE c.cc_acuse = '"+no_acuse+"'"+
				"      AND d.ic_documento = c.ic_documento"   +
				"      AND d.ic_pyme = pe.ic_pyme"   +
				"      AND c.ic_cambio_estatus = ce.ic_cambio_estatus"   +
				"      AND d.ic_epo = pe.ic_epo"   +
				"		 AND d.cg_tipo_compra = tc.cc_clave(+)"+
        		(limitada?"    AND rownum < 1001 ":"")+
				" ORDER BY d.ic_moneda"  ;
			ResultSet rs = con.queryDB(query);
			log.debug("QUERY getDoctosCambioImporte"+query);
			while (rs.next()) {
				mp = new HashMap();
				mp.put("CVE_PROVEEDOR",rs.getString(1).trim());
				mp.put("NUM_DOCTO",rs.getString(2).trim());
				mp.put("MONTO_ANTERIOR",(rs.getString(3)==null?"":rs.getString(3).trim()));
				mp.put("MONTO_NUEVO",(rs.getString(4)==null?"":rs.getString(4).trim()));
				mp.put("CAUSA",(rs.getString(5)==null?"":rs.getString(5).trim()));
				mp.put("CAMBIO_MOTIVO",rs.getString(6).trim());
				mp.put("FECVENC_ANTERIOR",(rs.getString(7)==null?"":rs.getString(7).trim()));
				mp.put("FECVENC_NUEVA",(rs.getString(8)==null?"":rs.getString(8).trim()));
				mp.put("FECVENC_PROVANTE",(rs.getString(9)==null?"":rs.getString(9).trim()));
				mp.put("FECVENC_PROVNUEVA",(rs.getString(10)==null?"":rs.getString(10).trim()));
				mp.put("FEC_ENTREGA",(rs.getString(11)==null?"":rs.getString(11).trim()));
				mp.put("TIPO_COMPRA",(rs.getString(12)==null?"":rs.getString(12).trim()));
				mp.put("CVE_PRESUPUESTAL",(rs.getString(13)==null?"":rs.getString(13).trim()));
				mp.put("PERIODO",(rs.getString(14)==null?"":rs.getString(14).trim()));
				mp.put("NOM_USUARIO",(rs.getString(15)==null?"":rs.getString(15).trim()));


				lstDoctosMant.add(mp);
			}
			con.cierraStatement();

		}catch(Throwable e) {
				log.error("Exception en getDoctosCambioImporte. "+e);
				throw new AppException("Error en la consulta de Mantenimieno de Doctos", e);
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return lstDoctosMant;
	}

	/**
	 * Obtiene dias inhabiles por a�o
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param con
	 * @param epo
	 */
	private String getDiasInhabilesXanio(String epo,AccesoDB con) throws NafinException {
	String sDiaMesInhabiles = "";
		try{
			String query = "select to_char(df_dia_inhabil,'dd/mm/yyyy') from comcat_dia_inhabil where df_dia_inhabil is not null "+
								" UNION "+
								" select to_char(df_dia_inhabil,'dd/mm/yyyy') from comrel_dia_inhabil_x_epo where df_dia_inhabil is not null and ic_epo = " + epo;
			ResultSet rs = con.queryDB(query);
			while(rs.next())
				sDiaMesInhabiles += rs.getString(1).trim()+";";

			con.cierraStatement();
		}
		catch(Exception e) {
			log.error("Exception en getDiasInhabilesXanio. "+e);
			throw new NafinException("SIST0001");
		}
	return sDiaMesInhabiles;
	}

	private void validaFechaVencimientoXanio(String fecha_venc,String sNoCliente,AccesoDB con)
		throws NafinException{
		log.info("CargaDocumentoBean::validaFechaVencimientoXanio (E)");
		try{
			Calendar dia_fv = new GregorianCalendar();
			java.util.Date fechaVenc=Comunes.parseDate(fecha_venc);
			String sLineaFecha = getDiasInhabilesXanio(sNoCliente,con);
			if (sLineaFecha.length() > 0) {
				StringTokenizer st = new StringTokenizer(sLineaFecha,";");
				String sDiaMesInhabil = "";
				while(st.hasMoreElements()) {
					sDiaMesInhabil = st.nextToken();
					//log.debug(sDiaMesInhabil);
					if(fecha_venc.equals(sDiaMesInhabil)) {
						throw new NafinException("DIST0010");
					}
				}
			}
			/* Validacion de que la fecha de Vencimiento que no sea Sabado o Domingo. */
			dia_fv.setTime(fechaVenc);
			int no_dia_semana=dia_fv.get(Calendar.DAY_OF_WEEK);
			if(no_dia_semana==7 || no_dia_semana==1) { // 7 Sabado y 1 Domingo.
				throw new NafinException("DIST0010");
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.error("CargaDocumentoBean::validaFechaVencimientoXanio Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			log.info("CargaDocumentoBean::validaFechaVencimientoXanio (S)");
		}

	}

	public boolean validaPymeXEpoBloqueda(String cveEpo, String cvePyme, String cveProducto) throws AppException{
		log.info("CargaDocumentoBean::validaPymeXEpoBloqueda(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean sibloqueada = false;
		try{
			con.conexionDB();


			strSQL = "SELECT cs_bloqueo " +
						"  FROM comrel_bloqueo_pyme_x_producto " +
						" WHERE ic_pyme = ? AND ic_producto_nafin = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1,Long.parseLong(cvePyme));
			ps.setLong(2,Long.parseLong(cveProducto));

			rs = ps.executeQuery();
			if(rs.next()){
				if("B".equals(rs.getString("cs_bloqueo"))){
					System.out.println("SI ENTRO A BLOQUEO");
					sibloqueada = true;
				}
			}
			rs.close();
			ps.close();

			if(!sibloqueada){
				strSQL = "SELECT cs_bloqueo " +
							"  FROM comrel_bloqueo_pyme_x_epo " +
							" WHERE ic_pyme = ? AND ic_epo = ? AND ic_producto_nafin = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1,Long.parseLong(cvePyme));
				ps.setLong(2,Long.parseLong(cveEpo));
				ps.setLong(3,Long.parseLong(cveProducto));

				rs = ps.executeQuery();
				if(rs.next()){
					if("B".equals(rs.getString("cs_bloqueo")))
						sibloqueada = true;
				}
				rs.close();
				ps.close();
			}

			return sibloqueada;
		}catch(Throwable t){
			throw new AppException("Error al consultar si la pyme esta bloqueda para la epo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CargaDocumentoBean::validaPymeXEpoBloqueda(S)");
		}
	}

	public boolean validaPymeXEpoBloqXNumProv(String numProvedor, String cveEPO, String cveProducto) throws AppException{
		log.info("CargaDocumentoBean::validaPymeXEpoBloqXNumProv(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean sibloqueada = false;
		try{
			con.conexionDB();
			String cvePyme = "";
			String cveEpo = "";

			strSQL = "SELECT ic_epo, ic_pyme " +
						"  FROM comrel_pyme_epo " +
						" WHERE cg_pyme_epo_interno = ? " +
						" AND ic_epo = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, numProvedor);
			ps.setLong(2, Long.parseLong(cveEPO));

			rs = ps.executeQuery();
			if(rs.next()){
				System.out.println(" si entro al primer query ");
				cveEpo = rs.getString("ic_epo");
				cvePyme = rs.getString("ic_pyme");
			}
			rs.close();
			ps.close();

			if(!"".equals(cveEpo) && !"".equals(cvePyme)){

				strSQL = "SELECT cs_bloqueo " +
						"  FROM comrel_bloqueo_pyme_x_producto " +
						" WHERE ic_pyme = ? AND ic_producto_nafin = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1,Long.parseLong(cvePyme));
				ps.setLong(2,Long.parseLong(cveProducto));

				rs = ps.executeQuery();
				if(rs.next()){
					if("B".equals(rs.getString("cs_bloqueo"))){
						System.out.println("SI ENTRO A BLOQUEO");
						sibloqueada = true;
					}
				}
				rs.close();
				ps.close();

				if(!sibloqueada){
					strSQL = "SELECT cs_bloqueo " +
							"  FROM comrel_bloqueo_pyme_x_epo " +
							" WHERE ic_pyme = ? AND ic_epo = ? AND ic_producto_nafin = ? ";

					ps = con.queryPrecompilado(strSQL);
					ps.setLong(1,Long.parseLong(cvePyme));
					ps.setLong(2,Long.parseLong(cveEpo));
					ps.setLong(3,Long.parseLong(cveProducto));

					rs = ps.executeQuery();
					if(rs.next()){
						if("B".equals(rs.getString("cs_bloqueo"))){
							System.out.println("SI ENTRO A BLOQUEO");
							sibloqueada = true;
						}
					}
					rs.close();
					ps.close();
				}
			}

			return sibloqueada;
		}catch(Throwable t){
			throw new AppException("Error al consultar si la pyme esta bloqueda para la epo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CargaDocumentoBean::validaPymeXEpoBloqXNumProv(S)");
		}
	}

	public String getDispersiones(String usuario, String password, String claveEPO, String fechaInicial, String fechaFinal){
		AccesoDB con = null;//new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String strSQL = "";
		StringBuffer sbArchivo = new StringBuffer();
		String nombreEpo = "";

		String tituloMensaje = "Consulta getDispersiones";
		String descMsgError = "";
		try{
			//con.conexionDB();


			//**********************************Validaci�n de parametros:*****************************
			//try {

			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD");
			}

			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ");
			}

			if (claveEPO == null || claveEPO.equals("")) {
				throw new Exception("El par�metro -Clave de EPO- debe ser especificado");
			}
			if (claveEPO != null && !claveEPO.equals("")) {
				Integer.parseInt(claveEPO);

				ps = con.queryPrecompilado("select cg_razon_social from comcat_epo where ic_epo = ? ");
				ps.setLong(1,Long.parseLong(claveEPO));
				rs = ps.executeQuery();
				if(rs.next()){
					nombreEpo = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				}
				rs.close();
				ps.close();
			}

			if (fechaInicial != null && !fechaInicial.equals("")) {
				Comunes.parseDate(fechaInicial);
			}else{
				fechaInicial	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			}
			if (fechaFinal != null && !fechaFinal.equals("")) {
				Comunes.parseDate(fechaFinal);
			}else{
				fechaFinal	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			}

			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

			log.info("getAvisosNotificacionWS::Validando claveEPO/usuario");

			if (!cuentasEPO.contains(usuario)) {
				throw new Exception("El usuario no es de la epo especificada");
			}

			log.info("getAvisosNotificacionWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(usuario, password)) {
				throw new Exception("Fall� la autenticaci�n del usuario");
			}

			//INICIA CODIGO PARA CONSULTA
			strSQL = "SELECT   doc.ic_documento clave_documento,   " +
							"			 pym.cg_rfc rfc_pyme, " +
							"         doc.ig_numero_docto num_documento,  " +
							"         to_char(doc.DF_FECHA_VENC,'dd/mm/yyyy') fec_vencimiento, " +
							"         doc.FN_MONTO importe_doc,  " +
							"         doc.fn_monto_dscto AS importe_dscto, " +
							"         doc.IC_ESTATUS_DOCTO estatus_doc, " +
							"         ced.CD_DESCRIPCION nombre_estatus, " +
							"         doc.CT_REFERENCIA referencia, " +
							"         doc.CG_CAMPO1 campo1, " +
							"         doc.CG_CAMPO2 campo2, " +
							"         doc.CG_CAMPO3 campo3, " +
							"         doc.CG_CAMPO4 campo4, " +
						"         doc.CG_CAMPO5 campo5, " +
						"         ci.cg_rfc rfc_if,  " +
						"         pym.cg_rfc rfc_pyme, " +
						"         ff.ic_flujo_fondos, " +
						"         to_char(ff.df_registro, 'dd/mm/yyyy') fec_dispersion, " +
						"         eff.ic_estatus_ff estatus_ff,  " +
						"         eff.cd_descripcion nomb_estatus_ff " +
							"    FROM com_documento doc, " +
							"         com_docto_seleccionado sel, " +
							"         comcat_pyme pym, " +
						"         comcat_if ci, " +
						"         comcat_estatus_docto ced, " +
						"         int_flujo_fondos ff, " +
						"         comcat_estatus_ff eff, " +
							"         comrel_documento_ff dff, " +
						"         cfe_m_encabezado me  " +
						"   WHERE doc.ic_documento = sel.ic_documento(+)  " +
							"     AND doc.ic_pyme = pym.ic_pyme " +
						"     AND doc.ic_if = ci.ic_if(+) " +
						"     AND doc.ic_documento = dff.ic_documento(+) " +
						"     AND dff.IC_FLUJO_FONDOS = ff.IC_FLUJO_FONDOS(+) " +
						"     AND ff.ic_flujo_fondos = me.idoperacion_cen_i(+) " +
						"     AND me.status_cen_i = eff.ic_estatus_ff(+)  " +
						"     AND dff.ic_producto_nafin(+) = 1 " +
							"		AND doc.ic_estatus_docto = ced.ic_estatus_docto " +
						"		AND doc.ic_epo = ? " +
						"     AND doc.df_fecha_venc >= to_date(?,'dd/mm/yyyy') " +
						"     AND doc.df_fecha_VENC < to_date(?,'dd/mm/yyyy')+1 " +
						"     AND doc.ic_estatus_docto  not in ( 5, 6, 7 )   "+
							" ORDER BY doc.cs_dscto_especial DESC, doc.ic_documento ";
			try{
				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(claveEPO));
				ps.setString(2, fechaInicial);
				ps.setString(3, fechaFinal);
				rs = ps.executeQuery();
			} catch(Throwable t) {
				if(rs!=null)rs.close();
				if(ps!=null)ps.close();
				throw new Exception ("Error al compilar la consulta de documentos. ", t);
			}
			int i = 0;

			while(rs.next()){

				String ic_flujo_fondos = rs.getString("ic_flujo_fondos")==null?"":rs.getString("ic_flujo_fondos");
				String fec_dispersion = rs.getString("fec_dispersion")==null?"":rs.getString("fec_dispersion");
				String rfc_pyme = rs.getString("rfc_pyme")==null?"":rs.getString("rfc_pyme");
				String rfc_if = rs.getString("rfc_if")==null?"":rs.getString("rfc_if");
				String estatus_ff = rs.getString("estatus_ff")==null?"":rs.getString("estatus_ff");
				String nomb_estatus_ff = rs.getString("nomb_estatus_ff")==null?"":rs.getString("nomb_estatus_ff");
				String rfc_pyme_int = rs.getString("rfc_pyme")==null?"":rs.getString("rfc_pyme");
				String num_documento = rs.getString("num_documento")==null?"":rs.getString("num_documento");
				String fec_vencimiento = rs.getString("fec_vencimiento")==null?"":rs.getString("fec_vencimiento");
				String importe_doc = rs.getString("importe_doc")==null?"":rs.getString("importe_doc");
				String importe_dscto = rs.getString("importe_dscto")==null?"":rs.getString("importe_dscto");
				String estatus_doc = rs.getString("estatus_doc")==null?"":rs.getString("estatus_doc");
				String nombre_estatus = rs.getString("nombre_estatus")==null?"":rs.getString("nombre_estatus");
				String referencia = rs.getString("referencia")==null?"":rs.getString("referencia");
				String campo1 = rs.getString("campo1")==null?"":rs.getString("campo1");
				String campo2 = rs.getString("campo2")==null?"":rs.getString("campo2");
				String campo3 = rs.getString("campo3")==null?"":rs.getString("campo3");
				String campo4 = rs.getString("campo4")==null?"":rs.getString("campo4");
				String campo5 = rs.getString("campo5")==null?"":rs.getString("campo5");
				String clave_documento = rs.getString("clave_documento")==null?"":rs.getString("clave_documento");

					if(i==0){
						sbArchivo.append(
							"RFC proveedor, RFC IF, Numero Documento, Fecha Vencimiento, " +
							"Fecha Dispersion, Importe Documento, Importe Descuento, Clave Estatus, " +
						"Estatus Documento, Clave Estatus Dispersion, Estatus Dispersion, Referencia, " +
						"Campo Adicional 1, Campo Adicional 2, Campo Adicional 3, Campo Adicional 4, Campo Adicional 5, "+
						"Clave Documento \n");
					}

					sbArchivo.append(
							rfc_pyme_int.replace(',',' ')+", " +
							rfc_if.replace(',',' ')+", " +
							num_documento+", " +
							fec_vencimiento.replace(',',' ')+", " +
							fec_dispersion+", " +
							Comunes.formatoDecimal(importe_doc,2,false)+", " +
							Comunes.formatoDecimal(importe_dscto,2,false)+", " +
							estatus_doc+", " +
							nombre_estatus+", " +
							estatus_ff+", " +
							nomb_estatus_ff+", " +
							referencia+", " +
							campo1.replace(',',' ')+", " +
							campo2.replace(',',' ')+", " +
							campo3.replace(',',' ')+", " +
							campo4.replace(',',' ')+", " +
						campo5.replace(',',' ')+", " +
						clave_documento+"\n");
					i++;
			}
			rs.close();
			ps.close();

			if(i==0){
				sbArchivo.append("No se encontraron documentos \n");
			}

			log.debug("sbArchivo ====== "+sbArchivo);
		}catch(Exception t){
			t.printStackTrace();
			sbArchivo.append("ERROR. " + t.getMessage());
			descMsgError = t.getMessage();
			String error =
				"Error: " + t.getMessage() + "\n" +
				"claveEPO=" + claveEPO + "\n" +
				"usuario=" + usuario + "\n" +
				"password=********" + "\n" +
				"fechaInicial=" + fechaInicial + "\n" +
				"fechaFinal=" + fechaFinal + "\n";

				log.error(error);

			try { //Si el env�o del correo falla se ignora.
				String to = this.getCuentasWS(claveEPO);
				if (to != null) {

					String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
						"font-size: 10px; "+
						"font-weight: bold;"+
						"color: #FFFFFF;"+
						"background-color: #4d6188;"+
						"padding-top: 3px;"+
						"padding-right: 1px;"+
						"padding-bottom: 1px;"+
						"padding-left: 3px;"+
						"height: 25px;"+
						"border: 1px solid #1a3c54;'";

					String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
						"color:#000066; "+
						"padding-top:1px; "+
						"padding-right:2px; "+
						"padding-bottom:1px; "+
						"padding-left:2px; "+
						"height:22px; "+
						"font-size:11px;'";

					String tabla1 = "<table border=\"1\">";
					tabla1 += "	<tr>" +
							 "		<td align=\"center\" "+styleEncabezados+">Parametros</td>" +
							 "		<td align=\"center\" "+styleEncabezados+">Valores</td>" +
							 "		<td align=\"center\" "+styleEncabezados+">Descripcion del Error</td>" +
							 "	</tr>";

					tabla1 += "	<tr>" +
						"		<td align=\"center\" "+style+">claveEPO</td>" +
						"		<td align=\"center\" "+style+">" + claveEPO + "</td>" +
						"		<td align=\"center\" "+style+" rowspan=5>" + descMsgError + "</td>" +
						"	</tr>";
					tabla1 += "	<tr>" +
						"		<td align=\"center\" "+style+">usuario</td>" +
						"		<td align=\"center\" "+style+">" + usuario + "</td>" +
						"	</tr>";
					tabla1 += "	<tr>" +
						"		<td align=\"center\" "+style+">password</td>" +
						"		<td align=\"center\" "+style+">********</td>" +
						"	</tr>";
					tabla1 += "	<tr>" +
						"		<td align=\"center\" "+style+">fechaInicial</td>" +
						"		<td align=\"center\" "+style+">" + fechaInicial + "</td>" +
						"	</tr>";
					tabla1 += "	<tr>" +
						"		<td align=\"center\" "+style+">fechaFinal</td>" +
						"		<td align=\"center\" "+style+">" + fechaFinal + "</td>" +
						"	</tr>";
					tabla1 += "</table><br><br>";

					String textoCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p> Nombre EPO: "+nombreEpo+"<br><br></p>"+tabla1+"</form>";

					Correo correo = new Correo();
					correo.enviarTextoHTML("No_response@nafin.gob.mx", to, tituloMensaje, textoCorreo);
				}
			} catch(Throwable t2) {
				t2.printStackTrace();
				log.error("Error al enviar correo " + t2.getMessage());
			}

			return error;

		} finally {
			if (con!=null && con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return sbArchivo.toString();
	}

	/**
	 *  Metodo para validar la duplicidad del documentos a cargar
	 *  Cadena Productiva ,   Fecha de Emisi�n (dd/mm/aaaa),  Moneda,  Proveedor ,  Monto Documento (nuevo campo).
	 *  Fodea 038-2014 	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param monto
	 * @param ic_pyme
	 * @param ic_moneda
	 * @param df_fecha_docto
	 * @param ic_epo
	 */

	public String  validaDoctoDuplicado(String ic_epo, String df_fecha_docto, String ic_moneda, String ic_pyme,  String monto, String ic_proc_docto ) throws AppException {
		log.info("validaDoctoDuplicado (E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String valida="N";
		ResultSet 	      rs	   =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List lVarBind		= new ArrayList();

		try{

			con.conexionDB();

			qrySentencia.append( " select  count(*) as total  "+
										"	from  comtmp_proc_docto   "+
										"	where ic_epo  = ?  "+
										"	and to_char(df_fecha_docto,'dd/mm/yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'dd/mm/yyyy')  "+
										"	and ic_moneda = ?  "+
										"	and ic_pyme  =  ?  "+
										"	and fn_monto =  ?  "+
										"  and ic_estatus_docto  not in (1, 5 )  ");   //No Negociable, Baja

			qrySentencia.append( "   union all " );

			qrySentencia.append( " select  count(*) as total  "+
										"	from  com_documento   "+
										"	where ic_epo  = ?  "+
										"	and to_char(df_fecha_docto,'dd/mm/yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'dd/mm/yyyy')  "+
										"	and ic_moneda = ?  "+
										"	and ic_pyme  =  ?  "+
										"	and fn_monto =  ?  "+
										"  and ic_estatus_docto  not in (1, 5 )  ");   //No Negociable, Baja

			lVarBind		= new ArrayList();
			lVarBind.add(ic_epo);
			lVarBind.add(df_fecha_docto);
			lVarBind.add(ic_moneda);
			lVarBind.add(ic_pyme);
			lVarBind.add(monto);

			lVarBind.add(ic_epo);
			lVarBind.add(df_fecha_docto);
			lVarBind.add(ic_moneda);
			lVarBind.add(ic_pyme);
			lVarBind.add(monto);


			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
			int total = 0;
			while(rs.next()){
				total += rs.getInt(1);
			}
			rs.close();
			ps.close();

			if(total>0) {
				valida = "S";
			}

			if ( valida.equals("N") ) {
			 qrySentencia = new StringBuffer();
			 lVarBind		= new ArrayList();
				qrySentencia.append( " select count(1)   as total "+
								" from  comtmp_proc_docto    "+
								" where ic_epo  = ?    "+
								" and to_char(df_fecha_docto,'dd/mm/yyyy') = to_char(to_date(?,'dd/mm/yyyy'),'dd/mm/yyyy')  "+
								" and ic_moneda =?  "+
								" and ic_pyme  =  ?  "+
								" and fn_monto =  ?  "+
								" and ic_carga_docto = ?  "+
								" and ic_estatus_docto  not in (1 )  ");   //No Negociable

				lVarBind		= new ArrayList();
				lVarBind.add(ic_epo);
				lVarBind.add(df_fecha_docto);
				lVarBind.add(ic_moneda);
				lVarBind.add(ic_pyme);
				lVarBind.add(monto);
				lVarBind.add(ic_proc_docto);

				log.debug("qrySentencia.toString() "+qrySentencia.toString());
				log.debug("varBind "+lVarBind);

				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();
				if(rs.next()){
					if(rs.getInt(1)>0) {
						valida = "S";
					}
				}
				rs.close();
				ps.close();

			}


		}catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  validaDoctoDuplicado " +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("Error en  validaDoctoDuplicado (S)" );
      }

		return valida;
	}


	/**
	 * metodo que inserta a la aquellos documentos que estasn duplicados
	 * Fodea 038-2014
	 * @throws com.netro.exception.NafinException
	 * @param con
	 * @param sStatusDocto
	 * @param nombreUsuario
	 * @param usuario
	 * @param ic_pyme
	 * @param ic_epo
	 * @param fn_monto
	 * @param ic_moneda
	 * @param df_fecha_docto
	 * @param ig_numero_docto
	 * @param ic_documento
	 */
	public void insertaBitacoraDuplicadosCa(  String ig_numero_docto, String df_fecha_docto,
		String ic_moneda, String fn_monto, String ic_epo, String ic_pyme, String usuario, String nombreUsuario, String sStatusDocto, String proceso ) throws AppException {
		log.info("insertaBitacoraDuplicadosCa (E)");
		AccesoDB con = new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();
		List lVarBind		= new ArrayList();

		try{
			con.conexionDB();

			qrySentencia = new StringBuffer();
			qrySentencia.append( " INSERT INTO COM_BIT_DUPLICADOS (  "+
					" IC_DOCUMENTO ,  IG_NUMERO_DOCTO ,  DF_FECHA_DOCTO,   IC_MONEDA  , FN_MONTO ,  "+
					" IC_EPO ,  IC_PYME ,  IC_USUARIO ,  CG_NOMBRE_USUARIO  ,  DF_FECHA_ALERT , IC_ESTATUS_DOCTO  ) "+
					" VALUES ( SEQ_COM_BIT_DUPLICADOS.NEXTVAL ,  ? ,  TO_DATE(?,'DD/MM/YYYY'),   ?  , ? , ? ,  ? ,  ? ,  ?  , Sysdate , ?  ) ");

				lVarBind		= new ArrayList();
				lVarBind.add(ig_numero_docto);
				lVarBind.add(df_fecha_docto);
				lVarBind.add(ic_moneda);
				lVarBind.add(fn_monto);
				lVarBind.add(ic_epo);
				lVarBind.add(ic_pyme);
				lVarBind.add(usuario);
				lVarBind.add(nombreUsuario);
				lVarBind.add(sStatusDocto);
				con.ejecutaUpdateDB(qrySentencia.toString(), lVarBind);

				log.debug("qrySentencia.toString(). "+qrySentencia.toString() +"---"+lVarBind);


		}catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  insertaBitacoraDuplicadosCa " +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info(" insertaBitacoraDuplicadosCa (S)" );
      }
	}


  private void enviaCorreoDoctosDuplicados(AccesoDB con, String cveEpo, String cgReceipt)throws AppException
  {
    log.info("enviaCorreoDoctosDuplicados(E)");
    PreparedStatement ps = null;
    ResultSet rs = null;
    String strSQL = "";
    Correo correo = new Correo();
    String msgCorreo = "";
    CRutaFisicaWS cRuta = new CRutaFisicaWS();
    int contReg = 0;
    try
    {
      //con.conexionDB();
      String ruta = cRuta.getRutaFisica()+"00tmp/13descuento/";
      StringBuffer contenidoArchivo = new StringBuffer();

      strSQL = "SELECT d.IG_NUMERO_DOCTO, ce.cg_razon_social nombreepo, " +
            "       to_char(d.DF_FECHA_DOCTO, 'dd/mm/yyyy') DF_FECHA_DOCTO, " +
            "       cm.cd_nombre, cp.cg_razon_social nombrepyme, d.fn_monto " +
            "  FROM com_documento d, comcat_epo ce, comcat_moneda cm, comcat_pyme cp " + //, com_doctos_err_pub_ws cer " +
            " WHERE d.ic_epo = ce.ic_epo  " +
            " AND d.ic_moneda = cm.ic_moneda " +
            " AND d.ic_pyme = cp.ic_pyme " +
            //" AND d.ic_epo = cer.ic_epo " +
            //" AND d.ig_numero_docto = cer.ig_numero_docto " +
            " AND d.df_alta >= trunc(SYSDATE)  " +
            " AND d.df_alta < SYSDATE + 1 " +
            " AND d.ic_epo = ? " +
            " ANd d.ic_estatus_docto = 33 " +
            " AND d.cc_acuse = ? ";
            //" AND cer.cg_receipt = ? ";

      //System.out.println("strSQL === "+strSQL);

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(cveEpo));
      ps.setString(2,cgReceipt );
      rs = ps.executeQuery();
      CreaArchivo archivo = new CreaArchivo();
      String nombreArchivoPDF = archivo.nombreArchivo()+".pdf";
      contenidoArchivo.append("No. Documento, Cadena Productiva, Fecha de Emisi�n, Moneda, Proveedor, Monto\n");

      ComunesPDF pdfDoc = null;
      /*new ComunesPDF(2, ruta + nombreArchivoPDF, "", false, true, true);
      pdfDoc.setEncabezado("MEXICO", "0", "", "", "", "nafin.gif", cRuta.getRutaFisica(), "");

      //pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
      pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
      pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
      pdfDoc.addText("Documentos publicados con estatus \"Pendiente Duplicado\"","formas",ComunesPDF.CENTER);
      pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
      pdfDoc.setTable(6, 100);

      pdfDoc.setCell("No. Docto.","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Cadena Productiva","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emision","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Proveedor","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);*/

      while(rs.next())
      {
        if(contReg==0)
        {
          pdfDoc = new ComunesPDF(2, ruta + nombreArchivoPDF, "", false, true, true);
          pdfDoc.setEncabezado("MEXICO", "0", "", "", "", "nafin.gif", cRuta.getRutaFisica(), "");

          //pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
          pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);
          pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
          pdfDoc.addText("Documentos publicados con estatus \"Pendiente Duplicado\"","formas",ComunesPDF.CENTER);
          pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
          pdfDoc.setTable(6, 100);

          pdfDoc.setCell("No. Docto.","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Cadena Productiva","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Fecha de Emision","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Proveedor","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
        }
        contenidoArchivo.append(rs.getString("IG_NUMERO_DOCTO"));
        contenidoArchivo.append(","+(rs.getString("nombreepo")).replaceAll(","," "));
        contenidoArchivo.append(","+rs.getString("DF_FECHA_DOCTO"));
        contenidoArchivo.append(","+rs.getString("cd_nombre"));
        contenidoArchivo.append(","+(rs.getString("nombrepyme")).replaceAll(","," "));
        contenidoArchivo.append(","+rs.getString("fn_monto"));
        contenidoArchivo.append("\n");


        pdfDoc.setCell(rs.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("nombreepo"),"formas",ComunesPDF.LEFT);
        pdfDoc.setCell(rs.getString("DF_FECHA_DOCTO"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("cd_nombre"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("nombrepyme"),"formas",ComunesPDF.LEFT);
        pdfDoc.setCell("$"+ Comunes.formatoDecimal(rs.getString("fn_monto"),2),"formas",ComunesPDF.RIGHT);
        contReg++;
      }

      rs.close();
      ps.close();


      if(contReg>0){

        pdfDoc.addTable();
        pdfDoc.endDocument();

        String nombreArchivo = "";
        if (!archivo.make(contenidoArchivo.toString(), ruta, ".csv")) {

        } else {
          nombreArchivo = archivo.nombre;
        }

        //CREAR ARCHIVO EN MEMORIA
        /*ByteArrayOutputStream boStream = new ByteArrayOutputStream();
        Writer w = new OutputStreamWriter(boStream);
        w.write(contenidoArchivo.toString());
        byte[] bytes = boStream.toByteArray();*/

        msgCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>";
        msgCorreo += "<p>Estimado(s):</p>"+
                    "Se hace de su conocimiento que los documentos enlistados en los archivos adjuntos fueron publicados " +
                    "con estatus <b>Pendiente Duplicado</b> por presentar posible duplicidad en los siguientes campos, respecto a " +
                    "documentos previamente registrados: Cadena Productiva, Fecha de Emisi�n (dd/mm/aaaa), Moneda, Proveedor y Monto.<br><br>" +
                    "Por favor ingrese a alguna de las siguientes rutas del sitio de Cadenas Productivas en http://www.nafin.com para validarlos:<br> " +
                    "Descuento / Capturas / Manto. de Documentos. <br>"+
                    "Descuento / Capturas / Mantenimiento Masivo de Documentos<br><br> "+
                    "Atentamente <br>"+
                    "Nacional Financiera, S.N.C. </form>";

        ArrayList listaDeArchivos = new ArrayList();
        HashMap archivoAdjunto = new HashMap();
        archivoAdjunto.put("FILE_FULL_PATH", ruta+nombreArchivo );
        listaDeArchivos.add(archivoAdjunto);
        archivoAdjunto = new HashMap();
        archivoAdjunto.put("FILE_FULL_PATH", ruta+nombreArchivoPDF );
        listaDeArchivos.add(archivoAdjunto);

        correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx",getMailDocDuplicados(cveEpo),this.getCuentasWS(cveEpo), "NAFIN-Documentos Pendientes Duplicados",msgCorreo,new ArrayList(),listaDeArchivos);
      }
    }catch(Exception e){
      e.printStackTrace();
      throw new AppException("Error al consultar los documento publicados como pendientes de revisar", e);
    }finally
    {
      log.info("enviaCorreoDoctosDuplicados(S)");
    }
  }

	private boolean publicarDocDuplicados(String sNoEPO) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bPubDocDuplicados=false;
		try{
			con.conexionDB();
			String query =
					" SELECT COUNT (*)"   +
					"   FROM comrel_producto_epo"   +
					"  WHERE CG_VALIDA_DUPLIC_DOCTO = 'S'" +
					"   AND ic_epo = ? "+
					"   AND ic_producto_nafin = ? ";
		
			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(sNoEPO));
			ps.setInt(2, 1);
			rs = ps.executeQuery();
			if (rs.next())
				bPubDocDuplicados=(rs.getInt(1)>0)?true:false;
			else
				bPubDocDuplicados=false;
		
			rs.close();
			ps.close();
			
			return bPubDocDuplicados;
		
		}catch(Exception e) {
			log.error("Exception en publicarDocDuplicados. "+e);
			throw new AppException("Error al validar si la epo valida Doctos Duplicados");
		}
		finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	private String getMailDocDuplicados(String sNoEPO) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String mailDocDuplicados = "";
		try{
			con.conexionDB();
			String query =
					" SELECT CG_EMAIL_DUPLIC_DOCTO "   +
					"   FROM comrel_producto_epo"   +
					"  WHERE CG_VALIDA_DUPLIC_DOCTO = 'S'" +
					"   AND ic_epo = ? "+
					"   AND ic_producto_nafin = ? ";				
		
			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(sNoEPO));
			ps.setInt(2, 1);
			rs = ps.executeQuery();
			if (rs.next())
				mailDocDuplicados = (rs.getString(1));
		
		
			rs.close();
			ps.close();
		
			return mailDocDuplicados;
		
		}catch(Exception e) {
			log.error("Exception en getMailDocDuplicados. "+e);
		throw new AppException("Error al obtener el correo al que se enviaran los doctos de la epo que Valida Doctos Duplicados");
		}
		finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
	
	
	
	/**
	 * Fodea 017-2015
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param strDirectorioTemp 
	 */
	public String  retornoEstatusNegociableAut (String strDirectorioTemp, String estatus )	throws AppException	{
		log.info("retornoEstatusNegociableAut (E)");
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		String respuesta ="", ic_documento ="",  ic_estatus_docto ="", documentos ="",  
									 cambio =" Retorno a Negociable por proceso autom�tico" , envioCorreo ="N", correos ="";
		
		HashMap archivoAdjunto 		 = new HashMap();
		Correo correo = new Correo();
		ArrayList listaDeImagenes = null;
		ArrayList listaDeArchivos = new ArrayList();
		StringBuffer contenido = new StringBuffer();
		String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());		
	   String	hora	 = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	
		try {
			con.conexionDB();
			
			log.debug("estatus  "+estatus );      
			
			if (  ( estatus.equals("24") || estatus.equals("3") )   ||  (  estatus==null  || estatus.equals("") )   ){ 
			
				qrySentencia.append(" SELECT   d.ic_documento as ic_documento ,  "+									 
										  " d.ic_estatus_docto as  ic_estatus_docto "+									  
										  " from  com_documento d "+									 
										  " WHERE d.cs_dscto_especial != 'C'   ");
										  
				if(estatus==null  || estatus.equals("")) { 
					qrySentencia.append(" and  d.ic_estatus_docto in ( 24, 3) "); 
					
				}else if (estatus.equals("24") || estatus.equals("3") ) {
					qrySentencia.append(" and  d.ic_estatus_docto = "+estatus); 
				}			
							
				log.debug("qrySentencia  "+qrySentencia.toString() )  ;										
				
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();
				int	total = 0;
				while (rs.next()) {  
					total++;
					if(total<=1000) {
						documentos += rs.getString("ic_documento")==null?"":rs.getString("ic_documento")+",";
					}
					
					ic_estatus_docto = rs.getString("ic_estatus_docto")==null?"":rs.getString("ic_estatus_docto");
					ic_documento = rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
							
				// modifico el estatus de "Selecci�n Pyme  y En proceso de Autorizacion IF" a "Negociable"
					qrySentencia = new StringBuffer();
					qrySentencia.append(" update com_documento ");
					qrySentencia.append(" set  ic_estatus_docto = 2 ");
					qrySentencia.append(" where ic_estatus_docto in (24,3) ");
					qrySentencia.append(" and ic_documento  = ? ");
					ps = con.queryPrecompilado(qrySentencia.toString());
					ps.setInt(1, Integer.parseInt(ic_documento));
					ps.executeUpdate();
					ps.close();
					
				//	log.debug("modifico el estatus de Selecci�n Pyme  y En proceso de Autorizacion IF a Negociable  "+qrySentencia.toString() )  ;
	
					//registra el cambio de estatus
					qrySentencia = new StringBuffer();				
					qrySentencia.append(" INSERT INTO  COMHIS_CAMBIO_ESTATUS ");
					qrySentencia.append(" (dc_fecha_cambio , ic_documento, ic_cambio_estatus, ct_cambio_motivo ) ");
					qrySentencia.append(" values(sysdate, ? , ? , ?) ");
				
					if (ic_estatus_docto.equals("24")){
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setInt(1, Integer.parseInt(ic_documento));
						ps.setInt(2, 41);
						ps.setString(3, cambio);
						ps.executeUpdate();
						ps.close();
					} else if (ic_estatus_docto.equals("3")){
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setInt(1, Integer.parseInt(ic_documento));
						ps.setInt(2, 2);
						ps.setString(3, cambio);
						ps.executeUpdate();
						ps.close();
					}
				//	log.debug("registra el cambio de estatus   "+qrySentencia.toString() )  ;	
					envioCorreo ="S";
					
				}
				rs.close();
				ps.close();
			
			}else  {
				
				log.debug(" el cambio es solo para  estatus  Seleccion Pyme(3)  y En proceso de Autorizacion IF (3) el estatus que esta enviando es incorecto ");  
				
			}
			
			if(	envioCorreo.equals("S")	 && !documentos.equals("")){
			
				if(!documentos.equals("")){
					int tamanio2 =	documentos.length();
					String valor =  documentos.substring(tamanio2-1, tamanio2);
					if(valor.equals(",")){
						documentos =documentos.substring(0,tamanio2-1);
					}
				}
				
				//obtengo correos parametrizados en la pantalla  Admin Nafin/ Administraci�n / Parametrizaci�n / Otros Par�metros  	
				qrySentencia = new StringBuffer();
				qrySentencia.append("select cg_email_notificaciones_nafin as correos "+
				" from comcat_producto_nafin "+
				" where  ic_producto_nafin = 1  ");
				
				log.debug("correos parametrizados "+qrySentencia.toString() )  ;	
				
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();
				if (rs.next()) {
					correos =rs.getString("correos")==null?"":rs.getString("correos");
				}
				rs.close();
				ps.close();
				
				log.debug("correos "+correos )  ;
				
				//Valido que los correos sean correcto  
				String[] correosV;
				String delimiter = ",";
				correosV = correos.split(delimiter);
				int correoNoValido =0;
				for(int i=0;i<correosV.length;i++){  
				
					if(!Comunes.validaEmail(correosV[i].toString() ) ){
						correoNoValido++;
					}
				}     
				 
				log.debug("correoNoValido "+correoNoValido )  ;  
				log.debug("strDirectorioTemp "+strDirectorioTemp )  ;  
		
				if( correoNoValido==0)  {
									
					String strDirectorioFisico = strDirectorioTemp+"00tmp/13descuento/";
					//metodo que regresa el archivo
					String nombreArchivo = correoRetornoEstatusNegociableAut(documentos, strDirectorioFisico  );
	
					if(!nombreArchivo.equals("")) {
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );
					
						contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">"+	
							" Estimado(a) usuario(a) <BR><BR>"+  
							" La(s) siguiente(s) operacion(es) ha(n) sido retornadas (s) al estatus Negociable. <BR> "+
							"	Retornado(s) el d�a "+fecha+" a las "+hora+" horas."+
							" <BR><BR>Se anexa archivo con el detalle de los documentos."+
							" <BR><BR><BR><BR>ATENTAMENTE"+
							" <BR> Nacional Financiera, S.N.C. "+  
							"</span></span>");
							
						listaDeArchivos.add(archivoAdjunto);
						String de ="cadenas@nafin.gob.mx";
						String tituloMensaje ="Documentos Retornados a Negociable";
							
							
						try { //Si el env�o del correo falla se ignora.
							correo.enviaCorreoConDatosAdjuntos(de,correos,"",tituloMensaje,contenido.toString(),listaDeImagenes,listaDeArchivos);
						} catch(Throwable t) {
							log.error("Error al enviar correo " + t.getMessage());
						}
					}
				}else  {
					log.debug("El correo electr�nico no se env�a x formato icorrecto");
				}//if( correoNoValido==0)  {			
			}

		}catch(Exception e){
			resultado = false;
			log.error("retornoEstatusNegociableAut (Exception) " + e);
			throw new AppException("retornoEstatusNegociableAut(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("retornoEstatusNegociableAut (S)");

		return respuesta;
	}
	
	/**
	 * Fodea 017-2015
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param strDirectorioTemp
	 * @param documentos
	 */
	public String  correoRetornoEstatusNegociableAut (String documentos, String strDirectorioTemp   )	throws AppException	{
		log.info("correoRetornoEstatusNegociableAut (E)");
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer csv = new StringBuffer();
		String  nombreArchivo = "",  nombre_epo ="",  nombre_pyme ="",  nombre_if ="",
									  acuse ="",  num_documento ="",  moneda ="",  monto ="",  
									  porcentaje ="",  monto_descuento ="",   tasa_aceptada ="",   importe_interes ="",
									  importe_recibir ="",  factoraje ="", estatus ="";
									
		String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());	
		String	fecha1	 = new java.text.SimpleDateFormat("ddMMyyyy").format(new java.util.Date());		
		String	hora	 = new java.text.SimpleDateFormat("HHmm").format(new java.util.Date()); 
	
		java.io.File   	f 									= null;
		BufferedReader 	in									= null;
		BufferedWriter 	out 								= null;
		java.io.File 		archivo 							= null;
		OutputStreamWriter writer = null;
		
		try {
			con.conexionDB();

			nombreArchivo = strDirectorioTemp+"Negociables_"+fecha1+"_"+hora+".csv";
			writer  = new OutputStreamWriter(new FileOutputStream(nombreArchivo),"Cp1252");
			out   = new BufferedWriter(writer);
			 
			out.write("	Nombre  EPO " +",");
			out.write("	Nombre Pyme " +",");
			out.write("	Nombre IF " +",");
			out.write("	N�mero Acuse " +",");
			out.write("	Numero Docto " +",");
			out.write("	Moneda  " +",");
			out.write("	Monto Docto " +",");
			out.write("	Porcentaje " +",");
			out.write("	Monto Descuento " +",");
			out.write("	Tasa Aceptada " +",");
			out.write("	Importe inter�s " +",");
			out.write("	Importe a recibir " +",");
			out.write("	Tipo Factoraje " +",");
			out.write("	Fecha de Cambio " +",");
			out.write("	Estatus cambio " +",");
			
			out.write("\r\n");

			
			qrySentencia.append(" SELECT   d.ic_documento as ic_documeno ,  "+
									  " e.cg_razon_social as nombre_epo, "+
									  " p.cg_razon_social as nombre_pyme, "+
									  " i.cg_razon_social as nombre_if, "+
									  " d.cc_acuse as acuse, "+
									  " d.ig_numero_docto as num_documento, "+
									  " m.cd_nombre as moneda, "+
									  " d.fn_monto as monto, "+
									  " d.fn_porc_anticipo as porcentaje, "+
									  " d.fn_monto_dscto as monto_descuento, "+
									  " ds.in_tasa_aceptada as tasa_aceptada, "+
									  " ds.in_importe_interes as importe_interes, "+
									  " ds.in_importe_recibir as importe_recibir, "+
									  " tf.cg_nombre as factoraje , "+
									  " es.cd_descripcion   estatus   "+
									  
									  " from  com_documento d, "+
									  " com_docto_seleccionado ds, "+
									  " comcat_epo e  , "+
									  " comcat_pyme p,  "+
									  " comcat_if i, "+
									  " comcat_moneda m, "+
									  " comcat_tipo_factoraje tf,  "+
									  " comcat_estatus_docto es  "+
									  
									  " WHERE   d.ic_documento = ds.ic_documento "+									 
									  " AND d.cs_dscto_especial != 'C'   "+
									  " and d.ic_epo = e.ic_epo "+
									  " and d.ic_pyme = p.ic_pyme  "+
									  " and ds.ic_if = i.ic_if  "+
									  " and m.ic_moneda = d.ic_moneda "+
									  " and d.cs_dscto_especial = tf.cc_tipo_factoraje   "+
									  " and d.ic_estatus_docto = es.ic_estatus_docto  "+
									 " and d.ic_documento in ( "+ documentos + ") " +
									 
										" order by e.cg_razon_social asc ");
										
			log.debug("dcoumentos ===  "+qrySentencia.toString() )  ;										
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			
			while (rs.next()) {
			
				nombre_epo = rs.getString("nombre_epo")==null?"":rs.getString("nombre_epo");
				nombre_pyme = rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme");
				nombre_if = rs.getString("nombre_if")==null?"":rs.getString("nombre_if");
				acuse = rs.getString("acuse")==null?"":rs.getString("acuse");
				num_documento = rs.getString("num_documento")==null?"":rs.getString("num_documento");
				moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
				monto = rs.getString("monto")==null?"":rs.getString("monto");
				porcentaje = rs.getString("porcentaje")==null?"":rs.getString("porcentaje");
				monto_descuento = rs.getString("monto_descuento")==null?"":rs.getString("monto_descuento");
				tasa_aceptada = rs.getString("tasa_aceptada")==null?"":rs.getString("tasa_aceptada");
				importe_interes = rs.getString("importe_interes")==null?"":rs.getString("importe_interes");
				importe_recibir = rs.getString("importe_recibir")==null?"":rs.getString("importe_recibir");
				factoraje = rs.getString("factoraje")==null?"":rs.getString("factoraje");
				estatus = rs.getString("estatus")==null?"":rs.getString("estatus");
				
				
				out.write(nombre_epo.replace(',',' ')+",");
				out.write(nombre_pyme.replace(',',' ')+",");
				out.write(nombre_if.replace(',',' ')+",");
				out.write(acuse+",");
				out.write(num_documento+",");
				out.write(moneda+",");
				out.write(monto.replace(',',' ')+",");
				out.write(porcentaje.replace(',',' ')+",");
				out.write(monto_descuento.replace(',',' ')+",");
				out.write(tasa_aceptada.replace(',',' ')+",");
				out.write(importe_interes.replace(',',' ')+",");
				out.write(importe_recibir.replace(',',' ')+",");
				out.write(factoraje+",");
				out.write(fecha+",");				
				out.write(estatus+"");
				out.write("\r\n");	 		
				
			}
			rs.close();
			ps.close();
			
			out.close();	
			

		}catch(Exception e){
			resultado = false;
			log.error("correoRetornoEstatusNegociableAut (Exception) " + e);
			throw new AppException("correoRetornoEstatusNegociableAut(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("correoRetornoEstatusNegociableAut (S)");

		return nombreArchivo;
	}
	
	
	
	
	
	
	
	
	
	
	/** metodo para obtener el  beneficiario  con respecto a la epo y referencia   
	 * se parametriza en Admin nafin /Administraci�n / Parametrizaci�n / Relaciones EPO-IF
	 *  2017_003
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param sNoEPO
	 * @param referencia
	 */
    private String geticIFxReferencia(int sNoEPO, String referencia ) throws AppException {
	
	log.info ("geticIFxReferencia e ");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	String icIf = "";
	
	try{
	    
	    con.conexionDB();
	    	    
	    String query =" select  ic_if  from   comrel_if_epo " + 
			  " where ic_epo = ?   " + 
			  " and CG_REFERENCIA =   ?  ";				
		
	    ps = con.queryPrecompilado(query);
	    ps.setInt(1, sNoEPO);
	    ps.setString(2, referencia);
	    rs = ps.executeQuery();
	    if (rs.next()){
		icIf = (rs.getString(1));
	    }
	    rs.close();
	    ps.close();
		
	    return icIf;
		
	}catch(Exception e) {
	    log.error("Exception en geticIFxReferencia. "+e);
	    throw new AppException("Error al obtener el ic_if de la epo  pemex con respecto a la referencia  ");
	} finally {
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}
	
    }	
    
    
    /**
     *metodo para saber si existe una referencia parametrizada en  relacion epo if 
     * se parametriza en Admin nafin /Administraci�n / Parametrizaci�n / Relaciones EPO-IF
     * 2017_003
     * @param sNoEPO
     * @param referencia
     * @return
     * @throws AppException
     */
    private int getExisteicIFxReferencia(int sNoEPO, String referencia ) throws AppException {
	
	log.info ("getExisteicIFxReferencia e ");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	int existe = 0;
	
	try{
	    
	    con.conexionDB();
	    	    
	    String query =" select  count(*)  from   comrel_if_epo " + 
			  " where ic_epo = ?   " + 
			  " and CG_REFERENCIA =   ?  ";	  			
		
	    ps = con.queryPrecompilado(query);
	    ps.setInt(1, sNoEPO);
	    ps.setString(2, referencia);
	    rs = ps.executeQuery();
	    if (rs.next()){
		existe = (rs.getInt(1));
	    }
	    rs.close();
	    ps.close();
		
	    return existe;
		
	}catch(Exception e) {
	    log.error("Exception en getExisteicIFxReferencia. "+e);
	    throw new AppException("Error al obtener el ic_if de la epo  pemex con respecto a la referencia  ");
	} finally {
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}	
    }
    
    
    
}// Fin del Bean