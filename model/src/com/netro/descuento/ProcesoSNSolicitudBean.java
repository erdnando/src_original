package com.netro.descuento;

import com.netro.exception.NafinException;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.VectorTokenizer;

@Stateless(name = "ProcesoSNSolicitudEJB" , mappedName = "ProcesoSNSolicitudEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcesoSNSolicitudBean implements ProcesoSNSolicitud {

	public Hashtable ohprocesarSolicitudes(String esDocumentos) throws NafinException {
	System.out.println("ohprocesarSolicitudes(E)");
	boolean lbOk = true, bOkDoctos = true;
	StringBuffer lsbError=new StringBuffer();
	int liNumLinea = 1;
	String lsMsgError = "", lsFolio = "", lsNumPrestamo = "", lsFechaOperacion = "", lsLinea="", lsQry="";
	Vector lvDatos = null;
	VectorTokenizer lvtValores = null;
	ResultSet lrsQry = null;
	Vector lvFolio = new Vector();
	Vector lvNumeroPrestamo = new Vector();
	Vector lvFecha = new Vector();
	Hashtable lhResultados = new Hashtable();
	AccesoDB lobdConexion = new AccesoDB();
	try{
		lobdConexion.conexionDB();
		StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
		while(lstDocto.hasMoreElements()) {
			lsLinea = lstDocto.nextToken();
			//System.out.println(lsLinea);
			lvtValores = new VectorTokenizer(lsLinea,"|");
			lvDatos = lvtValores.getValuesVector();
			lsMsgError = "Error en la linea: "+ liNumLinea + ", ";
			//System.out.println(lvDatos.size());
			
			if (lvDatos.size() < 3)	{
				bOkDoctos = lbOk = false;
				lsbError.append(lsMsgError +" no coincide con el layout. Por favor verifiquelo.\n\n");
			} else { // if lvDatos
				
				// 1. LEER CAMPOS
				
				lsFolio 				= lvDatos.get(0).toString();
				lsNumPrestamo 		= lvDatos.get(1).toString();
				lsFechaOperacion 	= lvDatos.get(2).toString();
				
				
				// 2. REVISAR QUE EL FOLIO CUMPLA CON EL FORMATO
				
				lsQry = "select ic_folio from com_solicitud where ic_folio = '" + lsFolio + "'";
				//System.out.println(lsQry);
				lrsQry = lobdConexion.queryDB(lsQry);
				if(! lrsQry.next()) {
					lbOk = false;
					lsbError.append(lsMsgError +" no existe la solicitud con el folio: "+lsFolio+".\n\n");
				}
				lobdConexion.cierraStatement();
				
				//buscamos la solicitud que sea En Proceso
				lsQry = "select ic_folio from com_solicitud where ic_folio = '"+lsFolio+"' and ic_estatus_solic = 2";
				//System.out.println(lsQry);
				lrsQry = lobdConexion.queryDB(lsQry);
				if(! lrsQry.next()) {
					lbOk = false;
					lsbError.append(lsMsgError +" la solicitud con el folio: "+lsFolio+" no se encuentra en estatus de En Proceso.\n\n");
				}
				lobdConexion.cierraStatement();
				
				
	         // 3. REVISAR QUE EL N�MERO DE PR�STAMO CUMPLA CON EL FORMATO
				if(lsNumPrestamo.length()    > 8) {
					
					lbOk = false;
					lsbError.append(lsMsgError + " el campo Numero de Prestamo sobrepasa la longitud permitida.\n\n");
					
				} else if ( !Comunes.esNumero(lsNumPrestamo)      ) {
					
		      	lbOk = false;
			   	lsbError.append(lsMsgError + " el campo Numero de Prestamo no es un numero.\n\n");
			   	
				} else {
					
					//buscamos que no exista el n�mero de prestamo en la base de datos
					lsQry = "select * from com_solicitud where ig_numero_prestamo = " + lsNumPrestamo;
					//System.out.println(lsQry);
					lrsQry = lobdConexion.queryDB(lsQry);
					if(lrsQry.next()) {
						lbOk = false;
						lsbError.append(lsMsgError +" el numero de prestamo: " + lsNumPrestamo + ", ya existe en la base de datos.\n\n");
					}
					lobdConexion.cierraStatement();
					
				}
				
				// 4. REVISAR QUE LA FECHA DE OPERACI�N CUMPLA CON EL FORMATO
				if(lsFechaOperacion.length() > 10) {
					
					lbOk = false;
					lsbError.append(lsMsgError + " el campo Fecha de Operacion sobrepasa la longitud permitida.\n\n");
					
				} else if ( !Comunes.checaFecha(lsFechaOperacion) ) {
					
		      	lbOk = false;
			   	lsbError.append(lsMsgError + " la Fecha de Operacion (" + lsFechaOperacion + ") no es valida.\n\n");
			   	
				}
				
				// 5. INSERTAR SOLICITUD
				if(lbOk) {
					bOkDoctos = bactualizaSolicitud(lsFechaOperacion, lsNumPrestamo, lsFolio, lobdConexion);
					//Agrega los valores a los vectores para Despliege en el jsp.
					lvFolio.add(lsFolio);
					lvNumeroPrestamo.add(lsNumPrestamo);
					lvFecha.add(lsFechaOperacion);
				} else {
					bOkDoctos = false;
				}
				
			}// else lvDatos
			liNumLinea++;
			// Vaciar los vectores
			lvDatos.removeAllElements();
			
		}// while mientras existan mas elementos.
		
		lhResultados.put("lbOk", new Boolean(bOkDoctos));
		lhResultados.put("lsbError", lsbError.toString());
		lhResultados.put("lvFolio", lvFolio);
		lhResultados.put("lvNumeroPrestamo", lvNumeroPrestamo);
		lhResultados.put("lvFecha", lvFecha);
		//Verificamos que no haya ning�n error en el procesamiento mediante la bandera ok
		//System.out.println("El valor de bOkDoctos: "+bOkDoctos);
		lobdConexion.terminaTransaccion(bOkDoctos);
		
	} catch (NafinException ne) {
		System.out.println("NafinException:ohprocesarSolicitudes(E)");
		throw ne;
	} catch (Exception e) {
		System.out.println("Exception en hprocesarDocumentos(). "+e);
		throw new NafinException("SIST0001");
	} finally {
		System.out.println("ohprocesarSolicitudes(S)");
		if (lobdConexion.hayConexionAbierta())
			lobdConexion.cierraConexionDB();
	}	
	return lhResultados;
	} // Fin Metodo hprocesarDocumentos
	
	
	private boolean bactualizaSolicitud(String lsFechaOperacion, String lsNumPrestamo, 
										String lsFolio, AccesoDB lobdConexion) throws NafinException {
	boolean bOkDoctos = true;
		try{
			String lsQry = " update com_solicitud " +
							"set df_operacion = TO_DATE('" + lsFechaOperacion + "','DD/MM/YYYY') " +
							", ig_numero_prestamo = " + lsNumPrestamo +
							", ic_estatus_solic = 3 " +
							", cg_tipo_busqueda = 'M' " +
							"where ic_folio = '" + lsFolio + "' and ic_estatus_solic = 2";
			System.out.println(lsQry);
			lobdConexion.ejecutaSQL(lsQry);
		} catch (SQLException sqle){
			bOkDoctos = false;
			throw new NafinException("DSCT0024");
		}
	return bOkDoctos;
	}
		
}// Fin del Bean