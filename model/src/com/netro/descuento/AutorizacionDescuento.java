/*************************************************************************************
*
* Nombre de Clase o de Archivo: AutorizacionDescuento
*
* Versi�n: 0.1
*
* Fecha Creaci�n: 22/01/2002
*
* Autor: Gilberto E. Aparicio
*
* Fecha Ult. Modificaci�n: 07/02/2002
*
* Descripci�n de Clase:
*  
*************************************************************************************/

package com.netro.descuento;

import com.nafin.descuento.ws.CuentasPymeIFWS;
import com.nafin.descuento.ws.DocumentoIFWS;
import com.nafin.descuento.ws.DocumentoIFWSV2;
import com.nafin.descuento.ws.ProcesoIFWSInfo;
import com.nafin.descuento.ws.ProcesoIFWSInfoV2;

import com.netro.exception.NafinException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;

@Remote
public interface AutorizacionDescuento {

	public void eliminarAmortTemp (String claveRegistroTemporal)
			throws NafinException;

	public void eliminarDatosTempExt (String numeroProcesoSolicitudExt)
		throws NafinException;

	public void eliminarDatosTempExtCargaMasiva (String numeroProceso)
		throws NafinException;

	public boolean esFactorajeVencido (String claveEpo)
		throws NafinException;

	public boolean esFactorajeDistribuido (String claveEpo)
		throws NafinException;

	public boolean faltanAmortTempPorCapturar (String claveRegistroTemporal,
			int numeroAmortCapturar)
		throws NafinException;

	public void generarSolicitudesExterna(String numeroProcesoSolicitudExt,
			String acuse, String claveUsuario,
			String montoDescuentoMN, String montoDescuentoDL,
			String montoMN, String montoDL)
		throws NafinException;

	public Hashtable generarSolicitudInterna(Vector claveDocumento, String acuse,
			String claveEpo, String emisor,
			String montoMN,String montoInteresMN,
			String montoDsctoMN, String montoDL, String montoInteresDL,
			String montoDsctoDL, String claveUsuario,
			String reciboElectronico, String tipoFondeo,
			String noCliente, String fechaCarga,
			String horaCarga, String horaCargaF, String nombreUsuario,
			String plazoMaxBO, String strDirectorioTemp, String strPais,  String iNoNafinElectronico, 
			String strNombre, String strLogo, String strDirectorioPublicacion)
		throws NafinException;

	public Vector getAmortizaciones (String folio)
		throws NafinException;

	public Vector getAmortsTemps (String numeroProcesoSolicitudExt)
		throws NafinException;

	public String [] getAmortTemp (String claveRegistroTemporal)
		throws NafinException;

	public String [] getCamposAdicionales(String claveEPO)
		throws NafinException;

	public String [] getCliente(String numeroSirac)
		throws NafinException;

	public Vector getDoctosProcesados (String acuse, String claveEPO, String claveIF)
		throws NafinException;

/*	public void setEnlaceAutomatico(String claveIF, String acuse, String contenidoArchivo)
		throws NafinException;*/

	public Vector getDoctosProcesar (String claveDocto[], String claveIF, String ic_epo)
		throws NafinException;

	public Vector getDoctosProcesarMasiva (String numProceso)
		throws NafinException;

	public Vector getDoctosSelecPyme(String clavePYME, String claveEPO,
			String claveIF, String claveDoctoTodos )
		throws NafinException;

	public String [] getEsquemaExterno(String esquemaExt)
		throws NafinException;

	public int getNumCamposAdicionales(String claveEPO)
		throws NafinException;

	public String getNumProcesoSolicitudExt()
		throws NafinException;

	public String getNumProcesoSolicitudesExt()
		throws NafinException;

	public Vector getSolicExtProcesadas (String acuse, String claveIF)
		throws NafinException;

	public String[] getTotalesDoctosProcesar (String claveDocto[],
			String claveEPO)
		throws NafinException;

	public boolean hayClaseDoctoAsociado (String claveEpo)
		throws NafinException;

	public boolean hayDoctosAplicCred(String claveIF)
		throws NafinException;

	public void setAmortTemp (String numeroProcesoSolicitudExt, String numero,
			String importe, String fecha)
		throws NafinException;

	public void setSolicExtTemp (String numeroProcesoSolicitudExt, String esquemaSolicitud,
			String claveIF, String clavePyme,
			String numeroProveedor, String numeroDocumento,
			String numeroFactura, String montoDocumento,
			String montoDescuento, String emisor,
			String plazo, String tipoPlazo,
			String periodoPagoCapital, String periodoPagoInteres,
			String descripcionBienes, String domicilioPago,
			String relacionMatUF, String sobretasaUF,
			String tasaNetaUF, String relMatIF,
			String sobretasaIF, String numeroAmortizaciones,
			String fechaPrimerPagoCapital, String fechaPrimerPagoInteres,
			String fechaVencDocto, String fechaVencDscto,
			String diaPago, String fechaEmisionTitulo,
			String numeroContrato, String lugarFirma)
		throws NafinException;

	public Hashtable setSolicitudesExtTemp(String claveIF, String solicitudes)
		throws NafinException;

	public void verificarHorario()
		throws NafinException;

	public abstract boolean hayFondeoPropio(String claveIF,String ic_epo)
			throws NafinException;

	public HashMap getDatosFondeo(String claveIF, String ic_epo, String ic_producto_nafin)
			throws NafinException;


	public abstract Vector getArchAutorizacionAuto ()
			throws NafinException;

	public void ConfirmacionAutomaticaIF(String rutaApp, String idejecucion)
			throws NafinException;

	public String OperaConFondeoPropio(String ic_if,String clavesDoctos)
			throws NafinException;
			
	public Vector getFechasConvenioyDescuento(String sNoIf, String sProducto)
			throws NafinException;

	public Vector getNombreyPuesto(String sNoIf)
			throws NafinException;

	public Hashtable getPlazoBO(String sNoEPO, String sNoIF, int[] iNoMoneda)
			throws NafinException;

	public void validaAutOperSF(String ic_if)
			throws NafinException;

	public List getFechasEnProceso(String ic_if)			
			throws NafinException;

	public List ValidaConfirmacionAutomaticaIF(String ruta,String snombreArch,String ic_if,String ne)
			throws NafinException;

	public List PreAcuseImportarArchivo(String ruta,String snombreArch,String ic_if)
			throws NafinException;

	public String ConfirmacionManualIF(String ruta,String snombreArch,String ic_if)
			throws NafinException;
			
	public void procesaDoctos(OperadosEnlace enlace,String claveIF)			
			throws NafinException; 

	public ProcesoIFWSInfo getDoctosSelecPymeWS(String claveUsuario, 
			String password, String claveIF);
	
	public ProcesoIFWSInfo confirmacionDoctosIFWS (String usuario, 
			String password, 	String claveIF, 
			DocumentoIFWS[] documentos, String pkcs7, String serial) 
			;

	public String getCadenaFirmarWS(DocumentoIFWS[] documentos) 
			;
	
	public ProcesoIFWSInfo getAvisosNotificacionWS (String claveUsuario, 
			String password, 	String claveIF, 
			String fecNotificacionIni, String fecNotificacionFin, 
			String fecVencIni, String fecVencFin,
			String claveEpo, String acuseIf);
	
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[])
		throws AppException; 
		
	public HashMap getTipoFondeo(String claveIF, ArrayList listaClavesEpo, String icProductoNafin)
		throws AppException;
		
	public String getDescripcionTipoFondeo(HashMap catalogoTipoFondeo, String claveEpo);
		
	public HashMap  validaLimiteEpo(String cveEpo, String cveIF);
	
	//Migracion IF
	public String  descripcionEPO(String  ic_epo );
	
	public ProcesoIFWSInfoV2 getCtasBancPymePend(String claveUsuario,String password,String claveIF,
	String fechaParamInic,String fechaParamFin,String claveEpo);
	
	public ProcesoIFWSInfoV2 getCtasBancPymeAutorizadas(String claveUsuario,String password,String claveIF,
	String fechaParamInic,String fechaParamFin,String claveEpo);
	public ProcesoIFWSInfoV2 confirmacionCuentasIFWS (String claveUsuario,
			String password, 	String claveIF,CuentasPymeIFWS[] cuentas);
	public ProcesoIFWSInfoV2 confirmacionDoctosIFWSV2 (String claveUsuario,
			String password, 	String claveIF,
			DocumentoIFWSV2[] documentos, String pkcs7, String serial,String detalle_doctos_error);
	public ProcesoIFWSInfoV2 getDoctosSelecPymeWSV2(String claveUsuario,
			String password, String claveIF);
	public ProcesoIFWSInfoV2 getAvisosNotificacionWSV2 (String claveUsuario,
			String password, 	String claveIF,
			String fecNotificacionIni, String fecNotificacionFin,
			String fecVencIni, String fecVencFin,
			String claveEpo, String acuseIf);
	public ProcesoIFWSInfoV2 confirmacionCuentasIFWS (String claveIF,String claveUsuario,CuentasPymeIFWS[] cuentas);
	public String  getRFCPyme(String icPyme);
	public boolean  getOperaIFEPOFISO(String icIF,String icEPO);
	public String  getMonedaPorCuenta(String cuenta);
	public ProcesoIFWSInfoV2 getLimitesPorEpo (String claveUsuario,
			String password, 	String claveIF);
	public  ProcesoIFWSInfoV2 getProveedoresCFDI (String claveUsuario,
			String password, 	String claveIF,String fechaOperaIni,String fechaOperaFin);
	
	public String  generaArchivosIFDesAutoCSV (HashMap parametros );   
}
