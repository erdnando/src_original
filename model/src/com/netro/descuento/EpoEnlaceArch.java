package com.netro.descuento;

public interface EpoEnlaceArch {

	public abstract String getArchivoAcuse();

	public abstract String getArchivoErrores();

	public abstract String getArchivo();

	public abstract String getNombreEnlace();

	public abstract String getInsertaDatos();

	public abstract String getAcusePublicacion();

	public abstract String getErroresPublicacion();

	public abstract String getRutaArchivo();
	
	/**
	 * Establece la ruta donde se van a generar los archivos.
	 *
	 */
	public void setRutaArchivo(String rutaArchivo);
	

}//EpoEnlace2
