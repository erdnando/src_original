package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class WMEnlace implements EpoEnlace, EpoEnlaceAll, Serializable {

  private ArrayList lErrores = new ArrayList();
  private ArrayList lAlmacenErroresAux = null;
	public String getTablaDocumentos()	{	return "comtmp_doctos_pub_wm";	}
	public String getTablaAcuse()		{	return "com_doctos_pub_acu_wm";	}
	public String getTablaErrores()		{	return "com_doctos_err_pub_wm";	}

	public String getDocuments(){
		return 	"select ig_numero_docto" +
				" ,cg_pyme_epo_interno" +
				" ,df_fecha_docto" +
				" ,df_fecha_venc" +
				" ,ic_moneda " +
				" ,fn_monto" +
				" ,cs_dscto_especial" +
				" ,ct_referencia" +
				" ,cg_campo1" +
				" ,cg_campo2" +
				" ,cg_campo3" +
				" ,cg_campo4 " +
				" ,cg_campo5 "+
				" , '' as ic_if"+
				" , '' AS IC_NAFIN_ELECTRONICO "+
				" from " + getTablaDocumentos();
	}
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+getTablaErrores()+" (ig_numero_docto,cg_error) values('"+err.getIgNumeroDocto()+"','"+err.getCgError()+"')";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	"update "+getTablaAcuse()+
				" set IN_TOTAL_DOCTO_MN="+acu.getInTotalDoctoMn()+
				", FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				", IN_TOTAL_DOCTO_DL="+acu.getInTotalDoctoDl()+
				", FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+ "'"+
				" where cc_acuse='"+acu.getCcAcuse()+"'";
	}
	public String getInsertAcuse(AcuseEnl acu){
		return		" insert into "+getTablaAcuse()+"(CC_ACUSE, IN_TOTAL_DOCTO_MN, FN_TOTAL_MONTO_MN, "+
					" IN_TOTAL_DOCTO_DL, FN_TOTAL_MONTO_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
					" values('"+acu.getCcAcuse()+"',"+acu.getInTotalDoctoMn()+","+acu.getFnTotalMontoMn()+","+acu.getInTotalDoctoDl()+","+acu.getFnTotalMontoDl()+",SYSDATE,'"+((acu.getCsEstatus()).equals("null")?"":acu.getCsEstatus())+"')";
	}
	public String getCondicionQuery(){
		return "";
	}
  public void addErrores(ErroresEnl err){
		lErrores.add(err);
  }
  public List getErrores(){
    return lErrores;
  }
}
