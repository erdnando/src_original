package com.netro.descuento;

import java.util.Hashtable;

public interface EpoEnlOperados extends java.io.Serializable {
	public abstract String getTablaDoctosOpe();
	public abstract String getBorraDoctosOpe();
	public abstract String getBorraDoctosVenc();
	public abstract String getBorraDoctosBaja();
	public abstract String getInsertaDoctosOpe();
	public abstract String getDoctosOperados(Hashtable alParamEPO);
	public abstract String getDoctosEliminacion(Hashtable alParamEPO);
	public abstract String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos);

}
