package com.netro.descuento;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "AutorizacionSolicitudEJB" , mappedName = "AutorizacionSolicitudEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutorizacionSolicitudBean implements AutorizacionSolicitud {

	private final static Log log = ServiceLocator.getInstance().getLog(AutorizacionSolicitudBean.class);


	/**
	 * Determina si un if est� bloqueado o no
	 * @param claveIf Clave del intermediario financiero
	 * @param claveProducto Clave del producto nafin
	 * @return true si esta bloqueado o false de lo contrario
	 *
	 */
	public boolean esIfBloqueado(String claveIf, String claveProducto)
			throws NafinException {

		int ic_if = 0;
		int ic_producto_nafin = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveIf == null || claveProducto == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(claveIf);
			ic_producto_nafin = Integer.parseInt(claveProducto);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" claveIf=" + claveIf +
					" claveProducto=" + claveProducto);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		boolean bloqueado = false;
		AccesoDB con = new AccesoDB();
		String strSQL =
				" SELECT  " +
				" 	count(*) as registroBloqueado  " +
				" FROM " +
				" 	comrel_bloqueo_if_x_producto " +
				" WHERE " +
				" 	ic_if = ? " +
				" 	AND ic_producto_nafin = ? " +
				" 	AND cs_bloqueo = 'B' ";
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_producto_nafin);
			ResultSet rs = ps.executeQuery();
			rs.next();
			if (rs.getInt("registroBloqueado") > 0) {
				bloqueado = true;
			} else {
				bloqueado = false;
			}
			rs.close();
			ps.close();

			return bloqueado;

		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/*********************************************************************************************
	*
	*	    void borrarDoctosCargadosTmp()
	*
	*********************************************************************************************/
    // Agregado 16/10/2002	--CARP
	public void borrarDoctosCargadosTmp( int eicProcSolic )
	    throws NafinException
	{
        String lsSQL = null;
        boolean lbOK = true;

		AccesoDB lodbConexion = new AccesoDB();     // que DS  me conecto?

		System.out.println(" AutorizacionSolicitudEJB::borrarDoctosCargadosTmp(E)");

		try {
			lodbConexion.conexionDB();

			lsSQL = "delete from comtmp_amort_portal where ic_solic_portal in"+
					" (select ic_solic_portal from comtmp_solic_portal where ic_proc_solic = "+eicProcSolic+")";
			lodbConexion.ejecutaSQL(lsSQL);

            lsSQL = "delete from comtmp_solic_portal where ic_proc_solic = "+ eicProcSolic;
			lodbConexion.ejecutaSQL(lsSQL);

		} catch (Exception epError){
        	lbOK = false;
			System.out.println("Error Inesperado:"+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
			System.out.println(" AutorizacionSolicitudEJB::borrarDoctosCargadosTmp(S)");
		}
	}

	public String getArchivoDoctoAplicCred()
		throws NafinException {

		String cadenaDoctosAplicCredito = null;
		Vector registros;
		int numRegistros = 0;

		try {

			registros = getDoctoAplicCredito();
			numRegistros = registros.size();

			if (numRegistros > 0) {
				cadenaDoctosAplicCredito = "Intermediario Financiero,Epo,Pyme"+
					",Num. Solicitud,Moneda,Monto a Operar,Num. del prestamo"+
					",Num. Anticipo Nafin,Num. Anticipo Epo,Fecha Vencimiento Credito"+
					",Moneda,Monto\n";
			}

			for (int i = 0; i < numRegistros; i++) {
				Vector registro = (Vector) registros.get(i);

				String nombreIf = registro.get(0).toString();
				String nombreEpo = registro.get(1).toString();
				String nombrePyme = registro.get(2).toString();
				String folio = registro.get(3).toString();
				String monedaNombre = registro.get(4).toString();
				String monto = registro.get(5).toString();
				String prestamo = registro.get(6).toString();
				String anticipoNafin = registro.get(7).toString();
				String anticipoEpo = registro.get(8).toString();
				String vencimiento = registro.get(9).toString();
				String monedaPrestamo = registro.get(10).toString();
				String montoPrestamo = registro.get(11).toString();

				cadenaDoctosAplicCredito += nombreIf+","+nombreEpo+","+nombrePyme+","+folio+
					","+monedaNombre+","+monto+","+prestamo+","+anticipoNafin+","+anticipoEpo+
					","+vencimiento+","+monedaPrestamo+","+montoPrestamo+"\n";
			} //fin del for

			return cadenaDoctosAplicCredito;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en getArchivoDoctoAplicCred: "+e);
			throw new NafinException("SIST0001");
		} finally {}
	}


	public Vector getDoctoAplicCredito()
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		Vector registros = new Vector();
		ResultSet rs = null;
		int numRegistros = 0;

		try {
			con.conexionDB();
			qrySentencia="select i.cg_razon_social as nombreIf"+
				" , e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme"+
				" , s.ic_folio, m.cd_nombre as monedaDocumento, d.fn_monto, a.ig_numero_prestamo"+
				" , a.ic_pedido, ped.ig_numero_pedido, TO_CHAR(ps.df_vencimiento,'dd/mm/yyyy') as df_vencimiento"+
				" , m2.cd_nombre as monedaPedido, ps.fn_saldo_total"+
				" from com_documento d, com_docto_seleccionado ds, com_solicitud s"+
				" , comcat_pyme p, comcat_if i, comcat_epo e, comcat_moneda m"+
				" , com_cobro_credito cc, com_pedido_seleccionado ps, com_pedido ped"+
				" , com_anticipo a, comcat_moneda m2"+
				" where s.ic_estatus_solic = 2"+
				" and d.ic_estatus_docto = 16"+
				" and d.ic_documento = ds.ic_documento"+
				" and ds.ic_documento = s.ic_documento"+
				" and ds.ic_documento = cc.ic_documento"+
				" and cc.ic_pedido = ps.ic_pedido"+
				" and ps.ic_pedido = ped.ic_pedido"+
				" and ps.ic_pedido = a.ic_pedido"+
				" and ped.ic_moneda = m2.ic_moneda"+
				" and ds.ic_if = i.ic_if"+
				" and d.ic_moneda = m.ic_moneda"+
				" and d.ic_epo = e.ic_epo"+
				" and d.ic_pyme = p.ic_pyme"+
				" and s.cs_tipo_solicitud = 'C'"+
				" and TO_CHAR(s.df_fecha_solicitud,'ddmmyyyy') = TO_CHAR(sysdate,'ddmmyyyy')";

			rs = con.queryDB(qrySentencia);
			numRegistros = 0;

			while(rs.next()) {
				Vector registro = new Vector(12);

				registro.add(0, (rs.getString("nombreIf") == null)?"":rs.getString("nombreIf") );
				registro.add(1, (rs.getString("nombreEpo") == null)?"":rs.getString("nombreEpo") );
				registro.add(2, (rs.getString("nombrePyme") == null)?"":rs.getString("nombrePyme") );
				registro.add(3, (rs.getString("ic_folio") == null)?"":rs.getString("ic_folio") );
				registro.add(4, (rs.getString("monedaDocumento") == null)?"":rs.getString("monedaDocumento") );
				registro.add(5, (rs.getString("fn_monto") == null)?"":rs.getString("fn_monto") );
				registro.add(6, (rs.getString("ig_numero_prestamo") == null)?"":rs.getString("ig_numero_prestamo") );
				registro.add(7, (rs.getString("ic_pedido") == null)?"":rs.getString("ic_pedido"));
				registro.add(8, (rs.getString("ig_numero_pedido") == null)?"":rs.getString("ig_numero_pedido") );
				registro.add(9, (rs.getString("df_vencimiento") == null)?"":rs.getString("df_vencimiento"));
				registro.add(10, (rs.getString("monedaPedido") == null)?"":rs.getString("monedaPedido") );
				registro.add(11, (rs.getString("fn_saldo_total") == null)?"":rs.getString("fn_saldo_total") );

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getDoctoAplicCredito: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	/*********************************************************************************************
	*
	*	    Vector getDoctosCargados()
	*
	*********************************************************************************************/
    // Agregado 15/10/2002	--CARP
	public Vector getDoctosCargados(String esAcuse, String esFolio, String esEstatus, String esIf,
									String esFechaOper, String esNoSirac, String esNoPrestamo) throws NafinException {
		StringBuffer lsCadenaSQL   	= new StringBuffer();
		Vector lovResultado = new Vector();
		Vector lovDatos = null;
		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();     // que DS  me conecto?

		System.out.println(" AutorizacionSolicitudEJB::getDoctosCargados(E)");

		try {
			lodbConexion.conexionDB();

			lsCadenaSQL.append(
				" select P.cg_razon_social as nombrePyme, I.cg_razon_social as nombreIF, SP.ig_clave_sirac as claveSirac, "+
				" SP.ig_sucursal as numSucursal, M.cd_nombre as nombreMoneda, SP.ig_numero_docto as numDocto, "+
				" SP.fn_importe_docto as importeDocto, SP.fn_importe_dscto as importeDscto, E.cd_descripcion as nomEmisor, "+
				" TC.cd_descripcion as descTipoCred, P1.cd_descripcion as descPPC, P2.cd_descripcion as descPPI, "+
				" SP.cg_bienes_servicios as bienesServ, CD.cd_descripcion as descClaseDocto, "+
				" SP.cg_domicilio_pago as domPago, TUF.cd_nombre as descTasaUF, SP.cg_rmuf as relmatUF, "+
				" SP.fg_stuf as sobretasaUF, TI.cd_nombre as descTasaI, SP.cg_rmif as relmatI, "+
				" SP.fg_stif as sobretasaI, SP.in_numero_amort as numAmort, TA.cd_descripcion as descTAmort, "+
				" TO_CHAR(SP.df_ppc,'DD/MM/YYYY') as fechaPPC, SP.in_dia_pago as diaPago, "+
				" TO_CHAR(SP.df_ppi,'DD/MM/YYYY') as fechaPPI, TO_CHAR(SP.df_v_documento,'DD/MM/YYYY') as fechaDocto, "+
				" TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, SP.ig_plazo as plazo, "+
				" TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, SP.cg_lugar_firma as lugarFirma, "+
				" SP.cs_amort_ajuste as tipoAmortAjuste, SP.fg_importe_ajuste as importeAjuste, "+
				" SP.ic_solic_portal as numFolio, TO_CHAR(SP.df_operacion,'DD/MM/YYYY') as fechaOperacion, "+
				" SP.ig_numero_prestamo as numPrestamo, SP.cc_acuse as numAcuse "+
				"  FROM COM_SOLIC_PORTAL SP"   +
				"  , COMCAT_IF I"   +
				"  , COMCAT_EMISOR E"   +
				"  , COMCAT_TIPO_CREDITO TC"   +
				"  , COMCAT_PERIODICIDAD P1"   +
				"  , COMCAT_PERIODICIDAD P2"   +
				"  , COMCAT_TASA TI"   +
				"  , COMCAT_TASA TUF"   +
				"  , COMCAT_TABLA_AMORT TA"   +
				"  , COMCAT_PYME P"   +
				"  , COMCAT_MONEDA M"   +
				"  , COMCAT_CLASE_DOCTO CD"  +
				" where SP.ic_if = I.ic_if "+
				" and SP.ic_emisor = E.ic_emisor(+) "+
				" and SP.ic_tipo_credito = TC.ic_tipo_credito "+
				" and SP.ic_periodicidad_c = P1.ic_periodicidad "+
				" and SP.ic_periodicidad_i = P2.ic_periodicidad "+
				" and SP.ic_tasaif = TI.ic_tasa "+
				" and SP.ic_tasauf = TUF.ic_tasa(+) "+
				" and SP.ic_tabla_amort = TA.ic_tabla_amort "+
				" and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
				" and SP.ic_moneda = M.ic_moneda(+) "+
				" and SP.ic_clase_docto = CD.ic_clase_docto(+) "+
				(esAcuse.equals("")?"":" and SP.cc_acuse ='"+esAcuse+"'")+
				(esFolio.equals("")?"":" and SP.ic_solic_portal = "+esFolio)+
				(esEstatus.equals("")?"":" and SP.ic_estatus_solic = "+esEstatus)+
				(esIf.equals("")?"":" and SP.ic_if = "+esIf)+
				(esFechaOper.equals("")?"":" and TO_CHAR(SP.df_operacion,'DD/MM/YYYY') = '"+esFechaOper+"'")+
				(esNoSirac.equals("")?"":" and SP.ig_clave_sirac = "+esNoSirac)+
				(esNoPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+esNoPrestamo));

			lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());

			while(lrsSel.next()){
                lovDatos = new Vector();
/*0*/			lovDatos.addElement((lrsSel.getString("nombrePyme") == null)? "" : lrsSel.getString("nombrePyme"));				//
/*1*/			lovDatos.addElement((lrsSel.getString("claveSirac") == null)? "" : lrsSel.getString("claveSirac"));				//
/*2*/			lovDatos.addElement((lrsSel.getString("nombreMoneda") == null)? "" : lrsSel.getString("nombreMoneda"));			//
/*3*/			lovDatos.addElement((lrsSel.getString("numDocto") == null)? "" : lrsSel.getString("numDocto"));					//
/*4*/			lovDatos.addElement((lrsSel.getString("importeDocto")==null)?"0" : Comunes.formatoDecimal(lrsSel.getString("importeDocto"),2));	//
/*5*/			lovDatos.addElement((lrsSel.getString("importeDscto")==null)?"0" : Comunes.formatoDecimal(lrsSel.getString("importeDscto"),2));	//
/*6*/			lovDatos.addElement((lrsSel.getString("descTipoCred") == null)? "" : lrsSel.getString("descTipoCred"));			//
/*7*/			lovDatos.addElement((lrsSel.getString("fechaDscto") == null)? "" : lrsSel.getString("fechaDscto"));				//
/*8*/			lovDatos.addElement((lrsSel.getString("fechaETC") == null)? "" : lrsSel.getString("fechaETC"));					//
/*9*/			lovDatos.addElement((lrsSel.getString("numFolio") == null)? "" : lrsSel.getString("numFolio"));					//
/*10*/			lovDatos.addElement((lrsSel.getString("fechaOperacion") == null)? "" : lrsSel.getString("fechaOperacion"));		//
/*11*/			lovDatos.addElement((lrsSel.getString("numPrestamo") == null)? "" : lrsSel.getString("numPrestamo"));			//
/*12*/			lovDatos.addElement((lrsSel.getString("numAcuse") == null)? "" : lrsSel.getString("numAcuse"));					//
/*13*/			lovDatos.addElement((lrsSel.getString("nombreIF") == null)? "" : lrsSel.getString("nombreIF"));					//
/*14*/			lovDatos.addElement((lrsSel.getString("numAmort") == null)? "" : lrsSel.getString("numAmort"));					//
/*15*/			lovDatos.addElement((lrsSel.getString("descTasaI") == null)? "" : lrsSel.getString("descTasaI"));				//
/*16*/			lovDatos.addElement((lrsSel.getString("descTAmort") == null)? "" : lrsSel.getString("descTAmort"));				//
                //lovDatos.addElement((lrsSel.getString("nomEmisor") == null)? "" : lrsSel.getString("nomEmisor"));				//
				//lovDatos.addElement((lrsSel.getString("numSucursal") == null)? "" : lrsSel.getString("numSucursal"));			//
                /*lovDatos.addElement((lrsSel.getString("descPPC") == null)? "" : lrsSel.getString("descPPC"));					//
                lovDatos.addElement((lrsSel.getString("descPPI") == null)? "" : lrsSel.getString("descPPI"));					//
				lovDatos.addElement((lrsSel.getString("bienesServ") == null)? "" : lrsSel.getString("bienesServ"));				//
				lovDatos.addElement((lrsSel.getString("descClaseDocto") == null)? "" : lrsSel.getString("descClaseDocto"));		//
				lovDatos.addElement((lrsSel.getString("domPago") == null)? "" : lrsSel.getString("domPago"));					//
				lovDatos.addElement((lrsSel.getString("descTasaUF") == null)? "" : lrsSel.getString("descTasaUF"));				//
				lovDatos.addElement((lrsSel.getString("relmatUF") == null)? "" : lrsSel.getString("relmatUF"));					//
				lovDatos.addElement((lrsSel.getString("sobretasaUF") == null)? "" : lrsSel.getString("sobretasaUF"));			//
				lovDatos.addElement((lrsSel.getString("relmatI") == null)? "" : lrsSel.getString("relmatI"));					//
				lovDatos.addElement((lrsSel.getString("sobretasaI") == null)? "" : lrsSel.getString("sobretasaI"));				//
				lovDatos.addElement((lrsSel.getString("numAmort") == null)? "" : lrsSel.getString("numAmort"));					//
                lovDatos.addElement((lrsSel.getString("fechaPPC") == null)? "" : lrsSel.getString("fechaPPC"));					//
				lovDatos.addElement((lrsSel.getString("diaPago") == null)? "" : lrsSel.getString("diaPago"));					//
                lovDatos.addElement((lrsSel.getString("fechaPPI") == null)? "" : lrsSel.getString("fechaPPI"));					//
				lovDatos.addElement((lrsSel.getString("fechaDocto") == null)? "" : lrsSel.getString("fechaDocto"));				// */
                //lovDatos.addElement((lrsSel.getString("plazo") == null)? "" : lrsSel.getString("plazo"));						//
				/*lovDatos.addElement((lrsSel.getString("lugarFirma") == null)? "" : lrsSel.getString("lugarFirma"));				//
                lovDatos.addElement((lrsSel.getString("tipoAmortAjuste") == null)? "" : lrsSel.getString("tipoAmortAjuste"));	//
                lovDatos.addElement((lrsSel.getString("importeAjuste")==null)?"0": Comunes.formatoDecimal(lrsSel.getString("importeAjuste"),2)); //*/
                lovResultado.addElement(lovDatos);
            }
			lodbConexion.cierraStatement();

			return lovResultado;

		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta()) {
				lodbConexion.cierraConexionDB();
			}
			System.out.println(" AutorizacionSolicitudEJB::getDoctosCargados(S)");
		}
	}
	/*********************************************************************************************
	*
	*	    Vector getDoctosCargadosTmp()
	*
	*********************************************************************************************/
    // Agregado 15/10/2002	--CARP-HDG
	public Vector getDoctosCargadosTmp( int eicProcSolic )
	    throws NafinException
	{
		StringBuffer lsCadenaSQL   	= new StringBuffer();
		Vector lovResultado = new Vector();
		Vector lovDatos = null;
		ResultSet lrsSel = null;
		PreparedStatement ps = null;
		AccesoDB lodbConexion = new AccesoDB();     // que DS  me conecto?

		System.out.println(" AutorizacionSolicitudEJB::getDoctosCargadosTmp(E)");

		try {
			lodbConexion.conexionDB();


			lsCadenaSQL = new StringBuffer("");
			lsCadenaSQL.append(
				" SELECT p.cg_razon_social AS nombrepyme, i.cg_razon_social AS nombreif, sp.ig_clave_sirac AS clavesirac,"   +
				"        sp.ig_sucursal AS numsucursal, m.cd_nombre AS nombremoneda, sp.ig_numero_docto AS numdocto,"   +
				"        sp.fn_importe_docto AS importedocto, sp.fn_importe_dscto AS importedscto,"   +
				"        e.cd_descripcion AS nomemisor, tc.cd_descripcion AS desctipocred, p1.cd_descripcion AS descppc,"   +
				"        p2.cd_descripcion AS descppi, sp.cg_bienes_servicios AS bienesserv,"   +
				"        cd.cd_descripcion AS descclasedocto, sp.cg_domicilio_pago AS dompago, tuf.cd_nombre AS desctasauf,"   +
				"        sp.cg_rmuf AS relmatuf, sp.fg_stuf AS sobretasauf, ti.cd_nombre AS desctasai, sp.cg_rmif AS relmati,"   +
				"        sp.fg_stif AS sobretasai, sp.in_numero_amort AS numamort, ta.cd_descripcion AS desctamort,"   +
				"        sp.df_ppc AS fechappc, sp.in_dia_pago AS diapago,"   +
				"        sp.df_ppi AS fechappi,"   +
				"        sp.df_v_documento AS fechadocto,"   +
				"        sp.df_v_descuento AS fechadscto, sp.ig_plazo AS plazo,"   +
				"        sp.df_etc AS fechaetc, sp.cg_lugar_firma AS lugarfirma,"   +
				"        sp.cs_amort_ajuste AS tipoamortajuste, sp.fg_importe_ajuste AS importeajuste,"   +
				"        TO_CHAR (pi.df_convenio, 'dd/mm/yyyy') AS fechaconvenio, m.ic_moneda,"   +
				"        NVL (pf.cg_puesto, '') AS puesto_personal, NVL (pf.cg_nombre, '') AS nombre_personal,"   +
				"        NVL (pf.cg_ap_paterno, '') AS paterno_personal, NVL (pf.cg_ap_materno, '') AS materno_personal,"   +
				"			sp.cg_destino_credito AS destcredito "	+
				"   FROM comtmp_solic_portal sp,"   +
				"        comcat_if i,"   +
				"        comcat_emisor e,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_tabla_amort ta,"   +
				"        comcat_pyme p,"   +
				"        comcat_moneda m,"   +
				"        comcat_clase_docto cd,"   +
				"        comrel_producto_if pi,"   +
				"        com_personal_facultado pf"   +
				"  WHERE sp.ic_if = i.ic_if"   +
				"    AND sp.ic_if = pi.ic_if(+)"   +
				"    AND sp.ic_emisor = e.ic_emisor(+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tasaif = ti.ic_tasa"   +
				"    AND sp.ic_tasauf = tuf.ic_tasa(+)"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ig_clave_sirac = p.in_numero_sirac(+)"   +
				"    AND sp.ic_moneda = m.ic_moneda(+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto"   +
				"    AND pi.ic_producto_nafin(+) = 0"   +
				"    AND i.ic_if = pf.ic_if(+)"   +
				"    AND pf.ic_producto_nafin(+) = 0"   +
				"    AND sp.cg_reg_ok = 'S'"   +
				"    AND sp.ic_proc_solic = ?");
			System.out.println("AutorizacionSolicitudBean.getDoctosCargadosTmp(lsCadenaSQL.toString())" + lsCadenaSQL.toString());
			ps = lodbConexion.queryPrecompilado(lsCadenaSQL.toString());
			ps.setInt(1,eicProcSolic);
			lrsSel = ps.executeQuery();
			while(lrsSel.next()){
                lovDatos = new Vector();
/*0*/			lovDatos.addElement((lrsSel.getString("nombrePyme") == null)? "" : lrsSel.getString("nombrePyme"));				//
/*1*/			lovDatos.addElement((lrsSel.getString("claveSirac") == null)? "" : lrsSel.getString("claveSirac"));				//
/*2*/			lovDatos.addElement((lrsSel.getString("nombreMoneda") == null)? "" : lrsSel.getString("nombreMoneda"));			//
/*3*/			lovDatos.addElement((lrsSel.getString("numDocto") == null)? "" : lrsSel.getString("numDocto"));					//
/*4*/			lovDatos.addElement((lrsSel.getString("importeDocto")==null)?"0" : Comunes.formatoDecimal(lrsSel.getString("importeDocto"),2));	//
/*5*/			lovDatos.addElement((lrsSel.getString("importeDscto")==null)?"0" : Comunes.formatoDecimal(lrsSel.getString("importeDscto"),2));	//
/*6*/			lovDatos.addElement((lrsSel.getString("descTipoCred") == null)? "" : lrsSel.getString("descTipoCred"));			//
/*7*/			lovDatos.addElement((lrsSel.getString("fechaDscto") == null)? "" : lrsSel.getString("fechaDscto"));				//
/*8*/			lovDatos.addElement((lrsSel.getString("fechaETC") == null)? "" : lrsSel.getString("fechaETC"));					//
/*9*/			lovDatos.addElement((lrsSel.getString("descTAmort") == null)? "" : lrsSel.getString("descTAmort"));				//
/*10*/			lovDatos.addElement((lrsSel.getString("descTasaI") == null)? "" : lrsSel.getString("descTasaI"));				//
/*11*/			lovDatos.addElement((lrsSel.getString("numAmort") == null)? "" : lrsSel.getString("numAmort"));					//
/*12*/			lovDatos.addElement((lrsSel.getString("nomEmisor") == null)? "" : lrsSel.getString("nomEmisor"));					//
/*13*/			lovDatos.addElement((lrsSel.getString("descClaseDocto") == null)? "" : lrsSel.getString("descClaseDocto"));					//
/*14*/			lovDatos.addElement((lrsSel.getString("lugarFirma") == null)? "" : lrsSel.getString("lugarFirma"));					//
/*15*/			lovDatos.addElement((lrsSel.getString("domPago") == null)? "" : lrsSel.getString("domPago"));					//
/*16*/			lovDatos.addElement((lrsSel.getString("fechaDocto") == null)? "" : lrsSel.getString("fechaDocto"));					//
/*17*/			lovDatos.addElement(new Double(lrsSel.getDouble("importeDocto")));					//
/*18*/			lovDatos.addElement((lrsSel.getString("fechaConvenio") == null)? "" : lrsSel.getString("fechaConvenio"));					//
/*19*/			lovDatos.addElement((lrsSel.getString("ic_moneda") == null)? "" : lrsSel.getString("ic_moneda"));					//
/*20*/			lovDatos.addElement(new Double(lrsSel.getDouble("importeDscto")));	//
/*21*/			lovDatos.addElement((lrsSel.getString("puesto_personal") == null)? "" : lrsSel.getString("puesto_personal"));					//
/*22*/			lovDatos.addElement((lrsSel.getString("nombre_personal") == null)? "" : lrsSel.getString("nombre_personal"));					//
/*23*/			lovDatos.addElement((lrsSel.getString("paterno_personal") == null)? "" : lrsSel.getString("paterno_personal"));					//
/*24*/			lovDatos.addElement((lrsSel.getString("materno_personal") == null)? "" : lrsSel.getString("materno_personal"));					//
/*25*/			lovDatos.addElement((lrsSel.getString("destcredito") == null)? "" : lrsSel.getString("destcredito"));					//
                lovResultado.addElement(lovDatos);
            }
			ps.close();

			return lovResultado;

		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			epError.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta()) {
				lodbConexion.cierraConexionDB();
			}
			System.out.println(" AutorizacionSolicitudEJB::getDoctosCargadosTmp(S)");
		}
	}


	public Vector getSolicitudesManuales(String claveIF, String claveEPO,
			String claveMoneda, String tipoSolicitud,
			String folios, String estatusSolicitud)
		throws NafinException {
		System.out.println("AutorizacionSolicitudBean::getSolicitudesManuales (E)");
		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		String qrySentenciaC = "";
		String qrySentenciaE = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String condicion = "";
		String condicion1 = "";
		Vector registros = new Vector();
		int numRegistros = 0;

		claveIF = (claveIF == null)?"":claveIF;
		claveEPO = (claveEPO == null)?"":claveEPO;
		claveMoneda = (claveMoneda == null)?"":claveMoneda;
		tipoSolicitud = (tipoSolicitud == null)?"":tipoSolicitud;
		folios = (folios == null)?"":folios;

		try {
			if (!claveIF.equals("")) {
				//condicion=condicion+" and ds.ic_if = "+claveIF;
				//condicion1=condicion1+" and de.ic_if = "+claveIF;
				condicion=condicion+" and ds.ic_if = ? ";
				condicion1=condicion1+" and de.ic_if = ? ";
			} else if (!claveEPO.equals("")) {
				//condicion=condicion+" and ds.ic_epo = "+claveEPO;
				condicion=condicion+" and ds.ic_epo = ? ";
			}

			if (!claveMoneda.equals("")) {
				//condicion=condicion+" and d.ic_moneda = "+claveMoneda;
				//condicion1=condicion1+" and de.ic_moneda = "+claveMoneda;
				condicion=condicion+" and d.ic_moneda = ? ";
				condicion1=condicion1+" and de.ic_moneda = ? ";
			}

			if (!tipoSolicitud.equals("")) {
				//condicion=condicion+" and s.cs_tipo_solicitud = '"+tipoSolicitud+"' ";
				//condicion1=condicion1+" and s.cs_tipo_solicitud = '"+tipoSolicitud+"' ";
				condicion=condicion+" and s.cs_tipo_solicitud = ? ";
				condicion1=condicion1+" and s.cs_tipo_solicitud = ? ";
			}

			if (!folios.equals("")) {
				StringTokenizer st = new StringTokenizer(folios,",");
				String foliosAux="";//7,8,9,10 ==> ?,?,?,?
				for(int ist=0;ist<st.countTokens();ist++){
					foliosAux += "?" + ((st.countTokens()==(ist+1))?"":",");
				}
				condicion=condicion+" and s.ic_folio in ("+foliosAux+") ";
				condicion1=condicion1+" and s.ic_folio in ("+foliosAux+") ";
			}

			if (estatusSolicitud.equals("1")) {
				condicion += " and s.ic_bloqueo = 1 ";
				condicion1 += " and s.ic_bloqueo = 1 ";
			} else if (estatusSolicitud.equals("2")) {
				condicion += " and s.ic_bloqueo != 3 ";
				condicion1 += " and s.ic_bloqueo != 3 ";
			}

			qrySentenciaC=" select s.ic_folio "+
				" , i.cg_razon_social as nombreIf"+
				" , e.cg_razon_social as nombreEpo "+
				" , p.cg_razon_social as nombrePyme"+
				" , es.cd_descripcion, d.ic_moneda, m.cd_nombre, d.fn_monto_dscto"+
				" , ds.in_importe_recibir, 'Interna' as tipo, d.CS_DSCTO_ESPECIAL "+
				" , decode(d.cs_dscto_especial,'D',ci.cg_razon_social,'') as BENEF"+
				" , decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,0) as PORC_BENEF"+
				" , decode(d.cs_dscto_especial,'D',ds.fn_importe_recibir_benef,0) as IMPORTE_RECIBIR"+
				" , decode(d.cs_dscto_especial,'D',ds.in_importe_recibir - (ds.fn_importe_recibir_benef),0) as NETO_REC_PYME"+
				" from com_solicitud s, com_documento d, com_docto_seleccionado ds"+
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_if i"+
				" , comcat_estatus_solic es, comrel_pyme_epo pe"+
				" , comcat_if ci"+
				" where s.ic_documento=ds.ic_documento"+
				" and ds.ic_documento=d.ic_documento"+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_epo=e.ic_epo"+
				" and ds.ic_if=i.ic_if"+
				" and d.ic_pyme=p.ic_pyme"+
				" and s.ic_estatus_solic = es.ic_estatus_solic"+
				" and s.ic_estatus_solic = ? " +
				" and s.cs_tipo_solicitud='C'"+
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
				" and e.cs_habilitado='S'"+
				" and pe.cs_habilitado='S'"+
				" and i.cs_habilitado='S'"+
				" and d.ic_beneficiario = ci.ic_if(+)"+
				" "+condicion;

			qrySentenciaE=" select s.ic_folio "+
				" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIf"+
				" , '(No Aplica)' as nombreEpo "+
				" , (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as NombrePyme"+
				" , es.cd_descripcion, de.ic_moneda, m.cd_nombre, de.fn_monto_dscto"+
				" , 0 as in_importe_recibir, 'Externa' as tipo, '' as CS_DSCTO_ESPECIAL"+
				" , '' as BENEF"+
				" , 0 as PORC_BENEF"+
				" , 0 as IMPORTE_RECIBIR"+
				" , 0 as NETO_REC_PYME"+
				" from com_solicitud s, comcat_if i, com_docto_ext de, comcat_estatus_solic es"+
				" , comcat_pyme p, comcat_moneda m"+
				" where s.ic_folio = de.ic_folio"+
				" and de.ic_moneda = m.ic_moneda"+
				" and de.ic_if = i.ic_if"+
				" and de.ic_pyme = p.ic_pyme"+
				" and s.ic_estatus_solic = es.ic_estatus_solic"+
				" and s.ic_estatus_solic = ? "+
				" and s.cs_tipo_solicitud='E'"+
				" and i.cs_habilitado='S'"+
				" and p.cs_habilitado='S'"+
				" "+condicion1;

			if (tipoSolicitud.equals("C")) {
				qrySentencia=qrySentenciaC;
			} else if (tipoSolicitud.equals("E")) {
				qrySentencia=qrySentenciaE;
			} else {
				qrySentencia = qrySentenciaC+" UNION ALL "+qrySentenciaE;
			}

			qrySentencia = qrySentencia+" order by nombreIf, nombreEpo, nombrePyme ";

			System.out.println("*************** getSolicitudesManuales qrySentencia:"+qrySentencia);
			con.conexionDB();
			/**/
			int condicionCount = 0;
			ps = con.queryPrecompilado(qrySentencia);
			if (tipoSolicitud.equals("C")) {//qrySentencia=qrySentenciaC;
				ps.setInt(++condicionCount,Integer.parseInt(estatusSolicitud.trim()));
				if (!claveIF.equals("")) {
				   ps.setInt(++condicionCount,Integer.parseInt(claveIF.trim()));
				} else if (!claveEPO.equals("")) {
				   ps.setInt(++condicionCount,Integer.parseInt(claveEPO.trim()));
				}
				if (!claveMoneda.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveMoneda.trim()));
				}
				if (!tipoSolicitud.equals("")) {
					ps.setString(++condicionCount,tipoSolicitud);
				}
				if (!folios.equals("")) {
					StringTokenizer st = new StringTokenizer(folios,",");
					int tamanio_st=st.countTokens();
					for(int ist=0;ist<tamanio_st;ist++){// ?,?,?,? ==> 7,8,9,10
						ps.setString(++condicionCount,st.nextToken());
					}
				}
			} else if (tipoSolicitud.equals("E")) {//qrySentencia=qrySentenciaE;
				ps.setInt(++condicionCount,Integer.parseInt(estatusSolicitud.trim()));
				if (!claveIF.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveIF.trim()));
				}
				if (!claveMoneda.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveMoneda.trim()));
				}
				if (!tipoSolicitud.equals("")) {
					ps.setString(++condicionCount,tipoSolicitud);
				}
				if (!folios.equals("")) {
					StringTokenizer st = new StringTokenizer(folios,",");
					int tamanio_st=st.countTokens();
					for(int ist=0;ist<tamanio_st;ist++){// ?,?,?,? ==> 7,8,9,10
						ps.setString(++condicionCount,st.nextToken());
					}
				}
			} else {//qrySentencia = qrySentenciaC+" UNION ALL "+qrySentenciaE;
				ps.setInt(++condicionCount,Integer.parseInt(estatusSolicitud.trim()));
				if (!claveIF.equals("")) {
				   ps.setInt(++condicionCount,Integer.parseInt(claveIF.trim()));
				} else if (!claveEPO.equals("")) {
				   ps.setInt(++condicionCount,Integer.parseInt(claveEPO.trim()));
				}
				if (!claveMoneda.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveMoneda.trim()));
				}
				if (!tipoSolicitud.equals("")) {
					ps.setString(++condicionCount,tipoSolicitud);
				}
				if (!folios.equals("")) {
					StringTokenizer st = new StringTokenizer(folios,",");
					int tamanio_st=st.countTokens();
					for(int ist=0;ist<tamanio_st;ist++){// ?,?,?,? ==> 7,8,9,10
						ps.setString(++condicionCount,st.nextToken());
					}
				}
				ps.setInt(++condicionCount,Integer.parseInt(estatusSolicitud.trim()));
				if (!claveIF.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveIF.trim()));
				}
				if (!claveMoneda.equals("")) {
					ps.setInt(++condicionCount,Integer.parseInt(claveMoneda.trim()));
				}
				if (!tipoSolicitud.equals("")) {
					ps.setString(++condicionCount,tipoSolicitud);
				}
				if (!folios.equals("")) {
					StringTokenizer st = new StringTokenizer(folios,",");
					int tamanio_st=st.countTokens();
					for(int ist=0;ist<tamanio_st;ist++){// ?,?,?,? ==> 7,8,9,10
						ps.setString(++condicionCount,st.nextToken());
					}
				}
			}

			//rs = con.queryDB(qrySentencia);
			/**/
			rs = ps.executeQuery();
			System.out.println("*************** getSolicitudesManuales qrySentencia: ejecutada");
			System.out.println("*************** condicionCount:"+condicionCount);
			while (rs.next()) {
				Vector registro = new Vector(11);
				numRegistros++;

				registro.add(0, (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") );
				registro.add(1, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF"));
				registro.add(2, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(3, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME"));
				registro.add(4, (rs.getString("CD_DESCRIPCION") == null)?"":rs.getString("CD_DESCRIPCION") ); //estatus Solicitud
				registro.add(5, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA"));
				registro.add(6, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") ); //Nombre de la moneda
				registro.add(7, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO"));
				registro.add(8, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR") );
				registro.add(9, (rs.getString("TIPO") == null)?"":rs.getString("TIPO"));
				registro.add(10, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );
				registro.add(11, (rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				registro.add(12, (rs.getString("PORC_BENEF") == null) ? "" : (rs.getString("PORC_BENEF").equals("0")) ? "" : rs.getString("PORC_BENEF"));
				registro.add(13, (rs.getString("IMPORTE_RECIBIR") == null) ? "" : (rs.getString("IMPORTE_RECIBIR").equals("0")) ? "" : rs.getString("IMPORTE_RECIBIR"));
				registro.add(14, (rs.getString("NETO_REC_PYME") == null) ? "" : (rs.getString("NETO_REC_PYME").equals("0")) ? "" : rs.getString("NETO_REC_PYME"));

				registros.add(registro);

			}
			rs.close();
			//con.cierraStatement();
			if(ps != null) ps.close();
			System.out.println("*********************** termine procesar qrySentencia");
			if (numRegistros > 0) {
				return registros;
			} else {
				throw new NafinException("GRAL0004");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Error en getSolicitudesManuales: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getSolicOperadas(String folios)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		Vector registros = new Vector();
		ResultSet rs = null;

		try {
			con.conexionDB();
			qrySentencia=" select s.ic_folio, i.cg_razon_social as nombreIf"+
				" , e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto_dscto, es.cd_descripcion"+
				" , s.cg_causa_rechazo, ds.in_importe_recibir, d.CS_DSCTO_ESPECIAL "+
  			" , decode(d.cs_dscto_especial,'D',ci.cg_razon_social,'') as BENEF"+
  			" , decode(d.cs_dscto_especial,'D', d.fn_porc_beneficiario,0) as PORC_BENEF"+
  			" , decode(d.cs_dscto_especial,'D', DS.fn_importe_recibir_benef,0) as IMPORTE_RECIBIR"+
  			" , decode(d.cs_dscto_especial,'D', DS.in_importe_recibir - DS.fn_importe_recibir_benef,0) as NETO_REC_PYME"+
				" from com_solicitud s, com_documento d, com_docto_seleccionado ds"+
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_if i"+
  			" , comcat_estatus_solic es,comcat_if ci"+
				" where s.ic_documento=ds.ic_documento"+
				" and ds.ic_documento=d.ic_documento"+
				" and d.ic_moneda=m.ic_moneda"+
				" and s.ic_estatus_solic=es.ic_estatus_solic"+
				" and d.ic_epo=e.ic_epo"+
				" and ds.ic_if=i.ic_if"+
				" and d.ic_pyme=p.ic_pyme"+
				" and s.cs_tipo_solicitud = 'C'"+
				" and s.ic_folio in ("+folios+")"+
  			" and d.ic_beneficiario = ci.ic_if(+)"+
				" UNION ALL "+
				" select s.ic_folio,i.cg_razon_social as nombreIf, '(No Aplica)' as nombreEpo"+
				" , p.cg_razon_social as NombrePyme, de.ic_moneda, m.cd_nombre, de.fn_monto_dscto"+
				" , es.cd_descripcion, s.cg_causa_rechazo, 0 as in_importe_recibir, '' "+
   			" , '' as BENEF"+
  			" , 0 as PORC_BENEF"+
  			" , 0 as IMPORTE_RECIBIR"+
  			" , 0 as NETO_REC_PYME"+
				" from com_solicitud s, comcat_if i, com_docto_ext de, comcat_estatus_solic es"+
				" , comcat_pyme p, comcat_moneda m"+
				" where s.ic_folio = de.ic_folio"+
				" and de.ic_moneda = m.ic_moneda"+
				" and de.ic_if = i.ic_if"+
				" and de.ic_pyme = p.ic_pyme"+
				" and s.cs_tipo_solicitud = 'E'"+
				" and s.ic_estatus_solic = es.ic_estatus_solic"+
				" and s.ic_folio in ("+folios+")"+
				" order by nombreIf, nombreEpo, nombrePyme";

			rs = con.queryDB(qrySentencia);

			while(rs.next()) {
				Vector registro = new Vector(11);

				registro.add(0, (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") );
				registro.add(1, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(2, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(3, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") );	//Moneda
				registro.add(4, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA") );
				registro.add(5, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(6, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );
				registro.add(7, (rs.getString("CD_DESCRIPCION") == null)?"":rs.getString("CD_DESCRIPCION") );	//Estatus solicitud
				registro.add(8, (rs.getString("CG_CAUSA_RECHAZO") == null)?"":rs.getString("CG_CAUSA_RECHAZO") );
				registro.add(9, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR") );
				registro.add(10, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );
        registro.add(11, (rs.getString("NETO_REC_PYME") == null) ? "" : (rs.getString("NETO_REC_PYME").equals("0")) ? "" : rs.getString("NETO_REC_PYME"));
        registro.add(12, (rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
        registro.add(13, (rs.getString("PORC_BENEF") == null) ? "" : (rs.getString("PORC_BENEF").equals("0")) ? "" : rs.getString("PORC_BENEF"));
        registro.add(14, (rs.getString("IMPORTE_RECIBIR") == null) ? "" : (rs.getString("IMPORTE_RECIBIR").equals("0")) ? "" : rs.getString("IMPORTE_RECIBIR"));

				registros.add(registro);
			} //fin del while
			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			System.out.println("Error en getSolicOperadas: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getSolicProcesadas(String folios)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		Vector registros = new Vector();
		ResultSet rs = null;

		try {
			con.conexionDB();

			int i=0;
			VectorTokenizer vt = new VectorTokenizer(folios, ",");
			Vector vecFolios = vt.getValuesVector();
			String sCadenaInicial = (vecFolios.size()>0)?"?":"";
			StringBuffer sbInterrogacion = new StringBuffer(sCadenaInicial);
			for(i=0; i<vecFolios.size()-1; i++) {
				sbInterrogacion.append(",?");
			} // for

			//ATENCION. El orden de las TABLAS en el sig. query NO DEBE alterarse.
			qrySentencia="SELECT   /*+ ordered Use_nl(N1, N2, N3, S, EA, TC, TAM, ESS, TA, OES, A3, DS, I, D, MON,P, EST, SEC, DOM, ES) INDEX(S IN_COM_SOLICITUD_02_NUK) */ "+
					"		s.ic_folio,"+
					"       p.in_numero_sirac, p.cg_razon_social, p.cg_rfc, "+
					"       (n3.ic_nafin_electronico || ' / ' || i.cg_razon_social) ifb, "+
					"       (dom.ic_estado || ' / ' || es.cd_nombre) estado, "+
					"       dom.cg_municipio, "+
					"       (dom.cg_calle || ' Ext. ' || dom.cg_numero_ext || ' Int. ' || dom.cg_numero_int ) domicilio, "+
					"       dom.cg_colonia, dom.cn_cp, "+
					"       TO_CHAR (p.df_constitucion, 'DD/MM/YYYY') fecha_const, "+
					"       (p.ic_estrato || ' / ' || est.cd_nombre) estrato, "+
					"       p.fn_ventas_net_tot, p.fn_ventas_net_exp, "+
					"       (p.ic_sector_econ || ' / ' || sec.cd_nombre) sector, "+
					"       p.cg_productos, "+
					"       (d.ic_moneda || ' / ' || mon.cd_nombre) moneda, "+
					"       (s.ic_estatus_solic || ' / ' || ess.cd_descripcion) estatus, "+
					"       d.fn_monto, (ds.in_importe_recibir) fn_monto_dscto, "+
					"       (s.ic_tipo_credito || ' / ' || tc.cd_descripcion) tipo_credito, "+
					"       (s.ic_esquema_amort || ' / ' || ea.cd_descripcion) esquema_amort, "+
					"       (s.ig_plazo || ' / ' || s.cg_tipo_plazo) plazo, "+
					"       s.cg_perio_pago_cap, s.cg_perio_pago_int, "+
					"       (s.ic_tasaif || ' / ' || ta.cd_nombre) tasauf, "+
					"       ds.cg_rel_mat AS rmuf, ds.cg_puntos AS stuf, "+
					"       (ds.in_tasa_aceptada) tnuf, "+
					"       TO_CHAR (s.df_ppc, 'DD/MM/YYYY') fechappc, "+
					"       TO_CHAR (s.df_ppi, 'DD/MM/YYYY') fechappi, s.in_dia_pago, "+
					"       TO_CHAR (s.df_v_descuento, 'DD/MM/YYYY') fechavenc, "+
					"       (s.ic_tasaif || ' / ' || ta.cd_nombre) tasaif, s.cg_rmif, "+
					"       s.in_stif, n1.ic_nafin_electronico ne_epo, "+
					"       n2.ic_nafin_electronico ne_pyme, d.ig_numero_docto, "+
					"       (s.ic_oficina || ' / ' || oes.cd_descripcion) oficina, "+
					"       a3.ic_usuario as  funcionario, "+
					"       d.ic_moneda, s.in_numero_amort, "+
					"       (s.ic_tabla_amort || ' / ' || tam.cd_descripcion) tabla_amort, "+
					"       s.cg_emisor, d.cs_dscto_especial, "+
					"       DECODE (d.cs_dscto_especial, 'D', ci.cg_razon_social, '') AS benef, "+
					"       DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 0) AS porc_benef, "+
					"       DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 0) AS importe_recibir, "+
					"       DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 0 ) AS neto_rec_pyme "+
					"       , ped.ic_producto_nafin as ic_producto_nafinp "+
					"       , ant.ig_numero_prestamo as ig_numero_prestamop "+
					"       , pn.ic_nombre  as ic_nombrep "+
					"       , id.ic_producto_nafin as ic_producto_nafini "+
					"       , isol.ig_numero_prestamo  as ig_numero_prestamoi "+
					"       , pn2.ic_nombre as ic_nombrei "+
					"		, 'AutorizacionSolicitudBean:getSolicProcesadas()' "+
					"  FROM comrel_nafin n1, "+
					"       com_solicitud s, "+
					"       comcat_esquema_amort ea, "+
					"       comcat_tipo_credito tc, "+
					"       comcat_tabla_amort tam, "+
					"       comcat_estatus_solic ess, "+
					"       comcat_tasa ta, "+
					"       comcat_oficina_estatal oes, "+
					"       com_acuse3 a3, "+
					"       com_docto_seleccionado ds, "+
					"       comcat_if i, "+
					"       com_documento d, "+
					"       comcat_moneda mon, "+
					"       comcat_pyme p, "+
					"       comcat_estrato est, "+
					"       comcat_sector_econ sec, "+
					"       com_domicilio dom, "+
					"       comcat_estado es, "+
					"       comrel_nafin n2, "+
					"       comrel_nafin n3, "+
					"       comcat_if ci, "+
					"       com_cobro_credito cc, "+
					"       com_pedido_seleccionado ps, "+
					"       com_pedido ped, "+
					"       com_anticipo ant, "+
					"       comcat_producto_nafin pn, "+
					"       comcat_producto_nafin pn2, "+
					"       inv_pago ip, "+
					"       inv_disposicion id, "+
					"       inv_solicitud isol "+
					" WHERE s.ic_estatus_solic = 2 "+
					"   AND d.ic_documento = ds.ic_documento "+
					"   AND ds.ic_documento = s.ic_documento "+
					"   AND d.ic_pyme = p.ic_pyme "+
					"   AND p.ic_pyme = dom.ic_pyme (+) "+
					"   AND UPPER (dom.cs_fiscal) = 'S' "+
					"   AND dom.ic_estado = es.ic_estado (+) "+
					"   AND p.ic_estrato = est.ic_estrato (+) "+
					"   AND p.ic_sector_econ = sec.ic_sector_econ (+) "+
					"   AND d.ic_moneda = mon.ic_moneda "+
					"   AND s.ic_estatus_solic = ess.ic_estatus_solic "+
					"   AND s.ic_tabla_amort = tam.ic_tabla_amort "+
					"   AND ds.ic_if = i.ic_if "+
					"   AND s.ic_tipo_credito = tc.ic_tipo_credito "+
					"   AND s.ic_esquema_amort = ea.ic_esquema_amort "+
					"   AND (d.ic_epo = n1.ic_epo_pyme_if AND UPPER (n1.cg_tipo) = 'E') "+
					"   AND (d.ic_pyme = n2.ic_epo_pyme_if AND UPPER (n2.cg_tipo) = 'P') "+
					"   AND (ds.ic_if = n3.ic_epo_pyme_if AND UPPER (n3.cg_tipo) = 'I') "+
					"   AND s.ic_oficina = oes.ic_oficina "+
					"   AND s.cc_acuse = a3.cc_acuse "+
					"   AND s.ic_tasaif = ta.ic_tasa "+
					"   AND s.ic_folio in ("+sbInterrogacion.toString()+")"+
					"   AND d.ic_beneficiario = ci.ic_if (+) "+
					"   AND ds.ic_documento = cc.ic_documento (+) "+
					"   AND cc.ic_pedido = ps.ic_pedido (+) "+
					"   AND cc.ic_pedido = ped.ic_pedido (+) "+
					"   AND ps.ic_pedido = ant.ic_pedido (+) "+
					"   AND ped.ic_producto_nafin = pn.ic_producto_nafin (+) "+
					"   AND ds.ic_documento = ip.ic_documento (+) "+
					"   AND ip.cc_disposicion = id.cc_disposicion (+) "+
					"   AND id.cc_disposicion = isol.cc_disposicion (+) "+
					"   AND id.ic_producto_nafin = pn2.ic_producto_nafin (+) "+
					" GROUP BY s.ic_folio, p.in_numero_sirac, p.cg_razon_social, "+
					"          p.cg_rfc, n3.ic_nafin_electronico, i.cg_razon_social, "+
					"          dom.ic_estado, es.cd_nombre, dom.cg_municipio, "+
					"          dom.cg_calle, dom.cg_numero_ext, dom.cg_numero_int, "+
					"          dom.cg_colonia, dom.cn_cp, p.df_constitucion, "+
					"          p.ic_estrato, est.cd_nombre, p.fn_ventas_net_tot, "+
					"          p.fn_ventas_net_exp, p.ic_sector_econ, sec.cd_nombre, "+
					"          p.cg_productos, d.ic_moneda, mon.cd_nombre, "+
					"          s.ic_estatus_solic, ess.cd_descripcion, d.fn_monto, "+
					"          ds.in_importe_recibir, s.ic_tipo_credito, tc.cd_descripcion, "+
					"          s.ic_esquema_amort, ea.cd_descripcion, s.ig_plazo, "+
					"          s.cg_tipo_plazo, s.cg_perio_pago_cap, s.cg_perio_pago_int, "+
					"          s.ic_tasaif, ta.cd_nombre, ds.cg_rel_mat, "+
					"          ds.cg_puntos, ds.in_tasa_aceptada, s.df_ppc, "+
					"          s.df_ppi, s.in_dia_pago, s.df_v_descuento, "+
					"          s.ic_tasaif, ta.cd_nombre, s.cg_rmif, "+
					"          s.in_stif, n1.ic_nafin_electronico, n2.ic_nafin_electronico, "+
					"          d.ig_numero_docto, s.ic_oficina, oes.cd_descripcion, "+
					"          a3.ic_usuario,"+
					"          d.ic_moneda, s.in_numero_amort, "+
					"          s.ic_tabla_amort, tam.cd_descripcion, s.cg_emisor, "+
					"          d.cs_dscto_especial, ci.cg_razon_social, d.fn_porc_beneficiario, "+
					"          ds.fn_importe_recibir_benef, ds.in_importe_recibir, ds.fn_importe_recibir_benef, "+
					"          ped.ic_producto_nafin, ant.ig_numero_prestamo, pn.ic_nombre, "+
					"          id.ic_producto_nafin, pn2.ic_nombre, isol.ig_numero_prestamo";

			System.out.println(qrySentencia);
			//rs = con.queryDB(qrySentencia);
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			for(i=0; i<vecFolios.size(); i++) {
				ps.setObject(i+1, vecFolios.get(i));
			}
			rs = ps.executeQuery();

			while(rs.next()) {
				Vector registro = new Vector(46);

				registro.add(0, (rs.getString("ic_folio") == null)?"":rs.getString("ic_folio") );
				registro.add(1, (rs.getString("in_numero_sirac") == null)?"":rs.getString("in_numero_sirac") );
				registro.add(2, (rs.getString("cg_razon_social") == null)?"":rs.getString("cg_razon_social") );//NombrePyme
				registro.add(3, (rs.getString("cg_rfc") == null)?"":rs.getString("cg_rfc") );
				registro.add(4, (rs.getString("IFB") == null)?"":rs.getString("IFB") );
				registro.add(5, (rs.getString("ESTADO") == null)?"":rs.getString("ESTADO") );
				registro.add(6, (rs.getString("cg_municipio") == null)?"":rs.getString("cg_municipio") );
				registro.add(7, (rs.getString("DOMICILIO") == null)?"":rs.getString("DOMICILIO") );
				registro.add(8, (rs.getString("cg_colonia") == null)?"":rs.getString("cg_colonia") );
				registro.add(9, (rs.getString("cn_cp") == null)?"":rs.getString("cn_cp") );
				registro.add(10, (rs.getString("FECHA_CONST") == null)?"":rs.getString("FECHA_CONST") );
				registro.add(11, (rs.getString("ESTRATO") == null)?"":rs.getString("ESTRATO") );
				registro.add(12, (rs.getString("fn_ventas_net_tot") == null)?"":rs.getString("fn_ventas_net_tot") );
				registro.add(13, (rs.getString("fn_ventas_net_exp") == null)?"":rs.getString("fn_ventas_net_exp") );
				registro.add(14, (rs.getString("SECTOR") == null)?"":rs.getString("SECTOR") );
				registro.add(15, (rs.getString("cg_productos") == null)?"":rs.getString("cg_productos") );
				registro.add(16, (rs.getString("MONEDA") == null)?"":rs.getString("MONEDA") );
				registro.add(17, (rs.getString("ESTATUS") == null)?"":rs.getString("ESTATUS") );
				registro.add(18, (rs.getString("fn_monto") == null)?"":rs.getString("fn_monto") );
				registro.add(19, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(20, (rs.getString("TIPO_CREDITO") == null)?"":rs.getString("TIPO_CREDITO") );
				registro.add(21, (rs.getString("ESQUEMA_AMORT") == null)?"":rs.getString("ESQUEMA_AMORT") );
				registro.add(22, (rs.getString("PLAZO") == null)?"":rs.getString("PLAZO") );
				registro.add(23, (rs.getString("cg_perio_pago_cap") == null)?"":rs.getString("cg_perio_pago_cap") );
				registro.add(24, (rs.getString("cg_perio_pago_int") == null)?"":rs.getString("cg_perio_pago_int") );
				registro.add(25, (rs.getString("TASAUF") == null)?"":rs.getString("TASAUF") );
				registro.add(26, (rs.getString("RMUF") == null)?"":rs.getString("RMUF") );
				registro.add(27, (rs.getString("STUF") == null)?"":rs.getString("STUF") );
				registro.add(28, (rs.getString("TNUF") == null)?"":rs.getString("TNUF") );
				registro.add(29, (rs.getString("FECHAPPC") == null)?"":rs.getString("FECHAPPC") );
				registro.add(30, (rs.getString("FECHAPPI") == null)?"":rs.getString("FECHAPPI") );
				registro.add(31, (rs.getString("in_dia_pago") == null)?"":rs.getString("in_dia_pago") );
				registro.add(32, (rs.getString("FECHAVENC") == null)?"":rs.getString("FECHAVENC") );
				registro.add(33, (rs.getString("TASAIF") == null)?"":rs.getString("TASAIF") );
				registro.add(34, (rs.getString("cg_rmif") == null)?"":rs.getString("cg_rmif") );
				registro.add(35, (rs.getString("in_stif") == null)?"":rs.getString("in_stif") );
				registro.add(36, (rs.getString("NE_EPO") == null)?"":rs.getString("NE_EPO") );
				registro.add(37, (rs.getString("NE_PYME") == null)?"":rs.getString("NE_PYME") );
				registro.add(38, (rs.getString("ig_numero_docto") == null)?"":rs.getString("ig_numero_docto") );
				registro.add(39, (rs.getString("OFICINA") == null)?"":rs.getString("OFICINA") );
				registro.add(40, (rs.getString("FUNCIONARIO") == null)?"":rs.getString("FUNCIONARIO") );
				registro.add(41, (rs.getString("ic_moneda") == null)?"":rs.getString("ic_moneda") ); //Este campo no se emplea acualmente...
				registro.add(42, (rs.getString("in_numero_amort") == null)?"":rs.getString("in_numero_amort") );
				registro.add(43, (rs.getString("TABLA_AMORT") == null)?"":rs.getString("TABLA_AMORT") );
				registro.add(44, (rs.getString("cg_emisor") == null)?"":rs.getString("cg_emisor") );
				registro.add(45, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );
				registro.add(46, (rs.getString("NETO_REC_PYME") == null) ? "" : (rs.getString("NETO_REC_PYME").equals("0")) ? "" : rs.getString("NETO_REC_PYME"));
				registro.add(47, (rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				registro.add(48, (rs.getString("PORC_BENEF") == null) ? "" : (rs.getString("PORC_BENEF").equals("0")) ? "" : rs.getString("PORC_BENEF"));
				registro.add(49, (rs.getString("IMPORTE_RECIBIR") == null) ? "" : (rs.getString("IMPORTE_RECIBIR").equals("0")) ? "" : rs.getString("IMPORTE_RECIBIR"));


				if ("2".equals(rs.getString("IC_PRODUCTO_NAFINP"))) {
					registro.add(50, (rs.getString("IG_NUMERO_PRESTAMOP") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMOP") );
					registro.add(51, rs.getString("IC_NOMBREP") );
				} else if ("5".equals(rs.getString("IC_PRODUCTO_NAFINI"))) {
					registro.add(50, (rs.getString("IG_NUMERO_PRESTAMOI") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMOI") );
					registro.add(51, rs.getString("IC_NOMBREI") );
				} else {
					registro.add(50, " " );
					registro.add(51, " " );
				}
				registros.add(registro);
			} //fin del while
			rs.close();
			if(ps!=null) ps.close();
			//con.cierraStatement();

			return registros;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error en getSolicProcesadas: "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	/*********************************************************************************************
	*
	*	    Vector ovProcesarArchivo()   [ Carga Masiva ]
	*
	*********************************************************************************************/
    // Agregado 15/10/2002	--CARP
	public Vector ovProcesarArchivo(String esNombreArchivo, String esDirectorio, String NoIf,
									String TipoPlazo) throws NafinException	{

		boolean lbOK = true;
		boolean bBotonProcesar = true;

		BigDecimal bdMtoDoctos = new BigDecimal("0.0");
		BigDecimal bdMtoDsctos = new BigDecimal("0.0");

		StringBuffer lsCadenaSQL   	= new StringBuffer();

		String let="";
		String claveSirac="", noSucBanco="", moneda="", numDocto="";
		String importeDocto="", importeDscto="", emisor="", tipoCredito="", ppc="", ppi="";
		String claseDocto="", domicilioPago="", tasaInteresUF="", relMatUF="", sobreTasaUF="";
		String tasaInteresI="", relMatI="", sobreTasaI="", tablaAmort="", numAmort="";
		String fppc="", fppi="", bieneServi="", fechVencDocto="", fechVencDscto	="";
		String diaPago="", fetc="", lugarFirma="", amortAjuste="", amortImporte="", amortFecha="";
		String /*icIf="",*/ icEmisor="", icTipoCredito="", icTipoPPC="";
		String icTipoPPI="", icClaseDocto="", icTasaUF="", icTasaIntermed="", icTablaAmort="";
		String tipoRenta="";
		String numAmortRec="";
		String plazoMensual="";
		String sTomoPlazoGral = "N";
		String lsLinea 	  = "";

		int PlazoFPPC=0;
		int PlazoFPPI=0;
		int PlazoFVD=0;
		int iPlazoMaxBO=0;
		int liPipesporlinea=0;
        int liLineadocs=1;
		int licProcSolic = 0;

		VectorTokenizer lvt = null;
		Vector lovDatos 	= null;
       	Vector lovTablaAmort= null;
		Vector lovTmpBO 	= null;
		Vector lovResultado = new Vector();
		Vector VecError		= new Vector();
		Vector VecErrores 	= new Vector();
		Vector VecProc 		= new Vector();
		Vector VecProcs 	= new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" AutorizacionSolicitudEJB::ovProcesarArchivo(E)");
		int iTipoPiso =0;
		try {
			lodbConexion.conexionDB();

			iTipoPiso = getTipoPisoIF(NoIf, lodbConexion);

			System.out.println("Va a abrir el Archivo:"+esNombreArchivo);
			File lofArchivo = new File(esNombreArchivo);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));

			System.out.println("Abrio el Archivo");

			//Obtiene el maximo de ic_proc_solic.
			lsCadenaSQL.delete(0, lsCadenaSQL.length());
			lsCadenaSQL.append("select (NVL(max(ic_proc_solic),0)+1) from comtmp_solic_portal");
			lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
			if(lrsSel.next())
				licProcSolic = (lrsSel.getInt(1)==0) ?1 : lrsSel.getInt(1);
			lodbConexion.cierraStatement();

			System.out.println("Empieza a Leer el Archivo");
			for ( liLineadocs = 1; (lsLinea = br.readLine()) != null; liLineadocs++) {
				System.out.println("Linea "+liLineadocs);
				lbOK = true;

				lsLinea = lsLinea.trim();
				if (lsLinea.length()==0)
					continue;
				liPipesporlinea = 0;

				for (int n=0; n<lsLinea.length(); n++) {
					let = lsLinea.substring(n,n+1);
					if (let.equals("|"))
						liPipesporlinea++;
				}

				lvt = new VectorTokenizer(lsLinea,"|");
                lovDatos = lvt.getValuesVector();

                // Valida el n�mero de datos de la l�nea
				if( lovDatos.size() < 26) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("No coincide con el layout requerido.");
					VecError.addElement("Verifique el n�mero de datos.");
					VecErrores.addElement(VecError);
                    lbOK = false;
				} else {
					claveSirac		=(lovDatos.size()>=1)?Comunes.quitaComitasSimples(lovDatos.get(0).toString().trim()).trim():"";
					noSucBanco		=(lovDatos.size()>=2)?Comunes.quitaComitasSimples(lovDatos.get(1).toString().trim()).trim():"";
					moneda			=(lovDatos.size()>=3)?Comunes.quitaComitasSimples(lovDatos.get(2).toString().trim()).trim():"";
					numDocto		=(lovDatos.size()>=4)?Comunes.quitaComitasSimples(lovDatos.get(3).toString().trim()).trim():"";
					importeDocto	=(lovDatos.size()>=5)?Comunes.quitaComitasSimples(lovDatos.get(4).toString().trim()).trim():"";
					importeDscto	=(lovDatos.size()>=6)?Comunes.quitaComitasSimples(lovDatos.get(5).toString().trim()).trim():"";
					emisor 			=(lovDatos.size()>=7)?Comunes.quitaComitasSimples(lovDatos.get(6).toString().trim()).trim():"";
					tipoCredito		=(lovDatos.size()>=8)?Comunes.quitaComitasSimples(lovDatos.get(7).toString().trim()).trim():"";
					ppc				=(lovDatos.size()>=9)?Comunes.quitaComitasSimples(lovDatos.get(8).toString().trim()).trim():"";
					ppi 			=(lovDatos.size()>=10)?Comunes.quitaComitasSimples(lovDatos.get(9).toString().trim()).trim():"";
					bieneServi 		=(lovDatos.size()>=11)?Comunes.quitaComitasSimples(lovDatos.get(10).toString().trim()).trim():"";
					claseDocto 		=(lovDatos.size()>=12)?Comunes.quitaComitasSimples(lovDatos.get(11).toString().trim()).trim():"";
					domicilioPago 	=(lovDatos.size()>=13)?Comunes.quitaComitasSimples(lovDatos.get(12).toString().trim()).trim():"";
					tasaInteresUF 	=(lovDatos.size()>=14)?Comunes.quitaComitasSimples(lovDatos.get(13).toString().trim()).trim():"";
					relMatUF 		=(lovDatos.size()>=15)?Comunes.quitaComitasSimples(lovDatos.get(14).toString().trim()).trim():"";
					sobreTasaUF		=(lovDatos.size()>=16)?Comunes.quitaComitasSimples(lovDatos.get(15).toString().trim()).trim():"";
					tasaInteresI	=(lovDatos.size()>=17)?Comunes.quitaComitasSimples(lovDatos.get(16).toString().trim()).trim():"";
					numAmort		=(lovDatos.size()>=18)?Comunes.quitaComitasSimples(lovDatos.get(17).toString().trim()).trim():"";
					tablaAmort 		=(lovDatos.size()>=19)?Comunes.quitaComitasSimples(lovDatos.get(18).toString().trim()).trim():"";
					fppc			=(lovDatos.size()>=20)?Comunes.quitaComitasSimples(lovDatos.get(19).toString().trim()).trim():"";
					fppi			=(lovDatos.size()>=21)?Comunes.quitaComitasSimples(lovDatos.get(20).toString().trim()).trim():"";
					diaPago			=(lovDatos.size()>=22)?Comunes.quitaComitasSimples(lovDatos.get(21).toString().trim()).trim():"";
					fechVencDocto	=(lovDatos.size()>=23)?Comunes.quitaComitasSimples(lovDatos.get(22).toString().trim()).trim():"";
					fechVencDscto	=(lovDatos.size()>=24)?Comunes.quitaComitasSimples(lovDatos.get(23).toString().trim()).trim():"";
					fetc			=(lovDatos.size()>=25)?Comunes.quitaComitasSimples(lovDatos.get(24).toString().trim()).trim():"";
					lugarFirma		=(lovDatos.size()>=26)?Comunes.quitaComitasSimples(lovDatos.get(25).toString().trim()).trim():"";
					amortAjuste	 	=(lovDatos.size()>=27)?Comunes.quitaComitasSimples(lovDatos.get(26).toString().trim()).trim():"";
					tipoRenta		=(lovDatos.size()>=28)?Comunes.quitaComitasSimples(lovDatos.get(27).toString().trim()).trim():"";

					// eliminamos las comillas de todos los campos y las comas de los numericos
					claveSirac		= Comunes.strtr(claveSirac,"\"","");
					noSucBanco		= Comunes.strtr(noSucBanco,"\"","");
					moneda			= Comunes.strtr(moneda,"\"","");
					numDocto		= Comunes.strtr(numDocto,"\"","");
					importeDocto	= Comunes.strtr(importeDocto,"\"","");
					importeDocto	= Comunes.strtr(importeDocto,",","");
					importeDscto	= Comunes.strtr(importeDscto,"\"","");
					importeDscto	= Comunes.strtr(importeDscto,",","");
					emisor 			= Comunes.strtr(emisor,"\"","");
					tipoCredito		= Comunes.strtr(tipoCredito,"\"","");
					ppc				= Comunes.strtr(ppc,"\"","");
					ppi 			= Comunes.strtr(ppi,"\"","");
					bieneServi 		= Comunes.strtr(bieneServi,"\"","");
					claseDocto 		= Comunes.strtr(claseDocto,"\"","");
					domicilioPago 	= Comunes.strtr(domicilioPago,"\"","");
					tasaInteresUF 	= Comunes.strtr(tasaInteresUF,"\"","");
					relMatUF 		= Comunes.strtr(relMatUF,"\"","");
					sobreTasaUF		= Comunes.strtr(sobreTasaUF,"\"","");
					tasaInteresI	= Comunes.strtr(tasaInteresI,"\"","");
					numAmort		= Comunes.strtr(numAmort,"\"","");
					tablaAmort 		= Comunes.strtr(tablaAmort,"\"","");
					fppc			= Comunes.strtr(fppc,"\"","");
					fppi			= Comunes.strtr(fppi,"\"","");
					diaPago			= Comunes.strtr(diaPago,"\"","");
					fechVencDocto	= Comunes.strtr(fechVencDocto,"\"","");
					fechVencDscto	= Comunes.strtr(fechVencDscto,"\"","");
					fetc			= Comunes.strtr(fetc,"\"","");
					lugarFirma		= Comunes.strtr(lugarFirma,"\"","");
					amortAjuste	 	= Comunes.strtr(amortAjuste,"\"","");
					tipoRenta		= Comunes.strtr(tipoRenta,"\"","");

					tipoRenta		= "'"+tipoRenta+"'";
					relMatI			="null";
					sobreTasaI		="null";
					numAmortRec 	="null";
					plazoMensual	="S";
				}

				//Validaci�n de la Clave de Sirac. Campo 1
				if (claveSirac.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 1: La Clave de Sirac es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
                    lbOK = false;
				} else if(lbOK){
					if (claveSirac.length()>7) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 1: La Clave de Sirac excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 7");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
					if (!Comunes.esNumero(claveSirac)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 1: La Clave de Sirac no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} /*else {
						lsCadenaSQL.delete(0, lsCadenaSQL.length());
						lsCadenaSQL.append("select in_numero_sirac from comcat_pyme where in_numero_sirac ="+claveSirac);
						lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
						if(!lrsSel.next()) {
							lsError.append(lsLineaError+" la Clave Sirac no existe para poder realizar operaciones.\n");
							lbOK = false;
						}
						lrsSel.close();
			            lodbConexion.cierraStatement();
					}*/
				}
				//Validaci�n para el N�mero de Sucursal del Banco. Campo 2
				if (noSucBanco.equals(""))
					noSucBanco = "null";
				else if(lbOK){
					if (!Comunes.esNumero(noSucBanco)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 2: El N�mero de Sucursal del Banco no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(noSucBanco.length()>10) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 2: El N�mero de Sucursal del Banco excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 10.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el tipo de Moneda. Campo 3
				if (moneda.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 3: La Clave de la Moneda es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!moneda.equals("1") && !moneda.equals("54")) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 3: No es V�lido el tipo de Moneda.");
						VecError.addElement("Claves de Moneda: 1,54.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el N�mero de Documento. Campo 4
				if (numDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 4: El N�mero de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if (!Comunes.esNumero(numDocto)&&lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: No es v�lido el N�mero de Documento.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
				} else if(numDocto.length()>10&&lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: El N�mero de Documento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 10.");
						VecErrores.addElement(VecError);
						lbOK = false;
				}
				//Valida si existe el N�mero de Documento. Campo 4
				if(lbOK){
					try{
						existeNoDocto(numDocto, NoIf, ""+licProcSolic, claveSirac, lodbConexion);
					}catch(NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: N�mero de Documento repetido.");
						VecError.addElement("Cambie el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Importe de Documento. Campo 5
				if (importeDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 5: El Importe de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if (!Comunes.esDecimal(importeDocto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDocto.length() > 22) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe de Documento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 22.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDocto.indexOf(".")!=-1) {
						if (importeDocto.substring(0, importeDocto.indexOf(".")).length() > 19 || importeDocto.substring(importeDocto.indexOf(".")+1,importeDocto.length()).length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido.");
							VecError.addElement("El dato no puede tener m�s de 19 enteros y 2 decimales.");
							lbOK = false;
						}
					}

					if(Double.parseDouble(importeDocto)<=0 && lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe del Documento debe ser mayor a cero.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Importe de Descuento. Campo 6
				if (importeDscto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 6: El Importe de Descuento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if (!Comunes.esDecimal(importeDscto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDscto.length() > 22) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe de Descuento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 22.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDscto.indexOf(".")!=-1) {
						if (importeDscto.substring(0, importeDscto.indexOf(".")).length() > 19 || importeDscto.substring(importeDscto.indexOf(".")+1,importeDscto.length()).length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido.");
							VecError.addElement("El dato no puede tener m�s de 19 enteros y 2 decimales.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
					if(Double.parseDouble(importeDscto)<=0) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe del Descuento debe ser mayor a cero.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if((Double.parseDouble(importeDscto) > Double.parseDouble(importeDocto))) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe de Descuento no puede ser mayor al Importe de Documento.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Clave del Emisor. Campo 7
				if (!emisor.equals("")&&lbOK) {
					if(!Comunes.esNumero(emisor)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 7: La Clave del Emisor no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icEmisor = sValidaEmisor(emisor, lodbConexion);
	        	        } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 7: La Clave del Emisor no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Emisores.");
							VecErrores.addElement(VecError);
						lbOK = false;
		                }
					}
				} else
					icEmisor = "null";
				//Validaci�n para el Tipo de Cr�dito. Campo 8
				if(tipoCredito.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tipoCredito)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoCredito = sValidaTipoCredito(tipoCredito, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tipos de Cr�dito.");
							VecErrores.addElement(VecError);
						lbOK = false;
		                }
					}
				}
				//Validaci�n para la Periodicidad del Pago de Capital. Campo 9
				if(ppc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(ppc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoPPC = sValidaPeriodicidadPP(ppc, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Periodicidades.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para la Periodicidad del Pago de Intereses. Campo 10
				if(ppi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(ppi)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoPPI = sValidaPeriodicidadPP(ppi, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Periodicidades.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n de los Bienes y Servicios. Campo 11
				if(bieneServi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 11: La Descripci�n de los Bienes y Servicios es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					bieneServi=(bieneServi.length()>60)?"'"+bieneServi.substring(0,59)+"'":"'"+bieneServi+"'";
				//Validaci�n de la Clase de Documento. Campo 12
				if(claseDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 12: La Clave de la Clase de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(claseDocto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 12: La Clave de la Clase de Documento no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icClaseDocto = sValidaClaseDocto(claseDocto, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 12: La Clave de la Clase de Documento no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Clases de Documento.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para el Domicilio de Pago. Campo 13
				if (domicilioPago.equals("")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 13: El Domicilio de Pago es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					domicilioPago=(domicilioPago.length()>60)?"'"+domicilioPago.substring(0,59)+"'":"'"+domicilioPago+"'";
				//Validaci�n para la Tasa de Interes del Usuario Final. Campo 14
				if(tasaInteresUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final es un campo requerido.");
						VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
						VecErrores.addElement(VecError);
						lbOK = false;
				}else if(tasaInteresUF.equals("")){
					icTasaUF = "null";
				}else if(lbOK){
					if(!Comunes.esNumero(tasaInteresUF)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTasaUF = sValidaTasaInteresIntermed(tasaInteresUF,"N",lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tasas de Inter�s.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para la Relaci�n Matem�tica del Usuario Final. Campo 15
				if(relMatUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 15: La Relaci�n Matem�tica del Usuario Final es un campo requerido.");
					VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}else if(relMatUF.equals("")){
					relMatUF = "null";
				}else if(!relMatUF.equals("+") /*&& !relMatUF.equals("-") && !relMatUF.equals("*") && !relMatUF.equals("/")*/&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 15: La Relaci�n Matem�tica del Usuario Final no es v�lida.");
					VecError.addElement("Dato v�lido: +");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					relMatUF = "'"+relMatUF+"'";
				//Validaci�n para la SobreTasa del Usuario Final. Campo 16
				if(sobreTasaUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 16: La SobreTasa del Usuario Final es un campo requerido.");
					VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}else if(sobreTasaUF.equals("")){
					sobreTasaUF = "null";
				}else if(lbOK){
					if(!Comunes.esDecimal(sobreTasaUF)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final no es v�lida.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(sobreTasaUF.length() > 7) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2 enteros y 4 decimales.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(sobreTasaUF.indexOf(".")==-1) {
						if(sobreTasaUF.length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida de enteros.");
							VecError.addElement("Longitud m�xima permitida: 2 enteros.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					} else if(sobreTasaUF.substring(0,sobreTasaUF.indexOf(".")).length() > 2 || sobreTasaUF.substring(sobreTasaUF.indexOf(".")+1, sobreTasaUF.length()).length() > 4) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2 enteros y 4 decimales.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Tasa de Interes del Intermediario. Campo 17
				if(tasaInteresI.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 17: La Tasa de Interes del Intermediario es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tasaInteresI)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 17: La Clave de la Tasa de Interes del Intermediario no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTasaIntermed = sValidaTasaInteresIntermed(tasaInteresI,"S",lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tasas de Inter�s habilitadas en Cr�dito Electr�nico.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para el N�mero de Amortizaciones. Campo 18
				if(numAmort.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(!Comunes.esNumero(numAmort)&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones no es v�lido.");
					VecError.addElement("Dato num�rico.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(numAmort.length() > 3 && lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones excede la longitud permitida.");
					VecError.addElement("Longitud m�xima permitida: 3.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}
				//Validaci�n para el Tipo de Amortizaci�n. Campo 19
				if(tablaAmort.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tablaAmort)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTablaAmort = sValidaTablaAmortizacion(tablaAmort,lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tipos de Amortizaci�n.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Para validaciones de fechas.
				java.util.Date fechaHoy = new java.util.Date();
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				//Validaci�n para la Fecha del Primer Pago de Capital. Campo 20
				if(fppc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 20: La Fecha del Primer Pago de Capital es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fppc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 20: La Fecha del Primer Pago de Capital no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fPPC=Comunes.parseDate(fppc);
						int iComp = fPPC.compareTo(fechaHoy);
						if (iComp < 0 && !fppc.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital "+fppc+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para la Fecha de Primer Pago de Interes. Campo 21
				if(fppi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 21: La Fecha del Primer Pago de Inter�s es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fppi)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 21: La Fecha del Primer Pago de Inter�s no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fPPI=Comunes.parseDate(fppi);
						int iComp = fPPI.compareTo(fechaHoy);
						if (iComp < 0 && !fppi.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s "+fppi+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para el D�a de Pago. Campo 22
				if(diaPago.equals("")&&lbOK)
					fppc.substring(0,2);
				else if(lbOK){
					if(!Comunes.esNumero(diaPago)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 22: El D�a de Pago no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(diaPago.length()>2) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 22: El D�a de Pago excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Fecha de Vencimiento del Documento. Campo 23
				if(fechVencDocto.equals("")){
					fechVencDocto = "";
				} else if(!Comunes.checaFecha(fechVencDocto)&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 23: La Fecha de Vencimiento del Documento no es una fecha correcta.");
					VecError.addElement("Formato v�lido: dd/mm/yyyy");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					java.util.Date fechaVenc=Comunes.parseDate(fechVencDocto);
					int iComp = fechaVenc.compareTo(fechaHoy);
					if (iComp < 0 && !fechVencDocto.equals(sdf.format(fechaHoy)) ) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 23: La Fecha de Vencimiento del Documento "+fechVencDocto+" es menor a la Fecha de Hoy.");
						VecError.addElement("Verifique el dato capturado");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Fecha de Vencimiento del Descuento. Campo 24
				if(fechVencDscto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fechVencDscto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fechaVenc=Comunes.parseDate(fechVencDscto);
						int iComp = fechaVenc.compareTo(fechaHoy);
						if (iComp < 0 && !fechVencDscto.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento "+fechVencDscto+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito. Campo 25
				if(fetc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fetc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Lugar de Firma. Campo 26
				if(lugarFirma.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 26: El Lugar de Firma es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					lugarFirma = (lugarFirma.length()>50)?"'"+lugarFirma.substring(0,49)+"'":"'"+lugarFirma+"'";
				//Validaci�n para la Amortizaci�n de Ajuste. Campo 27
				amortImporte=""; amortFecha="";
               	lovTablaAmort = null;
               	if(!amortAjuste.equals("")&&lbOK){
               		if(!(amortAjuste.equals("P")||amortAjuste.equals("U"))){
	               		VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 27: El Tipo de Amortizaci�n de Ajuste no es v�lido.");
						VecError.addElement("Tipos de Amortizaci�n de Ajuste: 'P' Primero, 'U' �ltimo.");
						VecErrores.addElement(VecError);
						lbOK = false;
	               	} else {
						try {
							lovTablaAmort = ovValidaAmortizacion(amortAjuste,lovDatos);
	                        if (lovTablaAmort.size() > 0){
	                        	amortAjuste  = lovTablaAmort.elementAt(0).toString();
	                            amortImporte = lovTablaAmort.elementAt(1).toString();
	                            amortFecha 	 = lovTablaAmort.elementAt(2).toString();
	                        }
	        	        } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 27: Las Amortizaciones no est�n completas.");
							VecError.addElement("Inserte N�mero, Importe, Fecha y Tipo por cada Amortizaci�n de Ajuste");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
						if((amortImporte.equals("") || amortFecha.equals(""))&&lbOK) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 27: Los Campos Din�micos de las Amortizaciones no est�n completos.");
							VecError.addElement("Verifique el Importe y Fecha de cada Amortizaci�n de Ajuste");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				} else {
					amortImporte = "null";
					amortAjuste = "null";
				}
				//Validaci�n para el Tipo de Renta. Campo 28 (Agregado MRZL)
				if(tablaAmort.equals("5")&&lbOK){
					if((!tipoRenta.equals("''"))){
	               		if(!(tipoRenta.equals("'N'")||tipoRenta.equals("'S'"))){
		               		VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 28: El Tipo de Renta no es v�lido.");
							VecError.addElement("Tipos de Renta: 'S' S�, 'N' No.");
							VecErrores.addElement(VecError);
							lbOK = false;
		               	}
					} else {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 28: El Tipo de Renta es un campo requerido.");
						VecError.addElement("Es obligatorio con Tipo de Amortizaci�n: Plan de Pagos. Tipos de Renta: 'S' S�, 'N' No.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaciones en Bases de Operaci�n (Agregado MRZL)
				if(lbOK){
					try{
						lovTmpBO = sGetDatosBO(NoIf,iTipoPiso,TipoPlazo,lodbConexion);
					} catch(Exception e){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("No existen registros parametrizados en Bases de Operaci�n para el Tipo de Plazo elegido.");
						VecError.addElement("Consulte las Bases de Operaci�n de Cr�dito Electr�nico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Emisor con las Bases de Operaci�n. Campo 7
				if(lbOK){
					Vector tmpicEmisor = new Vector();
					String tmpEmisores = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicEmisor = (Vector) lovTmpBO.elementAt(i);

						if (icEmisor.equals(tmpicEmisor.elementAt(0)))
							lbOK = true;
						else {
							if(tmpEmisores.equals(""))
								tmpEmisores = "Claves de Emisor v�lidas: ";
							else
								tmpEmisores += ", ";

							tmpEmisores += tmpicEmisor.elementAt(0) +
											"-" + tmpicEmisor.elementAt(1);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 7: La Clave del Emisor no existe para el Tipo de Plazo elegido");
						VecError.addElement(tmpEmisores);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Tipo de Cr�dito con las Bases de Operaci�n. Campo 8
				if(lbOK){
					Vector tmpicTipoCred = new Vector();
					String tmpCreditos = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTipoCred = (Vector) lovTmpBO.elementAt(i);

						if (icTipoCredito.equals(tmpicTipoCred.elementAt(2)))
							lbOK = true;
						else {
							if(tmpCreditos.equals(""))
								tmpCreditos = "Claves de Tipo de Cr�dito v�lidas: ";
							else
								tmpCreditos += ", ";

							tmpCreditos += tmpicTipoCred.elementAt(2) +
											"-" + tmpicTipoCred.elementAt(3);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no existe para el Tipo de Plazo elegido y la Clave de Emisor capturada");
						VecError.addElement(tmpCreditos);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}

				//Valida Tasa de Inter�s del Intermediario con las Bases de Operaci�n. Campo 17
				if(lbOK){
					Vector tmpicTasaI = new Vector();
					String tmpTasas = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTasaI = (Vector) lovTmpBO.elementAt(i);

						if (icTasaIntermed.equals(tmpicTasaI.elementAt(4)))
							lbOK = true;
						else {
							if(tmpTasas.equals(""))
								tmpTasas = "Claves de Tasa de Inter�s del Intermediario v�lidas: ";
							else
								tmpTasas += ", ";

							tmpTasas += tmpicTasaI.elementAt(4) +
											"-" + tmpicTasaI.elementAt(5);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe para el Tipo de Plazo elegido y las Claves de Emisor y de Tipo de Cr�dito capturadas");
						VecError.addElement(tmpTasas);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Tipo de Amortizaci�n con las Bases de Operaci�n. Campo 19
				if(lbOK){
					Vector tmpicTablaAmort = new Vector();
					String tmpAmort = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTablaAmort = (Vector) lovTmpBO.elementAt(i);

						if (icTablaAmort.equals(tmpicTablaAmort.elementAt(6)))
							lbOK = true;
						else {
							if(tmpAmort.equals(""))
								tmpAmort = "Claves de Tipo de Amortizaci�n v�lidas: ";
							else
								tmpAmort += ", ";

							tmpAmort += tmpicTablaAmort.elementAt(6) +
											"-" + tmpicTablaAmort.elementAt(7);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no existe para el Tipo de Plazo elegido y las Claves de Emisor, Tipo de Cr�dito y Tasa de Inter�s del Intermediario capturadas");
						VecError.addElement(tmpAmort);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n de la Fecha de Primer Pago de Capital, Fecha de Primer Pago de Inter�s
				//y Fecha de Vencimiento del Descuento con las Bases de Operaci�n. Campos 20,21,24
				if(lbOK){
				//	System.out.println(fechVencDscto);
		            lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append("select trunc(TO_DATE('"+fppc+"','dd/mm/yyyy')) - trunc(SYSDATE),");
					lsCadenaSQL.append("trunc(TO_DATE('"+fppi+"','dd/mm/yyyy')) - trunc(SYSDATE),");
					lsCadenaSQL.append("trunc(TO_DATE('"+fechVencDscto+"','dd/mm/yyyy')) - trunc(SYSDATE) from dual");
					lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
					if (lrsSel.next()){
						PlazoFPPC = lrsSel.getInt(1);
						PlazoFPPI = lrsSel.getInt(2);
						PlazoFVD  = lrsSel.getInt(3);
					}
				//	System.out.println(Plazo);
		            lodbConexion.cierraStatement();
		         }
				// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
				if(lbOK) {	// Si paso bien todas las condiciones de tipos y valores se hace la validaci�n.z
					Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, lodbConexion);
					iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
					if(TipoPlazo.equals("C")) {	// Cr�dito.
						if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en la b.o. se manda el mensaje.
							if(iTipoPiso==1) {
								VecError = new Vector();
								VecError.addElement(""+liLineadocs);
								VecError.addElement("Campo 24: No existe Plazo M�ximo.");
								VecError.addElement("Verifique los par�metros de Bases de Operaci�n.");
								VecErrores.addElement(VecError);
								lbOK = false;
							}
							if(iTipoPiso==2) {
								VecError = new Vector();
								VecError.addElement(""+liLineadocs);
								VecError.addElement("Campo 24: No existe Plazo M�ximo.");
								VecError.addElement("Favor de comunicarse al 01 800 CADENAS.");
								VecErrores.addElement(VecError);
								lbOK = false;
							}
						} else if(PlazoFPPC > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						} else if(PlazoFPPI > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						} else if(PlazoFVD > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					} else if(TipoPlazo.equals("F")) {	//	Factoraje.
						if(iPlazoMaxBO==0 && iTipoPiso==1) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: No existe Plazo M�ximo.");
							VecError.addElement("Verifique los par�metros de Bases de Operaci�n.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFPPC > iPlazoMaxBO) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFPPI > iPlazoMaxBO) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFVD > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
								/*
								//-----------------------------------------------------------------------------------
								Foda 001 2005
									-- Antes: si el plazo era mayor al plazo m�ximo, recortaba la fecha
									-- Nvo Req: si el plazo es mayor al plazo m�ximo, enviar mensaje de error
								//-----------------------------------------------------------------------------------
								GregorianCalendar cFechaVencDesc = new GregorianCalendar();
								cFechaVencDesc.add(Calendar.DATE, iPlazoMaxBO);	//Se Suma el plazo a la fecha de hoy.
								// Validamos si la fecha del plazo es d�a inhabil.
								Vector vDiasInhabiles = getDiasInhabiles(lodbConexion); // Obtenemos los d�as inhabiles.
								Hashtable hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO);
								cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
								Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
								boolean bCambioPlazo = false;
								bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								while(bCambioPlazo) {
									hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, Plazo);
									cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
									Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
									bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								}
								fechVencDscto = sdf.format(cFechaVencDesc.getTime());
								// Se reasigna la fecha PPC y la fecha PPI si alguna o ambas son mayores a la de Descuento.
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(fppc))) {
									fppc = sdf.format(cFechaVencDesc.getTime());
									diaPago = sdf.format(cFechaVencDesc.getTime()).substring(0,2);
								}
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(fppi)))
									fppi = sdf.format(cFechaVencDesc.getTime());

								sTomoPlazoGral = hPlazoBO.get("sTomoPlazoGral").toString();
								*/
							} // Plazo > iPlazoMaxBO
					} // TipoPlazo.equals("F")
				} // lbOK

				//Validaci�n de Fecha M�xima de Amortizaci�n Recortada. Campo 28 = R
				String fechaMax = "";
				if(lbOK){
					fechaMax = getFechaMaxRecortado(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, lodbConexion);
					System.out.println("=====LA FECHA MAX = -"+ fechaMax +"-");
				}
				//System.out.println(fechVencDscto);
				System.out.println("tablaAmort: "+tablaAmort+ "lbOK: "+lbOK);
				if("5".equals(tablaAmort)&&lbOK){
					Vector vecCampos = new Vector();
					String fecMaxVenc = "";
					try {
						lsCadenaSQL.delete(0, lsCadenaSQL.length());
						lsCadenaSQL.append("select to_char(sysdate+"+iPlazoMaxBO+",'dd/mm/yyyy') from dual");
						lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
						if (lrsSel.next())
							fecMaxVenc = lrsSel.getString(1);
			            lodbConexion.cierraStatement();
						vecCampos = ovObtenerCamposDeAmort(lovDatos,fechaMax,fecMaxVenc,iPlazoMaxBO);
					} catch (NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo "+campoError+": "+error);
						VecError.addElement(solucion);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
System.out.println("tipoRenta:"+tipoRenta);
					if("'N'".equals(tipoRenta)&&lbOK){
						fppc			= (String)vecCampos.get(0);
						fppi			= (String)vecCampos.get(1);
						fechVencDscto	= (String)vecCampos.get(2);
						diaPago			= (String)vecCampos.get(3);

						fppc			= Comunes.strtr(fppc,"\"","");
						fppi			= Comunes.strtr(fppi,"\"","");
						fechVencDscto	= Comunes.strtr(fechVencDscto,"\"","");
						diaPago			= Comunes.strtr(diaPago,"\"","");

					}else if(lbOK){
System.out.println("lbOK:"+lbOK);
						String diappi	= fppi.substring(0,2);
						String diavenc	= fechVencDscto.substring(0,2);
System.out.println("diappi:"+diappi);
System.out.println("diavenc:"+diavenc);
						if(!diappi.equals(diavenc)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: Los d�as de pago deben ser iguales para todas las amortizaciones");
							VecError.addElement("D�a de Pago Fecha de Primer Pago de Interes = D�a de Pago Fecha de Vencimiento del Descuento.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
System.out.println("ppc:"+ppc);
						if("14".equals(ppc)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 9: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
							VecError.addElement(sGetPeriodicidad(lodbConexion));
							VecErrores.addElement(VecError);
							lbOK = false;
						}
System.out.println("ppi:"+ppi);
						if("14".equals(ppi)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 10: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
							VecError.addElement(sGetPeriodicidad(lodbConexion));
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
					if(lbOK){
						numAmortRec		= (String)vecCampos.get(4);
						plazoMensual	= (String)vecCampos.get(5);

						numAmortRec		= Comunes.strtr(numAmortRec,"\"","");
						plazoMensual	= Comunes.strtr(plazoMensual,"\"","");
					}
				}else{
					tipoRenta = "null";
				}

				String icSolicPortal = "";
				if(lbOK){
					lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append(
						" select NVL(MAX(ic_solic_portal),0)+1"+
						" from comtmp_solic_portal");
					lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
					if(lrsSel.next()){
						icSolicPortal = lrsSel.getString(1);
					}
					lodbConexion.cierraStatement();

					lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append(
							"insert into comtmp_solic_portal (ic_solic_portal,ic_proc_solic, ic_if, ig_clave_sirac, "+
							"ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, "+
							"fn_importe_dscto, ic_emisor, ic_tipo_credito, ic_periodicidad_c, "+
							"ic_periodicidad_i, cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, "+
							"ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, "+
							"in_numero_amort, ic_tabla_amort, df_ppc, df_ppi, in_dia_pago, "+
							"df_v_documento, df_v_descuento, ig_plazo, df_etc, cg_lugar_firma, "+
							"cs_amort_ajuste, fg_importe_ajuste, cs_tipo_plazo, cs_plazo_maximo_factoraje,cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual) "+
							" values("+icSolicPortal+","+licProcSolic+","+NoIf+","+claveSirac+", "+noSucBanco+", "+moneda+", "+
							numDocto+", "+importeDocto+", "+importeDscto+", "+icEmisor+", "+
							icTipoCredito+", "+icTipoPPC+", "+icTipoPPI+", "+bieneServi+", "+
							icClaseDocto+", "+domicilioPago+", "+icTasaUF+", "+relMatUF+", "+
							sobreTasaUF+", "+icTasaIntermed+", "+relMatI+", "+sobreTasaI+", "+
							numAmort+", "+icTablaAmort+", "+
							"TO_DATE('"+fppc+"','DD/MM/YYYY'), TO_DATE('"+fppi+"','DD/MM/YYYY'), "+
							diaPago+", "+(fechVencDocto.equals("")?"null":"TO_DATE('"+fechVencDocto+"','DD/MM/YYYY')")+", "+
							"TO_DATE('"+fechVencDscto+"','DD/MM/YYYY'), "+PlazoFVD+", "+
							"TO_DATE('"+fetc+"','DD/MM/YYYY'), "+lugarFirma+", "+amortAjuste+", "+
							amortImporte+",'"+TipoPlazo+"','"+sTomoPlazoGral+"',"+tipoRenta+","+numAmortRec+",'"+plazoMensual+"' )"
					);

					try {
						System.out.println(lsCadenaSQL.toString());
						lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
	                } catch (Exception error) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Error de Concurrencia, hay registros Repetidos.");
						VecError.addElement("Verifique los datos capturados");
						VecErrores.addElement(VecError);
						lbOK = false;
						error.printStackTrace();
					}
				}

				// Valida e inserta las amortizaciones
System.out.println("tipoRenta: "+tipoRenta);
System.out.println("lbOK: "+lbOK);
				if("'N'".equals(tipoRenta)&&lbOK){
					//System.out.println("Validaci�n de amortizaciones");
					try{
System.out.println("icSolicPortal: "+icSolicPortal);
						ovGenerarAmortizaciones(icSolicPortal,lovDatos,fechaMax,lodbConexion,licProcSolic);
System.out.println("Despues de cargar amortizaciones");
					} catch(NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						if(TA){
							VecError.addElement("Campo "+campoError+": El Tipo de Amortizaci�n no es v�lido");
							VecError.addElement("Tipos de Amortizaci�n v�lidos: 'A' Capital e Intereses, 'I' Intereses, 'R' Recortada");
						} else {
							VecError.addElement("Campo "+campoError+": "+ne.getMsgError());
							if(ne.getCodError().equals("CRED0012")||ne.getCodError().equals("CRED0013")){
								VecError.addElement("Verifique los montos de las amortizaciones tipo 'A'");
							}else{
								VecError.addElement("Verifique los datos capturados");
							}
						}
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}

				if(lbOK){
					VecProc = new Vector();
					VecProc.addElement(""+liLineadocs);
					VecProc.addElement(NoIf);
					VecProc.addElement(numDocto);
					VecProcs.addElement(VecProc);
					bdMtoDoctos = bdMtoDoctos.add(new BigDecimal(importeDocto));
					bdMtoDsctos = bdMtoDsctos.add(new BigDecimal(importeDscto));
				}

				lodbConexion.terminaTransaccion(lbOK);

            }//fin del for (mientras lofArchivo != null)

			lovResultado.addElement(VecErrores);    //Registros con error
			lovResultado.addElement(VecProcs);		//Registros procesados
			lovResultado.addElement(bdMtoDoctos);	//Monto Documentos
			lovResultado.addElement(bdMtoDsctos);	//Monto Descuentos
			lovResultado.addElement(new Integer(licProcSolic));		//Clave

			System.out.println("Termino");
			return lovResultado;
		} catch(NafinException ne){
			throw ne;
		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			epError.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(bBotonProcesar);
			if (lodbConexion.hayConexionAbierta()) {
				lodbConexion.cierraConexionDB();
			}
			System.out.println(" AutorizacionSolicitudEJB::ovProcesarArchivo(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovProcesarArchivoXLS()   [ Carga Masiva ]
	*
	*********************************************************************************************/
    // Agregado 01/11/2005	--MRZL
	public Vector ovProcesarArchivoXLS(List lDatos,String NoIf,String TipoPlazo)
	throws NafinException	{
		System.out.println(" AutorizacionSolicitudEJB::ovProcesarArchivoXLS(E)");

		AccesoDB lodbConexion = new AccesoDB();
		ResultSet lrsSel = null;
		StringBuffer lsCadenaSQL   	= new StringBuffer();

		int iTipoPiso =0;
		boolean lbOK = true;
		boolean bBotonProcesar = true;

		List lovDatos 		= new ArrayList();
		Vector lovResultado = new Vector();
		Vector lovTablaAmort= new Vector();
		Vector lovTmpBO		= new Vector();
		Vector VecError		= new Vector();
		Vector VecErrores 	= new Vector();
		Vector VecProc 		= new Vector();
		Vector VecProcs 	= new Vector();

		BigDecimal bdMtoDoctos = new BigDecimal("0.0");
		BigDecimal bdMtoDsctos = new BigDecimal("0.0");

		String claveSirac="", noSucBanco="", moneda="", numDocto="";
		String importeDocto="", importeDscto="", emisor="", tipoCredito="", ppc="", ppi="";
		String claseDocto="", domicilioPago="", tasaInteresUF="", relMatUF="", sobreTasaUF="";
		String tasaInteresI="", relMatI="", sobreTasaI="", tablaAmort="", numAmort="";
		String fppc="", fppi="", bieneServi="", fechVencDocto="", fechVencDscto	="";
		String diaPago="", fetc="", lugarFirma="", amortAjuste="", amortImporte="", amortFecha="";
		String icEmisor="", icTipoCredito="", icTipoPPC="";
		String icTipoPPI="", icClaseDocto="", icTasaUF="", icTasaIntermed="", icTablaAmort="";
		String tipoRenta="";
		String numAmortRec="";
		String plazoMensual="";
		String sTomoPlazoGral = "N";

		int PlazoFPPC=0;
		int PlazoFPPI=0;
		int PlazoFVD=0;
		int iPlazoMaxBO=0;
		int liLineadocs=1;
		int licProcSolic = 0;

		try {
			lodbConexion.conexionDB();

			//Obtiene el Tipo de Piso
			iTipoPiso = getTipoPisoIF(NoIf, lodbConexion);

			//Obtiene el maximo de ic_proc_solic.
			lsCadenaSQL.delete(0, lsCadenaSQL.length());
			lsCadenaSQL.append("select (NVL(max(ic_proc_solic),0)+1) from comtmp_solic_portal");
			lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
			if(lrsSel.next())
				licProcSolic = (lrsSel.getInt(1)==0) ?1 : lrsSel.getInt(1);
			lodbConexion.cierraStatement();

			for(liLineadocs = 1;liLineadocs<lDatos.size();liLineadocs++){
				System.out.println("Valida Linea "+liLineadocs);
				lbOK = true;
				lovDatos = (ArrayList) lDatos.get(liLineadocs);

				// Valida el n�mero de datos de la l�nea
				if(lovDatos.size() < 26) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("No coincide con el layout requerido.");
					VecError.addElement("Verifique el n�mero de datos.");
					VecErrores.addElement(VecError);
                    lbOK = false;
				} else {
					claveSirac		=(lovDatos.size()>=1)?Comunes.quitaComitasSimples(lovDatos.get(0).toString().trim()).trim():"";
					noSucBanco		=(lovDatos.size()>=2)?Comunes.quitaComitasSimples(lovDatos.get(1).toString().trim()).trim():"";
					moneda			=(lovDatos.size()>=3)?Comunes.quitaComitasSimples(lovDatos.get(2).toString().trim()).trim():"";
					numDocto		=(lovDatos.size()>=4)?Comunes.quitaComitasSimples(lovDatos.get(3).toString().trim()).trim():"";
					importeDocto	=(lovDatos.size()>=5)?Comunes.quitaComitasSimples(lovDatos.get(4).toString().trim()).trim():"";
					importeDscto	=(lovDatos.size()>=6)?Comunes.quitaComitasSimples(lovDatos.get(5).toString().trim()).trim():"";
					emisor 			=(lovDatos.size()>=7)?Comunes.quitaComitasSimples(lovDatos.get(6).toString().trim()).trim():"";
					tipoCredito		=(lovDatos.size()>=8)?Comunes.quitaComitasSimples(lovDatos.get(7).toString().trim()).trim():"";
					ppc				=(lovDatos.size()>=9)?Comunes.quitaComitasSimples(lovDatos.get(8).toString().trim()).trim():"";
					ppi 			=(lovDatos.size()>=10)?Comunes.quitaComitasSimples(lovDatos.get(9).toString().trim()).trim():"";
					bieneServi 		=(lovDatos.size()>=11)?Comunes.quitaComitasSimples(lovDatos.get(10).toString().trim()).trim():"";
					claseDocto 		=(lovDatos.size()>=12)?Comunes.quitaComitasSimples(lovDatos.get(11).toString().trim()).trim():"";
					domicilioPago 	=(lovDatos.size()>=13)?Comunes.quitaComitasSimples(lovDatos.get(12).toString().trim()).trim():"";
					tasaInteresUF 	=(lovDatos.size()>=14)?Comunes.quitaComitasSimples(lovDatos.get(13).toString().trim()).trim():"";
					relMatUF 		=(lovDatos.size()>=15)?Comunes.quitaComitasSimples(lovDatos.get(14).toString().trim()).trim():"";
					sobreTasaUF		=(lovDatos.size()>=16)?Comunes.quitaComitasSimples(lovDatos.get(15).toString().trim()).trim():"";
					tasaInteresI	=(lovDatos.size()>=17)?Comunes.quitaComitasSimples(lovDatos.get(16).toString().trim()).trim():"";
					numAmort		=(lovDatos.size()>=18)?Comunes.quitaComitasSimples(lovDatos.get(17).toString().trim()).trim():"";
					tablaAmort 		=(lovDatos.size()>=19)?Comunes.quitaComitasSimples(lovDatos.get(18).toString().trim()).trim():"";
					fppc			=(lovDatos.size()>=20)?Comunes.quitaComitasSimples(lovDatos.get(19).toString().trim()).trim():"";
					fppi			=(lovDatos.size()>=21)?Comunes.quitaComitasSimples(lovDatos.get(20).toString().trim()).trim():"";
					diaPago			=(lovDatos.size()>=22)?Comunes.quitaComitasSimples(lovDatos.get(21).toString().trim()).trim():"";
					fechVencDocto	=(lovDatos.size()>=23)?Comunes.quitaComitasSimples(lovDatos.get(22).toString().trim()).trim():"";
					fechVencDscto	=(lovDatos.size()>=24)?Comunes.quitaComitasSimples(lovDatos.get(23).toString().trim()).trim():"";
					fetc			=(lovDatos.size()>=25)?Comunes.quitaComitasSimples(lovDatos.get(24).toString().trim()).trim():"";
					lugarFirma		=(lovDatos.size()>=26)?Comunes.quitaComitasSimples(lovDatos.get(25).toString().trim()).trim():"";
					amortAjuste	 	=(lovDatos.size()>=27)?Comunes.quitaComitasSimples(lovDatos.get(26).toString().trim()).trim():"";
					tipoRenta		=(lovDatos.size()>=28)?Comunes.quitaComitasSimples(lovDatos.get(27).toString().trim()).trim():"";

					// eliminamos las comillas de todos los campos y las comas de los numericos
					claveSirac		= Comunes.strtr(claveSirac,"\"","");
					noSucBanco		= Comunes.strtr(noSucBanco,"\"","");
					moneda			= Comunes.strtr(moneda,"\"","");
					numDocto		= Comunes.strtr(numDocto,"\"","");
					importeDocto	= Comunes.strtr(importeDocto,"\"","");
					importeDocto	= Comunes.strtr(importeDocto,",","");
					importeDscto	= Comunes.strtr(importeDscto,"\"","");
					importeDscto	= Comunes.strtr(importeDscto,",","");
					emisor 			= Comunes.strtr(emisor,"\"","");
					tipoCredito		= Comunes.strtr(tipoCredito,"\"","");
					ppc				= Comunes.strtr(ppc,"\"","");
					ppi 			= Comunes.strtr(ppi,"\"","");
					bieneServi 		= Comunes.strtr(bieneServi,"\"","");
					claseDocto 		= Comunes.strtr(claseDocto,"\"","");
					domicilioPago 	= Comunes.strtr(domicilioPago,"\"","");
					tasaInteresUF 	= Comunes.strtr(tasaInteresUF,"\"","");
					relMatUF 		= Comunes.strtr(relMatUF,"\"","");
					sobreTasaUF		= Comunes.strtr(sobreTasaUF,"\"","");
					tasaInteresI	= Comunes.strtr(tasaInteresI,"\"","");
					numAmort		= Comunes.strtr(numAmort,"\"","");
					tablaAmort 		= Comunes.strtr(tablaAmort,"\"","");
					fppc			= Comunes.strtr(fppc,"\"","");
					fppi			= Comunes.strtr(fppi,"\"","");
					diaPago			= Comunes.strtr(diaPago,"\"","");
					fechVencDocto	= Comunes.strtr(fechVencDocto,"\"","");
					fechVencDscto	= Comunes.strtr(fechVencDscto,"\"","");
					fetc			= Comunes.strtr(fetc,"\"","");
					lugarFirma		= Comunes.strtr(lugarFirma,"\"","");
					amortAjuste	 	= Comunes.strtr(amortAjuste,"\"","");
					tipoRenta		= Comunes.strtr(tipoRenta,"\"","");

					tipoRenta		= "'"+tipoRenta+"'";
					relMatI			= "null";
					sobreTasaI		= "null";
					numAmortRec 	= "null";
					plazoMensual	= "S";
				}

				//Validaci�n de la Clave de Sirac. Campo 1
				//System.out.println("Clave Sirac: "+claveSirac);
				if (claveSirac.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 1: La Clave de Sirac es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
                    lbOK = false;
				} else if(lbOK){
					if (claveSirac.length()>7) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 1: La Clave de Sirac excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 7");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
					if (!Comunes.esNumero(claveSirac)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 1: La Clave de Sirac no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} /*else {
						lsCadenaSQL.delete(0, lsCadenaSQL.length());
						lsCadenaSQL.append("select in_numero_sirac from comcat_pyme where in_numero_sirac ="+claveSirac);
						lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
						if(!lrsSel.next()) {
							lsError.append(lsLineaError+" la Clave Sirac no existe para poder realizar operaciones.\n");
							lbOK = false;
						}
						lrsSel.close();
			            lodbConexion.cierraStatement();
					}*/
				}
				//Validaci�n para el N�mero de Sucursal del Banco. Campo 2
				//System.out.println("N�mero de Sucursal del Banco: "+noSucBanco);
				if (noSucBanco.equals(""))
					noSucBanco = "null";
				else if(lbOK){
					if (!Comunes.esNumero(noSucBanco)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 2: El N�mero de Sucursal del Banco no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(noSucBanco.length()>10) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 2: El N�mero de Sucursal del Banco excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 10.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el tipo de Moneda. Campo 3
				//System.out.println("Moneda: "+moneda);
				if (moneda.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 3: La Clave de la Moneda es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!moneda.equals("1") && !moneda.equals("54")) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 3: No es V�lido el tipo de Moneda.");
						VecError.addElement("Claves de Moneda: 1,54.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el N�mero de Documento. Campo 4
				//System.out.println("No. Documento: "+numDocto);
				if (numDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 4: El N�mero de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if (!Comunes.esNumero(numDocto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: No es v�lido el N�mero de Documento.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(numDocto.length()>10) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: El N�mero de Documento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 10.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida si existe el N�mero de Documento. Campo 4
				if(lbOK){
					try{
						existeNoDocto(numDocto,NoIf,""+licProcSolic,claveSirac,lodbConexion);
					}catch(NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 4: N�mero de Documento repetido.");
						VecError.addElement("Cambie el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Importe de Documento. Campo 5
				//System.out.println("Importe Docto: "+importeDocto);
				if (importeDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 5: El Importe de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if (!Comunes.esDecimal(importeDocto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDocto.length() > 22) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe de Documento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 22.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDocto.indexOf(".")!=-1) {
						if (importeDocto.substring(0, importeDocto.indexOf(".")).length() > 19 || importeDocto.substring(importeDocto.indexOf(".")+1,importeDocto.length()).length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido.");
							VecError.addElement("El dato no puede tener m�s de 19 enteros y 2 decimales.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}

					if(Double.parseDouble(importeDocto) <= 0.0 && lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 5: El Importe del Documento debe ser mayor a cero.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Importe de Descuento. Campo 6
				//System.out.println("Importe Descto: "+importeDscto);
				if (importeDscto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 6: El Importe de Descuento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if (!Comunes.esDecimal(importeDscto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDscto.length() > 22) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe de Descuento excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 22.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(importeDscto.indexOf(".")!=-1) {
						if (importeDscto.substring(0, importeDscto.indexOf(".")).length() > 19 || importeDscto.substring(importeDscto.indexOf(".")+1,importeDscto.length()).length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido.");
							VecError.addElement("El dato no puede tener m�s de 19 enteros y 2 decimales.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}

					if(Double.parseDouble(importeDscto) <= 0.0 && lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe del Descuento debe ser mayor a cero.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if((Double.parseDouble(importeDscto) > Double.parseDouble(importeDocto)) && lbOK) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 6: El Importe de Descuento no puede ser mayor al Importe de Documento.");
						VecError.addElement("Verifique el dato capturado.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Clave del Emisor. Campo 7
				//System.out.println("Emisor: "+emisor);
				if (!emisor.equals("")&&lbOK) {
					if(!Comunes.esNumero(emisor)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 7: La Clave del Emisor no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icEmisor = sValidaEmisor(emisor, lodbConexion);
	        	        } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 7: La Clave del Emisor no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Emisores.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				} else
					icEmisor = "null";
				//Validaci�n para el Tipo de Cr�dito. Campo 8
				//System.out.println("Tipo Credito: "+tipoCredito);
				if(tipoCredito.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tipoCredito)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoCredito = sValidaTipoCredito(tipoCredito, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tipos de Cr�dito.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para la Periodicidad del Pago de Capital. Campo 9
				//System.out.println("PPC: "+ppc);
				if(ppc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(ppc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoPPC = sValidaPeriodicidadPP(ppc, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 9: La Clave de la Periodicidad del Pago de Capital no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Periodicidades.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para la Periodicidad del Pago de Intereses. Campo 10
				//System.out.println("PPI: "+ppi);
				if(ppi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(ppi)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTipoPPI = sValidaPeriodicidadPP(ppi, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 10: La Clave de la Periodicidad del Pago de Intereses no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Periodicidades.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n de los Bienes y Servicios. Campo 11
				//System.out.println("Bienes: "+bieneServi);
				if(bieneServi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 11: La Descripci�n de los Bienes y Servicios es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					bieneServi=(bieneServi.length()>60)?"'"+bieneServi.substring(0,59)+"'":"'"+bieneServi+"'";
				//Validaci�n de la Clase de Documento. Campo 12
				//System.out.println("Clase Documento: "+claseDocto);
				if(claseDocto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 12: La Clave de la Clase de Documento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(claseDocto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 12: La Clave de la Clase de Documento no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icClaseDocto = sValidaClaseDocto(claseDocto, lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 12: La Clave de la Clase de Documento no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Clases de Documento.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para el Domicilio de Pago. Campo 13
				//System.out.println("Domicilio: "+domicilioPago);
				if (domicilioPago.equals("")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 13: El Domicilio de Pago es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					domicilioPago=(domicilioPago.length()>60)?"'"+domicilioPago.substring(0,59)+"'":"'"+domicilioPago+"'";
				//Validaci�n para la Tasa de Interes del Usuario Final. Campo 14
				//System.out.println("Tasa Interes UF: "+tasaInteresUF);
				if(tasaInteresUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final es un campo requerido.");
						VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
						VecErrores.addElement(VecError);
						lbOK = false;
				}else if(tasaInteresUF.equals("")){
					icTasaUF = "null";
				}else if(lbOK){
					if(!Comunes.esNumero(tasaInteresUF)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTasaUF = sValidaTasaInteresIntermed(tasaInteresUF,"N",lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tasas de Inter�s.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para la Relaci�n Matem�tica del Usuario Final. Campo 15
				//System.out.println("Relaci�n Mat UF: "+relMatUF);
				if(relMatUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 15: La Relaci�n Matem�tica del Usuario Final es un campo requerido.");
					VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}else if(relMatUF.equals("")){
					relMatUF = "null";
				}else if(!relMatUF.equals("+") /*&& !relMatUF.equals("-") && !relMatUF.equals("*") && !relMatUF.equals("/")*/&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 15: La Relaci�n Matem�tica del Usuario Final no es v�lida.");
					VecError.addElement("Dato v�lido: +");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					relMatUF = "'"+relMatUF+"'";
				//Validaci�n para la SobreTasa del Usuario Final. Campo 16
				//System.out.println("SobreTasa UF: "+sobreTasaUF);
				if(sobreTasaUF.equals("")&&TipoPlazo.equals("C")&&lbOK){
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 16: La SobreTasa del Usuario Final es un campo requerido.");
					VecError.addElement("Es obligatoria para operaciones de cr�dito. Inserte el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}else if(sobreTasaUF.equals("")){
					sobreTasaUF = "null";
				}else if(lbOK){
					if(!Comunes.esDecimal(sobreTasaUF)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final no es v�lida.");
						VecError.addElement("Dato num�rico decimal.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(sobreTasaUF.length() > 7) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2 enteros y 4 decimales.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(sobreTasaUF.indexOf(".")==-1) {
						if(sobreTasaUF.length() > 2) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida de enteros.");
							VecError.addElement("Longitud m�xima permitida: 2 enteros.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					} else if(sobreTasaUF.substring(0,sobreTasaUF.indexOf(".")).length() > 2 || sobreTasaUF.substring(sobreTasaUF.indexOf(".")+1, sobreTasaUF.length()).length() > 4) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 16: La SobreTasa del Usuario Final excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2 enteros y 4 decimales.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Tasa de Interes del Intermediario. Campo 17
				//System.out.println("Tasa Interes IF: "+tasaInteresI);
				if(tasaInteresI.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 17: La Tasa de Interes del Intermediario es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tasaInteresI)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 17: La Clave de la Tasa de Interes del Intermediario no es v�lida.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTasaIntermed = sValidaTasaInteresIntermed(tasaInteresI,"S",lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tasas de Inter�s habilitadas en Cr�dito Electr�nico.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Validaci�n para el N�mero de Amortizaciones. Campo 18
				//System.out.println("No. Amort.: "+numAmort);
				if(numAmort.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(!Comunes.esNumero(numAmort)&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones no es v�lido.");
					VecError.addElement("Dato num�rico.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(numAmort.length() > 3 && lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 18: El N�mero de Amortizaciones excede la longitud permitida.");
					VecError.addElement("Longitud m�xima permitida: 3.");
					VecErrores.addElement(VecError);
					lbOK = false;
				}
				//Validaci�n para el Tipo de Amortizaci�n. Campo 19
				//System.out.println("Tabla Amort: "+tablaAmort);
				if(tablaAmort.equals("")) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.esNumero(tablaAmort)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						try {
							icTablaAmort = sValidaTablaAmortizacion(tablaAmort,lodbConexion);
		                } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no existe en el Cat�logo.");
							VecError.addElement("Consulte el Cat�logo de Tipos de Amortizaci�n.");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
					}
				}
				//Para validaciones de fechas.
				java.util.Date fechaHoy = new java.util.Date();
				SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				//Validaci�n para la Fecha del Primer Pago de Capital. Campo 20
				//System.out.println("FPPC: "+fppc);
				if(fppc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 20: La Fecha del Primer Pago de Capital es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fppc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 20: La Fecha del Primer Pago de Capital no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fPPC=Comunes.parseDate(fppc);
						int iComp = fPPC.compareTo(fechaHoy);
						if (iComp < 0 && !fppc.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital "+fppc+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para la Fecha de Primer Pago de Interes. Campo 21
				//System.out.println("FPPI: "+fppi);
				if(fppi.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 21: La Fecha del Primer Pago de Inter�s es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fppi)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 21: La Fecha del Primer Pago de Inter�s no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fPPI=Comunes.parseDate(fppi);
						int iComp = fPPI.compareTo(fechaHoy);
						if (iComp < 0 && !fppi.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s "+fppi+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para el D�a de Pago. Campo 22
				//System.out.println("Dia Pago: "+diaPago);
				if(diaPago.equals("")&&lbOK)
					fppc.substring(0,2);
				else if(lbOK){
					if(!Comunes.esNumero(diaPago)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 22: El D�a de Pago no es v�lido.");
						VecError.addElement("Dato num�rico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else if(diaPago.length()>2) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 18: El D�a de Pago excede la longitud permitida.");
						VecError.addElement("Longitud m�xima permitida: 2.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Fecha de Vencimiento del Documento. Campo 23
				//System.out.println("Fecha Vencimiento Docto: "+fechVencDocto);
				if(fechVencDocto.equals("")){
					fechVencDocto = "";
				} else if(!Comunes.checaFecha(fechVencDocto)&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 23: La Fecha de Vencimiento del Documento no es una fecha correcta.");
					VecError.addElement("Formato v�lido: dd/mm/yyyy");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					java.util.Date fechaVenc=Comunes.parseDate(fechVencDocto);
					int iComp = fechaVenc.compareTo(fechaHoy);
					if (iComp < 0 && !fechVencDocto.equals(sdf.format(fechaHoy)) ) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 23: La Fecha de Vencimiento del Documento "+fechVencDocto+" es menor a la Fecha de Hoy.");
						VecError.addElement("Verifique el dato capturado");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para la Fecha de Vencimiento del Descuento. Campo 24
				//System.out.println("Fecha Vencimiento Descto: "+fechVencDscto);
				if(fechVencDscto.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fechVencDscto)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					} else {
						java.util.Date fechaVenc=Comunes.parseDate(fechVencDscto);
						int iComp = fechaVenc.compareTo(fechaHoy);
						if (iComp < 0 && !fechVencDscto.equals(sdf.format(fechaHoy)) ) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento "+fechVencDscto+" es menor a la Fecha de Hoy.");
							VecError.addElement("Verifique el dato capturado");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				}
				//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito. Campo 25
				//System.out.println("Fecha Emisiont Titulo Credito: "+fetc);
				if(fetc.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else if(lbOK){
					if(!Comunes.checaFecha(fetc)) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito no es una fecha correcta.");
						VecError.addElement("Formato v�lido: dd/mm/yyyy");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n para el Lugar de Firma. Campo 26
				//System.out.println("Fecha Vencimiento Docto: "+fechVencDocto);
				if(lugarFirma.equals("")&&lbOK) {
					VecError = new Vector();
					VecError.addElement(""+liLineadocs);
					VecError.addElement("Campo 26: El Lugar de Firma es un campo requerido.");
					VecError.addElement("Verifique que est� capturado el dato.");
					VecErrores.addElement(VecError);
					lbOK = false;
				} else
					lugarFirma = (lugarFirma.length()>50)?"'"+lugarFirma.substring(0,49)+"'":"'"+lugarFirma+"'";
				//Validaci�n para la Amortizaci�n de Ajuste. Campo 27
				//System.out.println("Amortizacion de Ajuste: "+amortAjuste);
				amortImporte=""; amortFecha="";
               	lovTablaAmort = null;
               	if(!amortAjuste.equals("")&&lbOK){
               		if(!(amortAjuste.equals("P")||amortAjuste.equals("U"))){
	               		VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 27: El Tipo de Amortizaci�n de Ajuste no es v�lido.");
						VecError.addElement("Tipos de Amortizaci�n de Ajuste: 'P' Primero, 'U' �ltimo.");
						VecErrores.addElement(VecError);
						lbOK = false;
	               	} else {
						try {
							lovTablaAmort = ovValidaAmortizacion(amortAjuste,lovDatos);
	                        if (lovTablaAmort.size() > 0){
	                        	amortAjuste  = lovTablaAmort.elementAt(0).toString();
	                            amortImporte = lovTablaAmort.elementAt(1).toString();
	                            amortFecha 	 = lovTablaAmort.elementAt(2).toString();
	                        }
	        	        } catch (Exception e){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 27: Las Amortizaciones no est�n completas.");
							VecError.addElement("Inserte N�mero, Importe, Fecha y Tipo por cada Amortizaci�n de Ajuste");
							VecErrores.addElement(VecError);
							lbOK = false;
		                }
						if((amortImporte.equals("") || amortFecha.equals(""))&&lbOK) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 27: Los Campos Din�micos de las Amortizaciones no est�n completos.");
							VecError.addElement("Verifique el Importe y Fecha de cada Amortizaci�n de Ajuste");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
				} else {
					amortImporte = "null";
					amortAjuste = "null";
				}
				//Validaci�n para el Tipo de Renta. Campo 28
				//System.out.println("Tipo Renta: "+tipoRenta);
				if(tablaAmort.equals("5")&&lbOK){
					if((!tipoRenta.equals("''"))){
	               		if(!(tipoRenta.equals("'N'")||tipoRenta.equals("'S'"))){
		               		VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 28: El Tipo de Renta no es v�lido.");
							VecError.addElement("Tipos de Renta: 'S' S�, 'N' No.");
							VecErrores.addElement(VecError);
							lbOK = false;
		               	}
					} else {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 28: El Tipo de Renta es un campo requerido.");
						VecError.addElement("Es obligatorio con Tipo de Amortizaci�n: Plan de Pagos. Tipos de Renta: 'S' S�, 'N' No.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaciones en Bases de Operaci�n
				if(lbOK){
					try{
						lovTmpBO = sGetDatosBO(NoIf,iTipoPiso,TipoPlazo,lodbConexion);
					} catch(Exception e){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("No existen registros parametrizados en Bases de Operaci�n para el Tipo de Plazo elegido.");
						VecError.addElement("Consulte las Bases de Operaci�n de Cr�dito Electr�nico.");
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Emisor con las Bases de Operaci�n. Campo 7
				if(lbOK){
					Vector tmpicEmisor = new Vector();
					String tmpEmisores = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicEmisor = (Vector) lovTmpBO.elementAt(i);

						if (icEmisor.equals(tmpicEmisor.elementAt(0)))
							lbOK = true;
						else {
							if(tmpEmisores.equals(""))
								tmpEmisores = "Claves de Emisor v�lidas: ";
							else
								tmpEmisores += ", ";

							tmpEmisores += tmpicEmisor.elementAt(0) +
											"-" + tmpicEmisor.elementAt(1);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 7: La Clave del Emisor no existe para el Tipo de Plazo elegido");
						VecError.addElement(tmpEmisores);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Tipo de Cr�dito con las Bases de Operaci�n. Campo 8
				if(lbOK){
					Vector tmpicTipoCred = new Vector();
					String tmpCreditos = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTipoCred = (Vector) lovTmpBO.elementAt(i);

						if (icTipoCredito.equals(tmpicTipoCred.elementAt(2)))
							lbOK = true;
						else {
							if(tmpCreditos.equals(""))
								tmpCreditos = "Claves de Tipo de Cr�dito v�lidas: ";
							else
								tmpCreditos += ", ";

							tmpCreditos += tmpicTipoCred.elementAt(2) +
											"-" + tmpicTipoCred.elementAt(3);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 8: La Clave del Tipo de Cr�dito no existe para el Tipo de Plazo elegido y la Clave de Emisor capturada");
						VecError.addElement(tmpCreditos);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}

				//Valida Tasa de Inter�s del Intermediario con las Bases de Operaci�n. Campo 17
				if(lbOK){
					Vector tmpicTasaI = new Vector();
					String tmpTasas = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTasaI = (Vector) lovTmpBO.elementAt(i);

						if (icTasaIntermed.equals(tmpicTasaI.elementAt(4)))
							lbOK = true;
						else {
							if(tmpTasas.equals(""))
								tmpTasas = "Claves de Tasa de Inter�s del Intermediario v�lidas: ";
							else
								tmpTasas += ", ";

							tmpTasas += tmpicTasaI.elementAt(4) +
											"-" + tmpicTasaI.elementAt(5);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe para el Tipo de Plazo elegido y las Claves de Emisor y de Tipo de Cr�dito capturadas");
						VecError.addElement(tmpTasas);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Valida Tipo de Amortizaci�n con las Bases de Operaci�n. Campo 19
				if(lbOK){
					Vector tmpicTablaAmort = new Vector();
					String tmpAmort = "";
					lbOK = false;

					for(int i=0;i<lovTmpBO.size();i++){
						tmpicTablaAmort = (Vector) lovTmpBO.elementAt(i);

						if (icTablaAmort.equals(tmpicTablaAmort.elementAt(6)))
							lbOK = true;
						else {
							if(tmpAmort.equals(""))
								tmpAmort = "Claves de Tipo de Amortizaci�n v�lidas: ";
							else
								tmpAmort += ", ";

							tmpAmort += tmpicTablaAmort.elementAt(6) +
											"-" + tmpicTablaAmort.elementAt(7);
						}
					}

					if(!lbOK){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo 19: La Clave del Tipo de Amortizaci�n no existe para el Tipo de Plazo elegido y las Claves de Emisor, Tipo de Cr�dito y Tasa de Inter�s del Intermediario capturadas");
						VecError.addElement(tmpAmort);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}
				//Validaci�n de la Fecha de Primer Pago de Capital, Fecha de Primer Pago de Inter�s
				//y Fecha de Vencimiento del Descuento con las Bases de Operaci�n. Campos 20,21,24
				if(lbOK){
		            lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append("select trunc(TO_DATE('"+fppc+"','dd/mm/yyyy')) - trunc(SYSDATE),");
					lsCadenaSQL.append("trunc(TO_DATE('"+fppi+"','dd/mm/yyyy')) - trunc(SYSDATE),");
					lsCadenaSQL.append("trunc(TO_DATE('"+fechVencDscto+"','dd/mm/yyyy')) - trunc(SYSDATE) from dual");
					lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
					if (lrsSel.next()){
						PlazoFPPC = lrsSel.getInt(1);
						PlazoFPPI = lrsSel.getInt(2);
						PlazoFVD  = lrsSel.getInt(3);
					}
		            lodbConexion.cierraStatement();
		         }
				// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
				if(lbOK) {	// Si paso bien todas las condiciones de tipos y valores se hace la validaci�n.
					Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, lodbConexion);
					iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
					if(TipoPlazo.equals("C")) {	// Cr�dito.
						if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en la b.o. se manda el mensaje.
							if(iTipoPiso==1) {
								VecError = new Vector();
								VecError.addElement(""+liLineadocs);
								VecError.addElement("Campo 24: No existe Plazo M�ximo.");
								VecError.addElement("Verifique los par�metros de Bases de Operaci�n.");
								VecErrores.addElement(VecError);
								lbOK = false;
							}
							if(iTipoPiso==2) {
								VecError = new Vector();
								VecError.addElement(""+liLineadocs);
								VecError.addElement("Campo 24: No existe Plazo M�ximo.");
								VecError.addElement("Favor de comunicarse al 01 800 CADENAS.");
								VecErrores.addElement(VecError);
								lbOK = false;
							}
						} else if(PlazoFPPC > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						} else if(PlazoFPPI > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						} else if(PlazoFVD > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					} else if(TipoPlazo.equals("F")) {	//	Factoraje.
						if(iPlazoMaxBO==0 && iTipoPiso==1) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: No existe Plazo M�ximo.");
							VecError.addElement("Verifique los par�metros de Bases de Operaci�n.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFPPC > iPlazoMaxBO) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFPPI > iPlazoMaxBO) {
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if(PlazoFVD > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
							VecError.addElement("Plazo M�ximo: "+iPlazoMaxBO);
							VecErrores.addElement(VecError);
							lbOK = false;
								/*
								//-----------------------------------------------------------------------------------
								Foda 001 2005
									-- Antes: si el plazo era mayor al plazo m�ximo, recortaba la fecha
									-- Nvo Req: si el plazo es mayor al plazo m�ximo, enviar mensaje de error
								//-----------------------------------------------------------------------------------
								GregorianCalendar cFechaVencDesc = new GregorianCalendar();
								cFechaVencDesc.add(Calendar.DATE, iPlazoMaxBO);	//Se Suma el plazo a la fecha de hoy.
								// Validamos si la fecha del plazo es d�a inhabil.
								Vector vDiasInhabiles = getDiasInhabiles(lodbConexion); // Obtenemos los d�as inhabiles.
								Hashtable hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO);
								cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
								Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
								boolean bCambioPlazo = false;
								bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								while(bCambioPlazo) {
									hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, Plazo);
									cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
									Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
									bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								}
								fechVencDscto = sdf.format(cFechaVencDesc.getTime());
								// Se reasigna la fecha PPC y la fecha PPI si alguna o ambas son mayores a la de Descuento.
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(fppc))) {
									fppc = sdf.format(cFechaVencDesc.getTime());
									diaPago = sdf.format(cFechaVencDesc.getTime()).substring(0,2);
								}
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(fppi)))
									fppi = sdf.format(cFechaVencDesc.getTime());

								sTomoPlazoGral = hPlazoBO.get("sTomoPlazoGral").toString();
								*/
							} // Plazo > iPlazoMaxBO
					} // TipoPlazo.equals("F")
				} // lbOK

				//Validaci�n de Fecha M�xima de Amortizaci�n Recortada. Campo 28 = R
				String fechaMax = "";
				if(lbOK){
					fechaMax = getFechaMaxRecortado(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, lodbConexion);
					System.out.println("=====LA FECHA MAX = -"+ fechaMax +"-");
				}
				//System.out.println(fechVencDscto);
				System.out.println("tablaAmort: "+tablaAmort+ "lbOK: "+lbOK);
				if("5".equals(tablaAmort)&&lbOK){
					Vector vecCampos = new Vector();
					String fecMaxVenc = "";
					try {
						lsCadenaSQL.delete(0, lsCadenaSQL.length());
						lsCadenaSQL.append("select to_char(sysdate+"+iPlazoMaxBO+",'dd/mm/yyyy') from dual");
						lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
						if (lrsSel.next())
							fecMaxVenc = lrsSel.getString(1);
			            lodbConexion.cierraStatement();
						vecCampos = ovObtenerCamposDeAmort(lovDatos,fechaMax,fecMaxVenc,iPlazoMaxBO);
					} catch (NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Campo "+campoError+": "+error);
						VecError.addElement(solucion);
						VecErrores.addElement(VecError);
						lbOK = false;
					}
					if("'N'".equals(tipoRenta)&&lbOK){
						fppc			= (String)vecCampos.get(0);
						fppi			= (String)vecCampos.get(1);
						fechVencDscto	= (String)vecCampos.get(2);
						diaPago			= (String)vecCampos.get(3);

						fppc			= Comunes.strtr(fppc,"\"","");
						fppi			= Comunes.strtr(fppi,"\"","");
						fechVencDscto	= Comunes.strtr(fechVencDscto,"\"","");
						diaPago			= Comunes.strtr(diaPago,"\"","");

					}else if(lbOK){
						String diappi	= fppi.substring(0,2);
						String diavenc	= fechVencDscto.substring(0,2);
						if(!diappi.equals(diavenc)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 24: Los d�as de pago deben ser iguales para todas las amortizaciones");
							VecError.addElement("D�a de Pago Fecha de Primer Pago de Interes = D�a de Pago Fecha de Vencimiento del Descuento.");
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if("14".equals(ppc)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 9: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
							VecError.addElement(sGetPeriodicidad(lodbConexion));
							VecErrores.addElement(VecError);
							lbOK = false;
						}
						if("14".equals(ppi)){
							VecError = new Vector();
							VecError.addElement(""+liLineadocs);
							VecError.addElement("Campo 10: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
							VecError.addElement(sGetPeriodicidad(lodbConexion));
							VecErrores.addElement(VecError);
							lbOK = false;
						}
					}
					if(lbOK){
						numAmortRec		= (String)vecCampos.get(4);
						plazoMensual	= (String)vecCampos.get(5);

						numAmortRec		= Comunes.strtr(numAmortRec,"\"","");
						plazoMensual	= Comunes.strtr(plazoMensual,"\"","");
					}
				}else{
					tipoRenta = "null";
				}

				String icSolicPortal = "";
				if(lbOK){
					lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append(
						" select NVL(MAX(ic_solic_portal),0)+1"+
						" from comtmp_solic_portal");
					lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
					if(lrsSel.next()){
						icSolicPortal = lrsSel.getString(1);
					}
					lodbConexion.cierraStatement();

					lsCadenaSQL.delete(0, lsCadenaSQL.length());
					lsCadenaSQL.append(
							"insert into comtmp_solic_portal (ic_solic_portal,ic_proc_solic, ic_if, ig_clave_sirac, "+
							"ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, "+
							"fn_importe_dscto, ic_emisor, ic_tipo_credito, ic_periodicidad_c, "+
							"ic_periodicidad_i, cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, "+
							"ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, "+
							"in_numero_amort, ic_tabla_amort, df_ppc, df_ppi, in_dia_pago, "+
							"df_v_documento, df_v_descuento, ig_plazo, df_etc, cg_lugar_firma, "+
							"cs_amort_ajuste, fg_importe_ajuste, cs_tipo_plazo, cs_plazo_maximo_factoraje,cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual) "+
							" values("+icSolicPortal+","+licProcSolic+","+NoIf+","+claveSirac+", "+noSucBanco+", "+moneda+", "+
							numDocto+", "+importeDocto+", "+importeDscto+", "+icEmisor+", "+
							icTipoCredito+", "+icTipoPPC+", "+icTipoPPI+", "+bieneServi+", "+
							icClaseDocto+", "+domicilioPago+", "+icTasaUF+", "+relMatUF+", "+
							sobreTasaUF+", "+icTasaIntermed+", "+relMatI+", "+sobreTasaI+", "+
							numAmort+", "+icTablaAmort+", "+
							"TO_DATE('"+fppc+"','DD/MM/YYYY'), TO_DATE('"+fppi+"','DD/MM/YYYY'), "+
							diaPago+", "+(fechVencDocto.equals("")?"null":"TO_DATE('"+fechVencDocto+"','DD/MM/YYYY')")+", "+
							"TO_DATE('"+fechVencDscto+"','DD/MM/YYYY'), "+PlazoFVD+", "+
							"TO_DATE('"+fetc+"','DD/MM/YYYY'), "+lugarFirma+", "+amortAjuste+", "+
							amortImporte+",'"+TipoPlazo+"','"+sTomoPlazoGral+"',"+tipoRenta+","+numAmortRec+",'"+plazoMensual+"' )"
					);

					try {
						System.out.println(lsCadenaSQL.toString());
						lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
	                } catch (Exception error) {
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						VecError.addElement("Error de Concurrencia, hay registros Repetidos.");
						VecError.addElement("Verifique los datos capturados");
						VecErrores.addElement(VecError);
						lbOK = false;
						error.printStackTrace();
					}
				}

				System.out.println(tipoRenta);
				System.out.println(lbOK);
				// Valida e inserta las amortizaciones
				if("'N'".equals(tipoRenta)&&lbOK){
					//System.out.println("Validaci�n de amortizaciones");
					try{
						ovGenerarAmortizaciones(icSolicPortal,lovDatos,fechaMax,lodbConexion);
					} catch(NafinException ne){
						VecError = new Vector();
						VecError.addElement(""+liLineadocs);
						if(TA){
							VecError.addElement("Campo "+campoError+": El Tipo de Amortizaci�n no es v�lido");
							VecError.addElement("Tipos de Amortizaci�n v�lidos: 'A' Capital e Intereses, 'I' Intereses, 'R' Recortada");
						} else {
							VecError.addElement("Campo "+campoError+": "+ne.getMsgError());
							if(ne.getCodError().equals("CRED0012")||ne.getCodError().equals("CRED0013")){
								VecError.addElement("Verifique los montos de las amortizaciones tipo 'A'");
							}else{
								VecError.addElement("Verifique los datos capturados");
							}
						}
						VecErrores.addElement(VecError);
						lbOK = false;
					}
				}

				if(lbOK){
					VecProc = new Vector();
					VecProc.addElement(""+liLineadocs);
					VecProc.addElement(NoIf);
					VecProc.addElement(numDocto);
					VecProcs.addElement(VecProc);
					bdMtoDoctos = bdMtoDoctos.add(new BigDecimal(importeDocto));
					bdMtoDsctos = bdMtoDsctos.add(new BigDecimal(importeDscto));
				}

				lodbConexion.terminaTransaccion(lbOK);
			}

			lovResultado.addElement(VecErrores);    //Registros con error
			lovResultado.addElement(VecProcs);		//Registros procesados
			lovResultado.addElement(bdMtoDoctos);	//Monto Documentos
			lovResultado.addElement(bdMtoDsctos);	//Monto Descuentos
			lovResultado.addElement(new Integer(licProcSolic));		//Clave

			System.out.println("Termino");
			return lovResultado;
		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			epError.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(bBotonProcesar);
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
			System.out.println(" AutorizacionSolicitudEJB::ovProcesarArchivoXLS(S)");
		}
	}

	private String error = "";
	private String solucion = "";
	public Vector ovObtenerCamposDeAmort(Vector eovDatos,String fechaMax,String fecMaxVenc,int PlazoMax)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean.ovObtenerCamposDeAmort (E)");
		Vector vecRetorno = new Vector();
		int i = 0;
		String fppc		= "";
		String fppi		= "";
		String fvencdes	= "";
		String diappc	= "";
		int		numAmortRec = 0;
		java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
		java.util.Date		dFecAmortAnt	= Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
		java.util.Date 		fMaxVenc		= Comunes.parseDate(fecMaxVenc);
		java.util.Date 		fechaHoy		= new java.util.Date();
		String	plazoMensual 	= "S";
		try{
			for(i=28;i<eovDatos.size();i+=4){
				String montoAmort = eovDatos.get(i+1).toString().trim();
				String fechaAmort = eovDatos.get(i+2).toString().trim();
				String tipoAmort  = eovDatos.get(i+3).toString().trim();

				if(montoAmort.equals("")){
					campoError = i + 2;
					error = "El Importe de Amortizaci�n no est� capturado.";
					solucion = "Inserte el dato";
					throw new NafinException("SIST0001");
				} else if(!Comunes.esDecimal(montoAmort)) {
						campoError = i + 2;
						error = "El Importe de Amortizaci�n no es un valor v�lido.";
						solucion = "Dato num�rico decimal";
						throw new NafinException("SIST0001");
				}
				if(fechaAmort.equals("")){
					campoError = i + 3;
					error = "La Fecha de Amortizaci�n no est� capturada.";
					solucion = "Inserte el dato";
					throw new NafinException("SIST0001");
				} else {
					if(!Comunes.checaFecha(fechaAmort)) {
						campoError = i + 3;
						error = "La Fecha de Amortizaci�n no es una fecha correcta";
						solucion = "Formato v�lido: dd/mm/yyyy";
						throw new NafinException("SIST0001");
					} else {
						java.util.Date fecAmorttmp = Comunes.parseDate(fechaAmort);
						int iComp = fecAmorttmp.compareTo(fechaHoy);
						if(iComp < 0){
							campoError = i + 3;
							error = "La Fecha de Amortizaci�n es menor a la Fecha de Hoy";
							solucion = "Verifique el dato capturado";
							throw new NafinException("SIST0001");
						}
						iComp = fecAmorttmp.compareTo(fMaxVenc);
						if(iComp > 0){
							campoError = i + 3;
							error = "La Fecha de Amortizaci�n es mayor al plazo permitido";
							solucion = "Plazo M�ximo: " + PlazoMax;
							throw new NafinException("SIST0001");
						}
					}
				}
				if(!(tipoAmort.equals("A")||tipoAmort.equals("I")||tipoAmort.equals("R"))){
					campoError = i + 4;
					error = "El Tipo de Amortizaci�n no es v�lido";
					solucion = "Tipos de Amortizaci�n v�lidos: A-Capital e Intereses, I-Intereses, R-Recortada.";
					throw new NafinException("Tipo de Amortizaci�n");
				}

				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);

				if(dFecAmortAnt!=null){
					long lfecAmort = dFecAmort.getTime();
					long lfecAmortAnt = dFecAmortAnt.getTime();
					double diferencia = (lfecAmort - lfecAmortAnt)/86400000;

					System.out.println("\n\n\n\nLINEA 3989, LA DIFERENCIA = "+diferencia);

					if((int)diferencia>31){
						plazoMensual = "N";
					}
				}
				//System.out.println("=====FECHA AMORT = -"+dFecAmort.toString()+"-");
				//System.out.println("=====LA FECHA MAX = -"+dFecMax.toString()+"-");
				//System.out.println("=====LA COMPARACION = "+dFecMax.compareTo(dFecAmort));

				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				if("A".equals(tipoAmort)){
					if("".equals(fppc))
						fppc = fechaAmort;
					fvencdes = fechaAmort;
				}
				if(("I".equals(tipoAmort)||"A".equals(tipoAmort))&&"".equals(fppi)){
					fppi = fechaAmort;
				}
				if("R".equals(tipoAmort)&&Double.parseDouble(montoAmort)>0){
					numAmortRec++;
				}
				dFecAmortAnt = Comunes.parseDate(fechaAmort);
			}
			if(!"".equals(fppc))
				diappc	= fppc.substring(0,2);

/*0*/		vecRetorno.add(fppc);
/*1*/		vecRetorno.add(fppi);
/*2*/		vecRetorno.add(fvencdes);
/*3*/		vecRetorno.add(diappc);
/*4*/		vecRetorno.add(numAmortRec+"");
/*5*/		vecRetorno.add(plazoMensual);

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSolicitudBean.ovObtenerCamposDeAmort (S)");
		}
		return vecRetorno;
	}

	private Vector ovObtenerCamposDeAmort(List eovDatos,String fechaMax,String fecMaxVenc,int PlazoMax)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean.ovObtenerCamposDeAmort (E)");
		Vector vecRetorno = new Vector();
		int i = 0;
		String fppc		= "";
		String fppi		= "";
		String fvencdes	= "";
		String diappc	= "";
		int		numAmortRec = 0;
		java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
		java.util.Date		dFecAmortAnt	= Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
		java.util.Date 		fMaxVenc		= Comunes.parseDate(fecMaxVenc);
		java.util.Date 		fechaHoy		= new java.util.Date();
		String	plazoMensual 	= "S";
		try{
			for(i=28;i<eovDatos.size();i+=4){
				String montoAmort = eovDatos.get(i+1).toString().trim();
				String fechaAmort = eovDatos.get(i+2).toString().trim();
				String tipoAmort  = eovDatos.get(i+3).toString().trim();

				//System.out.println(montoAmort);
				//System.out.println(fechaAmort);
				//System.out.println(tipoAmort);

				if(montoAmort.equals("")){
					campoError = i + 2;
					error = "El Importe de Amortizaci�n no est� capturado.";
					solucion = "Inserte el dato";
					throw new NafinException("SIST0001");
				} else if(!Comunes.esDecimal(montoAmort)) {
						campoError = i + 2;
						error = "El Importe de Amortizaci�n no es un valor v�lido.";
						solucion = "Dato num�rico decimal";
						throw new NafinException("SIST0001");
				}
				if(fechaAmort.equals("")){
					campoError = i + 3;
					error = "La Fecha de Amortizaci�n no est� capturada.";
					solucion = "Inserte el dato";
					throw new NafinException("SIST0001");
				} else {
					if(!Comunes.checaFecha(fechaAmort)) {
						campoError = i + 3;
						error = "La Fecha de Amortizaci�n no es una fecha correcta";
						solucion = "Formato v�lido: dd/mm/yyyy";
						throw new NafinException("SIST0001");
					} else {
						java.util.Date fecAmorttmp = Comunes.parseDate(fechaAmort);
						int iComp = fecAmorttmp.compareTo(fechaHoy);
						if(iComp < 0){
							campoError = i + 3;
							error = "La Fecha de Amortizaci�n es menor a la Fecha de Hoy";
							solucion = "Verifique el dato capturado";
							throw new NafinException("SIST0001");
						}
						iComp = fecAmorttmp.compareTo(fMaxVenc);
						if(iComp > 0){
							campoError = i + 3;
							error = "La Fecha de Amortizaci�n es mayor al plazo permitido";
							solucion = "Plazo M�ximo: " + PlazoMax;
							throw new NafinException("SIST0001");
						}
					}
				}
				if(!(tipoAmort.equals("A")||tipoAmort.equals("I")||tipoAmort.equals("R"))){
					campoError = i + 4;
					error = "El Tipo de Amortizaci�n no es v�lido";
					solucion = "Tipos de Amortizaci�n v�lidos: A-Capital e Intereses, I-Intereses, R-Recortada.";
					throw new NafinException("Tipo de Amortizaci�n");
				}

				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);

				if(dFecAmortAnt!=null){
					long lfecAmort = dFecAmort.getTime();
					long lfecAmortAnt = dFecAmortAnt.getTime();
					double diferencia = (lfecAmort - lfecAmortAnt)/86400000;

					System.out.println("\n\n\n\nLINEA 4113, LA DIFERENCIA = "+diferencia);

					if((int)diferencia>31){
						plazoMensual = "N";
					}
				}
				//System.out.println("=====FECHA AMORT = -"+dFecAmort.toString()+"-");
				//System.out.println("=====LA FECHA MAX = -"+dFecMax.toString()+"-");
				//System.out.println("=====LA COMPARACION = "+dFecMax.compareTo(dFecAmort));

				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				if("A".equals(tipoAmort)){
					if("".equals(fppc))
						fppc = fechaAmort;
					fvencdes = fechaAmort;
				}
				if(("I".equals(tipoAmort)||"A".equals(tipoAmort))&&"".equals(fppi)){
					fppi = fechaAmort;
				}
				if("R".equals(tipoAmort)&&Double.parseDouble(montoAmort)>0){
					numAmortRec++;
				}
				dFecAmortAnt = Comunes.parseDate(fechaAmort);
			}
			if(!"".equals(fppc))
				diappc	= fppc.substring(0,2);

/*0*/		vecRetorno.add(fppc);
/*1*/		vecRetorno.add(fppi);
/*2*/		vecRetorno.add(fvencdes);
/*3*/		vecRetorno.add(diappc);
/*4*/		vecRetorno.add(numAmortRec+"");
/*5*/		vecRetorno.add(plazoMensual);

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSolicitudBean.ovObtenerCamposDeAmort (S)");
		}
		return vecRetorno;
	}

	private void ovInsertaAmortizacionesTmp(String icSolicPortal,String icProcSolicPortal,String fvencAmort[],String tipoAmort[],String montoAmort[],AccesoDB con)
		throws NafinException{
		System.out.println("AutorizacionSOlicitudBean.ovInsertaAmortizacionesTmp (E)");
		try{
			PreparedStatement	ps				= null;
			String				qrySentencia	= "";
			int 				i = 0;

			qrySentencia =
				" INSERT INTO comtmp_amort_portal"   +
				" (ic_amort_portal,ic_solic_portal,cg_tipo_amort,fn_monto,df_vencimiento, ic_proc_solic)"   +
				" SELECT NVL(MAX(ic_amort_portal),0)+1,?,?,?,?,? FROM comtmp_amort_portal"   +
				" WHERE ic_solic_portal = ?"	+
				" AND ic_proc_solic = ?";
			if(tipoAmort!=null){
				ps = con.queryPrecompilado(qrySentencia);
				for(i=0; i<tipoAmort.length; i++){
					//ejecutamos el insert en las tablas temporales para cada amortizacion
					if(("R".equals(tipoAmort[i])&&Double.parseDouble(montoAmort[i])>0)||!"R".equals(tipoAmort[i])){
						ps.clearParameters();
						ps.setString(1,icSolicPortal);
						ps.setString(2,tipoAmort[i]);
						ps.setString(3,montoAmort[i]);
						ps.setString(4,fvencAmort[i]);
						ps.setString(5,icProcSolicPortal);
						ps.setString(6,icSolicPortal);
						ps.setString(7,icProcSolicPortal);
						ps.execute();
					}
				} //for
				ps.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovInsertaAmortizacionesTmp(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSOlicitudBean.ovInsertaAmortizacionesTMp (S)");
		}
	}

	/* Metodo para generar amortizaciones en el caso de que no se trate de una tabla de Plan de Pagos MPCS 08/10/2004 */
	public void ovGenerarTablaAmortizacionesTmp(String icSolicPortal,String sFechaPrimerPago, String sPeriodicidadCap, String sMontoCap, String sNumAmort, AccesoDB con)
	throws NafinException{
		System.out.println("AutorizacionSOlicitudBean.ovGenerarTablaAmortizaciones (E)");
		StringBuffer sbQuery = new StringBuffer("SELECT ");
		String sMontoPrimero = null;
		String sMontoUltimo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Vector vecDatos = new Vector();
		int iNumAmort = 0, iPeriodicidad = 0;
		try{
			if (icSolicPortal==null||sFechaPrimerPago==null||sPeriodicidadCap==null||sMontoCap==null||sNumAmort==null||"".equals(icSolicPortal)||"".equals(sFechaPrimerPago)||"".equals(sPeriodicidadCap)||"".equals(sMontoCap)||"".equals(sNumAmort))
				throw new NafinException("EQUP0046");
			// Validacion de los tipos de datos
			try{
				iNumAmort = Integer.parseInt(sNumAmort);
				iPeriodicidad = Integer.parseInt(sPeriodicidadCap);
			} catch (Exception e)
			{	System.out.println("Excepcion en AutorizacionSolicitudBean.ovGenerarTablaAmortizaciones::"+e.toString());
			    throw new NafinException("GRAL0022");
			}
			//Calcular los datos
			for (int i=1;i<=iNumAmort;i++)
			{   if (i>1) sbQuery.append(" , ");
				sbQuery.append("DECODE(cg_tipo_incremento,'M',TO_CHAR(ADD_MONTHS(TO_DATE('"+sFechaPrimerPago+"','dd/mm/yyyy'),(("+i+"-1)*ig_incremento)),'DD/MM/YYYY'), "+
	   							" TO_CHAR(TO_DATE('"+sFechaPrimerPago+"','dd/mm/yyyy')+(("+i+"-1)*ig_incremento)),'DD/MM/YYYY') FECHAVENC"+i);
			} // Fin del for
			sbQuery.append(" , trim(to_char(ROUND("+sMontoCap+"/"+sNumAmort+",2),'999999999.99')) MONTOP, "   +
							"trim(to_char("+sMontoCap+" - (ROUND("+sMontoCap+"/"+sNumAmort+",2)*("+sNumAmort+"-1)),'999999999.99')) MONTOU"   +
							" FROM   COMCAT_PERIODICIDAD"   +
							" WHERE IC_PERIODICIDAD= ? ");
			ps = con.queryPrecompilado(sbQuery.toString());
			ps.setInt(1,iPeriodicidad);
			rs = ps.executeQuery();
			if (rs.next())
			{ for (int i=1;i<=iNumAmort;i++)
			  {
				vecDatos.add(i-1,(rs.getString("FECHAVENC"+String.valueOf(i))==null)?"":rs.getString("FECHAVENC"+String.valueOf(i)));
			  } // Fin del for
			  sMontoPrimero=(rs.getString("MONTOP")==null)?"0":rs.getString("MONTOP");
			  sMontoUltimo=(rs.getString("MONTOU")==null)?"0":rs.getString("MONTOU");
			} else
			{	System.out.println("Excepcion en AutorizacionSolicitudBean.ovGenerarTablaAmortizaciones::No se encontro la periodicidad del pago");
				throw new NafinException("EQUP0050");
			}
			//Inicializacion de variables
			ps.clearParameters(); ps=null; if (rs!=null) rs.close();
			sbQuery.delete(0,sbQuery.length());
			//Insercion en la tabla de amortizacion
			sbQuery.append(" INSERT INTO comtmp_amort_portal"   +
							" (ic_amort_portal,ic_solic_portal,cg_tipo_amort,fn_monto,df_vencimiento)"   +
							" SELECT NVL(MAX(ic_amort_portal),0)+1,?,?,to_number(?,'999999999.99'),to_date(?,'dd/mm/yyyy') FROM comtmp_amort_portal"   +
							" WHERE ic_solic_portal = ?");
			ps = con.queryPrecompilado(sbQuery.toString());
			for (int i=0;i<iNumAmort;i++)
			{
			   ps.setString(1,icSolicPortal);
			   ps.setString(2,"C");
			   if (i==(iNumAmort-1))  ps.setString(3,sMontoUltimo);
			   else					  ps.setString(3,sMontoPrimero);
			   ps.setString(4,(String)vecDatos.get(i));
			   ps.setString(5,icSolicPortal);
			   ps.execute();
			   ps.clearParameters();
			}
			if (ps!=null) ps.close();
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovGenerarTablaAmortizaciones(): "+ e.getMessage());
			throw new NafinException("EQUP0045");
		}finally{
			System.out.println("AutorizacionSOlicitudBean.ovGenerarTablaAmortizaciones (S)");
		}
	}

	private int campoError = 0;
	private boolean TA = false;

	public void ovGenerarAmortizaciones(String icSolicPortal,Vector eovDatos,String fechaMax,AccesoDB con, int ic_proc_solic)
		throws NafinException{
		System.out.println("AutorizacionSOlicitudBean.ovGenerarAmortizaciones (E)");
		try{
			Vector				vecAmortizaciones = new Vector();
			PreparedStatement	ps				= null;
			String				qrySentencia	= "";
			boolean				recortada		= false;
			boolean				interes			= false;
			boolean				capitalInt		= false;
			int 				i = 0, j = 0;
			int					numAmort		= 0;
			double				importeDocto	= 0;
			double				importeDscto	= 0;
			BigDecimal			totalImpAmort	= new BigDecimal(0.0);
			BigDecimal			totalImpAmNoRec	= new BigDecimal(0.0);
			java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
			java.util.Date		dFecAmortAnt	= new java.util.Date();

			numAmort		= Integer.parseInt(eovDatos.get(17).toString().trim());
			importeDocto	= Double.parseDouble(eovDatos.get(4).toString().trim());
			importeDscto	= Double.parseDouble(eovDatos.get(5).toString().trim());

			for(i=28;i<eovDatos.size();i++){
				vecAmortizaciones.add(eovDatos.get(i));
			}

			qrySentencia =
				" INSERT INTO comtmp_amort_portal"   +
				" (ic_amort_portal,ic_solic_portal,cg_tipo_amort,fn_monto,df_vencimiento,ic_proc_solic)"   +
				" SELECT NVL(MAX(ic_amort_portal),0)+1,?,?,?,to_date(?,'dd/mm/yyyy'),? FROM comtmp_amort_portal"   +
				" WHERE ic_solic_portal = ?"  ;

			ps = con.queryPrecompilado(qrySentencia);
			for(i=0,j=0; i<vecAmortizaciones.size(); i+=4,j++){
				String strImpAmort = vecAmortizaciones.get(i+1).toString().trim();
				String fechaAmort = vecAmortizaciones.get(i+2).toString().trim();
				double importeAmort = 0;

				strImpAmort = Comunes.strtr(strImpAmort,"\"","");
				strImpAmort = Comunes.strtr(strImpAmort,",","");

				importeAmort = Double.parseDouble(strImpAmort);



				String tipoAmort = vecAmortizaciones.get(i+3).toString().trim();
				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);

				/* revisa que la fecha no sea menor a la anterior*/
				if(i>0){
					if(dFecAmortAnt.compareTo(dFecAmort)>=0){
						campoError = 28 + i + 3;
						throw new NafinException("CRED0006");
					}
				}
				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				/*Valida los tipos de amortizaciones*/
				if("A".equals(tipoAmort)){
					capitalInt = true;
					totalImpAmNoRec = totalImpAmNoRec.add(new BigDecimal(importeAmort));
					if(importeAmort==0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0012");
					}
					if(importeAmort<0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0013");
					}
				}else if("I".equals(tipoAmort)){
					importeAmort = 0;
					interes = true;
					/*if(capitalInt)
						throw new NafinException("CRED0002");*/
				}else if("R".equals(tipoAmort)){
					recortada = true;
					if(capitalInt||interes){
						campoError = 28 + i + 4;
						throw new NafinException("CRED0001");
					}

					System.out.println("LA FECHA MAXIMA PERMITIDA = "+dFecMax);
					System.out.println("LA FECHA DE LA AMORTIZACION = "+dFecAmort);
					System.out.println("LA COMPARACION = "+dFecMax.compareTo(dFecAmort));
					if(dFecMax.compareTo(dFecAmort)<0){
						System.out.println("INCREIBLEMENTE SI PASO POR AQUI");
						campoError = 28 + i + 3;
						throw new NafinException("CRED0005");
					}

				} else {
					campoError = 28 + i + 4;
					TA = true;
					return;
				}
				dFecAmortAnt 	= dFecAmort;
				totalImpAmort	= totalImpAmort.add(new BigDecimal(importeAmort));
				//ejecutamos el insert en las tablas temporales para cada amortizacion
				if(("R".equals(tipoAmort)&&importeAmort>0)||!"R".equals(tipoAmort)){

					tipoAmort = Comunes.strtr(tipoAmort,"\"","");
					fechaAmort = Comunes.strtr(fechaAmort,"\"","");
System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
System.out.println("qrySentencia:"+qrySentencia);
System.out.println("icSolicPortal:"+icSolicPortal);
System.out.println("tipoAmort:"+tipoAmort);
System.out.println("importeAmort:"+importeAmort);
System.out.println("fechaAmort:"+fechaAmort);
System.out.println("icSolicPortal:"+icSolicPortal);
					ps.setString(1,icSolicPortal);
					ps.setString(2,tipoAmort);
					ps.setDouble(3,importeAmort);
					ps.setString(4,fechaAmort);

					ps.setInt(5,ic_proc_solic);
					ps.setString(6,icSolicPortal);
					ps.execute();
				}
			} //for
			ps.close();
			if(j!=numAmort){
				campoError = 18;
				throw new NafinException("CRED0003");
			}

			double tmpTotImpAmort = (double)Math.round(totalImpAmort.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORTIZACIONES = "+tmpTotImpAmort);
			System.out.println("TOTAL IMPORTE DOCTOS = "+importeDocto);

			if(tmpTotImpAmort!=importeDocto){
				campoError = 5;
				throw new NafinException("CRED0007");
			}

			double tmpTotImpAmNoRec = (double)Math.round(totalImpAmNoRec.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORT NO REC = "+tmpTotImpAmNoRec);
			System.out.println("TOTAL IMPORTE DSCTO = "+importeDscto);

			if(tmpTotImpAmNoRec!=importeDscto){
				campoError = 6;
				throw new NafinException("CRED0008");
			}

			System.out.println(" El vector de amortizaciones = "+vecAmortizaciones);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovGenerarAmortizaciones(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSOlicitudBean.ovGenerarAmortizaciones (S)");
		}
	}
	private void ovGenerarAmortizaciones(String icSolicPortal,List eovDatos,String fechaMax,AccesoDB con)
		throws NafinException{
		System.out.println("AutorizacionSOlicitudBean.ovGenerarAmortizaciones (E)");
		try{
			Vector				vecAmortizaciones = new Vector();
			PreparedStatement	ps				= null;
			String				qrySentencia	= "";
			boolean				recortada		= false;
			boolean				interes			= false;
			boolean				capitalInt		= false;
			int 				i = 0, j = 0;
			int					numAmort		= 0;
			double				importeDocto	= 0;
			double				importeDscto	= 0;
			BigDecimal			totalImpAmort	= new BigDecimal(0.0);
			BigDecimal			totalImpAmNoRec	= new BigDecimal(0.0);
			java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
			java.util.Date		dFecAmortAnt	= new java.util.Date();

			numAmort		= Integer.parseInt(eovDatos.get(17).toString().trim());
			importeDocto	= Double.parseDouble(eovDatos.get(4).toString().trim());
			importeDscto	= Double.parseDouble(eovDatos.get(5).toString().trim());

			for(i=28;i<eovDatos.size();i++){
				vecAmortizaciones.add(eovDatos.get(i));
			}

			qrySentencia =
				" INSERT INTO comtmp_amort_portal"   +
				" (ic_amort_portal,ic_solic_portal,cg_tipo_amort,fn_monto,df_vencimiento)"   +
				" SELECT NVL(MAX(ic_amort_portal),0)+1,?,?,?,to_date(?,'dd/mm/yyyy') FROM comtmp_amort_portal"   +
				" WHERE ic_solic_portal = ?"  ;

			ps = con.queryPrecompilado(qrySentencia);
			for(i=0,j=0; i<vecAmortizaciones.size(); i+=4,j++){
				String strImpAmort = vecAmortizaciones.get(i+1).toString().trim();
				String fechaAmort = vecAmortizaciones.get(i+2).toString().trim();
				double importeAmort = 0;

				strImpAmort = Comunes.strtr(strImpAmort,"\"","");
				strImpAmort = Comunes.strtr(strImpAmort,",","");

				importeAmort = Double.parseDouble(strImpAmort);



				String tipoAmort = vecAmortizaciones.get(i+3).toString().trim();
				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);

				/* revisa que la fecha no sea menor a la anterior*/
				if(i>0){
					if(dFecAmortAnt.compareTo(dFecAmort)>=0){
						campoError = 28 + i + 3;
						throw new NafinException("CRED0006");
					}
				}
				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				/*Valida los tipos de amortizaciones*/
				if("A".equals(tipoAmort)){
					capitalInt = true;
					totalImpAmNoRec = totalImpAmNoRec.add(new BigDecimal(importeAmort));
					if(importeAmort==0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0012");
					}
					if(importeAmort<0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0013");
					}
				}else if("I".equals(tipoAmort)){
					importeAmort = 0;
					interes = true;
					/*if(capitalInt)
						throw new NafinException("CRED0002");*/
				}else if("R".equals(tipoAmort)){
					recortada = true;
					if(capitalInt||interes){
						campoError = 28 + i + 4;
						throw new NafinException("CRED0001");
					}

					System.out.println("LA FECHA MAXIMA PERMITIDA = "+dFecMax);
					System.out.println("LA FECHA DE LA AMORTIZACION = "+dFecAmort);
					System.out.println("LA COMPARACION = "+dFecMax.compareTo(dFecAmort));
					if(dFecMax.compareTo(dFecAmort)<0){
						System.out.println("INCREIBLEMENTE SI PASO POR AQUI");
						campoError = 28 + i + 3;
						throw new NafinException("CRED0005");
					}

				} else {
					campoError = 28 + i + 4;
					TA = true;
					return;
				}
				dFecAmortAnt 	= dFecAmort;
				totalImpAmort	= totalImpAmort.add(new BigDecimal(importeAmort));
				//ejecutamos el insert en las tablas temporales para cada amortizacion
				if(("R".equals(tipoAmort)&&importeAmort>0)||!"R".equals(tipoAmort)){

					tipoAmort = Comunes.strtr(tipoAmort,"\"","");
					fechaAmort = Comunes.strtr(fechaAmort,"\"","");

					ps.setString(1,icSolicPortal);
					ps.setString(2,tipoAmort);
					ps.setDouble(3,importeAmort);
					ps.setString(4,fechaAmort);
					ps.setString(5,icSolicPortal);
					ps.execute();
				}
			} //for
			ps.close();
			if(j!=numAmort){
				campoError = 18;
				throw new NafinException("CRED0003");
			}

			double tmpTotImpAmort = (double)Math.round(totalImpAmort.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORTIZACIONES = "+tmpTotImpAmort);
			System.out.println("TOTAL IMPORTE DOCTOS = "+importeDocto);

			if(tmpTotImpAmort!=importeDocto){
				campoError = 5;
				throw new NafinException("CRED0007");
			}

			double tmpTotImpAmNoRec = (double)Math.round(totalImpAmNoRec.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORT NO REC = "+tmpTotImpAmNoRec);
			System.out.println("TOTAL IMPORTE DSCTO = "+importeDscto);

			if(tmpTotImpAmNoRec!=importeDscto){
				campoError = 6;
				throw new NafinException("CRED0008");
			}

			System.out.println(" El vector de amortizaciones = "+vecAmortizaciones);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovGenerarAmortizaciones(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSOlicitudBean.ovGenerarAmortizaciones (S)");
		}
	}

	/* Metodo empleado en Registro individual Equipamiento Electronico */
	public String getFechaMaxRecortado(String TipoPlazo, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, String sProducto)
							throws NafinException{
		System.out.println("AutorizacionSolicbean::getFechaMaxRecortado (E)");
		AccesoDB	con = new AccesoDB();
		String retorno = "";
		int iTipoPiso = 0;
		try{
			con.conexionDB();
			iTipoPiso = getTipoPisoIF(NoIF, con);
			retorno = getFechaMaxRecortado(TipoPlazo, iTipoPiso, NoIF, TasaI, TipoCredito,Amortizacion, Emisor, Moneda, sProducto, con);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.getFechaMaxRecortado(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		return retorno;
	}

	/* Metodo para Equipamiento Electronico */
	private String getFechaMaxRecortado(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, String sProducto, AccesoDB con)
		throws NafinException {
	System.out.println("AutorizacionSolicitudBean::getFechaMaxRecortado (E)");
	PreparedStatement ps = null;
	ResultSet rs = null;
	String query = "";
	String fechaMax = "";
		try{
			query = " select to_char(sysdate + dias_minimos_primer_pago,'dd/mm/yyyy') "+
						"   from com_base_op_credito boc "+
						"	,no_bases_de_operacion nbo"+
						" where boc.ig_codigo_base = nbo.codigo_base_operacion"+
						"	and boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ")+
						(sProducto.equals("")?" and boc.ic_producto_nafin is null ":"	and boc.ic_producto_nafin = ? ");

				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);								System.out.println("***TIPO PISO = "+iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));			System.out.println("***IF = "+NoIF);
				ps.setInt(3, Integer.parseInt(TasaI.trim()));			System.out.println("***TASA IF = "+TasaI);
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));		System.out.println("***TIPO CREDITO = "+TipoCredito);
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));	System.out.println("***AMORTIZACION = "+Amortizacion);
				ps.setObject(6, TipoPlazo);								System.out.println("***TIPO PLAZO = "+TipoPlazo);
				if(!Emisor.equals(""))
				{  ps.setInt(7, Integer.parseInt(Emisor.trim()));
				   if(!sProducto.equals(""))   ps.setInt(8, Integer.parseInt(sProducto.trim()));
				} else if (!sProducto.equals(""))   ps.setInt(7, Integer.parseInt(sProducto.trim()));
				System.out.println("***EMISOR = "+Emisor);
				rs = ps.executeQuery();
				if(rs.next()){
					fechaMax = (rs.getString(1)==null)?"":rs.getString(1);
				}
				if(ps!=null) ps.close();

				if("".equals(fechaMax))
					throw new NafinException("CRED0004");
		}catch(NafinException ne){
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getFechaMinRecortado(). "+e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSolicitudBean::getFechaMaxRecortado (S)");
		}
	return fechaMax;
	}




	public String getFechaMaxRecortado(String TipoPlazo, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda)
							throws NafinException{
		System.out.println("AutorizacionSolicbean::getFechaMaxRecortado (E)");
		AccesoDB	con = new AccesoDB();
		String retorno = "";
		int iTipoPiso = 0;
		try{
			con.conexionDB();
			iTipoPiso = getTipoPisoIF(NoIF, con);
			retorno = getFechaMaxRecortado(TipoPlazo, iTipoPiso, NoIF, TasaI, TipoCredito,Amortizacion, Emisor, Moneda, con);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.getFechaMaxRecortado(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		return retorno;
	}

	private String getFechaMaxRecortado(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, AccesoDB con)
		throws NafinException {
	System.out.println("AutorizacionSolicitudBean::getFechaMaxRecortado (E)");
	PreparedStatement ps = null;
	ResultSet rs = null;
	String query = "";
	String fechaMax = "";
		try{
			query = " select to_char(sysdate + dias_minimos_primer_pago,'dd/mm/yyyy') "+
						"   from com_base_op_credito boc "+
						"	,no_bases_de_operacion nbo"+
						" where boc.ig_codigo_base = nbo.codigo_base_operacion"+
						"	and boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ");

				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);								System.out.println("***TIPO PISO = "+iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));			System.out.println("***IF = "+NoIF);
				ps.setInt(3, Integer.parseInt(TasaI.trim()));			System.out.println("***TASA IF = "+TasaI);
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));		System.out.println("***TIPO CREDITO = "+TipoCredito);
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));	System.out.println("***AMORTIZACION = "+Amortizacion);
				ps.setObject(6, TipoPlazo);								System.out.println("***TIPO PLAZO = "+TipoPlazo);
				if(!Emisor.equals("")) ps.setInt(7, Integer.parseInt(Emisor.trim()));	System.out.println("***EMISOR = "+Emisor);
				rs = ps.executeQuery();
				if(rs.next()){
					fechaMax = (rs.getString(1)==null)?"":rs.getString(1);
				}
				if(ps!=null) ps.close();

				if("".equals(fechaMax))
					throw new NafinException("CRED0004");
		}catch(NafinException ne){
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getFechaMinRecortado(). "+e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizacionSolicitudBean::getFechaMaxRecortado (S)");
		}
	return fechaMax;
	}


	/**
	 * Alamacena los datos de la solicitud para credito
	 * electr�nico y equipamiento
	 *
	 *
	 */
    // Agregado 16/10/2002	--CARP
    // Modificado 12/10/2004 --MPCS
	public Vector ovTransmitirDoctos( int eicProcSolic, String esAcuse )
			throws NafinException {
			BigDecimal bdMtoDoctos = new BigDecimal("0.0");
			BigDecimal bdMtoDsctos = new BigDecimal("0.0");
			String lsicIf=null, lsClaveSirac=null, lsSucursal=null, lsMoneda=null, lsNoDocto=null;
			String lsImpteDocto=null, lsImpteDscto=null, lsicEmisor=null, lstipoCred=null;
			String lsicPerioC=null, lsicPerioI=null, lsBienServ=null, lsClaseDocto=null;
			String lsDomPago=null, lsicTasaUF=null, lsRelMatUF=null, lsSobreTasUF=null;
			String lsicTasaIF=null, lsRelMatIF=null, lsSobreTasIF=null, lsNoAmort=null;
			String lstablaAmort=null, lsdfPPC=null, lsdiaPago=null, lsdfPPI=null;
			String lsdfDocto=null, lsdfDscto=null, lsPlazo=null, lsdfETC=null, lsLugarFirma=null;
			String lsamortAjuste=null, lsimporAjuste=null, lsTipoPlazo=null, lsAplicPlazo=null;
			String lsIcSolPorTmp = "";
			String lsTipoRenta	= "";
			String lsNumAmortRec = "";
			String lsPlazoMensual = "";
			String sProducto = "", sSolicPyme="", sSolicTroya="";
			String lsDestCredito = "";
			int licSolicPortal = 0;
			int liNoSolicCarga = 0;

        String codigoBaseOpDinamica = "";

        Vector lovResultado = new Vector();
        String lsSQLMax 	= null;
		StringBuffer lsCadenaSQL   	= new StringBuffer();

		ResultSet lrsSel = null;
		ResultSet lrsSel2 = null;
		AccesoDB lodbConexion = new AccesoDB();
		boolean bOk = true;
		System.out.println(" AutorizacionSolicitudEJB::ovTransmitirDoctos(E)");

		try {
			lodbConexion.conexionDB();

			lsCadenaSQL.append(
				" SELECT ic_if, ig_clave_sirac, ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto,"   +
				"        ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i,"   +
				"        SUBSTR (cg_bienes_servicios, 0, 60) cg_bienes_servicios, ic_clase_docto,"   +
				"        SUBSTR (cg_domicilio_pago, 0, 60) cg_domicilio_pago, ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif,"   +
				"        fg_stif, in_numero_amort, ic_tabla_amort, df_ppc AS fechappc, in_dia_pago, df_ppi AS fechappi,"   +
				"        df_v_documento AS fechadocto, df_v_descuento AS fechadscto, ig_plazo, df_etc AS fechaetc,"   +
				"        cg_lugar_firma, UPPER (cs_amort_ajuste) cs_amort_ajuste, fg_importe_ajuste, cs_tipo_plazo,"   +
				"        cs_plazo_maximo_factoraje, ic_solic_portal, UPPER (cs_tipo_renta) cs_tipo_renta, in_numero_amort_rec,"   +
				"        cs_plazo_mensual, ic_producto_nafin, ic_solicitud_equipo, ig_numero_solic_troya"   +
				"        ,ig_base_operacion, cg_destino_credito "   +
				"   FROM comtmp_solic_portal"   +
				"  WHERE ic_proc_solic = "+eicProcSolic+" "   +
				"    AND cg_reg_ok = 'S'");
			bOk = true;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
			while(lrsSel.next()){
				lsicIf 		 			= (lrsSel.getString("ic_if")==null)?"null" : lrsSel.getString("ic_if");
				lsClaveSirac 			= (lrsSel.getString("ig_clave_sirac")==null)?"null" : lrsSel.getString("ig_clave_sirac");
				lsSucursal	 			= (lrsSel.getString("ig_sucursal")==null)?"null" : lrsSel.getString("ig_sucursal");
				lsMoneda	 				= (lrsSel.getString("ic_moneda")==null)?"null" : lrsSel.getString("ic_moneda");
				lsNoDocto	 			= (lrsSel.getString("ig_numero_docto")==null)?"null" : lrsSel.getString("ig_numero_docto");
				lsImpteDocto 			= (lrsSel.getString("fn_importe_docto")==null)?"null" : lrsSel.getString("fn_importe_docto");
				lsImpteDscto 			= (lrsSel.getString("fn_importe_dscto")==null)?"null" : lrsSel.getString("fn_importe_dscto");
				lsicEmisor 	 			= (lrsSel.getString("ic_emisor")==null)?"null" : lrsSel.getString("ic_emisor");
				lstipoCred 	 			= (lrsSel.getString("ic_tipo_credito")==null)?"null" : lrsSel.getString("ic_tipo_credito");
				lsicPerioC 	 			= (lrsSel.getString("ic_periodicidad_c")==null)?"null" : lrsSel.getString("ic_periodicidad_c");
				lsicPerioI 	 			= (lrsSel.getString("ic_periodicidad_i")==null)?"null" : lrsSel.getString("ic_periodicidad_i");
				lsBienServ   			= (lrsSel.getString("cg_bienes_servicios")==null)?"null" : "'"+lrsSel.getString("cg_bienes_servicios")+"'";
				lsClaseDocto 			= (lrsSel.getString("ic_clase_docto")==null)?"null" : lrsSel.getString("ic_clase_docto");
				lsDomPago    			= (lrsSel.getString("cg_domicilio_pago")==null)?"null" : "'"+lrsSel.getString("cg_domicilio_pago")+"'";
				lsicTasaUF 	 			= (lrsSel.getString("ic_tasauf")==null)?"null" : lrsSel.getString("ic_tasauf");
				lsRelMatUF   			= (lrsSel.getString("cg_rmuf")==null)?"null" : "'"+lrsSel.getString("cg_rmuf")+"'";
				lsSobreTasUF 			= (lrsSel.getString("fg_stuf")==null)?"null" : lrsSel.getString("fg_stuf");
				lsicTasaIF 	 			= (lrsSel.getString("ic_tasaif")==null)?"null" : lrsSel.getString("ic_tasaif");
				lsRelMatIF   			= (lrsSel.getString("cg_rmif")==null)?"null" : "'"+lrsSel.getString("cg_rmif")+"'";
				lsSobreTasIF 			= (lrsSel.getString("fg_stif")==null)?"null" : lrsSel.getString("fg_stif");
				lsNoAmort    			= (lrsSel.getString("in_numero_amort")==null)?"null" : lrsSel.getString("in_numero_amort");
				lstablaAmort 			= (lrsSel.getString("ic_tabla_amort")==null)?"null" : lrsSel.getString("ic_tabla_amort");
				lsdfPPC 	 				= (lrsSel.getString("fechaPPC")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaPPC")+"','DD/MM/YYYY')";
				lsdiaPago 				= (lrsSel.getString("in_dia_pago")==null)?"null" : lrsSel.getString("in_dia_pago");
				lsdfPPI 					= (lrsSel.getString("fechaPPI")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaPPI")+"','DD/MM/YYYY')";
				lsdfDocto				= (lrsSel.getString("fechaDocto")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaDocto")+"','DD/MM/YYYY')";
				lsdfDscto 				= (lrsSel.getString("fechaDscto")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaDscto")+"','DD/MM/YYYY')";
				lsPlazo		 			= (lrsSel.getString("ig_plazo")==null)?"null" : lrsSel.getString("ig_plazo");
				lsdfETC		 			= (lrsSel.getString("fechaETC")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaETC")+"','DD/MM/YYYY')";
				lsLugarFirma 			= (lrsSel.getString("cg_lugar_firma")==null)?"null" : "'"+lrsSel.getString("cg_lugar_firma")+"'";
				lsamortAjuste			= (lrsSel.getString("cs_amort_ajuste")==null)?"null" : "'"+lrsSel.getString("cs_amort_ajuste")+"'";
				lsimporAjuste			= (lrsSel.getString("fg_importe_ajuste")==null)?"null" : lrsSel.getString("fg_importe_ajuste");
				lsTipoPlazo	 			= (lrsSel.getString("cs_tipo_plazo")==null)?"F" : lrsSel.getString("cs_tipo_plazo");
				lsAplicPlazo 			= (lrsSel.getString("cs_plazo_maximo_factoraje")==null)?"N" : lrsSel.getString("cs_plazo_maximo_factoraje");
				lsIcSolPorTmp			= (lrsSel.getString("ic_solic_portal")==null)?"N" : lrsSel.getString("ic_solic_portal");
				lsTipoRenta				= (lrsSel.getString("cs_tipo_renta")==null)?"null" :("'"+lrsSel.getString("cs_tipo_renta")+"'");
				lsNumAmortRec 			= (lrsSel.getString("in_numero_amort_rec")==null)?"null" : lrsSel.getString("in_numero_amort_rec");
				lsPlazoMensual			= (lrsSel.getString("cs_plazo_mensual")==null)?"null" : lrsSel.getString("cs_plazo_mensual");
				sProducto 				= (lrsSel.getString("ic_producto_nafin")==null)?"null" : lrsSel.getString("ic_producto_nafin");
				sSolicPyme				= (lrsSel.getString("ic_solicitud_equipo")==null)?"null" : lrsSel.getString("ic_solicitud_equipo");
				sSolicTroya				= (lrsSel.getString("ig_numero_solic_troya")==null)?"" : lrsSel.getString("ig_numero_solic_troya");
				codigoBaseOpDinamica = (lrsSel.getString("ig_base_operacion")==null)?"null" : lrsSel.getString("ig_base_operacion");
				lsDestCredito    			= (lrsSel.getString("cg_destino_credito")==null)?"null" : "'"+lrsSel.getString("cg_destino_credito")+"'";

				//Valida que por concurrencia no se repitan los registros
				existeNoDocto(lsNoDocto, lsicIf, lsClaveSirac, lodbConexion);


				lsSQLMax = " SELECT SEQ_COM_SOLIC_PORTAL.NEXTVAL FROM DUAL ";
				lrsSel2 = lodbConexion.queryDB(lsSQLMax);
				if (lrsSel2.next())
					licSolicPortal = (lrsSel2.getInt(1)==0)?1:lrsSel2.getInt(1);
                lrsSel2.close();


		        //Credito Electr�nico no tiene clave de producto en comcat_producto_nafin
		        //Pero se utiliza el 0 (o null en el siguiente caso) para representar
		        //el producto
		        int estatusSolicitud = (sProducto.equals("null") &&
		        		 esIfBloqueado(lsicIf, "0"))?12:1; //0 = Es la clave para Credito Electr�nico

                lsCadenaSQL.delete(0, lsCadenaSQL.length());
				lsCadenaSQL.append(
					" INSERT INTO com_solic_portal (ic_solic_portal, ic_if, ig_clave_sirac, "+
					" ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, "+
					" fn_importe_dscto, ic_emisor, ic_tipo_credito, ic_periodicidad_c, "+
					" ic_periodicidad_i, cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, "+
					" ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort, "+
					" ic_tabla_amort, df_ppc, in_dia_pago, df_ppi, df_v_documento, df_v_descuento, "+
					" ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste, fg_importe_ajuste, "+
	                " ic_estatus_solic, cc_acuse, cs_tipo_plazo, cs_plazo_maximo_factoraje,"+
					" cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual, "+
					" ic_producto_nafin,ic_solicitud_equipo,ig_numero_solic_troya, cc_llave, ig_base_operacion, cg_destino_credito) "+
					" values ("+licSolicPortal+","+lsicIf+","+lsClaveSirac+","+lsSucursal+","+
					lsMoneda+","+lsNoDocto+","+lsImpteDocto+","+lsImpteDscto+","+lsicEmisor+","+
					lstipoCred+","+lsicPerioC+","+lsicPerioI+","+lsBienServ+","+lsClaseDocto+","+
	                lsDomPago+","+lsicTasaUF+","+lsRelMatUF+","+lsSobreTasUF+","+lsicTasaIF+","+
					lsRelMatIF+","+lsSobreTasIF+","+lsNoAmort+","+lstablaAmort+","+lsdfPPC+","+
					lsdiaPago+","+lsdfPPI+","+lsdfDocto+","+lsdfDscto+","+lsPlazo+","+
	                lsdfETC+","+lsLugarFirma+","+lsamortAjuste+","+lsimporAjuste+"," +
	                estatusSolicitud + ",'"+
					esAcuse+"','"+lsTipoPlazo+"','"+lsAplicPlazo+"',"+
					lsTipoRenta+","+lsNumAmortRec+",'"+lsPlazoMensual+"',"+
					sProducto+","+sSolicPyme+",'"+sSolicTroya+"', '" +
					lsicIf+lsClaveSirac+lsNoDocto+"'||to_char(sysdate,'ddmmyyyy')"+ // Este campo se agreg� para ser la llave
																										 // y evitar registros repetidos
					","+codigoBaseOpDinamica+","+lsDestCredito+")");


                lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
				lsCadenaSQL.delete(0, lsCadenaSQL.length());
				lsCadenaSQL.append(
					" INSERT INTO com_amort_portal"   +
					"             (ic_amort_portal, ic_solic_portal, cg_tipo_amort, fn_monto, df_vencimiento)"   +
					"    SELECT ic_amort_portal, "+licSolicPortal+", UPPER (cg_tipo_amort) cg_tipo_amort, TO_NUMBER (fn_monto),"   +
					"           TO_DATE (df_vencimiento, 'dd/mm/yyyy')"   +
					"      FROM comtmp_amort_portal"   +
					"     WHERE ic_solic_portal = "+lsIcSolPorTmp+" "+
					"       AND ic_proc_solic = "+eicProcSolic+" ");

                lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
				liNoSolicCarga++;
				bdMtoDoctos = bdMtoDoctos.add(new BigDecimal(lsImpteDocto));
				bdMtoDsctos = bdMtoDsctos.add(new BigDecimal(lsImpteDscto));

			} //while
			lodbConexion.cierraStatement();

			lovResultado.addElement(new Integer(liNoSolicCarga));	//Doctos Cargados
			lovResultado.addElement(bdMtoDoctos);			//Monto total de Doctos Cargados
			lovResultado.addElement(bdMtoDsctos);			//Monto total de Doctos Cargados
			return lovResultado;
		} catch (SQLException sqleError){
			bOk = false;
			System.out.println("Error SQLException, AutorizacionSolicitudEJB.ovTransmitirDoctos(SQLException): "+ sqleError.getMessage());
			throw new NafinException("CRED0011");
		} catch (NafinException neError){
			bOk = false;
			System.out.println("Error NafinException, AutorizacionSolicitudEJB.ovTransmitirDoctos(NafinException): "+ neError.getMsgError());
			throw neError;
		} catch (Exception epError){
			bOk = false;
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovTransmitirDoctos(): "+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(bOk);
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
			System.out.println("AutorizacionSolicitudEJB::ovTransmitirDoctos(S)");
		}
	}

	/*************************************************************************
	*	    String sValidaAmortizacion()
	*  Validaci�n de Amortizaci�n.
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private Vector ovValidaAmortizacion(String esAmortAjuste,Vector eovDatos)
    	throws Exception
    {
        String amortImporte	= null;
        String amortFecha	= null;

        Vector lovResultado = new Vector();

		if (esAmortAjuste.equalsIgnoreCase("P")) {
			if (eovDatos.size()>28) {
				amortImporte=(eovDatos.size()>=30)?Comunes.quitaComitasSimples(eovDatos.get(29).toString().trim()).trim():"";
				amortFecha	=(eovDatos.size()>=31)?Comunes.quitaComitasSimples(eovDatos.get(30).toString().trim()).trim():"";
			}
		} else if(esAmortAjuste.equalsIgnoreCase("U")) {
			if (eovDatos.size()>28) {
				int elementos = eovDatos.size()-28;
				System.out.println("ELEMENTOS = "+elementos);
				if ((elementos%4) == 0) {	// Se Hace el Modulo para ver si vienen completas las duplas de amortizacion.
					int noAmorti = elementos/4;	// N�mero de Amortizaciones Provenientes.
					int indAmorImp=30, indAmorFec=31;
					System.out.println("NUMERO DE AMORTIZACIONES PROVENIENTES = "+noAmorti);
					for (int i=0; i<(noAmorti-1); i++) {
						indAmorImp = indAmorImp+4;
						indAmorFec = indAmorFec+4;
						System.out.println("ITERACION "+i+" INDAMORTIMP="+indAmorImp+" INDAMORFEC="+indAmorFec);
					}
					System.out.println("El vector de datos = "+eovDatos);
					System.out.println("El indice amort Importe = "+indAmorImp);
					System.out.println("El indice amort Fecha = "+indAmorFec);
					amortImporte=(eovDatos.size()>=indAmorImp)?Comunes.quitaComitasSimples(eovDatos.get(indAmorImp-1).toString().trim()).trim():"";
					amortFecha=(eovDatos.size()>=indAmorFec)?Comunes.quitaComitasSimples(eovDatos.get(indAmorFec-1).toString().trim()).trim():"";
				}
			} else {
				throw new Exception("Error al validar la Amortizacion, las amortizaciones no estan completas");
			}
		} else {
			throw new Exception("Error al validar la Amortizacion");
        }

        lovResultado.addElement("'"+esAmortAjuste+"'");
		lovResultado.addElement(amortImporte);
        lovResultado.addElement(amortFecha);

        return lovResultado;
    }

	/*************************************************************************
	*  String sValidaAmortizacion()
	*  Validaci�n de Amortizaci�n.
	**************************************************************************/
    //Agregado 01/11/2005		--MRZL
    private Vector ovValidaAmortizacion(String esAmortAjuste,List eovDatos)
    	throws Exception
    {
        String amortImporte	= null;
        String amortFecha	= null;

        Vector lovResultado = new Vector();

		if (esAmortAjuste.equalsIgnoreCase("P")) {
			if (eovDatos.size()>28) {
				amortImporte=(eovDatos.size()>=30)?Comunes.quitaComitasSimples(eovDatos.get(29).toString().trim()).trim():"";
				amortFecha	=(eovDatos.size()>=31)?Comunes.quitaComitasSimples(eovDatos.get(30).toString().trim()).trim():"";
			}
		} else if(esAmortAjuste.equalsIgnoreCase("U")) {
			if (eovDatos.size()>28) {
				int elementos = eovDatos.size()-28;
				System.out.println("ELEMENTOS = "+elementos);
				if ((elementos%4) == 0) {	// Se Hace el Modulo para ver si vienen completas las duplas de amortizacion.
					int noAmorti = elementos/4;	// N�mero de Amortizaciones Provenientes.
					int indAmorImp=30, indAmorFec=31;
					System.out.println("NUMERO DE AMORTIZACIONES PROVENIENTES = "+noAmorti);
					for (int i=0; i<(noAmorti-1); i++) {
						indAmorImp = indAmorImp+4;
						indAmorFec = indAmorFec+4;
						System.out.println("ITERACION "+i+" INDAMORTIMP="+indAmorImp+" INDAMORFEC="+indAmorFec);
					}
					System.out.println("El vector de datos = "+eovDatos);
					System.out.println("El indice amort Importe = "+indAmorImp);
					System.out.println("El indice amort Fecha = "+indAmorFec);
					amortImporte=(eovDatos.size()>=indAmorImp)?Comunes.quitaComitasSimples(eovDatos.get(indAmorImp-1).toString().trim()).trim():"";
					amortFecha=(eovDatos.size()>=indAmorFec)?Comunes.quitaComitasSimples(eovDatos.get(indAmorFec-1).toString().trim()).trim():"";
				}
			} else {
				throw new Exception("Error al validar la Amortizacion, las amortizaciones no estan completas");
			}
		} else {
			throw new Exception("Error al validar la Amortizacion");
        }

        lovResultado.addElement("'"+esAmortAjuste+"'");
		lovResultado.addElement(amortImporte);
        lovResultado.addElement(amortFecha);

        return lovResultado;
    }


	public void procesarSolicitudes(String folios)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = "";
		ResultSet rs = null;
		boolean ok = true;

		try {
			con.conexionDB();

			qrySentencia="select count(*) from com_solicitud"+
				" where ic_folio in ("+folios+")"+
				" and ic_estatus_solic != 1";

			rs = con.queryDB(qrySentencia);
			rs.next();

			if (rs.getInt(1) == 0) {//VALIDACION para evitar errores de CONCURRENCIA
				qrySentencia="update com_solicitud set ic_estatus_solic=2 "+
					" where ic_folio in ("+folios+")";

				con.ejecutaSQL(qrySentencia);
			} else {
				throw new NafinException("DSCT0025"); //Error por concurrencia
			}

		} catch(NafinException ne){
                        ok = false;
                	throw ne;
		} catch (Exception e) {
                	ok = false;
			System.out.println("Error en procesarSolicitudes: " + e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
                        	con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

	/*************************************************************************
	*	    String sValidaEmisor()
	*  Validaci�n emisor.
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private String sValidaEmisor( String esEmisor, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licEmisor 	= null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_emisor from comcat_emisor ");
		lsSQL.append(" where ic_emisor = "+esEmisor);

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licEmisor = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar el emisor");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licEmisor;
    }


	/*************************************************************************
	*	    String sValidaPeriodicidadPP()
	*  Validaci�n para la Periodicidad del Pago de Capital.
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private String sValidaPeriodicidadPP( String esppc, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licTipoPPC = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_periodicidad from comcat_periodicidad ");
		lsSQL.append(" where ic_periodicidad = "+esppc);

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licTipoPPC = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Periodicidad del Pago de Capital");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licTipoPPC;
    }
	/*************************************************************************
	*	    String sValidaClaseDocto()
	*  Validaci�n para la Clase del Documento.
	**************************************************************************/
    //Agregado 15/01/2003		--HDG
    private String sValidaClaseDocto( String esClaseDocto, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licClaseDocto = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_clase_docto from comcat_clase_docto ");
		lsSQL.append(" where ic_clase_docto = "+esClaseDocto);

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licClaseDocto = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Clase del Documento");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licClaseDocto;
    }
	/*************************************************************************
	*	    String sValidaTablaAmortizacion()
	*  Validaci�n de la Tabla de Amortizaci�n.
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private String sValidaTablaAmortizacion( String establaAmortizacion, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licTablaAmort = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_tabla_amort from comcat_tabla_amort ");
		lsSQL.append(" where ic_tabla_amort = "+establaAmortizacion);

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licTablaAmort = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Tabla de Amortizaci�n");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licTablaAmort;
    }
	/*************************************************************************
	*	    String sValidaTasaInteresIntermed()
	*  Validaci�n de la Tasa de Interes clave del Intermediario.
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private String sValidaTasaInteresIntermed( String estasaInteresIntermed, String habilitado, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licTasaIntermed = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_tasa from comcat_tasa ");
		lsSQL.append(" where ic_tasa = "+estasaInteresIntermed);

		if (habilitado.equals("S"))
			lsSQL.append(" and cs_creditoelec = 'S'");

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licTasaIntermed = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Tasa de Interes clave del Intermediario");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licTasaIntermed;
    }
	/*************************************************************************
	*	    String sValidatipoCredito()
	*  Validaci�n Tipo cr�dito
	**************************************************************************/
    //Agregado 15/10/2002		--CARP
    private String sValidaTipoCredito( String esTipoCredito, AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        String licTipoCredito = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_tipo_credito from comcat_tipo_credito ");
		lsSQL.append(" where ic_tipo_credito = "+esTipoCredito);
		lsSQL.append(" and cs_creditoelec = 'S'");

		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		if (lrsSel.next())
			licTipoCredito = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar el Tipo cr�dito");
        }
        eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return licTipoCredito;
    }

	public Hashtable ohProcesaOperacSolicitudes(String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
												String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto,
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios,
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF,
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI,
												String NoAmortizaciones, String Amortizacion, String FechaPPC,
												String DiaPago, String FechaPPI, String FechaVencDocto,
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,
												String AmortizacionAjuste, String ImporAmortAjuste,
												String FechaAmortAjuste, String Acuse) throws NafinException	{
	String lsQry = "", sTomoPlazoGral = "N";
	ResultSet lrs = null;
	AccesoDB lobdCon = new AccesoDB();
	StringBuffer lsbError = new StringBuffer();
	Hashtable hRes = new Hashtable();
	boolean lbOK = true;
		try {
			lobdCon.conexionDB();
			String NoSolicPortal="";
			int Plazo=0;

			//Validaci�n para el Intermediario.
			if(!Comunes.esNumero(NoIF)) {
				lsbError.append("No es V�lido el N�mero de Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Clave de Sirac.
			if (!Comunes.esNumero(NoClienteSIRAC)) {
				lsbError.append("No es V�lido el N�mero de Sirac.\n");
				lbOK = false;
			} /*else {
				lsQry = "select in_numero_sirac from comcat_pyme "+
						"where in_numero_sirac ="+NoClienteSIRAC;
				lrs = lobdCon.queryDB(lsQry);
				if(!lrs.next()) {
					lsbError.append("El N�mero Sirac no existe para poder realizar operaciones.\n");
					lbOK = false;
				}
	            lobdCon.cierraStatement();
			}*/

			//Validaci�n para el N�mero de Sucursal del Banco.
			NoSucBanco = (NoSucBanco.equals(""))?"null":NoSucBanco;
			//Validaci�n para el tipo de Moneda.
			if(!Moneda.equals("1") && !Moneda.equals("54")) {
				lsbError.append("No es V�lido el tipo de Moneda.\n");
				lbOK = false;
			}
			//Validaci�n para el N�mero de Documento.
			if (!Comunes.esNumero(NoDocumento)) {
				lsbError.append("No es V�lido el N�mero de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Importe de Documento.
			if (!Comunes.esDecimal(ImporteDocto)) {
				lsbError.append("No es V�lido el Importe del Documento.\n");
				lbOK = false;
			} else {
				if ((ImporteDocto.substring(ImporteDocto.indexOf(".")+1,ImporteDocto.length())).length() > 2) {
					lsbError.append("El Importe del Documento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para el Importe de Descuento.
			if (!Comunes.esDecimal(ImporteDscto)) {
				lsbError.append("El Importe del Descuento no es un N�mero V�lido.\n");
				lbOK = false;
			} else {
				if ((ImporteDscto.substring(ImporteDscto.indexOf(".")+1,ImporteDscto.length())).length() > 2) {
					lsbError.append("El Importe del Descuento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para la Clave del Emisor.

			//Validaci�n para el Tipo de Cr�dito.
			if(!Comunes.esNumero(TipoCredito)) {
				lsbError.append("No es V�lido el tipo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Capital.
			if(!Comunes.esNumero(PPCapital)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Intereses.
			if(!Comunes.esNumero(PPIntereses)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n de los Bienes y Servicios.
			BienesServicios=(BienesServicios.length()>60)?"'"+BienesServicios.substring(0,59)+"'":"'"+BienesServicios+"'";
			//Validaci�n de la Clase de Documento.
			if(!Comunes.esNumero(ClaseDocto)) {
				lsbError.append("No es V�lido la Clase de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Domicilio de Pago.
			DomicilioPago=(DomicilioPago.equals(""))?"null":(DomicilioPago.length()>60?"'"+DomicilioPago.substring(0,59)+"'":"'"+DomicilioPago+"'");
			//Validaci�n para la Tasa de Interes del Usuario Final.
			if(TasaUF.equals(""))
				TasaUF = "null";
			else if(!Comunes.esNumero(TasaUF)) {
				lsbError.append("No es V�lida la Tasa Base del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Usuario Final.
			if(RelMatUF.equals(""))
				RelMatUF = "null";
			else if(!RelMatUF.equals("+") && !RelMatUF.equals("-") && !RelMatUF.equals("*") && RelMatUF.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Usuario Final.\n");
				lbOK = false;
			} else
				RelMatUF = "'"+RelMatUF+"'";
			//Validaci�n para la SobreTasa del Usuario Final.
			if(SobreTasaUF.equals(""))
				SobreTasaUF = "null";
			else if(!Comunes.esDecimal(SobreTasaUF)) {
				lsbError.append("No es V�lida la SobreTasa del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Tasa de Interes del Intermediario.
			if(!Comunes.esNumero(TasaI)) {
				lsbError.append("No es V�lida la Tasa de Interes del Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Intermediario.
			if(RelMatI.equals(""))
				RelMatI = "null";
			else if(!RelMatI.equals("+") && !RelMatI.equals("-") && !RelMatI.equals("*") && RelMatI.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Intermediario.\n");
				lbOK = false;
			} else
				RelMatI = "'"+RelMatI+"'";
			//Validaci�n para la SobreTasa del Intermediario.
			if(SobreTasaI.equals(""))
				SobreTasaI = "null";
			else if(!Comunes.esNumero(SobreTasaI)) {
				lsbError.append("No es V�lida la SobreTasa del Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para el N�mero de Amortizaciones.
			if(!Comunes.esNumero(NoAmortizaciones)) {
				lsbError.append("No es V�lido el N�mero de Amortizaciones.\n");
				lbOK = false;
			}
			//Validaci�n para el Tipo de Amortizaci�n.
			if(!Comunes.esNumero(Amortizacion)) {
				lsbError.append("No es V�lido el Tipo de Amortizaci�n.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Capital.
			if(!Comunes.checaFecha(FechaPPC)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para el D�a de Pago.
			if(!Comunes.esNumero(DiaPago)) {
				lsbError.append("No es V�lido el D�a de Pago.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Interes.
			if(!Comunes.checaFecha(FechaPPI)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Vencimiento del Documento.
			if(!FechaVencDocto.equals("")) {
				if(!Comunes.checaFecha(FechaVencDocto)) {
					lsbError.append("No es V�lida la Fecha de Vencimiento del Documento.\n");
					lbOK = false;
				}
			}
			//Validaci�n para la Fecha de Vencimiento del Descuento.
			if(!Comunes.checaFecha(FechaVencDscto)) {
				lsbError.append("No es V�lida la Fecha de Vencimiento del Descuento.\n");
				lbOK = false;
			} else {
				lsQry = "select trunc(TO_DATE('"+FechaVencDscto+"','dd/mm/yyyy')) - trunc(SYSDATE) from dual";
				lrs = lobdCon.queryDB(lsQry);
				if (lrs.next())
					Plazo = lrs.getInt(1);
	            lobdCon.cierraStatement();
				// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
				if(lbOK) {	// Si paso bien todas las condiciones de tipos y valores se hace la validaci�n.
					int iTipoPiso = getTipoPisoIF(NoIF, lobdCon);
					Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIF, TasaI, TipoCredito, Amortizacion, Emisor, Moneda, lobdCon);
					int iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
					if(TipoPlazo.equals("C")) {	// Cr�dito.
						if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en al b.o. se manda el mensaje.
							if(iTipoPiso==1) {
								lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
								lbOK = false;
							}
							if(iTipoPiso==2) {
								lsbError.append("Clave 1008, Favor de comunicarse al 01 800 CADENAS.\n");
								lbOK = false;
							}
						} else if(Plazo > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							lsbError.append("Plazo mayor al m�ximo permitido.\n");
							lbOK = false;
						}
					} else if(TipoPlazo.equals("F")) {	//	Factoraje.
						if(iPlazoMaxBO==0 && iTipoPiso==1) {
							lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
							lbOK = false;
						}
						if(lbOK) {	// Si no hay alg�n error se recorta el plazo si el IF es de 1ro � 2do piso.
							if(Plazo > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
								lsbError.append("Plazo mayor al m�ximo permitido.\n");
								lbOK = false;
								/*GregorianCalendar cFechaVencDesc = new GregorianCalendar();
								cFechaVencDesc.add(Calendar.DATE, iPlazoMaxBO);	//Se Suma el plazo a la fecha de hoy.
								// Validamos si la fecha del plazo es d�a inhabil.
								Vector vDiasInhabiles = getDiasInhabiles(lobdCon); // Obtenemos los d�as inhabiles.
								Hashtable hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO);
								cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
								Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
								boolean bCambioPlazo = false;
								bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								while(bCambioPlazo) {
									hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, Plazo);
									cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
									Plazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
									bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
								}
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
								FechaVencDscto = sdf.format(cFechaVencDesc.getTime());
								// Se reasigna la fecha PPC y la fecha PPI si alguna o ambas son mayores a la de Descuento.
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(FechaPPC))) {
									FechaPPC = sdf.format(cFechaVencDesc.getTime());
									DiaPago = sdf.format(cFechaVencDesc.getTime()).substring(0,2);
								}
								if(cFechaVencDesc.getTime().before(Comunes.parseDate(FechaPPI)))
									FechaPPI = sdf.format(cFechaVencDesc.getTime());

								sTomoPlazoGral = hPlazoBO.get("sTomoPlazoGral").toString();*/
							} // Plazo > iPlazoMaxBO
						}
					} // TipoPlazo.equals("F")
				} // lbOK
			}
			//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito.
			if(!Comunes.checaFecha(FechaEmisionTC)) {
				lsbError.append("No es V�lida la Fecha de Emisi�n del T�tulo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para el Lugar de Firma.
			LugarFirma = (LugarFirma.length()>50)?"'"+LugarFirma.substring(0,49)+"'":"'"+LugarFirma+"'";
			//Validaci�n para el tipo de Amortizaci�n de Ajuste.
			if(!AmortizacionAjuste.trim().equals("")) {
				if(!AmortizacionAjuste.equalsIgnoreCase("P") && !AmortizacionAjuste.equalsIgnoreCase("U")) {
					lsbError.append("No es V�lido el Tipo de Amortizaci�n de Ajuste.\n");
					lbOK = false;
				} else
					AmortizacionAjuste = "'"+AmortizacionAjuste.toUpperCase()+"'";
			} else
				AmortizacionAjuste = "null";
			//Validaci�n para el Importe del Ajuste.
			if(!ImporAmortAjuste.equals("")) {
				if (!Comunes.esDecimal(ImporAmortAjuste)) {
					lsbError.append("No es V�lido el Importe de la Amortizaci�n del Ajuste.\n");
					lbOK = false;
				} else {
					if ((ImporAmortAjuste.substring(ImporAmortAjuste.indexOf(".")+1,ImporAmortAjuste.length())).length() > 2) {
						lsbError.append("El Importe Amortizaci�n Ajuste no puede tener mas de 2 decimales.\n");
						lbOK = false;
					}
				}
			} else
				ImporAmortAjuste = "null";
			//Validaci�n para la Fecha de Amortizaci�n Ajuste.

			if(lbOK) {
				lsQry = "SELECT SEQ_COM_SOLIC_PORTAL.NEXTVAL FROM DUAL";
				lrs = lobdCon.queryDB(lsQry);
				if (lrs.next())
					NoSolicPortal = lrs.getString(1);
	            lobdCon.cierraStatement();

				lsQry = "INSERT INTO com_solic_portal (ic_solic_portal, ic_if, ig_clave_sirac, "+
						"ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto, "+
						"ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i, "+
						"cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, ic_tasauf, "+
						"cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort, "+
						"ic_tabla_amort, df_ppc, in_dia_pago, df_ppi, df_v_documento, df_v_descuento, "+
						"ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste, fg_importe_ajuste, "+
						"ic_estatus_solic, cc_acuse, cs_tipo_plazo, cs_plazo_maximo_factoraje) "+
						"values("+NoSolicPortal+","+NoIF+","+NoClienteSIRAC+","+NoSucBanco+","+Moneda
						+","+NoDocumento+","+ImporteDocto+","+ImporteDscto+","+(Emisor.equals("")?"null":Emisor)
	           			+","+TipoCredito+","+PPCapital+","+PPIntereses+","+BienesServicios
						+","+ClaseDocto+","+DomicilioPago+","+TasaUF+","+RelMatUF+","+SobreTasaUF
						+","+TasaI+","+RelMatI+","+SobreTasaI+","+NoAmortizaciones+","+Amortizacion
						+",TO_DATE('"+FechaPPC+"','DD/MM/YYYY'),"+DiaPago
	            		+",TO_DATE('"+FechaPPI+"','DD/MM/YYYY'),TO_DATE('"+FechaVencDocto+"','DD/MM/YYYY')"
						+",TO_DATE('"+FechaVencDscto+"','DD/MM/YYYY'),"+Plazo
						+",TO_DATE('"+FechaEmisionTC+"','DD/MM/YYYY'),"+LugarFirma+","+AmortizacionAjuste
	            		+","+ImporAmortAjuste+",1,'"+Acuse+"','"+TipoPlazo+"','"+sTomoPlazoGral+"')";
				try {
	               	lobdCon.ejecutaSQL(lsQry);
				} catch (Exception err) {
					throw new NafinException("DSCT0061");
				}
			}
			hRes.put("lbOK", new Boolean(lbOK));
			hRes.put("lsbError", lsbError.toString());
			hRes.put("NoSolicPortal", NoSolicPortal);
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) { e.printStackTrace(); }
		finally {
			lobdCon.terminaTransaccion(lbOK);
			if(lobdCon.hayConexionAbierta()) lobdCon.cierraConexionDB();
		}
	return hRes;
	}



	/* Version donde se agregan los parametros necesarios para Equipamiento Electronico MPCS 08/10/2004 */
	/****************************************************************************************************/
	public Hashtable ohProcesaOperacSolicTmp(String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
												String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto,
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios,
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF,
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI,
												String NoAmortizaciones, String Amortizacion, String FechaPPC,
												String DiaPago, String FechaPPI, String FechaVencDocto,
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,
												String AmortizacionAjuste, String ImporAmortAjuste,
												String FechaAmortAjuste,String tipoRenta,int numeroAmortRec,
												String fvencAmort[],String tipoAmort[],String montoAmort[],
												String sTipoPiso, String sSolicPYME, String sPyme,
												String sSolicTROYA, String sProducto, String codigoBaseOperacion, String destinoCredito ) throws NafinException	{
	String lsQry = "", sTomoPlazoGral = "N";
	ResultSet lrs = null;
	AccesoDB lobdCon = new AccesoDB();
	StringBuffer lsbError = new StringBuffer();
	Hashtable hRes = new Hashtable();
	boolean lbOK = true;
		try {
			lobdCon.conexionDB();
			String NoSolicPortal="";
			String NoProcSolic = "";
			int Plazo=0;

			boolean	codigoBaseOpDinamicaHabilitado 	= false;
			boolean  existeCodigoBaseOpDinamica 		= false;

			//Validaci�n para el Intermediario.
			if(!Comunes.esNumero(NoIF)) {
				lsbError.append("No es V�lido el N�mero de Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Clave de Sirac.
			if (!Comunes.esNumero(NoClienteSIRAC)) {
				lsbError.append("No es V�lido el N�mero de Sirac.\n");
				lbOK = false;
			}

			/****** Validaciones agregadas para Equipamiento Electronico *************/
			/*************** Solo aplican si son proporcionadas **********************/
			//Validaci�n para el Tipo de Piso
			if(!"".equals(sTipoPiso)&&!sTipoPiso.equals("1") && !sTipoPiso.equals("2")) {
				lsbError.append("No es V�lido el Tipo de Piso del Intermediario.\n");
				lbOK = false;
			}
			//Validaciones para el ic_pyme
			if (!"".equals(sPyme)&&!Comunes.esNumero(sPyme)) {
				lsbError.append("No es V�lido la Clave de Pyme.\n");
				lbOK = false;
			}
			//Validaciones para el producto nafin
			if (!"".equals(sProducto)&&!Comunes.esNumero(sProducto)) {
				lsbError.append("No es V�lido la Clave del Producto.\n");
				lbOK = false;
			}
			//Validaciones sobre el ig_folio de la solicitud de credito registrada por la Pyme
			if (!"".equals(sSolicPYME)&&!Comunes.esNumero(sSolicPYME)) {
				lsbError.append("No es V�lido el N�mero de Solicitud registrada por la Pyme.\n");
				lbOK = false;
			}
			//Validaciones sobre la solicitud de troya
			if ("1".equals(sTipoPiso)&&!"".equals(sSolicTROYA)&&!Comunes.esNumero(sSolicTROYA)) {
				lsbError.append("No es V�lido el N�mero de Solicitud Troya.\n");
				lbOK = false;
			}
			/*************************************************************************/

			//Validaci�n para el N�mero de Sucursal del Banco.
			NoSucBanco = (NoSucBanco.equals(""))?"null":NoSucBanco;
			//Validaci�n para el tipo de Moneda.
			if(!Moneda.equals("1") && !Moneda.equals("54")) {
				lsbError.append("No es V�lido el tipo de Moneda.\n");
				lbOK = false;
			}
			//Validaci�n para el N�mero de Documento.
			if (!Comunes.esNumero(NoDocumento)) {
				lsbError.append("No es V�lido el N�mero de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Importe de Documento.
			if (!Comunes.esDecimal(ImporteDocto)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lido el Importe del Cr�dito.\n":"No es V�lido el Importe del Documento.\n");
				lbOK = false;
			} else {
				if ((ImporteDocto.substring(ImporteDocto.indexOf(".")+1,ImporteDocto.length())).length() > 2) {
					lsbError.append("El Importe del Documento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para el Importe de Descuento.
			if (!Comunes.esDecimal(ImporteDscto)) {
				lsbError.append(("7".equals(sProducto))?"El Importe de Disposici�n no es un N�mero V�lido.\n":"El Importe del Descuento no es un N�mero V�lido.\n");
				lbOK = false;
			} else {
				if ((ImporteDscto.substring(ImporteDscto.indexOf(".")+1,ImporteDscto.length())).length() > 2) {
					lsbError.append(("7".equals(sProducto))?"El Importe de Disposici�n no puede tener mas de 2 decimales.\n":"El Importe del Descuento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para el Tipo de Cr�dito.
			if(!Comunes.esNumero(TipoCredito)) {
				lsbError.append("No es V�lido el tipo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Capital.
			if(!Comunes.esNumero(PPCapital)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Intereses.
			if(!Comunes.esNumero(PPIntereses)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n de los Bienes y Servicios.
			BienesServicios=(BienesServicios.length()>60)?"'"+BienesServicios.substring(0,59)+"'":"'"+BienesServicios+"'";
			//Validaci�n de la Clase de Documento.
			if(!Comunes.esNumero(ClaseDocto)) {
				lsbError.append("No es V�lido la Clase de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Domicilio de Pago.
			DomicilioPago=(DomicilioPago.equals(""))?"null":(DomicilioPago.length()>60?"'"+DomicilioPago.substring(0,59)+"'":"'"+DomicilioPago+"'");
			
			destinoCredito=(destinoCredito.equals(""))?"null":(destinoCredito.length()>400?"'"+destinoCredito.substring(0,399)+"'":"'"+destinoCredito+"'");
			
			//Validaci�n para la Tasa de Interes del Usuario Final.
			if(TasaUF.equals(""))
				TasaUF = "null";
			else if(!Comunes.esNumero(TasaUF)) {
				lsbError.append("No es V�lida la Tasa Base del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Usuario Final.
			if(RelMatUF.equals(""))
				RelMatUF = "null";
			else if(!RelMatUF.equals("+") && !RelMatUF.equals("-") && !RelMatUF.equals("*") && RelMatUF.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Usuario Final.\n");
				lbOK = false;
			} else
				RelMatUF = "'"+RelMatUF+"'";
			//Validaci�n para la SobreTasa del Usuario Final.
			if(SobreTasaUF.equals(""))
				SobreTasaUF = "null";
			else if(!Comunes.esDecimal(SobreTasaUF)) {
				lsbError.append("No es V�lida la SobreTasa del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Tasa de Interes del Intermediario.
			if(!Comunes.esNumero(TasaI)) {
				lsbError.append("No es V�lida la Tasa de Interes del Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Intermediario.
			if(RelMatI.equals(""))
				RelMatI = "null";
			else if(!RelMatI.equals("+") && !RelMatI.equals("-") && !RelMatI.equals("*") && RelMatI.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Intermediario.\n");
				lbOK = false;
			} else
				RelMatI = "'"+RelMatI+"'";
			//Validaci�n para la SobreTasa del Intermediario.
			if(SobreTasaI.equals(""))
				SobreTasaI = "null";
			else if(!Comunes.esNumero(SobreTasaI)) {
				lsbError.append("No es V�lida la SobreTasa del Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para el N�mero de Amortizaciones.
			if(!Comunes.esNumero(NoAmortizaciones)) {
				lsbError.append("No es V�lido el N�mero de Amortizaciones.\n");
				lbOK = false;
			}
			//Validaci�n para el Tipo de Amortizaci�n.
			if(!Comunes.esNumero(Amortizacion)) {
				lsbError.append("No es V�lido el Tipo de Amortizaci�n.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Capital.
			if(!Comunes.checaFecha(FechaPPC)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para el D�a de Pago.
			if(!Comunes.esNumero(DiaPago)) {
				lsbError.append("No es V�lido el D�a de Pago.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Interes.
			if(!Comunes.checaFecha(FechaPPI)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Vencimiento del Documento.
			if(!FechaVencDocto.equals("")) {
				if(!Comunes.checaFecha(FechaVencDocto)) {
					lsbError.append("No es V�lida la Fecha de Vencimiento del Documento.\n");
					lbOK = false;
				}
			}
			//Validaci�n para la Fecha de Vencimiento del Descuento.
			if(!Comunes.checaFecha(FechaVencDscto)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lida la Fecha de Vencimiento de la L�nea.\n":"No es V�lida la Fecha de Vencimiento del Descuento.\n");
				lbOK = false;
			} else {
				lsQry = "select trunc(TO_DATE('"+FechaVencDscto+"','dd/mm/yyyy')) - trunc(SYSDATE) from dual";
				lrs = lobdCon.queryDB(lsQry);
				if (lrs.next())
					Plazo = lrs.getInt(1);
	            lobdCon.cierraStatement();
				// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
				if(lbOK) {	// Si paso bien todas las condiciones de tipos y valores se hace la validaci�n.
					int iTipoPiso = getTipoPisoIF(NoIF, lobdCon);
					Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIF, TasaI, TipoCredito, Amortizacion, Emisor, Moneda, lobdCon, sProducto, PPCapital, tipoRenta);
					int iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
					if(TipoPlazo.equals("C")) {	// Cr�dito.
						if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en al b.o. se manda el mensaje.
							if(iTipoPiso==1) {
								lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
								lbOK = false;
							}
							if(iTipoPiso==2) {
								lsbError.append("Clave 1008, Favor de comunicarse al 01 800 CADENAS.\n");
								lbOK = false;
							}
						} else if(Plazo > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							lsbError.append("Plazo mayor al m�ximo permitido.\n");
							lbOK = false;
						}
					} else if(TipoPlazo.equals("F")) {	//	Factoraje.
						if(iPlazoMaxBO==0 && iTipoPiso==1) {
							lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
							lbOK = false;
						}
						if(lbOK) {	// Si no hay alg�n error se recorta el plazo si el IF es de 1ro � 2do piso.
							if(Plazo > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
								lsbError.append("Plazo mayor al m�ximo permitido.\n");
								lbOK = false;

							} // Plazo > iPlazoMaxBO
						}
					} // TipoPlazo.equals("F")

					// Validar C�digo de Operaci�n Din�mica
					codigoBaseOpDinamicaHabilitado 	= ( (Boolean) hPlazoBO.get("codigoBaseOpDinamicaHabilitado") ).booleanValue();
					existeCodigoBaseOpDinamica		= codigoBaseOperacion != null && !"".equals(codigoBaseOperacion.trim())?true:false;
					if(         !codigoBaseOpDinamicaHabilitado && existeCodigoBaseOpDinamica  ){
						lsbError.append("El C�digo de la Base de Operaci�n Din�mica fue incluido, pero no es requerido.\n");
						lbOK = false;
					}

				} // lbOK
			}
			//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito.
			if(!Comunes.checaFecha(FechaEmisionTC)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lida la Fecha de Emisi�n del Pagar�.\n":"No es V�lida la Fecha de Emisi�n del T�tulo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para el Lugar de Firma.
			LugarFirma = (LugarFirma.length()>50)?"'"+LugarFirma.substring(0,49)+"'":"'"+LugarFirma+"'";
			//Validaci�n para el tipo de Amortizaci�n de Ajuste.
			if(!AmortizacionAjuste.trim().equals("")) {
				if(!AmortizacionAjuste.equalsIgnoreCase("P") && !AmortizacionAjuste.equalsIgnoreCase("U")) {
					lsbError.append("No es V�lido el Tipo de Amortizaci�n de Ajuste.\n");
					lbOK = false;
				} else
					AmortizacionAjuste = "'"+AmortizacionAjuste.toUpperCase()+"'";
			} else
				AmortizacionAjuste = "null";
			//Validaci�n para el Importe del Ajuste.
			if(!ImporAmortAjuste.equals("")) {
				if (!Comunes.esDecimal(ImporAmortAjuste)) {
					lsbError.append("No es V�lido el Importe de la Amortizaci�n del Ajuste.\n");
					lbOK = false;
				} else {
					if ((ImporAmortAjuste.substring(ImporAmortAjuste.indexOf(".")+1,ImporAmortAjuste.length())).length() > 2) {
						lsbError.append("El Importe Amortizaci�n Ajuste no puede tener mas de 2 decimales.\n");
						lbOK = false;
					}
				}
			} else
				ImporAmortAjuste = "null";

			lsQry = " select seq_comtmp_proc_solic_portal.NEXTVAL from dual"  ;
			lrs = lobdCon.queryDB(lsQry);
			if (lrs.next()){
				NoProcSolic		= lrs.getString(1);
			}
            lobdCon.cierraStatement();
			lsQry = " select NVL(max(ic_solic_portal),0)+1 from comtmp_solic_portal"  ;
			lrs = lobdCon.queryDB(lsQry);
			if (lrs.next()){
				NoSolicPortal	= lrs.getString(1);
			}
            lobdCon.cierraStatement();

			//Valida que no se repita el numero de documento
			try{
				existeNoDocto(NoDocumento,NoIF,NoProcSolic,NoClienteSIRAC,lobdCon);
			}catch(NafinException ne){
				lsbError.append(ne.getMsgError()+".\n");
				lbOK = false;
			}

			if(lbOK) {

				if("S".equals(tipoRenta)||"N".equals(tipoRenta))
					tipoRenta = "'"+tipoRenta+"'";
				else
					tipoRenta = "null";

				String plazoMensual = "S";
				java.util.Date fecAnt = Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));

				if (!"null".equals(tipoRenta)&&fvencAmort!=null) {
					for(int i=0;i<fvencAmort.length;i++){
						java.util.Date fec = Comunes.parseDate(fvencAmort[i]);
						if(fecAnt!=null){
							long lfecAnt = fecAnt.getTime();
							long lfec 	 = fec.getTime();
							double diferencia = (lfec - lfecAnt)/86400000;

							System.out.println("\n\n\nLINEA 6290 LA DIFERENCIA = "+diferencia);

							if((int)diferencia>31){
								plazoMensual = "N";
							}
						}
						fecAnt = fec;
					}
				}


				lsQry = " INSERT INTO comtmp_solic_portal (ic_proc_solic,ic_solic_portal, ic_if, ig_clave_sirac, "+
						" ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto, "+
						" ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i, "+
						" cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, ic_tasauf, "+
						" cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort, "+
						" ic_tabla_amort, df_ppc, in_dia_pago, df_ppi, df_v_documento, df_v_descuento, "+
						" ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste, fg_importe_ajuste, "+
						" cs_tipo_plazo, cs_plazo_maximo_factoraje,cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual, "+
						" ic_producto_nafin,ig_numero_solic_troya,ic_solicitud_equipo, ig_base_operacion, cg_destino_credito ) "+
						" values("+NoProcSolic+","+NoSolicPortal+","+NoIF+","+NoClienteSIRAC+","+NoSucBanco+","+Moneda
						+" ,"+NoDocumento+","+ImporteDocto+","+ImporteDscto+","+(Emisor.equals("")?"null":Emisor)
	           			+" ,"+TipoCredito+","+PPCapital+","+PPIntereses+","+BienesServicios
						+" ,"+ClaseDocto+","+DomicilioPago+","+TasaUF+","+RelMatUF+","+SobreTasaUF
						+" ,"+TasaI+","+RelMatI+","+SobreTasaI+","+NoAmortizaciones+","+Amortizacion
						+" ,'"+FechaPPC+"',"+DiaPago
	            		+" ,'"+FechaPPI+"','"+FechaVencDocto+"'"
						+" ,'"+FechaVencDscto+"',"+Plazo
						+" ,'"+FechaEmisionTC+"',"+LugarFirma+","+AmortizacionAjuste
            		+" ,"+ImporAmortAjuste+",'"+TipoPlazo+"','"+sTomoPlazoGral+"',"+tipoRenta+","+numeroAmortRec+",'"+plazoMensual+"',"
            		+(sProducto.equals("")?"null":sProducto)+","+(sSolicTROYA.equals("")?"null":sSolicTROYA)+","+(sSolicPYME.equals("")?"null":sSolicPYME)
            		+","+(existeCodigoBaseOpDinamica?codigoBaseOperacion:"null")
						+","+(destinoCredito.equals("")?"null":destinoCredito)+")";
/*						+" ,TO_DATE('"+FechaPPC+"','DD/MM/YYYY'),"+DiaPago
//	            		+" ,TO_DATE('"+FechaPPI+"','DD/MM/YYYY'),TO_DATE('"+FechaVencDocto+"','DD/MM/YYYY')"
//						+" ,TO_DATE('"+FechaVencDscto+"','DD/MM/YYYY'),"+Plazo
//						+" ,TO_DATE('"+FechaEmisionTC+"','DD/MM/YYYY'),"+LugarFirma+","+AmortizacionAjuste*/
				try {
					System.out.println("lsQry)" + lsQry);
					lobdCon.ejecutaSQL(lsQry);
				} catch (Exception err) {
					throw new NafinException("DSCT0061");
				}
				if (tipoAmort==null ){
					if (!"".equals(sProducto)){
						ovGenerarTablaAmortizacionesTmp(NoSolicPortal,FechaPPC,PPCapital,ImporteDscto,NoAmortizaciones,lobdCon);
					}
				} else{
					ovInsertaAmortizacionesTmp(NoSolicPortal,NoProcSolic,fvencAmort,tipoAmort,montoAmort,lobdCon);
				} //Fin de la manera en que se generar� el calendario
			}

			hRes.put("lbOK", new Boolean(lbOK));
			hRes.put("lsbError", lsbError.toString());
			hRes.put("NoSolicPortal", NoSolicPortal);
			hRes.put("NoProcSolic", NoProcSolic);
		}catch(NafinException ne) {
			lbOK = false;
			throw ne;
		}catch(Exception e) {
			lbOK = false;
			e.printStackTrace();
		}finally {
			lobdCon.terminaTransaccion(lbOK);
			if(lobdCon.hayConexionAbierta()) lobdCon.cierraConexionDB();
		}
	return hRes;
	} // Fin del m�todo ohProcesaOperacSolicTmp que inlcuye Equipamiento Electronico

	private void existeNoDocto(String NoDocto,String NoIF,String icProcSolic,String claveSirac,AccesoDB con)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::existeNoDocto (E)");
		PreparedStatement	ps 		= null;
		ResultSet			rs 		= null;
		String		qrySentencia	= "";
			try{
				int numCols = 0;
				/*qrySentencia =
					" select count(*) from com_solic_portal" +
					" where ig_numero_docto = ?" +
					" and ic_if = ?" +
					" and IG_CLAVE_SIRAC = ?"+
					" and trunc(df_carga) = trunc(sysdate)";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,NoDocto);
				ps.setString(2,NoIF);
				ps.setString(3,claveSirac);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				ps.close();
				if(numCols>0)
					throw new NafinException("CRED0009");*/

				existeNoDocto(NoDocto, NoIF, claveSirac, con);

				qrySentencia =
					" select count(1) from comtmp_solic_portal" +
					" where ig_numero_docto = ?" +
					" and ic_if = ?" +
					" and ic_proc_solic = ?"+
					" and IG_CLAVE_SIRAC = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,NoDocto);
				ps.setString(2,NoIF);
				ps.setString(3,icProcSolic);
				ps.setString(4,claveSirac);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				ps.close();
				if(numCols>0)
					throw new NafinException("CRED0009");

			}catch(NafinException ne){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto(NafinException) "+ne);
				throw ne;
			}catch(Exception e){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto Exception "+e);
				throw new NafinException("SIST0001");
			}finally{
				System.out.println("AutorizacionSolicitudBean::existeNoDocto (s)");
			}
	}

		private void existeNoDoctoE(String NoDocto,String NoIF,String claveSirac,AccesoDB con)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::existeNoDocto (E)");
		PreparedStatement	ps 		= null;
		ResultSet			rs 		= null;
		String		qrySentencia	= "";
			try{
				int numCols = 0;
				/*qrySentencia =
					" select count(*) from com_solic_portal" +
					" where ig_numero_docto = ?" +
					" and ic_if = ?" +
					" and IG_CLAVE_SIRAC = ?"+
					" and trunc(df_carga) = trunc(sysdate)";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,NoDocto);
				ps.setString(2,NoIF);
				ps.setString(3,claveSirac);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				ps.close();
				if(numCols>0)
					throw new NafinException("CRED0009");*/

				qrySentencia =
					" select count(1) from com_solic_portal" +
					" where ig_numero_docto = ?" +
					" and ic_if = ?" +
					" and IG_CLAVE_SIRAC = ?" +
					" and IC_ESTATUS_SOLIC <>4" ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,NoDocto);
				ps.setString(2,NoIF);
				ps.setString(3,claveSirac);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				ps.close();
				if(numCols>0)
					throw new NafinException("CRED0009");

			}catch(NafinException ne){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto(NafinException) "+ne);
				throw ne;
			}catch(Exception e){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto Exception "+e);
				throw new NafinException("SIST0001");
			}finally{
				System.out.println("AutorizacionSolicitudBean::existeNoDocto (s)");
			}
	}


	private void existeNoDocto(String NoDocto,String NoIF,String claveSirac,AccesoDB con)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::existeNoDocto (E2)");
		PreparedStatement	ps 		= null;
		ResultSet			rs 		= null;
		String		qrySentencia	= "";
			try{
				int numCols = 0;
				/*
				qrySentencia =
					" select count(1) from com_solic_portal" +
					" where ig_numero_docto = ?" +
					" and ic_if = ?" +
					" and IG_CLAVE_SIRAC = ?"+
					" and trunc(df_carga) = trunc(sysdate)";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,NoDocto);
				ps.setString(2,NoIF);
				ps.setString(3,claveSirac);
				*/
				String fechaHoy = (new java.text.SimpleDateFormat("ddMMyyyy")).format(new java.util.Date());

				qrySentencia =
					" select count(1) from com_solic_portal" +
					" where cc_llave = ?";

				//System.out.println("AutorizacionSolicitudBean::existeNoDocto(query) " + qrySentencia);

				ps = con.queryPrecompilado(qrySentencia);

				ps.setString(1,NoIF + claveSirac + NoDocto + fechaHoy);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				ps.close();
				if(numCols>0)
					throw new NafinException("CRED0009");

			}catch(NafinException ne){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto(NafinException) "+ne);
				throw ne;
			}catch(Exception e){
				System.out.println("AutorizacionSolicitudBean::existeNoDocto(NafinException) "+e);
				throw new NafinException("SIST0001");
			}finally{
				System.out.println("AutorizacionSolicitudBean::existeNoDocto (S2)");
			}
	}

	public Vector getSolicitudes(String sNoSolicPortal) throws NafinException {
	AccesoDB con  = new AccesoDB();
	Vector vSolicitudes = new Vector();
	Vector vd = null;
		try{
			con.conexionDB();
			String query = " select P.cg_razon_social, SP.ig_clave_sirac, SP.ig_numero_docto, "+
							"TO_CHAR(SP.df_etc,'dd/mm/yyyy'), TO_CHAR(SP.df_v_descuento,'dd/mm/yyyy'), "+
							"M.cd_nombre, TC.cd_descripcion, SP.fn_importe_docto, "+
							"SP.fn_importe_dscto, SP.ic_solic_portal "+
							"from com_solic_portal SP, comcat_moneda M, comcat_tipo_credito TC, "+
							"comcat_pyme P "+
							"where SP.ic_solic_portal = "+sNoSolicPortal+
							"and M.ic_moneda = SP.ic_moneda "+
							"and TC.ic_tipo_credito = SP.ic_tipo_credito "+
							"and P.in_numero_sirac(+) = SP.ig_clave_sirac";
			ResultSet rs = con.queryDB(query);
			while (rs.next()) {
				vd = new Vector();
				vd.add((rs.getString(1)==null?"":rs.getString(1).trim()));
				vd.add((rs.getString(2)==null?"":rs.getString(2).trim()));
				vd.add((rs.getString(3)==null?"":rs.getString(3).trim()));
				vd.add((rs.getString(4)==null?"":rs.getString(4).trim()));
				vd.add((rs.getString(5)==null?"":rs.getString(5).trim()));
				vd.add((rs.getString(6)==null?"":rs.getString(6).trim()));
				vd.add((rs.getString(7)==null?"":rs.getString(7).trim()));
				vd.add((rs.getString(8)==null?"":rs.getString(8).trim()));
				vd.add((rs.getString(9)==null?"":rs.getString(9).trim()));
				vd.add((rs.getString(10)==null?"":rs.getString(10).trim()));
				vSolicitudes.addElement(vd);
			}
			con.cierraStatement();
		}
		catch(Exception e) {
			System.out.println("Exception en getSolicitudes(). "+e);
			throw new NafinException("DSCT0066");
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return vSolicitudes;
	}

	// Agregado por HDG, 27/10/2003, obtiene el tipo de piso que tiene el Intermediario Foda49.
	private int getTipoPisoIF(String sNoIF, AccesoDB con) throws NafinException {
	int iTipoPiso = 0;
	PreparedStatement ps = null;
		try {
			ps = con.queryPrecompilado("select ig_tipo_piso from comcat_if where ic_if = ?");
			ps.setInt(1, Integer.parseInt(sNoIF.trim()));
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				iTipoPiso = rs.getInt("ig_tipo_piso");
			ps.clearParameters();
			if(ps!=null) ps.close();
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getTipoPisoIF(). "+e.getMessage());
			throw new NafinException("SIST0001");
		}
	return iTipoPiso;
	}

	// Agregado por HDG, 20/10/2003, obtiene el plazo de la base de operaci�n de credito electronico, para aplicarlo a la solicitud Foda49.
	private Hashtable getPlazoBO(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, AccesoDB con) throws NafinException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	int plazoMax = 0;
	Hashtable hPlazoBO = new Hashtable();
	String query = "", sTomoPlazoGral = "N";
		try{
			query = " select nvl(max(boc.ig_plazo_maximo),0) as plazoMax "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ");
			try {
System.out.println("query: "+query);
System.out.println("iTipoPiso: "+iTipoPiso);
System.out.println("NoIF: "+NoIF);
System.out.println("TasaI: "+TasaI);
System.out.println("TipoCredito: "+TipoCredito);
System.out.println("Amortizacion: "+Amortizacion);
System.out.println("TipoPlazo: "+TipoPlazo);
System.out.println("Emisor: "+Emisor);
				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));
				ps.setInt(3, Integer.parseInt(TasaI.trim()));
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));
				ps.setObject(6, TipoPlazo);
				if(!Emisor.equals("")) ps.setInt(7, Integer.parseInt(Emisor.trim()));
				rs = ps.executeQuery();
				if(rs.next())
					plazoMax = rs.getInt("plazoMax");
				ps.clearParameters();

				// En caso de ser Factoraje y que no tenga parametrizada un B.O. se toma el plazo generico de la moneda.
				if(TipoPlazo.equals("F") && plazoMax==0 && iTipoPiso==2) {
					query = "select decode(?, 1, ig_plazo_maximo_factoraje, 54, ig_plazo_maximo_factoraje_dl) as plazoMax "+
							"from comcat_producto_nafin "+
							"where ic_producto_nafin = 1 ";

System.out.println("query: "+query);
System.out.println("Moneda: "+Moneda);
					ps = con.queryPrecompilado(query);
					ps.setInt(1, Integer.parseInt(Moneda.trim()));
					rs = ps.executeQuery();
					if(rs.next()) {
						plazoMax = rs.getInt("plazoMax");
						sTomoPlazoGral = "S";
					}
					ps.clearParameters();
				}

				if(ps!=null) ps.close();
			} catch(SQLException sqle) {
				throw new NafinException("DSCT0085");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getPlazoBO(). "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			hPlazoBO.put("plazoMax", new Integer(plazoMax));
			hPlazoBO.put("sTomoPlazoGral", sTomoPlazoGral);
		}
	return hPlazoBO;
	}

	// Agregado por MPCS 08/10/2004, para Equipamiento Electronico. Solo afecta las cargas individuales
	private Hashtable getPlazoBO(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, AccesoDB con, String sProducto, String iPeriodicidad, String iTipoRenta ) throws NafinException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	int plazoMax = 0;
	Hashtable hPlazoBO = new Hashtable();
	String 	query = "", sTomoPlazoGral = "N";
	boolean 	codigoBaseOpDinamicaHabilitado = false;
	boolean	plazoEncontrado = false;
	int 		idx 				 = 0;
		try{

			/*
				query =
						" select nvl(max(boc.ig_plazo_maximo),0) as plazoMax "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ")+
						(sProducto.equals("")?" and boc.ic_producto_nafin is null ":"	and boc.ic_producto_nafin = ? ");
			*/
				query =
						" select nvl(boc.ig_plazo_maximo,0) as plazoMax, boc.cs_base_op_dinamica as cs_base_op_dinamica "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ")+
						(sProducto.equals("")?" and boc.ic_producto_nafin is null ":"	and boc.ic_producto_nafin = ? ")+// F014-2012 JSHD. YA NO SE USA
						(iPeriodicidad.equals("")?" and boc.ic_periodicidad is null ":" and boc.ic_periodicidad  = ? ")+//Se agrega condicion por F014-2012 JSHD
						(iTipoRenta.equals("")?   " and boc.cs_tipo_renta   is null ":" and boc.cs_tipo_renta    = ? ")+//Se agrega condicion por F014-2012 JSHD
						" order by plazoMax desc, boc.ic_base_op_credito desc ";
			try {
				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));
				ps.setInt(3, Integer.parseInt(TasaI.trim()));
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));
				ps.setObject(6, TipoPlazo);
				idx = 7;
				if(!Emisor.equals("")) 			ps.setInt(idx++, 		Integer.parseInt(Emisor.trim()));
				if(!sProducto.equals("")) 		ps.setInt(idx++, 		Integer.parseInt(sProducto.trim()));
				if(!iPeriodicidad.equals("")) ps.setInt(idx++, 		Integer.parseInt(iPeriodicidad.trim()));
				if(!iTipoRenta.equals("")) 	ps.setString(idx++, 	iTipoRenta.trim());
				rs = ps.executeQuery();
				if(rs.next()){
					plazoMax 								= rs.getInt("plazoMax");
					codigoBaseOpDinamicaHabilitado 	= "S".equals(rs.getString("cs_base_op_dinamica"))?true:false;
					plazoEncontrado 						= true;
				} else {
					plazoMax 								= 0;
					codigoBaseOpDinamicaHabilitado	= false;
					plazoEncontrado 						= false;
				}
				ps.clearParameters();
				
				if(!plazoEncontrado){
					query = 
						" select nvl(boc.ig_plazo_maximo,0) as plazoMax, boc.cs_base_op_dinamica as cs_base_op_dinamica "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ")+
						(sProducto.equals("")?" and boc.ic_producto_nafin is null ":"	and boc.ic_producto_nafin = ? ")+// F014-2012 JSHD. YA NO SE USA
						" and boc.ic_periodicidad is null "+//Se agrega condicion por F014-2012 JSHD
						" and boc.cs_tipo_renta   is null "+//Se agrega condicion por F014-2012 JSHD
						" order by plazoMax desc, boc.ic_base_op_credito desc ";

					ps = con.queryPrecompilado(query);
					ps.setInt(1, iTipoPiso);
					ps.setInt(2, Integer.parseInt(NoIF.trim()));
					ps.setInt(3, Integer.parseInt(TasaI.trim()));
					ps.setInt(4, Integer.parseInt(TipoCredito.trim()));
					ps.setInt(5, Integer.parseInt(Amortizacion.trim()));
					ps.setObject(6, TipoPlazo);
					idx = 7;
					if(!Emisor.equals("")) 			ps.setInt(idx++, 		Integer.parseInt(Emisor.trim()));
					if(!sProducto.equals("")) 		ps.setInt(idx++, 		Integer.parseInt(sProducto.trim()));
					rs = ps.executeQuery();
					if(rs.next()){
						plazoMax 								= rs.getInt("plazoMax");
						codigoBaseOpDinamicaHabilitado 	= "S".equals(rs.getString("cs_base_op_dinamica"))?true:false;
						plazoEncontrado 						= true;
					} else {
						plazoMax 								= 0;
						codigoBaseOpDinamicaHabilitado	= false;
						plazoEncontrado 						= false;
					}
					ps.clearParameters();
					
				}

				// En caso de ser Factoraje y que no tenga parametrizada un B.O. se toma el plazo generico de la moneda.
				if(TipoPlazo.equals("F") && plazoMax==0 && iTipoPiso==2) {

					rs.close();
					ps.close();

					query = "select decode(?, 1, ig_plazo_maximo_factoraje, 54, ig_plazo_maximo_factoraje_dl) as plazoMax "+
							"from comcat_producto_nafin "+
							"where ic_producto_nafin = 1 ";
					ps = con.queryPrecompilado(query);
					ps.setInt(1, Integer.parseInt(Moneda.trim()));
					rs = ps.executeQuery();
					if(rs.next()) {
						plazoMax = rs.getInt("plazoMax");
						sTomoPlazoGral = "S";
					}
					ps.clearParameters();
				}

			} catch(SQLException sqle) {
				throw new NafinException("DSCT0085");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getPlazoBO(). "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {

			if( rs != null){ try { rs.close(); }catch(Exception e){} }
			if( ps != null){ try { ps.close(); }catch(Exception e){} }

			hPlazoBO.put("plazoMax", 								new Integer(plazoMax)							);
			hPlazoBO.put("sTomoPlazoGral", 						sTomoPlazoGral										);
			hPlazoBO.put("codigoBaseOpDinamicaHabilitado", 	new Boolean(codigoBaseOpDinamicaHabilitado) );

			System.out.println("CargaCreditoEJB::getPlazoBO (S)");

		}

		return hPlazoBO;

	}

	/**
	 * Realiza la generaci�n del archivo de origen del producto Cr�dito Electronico
	 * x Solicitud
	 * @param sNoSolicitud N�mero de Solicitud
	 * @return Archivo origen
	 *
	 * Creado por HDG 14/04/2005, foda 26.
	 */

	public String getDatosSolicitudCredito(String sNoSolicitud) throws NafinException {

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (sNoSolicitud == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" sNoSolicitud=" + sNoSolicitud);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			return getDatosSolicitudesCredito(sNoSolicitud, "SOLICITUD");
		} catch(SQLException sqle) {
			throw new NafinException("CRED0010");
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		}
	}

	/**
	 * Realiza la generaci�n del archivo de origen del producto Cr�dito Electronico
	 * x Acuse
	 *
	 * @param acuse Clave de Acuse
	 * @return Archivo origen
	 *
	 */

	public String getDatosSolicitudCreditoxAcuse(String acuse)
			throws NafinException {

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (acuse == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" acuse=" + acuse);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		try {
			return getDatosSolicitudesCredito(acuse, "ACUSE");
		} catch(SQLException sqle) {
			System.out.println("getDatosSolicitudCreditoxAcuse Error:: " +
					sqle.getMessage());
			throw new NafinException("CRED0010");
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		}
	}

/*	AccesoDB con = new AccesoDB();
	PreparedStatement ps;
	ResultSet rs;
	String sContenidoArchivo = new String();
		try {
			con.conexionDB();
			String sQuery = "select ig_clave_sirac, ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto, "+
							"ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i, cg_bienes_servicios, "+
							"ic_clase_docto, cg_domicilio_pago, ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, in_numero_amort, "+
							"ic_tabla_amort, to_char(df_ppc,'dd/mm/yyyy') as df_ppc, to_char(df_ppi,'dd/mm/yyyy') as df_ppi, in_dia_pago, "+
							"to_char(df_v_documento,'dd/mm/yyyy') as df_v_documento, to_char(df_v_descuento,'dd/mm/yyyy') as df_v_descuento, "+
							"to_char(df_etc,'dd/mm/yyyy') as df_etc, cg_lugar_firma, cs_amort_ajuste, cs_tipo_renta, fg_importe_ajuste "+
							"from com_solic_portal "+
							"where ic_solic_portal = ? ";
			ps = con.queryPrecompilado(sQuery);
			ps.setString(1, sNoSolicitud);
			try {
				rs = ps.executeQuery();
				if(rs.next()) {
					StringBuffer sbEncabezado = new StringBuffer("Clave SIRAC,Sucursal,Clave Moneda,N�mero de documento,Importe documento,Importe descuento,Clave Emisor,Clave Tipo de Cr�dito,Clave Periodicidad pago capital,Clave Periodicidad pago Inter�s,Descripci�n de Bienes y Servicios,Clave Clase de documento,Domocilio pago,Clave Tasa de inter�s usuario final,Relaci�n matem�tica usuario final,Sobretasa usuario final,Clave Tasa de inter�s intermediario,N�mero de Amortizaciones,Clave Tabla amortizaci�n,Fecha primer pago capital,Fecha primer pago de interes,D�a de pago,Fecha vencimiento documento,Fecha vencimiento descuento,Fecha emisi�n t�tulo de cr�dito,Lugar firma,Amortizaci�n Ajuste,Tipo Renta,Importe Ajuste");
					StringBuffer sbDatosSolicitud = new StringBuffer();
					sbDatosSolicitud.append((rs.getString("ig_clave_sirac")==null?"":rs.getString("ig_clave_sirac")).concat(","));
					sbDatosSolicitud.append((rs.getString("ig_sucursal")==null?"":rs.getString("ig_sucursal")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda")).concat(","));
					sbDatosSolicitud.append((rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto")).concat(","));
					sbDatosSolicitud.append((rs.getString("fn_importe_docto")==null?"":rs.getString("fn_importe_docto")).concat(","));
					sbDatosSolicitud.append((rs.getString("fn_importe_dscto")==null?"":rs.getString("fn_importe_dscto")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_emisor")==null?"":rs.getString("ic_emisor")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_tipo_credito")==null?"":rs.getString("ic_tipo_credito")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_periodicidad_c")==null?"":rs.getString("ic_periodicidad_c")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_periodicidad_i")==null?"":rs.getString("ic_periodicidad_i")).concat(","));
					sbDatosSolicitud.append((rs.getString("cg_bienes_servicios")==null?"":rs.getString("cg_bienes_servicios").replace(',',' ')).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_clase_docto")==null?"":rs.getString("ic_clase_docto")).concat(","));
					sbDatosSolicitud.append((rs.getString("cg_domicilio_pago")==null?"":rs.getString("cg_domicilio_pago").replace(',',' ')).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_tasauf")==null?"":rs.getString("ic_tasauf")).concat(","));
					sbDatosSolicitud.append((rs.getString("cg_rmuf")==null?"":rs.getString("cg_rmuf")).concat(","));
					sbDatosSolicitud.append((rs.getString("fg_stuf")==null?"":rs.getString("fg_stuf")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_tasaif")==null?"":rs.getString("ic_tasaif")).concat(","));
					sbDatosSolicitud.append((rs.getString("in_numero_amort")==null?"":rs.getString("in_numero_amort")).concat(","));
					sbDatosSolicitud.append((rs.getString("ic_tabla_amort")==null?"":rs.getString("ic_tabla_amort")).concat(","));
					sbDatosSolicitud.append((rs.getString("df_ppc")==null?"":rs.getString("df_ppc")).concat(","));
					sbDatosSolicitud.append((rs.getString("df_ppi")==null?"":rs.getString("df_ppi")).concat(","));
					sbDatosSolicitud.append((rs.getString("in_dia_pago")==null?"":rs.getString("in_dia_pago")).concat(","));
					sbDatosSolicitud.append((rs.getString("df_v_documento")==null?"":rs.getString("df_v_documento")).concat(","));
					sbDatosSolicitud.append((rs.getString("df_v_descuento")==null?"":rs.getString("df_v_descuento")).concat(","));
					sbDatosSolicitud.append((rs.getString("df_etc")==null?"":rs.getString("df_etc")).concat(","));
					sbDatosSolicitud.append((rs.getString("cg_lugar_firma")==null?"":rs.getString("cg_lugar_firma").replace(',',' ')).concat(","));
					sbDatosSolicitud.append((rs.getString("cs_amort_ajuste")==null?"":rs.getString("cs_amort_ajuste")).concat(","));
					sbDatosSolicitud.append((rs.getString("cs_tipo_renta")==null?"":rs.getString("cs_tipo_renta")).concat(","));
					sbDatosSolicitud.append((rs.getString("fg_importe_ajuste")==null?"":rs.getString("fg_importe_ajuste")));

					ps.clearParameters();
					// Cuando el Tipo de amortizaci�n sea = 5 y el Tipo de Renta = N, se desglosar�n cada una de las amortizaciones.
					String sTipoRenta = (rs.getString("cs_tipo_renta")==null?"":rs.getString("cs_tipo_renta"));
					if(rs.getInt("ic_tabla_amort")==5 && sTipoRenta.equals("N")) {
						sQuery ="select ic_amort_portal, cg_tipo_amort, fn_monto, "+
								"to_char(df_vencimiento, 'dd/mm/yyyy') as df_vencimiento "+
								"from com_amort_portal "+
								"where ic_solic_portal = ? "+
								"order by ic_amort_portal ";
						ps = con.queryPrecompilado(sQuery);
						ps.setString(1, sNoSolicitud);
						rs = ps.executeQuery();
						while(rs.next()) {
							sbEncabezado.append(",Amortizaci�n n�mero,Amortizaci�n importe,Amortizaci�n fecha,Tipo de Amortizaci�n");
							sbDatosSolicitud.append(","+(rs.getString("ic_amort_portal")==null?"":rs.getString("ic_amort_portal")));
							sbDatosSolicitud.append(","+(rs.getString("fn_monto")==null?"":rs.getString("fn_monto")));
							sbDatosSolicitud.append(","+(rs.getString("df_vencimiento")==null?"":rs.getString("df_vencimiento")));
							sbDatosSolicitud.append(","+(rs.getString("cg_tipo_amort")==null?"":rs.getString("cg_tipo_amort")));
						}
						rs.close();
					}
					sbEncabezado.append("\r\n");
					sbDatosSolicitud.append("\r\n");

					sContenidoArchivo = sbEncabezado.toString().concat(sbDatosSolicitud.toString());
				}
				if(ps!=null) ps.close();

			} catch(SQLException sqle) {
				throw new NafinException("CRED0010");
			}

		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getTipoPisoIF(). "+e.getMessage());
			throw new NafinException("SIST0001");
		}
	return sContenidoArchivo;
	}
*/



	/**
	 * Obtiene los datos de las solicitudes de com_solic_portal
	 * @param clave Clave de la solicitud o del acuse
	 * @param tipoClave Tipo de clave: SOLICITUD � ACUSE segun se desee
	 * @return Cadena con el contenido de la informaci�n resultante.
	 */
	private String getDatosSolicitudesCredito(String clave, String tipoClave)
			throws SQLException, NamingException {
		AccesoDB con = new AccesoDB();
		String condicion = "";
		if (tipoClave.equals("SOLICITUD")) {
			condicion = " ic_solic_portal = ? ";
		} else if (tipoClave.equals("ACUSE")) {
			condicion = " cc_acuse = ? ";
		}

		String sContenidoArchivo = new String();
		try {
			con.conexionDB();


			String sQuery = "select ig_clave_sirac, ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto, "+
							"ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i, cg_bienes_servicios, "+
							"ic_clase_docto, cg_domicilio_pago, ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, in_numero_amort, "+
							"ic_tabla_amort, to_char(df_ppc,'dd/mm/yyyy') as df_ppc, to_char(df_ppi,'dd/mm/yyyy') as df_ppi, in_dia_pago, "+
							"to_char(df_v_documento,'dd/mm/yyyy') as df_v_documento, to_char(df_v_descuento,'dd/mm/yyyy') as df_v_descuento, "+
							"to_char(df_etc,'dd/mm/yyyy') as df_etc, cg_lugar_firma, cs_amort_ajuste, cs_tipo_renta, fg_importe_ajuste, cg_destino_credito, cs_tipo_plazo, "+
							"ic_solic_portal " +
							"from com_solic_portal "+
							"where " + condicion;
			PreparedStatement psSolicitudes = con.queryPrecompilado(sQuery);
			psSolicitudes.setString(1, clave);

			ResultSet rsSolicitudes = psSolicitudes.executeQuery();
			StringBuffer sbEncabezado = new StringBuffer("");
			StringBuffer sbDatosSolicitud = new StringBuffer();

			String tipoPlazo = "";
			int numCiclo = 0;
			while(rsSolicitudes.next()) {
				tipoPlazo = (rsSolicitudes.getString("cs_tipo_plazo")==null?"":rsSolicitudes.getString("cs_tipo_plazo"));
				if(numCiclo==0){
					sbEncabezado.append("Clave SIRAC,Sucursal," +
							"Clave Moneda,N�mero de documento,Importe documento," +
							"Importe descuento,Clave Emisor,Clave Tipo de Cr�dito," +
							"Clave Periodicidad pago capital," +
							"Clave Periodicidad pago Inter�s," +
							"Descripci�n de Bienes y Servicios"); 
					if("C".equals(tipoPlazo)){
						sbEncabezado.append(",Destino del Cr�dito");
					}
					sbEncabezado.append(",Clave Clase de documento,"+
							"Domicilio pago,Clave Tasa de inter�s usuario final," +
							"Relaci�n matem�tica usuario final,Sobretasa usuario final," +
							"Clave Tasa de inter�s intermediario,N�mero de Amortizaciones," +
							"Clave Tabla amortizaci�n,Fecha primer pago capital," +
							"Fecha primer pago de interes,D�a de pago," +
							"Fecha vencimiento documento,Fecha vencimiento descuento,"+
							"Fecha emisi�n t�tulo de cr�dito,Lugar firma,"+
							"Amortizaci�n Ajuste,Tipo Renta,Importe Ajuste");
					if (tipoClave.equals("ACUSE")) {
						sbEncabezado.append(",Amortizaciones");
					}
					
				}
				
				String ic_solic_portal = rsSolicitudes.getString("ic_solic_portal");
				sbDatosSolicitud.append((rsSolicitudes.getString("ig_clave_sirac")==null?"":rsSolicitudes.getString("ig_clave_sirac")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ig_sucursal")==null?"":rsSolicitudes.getString("ig_sucursal")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_moneda")==null?"":rsSolicitudes.getString("ic_moneda")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ig_numero_docto")==null?"":rsSolicitudes.getString("ig_numero_docto")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("fn_importe_docto")==null?"":rsSolicitudes.getString("fn_importe_docto")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("fn_importe_dscto")==null?"":rsSolicitudes.getString("fn_importe_dscto")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_emisor")==null?"":rsSolicitudes.getString("ic_emisor")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_tipo_credito")==null?"":rsSolicitudes.getString("ic_tipo_credito")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_periodicidad_c")==null?"":rsSolicitudes.getString("ic_periodicidad_c")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_periodicidad_i")==null?"":rsSolicitudes.getString("ic_periodicidad_i")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cg_bienes_servicios")==null?"":rsSolicitudes.getString("cg_bienes_servicios").replace(',',' ')).concat(","));
				if("C".equals(tipoPlazo)){
					sbDatosSolicitud.append((rsSolicitudes.getString("cg_destino_credito")==null?"":rsSolicitudes.getString("cg_destino_credito")).concat(","));
				}
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_clase_docto")==null?"":rsSolicitudes.getString("ic_clase_docto")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cg_domicilio_pago")==null?"":rsSolicitudes.getString("cg_domicilio_pago").replace(',',' ')).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_tasauf")==null?"":rsSolicitudes.getString("ic_tasauf")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cg_rmuf")==null?"":rsSolicitudes.getString("cg_rmuf")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("fg_stuf")==null?"":rsSolicitudes.getString("fg_stuf")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_tasaif")==null?"":rsSolicitudes.getString("ic_tasaif")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("in_numero_amort")==null?"":rsSolicitudes.getString("in_numero_amort")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("ic_tabla_amort")==null?"":rsSolicitudes.getString("ic_tabla_amort")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("df_ppc")==null?"":rsSolicitudes.getString("df_ppc")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("df_ppi")==null?"":rsSolicitudes.getString("df_ppi")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("in_dia_pago")==null?"":rsSolicitudes.getString("in_dia_pago")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("df_v_documento")==null?"":rsSolicitudes.getString("df_v_documento")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("df_v_descuento")==null?"":rsSolicitudes.getString("df_v_descuento")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("df_etc")==null?"":rsSolicitudes.getString("df_etc")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cg_lugar_firma")==null?"":rsSolicitudes.getString("cg_lugar_firma").replace(',',' ')).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cs_amort_ajuste")==null?"":rsSolicitudes.getString("cs_amort_ajuste")).concat(","));
				sbDatosSolicitud.append((rsSolicitudes.getString("cs_tipo_renta")==null?"":rsSolicitudes.getString("cs_tipo_renta")).concat(","));
				

				// Cuando el Tipo de amortizaci�n sea = 5 y el Tipo de Renta = N, se desglosar�n cada una de las amortizaciones.
				String sTipoRenta = (rsSolicitudes.getString("cs_tipo_renta")==null?"":rsSolicitudes.getString("cs_tipo_renta"));
				if(rsSolicitudes.getInt("ic_tabla_amort")==5 && sTipoRenta.equals("N")) {
					sQuery ="select ic_amort_portal, cg_tipo_amort, fn_monto, "+
							"to_char(df_vencimiento, 'dd/mm/yyyy') as df_vencimiento "+
							"from com_amort_portal "+
							"where ic_solic_portal = ? "+
							"order by ic_amort_portal ";
					PreparedStatement psAmortizaciones = con.queryPrecompilado(sQuery);
					psAmortizaciones.setString(1, ic_solic_portal);

					ResultSet rsAmortizaciones = psAmortizaciones.executeQuery();
					while(rsAmortizaciones.next()) {
						if (tipoClave.equals("SOLICITUD")) {
							sbEncabezado.append(",Amortizaci�n n�mero,Amortizaci�n importe,Amortizaci�n fecha,Tipo de Amortizaci�n");
						}
						sbDatosSolicitud.append(","+(rsAmortizaciones.getString("ic_amort_portal")==null?"":rsAmortizaciones.getString("ic_amort_portal")));
						sbDatosSolicitud.append(","+(rsAmortizaciones.getString("fn_monto")==null?"":rsAmortizaciones.getString("fn_monto")));
						sbDatosSolicitud.append(","+(rsAmortizaciones.getString("df_vencimiento")==null?"":rsAmortizaciones.getString("df_vencimiento")));
						sbDatosSolicitud.append(","+(rsAmortizaciones.getString("cg_tipo_amort")==null?"":rsAmortizaciones.getString("cg_tipo_amort")));
					}
					rsAmortizaciones.close();
					psAmortizaciones.close();
				}
				sbDatosSolicitud.append("\r\n");
			}
			rsSolicitudes.close();
			psSolicitudes.close();

			sbEncabezado.append("\r\n");
			sContenidoArchivo = sbEncabezado.toString().concat(sbDatosSolicitud.toString());


			return sContenidoArchivo;
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/*************************************************************************
	*	Vector sGetDatosBO()
	*  	Obtiene los datos de las bases de operaci�n
	**************************************************************************/
    //Agregado 28/10/2005		--MRZL
    private Vector sGetDatosBO(String NoIF,int TipoPiso,String TipoPlazo,AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        ResultSet lrsSel 	= null;
        Vector vecFilas		= new Vector();
        Vector vecColumnas	= null;
        int registros		= 0;

		lsSQL.append(" select distinct 'null' as icEmisor, 'SIN EMISOR' as nomEmisor ");
        lsSQL.append(" ,bo.ic_tipo_credito as icTipoCred, tc.cd_descripcion as nomTipoCred ");
        lsSQL.append(" ,bo.ic_tasa as icTasaI, ta.cd_nombre as nomTasaI ");
        lsSQL.append(" ,bo.ic_tabla_amort as icTablaAmort, tam.cd_descripcion as nomTablaAmort ");
		lsSQL.append(" from com_base_op_credito bo,  ");
		lsSQL.append(" comcat_tasa ta,  ");
		lsSQL.append(" comcat_tipo_credito tc,  ");
		lsSQL.append(" comcat_tabla_amort tam ");
		lsSQL.append(" where bo.ic_tasa = ta.ic_tasa  ");
		lsSQL.append(" and bo.ic_tipo_credito = tc.ic_tipo_credito  ");
		lsSQL.append(" and bo.ic_tabla_amort = tam.ic_tabla_amort  ");
		lsSQL.append(" and ta.cs_creditoelec ='S' ");
		lsSQL.append(" and tc.cs_creditoelec ='S' ");
		lsSQL.append(" and bo.ic_emisor is null ");
		lsSQL.append(" and bo.ic_if = "+NoIF);
		lsSQL.append(" and bo.ig_tipo_cartera = "+TipoPiso);
		lsSQL.append(" and bo.cs_tipo_plazo = '"+TipoPlazo+"'");

		System.out.println(lsSQL.toString());
		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		while (lrsSel.next()){
			vecColumnas = new Vector();
			vecColumnas.addElement(lrsSel.getString("icEmisor"));
			vecColumnas.addElement(lrsSel.getString("nomEmisor"));
			vecColumnas.addElement(lrsSel.getString("icTipoCred"));
			vecColumnas.addElement(lrsSel.getString("nomTipoCred"));
			vecColumnas.addElement(lrsSel.getString("icTasaI"));
			vecColumnas.addElement(lrsSel.getString("nomTasaI"));
			vecColumnas.addElement(lrsSel.getString("icTablaAmort"));
			vecColumnas.addElement(lrsSel.getString("nomTablaAmort"));

			vecFilas.addElement(vecColumnas);
			registros++;
        }

		lsSQL.delete(0,lsSQL.length());
		lsSQL.append(" select distinct bo.ic_emisor as icEmisor, e.cd_descripcion as nomEmisor ");
        lsSQL.append(" ,bo.ic_tipo_credito as icTipoCred, tc.cd_descripcion as nomTipoCred ");
        lsSQL.append(" ,bo.ic_tasa as icTasaI, ta.cd_nombre as nomTasaI ");
        lsSQL.append(" ,bo.ic_tabla_amort as icTablaAmort, tam.cd_descripcion as nomTablaAmort ");
		lsSQL.append(" from com_base_op_credito bo,  ");
		lsSQL.append(" comcat_emisor e,  ");
		lsSQL.append(" comcat_tasa ta,  ");
		lsSQL.append(" comcat_tipo_credito tc,  ");
		lsSQL.append(" comcat_tabla_amort tam ");
		lsSQL.append(" where bo.ic_emisor = e.ic_emisor ");
		lsSQL.append(" and bo.ic_tasa = ta.ic_tasa  ");
		lsSQL.append(" and bo.ic_tipo_credito = tc.ic_tipo_credito  ");
		lsSQL.append(" and bo.ic_tabla_amort = tam.ic_tabla_amort  ");
		lsSQL.append(" and ta.cs_creditoelec ='S' ");
		lsSQL.append(" and tc.cs_creditoelec ='S' ");
		lsSQL.append(" and bo.ic_if = "+NoIF);
		lsSQL.append(" and bo.ig_tipo_cartera = "+TipoPiso);
		lsSQL.append(" and bo.cs_tipo_plazo = '"+TipoPlazo+"'");

		System.out.println(lsSQL.toString());
		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		while (lrsSel.next()){
			vecColumnas = new Vector();
			vecColumnas.addElement(lrsSel.getString("icEmisor"));
			vecColumnas.addElement(lrsSel.getString("nomEmisor"));
			vecColumnas.addElement(lrsSel.getString("icTipoCred"));
			vecColumnas.addElement(lrsSel.getString("nomTipoCred"));
			vecColumnas.addElement(lrsSel.getString("icTasaI"));
			vecColumnas.addElement(lrsSel.getString("nomTasaI"));
			vecColumnas.addElement(lrsSel.getString("icTablaAmort"));
			vecColumnas.addElement(lrsSel.getString("nomTablaAmort"));

			vecFilas.addElement(vecColumnas);
			registros++;
        }
        if (registros == 0){
			throw new Exception("No existen registros parametrizados en Bases de Operaci�n para el Tipo de Plazo elegido");
        }
		eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return vecFilas;
    }

    /*************************************************************************
	*	Vector sGetDatosBO()
	*  	Obtiene los datos de las bases de operaci�n
	**************************************************************************/
    //Agregado 28/10/2005		--MRZL
    public Vector sGetDatosBO(String NoIF,String TipoPiso)
    	throws NafinException
    {
    	AccesoDB eobdConexion = new AccesoDB();
    	StringBuffer lsSQL 	= new StringBuffer();
        ResultSet lrsSel 	= null;
        Vector vecFilas		= new Vector();
        Vector vecColumnas	= null;

        try {
			eobdConexion.conexionDB();
			System.out.println(" AutorizacionSolicitudEJB::sGetDatosBO(E)");

			lsSQL.append(" select decode(bo.cs_tipo_plazo,'C','Cr�dito','F','Factoraje') as TipoPlazo ");
	      lsSQL.append(" ,bo.ic_tipo_credito||'-'||tc.cd_descripcion as TipoCred ");
	      lsSQL.append(" ,'(vacio)-SIN EMISOR' as Emisor ");
	      lsSQL.append(" ,bo.ic_tasa||'-'||ta.cd_nombre as Tasa ");
	      lsSQL.append(" ,bo.ic_tabla_amort||'-'||tam.cd_descripcion as TablaAmort ");
	      lsSQL.append(" ,bo.ig_plazo_minimo as PMin ");
	      lsSQL.append(" ,bo.ig_plazo_maximo as PMax ");
			lsSQL.append(" ,nvl(cp.cd_descripcion,'-') as nomPeriodicidad ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" ,DECODE(bo.cg_tipo_interes,'S','Simple','C','Compuesto', '-' ) as tipoInteres ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" ,decode(bo.cs_tipo_renta,'S','Si', 'N','No','N/A') as tipoRenta ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" from com_base_op_credito bo,  ");
			lsSQL.append(" comcat_tasa ta,  ");
			lsSQL.append(" comcat_tipo_credito tc,  ");
			lsSQL.append(" comcat_tabla_amort tam, ");
			lsSQL.append(" comcat_periodicidad cp ");// se agrega tabla para mostrar periodicidad F020-2011 FVR
			lsSQL.append(" where bo.ic_tasa = ta.ic_tasa  ");
			lsSQL.append(" and bo.ic_tipo_credito = tc.ic_tipo_credito  ");
			lsSQL.append(" and bo.ic_tabla_amort = tam.ic_tabla_amort  ");
			lsSQL.append(" and bo.ic_periodicidad = CP.ic_periodicidad(+) ");//se agrega join para mostrar periodicidad F020-2011 FVR
			lsSQL.append(" and ta.cs_creditoelec ='S' ");
			lsSQL.append(" and tc.cs_creditoelec ='S' ");
			lsSQL.append(" and bo.ic_emisor is null ");
			lsSQL.append(" and bo.ic_if = "+NoIF);
			lsSQL.append(" and bo.ig_tipo_cartera = "+TipoPiso);
			lsSQL.append(" and bo.cs_tipo_plazo in ('F','C') ");
			lsSQL.append(" order by 1,2,3,4,5,6 ");

			System.out.println(lsSQL.toString());
			lrsSel = eobdConexion.queryDB(lsSQL.toString());
			while(lrsSel.next()){
				vecColumnas = new Vector();
				vecColumnas.addElement(lrsSel.getString("TipoPlazo"));
				vecColumnas.addElement(lrsSel.getString("TipoCred"));
				vecColumnas.addElement(lrsSel.getString("Emisor"));
				vecColumnas.addElement(lrsSel.getString("Tasa"));
				vecColumnas.addElement(lrsSel.getString("TablaAmort"));
				vecColumnas.addElement(lrsSel.getString("PMin"));
				vecColumnas.addElement(lrsSel.getString("PMax"));
				vecColumnas.addElement(lrsSel.getString("nomPeriodicidad"));
				vecColumnas.addElement(lrsSel.getString("tipoInteres"));
				vecColumnas.addElement(lrsSel.getString("tipoRenta"));

				vecFilas.addElement(vecColumnas);
	        }

			lsSQL.delete(0,lsSQL.length());
			lsSQL.append(" select decode(bo.cs_tipo_plazo,'C','Cr�dito','F','Factoraje') as TipoPlazo ");
			lsSQL.append(" ,bo.ic_tipo_credito||'-'||tc.cd_descripcion as TipoCred ");
			lsSQL.append(" ,bo.ic_emisor||'-'||e.cd_descripcion as Emisor ");
			lsSQL.append(" ,bo.ic_tasa||'-'||ta.cd_nombre as Tasa ");
			lsSQL.append(" ,bo.ic_tabla_amort||'-'||tam.cd_descripcion as TablaAmort ");
			lsSQL.append(" ,bo.ig_plazo_minimo as PMin ");
			lsSQL.append(" ,bo.ig_plazo_maximo as PMax ");
			lsSQL.append(" ,nvl(cp.cd_descripcion,'-') as nomPeriodicidad ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" ,DECODE(bo.cg_tipo_interes,'S','Simple','C','Compuesto','-' ) as tipoInteres ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" ,decode(bo.cs_tipo_renta,'S','Si', 'N','No','N/A') as tipoRenta ");//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
			lsSQL.append(" from com_base_op_credito bo,  ");
			lsSQL.append(" comcat_emisor e,  ");
			lsSQL.append(" comcat_tasa ta,  ");
			lsSQL.append(" comcat_tipo_credito tc,  ");
			lsSQL.append(" comcat_tabla_amort tam, ");
			lsSQL.append(" comcat_periodicidad cp ");// se agrega tabla para mostrar periodicidad F020-2011 FVR
			lsSQL.append(" where bo.ic_emisor = e.ic_emisor ");
			lsSQL.append(" and bo.ic_tasa = ta.ic_tasa  ");
			lsSQL.append(" and bo.ic_tipo_credito = tc.ic_tipo_credito  ");
			lsSQL.append(" and bo.ic_tabla_amort = tam.ic_tabla_amort  ");
			lsSQL.append(" and bo.ic_periodicidad = CP.ic_periodicidad(+) ");//se agrega join para mostrar periodicidad F020-2011 FVR
			lsSQL.append(" and ta.cs_creditoelec ='S' ");
			lsSQL.append(" and tc.cs_creditoelec ='S' ");
			lsSQL.append(" and bo.ic_if = "+NoIF);
			lsSQL.append(" and bo.ig_tipo_cartera = "+TipoPiso);
			lsSQL.append(" and bo.cs_tipo_plazo in ('F','C') ");
			lsSQL.append(" order by 1,2,3,4,5,6 ");

			System.out.println(lsSQL.toString());
			lrsSel = eobdConexion.queryDB(lsSQL.toString());
			while (lrsSel.next()){
				vecColumnas = new Vector();
				vecColumnas.addElement(lrsSel.getString("TipoPlazo"));
				vecColumnas.addElement(lrsSel.getString("TipoCred"));
				vecColumnas.addElement(lrsSel.getString("Emisor"));
				vecColumnas.addElement(lrsSel.getString("Tasa"));
				vecColumnas.addElement(lrsSel.getString("TablaAmort"));
				vecColumnas.addElement(lrsSel.getString("PMin"));
				vecColumnas.addElement(lrsSel.getString("PMax"));
				vecColumnas.addElement(lrsSel.getString("nomPeriodicidad"));
				vecColumnas.addElement(lrsSel.getString("tipoInteres"));
				vecColumnas.addElement(lrsSel.getString("tipoRenta"));

				vecFilas.addElement(vecColumnas);
	        }
			eobdConexion.cierraStatement();
	        lrsSel.close();
	        lsSQL = null;
	    } catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			epError.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			eobdConexion.terminaTransaccion(true);
			if (eobdConexion.hayConexionAbierta()) {
				eobdConexion.cierraConexionDB();
			}
			System.out.println(" AutorizacionSolicitudEJB::sGetDatosBO(S)");
		}
	    return vecFilas;
    }

	/*************************************************************************
	*	String sGetPeriodicidad()
	*  	Obtiene las claves de periodicidad v�lidos
	**************************************************************************/
    //Agregado 28/10/2005		--MRZL
    private String sGetPeriodicidad(AccesoDB eobdConexion)
    	throws Exception
    {
    	StringBuffer lsSQL 	= new StringBuffer();
        ResultSet lrsSel 	= null;
        String sPeriod		= "";

		lsSQL.append(" select ic_periodicidad as icP,cd_descripcion as nomP ");
        lsSQL.append(" from comcat_periodicidad ");
		lsSQL.append(" where ic_periodicidad not in (14) ");
		lsSQL.append(" order by 1 ");

		System.out.println(lsSQL.toString());
		lrsSel = eobdConexion.queryDB(lsSQL.toString());
		while (lrsSel.next()){
			if(sPeriod.equals(""))
				sPeriod = "Claves de Periodicidad v�lidas: ";
			else
				sPeriod += ", ";

			sPeriod += lrsSel.getString("icP") + "-" + lrsSel.getString("nomP");
        }

		eobdConexion.cierraStatement();
        lrsSel.close();
        lsSQL = null;
        return sPeriod;
    }


   public ArrayList getSolicitud(String claveIF,String sHorario)
		   throws NafinException {

	  ArrayList campos = new ArrayList();
	  HashMap cotiza= new HashMap();
	  AccesoDB con = null;
	  ResultSet			rs						= null;
	  PreparedStatement	ps						= null;

	     try
        {
            con = new AccesoDB();
			con.conexionDB();


		 String query = " SELECT  TO_CHAR(CS.DF_SOLICITUD,'DD/MM/YYYY') AS FECHA_SOLICITUD,"   +
						" TO_CHAR(CS.DF_COTIZACION,'DD/MM/YYYY  HH24:MM:SS') AS FECHA_COTIZACION,"   +
						" (TO_CHAR(CS.DF_SOLICITUD,'YYYY') || TO_CHAR(CS.DF_SOLICITUD,'MM')"   +
						" || '-' || CS.IG_NUM_SOLIC) AS NUMERO_SOLICITUD,"   +
						" CM.CD_NOMBRE AS MONEDA,"   +
						" CS.IC_MONEDA AS CLAVE_MONEDA," +
						" (CE.CG_NOMBRE || ' ' || CE.CG_APPATERNO || ' ' || CE.CG_APMATERNO) AS EJECUTIVO,"   +
						" CS.FN_MONTO AS MONTO, "   +
						" CS.IG_PLAZO AS PLAZO,"   +
						" DECODE(CS.CS_ACEPTADO_MD,'S',CS.IG_TASA_MD,CS.IG_TASA_SPOT) AS TASA_CONFIRMADA,"   +
						" TO_CHAR(CD.DF_DISPOSICION,'DD/MM/YYYY') AS FECHA_DISPOSICION,"   +
						" CD.IC_DISPOSICION,"   +
						" TO_CHAR(SIGDIAHABIL2(CD.DF_DISPOSICION,-1,PS.IG_DIAS_SOLIC_REC),'dd/mm/yyyy') AS FECHA_LIMITE,"   +
						" CD.IC_SOLIC_ESP,"   +
						" DECODE(CS.IC_IF,NULL,CS.CG_RAZON_SOCIAL_IF,CI.CG_RAZON_SOCIAL) AS CG_RAZON_SOCIAL,"   +
						" DECODE('"+ sHorario +"','N','Fuera de horario de servicio',BUSCA_BASE(CS.IC_IF,CR.IC_ESQUEMA_RECUP,CS.IG_PLAZO,CS.IC_TASA,CS.CG_UT_PPC)) AS REGISTRO, "   +
						" CR.IC_ESQUEMA_RECUP AS ESQUEMA_RECUP, " +
						" TO_CHAR(CD.DF_VENCIMIENTO,'DD/MM/YYYY') AS FECHA_VENCIMIENTO," +
						" CS.IC_TASA AS TASA_BASE," +
						" CS.IG_DIA_PAGO AS DIA_PAGO," +
						" DECODE (CS.CG_UT_PPC, 7.5, 7, 15,14,30,1,60,2,90,3,180,6,360,12, CS.CG_UT_PPC) AS PPC , " +
						" DECODE (CS.CG_UT_PPI, 7.5, 7, 15,14,30,1,60,2,90,3,180,6,360,12, CG_UT_PPI) AS PPI, " +
						" CS.CG_UT_PPC AS NPPC," +
						" CS.CG_UT_PPI AS NPPI" +
						" FROM COT_SOLIC_ESP CS,"   +
						" COTCAT_ESQUEMA_RECUP CR,"   +
						" COTCAT_EJECUTIVO CE,"   +
						" COMCAT_MONEDA CM,"   +
						" COTREL_IF_EJECUTIVO IE,"   +
						" COT_DISPOSICION CD,"   +
						" COMCAT_IF CI,"   +
						" PARAMETROSSISTEMA PS"   +
						" WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO"   +
						" AND CR.IC_ESQUEMA_RECUP=CS.IC_ESQUEMA_RECUP"   +
						" AND CS.IC_MONEDA = CM.IC_MONEDA"   +
						" AND CD.IC_SOLIC_ESP=CS.IC_SOLIC_ESP"   +
						" AND CS.IC_IF = IE.IC_IF(+)"   +
						" AND CI.IC_IF(+) = CS.IC_IF"   +
						" AND CS.IC_MONEDA = CM.IC_MONEDA"   +
						" AND CS.IC_IF=?"  +
						" AND CS.IC_MONEDA=PS.IC_MONEDA"   +
						" AND TRUNC(SIGDIAHABIL2(CD.DF_DISPOSICION,-1,PS.IG_DIAS_SOLIC_REC))>=TRUNC(SYSDATE)"   +
						" AND CD.IC_DISPOSICION=1"   +
						" AND CS.IC_ESTATUS=7" +
						" AND CS.IC_SOLIC_ESP IN (SELECT CS1.IC_SOLIC_ESP " +
                                                 " FROM  COT_SOLIC_ESP CS1 " +
						                         " MINUS SELECT CP.IC_SOLIC_ESP " +
						                         " FROM COM_SOLIC_PORTAL CP "  +
						                         " WHERE IC_ESTATUS_SOLIC <>4) " ;


        ps = con.queryPrecompilado(query);
        ps.setInt(1, Integer.parseInt(claveIF));
		rs = ps.executeQuery();

		while (rs.next()) {
		  cotiza=new HashMap();
		  cotiza.put("fecha_solicitud",(rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD"));
		  cotiza.put("fecha_cotizacion",(rs.getString("FECHA_COTIZACION")==null)?"":rs.getString("FECHA_COTIZACION"));
		  cotiza.put("numero_solicitud",(rs.getString("NUMERO_SOLICITUD")==null)?"":rs.getString("NUMERO_SOLICITUD"));
		  cotiza.put("moneda",(rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
		  cotiza.put("ejecutivo",(rs.getString("EJECUTIVO")==null)?"":rs.getString("EJECUTIVO"));
		  cotiza.put("clave_moneda",(rs.getString("CLAVE_MONEDA")==null)?"":rs.getString("CLAVE_MONEDA"));
		  cotiza.put("monto",(rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
		  cotiza.put("plazo",(rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
		  cotiza.put("tasa_confirmada",(rs.getString("TASA_CONFIRMADA")==null)?"":rs.getString("TASA_CONFIRMADA"));
		  cotiza.put("fecha_disposicion",(rs.getString("FECHA_DISPOSICION")==null)?"":rs.getString("FECHA_DISPOSICION"));
		  cotiza.put("ic_disposicion",(rs.getString("IC_DISPOSICION")==null)?"":rs.getString("IC_DISPOSICION"));
		  cotiza.put("fecha_limite",(rs.getString("FECHA_LIMITE")==null)?"":rs.getString("FECHA_LIMITE"));
		  cotiza.put("ic_solic_esp",(rs.getString("IC_SOLIC_ESP")==null)?"":rs.getString("IC_SOLIC_ESP"));
		  cotiza.put("cg_razon_social",(rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
		  cotiza.put("registro", (rs.getString("REGISTRO")==null)?"":rs.getString("REGISTRO"));
          cotiza.put("esquema", (rs.getString("ESQUEMA_RECUP")==null)?"":rs.getString("ESQUEMA_RECUP"));
          cotiza.put("fecha_vencimiento", (rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
		  cotiza.put("tasa_base", (rs.getString("TASA_BASE")==null)?"":rs.getString("TASA_BASE"));
		  cotiza.put("dia_pago", (rs.getString("DIA_PAGO")==null)?"":rs.getString("DIA_PAGO"));
		  cotiza.put("ppi", (rs.getString("PPI")==null)?"":rs.getString("PPI"));
		  cotiza.put("ppc", (rs.getString("PPC")==null)?"":rs.getString("PPC"));
		  cotiza.put("nppc", (rs.getString("NPPC")==null)?"":rs.getString("NPPC"));
		  cotiza.put("nppi", (rs.getString("NPPI")==null)?"":rs.getString("NPPI"));
		  campos.add(cotiza);
		} // while
		rs.close();
        if(ps!=null) ps.close();
	    }catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
		  if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
		}
	  }
    return campos;
  }


    public ArrayList getCamposcotPagos(String strsolicesp, String fechalimite)
		   throws NafinException {

	  ArrayList campos = new ArrayList();
	  Vector nombres= new Vector();
	  AccesoDB con = null;
	  ResultSet			rs						= null;
	  PreparedStatement	ps						= null;

	     try
        {
            con = new AccesoDB();
			con.conexionDB();

		String query = " select cp.ic_pago as numamort,to_char(cp.df_pago,'dd/mm/yyyy') as fechaamort,  cp.fn_monto as montoamort, sign(trunc(cp.df_pago) - to_date('"+fechalimite+ "', 'dd/mm/yyyy')) as recortada"+
					   " from cot_pago cp"   +
					   " where cp.ic_solic_esp=?"   +
					   " group by cp.ic_pago, cp.fn_monto, cp.df_pago"   +
					   " order by ic_pago"  ;

        ps = con.queryPrecompilado(query);
        ps.setInt(1, Integer.parseInt(strsolicesp));
		rs = ps.executeQuery();

		while (rs.next()) {
		  nombres=new Vector();
		  nombres.add(0,(rs.getString("numamort")==null)?"":rs.getString("numamort"));
		  nombres.add(1,(rs.getString("fechaamort")==null)?"":rs.getString("fechaamort"));
		  nombres.add(2,(rs.getString("montoamort")==null)?"":rs.getString("montoamort"));
		  nombres.add(3,(rs.getString("recortada")==null)?"":rs.getString("recortada"));
		  campos.add(nombres);
		} // while
		rs.close();
        if(ps!=null) ps.close();
	    }catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
		  if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
		}
	  }
    return campos;
  }

  /* Para insertar el temporal cuando el tipo de plazo es E*/
	public Hashtable ohProcesaOperacSolicETmp(String TipoPlazo, String NoIF, String NoClienteSIRAC, String Nombre,
											    String NoSucBanco, String Moneda, String NoDocumento, String ImporteDocto,
												String ImporteDscto, String Emisor, String TipoCredito,
												String PPCapital, String PPIntereses, String BienesServicios,
												String ClaseDocto, String DomicilioPago, String TasaUF, String RelMatUF,
												String SobreTasaUF, String TasaI, String RelMatI, String SobreTasaI,
												String NoAmortizaciones, String Amortizacion, String FechaPPC,
												String DiaPago, String FechaPPI, String FechaVencDocto,
												String FechaVencDscto, String FechaEmisionTC, String LugarFirma,
												String AmortizacionAjuste, String ImporAmortAjuste,
												String FechaAmortAjuste,String tipoRenta,int numeroAmortRec,
												String fvencAmort[],String tipoAmort[],String montoAmort[],
												String sTipoPiso, String sSolicPYME, String sPyme,
												String sSolicTROYA, String sProducto) throws NafinException	{
	String lsQry = "", sTomoPlazoGral = "N";
	ResultSet lrs = null;
	AccesoDB lobdCon = new AccesoDB();
	StringBuffer lsbError = new StringBuffer();
	Hashtable hRes = new Hashtable();
	boolean lbOK = true;
		try {
			lobdCon.conexionDB();
			String NoSolicPortal="";
			String NoProcSolic = "";
			int Plazo=0;

			//Validaci�n para el Intermediario.
			if(!Comunes.esNumero(NoIF)) {
				lsbError.append("No es V�lido el N�mero de Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Clave de Sirac.
			if (!Comunes.esNumero(NoClienteSIRAC)) {
				lsbError.append("No es V�lido el N�mero de Sirac.\n");
				lbOK = false;
			}

			//Validaci�n para el N�mero de Sucursal del Banco.
			NoSucBanco = (NoSucBanco.equals(""))?"null":NoSucBanco;
			//Validaci�n para el tipo de Moneda.
			if(!Moneda.equals("1") && !Moneda.equals("54")) {
				lsbError.append("No es V�lido el tipo de Moneda.\n");
				lbOK = false;
			}
			//Validaci�n para el N�mero de Documento.
			if (!Comunes.esNumero(NoDocumento)) {
				lsbError.append("No es V�lido el N�mero de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Importe de Documento.
			if (!Comunes.esDecimal(ImporteDocto)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lido el Importe del Cr�dito.\n":"No es V�lido el Importe del Documento.\n");
				lbOK = false;
			} else {
				if ((ImporteDocto.substring(ImporteDocto.indexOf(".")+1,ImporteDocto.length())).length() > 2) {
					lsbError.append("El Importe del Documento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para el Importe de Descuento.
			if (!Comunes.esDecimal(ImporteDscto)) {
				lsbError.append(("7".equals(sProducto))?"El Importe de Disposici�n no es un N�mero V�lido.\n":"El Importe del Descuento no es un N�mero V�lido.\n");
				lbOK = false;
			} else {
				if ((ImporteDscto.substring(ImporteDscto.indexOf(".")+1,ImporteDscto.length())).length() > 2) {
					lsbError.append(("7".equals(sProducto))?"El Importe de Disposici�n no puede tener mas de 2 decimales.\n":"El Importe del Descuento no puede tener mas de 2 decimales.\n");
					lbOK = false;
				}
			}
			//Validaci�n para el Tipo de Cr�dito.
			if(!Comunes.esNumero(TipoCredito)) {
				lsbError.append("No es V�lido el tipo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Capital.
			if(!Comunes.esNumero(PPCapital)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para la Periodicidad del Pago de Intereses.
			if(!Comunes.esNumero(PPIntereses)) {
				lsbError.append("No es V�lido el tipo de Periodicidad de Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n de los Bienes y Servicios.
			BienesServicios=(BienesServicios.length()>60)?"'"+BienesServicios.substring(0,59)+"'":"'"+BienesServicios+"'";
			//Validaci�n de la Clase de Documento.
			if(!Comunes.esNumero(ClaseDocto)) {
				lsbError.append("No es V�lido la Clase de Documento.\n");
				lbOK = false;
			}
			//Validaci�n para el Domicilio de Pago.
			DomicilioPago=(DomicilioPago.equals(""))?"null":(DomicilioPago.length()>60?"'"+DomicilioPago.substring(0,59)+"'":"'"+DomicilioPago+"'");
			//Validaci�n para la Tasa de Interes del Usuario Final.
			if(TasaUF.equals(""))
				TasaUF = "null";
			else if(!Comunes.esNumero(TasaUF)) {
				lsbError.append("No es V�lida la Tasa Base del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Usuario Final.
			if(RelMatUF.equals(""))
				RelMatUF = "null";
			else if(!RelMatUF.equals("+") && !RelMatUF.equals("-") && !RelMatUF.equals("*") && RelMatUF.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Usuario Final.\n");
				lbOK = false;
			} else
				RelMatUF = "'"+RelMatUF+"'";
			//Validaci�n para la SobreTasa del Usuario Final.
			if(SobreTasaUF.equals(""))
				SobreTasaUF = "null";
			else if(!Comunes.esDecimal(SobreTasaUF)) {
				lsbError.append("No es V�lida la SobreTasa del Usuario Final.\n");
				lbOK = false;
			}
			//Validaci�n para la Tasa de Interes del Intermediario.
			if(!Comunes.esNumero(TasaI)) {
				lsbError.append("No es V�lida la Tasa de Interes del Intermediario.\n");
				lbOK = false;
			}
			//Validaci�n para la Relaci�n Matem�tica del Intermediario.
			if(RelMatI.equals(""))
				RelMatI = "null";
			else if(!RelMatI.equals("+") && !RelMatI.equals("-") && !RelMatI.equals("*") && RelMatI.equals("/")) {
				lsbError.append("No es V�lida la Relaci�n Matem�tica del Intermediario.\n");
				lbOK = false;
			} else
				RelMatI = "'"+RelMatI+"'";
			//Validaci�n para la SobreTasa del Intermediario.
			if(SobreTasaI.equals(""))
				SobreTasaI = "null";
			//Validaci�n para el N�mero de Amortizaciones.
			if(!Comunes.esNumero(NoAmortizaciones)) {
				lsbError.append("No es V�lido el N�mero de Amortizaciones.\n");
				lbOK = false;
			}
			//Validaci�n para el Tipo de Amortizaci�n.
			if(!Comunes.esNumero(Amortizacion)) {
				lsbError.append("No es V�lido el Tipo de Amortizaci�n.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Capital.
			if(!Comunes.checaFecha(FechaPPC)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Capital.\n");
				lbOK = false;
			}
			//Validaci�n para el D�a de Pago.
			if(!Comunes.esNumero(DiaPago)) {
				lsbError.append("No es V�lido el D�a de Pago.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Primer Pago de Interes.
			if(!Comunes.checaFecha(FechaPPI)) {
				lsbError.append("No es V�lida la Fecha de Primer Pago de Interes.\n");
				lbOK = false;
			}
			//Validaci�n para la Fecha de Vencimiento del Documento.
			if(!FechaVencDocto.equals("")) {
				if(!Comunes.checaFecha(FechaVencDocto)) {
					lsbError.append("No es V�lida la Fecha de Vencimiento del Documento.\n");
					lbOK = false;
				}
			}
			//Validaci�n para la Fecha de Vencimiento del Descuento.
			if(!Comunes.checaFecha(FechaVencDscto)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lida la Fecha de Vencimiento de la L�nea.\n":"No es V�lida la Fecha de Vencimiento del Descuento.\n");
				lbOK = false;
			} else {
				lsQry = "select trunc(TO_DATE('"+FechaVencDscto+"','dd/mm/yyyy')) - trunc(SYSDATE) from dual";
				lrs = lobdCon.queryDB(lsQry);
				if (lrs.next())
					Plazo = lrs.getInt(1);
	            lobdCon.cierraStatement();
				// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
				if(lbOK) {	// Si paso bien todas las condiciones de tipos y valores se hace la validaci�n.
					int iTipoPiso = getTipoPisoIF(NoIF, lobdCon);
					Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIF, TasaI, TipoCredito, Amortizacion, Emisor, Moneda, lobdCon, sProducto, "", "");
					int iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
					if(TipoPlazo.equals("C")) {	// Cr�dito.
						if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en al b.o. se manda el mensaje.
							if(iTipoPiso==1) {
								lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
								lbOK = false;
							}
							if(iTipoPiso==2) {
								lsbError.append("Clave 1008, Favor de comunicarse al 01 800 CADENAS.\n");
								lbOK = false;
							}
						} else if(Plazo > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
							lsbError.append("Plazo mayor al m�ximo permitido.\n");
							lbOK = false;
						}
					} else if(TipoPlazo.equals("F")) {	//	Factoraje.
						if(iPlazoMaxBO==0 && iTipoPiso==1) {
							lsbError.append("Clave 1008, Favor de revisar par�metros de bases de operaci�n.\n");
							lbOK = false;
						}
						if(lbOK) {	// Si no hay alg�n error se recorta el plazo si el IF es de 1ro � 2do piso.
							if(Plazo > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
								lsbError.append("Plazo mayor al m�ximo permitido.\n");
								lbOK = false;
							} // Plazo > iPlazoMaxBO
						}
					} // TipoPlazo.equals("F")
				} // lbOK
			}
			//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito.
			if(!Comunes.checaFecha(FechaEmisionTC)) {
				lsbError.append(("7".equals(sProducto))?"No es V�lida la Fecha de Emisi�n del Pagar�.\n":"No es V�lida la Fecha de Emisi�n del T�tulo de Cr�dito.\n");
				lbOK = false;
			}
			//Validaci�n para el Lugar de Firma.
			LugarFirma = (LugarFirma.length()>50)?"'"+LugarFirma.substring(0,49)+"'":"'"+LugarFirma+"'";
			//Validaci�n para el tipo de Amortizaci�n de Ajuste.
			if(!AmortizacionAjuste.trim().equals("")) {
				if(!AmortizacionAjuste.equalsIgnoreCase("P") && !AmortizacionAjuste.equalsIgnoreCase("U")) {
					lsbError.append("No es V�lido el Tipo de Amortizaci�n de Ajuste.\n");
					lbOK = false;
				} else
					AmortizacionAjuste = "'"+AmortizacionAjuste.toUpperCase()+"'";
			} else
				AmortizacionAjuste = "null";
			//Validaci�n para el Importe del Ajuste.
			if(!ImporAmortAjuste.equals("")) {
				if (!Comunes.esDecimal(ImporAmortAjuste)) {
					lsbError.append("No es V�lido el Importe de la Amortizaci�n del Ajuste.\n");
					lbOK = false;
				} else {
					if ((ImporAmortAjuste.substring(ImporAmortAjuste.indexOf(".")+1,ImporAmortAjuste.length())).length() > 2) {
						lsbError.append("El Importe Amortizaci�n Ajuste no puede tener mas de 2 decimales.\n");
						lbOK = false;
					}
				}
			} else
				ImporAmortAjuste = "null";

			lsQry = " select NVL(max(ic_proc_solic),0)+1,NVL(max(ic_solic_portal),0)+1 from comtmp_solic_portal"  ;
			lrs = lobdCon.queryDB(lsQry);
			if (lrs.next()){
				NoProcSolic		= lrs.getString(1);
				NoSolicPortal	= lrs.getString(2);
			}
            lobdCon.cierraStatement();

			//Valida que no se repita el numero de documento
			try{
				existeNoDoctoE(NoDocumento,NoIF,NoClienteSIRAC,lobdCon);
			}catch(NafinException ne){
				lsbError.append(ne.getMsgError()+".\n");
				lbOK = false;
			}

			if(lbOK) {
                 String tipo_renta_aux=tipoRenta;
				if("S".equals(tipoRenta)||"N".equals(tipoRenta))
					tipoRenta = "'"+tipoRenta+"'";
				else
					tipoRenta = "null";

				String plazoMensual = "S";
				java.util.Date fecAnt = Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));

				if (!"null".equals(tipoRenta)&&fvencAmort!=null) {
					for(int i=0;i<fvencAmort.length;i++){
						java.util.Date fec = Comunes.parseDate(fvencAmort[i]);
						if(fecAnt!=null){
							long lfecAnt = fecAnt.getTime();
							long lfec 	 = fec.getTime();
							double diferencia = (lfec - lfecAnt)/86400000;

							System.out.println("\n\n\n\nLINEA 7728 LA DIFERENCIA = "+diferencia);

							if((int)diferencia>31){
								plazoMensual = "N";
							}
						}
						fecAnt = fec;
					}
				}


				lsQry = " INSERT INTO comtmp_solic_portal (ic_proc_solic,ic_solic_portal, ic_if, ig_clave_sirac, "+
						" ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto, "+
						" ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i, "+
						" cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, ic_tasauf, "+
						" cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort, "+
						" ic_tabla_amort, df_ppc, in_dia_pago, df_ppi, df_v_documento, df_v_descuento, "+
						" ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste, fg_importe_ajuste, "+
						" cs_tipo_plazo, cs_plazo_maximo_factoraje,cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual, "+
						" ic_producto_nafin,ig_numero_solic_troya,ic_solicitud_equipo) "+
						" values("+NoProcSolic+","+NoSolicPortal+","+NoIF+","+NoClienteSIRAC+","+NoSucBanco+","+Moneda
						+" ,"+NoDocumento+","+ImporteDocto+","+ImporteDscto+","+(Emisor.equals("")?"null":Emisor)
	           			+" ,"+TipoCredito+","+PPCapital+","+PPIntereses+","+BienesServicios
						+" ,"+ClaseDocto+","+DomicilioPago+","+TasaUF+","+RelMatUF+","+SobreTasaUF
						+" ,"+TasaI+","+RelMatI+","+SobreTasaI+","+NoAmortizaciones+","+Amortizacion
//						+" ,TO_DATE('"+FechaPPC+"','DD/MM/YYYY'),"+DiaPago
//	            		+" ,TO_DATE('"+FechaPPI+"','DD/MM/YYYY'),TO_DATE('"+FechaVencDocto+"','DD/MM/YYYY')"
//						+" ,TO_DATE('"+FechaVencDscto+"','DD/MM/YYYY'),"+Plazo
//						+" ,TO_DATE('"+FechaEmisionTC+"','DD/MM/YYYY'),"+LugarFirma+","+AmortizacionAjuste
						+" ,'"+FechaPPC+"',"+DiaPago
	            		+" ,'"+FechaPPI+"','"+FechaVencDocto+"'"
						+" ,'"+FechaVencDscto+"',"+Plazo
						+" ,'"+FechaEmisionTC+"',"+LugarFirma+","+AmortizacionAjuste
	            		+" ,"+ImporAmortAjuste+",'"+TipoPlazo+"','"+sTomoPlazoGral+"',"+tipoRenta+","+numeroAmortRec+",'"+plazoMensual+"',"
	            		+(sProducto.equals("")?"null":sProducto)+","+(sSolicTROYA.equals("")?"null":sSolicTROYA)+","+(sSolicPYME.equals("")?"null":sSolicPYME)+")";
				try {
	               	lobdCon.ejecutaSQL(lsQry);
				} catch (Exception err) {
					throw new NafinException("DSCT0061");
				}
				System.out.println("amortizacion:"+Amortizacion);
				System.out.println("tipoRenta:"+tipoRenta);
				if (Amortizacion.equals("5")&&tipo_renta_aux.equals("N"))
				{  ovInsertaAmortizacionesTmp(NoSolicPortal,NoProcSolic,fvencAmort,tipoAmort,montoAmort,lobdCon);
				} //Fin de la manera en que se generar� el calendario
			}

			hRes.put("lbOK", new Boolean(lbOK));
			hRes.put("lsbError", lsbError.toString());
			hRes.put("NoSolicPortal", NoSolicPortal);
			hRes.put("NoProcSolic", NoProcSolic);
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) { e.printStackTrace(); }
		finally {
			lobdCon.terminaTransaccion(lbOK);
			if(lobdCon.hayConexionAbierta()) lobdCon.cierraConexionDB();
		}
	return hRes;
	} // Fin del m�todo ohProcesaOperacSolicTmpE
	//DEPRECATED
 	public Vector ovTransmitirDoctosE( int eicProcSolic, String esAcuse )
			throws NafinException {
		BigDecimal bdMtoDoctos = new BigDecimal("0.0");
		BigDecimal bdMtoDsctos = new BigDecimal("0.0");
		String lsicIf=null, lsClaveSirac=null, lsSucursal=null, lsMoneda=null, lsNoDocto=null;
        String lsImpteDocto=null, lsImpteDscto=null, lsicEmisor=null, lstipoCred=null;
        String lsicPerioC=null, lsicPerioI=null, lsBienServ=null, lsClaseDocto=null;
		String lsDomPago=null, lsicTasaUF=null, lsRelMatUF=null, lsSobreTasUF=null;
		String lsicTasaIF=null, lsRelMatIF=null, lsSobreTasIF=null, lsNoAmort=null;
		String lstablaAmort=null, lsdfPPC=null, lsdiaPago=null, lsdfPPI=null;
        String lsdfDocto=null, lsdfDscto=null, lsPlazo=null, lsdfETC=null, lsLugarFirma=null;
        String lsamortAjuste=null, lsimporAjuste=null, lsTipoPlazo=null, lsAplicPlazo=null;
        String lsIcSolPorTmp = "";
		String lsTipoRenta	= "";
		String lsNumAmortRec = "";
		String lsPlazoMensual = "";
		String sProducto = "", sSolicPyme="", sSolicTroya="";
        int licSolicPortal = 0;
        int liNoSolicCarga = 0;


        Vector lovResultado = new Vector();
        String lsSQLMax 	= null;
		StringBuffer lsCadenaSQL   	= new StringBuffer();

		ResultSet lrsSel = null;
		ResultSet lrsSel2 = null;
		AccesoDB lodbConexion = new AccesoDB();
		boolean bOk = true;
		System.out.println(" AutorizacionSolicitudEJB::ovTransmitirDoctos(E)");

		try {
			lodbConexion.conexionDB();

			lsCadenaSQL.append(
				" select ic_if, ig_clave_sirac, ig_sucursal, ic_moneda, ig_numero_docto, "+
				" fn_importe_docto, fn_importe_dscto, ic_emisor, ic_tipo_credito, "+
				" ic_periodicidad_c, ic_periodicidad_i, cg_bienes_servicios, ic_clase_docto, "+
				" cg_domicilio_pago, ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, "+
				" fg_stif, in_numero_amort, ic_tabla_amort,  "+
	            " TO_CHAR(TO_DATE(df_ppc,'DD/MM/YYYY'),'DD/MM/YYYY') as fechaPPC, in_dia_pago, "+
				" TO_CHAR(TO_DATE(df_ppi,'DD/MM/YYYY'),'DD/MM/YYYY') as fechaPPI, TO_CHAR(TO_DATE(df_v_documento,'DD/MM/YYYY'),'DD/MM/YYYY') as fechaDocto, "+
	            " TO_CHAR(TO_DATE(df_v_descuento,'DD/MM/YYYY'),'DD/MM/YYYY') as fechaDscto, ig_plazo, "+
				" TO_CHAR(TO_DATE(df_etc,'DD/MM/YYYY'),'DD/MM/YYYY') as fechaETC, cg_lugar_firma, cs_amort_ajuste, "+
	            " fg_importe_ajuste, cs_tipo_plazo, cs_plazo_maximo_factoraje "+
				" ,ic_solic_portal"+
				" ,cs_tipo_renta"+
				" ,in_numero_amort_rec"+
				" ,cs_plazo_mensual"+
				" ,ic_producto_nafin"+
				" ,ic_solicitud_equipo"+
				" ,ig_numero_solic_troya"+
	            " from comtmp_solic_portal "+
	            " where ic_proc_solic = "+eicProcSolic);
			bOk = true;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL.toString());
			while(lrsSel.next()){
				lsicIf 		 		= (lrsSel.getString("ic_if")==null)?"null" : lrsSel.getString("ic_if");
				lsClaveSirac 		= (lrsSel.getString("ig_clave_sirac")==null)?"null" : lrsSel.getString("ig_clave_sirac");
				lsSucursal	 		= (lrsSel.getString("ig_sucursal")==null)?"null" : lrsSel.getString("ig_sucursal");
				lsMoneda	 		= (lrsSel.getString("ic_moneda")==null)?"null" : lrsSel.getString("ic_moneda");
				lsNoDocto	 		= (lrsSel.getString("ig_numero_docto")==null)?"null" : lrsSel.getString("ig_numero_docto");
				lsImpteDocto 		= (lrsSel.getString("fn_importe_docto")==null)?"null" : lrsSel.getString("fn_importe_docto");
				lsImpteDscto 		= (lrsSel.getString("fn_importe_dscto")==null)?"null" : lrsSel.getString("fn_importe_dscto");
				lsicEmisor 	 		= (lrsSel.getString("ic_emisor")==null)?"null" : lrsSel.getString("ic_emisor");
				lstipoCred 	 		= (lrsSel.getString("ic_tipo_credito")==null)?"null" : lrsSel.getString("ic_tipo_credito");
				lsicPerioC 	 		= (lrsSel.getString("ic_periodicidad_c")==null)?"null" : lrsSel.getString("ic_periodicidad_c");
				lsicPerioI 	 		= (lrsSel.getString("ic_periodicidad_i")==null)?"null" : lrsSel.getString("ic_periodicidad_i");
				lsBienServ   		= (lrsSel.getString("cg_bienes_servicios")==null)?"null" : "'"+lrsSel.getString("cg_bienes_servicios")+"'";
				lsClaseDocto 		= (lrsSel.getString("ic_clase_docto")==null)?"null" : lrsSel.getString("ic_clase_docto");
				lsDomPago    		= (lrsSel.getString("cg_domicilio_pago")==null)?"null" : "'"+lrsSel.getString("cg_domicilio_pago")+"'";
				lsicTasaUF 	 		= (lrsSel.getString("ic_tasauf")==null)?"null" : lrsSel.getString("ic_tasauf");
				lsRelMatUF   		= (lrsSel.getString("cg_rmuf")==null)?"null" : "'"+lrsSel.getString("cg_rmuf")+"'";
				lsSobreTasUF 		= (lrsSel.getString("fg_stuf")==null)?"null" : lrsSel.getString("fg_stuf");
				lsicTasaIF 	 		= (lrsSel.getString("ic_tasaif")==null)?"null" : lrsSel.getString("ic_tasaif");
				lsRelMatIF   		= (lrsSel.getString("cg_rmif")==null)?"null" : "'"+lrsSel.getString("cg_rmif")+"'";
				lsSobreTasIF 		= (lrsSel.getString("fg_stif")==null)?"null" : lrsSel.getString("fg_stif");
				lsNoAmort    		= (lrsSel.getString("in_numero_amort")==null)?"null" : lrsSel.getString("in_numero_amort");
				lstablaAmort 		= (lrsSel.getString("ic_tabla_amort")==null)?"null" : lrsSel.getString("ic_tabla_amort");
				lsdfPPC 	 		= (lrsSel.getString("fechaPPC")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaPPC")+"','DD/MM/YYYY')";
				lsdiaPago 			= (lrsSel.getString("in_dia_pago")==null)?"null" : lrsSel.getString("in_dia_pago");
				lsdfPPI 			= (lrsSel.getString("fechaPPI")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaPPI")+"','DD/MM/YYYY')";
				lsdfDocto			= (lrsSel.getString("fechaDocto")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaDocto")+"','DD/MM/YYYY')";
				lsdfDscto 			= (lrsSel.getString("fechaDscto")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaDscto")+"','DD/MM/YYYY')";
				lsPlazo		 		= (lrsSel.getString("ig_plazo")==null)?"null" : lrsSel.getString("ig_plazo");
				lsdfETC		 		= (lrsSel.getString("fechaETC")==null)?"null" : "TO_DATE('"+lrsSel.getString("fechaETC")+"','DD/MM/YYYY')";
				lsLugarFirma 		= (lrsSel.getString("cg_lugar_firma")==null)?"null" : "'"+lrsSel.getString("cg_lugar_firma")+"'";
				lsamortAjuste		= (lrsSel.getString("cs_amort_ajuste")==null)?"null" : "'"+lrsSel.getString("cs_amort_ajuste")+"'";
				lsimporAjuste		= (lrsSel.getString("fg_importe_ajuste")==null)?"null" : lrsSel.getString("fg_importe_ajuste");
				lsTipoPlazo	 		= (lrsSel.getString("cs_tipo_plazo")==null)?"F" : lrsSel.getString("cs_tipo_plazo");
				lsAplicPlazo 		= (lrsSel.getString("cs_plazo_maximo_factoraje")==null)?"N" : lrsSel.getString("cs_plazo_maximo_factoraje");
				lsIcSolPorTmp		= (lrsSel.getString("ic_solic_portal")==null)?"N" : lrsSel.getString("ic_solic_portal");
				lsTipoRenta			= (lrsSel.getString("cs_tipo_renta")==null)?"null" :("'"+lrsSel.getString("cs_tipo_renta")+"'");
				lsNumAmortRec 		= (lrsSel.getString("in_numero_amort_rec")==null)?"null" : lrsSel.getString("in_numero_amort_rec");
				lsPlazoMensual		= (lrsSel.getString("cs_plazo_mensual")==null)?"null" : lrsSel.getString("cs_plazo_mensual");
				sProducto 			= (lrsSel.getString("ic_producto_nafin")==null)?"null" : lrsSel.getString("ic_producto_nafin");
				sSolicPyme			= (lrsSel.getString("ic_solicitud_equipo")==null)?"null" : lrsSel.getString("ic_solicitud_equipo");
				sSolicTroya			= (lrsSel.getString("ig_numero_solic_troya")==null)?"" : lrsSel.getString("ig_numero_solic_troya");

				//Valida que por concurrencia no se repitan los registros
				//existeNoDocto(lsNoDocto, lsicIf, lsClaveSirac, lodbConexion);


				lsSQLMax = " SELECT SEQ_COM_SOLIC_PORTAL.NEXTVAL FROM DUAL ";
				lrsSel2 = lodbConexion.queryDB(lsSQLMax);
				if (lrsSel2.next())
					licSolicPortal = (lrsSel2.getInt(1)==0)?1:lrsSel2.getInt(1);
                lrsSel2.close();


		        //Credito Electr�nico no tiene clave de producto en comcat_producto_nafin
		        //Pero se utiliza el 0 (o null en el siguiente caso) para representar
		        //el producto
		        int estatusSolicitud = (sProducto.equals("null") &&
		        		 esIfBloqueado(lsicIf, "0"))?12:1; //0 = Es la clave para Credito Electr�nico

                lsCadenaSQL.delete(0, lsCadenaSQL.length());
				lsCadenaSQL.append(
					" INSERT INTO com_solic_portal (ic_solic_portal, ic_if, ig_clave_sirac, "+
					" ig_sucursal, ic_moneda, ig_numero_docto, fn_importe_docto, "+
					" fn_importe_dscto, ic_emisor, ic_tipo_credito, ic_periodicidad_c, "+
					" ic_periodicidad_i, cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, "+
					" ic_tasauf, cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort, "+
					" ic_tabla_amort, df_ppc, in_dia_pago, df_ppi, df_v_documento, df_v_descuento, "+
					" ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste, fg_importe_ajuste, "+
	                " ic_estatus_solic, cc_acuse, cs_tipo_plazo, cs_plazo_maximo_factoraje,"+
					" cs_tipo_renta,in_numero_amort_rec,cs_plazo_mensual, "+
					" ic_producto_nafin,ic_solicitud_equipo,ig_numero_solic_troya, cc_llave, ic_solic_esp) "+
					" values ("+licSolicPortal+","+lsicIf+","+lsClaveSirac+","+lsSucursal+","+
					lsMoneda+","+lsNoDocto+","+lsImpteDocto+","+lsImpteDscto+","+lsicEmisor+","+
					lstipoCred+","+lsicPerioC+","+lsicPerioI+","+lsBienServ+","+lsClaseDocto+","+
	                lsDomPago+","+lsicTasaUF+","+lsRelMatUF+","+lsSobreTasUF+","+lsicTasaIF+","+
					lsRelMatIF+","+lsSobreTasIF+","+lsNoAmort+","+lstablaAmort+","+lsdfPPC+","+
					lsdiaPago+","+lsdfPPI+","+lsdfDocto+","+lsdfDscto+","+lsPlazo+","+
	                lsdfETC+","+lsLugarFirma+","+lsamortAjuste+","+lsimporAjuste+"," +
	                estatusSolicitud + ",'"+
					esAcuse+"','"+lsTipoPlazo+"','"+lsAplicPlazo+"',"+
					lsTipoRenta+","+lsNumAmortRec+",'"+lsPlazoMensual+"',"+
					sProducto+","+sSolicPyme+",'"+sSolicTroya+"', '" +
					lsicIf+lsClaveSirac+lsNoDocto+"'||to_char(sysdate,'ddmmyyyyhh24miss'),"+lsNoDocto+")");

                lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
				lsCadenaSQL.delete(0, lsCadenaSQL.length());
				lsCadenaSQL.append(
						" INSERT INTO COM_AMORT_PORTAL"   +
						" (IC_AMORT_PORTAL,IC_SOLIC_PORTAL,CG_TIPO_AMORT,FN_MONTO,DF_VENCIMIENTO)"   +
						" SELECT IC_AMORT_PORTAL,"+licSolicPortal+",CG_TIPO_AMORT,FN_MONTO,to_date(DF_VENCIMIENTO,'dd/mm/yyyy')"   +
						" FROM COMTMP_AMORT_PORTAL WHERE IC_SOLIC_PORTAL = "+lsIcSolPorTmp) ;

                lodbConexion.ejecutaSQL(lsCadenaSQL.toString());
				liNoSolicCarga++;
				bdMtoDoctos = bdMtoDoctos.add(new BigDecimal(lsImpteDocto));
				bdMtoDsctos = bdMtoDsctos.add(new BigDecimal(lsImpteDscto));

			} //while
			lodbConexion.cierraStatement();

			lovResultado.addElement(new Integer(liNoSolicCarga));	//Doctos Cargados
			lovResultado.addElement(bdMtoDoctos);			//Monto total de Doctos Cargados
			lovResultado.addElement(bdMtoDsctos);			//Monto total de Doctos Cargados
			return lovResultado;
		} catch (SQLException sqleError){
			bOk = false;
			System.out.println("Error SQLException, AutorizacionSolicitudEJB.ovTransmitirDoctos(SQLException): "+ sqleError.getMessage());
			throw new NafinException("CRED0011");
		} catch (NafinException neError){
			bOk = false;
			System.out.println("Error NafinException, AutorizacionSolicitudEJB.ovTransmitirDoctos(NafinException): "+ neError.getMsgError());
			throw neError;
		} catch (Exception epError){
			bOk = false;
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovTransmitirDoctos(): "+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(bOk);
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
			System.out.println("AutorizacionSolicitudEJB::ovTransmitirDoctos(S)");
		}
	}



	public Map ovProcesarArchivoSiracCE(List lDatos)
	throws NafinException	{
		System.out.println(" ovProcesarArchivoXLS::procesarArchivoXLS(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		String		qrySentencia	= "";

		Map mRetorno = new HashMap();
		int 	liLineadocs= 0;
		try {
			con.conexionDB();

			List lErrores	= new ArrayList();
			List lCorrectos	= new ArrayList();
			Map Error	= null;
			String	folio		= null;
			String	prestamo	= null;
			String	fechaOper	= null;

			if (lDatos.size() == 0){
				Error = new HashMap();
				Error.put("linea","000");
				Error.put("error","El archivo contiene m�s de una hoja de c&aacute;lculo<br>o el contenido es incorrecto.");
				Error.put("solucion","Inserte un archivo con una sola hoja de<br>c&aacute;lculo y verifique el layout. <a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
				lErrores.add(Error);
			} else {
				List lovDatos = (ArrayList) lDatos.get(0);
				if (lovDatos.size() < 3){
					Error = new HashMap();
					Error.put("linea","000");
					Error.put("error","No coincide con el layout requerido.");
					Error.put("solucion","Verifique el n�mero de datos.");
					lErrores.add(Error);
				}
			}

			qrySentencia =
				" select ic_solic_portal"+
				" ,ic_estatus_solic "   +
				" from com_solic_portal"   +
				" where ic_solic_portal = ?";
			ps = con.queryPrecompilado(qrySentencia);
			for(liLineadocs = 1;liLineadocs<lDatos.size();liLineadocs++){
				System.out.println("Valida Linea "+liLineadocs);

				List lovDatos = (List) lDatos.get(liLineadocs);

				// Valida el n�mero de datos de la l�nea
				if(lovDatos.size() < 3) {
					Error = new HashMap();
					Error.put("linea",""+liLineadocs);
					Error.put("error","No coincide con el layout requerido.");
					Error.put("solucion","Verifique el n&uacute;mero de datos.");
					lErrores.add(Error);
				} else {
					boolean ok = true;
					folio			=(lovDatos.size()>=1)?Comunes.quitaComitasSimples(lovDatos.get(0).toString().trim()).trim():"";
					prestamo		=(lovDatos.size()>=2)?Comunes.quitaComitasSimples(lovDatos.get(1).toString().trim()).trim():"";
					fechaOper		=(lovDatos.size()>=3)?Comunes.quitaComitasSimples(lovDatos.get(2).toString().trim()).trim():"";

					try{
						Integer.parseInt(folio);
					}catch(Exception e){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","El valor del folio es incorrecto");
						Error.put("solucion","Verifique que este capturado y que sea un valor num&eacute;rico.&nbsp;<a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
						lErrores.add(Error);
						ok = false;
					}

					if(prestamo!=null&&prestamo.length()>12){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del prestamo excede el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a ocho digitos.&nbsp;<a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
						lErrores.add(Error);
						ok = false;
					}else{
						try{
							Long.parseLong(prestamo);
						}catch(Exception e){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El valor del prestamo es incorrecto");
							Error.put("solucion","Verifique que este capturado y que sea un valor num&eacute;rico.&nbsp;<a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
							lErrores.add(Error);
							ok = false;
						}
					}

					if(fechaOper==null||fechaOper.length()!=10){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El formato de fecha es incorrecto");
							Error.put("solucion","Verifique que la celda este marcada como tipo texto<br>y que la fecha este capturada en formato dd/mm/aaaa.&nbsp;<a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
							lErrores.add(Error);
							ok = false;
					}else if(!Comunes.checaFecha(fechaOper)){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","La fecha capturada no es valida");
							Error.put("solucion","Verifique que sea una fecha correcta en el calendario.&nbsp;<a href='javascript:ayuda();'><font color='blue'>Ayuda</font></a>");
							lErrores.add(Error);
							ok = false;
					}
					if(ok){
						ps.setString(1,folio);
						rs = ps.executeQuery();
						if(rs.next()){
							if(rs.getInt("ic_estatus_solic")==4){
								lCorrectos.add(Integer.toHexString(rs.getInt("ic_solic_portal"))+"|"+Integer.toOctalString(Integer.parseInt(prestamo))+"|"+fechaOper);
							}else{
								Error = new HashMap();
								Error.put("linea",""+liLineadocs);
								Error.put("error","La solicitud no tiene el estatus Rechazada");
								Error.put("solucion","Verifique el n&uacute;mero de folio capturado.");
								lErrores.add(Error);
							}
						}else{
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","No existe n&uacute;mero de solicitud en N@fin-Electr&oacute;nico.");
							Error.put("solucion","Verifique que sea correcto.");
							lErrores.add(Error);
						}
						rs.close();
					}
				}
			}
			ps.close();

			mRetorno.put("errores",lErrores);
			mRetorno.put("correctos",lCorrectos);
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("PARM0009");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println(" ovProcesarArchivoXLS::procesarArchivoXLS(E)");
		}
		return mRetorno;
	}


	public Map confirmaSiracCredEle(String fpf[],String icUsuario)
	throws NafinException	{
		System.out.println(" confirmaSiracCredEle::procesarArchivoXLS(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		String		qrySentencia	= "";

		Map 			mRetorno	= new HashMap();
		List			lFolios		= new ArrayList();
		List			lPrestamos	= new ArrayList();
		List			lFechas		= new ArrayList();
		StringBuffer	foliosIn	= new StringBuffer("");
		int				numDoctosMN	= 0;
		int				numDoctosUSD= 0;
		double			totMontoMN	= 0;
		double			totMontoUSD	= 0;
		boolean			ok			= true;
		try {
			con.conexionDB();
			Acuse acuse = new Acuse(4,"1",con);

			String fecha = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String hora = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			for(int i=0;i<fpf.length;i++){
				List lReg = Comunes.explode("|",fpf[i]);
				String folio	= (String)lReg.get(0);
				String prestamo	= (String)lReg.get(1);
				lFechas.add(lReg.get(2));
				folio			= ""+Integer.parseInt(folio,16);
				prestamo		= ""+Integer.parseInt(prestamo,8);
				lFolios.add(folio);
				lPrestamos.add(prestamo);
				if(i>0)
					foliosIn.append(",");
				foliosIn.append("?");
			}
			qrySentencia =
				" select count(1) as numDoctos,sum(fn_importe_dscto) as totMonto,ic_moneda"   +
				" from com_solic_portal"   +
				" where ic_solic_portal in("+foliosIn.toString()+")"   +
				" group by ic_moneda"  ;
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<lFolios.size();i++){
				ps.setString((i+1),lFolios.get(i).toString());
			}
			rs = ps.executeQuery();
			while(rs.next()){
				if(rs.getInt("ic_moneda")==1){
					numDoctosMN		= rs.getInt("numDoctos");
					totMontoMN		= rs.getDouble("totMonto");
				}else if(rs.getInt("ic_moneda")==54){
					numDoctosUSD	= rs.getInt("numDoctos");
					totMontoUSD		= rs.getDouble("totMonto");
				}
			}
			rs.close();
			ps.close();

			qrySentencia =
				" insert into com_acuse4" +
				"(CC_ACUSE, IC_USUARIO, IN_TOTAL_DOCTO_MN, FN_TOTAL_MONTO_MN, IN_TOTAL_DOCTO_DL, FN_TOTAL_MONTO_DL, IC_PRODUCTO_NAFIN, DF_FECHA_HORA_CARGA)" +
				"values(?,?,?,?,?,?,0,to_date(?,'dd/mm/yyyy hh24:mi:ss'))";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,acuse.toString());
			ps.setString(2,icUsuario);
			ps.setInt(3,numDoctosMN);
			ps.setDouble(4,totMontoMN);
			ps.setInt(5,numDoctosUSD);
			ps.setDouble(6,totMontoUSD);
			ps.setString(7,fecha+" "+hora);
			ps.execute();

			qrySentencia =
				" update com_solic_portal"+
				" set ig_numero_prestamo = ?"+
				" ,df_operacion = to_date(?,'dd/mm/yyyy')"+
				" ,ic_estatus_solic = 3"+
				" ,cc_acuse_nafin = ?"+
				" where ic_solic_portal = ?";
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<lFolios.size();i++){
				ps.setString(1,lPrestamos.get(i).toString());
				ps.setString(2,lFechas.get(i).toString());
				ps.setString(3,acuse.toString());
				ps.setString(4,lFolios.get(i).toString());
				ps.execute();
			}
			ps.close();

			mRetorno.put("acuse",acuse.toString());
			mRetorno.put("fecha",fecha);
			mRetorno.put("hora",hora);


		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println(" confirmaSiracCredEle::procesarArchivoXLS(E)");
		}
		return mRetorno;
	}

//>>>IHJ>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	public List getDatosCargaMasivaCE(String rutaFisica, String archivo, String ext) throws NafinException {
		System.out.println("AutorizacionSolicitudBean::getDatosCargaMasivaCE(E)");
		List 	lDatos 		= new ArrayList();
		String 	lsLinea 	= "";
		int 	liLineadocs = 1;
		boolean	lbOK 		= false;
		try {
			if("TXT".equals(ext)) {
				java.io.File file = new java.io.File(rutaFisica+archivo);
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
				for (liLineadocs = 1; (lsLinea = br.readLine()) != null; liLineadocs++) {
					lbOK = true;
					lsLinea = lsLinea.trim();
					if (lsLinea.length()==0)
						continue;
					VectorTokenizer lvt = new VectorTokenizer(lsLinea,"|");
    	            Vector lovDatos = lvt.getValuesVector();
					lDatos.add(lovDatos);
				}//for (liLineadocs = 1; (lsLinea = br.readLine()) != null; liLineadocs++)
			} else if("XLS".equals(ext)) {
				ComunesXLS xls = new ComunesXLS();
				lDatos = (ArrayList) xls.LeerXLS(rutaFisica+archivo, false);
			}
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getDatosCargaMasivaCE(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("AutorizacionSolicitudBean::getDatosCargaMasivaCE(S)");
		}
		return lDatos;
	}//getDatosCargaMasiva

	public void setDatosCargaMasivaCE(
						String ig_clave_sirac,		String ig_sucursal,			String ic_moneda,			String ig_numero_docto,
						String fn_importe_docto,	String fn_importe_dscto,	String ic_emisor,			String ic_tipo_credito,
						String ic_periodicidad_c,	String ic_periodicidad_i,	String cg_bienes_servicios,	String ic_clase_docto,
						String cg_domicilio_pago,	String ic_tasauf,			String cg_rmuf, 			String fg_stuf,
						String ic_tasaif,			String in_numero_amort,		String ic_tabla_amort,		String df_ppc,
						String df_ppi,				String in_dia_pago,			String df_v_documento,		String df_v_descuento,
						String df_etc, 				String cg_lugar_firma,		String cs_amort_ajuste,		String cs_tipo_renta,
						String cg_destino_credito, String tipoPlazo,
						String ic_proc_solic,		List lRegistro, 			String cg_reg_ok,			String cg_error
		) throws NafinException {

		System.out.println("AutorizacionSolicitudBean::setDatosCargaMasivaCE(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		boolean				bOk				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			String ic_solic_portal = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_solic_portal) + 1, 1)"   +
				"   FROM comtmp_solic_portal"   +
				"  WHERE ic_proc_solic = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, new Long(ic_proc_solic).longValue());
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				ic_solic_portal = rs.getString(1)==null?"1":rs.getString(1);
			}
			rs.close();ps.close();

			qrySentencia =
				" INSERT INTO comtmp_solic_portal ("   +
				"              ic_proc_solic, ic_solic_portal, ig_clave_sirac, ig_sucursal,"   +
				"              ic_moneda, ig_numero_docto, fn_importe_docto, fn_importe_dscto,"   +
				"              ic_emisor, ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i,"   +
				"              cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, ic_tasauf,"   +
				"              cg_rmuf, fg_stuf, ic_tasaif, in_numero_amort, ic_tabla_amort,"   +
				"              df_ppc, df_ppi, in_dia_pago, df_v_documento, df_v_descuento,"   +
				"              df_etc, cg_lugar_firma, cs_amort_ajuste, cs_tipo_renta, cg_destino_credito,"   +
				"              cg_reg_ok, cg_error "   +
				" 			 )"   +
				"      VALUES ("   +
				"              ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?, ?,"   +
				"              ?, ? "   +
				"             )"  ;
				System.out.println("qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, new Long(ic_proc_solic).longValue());
			ps.setLong( 2, new Long(ic_solic_portal).longValue());
			ps.setString( 3, ig_clave_sirac);
			ps.setString( 4, ig_sucursal);
			ps.setString( 5, ic_moneda);
			ps.setString( 6, ig_numero_docto);
			ps.setString( 7, fn_importe_docto);
			ps.setString( 8, fn_importe_dscto);
			ps.setString( 9, ic_emisor);
			ps.setString(10, ic_tipo_credito);
			ps.setString(11, ic_periodicidad_c);
			ps.setString(12, ic_periodicidad_i);
			ps.setString(13, cg_bienes_servicios);
			ps.setString(14, ic_clase_docto);
			ps.setString(15, cg_domicilio_pago);
			ps.setString(16, ic_tasauf);
			ps.setString(17, cg_rmuf);
			ps.setString(18, fg_stuf);
			ps.setString(19, ic_tasaif);
			ps.setString(20, in_numero_amort);
			ps.setString(21, ic_tabla_amort);
			ps.setString(22, df_ppc);
System.out.println("df_ppc: "+df_ppc);
			ps.setString(23, df_ppi);
System.out.println("df_ppi: "+df_ppi);
			ps.setString(24, in_dia_pago);
			ps.setString(25, df_v_documento);
			ps.setString(26, df_v_descuento);
			ps.setString(27, df_etc);
			ps.setString(28, cg_lugar_firma);
			ps.setString(29, cs_amort_ajuste);
			ps.setString(30, cs_tipo_renta);
			ps.setString(31, cg_destino_credito);
			ps.setString(32, cg_reg_ok);
			ps.setString(33, cg_error);
			ps.executeUpdate();
			ps.close();

			// Insertar C�digo de Base de Operaci�n Din�mica
			int offsetUltimo = 0;
			int numParams = "C".equals(tipoPlazo)?29:28;
			if( lRegistro.size() > numParams  && ( (( lRegistro.size() - numParams ) % 4) == 1 ) ){

				String codigoBaseOperacionDinamica 	= (String) lRegistro.get(lRegistro.size()-1);
				codigoBaseOperacionDinamica			= codigoBaseOperacionDinamica == null?"":codigoBaseOperacionDinamica.trim();
				codigoBaseOperacionDinamica	      = Comunes.strtr(Comunes.quitaComitasSimples(codigoBaseOperacionDinamica),"\"","").trim();
				codigoBaseOperacionDinamica	      = codigoBaseOperacionDinamica.length() > 6?codigoBaseOperacionDinamica.substring(0,7):codigoBaseOperacionDinamica;

				log.debug("Se especific� c�digo de base de operaci�n din�mica = <"+codigoBaseOperacionDinamica+"> para ic_proc_solic = <"+ic_proc_solic+">, ic_solic_portal = <"+ic_solic_portal+">");

				qrySentencia =
					"UPDATE "  +
					"	COMTMP_SOLIC_PORTAL "  +
					"SET "  +
					"	IG_BASE_OPERACION = ? "  +
					"WHERE "  +
					"	IC_PROC_SOLIC     = ? AND "  +
					"	IC_SOLIC_PORTAL   = ? " ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString( 1, codigoBaseOperacionDinamica   			 );
				ps.setLong(   2, new Long(ic_proc_solic).longValue()   );
				ps.setLong(   3, new Long(ic_solic_portal).longValue() );
				ps.executeUpdate();
				ps.close();

				offsetUltimo = -1;

			} else {
				log.debug("No se especific� ning�n c�digo de base de operaci�n din�mica para ic_proc_solic = <"+ic_proc_solic+">, ic_solic_portal = <"+ic_solic_portal+">");
			}

			// Leer los registros con los valores de la amortizacion
			for(int i=numParams;i<(lRegistro.size()+offsetUltimo);i+=4) {
				String ic_amort_portal = "1";
				qrySentencia =
					" SELECT NVL (MAX (ic_amort_portal) + 1, 1)"   +
					"   FROM comtmp_amort_portal"   +
					"  WHERE ic_proc_solic = ?"  +
					"    AND ic_solic_portal = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setLong( 1, new Long(ic_proc_solic).longValue());
				ps.setLong( 2, new Long(ic_solic_portal).longValue());
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					ic_amort_portal = rs.getString(1)==null?"1":rs.getString(1);
				}
				rs.close();ps.close();
System.out.println("********************************************************* "+lRegistro.get(i+0));
				String montoAmort = lRegistro.get(i+1).toString().trim();
System.out.println("montoAmort: "+montoAmort);
				String fechaAmort = lRegistro.get(i+2).toString().trim();
System.out.println("fechaAmort: "+fechaAmort);
				String tipoAmort  = lRegistro.get(i+3).toString().trim();
System.out.println("tipoAmort: "+tipoAmort);

				qrySentencia =
					" INSERT INTO comtmp_amort_portal("   +
					"              ic_proc_solic, ic_solic_portal, ic_amort_portal, fn_monto, df_vencimiento, cg_tipo_amort)"   +
					"      VALUES ("   +
					"              ?, ?, ?, ?, ?, ?)"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setLong( 1, new Long(ic_proc_solic).longValue());
				ps.setLong( 2, new Long(ic_solic_portal).longValue());
				ps.setLong( 3, new Long(ic_amort_portal).longValue());
				ps.setString(4, montoAmort);
				ps.setString(5, fechaAmort);
				ps.setString(6, tipoAmort);
				ps.executeUpdate();
				ps.close();

			}//for(int i=28;i<lRegistro.size();i+=4)

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::setDatosCargaMasivaCE(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::setDatosCargaMasivaCE(S)");
		}
	}//setDatosCargaMasivaCE

	public List getTotalesCargaM(String ic_proc_solic) throws NafinException {
		System.out.println("AutorizacionSolicitudBean::getTotalesCargaM(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List 				lRenglones		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (ig_numero_docto), SUM (fn_importe_docto), SUM (fn_importe_dscto)"   +
				"   FROM comtmp_solic_portal"   +
				"  WHERE ic_proc_solic = ?"   +
				"    AND cg_reg_ok = 'S'"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, new Long(ic_proc_solic).longValue());
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String registros 	= rs.getString(1)==null?"0":rs.getString(1);
				String importe 		= rs.getString(2)==null?"0":rs.getString(2);
				String importe_dsc 	= rs.getString(3)==null?"0":rs.getString(3);
				lRenglones.add(registros);
				lRenglones.add(importe);
				lRenglones.add(importe_dsc);
			}
			rs.close();ps.close();
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getTotalesCargaM(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getTotalesCargaM(S)");
		}
		return lRenglones;
	}//getTotalesCargaM

	public List getDetalleCarga(String ic_proc_solic) throws NafinException {
		System.out.println("AutorizacionSolicitudBean::getDetalleCarga(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List 				lRenglones		= new ArrayList();
		List 				lRegOK			= new ArrayList();
		List 				lRegError		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT   ic_solic_portal, 'Linea ' || ic_solic_portal || ': Documento ' || ig_numero_docto"   +
				"     FROM comtmp_solic_portal"   +
				"    WHERE ic_proc_solic = ?"   +
				"      AND cg_reg_ok = 'S'"   +
				" ORDER BY ic_solic_portal"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, new Long(ic_proc_solic).longValue());
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String mensajeOK = rs.getString(2)==null?"":rs.getString(2);
				lRegOK.add(mensajeOK);
			}
			rs.close();ps.close();

			qrySentencia =
				" SELECT   ic_solic_portal, cg_error"   +
				"     FROM comtmp_solic_portal"   +
				"    WHERE ic_proc_solic = ?"   +
				"      AND cg_reg_ok = 'N'"   +
				" ORDER BY ic_solic_portal"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, new Long(ic_proc_solic).longValue());
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String mensajeError = rs.getString(2)==null?"":rs.getString(2);
				lRegError.add(mensajeError);
			}
			rs.close();ps.close();
			lRenglones.add(lRegOK);
			lRenglones.add(lRegError);
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getDetalleCarga(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getDetalleCarga(S)");
		}
		return lRenglones;
	}//getDetalleCarga

//<<<IHJ<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	/*
	public HashMap evaluarArchivoDeOperacionesEmergentes(String rutaArchivo,String tipoDeRegistro)
		throws NafinException {
		System.out.println("AutorizacionSolicitudBean::evaluarArchivoDeOperacionesEmergentes(E)");
		HashMap 			registros					= new HashMap();
		StringBuffer	lineasSinError				= new StringBuffer();
		StringBuffer	lineasConError				= new StringBuffer();
		StringBuffer	numerosDeLineaSinError	= new StringBuffer();
		StringBuffer	numerosDeLineaConError	= new StringBuffer();
		int				numeroDeLinea				= 0;
		int				registrosConErrores		= 0;
		int				registrosSinErrores		= 0;
		String 			linea							= null;
		BigDecimal		montoTotal					= new BigDecimal("0.00");
		java.io.File   f								= null;
		BufferedReader br								= null;
		ArrayList		listaNumDocumento			= new ArrayList();
		boolean			esTabasco					= false;

		try {

			// Obtener la fecha de carga
			SimpleDateFormat 		simpleDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String 					fechaCarga = simpleDate.format(Fecha.getSysDate());

			// Arbir archivo
			f	= new java.io.File(rutaArchivo);
			br	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// procesar registros
			while ((linea=br.readLine())!=null){
				String 			[]campo 	= linea.split("\\|");
				StringBuffer	error 	= new StringBuffer();

				String	clienteSIRAC							= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():""); //Numerico
				String	tipoPrograma							= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim().toUpperCase():""); //Alfanumerico
				String	montoDeCapital							= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():""); //Numerico
				String	numTotalDeAmortizaciones			= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():""); //Numerico
				String	numTotalDeAmortizacionesDeGracia	= Comunes.quitaComitasSimplesyDobles(campo.length>4?campo[4].trim():""); //Numerico
				String	fechaDeFormalizacion					= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():""); //Fecha
				String	sucursal									= Comunes.quitaComitasSimplesyDobles(campo.length>6?campo[6].trim():""); //Num�rico
				String	moneda									= Comunes.quitaComitasSimplesyDobles(campo.length>7?campo[7].trim():""); //Num�rico
				String	numDocumento							= Comunes.quitaComitasSimplesyDobles(campo.length>8?campo[8].trim():""); //Num�rico
				String	periodicidadDePagoACapital			= Comunes.quitaComitasSimplesyDobles(campo.length>9?campo[9].trim():""); //Num�rico
				String	periodicidadDePagoDeIntereses		= Comunes.quitaComitasSimplesyDobles(campo.length>10?campo[10].trim():"");//Num�rico
				String	descripcionDeBienesYServicios		= Comunes.quitaComitasSimplesyDobles(campo.length>11?campo[11].trim():"");//Alfanum�rico
				String	tipoDeDocumento						= Comunes.quitaComitasSimplesyDobles(campo.length>12?campo[12].trim():"");//Num�rico
				String	domicilioPago							= Comunes.quitaComitasSimplesyDobles(campo.length>13?campo[13].trim():"");//Alfanum�rico
				String 	tasaSubsidio							= Comunes.quitaComitasSimplesyDobles(campo.length>14?campo[14].trim():"");//Num�rico
				String	tasaAlCliente							= Comunes.quitaComitasSimplesyDobles(campo.length>15?campo[15].trim():"");//Num�rico
				String	tasaFueraDePeriodoDeGracia			= Comunes.quitaComitasSimplesyDobles(campo.length>16?campo[16].trim():"");//Alfanum�rico

				// Recortar longitudes
				clienteSIRAC         				= ((clienteSIRAC.length()								>12)	?clienteSIRAC.substring(0,13)								:clienteSIRAC);
				tipoPrograma            			= ((tipoPrograma.length()								>1)	?tipoPrograma.substring(0,2)								:tipoPrograma);
				montoDeCapital       				= ((montoDeCapital.length()							>10)	?montoDeCapital.substring(0,11)							:montoDeCapital);
				numTotalDeAmortizaciones     		= ((numTotalDeAmortizaciones.length()				>5)	?numTotalDeAmortizaciones.substring(0,6)				:numTotalDeAmortizaciones);
				numTotalDeAmortizacionesDeGracia	= ((numTotalDeAmortizacionesDeGracia.length()	>5)	?numTotalDeAmortizacionesDeGracia.substring(0,6)	:numTotalDeAmortizacionesDeGracia);
				fechaDeFormalizacion   				= ((fechaDeFormalizacion.length()					>10)	?fechaDeFormalizacion.substring(0,11)					:fechaDeFormalizacion);
				sucursal                			= ((sucursal.length()									>10)	?sucursal.substring(0,11)									:sucursal);
				moneda                  			= ((moneda.length()										>2)	?moneda.substring(0,3)										:moneda);
				numDocumento            			= ((numDocumento.length()								>10)	?numDocumento.substring(0,11)								:numDocumento);
				periodicidadDePagoACapital			= ((periodicidadDePagoACapital.length()			>2)	?periodicidadDePagoACapital.substring(0,3)			:periodicidadDePagoACapital);
				periodicidadDePagoDeIntereses		= ((periodicidadDePagoDeIntereses.length()		>2)	?periodicidadDePagoDeIntereses.substring(0,3)		:periodicidadDePagoDeIntereses);
				descripcionDeBienesYServicios		= ((descripcionDeBienesYServicios.length()		>60)	?descripcionDeBienesYServicios.substring(0,61)		:descripcionDeBienesYServicios);
				tipoDeDocumento 						= ((tipoDeDocumento.length()							>3)	?tipoDeDocumento.substring(0,4)							:tipoDeDocumento);
				domicilioPago							= ((domicilioPago.length()								>60)	?domicilioPago.substring(0,61)							:domicilioPago);
				tasaSubsidio							= ((tasaSubsidio.length()								>8)	?tasaSubsidio.substring(0,9)								:tasaSubsidio);
				tasaAlCliente							= ((tasaAlCliente.length()								>8)	?tasaAlCliente.substring(0,9)								:tasaAlCliente);
				if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")){
						tasaFueraDePeriodoDeGracia	= ((tasaFueraDePeriodoDeGracia.length()			>8)	?tasaFueraDePeriodoDeGracia.substring(0,9)			:tasaFueraDePeriodoDeGracia);
				}
				// Verificar que no haya campos vacios
				if(clienteSIRAC.length() 							== 0)	error.append("El campo \"Cliente SIRAC\" es requerido, ");
				if(tipoPrograma.length() 							== 0)	error.append("El campo \"Tipo de Programa\" es requerido, "); else { esTabasco = (tipoPrograma.toUpperCase().equals("T"))?true:false; }
				if(montoDeCapital.length() 						== 0)	error.append("El campo \"Monto de Capital\" es requerido, ");
				if(numTotalDeAmortizaciones.length() 			== 0)	error.append("El campo \"Num. Total de Amortizaciones\" es requerido, ");
				if(numTotalDeAmortizacionesDeGracia.length() == 0)	error.append("El campo \"Num. Total de Amortizaciones de Gracia\" es requerido, ");
				if(fechaDeFormalizacion.length() 				== 0)	error.append("El campo \"Fecha de Formalizacion\" es requerido, ");
				//if(sucursal.length() 						 		== 0)	error.append("El campo \"Sucursal\" es requerido, "); // Campo no obligatorio
				if(moneda.length() 									== 0)	error.append("El campo \"Moneda\" es requerido, ");
				if(numDocumento.length() 							== 0)	error.append("El campo \"Num Documento\" es requerido, ");
				if(periodicidadDePagoACapital.length() 		== 0)	error.append("El campo \"Periodicidad de pago a Capital\" es requerido, ");
				if(periodicidadDePagoDeIntereses.length() 	== 0) error.append("El campo \"Periodicidad de pago de Intereses\" es requerido, ");
				if(descripcionDeBienesYServicios.length() 	== 0) error.append("El campo \"Descripcion de Bienes y Servicios\" es requerido, ");
				if(tipoDeDocumento.length() 						== 0)	error.append("El campo \"Tipo de Documento\" es requerido, ");
				if(domicilioPago.length() 							== 0)	error.append("El campo \"Domicilio Pago\" es requerido, ");
				if(tasaSubsidio.length() 							== 0)	error.append("El campo \"Tasa Subsidio\" es requerido, ");
				if(tasaAlCliente.length() 							== 0)	error.append("El campo \"Tasa al Cliente\" es requerido, ");
				if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO") && !esTabasco ){
					if(tasaFueraDePeriodoDeGracia.length() 	== 0)	error.append("El campo \"Tasa Fuera de Periodo de Gracia\" es requerido, ");
				}

				// Verificar longitudes
				if(clienteSIRAC.length() 							>12)	error.append("El campo \"Cliente SIRAC\" excede la longitud maxima permitida, ");
				if(tipoPrograma.length() 							>1)	error.append("El campo \"Tipo de Programa\" excede la longitud maxima permitida, ");
				if(montoDeCapital.length() 						>10)	error.append("El campo \"Monto de Capital\" excede la longitud maxima permitida, ");
				if(numTotalDeAmortizaciones.length() 			>5)	error.append("El campo \"Num. Total de Amortizaciones\" excede la longitud maxima permitida, ");
				if(numTotalDeAmortizacionesDeGracia.length() >5)	error.append("El campo \"Num. Total de Amortizaciones de Gracia\" excede la longitud maxima permitida, ");
				if(fechaDeFormalizacion.length() 				>10)	error.append("El campo \"Fecha de Formalizacion\" excede la longitud maxima permitida, ");
				if(sucursal.length() 								>10)	error.append("El campo \"Sucursal\" excede la longitud maxima permitida, ");
				if(moneda.length() 									>2)	error.append("El campo \"Moneda\" excede la longitud maxima permitida, ");
				if(numDocumento.length() 							>10)	error.append("El campo \"Num Documento\" excede la longitud maxima permitida, ");
				if(periodicidadDePagoACapital.length() 		>2)	error.append("El campo \"Periodicidad de pago a Capital\" excede la longitud maxima permitida, ");
				if(periodicidadDePagoDeIntereses.length() 	>2) 	error.append("El campo \"Periodicidad de pago de Intereses\" excede la longitud maxima permitida, ");
				if(descripcionDeBienesYServicios.length() 	>60) 	error.append("El campo \"Descripcion de Bienes y Servicios\" excede la longitud maxima permitida, ");
				if(tipoDeDocumento.length() 						>3)	error.append("El campo \"Tipo de Documento\" excede la longitud maxima permitida, ");
				if(domicilioPago.length() 							>60)	error.append("El campo \"Domicilio Pago\" excede la longitud maxima permitida, ");
				if(tasaSubsidio.length() 							>8)	error.append("El campo \"Tasa Subsidio\" excede la longitud maxima permitida, ");
				if(tasaAlCliente.length() 							>8)	error.append("El campo \"Tasa al Cliente\" excede la longitud maxima permitida, ");
				if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !esTabasco ){
						if(tasaFueraDePeriodoDeGracia.length() >8)	error.append("El campo \"Tasa Fuera de Periodo de Gracia\" excede la longitud maxima permitida, ");
				}

				// Validar tipo de caracteres
				boolean esFormatoFechaDeFormalizacionValido 				= false;
				boolean esFormatoMonedaValido					 				= false;
				boolean esFormatoTipoDeDocumentoValido		 				= false;
				boolean esFormatoTasaSubsidioValido							= false;
				boolean esFormatoTasaAlClienteValido						= false;
				boolean esFormatoPeriodicidadDePagoACapitalValido 		= false;
				boolean esFormatoPeriodicidadDePagoDeInteresesValido	= false;
				boolean esFormatoNumeroDeDocumentoValido					= false;
				boolean esFormatoTipoProgramaValido							= false;
				boolean esFormatoTasaFueraDePeriodoDeGraciaValido		= false;

				if(!clienteSIRAC.equals("") 		 			 			&& !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(clienteSIRAC))								error.append("El campo \"Cliente SIRAC\" tiene un numero invalido, ");
				if(!tipoPrograma.equals("") 		 			 			&& !Comunes.esAlfanumerico(tipoPrograma))																error.append("El campo \"Tipo de Programa\" tiene un caracter no permitido, ");                 else esFormatoTipoProgramaValido							= true;
				if(!montoDeCapital.equals("") 		 					&& !Comunes.esDecimalPositivoYDiferenteDeCero(montoDeCapital)) 								error.append("El campo \"Monto de Capital\" tiene un numero invalido, ");
				if(!numTotalDeAmortizaciones.equals("") 				&& !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(numTotalDeAmortizaciones))				error.append("El campo \"Num. Total de Amortizaciones\" tiene un numero invalido, ");
				if(tipoDeRegistro.equals("REGISTRO_VIGENTE")    	&& !numTotalDeAmortizacionesDeGracia.equals("") && !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(numTotalDeAmortizacionesDeGracia)) 	error.append("El campo \"Num. Total de Amortizaciones de Gracia\" tiene un numero invalido, ");
				if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !numTotalDeAmortizacionesDeGracia.equals("") && !Comunes.esNumeroEnteroPositivo(numTotalDeAmortizacionesDeGracia)) 						error.append("El campo \"Num. Total de Amortizaciones de Gracia\" tiene un numero invalido, ");
				if(!fechaDeFormalizacion.equals("") 					&& !Comunes.esFechaValida(fechaDeFormalizacion,"dd/MM/yyyy"))									error.append("El campo \"Fecha de Formalizacion\" no es valido, ");										else esFormatoFechaDeFormalizacionValido 				= true;
				if(!sucursal.equals("") 			 	  					&& !Comunes.esNumeroEnteroPositivo(sucursal))														error.append("El campo \"Sucursal\" tiene un numero invalido, ");
				if(!moneda.equals("") 			 	  						&& !Comunes.esNumeroEnteroPositivo(moneda))															error.append("El campo \"Moneda\" tiene un numero invalido, ");											else esFormatoMonedaValido					 				= true;
				if(!numDocumento.equals("") 			 	  				&& !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(numDocumento))								error.append("El campo \"Num Documento\" tiene un numero invalido, ");									else esFormatoNumeroDeDocumentoValido					= true;
				if(!periodicidadDePagoACapital.equals("") 			&& !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(periodicidadDePagoACapital))			error.append("El campo \"Periodicidad de pago a Capital\" tiene un numero invalido, ");			else esFormatoPeriodicidadDePagoACapitalValido 		= true;
				if(!periodicidadDePagoDeIntereses.equals("") 		&& !Comunes.esNumeroEnteroPositivoYDiferenteDeCero(periodicidadDePagoDeIntereses))		error.append("El campo \"Periodicidad de pago de Intereses\" tiene un numero invalido, ");		else esFormatoPeriodicidadDePagoDeInteresesValido	= true;
				//if(!descripcionDeBienesYServicios.equals("") 		&& !Comunes.esAlfanumerico(descripcionDeBienesYServicios," ������������.,-#&"))			error.append("El campo \"Descripcion de Bienes y Servicios\" tiene caracteres no permitidos, ");// Cualquier caracter es valido
				if(!tipoDeDocumento.equals("") 			 	  			&& !Comunes.esNumeroEnteroPositivo(tipoDeDocumento))												error.append("El campo \"Tipo de Documento\" tiene un numero invalido, ");								else esFormatoTipoDeDocumentoValido		 				= true;
				//if(!domicilioPago.equals("") 							&& !Comunes.esAlfanumerico(domicilioPago," ������������.,-#&"))								error.append("El campo \"Domicilio Pago\" tiene caracteres no permitidos, ");//Cualquier caracter es valido
				if(!tasaSubsidio.equals("") 			 	  				&& !Comunes.esDecimalPositivoYDiferenteDeCero(tasaSubsidio))									error.append("El campo \"Tasa Subsidio\" tiene un numero invalido, ");									else esFormatoTasaSubsidioValido							= true;
				if(!tasaAlCliente.equals("") 			 	  				&& !Comunes.esDecimalPositivoYDiferenteDeCero(tasaAlCliente))									error.append("El campo \"Tasa al Cliente\" tiene un numero invalido, ");								else esFormatoTasaAlClienteValido		 				= true;
				if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !esTabasco ){
					if(!tasaFueraDePeriodoDeGracia.equals("")			&& !Comunes.esDecimalPositivoYDiferenteDeCero(tasaFueraDePeriodoDeGracia))					error.append("El campo \"Tasa Fuera de Periodo de Gracia\" tiene al menos un digito no valido, ");
																																																																																		else esFormatoTasaFueraDePeriodoDeGraciaValido		= true;
				}

				// Validar logica de los datos
				String[]    tiposDePrograma						   = getClavesDeTipoDePrograma(tipoDeRegistro);//
				Collection  coleccionDeTiposDePrograma				= Arrays.asList(tiposDePrograma);
				String[] 	clavesDeMoneda 							= {"1","01"}; //, "54"
				Collection 	coleccionDeClavesDeMoneda 				= Arrays.asList(clavesDeMoneda);
				String[] 	tiposDeDocumento 							= {"1","2","3","4","5","6","7"};
				Collection 	coleccionDeTiposDeDocumento 			= Arrays.asList(tiposDeDocumento);
				String[]		periodicidadesPagoCapital				= {"7"};
				Collection	coleccionPeriodicidadesPagoCapital 	= Arrays.asList(periodicidadesPagoCapital);
				String[]		periodicidadesPagoInteres				= {"1"}; // Diario
				Collection	coleccionPeriodicidadesPagoInteres 	= Arrays.asList(periodicidadesPagoInteres);

				if(esFormatoTipoProgramaValido						&& !coleccionDeTiposDePrograma.contains(tipoPrograma))										error.append("El \"Tipo de Programa\" solo puede ser \"T\", ");
				if(tipoDeRegistro.equals("REGISTRO_VIGENTE")   	&& esFormatoFechaDeFormalizacionValido && !Comunes.esFechaMayorOIgualAlaFechaDeHoy(fechaDeFormalizacion,"dd/MM/yyyy"))		error.append("El campo \"Fecha de Formalizacion\" no puede ser menor a la fecha del dia de hoy, ");
				if(esFormatoMonedaValido 								&& !coleccionDeClavesDeMoneda.contains(moneda))													error.append("El campo \"Moneda\" posee una clave invalida, ");
				if(esFormatoPeriodicidadDePagoACapitalValido		&& !coleccionPeriodicidadesPagoCapital.contains(periodicidadDePagoACapital))			error.append("Solo se pueden capturar operaciones semanales en el campo \"Periodicidad de pago a Capital\", ");
				if(esFormatoPeriodicidadDePagoDeInteresesValido && !coleccionPeriodicidadesPagoInteres.contains(periodicidadDePagoDeIntereses))		error.append("Solo se pueden capturar operaciones diarias en el campo \"Periodicidad de pago de Intereses\", ");
				if(esFormatoTipoDeDocumentoValido 					&& !coleccionDeTiposDeDocumento.contains(tipoDeDocumento))									error.append("El campo \"Tipo de Documento\" posee una clave incorrecta, ");
				if(esFormatoTasaSubsidioValido						&& Comunes.excedeCantidadDeEnterosYDecimales(tasaSubsidio,3,4))							error.append("El campo \"Tasa Subsidio\" no debe poseer mas de 3 enteros, ni mas de 4 decimales, ");
				if(esFormatoTasaAlClienteValido	    				&& Comunes.excedeCantidadDeEnterosYDecimales(tasaAlCliente,3,4))							error.append("El campo \"Tasa al Cliente\" no debe poseer mas de 3 enteros, ni mas de 4 decimales, ");
				if(esFormatoTasaFueraDePeriodoDeGraciaValido		&& Comunes.excedeCantidadDeEnterosYDecimales(tasaFueraDePeriodoDeGracia,3,4))			error.append("El campo \"Tasa Fuera de Periodo de Gracia\" no debe poseer mas de 3 enteros, ni mas de 4 decimales, ");
				if(esFormatoNumeroDeDocumentoValido){
					if(listaNumDocumento.contains(numDocumento)){
						error.append("El Numero de Documento: "+numDocumento+" no puede estar repetido, ");
					}else{
						listaNumDocumento.add(numDocumento);
					}
				}

				// Validar que el numero de documento no este repetido
				numeroDeLinea++;
				if(error.length() != 0){
					error.replace(error.length()-2,error.length(),""); // Borrar la cadena: ", " que se encuentra al final
					lineasConError.append("L�nea "+numeroDeLinea + ": " + error.toString() + ".\n");
					numerosDeLineaConError.append(numeroDeLinea+",");
					registrosConErrores++;
				}else{
					lineasSinError.append("L�nea "+numeroDeLinea + ": Numero de Docto. "+ numDocumento+".\n");
					numerosDeLineaSinError.append(numeroDeLinea+",");
					montoTotal = montoTotal.add(new BigDecimal(montoDeCapital));
					registrosSinErrores++;
				}

			}

			// Quitar comas de las listas
			if(numerosDeLineaSinError.length()>0)
				numerosDeLineaSinError.delete(numerosDeLineaSinError.length()-1,numerosDeLineaSinError.length());
			if(numerosDeLineaConError.length()>0)
				numerosDeLineaConError.delete(numerosDeLineaConError.length()-1,numerosDeLineaConError.length());

			// Resultados
			registros.put("LINEAS_SIN_ERROR",				lineasSinError.toString());
			registros.put("LINEAS_CON_ERROR",				lineasConError.toString());
			registros.put("REGISTROS_PROCESADOS",			Integer.toString(numeroDeLinea));
			registros.put("REGISTROS_SIN_ERRORES",			Integer.toString(registrosSinErrores));
			registros.put("REGISTROS_CON_ERRORES",			Integer.toString(registrosConErrores));
			registros.put("MONTO_TOTAL",						Comunes.formatoDecimal(montoTotal.toPlainString(),2,true));// Solo se considera el monto de capital de los documentos sin errores
			registros.put("FECHA_HORA_CARGA",				fechaCarga);
			registros.put("NUMEROS_DE_LINEA_SIN_ERROR",	numerosDeLineaSinError.toString());
			registros.put("NUMEROS_DE_LINEA_CON_ERROR",	numerosDeLineaConError.toString());

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::evaluarArchivoDeOperacionesEmergentes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if( br != null) try{ br.close();}catch(Exception e){};
			System.out.println("AutorizacionSolicitudBean::evaluarArchivoDeOperacionesEmergentes(S)");
		}

		return registros;
	}
*/
	public String getArchivoConErroresDelRegOperacionesEmergentes(String strDirectorio, String rutaArchivo, String listaDocsConError)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::getArchivoConErroresDelRegOperacionesEmergentes(E)");
		String 				linea 	= null;
		java.io.File   	f 			= null;
		BufferedReader 	in			= null;
		BufferedWriter 	out 		= null;
		java.io.File 		archivo 	= null;

		if(listaDocsConError == null || listaDocsConError.equals(""))
			return null;

		String		[]lineasConError				= listaDocsConError.split(",");
		Collection 	coleccionLineasConError 	= Arrays.asList(lineasConError);

		try {
			// Crear archivo temporal donde se guardaran los archivos con errores
        	archivo = File.createTempFile(
			  					"ArchivoConErrores",
								".txt",
								new File(strDirectorio));
		  	out = new BufferedWriter(new FileWriter(archivo));

		  	System.out.println("File Name:      "+archivo.getName() +" was created"); // Debug info
        	System.out.println("Full File Path: "+archivo.getPath() +" was created"); // Debug info

		  	// Arbir archivo
			f	= new java.io.File(rutaArchivo);
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar registros
			int numeroLinea=1;
			while((linea=in.readLine())!=null){
				if(coleccionLineasConError.contains(Integer.toString(numeroLinea))){
					out.write(linea+"\n");
				}
				numeroLinea++;
			}

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::getArchivoConErroresDelRegOperacionesEmergentes(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close();}catch(Exception e){};
			if(out 	!= null )	try{out.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::getArchivoConErroresDelRegOperacionesEmergentes(S)");
		return archivo.getName();
	}

/*
	public String generaArchivoDeRegistroOperacionesEmergentes(String strDirectorio, String rutaArchivo, String listaDoctos)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::generaArchivoDeRegistroOperacionesEmergentes(E)");
		String 				linea 	= null;
		java.io.File   	f 			= null;
		BufferedReader 	in			= null;
		BufferedWriter 	out 		= null;
		java.io.File 		archivo 	= null;

		if(listaDoctos == null || listaDoctos.equals(""))
			return null;

		String		[]lineas							= listaDoctos.split(",");
		Collection 	coleccionLineas 				= Arrays.asList(lineas);

		try {
			// Crear archivo temporal donde se guardaran los archivos con errores
        	archivo = File.createTempFile(
			  					"ArchivoOperaciones",
								".txt",
								new File(strDirectorio));
		  	out = new BufferedWriter(new FileWriter(archivo));

		  	System.out.println("File Name:      "+archivo.getName() +" was created"); // Debug info
        	System.out.println("Full File Path: "+archivo.getPath() +" was created"); // Debug info

		  	// Arbir archivo
			f	= new java.io.File(rutaArchivo);
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar registros
			int numeroLinea=1;
			while((linea=in.readLine())!=null){
				if(coleccionLineas.contains(Integer.toString(numeroLinea))){
					String 			[]campo 	= linea.split("\\|");

					String	clienteSIRAC							= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():"");
					//String	tipoPrograma							= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim().toUpperCase():"");
					String	montoDeCapital							= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():"");
					String	numTotalDeAmortizaciones			= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():"0");
					String	numTotalDeAmortizacionesDeGracia	= Comunes.quitaComitasSimplesyDobles(campo.length>4?campo[4].trim():"");
					String	fechaDeFormalizacion					= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():"");
					String	sucursal									= Comunes.quitaComitasSimplesyDobles(campo.length>6?campo[6].trim():"");
					String	moneda									= Comunes.quitaComitasSimplesyDobles(campo.length>7?campo[7].trim():"");
					String	numDocumento							= Comunes.quitaComitasSimplesyDobles(campo.length>8?campo[8].trim():"");
					String	periodicidadDePagoACapital			= Comunes.quitaComitasSimplesyDobles(campo.length>9?campo[9].trim():"");
					String	periodicidadDePagoDeIntereses		= Comunes.quitaComitasSimplesyDobles(campo.length>10?campo[10].trim():"");
					String	descripcionDeBienesYServicios		= Comunes.quitaComitasSimplesyDobles(campo.length>11?campo[11].trim():"");
					String	tipoDeDocumento						= Comunes.quitaComitasSimplesyDobles(campo.length>12?campo[12].trim():"");
					String	domicilioPago							= Comunes.quitaComitasSimplesyDobles(campo.length>13?campo[13].trim():"");
					String 	tasaSubsidio							= Comunes.quitaComitasSimplesyDobles(campo.length>14?campo[14].trim():"");
					String	tasaAlCliente							= Comunes.quitaComitasSimplesyDobles(campo.length>15?campo[15].trim():"");

					String			emisor									= "";		// Emisor, no ha sido especificado aun
					String			tipoCredito								= "291"; //"112"; // Tipo Credito, no ha sido especificado aun
					String 			tasaUsuarioFinal						= "20";  // Tasa Usuario Final, no ha sido especificado aun
					String 			relacionMatematica					= "+";
					String			tablaAmortizacion						= "5";
					String 			sobretasAUF								= "3";   // Sobretas a UF,  no ha sido especificado aun.
					String			tasaIF									= "26";  //"33";   // Tasa IF, no ha sido especificado aun
					String 			fechaPrimerPagoCapital				= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",7*(Integer.parseInt(numTotalDeAmortizacionesDeGracia)+1));
					String			fechaPrimerPagoInteres				= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",7);//fechaPrimerPagoCapital;
					String   		diaPago									= fechaPrimerPagoCapital.split("\\/")[0];//fechaDeFormalizacion.split("\\/")[0];
					String   		fechaVencimientoDoc					= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",Integer.parseInt(numTotalDeAmortizaciones)*7);
					String   		fechaVencimientoDesc					= fechaVencimientoDoc;
					String			fechaEmiTituloCred					= fechaDeFormalizacion; // No ha sido especificado aun
					String			lugarFirma								= "MEXICO D.F"; // No ha sido especificado aun
					String 			ajusteAmortizacion					= "";
					String			tipoRenta								= "N";

					out.write(
									clienteSIRAC 						+'|'+
									sucursal								+'|'+
									moneda								+'|'+
									numDocumento						+'|'+
									montoDeCapital 					+'|'+
									montoDeCapital 					+'|'+
									emisor								+'|'+
									tipoCredito							+'|'+
									periodicidadDePagoACapital 	+'|'+
									periodicidadDePagoDeIntereses +'|'+
									descripcionDeBienesYServicios +'|'+
									tipoDeDocumento					+'|'+
									domicilioPago						+'|'+
									tasaUsuarioFinal					+'|'+
									relacionMatematica 				+'|'+
									sobretasAUF							+'|'+
									tasaIF								+'|'+
									numTotalDeAmortizaciones      +'|'+
									tablaAmortizacion					+'|'+
									fechaPrimerPagoCapital			+'|'+
									fechaPrimerPagoInteres			+'|'+
									diaPago								+'|'+
									fechaVencimientoDoc				+'|'+
									fechaVencimientoDesc				+'|'+
									fechaEmiTituloCred				+'|'+
									lugarFirma							+'|'+
									ajusteAmortizacion				+'|'+
									tipoRenta

								);

					// ---
					// Generar Amortizaciones
					int 			plazoDelCredito				= Integer.parseInt(numTotalDeAmortizaciones);
					int 			plazoConGracia					= Integer.parseInt(numTotalDeAmortizacionesDeGracia);
					String 		fechaAbono						= fechaDeFormalizacion;
					BigDecimal	iva								= new BigDecimal("0.15");
					BigDecimal  montoTotalDeCapital			= new BigDecimal(montoDeCapital).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal  saldoInicial					= new BigDecimal(montoDeCapital);
					BigDecimal  saldoFinal						= new BigDecimal(montoDeCapital);
					BigDecimal  subsidio							= new BigDecimal("0.00");
					//BigDecimal  saldoFinalAnterior			= saldoFinal;
					BigDecimal  tasaSubsidioAnualSinIVA 	= new BigDecimal(tasaSubsidio);
					BigDecimal	tasaClienteAnualSinIVA		= new BigDecimal(tasaAlCliente);
					BigDecimal	tasaClienteSemanalSinIVA	= tasaClienteAnualSinIVA.divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("7")).setScale(20,BigDecimal.ROUND_HALF_UP);//tasaClienteAnualSinIVA.multiply(new BigDecimal("7")).divide(new BigDecimal("360"),17,BigDecimal.ROUND_HALF_UP).setScale(4,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaClienteSemanalConIVA	= tasaClienteSemanalSinIVA.multiply(new BigDecimal("1.15")).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	dias								= new BigDecimal("7");
					BigDecimal	interes							= null;
					BigDecimal	ivaDelInteres					= null;
					BigDecimal  pagoCTE							= getPago(tasaClienteSemanalConIVA,new BigDecimal(plazoDelCredito- plazoConGracia),saldoInicial);
					BigDecimal	pagoDeCapital					= null;
					BigDecimal 	sumatoriaDePagosDeCapital  = new BigDecimal("0.00");


					boolean		esPeriodoDeGracia		= false;

					for(int periodo=1;periodo<=plazoDelCredito;periodo++){

						if(periodo<=plazoConGracia){
							esPeriodoDeGracia	= true;
						}else{
							esPeriodoDeGracia	= false;
						}

						// Calcular Fecha de Abono
						fechaAbono		= Fecha.sumaFechaDias(fechaAbono,"dd/MM/yyyy",7);
						// Calcular subsidio
						subsidio 		= getSubsidio(saldoInicial,tasaSubsidioAnualSinIVA,dias);
						// Calcular el saldo actual
						//saldoInicial 	= saldoFinalAnterior;

						if(!esPeriodoDeGracia){
							// Calcular intereses
							interes 					= getInteres(saldoInicial,tasaClienteAnualSinIVA,dias);
							// Calcular iva de los intereses
							ivaDelInteres 			= getIvaDelInteres(interes,iva);
							// Calcular Pago de Capital
							pagoDeCapital 			= pagoCTE.subtract(interes).subtract(ivaDelInteres);
							// Calcular Saldo Final
							saldoFinal 				= saldoInicial.subtract(pagoDeCapital);
						}

						// Para la ultima linea ajustar
						// 	Pago de Capital  = Pago de Capital + Saldo Final y
						//    Saldo Final = 0.00
						if(periodo == plazoDelCredito){
							pagoDeCapital 	= pagoDeCapital.add(saldoFinal).setScale(2,BigDecimal.ROUND_HALF_UP);
							saldoFinal		= new BigDecimal("0.00");
							sumatoriaDePagosDeCapital 	= sumatoriaDePagosDeCapital.add(pagoDeCapital);
							BigDecimal	diferencia		= montoTotalDeCapital.subtract(sumatoriaDePagosDeCapital);
							pagoDeCapital 					= pagoDeCapital.add(diferencia);
						}else{
							sumatoriaDePagosDeCapital  = 	sumatoriaDePagosDeCapital.add((pagoDeCapital == null?new BigDecimal("0.0"):pagoDeCapital.setScale(2,BigDecimal.ROUND_HALF_UP)));
						}

						out.write(
											  "|" + periodo
											  + "|" + Comunes.formatoDecimal( (pagoDeCapital == null?"0.0":pagoDeCapital.toPlainString()),2,false)
											+ "|" + fechaAbono
											+ "|" + (pagoDeCapital == null?"I":"A")
									);

						// Actualizar saldo final anterior
						saldoInicial	= saldoFinal;

					}
					out.write("\n");
					//---

				}
				numeroLinea++;
			}

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::generaArchivoDeRegistroOperacionesEmergentes(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close();}catch(Exception e){};
			if(out 	!= null )	try{out.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::generaArchivoDeRegistroOperacionesEmergentes(S)");
		return archivo.getName();
	}
*/
/*
	public String generaArchivoCSVConTablaDeAmortizaciones(String strDirectorio, String rutaArchivo, String listaDoctos,String tipoDeRegistro)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::generaArchivoCSVConTablaDeAmortizaciones(E)");
		String 				linea 							= null;
		java.io.File   	f 									= null;
		BufferedReader 	in									= null;
		BufferedWriter 	out 								= null;
		java.io.File 		archivo 							= null;
		String				nombreClienteSIRAC			= "";
		String				tasaFueraDePeriodoDeGracia	= "0.00";
		boolean				esTabasco						= false;

		if(listaDoctos == null || listaDoctos.equals(""))
			return null;

		String		[]lineas							= listaDoctos.split(",");
		Collection 	coleccionLineas 				= Arrays.asList(lineas);

		try {
			// Crear archivo temporal donde se guardaran los archivos con errores
        	archivo = File.createTempFile(
			  					"DetalleAmortizaciones",
								".csv",
								new File(strDirectorio));
		  	out = new BufferedWriter(new FileWriter(archivo));

		  	System.out.println("File Name:      "+archivo.getName() +" was created"); // Debug info
        	System.out.println("Full File Path: "+archivo.getPath() +" was created"); // Debug info

		  	// Arbir archivo
			f	= new java.io.File(rutaArchivo);
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar registros
			int numeroLinea=1;
			while((linea=in.readLine())!=null){

				if(coleccionLineas.contains(Integer.toString(numeroLinea))){
					String 			[]campo 	= linea.split("\\|");

					// Leer parametros
					String	clienteSIRAC							= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():"");
					String	tipoPrograma							= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim().toUpperCase():"");  esTabasco 	= (tipoPrograma.toUpperCase().equals("T"))?true:false;
					String	montoDeCapital							= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():"");
					String	numTotalDeAmortizaciones			= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():"0");
					String	numTotalDeAmortizacionesDeGracia	= Comunes.quitaComitasSimplesyDobles(campo.length>4?campo[4].trim():"");
					String	fechaDeFormalizacion					= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():"");
					//String	sucursal									= Comunes.quitaComitasSimplesyDobles(campo.length>6?campo[6].trim():"");
					//String	moneda									= Comunes.quitaComitasSimplesyDobles(campo.length>7?campo[7].trim():"");
					String	numDocumento							= Comunes.quitaComitasSimplesyDobles(campo.length>8?campo[8].trim():"");
					//String	periodicidadDePagoACapital			= Comunes.quitaComitasSimplesyDobles(campo.length>9?campo[9].trim():"");
					//String	periodicidadDePagoDeIntereses		= Comunes.quitaComitasSimplesyDobles(campo.length>10?campo[10].trim():"");
					//String	descripcionDeBienesYServicios		= Comunes.quitaComitasSimplesyDobles(campo.length>11?campo[11].trim():"");
					//String	tipoDeDocumento						= Comunes.quitaComitasSimplesyDobles(campo.length>12?campo[12].trim():"");
					//String	domicilioPago							= Comunes.quitaComitasSimplesyDobles(campo.length>13?campo[13].trim():"");
					String 	tasaSubsidio							= Comunes.quitaComitasSimplesyDobles(campo.length>14?campo[14].trim():"");
					String	tasaAlCliente							= Comunes.quitaComitasSimplesyDobles(campo.length>15?campo[15].trim():"");
					if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO") && !esTabasco ){
						tasaFueraDePeriodoDeGracia					= Comunes.quitaComitasSimplesyDobles(campo.length>16?campo[16].trim():"");
						nombreClienteSIRAC							= getNombreClienteSIRAC(clienteSIRAC);
					}

					// Generar parametros restantes
					//String	emisor									= "";	   // Emisor, no ha sido especificado aun
					//String	tipoCredito								= "291"; //"112"; // Tipo Credito, no ha sido especificado aun
					//String 	tasaUsuarioFinal						= "20";  // Tasa Usuario Final, no ha sido especificado aun
					//String 	relacionMatematica					= "+";
					//String	tablaAmortizacion						= "5";
					//String 	sobretasAUF								= "3";   // Sobretas a UF,  no ha sido especificado aun.
					//String	tasaIF									= "26";  //"33";   // Tasa IF, no ha sido especificado aun
					//String 	fechaPrimerPagoCapital				= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",7);
					//String	fechaPrimerPagoInteres				= fechaPrimerPagoCapital;
					//String   diaPago									= fechaDeFormalizacion.split("\\/")[0]; // Falta especificar
					//String   fechaVencimientoDoc					= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",Integer.parseInt(numTotalDeAmortizaciones)*7);
					//String   fechaVencimientoDesc					= fechaVencimientoDoc;
					//String	fechaEmiTituloCred					= fechaDeFormalizacion; // No ha sido especificado aun
					//String	lugarFirma								= "MEXICO D.F"; // No ha sido especificado aun
					//String 	ajusteAmortizacion					= "";
					//String	tipoRenta								= "N";

					// Imprime encabezado
					if(tipoDeRegistro.equals("REGISTRO_VIGENTE")){
						out.write(
										"Numero de Documento:"	+ "," +
										numDocumento				+ "\n"
									);
					}else	if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")){
						out.write(
										"Cliente SIRAC:"			+ "," +
									 	clienteSIRAC 				+ "," +
										"Numero de Documento:"	+ "," +
										numDocumento				+ "\n"
									);
						out.write(
										"Nombre Cliente SIRAC:"	+ "," +
										 nombreClienteSIRAC		+ "\n"
									);
					}

					out.write(
									"PERIODO,"					+
									"FECHA ABONO,"				+
									"SALDO INICIAL,"			+
									"SUBSIDIO,"					+
									"INTERESES GENERADOS," 	+
									"IVA INTERESES,"			+
									"PAGO DE CAPITAL,"		+
									"PAGO CTE,"					+
									"SALDO FINAL"				+
									"\n"
								);

					// Generar Amortizaciones

					int 			plazoDelCredito						= Integer.parseInt(numTotalDeAmortizaciones);
					int 			plazoConGracia							= Integer.parseInt(numTotalDeAmortizacionesDeGracia);
					String 		fechaAbono								= fechaDeFormalizacion;
					BigDecimal	iva										= tipoPrograma.equals("T") || tipoPrograma.equals("t")?new BigDecimal("0.15"):new BigDecimal("0.10");
					BigDecimal  montoTotalDeCapital					= new BigDecimal(montoDeCapital).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal  saldoInicial							= new BigDecimal(montoDeCapital);
					BigDecimal  saldoFinal								= new BigDecimal(montoDeCapital);
					BigDecimal  subsidio									= new BigDecimal("0.00");
					//BigDecimal  saldoFinalAnterior					= saldoFinal;
					BigDecimal  tasaSubsidioAnualSinIVA 			= new BigDecimal(tasaSubsidio);
					BigDecimal	tasaClienteAnualSinIVA				= new BigDecimal(tasaAlCliente);
					//BigDecimal	tasaFueraDePeriodoDeGraciaSinIVA	= (tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO") && !esTabasco )?new BigDecimal(tasaFueraDePeriodoDeGracia):new BigDecimal("0.00");
					BigDecimal	tasaClienteSemanalSinIVA			= tasaClienteAnualSinIVA.divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("7")).setScale(20,BigDecimal.ROUND_HALF_UP);//tasaClienteAnualSinIVA.multiply(new BigDecimal("7")).divide(new BigDecimal("360"),17,BigDecimal.ROUND_HALF_UP).setScale(4,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaClienteSemanalConIVA			= tasaClienteSemanalSinIVA.multiply((new BigDecimal("1")).add(iva)).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	dias										= new BigDecimal("7");
					BigDecimal	interes									= null;
					BigDecimal	ivaDelInteres							= null;
					BigDecimal  pagoCTE									= null;//;getPago(tasaClienteSemanalConIVA,new BigDecimal(plazoDelCredito- plazoConGracia),saldoInicial);
					BigDecimal	pagoDeCapital							= null;
					BigDecimal 	sumatoriaDePagosDeCapital  		= new BigDecimal("0.00");

					// Calcular otras tasas
					BigDecimal	tasaSubsidioSemanal					= (new BigDecimal(tasaSubsidio)).divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("7")).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaClienteSemanal					= (new BigDecimal(tasaAlCliente)).divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("7")).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaClienteSemanal2					= (new BigDecimal(tasaAlCliente)).divide(new BigDecimal("52"),20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaClienteSemanal2ConIVA			= tasaClienteSemanal2.multiply((new BigDecimal("1")).add(iva)).setScale(20,BigDecimal.ROUND_HALF_UP);
					BigDecimal	tasaSubsidioSemanalSinGracia		= (new BigDecimal(tasaFueraDePeriodoDeGracia)).divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("7")).setScale(20,BigDecimal.ROUND_HALF_UP);

					if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO") && !esTabasco ){
						pagoCTE		= getPago(tasaClienteSemanal2ConIVA,new BigDecimal(plazoDelCredito-plazoConGracia),saldoInicial);
					}else{
						pagoCTE		= getPago(tasaClienteSemanalConIVA, new BigDecimal(plazoDelCredito-plazoConGracia),saldoInicial);
					}

					boolean		esPeriodoDeGracia		= false;

					for(int periodo=1;periodo<=plazoDelCredito;periodo++){

						if(periodo<=plazoConGracia){
							esPeriodoDeGracia	= true;
						}else{
							esPeriodoDeGracia	= false;
						}

						// Calcular Fecha de Abono
						fechaAbono		= Fecha.sumaFechaDias(fechaAbono,"dd/MM/yyyy",7);

						// Calcular subsidio
						if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !esTabasco  ){
							subsidio 		= tasaSubsidioSemanal.multiply(new BigDecimal("0.01")).multiply(saldoInicial).setScale(20,BigDecimal.ROUND_HALF_UP);
						}else{
							subsidio 		= getSubsidio(saldoInicial,tasaSubsidioAnualSinIVA,dias);
						}
						// Calcular el saldo actual
						//saldoInicial 	= saldoFinalAnterior;

						if(!esPeriodoDeGracia){
							// Calcular subsidio si es un registro extemporaneo
							if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !esTabasco ){
								subsidio 	= tasaSubsidioSemanalSinGracia.multiply(new BigDecimal("0.01")).multiply(saldoInicial).setScale(20,BigDecimal.ROUND_HALF_UP);
							}

							// Calcular intereses
							if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")  && !esTabasco ){
								interes 		= tasaClienteSemanal.multiply(new BigDecimal("0.01")).multiply(saldoInicial).setScale(20,BigDecimal.ROUND_HALF_UP);
							}else{
								interes 		= getInteres(saldoInicial,tasaClienteAnualSinIVA,dias);
							}

							// Calcular iva de los intereses
							ivaDelInteres 			= getIvaDelInteres(interes,iva);
							// Calcular Pago de Capital
							pagoDeCapital 			= pagoCTE.subtract(interes).subtract(ivaDelInteres);
							// Calcular Saldo Final
							saldoFinal 				= saldoInicial.subtract(pagoDeCapital);

							// Para la ultima linea ajustar
							// 	Pago de Capital  = Pago de Capital + Saldo Final y
							//    Saldo Final      = 0.00
							if(periodo == plazoDelCredito){
								pagoDeCapital 					= pagoDeCapital.add(saldoFinal).setScale(2,BigDecimal.ROUND_HALF_UP);
								saldoFinal						= new BigDecimal("0.00");
								sumatoriaDePagosDeCapital 	= sumatoriaDePagosDeCapital.add(pagoDeCapital);
								BigDecimal	diferencia		= montoTotalDeCapital.subtract(sumatoriaDePagosDeCapital);
								pagoDeCapital 					= pagoDeCapital.add(diferencia);
							}else{
								sumatoriaDePagosDeCapital  = 	sumatoriaDePagosDeCapital.add(pagoDeCapital.setScale(2,BigDecimal.ROUND_HALF_UP));
							}

						}

						if(tipoDeRegistro.equals("REGISTRO_EXTEMPORANEO")){
							out.write(
											periodo						+ "," +
											fechaAbono					+ "," +
											"\""+Comunes.formatoDecimal(saldoInicial.toPlainString(),	2,true)+ "\"," 				 +
											"\""+Comunes.formatoDecimal(subsidio.toPlainString(),		2,true)+ "\"," 				 +
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(interes.toPlainString(),			2,true)+ "\",")+
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(ivaDelInteres.toPlainString(),	2,true)+ "\",")+
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(pagoDeCapital.toPlainString(),	2,true)+ "\",")+
											(esPeriodoDeGracia?"-,":"\"$"+ Comunes.formatoDecimal(pagoCTE.toPlainString(),		2,true)+ "\",")+
											"\""+Comunes.formatoDecimal(saldoFinal.toPlainString(),			2,true)+ "\""				 +
											"\n"
									);
						}else{
									out.write(
											periodo						+ "," +
											fechaAbono					+ "," +
											"\""+Comunes.formatoDecimal(saldoInicial.toPlainString(),	2,true)+ "\"," 				 +
											"\""+Comunes.formatoDecimal(subsidio.toPlainString(),		2,true)+ "\"," 				 +
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(interes.toPlainString(),			2,true)+ "\",")+
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(ivaDelInteres.toPlainString(),	2,true)+ "\",")+
											(esPeriodoDeGracia?"-,":"\""		+ Comunes.formatoDecimal(pagoDeCapital.toPlainString(),	2,true)+ "\",")+
											(esPeriodoDeGracia?"$0.00,":"\"$"+ Comunes.formatoDecimal(pagoCTE.toPlainString(),		2,true)+ "\",")+
											"\""+Comunes.formatoDecimal(saldoFinal.toPlainString(),			2,true)+ "\""				 +
											"\n"
									);
						}

						// Actualizar saldo final anterior
						saldoInicial	= saldoFinal;

					}
					// Imprimir
					out.write("\n"+"\n");
				}
				numeroLinea++;
			}

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::generaArchivoCSVConTablaDeAmortizaciones(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close(); }catch(Exception e){};
			if(out 	!= null )	try{out.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::generaArchivoCSVConTablaDeAmortizaciones(S)");
		return archivo.getName();
	}
*/
	/*********************************************************************************************
	*
	*	    ArrayList getPendientes()
	*
	*********************************************************************************************/
    // Agregado 27/03/2008	Victor Lopez

	 public ArrayList getPendientes(String ic_if, String fecha_de, String fecha_a)
	 throws NafinException{
	 System.out.println("AutorizacionSolicitudBean::getPendientes(E)");
	 AccesoDB 		con				= new AccesoDB();
	 ArrayList 		pendientes		= new ArrayList();
	 HashMap 		registros 		= new HashMap();
	 String			qrySentencia;
	 PreparedStatement ps			= null;
	 ResultSet rs						= null;

	 try {
		con.conexionDB();
			qrySentencia =
				"	SELECT   ig_numero_prestamo AS numero_prestamo, fn_importe_docto AS monto, " +
				"	TO_CHAR (df_operacion, 'dd/mm/yyyy') AS fecha " +
				"	FROM com_solic_portal sol " +
				"	WHERE ic_if = ? " +
				"	AND ig_numero_prestamo IS NOT NULL " +
				"	AND df_operacion IS NOT NULL " +
				"	AND ic_estatus_solic = 3 " +
				"	AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy') " +
				"	AND df_operacion < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
				"	AND ic_solic_portal NOT IN (" +
				"		SELECT ic_solic_portal " +
            "     FROM com_solic_portal_det " +
            "     WHERE ic_solic_portal = sol.ic_solic_portal " +
            "     AND ig_numero_prestamo = sol.ig_numero_prestamo) " +
				"		ORDER BY df_operacion ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, ic_if);
				ps.setString(2, fecha_de);
				ps.setString(3, fecha_a);
				rs = ps.executeQuery();
				while(rs.next()){
					registros = new HashMap();
					registros.put("numero_prestamo",(rs.getString("NUMERO_PRESTAMO")==null)?"":rs.getString("NUMERO_PRESTAMO"));
					registros.put("monto",Comunes.formatoDecimal(rs.getDouble("MONTO"),2));
					registros.put("fecha",(rs.getString("FECHA")==null)?"":rs.getString("FECHA"));
					pendientes.add(registros);
				}
	 }

	 catch (Exception e)
	 {	   System.out.println("AutorizacionSolicitudBean::getPendientes(E)" +e);
	       e.printStackTrace();
		   //throw new NafinException("SIST0001");
	 }
	 finally{
	 	  if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getPendientes(S)");
	 }
	 return  pendientes;//consulta;
	 }//fin getPendientes

	/*********************************************************************************************
	*
	*	    String setDatosTmpDetSolic()
	*
	*********************************************************************************************/
    // Agregado 04/04/2008	Victor Lopez

	public String setDatosTmpDetSolic(String ic_proc_carga, List lRegistros)throws NafinException{
		System.out.println("AutorizacionSolicitud::setDatosTmpDetSolic(E)");
		AccesoDB 			con				= new AccesoDB();
		boolean				bOk				= true;
		Registros			registros;
		String				qrySentencia = "";
		List 				lVarBind = null;
		try {
			con.conexionDB();

			if("0".equals(ic_proc_carga)) {
				qrySentencia =
					" SELECT seq_comtmp_solic_portal_det.NEXTVAL ic_proc_solic, 'AutorizacionSolicitud::setDatosTmpDetSolic()' "   +
					"   FROM DUAL"  ;
				registros = con.consultarDB(qrySentencia);
				if(registros.next()) {
					ic_proc_carga = registros.getString("ic_proc_solic");
				}
			}

			String ic_solic_portal_det  = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_solic_portal_det) + 1, 1) ic_solic_portal_det, 'AutorizacionSolicitud::setDatosTmpDetSolic()'"   +
				"   FROM comtmp_solic_portal_det"   +
				"  WHERE ic_proc_carga = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				ic_solic_portal_det = registros.getString("ic_solic_portal_det");
			}

			qrySentencia =
				" INSERT INTO comtmp_solic_portal_det"   +
				"             (ic_proc_carga, ic_solic_portal_det, ig_numero_prestamo, cg_nombre, cg_segnombre, "   +
				"              cg_appat, cg_apmat, cg_rfc, df_nacimiento, fn_importe, " +
				"					cg_concepto, cg_estado, cg_municipio, cg_status, cg_error)"   +
				"      VALUES (?, ?, ?, ?, ?,"   +
				"              ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'), ?,"  +
				"					?, ?, ?, ?, ?)"  ;

			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(new Long(ic_solic_portal_det));

			String 	campo;
			String 	layout[]		= {"Numero de prestamo", "Nombre", "Nombre 2", "Apellido Paterno", "Apellido Materno", "RFC", "Fecha de Nacimiento", "Importe", "Concepto", "Estado", "Municipio"};
			int 	longcampo[] 	= {9, 20, 20, 20, 20, 15, 10, 15, 20, 15, 20, 20};
			String 	cg_estatus		= "S",
					cg_error		= "";
			if(lRegistros.size()!=layout.length) {
				cg_estatus 	= "N";
				cg_error 	= "El registro no coincide con el layout. ";
			}

			for(int i=0;i<layout.length;i++) {
				campo = (lRegistros.size()>=(i+1))?lRegistros.get(i).toString().trim():"";
				if(campo.length()>longcampo[i]) {
					cg_estatus	= "N";
					cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampo[i]+"). ";
					campo = campo.substring(0, longcampo[i]);
				}
				if(i==0){
					Pattern p = Pattern.compile("[^0-9]");
					Matcher m = p.matcher(campo);
						if(m.find())
						{
							lVarBind.add("");
						}
						else
						{
							lVarBind.add(campo);
						}
				}
				else if(i==6)
				{
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					df.setLenient(false);
					ParsePosition pos = new ParsePosition(0);

					String strDate = campo.toString();

					Date date = df.parse(strDate, pos);

					// Checar los errores que pueden suceder
					if ((date == null) || (pos.getErrorIndex() != -1)) {
						lVarBind.add("");
					}
					else
					{
						lVarBind.add(campo);
					}
				}
				else if(i==7)
				{
					String monto = Comunes.suprimeCaracteres(campo, ",");
					Pattern p = Pattern.compile("[^0-9\\.]");
					Matcher m = p.matcher(monto);
						if(m.find())
						{
							lVarBind.add("");
						}
						else
						{
							lVarBind.add(monto);
						}
				}
				else{
				lVarBind.add(campo);
				}
			}

			lVarBind.add(cg_estatus);
			lVarBind.add(cg_error);
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

			} catch(Exception e) {
			System.out.println("AutorizacionSolicitud::setDatosTmpDetSolic(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitud::setDatosTmpDetSolic(S)");
		}
		return ic_proc_carga;
	}//setDatosTmpDetSolic

	/*********************************************************************************************
	*
	*	    Registros getEstatisCargaSolicDet()
	*
	*********************************************************************************************/
    // Agregado 07/04/2008	Victor Lopez
	public Registros getEstatisCargaSolicDet(String ic_proc_carga) throws NafinException {
		System.out.println("AutorizacionSolicitud::getEstatisCargaSolicDet(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0) ig_num_procesados, 'AutorizacionSolicitud::getEstatisCargaSolicDet()'"   +
				"   FROM bit_tmp_solic_portal_det"   +
				"  WHERE ic_proc_carga = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitud::getEstatisCargaSolicDet(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitud::getEstatisCargaSolicDet(S)");
		}
		return registros;
	}//getEstatusCargaCLABE


	 /*********************************************************************************************
	*
	*	    Registros getRegistrosValidosSolicDetalle()
	*
	*********************************************************************************************/
	 public Registros getRegistrosValidadosSolicDetalle(String ic_proc_carga, String cg_estatus, String ordenar) throws NafinException {
		System.out.println("AutorizacionSolicitud::getRegistrosValidosSolicDetalle(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				"  SELECT spd.ic_solic_portal_det, spd.ig_numero_prestamo, spd.cg_nombre, spd.cg_segnombre, spd.cg_appat, "   +
				"  spd.cg_apmat, spd.cg_rfc, TO_CHAR(spd.df_nacimiento,'dd/mm/yyyy') df_nacimiento, spd.fn_importe, spd.cg_concepto, "   +
				"  spd.cg_estado, spd.cg_municipio, spd.cg_status, spd.cg_error "   +
				"  FROM comtmp_solic_portal_det spd "   +
				"  WHERE  spd.ic_proc_carga = ? "   +
				"  AND spd.cg_status = ? "   +
				"  ORDER BY spd." + ordenar;

			System.out.println(qrySentencia);
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(cg_estatus);
			System.out.println(lVarBind);
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitud::getRegistrosValidosSolicDetalle(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitud::getRegistrosValidosSolicDetalle(S)");
		}
		return registros;
	}//getRegistrosValidadosSolicDetalle

	/*********************************************************************************************
	*
	*	    void setCargaDetalleMasiva()
	*
	*********************************************************************************************/
	 	public void setCargaDetalleMasiva(Registros registros) throws NafinException {
		System.out.println("AutorizacionSolicitud::setCargaDetalleMasiva(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			datos 			= new Registros();
		ResultSet existe = null;
		String				qrySentencia;
		List 					lVarBind;
		boolean				bOk				= true;
		String				num_prestamo = "", importe = "";
		String qryConsulta = "";
		int num=0;
		try {
			con.conexionDB();
			while(registros.next()) {
			num_prestamo = registros.getString("ig_numero_prestamo");
			importe = registros.getString("fn_importe");
				qrySentencia = "SELECT ic_solic_portal " +
						"   from com_solic_portal " +
						"   where ig_numero_prestamo = " + num_prestamo ;
				//validar que no este ya el prestamo por alguna actualizacion de la pagina
				qryConsulta = "SELECT COUNT(*) FROM com_solic_portal_det " +
								" WHERE ig_numero_prestamo = " + num_prestamo +
								" AND fn_importe = " + importe;
				existe = con.queryDB(qryConsulta);
				while(existe.next()){
					num = existe.getInt(1);
				}
				if(num==0){//if((num==0&&num2==1)||(num==1&&num2==0)){
					datos = con.consultarDB(qrySentencia);
					if(datos.next()) {
					qrySentencia =
						" INSERT INTO com_solic_portal_det"   +
						"             (ic_solic_portal_det, ic_solic_portal, ig_numero_prestamo, cg_nombre,"   +
						"              cg_segnombre, cg_appat, cg_apmat, cg_rfc, df_nacimiento,"   +
						"              fn_importe, cg_concepto, cg_estado, cg_municipio) "   +
						"      VALUES (seq_com_solic_det.NEXTVAL, ?, ?, ?,"   +
						"              ?, ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'),"   +
						"              ?, ?, ?, ?)"  ;

					lVarBind = new ArrayList();
					lVarBind.add(new Long(datos.getString("ic_solic_portal")));
					lVarBind.add(new Long(num_prestamo));
					lVarBind.add(registros.getString("cg_nombre"));
					lVarBind.add(registros.getString("cg_segnombre"));
					lVarBind.add(registros.getString("cg_appat"));
					lVarBind.add(registros.getString("cg_apmat"));
					lVarBind.add(registros.getString("cg_rfc"));
					lVarBind.add(registros.getString("df_nacimiento"));
					lVarBind.add(new Double(registros.getString("fn_importe")));
					lVarBind.add(registros.getString("cg_concepto"));
					lVarBind.add(registros.getString("cg_estado"));
					lVarBind.add(registros.getString("cg_municipio"));
					System.out.println("Inserta: "+lVarBind);
					con.ejecutaUpdateDB(qrySentencia, lVarBind);
					}
				}
			}//while(registros.next())
		} catch(Exception e) {
			bOk = false;
			System.out.println("AutorizacionSolicitud::setCargaDetalleMasiva(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("AutorizacionSolicitud::setCargaDetalleMasiva(S)");
		}
	}//setCargaDetalleMasiva

	/*********************************************************************************************
	*
	*	    List detalleComprobarMonto()
	*
	*********************************************************************************************/
	public List detalleComprobarMonto(String ic_proc_carga) throws NafinException
	{
		AccesoDB 			con				= new AccesoDB();
		String 				qrySentencia	= null;
		String				qrySumatoria	= null;
		String 				qryMontos		= null;
		boolean				bOk				= false;
		PreparedStatement ps      = null;
		ResultSet rs              = null;
		HashMap campos     			= new HashMap();
		ArrayList list 				= new ArrayList();
		ArrayList sumatorias			= new ArrayList();
		ArrayList montos				= new ArrayList();
		ArrayList noAprobados		= new ArrayList();
		String prestamo = "", monto1 = "", monto2 = "";
		Iterator ite, ite1, ite2;
		System.out.println("AutorizacionSolicitud::detalleComprobarMonto(E)");
		try {
			con.conexionDB();
			//consulta para obtener los numeros de prestamo involucrados en la carga
			qrySentencia = " SELECT  distinct ig_numero_prestamo " +
									" FROM comtmp_solic_portal_det  " +
									" WHERE ic_proc_carga = " + ic_proc_carga +
									" AND cg_status = 'S' ";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next())
			{
				list.add(rs.getString(1));
			}
			ps.close();rs.close();

			ite = list.iterator();
			while(ite.hasNext()){
			prestamo = (String)ite.next();
			//esta consulta obtiene el monto sumado del detalle
			qrySumatoria = " SELECT SUM(fn_importe) as SUMA " +
								" FROM comtmp_solic_portal_det " +
								"	WHERE ig_numero_prestamo = " + prestamo +
								"		AND cg_status = 'S' " +
								"		AND fn_importe IS NOT NULL " +
								"		AND ic_proc_carga= " + ic_proc_carga ;
			ps = con.queryPrecompilado(qrySumatoria);
			rs = ps.executeQuery();
			ps.clearParameters();
				while(rs.next())
				{
					sumatorias.add(rs.getString(1));
				}
			//con esto se obtiene el monto correcto que se debe cargar
			qryMontos = " SELECT fn_importe_docto " +
							" FROM com_solic_portal " +
							" WHERE ig_numero_prestamo = " + prestamo;
			ps = con.queryPrecompilado(qryMontos);
			rs = ps.executeQuery();
			ps.clearParameters();
				while(rs.next())
				{
					montos.add(rs.getString(1));
				}

			}
			ps.close();rs.close();

			ite = list.iterator();
			ite1 = sumatorias.iterator();
			ite2 = montos.iterator();

			//se llena la lista con los prestamos que no cumplen con la condicion del monto
			while(ite1.hasNext() && ite2.hasNext() && ite.hasNext())
			{
				prestamo = (String)ite.next();
				monto1 = (String)ite1.next();
				monto2 = (String)ite2.next();
				if(!monto1.equals(monto2))
				{
					bOk = true;
					campos = new HashMap();
					campos.put("MONTOREGISTRADO", monto1);
					campos.put("MONTOTOTAL", monto2);
					campos.put("PRESTAMO", prestamo);
					noAprobados.add(campos);

					String qryActualizaDetalle = " UPDATE comtmp_solic_portal_det " +
														" SET cg_status = 'N' " +
														" WHERE ic_proc_carga = " + ic_proc_carga +
														" AND ig_numero_prestamo = " + prestamo +
														" AND cg_status = 'S' " ;
					System.out.println(qryActualizaDetalle);

					ps = con.queryPrecompilado(qryActualizaDetalle);
					ps.executeUpdate();
					con.terminaTransaccion(true);
					ps.close();

				}
			}

			} catch(Exception e) {
			bOk = false;
			System.out.println("AutorizacionSolicitud::detalleComprobarMonto(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("AutorizacionSolicitud::detalleComprobarMonto(S)");
		}
		return bOk?noAprobados:null;
	}//detalleComprobarMonto


	private BigDecimal getSubsidio(BigDecimal saldoActual,BigDecimal tasaSubsidioAnualSinIVA, BigDecimal dias){
		BigDecimal subsidio = new BigDecimal("0.00");

		try {
			subsidio = saldoActual.multiply(tasaSubsidioAnualSinIVA.multiply(new BigDecimal("0.01")));
			subsidio	= subsidio.divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP);// 17
			subsidio = subsidio.setScale(20,BigDecimal.ROUND_HALF_UP);//2
			subsidio = subsidio.multiply(dias);
		}catch(Exception e){
			return new BigDecimal("0.00");
		}

		return subsidio.setScale(20,BigDecimal.ROUND_HALF_UP); // return subsidio;
	}

	private BigDecimal getInteres(BigDecimal saldoActual,BigDecimal tasaSubsidioAnualSinIVA, BigDecimal dias){
		BigDecimal interes = new BigDecimal("0.00");

		try {
			interes  = saldoActual.multiply(tasaSubsidioAnualSinIVA.multiply(new BigDecimal("0.01")));
			interes	= interes.divide(new BigDecimal("360"),20,BigDecimal.ROUND_HALF_UP);// 17
			interes  = interes .multiply(dias);
			interes  = interes.setScale(20,BigDecimal.ROUND_HALF_UP);//2
		}catch(Exception e){
			return new BigDecimal("0.00");
		}

		return interes;
	}

	private BigDecimal getIvaDelInteres(BigDecimal interes,BigDecimal iva){
		if(interes == null)
			return new BigDecimal("0.00");
		return interes.multiply(iva).setScale(20,BigDecimal.ROUND_HALF_UP);// 2
	}

	private BigDecimal getPago(BigDecimal tasa, BigDecimal  numPagos, BigDecimal capital){
		BigDecimal pago 	= new BigDecimal("0.00");
		BigDecimal uno 	= new BigDecimal("1");

		try {
			tasa=tasa.multiply(new BigDecimal("0.01"));
			BigDecimal denominador 	= tasa.add(uno);
			denominador 				= power(denominador,numPagos);
			denominador 				= denominador.subtract(uno);
			BigDecimal cociente 		= tasa.divide(denominador,20,BigDecimal.ROUND_HALF_UP);// 17
			BigDecimal coeficiente 	= cociente.add(tasa);
			pago 							= coeficiente.multiply(capital);
			pago							= pago.setScale(20,BigDecimal.ROUND_HALF_UP);// 12

		} catch(Exception e){
			return new BigDecimal("0.00");
		}

		return pago;
	}

	private BigDecimal power(BigDecimal base, BigDecimal exponente){ // Nota: solo funciona para exponentes positivos
		BigDecimal resultado 	= new BigDecimal("1");

		try {
			// Extraer parte entera
			exponente = new BigDecimal(exponente.toBigInteger().toString());

			if(exponente.compareTo(new BigDecimal("0")) == 0)
				return new BigDecimal("1");
			if(exponente.compareTo(new BigDecimal("0")) == -1)
				exponente = exponente.multiply(new BigDecimal("-1"));

			for(BigDecimal cuenta=new BigDecimal("0"); exponente.compareTo(cuenta) == 1; cuenta = cuenta.add(new BigDecimal("1"))){
			    resultado = resultado.multiply(base);
			}
		}catch(Exception e){
			return new BigDecimal("0.00");
		}

		return resultado;
	}

	public String getCSVconDetallePorRegistroArchivosEnviados(String path,String numPrestamo, String icSolicPortal) 
			throws Exception{

		String 	query  =
								"SELECT "  +
								"	IC_SOLIC_PORTAL_DET, "  +
								"	IC_SOLIC_PORTAL, "  +
								"	IG_NUMERO_PRESTAMO, "  +
								"	CG_NOMBRE, "  +
								"	CG_SEGNOMBRE, "  +
								"	CG_APPAT, "  +
								"	CG_APMAT, "  +
								"	CG_RFC, "  +
								"	TO_CHAR(DF_NACIMIENTO,'dd/mm/yyyy') DF_NACIMIENTO, "  +
								"	'\"'||TO_CHAR(FN_IMPORTE,'99,999,999,999,999,999.99')||'\"' AS FN_IMPORTE, "  +
								"	CG_CONCEPTO, "  +
								"	CG_ESTADO, "  +
								"	CG_MUNICIPIO "  +
								"FROM "  +
								"  COM_SOLIC_PORTAL_DET "  +
								"WHERE "  +
								"	IC_SOLIC_PORTAL        = ?  AND "  +
								"	IG_NUMERO_PRESTAMO     = ?      ";

		ArrayList	lVarBind			= new ArrayList();
		lVarBind.add(icSolicPortal);
		lVarBind.add(numPrestamo);

		String 		[]encabezado	=
												{
													"IC_SOLIC_PORTAL_DET",
													"IC_SOLIC_PORTAL",
													"IG_NUMERO_PRESTAMO",
													"CG_NOMBRE",
													"CG_SEGNOMBRE",
													"CG_APPAT",
													"CG_APMAT",
													"CG_RFC",
													"DF_NACIMIENTO",
													"FN_IMPORTE",
													"CG_CONCEPTO",
													"CG_ESTADO",
													"CG_MUNICIPIO"
												};
		int 		numCols 		= 13;
		String 	separador 	= ",";
		String	extension	= "csv";

		return Comunes.getCreateFile(query, lVarBind, path, separador, extension, encabezado, numCols);

	}

	public List getDetalleArchivosEnviados(String ic_if)
		throws NafinException {

		System.out.println("AutorizacionSolicitudBean::getDetalleArchivosEnviados(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		ArrayList		lista				= new ArrayList();
		boolean 			hayRegistros 	= false;
		
		try {
				
				con.conexionDB();
				qrySentencia =
						   "SELECT    "  +
                    "    p.ig_numero_prestamo                    AS numero_de_prestamo,  "  +
                    "    p.fn_importe_docto                      AS monto,               "  +
                    "    p.ic_solic_portal                       AS ic_solic_portal,     "  +
                    "    COUNT (1)                               AS numero_de_clientes,  "  +
						  "    TO_CHAR (p.df_carga, 'dd/mm/yyyy')      AS fecha_de_carga       "  +
                    
                    "FROM  "  +
                    "    com_solic_portal p "  +
                    "WHERE  "  +
                    "    p.ic_estatus_solic       = ?                      AND  "  + // 3
                    "    p.ig_numero_prestamo     IS NOT NULL              AND  "  +
                    "    p.df_operacion           IS NOT NULL              AND  "  +
                    "    p.ic_if                  = ?        "  + // ic_if
                    "GROUP BY                                "  +
                    "    p.ig_numero_prestamo,               "  +
                    "    p.fn_importe_docto,                 "  +
                    "    p.ic_solic_portal,                  " +
						  "    TO_CHAR (p.df_carga, 'dd/mm/yyyy')";

				lVarBind.add(new Integer(3));
				lVarBind.add(new Integer(ic_if));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				
				while(registros != null && registros.next()){
					HashMap registro = new HashMap();
					hayRegistros	= true;

					registro.put("NUMERO_DE_PRESTAMO",	registros.getString("numero_de_prestamo"));
					registro.put("MONTO",					"$ " + (registros.getString("monto").equals("")?"0.00":Comunes.formatoDecimal(registros.getString("monto"),2,true)));
					registro.put("NUMERO_DE_CLIENTES",	(registros.getString("numero_de_clientes").equals("")?"0":registros.getString("numero_de_clientes")));
					registro.put("IC_SOLIC_PORTAL",		registros.getString("ic_solic_portal"));
					registro.put("FECHA_DE_CARGA",		registros.getString("fecha_de_carga"));

					lista.add(registro);
				}
				

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getDetalleArchivosEnviados(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getDetalleArchivosEnviados(S)");
		}
      return hayRegistros?lista:null;
	}

	public List getListaDeIntermediariosFinancierosNoBancarios()
		throws NafinException {

		System.out.println("AutorizacionSolicitudBean::getListaDeIntermediariosFinancierosNoBancarios(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		ArrayList		lista				= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT   " +
					"	CIF.IC_IF                               as ic_if, " +
               "  CIF.CG_RAZON_SOCIAL                     as cg_razon_social " +
					"FROM  " +
               "   com_solic_portal p, COMCAT_IF CIF " +
               "WHERE   " +
               "   p.ic_estatus_solic       = ?                      AND  " +
               "   p.ig_numero_prestamo     IS NOT NULL              AND  " +
               "   p.df_operacion           IS NOT NULL              AND  " +
               "   p.ic_if                  = CIF.IC_IF              AND  " +
               "   CIF.CS_TIPO              = ? 							AND  " +
					"	 CS_HABILITADO            = ?                           " +
               "GROUP BY  																  " +
               "    CIF.IC_IF,  														  " +
               "    CIF.CG_RAZON_SOCIAL 											  " +
               "ORDER BY CIF.CG_RAZON_SOCIAL";

				lVarBind.add(new Integer(3));
				lVarBind.add("NB");
				lVarBind.add("S");


				/*qrySentencia =
					"SELECT " +
					"	CIF.IC_IF, " +
					"	CIF.CG_RAZON_SOCIAL " +
					"FROM " +
					"	COMCAT_IF CIF " +
					"WHERE " +
					"	CIF.CS_TIPO   = ? AND " + // 'NB'
					"	CS_HABILITADO = ?   " +    // 'S'
					"ORDER BY CIF.CG_RAZON_SOCIAL ";

				lVarBind.add("NB");
				lVarBind.add("S");*/

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				// Agregar los registros de la consulta
				while(registros != null && registros.next()){
					HashMap		registro=new HashMap();
					
					registro.put("DESCRIPCION",registros.getString("CG_RAZON_SOCIAL"));
					registro.put("ID",			registros.getString("IC_IF"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getListaDeIntermediariosFinancierosNoBancarios(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getListaDeIntermediariosFinancierosNoBancarios(S)");
		}
      return lista;
	}

	public boolean depurarDetalles(String[] numprestamos)throws NafinException
  {
		System.out.println("AutorizacionSolicitudBean::depurarDetalles(E)");
		AccesoDB con 					= new AccesoDB();
		List lVarBind					= new ArrayList();
    try
    {
		con.conexionDB();
		String eliminaDetalles = "  DELETE FROM COM_SOLIC_PORTAL_DET " +
                              "  WHERE IG_NUMERO_PRESTAMO = ? ";
			System.out.println(eliminaDetalles);

			for(int i = 0; i < numprestamos.length; ++i)
         {
            lVarBind.add(numprestamos[i]);

				System.out.println(lVarBind);
				con.ejecutaUpdateDB(eliminaDetalles, lVarBind);
            con.terminaTransaccion(true);
				lVarBind.clear();
         }
			return true;
		}
		catch(Exception e)
		{
			System.out.println(":: Ocurrio un error en depurarDetalles:: "+ e.toString());
			return false;
		}
    finally{
      if (con.hayConexionAbierta())
				con.cierraConexionDB();
		System.out.println("AutorizacionSolicitudBean::depurarDetalles(E)");
	 }
  }//depurarDetalles

    public List getDetalleAcuse(String carga)
		throws NafinException {

		System.out.println("AutorizacionSolicitudBean::getDetalleAcuse(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		ArrayList		lista				= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
					" SELECT ig_numero_prestamo AS NUMPRESTAMO, sum(fn_importe) AS IMPORTE, count(fn_importe) AS DETALLES, TO_CHAR(sysdate, 'dd/mm/yyyy') AS FECHA  " +
					" FROM comtmp_solic_portal_det " +
					" WHERE ig_numero_prestamo is not null " +
					" AND ic_proc_carga = ? " +
					" AND cg_status = 'S' " +
					" GROUP BY ig_numero_prestamo ";
				System.out.println("AutorizacionSolicitudBean::getDetalleAcuse(Consulta)::: "+qrySentencia);

				lVarBind.add(carga);
				System.out.println(lVarBind);

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				// Agregar los registros de la consulta
				while(registros != null && registros.next()){
					HashMap		registro=new HashMap();
					registro.put("NUMPRESTAMO", registros.getString("NUMPRESTAMO"));
					registro.put("IMPORTE",	Comunes.formatoDecimal(registros.getString("IMPORTE"),2));
					registro.put("DETALLES", registros.getString("DETALLES"));
					registro.put("FECHA",registros.getString("FECHA"));
					lista.add(registro);
				}

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getDetalleAcuse(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getDetalleAcuse(S)");
		}
      return lista;
	}//getDetalleAcuse

	public String generaArchivoAFirmarOperacionesExtemporaneas(String strDirectorio, String rutaArchivo, String listaDoctos)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::generaArchivoAFirmarOperacionesExtemporaneas(E)");
		String 				linea 	= null;
		java.io.File   	f 			= null;
		BufferedReader 	in			= null;
		BufferedWriter 	out 		= null;
		java.io.File 		archivo 	= null;

		if(listaDoctos == null || listaDoctos.equals(""))
			return null;

		String		[]lineas							= listaDoctos.split(",");
		Collection 	coleccionLineas 				= Arrays.asList(lineas);

		try {
			// Crear archivo temporal donde se guardaran los archivos con errores
        	archivo = File.createTempFile(
			  					"ArchivoOpExtemporaneas",
								".txt",
								new File(strDirectorio));
		  	out = new BufferedWriter(new FileWriter(archivo));

		  	System.out.println("File Name:      "+archivo.getName() +" was created"); // Debug info
        	System.out.println("Full File Path: "+archivo.getPath() +" was created"); // Debug info

		  	// Arbir archivo
			f	= new java.io.File(rutaArchivo);
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar registros
			int numeroLinea		=1;
			int numLineaEscrita	=1;

			while((linea=in.readLine())!=null){
				if(coleccionLineas.contains(Integer.toString(numeroLinea))){
					String 			[]campo 	= linea.split("\\|");

					String	clienteSIRAC					= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():"");
					String	montoDeCapital					= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():"");
					String	numTotalDeAmortizaciones	= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():"0");
					String	fechaDeFormalizacion			= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():"");
					String	numDocumento					= Comunes.quitaComitasSimplesyDobles(campo.length>8?campo[8].trim():"");
					String   fechaVencimientoDoc			= Fecha.sumaFechaDias(fechaDeFormalizacion,"dd/MM/yyyy",Integer.parseInt(numTotalDeAmortizaciones)*7);

					out.write(
									Integer.toString(numLineaEscrita)						+'|'+
									clienteSIRAC 													+'|'+
									numDocumento													+'|'+
									fechaDeFormalizacion											+'|'+
									fechaVencimientoDoc											+'|'+
									numTotalDeAmortizaciones									+'|'+
									Comunes.formatoDecimal(montoDeCapital,2,true) 		+'|'+
									Comunes.formatoDecimal(montoDeCapital,2,true) 		+"\n"
								);

					numLineaEscrita++;


				}
				numeroLinea++;
			}

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::generaArchivoAFirmarOperacionesExtemporaneas(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close();}catch(Exception e){};
			if(out 	!= null )	try{out.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::generaArchivoAFirmarOperacionesExtemporaneas(S)");
		return archivo.getName();
	}
/*
	public HashMap getResumenDeSolicitudesExtemporaneas(String strDirectorio, String nombreArchivo)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::getResumenDeSolicitudesExtemporaneas(E)");
		String 				linea 		= null;
		java.io.File   	f 				= null;
		BufferedReader 	in				= null;
		HashMap				resultado	= new HashMap();

		try {

		  	// Arbir archivo
			f	= new java.io.File(strDirectorio + nombreArchivo );
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar registros
			String 		numLineaEscrita		=  "0";
			BigDecimal	montoTotalDocumentos	= new BigDecimal("0.00");

			while((linea=in.readLine())!=null){

					String 			[]campo 	= linea.split("\\|");

					numLineaEscrita							= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():"");

					//String	clienteSIRAC					= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim():"");
					//String	numDocumento					= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():"");
					//String	fechaDeFormalizacion			= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():"");
					//String	fechaVencimientoDoc			= Comunes.quitaComitasSimplesyDobles(campo.length>4?campo[4].trim():"");
					//String	numTotalDeAmortizaciones	= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():"");
					String	montoDeCapital					= Comunes.quitaComitasSimplesyDobles(campo.length>6?campo[6].trim():"0.00");
					//String	montoDelDescuento				= Comunes.quitaComitasSimplesyDobles(campo.length>7?campo[7].trim():"0.00");

					montoDeCapital = montoDeCapital.equals("")?"0.00":montoDeCapital;
					montoDeCapital = Comunes.suprimeCaracteres(montoDeCapital,",");

					BigDecimal 	montoDelDocumento = new BigDecimal(montoDeCapital);

					montoTotalDocumentos = montoTotalDocumentos.add(montoDelDocumento);
			}

			//Establecer los resultados
			resultado.put("NUM_TOTAL_OPERACIONES_REGISTRADAS",	numLineaEscrita);
			resultado.put("MONTO_TOTAL_IMPORTE_DOCUMENTOS",		"$"+Comunes.formatoDecimal(montoTotalDocumentos.toPlainString(),2,true));
			resultado.put("MONTO_TOTAL_IMPORTE_DESCUENTOS",		"$"+Comunes.formatoDecimal(montoTotalDocumentos.toPlainString(),2,true));

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::getResumenDeSolicitudesExtemporaneas(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::getResumenDeSolicitudesExtemporaneas(S)");
		return resultado;
	}
*/
	public List getListaDeRegistrosExtemporaneosAFirmar(String strDirectorio, String nombreArchivo)
		throws NafinException{
		System.out.println("AutorizacionSolicitudBean::getListaDeRegistrosExtemporaneosAFirmar(E)");
		String 				linea 		= null;
		java.io.File   	f 				= null;
		BufferedReader 	in				= null;
		List					lista			= new ArrayList();

		try {

		  	// Arbir archivo
			f	= new java.io.File(strDirectorio + nombreArchivo);
			in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

			// Procesar Archivo
			while((linea=in.readLine())!=null){

					String 			[]campo 	= linea.split("\\|");

					HashMap registro = new HashMap();

					String   numLineaEscrita				= Comunes.quitaComitasSimplesyDobles(campo.length>0?campo[0].trim():"");
					String	clienteSIRAC					= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim():"");
					String	numDocumento					= Comunes.quitaComitasSimplesyDobles(campo.length>2?campo[2].trim():"");
					String	fechaDeFormalizacion			= Comunes.quitaComitasSimplesyDobles(campo.length>3?campo[3].trim():"");
					String	fechaVencimientoDoc			= Comunes.quitaComitasSimplesyDobles(campo.length>4?campo[4].trim():"");
					String	numTotalDeAmortizaciones	= Comunes.quitaComitasSimplesyDobles(campo.length>5?campo[5].trim():"");
					String	montoDeCapital					= Comunes.quitaComitasSimplesyDobles(campo.length>6?campo[6].trim():"0.00");
					String	montoDelDescuento				= Comunes.quitaComitasSimplesyDobles(campo.length>7?campo[7].trim():"0.00");

					registro.put("NUMERO",							numLineaEscrita);
					registro.put("CLIENTE_SIRAC",					clienteSIRAC);
					registro.put("NUMERO_DE_DOCUMENTO",			numDocumento);
					registro.put("FECHA_DE_EMISION",				fechaDeFormalizacion);
					registro.put("FECHA_DE_VENCIMIENTO",		fechaVencimientoDoc);
					registro.put("NUMERO_DE_AMORTIZACIONES",	numTotalDeAmortizaciones);
					registro.put("MONTO_DEL_DOCUMENTO",			montoDeCapital);
					registro.put("MONTO_DEL_DESCUENTO",			montoDelDescuento);

					lista.add(registro);

			}

		} catch (Exception e) {
			System.out.println("AutorizacionSolicitudBean::getListaDeRegistrosExtemporaneosAFirmar(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(in 	!= null )	try{in.close();}catch(Exception e){};
		}

		System.out.println("AutorizacionSolicitudBean::getListaDeRegistrosExtemporaneosAFirmar(S)");
		return lista;
	}

	public void subeArchivoCSVDeRegistroExtemporaneo(String nombreUsuario,String claveUsuario,String claveIF, String fechaDeActualizacion, String numeroTotalOperaciones, String montoTotalImporteDocumentos, String montoTotalImporteDescuento,String recibo, String directorio, String nombreArchivo,String hash,String tipoDePrograma) throws NafinException{
		System.out.println("AutorizacionSolicitudBean::subeArchivoCSVDeRegistroExtemporaneo(E)");
		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia	= null;
		List 					lVarBind			= new ArrayList();
		boolean				lbOK				= true;
		PreparedStatement	ps					= null;
		Registros			registros		= null;
		try {

				con.conexionDB();
				// Insertar documentos
				qrySentencia =
					"INSERT INTO 											" +
					"	COM_SOLIC_EMERGENTES 							" +
					"		(													" +
					"			IC_SOLIC_EMERGENTES,						" +
					" 			IC_IF,										" +
					"			IG_NUMERO_DOCTOS,							" +
					"			DF_CARGA, 									" +
					"			FN_IMPORTE_DOCTO,							" +
					"			FN_IMPORTE_DSCTO,							" +
					"			IC_USUARIO,									" +
					"			CG_NOMBRE_USUARIO,						" +
					"			CG_HASH_MD5,								" +
					"			CD_CLAVE										" +
					"		) 													" +
					"VALUES 													" +
					"		(													" +
					"			?,												" +
					"			?,												" +
					"			?,												" +
					"			TO_DATE(?,'DD/MM/YYYY HH:MI:SS'),	" +
					"			?,												" +
					"			?,												" +
					"			?,												" +
					"			?,												" +
					"			?,												" +
					"			?												" +
					"		) 													";
				lVarBind.add(new Integer(recibo));
				lVarBind.add(new Integer(claveIF));
				lVarBind.add(new Integer(numeroTotalOperaciones));
				lVarBind.add(fechaDeActualizacion);
				lVarBind.add(montoTotalImporteDocumentos);
				lVarBind.add(montoTotalImporteDescuento);
				lVarBind.add(new Integer(claveUsuario));
				lVarBind.add(nombreUsuario);
				lVarBind.add(hash);
				lVarBind.add(tipoDePrograma);
				con.ejecutaUpdateDB(qrySentencia, lVarBind);

				// Comprobar Hash
				qrySentencia =
					"SELECT                   " +
					"	CG_HASH_MD5 AS HASH    " +
					"FROM                     " +
					"	COM_SOLIC_EMERGENTES   " +
					"WHERE                    " +
					"	IC_SOLIC_EMERGENTES = ?";
				lVarBind.clear();
				lVarBind.add(new Integer(recibo));
				registros = con.consultarDB(qrySentencia, lVarBind,false);
				if(registros != null && registros.next()){
					String hashEnBaseDeDatos	= registros.getString("HASH");
					String hashActual 			= Comunes.getHash(directorio + nombreArchivo);

					if(!hashActual.equals(hashEnBaseDeDatos)){
						throw new Exception("El hash MD5 del Archivo: '" + directorio + nombreArchivo + "' no coincide con el hash MD5 actual. HASH MD5 ACTUAL = " + hashActual + " . HASH DE LA BASE DE DATOS = "+ hashEnBaseDeDatos );
					}
				}

				// Cargar Archivo
				java.io.File archivo = new java.io.File(directorio + nombreArchivo);
				qrySentencia =
						"UPDATE 							" +
						"	COM_SOLIC_EMERGENTES 	" +
						"SET 								" +
						"	BI_ARCHIVO_EXT 	= ?	" +
						"WHERE 							" +
						"	IC_SOLIC_EMERGENTES = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setBinaryStream(1, new FileInputStream(archivo), (int)archivo.length());
				ps.setInt(2, Integer.parseInt(recibo));
				ps.executeUpdate();
				ps.close();

			} catch(Exception e) {
				System.out.println("AutorizacionSolicitudBean::subeArchivoCSVDeRegistroExtemporaneo(Exception) "+e);
				e.printStackTrace();
				lbOK = false;
				throw new NafinException("SIST0001");
			} finally {
				con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("AutorizacionSolicitudBean::subeArchivoCSVDeRegistroExtemporaneo(S)");
			}
	}

	public String getNumeroRecibo() throws NafinException
	{
		System.out.println("AutorizacionSolicitudBean::getNumeroRecibo(S)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String			resultado		= "0";

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT														" +
					"	MAX(IC_SOLIC_EMERGENTES)+1 AS NUMERO_RECIBO  " +
					"FROM                                           " +
					"	COM_SOLIC_EMERGENTES 				            ";
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					resultado = registros.getString("NUMERO_RECIBO").equals("")?"0":registros.getString("NUMERO_RECIBO");
				}
		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getNumeroRecibo(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getNumeroRecibo(S)");
		}
		return resultado;
	}

	public HashMap autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente)
		throws NafinException{

		System.out.println("AutorizacionSolicitudBean::autenticaFirmaDigital(E)");

		pkcs7					= (pkcs7 			== null?"":pkcs7);
		textoFirmado		= (textoFirmado 	== null?"":textoFirmado);
		numeroCliente		= (numeroCliente 	== null?"":numeroCliente);
		serial				= (serial 			== null?"":serial);

		String 		folioCertificado 	= null;
		netropology.utilerias.Seguridad	seguridad			= null;
		char 			getReceipt 			= 'Y';
		HashMap  	resultado			= new HashMap();

		if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") && !numeroCliente.equals("") ) {
			folioCertificado 	= "01CC"+numeroCliente+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
			seguridad 			= new netropology.utilerias.Seguridad();

			if (!seguridad.autenticar(folioCertificado, serial, pkcs7, textoFirmado, getReceipt)) {
				resultado.put("VERIFICACION_EXITOSA","false");
				resultado.put("MENSAJE_ERROR","La autentificacion no se llevo a cabo: <br>"+seguridad.mostrarError());
			}else{
				resultado.put("VERIFICACION_EXITOSA","true");
				resultado.put("RECIBO",seguridad.getAcuse());
				resultado.put("FOLIO",folioCertificado);
			}

		}else{
			System.out.println("AutorizacionSolicitudBean::autenticaFirmaDigital(Exception)");
			System.out.println("Uno o mas parametros vienen vacios: ");
			System.out.println("      textoFirmado   = " + textoFirmado);
			System.out.println("      serial         = " + serial);
			System.out.println("      numeroCliente  = " + numeroCliente);
			System.out.println("      pkcs7          = " + pkcs7);
			throw new NafinException("GRAL0021");
		}

		System.out.println("AutorizacionSolicitudBean::autenticaFirmaDigital(S)");
		return resultado;
	}

	public String	getFechaDeActualizacion()
		throws NafinException {
		System.out.println("AutorizacionSolicitudBean::getFechaDeActualizacion(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			resultado				= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 	" +
					"	TO_CHAR(SYSDATE,'DD/MM/YYYY HH:MI:SS') AS FECHA " +
					"FROM 	" +
					"	DUAL 	";

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					resultado = registros.getString("FECHA");
				}

		}catch(Exception e){
			System.out.println("AutorizacionSolicitudBean::getFechaDeActualizacion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getFechaDeActualizacion(S)");
		}

		return resultado;
	}

	public boolean estaRepetidoHashMD5(String hash)
		throws NafinException{

		System.out.println("AutorizacionSolicitudBean::estaRepetidoHashMD5(E)");

		// Realizar consulta
		AccesoDB 		con 								= new AccesoDB();
		String 			qrySentencia					= null;
		Registros		registros						= null;
		List 				lVarBind							= new ArrayList();
		boolean 			estaRepetido					= false;

		try {
				con.conexionDB();
				qrySentencia =
				    "SELECT                                            " +
					 "		DECODE(COUNT(1),0,'false','true') AS REPETIDO " +
					 "FROM                                              " +
					 "		COM_SOLIC_EMERGENTES                          " +
					 "WHERE                                             " +
					 "		CG_HASH_MD5 = ?                               ";
				lVarBind.add(hash);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					estaRepetido = registros.getString("REPETIDO").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::estaRepetidoHashMD5(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::estaRepetidoHashMD5(S)");
		}
		return estaRepetido;

	}
//======================================================================>> Fodea 038 - extemporaneas BAZ
/**
 * M�todo que obtiene la informaci�n de
 * @param producto Es el producto de piso uno con tasas parametrizadas
 * @param plazo Es el plazo en dias con el que se obtiene la tasa parametricada.
 * @return HashMap con la tasa base y la sobretasa
 * */
  public HashMap consultarTablasEmergentes(String intermediario) throws NafinException{
		AccesoDB con = new AccesoDB();
		HashMap tablasEmergentes = new HashMap();
    PreparedStatement ps = null;
    ResultSet rs = null;
    String strSQL = "";
		int i = 0;

    System.out.println("ServiciosNafElectBean::consultarTablasEmergentes(S)");

		try {
			con.conexionDB();
      strSQL = " SELECT SEM.IC_SOLIC_EMERGENTES,"+
							 " SEM.IG_NUMERO_DOCTOS,"+
							 " SEM.FN_IMPORTE_DOCTO,"+
							 " SEM.FN_IMPORTE_DSCTO,"+
							 " TO_CHAR(SEM.DF_CARGA, 'DD/MM/YYYY'),"+
							 " SEM.IC_USUARIO,"+
							 " SEM.CG_NOMBRE_USUARIO,"+
							 " CAT.CD_DESCRIPCION AS PROGRAMA " +
							 " FROM COM_SOLIC_EMERGENTES SEM, "+
							 " COMCAT_OPER_EMERGENTES   CAT " +
							 " WHERE " +
							 "	SEM.CD_CLAVE   = CAT.CD_CLAVE  and " +
							 " SEM.IC_IF 		= "+intermediario+
							 " ORDER BY SEM.IC_SOLIC_EMERGENTES";

      ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();

			while(rs.next()){
				tablasEmergentes.put("icSolicitud"+i,rs.getString(1).trim());
				tablasEmergentes.put("numeroDocumentos"+i,rs.getString(2).trim());
				tablasEmergentes.put("montoDocumentos"+i,rs.getString(3).trim());
				tablasEmergentes.put("montoDescuentos"+i,rs.getString(4).trim());
				tablasEmergentes.put("fechaEnvio"+i,rs.getString(5).trim());
				tablasEmergentes.put("usuario"+i,rs.getString(6).trim());
				tablasEmergentes.put("nombreUsuario"+i,rs.getString(7).trim());
				tablasEmergentes.put("programa"+i,rs.getString(8).trim());
				i++;
			}

			tablasEmergentes.put("registros", Integer.toString(i));

			rs.close();
			ps.close();

			return tablasEmergentes;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
      System.out.println("ServiciosNafElectBean::consultarTablasEmergentes(E)");
		}
	}

  /**
   * Obtiene la informaci�n del archivo del documento cargado en la tabla.
   * @return archivo cargado.
   * @param numeroSolicitud
   * @param directorio
   * @param iFin
   */
  public File getArchivoTablasEmergentes(String numeroSolicitud, String iFin, String directorio) throws NafinException{
    AccesoDB con         = new AccesoDB();
		FileOutputStream fos = null;
    File file            = null;
    PreparedStatement ps = null;
    ResultSet rs         = null;
    String qryDocto      = "";

    try{
      con.conexionDB();
      qryDocto = " SELECT BI_ARCHIVO_EXT"+
								 " FROM COM_SOLIC_EMERGENTES"+
								 " WHERE IC_SOLIC_EMERGENTES = "+numeroSolicitud+
								 " AND IC_IF = "+iFin;

      ps = con.queryPrecompilado(qryDocto);
      rs = ps.executeQuery();

			if(rs.next()){
				int f = (int)(100000*Math.random());
        String pathname = directorio + "\\DetalleAmortizaciones"+f+".csv";
        file = new File(pathname);
        fos = new FileOutputStream(file);
        Blob bin = rs.getBlob("BI_ARCHIVO_EXT");
        InputStream inStream = bin.getBinaryStream();
        int size = (int)bin.length();
        byte[] buffer = new byte[size];
        int length = -1;

				while ((length = inStream.read(buffer)) != -1){
          fos.write(buffer, 0, length);
        }
      }
    }catch(Exception e){
      System.out.println("::: Error consultando documentos :: AutorizacionSolicitudEJB.getArchivoTablasEmergentes :: "+e.toString());
      e.printStackTrace();
			throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
    }
    return file;
  }

/**
 * Elimina de la tabla temporal los datos de los registros selecccionados.
 * @throws com.netro.exception.NafinException
 * @param numerosSol Los numeros de solicitud de los registros a eliminar.
 * @param iFin La clave del intermediario financiero.
 */
 public void eliminarTablasEmergentes(String numerosSol, String iFin) throws NafinException{
	AccesoDB con	= new AccesoDB();

	try{
		con.conexionDB();

		String strSQL = " DELETE COM_SOLIC_EMERGENTES"+
									  " WHERE IC_SOLIC_EMERGENTES IN ("+numerosSol+")"+
									  "   AND IC_IF = "+iFin;
		con.ejecutaUpdateDB(strSQL);

	}catch( SQLException errorSQL){
		con.cierraConexionDB();
		throw new NafinException("SIST0001");
	}catch(Exception e){
		e.printStackTrace();
		con.cierraConexionDB();
		throw new NafinException("SIST0001");
	}
	finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
	}
 }
//======================================================================>> Fodea 038 - extemporaneas BAZ

	public List getTiposDePrograma(String tipoRegistro)
		throws NafinException {

		System.out.println("AutorizacionSolicitudBean::getTiposDePrograma(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String			condicion		= null;
		List				resultado		= new ArrayList();

		try {
				con.conexionDB();

				if(tipoRegistro.equals("REGISTRO_EXTEMPORANEO")){
					condicion = "CS_OPERACIONES_EXTEMPORANEAS";
				}else	if(tipoRegistro.equals("REGISTRO_VIGENTE")){
					condicion = "CS_OPERACIONES_VIGENTES";
				}

				qrySentencia =
					"SELECT 																" +
					"	CD_CLAVE AS CLAVE, CD_DESCRIPCION AS DESCRIPCION 	" +
					"FROM 																" +
					"	COMCAT_OPER_EMERGENTES 										" +
					"WHERE 																" +
					"	"+ condicion +" = ?	 										" +
					"ORDER BY CD_DESCRIPCION										";

				lVarBind.add("S");
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next() ){
					HashMap 	registro		= new HashMap();

					registro.put("CLAVE", 			registros.getString("CLAVE"));
					registro.put("DESCRIPCION", 	registros.getString("DESCRIPCION"));


					resultado.add(registro);
				}

		} catch(Exception e) {
			System.out.println("AutorizacionSolicitudBean::getTiposDePrograma(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizacionSolicitudBean::getTiposDePrograma(S)");
		}

		return resultado;
	}

	public String getSecuenciaDeTiposDePrograma(List lista){

		System.out.println("AutorizacionSolicitudBean::getSecuenciaDeTiposDeProgramaE)");

		if(lista == null) { return ""; }

		StringBuffer resultado = new StringBuffer();

		for(int i=0;i<lista.size();i++){
			HashMap registro = (HashMap) lista.get(i);
			resultado.append("\"" + registro.get("CLAVE") + "\" " + registro.get("DESCRIPCION"));
			if(i == (lista.size() - 2) ){
				resultado.append(" o ");
			}else if(i <= (lista.size() - 3)){
				resultado.append(", ");
			}

		}
		System.out.println("AutorizacionSolicitudBean::getSecuenciaDeTiposDePrograma(S)");

		return resultado.toString();
	}

	public String[] getClavesDeTipoDePrograma(String tipoRegistro)
		throws NafinException{
		List 		lista 	= getTiposDePrograma(tipoRegistro);
		String 	[]claves = new String[lista.size()*2];

		// En Mayusculas
		for(int i=0;i<lista.size();i++){
			HashMap registro = (HashMap)lista.get(i);
			claves[i] = ((String)registro.get("CLAVE")).toUpperCase();
		}

		// En Minusculas
		int offset = lista.size();
		for(int i=0;i<lista.size();i++){
			HashMap registro = (HashMap)lista.get(i);
			claves[i+offset] = ((String)registro.get("CLAVE")).toLowerCase();
		}

		return claves;
	}

	public boolean existeMasDeUnTipoDePrograma(String rutaArchivo, String listaDoctos)
		throws NafinException{

			System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(E)");

			String 			linea 		= null;
			java.io.File   f 				= null;
			BufferedReader in				= null;
			HashSet			set			= new HashSet();
			boolean			resultado	= false;

			if(listaDoctos == null || listaDoctos.equals(""))
				return false;

			String		[]lineas			= listaDoctos.split(",");
			Collection 	coleccionLineas 	= Arrays.asList(lineas);

			try {

				// Arbir archivo
				f	= new java.io.File(rutaArchivo);
				in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

				// Procesar registros
				int numeroLinea		= 1;
				while((linea=in.readLine())!=null){
					if(coleccionLineas.contains(Integer.toString(numeroLinea))){
						String []campo 		= linea.split("\\|");
						String tipoPrograma	= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim():"");
						set.add(tipoPrograma.toUpperCase());
					}
					numeroLinea++;
				}

				// Evaluar si hay mas de un tipo de programa
				if( set.size() > 1){
					resultado = true;
				}

			} catch (Exception e) {
				System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(Exception)");
				e.printStackTrace();
				throw new NafinException("SIST0001");
			} finally {
				if(in 	!= null )	try{in.close();}catch(Exception e){};
				System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(S)");
			}

			return resultado;
		}

	public String getTipoDePrograma(String rutaArchivo, String listaDoctos)
		throws NafinException{

			System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(E)");

			String 			linea 			= null;
			java.io.File   f 					= null;
			BufferedReader in					= null;
			String 			tipoPrograma	= null;

			if(listaDoctos == null || listaDoctos.equals(""))
				return "";

			String		[]lineas			= listaDoctos.split(",");
			Collection 	coleccionLineas 	= Arrays.asList(lineas);

			try {

				// Arbir archivo
				f	= new java.io.File(rutaArchivo);
				in	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));

				// Procesar registros
				int numeroLinea		= 1;
				while((linea=in.readLine())!=null){
					if(coleccionLineas.contains(Integer.toString(numeroLinea))){
						String []campo 		= linea.split("\\|");
						tipoPrograma	= Comunes.quitaComitasSimplesyDobles(campo.length>1?campo[1].trim():"");
						break;
					}
					numeroLinea++;
				}

			} catch (Exception e) {
				System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(Exception)");
				e.printStackTrace();
				throw new NafinException("SIST0001");
			} finally {
				if(in 	!= null )	try{in.close();}catch(Exception e){};
				System.out.println("AutorizacionSolicitudBean::existeMasDeUnTipoDePrograma(S)");
			}

			return tipoPrograma;
		}

		private String getNombreClienteSIRAC(String clienteSIRAC) throws NafinException{

			System.out.println("AutorizacionSolicitudBean::getNombreClienteSIRAC(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			List 				lVarBind			= new ArrayList();
			Registros		registros		= null;
			String			resultado		= "";

			try {
				con.conexionDB();

				qrySentencia =
					"SELECT 																			" +
					"	DECODE( 																		" +
					"		LENGTH(CG_RFC), 														" +
               "			15, CG_APPAT || ' ' || CG_APMAT || ' ' || CG_NOMBRE, 	" +
               "			14, CG_RAZON_SOCIAL, 											" +
               "		 	''																		" +
					"			) AS NOMBLE_CLIENTE_SIRAC										" +
					"FROM 																			" +
					"	COMCAT_PYME 																" +
					"WHERE 																			" +
					"	IN_NUMERO_SIRAC = ?														";

				lVarBind.add(clienteSIRAC);

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next() ){
					resultado = registros.getString("NOMBLE_CLIENTE_SIRAC");
				}

			} catch(Exception e) {
				System.out.println("AutorizacionSolicitudBean::getNombreClienteSIRAC(Exception)");
				e.printStackTrace();
				throw new NafinException("SIST0001");
			} finally {
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("AutorizacionSolicitudBean::getNombreClienteSIRAC(S)");
			}

			return resultado;
		}

		public List getIFDescuento() throws NafinException{

			System.out.println("AutorizacionSolicitudBean::getIFDescuento(E)");

			AccesoDB con = new AccesoDB();
			List coleccionElementos = new ArrayList();

			String qryCatIf;
			try
			{
				con.conexionDB();
				Registros regCatIf	=	null;

				qryCatIf = " SELECT I.ic_if CLAVE ,I.cg_razon_social DESCRIPCION " +
								" 	FROM  comcat_if I  , comrel_producto_if cpi " +
								"	WHERE I.cs_habilitado='S'  AND cpi.ic_if = I.ic_if  AND cpi.ic_producto_nafin = 1  ORDER BY 2  " ;

				regCatIf = con.consultarDB(qryCatIf);
				while(regCatIf.next()) {
						ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
						elementoCatalogo.setClave(regCatIf.getString("CLAVE"));
						elementoCatalogo.setDescripcion(regCatIf.getString("DESCRIPCION"));
						coleccionElementos.add(elementoCatalogo);
				}


			} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
			} finally {
				System.out.println("AutorizacionSolicitudBean::getIFDescuento(S)");
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}

			return coleccionElementos;
		}
		

	public String getOperCredElecPDF(String strRutaTmp, String strRutaFisica, String cveSolicPortal) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps=null, ps2 = null;
		ResultSet rs=null, rs2 = null;
		
		try{
			con.conexionDB();
			
			String nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strRutaTmp + nombreArchivo);
			String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual		= fechaActual.substring(0,2);
			String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual		= fechaActual.substring(6,10);
			String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			/*
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			*/
			
			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
			
			String qrySelect  = "";
			qrySelect = "SELECT cs.ig_clave_sirac, cp.cg_razon_social nombrecliente, cs.ig_sucursal, cs.ic_moneda, " +
						"       cm.cd_nombre nombremoneda, cs.ig_numero_docto, cs.fn_importe_docto, " +
						"       cs.fn_importe_dscto, cs.ic_tipo_credito, ct.cd_descripcion tipocredito, " +
						"       cs.ic_emisor, ce.cd_descripcion nombreemisor, " +
						"       cpd.cd_descripcion pcapital, cpi.cd_descripcion pinteres, " +
						"       cs.cg_bienes_servicios, cs.ic_clase_docto, " +
						"       cd.cd_descripcion clasedocto, cs.cg_domicilio_pago, " +
						"       cs.cg_destino_credito, cs.ic_tasauf, ct.cd_nombre nombretbuf, " +
						"       cs.cg_rmuf, cs.fg_stuf, cs.ic_tasaif, cti.cd_nombre nombretbif, " +
						"       cs.cg_rmif, cs.fg_stif, cs.in_numero_amort, cs.ic_tabla_amort, " +
						"       cta.cd_descripcion tablaamort, TO_CHAR (cs.df_ppc,'dd/mm/yyyy') df_ppc, " +
						"       cs.in_dia_pago, TO_CHAR (cs.df_ppi, 'dd/mm/yyyy') df_ppi, " +
						"       TO_CHAR (cs.df_v_documento, 'dd/mm/yyyy') df_v_documento, " +
						"       TO_CHAR (cs.df_v_descuento, 'dd/mm/yyyy') df_v_descuento, " +
						"       TO_CHAR (cs.df_etc, 'dd/mm/yyyy') df_etc, cs.cg_lugar_firma, " +
						"       cs.cs_amort_ajuste, cs.fg_importe_ajuste, cs.CS_TIPO_RENTA " +
						"  FROM com_solic_portal cs, " +
						"       comcat_pyme cp, " +
						"       comcat_moneda cm, " +
						"       comcat_tipo_credito ct, " +
						"       comcat_emisor ce, " +
						"       comcat_clase_docto cd, " +
						"       comcat_tasa ct, " +
						"       comcat_tasa cti, " +
						"       comcat_periodicidad cpd, " +
						"       comcat_periodicidad cpi, " +
						"       comcat_tabla_amort cta " +
						" WHERE cs.ic_moneda = cm.ic_moneda " +
						"   AND cs.ig_clave_sirac = cp.in_numero_sirac(+) " +
						"   AND cs.ic_tipo_credito = ct.ic_tipo_credito " +
						"   AND cs.ic_emisor = ce.ic_emisor(+) " +
						"   AND cs.ic_clase_docto = cd.ic_clase_docto " +
						"   AND cs.ic_tasauf = ct.ic_tasa(+) " +
						"   AND cs.ic_tasaif = cti.ic_tasa " +
						"   AND cs.ic_periodicidad_c = cpd.ic_periodicidad " +
						"   AND cs.ic_periodicidad_i = cpi.ic_periodicidad " +
						"   AND cs.ic_tabla_amort = cta.ic_tabla_amort " +
						"   AND cs.ic_solic_portal = ? ";
			
			ps = con.queryPrecompilado(qrySelect);
			ps.setLong(1, Long.parseLong(cveSolicPortal));
			rs = ps.executeQuery(); 
			
			int numCiclo = 0;
			int numCols = 0;
			while(rs!=null & rs.next()){
				String ig_clave_sirac = rs.getString("ig_clave_sirac")==null?"":rs.getString("ig_clave_sirac");
				String nombrecliente  = rs.getString("nombrecliente")==null?"":rs.getString("nombrecliente");
				String ig_sucursal = rs.getString("ig_sucursal")==null?"":rs.getString("ig_sucursal");
				//String ic_moneda = rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String nombremoneda  = rs.getString("nombremoneda")==null?"":rs.getString("nombremoneda");
				String ig_numero_docto = rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
				String fn_importe_docto = rs.getString("fn_importe_docto")==null?"":rs.getString("fn_importe_docto");
				String fn_importe_dscto = rs.getString("fn_importe_dscto")==null?"":rs.getString("fn_importe_dscto");
				//String ic_tipo_credito = rs.getString("ic_tipo_credito")==null?"":rs.getString("ic_tipo_credito");
				String tipocredito  = rs.getString("tipocredito")==null?"":rs.getString("tipocredito");
				//String ic_emisor = rs.getString("ic_emisor")==null?"":rs.getString("ic_emisor");
				String nombreemisor  = rs.getString("nombreemisor")==null?"":rs.getString("nombreemisor");
				String pcapital  = rs.getString("pcapital")==null?"":rs.getString("pcapital");
				String pinteres  = rs.getString("pinteres")==null?"":rs.getString("pinteres");
				String cg_bienes_servicios = rs.getString("cg_bienes_servicios")==null?"":rs.getString("cg_bienes_servicios");
				//String ic_clase_docto = rs.getString("ic_clase_docto")==null?"":rs.getString("ic_clase_docto");
				String clasedocto  = rs.getString("clasedocto")==null?"":rs.getString("clasedocto");
				String cg_domicilio_pago = rs.getString("cg_domicilio_pago")==null?"":rs.getString("cg_domicilio_pago");
				String cg_destino_credito = rs.getString("cg_destino_credito")==null?"":rs.getString("cg_destino_credito");
				//String ic_tasauf = rs.getString("ic_tasauf")==null?"":rs.getString("ic_tasauf");
				String nombretbuf  = rs.getString("nombretbuf")==null?"":rs.getString("nombretbuf");
				String cg_rmuf = rs.getString("cg_rmuf")==null?"":rs.getString("cg_rmuf");
				String fg_stuf = rs.getString("fg_stuf")==null?"":rs.getString("fg_stuf");
				//String ic_tasaif = rs.getString("ic_tasaif")==null?"":rs.getString("ic_tasaif");
				String nombretbif  = rs.getString("nombretbif")==null?"":rs.getString("nombretbif");
				//String cg_rmif = rs.getString("cg_rmif")==null?"":rs.getString("cg_rmif");
				//String fg_stif = rs.getString("fg_stif")==null?"":rs.getString("fg_stif");
				String in_numero_amort = rs.getString("in_numero_amort")==null?"":rs.getString("in_numero_amort");
				String ic_tabla_amort = rs.getString("ic_tabla_amort")==null?"":rs.getString("ic_tabla_amort");
				String tablaamort = rs.getString("tablaamort")==null?"":rs.getString("tablaamort");
				String df_ppc = rs.getString("df_ppc")==null?"":rs.getString("df_ppc");
				String in_dia_pago = rs.getString("in_dia_pago")==null?"":rs.getString("in_dia_pago");
				String df_ppi = rs.getString("df_ppi")==null?"":rs.getString("df_ppi");
				String df_v_documento = rs.getString("df_v_documento")==null?"":rs.getString("df_v_documento");
				String df_v_descuento = rs.getString("df_v_descuento")==null?"":rs.getString("df_v_descuento");
				String df_etc = rs.getString("df_etc")==null?"":rs.getString("df_etc");
				String cg_lugar_firma = rs.getString("cg_lugar_firma")==null?"":rs.getString("cg_lugar_firma");
				String cs_amort_ajuste = rs.getString("cs_amort_ajuste")==null?"":rs.getString("cs_amort_ajuste");
				String fg_importe_ajuste = rs.getString("fg_importe_ajuste")==null?"":rs.getString("fg_importe_ajuste");
				String cs_tipo_renta = rs.getString("cs_tipo_renta")==null?"No":( (rs.getString("cs_tipo_renta")).equals("S")?"Si":"No");
				
				if(numCiclo==0){
					
					pdfDoc.setTable(16,100,new float[]{4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f});
					pdfDoc.setCell("No. Cliente SIRAC","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("No Sucursal Banco","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("No de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre del Emisor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Descripci�n de Bienes y Servicios","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Destino del Cr�dito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clase de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Domicilio de Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa Base","celda01",ComunesPDF.CENTER);//16
					
				}
				
				pdfDoc.setCell(ig_clave_sirac,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombrecliente,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ig_sucursal,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombremoneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fn_importe_docto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fn_importe_dscto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipocredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreemisor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pcapital,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pinteres,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cg_bienes_servicios,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cg_destino_credito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(clasedocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cg_domicilio_pago,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombretbuf,"formas",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				if(numCiclo==0){
					
					numCols = 5;
					if( "1".equals(ic_tabla_amort) || "2".equals(ic_tabla_amort) || "4".equals(ic_tabla_amort) ){
						numCols += 7;
						//pdfDoc.setTable(numCols,100,new float[]{4.76f,4.76f,4.76f,4.76f,4.76f,4.76f,4.76f});
					}
					
					if( "5".equals(ic_tabla_amort) && "S".equals(cs_tipo_renta)){
						numCols += 8;
					}
					if( "3".equals(ic_tabla_amort) ){
						numCols += 10;
					}
					if( "5".equals(ic_tabla_amort) && !"S".equals(cs_tipo_renta)){
						numCols += 4;
					}
					
					pdfDoc.setTable(numCols,100);
					
					pdfDoc.setCell("Relaci�n Matem�tica","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Sobretasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa Base Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Amortizaciones","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tabla de Amortizaci�n","celda01",ComunesPDF.CENTER);
					
					if( "1".equals(ic_tabla_amort) || "2".equals(ic_tabla_amort) || "3".equals(ic_tabla_amort) || "4".equals(ic_tabla_amort) || ("5".equals(ic_tabla_amort) && "S".equals(cs_tipo_renta)) ){
						if( "5".equals(ic_tabla_amort) && "S".equals(cs_tipo_renta)){
							pdfDoc.setCell("Tipo de Renta","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Fecha de primer pago de capital","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("D�a de pago","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de primer pago de inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento del docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento del descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de emisi�n del t�tulo de cr�dito","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Lugar de firma","celda01",ComunesPDF.CENTER);
						
						if("3".equals(ic_tabla_amort) ){
							pdfDoc.setCell("Amortizaci�n de Ajuste","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Importe Amortizaci�n de Ajuste","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Amortizaci�n de Ajuste","celda01",ComunesPDF.CENTER);
						}
					}
					
					if( "5".equals(ic_tabla_amort) && !"S".equals(cs_tipo_renta)){
						pdfDoc.setCell("Tipo de Renta","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento del docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de emisi�n del t�tulo de cr�dito","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Lugar de firma","celda01",ComunesPDF.CENTER);
					}
				}
				
				pdfDoc.setCell(cg_rmuf,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fg_stuf,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombretbif,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(in_numero_amort,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tablaamort,"formas",ComunesPDF.CENTER);
				
				if( "1".equals(ic_tabla_amort) || "2".equals(ic_tabla_amort) || "3".equals(ic_tabla_amort) || "4".equals(ic_tabla_amort) || ("5".equals(ic_tabla_amort) && "S".equals(cs_tipo_renta)) ){
						if( "5".equals(ic_tabla_amort) && "S".equals(cs_tipo_renta)){
						pdfDoc.setCell(cs_tipo_renta,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(df_ppc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_dia_pago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_ppi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_v_documento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_v_descuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_etc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_lugar_firma,"formas",ComunesPDF.CENTER);
					
					if("3".equals(ic_tabla_amort) ){
						pdfDoc.setCell(cs_amort_ajuste,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fg_importe_ajuste,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
				}
				
				if( "5".equals(ic_tabla_amort) && !"S".equals(cs_tipo_renta)){
					pdfDoc.setCell(cs_tipo_renta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_v_documento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_etc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_lugar_firma,"formas",ComunesPDF.CENTER);
				
					qrySelect = "SELECT ic_amort_portal, ic_solic_portal, cg_tipo_amort, fn_monto, " +
									"       to_char(df_vencimiento,'dd/mm/yyyy') df_vencimiento " +
									"  FROM com_amort_portal " +
									" WHERE ic_solic_portal = ? ";
									
					ps2 = con.queryPrecompilado(qrySelect);
					ps2.setLong(1, Long.parseLong(cveSolicPortal));
					rs2 = ps2.executeQuery();
					
					int cicloInterno = 0;
					while(rs2!=null && rs2.next()){
						if(cicloInterno==0){
							pdfDoc.addTable();
							pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
							pdfDoc.setTable(4,60);
							
							pdfDoc.setCell("Amortizaciones","celda01",ComunesPDF.CENTER, 4);
							pdfDoc.setCell("N�mero de Amortizaciones","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo de Amortizaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Importe de la Amortizaci�n","celda01",ComunesPDF.CENTER);
						}
						
						pdfDoc.setCell(rs2.getString("ic_amort_portal")==null?"":rs2.getString("ic_amort_portal"),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(rs2.getString("df_vencimiento")==null?"":rs2.getString("df_vencimiento"),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(rs2.getString("cg_tipo_amort")==null?"":rs2.getString("cg_tipo_amort"),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(rs2.getString("fn_monto")==null?"":rs2.getString("fn_monto"),"formas",ComunesPDF.CENTER);
						cicloInterno++;
					}
					rs2.close();
					ps2.close();

				}
				
				numCiclo++;
			}
			
			rs.close();
			ps.close();
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
			return nombreArchivo;
			
		}catch(Throwable t){
			throw new AppException("Error al generar pdf de la operacion solicitada", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
	
	/**
 * M�todo que obtiene la informaci�n de
 * @param producto Es el producto de piso uno con tasas parametrizadas
 * @param plazo Es el plazo en dias con el que se obtiene la tasa parametricada.
 * @return HashMap con la tasa base y la sobretasa
 * es usado para la pantalla
 * Nafin-Nafinet Descuento Elect�nico - Cr�dito Electr�nico-Consultas-Descarga de Tablas Emergentes
 * */
  public List consultarTablasEmergentesExt(String intermediario) throws NafinException{
		AccesoDB con = new AccesoDB();
		HashMap tablasEmergentes = new HashMap();
    PreparedStatement ps = null;
    ResultSet rs = null;
    String strSQL = "";
	List registros = new ArrayList();//nueva

    System.out.println("ServiciosNafElectBean::consultarTablasEmergentes(S)");

		try {
			con.conexionDB();
      strSQL = " SELECT SEM.IC_SOLIC_EMERGENTES,"+
							 " SEM.IG_NUMERO_DOCTOS,"+
							 " SEM.FN_IMPORTE_DOCTO,"+
							 " SEM.FN_IMPORTE_DSCTO,"+
							 " TO_CHAR(SEM.DF_CARGA, 'DD/MM/YYYY'),"+
							 " SEM.IC_USUARIO,"+
							 " SEM.CG_NOMBRE_USUARIO,"+
							 " CAT.CD_DESCRIPCION AS PROGRAMA " +
							 " FROM COM_SOLIC_EMERGENTES SEM, "+
							 " COMCAT_OPER_EMERGENTES   CAT " +
							 " WHERE " +
							 "	SEM.CD_CLAVE   = CAT.CD_CLAVE  and " +
							 " SEM.IC_IF 		= "+intermediario+
							 " ORDER BY SEM.IC_SOLIC_EMERGENTES";

      ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();

			while(rs.next()){
				tablasEmergentes = new HashMap();
				tablasEmergentes.put("icSolicitud",rs.getString(1).trim());
				tablasEmergentes.put("numeroDocumentos",rs.getString(2).trim());
				tablasEmergentes.put("montoDocumentos",rs.getString(3).trim());
				tablasEmergentes.put("montoDescuentos",rs.getString(4).trim());
				tablasEmergentes.put("fechaEnvio",rs.getString(5).trim());
				tablasEmergentes.put("usuario",rs.getString(6).trim());
				tablasEmergentes.put("nombreUsuario",rs.getString(7).trim());
				tablasEmergentes.put("programa",rs.getString(8).trim());
				registros.add(tablasEmergentes);
				
			}

			//tablasEmergentes.put("registros", Integer.toString(i));

			rs.close();
			ps.close();

			return registros;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
      System.out.println("ServiciosNafElectBean::consultarTablasEmergentes(E)");
		}
	}



}	//fin de clase
