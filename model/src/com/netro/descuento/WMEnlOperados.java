package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class WMEnlOperados implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

	public WMEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_wm";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,5,6,7,16,41)";
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9)";
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7)";
	}

	public String getInsertaDoctosOpe(){
		return "insert into "+getTablaDoctosOpe()+" (" +
					" ic_documento,"   +
					" ic_estatus_docto,"   +
					" df_fecha_venc,"   +
					" cg_razon_social_pyme,"   +
					" ig_numero_docto, "   +
					" fn_monto,"   +
					" in_tasa_aceptada,"   +
					" in_numero_proveedor,"   +
					" df_fecha_docto,"   +
					" df_fecha_descuento,"   +
					" cg_compania,"   +
					" cg_razon_social_if) ";
	}


	public String getDoctosOperados(Hashtable alParamEPO){
		return	" SELECT /*+index(d) index(a) use_nl(d ds s cpe a i)*/"   +
					"         d.ic_documento,"   +
//					"         DECODE (cpe.cg_tipo_pyme, 'M', 41, 4) ic_estatus_docto,"   +
					"         d.ic_estatus_docto,"   +
					"         d.df_fecha_venc,"   +
					"         SUBSTR (d.cg_campo3, 1, 100) AS nombrepyme,"   +
					"         d.ig_numero_docto,"   +
					"         d.fn_monto,"   +
					"         ds.in_tasa_aceptada,"   +
					"         TO_NUMBER (SUBSTR (d.cg_campo2, 1, 9)) AS numeroproveedor,"   +
					"         d.df_fecha_docto AS fechaemision,"   +
					"         a.df_fecha_hora AS fechaoperacion,"   +
					"         TO_NUMBER (SUBSTR (d.cg_campo1, 1, 4)) AS numcompania,"   +
					"         SUBSTR (i.cg_razon_social, 1, 50) AS nombreif"   +
					"   FROM com_documento d,"   +
					"        com_docto_seleccionado ds,"   +
					"        com_solicitud s,"   +
					"        comrel_pyme_epo cpe,"   +
					"        com_acuse3 a,"   +
					"        comcat_if i"   +
					"  WHERE a.cc_acuse = s.cc_acuse"   +
					"    AND s.ic_documento = ds.ic_documento"   +
					"    AND ds.ic_documento = d.ic_documento"   +
					"    AND d.ic_epo = cpe.ic_epo"   +
					"    AND d.ic_pyme = cpe.ic_pyme"   +
					"    AND d.ic_if = i.ic_if"   +
					"    AND a.df_fecha_hora >= TRUNC (SYSDATE)"   +
					"    AND a.df_fecha_hora < TRUNC (SYSDATE + 1)"   +
					"    AND d.ic_epo = "+ic_epo;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
			
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));
		String condicion = "";
		
		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
				condicion =
					" and ce.dc_fecha_cambio >= (SYSDATE -3)";
			} else {
				condicion =
					" and ce.dc_fecha_cambio >= (SYSDATE -1)";
			}		
			 
			return " SELECT "   +
					"        d.ic_documento, "   +
					"        d.ic_estatus_docto, "   +
					"        d.df_fecha_venc,"   +
					"        SUBSTR (d.cg_campo3, 1, 100) AS nombrepyme, "   +
					"        d.ig_numero_docto,"   +
					"        d.fn_monto, "   +
					"        0 AS in_tasa_aceptada,"   +
					"        TO_NUMBER (SUBSTR (d.cg_campo2, 1, 9)) AS numeroproveedor,"   +
					"        d.df_fecha_docto AS fechaemision, "   +
					"        ce.dc_fecha_cambio AS fechaoperacion,"   +
					"        TO_NUMBER (SUBSTR (d.cg_campo1, 1, 4)) AS numcompania, "   +
					"        NULL AS nombreif"   +
					"   FROM com_documento d, comhis_cambio_estatus ce"   +
					"  WHERE d.ic_documento = ce.ic_documento"   +
					condicion+					
					"    AND ce.dc_fecha_cambio < TRUNC (SYSDATE + 1)"   +
					"    AND d.ic_estatus_docto IN (5, 6, 7)"   +
					"    AND d.ic_epo = "+ic_epo+
					"    and ce.IC_CAMBIO_ESTATUS IN (4,5,6) ";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		String condicion = "";
		//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+

		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-3)";
		} else {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-1)";
		}

		String qrySentencia = " select d.ic_documento, 9 as ic_estatus_docto"+
					"	, d.df_fecha_venc"+
					" , substr(d.cg_campo3,1,100) as nombrePyme, d.ig_numero_docto, d.fn_monto"+
					" , NULL as tasa_aceptada " +
					" , TO_NUMBER(substr(d.cg_campo2,1,9)) as numeroProveedor"+
					" , d.df_fecha_docto as fechaEmision"+
					" , NULL as fecha_descuento " +
					" , TO_NUMBER(substr(d.cg_campo1,1,4)) as numCompania"+
					" , NULL AS nombreif " +
					" from com_documento d, comcat_pyme p "+
					" where"+
					" d.ic_estatus_docto=9 "+	//9 = Vencido sin operar
					//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+
					condicion+
					" and d.df_fecha_venc < TRUNC(SYSDATE+"+diasMinimos+") "+
					" and d.ic_epo = "+ic_epo+
					" and d.ic_pyme = p.ic_pyme";
		return qrySentencia;
	}


}
