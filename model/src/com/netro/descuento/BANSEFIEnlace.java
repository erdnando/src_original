package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class BANSEFIEnlace extends EpoPEF implements EpoEnlace,Serializable {


	public String getTablaDocumentos()	{	return "COMTMP_DOCTOS_PUB_BANSEFI";	}
	public String getTablaAcuse()			{	return "COM_DOCTOS_PUB_ACU_BANSEFI";	}
	public String getTablaErrores()		{	return "COM_DOCTOS_ERR_PUB_BANSEFI";	}

	public String getDocuments(){
		return  " select IG_NUMERO_DOCTO "+
				" , CG_PYME_EPO_INTERNO "+
				" , DF_FECHA_DOCTO "+
				" , DF_FECHA_VENC "+
				" , IC_MONEDA "+
				" , FN_MONTO "+
				" , CS_DSCTO_ESPECIAL "+
				" , CT_REFERENCIA "+
				" , CG_CAMPO1 "+
				" , CG_CAMPO2 "+
				" , CG_CAMPO3 "+
				" , CG_CAMPO4 "+
				" , CG_CAMPO5 "+
				" , '' as ic_if"+
				" , '' AS IC_NAFIN_ELECTRONICO "+ getCamposPEF()+
				" from " + getTablaDocumentos();
	}
	
	public String getInsertaErrores(ErroresEnl err){
		return	"insert into "+ getTablaErrores()+" (IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR, " + getCamposDigitoIdentificador() + ") "+
					" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"'"+ ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+")";
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set " + 
					"  IN_TOTAL_PROC="+acu.getInTotalProc()+
					" ,FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
					" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+
					" ,IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+
					" ,FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
					" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+
					" ,IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
					" ,DF_FECHAHORA_CARGA = SYSDATE " +
					" ,CS_ESTATUS='"+acu.getCsEstatus()+
					"' where CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}
	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE, "+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		return " ";
	}
	
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }

}
