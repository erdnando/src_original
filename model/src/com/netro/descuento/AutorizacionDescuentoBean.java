package com.netro.descuento;


import com.nafin.descuento.ws.CuentasPymeIFWS;
import com.nafin.descuento.ws.DocumentoIFWS;
import com.nafin.descuento.ws.DocumentoIFWSV2;
import com.nafin.descuento.ws.LimitesEpo;
import com.nafin.descuento.ws.ProcesoIFWSInfo;
import com.nafin.descuento.ws.ProcesoIFWSInfoV2;
import com.nafin.descuento.ws.ProveedoresCFDI;

import com.netro.afiliacion.Afiliacion;
import com.netro.exception.Horario;
import com.netro.exception.NafinException;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.net.URLEncoder;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.ClaseDESBase64;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CrearArchivosAcuses;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
import netropology.utilerias.*;
import com.netro.pdf.ComunesPDF;

import java.util.Arrays;

import java.util.Set;

import org.apache.commons.logging.Log;

/**
 * EJB que contiene la logica de los procesos asociados a la autorizacion
 * del descuento por parte del Intermediario Financiero
 *
 */
@Stateless(name = "AutorizacionDescuentoEJB" , mappedName = "AutorizacionDescuentoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutorizacionDescuentoBean implements AutorizacionDescuento {

	private final static Log log = ServiceLocator.getInstance().getLog(AutorizacionDescuentoBean.class);

	public final static int PRODUCTO_DSCTO = 1;

	public static final int CLAVE_TASA_USUARIO_FINAL = 1;
	public static final int CLAVE_MONEDA = 3;
	public static final int CLAVE_OFICINA = 5;
	public static final int CLAVE_TIPO_CREDITO = 7;
	public static final int CLAVE_ESQUEMA_AMORT = 9;
	public static final int CLAVE_CLASE_DOCTO = 11;
	public static final int CLAVE_TABLA_AMORT = 13;
	public static final int CLAVE_TASA_IF = 15;


	public void eliminarAmortTemp (String claveRegistroTemporal)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		try {
			con.conexionDB();
			qrySentencia = "delete from comtmp_amortizacion" +
				" where ic_amortizacion=" + claveRegistroTemporal;
			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public void eliminarDatosTempExt (String numeroProcesoSolicitudExt)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		try {
			con.conexionDB();

			qrySentencia = "delete from comtmp_amortizacion"+
				" where ic_proc_solicitud=" + numeroProcesoSolicitudExt;
			con.ejecutaSQL(qrySentencia);

			qrySentencia = "delete from comtmp_solicitud"+
				" where ic_proc_solicitud=" + numeroProcesoSolicitudExt;

			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);

		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public void eliminarDatosTempExtCargaMasiva (String numeroProceso)
		throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		try {
			con.conexionDB();
			qrySentencia="DELETE COMTMP_AMORTIZACION " +
				"WHERE ic_proc_solicitud in (SELECT IC_PROC_SOLICITUD " +
				"FROM COMTMP_SOLICITUD " +
				"WHERE ig_numero_proceso = " + numeroProceso+")";
			con.ejecutaSQL(qrySentencia);

			qrySentencia="delete from comtmp_solicitud where ig_numero_proceso = " + numeroProceso;
			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);

		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	public boolean esFactorajeVencido(String claveEpo)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
                boolean ok = true;
		try {
			con.conexionDB();

			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_vencido = 'S'"   +
				"    AND ic_epo = " + claveEpo;

			rs = con.queryDB(qrySentencia);
			rs.next();

			if(rs.getInt(1) > 0) {
				ok =  true;
			} else {
				ok = false;
			}
			con.cierraStatement();
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
        return ok;
	}

	public boolean esFactorajeDistribuido(String claveEpo)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
                boolean ok = true;

		try {
			con.conexionDB();

			qrySentencia=
				" SELECT COUNT (1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_distribuido = 'S'"   +
				"    AND ic_epo = " + claveEpo;

			rs = con.queryDB(qrySentencia);
			rs.next();

			if(rs.getInt(1) > 0) {
				ok = true;
			} else {
				ok =  false;
			}
			con.cierraStatement();
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
        return ok;
	}

	public boolean faltanAmortTempPorCapturar (String numeroProcesoSolicitudExt, int numeroAmortCapturar)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
                boolean ok = true;

		try {
			con.conexionDB();

			qrySentencia="select count(*) as tmpNumAmortizaciones from comtmp_amortizacion"+
				" where ic_proc_solicitud=" + numeroProcesoSolicitudExt;

			rs = con.queryDB(qrySentencia);
			rs.next();

			int numAmortCapturadas = rs.getInt("TMPNUMAMORTIZACIONES");

			if (numAmortCapturadas < numeroAmortCapturar) {
				ok = true;
			} else {
				ok = false;
			}
			con.cierraStatement();
		} catch (Exception e) {
			log.error("Error inesperado: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
        return ok;
	}


	public void generarSolicitudesExterna(String numeroProcesoSolicitudExt,
			String acuse, String claveUsuario,
			String montoDsctoMN, String montoDsctoDL,
			String montoMN, String montoDL)
		throws NafinException {

		boolean error = false;

		AccesoDB con  = new AccesoDB();
		try {
			con.conexionDB();
			String qrySentencia = "";
			String folio = "";

			setAcuse(acuse, montoMN, null, montoDsctoMN,
							montoDL, null, montoDsctoDL,
							claveUsuario, "", con);

			String qrySolics = "SELECT ic_proc_solicitud FROM COMTMP_SOLICITUD"+
				" WHERE ig_numero_proceso = " + numeroProcesoSolicitudExt;
			ResultSet rsSolicitudes = con.queryDB(qrySolics);
			String icProcSolicitud = "";

			while(rsSolicitudes.next()) {
				icProcSolicitud = rsSolicitudes.getString(1).trim();
				folio = getFolioSolicitud(con);

				qrySentencia = "insert into com_solicitud ( " +
						" ic_folio, ic_esquema_amort, ic_tipo_credito, " +
						" ic_oficina, cc_acuse, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap, " +
						" cg_perio_pago_int, cg_desc_bienes, ic_clase_docto, " +
						" cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif, " +
						" ic_tabla_amort, in_numero_amort, df_ppc, df_ppi, " +
						" df_v_documento, df_v_descuento, in_dia_pago, " +
						" df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic, " +
						" df_fecha_solicitud, cs_tipo_solicitud, cg_emisor, ic_bloqueo) " +
						" select '" + folio + "', ic_esquema_amort, ic_tipo_credito, " +
						" ic_oficina, '" + acuse + "', ig_plazo, cg_tipo_plazo, cg_perio_pago_cap, " +
						" cg_perio_pago_int, cg_desc_bienes, ic_clase_docto, " +
						" cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif, " +
						" ic_tabla_amort, in_numero_amort, df_ppc, df_ppi, " +
						" df_v_documento, df_v_descuento, in_dia_pago, " +
						" df_etc, cg_lugar_firma, ig_numero_contrato, 1, " +
						" sysdate, 'E', cg_emisor ,1 " + //ic_bloqueo = 1 Cancelada... para que el APi la ignore.
						" from comtmp_solicitud " +
						" where ic_proc_solicitud="+icProcSolicitud;

				con.ejecutaSQL(qrySentencia);

				qrySentencia = "insert into com_docto_ext ( " +
					"ic_folio, ic_if, ic_pyme, ic_tasa, cg_rmuf, fn_stuf " +
					", fn_tnuf, ic_moneda, ig_numero_docto, ig_numero_prov " +
					", ig_numero_factura, fn_monto_docto, fn_monto_dscto " +
					", df_docto ) " +
					"select '" + folio + "', ic_if, ic_pyme, ic_tasa, cg_rmuf, fn_stuf " +
					", fn_tnuf, ic_moneda, ig_numero_docto, ig_numero_prov " +
					", ig_numero_factura, fn_monto_docto, fn_monto_dscto " +
					", df_etc " +
					"from comtmp_solicitud " +
					"where ic_proc_solicitud="+icProcSolicitud;

				con.ejecutaSQL(qrySentencia);

				qrySentencia = "insert into com_amortizacion (ic_amortizacion, ic_folio, in_numero " +
					", fn_importe, df_fecha) " +
					"select seq_com_amortizacion_ic_amor.NEXTVAL, '" + folio + "', in_numero " +
					", fn_importe, df_fecha " +
					"from comtmp_amortizacion " +
					"where ic_proc_solicitud="+icProcSolicitud;

				con.ejecutaSQL(qrySentencia);
			}
			con.cierraStatement();
		} catch (Exception e) {
			log.error("Error en generarSolicitudesExterna: ", e);
			error = true;
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				if (error == true) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
				con.cierraConexionDB();
			}
		}
	}

	public Vector getAmortizaciones (String folio)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia  = "select in_numero, fn_importe"+
				" , TO_CHAR(df_fecha,'dd/mm/yyyy') as df_fecha"+
				" from com_amortizacion"+
				" where ic_folio="+folio+
				" order by in_numero";

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(3);
				registro.add(0, (rs.getString("IN_NUMERO") == null)?"":rs.getString("IN_NUMERO") );
				registro.add(1, (rs.getString("FN_IMPORTE") == null)?"":rs.getString("FN_IMPORTE") );
				registro.add(2, (rs.getString("DF_FECHA") == null)?"":rs.getString("DF_FECHA") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			log.error("Error en getAmortizaciones: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public Vector getAmortsTemps (String numeroProcesoSolicitudExt)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia = "select ic_amortizacion, in_numero, fn_importe" +
				" , TO_CHAR(df_fecha,'dd/mm/yyyy') as df_fecha" +
				" from comtmp_amortizacion" +
				" where ic_proc_solicitud = " + numeroProcesoSolicitudExt +
				" order by in_numero";

			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				Vector registro = new Vector(4);

				registro.add(0, (rs.getString("IC_AMORTIZACION") == null)?"":rs.getString("IC_AMORTIZACION") );
				registro.add(1, (rs.getString("IN_NUMERO") == null)?"":rs.getString("IN_NUMERO"));
				registro.add(2, (rs.getString("FN_IMPORTE") == null)?"":rs.getString("FN_IMPORTE"));
				registro.add(3, (rs.getString("DF_FECHA") == null)?"":rs.getString("DF_FECHA"));

				registros.add(registro);
			}
			rs.close();
			con.cierraStatement();


			return registros;

		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public String [] getAmortTemp (String claveRegistroTemporal)
			throws NafinException {


		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		try {
			con.conexionDB();
			String [] amortizacion = new String[3];

			qrySentencia = "select in_numero, fn_importe" +
				" , TO_CHAR(df_fecha,'dd/mm/yyyy') as df_fecha" +
				" from comtmp_amortizacion" +
				" where ic_amortizacion=" + claveRegistroTemporal;

			rs = con.queryDB(qrySentencia);
			rs.next();
			amortizacion[0] = rs.getString("IN_NUMERO");
			amortizacion[1] = rs.getString("FN_IMPORTE");
			amortizacion[2] = rs.getString("DF_FECHA");
			con.cierraStatement();
			return amortizacion;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public String [] getCamposAdicionales(String claveEPO)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		int i = 0;
		String [] camposAdicionales = new String[5];

		try {
			con.conexionDB();
		// El query para obtener los campos adicionales definidos por la epo en comrel_visor
		  qrySentencia = "select ic_no_campo, INITCAP(CG_NOMBRE_CAMPO) as nombreCampo" +
					" from comrel_visor where ic_epo= " + claveEPO +
					" and ic_producto_nafin = 1 order by ic_no_campo";

		  rs = con.queryDB(qrySentencia);

			while(rs.next()) {
				camposAdicionales[i] = rs.getString("NOMBRECAMPO");
				i++;
			}
			rs.close();
			con.cierraStatement();

			return camposAdicionales;

		} catch (Exception e) {
			log.error("Error en getCamposAdicionales: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public String [] getCliente(String numeroSirac)
			throws NafinException {


		String [] cliente = new String [2];
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		try
		{
			con.conexionDB();

			qrySentencia = "select ic_pyme, cg_razon_social" +
				" from comcat_pyme " +
				" where in_numero_sirac=" + numeroSirac +
				" and cs_habilitado='S'";
			log.debug("getCliente()::" + qrySentencia);
			rs = con.queryDB(qrySentencia);

			if (rs.next()) {
				cliente[0] = rs.getString("IC_PYME");
				cliente[1] = rs.getString("CG_RAZON_SOCIAL");
			} else {
				// El cliente no existe o no est� habilitado
				throw new NafinException("DSCT0004");
			}
			con.cierraStatement();
			return cliente;
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public Vector getDoctosProcesados (String acuse, String claveEPO, String claveIF)
		throws NafinException {

		AccesoDB 			con  					= new AccesoDB();
		String 				qrySentencia 			= null;
		String 				campos 					= "";
		ResultSet 			rs 						= null;
		PreparedStatement 	ps						= null;
		Vector 				registros 				= new Vector();
		int 				numCamposAdicionales	= 0;
		try {
			con.conexionDB();
/*
			Context context = ContextoJNDI.getInitialContext();
			ISeleccionDocumentoHome seleccionDocumentoHome = (ISeleccionDocumentoHome) context.lookup("SeleccionDocumentoEJB");
			ISeleccionDocumento BeanSeleccionDocumento = seleccionDocumentoHome.create();
*/
			String sFechaVencPyme = "";//BeanSeleccionDocumento.operaFechaVencPyme(claveEPO);

			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);

			qrySentencia =
				" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
				"   FROM dual"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(claveEPO));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
			}
			rs.close();ps.close();

			numCamposAdicionales = getNumCamposAdicionales(claveEPO, con);
			for (int i=1; i <= numCamposAdicionales; i++) {
				campos += ", d.CG_CAMPO" + i;
			}
			// Optimizado
			qrySentencia =
				" SELECT   /*+ use_nl(s,d,m,i,ce,ds,ie,ci,p,pe)*/ "   +
				"        d.ic_documento, DECODE(CS_OPERA_FISO,'S',99999,p.in_numero_sirac) as in_numero_sirac,DECODE(ds.CS_OPERA_FISO,'S', ifs.cg_razon_social ,p.cg_razon_social) as nombrepyme, d.ig_numero_docto, pe.cg_pyme_epo_interno,"   +
				"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
				"        d.ic_moneda, m.cd_nombre, d.fn_monto, DECODE(CS_OPERA_FISO,'S',NVL(ds.in_importe_interes_fondeo,0),ds.in_importe_interes) as in_importe_interes, DECODE(CS_OPERA_FISO,'S',NVL(ds.in_importe_recibir_fondeo,0),ds.in_importe_recibir) as in_importe_recibir,"+
				" 			DECODE(CS_OPERA_FISO,'S',NVL(ds.in_tasa_aceptada_fondeo,0),ds.in_tasa_aceptada) as in_tasa_aceptada,"   +
				"        d.fn_monto_dscto, s.ic_folio, "+
				"        s.ig_plazo as plazo,"   +
				"        d.fn_porc_anticipo, d.fn_monto - d.fn_monto_dscto AS recursogarantia, s.ic_folio, s.cc_acuse, s.ic_oficina, d.ic_pyme,"   +
				"        ce.cg_razon_social AS nombreepo, d.ic_if, ci.cg_razon_social AS nombreif, TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta,"   +
				"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud, s.ic_tasaif, DECODE(ds.CS_OPERA_FISO,'S',p.cg_razon_social,d.ct_referencia) as ct_referencia"+
				campos+" , "+
				"		d.cs_dscto_especial,"   +
				"     d.ic_estatus_docto,"+
				"  	  DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS benef"   +//FODEA 042 - 2009 ACF
				"	   , DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS banco_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS sucursal_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS cta_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS porc_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS importe_recibir"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme,"   +//FODEA 042 - 2009 ACF
				"		tf.cg_nombre as tipo_factoraje, " +
				"	( CASE WHEN ds.cs_opera_fiso= 'S' then  p.cg_razon_social else '' END)  as referencia, "+
				"   'AutorizacionDescuentoBean::getDoctosProcesados' origenquery " +
				"   FROM com_solicitud s " +
				"	, com_documento d " +
				"	, comcat_moneda m"   +
				"	, comcat_if i " +
				"	, comcat_epo ce " +
				"	, com_docto_seleccionado ds "+
				"	, comrel_if_epo ie " +
				"	, comcat_if ci " +
				"	, comcat_pyme p " +
				"	, comrel_pyme_epo pe " +
				"	, comcat_tipo_factoraje tf, " +
				" 		comcat_if ifs "+
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"		AND ifs.ic_if = ds.ic_if "+
				"    AND ds.ic_epo = d.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ce.ic_epo"   +
				"    AND d.ic_if = ci.ic_if"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.cs_dscto_especial = tf.cc_tipo_factoraje "   +
				"    AND s.ic_documento = d.ic_documento"   +
				"    AND d.ic_if = ?"+
				"    AND s.cc_acuse = ?"+
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND ie.ic_if (+) = d.ic_beneficiario"   +
				"    AND ie.ic_epo (+) = d.ic_epo"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  ORDER BY p.cg_razon_social"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(claveIF));
			ps.setString(2,acuse);
			rs = ps.executeQuery();
			while (rs.next()) {
				Vector registro = new Vector(30+numCamposAdicionales);
				registro.add(0, (rs.getString("IN_NUMERO_SIRAC") == null)?"":rs.getString("IN_NUMERO_SIRAC") );
				registro.add(1, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );
				registro.add(2, (rs.getString("IG_NUMERO_DOCTO") == null)?"":rs.getString("IG_NUMERO_DOCTO") );
				registro.add(3, (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO") );
				registro.add(4, (rs.getString("DF_FECHA_VENC") == null)?"":rs.getString("DF_FECHA_VENC") );
				registro.add(5, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA") );
				registro.add(6, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") ); //monedanombre
				registro.add(7, (rs.getString("FN_MONTO") == null)?"":rs.getString("FN_MONTO") );
				registro.add(8, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(9, (rs.getString("FN_PORC_ANTICIPO") == null)?"":rs.getString("FN_PORC_ANTICIPO") );
				registro.add(10, (rs.getString("RECURSOGARANTIA") == null)?"":rs.getString("RECURSOGARANTIA") );
				registro.add(11, (rs.getString("IN_IMPORTE_INTERES") == null)?"":rs.getString("IN_IMPORTE_INTERES") );
				registro.add(12, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR") );
				registro.add(13, (rs.getString("IN_TASA_ACEPTADA") == null)?"":rs.getString("IN_TASA_ACEPTADA") );
				registro.add(14, (rs.getString("PLAZO") == null)?"":rs.getString("PLAZO") );
				registro.add(15, (rs.getString("CG_PYME_EPO_INTERNO") == null)?"":rs.getString("CG_PYME_EPO_INTERNO") );
				registro.add(16, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );
				registro.add(17, (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") );
				registro.add(18, (rs.getString("CC_ACUSE") == null)?"":rs.getString("CC_ACUSE") );
				registro.add(19, (rs.getString("IC_OFICINA") == null)?"":rs.getString("IC_OFICINA") );
				registro.add(20, (rs.getString("IC_PYME") == null)?"":rs.getString("IC_PYME") );
				registro.add(21, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(22, (rs.getString("IC_IF") == null)?"":rs.getString("IC_IF") );
				registro.add(23, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(24, (rs.getString("DF_ALTA") == null)?"":rs.getString("DF_ALTA") );
				registro.add(25, (rs.getString("DF_FECHA_SOLICITUD") == null)?"":rs.getString("DF_FECHA_SOLICITUD") );
				registro.add(26, (rs.getString("IC_TASAIF") == null)?"":rs.getString("IC_TASAIF") );
				registro.add(27, (rs.getString("CT_REFERENCIA") == null)?"":rs.getString("CT_REFERENCIA") );
				registro.add(28, (rs.getString("IC_ESTATUS_DOCTO") == null)?"":rs.getString("IC_ESTATUS_DOCTO") );
				registro.add(29, (rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				registro.add(30, (rs.getString("BANCO_BENEF") == null) ? "" : rs.getString("BANCO_BENEF"));
				registro.add(31, (rs.getString("SUCURSAL_BENEF") == null) ? "" : rs.getString("SUCURSAL_BENEF"));
				registro.add(32, (rs.getString("CTA_BENEF") == null) ? "" : rs.getString("CTA_BENEF"));
				registro.add(33, (rs.getString("PORC_BENEF") == null) ? "" : rs.getString("PORC_BENEF"));
				registro.add(34, (rs.getString("IMPORTE_RECIBIR") == null) ? "" : rs.getString("IMPORTE_RECIBIR"));
				registro.add(35, (rs.getString("NETO_REC_PYME") == null) ? "" : rs.getString("NETO_REC_PYME"));
				registro.add(36, (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE"));
				registro.add(37, (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA"));
				
				for (int i = 1; i <= numCamposAdicionales; i++) {
					registro.add(37 + i, (rs.getString("CG_CAMPO" + i) == null)?"":rs.getString("CG_CAMPO" + i));
				}
				registros.add(registro);
			}
			rs.close();
			if(ps!=null) ps.close();
			log.debug(qrySentencia);
			
//			con.cierraStatement();
			return registros;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error en getDoctosProcesados: ",e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	private Vector getDoctosProcesadosC(String acuse, String claveEPO, String claveIF, AccesoDB con)
		throws NafinException {
		String 				qrySentencia 			= null;
		String 				campos 					= "";
		ResultSet			rs 						= null;
		PreparedStatement 	ps						= null;
		Vector 				registros 				= new Vector();
		int 				numCamposAdicionales 	= 0;
    boolean hayFondeoPropio = false;


		try {
			log.info("getDoctosProcesadosC(E)");
/*
			Context context = ContextoJNDI.getInitialContext();
			ISeleccionDocumentoHome seleccionDocumentoHome = (ISeleccionDocumentoHome) context.lookup("SeleccionDocumentoEJB");
			ISeleccionDocumento BeanSeleccionDocumento = seleccionDocumentoHome.create();
*/
			String sFechaVencPyme = "";//BeanSeleccionDocumento.operaFechaVencPyme(claveEPO);

			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);
			try {
				hayFondeoPropio = hayFondeoPropio(claveIF,claveEPO);
			} catch(Throwable t) {
				throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
			}
			if (!hayFondeoPropio) {

        qrySentencia =
          " SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
          "   FROM dual"  ;
        ps = con.queryPrecompilado(qrySentencia);
        ps.setInt(1, Integer.parseInt(claveEPO));
        rs = ps.executeQuery();
        ps.clearParameters();
        if(rs.next()) {
          fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
        }
        rs.close();ps.close();
      }

			numCamposAdicionales = getNumCamposAdicionalesC(claveEPO, con);

			for (int i=1; i <= numCamposAdicionales; i++) {
				campos += ", d.CG_CAMPO" + i;
			}

// Optimizado EGB Garant�a 001 - 2004
			qrySentencia =
				" SELECT   /*+use_nl(s,ds,d,pe,ie,p,i,ci,ce,m)*/"   +
				"        d.ic_documento, DECODE(CS_OPERA_FISO,'S',99999,p.in_numero_sirac) as in_numero_sirac, DECODE(ds.CS_OPERA_FISO,'S', ifs.cg_razon_social ,p.cg_razon_social) as nombrepyme, d.ig_numero_docto, pe.cg_pyme_epo_interno,"   +
				"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
				"        d.ic_moneda, m.cd_nombre, d.fn_monto, DECODE(CS_OPERA_FISO,'S',NVL(ds.in_importe_interes_fondeo,0),ds.in_importe_interes) as in_importe_interes, DECODE(CS_OPERA_FISO,'S',NVL(ds.in_importe_recibir_fondeo,0),ds.in_importe_recibir) as in_importe_recibir "+
				" 			, DECODE(CS_OPERA_FISO,'S',NVL(ds.in_tasa_aceptada_fondeo,0),ds.in_tasa_aceptada) as IN_TASA_ACEPTADA,"   +
				"        d.fn_monto_dscto, s.ic_folio, "+
				"        s.ig_plazo plazo,"   +
				"        d.fn_porc_anticipo, d.fn_monto - d.fn_monto_dscto AS recursogarantia, s.ic_folio, s.cc_acuse, s.ic_oficina, d.ic_pyme,"   +
				"        ce.cg_razon_social AS nombreepo, d.ic_if, ci.cg_razon_social AS nombreif, TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta,"   +
				"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud, s.ic_tasaif, DECODE(ds.CS_OPERA_FISO,'S',p.cg_razon_social,d.ct_referencia) as ct_referencia "+
						 campos+" , "+
				"		d.cs_dscto_especial,"   +
				"     d.ic_estatus_docto,"+
				"  	  DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS benef"   +//FODEA 042 - 2009 ACF
				"	   , DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS banco_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS sucursal_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS cta_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS porc_benef"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS importe_recibir"   +//FODEA 042 - 2009 ACF
				"  	, DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme,"   +//FODEA 042 - 2009 ACF
				"		tf.cg_nombre as tipo_factoraje, "   +
				"  		( CASE WHEN ds.cs_opera_fiso= 'S' then  p.cg_razon_social else '' END)  as referencia, "+
				"     'AutorizacionDescuentoBean::getDoctosProcesadosC' origenquery"   +
				"   FROM com_solicitud s,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_documento d,"   +
				"        comrel_pyme_epo pe,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_pyme p,"   +
				"        comcat_if i,"   +
				"        comcat_if ci,"   +
				"        comcat_epo ce,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_factoraje tf, "   +
				"			comcat_if ifs "+
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"	  AND ifs.ic_if = ds.ic_if "+
				"    AND ds.ic_epo = d.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ce.ic_epo"   +
				"    AND d.ic_if = ci.ic_if"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND s.ic_documento = d.ic_documento"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND d.cs_dscto_especial = tf.cc_tipo_factoraje"   +
				"    AND ie.ic_if (+) = d.ic_beneficiario"   +
				"    AND ie.ic_epo (+) = d.ic_epo"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"    AND d.ic_if = ?"   +
				"    AND s.cc_acuse = ?"   +
				"  ORDER BY p.cg_razon_social"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(claveIF));
			ps.setString(2,acuse);
			rs = ps.executeQuery();
			while (rs.next()) {
				Vector registro = new Vector(30+numCamposAdicionales);
				registro.add(0, (rs.getString("IN_NUMERO_SIRAC") == null)?"":rs.getString("IN_NUMERO_SIRAC") );
				registro.add(1, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );
				registro.add(2, (rs.getString("IG_NUMERO_DOCTO") == null)?"":rs.getString("IG_NUMERO_DOCTO") );
				registro.add(3, (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO") );
				registro.add(4, (rs.getString("DF_FECHA_VENC") == null)?"":rs.getString("DF_FECHA_VENC") );
				registro.add(5, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA") );
				registro.add(6, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") ); //monedanombre
				registro.add(7, (rs.getString("FN_MONTO") == null)?"":rs.getString("FN_MONTO") );
				registro.add(8, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(9, (rs.getString("FN_PORC_ANTICIPO") == null)?"":rs.getString("FN_PORC_ANTICIPO") );
				registro.add(10, (rs.getString("RECURSOGARANTIA") == null)?"":rs.getString("RECURSOGARANTIA") );
				registro.add(11, (rs.getString("IN_IMPORTE_INTERES") == null)?"":rs.getString("IN_IMPORTE_INTERES") );
				registro.add(12, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR") );
				registro.add(13, (rs.getString("IN_TASA_ACEPTADA") == null)?"":rs.getString("IN_TASA_ACEPTADA") );
				registro.add(14, (rs.getString("PLAZO") == null)?"":rs.getString("PLAZO") );
				registro.add(15, (rs.getString("CG_PYME_EPO_INTERNO") == null)?"":rs.getString("CG_PYME_EPO_INTERNO") );
				registro.add(16, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );
				registro.add(17, (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") );
				registro.add(18, (rs.getString("CC_ACUSE") == null)?"":rs.getString("CC_ACUSE") );
				registro.add(19, (rs.getString("IC_OFICINA") == null)?"":rs.getString("IC_OFICINA") );
				registro.add(20, (rs.getString("IC_PYME") == null)?"":rs.getString("IC_PYME") );
				registro.add(21, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO") );
				registro.add(22, (rs.getString("IC_IF") == null)?"":rs.getString("IC_IF") );
				registro.add(23, (rs.getString("NOMBREIF") == null)?"":rs.getString("NOMBREIF") );
				registro.add(24, (rs.getString("DF_ALTA") == null)?"":rs.getString("DF_ALTA") );
				registro.add(25, (rs.getString("DF_FECHA_SOLICITUD") == null)?"":rs.getString("DF_FECHA_SOLICITUD") );
				registro.add(26, (rs.getString("IC_TASAIF") == null)?"":rs.getString("IC_TASAIF") );
				registro.add(27, (rs.getString("CT_REFERENCIA") == null)?"":rs.getString("CT_REFERENCIA") );
				registro.add(28, (rs.getString("IC_ESTATUS_DOCTO") == null)?"":rs.getString("IC_ESTATUS_DOCTO") );
				registro.add(29, (rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				registro.add(30, (rs.getString("BANCO_BENEF") == null) ? "" : rs.getString("BANCO_BENEF"));
				registro.add(31, (rs.getString("SUCURSAL_BENEF") == null) ? "" : rs.getString("SUCURSAL_BENEF"));
				registro.add(32, (rs.getString("CTA_BENEF") == null) ? "" : rs.getString("CTA_BENEF"));
				registro.add(33, (rs.getString("PORC_BENEF") == null) ? "" : rs.getString("PORC_BENEF"));
				registro.add(34, (rs.getString("IMPORTE_RECIBIR") == null) ? "" : rs.getString("IMPORTE_RECIBIR"));
				registro.add(35, (rs.getString("NETO_REC_PYME") == null) ? "" : rs.getString("NETO_REC_PYME"));
				registro.add(36, (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE"));
				registro.add(37,(rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA"));
				for (int i = 1; i <= numCamposAdicionales; i++) {
					registro.add(37 + i, (rs.getString("CG_CAMPO" + i) == null)?"":rs.getString("CG_CAMPO" + i));
				}

				registros.add(registro);
			}
			rs.close();
			if(ps!=null) ps.close();

//			con.cierraStatement();

			log.info("getDoctosProcesadosC(S)");
			return registros;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error en getDoctosProcesadosC: ", e);
			throw new NafinException("SIST0001");
		}
	}

	public Vector getDoctosProcesar (String claveDocto[], String claveIF, String ic_epo)
		throws NafinException {
		log.info("getDoctosProcesar(E)");
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		String clavesDocumento = "";

		Vector registros = new Vector();
    boolean hayFondeoPropio = false;

		try {
			con.conexionDB();
/*
			Context context = ContextoJNDI.getInitialContext();
			ISeleccionDocumentoHome seleccionDocumentoHome = (ISeleccionDocumentoHome) context.lookup("SeleccionDocumentoEJB");
			ISeleccionDocumento BeanSeleccionDocumento = seleccionDocumentoHome.create();
*/

			String sFechaVencPyme = "";//BeanSeleccionDocumento.operaFechaVencPyme(ic_epo);

			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);
			try {
				hayFondeoPropio = hayFondeoPropio(claveIF,ic_epo);
			} catch(Throwable t) {
				throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
			}
      if (!hayFondeoPropio) {
        qrySentencia =
          " SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
          "   FROM dual"  ;
        ps = con.queryPrecompilado(qrySentencia);
        ps.setInt(1, Integer.parseInt(ic_epo));
        rs = ps.executeQuery();
        ps.clearParameters();
        if(rs.next()) {
          fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
        }
        rs.close();ps.close();
      }

			if (claveDocto != null) {
				for(int i = 0; i < claveDocto.length; i++) {
					if (i == 0) {
						clavesDocumento = "?";
					} else {
						clavesDocumento = clavesDocumento + ", ?";
					}
				}
			} else {
				throw new NafinException("DSCT0017");
			}

			qrySentencia=
				" SELECT d.ic_documento,  DECODE(ds.CS_OPERA_FISO,'S',99999, p.in_numero_sirac) as in_numero_sirac,  DECODE(ds.CS_OPERA_FISO,'S', ifs.cg_razon_social ,p.cg_razon_social) as cg_razon_social, d.ig_numero_docto, pe.cg_pyme_epo_interno,"   +
				"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
				"        d.ic_moneda, m.cd_nombre, d.fn_monto, DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_IMPORTE_INTERES_FONDEO,0), ds.in_importe_interes) as in_importe_interes, DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_IMPORTE_RECIBIR_FONDEO,0),ds.in_importe_recibir) as in_importe_recibir,"+
				" 			DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_TASA_ACEPTADA_FONDEO,0) , ds.in_tasa_aceptada) as in_tasa_aceptada, d.fn_monto_dscto, "   +
				"        (d.df_fecha_venc"+sFechaVencPyme+" - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) plazo, "   +
				"        d.fn_porc_anticipo, d.fn_monto - d.fn_monto_dscto AS recursogarantia, d.cs_dscto_especial,"   +
				"  		 DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS benef"   +//FODEA 042 - 2009 ACF
				"		   , DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS banco_benef"   +//FODEA 042 - 2009 ACF
				"  		, DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS sucursal_benef"   +//FODEA 042 - 2009 ACF
				"  		, DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS cta_benef"   +//FODEA 042 - 2009 ACF
				"  		, DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS porc_benef"   +//FODEA 042 - 2009 ACF
				"  		, DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS importe_recibir"   +//FODEA 042 - 2009 ACF
				"  		, DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme,"   +//FODEA 042 - 2009 ACF
				"        e.cg_razon_social AS nombre_epo,"   +
				"        tf.cg_nombre as tipo_factoraje,"   +
				"  		( CASE WHEN ds.cs_opera_fiso= 'S' then  p.cg_razon_social else '' END)  as referencia"+
				"        ,'AutorizacionDescuentoBean.getDoctosProcesar()' as pantalla "  +
				"   FROM com_docto_seleccionado ds,"   +
				"        com_documento d,"   +
				"        comrel_pyme_epo pe,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_pyme p,"   +
				"        comcat_epo e,"   +
				"        comcat_if i,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto cd,"   +
				"        comcat_tipo_factoraje tf,"   +
				"			comcat_if ifs"+
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"		AND ifs.ic_if = ds.ic_if "+
				"    AND d.ic_estatus_docto = cd.ic_estatus_docto"   +
				"    AND ds.ic_epo = d.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.cs_dscto_especial = tf.cc_tipo_factoraje"   +
				"    AND d.ic_estatus_docto = 3"   +
				"    AND d.ic_if = ?"   +
				"    AND pe.cs_habilitado = 'S'"   +
				"    AND d.ic_documento IN ("+clavesDocumento+")"+
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND ie.ic_if (+) = d.ic_beneficiario"   +
				"    AND ie.ic_epo (+) = d.ic_epo"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  ORDER BY p.cg_razon_social"  ;
			PreparedStatement pstmt = con.queryPrecompilado(qrySentencia);
			pstmt.setInt(1, Integer.parseInt(claveIF) );
			int cont = 1;
			for(int i = 0; i < claveDocto.length; i++) {
				cont++;pstmt.setInt(cont , Integer.parseInt(claveDocto[i]));
			}
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Vector registro = new Vector(18);
				registro.add(0, (rs.getString("IC_DOCUMENTO") == null)?"":rs.getString("IC_DOCUMENTO") );
				registro.add(1, (rs.getString("IN_NUMERO_SIRAC") == null)?"":rs.getString("IN_NUMERO_SIRAC") );
				registro.add(2, (rs.getString("CG_RAZON_SOCIAL") == null)?"":rs.getString("CG_RAZON_SOCIAL") );	//Clave de la pyme
				registro.add(3, (rs.getString("IG_NUMERO_DOCTO") == null)?"":rs.getString("IG_NUMERO_DOCTO") );
				registro.add(4, (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO") );
				registro.add(5, (rs.getString("DF_FECHA_VENC") == null)?"":rs.getString("DF_FECHA_VENC") );
				registro.add(6, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA") );
				registro.add(7, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") ); //monedanombre
				registro.add(8, (rs.getString("FN_MONTO") == null)?"":rs.getString("FN_MONTO") );
				registro.add(9, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(10, (rs.getString("FN_PORC_ANTICIPO") == null)?"":rs.getString("FN_PORC_ANTICIPO") );
				registro.add(11, (rs.getString("RECURSOGARANTIA") == null)?"":rs.getString("RECURSOGARANTIA") );
				registro.add(12, (rs.getString("IN_IMPORTE_INTERES") == null)?"":rs.getString("IN_IMPORTE_INTERES") );
				registro.add(13, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR") );
				registro.add(14, (rs.getString("IN_TASA_ACEPTADA") == null)?"":rs.getString("IN_TASA_ACEPTADA") );
				registro.add(15, (rs.getString("PLAZO") == null)?"":rs.getString("PLAZO") );
				registro.add(16, (rs.getString("CG_PYME_EPO_INTERNO") == null)?"":rs.getString("CG_PYME_EPO_INTERNO") );
				registro.add(17, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL") );

				registro.add(18,(rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				registro.add(19,(rs.getString("BANCO_BENEF") == null) ? "" : rs.getString("BANCO_BENEF"));
				registro.add(20,(rs.getString("SUCURSAL_BENEF") == null) ? "" : rs.getString("SUCURSAL_BENEF"));
				registro.add(21,(rs.getString("CTA_BENEF") == null) ? "" : rs.getString("CTA_BENEF"));
				registro.add(22,(rs.getString("PORC_BENEF") == null) ? "" : rs.getString("PORC_BENEF"));
				registro.add(23,(rs.getString("IMPORTE_RECIBIR") == null) ? "" : rs.getString("IMPORTE_RECIBIR"));
				registro.add(24,(rs.getString("NETO_REC_PYME") == null) ? "" : rs.getString("NETO_REC_PYME"));
				registro.add(25,(rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO"));
				registro.add(26,(rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE"));
				registro.add(27,(rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA"));
				registros.add(registro);
			}

			rs.close();
			pstmt.close();

			return registros;

		} catch (NafinException ne) {
			log.error("getDoctosProcesar(Error): " + ne.getMessage());
			throw ne;
		} catch (Exception e) {
			log.error("getDoctosProcesar(Error): ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("AutorizacionDescuentoBean.getDoctosProcesar(S): ");
		}
	}

	public Vector getDoctosProcesarMasiva (String numProceso)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		Vector registros = new Vector();

		try {

			con.conexionDB();

			qrySentencia=" SELECT P.cg_razon_social, M.cd_nombre, TS.fn_monto_docto "+
				" , TS.fn_monto_dscto, to_char(TS.df_v_descuento, 'dd/mm/yyyy') as df_v_descuento"+
				" , TS.in_numero_amort " +
				" FROM COMTMP_SOLICITUD TS, COMCAT_PYME P, COMCAT_MONEDA M " +
				" WHERE TS.ic_pyme = P.ic_pyme AND TS.ic_moneda = M.ic_moneda " +
				" AND TS.ig_numero_proceso = " + numProceso;

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {
				Vector registro = new Vector(6);
				registro.add(0, (rs.getString("CG_RAZON_SOCIAL") == null)?"":rs.getString("CG_RAZON_SOCIAL") ); //Nombre de la pyme
				registro.add(1, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") );	//Nombre de la moneda
				registro.add(2, (rs.getString("FN_MONTO_DOCTO") == null)?"":rs.getString("FN_MONTO_DOCTO") );
				registro.add(3, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(4, (rs.getString("DF_V_DESCUENTO") == null)?"":rs.getString("DF_V_DESCUENTO") );
				registro.add(5, (rs.getString("IN_NUMERO_AMORT") == null)?"":rs.getString("IN_NUMERO_AMORT") );

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			log.error("Error en getDoctosProcesarMasiva: ",e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public String getCadenaFirmarWS(DocumentoIFWS[] documentos) {
        log.info("AutorizacionDescuentoBean::getCadenaFirmarWS(E)");
		
		DocumentoIFWSV2[] docs = new DocumentoIFWSV2[documentos.length];
		
		for(int i=0;i<documentos.length;i++){
			docs[i]=new DocumentoIFWSV2();
			docs[i].setClaveDocumento(documentos[i].getClaveDocumento());
			docs[i].setClaveEstatus(documentos[i].getClaveEstatus());
			docs[i].setAcusePyme(documentos[i].getAcusePyme());
			docs[i].setClaveBancoFondeo(documentos[i].getClaveBancoFondeo());
			docs[i].setClaveEpo(documentos[i].getClaveEpo());
			docs[i].setClaveMoneda(documentos[i].getClaveMoneda());			
			docs[i].setFechaEmision(documentos[i].getFechaEmision());			
			docs[i].setFechaSeleccionPyme(documentos[i].getFechaSeleccionPyme());			
			docs[i].setFechaPublicacion(documentos[i].getFechaPublicacion());
			docs[i].setFechaVencimiento(documentos[i].getFechaVencimiento());
			docs[i].setMontoDescuento(documentos[i].getMontoDescuento());
			docs[i].setTasaAceptada(documentos[i].getTasaAceptada());
			docs[i].setImporteInteres(documentos[i].getImporteInteres());			
			docs[i].setImporteRecibir(documentos[i].getImporteRecibir());
			docs[i].setNombreEpo(documentos[i].getNombreEpo());			
			docs[i].setNombreMoneda(documentos[i].getNombreMoneda());			
			docs[i].setNombrePyme(documentos[i].getNombrePyme());			
			docs[i].setNumeroDocumento(documentos[i].getNumeroDocumento());			
			docs[i].setNumeroProveedor(documentos[i].getNumeroProveedor());
			docs[i].setNumeroSirac(documentos[i].getNumeroSirac());
			docs[i].setTipoFactoraje(documentos[i].getTipoFactoraje());
			docs[i].setClaveRechazo(documentos[i].getClaveRechazo());   
			 
			}
		
		String str = new String();	 
		
		str = this.getCadenaFirmarWSV2(docs);
		
		log.info("AutorizacionDescuentoBean::getCadenaFirmarWS(S)");
		return str.toString();		
	}


	/**
	 * Obtiene la cadena a firmar por parte del IF, usando la autorizacion via
	 * WebService.
	 * La cadena est� confirmado unicamente por los atributos que tengan valor.
	 * @param documentos Arreglo de documentos que ser�n utilizados en el proceso
	 * de cambio de estatus v�a WebService.
	 * @return Cadena a firmar
	 */
	private String getCadenaFirmarWSV2(DocumentoIFWSV2[] documentos) {
		log.info("AutorizacionDescuentoBean::getCadenaFirmarWSV2(E)");
		StringBuffer str = new StringBuffer();
		if (documentos != null && documentos.length > 0) {
			for (int i = 0; i < documentos.length; i++) {
				if (documentos[i].getClaveDocumento() != null && !documentos[i].getClaveDocumento().equals("")) {
					str.append(documentos[i].getClaveDocumento());
				} else {
					throw new AppException("La Clave de Documento es requerida para cada uno de los documentos");
				}
				if (documentos[i].getClaveEstatus() != null && !documentos[i].getClaveEstatus().equals("")) {
					str.append("|"+documentos[i].getClaveEstatus());
				} else {
					throw new AppException("La Clave del Estatus es requerida para cada uno de los documentos");
				}

				//Si el documento se pasa a negociable (se rechaza la autorizacion del descuento) es 
				//obligatorio poner la clave de rechazo, si no est�, lanza exepcion.
				if (!"PantallaIF".equals(documentos[i].getTipoProceso()) )  {  
				
					if ( "2".equals(documentos[i].getClaveEstatus()) && (
							documentos[i].getClaveRechazo() == null || documentos[i].getClaveRechazo().length() == 0) ) {
						throw new AppException("La Clave de Rechazo es requerida si el estatus es 2 (Negociable) para cada uno de los documentos");
					}			 

					if (documentos[i].getClaveRechazo() != null && !documentos[i].getClaveRechazo().equals("")) {
						str.append("|"+documentos[i].getClaveRechazo());
					}
				}
			
				if (documentos[i].getAcusePyme() != null && !documentos[i].getAcusePyme().equals("")) {
					str.append("|"+documentos[i].getAcusePyme());
				}
			
				if (documentos[i].getClaveBancoFondeo() != null && !documentos[i].getClaveBancoFondeo().equals("")) {
					str.append("|"+documentos[i].getClaveBancoFondeo());
				}
				if (documentos[i].getClaveEpo() != null && !documentos[i].getClaveEpo().equals("")) {
					str.append("|"+documentos[i].getClaveEpo());
				}
				if (documentos[i].getClaveMoneda() != null && !documentos[i].getClaveMoneda().equals("")) {
					str.append("|"+documentos[i].getClaveMoneda());
				}
				if (documentos[i].getFechaEmision() != null && !documentos[i].getFechaEmision().equals("")) {
					str.append("|"+documentos[i].getFechaEmision());
				}
				if (documentos[i].getFechaSeleccionPyme() != null && !documentos[i].getFechaSeleccionPyme().equals("")) {
					str.append("|"+documentos[i].getFechaSeleccionPyme());
				}
				if (documentos[i].getFechaPublicacion() != null && !documentos[i].getFechaPublicacion().equals("")) {
					str.append("|"+documentos[i].getFechaPublicacion());
				}
				if (documentos[i].getFechaVencimiento() != null && !documentos[i].getFechaVencimiento().equals("")) {
					str.append("|"+documentos[i].getFechaVencimiento());
				}
				if (documentos[i].getMontoDescuento() != null && !documentos[i].getMontoDescuento().equals("")) {
					str.append("|"+documentos[i].getMontoDescuento());
				}
				if (documentos[i].getTasaAceptada() != null && !documentos[i].getTasaAceptada().equals("")) {
					str.append("|"+documentos[i].getTasaAceptada());
				}
				if (documentos[i].getImporteInteres() != null && !documentos[i].getImporteInteres().equals("")) {
					str.append("|"+documentos[i].getImporteInteres());
				}
				if (documentos[i].getImporteRecibir() != null && !documentos[i].getImporteRecibir().equals("")) {
					str.append("|"+documentos[i].getImporteRecibir());
				}
				if (documentos[i].getNombreEpo() != null && !documentos[i].getNombreEpo().equals("")) {
					str.append("|"+documentos[i].getNombreEpo());
				}
				if (documentos[i].getNombreMoneda() != null && !documentos[i].getNombreMoneda().equals("")) {
					str.append("|"+documentos[i].getNombreMoneda());
				}
				if (documentos[i].getNombrePyme() != null && !documentos[i].getNombrePyme().equals("")) {
					str.append("|"+documentos[i].getNombrePyme());
				}
				if (documentos[i].getNumeroDocumento() != null && !documentos[i].getNumeroDocumento().equals("")) {
					str.append("|"+documentos[i].getNumeroDocumento());
				}
				if (documentos[i].getNumeroProveedor() != null && !documentos[i].getNumeroProveedor().equals("")) {
					str.append("|"+documentos[i].getNumeroProveedor());
				}
				if (documentos[i].getNumeroSirac() != null && !documentos[i].getNumeroSirac().equals("")) {
					str.append("|"+documentos[i].getNumeroSirac());
				}
				if (documentos[i].getTipoFactoraje() != null && !documentos[i].getTipoFactoraje().equals("")) {
					str.append("|"+documentos[i].getTipoFactoraje());
				}
				str.append("\n");
			} //fin del for
		}//fin if hay datos.
	    log.info("AutorizacionDescuentoBean::getCadenaFirmarWSV2(S)");
		return str.toString();
	}





	/**
	 * Realiza el cambio de estatus de los documentos en proceso de autorizacion IF,
	 * segun corresponda. La carga puede ser parcial, lo que significa que si
	 * hay un error en un documento... esto no afecta a los dem�s documentos correctos.
	 *
	 * El proceso, regresa un objeto ProcesoIFWSInfo, el cual contiene informacion
	 * acerca de la ejecuci�n.
	 *
	 * Atrav�s de ProcesoIFWSInfo.getCodigoEjecucion() y ProcesoIFWSInfo.getDocumentos()
	 * se puede determinar cual fue el resultado de la ejecuci�n del proceso, de la
	 * siguiente manera:
	 *
	 * codigoEjecucion == 1 y documentos == null.
	 * 	Todos los documentos fueron procesados correctamente.
	 * codigoEjecucion == 1 y documentos != null.
	 * 	Hay uno o varios documentos que presentaron algun tipo de error.
	 *    El error puede ser normal de aplicacion (p.e. que el c�digo de rechazo no esta dado de alta en la aplicacion)
	 *    o puede ser error inesperado del sistema (Error en BD al realizar el update de cambio de estatus del documento )
	 * codigoEjecucion == 2 y documentos != null.
	 * 	Esta combinaci�n puede darse cuando existe un error que impide el procesamiento de los documentos.
	 *    El error puede ser normal de aplicacion (p.e. el uso de un certificado digital que no es del usuario)
	 *    o puede ser un error inesperado del sistema (p.e. una falla de conexion al sistema de autenticacion de usuarios)
	 *
	 *
	 * -------------------------------------------------------------------------------------------------------------------
	 * <br><br>
	 *
	 * codigoEjecucion == 2 y documentos == null.
	 * 	Esta combinaci�n, m�s bien, se podr�a presentar en el WebService
	 *    que invoca este m�todo, lo cual significaria que hubo problemas
	 *    para accesar este EJB.
	 *
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param documentos Arreglo de DocumentoIFWS que contiene la informaci�n
	 * 		de los documentos a procesar unicamente.
	 * @param pkcs7 Firma Digital de la cadena de documentos a procesar
	 * 		en formato PKCS7
	 * @param serial Numero de Serie del Certificado
	 * @return Objeto ProcesoIFWSInfo que contiene:
	 *
	 * 	codigoEjecucion Codigo de ejecuci�n resultante del proceso:
	 *    	1.- El proceso puedo finalizar completamente
	 *       2.- El proceso fue interrumpido debido a un error normal de aplicacion.
	 *       	por ejemplo, el password no es valido
	 *          � el proceso fue interrumpido debido a un funcionamiento anormal del sistema.
	 *          por ejemplo, un error de conexion a la Base de Datos.
	 *    resumenEjecucion Contiene una descripcion en texto del resultado de la ejecucion.
	 *    	Puede contener como texto, datos del acuse resultante de la ejecucici�n o
	 *       en caso de error. Si es normal de aplicacion comenzara con "ERROR|" y luego el detalle
	 *       del error. Si es un error inesperado de sistema comenzara con "ERROR INESPERADO|" y
	 *       luego el detalle del error.
	 *    documentos Contiene la lista de objetos DocumentoIFWS con los
	 *    	documentos que tuvieron error (en caso de que exista error) o que no
	 *    	pudieron ser procesados, ya sea debido a un error inesperado
	 *       (en cuyo caso la descripcion comienza con "ERROR INESPERADO|") o como o un
	 *    	error normal de aplicacion (en cuyo caso la descripcion comienza con "ERROR|")
	 */
	 
	 
	 public ProcesoIFWSInfo confirmacionDoctosIFWS (String claveUsuario,
			String password, 	String claveIF,
			DocumentoIFWS[] documentos, String pkcs7, String serial) {
			DocumentoIFWSV2[] docs = new DocumentoIFWSV2[documentos.length];
			for(int i=0;i<documentos.length;i++){
				docs[i]=new DocumentoIFWSV2();
				
						docs[i].setAcusePyme(documentos[i].getAcusePyme());
						docs[i].setClaveBancoFondeo(documentos[i].getClaveBancoFondeo());
						docs[i].setClaveDocumento(documentos[i].getClaveDocumento());
						docs[i].setClaveEpo(documentos[i].getClaveEpo());
						docs[i].setClaveMoneda(documentos[i].getClaveMoneda());
						docs[i].setClaveEstatus(documentos[i].getClaveEstatus());
						docs[i].setFechaEmision(documentos[i].getFechaEmision());
						docs[i].setFechaSeleccionPyme(documentos[i].getFechaSeleccionPyme());
						docs[i].setFechaPublicacion(documentos[i].getFechaPublicacion());
						docs[i].setFechaVencimiento(documentos[i].getFechaVencimiento());
						docs[i].setMontoDescuento(documentos[i].getMontoDescuento());
						docs[i].setTasaAceptada(documentos[i].getTasaAceptada());
						docs[i].setImporteInteres(documentos[i].getImporteInteres());
						docs[i].setImporteRecibir(documentos[i].getImporteRecibir());
						docs[i].setNombreEpo(documentos[i].getNombreEpo());
						docs[i].setNombreMoneda(documentos[i].getNombreMoneda());
						docs[i].setNombrePyme(documentos[i].getNombrePyme());
						docs[i].setNumeroDocumento(documentos[i].getNumeroDocumento());
						docs[i].setNumeroProveedor(documentos[i].getNumeroProveedor());
						docs[i].setNumeroSirac(documentos[i].getNumeroSirac());
						docs[i].setTipoFactoraje(documentos[i].getTipoFactoraje());
						docs[i].setClaveRechazo(documentos[i].getClaveRechazo());
						docs[i].setErrorDetalle(documentos[i].getErrorDetalle());
						docs[i].setCampoAdicional1(documentos[i].getCampoAdicional1());
						docs[i].setCampoAdicional2(documentos[i].getCampoAdicional2());
						docs[i].setCampoAdicional3(documentos[i].getCampoAdicional3());
						docs[i].setCampoAdicional4(documentos[i].getCampoAdicional4());
						docs[i].setCampoAdicional5(documentos[i].getCampoAdicional5());
						docs[i].setMontoDocumento(documentos[i].getMontoDocumento());
						docs[i].setPorcentajeDescuento(documentos[i].getPorcentajeDescuento());
						docs[i].setRecursoGarantia(documentos[i].getRecursoGarantia());
						docs[i].setPlazo(documentos[i].getPlazo());
						docs[i].setNombreEstatus(documentos[i].getNombreEstatus());
						docs[i].setReferencia(documentos[i].getReferencia());
						docs[i].setClaveIF(documentos[i].getClaveIF());
						docs[i].setNombreIF(documentos[i].getNombreIF());
						//FVR
						docs[i].setClavePYME(documentos[i].getClavePYME());
						docs[i].setFechaNotificacion(documentos[i].getFechaNotificacion());
						docs[i].setPorcentajeAnticipo(documentos[i].getPorcentajeAnticipo());
						docs[i].setNombreTipoFactoraje(documentos[i].getNombreTipoFactoraje());
						docs[i].setAcuseIF(documentos[i].getAcuseIF());

			}
			ProcesoIFWSInfoV2 procV2=confirmacionDoctosIFWSV2( claveUsuario,
			 password,  claveIF,docs,pkcs7,serial,false);
			 ProcesoIFWSInfo proceso=new ProcesoIFWSInfo();
			 proceso.setCodigoEjecucion(procV2.getCodigoEjecucion());
			 proceso.setResumenEjecucion(procV2.getResumenEjecucion());
			 
			 DocumentoIFWSV2[] arrDocumentosV2 = procV2.getDocumentos();
			 if(arrDocumentosV2!=null){
				 DocumentoIFWS[] arrDocumentos=new DocumentoIFWS[arrDocumentosV2.length];
				 for(int i=0;i<arrDocumentosV2.length;i++){
					arrDocumentos[i]=new DocumentoIFWS();
					arrDocumentos[i].setAcusePyme(arrDocumentosV2[i].getAcusePyme());
						arrDocumentos[i].setClaveBancoFondeo(arrDocumentosV2[i].getClaveBancoFondeo());
						arrDocumentos[i].setClaveDocumento(arrDocumentosV2[i].getClaveDocumento());
						arrDocumentos[i].setClaveEpo(arrDocumentosV2[i].getClaveEpo());
						arrDocumentos[i].setClaveMoneda(arrDocumentosV2[i].getClaveMoneda());
						arrDocumentos[i].setClaveEstatus(arrDocumentosV2[i].getClaveEstatus());
						arrDocumentos[i].setFechaEmision(arrDocumentosV2[i].getFechaEmision());
						arrDocumentos[i].setFechaSeleccionPyme(arrDocumentosV2[i].getFechaSeleccionPyme());
						arrDocumentos[i].setFechaPublicacion(arrDocumentosV2[i].getFechaPublicacion());
						arrDocumentos[i].setFechaVencimiento(arrDocumentosV2[i].getFechaVencimiento());
						arrDocumentos[i].setMontoDescuento(arrDocumentosV2[i].getMontoDescuento());
						arrDocumentos[i].setTasaAceptada(arrDocumentosV2[i].getTasaAceptada());
						arrDocumentos[i].setImporteInteres(arrDocumentosV2[i].getImporteInteres());
						arrDocumentos[i].setImporteRecibir(arrDocumentosV2[i].getImporteRecibir());
						arrDocumentos[i].setNombreEpo(arrDocumentosV2[i].getNombreEpo());
						arrDocumentos[i].setNombreMoneda(arrDocumentosV2[i].getNombreMoneda());
						arrDocumentos[i].setNombrePyme(arrDocumentosV2[i].getNombrePyme());
						arrDocumentos[i].setNumeroDocumento(arrDocumentosV2[i].getNumeroDocumento());
						arrDocumentos[i].setNumeroProveedor(arrDocumentosV2[i].getNumeroProveedor());
						arrDocumentos[i].setNumeroSirac(arrDocumentosV2[i].getNumeroSirac());
						arrDocumentos[i].setTipoFactoraje(arrDocumentosV2[i].getTipoFactoraje());
						arrDocumentos[i].setClaveRechazo(arrDocumentosV2[i].getClaveRechazo());
						arrDocumentos[i].setErrorDetalle(arrDocumentosV2[i].getErrorDetalle());
						arrDocumentos[i].setCampoAdicional1(arrDocumentosV2[i].getCampoAdicional1());
						arrDocumentos[i].setCampoAdicional2(arrDocumentosV2[i].getCampoAdicional2());
						arrDocumentos[i].setCampoAdicional3(arrDocumentosV2[i].getCampoAdicional3());
						arrDocumentos[i].setCampoAdicional4(arrDocumentosV2[i].getCampoAdicional4());
						arrDocumentos[i].setCampoAdicional5(arrDocumentosV2[i].getCampoAdicional5());
						arrDocumentos[i].setMontoDocumento(arrDocumentosV2[i].getMontoDocumento());
						arrDocumentos[i].setPorcentajeDescuento(arrDocumentosV2[i].getPorcentajeDescuento());
						arrDocumentos[i].setRecursoGarantia(arrDocumentosV2[i].getRecursoGarantia());
						arrDocumentos[i].setPlazo(arrDocumentosV2[i].getPlazo());
						arrDocumentos[i].setNombreEstatus(arrDocumentosV2[i].getNombreEstatus());
						arrDocumentos[i].setReferencia(arrDocumentosV2[i].getReferencia());
						arrDocumentos[i].setClaveIF(arrDocumentosV2[i].getClaveIF());
						arrDocumentos[i].setNombreIF(arrDocumentosV2[i].getNombreIF());
						//FVR
						arrDocumentos[i].setClavePYME(arrDocumentosV2[i].getClavePYME());
						arrDocumentos[i].setFechaNotificacion(arrDocumentosV2[i].getFechaNotificacion());
						arrDocumentos[i].setPorcentajeAnticipo(arrDocumentosV2[i].getPorcentajeAnticipo());
						arrDocumentos[i].setNombreTipoFactoraje(arrDocumentosV2[i].getNombreTipoFactoraje());
						arrDocumentos[i].setAcuseIF(arrDocumentosV2[i].getAcuseIF());
				 }
				 proceso.setDocumentos(arrDocumentos);
			 }
			 return proceso;

			
			}
			
			public ProcesoIFWSInfoV2 confirmacionDoctosIFWSV2 (String claveUsuario,
			String password, 	String claveIF,
			DocumentoIFWSV2[] documentos, String pkcs7, String serial,String detalle_doctos_error) {
					return this.confirmacionDoctosIFWSV2(claveUsuario,password,claveIF,documentos,pkcs7,serial,(detalle_doctos_error.equals("S")?true:false));
			}

    private String convertStringToHex(String str) {
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int)chars[i]));
        }
        return hex.toString();
    }
    
	/**
	 * Versi�n 2 del WS de confirmaci�n de Documentos IF.
	 * @param claveUsuario Clave del usuario
	 * @param password contrase�a 
	 * @param claveIF Clave del IF (ic_if)
	 * @param documentos arreglo de documentos a confirmar
	 * @param pkcs7 Cadena firmada en formato PKCS7
	 * @param serial Serial del certificado
	 * @param banderaV2 bandera que identifica ???
	 */
	public ProcesoIFWSInfoV2 confirmacionDoctosIFWSV2 (String claveUsuario,
			String password, 	String claveIF,
			DocumentoIFWSV2[] documentos, String pkcs7, String serial,boolean banderaV2) {
		log.info("AutorizacionDescuentoBean::confirmacionDoctosIFWSV2(E)("+claveIF+"_"+claveUsuario+")");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		List lDocumentosError = new ArrayList();
		List lDocumentosV2 =new ArrayList();
		int i = 0; //numero de registros procesados

		int iClaveIF = 0;
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

		//**********************************Validaci�n de parametros:*****************************
		String tipoProceso ="";
		for (i=0; i < documentos.length; i++) {
			DocumentoIFWSV2 doc = documentos[i];
			tipoProceso = doc.getTipoProceso();
		}
		
		StringBuffer 	no_documentos  = new StringBuffer();  
		
		log.debug("tipoProceso ==================> "+tipoProceso);
		
		
		try {	
			if(!tipoProceso.equals("PantallaIF")  ) { 
				if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")  ||
					documentos == null ||
					pkcs7 == null || pkcs7.equals("") ||
					serial == null || serial.equals("") ) {
					throw new Exception("Los parametros son requeridos");
				}
			}if(tipoProceso.equals("PantallaIF")  ) { 
				if (claveUsuario == null || claveUsuario.equals("") ||					
					claveIF == null || claveIF.equals("")  ||
					documentos == null ||
					pkcs7 == null || pkcs7.equals("") ||
					serial == null || serial.equals("") ) {
					throw new Exception("Los parametros son requeridos");
				}
			}
			
			iClaveIF = Integer.parseInt(claveIF);
	
		} catch(Exception e) {
			log.error("confirmacionDoctosIFWSV2(Error) ", e);
			// Como no entr� al ciclo, se regresan todos los documentos recibidos (si existen)
			if (documentos != null) {
				for (i=0; i < documentos.length; i++) {
					DocumentoIFWSV2 doc = documentos[i];
					doc.setErrorDetalle("ERROR|Registro no procesado");
					doc.setTipoError(1);
				}
			}
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
				"(" + claveUsuario + "," + password + "," + claveIF +
					"," + documentos + "," + pkcs7 + "," + serial + ")" + ". Error=" + e.getMessage());
			resultadoProceso.setDocumentos(documentos);
			return resultadoProceso;
		}		
		
		//****************************************************************************************


		if (documentos == null) {
			//No hay documentos por procesar. Termina
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //Exito
			resultadoProceso.setResumenEjecucion("No hay documentos por procesar");
			return resultadoProceso;

		}

		AccesoDB con = null;
		boolean exito = true;
		//boolean error = false;
		String acuse = "";

		PreparedStatement psParamNotasCred = null;
		PreparedStatement psRegistroSolicitud = null;
		PreparedStatement psRechazo = null;
		PreparedStatement psDoc = null;
		PreparedStatement psSolicitud = null;
		PreparedStatement psUpdate = null;
		PreparedStatement psAmortizacion = null;


		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}
			
			
			//******************Inicia Validaci�n de Password *************************
		
				//Validacion del usuario y Password y que sea del IF especificado
				UtilUsr utilUsr = new UtilUsr();
				List cuentasIF = null;
				try {
					cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
				} catch(Throwable t) {
					throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
				}                    
				log.debug("confirmacionDoctosIFWSV2(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
				log.trace("confirmacionDoctosIFWSV2::cuentasIF:"+cuentasIF);
                    
				if (!cuentasIF.contains(claveUsuario)) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					for (i=0; i < documentos.length; i++) {
						DocumentoIFWSV2 doc = documentos[i];
						doc.setErrorDetalle("ERROR|Registro no procesado");
						doc.setTipoError(1);
					}
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");
					resultadoProceso.setDocumentos(documentos);
					return resultadoProceso; //Termina Proceso
				}
	
			if(!tipoProceso.equals("PantallaIF")  ) { 
				log.debug("confirmacionDoctosIFWSV2::Validando usuario/passwd");
				try {
					if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
						//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
						for (i=0; i < documentos.length; i++) {
							DocumentoIFWSV2 doc = documentos[i];
							doc.setErrorDetalle("ERROR|Registro no procesado");
							doc.setTipoError(1);
						}
						resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
						resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
						resultadoProceso.setDocumentos(documentos);
						return resultadoProceso; //Termina Proceso
					}
				} catch(Throwable t) {
					throw new Exception("Error en el componente de validacion de usuario. ", t);
				}
				
			}
			//***************************Termina autenticacion del usuario**********

			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			PreparedStatement ps = null;
			String qrySentencia = null;
			try {
				//Creo que este alter es para que funcione adecuadamente la funcion sigfechahabilxepo
				qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
				con.terminaTransaccion(true);
			} catch(Throwable t) {
				throw new Exception("Error al establecer la informacion de idioma en BD");
			}


/*			String queryComision =
					" SELECT fg_porc_comision_fondeo "+
					" FROM comcat_producto_nafin "+
					" WHERE ic_producto_nafin = ? ";
			PreparedStatement psComision = con.queryPrecompilado(queryComision);
			psComision.setInt(1,1);	//1= Producto de Descuento Electronico
			ResultSet rsComision = psComision.executeQuery();
			String porcentajeComisionFondeo = "";
			if (rsComision.next()) {
				porcentajeComisionFondeo = rsComision.getString("fg_porc_comision_fondeo");
			}
			rsComision.close();
			psComision.close();
*/

			//----------------------- Definicion de queries que se ocupan dentro del ciclo ---------------

			try {
				String strSQL =
						" SELECT TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto "+
						" 	, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc "+
						" 	, TO_CHAR(d.df_fecha_venc,'dd') as  diaVencimiento "+
						" 	, ds.in_importe_recibir " +
						" 	, t.ic_tasa "+
						" 	, pd.ic_clase_docto "+
						" 	, i.ig_tipo_piso "+
						" 	, TO_CHAR(sigfechahabilxepo(d.ic_epo, TRUNC(SYSDATE), 1, '+'),'dd/mm/yyyy') as sigFechaHabilXEpo "+
						" 	, (d.df_fecha_venc - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, sigfechahabilxepo(d.ic_epo, TRUNC(SYSDATE), 1, '+')) ) as plazo " +
						" 	, e.ic_banco_fondeo "+
						" 	, d.ic_estatus_docto " +
						" 	, e.ic_epo " +
						" 	, e.cg_razon_social as nombreEpo " +
						" 	, d.ic_moneda as claveMoneda " +
						" FROM com_documento d "+
						" 	, comcat_tasa t "+
						" 	, comrel_producto_docto pd "+
						" 	, com_docto_seleccionado ds "+
						" 	, comrel_tasa_autorizada ta "+
						" 	, comrel_tasa_base tb "+
						" 	, comrel_if_epo ie "+
						" 	, comcat_if i "+
						" 	, comcat_epo e " +
						" WHERE "+
						" 	ds.dc_fecha_tasa = ta.dc_fecha_tasa "+
						" 	AND ds.ic_epo = ta.ic_epo "+
						" 	AND ds.ic_if = ta.ic_if "+
						" 	AND ta.dc_fecha_tasa = tb.dc_fecha_tasa "+
						" 	AND tb.ic_tasa = t.ic_tasa "+
						" 	AND t.ic_moneda = d.ic_moneda "+
						" 	AND d.ic_epo=pd.ic_epo "+
						" 	AND pd.ic_producto_nafin=1 "+
						" 	AND d.ic_documento = ds.ic_documento"+
						" 	AND ds.ic_if = ie.ic_if " +
						" 	AND ds.ic_epo = ie.ic_epo " +
						" 	AND ie.ic_if = i.ic_if " +
						" 	AND ie.ic_epo = e.ic_epo " +
						" 	AND d.ic_documento = ? " +
						" 	AND d.ic_if = ? ";

				psDoc = con.queryPrecompilado(strSQL);
			} catch (Throwable t) {
				throw new Exception("Error al precompilar la consulta de documentos.",t);
			}

			try {
				String querySolicitud =
						" SELECT COUNT(*) as numRegistros " +
						" FROM com_solicitud "+
						" WHERE ic_documento = ? ";
				psSolicitud = con.queryPrecompilado(querySolicitud);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la consulta de cantidad de solicitudes.",t);
			}

			try {
				String strSQLRechazo =
						" SELECT cg_descripcion AS causaRechazo " +
						" FROM com_error_if     " +
						" WHERE cc_error_if = ? " +
						"   AND ic_if       = ? ";

				psRechazo = con.queryPrecompilado(strSQLRechazo);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la consulta de errores de IF.",t);
			}

			try{
				String queryUpdate =
						" UPDATE com_documento " +
						" SET ic_estatus_docto = ? " +
						" WHERE ic_documento = ? ";
				psUpdate = con.queryPrecompilado(queryUpdate);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la actualizacion del cambio de estatus del documento.",t);
			}

			try {
				String queryRegistroSolicitud =
						" INSERT INTO com_solicitud (" +
						" ic_folio, ic_documento, ic_esquema_amort, ic_tipo_credito," +
						" ic_oficina, cc_acuse, " +
						" ig_plazo, " +
						" cg_tipo_plazo, cg_perio_pago_cap," +
						" cg_perio_pago_int, cg_desc_bienes, ic_clase_docto," +
						" cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif," +
						" ic_tabla_amort, in_numero_amort, df_ppc, df_ppi," +
						" df_v_documento, df_v_descuento, in_dia_pago," +
						" df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic," +
						" df_fecha_solicitud, ic_bloqueo, cs_tipo_solicitud, " +
						" cg_emisor,ic_banco_fondeo) " +
						" VALUES (?,?, 1, 1," +
						" 90, ?, " +
						" TO_DATE(?,'dd/mm/yyyy') - DECODE(?, 1, TRUNC(SYSDATE), 54, TO_DATE(?, 'dd/mm/yyyy')), "+
						" 'D', 'Al Vto.', " +
						" 'Al Vto.', ' ', ?, " +
						" 'Mexico, D.F.', ?, '+', 0, " +
						" 1, 1, TO_DATE(?,'dd/mm/yyyy'), TO_DATE(?,'dd/mm/yyyy'), " +
						" TO_DATE(?,'dd/mm/yyyy'), TO_DATE(?,'dd/mm/yyyy'), ?, " +
						" TO_DATE(?,'dd/mm/yyyy'), 'Mexico, D.F.', 0, ?, " +
						" sysdate, ?, 'C', " +
						" ?,?)";

				psRegistroSolicitud = con.queryPrecompilado(queryRegistroSolicitud);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la insercion de la solicitud.",t);
			}

			try {
				String qryParamNotasCred =
						" SELECT cs_opera_notas_cred "   +
						" FROM comrel_producto_epo "   +
						" WHERE ic_producto_nafin = ? "   +
						" AND ic_epo = ? "  ;

				psParamNotasCred = con.queryPrecompilado(qryParamNotasCred);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la consulta de parametro de operacion de notas de credito.",t);
			}

			try {
				String qryAmortizacion =
						" INSERT INTO com_amortizacion (ic_amortizacion, ic_folio, in_numero" +
						" , fn_importe, df_fecha)" +
						" VALUES (seq_com_amortizacion_ic_amor.NEXTVAL, ? " +
						" , 1, ? , TO_DATE(?,'dd/mm/yyyy'))";

				psAmortizacion = con.queryPrecompilado(qryAmortizacion);
			} catch(Throwable t) {
				throw new Exception("Error al precompilar la insercion de las amortizaciones.",t);
			}


			//----------------------- ----------------------------------------------- ---------------


			//-------------------------- VALIDACION De FIRMA PKCS7 ----------------------------

			log.info("confirmacionDoctosIFWSV2()::Validando que el certificado sea del usuario especificado");
			PreparedStatement psSerial = null;
			String strSerial = "";

			try {
				String qryTraeSerial =
						" SELECT dn_user " +
						" FROM users_seguridata "+
						" WHERE UPPER(trim(table_uid)) = UPPER(?) ";
				psSerial = con.queryPrecompilado(qryTraeSerial);
				psSerial.setString(1, claveUsuario);
				ResultSet rsSerial = psSerial.executeQuery();
				
				log.debug("qryTraeSerial ==  "+qryTraeSerial+"\n  claveUsuario==  "+claveUsuario);

				if(rsSerial.next()) {
					strSerial = rsSerial.getString(1).trim();
				}
				rsSerial.close();
				psSerial.close();
			} catch(Throwable t) {
				throw new Exception("Error al intentar verificar el numero de serie del certificado.",t);
			} finally {
				con.terminaTransaccion(true); //Se realiza un commit dado que se accesa una tabla remota
			}
            
		    log.trace("serial:"+serial.length());
		    if(serial.length()==20) {
		        serial = convertStringToHex(serial);
		    }
		    log.trace("serial:"+serial.length());
		    log.trace("serial:"+serial);
		    log.trace("strSerial:"+strSerial);
            
			if (!serial.equals(strSerial)) {
				//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
				for (i=0; i < documentos.length; i++) {
					DocumentoIFWSV2 doc = documentos[i];
					doc.setErrorDetalle("ERROR|Registro no procesado");
					doc.setTipoError(1);
				}
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|El certificado con numero de serie " + serial + " no corresponde al usuario " + claveUsuario);
				resultadoProceso.setDocumentos(documentos);
				return resultadoProceso;
			}
			String cadenaOriginalFirmada = "";

			try {
				cadenaOriginalFirmada = this.getCadenaFirmarWSV2(documentos);
			} catch(AppException e) {
				//Si es AppException significa que es un error normal de aplicaci�n al intentar obetener la cadena Original
				for (i=0; i < documentos.length; i++) {
					DocumentoIFWSV2 doc = documentos[i];
					doc.setErrorDetalle("ERROR|Registro no procesado");
					doc.setTipoError(1);
				}				
				
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|Error al intentar obtener la cadena original antes de la firma." + e.getMessage() );
				resultadoProceso.setDocumentos(documentos);
				return resultadoProceso;
			}catch(Throwable t) {
				throw new Exception("Error al intentar obtener la cadena original antes de la firma.",t);
			}
			Seguridad s = null;

			try {
				s = new Seguridad();
			} catch(Throwable t) {
				throw new Exception("Error al inicializar el componente de Seguridad.",t);
			}
			char getReceipt = 'Y';
			String receipt = "";

			//Se realiza la creacion de un acuse vacio y se inserta definitivamente (GeneraAcuse da commit)
			try {
				if(!tipoProceso.equals("PantallaIF")  ) { 
					acuse = this.GeneraAcuse(claveIF, "ws_confirmacion_" + claveUsuario, con);
				}else {
					acuse = this.GeneraAcuse(claveIF, "confi_Doctos_"+claveUsuario, con);
				}
				//la autenticacion del mensaje requiere este numero de acuse
			} catch(Throwable t) {
				throw new Exception("Error al generar el acuse de la operacion.",t);
			}

			log.info("confirmacionDoctosIFWS()::Autenticando Firma Digital");
						
			boolean autenticarMensaje = false; 
			try {
				autenticarMensaje =
						s.autenticar(acuse, serial, pkcs7,
								cadenaOriginalFirmada, getReceipt);
			
			log.info("autenticarMensaje   "+autenticarMensaje);
			
				if (autenticarMensaje) {
					receipt = s.getAcuse();		
					resultadoProceso.setRecibo(receipt);	
					resultadoProceso.setAcuse(acuse);  
				} else {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					for (i=0; i < documentos.length; i++) {
						DocumentoIFWSV2 doc = documentos[i];
						doc.setErrorDetalle("ERROR|Registro no procesado");
						doc.setTipoError(1);
					}
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del mensaje");
					resultadoProceso.setDocumentos(documentos);
					BorraAcuse(acuse,con); //Borra el Acuse y realiza commit
					return resultadoProceso; //Termina Proceso
				}
			} catch(Throwable t) {
				throw new Exception("Error en el proceso de autenticacion del mensaje.",t);
			}

			// Una vez que se entra al ciclo, aun los errores inesperados no interrumpen
			// la ejecuci�n del procesos...
			int numOperados = 0;				
			int numNegociables = 0;	
			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv Inicio de Ciclo vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			log.info("----documentos.length -----  "+documentos.length );
			for (i=0; i < documentos.length; i++) {
			
				DocumentoIFWSV2 doc = null;
				StringBuffer detalleError = null;
				String folio="";
				try { //Como se permiten cargas parciales, se debe cachar cualquier error y continuar con el siguiente registro
					doc = documentos[i];
					log.debug("confirmacionDoctosIFWSV2():: Documento Recibido[" + i + "]=" + doc);
					detalleError = new StringBuffer();

					psParamNotasCred.clearParameters();
					psRegistroSolicitud.clearParameters();
					psUpdate.clearParameters();
					psRechazo.clearParameters();
					psSolicitud.clearParameters();
					psDoc.clearParameters();
					psAmortizacion.clearParameters();

					//************************Validaci�n de atributos de documentos:**************************
					long lClaveDocumento = 0;
					try {
						lClaveDocumento = Long.parseLong(doc.getClaveDocumento());
					} catch(NumberFormatException e) {
						detalleError.append("El valor de la clave del documento no es un numero valido\n");
					}


					int iClaveEstatusDoctoOriginal = 0;		//En esta variable se guarda el estatus que tiene actualmente el documento en BD.
					String fechaDocumento = "";
					String fechaVencimiento = "";
					String diaVencimiento = "";
					String importeRecibir = "";
					int claveTasa = 0;
					int  claveClaseDocumento = 0;
					int iTipoPiso = 0;
					int iDiaPlazo = 0;
					int claveBancoFondeo = 0;
					String claveEpo = "";
					String nombreEpo = "";
					int claveMoneda = 0;
					String fechaSigDol = "";
					boolean hayFondeoPropio = false;
					
					//Valida la existencia de la clave del documento (ic_documento)
					ResultSet rs = null;
					try {
						
						psDoc.setLong(1, lClaveDocumento);
						psDoc.setInt(2, iClaveIF);
						rs = psDoc.executeQuery();
						if (rs.next()) {
							iClaveEstatusDoctoOriginal = rs.getInt("ic_estatus_docto");
							fechaDocumento = rs.getString("df_fecha_docto");
							fechaVencimiento = rs.getString("df_fecha_venc");
							diaVencimiento = rs.getString("diaVencimiento");
							importeRecibir = rs.getString("in_importe_recibir");
							claveTasa = rs.getInt("ic_tasa");
							claveClaseDocumento = rs.getInt("ic_clase_docto");
							iTipoPiso = rs.getInt("ig_tipo_piso");
							iDiaPlazo = rs.getInt("plazo");
							claveBancoFondeo = rs.getInt("ic_banco_fondeo");
							claveEpo = rs.getString("ic_epo");
							nombreEpo = rs.getString("nombreEpo");
							claveMoneda = rs.getInt("claveMoneda");
						try {
							hayFondeoPropio = hayFondeoPropio(claveIF,claveEpo);
						} catch(Throwable t) {
							throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
						}
							fechaSigDol = (rs.getString("sigFechaHabilXEpo")==null || hayFondeoPropio)?new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()):rs.getString("sigFechaHabilXEpo");

						} else {
							detalleError.append("El valor de la clave del documento no existe o no pertenece al intermediario\n");
						}
						rs.close();
					} catch(Throwable t) {
						throw new Exception("Error al obtener los datos del documento.",t);
					}

					//Los documentos que pueden ser cambiados de estatus en este proceso son los que actualmente tengan 24 (En proceso de autorizacion IF)
					if(!doc.getTipoProceso().equals("PantallaIF"))  {					
						if (iClaveEstatusDoctoOriginal != 24) {
							detalleError.append("El documento especificado no se encuentra en estatus " +
									" en proceso de Autorizacion IF. Estatus Actual=" + iClaveEstatusDoctoOriginal );
						}
					}else {
						if (iClaveEstatusDoctoOriginal != 3) {
							detalleError.append("El documento especificado no se encuentra en estatus  en proceso de Seleccionada Pyme Estatus Actual=" + iClaveEstatusDoctoOriginal );
						}
					} 

					int iClaveEstatusAsignar = 0;
					try {
						iClaveEstatusAsignar = Integer.parseInt(doc.getClaveEstatus());
					} catch(NumberFormatException e) {
						detalleError.append("El valor de la clave del estatus no es un numero valido");
					}
					//Bancomer va a mandar documentos de 24 a 24 :-) porque as� est� su aplicacion
					//Se van a ignorar...
					if (iClaveEstatusDoctoOriginal == 24 && iClaveEstatusAsignar != 2 && iClaveEstatusAsignar != 4 && iClaveEstatusAsignar != 24) {
						detalleError.append("El valor de la Clave de Estatus a asignar no es valido. " +
								"Valores validos: 2 (Rechazado), 4 (Operado), 24 (En proceso de autorizacion IF) ");
					}

					tipoProceso = doc.getTipoProceso();
					String claveRechazo = doc.getClaveRechazo();
					String causaRechazo = "";
					
					
					if (!tipoProceso.equals("PantallaIF")  ) {
					
						if (iClaveEstatusAsignar == 2 && (claveRechazo == null || claveRechazo.trim().equals("")) ) {
							//Estatus 2 en realidad es Negociable, pero en este proceso significa que el IF lo rechazo
							// y por lo tanto se requiere la clave de rechazo
							detalleError.append("El valor de la Clave de Rechazo es obligatorio si el estatus a asignar es 2 (Rechazado). ");
						} else if (iClaveEstatusAsignar == 2 && claveRechazo.length() > 8 ) {
							detalleError.append("El valor de la Clave de Rechazo no es valido. ");
						} else if (iClaveEstatusAsignar == 2 && claveRechazo.length() <= 8 ) {
	
							try {
	
								psRechazo.setString(1, claveRechazo);
								psRechazo.setInt(2, Integer.parseInt(claveIF));
								rs = psRechazo.executeQuery();
	
								if (rs.next()) {
									causaRechazo = rs.getString("causaRechazo");
								} else {
									detalleError.append("El valor de la Clave de Rechazo no es una clave existente. ");
								}
								rs.close();
	
							} catch(NumberFormatException e) {
								detalleError.append("El formato de la Clave de Rechazo no es valido\n");
							} catch(Throwable t) {
								throw new Exception("Error al consultar la clave de rechazo.",t);
							}
	
						}
					
					}else if (tipoProceso.equals("PantallaIF") ) {
						causaRechazo = doc.getCausaRechazo();						
					}
										
					if (detalleError.length()>0) { //Si hay Errores
						doc.setErrorDetalle("ERROR|" + detalleError.toString());
						doc.setTipoError(1);
						lDocumentosError.add(doc);
						continue;		//Continua con el siguiente Documento
					} else {
						//error = false;
						doc.setErrorDetalle("");
						doc.setTipoError(0);
					}

					//if (!error) {	//Si no hay error de validaci�n de los valores de atributos
					//codigo para procesar el registro
					if (iClaveEstatusAsignar == 2) {	//Se regresa a negociable (Rechazo por parte del IF)
						try {
							String query =
									" INSERT INTO comhis_cambio_estatus ( " +
									" dc_fecha_cambio, ic_documento, " +
									" ic_cambio_estatus, ct_cambio_motivo, cg_nombre_usuario, cc_acuse3 ) " + 
									" VALUES (sysdate,?,?,?,?,? )";

							PreparedStatement psCambioEstatus = con.queryPrecompilado(query);
							psCambioEstatus.setLong(1,lClaveDocumento);

							int cambioEstatus = 0;
							if (iClaveEstatusDoctoOriginal == 3) { //Si pasa de Seleccionado Pyme a Negociable
								cambioEstatus = 2;
							} else if (iClaveEstatusDoctoOriginal == 24) {	//Si pasa de En proceso de Autorizacion IF a Negociable
								cambioEstatus = 23;
							}
							 
							//Registra el cambio de estatus del Documento
							psCambioEstatus.setInt(2, cambioEstatus );
							psCambioEstatus.setString(3, causaRechazo );
							psCambioEstatus.setString(4, claveUsuario );
							psCambioEstatus.setString(5, acuse );							
							psCambioEstatus.executeUpdate();
							psCambioEstatus.close();

						} catch(Throwable t) {
							throw new Exception("Error al registrar el cambio de estatus del documento en bitacora.",t);
						}
						try {
							// Modifica el estatus del documento
							psUpdate.setInt(1, 2);	//Negociable
							psUpdate.setLong(2, lClaveDocumento);
							psUpdate.executeUpdate();
						} catch(Throwable t) {
							throw new Exception("Error al realizar el cambio de estatus del documento.",t);
						}
						try {
							//Dado que se permiten cargas parciales, se realiza el commit de
							//la transaccion x registro
							con.terminaTransaccion(true);
						} catch(Throwable t) {
							throw new Exception("Error al realizar el commit de los cambios del documento.",t);
						}

						numNegociables++;
						
					} else if (iClaveEstatusAsignar == 24) {  //En proceso de Autorizacion IF
						//Se ignora debido a que los documentos ya estan en estatus 24...
						/*try {
							// Modifica el estatus del documento
							psUpdate.setInt(1, 24); //En proceso de Autorizacion IF
							psUpdate.setLong(2,lClaveDocumento);
							psUpdate.executeUpdate();
						} catch(Throwable t) {
							throw new Exception("Error al realizar el cambio de estatus del documento.",t);
						}
						try {
							//Dado que se permiten cargas parciales, se realiza el commit de
							//la transaccion x registro
							con.terminaTransaccion(true);
						} catch(Throwable t) {
							throw new Exception("Error al realizar el commit de los cambios del documento.",t);
						}*/
					} else if (iClaveEstatusAsignar == 4) { //Operada
						int estatusSolic = 0;
						try {
							if (!hayClaseDoctoAsociado(claveEpo, con)) {	//No existe una clase de documento asociado para realizar Descuento?
								doc.setErrorDetalle("ERROR|No existe una clase de documento asociado para realizar Descuento");
								doc.setTipoError(1);
								lDocumentosError.add(doc);
								continue; //Continua con el siguiente documento
							}
						} catch(Throwable t) {
							throw new Exception("Error al verificar la clase de documento.",t);
						}

						int iPlazoMaxBO = 0;
						Map mPlazoBO = null;
						try {
							//Determina si existe base de operaci�n para la epo-if-moneda
							mPlazoBO = this.getPlazoBO(claveEpo, claveIF, new int[] {claveMoneda});

							List lPlazos = Comunes.explode(":", (String)mPlazoBO.get("sPlazoBO"));
							log.debug("confirmacionDoctosIFWSV2()::lPlazos=" + lPlazos);
							if (lPlazos != null && lPlazos.size() != 0) {
								//Solo debe tener un elemento los plazos, ya que solo se pasa una moneda a getPlazoBO
								List elementos = Comunes.explode( "|", (String)lPlazos.get(0) );
								log.debug("confirmacionDoctosIFWSV2()::elementos=" + elementos);
								if(elementos!=null && elementos.size() !=0) {
									iPlazoMaxBO = Integer.parseInt((String)elementos.get(1));//Plazo
								}
								log.debug("confirmacionDoctosIFWSV2()::iPlazoMaxBO=" + iPlazoMaxBO);
//											mPlazoBO.get("sMsgError");
//										mPlazoBO.get("sPlazoBO");
							}

							if (iPlazoMaxBO == 0) { //No se encontro Base de operacion
								doc.setErrorDetalle("ERROR|" + (String)mPlazoBO.get("sMsgError"));
								lDocumentosError.add(doc);
								doc.setTipoError(1);
								continue; //Continua con el siguiente documento
							}
						} catch(Throwable t) {
							throw new Exception("Error al determinar la base de operacion epo-if-moneda.",t);
						}

						boolean bOperaNotasCredito = false;
						Vector vDiasInhabiles = null;
						SimpleDateFormat sdfv = null;
						GregorianCalendar cFechaVencDesc = new GregorianCalendar();	// Por defecto siempre toma la fecha del d�a actual.
						String sFechaVencDesc = "";
						String bloqueo = "NULL";
						//boolean hayFondeoPropio = false;

						//Obtiene la parametrizacon de Notas de Cr�dito
						ResultSet rsParamNC = null;
						try {
							psParamNotasCred.setInt(1,1); //1=Descuento Electronico
							psParamNotasCred.setInt(2,Integer.parseInt(claveEpo));

							rsParamNC = psParamNotasCred.executeQuery();
							if (rsParamNC.next()) {
								String operaNotasCredito = rsParamNC.getString("cs_opera_notas_cred");
								bOperaNotasCredito = (operaNotasCredito !=null && operaNotasCredito.equals("S"))?true:false;
							}
						} catch(Throwable t) {
							throw new Exception("Error al obtener la parametrizacion de notas de credito.",t);
						} finally {
							rsParamNC.close();
						}
						try {
							vDiasInhabiles = getDiasInhabiles(con);
						} catch(Throwable t) {
							throw new Exception("Error al obtener dias inhabiles.",t);
						}
						sdfv = new SimpleDateFormat("dd/MM/yyyy");
						try {
							hayFondeoPropio = hayFondeoPropio(claveIF,claveEpo);
						} catch(Throwable t) {
							throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
						}

						if (hayFondeoPropio) {
							estatusSolic = 10; //Operada con fondeo propio
						} else {
							estatusSolic = 1;	//Seleccionado IF
						}

						//Si el Banco de Fondeo es Bancomext y el estatus de la solicitud esta como 1 (Seleccionado IF)
						if(claveBancoFondeo == 2 && estatusSolic == 1) {
							bloqueo = "6";
							estatusSolic = 2;	//2= En proceso
						}


						//Verifica que la Solicitud no exista previamente

						ResultSet rsSolic = null;
						boolean existeSolicitud = false;
						try {
							psSolicitud.setLong(1,lClaveDocumento);
							rsSolic = psSolicitud.executeQuery();
							rsSolic.next();
							existeSolicitud = (rsSolic.getInt("numRegistros") > 0)?true:false;
						} catch(Throwable t) {
							throw new Exception("Error al realizar la verificacion de la existencia de la solicitud.",t);
						} finally {
							rsSolic.close();
						}

						if (existeSolicitud) {
							doc.setErrorDetalle("ERROR|El documento ya tiene registrada una solicitud de descuento previa");
							doc.setTipoError(1);
							lDocumentosError.add(doc);
							continue; //Continua con el siguiente documento
						}

//-----------------------------------------------------------------------------------------------------------
						if (!hayFondeoPropio) { //No es fondeo propio
							try {
								if(iDiaPlazo > iPlazoMaxBO) {			///????????
									cFechaVencDesc.add(Calendar.DATE, iPlazoMaxBO);
									// Validamos si la fecha del plazo es d�a inhabil.
									Hashtable hNuevaFecha = this.setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO, "N");
									cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
									iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
									boolean bCambioPlazo = false;
									bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
									while(bCambioPlazo) {
										hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO, "N");
										cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
										iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
										bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
									}
									sFechaVencDesc = sdfv.format(cFechaVencDesc.getTime());
									diaVencimiento = sdfv.format(cFechaVencDesc.getTime()).substring(0,2);
								}
							} catch(Throwable t) {
								throw new Exception("Error en validaciones de plazo.",t);
							}
						} // Fin de no es fondeo propio

						String sFechaVencimiento = (sFechaVencDesc.equals(""))?fechaVencimiento:sFechaVencDesc;

						log.debug("confirmacionDoctosIFWSV2():: Validaciones OK. Se prepara la generacion de la solicitud");
						

						//error = false;
						try {
							folio = getFolioSolicitud(claveIF,con);
							
						} catch(Throwable t) {
							throw new Exception("Error al obtener el folio de la solicitud.",t);
						}
						try {
							log.debug("sFechaVencimiento="+ sFechaVencimiento);
							log.debug("fechaSigDol="+ fechaSigDol);
							log.debug("fechaDocumento="+ fechaDocumento);
							psRegistroSolicitud.setString(1, folio);
							psRegistroSolicitud.setLong(2, lClaveDocumento);
							psRegistroSolicitud.setString(3, acuse);
							psRegistroSolicitud.setString(4, sFechaVencimiento);
							psRegistroSolicitud.setInt(5, claveMoneda);
							psRegistroSolicitud.setString(6, fechaSigDol);
							psRegistroSolicitud.setInt(7, claveClaseDocumento);
							psRegistroSolicitud.setInt(8, claveTasa);
							psRegistroSolicitud.setString(9, sFechaVencimiento);
							psRegistroSolicitud.setString(10, sFechaVencimiento);
							psRegistroSolicitud.setString(11, sFechaVencimiento);
							psRegistroSolicitud.setString(12, sFechaVencimiento);
							psRegistroSolicitud.setInt(13, Integer.parseInt(diaVencimiento));
							psRegistroSolicitud.setString(14, fechaDocumento);
							psRegistroSolicitud.setInt(15, estatusSolic);
							if (bloqueo == null || bloqueo.equals("NULL") || bloqueo.equals("")) {
								psRegistroSolicitud.setNull(16, Types.NUMERIC);
							} else {
								psRegistroSolicitud.setInt(16, Integer.parseInt(bloqueo));
							}
							psRegistroSolicitud.setString(17, nombreEpo);
							psRegistroSolicitud.setInt(18, claveBancoFondeo);

							//Inserci�n de la solicitud
							psRegistroSolicitud.executeUpdate();
						} catch(Throwable t) {
							throw new Exception("Error al registrar la solicitud " + folio + " en BD.",t);
						}
						log.debug("confirmacionDoctosIFWSV2():: Solicitud: " + folio + " insertada");

						try {
							// Inserci�n de la amortizacion en com_amortizacion
							psAmortizacion.setString(1, folio);
							psAmortizacion.setDouble(2, Double.parseDouble(importeRecibir));
							psAmortizacion.setString(3, fechaVencimiento);
							psAmortizacion.executeUpdate();
						} catch(Throwable t) {
							throw new Exception("Error al registrar la amortizacion.",t);
						}

						log.debug("confirmacionDoctosIFWSV2():: Amortizacion:  insertada");
						try {
							//Actualizamos al documento con el estatus Operada (4)
							// Modifica el estatus del documento
							psUpdate.setInt(1, 4);	//Documento Operado
							psUpdate.setLong(2,lClaveDocumento);
							psUpdate.executeUpdate();
						} catch(Throwable t) {
							throw new Exception("Error al realizar el cambio de estatus (a operado) del documento.",t);
						}

						log.debug("confirmacionDoctosIFWSV2():: Estatus del documento actualizado");
						PreparedStatement psNC = null;
						try {
							if(bOperaNotasCredito) {
								//Actualiza las Notas de credito asociadas a la solicitud procesada
								qrySentencia =
										" UPDATE COM_DOCUMENTO SET ic_estatus_docto = ? " +
										" WHERE ic_docto_asociado = ? ";
								psNC = con.queryPrecompilado(qrySentencia);
								psNC.setInt(1,4);	//Documento operado
								psNC.setLong(2,lClaveDocumento);
								psNC.executeUpdate();
								log.debug("confirmacionDoctosIFWSV2():: Notas de credito asociadas actualizadas");
							} //Opera Notas de credito
						}catch(Throwable t) {
							con.terminaTransaccion(false);
							throw new Exception("Error al actualizar las notas de credito asociadas a la solicitud procesada", t);
						} finally {
							if(psNC != null)  {
							psNC.close();
							}
						}
//-----------------------------------------------------------------------------------------------------------
						//Debido a que hay cargas parciales, aquellos registros que lleguen hasta aqui
						//deben ser insertados definitivamente. Por ello se realiza el commit por documento valido
						con.terminaTransaccion(true);
						numOperados++;
					} //Fin de procesamiento estatus de documento 4 (operado)
				} catch(Throwable t) {
					//Error inesperado en el procesamiento del documento.
					//Debe continuar con el siguiente documento.
					doc.setErrorDetalle("ERROR INESPERADO|" + detalleError.toString() + "\nDetalle: " + t.getMessage());
					doc.setTipoError(2);
					lDocumentosError.add(doc);
					erroresInesperados.append(doc.getErrorDetalle()+"\r\n");
					log.error("Hubo errores Inesperados "+erroresInesperados);

					log.error("confirmacionDoctosIFWSV2(Error) ", t);
					try {
						if (con.hayConexionAbierta()) {
							//en caso de error inesperado realiza un rollback.
							con.terminaTransaccion(false);
						}
					} catch(Throwable t1) {
						log.error("confirmacionDoctosIFWSV2(Error) ", t1);
					}
					continue;
				} finally{
					
					if(banderaV2){
						if(doc.getErrorDetalle().equals("")){
							doc.setFolio(folio);
							doc.setAcuseConfirmacion(acuse);
							SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
							Date fechaDate = new Date();
							//log.info("\n\naqui pase.\n\n");
							String fecha=formateador.format(fechaDate);
							doc.setFechaAltaSolicitud(fecha);
						}
						lDocumentosV2.add(doc);
						
					}
				}//fin del try-catch del ciclo
							
				no_documentos.append(doc.getClaveDocumento()+",");
				
			} //fin for
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//---------------------------------- Fin del ciclo -----------------------------------
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
		
			//Se actualizan los datos del acuse con base en los registros insertados en com_solicitud (Solicitudes generadas)
			//Si no hay registros o existe un error al actualizar el acuse, se elimina el acuse
			boolean acuseNoActualizado = true;
			if (numOperados > 0 || numNegociables>0 ) {
				try {
					acuseNoActualizado = this.ActualizaAcusePanIF(acuse, receipt,  con, no_documentos.toString() );							
				} catch (Throwable t) {
					throw new Exception("Error al actualizar los datos del acuse", t);
				}
			} else {
				acuseNoActualizado = true;
			}						
						
			if (acuseNoActualizado && !acuse.equals("") ) {
				//Si el acuse no se actualizo porque No se registraron solicitudes (no se operaron documentos)
				//, se elimina el acuse
				BorraAcuse(acuse,con); //BorraAcuse realiza commit
				acuse = null;	//Dado que el acuse se borra se establece en Null, para identificar que finalmente no hubo acuse.
			}			
			
			exito=true;

			DocumentoIFWSV2[] arrDocumentosError = null;
			if (lDocumentosError.size() > 0) {
				arrDocumentosError = new DocumentoIFWSV2[lDocumentosError.size()];
				lDocumentosError.toArray(arrDocumentosError);
			}

			resultadoProceso.setDocumentos(arrDocumentosError);

			//NOTA: Que el codigo de ejecuci�n sea 1, no significa que no haya errores en los documentos
			//Ver documentaci�n del M�todo
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
			   
			String datosAcuse = "";
			try {
				if (acuse != null) {	//Si hay acuse... porque hubo registros procesados
					String qryAcuse =
							" SELECT in_monto_mn,in_monto_int_mn," +
							" in_monto_dscto_mn,in_monto_dl, " +
							" in_monto_int_dl,in_monto_dscto_dl " +
							" FROM com_acuse3 " +
							" WHERE cc_acuse = ? ";
					PreparedStatement psAcuse = con.queryPrecompilado(qryAcuse);
					psAcuse.setString(1, acuse);
					ResultSet rsAcuse = psAcuse.executeQuery();
					if (rsAcuse.next()) {
						String montoTotalMN = rsAcuse.getString("in_monto_mn");
						String montoInteresTotalMN = rsAcuse.getString("in_monto_int_mn");
						String montoDescuentoTotalMN = rsAcuse.getString("in_monto_dscto_mn");
						String montoTotalDL = rsAcuse.getString("in_monto_dl");
						String montoInteresTotalDL = rsAcuse.getString("in_monto_int_dl");
						String montoDescuentoTotalDL = rsAcuse.getString("in_monto_dscto_dl");

						datosAcuse =
								"Acuse: " + acuse + "\n" +
								"Numero de Documentos recibidos: " + documentos.length + "\n" +
								"Documentos con Error: " + lDocumentosError.size() + "\n" +
								"Monto MXP: " + montoTotalMN + "\n" +
								"Monto Interes MXP: " + montoInteresTotalMN+ "\n" +
								"Monto Descuento MXP: " + montoDescuentoTotalMN + "\n" +
								"Monto USD: " + montoTotalDL + "\n" +
								"Monto Interes USD: " + montoInteresTotalDL + "\n" +
								"Monto Descuento USD: " + montoDescuentoTotalDL + "\n";
					}
					rsAcuse.close();
					psAcuse.close();
				}
				resultadoProceso.setResumenEjecucion("PROCESO FINALIZADO\n" + datosAcuse);   	
				
			   if (acuse != null) {
					CrearArchivosAcuses arch = new CrearArchivosAcuses();

					arch.setAcuse(acuse);
					arch.setUsuario(claveUsuario);
					arch.setTipoUsuario("IF");
					arch.setVersion("WEBSERVICE");

					try {

						arch.setResumenEjecucion(resultadoProceso.getResumenEjecucion());
						arch.setRecibo(resultadoProceso.getRecibo());
						arch.guardarArchivo();

					} catch (Exception exception) {

						java.io.StringWriter outSW = new java.io.StringWriter();
						exception.printStackTrace(new java.io.PrintWriter(outSW));
						String stackTrace = outSW.toString();
						arch.setDesError(stackTrace);
						arch.guardaBitacoraArch();						
					}

				}
				
				
				
			} catch (Throwable t) {
				throw new Exception("Error al obtener los datos del acuse", t);
			}

		} catch (Throwable t) { //Aqui llegan todos los errores inesperados que puedan ocurrir.
			exito = false;
			DocumentoIFWSV2[] arrDocumentosError = null;
			log.error("confirmacionDoctosIFWSV2(Error) ", t);
			// Si los errores son antes de que empiece el ciclo de procesamiento de documentos
			// significa que no proceso ningun registro, por lo cual se debe regresar
			// todos los documentos recibidos, como documentos con error...
			if (i == 0) { //si i es 0 significa que no entro al ciclo para procesar docto
				for (i=0; i < documentos.length; i++) {
					DocumentoIFWSV2 doc = documentos[i];
					doc.setErrorDetalle("ERROR INESPERADO|Registro no procesado");
					doc.setTipoError(2);

					erroresInesperados.append(doc.getErrorDetalle()+"\r\n");
					log.error("Hubo errores Inesperados "+erroresInesperados);

				}
				arrDocumentosError = documentos;
				//borra el acuse ya que no se proceso ningun registro
			} else {
				// Si el proceso es despues del ciclo de procesamiento de documentos,
				// si hubo errores, ya sea esperados o inesperados, estos estan especificados
				// en el detalle de Error del objeto del documento
				if (lDocumentosError.size() > 0) {
					arrDocumentosError = new DocumentoIFWSV2[lDocumentosError.size()];
					lDocumentosError.toArray(arrDocumentosError);
				}
			}
			
			resultadoProceso.setDocumentos(arrDocumentosError);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());
			
			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("Hubo errores Inesperados "+erroresInesperados);


			/* llamado del metodo para enviar correo de Errores Inesperados siempre y cuando hayan*/
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"C");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}

		} finally {
		if(banderaV2){
				DocumentoIFWSV2[] arrDocumentosError = new DocumentoIFWSV2[lDocumentosV2.size()];
				lDocumentosV2.toArray(arrDocumentosError);
				resultadoProceso.setDocumentos(arrDocumentosError);
			}
			if (con.hayConexionAbierta()) {
				try {
					psParamNotasCred.close();
					psRegistroSolicitud.close();
					psUpdate.close();
					psRechazo.close();
					psSolicitud.close();
					psDoc.close();
					psAmortizacion.close();
					con.terminaTransaccion(exito);
					//Si hubo un error inesperado que interrumpio el proceso (exito = false) y hay acuse 
					//entonces se borra dicho acuse.
					if ( !exito && !acuse.equals("") ) {
						//dado que BorraAcuse da commit, se coloca hasta el final despues
						//de con.terminaTransaccion(exito); para que no afecte la transaccion principal
						BorraAcuse(acuse, con);
					}
					con.cierraConexionDB();
				} catch(Exception e1) {
					log.error("confirmacionDoctosIFWSV2(Error)::" + e1.getMessage());
				}
			}
			log.info("AutorizacionDescuentoBean::confirmacionDoctosIFWSV2(S)("+claveIF+"_"+claveUsuario+")");
		}
		return resultadoProceso;
	}

	/**
	 * Obtiene la parametrizaci�n de moneda del IF para manejo en Web Service
	 *
	 * @param iClaveIF Clave del Intermediario (ic_if)
	 * @return Cadena con la informaci�n de la moneda manejada
	 * 		MN - Moneda Nacional
	 *       DA - Dolares Americanos
	 *       A  - Ambas
	 */
	private String getParametrizacionMonedaWS(int iClaveIF) throws Exception {
		AccesoDB con = null;
		String monedaManejada = "A";
		try {
			con = new AccesoDB();
			con.conexionDB();
			//Se obtiene la parametrizacion de la moneda
			String strSQL =
					" SELECT cg_valor " +
					"  FROM com_parametrizacion_if " +
					" WHERE ic_if = ? AND cc_parametro_if = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, iClaveIF);
			ps.setString(2, "TIPO_MONEDA");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				monedaManejada = (rs.getString("cg_valor")!= null)?rs.getString("cg_valor"):"A";
			}
			rs.close();
			ps.close();
			return monedaManejada;
		} catch(Throwable t) {
			throw new Exception("Error al obtener la parametrizacion del IF. ", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public ProcesoIFWSInfo getDoctosSelecPymeWS(String claveUsuario,
			String password, String claveIF) {
				ProcesoIFWSInfoV2 procV2=getDoctosSelecPymeWSV2( claveUsuario,
			 password,  claveIF,"");
			 ProcesoIFWSInfo proceso=new ProcesoIFWSInfo();
			 proceso.setCodigoEjecucion(procV2.getCodigoEjecucion());
			 proceso.setResumenEjecucion(procV2.getResumenEjecucion());
			 
			 DocumentoIFWSV2[] arrDocumentosV2 = procV2.getDocumentos();
			 
			 if(arrDocumentosV2!=null){
				 DocumentoIFWS[] arrDocumentos=new DocumentoIFWS[arrDocumentosV2.length];
				 for(int i=0;i<arrDocumentosV2.length;i++){
						arrDocumentos[i]=new DocumentoIFWS();
						arrDocumentos[i].setAcusePyme(arrDocumentosV2[i].getAcusePyme());
						arrDocumentos[i].setClaveBancoFondeo(arrDocumentosV2[i].getClaveBancoFondeo());
						arrDocumentos[i].setClaveDocumento(arrDocumentosV2[i].getClaveDocumento());
						arrDocumentos[i].setClaveEpo(arrDocumentosV2[i].getClaveEpo());
						arrDocumentos[i].setClaveMoneda(arrDocumentosV2[i].getClaveMoneda());
						arrDocumentos[i].setClaveEstatus(arrDocumentosV2[i].getClaveEstatus());
						arrDocumentos[i].setFechaEmision(arrDocumentosV2[i].getFechaEmision());
						arrDocumentos[i].setFechaSeleccionPyme(arrDocumentosV2[i].getFechaSeleccionPyme());
						arrDocumentos[i].setFechaPublicacion(arrDocumentosV2[i].getFechaPublicacion());
						arrDocumentos[i].setFechaVencimiento(arrDocumentosV2[i].getFechaVencimiento());
						arrDocumentos[i].setMontoDescuento(arrDocumentosV2[i].getMontoDescuento());
						arrDocumentos[i].setTasaAceptada(arrDocumentosV2[i].getTasaAceptada());
						arrDocumentos[i].setImporteInteres(arrDocumentosV2[i].getImporteInteres());
						arrDocumentos[i].setImporteRecibir(arrDocumentosV2[i].getImporteRecibir());
						arrDocumentos[i].setNombreEpo(arrDocumentosV2[i].getNombreEpo());
						arrDocumentos[i].setNombreMoneda(arrDocumentosV2[i].getNombreMoneda());
						arrDocumentos[i].setNombrePyme(arrDocumentosV2[i].getNombrePyme());
						arrDocumentos[i].setNumeroDocumento(arrDocumentosV2[i].getNumeroDocumento());
						arrDocumentos[i].setNumeroProveedor(arrDocumentosV2[i].getNumeroProveedor());
						arrDocumentos[i].setNumeroSirac(arrDocumentosV2[i].getNumeroSirac());
						arrDocumentos[i].setTipoFactoraje(arrDocumentosV2[i].getTipoFactoraje());
						arrDocumentos[i].setClaveRechazo(arrDocumentosV2[i].getClaveRechazo());
						arrDocumentos[i].setErrorDetalle(arrDocumentosV2[i].getErrorDetalle());
						arrDocumentos[i].setCampoAdicional1(arrDocumentosV2[i].getCampoAdicional1());
						arrDocumentos[i].setCampoAdicional2(arrDocumentosV2[i].getCampoAdicional2());
						arrDocumentos[i].setCampoAdicional3(arrDocumentosV2[i].getCampoAdicional3());
						arrDocumentos[i].setCampoAdicional4(arrDocumentosV2[i].getCampoAdicional4());
						arrDocumentos[i].setCampoAdicional5(arrDocumentosV2[i].getCampoAdicional5());
						arrDocumentos[i].setMontoDocumento(arrDocumentosV2[i].getMontoDocumento());
						arrDocumentos[i].setPorcentajeDescuento(arrDocumentosV2[i].getPorcentajeDescuento());
						arrDocumentos[i].setRecursoGarantia(arrDocumentosV2[i].getRecursoGarantia());
						arrDocumentos[i].setPlazo(arrDocumentosV2[i].getPlazo());
						arrDocumentos[i].setNombreEstatus(arrDocumentosV2[i].getNombreEstatus());
						arrDocumentos[i].setReferencia(arrDocumentosV2[i].getReferencia());
						arrDocumentos[i].setClaveIF(arrDocumentosV2[i].getClaveIF());
						arrDocumentos[i].setNombreIF(arrDocumentosV2[i].getNombreIF());
						//FVR
						arrDocumentos[i].setClavePYME(arrDocumentosV2[i].getClavePYME());
						arrDocumentos[i].setFechaNotificacion(arrDocumentosV2[i].getFechaNotificacion());
						arrDocumentos[i].setPorcentajeAnticipo(arrDocumentosV2[i].getPorcentajeAnticipo());
						arrDocumentos[i].setNombreTipoFactoraje(arrDocumentosV2[i].getNombreTipoFactoraje());
						arrDocumentos[i].setAcuseIF(arrDocumentosV2[i].getAcuseIF());
					 
				 }
				 proceso.setDocumentos(arrDocumentos);
			 }
			 return proceso;
			}
	public ProcesoIFWSInfoV2 getDoctosSelecPymeWSV2(String claveUsuario,
			String password, String claveIF) {
				return getDoctosSelecPymeWSV2( claveUsuario,
			 password,  claveIF,"Version2");
			}

	/**
	 * Obtiene Los documentos Seleccionados Pyme, para su operaci�n de
	 * Descuento.
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * Al momento de regresar los documentos que se encuentran seleccionados
	 * por la Pyme, estos se pasan autom�ticamente a estatus 24 (En proceso
	 * de Autorizacion IF)
	 *
	 * Los resultados de este m�todo, dependen de la parametrizacion del IF
	 * "TIPO_MONEDA" en la Base de Datos. Si este es "A" Regresa MN y USD.
	 * Si es "MN" solo regresa los documentos de Moneda Nacional (ic_moneda = 1)
	 * y si es "DA" solo regresa Dolares Americanos (ic_moneda = 54)
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 * 		del proceso mas los objetos DocumentoIFWS con los datos de los
	 *    documentos seleccionados
	 */
	public ProcesoIFWSInfoV2 getDoctosSelecPymeWSV2(String claveUsuario,
			String password, String claveIF, String version) {
		log.info("AutorizacionDescuentoBean::getDoctosSelecPymeWSV2(E)("+claveIF+"_"+claveUsuario+")");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		List lDocumentos = new ArrayList();
		StringBuffer EposIn =  new StringBuffer();  
		String nombreIF  ="";

		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

		//Los ERRORES INESPERADOS se relanzan para que el Catch m�s externo
		//sea el encargado de considerarlos. Agregando al resumen de ejecucion
		//ERROR INESPERADO|
		//Los errores inesperados incluyen por ejemplo a las fallas de
		// BD, de conexion al OID, etc...
		int iClaveIF = 0;		
		StringBuilder epoNoCumplenHorario =  new StringBuilder();  
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}

			iClaveIF = Integer.parseInt(claveIF);

		} catch(Exception e) {
			log.error("getDoctosSelecPymeWSV2(Error)(" + claveIF + "_" + claveUsuario + ") ", e);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF + ")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}
		//****************************************************************************************


		AccesoDB con = null;
		boolean updateOk = true;
		String monedaParametrizadaWS = "";
		Map mEpos = new HashMap();
		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}

			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = null;
			try {
				cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			} catch(Throwable t) {
				t.printStackTrace();
				throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
			}
			log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("getDoctosSelecPymeWSV2::cuentasIF:"+cuentasIF);
                    
                        if(cuentasIF.size()>0) {
                            if (!cuentasIF.contains(claveUsuario)) {
                                resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
                                resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para el IF "+claveIF);
                                return resultadoProceso;
                            }
                        } else if(cuentasIF.size()<=0) {
                            resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
                            resultadoProceso.setResumenEjecucion("ERROR|No se encontraron cuentas para el IF "+claveIF);
                            return resultadoProceso;
                        }
                    
			log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + ")::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					return resultadoProceso;
				}
			} catch(Throwable t) {
				throw new Exception("Error en el componente de validacion de usuario. ", t);
			}

			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			monedaParametrizadaWS = getParametrizacionMonedaWS(iClaveIF);
			String condicionMonedaWS = "";
			if (monedaParametrizadaWS.equals("MN") || monedaParametrizadaWS.equals("DA")) {
                            condicionMonedaWS = " AND d.ic_moneda = ? ";
			}
			
			//NE_2018_02 (i) parametro para saber si el  Intermediario Financiero puede descargar los documentos de factoraje Distribuido. 
			String descargaFactoDistribuido = this.getParametrosIf( iClaveIF, "DESCARGA_FACTO_DISTRIBUIDO");			
			log.debug ("descargaFactoDistribuido :: "+descargaFactoDistribuido);
			
			String condiDesFecDistri = "";
			if ("N".equals(descargaFactoDistribuido)  ) { 
				condiDesFecDistri =  " AND d.cs_dscto_especial != ?  ";
			}			
			//NE_2018_02 (f)
			
			PreparedStatement ps = null;
			PreparedStatement psE = null;
			PreparedStatement psI = null;
			
			try {
				
				ResultSet rsI = null;
				String strSQLIF = " select cg_razon_social as NOMBREIF  from comcat_if  where  ic_if =  ?";
				psI = con.queryPrecompilado(strSQLIF);
				psI.setInt(1, iClaveIF);
				rsI = psI.executeQuery();
				if(rsI.next()) {
                                    nombreIF = rsI.getString("NOMBREIF");
				}
				rsI.close();psI.close();
                            
				// OBTENGO LAS EPOS Y  VALIDO SI TIENEN  LIMITE SOBREGIRADO
				
				String strSQLEPO =
                                                " SELECT /*+index (d IN_COM_DOCUMENTO_04_NUK)*/  "+
                                                "        d.ic_epo            AS claveepo, "+
                                                "        e.cg_razon_social   AS nombreepo, "+
                                                "        COUNT(*) AS total_docto, "+
                                                "        SUM(fn_monto_dscto) AS monto_descto, "+
                                                "        d.ic_moneda         AS moneda "+
                                                " FROM com_documento              d, "+
                                                "      comrel_pyme_epo            pe, "+
                                                "      comrel_if_epo_x_producto   iexp, "+
                                                "      comcat_epo                 e "+
                                                " WHERE d.ic_epo = pe.ic_epo "+
                                                "       AND d.ic_pyme = pe.ic_pyme "+
                                                "       AND d.ic_if = iexp.ic_if "+
                                                "       AND d.ic_epo = iexp.ic_epo "+
                                                "       AND d.ic_epo = e.ic_epo "+
                                                "       AND pe.cs_habilitado = ? "+
                                                "       AND iexp.ic_producto_nafin = ? "+
                                                "       AND d.cs_dscto_especial != ? "+
                                                "       AND d.ic_if = ? "+
                                                condicionMonedaWS+
                                                "       AND d.ic_estatus_docto = ? "+
                                                " GROUP BY d.ic_epo, "+
                                                "          e.cg_razon_social, "+
                                                "          d.ic_moneda "+
                                                " ORDER BY d.ic_epo ";
                            
				List lParams = new ArrayList();
                                lParams.add("S"); //Relacion Pyme-Epo Habilitada S=Si
                                lParams.add(new Integer(1));  //1.- Descuento
                                lParams.add("C"); //C=Notas de Credito  
                                lParams.add(new Integer(iClaveIF));
                                if (monedaParametrizadaWS.equals("MN")) {
                                    lParams.add(new Integer(1));        //1.- Moneda Nacional
                                } else if (monedaParametrizadaWS.equals("DA")) {
                                    lParams.add(new Integer(54));       //54.- Dolares Americanos
                                }                                                               
				lParams.add(new Integer(3)); //Estatus del Documento 3= Seleccionado Pyme
				log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: strSQLEPO=" + strSQLEPO);
				log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: strSQLEPO Params=" + lParams);
				psE = con.queryPrecompilado(strSQLEPO, lParams);
								
				ResultSet rsE = null;
				rsE = psE.executeQuery();
				
				while (rsE.next()) {
					//ValidacionLimiteEPO validaLimEpo = (ValidacionLimiteEPO)mEpos.get(rsE.getString("claveEpo"));
					String mensaje = "", mensaje_dl = "";
					String montoDiferencia = "", montoDiferencia_dl = "";					
					String ic_moneda =  rsE.getString("moneda"); 
					
					
						Map resValida = this.validaLimiteEpo(rsE.getString("claveEpo"), claveIF);
						mensaje  = (String)resValida.get("MENSAJE");
						montoDiferencia  = (String)resValida.get("MONTO_DIFERIENCIA").toString().replace('-',' ');
						
						mensaje_dl  = (String)resValida.get("MENSAJE_DL");
						montoDiferencia_dl  = (String)resValida.get("MONTO_DIFERIENCIA_DL").toString().replace('-',' ');
						
						ValidacionLimiteEPO validacionLimite = new ValidacionLimiteEPO();
						validacionLimite.setNombreEPO(rsE.getString("nombreEpo"));
						validacionLimite.setMontoDoctos(rsE.getString("MONTO_DESCTO"));
						validacionLimite.setTotalDoctos(rsE.getString("TOTAL_DOCTO"));	
						
						if (ic_moneda.equals("1")) {   //Moneda Nacional 
						
							if(mensaje.equals("NoHaySobreGirado")) {
								validacionLimite.setSobregiro(false);
								if (validaHoraEnvOpeIFs(rsE.getString("claveEpo"))) {
									//Solo considera las EPOS que pasen la validaci�n de horario. (Actualmente EPOs de PEMEX)											
									
									if(validaHoraEpoIFs( rsE.getString("claveEpo"),  claveIF)) {	//2018_02											
										
										EposIn.append(rsE.getString("claveEpo")+",");	 // Epos que NoHaySobreGirado
									}else {										
										// no cumple con el horario de EPO e IF  2018_02
										epoNoCumplenHorario.append(epoNoCumplenHorario.length() > 0 ? "\n " :"").append(rsE.getString("nombreEpo").toString() +", No. de Documentos = "+rsE.getString("TOTAL_DOCTO") +" Moneda =  MONEDA NACIONAL" );	
									}
									
								} else {
									log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Epo omitida por validacion validaHoraEnvOpeIFs: " + rsE.getString("claveEpo"));
								}
							} else if(mensaje.equals("SobreGirado")) {
								validacionLimite.setSobregiro(true);
								validacionLimite.setMontoSobregiro(montoDiferencia);		
								validacionLimite.setTipoMoneda("Moneda Nacional");	
								mEpos.put(rsE.getString("claveEpo")+'|'+ic_moneda, validacionLimite);// Epos con SobreGirado
							}
						
						}else  if (ic_moneda.equals("54")) {  //Dolar  Americano 						
						
							if(mensaje_dl.equals("NoHaySobreGirado")) {
								validacionLimite.setSobregiro(false);
								if (validaHoraEnvOpeIFs(rsE.getString("claveEpo"))) {
									//Solo considera las EPOS que pasen la validaci�n de horario. (Actualmente EPOs de PEMEX)
									
									if(validaHoraEpoIFs( rsE.getString("claveEpo"),  claveIF)) {	//2018_02		
											EposIn.append(rsE.getString("claveEpo")+",");	 // Epos que NoHaySobreGirado
									}else {										
										// no cumple con el horario de EPO e IF  2018_02
										epoNoCumplenHorario.append(epoNoCumplenHorario.length() > 0 ? "\n " :"").append(rsE.getString("nombreEpo").toString() +", No. de Documentos = "+rsE.getString("TOTAL_DOCTO") +" Moneda =  MONEDA NACIONAL" );	
									}
																	
								} else {
									log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Epo omitida por validacion validaHoraEnvOpeIFs: " + rsE.getString("claveEpo"));
								}
							} else if(mensaje_dl.equals("SobreGirado")) {
								validacionLimite.setSobregiro(true);
								validacionLimite.setMontoSobregiro(montoDiferencia_dl);	
								validacionLimite.setTipoMoneda("D�lar Americano");									
								mEpos.put(rsE.getString("claveEpo")+'|'+ic_moneda, validacionLimite);// Epos con SobreGirado
							}
							
						}
					
				}
				rsE.close();
				psE.close();
				
				log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Epos sin Sobregiro: " + EposIn);
				log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Epos con Sobregiro: " + mEpos);
				
			} catch(Throwable t) {
				throw new Exception("Error Error al realizar la consulta para obtener las EPOS ", t);
			}
				//*********************************************
			
			log.debug("----------EposIn----------"+ EposIn);	
			if(!EposIn.toString().equals("")) {
				try {				
					EposIn.deleteCharAt(EposIn.length()-1);
                                    
                                        List<String> lEposIn = Arrays.asList(EposIn.toString().split("\\s*,\\s*"));
                                        Set<String> slEposIn = new HashSet<String>(lEposIn);
                                        lEposIn = new ArrayList<String>(slEposIn);
                                        //clear the Stringbuffer content
                                        EposIn.delete(0, EposIn.length());
                                        for(String sEPOs : lEposIn) {
				            EposIn = EposIn.length() > 0 ? EposIn.append(",").append("?") : EposIn.append("?");
				        }
                                    
                                    
					// Obtengo todos los documentos 
					String strSQL =
						" SELECT /*+index(d IN_COM_DOCUMENTO_04_NUK)*/ " +
						"  ds.cc_acuse as acusePyme, " +
						"  e.ic_banco_fondeo as claveBancoFondeo, " +
						"  d.ic_documento as claveDocumento, " +
						"  e.ic_epo as claveEpo, " +
						"  i.ic_if as claveIF, " +
						"  i.cg_razon_social  as nombreIF, " +
						"  CASE WHEN m.ic_moneda = 1 THEN 'MXP' WHEN m.ic_moneda = 54 THEN 'USD' END AS claveMoneda, " +
						"  d.ic_estatus_docto as claveEstatus, " +
						"  TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as fechaEmision, " +
						"  TO_CHAR(ds.df_fecha_seleccion,'dd/mm/yyyy') as fechaSeleccionPyme, " +
						"  TO_CHAR(d.df_alta,'dd/mm/yyyy') as fechaPublicacion, " +
						"  TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as fechaVencimiento, " +
						"  d.fn_monto_dscto as montoDescuento, " +
						" 	d.df_fecha_venc -  " +
						" 	(CASE  " +
						" 	WHEN d.ic_moneda = 1 OR (d.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN d.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo_sydb(d.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) AS plazo, " +
						//"  (d.df_fecha_venc - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, sigfechahabilxepo(d.ic_epo, TRUNC(SYSDATE), 1, '+')) ) as plazo, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada) as tasaAceptada, " +
						"  d.fn_monto as montoDocumento, " +
						"  d.fn_monto-d.fn_monto_dscto as recursoGarantia, " +
						"  TO_CHAR(d.fn_monto_dscto/d.fn_monto*100,'999') as porcentajeDescuento, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',p.cg_razon_social,d.ct_referencia) as ct_referencia, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.in_importe_interes_fondeo,0),ds.in_importe_interes) as importeInteres, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.in_importe_recibir_fondeo,0) ,ds.in_importe_recibir) as importeRecibir, " +
						"  e.cg_razon_social as nombreEpo, " +
						"  m.cd_nombre as nombreMoneda, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',ifs.cg_razon_social,p.cg_razon_social) as nombrePyme, " +
						"  d.ig_numero_docto as numeroDocumento, " +
						"  pe.cg_pyme_epo_interno as numeroProveedor, " +
						"  DECODE(ds.CS_OPERA_FISO,'S',99999, p.in_numero_sirac) as numeroSirac, " +
						"  d.cs_dscto_especial as tipoFactoraje, " +
						"  ctf.cg_nombre as nombreFactoraje,"+
						"  ed.cd_descripcion as nombreEstatus, " +
						"  d.ct_referencia as referencia, " +
						"  d.cg_campo1 as campoAdicional1, " +
						"  d.cg_campo2 as campoAdicional2, " +
						"  d.cg_campo3 as campoAdicional3, " +
						"  d.cg_campo4 as campoAdicional4, " +
						"  d.cg_campo5 as campoAdicional5, " +
						"  p.cg_nombre as nombrePF, p.cg_appat apellidoPaternoPF, p.cg_apmat apellidoMaternoPF, " +
						"  ds.cg_cuenta, c.cg_email as emailBeneficiario, " +
						"  p.cs_tipo_persona, " + 
						"  d.CS_VALIDACION_JUR as VALIDACION_JUR ,  "+  //NE_2018_02
						"  'AutorizacionDescuentoBean::getDoctosSelecPymeWS'  " +
						"  FROM   " +
						"     com_documento d, " +
						"     com_docto_seleccionado ds,  " +
                                                "     comrel_pyme_epo pe, " +                                                
                                                "     comrel_if_epo_x_producto iexp, " +                                                
						"     comcat_pyme p,  " +
                                                "     com_contacto c, " +
						"     comcat_epo e, " +
						"     comcat_if i, " +
						"     comcat_if ifs, " +
                                                "     comcat_moneda m, " +
						"     comcat_estatus_docto ed, " +
						"     comcat_tipo_factoraje ctf "+
						"  WHERE d.ic_documento = ds.ic_documento " +
                                                "  and d.ic_epo = pe.ic_epo  " +
                                                "  and d.ic_pyme = pe.ic_pyme " +
                                                "  and d.ic_pyme = c.ic_pyme " +
                                                "  and d.ic_if = iexp.ic_if " +
                                                "  and d.ic_epo = iexp.ic_epo " +
                                                "  and d.ic_pyme = p.ic_pyme " +
                                                "  and d.ic_epo = e.ic_epo " +                                                
                                                "  and d.ic_if = i.ic_if " +
                                                "  and ds.ic_if = ifs.ic_if " +
                                                "  and d.ic_moneda = m.ic_moneda " +
                                                "  and d.ic_estatus_docto = ed.ic_estatus_docto " +
						"  and d.cs_dscto_especial = ctf.cc_tipo_factoraje "+
                                                "  and iexp.ic_producto_nafin = ? " + 
						"  and c.cs_primer_contacto = ? " +
                                                "  and pe.cs_habilitado = ? " +
                                                "  and d.cs_dscto_especial != ? " +
                                                condiDesFecDistri + //NE_2018_02
                                                "  and d.ic_if=? " +
                                                condicionMonedaWS+
						"  and d.ic_epo IN ("+EposIn+")"+
						"  and d.ic_estatus_docto = ? " +
						"  and rownum <=200 " +
						"  ORDER BY d.ic_epo, d.ic_documento  ";//Es importante que el orden del query no se mueva.
					
					List lParams = new ArrayList();
                                        lParams.add(new Integer(1));            //1.- Descuento
                                        lParams.add("S");                       //Primer contacto S
                                        lParams.add("S");                       //Relacion Pyme-Epo Habilitada S=Si
                                        lParams.add("C");                       //C=Notas de Credito
                                        if ("N".equals(descargaFactoDistribuido)  ) {   //NE_2018_02 
                                            lParams.add("D"); //D= Distribuido
                                        }
                                        lParams.add(new Integer(iClaveIF));                             
                                        if (monedaParametrizadaWS.equals("MN")) {
                                            lParams.add(new Integer(1));        //1.- Moneda Nacional
                                        } else if (monedaParametrizadaWS.equals("DA")) {
                                            lParams.add(new Integer(54));       //54.- Dolares Americanos
                                        }
                                        for (String temp : lEposIn) {
                                            lParams.add(new Integer(temp));     //EposIn
                                        }
					lParams.add(new Integer(3));            //Estatus del Documento 3= Seleccionado Pyme
                                    
					log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: strSQL=" + strSQL);
					log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: strSQL Params=" + lParams);
                                    
					ps = con.queryPrecompilado(strSQL, lParams);
				} catch(Throwable t) {
					throw new Exception("Error al precompilar la consulta de documentos. ", t);
				}

				PreparedStatement psUpdate = null;
				try {
					String updateDocto =
						" UPDATE com_documento " +
						" SET ic_estatus_docto = ? " +
						" WHERE ic_estatus_docto = ? " +
						" AND ic_documento = ? ";
					psUpdate = con.queryPrecompilado(updateDocto);
				} catch(Throwable t) {
					throw new Exception("Error al precompilar la actualizacion de documentos.", t);
				}
				ResultSet rs = null;
				try {
					if(!EposIn.equals("")) {
						rs = ps.executeQuery();
					}
				} catch(Throwable t) {
					throw new Exception("Error al realizar la consulta de documentos.", t);
				}
				try {
					String claveDocumentoAnterior = "";
					while (rs.next()) {
						boolean valida = validaHoraEnvOpeIFs(rs.getString("claveEpo"));
						log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: validaHoraEnvOpeIFs(" + rs.getString("claveEpo") + ")=" + valida + ")");
						
						if(valida){
							if (claveDocumentoAnterior.equals(rs.getString("claveDocumento"))) {
								//Este bloque es para evitar informaci�n repetida si hay m�s de un contacto marcado como principal
								//lo cual reportaria mas de una vez el mismo documento. Es importante que el orden del query no se mueva.
								log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Documento repetido=" + rs.getString("claveDocumento"));
								continue;
							} else {
								claveDocumentoAnterior = rs.getString("claveDocumento");
							}
							
							//NE_2018_02
						   String validaJu =  (rs.getString("VALIDACION_JUR") == null)?"N":rs.getString("VALIDACION_JUR") ;
							String tipoFactoraje =  (rs.getString("tipoFactoraje") == null)?"N":rs.getString("tipoFactoraje") ;							
							log.debug (" tipoFactoraje  "+tipoFactoraje +"   validaJu  "+ validaJu);
							
							//NE_2018_02  los documentos  de Factoraje Distribuido se validar� que los documentos est�n autorizados por el �rea Jur�dica de PEMEX 
							if (!"D".equals(tipoFactoraje)   ||  (  "D".equals(tipoFactoraje)   &&  "S".equals(validaJu)) ) {						
						
							DocumentoIFWSV2 documento = new DocumentoIFWSV2();
							documento.setAcusePyme(rs.getString("acusePyme"));
							documento.setClaveBancoFondeo(rs.getString("claveBancoFondeo"));
							documento.setClaveDocumento(rs.getString("claveDocumento"));
							//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - INI
							String cveEpoExt = getClaveEpoExternaWSIF(String.valueOf(iClaveIF), rs.getString("claveEpo"), rs.getString("tipoFactoraje"));
							
							/*
							 if("1049".equals(rs.getString("claveEpo")) && "V".equals(rs.getString("tipoFactoraje")) && iClaveIF==9) {
								documento.setClaveEpo("9900");
							} else if("1097".equals(rs.getString("claveEpo")) && "V".equals(rs.getString("tipoFactoraje")) && iClaveIF==9) {
								documento.setClaveEpo("9901");
							} else if("902".equals(rs.getString("claveEpo")) && "V".equals(rs.getString("tipoFactoraje")) && iClaveIF==9) {
								documento.setClaveEpo("9902");
							} else if("1151".equals(rs.getString("claveEpo")) && "V".equals(rs.getString("tipoFactoraje")) && iClaveIF==9) {
								documento.setClaveEpo("9903");
							} else if("1333".equals(rs.getString("claveEpo")) && "V".equals(rs.getString("tipoFactoraje")) && iClaveIF==9) {
								documento.setClaveEpo("9905");
							}
							*/
							
							if(cveEpoExt!=null && !cveEpoExt.equals("")){
								documento.setClaveEpo(cveEpoExt);
							} else {
								documento.setClaveEpo(rs.getString("claveEpo"));
							}
							//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - FIN

							documento.setClaveMoneda(rs.getString("claveMoneda"));
							documento.setClaveEstatus(rs.getString("claveEstatus"));
							documento.setFechaEmision(rs.getString("fechaEmision"));
							documento.setFechaSeleccionPyme(rs.getString("fechaSeleccionPyme"));
							documento.setFechaPublicacion(rs.getString("fechaPublicacion"));
							documento.setFechaVencimiento(rs.getString("fechaVencimiento"));
							documento.setMontoDescuento(rs.getString("montoDescuento"));
							documento.setTasaAceptada(rs.getString("tasaAceptada"));
							documento.setImporteInteres(rs.getString("importeInteres"));
							documento.setImporteRecibir(rs.getString("importeRecibir"));
							documento.setNombreEpo(rs.getString("nombreEpo"));
							documento.setNombreMoneda(rs.getString("nombreMoneda"));
							documento.setNombrePyme(rs.getString("nombrePyme"));

							String numeroDeDocumento = rs.getString("numeroDocumento");
							numeroDeDocumento = (numeroDeDocumento!=null)?numeroDeDocumento.replaceAll("[\\p{Cntrl}&&[^\t]]", ""):numeroDeDocumento;
							documento.setNumeroDocumento(numeroDeDocumento);

							documento.setNumeroProveedor(rs.getString("numeroProveedor"));
							documento.setNumeroSirac(rs.getString("numeroSirac"));
							documento.setTipoFactoraje(rs.getString("tipoFactoraje"));
							documento.setNombreTipoFactoraje(rs.getString("nombreFactoraje"));
							documento.setClaveIF(rs.getString("claveIF"));
							documento.setNombreIF(rs.getString("nombreIF"));
							documento.setMontoDocumento(rs.getString("montoDocumento"));
							documento.setPorcentajeDescuento(rs.getString("porcentajeDescuento"));
							documento.setRecursoGarantia(rs.getString("recursoGarantia"));
							documento.setPlazo(rs.getString("plazo"));
							documento.setNombreEstatus(rs.getString("nombreEstatus"));
							String referencia = rs.getString("referencia");
							referencia = (referencia!=null)?referencia.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):referencia;
							String campoAdicional1 = rs.getString("campoAdicional1");
							campoAdicional1 = (campoAdicional1!=null)?campoAdicional1.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):campoAdicional1;
							String campoAdicional2 = rs.getString("campoAdicional2");
							campoAdicional2 = (campoAdicional2!=null)?campoAdicional2.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):campoAdicional2;
							String campoAdicional3 = rs.getString("campoAdicional3");
							campoAdicional3 = (campoAdicional3!=null)?campoAdicional3.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):campoAdicional3;
							String campoAdicional4 = rs.getString("campoAdicional4");
							campoAdicional4 = (campoAdicional4!=null)?campoAdicional4.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):campoAdicional4;
							String campoAdicional5 = rs.getString("campoAdicional5");
							campoAdicional5 = (campoAdicional5!=null)?campoAdicional5.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):campoAdicional5;

							documento.setReferencia(referencia);
							documento.setCampoAdicional1(campoAdicional1);
							documento.setCampoAdicional2(campoAdicional2);
							documento.setCampoAdicional3(campoAdicional3);
							documento.setCampoAdicional4(campoAdicional4);
							documento.setCampoAdicional5(campoAdicional5);
							
							documento.setNombrePF(rs.getString("nombrePF"));
							documento.setApellidoPaternoPF(rs.getString("apellidoPaternoPF"));
							documento.setApellidoMaternoPF(rs.getString("apellidoMaternoPF"));
							documento.setCuentaCLABE(rs.getString("cg_cuenta"));
							documento.setEmailBeneficiario(rs.getString("emailBeneficiario"));
							documento.setTipoPersona(rs.getString("cs_tipo_persona"));
							log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Se agrega a lista de documentos a regresar: " + documento.getClaveDocumento());
							lDocumentos.add(documento);
	
							psUpdate.clearParameters();
							psUpdate.setInt(1,24);
							psUpdate.setInt(2,3);
							psUpdate.setLong(3,rs.getLong("claveDocumento"));
							psUpdate.executeUpdate();
																			
							log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Cambio estatus a 24. documento=" + documento.getClaveDocumento());
							
							}//Factoraje
						}
					} //fin while
					psUpdate.close();
					rs.close();
					ps.close();
					updateOk = true;

				} catch(Throwable t) {
					throw new Exception("Error al obtener/actualizar los documentos.", t);
				}

			}
			
			//Notificaci�n de sobregiro de lineas.
			StringBuffer mensajeCorreo = new StringBuffer(
					"<html><head><style>"+
					"body{" +
					"	background-color:#FFFFFF;" +
					"	margin-left: 0px;" +
					"	margin-top: 0px;" +
					"	margin-right: 0px;" +
					"	margin-bottom: 0px;" +
					"	font-family: Verdana, Arial, Helvetica, sans-serif;" +
					"	font-size: 12px;" +
					"	font-weight: normal;" +
					"	color: #000000;" +
					"	text-decoration: none;" +
					"	background-position: 0px 0px;" +
					"	height:100%;" +
					"}" +
					"</style></head>" +
					"<body>"+
					"<b>" +nombreIF +"</b><br>" +
					"Se le comunica que las siguientes l�neas tienen un sobregiro " +
					"como se muestran a continuaci�n. Favor de verificar.<br><br>" +
					"<table border='1'><tr><td><b>Clave Epo</td><td><b>Nombre EPO</td><td><b>Total de Documentos</td> <td><b>Monto Total de Documentos</td><td><b>Monto sobregiro</td><td><b>Moneda</td></tr>");
			try {
			
								
				Iterator it = mEpos.keySet().iterator();
				boolean enviarCorreo = false;
				while (it.hasNext()) {
					String claveEpo1 = (String)it.next();									
					String claveEpo = claveEpo1.substring(0,claveEpo1.indexOf('|'));	
					
					ValidacionLimiteEPO valLimEpo = (ValidacionLimiteEPO)mEpos.get(claveEpo1); 									
					
					if (valLimEpo.getSobregiro() == true  ){
						enviarCorreo = true;
						mensajeCorreo.append("<tr><td align='center'>" + claveEpo + "</td><td>" + valLimEpo.getNombreEPO() + 
													"</td> <td align='center'> "+valLimEpo.getTotalDoctos() +
													"</td><td align='center'> $"+Comunes.formatoDecimal(valLimEpo.getMontoDoctos(),2) + 
													"</td><td align='center'> $"+Comunes.formatoDecimal(valLimEpo.getMontoSobregiro(),2) + 
													"</td><td align='center'> "+valLimEpo.getTipoMoneda() +													
													"</td></tr>");
					}
				}//while
				mensajeCorreo.append("</table>");
				mensajeCorreo.append("<br>");
				mensajeCorreo.append("<table border='0'><tr><td><b>Nota:</b></td></tr>");
				mensajeCorreo.append("<tr><td><b>1)</b> Este mensaje fu� enviado desde una cuenta de notificaciones que no puede aceptar correos electr�nicos de entrada. <br>Por favor no responda a este mensaje.</td></tr>");	
								
				if (enviarCorreo) {
					log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Envio Correo Sobregiro");
					String queryDestinatario =
							" SELECT cg_email_contingencia " +
							" FROM comcat_producto_nafin " +
							" WHERE ic_producto_nafin = 1 ";
					Registros reg = con.consultarDB(queryDestinatario);
					String destinatarioCC ="";
					if(reg.next()) {
						destinatarioCC = reg.getString("cg_email_contingencia");
					}
					//correo de Notificaci�n para el IF 2012
					String destinatarioIF ="";					
					String queryDestinatarioIF= " SELECT  NVL(pz.cg_valor, '') emailIF  "+
							"  FROM com_parametrizacion_if pz, comcat_parametro_if pe "+
							"  WHERE pz.cc_parametro_if = pe.cc_parametro_if  "+
							" AND pz.ic_if = "+iClaveIF+
							" AND pz.cc_parametro_if = 'CORREO_NOTIFICACION'";
					Registros regIF = con.consultarDB(queryDestinatarioIF);
					if(regIF.next()) {
						destinatarioIF = regIF.getString("emailIF");
					}
									
					if(destinatarioIF.toString().equals("") || destinatarioIF ==null) {
						destinatarioIF=destinatarioCC;
						mensajeCorreo.append("<tr><td style='color:#FF0000' ><b>2)</b> NO se encuentra parametrizado el correo definido en el par�metro 'Correo de Notificaci�n', por lo que este correo s�lo se esta enviando al Administrador.</td></tr>");	
					}
					
					mensajeCorreo.append("</table> </body></html>");				
												
					Correo correo = new Correo();
					correo.enviarTextoHTML("noreply@nafin.gob.mx",
					destinatarioIF, 
					"Error. Sobregiro de Limite por EPO",
					mensajeCorreo.toString(),
					destinatarioCC);
					log.debug("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: Correo Sobregiro enviado");
					
				}
			} catch(Throwable t) {
				log.error("getDoctosSelecPymeWSV2(Error)(" + claveIF + "_" + claveUsuario + ")::Error al enviar correo de sobregiro de limites por EPO", t);
			}

			DocumentoIFWSV2[] arrDocumentos = null;
			if (lDocumentos.size() > 0) {
				arrDocumentos = new DocumentoIFWSV2[lDocumentos.size()];
				lDocumentos.toArray(arrDocumentos);
			}
			log.info("getDoctosSelecPymeWSV2(" + claveIF + "_" + claveUsuario + "):: " + lDocumentos.size() + " Documentos a regresar");
			resultadoProceso.setDocumentos(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
			
			if (!"".equals(epoNoCumplenHorario.toString())  ) {
				resultadoProceso.setResumenEjecucion("Epos con horarios fuera de operaci�n: \n "+epoNoCumplenHorario.toString());
			}
			
		} catch( Throwable  t) { //en esta secci�n caen todos los errores inesperados
			updateOk = false;
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
			log.error("getDoctosSelecPymeWSV2(Error)", t);

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("getDoctosSelecPymeWSV2(Error)(" + claveIF + "_" + claveUsuario + ")::Hubo Errores Inesperados "+ erroresInesperados.toString());

					/* llamado del metodo para enviar correo de Errores Inesperados siempre y cuando hayan*/
			 try {

				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados, "S");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}

			return resultadoProceso;
		} finally {
		    log.info("AutorizacionDescuentoBean::getDoctosSelecPymeWSV2(S)("+claveIF+"_"+claveUsuario+")");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(updateOk);
				con.cierraConexionDB();
			}
		}
		return resultadoProceso;
	}


	/**
	 * Obtiene la informaci�n de los Documentos Seleccionados de la Pyme
	 * @param clavePYME Clave de la pyme (ic_pyme)
	 * @param claveEPO Clave de la epo (ic_epo)
	 * @param claveIF Clave del if (ic_if)
	 * @return Vector de Vectores con la informaci�n del documento seleccionado
	 * @throws com.netro.exception.NafinException
	 */
	public Vector getDoctosSelecPyme(String clavePYME, String claveEPO,
			String claveIF, String claveDoctoTodos)
		throws NafinException {
		log.info("getDoctosSelecPyme(E)");
		AccesoDB con  = new AccesoDB();
		StringBuilder qrySentencia = new StringBuilder();
		ResultSet rs = null;
		PreparedStatement ps = null;
		String condicion = "";
		String campos = "";
		Vector registros = new Vector();
		int numCamposAdicionales = 0;
		int numRegistros = 0;
		boolean hayFondeoPropio = false;
		String condicionDoc ="";

		try {
			con.conexionDB();

			String sFechaVencPyme = "";//BeanSeleccionDocumento.operaFechaVencPyme(claveEPO);

			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			qrySentencia = new StringBuilder();
			qrySentencia.append("ALTER SESSION SET NLS_TERRITORY = MEXICO");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);
			try {
				hayFondeoPropio = hayFondeoPropio(claveIF,claveEPO);
			} catch(Throwable t) {
				throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
			}
			if (!hayFondeoPropio) {
				qrySentencia = new StringBuilder();
				qrySentencia.append(" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha FROM dual");
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1, Integer.parseInt(claveEPO));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
				}
				rs.close();ps.close();
			}

			if ( !clavePYME.equals("TODOS") && !clavePYME.equals("NULL") ) {
				condicion = " and d.ic_pyme = ?";
			}

			numCamposAdicionales = getNumCamposAdicionales(claveEPO, con);
			for (int i=1; i <= numCamposAdicionales; i++) {
				campos += ", d.CG_CAMPO" + i;
			}

			if ( !claveDoctoTodos.equals("") ) {
				condicionDoc = " and d.ic_documento in( "+claveDoctoTodos+")";
			}
			
			qrySentencia = new StringBuilder();
			qrySentencia.append(
				" SELECT /*+  leading (d) use_nl(ds d p pe e ie i cd m tf)*/ " +
				"  d.ic_documento, DECODE(ds.CS_OPERA_FISO,'S',99999, p.in_numero_sirac) as in_numero_sirac, DECODE(ds.CS_OPERA_FISO,'S', ifs.cg_razon_social ,p.cg_razon_social) as nombrePyme"   +
				"  , d.ig_numero_docto, pe.cg_pyme_epo_interno, e.cg_razon_social as nombreEpo"   +
				"  , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"   +
				"  , TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') as df_fecha_venc"   +
				"  , d.ic_moneda, m.cd_nombre, d.fn_monto, DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_IMPORTE_INTERES_FONDEO,0) , ds.in_importe_interes) as in_importe_interes"   +
				"  , DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_IMPORTE_RECIBIR_FONDEO,0),ds.in_importe_recibir) as in_importe_recibir,DECODE(ds.CS_OPERA_FISO,'S',NVL(ds.IN_TASA_ACEPTADA_FONDEO,0) , ds.in_tasa_aceptada) as in_tasa_aceptada, d.fn_monto_dscto"   +
				"  , (D.df_fecha_venc"+sFechaVencPyme+" - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) plazo"   +
				"  , d.fn_porc_anticipo, d.fn_monto-d.fn_monto_dscto as recursoGarantia"   +
				"  , d.df_fecha_docto, cd.cd_descripcion, TO_CHAR(d.df_alta,'dd/mm/yyyy') as df_alta, DECODE(ds.CS_OPERA_FISO,'S',p.cg_razon_social,d.ct_referencia) as ct_referencia "   +
				"  , d.CS_DSCTO_ESPECIAL, d.ic_estatus_docto  " + campos +
				"  , DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS benef"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS banco_benef"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS sucursal_benef"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS cta_benef"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS porc_benef"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS importe_recibir"   +//FODEA 042 - 2009 ACF
				"  , DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme"   +//FODEA 042 - 2009 ACF
				"  , tf.cg_nombre as tipo_factoraje " +
				"  , rn.ic_nafin_electronico as nafin_elect_benef " + //FODEA 032 - 2010 REQ4 Rebos
				"  ,( CASE WHEN ds.cs_opera_fiso= 'S' then  p.cg_razon_social else '' END)  as referencia"+
				"  , 'AutorizacionDescuentoBean::getDoctosSelecPyme' "+
				"  , d.CG_NUM_CONTRATO as contrato, d.CS_VALIDACION_JUR  as validacionJR"+
				"  from com_docto_seleccionado ds, "   +
				" 	com_documento d, "   +
				" 	comcat_pyme p, "   +
				" 	comrel_pyme_epo pe,"   +
				" 	comcat_epo e,"   +
				" 	comrel_if_epo ie,"   +
				" 	comcat_if i,"   +
				" 	comcat_estatus_docto cd,"   +
				" 	comcat_moneda m,"   +
				" 	comcat_tipo_factoraje tf,"   +
				"   comrel_nafin rn, " + //FODEA 032 - 2010 REQ4 Rebos
				"	 comcat_if ifs " + //FODEA 017-2013
				
				"  where "   +
				"  ds.ic_documento=d.ic_documento"   +
				"  and ds.ic_if = ifs.ic_if"+
				"  and ds.ic_epo=d.ic_epo"   +
				"  and d.ic_pyme=p.ic_pyme"   +
				"  and d.ic_pyme=d.ic_pyme"   +
				"  and d.ic_moneda=m.ic_moneda"   +
				"  and d.ic_estatus_docto = cd.ic_estatus_docto"   +
				"  and d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"   +
				"  and ie.ic_if = i.ic_if(+)"   +
				"  and d.ic_epo = pe.ic_epo "   +
				"  and d.ic_pyme = pe.ic_pyme"   +
				"  and d.ic_epo = e.ic_epo"   +
				"  and ie.ic_if(+) = d.ic_beneficiario"   +
				"  and pe.cs_habilitado = 'S'"   +
				"  AND cs_dscto_especial !='C' "+
				"  and cd.ic_estatus_docto=3"   +
				"  and d.ic_estatus_docto=3"   +
				"  and ie.ic_epo(+)=? " +
				"  and pe.ic_epo=? " +
				"  and e.ic_epo=? " +
				"  and d.ic_epo=? " +
				"  and d.ic_if=? "+
				"  and ds.ic_epo=? " +
				" "+condicion+	//El estatus 3 es seleccionado Pyme
				"  and rn.ic_epo_pyme_if(+) = d.ic_beneficiario " + //FODEA 032 2010 - REQ4 Rebos
				"  and rn.cg_tipo(+) = 'I' " + //FODEA 032 - 2010 REQ4 Rebos
				" "+condicionDoc+
				"  and rownum between 1 and 350 "   +
				"  order by p.cg_razon_social");

			log.debug("getDoctosSelecPyme()::" + qrySentencia);
			log.debug("claveEPO:: " + claveEPO + ". claveIF:: " + claveIF );
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,Integer.parseInt(claveEPO));
			ps.setInt(2,Integer.parseInt(claveEPO));
			ps.setInt(3,Integer.parseInt(claveEPO));
			ps.setInt(4,Integer.parseInt(claveEPO));
			ps.setInt(5,Integer.parseInt(claveIF));
			ps.setInt(6,Integer.parseInt(claveEPO));
			if ( !clavePYME.equals("TODOS") && !clavePYME.equals("NULL") ) {
				ps.setInt(7,Integer.parseInt(clavePYME));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				Vector registro = new Vector(23 + numCamposAdicionales);
				numRegistros++;
			    
				String  tipofactoraje = (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL");
				String  contrato =  (rs.getString("contrato") == null)?"":rs.getString("contrato");
				String validacionJr =   (rs.getString("validacionJR") == null)?"":rs.getString("validacionJR");
				
				if (  ( "D".equals(tipofactoraje) && !"".equals(contrato)  &&   "S".equals(validacionJr) ) 
					||  ( "D".equals(tipofactoraje)  && "".equals(contrato)  )  ||  !"D".equals(tipofactoraje)  ){
	    
				    registro.add(0, (rs.getString("IC_DOCUMENTO") == null)?"":rs.getString("IC_DOCUMENTO") );
				    registro.add(1, (rs.getString("IN_NUMERO_SIRAC") == null)?"":rs.getString("IN_NUMERO_SIRAC"));
				    registro.add(2, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME"));
				    registro.add(3, (rs.getString("IG_NUMERO_DOCTO") == null)?"":rs.getString("IG_NUMERO_DOCTO"));
				    registro.add(4, (rs.getString("CG_PYME_EPO_INTERNO") == null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
				    registro.add(5, (rs.getString("NOMBREEPO") == null)?"":rs.getString("NOMBREEPO"));
				    registro.add(6, (rs.getString("DF_FECHA_DOCTO") == null)?"":rs.getString("DF_FECHA_DOCTO"));
				    registro.add(7, (rs.getString("DF_FECHA_VENC") == null)?"":rs.getString("DF_FECHA_VENC"));
				    registro.add(8, (rs.getString("IC_MONEDA") == null)?"":rs.getString("IC_MONEDA"));
				    registro.add(9, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE"));
				    registro.add(10, (rs.getString("FN_MONTO") == null)?"":rs.getString("FN_MONTO"));
				    registro.add(11, (rs.getString("IN_IMPORTE_INTERES") == null)?"":rs.getString("IN_IMPORTE_INTERES"));
				    registro.add(12, (rs.getString("IN_IMPORTE_RECIBIR") == null)?"":rs.getString("IN_IMPORTE_RECIBIR"));
				    registro.add(13, (rs.getString("IN_TASA_ACEPTADA") == null)?"":rs.getString("IN_TASA_ACEPTADA"));
				    registro.add(14, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO"));
				    registro.add(15, (rs.getString("PLAZO") == null)?"":rs.getString("PLAZO"));
				    registro.add(16, (rs.getString("FN_PORC_ANTICIPO") == null)?"":rs.getString("FN_PORC_ANTICIPO"));
				    registro.add(17, (rs.getString("RECURSOGARANTIA") == null)?"":rs.getString("RECURSOGARANTIA"));
				    registro.add(18, (rs.getString("CD_DESCRIPCION") == null)?"":rs.getString("CD_DESCRIPCION")); //Estatus del documento
				    registro.add(19, (rs.getString("DF_ALTA") == null)?"":rs.getString("DF_ALTA"));
				    registro.add(20, (rs.getString("CT_REFERENCIA") == null)?"":rs.getString("CT_REFERENCIA"));
				    registro.add(21, (rs.getString("CS_DSCTO_ESPECIAL") == null)?"":rs.getString("CS_DSCTO_ESPECIAL"));
				    registro.add(22, (rs.getString("IC_ESTATUS_DOCTO") == null)?"":rs.getString("IC_ESTATUS_DOCTO"));
    
				    registro.add(23,(rs.getString("BENEF") == null) ? "" : rs.getString("BENEF"));
				    registro.add(24,(rs.getString("BANCO_BENEF") == null) ? "" : rs.getString("BANCO_BENEF"));
				    registro.add(25,(rs.getString("SUCURSAL_BENEF") == null) ? "" : rs.getString("SUCURSAL_BENEF"));
				    registro.add(26,(rs.getString("CTA_BENEF") == null) ? "" : rs.getString("CTA_BENEF"));
				    registro.add(27,(rs.getString("PORC_BENEF") == null) ? "" : rs.getString("PORC_BENEF"));
				    registro.add(28,(rs.getString("IMPORTE_RECIBIR") == null) ? "" : rs.getString("IMPORTE_RECIBIR"));
				    registro.add(29,(rs.getString("NETO_REC_PYME") == null) ? "" : rs.getString("NETO_REC_PYME"));
				    registro.add(30,(rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE"));
				    registro.add(31,(rs.getString("NAFIN_ELECT_BENEF") == null) ? "" : rs.getString("NAFIN_ELECT_BENEF")); //FODEA 032 - 2010 REQ4 Rebos
				    registro.add(32,(rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA")); //FODEA 032 - 2010 REQ4 Rebos
				    for (int i = 1; i <= numCamposAdicionales; i++) {
					    registro.add(32 + i, (rs.getString("CG_CAMPO" + i) == null)?"":rs.getString("CG_CAMPO" + i));
				    }
				    registros.add(registro);
				
				}
				//registro.clear();
			}
			rs.close();
			if(ps!=null) ps.close();
//			con.cierraStatement();
			if (numRegistros > 0) {
				return registros;
			} else {
				throw new NafinException("GRAL0004");
			}
		} catch (NafinException ne) {
			log.error("getDoctosSelecPyme(Error): "+ ne.getMessage() );
			ne.printStackTrace();
			throw ne;
		} catch (Exception e) {
			log.error("getDoctosSelecPyme(Error): ", e );
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getDoctosSelecPyme(S) ");
		}
	}


	public String [] getEsquemaExterno(String esquemaExt)
			throws NafinException {

		String [] esquema = new String [16];
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		try
		{
			con.conexionDB();

			qrySentencia = "SELECT T.cd_nombre as tasaNombreUF"+
				" , E.ic_tasauf, INITCAP(M.cd_nombre) as monedaNombre" +
				" , E.ic_moneda, INITCAP(O.cd_descripcion) as oficinaEst" +
				" , E.ic_oficina, INITCAP(T.cd_descripcion) as tasaDescripcion"+
				" , E.ic_tipo_credito" +
				" , INITCAP(EA.cd_descripcion) as EsquemaAmort, E.ic_esquema_amort"+
				" , INITCAP(CD.cd_descripcion) as claseDocumento" +
				" , E.ic_clase_docto, INITCAP(TA.cd_descripcion) as tablaAmort"+
				" , E.ic_tabla_amort, T2.cd_nombre as TasaNombreIF, E.ic_tasaif" +
				" FROM COMREL_ESQUEMA_EXT E, COMCAT_TASA T, COMCAT_MONEDA M" +
				" ,COMCAT_OFICINA_ESTATAL O, COMCAT_TIPO_CREDITO T" +
				" ,COMCAT_ESQUEMA_AMORT EA, COMCAT_CLASE_DOCTO CD" +
				" ,COMCAT_TABLA_AMORT TA, COMCAT_TASA T2" +
				" WHERE E.ic_tasauf = T.ic_tasa AND E.ic_moneda = M.ic_moneda " +
				" AND E.ic_oficina = O.ic_oficina AND T.ic_tipo_credito = E.ic_tipo_credito" +
				" AND EA.ic_esquema_amort = E.ic_esquema_amort " +
				" AND CD.ic_clase_docto = E.ic_clase_docto " +
				" AND TA.ic_tabla_amort = E.ic_tabla_amort" +
				" AND E.ic_tasaif = T2.ic_tasa " +
				" AND ic_esquema_solic = " + esquemaExt;

			rs = con.queryDB(qrySentencia);
			if (rs.next())
			{
				esquema[0] = rs.getString("TASANOMBREUF");
				esquema[1] = rs.getString("IC_TASAUF");
				esquema[2] = rs.getString("MONEDANOMBRE");
				esquema[3] = rs.getString("IC_MONEDA");
				esquema[4] = rs.getString("OFICINAEST");
				esquema[5] = rs.getString("IC_OFICINA");
				esquema[6] = rs.getString("TASADESCRIPCION");
				esquema[7] = rs.getString("IC_TIPO_CREDITO");
				esquema[8] = rs.getString("ESQUEMAAMORT");
				esquema[9] = rs.getString("IC_ESQUEMA_AMORT");
				esquema[10] = rs.getString("CLASEDOCUMENTO");
				esquema[11] = rs.getString("IC_CLASE_DOCTO");
				esquema[12] = rs.getString("TABLAAMORT");
				esquema[13] = rs.getString("IC_TABLA_AMORT");
				esquema[14] = rs.getString("TASANOMBREIF");
				esquema[15] = rs.getString("IC_TASAIF");
			} else {
				// La clave de Producto no existe";
				throw new NafinException ("DSCT0005");
			}
			con.cierraStatement();
			return esquema;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException ("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public int getNumCamposAdicionales(String claveEPO)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		int numeroCamposAdicionales = 0;

		try {

			// El query para obtener los campos adicionales definidos por la epo en comrel_visor
			qrySentencia = "select count(*) "+
						"  from comrel_visor where ic_epo= " + claveEPO +
						"  and ic_producto_nafin = 1";

			con.conexionDB();
			rs = con.queryDB(qrySentencia);
			rs.next();
			numeroCamposAdicionales = rs.getInt(1);

			rs.close();
			con.cierraStatement();

			return numeroCamposAdicionales;

		} catch (Exception e) {
			log.error("Error en getNumCamposAdicionales: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public int getNumCamposAdicionales(String claveEPO, AccesoDB con) throws NafinException {
		String qrySentencia = null;
		ResultSet rs = null;
		int numeroCamposAdicionales = 0;

		try {

			// El query para obtener los campos adicionales definidos por la epo en comrel_visor
			qrySentencia = "select count(*) "+
						 "  from comrel_visor where ic_epo= " + claveEPO +
						 "  and ic_producto_nafin = 1";
			rs = con.queryDB(qrySentencia);
			rs.next();
			numeroCamposAdicionales = rs.getInt(1);

			rs.close();
			con.cierraStatement();

			return numeroCamposAdicionales;

		} catch (Exception e) {
			log.error("Error en getNumCamposAdicionales: ", e);
			throw new NafinException("SIST0001");
		}
	}

	private int getNumCamposAdicionalesC(String claveEPO, AccesoDB con) throws NafinException {
		String qrySentencia = null;
		ResultSet rs = null;
		int numeroCamposAdicionales = 0;

		try {
			// El query para obtener los campos adicionales definidos por la epo en comrel_visor
			qrySentencia = "select count(*) from comrel_visor "+
						 " where ic_epo= " + claveEPO +
						 " and ic_producto_nafin = 1";
			rs = con.queryDB(qrySentencia);
			rs.next();
			numeroCamposAdicionales = rs.getInt(1);

			rs.close();
			con.cierraStatement();

			return numeroCamposAdicionales;

		} catch (Exception e) {
			log.error("Error en getNumCamposAdicionales: ", e);
			throw new NafinException("SIST0001");
		}
	}

	public String getNumProcesoSolicitudExt() throws NafinException {
		String numProcesoSolicitud = null;

		AccesoDB con  = new AccesoDB();
		try {
			con.conexionDB();

			String qrySentencia = "select max(ic_proc_solicitud) as ic_proc from comtmp_solicitud";
			ResultSet rs = con.queryDB(qrySentencia);
			numProcesoSolicitud = (rs.next())?String.valueOf(rs.getInt("IC_PROC")+1):"1";
			con.cierraStatement();
			return numProcesoSolicitud;
		} catch (Exception e) {
			log.error("Error en getNumProcesoSolicitudExt: ", e );
			throw new NafinException("SIST0001");
		} finally {
			con.cierraConexionDB();
		}
	}

	public String getNumProcesoSolicitudesExt() throws NafinException {
		String numProcesoSolicitudes = null;

		AccesoDB con  = new AccesoDB();
		try {
			con.conexionDB();

			String qrySentencia = "SELECT max(IG_NUMERO_PROCESO) as IC_PROC FROM COMTMP_SOLICITUD";
			ResultSet rs = con.queryDB(qrySentencia);
			numProcesoSolicitudes = (rs.next())?String.valueOf(rs.getInt("IC_PROC")+1):"1";
			con.cierraStatement();
			return numProcesoSolicitudes;
		} catch (Exception e) {
			log.error("Error en getNumProcesoSolicitudesExt: ", e);
			throw new NafinException("SIST0001");
		} finally {
			log.debug("\n\nCerrando conexion de getNumProcesoSolicitudesExt");
			con.cierraConexionDB();
		}
	}


	public Vector getSolicExtProcesadas (String acuse, String claveIF)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		Vector registros = new Vector();

		try {
			con.conexionDB();

			qrySentencia="select de.fn_monto_docto, de.fn_monto_dscto, de.fn_tnuf"+
				" , to_char(S.df_v_descuento, 'dd/mm/yyyy') as df_v_descuento"+
				" , p.cg_razon_social as nombrePyme, s.in_numero_amort, s.ic_folio"+
				" , m.cd_nombre"+
				" from com_docto_ext de, comcat_pyme p, com_solicitud s, comcat_moneda m"+
				" where s.ic_folio = de.ic_folio"+
				" AND de.ic_moneda = m.ic_moneda"+
				" and de.ic_pyme=p.ic_pyme"+
				" and de.ic_if="+claveIF+
				" and s.cc_acuse = '"+acuse+"'";

			rs = con.queryDB(qrySentencia);

			while (rs.next()) {

				Vector registro = new Vector(8);
				registro.add(0, (rs.getString("NOMBREPYME") == null)?"":rs.getString("NOMBREPYME") );
				registro.add(1, (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") );
				registro.add(2, (rs.getString("FN_MONTO_DOCTO") == null)?"":rs.getString("FN_MONTO_DOCTO") );
				registro.add(3, (rs.getString("FN_MONTO_DSCTO") == null)?"":rs.getString("FN_MONTO_DSCTO") );
				registro.add(4, (rs.getString("IN_NUMERO_AMORT") == null)?"":rs.getString("IN_NUMERO_AMORT") );
				registro.add(5, (rs.getString("FN_TNUF") == null)?"":rs.getString("FN_TNUF") );
				registro.add(6, (rs.getString("DF_V_DESCUENTO") == null)?"":rs.getString("DF_V_DESCUENTO") );
				registro.add(7, (rs.getString("CD_NOMBRE") == null)?"":rs.getString("CD_NOMBRE") ); //Moneda nombre

				registros.add(registro);
			}

			rs.close();
			con.cierraStatement();

			return registros;

		} catch (Exception e) {
			log.error("Error en getSolicExtProcesadas: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public String[] getTotalesDoctosProcesar (String claveDocto[], String claveEPO)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;

		String clavesDocumento = "";

		String [] montos = new String[6];

		try {

			if (claveDocto != null) {
				for(int i = 0; i < claveDocto.length; i++) {
					if (i == 0) {
						clavesDocumento = claveDocto[i];
					} else {
						clavesDocumento = clavesDocumento + "," + claveDocto[i];
					}
				}
			} else {
				throw new NafinException("DSCT0017");
			}

			con.conexionDB();

			qrySentencia="select sum(d.fn_monto_dscto) as montoMN"+
				" , sum(DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes)) as montoInteresMN"+
				" , sum(DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir)) as montoRecibirMN"+
				" from com_documento d, com_docto_seleccionado ds"+
				" where"+
				" d.ic_documento=ds.ic_documento"+
				" and d.ic_epo="+claveEPO+
				" and d.ic_estatus_docto=3"+
				" and ds.ic_documento in ("+clavesDocumento+")"+
				" and d.ic_moneda=1";

			rs = con.queryDB(qrySentencia);
			rs.next();

			montos[0] = (rs.getString("MONTOMN") != null)?rs.getString("MONTOMN"):"0.00";
			montos[1] = (rs.getString("MONTOINTERESMN") != null)?rs.getString("MONTOINTERESMN"):"0.00";
			montos[2] = (rs.getString("MONTORECIBIRMN") != null)?rs.getString("MONTORECIBIRMN"):"0.00";

			rs.close();
			con.cierraStatement();

			qrySentencia="select sum(d.fn_monto_dscto) as montoDL"+
				" , sum(DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes)) as montoInteresDL"+
				" , sum(DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir)) as montoRecibirDL"+
				" from com_documento d, com_docto_seleccionado ds"+
				" where"+
				" d.ic_documento=ds.ic_documento"+
				" and d.ic_epo="+claveEPO+
				" and d.ic_estatus_docto=3"+
				" and ds.ic_documento in ("+clavesDocumento+")"+
				" and d.ic_moneda=54";

			rs = con.queryDB(qrySentencia);
			rs.next();

			montos[3] = (rs.getString("MONTODL") != null)?rs.getString("MONTODL"):"0.00";
			montos[4] = (rs.getString("MONTOINTERESDL") != null)?rs.getString("MONTOINTERESDL"):"0.00";
			montos[5] = (rs.getString("MONTORECIBIRDL") != null)?rs.getString("MONTORECIBIRDL"):"0.00";

			rs.close();
			con.cierraStatement();

			return montos;

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.error("Error en getTotalesDoctosProcesar: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public boolean hayClaseDoctoAsociado (String claveEpo)
		throws NafinException {

		boolean existe = false;

		ResultSet rs = null;
		String qrySentencia = "";

		AccesoDB con  = new AccesoDB();
		try
		{
			con.conexionDB();

			qrySentencia="select count(*)"+
				" from comrel_producto_docto pd"+
				" where pd.ic_epo=" + claveEpo +
				" and pd.ic_producto_nafin=1";
				
		log.debug("qrySentencia === "+qrySentencia);
			rs = con.queryDB(qrySentencia);
			rs.next();

			existe = (rs.getInt(1) > 0)?true:false;	//Existe una clase de documento asociado para realizar Descuento?

			rs.close();
			con.cierraStatement();

			return existe;

		} catch (Exception e) {
			log.error("Error hayClaseDoctoAsociado: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	public boolean hayClaseDoctoAsociado (String claveEpo, AccesoDB con) throws NafinException {
		boolean existe = false;
		ResultSet rs = null;
		String qrySentencia = "";

		try	{
			qrySentencia="select count(*)"+
				" from comrel_producto_docto pd"+
				" where pd.ic_epo=" + claveEpo +
				" and pd.ic_producto_nafin=1";

			rs = con.queryDB(qrySentencia);
			rs.next();

			existe = (rs.getInt(1) > 0)?true:false;	//Existe una clase de documento asociado para realizar Descuento?

			rs.close();
			con.cierraStatement();

			return existe;

		} catch (Exception e) {
			log.error("Error hayClaseDoctoAsociado: ", e);
			throw new NafinException("SIST0001");
		}
	}

	/**
	 * Determina si existen documentos aplicados a credito.
	 * @param claveIF Clave del Intermediario Finaciero
	 * @return true Si existen documentos o false de lo contrario
	 *
	 */
	public boolean hayDoctosAplicCred(String claveIF)
			throws NafinException {
		log.info("hayDoctosAplicCred(E)");

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
                boolean ok = true;

		try {
			con.conexionDB();
			qrySentencia=
				"  SELECT /*+ index(d IN_COM_DOCUMENTO_04_NUK) */"   +
				"         COUNT(1)"   +
				"  from com_documento d"   +
				"  where   EXISTS (select 1"   +
				"  		   from COM_DOCTO_SELECCIONADO ds"   +
				"  		  where D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"                  AND DS.IC_IF=  ? " +
				"                  AND DS.DF_FECHA_SELECCION >= TRUNC(SYSDATE)"   +
				"                  AND DS.DF_FECHA_SELECCION < TRUNC(SYSDATE) + 1"   +
				"                )"   +
				"    AND D.IC_ESTATUS_DOCTO=?"  ;

			PreparedStatement pstmt = con.queryPrecompilado(qrySentencia);
			pstmt.setInt(1, Integer.parseInt(claveIF) );
			pstmt.setInt(2, 16); //Aplicado a credito
			rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt(1) > 0) {
				ok = true;
			} else {
				ok = false;
			}
			rs.close();
			if(pstmt != null) pstmt.close();

		} catch (Exception e) {
			log.error("hayDoctosAplicCred(Error): ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.error("hayDoctosAplicCred(S)");
		}
		return ok;
	}


	public void setAmortTemp (String numeroProcesoSolicitudExt, String numeroAmort,
															String importe, String fecha)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		try {
			con.conexionDB();
			qrySentencia = "insert into comtmp_amortizacion (ic_amortizacion"+
				", ic_proc_solicitud, in_numero, fn_importe,df_fecha)"+
				" values (seq_comtmp_amort_ic_amort.NEXTVAL"+
				" , "+numeroProcesoSolicitudExt+", "+numeroAmort+
				" , "+importe+", TO_DATE('"+fecha+"','dd/mm/yyyy'))";

			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public void setSolicExtTemp (String numeroProcesoSolicitudExt, String esquemaSolicitud,
																String claveIF, String clavePyme,
																String numeroProveedor, String numeroDocumento,
																String numeroFactura, String montoDocumento,
																String montoDescuento, String emisor,
																String plazo, String tipoPlazo,
																String periodoPagoCapital, String periodoPagoInteres,
																String descripcionBienes, String domicilioPago,
																String relacionMatUF, String sobretasaUF,
																String tasaNetaUF, String relMatIF,
																String sobretasaIF, String numeroAmortizaciones,
																String fechaPrimerPagoCapital, String fechaPrimerPagoInteres,
																String fechaVencDocto, String fechaVencDscto,
																String diaPago, String fechaEmisionTitulo,
																String numeroContrato, String lugarFirma)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;

		try
		{
			String [] esquema;

			con.conexionDB();

			esquema = getEsquemaExterno(esquemaSolicitud);

			// Si los valores son numericos, se establecen en nulo cuando vienen vacios
			if (numeroProveedor.equals("")) {
				numeroProveedor = "NULL";
			}
			if (numeroFactura.equals("")) {
				numeroFactura = "NULL";
			}
			if (numeroContrato.equals("")) {
				numeroContrato = "NULL";
			}
			if (clavePyme.equals("")) {
				clavePyme = "NULL";
			}


			qrySentencia = "insert into comtmp_solicitud (ic_proc_solicitud"+
				" , ic_esquema_amort, ic_tipo_credito, ic_oficina"+
				" , cg_emisor, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap"+
				" , cg_perio_pago_int, cg_desc_bienes, ic_clase_docto, cg_domicilio_pago"+
				" , ic_tasaif, cg_rmif, in_stif, ic_tabla_amort, in_numero_amort"+
				" , df_ppc, df_ppi, df_v_documento, df_v_descuento, in_dia_pago"+
				" , df_etc, cg_lugar_firma, ig_numero_contrato, df_fecha_solicitud"+
				" , ic_if, ic_pyme, ic_tasa, cg_rmuf, fn_stuf, fn_tnuf, ic_moneda"+
				" , ig_numero_docto, ig_numero_prov, ig_numero_factura, fn_monto_docto"+
				" , fn_monto_dscto, df_docto)"+
				" values ("+numeroProcesoSolicitudExt+
				" , "+esquema[CLAVE_ESQUEMA_AMORT]+","+esquema[CLAVE_TIPO_CREDITO]+
				" , "+esquema[CLAVE_OFICINA]+
				" , '"+emisor+"', "+plazo+",'"+tipoPlazo+"'"+
				" , '"+periodoPagoCapital+"'"+
				" , '"+periodoPagoInteres+"', '"+descripcionBienes+"'"+
				" , "+esquema[CLAVE_CLASE_DOCTO]+
				" , '"+domicilioPago+"' , "+esquema[CLAVE_TASA_IF]+", '"+relMatIF+"'"+
				" , "+sobretasaIF+
				" , "+esquema[CLAVE_TABLA_AMORT]+", "+numeroAmortizaciones+
				"	, TO_DATE('"+fechaPrimerPagoCapital+"','dd/mm/yyyy'), TO_DATE('"+fechaPrimerPagoInteres+"','dd/mm/yyyy')"+
				" , TO_DATE('"+fechaVencDocto+"','dd/mm/yyyy')"+
				" , TO_DATE('"+fechaVencDscto+"','dd/mm/yyyy')"+
				" , "+diaPago+", TO_DATE('"+fechaEmisionTitulo+"','dd/mm/yyyy')"+
				" , '"+lugarFirma+"'"+
				"	, "+numeroContrato+", TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')"+
				"	, "+claveIF+", "+clavePyme+", "+esquema[CLAVE_TASA_USUARIO_FINAL]+
				" , '"+relacionMatUF+"', "+sobretasaUF+", "+tasaNetaUF+
				"	, "+esquema[CLAVE_MONEDA]+", "+numeroDocumento+", "+numeroProveedor+
				" , "+numeroFactura+
				"	, "+montoDocumento+", "+montoDescuento+
				", TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy'))";

			//out.println(qrySentencia);
			con.ejecutaSQL(qrySentencia);
			con.terminaTransaccion(true);
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	public Hashtable setSolicitudesExtTemp(String claveIF, String solicitudes)
		throws NafinException {

		AccesoDB con  = new AccesoDB();
		boolean ok = true;
		try {
			con.conexionDB();

			Hashtable hResultados = new Hashtable();
			String igNumeroProceso = "";

			int txttotdoc = 0;
			int txttotdocdol = 0;
			double txtmtodoc = 0;
			double txtmtodocdol = 0;
			double totalmn = 0;
			double totaldol = 0;

			StringBuffer error = new StringBuffer();

			String linea = "";
			int numLinea = 0;
			int indice = 0;
			String msgerr = "";
			String cadenaAmort = "";
			Vector vecdat = null;

			double sumaAmort = 0;

			//Declaraci�n de variables del primer bloque del layout
			String claveSirac = "";

			//Declaraci�n de variables del segundo bloque del layout
			String claveProducto = "";
			String numProveedor = "";
			String numeroDocto = "";
			String numeroFactura = "";
			String importeDocto = "";
			String importeDscto = "";
			String nombreEmisor = "";
			String plazo = "";
			String tipoPlazo = "";
			String ppc = "";
			String ppi = "";
			String descBienes = "";
			String domicilioPago = "";
			String rmuf = "";
			String stuf = "";
			String tnuf = "";
			String rmif = "";
			String stif = "";
			String numeroAmort = "";
			String fechaPPC = "";
			String fechaPPI = "";
			String fechaVDocto = "";
			String fechaVDscto = "";
			String diaPago = "";
			String fechaETC = "";
			String lugarFirma = "";
			String numeroContrato = "";

			String icEsquemaSolic = "";
			String icTasauf = "";
			String icMoneda = "";
			String icTablaAmort = "";
			String icClaseDocto = "";
			String icEsquemaAmort = "";
			String icTipoCredito = "";
			String icOficina = "";
			String icTasaif = "";
			String icPyme = "NULL";
			String icProcSolicitud = "";


			StringTokenizer solicitud = new StringTokenizer(solicitudes,"\n");
			igNumeroProceso = getNumProcesoSolicitudesExt();
			while(solicitud.hasMoreElements()) {
				linea = solicitud.nextToken();

				VectorTokenizer valores = new VectorTokenizer(linea,"|");
				vecdat = valores.getValuesVector();
				Vector vecAmort = new Vector();

				indice = 1;
				sumaAmort = 0;
				msgerr = "Error en la l&iacute;nea: "+ String.valueOf(numLinea + 1) + ", ";

				claveSirac = Comunes.checaVacio(vecdat.get(0).toString().trim());
				//nombre = "'" + vecdat.get(1).toString().trim() + "'";
				//tipoPersona = "'" + vecdat.get(2).toString().trim() + "'";
				//rfc = "'" + vecdat.get(3).toString().trim() + "'";
				//domicilio = "'" + vecdat.get(4).toString().trim() + "'";
				//colonia = "'" + vecdat.get(5).toString().trim() + "'";
				//cp = Comunes.checaVacio(vecdat.get(6).toString().trim());
				//estado = "'" + vecdat.get(8).toString().trim() + "'";
				//municipio = "'" + vecdat.get(9).toString().trim() + "'";
				//telefono = "'" + vecdat.get(10).toString().trim() + "'";
				//email = "'" + vecdat.get(11).toString().trim() + "'";
				//fechaConst = vecdat.get(12).toString().trim();

				if (claveSirac.equals("NULL")) {
					ok = false;
					error.append(msgerr + " el n&uacute;mero Sirac debe ser especificado\n\n");
				}
/*
				if (!"".equals(fechaConst)) {
					if (Comunes.checaFecha(fechaConst)) {
						fechaConst = "TO_DATE('" + fechaConst + "','DD/MM/YYYY')";
					} else {
						ok = false;
						error.append(msgerr + " la fecha de constituci&oacute;n (" + fechaConst + ") del cliente no es valida.\n\n");
					}
				} else {
					fechaConst = "NULL";
				}
*/
				//personalOcup = Comunes.checaVacio(vecdat.get(13).toString().trim());
				//empleosGen = Comunes.checaVacio(vecdat.get(14).toString().trim());
				//estrato = "'" + vecdat.get(16).toString().trim() + "'";
				//ventasTot = Comunes.checaVacio(vecdat.get(17).toString().trim());
				//ventasExp = Comunes.checaVacio(vecdat.get(18).toString().trim());
				//activoTot = Comunes.checaVacio(vecdat.get(19).toString().trim());
				//capitalCont = Comunes.checaVacio(vecdat.get(20).toString().trim());
				//actividad = "'" + vecdat.get(22).toString().trim() + "'";
				//productos = "'" + vecdat.get(23).toString().trim() + "'";

				//Asignaci�n de variables del segundo bloque del layout
				claveProducto = Comunes.checaVacio(vecdat.get(24).toString().trim());
				numProveedor = "'" + vecdat.get(25).toString().trim() + "'";
				numeroDocto = Comunes.checaVacio(vecdat.get(30).toString().trim());
				numeroFactura = Comunes.checaVacio(vecdat.get(31).toString().trim());
				importeDocto = vecdat.get(32).toString().trim();
				if (! Comunes.esDecimal(importeDocto)) {
					ok = false;
					error.append(msgerr + " el importe del documento proporcionado, no es un n&uacute;mero v&aacute;lido.\n\n");
				}
				importeDscto = vecdat.get(33).toString().trim();
				if (! Comunes.esDecimal(importeDscto)) {
					ok = false;
					error.append(msgerr + " el importe del descuento proporcionado: " + importeDscto + ", no es un n&uacute;mero v&aacute;lido.\n\n");
				}

				nombreEmisor = "'" + vecdat.get(34).toString().trim() + "'";
				plazo = Comunes.checaVacio(vecdat.get(39).toString().trim());
				if (! Comunes.esNumero(plazo)) {
					ok = false;
					error.append(msgerr + " el plazo proporcionado, no es un n&uacute;mero v&aacute;lido.\n\n");
				}
				tipoPlazo = "'" + vecdat.get(40).toString().trim() + "'";
				ppc = "'" + vecdat.get(41).toString().trim() + "'";
				ppi = "'" + vecdat.get(42).toString().trim() + "'";
				descBienes = "'" + vecdat.get(43).toString().trim() + "'";
				domicilioPago = "'" + vecdat.get(46).toString().trim() + "'";
				rmuf = "'" + vecdat.get(49).toString().trim() + "'";
				stuf = Comunes.checaVacio(vecdat.get(50).toString().trim());
				tnuf = Comunes.checaVacio(vecdat.get(51).toString().trim());
				rmif = "'" + vecdat.get(54).toString().trim() + "'";
				stif = Comunes.checaVacio(vecdat.get(55).toString().trim());
				numeroAmort = Comunes.checaVacio(vecdat.get(58).toString().trim());
				fechaPPC = vecdat.get(59).toString().trim();
				if (Comunes.checaFecha(fechaPPC)) {
					fechaPPC = "TO_DATE('"	+ fechaPPC + "','DD/MM/YYYY')";
				} else {
					ok = false;
					error.append(msgerr + " la fecha del Primer Pago de Capital (" + fechaPPC + ") no es v&aacute;lida.\n\n");
				}
				fechaPPI = vecdat.get(60).toString().trim();
				if (Comunes.checaFecha(fechaPPI)) {
					fechaPPI = "TO_DATE('"	+ fechaPPI + "','DD/MM/YYYY')";
				} else {
					ok = false;
					error.append(msgerr + " la fecha del Primer Pago de Intereses (" + fechaPPI + ") no es v&aacute;lida.\n\n");
				}
				fechaVDocto = vecdat.get(61).toString().trim();
				if (Comunes.checaFecha(fechaVDocto)) {
					fechaVDocto = "TO_DATE('"	+ fechaVDocto + "','DD/MM/YYYY')";
				} else {
					ok = false;
					error.append(msgerr + " la fecha del Vencimiento del Documento (" + fechaPPC + ") no es v&aacute;lida.\n\n");
				}
				fechaVDscto = vecdat.get(62).toString().trim();
				if (Comunes.checaFecha(fechaVDscto)) {
					fechaVDscto = "TO_DATE('"	+ fechaVDscto + "','DD/MM/YYYY')";
				} else {
					ok = false;
					error.append(msgerr +" la fecha del Vencimiento del Descuento (" + fechaVDscto + ") no es v&aacute;lida.\n\n");
				}
				diaPago = vecdat.get(63).toString().trim();
				fechaETC = vecdat.get(64).toString().trim();
				if (Comunes.checaFecha(fechaETC)) {
					fechaETC = "TO_DATE('"	+ fechaETC + "','DD/MM/YYYY')";
				} else {
					ok = false;
					error.append(msgerr +" la fecha de Emisi&oacute;n del Documento (" + fechaETC + ") no es v&aacute;lida.\n\n");
				}
				lugarFirma = "'" + vecdat.get(65).toString().trim() + "'";
				numeroContrato = vecdat.get(66).toString().trim();
				for (int i = 67; i < vecdat.size(); i++) {
					if (indice == 1) {
						cadenaAmort = vecdat.get(i).toString().trim();
					}
					if (indice == 2) {
						String importeAmortizacion = vecdat.get(i).toString().trim();
						if (Comunes.esDecimal(importeAmortizacion)) {
							sumaAmort = sumaAmort + Double.parseDouble(importeAmortizacion);
							if (sumaAmort > Double.parseDouble(importeDscto)) {
								ok = false;
								error.append(msgerr + "la suma de las amortizaciones sobrepasa el importe del descuento.\n\n");
							} else {
								cadenaAmort = cadenaAmort + "," + importeAmortizacion;
							}
						} else {
							ok = false;
							error.append(msgerr + "el importe de las amortizaciones: " + importeAmortizacion + ", no es un n&uacute;mero v&aacute;lido.\n\n");
						}
					}
					if (indice == 3) {
						String fechaAmort = vecdat.get(i).toString().trim();
						if (Comunes.checaFecha(fechaAmort)) {
							cadenaAmort = cadenaAmort + ", TO_DATE('"	+ fechaAmort + "','DD/MM/YYYY')";
						} else {
							ok = false;
							error.append(msgerr +" las fechas de amortizaciones proporcionadas no son v&aacute;lidas.\n\n");
						}
					}
					indice ++;
					if (indice > 3) {
						vecAmort.add(cadenaAmort);
						indice = 1;
						cadenaAmort = "";
					}
				}

				if (sumaAmort == Double.parseDouble(importeDscto)) {
					try {
						String esquema[] =  getEsquemaExterno(claveProducto);
						icEsquemaSolic = claveProducto;
						icTasauf = esquema[1];
						icMoneda = esquema[3];
						icOficina = esquema[5];
						icTipoCredito = esquema[7];
						icEsquemaAmort = esquema[9];
						icClaseDocto = esquema[11];
						icTablaAmort = esquema[13];
						icTasaif = esquema[15];
						//Calculamos los importes totales dependiendo de la moneda.
						switch (Integer.parseInt(icMoneda)) {
							case 1:
								txttotdoc ++;
								txtmtodoc = txtmtodoc + Double.parseDouble(importeDscto);
								totalmn = totalmn + Double.parseDouble(importeDocto);
								break;
							case 54:
								txttotdocdol ++;
								txtmtodocdol = txtmtodocdol + Double.parseDouble(importeDscto);
								totaldol = totaldol + Double.parseDouble(importeDocto);
								break;
						}
					} catch (NafinException ne) {
						ok = false;
						error.append(msgerr +" no es posible procesar el archivo debido a que la Clave del producto: " +
								claveProducto + ", no existe en la base de datos.\n\n");
					}

					//Verificamos si la clave que enviaron como clave_SIRAC exista en la BD
					try {
						String [] cliente = getCliente(claveSirac);
						icPyme = cliente[0];
					} catch (NafinException ne) {
						ok = false;
						error.append(msgerr +" no es posible procesar el archivo debido a que la Clave de Sirac: " +
								claveSirac + ", se encuentra deshabilitado para el IF o no existe.\n\n");
					}

					if (ok) {
						//Obtiene el maximo de ic_proc_solicitud.
						String qrySentencia = "select max(ic_proc_solicitud) as ic_proc from comtmp_solicitud";
						ResultSet rs = con.queryDB(qrySentencia);
						icProcSolicitud = (rs.next())?String.valueOf(rs.getInt("IC_PROC")+1):"1";
						con.cierraStatement();
						// Aqui empieza la inserci�n de la solicitud y amortizaciones
						// Realizamos la inseci�n de la solicitud, si fallara rechazamos toda la transacci�n
						String insertSolic = "INSERT INTO COMTMP_SOLICITUD(IC_PROC_SOLICITUD, IG_NUMERO_PROCESO, IC_ESQUEMA_AMORT, IC_TIPO_CREDITO, " +
							"IC_OFICINA, IC_MONEDA, IC_CLASE_DOCTO, IC_TASA, IC_TASAIF, IC_TABLA_AMORT, IC_IF, IC_PYME, " +
							"IG_NUMERO_PROV, IG_NUMERO_DOCTO, IG_NUMERO_FACTURA, FN_MONTO_DOCTO, FN_MONTO_DSCTO, " +
							"CG_EMISOR, IG_PLAZO, CG_TIPO_PLAZO, CG_PERIO_PAGO_CAP, CG_PERIO_PAGO_INT, CG_DESC_BIENES, " +
							"CG_DOMICILIO_PAGO, CG_RMUF, FN_STUF, FN_TNUF, CG_RMIF, IN_STIF, IN_NUMERO_AMORT, DF_PPC, " +
							"DF_PPI, DF_V_DOCUMENTO, DF_V_DESCUENTO, IN_DIA_PAGO, DF_ETC, CG_LUGAR_FIRMA, " +
							"IG_NUMERO_CONTRATO, DF_FECHA_SOLICITUD) " +
							"values(" + icProcSolicitud + ", " + igNumeroProceso + ", " + icEsquemaAmort + ", " + icTipoCredito + ", " +
							icOficina + ", " + icMoneda + ", " + icClaseDocto + ", " + icTasauf +", " + icTasaif + ", " + icTablaAmort +", " +
							claveIF + ", " + icPyme + ", " + numProveedor + ", " + numeroDocto + ", " + numeroFactura + ", " + importeDocto + ", " + importeDscto + ", " +
							nombreEmisor + ", " + plazo + ", " + tipoPlazo + ", " + ppc + ", " + ppi + ", " + descBienes + ", " +
							domicilioPago + ", " + rmuf + ", " + stuf + ", " + tnuf + ", " + rmif + ", " + stif + ", " + numeroAmort + ", " + fechaPPC + ", " +
							fechaPPI + ", " + fechaVDocto + ", " + fechaVDscto + ", " + diaPago + ", " + fechaETC + ", " + lugarFirma + ", " + numeroContrato +", SYSDATE)";
						//out.println(insertSolic);
						con.ejecutaSQL(insertSolic);
						for (int j = 0; j < vecAmort.size(); j++) {
							String insAmort = "INSERT INTO COMTMP_AMORTIZACION(IC_AMORTIZACION, IC_PROC_SOLICITUD, " +
							"IN_NUMERO, FN_IMPORTE, DF_FECHA) " +
							"VALUES(seq_comtmp_amort_ic_amort.NEXTVAL, " + icProcSolicitud + ", " + vecAmort.get(j).toString().trim() + ")";
							//out.println(insAmort+"<br>");
							con.ejecutaSQL(insAmort);
						}
					}
				} else {
					ok = false;
					error.append(msgerr +" el importe de las amortizaciones no coincide con el importe del descuento.\n\n");
				}
				numLinea ++;
				// Vaciar los vectores
				vecdat.removeAllElements();
				vecAmort.removeAllElements();
			}//While linea
			hResultados.put( "ok", new Boolean(ok) );
			hResultados.put( "numProceso", igNumeroProceso);
			hResultados.put( "mensajeError", error.toString() );
			hResultados.put( "numRegitrosMN", new Integer(txttotdoc) );
			hResultados.put( "numRegistrosDL", new Integer(txttotdocdol) );
			hResultados.put( "montoTotalDsctoMN", new Double(txtmtodoc) );
			hResultados.put( "montoTotalDsctoDL", new Double(txtmtodocdol) );
			hResultados.put( "montoTotalMN", new Double(totalmn) );
			hResultados.put( "montoTotalDL", new Double(totaldol) );
			return hResultados;

		} catch(NafinException ne) {
			ok = false;
			throw ne;
		} catch (Exception e) {
			log.error( "Error en generarSolicitudesExterna: ", e );
			ok = false;
			throw new NafinException("SIST0001");
		} finally {
			log.info("\n\nCerrando conexion de generarSolicitudesExterna");
			if (con.hayConexionAbierta()) {
				if (ok) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
				con.cierraConexionDB();
			}
		}
	}



	public void verificarHorario()
			throws NafinException {
		try {
			Horario.validarHorario(PRODUCTO_DSCTO, "IF");
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		}
	}


/*****************************************************************************
                                   M�todos Privados
*****************************************************************************/
	private String getFolioSolicitud(AccesoDB con) throws SQLException {
		String folio = null;

		String qrySentencia="select max(TO_NUMBER(substr(ic_folio,1,10))) as consecutivo"+
			" from com_solicitud";

		ResultSet rs = con.queryDB(qrySentencia);
		rs.next();
		int consecutivo = rs.getInt("CONSECUTIVO") + 1;
		rs.close();
		con.cierraStatement();

		folio = Comunes.calculaFolio(consecutivo,10);

		return folio;
	}


	/* Agregado por MPCS 15/10/2003 FODA 095
	   Modificado EGB 18/10/2005
	   Metodo Publico utilizado por el AutorizacionDescuentoEJB y la clase Automatico
	*/
	public String getFolioSolicitud(String noCliente, AccesoDB con) throws SQLException {
		String folio2 = null;

		folio2 = Comunes.calculaFolio(getSecuencia("1", noCliente, con), 10);
		log.debug("AutorizacionDescuentoBean.getSecuencia(secuencia) "  + folio2);

		return folio2;
	}

	/* Agregado por EGB 18/10/2005
	*/
	private String getSecuencia(String icProductoNafin, String noCliente, AccesoDB con) throws SQLException {
		PreparedStatement ps = null;
	    String dia_operacion="";
	    String dia_operacion_ad="";
		int consecutivo = 0;
		String secuencia = null;
		log.info("getSecuencia(E)");
		try {
		    String qrySentencia =
                "SELECT TO_CHAR (SYSDATE, 'yy') DIAOP, TO_CHAR (SYSDATE, 'y') DIAOPAD FROM dual";
		    ps = con.queryPrecompilado(qrySentencia);
		    ResultSet rs = ps.executeQuery();
		    if (rs.next()) {
                dia_operacion = rs.getString("DIAOP");
                dia_operacion_ad = rs.getString("DIAOPAD");
            }
            rs.close();
		    ps.clearParameters();
		    if(ps!=null) ps.close();
            
			qrySentencia =
				"SELECT LPAD (   SUBSTR (LPAD (NVL (?, '0'), 3, '0'), 1, 3) " +
				"             || ? " +
				"             || LPAD (ig_folio, 6, '0') " +
				"             +  1, " +
				"             10, " +
				"             '0' " +
				"            ) AS secuencia, " +
				"       ig_folio + 1 AS consecutivo " +
				"  FROM com_folio_x_if " +
				" WHERE ic_producto_nafin = ? " +
				"   AND ic_if = ? " +
				"   AND cg_anio = ? ";
			log.debug("\n getSecuencia(query)\n"+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, noCliente);
		    ps.setInt(2, Integer.parseInt(dia_operacion_ad));
			ps.setInt(3, Integer.parseInt(icProductoNafin));
			ps.setInt(4, Integer.parseInt(noCliente));
		    ps.setInt(5, Integer.parseInt(dia_operacion));
            
			rs = ps.executeQuery();
			if (rs.next()) {
				consecutivo = rs.getInt("CONSECUTIVO");
				secuencia   = rs.getString("SECUENCIA");
				qrySentencia =
					"UPDATE com_folio_x_if " +
					"   SET ig_folio = ? " +
					" WHERE ic_producto_nafin = ? " +
					"   AND ic_if = ? " +
					"   AND cg_anio = ? ";
			} else {
				consecutivo = 1;
				String anio = new SimpleDateFormat("y").format(new java.util.Date());
				secuencia = Comunes.rellenaCeros(noCliente,3) + anio.substring(1) + "000001"; ////Solo se quiere un digito para el a�o, no dos. Por eso el substring
				qrySentencia =
					"INSERT INTO com_folio_x_if " +
					"            (ig_folio, ic_producto_nafin, ic_if, cg_anio) " +
					"     VALUES (?, ?, ?, ?) ";
			}
            rs.close();
			ps.clearParameters();
			if(ps!=null) ps.close();

			log.debug("\n getSecuencia(query)\n"+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, consecutivo);
			ps.setInt(2, Integer.parseInt(icProductoNafin));
			ps.setInt(3, Integer.parseInt(noCliente));
		    ps.setInt(4, Integer.parseInt(dia_operacion));
            
			ps.execute();
			if(ps!=null) ps.close();

		} catch (SQLException errorSQL) {
			log.error("getSecuencia(SQLException)", errorSQL);
			errorSQL.printStackTrace();
			throw errorSQL;
		}
		log.info("getSecuencia(S)");
		return secuencia;
	}


	/**
	 * Almacena el acuse en la Base de Datos.
	 * El metodo no cierra la conexion de BD, esa tarea le corresponde a los
	 * m�todos que lo manden llamar
	 * @param acuse Acuse
	 * @param montoMN Monto de documentos en Moneda Nacional
	 * @param montoInteresMN Monto Interes en Moneda Nacional
	 * @param montoDsctoMN Monto del descuento en Moneda Nacional
	 * @param montoDL Monto de documentos en Dolares
	 * @param montoInteresDL Monto de interes en Dolares
	 * @param montoDsctoDL Monto del Descuento en Dolares
	 * @param claveUsuario Clave del usuario que realiza la transaccion
	 * @param reciboElectronico Recibo electronico
	 * @param con Conexion de BD establecida
	 * @throws com.netro.exception.NafinException
	 */
	private void setAcuse(String acuse, String montoMN, String montoInteresMN, String montoDsctoMN,
			String montoDL, String montoInteresDL, String montoDsctoDL,
			String claveUsuario, String reciboElectronico, AccesoDB con)
			throws NafinException {

		String qrySentencia ="";
		try {
			if (acuse == null || acuse.trim().equals("")) {	//Acuse mal formado
				throw new NafinException("GRAL0006");
			}

			montoInteresMN = (montoInteresMN == null || montoInteresMN.equals("")) ? "NULL" : montoInteresMN;
			montoInteresDL = (montoInteresDL == null || montoInteresDL.equals("")) ? "NULL" : montoInteresDL;


			qrySentencia =
				" INSERT INTO com_acuse3 (cc_acuse, in_monto_mn, in_monto_int_mn"+
				" , in_monto_dscto_mn, in_monto_dl, in_monto_int_dl"+
				" , in_monto_dscto_dl, df_fecha_hora, ic_usuario,cg_recibo_electronico)"+
				" VALUES ('"+acuse+"', "+montoMN+"+0, "+montoInteresMN+
				"+0 , "+montoDsctoMN+"+0, "+montoDL+"+0, "+montoInteresDL+
				"+0 , "+montoDsctoDL+"+0, sysdate, '"+claveUsuario+"','"+reciboElectronico+"')";

			con.ejecutaSQL(qrySentencia);
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("GRAL0007");
		}
	}


	/**
	 * Determina si esta parametrizado el fondeo propio para la EPO
	 * con el IF especificado.
	 * por motivos de compatibilidad, ic_epo puede trae la cadena NULL en lugar
	 * de la clave del ic_epo
	 *
	 *
	 * @param claveIF Clave del IF
	 * @param ic_epo Clave de la Epo
	 * @return true si maneja fondeo propio o false de lo contrario
	 * @throws com.netro.exception.NafinException
	 */
	public boolean hayFondeoPropio(String claveIF,String ic_epo)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		String tipoFondeo = "";
		boolean resultado = false;

		try {
			if(ic_epo!=null&&!"".equals(ic_epo)) {
				con.conexionDB();
				qrySentencia =
						" SELECT CS_TIPO_FONDEO " +
						" FROM COMREL_IF_EPO_X_PRODUCTO" +
						" WHERE IC_IF = ? " +
						" AND IC_EPO =  ? " +
						" AND IC_PRODUCTO_NAFIN = ?";

				PreparedStatement ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(claveIF));
				if (ic_epo.equalsIgnoreCase("NULL")) {
					ps.setNull(2,Types.NUMERIC);
				} else {
					ps.setInt(2,Integer.parseInt(ic_epo));
				}
				ps.setInt(3,1);	//1=Descuento Electronico

				rs = ps.executeQuery();

				if(rs.next()){
					tipoFondeo = rs.getString("CS_TIPO_FONDEO");
					if("P".equals(tipoFondeo))
						resultado = true;
				}
				con.cierraStatement();
			}
		} catch (Exception e) {
			throw new AppException("Error al determinar el manejo de fondeo propio", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return resultado;
	}


//METODOS AGREGADOS POR EGB FODA 046-2004
	public HashMap getDatosFondeo(String claveIF, String ic_epo, String ic_producto_nafin)
			throws NafinException {
		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
    	HashMap ohDatosFondeo = new HashMap();

		try {
			log.info("getDatosFondeo(E)");
    		if(ic_epo!=null&&!"".equals(ic_epo)){
				con.conexionDB();
				qrySentencia =	"SELECT CS_TIPO_FONDEO, NVL(CS_RECORTE_SOLIC, 'N') as CS_RECORTE_SOLIC "+
						" FROM COMREL_IF_EPO_X_PRODUCTO"+
						" WHERE IC_IF = "+claveIF+
						" AND IC_EPO = "+ic_epo+
						" AND IC_PRODUCTO_NAFIN = "+ ic_producto_nafin;
				rs = con.queryDB(qrySentencia);
				if(rs.next()){
					ohDatosFondeo.put("TIPO_FONDEO", rs.getString("CS_TIPO_FONDEO"));
					ohDatosFondeo.put("RECORTE_SOLIC", rs.getString("CS_RECORTE_SOLIC"));
	      		}
        		con.cierraStatement();
      		}
	  		return ohDatosFondeo;
		} catch (Exception e) {
			log.error("getDatosFondeo(Error)", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getDatosFondeo(S)");
		}
	}

  // METODOS AGREGADOS POR MPCS

	public Vector getArchAutorizacionAuto ()
		throws NafinException {

		Vector vecFilas=new Vector();
		Vector vecColumnas=null;
		AccesoDB con  = new AccesoDB();
        ResultSet rs=null;
        try {
			con.conexionDB();
         	String query = " select 'e'||to_char(sysdate,'ddmm')||n.ic_nafin_electronico||'.txt' as archivo, "+
                           "        pi.ic_if "+
                           " from comrel_producto_if pi, comrel_nafin n "+
		 				  " where pi.ic_producto_nafin = 1 "+
		 				  " and pi.CS_AUT_OPER_SF = 'S' "+
  		 				  " and pi.ic_if = n.ic_epo_pyme_if "+
		 				  " and n.cg_tipo = 'I' ";
			rs = con.queryDB(query);
			while (rs.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement(rs.getString("ARCHIVO"));
				vecColumnas.addElement(rs.getString("IC_IF"));
				vecFilas.addElement(vecColumnas);



		    }
		    rs.close();
		    con.cierraStatement();
		} catch (Exception e) {
			log.error("getArchAutorizacionAuto(Error)", e);
			throw new NafinException("DSCT0072");
		}finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	    return vecFilas;
	} //Fin del m�todo getArchAutorizacionAuto

	private Vector getArchAutorizacionAuto (AccesoDB con, String idejecucion)
		throws NafinException {
		log.info("getArchAutorizacionAuto(E)");
		Vector vecFilas=new Vector();
		Vector vecColumnas=null;
        ResultSet rs=null;
        try {
         	String query =
						"SELECT    's' " +
						"       || TO_CHAR (SYSDATE, 'ddmmyy') " +
						"       || '"+idejecucion+"' " +
						"       || n.ic_nafin_electronico " +
						"       || '.txt' AS archivo, " +
						"       pi.ic_if " +
						"  FROM comrel_producto_if pi, comrel_nafin n " +
						" WHERE pi.ic_producto_nafin = 1 " +
						"   AND pi.cs_aut_oper_sf = 'S' " +
						"   AND pi.ic_if = n.ic_epo_pyme_if " +
						"   AND n.cg_tipo = 'I' ";
			rs = con.queryDB(query);
			while (rs.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement(rs.getString("ARCHIVO"));
				vecColumnas.addElement(rs.getString("IC_IF"));
				vecFilas.addElement(vecColumnas);
		    }
		    rs.close();
		    con.cierraStatement();
		} catch (Exception e) {
			log.error("getArchAutorizacionAuto(Error)", e);
			throw new NafinException("DSCT0072");
		} finally {
			log.info("getArchAutorizacionAuto(S)");
		}
	    return vecFilas;
	} //Fin del m�todo getArchAutorizacionAuto


	/**
	* Realiza la Generaci�n del Acuse de la transacci�n de autorizaci�n de
	* descuento y lo almacena en la tabla de acuses.
	* El m�todo que mande llamar a este, es el responsable de cerrar la conexion.
	* @param ic_if Clave del Intermediario financiero
	* @param con Conexion establecida de Base de Datos.
	* @return Cadena con el acuse generado
	* @throws com.netro.exception.NafinException
	*/
	private String GeneraAcuse(String ic_if, AccesoDB con)
			throws NafinException {

		return this.GeneraAcuse(ic_if, "dscto_aut", con);
	} // Fin del proceso GeneraAcuse

	/**
	* Realiza la Generaci�n del Acuse de la transacci�n de autorizaci�n de
	* descuento y lo almacena en la tabla de acuses.
	* El m�todo que mande llamar a este, es el responsable de cerrar la conexion.
	* @param ic_if Clave del Intermediario financiero
	* @param usuario Clave del usuario
	* @param con Conexion establecida de Base de Datos.
	* @return Cadena con el acuse generado
	* @throws com.netro.exception.NafinException
	*/
	private String GeneraAcuse(String ic_if, String usuario, AccesoDB con)
			throws NafinException {

		String acuse="";
		boolean error = false;
		try {
			Acuse no_acuse = new Acuse(Acuse.ACUSE_IF,"1");
			acuse=no_acuse.toString();
			String claveUsuario=usuario;
			this.setAcuse(acuse, "0", "0", "0", "0", "0", "0",claveUsuario,"",con);
		} catch (Exception e) {
			error = true;
			throw new NafinException("DSCT0062");
		} finally {
			if (con.hayConexionAbierta()) {
				// Realiza un commit o un rollback
				if (error == true) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
			} // If cuando hay conexion
		}
		return acuse;
	} // Fin del proceso GeneraAcuse

	private boolean hayFondeoPropio(String claveIF,String ic_epo, AccesoDB con  )
			throws NafinException {
		log.info("hayFondeoPropio(E)");
		String qrySentencia = null;
		ResultSet rs = null;
    	String tipoFondeo = "";
	    boolean resultado = false;
		try {
    		if(ic_epo!=null&&!"".equals(ic_epo)){
				qrySentencia =	"SELECT CS_TIPO_FONDEO "+
						" FROM COMREL_IF_EPO_X_PRODUCTO"+
						" WHERE IC_IF = "+claveIF+
						" AND IC_EPO = "+ic_epo+
						" AND IC_PRODUCTO_NAFIN = 1";
				rs = con.queryDB(qrySentencia);
				if(rs.next()){
					tipoFondeo = rs.getString("CS_TIPO_FONDEO");
        			if("P".equals(tipoFondeo))
          				resultado = true;
	      		}
	      		rs.close();
        		con.cierraStatement();
			}
		} catch (Exception e) {
			throw new NafinException("DSCT0073");
		} finally {
			log.info("hayFondeoPropio(S)");
		}
		return resultado;
	}

	/**
	 * Realiza la operaci�n o rechazo del descuento del documento, por
	 * parte del IF.
	 * @param sc_if Clave del IF
	 * @param sc_epo Clave de la EPO
	 * @param sc_sirac Clave Sirac
	 * @param sc_moneda Moneda del documento
	 * @param ig_numero_docto Numero de documento
	 * @param sf_vencimiento Fecha de vencimiento
	 * @param sc_estatus Estatus
	 * @param acuse Acuse
	 * @param con Conexion
	 * @throws com.netro.exception.NafinException
	 */
	private void ProcesaDocto(String sc_if,
								String sc_epo,
								String sc_sirac,
								String sc_moneda,
								String ig_numero_docto,
								String sf_vencimiento,
								String sc_estatus,
								String acuse,
								AccesoDB con)
			throws NafinException
	{
		log.info("ProcesaDocto(E)");
		boolean error=false;
		int ic_docto=0,ic_epo=0,ic_if=0;
		String aux=null;
		String query="";
		String estatusSolic="1";
		String porcentajeFondeo="null";
		ResultSet rs=null;
		int ic_moneda=(sc_moneda.equals("MXP"))?1:0;
		try
		{   /* Verificaciones de datos */
			if (ic_moneda==0)   ic_moneda=(sc_moneda.equals("USD"))?54:0;
			aux=sf_vencimiento.substring(8,10)+"/"+sf_vencimiento.substring(5,7)+"/"+sf_vencimiento.substring(0,4);
			try
			{	if ((!Comunes.checaFecha(aux))||(ic_moneda==0))
				{	error=true;
				}
				ic_epo=Integer.parseInt(sc_epo);
				ic_if=Integer.parseInt(sc_if);
			}
			catch (Exception e)
			{	throw new NafinException("DSCT0012");
			}
			if (!error) {
			  /* Busqeda del documento */
			  query =
			  		" SELECT ic_documento FROM com_documento d WHERE "+
					" EXISTS (SELECT 1 FROM comcat_pyme p WHERE "+
					"              p.ic_pyme=d.ic_pyme AND "+
					"              p.in_numero_sirac = '"+sc_sirac+"' ) AND "+
					" ic_if=to_number('"+sc_if+"') AND "+
					" ic_epo=to_number('"+sc_epo+"') AND "+
					" ic_moneda="+ic_moneda+" AND "+
					" TRUNC(df_fecha_venc)=TO_DATE('"+sf_vencimiento+"','yyyy-mm-dd') AND "+
					" ic_estatus_docto=24 AND "+
					" ig_numero_docto='"+ig_numero_docto+"' ";
			  rs= con.queryDB(query);
			  if(rs.next())
			  {
			  		ic_docto=rs.getInt("IC_DOCUMENTO");
		  	  }
			  rs.close();
			  con.cierraStatement();

				if (sc_estatus.equals("OK")&&(ic_docto!=0))
	            {
					log.debug("Docto aceptado:"+ic_docto+" Numero:"+ig_numero_docto);
					/* Validaciones para la generaci�n de solicitud */
					if (hayClaseDoctoAsociado(Integer.toString(ic_epo), con)) {	//No existe una clase de documento asociado para realizar Descuento?
                      //if (hayFondeoPropio(Integer.toString(ic_if),Integer.toString(ic_epo),con)) {
						/* Fondeo Propio .- Obtencion del porcentaje de comision */
						query = "select FG_PORC_COMISION_FONDEO"+
										" from comcat_producto_nafin"+
										" where ic_producto_nafin = 1";
						rs = con.queryDB(query);
						if (rs.next())
						{  porcentajeFondeo = rs.getString("FG_PORC_COMISION_FONDEO");
						   estatusSolic = "10";
						} else
						{ log.info("No se encontr� porcentaje de comisi�n");
						  error=true;
						}
						/* Se obtuvo porcentaje */
						rs.close();
						con.cierraStatement();
					    /* } Se manejo fondeo propio */
						/* Verificar que solo exista una solicitud por documento */
						query = "select count(*) from com_solicitud"+
								" where ic_documento=" + ic_docto;
						rs = con.queryDB(query);
					  	rs.next();
		  				if (rs.getInt(1) == 0) { //El ic_documento no existe en solicitudes?
		  				    rs.close();
					  		con.cierraStatement();
							/* Obtencion de los datos del documento */
							String fechaDocumento = "";
							String fechaVencimiento = "";
							String diaVencimiento = "";
							String tasaIF = "";
							String claseDocumento = "";
							String montoDescuento = "";
							int litipoPiso = 0;
							int iDiaPlazo = 0;
							String sFechaVencDesc = "";
							String sPagoAntic = null;
							String ic_banco_fondeo = "";
							query = " select TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto "+
									" , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc "+
									" , TO_CHAR(d.df_fecha_venc,'dd') as  diaVencimiento "+
									" , ds.in_importe_recibir " +
									" , t.ic_tasa "+
									" , pd.ic_clase_docto "+
									" , i.ig_tipo_piso "+
									" , trunc(d.df_fecha_venc) - trunc(sysdate) as plazo "+
									" , ie.cs_pago_anticipado "+
									" , e.ic_banco_fondeo "+
									" from com_documento d "+
									" , comcat_tasa t "+
									" , comrel_producto_docto pd "+
									" , com_docto_seleccionado ds "+
									" , comrel_tasa_autorizada ta "+
									" , comrel_tasa_base tb "+
									" , comrel_if_epo ie "+
									" , comcat_if i "+
									" , comcat_epo e " +
									" where "+
									" ds.dc_fecha_tasa = ta.dc_fecha_tasa "+
									" and ds.ic_epo = ta.ic_epo "+
									" and ds.ic_if = ta.ic_if "+
									" and ta.dc_fecha_tasa = tb.dc_fecha_tasa "+
									" and tb.ic_tasa = t.ic_tasa "+
									" and t.ic_moneda = d.ic_moneda "+
									" and d.ic_epo=pd.ic_epo "+
									" and pd.ic_producto_nafin=1 "+
									" and d.ic_documento=" + ic_docto +
									" and d.ic_documento = ds.ic_documento"+
									" and ds.ic_if = ie.ic_if " +
									" and ds.ic_epo = ie.ic_epo " +
									" and ie.ic_if = i.ic_if " +
									" and ie.ic_epo = e.ic_epo ";

							log.debug("ProcesaDocto()::" + query);
							rs = con.queryDB(query);
							if (rs.next())
							{
								fechaDocumento = rs.getString("DF_FECHA_DOCTO");
								fechaVencimiento = rs.getString("DF_FECHA_VENC");
								diaVencimiento = rs.getString("DIAVENCIMIENTO");
								tasaIF = rs.getString("IC_TASA");
								claseDocumento = rs.getString("IC_CLASE_DOCTO");
								montoDescuento = rs.getString("IN_IMPORTE_RECIBIR");
								litipoPiso = rs.getInt("IG_TIPO_PISO");
								iDiaPlazo = rs.getInt("PLAZO") ;
								int iDiasVenc = iDiaPlazo;
								sPagoAntic = (rs.getString("CS_PAGO_ANTICIPADO") == null)?"N":rs.getString("CS_PAGO_ANTICIPADO");
								sFechaVencDesc = "";
								ic_banco_fondeo = rs.getString("IC_BANCO_FONDEO");
								rs.close();
								con.cierraStatement();
								if((litipoPiso == 2)&&(!hayFondeoPropio(Integer.toString(ic_if),Integer.toString(ic_epo),con))) {	// Si el IF es de 2 piso y no es fondeo propio se hace los calculos de los d�as de plazo.
									boolean bEntroEnUnPlazo = false;
									Calendar cFechaVencDesc = Calendar.getInstance();
									cFechaVencDesc.setTime(new java.util.Date());
									SimpleDateFormat sdfv = new SimpleDateFormat("dd/MM/yyyy");
									if ("S".equals(sPagoAntic)) {
										query="select in_dia from com_pago_antic_detalle "+
												" where "+iDiaPlazo+" between in_plazo_minimo and in_plazo_maximo "+
												" and ic_if = "+ ic_if +
												" and ic_epo = "+ic_epo;
										rs = con.queryDB(query);
										if(rs.next()) {
											iDiaPlazo = rs.getInt("IN_DIA");	//El nuevo plazo.
											if(iDiasVenc > iDiaPlazo) {	// Suma el plazo solo si este es mayor a los dias adicionales.
											/* Solo si existe una reduccion real del plazo */
												bEntroEnUnPlazo = true;
												if(iDiaPlazo > 120) {
												 	iDiaPlazo=120;
												}
								         		cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
											} else
											{	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
											}
										} else
										{ /* No se encuentra parametrizado el rango */
											if(iDiaPlazo > 120) {
											 	iDiaPlazo=120;
								         		cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
												bEntroEnUnPlazo = true;
											} else
											{	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
											}
										}
										rs.close();
										con.cierraStatement();
									} else /* No hay ajuste por pago anticipado */
									{ /* El ajuste se hace porque sobrepasa el plazo tope de 120 dias */
											if(iDiaPlazo > 120) {
											 	iDiaPlazo=120;
								         		cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
												bEntroEnUnPlazo = true;
											} else
											{	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
											}
									} /* Fin del if-else del pago anticipado */
									if (bEntroEnUnPlazo)
									{  /* Validaciones porque hubo cambios en las fechas de vencimiento */
										boolean busca=true;
										boolean aumenta=true;
										String  inhabil=null;
										while (busca)
										{  	query="SELECT ES_INHABIL(TO_DATE('"+sdfv.format(cFechaVencDesc.getTime())+
													"','DD/MM/YYYY')) as INHABIL FROM DUAL";
											log.debug("Ajuste de fechas:"+sdfv.format(cFechaVencDesc.getTime()));
											rs=con.queryDB(query);
											if (rs.next())
											{ inhabil=rs.getString("INHABIL");
											  if (inhabil.equals("0"))
											  { /* No es inhabil el dia */
											    busca=false;
										  	  } else if ((iDiaPlazo<=120)&&(aumenta))
										  	  { /* Se aumenta el vencimiento por el ajuste */
										  	    if (iDiaPlazo==120)
										  	    {  aumenta=false;
										  	       iDiaPlazo--;
										  	       cFechaVencDesc.add(Calendar.DATE, -1);
												} else
												{  iDiaPlazo++;
												   cFechaVencDesc.add(Calendar.DATE, 1);
												}
										  	  } else if ((iDiaPlazo<=120)&&(!aumenta))
										  	  { /* Se disminuye el vencimiento por el tope de 120 dias*/
										  	       iDiaPlazo--;
										  	       cFechaVencDesc.add(Calendar.DATE, -1);
											  } else
											  { /* Nunca se toca este codigo */
											    error=true;
											    busca=false;
										  	  }
											} else
											{ /* No se pudo determinar si es inhabil */
											  error=true;
											  log.info("No se puede determinar si es dia inhabil");
											  busca=false;
											}
											rs.close();
											con.cierraStatement();
										} /* while de la busqueda del dia habil mas cercano*/
									} // Fin del ajuste a la fecha de vencimiento
									sFechaVencDesc = sdfv.format(cFechaVencDesc.getTime());
									diaVencimiento = sdfv.format(cFechaVencDesc.getTime()).substring(0,2);
								} // Fin de los ajustes por ser IF de segundo piso

								String sFechaVencimiento = (sFechaVencDesc.equals(""))?fechaVencimiento:sFechaVencDesc;
								String folio=null;
			 					if (!error)
			 					{	/* Obtenci�n de la raz�n social de la EPO */
			 						query="select cg_razon_social from comcat_epo where ic_epo="+ic_epo;
			 						try
			 						{ rs=con.queryDB(query);
			 						  if (rs.next())
			 						  {  String emisor=rs.getString("CG_RAZON_SOCIAL");
									  	 rs.close();
									  	 con.cierraStatement();
										/* Obtenci�n del folio de la solicitud */
										folio = getFolioSolicitud(sc_if, con);
										query="insert into com_solicitud (" +
											" ic_folio, ic_documento, ic_esquema_amort, ic_tipo_credito," +
											" ic_oficina, cc_acuse, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap," +
											" cg_perio_pago_int, cg_desc_bienes, ic_clase_docto," +
											" cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif," +
											" ic_tabla_amort, in_numero_amort, df_ppc, df_ppi," +
											" df_v_documento, df_v_descuento, in_dia_pago," +
											" df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic," +
											" df_fecha_solicitud, ic_bloqueo, cs_tipo_solicitud, " +
											" cg_emisor,fg_porc_comision_fondeo,ic_banco_fondeo) " +
											" values ('" + folio + "', " + ic_docto + ", 1, 1, 90, '" + acuse + "'" +
											" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy') - TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')" +
									//		" , " + iDiaPlazo +
											" , 'D', 'Al Vto.', 'Al Vto.', ' ', " + claseDocumento + ", 'Mexico, D.F.'" +
											" , " + tasaIF + ", '+', 0, 1, 1" +
											" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
											" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
											" , TO_DATE('" + fechaVencimiento + "','dd/mm/yyyy')" +
											" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
											" , " + diaVencimiento + ", TO_DATE('" + fechaDocumento + "','dd/mm/yyyy')" +
											" , 'Mexico, D.F.', 0, "+estatusSolic+", sysdate, NULL, 'C', '" + emisor + "',"+porcentajeFondeo+ ", "+ic_banco_fondeo+")";
			 							log.debug("ProcesaDocto()::" + query);
										con.ejecutaSQL(query);
										log.debug(new java.util.Date() + " Solicitud: " + folio + " insertada");
								      } /* fin de la extraccion de la razon social */
									}catch(SQLException sqle) {
										//error = true;
										throw new NafinException("DSCT0071");
									}
								} /* Fin del if insercion de la solicitud */
								if (!error)
								{	query = "insert into com_amortizacion (ic_amortizacion, ic_folio, in_numero" +
											" , fn_importe, df_fecha)" +
											" values (seq_com_amortizacion_ic_amor.NEXTVAL, '" + folio + "'" +
											" , 1," + montoDescuento + ", TO_DATE('" + fechaVencimiento + "','dd/mm/yyyy'))";
									try {
										con.ejecutaSQL(query);
										//Actualizamos al documento con el estatus Operada (4)
										query = "UPDATE COM_DOCUMENTO set ic_estatus_docto = 4 " +
											"where ic_documento = " + ic_docto;
										try {
											con.ejecutaSQL(query);
										}catch(SQLException sqle) {
											//error = true;
											throw new NafinException("DSCT0063");
										}
									}catch(SQLException sqle) {
										error = true;
									}
								} /* Fin del if de la Insercion de la amortizacion y la actualizacion del docto */
							} else
					  		{	rs.close();
					  			con.cierraStatement();
								error=true;
								throw new AppException("No se pudo encontrar los datos del documento");
							} /* Fin del if de la extraccion de los datos del documento */
						} else
					  	{	rs.close();
					  		con.cierraStatement();
							error=true;
							throw new AppException("Existen solicitudes ya registradas para esta EPO");
						} /* Fin de las busqueda en solicitudes */
					} /* if Validacion de Documento asociado */
					else
				    {
				    			error=true;
				    			throw new AppException("Sin documento asociado a la operaci�n");
					} /* else Validacion de  Documento asociado */
				} else if ((ic_docto!=0)&&( (sc_estatus.length())<=8 && (sc_estatus.length())>0)) // El estatus es diferente a OK y se buscara el error
				{
					log.debug("Docto con error:"+ic_docto+" Numero:"+ig_numero_docto);
					String causa;

					try
					{	/* Busca la descripcion de la causa de rechazo */
						query = "select cg_descripcion from com_error_if where "+
								" cc_error_if = '"+sc_estatus+"' "+
								" and ic_if = "+sc_if+" ";
						rs=con.queryDB(query);
						if (rs.next())
						{	causa=rs.getString("CG_DESCRIPCION");
						    rs.close();
						    con.cierraStatement();
							/* Inserta historico */
							query="INSERT INTO comhis_cambio_estatus (dc_fecha_cambio,ic_documento"+
								" , ic_cambio_estatus, ct_cambio_motivo)"+
								" values (sysdate,"+ic_docto+",23,'"+causa+"')";
							con.ejecutaSQL(query);
							/* Modifica el estatus del documento */
							query=" update com_documento set ic_estatus_docto=2 where ic_documento='"+ic_docto+"' ";
							con.ejecutaSQL(query);
						} else
						{	//error=true;
						    rs.close();
						    con.cierraStatement();
						    throw new AppException("No se encontr� la descripci�n de la causa de rechazo");
						}
					} catch(Exception e) {
						//error=true;
						throw new NafinException("DSCT0011");
					}
				} else /* No se encontro el documento */
				{
					error=true;
					throw new AppException("No se encontro el documento.");
			    }
		 	 } else
		 	 {	throw new AppException("La moneda o el formato de fecha es invalido");
			 } //If cuando no existe error en los datos iniciales
		} /* try principal */
		catch(SQLException sqle) {	// Marcar el registro con error
			sqle.printStackTrace();
			error = true;
		}
		catch(NafinException ne) {	// Marcar el registro con error
			error = true;
			log.error("ProcesaDocto(Exception)",ne);
		} finally {
			if (con.hayConexionAbierta()) {
				/* Realiza un commit o un rollback */
				if (error == true)
				{	con.terminaTransaccion(false);
				} else
				{ 	con.terminaTransaccion(true);
				}
			} /* If cuando hay conexion */
			log.info("ProcesaDocto(S)");
		}
	} // Fin del m�todo ProcesaDocto


	/**
	 * Realiza la actualizaci�n de los datos del acuse (montos, num. registros
	 *  procesados,etc.).
	 *
	 * @param acuse clave del Acuse
	 * @param con Conexion de Base de datos (debe estar establecida la conexion)
	 * @return true Si se logr� la actualizaci�n o false de lo contrario.
	 * @throws com.netro.exception.NafinException
	 */
	public boolean ActualizaAcuse(String acuse,AccesoDB con)
			throws NafinException {
		//Por motivos de compatibilidad se deja este m�todo que al actualizar el acuse no considera el recibo de la firma digital.
		//ademas de que si hay una excepci�n no la reporte... solo un true o false
		return this.ActualizaAcuse(acuse, "", con);
	}


	/**
	 * Realiza la actualizaci�n de los datos del acuse (montos, num. registros
	 *  procesados,etc.).
	 *
	 * @param acuse clave del Acuse
	 * @param reciboFirma Recibo regresado por el proceso de firma digital
	 * @param con Conexion de Base de datos (debe estar establecida la conexion)
	 * @return true Si exiti� un error al realizar la actualizaci�n o false de lo contrario.
	 * @throws com.netro.exception.NafinException
	 */
	public boolean ActualizaAcuse(String acuse, String reciboFirma, AccesoDB con)
			throws NafinException {
		log.info("ActualizaAcuse(E)");
		boolean error=false;

		/* Actualiza el acuse con los datos de las solicitudes */
		String qrybuscadatos=
				" SELECT nvl(sum(decode(d.ic_moneda,1,d.fn_monto,0)),0) monto_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,d.fn_monto,0)),0) monto_dl, "+
				"		nvl(sum(d.fn_monto),0) monto_total, "+
				"		nvl(sum(decode(d.ic_moneda,1,ds.in_importe_interes,0)),0) monto_int_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,ds.in_importe_interes,0)),0) monto_int_dl, "+
				"		nvl(sum(ds.in_importe_interes),0) monto_int_total, "+
				"		nvl(sum(decode(d.ic_moneda,1,d.fn_monto_dscto,0)),0) monto_d_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,d.fn_monto_dscto,0)),0) monto_d_dl, "+
				"		nvl(sum(d.fn_monto_dscto),0) monto_total, "+
				"		nvl(sum(1),0) total_reg "+
				" FROM com_solicitud s, "+
				"      com_documento d, "+
				"      com_docto_seleccionado ds "+
				" WHERE s.ic_documento=d.ic_documento "+
				"   and s.ic_documento=ds.ic_documento "+
				"   and d.ic_documento=ds.ic_documento "+
				"  	and s.cc_acuse=? ";
		ResultSet rs=null;
		PreparedStatement ps = null;
		try {
			ps = con.queryPrecompilado(qrybuscadatos);
			ps.setString(1, acuse);
			rs = ps.executeQuery();
			if (rs.next()) {
				String in_monto_mn=rs.getString("MONTO_MN");
				String in_monto_dl=rs.getString("MONTO_DL");
				String in_monto_int_mn=rs.getString("MONTO_INT_MN");
				String in_monto_int_dl=rs.getString("MONTO_INT_DL");
				String in_monto_dscto_mn=rs.getString("MONTO_D_MN");
				String in_monto_dscto_dl=rs.getString("MONTO_D_DL");
				String total_reg=rs.getString("TOTAL_REG");
				rs.close();
				con.cierraStatement();
				if (!(total_reg.equals("0"))) {
					String qryactualiza =
							" UPDATE com_acuse3 SET "+
							"IN_MONTO_MN = ?, "+
							"IN_MONTO_INT_MN = ?, "+
							"IN_MONTO_DSCTO_MN = ?, "+
							"IN_MONTO_DL = ?, "+
							"IN_MONTO_INT_DL = ?, "+
							"IN_MONTO_DSCTO_DL = ?, "+
							"cg_recibo_electronico = ? " +
							"WHERE cc_acuse = ? ";
					PreparedStatement psUpdate = con.queryPrecompilado(qryactualiza);
					psUpdate.setDouble(1,Double.parseDouble(in_monto_mn));
					psUpdate.setDouble(2,Double.parseDouble(in_monto_int_mn));
					psUpdate.setDouble(3,Double.parseDouble(in_monto_dscto_mn));
					psUpdate.setDouble(4,Double.parseDouble(in_monto_dl));
					psUpdate.setDouble(5,Double.parseDouble(in_monto_int_dl));
					psUpdate.setDouble(6,Double.parseDouble(in_monto_dscto_dl));
					psUpdate.setString(7,reciboFirma);
					psUpdate.setString(8,acuse);
					psUpdate.executeUpdate();
					psUpdate.close();
				} else {
					error=true; /* No existen registros que pertenezcan al acuse */
					log.info("ActualizaAcuse():: NO existen registros que pertenezcan al acuse");
				}
			} else {
				error=true;
			}
		} catch (Exception e) {
			log.error("ActualizaAcuse(Error): ", e);
			error=true;
			if ( !"".equals(reciboFirma) ) {		//Si reciboFirma no es vacio es una invocaci�n directa desde este metodo por lo cual se puede lanzar la excepci�n
				throw new AppException("Error al actualizar el acuse", e);
			}
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs!=null) {
						rs.close();
					}
					if (ps!=null) {
						ps.close();
					}
				}catch(Exception e) {
					log.error("ActualizaAcuse(Error)" + e.getMessage(), e);
				}

				/* Realiza un commit o un rollback */
				if (error == true) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
			} /* If cuando hay conexion */
			log.info("ActualizaAcuse(S)");
		}
		return error;
	}

	/**
	 * Fodea 041-2014  
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param no_documentos
	 * @param con
	 * @param reciboFirma
	 * @param acuse
	 */
	public boolean ActualizaAcusePanIF(String acuse, String reciboFirma, AccesoDB con, String no_documentos ) 	throws NafinException {
		log.info("ActualizaAcusePanIF(E)");
		boolean error=false; 

		if(!no_documentos.equals("")){
			no_documentos = no_documentos.substring(0,no_documentos.length()-1);
		}
			
		/* Actualiza el acuse con los datos de las solicitudes */
		String qrybuscadatos=
				" SELECT nvl(sum(decode(d.ic_moneda,1,d.fn_monto,0)),0) monto_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,d.fn_monto,0)),0) monto_dl, "+
				"		nvl(sum(d.fn_monto),0) monto_total, "+
				"		nvl(sum(decode(d.ic_moneda,1,ds.in_importe_interes,0)),0) monto_int_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,ds.in_importe_interes,0)),0) monto_int_dl, "+
				"		nvl(sum(ds.in_importe_interes),0) monto_int_total, "+
				"		nvl(sum(decode(d.ic_moneda,1,d.fn_monto_dscto,0)),0) monto_d_mn, "+
				"		nvl(sum(decode(d.ic_moneda,54,d.fn_monto_dscto,0)),0) monto_d_dl, "+
				"		nvl(sum(d.fn_monto_dscto),0) monto_total, "+
				"		nvl(sum(1),0) total_reg "+
				" FROM com_documento d, "+
				"      com_docto_seleccionado ds "+
				" WHERE d.ic_documento=ds.ic_documento "+
				" and d.ic_documento in ( "+no_documentos+" ) ";
				
		ResultSet rs=null;
		PreparedStatement ps = null;
		try {
		
			log.info("qrybuscadatos "+qrybuscadatos);  
		
			ps = con.queryPrecompilado(qrybuscadatos);			
			rs = ps.executeQuery();
			if (rs.next()) {
				String in_monto_mn=rs.getString("MONTO_MN");
				String in_monto_dl=rs.getString("MONTO_DL");
				String in_monto_int_mn=rs.getString("MONTO_INT_MN");
				String in_monto_int_dl=rs.getString("MONTO_INT_DL");
				String in_monto_dscto_mn=rs.getString("MONTO_D_MN");
				String in_monto_dscto_dl=rs.getString("MONTO_D_DL");
				String total_reg=rs.getString("TOTAL_REG");
				rs.close();
				con.cierraStatement();
				if (!(total_reg.equals("0"))) {
					String qryactualiza =
							" UPDATE com_acuse3 SET "+
							"IN_MONTO_MN = ?, "+
							"IN_MONTO_INT_MN = ?, "+
							"IN_MONTO_DSCTO_MN = ?, "+
							"IN_MONTO_DL = ?, "+
							"IN_MONTO_INT_DL = ?, "+
							"IN_MONTO_DSCTO_DL = ?, "+
							"cg_recibo_electronico = ? " +
							"WHERE cc_acuse = ? ";
					PreparedStatement psUpdate = con.queryPrecompilado(qryactualiza);
					psUpdate.setDouble(1,Double.parseDouble(in_monto_mn));
					psUpdate.setDouble(2,Double.parseDouble(in_monto_int_mn));
					psUpdate.setDouble(3,Double.parseDouble(in_monto_dscto_mn));
					psUpdate.setDouble(4,Double.parseDouble(in_monto_dl));
					psUpdate.setDouble(5,Double.parseDouble(in_monto_int_dl));
					psUpdate.setDouble(6,Double.parseDouble(in_monto_dscto_dl));
					psUpdate.setString(7,reciboFirma);
					psUpdate.setString(8,acuse);
					psUpdate.executeUpdate();
					psUpdate.close();
				} else {
					error=true; /* No existen registros que pertenezcan al acuse */
					log.info("ActualizaAcusePanIF():: NO existen registros que pertenezcan al acuse");
				}
			} else {
				error=true;
			}
		} catch (Exception e) {
			log.error("ActualizaAcusePanIF(Error): ", e);
			error=true;
			if ( !"".equals(reciboFirma) ) {		//Si reciboFirma no es vacio es una invocaci�n directa desde este metodo por lo cual se puede lanzar la excepci�n
				throw new AppException("Error al actualizar el acuse", e);
			}
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs!=null) {
						rs.close();
					}
					if (ps!=null) {
						ps.close();
					}
				}catch(Exception e) {
					log.error("ActualizaAcusePanIF(Error)" + e.getMessage(), e);
				}

				/* Realiza un commit o un rollback */
				if (error == true) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
			} /* If cuando hay conexion */
			log.info("ActualizaAcusePanIF(S)");
		}
		return error;
	}
	
    private void BorraAcuse(String acuse,AccesoDB con)
		throws NafinException
    {	/* Regresar los documentos a en proceso de autorizacion IF */
    	log.info("BorraAcuse(E):"+acuse);
		String qryborra =
			"UPDATE com_documento " +
			"   SET ic_estatus_docto = 24 " +
			" WHERE ic_documento IN (SELECT sol.ic_documento " +
			"                          FROM com_solicitud sol " +
			"                         WHERE sol.cc_acuse = '"+acuse+"') ";
        try
        {	con.ejecutaSQL(qryborra);

        	qryborra =
				"DELETE com_amortizacion " +
				"      WHERE ic_folio IN (SELECT sol.ic_folio " +
				"                           FROM com_solicitud sol " +
				"                          WHERE sol.cc_acuse = '"+acuse+"') ";
        	con.ejecutaSQL(qryborra);


        	qryborra = "DELETE com_solicitud s WHERE s.cc_acuse = '"+acuse+"' ";
        	con.ejecutaSQL(qryborra);

			qryborra = "DELETE com_acuse3 WHERE cc_acuse = '"+acuse+"' ";
        	con.ejecutaSQL(qryborra);
			con.terminaTransaccion(true);
		/* Manejo de exceptiones */
		} catch (Exception e) {
			if (con.hayConexionAbierta())
			{
				con.terminaTransaccion(false);
			}
			throw new NafinException("DSCT0069");
		} finally {
				log.info("BorraAcuse(S)");
		}
	}


	public void ConfirmacionAutomaticaIF(String rutaApp, String idejecucion)
		throws NafinException
	{
		AccesoDB con = new AccesoDB();
		Calendar calendario= new GregorianCalendar();
		String meses[]={"01","02","03","04","05","06","07","08","09","10","11","12"};
        boolean error = false;
        String acuse="";

		try
		{	con.conexionDB();
			FileWriter fstream = new FileWriter(rutaApp+calendario.get(Calendar.DATE)+meses[calendario.get(Calendar.MONTH)]+calendario.get(Calendar.YEAR)+"MonProcBBVA.log");
			BufferedWriter out = new BufferedWriter(fstream);
			log.debug("Inicio del proceso : Confirmacion de Autorizacion IF");
            String snombreArch = "", linea = "",ic_if="";
            Vector VecFilas=null, VecColumnas=null;
            String ruta= rutaApp;
            StringBuffer nombreArchivo= new StringBuffer ("");
            /* Obtencion de los nombre a archivos a procesar */
            VecFilas=getArchAutorizacionAuto(con, idejecucion);
			/* Por cada nombre */
			for(int i=0; i<=(VecFilas.size()-1); i++) {
				VecColumnas=(Vector)VecFilas.get(i);
				snombreArch = (String) VecColumnas.get(0);
				ic_if = (String) VecColumnas.get(1);
				nombreArchivo = new StringBuffer ("");
				nombreArchivo.append(ruta+snombreArch);
				java.io.File f=new java.io.File(nombreArchivo.toString());
				log.debug("File:"+f.getAbsoluteFile());


			  if (f.exists())
			  {
				log.debug("Inicia Proceso");
				out.write("Inicia Proceso\n");

				BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			  	/* Extraccion de la primera linea */
			  	 if ((linea=br.readLine())!=null)
			  	 { 	/* Obtenci�n del acuse por IF */
			  		acuse=GeneraAcuse(ic_if,con);
                     if (!acuse.equals(""))
					 {
						 for (int j=1; (linea=br.readLine())!=null; j++)
						 {  if ((linea.length())>=125)
							{
								String sc_epo 				= linea.substring(0,6).trim();
								if(!Comunes.esNumero(sc_epo)) {
									sc_epo = sc_epo.substring(0,2);
								}
								String sc_sirac 			= (new Long(linea.substring(6,18))).toString();
								String sc_moneda 			= linea.substring(18,21).trim();
								String ig_numero_docto 		= linea.substring(21,37).trim();
								String sf_vencimiento 		= linea.substring(37,47).trim();
								String sc_estatus 			= linea.substring(125,132).trim();

								log.debug(	ic_if+","+
													sc_epo+","+
													sc_sirac+","+
													sc_moneda+","+
													ig_numero_docto+","+
													sf_vencimiento+","+
													sc_estatus);

								out.write("EPO: "+sc_epo+","+
										  "IF: "+ic_if+","+
										  "Moneda: "+sc_moneda+","+
										  "IGNumDoc: "+ig_numero_docto+","+
										  "Vencimiento: "+sf_vencimiento+","+
										  "Estatus: "+sc_estatus+","+
										  "SIRAC: "+sc_sirac+"\n");
								try {
							 		ProcesaDocto(
													ic_if,
													sc_epo,
													sc_sirac,
													sc_moneda,
													ig_numero_docto,
													sf_vencimiento,
													sc_estatus,
													acuse,
													con);
/*							 		ProcesaDocto(ic_if,
											linea.substring(0,6).trim(),
											linea.substring(6,13).trim(),
											(linea.substring(13,16)).trim(),
											(linea.substring(16,32)).trim(),
											(linea.substring(32,42)).trim(),
											(linea.substring(120,127)).trim(),
											acuse,con);*/
								} catch (NafinException ne) {
									log.error ("Error " + ne.getMessage());
								}
								log.debug("Linea:"+j+" procesada");

								out.write("Linea:"+j+" procesada\n");
					     	}
						 } //Fin del for de  la lectura de las l�neas
			  			/* Actualiza el acuse */
			  			error= ActualizaAcuse(acuse,con);
		  	  			if ((error)&&(!acuse.equals("")))
			  			{  BorraAcuse(acuse,con);
			  			}
			  			if ((!error)&&(!acuse.equals("")))
			  			{		log.debug("Acuse generado:"+acuse);
			  					out.write("Acuse generado:"+acuse+"\n");
		  	  			}
			  			error=false;
		  			    acuse="";
				     }
			  	 } /* Lectura de la primera linea */
		  	  }  else
		  	  { log.info("No se encontr� el archivo");
		  	  	out.write("No se encontr� el archivo\n");
		  	    error=true;
			  } /* Procesamiento solo si existe el archivo */
			} /* Fin del for */
			out.close();
		}	catch (IOException ioe){
			log.info("Error en log");
		}	catch (Exception e)
		{	error=true;
			//log.info("Error en el proceso autom�tico de autorizaci�n: "+e);
			throw new NafinException("DSCT0070");
		}
		finally
		{
			if (con.hayConexionAbierta())
			{ error= ActualizaAcuse(acuse,con);
			  if ((error)&&(!acuse.equals("")))
			  {  BorraAcuse(acuse,con);
			  }
			  con.cierraConexionDB();
			}
		}
	} // Fin del metodo ConfirmacionAutomaticaIF


	public String ConfirmacionManualIF(String ruta,String snombreArch,String ic_if)
		throws NafinException
	{
		AccesoDB con  = new AccesoDB();
        boolean error = false;
        String acuse="";

		try
		{	con.conexionDB();
			log.debug("Inicio del proceso : Confirmacion de Autorizacion IF");
            String linea = "";
            StringBuffer nombreArchivo= new StringBuffer ("");
            /* Obtencion de los nombre a archivos a procesar */
			/* Por cada nombre */
			  nombreArchivo.delete(0,100);
			  nombreArchivo.append(ruta+snombreArch);
			  log.debug("Procesando Arch:"+snombreArch);
			  //StringBuffer ruta2= new StringBuffer("/opt/weblogic/myserver/public_html/nafin/FileTransfer/Qm/Colalocal/E11039862.txt");
			  java.io.File f=new java.io.File(nombreArchivo.toString());
			  if (f.exists())
			  {
				BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			  	/* Extraccion de la primera linea */
			  	 if ((linea=br.readLine())!=null)
			  	 { 	/* Obtenci�n del acuse por IF */
			  		acuse=GeneraAcuse(ic_if,con);
                     if (!acuse.equals(""))
					 {
						 for (int j=1; (linea=br.readLine())!=null; j++)
						 {  if ((linea.length())>=125)
							{
								log.debug(ic_if+","+
												 linea.substring(0,6)+","+
												 linea.substring(6,13)+","+
												 (linea.substring(13,16)).trim()+","+
												 (linea.substring(16,32)).trim()+","+
												 (linea.substring(32,42)).trim()+","+
												 (linea.substring(120,127)).trim());
								try
							 	{	ProcesaDocto(ic_if,
											linea.substring(0,6).trim(),
											linea.substring(6,13).trim(),
											(linea.substring(13,16)).trim(),
											(linea.substring(16,32)).trim(),
											(linea.substring(32,42)).trim(),
											(linea.substring(120,127)).trim(),
											acuse,con);
								} catch (NafinException ne)
								{	log.error(ne.getMessage()+ne);
								}
								log.debug("Linea:"+j+" procesada");
					     	}
						 } //Fin del for de  la lectura de las l�neas
			  			/* Actualiza el acuse */
			  			error= ActualizaAcuse(acuse,con);
		  	  			if ((error)&&(!acuse.equals("")))
			  			{  BorraAcuse(acuse,con);
			  			}
			  			if ((!error)&&(!acuse.equals("")))
			  			{		log.debug("Acuse generado:"+acuse);
		  	  			}
			  			error=false;
				     }
			  	 } /* Lectura de la primera linea */
		  	  }  else
		  	  { log.info("No se encontr� el archivo");
		  	    error=true;
			  } /* Procesamiento solo si existe el archivo */
		} catch (Exception e) {
			error=true;
			//log.info("Error en el proceso autom�tico de autorizaci�n: "+e);
			throw new NafinException("SIST0001");
		} finally{
			if (con.hayConexionAbierta()){
			  con.terminaTransaccion(!error);
			  con.cierraConexionDB();
			}
		}
		return acuse;
	} // Fin del metodo ConfirmacionAutomaticaIF



	public List ValidaConfirmacionAutomaticaIF(String ruta,String snombreArch,String ic_if,String ne)
		throws NafinException
	{
		AccesoDB con  = new AccesoDB();
        boolean error = false;
		boolean errorLinea = false;
		List 	alRetorno = new ArrayList();
		StringBuffer	sbCorrectos = new StringBuffer("");
		StringBuffer	sbErrores	= new StringBuffer("");
		int numCorrectos	= 0;
		int numErrores		= 0;
		StringBuffer numDoctos = new StringBuffer("");
		try {
			con.conexionDB();
			log.info("Inicio : Valida Confirmacion de Autorizacion IF");
            String linea = "";
            StringBuffer nombreArchivo= new StringBuffer ("");

			nombreArchivo.delete(0,100);
			nombreArchivo.append(ruta+snombreArch);
			log.debug("Procesando Arch:"+snombreArch);
			//validacion del nombre del archivo
			if(snombreArch.charAt(0)!='e'){
				sbErrores.append("El nombre del archivo no lleva el prefjo 'e'\n");
				error = true;
			}
			if(snombreArch.indexOf(".txt")<0){
				sbErrores.append("La extension del archivo debe ser txt\n");
				error = true;
			}
			try{
				int dia = Integer.parseInt(snombreArch.substring(1,3));
				int mes = Integer.parseInt(snombreArch.substring(3,5));
				if(dia <0 || dia > 31){
					sbErrores.append("El nombre del archivo no contiene un dia valido");
					error = true;
				}
				if(mes <0 || mes > 31){
					sbErrores.append("El nombre del archivo no contiene un mes valido");
					error = true;
				}
			}catch(Exception e){
				sbErrores.append("El nombre del archivo no indica la fecha del proceso\n");
				error = true;
			}
			if(snombreArch.indexOf(ne)<0){
				sbErrores.append("El nombre del archivo debe finalizar con: '"+ne+".txt'\n");
				error = true;
			}

			java.io.File f=new java.io.File(nombreArchivo.toString());
			if (f.exists()&&!error){
				BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			 	/* Extraccion de la primera linea */
				br.readLine();
					 for (int j=1; (linea=br.readLine())!=null; j++){
					 	String msgError = "Error en la linea "+j+":";
						errorLinea = false;
					 	if ((linea.length())>=125){
							String ic_epo		=	linea.substring(0,6);
							String num_sirac	=	linea.substring(6,13);
							String moneda		=	linea.substring(13,16).trim();
							String numDocto		=	linea.substring(16,32).trim();
							String fecha_venc	=	linea.substring(32,42).trim();
							//String estatus		=	linea.substring(120,127);

							try{
								Integer.parseInt(ic_epo.trim());
							}catch(Exception e){
								sbErrores.append(msgError+" La clave de la epo debe ser numerica\n");
								error = true;
								errorLinea = true;
							}
							try{
								if(num_sirac.trim().length()!=7){
									sbErrores.append(msgError+" La longitud del numero sirac debe ser siete digitos, favor de llenar con ceros a la izquierda\n");
									error = true;
									errorLinea = true;
								}
								Integer.parseInt(num_sirac.trim());
							}catch(Exception e){
								sbErrores.append(msgError+" El numero sirac del cliente es incorrecto\n");
								error = true;
								errorLinea = true;
							}
							if(	!"MXP".equals(moneda)&&!"USD".equals(moneda)){
								sbErrores.append(msgError+" La clave de la moneda es incorrecta\n");
								error = true;
								errorLinea = true;
							}

							try {
					            int ano = Integer.parseInt(fecha_venc.substring(0,4));
					            int mes = Integer.parseInt(fecha_venc.substring(5,7))-1;
					            int dia = Integer.parseInt(fecha_venc.substring(8,10));
					            Calendar fecha = new GregorianCalendar(ano, mes, dia);
					            int year = fecha.get(Calendar.YEAR);
					            int month = fecha.get(Calendar.MONTH);
					            int day = fecha.get(Calendar.DAY_OF_MONTH);

					            if((dia!=day) || (mes!=month) || (ano!=year)){
									error = true;
									errorLinea = true;
									sbErrores.append(msgError+ " La fecha no es valida o no esta en formato AAAA-MM-DD. \n");
								}
					        }catch(Exception e) {
								error = true;
								errorLinea = true;
								sbErrores.append(msgError+ " La fecha no es valida o no esta en formato AAAA-MM-DD. \n");
							}
							/*if(!"OK".equals(estatus)&&estatus.trim().length()<7){
								error = true;
								errorLinea = true;
								sbErrores.append(msgError+" El valor del campo estatus es incorrecto");
							}*/
							if(numDoctos.length()>0){
								numDoctos.append(",");
							}
							numDoctos.append("'"+numDocto+"'");

							if(!errorLinea){
								sbCorrectos.append("Linea "+j+": Numero de documento: "+numDocto+"\n");
								numCorrectos++;
							}else{
								numErrores++;
							}
				     	}else{
							sbErrores.append(msgError+" La longitud de la linea es menor a la necesaria\n");
							error = true;
							errorLinea = true;
						}
					 } //Fin del for de  la lectura de las l�neas
			 		error=false;
			}  else if(!error){
				sbErrores.append("Error al leer el archivo");
			    error=true;
			}
			int numReg = 0;
			String qrySentencia =
				" select count(1)"+
				" from com_documento d"+
				" ,com_docto_seleccionado ds"+
				" where d.ic_documento = ds.ic_documento"+
				" and d.ig_numero_docto in("+numDoctos.toString()+")"+
				" and ds.ic_if = "+ic_if+
				" and d.ic_estatus_docto = 24";
			ResultSet rs = con.queryDB(qrySentencia);
			if(rs.next()){
				numReg = rs.getInt(1);
			}
			con.cierraStatement();
			if(numCorrectos+numErrores!=numReg){
				sbErrores.append("Error: El archivo tiene numeros de documentos que no existen en el sistema\n");
				error = true;
			}
/*0*/		alRetorno.add(sbCorrectos);
/*1*/		alRetorno.add(sbErrores);
/*2*/		alRetorno.add(new Integer(numCorrectos));
/*3*/		alRetorno.add(new Integer(numErrores));
		} catch (Exception e)
		{	error=true;
			log.error("(ConfirmacionAutomaticaIF)::Error en el proceso autom�tico de autorizaci�n: ",e);
			throw new NafinException("DSCT0070");
		} finally{
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return alRetorno;
	} // Fin del metodo ConfirmacionAutomaticaIF



	public List PreAcuseImportarArchivo(String ruta,String snombreArch,String ic_if)
		throws NafinException
	{
		AccesoDB con  = new AccesoDB();
        boolean error = false;
		List 	alFilas = new ArrayList();
		List	alColumnas = null;
		StringBuffer numDoctosOpe	= new StringBuffer("");
		StringBuffer numDoctosNeg	= new StringBuffer("");
		Hashtable	 htErrores		= new Hashtable();
		try {
			con.conexionDB();
			log.info("PreAcuseImportarArchivo::Inicio : Acuse Importar Archivo");
            String linea = "";
            StringBuffer nombreArchivo= new StringBuffer ("");

			nombreArchivo.delete(0,100);
			nombreArchivo.append(ruta+snombreArch);
			//validacion del nombre del archivo


			java.io.File f=new java.io.File(nombreArchivo.toString());
			if (f.exists()){
				log.debug("SI ABRIO EL ARCHIVO Y  LO ESTA LEYENDO");

				BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				br.readLine();
				for (int j=1; (linea=br.readLine())!=null; j++){
					String numDocto		=	linea.substring(16,32).trim();
					String estatus		=	linea.substring(120,127);

					if("OK".equals(estatus.trim())){
						if(numDoctosOpe.length()>0){
							numDoctosOpe.append(",");
						}
						numDoctosOpe.append("'"+numDocto+"'");
					}else{
						if(numDoctosNeg.length()>0){
							numDoctosNeg.append(",");
						}
						numDoctosNeg.append("'"+numDocto+"'");
						htErrores.put(numDocto,"B"+estatus);
					}
				}
				StringBuffer qrySentencia = new StringBuffer("");
				if(numDoctosOpe.length()>0){
					qrySentencia.append(
						" select e.cg_razon_social as nombre_epo"+
						" ,p.cg_razon_social as nombre_pyme"+
						" ,d.ig_numero_docto"+
						" ,m.cd_nombre as moneda" +
						" ,d.fn_monto"+
						" ,to_char(d.df_fecha_venc,'dd/mm/yyyy') as fecha_venc"+
						" ,'Operado con Fondeo Propio' as estatus"+
						" ,m.ic_moneda"+
						" from com_documento d"+
						" ,com_docto_seleccionado ds"+
						" ,comcat_epo e"+
						" ,comcat_pyme p"+
						" ,comcat_moneda m"+
						" where d.ic_documento = ds.ic_documento"+
						" and d.ic_epo = e.ic_epo"+
						" and d.ic_pyme = p.ic_pyme"+
						" and d.ic_moneda = m.ic_moneda"+
						" and d.ic_estatus_docto = 24"+
						" and d.ig_numero_docto in("+numDoctosOpe.toString()+")"+
						" and ds.ic_if = "+ic_if
					);
				}

				if(numDoctosNeg.length()>0){
					if(qrySentencia.length()>0){
						qrySentencia.append(" UNION ALL ");
					}

					qrySentencia.append(
						" select e.cg_razon_social as nombre_epo"+
						" ,p.cg_razon_social as nombre_pyme"+
						" ,d.ig_numero_docto"+
						" ,m.cd_nombre as moneda" +
						" ,d.fn_monto"+
						" ,to_char(d.df_fecha_venc,'dd/mm/yyyy') as fecha_venc"+
						" ,'Negociable' as estatus"+
						" ,m.ic_moneda"+
						" from com_documento d"+
						" ,com_docto_seleccionado ds"+
						" ,comcat_epo e"+
						" ,comcat_pyme p"+
						" ,comcat_moneda m"+
						" where d.ic_documento = ds.ic_documento"+
						" and d.ic_epo = e.ic_epo"+
						" and d.ic_pyme = p.ic_pyme"+
						" and d.ic_moneda = m.ic_moneda"+
						" and d.ic_estatus_docto = 24"+
						" and d.ig_numero_docto in("+numDoctosNeg.toString()+")"+
						" and ds.ic_if = "+ic_if
					);
				}
				log.debug("PreAcuseImportarArchivo()::" + qrySentencia.toString());
				ResultSet rs = con.queryDB(qrySentencia.toString());
				while(rs.next()){
					String numErr = "";
					alColumnas = new ArrayList();
	/*0*/			alColumnas.add((rs.getString("nombre_epo")==null)?"":rs.getString("nombre_epo"));
	/*1*/			alColumnas.add((rs.getString("nombre_pyme")==null)?"":rs.getString("nombre_pyme"));
	/*2*/			alColumnas.add((rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto"));
	/*3*/			alColumnas.add((rs.getString("moneda")==null)?"":rs.getString("moneda"));
	/*4*/			alColumnas.add((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
	/*5*/			alColumnas.add((rs.getString("fecha_venc")==null)?"":rs.getString("fecha_venc"));
	/*6*/			alColumnas.add((rs.getString("estatus")==null)?"":rs.getString("estatus"));
					numErr = (String)htErrores.get(rs.getString("ig_numero_docto"));
					if(numErr==null)
	/*7*/					alColumnas.add("");
					else{
						try{
							throw new NafinException(numErr);
						}catch(NafinException ne){
	/*7*/						alColumnas.add(ne.getMsgError());
						}
					}
	/*8*/			alColumnas.add((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
					alFilas.add(alColumnas);

				}
				con.cierraStatement();
			}else{
				log.info("NO ABRIO EL ARCHIVO");
			}
		} catch (Exception e)	{
			error=true;
			e.printStackTrace();
			throw new NafinException("DSCT0070");
		} finally{
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return alFilas;
	} // Fin del metodo ConfirmacionAutomaticaIF




	public String OperaConFondeoPropio(String ic_if,String clavesDoctos)
		throws NafinException
	{
		AccesoDB con  = new AccesoDB();
        boolean error = false;
        String acuse="";
		ResultSet			rs = null;
		String				qrySentencia = null;
		try
		{	con.conexionDB();
			log.info("OperaConFondeoPropio(E)");
            ArrayList alFilas = null,alColumnas = null;

	  		acuse=GeneraAcuse(ic_if,con);
			if (!acuse.equals("")){
				qrySentencia =
					"select ic_epo"+
					" ,p.in_numero_sirac"+
					" ,decode(d.ic_moneda,1,'MXP',54,'USD') as moneda"+
					" ,d.ig_numero_docto"+
					" ,to_char(d.df_fecha_venc,'yyyy-mm-dd') as fecha_venc"+
					" from com_documento d"+
					" ,comcat_pyme p"+
					" where d.ic_pyme = p.ic_pyme"+
					" and d.ic_documento in("+clavesDoctos+")";
				rs = con.queryDB(qrySentencia);
				alFilas = new ArrayList();
				while(rs.next()){
					alColumnas = new ArrayList();
/*0*/				alColumnas.add((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
/*1*/				alColumnas.add((rs.getString("in_numero_sirac")==null)?"":rs.getString("in_numero_sirac"));
/*2*/				alColumnas.add((rs.getString("moneda")==null)?"":rs.getString("moneda"));
/*3*/				alColumnas.add((rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto"));
/*4*/				alColumnas.add((rs.getString("fecha_venc")==null)?"":rs.getString("fecha_venc"));
					alFilas.add(alColumnas);
				}
				con.cierraStatement();
				for(int i=0;i<alFilas.size();i++){
					alColumnas = (ArrayList)alFilas.get(i);
					try{
						ProcesaDocto(ic_if,
							alColumnas.get(0).toString(),
							alColumnas.get(1).toString(),
							alColumnas.get(2).toString(),
							alColumnas.get(3).toString(),
							alColumnas.get(4).toString(),
							"OK",acuse,con);
					} catch (NafinException ne){
						log.error("OperaConFondeoPropio(Error)" + ne.getMessage());
					}
				}

	  			/* Actualiza el acuse */
	  			error= ActualizaAcuse(acuse,con);
  	  			if ((error)&&(!acuse.equals(""))){
					BorraAcuse(acuse,con);
	  			}
	  			if ((!error)&&(!acuse.equals(""))){
						log.debug("Acuse generado:"+acuse);
  	  		}
	  			error=false;
			}
		} catch (Exception e){
			error=true;
			log.error("OperaConFondeoPropio(Exception)", e);
			throw new NafinException("DSCT0070");
		} finally{
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(!error);
				con.cierraConexionDB();
			}
		}
		return acuse;
	} // Fin del metodo ConfirmacionAutomaticaIF

	/* Agregado EGB */
	/**
	 * Genera la solicitud del descuento
	 * @param claveDocumento
	 * @param acuse
	 * @param claveEpo
	 * @param emisor
	 * @param montoMN
	 * @param montoInteresMN
	 * @param montoDsctoMN
	 * @param montoDL
	 * @param montoInteresDL
	 * @param montoDsctoDL
	 * @param claveUsuario
	 * @param reciboElectronico
	 * @param datosFondeo
	 * @param noCliente
	 * @param fechaCarga
	 * @param horaCarga
	 * @param horaCargaF
	 * @param nombreUsuario
	 * @param plazoMaxBO
	 * @return
	 * @throws com.netro.exception.NafinException
	 */
	public Hashtable generarSolicitudInterna(Vector claveDocumento, String acuse, String claveEpo,
											String emisor, String montoMN, String montoInteresMN,
											String montoDsctoMN, String montoDL, String montoInteresDL,
											String montoDsctoDL, String claveUsuario,
											String reciboElectronico, String datosFondeo, String noCliente,
											String fechaCarga, String horaCarga, String horaCargaF, String nombreUsuario,
											String plazoMaxBO, String strDirectorioTemp, String strPais,  String iNoNafinElectronico, 
											String strNombre, String strLogo, String strDirectorioPublicacion ) throws NafinException {
		
		Hashtable hProceSolicitud = new Hashtable();
		boolean error = false, bErrorEnlaceEnvio = false;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String qrySentencia = "";
		String icDocumento = "";
		String folio = "";
		String fechaDocumento = "";
		String fechaVencimiento = "";
		String diaVencimiento = "";
		String tasaIF = "";
		String claseDocumento = "";
		String montoDescuento = "";
	    String estatusSolic = "1";
		String sFechaVencDesc = "";
		String sPagoAntic = null;
		int i = 0;
		int numRegistrosMN = 0;
		int numRegistrosDL = 0;
		Vector vSolicitudes = null;
		StringBuffer contenidoArchivo = new StringBuffer();
		StringBuffer strArchivoVble = new StringBuffer();
		StringBuffer strArchivoFijo = new StringBuffer();
		StringBuffer strCabeza = new StringBuffer();
		StringBuffer strCabezaF = new StringBuffer();
		StringBuffer strPieFijo = new StringBuffer();
		StringBuffer strPieVble = new StringBuffer();
		int litipoPiso = 0;
		int iDiaPlazo = 0;
		String tipoFondeo, recorteSolic;
	   String nombreArchivoCSV ="", nombreArchivoPDF ="";

		AccesoDB con  = new AccesoDB();
		try	{
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
			Hashtable alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, 1);
			con.conexionDB();

			tipoFondeo = datosFondeo.substring(0,1);	//N normal, P Propio
			recorteSolic = datosFondeo.substring(2,3);	//S Si, N No  (En produccion no hay ninguna S, parece ser que no se ocupa)

      boolean bFactorajeVenc = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?true:false;
			boolean bFactorajeDist = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?true:false;
      boolean bFactorajeMand = ("S".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?true:false;
		boolean bOperaFactorajeVencInfonavit = ("S".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?true:false;//FODEA 042 - 2009 ACF
      //boolean bTipoFactoraje = (bFactorajeVenc || bFactorajeDist || bFactorajeMand)?true:false;
		boolean bTipoFactoraje = (bFactorajeVenc || bFactorajeDist || bFactorajeMand || bOperaFactorajeVencInfonavit)?true:false;//FODEA 042 - 2009 ACF
			String diaAnticipado = null;
			log.debug("bFactorajeVenc: " + bFactorajeVenc+" bFactorajeDist: " + bFactorajeDist + " bFactorajeMand: " + bFactorajeMand+ " bOperaFactorajeVencInfonavit: " + bOperaFactorajeVencInfonavit);//FODEA 042 - 2009 ACF
			log.debug("bTipoFactoraje: " + bTipoFactoraje);

			if (!hayClaseDoctoAsociado(claveEpo, con)) {	//No existe una clase de documento asociado para realizar Descuento?
				throw new NafinException("DSCT0020");
			}

			//Guarda el acuse de la transaccion
			setAcuse(acuse, montoMN, montoInteresMN, montoDsctoMN,
					montoDL, montoInteresDL, montoDsctoDL,
					claveUsuario, reciboElectronico, con);

			estatusSolic = "1"; //1=Seleccionada IF
			if("P".equals(tipoFondeo)){	// Cuando es fondeo Propio.
				estatusSolic = "10";	//Operada con fondeo propio
			}

			// Obtenemos el plazo de acuerdo al tipo de moneda, esto para saber cu�l se va a aplicar, foda 49.

			Hashtable hMonedaPlazo = new Hashtable();
			if(tipoFondeo.equals("N") || (tipoFondeo.equals("P") && recorteSolic.equals("S"))) { //Tipo de fondeo Normal (N) � Propio(P) con Recorte  ��??

				VectorTokenizer vt = new VectorTokenizer(plazoMaxBO,":");
				Vector vdatos = vt.getValuesVector();

				String sReg = "";
				for(int p=0; p<(vdatos.size()-1); p++) {
					sReg = vdatos.get(p).toString();
					hMonedaPlazo.put(new Integer(sReg.substring(0, sReg.indexOf("|"))), new Integer(sReg.substring(sReg.indexOf("|")+1, sReg.length())));			//????
				}
			}

			//Obtiene la parametrizaci�n de manejo de Notas de Cr�dito
			qrySentencia =
				" SELECT cs_opera_notas_cred"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND ic_epo = "+claveEpo+" "  ;
			ResultSet rsNC = con.queryDB(qrySentencia);
			String cs_opera_notas_cred = "";
			if (rsNC.next()) {
				cs_opera_notas_cred = rsNC.getString(1)==null?"":rsNC.getString(1);
			}
			rsNC.close();con.cierraStatement();

			// Fodea 002 - 2010
			// Verificar si la EPO tiene habilitado Aplicar Notas de Credito a Multiples Documentos
			qrySentencia =
				"SELECT "  +
				"  NVL(pz.cg_valor, 'N') AS CS_APLICAR_NOTAS_CRED "  +
				"FROM "  +
				"  com_parametrizacion_epo pz, "  +
				"  comcat_parametro_epo    pe "  +
				"WHERE "  +
				"  PE.CC_PARAMETRO_EPO    = 'CS_APLICAR_NOTAS_CRED' AND "  +
				"  pz.cc_parametro_epo(+) = pe.cc_parametro_epo AND "  +
				"  pz.ic_epo(+)           = "+claveEpo+" ";
			rsNC = con.queryDB(qrySentencia);
			String cs_aplicar_notas_cred = "";
			if (rsNC.next()) {
				cs_aplicar_notas_cred = rsNC.getString(1)==null?"":rsNC.getString(1);
			}
			rsNC.close();con.cierraStatement();


			Vector vDiasInhabiles = getDiasInhabiles(con);
			SimpleDateFormat sdfv = new SimpleDateFormat("dd/MM/yyyy");
			// MODIFICADO EGB 10/04/2003

/*
			Context context = ContextoJNDI.getInitialContext();
			ISeleccionDocumentoHome seleccionDocumentoHome = (ISeleccionDocumentoHome) context.lookup("SeleccionDocumentoEJB");
			ISeleccionDocumento BeanSeleccionDocumento = seleccionDocumentoHome.create();
*/

			String sFechaVencPyme = "";//BeanSeleccionDocumento.operaFechaVencPyme(claveEpo);


			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String bancoFondeo = "1";
			String bloqueo = "NULL";
			qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);



			qrySentencia =
				" SELECT TO_CHAR (sigfechahabilxepo (ic_epo, TRUNC (SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha,"   +
				"        ic_banco_fondeo"   +
				"   FROM comcat_epo"   +
				"  WHERE ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(claveEpo));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				if(!"P".equals(tipoFondeo)){	// Cuando es fondeo Propio.
					fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
				}
				bancoFondeo = rs.getString(2)==null?"":rs.getString(2);
			}
			rs.close();ps.close();

			//Si el Banco de Fondeo es Bancomext y el estatus de la solicitud esta como 1 (Seleccionado IF)
			if("2".equals(bancoFondeo) && "1".equals(estatusSolic)) {
				bloqueo = "6";	//???
				estatusSolic = "2";	//2= En proceso
			}

			qrySentencia = " select TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto "+
						" , TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') as df_fecha_venc "+
						" , TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd') as  diaVencimiento "+
						" , ds.in_importe_recibir " +
						" , t.ic_tasa "+
						" , pd.ic_clase_docto "+
						" , i.ig_tipo_piso "+
						" , (d.df_fecha_venc"+sFechaVencPyme+" - DECODE(d.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) as plazo "+
						" , ie.cs_pago_anticipado "+
						" , PAD.in_dia "+
						" , d.ic_estatus_docto " +
						" , d.ic_documento " +
						" , d.ic_moneda "+
						" , 'AutorizacionDescuentoBean.generarSolicitudInterna()' as pantalla "+
						" from com_documento d "+
						" , comcat_tasa t "+
						" , comrel_producto_docto pd "+
						" , com_docto_seleccionado ds "+
						" , comrel_tasa_autorizada ta "+
						" , comrel_tasa_base tb "+
						" , comrel_if_epo ie "+
						" , comcat_if i "+
						" , (select in_dia, in_plazo_minimo, in_plazo_maximo "+
						"   from com_pago_antic_detalle "+
						"   where ic_if = " + noCliente+
						"   and ic_epo = " + claveEpo+
						"   order by 2) PAD "+
						" where "+
						" ds.dc_fecha_tasa = ta.dc_fecha_tasa "+
						" and ds.ic_epo = ta.ic_epo "+
						" and d.ic_if = ta.ic_if "+
						" and ta.dc_fecha_tasa = tb.dc_fecha_tasa "+
						" and tb.ic_tasa = t.ic_tasa "+
						" and t.ic_moneda = d.ic_moneda "+
						" and d.ic_epo=pd.ic_epo "+
						" and pd.ic_producto_nafin=1 "+
						" and d.ic_documento in (" + Comunes.implode(",", claveDocumento) + ") "+
						" and d.ic_documento = ds.ic_documento"+
						" and d.ic_if = ie.ic_if " +
						" and ds.ic_epo = ie.ic_epo " +
						" and ie.ic_if = i.ic_if "+
						" and (trunc(d.df_fecha_venc) - trunc(sysdate)) between in_plazo_minimo (+) and in_plazo_maximo (+) ";

			log.debug("generarSolicitudInterna()::" + qrySentencia);
			rs = con.queryDB(qrySentencia);
			while (rs.next()) {
				if (rs.getInt("IC_ESTATUS_DOCTO") == 4) { //El ic_documento ya existe en solicitudes?  ic_estatus_docto = 4 (Operado)
					throw new NafinException("DSCT0019");
				}
				icDocumento = rs.getString("IC_DOCUMENTO");
				fechaDocumento = rs.getString("DF_FECHA_DOCTO");
				fechaVencimiento = rs.getString("DF_FECHA_VENC");
				diaVencimiento = rs.getString("DIAVENCIMIENTO");
				tasaIF = rs.getString("IC_TASA");
				claseDocumento = rs.getString("IC_CLASE_DOCTO");
				montoDescuento = rs.getString("IN_IMPORTE_RECIBIR");
				litipoPiso = rs.getInt("IG_TIPO_PISO");
				String ic_moneda = rs.getString("ic_moneda");
				iDiaPlazo = 0;
				iDiaPlazo = rs.getInt("PLAZO");
				int iDiasVenc = iDiaPlazo;
				sPagoAntic = (rs.getString("CS_PAGO_ANTICIPADO") == null)?"N":rs.getString("CS_PAGO_ANTICIPADO");
				diaAnticipado = (rs.getString("IN_DIA") == null)?"":rs.getString("IN_DIA"); // Agregado EGB 10/04/2003
				sFechaVencDesc = "";
				GregorianCalendar cFechaVencDesc = new GregorianCalendar();	// Por defecto siempre toma la fecha del d�a actual.
				// Modificado por HDG 14/10/2003, foda 49
				//Cambio Foda 046 - 2004 EGB
				//if(!tipoFondeo.equals("P")) {
				if(tipoFondeo.equals("N") || (tipoFondeo.equals("P") && recorteSolic.equals("S"))) {

					if(litipoPiso == 2) {	// Si el IF es de 2 piso se hace los c�lculos de los d�as de plazo.
						if ("S".equals(sPagoAntic)) { // Tiene pago anticipado?   -----Esta funcionalidad  de pago anticipado no se utiliza en produccion. no hay ninguna con S----
							if (!"".equals(diaAnticipado)) {  // Tiene Recompra?
								iDiaPlazo = rs.getInt("IN_DIA");	// Obtiene los d�as de recompra, el nuevo plazo.
								if(iDiasVenc > iDiaPlazo) {	// Si el plazo normal es mayor al plazo de la recompra?
									cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);	// Suma el plazo a la fecha de hoy.
									// Validamos si la fecha del plazo es d�a inhabil.
									Hashtable hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iDiaPlazo, "S");
									cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
									iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
									boolean bCambioPlazo = false;
									bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
									while(bCambioPlazo) {
										hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iDiaPlazo, "S");
										cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
										iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
										bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
									}
									sFechaVencDesc = sdfv.format(cFechaVencDesc.getTime());
									diaVencimiento = sdfv.format(cFechaVencDesc.getTime()).substring(0,2);
								} // iDiasVenc
							}
						}
					} //TipoPiso==2

					// Valida el plazo obtenido contra la Base de Operaci�n m�xima parametrizada, aplica para 1ro y 2do Piso.
					int iPlazoMaxBO = new Integer(hMonedaPlazo.get(new Integer(rs.getInt("IC_MONEDA"))).toString()).intValue();
					if(iDiaPlazo > iPlazoMaxBO) {
						cFechaVencDesc.add(Calendar.DATE, iPlazoMaxBO);
						// Validamos si la fecha del plazo es d�a inhabil.
						Hashtable hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO, "N");
						cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
						iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
						boolean bCambioPlazo = false;
						bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
						while(bCambioPlazo) {
							hNuevaFecha = setDiasInhabilesPlazo(vDiasInhabiles, cFechaVencDesc, iPlazoMaxBO, "N");
							cFechaVencDesc = (GregorianCalendar)hNuevaFecha.get("cFechaVencDesc");
							iDiaPlazo = Integer.parseInt(hNuevaFecha.get("iDiaPlazo").toString());
							bCambioPlazo = ((Boolean)hNuevaFecha.get("bCambioPlazo")).booleanValue();
						}
						sFechaVencDesc = sdfv.format(cFechaVencDesc.getTime());
						diaVencimiento = sdfv.format(cFechaVencDesc.getTime()).substring(0,2);
					}
				} // tipoFondeo
				String sFechaVencimiento = (sFechaVencDesc.equals(""))?fechaVencimiento:sFechaVencDesc;
				folio = getFolioSolicitud(noCliente,con);
				//folio = getFolioSolicitud(con); MODIFICADO MPCS FODA 095
				qrySentencia="insert into com_solicitud (" +
							" ic_folio, ic_documento, ic_esquema_amort, ic_tipo_credito," +
							" ic_oficina, cc_acuse, ig_plazo, cg_tipo_plazo, cg_perio_pago_cap," +
							" cg_perio_pago_int, cg_desc_bienes, ic_clase_docto," +
							" cg_domicilio_pago, ic_tasaif, cg_rmif, in_stif," +
							" ic_tabla_amort, in_numero_amort, df_ppc, df_ppi," +
							" df_v_documento, df_v_descuento, in_dia_pago," +
							" df_etc, cg_lugar_firma, ig_numero_contrato, ic_estatus_solic," +
							" df_fecha_solicitud, ic_bloqueo, cs_tipo_solicitud," +
							" cg_emisor, ic_banco_fondeo)" +
							" values ('" + folio + "', " + icDocumento + ", 1, 1, 90, '" + acuse + "'" +
							" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy') - DECODE("+ic_moneda+", 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy')) "+
//							" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy') - TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')" +
							" , 'D', 'Al Vto.', 'Al Vto.', ' ', " + claseDocumento + ", 'Mexico, D.F.'" +
							" , " + tasaIF + ", '+', 0, 1, 1" +
							" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
							" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
							" , TO_DATE('" + fechaVencimiento + "','dd/mm/yyyy')" +
							" , TO_DATE('" + sFechaVencimiento + "','dd/mm/yyyy')" +
							" , " + diaVencimiento + ", TO_DATE('" + fechaDocumento + "','dd/mm/yyyy')" +
							" , 'Mexico, D.F.', 0, "+estatusSolic+", sysdate, "+bloqueo+", 'C', '" + emisor + "', "+bancoFondeo+")";
					log.debug("\n\n"+qrySentencia);
				try {
					con.ejecutaSQL(qrySentencia);
					log.debug(new java.util.Date() + " Solicitud: " + folio + " insertada");
				}catch(SQLException sqle) {
					error = true;
					throw new NafinException("DSCT0061");
				}

				qrySentencia = "insert into com_amortizacion (ic_amortizacion, ic_folio, in_numero" +
								" , fn_importe, df_fecha)" +
								" values (seq_com_amortizacion_ic_amor.NEXTVAL, '" + folio + "'" +
								" , 1," + montoDescuento + ", TO_DATE('" + fechaVencimiento + "','dd/mm/yyyy'))";

				try {
					con.ejecutaSQL(qrySentencia);
					//con.cierraStatement();
				}catch(SQLException sqle) {
					error = true;
					throw new NafinException("DSCT0063");
				}

				//Actualizamos al documento con el estatus Operada (4)
				qrySentencia =
					"UPDATE COM_DOCUMENTO set ic_estatus_docto = 4 " +
					"where ic_documento = " + icDocumento;

				try {
					con.ejecutaSQL(qrySentencia);
					//con.cierraStatement();
				}catch(SQLException sqle) {
					error = true;
					throw new NafinException("DSCT0011");
				}

				//Actualiza las Notas de Credito Simples asociadas a la solicitud procesada
				if("S".equals(cs_opera_notas_cred) && !("S".equals(cs_aplicar_notas_cred)) ) {
					qrySentencia =
						"UPDATE COM_DOCUMENTO set ic_estatus_docto = 4 " +
						"where ic_docto_asociado = " + icDocumento;
					try {
						con.ejecutaSQL(qrySentencia);
					}catch(SQLException sqle) {
						error = true;
						throw new NafinException("DSCT0011");
					}
				// Fodea 002 - 2010
				//Actualiza las Notas de Credito Multiples asociadas a la solicitud procesada
				}else if("S".equals(cs_opera_notas_cred) && "S".equals(cs_aplicar_notas_cred)){
					qrySentencia =
						"UPDATE COM_DOCUMENTO set IC_ESTATUS_DOCTO = 4 " +
						"where IC_DOCUMENTO in ( select IC_NOTA_CREDITO from COMREL_NOTA_DOCTO where IC_DOCUMENTO = "+ icDocumento +" )";
					try {
						con.ejecutaSQL(qrySentencia);
					}catch(SQLException sqle) {
						error = true;
						throw new NafinException("DSCT0011");
					}
				}

			} // while
			rs.close();
			con.cierraStatement();


			boolean SIN_COMAS = false;
			vSolicitudes = getDoctosProcesadosC(acuse, claveEpo, noCliente, con);
			if(vSolicitudes.size() > 0) {
				
				
				
				HashMap parametros = new HashMap();
			   parametros.put("acuse", acuse);
			   parametros.put("ic_epo", claveEpo);
			   parametros.put("ic_if", noCliente);
				parametros.put("hidRecibo", reciboElectronico);
			   parametros.put("monto_descuento_mn", montoMN);
			   parametros.put("monto_interes_mn", montoInteresMN);
			   parametros.put("monto_operar_mn", montoDsctoMN);
				parametros.put("monto_descuento_dl", montoDL);
				parametros.put("monto_interes_dl", montoInteresDL);
			   parametros.put("monto_operar_dl", montoDsctoDL);
			   parametros.put("iNoUsuario", claveUsuario);
			   parametros.put("strNombreUsuario", nombreUsuario);
			   parametros.put("hidFechaCarga", fechaCarga);
			   parametros.put("hidHoraCarga", horaCarga);
				parametros.put("strDirectorioTemp", strDirectorioTemp); 
			   parametros.put("strPais", strPais);
			   parametros.put("iNoNafinElectronico", iNoNafinElectronico);          
			   parametros.put("strNombre", strNombre);
			   parametros.put("strLogo", strLogo);
			   parametros.put("strDirectorioPublicacion", strDirectorioPublicacion);  
			   log.info("--------------------------Inicio Generaci�n de Archivos CSV y PDF  --------------------------");
			                  
			   HashMap  datosArchCsv =  this.generaArchivosIFDesAuto (parametros, vSolicitudes, "CSV");
			   if(datosArchCsv.size()>0){
					nombreArchivoCSV = datosArchCsv.get("nombreArchivo").toString();
			      contenidoArchivo.append(datosArchCsv.get("contenidoArchivo"));
				}
			   HashMap  datosArchPDf =  this.generaArchivosIFDesAuto (parametros, vSolicitudes, "PDF");
			   if(datosArchPDf.size()>0){
					nombreArchivoPDF = datosArchPDf.get("nombreArchivo").toString();
				}
			   log.info("--------------------------Termina Generaci�n de Archivos CSV y PDF--------------------------");
			
				// Encabezado del archivo Variable.
				strCabeza.append("H,"+fechaCarga+","+horaCarga+",Documentos Autorizados\n");

				// Encabezado del archivo Fijo.
				strCabezaF.append("H"+fechaCarga+horaCargaF+"Documentos Autorizados\n");

				// Descripci�n de los Campos del Archivo Variable.
				strArchivoVble.append("\nD,Numero Folio,Numero Documento,Numero Acuse,Clave Oficina,Clave PYME,"+
									  "Numero Cliente SIRAC,Nombre PYME,Clave EPO,Nombre EPO,Clave IF,Nombre IF,"+
									  "Fecha Documento,Fecha Vencimiento,Clave Moneda,Moneda");
				if(bTipoFactoraje) strArchivoVble.append(",Tipo Factoraje");
				strArchivoVble.append(",Monto Documento,Porcentaje Descuento,Recurso Garantia,Monto Descuento,"+
									"Monto Interes,Monto Operar,Tasa Interes PYME,Plazo,Clave Estatus,Estatus,"+
									"Fecha Alta,Fecha Solicitud,Referencia,CG_CAMPO1,CG_CAMPO2,CG_CAMPO3,CG_CAMPO4,CG_CAMPO5,Tipo Solicitud");
				//if(bFactorajeDist){
				if(bFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
					strArchivoVble.append(",Beneficiario,Banco Beneficiario,Sucursal Beneficiaria,Cuenta Beneficiaria"+
										",% Beneficiario,Importe a Recibir del Beneficiario,Neto a Recibir PyME");
				}

				String estatus = "Operado";
				String tipoSolicitud = "Interna";
				String tipoFactoraje="", lineacd = "", lineacdfr = "";
				int MAX_NUM_CAMPOS_ADICIONALES = 5;
				int numCamposAdicionales = 0;

				double dblTotalMontoPesos = 0;
				double dblTotalDescuentoPesos = 0;
				double dblTotalOperarPesos = 0;
				double dblTotalRecursosPesos = 0;
				double dblTotalInteresPesos = 0;

				double dblTotalMontoDolares = 0;
				double dblTotalDescuentoDolares = 0;
				double dblTotalOperarDolares = 0;
				double dblTotalRecursosDolares = 0;
				double dblTotalInteresDolares = 0;

				numCamposAdicionales = getNumCamposAdicionalesC(claveEpo, con);
				// Cuerpo de los Archivos "las Solicitudes".
				for (i=0; i < vSolicitudes.size(); i++) {
					Vector registro = (Vector)vSolicitudes.get(i);
					String numSirac = registro.get(0).toString();
					String nombrePyme = registro.get(1).toString();
					String numDocumento = registro.get(2).toString();
					fechaDocumento = registro.get(3).toString();
					fechaVencimiento = registro.get(4).toString();
					String nombreMoneda = registro.get(6).toString();
					String monto = registro.get(7).toString();
					montoDescuento = registro.get(8).toString();
					String importeInteres = registro.get(11).toString();
					String importeRecibir = registro.get(12).toString();
					String tasaAceptada = registro.get(13).toString();
					String plazo = registro.get(14).toString();
					folio = registro.get(17).toString();
					String numProveedorInterno = registro.get(15).toString();
					String porcentajeAnticipo = registro.get(9).toString();
					String recursoGarantia = registro.get(10).toString();
					String descuentoEspecial = registro.get(16).toString();

					String cc_acuse = registro.get(18).toString();
					String claveOficina = registro.get(19).toString();
					String nombreEpo = registro.get(21).toString();
					String claveIf = registro.get(22).toString();
					String nombreIf = registro.get(23).toString();
					String claveMoneda = registro.get(5).toString();
					String claveEstatusDocumento = registro.get(28).toString();
					String fechaAlta = registro.get(24).toString();
					String fechaSolicitud = registro.get(25).toString();
					String referencia = registro.get(27).toString();
					//String referenciaFide = registro.get(37).toString();
					lineacd = "";
					lineacdfr = "";
					for (int j = 1; j <= MAX_NUM_CAMPOS_ADICIONALES; j++) {
						if (j <= numCamposAdicionales) {
							lineacd +=  (registro.get(37 + j).toString()).replace(',',' ') + ",";
							lineacdfr += Comunes.formatoFijo(registro.get(37 + j).toString(),50," ","A");
						} else {
							lineacd += ",";
							lineacdfr += Comunes.formatoFijo(" ",50," ","A");
						}
					}

          tipoFactoraje = registro.get(36).toString();

					/*if(bFactorajeVenc) // Para Factoraje Vencido
						tipoFactoraje = (descuentoEspecial.equals("N"))?"Normal": (descuentoEspecial.equals("V"))?"Vencido" : (descuentoEspecial.equals("D"))?"Distribuido":"";*/

					
					// Cuerpo del Archivo Variable.
					strArchivoVble.append("\nD,"+folio+","+numDocumento+","+cc_acuse+","+
										  claveOficina+","+numProveedorInterno+","+numSirac+","+
										  nombrePyme.replace(',',' ')+","+claveEpo+","+
										  nombreEpo.replace(',',' ')+","+claveIf+","+
										  nombreIf.replace(',',' ')+","+fechaDocumento +","+
										  fechaVencimiento+","+claveMoneda+","+nombreMoneda+",");

					// Cuerpo del Archivo Fijo.
					strArchivoFijo.append("\nD"+Comunes.formatoFijo(folio,11," ","A") +
										  Comunes.formatoFijo(numDocumento,15," ","A") +
										  Comunes.formatoFijo(cc_acuse,14," ","A") +
										  Comunes.formatoFijo(claveOficina,2," ","A") +
										  Comunes.formatoFijo(numProveedorInterno,25," ","A") +
										  Comunes.formatoFijo(numSirac,7,"0","") +
										  Comunes.formatoFijo(nombrePyme,100," ","A") +
										  Comunes.formatoFijo(claveEpo,6," ","A") +
										  Comunes.formatoFijo(nombreEpo,100," ","A") +
										  Comunes.formatoFijo(claveIf,6," ","A") +
										  Comunes.formatoFijo(nombreIf,100," ","A") +
										  Comunes.formatoFijo(fechaDocumento,10," ","") +
										  Comunes.formatoFijo(fechaVencimiento,10," ","") +
										  Comunes.formatoFijo(claveMoneda,3," ","A") +
										  Comunes.formatoFijo(nombreMoneda,30," ","A"));

				//Se omite la discriminacion de tipo de factoraje para la creacion del archivo fijo
						strArchivoFijo.append(Comunes.formatoFijo(descuentoEspecial,1," ","A"));
					if(bTipoFactoraje) {						
						strArchivoVble.append(tipoFactoraje+",");
					}


					strArchivoVble.append(Comunes.formatoDecimal(monto,2,SIN_COMAS)+","+
								  Comunes.formatoDecimal(porcentajeAnticipo,0,SIN_COMAS)+","+
								  Comunes.formatoDecimal(recursoGarantia,2,SIN_COMAS)+","+
								  Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS)+","+
								  Comunes.formatoDecimal(importeInteres,2,SIN_COMAS)+","+
								  Comunes.formatoDecimal(importeRecibir,2,SIN_COMAS)+","+
								  Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS)+","+
								  plazo+","+claveEstatusDocumento+","+estatus+","+
								  fechaAlta+","+fechaSolicitud+","+referencia+","+
								  lineacd +tipoSolicitud);

					strArchivoFijo.append(Comunes.formatoFijo(Comunes.formatoDecimal(monto,2,SIN_COMAS),15,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(porcentajeAnticipo,0,SIN_COMAS),3,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(recursoGarantia,2,SIN_COMAS),15,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),15,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(importeInteres,2,SIN_COMAS),15,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(importeRecibir,2,SIN_COMAS),15,"0","") +
										  Comunes.formatoFijo(Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS),9,"0","") +
										  Comunes.formatoFijo(plazo,3,"0","") +
										  Comunes.formatoFijo(claveEstatusDocumento,2," ","A") +
										  Comunes.formatoFijo(estatus,20," ","A") +
										  Comunes.formatoFijo(fechaAlta,10," ","") +
										  Comunes.formatoFijo(fechaSolicitud,10," ","") +
										  Comunes.formatoFijo(referencia,50," ","A") +
										  lineacdfr + Comunes.formatoFijo(tipoSolicitud,7," ","A"));

					//if(bFactorajeDist){
					if(bFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
						String beneficiario = registro.get(29).toString();
						String bancoBeneficiario = registro.get(30).toString();
						String sucursalBeneficiario = registro.get(31).toString();
						String cuentaBeneficiario = registro.get(32).toString();
						String porcBeneficiario = registro.get(33).toString();
						String importeRecibirBenef = registro.get(34).toString();
						String netoRecibirPyme = registro.get(35).toString();

						
						strArchivoVble.append(","+beneficiario.replace(',', ' ')+","+//FODEA 042 - 2009 ACF
											bancoBeneficiario.replace(',', ' ')+","+//FODEA 042 - 2009 ACF
											sucursalBeneficiario+","+
											cuentaBeneficiario+","+
											porcBeneficiario+","+
											importeRecibirBenef+","+
											netoRecibirPyme);

						strArchivoFijo.append(Comunes.formatoFijo(beneficiario,60," ","")+
											Comunes.formatoFijo(bancoBeneficiario,40," ","")+
											Comunes.formatoFijo(sucursalBeneficiario,40," ","")+
											Comunes.formatoFijo(cuentaBeneficiario,15," ","")+
											Comunes.formatoFijo(porcBeneficiario ,6,"0","2")+
											Comunes.formatoFijo(importeRecibirBenef,19,"0","2")+
											Comunes.formatoFijo(netoRecibirPyme,19,"0","2"));
					}
					//contenidoArchivo.append(","+referenciaFide);

					if (claveMoneda.equals("1")) {
						numRegistrosMN++;
						dblTotalMontoPesos += new Double(monto).doubleValue();
						dblTotalDescuentoPesos += new Double(montoDescuento).doubleValue();
						dblTotalOperarPesos += new Double(importeRecibir).doubleValue();
						dblTotalRecursosPesos += new Double(recursoGarantia).doubleValue();
						dblTotalInteresPesos += new Double(importeInteres).doubleValue();
					} else if (claveMoneda.equals("54")) {
						numRegistrosDL++;
						dblTotalMontoDolares += new Double(monto).doubleValue();
						dblTotalDescuentoDolares += new Double(montoDescuento).doubleValue();
						dblTotalOperarDolares += new Double(importeRecibir).doubleValue();
						dblTotalRecursosDolares += new Double(recursoGarantia).doubleValue();
						dblTotalInteresDolares += new Double(importeInteres).doubleValue();
					}

				} // fin del for.

				strPieFijo.append("\nT" +
								Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,SIN_COMAS), 12, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,SIN_COMAS), 12, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,SIN_COMAS), 15, "0","") +
								Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,SIN_COMAS), 15, "0","") );

				// Guarda archivo variable.
				strPieVble.append("\nT," +
								"Total Registros MN," + Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,SIN_COMAS), 12, "0","") + "," +
								"Total Monto Documentos MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,SIN_COMAS), 15, "0","") + "," +
								"Total Recursos Garantia MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,SIN_COMAS), 15, "0","") + "," +
		 						"Total Monto Descuento MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,SIN_COMAS), 15, "0","") + "," +
								"Total Monto Interes MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,SIN_COMAS), 15, "0","") + "," +
								"Total Monto Operar MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,SIN_COMAS), 15, "0","") + "," +
								"Total Registros DL," + Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,SIN_COMAS), 12, "0","") + "," +
								"Total Monto Documentos DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,SIN_COMAS), 15, "0","") + "," +
		 						"Total Recursos Garantia DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,SIN_COMAS), 15, "0","") + "," +
								"Total Monto Descuento DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,SIN_COMAS), 15, "0","") + "," +
								"Total Monto Interes DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,SIN_COMAS), 15, "0","") + "," +
								"Total Monto Operar DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,SIN_COMAS), 15, "0","") );

				// Para el envio autom�tico del archivo.
				try {
					String sNombreTabla = "";
					qrySentencia = "select cg_tabla_enlace from comcat_if "+
									"where ic_if = "+noCliente;
					rs = con.queryDB(qrySentencia);
					if(rs.next()) {
						if(rs.getString(1)!=null) {
							sNombreTabla = rs.getString(1).trim();
							int iLonguitudArchivo = contenidoArchivo.length();
							int iTamanok = 31999;
							int iIniciok = 0, iFink = 0;
							String sPiezaArch = "";
							CallableStatement cs = con.ejecutaSP("SP_INSERTA_LOB2(?,?,?,?,?,?,?)");
							for(int v=0; v <= (int)(contenidoArchivo.length()/iTamanok); v++) {
								iFink = ((iTamanok*(v+1))+(v==0?0:1));
								sPiezaArch = contenidoArchivo.substring(iIniciok, (iFink<iLonguitudArchivo?iFink:iLonguitudArchivo) );
								iIniciok = iFink + 1;
								cs.setString(1, sNombreTabla);
								cs.setString(2, "df_autorizacion, cc_acuse");
								cs.setString(3, "tt_contenido");
								cs.setString(4, "'"+fechaCarga+" "+horaCarga+"','"+acuse+"'");
								cs.setString(5, sPiezaArch);
								cs.setString(6, "df_autorizacion='"+fechaCarga+" "+horaCarga+"' and cc_acuse='"+acuse+"'");
								cs.setInt(7, v);
								cs.execute();
							}
							if(cs!=null) cs.close();
							con.cierraStatement();
						} // if
					} // if
					rs.close();
					con.cierraStatement();

				} catch(SQLException sqle) {	// Si hay alg�n error que continue con el proceso.
					//throw new NafinException("DSCT0061");
					sqle.printStackTrace();
					bErrorEnlaceEnvio = true;	// Bandera para el envio del Popup.
				}
			}// if vSolicitudes.
		} catch (NafinException ne) {
			error = true;
			throw ne;
		} catch (Exception e) {
			error = true;
			log.error("Error generarSolicitudInterna: ", e);
			throw new NafinException("SIST0001");
		} finally {
			if (error == true)
				con.terminaTransaccion(false);
			else {
				con.terminaTransaccion(true);
				log.debug("EL VECTOR DE SOLICITUDES = "+vSolicitudes);
				hProceSolicitud.put("vSolicitudes", vSolicitudes);
				hProceSolicitud.put("numRegistrosMN", new Integer(numRegistrosMN));
				hProceSolicitud.put("numRegistrosDL", new Integer(numRegistrosDL));
				hProceSolicitud.put("bErrorEnlaceEnvio", new Boolean(bErrorEnlaceEnvio));				
				hProceSolicitud.put("strArchivoVble", strCabeza.toString() + strArchivoVble.toString() + strPieVble.toString());
				hProceSolicitud.put("strArchivoFijo", strCabezaF.toString() + strArchivoFijo.toString() + strPieFijo.toString());
			   hProceSolicitud.put("nombreArchivoCSV", nombreArchivoCSV); 
			               hProceSolicitud.put("nombreArchivoPDF", nombreArchivoPDF);     
			}
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return hProceSolicitud;
	}

	// M�todo getDiasInhabiles() agregado por HDG 14/10/2003, foda 49.

	private Vector getDiasInhabiles(AccesoDB con) throws NafinException {
	Vector vDiasInhabiles = new Vector();
		try {
			String query = "select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null ";
			try {
				ResultSet rs = con.queryDB(query);
				while(rs.next())
					vDiasInhabiles.addElement((rs.getString(1)==null?"":rs.getString(1).trim()));
				rs.close();
				con.cierraStatement();
			} catch(SQLException sqle) {
				throw new NafinException("DSCT0041");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			log.error("getDiasInhabiles(Error). ", e);
			throw new NafinException("SIST0001");
		}
	return vDiasInhabiles;
	}

	// M�todo setDiasInhabilesPlazo() agregado por HDG 14/10/2003, foda 49.

	private Hashtable setDiasInhabilesPlazo(Vector vDiasInhabiles, GregorianCalendar cFechaVencDesc,
											int iDiaPlazo, String sIncremento) throws NafinException {
	Hashtable hNuevoPlazo = new Hashtable();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
	boolean bCambioPlazo = false;
		try {
			for(int d=0; d<vDiasInhabiles.size(); d++) {
				if(vDiasInhabiles.get(d).toString().equals(sdf.format(cFechaVencDesc.getTime()))) {
					cFechaVencDesc.add(Calendar.DATE, (sIncremento.equals("S")?1:-1));	//Mas o	Menos un dia.
					if(sIncremento.equals("S")) iDiaPlazo++; else iDiaPlazo--;
					bCambioPlazo = true;
				}
			}
			int dia = cFechaVencDesc.get(Calendar.DAY_OF_WEEK);
			if(dia == 1) { // Domingo
				cFechaVencDesc.add(Calendar.DATE, (sIncremento.equals("S")?1:-2)); //Mas o Menos un dia.
				if(sIncremento.equals("S")) iDiaPlazo++; else iDiaPlazo-=2;
				bCambioPlazo = true;
			}
			if(dia == 7) { // S�bado
				cFechaVencDesc.add(Calendar.DATE, (sIncremento.equals("S")?2:-1)); //Mas o Menos dos dias.
				if(sIncremento.equals("S")) iDiaPlazo+=2; else iDiaPlazo--;
				bCambioPlazo = true;
			}
			hNuevoPlazo.put("cFechaVencDesc", cFechaVencDesc);
			hNuevoPlazo.put("iDiaPlazo", new Integer(iDiaPlazo));
			hNuevoPlazo.put("bCambioPlazo", new Boolean(bCambioPlazo));
		} catch(Exception e) {
			e.printStackTrace();
			log.error("setDiasInhabilesPlazo(Error) " + e.getMessage());
			throw new NafinException("SIST0001");
		}
	return hNuevoPlazo;
	}

	/* M�todo getFechasConvenioyDescuento() agregado por HDG el 03/06/2003 */
	public Vector getFechasConvenioyDescuento(String sNoIf, String sProducto) throws NafinException {
		log.info("getFechasConvenioyDescuento(E)");
		Vector vFechas = new Vector();
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			String query = "Select to_char(df_convenio,'dd') " +
							"		||' de '|| " +
							"		decode(to_char(df_convenio,'mm'), '01','Enero', " +
							"										  '02','Febrero'," +
							"										  '03','Marzo'," +
							"										  '04','Abril'," +
							"										  '05','Mayo'," +
							"										  '06','Junio'," +
							"										  '07','Julio'," +
							"										  '08','Agosto'," +
							"										  '09','Septiembre'," +
							"										  '10','Octubre'," +
							"										  '11','Noviembre'," +
							"										  '12','Diciembre') "+
							"		||' de '||" +
							"		to_char(df_convenio,'YYYY'), "+
							"		to_char(sysdate,'dd') " +
							"		||' de '|| " +
							"		decode(to_char(sysdate,'mm'), '01','Enero', " +
							"									  '02','Febrero'," +
							"									  '03','Marzo'," +
							"									  '04','Abril'," +
							"									  '05','Mayo'," +
							"									  '06','Junio'," +
							"									  '07','Julio'," +
							"									  '08','Agosto'," +
							"									  '09','Septiembre'," +
							"									  '10','Octubre'," +
							"									  '11','Noviembre'," +
							"									  '12','Diciembre') "+
							"		||' de '||" +
							"		to_char(sysdate,'YYYY') "+
							"  From comrel_producto_if " +
							" Where ic_if = ? " +
							"  and ic_producto_nafin = ? ";
			try{
				PreparedStatement pstmt = con.queryPrecompilado( query );
				pstmt.setInt(1,Integer.parseInt(sNoIf));
				pstmt.setInt(2,Integer.parseInt(sProducto));

				ResultSet rs = pstmt.executeQuery();
				pstmt.clearParameters();
				if (rs.next()) {
					vFechas.addElement(rs.getString(1));
					vFechas.addElement(rs.getString(2));
				}
				rs.close();
				if(pstmt != null) pstmt.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0080"); }

		}
		catch(NafinException ne) {
			log.error("getFechasConvenioyDescuento(Error)" + ne.getMessage());
			throw ne;
		}
		catch(Exception e) {
			log.error("getFechasConvenioyDescuento(Error)", e);
			throw new NafinException("SIST0001");
		}
		finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getFechasConvenioyDescuento(S)" );
		}
	return vFechas;
	}

	/* M�todo getNombreyPuesto() agregado por HDG el 03/06/2003 */
	public Vector getNombreyPuesto(String sNoIf) throws NafinException {
		Vector vDatos = new Vector();
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			String query = " Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
			  				"		cg_puesto " +
							" From com_personal_facultado " +
							" Where ic_if = " + sNoIf +
							" And ic_producto_nafin = 1";
			try{
				ResultSet rs = con.queryDB(query);
				if (rs.next()) {
					vDatos.addElement(rs.getString(1));
					vDatos.addElement(rs.getString(2));
				}
				rs.close();
			} catch(SQLException sqle) { throw new NafinException("DSCT0081"); }
			con.cierraStatement();

		}
		catch(NafinException ne) {	throw ne; }
		catch(Exception e) {
			log.error("getNombreyPuesto()", e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDatos;
	}


	/**
	 * Obtiene los plazos de la base de operaci�n para aplicarlo a la solicitud de descuento.
	 * @param sNoEPO Clave de la Epo
	 * @param sNoIF Clave del IF
	 * @param iNoMoneda Arreglo con las clave de moneda.
	 * @return Objeto con dos elementos.
	 * 		sPlazoBO Cadena con el formato: <i>claveMoneda1</i>|<i>Plazo1</i>:<i>claveMoneda2</i>|<i>Plazo2</i>:
	 *    sMsgError Mensaje de Error por no haber encontrado tasas de operacion
	 * @throws com.netro.exception.NafinException
	 */
	public Hashtable getPlazoBO(String sNoEPO, String sNoIF, int[] iNoMoneda) throws NafinException {
		log.info("getPlazoBO(E)");
		AccesoDB con = new AccesoDB();
		Hashtable hPlazoBO = new Hashtable();
		StringBuffer sbPlazoMaxBO = new StringBuffer("");
		String query = "", sMsgError = "";
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			int iTipoPiso = 0;
			try {
				query = " select decode(k.ig_tipo_piso, 1, n.plazoMax, 2, decode(l.cg_conv_prov, 'N', m.plazoMaxGral, 'S', n.plazoMax, m.plazoMaxGral)) as Plazo, k.ig_tipo_piso "+
						" from  (select nvl(max(bo.ig_plazo_maximo),0) as plazoMax "+
								"  from comrel_base_operacion bo, comrel_if_epo ie, comcat_if i, comcat_epo e "+
								" where ie.ic_epo = bo.ic_epo "+
								"	and ie.ic_if = i.ic_if "+
								"	and ie.ic_epo = e.ic_epo "+
								"	and bo.ig_tipo_cartera = i.ig_tipo_piso "+
								"	and bo.cs_tipo_plazo = 'F' "+
								"	and bo.ic_producto_nafin = 1 "+
								"	and bo.ic_moneda = ? "+
								"	and bo.ic_epo = ? "+
								"	and i.ic_if = ? ) n, "+
								"(select decode( ? , 1, ig_plazo_maximo_factoraje, 54, ig_plazo_maximo_factoraje_dl) as plazoMaxGral "+
								"   from comcat_producto_nafin where ic_producto_nafin = 1) m, "+
								"(select cg_conv_prov from comcat_epo where ic_epo = ? ) l,"+
								"(select ig_tipo_piso from comcat_if where ic_if = ? ) k";
				ps = con.queryPrecompilado(query);

				for(int i=0; i < iNoMoneda.length; i++) {
					ps.clearParameters();
					ps.setInt(1, iNoMoneda[i]);
					ps.setInt(2, Integer.parseInt(sNoEPO));
					ps.setInt(3, Integer.parseInt(sNoIF));
					ps.setInt(4, iNoMoneda[i]);
					ps.setInt(5, Integer.parseInt(sNoEPO));
					ps.setInt(6, Integer.parseInt(sNoIF));

					rs = ps.executeQuery();
					if(rs.next()) {
						iTipoPiso = rs.getInt("ig_tipo_piso");
						if(rs.getInt("Plazo")!=0) {
							sbPlazoMaxBO.append(iNoMoneda[i]+"|"+rs.getString("Plazo")+":");
						} else { // Si no obtuvo ning�n plazo entonces de acuerdo al tipo de piso del if se asigna el mensaje de error.
							if(iTipoPiso==1)
								sMsgError = "Clave 1008, Favor de revisar parametros de bases de operaci�n.";
							if(iTipoPiso==2)
								sMsgError = "Clave 1008, Favor de revisar parametros de bases de operaci�n. Epo: " + sNoEPO + ", Moneda: " + iNoMoneda[i];
						}
					}
					rs.close();
				} // for
				if(ps!=null) ps.close(); 
				 
				hPlazoBO.put("sPlazoBO", sbPlazoMaxBO.toString());
				hPlazoBO.put("sMsgError", sMsgError);
			} catch(SQLException sqle) {
				throw new NafinException("DSCT0085");
			}
		} catch(NafinException ne) {
			log.error("getPlazoBO(Error)" + ne.getMessage());
			throw ne;
		} catch(Exception e) {
			log.error("getPlazoBO(Error)", e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("getPlazoBO(S)");
		}
		return hPlazoBO;
	}


	public void validaAutOperSF(String ic_if)
		throws NafinException{
		log.info("validaAutOperSF (E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet			rs = null;
		String 				qrySentencia = "";
		boolean				csAutOperSF = false;
		try{
			con.conexionDB();
			qrySentencia =
				" SELECT CS_AUT_OPER_SF FROM COMREL_PRODUCTO_IF "+
				" WHERE IC_IF = ? "+
				" AND IC_PRODUCTO_NAFIN = 1 ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if(rs.next()){
				csAutOperSF = "S".equals(rs.getString("CS_AUT_OPER_SF"));
			}
			rs.close();
			ps.close();
			if(!csAutOperSF)
				throw new NafinException("GRAL0009");

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("validaAutOperSF (S)");
		}

	}

	public List getFechasEnProceso(String ic_if)
		throws NafinException{
		log.info("getFechasEnProceso (E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		String 				qrySentencia	= "";
		List				alFechas		= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia =
				" select distinct to_char(ds.df_fecha_seleccion,'dd/mm/yyyy') as fecha_sel "+
				" from com_documento d "+
				" , com_docto_seleccionado ds "+
				" where d.ic_documento = ds.ic_documento "+
				" and d.ic_estatus_docto = 24 "+
				" and d.cs_dscto_especial !='C'"+
				" and ds.ic_if = ? ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			while(rs.next()){
				alFechas.add((rs.getString("fecha_sel")==null)?"":rs.getString("fecha_sel"));
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFechasEnProceso (S)");
		}
		return alFechas;
	}

	public void procesaDoctos(OperadosEnlace enlace,String claveIF) throws NafinException {

		log.info("procesaDoctos(E)");

		// Configurar conexion

	   AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= null;
		Registros		registros		= null;
		Registros		documento		= null;
		boolean			hayExito 		= true;

		// ACTUALIZAR ESTATUS EN 24 DE LOS DOCUMENTOS EN COM_DOCUMENTO QUE SE ENCUENTREN
		// EN LA TABLA DE ENLACE CORRESPONDIENTE
		// EXTRAER IC_DOCUMENTOS DE LA TABLA COMTMP_DOCTOS_OPER_BANAMEX QUE CUMPLA CON LAS
		hayExito = true;
		try {
				con.conexionDB();
				qrySentencia 	=
					 "SELECT  " +
	             "		O.IC_DOCUMENTO  " +
					 "FROM " +
	             "		COM_DOCUMENTO D, " + enlace.getTablaDocumentos() + " O " +
					 "WHERE " +
					 "    D.IC_ESTATUS_DOCTO 	= ? 					AND " +
					 "    O.IC_DOCUMENTO 		= D.IC_DOCUMENTO 	AND " +
                "		O.DF_FECHA_OPER  		IS NULL 				AND " +
                "    O.CC_ACUSE       		IS NULL 				    ";


				lVarBind 		= new ArrayList();
				lVarBind.add(new Integer(3));

				registros 		= con.consultarDB(qrySentencia, lVarBind, false);

		}catch(Exception e) {
			log.error("procesaDoctos(Error)", e);
			log.error("Al extraer ic_documento de la tabla: " + enlace.getTablaDocumentos() );
			hayExito = false;
			throw new NafinException("SIST0001");
		}finally{
			if(!hayExito && con.hayConexionAbierta()){
				con.cierraConexionDB();
				log.info("procesaDoctos(S)");
			}
		}

		// ACTUALIZAR A 24 STATUS DE LOS DOCUMENTOS CUYO IC FUE PREVIAMENTE EXTRAIDO
		while(registros != null && registros.next()){

			String ic_documento = null;

			try{
				hayExito 		= true;
				ic_documento 	= registros.getString("IC_DOCUMENTO");

				// Realizar update de estatus documento
				qrySentencia =
				     "UPDATE " +
	              "   COM_DOCUMENTO " +
	              "SET " +
	              "	IC_ESTATUS_DOCTO 	= ? " + // 24
	              "WHERE " +
	              "   IC_DOCUMENTO 		= ? AND " +
					  "	IC_ESTATUS_DOCTO 	= ? ";

				lVarBind.clear();
				lVarBind.add(new Integer(24));
				lVarBind.add(new Integer(ic_documento));
				lVarBind.add(new Integer(3));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);

			} catch(Exception e) {
				log.error("procesaDoctos(Error)", e);
				log.error(" El Documento cuyo ID es: " + ic_documento + " no pudo ser actualizado a estatus 24");
				hayExito = false;
			} finally {
				con.terminaTransaccion(hayExito);
			}

		}

		// EXTRAER REGISTROS CON ESTATUS "OK"
		hayExito = true;
		try {
				enlace.generaQueryDeDocumentos();
				qrySentencia 	= enlace.getQuery();
				lVarBind 		= enlace.getListaDeVariablesBind();
				documento		= con.consultarDB(qrySentencia, lVarBind, false);
		}catch(Exception e) {
				log.error("procesaDoctos(Error)", e);
				hayExito = false;
				throw new NafinException("SIST0001");
		}finally{
			if(!hayExito && con.hayConexionAbierta()) con.cierraConexionDB();
		}

		// GENERAR ACUSE
		String acuse = null;
		try {

			acuse	= GeneraAcuse(claveIF,con);
			if(acuse == null)
				throw new Exception("El acuse no puede ser null");

		}catch(Exception e){
			log.error("procesaDoctos(Error)", e);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}

		// PROCESAR DOCUMENTOS
		boolean	hayAlMenosUnRegistro 	= false;
		while(documento.next())
		{
			hayAlMenosUnRegistro				= true;

			String sc_if 				= documento.getString("SC_IF");
			String sc_epo 				= documento.getString("SC_EPO");
			String sc_sirac			= documento.getString("SC_SIRAC");
			String sc_moneda			= documento.getString("SC_MONEDA");
			String ig_numero_docto	= documento.getString("IG_NUMERO_DOCTO");
			String sf_vencimiento	= documento.getString("SF_VENCIMIENTO");
			String sc_estatus			= documento.getString("SC_ESTATUS");

			boolean 		error					=	false;
			int			ic_docto				=	0;
			int			ic_epo				=	0;
			int			ic_if					=	0;
			String 		aux					=	null;
			String 		query					=	"";
			String 		estatusSolic		=	"1";
			String 		porcentajeFondeo	=	"";

			int ic_moneda = Integer.parseInt(sc_moneda);

			if(ic_moneda != 1 && ic_moneda != 54){
				ic_moneda = 0;
			}

			try{   /* Verificaciones de datos */

				aux=sf_vencimiento;

				try{

					if ((!Comunes.checaFecha(aux)) || (ic_moneda==0))	{
						error = true;
					}
					ic_epo	=	Integer.parseInt(sc_epo);
					ic_if		=	Integer.parseInt(sc_if);
				}catch (Exception e){
					e.printStackTrace();
					throw new NafinException("DSCT0012");
				}

				if (!error){ //log.info(ic_if+" "+ic_epo+" "+ic_moneda+" "+ig_numero_docto);
				  /* Busqueda del documento */
				  query =
				  		"SELECT "  +
						"	IC_DOCUMENTO "  +
						"FROM "  +
						"	COM_DOCUMENTO D " +
						"WHERE "  +
						"	EXISTS "  +
						"			(  "  +
						"				SELECT "  +
						"					1 "  +
						"				FROM "  +
						"					COMCAT_PYME P "  +
						"				WHERE "  +
						"              P.IC_PYME				=	D.IC_PYME AND "  +
						"              P.IN_NUMERO_SIRAC 	=  ? " +								// sc_sirac
						"			)  "  +
						" AND "+
						" 	IC_IF						=	TO_NUMBER(?) 	AND "+ 						// sc_if
						" 	IC_EPO					=	TO_NUMBER(?) 	AND "+ 						// sc_epo
						" 	IC_MONEDA				=	? 					AND "+ 						// ic_moneda
						" 	TRUNC(DF_FECHA_VENC)	=	TO_DATE(?,'dd/mm/yyyy') AND "+ 			// sf_vencimiento
						" 	IC_ESTATUS_DOCTO		=	? AND "+ 										// 24
						" 	IG_NUMERO_DOCTO		=	? "; 												// ig_numero_docto

				  lVarBind.clear();
				  lVarBind.add(sc_sirac);
				  lVarBind.add(sc_if);
				  lVarBind.add(sc_epo);
				  lVarBind.add(new Integer(ic_moneda));
				  lVarBind.add(sf_vencimiento);
				  lVarBind.add(new Integer(24));
				  lVarBind.add(ig_numero_docto);

				  log.debug("\nsc_sirac: " + sc_sirac);// Debug info
				  log.debug("sc_if: " + sc_if);// Debug info
				  log.debug("sc_epo: " + sc_epo);// Debug info
				  log.debug("ic_moneda: " + ic_moneda);// Debug info
				  log.debug("sf_vencimiento: " + sf_vencimiento);// Debug info
				  log.debug("ig_numero_docto: " + ig_numero_docto + "\n");// Debug info

				  registros= con.consultarDB(query,lVarBind,false);
				  if(registros.next()) {
					  ic_docto=registros.getString("IC_DOCUMENTO").equals("")?0:Integer.parseInt(registros.getString("IC_DOCUMENTO"));
			  	  }
				  log.debug("ic_docto: "+ic_docto);

					if (sc_estatus.equals("OK")&&(ic_docto!=0)){

						log.debug("Docto aceptado:"+ic_docto+" Numero:"+ig_numero_docto);
						/* Validaciones para la generaci�n de solicitud */
						if (hayClaseDoctoAsociado(Integer.toString(ic_epo), con)) {	//No existe una clase de documento asociado para realizar Descuento?
	                 if (hayFondeoPropio(Integer.toString(ic_if),Integer.toString(ic_epo),con)) {
							  /* Fondeo Propio .- Obtencion del porcentaje de comision */
							  query = "SELECT " +
									  "	FG_PORC_COMISION_FONDEO "+
									  "FROM "  +
									  "	COMCAT_PRODUCTO_NAFIN " +
									  "WHERE "  +
									  "	IC_PRODUCTO_NAFIN = ?";
								lVarBind.clear();
								lVarBind.add(new Integer(1));

								registros = con.consultarDB(query,lVarBind,false);
								if (registros.next()){
									porcentajeFondeo = registros.getString("FG_PORC_COMISION_FONDEO");
									estatusSolic = "10";
								} else {
									log.debug("No se encontr� porcentaje de comisi�n");
									error=true;
								}
								/* Se obtuvo porcentaje */

						   } //Se manejo fondeo propio

							/* Verificar que solo exista una solicitud por documento */
							query = "SELECT " +
									  "	COUNT(*) " +
									  "FROM " +
									  "	COM_SOLICITUD " +
									  "WHERE " +
									  "	IC_DOCUMENTO = ?";

							lVarBind.clear();
							lVarBind.add(new Integer(ic_docto));

							registros = con.consultarDB(query,lVarBind,false);

						  	registros.next();
			  				if ( (registros.getString(1).equals("")?0:(Integer.parseInt(registros.getString(1)))) == 0) { //El ic_documento no existe en solicitudes?
								/* Obtencion de los datos del documento */
								String fechaDocumento 	= "";
								String fechaVencimiento = "";
								String diaVencimiento 	= "";
								String tasaIF 				= "";
								String claseDocumento 	= "";
								String montoDescuento 	= "";
								int litipoPiso 			= 0;
								int iDiaPlazo 				= 0;
								String sFechaVencDesc 	= "";
								String sPagoAntic 		= null;
								String ic_banco_fondeo 	= "";
								query =
										"SELECT " +
										"		TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS DF_FECHA_DOCTO, 	"+
										" 		TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') 	AS DF_FECHA_VENC, 	"+
										" 		TO_CHAR(D.DF_FECHA_VENC,'DD') 			AS DIAVENCIMIENTO, 	"+
										" 		DS.IN_IMPORTE_RECIBIR, " +
										" 		T.IC_TASA, "+
										" 		PD.IC_CLASE_DOCTO, "+
										" 		I.IG_TIPO_PISO, "+
										" 		TRUNC(D.DF_FECHA_VENC) - TRUNC(SYSDATE) AS PLAZO, "+
										" 		IE.CS_PAGO_ANTICIPADO, "+
										" 		E.IC_BANCO_FONDEO "+
										"FROM "  +
										"		COM_DOCUMENTO D, "+
										" 		COMCAT_TASA T, "+
										" 		COMREL_PRODUCTO_DOCTO PD, "+
										" 		COM_DOCTO_SELECCIONADO DS, "+
										" 		COMREL_TASA_AUTORIZADA TA, "+
										" 		COMREL_TASA_BASE TB, "+
										" 		COMREL_IF_EPO IE, "+
										" 		COMCAT_IF I, "+
										" 		COMCAT_EPO E " +
										"WHERE " +
										"		DS.DC_FECHA_TASA 		= 	TA.DC_FECHA_TASA	AND "+
										"     DS.IC_EPO 				= 	TA.IC_EPO 			AND "+
										"     DS.IC_IF 				= 	TA.IC_IF 			AND "+
										"     TA.DC_FECHA_TASA 		= 	TB.DC_FECHA_TASA 	AND "+
										"     TB.IC_TASA 				= 	T.IC_TASA 			AND "+
										" 		T.IC_MONEDA 			= 	D.IC_MONEDA 		AND "+
										"  	D.IC_EPO					=	PD.IC_EPO 			AND "+
										" 		PD.IC_PRODUCTO_NAFIN	=	? 						AND "+ // 1
										" 		D.IC_DOCUMENTO 		= 	? 						AND "+ // IC_DOCTO
										"     D.IC_DOCUMENTO 		= 	DS.IC_DOCUMENTO 	AND "+
										"     DS.IC_IF 				= 	IE.IC_IF 			AND "+
										"     DS.IC_EPO 				= 	IE.IC_EPO 			AND "+
										"     IE.IC_IF 				= 	I.IC_IF 				AND "+
										"     IE.IC_EPO 				= 	E.IC_EPO ";

								lVarBind.clear();
								lVarBind.add(new Integer(1));
								lVarBind.add(new Integer(ic_docto));

								registros = con.consultarDB(query,lVarBind,false);

								if (registros.next())
								{
									fechaDocumento 	= registros.getString("DF_FECHA_DOCTO");
									fechaVencimiento 	= registros.getString("DF_FECHA_VENC");
									diaVencimiento 	= registros.getString("DIAVENCIMIENTO");
									tasaIF 				= registros.getString("IC_TASA");
									claseDocumento 	= registros.getString("IC_CLASE_DOCTO");
									montoDescuento 	= registros.getString("IN_IMPORTE_RECIBIR");
									litipoPiso 			= registros.getString("IG_TIPO_PISO").equals("")?0:Integer.parseInt(registros.getString("IG_TIPO_PISO"));
									iDiaPlazo 			= registros.getString("PLAZO").equals("")?0:Integer.parseInt(registros.getString("PLAZO")) ;
									int iDiasVenc 		= iDiaPlazo;
									sPagoAntic 			= (registros.getString("CS_PAGO_ANTICIPADO").equals(""))?"N":registros.getString("CS_PAGO_ANTICIPADO");
									sFechaVencDesc 	= "";
									ic_banco_fondeo 	= registros.getString("IC_BANCO_FONDEO");

									if((litipoPiso == 2)&&(!hayFondeoPropio(Integer.toString(ic_if),Integer.toString(ic_epo),con))) {	// Si el IF es de 2 piso y no es fondeo propio se hace los calculos de los d�as de plazo.

										boolean 	bEntroEnUnPlazo 	= false;
										Calendar cFechaVencDesc 	= Calendar.getInstance();

										cFechaVencDesc.setTime(new java.util.Date());

										SimpleDateFormat sdfv 	= new SimpleDateFormat("dd/MM/yyyy");

										if ("S".equals(sPagoAntic)) {
											query=
													"SELECT " +
													"	IN_DIA " +
													"FROM " +
													"	COM_PAGO_ANTIC_DETALLE "+
													" WHERE " +
													"	? BETWEEN IN_PLAZO_MINIMO AND IN_PLAZO_MAXIMO "+ // iDiaPlazo
													" 	AND IC_IF = ? " + //ic_if
													" 	AND IC_EPO = ?";//ic_epo

											lVarBind.clear();
											lVarBind.add(new Integer(iDiaPlazo));
											lVarBind.add(new Integer(ic_if));
											lVarBind.add(new Integer(ic_epo));

											registros = con.consultarDB(query,lVarBind,false);
											if(registros.next()) {
												iDiaPlazo = registros.getString("IN_DIA").equals("")?0:Integer.parseInt(registros.getString("IN_DIA"));	//El nuevo plazo.
												if(iDiasVenc > iDiaPlazo) {	// Suma el plazo solo si este es mayor a los dias adicionales.
												/* Solo si existe una reduccion real del plazo */
													bEntroEnUnPlazo = true;
													if(iDiaPlazo > 120) {
													 	iDiaPlazo=120;
													}
									         	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);

												} else {
													cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
												}
											} else {
												/* No se encuentra parametrizado el rango */
												if(iDiaPlazo > 120) {
												 	iDiaPlazo			=	120;
									         	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
													bEntroEnUnPlazo 	= 	true;
												} else {
													cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
												}

											}

										} else { /* No hay ajuste por pago anticipado */
												/* El ajuste se hace porque sobrepasa el plazo tope de 120 dias */
												if(iDiaPlazo > 120) {
												 	iDiaPlazo			=	120;
									         	cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
													bEntroEnUnPlazo 	= true;
												} else {
													cFechaVencDesc.add(Calendar.DATE, iDiaPlazo);
												}
										} /* Fin del if-else del pago anticipado */
										if (bEntroEnUnPlazo){

											/* Validaciones porque hubo cambios en las fechas de vencimiento */
											boolean busca		=	true;
											boolean aumenta	=	true;
											String  inhabil	=	null;

											while (busca){
												query	=
														"SELECT " +
														"  ES_INHABIL(TO_DATE(?,'DD/MM/YYYY')) as INHABIL " +
														"FROM " +
														"	DUAL";

												lVarBind.clear();
												lVarBind.add(sdfv.format(cFechaVencDesc.getTime()));

												log.debug("Ajuste de fechas:"+sdfv.format(cFechaVencDesc.getTime()));
												registros=con.consultarDB(query,lVarBind,false);
												if (registros.next()){
													inhabil=registros.getString("INHABIL");
													if (inhabil.equals("0")){
														/* No es inhabil el dia */
														busca=false;
													} else if ((iDiaPlazo<=120)&&(aumenta)) { /* Se aumenta el vencimiento por el ajuste */
														if (iDiaPlazo==120){
															aumenta=false;
															iDiaPlazo--;
															cFechaVencDesc.add(Calendar.DATE, -1);
														} else{
															iDiaPlazo++;
															cFechaVencDesc.add(Calendar.DATE, 1);
														}
											  	  } else if ((iDiaPlazo<=120)&&(!aumenta)){
													  	/* Se disminuye el vencimiento por el tope de 120 dias*/
											  	      iDiaPlazo--;
											  	      cFechaVencDesc.add(Calendar.DATE, -1);
												  } else {
													  /* Nunca se toca este codigo */
													  error	=	true;
													  busca	=	false;
											  	  }
												} else {
													/* No se pudo determinar si es inhabil */
													error	=	true;
													log.info("No se puede determinar si es dia inhabil");
													busca	=	false;
												}

											} /* while de la busqueda del dia habil mas cercano*/
										} // Fin del ajuste a la fecha de vencimiento
										sFechaVencDesc = sdfv.format(cFechaVencDesc.getTime());
										diaVencimiento = sdfv.format(cFechaVencDesc.getTime()).substring(0,2);
									} // Fin de los ajustes por ser IF de segundo piso

									String sFechaVencimiento = (sFechaVencDesc.equals(""))?fechaVencimiento:sFechaVencDesc;
									String folio=null;

				 					if (!error){
										/* Obtenci�n de la raz�n social de la EPO */
				 						query	=
											"SELECT " +
											"	CG_RAZON_SOCIAL " +
											"FROM " +
											"	COMCAT_EPO WHERE IC_EPO = ?"; //ic_epo

										lVarBind.clear();
										lVarBind.add(new Integer(ic_epo));

				 						try{

											registros=con.consultarDB(query,lVarBind,false);

											if (registros.next()){
											  String emisor=registros.getString("CG_RAZON_SOCIAL");

											  /* Obtenci�n del folio de la solicitud */
											  folio = getFolioSolicitud(sc_if, con);
											  query=
													"INSERT INTO " +
													"	 COM_SOLICITUD " +
													"		(" +
													" 			IC_FOLIO, " +
													"			IC_DOCUMENTO, " +
													"			IC_ESQUEMA_AMORT, " +
													"			IC_TIPO_CREDITO, " +
													"			IC_OFICINA, " +
													"			CC_ACUSE, " +
													"			IG_PLAZO, " +
													"			CG_TIPO_PLAZO, " +
													"			CG_PERIO_PAGO_CAP, " +
													" 			CG_PERIO_PAGO_INT, " +
													"			CG_DESC_BIENES, " +
													"			IC_CLASE_DOCTO, " +
													" 			CG_DOMICILIO_PAGO, " +
													"			IC_TASAIF, " +
													"			CG_RMIF, " +
													"			IN_STIF," +
													" 			IC_TABLA_AMORT, " +
													"			IN_NUMERO_AMORT, " +
													"			DF_PPC, " +
													"			DF_PPI, " +
													" 			DF_V_DOCUMENTO, " +
													"			DF_V_DESCUENTO, " +
													"			IN_DIA_PAGO, " +
													" 			DF_ETC, " +
													"			CG_LUGAR_FIRMA, " +
													"			IG_NUMERO_CONTRATO, " +
													"			IC_ESTATUS_SOLIC, " +
													" 			DF_FECHA_SOLICITUD, " +
													"			IC_BLOQUEO, " +
													"			CS_TIPO_SOLICITUD, " +
													" 			CG_EMISOR, " +
													"			FG_PORC_COMISION_FONDEO, " +
													"			IC_BANCO_FONDEO" +
													"		) " +
													" VALUES " +
													"		(  " +
													"			?," + // folio
													"			?," + // ic_docto
													"			?," + // 1,
													"			?," + // 1,
													"			?," + // 90,
													"			?," + // acuse
													"  		TO_DATE(?,'dd/mm/yyyy') - TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')," + //sFechaVencimiento
													"  		?," + // 'D'
													"			?," + // 'Al Vto.'
													"			?," + // 'Al Vto.',
													"			?," + // ' ',
													"			?," + // claseDocumento
													"			?," + //	'Mexico, D.F.'
													"			?," + // tasaIF
													"			?," + //'+'
													"			?," + // 0
													"			?," + //1
													"			?," + //1
													" 			TO_DATE(?,'dd/mm/yyyy')," + // sFechaVencimiento
													"  		TO_DATE(?,'dd/mm/yyyy')," + // sFechaVencimiento
													" 			TO_DATE(?,'dd/mm/yyyy')," + // fechaVencimiento
													" 			TO_DATE(?,'dd/mm/yyyy')," + // sFechaVencimiento
													" 			?," + //diaVencimiento
													"			TO_DATE(?,'dd/mm/yyyy')," + // fechaDocumento
													" 			?," + // 'Mexico, D.F.'
													"			?," + // 0
													"			?," + // estatusSolic
													"			SYSDATE," + // sysdate
													"			?," + // NULL
													"			?," + //'C'
													"			?," + // emisor
													"			?," + // porcentajeFondeo
													"			? " + // ic_banco_fondeo
													"		)";
													//log.info("\n\n"+query);

												lVarBind.clear();
												lVarBind.add(folio);
												lVarBind.add(new Integer(ic_docto));
												lVarBind.add(new Integer(1));
												lVarBind.add(new Integer(1));
												lVarBind.add(new Integer(90));
												lVarBind.add(acuse);
												lVarBind.add(sFechaVencimiento);
												lVarBind.add("D");
												lVarBind.add("Al Vto.");
												lVarBind.add("Al Vto.");
												lVarBind.add(" ");
												lVarBind.add(claseDocumento);
												lVarBind.add("Mexico, D.F.");
												lVarBind.add(tasaIF);
												lVarBind.add("+");
												lVarBind.add(new Integer(0));
												lVarBind.add(new Integer(1));
												lVarBind.add(new Integer(1));
												lVarBind.add(sFechaVencimiento);
												lVarBind.add(sFechaVencimiento);
												lVarBind.add(fechaVencimiento);
												lVarBind.add(sFechaVencimiento);
												lVarBind.add(diaVencimiento);
												lVarBind.add(fechaDocumento);
												lVarBind.add("Mexico, D.F.");
												lVarBind.add(new Integer(0));
												lVarBind.add(estatusSolic);
												//lVarBind.add(sysdate);
												lVarBind.add(Integer.TYPE);
												lVarBind.add("C");
												lVarBind.add(emisor);
												lVarBind.add(porcentajeFondeo);
												lVarBind.add(new Integer(ic_banco_fondeo));

												con.ejecutaUpdateDB(query, lVarBind);
												log.debug(new java.util.Date() + " Solicitud: " + folio + " insertada");
									      } /* fin de la extraccion de la razon social */
										}catch(SQLException sqle){
											//error = true;
											throw new NafinException("DSCT0071");
										}
									} /* Fin del if insercion de la solicitud */
									if (!error){
										query =
												"INSERT INTO " +
												"	COM_AMORTIZACION " +
												"		( " +
												"			IC_AMORTIZACION, " +
												"			IC_FOLIO, " +
												"			IN_NUMERO, " +
												" 			FN_IMPORTE, " +
												"			DF_FECHA " +
												"		) " +
												" VALUES " +
												"		( " +
												"			SEQ_COM_AMORTIZACION_IC_AMOR.NEXTVAL, " +
												"			?," + // folio
												" 			?," + // 1
												"			?," + // montoDescuento
												"			TO_DATE(?,'DD/MM/YYYY')  " + // fechaVencimiento
												"		) ";

												lVarBind.clear();
												lVarBind.add(folio);
												lVarBind.add(new Integer(1));
												lVarBind.add(montoDescuento);
												lVarBind.add(fechaVencimiento);

										//log.info("\n\n"+query);
										try {
											con.ejecutaUpdateDB(query, lVarBind);

											//Actualizamos al documento con el estatus Operada (4)
											query = "UPDATE " +
													  "	COM_DOCUMENTO " +
													  "SET " +
													  "	IC_ESTATUS_DOCTO = ? " + 	//4
													  "WHERE " +
													  "	IC_DOCUMENTO = ? "; 			//ic_docto
											//log.info("\n\n"+query);
											lVarBind.clear();
											lVarBind.add(new Integer(4));
											lVarBind.add(new Integer(ic_docto));

											try {
												con.ejecutaUpdateDB(query, lVarBind);
											}catch(SQLException sqle) {
												throw new NafinException("DSCT0063");
											}

											// Actualizar acuse y fecha de operacion en la tabla del enlace
											query = "UPDATE " +
													  " 	" + enlace.getTablaDocumentos()  + " " +
													  "SET " +
													  "	CC_ACUSE 		= ?, 			" + 	// acuse
													  "	DF_FECHA_OPER 	= SYSDATE	" + 	// sysdate
													  "WHERE " +
													  "	IC_DOCUMENTO = ? "; 			//ic_docto
											//log.info("\n\n"+query);
											lVarBind.clear();
											lVarBind.add(acuse);
											lVarBind.add(new Integer(ic_docto));

											try {
												con.ejecutaUpdateDB(query, lVarBind);
											}catch(SQLException sqle) {
												throw new NafinException("DSCT0063");// Preguntar
											}

										}catch(SQLException sqle) {
											error = true;
										}

									} /* Fin del if de la Insercion de la amortizacion y la actualizacion del docto */

								} else{

									error=true;
									log.info("No se pudo encontrar los datos del documento");

								} /* Fin del if de la extraccion de los datos del documento */
							}else{

								error=true;
								log.info("Existen solicitudes ya registradas para esta EPO");

							} /* Fin de las busqueda en solicitudes */
						}else{
							/* if Validacion de Documento asociado */
					    	error=true;
					    	log.info("Sin documento asociado a la operaci�n");
						} /* else Validacion de  Documento asociado */
					}else{
						/* No se encontro el documento */
						error		=	false;
						query 	=
							"UPDATE " +
							"   "+ enlace.getTablaDocumentos() + " " +
							"SET " +
							"	DF_FECHA_OPER 		= SYSDATE " +
							"WHERE " +
							"   IG_NUMERO_DOCTO 	= ? ";

						lVarBind.clear();
						lVarBind.add(ig_numero_docto);

						con.ejecutaUpdateDB(query, lVarBind);

				   }
			 	 }else{
					 log.info("La moneda o el formato de fecha es invalido");
				 } //If cuando no existe error en los datos iniciales
			}catch(SQLException sqle) {
				// Marcar el registro con error
				log.error("procesaDoctos(Error)", sqle);
				error = true;
			}catch(NafinException ne) {
				// Marcar el registro con error
				log.error("procesaDoctos(Error)" + ne.getMessage());
				error = true;
			}catch(Exception e){
				log.error("procesaDoctos(Error)", e);
				error=true;
			}finally {
		        if (con.hayConexionAbierta()) {
					  /* Realiza un commit o un rollback */
					  if (error == true){
						  con.terminaTransaccion(false);
					  }else{
						  con.terminaTransaccion(true);
					  }
				} /* If cuando hay conexion */
				log.info("procesaDoctos(S)");
	  		}
		}
		// Actualizar acuse
		if(hayAlMenosUnRegistro){
				try{
					ActualizaAcuse(acuse,con);
				}catch(Exception e){
					log.error("procesaDoctos(Error)", e);
					if(con.hayConexionAbierta()) con.cierraConexionDB();
					throw new NafinException("SIST0001");
				}
		}

		// EXTRAER REGISTROS CON ERROR
		try {
				enlace.generaQueryDeDocumentosConError();
				qrySentencia 	= enlace.getQuery();
				lVarBind 		= enlace.getListaDeVariablesBind();
				documento 		= con.consultarDB(qrySentencia, lVarBind, false);
		}catch(Exception e) {
				log.error("procesaDoctos(Exception) ", e);
				e.printStackTrace();
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				throw new NafinException("SIST0001");
		}

	   // PROCESAR DOCUMENTOS CON ERROR
		while(documento.next())
		{

			String sc_if 				= documento.getString("SC_IF");
			String sc_epo 				= documento.getString("SC_EPO");
			String sc_sirac			= documento.getString("SC_SIRAC");
			String sc_moneda			= documento.getString("SC_MONEDA");
			String ig_numero_docto	= documento.getString("IG_NUMERO_DOCTO");
			String sf_vencimiento	= documento.getString("SF_VENCIMIENTO");
			String sc_estatus			= documento.getString("SC_ESTATUS");

			log.debug("Docto: " + ig_numero_docto  + " presenta el siguiente codigo de error: " + sc_estatus );// Debug info

			boolean 		error					=	false;
			int 			ic_docto				=	0;
			int			ic_epo				=	0;
			int			ic_if					=	0;
			String 		aux					=	null;
			String 		query					=	"";

			int 			ic_moneda			=	Integer.parseInt(sc_moneda);

			if(ic_moneda != 1 && ic_moneda != 54){
				log.debug("No se pudo determinar la clave de la moneda, clave: " + ic_moneda); // Debug info
				ic_moneda = 0;
			}

			try{   /* Verificaciones de datos */

				aux = sf_vencimiento;

				try{

					if ((!Comunes.checaFecha(aux))||(ic_moneda==0))	{
						log.debug("La fecha " + aux + " no es valida."); // Debug info
						error=true;
					}

					ic_epo	=	Integer.parseInt(sc_epo);
					ic_if		=	Integer.parseInt(sc_if);

				}catch (Exception e){
					log.error("Error al verificar fecha y extraer ic_epo e ic_if en la seccion de documentos con error", e);
					throw new NafinException("DSCT0012");
				}

				if (!error){
				  /* Busqeda del documento */
				  query =
				  		"SELECT "  +
						" 	ic_documento "  +
						"FROM "  +
						"	com_documento d "  +
						"WHERE "  +
						" EXISTS "  +
						"			( " +
						"				SELECT 			  "  +
						"					1   			  "  +
						"				FROM 				  "  +
						"					comcat_pyme p "  +
						"				WHERE 			  "  +
						"              p.ic_pyme			=	d.ic_pyme AND "  +
						"              p.in_numero_sirac = ? " +  						// sc_sirac
						"			 ) " +
						" AND "  +
						" ic_if						=	to_number(?) AND "+ 					// sc_if
						" ic_epo						=	to_number(?) AND "+ 					// sc_epo
						" ic_moneda					=	? AND "+ 								// ic_moneda
						" TRUNC(df_fecha_venc)	=	TO_DATE(?,'dd/mm/yyyy') AND "+ 	// sf_vencimiento
						" ic_estatus_docto		=	? AND "+ 								// 24
						" ig_numero_docto			=	? "; 										// ig_numero_docto

				  lVarBind.clear();
				  lVarBind.add(sc_sirac);
				  lVarBind.add(sc_if);
				  lVarBind.add(sc_epo);
				  lVarBind.add(new Integer(ic_moneda));
				  lVarBind.add(sf_vencimiento);
				  lVarBind.add(new Integer(24));
				  lVarBind.add(ig_numero_docto);

				  log.debug("sc_sirac:         " + sc_sirac); 			// Debug info
				  log.debug("sc_if:            " + sc_if); 				// Debug info
				  log.debug("sc_epo:           " + sc_epo); 				// Debug info
				  log.debug("ic_moneda:        " + ic_moneda); 			// Debug info
				  log.debug("ic_estatus_docto: " + 24); 					// Debug info
				  log.debug("sf_vencimiento:   " + sf_vencimiento); 	// Debug info
				  log.debug("ig_numero_docto:  " + ig_numero_docto); 	// Debug info

				  registros= con.consultarDB(query,lVarBind,false);
				  if(registros.next()) {
					  ic_docto=registros.getString("IC_DOCUMENTO").equals("")?0:Integer.parseInt(registros.getString("IC_DOCUMENTO"));
					}else{ // Debug info
							log.debug("No se encontraron documentos con error que tuvieran un estatus valido");
				  } // Debug info

	  			  log.debug("Docto con error:"+ic_docto+" Numero:"+ig_numero_docto);
				  String	causa;

				  // Verificar que sea un error valido
				  boolean esErrorDesconocido = false;

				  if(!sc_estatus.trim().equals("") && sc_estatus.length() > 0 ){
					  sc_estatus = sc_estatus.length() > 8? sc_estatus.substring(0,8):sc_estatus;
				  }else{
					  esErrorDesconocido = true;
				  }

				  if(ic_docto != 0 && !esErrorDesconocido ){ // && claveIF != null
				  		try{
					   	/* Busca la descripcion de la causa de rechazo */
					  		query =
					  			"SELECT "  +
								"	CG_DESCRIPCION "  +
								"FROM "  +
								"	COM_ERROR_IF "  +
								"WHERE "  +
								" CC_ERROR_IF 	= ? AND "+ 	// CLAVE ERROR
								" IC_IF 			= ? "; 		// CLAVE IF
							lVarBind.clear();
							lVarBind.add(sc_estatus);
							lVarBind.add(claveIF);

							log.debug("Docto con error: " + ig_numero_docto + " cc_error_if: " + sc_estatus); // Debug info

							registros=con.consultarDB(query,lVarBind,false);

							if (registros.next()){

								causa	= registros.getString("CG_DESCRIPCION");

								/* Inserta historico */
								query =
									"INSERT INTO "  +
									"	COMHIS_CAMBIO_ESTATUS "  +
									"		( "  +
									"			DC_FECHA_CAMBIO, 	 "  +
									"			IC_DOCUMENTO, 	 	 "	 +
									" 			IC_CAMBIO_ESTATUS, "  +
									"			CT_CAMBIO_MOTIVO	 "  +
									"		) "  +
									"  VALUES " +
									"		( "  +
									"			SYSDATE,"  +
									"			?," + // ic_docto
									"			?," + // 23
									"			? " + // causa
									"		) ";

								lVarBind.clear();
								lVarBind.add(new Integer(ic_docto));
								lVarBind.add(new Integer(23));
								lVarBind.add(causa);

								con.ejecutaUpdateDB(query, lVarBind);
								log.debug("Se realiza update en COMHIS_CAMBIO_ESTATUS a estatus 23 para ic_docto: " + ic_docto + ", con la siguiente causa: " + causa); // Debug info

								/* Modifica el estatus del documento */
								query =
									"UPDATE "  +
									"	COM_DOCUMENTO "  +
									"SET " +
									"	IC_ESTATUS_DOCTO	=	? "  + 	//2
									"WHERE "  +
									"	IC_DOCUMENTO		=  ? "; 		//ic_docto

								lVarBind.clear();
								lVarBind.add(new Integer(2));
								lVarBind.add(new Integer(ic_docto));

								con.ejecutaUpdateDB(query, lVarBind);
								log.debug("Se realiza update en COM_DOCUMENTO  a estatus 2 para ic_docto: " + ic_docto);// Debug info

								// Actualizar fecha de operacion en la tabla del enlace
								query = "UPDATE " +
								  	  " 	" + enlace.getTablaDocumentos()  + " " +
									  "SET " +
									  "	DF_FECHA_OPER 	= SYSDATE	" + 	// sysdate
									  "WHERE " +
									  "	IC_DOCUMENTO = ? "; 					//ic_docto

								lVarBind.clear();
								lVarBind.add(new Integer(ic_docto));

								con.ejecutaUpdateDB(query, lVarBind);
								log.debug("Se realizar actualizacion de la hora en DF_FECHA_OPER para la Tabla: " + enlace.getTablaDocumentos() + ", para el siguiente documento: " + ic_docto); // Debug info

							} else {	//error=true;
						   	log.debug("No se encontr� la descripci�n dela causa de rechazo");
							}
						}catch(Exception e) {
							throw new NafinException("DSCT0011");
						}

					}else{
						/* No se encontro el documento */
						error		=	false;
						query 	=
							"UPDATE " +
							"   "+ enlace.getTablaDocumentos() + " " +
							"SET " +
							"	DF_FECHA_OPER 		= SYSDATE " +
							"WHERE " +
							"   IG_NUMERO_DOCTO 	= ? ";

						lVarBind.clear();
						lVarBind.add(ig_numero_docto);

						con.ejecutaUpdateDB(query, lVarBind);
					}
			 	 }else{
					 log.debug("La moneda o el formato de fecha es invalido");
				 } //If cuando no existe error en los datos iniciales

			}catch(SQLException sqle) {	// Marcar el registro con error
				log.error("procesaDoctos(Error)", sqle);
				error = true;
			}catch(NafinException ne) {	// Marcar el registro con error
				log.error("procesaDoctos(Error)" + ne.getMessage());
				error = true;
			}catch(Exception e){
				log.error("procesaDoctos(Error)", e);
				error = true;
			}finally{

		      if (con.hayConexionAbierta()) {      /* Realiza un commit o un rollback */
		         if (error == true){
						con.terminaTransaccion(false);
				   } else {
						con.terminaTransaccion(true);
				   }
				}
				/* If cuando hay conexion */
				log.error("procesaDoctos(S)");

	  	}

		}

		if(con != null && con.hayConexionAbierta()) con.cierraConexionDB();
	}


	private boolean getLimitarRangoFechas() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH");	//HH es formato 24 horas.
		String hora = sdf.format(new java.util.Date());
		int iHora = Integer.parseInt(hora);
		if (iHora < 15) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Permitir� obtener el aviso de notificaci�n de los documentos autorizados por el intermediario
	 * Los resultados de este m�todo, dependen de la parametrizacion del IF
	 * "TIPO_MONEDA" en la Base de Datos. Si este es "A" Regresa MN y USD.
	 * Si es "MN" solo regresa los documentos de Moneda Nacional (ic_moneda = 1)
	 * y si es "DA" solo regresa Dolares Americanos (ic_moneda = 54)
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del IF
	 * @param fecNotificacionIni Fecha de notificacion inicial
	 * @param fecNotificacionFin Fecha de notificacion final
	 * @param fecVencIni Fecha de vencimiento inicial
	 * @param fecVencFin Fecha de vencimiento final
	 * @param claveEpo Clave de la Epo
	 * @param acuseIf Acuse del IF
	 * @return  ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 * del proceso mas los objetos DocumentoIFWS con los datos de los
	 * documentos
	 */
		 public ProcesoIFWSInfo getAvisosNotificacionWS(String claveUsuario,
			String password, 	String claveIF,
			String fecNotificacionIni, String fecNotificacionFin,
			String fecVencIni, String fecVencFin,
			String claveEpo, String acuseIf) {
		
			ProcesoIFWSInfoV2 procV2=this.getAvisosNotificacionWSV2 (claveUsuario,
			 password, 	 claveIF,
			 fecNotificacionIni,  fecNotificacionFin,
			 fecVencIni,  fecVencFin,
			 claveEpo,  acuseIf,true);
			 ProcesoIFWSInfo proceso=new ProcesoIFWSInfo();
			 proceso.setCodigoEjecucion(procV2.getCodigoEjecucion());
			 proceso.setResumenEjecucion(procV2.getResumenEjecucion());
			 
			 DocumentoIFWSV2[] arrDocumentosV2 = procV2.getDocumentos();
			 if(arrDocumentosV2!=null){
				 DocumentoIFWS[] arrDocumentos=new DocumentoIFWS[arrDocumentosV2.length];
				 for(int i=0;i<arrDocumentosV2.length;i++){
				 arrDocumentos[i]=new DocumentoIFWS();
					arrDocumentos[i].setAcusePyme(arrDocumentosV2[i].getAcusePyme());
						arrDocumentos[i].setClaveBancoFondeo(arrDocumentosV2[i].getClaveBancoFondeo());
						arrDocumentos[i].setClaveDocumento(arrDocumentosV2[i].getClaveDocumento());
						arrDocumentos[i].setClaveEpo(arrDocumentosV2[i].getClaveEpo());
						arrDocumentos[i].setClaveMoneda(arrDocumentosV2[i].getClaveMoneda());
						arrDocumentos[i].setClaveEstatus(arrDocumentosV2[i].getClaveEstatus());
						arrDocumentos[i].setFechaEmision(arrDocumentosV2[i].getFechaEmision());
						arrDocumentos[i].setFechaSeleccionPyme(arrDocumentosV2[i].getFechaSeleccionPyme());
						arrDocumentos[i].setFechaPublicacion(arrDocumentosV2[i].getFechaPublicacion());
						arrDocumentos[i].setFechaVencimiento(arrDocumentosV2[i].getFechaVencimiento());
						arrDocumentos[i].setMontoDescuento(arrDocumentosV2[i].getMontoDescuento());
						arrDocumentos[i].setTasaAceptada(arrDocumentosV2[i].getTasaAceptada());
						arrDocumentos[i].setImporteInteres(arrDocumentosV2[i].getImporteInteres());
						arrDocumentos[i].setImporteRecibir(arrDocumentosV2[i].getImporteRecibir());
						arrDocumentos[i].setNombreEpo(arrDocumentosV2[i].getNombreEpo());
						arrDocumentos[i].setNombreMoneda(arrDocumentosV2[i].getNombreMoneda());
						arrDocumentos[i].setNombrePyme(arrDocumentosV2[i].getNombrePyme());
						arrDocumentos[i].setNumeroDocumento(arrDocumentosV2[i].getNumeroDocumento());
						arrDocumentos[i].setNumeroProveedor(arrDocumentosV2[i].getNumeroProveedor());
						arrDocumentos[i].setNumeroSirac(arrDocumentosV2[i].getNumeroSirac());
						arrDocumentos[i].setTipoFactoraje(arrDocumentosV2[i].getTipoFactoraje());
						arrDocumentos[i].setClaveRechazo(arrDocumentosV2[i].getClaveRechazo());
						arrDocumentos[i].setErrorDetalle(arrDocumentosV2[i].getErrorDetalle());
						arrDocumentos[i].setCampoAdicional1(arrDocumentosV2[i].getCampoAdicional1());
						arrDocumentos[i].setCampoAdicional2(arrDocumentosV2[i].getCampoAdicional2());
						arrDocumentos[i].setCampoAdicional3(arrDocumentosV2[i].getCampoAdicional3());
						arrDocumentos[i].setCampoAdicional4(arrDocumentosV2[i].getCampoAdicional4());
						arrDocumentos[i].setCampoAdicional5(arrDocumentosV2[i].getCampoAdicional5());
						arrDocumentos[i].setMontoDocumento(arrDocumentosV2[i].getMontoDocumento());
						arrDocumentos[i].setPorcentajeDescuento(arrDocumentosV2[i].getPorcentajeDescuento());
						arrDocumentos[i].setRecursoGarantia(arrDocumentosV2[i].getRecursoGarantia());
						arrDocumentos[i].setPlazo(arrDocumentosV2[i].getPlazo());
						arrDocumentos[i].setNombreEstatus(arrDocumentosV2[i].getNombreEstatus());
						arrDocumentos[i].setReferencia(arrDocumentosV2[i].getReferencia());
						arrDocumentos[i].setClaveIF(arrDocumentosV2[i].getClaveIF());
						arrDocumentos[i].setNombreIF(arrDocumentosV2[i].getNombreIF());
						//FVR
						arrDocumentos[i].setClavePYME(arrDocumentosV2[i].getClavePYME());
						arrDocumentos[i].setFechaNotificacion(arrDocumentosV2[i].getFechaNotificacion());
						arrDocumentos[i].setPorcentajeAnticipo(arrDocumentosV2[i].getPorcentajeAnticipo());
						arrDocumentos[i].setNombreTipoFactoraje(arrDocumentosV2[i].getNombreTipoFactoraje());
						arrDocumentos[i].setAcuseIF(arrDocumentosV2[i].getAcuseIF());
				 }
				 proceso.setDocumentos(arrDocumentos);
			 }
			 return proceso;

			}
			
			public ProcesoIFWSInfoV2 getAvisosNotificacionWSV2 (String claveUsuario,
			String password, 	String claveIF,
			String fecNotificacionIni, String fecNotificacionFin,
			String fecVencIni, String fecVencFin,
			String claveEpo, String acuseIf) {
					return this.getAvisosNotificacionWSV2 (claveUsuario,
			 password, 	 claveIF,
			 fecNotificacionIni,  fecNotificacionFin,
			 fecVencIni,  fecVencFin,
			 claveEpo,  acuseIf,true);
			}
	 
	public ProcesoIFWSInfoV2 getAvisosNotificacionWSV2 (String claveUsuario,
			String password, 	String claveIF,
			String fecNotificacionIni, String fecNotificacionFin,
			String fecVencIni, String fecVencFin,
			String claveEpo, String acuseIf,boolean bandera) {
		log.info("AutorizacionDescuentoBean::getAvisosNotificacionWSV2(E)("+claveIF+"_"+claveUsuario+")");
		int iClaveIF = 0;
		int iClaveEPO = 0;
		boolean existeRangofechas = false;
		List lstDocument = new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement psPrincipal = null;
		ResultSet rsPrincipal = null;
		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		UtilUsr utilUsr = new UtilUsr();
		String monedaParametrizadaWS = "";
		String tipoFactoraje = "";

	
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);


			if(claveEpo != null && !claveEpo.equals(""))
				iClaveEPO = Integer.parseInt(claveEpo);



			if(fecNotificacionIni!=null && !fecNotificacionIni.equals("")){
				if(!Comunes.esFechaValida(fecNotificacionIni,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}
			if(fecNotificacionFin!=null && !fecNotificacionFin.equals("")){
				if(!Comunes.esFechaValida(fecNotificacionFin,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}
			if(fecVencIni!=null && !fecVencIni.equals("")){
				if(!Comunes.esFechaValida(fecVencIni,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}
			if(fecVencFin!=null && !fecVencFin.equals("")){
				if(!Comunes.esFechaValida(fecVencFin,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}

			if(!existeRangofechas) throw new Exception("Se requiere al menos un rango de fechas");

		} catch(Exception e) {
			log.error("avisosNotificacionWS(Error) ", e);

			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso

			resultadoProceso.setResumenEjecucion("Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF +
					"," + claveEpo + "," + fecNotificacionIni + "," + fecNotificacionFin +
					"," + fecVencIni + "," + fecVencFin +")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}


		//******************************Validacion de fechas*********************************
		try{

			boolean limitarRangoFechas = this.getLimitarRangoFechas();

			if(fecNotificacionIni!=null && !fecNotificacionIni.equals("")){
				if(fecNotificacionFin==null || fecNotificacionFin.equals("")) throw new Exception("Se requiere fecha de Notificacion final");
				else{
					if(!Comunes.esComparacionFechaValida(fecNotificacionFin,"ge",fecNotificacionIni,"dd/MM/yyyy"))
						throw new Exception("La fecha de notificacion final debe ser mayor o igual a la inicial");

					if(limitarRangoFechas && Fecha.restaFechas(fecNotificacionIni,fecNotificacionFin)>7)
						throw new Exception("El rango de fechas no debe ser mayor a 7 dias");

				}
			}


			if(fecVencIni!=null && !fecVencIni.equals("")){
				if(fecVencFin==null || fecVencFin.equals("")) throw new Exception("Se requiere fecha de Vencimiento final");
				else{
					if(!Comunes.esComparacionFechaValida(fecVencFin,"ge",fecVencIni,"dd/MM/yyyy"))
						throw new Exception("La fecha de vencimiento final debe ser mayor o igual a la inicial");

					if(limitarRangoFechas && Fecha.restaFechas(fecVencIni,fecVencFin)>7)
						throw new Exception("El rango de fechas no debe ser mayor a 7 dias");
				}
			}


		} catch(Exception e) {
			log.error("avisosNotificacionWS(Error) " +  e.getMessage());

			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso

			resultadoProceso.setResumenEjecucion("Error en rango de Fechas: " +
					"(" + fecNotificacionIni + "," + fecNotificacionFin + "," + fecVencIni +
					"," + fecVencFin + ")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}

		try {
			//Validacion del usuario y Password y que sea del IF especificado
			List cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			log.debug("avisosNotificacionWS(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("avisosNotificacionWS::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("El usuario no existe para el IF especificado");
				return resultadoProceso; //Termina Proceso
			}

			log.debug("avisosNotificacionWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("Fallo la autenticacion del usuario");
				return resultadoProceso; //Termina Proceso
			}

			con.conexionDB();

//**********************************VALIDACIONES PARA EL QUERY**************************
			StringBuffer condicion = new StringBuffer();
			if(fecNotificacionIni!= null && !fecNotificacionIni.equals(""))
			{
				if(fecNotificacionFin!= null && !fecNotificacionFin.equals(""))
					condicion.append(
						" AND (a3.df_fecha_hora >= trunc(to_date(?,'dd/mm/yyyy')) " +
						" AND  a3.df_fecha_hora < trunc(to_date(?,'dd/mm/yyyy')+1)) ");
			}

			if(fecVencIni!=null && !fecVencIni.equals(""))
			{
				if(fecVencFin!=null && !fecVencFin.equals(""))
					condicion.append(
						" AND (d.df_fecha_venc >= trunc(to_date(?,'dd/mm/yyyy')) " +
						" AND d.df_fecha_venc < trunc(to_date(?,'dd/mm/yyyy')+1)) ");
			}

			//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - INI
			boolean epoExterna = false;
			if (claveEpo!=null && !claveEpo.equals("")){
				condicion.append( " AND D.ic_epo = ? ");
				Map mEpo = this.getClaveEpoExternaAInternaWSIF(claveIF, claveEpo);
				if (mEpo.get("IC_EPO") != null ) {
					tipoFactoraje = (String)mEpo.get("CC_TIPO_FACTORAJE");
					claveEpo = (String)mEpo.get("IC_EPO");
					condicion.append(" AND d.cs_dscto_especial = ? ");
					epoExterna = true;
				}
			}
			//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - FIN

			if (acuseIf!=null && !acuseIf.equals(""))
				condicion.append( " AND A3.cc_acuse = ? ");

			if( (claveEpo ==null || claveEpo.equals("")) &&
					(fecNotificacionIni==null || fecNotificacionIni.equals("")) &&
					(fecNotificacionFin==null || fecNotificacionFin.equals("")) &&
					(acuseIf==null || acuseIf.equals("") ) ){// && tipoFactoraje.equals("")
				condicion.append(
					"   AND a3.df_fecha_hora >= TO_DATE (?, 'dd/mm/yyyy') " +
					"   AND a3.df_fecha_hora < (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
			}

			monedaParametrizadaWS = getParametrizacionMonedaWS(iClaveIF);

			if (monedaParametrizadaWS.equals("MN") ||
					monedaParametrizadaWS.equals("DA")) {
				condicion.append(" AND d.ic_moneda = ? ");
			}


			String sentenciaSQL =
					//"SELECT	/*+ use_nl(d ds e a3 p m ie s pe tf) index(d CP_COM_DOCUMENTO_PK) index(tf COMCAT_TIPO_FACTORAJE_PK) index (s IN_COM_SOLICITUD_11_NUK)*/ " +
					"SELECT	/*+ index(a3) use_nl(d ds e a3 p m ie s pe tf)*/ " +
					"		d.ic_documento as claveDocumento, " +
					"		d.ic_estatus_docto as claveEstatus,         " +
					"        ds.CC_ACUSE as acusePyme, " +
					"        d.CC_ACUSE as acuseIf, " +
					"        e.ic_banco_fondeo as claveBancoFondeo, " +
					"        e.ic_epo as claveEpo, " +
					"        m.ic_moneda as claveMoneda, " +
					"        TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') as fecha_emision, " +
					"        TO_CHAR(ds.df_fecha_seleccion,'dd/mm/yyyy') as fechaSeleccionPyme, " +
					"        TO_CHAR(d.df_alta,'dd/mm/yyyy') as fechaPublicacion, " +
					"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as fechaVencimiento, " +
					"        d.fn_monto_dscto as montoDescuento, " +
					"        DECODE(ds.CS_OPERA_FISO,'S',ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada) as tasaAceptada, " +
					"        DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes) as importeInteres, " +
					"        DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir) as importeRecibir,  " +
					"        (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' '|| e.cg_razon_social)  as nombreEpo,  " +
					"        m.cd_nombre as nombreMoneda,	  " +
					"        DECODE(ds.CS_OPERA_FISO,'S',ifs.cg_razon_social,(DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' '|| p.cg_razon_social)) as nombrePyme,  " +
					"        d.ig_numero_docto as numeroDocumento,  " +
					"        pe.cg_pyme_epo_interno as numeroProveedor,  " +
					"        DECODE(ds.CS_OPERA_FISO,'S',99999,p.in_numero_sirac) as numeroSirac,  " +
					"        d.cs_dscto_especial as tipoFactoraje, " +
					"        p.ic_pyme AS clavePyme, " +
					"        TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') as fechaNotificacion, 		 " +
					"        d.fn_monto, d.fn_porc_anticipo as porcentajeAnticipo,  " +
					"        tf.cg_nombre AS nombreTipoFactoraje, " +
					"			d.CG_CAMPO1 as campo1, "+
					"			d.CG_CAMPO2 as campo2, "+
					"			d.CG_CAMPO3 as campo3, "+
					"			d.CG_CAMPO4 as campo4, "+
					"			d.CG_CAMPO5 as campo5, "+
					"			d.CG_CAMPO5 as campo5, "+
					"  DECODE(ds.CS_OPERA_FISO,'S',p.cg_razon_social,'') as ct_referencia, " +
					"        TO_CHAR (s.DF_OPERACION, 'DD/MM/YYYY') as FECHA_OPERACION, " +// AJUSTE: AGUST�IN BAUTISTA
					"        s.IG_NUMERO_PRESTAMO, " +                                     // AJUSTE: AGUST�IN BAUTISTA
					"        s.FG_PORC_COMISION_FONDEO, " +                                // AJUSTE: AGUST�IN BAUTISTA
					"        s.FG_TASA_FONDEO_NAFIN, " +                                   // AJUSTE: AGUST�IN BAUTISTA
					"        s.IC_ESTATUS_SOLIC, " +                                       // AJUSTE: AGUST�IN BAUTISTA
					"        s.IC_BLOQUEO, " +                                             // AJUSTE: AGUST�IN BAUTISTA
					"        ds.CG_CUENTA, " +                                             // AJUSTE: AGUST�IN BAUTISTA
					"       'AutorizacionDescuentoBean::avisosNotificacionWS' AS pantalla " +
					"  FROM com_documento d, " +
					"       com_docto_seleccionado ds, " +
					"       comcat_epo e, " +
					"       com_acuse3 a3, " +
					"       comcat_pyme p, " +
					"       comcat_moneda m, " +
					"       comrel_if_epo ie, " +
					"       com_solicitud s, " +
					"       comrel_pyme_epo pe, " +
					"       comcat_tipo_factoraje tf, " +
					"		  comcat_if ifs"+
					" WHERE d.ic_documento = ds.ic_documento " +
					"	 AND ds.ic_if = ifs.ic_if"+	
					"   AND d.cs_dscto_especial != 'C' " +
					"   AND d.ic_documento = s.ic_documento " +
					"   AND s.cc_acuse = a3.cc_acuse " +
					"   AND d.ic_if = ie.ic_if " +
					"   AND d.ic_epo = ie.ic_epo " +
					"   AND ie.ic_if = ? "+
					"   AND ie.cs_vobo_nafin = 'S' " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_pyme = p.ic_pyme " +
					"   AND d.ic_moneda = m.ic_moneda " +
					"   and d.ic_epo = pe.ic_epo(+) " +
					"   and d.ic_pyme = pe.ic_pyme(+) " +
					"   AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
					condicion.toString();

			//log.debug("avisosNotificacionWS:sentenciaSQL:>>>>"+sentenciaSQL);
			List params = new ArrayList();
			
			//---->>>>Pasar mas bajo   psPrincipal= con.queryPrecompilado(sentenciaSQL);
			params.add(new Integer(claveIF));

			if(fecNotificacionIni!=null && !fecNotificacionIni.equals(""))
				if(fecNotificacionFin!=null && !fecNotificacionFin.equals("")){
					params.add(fecNotificacionIni);
					params.add(fecNotificacionFin);
				}

			if(fecVencIni!=null && !fecVencIni.equals(""))
				if(fecVencFin!=null && !fecVencFin.equals("")){
					params.add(fecVencIni);
					params.add(fecVencFin);
				}

			if (claveEpo!=null && !claveEpo.equals("")){
				params.add(new Integer(claveEpo));
				//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - INI
				if (epoExterna){
					params.add(tipoFactoraje);
				}
			}
			//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - INI

			if (acuseIf!=null && !acuseIf.equals("")) {
				params.add(acuseIf);
			}

			if( (claveEpo==null || claveEpo.equals("")) &&
					(fecNotificacionIni==null || fecNotificacionIni.equals("")) &&
					(fecNotificacionFin==null || fecNotificacionFin.equals("")) &&
					(acuseIf==null || acuseIf.equals("")) ) {
				params.add(fechaActual);
				params.add(fechaActual);
			}

			if (monedaParametrizadaWS.equals("MN")) {
				params.add(new Integer(1));	//1.- Moneda Nacional
			} else if (monedaParametrizadaWS.equals("DA")) {
				params.add(new Integer(54));	//54.- Dolares Americanos
			}
			log.debug("getAvisosNotificacionWSV2: qry=" + sentenciaSQL);
			log.debug("getAvisosNotificacionWSV2: params=" + params);

			psPrincipal= con.queryPrecompilado(sentenciaSQL, params);
			rsPrincipal = psPrincipal.executeQuery();
			if(rsPrincipal!=null){
				while(rsPrincipal.next()){
					DocumentoIFWSV2 documento = new DocumentoIFWSV2();
					documento.setClaveDocumento((rsPrincipal.getString("claveDocumento")==null)?"":rsPrincipal.getString("claveDocumento"));
					documento.setClaveEstatus((rsPrincipal.getString("claveEstatus")==null)?"":rsPrincipal.getString("claveEstatus"));
					documento.setAcusePyme((rsPrincipal.getString("acusePyme")==null)?"":rsPrincipal.getString("acusePyme"));
					documento.setAcuseIF((rsPrincipal.getString("acuseIf")==null)?"":rsPrincipal.getString("acuseIf"));
					documento.setClaveBancoFondeo((rsPrincipal.getString("claveBancoFondeo")==null)?"":rsPrincipal.getString("claveBancoFondeo"));
					//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - INI
					String cveEpoExt = this.getClaveEpoExternaWSIF(String.valueOf(iClaveIF), 
							rsPrincipal.getString("claveEpo"), rsPrincipal.getString("tipoFactoraje"));
					
					if(cveEpoExt!=null && !cveEpoExt.equals("")){
						documento.setClaveEpo(cveEpoExt);
					} else {
						documento.setClaveEpo((rsPrincipal.getString("claveEpo")==null)?"":rsPrincipal.getString("claveEpo"));
					}
					//SE REALIZA AJUSTE HARDCODE POR PETICION DEL USUARIO (FODEA XXX-2012) - FIN
					documento.setClaveMoneda((rsPrincipal.getString("claveMoneda")==null)?"":rsPrincipal.getString("claveMoneda"));
					documento.setFechaEmision((rsPrincipal.getString("fecha_emision")==null)?"":rsPrincipal.getString("fecha_emision"));
					documento.setFechaSeleccionPyme((rsPrincipal.getString("fechaSeleccionPyme")==null)?"":rsPrincipal.getString("fechaSeleccionPyme"));
					documento.setFechaPublicacion((rsPrincipal.getString("fechaPublicacion")==null)?"":rsPrincipal.getString("fechaPublicacion"));
					documento.setFechaVencimiento((rsPrincipal.getString("fechaVencimiento")==null)?"":rsPrincipal.getString("fechaVencimiento"));
					documento.setMontoDescuento((rsPrincipal.getString("montoDescuento")==null)?"":rsPrincipal.getString("montoDescuento"));
					documento.setTasaAceptada((rsPrincipal.getString("tasaAceptada")==null)?"":rsPrincipal.getString("tasaAceptada"));
					documento.setImporteInteres((rsPrincipal.getString("importeInteres")==null)?"":rsPrincipal.getString("importeInteres"));
					documento.setImporteRecibir((rsPrincipal.getString("importeRecibir")==null)?"":rsPrincipal.getString("importeRecibir"));
					documento.setNombreEpo((rsPrincipal.getString("nombreEpo")==null)?"":rsPrincipal.getString("nombreEpo"));
					documento.setNombreMoneda((rsPrincipal.getString("nombreMoneda")==null)?"":rsPrincipal.getString("nombreMoneda"));
					documento.setNombrePyme((rsPrincipal.getString("nombrePyme")==null)?"":rsPrincipal.getString("nombrePyme"));

					String numeroDeDocumento = rsPrincipal.getString("numeroDocumento");
					documento.setNumeroDocumento((numeroDeDocumento==null)?"":numeroDeDocumento.replaceAll("[\\p{Cntrl}&&[^\t]]", ""));

					documento.setNumeroProveedor((rsPrincipal.getString("numeroProveedor")==null)?"":rsPrincipal.getString("numeroProveedor"));
					documento.setNumeroSirac((rsPrincipal.getString("numeroSirac")==null)?"":rsPrincipal.getString("numeroSirac"));
					documento.setTipoFactoraje((rsPrincipal.getString("tipoFactoraje")==null)?"":rsPrincipal.getString("tipoFactoraje"));


					String campoAdicional1 = rsPrincipal.getString("campo1");
					campoAdicional1 = (campoAdicional1!=null)?campoAdicional1.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional2 = rsPrincipal.getString("campo2");
					campoAdicional2 = (campoAdicional2!=null)?campoAdicional2.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional3 = rsPrincipal.getString("campo3");
					campoAdicional3 = (campoAdicional3!=null)?campoAdicional3.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional4 = rsPrincipal.getString("campo4");
					campoAdicional4 = (campoAdicional4!=null)?campoAdicional4.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional5 = rsPrincipal.getString("campo5");
					campoAdicional5 = (campoAdicional5!=null)?campoAdicional5.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";


					documento.setCampoAdicional1(campoAdicional1);
					documento.setCampoAdicional2(campoAdicional2);
					documento.setCampoAdicional3(campoAdicional3);
					documento.setCampoAdicional4(campoAdicional4);
					documento.setCampoAdicional5(campoAdicional5);

					documento.setClavePYME((rsPrincipal.getString("clavePYME")==null)?"":rsPrincipal.getString("clavePYME"));
					documento.setFechaNotificacion((rsPrincipal.getString("fechaNotificacion")==null)?"":rsPrincipal.getString("fechaNotificacion"));
					documento.setPorcentajeAnticipo((rsPrincipal.getString("porcentajeAnticipo")==null)?"":rsPrincipal.getString("porcentajeAnticipo"));
					documento.setNombreTipoFactoraje((rsPrincipal.getString("nombreTipoFactoraje")==null)?"":rsPrincipal.getString("nombreTipoFactoraje"));
					documento.setReferencia((rsPrincipal.getString("ct_referencia")==null)?"":rsPrincipal.getString("ct_referencia"));
					
					// AGUSTIN BAUTISTA
					documento.setFechaOperacion(     (rsPrincipal.getString("FECHA_OPERACION")         ==null)?"":rsPrincipal.getString("FECHA_OPERACION")          );
					documento.setCuentaDeposito(     (rsPrincipal.getString("CG_CUENTA")               ==null)?"":rsPrincipal.getString("CG_CUENTA")                );
					documento.setPorcComisionFondeo( (rsPrincipal.getString("FG_PORC_COMISION_FONDEO") ==null)?"":rsPrincipal.getString("FG_PORC_COMISION_FONDEO")  );
					documento.setTasaFondeoNafin(    (rsPrincipal.getString("FG_TASA_FONDEO_NAFIN")    ==null)?"":rsPrincipal.getString("FG_TASA_FONDEO_NAFIN")     );
					documento.setNumeroPrestamo(     (rsPrincipal.getString("IG_NUMERO_PRESTAMO")      ==null)?"":rsPrincipal.getString("IG_NUMERO_PRESTAMO")       );
					documento.setClaveEstatusSolic(  (rsPrincipal.getString("IC_ESTATUS_SOLIC")        ==null)?"":rsPrincipal.getString("IC_ESTATUS_SOLIC")         );
					documento.setClaveBloqueo(       (rsPrincipal.getString("IC_BLOQUEO")              ==null)?"":rsPrincipal.getString("IC_BLOQUEO")               );

					lstDocument.add(documento);
				}
			}

			if(rsPrincipal!=null)rsPrincipal.close();
			if(psPrincipal!=null)psPrincipal.close();
			//ps.close();

			DocumentoIFWSV2[] arrDocumentos = null;
			if (lstDocument.size() > 0) {
				arrDocumentos = new DocumentoIFWSV2[lstDocument.size()];
				lstDocument.toArray(arrDocumentos);
			}

			resultadoProceso.setDocumentos(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito

		} catch (Throwable t) {

			log.error("avisosNotificacionWS(Error) ", t);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			resultadoProceso.setResumenEjecucion("Error Inesperado. Proceso Cancelado:" + t.getMessage());

		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
	    	log.info("AutorizacionDescuentoBean::getAvisosNotificacionWSV2(S)("+claveIF+"_"+claveUsuario+")");			
		}//finaliza  try inicial
		return resultadoProceso;
	}//cierre avisoIFWS


	/**
	 * Actualiza la lista de documentos que seran Autorizados, incluyendo aquellos documentos
	 * que no fueron seleccionados, pero que comparten la misma nota de
	 * credito. Solo para usarse si la EPO tiene habilitada las Notas de Credito a Multiples
	 * Documentos.
	 *
	 * @Fodea 002 - 2010
	 *
	 * @param doctosSeleccionados Arreglo con los Id de los documentos consultados; ver:
	 *                             nafin-web/13descuento/13pki/13if/13forma3a.jsp
	 *
	 * @return ArrayList con la lista de documentos actualizada.
	 */
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[])
		throws AppException{

		log.info("actualizaDoctosSeleccionados(E)");

		ArrayList 		resultado 				= new ArrayList();
		HashSet			listaDocumentos 		= new HashSet();
		HashSet   		listaNotasDeCredito 	= new HashSet();
		HashMap			documentosAgregados	= new HashMap();

		AccesoDB 		con 						= new AccesoDB();
		StringBuffer	query						= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		int 				numeroDocumentosIniciales = 0;
		// Verificar que hayan datos
		if(doctosSeleccionados == null || (doctosSeleccionados.length == 0 ) ){
			resultado.add(doctosSeleccionados);
			resultado.add(documentosAgregados);
			resultado.add(new Boolean("false"));
			log.info("actualizaDoctosSeleccionados(S)");
			return resultado;
		}

		// Obtener lista de IDs de los documentos que han sido seleccionados para cambiar estatus a negociables
		for(int i=0;i<doctosSeleccionados.length;i++){
			String icDocumento = (String) doctosSeleccionados[i];
			listaDocumentos.add(icDocumento);
		}

		try{

			con.conexionDB();

			int numeroDoctosAnteriores = 0;
			numeroDocumentosIniciales = listaDocumentos.size();

			do{
				numeroDoctosAnteriores = listaDocumentos.size();

				// 1. Traer notas de credito asociadas
				query						= new StringBuffer();
				query.append(
					"select                     "  +
					"	distinct IC_NOTA_CREDITO "  +
					"from                       "  +
					"	comrel_nota_docto        "  +
					"where                      "  +
					"	ic_documento in (        ");
				for(int i=0;i<listaDocumentos.size();i++){
					if(i>0) query.append(",");
					query.append("?");
				}
				query.append(" ) ");

				lVarBind.clear();
				Iterator it = listaDocumentos.iterator();
				while(it.hasNext()) {
					lVarBind.add((String)it.next());
				}

				registros = con.consultarDB(query.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					listaNotasDeCredito.add(registros.getString("IC_NOTA_CREDITO"));
				}

				if(listaNotasDeCredito.size() == 0){
					break;
				}
				// 2. Traer documentos relacionados
				query						= new StringBuffer();
				query.append(
					"select                    "  +
					"	distinct IC_DOCUMENTO   "  +
					"from                      "  +
					"	comrel_nota_docto       "  +
					"where                     "  +
					"	ic_nota_credito in (    ");
				for(int i=0;i<listaNotasDeCredito.size();i++){
					if(i>0) query.append(",");
					query.append("?");
				}
				query.append(" ) ");

				lVarBind.clear();
				it = listaNotasDeCredito.iterator();
				while(it.hasNext()) {
					lVarBind.add((String)it.next());
				}

				registros = con.consultarDB(query.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					listaDocumentos.add(registros.getString("IC_DOCUMENTO"));
				}

			}while(
				numeroDoctosAnteriores != listaDocumentos.size()
			); // 3. Comparar si hay diferencias

			// Construir Lista Unica con todos los IDs de los Documentos
			HashSet listaDoctosConsultados = new HashSet();
			for(int i=0;i<doctosSeleccionados.length;i++){
				 listaDoctosConsultados.add((String)doctosSeleccionados[i]);
			}
			Iterator iterator = listaDocumentos.iterator();
			while(iterator.hasNext()){
				listaDoctosConsultados.add((String)iterator.next());
			}
			// Buscar los documentos que se agregaron
			iterator = listaDoctosConsultados.iterator();
			while(iterator.hasNext()){
				String documento = (String)iterator.next();
				if(!existeEnArray(doctosSeleccionados,documento)){
					documentosAgregados.put(documento,"true");
				}
			}
			// Crear Nuevo Array de Documentos Seleccionados y agregar todos los  ids.
			String[] newDoctosSeleccionados = new String[listaDoctosConsultados.size()];
			iterator = listaDoctosConsultados.iterator();
			int k =0;
			while(iterator.hasNext()){
				String icDocto = (String)iterator.next();
				newDoctosSeleccionados[k] = icDocto;
				k++;
			}
			// Agregar los resultados obtenidos
			resultado.add(newDoctosSeleccionados);
			resultado.add(documentosAgregados);
			if( numeroDocumentosIniciales != listaDocumentos.size()){
				resultado.add(new Boolean("true")); // Se encontraron otros documentos a los que se les aplico
																// la Nota de Credito Multiple, por lo que se agregaron a la
																// lista de Documentos que tambien deben ser Autorizados.
			}else{
				resultado.add(new Boolean("false"));
			}

		}catch(Exception e){
			log.info("actualizaDoctosSeleccionados(Exception)");
			log.info("actualizaDoctosSeleccionados.doctosSeleccionados = <"+doctosSeleccionados+">");
			e.printStackTrace();
			throw new AppException("Error al actualizar lista de Doctos Asociados a las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("actualizaDoctosSeleccionados(S)");
		}

		return resultado;
	}

	/**
	 *  Busca en un <tt>Array</tt> un objeto de tipo <tt>String</tt>.
	 *
	 *  @param lista Array con los elementos a buscar
	 *  @param elemento Objeto de Tipo String con el contenido a buscar.
	 *
	 *  @return <tt>true</tt> si la busqueda fue exitosa. <tt>false</tt>
	 *          en caso contrario.
	 */
	private boolean existeEnArray(String[] lista,String elemento){
		log.info("existeEnArray(E)");

		boolean encontrado = false;
		if( (lista == null) || (lista.length == 0) || (elemento == null) ){
			log.info("existeEnArray(S)");
			return false;
		}

		for(int i=0;i<lista.length;i++){
			String registro = lista[i];
			if(elemento.equals(registro)){
				encontrado = true;
				break;
			}
		}

		log.info("existeEnArray(S)");
		return encontrado;
	}

	/**
	 * metodo que envia los errores Inesperados en el proceso de WS
	 * a los correos que existen en la tabla de comcat_correoWS
	 * esto es enviado solo si hay Errores Inesperados
	 * @param errorInesperado Datos del error Inesperado. Ser� el cuerpo del correo
	 * @param metodo Proceso que genera el error. Con base en este par�metro se 
	 * 	selecciona el titulo del correo.
	 * @return
	 */
	private String enviodeCorreoWS(StringBuffer errorInesperado, String metodo) {
		log.info("EnviodeCorreoWS (E)");

		StringBuffer  qrySentencia = new StringBuffer();
		PreparedStatement  ps	= null;
		ResultSet  rs	= null;
		List correos = new ArrayList();
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;

		String respuesta ="";

		String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px;'";

		String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
				"font-size: 10px; "+
				"font-weight: bold;"+
				"color: #FFFFFF;"+
				"background-color: #4d6188;"+
				"padding-top: 3px;"+
				"padding-right: 1px;"+
				"padding-bottom: 1px;"+
				"padding-left: 3px;"+
				"height: 25px;"+
				"border: 1px solid #1a3c54;'";


		String titulo ="";
		if(metodo.equals("S")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE DOCUMENTOS PYME WS";
		}else if (metodo.equals("C")){
			titulo = "ERRORES INESPERADOS EN LA CONFIRMACION DE DOCUMENTOS WS ";
		}else if (metodo.equals("CTAS")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE CUENTAS PYME PENDIENTES POR AUTORIZAR WS ";
		}else if (metodo.equals("CTAS_A")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE CUENTAS PYME AUTORIZADAS WS ";
		}else if (metodo.equals("C_CTAS")){
			titulo = "ERRORES INESPERADOS EN LA CONFIRMACION DE CUENTAS PYME WS ";
		}

		log.debug(errorInesperado);
		try {
			con.conexionDB();

			if(!errorInesperado.toString().equals("")) {
				qrySentencia.append(" SELECT cg_email FROM comcat_correoWS ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();

				while(rs.next()){
					correos.add(rs.getString("CG_EMAIL"));
				}
				rs.close();
				ps.close();

				for(int i=0;i< correos.size();i++){

					String correoPara =(String)correos.get(i);

					log.debug(correoPara);

					StringBuffer mensajeCorreo =  new StringBuffer();
					mensajeCorreo.append("<html>");
					mensajeCorreo.append("<head>");
					mensajeCorreo.append("<title>");
					mensajeCorreo.append(""+titulo+"");
					mensajeCorreo.append("</title>");
					mensajeCorreo.append("</head>");
					mensajeCorreo.append("<body>");
	
					mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='400'>");
					mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='left'><b>ERRORES INESPERADOS: :<b></td></tr>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+errorInesperado+"\r\n"+"<br></td></tr>");
	
					mensajeCorreo.append("<p>");
					mensajeCorreo.append("<p>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+"Atentamente"+"</td></tr>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+"Nacional Financiera  S.N.C."+"</td></tr>");
					mensajeCorreo.append("</table>");
					mensajeCorreo.append("</body>");
					mensajeCorreo.append("</html>");
	
					Correo correo = new Correo();
					correo.enviarTextoHTML("cadenas@nafin.gob.mx", correoPara, titulo, mensajeCorreo.toString());
				}
				con.terminaTransaccion(true);
			}

			return respuesta;
		} catch(Exception e) {
			log.error("EnviodeCorreoWS (Exception) " + e);
			throw new AppException("Error al enviar el correo de '" + titulo + "'",  e);
		} finally {
			log.info("EnviodeCorreoWS (S)");
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}

	// FODEA 029 - 2011. MANTENIMIENTO DE DISTRIBUIDORES (BY JSHD)
	/**
	 *
	 * Devuelve un <tt>HashMap</tt> con los Tipos de Fondeo para cada una de las EPOs cuyos ids
	 * se especifican en la siguiente lista: <tt>listaClavesEpo</tt>
	 *
	 * @param claveIF Clave del Intermediario Financiero.
	 * @param listaClavesEpo <tt>ArrayList</tt> con los ids de las EPOs.
	 * @param icProductoNafin Clave del Producto Nafin.
	 *
	 * @return <tt>HashMap</tt> con las parejas de valores (ic_epo,tipo_fondeo) solicitados
	 *
	 */
  	public HashMap getTipoFondeo(String claveIF, ArrayList listaClavesEpo, String icProductoNafin)
		throws AppException {

		 log.info("getTipoFondeo(E)");

		 if( listaClavesEpo == null || listaClavesEpo.size() == 0 ){
			 log.info("getTipoFondeo(S)");
			 return new HashMap();
		 }

		 AccesoDB 				con  				= new AccesoDB();
		 StringBuffer 			qrySentencia 	= new StringBuffer();
		 ResultSet 				rs 				= null;
		 PreparedStatement 	ps		 			= null;

		 HashMap					catalogo			= new HashMap();

		 try {

		 	con.conexionDB();

			StringBuffer variablesBind = new StringBuffer();
			for(int i=0;i<listaClavesEpo.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}

			qrySentencia.append(
					"SELECT                          "  +
					"	CS_TIPO_FONDEO, IC_EPO        "  +
					" FROM                           "  +
					"	COMREL_IF_EPO_X_PRODUCTO      "  +
					" WHERE                          "  +
					"	IC_IF                    =  ? "  + // claveIF
					"  AND IC_EPO               IN ("+variablesBind.toString()+") "  + // ic_epo
					"  AND IC_PRODUCTO_NAFIN    =  ? "    // ic_producto_nafin
			);

			ps = con.queryPrecompilado(qrySentencia.toString());
			int idx = 1;
			ps.setInt(idx++,Integer.parseInt(claveIF));
			for(int i=0;i<listaClavesEpo.size();i++){
				ps.setInt(idx++,Integer.parseInt((String)listaClavesEpo.get(i)));
			}
			ps.setInt(idx++,Integer.parseInt(icProductoNafin));
			rs = ps.executeQuery();

			while(rs != null && rs.next()){
				catalogo.put( rs.getString("IC_EPO"),rs.getString("CS_TIPO_FONDEO") );
			}

		 } catch(Exception e) {
		 	catalogo = new HashMap();
			log.error("getTipoFondeo(Exception)");
			log.error("getTipoFondeo.claveIF         = <"+claveIF+">");
			log.error("getTipoFondeo.listaClavesEpo  = <"+listaClavesEpo+">");
			log.error("getTipoFondeo.icProductoNafin = <"+icProductoNafin+">");
			e.printStackTrace();
			throw new AppException("Error al obtener el Tipo de Fondeo");
		 } finally {
		 	if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getTipoFondeo(S)");
		 }
		 return catalogo;

	}

	public String getDescripcionTipoFondeo(HashMap catalogoTipoFondeo, String claveEpo){

		 String descripcion = "";

		 if("P".equals((String) catalogoTipoFondeo.get(claveEpo))){
				 descripcion = "Fondeo Propio";
		 } else if ("N".equals((String) catalogoTipoFondeo.get(claveEpo))){
				 descripcion = "Fondeo Nafin";
		 }

		 return descripcion;

	}

	/**
	 * Valida que la hora de envio de operacios de IFs se cumpla (F026-2011 FVR)
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cveEpo
	 */
	private boolean validaHoraEnvOpeIFs(String cveEpo) throws AppException{
		log.debug("validaHoraEnvOpeIFs (E)::ic_epo="+cveEpo);
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sQuery = "";
		boolean horaValida = true;
		try{
			con.conexionDB();
			String horaEnvOpeIf = "";
			Calendar cal = Calendar.getInstance();
			cal.setTime(new java.util.Date());

			int iHora     = cal.get(Calendar.HOUR_OF_DAY);
			int iMinutos  = cal.get(Calendar.MINUTE);

			if(!cveEpo.equals("")){
					sQuery =
						"SELECT pe.cc_parametro_epo, cg_valor  " +
						"  FROM com_parametrizacion_epo pz, comcat_parametro_epo pe  " +
						" WHERE pz.cc_parametro_epo = pe.cc_parametro_epo  " +
						" AND pz.ic_epo = ?  " +
						" AND pz.cc_parametro_epo = ? ";


					log.debug(":::Consulta Parametros:2::"+ sQuery);
					log.debug(":::Bind::"+ cveEpo);
					ps = con.queryPrecompilado(sQuery);
					ps.setInt(1, Integer.parseInt(cveEpo));
					ps.setString(2,"HORA_ENV_OPE_IF");

					rs = ps.executeQuery();
					if(rs!=null && rs.next()) {
						horaEnvOpeIf = rs.getString("cg_valor")==null?"":(String)rs.getString("cg_valor");
						//se realiza validacion de horario
						if(!"".equals(horaEnvOpeIf)){
							int horaEnv   = Integer.parseInt( horaEnvOpeIf.substring(0, horaEnvOpeIf.indexOf(":")) );
							int minEnv   = Integer.parseInt( horaEnvOpeIf.substring(horaEnvOpeIf.indexOf(":")+1) );

							if (iHora < horaEnv){
								horaValida = false;
							}else{
								if (iHora == horaEnv && iMinutos < minEnv)
									horaValida = false;
							}

						}
					}
					rs.close();
					if(ps!=null) ps.close();
				}
			return horaValida;
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("error al validar horario de envio de operaciones a IFs",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.debug("validaHoraEnvOpeIFs (S)");
		}
	}


	public HashMap  validaLimiteEpo(String cveEpo, String cveIF) throws AppException{

		log.debug("validaLimiteEpo (E)::ic_epo="+cveEpo);
		log.debug("validaLimiteEpo (E)::cveIF="+cveIF);

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement psUp = null;
		StringBuffer query	= new StringBuffer();

		HashMap datos =  new HashMap();

		BigDecimal montoLimite = new BigDecimal(0);
		BigDecimal montoLimiteUsado = new BigDecimal(0);
		BigDecimal montoLimiteEPO = new BigDecimal(0);
		BigDecimal difMontoLimite = new BigDecimal(0);
		
		BigDecimal difMontoLimite_dl = new BigDecimal(0);//Fodea XX-2014
		BigDecimal montoLimite_dl = new BigDecimal(0); //Fodea XX-2014
		BigDecimal montoLimiteUsado_dl = new BigDecimal(0); //Fodea XX-2014
		BigDecimal montoLimiteEPO_dl = new BigDecimal(0); //Fodea XX-2014

		String mensaje  ="NoHaySobreGirado";
		String mensaje_dl  ="NoHaySobreGirado";

		try{
			con.conexionDB();

				query	= new StringBuffer();
				query.append(	" SELECT /*+ use_nl(d ds) index(d in_com_documento_04_nuk) */ "+
									 " SUM (DECODE(d.ic_moneda, '1',  NVL (d.fn_monto_dscto, 0) , 0 )) AS v_fn_limite_util_docto,  "+
									 " SUM (DECODE(d.ic_moneda, '54', NVL (d.fn_monto_dscto , 0), 0 ) ) AS v_fn_limite_util_docto_dl "+ 
									"	FROM com_documento d, com_docto_seleccionado ds"+
									" WHERE d.ic_documento = ds.ic_documento "+
									"	 AND d.ic_estatus_docto IN (3, 4, 24 ) "+
									"	 AND d.ic_epo = ? "+
									"	 AND d.ic_if = ? ");

					log.debug(":::Consulta validaLimiteEpo::"+ query.toString());

					ps = con.queryPrecompilado(query.toString());
					ps.setInt(1, Integer.parseInt(cveEpo));					
					ps.setInt(2, Integer.parseInt(cveIF));
					rs = ps.executeQuery();
					if(rs.next()) {
						montoLimite = new BigDecimal(rs.getString("v_fn_limite_util_docto")==null?"0":(String)rs.getString("v_fn_limite_util_docto"));
						montoLimite_dl = new BigDecimal(rs.getString("v_fn_limite_util_docto_dl")==null?"0":(String)rs.getString("v_fn_limite_util_docto_dl"));
						
					}
					rs.close();
					ps.close();

					query	= new StringBuffer();
					query.append( " SELECT  IN_LIMITE_EPO as limiteEpo,  ie.FN_LIMITE_UTILIZADO  as limiteUsado "+
												" ,IN_LIMITE_EPO_DL as limiteEpo_dl,  ie.FN_LIMITE_UTILIZADO_DL  as limiteUsado_dl "+ //Fodea XX-2014
												" FROM  comrel_if_epo ie   "+
												" WHERE ic_epo = ? "+
												"  and ic_if = ? ");

					log.debug( "query   "+query.toString());


					ps1 = con.queryPrecompilado(query.toString());
					ps1.setInt(1, Integer.parseInt(cveEpo));
					ps1.setInt(2, Integer.parseInt(cveIF));
					rs1 = ps1.executeQuery();
					if(rs1.next()) {
						montoLimiteEPO = new BigDecimal(rs1.getString("limiteEpo")==null?"0":(String)rs1.getString("limiteEpo"));
						montoLimiteUsado = new BigDecimal(rs1.getString("limiteUsado")==null?"0":(String)rs1.getString("limiteUsado"));
						
						montoLimiteEPO_dl = new BigDecimal(rs1.getString("limiteEpo_dl")==null?"0":(String)rs1.getString("limiteEpo_dl"));
						montoLimiteUsado_dl = new BigDecimal(rs1.getString("limiteUsado_dl")==null?"0":(String)rs1.getString("limiteUsado_dl"));
						
						
						
					}
					rs1.close();
					ps1.close();


					difMontoLimite = 	montoLimiteEPO.subtract(montoLimite);
					difMontoLimite_dl = 	montoLimiteEPO_dl.subtract(montoLimite_dl);

					log.debug(" montoLimite    == "+montoLimite.toPlainString() +    "  montoLimiteEPO    =="+montoLimiteEPO.toPlainString()    +" montoLimiteUsado ==    "+montoLimiteUsado.toPlainString() +    "  difMontoLimite =="+difMontoLimite.toPlainString());
					log.debug(" montoLimite_dl == "+montoLimite_dl + "  montoLimiteEPO_dl =="+montoLimiteEPO_dl.toPlainString()  +" montoLimiteUsado_dl == "+montoLimiteUsado_dl.toPlainString() + "  difMontoLimite_dl =="+difMontoLimite_dl.toPlainString());
					

					int valor   = montoLimite.compareTo(montoLimiteEPO);
					log.debug(" valor  mn == "+valor);
					if (valor  > 0){
						mensaje = "SobreGirado";
					}
					
					int valor_dl   = montoLimite_dl.compareTo(montoLimiteEPO_dl);
					log.debug(" valor DL  == "+valor_dl );
					if (valor_dl  > 0){
						mensaje_dl = "SobreGirado";
					}
					

					if( !montoLimite.equals(montoLimiteUsado) || !montoLimite_dl.equals(montoLimiteUsado_dl)  ) {
						query	= new StringBuffer();
						query.append(" UPDATE comrel_if_epo "+
												" SET fn_limite_utilizado = ? "+
												" ,fn_limite_utilizado_dl = ? "+
												" WHERE ic_epo = ? "+
												" AND ic_if = ? ");

						log.debug("Update   "+query.toString());

						psUp = con.queryPrecompilado(query.toString());
						psUp.setBigDecimal(1, montoLimite);
						psUp.setBigDecimal(2, montoLimite_dl);
						psUp.setInt(3, Integer.parseInt(cveEpo));
						psUp.setInt(4, Integer.parseInt(cveIF));
						psUp.executeUpdate();
					}

					datos.put("MENSAJE",mensaje);
					datos.put("MONTO_DIFERIENCIA",difMontoLimite);
					datos.put("MENSAJE_DL",mensaje_dl);
					datos.put("MONTO_DIFERIENCIA_DL",difMontoLimite_dl);
				
					log.debug(" datos   == "+datos );

			return datos;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error al validar limite por EPO", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.debug("validaLimiteEpo (S)");
		}
	}
	 // MIGRACION IF
	 /**
	 *  Metodo para obtener  el nombre de la EPO seleccioanda
	 *  pantalla Descuento Electronico -Autorizacion de Descuento
	 * @return
	 * @param ic_epo
	 */
	public String  descripcionEPO(String  ic_epo ){

		String 	qrySentencia 	= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		String descripcion = "";

		try{
			con.conexionDB();

			qrySentencia = " select cg_razon_social as descripcion  from comcat_epo  where ic_epo = ?  ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs.next()){
				descripcion = rs.getString("descripcion")==null?"":rs.getString("descripcion");
			}
			rs.close();
			ps.close();

	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
		return descripcion;
	}

	/**
	 * Obtiene Las cuentas bancarias pendientes por autorizar
	 * 
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 * 		del proceso mas los objetos DocumentoIFWS con los datos de los
	 *    documentos seleccionados
	 */
	public ProcesoIFWSInfoV2 getCtasBancPymePend(String claveUsuario,String password,String claveIF,
	String fechaParamInic,String fechaParamFin,String claveEpo) {
		log.info("AutorizacionDescuentoBean::getCtasBancPymePend(E)("+claveIF+"_"+claveUsuario+")");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		List lCuentas = new ArrayList();
		
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

		//Los ERRORES INESPERADOS se relanzan para que el Catch m�s externo
		//sea el encargado de considerarlos. Agregando al resumen de ejecucion
		//ERROR INESPERADO|
		//Los errores inesperados incluyen por ejemplo a las fallas de
		// BD, de conexion al OID, etc...
	
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}
			

		} catch(Exception e) {
			log.error("getCtasBancPymePend(Error) ", e);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password +")" + "Error=" + e.getMessage());
			erroresInesperados.append(e.getMessage());
			return resultadoProceso;
		}finally {
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
		}
		//****************************************************************************************
		try {

			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			log.debug("getCtasBancPymePend(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("getCtasBancPymePend::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("El usuario no existe para el IF especificado");
				return resultadoProceso; //Termina Proceso
			}

			log.debug(":Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("Fallo la autenticacion del usuario");
				return resultadoProceso; //Termina Proceso
			}
			if(fechaParamInic!=null && !fechaParamInic.equals("")&&fechaParamFin!=null && !fechaParamFin.equals("")){
				if(!Comunes.esFechaValida(fechaParamInic,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				if(!Comunes.esFechaValida(fechaParamFin,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
								
			}
			
			
			if(fechaParamInic!=null && !fechaParamInic.equals("")){
				if(fechaParamFin==null || fechaParamFin.equals("")) throw new Exception("Se requiere fecha de Notificacion final");
				else{
					if(!Comunes.esComparacionFechaValida(fechaParamFin,"ge",fechaParamInic,"dd/MM/yyyy"))
						throw new Exception("La fecha de notificacion final debe ser mayor o igual a la inicial");

					if(Fecha.restaFechas(fechaParamFin,fechaParamInic)>90)
						throw new Exception("El rango de fechas no debe ser mayor a 90 dias");

				}
			}

			
		}catch(Exception e){
			
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + e.getMessage());
			log.error("(Error)", e);
			erroresInesperados.append(e.getMessage());
			return resultadoProceso;
		} finally{
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
		}

		AccesoDB con = null;
		boolean updateOk = true;
		Registros reg=null;
		String Update="update comrel_pyme_if set cs_descarga_ws='S' where " +
		" ic_cuenta_bancaria = ? and ic_if = ? and ic_epo = ?";
		List condUpdate= new ArrayList();
		try{
		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}
			


			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			
			try{

			List conditions = new ArrayList();
				StringBuffer qrySentencia=new StringBuffer();
				qrySentencia.append(
				" select st.EPO,  st.FECHA_FIRMA, st.cuenta_clabe, "+
			 " st.MONEDA,  st.CG_RFC, st.nafin_electronico, st.convenio," +
			 " st.fecha_param,"+
			 " st.ic_cuenta_bancaria, st.IN_NUMERO_SIRAC, st.cg_razon_social, st.cd_plaza, "+
			 "	st.FECHA_PARAMETRIZACION,  st.EXPEDIENTE_DISPONIBLE, Decode(nvl(vp.CS_DOC_VIGENTES,'N'),'N','','S','SI') as PRIORIDAD,  " +
			 " st.cambio_cuenta as cambio_cuenta, st.ic_moneda, st.ic_epo, st.cg_banco, st.cg_numero_cuenta, st.cg_cuenta_clabe as clabe, "+
			 " st.cg_sucursal, st.cs_convenio_unico,st.descarga_ws, st.ic_pyme,  st.nombre_IF"+
			 "	FROM vm_pymes_doc_vig vp,( "+
          " SELECT /*+use_nl(cif pyme cpi cctabanc m epo) index(cctabanc) index(cpi) index(cn)*/ "+
          "        pyme.in_numero_sirac AS IN_NUMERO_SIRAC, " +
          "        pyme.cg_razon_social AS cg_razon_social, " +
			 "			 m.ic_moneda as IC_MONEDA,     "+
          "        m.cd_nombre AS MONEDA, " +
          "        pyme.cg_rfc AS CG_RFC, " +
          "        cn.ic_nafin_electronico AS nafin_electronico, " +
          "        TO_CHAR(cpi.df_vobo_if, 'dd/mm/yyyy') AS FECHA_PARAMETRIZACION, " +
          "        epo.cg_razon_social AS EPO, " +
          "        decode(pyme.cs_expediente_efile, 'S', 'SI', 'N', 'NO') as EXPEDIENTE_DISPONIBLE, " +
			 "			 pyme.ic_pyme as ic_pyme, " +
			 "        epo.ic_epo as ic_epo, "+
			 "			 to_char(pyme.df_firma_conv_unico,'dd/mm/yyyy') as FECHA_FIRMA, "+
			 "			 cctabanc.cg_cuenta_clabe as cuenta_clabe, "+
			 "     TO_CHAR(cpi.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM, " +
			 "			 cpi.cs_cambio_cuenta as cambio_cuenta, "+
			 "			 cctabanc.cg_banco, "+
			 "			 cpi.ic_cuenta_bancaria, "+
			 "			 cctabanc.cg_numero_cuenta, "+
			 "			 cctabanc.cg_cuenta_clabe, "+
			 "			 cctabanc.cg_sucursal, "+
			 "			 DECODE (pyme.cs_convenio_unico, 'S', 'SI', 'NO') convenio, "+
			 "			 nvl(PLAZA.CD_DESCRIPCION,'') as cd_plaza,"+
			 "			 cpi.cs_descarga_ws AS descarga_ws,"+
			 "			 cctabanc.cs_convenio_unico,  "+
			 "        cif.cg_razon_social AS nombre_IF  "+
          "   FROM comrel_pyme_if cpi,  " +
          "        comcat_epo epo, " +
			 "			 comcat_plaza plaza, "+
          "        comrel_cuenta_bancaria cctabanc, " +
          "        comcat_if cif,       " +
          "        comcat_pyme pyme, " +
          "        comcat_moneda m, " +
          "        comrel_nafin cn " +
          "  WHERE cpi.ic_cuenta_bancaria = cctabanc.ic_cuenta_bancaria  " +
          "    AND cpi.ic_if = cif.ic_if " +
          "    AND cpi.ic_epo = epo.ic_epo  " +
			 "		 and plaza.ic_plaza(+) = cctabanc.ic_plaza "+
          "    AND cctabanc.ic_moneda = m.ic_moneda " +
          "    AND cctabanc.ic_pyme = pyme.ic_pyme   " +
          "    AND cpi.df_autorizacion is null " +
          "    AND cn.ic_epo_pyme_if = pyme.ic_pyme " +
          "	   AND cpi.CS_ESTATUS IS NULL "+
          "    AND cpi.cs_borrado = ? " +
          "    AND cctabanc.cs_borrado = ? " +
          "    AND cn.cg_tipo = ? " +
          "    AND cpi.cs_vobo_if = ? " +
          "    AND cif.ic_if = ? " );
          conditions.add("N");
          conditions.add("N");
			 conditions.add("P");
          conditions.add("N");
          conditions.add(claveIF);
			 if(!claveEpo.equals("")){
             qrySentencia.append("   AND epo.ic_epo = ? ");
             conditions.add(claveEpo);
           }
           
			  if(!fechaParamInic.equals("") && !fechaParamFin.equals("")){
					qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy') ");
					qrySentencia.append(" AND cpi.df_vobo_if < TO_DATE(?, 'dd/mm/yyyy')+1 ");
					conditions.add(fechaParamInic);
					conditions.add(fechaParamFin);
				 }
			 
			 
             qrySentencia.append("    AND pyme.cs_expediente_efile = ? ");
             conditions.add("S");
				 
				if(!claveEpo.equals("")){
					qrySentencia.append("  and epo.ic_epo = ? ");
					conditions.add(claveEpo);
				} 
        
		  		qrySentencia.append(") st " +
				"where st.ic_pyme = vp.ic_pyme(+) " +
				"and st.ic_epo = vp.ic_epo(+) ");

				//qrySentencia.append(" ORDER BY st.PYME ");
				
				/*String qrySentenciaX =	"SELECT ST.IC_PYME, ST.CG_RAZON_SOCIAL, ST.IC_EPO, ST.IN_NUMERO_SIRAC, ST.CG_RFC, NVL(VP.CS_DOC_VIGENTES,'N') as expediente  " +
						"FROM vm_pymes_doc_vig VP, (SELECT distinct(PYME.IC_PYME) AS IC_PYME,  " +
						"		decode (PYME.cs_habilitado,'N','*','S',' ')||' '||PYME.CG_RAZON_SOCIAL AS CG_RAZON_SOCIAL,  " +
						"		RELPYMEIF.ic_epo AS IC_EPO,  " +
						"   	PYME.in_numero_sirac AS in_numero_sirac,  " +
						"		PYME.cg_rfc AS cg_rfc " +
						"		FROM COMCAT_MONEDA M, " +
						"		COMCAT_PYME PYME, " +
						"		COMREL_CUENTA_BANCARIA RELCTABANC, " +
						"		COMREL_PYME_IF RELPYMEIF, " +
						"		comcat_epo EPO " +
				//" ,    COMCAT_PLAZA PLAZA  	 "+
						"       , comrel_pyme_epo pe "+
						"		WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA " +
						"		AND PYME.IC_PYME = RELCTABANC.IC_PYME " +
						"		AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
						"		AND EPO.ic_epo = RELPYMEIF.IC_EPO " +
						"		AND EPO.cs_habilitado = 'S' " +
						"		AND RELPYMEIF.CS_VOBO_IF ='N' "+
						"		AND RELPYMEIF.IC_IF = ? " +
						//"   AND RELPYMEIF.IC_PLAZA = PLAZA.IC_PLAZA " +
						"		AND RELPYMEIF.CS_BORRADO='N' " +
						"		AND RELPYMEIF.CS_ESTATUS IS NULL " +
						"		AND pe.ic_pyme = PYME.IC_PYME " +
//============================================================================>> FODEA 002 - 2009 (I) Condici�n para que solo se muetren pymes con No. de SIRAC
				    "   AND PYME.in_numero_sirac IS NOT NULL";
//============================================================================>> FODEA 002 - 2009 (F)
					
					conditions.add(claveIF);
					if(!claveEpo.equals("")) {
						if(!claveEpo.equals("0")) {
							qrySentenciaX += " AND RELPYMEIF.IC_EPO = ? " ;
							qrySentenciaX += "	  AND pe.IC_EPO = ? " ;
							conditions.add(claveEpo);
							conditions.add(claveEpo);
						}
						
					}
					
					if(!fechaParamInic.equals("")&&!fechaParamFin.equals("")){
						qrySentenciaX += " AND RELPYMEIF.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy') ";
						qrySentenciaX += " AND RELPYMEIF.df_vobo_if <  TO_DATE(?, 'dd/mm/yyyy')+1 ";
						conditions.add(fechaParamInic);
						conditions.add(fechaParamFin);
					}
					

					qrySentenciaX += " AND PYME.CS_EXPEDIENTE_EFILE = 'S' ";
					qrySentenciaX += " ) ST " +
										" WHERE ST.IC_EPO = VP.IC_EPO(+) " +
										" AND ST.IC_PYME = VP.IC_PYME(+) " +
										"ORDER BY 2 ";*/
				log.info("qrySentencia----->"+qrySentencia);
				log.info("conditions ---->"+conditions);
				
				reg = con.consultarDB(qrySentencia.toString(),conditions,true);
				String key = "5d75253j4j91j27c582ji69373y90z24";
				ClaseDESBase64 obj = new ClaseDESBase64(key);
				String encript;
				
				while (reg.next()){	
						condUpdate.clear();
						condUpdate.add(reg.getString("IC_CUENTA_BANCARIA"));
						condUpdate.add(claveIF);
						condUpdate.add(reg.getString("IC_EPO"));
						con.ejecutaUpdateDB(Update,condUpdate);
						
						CuentasPymeIFWS cuenta = new CuentasPymeIFWS();
						
						cuenta.setNumeroSiracPyme(reg.getString("IN_NUMERO_SIRAC"));
						cuenta.setRfcPyme(reg.getString("CG_RFC"));
						cuenta.setNombrePyme(reg.getString("CG_RAZON_SOCIAL"));
						cuenta.setClaveMoneda(reg.getString("IC_MONEDA"));
						cuenta.setDescripcionMoneda(reg.getString("MONEDA"));
						cuenta.setClaveEpo(reg.getString("IC_EPO"));
						cuenta.setDescripcionEpo(reg.getString("EPO"));
						cuenta.setBancoServicio(reg.getString("CG_BANCO"));
						cuenta.setNumeroCuenta(reg.getString("CG_NUMERO_CUENTA"));
						cuenta.setNumeroCuentaClabe(reg.getString("CLABE"));
						cuenta.setSucursalCuenta(reg.getString("CG_SUCURSAL"));
						cuenta.setPlazaCuenta(reg.getString("CD_PLAZA"));
						cuenta.setConvenioUnico(reg.getString("CONVENIO"));
						cuenta.setDescargaWS(reg.getString("DESCARGA_WS"));
						cuenta.setExpedienteDigital(reg.getString("EXPEDIENTE_DISPONIBLE"));
						cuenta.setFechaFirmaAutografaCU(reg.getString("FECHA_FIRMA"));
						cuenta.setFechaParamAutoCU(reg.getString("FECHA_PARAM"));
						cuenta.setNumeroNafinElectPyme(reg.getString("NAFIN_ELECTRONICO"));
						cuenta.setPrioridad(reg.getString("PRIORIDAD"));
						cuenta.setCambioCuenta(reg.getString("CAMBIO_CUENTA"));	
						encript=obj.encriptar(cuenta.getRfcPyme());
						cuenta.setClaveExpediente(URLEncoder.encode(encript, "UTF-8"));
						lCuentas.add(cuenta);

						
					} //fin while
				}catch(Throwable t) {
						throw new Exception("Error al generar las cuentas", t);
			}
			

			} catch(Throwable t) {
				throw new Exception("Error al obtener los datos", t);
			}

			
			CuentasPymeIFWS[] arrDocumentos = null;
			if (lCuentas.size() > 0) {
				arrDocumentos = new CuentasPymeIFWS[lCuentas.size()];
				lCuentas.toArray(arrDocumentos);
			}

			resultadoProceso.setCuentas(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
		} catch( Throwable  t) { //en esta secci�n caen todos los errores inesperados
			
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
			log.error("(Error)", t);

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("Hubo Errores Inesperados "+ erroresInesperados.toString());

					/* llamado del metodo para enviar correo de Errores Inesperados siemprey cuando hayan*/
			

			return resultadoProceso;
		} finally {
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
			log.info("AutorizacionDescuentoBean::getCtasBancPymePend(S)("+claveIF+"_"+claveUsuario+")");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(updateOk);
				con.cierraConexionDB();
			}
		}
		return resultadoProceso;
	}
	
	
	/**
	 * Obtiene Las cuentas bancarias Autorizadas
	 * 
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 * 		del proceso mas los objetos DocumentoIFWS con los datos de los
	 *    documentos seleccionados
	 */
	public ProcesoIFWSInfoV2 getCtasBancPymeAutorizadas(String claveUsuario,String password,String claveIF,
	String fechaParamInic,String fechaParamFin,String claveEpo) {
		log.info("AutorizacionDescuentoBean::getCtasBancPymeAutorizadas(E)("+claveIF+"_"+claveUsuario+")");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		List lCuentas = new ArrayList();
		
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

		//Los ERRORES INESPERADOS se relanzan para que el Catch m�s externo
		//sea el encargado de considerarlos. Agregando al resumen de ejecucion
		//ERROR INESPERADO|
		//Los errores inesperados incluyen por ejemplo a las fallas de
		// BD, de conexion al OID, etc...
	
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}
			

		} catch(Exception e) {
			log.error("getCtasBancPymeAutorizadas(Error) ", e);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password +")" + "Error=" + e.getMessage());
			erroresInesperados.append(e.getMessage());
			return resultadoProceso;
		}finally {
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS_A");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
		}
		//****************************************************************************************
		try {
		
			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			log.debug("getCtasBancPymeAutorizadas(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("getCtasBancPymeAutorizadas::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("El usuario no existe para el IF especificado");
				return resultadoProceso; //Termina Proceso
			}

			log.debug(":Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("Fallo la autenticacion del usuario");
				return resultadoProceso; //Termina Proceso
			}
			if(fechaParamInic!=null && !fechaParamInic.equals("")&&fechaParamFin!=null && !fechaParamFin.equals("")){
				if(!Comunes.esFechaValida(fechaParamInic,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				if(!Comunes.esFechaValida(fechaParamFin,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
								
			}
			
			
			if(fechaParamInic!=null && !fechaParamInic.equals("")){
				if(fechaParamFin==null || fechaParamFin.equals("")) throw new Exception("Se requiere fecha de Notificacion final");
				else{
					if(!Comunes.esComparacionFechaValida(fechaParamFin,"ge",fechaParamInic,"dd/MM/yyyy"))
						throw new Exception("La fecha de notificacion final debe ser mayor o igual a la inicial");

					//if(Fecha.restaFechas(fechaParamFin,fechaParamInic)>90)
					//	throw new Exception("El rango de fechas no debe ser mayor a 90 dias");

				}
			}

			
		}catch(Exception e){
			
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + e.getMessage());
			log.error("(Error)", e);
			erroresInesperados.append(e.getMessage());
			return resultadoProceso;
		} finally{
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS_A");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
		}

		AccesoDB con = null;
		boolean updateOk = true;
		Registros reg=null;

		try{
		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}
			


			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			
			try{

			List conditions = new ArrayList();
				
				StringBuffer qrySentencia= new StringBuffer();
				qrySentencia.append(
        " SELECT  "   +
        "     relpymeif.cs_vobo_if,"   +
        "     relctabanc.ic_cuenta_bancaria,"   +
        "     decode (pyme.cs_habilitado, 'N', '* ', 'S', ' ') as habilitado, "+
		  "     pyme.cg_razon_social , "   +
		   
        "     relctabanc.cg_banco,"   +
        "     relctabanc.cg_numero_cuenta,"   +
        "     m.cd_nombre as MONEDA,"   +
		  "     m.ic_moneda,"+
        "     relpymeif.ic_epo,"   +
        "     pyme.ic_pyme,   "   +
        "     relctabanc.cg_sucursal,"   +
        "     epo.cg_razon_social as EPO,"   +
        "     pyme.cg_rfc,"   +
        "     NVL(plaza.cd_descripcion, '') as cd_plaza,    "   +
        "     NVL(PE.cg_pyme_epo_interno, '') as NUMPROVEEDOR,    "   +
				"	  NVL(pyme.IN_NUMERO_SIRAC, '') as IN_NUMERO_SIRAC, " +  //Numero sirac de la pyme
//============================================================================>> FODEA 020 - 2009 (I) Se obtiene 4 valores de Convenio Unico
			"      relctabanc.CG_CUENTA_CLABE as CLABE " +
			"	,		 TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') as FECHA_FIRMA  " +
			" ,    TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') as FECHA_PARAM " +
			" ,    DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') as CONVENIO, " +		
//============================================================================>> FODEA 020 - 2009 (F)		
			"	decode(pyme.cs_expediente_efile, 'S', 'SI', 'N', 'NO') as EXPEDIENTE_DISPONIBLE, " +
			"			 RELPYMEIF.cs_cambio_cuenta as cambio_cuenta, "+
        "     crn.ic_nafin_electronico as nafin_electronico"+//FODEA 018 - 2009 ACF
        " FROM comrel_cuenta_bancaria relctabanc, "   +
        " comrel_pyme_if relpymeif, "   +
        " comrel_nafin cna,"   +
        " comcat_epo epo, "   +
        " comcat_plaza plaza, "   +
        " comrel_pyme_epo pe,"   +
        " comcat_pyme pyme,  "   +
        " comrel_nafin crn,  "   +//FODEA 018 - 2009 ACF
        " comcat_moneda m"   +
        " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
        " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
        " AND pyme.ic_pyme = crn.ic_epo_pyme_if "   +//FODEA 018 - 2009 ACF
        " AND crn.cg_tipo = 'P' "   +//FODEA 018 - 2009 ACF
        " AND relctabanc.ic_moneda = m.ic_moneda "   +
        " AND relctabanc.ic_plaza = plaza.ic_plaza(+) "   +
        " AND relpymeif.ic_epo = epo.ic_epo"   +
        " AND relpymeif.ic_if = ? "+
        " AND relpymeif.cs_borrado = 'N' "   +
        " AND pe.ic_pyme = pyme.ic_pyme "   +
        " AND cna.cg_tipo = 'P'"   +
        " AND cna.ic_epo_pyme_if = relctabanc.ic_pyme"   +
        " AND relpymeif.ic_epo = pe.ic_epo "   +
        " AND relpymeif.cs_vobo_if = 'S'"+
//============================================================================>> FODEA 002 - 2009 (I) Condici�n para seleccionar �nicamente pymes que tienen numero SIRAC
				" AND pyme.in_numero_sirac IS NOT NULL");
//============================================================================>> FODEA 002 - 2009 (F)
			conditions.add(claveIF);
			if (!claveEpo.equals("")){
				qrySentencia.append(" AND relpymeif.ic_epo =? ");
				conditions.add(claveEpo);
			}
				
			if(!fechaParamInic.equals("")&&!fechaParamFin.equals("")){
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  >= TO_DATE (?  , 'DD/MM/YYYY')");				
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  < TO_DATE (? , 'DD/MM/YYYY')+1 ");	 
				conditions.add(fechaParamInic);
				conditions.add(fechaParamFin);
			}

		qrySentencia.append(" AND PYME.CS_EXPEDIENTE_EFILE = 'S' ");
		qrySentencia.append(" ORDER BY 3, 4 ");
		reg=con.consultarDB(qrySentencia.toString(),conditions,true);
		String key = "5d75253j4j91j27c582ji69373y90z24";
      ClaseDESBase64 obj = new ClaseDESBase64(key);
		String encript;
		
		while (reg.next()){
						
						
						CuentasPymeIFWS cuenta = new CuentasPymeIFWS();
						
						cuenta.setNumeroSiracPyme(reg.getString("IN_NUMERO_SIRAC"));
						cuenta.setRfcPyme(reg.getString("CG_RFC"));
						cuenta.setNombrePyme(reg.getString("CG_RAZON_SOCIAL"));
						cuenta.setClaveMoneda(reg.getString("IC_MONEDA"));
						cuenta.setDescripcionMoneda(reg.getString("MONEDA"));
						cuenta.setClaveEpo(reg.getString("IC_EPO"));
						cuenta.setDescripcionEpo(reg.getString("EPO"));
						cuenta.setBancoServicio(reg.getString("CG_BANCO"));
						cuenta.setNumeroCuenta(reg.getString("CG_NUMERO_CUENTA"));
						cuenta.setNumeroCuentaClabe(reg.getString("CLABE"));
						cuenta.setSucursalCuenta(reg.getString("CG_SUCURSAL"));
						cuenta.setPlazaCuenta(reg.getString("CD_PLAZA"));
						cuenta.setConvenioUnico(reg.getString("CONVENIO"));
						//cuenta.setDescargaWS(reg.getString("DESCARGA_WS"));
						cuenta.setExpedienteDigital(reg.getString("EXPEDIENTE_DISPONIBLE"));
						cuenta.setFechaFirmaAutografaCU(reg.getString("FECHA_FIRMA"));
						cuenta.setFechaParamAutoCU(reg.getString("FECHA_PARAM"));
						cuenta.setNumeroNafinElectPyme(reg.getString("NAFIN_ELECTRONICO"));
						//cuenta.setPrioridad(reg.getString("EXPEDIENTE"));
						cuenta.setCambioCuenta(reg.getString("CAMBIO_CUENTA"));
						encript=obj.encriptar(cuenta.getRfcPyme());
						cuenta.setClaveExpediente(URLEncoder.encode(encript, "UTF-8"));
						lCuentas.add(cuenta);

						
					} //fin while
				}catch(Throwable t) {
						throw new Exception("Error al generar las cuentas", t);
			}
			
			} catch(Throwable t) {
				throw new Exception("Error al obtener los datos", t);
			}
			
			CuentasPymeIFWS[] arrDocumentos = null;
			if (lCuentas.size() > 0) {
				arrDocumentos = new CuentasPymeIFWS[lCuentas.size()];
				lCuentas.toArray(arrDocumentos);
			}
			resultadoProceso.setCuentas(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
		} catch( Throwable  t) { //en esta secci�n caen todos los errores inesperados
			
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
			log.error("(Error)", t);

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("Hubo Errores Inesperados "+ erroresInesperados.toString());

					/* llamado del metodo para enviar correo de Errores Inesperados siemprey cuando hayan*/
			

			return resultadoProceso;
		} finally {
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"CTAS_A");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
			log.info("AutorizacionDescuentoBean::getCtasBancPymeAutorizadas(S)("+claveIF+"_"+claveUsuario+")");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(updateOk);
				con.cierraConexionDB();
			}
		}
		return resultadoProceso;
	}





/**
	 * Realiza el cambio de estatus de las cuentas en proceso de autorizacion IF,
	 * segun corresponda. La carga puede ser parcial, lo que significa que si
	 * hay un error en un documento... esto no afecta a los dem�s documentos correctos.
	 *
	 * El proceso, regresa un objeto ProcesoIFWSInfo, el cual contiene informacion
	 * acerca de la ejecuci�n.
	 *
	 * Atrav�s de ProcesoIFWSInfo.getCodigoEjecucion() y ProcesoIFWSInfo.getCuentas()
	 * se puede determinar cual fue el resultado de la ejecuci�n del proceso, de la
	 * siguiente manera:
	 *
	 * codigoEjecucion == 1 y documentos == null.
	 * 	Todos los documentos fueron procesados correctamente.
	 * codigoEjecucion == 1 y documentos != null.
	 * 	Hay una o varias cuentas que presentaron algun tipo de error.
	 *    El error puede ser normal de aplicacion (p.e. que el c�digo de rechazo no esta dado de alta en la aplicacion)
	 *    o puede ser error inesperado del sistema (Error en BD al realizar el update de cambio de estatus de la cuenta )
	 * codigoEjecucion == 2 y documentos != null.
	 * 	Esta combinaci�n puede darse cuando existe un error que impide el procesamiento de las cuentas.
	 *    El error puede ser normal de aplicacion (p.e. el uso de un certificado digital que no es del usuario)
	 *    o puede ser un error inesperado del sistema (p.e. una falla de conexion al sistema de autenticacion de usuarios)
	 *
	 *
	 * -------------------------------------------------------------------------------------------------------------------
	 * <br><br>
	 *
	 * codigoEjecucion == 2 y documentos == null.
	 * 	Esta combinaci�n, m�s bien, se podr�a presentar en el WebService
	 *    que invoca este m�todo, lo cual significaria que hubo problemas
	 *    para accesar este EJB.
	 *
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param documentos Arreglo de DocumentoIFWS que contiene la informaci�n
	 * 		de los documentos a procesar unicamente.
	 * @return Objeto ProcesoIFWSInfo que contiene:
	 *
	 * 	codigoEjecucion Codigo de ejecuci�n resultante del proceso:
	 *    	1.- El proceso puedo finalizar completamente
	 *       2.- El proceso fue interrumpido debido a un error normal de aplicacion.
	 *       	por ejemplo, el password no es valido
	 *          � el proceso fue interrumpido debido a un funcionamiento anormal del sistema.
	 *          por ejemplo, un error de conexion a la Base de Datos.
	 *    resumenEjecucion Contiene una descripcion en texto del resultado de la ejecucion.
	 *    	Puede contener como texto, datos del acuse resultante de la ejecucici�n o
	 *       en caso de error. Si es normal de aplicacion comenzara con "ERROR|" y luego el detalle
	 *       del error. Si es un error inesperado de sistema comenzara con "ERROR INESPERADO|" y
	 *       luego el detalle del error.
	 *    documentos Contiene la lista de objetos DocumentoIFWS con los
	 *    	documentos que tuvieron error (en caso de que exista error) o que no
	 *    	pudieron ser procesados, ya sea debido a un error inesperado
	 *       (en cuyo caso la descripcion comienza con "ERROR INESPERADO|") o como o un
	 *    	error normal de aplicacion (en cuyo caso la descripcion comienza con "ERROR|")
	 */
	 public ProcesoIFWSInfoV2 confirmacionCuentasIFWS (String claveIF,String claveUsuario,CuentasPymeIFWS[] cuentas) {
		
		log.info("confirmacionCuentasIFWS(E)");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		resultadoProceso.setCodigoEjecucion(new Integer(1));
		List lCuentasError = new ArrayList();

		int i = 0; //numero de registros procesados
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		int iClaveIF = 0;
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa
		boolean error = false;
		List conditions = new ArrayList();
		Registros reg=null;
		
		UtilUsr utils = new UtilUsr();

		List tipoFactoraje = new ArrayList();	
		Hashtable alParamEPO1 = new Hashtable();
		try{
			iClaveIF = Integer.parseInt(claveIF);
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
			Afiliacion beanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		
			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			PreparedStatement ps = null;
			String qrySentencia = null;
			try {
				//Creo que este alter es para que funcione adecuadamente la funcion sigfechahabilxepo
				qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
				con.terminaTransaccion(true);
			} catch(Throwable t) {
				throw new Exception("Error al establecer la informacion de idioma en BD");
			}

			//----------------------- Definicion de queries que se ocupan dentro del ciclo ---------------

			
				
				int contadorPyme;
				
				//Query para obtener la ic_cuenta_bancaria
				String strSQL =
					" SELECT RELCTABANC.IC_CUENTA_BANCARIA, PYME.CS_CONVENIO_UNICO,\n" + 
					"  RELPYMEIF.IC_EPO ,PYME.IC_PYME, PYME.CG_RFC, RELPYMEIF.CS_VOBO_IF, " + 
				   " EPO.CG_RAZON_SOCIAL AS NOMBRE_EPO," + 
				   " PYME.CG_RAZON_SOCIAL AS NOMBRE_PYME," + 
					"  M.CD_NOMBRE AS MONEDA," + 
					" M.IC_MONEDA AS IC_MONEDA," + 
					" crn.IC_NAFIN_ELECTRONICO AS NUM_NAFIN" + 	
					" , i.CG_RAZON_SOCIAL AS NOMBRE_IF " +
					" FROM COMCAT_MONEDA M, COMCAT_PYME PYME, \n" + 
					" COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF,comcat_epo EPO \n" + 
					" ,COMCAT_PLAZA PLAZA \n" + 
					" ,comrel_pyme_epo pe \n" + 
					" ,comrel_nafin crn \n" + 
					" , comcat_if i "+
					" WHERE RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA \n" + 
					" AND PYME.IC_PYME = RELCTABANC.IC_PYME AND RELCTABANC.IC_MONEDA = M.IC_MONEDA AND EPO.ic_epo = RELPYMEIF.IC_EPO \n" + 
					"      AND RELCTABANC.IC_PLAZA = PLAZA.IC_PLAZA(+) \n" + 
					" AND pyme.ic_pyme = crn.ic_epo_pyme_if\n" + 
					" AND crn.cg_tipo = 'P'\n" + 
					" AND RELPYMEIF.CS_BORRADO='N' \n" + 
					" AND pe.ic_pyme = PYME.IC_PYME \n" + 
					" AND RELPYMEIF.IC_EPO = PE.IC_EPO \n" + 
					" AND RELPYMEIF.CS_VOBO_IF = 'N' \n" + 
					" AND RELPYMEIF.IC_IF = ?\n" + 
					" AND PYME.CG_RFC  =  ?\n" + 
					" AND RELPYMEIF.IC_EPO = ? \n" + 
					" AND PE.IC_EPO =  ? \n" + 
					" AND M.ic_moneda = ?"+
					" AND RELPYMEIF.IC_IF = i.ic_if  ";
		
				String strSQLPymeNueva="SELECT COUNT (1) as contador " +
												"  FROM comrel_pyme_epo " +
												" WHERE cs_aceptacion = ? " +
												"  AND ic_pyme = ?";
											
				String strPymeCuenta="SELECT NVL(cs_cambio_cuenta,'N') as cs_cambio_cuenta FROM comrel_pyme_if " +
					" WHERE ic_if = ? AND ic_epo = ? AND ic_cuenta_bancaria = ? ";
				
				String sQueryConvenioUnico =
									" SELECT CS_CONVENIO_UNICO " +
									"  FROM comcat_if " +
									" WHERE ic_if = ? " ;
				
				String SQLExec = "UPDATE comrel_pyme_epo SET cs_aceptacion = 'H', " +
					" df_aceptacion = SYSDATE WHERE ic_epo = ? " +
					" AND ic_pyme = ? ";
					

				String eliminaRegistroComhis_cambios_cta = "UPDATE COMHIS_CAMBIOS_CTA SET CS_AUTORIZA_IF = 'S' " +
															"WHERE IC_IF = ? " +
															"AND IC_EPO = ? " +
															"AND IC_CUENTA_BANCARIA = ? ";
				String ValBitquery="SELECT COUNT (1) as cuenta " +
													"  FROM BIT_CONVENIO_UNICO " +
													" WHERE IC_PYME  = ? " +
													"  AND IC_IF = ?  ";
													
				String insertBitacoraA ="INSERT INTO BIT_CONVENIO_UNICO " +
																	"( IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
																	") VALUES " +
																	"( ?,?,SYSDATE,SYSDATE,?) ";
																	
				String insertBitacoraB="INSERT INTO BIT_CONVENIO_UNICO " +
																		"( " +
																		"  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
																		") VALUES " +
																		"( ? ,?,SYSDATE,TO_DATE((SELECT TO_CHAR(DF_FIRMA_CONV_UNICO,'DD/MM/YYYY hh24:mi:ss') FROM COMCAT_PYME WHERE IC_PYME = ? ),'DD/MM/YYYY hh24:mi:ss'),?) ";
				//String insertBitacoraC= "INSERT INTO BIT_CONVENIO_UNICO " +
				//														"( " +
				//														"  IC_IF ,  IC_PYME  ,  DF_AUTORIZACION_IF ,  DF_ACEPTACION_PYME ,  IC_USUARIO           " +
				//														") VALUES " +
				//														"( ?,?,SYSDATE,TO_DATE((SELECT TO_CHAR(MIN(DF_ACEPTACION_PYME),'DD/MM/YYYY hh24:mi:ss') FROM BIT_CONVENIO_UNICO WHERE IC_PYME = ?),'DD/MM/YYYY hh24:mi:ss'),? ) ";
				String strSQLPymeVieja="SELECT COUNT(1) as contador" +
													"  FROM bit_convenio_unico " +
													" WHERE ic_pyme = ? ";
													
				//String qryNEif= " SELECT ic_nafin_electronico  " +
				//							"   FROM comrel_nafin  " +
				//							"  WHERE ic_epo_pyme_if = ? ";
				String insertBitacoraGral="\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
						"\n IC_CAMBIO,  " +
						"\n CC_PANTALLA_BITACORA,  " +
						"\n CG_CLAVE_AFILIADO, " +
						"\n IC_NAFIN_ELECTRONICO, " +
						"\n DF_CAMBIO, " +
						"\n IC_USUARIO, " +
						"\n CG_NOMBRE_USUARIO, " +
						"\n CG_ANTERIOR, " +
						"\n CG_ACTUAL, " +
						"\n IC_GRUPO_EPO, " +
						"\n IC_EPO ) " +
						"\n VALUES(  " +
						"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
						"\n 'CUENTABANCIF', " +
						"\n 'P', " +
						"\n ? , " +
						"\n SYSDATE, " +
						"\n ?,  " +
						"\n ?, " +
						"\n '',  " +
						"\n ?, " +
						"\n '', " +
						"\n '' ) ";
			String qryRelIFPYMEOk="";
			
			
					//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			List epoFactV = new ArrayList();			
			
			for (i=0; i < cuentas.length; i++) {
				CuentasPymeIFWS cuent = null;
				cuent = cuentas[i];
				String claveEpo = cuent.getClaveEpo();
				
				alParamEPO1 = BeanParamDscto.getParametrosEPO(claveEpo,1);	
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
				if(bOperaFactorajeVencido.equals("S"))  {
					epoFactV.add(claveEpo);						
				}
			}
		
			//----------------------- ----------------------------------------------- ---------------
			// Una vez que se entra al ciclo, aun los errores inesperados no interrumpen
			// la ejecuci�n del procesos...

			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv Inicio de Ciclo vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
			for (i=0; i < cuentas.length; i++) {
				CuentasPymeIFWS cuent = null;
				StringBuffer detalleError = null;
				try { //Como se permiten cargas parciales, se debe cachar cualquier error y continuar con el siguiente registro
					cuent = cuentas[i];
					log.debug("confirmacionCuentasIFWS():: Cuenta Recibida[" + i + "]=" + cuent);
					detalleError = new StringBuffer();

				

					//************************Validaci�n de atributos de documentos:**************************
					String rfc;
					int claveMoneda=0;
					int claveEpo=0;
					
					
					rfc = cuent.getRfcPyme();
					if(rfc.equals(""))
						detalleError.append("El valor del RFC de la cuenta no es valida\n");
					
					try {
							claveMoneda = Integer.parseInt(cuent.getClaveMoneda());
						} catch(NumberFormatException e) {
							detalleError.append("El valor de la clave de moneda de la cuenta no es un numero valido\n");
						}
					try {
							claveEpo = Integer.parseInt(cuent.getClaveEpo());
						} catch(NumberFormatException e) {
							detalleError.append("El valor de la clave de la EPO de la cuenta no es un numero valido\n");
						}
					conditions.clear();
					conditions.add(claveIF);
					conditions.add(rfc);
					conditions.add(claveEpo+"");
					conditions.add(claveEpo+"");
					conditions.add(claveMoneda+"");
					reg=con.consultarDB(strSQL,conditions);
					
					String cuentaBanc="";
					String convenioUnico="";
					String icPyme="";
					String sIfConvenioUnico="";
					//Valida la existencia de la clave del documento (ic_documento)
					String ctaOk="";
					
					String ic_pyme="", nombreEpo = "", nombrePyme ="", nombreMoneda ="",
					nun_elecPyme ="", orden= "", ic_moneda ="", nombreIF ="";
					
					try {
											
						if (reg.next()) {
							cuentaBanc=reg.getString("IC_CUENTA_BANCARIA");
							convenioUnico=reg.getString("CS_CONVENIO_UNICO");
							icPyme=reg.getString("IC_PYME");
							ctaOk=reg.getString("CS_VOBO_IF");
							
							ic_pyme=reg.getString("IC_PYME");
							nombreEpo=reg.getString("NOMBRE_EPO");
							nombrePyme=reg.getString("NOMBRE_PYME");
							nombreMoneda=reg.getString("MONEDA");
							nun_elecPyme=reg.getString("NUM_NAFIN");
							ic_moneda=reg.getString("IC_MONEDA");
							nombreIF = reg.getString("NOMBRE_IF");
							
							if(ctaOk.equals("S")){
								detalleError.append("La Cuenta ya fue confirmada\n");
							}
						} else {
							detalleError.append("La Cuenta no existe para los parametros establecidos\n");
						}
					} catch(Throwable t) {
						throw new Exception("Error al obtener los datos de la Cuenta.",t);
					}
					if (detalleError.length()>0) { //Si hay Errores
						cuent.setErrorDetalle("ERROR|" + detalleError.toString());
						cuent.setTipoError(1);
						lCuentasError.add(cuent);
						continue;		//Continua con la siguiente Cuenta
					} else {
						error = false;
						cuent.setErrorDetalle("");
						cuent.setTipoError(0);
					}
					
					try{
					//validacion de si la pyme es nueva o vieja
					conditions.clear();
					conditions.add("H");
					conditions.add(icPyme);					
					reg=con.consultarDB(strSQLPymeNueva,conditions,true);
					contadorPyme=0;
					if(reg.next()){
						contadorPyme=Integer.parseInt(reg.getString("CONTADOR"));
					}
					//Query cambio cuenta
					
					conditions.clear();
					conditions.add(claveIF);
					conditions.add(claveEpo+"");
					conditions.add(cuentaBanc);
					
					reg=con.consultarDB(strPymeCuenta,conditions,true);
					
					
					String dfvoboif = ", df_vobo_if = SYSDATE";
					if(reg.next())
					if(reg.getString("CS_CAMBIO_CUENTA").equals("S")){
						dfvoboif="";
					}
					
					String relacionConvenioUnico = "";
					
					conditions.clear();
					conditions.add(claveIF);
					reg=con.consultarDB(sQueryConvenioUnico,conditions,true);
					if(reg.next())
					 sIfConvenioUnico=reg.getString("CS_CONVENIO_UNICO");
					
					if (sIfConvenioUnico.equals("S") && convenioUnico.equals("S")){
						relacionConvenioUnico = " , CS_CONVENIO_UNICO = 'S' ";
					}
					
					 qryRelIFPYMEOk = "UPDATE comrel_pyme_if SET cs_vobo_if = 'S' "+dfvoboif + relacionConvenioUnico +
					" WHERE ic_if = ? AND ic_epo = ? "+
					" AND ic_cuenta_bancaria = ? " ;
					
					}catch (Throwable t) {
							throw new Exception("Error al actualizar las Cuentas.",t);
					}
					
					//VALIDAR PARA METER EN BITACORA
					//validar que esa relacion no exista en la bitacora anteriormente
					//este contador indicara si ya hubo un guardado de esa misma relacion anteriormente,
					//puede ser que la cuenta se borre y entonces vendra una segunda autorizacion,
					//asi que con esto se valida no mas insert con misma pyme/if en bitacora ni actualizacion de fecha.
					try{
					int contadorValBit=0;
					
					conditions.clear();
					conditions.add(icPyme);
					conditions.add(claveIF);
					reg=con.consultarDB(ValBitquery,conditions,true);
					
					if(reg.next()){
						contadorValBit=Integer.parseInt(reg.getString("CUENTA"));
					}
					
					
					//SACANDO EL CONVENIO UNICO DE LA PYME
					//Si los dos manejan convenio unico
					
					if (sIfConvenioUnico.equals("S") && convenioUnico.equals("S") && contadorValBit==0){
						
						if(contadorPyme==0){
							//LA PYME ES NUEVA SI EL CONTADOR ES 0
							conditions.clear();
							conditions.add(claveIF);
							conditions.add(icPyme);
							conditions.add(claveUsuario);
							con.ejecutaUpdateDB(insertBitacoraA,conditions);
						}else{
							//SI EL CONTADOR NO ES CERO SE TRATA DE UNA PYME QUE ES VIEJA
							//validar si es la primera vez de esa pyme en la bitacora.
							int contadorExisteEnCU = 0;
							conditions.clear();
							conditions.add(icPyme);
							reg=con.consultarDB(strSQLPymeVieja,conditions,true);
							if(reg.next()){
								contadorExisteEnCU=Integer.parseInt(reg.getString("CONTADOR"));
							}
							
							if(contadorExisteEnCU==0){
								conditions.clear();
								conditions.add(claveIF);
								conditions.add(icPyme);
								conditions.add(icPyme);
								conditions.add(claveUsuario);
								con.ejecutaUpdateDB(insertBitacoraB,conditions);
							}else{
								conditions.clear();
								conditions.add(claveIF);
								conditions.add(icPyme);
								conditions.add(icPyme);
								conditions.add(claveUsuario);
								con.ejecutaUpdateDB(insertBitacoraB,conditions);
							}
						}
					}
					
										
						
						conditions.clear();
						conditions.add(claveIF);
						conditions.add(claveEpo+"");
						conditions.add(cuentaBanc);
						con.ejecutaUpdateDB(qryRelIFPYMEOk,conditions);
										
				
								
					conditions.clear();
					conditions.add(claveEpo+"");
					conditions.add(icPyme);
					con.ejecutaUpdateDB(SQLExec,conditions);
					
					
					conditions.clear();
					conditions.add(claveIF);
					conditions.add(claveEpo+"");
					conditions.add(cuentaBanc);
					con.ejecutaUpdateDB(eliminaRegistroComhis_cambios_cta,conditions);
					
					
					log.debug("qryRelIFPYMEOk : "+qryRelIFPYMEOk   +"   conditions  " +conditions);
					log.debug("SQLExec : "+SQLExec +"  conditions  " +conditions);
					log.debug("eliminaRegistroComhis_cambios_cta : "+eliminaRegistroComhis_cambios_cta+"      conditions  " +conditions);
					
					List lstLogin = utils.getUsuariosxAfiliado(icPyme, "P");
					
					
				if(!lstLogin.isEmpty()) {	//Si tiene una cuenta ya existente en el OID de -----N@E-----
					String loginUsuario = lstLogin.get(0).toString();
					utils.setPerfilUsuario(loginUsuario, "ADMIN PYME");
				} else {
					//NO existe la cuenta en el OID de N@E.

					if (utils.getExisteSolicitudAltaUsuario(icPyme, "P")) {
						//Si no existe la cuenta en el OID pero ya hay una solicitud de alta de usuario en el ARGUS, ajusta el perfil
						utils.setPerfilSolicitudAltaUsuario(icPyme, "P", "ADMIN PYME");
					} else {
						//No existe la cuenta de N@E en OID y no hay solicitud de alta de usuario

						SolicitudArgus solicitud = new SolicitudArgus();
						Usuario usr = new Usuario();
						usr.setClaveAfiliado(icPyme);
						usr.setTipoAfiliado("P");
						solicitud.setUsuario(usr);

						String loginAsociado = utils.getCuentaUsuarioAsociada(icPyme);						
						if (loginAsociado != null && !loginAsociado.equals("") ) {
							//Existe una cuenta (del Sistema de compras) en el OID que
							//est� preasociada al proveedor, solo se le agregan el perfil, tipo de afiliado
							//y clave de afiliado a la cuenta de manera que pueda ingresar a N@E.
							usr.setLogin(loginAsociado);
							utils.actualizarUsuario(solicitud);
							//La actualizacion del usuario no contempla la actualizacion del perfil.
							//por lo cual se hace en una instrucci�n aparte.
							utils.setPerfilUsuario(loginAsociado, "ADMIN PYME");
						} else {
							//Si no hay cuenta ni de N@E ni compras que pueda usarse, se genera la solicitud

							usr.setPerfil("ADMIN PYME");
							utils.complementarSolicitudArgus(solicitud, "P", icPyme);

							Registros reg1 = beanAfiliacion.getDatosContacto(solicitud.getRFC(), "");
							if (reg1.next()) {
								usr.setNombre(reg1.getString("contacto_nombre"));
								usr.setApellidoPaterno(reg1.getString("contacto_appat"));
								usr.setApellidoMaterno(reg1.getString("contacto_apmat"));
								usr.setEmail(reg1.getString("contacto_email"));
							}

							utils.registrarSolicitudAltaUsuario(solicitud);
						}
					}
				}
				
				/*
				conditions.clear();
				conditions.add(claveIF);
				String cadena="";
				String nafElecIF="";
				reg=con.consultaDB(qryNEif,conditions,true);
				if(reg.next()){
					nafElecIF=reg.getString("IC_NAFIN_ELECTRONICO");
				}*/
				
				int iNoNafinElectronico = 0;
				//UtilUsr utilUsr = new UtilUsr();
				UtilUsr utilUsr = new UtilUsr();
				Usuario usuario = utilUsr.getUsuario(claveUsuario);
				String strNombreUsuario = usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno() + " " + usuario.getNombre();
				String qryTraeNumNafinElect = 
					" SELECT ic_nafin_electronico " +
					" FROM comrel_nafin " +
					" WHERE ic_epo_pyme_if = ? "+
					" AND cg_tipo = ? ";
				conditions.clear();
				conditions.add(claveIF);
				conditions.add(usuario.getTipoAfiliado());
				reg=con.consultarDB(qryTraeNumNafinElect,conditions,true);	
				if(reg.next()){
					iNoNafinElectronico=Integer.parseInt(reg.getString("IC_NAFIN_ELECTRONICO"));
				}
				
				String strNombreEmpresa="";
				String qryNombreEmp  = 
				" SELECT cg_razon_social AS NOMBRE " +
				" FROM COMCAT_IF " +
				" WHERE ic_if = ? ";
				
				if(usuario.getTipoAfiliado().equals("N")){
					if("ADMIN BANCOMEXT".equals(usuario.getPerfil())) {
							strNombreEmpresa = "Administrador Bancomext";
						} else {
								strNombreEmpresa = "Administrador Nafin";
						}
				}else{
					conditions.clear();
					conditions.add(claveIF);
					reg = con.consultarDB(qryNombreEmp, conditions);
					if(reg.next()) {
						strNombreEmpresa = reg.getString(1);
					}
				}
				
				conditions.clear();
				conditions.add(iNoNafinElectronico+"");
				conditions.add(claveUsuario);
				conditions.add(strNombreUsuario);
				conditions.add("Cuenta Autorizada = "+strNombreEmpresa+" "+iNoNafinElectronico);
				
				con.ejecutaUpdateDB(insertBitacoraGral,conditions);
					}catch(Throwable t){
						throw new Exception("Error al insertar bitacora.",t);
					}
					
					
				//*******INICIA ************** FODEA DESCUENTO AUTOMATICO***************************
							
				alParamEPO1 = BeanParamDscto.getParametrosEPO(String.valueOf(claveEpo),1);	
				String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
				String nombreFactoraje ="";
				tipoFactoraje = new ArrayList();	
				tipoFactoraje.add("N");// Factoraje Normal
				if(bOperaFactorajeVencido.equals("S"))  {
					tipoFactoraje.add("V"); // Factoraje Vencido
				}
				
				StringBuffer SQL	= 	new StringBuffer(); 					
				SQL.append("	INSERT INTO COMREL_DSCTO_AUT_PYME	");
				SQL.append("		(											");
				SQL.append("			IC_PYME								");
				SQL.append("			,IC_EPO								");
				SQL.append("			,IC_IF								");
				SQL.append("			,IC_MONEDA							");
				SQL.append("			,CS_ORDEN							");
				SQL.append("			,CS_DSCTO_AUTOMATICO_DIA		");
				SQL.append("			,CS_DSCTO_AUTOMATICO_PROC		");
				SQL.append("			,CC_TIPO_FACTORAJE				");
				SQL.append("			,IC_USUARIO							");
				SQL.append("			,CG_NOMBRE_USUARIO				");						
				SQL.append("			,CC_ACUSE							");
				SQL.append("			,DF_FECHAHORA_OPER				");
				SQL.append("		)											");
				SQL.append("		VALUES (									");
				SQL.append("			?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, SYSDATE    ) ");	
				
				StringBuffer insertBitacora = new StringBuffer();								 			 
				insertBitacora.append(
								"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
								"\n IC_CAMBIO,  " +
								"\n CC_PANTALLA_BITACORA,  " +
								"\n CG_CLAVE_AFILIADO, " +
								"\n IC_NAFIN_ELECTRONICO, " +
								"\n DF_CAMBIO, " +
								"\n IC_USUARIO, " +
								"\n CG_NOMBRE_USUARIO, " +
								"\n CG_ANTERIOR, " +
								"\n CG_ACTUAL, " +
								"\n IC_GRUPO_EPO, " +
								"\n IC_EPO ) " +
								"\n VALUES(  " +
								"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
								"\n 'DESAUTOMA', " +  
								"\n 'P', " +
								"\n  ?  , " +
								"\n SYSDATE, " +
								"\n    ? ,  " +
								"\n   ? , " +  
								"\n '',  " +  
								"\n   ?,  " + 
								"\n '', " +
								"\n '' ) " );
					
					StringBuffer SQLU	= 	new StringBuffer();  
					SQLU.append(" UPDATE COMCAT_PYME 				");
					SQLU.append(" SET CS_DSCTO_AUTOMATICO = 'S' ");
					SQLU.append(" WHERE IC_PYME = ? 				");	
					
					List variables = new ArrayList();							
					PreparedStatement pst		= 	null;
								
					log.info(" contadorPyme ----->"+contadorPyme);		
					
				if (contadorPyme==0 )  {
					
					variables = new ArrayList();
					variables.add(ic_pyme);
					pst = con.queryPrecompilado(SQLU.toString(), variables);
					pst.executeUpdate();
					pst.close();
		
					for(int y=0; y<tipoFactoraje.size(); y++)  {
						String mensajeOrden ="";
						String valorFact =  (String)tipoFactoraje.get(y);
						if(valorFact.equals("N"))  {  orden = "1";  nombreFactoraje = "Normal";  mensajeOrden= "Orden= "+orden+"\n"; } 
						if(valorFact.equals("V"))  {  orden = "0";  nombreFactoraje = "Vencido"; }
						String actual =  mensajeOrden+
											  " Modalidad= Primer dia desde su publicacion "+"\n"+
											  " EPO="+nombreEpo+"\n"+
											  " PYME="+nombrePyme+"\n"+
											  " IF="+nombreIF	+"\n"+	
											  " Moneda= "+nombreMoneda+	"\n"+	  
										     " Pantalla= WEBSERVICE "+"\n"+
											  " Tipo Factoraje =" +nombreFactoraje+"\n"+
											  " Descuento Autom�tico = S " ;
									
						
						// insert en COMREL_DSCTO_AUT_PYME 
						variables = new ArrayList();
						variables.add(new Integer(ic_pyme));	//PYME
						variables.add( new Integer(claveEpo));	//EPO
						variables.add( new Integer(iClaveIF)); //IC_IF
						variables.add( new Integer(ic_moneda)); //MONEDA
						variables.add(orden);						//ORDEN
						variables.add("P");							//MODALIDAD p -> primer dia...
						variables.add("S");							//S - APLICA DSCTO AUTOMATICO
						variables.add(valorFact);				//TIPO FACTIRAJE
						variables.add("WEBSERVICE");					//IC_USUARIO
						variables.add("Web Service Confirmaci�n Cuentas Pymes");		//NOMBRE USUARIO
						variables.add("0");	    //ACUSE						
						pst = con.queryPrecompilado(SQL.toString(), variables);
						pst.executeUpdate();
						pst.close();
						
							//insert en BIT_CAMBIOS_GRAL 
						variables = new ArrayList();
						variables.add(nun_elecPyme);	//nun_elecPyme
						variables.add("WEBSERVICE");	//
						variables.add( "Web Service Confirmaci�n Cuentas Pymes"); 
						variables.add( actual); //MONEDA				
						pst = con.queryPrecompilado(insertBitacora.toString(), variables);
						pst.executeUpdate();
						pst.close();
					}//for	
				}
				
				// Fodea 010-2013 - Reafiliacion Automatica
				ParametrosDescuentoBean param  = new ParametrosDescuentoBean();
				param.paramCUDescAuto(claveIF,  nombreIF, ic_pyme, nombrePyme, String.valueOf(claveEpo) , nombreEpo,  ic_moneda,  "WEBSERVICE" ,  "Web Service Confirmaci�n Cuentas Pymes(confirmacionCuentasIFWS)", "WEBSERVICE", con);
						
				//Debido a que hay cargas parciales, aquellos registros que lleguen hasta aqui
				//deben ser insertados definitivamente. Por ello se realiza el commit por documento valido
				con.terminaTransaccion(true);
				
				
				}catch(Throwable t){
					//Error inesperado en el procesamiento del documento.
					//Debe continuar con el siguiente documento.
					cuent.setErrorDetalle("ERROR INESPERADO|" + detalleError.toString() + "\nDetalle: " + t.getMessage());
					cuent.setTipoError(2);
					lCuentasError.add(cuent);
					erroresInesperados.append(cuent.getErrorDetalle()+"\r\n");
					log.error("Hubo errores Inesperados "+erroresInesperados);

					log.error("confirmacionCuentasIFWS(Error) ", t);
					try {
						if (con.hayConexionAbierta()) {
							//en caso de error inesperado realiza un rollback.
							con.terminaTransaccion(false);
						}
					} catch(Throwable t1) {
						log.error("confirmacionCuentasIFWS(Error) ", t1);
					}
					continue;
				}
//-----------------------------------------------------------------------------------------------------------
						
									
					
				}//fin del try-catch del ciclo
		 //fin for
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//---------------------------------- Fin del ciclo -----------------------------------
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			
			if (lCuentasError.size() > 0) {
					CuentasPymeIFWS[] arrCuentasError1 = new CuentasPymeIFWS[lCuentasError.size()];
					lCuentasError.toArray(arrCuentasError1);
					resultadoProceso.setCuentas(arrCuentasError1);
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			
			

	} catch (Throwable t) { //Aqui llegan todos los errores inesperados que puedan ocurrir.
			exito = false;
			CuentasPymeIFWS[] arrCuentasError = null;
			log.error("confirmacionCuentasIFWS(Error) ", t);
			// Si los errores son antes de que empiece el ciclo de procesamiento de documentos
			// significa que no proceso ningun registro, por lo cual se debe regresar
			// todos los documentos recibidos, como documentos con error...
			if (i == 0) { //si i es 0 significa que no entro al ciclo para procesar docto
				for (i=0; i < cuentas.length; i++) {
					CuentasPymeIFWS cuent = cuentas[i];
					cuent.setErrorDetalle("ERROR INESPERADO|Registro no procesado");
					cuent.setTipoError(2);

					erroresInesperados.append(cuent.getErrorDetalle()+"\r\n");
					log.error("Hubo errores Inesperados "+erroresInesperados);

				}
				arrCuentasError = cuentas;
				//borra el acuse ya que no se proceso ningun registro
			} else {
				// Si el proceso es despues del ciclo de procesamiento de documentos,
				// si hubo errores, ya sea esperados o inesperados, estos estan especificados
				// en el detalle de Error del objeto de Cuenta
				if (lCuentasError.size() > 0) {
					arrCuentasError = new CuentasPymeIFWS[lCuentasError.size()];
					lCuentasError.toArray(arrCuentasError);
				}
			}
			resultadoProceso.setCuentas(arrCuentasError);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("Hubo errores Inesperados "+erroresInesperados);


			/* llamado del metodo para enviar correo de Errores Inesperados siemprey cuando hayan*/
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"C_CTAS");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}

		} finally {
			if (con.hayConexionAbierta()) {
				try {
				
					con.terminaTransaccion(exito);
 
					con.cierraConexionDB();
				} catch(Exception e1) {
					log.error("confirmacionCuentasIFWS(Error)::" + e1.getMessage());
				}
			}
		}

		log.info("confirmacionCuentasIFWS(S)");
		return resultadoProceso;
	}
	public ProcesoIFWSInfoV2 confirmacionCuentasIFWS (String claveUsuario,
			String password, 	String claveIF,
			CuentasPymeIFWS[] cuentas) {
		log.info("AutorizacionDescuentoBean::confirmacionCuentasIFWS(E)("+claveIF+"_"+claveUsuario+")");

		ProcesoIFWSInfoV2 resultadoProceso = new ProcesoIFWSInfoV2();
		resultadoProceso.setCodigoEjecucion(new Integer(1));
		List lCuentasError = new ArrayList();

		int i = 0; //numero de registros procesados
		//boolean falloActualizacionAcuse = false;
		AccesoDB con=null;
		boolean exito = true;
		int iClaveIF = 0;
		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa
		
		try{
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")  ||
					cuentas == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);

		} catch(Exception e) {
			log.error("confirmacionCuentasIFWS(Error) ", e);
			// Como no entr� al ciclo, se regresan todos los documentos recibidos (si existen)
			if (cuentas != null) {
				for (i=0; i < cuentas.length; i++) {
					CuentasPymeIFWS cuent = cuentas[i];
					cuent.setErrorDetalle("ERROR|Registro no procesado");
					cuent.setTipoError(1);
				}
			}
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF +
					"," + cuentas + ")" + ". Error=" + e.getMessage());
			resultadoProceso.setCuentas(cuentas);
			return resultadoProceso;
		}
		//****************************************************************************************


		if (cuentas == null) {
			//No hay documentos por procesar. Termina
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //Exito
			resultadoProceso.setResumenEjecucion("No hay Cuentas por procesar");
			return resultadoProceso;

		}

			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}

			//Validacion del usuario y Password y que sea del IF especificado
			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = null;
			try {
				cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			} catch(Throwable t) {
				throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
			}
			log.debug("confirmacionCuentasIFWS(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("confirmacionCuentasIFWS::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {
				//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
				for (i=0; i < cuentas.length; i++) {
					CuentasPymeIFWS cuent = cuentas[i];
					cuent.setErrorDetalle("ERROR|Registro no procesado");
					cuent.setTipoError(1);
				}
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");
				resultadoProceso.setCuentas(cuentas);
				return resultadoProceso; //Termina Proceso
			}

			log.debug("confirmacionCuentasIFWS::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					for (i=0; i < cuentas.length; i++) {
						CuentasPymeIFWS cuent = cuentas[i];
						cuent.setErrorDetalle("ERROR|Registro no procesado");
						cuent.setTipoError(1);
					}
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					resultadoProceso.setCuentas(cuentas);
					return resultadoProceso; //Termina Proceso
				}
			} catch(Throwable t) {
				throw new Exception("Error en el componente de validacion de usuario. ", t);
			}
		
		} catch (Throwable t) { //Aqui llegan todos los errores inesperados que puedan ocurrir.
			exito = false;
			CuentasPymeIFWS[] arrCuentasError = null;
			log.error("confirmacionCuentasIFWS(Error) ", t);
			// Si los errores son antes de que empiece el ciclo de procesamiento de documentos
			// significa que no proceso ningun registro, por lo cual se debe regresar
			// todos los documentos recibidos, como documentos con error...
			if (i == 0) { //si i es 0 significa que no entro al ciclo para procesar docto
				for (i=0; i < cuentas.length; i++) {
					CuentasPymeIFWS cuent = cuentas[i];
					cuent.setErrorDetalle("ERROR INESPERADO|Registro no procesado");
					cuent.setTipoError(2);
					erroresInesperados.append(cuent.getErrorDetalle()+"\r\n");
					log.error("Hubo errores Inesperados "+erroresInesperados);

				}
				arrCuentasError = cuentas;
				//borra el acuse ya que no se proceso ningun registro
			} else {
				// Si el proceso es despues del ciclo de procesamiento de documentos,
				// si hubo errores, ya sea esperados o inesperados, estos estan especificados
				// en el detalle de Error del objeto de Cuenta
				if (lCuentasError.size() > 0) {
					arrCuentasError = new CuentasPymeIFWS[lCuentasError.size()];
					lCuentasError.toArray(arrCuentasError);
				}
			}
			resultadoProceso.setCuentas(arrCuentasError);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("Hubo errores Inesperados "+erroresInesperados);


			/* llamado del metodo para enviar correo de Errores Inesperados siemprey cuando hayan*/
			try {
				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " + 
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados,"C_CTAS");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
			}
			return resultadoProceso;
		} finally {
			if (con.hayConexionAbierta()) {
				try {
				
					con.terminaTransaccion(exito);
 
					con.cierraConexionDB();
				} catch(Exception e1) {
					log.error("confirmacionCuentasIFWS(Error)::" + e1.getMessage());
				}
			}
			log.info("AutorizacionDescuentoBean::confirmacionCuentasIFWS(S)("+claveIF+"_"+claveUsuario+")");			
		}
		return confirmacionCuentasIFWS(claveIF,claveUsuario,cuentas);
		
	}

	
	 
	/**
	 * Realiza la traduccion de ciertas claves de epos que no existen en comcat_epo. Son epos que manejan los intermediarios.
	 * @param cveIf	Clave del IF
	 * @param cveEpo	Clave de la EPO
	 * @param tipoFactoraje	Tipo de Factoraje
	 * @return ?
	 * @throws netropology.utilerias.AppException
	 */
	private String getClaveEpoExternaWSIF(String cveIf, String cveEpo, 
			String tipoFactoraje) throws AppException {
		log.info("getClaveEpoExternaWSIF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strQry = "";
		String cgEpoExt = "";
		try{
			con.conexionDB();
			
			strQry = "SELECT ig_epo_externa " +
				"  FROM comrel_if_epo_wsif " +
				" WHERE ic_if = ? AND ic_epo = ? AND cc_tipo_factoraje = ? ";
				
			ps = con.queryPrecompilado(strQry);
			ps.setLong(1, Long.parseLong(cveIf));
			ps.setLong(2, Long.parseLong(cveEpo));
			ps.setString(3, tipoFactoraje);
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				cgEpoExt = rs.getString("ig_epo_externa");
			}
			
			rs.close();
			ps.close();
			
			return cgEpoExt;

		}catch(Throwable t){
			throw new AppException("Error al consultar Clave EPO externa para el Intermediario", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getClaveEpoExternaWSIF(S)-"+cgEpoExt);
		}
	}

	/**
	 * Realiza la traduccion de ciertas claves de epos que no existen en comcat_epo. Son epos que manejan los intermediarios.
	 * @param cveIf	Clave del IF
	 * @param cveEpoExterna	Clave de la EPO Externa
	 * @return Mapa con las siguientes llaves:
	 * 	IC_EPO.- Clave de la Epo como aaparece en comcat_epo
	 *    CC_TIPO_FACTORAJE.- Clave del tipo de factoraje
	 */
	private Map getClaveEpoExternaAInternaWSIF(String cveIf, String cveEpoExterna) {
		log.info("getClaveEpoExternaAInternaWSIF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strQry = "";
		Map mEpo = new HashMap();
		try{
			con.conexionDB();
			
			strQry = 
				" SELECT ic_epo, CASE WHEN ig_epo_externa = ? THEN cc_tipo_factoraje ELSE 'N' END AS cc_tipo_factoraje " +
				" FROM comrel_if_epo_wsif " +
				" WHERE ic_if = ? AND (ig_epo_externa = ? OR ic_epo = ?) ";
			
			ps = con.queryPrecompilado(strQry);
			ps.setLong(1, Long.parseLong(cveEpoExterna));
			ps.setLong(2, Long.parseLong(cveIf));
			ps.setLong(3, Long.parseLong(cveEpoExterna));
			ps.setLong(4, Long.parseLong(cveEpoExterna));
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				mEpo.put("IC_EPO",rs.getString("ic_epo"));
				mEpo.put("CC_TIPO_FACTORAJE", rs.getString("cc_tipo_factoraje"));
			}
			
			rs.close();
			ps.close();
			
			return mEpo;

		}catch(Throwable t){
			throw new AppException("Error al consultar Clave EPO y tipo de factoraje para el Intermediario", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getClaveEpoExternaAInternaWSIF(S)");
		}
	}


	
		public String  getRFCPyme(String icPyme) {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		String rfcPyme="";
		ResultSet rs = null;
		try {
			con.conexionDB();

			qrySentencia=
				" SELECT cg_rfc"   +
				"   FROM comcat_pyme"   +
				"  WHERE ic_pyme = "  + icPyme ;

			rs = con.queryDB(qrySentencia);
			if(rs.next())

			{
				rfcPyme=rs.getString(1);
			}
			con.cierraStatement();
		} catch (Exception e) {
			throw  new AppException("Ocurrio un error al consultar la BD");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
        return rfcPyme;
	}
	  
	public boolean  getOperaIFEPOFISO(String icIF,String icEPO) {

		AccesoDB con  = new AccesoDB();
		boolean bandera=false;
		String  operaIF="N",operaEPO="N";
		
		
		
		try {
			con.conexionDB();

			if(!icIF.equals("")){
				String qrySentenciaIF=	" select CS_OPERA_FIDEICOMISO from comcat_if where ic_if = "+icIF;  
				PreparedStatement psIf = con.queryPrecompilado(qrySentenciaIF);
				 ResultSet  rsIf	 = psIf.executeQuery();
				if(rsIf.next())		{
					operaIF = rsIf.getString(1);
				}
				rsIf.close();
				psIf.close();
			}
			
			String qrySentenciaEPO=	"select CG_VALOR as CS_OPERA_FIDEICOMISO from COM_PARAMETRIZACION_EPO  where ic_epo = "+icEPO+" and CC_PARAMETRO_EPO =  'CS_OPERA_FIDEICOMISO'" ;
			
			
			PreparedStatement psEPo = con.queryPrecompilado(qrySentenciaEPO);
			ResultSet  rsEPo	 = psEPo.executeQuery();
			if(rsEPo.next())	{
				operaEPO = rsEPo.getString(1);
			}
			rsEPo.close(); 
			psEPo.close(); 
			
			log.info("ic_epo:"+icEPO+" operaEPO "+operaEPO +"---operaIF--"+operaIF);    
			
			if(operaEPO.equals("S")   &&  operaIF.equals("S")  )  {
				bandera =true;
			}else  {
				bandera =false;
			}
						
		} catch (Exception e) {
			throw  new AppException("Ocurrio un error al consultar la BD");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
				
		return bandera;
        
	}
	
	public String  getMonedaPorCuenta(String cuenta) {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		String moneda="";
		ResultSet rs = null;
		try {
			con.conexionDB();

			qrySentencia=
				"SELECT ic_moneda " +
					"                         FROM comrel_cuenta_bancaria " +
					"                        WHERE ic_cuenta_bancaria = "+cuenta ;

			rs = con.queryDB(qrySentencia);
			if(rs.next())

			{
				moneda=rs.getString(1);
			}
			con.cierraStatement();
		} catch (Exception e) {
			throw  new AppException("Ocurrio un error al consultar la BD");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
        return moneda;
	}
	
	/**
	 * MEtodo para el Web Service que retorna los limites por epo
	 * @param clave usuario
	 * @param password
	 * @param clave IF
	 * @return un objeto ProcesoIFWSInfoV2
	 */
	public ProcesoIFWSInfoV2 getLimitesPorEpo (String claveUsuario,
			String password, 	String claveIF) {
		log.info("AutorizacionDescuentoBean::getLimitesPorEpo(E)("+claveIF+"_"+claveUsuario+")");
		StringBuffer Error = new StringBuffer("");
		LimitesEpo[] arrEpos = null;
		AccesoDB con =  new AccesoDB();
		ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;      
		boolean exito = true;
		int iClaveIF = 0;
		
		try{
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")  ) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);

		} catch(Exception e) {
			log.error("getLimitesPorEpo(Error) ", e);	
			Error.append("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF +
					")" + ". Error=" + e.getMessage());
			ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
			retorno.setResumenEjecucion(Error.toString());
			retorno.setCodigoEjecucion(new Integer(1));
			return retorno;
		}
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}

			//Validacion del usuario y Password y que sea del IF especificado
			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = null;
			try {
				cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			} catch(Throwable t) {
				throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
			}
			log.debug("getLimitesPorEpo(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("getLimitesPorEpo::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {

				Error.append("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");				
				ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
				retorno.setResumenEjecucion(Error.toString());
				retorno.setCodigoEjecucion(new Integer(1));
				return retorno;
			}

			log.debug("getLimitesPorEpo::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					
					Error.append("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
					retorno.setResumenEjecucion(Error.toString());
					retorno.setCodigoEjecucion(new Integer(1));
					return retorno;
				}
			} catch(Throwable t) {
				throw new Exception("Error en el componente de validacion de usuario. ", t);
			}
			
			try {
				con.conexionDB();
				
				String strQry = "SELECT E.IC_EPO, E.CG_RAZON_SOCIAL AS NOMBRE_EPO, IE.IC_IF, IN_LIMITE_EPO, FN_LIMITE_UTILIZADO, CS_ACTIVA_LIMITE, FN_MONTO_COMPROMETIDO, 'PESOS' AS MONEDA, TO_CHAR(DF_VENC_LINEA,'DD/MM/YYYY') as FECHA_VENC, TO_CHAR(DF_CAMBIO_ADMON,'DD/MM/YYYY') as FECHA_CAMBIO  ,  " +
									 "	IN_LIMITE_EPO_dl, FN_LIMITE_UTILIZADO_DL, FN_MONTO_COMPROMETIDO_DL, 'DOLARES' AS MONEDA_DL  "+
										"FROM COMREL_IF_EPO IE, COMCAT_EPO E  " +
										"WHERE IE.IC_EPO = E.IC_EPO " +
										"AND IC_IF = ? " +
										"AND IE.CS_VOBO_NAFIN = 'S' " +
										"AND IE.CS_ACEPTACION = 'S' " +
										"ORDER BY E.CG_RAZON_SOCIAL ";
				List lVarBind		= new ArrayList();
				lVarBind		= new ArrayList();         
				lVarBind.add(claveIF);
				ps = con.queryPrecompilado(strQry,lVarBind );	
				rs = ps.executeQuery();
				List listaRetorno = new ArrayList();
				LimitesEpo objet = null;
				while(rs.next() && rs!=null){           
					objet = new LimitesEpo();
					objet.setActivaLimite((rs.getString("CS_ACTIVA_LIMITE")==null?"":rs.getString("CS_ACTIVA_LIMITE")));
					objet.setClaveEPO((rs.getString("IC_EPO")==null?"":rs.getString("IC_EPO")));
					objet.setClaveIF((rs.getString("IC_IF")==null?"":rs.getString("IC_IF")));
					objet.setLimiteEPO((rs.getString("IN_LIMITE_EPO")==null?"":rs.getString("IN_LIMITE_EPO")));
					objet.setLimiteUtilizado((rs.getString("FN_LIMITE_UTILIZADO")==null?"":rs.getString("FN_LIMITE_UTILIZADO")));
					objet.setMoneda((rs.getString("MONEDA")==null?"":rs.getString("MONEDA")));
					objet.setMontoComprometido((rs.getString("FN_MONTO_COMPROMETIDO")==null?"":rs.getString("FN_MONTO_COMPROMETIDO")));
					
					objet.setLimiteEPO_dl((rs.getString("IN_LIMITE_EPO_DL")==null?"":rs.getString("IN_LIMITE_EPO_DL")));					
					objet.setLimiteUtilizado_dl((rs.getString("FN_LIMITE_UTILIZADO_DL")==null?"":rs.getString("FN_LIMITE_UTILIZADO_DL")));
					objet.setMoneda_dl((rs.getString("MONEDA_DL")==null?"":rs.getString("MONEDA_DL")));
					objet.setMontoComprometido_dl((rs.getString("FN_MONTO_COMPROMETIDO_DL")==null?"":rs.getString("FN_MONTO_COMPROMETIDO_DL")));						
					
					objet.setNombreEPO((rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO")));
					objet.setFechaCambioAdmin((rs.getString("FECHA_CAMBIO")==null?"":rs.getString("FECHA_CAMBIO")));
					objet.setFechaLineaCredito((rs.getString("FECHA_VENC")==null?"":rs.getString("FECHA_VENC")));
														
					listaRetorno.add(objet);
				}
				
				rs.close();
				ps.close(); 
				
				if (listaRetorno.size()==0) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					Error.append("No hay regristros" );
					ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
					retorno.setResumenEjecucion(Error.toString());
					retorno.setCodigoEjecucion(new Integer(2));
					return retorno;
				}
				arrEpos = new LimitesEpo[listaRetorno.size()];
					listaRetorno.toArray(arrEpos);
			} catch(Throwable t){
				throw new Exception("Error al consultar en la base de Datos. ", t);
			}
		
		} catch (Throwable t){ //Aqui llegan todos los errores inesperados que puedan ocurrir.
			exito = false;
			Error.append("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());
			log.error("Hubo errores Inesperados "+t.getMessage());

			ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
			retorno.setResumenEjecucion(Error.toString());
			retorno.setCodigoEjecucion(new Integer(1));
			return retorno;
		} finally {
			if (con.hayConexionAbierta()) {
				try {
				
					con.terminaTransaccion(exito);
 
					con.cierraConexionDB();
				} catch(Exception e1) {
					log.error("getLimitesPorEpo(Error)::" + e1.getMessage());
				}
			}
			log.info("AutorizacionDescuentoBean::getLimitesPorEpo(S)("+claveIF+"_"+claveUsuario+")");
		}
		ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
		retorno.setLimitesEpo(arrEpos);
		retorno.setCodigoEjecucion(new Integer(2));
		return retorno;
		
	}
	/**
	 * MEtodo para el Web Service que retorna los proveedores 
	 * @param clave usuario
	 * @param password
	 * @param clave IF
	 * @param Fecha inicial
	 * @param Fecha Final
	 * @return un objeto ProcesoIFWSInfoV2
	 */
	
	public  ProcesoIFWSInfoV2 getProveedoresCFDI (String claveUsuario,
			String password, 	String claveIF,String fechaOperaIni,String fechaOperaFin) {
		log.info("AutorizacionDescuentoBean::getProveedoresCFDI(E)("+claveIF+"_"+claveUsuario+")");
		StringBuffer Error = new StringBuffer("");
		ProveedoresCFDI[] arrProv = null;
		AccesoDB con =  new AccesoDB();
		ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;      
		boolean exito = true;
		int iClaveIF = 0;
		StringBuffer cadena =new StringBuffer("");
		List lVarBind		= new ArrayList();
		lVarBind		= new ArrayList();     
		try{
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			if ((fechaOperaIni!=null&&!fechaOperaIni.equals(""))&&
					(fechaOperaFin!=null&&!fechaOperaFin.equals(""))){
						
						cadena.append(
					"   AND s.DF_FECHA_SOLICITUD >= to_date(?,'dd/mm/yyyy')"+
					"   AND s.DF_FECHA_SOLICITUD < to_date(?,'dd/mm/yyyy') + 1 ");
					lVarBind.add(fechaOperaIni);
					lVarBind.add(fechaOperaFin);
				}else
				{
						cadena.append(
					"   AND s.DF_FECHA_SOLICITUD >= trunc(SYSDATE) "+
					"   AND s.DF_FECHA_SOLICITUD < trunc(SYSDATE) + 1 ");
				}
			iClaveIF = Integer.parseInt(claveIF);

		} catch(Exception e) {
			log.error("getProveedoresCFDI(Error) ", e);	
			Error.append("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF +
					")" + ". Error=" + e.getMessage());
			ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
			retorno.setResumenEjecucion(Error.toString());
			retorno.setCodigoEjecucion(new Integer(2));
			return retorno;
		}
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}

			//Validacion del usuario y Password y que sea del IF especificado
			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = null;
			try {
				cuentasIF = utilUsr.getUsuariosxAfiliadoxPerfil(claveIF, "I", "ADMIN IF");
			} catch(Throwable t) {
				throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
			}
			log.debug("getProveedoresCFDI(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario ("+cuentasIF.size()+")");
			log.trace("getProveedoresCFDI::cuentasIF:"+cuentasIF);

			if (!cuentasIF.contains(claveUsuario)) {
				log.info("getProveedoresCFDI(Error)");
				Error.append("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");				
				ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
				retorno.setResumenEjecucion(Error.toString());
				retorno.setCodigoEjecucion(new Integer(2));
				return retorno;
			}

			log.debug("getProveedoresCFDI::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					log.info("getProveedoresCFDI(Error)");
					Error.append("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
					retorno.setResumenEjecucion(Error.toString());
					retorno.setCodigoEjecucion(new Integer(2));
					return retorno;
				}
			} catch(Throwable t) {
				throw new Exception("Error en el componente de validacion de usuario. ", t);
			}
			
			try {
				con.conexionDB();
				
				StringBuffer strQry = new StringBuffer("SELECT p.cg_rfc AS rfc_receptor, p.cg_razon_social AS nombre_receptor,   " +
              "       cdom.cg_calle, cdom.cg_numero_ext, cdom.cg_numero_int, cdom.cg_colonia,   " +
              "       cm.cd_nombre AS localidad, cm.cd_nombre AS municipio_delegacion,   " +
              "       ce.cd_nombre AS estado, cp.cd_descripcion AS pais,   " +
              "       cdom.cn_cp AS codigo_postal, p.in_numero_sirac,   " +
              "       TO_CHAR (s.DF_FECHA_SOLICITUD, 'dd/mm/yyyy') AS fechaoperacion,   " +
              "		  mon.CD_NOMBRE as moneda,  " +
              "       sum(d.fn_monto_dscto) AS montodescuento,   " +
              "       sum(ds.in_importe_interes) AS importeinteres,   " +
              "       sum(ds.in_importe_recibir) AS importerecibir,  " +
              "       cep.ic_epo as clave_epo, cep.cg_razon_social	as nombre_epo,  " +
              "       count(d.ic_documento) as total_doctos,  " +
              "       decode(cc.cg_email,null, p.cg_email, cc.cg_email) as correo_contacto, " +
              "       DOCTOS_PROVEEDOR_CFDI(d.ic_if, cep.ic_epo, cdom.ic_domicilio_epo, cm.ic_pais,   " +
              "                            cm.ic_estado, cm.ic_municipio,  p.ic_pyme, mon.ic_moneda, " +
              "                            TO_CHAR (s.DF_FECHA_SOLICITUD, 'dd/mm/yyyy')) as numero_doctos,  " +
              "       d.ic_if, cdom.ic_domicilio_epo,  cm.ic_pais, cm.ic_estado, cm.ic_municipio, p.ic_pyme,mon.ic_moneda " +
              "     FROM comcat_pyme p,   " +
              "       com_domicilio cdom,   " +
              "       comcat_municipio cm,   " +
              "       comcat_estado ce,   " +
              "       comcat_pais cp,   " +
              "       com_documento d,   " +
              "       com_docto_seleccionado ds,   " +
              "       com_solicitud s,   " +
              "	   comcat_moneda mon,  " +
              "       comcat_epo cep,  " +
              "       com_contacto cc  " +
              " WHERE cdom.ic_pyme = p.ic_pyme   " +
              "   AND cm.ic_municipio = cdom.ic_municipio   " +
              "   AND ce.ic_estado = cdom.ic_estado   " +
              "   AND cm.ic_municipio = cdom.ic_municipio   " +
              "   AND cm.ic_estado = cdom.ic_estado   " +
              "   AND cp.ic_pais = cdom.ic_pais   " +
              "   AND d.IC_MONEDA = mon.IC_MONEDA   " +
              "   AND d.ic_documento = ds.ic_documento   " +
              "   AND d.cs_dscto_especial != 'C'   " +
              "   AND d.ic_documento = s.ic_documento   " +
              "   AND d.ic_pyme = p.ic_pyme   " +
              "   AND d.ic_epo = cep.ic_epo  " +
              "   and p.ic_pyme = cc.ic_pyme(+)  " +
              cadena.toString() +
              "   AND d.ic_if =  ? " +
              "   and CS_FISCAL = 'S'  " +
              "   and CS_OPERA_FISO = 'N'  " +
              "   and cc.cs_primer_contacto = 'S'   " +
              "   group by p.cg_rfc, p.cg_razon_social ,  " +
              "       cdom.cg_calle, cdom.cg_numero_ext, cdom.cg_numero_int, cdom.cg_colonia,  " +
              "       cm.cd_nombre, cm.cd_nombre ,  " +
              "       ce.cd_nombre , cp.cd_descripcion ,  " +
              "       cdom.cn_cp, p.in_numero_sirac,  " +
              "       TO_CHAR (s.DF_FECHA_SOLICITUD, 'dd/mm/yyyy'),  " +
              "       mon.CD_NOMBRE, cep.ic_epo, cep.cg_razon_social,  " +
              "       decode(cc.cg_email,null, p.cg_email, cc.cg_email),  " +
              "       d.ic_if, cdom.ic_domicilio_epo, cm.ic_estado, cm.ic_municipio, " +
              "       cm.ic_pais, p.ic_pyme, mon.ic_moneda ");
					
				lVarBind.add(claveIF);
				
				ps = con.queryPrecompilado(strQry.toString(),lVarBind );	
				rs = ps.executeQuery();
				List listaRetorno = new ArrayList();
				ProveedoresCFDI objet = null;
				while(rs.next() && rs!=null){           
					objet = new ProveedoresCFDI();
					objet.setCalle((rs.getString("CG_CALLE")==null?"":rs.getString("CG_CALLE")));
					objet.setCodigoPostal((rs.getString("CODIGO_POSTAL")==null?"":rs.getString("CODIGO_POSTAL")));
					objet.setColonia((rs.getString("CG_COLONIA")==null?"":rs.getString("CG_COLONIA")));
					objet.setDelegacion_O_Municipio((rs.getString("MUNICIPIO_DELEGACION")==null?"":rs.getString("MUNICIPIO_DELEGACION")));
					objet.setEstado((rs.getString("ESTADO")==null?"":rs.getString("ESTADO")));
					objet.setFechaOperacion((rs.getString("FECHAOPERACION")==null?"":rs.getString("FECHAOPERACION")));
					objet.setLocalidad((rs.getString("LOCALIDAD")==null?"":rs.getString("LOCALIDAD")));
					objet.setMontoTotalDescuento((rs.getString("MONTODESCUENTO")==null?"":rs.getString("MONTODESCUENTO")));
					objet.setMontoTotalInteres((rs.getString("IMPORTEINTERES")==null?"":rs.getString("IMPORTEINTERES")));
					objet.setMontoTotalRecibir((rs.getString("IMPORTERECIBIR")==null?"":rs.getString("IMPORTERECIBIR")));
					objet.setNumeroSirac((rs.getString("IN_NUMERO_SIRAC")==null?"":rs.getString("IN_NUMERO_SIRAC")));
					//objet.setNumExterior((rs.getString("CG_NUMERO_EXT")==null?"":rs.getString("CG_NUMERO_EXT")));
					//objet.setNumInterior((rs.getString("CG_NUMERO_INT")==null?"":rs.getString("CG_NUMERO_INT")));
					objet.setMoneda((rs.getString("MONEDA")==null?"":rs.getString("MONEDA")));
					objet.setPais((rs.getString("PAIS")==null?"":rs.getString("PAIS")));
					objet.setNombreReceptor((rs.getString("NOMBRE_RECEPTOR")==null?"":rs.getString("NOMBRE_RECEPTOR")));
					objet.setRfcReceptor((rs.getString("RFC_RECEPTOR")==null?"":rs.getString("RFC_RECEPTOR")));
          
          objet.setClaveEpo((rs.getString("CLAVE_EPO")==null?"":rs.getString("CLAVE_EPO")));
          objet.setNombreEpo ((rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO")));
          objet.setTotalDocumentos((rs.getString("TOTAL_DOCTOS")==null?"":rs.getString("TOTAL_DOCTOS")));
          objet.setNumeroDocumentos((rs.getString("NUMERO_DOCTOS")==null?"":rs.getString("NUMERO_DOCTOS")));
          objet.setCorreoElectronico((rs.getString("CORREO_CONTACTO")==null?"":rs.getString("CORREO_CONTACTO")));
          
          
					listaRetorno.add(objet);
				}
				
				rs.close();
				ps.close(); 
				
				if (listaRetorno.size()==0) {
					//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
					log.info("getProveedoresCFDI(Error)");
					Error.append("No hay regristros" );
					ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
					retorno.setResumenEjecucion(Error.toString());
					retorno.setCodigoEjecucion(new Integer(1));
					return retorno;
				}
				arrProv = new ProveedoresCFDI[listaRetorno.size()];
				log.info(listaRetorno.size()+"");
				listaRetorno.toArray(arrProv);
			} catch(Throwable t){
				log.error(t.getMessage());
				throw new Exception("Error al consultar en la base de Datos. ", t);
			}
		
		} catch (Throwable t){ //Aqui llegan todos los errores inesperados que puedan ocurrir.
			exito = false;
			Error.append("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());
			log.error("Hubo errores Inesperados "+t.getMessage());

			ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
			retorno.setResumenEjecucion(Error.toString());
			retorno.setCodigoEjecucion(new Integer(2));
			return retorno;
		} finally {
			if (con.hayConexionAbierta()) {
				try {
				
					con.terminaTransaccion(exito);
 
					con.cierraConexionDB();
				} catch(Exception e1) {
					log.error("getProveedoresCFDI(Error)::" + e1.getMessage());
				}
			}
			log.info("AutorizacionDescuentoBean::getProveedoresCFDI(S)("+claveIF+"_"+claveUsuario+")");
		}
		ProcesoIFWSInfoV2 retorno  = new ProcesoIFWSInfoV2();
		retorno.setProveedores(arrProv);
		retorno.setCodigoEjecucion(new Integer(1));
		log.info(retorno);
		return retorno;
		
	}
	
	
	/**
		 * 
		 * @throws com.netro.exception.NafinException
		 * @return 
		 * @param hidRecibo
		 * @param strDirectorioTemp
		 * @param ic_if
		 * @param ic_epo
		 * @param acuse
		 */
		private HashMap  generaArchivosIFDesAuto (HashMap parametros, Vector vSolicitudes,   String tipoArchivo  ) throws NafinException { 
		
			log.info("generaArchivosIFDesAuto(E)");
			HashMap nomArchivos = new HashMap (); 
			Hashtable alParamEPO = new Hashtable (); 
			CreaArchivo archivo = new CreaArchivo();
			CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses(); 
			StringBuffer contenidoArchivo = new StringBuffer();
			
			boolean bOperaFactorajeVencido = false, bOperaFactorajeDist = false, bOperaFactorajeMand = false, bOperaFactorajeVencInfonavit = false,  bTipoFactoraje = false;
		
			String [] campoAdicional;
			int numRegistros = 0, numRegistrosMN = 0,  numRegistrosDL = 0, numCamposAdicionales = 0;
			String nombreArchivo = "";
				
			try{
			
				
			   ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
				
				String acuse = parametros.get("acuse").toString();
				String iNoUsuario = parametros.get("iNoUsuario").toString();
				String strDirectorioTemp = parametros.get("strDirectorioTemp").toString();
				String ic_epo = parametros.get("ic_epo").toString();
				String ic_if = parametros.get("ic_if").toString();          
				String hidRecibo = parametros.get("hidRecibo").toString();           
				String monto_descuento_mn = parametros.get("monto_descuento_mn").toString();
				String monto_interes_mn = parametros.get("monto_interes_mn").toString();
				String monto_operar_mn = parametros.get("monto_operar_mn").toString();
				String monto_descuento_dl = parametros.get("monto_descuento_dl").toString();
				String monto_interes_dl = parametros.get("monto_interes_dl").toString();
				String monto_operar_dl = parametros.get("monto_operar_dl").toString();
				
				String strNombreUsuario = parametros.get("strNombreUsuario").toString();               
				String hidFechaCarga = parametros.get("hidFechaCarga").toString();
				String hidHoraCarga = parametros.get("hidHoraCarga").toString();     
				
				String strPais = parametros.get("strPais").toString();      
				String iNoNafinElectronico = parametros.get("iNoNafinElectronico").toString();                           
				String strNombre = parametros.get("strNombre").toString();           
				String strLogo = parametros.get("strLogo").toString();   
				String strDirectorioPublicacion = parametros.get("strDirectorioPublicacion").toString();  
				
					
				generaAch.setAcuse(acuse);
				generaAch.setTipoArchivo(tipoArchivo); 
				generaAch.setUsuario(iNoUsuario);
				generaAch.setVersion("PANTALLA");   
				generaAch.setStrDirectorioTemp(strDirectorioTemp);
				
				int existeArch=   generaAch.existerArcAcuse();
		
				if(existeArch==0 ) { //verifica si existe archivo
							
					
					
					log.info("acuse  "+acuse);   
					log.info("ic_epo  "+ic_epo); 
					log.info("ic_if  "+ic_if);   
					log.info("strDirectorioTemp  "+strDirectorioTemp);   
					log.info("hidRecibo  "+hidRecibo); 
					log.info("monto_descuento_mn  "+monto_descuento_mn); 
					log.info("monto_interes_mn  "+monto_interes_mn);  
					log.info("monto_operar_mn  "+monto_operar_mn); 
					log.info("monto_descuento_dl  "+monto_descuento_dl); 
					log.info("monto_interes_dl  "+monto_interes_dl);  
					log.info("monto_operar_dl  "+monto_operar_dl); 
					log.info("iNoUsuario  "+iNoUsuario);  
					log.info("strNombreUsuario  "+strNombreUsuario);  
					log.info("hidFechaCarga  "+hidFechaCarga);  
					log.info("hidHoraCarga  "+hidHoraCarga); 
						
					
					
				
					alParamEPO = BeanParamDscto.getParametrosEPO(ic_epo, 1);
					bOperaFactorajeVencido = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?true:false;
					bOperaFactorajeDist = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?true:false;
					bOperaFactorajeMand = ("S".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?true:false;
					bOperaFactorajeVencInfonavit = ("S".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?true:false;
					bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactorajeMand || bOperaFactorajeVencInfonavit)?true:false;
					//obtenemos el nombre de la EPO
					String nombreEpo  = this.descripcionEPO(ic_epo);
			
					numCamposAdicionales = this.getNumCamposAdicionales(ic_epo);
				
					campoAdicional = this.getCamposAdicionales(ic_epo);
					
					if(tipoArchivo.equals("PDF"))  {
					
						Vector  registros = this.getDoctosProcesados(acuse, ic_epo, ic_if);
						
						nombreArchivo = archivo.nombreArchivo()+".pdf";
			
						ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
						 pdfDoc.encabezadoConImagenes(pdfDoc,strPais ,  iNoNafinElectronico, "", strNombre, 
										strNombreUsuario,strLogo, strDirectorioPublicacion);  
						
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
						pdfDoc.addText("  ","formasB",ComunesPDF.CENTER);
						pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
					
						pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito","formasB",ComunesPDF.CENTER);
						pdfDoc.addText("Recibo: "+hidRecibo,"formasB",ComunesPDF.CENTER);
						pdfDoc.setTable(7,65);
						pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,3);
						pdfDoc.setCell("Dolares","celda01",ComunesPDF.CENTER,3);
						pdfDoc.setCell("Total Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Interes","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Interes","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(ic_epo+" "+nombreEpo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_descuento_mn,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_interes_mn,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_operar_mn,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_descuento_dl,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_interes_dl,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_operar_dl,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.addTable();
			
						pdfDoc.setTable(2,65);
						pdfDoc.setCell("Cifras de Control","celda01",ComunesPDF.CENTER,2);
						pdfDoc.setCell("No. de acuse","formas");
						pdfDoc.setCell(acuse,"formas");
						pdfDoc.setCell("Fecha de carga","formas");
						pdfDoc.setCell(hidFechaCarga,"formas");
						pdfDoc.setCell("Hora de carga","formas");
						pdfDoc.setCell(hidHoraCarga,"formas");
						pdfDoc.setCell("Usuario","formas");
						pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas");
						String mensaje = "";
				
						pdfDoc.setCell(mensaje,"celda02",ComunesPDF.JUSTIFIED,2);
						pdfDoc.addTable();
						int cols = 13;
						int colsB = 0;
						if(bTipoFactoraje)
							cols++;
						pdfDoc.setTable(cols);
						pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Cliente SIRAC","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Proveedor","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Documento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fechade Vencimiento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
						if(bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto a  Operar","celda01",ComunesPDF.CENTER);
						colsB = 5;
						pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Solicitud","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("No. Proveedor","celda01",ComunesPDF.CENTER);
						if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit){//FODEA 042 - 2009 ACF
							colsB+=7;
							pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Neto Recibir Pyme","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						colsB++;
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER,cols-colsB);
					
						for (int i = 0; i < registros.size(); i++) {
							Vector registro = (Vector) registros.get(i);
								
							String numSirac = registro.get(0).toString();
							String nombrePyme = registro.get(1).toString();
							String numDocumento = registro.get(2).toString();
							String fechaDocumento = registro.get(3).toString();
							String fechaVencimiento = registro.get(4).toString();
							String claveMoneda = registro.get(5).toString();
							String nombreMoneda = registro.get(6).toString();
							String monto = registro.get(7).toString();
							String montoDescuento = registro.get(8).toString();
							String porcentajeAnticipo = registro.get(9).toString();
							String recursoGarantia = registro.get(10).toString();
							String importeInteres = registro.get(11).toString();
							String importeRecibir = registro.get(12).toString();
							String tasaAceptada = registro.get(13).toString();
							String plazo = registro.get(14).toString();
							String numProveedorInterno = registro.get(15).toString();
							String descuentoEspecial = registro.get(16).toString();
							String folio = registro.get(17).toString();     
							String beneficiario = registro.get(29).toString();
							String bancoBeneficiario = registro.get(30).toString();
							String sucursalBeneficiario = registro.get(31).toString();
							String cuentaBeneficiario = registro.get(32).toString();
							String porcBeneficiario = registro.get(33).toString();
							String importeRecibirBenef = registro.get(34).toString();
							String netoRecibirPyme = registro.get(35).toString();
							String tipoFactoraje = registro.get(36).toString();
							String referenciaFide = registro.get(37).toString();
							if (claveMoneda.equals("1")) {
								numRegistrosMN++;
							} else if (claveMoneda.equals("54")) {
								numRegistrosDL++;
							}
							String clase = "formas";
							if(i%5==4)
								clase = "celda02";
							else
								clase = "formas";
							pdfDoc.setCell("A",clase,ComunesPDF.CENTER);
							pdfDoc.setCell(numSirac,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(nombrePyme,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(numDocumento,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(fechaDocumento,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(nombreMoneda,clase,ComunesPDF.CENTER);
							if(bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
								pdfDoc.setCell(tipoFactoraje,clase,ComunesPDF.CENTER);
							}
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),clase,ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %",clase,ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(recursoGarantia,2),clase,ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescuento,2),clase,ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeInteres,2),clase,ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeRecibir,2),clase,ComunesPDF.CENTER);
							pdfDoc.setCell("B",clase,ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(tasaAceptada,5)+" %",clase,ComunesPDF.CENTER);
							pdfDoc.setCell(plazo,clase,ComunesPDF.CENTER);
							pdfDoc.setCell(folio.substring(0,10)+"-"+folio.substring(10,11),clase,ComunesPDF.CENTER);
							pdfDoc.setCell(numProveedorInterno,clase,ComunesPDF.CENTER);
							if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
								pdfDoc.setCell(beneficiario,clase,ComunesPDF.CENTER);
								pdfDoc.setCell(bancoBeneficiario,clase,ComunesPDF.CENTER);
								pdfDoc.setCell(sucursalBeneficiario,clase,ComunesPDF.CENTER);
								pdfDoc.setCell(cuentaBeneficiario,clase,ComunesPDF.CENTER);
								pdfDoc.setCell(porcBeneficiario,clase,ComunesPDF.CENTER);
								pdfDoc.setCell(Comunes.formatoMoneda2(importeRecibirBenef,true),clase,ComunesPDF.CENTER);
								pdfDoc.setCell(Comunes.formatoMoneda2(netoRecibirPyme,true),clase,ComunesPDF.CENTER);
							}
							pdfDoc.setCell("",clase,ComunesPDF.CENTER);
							
							pdfDoc.setCell("",clase,ComunesPDF.CENTER,cols-colsB);
							} //fin del for
					
							if (numRegistrosMN!=0) {
								pdfDoc.setCell("Numero de documentos M.N.","formasB",ComunesPDF.CENTER,3);
								pdfDoc.setCell(new Integer(numRegistrosMN).toString(),"formasB",ComunesPDF.CENTER,2);
								pdfDoc.setCell("","formas",ComunesPDF.CENTER,cols-5);
							}
							if (numRegistrosDL!=0) {
								pdfDoc.setCell("Numero de documentos D�lares","formasB",ComunesPDF.CENTER,3);
								pdfDoc.setCell(new Integer(numRegistrosDL).toString(),"formasB",ComunesPDF.CENTER,2);
								pdfDoc.setCell("","formas",ComunesPDF.CENTER,cols-5);
							}
							pdfDoc.addTable();
							pdfDoc.endDocument();
						
						nomArchivos.put("nombreArchivo", nombreArchivo);
					
					}else  if(tipoArchivo.equals("CSV"))  {
						
						contenidoArchivo.append("EPO,Monto Descuento M.N.,Monto de Intereses M.N.,Monto a Operar M.N.,Monto Descuento USD,Monto de Intereses USD,Monto a Operar USD\n");
						contenidoArchivo.append(ic_epo.replace(',',' ')+
												","+Comunes.formatoDecimal(monto_descuento_mn,2,false)+
												","+Comunes.formatoDecimal(monto_interes_mn,2,false)+
												","+Comunes.formatoDecimal(monto_operar_mn,2,false)+
												","+Comunes.formatoDecimal(monto_descuento_dl,2,false)+
												","+Comunes.formatoDecimal(monto_interes_dl,2,false)+
												","+Comunes.formatoDecimal(monto_operar_dl,2,false)+"\n"+" "+"\n");
						contenidoArchivo.append("Numero de Acuse,Fecha de Carga,Hora de Carga,Usuario de Captura\n");
						contenidoArchivo.append(acuse+","+hidFechaCarga+","+hidHoraCarga+","+iNoUsuario+" "+strNombreUsuario+"\n"+" "+"\n");
		
						contenidoArchivo.append("No. Cliente SIRAC,Proveedor,Numero de documento,"+
										"Fecha de Emision,Fecha de Vencimiento,Moneda,Monto,Monto Descuento,"+
										"Monto Interes,Monto a Operar,Tasa,Plazo,Folio,No. Proveedor,"+
										"Porcentaje de Descuento,Recurso en Garantia");
						if(bTipoFactoraje) {
							contenidoArchivo.append(",Tipo Factoraje");
						}
						if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
							contenidoArchivo.append(",Beneficiario,Banco Beneficiario,Sucursal Beneficiaria,Cuenta Beneficiaria"+
													",% Beneficiario,Importe a Recibir del Beneficiario,Neto a Recibir PyME");
						}
					
						for (int i=0; i < vSolicitudes.size(); i++) {
							Vector registro = (Vector)vSolicitudes.get(i);
							String numSirac = registro.get(0).toString();
							String nombrePyme = registro.get(1).toString();
							String numDocumento = registro.get(2).toString();
							String fechaDocumento = registro.get(3).toString();
							String fechaVencimiento = registro.get(4).toString();
							String nombreMoneda = registro.get(6).toString();
							String monto = registro.get(7).toString();
							String montoDescuento = registro.get(8).toString();
							String importeInteres = registro.get(11).toString();
							String importeRecibir = registro.get(12).toString();
							String tasaAceptada = registro.get(13).toString();
							String plazo = registro.get(14).toString();
							String folio = registro.get(17).toString();
							String numProveedorInterno = registro.get(15).toString();
							String porcentajeAnticipo = registro.get(9).toString();
							String recursoGarantia = registro.get(10).toString();
							String descuentoEspecial = registro.get(16).toString();
		
							String cc_acuse = registro.get(18).toString();
							String claveOficina = registro.get(19).toString();
							nombreEpo = registro.get(21).toString();
							String claveIf = registro.get(22).toString();
							String nombreIf = registro.get(23).toString();
							String claveMoneda = registro.get(5).toString();
							String claveEstatusDocumento = registro.get(28).toString();
							String fechaAlta = registro.get(24).toString();
							String fechaSolicitud = registro.get(25).toString();
							String referencia = registro.get(27).toString();
							String beneficiario = registro.get(29).toString();
							String bancoBeneficiario = registro.get(30).toString();
							String sucursalBeneficiario = registro.get(31).toString();
							String cuentaBeneficiario = registro.get(32).toString();
							String porcBeneficiario = registro.get(33).toString();
							String importeRecibirBenef = registro.get(34).toString();
							String netoRecibirPyme = registro.get(35).toString();                
							String  tipoFactoraje = registro.get(36).toString();
							String referenciaFide = registro.get(37).toString();
						
							contenidoArchivo.append("\n"+numSirac+","+nombrePyme.replace(',',' ')+","+numDocumento+
													","+fechaDocumento+","+fechaVencimiento+","+nombreMoneda+
													","+Comunes.formatoDecimal(monto,2,false)+
													","+Comunes.formatoDecimal(montoDescuento,2,false)+
													","+Comunes.formatoDecimal(importeInteres,2,false)+
													","+Comunes.formatoDecimal(importeRecibir,2,false)+
													","+Comunes.formatoDecimal(tasaAceptada,5,false)+
													","+plazo+","+folio+","+numProveedorInterno+
													","+Comunes.formatoDecimal(porcentajeAnticipo,0,false)+
													","+Comunes.formatoDecimal(recursoGarantia,2,false));                               
							if(bTipoFactoraje) {
								contenidoArchivo.append(","+tipoFactoraje);              
							}
							if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
								contenidoArchivo.append(","+beneficiario.replace(',', ' ')+","+//FODEA 042 - 2009 ACF
															bancoBeneficiario.replace(',', ' ')+","+//FODEA 042 - 2009 ACF
															sucursalBeneficiario+","+
															cuentaBeneficiario+","+
															porcBeneficiario+","+
															importeRecibirBenef+","+
															netoRecibirPyme);
							}
						}
									
						if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp,".csv")){
							log.error("<--!Error al generar el archivo-->");
						}else{
							nombreArchivo = archivo.nombre;
						}
					
						nomArchivos.put("nombreArchivo", nombreArchivo);
						nomArchivos.put("contenidoArchivo", contenidoArchivo.toString());
							
					}
					
					 //se guarda el archivo 
						generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
						generaAch.guardarArchivo() ; //Guardar 
				}else  {
					nombreArchivo = generaAch.desArchAcuse() ; //Descargar
					nomArchivos.put("nombreArchivo", nombreArchivo);
				}
					
			} catch (Exception exception) {
			
				java.io.StringWriter outSW = new java.io.StringWriter();
				exception.printStackTrace(new java.io.PrintWriter(outSW));
				String stackTrace = outSW.toString();        
				log.info("generaArchivosIFDesAuto  ERROR  " +tipoArchivo+"     "+stackTrace );     
				log.info("generaArchivosIFDesAuto ERROR  " +tipoArchivo+"   "+exception);
				generaAch.setDesError(stackTrace);           
				generaAch.guardaBitacoraArch();     					
				
			} finally {
				log.info("generaArchivosIFDesAuto (S)"); 
			}
			
			return nomArchivos;

		}

		public String  generaArchivosIFDesAutoCSV (HashMap parametros )  {  
			String  nombreArchivoCSV =""; 
			AccesoDB con         = new AccesoDB();
			boolean exito = true;
			log.info("  generaArchivosIFDesAutoCSV (E) "); 
			try {

				con.conexionDB();
				
				String acuse = parametros.get("acuse").toString();
				String ic_epo = parametros.get("ic_epo").toString();
				String ic_if = parametros.get("ic_if").toString();
				
				
				Vector  vSolicitudes = getDoctosProcesadosC(acuse, ic_epo, ic_if, con);
		
				HashMap  datosArchCsv =  this.generaArchivosIFDesAuto (parametros, vSolicitudes, "CSV");
				if(datosArchCsv.size()>0){
					nombreArchivoCSV = datosArchCsv.get("nombreArchivo").toString();           
				} 
		 
			
			} catch (Throwable e) {
				exito = false;
				e.printStackTrace();
				log.error("generaArchivosIFDesAutoCSV  " + e);
				throw new AppException("SIST0001");
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
					log.info("  generaArchivosIFDesAutoCSV (S) ");
				}
			}
			
			return nombreArchivoCSV;
		}

	/**
	 *  metodo para validar los horarios 
	 * @param cveEpo
	 * @param cveIF
	 * @return
	 * @throws AppException
	 */
	private boolean validaHoraEpoIFs(String cveEpo, String cveIF) throws AppException {
		log.info("validaHoraEpoIFs (e)");		
		boolean horaValida = true;
		try {

			Horario.validarHorarioIF(1, "IF", cveEpo, cveIF);

		} catch (Exception ne) {
			log.error("validaHoraEpoIFs (e)" + ne);
			horaValida = false;
		} 
		
		log.info("validaHoraEpoIFs (s)"  +horaValida );
		return horaValida;
		
	}
	
	
	/**
	 * metodo para obtener la parametrizaci�n del IF por nombre del parametro
	 * @param cveIF
	 * @param nomParametro
	 * @return
	 * @throws AppException
	 */
	private String  getParametrosIf( int cveIF, String nomParametro) throws AppException {
		log.info("getParametrosIf (e)");
		AccesoDB con = new AccesoDB();
		String  lsvalor= "N";
		StringBuilder query = new StringBuilder();
		List lVarBind = new ArrayList();

		try {
			con.conexionDB();

			query = new StringBuilder();
			query.append(  " SELECT pz.cg_valor AS  VALOR " + 
								" FROM com_parametrizacion_if pz, comcat_parametro_if pe " + 
								" WHERE pz.cc_parametro_if = pe.cc_parametro_if " + 
								" AND pz.ic_if = ?" + 
								" AND pe.cc_parametro_if = ? ");
			lVarBind = new ArrayList();
			lVarBind.add(cveIF);
			lVarBind.add(nomParametro);	
			
			log.debug ("query "+ query.toString() +" \n lVarBind  "+ lVarBind );
			
			PreparedStatement ps = con.queryPrecompilado(query.toString(), lVarBind);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				lsvalor =  (rs.getString("VALOR") == null)?"N":rs.getString("VALOR");			
			}
			rs.close();
			ps.close();

			return lsvalor;

		} catch (Exception e) {
			throw new AppException("error obtener el valor  getParametrosIF", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getParametrosIf (S)");
		}
	}	
	

}	//fin de clase


