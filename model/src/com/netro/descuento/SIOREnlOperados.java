package com.netro.descuento;

import java.io.Serializable;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;

public class SIOREnlOperados extends EpoPEF implements EpoEnlOperados, Serializable  {
    
    private String ic_epo = "";
    
    public SIOREnlOperados(String ic_epo){
        this.ic_epo = ic_epo;
    }
    
    public String getTablaDoctosOpe(){
        return "com_doctos_ope_sior";
    }
    
    public String getBorraDoctosOpe(){
        return "delete "+getTablaDoctosOpe();
    
    }
    public String getBorraDoctosVenc(){
        return "delete "+getTablaDoctosOpe() + " where ic_estatus_docto = 9";
    }
    public String getBorraDoctosBaja(){
        return "";
    }
    
    public String getInsertaDoctosOpe() {
        return 
            "INSERT INTO "+getTablaDoctosOpe()+" " +
            "            (cg_razon_social_if, df_fecha_aut, cc_acuse, in_numero_proveedor, " +
            "             cg_razon_social_pyme, ig_numero_docto, df_fecha_docto, " +
            "             df_fecha_venc, ic_moneda, fn_monto, cg_sucursal, cg_banco, " +
            "             cg_cuenta_bco, fn_porc_dscto, fn_monto_dscto, in_tasa_aceptada, " +
            "             ct_referencia " + getCamposPEFOper()+
            "             , cg_rfc_if, ic_estatus_docto " +//FODEA 026 - 2009 ACF
            "            ) ";
    }
    
    public String getDoctosOperados(Hashtable alParamEPO){
        String sPubEPOPEFFlagVal = alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
        String sDigitoIdentFlagVal = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		return	
			"SELECT " +
			"  SUBSTR (i.cg_razon_social, 1, 50) cg_razon_social_if, " +
			"  a.df_fecha_hora, " +
			"  a.cc_acuse, " +
			"  pe.cg_pyme_epo_interno, " +
			"  SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
			"  d.ig_numero_docto, " +
			"  d.df_fecha_docto, " +
			"  d.df_fecha_venc, " +
			"  d.ic_moneda, " +
			"  d.fn_monto, " +
			"  cb.cg_sucursal, " +
			"  cb.cg_banco, " +
			"  cb.cg_numero_cuenta, " +
			"  d.fn_porc_anticipo, " +
			"  d.fn_monto_dscto, " +
			"  ds.in_tasa_aceptada, " +
			"  d.ct_referencia " +
			(("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
			(("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
			"  ,i.cg_rfc cg_rfc_if " +
			"  ,4 ic_estatus_docto " +
			"FROM com_documento d, " +
			"  com_docto_seleccionado ds, " +
			"  com_solicitud s, " +
			"  com_acuse2 a, " +
			"  comcat_if i, " +
			"  comrel_pyme_epo pe, " +
			"  comrel_pyme_if pi, " +
			"  comrel_cuenta_bancaria cb, " +
			"  comcat_pyme py, " +
			"  comrel_if_epo ie " +
			"WHERE a.cc_acuse          = ds.cc_acuse " +
			"AND s.ic_documento        = ds.ic_documento " +
			"AND ds.ic_documento       = d.ic_documento " +
			"AND d.ic_if               = i.ic_if " +
			"AND d.ic_pyme             = pe.ic_pyme " +
			"AND d.ic_epo              = pe.ic_epo " +
			"AND d.ic_epo              = pi.ic_epo " +
			"AND ds.ic_if              = pi.ic_if " +
			"AND d.ic_moneda           = cb.ic_moneda " +
			"AND d.ic_pyme             = cb.ic_pyme " +
			"AND d.ic_pyme             = py.ic_pyme " +
			"AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
			"AND d.ic_epo              = ie.ic_epo " +
			"AND d.ic_if               = ie.ic_if " +
			"AND pi.cs_vobo_if         = 'S' " +
			"AND pi.cs_borrado         = 'N' " +
			"AND cb.cs_borrado         = 'N' " +
			"AND d.ic_estatus_docto   IN ( 4, 16 ) " +
			"AND d.ic_epo              = "+ic_epo+" " +
			"AND s.df_fecha_solicitud >= TRUNC (SYSDATE) " +
			"AND s.df_fecha_solicitud  < TRUNC (SYSDATE) + 1";
		
/*
"SELECT SUBSTR (i.cg_razon_social, 1, 50) cg_razon_social_if, a.df_fecha_hora, " +
"       a.cc_acuse, pe.cg_pyme_epo_interno, " +
"       SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
"       d.ig_numero_docto, d.df_fecha_docto, d.df_fecha_venc, d.ic_moneda, " +
"       d.fn_monto, cb.cg_sucursal, cb.cg_banco, cb.cg_numero_cuenta, " +
"       d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_tasa_aceptada, " +
"       d.ct_referencia " +
(("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
(("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
", i.cg_rfc cg_rfc_if, d.ic_estatus_docto "+//FODEA 026 - 2009 ACF
"  FROM com_documento d, " +
"       com_docto_seleccionado ds, " +
"       com_solicitud s, " +
"       com_acuse2 a, " +
"       comcat_if i, " +
"       comrel_pyme_epo pe, " +
"       comrel_pyme_if pi, " +
"       comrel_cuenta_bancaria cb, " +
"       comcat_pyme py, " +
"       comrel_if_epo ie " +
" WHERE a.cc_acuse = ds.cc_acuse " +
"   AND s.ic_documento = ds.ic_documento " +
"   AND ds.ic_documento = d.ic_documento " +
"   AND d.ic_if = i.ic_if " +
"   AND d.ic_pyme = pe.ic_pyme " +
"   AND d.ic_epo = pe.ic_epo " +
"   AND d.ic_epo = pi.ic_epo " +
"   AND ds.ic_if = pi.ic_if " +
"   AND d.ic_moneda = cb.ic_moneda " +
"   AND d.ic_pyme = cb.ic_pyme " +
"   AND d.ic_pyme = py.ic_pyme " +
"   AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
"   AND d.ic_epo = ie.ic_epo " +
"   AND d.ic_if = ie.ic_if " +
"   AND pi.cs_vobo_if = 'S' " +
"   AND pi.cs_borrado = 'N' " +
"   AND cb.cs_borrado = 'N' " +
"   AND d.ic_estatus_docto IN (4, 16) " +
"   AND s.df_fecha_solicitud >= TRUNC (SYSDATE) " +
"   AND s.df_fecha_solicitud < TRUNC (SYSDATE) + 1 " +
"   AND d.ic_epo = " + ic_epo;
*/
					
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
		return "";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
            //return "";
            //FODEA 026 - 2009 ACF (I)
            String sPubEPOPEFFlagVal = alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
            String sDigitoIdentFlagVal = alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
			/*
            String fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaAct	= fechaHoy.substring(0, 2);
            String mesAct	= fechaHoy.substring(3, 5);
            String anyoAct	= fechaHoy.substring(6, 10);
            Calendar cal = new GregorianCalendar();
            cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));
            String condicion = "";
    
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                condicion = " AND d.df_fecha_venc >= TRUNC(SYSDATE + " + diasMinimos + " - 3)";
            } else {
                condicion = " AND d.df_fecha_venc >= TRUNC(SYSDATE + " + diasMinimos + " - 1)";
            }
			*/
    
            return
				"SELECT " +
				"  NULL cg_razon_social_if, " +
				"  NULL df_fecha_hora, " +
				"  NULL cc_acuse, " +
				"  pe.cg_pyme_epo_interno cg_pyme_epo_interno, " +
				"  SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme , " +
				"  d.ig_numero_docto ig_numero_docto, " +
				"  d.df_fecha_docto df_fecha_docto, " +
				"  d.df_fecha_venc df_fecha_venc, " +
				"  d.ic_moneda ic_moneda, " +
				"  d.fn_monto fn_monto, " +
				"  NULL cg_sucursal, " +
				"  NULL cg_banco, " +
				"  NULL cg_numero_cuenta, " +
				"  NULL fn_porc_anticipo, " +
				"  NULL fn_monto_dscto, " +
				"  NULL in_tasa_aceptada, " +
				"  d.ct_referencia cc_acuse " +
                (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
                (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"  ,NULL                 AS cg_rfc_if " +
				"  ,9                    AS ic_estatus_docto " +
				"FROM com_documento d, " +
				"  comrel_pyme_epo pe, " +
				"  comcat_pyme py, " +
				"  comrel_producto_epo cpe, " +
				"  comcat_producto_nafin cpn " +
				"WHERE d.ic_pyme           = pe.ic_pyme " +
				"AND d.ic_epo              = pe.ic_epo " +
				"AND d.ic_pyme             = py.ic_pyme " +
				"AND cpe.ic_producto_nafin = cpn.ic_producto_nafin " +
				"AND cpe.ic_producto_nafin = 1 " +
				"AND cpe.ic_epo            = d.ic_epo " +
				"AND d.df_fecha_venc      >= TRUNC(sigfechahabilxepo_sydb ("+ic_epo+", SYSDATE, 1, '-') + NVL(cpe.ig_dias_minimo, cpn.in_dias_minimo)) " +
				"AND d.df_fecha_venc       < TRUNC(SYSDATE                                       + NVL(cpe.ig_dias_minimo, cpn.in_dias_minimo)) " +
				"AND d.ic_estatus_docto                                                         IN (9, 10) " +
				"AND d.ic_epo              = "+ic_epo+" " +
				"UNION " +
				"SELECT " +
				"  NULL cg_razon_social_if, " +
				"  NULL df_fecha_hora, " +
				"  NULL cc_acuse, " +
				"  pe.cg_pyme_epo_interno cg_pyme_epo_interno, " +
				"  SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme , " +
				"  d.ig_numero_docto ig_numero_docto, " +
				"  d.df_fecha_docto df_fecha_docto, " +
				"  d.df_fecha_venc df_fecha_venc, " +
				"  d.ic_moneda ic_moneda, " +
				"  d.fn_monto fn_monto, " +
				"  NULL cg_sucursal, " +
				"  NULL cg_banco, " +
				"  NULL cg_numero_cuenta, " +
				"  NULL fn_porc_anticipo, " +
				"  NULL fn_monto_dscto, " +
				"  NULL in_tasa_aceptada, " +
				"  d.ct_referencia cc_acuse " +
                (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
                (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				"  ,NULL cg_rfc_if " +
				"  ,9 ic_estatus_docto " +
				"FROM com_documento d, " +
				"  comrel_pyme_epo pe, " +
				"  comcat_pyme py, " +
				"  comrel_producto_epo cpe, " +
				"  comcat_producto_nafin cpn " +
				"WHERE d.ic_pyme           = pe.ic_pyme " +
				"AND d.ic_epo              = pe.ic_epo " +
				"AND d.ic_pyme             = py.ic_pyme " +
				"AND d.ic_epo              = cpe.ic_epo " +
				"AND cpe.ic_producto_nafin = cpn.ic_producto_nafin " +
				"AND cpn.ic_producto_nafin = 1 " +
				"AND d.df_alta            >= TRUNC(sigfechahabilxepo_sydb ("+ic_epo+", SYSDATE, 1, '-')) " +
				"AND d.ic_estatus_docto   IN (9,10) " +
				"AND d.ic_epo              = "+ic_epo;

/*
                "SELECT NULL AS cg_razon_social_if," +
                " NULL AS df_fecha_hora," +
                " NULL AS cc_acuse," +
                " pe.cg_pyme_epo_interno AS cg_pyme_epo_interno," +
                " SUBSTR (py.cg_razon_social, 1, 70) AS cg_razon_social_pyme," +
                " d.ig_numero_docto AS ig_numero_docto," +
                " d.df_fecha_docto AS df_fecha_docto," +
                " d.df_fecha_venc AS df_fecha_venc," +
                " d.ic_moneda AS ic_moneda," +
                " d.fn_monto AS fn_monto," +
                " NULL AS cg_sucursal," +
                " NULL AS cg_banco," +
                " NULL AS cg_numero_cuenta," +
                " NULL AS fn_porc_anticipo," +
                " NULL AS fn_monto_dscto," +
                " NULL AS in_tasa_aceptada," +
                " d.ct_referencia AS cc_acuse"+
                (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
                (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
                ", NULL AS cg_rfc_if,"+
                " d.ic_estatus_docto AS ic_estatus_docto "+
                "  FROM com_documento d, " +
                "       comrel_pyme_epo pe, " +
                "       comcat_pyme py" +
                " WHERE d.ic_pyme = pe.ic_pyme " +
                "  AND d.ic_epo = pe.ic_epo " +
                "  AND d.ic_pyme = py.ic_pyme " +
                "  AND d.ic_estatus_docto = 9 " +
                condicion +
                "  AND d.df_fecha_venc < TRUNC(SYSDATE + " + diasMinimos + ")" +
                "  AND d.ic_epo = " + ic_epo+
                //FODEA 026 - 2009 ACF (F)
                " UNION " +
                //FODEA 006-Feb2010 Rebos - Se agrego la notificación de documentos publicados como vencidos
                "SELECT NULL AS cg_razon_social_if," +
                " NULL AS df_fecha_hora," +
                " NULL AS cc_acuse," +
                " pe.cg_pyme_epo_interno AS cg_pyme_epo_interno," +
                " SUBSTR (py.cg_razon_social, 1, 70) AS cg_razon_social_pyme," +
                " d.ig_numero_docto AS ig_numero_docto," +
                " d.df_fecha_docto AS df_fecha_docto," +
                " d.df_fecha_venc AS df_fecha_venc," +
                " d.ic_moneda AS ic_moneda," +
                " d.fn_monto AS fn_monto," +
                " NULL AS cg_sucursal," +
                " NULL AS cg_banco," +
                " NULL AS cg_numero_cuenta," +
                " NULL AS fn_porc_anticipo," +
                " NULL AS fn_monto_dscto," +
                " NULL AS in_tasa_aceptada," +
                " d.ct_referencia AS cc_acuse"+
                (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
                (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
                ", NULL AS cg_rfc_if,"+
                " d.ic_estatus_docto AS ic_estatus_docto "+
                "  FROM com_documento d, " +
                "       comrel_pyme_epo pe, " +
                "       comcat_pyme py" +
                " WHERE d.ic_pyme = pe.ic_pyme " +
                "  AND d.ic_epo = pe.ic_epo " +
                "  AND d.ic_pyme = py.ic_pyme " +
                "  AND d.ic_estatus_docto = 9 " +
                "  AND d.df_alta > TRUNC(SYSDATE-1)"+
                "  AND d.ic_epo = "+ic_epo;
*/

	}
        
}//SIOREnlOperados
