/*************************************************************************************
*
* Nombre de Clase o Archivo: ParametrosDescuento.java
*
* Versi�n: 1.0
*
* Fecha Creaci�n: 15/Noviembre/2004
*
* Autor: Hugo D�az Garc�a
*
* Descripci�n de Clase: Interface remota del EJB de Parametros de Descuento Electronico.
*
*************************************************************************************/

package com.netro.descuento;

import java.util.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface ParametrosDescuento {
	/*M�todo usado en la pantalla de NAFIN - Descuento Electr�nico - Capturas -Puntos Rebate*/
	public boolean actualizaPuntosRebate(String claveEPO, String numPuntosRebate[]);
	/* M�todos usados en la pantalla de NAFIN - Capturas - Parametro EPO Mediana */
	public ArrayList getEpoMediana()throws NafinException;
	public ArrayList getIntermediarioPorEpo(String sNoEpo)throws NafinException;
	public ArrayList getEpoPymeIfDeEpoMediana(String sNoEpo, String sNoPyme)throws NafinException;
	public void setParametrosPignoracion(String[] sAutoPigEPI, String sDiasVenc[],
										String sNoEpoMed)throws NafinException;

	/* M�todos usados en la pantalla de NAFIN - Descuento Electronico - Capturas y Consultas - Parametros por EPO */
	public void setOperaNotasCredito(String sNoEPO, String sOperaNotasCredito, int iNoProducto)throws NafinException;
	public String getOperaNotasCredito(String sNoEPO, int iNoProducto)throws NafinException;
    /**********************/
    /* MPCS FODA 021-2005 */
	/**********************/
	public ArrayList getParamEPO(String sNoEPO, int iNoProducto)throws NafinException;
	public void setParamEPO(String sNoEPO, ArrayList alParametros, int iNoProducto, String claveUsuario)throws NafinException;
    /**********************/
    /* EGB FODEA 029-2009 */
	/**********************/
	public Hashtable getParametrosEPO(String sNoEPO, int iNoProducto)throws NafinException;
	public void setParametrosEPO(String sNoEPO, Hashtable alParametros, int iNoProducto, String claveUsuario)throws NafinException;

	public abstract List getIntermediarios(String ic_epo)throws NafinException;
	public abstract void setOrdenAplicacionIF(String ic_epo, String aplicacion[])throws NafinException;

	public List getDatosClausulado(String claveEpo, String consecutivo)
			throws NafinException;
	public ContratoEpo getDatosUltimoClausulado(String claveEpo)
			throws NafinException;

	public Map guardaParametrosCobMan(String productos[],String minCierre)
		throws NafinException;

	public String guardarClausulado(ContratoEpo contrato, String rutaArchivo)
			throws NafinException;
	public void actualizarClausulado(ContratoEpo contrato, String rutaArchivo)
			throws NafinException;


	public Map getParametrosCobMan()
		throws NafinException;

	public abstract List getPymesSinDsctoAuto(String ic_epo, String ic_moneda)
		throws NafinException;

	public abstract int getNumeroLineas(String ic_epo, String ic_pyme, String ic_moneda)
		throws NafinException;

	public List getParamSubsidioEpo(String ic_epo, String ic_epo_dest, String ic_pyme)
		throws NafinException;

	public void setParamSubsidioEpo(String ic_epo, String ic_epo_dest, String ic_pyme, String cs_activo, String fg_puntos, String fg_limite, String df_inicio_apoyo)
		throws NafinException;

	/******************/
    /* FODA 075-2007 */
	/*****************/
	public ArrayList getParamPorEPO(String sNoEPO, String icBancoFondeo)throws NafinException;//FODEA 008 - 2009 ACF

  public String setParamEpoPymeIf() throws NafinException;

  public void actualizarClausuladoIF(ContratoIF contrato, String rutaArchivo) throws NafinException;

  public String guardarClausuladoIF(ContratoIF contrato, String rutaArchivo) throws NafinException;

  public List getDatosClausuladoIF(String claveIF, String consecutivo) throws NafinException;

  public ContratoIF getDatosUltimoClausuladoIF(String claveIF) throws NafinException;

  // F045 - 2008

  public boolean  estaHabilitadoNumeroSIAFF(String icEpo) throws NafinException;

  // F014 - 2008

  public boolean  esConvenioCuatro(String icEpo, String icPyme) throws NafinException;
  public boolean  esConvenioTres(String icEpo, String icPyme) throws NafinException;
  public boolean  hayOrdenDeOperacion(String icEpo, String icPyme) throws NafinException;
  public String   getNombrePyme(String icPyme)throws NafinException;
  public List     getListaDeIntermediariosFinancieros(String icEpo,String icPyme)throws NafinException;
  public String   getNombreIF(String claveIF)throws NafinException;
  public String 	validaContrasenna(String cgLogin, String cgPassword,  String epo, String iNoCliente , String sesIdiomaUsuario, String tipoUsuario,  String remoteAddress, String remoteHost, String sesCveSistema)	throws NafinException;
  public void 		actualizaOrdenDeIntermediariosFinancieros(String icEpo,String icPyme, List listaIntermediarios) throws NafinException;
  public List 		getPymeDescuento(String nae,String rfc, String nombre, String sirac) throws NafinException;
  public HashMap 	getDomicilioPyme(String ic_pyme) throws NafinException;
  public List 		getEPOxBancoFondeo(String ic_banco_fondeo) throws NafinException;
  public HashMap  getConsultaDescuentoIf(String ic_epo, String nae)  throws NafinException;
  public String 	actualizaBitacoraOrdenIF(String ic_epo, String iNoUsuario, String ic_pyme, List listaIntermediariosAnteriores, List listaIntermediariosActuales) throws NafinException;
  public HashMap  autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente) throws NafinException;
  public List 		getEpoParamDescAut(String ic_pyme) throws NafinException;
  public List 		getBancosOrden(String ic_epo, String ic_pyme) throws NafinException;
  public boolean 	haSidoRechazadoElDescuentoAutomatico(String icPyme) throws NafinException;
  public void 		rechazarDescuentoAutomatico(String icPyme)throws NafinException;
	//	FODEA 002 - 2009 (I)
	public int getNumeroCamposAdicionales(String icEpo) throws NafinException;
	public List getNombresCamposAdicionales(String icEpo) throws NafinException;
	public List getDatosCamposAdicionales(String icDocumento, String icEpo, int numeroCampos) throws NafinException;
	//	FODEA 002 - 2009 (F)

  public HashMap getParametrosPYME(String numeroNafinElectronicoDeLaPYME, String claveEPO)  throws NafinException;
  public List getBancosOrden(String ic_pyme)	throws NafinException;

  //AGREGADO POR FODEA 036-2009
  public String setParamEpoPymeIf(List a, String tipo_val) throws NafinException;

	public void setEstatusDelExpedienteEfileEnTablaPyme(String rfc, boolean estatus)
		throws AppException;
	public boolean estaDisponibleSistemaEfile();
	public void actualizaPymesConExpedientesCargadosEnEfile(String query)
		throws AppException;
	public  String getNombrePyme(String noNafinElectronico,String claveEpo)
		throws AppException;

    public void getPymesSinExpEfile(String ic_if) throws AppException;//I.A. FODEA 057 - 2009
	//FODEA 016-2010 FVR
	public Hashtable getParametrosIF(String sNoIF, int iNoProducto) throws NafinException;
	public void setParametrosIF(String sNoIF, Hashtable alParametros, int iNoProducto, String claveUsuario) throws NafinException;

	// Fodea 002 - 2010 -- JSHD
	public boolean operaNotasDeCreditoParaDescuentoElectronico(String ic_epo)
		throws AppException;
	public boolean hasAplicarNotaDeCreditoAVariosDoctosEnabled(String ic_epo)
		throws AppException;
	public boolean existeReferenciaEnComrelNotaDocto(String ic_nota_credito)
		throws AppException;
	public boolean esDocumentoConNotaDeCreditoMultipleAplicada(String ic_documento)
		throws AppException;
	public boolean existeDocumentoAsociado(String ic_nota_credito)
		throws AppException;
		public boolean esDocumentoConNotaDeCreditoSimpleAplicada(String ic_documento)
		throws AppException;

  public String obtenerPymeConNumeroDeProveedor(String numeroProveedor, String claveEpo) throws AppException;//FODEA 028 - 2010 ACF

	public String obtenerCorreoRepresentanteLegalBenef(String claveBeneficiario) throws AppException;//FODEA 032 - 2010 ACF
	public String obtenerEstatusAvisoPublicacionBenef(String claveBeneficiario) throws AppException;//FODEA 032 - 2010 ACF
	public void guardarEstatusAvisoPublicacionBenef(String claveBeneficiario, String loginUsuario, String csAvisoPublicacion) throws AppException;//FODEA 032 - 2010 ACF
	public HashMap obtenerBitacoraAvisoPublicacionBenef(String claveBeneficiario) throws AppException;//FODEA 032 - 2010 ACF
	//Fodea 060-2010
	public String getOperaFirmaMancomunada(String sNoEPO, int iNoProducto) 	 throws AppException;
	public String getValidarFechaLimite(String sNoEPO, String sNoIF) 	throws AppException; //Fodea 022-2011
	public List avisoLimiteLineaC(String sNoIF) throws AppException; //Fodea 022-2011

	//F021-2011
	public void setDatosLiberMasivaPyme(String ruta, String procesoId, String cveIf) throws AppException; //Fodea 021-2011
	public void validaCargaLiberMasiva(String procesoId, String cveIf) throws AppException; //Fodea 021-2011
	public HashMap getResLiberMasivaCtasPymes(String procesoId) throws AppException; //Fodea 021-2011
	public List getPreAcuseLiberMasivaPymes(String procesoId) throws AppException; //Fodea 021-2011
	public List getAcuseLiberMasivaPymes(String procesoId) throws AppException; //Fodea 021-2011
	public boolean validaHoraEnvOpeIFs(String cveEpo) throws AppException; //Fodea 026-2011

  public String obtenerJSONparametrosPorEpo(String claveEpo, String claveProducto) throws AppException;//FODEA 017 - 2011 ACF
  public HashMap obtenerInformacionEpo(String claveEpo) throws AppException;//FODEA 017 - 2011 ACF

	public String getParamPorEPOEXTJS(String sNoEPO, String icBancoFondeo)throws AppException; //FODEA 017 - 2011 DLHC

	public boolean getOperaFactorajeVencidoGral(int ic_pyme) throws AppException;//FODEA 017 - 2011 Descuentos-cambio de estatus
   public boolean getOperaFactorajeAutomaticoGral(int ic_pyme) throws AppException; //FODEA 026-2012
	public boolean getOperaFactorajeDistribuidoGral(int ic_pyme) throws AppException;//FODEA 017 - 2011 Descuentos-cambio de estatus
	public boolean getOperaNotasCreditoGral(int ic_pyme) throws AppException;//FODEA 017 - 2011 Descuentos-cambio de estatus

	public boolean mostrarOpcionPignorado(String icEpo, String icPyme) throws AppException;
	public Map getParametrosModuloDescuento(String claveAfiliado,
			String tipoAfiliado, String claveEpoRelacionada);
	public Registros getCamposAdicionales(String ic_epo);
	public Registros getCamposAdicionalesDetalle(String ic_epo);
	public Registros getTipoAforoPantalla(String ic_epo);
	public Registros getDatosDocumentos(String ic_epo, String ic_docto);
	public Registros getDatosDocumentosDetalle(String ic_docto);
	public Registros getDatosAcuse(String ccAcuse);
	public Registros getDatosTotalesAcuse(String ccAcuse);
	public Registros getDatosAcuse2(String ccAcuse);
	public Registros getModificacionMontos(String sFechaVencPyme, String ic_docto);

	public HashMap setParametrosDsctoAutomatico(String autorizacionDsctoAutomatico, String clavePyme, String claveEpo, String 	iNoUsuario,	String[] if_cta, String[] hidHuboCambio,	String[] hidHuboCambioDia,	String[] hidDsctoAutomaticoDia, String[] hidIcEpo, String[] hidIcMoneda	); // FODEA 017 - 2011
	public boolean contratoParametrizado(String claveEpo);
	public boolean contratoAceptado(String clavePyme, String claveEpo);
	public boolean contratoParametrizadoIF(String clavePyme);
	public boolean contratoAceptadoIF(String clavePyme);
	public boolean aceptaPromocion(String ic_pyme, String iNoEPO);
	public boolean hayCuentasPendientesPyme(String clavePyme);
	public List getMensajesInicialesAdicionales(String claveAfiliado,
			String tipoAfiliado, String claveEpoRelacionada);
	public String getNombreEPO(String claveEPO) throws AppException;
		//Fodea Migraci�n EPO
	public HashMap actualizaOtrosParametros(String porcentajeM, String porcentajeD, String operaFactVenc, String operaFactDist, String diasMin, String diasMax, String diasNotif, String diasNotifReact, String claveEpo, String sDesAutoFactVenci, String operaDescAutoEPO);  
	public HashMap datosOtrosParametros (String claveEpo, String flagfvenc);
	public HashMap  getOperaFactoraje(String claveEPO);

	public List getCamposAdicionales(String claveEPO, String producto) throws AppException;
	public String getfacVencido(String claveEPO, String producto) throws AppException;

	//Fodea Migraci�n IF
	public List  datosPymes(String  num_electronico , String ic_epo )throws AppException;
	public Hashtable  parametrizacion(String  ic_if, String ic_epo )throws AppException;
	public List  datosPymesInfSolic(String  num_electronico , String ic_epo )throws AppException;
	public Hashtable  parametrizacionIF(String  ic_if )throws AppException;
	public String  sIfConvenioUnico(String  ic_if ) throws AppException;

	public String  catalogoPyme(String  ic_epo, String ic_if );
	public boolean tieneExpedienteEnEfile(String rfc)throws AppException;
	public int  verifCuentasPendResul(String  ic_if ) ;
	public String  consAutotizacion(List parametos);
	public String  archivoConsAutotizacion(List parametros) ;
	public String  autorizacionCuenta(String  chkCtaBanc[] ,	String sIfConvenioUnico,String icIfActual, String strLogin , String iNoNafinElectronico, String strNombreUsuario, String strNombre	);
	public String  archivoAcuseAutorizacion(List parametros ) ;
	public String cuentasPedientes(String  ic_if ) ;
	public String  archivoConsAutoCarga(List parametros) ;

	public String  generarArchError(String numeroProceso, String  strDirectorioTemp ) throws AppException;
	public String  archivAcuseCSVyPDF(List parametros )  throws AppException;
	public int  verifPYMEsinNumProv(String claveEpo)  throws AppException;  //Migraci�n EPO

	public String getParametrosFactoraje(String ic_epo) throws AppException;


	public HashMap creaFormulario() throws AppException;  //Migraci�n Nafin
	public String  actOtrosParametros( List parametros )  throws AppException;  //Migraci�n Nafin
	public Registros getParametrosPymeNafin(String numeroDeProveedor);	//Migraci�n Nafin
	public Registros getParametrosPymeNafin(String numeroDeProveedor, String ic_banco_fondeo, String ic_epo);	//Migraci�n Nafin
	public Registros getUsuarioCesion(String id_usuario);	//Migraci�n Nafin
	public Registros getCtasPyme(String ic_pyme);
	public  boolean cambiaCuentas(String clavesIF,String icPyme,String cuentaNueva,String iNoUsuario,String strNombreUsuario);
	public Registros obtenerCuentasBancarias(String icPyme,String moneda);
	public Registros obtenerCuentaSeleccionada(String icPyme,String moneda,String ic_if,String ic_epo);
	public  String cambioCuentaIndividual(String EjecutarCambioCtaCliente,String txtaNoIF,String chkIF,
	String chkOperaIF,String hidOperaIF,String txtCtaAnterior,String rdCta,String cboMoneda,
	String Epo,String Pyme,String iNoUsuario,String strNombreUsuario, String ic_nafin_electronico , String login );
	public List getCuentasMasiva(String Epos[],String icPyme,String icMoneda);
	public List getCuentasMasivasGrupo(String Pyme,String icCuenta,String icGrupo);
	public List confirmarCuentasMasiva(String valores,String Epo,String Pyme, String strLogin , String strNombreUsuario, String ic_nafin_electronico );
	public boolean reasignarCuentasBancarias(String ic_if,String ic_pyme,String ic_epo_base,String ic_cuenta_bancaria,String [] clavesEpo ,String strLogin , String strNombreUsuario );

	public Hashtable getDatosPymeNafinElec(String noNafinElec, boolean consulta);
	public Hashtable getDatosPymeAmodificar(String sPyme, String txtIcCtaBanco );
	public String setDatosPymeAmodificar(String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,	String sPyme,
								String txtNE, String cboPlaza,	String txtCtaClabe,	String chkCU,	String txtIcCtaBanco, String iNoUsuario,  String txtCtaClabeC );
	public String eliminaDatosPymeAmodificar(String sPyme,	String txtNE, String txtIcCtaBanco, String iNoUsuario );
	public String addDatosPymeAmodificar(String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,	String sPyme,
												String txtNE, String cboPlaza,	String txtCtaClabe,	String chkCU,	String iNoUsuario, String confirtxtCuentaClabe  );
	public Registros getDatosPymeCatalogoIf(String ic_epo);
	public Hashtable addDatosCtasDist(String ic_epo,	String txtBanco,	String txtSucursal,	String cboMoneda,	String txtCuenta,
								String ic_if,	String txtNE, String cboPlaza,	String CtasCapturadas );
	public boolean addDatosCtasEpo(String ic_epo,String ic_if,String txtBanco,String txtSucursal,String txtCuenta,String cboPlaza,String txtCuentaClabe);
	public Registros getDatosCtasEpo(String ic_if,	String ic_epo);
	public String setDatosCtasEpo(String icIF,String icEpo, String txtNumCuenta, String ic_num_cuenta_CLABE,String VoBoNafin,
					String Plaza, String Sucursal, String cmb_banco, String txtBanco, String txtPlazaAnt, String iNoUsuario );

	public Registros getParametrosPymeNafinSeleccionIF(String numeroDeProveedor);
	public boolean obtenerInstruccionIrrevocable(String icPyme,String cboIF,String Epo);

	public List consultaClavesEpoWSIF(String cveIF)throws AppException;
	public void guardaCveEposWsIF(List lstClavesEpos)throws AppException;

	public String getOperaFideicomiIF(String ic_if)throws AppException; //Fodea 017-2013
	//public int getOperaConevio4PYME(String ic_pyme)throws AppException; //Fodea 017-2013
	public String getOperaFideicomisoEpo(String ic_epo)throws AppException; //Fodea 017-2013
	public String  getOperaFideicomisoPYME(String ic_pyme )throws AppException; //Fodea 019-2013

	public String  getOperaFactVencido(String ic_epo, String ic_if  )throws AppException; //Fodea 016-2014

	public String  getValidaCcuentaClablePyme(String ic_nafin_electronico  )throws AppException;   //Fodea 019-2014
	public String  getOpEntidadGobierno(String ic_pyme )throws AppException; //Fodea 034-2014
	public String  getValidaPymeEpoRea(String ic_pyme, String ic_epo  )throws AppException; //Fodea 019-2014

  public String  getValidaEposDispersionPyme(String ic_pyme  )throws AppException;  //Fodea 033-2014
  public String  getNombreMoneda(String ic_moneda )throws AppException;   //Fodea 033-2014
  public Hashtable getDatosDispersion(String ic_pyme, String nafinElectrico, String moneda, String txtCuenta  )throws AppException;   //Fodea 033-2014

  public String ovinsertarCuentaCBP(  String loginUsuario, String sPyme,   String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, String  ic_bancos_tef ,  String txtSucursal_cue,  String  txtPlaza_cue , String cg_cuenta_cue,
										String ic_tipo_cuenta, 	String cg_tipo_afiliado,  	String ic_Swift , String  ic_aba , 	String txtCtaClabe ,
										String ic_epos[], String ic_cuentas[] , String rfc ) throws NafinException;   //Fodea 033-2014

	public boolean getValidaCveCder() throws AppException;

	//Fodea 16-2018
	public List datosPymes(String numElectronico)throws AppException;
	public String obtenerJSONContratosFactDistribuido(String id, String tabla) throws AppException;
	public String obtenerJSONCuentasAgregadasTmp(List<String> ids, Long icConsecutivo, String tipoTabla, String usuario) throws AppException;
	public Long agregarCuentaTmp(String numNePyme,Long icEpo,Long icIf,Long benef,Long moneda,Long bancoServicio,Long plaza,String sucursal,String cuenta,String cuentaClabe,String cuentaSpid,String swift,String aba,Long icPyme, String[] cgContratos)throws NafinException;
	public Long agregarCuenta(Long icConsecutivo, String usuario)throws NafinException;
	public boolean modificarCuenta(Long icConsecutivo, String numNePyme,Long icEpo,Long icIf,Long icPyme,Long benef,Long moneda,Long bancoServicio,Long plaza,String sucursal,String cuenta,String cuentaClabe,String cuentaSpid,String swift,String aba, String usuario, String[] cgContratos)throws NafinException;
	public boolean eliminarCuenta(Long icConsecutivo, Long icBenef, String usuario)throws NafinException;
	public boolean existeCuentaDuplicada(List<String> ids, Long icEpo,Long icPyme,Long icIf,Long icMoneda,Long icBenef, String tipoTabla)throws NafinException;
	public boolean existeCuentaParametrizada(Long icEpo,Long icPyme,Long icIf,Long icMoneda)throws NafinException;
	public boolean eliminarCuentasTemporales(List<String> ids)throws NafinException;
	public String autorizarCuentasBeneficiarios(String[] cuentasPorAutorizar, String acuse, String recibo, String usuario, Long numNafinElec) throws NafinException;
	public String obtenerJSONCuentasAutorizadas(String acuse) throws AppException;
	public String obtenerJSONCambioCuentasPorAutorizar(Long numNafinElec) throws AppException;
}
