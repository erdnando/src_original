package com.netro.descuento;

public interface EpoEnlaceDetalle {

	public abstract String getTablaDetalles();
	
	public abstract String getDetalles(String ic_epo);
	
}//EpoEnlaceDetalle
