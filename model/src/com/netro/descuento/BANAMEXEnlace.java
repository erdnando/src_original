package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class BANAMEXEnlace implements OperadosEnlace,Serializable {
	
	private String 		query 						= null;
	private ArrayList		listaDeVariablesBind		= null;
	private String 		claveDeError				= null;
	private String			tablaDocumentos			= null;
	private String 		tablaErrores				= null;
	
	public BANAMEXEnlace() {
		setTablaDocumentos(	"COMTMP_DOCTOS_OPER_BANAMEX"	);
		setTablaErrores(		"COMCAT_ERROR"					   );
		setClaveDeError(		"BNMX"								);
	}
		
	public String 		getTablaDocumentos()						{	return tablaDocumentos;			}
	public String 		getTablaErrores()							{	return tablaErrores;				}
	public String 		getQuery()									{	return query;						}  
	public String 		getClaveDeError()							{	return claveDeError;				}
	public ArrayList  getListaDeVariablesBind()				{	return listaDeVariablesBind;	}
	
	public void 		setTablaDocumentos(String	tablaDocumentos)					{	this.tablaDocumentos 		= tablaDocumentos;		}
	public void 		setTablaErrores(String	tablaErrores)							{	this.tablaErrores				= tablaErrores;			}
	public void 		setQuery(String queryDocumentos)									{	this.query						= query;						}  
	public void 		setClaveDeError(String claveModuloError)						{	this.claveDeError 			= claveDeError;			}
	public void  		setListaDeVariablesBind(ArrayList listaDeVariablesBind)	{	this.listaDeVariablesBind	= listaDeVariablesBind; }
	
	public void 		generaQueryDeDocumentos(){
		
		listaDeVariablesBind = new ArrayList();
		listaDeVariablesBind.add("OK");

		
		query =
					"SELECT  "  +
					"    B.IC_IF AS SC_IF, "  +
					"    B.IC_EPO AS SC_EPO, "  +
					"    B.IN_NUMERO_SIRAC AS SC_SIRAC,  "  +
					"    B.IC_MONEDA AS SC_MONEDA,  "  +
					"    B.IG_NUMERO_DOCTO AS IG_NUMERO_DOCTO,  "  +
					"    TO_CHAR(B.DF_VENCIMIENTO,'dd/mm/yyyy') AS SF_VENCIMIENTO,  "  +
					"    B.CG_ESTATUS AS SC_ESTATUS  "  +
					"FROM "  +
					"    " + getTablaDocumentos() +" B "  +
					"WHERE "  +
					"	  UPPER(B.CG_ESTATUS) 	= ? and " +
					"	  B.CC_ACUSE IS NULL  AND " +
					"	  B.DF_FECHA_OPER IS NULL	";
		        
	}
	
	public void	generaQueryDeDocumentosConError(){
		
		listaDeVariablesBind = new ArrayList();
		listaDeVariablesBind.add("OK");
		listaDeVariablesBind.add(new Integer(1));
		listaDeVariablesBind.add(new Integer(4));
		listaDeVariablesBind.add(new Integer(5));
		listaDeVariablesBind.add(new Integer(8));
		listaDeVariablesBind.add(new Integer(9));
		
		query =
					"SELECT  "  +
					"    B.IC_IF AS SC_IF, "  +
					"    B.IC_EPO AS SC_EPO, "  +
					"    B.IN_NUMERO_SIRAC AS SC_SIRAC,  "  +
					"    B.IC_MONEDA AS SC_MONEDA,  "  +
					"    B.IG_NUMERO_DOCTO AS IG_NUMERO_DOCTO,  "  +
					"    TO_CHAR(B.DF_VENCIMIENTO,'dd/mm/yyyy') AS SF_VENCIMIENTO,  "  +
					"    B.CG_ESTATUS AS SC_ESTATUS,  "  +
					"    B.CC_ACUSE AS ACUSE, "  +
					"    E.CG_DESCRIPCION "  +
					"FROM "  +
					"    " + getTablaDocumentos() +" B, "  +
					"    " + getTablaErrores()    +" E  "  +
					"WHERE "  +
					"	  B.CG_ESTATUS 		   != ?     AND "  +
					"	  B.DF_FECHA_OPER			IS NULL  AND "  +
					"    E.CC_MODULO 				=  SUBSTR(B.CG_ESTATUS, ?, ?) AND "  +
					"    E.IC_ERROR  				=  TO_NUMBER(SUBSTR(B.CG_ESTATUS, ?, ?)) AND "  +
					"    LENGTH(B.CG_ESTATUS) 	<  ? ";
		        
	}
		
}
