package com.netro.descuento;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "InformacionTOICEJB" , mappedName = "InformacionTOICEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class InformacionTOICBean implements InformacionTOIC {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(InformacionTOICBean.class);
	
	/**
	 * M�todo que hace una consulta en la base de datos de Promoepo para obtener los datos del reporte de Resumen de Operaciones.
	 * @deprecated Fodea 040 - 2011
	 */	
	 public HashMap getDatosCtaPdf(String fechaInicio, String fechaFin, String ic_epo)
	 	throws SQLException, Exception{
		
		System.out.println("getDatosCtaPdf(E)");
		
		AccesoDB 			con				= new AccesoDB();
		
		ResultSet 			rsQuery       	= null;
		PreparedStatement stQuery       	= null;
		
		try
		{
			System.out.println("Begin EdoCtaPefDAO.getDatosCtaPdf()");
			
			con.conexionDB();

			HashMap 	map 										= new HashMap();

			String montoPublicado   = " SUM (PUB.FN_MONTO_PUBLICADO)          FN_MONTO_PUBLICADO,   "; // Fodea 040 - 2011
			String montoSuceptible  = " SUM (PUB.FN_MONTO_SUSCEPTIBLE)        FN_MONTO_SUSCEPTIBLE  "; // Fodea 040 - 2011
			String montoOperado     = " ROUND( SUM (OPE.FN_MONTO_OPERADO), 2) FN_MONTO_OPERADO      "; // Fodea 040 - 2011
			String montoNetoOperado = " SUM (OPE.FN_MONTO_DSCTO)              FN_MONTO_NETO         "; // Fodea 040 - 2011
			String interes          = " SUM (OPE.FN_MONTO_INTERES)            FN_MONTO_INTERES      "; // Fodea 040 - 2011
			
			String ic_moneda			= null;
			String qryInformacion   = null;
         
			/*
			 * CALCULAR "TOTALES" PARA MONEDA NACIONAL
			 */
			ic_moneda = "1"; // Moneda Nacional
			qryInformacion =
				"\n SELECT                                                                  "  +
				"\n        PUB.IG_NUM_DOCTOS                     AS DOCUMENTOS,             "  +
				"\n        PUB.FN_MONTO_PUBLICADO                AS MONTO_TOTAL,            "  +
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA_PUB, 2)  AS PLAZO_PROM_PUBL,        "  +
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA, 2)      AS PLAZO_PROM_PAGO,        "  +
				"\n        PUB.IG_DOCTOS_SUSCEPTIBLE             AS DOCS_SUSEP_FACTORAJE,   "  +
				"\n        PUB.FN_MONTO_SUSCEPTIBLE              AS MONTO_SUCEP,            "  +
				"\n        OPE.IG_NUM_DOCTOS                     AS DOCS_OPERADOS,          "  +
				"\n        OPE.FN_MONTO_OPERADO                  AS MONTO_OPERADO,          "  +
				"\n        OPE.FN_MONTO_NETO                     AS FN_MONTO_NETO,          "  +
				"\n        OPE.FN_MONTO_INTERES                  AS FN_MONTO_INTERES        "  +
				"\n FROM   "  +
				"\n        ( " +
				"\n         SELECT /*+ INDEX(PUB)*/ " +
				"\n                PUB.IC_EPO, " +
				"\n                SUM (PUB.IG_NUM_DOCTOS)         IG_NUM_DOCTOS,"   +
				"\n               " + montoPublicado +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA_PUB)/ SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA_PUB, "   +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA)    / SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA, "   +
				"\n                SUM (PUB.IG_DOCTOS_SUSCEPTIBLE)  IG_DOCTOS_SUSCEPTIBLE, "   +
				"\n               " + montoSuceptible +
				"\n         FROM   R_PRM_PUBL_HIST_PYMES_NE PUB "   +
				"\n         WHERE  PUB.ic_epo           = " + ic_epo +
				"\n         AND    PUB.DF_PUBLICACION >= TO_DATE ('" + fechaInicio + "', 'dd/mm/yyyy')"   +
				"\n         AND    PUB.DF_PUBLICACION  < TO_DATE ('" + fechaFin    + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    PUB.IC_MONEDA =  " +ic_moneda+ "  " + // Fodea 040 - 2011
				"\n         GROUP BY PUB.IC_EPO " +
				"\n        ) PUB,"   +
				"\n       ( " +
				"\n         SELECT IC_EPO, " +
				"\n                COUNT (OPE.IC_PYME)        IC_PYME, " +
				"\n                SUM (OPE.IG_NUM_DOCTOS)    IG_NUM_DOCTOS,"   +
				"\n               " + montoOperado     + ", " +
				"\n               " + montoNetoOperado + ", " +
				"\n               " + interes          + "  " +
				"\n         FROM   R_PRM_OPER_HIST_PYMES_NE OPE "   +
				"\n         WHERE  OPE.IC_EPO = " + ic_epo +
				"\n         AND    OPE.DF_OPERACION >= TO_DATE ('" + fechaInicio+ "', 'dd/mm/yyyy')"   +
				"\n         AND    OPE.DF_OPERACION  < TO_DATE ('" + fechaFin   + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    OPE.IC_MONEDA =  " +ic_moneda+ "  " + // Fodea 040 - 2011
				"\n         GROUP BY IC_EPO " +
				"\n        ) OPE,"   +
				"\n        R_VM_OPER_EPOS                 VM " +
				"\n WHERE  "  +
				"\n        VM.IC_EPO             = PUB.IC_EPO(+)  "  +
				"\n AND    VM.IC_EPO             = OPE.IC_EPO(+)  "  +
				"\n AND    VM.IC_EPO            != 16"  +
				"\n AND    vm.ic_epo 			   = "+ ic_epo ;
				
			System.out.println("Query datos PDF(MXN): " + qryInformacion);
	
			stQuery = con.queryPrecompilado(qryInformacion);
			rsQuery = stQuery.executeQuery();	
			
			if(rsQuery.next()){

				/**************************************************************************************************************************************
				 	SI BANCO DE FONDEO EN VM_OPER_EPOS ES NAFIN   OR   SI BANCO DE FONDEO EN VM_OPER_EPOS ES BANCOMEXT   Y   TIENE OPERACI�N CON NAFIN
				 **************************************************************************************************************************************/
 
					 // 1. Proveedores
					map.put("ProveedoresRegistrados_MXN",        "0"); // No se ocupa
					map.put("ProveedoresCXPRegistradas_MXN",		"0"); // No se ocupa
					
					// 2. Documentos Registrados 
					map.put("Documentos_MXN",							(rsQuery.getString("DOCUMENTOS")           == null)?"0":rsQuery.getString("DOCUMENTOS"));
					map.put("MontoTotal_MXN",							(rsQuery.getString("MONTO_TOTAL")          == null)?"0":rsQuery.getString("MONTO_TOTAL"));
					/*
					double dPlazoPromedioRegistroCXP     			= (rsQuery.getString("PLAZO_PROM_PUBL")    == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PUBL"));
					map.put("PlazoPromedioRegistroCXP_MXN",		""+Math.round(dPlazoPromedioRegistroCXP));
					*/
					map.put("PlazoPromedioRegistroCXP_MXN",		"0"); // No se ocupa
					/*
					double dPlazoPromedioPago           			= (rsQuery.getString("PLAZO_PROM_PAGO")      == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PAGO"));
					map.put("PlazoPromedioPago_MXN",					""+Math.round(dPlazoPromedioPago));	
					*/
					map.put("PlazoPromedioPago_MXN",					"0"); // No se ocupa	
					map.put("DocumentosSuscepFactoraje_MXN",		(rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?"0":rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					map.put("MontoSusceptibleFactoraje_MXN",		(rsQuery.getString("MONTO_SUCEP")          == null)?"0":rsQuery.getString("MONTO_SUCEP"));
					
					// 3. Factoraje
					
					map.put("DocumentosOperadosFactoraje_MXN",	(rsQuery.getString("DOCS_OPERADOS")    == null)?"0":rsQuery.getString("DOCS_OPERADOS"));
					map.put("MontoOperadoFactoraje_MXN",			(rsQuery.getString("MONTO_OPERADO")    == null)?"0":rsQuery.getString("MONTO_OPERADO"));
					map.put("InteresesGeneradosFactoraje_MXN",	(rsQuery.getString("FN_MONTO_INTERES") == null)?"0":rsQuery.getString("FN_MONTO_INTERES"));
					map.put("MontoTotalOperado_MXN",					(rsQuery.getString("FN_MONTO_NETO")    == null)?"0":rsQuery.getString("FN_MONTO_NETO"));

					// 4. Estadisticas
					// Total Proveedores con CXP registradas / Total Proveedores registrados
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_MXN","0");  // No se ocupa
						
					// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
					double dDocumentosSuscepFactoraje   = (rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?0:Double.parseDouble(rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					double dDocumentosOperadosFactoraje = (rsQuery.getString("DOCS_OPERADOS")        == null)?0:Double.parseDouble(rsQuery.getString("DOCS_OPERADOS"));
					if((dDocumentosSuscepFactoraje == 0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0){
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_MXN","0");
					}else{
						dDocumentosOperadosFactoraje = dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje;
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_MXN",""+dDocumentosOperadosFactoraje);
					}

					// Monto Operado en Factoraje / Monto Susceptible de Factoraje
					double dMontoOperadoFactoraje   		= (rsQuery.getString("MONTO_OPERADO") == null)?0:Double.parseDouble(rsQuery.getString("MONTO_OPERADO"));
					double dMontoSusceptibleFactoraje 	= (rsQuery.getString("MONTO_SUCEP")   == null)?0:Double.parseDouble(rsQuery.getString("MONTO_SUCEP"));										
					if((dMontoOperadoFactoraje == 0 && dMontoSusceptibleFactoraje==0) || dMontoSusceptibleFactoraje==0){
						map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_MXN","0");
					}else{
						dMontoOperadoFactoraje = dMontoOperadoFactoraje / dMontoSusceptibleFactoraje ;
						map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_MXN", ""+dMontoOperadoFactoraje);
					}

   			} // end if resultset

			//System.out.println("map =" + map.toString());

      	rsQuery.close();
			stQuery.close();

			/*
			 * CALCULAR TOTALES PARA "DOLARES AMERICANOS"
			 */
			ic_moneda = "54";  // Dolares
			qryInformacion =
				"\n SELECT                                                                  "  +
				"\n        PUB.IG_NUM_DOCTOS                     AS DOCUMENTOS,             "  +
				"\n        PUB.FN_MONTO_PUBLICADO                AS MONTO_TOTAL,            "  +
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA_PUB, 2)  AS PLAZO_PROM_PUBL,        "  +
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA, 2)      AS PLAZO_PROM_PAGO,        "  +
				"\n        PUB.IG_DOCTOS_SUSCEPTIBLE             AS DOCS_SUSEP_FACTORAJE,   "  +
				"\n        PUB.FN_MONTO_SUSCEPTIBLE              AS MONTO_SUCEP,            "  +
				"\n        OPE.IG_NUM_DOCTOS                     AS DOCS_OPERADOS,          "  +
				"\n        OPE.FN_MONTO_OPERADO                  AS MONTO_OPERADO,          "  +
				"\n        OPE.FN_MONTO_NETO                     AS FN_MONTO_NETO,          "  +
				"\n        OPE.FN_MONTO_INTERES                  AS FN_MONTO_INTERES        "  +
				"\n FROM   "  +
				"\n        ( " +
				"\n         SELECT /*+ INDEX(PUB)*/ " +
				"\n                PUB.IC_EPO, " +
				"\n                SUM (PUB.IG_NUM_DOCTOS)         IG_NUM_DOCTOS,"   +
				"\n               " + montoPublicado +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA_PUB)/ SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA_PUB, "   +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA)    / SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA, "   +
				"\n                SUM (PUB.IG_DOCTOS_SUSCEPTIBLE)  IG_DOCTOS_SUSCEPTIBLE, "   +
				"\n               " + montoSuceptible +
				"\n         FROM   R_PRM_PUBL_HIST_PYMES_NE PUB "   +
				"\n         WHERE  PUB.ic_epo           = " + ic_epo +
				"\n         AND    PUB.DF_PUBLICACION >= TO_DATE ('" + fechaInicio + "', 'dd/mm/yyyy')"   +
				"\n         AND    PUB.DF_PUBLICACION  < TO_DATE ('" + fechaFin    + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    PUB.IC_MONEDA =  " +ic_moneda+ "  " + // Fodea 040 - 2011
				"\n         GROUP BY PUB.IC_EPO " +
				"\n        ) PUB,"   +
				"\n       ( " +
				"\n         SELECT IC_EPO, " +
				"\n                COUNT (OPE.IC_PYME)        IC_PYME, " +
				"\n                SUM (OPE.IG_NUM_DOCTOS)    IG_NUM_DOCTOS,"   +
				"\n               " + montoOperado     + ", " +
				"\n               " + montoNetoOperado + ", " +
				"\n               " + interes          + "  " +
				"\n         FROM   R_PRM_OPER_HIST_PYMES_NE OPE "   +
				"\n         WHERE  OPE.IC_EPO = " + ic_epo +
				"\n         AND    OPE.DF_OPERACION >= TO_DATE ('" + fechaInicio+ "', 'dd/mm/yyyy')"   +
				"\n         AND    OPE.DF_OPERACION  < TO_DATE ('" + fechaFin   + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    OPE.IC_MONEDA =  " +ic_moneda+ "  " + // Fodea 040 - 2011
				"\n         GROUP BY IC_EPO " +
				"\n        ) OPE,"   +
				"\n        R_VM_OPER_EPOS                 VM " +
				"\n WHERE  "  +
				"\n        VM.IC_EPO             = PUB.IC_EPO(+)  "  +
				"\n AND    VM.IC_EPO             = OPE.IC_EPO(+)  "  +
				"\n AND    VM.IC_EPO            != 16"  +
				"\n AND    vm.ic_epo 			   = "+ ic_epo ;
				
			System.out.println("Query datos PDF(DL): " + qryInformacion);
	
			stQuery = con.queryPrecompilado(qryInformacion);
			rsQuery = stQuery.executeQuery();	
			
			if(rsQuery.next()){

				/**************************************************************************************************************************************
				 	SI BANCO DE FONDEO EN VM_OPER_EPOS ES NAFIN   OR   SI BANCO DE FONDEO EN VM_OPER_EPOS ES BANCOMEXT   Y   TIENE OPERACI�N CON NAFIN
				 **************************************************************************************************************************************/
 
					// 1. Proveedores
					map.put("ProveedoresRegistrados_DL",		"0"); // No se ocupa
					map.put("ProveedoresCXPRegistradas_DL",	"0"); // No se ocupa
					
					// 2. Documentos Registrados 
					map.put("Documentos_DL",						(rsQuery.getString("DOCUMENTOS")           == null)?"0":rsQuery.getString("DOCUMENTOS"));
					map.put("MontoTotal_DL",						(rsQuery.getString("MONTO_TOTAL")          == null)?"0":rsQuery.getString("MONTO_TOTAL"));
					/*
					double dPlazoPromedioRegistroCXP     		= (rsQuery.getString("PLAZO_PROM_PUBL")      == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PUBL"));
					map.put("PlazoPromedioRegistroCXP_DL",		""+Math.round(dPlazoPromedioRegistroCXP));
					*/
					map.put("PlazoPromedioRegistroCXP_DL",		"0"); // No se ocupa
					/*
					double dPlazoPromedioPago            		= (rsQuery.getString("PLAZO_PROM_PAGO")      == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PAGO"));
					map.put("PlazoPromedioPago_DL",				""+Math.round(dPlazoPromedioPago));
					*/
					map.put("PlazoPromedioPago_DL",				"0"); // No se ocupa
					map.put("DocumentosSuscepFactoraje_DL",	(rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?"0":rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					map.put("MontoSusceptibleFactoraje_DL",	(rsQuery.getString("MONTO_SUCEP")          == null)?"0":rsQuery.getString("MONTO_SUCEP"));
					
					// 3. Factoraje
					
					map.put("DocumentosOperadosFactoraje_DL",	(rsQuery.getString("DOCS_OPERADOS")    == null)?"0":rsQuery.getString("DOCS_OPERADOS"));
					map.put("MontoOperadoFactoraje_DL",			(rsQuery.getString("MONTO_OPERADO")    == null)?"0":rsQuery.getString("MONTO_OPERADO"));
					map.put("InteresesGeneradosFactoraje_DL",	(rsQuery.getString("FN_MONTO_INTERES") == null)?"0":rsQuery.getString("FN_MONTO_INTERES"));
					map.put("MontoTotalOperado_DL",				(rsQuery.getString("FN_MONTO_NETO")    == null)?"0":rsQuery.getString("FN_MONTO_NETO"));

					// 4. Estadisticas
					// Total Proveedores con CXP registradas / Total Proveedores registrados
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_DL","0"); // No se ocupa
					
					// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
					double dDocumentosSuscepFactoraje   = (rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?0:Double.parseDouble(rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					double dDocumentosOperadosFactoraje = (rsQuery.getString("DOCS_OPERADOS")        == null)?0:Double.parseDouble(rsQuery.getString("DOCS_OPERADOS"));
					if((dDocumentosSuscepFactoraje == 0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0){
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_DL","0");
					}else{
						dDocumentosOperadosFactoraje = dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje;
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_DL",""+dDocumentosOperadosFactoraje);
					}

					// Monto Operado en Factoraje / Monto Susceptible de Factoraje
					double dMontoOperadoFactoraje   		= (rsQuery.getString("MONTO_OPERADO") == null)?0:Double.parseDouble(rsQuery.getString("MONTO_OPERADO"));
					double dMontoSusceptibleFactoraje 	= (rsQuery.getString("MONTO_SUCEP")   == null)?0:Double.parseDouble(rsQuery.getString("MONTO_SUCEP"));										
					if((dMontoOperadoFactoraje == 0 && dMontoSusceptibleFactoraje==0) || dMontoSusceptibleFactoraje==0){
						map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_DL","0");
					}else{
						dMontoOperadoFactoraje = dMontoOperadoFactoraje / dMontoSusceptibleFactoraje ;
						map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_DL", ""+dMontoOperadoFactoraje);
					}

   			} // end if resultset

			//System.out.println("map =" + map.toString());

      	rsQuery.close();
			stQuery.close();
 
			/*
			 * CALCULAR TOTAL GENERAL
			 */
			qryInformacion =
				"\n SELECT VM.CG_RAZON_SOCIAL                          AS EPO, "             +
				"\n        decode( VM.ic_tipo_organismo,null,0,1 ) 	 AS pef2,   " 			  +
				"\n        decode( VM.ic_secretaria, null, 0, 1 ) 		 AS pef1, " 			  +
				"\n        REG.IC_PYME                                 AS PROV_REG,"         + // Total Proveedores registrados
				"\n        PUB2.IC_PYME                                AS PROV_CON_PUB,"     + // Total Proveedores con CXP registradas 
				"\n        PUB.IG_NUM_DOCTOS                     		 AS DOCUMENTOS,"       + // Numero total de Documentos
				"\n        PUB.FN_MONTO_PUBLICADO                		 AS MONTO_TOTAL, "     + // Monto Total
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA_PUB, 2)        AS PLAZO_PROM_PUBL,"  + // Plazo Promedio de Registro de CXP
				"\n        ROUND(PUB.IG_DIAS_DIFERENCIA, 2)            AS PLAZO_PROM_PAGO,"  + // Plazo Promedio Pago
				"\n        PUB.IG_DOCTOS_SUSCEPTIBLE                   AS DOCS_SUSEP_FACTORAJE,"  + // Documentos Suscep Factoraje
				"\n        PUB.FN_MONTO_SUSCEPTIBLE                    AS MONTO_SUCEP,"      + // Monto Susceptible Factoraje
				"\n        OPE.IG_NUM_DOCTOS                           AS DOCS_OPERADOS,"    + // Documentos Operados Factoraje
				"\n        OPE.FN_MONTO_OPERADO                        AS MONTO_OPERADO, "   + // Monto Operado Factoraje
				"\n        OPE.FN_MONTO_NETO                           AS FN_MONTO_NETO, "   + // Monto Total Operado
				"\n        OPE.FN_MONTO_INTERES                        AS FN_MONTO_INTERES " + // Monto de Intereses Pagados por proveedores
				"\n FROM   ( " +
				"\n         SELECT IC_EPO, COUNT (REP.IC_PYME) IC_PYME"   +
				"\n         FROM   R_PRMREL_EPO_PYME REP"   +
//				"\n         WHERE  REP.DF_RELACION  >= TO_DATE ('" + fechaInicio + "', 'dd/mm/yyyy') "   +
				"\n         WHERE REP.DF_RELACION  < TO_DATE ('" + fechaFin + "', 'dd/mm/yyyy') + 1"   +
				"\n         AND    ic_epo           = " + ic_epo +
				"\n         GROUP  BY IC_EPO " +
				"\n        ) REG,"   +
				"\n        ( " +
				"\n         SELECT /*+ INDEX(PUB)*/ " +
				"\n                PUB.IC_EPO, " +
				"\n                SUM (PUB.IG_NUM_DOCTOS)         IG_NUM_DOCTOS,"   +
				"\n               " + montoPublicado +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA_PUB)/ SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA_PUB, "   +
				"\n                SUM (PUB.IG_DIAS_DIFERENCIA)    / SUM (PUB.IG_NUM_DOCTOS) IG_DIAS_DIFERENCIA, "   +
				"\n                SUM (PUB.IG_DOCTOS_SUSCEPTIBLE)  IG_DOCTOS_SUSCEPTIBLE, "   +
				"\n               " + montoSuceptible +
				"\n         FROM   R_PRM_PUBL_HIST_PYMES_NE PUB "   +
				"\n         WHERE  PUB.ic_epo           = " + ic_epo +
				"\n         AND    PUB.DF_PUBLICACION >= TO_DATE ('" + fechaInicio + "', 'dd/mm/yyyy')"   +
				"\n         AND    PUB.DF_PUBLICACION  < TO_DATE ('" + fechaFin    + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    PUB.IC_MONEDA IN  (1, 54) " + // Fodea 040 - 2011
				"\n         GROUP BY PUB.IC_EPO " +
				"\n        ) PUB,"   +
				"\n       ( " +
				"\n         SELECT B.IC_EPO IC_EPO, COUNT (B.IC_PYME) IC_PYME " +
				"\n         FROM ( " +
				"\n                SELECT /*+ INDEX(A)*/ DISTINCT A.IC_PYME IC_PYME, A.IC_EPO IC_EPO " +
				"\n                FROM   R_PRM_PUBL_HIST_PYMES_NE A " +
				"\n                WHERE  A.IC_EPO = " + ic_epo +
				"\n                AND    A.DF_PUBLICACION >= TO_DATE ('" + fechaInicio+ "', 'dd/mm/yyyy') " +
				"\n                AND    A.DF_PUBLICACION  < TO_DATE ('" + fechaFin   + "', 'dd/mm/yyyy') + 1 " +
				"\n			       AND    A.IC_MONEDA  IN  (1, 54)   " + // Fodea 040 - 2011
				"\n              )B " +
				"\n         GROUP BY B.IC_EPO " +
				"\n       )PUB2, " +
				"\n       ( " +
				"\n         SELECT IC_EPO, " +
				"\n                COUNT (OPE.IC_PYME)        IC_PYME, " +
				"\n                SUM (OPE.IG_NUM_DOCTOS)    IG_NUM_DOCTOS,"   +
				"\n               " + montoOperado     + ", " +
				"\n               " + montoNetoOperado + ", " +
				"\n               " + interes          + "  " +
				"\n         FROM   R_PRM_OPER_HIST_PYMES_NE OPE "   +
				"\n         WHERE  OPE.IC_EPO = " + ic_epo +
				"\n         AND    OPE.DF_OPERACION >= TO_DATE ('" + fechaInicio+ "', 'dd/mm/yyyy')"   +
				"\n         AND    OPE.DF_OPERACION  < TO_DATE ('" + fechaFin   + "', 'dd/mm/yyyy') + 1"   +
				"\n			AND    OPE.IC_MONEDA  IN  (1, 54) " + // Fodea 040 - 2011
				"\n         GROUP BY IC_EPO " +
				"\n        ) OPE,"   +
				"\n        R_VM_OPER_EPOS                 VM " +
				"\n WHERE  VM.IC_EPO             = REG.IC_EPO(+)"   +
				"\n AND    VM.IC_EPO             = PUB.IC_EPO(+)"   +
				"\n AND    VM.IC_EPO             = OPE.IC_EPO(+)"   +
				"\n AND    VM.IC_EPO             = PUB2.IC_EPO(+)"  +
				"\n AND    VM.IC_EPO            != 16"  +
				"\n AND    vm.ic_epo 				= "+ ic_epo ;
				
			System.out.println("Query datos PDF(TOTALES): " + qryInformacion);
	
			stQuery = con.queryPrecompilado(qryInformacion);
			rsQuery = stQuery.executeQuery();	
			
			if(rsQuery.next()){

				/**************************************************************************************************************************************
				 	SI BANCO DE FONDEO EN VM_OPER_EPOS ES NAFIN   OR   SI BANCO DE FONDEO EN VM_OPER_EPOS ES BANCOMEXT   Y   TIENE OPERACI�N CON NAFIN
				 **************************************************************************************************************************************/
 
				 	map.put("DependenciaEntidad",					rsQuery.getString("EPO"));
					map.put("Secretaria",							rsQuery.getString("pef1"));
					map.put("Tipo",									rsQuery.getString("pef2"));
					
					// 1. Proveedores
					map.put("ProveedoresRegistrados",			(rsQuery.getString("PROV_REG")             == null)?"0":rsQuery.getString("PROV_REG"));
					map.put("ProveedoresCXPRegistradas",		(rsQuery.getString("PROV_CON_PUB")         == null)?"0":rsQuery.getString("PROV_CON_PUB"));
					
					// 2. Documentos Registrados 
					map.put("Documentos",							(rsQuery.getString("DOCUMENTOS")           == null)?"0":rsQuery.getString("DOCUMENTOS"));
					map.put("MontoTotal",							"0"); // No se ocupa
					double dPlazoPromedioRegistroCXP     		= (rsQuery.getString("PLAZO_PROM_PUBL")      == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PUBL"));
					map.put("PlazoPromedioRegistroCXP",			""+Math.round(dPlazoPromedioRegistroCXP));
					double dPlazoPromedioPago            		= (rsQuery.getString("PLAZO_PROM_PAGO")      == null)? 0 :Double.parseDouble(rsQuery.getString("PLAZO_PROM_PAGO"));
					map.put("PlazoPromedioPago",					""+Math.round(dPlazoPromedioPago));	
					map.put("DocumentosSuscepFactoraje",		(rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?"0":rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					map.put("MontoSusceptibleFactoraje",		"0"); // No se ocupa
					
					// 3. Factoraje
					
					map.put("DocumentosOperadosFactoraje",		(rsQuery.getString("DOCS_OPERADOS")    == null)?"0":rsQuery.getString("DOCS_OPERADOS"));
					map.put("MontoOperadoFactoraje",				"0"); // No se ocupa
					map.put("InteresesGeneradosFactoraje",		"0"); // No se ocupa
					map.put("MontoTotalOperado",				   "0"); // No se ocupa
					
					
					// 4. Estadisticas
					// Total Proveedores con CXP registradas / Total Proveedores registrados
					double dProveedoresCXPRegistradas = (rsQuery.getString("PROV_CON_PUB") == null)?0:Double.parseDouble(rsQuery.getString("PROV_CON_PUB"));
					double dProveedoresRegistrados    = (rsQuery.getString("PROV_REG")     == null)?0:Double.parseDouble(rsQuery.getString("PROV_REG"));
					if( (dProveedoresCXPRegistradas==0 && dProveedoresRegistrados==0) || dProveedoresRegistrados==0 ){
						map.put("TProveedoresCXPRegistradas_proveedoresRegistrados","0");
					}else{
						dProveedoresCXPRegistradas=dProveedoresCXPRegistradas/dProveedoresRegistrados;
						map.put("TProveedoresCXPRegistradas_proveedoresRegistrados",""+dProveedoresCXPRegistradas);
					}

					// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
					double dDocumentosSuscepFactoraje   = (rsQuery.getString("DOCS_SUSEP_FACTORAJE") == null)?0:Double.parseDouble(rsQuery.getString("DOCS_SUSEP_FACTORAJE"));
					double dDocumentosOperadosFactoraje = (rsQuery.getString("DOCS_OPERADOS")        == null)?0:Double.parseDouble(rsQuery.getString("DOCS_OPERADOS"));
					if((dDocumentosSuscepFactoraje == 0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0){
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje","0");
					}else{
						dDocumentosOperadosFactoraje = dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje;
						map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje",""+dDocumentosOperadosFactoraje);
					}

					// Monto Operado en Factoraje / Monto Susceptible de Factoraje
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje","0"); // No se ocupa
					
   			} // end if resultset

			//System.out.println("map =" + map.toString());

      	rsQuery.close();
			stQuery.close();
 
			return map;

		}catch(SQLException sqle){
			System.out.println("InformacionTOICBean.getDatosCtaPdf -SQLException Exception -: "+ sqle);
			sqle.printStackTrace();
			throw sqle;
		}catch(Exception e){
			System.out.println("InformacionTOICBean.getDatosCtaPdf - Exception -: "+ e);
			e.printStackTrace();
			throw e;
		} finally {
			
			if( rsQuery != null ){ try{ rsQuery.close(); }catch(Exception e){} }
			if( stQuery != null ){ try{ stQuery.close(); }catch(Exception e){} }
			
			con.terminaTransaccion(true); //Se ejecuta el commit en un select para que no haya problemas con la conexion a tablas remotas
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("getDatosCtaPdf(S)");
			
		}

	}
	
	/**
	 * Metodo que muestra el resumen de la carga de acuerdo a la fecha en que
	 * se realizo el proceso mostrando los registros cargados, los registros
	 * encontrados y los no encontrados de FFON en NAE
	 * @throws com.netro.exception.NafinException
	 * @return Lista con los datos procesados
	 * @param fecha_proceso_a String con el rango final de la fecha del proceso
	 * @param fecha_proceso_de String con el rango iniciar de la fecha del proceso
	 */
	public List getResumenCargaFFON(String fecha_proceso_de, String fecha_proceso_a) throws NafinException{
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer 	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		List				resultados		= new ArrayList();
		System.out.println("InformacionTOICBean::getResumenCargaFFON(E)");
		try 
		{	
			con.conexionDB();
			qrySentencia.append(
				" SELECT ic_carga_ffon CLAVE_CARGA, " + 
				"\n 		TO_CHAR(df_proceso, 'dd/mm/yyyy') FECHA_PROCESO, " + 
				"\n 		ig_registros_cargados CARGADOS, " +
				"\n 		ig_registros_encontrados_ne ENCONTRADOS, " +
				"\n 		ig_registros_no_encontrados_ne NO_ENCONTRADOS " +
				"\n FROM com_resumen_carga_ffon " + 
				"\n WHERE 1=1 ");
			if(!"".equals(fecha_proceso_de) && fecha_proceso_de != null){
				qrySentencia.append("\n AND df_proceso >= to_date(?, 'dd/mm/yyyy') " + 
					"\n 		AND df_proceso < to_date(?, 'dd/mm/yyyy') + 1 ");
				lVarBind.add(fecha_proceso_de);
				lVarBind.add(fecha_proceso_a);
			}
			
			System.out.println("query:: " + qrySentencia);
			System.out.println("bind:: " + lVarBind);
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next() ){
				HashMap registro = new HashMap();
				registro.put("CLAVE_CARGA",				registros.getString("CLAVE_CARGA"));
				registro.put("FECHA_PROCESO",				registros.getString("FECHA_PROCESO"));
				registro.put("CARGADOS",					registros.getString("CARGADOS").equals("")?"0":registros.getString("CARGADOS"));
				registro.put("ENCONTRADOS",				registros.getString("ENCONTRADOS").equals("")?"0":registros.getString("ENCONTRADOS"));
				registro.put("NO_ENCONTRADOS",			registros.getString("NO_ENCONTRADOS").equals("")?"0":registros.getString("NO_ENCONTRADOS"));
				
				resultados.add(registro);
			}
				
		} catch(Exception e) {
			System.out.println("InformacionTOICBean::getResumenCargaFFON(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("InformacionTOICBean::getResumenCargaFFON(S)");
		}	
		return resultados;
	}
	
	/**
	 * Muestra el detalle de la carga si es que se quieren mostrar los encontrados en nae,
	 * los no encontrados de FFON en nae y simplemente los registros cargados 
	 * de FFON a NAE sin tomar en cuenta coincidencias
	 * @throws com.netro.exception.NafinException
	 * @return Lista con los registros detallados de la carga de acuerdo al tipo que se requiera
	 * @param ic_carga Clave de la carga 
	 * @param tipo Tipo de registros que se van a mostrar, 'S' encontrados, 'N' no encontrados,
	 * 	'' Sin tomar en cuenta las coincidencias
	 */
	public List getDetalleCarga(String tipo, String ic_carga) throws NafinException{
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer 	qrySentencia	= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		List				resultados		= new ArrayList();
		System.out.println("InformacionTOICBean::getDetalleCarga(E)");
		try 
		{	
			con.conexionDB();
			
			qrySentencia.append(
				" SELECT ig_numero_docto NUMDOCTO_NAE, ig_numero_docto_ffon NUMDOCTO_FFON, to_char(df_valor, 'dd/mm/yyyy') FECHA_VENCIMIENTO, " + 
				"	fg_importe MONTO, cd_nombre MONEDA, cg_solicitante_pago USUARIO, cg_area_compradora AREACOMPRADORA, " +
				"	cg_rfc_beneficiario RFCBENEFICIARIO, cg_nombre_beneficiario BENEFICIARIO " +
				" FROM com_detalle_carga_ffon det, comcat_moneda mon " + 
				" WHERE det.cg_divisa = mon.cg_divisa(+) " + 
				" AND ic_carga_ffon = ?");
			lVarBind.add(new Integer(ic_carga));
			if(!"".equals(tipo) && tipo != null){
				qrySentencia.append(" AND cs_registrado_en_cadenas = ? ");
				lVarBind.add(tipo);
			}
			
			System.out.println("query:: " + qrySentencia);
			System.out.println("bind:: " + lVarBind);
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			
			while(registros != null && registros.next() ){
				HashMap registro = new HashMap();
				registro.put("NUMDOCTO_NAE",				registros.getString("NUMDOCTO_NAE"));
				registro.put("NUMDOCTO_FFON",				registros.getString("NUMDOCTO_FFON"));
				registro.put("FECHA_VENCIMIENTO",		registros.getString("FECHA_VENCIMIENTO"));
				registro.put("MONTO",						registros.getString("MONTO"));
				registro.put("MONEDA",						registros.getString("MONEDA"));
				registro.put("USUARIO",						registros.getString("USUARIO"));
				registro.put("AREACOMPRADORA",			registros.getString("AREACOMPRADORA"));
				registro.put("RFCBENEFICIARIO",			registros.getString("RFCBENEFICIARIO"));
				registro.put("BENEFICIARIO",				registros.getString("BENEFICIARIO"));
				
				resultados.add(registro);
			}
				
		} catch(Exception e) {
			System.out.println("InformacionTOICBean::getDetalleCarga(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("InformacionTOICBean::getDetalleCarga(S)");
		}	
		return resultados;
	}
	
	
	
	/**
	 * Este m�todo lee el archivo txt que contiene la informaci�n de los documentos SIAFF,
	 * carga los registros de manera temporal y realiza la validaci�n de la informaci�n mediante un 
	 * proceso almacenado. Este m�todo se emplea tanto en la carga manual como en la autom�tica
	 * por lo que de realizarse alguna modificaci�n debe tomerse en cuenta el alcance de la misma.
	 * @param numeroProceso - identificador del proceso realizado
	 * @param Ruta - ruta del archivo a cargar.
	 * @param tipo_carga Tipo de carga que se realiza: manual o autom�tica.
	 * @throws com.netro.exception.NafinException
	 */
	public void setCargaTmpMasivaSIAFF(String Ruta, int numeroProceso, String tipo_carga) throws NafinException {
		log.info("setCargaTmpMasivaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps	= null;
		
		VectorTokenizer vtd	= null;
		Vector vecdet = null;
		
		int contregistros	= 0;
		int numeroLinea = 1;
		boolean lbOK = true;
		//FODEA 034 - 2009 ACF (I)
		StringBuffer strSQL = new StringBuffer();
		String lineaf = "";
		
		String strRamoSiaff = "";
		String strUnidadSiaff = "";
		String strFolioCLC = "";
		String strFolioDependencia = "";
		String strFechaAlta = "";
		String strDigitoIdentificador	= "";
		String strRfcBeneficiario = "";
		String strNombreBeneficiario = "";
		String strClaveDivisa = "";
		String strOgto = "";
		String strFechaPago = "";
		String strEstatus = "";
		String strImporte = "";
		
		try{
			con.conexionDB();
			//SE OBTIENEN LOS VALORES DEL ARCHIVO DE TEXTO, Y SE INSERTAN EN LA TABLA COMTMP_DETALLE_CARGA_SIAFF PARA SU VALIDACI�N
			java.io.File   f	= new java.io.File(Ruta);
			BufferedReader br	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));
			while((lineaf=br.readLine()) != null){
				contregistros++;
				vtd = new VectorTokenizer(lineaf,"|");
				vecdet = vtd.getValuesVector();
				
				// Obtener los campos individuales de cada uno de los registros del archivo
				strRamoSiaff = (vecdet.size()>= 1)?vecdet.get(0).toString().trim():"";
				strUnidadSiaff = (vecdet.size()>= 2)?vecdet.get(1).toString().trim().toUpperCase():"";
				strFolioCLC = (vecdet.size()>= 3)?vecdet.get(2).toString().trim().toUpperCase():"";
				strFolioDependencia = (vecdet.size()>= 4)?vecdet.get(3).toString().trim().toUpperCase():"";
				strFechaAlta = (vecdet.size()>= 5)?vecdet.get(4).toString().trim().toUpperCase():"";
				strDigitoIdentificador = (vecdet.size()>= 6)?vecdet.get(5).toString().trim().toUpperCase():"";
				strRfcBeneficiario = (vecdet.size()>= 7)?vecdet.get(6).toString().trim().toUpperCase():"";
				strNombreBeneficiario = (vecdet.size()>= 8)?vecdet.get(7).toString().trim().toUpperCase():"";
				strClaveDivisa = (vecdet.size()>= 9)?vecdet.get(8).toString().trim().toUpperCase():"";
				strOgto = (vecdet.size()>=10)?vecdet.get(9).toString().trim().toUpperCase():"";
				strFechaPago = (vecdet.size()>=11)?vecdet.get(10).toString().trim().toUpperCase():"";
				strEstatus = (vecdet.size()>= 12)?vecdet.get(11).toString().trim():"";
				strImporte = (vecdet.size()>= 13)?vecdet.get(12).toString().trim().toUpperCase():"";
				
				// Quitar comillas simples y dobles
				strRamoSiaff = Comunes.quitaComitasSimplesyDobles(strRamoSiaff);
				strUnidadSiaff = Comunes.quitaComitasSimplesyDobles(strUnidadSiaff);
				strFolioCLC = Comunes.quitaComitasSimplesyDobles(strFolioCLC);
				strFolioDependencia = Comunes.quitaComitasSimplesyDobles(strFolioDependencia);
				strFechaAlta = Comunes.quitaComitasSimplesyDobles(strFechaAlta);
				strDigitoIdentificador = Comunes.quitaComitasSimplesyDobles(strDigitoIdentificador);
				strRfcBeneficiario = Comunes.quitaComitasSimplesyDobles(strRfcBeneficiario);
				strNombreBeneficiario = Comunes.quitaComitasSimplesyDobles(strNombreBeneficiario);
				strClaveDivisa = Comunes.quitaComitasSimplesyDobles(strClaveDivisa);
				strOgto = Comunes.quitaComitasSimplesyDobles(strOgto);
				strFechaPago = Comunes.quitaComitasSimplesyDobles(strFechaPago);
				strEstatus = Comunes.quitaComitasSimplesyDobles(strEstatus);
				strImporte = Comunes.quitaComitasSimplesyDobles(strImporte);
				
				// Recortar los caracteres de los campos en caso que excedan el tama�o permitido
				strRamoSiaff = ((strRamoSiaff.length() > 2)?strRamoSiaff.substring(0, 3):strRamoSiaff);
				strUnidadSiaff = ((strUnidadSiaff.length() > 3)?strUnidadSiaff.substring(0, 4):strUnidadSiaff);
				strFolioCLC = ((strFolioCLC.length() > 8)?strFolioCLC.substring(0, 9):strFolioCLC);
				strFolioDependencia = ((strFolioDependencia.length() > 15)?strFolioDependencia.substring(0, 16):strFolioDependencia);
				strFechaAlta = ((strFechaAlta.length() > 10)?strFechaAlta.substring(0, 11):strFechaAlta);
				strDigitoIdentificador = ((strDigitoIdentificador.length() > 15)?strDigitoIdentificador.substring(0, 16):strDigitoIdentificador);
				strRfcBeneficiario = ((strRfcBeneficiario.length()> 13)?strRfcBeneficiario.substring(0, 14):strRfcBeneficiario);
				strNombreBeneficiario = ((strNombreBeneficiario.length() > 120)?strNombreBeneficiario.substring(0, 121):strNombreBeneficiario);
				strClaveDivisa = ((strClaveDivisa.length() > 3)?strClaveDivisa.substring(0, 4):strClaveDivisa);
				strOgto = ((strOgto.length() > 4)?strOgto.substring(0, 5):strOgto);
				strFechaPago = ((strFechaPago.length() > 10)?strFechaPago.substring(0, 11):strFechaPago);
				strEstatus = ((strEstatus.length() > 15)?strEstatus.substring(0, 16):strEstatus);
				strImporte = ((strImporte.length() > 21)?strImporte.substring(0, 21):strImporte);
				
				// Eliminacion de acentos en datos, si es que estos existen
				strRamoSiaff = Comunes.eliminaAcentos(strRamoSiaff);
				strUnidadSiaff = Comunes.eliminaAcentos(strUnidadSiaff);
				strFolioCLC = Comunes.eliminaAcentos(strFolioCLC);
				strFolioDependencia = Comunes.eliminaAcentos(strFolioDependencia);
				strFechaAlta = Comunes.eliminaAcentos(strFechaAlta);
				strDigitoIdentificador = Comunes.eliminaAcentos(strDigitoIdentificador);
				strRfcBeneficiario = Comunes.eliminaAcentos(strRfcBeneficiario);
				strNombreBeneficiario = Comunes.eliminaAcentos(strNombreBeneficiario);
				strClaveDivisa = Comunes.eliminaAcentos(strClaveDivisa);
				strOgto = Comunes.eliminaAcentos(strOgto);
				strFechaPago = Comunes.eliminaAcentos(strFechaPago);
				strEstatus = Comunes.eliminaAcentos(strEstatus);
				strImporte = Comunes.eliminaAcentos(strImporte);

				strSQL = new StringBuffer();
				
				// Insertar los datos en la tabla temporal COMTMP_DETALLE_CARGA_SIAFF
				strSQL.append("INSERT INTO COMTMP_DETALLE_CARGA_SIAFF (");
				strSQL.append(" IG_NUM_PROCESO,");
				strSQL.append(" CC_RAMO_SIAFF,");
				strSQL.append(" CC_UNIDAD_SIAFF,");
				strSQL.append(" CG_FOLIO_CLC,");
				strSQL.append(" CG_FOLIO_DEPENDENCIA,");
				strSQL.append(" DF_ALTA_CLC,");
				strSQL.append(" IG_DIGITO_IDENTIFICADOR,");
				strSQL.append(" CG_RFC_BENEFICIARIO,");
				strSQL.append(" CG_NOMBRE_BENEFICIARIO,");
				strSQL.append(" CG_DIVISA,");
				strSQL.append(" IG_OGTO,");
				strSQL.append(" DF_PAGO,");
				strSQL.append(" CG_ESTATUS,");
				strSQL.append(" FG_IMPORTE,");
				strSQL.append(" CG_TIPO_CARGA,");
				strSQL.append(" IG_LINEA_REG");
				strSQL.append(") VALUES (");
				strSQL.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?");
				strSQL.append(")");
				log.debug("..:: strSQL : "+strSQL.toString());
				ps = con.queryPrecompilado(strSQL.toString());
				ps.clearParameters();
				ps.setInt(   1, numeroProceso         );
				ps.setString(2, strRamoSiaff          );
				ps.setString(3, strUnidadSiaff        );
				ps.setString(4, strFolioCLC           );
				ps.setString(5, strFolioDependencia   );
				ps.setString(6, strFechaAlta          );
				ps.setString(7, strDigitoIdentificador);
				ps.setString(8, strRfcBeneficiario    );
				ps.setString(9, strNombreBeneficiario );
				ps.setString(10, strClaveDivisa       );
				ps.setString(11, strOgto              );
				ps.setString(12, strFechaPago         );
				ps.setString(13, strEstatus           );
				ps.setString(14, strImporte           );
				ps.setString(15, tipo_carga           );
				ps.setInt(   16, contregistros        );

				ps.executeUpdate();
				ps.close();

				numeroLinea++;

				//Realizar un COMMIT cada 100 registros
				if (numeroLinea % 100 == 0){con.terminaTransaccion(true);}
			}

			con.terminaTransaccion(true);

			if(contregistros > 0){
				CallableStatement sp = null;
				try{
					log.debug("..:: INICIA PROCESO DE CARGA NUMERO : "+numeroProceso+" ::..");
					sp = con.ejecutaSP("SP_CARGA_SIAFF("+numeroProceso+")");
					sp.execute();
					sp.close();
					log.debug("..:: PROCESO DE CARGA FINALIZADO ::..");
				}catch(Exception e){
					e.printStackTrace();
					throw new NafinException("Error en proceso de carga");
				}
			}
		//FODEA 034 - 2009 ACF (F)
		}catch (IOException ioe){
			lbOK = false;
			ioe.printStackTrace();
			throw new NafinException("SIST0001");
		}catch (Exception e){
			lbOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("setCargaTmpMasivaSIAFF(F)");
		}
	}

	/**
	* Metodo que indica si existe en la tabla de nafin electronico un reporte con fecha DF_MES_REPORTE = fechaInicioMensual
	* eso quiere decir que ya hay reportes en la tabla de nafin electronico
	* @param fechaInicioMensual Cadena con la fecha en formato dd/mm/YYYY
	* @param sEpo cadena con el IC_epo
	* 
	* @return boolean bandera que indica si existen 3 reportes para esa epo en esa fecha, si no, se volveran a consultar los 3 e insertaran
	* @throws com.netro.exception.NafinException 
	* @deprecated Fodea 040 - 2011
	**/
	public boolean getExisteReporte(String fechaInicioMensual, String sEpo) throws NafinException {
		
		System.out.println("getExisteReporte(E)");
		
		boolean 					resultado = false;
		AccesoDB 					con				= new AccesoDB();
		PreparedStatement ps				= null;
		ResultSet 				rs 				= null;
		
		try{
			con.conexionDB();
			
			String Query = "SELECT COUNT(1) numregs " + 
			               "FROM COMBIT_OPER_EPOS " + 
										 "WHERE DF_MES_REPORTE = TO_DATE(?,'DD/MM/YYYY') " + 
										 "and DF_ALTA >= ADD_MONTHS(to_date(?,'dd/mm/yyyy'),1) " +
										 "and ic_epo = ?";
										 
			ps = con.queryPrecompilado(Query);
			ps.setString(1,fechaInicioMensual);
			ps.setString(2,fechaInicioMensual);
			ps.setInt(3,Integer.parseInt(sEpo));
			
			System.out.println("query : "+Query);
			System.out.println("Bind1 : "+fechaInicioMensual);
			System.out.println("Bind2 : "+fechaInicioMensual);
			System.out.println("Bind3 : "+sEpo);
			
			rs = ps.executeQuery();
			
			if(rs.next()){
			
				int regs = Integer.parseInt(rs.getString("numregs"));
				
				System.out.println("regs : " + regs);
				
				if(regs==9){				
					resultado = true;			
				}
			}
		
		
      rs.close();
			ps.close();			
		
			return resultado;
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getExisteReporte(S)");
		}
	}

	/**
	 * Metodo que indica si existe en la tabla (COMBIT_OPER_EPOS) de nafin electronico un reporte con fecha DF_ALTA = fechaActual
	 * sirve para no realizar consultas a promo epo cuando ya tenga el reporte del dia actual
	 * @param fechaActual Cadena con la fecha en formato dd/mm/YYYY
	 * @param sEpo cadena con el IC_epo
	 * 
	 * @return boolean que indica si existe reportes en ese dia y para esa epo
	 * @throws com.netro.exception.NafinException
	 * @deprecated Fodea 040 - 2011
	 */
	public boolean getExisteReporteDia(String fechaActual, String sEpo) throws NafinException {
		boolean 					resultado = false;
		System.out.println("getExisteReporteDia(E)");
		AccesoDB 					con				= new AccesoDB();
		PreparedStatement ps				= null;
		ResultSet 				rs 				= null;
		
		try{
			con.conexionDB();
			
			String Query = "SELECT COUNT(1) numregs FROM COMBIT_OPER_EPOS WHERE to_char(df_alta, 'dd/mm/yyyy') = ? and ic_epo = ?";
						
			ps = con.queryPrecompilado(Query);
			ps.setString(1,fechaActual);
			ps.setInt(2,Integer.parseInt(sEpo));
			
			System.out.println("query : "+Query);
			System.out.println("Bind1 : "+fechaActual);
			System.out.println("Bind2 : "+sEpo);			
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				
				int regs = Integer.parseInt(rs.getString("numregs"));
				
				System.out.println("regs : " + regs);
				
				if(regs==9){				
					resultado = true;			
				}
			}

      rs.close();
			ps.close();		
	
			return resultado;
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getExisteReporteDia(S)");
		}		
		
	}

	/***
	 * 
	 * Metodo que consulta los datos en nafin electronico traidos de promoepo, son reportes que se consultaron anteriormente y ya
	 * se guardaron en N@E
	 * 
	 * @param fechaInicioMensual Cadena con la fecha en formato dd/mm/yyyy
	 * @param sEpo Cadena con el numero de Epo del reporte
	 * @param cadena con el tipo de Consulta donde M - Mensual, A - Anual, C - Acumulado
	 * 
	 * @return HashMap con todos los datos de el reporte en esa fecha
	 * @throws com.netro.exception.NafinException
	 * @deprecated Fodea 040 - 2011
	 **/ 
	public HashMap getDatosCtaPdfNAE( String fechaInicioMensual, String sEpo, String tipoConsulta ) throws NafinException {
		
		System.out.println("getDatosCtaPdfNAE(E)");
		
		HashMap map 				= new HashMap();			
		AccesoDB 					con	= new AccesoDB();
		PreparedStatement 		ps		= null;
		ResultSet 					rs 	= null;
		
		try{
			con.conexionDB();
			
			String Query = "SELECT epo.cg_razon_social EPO, oper.ic_tipo_organismo pef2, oper.ic_secretaria pef1, oper.ig_prov_reg PROV_REG, oper.ig_prov_con_pub PROV_CON_PUB, " +
										"       oper.ig_num_doctos DOCUMENTOS, oper.fn_monto_publicado MONTO_TOTAL, oper.ig_dias_diferencia_pub PLAZO_PROM_PUBL, " +
										"       oper.ig_dias_diferencia PLAZO_PROM_PAGO, oper.ig_doctos_susceptible DOCS_SUSEP_FACTORAJE, oper.fn_monto_susceptible MONTO_SUCEP, " +
										"       oper.ig_docs_operados DOCS_OPERADOS, oper.fn_monto_operado MONTO_OPERADO, oper.fn_monto_neto FN_MONTO_NETO, oper.fn_monto_interes FN_MONTO_INTERES        " +
										"  FROM combit_oper_epos oper, comcat_epo epo " +
										" WHERE epo.ic_epo = oper.ic_epo " +
										"     AND oper.ic_epo = ?  " +
										"     AND oper.df_mes_reporte = TO_DATE(?,'DD/MM/YYYY')  " +
										"     AND oper.ic_periodo = ? " +
										"     AND oper.cc_columna = ? ";
						
			ps = con.queryPrecompilado(Query);
			
			// 1. LEER VALORES COLUMNA "MONEDA NACIONAL"
			
			ps.clearParameters();
			ps.setInt(1,Integer.parseInt(sEpo));
			ps.setString(2,fechaInicioMensual);
			ps.setString(3,tipoConsulta);
			ps.setString(4,"MXN");
			
			System.out.println("query(mxn) : "+Query);
			System.out.println("Bind1(mxn) : "+sEpo);
			System.out.println("Bind2(mxn) : "+fechaInicioMensual);			
			System.out.println("Bind3(mxn) : "+tipoConsulta);			
			System.out.println("Bind4(mxn) : "+"MXN");	
			
			rs = ps.executeQuery();
			
			if(rs.next()){

				map.put("DependenciaEntidad",				rs.getString("EPO"));
				map.put("Secretaria",						rs.getString("pef1"));
				map.put("Tipo",								rs.getString("pef2"));
				
				// 1. Proveedores
				map.put("ProveedoresRegistrados_MXN",			rs.getString("PROV_REG"));
				map.put("ProveedoresCXPRegistradas_MXN",		rs.getString("PROV_CON_PUB"));
				
				// 2. Documentos Registrados 
				map.put("Documentos_MXN",							rs.getString("DOCUMENTOS"));
				map.put("MontoTotal_MXN",							rs.getString("MONTO_TOTAL"));
				map.put("PlazoPromedioRegistroCXP_MXN",		Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PUBL")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PUBL"))),0));
				map.put("PlazoPromedioPago_MXN",					Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PAGO")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PAGO"))),0));										
				map.put("DocumentosSuscepFactoraje_MXN",		rs.getString("DOCS_SUSEP_FACTORAJE"));
				map.put("MontoSusceptibleFactoraje_MXN",		rs.getString("MONTO_SUCEP"));
				
				// 3. Factoraje
				map.put("DocumentosOperadosFactoraje_MXN",	rs.getString("DOCS_OPERADOS"));
				map.put("MontoTotalOperado_MXN",					rs.getString("FN_MONTO_NETO"));
				map.put("InteresesGeneradosFactoraje_MXN",	rs.getString("FN_MONTO_INTERES"));
				map.put("MontoOperadoFactoraje_MXN",			rs.getString("MONTO_OPERADO"));

				// 4. Estadisticas
				// Total Proveedores con CXP registradas / Total Proveedores registrados
				double dProveedoresCXPRegistradas 	= (rs.getString("PROV_CON_PUB")==null) ?0:Double.parseDouble(rs.getString("PROV_CON_PUB"));
				double dProveedoresRegistrados 		= (rs.getString("PROV_REG")==null)		?0:Double.parseDouble(rs.getString("PROV_REG"));
				if( (dProveedoresCXPRegistradas == 0 && dProveedoresRegistrados==0) || dProveedoresRegistrados==0 ){
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_MXN","0");
				}else{
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_MXN",""+(dProveedoresCXPRegistradas/dProveedoresRegistrados));
				}

				// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
				double dDocumentosSuscepFactoraje   = (rs.getString("DOCS_SUSEP_FACTORAJE")==null)?0:Double.parseDouble(rs.getString("DOCS_SUSEP_FACTORAJE"));
				double dDocumentosOperadosFactoraje = (rs.getString("DOCS_OPERADOS")==null)		 ?0:Double.parseDouble(rs.getString("DOCS_OPERADOS"));
				if( (dDocumentosSuscepFactoraje==0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0 ){
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_MXN","0");
				}else{
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_MXN",""+(dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje));
				}

				// Monto Operado en Factoraje / Monto Susceptible de Factoraje
				double dMontoOperadoFactoraje   		= (rs.getString("MONTO_OPERADO")==null)	?0:Double.parseDouble(rs.getString("MONTO_OPERADO"));
				double dMontoSusceptibleFactoraje 	= (rs.getString("MONTO_SUCEP")==null)		?0:Double.parseDouble(rs.getString("MONTO_SUCEP"));										
				if( (dMontoOperadoFactoraje==0 && dMontoSusceptibleFactoraje==0) || dMontoSusceptibleFactoraje==0 ){
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_MXN","0");
				}else{
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_MXN", ""+(dMontoOperadoFactoraje / dMontoSusceptibleFactoraje));
				}

			} //if rs.next
		
			rs.close();
 
			// 2. Leer Valores Columna "Dolar Americano"
			ps.clearParameters();
			ps.setInt(1,Integer.parseInt(sEpo));
			ps.setString(2,fechaInicioMensual);
			ps.setString(3,tipoConsulta);
			ps.setString(4,"DL");
			
			System.out.println("query : "+Query);
			System.out.println("Bind1 : "+sEpo);
			System.out.println("Bind2 : "+fechaInicioMensual);			
			System.out.println("Bind3 : "+tipoConsulta);			
			System.out.println("Bind4(dl) : "+"DL");
			
			rs = ps.executeQuery();
			
			if(rs.next()){

				map.put("DependenciaEntidad",			rs.getString("EPO"));
				map.put("Secretaria",					rs.getString("pef1"));
				map.put("Tipo",							rs.getString("pef2"));
				
				// 1. Proveedores
				map.put("ProveedoresRegistrados_DL",		rs.getString("PROV_REG"));
				map.put("ProveedoresCXPRegistradas_DL",	rs.getString("PROV_CON_PUB"));
				
				// 2. Documentos Registrados 
				map.put("Documentos_DL",						rs.getString("DOCUMENTOS"));
				map.put("MontoTotal_DL",						rs.getString("MONTO_TOTAL"));
				map.put("PlazoPromedioRegistroCXP_DL",		Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PUBL")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PUBL"))),0));
				map.put("PlazoPromedioPago_DL",				Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PAGO")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PAGO"))),0));										
				map.put("DocumentosSuscepFactoraje_DL",	rs.getString("DOCS_SUSEP_FACTORAJE"));
				map.put("MontoSusceptibleFactoraje_DL",	rs.getString("MONTO_SUCEP"));
				
				// 3. Factoraje
				map.put("DocumentosOperadosFactoraje_DL",	rs.getString("DOCS_OPERADOS"));
				map.put("MontoTotalOperado_DL",				rs.getString("FN_MONTO_NETO"));
				map.put("InteresesGeneradosFactoraje_DL",	rs.getString("FN_MONTO_INTERES"));
				map.put("MontoOperadoFactoraje_DL",			rs.getString("MONTO_OPERADO"));

				// 4. Estadisticas
				// Total Proveedores con CXP registradas / Total Proveedores registrados
				double dProveedoresCXPRegistradas = (rs.getString("PROV_CON_PUB")==null)?0:Double.parseDouble(rs.getString("PROV_CON_PUB"));
				double dProveedoresRegistrados 	 = (rs.getString("PROV_REG")    ==null)?0:Double.parseDouble(rs.getString("PROV_REG"));
				if( (dProveedoresCXPRegistradas==0 && dProveedoresRegistrados==0) || dProveedoresRegistrados==0 ){
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_DL","0");
				}else{
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados_DL",""+(dProveedoresCXPRegistradas/dProveedoresRegistrados));
				}

				// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
				double dDocumentosSuscepFactoraje   = (rs.getString("DOCS_SUSEP_FACTORAJE")==null)?0:Double.parseDouble(rs.getString("DOCS_SUSEP_FACTORAJE"));
				double dDocumentosOperadosFactoraje = (rs.getString("DOCS_OPERADOS")==null)		 ?0:Double.parseDouble(rs.getString("DOCS_OPERADOS"));
				if( (dDocumentosSuscepFactoraje==0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0 ){
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_DL","0");
				}else{
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje_DL",""+(dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje));
				}

				// Monto Operado en Factoraje / Monto Susceptible de Factoraje
				double dMontoOperadoFactoraje   		= (rs.getString("MONTO_OPERADO")==null)	?0:Double.parseDouble(rs.getString("MONTO_OPERADO"));
				double dMontoSusceptibleFactoraje 	= (rs.getString("MONTO_SUCEP")  ==null)   ?0:Double.parseDouble(rs.getString("MONTO_SUCEP"));										
				if( (dMontoOperadoFactoraje==0 && dMontoSusceptibleFactoraje==0) || dMontoSusceptibleFactoraje==0 ){
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_DL","0");
				}else{
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje_DL", ""+(dMontoOperadoFactoraje / dMontoSusceptibleFactoraje));
				}

			} //if rs.next
		
			rs.close();
			
			// 3. Leer Valores Columna "Totales"
			ps.clearParameters();
			ps.setInt(1,Integer.parseInt(sEpo));
			ps.setString(2,fechaInicioMensual);
			ps.setString(3,tipoConsulta);
			ps.setString(4,"TOT");
			
			System.out.println("query : "+Query);
			System.out.println("Bind1 : "+sEpo);
			System.out.println("Bind2 : "+fechaInicioMensual);			
			System.out.println("Bind3 : "+tipoConsulta);			
			System.out.println("Bind4(tot) : "+"TOT");
			
			rs = ps.executeQuery();
			
			if(rs.next()){

				map.put("DependenciaEntidad",					rs.getString("EPO"));
				map.put("Secretaria",							rs.getString("pef1"));
				map.put("Tipo",									rs.getString("pef2"));
				
				// 1. Proveedores
				map.put("ProveedoresRegistrados",			rs.getString("PROV_REG"));
				map.put("ProveedoresCXPRegistradas",		rs.getString("PROV_CON_PUB"));
				
				// 2. Documentos Registrados 
				map.put("Documentos",							rs.getString("DOCUMENTOS"));
				map.put("MontoTotal",							rs.getString("MONTO_TOTAL"));
				map.put("PlazoPromedioRegistroCXP",			Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PUBL")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PUBL"))),0));
				map.put("PlazoPromedioPago",					Comunes.formatoDecimal(Math.round((rs.getString("PLAZO_PROM_PAGO")==null)?0:Double.parseDouble(rs.getString("PLAZO_PROM_PAGO"))),0));										
				map.put("DocumentosSuscepFactoraje",		rs.getString("DOCS_SUSEP_FACTORAJE"));
				map.put("MontoSusceptibleFactoraje",		rs.getString("MONTO_SUCEP"));
				
				// 3. Factoraje
				map.put("DocumentosOperadosFactoraje",		rs.getString("DOCS_OPERADOS"));
				map.put("MontoTotalOperado",					rs.getString("FN_MONTO_NETO"));
				map.put("InteresesGeneradosFactoraje",		rs.getString("FN_MONTO_INTERES"));
				map.put("MontoOperadoFactoraje",				rs.getString("MONTO_OPERADO"));

				// 4. Estadisticas
				// Total Proveedores con CXP registradas / Total Proveedores registrados
				double dProveedoresCXPRegistradas = (rs.getString("PROV_CON_PUB")==null)?0:Double.parseDouble(rs.getString("PROV_CON_PUB"));
				double dProveedoresRegistrados 		= (rs.getString("PROV_REG")==null)		?0:Double.parseDouble(rs.getString("PROV_REG"));
				if( (dProveedoresCXPRegistradas==0 && dProveedoresRegistrados==0) || dProveedoresRegistrados==0 ){
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados","0");
				}else{
					map.put("TProveedoresCXPRegistradas_proveedoresRegistrados",""+(dProveedoresCXPRegistradas/dProveedoresRegistrados));
				}

				// Total Documentos Operados / Total Documentos Susceptibles de Factoraje
				double dDocumentosSuscepFactoraje   = (rs.getString("DOCS_SUSEP_FACTORAJE")==null)?0:Double.parseDouble(rs.getString("DOCS_SUSEP_FACTORAJE"));
				double dDocumentosOperadosFactoraje = (rs.getString("DOCS_OPERADOS")==null)				?0:Double.parseDouble(rs.getString("DOCS_OPERADOS"));
				if( (dDocumentosSuscepFactoraje==0 && dDocumentosOperadosFactoraje==0) || dDocumentosSuscepFactoraje==0 ){
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje","0");
				}else{
					map.put("TDocumentosOperadosFactoraje_documentosSuscepFactoraje",""+(dDocumentosOperadosFactoraje/dDocumentosSuscepFactoraje));
				}

				// Monto Operado en Factoraje / Monto Susceptible de Factoraje
				double dMontoOperadoFactoraje   		= (rs.getString("MONTO_OPERADO")==null)	?0:Double.parseDouble(rs.getString("MONTO_OPERADO"));
				double dMontoSusceptibleFactoraje 	= (rs.getString("MONTO_SUCEP")==null)		?0:Double.parseDouble(rs.getString("MONTO_SUCEP"));										
				if( (dMontoOperadoFactoraje==0 && dMontoSusceptibleFactoraje==0) || dMontoSusceptibleFactoraje==0 ){
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje","0");
				}else{
					map.put("TMontoOperadoFactoraje_montoSusceptibleFactoraje", ""+(dMontoOperadoFactoraje / dMontoSusceptibleFactoraje));
				}

			} //if rs.next
		
			rs.close();
			ps.close();	
			
			return map;
			
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			
			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getDatosCtaPdfNAE(S)");
			
		}				

	}
 
	/**
	 * Metodo que guarda los datos de 3 HashMaps a N@E, guarda 3 reportes traidos de promo Epo con DF_ALTA = Sysdate
	 * y DF_MES_REPORTE=fechaInicioMensual
	 * 
	 * @param ArDatosPDFMen HashMap con los datos del reporte mensual
	 * @param ArDatosPDFAnu HashMap con los datos del reporte Anual
	 * @param ArDatosPDFAcu HashMap con los datos del reporte Acumulado (desde el 01/01/2000 a la fecha limite)
	 * @param fechaInicioMensual Fecha del reporte en realidad se toma en cuenta el mes y a�o.
	 * @param sEpo Cadena con el numero de Epo del reporte 
	 * @param flag boolean que indica si se tienen que borrar los reportes anteriores
	 * ESPECIAL CUIDADO EN ESTA BANDERA QUE INDICA EL BORRADO DE LOS REPORTES DE ESA EPO y PARA ESA FECHA
	 * @throws com.netro.exception.NafinException
	 * @deprecated Fodea 040 - 2011
	 * 
	 */
	public void setDatosCtaPdf( HashMap ArDatosPDFMen, HashMap ArDatosPDFAnu, HashMap ArDatosPDFAcu, String fechaInicioMensual, String sEpo, boolean flag ) throws NafinException {
		
		System.out.println("setDatosCtaPdf(E)");
		AccesoDB 					con				= new AccesoDB();
		PreparedStatement ps				= null;
		boolean           rOK 			= true;
		try{
			con.conexionDB();
			
			if(flag){
			//existe la bandera que indica que se van a borrar los reportes de esa fecha y para esa Epo
				String Query = "DELETE COMBIT_OPER_EPOS WHERE ic_epo = ? and DF_MES_REPORTE = TO_DATE(?,'DD/MM/YYYY') ";
				ps = con.queryPrecompilado(Query);
				
				ps.setInt(1,Integer.parseInt(sEpo));
				ps.setString(2,fechaInicioMensual);
				System.out.println("query : "+Query);
				System.out.println("Bind1 : "+fechaInicioMensual);
				System.out.println("Bind2 : "+sEpo);
				
				ps.executeUpdate();
				ps.close();
			}
			
			//Mensual
			hssetDatosCtaPDF(ArDatosPDFMen,fechaInicioMensual,sEpo,"M",con);
			//Anual
			hssetDatosCtaPDF(ArDatosPDFAnu,fechaInicioMensual,sEpo,"A",con);
			//Acumulado
			hssetDatosCtaPDF(ArDatosPDFAcu,fechaInicioMensual,sEpo,"C",con);	
			
		}catch (Exception e){
			rOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			con.terminaTransaccion(rOK);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("setDatosCtaPdf(S)");
		}		

	}	
	
	/**
	 * Metodo hssetDatosCtaPDF se encarga de guardar en la base el reporte que se manda como un HashMap asociado a une epo y en una fecha particular
	 * Se debe de hacer 3 veces por cada epo en cada mes.
	 * 
	 * @param HashMap con todos los datos del reporte
	 * @param fechaInicioMensual Cadena con la fecha en formato dd/mm/yyyy en la cual es el punto de partida para ese reporte
	 * @param sEpo Epo asociada a los reportes
	 * @con AccesoDB es una instancia de la conexion, esto para manejar el mismo commit desde el metodo que llama a este metodo :D
	 * @throws com.netro.exception.NafinException
	 * @deprecated Fodea 040 - 2011
	 * 
	 */
	private void hssetDatosCtaPDF(HashMap ArDatos, String fechaInicioMensual, String sEpo, String tipoPeriodo,  AccesoDB con)throws NafinException {
	
		System.out.println("hssetDatosCtaPDF(E)");
		PreparedStatement ps = null;
	
		try{
			String Query = "INSERT INTO COMBIT_OPER_EPOS " +
										 " (IC_EPO, IC_TIPO_ORGANISMO, IC_SECRETARIA, IG_PROV_REG, IG_PROV_CON_PUB, " +
										 "  IG_NUM_DOCTOS, FN_MONTO_PUBLICADO, IG_DIAS_DIFERENCIA_PUB, IG_DIAS_DIFERENCIA, IG_DOCTOS_SUSCEPTIBLE, " + 
										 "  FN_MONTO_SUSCEPTIBLE, IG_DOCS_OPERADOS, FN_MONTO_OPERADO, FN_MONTO_NETO, FN_MONTO_INTERES, " + 
										 "  IC_PERIODO, DF_MES_REPORTE, DF_ALTA, CC_COLUMNA ) " +
										 " VALUES ( " +
										 "?,?,?,?,?, " + 
										 "?,?,?,?,?,  " + 
										 "?,?,?,?,?,  " +
										 "?,TO_DATE(?,'DD/MM/YYYY'),SYSDATE, ? )";
										 
			ps = con.queryPrecompilado(Query);

			System.out.println("query : "+Query);
			String columna = null;
			
			// 1. Insertar registros correspondientes a la Columna "Moneda Nacional"
			columna = "MXN";
			
			System.out.println("1 : "+sEpo);
			System.out.println("2 : "+(String)ArDatos.get("Tipo"));
			System.out.println("3 : "+(String)ArDatos.get("Secretaria"));
			
			System.out.println("4 : "+(String)ArDatos.get("ProveedoresRegistrados_MXN"));
			System.out.println("5 : "+(String)ArDatos.get("ProveedoresCXPRegistradas_MXN"));

			System.out.println("6 : "+(String)ArDatos.get("Documentos_MXN"));
			System.out.println("7 : "+(String)ArDatos.get("MontoTotal_MXN"));
			System.out.println("8 : "+(String)ArDatos.get("PlazoPromedioRegistroCXP_MXN"));
			System.out.println("9 : "+(String)ArDatos.get("PlazoPromedioPago_MXN"));
			System.out.println("10: "+(String)ArDatos.get("DocumentosSuscepFactoraje_MXN"));

			System.out.println("11 : "+(String)ArDatos.get("MontoSusceptibleFactoraje_MXN"));
			System.out.println("12 : "+(String)ArDatos.get("DocumentosOperadosFactoraje_MXN"));
			System.out.println("13 : "+(String)ArDatos.get("MontoOperadoFactoraje_MXN"));
			System.out.println("14 : "+(String)ArDatos.get("MontoTotalOperado_MXN"));
			System.out.println("15 : "+(String)ArDatos.get("InteresesGeneradosFactoraje_MXN"));

			System.out.println("16 : "+tipoPeriodo);
			System.out.println("17 : "+fechaInicioMensual);
			System.out.println("18 : "+columna);

			ps.clearParameters();
			
			ps.setInt(1 ,		Integer.parseInt(sEpo));
			ps.setString(2,	(String)ArDatos.get("Tipo"));
			ps.setString(3,	(String)ArDatos.get("Secretaria"));
			
			ps.setInt(4,		Integer.parseInt((String)ArDatos.get("ProveedoresRegistrados_MXN")));
			ps.setInt(5,		Integer.parseInt((String)ArDatos.get("ProveedoresCXPRegistradas_MXN")));

			ps.setInt(6,		Integer.parseInt((String)ArDatos.get("Documentos_MXN")));
			ps.setDouble(7,	Double.parseDouble((String)ArDatos.get("MontoTotal_MXN")));
			ps.setInt(8,		Integer.parseInt((String)ArDatos.get("PlazoPromedioRegistroCXP_MXN")));
			ps.setInt(9,		Integer.parseInt((String)ArDatos.get("PlazoPromedioPago_MXN")));
			ps.setInt(10,		Integer.parseInt((String)ArDatos.get("DocumentosSuscepFactoraje_MXN")));
			ps.setDouble(11,	Double.parseDouble((String)ArDatos.get("MontoSusceptibleFactoraje_MXN")));
			
			ps.setInt(12,		Integer.parseInt((String)ArDatos.get("DocumentosOperadosFactoraje_MXN")));
			ps.setDouble(13,	Double.parseDouble((String)ArDatos.get("MontoOperadoFactoraje_MXN")));
		
			ps.setDouble(14,	Double.parseDouble((String)ArDatos.get("MontoTotalOperado_MXN")));
			ps.setDouble(15,	Double.parseDouble((String)ArDatos.get("InteresesGeneradosFactoraje_MXN")));
		
			ps.setString(16,	tipoPeriodo);
			ps.setString(17,	fechaInicioMensual);
			ps.setString(18,  columna);
											
			ps.executeUpdate();
			
			// 2. Insertar Registros correspondientes a la columna "Dolares Americanos"
			columna = "DL";
			
			System.out.println("1 : "+sEpo);
			System.out.println("2 : "+(String)ArDatos.get("Tipo"));
			System.out.println("3 : "+(String)ArDatos.get("Secretaria"));
			
			System.out.println("4 : "+(String)ArDatos.get("ProveedoresRegistrados_DL"));
			System.out.println("5 : "+(String)ArDatos.get("ProveedoresCXPRegistradas_DL"));

			System.out.println("6 : "+(String)ArDatos.get("Documentos_DL"));
			System.out.println("7 : "+(String)ArDatos.get("MontoTotal_DL"));
			System.out.println("8 : "+(String)ArDatos.get("PlazoPromedioRegistroCXP_DL"));
			System.out.println("9 : "+(String)ArDatos.get("PlazoPromedioPago_DL"));
			System.out.println("10: "+(String)ArDatos.get("DocumentosSuscepFactoraje_DL"));

			System.out.println("11 : "+(String)ArDatos.get("MontoSusceptibleFactoraje_DL"));
			System.out.println("12 : "+(String)ArDatos.get("DocumentosOperadosFactoraje_DL"));
			System.out.println("13 : "+(String)ArDatos.get("MontoOperadoFactoraje_DL"));
			System.out.println("14 : "+(String)ArDatos.get("MontoTotalOperado_DL"));
			System.out.println("15 : "+(String)ArDatos.get("InteresesGeneradosFactoraje_DL"));

			System.out.println("16 : "+tipoPeriodo);
			System.out.println("17 : "+fechaInicioMensual);
			System.out.println("18 : "+columna);

			ps.clearParameters();
			
			ps.setInt(1 ,		Integer.parseInt(sEpo));
			ps.setString(2,	(String)ArDatos.get("Tipo"));
			ps.setString(3,	(String)ArDatos.get("Secretaria"));
			
			ps.setInt(4,		Integer.parseInt((String)ArDatos.get("ProveedoresRegistrados_DL")));
			ps.setInt(5,		Integer.parseInt((String)ArDatos.get("ProveedoresCXPRegistradas_DL")));

			ps.setInt(6,		Integer.parseInt((String)ArDatos.get("Documentos_DL")));
			ps.setDouble(7,	Double.parseDouble((String)ArDatos.get("MontoTotal_DL")));
			ps.setInt(8,		Integer.parseInt((String)ArDatos.get("PlazoPromedioRegistroCXP_DL")));
			ps.setInt(9,		Integer.parseInt((String)ArDatos.get("PlazoPromedioPago_DL")));
			ps.setInt(10,		Integer.parseInt((String)ArDatos.get("DocumentosSuscepFactoraje_DL")));
		
			ps.setDouble(11,	Double.parseDouble((String)ArDatos.get("MontoSusceptibleFactoraje_DL")));
			ps.setInt(12,		Integer.parseInt((String)ArDatos.get("DocumentosOperadosFactoraje_DL")));
			ps.setDouble(13,	Double.parseDouble((String)ArDatos.get("MontoOperadoFactoraje_DL")));
			ps.setDouble(14,	Double.parseDouble((String)ArDatos.get("MontoTotalOperado_DL")));
			ps.setDouble(15,	Double.parseDouble((String)ArDatos.get("InteresesGeneradosFactoraje_DL")));
		
			ps.setString(16,	tipoPeriodo);
			ps.setString(17,	fechaInicioMensual);
			ps.setString(18,  columna);
			
			ps.executeUpdate();
			
			// 3. Insertar Registros correspondientes a la columna "Totales"
			columna = "TOT";
			
			System.out.println("1 : "+sEpo);
			System.out.println("2 : "+(String)ArDatos.get("Tipo"));
			System.out.println("3 : "+(String)ArDatos.get("Secretaria"));
			
			System.out.println("4 : "+(String)ArDatos.get("ProveedoresRegistrados"));
			System.out.println("5 : "+(String)ArDatos.get("ProveedoresCXPRegistradas"));

			System.out.println("6 : "+(String)ArDatos.get("Documentos"));
			System.out.println("7 : "+(String)ArDatos.get("MontoTotal"));
			System.out.println("8 : "+(String)ArDatos.get("PlazoPromedioRegistroCXP"));
			System.out.println("9 : "+(String)ArDatos.get("PlazoPromedioPago"));
			System.out.println("10: "+(String)ArDatos.get("DocumentosSuscepFactoraje"));

			System.out.println("11 : "+(String)ArDatos.get("MontoSusceptibleFactoraje"));
			System.out.println("12 : "+(String)ArDatos.get("DocumentosOperadosFactoraje"));
			System.out.println("13 : "+(String)ArDatos.get("MontoOperadoFactoraje"));
			System.out.println("14 : "+(String)ArDatos.get("MontoTotalOperado"));
			System.out.println("15 : "+(String)ArDatos.get("InteresesGeneradosFactoraje"));

			System.out.println("16 : "+tipoPeriodo);
			System.out.println("17 : "+fechaInicioMensual);
			System.out.println("18 : "+columna);
			
			ps.clearParameters();
			ps.setInt(1 ,		Integer.parseInt(sEpo));
			ps.setString(2,	(String)ArDatos.get("Tipo"));
			ps.setString(3,	(String)ArDatos.get("Secretaria"));
			ps.setInt(4,		Integer.parseInt((String)ArDatos.get("ProveedoresRegistrados")));
			ps.setInt(5,		Integer.parseInt((String)ArDatos.get("ProveedoresCXPRegistradas")));

			ps.setInt(6,		Integer.parseInt((String)ArDatos.get("Documentos")));
			ps.setDouble(7,	Double.parseDouble((String)ArDatos.get("MontoTotal")));
			ps.setInt(8,		Integer.parseInt((String)ArDatos.get("PlazoPromedioRegistroCXP")));
			ps.setInt(9,		Integer.parseInt((String)ArDatos.get("PlazoPromedioPago")));
			ps.setInt(10,		Integer.parseInt((String)ArDatos.get("DocumentosSuscepFactoraje")));
		
			ps.setDouble(11,	Double.parseDouble((String)ArDatos.get("MontoSusceptibleFactoraje")));
			ps.setInt(12,		Integer.parseInt((String)ArDatos.get("DocumentosOperadosFactoraje")));
			ps.setDouble(13,	Double.parseDouble((String)ArDatos.get("MontoOperadoFactoraje")));
			ps.setDouble(14,	Double.parseDouble((String)ArDatos.get("MontoTotalOperado")));
			ps.setDouble(15,	Double.parseDouble((String)ArDatos.get("InteresesGeneradosFactoraje")));
		
			ps.setString(16,	tipoPeriodo);
			ps.setString(17,	fechaInicioMensual);
			ps.setString(18,	columna);
			
			ps.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			System.out.println("hssetDatosCtaPDF(S)");
		}
 
	}	//hssetDatosCtaPDF

	/**
	 * Ya valido  el archivo de carga de SIAFF
	 * este metodo se encarga de realizar la
	 * carga total del archivo txt
	 * @throws com.netro.exception.NafinException
	 * @param nombreArchivo -  nombre del archivo a cargar en sistema
	 * @param claveUsuario - id del usuario que realizara la carga
	 * @param numeroProceso - num identificador del proceso que realizara la carga
	 */
	public void procesarCargaSIAFF(int numeroProceso, int numero_carga_siaff, String claveUsuario, String nombreArchivo) throws NafinException {
		log.info("procesarCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean lbOK = true;
		int ic_resSIAFF = 0;
		int ic_detSIAFF = 0;
		
		try{
			con.conexionDB();			

			ic_resSIAFF = numero_carga_siaff;
						
			this.setResumenCargaSIAFF(numeroProceso, claveUsuario, nombreArchivo, ic_resSIAFF, con);
			
			strSQL = new StringBuffer();

			strSQL.append(" SELECT CC_RAMO_SIAFF,");
			strSQL.append(" CC_UNIDAD_SIAFF,");
			strSQL.append(" CG_FOLIO_CLC,");
			strSQL.append(" CG_FOLIO_DEPENDENCIA,");
			strSQL.append(" DF_ALTA_CLC,");
			strSQL.append(" IG_DIGITO_IDENTIFICADOR,");
			strSQL.append(" CG_RFC_BENEFICIARIO,");
			strSQL.append(" CG_NOMBRE_BENEFICIARIO,");
			strSQL.append(" CG_DIVISA,");
			strSQL.append(" IG_OGTO,");
			strSQL.append(" DF_PAGO,");
			strSQL.append(" CG_ESTATUS,");
			strSQL.append(" FG_IMPORTE,");
			strSQL.append(" IC_DOCUMENTO,");
			strSQL.append(" IC_EPO,");
			strSQL.append(" CS_REGISTRADO_EN_CADENAS");
			strSQL.append(" FROM COMTMP_DETALLE_CARGA_SIAFF");
			strSQL.append(" WHERE IG_NUM_PROCESO = ?");
			strSQL.append(" AND CS_REGISTRADO_EN_CADENAS IN (?, ?)");
			
			varBind.add(new Integer(numeroProceso));
			varBind.add("N");
			varBind.add("S");
			
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);

			ps = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = ps.executeQuery();			
			
			while(rs.next()){
				strSQL = new StringBuffer();
				
				strSQL.append(" SELECT SEQ_COM_DET_CARGA_SIAFF.NEXTVAL FROM DUAL");
						
				List lsDet = con.consultaDB(strSQL.toString());
				
				if(lsDet != null && lsDet.size() > 0){
					List valReg = (List)lsDet.get(0);
					ic_detSIAFF = Integer.parseInt((String)valReg.get(0));
				}
				
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO COM_DETALLE_CARGA_SIAFF (");
				strSQL.append(" IC_DETALLE_CARGA,");
				strSQL.append(" CC_RAMO_SIAFF,");
				strSQL.append(" CC_UNIDAD_SIAFF,");
				strSQL.append(" CG_FOLIO_CLC,");
				strSQL.append(" CG_FOLIO_DEPENDENCIA,");
				strSQL.append(" DF_ALTA_CLC,");
				strSQL.append(" IG_DIGITO_IDENTIFICADOR,");
				strSQL.append(" CG_RFC_BENEFICIARIO,");
				strSQL.append(" CG_NOMBRE_BENEFICIARIO,");
				strSQL.append(" CG_DIVISA,");
				strSQL.append(" IG_OGTO,");
				strSQL.append(" DF_PAGO,");
				strSQL.append(" CG_ESTATUS,");
				strSQL.append(" FG_IMPORTE,");
				strSQL.append(" IC_DOCUMENTO,");
				strSQL.append(" IC_EPO,");
				strSQL.append(" CS_REGISTRADO_EN_CADENAS,");
				strSQL.append(" IC_CARGA_SIAFF");
				strSQL.append(") VALUES (?, ?, ?, ?, ?, TO_DATE(?,'DD/MM/YYYY'), ?, ?, ?, ?, ?, TO_DATE(?, 'DD/MM/YYYY'), ?, ?, ?, ?, ?, ?)");
				
				varBind.add(new Integer(ic_detSIAFF));
				varBind.add(rs.getString("CC_RAMO_SIAFF"));
				varBind.add(rs.getString("CC_UNIDAD_SIAFF"));
				varBind.add(rs.getString("CG_FOLIO_CLC"));
				varBind.add(rs.getString("CG_FOLIO_DEPENDENCIA"));
				varBind.add(rs.getString("DF_ALTA_CLC"));
				varBind.add(rs.getString("IG_DIGITO_IDENTIFICADOR"));
				varBind.add(rs.getString("CG_RFC_BENEFICIARIO"));
				varBind.add(rs.getString("CG_NOMBRE_BENEFICIARIO"));
				varBind.add(rs.getString("CG_DIVISA"));
				varBind.add(rs.getString("IG_OGTO"));
				varBind.add(rs.getString("DF_PAGO"));
				varBind.add(rs.getString("CG_ESTATUS"));
				varBind.add(new Double(rs.getString("FG_IMPORTE")));
				varBind.add(new Long(rs.getString("IC_DOCUMENTO")));
				varBind.add(new Integer(rs.getString("IC_EPO")));
				varBind.add(rs.getString("CS_REGISTRADO_EN_CADENAS"));
				varBind.add(new Integer(ic_resSIAFF));

				log.debug("strSQL : "+strSQL.toString());
				log.debug("varBind : "+varBind);

				PreparedStatement psi = con.queryPrecompilado(strSQL.toString(), varBind);

				psi.executeUpdate();
				psi.close();
			}						
			
			rs.close();
			ps.close();
//******************************************************************************************
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT ig_linea_reg, cg_error_det");
			strSQL.append(" FROM comtmp_detalle_carga_siaff");
			strSQL.append(" WHERE ig_num_proceso = ?");
			strSQL.append(" AND cg_error_det IS NOT NULL");
			strSQL.append(" ORDER BY ig_linea_reg");
			
			varBind.add(new Integer(numeroProceso));
			
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);

			ps = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = ps.executeQuery();			
			
			while(rs.next()){
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO com_rechazo_carga_siaff (");
				strSQL.append(" ic_carga_siaff,");
				strSQL.append(" ig_linea,");
				strSQL.append(" cg_error_det");
				strSQL.append(") VALUES (?, ?, ?)");
				
				varBind.add(new Integer(ic_resSIAFF));
				varBind.add(new Integer(rs.getString("ig_linea_reg")));
				varBind.add(rs.getString("cg_error_det"));

				log.debug("strSQL : "+strSQL.toString());
				log.debug("varBind : "+varBind);

				PreparedStatement psi = con.queryPrecompilado(strSQL.toString(), varBind);
				psi.executeUpdate();
				psi.close();
			}
			
			rs.close();
			ps.close();
//******************************************************************************************
			strSQL = new StringBuffer();
			
			strSQL.append(" DELETE COMTMP_DETALLE_CARGA_SIAFF");
			
			con.ejecutaUpdateDB(strSQL.toString());
			
		}catch (Exception e){
			lbOK = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("procesarCargaSIAFF(F)");
		}
}
	
	/**
	 * Metodo que guarda resumen de la carga de SIAFF hecha previamente.
	 * Modificado FODEA 034 - 2009 ACF
	 * @throws com.netro.exception.NafinException
	 * @param nombreArchivo - nombre del archivo txt cargado
	 * @param claveUsuario - id del Usuario que realizo la carga
	 * @param numeroProceso - numero identificador del proceso que se realiza
	 */
	private void setResumenCargaSIAFF(int numeroProceso, String claveUsuario, String nombreArchivo, int ic_carga_siaff, AccesoDB con) throws NafinException{
		log.info("setResumenCargaSIAFF(I)");
		PreparedStatement ps = null;
		ResultSet rs = null;	
		boolean lbOk = true;
		int Nreg = 0;
		int Sreg = 0;
		int Treg = 0;
		int Terr = 0;
		String nombreCompletoUsuario = "";
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		
		try{
			if(!claveUsuario.equals("")){
				UtilUsr utilUsr = new UtilUsr();
				Usuario usuario = utilUsr.getUsuario(claveUsuario);
				nombreCompletoUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
			}else{
				claveUsuario = "PROC AUT";
				nombreCompletoUsuario = "PROC AUT";
			}
			
			strSQL.append(" SELECT CS_REGISTRADO_EN_CADENAS REGISTRADOS,  COUNT(1) NUM");
			strSQL.append(" FROM COMTMP_DETALLE_CARGA_SIAFF");
			strSQL.append(" WHERE IG_NUM_PROCESO = ?");
			strSQL.append(" AND CS_REGISTRADO_EN_CADENAS IN (?, ?)");
			strSQL.append(" GROUP BY  CS_REGISTRADO_EN_CADENAS");
			strSQL.append(" ORDER BY CS_REGISTRADO_EN_CADENAS");
			
			varBind.add(new Integer(numeroProceso));
			varBind.add("S");
			varBind.add("N");

			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);
			
			ps = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = ps.executeQuery();
			
			while(rs.next()){
				if(rs.getString("REGISTRADOS").equals("N"))
					Nreg = rs.getInt("NUM");
				if(rs.getString("REGISTRADOS").equals("S"))
					Sreg = rs.getInt("NUM");
			}
			
			rs.close();
			ps.close();
			/*se obtiene el numero de registros con error*/
			strSQL = new StringBuffer();
			varBind = new ArrayList();
			
			strSQL.append(" SELECT COUNT(1) FROM comtmp_detalle_carga_siaff WHERE ig_num_proceso = ? AND cg_error_det IS NOT NULL");
			varBind.add(new Integer(numeroProceso));
			
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);
			
			ps = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = ps.executeQuery();
			
			while(rs.next()){Terr = rs.getInt(1);}
			
			rs.close();
			ps.close();
			
			if(Nreg > 0 || Sreg > 0){
				Treg = Nreg + Sreg;
				
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO COM_RESUMEN_CARGA_SIAFF (");
				strSQL.append(" cg_nombre_archivo,");
				strSQL.append(" cg_nombre_usuario,");
				strSQL.append(" df_carga,");
				strSQL.append(" ic_carga_siaff,");
				strSQL.append(" ic_usuario,");
				strSQL.append(" ig_registros_cargados,");
				strSQL.append(" ig_registros_encontrados_ne,");
				strSQL.append(" ig_registros_no_encontrados_ne,");
				strSQL.append(" ig_registros_con_error");
				strSQL.append(") VALUES (?, ?, SYSDATE, ?, ?, ?, ?, ?, ?)");
				
				varBind.add(nombreArchivo);
				varBind.add(nombreCompletoUsuario);
				varBind.add(new Integer(ic_carga_siaff));
				varBind.add(claveUsuario);
				varBind.add(new Integer(Treg));
				varBind.add(new Integer(Sreg));
				varBind.add(new Integer(Nreg));
				varBind.add(new Integer(Terr));
				
				log.debug("strSQL : "+strSQL.toString());
				log.debug("varBind : "+varBind);
				
				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				ps.executeUpdate();
				ps.close();
			}
		}catch(Exception e){
			lbOk =false;
			e.printStackTrace();
			throw  new NafinException("SIST0001");
		}finally{
			log.info("setResumenCargaSIAFF(F)");
		}
	}

	/**
	 * M�todo que obtiene el resumen da la carga del reporte SIAFF para el perfil ADMIN NAFIN.
	 * @param fec_ini Fecha de inicio del rango de fechas a consultar.
	 * @param fec_fin Fecha de fin del rango de fechas a consultar.
	 * @return totalRegSIAFF Lista con el resultado de la consulta.
	 * @throws NafinException Excepcion al ocurrir un error durante la consulta.
	 */
	public List getCargaSIAFF(String fec_ini, String fec_fin) throws NafinException{
		log.info("getCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List totalRegSIAFF = new ArrayList();
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT DISTINCT ");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'Autom�tico', 'Manual') tipo_proceso,");
			strSQL.append(" crs.df_carga fcarga,");
			strSQL.append(" crs.cg_nombre_archivo nombreArc,");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'PROC AUT', crs.ic_usuario || ' ' || crs.cg_nombre_usuario) nomUsuario,");
			strSQL.append(" crs.ig_registros_cargados,");
			strSQL.append(" crs.ig_registros_encontrados_ne,");
			strSQL.append(" crs.ig_registros_no_encontrados_ne,");
			strSQL.append(" crs.ig_registros_con_error,");
			strSQL.append(" crs.ic_carga_siaff icSiaff,");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'A', 'M') tipo_carga");
			strSQL.append(" FROM com_detalle_carga_siaff cds");
			strSQL.append(", com_resumen_carga_siaff crs");
			strSQL.append(" WHERE cds.ic_carga_siaff = crs.ic_carga_siaff");

			if(!fec_ini.equals("")){
				strSQL.append(" AND crs.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fec_ini);
			}
			
			if(!fec_fin.equals("")){
				strSQL.append(" AND crs.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fec_fin);
			}
			
			strSQL.append(" ORDER BY crs.df_carga DESC");
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			ps = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = ps.executeQuery();
			
			while(rs.next()){
				List regSIAFF = new ArrayList();				
				regSIAFF.add(rs.getString("tipo_proceso"));
				regSIAFF.add(rs.getString("fcarga"));
				regSIAFF.add(rs.getString("nombreArc"));
				regSIAFF.add(rs.getString("nomUsuario"));
				regSIAFF.add(rs.getString("ig_registros_cargados"));
				regSIAFF.add(rs.getString("ig_registros_encontrados_ne"));
				regSIAFF.add(rs.getString("ig_registros_no_encontrados_ne"));
				regSIAFF.add(rs.getString("ig_registros_con_error"));
				regSIAFF.add(rs.getString("icSiaff"));
				regSIAFF.add(rs.getString("tipo_carga"));
				totalRegSIAFF.add(regSIAFF);
			}
			
			rs.close();
			ps.close();
			
			return totalRegSIAFF;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getCargaSIAFF(F)");
		}
	}

	/**
	 * M�todo que obtiene el resumen da la carga del reporte SIAFF para el perfil ADMIN EPO.
	 * @param fec_ini Fecha de inicio del rango de fechas a consultar.
	 * @param fec_fin Fecha de fin del rango de fechas a consultar.
	 * @param ic_epo Clave de la EPO.
	 * @return totalRegSIAFF Lista con el resultado de la consulta.
	 * @throws NafinException Excepcion al ocurrir un error durante la consulta.
	 */
	public List getCargaSIAFFxEpo(String fec_ini, String fec_fin, String ic_epo) throws NafinException{
		log.info("getCargaSIAFFxEpo(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List totalRegSIAFF = new ArrayList();
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT cds.cc_ramo_siaff,");
			strSQL.append(" cds.cc_unidad_siaff,");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'Autom�tico', 'Manual') tipo_proceso,");
			strSQL.append(" crs.df_carga fcarga,");
			strSQL.append(" crs.cg_nombre_archivo nombrearc,");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'PROC AUT', crs.ic_usuario || ' ' || crs.cg_nombre_usuario) nomUsuario,");
			strSQL.append(" crs.ic_carga_siaff icsiaff,");
			strSQL.append(" cds.cs_registrado_en_cadenas,");
			strSQL.append(" DECODE(crs.ic_usuario, 'PROC AUT', 'A', 'M') tipo_carga,");
			strSQL.append(" COUNT(cds.cs_registrado_en_cadenas) num");
			strSQL.append(" FROM com_detalle_carga_siaff cds");
			strSQL.append(", com_resumen_carga_siaff crs");
			strSQL.append(", comcat_epo ce");
			strSQL.append(" WHERE cds.ic_carga_siaff = crs.ic_carga_siaff");
			strSQL.append(" AND cds.cc_ramo_siaff = ce.cc_ramo_siaff");
			strSQL.append(" AND cds.cc_unidad_siaff = ce.cc_unidad_siaff");
			
			if(ic_epo!=null && !ic_epo.equals("")){
				strSQL.append(" AND ce.ic_epo = ?");
				varBind.add(new Integer(ic_epo));
			}
			if(!fec_ini.equals("")){
				strSQL.append(" AND crs.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fec_ini);
			}
			if(!fec_fin.equals("")){
				strSQL.append(" AND crs.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fec_fin);
			}

			strSQL.append(" GROUP BY cds.cc_ramo_siaff,");
			strSQL.append(" cds.cc_unidad_siaff,");
			strSQL.append(" crs.ic_usuario,");
			strSQL.append(" crs.df_carga,");
			strSQL.append(" crs.cg_nombre_archivo,");
			strSQL.append(" crs.cg_nombre_usuario,");
			strSQL.append(" crs.ic_carga_siaff,");
			strSQL.append(" cds.cs_registrado_en_cadenas ");
			strSQL.append(" ORDER BY crs.df_carga");
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);
			
			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				List regSIAFF = new ArrayList();
				regSIAFF.add(rs.getString("tipo_proceso"));
				regSIAFF.add(rs.getString("fcarga"));
				regSIAFF.add(rs.getString("nombreArc"));
				regSIAFF.add(rs.getString("nomUsuario"));
				regSIAFF.add(rs.getString("cs_registrado_en_cadenas"));
				regSIAFF.add(rs.getString("num"));
				regSIAFF.add(rs.getString("icSiaff"));
				totalRegSIAFF.add(regSIAFF);
			}
			
			rs.close();
			ps.close();
			
			return totalRegSIAFF;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getCargaSIAFFxEpo(F)");
		}
	}

	/**
	* Metodo que indica si existe parametrizacion de una epo en com_parametrizacion_epo 
	* eso quiere decir que ya hay valor 'S' para  requisito 12.1
	* @param sEpo cadena con el IC_epo
	* 
	* @return boolean bandera que indica si existe parametrizacion
	* @throws com.netro.exception.NafinException
	**/
	public boolean getExisteParametrizacionEpo(String sEpo) throws NafinException {
		
		System.out.println("getExisteParametrizacionEpo(E)");
		
		boolean 					resultado = false;
		AccesoDB 					con				= new AccesoDB();
		PreparedStatement ps				= null;
		ResultSet 				rs 				= null;
		
		try{
			con.conexionDB();
			
			String Query = "SELECT COUNT (cg_valor) numregs " +
			"  FROM com_parametrizacion_epo " +
			" WHERE cc_parametro_epo = 'PUB_EPO_PEF_USA_SIAFF' AND ic_epo = ? and cg_valor = ? ";

			ps = con.queryPrecompilado(Query);
			ps.setInt(1,Integer.parseInt(sEpo));
			ps.setString(2,"S");
			
			System.out.println("query : "+Query);
			System.out.println("Bind1 : "+sEpo);
			
			rs = ps.executeQuery();
			
			if(rs.next()){
			
				int regs = Integer.parseInt(rs.getString("numregs"));
				
				if(regs>0){				
					resultado = true;			
				}
			}		
		
      rs.close();
			ps.close();			
		
			return resultado;
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getExisteParametrizacionEpo(S)");
		}
	}	
	
	//****************************************************RESUMEN PYME**************************************************
	/**
	 * Metodo que devuelve una lista con el resumen de las pymes para una epo especificada. M�todo reprogramado dado que
   * su forma anterior no obtenia la informaci�n de manera correcta (ACF - 20/04/2010).
	 * @throws com.netro.exception.NafinException
	 * @return Lista con las pymes encontradas y su detalle
	 * @param df_publicacion_a Rango inicial para la fecha de publicacion
	 * @param df_publicacion_de Limite para la fecha de publicacion
	 * @param ic_epo Clave de la epo
	 */
	
	public HashMap getResumenPymes(String ic_epo, String df_publicacion_de, String df_publicacion_a) throws NafinException{
    log.info("getResumenPymes(E) ::..");
		AccesoDB con = new AccesoDB();
    BigDecimal numeroDocumentosPubMN = new BigDecimal("0.00");
    BigDecimal totalDocumentosPubMN = new BigDecimal("0.00");
    BigDecimal numeroDocumentosOpeMN = new BigDecimal("0.00");
    BigDecimal totalDocumentosOpeMN = new BigDecimal("0.00");
    BigDecimal totalInteresesMN = new BigDecimal("0.00");
    BigDecimal totalFactorajeMN = new BigDecimal("0.00");
    BigDecimal numeroDocumentosPubDL = new BigDecimal("0.00");
    BigDecimal totalDocumentosPubDL = new BigDecimal("0.00");
    BigDecimal numeroDocumentosOpeDL = new BigDecimal("0.00");
    BigDecimal totalDocumentosOpeDL = new BigDecimal("0.00");
    BigDecimal totalInteresesDL = new BigDecimal("0.00");
    BigDecimal totalFactorajeDL = new BigDecimal("0.00");
    StringBuffer strSQL = new StringBuffer();
    ArrayList varBind = new ArrayList();
		Registros registros	= null;
		HashMap TotalMonedaNacional	= new HashMap();
		HashMap TotalDolar = new HashMap();
		HashMap resultados = new HashMap();
		List MonedaNacional	= new ArrayList();
		List Dolar = new ArrayList();
		String strQueryEPO = null;
		String nombreEPO = "";
		
		try {
			con.conexionDB();
			
			strQueryEPO = " SELECT cg_razon_social FROM comcat_epo WHERE ic_epo = ? ";
			
			PreparedStatement psEPO = con.queryPrecompilado(strQueryEPO);
			psEPO.setInt(1, Integer.parseInt(ic_epo));
			ResultSet rsEPO = psEPO.executeQuery();
			if (rsEPO.next()) {
				nombreEPO = rsEPO.getString("cg_razon_social");
			}
			
			rsEPO.close();
			psEPO.close();

      strSQL.append(" SELECT rep.ic_pyme AS num_pyme,");
      strSQL.append(" pyme.cg_razon_social AS razon_social,");
      strSQL.append(" rep.cs_aceptacion AS habilitada,");
      strSQL.append(" SUM(rep.doctos_publicados) AS doctos_publicados,");
      strSQL.append(" SUM(rep.monto_publicado) AS monto_publicado,");
      strSQL.append(" SUM(rep.doctos_operados) AS doctos_operados,");
      strSQL.append(" SUM(rep.monto_operado) AS monto_operado,");
      strSQL.append(" SUM(rep.monto_intereses) AS monto_intereses,");
      strSQL.append(" SUM(rep.monto_factoraje) AS monto_factoraje");
      strSQL.append(" FROM comcat_pyme pyme,");
      strSQL.append(" (SELECT /*+use_nl(docto pe)*/ pe.ic_epo,");
      strSQL.append(" pe.ic_pyme,");
      strSQL.append(" DECODE(pe.cs_aceptacion, 'H', 'Hablilitada', NULL) AS cs_aceptacion,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_publicados,");
      strSQL.append(" SUM(docto.fn_monto) AS monto_publicado,");
      strSQL.append(" 0 AS doctos_operados,");
      strSQL.append(" 0 AS monto_operado,");
      strSQL.append(" 0 AS monto_intereses,");
      strSQL.append(" 0 AS monto_factoraje");
      strSQL.append(" FROM comrel_pyme_epo pe, com_documento docto");
      strSQL.append(" WHERE pe.ic_epo = docto.ic_epo");
      strSQL.append(" AND pe.ic_pyme = docto.ic_pyme");
      strSQL.append(" AND docto.ic_estatus_docto NOT IN (?)");
      strSQL.append(" AND docto.df_alta >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND docto.df_alta < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND docto.ic_moneda = ?");
      strSQL.append(" AND pe.ic_epo = ?");
      strSQL.append(" GROUP BY pe.ic_epo, pe.ic_pyme, pe.cs_aceptacion");
      strSQL.append(" UNION");
      strSQL.append(" SELECT /*+use_nl(sol docto cds pe)*/ docto.ic_epo,");
	  strSQL.append(" docto.ic_pyme,");
      strSQL.append(" DECODE(pe.cs_aceptacion, 'H', 'Hablilitada', NULL) AS cs_aceptacion,");
      strSQL.append(" 0 AS doctos_publicados,");
      strSQL.append(" 0 AS monto_publicado,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_operados,");
      strSQL.append(" SUM(docto.fn_monto) AS monto_operado,");
      strSQL.append(" SUM(cds.in_importe_interes) AS monto_intereses,");
      strSQL.append(" SUM(cds.in_importe_recibir) AS monto_factoraje");
      strSQL.append(" FROM com_solicitud sol, com_documento docto, com_docto_seleccionado cds, comrel_pyme_epo pe");
      strSQL.append(" WHERE sol.ic_documento = docto.ic_documento");
      strSQL.append(" AND docto.ic_documento = cds.ic_documento");
      strSQL.append(" AND docto.ic_epo = cds.ic_epo");
      strSQL.append(" AND docto.ic_if = cds.ic_if");
      strSQL.append(" AND docto.ic_epo = pe.ic_epo");
      strSQL.append(" AND docto.ic_pyme = pe.ic_pyme");
      strSQL.append(" AND sol.df_fecha_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND sol.df_fecha_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND docto.ic_moneda = ?");
      strSQL.append(" AND docto.ic_epo = ?");
      strSQL.append(" GROUP BY docto.ic_epo, docto.ic_pyme, pe.cs_aceptacion) rep");
      strSQL.append(" WHERE rep.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND rep.ic_epo = ?");
      strSQL.append(" GROUP BY rep.ic_pyme, pyme.cg_razon_social, rep.cs_aceptacion");
      strSQL.append(" ORDER BY pyme.cg_razon_social");
      
      varBind.add(new Integer(5));//Documentos con estatus BAJA
      varBind.add(df_publicacion_de);//fecha_ini
      varBind.add(df_publicacion_a);//fecha_fin
      varBind.add(new Integer(1));//moneda nacional
      varBind.add(new Integer(ic_epo));//ic_epo
      varBind.add(df_publicacion_de);//fecha_ini
      varBind.add(df_publicacion_a);//fecha_fin
      varBind.add(new Integer(1));//moneda nacional
      varBind.add(new Integer(ic_epo));//ic_epo
      varBind.add(new Integer(ic_epo));//ic_epo
      
      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);
      
      registros = con.consultarDB(strSQL.toString(), varBind, false);
      
      while (registros != null && registros.next()) {
        HashMap registro = new HashMap();
        registro.put("NUM_PYME", registros.getString("num_pyme"));        
				registro.put("RAZON_SOCIAL", registros.getString("razon_social"));
        registro.put("HABILITADA", registros.getString("habilitada"));
				registro.put("DOCUMENTOS_PUBLICADOS",	registros.getString("doctos_publicados"));
				registro.put("MONTO_PUBLICADO",	registros.getString("monto_publicado")==null?"":Comunes.formatoDecimal(registros.getString("monto_publicado"), 2, true));
        registro.put("DOCUMENTOS",	registros.getString("doctos_operados")==null?"":registros.getString("doctos_operados"));
        registro.put("MONTO_OPERADOS",	registros.getString("monto_operado")==null?"":Comunes.formatoDecimal(registros.getString("monto_operado"), 2, true));
        registro.put("INTERESES",	registros.getString("monto_intereses")==null?"":Comunes.formatoDecimal(registros.getString("monto_intereses"), 2, true));
        registro.put("MONTO_FACTORAJE",	registros.getString("monto_factoraje")==null?"":Comunes.formatoDecimal(registros.getString("monto_factoraje"), 2, true));
        MonedaNacional.add(registro);
        
        numeroDocumentosPubMN = numeroDocumentosPubMN.add(new BigDecimal(registros.getString("doctos_publicados")));
        totalDocumentosPubMN = totalDocumentosPubMN.add(new BigDecimal(registros.getString("monto_publicado")));
        numeroDocumentosOpeMN = numeroDocumentosOpeMN.add(new BigDecimal(registros.getString("doctos_operados")));
        totalDocumentosOpeMN = totalDocumentosOpeMN.add(new BigDecimal(registros.getString("monto_operado")));
        totalInteresesMN = totalInteresesMN.add(new BigDecimal(registros.getString("monto_intereses")));
        totalFactorajeMN = totalFactorajeMN.add(new BigDecimal(registros.getString("monto_factoraje")));
      }
      
      TotalMonedaNacional.put("DOCUMENTOS",	numeroDocumentosPubMN.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString());
      TotalMonedaNacional.put("MONTO", Comunes.formatoDecimal(totalDocumentosPubMN, 2, true));
      TotalMonedaNacional.put("DOCUMENTOSOPE",	numeroDocumentosOpeMN.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString());
      TotalMonedaNacional.put("MONTO_OPERADOS", Comunes.formatoDecimal(totalDocumentosOpeMN, 2, true));
      TotalMonedaNacional.put("INTERESES", Comunes.formatoDecimal(totalInteresesMN, 2, true));
      TotalMonedaNacional.put("MONTO_FACTORAJE", Comunes.formatoDecimal(totalFactorajeMN, 2, true));
      
      //Dolares
      strSQL = new StringBuffer();
      varBind = new ArrayList();

      strSQL.append(" SELECT rep.ic_pyme AS num_pyme,");
      strSQL.append(" pyme.cg_razon_social AS razon_social,");
      strSQL.append(" rep.cs_aceptacion AS habilitada,");
      strSQL.append(" SUM(rep.doctos_publicados) AS doctos_publicados,");
      strSQL.append(" SUM(rep.monto_publicado) AS monto_publicado,");
      strSQL.append(" SUM(rep.doctos_operados) AS doctos_operados,");
      strSQL.append(" SUM(rep.monto_operado) AS monto_operado,");
      strSQL.append(" SUM(rep.monto_intereses) AS monto_intereses,");
      strSQL.append(" SUM(rep.monto_factoraje) AS monto_factoraje");
      strSQL.append(" FROM comcat_pyme pyme,");
      strSQL.append(" (SELECT /*+use_nl(docto pe)*/ pe.ic_epo,");
      strSQL.append(" pe.ic_pyme,");
      strSQL.append(" DECODE(pe.cs_aceptacion, 'H', 'Hablilitada', NULL) AS cs_aceptacion,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_publicados,");
      strSQL.append(" SUM(docto.fn_monto) AS monto_publicado,");
      strSQL.append(" 0 AS doctos_operados,");
      strSQL.append(" 0 AS monto_operado,");
      strSQL.append(" 0 AS monto_intereses,");
      strSQL.append(" 0 AS monto_factoraje");
      strSQL.append(" FROM comrel_pyme_epo pe, com_documento docto");
      strSQL.append(" WHERE pe.ic_epo = docto.ic_epo");
      strSQL.append(" AND pe.ic_pyme = docto.ic_pyme");
      strSQL.append(" AND docto.ic_estatus_docto NOT IN (?)");
      strSQL.append(" AND docto.df_alta >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND docto.df_alta < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND docto.ic_moneda = ?");
      strSQL.append(" AND pe.ic_epo = ?");
      strSQL.append(" GROUP BY pe.ic_epo, pe.ic_pyme, pe.cs_aceptacion");
      strSQL.append(" UNION");
      strSQL.append(" SELECT /*+use_nl(sol docto cds pe)*/ docto.ic_epo,");
	  strSQL.append(" docto.ic_pyme,");
      strSQL.append(" DECODE(pe.cs_aceptacion, 'H', 'Hablilitada', NULL) AS cs_aceptacion,");
      strSQL.append(" 0 AS doctos_publicados,");
      strSQL.append(" 0 AS monto_publicado,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_operados,");
      strSQL.append(" SUM(docto.fn_monto) AS monto_operado,");
      strSQL.append(" SUM(cds.in_importe_interes) AS monto_intereses,");
      strSQL.append(" SUM(cds.in_importe_recibir) AS monto_factoraje");
      strSQL.append(" FROM com_solicitud sol, com_documento docto, com_docto_seleccionado cds, comrel_pyme_epo pe");
      strSQL.append(" WHERE sol.ic_documento = docto.ic_documento");
      strSQL.append(" AND docto.ic_documento = cds.ic_documento");
      strSQL.append(" AND docto.ic_epo = cds.ic_epo");
      strSQL.append(" AND docto.ic_if = cds.ic_if");
      strSQL.append(" AND docto.ic_epo = pe.ic_epo");
      strSQL.append(" AND docto.ic_pyme = pe.ic_pyme");
      strSQL.append(" AND sol.df_fecha_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND sol.df_fecha_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND docto.ic_moneda = ?");
      strSQL.append(" AND docto.ic_epo = ?");
      strSQL.append(" GROUP BY docto.ic_epo, docto.ic_pyme, pe.cs_aceptacion) rep");
      strSQL.append(" WHERE rep.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND rep.ic_epo = ?");
      strSQL.append(" GROUP BY rep.ic_pyme, pyme.cg_razon_social, rep.cs_aceptacion");
      strSQL.append(" ORDER BY pyme.cg_razon_social");
      
      varBind.add(new Integer(5));//Documentos con estatus BAJA
      varBind.add(df_publicacion_de);//fecha_ini
      varBind.add(df_publicacion_a);//fecha_fin
      varBind.add(new Integer(54));//Dolares
      varBind.add(new Integer(ic_epo));//ic_epo
      varBind.add(df_publicacion_de);//fecha_ini
      varBind.add(df_publicacion_a);//fecha_fin
      varBind.add(new Integer(54));//Dolares
      varBind.add(new Integer(ic_epo));//ic_epo
      varBind.add(new Integer(ic_epo));//ic_epo
      
      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);
      
      registros = con.consultarDB(strSQL.toString(), varBind, false);
      
      while (registros != null && registros.next()) {
        HashMap registro = new HashMap();
        registro.put("NUM_PYME", registros.getString("num_pyme"));        
				registro.put("RAZON_SOCIAL", registros.getString("razon_social"));
        registro.put("HABILITADA", registros.getString("habilitada"));
				registro.put("DOCUMENTOS_PUBLICADOS",	registros.getString("doctos_publicados"));
				registro.put("MONTO_PUBLICADO",	registros.getString("monto_publicado")==null?"":Comunes.formatoDecimal(registros.getString("monto_publicado"), 2, true));
        registro.put("DOCUMENTOS",	registros.getString("doctos_operados")==null?"":registros.getString("doctos_operados"));
        registro.put("MONTO_OPERADOS",	registros.getString("monto_operado")==null?"":Comunes.formatoDecimal(registros.getString("monto_operado"), 2, true));
        registro.put("INTERESES",	registros.getString("monto_intereses")==null?"":Comunes.formatoDecimal(registros.getString("monto_intereses"), 2, true));
        registro.put("MONTO_FACTORAJE",	registros.getString("monto_factoraje")==null?"":Comunes.formatoDecimal(registros.getString("monto_factoraje"), 2, true));
        Dolar.add(registro);

        numeroDocumentosPubDL = numeroDocumentosPubDL.add(new BigDecimal(registros.getString("doctos_publicados")));
        totalDocumentosPubDL = totalDocumentosPubDL.add(new BigDecimal(registros.getString("monto_publicado")));
        numeroDocumentosOpeDL = numeroDocumentosOpeDL.add(new BigDecimal(registros.getString("doctos_operados")));
        totalDocumentosOpeDL = totalDocumentosOpeDL.add(new BigDecimal(registros.getString("monto_operado")));
        totalInteresesDL = totalInteresesDL.add(new BigDecimal(registros.getString("monto_intereses")));
        totalFactorajeDL = totalFactorajeDL.add(new BigDecimal(registros.getString("monto_factoraje")));
      }
      
      TotalDolar.put("DOCUMENTOS",	numeroDocumentosPubDL.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString());
      TotalDolar.put("MONTO", Comunes.formatoDecimal(totalDocumentosPubDL, 2, true));
      TotalDolar.put("DOCUMENTOSOPE",	numeroDocumentosOpeDL.setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString());
      TotalDolar.put("MONTO_OPERADOS", Comunes.formatoDecimal(totalDocumentosOpeDL, 2, true));
      TotalDolar.put("INTERESES", Comunes.formatoDecimal(totalInteresesDL, 2, true));
      TotalDolar.put("MONTO_FACTORAJE", Comunes.formatoDecimal(totalFactorajeDL, 2, true));

			resultados.put("EPO", nombreEPO);
			resultados.put("MONEDANACIONAL", MonedaNacional);
			resultados.put("DOLAR", Dolar);
			resultados.put("TOTALMONEDANACIONAL", TotalMonedaNacional);
			resultados.put("TOTALDOLAR", TotalDolar);
				
		} catch(Exception e) {
      log.info("getResumenPymes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
      log.info("getResumenPymes(E) ::..");
		}	
		return resultados;
	}//getResumenPymes
	
	
	//****************************************************REPORTE ANALITICO DE DOCUMENTOS**************************************************
	/**
	 * Metodo que devuelve una lista con el reporte anal�tico de documentos
	 * @throws com.netro.exception.NafinException
	 * @return Lista con las pymes encontradas y su detalle
	 * @param df_publicacion_a Rango inicial para la fecha de publicacion
	 * @param df_publicacion_de Limite para la fecha de publicacion
	 * @param ic_epo Clave de la epo
	 */
	public HashMap getReporteAnaliticoDoctos(String ic_epo, String df_publicacion_de, String df_publicacion_a) throws NafinException{
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer 	qrySentencia	= new StringBuffer();
		StringBuffer   qryTotalMN		= new StringBuffer();
		StringBuffer   qryTotalMN2		= new StringBuffer();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		List				MonedaNacional	= new ArrayList();
		List				Dolar				= new ArrayList();
		HashMap			TotalMonedaNacional	= new HashMap();
		HashMap			TotalDolar				= new HashMap();
		HashMap			resultados 		= new HashMap();
		String			strQueryEPO = null;
		String 			nombreEPO 		= "";
		System.out.println("InformacionTOICBean::getResumenPymes(E)");
		try 
		{	
			con.conexionDB();
			
			strQueryEPO = " SELECT cg_razon_social FROM comcat_epo where ic_epo = ? ";
			
			PreparedStatement psEPO = con.queryPrecompilado(strQueryEPO);
			psEPO.setInt(1, Integer.parseInt(ic_epo));
			ResultSet rsEPO = psEPO.executeQuery();
			if (rsEPO.next()) {
				nombreEPO = rsEPO.getString("cg_razon_social");
			}
			
			rsEPO.close();
			psEPO.close();
			
			qrySentencia.append( 
        "SELECT /*+  USE_NL (d, p, cpe, s, ds) INDEX(d IN_COM_DOCUMENTO_10_NUK)*/ " +
        "       p.ic_pyme num_pyme, p.cg_razon_social razon_social, " +
        "       d.ig_numero_docto numerodoc, d.ic_documento icdocto, " +
        "       TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') dfemision, " +
        "       TO_CHAR (d.df_alta, 'dd/mm/yyyy') dfpublicacion, " +
        "       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') dfvencimiento, " +
        "       d.cg_clave_presupuestaria clavepresupuestaria, " +
        "       ROUND (d.df_alta - df_fecha_docto) plazoregistro, " +
        "       ROUND (d.df_fecha_venc - d.df_alta) plazopago, ic_moneda moneda, " +
        "       TO_CHAR (d.df_entrega, 'dd/mm/yyyy') fechaentrega, " +
        "       d.cg_tipo_compra tipocompra, cg_periodo periodo, d.ic_epo epo, " +
        "       cpe.cs_aceptacion habilitada, d.fn_monto montopublicado, " +
        "       CASE " +
        "          WHEN NVL (s.df_fecha_solicitud, " +
        "                    TO_DATE ('01/01/2000', 'dd/mm/yyyy') " +
        "                   ) >= TO_DATE (?, 'dd/mm/yyyy') " +
        "          AND NVL (s.df_fecha_solicitud, TO_DATE ('01/01/2000', 'dd/mm/yyyy')) < " +
        "                                       TO_DATE (?, 'dd/mm/yyyy') " +
        "                                       + 1 " +
        "             THEN ds.in_importe_interes " +
        "          ELSE 0 " +
        "       END intereses, " +
        "       CASE " +
        "          WHEN NVL (s.df_fecha_solicitud, " +
        "                    TO_DATE ('01/01/2000', 'dd/mm/yyyy') " +
        "                   ) >= TO_DATE (?, 'dd/mm/yyyy') " +
        "          AND NVL (s.df_fecha_solicitud, TO_DATE ('01/01/2000', 'dd/mm/yyyy')) < " +
        "                                       TO_DATE (?, 'dd/mm/yyyy') " +
        "                                       + 1 " +
        "             THEN ds.in_importe_recibir " +
        "          ELSE 0 " +
        "       END montooperado, " +
        "       d.ic_estatus_docto estatus, s.df_fecha_solicitud, d.df_alta, " +
		  "       DECODE(                                            "  +
		  "           S.IC_ESTATUS_SOLIC,                            "  +
		  "           1, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (1) Seleccionada IF
		  "           2, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (2) En proceso
		  "           3, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (3) Operada
		  "           5, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (5) Operada Pagada
		  "          10, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (10) Operado con Fondeo Propio
		  "          'N/A'                                           "  + // PARA TODOS LOS OTROS CASOS
		  "       ) AS FECHA_AUTORIZACION_IF                         "  + // Fodea 040 - 2011 By JSHD 
        "  FROM com_documento d, " +
        "       comcat_pyme p, " +
        "       comrel_pyme_epo cpe, " +
        "       com_docto_seleccionado ds, " +
        "       com_solicitud s " +
        " WHERE d.ic_pyme = p.ic_pyme " +
        "   AND d.ic_epo = cpe.ic_epo " +
        "   AND d.ic_pyme = cpe.ic_pyme " +
        "   AND p.ic_pyme = cpe.ic_pyme " +
        "   AND d.ic_documento = ds.ic_documento(+) " +
        "   AND d.ic_documento = s.ic_documento(+) " +
        "   AND d.df_alta >= TO_DATE (?, 'dd/mm/yyyy') " +
        "   AND d.df_alta < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
        "   AND d.ic_epo = ? " +
        "   AND d.ic_estatus_docto NOT IN (?) " +
        "   AND d.ic_moneda = ? " +
        "UNION " +
        "SELECT /*+  LEADING(s) USE_NL (d, p, cpe, s, ds) */ " +
        "       p.ic_pyme num_pyme, p.cg_razon_social razon_social, " +
        "       d.ig_numero_docto numerodoc, d.ic_documento icdocto, " +
        "       TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') dfemision, " +
        "       TO_CHAR (d.df_alta, 'dd/mm/yyyy') dfpublicacion, " +
        "       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') dfvencimiento, " +
        "       d.cg_clave_presupuestaria clavepresupuestaria, " +
        "       ROUND (d.df_alta - df_fecha_docto) plazoregistro, " +
        "       ROUND (d.df_fecha_venc - d.df_alta) plazopago, ic_moneda moneda, " +
        "       TO_CHAR (d.df_entrega, 'dd/mm/yyyy') fechaentrega, " +
        "       d.cg_tipo_compra tipocompra, cg_periodo periodo, d.ic_epo epo, " +
        "       cpe.cs_aceptacion habilitada, 0 AS montopublicado, " +
        "       ds.in_importe_interes intereses, ds.in_importe_recibir montooperado, " +
        "       d.ic_estatus_docto estatus, s.df_fecha_solicitud, d.df_alta, " +
		  "       DECODE(                                            "  +
		  "           S.IC_ESTATUS_SOLIC,                            "  +
		  "           1, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (1) Seleccionada IF
		  "           2, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (2) En proceso
		  "           3, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (3) Operada
		  "           5, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (5) Operada Pagada
		  "          10, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (10) Operado con Fondeo Propio
		  "          'N/A'                                           "  + // PARA TODOS LOS OTROS CASOS
		  "       ) AS FECHA_AUTORIZACION_IF                         "  + // Fodea 040 - 2011 By JSHD
        "  FROM com_documento d, " +
        "       comcat_pyme p, " +
        "       comrel_pyme_epo cpe, " +
        "       com_docto_seleccionado ds, " +
        "       com_solicitud s " +
        " WHERE d.ic_pyme = p.ic_pyme " +
        "   AND d.ic_epo = cpe.ic_epo " +
        "   AND d.ic_pyme = cpe.ic_pyme " +
        "   AND p.ic_pyme = cpe.ic_pyme " +
        "   AND d.ic_documento = ds.ic_documento " +
        "   AND d.ic_documento = s.ic_documento " +
        "   AND s.df_fecha_solicitud >= TO_DATE (?, 'dd/mm/yyyy') " +
        "   AND s.df_fecha_solicitud < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
        "   AND d.ic_epo = ? " +
        "   AND d.ic_moneda = ? " +
        "   AND d.ic_documento NOT IN ( " +
        "          SELECT /*+ INDEX(d IN_COM_DOCUMENTO_10_NUK)*/ " +
        "                 ic_documento " +
        "            FROM com_documento " +
        "           WHERE df_alta >= TO_DATE (?, 'dd/mm/yyyy') " +
        "             AND df_alta < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
        "             AND ic_epo = ? " +
        "             AND ic_estatus_docto NOT IN (?) " +
        "             AND ic_moneda = ?) " +
        "   ORDER BY 2" ); 

    
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
      lVarBind.add(new Integer("5")); //BAJA
			lVarBind.add(new Integer("1"));//MONEDA NACIONAL
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("1"));//MONEDA NACIONAL
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
      lVarBind.add(new Integer("5")); //BAJA
			lVarBind.add(new Integer("1"));//MONEDA NACIONAL
      
			
			System.out.println("query:: " + qrySentencia);
			System.out.println("bind:: " + lVarBind);

			//**************************************MONEDA NACIONAL**********************************//
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			
			while(registros != null && registros.next() ){
				HashMap registro = new HashMap();
				registro.put("NUM_PYME",					registros.getString("NUM_PYME"));
				registro.put("RAZON_SOCIAL",				registros.getString("RAZON_SOCIAL"));
				registro.put("NUMERODOC",					registros.getString("NUMERODOC"));
				registro.put("ICDOCTO",						registros.getString("ICDOCTO"));
				registro.put("DFEMISION",					registros.getString("DFEMISION"));
				registro.put("DFPUBLICACION",				registros.getString("DFPUBLICACION"));
				registro.put("DFVENCIMIENTO",				registros.getString("DFVENCIMIENTO"));
				registro.put("CLAVEPRESUPUESTARIA",		registros.getString("CLAVEPRESUPUESTARIA"));
				registro.put("PLAZOREGISTRO",				registros.getString("PLAZOREGISTRO"));
				registro.put("PLAZOPAGO",					registros.getString("PLAZOPAGO"));
				registro.put("MONEDA",						registros.getString("MONEDA"));
				registro.put("FECHAENTREGA",				registros.getString("FECHAENTREGA"));
				registro.put("TIPOCOMPRA",					registros.getString("TIPOCOMPRA"));
				registro.put("PERIODO",						registros.getString("PERIODO"));
				registro.put("EPO",							registros.getString("EPO"));
				registro.put("HABILITADA",					registros.getString("HABILITADA"));
				registro.put("MONTOPUBLICADO",			Comunes.formatoDecimal(registros.getString("MONTOPUBLICADO"),2,true));
				//if(registros.getString("ESTATUS").equals("4")||registros.getString("ESTATUS").equals("11")){
        registro.put("INTERESES",				Comunes.formatoDecimal(registros.getString("INTERESES"), 2, true));
        registro.put("MONTOOPERADO",			Comunes.formatoDecimal(registros.getString("MONTOOPERADO"), 2, true));
				//}else{
				//	registro.put("INTERESES",				Comunes.formatoDecimal("", 2, true));
				//	registro.put("MONTOOPERADO",			Comunes.formatoDecimal("", 2, true));
				//}
				registro.put("FECHAAUTORIZACIONIF",		registros.getString("FECHA_AUTORIZACION_IF")); // Fodea 040 - 2011
				
				MonedaNacional.add(registro);
			}
			
			
			///////////////////////////////////TOTAL MN////////////////////////////////////////////
			
			qryTotalMN.append(
				" SELECT avg(round(d.df_alta - df_fecha_docto)) PLAZOREGISTRO, 	avg(round(d.df_fecha_venc - d.df_alta)) PLAZOPAGO, sum(d.fn_monto) MONTOPUBLICADO " + 
				" FROM com_documento d, comcat_pyme p, comrel_pyme_epo cpe, com_docto_seleccionado ds " + 
         ", com_solicitud sol "+ 
				" WHERE " +  
				"  d.ic_pyme=p.ic_pyme " + 
				"	AND d.ic_epo = cpe.ic_epo " +
				"	AND d.ic_pyme = cpe.ic_pyme " +
				"	AND p.ic_pyme = cpe.ic_pyme " +
				"	AND d.ic_documento = ds.ic_documento(+) " +		     
       " and sol.ic_documento = ds.ic_documento "+
       " AND sol.df_fecha_solicitud >= TO_DATE(?, 'dd/mm/yyyy') "+
       " AND sol.df_fecha_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1 "+    
			 " 	AND d.ic_epo = ? " + 
			 "	AND d.ic_moneda = ? "); 
			lVarBind.clear();
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("1"));//MONEDA NACIONAL
			
			System.out.println("query Total:: " + qryTotalMN.toString());
			
			registros = con.consultarDB(qryTotalMN.toString(), lVarBind, false);
			
			if (registros != null && registros.next()){
				TotalMonedaNacional.put("PLAZOREGISTRO",			Comunes.formatoDecimal(registros.getString("PLAZOREGISTRO"),2,true));
				TotalMonedaNacional.put("PLAZOPAGO",				Comunes.formatoDecimal(registros.getString("PLAZOPAGO"),2,true));
				TotalMonedaNacional.put("MONTOPUBLICADO",			Comunes.formatoDecimal(registros.getString("MONTOPUBLICADO"),2,true));
			}
			
			
			qryTotalMN2.append(" SELECT sum(ds.in_importe_interes) INTERESES, sum(ds.in_importe_recibir) MONTOOPERADO " + 
				" FROM com_documento d, comcat_pyme p, comrel_pyme_epo cpe, com_docto_seleccionado ds " + 
        ", com_solicitud sol "+ 
				" WHERE " +  
				"  d.ic_pyme=p.ic_pyme " + 
				"	AND d.ic_epo = cpe.ic_epo " +
				"	AND d.ic_pyme = cpe.ic_pyme " +
				"	AND p.ic_pyme = cpe.ic_pyme " +
				"	AND d.ic_documento = ds.ic_documento(+) " +	
        " and sol.ic_documento = ds.ic_documento "+
        " AND sol.df_fecha_solicitud >= TO_DATE(?, 'dd/mm/yyyy') "+
        " AND sol.df_fecha_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1 "+    
				" 	AND d.ic_epo = ? " + 
				"	AND d.ic_moneda = ? " + 
				"	AND d.ic_estatus_docto in(?,?)"); 
			lVarBind.clear();
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("1"));//MONEDA NACIONAL
			lVarBind.add(new Integer("4"));
			lVarBind.add(new Integer("11"));
			
			registros = con.consultarDB(qryTotalMN2.toString(), lVarBind, false);
			
			if (registros != null && registros.next()){
				TotalMonedaNacional.put("INTERESES",			Comunes.formatoDecimal(registros.getString("INTERESES"),2,true));
				TotalMonedaNacional.put("MONTOOPERADO",		Comunes.formatoDecimal(registros.getString("MONTOOPERADO"),2,true));
			}
				
			//**************************************DOLAR**********************************//
			lVarBind.clear();
			/*lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
      lVarBind.add(new Integer("5"));
			lVarBind.add(new Integer("54"));//DOLAR*/
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
      lVarBind.add(new Integer("5")); //BAJA
			lVarBind.add(new Integer("54"));//DOLAR
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("54"));//DOLAR
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
      lVarBind.add(new Integer("5")); //BAJA
			lVarBind.add(new Integer("54"));//DOLAR
      
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			
			while(registros != null && registros.next() ){
				HashMap registro = new HashMap();
				registro.put("NUM_PYME",					registros.getString("NUM_PYME"));
				registro.put("RAZON_SOCIAL",				registros.getString("RAZON_SOCIAL"));
				registro.put("NUMERODOC",					registros.getString("NUMERODOC"));
				registro.put("ICDOCTO",						registros.getString("ICDOCTO"));
				registro.put("DFEMISION",					registros.getString("DFEMISION"));
				registro.put("DFPUBLICACION",				registros.getString("DFPUBLICACION"));
				registro.put("DFVENCIMIENTO",				registros.getString("DFVENCIMIENTO"));
				registro.put("CLAVEPRESUPUESTARIA",		registros.getString("CLAVEPRESUPUESTARIA"));
				registro.put("PLAZOREGISTRO",				registros.getString("PLAZOREGISTRO"));
				registro.put("PLAZOPAGO",					registros.getString("PLAZOPAGO"));
				registro.put("MONEDA",						registros.getString("MONEDA"));
				registro.put("FECHAENTREGA",				registros.getString("FECHAENTREGA"));
				registro.put("TIPOCOMPRA",					registros.getString("TIPOCOMPRA"));
				registro.put("PERIODO",						registros.getString("PERIODO"));
				registro.put("EPO",							registros.getString("EPO"));
				registro.put("HABILITADA",					registros.getString("HABILITADA"));
				registro.put("MONTOPUBLICADO",			Comunes.formatoDecimal(registros.getString("MONTOPUBLICADO"),2,true));
				//if(registros.getString("ESTATUS").equals("4")||registros.getString("ESTATUS").equals("11")){
        registro.put("INTERESES",				Comunes.formatoDecimal(registros.getString("INTERESES"), 2, true));
        registro.put("MONTOOPERADO",			Comunes.formatoDecimal(registros.getString("MONTOOPERADO"), 2, true));
				//}else{
				//	registro.put("INTERESES",				Comunes.formatoDecimal("", 2, true));
				//	registro.put("MONTOOPERADO",			Comunes.formatoDecimal("", 2, true));
				//}
				registro.put("FECHAAUTORIZACIONIF",		registros.getString("FECHA_AUTORIZACION_IF")); // Fodea 040 - 2011
				
				Dolar.add(registro);
			}
			
			///////////////////////////////////TOTAL DOLAR////////////////////////////////////////////
			lVarBind.clear();
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("54"));//DOLAR

			registros = con.consultarDB(qryTotalMN.toString(), lVarBind, false);
			
			if (registros != null && registros.next()){
				TotalDolar.put("PLAZOREGISTRO",		Comunes.formatoDecimal(registros.getString("PLAZOREGISTRO"),2,true));
				TotalDolar.put("PLAZOPAGO",			Comunes.formatoDecimal(registros.getString("PLAZOPAGO"),2,true));
				TotalDolar.put("MONTOPUBLICADO",		Comunes.formatoDecimal(registros.getString("MONTOPUBLICADO"),2,true));
			}
			
			lVarBind.clear();
			lVarBind.add(df_publicacion_de);
			lVarBind.add(df_publicacion_a);
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("54"));//DOLAR
			lVarBind.add(new Integer("4"));
			lVarBind.add(new Integer("11"));
			
			registros = con.consultarDB(qryTotalMN2.toString(), lVarBind, false);
			
			if (registros != null && registros.next()){
				TotalDolar.put("INTERESES",			Comunes.formatoDecimal(registros.getString("INTERESES"),2,true));
				TotalDolar.put("MONTOOPERADO",		Comunes.formatoDecimal(registros.getString("MONTOOPERADO"),2,true));
			}
							
			//System.out.println("MONEDA NACIONAL:: " + MonedaNacional);
			//System.out.println("DOLAR:: " + Dolar);*/
			
			resultados.put("EPO", nombreEPO);
			resultados.put("MONEDANACIONAL", MonedaNacional);
			resultados.put("DOLAR", Dolar);
			resultados.put("TOTALMONEDANACIONAL", TotalMonedaNacional);
			resultados.put("TOTALDOLAR", TotalDolar);
				
		} catch(Exception e) {
			System.out.println("InformacionTOICBean::getResumenPymes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("InformacionTOICBean::getResumenPymes(S)");
		}	
		return resultados;
	}
	
	/**
	 * Metodo que indica si esta parametrizado algun valor en especifico para una EPO
	 * @throws com.netro.exception.NafinException
	 * @return regresa valor boolean que indica si existe el parametro
	 * @param sEpo - numero de  identificacion de la EPO
	 * @param numParametro - numero que indica el parametro a validar
	 */
	public boolean getExisteParametroEpo(String numParametro, String sEpo) throws NafinException {
		
		System.out.println("getExisteParametroEpo(E)");
		
		boolean resultado = false;
		AccesoDB con		= new AccesoDB();
		ResultSet rs		= null;
		PreparedStatement ps	= null;
		
		
		try{
			con.conexionDB();
			
			if(numParametro.equals("12")){
				String Query = "SELECT COUNT (1) numregs " +
					"  FROM comrel_producto_epo " +
					"  where ic_epo = ? " +
					" and cs_publicacion_epo_pef = ? ";
	
				ps = con.queryPrecompilado(Query);
				ps.setInt(1,Integer.parseInt(sEpo));
				ps.setString(2,"S");
				
				System.out.println("query : "+Query);
				System.out.println("Bind1 : "+sEpo+", S");
				
				rs = ps.executeQuery();
				
				if(rs.next()){			
					int regs = Integer.parseInt(rs.getString("numregs"));				
					if(regs>0){				
						resultado = true;			
					}
				}		
			
				rs.close();
				ps.close();
			}
		
			return resultado;
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getExisteParametroEpo(S)");
		}
	}
	//*************************************************************************** FODEA 034 - 2009 ACF
	/**
	 * Este m�todo obtiene el n�mero con el que se identifica el proceso de carga de documentos SIAFF
	 * en curso, ya sea manual o autom�tica.
	 * @return numero_proceso Cadena con el n�mero que se asignar� al proceso de carga.
	 * @throws NafinException Cuando ocurre un error al generar el n�mero de proceso.
	 */
	public int getNumeroProcesoCargaSIAFF() throws NafinException{
		log.info("getNumeroProcesoCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		int numero_proceso = 0;
		try{
			con.conexionDB();
		
			strSQL.append(" SELECT SEQ_COMTMP_DET_CARGA_SIAFF.NEXTVAL FROM DUAL");
		
			pst = con.queryPrecompilado(strSQL.toString());
		
			rst = pst.executeQuery();
		
			while(rst.next()){numero_proceso = rst.getInt(1);}
		
			rst.close();
			pst.close();
		}catch(Exception e){
			log.info("getNumeroProcesoCargaSIAFF(ERROR) : Ocurrio un error al generar el numero de proceso");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getNumeroProcesoCargaSIAFF(F)");
		}
		return numero_proceso;
	}
	
	/**
	 * Este m�todo obtiene el n�mero con el que se identifica la carga de documentos SIAFF.
	 * @return numero_carga_siaff Cadena con el n�mero que identifica la carga del reporte SIAFF.
	 * @throws NafinException Cuando ocurre un error al generar el n�mero de proceso.
	 */
	public int getNumeroCargaSIAFF() throws NafinException{
		log.info("getNumeroCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		int numero_carga_siaff = 0;
		try{
			con.conexionDB();
		
			strSQL.append(" SELECT SEQ_COM_RES_CARGA_SIAFF.NEXTVAL FROM DUAL");
		
			pst = con.queryPrecompilado(strSQL.toString());
		
			rst = pst.executeQuery();
		
			while(rst.next()){numero_carga_siaff = rst.getInt(1);}
		
			rst.close();
			pst.close();
		}catch(Exception e){
			log.info("getNumeroCargaSIAFF(ERROR) : Ocurrio un error al generar el numero de carga SIAFF.");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getNumeroCargaSIAFF(F)");
		}
		return numero_carga_siaff;
	}	
	
	/**
	 * Este m�todo obtiene el n�mero con el que se identifica el proceso de carga de documentos SIAFF
	 * en curso, ya sea manual o autom�tica.
	 * @return numero_proceso Cadena con el n�mero que se asignar� al proceso de carga.
	 * @throws NafinException Cuando ocurre un error al generar el n�mero de proceso.
	 */
	public List getRegistrosErroneosCargaSIAFF(int ic_carga_siaff) throws NafinException{
		log.info("getRegistrosErroneosCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List registros_erroneos = new ArrayList();
		
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT cg_error_det");
			strSQL.append(" FROM com_rechazo_carga_siaff");
			strSQL.append(" WHERE ic_carga_siaff = ?");
			strSQL.append(" ORDER BY ig_linea");
			
			varBind.add(new Integer(ic_carga_siaff));
		
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);
		
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
		
			while(rst.next()){
				registros_erroneos.add(rst.getString(1) + "\n");
			}
		
			rst.close();
			pst.close();
		}catch(Exception e){
			log.info("ERROR: Ocurrio un error al generar el obtener los registros con errores de la carga SIAFF");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getRegistrosErroneosCargaSIAFF(F)");
		}
		return registros_erroneos;
	}
	
	/**
	 * Este m�todo obtiene una bandera que indica si en la carga de documentos SIAFF
	 * se encontraron registros con errores.
	 * @param ic_carga_siaff Cadena con el n�mero que se identifica la carga del reporte SIAFF.
	 * @return bandera_errores false si no existen errores, true en caso contrario.
	 * @throws NafinException Cuando ocurre un error al generar el n�mero de proceso.
	 */
	public boolean existenErroresCargaSIAFF(int ic_carga_siaff) throws NafinException{
		log.info("existenErroresCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		int errores = 0;
		boolean bandera_errores = false;
		
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT COUNT(1) errores");
			strSQL.append(" FROM com_rechazo_carga_siaff");
			strSQL.append(" WHERE ic_carga_siaff = ?");
			
			varBind.add(new Integer(ic_carga_siaff));
		
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);
		
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
		
			while(rst.next()){errores = rst.getInt(1);}
		
			rst.close();
			pst.close();
			
			if(errores == 0){bandera_errores = false;}else{bandera_errores = true;}
			
		}catch(Exception e){
			log.info("ERROR: Ocurrio un error al consultar errores en carga SIAFF");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("existenErroresCargaSIAFF(F)");
		}
		return bandera_errores;
	}

	/**
	 * Este m�todo obtiene una bandear que indica si en la carga de documentos SIAFF
	 * se encontraron registros con errores.
	 * @param ic_carga_siaff Cadena con el n�mero que se identifica la carga del reporte SIAFF.
	 * @throws NafinException Cuando ocurre un error al generar el n�mero de proceso.
	 */
	public void eliminarCargaSIAFF(int ic_carga_siaff) throws NafinException{
		log.info("eliminarCargaSIAFF(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_ok = true;
		
		try{
			con.conexionDB();

			strSQL.append(" DELETE com_detalle_carga_siaff");
			strSQL.append(" WHERE ic_carga_siaff = ?");

			varBind.add(new Integer(ic_carga_siaff));
		
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE com_resumen_carga_siaff");
			strSQL.append(" WHERE ic_carga_siaff = ?");

			varBind.add(new Integer(ic_carga_siaff));
		
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE com_rechazo_carga_siaff");
			strSQL.append(" WHERE ic_carga_siaff = ?");
			
			varBind.add(new Integer(ic_carga_siaff));
		
			log.debug("strSQL : "+strSQL.toString());
			log.debug("varBind : "+varBind);
		
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			
		}catch(Exception e){
			log.info("ERROR: Ocurrio un error al eliminar la carga SIAFF");
			trans_ok = false;
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_ok);
				con.cierraConexionDB();
			}
			log.info("eliminarCargaSIAFF(F)");
		}
	}
	//*************************************************************************** FODEA 034 - 2009 ACF
	/**
	 * metodo que regresa e que tipo de Epo 
	 * 1 PEF
	 * 2 GOBIERNOS Y MUNICIPIOS
	 * 3 PRIVADAS
	 * 4 PRUEBA
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param sEpo
	 */
		public String getTipoEpo(String sEpo) throws NafinException {
		
		System.out.println("getTipoEpo(E)");
		
		String resultado = "";
		AccesoDB con		= new AccesoDB();
		ResultSet rs		= null;
		PreparedStatement ps	= null;
		StringBuffer 	qrySentencia	= new StringBuffer();
		
		
		try{
			con.conexionDB();

				qrySentencia.append( " select ic_tipo_epo ");
				qrySentencia.append( " from comcat_epo ");
				qrySentencia.append( " where ic_epo = ? ");
		
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,Integer.parseInt(sEpo));
				
				log.debug ("qrySentencia : "+qrySentencia.toString());
				log.debug("sEpo   "+sEpo);
				
				rs = ps.executeQuery();
				
				if(rs.next()){			
					resultado = rs.getString("ic_tipo_epo")==null?"":rs.getString("ic_tipo_epo");				
				}	
			
				rs.close();
				ps.close();
			
		
			return resultado;
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getExisteParametroEpo(S)");
		}
	}
	
	/**
	 * 
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param informacion
	 */
	
	public List getInfoDiversa(List informacion) throws NafinException {
		
		System.out.println("getInfoDiversa(E)");
		
		List datos = new ArrayList();
		AccesoDB con		= new AccesoDB();
		ResultSet rs		= null;
		PreparedStatement ps	= null;
		StringBuffer 	qrySentencia	= new StringBuffer();
		String cadena ="",  contenido =""; 
		
		String strTipoUsuario = (String)informacion.get(0);
		String CadenasEpos = (String)informacion.get(1);
		String iNoEPO = (String)informacion.get(2);
		String iNoCliente  = (String)informacion.get(3);
		String publicacion = (String)informacion.get(4);

		try{
			con.conexionDB();

		if(strTipoUsuario.equals("NAFIN")) {
				cadena = CadenasEpos;
				qrySentencia.append( "select tt_contenido,ci_imagen,cg_archivo ");
				qrySentencia.append( " from com_publicacion ");
				qrySentencia.append( " where ic_publicacion = ?  and ic_epo = ? ");
							
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,Integer.parseInt(publicacion));
				ps.setInt(2,Integer.parseInt(CadenasEpos));				
			
			} else {
				cadena = iNoEPO;
				qrySentencia.append(" select pub.tt_contenido, pub.ci_imagen, pub.cg_archivo " );
				qrySentencia.append(" from com_publicacion pub " );
				qrySentencia.append(" , comrel_publicacion_usuario usr " );
				qrySentencia.append(" , comrel_nafin cn " );
				qrySentencia.append(" where pub.ic_publicacion = ?  and pub.ic_epo = ? "); 
				qrySentencia.append("	and usr.ic_publicacion = pub.ic_publicacion " );
				qrySentencia.append(" 	and usr.ic_nafin_electronico = cn.ic_nafin_electronico" );
				qrySentencia.append("	and cn.cg_tipo = 'P'" );
				qrySentencia.append("	and cn.ic_epo_pyme_if = ? " );
				
				System.out.println("qrySentencia.toString()   "+qrySentencia.toString());
				
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,Integer.parseInt(publicacion));
				ps.setInt(2,Integer.parseInt(iNoEPO));
				ps.setInt(3,Integer.parseInt(iNoCliente));
		}
		
		
		rs = ps.executeQuery();	
		if(rs.next()) {
			Clob clob;
			clob = rs.getClob(1);
			if (clob != null) 	contenido = clob.getSubString(1,(int)clob.length());
			String imagen  =rs.getString(2);
			String rutaImagen ="";
			if(imagen !=null) {  rutaImagen = "15archcadenas/pub_epo"+cadena+"/imagenes/"+ imagen; 	}
			String archivo =  rs.getString(3);
			String rutaArchivo = "";
			if(archivo !=null) { 		 rutaArchivo = "15archcadenas/pub_epo" + cadena + "/archivos/" + archivo; 	}	
			datos.add(contenido);
			datos.add(rutaImagen);
			datos.add(rutaArchivo);
		}
		rs.close();
		ps.close();
			
		
		return datos;
		
		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("getInfoDiversa(S)");
		}
	}
	/**
	 * Metodo que devuelve una cadena con la cual se generar� un archivo plano para
	 * el reporte SIAFF
	 * @return String con datos para generacion de archivo plano
	 * @param strTipoUsuario Tipo de usuario
	 * @param iNoCliente Clave EPO
	 * @param fini Clave Fecha de carga inicial introducida 
	 * @param ffin Clave Fecha de carga final introducida 
	 * @param cargaSiaff Clave de carga SIAFF
	 * @param flag Bandera de tipo de registros a consultar
	 */
	public String getArchivoPlanoSIAFF(String strTipoUsuario, String iNoCliente, String fini, String ffin, String cargaSiaff, String flag){
		log.info("getArchivoPlanoSIAFF (E)");
		AccesoDB con = new AccesoDB();
		StringBuffer contenidoArchivo = new StringBuffer();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String ramo_siaff = "";
		String unidad_siaff = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			con.conexionDB();
			if(!flag.equals("E")){
				if(strTipoUsuario.equals("EPO")){
					strSQL.append(" SELECT cc_ramo_siaff, cc_unidad_siaff");
					strSQL.append(" FROM comcat_epo");
					strSQL.append(" WHERE ic_epo = ?");
				
					ps = con.queryPrecompilado(strSQL.toString());
					ps.setInt(1,Integer.parseInt(iNoCliente));
					rs = ps.executeQuery();
				
					if(rs.next()){
						ramo_siaff =(rs.getString("cc_ramo_siaff")!=null)?(rs.getString("cc_ramo_siaff")):"";
						unidad_siaff =(rs.getString("cc_unidad_siaff")!=null)?(rs.getString("cc_unidad_siaff")):"";
					}
					rs.close();
					ps.close();
				}
				strSQL = new StringBuffer();
				varBind = new ArrayList();
		
				strSQL.append(" SELECT cc_ramo_siaff,");
				strSQL.append(" cc_unidad_siaff,");
				strSQL.append(" cg_folio_clc,");
				strSQL.append(" cg_folio_dependencia,");
				strSQL.append(" TO_CHAR(df_alta_clc, 'dd/mm/yyyy') df_alta_clc,");
				strSQL.append(" ig_digito_identificador,");
				strSQL.append(" cg_rfc_beneficiario,");
				strSQL.append(" cg_nombre_beneficiario,");
				strSQL.append(" cg_divisa,");
				strSQL.append(" ig_ogto,");
				strSQL.append(" TO_CHAR(df_pago, 'dd/mm/yyyy') df_pago,");
				strSQL.append(" cg_estatus,");
				strSQL.append(" fg_importe,");
				strSQL.append(" cs_registrado_en_cadenas");
				strSQL.append(" FROM com_detalle_carga_siaff cds");
				strSQL.append(", com_resumen_carga_siaff crs");
				strSQL.append(" WHERE cds.ic_carga_siaff = crs.ic_carga_siaff");
				strSQL.append(" AND crs.ic_carga_siaff = ?");
				
				varBind.add(new Long(cargaSiaff));
				
				if(strTipoUsuario.equals("EPO")){
					if(iNoCliente!=null && !iNoCliente.equals("")){
						strSQL.append(" AND cds.cc_ramo_siaff = ?");
						strSQL.append(" AND cds.cc_unidad_siaff = ?");
						
						varBind.add(ramo_siaff);
						varBind.add(unidad_siaff);
					}
				}	
				if(!flag.equals("T")){
					strSQL.append(" AND cds.cs_registrado_en_cadenas = ?");
					varBind.add(flag);
				}
				if(!fini.equals("")){
					strSQL.append(" AND crs.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
					varBind.add(fini);
				}
				if(!ffin.equals("")){
					strSQL.append(" AND crs.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
					varBind.add(ffin);
				}
				strSQL.append(" ORDER BY cds.cc_ramo_siaff, cc_unidad_siaff");
				
				log.info("..:: strSQL : " + strSQL.toString());
				log.info("..:: varBind : " + varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
		
				rs = ps.executeQuery();
				//Formatear lineas con errores
				while(rs != null && rs.next()) {
					// Extraer registros
					contenidoArchivo.append((rs.getString("cc_ramo_siaff")!=null)?rs.getString("cc_ramo_siaff"):""); 
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cc_unidad_siaff")!=null)?rs.getString("cc_unidad_siaff"):""); 
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_folio_clc")!=null)?rs.getString("cg_folio_clc"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_folio_dependencia")!=null)?rs.getString("cg_folio_dependencia"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("df_alta_clc")!=null)?rs.getString("df_alta_clc"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("ig_digito_identificador")!=null)?rs.getString("ig_digito_identificador"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_rfc_beneficiario")!=null)?rs.getString("cg_rfc_beneficiario"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_nombre_beneficiario")!=null)?rs.getString("cg_nombre_beneficiario"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_rfc_beneficiario")!=null)?rs.getString("cg_rfc_beneficiario"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_divisa")!=null)?rs.getString("cg_divisa"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("ig_ogto")!=null)?rs.getString("ig_ogto"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("df_pago")!=null)?rs.getString("df_pago"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("cg_estatus")!=null)?rs.getString("cg_estatus"):"");
					contenidoArchivo.append("|");
					contenidoArchivo.append((rs.getString("fg_importe")!=null)?rs.getString("fg_importe"):"");
					contenidoArchivo.append("|");
					// Agregar registro a archivo con errores
					contenidoArchivo.append("\n");
				}					
			}else{
				strSQL.append(" SELECT ig_linea, cg_error_det");
				strSQL.append(" FROM com_rechazo_carga_siaff");
				strSQL.append(" WHERE ic_carga_siaff = ?");
				strSQL.append(" ORDER BY ig_linea");
				
				varBind.add(new Long(cargaSiaff));
				
				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind);
		
				ps = con.queryPrecompilado(strSQL.toString(), varBind);
		
				rs = ps.executeQuery();
	
				while(rs.next()){
					contenidoArchivo.append((rs.getString("cg_error_det")!=null)?(rs.getString("cg_error_det")):"");
					contenidoArchivo.append("\n");
				}
			}
			rs.close();
			ps.close();
		}catch(Exception e) { 
			log.error("getArchivoPlanoSIAFF(Exception) "+e);
			throw new AppException("Error al obtener la cadena para archivo plano.",e);
		} finally { 
			if(con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getArchivoPlanoSIAFF (S)");
		}
		return contenidoArchivo.toString();
	}

	 /**
	 * Migraci�n Nafin 
	 * Pantalla de Captura del Reporte SIAFF/Captura del Reporte SIAFF
	 * @return 
	 * @param rutaArchivo
	 */
	public String leeArchivo(String rutaArchivo)	{
		String respuesta ="S", linea = "";
		int totaldoctos	= 0;		
		int numMaxRegistros	= 20000;
		
		try {
		
			java.io.File ft = new java.io.File(rutaArchivo);
			BufferedReader brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
		
			while((linea=brt.readLine())!=null){ totaldoctos++; }
		
			if( totaldoctos > numMaxRegistros  || totaldoctos == 0 ){			
				if(totaldoctos == 0) { 
					respuesta = "El archivo proporcionado est� vac�o.";
				}else{ 
					respuesta ="El archivo proporcionado tiene "+totaldoctos+" registros, por lo que excede el l�mitede registros permitidos, que es  "+numMaxRegistros+"  registros.";
				}
			}
		
		} catch(Exception e) {
			throw new AppException("Error al leer cargar de archivo", e);
		} finally {
			
		}		
		return respuesta;
	}//fin metodo
	
	/** Migraci�n Nafin
	 * Pantalla de Captura del Reporte SIAFF/Captura del Reporte SIAFF
	 * @return 
	 * @param numeroProceso
	 */
	public HashMap validaArchivo(int  numeroProceso)	{
		AccesoDB con =new AccesoDB();
		HashMap registros = new HashMap();
		String existError ="0";
		int contador =0;
		List lstConReg = new ArrayList();
		List lstSinReg = new ArrayList();
		List lstExistReg = new ArrayList();
		List lstLinea = new ArrayList();
		List lstError = new ArrayList();
		String btnProcesar ="N", conregistro = "", sinReg ="", dupReg = "", conregistroError ="";
		ResultSet rs = null;
		
		try {
			con.conexionDB();
			
			String qrySentencia=" select count(1) from COMTMP_DETALLE_CARGA_SIAFF " +
							" where ig_num_proceso = "+numeroProceso+" "+
							" and cg_error_reg is not null ";
							
			rs = con.queryDB(qrySentencia);
			 while(rs.next()){
				existError = rs.getString(1);				
			 }
			 rs.close();
			
			if(existError.equals("0")){
			
				String qryPagosReg = " select 'Linea '||ig_linea_reg numlinea, cs_registrado_en_cadenas registrado from " +
									" COMTMP_DETALLE_CARGA_SIAFF " +
									" where ig_num_proceso = "+numeroProceso+
									" and cs_registrado_en_cadenas in ('N','S','E') "+
									" and cg_error_reg is null "+
									" order by ig_linea_reg ";
				rs = con.queryDB(qryPagosReg);
	
				while(rs.next()){
					contador++;
					if(rs.getString("registrado").equals("S"))
						lstConReg.add(rs.getString("numlinea"));
					if(rs.getString("registrado").equals("N"))
						lstSinReg.add(rs.getString("numlinea"));
					if(rs.getString("registrado").equals("E"))
						lstExistReg.add(rs.getString("numlinea"));
				}
				rs.close();
				
			}else  {
			
				String qryPagosReg = " select 'Linea '||ig_linea_reg numlinea, cg_error_reg  errores from " +
										" COMTMP_DETALLE_CARGA_SIAFF " +
										" where ig_num_proceso = "+numeroProceso+
										" and cg_error_reg is not null ";
				rs = con.queryDB(qryPagosReg);
		
				while(rs.next()){
					contador++;
					lstLinea.add(rs.getString("numlinea"));
					lstError.add(rs.getString("errores"));
				}
				rs.close();
					
			}
			
			//*************************+
			if((lstConReg!=null && lstConReg.size()>0) || (lstSinReg!=null && lstSinReg.size()>0)){
				btnProcesar = "S";
			}
		
			// CONDICION QUE OBTIENE LOS PAGOS CON REGISTRO
			if(lstConReg!=null && lstConReg.size()>0){
				for(int x =0; x< lstConReg.size(); x++){
					conregistro += (String)lstConReg.get(x)+"\n";
				}
			}
			// CONDICION QUE OBTIENE LOS PAGOS SIN REGISTRO
			if(lstSinReg!=null && lstSinReg.size()>0){
				for(int x =0; x< lstSinReg.size(); x++){
					sinReg += (String)lstSinReg.get(x)+"\n";
				}
			}
				
			// CONDICION QUE OBTIENE LOS PAGOS CON REGISTRO
			if(lstExistReg!=null && lstExistReg.size()>0){
				for(int x =0; x< lstExistReg.size(); x++){
					dupReg += (String)lstExistReg.get(x)+"\n";
				}
			}
			//CONDICION PARA ARCHIVO CON ERRORES
			if(lstLinea!=null && lstLinea.size()>0){								
				for(int x =0; x< lstLinea.size(); x++){
					conregistroError += (String)lstLinea.get(x)+"\n"+(String)lstError.get(x)+"\n";
				}
			}		
		
			registros = new HashMap();	
			registros.put("BOTON_PROCESAR",btnProcesar);
			registros.put("CON_REGISTRO",conregistro);
			registros.put("SIN_REGISTRO",sinReg);
			registros.put("ERRORES",conregistroError);
			registros.put("DUPLICADOS",dupReg);
			registros.put("TOTAL_REGISTROS",String.valueOf(contador));
			
			
		
		} catch(Exception e) {
			throw new AppException("Error al cargar archivo", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}		
		return registros;
	}//fin metodo


	
	
}