package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class IMSSEnlOperados extends EpoPEF implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

	public IMSSEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "com_doctos_ope_imss";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (4,5,6,7,9,16)";
	}
	public String getBorraDoctosVenc(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (9)";
	}
	public String getBorraDoctosBaja(){
		return "delete "+getTablaDoctosOpe()+" where ic_estatus_docto in (5,6,7)";
	}

	public String getInsertaDoctosOpe(){
		return "insert into "+getTablaDoctosOpe()+" (ic_documento, ic_estatus_docto,"+
				" df_fecha_venc, cg_razon_social_pyme, ig_numero_docto, ic_moneda, fn_monto,"+
				" in_numero_proveedor, df_fecha_docto, cc_acuse, cg_razon_social_if,"+
				" ic_nafin_electronico, cg_rfc" + getCamposPEFOper()+") ";
	}


	public String getDoctosOperados(Hashtable alParamEPO){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    
		return	"select d.ic_documento, 4 as ic_estatus_docto, d.df_fecha_venc,"+
						" substr(p.cg_razon_social,1,70) as nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,  "+
						" pe.cg_pyme_epo_interno as numeroProveedor, d.df_fecha_docto as fechaEmision,"+
						" a.cc_acuse, substr(i.cg_razon_social,1,50) as nombreIf,"+
						" TO_CHAR(rn.ic_nafin_electronico) as ic_nafin_electronico, "+
						" substr(d.cg_campo1,1,16) as cg_rfc "+
            (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
            (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
						" from com_documento d, com_docto_seleccionado ds, com_solicitud s,"+
						" com_acuse2 a, comcat_if i, comrel_pyme_epo pe, comcat_pyme p, comrel_nafin rn"+
						" where a.cc_acuse = ds.cc_acuse"+
						" and s.ic_documento = ds.ic_documento"+
						" and ds.ic_documento = d.ic_documento"+
						" and ds.ic_if = rn.ic_epo_pyme_if"+
						" and d.ic_if = i.ic_if"+
						" and d.ic_pyme = pe.ic_pyme"+
						" and d.ic_epo = pe.ic_epo"+
						" and d.ic_pyme = p.ic_pyme"+
						" and d.ic_epo = "+ic_epo+
						" and d.ic_estatus_docto in (4,16) "+
						" and s.df_fecha_solicitud >= TRUNC(SYSDATE)  AND s.df_fecha_solicitud < TRUNC(SYSDATE) + 1 "+		
						" and rn.cg_tipo = 'I'";
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
    
			return " SELECT d.ic_documento, d.ic_estatus_docto, d.df_fecha_venc,"   +
				"  SUBSTR(p.cg_razon_social,1,70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
				"  pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
				"  NULL AS acuse,NULL AS IF,NULL AS nafin_electronico,"   +
				"  SUBSTR(d.cg_campo4,1,16) AS cg_rfc"  +
        (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
        (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
				" from com_documento d, comhis_cambio_estatus ce, comrel_pyme_epo pe "+
				" ,  comcat_pyme p " +
				" where d.ic_documento = ce.ic_documento "+
				" and d.ic_pyme = pe.ic_pyme "+
				" and d.ic_epo = pe.ic_epo "+
				" and d.ic_pyme = p.ic_pyme"+				
				" and ce.dc_fecha_cambio >= TRUNC(SYSDATE)  AND ce.dc_fecha_cambio < TRUNC(SYSDATE) + 1 "+	
				" and d.ic_estatus_docto in (5,6,7) "+
				" and ce.ic_cambio_estatus in (4,5,6) "+
				" and d.ic_epo = "+ic_epo;
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){

    String sPubEPOPEFFlagVal						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN LOS DEMAS CAMPOS
    String sDigitoIdentFlagVal					= alParamEPO.get("PUBLICACION_EPO_PEF").toString(); // ESTE ES EL PARAMETRO QUE DETERMINA SI SE UTILIZAN EL DIGITO IDENTIFICADOR
		String	fechaHoy	 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	diaAct	= fechaHoy.substring(0, 2);
		String	mesAct	= fechaHoy.substring(3, 5);
		String	anyoAct	= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyoAct), Integer.parseInt(mesAct)-1, Integer.parseInt(diaAct));

		String condicion = "";
		//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+

		if(cal.get(Calendar.DAY_OF_WEEK)==Calendar.MONDAY) {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-3)";
		} else {
			condicion =
				" and d.df_fecha_venc >= TRUNC(SYSDATE+"+diasMinimos+"-1)";
		}
		String qrySentencia = " SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"   +
					"  SUBSTR(p.cg_razon_social,1,70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
					"  pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
					"  NULL AS acuse, NULL AS razonsocialif,NULL AS ic_nafin_electronico,"   +
					"  SUBSTR(d.cg_campo4,1,16) AS cg_rfc"  +
          (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
          (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
					" from com_documento d, comcat_pyme p, comrel_pyme_epo pe "+					
					" where d.ic_pyme = p.ic_pyme "+
					" and d.ic_pyme = pe.ic_pyme "+
					" and d.ic_epo = pe.ic_epo "+
					" and d.ic_pyme = p.ic_pyme"+
					" and d.ic_estatus_docto = 9 "+
					condicion+
					" and d.df_fecha_venc < TRUNC(SYSDATE+"+diasMinimos+")"+
					//" and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy')+"+diasMinimos+"-1"+
					" and d.ic_epo = "+ic_epo+
					//FODEA 006-Feb2010 Rebos - Se agrego la notificación de documentos publicados como vencidos
					" UNION " +
					" SELECT d.ic_documento, 9 AS ic_estatus_docto, d.df_fecha_venc,"   +
					" SUBSTR(p.cg_razon_social,1,70) AS nombrePyme, d.ig_numero_docto, d.ic_moneda, d.fn_monto,"   +
					" pe.cg_pyme_epo_interno AS numeroProveedor, d.df_fecha_docto AS fechaEmision,"   +
					" NULL AS acuse, NULL AS razonsocialif, NULL AS ic_nafin_electronico,"   +
					" SUBSTR(d.cg_campo4,1,16) AS cg_rfc"  +
          (("S".equals(sPubEPOPEFFlagVal))?getCamposPEF():getCamposPEFFake())+
          (("S".equals(sDigitoIdentFlagVal))?","+getCamposDigitoIdentificadorCalc():","+getCamposDigitoIdentificadorFake())+
					" from com_documento d, comcat_pyme p, comrel_pyme_epo pe "+
					" where d.ic_pyme = p.ic_pyme "+
					" and d.ic_pyme = pe.ic_pyme "+
					" and d.ic_epo = pe.ic_epo "+
					" and d.ic_pyme = p.ic_pyme"+
					" and d.ic_estatus_docto = 9 "+
					" and d.df_alta > TRUNC(SYSDATE-1)"+
					" and d.ic_epo = "+ic_epo;					
					
		return qrySentencia;
	}

}