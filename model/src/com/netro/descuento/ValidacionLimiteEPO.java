package com.netro.descuento;
import java.math.BigDecimal;

public class ValidacionLimiteEPO implements java.io.Serializable {
	private boolean sobregiro;
	private String montoSobregiro;
	private BigDecimal montoSinOperar;
	private String nombreEPO;
	private String totalDoctos;
	private String montoDoctos; 
	private String tipoMoneda;

	public ValidacionLimiteEPO() {
	}

	public boolean getSobregiro() {
		return sobregiro;
	}

	public void setSobregiro(boolean sobregiro) {
		this.sobregiro = sobregiro;
	}

	public String getMontoSobregiro() {
		return montoSobregiro;
	}

	public void setMontoSobregiro(String montoSobregiro) {
		this.montoSobregiro = montoSobregiro;
	}

	public BigDecimal getMontoSinOperar() {
		return montoSinOperar;
	}

	public void setMontoSinOperar(BigDecimal montoSinOperar) {
		this.montoSinOperar = montoSinOperar;
	}

	public void setNombreEPO(String nombreEPO) {
		this.nombreEPO = nombreEPO;
	}

	public String getNombreEPO() {
		return nombreEPO;
	}

	public String getTotalDoctos() {
		return totalDoctos;
	}

	public void setTotalDoctos(String totalDoctos) {
		this.totalDoctos = totalDoctos;
	}

	public String getMontoDoctos() {
		return montoDoctos;
	}

	public void setMontoDoctos(String montoDoctos) {
		this.montoDoctos = montoDoctos;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
}