package com.netro.descuento;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		ArmaQueryFile.java
*
*	Proposito:	Clase ArmaQueryFile
*
*	Lenguaje:	Java
*
*	Autor:		Salvador Saldivar Barajas
*
*	Fecha Creacion:	21/09/2006
*
****************************************************************************************************************/

import netropology.utilerias.*;
import java.util.*;
import java.io.*;

public class ArmaQueryFile{
	StringBuffer  qrySentencia = null;
	String condicion = "";
	
	public void ArmaQueryFile(){
	}
	
/**
 * Arma cadena con query para vencimientos
 *@ author Salvador Saldivar Barajas
 *@ since 21 de septiembre de 2006
 *@ param ic_if Variable que indentifica al cliente
 *@ param cliente Variable que contiene nombre del cliente
 *@ param prestamo Variable que indica el tipo de prestamo
 *@ param Fecha_Corte Variable que indica la fecha de corte
 *@ param moneda Variable que indica el tipo de moneda
 *@ return regresa cadena con el query armado
 */	
	
	public String regresaQueryVenc(String ic_if,String cliente,String prestamo,String FechaCorte,String moneda){
		try{
			qrySentencia = new StringBuffer();
			condicion = "";
			qrySentencia.append(        
			"select " +
			"    /*+ " +
			"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
			"    */  " +
			" to_char(v.com_fechaprobablepago,'dd/mm/yyyy') as FechaProbablePago   "   +
			" ,v.CG_MODALIDADPAGO as ModalidadPago       "   +
			" ,v.IC_MONEDA as CodMoneda           "   +
			" ,m.cd_nombre as DescMoneda          "   +
			" ,v.ic_if as Intermediario       "   +
			" ,i.cg_razon_social as DescIntermediario   "   +
			" ,v.IG_BASEOPERACION as BaseOperacion       "   +
			" ,v.cg_descbaseoper as DescBaseOper        "   +
			" ,v.ig_cliente as Cliente             "   +
			" ,v.cg_nombrecliente as Nombre              "   +
			" ,v.ig_sucursal as Sucursal            "   +
			" ,v.ig_proyecto as Proyecto            "   +
			" ,v.IG_CONTRATO as Contrato            "   +
			" ,v.IG_PRESTAMO as Prestamo            "   +
			" ,v.IG_NUMELECTRONICO as NumElectronico      "   +
			" ,v.CG_BURSATILIZADO as Bursatilizado       "   +
			" ,v.IG_DISPOSICION as Disposicion         "   +
			" ,v.IG_SUBAPLICACION as SubAplicacion       "   +
			" ,v.IG_CUOTASEMIT as CuotasEmit          "   +
			" ,v.IG_CUOTASPORVENC as CuotasPorVenc       "   +
			" ,v.IG_TOTALCUOTAS as TotalCuotas         "   +
			" ,v.CG_ANIOMODALIDAD as AnioModalidad       "   +
			" ,v.IG_TIPOESTRATO as TipoEstrato         "   +
			" ,to_char(v.DF_FECHAOPERA,'dd/mm/yyyy') as FechaOpera          "   +
			" ,v.IG_SANCIONADO as Sancionado          "   +
			" ,v.IG_FRECUENCIACAPITAL as FrecuenciaCapital   "   +
			" ,v.IG_FRECUENCIAINTERES as FrecuenciaInteres   "   +
			" ,v.IG_TASABASE as TasaBase            "   +
			" ,v.CG_ESQUEMATASAS as EsquemaTasas        "   +
			" ,v.CG_RELMAT_1 as RelMat_1            "   +
			" ,v.FG_SPREAD_1 as Spread_1            "   +
			" ,v.CG_RELMAT_2 as RelMat_2            "   +
			" ,v.FG_SPREAD_2 as Spread_2            "   +
			" ,v.CG_RELMAT_3 as RelMat_3            "   +
			" ,v.FG_SPREAD_3 as Spread_3            "   +
			" ,v.CG_RELMAT_4 as RelMat_4            "   +
			" ,v.FG_MARGEN as Margen              "   +
			" ,v.IG_TIPOGARANTIA as TipoGarantia        "   +
			" ,v.FG_PORCDESCFOP as PorcDescFop         "   +
			" ,v.FG_TOTDESCFOP as TotDescFop          "   +
			" ,v.FG_PORCDESCFINAPE as PorcDescFinape      "   +
			" ,v.FG_TOTDESCFINAPE as TotDescFinape       "   +
			" ,v.FG_FCRECIMIENTO as FCrecimiento        "   +
			" ,to_char(v.DF_PERIODOINIC,'dd/mm/yyyy') as PeriodoInic         "   +
			" ,to_char(v.DF_PERIODOFIN,'dd/mm/yyyy') as PeriodoFin          "   +
			" ,v.IG_DIAS as Dias                "   +
			" ,v.FG_COMISION as Comision            "   +
			" ,v.FG_PORCGARANTIA as PorcGarantia        "   +
			" ,v.FG_PORCCOMISION as PorcComision        "   +
			" ,v.FG_SDOINSOLUTO as SdoInsoluto         "   +
			" ,v.FG_AMORTIZACION as Amortizacion        "   +
			" ,v.FG_INTERES as Interes             "   +
			" ,v.FG_INTCOBRADOXANTICIP as IntCobAnt           "   +
			" ,v.FG_TOTALVENCIMIENTO as TotalVencimiento    "   +
			" ,v.FG_PAGOTRAD as PagoTrad            "   +
			" ,v.FG_SUBSIDIO as Subsidio            "   +
			" ,v.FG_TOTALEXIGIBLE as TotalExigible       "   +
			" ,v.FG_CAPITALVENCIDO as CapitalVencido      "   +
			" ,v.FG_INTERESVENCIDO as InteresVencido      "   +
			" ,v.FG_TOTALCARVEN as TotalCarVen         "   +
			" ,v.FG_ADEUDOTOTAL as AdeudoTotal         "   +
			" ,v.FG_FINADICOTORGADO as FinAdicOtorgado     "   +
			" ,v.FG_FINADICRECUP as FinAdicRecup        "   +
			" ,v.FG_SDOINSNVO as SdoInsNvo           "   +
			" ,v.FG_SDOINSBASE as SdoInsBase"  +
			" ,I.ic_financiera as financiera"  +
                        " ,v.ig_numero_docto as NumeroDocumento"+//FODEA 015 - 2009
                        " ,v.cg_cuenta_clabe as CuentaCLABE "+
			" ,'ArmaQueryFile::regresaQueryVenc' " +
			"from  " +
			"        COM_VENCIMIENTO V,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   V.ic_moneda = M.ic_moneda  " +
			"    and V.ic_if = I.ic_if ");

	        if(!ic_if.equals("")) // Se agrega ic_if
	         	condicion += " and i.ic_if="+ic_if;
	        if(!cliente.equals(""))
	         	condicion += " and v.ig_cliente="+cliente;
	         if(!prestamo.equals(""))
	         	condicion += " and v.ig_prestamo="+prestamo;
	         if(!FechaCorte.equals(""))
	            condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
			if(!moneda.equals(""))
				condicion += " and V.ic_moneda = "+moneda;
				
		     qrySentencia.append(" " + condicion + " order by v.ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy') ");				
		}catch(Exception e){
			e.printStackTrace();
		}
		return qrySentencia.toString();
	}
	
/**
 * Arma cadena con query para operados
 *@ author Salvador Saldivar Barajas
 *@ since 21 de septiembre de 2006
 *@ param ic_if Variable que indentifica al cliente
 *@ param cliente Variable que contiene nombre del cliente
 *@ param prestamo Variable que indica el tipo de prestamo
 *@ param Fecha_Corte Variable que indica la fecha de corte
 *@ param moneda Variable que indica el tipo de moneda
 *@ return regresa cadena con el query armado
 */		
	
	public String regresaQueryOper(String ic_if,String cliente,String prestamo,String FechaCorte,String moneda){
		try{
			qrySentencia = new StringBuffer();
			condicion = "";
			qrySentencia.append(        
			"select " +
			"    /*+ " +
			"        INDEX (O IN_COM_OPERADO_01_NUK)" +
			"        USE_NL (O M)" +
			"    */ " +
			" IG_ESTATUS	AS	Status              "   +
			" ,to_char(O.DF_FECHAOPERACION,'dd/mm/yyyy')	AS	FechaOperacion      "   +
			" ,O.IC_MONEDA	AS	CodMoneda           "   +
			" ,M.CD_NOMBRE	AS	DescMoneda          "   +
			" ,O.FG_TIPOCAMBIO	AS	TipoCambio          "   +
			" ,O.IC_IF	AS	Intermediario       "   +
			" ,I.CG_RAZON_SOCIAL	AS	NomIntermediario    "   +
			" ,O.CG_CALIFICACION	AS	Calificacion        "   +
			" ,O.IG_BASEOPERACION	AS	BaseOperacion       "   +
			" ,O.CG_DESCBASEOP	AS	DescBaseOp          "   +
			" ,O.IG_CLIENTE	AS	Cliente             "   +
			" ,O.CG_NOMBRECLIENTE	AS	NombreCliente       "   +
			" ,O.IG_SUCCLIENTE	AS	SucCliente          "   +
			" ,O.IG_PROYECTO	AS	Proyecto            "   +
			" ,O.IG_CONTRATO	AS	Contrato            "   +
			" ,O.IG_PRESTAMO	AS	Prestamo            "   +
			" ,O.IG_NUMELECTRONICO	AS	NumElectronico      "   +
			" ,O.IG_DISPOSICION	AS	Disposicion         "   +
			" ,O.IG_ESTADO	AS	Estado              "   +
			" ,O.IG_MUNICIPIO	AS	Municipio           "   +
			" ,O.IG_SUBAPLICACION	AS	SubAplicacion       "   +
			" ,O.IG_SUCTRAM	AS	SucTram             "   +
			" ,to_char(O.DF_FECHAPRIPAGCAP,'dd/mm/yyyy')	AS	FechaPriPagCap      "   +
			" ,O.IG_TASA	AS	Tasa                "   +
			" ,O.CG_RELMAT	AS	RelMat              "   +
			" ,O.FG_SPREAD	AS	Spread              "   +
			" ,O.FG_MARGEN	AS	Margen              "   +
			" ,O.CG_ACTECO	AS	ActEco              "   +
			" ,O.CG_ANIOMOD	AS	AnioMOD            "   +
			" ,O.IG_ESTRATO	AS	Estrato             "   +
			" ,O.CG_AFIANZ	AS	Afianz              "   +
			" ,O.IG_FRECCAP	AS	FrecCap             "   +
			" ,O.IG_FRECINT	AS	FrecInt             "   +
			" ,O.IG_NUMCOUTAS	AS	NumCuotas           "   +
			" ,O.IG_TIPOAMORT	AS	TipoAmort           "   +
			" ,O.CG_MODPAGO	AS	ModPago             "   +
			" ,O.CG_CENTROFIN	AS	CentroFin           "   +
			" ,O.IG_SUCINTER	AS	SucInter            "   +
			" ,O.FG_MONTOOPER	AS	MontoOper           "   +
			" ,O.FG_MONTOPRIMCUOT	AS	MontoPrimCuot       "   +
			" ,O.FG_MONTOULTCOUT	AS	MontoUltCuota       "   +
			" ,O.FG_MONTORECAL	AS	MontoRecal          "   +
			" ,O.FG_MONTOPORGAR	AS	ComisionPorGar      "   +
			" ,O.FG_COMNODISP	AS	ComNoDisp           "   +
			" ,O.FG_INTCOBRADOXANTICIP	AS	IntCobAnt           "   +
			" ,O.IG_TIPOGAR	AS	TipoGar             "   +
			" ,O.FG_NETOOTORGADO	AS	NetoOtorgado        "   +
			" ,O.CG_PRIMABRUTA	AS	PrimaBruta"+
			" ,I.ic_financiera"+
			" ,'ArmaQueryFile::regresaQueryOper' " +
			"from  " +
			"        COM_OPERADO O,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   O.ic_moneda = M.ic_moneda  " +
			"    and O.ic_if = I.ic_if " +
			"	 and O.ic_if = " + ic_if);

	         if(!moneda.equals(""))
	         	condicion += " and o.ic_moneda="+moneda;
	         if(!cliente.equals(""))
	         	condicion += " and o.ig_cliente="+cliente;
	         if(!prestamo.equals(""))
	         	condicion += " and o.ig_prestamo="+prestamo;
	         if(!FechaCorte.equals(""))
	            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 
		     qrySentencia.append(" " + condicion + " order by O.ic_moneda, O.ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy') ");		     	
	    }catch(Exception e){
	      e.printStackTrace();
	    }
	    return qrySentencia.toString();
	}
	
/**
 * Arma cadena con query para estados de cuenta
 *@ author Salvador Saldivar Barajas
 *@ since 21 de septiembre de 2006
 *@ param ic_if Variable que indentifica al cliente
 *@ param cliente Variable que contiene nombre del cliente
 *@ param prestamo Variable que indica el tipo de prestamo
 *@ param Fecha_Corte Variable que indica la fecha de corte
 *@ param moneda Variable que indica el tipo de moneda
 *@ return regresa cadena con el query armado
 */		
	
	public String regresaQueryEdoCta(String ic_if,String cliente,String prestamo,String FechaCorte,String moneda){
		try{
			qrySentencia = new StringBuffer();
			condicion = "";
			qrySentencia.append(        
			"select " +
			"    /*+ " +
			"        INDEX (E IN_COM_ESTADO_CUENTA_01_NUK)" +
			"    */ " +
			" to_char(E.DF_FECHAFINMES,'dd/mm/yyyy')		AS FechaFinMes         "   +
			" ,E.IC_MONEDA						AS Moneda              "   +
			" ,M.CD_NOMBRE 					AS DescMoneda         "   +
			" ,E.IC_IF							AS Intermediario       "   +
			" ,I.CG_RAZON_SOCIAL 			AS DescIf         "   +
			" ,E.IG_CODSUCURSAL				AS CodSucursal         "   +
			" ,E.CG_NOMSUCURSAL				AS NomSucursal         "   +
			" ,E.IG_TIPOINTERMEDIARIO 		AS TipIntermediario    "   +
			" ,E.CG_TIPOINTERMEDIARIO_DESC	AS Descripcion         "   +
			" ,E.CG_DESCMODPAGO 				AS DescModPago         "   +
			" ,E.IG_PRODBANCO 				AS ProdBanco           "   +
			" ,E.CG_PRODBANCO_DESC 			AS Descripcion         "   +
			" ,E.IG_SUBAPL 					AS SubApl              "   +
			" ,E.CG_SUBAPL_DESC 				AS Descripcion         "   +
			" ,E.IG_CLIENTE 					AS Cliente             "   +
			" ,E.CG_NOMBRECLIENTE 			AS NombreCliente       "   +
			" ,E.IG_PROYECTO 					AS Proyecto            "   +
			" ,E.IG_CONTRATO 					AS Contrato            "   +
			" ,E.IG_PRESTAMO 					AS Prestamo            "   +
			" ,E.IG_NUMERO_ELECTRONICO 	AS NumElectronico      "   +
			" ,E.IG_DISPOSICION 				AS Disposicion         "   +
			" ,E.IG_FRECINTERES 				AS FrecInteres         "   +
			" ,E.IG_FRECCAPITAL 				AS FrecCapital         "   +
			" ,to_char(E.DF_FECHOPERACION,'dd/mm/yyyy')	AS FechOperacion       "   +
			" ,to_char(E.DF_FECHVENCIMIENTO,'dd/mm/yyyy') AS FechVencimiento     "   +
			" ,E.IG_TASAREFERENCIAL 		AS TasaReferencial     "   +
			" ,E.CG_DESCRIPCIONTASA 		AS DescripcionTasa     "   +
			" ,E.CG_RELMAT1					AS RelMat1             "   +
			" ,E.FG_SPREAD 					AS Spread              "   +
			" ,E.FG_MARGEN 					AS Margen              "   +
			" ,E.FG_TASAMORATORIA 			AS TasaMoratoria       "   +
			" ,E.IG_SANCION 					AS Sancion             "   +
			" ,E.DF_1ACUOTAVEN 				AS aCuotaVen          "   +
			" ,E.IG_DIAVENCIMIENTO 			AS DiaVencimiento      "   +
			" ,E.IG_DIAPROVISION 			AS DiaProvision        "   +
			" ,E.FG_MONTOOPERADO 			AS MontoOperado        "   +
			" ,E.FG_SALDOINSOLUTO 			AS SaldoInsoluto       "   +
			" ,E.FG_CAPITALVIGENTE 			AS CapitalVigente      "   +
			" ,E.FG_CAPITALVENCIDO 			AS CapitalVencido      "   +
			" ,E.FG_INTERESPROVI 			AS InteresProvi        "   +
			" ,E.FG_INTCOBRADOXANTICIP 	AS IntCobAnt           "   +
			" ,E.FG_INTERESGRAVPROV 		AS InteresGravProv     "   +
			" ,E.FG_IVAPROV 					AS IVAProv             "   +
			" ,E.FG_INTERESVENCIDO 			AS InteresVencido      "   +
			" ,E.FG_INTERESVENCIDOGRAVADO AS InteresVencidoGravado"   +
			" ,E.FG_IVAVENCIDO 				AS IVAVencido          "   +
			" ,E.FG_INTERESMORAT 			AS InteresMorat        "   +
			" ,E.FG_INTERESMORATGRAVADO 	AS InteresMoratGravado "   +
			" ,E.FG_IVASOBREMORATORIOS		AS IVAsobreMoratorios  "   +
			" ,E.FG_SOBRETASAMOR 			AS SobreTasaMor        "   +
			" ,E.FG_SOBRETASAMORGRAVADO 	AS SobreTasaMorGravado "   +
			" ,E.FG_OTROSADEUDOS 			AS OtrosAdeudos        "   +
			" ,E.FG_COMISIONGTIA 			AS ComisionGtia        "   +
			" ,E.FG_COMISIONES 				AS Comisiones          "   +
			" ,E.FG_SOBTASAGTIA_PORC 		AS SobTasaGtiaporc       "   +
			" ,E.FG_FINADICOTORG 			AS FinAdicOtorg        "   +
			" ,E.FG_FINANADICRECUP 			AS FinanAdicRecup      "   +
			" ,E.FG_TOTALFINAN 				AS TotalFinan          "   +
			" ,E.FG_ADEUDOTOTAL 				AS AdeudoTotal         "   +
			" ,E.FG_CAPITALRECUP 			AS CapitalRecup        "   +
			" ,E.FG_INTERESRECUP 			AS InteresRecup        "   +
			" ,E.FG_INTERESRECUPGRAVADO 	AS InteresRecupGravado "   +
			" ,E.FG_MORARECUP 				AS MoraRecup           "   +
			" ,E.FG_MORARECUPGRAVADO 		AS MoraRecupGravado    "   +
			" ,E.FG_IVARECUP 					AS IVARecup           "   +
			" ,E.FG_SUBSIDIOAPLICADO 		AS SubsidioAplicado    "   +
			" ,E.FG_SALDONAFIN 				AS SaldoNafin          "   +
			" ,E.FG_SALDOBURSATIL 			AS SaldoBursatil       "   +
			" ,E.FG_VALORTASA 				AS ValorTasa           "   +
			" ,E.FG_TASATOTAL 				AS TasaTotal           "   +
			" ,E.CG_EDOCARTERA 				AS EdoCartera"  +
			" ,I.ic_financiera"+
			" ,'ArmaQueryFile::regresaQueryEdoCta' " +
			"from  " +
			"        COM_ESTADO_CUENTA E,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   E.ic_moneda = M.ic_moneda  " +
			"    and E.ic_if = I.ic_if " +
			"	 and E.ic_if = " + ic_if);

	         if(!moneda.equals(""))
	         	condicion += " and E.ic_moneda="+moneda;
	         if(!cliente.equals(""))
	         	condicion += " and E.ig_cliente="+cliente;
	         if(!prestamo.equals(""))
	         	condicion += " and E.ig_prestamo="+prestamo;
	         if(!FechaCorte.equals(""))
	            condicion += " AND DF_FECHAFINMES >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAFINMES < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
	
		     qrySentencia.append(" " + condicion + " order by M.ic_moneda, E.ig_cliente, E.ig_prestamo ");	
	    }catch(Exception e){
	      e.printStackTrace();
	    }
	    return qrySentencia.toString();
	}
	
}