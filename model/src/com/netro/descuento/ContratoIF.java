package com.netro.descuento;

/**
 * Representa el Contrato parametrizado por IF.
 * @since F012-2008
 * @author Alberto Cruz
 */

public class ContratoIF implements java.io.Serializable {

	private String claveIF;
	private String consecutivo;
	private String tituloAviso;
	private String contenidoAviso;
	private String botonAviso;
	private ContratoIFArchivo contratoArchivo = new ContratoIFArchivo();
	private String textoAceptacion;
	private String botonAceptacion;
	private String mostrar;
	private java.util.Date fechaAlta;
	private boolean ultimo;
	private String nombreIF;

	public ContratoIF() {}

	/**
	 * Establece la clave de la if
	 * @param claveIF Clave de la if
	 *
	 */
	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
		contratoArchivo.setClaveIF(claveIF);
	}

	/**
	 * Obtiene la clave de la if
	 * @return Clave de la if
	 *
	 */
	public String getClaveIF() {
		return this.claveIF;
	}

	/**
	 * Establece el consecutivo
	 * @param consecutivo Consecutivo
	 *
	 */
	public void setConsecutivo(String consecutivo) {
		this.consecutivo = consecutivo;
		contratoArchivo.setConsecutivo(consecutivo);
	}

	/**
	 * Obtiene el consecutivo
	 * @return Consecutivo
	 *
	 */
	public String getConsecutivo() {
		return this.consecutivo;
	}

	/**
	 * Establece el titulo del aviso
	 * @param tituloAviso Titulo del aviso
	 *
	 */
	public void setTituloAviso(String tituloAviso) {
		this.tituloAviso = tituloAviso;
	}

	/**
	 * Establece el contenido del aviso
	 * @param contenidoAviso Contenido del aviso
	 *
	 */
	public void setContenidoAviso(String contenidoAviso) {
		this.contenidoAviso = contenidoAviso;
	}

	/**
	 * Establece el boton del aviso
	 * @param botonAviso Titulo del boton del aviso
	 *
	 */
	public void setBotonAviso(String botonAviso) {
		this.botonAviso = botonAviso;
	}

	/**
	 * Establece el texto de aceptaci�n
	 * @param textoAceptacion Texto que precede al boton de aceptaci�n
	 *
	 */
	public void setTextoAceptacion(String textoAceptacion) {
		this.textoAceptacion = textoAceptacion;
	}

	/**
	 * Establece el texto del boton de aceptaci�n
	 * @param botonAceptacion Texto del bot�n de aceptaci�n
	 *
	 */
	public void setBotonAceptacion(String botonAceptacion) {
		this.botonAceptacion = botonAceptacion;
	}

	/**
	 * Establece si el contrato ser� mostrado o no a las pymes de la IF
	 * @param mostrar S para mostrar el contrato o N para no mostrarlo
	 *
	 */
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}

	/**
	 * Establece la fecha-hora de alta del contrato
	 * @param fechaAlta Fecha de alta del contrato
	 *
	 */
	public void setFechaAlta(java.util.Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


	/**
	 * Obtiene el titulo del aviso
	 * @return Titulo del aviso
	 *
	 */
	public String getTituloAviso() {
		return tituloAviso;
	}

	/**
	 * Obtiene el contenido del aviso
	 * @return Contenido del aviso
	 *
	 */
	public String getContenidoAviso() {
		return this.contenidoAviso;
	}

	/**
	 * Obtiene el texto del boton del aviso
	 * @return Titulo del boton del aviso
	 *
	 */
	public String getBotonAviso() {
		return this.botonAviso;
	}

	/**
	 * Obtiene el texto de aceptaci�n
	 * @return Texto que precede al boton de aceptaci�n
	 *
	 */
	public String getTextoAceptacion() {
		return this.textoAceptacion;
	}

	/**
	 * Obtiene el texto del boton de aceptaci�n
	 * @param botonAceptacion Texto del bot�n de aceptaci�n
	 *
	 */
	public String getBotonAceptacion() {
		return this.botonAceptacion;
	}

	/**
	 * Obtiene si el contrato ser� mostrado o no a las pymes de la IF
	 * @return S para mostrar el contrato o N para no mostrarlo
	 *
	 */
	public String getMostrar() {
		return this.mostrar;
	}

	/**
	 * Establece la fecha-hora de alta del contrato
	 * @param fechaAlta Fecha de alta del contrato
	 *
	 */
	public java.util.Date getFechaAlta() {
		return this.fechaAlta;
	}


	/**
	 * Establece los datos del archivo del contrato
	 * @param contratoArchivo Objeto de ContratoIFArchivo
	 *
	 */
	public void setContratoArchivo(ContratoIFArchivo contratoArchivo) {
		this.contratoArchivo = contratoArchivo;
	}

	/**
	 * Obtiene los datos del archivo del contrato
	 * @return Objeto de ContratoIFArchivo con la informaci�n del archivo
	 *
	 */
	public ContratoIFArchivo getContratoArchivo() {
		return this.contratoArchivo;
	}



	/**
	 * Establece si es o no, el contrato m�s reciente
	 * @param claveIF Clave de la if
	 *
	 */
	public void setUltimo(boolean ultimo) {
		this.ultimo = ultimo;
	}

	/**
	 * Obtiene si es o no, el contrato m�s reciente
	 * @return true si es el m�s reciente o false de lo contrario
	 *
	 */
	public boolean esUltimo() {
		return this.ultimo;
	}


	/**
	 * Establece el nombre de la IF
	 * @param nombreIF Nombre de la IF
	 *
	 */
	public void setNombreIF(String nombreIF) {
		this.nombreIF = nombreIF;
	}

	/**
	 * Obtiene el nombre de la IF
	 * @return Nombre de la IF
	 *
	 */
	public String getNombreIF() {
		return this.nombreIF;
	}




	public String toString() {
		return
				"[" +
				"claveIF=" + this.claveIF + "," +
				"consecutivo=" + this.consecutivo + "," +
				"tituloAviso=" + this.tituloAviso + "," +
				"contenidoAviso=" + this.contenidoAviso + "," +
				"botonAviso=" +this. botonAviso + "," +
				"textoAceptacion=" + this.textoAceptacion + "," +
				"botonAceptacion=" + this.botonAceptacion + "," +
				"mostrar=" + this.mostrar + "," +
				"fechaAlta=" + this.fechaAlta +
				"contratoArchivo=" + this.contratoArchivo +
				"]";
	}

}// Fin de la Clase