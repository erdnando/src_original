package com.netro.descuento;

import java.util.*;
import java.math.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import com.nafin.ws.*;
import javax.ejb.Remote;

@Remote
public interface CargaDocumento{

	public int camposDetalleHabilitados(String sNoCliente) throws  NafinException;
	public boolean autorizadaFactorajeVencido(String sNoCliente) throws  NafinException;
	public boolean autorizadaFactorajeDistribuido(String sNoCliente) throws  NafinException;
	public String getNumaxProcesoDocto() throws  NafinException;
	public String getDiasInhabiles(String epo) throws  NafinException;
	public Vector getFechasVencimiento(String sNoCliente, int iTipoDato) throws  NafinException;

	public String generarArchivoOrigen(String ic_epo, String cc_acuse) throws  NafinException;

	public String getAvisosNotificacionWS(
			String claveEPO, String usuario, String password,
			String tipoAviso,
			String neIF,
			String fechaNotificacionIni,
			String fechaNotificacionFin,
			String fechaVencimientoIni,
			String fechaVencimientoFin,
			String acuseNotificacion);

	public Vector getCamposAdicionales(String sNoCliente) throws  NafinException;
	public Vector getNombresCamposAdicionales(String sNoCliente) throws  NafinException;
	public int existenDoctosCargados(String sNoCliente, String sNumeroDocto, String df_fecha_docto, String ic_moneda, String ic_pyme) throws  NafinException;
	public int existenDoctosCargadosTmp(String sProcDocto, String sNumeroDocto, String df_fecha_docto, String ic_moneda, String ic_pyme) throws  NafinException;
	public String getPyme(String sNoCliente, String sPymeEpoInterno) throws  NafinException;
	public Hashtable procesarDocumentos(String sNoCliente, String sLineasDoctos, String ctxtmtomindoc,
										String ctxtmtomaxdoc, String ctxtmtomindocdol,
										String ctxtmtomaxdocdol, String ctxttotdoc, String ctxttotdocdol,
										String ctxtmtodoc, String ctxtmtodocdol,
										String ic_proc_docto) throws  NafinException;

	public CargaDocumentoWSInfo procesarDocumentosWS(
			String claveEPO, String usuario, String password,
			String cadenaArchivo, String pkcs7, String serial);

	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto, String NombreMandate, String duplicado)
		throws  NafinException;

	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto,	String sDfFechaVencPyme, String NombreMandate,String duplicado)
		throws  NafinException;
	
	//FODEA 050 - VALC - 10/2008
	public boolean insertaDocumento(String sProcDocto, 		String sIcPyme, 			String sIgNumeroDocto,
									String sDfFechaDocto, 	String sDfFechaVenc, 		String sIcMoneda,
									String sFnMonto, 		String sCsDsctoEspecial, 	String sCtReferencia,
									String sCampAd1, 		String sCampAd2, 			String sCampAd3,
									String sCampAd4, 		String sCampAd5, 			String sIf,
									String sNoCliente, 		String sPorcBeneficiario, 	String sBeneficiario,
									String sEstatusDocto,	String sDfFechaVencPyme,
									String sDfEntrega,	String sTipoCompra,	String sClavePresupuestaria, String sPeriodo, String NombreMandate, String duplicado )
			throws  NafinException;

	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario, String NoMandante)
		throws  NafinException;

	public void insertaDocumentoCM(
					String sIcPyme, 			String sIgNumeroDocto,		String sDfFechaDocto,
					String sDfFechaVenc, 		String sIcMoneda,			String sFnMonto,
					String sCsDsctoEspecial, 	String sCtReferencia,		String sCampAd1,
					String sCampAd2, 			String sCampAd3,			String sCampAd4,
					String sCampAd5, 			String sNoBeneficiario, 	String sPorcBeneficiario,
					String sMontoBeneficiario, 	int iNumProceso, 			String sStatusRegistro,
					String sMensajeError, 		int iNumLinea, 				int iNumCampos,
					String sIcBeneficiario,		String sDfFechaVencPyme, String NoMandante)
		throws  NafinException;


	public void insertaDocumentoCM(String esPathFile,int esNumProceso) throws  NafinException;
	public boolean borraDoctosCargados(String sProcDoctoConsec, String sOrigen, String sNoCliente) throws  NafinException;
	public boolean borraDoctosCargados(String sProcDoctoConsec, String sOrigen) throws  NafinException;
	//public String getNumaxProcesoCargaDoctoDetalle();
	public Vector getCamposDetalle(String sNoCliente) throws  NafinException;
	public Hashtable procesarDocumentosDetalles(String sNoCliente, String sDoctosDetalle) throws  NafinException;
	public void insertaDocumentoDetalle(String sProcDoctoDet, String no_pyme, String ig_numero_docto,
											String consecutivo, String df_fecha_docto, String ic_moneda,
											String sCampo1, String sCampo2, String sCampo3, String sCampo4,
											String sCampo5,	String sCampo6, String sCampo7, String sCampo8,
											String sCampo9, String sCampo10) throws  NafinException;
	public void borraDoctosDetalleCargados(String sConseProcDoctoDet, String sOrigen) throws  NafinException;

	public Vector mostrarDocumentosCargados(String sProcDocto, String sAforo, String sNoCliente,  String sEstatus, String tipoCarga, String sAforoDL, String sesIdiomaUsuario, String doctoDuplicados ) throws  NafinException;

	public Vector mostrarDocumentosBitacoraCM (int NumProceso, String doctoDuplicados  ) throws  NafinException;
	public Vector getComDoctosCargados(String sAcuse, String sAforo, String sNoCliente, String sEstatus, String sAforoDL, String sesIdiomaUsuario ) throws  NafinException;

	public boolean insertaAcuse1(String sNoAcuse, String sNoTotDoctosMn, String sMontoMn,
								 String sNoTotDoctosDol, String sMontoDol, String sNoUsuario,
								 String sAcuseRecibo) throws  NafinException;
	public void insertaDoctosConDetalles(String ic_proc_docto, String ic_proc_docto_det,
											String ses_ic_epo, String no_acuse,  String iNoUsuario , String strNombreUsuario ) throws  NafinException;
	public Vector getDoctosCargados(String sConsecNoProc, String sOrigen) throws  NafinException;
	public Vector getDoctosDetalles(String sProcDocto, String sNumeroDocto, String df_fecha_docto, String ic_moneda, String ic_pyme) throws  NafinException;
	public Vector getDoctosDetalle(String sConsec) throws  NafinException;
	/**
	 *
	 * @param sNoCliente
	 * @param cbEPO
	 * @param factoraje
	 * @return
	 * @throws NafinException
	 */
	public Vector getComboBeneficiarios(String sNoCliente, String cbEPO, String factoraje ) throws  NafinException;

	public String getDocumentosSinOperarWS(
			String claveEPO, String usuario, String password);


	/* Metodos para Mantenimiento de Documentos. */
	public String getNumaxProcesoCambioImporte() throws  NafinException;

	public Hashtable procesarMantenimientoDocumentos(
						String sLineasDoctos,
						String ctxttotdoc, 			String ctxttotdocdol,
						String ctxtmtodoc, 			String ctxtmtodocdol,
						String ctxtmtomindoc, 		String ctxtmtomindocdol,
						String ctxtmtomaxdoc,		String ctxtmtomaxdocdol,
						String ic_proceso_cambio, 	String ic_epo,
						MensajeParam mensaje_param, String sesIdiomaUsuario,
						String sOperaFechaVencPyme)
		throws  NafinException;

	public Vector getDoctosCambioImporteTmp(String sProcesoCambio) throws  NafinException;
	public boolean borraDoctosCambioImporte(String sProcDocto) throws  NafinException;
	public void insertaDoctosCambioEstatus(String no_acuse, String totdoc, String mtodoc,
											String totdocdol, String mtodocdol,
											String noUsuario, String _acuse,
											String procDocto, String usuario) throws  NafinException; //FODEA 015 - 2009 ACF
	public Vector getDoctosCambioImporte(String no_acuse, String sesIdiomaUsuario) throws  NafinException;
	public Vector ObtenerDetalleCargaM (int iNumProceso, String idiomaUsuario ) throws  NafinException;
	public void EjecutaSPInsertaDocto (int iIcProcDocto, String sFolio, int iTotDoc, double dMontoDoc,
									     int iTotDocDol, double dMontoDocDol, String sNoUsuario, String sAcuse) throws  NafinException;
	public void EjecutaSPInsertaDocto (int iIcProcDocto, String sFolio, int iTotDoc, double dMontoDoc,
									     int iTotDocDol, double dMontoDocDol, String sNoUsuario, String sAcuse, String sHash) throws  NafinException;
	public void EjecutaSPInsertaDocto (int iIcProcDocto, String sFolio, int iTotDoc, double dMontoDoc, 	
										  int iTotDocDol, double dMontoDocDol, String sNoUsuario, String sAcuse, String sHash, String nombreUsuario, String correoUsuario, String doctoDuplicados) throws  NafinException;

	public void abreConexionDB() throws  NafinException;
	public void cierraConexionDB();
	public int getNumCamposDetalle(String sNoCliente) throws NafinException ;
	public int getNumProcesoDetalle() throws NafinException;
	public void insertDetallesTMP(int no_proceso, int numLinea, String strNoDocto, String strFechaDocto, String strMoneda,
						 String strConsecutivo, String strCampo1, String strCampo2, String strCampo3, String strCampo4,
						 String strCampo5, String strCampo6, String strCampo7, String strCampo8, String strCampo9,
					 String strCampo10, String EstatusReg, String ErrorReg, String strCgPymeEpoInterno) throws NafinException;
	public Vector mostrarEstadoProcesoCMD (int NumProceso ) throws NafinException ;
	public Vector getResumenOkCMD(int NumProceso)	throws NafinException;
	public String getResultadoErrorCMD ( int NumProceso, String idiomaUsuario) throws NafinException;

	public StringBuffer proceso(String EPO,EpoEnlace epoEnl) throws Exception;

	public StringBuffer procesoDocumentos(String EPO, EpoEnlOperados epoEnlOper);
	public StringBuffer procesoDocumentos(String EPO, EpoEnlOperados epoEnlOper, String status);

	public abstract String getResultadoErrorCMDProc ( int NumProceso)  throws NafinException;
	public abstract Vector getTotalesErrorCMD ( int NumProceso)  throws NafinException;
	public abstract Vector getTotalesValidosCMD ( int NumProceso)  throws NafinException;

	public String subsidioAutomatico(String ic_epo, String strFechaVenc, String strFechaIni)
		throws  NafinException;

	public String subsidioAutomatico(String ic_epo, String strFechaVenc, String strFechaIni, boolean bSave)
		throws  NafinException;

	public void subsidioAutomaticoEpo(String strFechaVenc, String strFechaIni)
		throws  NafinException;

	public abstract boolean autorizadaNotasCredito(String ic_epo) throws NafinException;

	public void insertaDatosTempEnlace(String qry, List lista) throws NafinException;

	public String getAcusePublicacionEnlace(String qry,String enlace) throws NafinException;

	public String getErroresPublicacionEnlace(String qry,String enlace) throws NafinException;

	public String getOperadosEnlace(String tabla) throws NafinException;

	public boolean publicarDocVencidos(String sNoEPO) throws NafinException;
	
	public Vector mostrarDocumentosCargadosConIcDocumento(String sProcDocto,String sAforo,String sNoCliente,String sEstatus,String tipoCarga,String sAforoDL,String sesIdiomaUsuario, String acuse ) throws NafinException;
	
	public Vector getComDoctosCargadosConIcDocumento(String sAcuse, String sAforo, String sNoCliente, String sEstatus, String sAforoDL, String sesIdiomaUsuario ) throws NafinException;
	
	public boolean estaHabilitadaLaPublicacionEPOPEF(String claveEPO)	throws NafinException;
	
	public void agregaParametrosPublicacionEPOPEF(int numeroProceso, int numeroLinea, HashMap parametrosPublicacionEPOPEF) throws  NafinException;
	//>>========================================================================== FODEA 050 - 2008
	public boolean tieneParamEpoPef(String icEpo) throws NafinException;
	//==========================================================================<<
	
	//>>========================================================================== FODEA 050 - 2008 - EBA
	public String generarArchivoOrigenconParam(String ic_epo, String cc_acuse, boolean bmostrarparams) throws NafinException;
	//==========================================================================<<

	public String getDocumentosBajaWS(String claveEPO, String usuario, String password, 
			String fechaInicio, String fechaFin);
	
	public void registraDoctoConErrorEnPublicacion(String icEpo, String cgNumeroProveedor, String igNumeroDocumento, String dfVencimiento, String icMoneda, String fnMonto, String ccTipoFactoraje, String cgUsuarioPublica, String cgEmail); // F059 - 2010 JSHD
		
	public void borraDoctosBitacoraErrorPublicacion(ArrayList listaRegistros)
		throws  AppException; // F059 - 2010 JSHD
	
	public Registros getDatosAcuse(String acuse);
	
	public String  procesoCambioEstatus (String ruta,  String claveEpos )	throws  AppException; //Fodea 036-2011	
	public String  archivoCorreosNotiIF(String archivo) throws  AppException; //Fodea 036-2011
	public List  cargaCorreosNotiIF(String intermediario, String notificacion,String usuario, String archivo, String nombreArchivo) throws  AppException; //Fodea 036-2011
	public String   ultimoArchNotIF(String intermediario, String notificacion )  throws  AppException; //Fodea 036-2011
	public String   EliminarNotIF(String intermediario, String notificacion )  throws  AppException; //Fodea 036-2011
	public String  procesoOperacionDia (String ruta)	throws  AppException; //Fodea 036-2011	
	
	//Migracion EPO
	public int noProcesoCM() throws NafinException;	
	public List  porcentajeProcesoCarga(int NumProceso, String  NumLineas) throws NafinException;
	public List getDoctosCambioImporteTmpExt(String sProcesoCambio, boolean limitado) throws  AppException;
	public List getDoctosCambioImporteExt(String no_acuse, boolean limitada) throws  AppException;
	
	public boolean validaPymeXEpoBloqueda(String cveEpo, String cvePyme, String cveProducto) throws AppException;
	public boolean validaPymeXEpoBloqXNumProv(String numProvedor, String cveEPO, String cveProducto) throws AppException;
	public String getDispersiones(String usuario, String password, String claveEPO, String fechaOperacionIni, String fechaOperacionFin);
	 
	public String  validaDoctoDuplicado(String ic_epo, String df_fecha_docto, String ic_moneda, String ic_pyme,  String monto, String ic_proc_docto) throws  AppException; //Fodea 019-2014  
	 
	public void insertaBitacoraDuplicadosCa( String ig_numero_docto, String df_fecha_docto,
		String ic_moneda, String fn_monto, String ic_epo, String ic_pyme, String usuario, String nombreUsuario,
		String sStatusDocto, String proceso )  throws  AppException;   
	public String retornoEstatusNegociableAut(String strDirectorioTemp,  String estatus ) throws AppException;
	public String correoRetornoEstatusNegociableAut(String documentos, String strDirectorioTemp) throws AppException;
		 
}