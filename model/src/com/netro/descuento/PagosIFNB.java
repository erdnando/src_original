package com.netro.descuento;

import java.util.*;
import com.netro.exception.*;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.*;
import javax.ejb.Remote;

@Remote
public interface PagosIFNB {

	public String obtenerVencimientosSirac()
			throws NafinException;
	public String obtenerOperadosSirac()
			throws NafinException;
	public String obtenerEstadosDeCuentaSirac()
			throws NafinException;
	public List consultarDetalleProceso(String icProc,
			String tipoProceso, String csTipo, String fechaOVFM) throws NafinException;
	
	public String crearCustomFile(List rs,String path, String tipo)
		throws NafinException;
	public List procesarArchivo(String strDirectorio, String strNombreArchivo,
			String strTipoBanco)
			throws NafinException;

	public void borrarPagosCargadosTmp(String numProceso);

	public void transmitirPagos(String numProceso, Acuse3 acuse)
			throws NafinException;

	public void capturaIndividualPagos(Acuse3 acuse, EncabezadoPagoIFNB encabezado,
			ArrayList al) throws NafinException;

	public void capturaIndividualPagosIFB(Acuse3 acuse, EncabezadoPagoIFNB encabezado,
			ArrayList al) throws NafinException;
	public void cedulaConciliacion(CedulaConciliacionIFNB cedula)
			throws NafinException;

	public List consultarProcesos(String fechaDe,String fechaA,String tipoProceso, String csTipo)
			throws NafinException;

	public List consultarProcesos(String fechaDe,String fechaA, String fechaProcDe, String fechaProcA, String tipoProceso, String csTipo)
			throws NafinException;

	public void cancelarProceso(List icProc, String tipoProceso, String numRegistros)
			throws NafinException;

	//public void cancelaProceso(String []icProcVenc)
	//	throws NafinException;

	public void eliminaPagos(String numEncabezado)
			throws NafinException;

	public void eliminaCedula(String sClaveIF, String sClaveMoneda,
			String sFechaCorte, String sFechaEnvio)
			throws NafinException;
	public void eliminaCedula(String sClaveIF, String sClaveMoneda,
			String sFechaCorte, String sFechaEnvio, String tipoLinea, String tipoAfiliado)
			throws NafinException;

	public Vector getEstadoCuenta(String df_fechafinmes,String ic_moneda,
			String ic_if,String txt_num_int)
			throws NafinException;
	public Vector getEstadoCuenta(
				String df_fechafinmes,
				String ic_moneda,
				String ic_if,
				String txt_num_int, String tipoLinea, String tipoAfiliado, String numSirac)
			throws NafinException;

	public void guardaDatosOBS(String df_fechafinmes, String ic_moneda,
			String ic_if, String cg_observaciones)
			throws NafinException;
	public void guardaDatosOBS(String df_fechafinmes, String ic_moneda, 
			String ic_if, String cg_observaciones, String numSirac)
			throws NafinException;

	public ArrayList getFechasCorte(int iDias, String sSigno, String tipoAfiliado)
			throws NafinException;

	public ArrayList getFechasCorte(int iDias, String sSigno,String icIf, String tipoLinea, String tipoAfiliado, String igCliente)
			throws NafinException;

	public void setBitacoraPagos(String ic_if, String datosBitacora,
			String sFechaVencIni, String sFechaVencFin, String sFechaProbIni)
			throws NafinException;

	public ArrayList cargaIndividualPagos(Acuse3 acuse, EncabezadoPagoIFNB encabezado)
			throws NafinException;

	public ArrayList cargaIndividualPagosIFB(Acuse3 acuse, EncabezadoPagoIFNB encabezado)
			throws NafinException;

	public ArrayList getDatosPrestamos(String prestamo)
			throws NafinException;

	public ArrayList consultaDetalles(EncabezadoPagoIFNB encabezado)
			throws NafinException;

	public ArrayList consultaDetallesIFB(EncabezadoPagoIFNB encabezado)
			throws NafinException;

	public String getTipoIF(String sNoIf)
			throws NafinException;

	public String crearArchivosZip(int opc, String ruta)
			throws NafinException;

	public void insertarDatosConsultaPCIf(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, String igprestamo, String icif, String cvecertificado, String pantalla)
			throws NafinException;
	public void insertarDatosConsultaPCIf(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, String igprestamo, String icif, String cvecertificado, String pantalla, String cveCliExterno, String tipoLinea)
			throws NafinException;
		
	public void insertarDatosConsultaPBmx(String icusuario, String cgnombreusuario, String dfcorte,  String icmoneda, String igcliente, String igprestamo, String icif, String cvecertificado, String pantalla)
			throws NafinException;	
	public Registros getArchivoPagos(String ic_if)
			throws NafinException;	
	public List getPagosFecha(String icMoneda,String iNoCliente,String sFechaVencIni,String sFechaVencFin,String sFechaProbIni,String sFechaProbFin,String sTipoBanco,Integer totalReg,Double totalValor)
			throws NafinException;	
	public HashMap getSiracFinaciera(String cveIf) throws AppException;
	public String getSiracLineaCredito(String cveLineaCred) throws AppException;
	public String consultaArchivosZip(String nombreZip, String strDirectorioTemp) throws AppException;
	public boolean existeArchivosZip(String nombreZip) throws AppException;
	public void guardarArchivosZip(String nombreZip, String strDirectorioTemp) throws AppException;
	public void depuraArchivosZip(String nombreZip) throws AppException;
	
}