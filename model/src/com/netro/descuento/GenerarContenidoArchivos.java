package com.netro.descuento;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		GenerarContenidoArchivos.java
*
*	Proposito:	Clase GenerarContenidoArchivos
*
*	Lenguaje:	Java
*
*	Autor:		Salvador Saldivar Barajas
*
*	Fecha Creacion:	19/09/2006
*
****************************************************************************************************************/

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

import java.sql.ResultSet;

import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GenerarContenidoArchivos {

	StringBuffer contenidoArchivo = null;

	private String rutaServerc;
        
	private final static Log log = ServiceLocator.getInstance().getLog(GenerarArchivos.class); 

	public GenerarContenidoArchivos() {}

	/**
	* Arma el formato del archivo con los datos recibidos
	* @author Salvador Saldivar Barajas
	* @since 19 de septiembre de 2006
	* @param rs Resultset con los datos encontrados
	* @param parametro Valor que nos indica si el formato del archivo sera grande o peque�o
	* @param opc Opcion que indica si es vencimiento, operado o estado de cuenta
	* @return regresa cadena con los datos formateados
	*/
	public String llenarContenido(ResultSet rs, String parametro, int opc, String ic_if, String FCorte) {
		try {
			log.info("GenerarContenidoArchivos::llenarContenido(E)");
			log.trace("rs:"+rs);
			log.trace("parametro:"+parametro);
			log.trace("opc:"+opc);
			log.trace("ic_if:"+ic_if);
			log.trace("FCorte:"+FCorte);
			switch(opc) {
				case 1: //Vencimientos
					log.debug("Vencimientos");
					contenidoArchivo = new StringBuffer("");
					if(parametro.equals("S")) {
						contenidoArchivo.append("FechaProbablePago   ;ModalidadPago       ;CodMoneda           ;DescMoneda          ;Intermediario       ;DescIntermediario   ;BaseOperacion       ;DescBaseOper        ;Cliente             ;Nombre              ;Sucursal            ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Bursatilizado       ;Disposicion         ;SubAplicacion       ;CuotasEmit          ;CuotasPorVenc       ;TotalCuotas         ");
						contenidoArchivo.append(";AnioModalidad       ;TipoEstrato         ;FechaOpera          ;Sancionado          ;FrecuenciaCapital   ;FrecuenciaInteres   ;TasaBase            ;EsquemaTasas        ;RelMat_1            ;Spread_1            ;RelMat_2            ;Spread_2            ;RelMat_3            ;Spread_3            ;RelMat_4            ;Margen              ;TipoGarantia        ;PorcDescFop         ;TotDescFop          ;PorcDescFinape      ;TotDescFinape       ");
						contenidoArchivo.append(";FCrecimiento        ;PeriodoInic         ;PeriodoFin          ;Dias                ;Comision            ;PorcGarantia        ;PorcComision        ;SdoInsoluto         ;Amortizacion        ;Interes             ;IntCobAnt           ;TotalVencimiento    ;PagoTrad            ;Subsidio            ;TotalExigible       ;CapitalVencido      ;InteresVencido      ;TotalCarVen         ;AdeudoTotal         ;FinAdicOtorgado     ;FinAdicRecup        ");
						contenidoArchivo.append(";SdoInsNvo           ;SdoInsBase");
					} else {
						contenidoArchivo.append("Cliente             ;Nombre              ;Prestamo            ;EsquemaTasas        ;Margen              ;TotDescFop          ;TotDescFinape       ;SdoInsoluto         ;Amortizacion        ;Interes             ;TotalVencimiento    ;TotalExigible       ;SdoInsNvo");
				  	}
					//FODEA 015 - 2009 ACF (I)
					//Nota: el ic_if en este caso (CASE 1) se toma para guardar la bandera que determina si se muestra el numero de documento.
					if(ic_if.equals("S")) {
						contenidoArchivo.append("             ;NumeroDocumento");
					} else if("".equals(ic_if)) {
						contenidoArchivo.append("             ;CuentaCLABE");
					}
					contenidoArchivo.append("\n");
					//FODEA 015 - 2009 ACF (F)
				  	while (rs.next()) {
						if(parametro.equals("S")) {
							contenidoArchivo.append (
								Comunes.formatoFijo(rs.getString("FechaProbablePago"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("ModalidadPago"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("CodMoneda"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("financiera"),10," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("DescIntermediario"),60," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("BaseOperacion"),10," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("DescBaseOper"),60," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Nombre"),150," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Sucursal"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Bursatilizado"),1," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Disposicion"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("SubAplicacion"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("CuotasEmit"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("CuotasPorVenc"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("TotalCuotas"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("AnioModalidad"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("TipoEstrato"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("FechaOpera"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Sancionado"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("FrecuenciaCapital"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("FrecuenciaInteres"),5," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TasaBase"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("EsquemaTasas"),31," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("RelMat_1"),2," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread_1"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("RelMat_2"),2," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread_2"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("RelMat_3"),2," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread_3"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("RelMat_4"),2," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("TipoGarantia"),5," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("PorcDescFop"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotDescFop"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("PorcDescFinape"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotDescFinape"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FCrecimiento"),13,false),19," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("PeriodoInic"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("PeriodoFin"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Dias"),5," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Comision"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("PorcGarantia"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("PorcComision"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SdoInsoluto"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Amortizacion"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Interes"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalVencimiento"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("PagoTrad"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Subsidio"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalExigible"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalVencido"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresVencido"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalCarVen"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("AdeudoTotal"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinAdicOtorgado"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinAdicRecup"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SdoInsNvo"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SdoInsBase"),2,false),19," ","D")
							);
							//FODEA 015 - 2009 ACF (I)
							if(ic_if.equals("S")) {
								contenidoArchivo.append(";"+Comunes.formatoFijo(rs.getString("NumeroDocumento"),15," ","A"));
							} else if("".equals(ic_if)) {
								contenidoArchivo.append(";"+Comunes.formatoFijo(rs.getString("CuentaCLABE"),18," ","A"));
							}
							contenidoArchivo.append("\n");
							//FODEA 015 - 2009 ACF (F)
						} else {
						    contenidoArchivo.append(
								Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Nombre"),150," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("EsquemaTasas"),31," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotDescFop"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotDescFinape"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SdoInsoluto"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Amortizacion"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Interes"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalVencimiento"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalExigible"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SdoInsNvo"),2,false),19," ","D")
							);
							//FODEA 015 - 2009 ACF (I)
							if(ic_if.equals("S")) {
								contenidoArchivo.append(";"+Comunes.formatoFijo(rs.getString("NumeroDocumento"),15," ","A"));
							}
							contenidoArchivo.append("\n");
							//FODEA 015 - 2009 ACF (F)
              			}
					} // fin while					
					break;
				
				case 2://Operados
					log.debug("Operados");
					contenidoArchivo = new StringBuffer("");
					if (parametro.equals("S")) {			
						contenidoArchivo.append("Status              ;FechaOperacion      ;CodMoneda           ;DescMoneda          ;TipoCambio          ;Intermediario       ;NomIntermediario    ;Calificacion        ;BaseOperacion       ;DescBaseOp          ;Cliente             ;NombreCliente       ;SucCliente          ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;Estado              ;Municipio           ;SubAplicacion       ;SucTram             ;FechaPriPagCap      ;Tasa                ;RelMat              ;Spread              ;Margen              ;ActEco              ;Anio/Mod            ;Estrato             ;Afianz              ;FrecCap             ;FrecInt             ;NumCuotas           ;TipoAmort           ;ModPago             ;CentroFin           ;SucInter            ;MontoOper           ;MontoPrimCuot       ;MontoUltCuota       ;MontoRecal          ;ComisionPorGar      ;ComNoDisp           ;IntCobAnt           ;TipoGar             ;NetoOtorgado        ;PrimaBruta          \n");
					} else {
						contenidoArchivo.append("FechaOperacion      ;Cliente             ;NombreCliente       ;Prestamo            ;NumElectronico      ;FechaPriPagCap      ;Tasa                ;Spread              ;Margen              ;MontoOper           ;MontoPrimCuot       ;NetoOtorgado          \n");
					} 					
					while (rs.next()) {
						if (parametro.equals("S")) {
							contenidoArchivo.append(
								Comunes.formatoFijo(rs.getString("Status"),2," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("FechaOperacion"),10," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("CodMoneda"),2," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TipoCambio"),4,false),15," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("ic_financiera"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("NomIntermediario"),60," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Calificacion"),13," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("BaseOperacion"),6," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("DescBaseOp"),60," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("NombreCliente"),70," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("SucCliente"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Disposicion"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Estado"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Municipio"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("SubAplicacion"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("SucTram"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("FechaPriPagCap"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Tasa"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("RelMat"),2," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("ActEco"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("AnioMOD"),10," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Estrato"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("Afianz"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("FrecCap"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("FrecInt"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("NumCuotas"),5," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("TipoAmort"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("ModPago"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("CentroFin"),5," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("SucInter"),5," ","A")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOper"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoPrimCuot"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoUltCuota"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoRecal"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComisionPorGar"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComNoDisp"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("TipoGar"),5," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("NetoOtorgado"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("PrimaBruta"),6," ","D")+";\n"
							);
						} else {	
							contenidoArchivo.append(
								Comunes.formatoFijo(rs.getString("FechaOperacion"),10," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("NombreCliente"),70," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("FechaPriPagCap"),20," ","A")+";"+ 
								Comunes.formatoFijo(rs.getString("Tasa"),5," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOper"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoPrimCuot"),2,false),19," ","D")+";"+ 
								Comunes.formatoFijo(rs.getString("PrimaBruta"),6," ","D")+";\n"
							);
						}	
					} // fin while
					break;
				
				case 3://Estados de Cuenta
					log.debug("Estados de Cuenta");
					PrintWriter salida = null;
					if(!ic_if.equals("")){
						//FileWriter fw = new FileWriter(this.rutaServerc+"16archivos/descuento/procesos/edocuenta/archedocuenta_"+FCorte+"_"+ic_if+"_S.txt", true);
						FileWriter fw = new FileWriter(this.rutaServerc+"00tmp/13descuento/archedocuenta_"+FCorte+"_"+ic_if+"_S.txt", true);
						BufferedWriter bw = new BufferedWriter(fw);
						salida = new PrintWriter(bw);
					}
					contenidoArchivo = new StringBuffer("");					
					contenidoArchivo.append("FechaFinMes         ;Moneda              ;Descripcion         ;Intermediario       ;Descripcion         ;CodSucursal         ;NomSucursal         ;TipIntermediario    ;Descripcion         ;DescModPago         ;ProdBanco           ;Descripcion         ;SubApl              ;Descripcion         ;Cliente             ;NombreCliente       ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;FrecInteres         ;FrecCapital         ;FechOperacion       ;FechVencimiento     ;TasaReferencial     ;DescripcionTasa     ;RelMat1             ;Spread              ;Margen              ;TasaMoratoria       ;Sancion             ;1aCuotaVen          ;DiaVencimiento      ;DiaProvision        ;MontoOperado        ;SaldoInsoluto       ;CapitalVigente      ;CapitalVencido      ;InteresProvi        ;IntCobAnt           ;InteresGravProv     ;IVAProv             ;InteresVencido      ;InteresVencidoGravado;IVAVencido          ;InteresMorat        ;InteresMoratGravado ;IVAsobreMoratorios  ;SobreTasaMor        ;SobreTasaMorGravado ;OtrosAdeudos        ;ComisionGtia        ;Comisiones          ;SobTasaGtia%        ;FinAdicOtorg        ;FinanAdicRecup      ;TotalFinan          ;AdeudoTotal         ;CapitalRecup        ;InteresRecup        ;InteresRecupGravado ;MoraRecup           ;MoraRecupGravado    ;IVA Recup           ;SubsidioAplicado    ;SaldoNafin          ;SaldoBursatil       ;ValorTasa           ;TasaTotal           ;EdoCartera          ;\n");					
					while (rs.next()) {
						contenidoArchivo.append(
							Comunes.formatoFijo(rs.getString("FechaFinMes"),11," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Moneda"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("ic_financiera"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescIf"),60," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("CodSucursal"),4," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("NomSucursal"),31," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("TipIntermediario"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Descripcion"),30," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescModPago"),25," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("ProdBanco"),6," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Descripcion"),60," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("SubApl"),3," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Descripcion"),40," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Cliente"),12," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NombreCliente"),150," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Disposicion"),3," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecInteres"),2," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecCapital"),2," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FechOperacion"),11," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FechVencimiento"),11," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("TasaReferencial"),4," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescripcionTasa"),30," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("RelMat1"),2," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),2,false),7," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),2,false),7," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TasaMoratoria"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Sancion"),3," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("aCuotaVen"),11," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DiaVencimiento"),2," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("DiaProvision"),11," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOperado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoInsoluto"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalVigente"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalVencido"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresProvi"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresGravProv"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAProv"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresVencido"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresVencidoGravado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAVencido"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresMorat"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresMoratGravado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAsobreMoratorios"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SobreTasaMor"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SobreTasaMorGravado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("OtrosAdeudos"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComisionGtia"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Comisiones"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("SobTasaGtiaporc"),8," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinAdicOtorg"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinanAdicRecup"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalFinan"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("AdeudoTotal"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalRecup"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresRecup"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresRecupGravado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MoraRecup"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MoraRecupGravado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVARecup"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SubsidioAplicado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoNafin"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoBursatil"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ValorTasa"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TasaTotal"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("EdoCartera"),2," ","A")+";\n");

						if(!ic_if.equals("")) {
							salida.print(contenidoArchivo.toString());
							contenidoArchivo = contenidoArchivo.delete(0,contenidoArchivo.length());
						}
					} // fin while
					if(!ic_if.equals("")) {
						salida.close();
					}
					break;
				default:
					log.error("*** Opcion erronea ***");
				}				
            } catch(Exception e) {
                log.error("GenerarContenidoArchivos::llenarContenido(Exception) "+e);
                e.printStackTrace();
            }
		log.info("GenerarContenidoArchivos::llenarContenido(E)");
		return contenidoArchivo.toString();			
	}

	public void setrutaServerc(String ruta){
		this.rutaServerc = ruta;
		System.out.println("GenerarContenidoArchivos ]]]]]] rutaServer::::: "+rutaServerc);
	}
}