package com.netro.descuento;

import java.util.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface ProcesoSNSolicitud {

    public Hashtable ohprocesarSolicitudes(String esDocumentos) throws NafinException;
	
}