package com.netro.descuento;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "LimitesPorEPOEJB" , mappedName = "LimitesPorEPOEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class LimitesPorEPOBean implements LimitesPorEPO {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(LimitesPorEPOBean.class);
	
	/**
	 * Metodo para actualizar la parametrizaci�n de limites por epo.
	 * 
	 * @param esLimiteEPO Montos del limite por EPO
	 * @param esIcEPO Claves de las EPOS
	 * @param esNoCliente Clave del IF
	 * @param esCsActivaLimite Identificadores de activacion o inactivacion del limite
	 * @param esFecVencLinea 
	 * @param esAvisoVencLinea
	 * @param esFecCamAdmon
 	 * @param esAvisoCamAdmon
	 * @param claveUsuario Clave del usuario que realiza la transacci�n
	 * @return ????
	 * @throws com.netro.exception.NafinException
	 */
	public boolean bactualizaLimites(String esLimiteEPO[], String esIcEPO[],
				  String esNoCliente, String esCsActivaLimite[],
					String esFecVencLinea[], String esAvisoVencLinea[],
					String esFecCamAdmon[], String esAvisoCamAdmon[], 
					String claveUsuario,String csActivaProgramado[], String csActivaProgramado2[], String csAcuseRecibo,  String lsFechaLimite2[]) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean bOkActualiza = true;
	String lsQry = "";	
		try{
			lobdConexion.conexionDB();
			
			String qryConsulta = 
				" SELECT ie.cs_activa_limite, n.ic_nafin_electronico as neIF " +
				" FROM comrel_if_epo ie, comrel_nafin n " +
				" WHERE ie.ic_epo = ? and ie.ic_if = ? " +
				" 	AND ie.ic_if = n.ic_epo_pyme_if " +
				" 	AND n.cg_tipo = ? ";
			PreparedStatement psCons = lobdConexion.queryPrecompilado(qryConsulta);

			lsQry =
						" UPDATE comrel_if_epo "+
						" SET in_limite_epo = ROUND(?,2) "+
						" ,  cs_activa_limite= ? "+
						" ,  df_venc_linea= TO_DATE(?,'DD/MM/YYYY') "+
						" ,  ig_aviso_venc_linea= ? "+
						" ,  df_cambio_admon= TO_DATE(?,'DD/MM/YYYY') "+
						" ,  ig_aviso_cambio_admon= ? "+	
						" , CS_FECHA_LIMITE = ? "+
						" WHERE ic_epo = ? and ic_if = ? ";
			
			//System.out.println(lsQry);
			
			PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);
			
			String queryRetornoN = "UPDATE com_documento " +
						"   SET ic_estatus_docto = ? " +
						" WHERE ic_epo = ? " +
						"   AND ic_estatus_docto = ? " +
						"   AND ic_documento IN ( " +
						"          SELECT cd.ic_documento " +
						"            FROM com_documento cd, com_docto_seleccionado cds " +
						"           WHERE cd.ic_documento = cds.ic_documento " +
						"             AND cds.ic_epo = ? " +
						"             AND cds.ic_if = ? " +
						"             AND cd.ic_estatus_docto = ?) ";
			
						
			PreparedStatement psRetornoN = null;
			
			for(int i=0; i < esIcEPO.length; i++) {
				
				if("S".equals(csActivaProgramado[i]) || "S".equals(csActivaProgramado2[i])){
					//se modifica estatus de documentos de Programadas Pyme a Negociables
					psRetornoN = lobdConexion.queryPrecompilado(queryRetornoN);					
					psRetornoN.setInt(1,2);
					psRetornoN.setInt(2, Integer.parseInt(esIcEPO[i]));
					psRetornoN.setInt(3,26);
					psRetornoN.setInt(4, Integer.parseInt(esIcEPO[i]));
					psRetornoN.setInt(5, Integer.parseInt(esNoCliente));
					psRetornoN.setInt(6,26);
					
					psRetornoN.executeUpdate();
					psRetornoN.close();
					
				}
				
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consulta para determinar si hubo cambio de activacion
				psCons.clearParameters();
				psCons.setInt(1, Integer.parseInt(esIcEPO[i]));
				psCons.setInt(2, Integer.parseInt(esNoCliente));
				psCons.setString(3, "I"); //I=IF
				
				ResultSet rsCons = psCons.executeQuery();
				rsCons.next();
				String limiteActivado = rsCons.getString("cs_activa_limite");
				String neIF = rsCons.getString("neIF");
				rsCons.close();
				
				if (!esCsActivaLimite[i].equals(limiteActivado)) {//Si hay cambio de activacion
					Bitacora.grabarEnBitacora(lobdConexion, "LIMITESEPO", "I",
							neIF, claveUsuario,
						"cs_activa_limite=" + limiteActivado + "\n" +
						"ic_if="+ esNoCliente + "\n" +
						"ic_epo=" + esIcEPO[i],
						"cs_activa_limite=" + esCsActivaLimite[i] + "\n" +
						"ic_if=" + esNoCliente + "\n" +
						"ic_epo=" + esIcEPO[i] + "\n" + 
						"ig_recibo=" + csAcuseRecibo);
				}
				
				
				// FIN Consulta para determinar si hubo cambio de activacion >>>>>>>>>>>>>>>>>>>>>>>>>>

				try{
					ps.clearParameters();
				
					ps.setDouble(1,Double.parseDouble(esLimiteEPO[i]));
					ps.setString(2, esCsActivaLimite[i]);
					/*INICIO: CODIGO POR FODEA 051-2008*/
					ps.setString(3, esFecVencLinea[i]);
					if(!"".equals(esAvisoVencLinea[i]))
						ps.setInt(4, Integer.parseInt(esAvisoVencLinea[i]));
					else
						ps.setNull(4,Types.INTEGER);
					ps.setString(5, esFecCamAdmon[i]);
					if(!"".equals(esAvisoCamAdmon[i]))
						ps.setInt(6, Integer.parseInt(esAvisoCamAdmon[i]));
					else
						ps.setNull(6,Types.INTEGER);
						/*FIN: CODIGO POR FODEA 051-2008*/
					ps.setString(7, lsFechaLimite2[i]); 
					ps.setInt(8, Integer.parseInt(esIcEPO[i]));					
					ps.setInt(9, Integer.parseInt(esNoCliente));
					ps.executeUpdate();
			

				} catch (SQLException sqle){
					sqle.printStackTrace();
					bOkActualiza = false;
					throw new NafinException("DSCT0031");
				}
			} //for
			ps.close();
			psCons.close();

		} catch (NafinException ne) {			
			throw ne;
		} catch (Exception e) {
			bOkActualiza = false;
			System.out.println("Exception en bactualizaLimites(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(bOkActualiza);
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return bOkActualiza;
	}


	/**
	 * Metodo para actualizar la parametrizaci�n de limites por epo.
	 * Este m�todo debe remplazar a 
	 * bactualizaLimites(String esLimiteEPO[], String esIcEPO[],
	 *			  String esNoCliente, String esCsActivaLimite[],
	 *				String esFecVencLinea[], String esAvisoVencLinea[],
	 *				String esFecCamAdmon[], String esAvisoCamAdmon[], 
	 *				String claveUsuario,String csActivaProgramado[], 
	 *          String csActivaProgramado2[], String csAcuseRecibo,  
	 *          String lsFechaLimite2[])
	 * @param registrosModificados Lista de registros modificados
	 * @param claveIF Clave del IF (ic_if)
	 * @param claveUsuario Clave del usuario que realiza la transacci�n
	 * @param csAcuseRecibo Acuse de recibo de la firma digital.
	 */
	public void bactualizaLimitesExt(
			List registrosModificados, String claveIF,
			String claveUsuario, String csAcuseRecibo,  String usuarioBloqueo, String dependenciaBloqueo  ) {
			
		log.info("bactualizaLimites(E)");
		AccesoDB lobdConexion = new AccesoDB();
		boolean bOkActualiza = true;
		String lsQry = "";	
		try{
			lobdConexion.conexionDB();
			
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class); 
				
			Iterator itReg = registrosModificados.iterator();
			while (itReg.hasNext()) {
			
				JSONObject registro = (JSONObject)itReg.next();			
				
				if(registro.getString("ACTIVA_PROCESO").equals("S") || registro.getString("ACTIVA_PROCESOB").equals("S")){
					//se modifica estatus de documentos de Programadas Pyme a Negociables
					String queryRetornoN = "UPDATE com_documento " +
						"   SET ic_estatus_docto = ? " +
						" WHERE ic_epo = ? " +
						"   AND ic_estatus_docto = ? " +
						"   AND ic_documento IN ( " +
						"          SELECT cd.ic_documento " +
						"            FROM com_documento cd, com_docto_seleccionado cds " +
						"           WHERE cd.ic_documento = cds.ic_documento " +
						"             AND cds.ic_epo = ? " +
						"             AND cds.ic_if = ? " +
						"             AND cd.ic_estatus_docto = ?) ";			
						
					PreparedStatement psRetornoN = null;
					psRetornoN = lobdConexion.queryPrecompilado(queryRetornoN);
					psRetornoN.setInt(1,2);
					psRetornoN.setInt(2, registro.getInt("IC_EPO"));
					psRetornoN.setInt(3,26);
					psRetornoN.setInt(4, registro.getInt("IC_EPO"));
					psRetornoN.setInt(5, Integer.parseInt(claveIF));
					psRetornoN.setInt(6,26);					
					psRetornoN.executeUpdate();
					psRetornoN.close();				
				}
				
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consulta para determinar si hubo cambio de activacion
					String qryConsulta = 
					" SELECT ie.cs_activa_limite, n.ic_nafin_electronico as neIF " +
					" FROM comrel_if_epo ie, comrel_nafin n " +
					" WHERE ie.ic_epo = ? and ie.ic_if = ? " +
					" 	AND ie.ic_if = n.ic_epo_pyme_if " +
					" 	AND n.cg_tipo = ? ";
				PreparedStatement psCons = lobdConexion.queryPrecompilado(qryConsulta);
				psCons.clearParameters();
				psCons.setInt(1, registro.getInt("IC_EPO"));
				psCons.setInt(2, Integer.parseInt(claveIF));
				psCons.setString(3, "I"); //I=IF				
				ResultSet rsCons = psCons.executeQuery();
				rsCons.next();
				boolean limiteActivado = (rsCons.getString("cs_activa_limite") != null && rsCons.getString("cs_activa_limite").equals("S"))?true:false;
				String neIF = rsCons.getString("neIF");
				rsCons.close();
				psCons.close();
				
				if ( limiteActivado != registro.getBoolean("CS_ACTIVA_LIMITE")) {//Si hay cambio de activacion
					Bitacora.grabarEnBitacora(lobdConexion, "LIMITESEPO", "I",
							neIF, claveUsuario,
						"cs_activa_limite=" + ((limiteActivado)?"S":"N") + "\n" +
						"ic_if="+ claveIF + "\n" +
						"ic_epo=" + registro.get("IC_EPO"),
						"cs_activa_limite=" + ((registro.getBoolean("CS_ACTIVA_LIMITE"))?"S":"N") + "\n" +
						"ic_if=" + claveIF + "\n" +
						"ic_epo=" + registro.get("IC_EPO") + "\n" + 
						"ig_recibo=" + csAcuseRecibo);
				}
				
				
					// Query para saber si el Intermediario Financiero registre por primera vez la l�nea de Cr�dito de una EPO	 F017-2013
					String queyValida1erVez =  "SELECT  ie.in_limite_epo  as MONTOMN ,  ie.in_limite_epo_dl  as MONTODL"+
					" FROM comrel_if_epo ie, comcat_epo e, comrel_producto_epo cpe "+
					" WHERE e.ic_epo = ie.ic_epo "+
					" AND e.cs_habilitado = 'S'   "+
					" AND ie.cs_vobo_nafin = 'S'  "+
					" AND ie.cs_bloqueo = 'N'  "+
					" AND cpe.ic_epo = e.ic_epo  "+
					" AND cpe.ic_epo = ie.ic_epo  "+
					" AND cpe.ic_producto_nafin = 1  "+
					" AND cpe.cs_limite_pyme != 'S'   "+
					" and ie.ic_epo =  "+registro.getString("IC_EPO") +
					" and ie.ic_if =  "+claveIF;
					log.debug( " queyValida1erVez---------------> "+ queyValida1erVez);
					Registros regisMV = lobdConexion.consultarDB(queyValida1erVez);
					double montoValida1erMN =0, montoValida1erDL = 0;
					if(regisMV.next()){						
						montoValida1erMN =  Double.parseDouble(regisMV.getString("MONTOMN"));
						montoValida1erDL =  Double.parseDouble(regisMV.getString("MONTODL"));
					}
					log.debug( " montoValida1erMN---------------> "+ montoValida1erMN);
					log.debug( " montoValida1erDL---------------> "+ montoValida1erDL);					
					String ordenIF = "",  lsQryOrden ="";
					if(montoValida1erMN==0 ||  montoValida1erDL==0 ){
						//quey para obtener el orden  en caso de que  el Intermediario Financiero registre por primera vez la l�nea de Cr�dito de una EPO
						String queyOrden  = "select max(nvl(ie.IG_NUM_ORDEN, 0)+1) AS ORDEN   " + 
										"	FROM comcat_if i, comrel_if_epo ie, comrel_producto_epo cpe  " +
										"  WHERE ie.ic_if = i.ic_if   " +
										"	 AND UPPER (ie.cs_vobo_nafin) = 'S'  " +										
										"	 AND cpe.ic_epo = ie.ic_epo  " +
										"	 AND cpe.ic_producto_nafin = 1   " +
										"	 AND cpe.cs_limite_pyme != 'S'  " +
										"	 AND i.cs_opera_fideicomiso = 'N'   " +
										"	 AND ie.ic_epo = "+registro.getString("IC_EPO") +
									  " ORDER BY i.cg_razon_social "; 
						log.debug( "queyOrden---------------> "+ queyOrden );
						Registros regis = lobdConexion.consultarDB(queyOrden);
						if(regis != null && regis.next()){
							ordenIF =regis.getString("ORDEN");
						}						
						lsQryOrden = " , IG_NUM_ORDEN = "+ordenIF;
					}
					
					
				// FIN Consulta para determinar si hubo cambio de activacion >>>>>>>>>>>>>>>>>>>>>>>>>>				
				lsQry =
							" UPDATE comrel_if_epo "+
							" SET in_limite_epo = ROUND(?,2) "+							
							" ,  cs_activa_limite= ? "+
							" ,  df_venc_linea= TO_DATE(?,'DD/MM/YYYY') "+
							" ,  ig_aviso_venc_linea= ? "+
							" ,  df_cambio_admon= TO_DATE(?,'DD/MM/YYYY') "+
							" ,  ig_aviso_cambio_admon= ? "+
							" ,  cs_fecha_limite = ? " +	
							lsQryOrden+ //F017-2013
							" ,in_limite_epo_dl = ROUND(?,2) "+ //Fodea 09-2014
							" ,DF_BLOQ_DES = Sysdate  "+
							" ,CG_USUARIO_BLOQ_DES   =  ?   "+
							" ,CG_DEPENDENCIA_BLOQ_DES   =  ?   "+
							" ,IC_IF_EPO_BLOQ_DES = ?   "+
							
							
							" WHERE ic_epo = ? and ic_if = ? ";
				//System.out.println(lsQry);
				log.debug( "lsQry---------------> "+ lsQry );
				PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);
				ps.clearParameters();
				ps.setDouble(1, registro.getDouble("NUEVO_MONTO_LIMITE"));
				ps.setString(2, ((registro.getBoolean("CS_ACTIVA_LIMITE")==true)?"S":"N") );
				/*INICIO: CODIGO POR FODEA 051-2008*/
				ps.setString(3, Fecha.getFechaDeISO(registro.optString("FEC_VENC_LINEA", null)));	//optString si el campo no trae valor, le asigna un default...
				if(!registro.get("IG_AVISO_VENC_LINEA").equals(JSONNull.getInstance())) {	//Los valores null en JSON no regresan un valor null en Java, regresan JSONNull
					ps.setInt(4, registro.getInt("IG_AVISO_VENC_LINEA"));
				} else {
					ps.setNull(4,Types.INTEGER);
				}				
				ps.setString(5, Fecha.getFechaDeISO(registro.getString("FEC_CAMBIO_ADMON")));			
				if(!registro.get("IG_AVISO_CAMBIO_ADMON").equals(JSONNull.getInstance())) {
					ps.setInt(6, registro.getInt("IG_AVISO_CAMBIO_ADMON"));
				} else {
					ps.setNull(6,Types.INTEGER);
				}
				ps.setString(7, ((registro.getBoolean("CS_FECHA_LIMITE")==true)?"S":"N"));
				ps.setDouble(8, registro.getDouble("NUEVO_MONTO_LIMITE_DL"));
				
				
				ps.setString(9, usuarioBloqueo);
				ps.setString(10, dependenciaBloqueo);				
				ps.setInt(11, Integer.parseInt(claveIF));
			
				ps.setInt(12, registro.getInt("IC_EPO"));
				ps.setInt(13, Integer.parseInt(claveIF));
				ps.executeUpdate();
				ps.close();
				
				System.out.println("---------------------------------------------------------------------------");
				System.out.println(lsQry);
				System.out.println("NUEVO_MONTO_LIMITE " +  registro.getDouble("NUEVO_MONTO_LIMITE"));
				System.out.println("IC_EPO " +  registro.getInt("IC_EPO"));
				System.out.println("claveIF " +  claveIF);
				System.out.println("---------------------------------------------------------------------------");  	


		
				// ------------------Inicia  Fodea 017-2013 ---------------------
				Hashtable alParamEPO = BeanParamDscto.getParametrosEPO(registro.getString("IC_EPO"), 1);
				String operaFideicomiso    = alParamEPO.get("CS_OPERA_FIDEICOMISO").toString(); //Fodea 017-2013
				String montoFideicomiso ="0", montoDisponible ="0", montoComprometido ="0";
				String montoFideicomiso_dl ="0", montoDisponible_dl ="0", montoComprometido_dl ="0";
				
				
				log.debug( " operaFideicomiso---------------> "+ operaFideicomiso);						
				
				if(operaFideicomiso.equals("S"))  {
				
				//Query para sacar la suma de los intermediarios que no operan fideicomiso
					String queyFide1 = "SELECT /*+index(ie) use_nl(ie e cpe)*/  " +
								" SUM(  ie.in_limite_epo)  as montoFideicomiso, "+   
								" SUM(ie.fn_limite_utilizado) montoDisponible , "+
								" SUM(decode(ie.FN_MONTO_COMPROMETIDO,'','0'))   montoComprometido, "+
								
								" SUM( ie.in_limite_epo_dl)  as montoFideicomiso_dl, "+   
								" SUM(ie.fn_limite_utilizado_dl) montoDisponible_dl , "+
								" SUM(decode(ie.FN_MONTO_COMPROMETIDO_DL,'','0'))   montoComprometido_dl "+
								
								" FROM comrel_if_epo ie, comcat_epo e, comrel_producto_epo cpe, comcat_if i "+
							  " WHERE e.ic_epo = ie.ic_epo "+
								" AND e.cs_habilitado = 'S'  "+
								" AND ie.cs_vobo_nafin = 'S'  "+
								 "AND ie.cs_bloqueo = 'N'  "+
								" AND cpe.ic_epo = e.ic_epo  "+
								" AND cpe.ic_epo = ie.ic_epo  "+
								" AND cpe.ic_producto_nafin = 1  "+
								" AND cpe.cs_limite_pyme != 'S'    "+         
								" and ie.ic_epo =  "+registro.getString("IC_EPO") +
								" and ie.ic_if = i.ic_if  "+
								" and i.ic_if !=  "+claveIF+
								" and i.CS_OPERA_FIDEICOMISO = 'N' "+
								" and cpe.CS_OPERA_FIDEICOMISO = 'S'  ";				
						
					log.debug( " queyFide1---------------> "+ queyFide1);
				
					PreparedStatement psConsF = lobdConexion.queryPrecompilado(queyFide1);				
					ResultSet rsConsF = psConsF.executeQuery();
					if (rsConsF.next()) {
						montoFideicomiso = rsConsF.getString("montoFideicomiso")==null?"0":rsConsF.getString("montoFideicomiso").trim();
						montoDisponible = rsConsF.getString("montoDisponible")==null?"0":rsConsF.getString("montoDisponible").trim(); 
						montoComprometido = rsConsF.getString("montoComprometido")==null?"0":rsConsF.getString("montoComprometido").trim(); 
					
						montoFideicomiso_dl = rsConsF.getString("montoFideicomiso_dl")==null?"0":rsConsF.getString("montoFideicomiso_dl").trim();
						montoDisponible_dl = rsConsF.getString("montoDisponible_dl")==null?"0":rsConsF.getString("montoDisponible_dl").trim(); 
						montoComprometido_dl = rsConsF.getString("montoComprometido_dl")==null?"0":rsConsF.getString("montoComprometido_dl").trim(); 
					
					
					}
					rsConsF.close();
					psConsF.close();
										
					
					double  montoTotalFide =  	Double.parseDouble((String)montoFideicomiso) + registro.getDouble("NUEVO_MONTO_LIMITE");					
					double  montoTotalFide_dl =  	Double.parseDouble((String)montoFideicomiso_dl) + registro.getDouble("NUEVO_MONTO_LIMITE_DL");					
							  
					// query para sacar a los IF que si operan Fideicomiso  a los cuales se les va actualizar los montos 
					String queyFideIF = "SELECT /*+index(ie) use_nl(ie e cpe)*/  " +
						"  ie.ic_if as IC_IF  , i.cg_razon_social  "+              
						" FROM comrel_if_epo ie, comcat_epo e, comrel_producto_epo cpe, comcat_if i "+
					  " WHERE e.ic_epo = ie.ic_epo "+
						" AND e.cs_habilitado = 'S'  "+
						" AND ie.cs_vobo_nafin = 'S'  "+
						" AND ie.cs_bloqueo = 'N'  "+
						" AND cpe.ic_epo = e.ic_epo  "+
						" AND cpe.ic_epo = ie.ic_epo  "+
						" AND cpe.ic_producto_nafin = 1  "+
						" AND cpe.cs_limite_pyme != 'S'    "+         
						" and ie.ic_epo =  "+registro.getString("IC_EPO") +
						" and ie.ic_if = i.ic_if  "+
						" and i.CS_OPERA_FIDEICOMISO = 'S' "+
						" and i.ic_if !=  "+claveIF+
						" and cpe.CS_OPERA_FIDEICOMISO = 'S'  ";				
				
					log.debug( "queyFideIF---------------> "+ queyFideIF );	  	
				
					PreparedStatement psConsIF = lobdConexion.queryPrecompilado(queyFideIF);				
					ResultSet rsConsIF = psConsIF.executeQuery();
					log.debug( "queyFideIF---------------> "+ queyFideIF );	  	
					while (rsConsIF.next()) {
					
						String ic_ifOperaFide =  rsConsIF.getString("IC_IF")==null?"":rsConsIF.getString("IC_IF");
						
						String lsQryIFide =" UPDATE comrel_if_epo "+
													" SET in_limite_epo = ROUND(?,2),  "+	
													" fn_limite_utilizado = ROUND(?,2), "+
													" FN_MONTO_COMPROMETIDO = ROUND(?,2), "+
													
													" in_limite_epo_dl = ROUND(?,2),  "+	
													" fn_limite_utilizado_dl = ROUND(?,2), "+
													" FN_MONTO_COMPROMETIDO_dl = ROUND(?,2), "+
													
													" cs_activa_limite  = ?  "+													
													" WHERE ic_epo = ? " + 
													" and ic_if = ? ";
						
						log.debug( "ordenIF---------------> "+ ordenIF );	  
						log.debug(" IC_EPO------------> "+registro.getInt("IC_EPO") );	
						log.debug(" ic_ifOperaFide------------> "+ic_ifOperaFide );	
						
						log.debug( "lsQryIFide---------------> "+ lsQryIFide );
						PreparedStatement psIF = lobdConexion.queryPrecompilado(lsQryIFide);
						psIF.setDouble(1,montoTotalFide);
						psIF.setDouble(2,Double.parseDouble(montoDisponible));	
						psIF.setDouble(3,Double.parseDouble(montoComprometido));							
						psIF.setDouble(4,montoTotalFide_dl);
						psIF.setDouble(5,Double.parseDouble(montoDisponible_dl));	
						psIF.setDouble(6,Double.parseDouble(montoComprometido_dl));	
						psIF.setString(7, "S");							
						psIF.setInt(8, registro.getInt("IC_EPO"));
						psIF.setInt(9, Integer.parseInt(ic_ifOperaFide));				
						psIF.executeUpdate();
						psIF.close();		
						
					}
					rsConsIF.close();
					rsConsIF.close();				
				}				
			}
			// ------------------Fin  Fodea 017-2013 ---------------------
			
			
		} catch (Exception e) {
			bOkActualiza = false;
			throw new AppException("Error al actualizar los limites por EPO", e);
		} finally {
			log.info("bactualizaLimites(S)");
			if (lobdConexion.hayConexionAbierta())  {
				lobdConexion.terminaTransaccion(bOkActualiza);
				lobdConexion.cierraConexionDB();
			}
		}
	}

	/**
	 * Query para obtener los datos de los limites por epo.
	 * @returns query
	 */
	private String ovgetLimitesQuery() {
		String lsQry = "Select /*+use_nl(lxe ml)*/ LXE.ic_epo, LXE.cg_razon_social, LXE.in_limite_epo, LXE.cs_activa_limite, "+
						" nvl(ML.enPesos,0)as montoLimite, "+
						" nvl(ml_d.endolar,0)as montoLimite_DL, "+
						/*INICIO: CODIGO A�ADIDO POR FODEA 051-2008*/
						" TO_CHAR(LXE.df_venc_linea,'DD/MM/YYYY') as fec_venc_linea, LXE.ig_aviso_venc_linea, "+
						" TO_CHAR(LXE.df_cambio_admon,'DD/MM/YYYY') as fec_cambio_admon, LXE.ig_aviso_cambio_admon, "+
						/*FIN: CODIGO A�ADIDO POR FODEA 051-2008*/
						" 'LimitesPorEPOBean.ovgetLimites()' as archivo, "+
						/*INICIO: CODIGO A�ADIDO POR FODEA 057-2009 FVR*/
						" NVL (mlc.montoComprometido, 0) AS limiteComprometido, "+
						" (NVL (mlc.montoComprometido, 0) + NVL (ml.enpesos, 0)) AS montoTotalLimite,  "+
						
						" NVL (mlc_d.montoComprometido_dl, 0) AS limiteComprometido_dl, "+
						" (NVL (mlc_d.montoComprometido_dl, 0) + NVL (ml_d.endolar, 0)) AS montoTotalLimite_dl "+
						
						" ,lxe.CS_FECHA_LIMITE as CS_FECHA_LIMITE "+  //Fodea 022-2011
						" ,lxe.IN_LIMITE_EPO_DL   "+ // Fodea 09-2014																
						" ,'N' as MONEDA_MN "+  // Fodea 09-2014	
						" ,'N' as MONEDA_DL "+	 // Fodea 09-2014	
						
						" , lxe.DESCRIPCION_ESTATUS as DESCRIPCION_ESTATUS   "+        
						" , lxe.FECHA_BLOQ_DESB as FECHA_BLOQ_DESB  "+
						" , lxe.USUARIO_BLOQ_DESB as USUARIO_BLOQ_DESB  "+
						" , lxe.DEPENDENCIA_BLOQ_DESB as DEPENDENCIA_BLOQ_DESB   "+  
						" , lxe.IC_IF_EPO_BLOQ_DES   as  IC_IF_EPO_BLOQ_DES  "  +
						" , lxe.AUX_ESTATUS   as  AUX_ESTATUS "  +
						" , 'N' as SELECCIONADOS	  "+
						" , lxe.IC_IF AS IC_IF  "+
						
						
						" from "+
						/*FIN: CODIGO A�ADIDO POR FODEA 057-2009 FVR*/
						" ( "+
						" select /*+index(ie) use_nl(ie e cpe)*/ IE.ic_epo, E.cg_razon_social, IE.in_limite_epo, IE.cs_activa_limite, "+
						/*INICIO: CODIGO A�ADIDO POR FODEA 051-2008*/
						" IE.df_venc_linea, IE.ig_aviso_venc_linea, IE.df_cambio_admon, IE.ig_aviso_cambio_admon "+
						/*FIN: CODIGO A�ADIDO POR FODEA 051-2008*/
						 ", IE.CS_FECHA_LIMITE " + //Fodea 022-2011
						 " ,IE.IN_LIMITE_EPO_DL " +// Fodea 09-2014		
						" ,'N' as MONEDA_MN "+  // Fodea 09-2014
						" ,'N' as MONEDA_DL "+  // Fodea 09-2014  
						
						" , decode(ie.CS_ACTIVA_LIMITE , 'S', 'Activa' ,'N' , 'Inactiva') as DESCRIPCION_ESTATUS   "+        
						" , TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy') as FECHA_BLOQ_DESB  "+
						" , ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB  "+
						" , ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+  
						" , ie.IC_IF_EPO_BLOQ_DES   as  IC_IF_EPO_BLOQ_DES  "  + 
						" , decode(ie.CS_ACTIVA_LIMITE , 'S', 'checked' ,'N' , '') as AUX_ESTATUS "+ 
						" , IE.ic_if as IC_IF "+
						 
						" from comrel_if_epo IE, comcat_epo E ,comrel_producto_epo cpe  "+						
						" where E.ic_epo = IE.ic_epo "+
						" and E.cs_habilitado='S' " +
						" and IE.cs_vobo_nafin = 'S' "+
						" and IE.cs_bloqueo = 'N'"+//FODEA 002 - 2009
						" and cpe.ic_epo=e.ic_epo "+
						" and cpe.ic_epo=ie.ic_epo "+
						" and cpe.ic_producto_nafin=1 "+
						" and cpe.cs_limite_pyme!='S' "+ 
						" and IE.ic_if = ? "+
						
						" ) LXE, "+
						" ( "+
						" select /*+index(d IN_COM_DOCUMENTO_04_NUK  ) use_nl(d ds)*/ D.ic_epo, sum(D.fn_monto_dscto ) as enPesos "+
						" from com_documento D, com_docto_seleccionado DS "+
						" where D.ic_epo = DS.ic_epo "+
						" and D.ic_documento = DS.ic_documento "+
						" and D.ic_estatus_docto in (3,4,8,24) "+
						" and D.ic_if = ? "+
						" and d.ic_moneda = 1   "+
						" group by D.ic_epo "+
						" ) ML,  "+
						"(SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK  ) use_nl(d ds)*/ " +
						" 	d.ic_epo, " +
						"  SUM (d.fn_monto_dscto * ds.fn_tipo_cambio) AS montoComprometido " +
						"  FROM com_documento d, com_docto_seleccionado ds " +
						"  WHERE d.ic_epo = ds.ic_epo " +
						"  AND d.ic_documento = ds.ic_documento " +
						"  AND d.ic_estatus_docto IN (26) " +
						"  AND d.ic_if = ? " +
						"  and d.ic_moneda = 1   "+
						"  GROUP BY d.ic_epo) mlc , "+
												
						" (SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK  ) use_nl(d ds)*/ "+
                  " d.ic_epo,  "+
                  " SUM (d.fn_monto_dscto ) AS enDolar "+
						" FROM com_documento d, com_docto_seleccionado ds  "+
						" WHERE d.ic_epo = ds.ic_epo  "+
						"  AND d.ic_documento = ds.ic_documento  "+
						" AND d.ic_estatus_docto IN (3, 4, 8, 24)  "+
						" AND d.ic_if = ?   "+
						" and d.ic_moneda = 54  "+
						" GROUP BY d.ic_epo) ml_d,   "+
          
						" (SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK  ) use_nl(d ds)*/   "+  
                  " d.ic_epo,   "+
						"  SUM (d.fn_monto_dscto    ) AS montocomprometido_dl   "+
						" FROM com_documento d, com_docto_seleccionado ds   "+
						"	WHERE d.ic_epo = ds.ic_epo  "+
						"	AND d.ic_documento = ds.ic_documento  "+
						" AND d.ic_estatus_docto IN (26)  "+
						" AND d.ic_if =  ?  "+  
						" and d.ic_moneda = 54   "+
						" GROUP BY d.ic_epo) mlc_d  "+    

						" where LXE.ic_epo = ML.ic_epo(+)  and lxe.ic_epo = mlc.ic_epo(+) "+
						" and  LXE.ic_epo = ML_d.ic_epo(+)  and lxe.ic_epo = mlc_d.ic_epo(+) "+
						
						" Order by LXE.cg_razon_social";
		return lsQry; 
	}

	/**
	 * Obtiene la informaci�n de los limites x EPO parametrizados por el IF
	 * @param esNoCliente Clave del Intermediario Financiero(ic_if)
	 * @return Vector con la informaci�n de los limites
	 */
	public Vector ovgetLimites(String esNoCliente) throws NafinException {
	Vector lovDatos = null;
	Vector lovLimitesEPOs = new Vector();
	AccesoDB lobdConexion = new AccesoDB();
	//con = new AccesoDB();
	//con.conexionDB();
	try{
		lobdConexion.conexionDB();

		String lsQry = this.ovgetLimitesQuery();

		System.out.println(":::::QUERY::::::"+lsQry);
		try {
			PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);
			ps.setString(1, esNoCliente);
			ps.setString(2, esNoCliente);
			ps.setString(3, esNoCliente);
			ResultSet lrsQry = ps.executeQuery();
			while(lrsQry.next()) {
				lovDatos = new Vector();
				lovDatos.add((lrsQry.getString("IC_EPO")==null?"":lrsQry.getString("IC_EPO").trim()));
				lovDatos.add((lrsQry.getString("CG_RAZON_SOCIAL")==null?"":lrsQry.getString("CG_RAZON_SOCIAL").trim()));
				lovDatos.add((lrsQry.getString("IN_LIMITE_EPO")==null?"":lrsQry.getString("IN_LIMITE_EPO").trim()));
				lovDatos.add((lrsQry.getString("CS_ACTIVA_LIMITE")==null?"":lrsQry.getString("CS_ACTIVA_LIMITE").trim()));
				lovDatos.add((lrsQry.getString("montoLimite")==null?"":lrsQry.getString("montoLimite").trim()));
				/*INCIO: CODIGO A�ADIDO POR FODEA 051-2008*/
				lovDatos.add((lrsQry.getString("fec_venc_linea")==null?"":lrsQry.getString("fec_venc_linea").trim()));
				lovDatos.add((lrsQry.getString("ig_aviso_venc_linea")==null?"":lrsQry.getString("ig_aviso_venc_linea").trim()));
				lovDatos.add((lrsQry.getString("fec_cambio_admon")==null?"":lrsQry.getString("fec_cambio_admon").trim()));
				lovDatos.add((lrsQry.getString("ig_aviso_cambio_admon")==null?"":lrsQry.getString("ig_aviso_cambio_admon").trim()));
				/*FIN: CODIGO A�ADIDO POR FODEA 051-2008*/
				/*INICIO: CODIGO A�ADIDO POR FODEA 057-2009*/
				lovDatos.add((lrsQry.getString("limiteComprometido")==null?"":lrsQry.getString("limiteComprometido").trim()));
				lovDatos.add((lrsQry.getString("montoTotalLimite")==null?"":lrsQry.getString("montoTotalLimite").trim()));
				/*FIN: CODIGO A�ADIDO POR FODEA 057-2009*/
				lovDatos.add((lrsQry.getString("CS_FECHA_LIMITE")==null?"":lrsQry.getString("CS_FECHA_LIMITE").trim())); //Fodea 022-2011
				lovDatos.add((lrsQry.getString("IN_LIMITE_EPO_DL")==null?"":lrsQry.getString("IN_LIMITE_EPO_DL").trim())); // Fodea 09-2014
				lovDatos.add((lrsQry.getString("MONEDA_MN")==null?"":lrsQry.getString("MONEDA_MN").trim())); //Fodea 09-2014
				lovDatos.add((lrsQry.getString("MONEDA_DL")==null?"":lrsQry.getString("MONEDA_DL").trim())); //Fodea 09-2014
					
				//System.out.println(":::::LOVDATOS::::"+lovDatos);
				lovLimitesEPOs.addElement(lovDatos);

			}
			lrsQry.close();
			ps.close();
			
			//lobdConexion.cierraX	Statement();
		} catch (SQLException sqle) { throw new NafinException("DSCT0032"); }

	} catch (NafinException ne) {
		throw ne;
	} catch (Exception e) {
		System.out.println("Exception en ovgetLimites(). "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
	}
	return lovLimitesEPOs;
	} // Fin Metodo ovgetLimites

	/**
	 * Obtiene la informaci�n de los limites x EPO parametrizados por el IF
	 * @param esNoCliente Clave del Intermediario Financiero(ic_if)
	 * @return Objeto Registros con la informaci�n de los limites
	 */
	public Registros ovgetLimitesExt(String esNoCliente) {
		log.debug("ovgetLimitesExt(E)");
		AccesoDB lobdConexion = new AccesoDB();
		Registros registros = new Registros();
		try{
			lobdConexion.conexionDB();
	
			String lsQry = this.ovgetLimitesQuery();
			
			List parametrosBind = new ArrayList();
			parametrosBind.add(new Integer(esNoCliente));
			parametrosBind.add(new Integer(esNoCliente));
			parametrosBind.add(new Integer(esNoCliente));
			parametrosBind.add(new Integer(esNoCliente));
			parametrosBind.add(new Integer(esNoCliente));
			log.debug("ovgetLimitesExt()::query: " + lsQry);
			log.debug("ovgetLimitesExt()::params: " + parametrosBind);
			registros = lobdConexion.consultarDB(lsQry, parametrosBind, true);
			return registros;
		} catch (Exception e) {
			throw new AppException("Error al obtener los limites por EPO", e);
		} finally {
			log.debug("ovgetLimitesExt(S)");
			if (lobdConexion.hayConexionAbierta()) {
				lobdConexion.cierraConexionDB();
			}
		}
	} // Fin Metodo ovgetLimitesExt




 public Vector ovgetLimitesPyME(String esNoCliente,String ic_epo,String ic_pyme) throws NafinException {
	Vector lovDatos = null;
	String condicion= "";
	Vector lovLimitesEPOs = new Vector();
	AccesoDB lobdConexion = new AccesoDB();

	try{
		lobdConexion.conexionDB();
		
		if(!"".equals(ic_pyme))
			 condicion+= " and P.ic_pyme=? ";

		String lsQry =    " SELECT DISTINCT p.ic_pyme, "+
						  " 	p.cg_razon_social, "+
						  "	lim_pyme.ic_pyme as ic_pyme_lim, "+
						  " nvl(lim_pyme.fn_limite,0) as LIMITE," +
                		  //" ROUND(((lim_pyme.fn_limite_utilizado/lim_pyme.fn_limite)*100),5) Porcentaje, "+
                		  " lim_pyme.cs_activa_limite , nvl(ML.enPesos,0)as montoLimite,  "+
                		  //" nvl((lim_pyme.fn_limite - lim_pyme.fn_limite_utilizado),0) as disponible, "+
                		  " nvl(lim_pyme.fn_limite_utilizado,0) as LIMITE_UTILIZADO, " +
							  " nvl(MLC.montoComp,0)as montoComprometido, "+
							  " (nvl(ML.enPesos,0) + nvl(MLC.montoComp,0)) as montoUtilComp "+
						  " FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb, comcat_pyme p,"+
						  "     (SELECT ic_pyme, fn_limite, fn_limite_utilizado, cs_activa_limite"+
						  "        FROM comrel_limite_pyme_if_x_epo"+
						  "      WHERE ic_if = ? "+
						  "         AND ic_epo = ? ) lim_pyme,"+
						  "  (select D.ic_pyme, sum(nvl(D.fn_monto_dscto * DS.fn_tipo_cambio,0)) as enPesos "+     
								  "  from com_documento D, com_docto_seleccionado DS "+     
								  "  where D.ic_if = DS.ic_if "+     
								  "  and D.ic_epo = DS.ic_epo "+    
								  "  and D.ic_documento = DS.ic_documento "+     
								  "  and D.ic_estatus_docto in (3,4,8,24) "+     
								  "  and D.ic_if = ?   "+ 
								  "  and D.ic_epo = ?   "+ 
								  "  group by D.ic_pyme) ML,    "+
						  " (select D.ic_pyme, sum(nvl(D.fn_monto_dscto * DS.fn_tipo_cambio,0)) as montoComp "+     
							  "  from com_documento D, com_docto_seleccionado DS "+     
							  "  where D.ic_if = DS.ic_if "+     
							  "  and D.ic_epo = DS.ic_epo "+    
							  "  and D.ic_documento = DS.ic_documento "+     
							  "  and D.ic_estatus_docto in (26) "+     
							  "  and D.ic_if = ?   "+ 
							  "  and D.ic_epo = ?   "+ 
							  "  group by D.ic_pyme) MLC    "+
						  " WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria "+
						  " AND cb.ic_pyme = p.ic_pyme "+
						  " AND pi.ic_epo = ? "+
						  " AND pi.ic_if = ? "+
						  " AND p.ic_pyme = lim_pyme.ic_pyme (+) "+
						  " AND cb.cs_borrado = 'N' "+
						  " AND pi.cs_borrado = 'N' "+ 
						  " AND pi.cs_vobo_if = 'S' "+ 
						  " and cb.ic_pyme=ML.ic_pyme(+) "+
						  " and cb.ic_pyme=MLC.ic_pyme(+) "+condicion;					

		//System.out.println(":::::QUERY::::::"+lsQry);
		try {
			PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);
			ps.setString(1, esNoCliente);
			ps.setString(2, ic_epo);
			ps.setString(3, esNoCliente);
			ps.setString(4, ic_epo);
			ps.setString(5, esNoCliente);
			ps.setString(6, ic_epo);
			ps.setString(7, ic_epo);
			ps.setString(8, esNoCliente);
			
			if(!"".equals(ic_pyme))
			  ps.setString(9, ic_pyme);
						
			ResultSet lrsQry = ps.executeQuery();
			ps.clearParameters();
			while(lrsQry.next()) {
				lovDatos = new Vector();
				lovDatos.add((lrsQry.getString("IC_PYME")==null?"":lrsQry.getString("IC_PYME").trim()));
				lovDatos.add((lrsQry.getString("CG_RAZON_SOCIAL")==null?"":lrsQry.getString("CG_RAZON_SOCIAL").trim()));
				lovDatos.add((lrsQry.getString("LIMITE")==null?"":lrsQry.getString("LIMITE").trim()));
				//lovDatos.add((lrsQry.getString("Porcentaje")==null?"":lrsQry.getString("Porcentaje").trim()));
				lovDatos.add((lrsQry.getString("CS_ACTIVA_LIMITE")==null?"":lrsQry.getString("CS_ACTIVA_LIMITE").trim()));
				lovDatos.add((lrsQry.getString("montoLimite")==null?"":lrsQry.getString("montoLimite").trim()));
				//lovDatos.add((lrsQry.getString("disponible")==null?"":lrsQry.getString("disponible").trim()));
				lovDatos.add((lrsQry.getString("LIMITE_UTILIZADO")==null?"":lrsQry.getString("LIMITE_UTILIZADO").trim()));
				lovDatos.add((lrsQry.getString("montoComprometido")==null?"":lrsQry.getString("montoComprometido").trim()));//FODEA 057-2009 FVR
				lovDatos.add((lrsQry.getString("montoUtilComp")==null?"":lrsQry.getString("montoUtilComp").trim()));//FODEA 057-2009 FVR
				//System.out.println(":::::LOVDATOS::::"+lovDatos);
				lovLimitesEPOs.addElement(lovDatos);

			}
			lrsQry.close();
			ps.close();
			//lobdConexion.cierraX	Statement();
		} catch (SQLException sqle) { throw new NafinException("DSCT0032"); }

	} catch (NafinException ne) {
		throw ne;
	} catch (Exception e) {
		System.out.println("Exception en ovgetLimitesPyME(). "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
	}
	return lovLimitesEPOs;
	} // Fin Metodo ovgetLimitesPyME


	public boolean bactualizaLimites_Pyme(String esLimitePYME[], String esIcEPO,
					   String esNoCliente, String esCsActivaLimite[], String esIcPyme[], String csActivaProgramado[],String csActivaProgramado2[]) throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		boolean bOkActualiza = true;
		String	qrySentencia	= "";
		String lsQry = "";
		int num	= 0;
		
		try{
			lobdConexion.conexionDB();
						
			for(int i=0; i < esIcPyme.length; i++) {
				
				qrySentencia = 
					" SELECT count(1) "+
					" FROM comrel_limite_pyme_if_x_epo "+
					" WHERE ic_epo =? "+
					" AND ic_if = ? "+
					" AND ic_pyme=? ";
			     System.out.println(qrySentencia);      
				PreparedStatement ps = lobdConexion.queryPrecompilado(qrySentencia);	
				ps.setInt(1, Integer.parseInt(esIcEPO));
				ps.setInt(2, Integer.parseInt(esNoCliente));
				ps.setInt(3, Integer.parseInt(esIcPyme[i]));
				ResultSet rs= ps.executeQuery();
				ps.clearParameters();					
				if(rs.next()) {
					num = rs.getInt(1);
				}
				rs.close();
				ps.close();
				//Si existe el registro, realiza el update						
				if(num>0) {	
					
					//INICIO: MODIFICACION FODEA 057-2009 FVR 
					if("S".equals(csActivaProgramado[i])  || "S".equals(csActivaProgramado2[i])){
						//SE REALIZA CAMBIO DE ESTATUS DE LOS DOCUMENTOS PROGRAMADOS-PYME a NEGOCIABLES
						lsQry = 
							"UPDATE com_documento " +
							"   SET ic_estatus_docto = ? " +
							" WHERE ic_epo = ? " +
							"   AND ic_pyme = ?  " +
							"   AND ic_estatus_docto = ? " +
							"   AND ic_documento IN ( " +
							"          SELECT cd.ic_documento " +
							"            FROM com_documento cd, com_docto_seleccionado cds " +
							"           WHERE cd.ic_documento = cds.ic_documento " +
							"				  AND cd.ic_epo = cds.ic_epo "+
							"             AND cds.ic_epo = ? " +
							"             AND cds.ic_if = ? " +
							"				  AND cd.ic_pyme = ? "+
							"             AND cd.ic_estatus_docto = ?) ";
						
						ps = lobdConexion.queryPrecompilado(lsQry);
						ps.setInt(1, 2);
						ps.setInt(2, Integer.parseInt(esIcEPO));
						ps.setInt(3, Integer.parseInt(esIcPyme[i]));
						ps.setInt(4, 26);
						ps.setInt(5, Integer.parseInt(esIcEPO));
						ps.setInt(6, Integer.parseInt(esNoCliente));
						ps.setInt(7, Integer.parseInt(esIcPyme[i]));
						ps.setInt(8, 26);
						ps.executeUpdate();
						ps.close();
						
					
					}//cierre de if (retornoProgramado)
					//FIN: MODIFICACION FODEA 057-2009 FVR 
					
					//SE REALIZA LA ACTUALIZACION DE LOS DATOS DEL LIMITE
					lsQry = 
						"update comrel_limite_pyme_if_x_epo"+
						" set fn_limite =   round(?,2) "+
						" ,  cs_activa_limite= ? "+
						" where ic_epo = ? and ic_if = ? and ic_pyme= ?";
					System.out.println(lsQry);
					//lobdConexion.ejecutaSQL(lsQry);
					ps = lobdConexion.queryPrecompilado(lsQry);
					ps.setDouble(1, Double.parseDouble(esLimitePYME[i]));
					ps.setString(2, esCsActivaLimite[i]);
					ps.setInt(3, Integer.parseInt(esIcEPO));
					ps.setInt(4, Integer.parseInt(esNoCliente));
					ps.setInt(5, Integer.parseInt(esIcPyme[i]));
					ps.executeUpdate();
					ps.close();
				//Si no existe el registro, inserta
				} else {
					lsQry= 
						" insert into comrel_limite_pyme_if_x_epo "+
						" ( ic_if,ic_epo,ic_pyme, fn_limite, cs_activa_limite) "+
						" values "+
						" (?,?,?,round(?,2),?)";
					System.out.println(lsQry);
				
					ps = lobdConexion.queryPrecompilado(lsQry);
					ps.setInt(1, Integer.parseInt(esNoCliente));
					ps.setInt(2, Integer.parseInt(esIcEPO));
					ps.setInt(3, Integer.parseInt(esIcPyme[i]));
					ps.setDouble(4, Double.parseDouble(esLimitePYME[i]));
					ps.setString(5, esCsActivaLimite[i]);
					ps.executeUpdate();
					ps.close();
				}//fin-else	
			}//for

		} catch (Exception e) {
			bOkActualiza = false;
			System.out.println("Exception en bactualizaLimitesPYME(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(bOkActualiza);
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return bOkActualiza;
	}
	
	 /**
	 * Este m�todo genera la consulta para Limites por IF como PYME y Limites por PYME como IF 
	 * @param ic_if clave del IF
	 * @param ic_epo clave de la EPO
	 * @param ic_pyme clave de la PYME
	 * @return lovDatos Vector con la informacion generada por la consulta (lsQry).
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */	
 public Vector ovgetLimitesPyME_C(String ic_if,String ic_epo,String ic_pyme) throws AppException {
	log.info("ovgetLimitesPyME_C (E)");
	Vector lovDatos = null;
	//	String condicion= "";
	StringBuffer condicion = new StringBuffer();
	StringBuffer lsQry = new StringBuffer();
	int cont=0;
	Vector lovLimitesEPOs = new Vector();
	AccesoDB lobdConexion = new AccesoDB();

	try{
		lobdConexion.conexionDB();
		
		if(!"".equals(ic_if))
			 condicion.append(" and lp.ic_if = ? ");
		
		if(!"".equals(ic_pyme))
			 condicion.append(" and lp.ic_pyme = ? ");
		
		
	lsQry.append(  "   SELECT (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as epo, "+
						"   (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as if,"+
						" (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as pyme, "+
						"	lp.fn_limite AS limite, "+
						"	ROUND(((nvl(lp.fn_limite_utilizado,0)/lp.fn_limite)*100),5) Porcentaje, "+
						"	(lp.fn_limite- nvl(lp.fn_limite_utilizado,0)) as utilizado,"+
						"	lp.cs_activa_limite as estatus, lp.fn_monto_comprometido AS monto_comprometido "+
						" ,ie.CS_FECHA_LIMITE as csFechalimite  "+	//Fodea 022-2011
						"	FROM comcat_pyme p, comrel_limite_pyme_if_x_epo lp, comcat_if i, comcat_epo e "+
						" , comrel_if_epo  ie  "+  //F022-2011
						"	WHERE lp.ic_pyme=p.ic_pyme "+
						"	AND lp.ic_epo=e.ic_epo "+
						"	AND lp.ic_if = i.ic_if "+
						"	AND lp.fn_limite != 0 "+
						"	AND lp.ic_epo=? "+condicion+
						" and ie.ic_epo = e.ic_epo "+ //F022-2011
				    " and ie.ic_if = i.ic_if "+ //F022-2011	
						"	ORDER BY p.cg_razon_social, i.cg_razon_social ");
		
		log.debug(":::::QUERY::::::"+lsQry);
		log.debug("ic_epo = "+ic_epo +", ic_if = "+ ic_if +", ic_pyme = "+ ic_pyme );
		try {
			PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry.toString());
			cont++;ps.setString(cont, ic_epo);
			if(!"".equals(ic_if)){
			cont++; ps.setString(cont, ic_if);
			}
			if(!"".equals(ic_pyme)){
			cont++; ps.setString(cont, ic_pyme);
			}		 	
						
			ResultSet lrsQry = ps.executeQuery();
			
			while(lrsQry.next()) {
				lovDatos = new Vector();
				lovDatos.add((lrsQry.getString("EPO")==null?"":lrsQry.getString("EPO").trim()));
				lovDatos.add((lrsQry.getString("IF")==null?"":lrsQry.getString("IF").trim()));
				lovDatos.add((lrsQry.getString("PYME")==null?"":lrsQry.getString("PYME").trim()));
				lovDatos.add((lrsQry.getString("LIMITE")==null?"":lrsQry.getString("LIMITE").trim()));
				lovDatos.add((lrsQry.getString("PORCENTAJE")==null?"":lrsQry.getString("PORCENTAJE").trim()));
				lovDatos.add((lrsQry.getString("UTILIZADO")==null?"":lrsQry.getString("UTILIZADO").trim()));
				lovDatos.add((lrsQry.getString("ESTATUS")==null?"":lrsQry.getString("ESTATUS").trim()));
				lovDatos.add((lrsQry.getString("MONTO_COMPROMETIDO")==null?"0.0":lrsQry.getString("MONTO_COMPROMETIDO").trim()));
				lovDatos.add((lrsQry.getString("csFechalimite")==null?"N":lrsQry.getString("csFechalimite").trim()));
				log.debug(":::::LOVDATOS::::"+lovDatos);
				lovLimitesEPOs.addElement(lovDatos);

			}
			//lobdConexion.cierraX	Statement();
			} catch (SQLException sqle) { throw new AppException("No Existen Limites asociados a alguna EPO para este IF."); }
													//throw new NafinException("DSCT0032");
			} catch (Exception e) {
				log.info("ovgetLimitesPyME_C -ERROR- ");
				e.printStackTrace();
			} finally {
				if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
			}
		log.info("ovgetLimitesPyME_C (S)");
		return lovLimitesEPOs;
	} // Fin Metodo ovgetLimitesPyME_C
	

	public void liberarLimitesxif(String valores, String fechaIni, String fechaFin) throws NafinException{

		System.out.println("liberarLimitesxif (E)");
		
		System.out.println("valores "+valores);
		System.out.println("fecha "+fechaIni);
		System.out.println("fecha "+fechaFin);
		
		
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		VectorTokenizer vt = null;
		Vector vecdat  = null;
		String qry = "Update com_documento set ic_estatus_docto = 11 " +
					" where df_fecha_venc >= trunc(to_date(?,'dd/mm/yyyy')) " +
					" and df_fecha_venc < trunc(to_date(?,'dd/mm/yyyy')+1) " +
					" and ic_if = ? and ic_moneda = ? and ic_estatus_docto = ? "+
					" and ic_epo = ? "+
					" and ic_documento = ? "; // Fodea 036 Mejoras II 2009  
		PreparedStatement psactualiza = null;
		try{
			con.conexionDB();
			valores += "|";
			psactualiza = con.queryPrecompilado(qry);
			StringTokenizer stvalores = new StringTokenizer(valores,"|");
			while(stvalores.hasMoreTokens()){
				String dato = stvalores.nextToken();
				vt = new VectorTokenizer(dato,",");
				vecdat = vt.getValuesVector();
				
				psactualiza.clearParameters();
				//int pcoma = dato.indexOf(',');
				String ic_if = vecdat.get(0).toString();//dato.substring(0,(pcoma));
				String ic_moneda = vecdat.get(1).toString(); //dato.substring((pcoma+1),dato.length());
				String ic_epo =  vecdat.get(2).toString();
				String documento =  vecdat.get(3).toString(); // Fodea 036 Mejoras II 2009 
				
				psactualiza.setString(1,fechaIni);
				psactualiza.setString(2,fechaFin);
				psactualiza.setInt(3,Integer.parseInt(ic_if));
				psactualiza.setInt(4,Integer.parseInt(ic_moneda));
				psactualiza.setInt(5,Integer.parseInt("4"));
				psactualiza.setInt(6,Integer.parseInt(ic_epo));
				psactualiza.setInt(7,Integer.parseInt(documento)); 
				
				psactualiza.executeUpdate();
			}
			psactualiza.close();
			
			System.out.println(" liberarLimitesxif ::qry::   "+qry);
			
			
			//con.cierraStatement();
		}catch(SQLException sqle){
			exito = false;
			System.out.println("\nError\n");
			sqle.printStackTrace();
		}catch(Exception e){
			exito = false;
			System.out.println("\nError\n"+e);
			e.printStackTrace();
		}finally{
			System.out.println("liberarLimitesxif (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}			
		}
	}


	public void bitacoraLimitesxif( String valores, String claveNombre, String noAcuse )   throws NafinException {
			log.info("bitacoraLimitesxif :::: (E)");
			
			AccesoDB con = new AccesoDB();
			boolean exito = true;
			StringBuffer strSQL =  new StringBuffer();
			
			
			String noDocumento = ""; 
			String montoDocumento = "";
			VectorTokenizer vt = null;
			Vector vecdat  = null;
			
			log.debug("valores :: "+valores);				
			log.debug("claveNombre :: "+claveNombre);
			log.debug("noAcuse ::"+noAcuse);
			
			try{
			con.conexionDB();
			PreparedStatement ps = null;
			
			strSQL.append(" INSERT INTO  BIT_BITACORA_LIBIF "+
						  "( DF_FECHA_PREPAGO, IC_DOCUMENTO, FN_MONTO_DOCUMENTO, CG_CLAVE_NOMBRE, IC_ACUSE ) "+
						  " VALUES(SYSDATE,?,?,?,?)");
						  
			valores += "|";
			
			ps = con.queryPrecompilado(strSQL.toString());
			
			StringTokenizer stvalores = new StringTokenizer(valores,"|");
			while(stvalores.hasMoreTokens()){
				String dato = stvalores.nextToken();
				vt = new VectorTokenizer(dato,",");
				vecdat = vt.getValuesVector();
				
				ps.clearParameters();
				
				noDocumento =  vecdat.get(3).toString(); 
				montoDocumento =  vecdat.get(4).toString(); 
			
				ps.setString(1,noDocumento);
				ps.setString(2,montoDocumento);
				ps.setString(3,claveNombre);
				ps.setString(4,noAcuse);
			
				ps.executeUpdate();
			}
				
			log.debug("strSQL:::::::: :: "+strSQL);			
			
			
			}catch(Exception e){
				exito = false;
				log.error("\nError\n"+e);
				e.printStackTrace();
			}finally{		
				log.info("bitacoraLimitesxif :::: (S)");
				if (con.hayConexionAbierta()){
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}	
			}
	}
	
	/**
	 * Valida si la EPO-IF tiene activa las monedas
	 * Moneda nacional y Dolares  
	 * Fodea 09-2014 Limites por EPO
	 * @return 
	 * @param ic_if
	 * @param ic_epo
	 */
	public HashMap  validaMonedas(String ic_epo , String ic_if){
	AccesoDB con = new AccesoDB();
		
	PreparedStatement ps	= null;
	ResultSet rs = null;	
	StringBuffer qrySentencia = new StringBuffer("");
	List lVarBind		= new ArrayList();
	HashMap 	resultado = new HashMap();
	String moneda ="", monedaDL =  "N",  monedaMN ="N";
	
	try{
		con.conexionDB();
						
		lVarBind		= new ArrayList();				
		qrySentencia.append( " Select distinct IC_MONEDA  FROM COMREL_IF_EPO_MONEDA   "+
									" where  ic_if  =  ? " );	
			lVarBind.add(ic_if);
			
			log.debug("ic_epo  "+ic_epo); 
		
		if(!ic_epo.equals("") )  {
			qrySentencia.append( " and ic_epo =  ? ");	
			lVarBind.add(ic_epo);	
		}
				      		
		log.debug("qrySentencia.toString()  "+qrySentencia.toString());
		log.debug("lVarBind  "+lVarBind);
		
		ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
		rs = ps.executeQuery();		
		while (rs.next()) {	
			moneda = rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA");
			if(moneda.equals("1") )  { monedaMN  ="S";  }
			if(moneda.equals("54") )  { monedaDL  ="S";  }
		}
		
		resultado.put("MONEDA_MN",monedaMN );
		resultado.put("MONEDA_DL",monedaDL );		
		
		rs.close();
		ps.close();
		con.cierraStatement();
			
	
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	return resultado;
}
	
}// Fin del Bean