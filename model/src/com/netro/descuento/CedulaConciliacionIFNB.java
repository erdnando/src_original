package com.netro.descuento;

import java.io.Serializable;

public class CedulaConciliacionIFNB implements Serializable {
	
	public CedulaConciliacionIFNB(){
	}

	/**
	 * Establece el tipo de credito
	 * @param strTipoCredito Tipo de Credito
	 */
	public void setTipoCredito(String strTipoCredito) {
		tipoCredito = strTipoCredito;
	}

	/**
	 * Establece el monto operado
	 * @param strMontoOperado Monto Operado
	 */
	public void setMontoOperado(String strMontoOperado) {
		montoOperado = strMontoOperado;
	}
	
	/**
	 * Establece el monto de capital vigente
	 * @param strCapitalVigente Capital Vigente
	 */
	public void setCapitalVigente(String strCapitalVigente) {
		capitalVigente = strCapitalVigente;
	}
	
	/**
	 * Establece el monto de capital vencido
	 * @param strCapitalVencido CapitalVencido
	 */
	public void setCapitalVencido(String strCapitalVencido) {
		capitalVencido = strCapitalVencido;
	}
	
	/**
	 * Establece el monto de interes vigente
	 * @param strInteresVigente Interes Vigente
	 */
	public void setInteresVigente(String strInteresVigente) {
		interesVigente = strInteresVigente;
	}
	
	/**
	 * Establece el monto de interes vencido
	 * @param strInteresVencido Interes Vencido
	 */
	public void setInteresVencido(String strInteresVencido) {
		interesVencido = strInteresVencido;
	}
	
	/**
	 * Establece el monto de interes moratorio
	 * @param strInteresMoratorio Interes Moratorio
	 */
	public void setInteresMoratorio(String strInteresMoratorio) {
		interesMoratorio = strInteresMoratorio;
	}
	
	/**
	 * Establece el monto total del adeudo
	 * @param strTotalAdeudo Monto Total del Adeudo
	 */
	public void setTotalAdeudo(String strTotalAdeudo) {
		totalAdeudo = strTotalAdeudo;
	}
	
	/**
	 * Establece la clave de SubAplicacion
	 * @param strTotalAdeudo Monto Total del Adeudo
	 */
	public void setClaveSubAplic(String strClaveSubAplic) {
		claveSubAplic = strClaveSubAplic;
	}
	
	/**
	 * Establece el saldo insoluto
	 * @param strTotalAdeudo Monto Total del Adeudo
	 */
	public void setSaldoInsoluto(String strSaldoInsoluto) {
		saldoInsoluto = strSaldoInsoluto;
	}
	
	/**
	 * Establece el numero de descuentos
	 * @param strDescuentos Descuentos
	 */
	public void setDescuentos(String strDescuentos) {
		descuentos = strDescuentos;
	}
	
	/**
	 * Establece la fecha de corte
	 * @param strFechaCorte Fecha de Corte
	 */
	public void setFechaCorte(String strFechaCorte) {
		fechaCorte = strFechaCorte;
	}

	/**
	 * Establece la clave del intermediario financiero
	 * @param strClaveIF Clave del IF
	 */
	public void setClaveIF(String strClaveIF) {
		claveIF = strClaveIF;
	}
	
	/**
	 * Establece la clave de la moneda
	 * @param strClaveMoneda Clave de la Moneda
	 */
	public void setMoneda(String strClaveMoneda) {
		claveMoneda = strClaveMoneda;
	}
	
	/**
	 * Establece la clave de la firma de cedula para el responsable de nafin
	 * @param strClaveFirmaNafin Clave de la Firma de Cedula de Nafin
	 */
	public void setClaveFirmaNafin(String strClaveFirmaNafin) {
		claveFirmaNafin = strClaveFirmaNafin;
	}
	
	/**
	 * Establece la clave de la firma de cedula para el responsable del if
	 * @param strClaveFirmaIF Clave de la Firma de Cedula del IF
	 */
	public void setClaveFirmaIF(String strClaveFirmaIF) {
		claveFirmaIF = strClaveFirmaIF;
	}

	/**
	 * Establece las observaciones
	 * @param strObservaciones Observaciones de la cedula
	 */
	public void setObservaciones(String strObservaciones) {
		observaciones = strObservaciones;
	}
	
	/**
	 * Establece el ic usuario
	 * @param stricusuario ic usuario de la cedula
	 */
	public void setIcUsuario(String stricusuario) {
		icUsuario = stricusuario;
	}
	
	/**
	 * Establece el nombre del usuario
	 * @param strnombreuser nombre del usuario de la cedula
	 */
	public void setNombreUsuario(String strnombreuser) {
		nombreUsuario = strnombreuser;
	}
	
	/**
	 * Establece la clave del certificado
	 * @param cvecert clave del certificado
	 */
	public void setCveCertificado(String cvecert) {
		cveCertificado = cvecert;
	}

	/**
	 * Obtiene el tipo de credito
	 * @return 	 Tipo de Credito de la Cedula de Conciliacion
	 */
	public String getTipoCredito() {
		return tipoCredito;
	}
		
	/**
	 * Obtiene el monto operado
	 * @return 	 Monto Operado de la Cedula de Conciliacion
	 */
	public String getMontoOperado() {
		return montoOperado;
	}

	/**
	 * Obtiene el monto del capital vigente
	 * @return Capital Vigente de la Cedula de Conciliacion
	 */
	public String getCapitalVigente(){
		return capitalVigente;
	}

	/**
	 * Obtiene el monto de capital Vencido
	 * @return Capital Vencido de la Cedula de Conciliacion
	 */
	public String getCapitalVencido() {
		return capitalVencido;
	}
	
	/**
	 * Obtiene el monto de interes vigente
	 * @return Interes Vigente de la Cedula de Conciliacion
	 */
	public String getInteresVigente() {
		return interesVigente;
	}

	/**
	 * Obtiene el monto de interes vencido
	 * @return Interes Vencido de la Cedula de Conciliacion
	 */
	public String getInteresVencido() {
		return interesVencido;
	}
	
	/**
	 * Obtiene el monto del interes moratorio
	 * @return Interes Moratorio de la Cedula de Conciliacion
	 */
	public String getInteresMoratorio() {
		return interesMoratorio;
	}
	
	/**
	 * Obtiene el monto total del adeudo
	 * @return Total Adeudo de la Cedula de Conciliacion
	 */
	public String getTotalAdeudo() {
		return totalAdeudo;
	}
	
	/**
	 * Obtiene la Clave del Subaplicacion
	 * @return Total Adeudo de la Cedula de Conciliacion
	 */
	public String getClaveSubAplic() {
		return claveSubAplic;
	}
	
	/**
	 * Obtiene el saldo insoluto
	 * @return Total Adeudo de la Cedula de Conciliacion
	 */
	public String getSaldoInsoluto() {
		return saldoInsoluto;
	}
	
	/**
	 * Obtiene el numero total de descuentos
	 * @return Descuentos
	 */	public String getDescuentos() {
		return descuentos;
	}
	
	/**
	 * Obtiene la Fecha de corte
	 * @return Fecha de Corte
	 */
	public String getFechaCorte() {
		return fechaCorte;
	}
	
	/**
	 * Obtiene la clave del IF
	 * @return Clave del IF
	 */
	public String getClaveIF() {
		return claveIF;
	}
	
	/**
	 * Obtiene la clave de la moneda
	 * @return Clave de la Moneda
	 */
	public String getClaveMoneda() {
		return claveMoneda;
	}

	/**
	 * Obtiene la clave de la firma de cedula de nafin
	 * @return Clave de la Firma de Cedula de Nafin
	 */
	public String getClaveFirmaNafin() {
		return claveFirmaNafin;
	}
	
	/**
	 * Obtiene la clave de la firma de cedula del IF
	 * @return Clave de la Firma de Cedula del IF
	 */
	public String getClaveFirmaIF() {
		return claveFirmaIF;
	}

	/**
	 * Obtiene las observaciones de la Cedula de Conciliacion
	 * @return Observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}
	
	/**
	 * Obtiene el ic usuario de la cedula del IF
	 * @return icusuario de la Cedula del IF
	 */
	public String getIcUsuario() {
		return icUsuario;
	}

	/**
	 * Obtiene el nombre de usuario de la Cedula de Conciliacion
	 * @return nombre de usuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	
	/**
	 * Obtiene la clave del certificado de la Cedula de Conciliacion
	 * @return clave del certificado
	 */
	public String getCveCertificado() {
		return cveCertificado;
	}
	
	
	/**
	 * Obtiene el query de insercion para la cedula de conciliacion
	 * @param strClaveCedulaConciliacion Clave de la cedula de conciliacion (PK)
	 * @return Cadena con la instruccion de SQL para insertar
	 * 		el registro.
	 */
	 
	public String getSQLInsert(String strClaveCedulaConciliacion) {
		
		StringBuffer campos = new StringBuffer();
		campos.append("ic_cedula_concilia");
		
		StringBuffer valoresCampos = new StringBuffer();
		valoresCampos.append(strClaveCedulaConciliacion);		

		campos.append(",df_carga");
		valoresCampos.append(",sysdate");
				
		if (tipoCredito != null) {
			campos.append(",cg_tipo_credito");
			valoresCampos.append(",'" + this.tipoCredito + "'");
		}
		if (montoOperado != null) {
			campos.append(",fg_monto_operado");
			valoresCampos.append("," + this.montoOperado);
		}
		if (capitalVigente != null) {
			campos.append(",fg_capital_vigente");
			valoresCampos.append("," + this.capitalVigente);
		}
		if (capitalVencido != null) {
			campos.append(",fg_capital_vencido");
			valoresCampos.append("," + this.capitalVencido);
		}
		if (interesVigente != null) {
			campos.append(",fg_interes_vigente");
			valoresCampos.append("," + this.interesVigente);
		}
		if (interesVencido != null) {
			campos.append(",fg_interes_vencido");
			valoresCampos.append("," + this.interesVencido);
		}
		if (interesMoratorio != null) {
			campos.append(",fg_interes_mora");
			valoresCampos.append("," + this.interesMoratorio);
		}
		if (totalAdeudo != null) {
			campos.append(",fg_total_adeudo");
			valoresCampos.append("," + this.totalAdeudo);
		}				
		if (descuentos != null) {
			campos.append(",ig_descuentos");
			valoresCampos.append("," + this.descuentos);
		}				
		if (fechaCorte != null) {
			campos.append(",df_fecha_corte");
			valoresCampos.append(
					",TO_DATE('" + this.fechaCorte + "', 'DD/MM/YYYY')");
		}
		if (claveIF != null) {
			campos.append(",ic_if");
			valoresCampos.append("," + this.claveIF);
		}
		if (claveMoneda != null) {
			campos.append(",ic_moneda");
			valoresCampos.append(",'" + this.claveMoneda + "'");
		}
		if (claveFirmaNafin != null) {
			campos.append(",ic_firma_cedula_naf");
			valoresCampos.append(",'" + this.claveFirmaNafin + "'");
		}
		if (claveFirmaIF != null) {
			campos.append(",ic_firma_cedula");
			valoresCampos.append(",'" + this.claveFirmaIF + "'");
		}
		if (observaciones != null) {
			campos.append(",cg_observaciones");
			valoresCampos.append(",'" + this.observaciones + "'");
		}
		if (claveSubAplic != null) {
			campos.append(",ig_subaplicacion");
			valoresCampos.append(",'" + this.claveSubAplic + "'");
		}
		if (saldoInsoluto != null) {
			campos.append(",fg_saldo_insoluto");
			valoresCampos.append(",'" + this.saldoInsoluto + "'");
		}
		//fodea 012
		if (icUsuario != null) {
			campos.append(",ic_usuario");
			valoresCampos.append(",'" + this.icUsuario + "'");
		}
		if (nombreUsuario != null) {
			campos.append(",cg_nombre_usuario");
			valoresCampos.append(",'" + this.nombreUsuario + "'");
		}
		if (cveCertificado != null) {
			campos.append(",ig_clave_certificado");
			valoresCampos.append(",'" + this.cveCertificado + "'");
		}	
    if (cveClienteExterno != null) {
			campos.append(",ic_cliente_externo");
			valoresCampos.append(",'" + this.cveClienteExterno + "'");
		}
    if (numeroSirac != null) {
			campos.append(",in_numero_sirac");
			valoresCampos.append(",'" + this.numeroSirac + "'");
		}
    if (tipoLinea != null) {
			campos.append(",cg_tipo_linea");
			valoresCampos.append(",'" + this.tipoLinea + "'");
		}
		
		String strSQL = 
				"INSERT INTO com_cedula_concilia (" + campos + ")" +
				" VALUES (" + valoresCampos + ")";
		
		return strSQL;		
	}
	private String tipoCredito;
	private String montoOperado;
	private String capitalVigente;
	private String capitalVencido;
	private String interesVigente;
	private String interesVencido;
	private String interesMoratorio;
	private String totalAdeudo;
	private String descuentos;
	private String fechaCorte;
	private String claveIF;
	private String claveMoneda;
	private String claveFirmaNafin;
	private String claveFirmaIF;
	private String observaciones;
	private String claveSubAplic;
	private String saldoInsoluto;
	private String icUsuario;
	private String nombreUsuario;
	private String cveCertificado;
  private String cveClienteExterno;
  private String numeroSirac;
  private String tipoLinea;


  public void setCveClienteExterno(String cveClienteExterno)
  {
    this.cveClienteExterno = cveClienteExterno;
  }


  public String getCveClienteExterno()
  {
    return cveClienteExterno;
  }


  public void setNumeroSirac(String numeroSirac)
  {
    this.numeroSirac = numeroSirac;
  }


  public String getNumeroSirac()
  {
    return numeroSirac;
  }


  public void setTipoLinea(String tipoLinea)
  {
    this.tipoLinea = tipoLinea;
  }


  public String getTipoLinea()
  {
    return tipoLinea;
  }
}
