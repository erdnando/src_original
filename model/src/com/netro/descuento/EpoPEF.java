package com.netro.descuento;

import java.util.List;

public class EpoPEF {

	public String getCamposPEF(){ 
    return ", DF_ENTREGA, CG_TIPO_COMPRA, CG_CLAVE_PRESUPUESTARIA, CG_PERIODO";
  }
	public String getCamposFormateadosPEF(){ 
    return ", TO_CHAR(DF_ENTREGA, 'dd/mm/yyyy') as DF_ENTREGA, CG_TIPO_COMPRA, CG_CLAVE_PRESUPUESTARIA, CG_PERIODO";
  }

	public String getCamposPEFFake(){ 
    return ", '' as DF_ENTREGA, '' as CG_TIPO_COMPRA, '' as CG_CLAVE_PRESUPUESTARIA, '' as CG_PERIODO";
  }

	public String getCamposDigitoIdentificador(){ 
    return " CG_DIGITO_IDENTIFICADOR ";
  }

	public String getCamposPEFOper(){ 
    return getCamposPEF() + ", " + getCamposDigitoIdentificador();
  }
  
	public String getCamposDigitoIdentificadorCalc(){ 
		return "  rellenaCeros(d.ic_epo||'','0000') || rellenaCeros(d.ic_documento||'','00000000000') as " + getCamposDigitoIdentificador();
	}

	public String getCamposDigitoIdentificadorFake(){ 
    return " '' as " + getCamposDigitoIdentificador();
  }

	public String getDescCamposPEF(){ 
    return ", Fecha de Recepci�n de Bienes y Servicios, Tipo de Compra (procedimiento), Clasificador por Objeto del Gasto, Plazo M�ximo";
  }

	public String getDescDigitoIdentificador(){ 
    return ", Digito Identificador";
  }

		
}//EpoPEF
