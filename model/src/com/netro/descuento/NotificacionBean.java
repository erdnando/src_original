package com.netro.descuento;


import com.netro.exception.NafinException;



import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Correo;
import netropology.utilerias.ServiceLocator;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "NotificacionBean", mappedName = "NotificacionEJB")
@TransactionManagement(TransactionManagementType.BEAN)
@Local
public class NotificacionBean implements Notificacion {
    @Resource
    SessionContext sessionContext;    
    private static final Log LOG = ServiceLocator.getInstance().getLog(com.netro.descuento.NotificacionBean.class);
    
    public NotificacionBean() {
    }
    
    @Override
    public void procesosaEjecutar(String ruta) throws NafinException {
        LOG.info("procesosaEjecutar (e)");
        
        Map<String, String> ulFecha = new HashMap<>();
        ulFecha = obtenerUltimaEjecucion();
        String fechaUl = (ulFecha.get("dfFechaEjecucion") != null? ulFecha.get("dfFechaEjecucion") : "");   
        
        try {
            if("".equals(fechaUl)){
                SimpleDateFormat sdfP = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                String dv = sdf.format(date);                
                String hr = "00:00:00";
                String fechaInicial = dv+" "+hr; 
                System.out.println("*** Fecha Hoy: "+fechaInicial);
                if(this.hayRegParaProcesar(fechaInicial) > 0){
                    this.avisoNotificacion(ruta);
                }else{
                    insertarBitN("No hubo L�neas de Cr�dito con cambio de estatus el d�a : ", "E");
                }
            }else{
                if(this.hayRegParaProcesar(fechaUl) > 0){
                    this.avisoNotificacion(ruta);
                }else{
                    insertarBitN("No hubo L�neas de Cr�dito con cambio de estatus el d�a : ", "E");
                }
            }
        } catch (Exception t) {
            throw new AppException("Error al enviar  notificacionEjecutivosNafinEstatusL ", t);
        }
        LOG.info("procesosaEjecutar (e)");
    }
    
    
    private int hayRegParaProcesar(String fecha) throws NafinException {
        LOG.info("hayRegParaProcesar (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int total = 0;
        System.out.println("**** "+fecha);

        try {
            con.conexionDB();
            qry = new StringBuilder();
            qry.append(" SELECT \n" + 
            "        COUNT(*) TOTAL\n" + 
            " FROM\n" + 
            "     comcat_epo e,\n" + 
            "     comrel_if_epo ie,\n" + 
            "     comcat_if i\n" + 
            " WHERE\n" + 
            "     ie.ic_epo = e.ic_epo\n" + 
            "     AND ie.ic_if = i.ic_if\n" + 
            "     AND ie.cs_vobo_nafin = ?\n" + 
            "     AND ie.df_bloq_des > TO_DATE(?, 'dd/MM/yyyy HH24:MI:SS') AND ie.df_bloq_des < sysdate\n" + 
            " ORDER BY\n" + 
            "     e.cg_razon_social,\n" + 
            "     i.cg_razon_social");
            
            lVarBind = new ArrayList();
            lVarBind.add("S");
            lVarBind.add(fecha); 

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt("TOTAL");
            }
           

        } catch (Exception e) {
            LOG.error("Exception en hayRegParaProcesar. " + e);
            throw new AppException("Error hayRegParaProcesar   ");
        } finally {
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }  
			
        }
        LOG.info("hayRegParaProcesar (S)");
        return total;
    }
   
    private void avisoNotificacion(String ruta) throws ParseException {     
        LOG.info("avisoNotificacion (e)");
        
        StringBuilder emailContacto = new StringBuilder();
        List datos = new ArrayList<>();
        StringBuilder mensaje = new StringBuilder();
        Map<String, String> epoif = new HashMap<>();
        
        List<String> email = new ArrayList<>();
        String lEmail = (obtenerCorreosEN() != null ? obtenerCorreosEN() : "");
		
		if("".equals(lEmail)){
			insertarBitN("No hubo correos electr�nicos parametrizados  ", "F");
		}else{		
			email = obtenerEmail(lEmail);        
			System.out.println("****  "+email.toString());
			Map<String, String> ulFecha = new HashMap<>();
			ulFecha = obtenerUltimaEjecucion();        
        
			String fechaUl = (ulFecha.get("dfFechaEjecucion") != null? ulFecha.get("dfFechaEjecucion") : "");        
        
			if("".equals(fechaUl)){
				
				String dv = (new SimpleDateFormat("dd/MM/yyyy")).format(new Date());            
				String hr = "00:00:00";
				String fechaInicial = dv+" "+hr;            
            
            
				if(!"".equals(lEmail)){
					datos = obtenerDatosLCredito(fechaInicial);
                
					for(int i = 0; i < datos.size(); i++){
						epoif = (HashMap) datos.get(i);                    
						mensaje.append("IC_EPO: "+epoif.get("IC_EPO").toString()+"; IC_IF: "+epoif.get("IC_IF")+" | ");
					}      	          
                
                
					for(String obj : email){
						emailContacto.append(emailContacto.length() > 0 ? ", " : " ").append(obj);
					}
					armadoEmail(ruta, emailContacto.toString(), datos);
               
					
					insertarBitProceso(mensaje.toString(), "E");
					mensaje.setLength(0);
					emailContacto.setLength(0);
					epoif.clear();
				}else{
	                
		            insertarBitProceso("No hubo correos electr�nicos parametrizados", "F");                
			    }           
			}else{
				
				datos = obtenerDatosLCredito(fechaUl);
            
				for(int j = 0; j < datos.size(); j++){
					epoif = (HashMap) datos.get(j);
					mensaje.append("IC_EPO: "+epoif.get("IC_EPO").toString()+"; IC_IF: "+epoif.get("IC_IF")+" | ");
				}
				for(String obj : email){
					emailContacto.append(emailContacto.length() > 0 ? ", " : " ").append(obj);
				}            
				armadoEmail(ruta, emailContacto.toString(), datos);
            
				
				insertarBitProceso(mensaje.toString(), "E");
				mensaje.setLength(0);
				emailContacto.setLength(0);
				epoif.clear();
			}
		}
        LOG.info("avisoNotificacion (s)");        
    }
    private String obtenerCorreosEN(){
        LOG.info("obtenerCorreosEN (e)");
        
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();              
        PreparedStatement ps = null;
        ResultSet rs = null;       
        String email = "";        

        try {
            con.conexionDB();
            qry = new StringBuilder();
            
            qry.append("SELECT  cg_emails FROM COM_EMAIL_DESTXEMAIL where ic_carga= (select max(ic_carga) from COM_EMAIL_DESTXEMAIL)");
            ps = con.queryPrecompilado(qry.toString());
            rs = ps.executeQuery();           
           
            if(rs.next()) {
                email = rs.getString("CG_EMAILS");                             
            }  
            
            

        } catch (Exception e) {
            LOG.error("Exception en obtenerCorreosEN. " + e);
            throw new AppException("Error obtenerCorreosEN   "+e);
        } finally {
            LOG.info("obtenerCorreosEN (S)");
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
			
        }
        LOG.info("obtenerCorreosEN (s)");
        return email;           
    }     
    
    private void armadoEmail(String ruta, String em, List datos) throws ParseException {
        LOG.info("armadoEmail (e)");
        
        Map<String, String> dat = new HashMap<>();
        
        try {
            Correo correo = new Correo();
            String remitente = "no_response@nafin.gob.mx";
            String asunto = "Cambio de Estatus L�neas de Cr�dito de Factoraje";
            StringBuilder msg = new StringBuilder();
            //StringBuilder emailContactoIFCC = new StringBuilder();
            String emailContacto = em;
           
            msg = new StringBuilder();
            msg.append(" <table width=\"550px\" border=\"0\"> ");            
            msg.append("<br><b> Estimado Ejecutivo Nafin </b>");
            msg.append("<br>   ");
            msg.append("<br>   ");
            msg.append("Por medio de la presente se le notifica que las siguientes l�neas de cr�dito de factoraje tuvieron un cambio de estatus:");            
            msg.append("<br>   ");
            msg.append("<br>   ");
            msg.append(" <table style=\"border: 1px solid; \">\n" + 
            "        <tr style=\"background-color: silver\">\n" + 
            "          <th>EPO</th>\n" + 
            "          <th>IF</th>\n" + 
            "          <th>Fecha y hora de cambio</th>\n" + 
            "          <th>Usuario</th>\n" + 
            "          <th>Origen</th>\n" + 
            "          <th>Nuevo Estatus</th>\n" + 
            "        </tr>\n");             
                for(int i = 0; i < datos.size(); i++){
                    msg.append("<tr>");
                    dat = (HashMap)datos.get(i);
                    msg.append("<td>"+dat.get("NOMBRE_EPO").toString()+"</td>\n");
                    msg.append("<td>"+dat.get("IF_NOMBRE").toString()+"</td>\n");
                    msg.append("<td style=\"text-align: center;\">"+dat.get("FECHA_BLOQ_DESB").toString()+"</td>\n");
                    msg.append("<td>"+dat.get("USUARIO_BLOQ_DESB").toString()+"</td>\n");
                    msg.append("<td style=\"text-align: center;\">"+dat.get("ORIGEN").toString()+"</td>\n");
                    msg.append("<td style=\"text-align: center;\">"+dat.get("NUEVOESTATUS").toString()+"</td>\n");
                    msg.append("</tr>");
                }            
            msg.append("\n" +             
            "      </table>");
            msg.append("<br>   ");
            msg.append("Estos cambios tambi�n los podr� consultar en Nafinet en la opci�n <b>Productos / Descuento / Consultas / L�mites por EPO </b>");            
            msg.append("</table> ");                                                                                         
                     
            try {                              
                correo.setCodificacion("charset=UTF-8");
                correo.enviaCorreoConDatosAdjuntos(remitente, emailContacto, null,
                                                             asunto, msg.toString(), null, null);
            }catch(Exception t) {
                LOG.error("Error al enviar correo armadoEmail" + t);
                throw new AppException("Error al enviar  armadoEmail ", t);
            }            

        } catch (Exception t) {                  
            throw new AppException("Error al enviar  armadoEmail ", t);
                 
        } finally {
            LOG.info("armadoEmail (s)");
        }              
    }
    
    private List<Map> obtenerDatosLCredito(String fechaI ){
        LOG.info("obtenerDatosLCredito (e)");
        
        List<Map> listEstatus = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();       
        PreparedStatement ps = null;
        ResultSet rs = null;       
        Map<String, String> inf = new HashMap<>();             
        
        System.out.println(fechaI);
        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append("SELECT /*+use_nl (e ie) index(ie)*/\n" + 
            "     ( DECODE(e.cs_habilitado,'N','*','S',' ')\n" + 
            "       || ' '\n" + 
            "       || e.cg_razon_social ) AS nombre_epo,\n" + 
            "        ( DECODE(i.cs_habilitado,'N','*','S',' ')\n" + 
            "       || ' '\n" + 
            "       || i.cg_razon_social ) if_nombre,\n" + 
            "\n" + 
            "     TO_CHAR(ie.df_bloq_des,'dd/MM/yyyy HH24:MI:SS') AS fecha_bloq_desb,\n" + 
            "     ie.cg_usuario_bloq_des        AS usuario_bloq_desb,\n" + 
            "     ie.cg_dependencia_bloq_des    AS dependencia_bloq_desb,\n" + 
            "     ie.cs_activa_limite as Estatus,\n" + 
            "     e.ic_epo as IC_EPO,\n" + 
            "     i.ic_if as IC_IF,\n" + 
            "     DECODE(ie.cs_activa_limite, 'N', 'INACTIVA', 'ACTIVA')  as NUEVOESTATUS,\n" + 
            "     CASE\n" + 
            "     WHEN ie.cg_dependencia_bloq_des = i.cg_razon_social THEN 'IF' ELSE 'EPO' END AS ORIGEN\n" + 
            " FROM\n" + 
            "     comcat_epo e,\n" + 
            "     comrel_if_epo ie,\n" + 
            "     comcat_if i\n" + 
            " WHERE\n" + 
            "     ie.ic_epo = e.ic_epo\n" + 
            "     AND ie.ic_if = i.ic_if\n" + 
            "     AND ie.cs_vobo_nafin = ?\n" + 
            "     AND (ie.df_bloq_des > TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') AND ie.df_bloq_des < sysdate)\n" + 
            " ORDER BY\n" + 
            "     e.cg_razon_social,\n" + 
            "     i.cg_razon_social");

            lVarBind = new ArrayList();
            lVarBind.add("S");
            lVarBind.add(fechaI);           
            
            
            LOG.debug("Query: "+qry.toString()+"  - lVarBind: "+lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();           
           
            while (rs.next()) {
                inf = new HashMap<>();
                inf.put("NOMBRE_EPO", rs.getString("NOMBRE_EPO"));
                inf.put("IF_NOMBRE", rs.getString("IF_NOMBRE"));
                inf.put("FECHA_BLOQ_DESB", rs.getString("FECHA_BLOQ_DESB"));
                inf.put("USUARIO_BLOQ_DESB", rs.getString("USUARIO_BLOQ_DESB"));
                inf.put("DEPENDENCIA_BLOQ_DESB", rs.getString("DEPENDENCIA_BLOQ_DESB")); 
                inf.put("NUEVOESTATUS", rs.getString("NUEVOESTATUS"));
                inf.put("ORIGEN", rs.getString("ORIGEN"));
                inf.put("IC_EPO", rs.getString("IC_EPO"));
                inf.put("IC_IF", rs.getString("IC_IF"));
                
                listEstatus.add(inf);               
            }            

        } catch (Exception e) {
            LOG.error("Exception en obtenerDatosLCredito. " + e);
            throw new AppException("Error obtenerDatosLCredito   "+e);
        } finally {
			
            LOG.info("obtenerDatosLCredito (S)");
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
			
        }       
        return listEstatus;           
    }  
    
    private static List<String> obtenerEmail(String linea) {
            StringBuilder correo = new StringBuilder();
            List<String> lis = new ArrayList<>();
            String separador = ";";
            String msg = linea.trim();
			String ms = "";
		
			if(!";".equals(msg.substring(msg.length()-1))) {
				ms = msg+";";
			}else {
				ms = msg;
			}
            
            for(int i = 0; i < ms.length(); i++) {
                if(separador.equals(ms.substring(i, i+1)) || i+1 == ms.length()) {                    
                    lis.add(correo.toString());                    
                    correo = new StringBuilder();
                }else {
                    correo.append(ms.substring(i, i+1));
                }                       
            }
            return lis;
        }
    
    private Map<String, String> obtenerUltimaEjecucion() {
        LOG.info("obtenerUltimaEjecucion (e)");
            
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;            
        Map<String, String> informacion = new HashMap<>();           

        try {
            con.conexionDB();
            qry = new StringBuilder();//TO_CHAR(CAL_FEC_LIM_ENT_E,'DD/MM/YYYY HH24:MI:SS') CAL_FEC_LIM_ENT_E
            qry.append("SELECT  to_char(df_fecha_ejecucion, 'dd/MM/yyyy HH24:MI:SS') as df_fecha_ejecucion, cs_estatus FROM com_bit_noti_lineas WHERE ic_bitacora = (SELECT MAX(ic_bitacora) FROM com_bit_noti_lineas where cs_estatus = ?)");
						              
            lVarBind = new ArrayList();
            lVarBind.add("E");                
            LOG.debug("Query: "+qry.toString()+" - lVArBind: "+lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                informacion = new HashMap<>();
                informacion.put("dfFechaEjecucion", rs.getString("df_fecha_ejecucion"));                
                informacion.put("csEstatus", rs.getString("cs_estatus"));  
            }
           

            } catch (Exception e) {               
                throw new AppException(" Error obtenerUltimaEjecucion "+e);
            } finally {                
                if (con.hayConexionAbierta()) {
					AccesoDB.cerrarResultSet(rs);
					AccesoDB.cerrarStatement(ps);
                    con.cierraConexionDB();
                }
				
            }
            LOG.info("obtenerUltimaEjecucion (s)");
            return informacion;

        }
    
    private void insertarBitProceso(String mensaje, String status) {
        LOG.info("insertarBitProceso (e)");
        
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
                 
        boolean exito = false;

        try {
            con.conexionDB();
            qry = new StringBuilder();
            qry.append("INSERT INTO COM_BIT_NOTI_LINEAS(IC_BITACORA, DF_FECHA_EJECUCION, MENSAJE, CS_ESTATUS) \n" + 
                    "    VALUES(COM_BIT_NOTI_LINEAS_SEQ.NEXTVAL, sysdate, ?, ?)");

            lVarBind = new ArrayList();
            lVarBind.add(mensaje);                
            lVarBind.add(status);             
            
            LOG.debug("Query: "+qry.toString()+" - lVarBind: "+lVarBind);    
            ps = con.queryPrecompilado(qry.toString(), lVarBind);                
            ps.executeUpdate();                
            ps.close();            
            exito = true;

            } catch (Exception e) {               
                throw new AppException(" Error insertarBitProceso "+e);
            } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }
            LOG.info("insertarBitProceso (s)");            
        }
    private void insertarBitN(String mensaje, String status) {
        LOG.info("insertarBitN (e)");
        
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
                 
        boolean exito = false;

        try {
            con.conexionDB();
            qry = new StringBuilder();
            qry.append("INSERT INTO COM_BIT_NOTI_LINEAS(IC_BITACORA, DF_FECHA_EJECUCION, MENSAJE, CS_ESTATUS) \n" + 
                    "    VALUES(COM_BIT_NOTI_LINEAS_SEQ.NEXTVAL, sysdate, concat(concat(?, ' '), to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS')), ?)");

            lVarBind = new ArrayList();
            lVarBind.add(mensaje);                
            lVarBind.add(status);             
            
            LOG.debug("Query: "+qry.toString()+" - lVarBind: "+lVarBind);    
            ps = con.queryPrecompilado(qry.toString(), lVarBind);                
            ps.executeUpdate();                
            ps.close();            
            exito = true;

            } catch (Exception e) {               
                throw new AppException(" Error insertarBitN "+e);
            } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }
            LOG.info("insertarBitN (s)");            
        }
    
}
