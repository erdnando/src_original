package com.netro.descuento;

public interface EpoEnlOperadosArch{

	public String getNombreArchivo();

	public abstract String getNombreEnlace();

	public abstract String getRutaArchivo();
}
