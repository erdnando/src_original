package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class CFEEnlace extends EpoPEF implements EpoEnlace,EpoCentralizada,Serializable {

	private String ic_epo = "";
	public CFEEnlace(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDocumentos()	{ return "COMTMP_DOCTOS_PUB_CFE";		}
	public String getTablaAcuse()		{ return "COM_DOCTOS_PUB_ACU_CFE";		}
	public String getTablaErrores()		{ return "COM_DOCTOS_ERR_PUB_CFE";		}
	public String getTablaDoctosAcuse()	{ return "COM_DOCTOS_PUB_ACU_CFE_DET";	}

	public String getDocuments(){

		return  "  SELECT IG_NUMERO_DOCTO"   +
				"  , CG_PYME_EPO_INTERNO"   +
				"  , DF_FECHA_DOCTO"   +
				"  , DF_FECHA_VENC"   +
				"  , IC_MONEDA"   +
				"  , FN_MONTO"   +
				"  , CS_DSCTO_ESPECIAL"   +
				"  , CT_REFERENCIA"   +
				"  , CG_CAMPO1"   +
				"  , cg_campo2"   +
				"  , cg_campo3"   +
				"  , cg_campo4"   +
				"  , trim(cg_campo5) as cg_campo5"   +
				"  , '' AS ic_if"   +
				"  , '' AS IC_NAFIN_ELECTRONICO"   +getCamposPEF()+
				"  FROM "+getTablaDocumentos();
	}
	public String getInsertaErrores(ErroresEnl err){
		/*return	"insert into "+getTablaErrores()+" (IC_EPO, IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR) "+
				" values("+ic_epo+",'"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"')";*/
		return	"insert into "+getTablaErrores()+
				" (IG_NUMERO_DOCTO, IG_NUMERO_ERROR, CG_ERROR, CG_CAMPO1, CG_CAMPO2,CC_ACUSE, " + getCamposDigitoIdentificador() + ") "+
				" values('"+err.getIgNumeroDocto()+"',"+err.getIgNumeroError()+",'"+err.getCgError()+"', "+ err.getInAEjercicio() +", "+ err.getInSociedad() +
				",'"+err.getCcAcuse()+"'"+ ((err.getCgDigitoIdentificador()==null)?",null":",'"+err.getCgDigitoIdentificador()+"'")+")";
	}


	public String getUpdateAcuse(AcuseEnl acu){
		/*return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+",FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+",IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+",FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+",IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+"' where CC_ACUSE='"+acu.getCcAcuse()+"' and ic_epo ="+ic_epo;*/
		return	" update "+getTablaAcuse()+" set IN_TOTAL_PROC="+acu.getInTotalProc()+",FN_TOTAL_MONTO_MN="+acu.getFnTotalMontoMn()+
				" ,IN_TOTAL_ACEP_MN="+acu.getInTotalAcepMn()+",IN_TOTAL_RECH_MN="+acu.getInTotalRechMn()+",FN_TOTAL_MONTO_DL="+acu.getFnTotalMontoDl()+
				" ,IN_TOTAL_ACEP_DL="+acu.getInTotalAcepDl()+",IN_TOTAL_RECH_DL="+acu.getInTotalRechDl()+
				" ,CS_ESTATUS='"+acu.getCsEstatus()+"' where CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}
	public String getInsertAcuse(AcuseEnl acu){
		/*return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+ic_epo+","+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";*/
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_TOTAL_PROC, FN_TOTAL_MONTO_MN"+
				", IN_TOTAL_ACEP_MN, IN_TOTAL_RECH_MN, FN_TOTAL_MONTO_DL, IN_TOTAL_ACEP_DL"+
				", IN_TOTAL_RECH_DL, DF_FECHAHORA_CARGA, CS_ESTATUS) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getInTotalProc()+","+acu.getFnTotalMontoMn()+
				","+acu.getInTotalAcepMn()+","+acu.getInTotalRechMn()+","+acu.getFnTotalMontoDl()+","+acu.getInTotalAcepDl()+
				","+acu.getInTotalRechDl()+",SYSDATE,"+acu.getCsEstatus()+")";
	}

	public String getCondicionQuery(){
		//return " where ic_epo = "+ic_epo;
		return " ";
	}
  public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }

}
