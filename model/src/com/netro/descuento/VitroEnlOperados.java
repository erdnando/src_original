package com.netro.descuento;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class VitroEnlOperados implements EpoEnlOperados, Serializable  {

	private String ic_epo = "";

	public VitroEnlOperados(String ic_epo){
		this.ic_epo = ic_epo;
	}

	public String getTablaDoctosOpe(){
		return "COM_DOCTOS_OPE_VITRO";
	}

	public String getBorraDoctosOpe(){
		return "delete "+getTablaDoctosOpe();

	}
	public String getBorraDoctosVenc(){
		return "";
	}
	public String getBorraDoctosBaja(){
		return "";
	}

	public String getInsertaDoctosOpe(){
		return 
			"INSERT INTO "+getTablaDoctosOpe()+" " +
			"            (ic_epo, cg_razon_social_if, df_fecha_aut, cc_acuse, in_numero_proveedor, " +
			"             cg_razon_social_pyme, ig_numero_docto, df_fecha_docto, " +
			"             df_fecha_venc, ic_moneda, fn_monto, cg_sucursal, cg_banco, " +
			"             cg_cuenta_bco, fn_porc_dscto, fn_monto_dscto, in_tasa_aceptada, " +
			"             ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, " +
			"             cg_campo5 " +
			"            ) ";

	}


	public String getDoctosOperados(Hashtable alParamEPO){
		return	
			"SELECT d.ic_epo, SUBSTR (i.cg_razon_social, 1, 50) cg_razon_social_if, a.df_fecha_hora, " +
			"       a.cc_acuse, pe.cg_pyme_epo_interno, " +
			"       SUBSTR (py.cg_razon_social, 1, 70) cg_razon_social_pyme, " +
			"       d.ig_numero_docto, d.df_fecha_docto, d.df_fecha_venc, d.ic_moneda, " +
			"       d.fn_monto, cb.cg_sucursal, cb.cg_banco, cb.cg_numero_cuenta, " +
			"       d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_tasa_aceptada, " +
			"       d.ct_referencia, d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, " +
			"       d.cg_campo5 " +
			"  FROM com_documento d, " +
			"       com_docto_seleccionado ds, " +
			"       com_solicitud s, " +
			"       com_acuse2 a, " +
			"       comcat_if i, " +
			"       comrel_pyme_epo pe, " +
			"       comrel_pyme_if pi, " +
			"       comrel_cuenta_bancaria cb, " +
			"       comcat_pyme py, " +
			"       comrel_if_epo ie " +
			" WHERE a.cc_acuse = ds.cc_acuse " +
			"   AND s.ic_documento = ds.ic_documento " +
			"   AND ds.ic_documento = d.ic_documento " +
			"   AND d.ic_if = i.ic_if " +
			"   AND d.ic_pyme = pe.ic_pyme " +
			"   AND d.ic_epo = pe.ic_epo " +
			"   AND d.ic_epo = pi.ic_epo " +
			"   AND ds.ic_if = pi.ic_if " +
			"   AND d.ic_moneda = cb.ic_moneda " +
			"   AND d.ic_pyme = cb.ic_pyme " +
			"   AND d.ic_pyme = py.ic_pyme " +
			"   AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
			"   AND d.ic_epo = ie.ic_epo " +
			"   AND d.ic_if = ie.ic_if " +
			"   AND pi.cs_vobo_if = 'S' " +
			"   AND pi.cs_borrado = 'N' " +
			"   AND cb.cs_borrado = 'N' " +
			"   AND d.ic_estatus_docto IN (4, 16) " +
			"   AND s.df_fecha_solicitud >= TRUNC (SYSDATE) " +
			"   AND s.df_fecha_solicitud < TRUNC (SYSDATE) + 1 " +
			"   AND d.ic_epo = " + ic_epo;
	}

	public String getDoctosEliminacion(Hashtable alParamEPO){
		return "";
	}

	public String getDoctosVencidoSinOperar(Hashtable alParamEPO, int diasMinimos){
		return "";
	}
	
}
