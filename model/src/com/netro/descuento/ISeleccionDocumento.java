/*************************************************************************************
*
* Nombre de Clase o Archivo: ISeleccionDocumento.java
*
* Versión: 		  1.0
*
* Fecha Creación: 8/febrero/2002
*
* Autor:          Cindy Ramos Pérez
*
* Fecha Ult. Modificación:
*
* Descripción de Clase: Interface remota del EJB de Descuentos de Documentos.
*
*************************************************************************************/

package com.netro.descuento;

import netropology.utilerias.*;
import java.util.*;
import com.netro.exception.*;
import java.math.BigDecimal;
import javax.ejb.Remote;

@Remote
public interface ISeleccionDocumento {

	public void vverificarHorario(int eiProductoNafin, String esTipoUsuario, String epo)
		throws NafinException, Exception;

	public Vector ovgetTasaAceptada(String esClavePyme, String esClaveEpo, String esClaveMoneda, String esClaveIf)
		throws NafinException;

	public Vector ovgetIntermediariosRelacionados(String esClavePyme, String esClaveEpo,
		String esClaveMoneda, String esIntermediarios)
		throws NafinException;

	public Vector ovgetIntermediariosRelacionados(String esClavePyme, String esClaveEpo,
		String esClaveMoneda, String esIntermediarios, String tipoPiso)
		throws NafinException;

	public Vector ovgetIntermediariosRelacionados(String esClavePyme, String esClaveEpo,
		String esClaveMoneda, String esIntermediarios,String tipoPiso, String tipoFact)
		throws NafinException;

	public float fgetTipoCambio(String esClaveMoneda)
		throws NafinException;

	public Vector ovgetDocumentosDescontar(String esClavePyme, String esClaveMoneda,
								String esClaveEpo, String esValorTasaAplicar, String esFechaVencDe,
								String esFechaVenca, String esIntermediario, String esTipoPiso)
				throws NafinException;

	public Vector ovgetDocumentosDescontar(String esClavePyme, String esClaveMoneda,
								String esClaveEpo, Vector evTasaAplicarPlazo, String esFechaVencDe,
								String esFechaVenca, String esIntermediario, String esTipoPiso)
				throws NafinException;

	public Vector ovgetDocumentosDescontar(String esClavePyme, String esClaveMoneda,
								String esClaveEpo, Vector evTasaAplicarPlazo, String esFechaVencDe,
								String esFechaVenca, String esIntermediario, String esTipoPiso,String esEstatusDocto)
				throws NafinException;

	public Vector ovGetDoctosCobManual(String esClavePyme, String esClaveMoneda,
			String esClaveEpo, Vector evTasaAplicarPlazo, String esFechaVencDe,
			String esFechaVenca, String esIntermediario, String esTipoPiso,
			String esEstatusDocto,String orden,String ascDesc )
				throws NafinException;

	public void vvalidarUsuario(String esClavePyme, String esLogin, String esPassword,
		 String esIP, String esHost, String esSistema )
		throws NafinException;

/*	public void vrealizarTransaccion( String esClaveMoneda, String esNoUsuario, String esTotalDescuento,
									 String esTotalIntereses, String esTotalRecibir, String esDoctosSeleccionados[],
								     String esdcFechaTasa, String esTasaAceptada, String esCgRelMat,
								     String esCgPuntos, String esValorTC, String  esCveEpo )
				throws NafinException;
*/

	public void vrealizarProgramacion(
			String esClaveMoneda, String esNoUsuario,
			String esAcuse,	String esTotalDescuento,
			String esDoctosSeleccionados, String esValorTC,
			String esCveEpo ) throws NafinException;

	/*public void vrealizarSeleccion( String esClaveMoneda, String iNoUsuario, String esAcuse,
								   String esTotalDescuento, String esTotalIntereses, String esTotalRecibir,
								   String esDoctosSeleccionados[], String esDcFechaTasa, String esTasaAceptada,
								   String esCgRelMat, String esCgPuntos, String esValorTC, String esCveEpo,
								   String esTipoTasa, String esPuntosPref )
				throws NafinException;*/

	public void vrealizarSeleccion( String esClaveMoneda, String iNoUsuario, String esAcuse,
								   String esTotalDescuento, String esTotalIntereses, String esTotalRecibir,
								   String esDoctosSeleccionados[], String esValorTC, String esCveEpo )
				throws NafinException;

	public Hashtable vrealizarCobranzaManual(  String esClaveMoneda, String esNoUsuario, String esAcuse,
		String esTotalDescuento, String esTotalIntereses, String esTotalRecibir,
		String esDoctosSeleccionados[], String esValorTC, String esCveEpo,String numPrestamo,String esCvePyme,String esNombreEpo,String saldoIntNormal,String saldoIntMora,String saldoTotal,String totalAplicar )
				throws NafinException;

	public Vector ovconsultarEPO( String esCvePyme ) throws NafinException;

	public Vector ovconsultarMoneda() throws NafinException;

	public void vvalidaFV( String esClaveEpo )
		throws NafinException;
	public boolean manejaFactoraje( String claveEpo, String  tipoFactoraje);
	public void vvalidaFD( String esClaveEpo )
		throws NafinException;

	public Vector ovconsultarEPOFV( String esCvePyme )
		throws NafinException;

	public Vector ovgetDocumentosDescontarFV(String esClavePyme, String esClaveMoneda,
	 									   String esClaveEpo, String esIntermediariosFin,
	 									   String esFechaVencDe, String esFechaVenca,
                       						Vector evTasasPlazo)
		throws NafinException;

	public Vector ovgetDocumentosDescontarFD(String esClavePyme, String esClaveMoneda,
				String esClaveEpo, Vector evTasaAplicarPlazo, String esFechaVencDe,
				String esFechaVenca, String esIntermediario, String esTipoPiso, String  icBeneficiario )
		throws NafinException;

	public List ovgetDocumentosDescontarxProgramacion(
			String esClavePyme, String esClaveMoneda,
			String esClaveEpo, String esFechaVencDe,
			String esFechaVencA, String esIntermediario,
			String esTipoPiso) throws NafinException;

	public boolean bDocumentosAplicados(String esClavePyme)
		throws NafinException;

	public Vector ovgetDetalleDocumentoAplicado(String esClavePyme,
			String esClaveDocumento)
			throws NafinException;

	public Vector ovgetDetalleDocumentosAplicados(String esClavePyme)
		throws NafinException;

	public Vector ovgetDocumentosAplicados(String esClavePyme)
		throws NafinException;

	public void vrealizarNotificacion(String esClavePyme)
		throws NafinException;

	public boolean bDocumentosAplicadosCI(String esClavePyme)
		throws NafinException;


	public Vector ovgetDocumentosAplicadosCI(String esClavePyme)
		throws NafinException;

	public Vector ovgetReporteDetalleDocumentosAplicados(String esClavePyme,
		String esClaveEpo, String esClaveIf) throws NafinException;


	public void vrealizarNotificacionCI(String esClavePyme)
		throws NafinException;

	public Vector vObtenerIfAutorizacionAutomatica()
		throws NafinException;

	public StringBuffer ObtenerArchAutAutomatica(String ic_if, String estatus,String fechas[])
		throws NafinException;

	public void cambiaStatusAProcesoAutorizacionIF(String sIcDocumentos)
		throws NafinException;

	public void vRealizarDescuentoAutomatico(String icEpo,String icPyme,String icIf,int proceso, String tipoDescuento, String modalidad, String ruta, String ruta2)
		throws NafinException, Exception;

    public abstract Vector ovcalculaInteres(BigDecimal ebMonto, int eiPlazo, Vector evTasasAplicar, String esTipoPiso )
		throws Exception;


	public String ovgetIntermediariosOperadoVenc(String esClavePyme, String esClaveEpo,
												 String esIntermediarios)
		throws Exception;

	public abstract Vector getSubsidiosXPyme(String ic_pyme, String ic_epo) throws Exception;
	public abstract Vector getSubsidiosXPyme(String ic_documento[]) throws Exception;

	public String getLimiteActivo(String sNoEpo, String sNoIf)
		throws Exception;

	public String getLimiteActivo(String sNoEpo, String sNoIf, String sNoPyme)
		throws Exception;

	public abstract ArrayList getDocumentosProgramados(String sNoPyme) throws Exception;

	public String getDiasInhabiles(String sNoEpo) throws NafinException;


	public abstract void setFechaProgramacion(String[] sNoDocumento, String[] sFechaProgDscto) throws Exception;

	public abstract void vDescAutoProgramados(String icEpo,String icPyme,String icIf)
 		throws Exception;

	public Map getResumenCobMan(String ic_pyme,String ordenaPor,String ascDesc)
 		throws Exception;

	public List getNombreCamposDinamicos(String claveEpo)
		throws java.sql.SQLException, javax.naming.NamingException;

	public int getHoraActual();

	public void vrealizarSeleccion(
			String esClaveMoneda, String esNoUsuario, String esAcuse,
			String esTotalDescuento, String esTotalIntereses, String esTotalRecibir,
			String esDoctosSeleccionados[], String esValorTC, String esCveEpo,
			String esTipoLimite )
 		throws Exception;

	public List getDetalleAmortPrestamo(String numPrestamo)
 		throws Exception;

	public String operaFechaVencPyme(String ic_epo)
 		throws Exception;

	public boolean bgetExisteParamDocs(String esClaveEpo)
			throws NafinException;



	public Vector ovgetDocumentosDescontarxMandato(
				String esClavePyme, String esClaveMoneda,
				String esClaveEpo, String esFechaVencDe,
				String esFechaVencA, Vector vTasaAceptada) throws NafinException;

	public String operaEpoConMandato(String cve_pyme, String cve_epo, String claveMoneda) throws NafinException;//FODEA 015 - 2011 ACF
	//public String operaEpoConMandato(String cve_pyme, String cve_epo) throws NafinException;
	//public void descuentoAutomaticoMandato(String icEpo, String icPyme, String icIf, int proceso) throws NafinException, Exception;//FODEA 041-2009


	public List ovgetDocumentosDescontarVI(String esClavePyme, String esClaveMoneda,
            String esClaveEpo, String esIntermediariosFin, String esFechaVencDe,
            String esFechaVenca, Vector evTasasPlazo )
			throws NafinException;

	/*FODEA 005-2009*/
	public boolean getDocVencSinOperDiaSigHabil(String ic_documento, String ic_epo, String fecha)
		throws NafinException;

	public Vector ovgetTasaAceptada(String esClavePyme, String esClaveEpo, String esClaveMoneda, String esClaveIf, String fecDiaSigHab)
		throws NafinException;

	public Vector ovgetDocumentosDescontarxMandato(
				String esClavePyme, String esClaveMoneda,
				String esClaveEpo, String esFechaVencDe,
				String esFechaVencA, Vector vTasaAceptada, String cveMandante) throws NafinException;
	/*-*/
	/*FODEA 016-2010 FVR*/
	public boolean operaEpoFactoraje24hrs(String icEpo ) throws NafinException;
	public boolean operaIfFactoraje24hrs(String icIf ) throws NafinException;

	// Fodea 002 - 2010
	public String getNumerosDeDocumento(String ic_nota_credito, String delimitador)
		throws AppException;
	public boolean operaNotasDeCreditoParaDescuentoElectronico(String ic_epo)
		throws AppException;
	public boolean hasAplicarNotaDeCreditoAVariosDoctosEnabled(String ic_epo)
		throws AppException;

	//FODEA 024-2010 FVR
	public boolean operaEpoDsectoAutUltimoDia(String icEpo ) throws NafinException;

	//FODEA 032-2010 FVR
	public boolean operaFactDistAutorizaCtasEntidades(String icEpo, String icIf, String beneficiario, String icMoneda ) throws NafinException;

	//FODEA 017-2011 FVR
	public String getBancoDeFondeo(String claveEPO)throws AppException;
	public HashMap getLimites(String esClavePyme, String esClaveEpo, String esClaveMoneda, String esClaveIF) throws AppException;
	public List getNombreCamposDinamicosJson(String claveEpo)throws AppException;
	public boolean parametrizacionEpoPef(String icEpo) throws AppException;
	public List valoresEpoPef(String icDocumento, String icEpo, String icPyme) throws AppException;
	public List ovgetIntermediariosRelacionadosJson(String esClavePyme, String esClaveEpo, String esClaveMoneda,
					String esIntermediarios, String tipoPiso, String tipoFact)throws NafinException;
	public List getValoresCamposDinamicos(String icDocumento, String icEpo, String icPyme) throws AppException;
	public int getTotalCamposAdicionales(String ic_epo);
	public String getNumeroDoctoAsociado(String ic_nota_credito);
	public String getNumeroNotaCreditoAsociada(String ic_documento);

	public Registros getResumenPublicacionXEpo(String clavePyme);


	/*FODEA 031-2011*/
	public Vector ovgetTasaAceptadaOferta(String esClavePyme, String esClaveEpo, String esClaveMoneda, String esClaveIf, String fecDiaSigHab, String cveOfertaTasa)
		throws NafinException;

	public Vector ovgetTotalMontoDocumentosDescontar(
		String esClavePyme, 		String esClaveMoneda, 	String esClaveEpo,
		String esFechaVencDe, 	String esFechaVenca, String esIntermediario, String esTipoPiso, String esEstatusDocto ) throws NafinException;

	public HashMap ovgetTotalDocumentosDescontar(
				String esClavePyme, 		String esClaveMoneda, 	String esClaveEpo,
				Vector evTasaAplicarPlazo, 	String esFechaVencDe, 	String esFechaVenca,
				String esIntermediario, 	String esTipoPiso, 		String esEstatusDocto ) throws NafinException;

	public List getIntermediariosConOfertaDeTasas(String cvePyme, String cveEpo, String cveMoneda, List Interm, String montoTotalDocto) throws AppException;
	public boolean existeResumenOperacion(String clavePyme);
	public void generarResumenOperacion(String clavePyme);
	public List generarXmlEstadisticas(String clavePyme);
	public boolean hayOfertaDeTasas(String iNoCliente, String cveMoneda, String cveEpo);

	public void procAsignaRedescuento() throws AppException;

	public String getCorreosPymeEntidad(String ic_pyme  ) throws AppException;  //Fodea 034-2014
	public String getValidaPymeEntidad(String ic_pyme  ) throws AppException;     //Fodea 034-2014
	public void  getDoctosNotifiIF_DesElec( ) throws AppException;  //Fodea 027-2015
	   
	public String  validaBloqueoPymeEpoIF(String esClavePyme,  String cboEpo, String cboIf) throws AppException;  //F025-2015    
    /**
     * 2017_003
     * @param cboEpo
     * @return
     * @throws AppException
     */
	public String  nombreEPo(  String cboEpo)   throws AppException;  //2017_003 
    /**
     * @param icEpo
     * @param icIf
     * @param icPyme
     * @param icBeneficiario
     * @param con
     * @return
     * @throws AppException
     */
	public boolean getBenefAutoxIF(String icEpo, String icIf,String icPyme, String icBeneficiario, AccesoDB con)throws AppException; 
	
	
	/**
	 * metodo para saber si le Pyme opera factoraje Distribuido
	 * 2018_02
	 * @param icPyme
	 * @return
	 */
	public String getPymeOperadaDistribuido(String icPyme)throws AppException; 
	
	/**
	 * Metodo para obtener los contratos de la cuenta bancarias  2018_02
	 * @param icIF
	 * @param icEpo
	 * @param icPyme
	 * @param icBeneficiario
	 * @param icMoneda
	 * @param con
	 * @param numContrato
	 * @return
	 * @throws AppException
	 */
	public boolean getListContratosDist(String icIF,String icEpo,String icPyme,String icBeneficiario,String icMoneda, AccesoDB con, String numContrato) throws AppException; 
	
	
	/**
	 * metodo para saber si el beneficiario tiene cuenta bancaria autorizada por el Intermediario Financiero
	 * en  Admin IF/ Administración / Parametrización / Cuentas Bancarias Beneficiario/ Autorización  2018_02
	 * @param icIF
	 * @param icEpo
	 * @param icPyme
	 * @param icBeneficiario
	 * @param icMoneda
	 * @param con
	 * @return
	 * @throws AppException
	 */
	public String  getCtaAutoIFBene(String icIF,String icEpo,String icPyme,String icBeneficiario,String icMoneda, AccesoDB con)throws AppException; 
	
	
	/**
	 * metodo para saber si el Intermediario Financiero Opera Montos Menores 
	 * @param claveIF
	 * @return
	 * @throws AppException
	 * @throws Exception
	 */
	public String  operaMontosMenoresIF(String claveIF ) throws AppException, Exception;   
	
	
}