package com.netro.descuento;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.util.List;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;


public class SubeArchivoEnl{

	String df_fecha_hoy  = new java.text.SimpleDateFormat("dd/MM/yy").format(new java.util.Date());

	public void llenaTablatmp(EpoEnlaceArch epoEnl, String direccion) throws Exception{

			CargaDocumento carga = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);

			if(direccion.equals("")){
				direccion = epoEnl.getRutaArchivo();
			}
			VectorTokenizer palabra = null;
			String fecha = df_fecha_hoy.substring(0,2)+df_fecha_hoy.substring(3,5)+df_fecha_hoy.substring(6,8);
			String rutatmp = direccion+epoEnl.getArchivo()+fecha+".TXT";
			File lfArchivo=new File(rutatmp);
			BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(lfArchivo)));
			String lsLinea = "";
			while((lsLinea = br.readLine())!=null) {
				palabra = new VectorTokenizer(lsLinea,"|");
				List lDatos = palabra.getValuesVector();
				carga.insertaDatosTempEnlace(epoEnl.getInsertaDatos(), lDatos);
			}
			br.close();
	}

	public void crearArchivoAcusePublicacion(EpoEnlaceArch epoEnl, String direccion) throws Exception{
		CreaArchivo archivo = new CreaArchivo();
		String contenidoArchivo = "";
			CargaDocumento carga = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);

			if(direccion.equals("")){
				direccion = epoEnl.getRutaArchivo();
			}

			String nomb = epoEnl.getArchivoAcuse()+df_fecha_hoy.substring(0,2)+df_fecha_hoy.substring(3,5)+df_fecha_hoy.substring(6,8);

			contenidoArchivo = carga.getAcusePublicacionEnlace(epoEnl.getAcusePublicacion(),epoEnl.getNombreEnlace());

			if(!archivo.make(contenidoArchivo,direccion,nomb,".TXT"))
				System.out.print("Error al generar el archivo de acuse de publicacion de documentos");

	}

	public void crearArchivoErroresPublicacion(EpoEnlaceArch epoEnl, String direccion) throws Exception{
		CreaArchivo archivo = new CreaArchivo();
		String contenidoArchivo = null;

			CargaDocumento carga = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);

			if(direccion.equals("")){
				direccion = epoEnl.getRutaArchivo();
			}

			String nomb = epoEnl.getArchivoErrores()+df_fecha_hoy.substring(0,2)+df_fecha_hoy.substring(3,5)+df_fecha_hoy.substring(6,8);

			contenidoArchivo = carga.getErroresPublicacionEnlace(epoEnl.getErroresPublicacion(),epoEnl.getNombreEnlace());

			if(!archivo.make(contenidoArchivo,direccion,nomb, ".TXT"))
				System.out.print("Error al generar el archivo de errores de publicacion de documentos");

	}

	public void crearArchivoOperados(EpoEnlOperados epoEnl,EpoEnlOperadosArch epoEnlArch, String direccion){
		CreaArchivo archivo = new CreaArchivo();
		String contenidoArchivo = null;

		try{
			CargaDocumento carga = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);

			if(direccion.equals("")){
				direccion = epoEnlArch.getRutaArchivo();
			}

			String nomb = epoEnlArch.getNombreArchivo()+df_fecha_hoy.substring(0,2)+df_fecha_hoy.substring(3,5)+df_fecha_hoy.substring(6,8);

			contenidoArchivo = carga.getOperadosEnlace(epoEnl.getTablaDoctosOpe());

			if(!archivo.make(contenidoArchivo,direccion,nomb, ".TXT"))
				System.out.print("Error al generar el archivo de operados");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
}