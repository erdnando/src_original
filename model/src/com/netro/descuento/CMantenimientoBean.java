package com.netro.descuento;

import com.netro.exception.NafinException;
import com.netro.zip.ComunesZIP;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/*************************************************************************************
 *
 * Nombre de Clase o de Archivo: CMantenimientoBean.java
 *
 * Versi�n: 		  1.0
 *
 * Fecha Creaci�n: 13/febrero/2002
 *
 * Autor:
 *
 * Fecha Ult. Modificaci�n: 18/Enero/2011
 *
 * Descripci�n de Clase: Clase que implementa los m�todos para el EJB de Mantenimiento en Descuento Electr�nico.
 *
 *************************************************************************************/
@Stateless(name = "MantenimientoEJB" , mappedName = "MantenimientoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CMantenimientoBean implements IMantenimiento {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CMantenimientoBean.class);

	/*********************************************************************************************
	*
	*	 	    void vvalidaFV()
	*
	*********************************************************************************************/
	public void vvalidaFV( String esClaveEpo )
			throws NafinException
	{
		String lsQrySentencia = null;
		ResultSet lrsResultado = null;

		int liValor = 0;

		System.out.println(" SeleccionDocumentoEJB::vvalidaFV(E)");

		AccesoDB lobdConexion = new AccesoDB();
		try {
			lobdConexion.conexionDB();

			lsQrySentencia  =
				" SELECT COUNT (1)"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = 1"   +
				"    AND cs_factoraje_vencido = 'S'"   +
				"    AND ic_epo = "+ esClaveEpo;

			lrsResultado = lobdConexion.queryDB(lsQrySentencia);

			if (lrsResultado.next()) {
				liValor = lrsResultado.getInt(1);
			}
			lobdConexion.cierraStatement();
			if( liValor == 0 )
				throw new NafinException("DSCT0018"); //Servicio no disponible para la EPO.
			return;
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (lobdConexion.hayConexionAbierta() == true)
				lobdConexion.cierraConexionDB();
			System.out.println(" SeleccionDocumentoEJB::vvalidaFV(S)");
		}
	}//Fin del m�todo vvalidaFV()


	/*********************************************************************************************
	*
	*	 	    String ovgetDocumentosMant()
	*
	*********************************************************************************************/
	public Vector ovgetDocumentosMant(String esClavePyme, String esNumeroDocto, String esFechaDocumento,
			String esFechaVencimiento, String esClaveMoneda, String esMontoMinimo, String esMontoMaximo,
			String esEstatus, String esAforo, String esClaveEpo, boolean ebOperaFactorajeVencido,
			String esDoctosSeleccionados[],	boolean ebInicial ,String esAforoDL, String sesIdiomaUsuario)
			throws NafinException {

		String lsCondiciones = "";
		String lsCondEstatus = "";
		String lsQryDoctos 	 = "";
		String lsQrySentencia = "";

		String lsIcDocumento = "";
		String lsNumeroDocto = "";
		String lsFechaDocto	 = "";
		String lsFechaVenc	 = "";
		String lsMonto		 = "";
		String lsNombrePyme	 = "";
		String lsEstatusDesc = "";
		String lsIcMoneda	 = "";
		String lsMonedaDesc	 = "";
		String lsMontoDscto	 = "";
		String lsTipoFactoraje	 = "";
		String lsNombreIF	 = "";
		String lsPorcentaje  = "";
        String lsEstatusDocto  = "";
        String lsFechaVencPyme="";

        sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

		Vector lovRegistro = null;
		Vector lovDoctosMant = new Vector();

		log.info(" MantenimientoEJB::ovgetDocumentosMant(E)");

		AccesoDB lodbConexion = new AccesoDB();
		try {
			ResultSet lrsDoctosMant = null;

			lodbConexion.conexionDB();
			if (ebInicial) {
				if (!esClavePyme.equals(""))
					lsCondiciones += " AND d.ic_pyme="+esClavePyme;
				if (!esNumeroDocto.equals(""))
					lsCondiciones += " AND d.ig_numero_docto='"+esNumeroDocto+"' ";
				if (!esFechaDocumento.equals(""))
					lsCondiciones += " AND d.df_fecha_docto=TO_DATE('"+esFechaDocumento+"','dd/mm/yyyy') ";
				if (!esFechaVencimiento.equals(""))
					lsCondiciones += " AND d.df_fecha_venc=TO_DATE('"+esFechaVencimiento+"','dd/mm/yyyy') ";
				if (!esClaveMoneda.equals(""))
					lsCondiciones += " AND d.ic_moneda="+esClaveMoneda;
				if (!esMontoMinimo.equals(""))
				{
					if (!esMontoMaximo.equals(""))
						lsCondiciones += " AND d.fn_monto BETWEEN "+esMontoMinimo+" AND "+esMontoMaximo;
					else
						lsCondiciones += " AND d.fn_monto = "+esMontoMaximo;
				}
				if (!esEstatus.equals(""))
					lsCondEstatus += " AND d.ic_estatus_docto = "+esEstatus;
				else
					lsCondEstatus += " AND d.ic_estatus_docto in (1,2,21)";

			} else {
				lsCondiciones += " AND d.ic_documento in ("+Comunes.implode(",",esDoctosSeleccionados)+") ";
			}
lsQryDoctos =
				"SELECT d.ic_documento, d.ig_numero_docto, "+
				"   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, "+
				"   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, "+
				"   d.fn_monto, "+
				"   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social, "+
				"   ed.cd_descripcion"+idioma+ " as cd_descripcion , " +
				"    m.ic_moneda, "+
				"   m.cd_nombre"+idioma+ " as cd_nombre, " +
				"   decode(d.ic_moneda,1,"+esAforo+",54,"+esAforoDL+")*d.fn_monto as montoDescuento, "+
				"   CS_DSCTO_ESPECIAL, "+
				"   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF "+
				"   ,d.ic_estatus_docto"+
				"   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme "+
				"   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante "+//<<===================================== FODEA 050 - 2008
				"   , tf.cg_nombre as TIPO_FACTORAJE "+
				" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, "+
				"   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf "+
				" WHERE d.ic_epo="+esClaveEpo+
				"   AND d.ic_pyme=py.ic_pyme"+
				"   AND d.CS_DSCTO_ESPECIAL!='C' "+
				"   AND py.ic_pyme = pe.ic_pyme"+
				"   AND pe.ic_epo = "+esClaveEpo+
				"   AND pe.cs_habilitado='S'"+
				"   AND d.ic_moneda=m.ic_moneda"+
				"  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				"   AND d.ic_estatus_docto=ed.ic_estatus_docto "+
				"   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  "+
				"   AND d.ic_if=I.ic_if(+) " + lsCondiciones +" "+lsCondEstatus+" ";
				if(!"1".equals(lsCondEstatus)) {
					lsCondEstatus = "";
					if ("2".equals(esEstatus)||"21".equals(esEstatus)||"1".equals(esEstatus) ||"28".equals(esEstatus)) {
						lsCondEstatus += " AND d.ic_estatus_docto = "+esEstatus;
					}

				//	if (!esEstatus.equals("")){  lsCondEstatus += " AND d.ic_estatus_docto = "+esEstatus;
					 else {
						lsCondEstatus += " AND d.ic_estatus_docto in (1,2,21)";
					}
					lsQryDoctos +=
						"UNION "+
						"SELECT d.ic_documento, d.ig_numero_docto, "+
						"   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, "+
						"   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, "+
						"   d.fn_monto, "+
						"   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social , "+
						"   ed.cd_descripcion"+idioma+ " as cd_descripcion , " +
						"   m.ic_moneda, "+
						"   m.cd_nombre"+idioma+ " as cd_nombre, " +
						"   decode(d.ic_moneda,1,"+esAforo+",54,"+esAforoDL+")*d.fn_monto as montoDescuento, "+
						"   CS_DSCTO_ESPECIAL, "+
						"   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF "+
						"   ,d.ic_estatus_docto"+
						"   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme "+
						"   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante, "+//<<================================= FODEA 050 - 2008
						"   tf.cg_nombre as TIPO_FACTORAJE "+
						" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, "+
						"   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf "+
						" WHERE d.ic_epo="+esClaveEpo+
						"   AND d.ic_pyme=py.ic_pyme"+
						"   AND d.CS_DSCTO_ESPECIAL='C' "+
						"   AND py.ic_pyme = pe.ic_pyme"+
						"   AND pe.ic_epo = "+esClaveEpo+
						"   AND pe.cs_habilitado='S'"+
						"   AND d.ic_moneda=m.ic_moneda"+
						"   AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
						"   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  "+
						"   AND d.ic_estatus_docto=ed.ic_estatus_docto "+
						"   AND d.ic_if=I.ic_if(+) " + lsCondiciones +" "+lsCondEstatus+" ";
				}

			lsQrySentencia = "select * from ("+lsQryDoctos+") ORDER BY cg_razon_social, ic_moneda, df_fecha_docto, ig_numero_docto ";

			log.debug("ovgetDocumentosMant ::."+lsQrySentencia);

			lrsDoctosMant = lodbConexion.queryDB(lsQrySentencia);

			if (lrsDoctosMant.next()){
				do {

					lsIcDocumento 	= lrsDoctosMant.getString(1).trim();
					lsNumeroDocto 	= lrsDoctosMant.getString(2).trim();
					lsFechaDocto  	= lrsDoctosMant.getString(3).trim();
					lsFechaVenc  	= lrsDoctosMant.getString(4)==null?"":lrsDoctosMant.getString(4).trim();
					lsMonto       	= lrsDoctosMant.getString(5).trim();
					lsNombrePyme    = lrsDoctosMant.getString(6).trim();
					lsEstatusDesc 	= lrsDoctosMant.getString(7).trim();
					lsIcMoneda  	= lrsDoctosMant.getString(8).trim();
					lsMonedaDesc	= lrsDoctosMant.getString(9).trim();
					lsMontoDscto	= (lrsDoctosMant.getString(10) == null)?"":lrsDoctosMant.getString(10);

					lsPorcentaje 	= "1".equals(lsIcMoneda)?esAforo:esAforoDL;
					lsPorcentaje 	= new Double(new Double(lsPorcentaje).doubleValue() * 100).toString();

					lsEstatusDocto  = lrsDoctosMant.getString(13).trim();

					/*if(ebOperaFactorajeVencido) { // Para Factoraje Vencido
						lsTipoFactoraje = (lrsDoctosMant.getString(11)==null)?"":lrsDoctosMant.getString(11);
						// si factoraje vencido: se aplica 100% de anticipo
						lsNombreIF = "";
						if(lsTipoFactoraje.equals("V")) {
							lsPorcentaje = "100";
							lsMontoDscto = lsMonto;
							lsNombreIF  = (lrsDoctosMant.getString(12)==null)?"":lrsDoctosMant.getString(12);
						}
					}
					*/
					lsTipoFactoraje = (lrsDoctosMant.getString(11)==null)?"":lrsDoctosMant.getString(11);
					lsPorcentaje = "100";
					lsMontoDscto = lsMonto;
					lsNombreIF  = (lrsDoctosMant.getString(12)==null)?"":lrsDoctosMant.getString(12);

					lsFechaVencPyme	= lrsDoctosMant.getString(14)==null?"":lrsDoctosMant.getString(14).trim();
					String lsIcPyme	= lrsDoctosMant.getString(15)==null?"":lrsDoctosMant.getString(15).trim();//<<================================= FODEA 050 - 2008
					String lsmandante	= lrsDoctosMant.getString(16)==null?"":lrsDoctosMant.getString(16).trim();//<<================================= FODEA 050 - 2008
               String lsFactoraje = lrsDoctosMant.getString(17)==null?"":lrsDoctosMant.getString(17).trim();//<<=

					lovRegistro = new Vector();

					if(lsTipoFactoraje.equals("N"))
						lsTipoFactoraje = (sesIdiomaUsuario.equals("EN"))?"Normal":"Normal";
					else if(lsTipoFactoraje.equals("M"))
						lsTipoFactoraje = (sesIdiomaUsuario.equals("EN"))?"Mandato":"Mandato";
					else if(lsTipoFactoraje.equals("V"))
						lsTipoFactoraje = (sesIdiomaUsuario.equals("EN"))?"Overcome":"Vencido";
					else if(lsTipoFactoraje.equals("D"))
						lsTipoFactoraje = (sesIdiomaUsuario.equals("EN"))?"Distributed":"Distribuido";
					else if(lsTipoFactoraje.equals("C"))
						lsTipoFactoraje = (sesIdiomaUsuario.equals("EN"))?"Credit Note":"Nota de Cr�dito";


					lovRegistro.add( lsIcDocumento );
					lovRegistro.add( lsNumeroDocto );
					lovRegistro.add( lsFechaDocto );
					lovRegistro.add( lsFechaVenc );
					lovRegistro.add( lsMonto);
					lovRegistro.add( lsNombrePyme);
					lovRegistro.add( lsEstatusDesc);
					lovRegistro.add( lsIcMoneda);
					lovRegistro.add( lsMonedaDesc);
					lovRegistro.add( lsMontoDscto);
					lovRegistro.add( lsTipoFactoraje);
					lovRegistro.add( lsNombreIF);
					lovRegistro.add( lsPorcentaje);
					lovRegistro.add( lsEstatusDocto);
					lovRegistro.add( lsFechaVencPyme);
					lovRegistro.add( lsIcPyme);//<<======================================= FODEA 050 - 2008
					lovRegistro.add( lsmandante); // Fodea 023 2009
					lovRegistro.add( lsFactoraje); // Fodea 023 2009


					lovDoctosMant.addElement( lovRegistro );
					log.debug("ovgetDocumentosMant ::lovDoctosMant::   "+lovDoctosMant);

				} while (lrsDoctosMant.next());
			}else
			   throw new NafinException("GRAL0004");

			lodbConexion.cierraStatement();
			lodbConexion.cierraConexionDB();
			return lovDoctosMant;
		} catch (NafinException ne) {
			log.error("ovgetDocumentosMant(Error): ",ne);
			throw ne;
		} catch (Exception e) {
			log.error("ovgetDocumentosMant(Error): ",e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta() == true)
				lodbConexion.cierraConexionDB();
			log.info(" MantenimientoEJB::ovgetDocumentosMant(S)");
		}
	}

	/*********************************************************************************************
	*
	* 	    void ovgetEstatusAsignar()
	*
	*********************************************************************************************/
	public Vector ovgetEstatusAsignar(String esEstatuDeConsulta, String sesIdiomaUsuario)
			throws NafinException
	{
		String lsQryEstatus  = "";

		Vector lovRegistro = null;
		Vector lovCatalogo = new Vector();
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

		AccesoDB lodbConexion = null;

		System.out.println(" MantenimientoEJB::ovgetEstatusAsignar(E)");
		try {
			ResultSet lrsEstatus = null;
			lodbConexion = new AccesoDB();

			// Abre la conexi�n a la base de datos
			lodbConexion.conexionDB();

			String lsEstatus = "";
			if(esEstatuDeConsulta.equals("1"))	// No Negociada
				lsEstatus = "5";			// Puede cambiar a Baja
			if(esEstatuDeConsulta.equals("2"))	// Negociada
				lsEstatus = "5,6,7,21";		// Puede cambiar a Baja, Descuento Fisico, Pagada Anticipado, Bloqueado
			if(esEstatuDeConsulta.equals("21"))	// Bloqueada
				lsEstatus = "2";			// Puede cambiar a Negociada

			if(esEstatuDeConsulta.equals("28"))	// Pre negociable
				lsEstatus = "5,6,7";			// Baja (5),Descuento F�sico (6), Pagado Anticipado (7) )


			lsQryEstatus  = "SELECT ic_estatus_docto, cd_descripcion"+idioma+" as cd_descripcion"+
							" FROM comcat_estatus_docto" +
							" WHERE ic_estatus_docto in ("+lsEstatus+") " +
							" ORDER BY ic_estatus_docto";


			lrsEstatus = lodbConexion.queryDB(lsQryEstatus);
			System.out.println("ovgetEstatusAsignar::."+lsQryEstatus+lrsEstatus);
			if (lrsEstatus.next()){
				do {
					lovRegistro = new Vector();

					lovRegistro.add(lrsEstatus.getString(1).trim());
					lovRegistro.add(lrsEstatus.getString(2).trim());

					lovCatalogo.addElement(lovRegistro);		//Vector con los registros de las EPOS

				} while (lrsEstatus.next());
			} else
				throw new NafinException("DSCT0021"); 		// No se encontraron estatus de documentos por asignar

			lodbConexion.cierraStatement();
			lodbConexion.cierraConexionDB();
			return lovCatalogo;
		} catch (NafinException Error){
			throw Error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta() == true)
				lodbConexion.cierraConexionDB();
			System.out.println("--- MantenimientoEJB::ovgetEstatusAsignar(S)----");
		}
	}//Fin del M�todo ovgetEstatusAsignar()


	/*********************************************************************************************
	*
	* 	    void vrealizarCambioEstatusDocto()
	*
	*********************************************************************************************/
	public void vrealizarCambioEstatusDocto( String esClaveNuevoEstatus, String esDoctosSeleccionados[],
											String esCambioMotivo, String esEstatusConsulta, String usuario )//FODEA 015 - 2009 ACF
			throws NafinException
	{
		String lsQryBusca  = "";
		String lsQryDocto  = "";
		int licambioEstatus=0;
		boolean lbOK = true;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println("**** MantenimientoEJB::vrealizarCambioEstatusDocto(E)******");
		try {
			ResultSet lrsBusca = null;

			// Abre la conexi�n a la base de datos
			lodbConexion.conexionDB();

			if(esEstatusConsulta.equals("2")) {	// NEGOCIABLE
				if (esClaveNuevoEstatus.equals("5")) 		//Baja
					licambioEstatus = 4;	//De negociable a baja
				else if (esClaveNuevoEstatus.equals("6"))  //Descuento F�sico
					licambioEstatus = 5;	//De negociable a descuento f�sico
				else if (esClaveNuevoEstatus.equals("7"))  //Pago Anticipado
					licambioEstatus = 6;	//De negociable a pago anticipado
				else if(esClaveNuevoEstatus.equals("21"))  //Bloqueado
					licambioEstatus = 15;   //De negociable a Bloqueado
			}
			if(esEstatusConsulta.equals("21")) {	// BLOQUEADO
				if(esClaveNuevoEstatus.equals("2"))   		// Negociable
					licambioEstatus = 16;	//De bloqueado a Negociable
			}
			if(esEstatusConsulta.equals("1")) {	// NO NEGOCIABLE
				if(esClaveNuevoEstatus.equals("5"))   		// Baja
					licambioEstatus = 24;	//De No Negociable a Baja
			}

			//fodea 060-2010
			if(esEstatusConsulta.equals("28")) {	// Pre Negociable
						if (esClaveNuevoEstatus.equals("5")) 		//Baja
							licambioEstatus = 37;	//Pre Negociable a Baja
						if (esClaveNuevoEstatus.equals("6")) 		//Descuento Fisico
							licambioEstatus = 38;	//Pre Negociable a Descuento Fisico
						if (esClaveNuevoEstatus.equals("7")) 		//pago Anticipado
							licambioEstatus = 39;	//Pre Negociable a pago Anticipado
			}

			//Fodea 038-2014
			if( esEstatusConsulta.equals("33") ) {	// Pendiente Duplicado
				if (esClaveNuevoEstatus.equals("2")) {		//Negociable
					licambioEstatus = 45;	//Pendiente Duplicado a Negociable
				} else if (esClaveNuevoEstatus.equals("5")) {	//Baja
					licambioEstatus = 46;	  //Pendiente Duplicado a Baja
				}
			}


			if (esDoctosSeleccionados!=null) {
				for (int i=0; i<=esDoctosSeleccionados.length-1; i++) {
					lsQryBusca =" select count(*) from com_documento"+
								" where ic_documento = "+esDoctosSeleccionados[i];
					/*	if(licambioEstatus!=16 && licambioEstatus!=24 )
							lsQryBusca += " and ic_estatus_docto != 2";
						else if(licambioEstatus==16)
							lsQryBusca += " and ic_estatus_docto != 21";
						else if(licambioEstatus==24)
							lsQryBusca += " and ic_estatus_docto != 1";
				*/
				lsQryBusca += " and ic_estatus_docto != "+esEstatusConsulta;

					System.out.println(lsQryBusca);

					lrsBusca = lodbConexion.queryDB(lsQryBusca);
					lrsBusca.next();

					if (lrsBusca.getInt(1)==0) {
						lsQryDocto="INSERT INTO comhis_cambio_estatus (dc_fecha_cambio,ic_documento"+
							" , ic_cambio_estatus, ct_cambio_motivo, cg_nombre_usuario)"+//FODEA 015 - 2009 ACF
							" values (sysdate,"+esDoctosSeleccionados[i]+","+licambioEstatus+", '"+esCambioMotivo+"', '"+usuario+"')";//FODEA 015 - 2009 ACF
              System.out.println("..:: lsQryDocto : "+lsQryDocto);
						try{
							lodbConexion.ejecutaSQL(lsQryDocto);
						} catch ( SQLException errorSQL){
							lbOK = false;
							throw new NafinException("DSCT0024"); // Problemas al realizar la inserci�n del cambio de estatus.
						}
						//System.out.println("Realiz� el insert del cambio de estatus");
					} else { // validaci�n de error por concurrencia
						lbOK = false;
						if (esClaveNuevoEstatus.equals("2")) {
							throw new NafinException("DSCT0028"); //Error en el proceso de cambio de estatus de Seleccionada Pyme a Negociable.
						} else {
							throw new NafinException("DSCT0023"); //Error en el proceso de cambio de estatus de Negociable a Otros.
						}
					}
					lrsBusca.close();
					lodbConexion.cierraStatement();

				   //actualizamos el estatus del documento
					lsQryDocto  = "UPDATE com_documento "+
								" SET ic_estatus_docto = "+esClaveNuevoEstatus+
								" WHERE ic_documento = "+esDoctosSeleccionados[i]+"";
					System.out.println(lsQryDocto);

				   try{
				   		lodbConexion.ejecutaSQL(lsQryDocto);
					} catch ( SQLException errorSQL){
						lbOK = false;
						throw new NafinException("DSCT0011"); // Error al actualizar el estatus del documento
					}

				}//fin for

			} else {
				lbOK = false;
				throw new NafinException("DSCT0022"); //No se selecciono ningun documento.
			}

	   	} catch ( NafinException Error){
			throw Error;
		} catch ( Exception epError){
			epError.printStackTrace();
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
		   	System.out.println(" MantenimientoEJB::vrealizarCambioEstatusDocto(S)");
		}
	}// Fin del m�todo vrealizarSeleccion()


	/*********************************************************************************************
	*
	*	 	    String ovgetDocumentosSelPyme()
	*
	*********************************************************************************************/
	public Vector ovgetDocumentosSelPyme(String esClaveEPO, String esNumeroDocto, String esFechaVencimiento,
			String esClaveMoneda, String esMontoMinimo, String esMontoMaximo, String esEstatus,
			String esClaveIF, String esDoctosSeleccionados[], boolean ebInicial, String nafinPyme) throws NafinException {

			return ovgetDocumentosSelPyme(esClaveEPO, esNumeroDocto, esFechaVencimiento,
				esClaveMoneda, esMontoMinimo, esMontoMaximo, esEstatus,
				esClaveIF, esDoctosSeleccionados, null, ebInicial, nafinPyme);
	}

	/*********************************************************************************************
	*
	*	 	    String ovgetDocumentosSelPyme()
	*
	*********************************************************************************************/

	public Vector ovgetDocumentosSelPyme(String esClaveEPO, String esNumeroDocto, String esFechaVencimiento,
		String esClaveMoneda, String esMontoMinimo, String esMontoMaximo, String esEstatus,
		String esClaveIF, String esDoctosSeleccionados[], String estatusDocumentos[], boolean ebInicial, String nafinPyme) throws NafinException {

		String lsTabla = "";
		String lsCondiciones = "";
		String lsQryDoctos 	 = "";
		String lsQry		 = "";
		Vector lovRegistro = null;
		Vector lovDoctosSelPyme = new Vector();
		Vector lovEstatus 	= new Vector();

		System.out.println(" MantenimientoEJB::ovgetDocumentosSelPyme(E)");

		AccesoDB lodbConexion = new AccesoDB();

		try {
			ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

			String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(esClaveEPO);

			ResultSet lrsDoctosSelPyme = null;
			PreparedStatement ps = null;
			lodbConexion.conexionDB();
			boolean autOperSF = false;
			lsQry =
				" select CS_AUT_OPER_SF from comrel_producto_if "+
				" where ic_if = ? "+
				" and ic_producto_nafin = 1";
			ps = lodbConexion.queryPrecompilado(lsQry);
			ps.setString(1,esClaveIF);
			lrsDoctosSelPyme = ps.executeQuery();
			if(lrsDoctosSelPyme.next()){
				autOperSF = "S".equals(lrsDoctosSelPyme.getString("CS_AUT_OPER_SF"));
			}
			ps.close();

			String hint = "";

			if (ebInicial) {
				hint = "/*+  ordered INDEX (d IN_COM_DOCUMENTO_04_NUK) USE_NL (d ds pe e ed m )  */";

				if (!nafinPyme.equals("")){
					lsTabla += ", comrel_nafin cn ";
					lsCondiciones += " AND d.ic_pyme = cn.ic_epo_pyme_if AND cn.cg_tipo = 'P' AND cn.ic_nafin_electronico = "+nafinPyme;
				}

				if (!esEstatus.equals(""))
					lsCondiciones += " AND d.ic_estatus_docto ="+esEstatus;
				else
					lsCondiciones += " AND d.ic_estatus_docto in (3,24) ";	//Modificado, Fodea 17
				//lsCondiciones += " AND d.ic_estatus_docto in (3,23,24) "; //agregado 23  15/11/2004

				if (!esClaveEPO.equals(""))
					lsCondiciones += " AND d.ic_epo = "+esClaveEPO;
				if (!esNumeroDocto.equals(""))
					lsCondiciones += " AND d.ig_numero_docto = '"+esNumeroDocto+"' ";
				if (!esFechaVencimiento.equals("")) {
					lsCondiciones +=
						" AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE('"+esFechaVencimiento+"','dd/mm/yyyy') "+
						" AND d.df_fecha_venc"+sFechaVencPyme+" < (TO_DATE('"+esFechaVencimiento+"','dd/mm/yyyy')+1) ";
				}
				if (!esClaveMoneda.equals(""))
					lsCondiciones += " AND d.ic_moneda = "+esClaveMoneda;
				if (!esMontoMinimo.equals("")) {
					if (!esMontoMaximo.equals(""))
						lsCondiciones += " AND d.fn_monto BETWEEN "+esMontoMinimo+" AND "+esMontoMaximo;
					else
						lsCondiciones += " AND d.fn_monto = "+esMontoMaximo;
				}
			} else {
				hint = "/*+ index(d CP_COM_DOCUMENTO_PK) USE_NL (d ds pe e ed m ) */";
				if(estatusDocumentos==null){
					lsCondiciones += " AND d.ic_documento in ("+Comunes.implode(",",esDoctosSeleccionados)+") ";
					for(int i=0;i<esDoctosSeleccionados.length;i++){
						lovEstatus.add("N");
					}
				}else{
					int i = 0;
					StringBuffer in = new StringBuffer("");
					for(i=0;i<esDoctosSeleccionados.length;i++){
						if(!"".equals(estatusDocumentos[i])){
							if(in.length()>0)
								in.append(",");
							in.append(esDoctosSeleccionados[i]);
							lovEstatus.add(estatusDocumentos[i]);
						}
					}
					if(i>0){
						lsCondiciones += " AND d.ic_documento in("+in.toString()+")";
					}
				}
			}

//			String hint = "/*+ use_nl(d, e, ed, m, ds, pe) index(d)*/";
//			String hint = "/*+ INDEX (D CP_COM_DOCUMENTO_PK) INDEX (DS CP_COM_DOCTO_SELECCIONADO_PK) INDEX (PE CP_COMREL_PYME_EPO_PK) INDEX (E CP_COMCAT_EPO_PK) INDEX (M CP_COMCAT_MONEDA_PK) USE_NL (D,DS,PE,E,M,ED) */";


			lsQryDoctos =
				" SELECT    "+hint+" "   +
				"           d.ic_documento,"   +
				"           d.ig_numero_docto,"   +
				"           TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc, d.fn_monto,"   +
				"           e.cg_razon_social, ed.cd_descripcion, m.ic_moneda, m.cd_nombre,"   +
				"           d.fn_porc_anticipo, d.fn_monto_dscto, "+
				"	decode(ds.cs_opera_fiso,'N',ds.in_importe_interes,'S',ds.in_importe_interes_fondeo) as in_importe_interes,"   +
				"  decode(ds.cs_opera_fiso,'N',ds.in_importe_recibir,'S',ds.in_importe_recibir_fondeo) as in_importe_recibir,"+
				"  d.ic_estatus_docto,"   +
				"           'CMantenimientoBean::ovgetDocumentosSelPyme', "   +
				//" 					cp.cg_razon_social as nombrePyme" +
				" 	decode(ds.cs_opera_fiso,'N',cp.cg_razon_social,'S',i.cg_razon_social) as nombrePyme," +
				//"   cp.cg_razon_social as referencia "+
				" decode(ds.cs_opera_fiso,'N',' ','S',cp.cg_razon_social) as referencia  "+
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          comrel_pyme_epo pe,"   +
				"          comcat_epo e,"   +
				"          comcat_estatus_docto ed,"   +
				"          comcat_moneda m," +
				" 			comcat_pyme cp,"   +
				" 			comcat_if i "   +
				lsTabla+
				"    WHERE d.ic_documento = ds.ic_documento(+)"   +
				"      AND d.ic_epo = e.ic_epo"   +
				"      AND d.ic_moneda = m.ic_moneda"   +
				"      AND d.ic_epo = pe.ic_epo"   +
				"      AND d.ic_pyme = pe.ic_pyme"   +
				"      AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"      AND d.ic_if = "+esClaveIF+
				"      AND pe.cs_habilitado = 'S'"   +
				"      AND e.cs_habilitado = 'S'"   +
				"      AND d.cs_dscto_especial != 'C'"   +
				" 		 AND d.ic_pyme = cp.ic_pyme" +
				" 		 AND d.ic_pyme = pe.ic_pyme" +
				" 		 AND ds.ic_if = i.ic_if" +
				lsCondiciones+
				" ORDER BY e.cg_razon_social, m.ic_moneda, d.df_fecha_docto, d.ig_numero_docto"  ;

			System.out.println("lsQryDoctos: " + lsQryDoctos);
			lrsDoctosSelPyme = lodbConexion.queryDB(lsQryDoctos);

			if (lrsDoctosSelPyme.next()){
				int i=0;
				do{
					lovRegistro = new Vector();
/*0*/			lovRegistro.add((lrsDoctosSelPyme.getString("ic_documento")==null?"":lrsDoctosSelPyme.getString("ic_documento").trim()));
/*1*/			lovRegistro.add((lrsDoctosSelPyme.getString("ig_numero_docto")==null?"":lrsDoctosSelPyme.getString("ig_numero_docto").trim()));
/*2*/			lovRegistro.add((lrsDoctosSelPyme.getString("df_fecha_venc")==null?"":lrsDoctosSelPyme.getString("df_fecha_venc").trim()));
/*3*/			lovRegistro.add((lrsDoctosSelPyme.getString("fn_monto")==null?"0":lrsDoctosSelPyme.getString("fn_monto").trim()));
/*4*/			lovRegistro.add((lrsDoctosSelPyme.getString("cg_razon_social")==null?"":lrsDoctosSelPyme.getString("cg_razon_social").trim()));
/*5*/			lovRegistro.add((lrsDoctosSelPyme.getString("cd_descripcion")==null?"":lrsDoctosSelPyme.getString("cd_descripcion").trim()));
/*6*/			lovRegistro.add((lrsDoctosSelPyme.getString("ic_moneda")==null?"":lrsDoctosSelPyme.getString("ic_moneda").trim()));
/*7*/			lovRegistro.add((lrsDoctosSelPyme.getString("cd_nombre")==null?"":lrsDoctosSelPyme.getString("cd_nombre").trim()));
/*8*/			lovRegistro.add((lrsDoctosSelPyme.getString("fn_porc_anticipo")==null?"":lrsDoctosSelPyme.getString("fn_porc_anticipo").trim()));
/*9*/			lovRegistro.add((lrsDoctosSelPyme.getString("fn_monto_dscto")==null?"0":lrsDoctosSelPyme.getString("fn_monto_dscto").trim()));
/*10*/		lovRegistro.add((lrsDoctosSelPyme.getString("in_importe_interes")==null?"":lrsDoctosSelPyme.getString("in_importe_interes").trim()));
/*11*/		lovRegistro.add((lrsDoctosSelPyme.getString("in_importe_recibir")==null?"":lrsDoctosSelPyme.getString("in_importe_recibir").trim()));
/*12*/		lovRegistro.add((lrsDoctosSelPyme.getString("ic_estatus_docto")==null?"":lrsDoctosSelPyme.getString("ic_estatus_docto").trim()));
/*13*/		lovRegistro.add(new Boolean(autOperSF));
/*14*/		if(lovEstatus.size()>i){
					lovRegistro.add(lovEstatus.get(i));
				}else{
					lovRegistro.add("N");
				}
					lovRegistro.add((lrsDoctosSelPyme.getString("nombrePyme")==null?"":lrsDoctosSelPyme.getString("nombrePyme").trim()));
					lovRegistro.add((lrsDoctosSelPyme.getString("referencia")==null?"":lrsDoctosSelPyme.getString("referencia").trim()));
					lovDoctosSelPyme.addElement( lovRegistro );
					i++;
				} while (lrsDoctosSelPyme.next());
			}else
				throw new NafinException("GRAL0004");

			lodbConexion.cierraStatement();
			lodbConexion.cierraConexionDB();

			return lovDoctosSelPyme;
		} catch (NafinException ne) {
			System.out.println("MantenimientoEJB.ovgetDocumentosSelPyme(): "+ne);
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.ovgetDocumentosSelPyme(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
			System.out.println(" MantenimientoEJB::ovgetDocumentosSelPyme(S)");
		}
	}

	/*********************************************************************************************
	*
	*	 	    Vector ovgetDocumentosPagosVto()
	*
	*********************************************************************************************/
	public Vector ovgetDocumentosPagosVto(String esClaveEPO, String esClavePyme, String esFechaVencimiento,
											String esEstatusSolicitud, String esNoClienteIf,
											String esTipoSolicitud) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	String lsCondicion="", lsCondicion1="", lsEstatusDocumento="", lsQryDoctos="";
	Vector lovRegistro = null;
	Vector lovDoctos = new Vector();
		try {
			lodbConexion.conexionDB();

			if (!esClaveEPO.equals(""))	{
				lsCondicion += " and d.ic_epo = "+esClaveEPO;
			}
			if (!esClavePyme.equals("") && !esClavePyme.equals("TODOS")) {
				lsCondicion += " and d.ic_pyme = "+esClavePyme;
				lsCondicion1 += " and de.ic_pyme = "+esClavePyme;
			}
			if(!esFechaVencimiento.equals("")) {
				lsCondicion += " and d.df_fecha_venc = TO_DATE('"+esFechaVencimiento+"','dd/mm/yyyy') ";
				lsCondicion1 += " and s.df_v_descuento = TO_DATE('"+esFechaVencimiento+"','dd/mm/yyyy') ";
			} else {
				lsCondicion += " and d.df_fecha_venc = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') ";
				lsCondicion1 += " and s.df_v_descuento = TO_DATE(TO_CHAR(sysdate,'dd/mm/yyyy'),'dd/mm/yyyy') ";
			}

			if (esEstatusSolicitud.equals("5"))
				lsEstatusDocumento="11";
			if (esEstatusSolicitud.equals("6"))
				lsEstatusDocumento="12";

			String lsQryInt=" select d.ic_documento, e.cg_razon_social as nombreEPO,"+
							" p.cg_razon_social as nombrePYME, i.cg_razon_social as nombreIF,"+
							" d.ig_numero_docto, d.fn_monto,"+
							" TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc,"+
							" m.ic_moneda, m.cd_nombre, s.ic_folio, 'C' as origen"+
							" from com_solicitud s, com_documento d, comrel_pyme_epo pe,"+
							" comcat_epo e, comcat_pyme p, comcat_if i, comcat_moneda m"+
							" where d.ic_documento = s.ic_documento"+
							" and d.ic_moneda = m.ic_moneda"+
							" and d.ic_pyme = p.ic_pyme"+
							" and d.ic_epo = e.ic_epo"+
							" and d.ic_epo = pe.ic_epo"+
							" and d.ic_pyme = pe.ic_pyme"+
							" and e.cs_habilitado = 'S' "+
							" and pe.cs_habilitado = 'S'"+
							" and d.ic_if = i.ic_if"+
							" and d.ic_estatus_docto = "+lsEstatusDocumento+
							" and d.ic_if = "+esNoClienteIf+
							" "+lsCondicion;

			String lsQryExt=" select 0 as ic_documento, 'No aplica' as nombreEPO,"+
							" p.cg_razon_social as nombrePYME, i.cg_razon_social as nombreIF,"+
							" de.ig_numero_docto, de.fn_monto_docto as fn_monto,"+
							" TO_CHAR(s.df_v_descuento,'dd/mm/yyyy') as df_fecha_venc,"+
							" m.ic_moneda, m.cd_nombre, s.ic_folio, 'E' as origen"+
							" from com_solicitud s, com_docto_ext de,"+
							" comcat_pyme p, comcat_if i, comcat_moneda m"+
							" where"+
							" s.ic_folio = de.ic_folio"+
							" and de.ic_moneda = m.ic_moneda"+
							" and de.ic_pyme = p.ic_pyme"+
							" and de.ic_if = i.ic_if"+
							" and s.ic_estatus_solic = "+esEstatusSolicitud+
							" and de.ic_if = "+esNoClienteIf+
							" and s.cs_tipo_solicitud = 'E'"+
							" "+lsCondicion1;

			if (esTipoSolicitud.equals("C"))
				lsQryDoctos = lsQryInt;
			else if (esTipoSolicitud.equals("E"))
				lsQryDoctos = lsQryExt;
			else
				lsQryDoctos = lsQryInt+" UNION ALL "+lsQryExt;
			//System.out.println(lsQryExt);

			ResultSet lrsDoctos = lodbConexion.queryDB(lsQryDoctos);
			while(lrsDoctos.next()) {
				lovRegistro = new Vector();
				lovRegistro.add(lrsDoctos.getString("IC_DOCUMENTO"));
				lovRegistro.add((lrsDoctos.getString("NOMBREEPO")==null?"":lrsDoctos.getString("IC_DOCUMENTO").trim()));
				lovRegistro.add((lrsDoctos.getString("NOMBREPYME")==null?"":lrsDoctos.getString("NOMBREPYME").trim()));
				lovRegistro.add((lrsDoctos.getString("NOMBREIF")==null?"":lrsDoctos.getString("NOMBREIF").trim()));
				lovRegistro.add(lrsDoctos.getString("IG_NUMERO_DOCTO"));
				lovRegistro.add(lrsDoctos.getString("IC_MONEDA"));
				lovRegistro.add(lrsDoctos.getString("CD_NOMBRE"));
				lovRegistro.add((lrsDoctos.getString("FN_MONTO")==null?"":lrsDoctos.getString("FN_MONTO").trim()));
				lovRegistro.add(lrsDoctos.getString("DF_FECHA_VENC"));
				lovRegistro.add(lrsDoctos.getString("IC_FOLIO"));
				lovRegistro.add(lrsDoctos.getString("ORIGEN"));
				lovDoctos.addElement(lovRegistro);
			}
			lodbConexion.cierraStatement();

		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.ovgetDocumentosPagosVto(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
		}
	return lovDoctos;
	}

	/*********************************************************************************************
	*
	*	 	    Hashtable ohactualizaDoctoSolicitud()
	*
	*********************************************************************************************/

	public Hashtable ohactualizaDoctoSolicitud(String esNuevoEstatusSolicitud,
												Vector evOrigenClave) throws NafinException {
	String lsQry = "", lsFolios="", lsDocumentos="", lsOrigen="", lsClave="";
	AccesoDB lobdConexion = new AccesoDB();
	Hashtable lohDoctosFolios = new Hashtable();
	boolean bOkInserta = true;
		try {
			lobdConexion.conexionDB();

			for(int i=0; i < evOrigenClave.size(); i++) {
				lsOrigen = evOrigenClave.get(i).toString().substring(0,1);
				lsClave = evOrigenClave.get(i).toString().substring(1);
				//System.out.println("Origen: "+lsOrigen+" Clave: "+lsClave);

				//Hay que generar registro en la bit�cora de cambios...
				if (esNuevoEstatusSolicitud.equals("6") && lsOrigen.equals("C")) {
					try {
						lsQry = "insert into comhis_cambio_estatus (dc_fecha_cambio,"+
								" ic_documento,ic_cambio_estatus)"+
								" values (sysdate, "+lsClave+", 7)";
						lobdConexion.ejecutaSQL(lsQry);
					} catch (SQLException sqle){
						bOkInserta = false;
						throw new NafinException("DSCT0024"); // Problemas al realizar la inserci�n del cambio de estatus.
					}
				}

				if (lsOrigen.equals("E"))
					lsFolios += (lsFolios.equals(""))?"'"+lsClave+"'":",'"+lsClave+"'";
				else if (lsOrigen.equals("C"))
					lsDocumentos += (lsDocumentos.equals(""))?"'"+lsClave+"'":",'"+lsClave+"'";
			} // for

			if (!lsFolios.equals("")) {
				try{
					lsQry = "update com_solicitud "+
							" set ic_estatus_solic = "+esNuevoEstatusSolicitud+
							" where ic_folio in ("+lsFolios+")";
					lobdConexion.ejecutaSQL(lsQry);
				} catch (SQLException sqle){
					bOkInserta = false;
					throw new NafinException("DSCT0010");
				}
			}
			else
				lsFolios = "NULL";

			if (!lsDocumentos.equals("")) {
				String esNuevoEstatusDocumento="";
				if (esNuevoEstatusSolicitud.equals("6"))
					esNuevoEstatusDocumento="12";	//Operado pendiente de pago (documento)
				else if (esNuevoEstatusSolicitud.equals("5"))
					esNuevoEstatusDocumento="11";	//Operado pagado (documento)

				try {
					lsQry = "update com_documento "+
							" set ic_estatus_docto = "+esNuevoEstatusDocumento+
							" where ic_documento in ("+lsDocumentos+")";
					lobdConexion.ejecutaSQL(lsQry);
				} catch (SQLException sqle){
					bOkInserta = false;
					throw new NafinException("DSCT0011"); // Error al actualizar el estatus del documento
				}
			}
			else
				lsDocumentos = "NULL";

			lohDoctosFolios.put("lsDocumentos",lsDocumentos);
			lohDoctosFolios.put("lsFolios",lsFolios);

		} catch (NafinException ne) {
			System.out.println("MantenimientoEJB.ohactualizaDoctoSolicitud(): "+ne);
			throw ne;
		} catch (Exception e) {
			bOkInserta = false;
			System.out.println("Exception.MantenimientoEJB.ohactualizaDoctoSolicitud(): "+e);
			throw new NafinException("DSCT0010");
		} finally {
			lobdConexion.terminaTransaccion(bOkInserta);
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lohDoctosFolios;
	}//Fin del m�todo ohactualizaDoctoSolicitud()

	/*********************************************************************************************
	*
	*	 	    Vector ovgetDocumentosCambioEstatus()
	*
	*********************************************************************************************/
	public Vector ovgetDocumentosCambioEstatus(String esClaveIf, String esFolios,
											String esDocumentos) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	Vector lovRegistro = null;
	Vector lovDoctos = new Vector();
	String lsQry = "";
		try {
			lodbConexion.conexionDB();

			lsQry = " select e.cg_razon_social as nombreEPO, p.cg_razon_social as nombrePYME,"+
					" i.cg_razon_social as nombreIF, d.ig_numero_docto,"+
					" d.fn_monto, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc,"+
					" m.ic_moneda, m.cd_nombre, s.ic_folio"+
					" from com_solicitud s, com_documento d, comcat_epo e,"+
					" comcat_pyme p, comcat_if i, comcat_moneda m"+
					" where d.ic_documento = s.ic_documento"+
					" and d.ic_moneda = m.ic_moneda"+
					" and d.ic_pyme = p.ic_pyme"+
					" and d.ic_epo = e.ic_epo"+
					" and e.cs_habilitado = 'S' "+
					" and d.ic_if = i.ic_if"+
					" and d.ic_if = "+esClaveIf+
					" and d.ic_documento in ("+esDocumentos+")"+
					" UNION ALL "+
					" select 'No aplica' as nombreEPO, p.cg_razon_social as nombrePYME,"+
					" i.cg_razon_social as nombreIF, de.ig_numero_docto,"+
					" de.fn_monto_docto as fn_monto, TO_CHAR(s.df_v_descuento,'dd/mm/yyyy') as df_fecha_venc,"+
					" m.ic_moneda, m.cd_nombre, s.ic_folio"+
					" from com_solicitud s, com_docto_ext de, comcat_pyme p,"+
					" comcat_if i, comcat_moneda m"+
					" where s.ic_folio = de.ic_folio"+
					" and de.ic_moneda = m.ic_moneda"+
					" and de.ic_pyme = p.ic_pyme"+
					" and de.ic_if = i.ic_if"+
					" and de.ic_if = "+esClaveIf+
					" and s.cs_tipo_solicitud = 'E'"+
					" and s.ic_folio in ("+esFolios+")";
			//System.out.print(lsQry);
			ResultSet lrsDoctos = lodbConexion.queryDB(lsQry);
			while (lrsDoctos.next()) {
				lovRegistro = new Vector();
				lovRegistro.add((lrsDoctos.getString("NOMBREEPO")==null?"":lrsDoctos.getString("NOMBREEPO").trim()));
				lovRegistro.add((lrsDoctos.getString("NOMBREPYME")==null?"":lrsDoctos.getString("NOMBREPYME").trim()));
				lovRegistro.add((lrsDoctos.getString("NOMBREIF")==null?"":lrsDoctos.getString("NOMBREIF").trim()));
				lovRegistro.add(lrsDoctos.getString("IG_NUMERO_DOCTO"));
				lovRegistro.add(lrsDoctos.getString("CD_NOMBRE"));
				lovRegistro.add(lrsDoctos.getString("IC_MONEDA"));
				lovRegistro.add(lrsDoctos.getString("FN_MONTO"));
				lovRegistro.add(lrsDoctos.getString("DF_FECHA_VENC"));
				lovRegistro.add(lrsDoctos.getString("IC_FOLIO"));
				lovDoctos.addElement(lovRegistro);
			}
			lodbConexion.cierraStatement();

		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.ovgetDocumentosCambioEstatus(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta())
				lodbConexion.cierraConexionDB();
		}
	return lovDoctos;
	}


    /**
     *Vector ovgetDocumentosCambioEstatusNeg()
     * @param esNoClienteEpo
     * @param esClavePyme
     * @param esClaveIf
     * @param esNoDocto
     * @param esFechaVenc
     * @param esClaveMoneda
     * @param esMontoMin
     * @param esMontoMax
     * @param tipofactoraje
     * @return
     * @throws NafinException
     */
	public Vector ovgetDocumentosCambioEstatusNeg(String esNoClienteEpo, String esClavePyme,
												String esClaveIf, String esNoDocto,
												String esFechaVenc, String esClaveMoneda,
												String esMontoMin, String esMontoMax, String tipofactoraje ) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	Vector lovRegistro = null;
	Vector lovDoctos = new Vector();
	StringBuilder lsQry = new StringBuilder();
	List lVarBind = new ArrayList();	
	ResultSet lrsDoctos = null;	
	PreparedStatement ps	= null;
	
		try {
			lodbConexion.conexionDB();
			
			lsQry = new StringBuilder();
			lVarBind = new ArrayList();
			lsQry.append(" select /*+ use_nl (d ds pe p ed m i ie t) leading (d ds) */ "+
                                " d.ic_documento, d.ig_numero_docto, "+
				" TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, d.fn_monto, "+
				" p.cg_razon_social, ed.cd_descripcion, m.ic_moneda, m.cd_nombre, "+
				" d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_importe_interes, "+
				" ds.in_importe_recibir, i.cg_razon_social as if_cg_razon_social,"+
				" TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme"+
				" , t.CG_NOMBRE  AS TIPO_FACTORAJE "+
				" from com_documento d, comcat_pyme p, comcat_estatus_docto ed, "+
				" comcat_moneda m, com_docto_seleccionado ds, comrel_pyme_epo pe, "+
				" comcat_if i, comrel_if_epo ie "+
				" ,COMCAT_TIPO_FACTORAJE t "+
				" where d.ic_estatus_docto = ? "+
				" and d.ic_documento = ds.ic_documento"+
				" and ds.ic_epo = ? "+
				" and d.ic_pyme = p.ic_pyme and d.ic_moneda = m.ic_moneda"+
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
				" and d.ic_if = ie.ic_if and d.ic_epo = ie.ic_epo and d.ic_if = i.ic_if "+
				" and pe.cs_habilitado = ?"+
				" and d.ic_estatus_docto = ed.ic_estatus_docto "+
				" and t.CC_TIPO_FACTORAJE  = d.CS_DSCTO_ESPECIAL ");
		    
		    
				lVarBind = new ArrayList();
				lVarBind.add("3");
				lVarBind.add(esNoClienteEpo);
				lVarBind.add("S");
		    
				if (!"".equals(esClavePyme)   ) {				
				    lsQry.append(" and d.ic_pyme = ? ");
				    lVarBind.add(esClavePyme);
				}
				if (!"".equals(esClaveIf)   ) {				
				    lsQry.append(" and d.ic_if = ? ");
				    lVarBind.add(esClaveIf);
				}
				if (!"".equals(esNoDocto)   ) {	
				    lsQry.append(" and d.ig_numero_docto= ? ");
				    lVarBind.add(esNoDocto);
				}
				if (!"".equals(esFechaVenc)   ) {	
				   // lsQry.append(" and d.df_fecha_venc=TO_DATE('"+esFechaVenc+"','dd/mm/yyyy') ");
				    lsQry.append(" and d.df_fecha_venc=TO_DATE(? ,'dd/mm/yyyy') ");
				    lVarBind.add(esFechaVenc);
				}
				if (!"".equals(esClaveMoneda)   ) {
				    lsQry.append(" and d.ic_moneda= ?  ");
				    lVarBind.add(esClaveMoneda);
				}
				if (!"".equals(esMontoMin)   ) {
				    if (!"".equals(esMontoMax)   ) {				
					lsQry.append(" and d.fn_monto between ?  and ?  ");
					lVarBind.add(esMontoMin);
					lVarBind.add(esMontoMax);
				    }else{
					lsQry.append(" and d.fn_monto = "+esMontoMin);
					lVarBind.add(esMontoMin);
				    }
				}
		    
				if (!"".equals(tipofactoraje)  ) {
				    lsQry.append(" and d.CS_DSCTO_ESPECIAL= ?  ");
				    lVarBind.add(tipofactoraje);
				}
		    
				lsQry.append(" order by p.cg_razon_social, m.ic_moneda, d.df_fecha_docto, d.ig_numero_docto ");
		    
			    log.debug( "..:: lsQry : "+lsQry.toString());
			    log.debug( "..:: lVarBind : "+lVarBind);
		    
			try{
				
				
			    
				ps = lodbConexion.queryPrecompilado(lsQry.toString(), lVarBind);
				lrsDoctos = ps.executeQuery();	
				while (lrsDoctos.next()) {
					lovRegistro = new Vector();
					lovRegistro.add(new Integer(lrsDoctos.getInt("IC_MONEDA")));
					lovRegistro.add(lrsDoctos.getString("IC_DOCUMENTO"));
					lovRegistro.add((lrsDoctos.getString("CG_RAZON_SOCIAL")==null?"":lrsDoctos.getString("CG_RAZON_SOCIAL").trim()));
					lovRegistro.add((lrsDoctos.getString("IF_CG_RAZON_SOCIAL")==null?"":lrsDoctos.getString("IF_CG_RAZON_SOCIAL").trim()));
					lovRegistro.add((lrsDoctos.getString("IG_NUMERO_DOCTO")==null?"":lrsDoctos.getString("IG_NUMERO_DOCTO").trim()));
					lovRegistro.add((lrsDoctos.getString("DF_FECHA_VENC")==null?"":lrsDoctos.getString("DF_FECHA_VENC").trim()));
					lovRegistro.add((lrsDoctos.getString("CD_DESCRIPCION")==null?"":lrsDoctos.getString("CD_DESCRIPCION").trim()));
					lovRegistro.add((lrsDoctos.getString("CD_NOMBRE")==null?"":lrsDoctos.getString("CD_NOMBRE").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_MONTO")==null?"":lrsDoctos.getString("FN_MONTO").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_PORC_ANTICIPO")==null?"":lrsDoctos.getString("FN_PORC_ANTICIPO").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_MONTO_DSCTO")==null?"":lrsDoctos.getString("FN_MONTO_DSCTO").trim()));
					lovRegistro.add((lrsDoctos.getString("IN_IMPORTE_INTERES")==null?"":lrsDoctos.getString("IN_IMPORTE_INTERES").trim()));
					lovRegistro.add((lrsDoctos.getString("IN_IMPORTE_RECIBIR")==null?"":lrsDoctos.getString("IN_IMPORTE_RECIBIR").trim()));
					lovRegistro.add((lrsDoctos.getString("DF_FECHA_VENC_PYME")==null?"":lrsDoctos.getString("DF_FECHA_VENC_PYME").trim()));
					lovRegistro.add((lrsDoctos.getString("TIPO_FACTORAJE")==null?"":lrsDoctos.getString("TIPO_FACTORAJE").trim()));
					lovDoctos.addElement(lovRegistro);
				}
				lrsDoctos.close();
				ps.close();
			    
			} catch(SQLException sqle) { throw new NafinException("DSCT0047"); }

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.ovgetDocumentosCambioEstatusNeg(): "+e);
			throw new NafinException("SIST0001");
		} finally {
		    if (lodbConexion.hayConexionAbierta()) { 
			lodbConexion.cierraConexionDB(); 
		    }
		}
	return lovDoctos;
	}

	/*********************************************************************************************
	*
	*	 	    Vector ovgetDocumentosSeleccionadosPyme()
	*
	*********************************************************************************************/
	public Vector ovgetDocumentosSeleccionadosPyme(String esNoClienteEpo, String esClaves) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	Vector lovRegistro = null;
	Vector lovDoctos = new Vector();
	String lsQry = "";
		try {
			lodbConexion.conexionDB();
			lsQry = "select d.ic_documento, d.ig_numero_docto, "+
						" TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, "+
						" p.cg_razon_social, ed.cd_descripcion, m.ic_moneda, m.cd_nombre, "+
						" d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_importe_interes, "+
						" ds.in_importe_recibir, i.cg_razon_social as if_cg_razon_social, "+
						" TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme "+
						" from com_documento d, comcat_pyme p, comcat_estatus_docto ed, "+
						" comcat_moneda m, com_docto_seleccionado ds, "+
						" comcat_if i, comrel_if_epo ie "+
						" where d.ic_estatus_docto = 3"+
						" and d.ic_documento = ds.ic_documento (+)"+
						" and ds.ic_epo (+)= "+esNoClienteEpo+
						" and d.ic_pyme = p.ic_pyme and d.ic_moneda = m.ic_moneda"+
						" and d.ic_estatus_docto = ed.ic_estatus_docto"+
						" and d.ic_if = ie.ic_if and d.ic_epo = ie.ic_epo and d.ic_if = i.ic_if "+
						" and d.ic_documento in ("+esClaves+")"+
						" order by p.cg_razon_social, m.ic_moneda, d.df_fecha_docto, d.ig_numero_docto";
			try{
				ResultSet lrsDoctos = lodbConexion.queryDB(lsQry);

				System.out.println("queryDB(lsQry)"+lsQry);

				while (lrsDoctos.next()) {
					lovRegistro = new Vector();
					lovRegistro.add(new Integer(lrsDoctos.getInt("IC_MONEDA")));
					lovRegistro.add(lrsDoctos.getString("IC_DOCUMENTO"));
					lovRegistro.add((lrsDoctos.getString("CG_RAZON_SOCIAL")==null?"":lrsDoctos.getString("CG_RAZON_SOCIAL").trim()));
					lovRegistro.add((lrsDoctos.getString("IF_CG_RAZON_SOCIAL")==null?"":lrsDoctos.getString("IF_CG_RAZON_SOCIAL").trim()));
					lovRegistro.add((lrsDoctos.getString("IG_NUMERO_DOCTO")==null?"":lrsDoctos.getString("IG_NUMERO_DOCTO").trim()));
					lovRegistro.add((lrsDoctos.getString("DF_FECHA_VENC")==null?"":lrsDoctos.getString("DF_FECHA_VENC").trim()));
					lovRegistro.add((lrsDoctos.getString("CD_DESCRIPCION")==null?"":lrsDoctos.getString("CD_DESCRIPCION").trim()));
					lovRegistro.add((lrsDoctos.getString("CD_NOMBRE")==null?"":lrsDoctos.getString("CD_NOMBRE").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_MONTO")==null?"":lrsDoctos.getString("FN_MONTO").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_PORC_ANTICIPO")==null?"":lrsDoctos.getString("FN_PORC_ANTICIPO").trim()));
					lovRegistro.add((lrsDoctos.getString("FN_MONTO_DSCTO")==null?"":lrsDoctos.getString("FN_MONTO_DSCTO").trim()));
					lovRegistro.add((lrsDoctos.getString("IN_IMPORTE_INTERES")==null?"":lrsDoctos.getString("IN_IMPORTE_INTERES").trim()));
					lovRegistro.add((lrsDoctos.getString("IN_IMPORTE_RECIBIR")==null?"":lrsDoctos.getString("IN_IMPORTE_RECIBIR").trim()));
					lovRegistro.add((lrsDoctos.getString("DF_FECHA_VENC_PYME")==null?"":lrsDoctos.getString("DF_FECHA_VENC_PYME").trim()));
					lovDoctos.addElement(lovRegistro);

				}

					System.out.println("lovRegistro"+lovRegistro);
				lodbConexion.cierraStatement();
			} catch(SQLException sqle) { throw new NafinException("DSCT0047"); }

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.ovgetDocumentosSeleccionadosPyme(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta()) lodbConexion.cierraConexionDB();
		}
	return lovDoctos;
	}

	/*********************************************************************************************
	*
	*	 	    String osactualizaHistoricoCambioEstatus()
	*
	*********************************************************************************************/

	public List osactualizaHistoricoCambioEstatus(String seleccionados[], String estatus[],String ct_cambio_motivo, String usuario) throws NafinException {//FODEA 015 - 2009 ACF
	AccesoDB lodbConexion = new AccesoDB();
	ResultSet lrsDoctos = null;
	PreparedStatement ps = null;
	String lsQry = "", lsClaves = "",lsClavesOper = "";
	String lsClavesSeleccionadasPyme = "";
	List retorno = new ArrayList();

		try {
			lodbConexion.conexionDB();
			for (int i=0; i<=seleccionados.length-1; i++) {

				try{

					if(estatus[i].equals("N") || estatus[i].equals("3")){

						lsQry =	"select count(*) from com_documento"+
						" where ic_documento = ?"+
						" and ic_estatus_docto not in (3,23,24) ";//agregado 23 15/11/2004

						ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setString(1,seleccionados[i]);
						lrsDoctos = ps.executeQuery();
						lrsDoctos.next();
						int existentes = lrsDoctos.getInt(1);
						lrsDoctos.close();

						if(existentes == 0) {	//No hubo error, por problemas de concurrencia?
							lsQry =	"select ic_estatus_docto from com_documento"+
									" where ic_documento = "+seleccionados[i];
							lrsDoctos = lodbConexion.queryDB(lsQry);
							lrsDoctos.next();


							//before String estatus = (lrsDoctos.getString(1).equals("3"))?"2":"23";
							/*new 18/11/2004*/
							String cambioEstatus ="";
							if(lrsDoctos.getString(1).equals("3")){
							   cambioEstatus="2";
							}
							else if(lrsDoctos.getString(1).equals("24")){
							   cambioEstatus="23";

							}
							else if(lrsDoctos.getString(1).equals("23")){
							   cambioEstatus="26";
							}
	                        /*end new 18/11/2004*/

							lrsDoctos.close();

							lsQry = "insert into comhis_cambio_estatus (dc_fecha_cambio,"+
									" ic_documento, ic_cambio_estatus, ct_cambio_motivo, cg_nombre_usuario)"+//FODEA 015 - 2009 ACF
									" values (sysdate,"+seleccionados[i]+", "+cambioEstatus+", '"+ct_cambio_motivo+"', '"+usuario+"')";//FODEA 015 - 2009 ACF
              System.out.println("..:: lsQry : "+lsQry);
							lodbConexion.ejecutaSQL(lsQry);


							lodbConexion.terminaTransaccion(true);

							if (lsClaves.equals(""))
								lsClaves = seleccionados[i];
							else
								lsClaves = lsClaves+","+seleccionados[i];
						}//fin del if
						ps.close();

					// Registrar en el historial, cambio de estatus de documento a Seleccionada Pyme
					}else if( estatus[i].equals("SP") ){

						// Revisar que el documento se encuentre en el estatus origen indicado
						lsQry =
							" select count(*) from com_documento "+
							" where ic_documento = ? "+
							" and ic_estatus_docto not in (24) ";// Por el momento solo se registraran los cambios para procesos
																			 // con estatus: En Proceso de Autorizacion IF
						ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setString(1,seleccionados[i]);
						lrsDoctos = ps.executeQuery();
						lrsDoctos.next();
						int existentes = lrsDoctos.getInt(1);
						lrsDoctos.close();

						// Existentes == 0 => el documento se encuentra en el estatus origen indicado
						if(existentes == 0) {	//No hubo error, por problemas de concurrencia?

							// Consultar estatus actual del documento
							lsQry =
									" select ic_estatus_docto from com_documento "+
									" where ic_documento = "+seleccionados[i];
							lrsDoctos = lodbConexion.queryDB(lsQry);
							lrsDoctos.next();

							String cambioEstatus ="";
							if(lrsDoctos.getString(1).equals("24")){
							   cambioEstatus="34"; // En Proceso de Autorizacion IF a Seleccionado Pyme
							}
							lrsDoctos.close();

							// Registrar en bitacora el cambio de estatus
							lsQry =
									" insert into comhis_cambio_estatus (dc_fecha_cambio, "+
									" ic_documento, ic_cambio_estatus, ct_cambio_motivo, cg_nombre_usuario)"+
									" values (sysdate,"+seleccionados[i]+", "+cambioEstatus+", '"+ct_cambio_motivo+"', '"+usuario+"')";
							System.out.println("..:: lsQry(SP) : "+lsQry);
							lodbConexion.ejecutaSQL(lsQry);
							lodbConexion.terminaTransaccion(true);

							// Agregar documento cuyo cambio de estatus fue registrado a la lista de Claves Seleccionadas Pyme
							if (lsClavesSeleccionadasPyme.equals(""))
								lsClavesSeleccionadasPyme = seleccionados[i];
							else
								lsClavesSeleccionadasPyme = lsClavesSeleccionadasPyme+","+seleccionados[i];

						}//fin del if
						ps.close();

					}else{	// fin de if estatus
						if (lsClavesOper.equals(""))
							lsClavesOper = seleccionados[i];
						else
							lsClavesOper = lsClavesOper+","+seleccionados[i];
					}
				} catch(SQLException sqle) { throw new NafinException("DSCT0011"); }
			} //for
			retorno.add(lsClaves);
			retorno.add(lsClavesOper);
			retorno.add(lsClavesSeleccionadasPyme);
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.osactualizaHistoricoCambioEstatus(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta()) lodbConexion.cierraConexionDB();
		}
	return retorno;
	}

	/*********************************************************************************************
	*
	*	 	    boolean bactualizaEstatusNegociable()
	*
	*********************************************************************************************/
	/** Actualiza a Negociables los documentos cuyos IDs se proporcionan en la lista <tt>esClaves</tt>.
	 *
	 * @param esClaves Lista con las claves de los documentos a retornar a negociables
	 *
	 */
	public boolean bactualizaEstatusNegociable(String esClaves) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	boolean lbActualizo = true;
	String lsQry = "";
		try {
			lodbConexion.conexionDB();
			lsQry = " update com_documento set ic_estatus_docto = 2"+
					" where ic_documento in ("+esClaves+")"+
					" and ic_estatus_docto in (3,23,24)";//agregado 23 15/11/2004
			try {
				lodbConexion.ejecutaSQL(lsQry);
			} catch(SQLException sqle) {
				lbActualizo = false;
				throw new NafinException("DSCT0011");
			}

			// Foda 99 (Notas de Credito) - HDG 13/12/2004
			vactualizaEstatusNegociableNotasCredito(esClaves, lodbConexion);

		} catch (NafinException ne) {
			lbActualizo = false;
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.bactualizaEstatusNegociable(): "+e);
			lbActualizo = false;
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta()){
				lodbConexion.terminaTransaccion(lbActualizo);
				lodbConexion.cierraConexionDB();
			}
		}
		return lbActualizo;
	}

	/**
	 * @deprecated
	 * ESTE METODO SE HA VUELTO OBSOLETO. NO USAR.
	 * Actualiza a Negociables los documentos cuyos IDs se proporcionan en la lista esClaves para una
	 * EPO en particular.
	 *
	 * @param esClaves Lista con las claves de los documentos a retornar a negociables
	 * @param ic_epo Clave de la Epo que publico los documentos.
	 *
	 */
	public boolean bactualizaEstatusNegociable(String esClaves,String ic_epo) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	boolean lbActualizo = true;
	String lsQry = "";
		try {
			lodbConexion.conexionDB();
			lsQry = " update com_documento set ic_estatus_docto = 2"+
					" where ic_documento in ("+esClaves+")"+
					" and ic_estatus_docto in (3,23,24)";//agregado 23 15/11/2004
			try {
				lodbConexion.ejecutaSQL(lsQry);
			} catch(SQLException sqle) {
				lbActualizo = false;
				throw new NafinException("DSCT0011");
			}

			// Foda 99 (Notas de Credito) - HDG 13/12/2004
			vactualizaEstatusNegociableNotasCredito(esClaves, lodbConexion,ic_epo);

		} catch (NafinException ne) {
			lbActualizo = false;
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.bactualizaEstatusNegociable(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbActualizo);
			if (lodbConexion.hayConexionAbierta()) lodbConexion.cierraConexionDB();
		}
	return lbActualizo;
	}

	/*********************************************************************************************
	*
	*	 	    void vactualizaEstatusNegociableNotasCredito()
	*
	*********************************************************************************************/
	/**
	 * Actualiza estatus a negociable de las notas de credito y borra referencias de los documentos
	 * de tablas como comhis_cambio_estatus.
	 *
	 * @param esClaves lista de IDs de los Documentos que se retornaran a negociables
	 * @param lobConexion conexion a la Base de Datos
	 */
	private void vactualizaEstatusNegociableNotasCredito(String listaDocumentos, AccesoDB lodbConexion) throws NafinException {

		String 				lsQry 			= "";
		ResultSet 			rs1, rs2 		= null;
		PreparedStatement ps 				= null;

		boolean 				hayDocumentos 	= false;
		StringBuffer		esClaves			= null;

		try {

			try {

				// 1. OBTENER AQUELLOS DOCUMENTOS SIN NOTA DE CREDITO Y AQUELLOS
				//    QUE UTILIZAN NOTAS DE CREDITO SIMPLES.
				hayDocumentos									= false;
				esClaves					 						= new StringBuffer();

				VectorTokenizer 	vtk 						= new VectorTokenizer(listaDocumentos,",");
				Vector 				claves 					= vtk.getValuesVector();

				StringBuffer		listaVariablesBind	= new StringBuffer();
				for(int k=0;k<claves.size();k++) {
					if(k>0) listaVariablesBind.append(",");
					listaVariablesBind.append("?");
				}

				lsQry = "SELECT           "  +
						  "	IC_DOCUMENTO  "  +
						  "FROM             "  +
						  "   COM_DOCUMENTO "  +
						  "WHERE            "  +
						  "    IC_DOCUMENTO IN ( "+listaVariablesBind.toString()+" ) AND "  +
						  "    IC_DOCUMENTO NOT IN ( "  +
						  "      SELECT DISTINCT IC_DOCUMENTO FROM COMREL_NOTA_DOCTO "  +
						  " ) ";

				ps = lodbConexion.queryPrecompilado(lsQry);
				for(int k=0;k<claves.size();k++) {
					String ic_documento = (String) claves.get(k);
					ps.setInt((k+1),Integer.parseInt(ic_documento));
				}
				rs1 = ps.executeQuery();

				for(int k=0;rs1.next();k++){
					if(k>0) esClaves.append(",");
					esClaves.append(rs1.getString("IC_DOCUMENTO"));
					hayDocumentos = true;
				}

				rs1.close();
				ps.close();
				lodbConexion.cierraStatement();

				if(hayDocumentos){

					VectorTokenizer 	vt 		= new VectorTokenizer(esClaves.toString(),",");
					Vector 				doctos 	= vt.getValuesVector();
					int 					existen 	= 0;

					for(int i=0;i<doctos.size();i++) {
						String ic_documento = doctos.get(i).toString();
						lsQry = " select count(1) from comhis_cambio_estatus "+
								" where ic_documento = ? "+
								" and ic_cambio_estatus = 8 ";
						ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setInt(1,Integer.parseInt(ic_documento));
						rs1 = ps.executeQuery();
						existen = 0;
						if(rs1.next())
							existen = rs1.getInt(1);
						rs1.close();
						if(ps!=null) ps.close();
						if(existen==0) {
							lsQry =
								" UPDATE com_documento"   +
								"    SET cs_cambio_importe = 'N'"   +
								"  WHERE ic_documento = ?"  ;
							ps = lodbConexion.queryPrecompilado(lsQry);
							ps.setInt(1,Integer.parseInt(ic_documento));
							ps.executeUpdate();
							if(ps!=null) ps.close();
						}//if(existen=0)
					}//for(int i=0;i<vt.size();i++)

					// Borra historial de los documentos que cambiaron de monto.
					lsQry = " delete comhis_cambio_estatus "+
							" where ic_documento in ("+esClaves.toString()+") "+
							" and ic_cambio_estatus = 28 ";
					lodbConexion.ejecutaSQL(lsQry);

					// Fodea 002 - 2010
					// Borra del historial las Notas de Credito
					lsQry =
							" delete comhis_cambio_estatus "  +
							" where ic_documento in ( select ic_documento "  +
							"                         from com_documento "  +
							"								  where ic_docto_asociado in ("  + esClaves.toString() +") )";
					lodbConexion.ejecutaSQL(lsQry);

					// Borra los documentos "notas de credito" que fueron seleccionadas.
					lsQry = " delete com_docto_seleccionado "+
							" where ic_documento in ( select ic_documento "+
							"							from com_documento "+
							"							where ic_docto_asociado in ("+esClaves.toString()+") )";
					lodbConexion.ejecutaSQL(lsQry);

					// Ahora se actualiza el monto del documento.
					lsQry = " select sum(fn_monto) as monto_docto, ic_docto_asociado "+
							" from com_documento "+
							" where ic_docto_asociado in ("+esClaves.toString()+")"+
							" group by ic_docto_asociado";
					rs1 = lodbConexion.queryDB(lsQry);

					while(rs1.next()) {
						lsQry = " select fn_monto+"+rs1.getString("monto_docto")+" as monto from com_documento "+
								" where ic_documento = "+rs1.getString("ic_docto_asociado");
						rs2 = lodbConexion.queryDB(lsQry);
						while(rs2.next()) {
							lsQry = " update com_documento "+
									" set fn_monto = "+rs2.getString("monto")+
									" where ic_documento = "+rs1.getString("ic_docto_asociado");
							lodbConexion.ejecutaSQL(lsQry);
						}
						rs2.close();
					}
					rs1.close();
					lodbConexion.cierraStatement();

					// Se actualizan los valores de la nota de credito.
					lsQry = " update com_documento "+
							" set ic_estatus_docto = 2 "+
							" , ic_if = null "+
							" , df_fecha_venc = null "+
							" , ic_docto_asociado = null "+
							" where ic_docto_asociado in ("+esClaves.toString()+")"+
							" and cs_dscto_especial = 'C' "+
							" and ic_estatus_docto in (3,23,24)";	//agregado 23 15/11/2004
					lodbConexion.ejecutaSQL(lsQry);

				}

				// 2. TRAER AQUELLOS DOCUMENTOS A LOS CUALES SE LES APLICO NOTAS DE CREDITO MULTIPLE
				hayDocumentos								= false;
				esClaves							 			= new StringBuffer();

				vtk 											= new VectorTokenizer(listaDocumentos,",");
				claves 										= vtk.getValuesVector();

				listaVariablesBind	= new StringBuffer();
				for(int k=0;k<claves.size();k++) {
					if(k>0) listaVariablesBind.append(",");
					listaVariablesBind.append("?");
				}

				lsQry = 	"SELECT                "  +
							"  DISTINCT			     "  +
			 				"   IC_DOCUMENTO       "  +
			 				"FROM                  "  +
			 				"   COMREL_NOTA_DOCTO  "  +
			 				"WHERE                 "  +
			 				"   IC_DOCUMENTO IN (  "  +
			 				"    " + listaVariablesBind.toString() + "  "  +
			 				"   ) ";

				ps = lodbConexion.queryPrecompilado(lsQry);
				for(int k=0;k<claves.size();k++) {
					String ic_documento = (String) claves.get(k);
					ps.setInt((k+1),Integer.parseInt(ic_documento));
				}
				rs1 = ps.executeQuery();

				for(int k=0;rs1.next();k++){
					if(k>0) esClaves.append(",");
					esClaves.append(rs1.getString("IC_DOCUMENTO"));
					hayDocumentos = true;
				}

				rs1.close();
				ps.close();
				lodbConexion.cierraStatement();

				if(hayDocumentos){

					VectorTokenizer 	vt 		= new VectorTokenizer(esClaves.toString(),",");
					Vector 				doctos 	= vt.getValuesVector();
					int 					existen 	= 0;

					for(int i=0;i<doctos.size();i++) {
						String ic_documento = doctos.get(i).toString();
						lsQry = " select count(1) from comhis_cambio_estatus "+
								" where ic_documento = ? "+
								" and ic_cambio_estatus = 8 ";
						ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setInt(1,Integer.parseInt(ic_documento));
						rs1 = ps.executeQuery();
						existen = 0;
						if(rs1.next())
							existen = rs1.getInt(1);
						rs1.close();
						if(ps!=null) ps.close();
						if(existen==0) {
							lsQry =
								" UPDATE com_documento"   +
								"    SET cs_cambio_importe = 'N'"   +
								"  WHERE ic_documento = ?"  ;
							ps = lodbConexion.queryPrecompilado(lsQry);
							ps.setInt(1,Integer.parseInt(ic_documento));
							ps.executeUpdate();
							if(ps!=null) ps.close();
						}//if(existen=0)
					}//for(int i=0;i<vt.size();i++)

					// Borra historial de los documentos que cambiaron de monto.
					lsQry = " delete comhis_cambio_estatus "+
							" where ic_documento in ("+esClaves.toString()+") "+
							" and ic_cambio_estatus = 28 ";

					lodbConexion.ejecutaSQL(lsQry);

					// Fodea 002 - 2010
					// Borra del historial las Notas de Credito
					lsQry =
							" delete comhis_cambio_estatus "  +
							" where ic_documento in ( select distinct ic_nota_credito "  +
							"                         from comrel_nota_docto "  +
							"								  where ic_documento in ("  + esClaves.toString() +") )";
					lodbConexion.ejecutaSQL(lsQry);

					// Fodea 002 - 2010
					// Borra los documentos "notas de credito" que fueron seleccionadas.
					lsQry =
							" delete com_docto_seleccionado "  +
							" where ic_documento in ( select distinct ic_nota_credito "  +
							"                         from comrel_nota_docto "  +
							"								  where ic_documento in ("  + esClaves.toString() +") )";
					lodbConexion.ejecutaSQL(lsQry);

					// Fodea 002 - 2010
					// Ahora se actualiza el monto del documento.
					lsQry = " select sum(fn_monto_aplicado) as monto_docto, ic_documento as ic_docto_asociado "  +
							  " from comrel_nota_docto "  +
							  " where ic_documento in ("  + esClaves.toString() + ")" +
							  " group by ic_documento";
					rs1 = lodbConexion.queryDB(lsQry);

					while(rs1.next()){
						lsQry = " select fn_monto+"+rs1.getString("monto_docto")+" as monto from com_documento "+
								" where ic_documento = "+rs1.getString("ic_docto_asociado");
						rs2 = lodbConexion.queryDB(lsQry);
						while(rs2.next()) {
							lsQry = " update com_documento "+
									" set fn_monto = "+rs2.getString("monto")+
									" where ic_documento = "+rs1.getString("ic_docto_asociado");
							lodbConexion.ejecutaSQL(lsQry);
						}
						rs2.close();
					}
					rs1.close();
					lodbConexion.cierraStatement();

					// Fodea 002 - 2010. Regresar Nota de Credito a su Estado Original
					lsQry = " update com_documento "+
							" set ic_estatus_docto = 2 "+
							" , ic_if = null "+
							" , df_fecha_venc = null "+
							" , ic_docto_asociado = null "+
							" where ic_documento in ( select distinct ic_nota_credito from comrel_nota_docto where ic_documento in ("+esClaves.toString()+") )"+
							" and cs_dscto_especial = 'C' "+
							" and ic_estatus_docto in (3,23,24)";
					lodbConexion.ejecutaSQL(lsQry);

					// Fodea 002 - 2010. Borrar de Comrel Nota Docto toda referencia a Notas de Credito Multiple
					lsQry = " delete comrel_nota_docto "+
							  " where ic_documento in ("+esClaves.toString()+") ";
					lodbConexion.ejecutaSQL(lsQry);

				}

			} catch(SQLException sqle) {
				System.out.println("Exception.MantenimientoEJB.vactualizaEstatusNegociableNotasCredito(): "+sqle);
				System.out.println("lsQry: "+lsQry);

				throw new NafinException("DSCT0094");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.vactualizaEstatusNegociableNotasCredito(): "+e);
			throw new NafinException("SIST0001");
		}
	}

	/**
	 * @deprecated
	 * ESTE METODO SE HA VUELTO OBSOLETO. NO USAR.
	 * Actualiza estatus a negociable de las notas de credito y borra referencias de los documentos
	 * de tablas como comhis_cambio_estatus.
	 *
	 * @param esClaves lista de IDs de los Documentos que se retornaran a negociables
	 * @param lobConexion conexion a la Base de Datos
	 * @param ic_epo Clave de la Epo que publico los documentos en cuestion.
	 */
	private void vactualizaEstatusNegociableNotasCredito(String esClaves, AccesoDB lodbConexion, String ic_epo) throws NafinException {

		String 				lsQry 	= "";
		ResultSet 			rs1, rs2 = null;
		PreparedStatement ps 		= null;

		boolean operaNotasDeCredito                = false;
		boolean aplicarNotasDeCreditoAVariosDoctos = false;

		try {

			if(ic_epo != null && !ic_epo.equals("")){
				// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
				operaNotasDeCredito                = operaNotasDeCreditoParaDescuentoElectronico(ic_epo);
				// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
				aplicarNotasDeCreditoAVariosDoctos = hasAplicarNotaDeCreditoAVariosDoctosEnabled(ic_epo);
			}

			try {
				VectorTokenizer vt = new VectorTokenizer(esClaves,",");
				Vector doctos = vt.getValuesVector();
				int existen = 0;
				for(int i=0;i<doctos.size();i++) {
					String ic_documento = doctos.get(i).toString();
					lsQry = " select count(1) from comhis_cambio_estatus "+
							" where ic_documento = ? "+
							" and ic_cambio_estatus = 8 ";
					ps = lodbConexion.queryPrecompilado(lsQry);
					ps.setInt(1,Integer.parseInt(ic_documento));
					rs1 = ps.executeQuery();
					existen = 0;
					if(rs1.next())
						existen = rs1.getInt(1);
					rs1.close();
					if(ps!=null) ps.close();
					if(existen==0) {
						lsQry =
							" UPDATE com_documento"   +
							"    SET cs_cambio_importe = 'N'"   +
							"  WHERE ic_documento = ?"  ;
						ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setInt(1,Integer.parseInt(ic_documento));
						ps.executeUpdate();
						if(ps!=null) ps.close();
					}//if(existen=0)
				}//for(int i=0;i<vt.size();i++)

				// Borra historial de los documentos que cambiaron de monto.
				lsQry = " delete comhis_cambio_estatus "+
						" where ic_documento in ("+esClaves+") "+
						" and ic_cambio_estatus = 28 ";

				lodbConexion.ejecutaSQL(lsQry);

				// Fodea 002 - 2010
				// Borra del historial las Notas de Credito
				lsQry =
						" delete comhis_cambio_estatus "  +
						" where ic_documento in ( select ic_documento "  +
						"                         from com_documento "  +
						"								  where ic_docto_asociado in ("  + esClaves +") )";
				// Si son notas de credito multiples
				if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					lsQry =
						" delete comhis_cambio_estatus "  +
						" where ic_documento in ( select distinct ic_nota_credito "  +
						"                         from comrel_nota_docto "  +
						"								  where ic_documento in ("  + esClaves +") )";
				}
				lodbConexion.ejecutaSQL(lsQry);

				// Borra los documentos "notas de credito" que fueron seleccionadas.
				lsQry = " delete com_docto_seleccionado "+
						" where ic_documento in ( select ic_documento "+
						"							from com_documento "+
						"							where ic_docto_asociado in ("+esClaves+") )";

				// Fodea 002 - 2010
				if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					lsQry =
						" delete com_docto_seleccionado "  +
						" where ic_documento in ( select distinct ic_nota_credito "  +
						"                         from comrel_nota_docto "  +
						"								  where ic_documento in ("  + esClaves +") )";
				}

				lodbConexion.ejecutaSQL(lsQry);

				// Ahora se actualiza el monto del documento.
				lsQry = " select sum(fn_monto) as monto_docto, ic_docto_asociado "+
						" from com_documento "+
						" where ic_docto_asociado in ("+esClaves+")"+
						" group by ic_docto_asociado";

				// Fodea 002 - 2010
				if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					lsQry = " select sum(fn_monto_aplicado) as monto_docto, ic_documento as ic_docto_asociado "  +
					        " from comrel_nota_docto "  +
							  " where ic_documento in ("  + esClaves + ")" +
							  " group by ic_documento";
				}

				rs1 = lodbConexion.queryDB(lsQry);
				while(rs1.next()) {

					lsQry = " select fn_monto+"+rs1.getString("monto_docto")+" as monto from com_documento "+
							" where ic_documento = "+rs1.getString("ic_docto_asociado");
					rs2 = lodbConexion.queryDB(lsQry);
					while(rs2.next()) {
						lsQry = " update com_documento "+
								" set fn_monto = "+rs2.getString("monto")+
								" where ic_documento = "+rs1.getString("ic_docto_asociado");
						lodbConexion.ejecutaSQL(lsQry);
					}
					rs2.close();
				}
				rs1.close();
				lodbConexion.cierraStatement();

				// Se actualizan los valores de la nota de credito.
				lsQry = " update com_documento "+
						" set ic_estatus_docto = 2 "+
						" , ic_if = null "+
						" , df_fecha_venc = null "+
						" , ic_docto_asociado = null "+
						" where ic_docto_asociado in ("+esClaves+")"+
						" and cs_dscto_especial = 'C' "+
						" and ic_estatus_docto in (3,23,24)";	//agregado 23 15/11/2004

				// Fodea 002 - 2010. Regresar Nota de Credito a su Estado Original
				if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					lsQry = " update com_documento "+
						" set ic_estatus_docto = 2 "+
						" , ic_if = null "+
						" , df_fecha_venc = null "+
						" , ic_docto_asociado = null "+
						" where ic_documento in ( select distinct ic_nota_credito from comrel_nota_docto where ic_documento in ("+esClaves+") )"+
						" and cs_dscto_especial = 'C' "+
						" and ic_estatus_docto in (3,23,24)";
				}
				lodbConexion.ejecutaSQL(lsQry);

				// Fodea 002 - 2010. Borrar de Comrel Nota Docto toda referencia a Notas de Credito Multiple
				if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					lsQry = " delete comrel_nota_docto "+
							  " where ic_documento in ("+esClaves+") ";
					lodbConexion.ejecutaSQL(lsQry);
				}

			} catch(SQLException sqle) {
				System.out.println("Exception.MantenimientoEJB.vactualizaEstatusNegociableNotasCredito(): "+sqle);
				System.out.println("lsQry: "+lsQry);

				throw new NafinException("DSCT0094");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("Exception.MantenimientoEJB.vactualizaEstatusNegociableNotasCredito(): "+e);
			throw new NafinException("SIST0001");
		}
	}

/*********************OBTIENE ACUSE DE DOCTOS PRE NEGOCIABLES SMJ 26/06/2006 FODA 044************************************/
public Vector ovgetPreNegociables(String ic_epo, String cc_acuse, String fechaCargaIni, String fechaCargaFin, String ic_usuario, String ic_estatus)
throws NafinException {
	return ovgetPreNegociables(ic_epo, cc_acuse, fechaCargaIni, fechaCargaFin, ic_usuario, ic_estatus,"");
}

public Vector ovgetPreNegociables(String ic_epo, String cc_acuse, String fechaCargaIni, String fechaCargaFin, String ic_usuario, String ic_estatus, String numDoctos) throws NafinException {
	Vector lovDatos = null;
	String condicion= "";
	int cont=0;
	Vector lovPrenegociables = new Vector();
	AccesoDB lobdConexion = new AccesoDB();

	try{
		lobdConexion.conexionDB();

		if(!"".equals(cc_acuse))
		condicion+="AND a.cc_acuse	= ? ";
		if(!"".equals(ic_usuario))
		condicion+="AND a.ic_usuario= ? ";
		if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin))
			condicion+=" AND a.df_fechahora_carga >= TO_DATE(?,'dd/mm/yyyy') AND a.df_fechahora_carga< TO_DATE(?,'dd/mm/yyyy') + 1 ";
		if(!"".equals(ic_estatus))
		condicion+="AND d.ic_estatus_docto= ? ";
		if(!("").equals(numDoctos))
				condicion+=" AND d.ig_numero_docto in ("+numDoctos+") ";


		String lsQry =  " SELECT a.cc_acuse, "+
						" TO_CHAR (a.df_fechahora_carga, 'DD/MM/YYYY') fecha_carga, a.ic_usuario, "+
						" a.in_total_docto_mn,a.fn_total_monto_mn,a.in_total_docto_dl,a.fn_total_monto_dl,a.cg_hash"+
						" FROM com_documento d, com_acuse1 a "+
						" WHERE d.cc_acuse=a.cc_acuse " +
						" AND d.ic_estatus_docto in (28,29) " +
						" AND d.ic_epo=? "+ condicion +
						" GROUP BY a.cc_acuse, TO_CHAR (a.df_fechahora_carga, 'DD/MM/YYYY') , "+
						" a.ic_usuario, a.in_total_docto_mn,a.fn_total_monto_mn,a.in_total_docto_dl,a.fn_total_monto_dl,a.cg_hash "+
						" ORDER BY a.cc_acuse, fecha_carga";

		//System.out.println(":::::QUERY::::::"+lsQry);
		//try {
			PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);

			cont++;ps.setString(cont, ic_epo);

			if(!"".equals(cc_acuse)){
				cont++;ps.setString(cont, cc_acuse);
			}
			if(!"".equals(ic_usuario)){
				cont++; ps.setString(cont, ic_usuario);
			}
			if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin)){
				cont++; ps.setString(cont, fechaCargaIni);
				cont++; ps.setString(cont, fechaCargaFin);
			}
			if(!"".equals(ic_estatus)){
				cont++; ps.setString(cont, ic_estatus);
			}

			ResultSet lrsQry = ps.executeQuery();

			while(lrsQry.next()) {
				lovDatos = new Vector();
				lovDatos.add((lrsQry.getString("cc_acuse")==null?"":lrsQry.getString("cc_acuse").trim()));
				lovDatos.add((lrsQry.getString("fecha_carga")==null?"":lrsQry.getString("fecha_carga").trim()));
				lovDatos.add((lrsQry.getString("ic_usuario")==null?"":lrsQry.getString("ic_usuario").trim()));
				lovDatos.add((lrsQry.getString("in_total_docto_mn")==null?"":lrsQry.getString("in_total_docto_mn").trim()));
 			    lovDatos.add((lrsQry.getString("fn_total_monto_mn")==null?"":lrsQry.getString("fn_total_monto_mn").trim()));
				lovDatos.add((lrsQry.getString("in_total_docto_dl")==null?"":lrsQry.getString("in_total_docto_dl").trim()));
				lovDatos.add((lrsQry.getString("fn_total_monto_dl")==null?"":lrsQry.getString("fn_total_monto_dl").trim()));
				lovDatos.add((lrsQry.getString("cg_hash")==null?"":lrsQry.getString("cg_hash").trim()));
				//System.out.println(":::::LOVDATOS::::"+lovDatos);
				lovPrenegociables.addElement(lovDatos);

			}
			//lobdConexion.cierraX	Statement();
		//} catch (SQLException sqle) { throw new NafinException("PARAM0010"); }

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("Exception en ovgetPreNegociales(). "+e);
		throw new NafinException("SIST0001");
	} finally {
		if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
	}
	return lovPrenegociables;
	} // Fin Metodo ovgetPreNegociales


/*********************ELIMINA LOS DOCUMENTOS PRE NEGOCIABLES SMJ 26/06/2006 FODA 044************************************/

	public void eliminaPrenegociables(String ic_epo, String cc_acuse) throws NafinException{
		AccesoDB con = null;
		boolean exito = true;
		String strSQL ="";
		String ic_docto="";
		StringBuffer strQuery = new StringBuffer();
		List varBind = new ArrayList();
		ResultSet rs = null;
		boolean existenDoctos = false;
		System.out.println("CMantenimientoBean::eliminaPrenegociables(E)");
		try {
		 	con = new AccesoDB();
			con.conexionDB();

			strSQL = " SELECT	ic_documento	FROM	com_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? "+
						" AND ic_estatus_docto IN (?, ?) ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			ps.setInt(3, 28);
			ps.setInt(4, 29);
			rs = ps.executeQuery();
			if(rs!=null){
				int i=0;
				while(rs.next()){
					ic_docto = rs.getString("ic_documento");
					if(i==0){
						strQuery.append(" DELETE FROM com_documento_detalle" +
											" WHERE ic_documento IN (?");
					}else{
						strQuery.append(",?");
					}
					varBind.add(new Long(ic_docto));
					i++;
				}
				strQuery.append(" ) ");
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

			if (varBind.size() > 0){
				ps = con.queryPrecompilado(strQuery.toString(), varBind);
				ps.executeUpdate();
				if(ps!=null)ps.close();
			}

			strSQL =" DELETE FROM com_documento" +
					" WHERE cc_acuse = ? " +
					" AND ic_epo=? " +
					" AND ic_estatus_docto in (?,?)";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			ps.setInt(3, 28);
			ps.setInt(4, 29);
			ps.executeUpdate();
			System.out.println(":::::strSQL 1 ::::::"+strSQL);

			if(ps!=null)ps.close();

			//valida si aun existe documentos para el acuse en cuestion
			strSQL = " select count(1) numDocs from  com_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				if(!"0".equals(rs.getString("numDocs")))
					existenDoctos = true;
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

			System.out.println(":::::strSQL 2 ::::::"+strSQL);


			if(!existenDoctos){
				strSQL =" DELETE FROM com_acuse1" +
						" WHERE cc_acuse = ?" ;

						ps = con.queryPrecompilado(strSQL);
						ps.setString(1, cc_acuse);
						ps.executeUpdate();

				System.out.println(":::::strSQL 3 ::::::"+strSQL);
			}



		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			exito = false;
			System.out.println("MantenimientoBean::eliminaPrenegociables Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoBean::eliminaPrenegociables (S)");
		}
	}//fin del metodo eliminaPrenegociables()

/***Obtiene los documentos Cargados para Confirmaci�n Hash***/
public Vector mostrarDocumentosPreNegociables(
			String sAforo,
			String sNoCliente,
			String sEstatus,
			String sAforoDL, String cc_acuse) throws NafinException {

	return mostrarDocumentosPreNegociables(sAforo, sNoCliente, sEstatus ,sAforoDL, cc_acuse, "");
}

public Vector mostrarDocumentosPreNegociables(
			String sAforo,
			String sNoCliente,
			String sEstatus,
			String sAforoDL, String cc_acuse, String numDoctos) throws NafinException {
		AccesoDB con = new AccesoDB();
		Vector vDoctos = new Vector();
		Vector vd = null;

		try{
			con.conexionDB();


			String query =
				" SELECT P.cg_razon_social, CD.ig_numero_docto, "+
				" TO_CHAR(CD.df_fecha_docto,'DD/MM/YYYY') as fecha_docto, "+
				" TO_CHAR(CD.df_fecha_venc,'DD/MM/YYYY') as fecha_venc, "+
				" M.cd_nombre AS CD_NOMBRE, "+
				" M.ic_moneda, "+
	            " CD.fn_monto, "+
				" CD.cs_dscto_especial, "+
				" 100*DECODE (CD.ic_moneda, 1, ?, 54, ?)  as porc_anticipo, "+
				" CD.fn_monto*DECODE (CD.ic_moneda, 1, ?, 54, ?)  as monto_dscto, "+
				" CD.ct_referencia,"+
				" I.cg_razon_social as nombre_IF, "+
	            " P.ic_pyme, "+
				" ED.cd_descripcion as cd_descripcion, "+
	            " CD.cg_campo1, CD.cg_campo2, "+
				" CD.cg_campo3, CD.cg_campo4, CD.cg_campo5, "+
				" B.cg_razon_social as nombre_beneficiario, "+
	            " CD.fn_porc_beneficiario, "+
				" (CD.fn_porc_beneficiario/100)*CD.fn_monto as monto_beneficiario, "+
				" CD.ic_estatus_docto "+
				" FROM  com_documento CD, comcat_estatus_docto ED, "+
				" comcat_pyme P, comcat_moneda M, comcat_if I, comcat_if B, com_acuse1 ca "+
				" WHERE CD.ic_pyme = P.ic_pyme "+
				" AND CD.ic_moneda = M.ic_moneda "+
				" AND CD.ic_estatus_docto = ED.ic_estatus_docto "+
				" AND CD.ic_if = I.ic_if(+) "+
				" AND CD.ic_beneficiario = B.ic_if(+) "+
	         " AND CD.cc_acuse=ca.cc_acuse "+
				" AND CD.ic_epo = ? "+
	         " AND ca.cc_acuse=? "+
	         " AND CD.ic_estatus_docto = ? ";
				if(!("").equals(numDoctos))
					query +=" AND CD.ig_numero_docto in ( "+numDoctos+") ";

			PreparedStatement ps = con.queryPrecompilado(query);
			ps.setString(1, sAforo);
			ps.setString(2, sAforoDL);
			ps.setString(3, sAforo);
			ps.setString(4, sAforoDL);
			ps.setString(5, sNoCliente);
			ps.setString(6, cc_acuse);
			ps.setString(7, sEstatus);

			//System.out.println(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				vd = new Vector();
				vd.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
				vd.add(rs.getString("IG_NUMERO_DOCTO"));
				vd.add(rs.getString("FECHA_DOCTO"));
				vd.add(rs.getString("FECHA_VENC"));
				vd.add(rs.getString("CD_NOMBRE"));
				vd.add(rs.getString("IC_MONEDA"));
				vd.add(rs.getString("FN_MONTO"));
				vd.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
				vd.add((rs.getString("PORC_ANTICIPO")==null?"":rs.getString("PORC_ANTICIPO")));
				vd.add((rs.getString("MONTO_DSCTO")==null?"":rs.getString("MONTO_DSCTO")));
				vd.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
				vd.add((rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF")));
				vd.add(rs.getString("IC_PYME"));
				vd.add(rs.getString("CD_DESCRIPCION"));
				vd.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
				vd.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
				vd.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
				vd.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
				vd.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
				vd.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
				vd.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
				vd.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
				vd.add(rs.getString("IC_ESTATUS_DOCTO"));
				vDoctos.add(vd);
				//System.out.println(vd);
			}
			rs.close();ps.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en mostrarDocumentosPreNegociables. "+e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDoctos;
	}	//fin-mostrarDocumentos


	/*********************************************************************************************
	*
	*	 	    boolean bactualizaEstatusPRENegociable()
	*
	*********************************************************************************************/
	public boolean bactualizaEstatusPreNegociable(String ic_epo, String cc_acuse,String ic_estatus, String usuario) throws NafinException {

		return bactualizaEstatusPreNegociable(ic_epo, cc_acuse,ic_estatus, "", usuario);
	}

	public boolean bactualizaEstatusPreNegociable(String ic_epo, String cc_acuse,String ic_estatus, String numDoctos, String usuario) throws NafinException {
	AccesoDB lodbConexion = new AccesoDB();
	boolean lbActualizo = true;
	String estatus="";
	String lsQry = "";
	String condicion= "";
	PreparedStatement ps = null;
	StringBuffer query = new StringBuffer();
	ResultSet rs = null;
	ArrayList datos = new ArrayList();
	String documento = "";

	if("28".equals(ic_estatus))
		estatus="2";
	if("29".equals(ic_estatus))
		estatus="1";
	if("28".equals(ic_estatus))
		condicion+=" AND ic_estatus_docto=28 ";
	if("29".equals(ic_estatus))
		condicion+=" AND ic_estatus_docto=29 ";
	if(!("").equals(numDoctos))
		condicion+="AND ig_numero_docto in ("+numDoctos+") ";

		try {
			  lodbConexion.conexionDB();


				//solo aplica para los documentos con estatus de Prenegociable
				//FODEA N@E 0005-2011
				log.debug(" numDoctos  "+numDoctos);

			if("28".equals(ic_estatus))	{
					if(!numDoctos.equals("")){	 //esto es cuando es individual
						query = new StringBuffer();
						query.append(" SELECT ic_documento   " );
						query.append(" FROM   com_documento " );
						query.append(" where  ig_numero_docto in ("+numDoctos+") ");

						ps = lodbConexion.queryPrecompilado(query.toString());
						rs = ps.executeQuery();
						while(rs.next()){
							datos.add(rs.getString("ic_documento")==null?"":rs.getString("ic_documento"));
						}
						rs.close();
						ps.close();

					} else{ //esto es cuando es masiva
						query = new StringBuffer();
						query.append(" SELECT ic_documento   " );
						query.append(" FROM   com_documento " );
						query.append(" where cc_acuse = ? " );
						query.append(" AND ic_epo= ? "+condicion);

						ps = lodbConexion.queryPrecompilado(query.toString());
						ps.setString(1, cc_acuse);
						ps.setInt(2, Integer.parseInt(ic_epo));

						rs = ps.executeQuery();
						while(rs.next()){
							datos.add(rs.getString("ic_documento")==null?"":rs.getString("ic_documento"));
						}
						rs.close();
						ps.close();

					}
					log.debug(" datos  "+datos);

					if(	datos.size()>0){
						for (int i=0; i < datos.size(); i++) {
							documento  = datos.get(i).toString();
							log.debug(" documento  "+documento);

							query = new StringBuffer();
							query.append("  INSERT INTO comhis_cambio_estatus  " );
							query.append("  (dc_fecha_cambio,  ic_cambio_estatus, ic_documento, cg_nombre_usuario) ");
							query.append("   values ( sysdate, ?, ?,? ) ");
							ps = lodbConexion.queryPrecompilado(query.toString());
							ps.setString(1, "40");
							ps.setString(2, documento);
							ps.setString(3, usuario);
							ps.executeUpdate();
							ps.close();
							log.debug(" Insert  "+query.toString());
						}	//termina for
					}//if(	datos.size()>0){
			}		//if("28".equals(ic_estatus))	{



					//cambia el estatus al documento
				   lsQry = " update com_documento " +
					" set ic_estatus_docto ="+estatus+
					" where cc_acuse = ? " +
					" AND ic_epo= ?"+ condicion;

			    ps = lodbConexion.queryPrecompilado(lsQry);
					ps.setString(1, cc_acuse);
					ps.setInt(2, Integer.parseInt(ic_epo));
					ps.executeUpdate();
					System.out.println(":::::strSQL UPDATE CAMBIO DE ESTATUS ::::::"+lsQry);


		} catch (Exception e) {
			e.printStackTrace();
			lbActualizo = false;
			System.out.println("Exception.bactualizaEstatusPreNegociable(): "+e);
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbActualizo);
			if (lodbConexion.hayConexionAbierta()) lodbConexion.cierraConexionDB();
		}
	return lbActualizo;
	}


	/*********************************************************************************************
	*
	*	 	    List mostrarDocumentosReasignados()
	*
	*********************************************************************************************/
	public List mostrarDocumentosReasignados(
			String sepo,
			String sno_electronico_prov,
			String sNoproveedor,
			String srfc,
			String snodoctos,
			String susuario_reasigno,
			String df_asigna_i,
			String df_asigna_f,
			String rutaDir) throws NafinException {
		AccesoDB con = new AccesoDB();
		List vDoctos = new ArrayList();
		List vd = null;
		log.info("mostrarDocumentosReasignados(E)");

		try{
			con.conexionDB();
			boolean	fileOK = false;
			int cont = 0;
			StringBuffer query = new StringBuffer();
			query.append( 	" SELECT /*+index(br)*/ "+
								" TO_CHAR (br.df_reasignacion, 'dd/mm/yyyy') AS dfreasignacion,"   +
								"        br.cg_razon_social_org AS cgnombre_org, br.cg_rfc_org AS rfc_org,"   +
								"        br.cg_calle_org AS calle_org, br.cg_colonia_org AS colonia_org,"   +
								"        br.cg_telefono1_org AS telefono_org, e.cg_razon_social AS nombre_epo,"   +
								"        br.cg_razon_social_des AS razon_social_des, br.cg_rfc_des AS rfc_des,"   +
								"        br.cg_calle_des AS calle_des, br.cg_colonia_des AS colonia_des,"   +
								"        br.cg_telefono1_des AS telefono_des, d.ig_numero_docto AS no_docto,"   +
								"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS fecha_doc,"   +
								"        TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_venci,"   +
								"        m.cd_nombre AS moneda,"   +
								//"        DECODE (d.cs_dscto_especial,'N', 'NORMAL','V', 'VENCIDA','DISTRIBUIDA') AS tipo_factoraje,"   +
								"			tf.cg_nombre AS tipo_factoraje, "+ // FODEA 042 - Agosto/2009
								"        d.fn_monto AS monto_docto, d.cg_campo1 AS cam_ad1,"   +
								"        d.cg_campo2 AS cam_ad2, d.cg_campo3 AS cam_ad3, d.cg_campo4 AS cam_ad4,"   +
								"        d.cg_campo5 AS cam_ad5, br.cg_causa AS causa,"   +
								"        br.bi_archivo_aut AS archivo, br.cg_extension as cg_extension "   +
								"   FROM bit_reasignacion_doctos br,"   +
								"        com_documento d,"   +
								"        comcat_epo e,"   +
								"        comrel_pyme_epo pe,"   +
								"        comcat_pyme p,"   +
								"        comrel_nafin na,"   +
								"        comcat_moneda m,"   +
								"			comcat_tipo_factoraje tf "+
								"  WHERE br.ic_documento = d.ic_documento"   +
								"    AND br.ic_epo = e.ic_epo"   +
								"    AND br.ic_epo = e.ic_epo"   +
								"    AND pe.ic_epo = e.ic_epo"   +
								"    AND br.ic_pyme_des = pe.ic_pyme"   +
								"    AND p.ic_pyme = br.ic_pyme_des"   +
								"    AND na.ic_epo_pyme_if = p.ic_pyme"   +
								"    AND na.cg_tipo = 'P'"   +
								"    AND p.ic_pyme = pe.ic_pyme"   +
								"    AND d.ic_moneda = m.ic_moneda" +
								"	  AND d.cs_dscto_especial = tf.cc_tipo_factoraje" );

			if (!sepo.equals("")) {
				query.append(" AND e.ic_epo = " + sepo);
			}
			if (!sno_electronico_prov.equals("")) {
				query.append(" AND na.IC_NAFIN_ELECTRONICO = " + sno_electronico_prov);
				//revisar que es cadena o integer

			}
			if (!sNoproveedor.equals("")) {
				query.append(" AND pe.CG_pyme_epo_interno = '" + sNoproveedor + "' ");

			}
			if (!srfc.equals("")) {
				query.append(" AND p.CG_RFC = '" + srfc + "' ");

			}
			if (!snodoctos.equals("")) {
				query.append(" AND d.IG_NUMERO_DOCTO = '" + snodoctos + "' ");

			}
			if (!susuario_reasigno.equals("")) {
				query.append(" AND br.ic_usuario = '" + susuario_reasigno + "' ");

			}
			if (!df_asigna_i.equals("") && !df_asigna_f.equals("")) {
				query.append("  AND TRUNC(br.df_reasignacion) BETWEEN (to_date('"+ df_asigna_i +"','dd/mm/yyyy')) AND (to_date('"+df_asigna_f+"','dd/mm/yyyy'))");

			}
			log.debug("::: query :::: "+query);
			ResultSet rs = con.queryDB(query.toString());
			while(rs.next()) {
				vd = new Vector();
				//cambiar todas estas :S
/*00*/			vd.add((rs.getString("dfreasignacion")==null?"?":rs.getString("dfreasignacion")));
/*01*/			vd.add((rs.getString("cgnombre_org")==null?"?":rs.getString("cgnombre_org")));
/*02*/			vd.add((rs.getString("rfc_org")==null?"":rs.getString("rfc_org")));
/*03*/			vd.add((rs.getString("calle_org")==null?"":rs.getString("calle_org")));
/*04*/			vd.add((rs.getString("colonia_org")==null?"":rs.getString("colonia_org")));
/*05*/			vd.add((rs.getString("telefono_org")==null?"":rs.getString("telefono_org")));
/*06*/			vd.add((rs.getString("nombre_epo")==null?"":rs.getString("nombre_epo")));
/*07*/			vd.add((rs.getString("razon_social_des")==null?"":rs.getString("razon_social_des")));
/*08*/			vd.add((rs.getString("rfc_des")==null?"":rs.getString("rfc_des")));
/*09*/			vd.add((rs.getString("calle_des")==null?"":rs.getString("calle_des")));
/*10*/			vd.add((rs.getString("colonia_des")==null?"":rs.getString("colonia_des")));
/*11*/			vd.add((rs.getString("telefono_des")==null?"":rs.getString("telefono_des")));
/*12*/			vd.add((rs.getString("no_docto")==null?"":rs.getString("no_docto")));
/*13*/			vd.add((rs.getString("fecha_doc")==null?"":rs.getString("fecha_doc")));
/*14*/			vd.add((rs.getString("fecha_venci")==null?"":rs.getString("fecha_venci")));
/*15*/			vd.add((rs.getString("moneda")==null?"":rs.getString("moneda")));
/*16*/			vd.add((rs.getString("tipo_factoraje")==null?"":rs.getString("tipo_factoraje")));
/*17*/			vd.add((rs.getString("monto_docto")==null?"":rs.getString("monto_docto")));
/*18*/			vd.add((rs.getString("cam_ad1")==null?"":rs.getString("cam_ad1")));
/*19*/			vd.add((rs.getString("cam_ad2")==null?"":rs.getString("cam_ad2")));
/*20*/			vd.add((rs.getString("cam_ad3")==null?"":rs.getString("cam_ad3")));
/*21*/			vd.add((rs.getString("cam_ad4")==null?"":rs.getString("cam_ad4")));
/*22*/			vd.add((rs.getString("cam_ad5")==null?"":rs.getString("cam_ad5")));
/*23*/			vd.add((rs.getString("causa")==null?"":rs.getString("causa")));
/*24			vd.add((rs.getString("cg_extension")==null?"":rs.getString("cg_extension")));  */
				String extension = (rs.getString("cg_extension")==null?"":rs.getString("cg_extension"));
				InputStream inputstream = rs.getBinaryStream("archivo");
				if(inputstream == null){
					log.info("Lo sentimos no hay archivo");
					/*24*/			vd.add("N");
					/*25*/			vd.add("mensajeNohay");
				}else if(!"".equals(extension)){
					cont +=1;
					String rutaArchivos = rutaDir;
					String x[]={"9","a","Z","8","b","Y","7","c","X","6","d","W","5","e","V","4","f","U","3","g","T","2","h","S","1","i","R","0","j","Q","9","k","P","8","l","O","7","m","N","6","n","M","5","o","L","4","p","K","3","q","J","2","r","I","1","s","H","9","t","G","8","u","F","7","v","E","6","w","D","5","x","C","4","y","B","3","z","A","2","a","Z","1","b","Y","0","c","X","9","d","W","8","e","V","7","f","U","6","g","T","5"};
					StringBuffer sb=new StringBuffer();
					for(int a=1; a<=8; a++) {
						int i=(int)((Math.random()*100)+1);
						sb.append(x[i-1]);
					}
					String nombreAr = sb.toString();
					String nom = nombreAr+cont+"."+extension;
			        String rutaGeneraArchivo                       = rutaArchivos+nom;
			        File file = new File(rutaGeneraArchivo);
			        fileOK = file.isFile();
					fileOK = file.exists();
					log.debug("rutaGeneraArchivo :: "+rutaGeneraArchivo);
			      log.debug("fileOK ::  "+fileOK);
					//if(fileOK){
						log.debug("fileOK ::  "+fileOK);
		  				   	byte arrByte[] = new byte[4096];
		                	OutputStream outstream = new BufferedOutputStream(new FileOutputStream(rutaGeneraArchivo));
		                 	int i = 0;
		                 	while((i = inputstream.read(arrByte, 0, arrByte.length)) != -1) {
		                             outstream.write(arrByte, 0, i);
		                 	}
						 	/*24*/			vd.add(nom);
							/*25*/			vd.add("cargarArchivo");
		                 	outstream.close();
					//}//if(fileOK){

				 } //else
				 else{
					/*24*/			vd.add("N");
					/*25*/			vd.add("mensajeNohay");
				 }
				vDoctos.add(vd);
			} //WHILE
			con.cierraStatement();
		} catch(Exception e) {
			log.info("Exception en mostrarDocumentosReasignados. "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	log.info("mostrarDocumentosReasignados(S)");
	return vDoctos;
	}

	/********************************************************************************
	 *metodo que devuelve el ic_reasignacion que se usara para insertar los datos en bitacora y
	 *actualizar el campo de archivo de autorizacioen en caso de existir.
	 *@author Salvador Saldivar Barajas
	 *@return cadena con el ic_reasignacion
	 *@throws NafinException Si no se puede obtener el ic_reasignacion de la BD
	 ******************************************************************************/
	private String generarICreasignacion(AccesoDB con)throws NafinException{
		System.out.println("generarICreasignacion(E)");
		String qry = "Select SEQ_BIT_REASIGNACION_DOCTOS.NEXTVAL as ic from dual";
		String regreso = "";
		ResultSet rs = null;
		try{
			rs = con.queryDB(qry);
			if(rs.next())
				regreso = rs.getString("ic");
			rs.close();
			con.cierraStatement();
		}catch(Exception e){
			System.out.println("Error::: "+e);
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("generarICreasignacion(S)");
			//la conexion no se cierra, hasta terminar de capturar todo
			/*if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}*/
		}
		return regreso;
	}
	/********************************************************************************
	 *metodo que guarda en bitacora los datos de la reasignacion de documentos negociables
	 * y actualiza el ic_pyme en com_documento
	 *@author Salvador Saldivar Barajas
	 *@param Lista que contiene los datos a guardar
	 *@return void
	 *@throws NafinException Si no se puede insertar en BD
	 ******************************************************************************/
	private void guardarBitacora(List datos, AccesoDB con)throws NafinException{
		System.out.println("GuardarBitacora(E)");
		String qry = "";
		boolean ok = true;
		//AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		try{
			//con.conexionDB();
			qry = "UPDATE com_documento SET ic_pyme = ?, cc_acuse = ? where ic_documento = ?";
			ps = con.queryPrecompilado(qry);
			ps.setInt(1,Integer.parseInt(datos.get(2).toString()));//ic_pyme_d
			ps.setString(2,datos.get(19).toString());//acuse
			ps.setInt(3,Integer.parseInt(datos.get(3).toString()));//ic_documento
			ps.executeUpdate();
			ps.close();
			System.out.println("UPDATE COM_DOCUMENTO OK");
			qry = "INSERT INTO BIT_REASIGNACION_DOCTOS " +
			"(ic_reasignacion, ic_epo, ic_documento, ic_pyme_org, cg_razon_social_org,cg_rfc_org, cg_calle_org, cg_colonia_org, cg_telefono1_org, " +
			" ic_pyme_des, cg_razon_social_des, cg_rfc_des, cg_calle_des, cg_colonia_des, cg_telefono1_des, ic_usuario, df_reasignacion, cg_causa, " +
			" bi_archivo_aut, cg_extension)";
			qry +="VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?)";
			ps = con.queryPrecompilado(qry);
			ps.setInt(1,Integer.parseInt(datos.get(17).toString()));//ic_reasignacion
			ps.setInt(2,Integer.parseInt(datos.get(0).toString()));//ic_epo
			ps.setInt(3,Integer.parseInt(datos.get(3).toString()));//ic_documento
			ps.setInt(4,Integer.parseInt(datos.get(1).toString()));//ic_pyme_o
			ps.setString(5,datos.get(6).toString());//cg_razon_social_o
			ps.setString(6,datos.get(8).toString());//cg_rfc_o
			ps.setString(7,datos.get(10).toString());//cg_calle_o
			ps.setString(8,datos.get(12).toString());//cg_colonia_o
			ps.setString(9,datos.get(14).toString());//cg_telefono1_o
			ps.setInt(10,Integer.parseInt(datos.get(2).toString()));//ic_pyme_d
			ps.setString(11,datos.get(7).toString());//cg_razon_social_d
			ps.setString(12,datos.get(9).toString());//cg_rfc_d
			ps.setString(13,datos.get(11).toString());//cg_calle_d
			ps.setString(14,datos.get(13).toString());//cg_colonia_d
			ps.setString(15,datos.get(15).toString());//cg_telefono1_d
			ps.setString(16,datos.get(16).toString());//ic_usuario
			ps.setString(17,datos.get(4).toString());//cg_causa
			ps.setString(18,"0");//archivo autorizacion
			ps.setString(19,datos.get(18).toString());//cg_extension
			ps.executeUpdate();
			ps.close();
			System.out.println("INSERT BIT_REASIGNACION_DOCTOS OK");
		}catch(Exception e){
			ok = false;
			System.out.println("Error::: "+e);
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("GuardarBitacora(S)");
			//la conexion no se cierra, hasta terminar de capturar todo
			/*if (con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}*/
		}
	}
	/********************************************************************************
	 *metodo que actualiza bitacora, guardando el archivo de autorizacion para la
	 *reasignacion de documentos negociables
	 *@author Salvador Saldivar Barajas
	 *@param Lista que contiene los datos a guardar
	 *@return void
	 *@throws NafinException Si no se puede actualizar en BD
	******************************************************************************/
	private void actualizarArchivoBitacora(List datos, AccesoDB con)throws NafinException{
		System.out.println("actualizarArchivoBitacora(E)");
		String qry = "UPDATE bit_reasignacion_doctos SET bi_archivo_aut=? WHERE ic_reasignacion = ?";
		boolean ok = true;
		PreparedStatement ps = null;
		try{
			File archivo = new File(datos.get(5).toString());
			FileInputStream fileinputstream = new FileInputStream(archivo);

			ps = con.queryPrecompilado(qry);
			ps.setBinaryStream(1, fileinputstream, (int)archivo.length());
			ps.setInt(2,Integer.parseInt(datos.get(17).toString()));//ic_reasignacion
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("actualizarArchivoBitacora(S)");
			//la conexion no se cierra, hasta terminar de capturar todo
			/*if (con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}*/
		}
	}
	/********************************************************************************
	 *metodo que guarda el acuse de los documentos que se reasignaron
	 *@author Salvador Saldivar Barajas
	 *@param datos Lista que contiene los datos a guardar del acuse
	 *@param lDatos Lista que contiene los datos a guardar en bitacora
	 *@return void
	 *@throws NafinException Si no se puede actualizar en BD
	******************************************************************************/
	public void guardarAcuseReasignacion(List datos, List lDatos)throws NafinException{
		System.out.println("guardarAcuseReasignacion(E)");
		String qry = "";
		String docto = "";
		boolean ok = true;
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringTokenizer std = null;
		List lDoctos = new ArrayList();
		int inoarchivos = 0;
		try{
			con.conexionDB();
			qry = "INSERT INTO COM_ACUSE1 " +
			"(cc_acuse, in_total_docto_mn, fn_total_monto_mn, in_total_docto_dl, fn_total_monto_dl, df_fechahora_carga, " +
			" ic_usuario, ic_producto_nafin) " +
			"VALUES(?,?,?,?,?,sysdate,?,?)";
			ps = con.queryPrecompilado(qry);
			ps.setString(1,datos.get(0).toString());//cc_acuse
			ps.setInt(2,Integer.parseInt(datos.get(1).toString()));//total_doctos_mn
			ps.setDouble(3,Double.parseDouble(datos.get(2).toString()));//total_monto_mn
			ps.setInt(4,Integer.parseInt(datos.get(3).toString()));//total_docots_dl
			ps.setDouble(5,Double.parseDouble(datos.get(4).toString()));//total_monto_dl
			ps.setString(6,datos.get(5).toString());//ic_usuario
			ps.setInt(7,Integer.parseInt(datos.get(6).toString()));//ic_producto_nafin
			ps.executeUpdate();
			ps.close();
			System.out.println("INSERT COM_ACUSE1 OK");
			std = new StringTokenizer(lDatos.get(3).toString(),",");
			while(std.hasMoreTokens()){
				lDoctos.add(std.nextToken().trim());
			}
			for(int a=0;a<lDoctos.size();a++){
				docto = lDoctos.get(a).toString();
				System.out.println("DOCTO:::: "+docto);
				lDatos.set(3,docto);
				String icReasignacion = generarICreasignacion(con);
				System.out.println("ICREASIGNACION:::: "+icReasignacion);
				lDatos.set(17,icReasignacion);//17
				inoarchivos = Integer.parseInt(lDatos.get(20).toString());
				System.out.println("INOARCHIVOS:::: "+inoarchivos);
				guardarBitacora(lDatos, con);
				if(inoarchivos>0){
					actualizarArchivoBitacora(lDatos, con);
				}
			}

		}catch(Exception e){
			ok = false;
			System.out.println("Error::: "+e);
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("guardarAcuseReasignacion(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

	/*********************************************************************************************
	*
	*	Este m�todo verifica si la epo maneja parametrizaci�n EPO-PEF.
	*
	*********************************************************************************************/
	public boolean tieneParamEpoPef(String icEpo) throws NafinException {
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List variablesBind = new ArrayList();
		PreparedStatement pst = null;
		ResultSet rst = null;
		boolean paramEpoPef = false;

		try{
			con.conexionDB();

			strSQL.append(" SELECT cg_valor");
			strSQL.append(" FROM com_parametrizacion_epo");
			strSQL.append(" WHERE ic_epo = ?");
			strSQL.append(" AND cc_parametro_epo = ?");

			variablesBind.add(new Long(icEpo));
			variablesBind.add("PUB_EPO_PEF_FECHA_RECEPCION");

			pst = con.queryPrecompilado(strSQL.toString(), variablesBind);

			rst = pst.executeQuery();

			String valor = "";

			while(rst.next()){
				valor = rst.getString(1)==null?"":rst.getString(1);
			}

			rst.close();
			pst.close();

			if(!valor.equals("") && valor.equals("S")){
				paramEpoPef = true;
			}else{
				paramEpoPef = false;
			}

			return paramEpoPef;
		}catch(Exception ne){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/*********************************************************************************************
	*	Este m�todo permite obtener la fecha de entrega, el tipo de compra, la clave presupuestaria y el periodo
	* para la epo. Siempre y cuando la epo maneje publicaci�n EPO-PEF.
	*
	*********************************************************************************************/
	public List getDoctosMantFechaRecepcion(String icDocumento, String icEpo, String icPyme) throws NafinException {
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List variablesBind = new ArrayList();
		PreparedStatement pst = null;
		ResultSet rst = null;
		List valoresEpoPef = new ArrayList();

		try{
			con.conexionDB();

			strSQL.append(" SELECT TO_CHAR(doc.df_entrega, 'DD/MM/YYYY'), com.cg_descripcion, doc.cg_clave_presupuestaria, doc.cg_periodo");
			strSQL.append(" FROM com_documento doc, comcat_tipo_compra com");
			strSQL.append(" WHERE doc.cg_tipo_compra = com.cc_clave");
			strSQL.append(" AND doc.ic_documento = ?");
			strSQL.append(" AND ic_epo = ?");
			strSQL.append(" AND ic_pyme = ?");

			variablesBind.add(new Long(icDocumento));
			variablesBind.add(new Long(icEpo));
			variablesBind.add(new Long(icPyme));

			pst = con.queryPrecompilado(strSQL.toString(), variablesBind);

			rst = pst.executeQuery();

			while(rst.next()){
				valoresEpoPef.add(rst.getString(1)==null?"":rst.getString(1));
				valoresEpoPef.add(rst.getString(2)==null?"":rst.getString(2));
				valoresEpoPef.add(rst.getString(3)==null?"":rst.getString(3));
				valoresEpoPef.add(rst.getString(4)==null?"":rst.getString(4));
			}

			rst.close();
			pst.close();

			return valoresEpoPef;
		}catch(Exception ne){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
//FODEA 015 - 2009 ACF (I)
  /**
   * Este m�todo obtiene el login y el nombre del usuario que modifico el estatus del documento,
   * la informaci�n se extrae de la tabla comhis_cambio_estatus.
   * @param icDocumento la clave del documento que modifico su estatus
   * @return login_y_usuario con el login y el usuario que modifico el estatus del documento.
   * @throws NafinException cuando ocurre un error en la consulta.
   */
  public String getUsuarioCambioEstatus(String icDocumento) throws NafinException{
    System.out.println("..:: com.netro.descuento.CMantenimientoBean.getUsuarioCambioEstatus(E)");
    AccesoDB con = new AccesoDB();
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    PreparedStatement pst = null;
    ResultSet rst = null;
    String login_y_usuario = "";
    try{
      con.conexionDB();
      strSQL.append("SELECT cg_nombre_usuario FROM comhis_cambio_estatus WHERE ic_documento = ?");
      varBind.add(new Long(icDocumento));
      //System.out.println("..:: strSQL : "+strSQL.toString());
      //System.out.println("..:: varBInd : "+varBind);
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();
      while(rst.next()){
        login_y_usuario = rst.getString(1)==null?"":rst.getString(1);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      System.out.println(" ERROR : "+e.getMessage());
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    System.out.println("..:: com.netro.descuento.CMantenimientoBean.getUsuarioCambioEstatus(S)");
    return login_y_usuario;
  }
//FODEA 015 - 2009 ACF (F)

//FODEA 053-2009 FVR
	/**
		 * Obtiene el acuse de documentos pendientes
		 * @throws com.netro.exception.NafinException
		 * @return
		 * @param ic_estatus - estatus del documento
		 * @param ic_usuario - clave del usuario
		 * @param fechaCargaFin - fecha final en la que se realizo la carga
		 * @param fechaCargaIni - fecha inicial en la que se realizo la carga
		 * @param cc_acuse - acuse generado al realizar la carga de los documentos
		 * @param ic_epo - clave de la epo
		 */
	public Vector ovgetPendientes(String ic_epo, String cc_acuse, String fechaCargaIni, String fechaCargaFin, String ic_usuario, String ic_estatus, String numDoctos) throws NafinException {
		Vector lovDatos = null;
		String condicion= "";
		int cont=0;
		Vector lovPrenegociables = new Vector();
		AccesoDB lobdConexion = new AccesoDB();

		try{
			lobdConexion.conexionDB();

			if(!"".equals(cc_acuse))
			condicion+="AND a.cc_acuse	= ? ";
			if(!"".equals(ic_usuario))
			condicion+="AND a.ic_usuario= ? ";
			if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin))
				condicion+=" AND a.df_fechahora_carga >= TO_DATE(?,'dd/mm/yyyy') AND a.df_fechahora_carga< TO_DATE(?,'dd/mm/yyyy') + 1 ";
			if(!"".equals(ic_estatus))
				condicion+="AND d.ic_estatus_docto= ? ";
			if(!("").equals(numDoctos))
				condicion+="AND d.ig_numero_docto in ("+numDoctos+") ";


			String lsQry =  " SELECT a.cc_acuse, "+
							" TO_CHAR (a.df_fechahora_carga, 'DD/MM/YYYY') fecha_carga, a.ic_usuario, "+
							" a.in_total_docto_mn,a.fn_total_monto_mn,a.in_total_docto_dl,a.fn_total_monto_dl,a.cg_hash"+
							" FROM com_documento d, com_acuse1 a "+
							" WHERE d.cc_acuse=a.cc_acuse " +
							" AND d.ic_estatus_docto in (30,31) " +
							" AND d.ic_epo=? "+ condicion +
							" GROUP BY a.cc_acuse, TO_CHAR (a.df_fechahora_carga, 'DD/MM/YYYY') , "+
							" a.ic_usuario, a.in_total_docto_mn,a.fn_total_monto_mn,a.in_total_docto_dl,a.fn_total_monto_dl,a.cg_hash "+
							" ORDER BY a.cc_acuse, fecha_carga";

			//System.out.println(":::::QUERY::::::"+lsQry);
			//try {
				PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);

				cont++;ps.setString(cont, ic_epo);

				if(!"".equals(cc_acuse)){
					cont++;ps.setString(cont, cc_acuse);
				}
				if(!"".equals(ic_usuario)){
					cont++; ps.setString(cont, ic_usuario);
				}
				if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin)){
					cont++; ps.setString(cont, fechaCargaIni);
					cont++; ps.setString(cont, fechaCargaFin);
				}
				if(!"".equals(ic_estatus)){
					cont++; ps.setString(cont, ic_estatus);
				}

				ResultSet lrsQry = ps.executeQuery();

				while(lrsQry.next()) {
					lovDatos = new Vector();
					/*00*/lovDatos.add((lrsQry.getString("cc_acuse")==null?"":lrsQry.getString("cc_acuse").trim()));
					/*01*/lovDatos.add((lrsQry.getString("fecha_carga")==null?"":lrsQry.getString("fecha_carga").trim()));
					/*02*/lovDatos.add((lrsQry.getString("ic_usuario")==null?"":lrsQry.getString("ic_usuario").trim()));
					/*03*/lovDatos.add((lrsQry.getString("in_total_docto_mn")==null?"":lrsQry.getString("in_total_docto_mn").trim()));
					/*04*/lovDatos.add((lrsQry.getString("fn_total_monto_mn")==null?"":lrsQry.getString("fn_total_monto_mn").trim()));
					/*05*/lovDatos.add((lrsQry.getString("in_total_docto_dl")==null?"":lrsQry.getString("in_total_docto_dl").trim()));
					/*06*/lovDatos.add((lrsQry.getString("fn_total_monto_dl")==null?"":lrsQry.getString("fn_total_monto_dl").trim()));
					/*07*/lovDatos.add((lrsQry.getString("cg_hash")==null?"":lrsQry.getString("cg_hash").trim()));
					//System.out.println(":::::LOVDATOS::::"+lovDatos);
					lovPrenegociables.addElement(lovDatos);
				}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception en ovgetPendientes(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lovPrenegociables;
	} // Fin Metodo ovgetPendientes

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param cc_acuse
	 * @param ic_epo
	 */
	public ArrayList ovgetDetallePendientes(String ic_epo, String cc_acuse, String numDoctos) throws NafinException {
		ArrayList lstDatos = null;
		String condicion= "";
		int cont=0;
		ArrayList lstPendientes = new ArrayList();
		AccesoDB lobdConexion = new AccesoDB();

		try{
			lobdConexion.conexionDB();

			if(!("").equals(numDoctos))
				condicion +="AND d.ig_numero_docto in ("+numDoctos+") ";

			String lsQry =  "SELECT   p.cg_razon_social nompyme, d.ig_numero_docto numdocto, " +
								"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') fecemision, " +
								"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') fecvenc, " +
								"         m.ic_moneda cvemoneda, d.fn_monto monto, f.cg_nombre nomfactoraje, " +
								"         a.cc_acuse acuse, d.ic_estatus_docto estatus, a.cg_hash hash " +
								"    FROM com_documento d, " +
								"         com_acuse1 a, " +
								"         comcat_pyme p, " +
								"         comcat_tipo_factoraje f, " +
								"         comcat_moneda m " +
								"   WHERE d.cc_acuse = a.cc_acuse " +
								"     AND d.ic_pyme = p.ic_pyme " +
								"     AND d.cs_dscto_especial = f.cc_tipo_factoraje " +
								"     AND d.ic_moneda = m.ic_moneda " +
								"     AND d.ic_estatus_docto IN (30, 31) " +
								"     AND d.ic_epo = ? " +
								"     AND a.cc_acuse = ? " +
								condicion+
								"ORDER BY d.df_fecha_docto ";


			//System.out.println(":::::QUERY::::::"+lsQry);
			//try {
				PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);

				cont++;ps.setString(cont, ic_epo);
				cont++;ps.setString(cont, cc_acuse);


				ResultSet lrsQry = ps.executeQuery();

				while(lrsQry.next()) {
					lstDatos = new ArrayList();
					/*00*/lstDatos.add((lrsQry.getString("nompyme")==null?"":lrsQry.getString("nompyme")));
					/*01*/lstDatos.add((lrsQry.getString("numdocto")==null?"":lrsQry.getString("numdocto").trim()));
					/*02*/lstDatos.add((lrsQry.getString("fecemision")==null?"":lrsQry.getString("fecemision").trim()));
					/*03*/lstDatos.add((lrsQry.getString("fecvenc")==null?"":lrsQry.getString("fecvenc").trim()));
					/*04*/lstDatos.add((lrsQry.getString("cvemoneda")==null?"":lrsQry.getString("cvemoneda").trim()));
					/*05*/lstDatos.add((lrsQry.getString("monto")==null?"":lrsQry.getString("monto").trim()));
					/*06*/lstDatos.add((lrsQry.getString("nomfactoraje")==null?"":lrsQry.getString("nomfactoraje").trim()));
					/*07*/lstDatos.add((lrsQry.getString("acuse")==null?"":lrsQry.getString("acuse").trim()));
					/*07*/lstDatos.add((lrsQry.getString("estatus")==null?"":lrsQry.getString("estatus").trim()));
					/*08*/lstDatos.add((lrsQry.getString("hash")==null?"":lrsQry.getString("hash").trim()));
					//System.out.println(":::::LOVDATOS::::"+lovDatos);
					lstPendientes.add(lstDatos);
				}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception en ovgetPendientes(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lstPendientes;
	} // Fin Metodo ovgetPendientes

	/**
	 * Elimina documentos con estatus pendientes asociados a un acuse
	 * @throws com.netro.exception.NafinException
	 * @param cc_acuse - identificador aignao a una carga en especifico
	 * @param ic_epo - clave de la cadena que realizo la carga
	 */
	public void eliminaPendientes(String ic_epo, String cc_acuse) throws NafinException{
		AccesoDB con = null;
		boolean exito = true;
		String strSQL ="";
		String ic_docto="";
		List varBind = new ArrayList();
		StringBuffer strQuery = new StringBuffer();
		ResultSet rs = null;
		boolean existenDoctos = false;
		System.out.println("CMantenimientoBean::eliminaPendientes(E)");
		try {
		 	con = new AccesoDB();
			con.conexionDB();

			strSQL = " SELECT	ic_documento	FROM	com_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? "+
						" AND ic_estatus_docto IN (?, ?) ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			ps.setInt(3, 30);
			ps.setInt(4, 31);
			rs = ps.executeQuery();
			if(rs!=null){
				int i=0;
				while(rs.next()){
					ic_docto = rs.getString("ic_documento");
					if(i==0){
						strQuery.append(" DELETE FROM com_documento_detalle" +
											" WHERE ic_documento IN (?");
					}else{
						strQuery.append(",?");
					}
					varBind.add(new Long(ic_docto));
					i++;
				}
				strQuery.append(" ) ");
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

			if (varBind.size() > 0){
				ps = con.queryPrecompilado(strQuery.toString(), varBind);
				ps.executeUpdate();
				if(ps!=null)ps.close();
			}

			strSQL = " DELETE FROM com_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? " +
						" AND ic_estatus_docto in (?,?)";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			ps.setInt(3, 30);
			ps.setInt(4, 31);
			ps.executeUpdate();
			if(ps!=null)ps.close();

			//valida si aun existe documentos para el acuse en cuestion
			strSQL = " select count(1) numDocs from  com_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				if(!"0".equals(rs.getString("numDocs"))){
					existenDoctos = true;
				}
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

			System.out.println(":::::strSQL 2 ::::::"+strSQL);

			if(!existenDoctos){
				strSQL = " DELETE FROM com_acuse1" +
							" WHERE cc_acuse = ?" ;

				ps = con.queryPrecompilado(strSQL);
				ps.setString(1, cc_acuse);
				ps.executeUpdate();

				System.out.println(":::::strSQL 3 ::::::"+strSQL);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			exito = false;
			System.out.println("MantenimientoBean::eliminaPendientes Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoBean::eliminaPendientes (S)");
		}
	}//fin del metodo eliminaPrenegociables()

	/**
	 * Se autorizan los documentos con estsus pendientes y pasan a pre-negociables
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param numDoctos - contiene valores de los documentos a autorizar
	 * @param ic_estatus - se indica el estatus de documento a considerar
	 * @param cc_acuse - identificador aignao a una carga en especifico
	 * @param ic_epo - clave de la cadena que realizo la carga
	 */
	public boolean bactualizaEstatusPendiente(String ic_epo, String cc_acuse,String ic_estatus, String numDoctos) throws NafinException {
		AccesoDB lodbConexion = new AccesoDB();
		boolean lbActualizo = true;
		String estatus="";
		String lsQry = "";
		StringBuffer condicion= new StringBuffer();

		if("30".equals(ic_estatus))
			estatus="28";
		if("31".equals(ic_estatus))
			estatus="29";
		if("30".equals(ic_estatus))
			condicion.append(" AND ic_estatus_docto=30 ");
		if("31".equals(ic_estatus))
			condicion.append(" AND ic_estatus_docto=31 ");
		if(!"".equals(numDoctos))
			condicion.append(" AND ig_numero_docto in ( "+numDoctos+")");
			try {
				lodbConexion.conexionDB();
				lsQry =	" update com_documento " +
							" set ic_estatus_docto ="+estatus+
							" where cc_acuse = ? " +
							" AND ic_epo= ? "+ condicion.toString();


				PreparedStatement ps = lodbConexion.queryPrecompilado(lsQry);
						ps.setString(1, cc_acuse);
						ps.setInt(2, Integer.parseInt(ic_epo));
						ps.executeUpdate();
				System.out.println(":::::strSQL UPDATE CAMBIO DE ESTATUS ::::::"+lsQry);

			} catch (Exception e) {
				e.printStackTrace();
				lbActualizo = false;
				System.out.println("Exception.bactualizaEstatusPendiente(): "+e);
				throw new NafinException("SIST0001");
			} finally {
				lodbConexion.terminaTransaccion(lbActualizo);
				if (lodbConexion.hayConexionAbierta()) lodbConexion.cierraConexionDB();
			}
		return lbActualizo;
	}


	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param numDoctos
	 * @param cc_acuse
	 * @param sAforoDL
	 * @param sEstatus
	 * @param sNoCliente
	 * @param sAforo
	 */
	public Vector mostrarDocumentosPendientes(
			String sAforo,
			String sNoCliente,
			String sEstatus,
			String sAforoDL, String cc_acuse, String numDoctos) throws NafinException {
		AccesoDB con = new AccesoDB();
		Vector vDoctos = new Vector();
		Vector vd = null;

		try{
			con.conexionDB();


			StringBuffer query = new StringBuffer();
			query.append(	" SELECT P.cg_razon_social, CD.ig_numero_docto, "+
				" TO_CHAR(CD.df_fecha_docto,'DD/MM/YYYY') as fecha_docto, "+
				" TO_CHAR(CD.df_fecha_venc,'DD/MM/YYYY') as fecha_venc, "+
				" M.cd_nombre AS CD_NOMBRE, "+
				" M.ic_moneda, "+
	            " CD.fn_monto, "+
				" CD.cs_dscto_especial, "+
				" 100*DECODE (CD.ic_moneda, 1, ?, 54, ?)  as porc_anticipo, "+
				" CD.fn_monto*DECODE (CD.ic_moneda, 1, ?, 54, ?)  as monto_dscto, "+
				" CD.ct_referencia,"+
				" I.cg_razon_social as nombre_IF, "+
	            " P.ic_pyme, "+
				" ED.cd_descripcion as cd_descripcion, "+
	            " CD.cg_campo1, CD.cg_campo2, "+
				" CD.cg_campo3, CD.cg_campo4, CD.cg_campo5, "+
				" B.cg_razon_social as nombre_beneficiario, "+
	            " CD.fn_porc_beneficiario, "+
				" (CD.fn_porc_beneficiario/100)*CD.fn_monto as monto_beneficiario, "+
				" CD.ic_estatus_docto "+
				" FROM  com_documento CD, comcat_estatus_docto ED, "+
				" comcat_pyme P, comcat_moneda M, comcat_if I, comcat_if B, com_acuse1 ca "+
				" WHERE CD.ic_pyme = P.ic_pyme "+
				" AND CD.ic_moneda = M.ic_moneda "+
				" AND CD.ic_estatus_docto = ED.ic_estatus_docto "+
				" AND CD.ic_if = I.ic_if(+) "+
				" AND CD.ic_beneficiario = B.ic_if(+) "+
	         " AND CD.cc_acuse=ca.cc_acuse "+
				" AND CD.ic_epo = ? "+
	         " AND ca.cc_acuse=? "+
	         " AND CD.ic_estatus_docto = ? ");
			if(!("").equals(numDoctos))
				query.append(" AND CD.ig_numero_docto in ( "+numDoctos+") ");

			PreparedStatement ps = con.queryPrecompilado(query.toString());
			ps.setString(1, sAforo);
			ps.setString(2, sAforoDL);
			ps.setString(3, sAforo);
			ps.setString(4, sAforoDL);
			ps.setString(5, sNoCliente);
			ps.setString(6, cc_acuse);
			ps.setString(7, sEstatus);

			//System.out.println(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				vd = new Vector();
				vd.add((rs.getString("CG_RAZON_SOCIAL")==null?"?":rs.getString("CG_RAZON_SOCIAL")));
				vd.add(rs.getString("IG_NUMERO_DOCTO"));
				vd.add(rs.getString("FECHA_DOCTO"));
				vd.add(rs.getString("FECHA_VENC"));
				vd.add(rs.getString("CD_NOMBRE"));
				vd.add(rs.getString("IC_MONEDA"));
				vd.add(rs.getString("FN_MONTO"));
				vd.add((rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));
				vd.add((rs.getString("PORC_ANTICIPO")==null?"":rs.getString("PORC_ANTICIPO")));
				vd.add((rs.getString("MONTO_DSCTO")==null?"":rs.getString("MONTO_DSCTO")));
				vd.add((rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA")));
				vd.add((rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF")));
				vd.add(rs.getString("IC_PYME"));
				vd.add(rs.getString("CD_DESCRIPCION"));
				vd.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));
				vd.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));
				vd.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));
				vd.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));
				vd.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));
				vd.add((rs.getString("NOMBRE_BENEFICIARIO")==null?"":rs.getString("NOMBRE_BENEFICIARIO")));
				vd.add((rs.getString("FN_PORC_BENEFICIARIO")==null?"":rs.getString("FN_PORC_BENEFICIARIO")));
				vd.add((rs.getDouble("MONTO_BENEFICIARIO")==0?"":rs.getString("MONTO_BENEFICIARIO")));
				vd.add(rs.getString("IC_ESTATUS_DOCTO"));
				vDoctos.add(vd);
				//System.out.println(vd);
			}
			rs.close();ps.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en mostrarDocumentosPendientes. "+e);
			throw new NafinException("SIST0001");
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return vDoctos;
	}	//fin-mostrarDocumentos


	public ArrayList ovgetDetallePreNegociables(String ic_epo, String cc_acuse, String numDoctos) throws NafinException {
		ArrayList lstDatos = null;
		String condicion= "";
		int cont=0;
		ArrayList lstPendientes = new ArrayList();
		AccesoDB lobdConexion = new AccesoDB();

		try{
			lobdConexion.conexionDB();

			if(!("").equals(numDoctos))
				condicion +="AND d.ig_numero_docto in ("+numDoctos+") ";

			String lsQry =  "SELECT   p.cg_razon_social nompyme, d.ig_numero_docto numdocto, " +
								"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') fecemision, " +
								"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') fecvenc, " +
								"         m.ic_moneda cvemoneda, d.fn_monto monto, f.cg_nombre nomfactoraje, " +
								"         a.cc_acuse acuse, d.ic_estatus_docto estatus " +
								"    FROM com_documento d, " +
								"         com_acuse1 a, " +
								"         comcat_pyme p, " +
								"         comcat_tipo_factoraje f, " +
								"         comcat_moneda m " +
								"   WHERE d.cc_acuse = a.cc_acuse " +
								"     AND d.ic_pyme = p.ic_pyme " +
								"     AND d.cs_dscto_especial = f.cc_tipo_factoraje " +
								"     AND d.ic_moneda = m.ic_moneda " +
								"     AND d.ic_estatus_docto IN (28, 29) " +
								"     AND d.ic_epo = ? " +
								"     AND a.cc_acuse = ? " +condicion+
								"ORDER BY d.df_fecha_docto ";


			//System.out.println(":::::QUERY::::::"+lsQry);
			//try {
				PreparedStatement ps = lobdConexion.queryPrecompilado(lsQry);

				cont++;ps.setString(cont, ic_epo);
				cont++;ps.setString(cont, cc_acuse);


				ResultSet lrsQry = ps.executeQuery();

				while(lrsQry.next()) {
					lstDatos = new ArrayList();
					/*00*/lstDatos.add((lrsQry.getString("nompyme")==null?"":lrsQry.getString("nompyme")));
					/*01*/lstDatos.add((lrsQry.getString("numdocto")==null?"":lrsQry.getString("numdocto").trim()));
					/*02*/lstDatos.add((lrsQry.getString("fecemision")==null?"":lrsQry.getString("fecemision").trim()));
					/*03*/lstDatos.add((lrsQry.getString("fecvenc")==null?"":lrsQry.getString("fecvenc").trim()));
					/*04*/lstDatos.add((lrsQry.getString("cvemoneda")==null?"":lrsQry.getString("cvemoneda").trim()));
					/*05*/lstDatos.add((lrsQry.getString("monto")==null?"":lrsQry.getString("monto").trim()));
					/*06*/lstDatos.add((lrsQry.getString("nomfactoraje")==null?"":lrsQry.getString("nomfactoraje").trim()));
					/*07*/lstDatos.add((lrsQry.getString("acuse")==null?"":lrsQry.getString("acuse").trim()));
					/*07*/lstDatos.add((lrsQry.getString("estatus")==null?"":lrsQry.getString("estatus").trim()));
					//System.out.println(":::::LOVDATOS::::"+lovDatos);
					lstPendientes.add(lstDatos);
				}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception en ovgetPendientes(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lstPendientes;
	} // Fin Metodo ovgetPendientes

	/**
	 *	Este metodo sirve para consultar si la EPO, cuyo ID se proporciona en el parametro <tt>ic_epo</tt>,
	 * Opera Notas de Credito para el Producto: Descuento Electronico.
	 * @Fodea 002 - 2010
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si opera notas de credito. <tt>false</tt> en caso contrario.
	 */
	private boolean operaNotasDeCreditoParaDescuentoElectronico(String ic_epo) throws AppException{
		log.info("operaNotasDeCreditoParaDescuentoElectronico(E)");
		boolean resultado = false;
		try {
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			resultado = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(ic_epo);

		}catch(Exception e){
			log.error("operaNotasDeCreditoParaDescuentoElectronico(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar parametro de la EPO.");
		}finally{
			log.info("operaNotasDeCreditoParaDescuentoElectronico(S)");
		}
		return resultado;
	}

	/**
	 *	Este metodo sirve para consultar si la EPO, cuyo ID se proporciona en el parametro <tt>ic_epo</tt>,
	 * tiene habilitada la Opcion de Aplicar Notas de Credito a Varios Documentos.
	 * @Fodea 002 - 2010
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitada. <tt>false</tt> en caso contrario.
	 */
	private boolean hasAplicarNotaDeCreditoAVariosDoctosEnabled(String ic_epo) throws AppException{
		log.info("hasAplicarNotaDeCreditoAVariosDoctosEnabled(E)");
		boolean resultado = false;
		try {
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			resultado = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(ic_epo);

		}catch(Exception e){
			log.error("hasAplicarNotaDeCreditoAVariosDoctosEnabled(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar parametro de la EPO.");
		}finally{
			log.info("hasAplicarNotaDeCreditoAVariosDoctosEnabled(S)");
		}
		return resultado;
	}

	/**
	 * Actualiza la lista de documentos que se convertiran en Negociables, incluyendo aquellos documentos
	 * que no fueron seleccionados para cambiarlos a estatus Negociable, pero que comparten la misma nota de
	 * credito. Solo para usarse si la EPO tiene habilitada las Notas de Credito a Multiples Documentos.
	 *
	 *  @param doctosSeleccionados Arreglo con los Id de los documentos consultados; ver:
	 *                             nafin-web/13descuento/13pki/13if/13forma7b.jsp
	 *  @param estatusDoctos Arreglo en el que se indican con una "N" los documentos que han sido seleccionados
	 *                       para cambiar a estatus Negociable. Una cadena vacia indica que el documento
	 *                       no ha sido seleccionado
	 * @return ArrayList con la lista de documentos actualizada y el estatus de los documentos actualizados, es
	 *         decir aquellos que tambien deben cambiar estatus a Negociable.
	 */
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[],String estatusDoctos[])
		throws AppException{

		return actualizaDoctosSeleccionados(doctosSeleccionados,estatusDoctos,"N");

	}

	/**
	 * Actualiza la lista de documentos que se convertiran al estatus indicado en <tt>estatusEsperado</tt>,
	 * incluyendo aquellos documentos que no fueron seleccionados para cambiarlos al estatus esperado,
	 * pero que comparten la misma nota de credito. Solo para usarse si la EPO tiene habilitada las
	 * Notas de Credito a Multiples Documentos.
	 *
	 *  @param doctosSeleccionados Arreglo con los Id de los documentos consultados; ver:
	 *                             nafin-web/13descuento/13pki/13if/13forma7b.jsp
	 *  @param estatusDoctos Arreglo en el que se indica el estatus de los documentos que han sido seleccionados
	 *                       para cambiar al estatus especificado. Una cadena vacia indica que el documento
	 *                       no ha sido seleccionado
	 *  @param estatusEsperado <tt>String</tt> con la clave del estatus: "N" para Negociable, "SP" para
	 *                         "Seleccionada Pyme" y "O" para Operado con Fondeo propio
	 * @return ArrayList con la lista de documentos actualizada y el estatus de los documentos actualizados, es
	 *         decir aquellos que tambien deben cambiar estatus al estatus indicado.
	 */
	public ArrayList actualizaDoctosSeleccionados(String doctosSeleccionados[],String estatusDoctos[],String estatusEsperado)
		throws AppException{

		log.info("actualizaDoctosSeleccionados(E)");

		ArrayList 		resultado 				= new ArrayList();
		HashSet			listaDocumentos 		= new HashSet();
		HashSet   		listaNotasDeCredito 	= new HashSet();

		AccesoDB 		con 						= new AccesoDB();
		StringBuffer	query						= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		int 				numeroDocumentosIniciales = 0;

		// Verificar que hayan datos
		if(doctosSeleccionados == null || (doctosSeleccionados.length == 0 ) || estatusDoctos == null || (estatusDoctos.length == 0) ){
			resultado.add(doctosSeleccionados);
			resultado.add(estatusDoctos);
			resultado.add(new Boolean("false"));
			log.info("actualizaDoctosSeleccionados(S)");
			return resultado;
		}

		// Validar que se haya especificado un estatus valido
		if(estatusEsperado == null || estatusEsperado.trim().equals("") ){
			resultado.add(doctosSeleccionados);
			resultado.add(estatusDoctos);
			resultado.add(new Boolean("false"));
			log.info("actualizaDoctosSeleccionados(S)");
			return resultado;
		}

		// Obtener lista de IDs de los documentos que han sido seleccionados para cambiar estatus a negociables
		for(int i=0;i<doctosSeleccionados.length;i++){
			String icDocumento = (String) doctosSeleccionados[i];
			String estatus 	 = (String) estatusDoctos[i];
			if(estatusEsperado.equals(estatus)){
				listaDocumentos.add(icDocumento);
			}
		}

		try{

			con.conexionDB();

			int numeroDoctosAnteriores = 0;
			numeroDocumentosIniciales = listaDocumentos.size();
			do{

				if(listaDocumentos.size() == 0){
					break;
				}

				numeroDoctosAnteriores = listaDocumentos.size();

				// 1. Traer notas de credito asociadas
				query						= new StringBuffer();
				query.append(
					"select                     "  +
					"	distinct IC_NOTA_CREDITO "  +
					"from                       "  +
					"	comrel_nota_docto        "  +
					"where                      "  +
					"	ic_documento in (        ");
				for(int i=0;i<listaDocumentos.size();i++){
					if(i>0) query.append(",");
					query.append("?");
				}
				query.append(" ) ");

				lVarBind.clear();
				Iterator it = listaDocumentos.iterator();
				while(it.hasNext()) {
					lVarBind.add((String)it.next());
				}

				registros = con.consultarDB(query.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					listaNotasDeCredito.add(registros.getString("IC_NOTA_CREDITO"));
				}

				if(listaNotasDeCredito.size() == 0){
					break;
				}
				// 2. Traer documentos relacionados
				query						= new StringBuffer();
				query.append(
					"select                    "  +
					"	distinct IC_DOCUMENTO   "  +
					"from                      "  +
					"	comrel_nota_docto       "  +
					"where                     "  +
					"	ic_nota_credito in (    ");
				for(int i=0;i<listaNotasDeCredito.size();i++){
					if(i>0) query.append(",");
					query.append("?");
				}
				query.append(" ) ");

				lVarBind.clear();
				it = listaNotasDeCredito.iterator();
				while(it.hasNext()) {
					lVarBind.add((String)it.next());
				}

				registros = con.consultarDB(query.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					listaDocumentos.add(registros.getString("IC_DOCUMENTO"));
				}

			}while(
				numeroDoctosAnteriores != listaDocumentos.size()
			); // 3. Comparar si hay diferencias

			// Construir Lista Unica con todos los IDs de los Documentos
			HashSet listaDoctosConsultados = new HashSet();
			for(int i=0;i<doctosSeleccionados.length;i++){
				 listaDoctosConsultados.add((String)doctosSeleccionados[i]);
			}
			Iterator iterator = listaDocumentos.iterator();
			while(iterator.hasNext()){
				listaDoctosConsultados.add((String)iterator.next());
			}
			// Crear Nuevo Array de Documentos Seleccionados y agregar todos los  ids.
			String[] newDoctosSeleccionados = new String[listaDoctosConsultados.size()];
			iterator = listaDoctosConsultados.iterator();
			int k =0;
			while(iterator.hasNext()){
				String icDocto = (String)iterator.next();
				newDoctosSeleccionados[k] = icDocto;
				k++;
			}
			// Crear Nuevo Array de Estatus Docto y agregarles "N" a aquellos que con estatus Negociable.
			String[] newEstatusDoctos = new String[listaDoctosConsultados.size()];
			for(int m = 0;m<newEstatusDoctos.length;m++){
				newEstatusDoctos[m] = "";
			}
			iterator = listaDocumentos.iterator();
			while(iterator.hasNext()){
				String icDoctoNegociable = (String)iterator.next();
				for(int j=0;j<newDoctosSeleccionados.length;j++){
					if(icDoctoNegociable.equals(newDoctosSeleccionados[j])){
						newEstatusDoctos[j]=estatusEsperado;
						break;
					}
				}
			}
			// Agregar los resultados obtenidos
			resultado.add(newDoctosSeleccionados);
			resultado.add(newEstatusDoctos);
			if( numeroDocumentosIniciales != listaDocumentos.size()){
				resultado.add(new Boolean("true")); // Se encontraron otros documentos a los que se les aplico
																// la Nota de Credito Multiple, por lo que se agregaron a la
																// lista de Documentos que tambien deben ser retornados a
																// Negociables.
			}else{
				resultado.add(new Boolean("false"));
			}

		}catch(Exception e){
			log.info("actualizaDoctosSeleccionados(Exception)");
			log.info("actualizaDoctosSeleccionados.doctosSeleccionados = <"+doctosSeleccionados+">");
			log.info("actualizaDoctosSeleccionados.estatusDoctos       = <"+estatusDoctos+">");
			e.printStackTrace();
			throw new AppException("Error al actualizar lista de Doctos Asociados a las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("actualizaDoctosSeleccionados(S)");
		}

		return resultado;

	}

	/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param noDocumento
	 * @param noIf
	 */

	public List ConsulCambioEstatusNaf(String noIf, String noDocumento)  throws AppException{
		log.info("ConsulCambioEstatusNaf(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		List datos = new ArrayList();
		List registros = new ArrayList();

		try{
			con.conexionDB();


		query = new StringBuffer();
		query.append(" SELECT ");
		query.append(" /*+  ordered INDEX (d IN_COM_DOCUMENTO_04_NUK) USE_NL (d ds pe e ed m )  */ ");

		query.append(" e.cg_razon_social as epo, ");
		query.append(" d.ig_numero_docto as noDocumento, ");
		query.append(" TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, ");
		query.append(" ed.cd_descripcion as estatus, ");
		query.append(" m.cd_nombre as moneda, ");
		query.append(" d.fn_monto as monto, ");
		query.append(" d.fn_porc_anticipo as porcentaje  ,  ");
		query.append(" '' as recursoGarantia, ");
		query.append(" d.fn_monto_dscto as montoDscto, ");
		query.append(" DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes) as montoInteres, ");
		query.append(" DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir) as montoOperar, ");
		query.append(" d.ic_documento as documento, ");
		query.append(" d.ic_moneda as ic_moneda, ");
		query.append("DECODE(ds.CS_OPERA_FISO,'S',ifs.cg_razon_social, ifs.CG_RAZON_SOCIAL) as nombre_if,"+
       "DECODE(ds.CS_OPERA_FISO,'S',ifs2.cg_razon_social,'N/A') as nombre_if_fondeo,"+
        "DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_interes||'','N/A') as in_importe_interes_fondeo,"+
        "DECODE(ds.CS_OPERA_FISO,'S',ds.in_importe_recibir||'','N/A') as in_importe_recibir_fondeo,") ;
		  query.append(" ds.CS_OPERA_FISO ");
    query.append(" FROM com_documento d, ");
    query.append(" com_docto_seleccionado ds, ");
		query.append(" comrel_pyme_epo pe, ");
	  query.append(" comcat_epo e, ");
		query.append(" comcat_estatus_docto ed, ");
		query.append(" comcat_moneda m, ");
		query.append(" comcat_if ifs,"+
							"comcat_if ifs2 ");
		query.append(" WHERE d.ic_documento = ds.ic_documento(+) ");
		query.append("  AND ifs.IC_IF = d.IC_IF ");
		query.append(" AND ifs2.IC_IF = ds.IC_IF ");
		query.append(" AND d.ic_epo = e.ic_epo ");
		query.append(" AND d.ic_moneda = m.ic_moneda ");
		query.append(" AND d.ic_epo = pe.ic_epo ");
		query.append(" AND d.ic_pyme = pe.ic_pyme ");
		query.append(" AND d.ic_estatus_docto = ed.ic_estatus_docto ");
		query.append(" AND pe.cs_habilitado = 'S' ");
		query.append(" AND e.cs_habilitado = 'S' ");
		query.append(" AND d.cs_dscto_especial != 'C' ");
		query.append(" AND d.ic_estatus_docto = 24 ");



		if(!noIf.equals("")){
			query.append(" AND ds.ic_if =  "+noIf);
		}
		if(!noDocumento.equals("") ){
			query.append(" AND d.ig_numero_docto in ('"+noDocumento+"')");
		}

	 query.append(" ORDER BY e.cg_razon_social, m.ic_moneda, d.df_fecha_docto, d.ig_numero_docto ");

		log.debug(query.toString());

		ps = con.queryPrecompilado(query.toString());
		rs = ps.executeQuery();
		while(rs.next()){
		datos = new ArrayList();

		/*0*/				datos.add((rs.getString("epo")==null?"":rs.getString("epo").trim()));
		/*1*/				datos.add((rs.getString("noDocumento")==null?"":rs.getString("noDocumento").trim()));
		/*2*/				datos.add((rs.getString("df_fecha_venc")==null?"":rs.getString("df_fecha_venc").trim()));
		/*3*/				datos.add((rs.getString("estatus")==null?"0":rs.getString("estatus").trim()));
		/*4*/				datos.add((rs.getString("moneda")==null?"":rs.getString("moneda").trim()));
		/*5*/				datos.add((rs.getString("monto")==null?"":rs.getString("monto").trim()));
		/*6*/				datos.add((rs.getString("porcentaje")==null?"":rs.getString("porcentaje").trim()));
		/*7*/				datos.add((rs.getString("recursoGarantia")==null?"":rs.getString("recursoGarantia").trim()));
		/*8*/				datos.add((rs.getString("montoDscto")==null?"":rs.getString("montoDscto").trim()));
		/*9*/				datos.add((rs.getString("montoInteres")==null?"0":rs.getString("montoInteres").trim()));
		/*10*/			datos.add((rs.getString("montoOperar")==null?"":rs.getString("montoOperar").trim()));
		/*11*/			datos.add((rs.getString("documento")==null?"":rs.getString("documento").trim()));
		/*12*/			datos.add((rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda").trim()));
		/*13*/			datos.add((rs.getString("nombre_if")==null?"":rs.getString("nombre_if").trim()));
		/*14*/			datos.add((rs.getString("nombre_if_fondeo")==null?"":rs.getString("nombre_if_fondeo").trim()));
		/*15*/			datos.add((rs.getString("in_importe_interes_fondeo")==null?"":rs.getString("in_importe_interes_fondeo").trim()));
		/*16*/			datos.add((rs.getString("in_importe_recibir_fondeo")==null?"":rs.getString("in_importe_recibir_fondeo").trim()));
		/*17*/			datos.add((rs.getString("CS_OPERA_FISO")==null?"":rs.getString("CS_OPERA_FISO").trim()));

		registros.add(datos);
		}
		rs.close();
		ps.close();


	return registros;

	}catch(Exception e){
		log.error(" ConsulCambioEstatusNaf Excepcion "+e);
		e.printStackTrace();
        throw new AppException("SIST0001");
    }finally{

		if(con.hayConexionAbierta()){
			con.terminaTransaccion(ok);
			con.cierraConexionDB();
		}
	log.info("ConsulCambioEstatusNaf(S)");
	}
}




	/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param noDocumento
	 * @param noIf
	 */

	public List ConsulCambioEstatusPre(String noDocumento, String estatus)  throws AppException{
		log.info("ConsulCambioEstatusPre(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		List datos = new ArrayList();
		List registros = new ArrayList();

		try{
			con.conexionDB();


		query = new StringBuffer();
		query.append(" SELECT ");
		query.append(" /*+  ordered INDEX (d IN_COM_DOCUMENTO_04_NUK) USE_NL (d ds pe e ed m )  */ ");

		query.append(" e.cg_razon_social as epo, ");
		query.append(" d.ig_numero_docto as noDocumento, ");
		query.append(" TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, ");
		query.append(" ed.cd_descripcion as estatus, ");
		query.append(" m.cd_nombre as moneda, ");
		query.append(" d.fn_monto as monto, ");
		query.append(" d.fn_porc_anticipo as porcentaje  ,  ");
		query.append(" '' as recursoGarantia, ");
		query.append(" d.fn_monto_dscto as montoDscto, ");
		query.append(" ds.in_importe_interes as montoInteres, ");
		query.append(" ds.in_importe_recibir as montoOperar, ");
		query.append(" d.ic_documento as documento, ");
		query.append(" d.ic_moneda as ic_moneda ");

    query.append(" FROM com_documento d, ");
    query.append(" com_docto_seleccionado ds, ");
		query.append(" comrel_pyme_epo pe, ");
	  query.append(" comcat_epo e, ");
		query.append(" comcat_estatus_docto ed, ");
		query.append(" comcat_moneda m ");
		query.append(" WHERE d.ic_documento = ds.ic_documento(+) ");
		query.append(" AND d.ic_epo = e.ic_epo ");
		query.append(" AND d.ic_moneda = m.ic_moneda ");
		query.append(" AND d.ic_epo = pe.ic_epo ");
		query.append(" AND d.ic_pyme = pe.ic_pyme ");
		query.append(" AND d.ic_estatus_docto = ed.ic_estatus_docto ");
		query.append(" AND pe.cs_habilitado = 'S' ");
		query.append(" AND e.cs_habilitado = 'S' ");
		query.append(" AND d.cs_dscto_especial != 'C' ");
		query.append(" AND d.ic_estatus_docto = "+estatus);

		if(!noDocumento.equals("") ){
			query.append(" AND d.ic_documento in ("+noDocumento+")");
		}

	 query.append(" ORDER BY e.cg_razon_social, m.ic_moneda, d.df_fecha_docto, d.ig_numero_docto ");
		ps = con.queryPrecompilado(query.toString());

		log.debug(query.toString());


		rs = ps.executeQuery();
		while(rs.next()){
		datos = new ArrayList();
		/*0*/				datos.add((rs.getString("epo")==null?"":rs.getString("epo").trim()));
		/*1*/				datos.add((rs.getString("noDocumento")==null?"":rs.getString("noDocumento").trim()));
		/*2*/				datos.add((rs.getString("df_fecha_venc")==null?"":rs.getString("df_fecha_venc").trim()));
		/*3*/				datos.add((rs.getString("estatus")==null?"0":rs.getString("estatus").trim()));
		/*4*/				datos.add((rs.getString("moneda")==null?"":rs.getString("moneda").trim()));
		/*5*/				datos.add((rs.getString("monto")==null?"":rs.getString("monto").trim()));
		/*6*/				datos.add((rs.getString("porcentaje")==null?"":rs.getString("porcentaje").trim()));
		/*7*/				datos.add((rs.getString("recursoGarantia")==null?"":rs.getString("recursoGarantia").trim()));
		/*8*/				datos.add((rs.getString("montoDscto")==null?"":rs.getString("montoDscto").trim()));
		/*9*/				datos.add((rs.getString("montoInteres")==null?"0":rs.getString("montoInteres").trim()));
		/*10*/			datos.add((rs.getString("montoOperar")==null?"":rs.getString("montoOperar").trim()));
		/*11*/			datos.add((rs.getString("documento")==null?"":rs.getString("documento").trim()));
		/*12*/			datos.add((rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda").trim()));

		registros.add(datos);
		}
		rs.close();
		ps.close();


	return registros;

	}catch(Exception e){
		log.error(" ConsulCambioEstatusPre Excepcion "+e);
		e.printStackTrace();
        throw new AppException("SIST0001");
    }finally{

		if(con.hayConexionAbierta()){
			con.terminaTransaccion(ok);
			con.cierraConexionDB();
		}
	log.info("ConsulCambioEstatusPre(S)");
	}
}


/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param datos
	 */
public String  getCambioEstatus(List datos )  throws AppException{
		log.info("getCambioEstatus(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		String acuse ="";

		try{
			con.conexionDB();

		String noDocumento = datos.get(0)==null?"":(String)datos.get(0);
		String causas = datos.get(1)==null?"":(String)datos.get(1);
		String _serial = datos.get(2)==null?"":(String)datos.get(2);
		String nombreUsuario = datos.get(3)==null?"":(String)datos.get(3);
		String folioCert = datos.get(4)==null?"":(String)datos.get(4);
		String estatus = datos.get(5)==null?"":(String)datos.get(5);
		String pkcs7 = datos.get(6)==null?"":(String)datos.get(6);
		String textoPlano = datos.get(7)==null?"":(String)datos.get(7);

		log.debug(" "+noDocumento);
		log.debug(" "+causas);
		log.debug(" "+_serial);
		log.debug(" "+nombreUsuario);
		log.debug(" "+folioCert);
		log.debug(" "+estatus);
		log.debug(" "+pkcs7);
		log.debug(" "+textoPlano);

			char getReceipt = 'Y';
			if(!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")){
				Seguridad s = new Seguridad();
				if( s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt) ){
					acuse = s.getAcuse();

					//se actualiza el estatus de
					//(24) En proceso de Autorizacion  IF a (3) Seleccionada Pyme
						query = new StringBuffer();
						query.append(" Update com_documento ");
						query.append(" SET  ic_estatus_docto =  "+estatus);
						query.append(" WHERE ic_documento in ("+noDocumento+")");

						log.debug(query.toString());

						ps = con.queryPrecompilado(query.toString());
						ps.executeUpdate();
						ps.close();



							if(!noDocumento.equals("")){
								List documento = new VectorTokenizer(noDocumento, "\n").getValuesVector();
								for(int i = 0; i < documento.size()-1; i++) 	{
											if(documento.get(i)!=null){
													List documentos = new VectorTokenizer((String)documento.get(i), ",").getValuesVector();
													for(int e = 0; e < documentos.size(); e++) 	{
															String undocu   = (documentos.size()>= 1)?documentos.get(e)==null?"":((String)documentos.get(e)).trim():"";

													log.debug("undocu "+undocu);
													if(!undocu.equals("")){
															query = new StringBuffer();
															query.append(" insert into comhis_cambio_estatus ");
															query.append(" (dc_fecha_cambio,ic_cambio_estatus,  ic_documento, ct_cambio_motivo, cg_nombre_usuario ) ");
															query.append(" values (sysdate, ?, ? ,? , ?) ");
															ps = con.queryPrecompilado(query.toString());
															ps.setString(1, "34");
															ps.setString(2, undocu);
															ps.setString(3, causas);
															ps.setString(4, nombreUsuario);
															ps.executeUpdate();
															ps.close();

															log.debug(query.toString());
													}//if(!undocu.equals("")){
													}
											}
									}
							}

				}//if( s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt) ){
			}//	if(!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")){

	return acuse;

	}catch(Exception e){
		log.error(" getCambioEstatus Excepcion "+e);
		e.printStackTrace();
        throw new AppException("SIST0001");
    }finally{

		if(con.hayConexionAbierta()){
			con.terminaTransaccion(ok);
			con.cierraConexionDB();
		}
	log.info("getCambioEstatus(S)");
	}
}

	/**
	 * Retorna a estatus "Seleccionada Pyme" a aquellos documentos que se encuentren en
	 * estatus: "En Proceso de Autorizacion IF"
	 *
	 * @param clavesDocumento <tt>String</tt> con cada una de las claves de los documentos,
	 *								  separados por comas.
	 *
	 * @return <tt>true</tt> si la operacion fue exitosa y <tt>false</tt> en caso contrario.
	 */
	public boolean bactualizaEstatusSeleccionadasPyme(String clavesDocumento)
		throws NafinException {

		log.info("bactualizaEstatusSeleccionadasPyme(E)");

		if( clavesDocumento == null || clavesDocumento.trim().equals("") ){
			log.info("bactualizaEstatusSeleccionadasPyme: No se especifico ningun documento");
			log.info("bactualizaEstatusSeleccionadasPyme(S)");
			return false;
		}

		AccesoDB 			con 				= new AccesoDB();
		boolean 				actualizacion 	= true;
		StringBuffer 		query 			= new StringBuffer();
		PreparedStatement	ps					= null;

		try {

			con.conexionDB();

			// Construir arreglo de variables bing
			String[] 		claves 			= clavesDocumento.split(",");
			StringBuffer 	variablesBind 	= new StringBuffer();
			for(int i=0;i<claves.length;i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}

			// Cambiar estatus de los documentos: En Proceso de Autorizacion IF a Seleccionada Pyme
			query.append(
					" UPDATE                         "  +
					"     COM_DOCUMENTO              "  +
					"SET                             "  +
					"     IC_ESTATUS_DOCTO = 3       "  + // Seleccionada Pyme
					" WHERE                          "  +
					"     IC_DOCUMENTO IN ( "+variablesBind.toString()+" ) AND "+
					"     IC_ESTATUS_DOCTO IN ( 24 ) "
			);

			try {

				ps = con.queryPrecompilado(query.toString());
				for(int indice=0;indice < claves.length;indice++){
					ps.setInt(indice+1,Integer.parseInt(claves[indice].trim()) );
				}
				ps.executeUpdate();

			} catch(SQLException sqle) {
				actualizacion = false;
				throw new NafinException("DSCT0011");
			} catch(Exception e){
				actualizacion = false;
				throw e;
			}

		} catch (NafinException ne) {
			log.error("bactualizaEstatusSeleccionadasPyme(NafinException)");
			log.error("bactualizaEstatusSeleccionadasPyme.clavesDocumento = <"+clavesDocumento+">");
			actualizacion = false;
			throw ne;
		} catch (Exception e) {
			log.error("bactualizaEstatusSeleccionadasPyme(Exception)");
			log.error("bactualizaEstatusSeleccionadasPyme.clavesDocumento = <"+clavesDocumento+">");
			actualizacion = false;
			throw new NafinException("SIST0001");
		} finally {

			if(ps != null ){ try{ ps.close(); }catch(Exception e){} }

			if (con.hayConexionAbierta()){
				con.terminaTransaccion(actualizacion);
				con.cierraConexionDB();
			}
			log.info("bactualizaEstatusSeleccionadasPyme(S)");

		}

		return actualizacion;

	}


	public List getEstatusAsignar(String esEstatuDeConsulta) throws AppException
	{
		log.info(" MantenimientoEJB::getEstatusAsignar(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lstCatalogo = new ArrayList();

		try {
			con.conexionDB();
			String lsQryEstatus  = "";
			String lsEstatus = "";

			if(esEstatuDeConsulta.equals("1"))	// No Negociada
				lsEstatus = "5";			// Puede cambiar a Baja
			if(esEstatuDeConsulta.equals("2"))	// Negociada
				lsEstatus = "5,6,7,21";	// Puede cambiar a Baja, Descuento Fisico, Pagada Anticipado, Bloqueado
			if(esEstatuDeConsulta.equals("21"))	// Bloqueada
				lsEstatus = "2";			// Puede cambiar a Negociada
			if(esEstatuDeConsulta.equals("28"))	// Pre negociable
				lsEstatus = "5,6,7";		// Baja (5),Descuento F�sico (6), Pagado Anticipado (7) )
			if(esEstatuDeConsulta.equals("33"))	// Pre negociable
				lsEstatus = "2, 5 ";		// Negociada (2), Baja (5)


			lsQryEstatus  = "SELECT ic_estatus_docto, cd_descripcion as cd_descripcion"+
							" FROM comcat_estatus_docto" +
							" WHERE ic_estatus_docto in ("+lsEstatus+") " +
							" ORDER BY ic_estatus_docto";


			ps = con.queryPrecompilado(lsQryEstatus);
			rs = ps.executeQuery();

			while(rs!=null && rs.next()){
					HashMap mpRegistro = new HashMap();
					mpRegistro.put("clave",rs.getString(1));
					mpRegistro.put("descripcion", rs.getString(2));

					lstCatalogo.add(mpRegistro);		//Vector con los registros de las EPOS

			}

			rs.close();
			ps.close();

			return lstCatalogo;

		} catch (Throwable t){
			throw new AppException("Error al consultar catalogo de estatus");
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();

			log.info("MantenimientoEJB::getEstatusAsignar(S)");
		}
	}//Fin del M�todo getEstatusAsignar()

  public String  getArchivoBajaProveedores(String cveEpo, String strDirectorioTmp) throws AppException
  {
    log.info("getArchivoBajaProveedores(E)");
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    StringBuffer strSQL = new StringBuffer();
    StringBuffer contenidoArchivo = new StringBuffer();
    try
    {
      con.conexionDB();
      strSQL.append("SELECT   cp.ic_pyme cvepyme, cp.cg_rfc rfc, cp.cg_razon_social nombrepyme, cpe.cg_pyme_epo_interno numprov");
      strSQL.append("    FROM comrel_pyme_epo cpe, comcat_pyme cp ");
      strSQL.append("   WHERE cpe.ic_pyme = cp.ic_pyme ");
      strSQL.append("     AND cpe.cs_aceptacion = ? ");
      strSQL.append("     AND cpe.ic_epo = ? ");
      strSQL.append("     AND cp.ic_tipo_cliente = ? ");
      strSQL.append("     AND NOT EXISTS (SELECT 1 ");
      strSQL.append("                       FROM com_documento ");
      strSQL.append("                      WHERE ic_pyme = cp.ic_pyme AND ic_epo = cpe.ic_epo ) ");
      strSQL.append("ORDER BY cp.cg_razon_social ");

      ps = con.queryPrecompilado(strSQL.toString());
      ps.setString(1,"S");
      ps.setInt(2, Integer.parseInt(cveEpo));
      ps.setInt(3, 1);
      rs = ps.executeQuery();

      while(rs.next())
      {
        contenidoArchivo.append(rs.getString("numprov")).append("|");
        contenidoArchivo.append(rs.getString("nombrepyme")).append("|");
        contenidoArchivo.append(rs.getString("rfc")).append("|");
        contenidoArchivo.append(rs.getString("cvepyme")).append("\n");
      }
      rs.close();
      ps.close();

      //CREAR ZIP
      CreaArchivo archivo = new CreaArchivo();
      archivo.make(contenidoArchivo.toString(), strDirectorioTmp, ".txt");
      String nombreArchivo = archivo.nombre;
      List listComprime  = new ArrayList();
      listComprime.add(nombreArchivo);

      String nombreZip = Comunes.cadenaAleatoria(16)+".zip";
      String rutaNombreZip = strDirectorioTmp+nombreZip;
      ComunesZIP.comprimir(listComprime,strDirectorioTmp,rutaNombreZip);

      return nombreZip;
    }catch(Throwable t)
    {
      log.info("getArchivoBajaProveedores(Error)");
      throw new AppException("Error al consultar y crear archivo de proveedores para dar de baja", t);
    }finally
    {
      if(con.hayConexionAbierta())
      {
        con.cierraConexionDB();
      }
      log.info("getArchivoBajaProveedores(S)");
    }
  }


  /**
	 * Metodo para la carga temporal del archivo a ser porcesado para la Modificacion(Ajuste)
	 * al Fondo Junior
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param rutaArchivo
	 */
	public String cargaDatosTmpBajaProveedores (String rutaArchivo, String rutaPdf) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String lineaf = "";
		String numeroProceso = "";
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		boolean commit = true;
		int contregistros = 0;
		int numLinea = 1;

		try{
			con.conexionDB();

			query = "SELECT SEQ_COMTMP_BAJA_PROVEEDOR.NEXTVAL numProc FROM DUAL";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				numeroProceso = rs.getString("numProc");
			}
			rs.close();
			ps.close();

			query = " INSERT INTO COMTMP_BAJA_PROVEEDOR " +
				"            (ic_numero_proceso, ic_numero_linea, CG_RFC, CG_RAZON_SOCIAL, CG_PYME_EPO_INTERNO, IC_PYME " +
				//"             CG_MENSAJES_ERROR " +
				"            ) " +
				"     VALUES (?, ?, ?, ?, ?, ? ) ";


			java.io.File   f	= new java.io.File(rutaArchivo);
			BufferedReader br	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));
			while ((lineaf=br.readLine())!=null){
				contregistros++;
				vtd		= new VectorTokenizer(lineaf,"|");
				vecdet	= vtd.getValuesVector();

        if(vecdet.size()>0){

          String numProv	=(vecdet.size()>=1)?vecdet.get(0).toString().trim():"";
          String nombrePyme		=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
          String rfcPyme	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
          String cvePyme	=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"0";

          numProv  =Comunes.quitaComitasSimplesyDobles(numProv);
          nombrePyme		 =Comunes.quitaComitasSimplesyDobles(nombrePyme);
          rfcPyme =Comunes.quitaComitasSimplesyDobles(rfcPyme);
          cvePyme =Comunes.quitaComitasSimplesyDobles(cvePyme);

          numProv	 =((numProv.length()>25)?numProv.substring(0,16):numProv);
          nombrePyme		 =((nombrePyme.length()>100)?nombrePyme.substring(0,101):nombrePyme);
          rfcPyme =((rfcPyme.length()>20)?rfcPyme.substring(0,21):rfcPyme);
          cvePyme =((cvePyme.length()>8)?cvePyme.substring(0,9):cvePyme);

          ps = con.queryPrecompilado(query);
          ps.setLong(1, Long.parseLong(numeroProceso));
          ps.setInt(2, numLinea++);
          ps.setString(3, rfcPyme);
          ps.setString(4, nombrePyme);
          ps.setString(5, numProv);
          ps.setString(6, cvePyme);

          ps.executeUpdate();
          ps.close();
        }

			}

			return numeroProceso;

		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al cargar informacion temporal de venc a modificar en Fondo Jr. ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

  private String validaXregBajaProv(AccesoDB con, String cvePyme, String cveEpo)throws AppException
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    String strSQL = "";
    String msgError = "";
    int result = 0;
    try{
      //Valida que el proveedor este afiliado a la epo
      strSQL = "SELECT count(*) " +
              "  FROM comrel_pyme_epo " +
              " WHERE ic_pyme = ? AND ic_epo = ? ";

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(cvePyme));
      ps.setLong(2, Long.parseLong(cveEpo));
      rs = ps.executeQuery();

      while(rs.next()){  result = rs.getInt(1); }
      rs.close();
      ps.close();

      if(result<=0)
      {
        msgError = "El proveedor que intenta eliminar no est� afiliado a la EPO seleccionada";
      }else{
        //Valida que la afliacion este activa
        strSQL = "SELECT count(*) " +
                "  FROM comrel_pyme_epo " +
                " WHERE ic_pyme = ? AND ic_epo = ? AND cs_aceptacion = ? ";

        ps = con.queryPrecompilado(strSQL);
        ps.setLong(1, Long.parseLong(cvePyme));
        ps.setLong(2, Long.parseLong(cveEpo));
        ps.setString(3, "S");
        rs = ps.executeQuery();

        while(rs.next()){  result = rs.getInt(1); }
        rs.close();
        ps.close();

        if(result<=0)
        {
          msgError = "El proveedor tiene estatus 'H' en la Afiliaci�n";
        }else{
          //Valida que el proveedor no cuente con documentos publicados.
          strSQL = "SELECT   count(*) " +
                  "    FROM comrel_pyme_epo cpe  " +
                  "   WHERE cpe.cs_aceptacion = 'S' " +
                  "     AND cpe.ic_pyme = ? " +
                  "     AND cpe.ic_epo = ?  " +
                  //"     AND cp.ic_tipo_cliente = ?  " +
                  "     AND EXISTS (SELECT 1  " +
                  "                       FROM com_documento  " +
                  "                      WHERE ic_pyme = cpe.ic_pyme AND ic_epo = cpe.ic_epo )  ";

          ps = con.queryPrecompilado(strSQL);
          ps.setLong(1, Long.parseLong(cvePyme));
          ps.setLong(2, Long.parseLong(cveEpo));
          //ps.setLong(3, Long.parseLong("1"));
          rs = ps.executeQuery();

          while(rs.next()){  result = rs.getInt(1); }
          rs.close();
          ps.close();
          if(result>0)
          {
            msgError = "El proveedor cuenta con documentos publicados";
          }
        }
      }

      return msgError;
    }catch(Throwable t)
    {
      throw new AppException("Error al validar la relacion pyme-epo para darla de baja", t);
    }

  }

  public void validaCargaProveedores(String numProceso, String cveEpo) throws AppException
  {
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    PreparedStatement ps2 = null;
    String strSQL = "";
    boolean commit = true;
    try
    {
      con.conexionDB();
      strSQL = " SELECT ic_numero_linea, CG_RFC, CG_RAZON_SOCIAL, CG_PYME_EPO_INTERNO, IC_PYME " +
               " FROM comtmp_baja_proveedor WHERE ic_numero_proceso = ? " +
               " ORDER BY ic_numero_linea ";

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(numProceso));
      rs = ps.executeQuery();

      String msgError = "";
      while(rs.next())
      {
        msgError = validaXregBajaProv(con, rs.getString("ic_pyme"), cveEpo);
        if(!"".equals(msgError))
        {
          strSQL = "UPDATE comtmp_baja_proveedor " +
                  "   SET cg_mensajes_error = ? " +
                  " WHERE ic_numero_proceso = ? AND ic_numero_linea = ? ";

          ps2 = con.queryPrecompilado(strSQL);
          ps2.setString(1,msgError);
          ps2.setLong(2, Long.parseLong(numProceso));
          ps2.setLong(3, Long.parseLong(rs.getString("ic_numero_linea")));
          ps2.executeUpdate();
          ps2.close();

        }
      }
      rs.close();
      ps.close();

    }catch(Throwable t)
    {
      commit = false;
      throw new AppException("Error al realizar validacion de registro a dar de baja de proveedores", t);
    }finally
    {
      if(con.hayConexionAbierta())
      {
        con.terminaTransaccion(commit);
        con.cierraConexionDB();
      }
    }
  }

  public List getListBajaProvValidados(String numProceso)throws AppException
  {
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    List lstResult = new ArrayList();
    String strSQL = "";
    try
    {
      con.conexionDB();

      strSQL = "SELECT   ic_numero_linea, cg_razon_social, " +
          "             ic_pyme, cg_razon_social, cg_rfc, cg_pyme_epo_interno " +
          "    FROM comtmp_baja_proveedor " +
          "   WHERE ic_numero_proceso = ? AND cg_mensajes_error IS NULL " +
          "   ORDER BY ic_numero_proceso, ic_numero_linea ";

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(numProceso));
      rs = ps.executeQuery();

      List lstCorrect = new ArrayList();
      while(rs.next()){
      HashMap hmCorrect = new HashMap();
      hmCorrect.put("LINEA", rs.getString("ic_numero_linea"));
      hmCorrect.put("DESCRIPCION", rs.getString("cg_razon_social"));
      hmCorrect.put("IC_PYME", rs.getString("ic_pyme"));
      hmCorrect.put("CG_RAZON_SOCIAL", rs.getString("cg_razon_social"));
      hmCorrect.put("CG_RFC", rs.getString("cg_rfc"));
      hmCorrect.put("CG_PYME_EPO_INTERNO", rs.getString("cg_pyme_epo_interno"));
      lstCorrect.add(hmCorrect);
      }
      rs.close();
      ps.close();

      strSQL = "SELECT   ic_numero_linea, cg_mensajes_error, " +
      "             ic_pyme, cg_razon_social, cg_rfc, cg_pyme_epo_interno " +
      "    FROM comtmp_baja_proveedor " +
      "   WHERE ic_numero_proceso = ? AND cg_mensajes_error IS NOT NULL " +
      "   ORDER BY ic_numero_proceso, ic_numero_linea ";

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(numProceso));
      rs = ps.executeQuery();

      List lstError = new ArrayList();
      while(rs.next()){
      HashMap hmCorrect = new HashMap();
      hmCorrect.put("LINEA", rs.getString("ic_numero_linea"));
      hmCorrect.put("DESCRIPCION", rs.getString("cg_mensajes_error"));
      hmCorrect.put("IC_PYME", rs.getString("ic_pyme"));
      hmCorrect.put("CG_RAZON_SOCIAL", rs.getString("cg_razon_social"));
      hmCorrect.put("CG_RFC", rs.getString("cg_rfc"));
      hmCorrect.put("CG_PYME_EPO_INTERNO", rs.getString("cg_pyme_epo_interno"));
      lstError.add(hmCorrect);
      }
      rs.close();
      ps.close();

      lstResult.add(lstCorrect);
      lstResult.add(lstError);

      return lstResult;
    }catch(Throwable t)
    {
      throw new AppException("Error al consultar resultado de la validacion de reg Baja Proveedores", t);
    }finally{
      if(con.hayConexionAbierta()){
      con.cierraConexionDB();
      }
    }
  }


  public String setBajaProveedores(String numProceso, String cveEpo, String rutaZip, String rutaPdf, String pkcs7, String textoFirmado, String serial ,String folio, String usuario) throws AppException
  {
    log.info("setBajaProveedores(E) ");
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    PreparedStatement ps2 = null;
    ResultSet rs2 = null;
    String strSQL = "";
    String  acuse = "";
    boolean commit = true;
    StringBuffer strDatosNuevos = new StringBuffer();
    String mostrarError = "";
    int regErroneos = 0;
    int regCorrectos = 0;
    try
    {
      con.conexionDB();
      strSQL = "";


      char getReceipt = 'Y';
      if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") ) {
         Seguridad s = new Seguridad();

			if ( s.autenticar(folio, serial, pkcs7, textoFirmado,getReceipt)  ) {
        acuse = s.getAcuse();

        strSQL = "SELECT   ic_numero_linea, ic_pyme, cg_mensajes_error " +
                    "    FROM comtmp_baja_proveedor " +
                    "   WHERE ic_numero_proceso = ? " +
                    //"    AND cg_mensajes_error IS NULL " +
                    "ORDER BY ic_numero_proceso, ic_numero_linea ";

          ps = con.queryPrecompilado(strSQL);
          ps.setLong(1, Long.parseLong(numProceso));
          rs = ps.executeQuery();
          int existReg = 0;

          while(rs.next())
          {
            if(rs.getString("cg_mensajes_error")==null || "".equals(rs.getString("cg_mensajes_error"))){
              strSQL = "SELECT COUNT (*) " +
                      "  FROM comrel_pyme_epo cpe " +
                      " WHERE cpe.ic_epo <> ?  " +
                      "  AND cpe.ic_pyme = ? ";

              ps2 = con.queryPrecompilado(strSQL);
              ps2.setLong(1, Long.parseLong(cveEpo));
              ps2.setLong(2, Long.parseLong(rs.getString("ic_pyme")));
              rs2 = ps2.executeQuery();
              if(rs2.next())
              {
                existReg = rs2.getInt(1);
              }
              rs2.close();
              ps2.close();

              strSQL = "delete COMREL_PYME_EPO_X_PRODUCTO " +
                      " where ic_epo = ? AND ic_pyme = ? ";
              ps2 = con.queryPrecompilado(strSQL);
              ps2.setLong(1, Long.parseLong(cveEpo));
              ps2.setLong(2, Long.parseLong(rs.getString("ic_pyme")));
              ps2.executeUpdate();
              ps2.close();

              strSQL = "DELETE FROM comrel_pyme_epo cpe " +
                    "      WHERE cpe.cs_aceptacion = 'S'  " +
                    "      AND cpe.ic_epo = ? AND cpe.ic_pyme = ? ";

              ps2 = con.queryPrecompilado(strSQL);
              ps2.setLong(1, Long.parseLong(cveEpo));
              ps2.setLong(2, Long.parseLong(rs.getString("ic_pyme")));
              ps2.executeUpdate();

              if(existReg<=0)
              {
                strSQL = "delete com_contacto where ic_pyme = ? ";
                ps2 = con.queryPrecompilado(strSQL);
                ps2.setLong(1, Long.parseLong(rs.getString("ic_pyme")));
                ps2.executeUpdate();
                ps2.close();

                strSQL = "delete COM_DOMICILIO where ic_pyme = ? ";
                ps2 = con.queryPrecompilado(strSQL);
                ps2.setLong(1, Long.parseLong(rs.getString("ic_pyme")));
                ps2.executeUpdate();
                ps2.close();

                strSQL = "delete SEG_USUARIO where ic_pyme = ? ";
                ps2 = con.queryPrecompilado(strSQL);
                ps2.setLong(1, Long.parseLong(rs.getString("ic_pyme")));
                ps2.executeUpdate();
                ps2.close();

                strSQL = "delete COMREL_PYME_IF_CREDELE where ic_pyme = ? ";
                ps2 = con.queryPrecompilado(strSQL);
                ps2.setLong(1, Long.parseLong(rs.getString("ic_pyme")));
                ps2.executeUpdate();
                ps2.close();

                strSQL = "delete comcat_pyme where ic_pyme = ? ";
                ps2 = con.queryPrecompilado(strSQL);
                ps2.setLong(1, Long.parseLong(rs.getString("ic_pyme")));
                ps2.executeUpdate();
                ps2.close();
              }
              regCorrectos++;
            }else
            {
              regErroneos++;
            }
          }
          rs.close();
          ps.close();
          int totalReg = regCorrectos+regErroneos;
          strDatosNuevos.append("IC_EPO: ").append(cveEpo).append(" Total Reg Enviados: ").append(totalReg).append(" Total Reg Eliminados: ").append(regCorrectos).append(" Folio: ").append(acuse);

          Bitacora.grabarEnBitacoraProvBaja(con, "CAPT_PROV_BAJA", "N","0", usuario, "", strDatosNuevos.toString(),rutaZip, rutaPdf );


        }else {

          mostrarError = 	s.mostrarError();
          log.info("setBajaProveedores :: "+mostrarError);
        }
			}

      return acuse;
    }catch(Throwable t )
    {
      commit = false;
      t.printStackTrace();
      throw new AppException("Error al dar de baja los proveedores");
    }finally
    {
      log.info("setBajaProveedores(S) ");
      if(con.hayConexionAbierta())
      {
        con.terminaTransaccion(commit);
        con.cierraConexionDB();
      }

    }
  }

  public String getDescargaArchivoBajaProv(String rutaDestino, String cveCambio, String tipoArchivo){
		StringBuffer strSQL = new StringBuffer();
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		String nombreArchivoTmp = null;


		try {
			con.conexionDB();
			strSQL.append(
          "SELECT bi_zip, bi_pdf " +
          "  FROM bit_cambios_gral " +
          " WHERE ic_cambio = ? "
			);


			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1,Integer.parseInt(cveCambio));

			rs = ps.executeQuery();
			String nombreArchivo = "";
			String ruta = "";
			while(rs.next()){
				InputStream inStream = null;
        if("PDF".equals(tipoArchivo)){
          inStream = rs.getBinaryStream("bi_pdf");
          nombreArchivo = cveCambio+"BajaProv";
          ruta = rutaDestino+nombreArchivo+".pdf";
        }

        if("ZIP".equals(tipoArchivo)){
          inStream = rs.getBinaryStream("bi_zip");
          nombreArchivo = cveCambio+"BajaProv";
          ruta = rutaDestino+nombreArchivo+".zip";
        }

				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.setNombre(nombreArchivo);

				if (!creaArchivo.make(inStream, ruta)) {
					throw new AppException("Error al generar el archivo en " + nombreArchivo);
				}

				nombreArchivoTmp = creaArchivo.getNombre();
        System.out.println("nombreArchivoTmp ===== "+nombreArchivoTmp);
				inStream.close();

			}
			rs.close();
			ps.close();

      return nombreArchivoTmp;

		}catch(Exception e) {
			throw new AppException("Error en la descarga de los archivos. ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
  }

}// Fin de la Clase