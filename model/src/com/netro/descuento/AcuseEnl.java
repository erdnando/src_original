package com.netro.descuento;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class AcuseEnl {

	private String	cc_acuse				= "";
	private String 	cs_estatus				= "null";
	private String 	cc_control				= "null";

	private int 	in_total_proc			= 0;
	private int 	in_total_proc_detalle	= 0;
	private int 	in_total_docto_mn		= 0;
	private int		in_total_docto_dl		= 0;	
	private double 	fn_total_monto_mn		= 0;
	private double 	fn_total_monto_dl		= 0;	
	private int 	in_total_proc_mn_neg	= 0;
	private double 	fn_total_monto_mn_neg	= 0;
	private int 	in_total_proc_dl_neg	= 0;
	private double 	fn_total_monto_dl_neg	= 0;
	private int 	in_total_proc_mn_noneg	= 0;
	private double 	fn_total_monto_mn_noneg	= 0;	
	private int 	in_total_proc_dl_noneg	= 0;
	private double 	fn_total_monto_dl_noneg	= 0;	
	private int 	in_total_acep_mn		= 0;
	private int 	in_total_rech_mn		= 0;
	private int 	in_total_acep_dl		= 0;
	private int 	in_total_rech_dl		= 0;
	private double	fn_total_monto_acep_mn	= 0;
	private double	fn_total_monto_rech_mn	= 0;
	private double	fn_total_monto_acep_dl	= 0;
	private double	fn_total_monto_rech_dl	= 0;

	public void add(double monto,int icMoneda,boolean aceptado,String descuento){
		in_total_proc++;
		if(icMoneda==1){
			in_total_docto_mn++;
			fn_total_monto_mn += monto;
			if("N".equalsIgnoreCase(descuento)||"V".equalsIgnoreCase(descuento)){
				in_total_proc_mn_neg++;				
				fn_total_monto_mn_neg += monto;
			}else if("X".equalsIgnoreCase(descuento)){
				in_total_proc_mn_noneg++;
				fn_total_monto_mn_noneg += monto;
			}
			if(aceptado){
				in_total_acep_mn++;
				fn_total_monto_acep_mn+=monto;
			}else{
				in_total_rech_mn++;
				fn_total_monto_rech_mn+=monto;
			}
		}else if(icMoneda==54){
			in_total_docto_dl++;
			fn_total_monto_dl += monto;
			if("N".equalsIgnoreCase(descuento)||"V".equalsIgnoreCase(descuento)){
				in_total_proc_dl_neg++;				
				fn_total_monto_dl_neg += monto;
			}else if("X".equalsIgnoreCase(descuento)){
				in_total_proc_dl_noneg++;
				fn_total_monto_dl_noneg += monto;
			}
			if(aceptado){
				in_total_acep_dl++;
				fn_total_monto_acep_dl+=monto;
			}else{
				in_total_rech_dl++;
				fn_total_monto_rech_dl+=monto;
			}
		}		
	}

	public void setCcAcuse(String cc_acuse)	{
		this.cc_acuse = cc_acuse;		
	}
	public void setCsEstatus(String cs_estatus){
		this.cs_estatus = cs_estatus;
	}
	public void setCcControl(String cc_control){
		this.cc_control = cc_control;
	}
	public void setInTotalProcDetalle(int totalprocdetalle){
		this.in_total_proc_detalle = totalprocdetalle;
	}
	
	public String getCcAcuse(){
		return this.cc_acuse;
	}
	public String getCsEstatus(){
		return this.cs_estatus;
	}
	public String getCcControl(){
		return this.cc_control;
	}	
	public int 	getInTotalProc(){
		return this.in_total_proc;
	}
	public int getInTotalProcDetalle(){
		return this.in_total_proc_detalle;
	}
	public int getInTotalprocMnNeg(){
		return this.in_total_proc_mn_neg;
	}
	public int 	getInTotalDoctoMn(){
		return this.in_total_docto_mn;
	}
	public double getFnTotalMontoMn(){
		return this.fn_total_monto_mn;
	}
	public double getFnTotalMontoMnNeg(){
		return this.fn_total_monto_mn_neg;
	}
	public int getInTotalAcepMn(){
		return this.in_total_acep_mn;
	}
	public int getInTotalRechMn(){	
		return this.in_total_rech_mn;
	}
	public int getInTotalProcMnNoneg(){
		return this.in_total_proc_mn_noneg;
	}
	public double getFnTotalMontoMnNoneg(){
		return this.fn_total_monto_mn_noneg;
	}
	public int 	getInTotalAcepDl(){
		return this.in_total_acep_dl;
	}
	public int getInTotalRechDl(){
		return this.in_total_rech_dl;
	}
	public int getInTotalProcDlNeg(){
		return this.in_total_proc_dl_neg;
	}
	public int getInTotalDoctoDl(){
		return this.in_total_docto_dl;
	}
	public double getFnTotalMontoDl(){
		return this.fn_total_monto_dl;
	}
	public double getFnTotalMontoDlNeg(){
		return this.fn_total_monto_dl_neg;
	}
	public int getInTotalProcDlNoneg(){
		return this.in_total_proc_dl_noneg;
	}
	public double getFnTotalMontoDlNoneg(){
		return this.fn_total_monto_dl_noneg;	
	}
	public double getFnTotalMontoRechMn(){
		return this.fn_total_monto_rech_mn;
	}
	public double getFnTotalMontoAcepMn(){
		return this.fn_total_monto_acep_mn;
	}	
	public double getFnTotalMontoRechDl(){
		return this.fn_total_monto_rech_dl;
	}
	public double getFnTotalMontoAcepDl(){
		return this.fn_total_monto_acep_dl;
	}	

	public String toString(){
		String strAcuse = 
			"\n[\nAcuse               = "+cc_acuse+
			",\nEstatus             = "+cs_estatus+
			",\nControl             = "+cc_control+
			",\nTotal Proc          = "+in_total_proc+
			",\nTotalProcDetalle    = "+in_total_proc_detalle+
			",\nTotalDoctos MN      = "+in_total_docto_mn+
			",\nTotalDoctos DL      = "+in_total_docto_dl+
			",\nTotalMonto MN       = "+fn_total_monto_mn+
			",\nTotalMonto DL       = "+fn_total_monto_dl+
			",\nTotalProc Mn Neg    = "+in_total_proc_mn_neg+
			",\nTotalMonto MN Neg   = "+fn_total_monto_mn_neg+
			",\nTotalProc DL Neg    = "+in_total_proc_dl_neg+
			",\nTotalMonto DL Neg   = "+fn_total_monto_dl_neg+
			",\nTotalProc MN NoNeg  = "+in_total_proc_mn_noneg+
			",\nTotalMonto MN Noneg = "+fn_total_monto_mn_noneg+
			",\nTotalProc DL Noneg  = "+in_total_proc_dl_noneg+
			",\nTotalMonto DL Noneg = "+fn_total_monto_dl_noneg+
			",\nTotalAcep MN        = "+in_total_acep_mn+
			",\nTotalMonto Acep MN  = "+fn_total_monto_acep_mn+
			",\nTotal Rech MN       = "+in_total_rech_mn+
			",\nTotalMonto Rech MN  = "+fn_total_monto_rech_mn+
			",\nTotal Acep DL       = "+in_total_acep_dl+
			",\nTotalMonto Acep DL  = "+fn_total_monto_acep_dl+
			",\nTotal Rech DL       = "+in_total_rech_dl+
			",\nTotalMonto Rech DL  = "+fn_total_monto_rech_dl+			
			"\n]";
			
		return strAcuse;
	}

}
