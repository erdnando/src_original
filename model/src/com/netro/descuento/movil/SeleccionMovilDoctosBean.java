package com.netro.descuento.movil;

import com.netro.afiliacion.Afiliacion;
import com.netro.descuento.ISeleccionDocumento;
import com.netro.descuento.ParametrosDescuento;
import com.netro.exception.Horario;
import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.seguridadbean.Seguridad;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.Context;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CorreoArchivoAdjunto;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 *
 *  EJB de Selecci&oacute;n M&oacute;vil de Documentos. Para esta primera versi&oacute;n, s&oacute;lo se tiene
 *  considerado lo siguiente:
 *		<ul>
 *  		<li>Factoraje Normal.</li>
 *       <li>Se puede seleccionar individualmente, como m&aacute;ximo, 20 documentos por EPO.</li>
 *			<li>No se tiene soporte para manejar Notas de Cr&eacute;dito.</li>
 *    </ul>
 *
 *  @since Fodea 051 - 2011, 28 de octubre de 2011.
 *  @author Jesus Salim Hernandez David
 *
 */

@Stateless(name = "SeleccionMovilDoctosEJB" , mappedName = "SeleccionMovilDoctosEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class SeleccionMovilDoctosBean implements SeleccionMovilDoctos {
   // CONSTANTES GENERALES

   private final static int      DOLAR_AMERICANO         		= 54;
   private final static int      MONEDA_NACIONAL         		= 1;

   private final static String   DESCUENTO_ELECTRONICO   		= "1";
	private final static int   	DOCUMENTO_NEGOCIABLE    		= 2;
	private final static String   NOTA_CREDITO 						= "C";
	private final static String 	FACTORAJE_NORMAL 					= "N";

	/**
	 * N&uacute;mero m&aacute;ximo de documentos individuales que pueden ser mostrados en pantalla. El valor actual
	 * de este n&uacute;mero es 20.
	 */
	public final static int			LIMITE_DOCTOS_MOSTRADOS	= 20;

	/**
	 * Rango m&aacute;ximo en d�as permitido para las consultas realizadas en los m&eacute;todos: <tt>getResumenOperacionesRealizadasPorIF</tt>
	 * y <tt>getResumenOperacionesRealizadasPorEPO</tt>. El valor actual de este n&uacute;mero es 15.
	 */
	public final static int			RANGO_MAXIMO_FECHAS		= 15;

   private final static Log log = ServiceLocator.getInstance().getLog(SeleccionMovilDoctosBean.class);

   // 1. METODOS CORRESPONDIENTES AL MODULO 1 { PANTALLA: ADMIN PYME - CONSULTA DE DOCUMENTOS }

	/**
	 * Devuelve un HashMap con el catalogo EPOs que operan con la PYME <tt>clavePyme</tt>
	 * con el producto indicado: <tt>claveProducto</tt>
	 * @throws AppException
	 *
	 * @param clavePyme <tt>String</tt> con el id de la PYME
	 * @param claveProducto <tt>String</tt> con la clave del producto, por ejemplo: 1 (DESCUENTO ELECTRONICO).
	 *
	 * @return <tt>ArrayList</tt> de HashMap, donde cada HashMap contiene lo siguiente:
	 *				<ul>
	 *					<li>claveEpo: 	Clave de la EPO <tt>ic_epo</tt></li>
	 *					<li>nombreEpo:	Nombre Comercial de la EPO</li>
	 *				</ul>
	 *
	 */
   private ArrayList getListaEpos(String clavePyme, String claveProducto)
		throws AppException{

		log.info("getListaEpos(E)");

		AccesoDB 			con 			= new AccesoDB();
		StringBuffer 		query 		= new StringBuffer();
		PreparedStatement	ps 			= null;
		ResultSet			rs				= null;

		ArrayList			listaEpos 	= new ArrayList();
		try {

			con.conexionDB();

			query.append(
				" SELECT "  +
				"   pe.ic_epo             as claveEpo, "  +
				"   e.cg_nombre_comercial as nombreEpo "  +
				" FROM "  +
				"   comrel_pyme_epo     pe, "  +
				"   comcat_epo          e,  "  +
				"   comrel_producto_epo pre "  +
				" WHERE pe.ic_epo 				= e.ic_epo "   +
				" AND pe.ic_epo 					= pre.ic_epo " +
				" AND pe.ic_pyme 					= ? "   +
				" AND e.cs_habilitado 			= ? "   +
				" AND pe.cs_aceptacion 			= ? "   +
				" AND pre.ic_producto_nafin 	= ? " +
			//	" AND ( pre.ic_modalidad IS NULL OR pre.ic_modalidad = ?) " +
				" ORDER BY e.cg_nombre_comercial "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,		Integer.parseInt(clavePyme));
			ps.setString(2, 	"S");
			ps.setString(3, 	"H");
			ps.setInt(4, 		Integer.parseInt(claveProducto));
			rs = ps.executeQuery();

			String 	nombreEpo	= null;
			String 	claveEpo		= null;
			HashMap	registro		= null;
			while(rs.next()){

				registro = new HashMap();

				claveEpo 	= rs.getString("claveEpo");
				nombreEpo 	= rs.getString("nombreEpo");
				nombreEpo	= nombreEpo == null || nombreEpo.trim().equals("")?"EPO "+claveEpo+" SIN NOMBRE":nombreEpo;

				registro.put("claveEpo",  claveEpo);
				registro.put("nombreEpo", nombreEpo);

				listaEpos.add(registro);

			}

		}catch(Exception e){

			log.error("getListaEpos(Exception)");
			log.error("getListaEpos.clavePyme     = <" + clavePyme     + ">");
			log.error("getListaEpos.claveProducto = <" + claveProducto + ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Error al obtener la lista de EPOs con documentos.");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getListaEpos(S)");
		}

		return listaEpos;

	}

	/**
	 *
	 * Determina si la EPO con id <tt>claveEpo</tt> publico notas de credito negociables para la PYME
	 * con id <tt>clavePyme</tt> para una moneda en especifico.
	 * @throws AppException
	 *
	 * @param claveEpo <tt>String</tt> con el id (<tt>ic_epo</tt>) de la EPO.
	 * @param clavePyme <tt>String</tt> con el id (<tt>ic_pyme</tt>) de la PYME.
	 * @param claveMoneda <tt>String</tt> con el id (<tt>ic_moneda</tt>) de la moneda.
	 *
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si se encontraron notas de credito negociables.
	 *         Devuelve <tt>false</tt> en caso contrario.
	 *
	 */
	private boolean hayNotasDeCreditoNegociables(String claveEpo, String clavePyme, String claveMoneda)
		throws AppException{

		log.info("hayNotasDeCreditoNegociables(E)");

		boolean 				hayNotasDeCredito	= false;

		AccesoDB 			con 					= new AccesoDB();
		StringBuffer 		query 				= new StringBuffer();
		PreparedStatement	ps 					= null;
		ResultSet			rs						= null;

		try {

			con.conexionDB();

			query.append(
				" SELECT "   +
				"		DECODE(COUNT(1),0,'false','true') as HAY_NOTAS "  +
				"   FROM "  +
				"		com_documento    "   +
				"  WHERE ic_epo 				= ? " +
				"    AND ic_estatus_docto 	= ? "   +
				"    AND ic_pyme 				= ? " +
				"    AND cs_dscto_especial = ? "   +
				"    AND ic_moneda 			= ? "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,		Integer.parseInt(claveEpo)								);
			ps.setInt(2, 		SeleccionMovilDoctosBean.DOCUMENTO_NEGOCIABLE	);
			ps.setInt(3,   	Integer.parseInt(clavePyme)							);
			ps.setString(4, 	SeleccionMovilDoctosBean.NOTA_CREDITO				);
			ps.setInt(5, 		Integer.parseInt(claveMoneda));
			rs = ps.executeQuery();

			if(rs.next()){
				hayNotasDeCredito = "true".equals(rs.getString("HAY_NOTAS"))?true:false;
			}

		}catch(Exception e){

			log.error("hayNotasDeCreditoNegociables(Exception)");
			log.error("hayNotasDeCreditoNegociables.claveEpo    = <" + claveEpo+ ">");
			log.error("hayNotasDeCreditoNegociables.clavePyme   = <" + clavePyme+ ">");
			log.error("hayNotasDeCreditoNegociables.claveMoneda = <" + claveMoneda+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Error al validar si la EPO "+claveEpo+" tiene publicadas notas de cr�dito negociables.");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("hayNotasDeCreditoNegociables(S)");
		}

		return hayNotasDeCredito;

	}

	/**
	 *
	 * Obtiene el total y el monto de los documentos negociables publicados por la EPO con <tt> ic_epo =
	 * claveEpo<tt> para la PYME con <tt>ic_pyme = clavePyme</tt>.
	 * @throws AppException
	 *
	 * @param claveEpo <tt>String</tt> con la clave de la EPO.
	 * @param clavePyme <tt>String</tt> con la clave de la PYME.
	 * @param claveMoneda <tt>String</tt> con la clave de la moneda con que fueron publicados los doctos.
	 *
	 * @return <tt>HashMap</tt> con los siguientes valores:
	 *				<ul>
	 *					<li>(String "montoTotal", 			BigDecimal con el monto total de los documentos)</li>
	 *					<li>(String "numeroDocumentos", 	Integer    con el numero de documentos negociables)</li>
	 *				</ul>
	 *
	 */
	private HashMap getResumenDoctosNegociablesPorEpo(String claveEpo, String clavePyme, String claveMoneda)
		throws AppException{

		log.info("getResumenDoctosNegociablesPorEpo(E)");

		AccesoDB 			con 					= new AccesoDB();
		StringBuffer 		query 				= new StringBuffer();
		PreparedStatement	ps 					= null;
		ResultSet			rs						= null;

		String 				montoTotal			= null;
		String 				numeroDocumentos  = null;

		HashMap				resumen				= new HashMap();

		try {

			con.conexionDB();

			// 1. Consultar monto de las notas de credito negociables
			query.setLength(0);
			query.append(
				" SELECT NVL (SUM (fn_monto), 0) AS MONTO_NOTAS_CREDITO "  +
				"   FROM com_documento           "  +
				"  WHERE ic_epo            = ?   "  +
				"    AND ic_estatus_docto  = ?   "  +
				"    AND ic_pyme           = ?   "  +
				"    AND cs_dscto_especial = ?   "  +
				"    AND ic_moneda         = ?   "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveEpo));
			ps.setInt(2, 		SeleccionMovilDoctosBean.DOCUMENTO_NEGOCIABLE);
			ps.setInt(3, 		Integer.parseInt(clavePyme));
			ps.setString(4, 	SeleccionMovilDoctosBean.NOTA_CREDITO);
			ps.setInt(5, 		Integer.parseInt(claveMoneda));
			rs = ps.executeQuery();

			String montoNotasCredito = null;
			if(rs.next()){
				montoNotasCredito = rs.getString("MONTO_NOTAS_CREDITO");
			}
			montoNotasCredito = montoNotasCredito == null || montoNotasCredito.trim().equals("")?"0":montoNotasCredito;

			rs.close();
			ps.close();

			// 2. Consultar montos
			query.setLength(0);
			query.append(
				" SELECT "  +
				"	  COUNT (*)                   AS numeroDocumentos, "  +
				"    NVL (SUM (fn_monto), 0) - ? AS montoTotal        "  +
				"  FROM                         "  +
				"     com_documento             "  +
				"  WHERE ic_epo            = ?  "  +
				"    AND ic_estatus_docto  = ?  "  +
				"    AND ic_pyme           = ?  "  +
				"    AND cs_dscto_especial <> ? "  +
				"    AND ic_moneda         = ?  "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setBigDecimal(1, 	new BigDecimal(montoNotasCredito));
			ps.setInt(2, 			Integer.parseInt(claveEpo));
			ps.setInt(3, 			SeleccionMovilDoctosBean.DOCUMENTO_NEGOCIABLE);
			ps.setInt(4, 			Integer.parseInt(clavePyme));
			ps.setString(5,		SeleccionMovilDoctosBean.NOTA_CREDITO);
			ps.setInt(6, 			Integer.parseInt(claveMoneda));
			rs = ps.executeQuery();

			if(rs.next()){
				montoTotal 			= rs.getString("montoTotal");
				numeroDocumentos 	= rs.getString("numeroDocumentos");
			}
			montoTotal 				= montoTotal 			== null || montoTotal.trim().equals("")		?"0":montoTotal;
			numeroDocumentos 		= numeroDocumentos 	== null || numeroDocumentos.trim().equals("")?"0":numeroDocumentos;

			resumen.put("montoTotal",		  new BigDecimal(montoTotal)    );
			resumen.put("numeroDocumentos", new Integer(numeroDocumentos) );

		}catch(Exception e){

			log.error("getResumenDoctosNegociablesPorEpo(Exception)");
			log.error("getResumenDoctosNegociablesPorEpo.claveEpo    = <" + claveEpo    + ">");
			log.error("getResumenDoctosNegociablesPorEpo.clavePyme   = <" + clavePyme   + ">");
			log.error("getResumenDoctosNegociablesPorEpo.claveMoneda = <" + claveMoneda + ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Error al consultar resumen de documentos negociables para la EPO "+claveEpo);

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getResumenDoctosNegociablesPorEpo(S)");

		}

		return resumen;

	}

   /**
    *
    * Devuelve el Resumen de Documentos Negociables publicados a la PYME. S&oacute;lo se trae informaci&oacute;n de
	 * aquellas EPOs que tengan publicado al menos un documentos para la moneda indicada.
	 *
	 *
    * @param claveMoneda <tt>String</tt> con la clave de la Moneda (<tt>comcat_moneda.ic_moneda</tt>).
    *                    Valores permitidos:
    *                    <ul>
    *                      <li>Moneda Nacional: 1</li>
    *                      <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
    *
    * @param clavePyme <tt>String</tt> con la clave de la PYME (<tt>comcat_pyme.ic_pyme</tt>). N&uacute;mero entero mayor a cero.
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br><br>
	 *			 Tabla 1. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"permisoSeleccionDocumentos"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si la PYME tiene permiso o no de seleccionar documentos.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensajePyme"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt>para indicar que hay un mensaje para la PYME y <tt>"false"</tt> en caso contrario.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensajePyme"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje PYME. Este campo s&oacute;lo tiene valor si <tt>"hayMensajePyme" == "true"</tt>, en caso contrario viene como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> para indicar que se produjo un error inesperado, la descripci&oacute;n del error se env&iacute;a en <tt>"mensajePyme"</tt>, y adem&aacute;s <tt>"listaResumenPorEPO"</tt> se env&iacute;a vac&iacute;a. Si no hay error se env&iacute;a en <tt>"false"</tt>.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                	la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"listaResumenPorEPO"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td><tt>ArrayList</tt> de <tt>HashMap</tt> con los resumenes de los documentos por EPO.<br>Para ver una descripcion del contenido de los <tt>HashMap</tt> ver Tabla 2.</td>
	 *					</tr>
	 *			  </table>
    * 			<br>
	 *			  Tabla 2. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList listaResumenPorEPO.</tt><br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"claveEpo"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>ID(<tt>comcat_epo.ic_epo</tt>) de la EPO.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombreEpo"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Nombre Comercial de la EPO.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"montoTotal"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Monto Total de los documentos negociables.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"montoTotalFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Monto Total de los documentos negociables con formato de moneda.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numeroDocumentos"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Numero Total de documentos negociables publicados por la EPO.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numeroDocumentosFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Numero Total de documentos negociables publicados por la EPO con separador de miles.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"esSeleccionable"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> para indicar si se pueden seleccionar o no los documentos.
	 *						 Si no es seleccionable el campo <tt>"mensajeEpo"</tt> mostrara la causa.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensajeEpo"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> para indicar si hay un mensaje que mostrar para esta seleccion.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensajeEpo"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje a mostrar. Este campo s&oacute;lo tiene contenido si <tt>"hayMensajeEpo" == "true"</tt>, en caso contrario viene como cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"fechaSiguienteDiaHabil"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Fecha del siguiente d&iacute;a h&aacute;bil de la EPO; la fecha viene con formato: <tt>dd/mm/aaaa</tt>; este campo
	 *						s&oacute;lo se env&iacute;a cuando la PYME accede al servicio de descuento fuera del horario y en caso de que se
	 *						trabaje bajo el esquema de  Factoraje 24hrs.
	 *						</td>
	 *					</tr>
	 *	 		  </table>
    */
   public HashMap getResumenDoctosNegociablesPorPyme(String clavePyme,String claveMoneda){

		log.info("getResumenDoctosNegociablesPorPyme(E)");
		log.debug("getResumenDoctosNegociablesPorPyme: Consultando el Resumen de documentos negociables con clave moneda "+claveMoneda+" para la PYME "+clavePyme);

      HashMap resumenDoctos = new HashMap();


		boolean 			permisoSeleccionDocumentos	= false;
		boolean 			hayMensajePyme					= false;
		StringBuffer	mensajePyme						= new StringBuffer();

		boolean			hayError							= false;
		ArrayList		listaResumenPorEPO			= new ArrayList();

      try {

		  // I. Realizar validaciones

        // 1. Validar que la clave de la pyme proporcionada sea un numero valido
        int icPyme = 0;
        try {

           icPyme = Integer.parseInt(clavePyme);
           if(icPyme < 1){
              throw new Exception();
           }

        }catch(Exception e){
           throw new AppException("La clave pyme no es v�lida.");
        }

        // 2. Validar que la clave de la Moneda se valida
        int icMoneda = 0;
        try {

           icMoneda = Integer.parseInt(claveMoneda);
           if(
               icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
               icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
            ){
              throw new Exception();
           }

        }catch(Exception e){
           throw new AppException("La clave moneda no es v�lida.");
        }

        // 3. Revisar que la PYME no se encuentre bloqueda
		  Afiliacion 			afiliacion 		= null;
		  try {
            afiliacion 							= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

        } catch(Exception e) {

           log.error("getResumenDoctosNegociablesPorPyme(EJB Exception)");
			  e.printStackTrace();

			  throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Afiliaci�n");

        }

        boolean esPymeBloqueda = false;
        try {

            esPymeBloqueda = afiliacion.pymeBloqueada(clavePyme,SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO);

            if(esPymeBloqueda){
               throw new AppException("No es posible seleccionar documentos debido a que la PYME se encuentra bloqueada");
            }

        } catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getResumenDoctosNegociablesPorPyme(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME se encuentra bloqueda");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{
				  throw e;
				}

        }

		 //Fodea 034-2014 (E)
		   String  esPEntidadGobierno = "N";
        try {

            esPEntidadGobierno = afiliacion.getPyme_EntidaGobierno(clavePyme);

            if("S".equals(esPEntidadGobierno)){
               throw new AppException("No es posible realizar el descuento del documento. Por favor comun�quese al Centro de Atenci�n a Clientes Cd. M�xico 50-89-61-07. Sin costo desde el interior al 01-800-NAFINSA (01-800-623-4672)");
            }

        } catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getResumenDoctosNegociablesPorPyme(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME es Entidad gobierno");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{
				  throw e;
				}

        }
		   //Fodea 034-2014 (S)

		  // II. Obtener lista de EPOs
		  // 1. Obtener instancia del EJB de SeleccionDocumento
		  ISeleccionDocumento	seleccionDocumento		= null;
		  try {

				seleccionDocumento 		= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

		  }catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(SeleccionDocumentoEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

		  }

        // 1. Leer lista de EPOs con documentos
        ArrayList 			listaEpos 							= getListaEpos(clavePyme,SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO);

		  HashMap 				registro 							= null;

		  String 				claveEpo 							= null;
		  String 				nombreEpo 							= null;

		  BigDecimal   		montoTotal							= null;
		  Integer				numeroDocumentos					= null;

		  final BigDecimal 	CERO									= new BigDecimal("0");
		  for(int idx = 0; idx < listaEpos.size(); idx++){

			  boolean		hayMensajeEpo						= false;
			  StringBuffer mensajeEpo							= null;

			  boolean		hayNotasDeCredito					= false;
			  boolean		esSeleccionable					= true;

			  // 1.1 Leer clave y nombre de la EPO
			  registro 	= (HashMap) listaEpos.get(idx);
			  claveEpo 	= (String) 	registro.get("claveEpo");
			  nombreEpo	= (String) 	registro.get("nombreEpo");
			  log.debug("getResumenDoctosNegociablesPorPyme: Consultando publicacion de documentos negociables para la EPO "+claveEpo+": "+nombreEpo);

			  // 1.2 Revisar si la EPO publico Notas de Credito negociables.
			  hayNotasDeCredito = hayNotasDeCreditoNegociables(claveEpo, clavePyme, claveMoneda);
			  if(hayNotasDeCredito){
				  esSeleccionable 	= false;
				  hayMensajeEpo 		= true;
				  mensajeEpo	 		= mensajeEpo == null?new StringBuffer():mensajeEpo;
				  mensajeEpo.append("Debido a que se encontraron Notas de Credito negociables, para poder seleccionar documentos se debe utilizar la interfaz web completa.");
				  log.debug("getResumenDoctosNegociablesPorPyme: EPO "+claveEpo+": " + mensajeEpo);
			  }
			  log.debug("getResumenDoctosNegociablesPorPyme: EPO "+claveEpo+": hayNotasDeCredito = "+hayNotasDeCredito);

			  // 1.3 Obtener resumen de documentos negociables: montoTotal y numeroDocumentos
			  HashMap resumenMonto 	= getResumenDoctosNegociablesPorEpo(claveEpo, clavePyme, claveMoneda);
			  montoTotal				= (BigDecimal) resumenMonto.get("montoTotal");
			  numeroDocumentos		= (Integer)    resumenMonto.get("numeroDocumentos");

			  log.debug("getResumenDoctosNegociablesPorPyme: EPO "+claveEpo+": montoTotal       = "+montoTotal.toPlainString());
			  log.debug("getResumenDoctosNegociablesPorPyme: EPO "+claveEpo+": numeroDocumentos = "+numeroDocumentos);

			  // 1.4 Validar que el monto de los documentos consultados sea mayor a cero
			  if(!hayNotasDeCredito && numeroDocumentos.intValue() > 0 && montoTotal.compareTo(CERO) <= 0){
				  esSeleccionable 	= false;
				  hayMensajeEpo 		= true;
				  mensajeEpo	 		= mensajeEpo == null?new StringBuffer():mensajeEpo;
				  mensajeEpo.append("El monto de los documentos debe ser mayor a cero.");
				  log.debug("getResumenDoctosNegociablesPorPyme: EPO "+claveEpo+": " + mensajeEpo);
			  }

			  // 1.5 Si hay documentos seleccionables h&aacute;bilitar bandera.
			  if(!hayNotasDeCredito && numeroDocumentos.intValue() > 0 && montoTotal.compareTo(CERO) > 0){
				  permisoSeleccionDocumentos = true;
			  }

			  // 1.6 Agregar los datos consultados a la respuesta
			  if( numeroDocumentos.intValue() > 0 ){

				  // Validar Horario de Descuento Electronico
				  String mensajeDiaHabilSiguiente 		= "";
				  String tipoServicio 						= "";
				  String fechaSiguienteDiaHabil			= "";
				  try{

						// Determina bajo que modalidad se encuentra el servicio de descuento electronico ofrecido por la epo
						// AB = abierto, SH="siguiente dia h&aacute;bil", excepcion en caso de que el servicio no este abierto
						tipoServicio = Horario.validarHorarioDescElec(Integer.parseInt(SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO), "PYME", claveEpo);

						// Si el descuento electronico parametrizado para la epo se encuentra bajo la modalidad de "siguiente dia h&aacute;bil"
						if("SH".equals(tipoServicio)){

							// Obtener fecha del siguiente dia h&aacute;bil
							int numeroDias =1;
							String fechaActual = Fecha.getFechaActual();
							while(!Horario.validarDiaSigHabil("PYME", claveEpo, numeroDias)){
								numeroDias +=1;
							}
							fechaSiguienteDiaHabil = Fecha.sumaFechaDias(fechaActual,numeroDias);

							// Validar que la epo y el if operen factoraje 24 hrs
							if (!seleccionDocumento.operaEpoFactoraje24hrs(claveEpo)){
								throw new AppException("La EPO seleccionada no Opera Factoraje 24hrs.");
							}

							// Agregar mensaje indicando que la operacion de descuento electronico sera realizada
							// en el siguiente dia h&aacute;bil
							mensajeDiaHabilSiguiente="Esta operaci�n ser� programada para el Siguiente D�a H�bil "+fechaSiguienteDiaHabil;

							if(!hayMensajeEpo){
								mensajeEpo	 		= new StringBuffer();
							}else{
								mensajeEpo.append("\n");
							}

							hayMensajeEpo			= true;
							mensajeEpo.append(mensajeDiaHabilSiguiente);

						}

				 } catch(Exception e) {

				 	 	hayMensajeEpo 		= true;
				 	 	mensajeEpo	 		= new StringBuffer();
						if(e instanceof NafinException){

							NafinException nafinException = (NafinException) e;
							if("SIST0001".equals(nafinException.getCodError())){

								log.error("getResumenDoctosNegociablesPorPyme(NafinException)");
								e.printStackTrace();

								mensajeEpo.append("Ocurri� un error al consultar el horario de servicio");

							}else{

								mensajeEpo.append(e.getMessage());

							}

						}else{

						  log.error("getResumenDoctosNegociablesPorPyme(Exception)");
						  e.printStackTrace();
						  mensajeEpo.append("Ocurri� un error al consultar el horario de servicio");

						}

						esSeleccionable = false;

				 }

				 // Agregar los datos
				 HashMap resumenEPO = new HashMap();
				 resumenEPO.put("claveEpo",  							claveEpo);
				 resumenEPO.put("nombreEpo", 							nombreEpo);
				 resumenEPO.put("montoTotal", 						montoTotal.toPlainString() );
				 resumenEPO.put("montoTotalFormateado",			"$"+Comunes.formatoDecimal(montoTotal.toPlainString(),2,true) 	);
				 resumenEPO.put("numeroDocumentos", 				numeroDocumentos.toString() );
				 resumenEPO.put("numeroDocumentosFormateado", 	Comunes.formatoDecimal(numeroDocumentos.toString(),0,true) 	);
				 resumenEPO.put("esSeleccionable", 					String.valueOf(esSeleccionable));
				 resumenEPO.put("hayMensajeEpo",						String.valueOf(hayMensajeEpo));
				 resumenEPO.put("mensajeEpo",							mensajeEpo == null?"":mensajeEpo.toString());
				 resumenEPO.put("fechaSiguienteDiaHabil",			fechaSiguienteDiaHabil);

				 listaResumenPorEPO.add(resumenEPO);
				 log.debug("getResumenDoctosNegociablesPorPyme: La EPO "+claveEpo+": "+nombreEpo+", se agrego a la lista de resumen.");

			  }else{
				 log.debug("getResumenDoctosNegociablesPorPyme: La EPO "+claveEpo+": "+nombreEpo+", no tiene documentos negociables.");
			  }

		  }

		  // III. Validaciones de salida

		  // 1 Validar que haya documentos seleccionables
		  if(listaResumenPorEPO.size() == 0){
			   hayMensajePyme 	= true;
				mensajePyme.append("No se encontraron documentos.");
				log.debug("getResumenDoctosNegociablesPorPyme: clavePyme "+clavePyme+": " + mensajePyme);
		  }else if(!permisoSeleccionDocumentos){
			   hayMensajePyme 	= true;
				mensajePyme.append("No hay documentos que puedan ser seleccionados.");
				log.debug("getResumenDoctosNegociablesPorPyme: clavePyme "+clavePyme+": " + mensajePyme);
		  }

      }catch(Throwable e){

			mensajePyme.setLength(0);
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getResumenDoctosNegociablesPorPyme(Exception)");
					log.error("getResumenDoctosNegociablesPorPyme.clavePyme   = <" + clavePyme   + ">");
					log.error("getResumenDoctosNegociablesPorPyme.claveMoneda = <" + claveMoneda + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				hayMensajePyme = true;
				mensajePyme.append(msg);

			}else{

				log.error("getResumenDoctosNegociablesPorPyme(Exception)");
				log.error("getResumenDoctosNegociablesPorPyme.clavePyme   = <" + clavePyme   + ">");
				log.error("getResumenDoctosNegociablesPorPyme.claveMoneda = <" + claveMoneda + ">");
				e.printStackTrace();

				hayMensajePyme	= true;
				mensajePyme.append("Ocurrio un error al consultar el Resumen de los documentos negociables.");
				hayError			= true;

         }

			// Adaptar respuesta enviada para que se imposibilite la seleccion de documentos.
			log.debug("getResumenDoctosNegociablesPorPyme: Se suprime permiso para seleccionar documentos. ");
			permisoSeleccionDocumentos	= false;
			listaResumenPorEPO			= new ArrayList();

      }finally {

			resumenDoctos.put("permisoSeleccionDocumentos",	String.valueOf(permisoSeleccionDocumentos)	);
			resumenDoctos.put("hayMensajePyme",					String.valueOf(hayMensajePyme)					);
			resumenDoctos.put("mensajePyme",						mensajePyme.toString()								);
			resumenDoctos.put("hayError",							String.valueOf(hayError)							);
			resumenDoctos.put("listaResumenPorEPO",			listaResumenPorEPO									);

         log.info("getResumenDoctosNegociablesPorPyme(S)");

      }

      return resumenDoctos;

   }

	// 2. METODOS CORRESPONDIENTES AL MODULO 2 { PANTALLA 5: ADMIN PYME - DESCONTAR ( SELECCION INTERMEDIARIO FINANCIERO ) }
	/**
	 * Devuelve la lista de los intermediarios financieros que operan con una EPO y PYME en especifico y para
	 * la moneda especificada.
	 *
	 * @param claveEpo <tt>String</tt> con el ID(<tt>comcat_epo.ic_epo</tt>) de la EPO. N&uacute;mero entero mayor a cero.
	 * @param clavePyme <tt>String</tt> con el ID(<tt>comcat_pyme.ic_pyme</tt>) de la PYME. N&uacute;mero entero mayor a cero.
	 * @param claveMoneda <tt>String</tt> con el ID(<tt>comcat_moneda.ic_moneda</tt> de la Moneda.
	 *                    Valores permitidos:
    *                    <ul>
    *                      <li>Moneda Nacional: 1</li>
    *                      <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
	 * @param fechaSiguienteDiaHabil <tt>String</tt> con la fecha del siguiente d&iacute;a h&aacute;bil de la EPO;
	 *                               la fecha debe tener el formato: <tt>dd/mm/aaaa</tt>; este campo
	 *                               s&oacute;lo se env&iacute;a cuando la PYME accede al servicio de descuento fuera
	 *                               del horario y en caso de que se trabaje bajo el esquema de
	 *                               Factoraje 24hrs.
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *				<br>
	 *			 Tabla 3. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"permisoSeleccionarIntermediarios"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay permiso o no para seleccionar un intermediario financiero.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"listaIntermediarios"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td><tt>ArrayList</tt> de <tt>HashMap</tt> con los datos de los intermediarios financieros. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>, ver Tabla 4.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje con la descripci&oacute;n de &eacute;ste se env&iacute;a en <tt>"mensaje"</tt>, adem&aacute;s <tt>"listaIntermediarios"</tt> se env&iacute;a vac&iacute;a.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                	la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *			</table>
	 * 		<br>
	 *			Tabla 4. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "listaIntermediarios".</tt><br>
	 *			<table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td colspan="2"><b>Llave</b></td>
	 *					<td colspan="2"><b>Valor</b></td>
	 *				</tr>
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Contenido</b></td>
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Descripci&oacute;n</b></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"claveIF"</td>
	 *					<td><tt>String</tt></td>
	 *               <td>ID(<tt>comcat_if.ic_if</tt>) de la Intermediario Financiero.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"nombreIF"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Nombre Comercial del Intermediario Financiero.</td>
	 *				</tr>
	 *			</table>
	 *
	 *
	 */
	public HashMap getListaIntermediariosFinancieros(String claveEpo, String clavePyme, String claveMoneda, String fechaSiguienteDiaHabil){

		log.info("getListaIntermediariosFinancieros(E)");
		HashMap 			intermediariosFinancieros 			= new HashMap();

		ArrayList		listaIntermediarios					= new ArrayList();
		boolean 			permisoSeleccionarIntermediarios = true;
		boolean			hayMensaje								= false;
		StringBuffer	mensaje									= null;
		boolean 			hayError									= false;

		try {

		  // I. Validaciones

        // 1. Validar que la clave de la epo proporcionada sea un numero valido
        int icEpo = 0;
        try {

           icEpo = Integer.parseInt(claveEpo);
           if(icEpo < 1){
              throw new Exception();
           }

        }catch(Exception e){
           throw new AppException("La clave EPO no es v�lida.");
        }

		  // 2. Validar que la clave de la pyme proporcionada sea un numero valido
        int icPyme = 0;
        try {

           icPyme = Integer.parseInt(clavePyme);
           if(icPyme < 1){
              throw new Exception();
           }

        }catch(Exception e){
           throw new AppException("La clave PYME no es v�lida.");
        }

        // 3. Validar que la clave de la Moneda se valida
        int icMoneda = 0;
        try {

           icMoneda = Integer.parseInt(claveMoneda);
           if(
               icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
               icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
            ){
              throw new Exception();
           }

        }catch(Exception e){
           throw new AppException("La clave de la moneda no es v�lida.");
        }

		  // 4. Validar que la fecha proporcionada si esta existe, sea valida
		  try {

           if(
              !"".equals(fechaSiguienteDiaHabil) && !Comunes.esFechaValida(fechaSiguienteDiaHabil,"dd/MM/yyyy")
           ){
              throw new Exception();
			  }

		  }catch(Exception e){
				throw new AppException("La fecha proporcionada no es v�lida, esta debe tener el formato: \"dd/mm/aaaa\" o venir como cadena vac�a.");
		  }

		  // II. Obtener Instancias de EJB

		  // 1. Obtener instancia del EJB de Seleccion de Documentos
		  ISeleccionDocumento seleccionDocumento = null;
		  try {
				seleccionDocumento 										= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

        } catch(Exception e) {

           log.error("getListaIntermediariosFinancieros(EJB Exception)");
			  e.printStackTrace();

			  throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

        }

		  // III. Consultar Intermediarios Financieros

		  // 1. Obtener lista de intermediarios financieros a partir de las tasas aceptadas
		  Vector vTasaAceptada = seleccionDocumento.ovgetTasaAceptada(
							clavePyme,
							claveEpo,
							claveMoneda,
							"",
							fechaSiguienteDiaHabil
			);
			String intermediariosFin = "0";
			for (int i= 0; i<vTasaAceptada.size(); i++) {
				Vector vDatos = (Vector) vTasaAceptada.get(i);
				if (i==0)
					intermediariosFin = 	vDatos.get(2).toString();
				else
					intermediariosFin += "," + vDatos.get(2).toString();
			}

			// 2. PROVEEDOR CON CREDITOS CON EL ESTATUS ? OPERADO VENCIDO?
			// => Para que se pueda descontar con ese IF si tiene un cr�dito vencido; al parecer esta seccion no se ocupa.
			intermediariosFin = seleccionDocumento.ovgetIntermediariosOperadoVenc(
					clavePyme,
					claveEpo,
					intermediariosFin
			);

			// 3. Obtener Lista de Intermediarios financieros relacionados
			if(intermediariosFin.equals("")){
				intermediariosFin = "null";
			}

			Vector vIntermediarios 	= seleccionDocumento.ovgetIntermediariosRelacionados(
				clavePyme,
				claveEpo,
				claveMoneda,
				intermediariosFin,
				"",
				SeleccionMovilDoctosBean.FACTORAJE_NORMAL
			);
			// 4. Agregar a la lista los intermediarios consultados
			for (int i = 0; i < vIntermediarios.size(); i++) {

				Vector 	vDatosIntermediarios = (Vector) vIntermediarios.get(i);
				HashMap 	registro					= new HashMap();

				registro.put("claveIF",	vDatosIntermediarios.get(0)); // claveIF|tipoPiso
				registro.put("nombreIF",vDatosIntermediarios.get(9));

				listaIntermediarios.add(registro);

			}

		}catch(Throwable e){

			mensaje = mensaje == null? new StringBuffer():mensaje;
			mensaje.setLength(0);

			if(e instanceof NafinException){

				NafinException nafinException = (NafinException) e;
				String 			msg				= nafinException.getMessage();
				if("SIST0001".equals(nafinException.getCodError())){ // Ocurrio un error inesperado

					log.error("getListaIntermediariosFinancieros(Exception)");
					log.error("getListaIntermediariosFinancieros.claveEpo    = <" + claveEpo    + ">");
					log.error("getListaIntermediariosFinancieros.clavePyme   = <" + clavePyme   + ">");
					log.error("getListaIntermediariosFinancieros.claveMoneda = <" + claveMoneda + ">");
					e.printStackTrace();

					msg 		= "Ocurri� un error al consultar la lista de Intermediarios Financieros.";
					hayError	= true;

				}

				hayMensaje	= true;
				mensaje.append(msg);

			}else if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getListaIntermediariosFinancieros(Exception)");
					log.error("getListaIntermediariosFinancieros.claveEpo    = <" + claveEpo    + ">");
					log.error("getListaIntermediariosFinancieros.clavePyme   = <" + clavePyme   + ">");
					log.error("getListaIntermediariosFinancieros.claveMoneda = <" + claveMoneda + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError	= true;

				}

				hayMensaje = true;
				mensaje.append(msg);

			}else{ // Ocurrio un error inesperado

				log.error("getListaIntermediariosFinancieros(Exception)");
				log.error("getListaIntermediariosFinancieros.claveEpo    = <" + claveEpo    + ">");
				log.error("getListaIntermediariosFinancieros.clavePyme   = <" + clavePyme   + ">");
				log.error("getListaIntermediariosFinancieros.claveMoneda = <" + claveMoneda + ">");
				e.printStackTrace();

				hayMensaje	= true;
				mensaje.append("Ocurrio un error al consultar la lista de intermediarios financieros.");
				hayError		= true;

         }

			// Adaptar respuesta enviada para que se imposibilite la seleccion de documentos.
			log.debug("getListaIntermediariosFinancieros: Se suprime permiso de seleccion de intermediario. ");
			permisoSeleccionarIntermediarios	= false;
			listaIntermediarios					= new ArrayList();


		}finally{

			intermediariosFinancieros.put("permisoSeleccionarIntermediarios",	String.valueOf(permisoSeleccionarIntermediarios) 	);
			intermediariosFinancieros.put("listaIntermediarios",					listaIntermediarios											);
			intermediariosFinancieros.put("hayMensaje",								String.valueOf(hayMensaje)									);
			intermediariosFinancieros.put("mensaje",									mensaje == null?"":mensaje.toString()		         );
			intermediariosFinancieros.put("hayError",									String.valueOf(hayError)									);

			log.info("getListaIntermediariosFinancieros(S)");
		}

		return intermediariosFinancieros;

	}

	// 3. METODOS CORRESPONDIENTES AL MODULO 3 { PANTALLA 6: ADMIN PYME - DESCONTAR ( SELECCION DOCUMENTOS ) }
	/**
	 *
	 * Devuelve el Tipo de Piso del Intermediario Financiero.
	 * @throws AppException
	 *
	 * @param claveIF ID(<tt>comcat_if.ic_if</tt>) del Intermediario Financiero.
	 *                Numero entero mayor a cero.
	 *
	 * @return <tt>String</tt> con el tipo de piso.
	 *
	 */
	private String getTipoPiso(String claveIF)
		throws AppException{

		log.info("getTipoPiso(E)");
		String 					tipoPiso = null;

		AccesoDB 				con		= new AccesoDB();
		PreparedStatement		ps			= null;
		ResultSet				rs			= null;
		StringBuffer			query 	= null;

		try {

			con.conexionDB();

			query = new StringBuffer();
			query.append(
				"SELECT          "  +
				"   IG_TIPO_PISO "  +
				"FROM            "  +
				"   COMCAT_IF    "  +
				"WHERE           "  +
				"   IC_IF = ?    "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveIF));
			rs = ps.executeQuery();

			if(rs.next()){
				tipoPiso = rs.getString("IG_TIPO_PISO").trim();
			}

		}catch(Exception e){

			log.error("getTipoPiso(Exception)");
			log.error("getTipoPiso.claveIF = <" + claveIF+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar el tipo de piso");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getTipoPiso(S)");

		}

		return tipoPiso;

	}

	/**
	 *
	 * Obtiene el resumen de los documentos negociables publicados por la EPO (<tt>claveEpo</tt>) a la PYME
	 * (<tt>clavePyme</tt>) para una moneda y con un Intermediario Financiero en espec&iacute;fico .
	 *
	 * @param claveEpo <tt>String</tt> con el id(<tt>comcat_epo.ic_epo</tt>) de la EPO. N&uacute;mero entero mayor a cero.
	 * @param claveIF <tt>String</tt> con el id(<tt>comcat_if.ic_if</tt>) del IF. N&uacute;mero entero mayor a cero.
	 * @param clavePyme <tt>String</tt> con el id(<tt>comcat_pyme.ic_pyme</tt>) de la PYME. N&uacute;mero entero mayor a cero.
	 * @param claveMoneda <tt>String</tt> con el id(<tt>comcat_moneda.ic_moneda</tt> de la Moneda.
	 *                    Valores permitidos:
    *                    <ul>
    *                      <li>Moneda Nacional: 1</li>
    *                      <li>D&oacute;lar Americano: 54</li>
    *                    </ul>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *				<br>
	 *			 Tabla 5. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"permisoSeleccionDocumentos"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay permiso o no para seleccionar documentos.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"listaResumenDoctos"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td><tt>ArrayList</tt> de <tt>HashMap</tt> con los datos de los documentos negociables agrupados por tasa. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>, ver Tabla 6.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensajeSeleccion"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensajeSeleccion"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensajeSeleccion" == "true"</tt>, en caso contrario se env&iacute;a como un <tt>ArrayList</tt> vac&iacute;o.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"fechaSiguienteDiaHabil"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Fecha del siguiente d&iacute;a h&aacute;bil de la EPO; la fecha viene con el siguiente formato: <tt>dd/mm/aaaa</tt>. Este campo s&oacute;lo trae
	 *                contenido cuando se opera en el modo Factoraje 24 hrs, en caso contrario este campo viene como cadena vac&iacute;a. El env&iacute;o de
	 *                este campo es obligatorio si se selecciona para descuento al menos un grupo de documentos.
	 *                </td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje
	 *                	con la descripci&oacute;n de &eacute;ste se env&iacute;a en <tt>"mensajeSeleccion"</tt>(s&oacute;lo se env&iacute;a el mensaje
	 *                	del error), adem&aacute;s <tt>"listaResumenDoctos"</tt>, <tt>"fechaSiguienteDiaHabil"</tt> se env&iacute;an vac&iacute;os,
	 *                	"permisoSeleccionDocumentos" se pone en "false".
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                   la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *                </td>
	 *					</tr>
	 *			</table>
	 * 		<br>
	 *			Tabla 6. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "listaResumenDoctos".</tt><br>
	 *			<table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td colspan="2"><b>Llave</b></td>
	 *					<td colspan="2"><b>Valor</b></td>
	 *				</tr>
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Contenido</b></td>
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Descripci&oacute;n</b></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"plazo"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Descripci&oacute;n del plazo de los documentos.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"totalDoctos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N&uacute;mero total de los documentos agrupados por tasa.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"totalDoctosFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N&uacute;mero total de los documentos agrupados por tasa, con separador de miles.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importe"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe total de los documentos agrupados por tasa.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importeFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe total de los documentos agrupados por tasa, con formato de moneda.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importeRecibir"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe total a recibir de los documentos agrupados por tasa.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importeRecibirFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe total a recibir de los documentos agrupados por tasa, con formato de moneda.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"tasa"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Valor de la Tasa.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"idsDocumentos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Lista de los ID(com_documento.ic_documento) de los documentos separados por el caracter ';'.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"submitString"</td>
	 *					<td><tt>String</tt></td>
	 *             <td><tt>String</tt> que se enviar� en caso de que los documentos agrupados sean seleccionados para descuento.<br>
	 *                <br>
	 *                El "submitString" contiene los siguientes campos separados por pipes en el siguiente orden:<br>
	 *						<ol>
	 *							<li>"totalDoctos"</li>
	 *							<li>"importe"</li>
	 *							<li>"importeRecibir"</li>
	 *							<li>"tasa"</li>
	 *							<li>"idsDocumentos"</li>
	 *						</ol>
	 *             </td>
	 *				</tr>
	 *			</table>
	 */
	public HashMap getDocumentosNegociablesAgrupadosPorTasa(String claveEpo, String claveIF, String clavePyme, String claveMoneda){

		log.info("getDocumentosNegociablesAgrupadosPorTasa(E)");

		HashMap        resumenDoctos    				= new HashMap();

		ArrayList   	mensajeSeleccion           = new ArrayList();
      boolean        hayMensajeSeleccion        = false;
      boolean        hayError                   = false;
      boolean        permisoSeleccionDocumentos = false;
		String 			fechaSiguienteDiaHabil 		= "";
      ArrayList      listaResumenDoctos         = new ArrayList();

		try {

			// I. VALIDACIONES:

			// 1. Validar que la clave de la epo proporcionada sea un numero valido
			int icEpo = 0;
			try {

           icEpo = Integer.parseInt(claveEpo);
           if(icEpo < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave EPO no es v�lida.");
			}

			// 2. Validar que la clave del Intermediario Financiero proporcionado sea un numero valido
			int icIF = 0;
			try {

           icIF = Integer.parseInt(claveIF);
           if(icIF < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave Intermediario Financiero no es v�lida.");
			}

			// 3. Validar que la clave de la pyme proporcionada sea un numero valido
			int icPyme = 0;
			try {

           icPyme = Integer.parseInt(clavePyme);
           if(icPyme < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave PYME no es v�lida.");
			}

			// 4. Validar que la clave de la Moneda se valida
			int icMoneda = 0;
			try {

           icMoneda = Integer.parseInt(claveMoneda);
           if(
               icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
               icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
            ){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave de la moneda no es v�lida.");
			}

			// II. Obtener instancias de EJB

			// 1. Obtener instancia del EJB de SeleccionDocumento
			ISeleccionDocumento	seleccionDocumento		= null;
			try {
				seleccionDocumento 		= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

			}catch(Exception e){

				log.error("getDocumentosNegociablesAgrupadosPorTasa(SeleccionDocumentoEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

			}

			// 2. Obtener instancia del EJB de Afiliacion
			Afiliacion afiliacion = null;
			try {
				afiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

			}catch(Exception e){

				log.error("getDocumentosNegociablesAgrupadosPorTasa(AfiliacionEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Afiliaci�n");

			}

			// 3. Obtener instancia del EJB de Parametros Descuento
			ParametrosDescuento parametrosDescuento = null;
			try {
				parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			}catch(Exception e){

				log.error("getDocumentosNegociablesAgrupadosPorTasa(ParametrosDescuentoEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Parametros Descuento");

			}

			// II. Validar que se cumplan las condiciones para seleccionar documentos

			// 1. Verificar si la pyme esta bloqueada para el producto descuento electronico
			boolean esPymeBloqueda = false;
			try {

				esPymeBloqueda = afiliacion.pymeBloqueada(clavePyme,SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO);

				if(esPymeBloqueda){
					throw new AppException("No es posible seleccionar documentos debido a que la PYME se encuentra bloqueada");
				}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME se encuentra bloqueda.");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

		 //Fodea 034-2014 (E)
		   String  esPEntidadGobierno = "N";
        try {

            esPEntidadGobierno = afiliacion.getPyme_EntidaGobierno(clavePyme);

            if("S".equals(esPEntidadGobierno)){
               throw new AppException("No es posible realizar el descuento del documento. Por favor comun�quese al Centro de Atenci�n a Clientes Cd. M�xico 50-89-61-07. Sin costo desde el interior al 01-800-NAFINSA (01-800-623-4672)");
            }

        } catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getResumenDoctosNegociablesPorPyme(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME es Entidad gobierno");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{
				  throw e;
				}

        }
		   //Fodea 034-2014 (S)


			// 2. Validar Horario de Descuento Electronico
			String mensajeDiaHabilSiguiente 		= "";
			String tipoServicio 						= "";
			try{

				// Determina bajo que modalidad se encuentra el servicio de descuento electronico ofrecido por la epo
				// AB = abierto, SH="siguiente dia h&aacute;bil", excepcion en caso de que el servicio no este abierto
				tipoServicio = Horario.validarHorarioDescElec(Integer.parseInt(SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO), "PYME", claveEpo);

				// Si el descuento electronico parametrizado para la epo se encuentra bajo la modalidad de "siguiente dia h&aacute;bil"
				if("SH".equals(tipoServicio)){

					// Obtener fecha del siguiente dia h&aacute;bil
					int numeroDias =1;
					String fechaActual = Fecha.getFechaActual();
					while(!Horario.validarDiaSigHabil("PYME", claveEpo, numeroDias)){
						numeroDias +=1;
					}
					fechaSiguienteDiaHabil = Fecha.sumaFechaDias(fechaActual,numeroDias);

					// Validar que la epo y el if operen factoraje 24 hrs
					if (!seleccionDocumento.operaEpoFactoraje24hrs(claveEpo)){
						throw new AppException("La EPO seleccionada no Opera Factoraje 24hrs.");
					}else if (!seleccionDocumento.operaIfFactoraje24hrs(claveIF)){
						throw new AppException("El Intermediario Financiero seleccionado no Opera Factoraje 24hrs.");
					}

					// Agregar mensaje indicando que la operacion de descuento electronico sera realizada
					// en el siguiente dia h&aacute;bil
					mensajeDiaHabilSiguiente="Esta operaci�n ser� programada para el Siguiente D�a H�bil "+fechaSiguienteDiaHabil;
					mensajeSeleccion.add(mensajeDiaHabilSiguiente);

				}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar el horario de servicio");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 3. Revisar si la EPO seleccionada Opera con Fecha de Vencimiento PYME
			boolean 	operaFechaVencimientoPyme 		= false;
			String 	mensajeFechaVencimientoPyme 	= "";
			try {

					// Revisar si la epo seleccionada opera con fecha de vencimiento pyme para el producto descuento electronico
					// si la epo opera con fecha de vencimiento pyme, seleccionDocumento.operaFechaVencPyme = "_pyme", en
					// caso contrario seleccionDocumento.operaFechaVencPyme = ""
					operaFechaVencimientoPyme = !"".equals(seleccionDocumento.operaFechaVencPyme(claveEpo))?true:false;

					// Si es asi, avisar que para las operaciones seleccionadas el intermediario financiero cobrara una comision
					if(operaFechaVencimientoPyme) {
						mensajeFechaVencimientoPyme = "Importante: Las operaciones seleccionadas se les aplicar� una comisi�n por parte del Intermediario Financiero con el que realice la transacci�n.";
						mensajeSeleccion.add(mensajeFechaVencimientoPyme);
					}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar la operaci�n con fecha de vencimiento");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// III. CONSULTAR DOCUMENTOS

			// 1. Obtener todas las tasas aceptadas
			Vector 		vTasaPlazoIF 	= new Vector();
			try {

				Vector 	vTasaAceptada 	= new Vector();

				// Leer valor de la tasa aceptada para la epo, cliente, moneda, intermediario financiero y siguiente dia h&aacute;bil parametrizado
				vTasaAceptada = seleccionDocumento.ovgetTasaAceptada(clavePyme, claveEpo, claveMoneda, claveIF, fechaSiguienteDiaHabil);

				// Leer tasas aceptadas para el intermediario: leer tasa plazo para el if seleccionado
				for (int i= 0; i<vTasaAceptada.size(); i++) {
					Vector vDatos = (Vector) vTasaAceptada.get(i);
					if (claveIF.equals(vDatos.get(2).toString())) {
						// Leer tasa plazo para el if seleccionado
						vTasaPlazoIF.addElement(vDatos);
					}
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los plazos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 2. Revisar si la busqueda excede o es igual a una fecha limite
			Vector 	vDocumentosDescontar = new Vector();
			boolean	fechaLimiteVencida	= false;
			try{

				// Si se ha vencido la fecha limite no se consultan los documentos
				fechaLimiteVencida =  "S".equals(parametrosDescuento.getValidarFechaLimite( claveEpo, claveIF ))?true:false;
				// Si es as�, cancelar consulta de documentos y h&aacute;bilitar bandera indicando que la linea de credito ha vencido
				if(fechaLimiteVencida){

					throw new NafinException("DSCT0103");

				// Consultar los documentos a descontar
				}else{

					// Si no se ha vencido la fecha limite, consultar los documentosa descontar
					String txtFechaVencDe	= "";	// Inicio fecha de vencimiento
					String txtFechaVenca 	= "";	// Fin fecha de vencimiento

					String sTipoPiso 			= getTipoPiso(claveIF); // comcat_if.ig_tipo_piso
					vDocumentosDescontar 	= seleccionDocumento.ovgetDocumentosDescontar(
								clavePyme,  		// Clave de la pyme
								claveMoneda,  		// Clave de la moneda
								claveEpo,  			// Clave de la epo
								vTasaPlazoIF,  	// Tasa plazo if
								txtFechaVencDe, 	// Inicio fecha de vencimiento
								txtFechaVenca, 	// Fin fecha de vencimiento
								claveIF, 			// Clave del intermediario financiero
								sTipoPiso			// Tipo de piso
							);
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los documentos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// IV. AGRUPAR DOCUMENTOS POR TASA

			// 1. Agrupar por Tasa los documentos consultados
			int 		i   					= 0;
			boolean 	documentoValido 	= false;
			try {

				// Si hay documentos h&aacute;bilitar permiso para seleccionarlos
				permisoSeleccionDocumentos = vDocumentosDescontar.size() > 0 ? true:false;

				// Iterar sobre los documentos consultados
				HashMap documetosAgrupados = new HashMap();
				for (i = 0; i < vDocumentosDescontar.size(); i++) {

					documentoValido 							= false;
					Vector 	vDatosDocumentosDescontar 	= (Vector) vDocumentosDescontar.get(i); // Leer datos del documento a descontar

					// Si el documento no es vencido sin operar para el dia h&aacute;bil siguiente, entonces el documento es valido
					// y se puede seleccionar para descuento
					if(!seleccionDocumento.getDocVencSinOperDiaSigHabil(vDatosDocumentosDescontar.get(9).toString(),claveEpo,fechaSiguienteDiaHabil)){
						documentoValido = true;
					}

					if(documentoValido){

						String 	tasa			= vDatosDocumentosDescontar.get(13).toString();
						boolean 	existeGrupo = documetosAgrupados.get(tasa) != null?true:false;

						if(!existeGrupo){

							HashMap		grupo 			= new HashMap();

							String 		plazo				= vDatosDocumentosDescontar.get(3).toString();
							Integer 		totalDoctos		= new Integer("1");
							BigDecimal 	importe			= new BigDecimal(vDatosDocumentosDescontar.get(5).toString());
							BigDecimal 	importeRecibir = new BigDecimal(vDatosDocumentosDescontar.get(8).toString());
							String 		idsDocumentos	= vDatosDocumentosDescontar.get(9).toString();

							grupo.put("plazo", 				plazo				);
							grupo.put("totalDoctos",		totalDoctos		);
							grupo.put("importe",				importe			);
							grupo.put("importeRecibir",	importeRecibir	);
							grupo.put("tasa",					tasa				);
							grupo.put("idsDocumentos",		idsDocumentos	);

							documetosAgrupados.put(tasa,grupo);

						}else{

							HashMap		grupo 					= (HashMap) documetosAgrupados.get(tasa);

							// Puesto para un plazo s&oacute;lo existe una tasa, la siguiente linea no se ocupa
							// String 	plazo			= vDatosDocumentosDescontar.get(3).toString();

							Integer  	totalDoctos	 			= (Integer)grupo.get("totalDoctos");
							totalDoctos					 			= new Integer(totalDoctos.intValue() + 1);

							BigDecimal 	importeGrupo 			= (BigDecimal)grupo.get("importe");
							BigDecimal  importeDocto 			= new BigDecimal( vDatosDocumentosDescontar.get(5).toString());
							importeGrupo				 			= importeGrupo.add(importeDocto);

							BigDecimal 	importeRecibirGrupo 	= (BigDecimal)grupo.get("importeRecibir");
							BigDecimal  importeRecibirDocto 	= new BigDecimal( vDatosDocumentosDescontar.get(8).toString() );
							importeRecibirGrupo				 	= importeRecibirGrupo.add(importeRecibirDocto);

							String 		idsDocumentos 			= (String) grupo.get("idsDocumentos");
							idsDocumentos							= idsDocumentos + ";" +vDatosDocumentosDescontar.get(9).toString();

							// El plazo siempre es el mismo para todos
							// grupo.put("plazo", 				plazo				);
							grupo.put("totalDoctos",			totalDoctos 				);
							grupo.put("importe",					importeGrupo				);
							grupo.put("importeRecibir",		importeRecibirGrupo		);
							// La tasa siempre es la misma para todos los documentos del grupo
							// grupo.put("tasa",						tasa				);
							grupo.put("idsDocumentos",			idsDocumentos				);

						}

					}

				}

				// 2. Convertir a String los objetos: totalDoctos, importe y importeRecibir; construir submit string.
				Iterator iterator = documetosAgrupados.keySet().iterator();
				while (iterator.hasNext()) {

               String      tasa           = (String)     iterator.next();
					HashMap 		grupo 			= (HashMap)    documetosAgrupados.get(tasa);

					Integer 		totalDoctos		= (Integer)		grupo.get("totalDoctos");
					BigDecimal 	importe 			= (BigDecimal)	grupo.get("importe");
					BigDecimal 	importeRecibir = (BigDecimal) grupo.get("importeRecibir");

					grupo.put("totalDoctos",					totalDoctos.toString()		);
					grupo.put("importe",							importe.toPlainString()			);
					grupo.put("importeRecibir",				importeRecibir.toPlainString()	);

					grupo.put("totalDoctosFormateado",		Comunes.formatoDecimal(totalDoctos.toString(),			0,true)	);
					grupo.put("importeFormateado",			"$"+Comunes.formatoDecimal(importe.toPlainString(),			2,true)	);
					grupo.put("importeRecibirFormateado",	"$"+Comunes.formatoDecimal(importeRecibir.toPlainString(),	2,true)	);

					String  		submitString	=
										grupo.get("totalDoctos")		+ "|" +
										grupo.get("importe")				+ "|" +
										grupo.get("importeRecibir") 	+ "|" +
										grupo.get("tasa") 				+ "|" +
										grupo.get("idsDocumentos");


					grupo.put("submitString",		submitString					);

				}

				// 3. Poner los documentos en un ArrayList
				iterator = documetosAgrupados.keySet().iterator();
				while (iterator.hasNext()) {
               String      tasa           = (String)  iterator.next();
					HashMap 		grupo 			= (HashMap) documetosAgrupados.get(tasa);
					listaResumenDoctos.add(grupo);
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al agrupar los documentos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

		}catch(Throwable e){

			mensajeSeleccion = new ArrayList();
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
					log.error("getDocumentosNegociablesAgrupadosPorTasa.claveEpo    = <" + claveEpo    + ">");
					log.error("getDocumentosNegociablesAgrupadosPorTasa.claveIF     = <" + claveIF     + ">");
					log.error("getDocumentosNegociablesAgrupadosPorTasa.clavePyme   = <" + clavePyme   + ">");
					log.error("getDocumentosNegociablesAgrupadosPorTasa.claveMoneda = <" + claveMoneda + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensajeSeleccion.add(msg);

			}else{

				log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
				log.error("getDocumentosNegociablesAgrupadosPorTasa.claveEpo    = <" + claveEpo    + ">");
				log.error("getDocumentosNegociablesAgrupadosPorTasa.claveIF     = <" + claveIF     + ">");
				log.error("getDocumentosNegociablesAgrupadosPorTasa.clavePyme   = <" + clavePyme   + ">");
				log.error("getDocumentosNegociablesAgrupadosPorTasa.claveMoneda = <" + claveMoneda + ">");
				e.printStackTrace();

				mensajeSeleccion.add("Ocurri� un error al consultar los documentos negociables.");
				hayError					= true;

         }

			// Adaptar respuesta enviada para que se imposibilite la seleccion de documentos.
			log.debug("getDocumentosNegociablesAgrupadosPorTasa: Se suprime permiso para seleccionar documentos. ");
			permisoSeleccionDocumentos	= false;
			listaResumenDoctos			= new ArrayList();
			fechaSiguienteDiaHabil		= "";

		}finally{

			hayMensajeSeleccion	= mensajeSeleccion.size() > 0?true:false;

			resumenDoctos.put("permisoSeleccionDocumentos",	String.valueOf(permisoSeleccionDocumentos)	);
			resumenDoctos.put("listaResumenDoctos",			listaResumenDoctos									);
			resumenDoctos.put("hayMensajeSeleccion",			String.valueOf(hayMensajeSeleccion)				);
			resumenDoctos.put("mensajeSeleccion",				mensajeSeleccion										);
			resumenDoctos.put("fechaSiguienteDiaHabil",		fechaSiguienteDiaHabil								);
			resumenDoctos.put("hayError",							String.valueOf(hayError)							);

			log.info("getDocumentosNegociablesAgrupadosPorTasa(S)");

		}

		return resumenDoctos;

	}

	// 4. METODOS CORRESPONDIENTES AL MODULO 4 { PANTALLA 7: ADMIN PYME - DESCONTAR ( ALERT DE DOCUMENTOS SELECCIONADOS ) }
	/**
	 * Calcula el n&uacute;mero e importe total de los documentos seleccionados a partir de un <tt>String Array</tt>.
	 * Tambi&eacute;n se determina si el importe total de los documentos es cero.
	 *
	 * @param submitString <tt>String Array</tt> con el resumen de los grupos de documentos que fueron seleccionados para descuento.<br>
	 *						Cada elemento de este <tt>String Array</tt> contiene los siguientes campos separados por pipes y en el siguiente orden:
	 *						<ol>
	 *							<li>totalDoctos: n&uacute;mero total de documentos.</li>
	 *							<li>importe: importe total de los documentos.</li>
	 *							<li>importeRecibir: importe total a recibir de los documentos.</li>
	 *							<li>tasa: valor de la tasa.</li>
	 *							<li>idsDocumentos: lista con los ID(<tt>com_documento.ic_documento</tt>) de los documentos; como separador se usa el caracter ';'.
	 *                   </li>
	 *						</ol>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			<br>
	 *			Tabla 7. Contenido de la Respuesta.<br>
	 *			<table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td colspan="2"><b>Llave</b></td>
	 *					<td colspan="2"><b>Valor</b></td>
	 *				</tr>
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Contenido</b></td>
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Descripci&oacute;n</b></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroTotalDocumentos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N&uacute;mero Total de Documentos</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroTotalDocumentosFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N&uacute;mero Total de Documentos con formato de miles</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importeTotalDocumentos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe Total de los Documentos</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"importeTotalDocumentosFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Importe Total de los Documentos con formato de moneda</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"montoTotalEsCero"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Si la suma total de los importes es cero viene en <tt>"true"</tt> en caso contrario viene en <tt>"false"</tt>.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"hayMensaje"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Si hay un mensaje que mostrar viene en <tt>"true"</tt>, en caso contrario, viene en <tt>"false"</tt></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"mensaje"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Mensaje a mostrar. S&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario viene como cadena vac&iacute;a.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"hayError"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Si se present&oacute; un error inesperado viene en <tt>"true"</tt>, en caso contrario, viene en <tt>"false"</tt>.
	 *						<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *					</td>
	 *				</tr>
	 *			</table>
	 */
	public HashMap getMensajeResumenDoctosSeleccionados(String[] submitString ){

		log.info("getMensajeResumenDoctosSeleccionados(E)");

		HashMap			totales										= new HashMap();

		BigDecimal		cero											= new BigDecimal("0");

		BigDecimal 		numeroTotalDocumentos 					= null;
		BigDecimal 		importeTotalDocumentos					= null;

		String 			numeroTotalDocumentosFormateado 		= null;
		String 			importeTotalDocumentosFormateado		= null;

		BigDecimal 		numeroDocumentos 							= null;
		BigDecimal 		importeDocumentos							= null;

		boolean 			montoTotalEsCero							= false;

		boolean			hayMensaje									= false;
		StringBuffer 	mensaje										= new StringBuffer();
		boolean			hayError										= false;

		try {

			// I. Validacion de parametros

			// Validar que se haya especificado un submitString con contenido
			if( submitString == null || submitString.length == 0){
				throw new AppException("No se especific� ning�n documento.");
			}

			// II. Obtener totales
			numeroTotalDocumentos		= new BigDecimal("0");
			importeTotalDocumentos		= new BigDecimal("0.00");
			for(int i=0;i<submitString.length;i++){

				//	1. Obtener los resumenes de los documentos seleccinados para los
				// cuales se realizara la suma.
				String datos[] 			= submitString[i].split("\\|");

				numeroDocumentos 			= new BigDecimal(datos[0]);
				importeDocumentos 		= new BigDecimal(datos[1]);

				//	2. Agrupar datos
				//		2.2 Sumar numero de documentos
				numeroTotalDocumentos 	= numeroTotalDocumentos.add(numeroDocumentos);
				//		2.1 Sumar montos por moneda
				importeTotalDocumentos 	= importeTotalDocumentos.add(importeDocumentos);

			}

			// III. Revisar si el monto es cero
			// montoTotalEsCero = true si el valor de la sumatoria para todas las monedas es cero
			montoTotalEsCero 						= ( cero.compareTo(importeTotalDocumentos) == 0 )?true:false;

			// IV. Formatear respuesta
			numeroTotalDocumentosFormateado	= Comunes.formatoDecimal(numeroTotalDocumentos.toPlainString(),			0,true);
			importeTotalDocumentosFormateado = "$"+Comunes.formatoDecimal(importeTotalDocumentos.toPlainString(),	2,true);

		}catch(Throwable e){

			mensaje.setLength(0);
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getMensajeResumenDoctosSeleccionados(Exception)");
					log.error("getMensajeResumenDoctosSeleccionados.submitString = <" + submitString+ ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensaje.append(msg);

			}else{

				log.error("getMensajeResumenDoctosSeleccionados(Exception)");
				log.error("getMensajeResumenDoctosSeleccionados.submitString = <" + submitString+ ">");
				e.printStackTrace();

				hayError 	= true;
				mensaje.append("Ocurri� un error al realizar el c�lculo de los totales de los documentos seleccionados");

			}

			hayMensaje 	= true;

			// Resetear los demas campos de respuesta
			numeroTotalDocumentos						= new BigDecimal("0");
			numeroTotalDocumentosFormateado			= "0";
			importeTotalDocumentos						= new BigDecimal("0.00");
			importeTotalDocumentosFormateado			= "$0.00";
			montoTotalEsCero								= false;

		}finally{

			// Construir respuesta

			//	1.	Numero Total Documentos.
			totales.put("numeroTotalDocumentos",				numeroTotalDocumentos.toPlainString());
			// 2. Numero Total Documentos con formato de miles.
			totales.put("numeroTotalDocumentosFormateado",	numeroTotalDocumentosFormateado);
			// 3. Importe Total de los Documentos.
			totales.put("importeTotalDocumentos",				importeTotalDocumentos.toPlainString());
			// 4. Importe Total de los Documentos con formato de moneda.
			totales.put("importeTotalDocumentosFormateado",	importeTotalDocumentosFormateado);
			// 5. String indicando si el monto total es cero.
			totales.put("montoTotalEsCero",						String.valueOf(montoTotalEsCero));
			// 6. String indicando si hay un mensaje.
			totales.put("hayMensaje",								String.valueOf(hayMensaje));
			// 7. String con el mensaje en caso de hubiera un mensaje.
			totales.put("mensaje",									mensaje.toString());
			// 8. String indican si se presento o no un error.
			totales.put("hayError",									String.valueOf(hayError));

			log.info("getMensajeResumenDoctosSeleccionados(S)");

		}

		return totales;

	}

	// 5. METODOS CORRESPONDIENTES AL MODULO 5 { PANTALLA 10 Y 8: ADMIN PYME - DESCONTAR ( RESUMEN DE LA OPERACION POR GRUPO DE DOCTOS Y CLAVE DE CONFIRMACION ) }
	/**
	 *
	 * Devuelve el resumen de los documentos seleccionados, as&iacute; como tambi&eacute;n, el detalle de un n&uacute;mero l&iacute;mite ({@link  #LIMITE_DOCTOS_MOSTRADOS})
	 * de los documentos que pertenecen a la selecci&oacute;n.
	 *
	 * @param claveEpo <tt>String</tt> con el ID(<tt>comcat_epo.ic_epo</tt>) de la EPO. N&uacute;mero entero mayor a cero.
	 * @param claveIF <tt>String</tt> con el ID(<tt>comcat_if.ic_if</tt>) del Intermediario Financiero. N&uacute;mero entero mayor a cero.
	 * @param clavePyme <tt>String</tt> con la clave de la PYME (<tt>comcat_pyme.ic_pyme</tt>). N&uacute;mero entero mayor a cero.
	 * @param claveMoneda <tt>String</tt> con la clave de la Moneda (<tt>comcat_moneda.ic_moneda</tt>).
    *                    Valores permitidos:
    *                    <ul>
    *                      <li>Moneda Nacional: 1</li>
    *                      <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
	 * @param fechaSiguienteDiaHabil <tt>String</tt> con la fecha del siguiente d&iacute;a h&aacute;bil de la EPO;
	 *                               la fecha debe tener el formato: <tt>dd/mm/aaaa</tt>; este campo
	 *                               s&oacute;lo se env&iacute;a cuando la PYME accede al servicio de descuento fuera
	 *                               del horario y en caso de que se trabaje bajo el esquema de
	 *                               Factoraje 24hrs.
	 * @param submitString <tt>String Array</tt> con el resumen de los grupos de documentos que fueron seleccionados para descuento.<br>
	 *						Cada elemento de este <tt>String Array</tt> contiene los siguientes campos separados por pipes y en el siguiente orden:
	 *						<ol>
	 *							<li>totalDoctos: n&uacute;mero total de documentos.</li>
	 *							<li>importe: importe total de los documentos.</li>
	 *							<li>importeRecibir: importe total a recibir de los documentos.</li>
	 *							<li>tasa: valor de la tasa.</li>
	 *							<li>idsDocumentos: lista con los ID(<tt>com_documento.ic_documento</tt>) de los documentos; como separador se usa el caracter ';'.
	 *                   </li>
	 *						</ol>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br><br>
	 *			 Tabla 8. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"permisoDescontar"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay permiso o no para descontar los documentos.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"detalleDoctosSeleccionados"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *             	<td><tt>ArrayList</tt> de <tt>HashMap</tt> con los datos de una cantidad registringida ({@link  #LIMITE_DOCTOS_MOSTRADOS})
	 *						de los documentos negociables seleccionados. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>,
	 *						ver Tabla 9.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>
	 *							<tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje
	 *                	con la descripci&oacute;n de &eacute;ste se env&iacute;a en el campo <tt>"mensaje"</tt>(s&oacute;lo se env&iacute;a el mensaje
	 *                	del error), adem&aacute;s <tt>"detalleDoctosSeleccionados"</tt> se env&iacute;a vac&iacute;o,
	 *                	"permisoDescontar" y "noSeMuestraDetalleDeTodosLosDoctos" se ponen en "false", los importes y el total se env&iacute;an en cero.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                   la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *                </td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalDocumentos"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>N&uacute;mero total de todos los documentos seleccionados.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeTotal"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe total de todos los documentos seleccionados.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeTotalRecibir"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe Total a Recibir de todos los documentos seleccionados.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalDocumentosFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>N&uacute;mero total de todos los documentos seleccionados con formato de miles.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeTotalFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe total de todos los documentos seleccionados con formato de moneda.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeTotalRecibirFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe Total a Recibir de todos los documentos seleccionados con formato de moneda.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"noSeMuestraDetalleDeTodosLosDoctos"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Si viene en <tt>"true"</tt>, indica que hay documentos cuyo detalle no se pudo presentar, debido a que
	 *						estos exceden la cantidad l&iacute;mite de documentos que se permite mostrar({@link #LIMITE_DOCTOS_MOSTRADOS});
	 *						viene en <tt>"false"</tt> en caso contrario.</td>
	 *					</tr>
	 *			  </table>
	 * 		  <br>
	 *			  Tabla 9. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "detalleDoctosSeleccionados".</tt><br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numeroDocto"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>N&uacute;mero del Documento. Este campo(<tt>com_documento.ig_numero_docto</tt>) es especificado por la EPO cuando publica el documento.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe del Documento con formato de moneda.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeRecibirFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe a Recibir del documento con formato de moneda.</td>
	 *					</tr>
	 *			  </table>
	 *
	 */
	public HashMap getResumenDeLaOperacion(
			String claveEpo,
			String claveIF,
			String clavePyme,
			String claveMoneda,
			String fechaSiguienteDiaHabil,
			String[] submitString
	){

		log.info("getResumenDeLaOperacion(E)");

		HashMap 			respuesta 									= new HashMap();
		ArrayList		detalleDoctosSeleccionados				= new ArrayList();
		boolean			hayError 									= false;
		boolean			hayMensaje 									= false;
		boolean			permisoDescontar							= false;
		StringBuffer	mensaje										= new StringBuffer();
		boolean			noSeMuestraDetalleDeTodosLosDoctos	= false;

		BigDecimal 		numeroDocumentos							= null;
		BigDecimal 		importeDocumentos							= null;
		BigDecimal 		importeRecibirDocumentos				= null;

		BigDecimal  	numeroTotalDocumentos					= null;
		BigDecimal 		importeTotalDocumentos					= null;
		BigDecimal		importeTotalRecibirDocumentos			= null;

		try {

			// I. Realizar validaciones de los campos proporcionados

			// 1. Validar que la clave de la epo proporcionada sea un numero valido
			int icEpo = 0;
			try {

           icEpo = Integer.parseInt(claveEpo);
           if(icEpo < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave EPO no es v�lida.");
			}

			// 2. Validar que la clave del Intermediario Financiero proporcionado sea un numero valido
			int icIF = 0;
			try {

           icIF = Integer.parseInt(claveIF);
           if(icIF < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave Intermediario Financiero no es v�lida.");
			}

			// 3. Validar que la clave de la pyme proporcionada sea un numero valido
			int icPyme = 0;
			try {

           icPyme = Integer.parseInt(clavePyme);
           if(icPyme < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave PYME no es v�lida.");
			}

			// 4. Validar que la clave de la Moneda se valida
			int icMoneda = 0;
			try {

           icMoneda = Integer.parseInt(claveMoneda);
           if(
               icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
               icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
            ){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave de la moneda no es v�lida.");
			}

			// 5. Validar la fecha del siguiente d�a h�bil
			try {

           if(
              !"".equals(fechaSiguienteDiaHabil) && !Comunes.esFechaValida(fechaSiguienteDiaHabil,"dd/MM/yyyy")
           ){
              throw new Exception();
			  }

			}catch(Exception e){
				throw new AppException("La fecha proporcionada no es v�lida, esta debe tener el formato: \"dd/mm/aaaa\" o venir como cadena vac�a.");
			}

			// 6. Validar que en el submitString se hayan proporcionado documentos
			if( submitString == null || submitString.length == 0){
				throw new AppException("No se especific� ning�n documento.");
			}

			// II. Obtener total de los documentos seleccionados

			numeroTotalDocumentos				= new BigDecimal("0");
			importeTotalDocumentos				= new BigDecimal("0.00");
			importeTotalRecibirDocumentos		= new BigDecimal("0.00");

			permisoDescontar						= submitString.length > 0?true:false;

			for(int i=0;i<submitString.length;i++){

				//	1. Obtener los resumenes de los documentos seleccinados para los
				// cuales se realizara la suma.
				String datos[] 					= submitString[i].split("\\|");

				numeroDocumentos 					= new BigDecimal(datos[0]);
				importeDocumentos 				= new BigDecimal(datos[1]);
				importeRecibirDocumentos		= new BigDecimal(datos[2]);

				//	2. Agrupar datos
				//		2.1 Sumar numero de documentos
				numeroTotalDocumentos 			= numeroTotalDocumentos.add(numeroDocumentos);
				//		2.2 Sumar importes por moneda
				importeTotalDocumentos 			= importeTotalDocumentos.add(importeDocumentos);
				//		2.3 Sumar importes a recibir por moneda
				importeTotalRecibirDocumentos	= importeTotalRecibirDocumentos.add(importeRecibirDocumentos);

			}

			respuesta.put("totalDocumentos", 					numeroTotalDocumentos.toPlainString()				);
			respuesta.put("importeTotal", 						importeTotalDocumentos.toPlainString()			);
			respuesta.put("importeTotalRecibir",				importeTotalRecibirDocumentos.toPlainString()	);

			respuesta.put("totalDocumentosFormateado",		(Object) Comunes.formatoDecimal(numeroTotalDocumentos.toPlainString(),					0,true));
			respuesta.put("importeTotalFormateado",			(Object) "$"+Comunes.formatoDecimal(importeTotalDocumentos.toPlainString(),			2,true));
			respuesta.put("importeTotalRecibirFormateado",	(Object) "$"+Comunes.formatoDecimal(importeTotalRecibirDocumentos.toPlainString(),	2,true));

			// III. Obtener ids de los documentos seleccionados
			ArrayList listaIdsDoctosSeleccionados 	= new ArrayList();
			for(int i=0;i<submitString.length;i++){

				String datos[] 							= submitString[i].split("\\|");
				String documentos[]						= datos[4].split(";");

				for(int j=0;j<documentos.length;j++){
					listaIdsDoctosSeleccionados.add(documentos[j]);
				}

			}

			// Si la cantidad de documentos seleccionados excede el numero limite de documentos individuales
			// cuyo detalle puede ser mostrado en pantalla.
			if( listaIdsDoctosSeleccionados.size() > SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS ){
				noSeMuestraDetalleDeTodosLosDoctos = true;
			}

			// IV. Obtener detalle individual de los documentos seleccionados

			// 1. Obtener instancia del EJB de Seleccion de Documentos
		   ISeleccionDocumento seleccionDocumento = null;
		   try {
				seleccionDocumento 										= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

         } catch(Exception e) {

            log.error("getListaIntermediariosFinancieros(EJB Exception)");
			   e.printStackTrace();

			   throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

         }

			// 2. Obtener tasa plazo para el IF seleccionado
			Vector 		vTasaPlazoIF 	= new Vector();
			try {

				Vector 	vTasaAceptada 	= new Vector();

				// Leer valor de la tasa aceptada para la epo, cliente, moneda, intermediario financiero y siguiente dia h&aacute;bil parametrizado
				vTasaAceptada = seleccionDocumento.ovgetTasaAceptada(clavePyme, claveEpo, claveMoneda, claveIF, fechaSiguienteDiaHabil);

				// Leer tasas aceptadas para el intermediario: leer tasa plazo para el if seleccionado
				for (int i= 0; i<vTasaAceptada.size(); i++) {
					Vector vDatos = (Vector) vTasaAceptada.get(i);
					if (claveIF.equals(vDatos.get(2).toString())) {
						// Leer tasa plazo para el if seleccionado
						vTasaPlazoIF.addElement(vDatos);
					}
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los plazos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 3. Obtener la lista de todos los documentos
			Vector 	vDocumentosDescontar = new Vector();
			try {

				// Si no se ha vencido la fecha limite, consultar los documentosa descontar
				String txtFechaVencDe	= "";	// Inicio fecha de vencimiento
				String txtFechaVenca 	= "";	// Fin fecha de vencimiento

				String sTipoPiso 			= getTipoPiso(claveIF); // comcat_if.ig_tipo_piso
				vDocumentosDescontar 	= seleccionDocumento.ovgetDocumentosDescontar(
							clavePyme,  		// Clave de la pyme
							claveMoneda,  		// Clave de la moneda
							claveEpo,  			// Clave de la epo
							vTasaPlazoIF,  	// Tasa plazo if
							txtFechaVencDe, 	// Inicio fecha de vencimiento
							txtFechaVenca, 	// Fin fecha de vencimiento
							claveIF, 			// Clave del intermediario financiero
							sTipoPiso			// Tipo de piso
				);

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los documentos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 4. Extraer una cantidad limitada de documentos, indicada por la variable SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS,
			// para ser mostrados al usuario en el resumen.
			for (int i=0; i<vDocumentosDescontar.size(); i++){

				// Leer datos del documento a descontar
				Vector vDatosDocumentosDescontar = (Vector) vDocumentosDescontar.get(i);

				// Leer Id del documento
				String idDocumento 					= vDatosDocumentosDescontar.get(9).toString();

				if(listaIdsDoctosSeleccionados.contains(idDocumento)){

					HashMap documento = new HashMap();

					documento.put("numeroDocto", 					vDatosDocumentosDescontar.get(0).toString());
					documento.put("importeFormateado",			"$"+Comunes.formatoDecimal(vDatosDocumentosDescontar.get(5).toString(),	2,true));
					documento.put("importeRecibirFormateado",	"$"+Comunes.formatoDecimal(vDatosDocumentosDescontar.get(8).toString(),	2,true));

					detalleDoctosSeleccionados.add(documento);

					if(detalleDoctosSeleccionados.size() == SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS ){
						break;
					}

				}

			}

		}catch(Throwable e){

			mensaje.setLength(0);
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getResumenDeLaOperacion(Exception)");
					log.error("getResumenDeLaOperacion.claveEpo               = <" + claveEpo               + ">");
					log.error("getResumenDeLaOperacion.claveIF                = <" + claveIF                + ">");
					log.error("getResumenDeLaOperacion.clavePyme              = <" + clavePyme              + ">");
					log.error("getResumenDeLaOperacion.claveMoneda            = <" + claveMoneda            + ">");
					log.error("getResumenDeLaOperacion.fechaSiguienteDiaHabil = <" + fechaSiguienteDiaHabil + ">");
					log.error("getResumenDeLaOperacion.submitString           = <" + submitString           + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensaje.append(msg);

			}else{

				log.error("getResumenDeLaOperacion(Exception)");
				log.error("getResumenDeLaOperacion.claveEpo               = <" + claveEpo               + ">");
				log.error("getResumenDeLaOperacion.claveIF                = <" + claveIF                + ">");
				log.error("getResumenDeLaOperacion.clavePyme              = <" + clavePyme              + ">");
				log.error("getResumenDeLaOperacion.claveMoneda            = <" + claveMoneda            + ">");
				log.error("getResumenDeLaOperacion.fechaSiguienteDiaHabil = <" + fechaSiguienteDiaHabil + ">");
				log.error("getResumenDeLaOperacion.submitString           = <" + submitString           + ">");
				e.printStackTrace();

				mensaje.append("Ocurri� un error inesperado al preparar el resumen de la seleccion de documentos.");
				hayError					= true;

         }

			// Si se llegara a presentar un error, resetear los campos con contenido de la respuesta
			log.debug("getDocumentosNegociablesAgrupadosPorTasa: Se suprime permiso para descontar documentos. ");
			permisoDescontar							= false;
			hayMensaje 									= true;
			detalleDoctosSeleccionados 			= new ArrayList();
			noSeMuestraDetalleDeTodosLosDoctos 	= false;

			respuesta.put("totalDocumentos", 					"0"		);
			respuesta.put("importeTotal", 						"0.00"	);
			respuesta.put("importeTotalRecibir",				"0.00"	);

			respuesta.put("totalDocumentosFormateado",		"0" 		);
			respuesta.put("importeTotalFormateado",			"$0.00"	);
			respuesta.put("importeTotalRecibirFormateado",	"$0.00"	);

		}finally{

			respuesta.put("permisoDescontar", 							String.valueOf(permisoDescontar)							);
			respuesta.put("hayMensaje", 									String.valueOf(hayMensaje)									);
			respuesta.put("mensaje", 										mensaje.toString()											);
			respuesta.put("detalleDoctosSeleccionados", 				detalleDoctosSeleccionados									);
			respuesta.put("hayError", 										String.valueOf(hayError)									);
			respuesta.put("noSeMuestraDetalleDeTodosLosDoctos",	String.valueOf(noSeMuestraDetalleDeTodosLosDoctos) );

			log.info("getResumenDeLaOperacion(S)");

		}

		return respuesta;

	}

	/**
	 *
	 * Devuelve los par&aacute;metros que se utilizan para autenticar la selecci&oacute;n de documentos
	 * realizada por el usuario.
	 *
	 * @param loginUsuario <tt>String</tt> con el login del usuario.
	 *
	 * @return <tt>HashMap</tt> con el siguiente contenido:<br>
	 *			  <br>
	 *		 	  Tabla 10. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayClaveCesion"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td><tt>"true"</tt> que indica si el usuario ya tiene registrada su clave de cesi&oacute;n de derechos y "false" en caso contrario.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td><tt>"true"</tt> que indica si se present&oacute; un error inesperado y "false" en caso contrario. Cuando se presenta un error,
	 *                	la descripci&oacute;n de este se env&iacute;a en el campo "mensaje", los campos: "hayClaveCesion" y "mostrarCheckBoxConfirmacionCorreo" se ponen
	 *                	en "false" y la lista "cajasClaveCesion" se env&iacute;a vac&iacute;a.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                   la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mostrarCheckBoxConfirmacionCorreo"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Si viene en "true", indica que el usuario no ha confirmado la direcci&oacute;n de correo donde se enviar&aacute; el archivo PDF con el acuse de los
	 *                documentos que seleccion&oacute; para descuento, por lo que se debe mostrar un checkbox en el que se solicite la confirmaci&oacute;n del correo;
	 *                la direcci&oacute;n de correo a confirmar es aquella que viene en sesi&oacute;n (debe ser la misma que se encuentra registrada en el OID).
	 *                <br>Si viene en "false" indica que el usuario ya confirm&oacute; la direcci&oacute;n
	 * 					de correo a donde se le enviar&aacute; el archivo PDF, por lo que ya no es necesario mostrar el checkbox de confirmaci&oacute;n.
	 *                </td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"cajasClaveCesion"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *             	<td><tt>ArrayList</tt> de <tt>String</tt> con los n&uacute;meros de caja que corresponden con la posici&oacute;n de los
	 *                caracteres de la clave de cesi&oacute;n de derechos que el usuario defini&oacute;. El n&uacute;mero de la primera caja es
	 *						el <tt>"1"</tt> y el de la &uacute;ltima es el "8". Siempre se enviar&aacute;n 3 n&uacute;meros de caja, los cuales
	 *						son elegidos de forma aleatoria.<br>Si el usuario no ha especificado una clave de sesi&oacute;n, esta lista se env&iacute;a vac&iacute;a.
	 *                </td>
	 *					</tr>
	 *			  </table>
	 *
	 */
	public HashMap getParametrosClaveConfirmacion(String loginUsuario){

		log.info("getParametrosClaveConfirmacion(E)");

		HashMap			respuesta									= new HashMap();

		boolean			hayClaveCesion								= false;
		boolean 			mostrarCheckBoxConfirmacionCorreo 	= false;
		StringBuffer 	mensaje										= null;
		boolean			hayMensaje									= false;
		boolean 			hayError										= false;
		ArrayList		cajasClaveCesion							= new ArrayList();

		try {

			// I. Validacion
			// 1. Validar que haya especificado el login del usuario
			if(loginUsuario == null || loginUsuario.trim().equals("")){
				throw new AppException("No se especific� el login del usuario");
			}

			// II. Obtener instancias de EJB
			// 1. Obtener EJB de Seguridad
			Context		context		= null;
			Seguridad	seguridad	= null;
			try {
				seguridad 		= ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);

			}catch(Exception e){

				log.error("getParametrosClaveConfirmacion(SeguridadEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Seguridad");

			}

			// III. Realizar las validaciones correspondientes
			// 1. Validar si el usuario tiene registrada una clave de sesion
			hayClaveCesion = seguridad.vvalidarCveCesion(loginUsuario);

			// 2. Revisar si se mostrar el combo de confirmacion de correo
			if(hayClaveCesion){

				boolean correoSinConfirmar 			= seguridad.vvalidarEstatusCorreo(loginUsuario);

				if(!correoSinConfirmar){
					mostrarCheckBoxConfirmacionCorreo 	= true;
				}else{
					mostrarCheckBoxConfirmacionCorreo	= false;
				}

			}else{
				mostrarCheckBoxConfirmacionCorreo 		= false;
			}

			// 3. Generar numeros aleatorios correspondiente a las cajas que solicitaran
			//    el caracter de la contrase�a
			ArrayList cajas 		= new ArrayList(); // {"1","2","3","4","5","6","7","8" );
         for(int i=1;i<=8;i++){
            cajas.add(String.valueOf(i));
         }

			// 4. Obtener numero aleatorio
			if(hayClaveCesion){
				Random	random		= new Random();
				for(int i=0;i<3;i++){

					int		indiceAleatorio	= random.nextInt(cajas.size());
					String 	numeroCaja			= (String) cajas.get(indiceAleatorio);

					// Agregar numero de caja a la lista de cajas
					cajasClaveCesion.add(numeroCaja);

					// Borrar numero de caja de la lista de numeros para evitar
					// que la caja sea elegida mas de una vez
					cajas.remove(indiceAleatorio);

				}
			}

		}catch(Throwable e){


			mensaje = new StringBuffer();
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("getParametrosClaveConfirmacion(Exception)");
					log.error("getParametrosClaveConfirmacion.loginUsuario = <" + loginUsuario+ ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensaje.append(msg);

			}else{

				log.error("getParametrosClaveConfirmacion(Exception)");
				log.error("getParametrosClaveConfirmacion.loginUsuario = <" + loginUsuario+ ">");
				e.printStackTrace();

				mensaje.append("Ocurri� un error al consultar los par�metros de la clave de sesi�n de derechos.");
				hayError									= true;

         }

			hayMensaje 									= true;

			hayClaveCesion								= false;
			mostrarCheckBoxConfirmacionCorreo	= false;
			cajasClaveCesion 							= new ArrayList();

		}finally{

			respuesta.put("hayClaveCesion",							String.valueOf(hayClaveCesion)							);
			respuesta.put("hayMensaje",								String.valueOf(hayMensaje)									);
			respuesta.put("mensaje",									(mensaje == null?"":mensaje.toString())				);
			respuesta.put("hayError",									String.valueOf(hayError)									);
			respuesta.put("mostrarCheckBoxConfirmacionCorreo",	String.valueOf(mostrarCheckBoxConfirmacionCorreo)	);
			respuesta.put("cajasClaveCesion",						cajasClaveCesion												);

			log.info("getParametrosClaveConfirmacion(S)");

		}

		return respuesta;

	}

	/**
	 * Guarda la clave de cesi�n de derechos
	 *
	 * @param loginUsuario 	<tt>String</tt> con el login del usuario.
	 * @param claveCesion  	<tt>String</tt> de 8 caracteres de logitud con la clave de confirmaci�n
	 *								(Clave de Cesi�n de Derechos)
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			  <br>
	 *			  Tabla 18.  Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"exito"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Con <tt>"true"</tt> indica que la clave de confirmaci&oacute;n se guard&oacute; con &eacute;xito, en caso contrario viene en <tt>"false"</tt>.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje con la descripci&oacute;n de &eacute;ste se env&iacute;a en <tt>"mensaje"</tt>.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                	la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *			</table>
	 *
	 */
	public HashMap guardarClaveConfirmacion(String loginUsuario, String claveCesion){

		log.info("guardarClaveConfirmacion(E)");

		HashMap			respuesta	= new HashMap();

		boolean			hayMensaje	= false;
		StringBuffer 	mensaje 		= null;
		boolean			hayError		= false;
		boolean 		 	exito			= true;

		try{

			// 1. Validar login del usuario
			if( loginUsuario == null || loginUsuario.trim().equals("") ){
				throw new AppException("El login del usuario es requerido.");
			}

			// 2. Validar la clave de cesion (que esta venga especificada)
			if( claveCesion == null || claveCesion.trim().equals("") ){
				throw new AppException("La clave de cesi�n es requerida.");
			}

			// 3. Validar que la clave de cesi�n sea de 8 caracteres
			if( claveCesion.length() != 8 ){
				throw new AppException("La clave de cesi�n debe de ser de 8 caracteres");
			}

			// 4. Obtener EJB de Seguridad
			Seguridad	seguridad	= null;
			try {
				seguridad 		= ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);

			}catch(Exception e){

				log.error("getParametrosClaveConfirmacion(SeguridadEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO: Ocurri� un error al obtener instancia del EJB de Seguridad");

			}

			// 5. Guardar clave de cesion
			seguridad.guardarCveCesEncriptada(loginUsuario,claveCesion);

		}catch(Throwable e){

			mensaje = new StringBuffer();
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("guardarClaveConfirmacion(Exception)");
					log.error("guardarClaveConfirmacion.loginUsuario = <" + loginUsuario + ">");
					log.error("guardarClaveConfirmacion.claveCesion  = <" + claveCesion  + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensaje.append(msg);

			}else{

				log.error("guardarClaveConfirmacion(Exception)");
				log.error("guardarClaveConfirmacion.loginUsuario = <" + loginUsuario + ">");
				log.error("guardarClaveConfirmacion.claveCesion  = <" + claveCesion  + ">");
				e.printStackTrace();

				mensaje.append("Ocurri� un error al guardar la clave de confirmaci�n.");
				hayError								= true;

			}

			hayMensaje 								= true;
			exito										= false;

		}finally{

			respuesta.put("hayMensaje",		String.valueOf(hayMensaje)			        );
			respuesta.put("mensaje",			(mensaje == null?"":mensaje.toString())  );
			respuesta.put("hayError",			String.valueOf(hayError)			        );
			respuesta.put("exito",				String.valueOf(exito) 	        );

			log.info("guardarClaveConfirmacion(S)");

		}

		return respuesta;

	}

	// 6. METODOS CORRESPONDIENTES AL MODULO 6 { PANTALLA 9, 11, 12 Y 13: ADMIN PYME - DESCONTAR ( RESUMEN DE LA OPERACION POR DOCUMENTO -- ACUSE -- ) }
	/**
	 *
	 * Valida que los caracteres de la clave de sesi&oacute;n de derechos solicitados al usuario, correspondan con los que defini&oacute;.
	 *
	 * @param loginUsuario			 <tt>String</tt> con el login del usuario.
	 * @param caracteresClaveCesion <tt>ArrayList</tt> con 3 elementos de tipo <tt>HashMap</tt> con los caracteres (y sus posiciones) de la clave de cesi�n de derechos.<br>
	 *		 	  							 Tabla 11. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "caracteresClaveCesion"</tt>.<br>
	 *		 	  							 <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *		 	  							 	<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *		 	  							 		<td colspan="2"><b>Llave</b></td>
	 *												<td colspan="2"><b>Valor</b></td>
	 *											</tr>
	 *											<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *												<td><b>Tipo Objeto</b></td>
	 *												<td><b>Contenido</b></td>
	 *												<td><b>Tipo Objeto</b></td>
	 *												<td><b>Descripci&oacute;n</b></td>
	 *											</tr>
	 *											<tr bgcolor="white" class="TableRowColor">
	 *												<td><tt>String</tt></td>
	 *												<td>"caracterContrasena"</td>
	 *												<td><tt>String</tt></td>
	 *             							<td>Caracter de la clave de cesi&oacute;n de derechos.</td>
	 *											</tr>
	 *											<tr bgcolor="white" class="TableRowColor">
	 *												<td><tt>String</tt></td>
	 *												<td>"caja"</td>
	 *												<td><tt>String</tt></td>
	 *             							<td>Posici&oacute;n del caracter dentro de la clave de cesi&oacute;n derechos. El primer carater de la clave
	 *												de cesi&oacute;n de derechos tiene asociada la posici&oacute;n "1", y el &uacute;ltimo caracter tiene la posici&oacute;n "8".</td>
	 *											</tr>
	 *										 </table>
	 * @param confirmarEmail		 <tt>boolean</tt> que con un valor <tt>true</tt> indica que el usuario ha confirmado que la direcci&oacute;n
	 *                             de correo que se le present&oacute; es v&aacute;lida y es donde desea que se le env&iacute;e el archivo con el detalle
	 *										 de los documentos que fueron seleccionados para descuento. Si viene en <tt>false</tt> NO se desautoriza
	 *										 la direcci&oacute;n de correo.
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			  <br>
	 *		 	  Tabla 12. Contenido de la Respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Viene en <tt>"true"</tt> si hay un mensaje que mostrar, en caso contrario, viene en <tt>"false"</tt>.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Si se present&oacute; un error inesperado viene en <tt>"true"</tt>, en caso contrario viene en <tt>"false"</tt>. Cuando se presenta un error,
	 *                	la descripci&oacute;n de este se env&iacute;a en el campo "mensaje" y el campo: "esClaveCorrecta" se pone en "false".
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                   la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"esClaveCorrecta"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Si los caracteres de la clave de cesi&oacute;n proporcionados son correctos viene en <tt>"true"</tt>, en caso
	 *						contrario viene en "false".
	 *                </td>
	 *					</tr>
	 *				</table>
	 *
	 */
	public HashMap validaParametrosClaveConfirmacion(String loginUsuario, ArrayList caracteresClaveCesion, boolean confirmarEmail){

		log.info("validaParametrosClaveConfirmacion(E)");

		HashMap 			respuesta 			= new HashMap();

		boolean			hayMensaje			= false;
		StringBuffer 	mensaje 				= null;
		boolean			hayError				= false;
		boolean			esClaveCorrecta 	= false;

		try {

			// I. Validar parametros de entrada
			// 1. Validar login del usuario
			if(loginUsuario == null || loginUsuario.trim().equals("")){
				throw new AppException("El login del usuario es requerido");
			}
			// 2. Validar que se hayan proporcionado los caracteres de la clave de cesi�n
			if(caracteresClaveCesion == null ){
				throw new AppException("La clave de cesi�n es requerida");
			}
			// 3. Validar que se hayan proporcionado en especifico 3 caracteres de la clave de cesi�n
			if( caracteresClaveCesion.size() != 3){
				throw new AppException("Se requieren 3 caracteres de la clave de cesi�n para poder validarla");
			}

			// II. Obtener instancias de los EJB
			// 1. Obtener EJB de Seguridad
			Seguridad	seguridad	= null;
			try {
				seguridad 		= ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);

			}catch(Exception e){

				log.error("getParametrosClaveConfirmacion(SeguridadEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Seguridad");

			}

			// III. Realizar las validaciones de los Parametros de la clave de confirmaci�n
			// 1. Validar clave de confirmacion usando el siguiente metodo
			ArrayList caracteresValidar = new ArrayList();
			// Agregar caracteres contrase�a
			for(int i=0;i<caracteresClaveCesion.size();i++){

				HashMap registro 					= (HashMap) caracteresClaveCesion.get(i);
				String  caracterContrasena		= (String)  registro.get("caracterContrasena");
				caracteresValidar.add(caracterContrasena);

			}
			// 2. Agregar posiciones caracteres contrase�a
			for(int i=0;i<caracteresClaveCesion.size();i++){

				HashMap 	registro 		= (HashMap) caracteresClaveCesion.get(i);
				int  		cajaAjustada	= Integer.parseInt( (String) registro.get("caja") ) - 1;
				String  	caja				= String.valueOf(cajaAjustada);
				caracteresValidar.add(caja);

			}
			// 3. Validar contrase�a
			esClaveCorrecta = seguridad.vvalidarCveConfirmacion(loginUsuario,caracteresValidar);

			// 4. Si la clave de confirmacion es correcta,
			if(esClaveCorrecta){

				// confirmar correo si el usuario lo ha inidicado as� y s�lo si no ha confirmado el correo.
				if( confirmarEmail && !seguridad.vvalidarEstatusCorreo(loginUsuario) ){

					// Actualizar estatus correo a "S" (confirmado)
					seguridad.actualizarEstatusCorreo(loginUsuario,"S");

				}

			// 5. Si la validacion no es exitosa enviar mensaje:
			}else{

				hayMensaje 	= true;
				mensaje 		= mensaje == null? new StringBuffer():mensaje;
				mensaje.append("La confirmaci�n de la clave de cesi�n es incorrecta, el proceso ha sido cancelado.");

			}

		}catch(Throwable e){

			mensaje = new StringBuffer();
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("validaParametrosClaveConfirmacion(Exception)");
					log.error("validaParametrosClaveConfirmacion.loginUsuario          = <" + loginUsuario          + ">");
					log.error("validaParametrosClaveConfirmacion.caracteresClaveCesion = <" + caracteresClaveCesion + ">");
					log.error("validaParametrosClaveConfirmacion.confirmarEmail        = <" + confirmarEmail        + ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensaje.append(msg);

			}else{

				log.error("validaParametrosClaveConfirmacion(Exception)");
				log.error("validaParametrosClaveConfirmacion.loginUsuario          = <" + loginUsuario          + ">");
				log.error("validaParametrosClaveConfirmacion.caracteresClaveCesion = <" + caracteresClaveCesion + ">");
				log.error("validaParametrosClaveConfirmacion.confirmarEmail        = <" + confirmarEmail        + ">");
				e.printStackTrace();

				mensaje.append("Ocurri� un error al confirmar los parametros de la clave de cesi�n de derechos.");
				hayError								= true;

			}

			hayMensaje 								= true;
			esClaveCorrecta						= false;

		}finally{

			respuesta.put("hayMensaje",		String.valueOf(hayMensaje)			        );
			respuesta.put("mensaje",			(mensaje == null?"":mensaje.toString())  );
			respuesta.put("hayError",			String.valueOf(hayError)			        );
			respuesta.put("esClaveCorrecta",	String.valueOf(esClaveCorrecta) 	        );

			log.info("validaParametrosClaveConfirmacion(S)");

		}

		return respuesta;

	}

	/**
	 *
	 * Pone el campo <tt>COM_ACUSE2.CS_OPERACION_MOVIL</tt>  en <tt>'S'</tt> para indicar que la operaci�n se
	 * realiz� desde un dispositivo m�vil.
	 * @throws AppException
	 *
	 * @param acuse <tt>String</tt> con el acuse de la operaci�n.
	 *
	 */
	private void registraOperacionMovil(String acuse)
		throws AppException{

		log.info("registraOperacionMovil(E)");
		AccesoDB 				con		= new AccesoDB();
		PreparedStatement		ps			= null;
		StringBuffer			query 	= null;

		boolean					exito		= true;

		try {

			con.conexionDB();

			query = new StringBuffer();
			query.append(
				"UPDATE          				"  +
				"   COM_ACUSE2 				"  +
				"SET            				"  +
				"   CS_OPERACION_MOVIL = ? "  +
				"WHERE           				"  +
				"   CC_ACUSE           = ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "S"		);
			ps.setString(2, acuse 	);
			ps.executeUpdate();

		}catch(Exception e){

			log.error("registraOperacionMovil(Exception)");
			log.error("registraOperacionMovil.acuse = <" + acuse + ">");
			e.printStackTrace();
			exito = false;

			throw new AppException("ERROR INESPERADO:Ocurri� un error al registrar el origen de la operaci�n.");

		}finally{

			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

			log.info("registraOperacionMovil(S)");

		}

	}

	/**
	 *
	 * Devuelve el Nombre de la Moneda.
	 * @throws AppException
	 *
	 * @param claveMoneda ID(<tt>comcat_moneda.ic_moneda</tt>) de la moneda. Numero entero mayor a cero.
	 *
	 * @return <tt>String</tt> con el nombre de la moneda.
	 *
	 */
	private String getNombreMoneda(String claveMoneda)
		throws AppException{

		log.info("getNombreMoneda(E)");
		String 					nombreMoneda 	= null;

		AccesoDB 				con				= new AccesoDB();
		PreparedStatement		ps					= null;
		ResultSet				rs					= null;
		StringBuffer			query 			= null;

		try {

			con.conexionDB();

			query = new StringBuffer();
			query.append(
				"SELECT          	              "  +
				"   CD_NOMBRE AS NOMBRE_MONEDA  "  +
				"FROM                           "  +
				"   COMCAT_MONEDA               "  +
				"WHERE           	              "  +
				"   IC_MONEDA = ?               "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveMoneda));
			rs = ps.executeQuery();

			if(rs.next()){
				nombreMoneda = rs.getString("NOMBRE_MONEDA");
			}
			nombreMoneda = nombreMoneda == null || nombreMoneda.trim().equals("")?"No disponible":nombreMoneda.trim();

		}catch(Exception e){

			log.error("getNombreMoneda(Exception)");
			log.error("getNombreMoneda.claveMoneda = <" + claveMoneda+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar la descripci�n de la moneda");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getNombreMoneda(S)");

		}

		return nombreMoneda;

	}

	/**
	 *
	 * Devuelve el Nombre de la EPO.
	 * @throws AppException
	 *
	 * @param claveMoneda ID(<tt>comcat_epo.ic_epo</tt>) de la EPO. Numero entero mayor a cero.
	 *
	 * @return <tt>String</tt> con el nombre de la EPO.
	 *
	 */
	private String getNombreEpo(String claveEpo)
		throws AppException{

		log.info("getNombreEpo(E)");
		String 					nombreEpo 		= null;

		AccesoDB 				con				= new AccesoDB();
		PreparedStatement		ps					= null;
		ResultSet				rs					= null;
		StringBuffer			query 			= null;

		try {

			con.conexionDB();

			query = new StringBuffer();
			query.append(
				"SELECT          	              		"  +
				"   CG_RAZON_SOCIAL AS NOMBRE_EPO  	"  +
				"FROM                           		"  +
				"   COMCAT_EPO               			"  +
				"WHERE           	              		"  +
				"   IC_EPO = ?               			"
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveEpo));
			rs = ps.executeQuery();

			if(rs.next()){
				nombreEpo = rs.getString("NOMBRE_EPO");
			}
			nombreEpo = nombreEpo == null || nombreEpo.trim().equals("")?"No disponible":nombreEpo.trim();

		}catch(Exception e){

			log.error("getNombreEpo(Exception)");
			log.error("getNombreEpo.claveEpo = <" + claveEpo + ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar la descripci�n de la EPO");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getNombreEpo(S)");

		}

		return nombreEpo;

	}

	/**
	 *
	 * Crea un archivo PDF con el Detalle de la Selecci&oacute;n de los Documentos el cual ser&aacute; enviado al usuario PYME
	 * que realiz&oacute; la selecci&oacute;n de los documentos.
	 * @throws AppException
	 *
	 * @param claveEpo 		ID<tt>comcat_epo.ic_epo</tt> con la clave de la EPO.
	 * @param directorioTemporal <tt>String</tt> con la ruta del directorio donde se guardar&aacute; el
	 *                   	PDF generado. El contenido de esta variable, debe ser el mismo que el que se
	 *								encuentra en la variable <tt>"global"</tt> <tt>strDirectorioTemp</tt>.
	 * @param directorioPublicacion <tt>String</tt> con la ruta del directorio f&iacute;sica del <tt>context root</tt>.
	 *                 	 	El contenido de esta variable, debe ser el mismo que el que se
	 *								encuentra en la variable <tt>"global"</tt> <tt>strDirectorioPublicacion</tt>.
	 * @param fechaDeCarga 	Fecha en la que se confirm&oacute; la selecci&oacute;n de los documentos. Debe tener formato
	 *								<tt>dd/mm/aaaa</tt>.
	 * @param horaDeCarga 	Hora en la que se confirm&oacute; la selecci&oacute;n de los documentos. Debe tener formato
	 *								<tt>hh:mm:ss am/pm</tt>.
	 * @param acuse 			<tt>String</tt> con el acuse formateado con guiones. Ver m&eacute;todo:
	 *								<tt>Acuse.formatear()</tt>
	 * @param parametrosUsuario <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			  <br>
	 *		 	  Tabla 13. Contenido de los par&aacute;metros del usuario.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"loginUsuario"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Login del usuario. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en la variable <tt>"global"</tt> <tt>iNoUsuario</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"pais"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del pa&iacute;s. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strPais</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numNafinElectronico"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>N&uacute;mero Nafin Electr&oacute;nico de la PYME. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>iNoNafinElectronico</tt>.
	 *             	</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"externo"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Atributo externo. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>sesExterno</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombre"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre de la PYME. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strNombre</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombreUsuario"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del Usuario. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strNombreUsuario</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"logo"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del archivo de imagen que se utilizar&aacute; como logo. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strLogo</tt>.
	 *						</td>
	 *					</tr>
	 *			  </table>
	 *			  <br>
	 * @param seleccionados 	<tt>String Array</tt> con los datos de los documentos seleccionados.
	 * @param nombreCuenta 		<tt>String</tt> con el nombre de la cuenta.
	 * @param nombreEpo 			<tt>String</tt> con el nombre de la EPO.
	 * @param totalMonto 		<tt>String</tt> con el monto total de los documentos
	 * @param totalDescuento 	<tt>String</tt> con el monto total de descuento.
	 * @param totalIntereses   <tt>String</tt> con el monto total de los intereses.
	 * @param totalRecibir 		<tt>String</tt> con el total del importe a recibir.
	 * @param mensajeDiaHabilSiguiente <tt>String</tt> con el mensaje indicando que la operaci&oacute;n se realizar&aacute;
	 *									el d&iacute;a h&aacute;bil siguiente. Este campo solo trae contenido cuando los documentos
	 *									fueron seleccionados fuera del horario de servicio del d&iacute;a actual y se tiene habilitada
	 *									la operaci&oacute;n bajo el esquema de factoraje 24 hrs.
    * @param nombreMoneda 		<tt>String</tt> con el nombre de la moneda.
    * @param clavePyme        <tt>String</tt> con la clave de la pyme.
	 *
	 * @return <tt>String</tt> con el nombre del archivo creado.
	 *
	 */
	private String creaPDFDetalleSeleccion(
			String 	claveEpo,
			String 	directorioTemporal, 		// strDirectorioTemp
			String 	directorioPublicacion, 	// strDirectorioPublicacion
			String 	fechaDeCarga,
			String 	horaDeCarga,
			String 	acuse, 						// Acuse.formatear()
			HashMap 	parametrosUsuario,
			String[] seleccionados,
			String  	nombreCuenta,
			String  	nombreEpo,
			String 	totalMonto,
			String 	totalDescuento,
			String 	totalIntereses,
			String   totalRecibir,
			String 	mensajeDiaHabilSiguiente,
         String   nombreMoneda,
         String   clavePyme
		) throws AppException{

			log.info("creaPDFDetalleSeleccion(E)");
			String 	nombreArchivo 				= "";

			try {

				// 1. Obtener instancia del EJB de SeleccionDocumento
				ISeleccionDocumento	seleccionDocumento		= null;
				try {
					seleccionDocumento 		= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

				}catch(Exception e){

					log.error("creaPDFDetalleSeleccion(Exception)");
					e.printStackTrace();

					throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

				}

				// 2. Definir la Leyenda legal
				//String leyendaAcuse =
				//		"Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, "+
				//		"transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la ley general de t&iacute;tulos y operaciones de cr&eacute;dito, "+
				//		"32 c del c&oacute;digo fiscal de la federaci&oacute;n y 2038 del c&oacute;digo civil federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que "+
				//		"se efect&uacute;e el descuento electr&oacute;nico o el factoraje electr&oacute;nico.";

				// 3. tiene Parametrizacion Epo PEF
				boolean 	tieneParametrizacionEpoPEF = seleccionDocumento.parametrizacionEpoPef(claveEpo);
				// Leer nombre de los campos dinamicos
				List 		nombreCamposDinamicos 		= seleccionDocumento.getNombreCamposDinamicos(claveEpo);

				// 2.2 Construir archivo pdf
				CreaArchivo 	archivo 					= new CreaArchivo();
				nombreArchivo 								= archivo.nombreArchivo()+".pdf";
				ComunesPDF 		pdfDoc 					= new ComunesPDF(2,directorioTemporal+nombreArchivo);
				String 			meses[] 					= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

				// 2.3 Construir encabezado del archivo pdf
				String 			fechaActual 			= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String 			diaActual   			= fechaActual.substring(0,2);
				String 			mesActual   			= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String 			anioActual  			= fechaActual.substring(6,10);
				String 			horaActual  			= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(
							pdfDoc,
							(String) parametrosUsuario.get("pais"),
									   (parametrosUsuario.get("numNafinElectronico") == null?"":(String) parametrosUsuario.get("numNafinElectronico")),
							(String) parametrosUsuario.get("externo"),
							(String) parametrosUsuario.get("nombre"),
							(String) parametrosUsuario.get("nombreUsuario"),
							(String) parametrosUsuario.get("logo"),
							directorioPublicacion
						);

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				if(!"".equals(mensajeDiaHabilSiguiente)){
					pdfDoc.addText(mensajeDiaHabilSiguiente,"formas",ComunesPDF.LEFT);
				}

				// 2.4 Construir tabla de cifras de control
				pdfDoc.setTable(2,50);
				pdfDoc.setCell("Cifras de Control",									"celda01",ComunesPDF.CENTER,	2	);
				pdfDoc.setCell("N�mero de Acuse",									"formas"									);
				pdfDoc.setCell(acuse,													"formas",ComunesPDF.RIGHT			);
				pdfDoc.setCell("Fecha de Carga",										"formas"									);
				pdfDoc.setCell(fechaDeCarga,	                              "formas",ComunesPDF.RIGHT			);
				pdfDoc.setCell("Hora de Carga",										"formas"									);
				pdfDoc.setCell(horaDeCarga,		                           "formas",ComunesPDF.RIGHT			);
				pdfDoc.setCell("Usuario de Captura",								"formas"									);
				pdfDoc.setCell( (String) parametrosUsuario.get("loginUsuario") + " - " + (String) parametrosUsuario.get("nombreUsuario"),
																								"formas",ComunesPDF.RIGHT			);
				String mensaje =
							"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito "+
							"los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la Ley General de "+
							"T�tulos y Operaciones de Cr�dito, 32 c del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal, haci�ndome "+
							"sabedor de su contenido y alcance, condicionado a que se efect�e el descuento electr�nico o el factoraje electr�nico.\n\n"+
                            "Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s� he emitido o emitir� a la Empresa de Primer Orden " +
                            "el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.";

                

				pdfDoc.setCell(mensaje,"celda02",ComunesPDF.LEFT,2);
				pdfDoc.addTable();

				// 2.5 Dependiendo del tipo de usuario calcular el numero de columnas de la tabla de datos de los documentos
				int columnas = 0;
				if(tieneParametrizacionEpoPEF){
					columnas = 13;
				}else{
					columnas = 9;
				}

				//List nombreCamposDinamicos = BeanSeleccionDocumento.getNombreCamposDinamicos(sNoEpo);
				//this.getNombreCamposDinamicos(epo.substring(0, epo.indexOf("|")));
				if(!nombreCamposDinamicos.isEmpty()){
					for(int i = 0; i < nombreCamposDinamicos.size(); i++){columnas++;}
				}

				pdfDoc.setTable(columnas);

				// 2.6 Crear cabecera de la tabla de datos
				pdfDoc.setCell("Cuenta de Deposito",		"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO",							"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento",		"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda",						"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento",			"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento",	"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar",			"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses",			"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Recibir",			"celda01",ComunesPDF.CENTER);

				//List nombreCamposDinamicos = BeanSeleccionDocumento.getNombreCamposDinamicos(sNoEpo);//this.getNombreCamposDinamicos(epo.substring(0, epo.indexOf("|")));
				if(!nombreCamposDinamicos.isEmpty()){
					for(int i = 0; i < nombreCamposDinamicos.size(); i++){
						pdfDoc.setCell((String)nombreCamposDinamicos.get(i),"celda01",ComunesPDF.CENTER);
					}
				}

				// 2.7 Si es epo pef tambien agregar las columnas siguientes
				if(tieneParametrizacionEpoPEF){
					pdfDoc.setCell("Fecha Entrega",			"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Compra",				"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave Presupuestaria",	"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Periodo",					"celda01",ComunesPDF.CENTER);
				}

				pdfDoc.setHeaders();

				// 2.8 Agregar "contenido" de los registros seleccionados
				String selec 				= Comunes.implode("@@",seleccionados);
				Vector vSeleccionados 	= new Vector();
				vSeleccionados 			= Comunes.explode("@@",selec);
				String clase 				= "formas";
				for(int i=0; i < vSeleccionados.size();i++) {

					Vector vDatos = new Vector();

					if(i%5==4){
						clase = "celda02";
					} else {
						clase = "formas";
					}

					vDatos = Comunes.explode("|",vSeleccionados.get(i).toString());
					pdfDoc.setCell(nombreCuenta,		clase,ComunesPDF.CENTER);
					pdfDoc.setCell(nombreEpo,			clase,ComunesPDF.CENTER);

					pdfDoc.setCell(vDatos.get(6).toString(),										clase, ComunesPDF.CENTER);
					pdfDoc.setCell(nombreMoneda,														clase, ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vDatos.get(8)+"", 2),		clase, ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(vDatos.get(4),0,false)+" %",	clase, ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vDatos.get(1)+"", 2),		clase, ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vDatos.get(2)+"", 2),		clase, ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vDatos.get(3)+"", 2),		clase, ComunesPDF.CENTER);

					//List nombreCamposDinamicos = this.getNombreCamposDinamicos(epo.substring(0, epo.indexOf("|")));
					//List valoresCamposDinamicos = this.getValoresCamposDinamicos((String)vDatos.get(0), epo.substring(0, epo.indexOf("|")), iNoCliente);
					List valoresCamposDinamicos = seleccionDocumento.getValoresCamposDinamicos((String)vDatos.get(0), claveEpo, clavePyme);
					if(!nombreCamposDinamicos.isEmpty() && !valoresCamposDinamicos.isEmpty()){
						for(int j = 0; j < nombreCamposDinamicos.size(); j++){
							pdfDoc.setCell((String)valoresCamposDinamicos.get(j),clase,ComunesPDF.CENTER);
						}
					}

					if(tieneParametrizacionEpoPEF){

						List valoresEpoPef = seleccionDocumento.valoresEpoPef((String)vDatos.get(0), claveEpo, clavePyme);
						if(valoresEpoPef.size() > 0){
							pdfDoc.setCell((String)valoresEpoPef.get(0),	clase,ComunesPDF.CENTER);
							pdfDoc.setCell((String)valoresEpoPef.get(1),	clase,ComunesPDF.CENTER);
							pdfDoc.setCell((String)valoresEpoPef.get(2),	clase,ComunesPDF.CENTER);
							pdfDoc.setCell((String)valoresEpoPef.get(3),	clase,ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell(" ",									clase,ComunesPDF.CENTER);
							pdfDoc.setCell(" ",									clase,ComunesPDF.CENTER);
							pdfDoc.setCell(" ",									clase,ComunesPDF.CENTER);
							pdfDoc.setCell(" ",									clase,ComunesPDF.CENTER);
						}

					}

				}
				pdfDoc.addTable();

				// 2.9 Agregar tabla de totales
				pdfDoc.setTable(5,50);
				pdfDoc.setCell("",							"celda01",ComunesPDF.CENTER,1);
				pdfDoc.setCell(nombreMoneda,				"celda01",ComunesPDF.CENTER,4);

				// Agregar cabecera de la tabla de totales
				pdfDoc.setCell("E P O  -  DATOS",			"formasB",ComunesPDF.CENTER );
				pdfDoc.setCell("Total Monto Documento",	"formasB",ComunesPDF.CENTER );
				pdfDoc.setCell("Total Monto Descuento",	"formasB",ComunesPDF.CENTER );
				pdfDoc.setCell("Total Monto de Inter�s",	"formasB",ComunesPDF.CENTER );
				pdfDoc.setCell("Total Importe a Recibir",	"formasB",ComunesPDF.CENTER );

				// AGREGAR VALORES DE LOS TOTALES
				pdfDoc.setCell(nombreEpo,						"formas",ComunesPDF.CENTER  );
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMonto, 		2),	"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalDescuento, 	2),	"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalIntereses, 	2),	"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalRecibir, 	2),	"formas",ComunesPDF.CENTER);
				pdfDoc.addTable();

				pdfDoc.endDocument();

			} catch (Exception e) {

				nombreArchivo = null;

				log.error("creaPDFDetalleSeleccion(Exception)");
				log.error("creaPDFDetalleSeleccion.claveEpo              = <" + claveEpo              + ">");
				log.error("creaPDFDetalleSeleccion.directorioTemporal    = <" + directorioTemporal    + ">");
				log.error("creaPDFDetalleSeleccion.directorioPublicacion = <" + directorioPublicacion + ">");
				log.error("creaPDFDetalleSeleccion.fechaDeCarga          = <" + fechaDeCarga          + ">");
				log.error("creaPDFDetalleSeleccion.horaDeCarga           = <" + horaDeCarga           + ">");
				log.error("creaPDFDetalleSeleccion.acuse                 = <" + acuse                 + ">");
				log.error("creaPDFDetalleSeleccion.parametrosUsuario     = <" + parametrosUsuario     + ">");
				log.error("creaPDFDetalleSeleccion.seleccionados         = <" + seleccionados         + ">");
				log.error("creaPDFDetalleSeleccion.nombreCuenta          = <" + nombreCuenta          + ">");
				log.error("creaPDFDetalleSeleccion.nombreEpo             = <" + nombreEpo             + ">");
				log.error("creaPDFDetalleSeleccion.totalMonto            = <" + totalMonto            + ">");
				log.error("creaPDFDetalleSeleccion.totalDescuento        = <" + totalDescuento        + ">");
				log.error("creaPDFDetalleSeleccion.totalIntereses        = <" + totalIntereses        + ">");
				log.error("creaPDFDetalleSeleccion.totalRecibir          = <" + totalRecibir          + ">");

				if( e instanceof AppException){
					throw (AppException)e;
				}else{
					e.printStackTrace();
					throw new AppException("ERROR INESPERADO:El archivo PDF con el detalle de la operaci�n no pudo ser generado");
				}

			}finally{

				log.info("creaPDFDetalleSeleccion(S)");

			}

			return nombreArchivo;

	}

	/**
	 * Devuelve el n&uacute;mero Nafin Electr&oacute;nico a partir del ID de la PYME.
	 * @throws AppException
	 * @param clavePyme String con la clave de la PYME.
	 * @return <tt>String</tt> con el N&uacute;mero Nafin Electr&oacute;nico de la PYME.
	 */
	private String getNumeroNafinElectronico(String clavePyme)
		throws AppException{

		log.info("getNumeroNafinElectronico(E)");

		AccesoDB 			con 	= new AccesoDB();
		PreparedStatement ps 	= null;
		ResultSet 			rs 	= null;
		StringBuffer 		query = new StringBuffer();

		String 				numeroNafinElectronico = null;

		try {

			con.conexionDB();
			long 		numeroClavePyme 	= Long.parseLong(clavePyme);
			String 	tipo 					= null;

			if(numeroClavePyme <= 0L ){
				throw new AppException("ERROR INESPERADO:La Clave PYME proporcionada no es valida");
			}else{
				tipo = "P"; // PYME
			}

			query.append(
				"SELECT " +
				"	ic_nafin_electronico as numero_nafin_electronico "  +
				"FROM "  +
				"	comrel_nafin " +
				" WHERE "  +
				"	ic_epo_pyme_if = ? AND "+
				" 	cg_tipo 			= ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1, 		numeroClavePyme	);
			ps.setString(2, 	tipo					);
			rs = ps.executeQuery();

			if(rs.next()){
				numeroNafinElectronico = rs.getString("numero_nafin_electronico");
			}else{
				throw new AppException("ERROR INESPERADO:La PYME no tiene n�mero Nafin Electr�nico");
			}

		}catch(Exception e){

			log.error("getNumeroNafinElectronico(Exception)");
			log.error("getNumeroNafinElectronico.clavePyme = <" + clavePyme+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error inesperado al consultar el n�mero Nafin Electr�nico de la PYME");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getNumeroNafinElectronico(S)");

		}

		return numeroNafinElectronico;

	}

	/**
	 * Devuelve el nombre del pais de las pymes (que para todos los casos es "MEXICO" )
	 * @return <tt>String</tt> con la clave del pais de las pymes
	 */
	private String getNombrePaisDeLaPyme(){
		return "MEXICO";
	}

	/**
	 * Este m&eacute;todo devuelve el nombre de la PYME.
	 * @throws AppException
	 * @return <tt>String</tt> con el nombre de la PYME
	 */
	private String getNombrePyme(String clavePyme){

		log.info("getNombrePyme(E)");

		AccesoDB 			con 	= new AccesoDB();
		PreparedStatement ps 	= null;
		ResultSet 			rs 	= null;
		StringBuffer 		query = new StringBuffer();

		String 				nombrePyme = null;

		try {

			con.conexionDB();

			long 		numeroClavePyme 	= Long.parseLong(clavePyme);

			if(numeroClavePyme <= 0L ){
				throw new AppException("ERROR INESPERADO:La Clave PYME proporcionada no es valida");
			}

			String tipoPersona = null;
			query.setLength(0);
			query.append(
				" SELECT 										"  +
				"		cs_tipo_persona as tipo_persona 	"  +
				" FROM 											"  +
				"	comcat_pyme 								"  +
				" WHERE 											"  +
				"	ic_pyme = ? 								"
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1, numeroClavePyme );
			rs = ps.executeQuery();

			if(rs.next()){
				tipoPersona = rs.getString("tipo_persona");
			}

			rs.close();
			ps.close();

			if( "M".equals(tipoPersona) ) {

				query.setLength(0);
				query.append(
					" SELECT cg_razon_social AS nombre " +
					" FROM comcat_pyme " +
					" WHERE ic_pyme =  ? "
				);

			} else {

				query.setLength(0);
				query.append(
					" SELECT cg_nombre || ' ' || cg_appat || ' ' || cg_apmat AS nombre "+
					" FROM comcat_pyme " +
					" WHERE ic_pyme = ? "
				);

			}

			ps = con.queryPrecompilado(query.toString());
			ps.setLong(1, numeroClavePyme );
			rs = ps.executeQuery();

			if(rs.next()){
				nombrePyme = rs.getString("nombre");
			}

			if( nombrePyme == null || nombrePyme.equals("null") ){
				nombrePyme="";
			}

		}catch(Exception e){

			log.error("getNombrePyme(Exception)");
			log.error("getNombrePyme.clavePyme = <" + clavePyme+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error inesperado al consultar el nombre de la PYME");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getNombrePyme(S)");

		}

		return nombrePyme;

	}

	/**
	 * Determina el atributo "externo" de la PYME.
	 * @return <tt>String</tt> indicando si la PYME es externa o no.
	 */
	private String getAtributoExternoDeLaPyme(String clavePyme)
		throws AppException{

		log.info("getAtributoExternoDeLaPyme(E)");

		String externo = null;

		try {

			if(clavePyme != null && Integer.parseInt(clavePyme) <= 0 ){
				externo = "S";
			}

		}catch(Exception e){

			log.error("getAtributoExternoDeLaPyme(Exception)");
			log.error("getAtributoExternoDeLaPyme.clavePyme = <" + clavePyme+ ">");
			e.printStackTrace();

			throw new AppException("ERROR INESPERADO:Ocurri� un error al determinar al atributo \"externo\"");

		}finally{
			log.info("getAtributoExternoDeLaPyme(S)");
		}

		return externo;

	}

 	/**
	 *
	 * Realiza la Selecci&oacute;n de los Documentos para Descuento Electr&oacute;nico.
	 *
	 * @param claveEpo <tt>String</tt> con el ID(<tt>comcat_epo.ic_epo</tt>) de la EPO. N&uacute;mero entero mayor a cero.
	 * @param claveIF <tt>String</tt> con el ID(<tt>comcat_if.ic_if</tt>) del Intermediario Financiero. N&uacute;mero entero mayor a cero.
	 * @param clavePyme <tt>String</tt> con el ID(<tt>comcat_pyme.ic_pyme</tt>) de la PYME. N&uacute;mero entero mayor a cero.
	 * @param claveMoneda <tt>String</tt> con la clave de la Moneda (<tt>comcat_moneda.ic_moneda</tt>).
    *                    Valores permitidos:
    *                    <ul>
    *                      <li>Moneda Nacional: 1</li>
    *                      <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
	 * @param fechaSiguienteDiaHabil <tt>String</tt> con la fecha del siguiente d&iacute;a h&aacute;bil de la EPO;
	 *                               la fecha debe tener el formato: <tt>dd/mm/aaaa</tt>; este campo
	 *                               s&oacute;lo se env&iacute;a cuando la PYME accede al servicio de descuento fuera
	 *                               del horario y en caso de que se trabaje bajo el esquema de
	 *                               Factoraje 24hrs.
	 * @param submitString <tt>String Array</tt> con el resumen de los grupos de documentos que fueron seleccionados para descuento.<br>
	 *						Cada elemento de este <tt>String Array</tt> contiene los siguientes campos separados por pipes y en el siguiente orden:
	 *						<ol>
	 *							<li>totalDoctos: n&uacute;mero total de documentos.</li>
	 *							<li>importe: importe total de los documentos.</li>
	 *							<li>importeRecibir: importe total a recibir de los documentos.</li>
	 *							<li>tasa: valor de la tasa.</li>
	 *							<li>idsDocumentos: lista con los ID(<tt>com_documento.ic_documento</tt>) de los documentos; como separador se usa el caracter ';'.
	 *                   </li>
	 *						</ol>
	 * @param loginUsuario <tt>String</tt> con el login del usuario.
	 * @param correoUsuario <tt>String</tt> con el correo del usuario tal como aparece en el OID.
	 * @param directorioTemporal <tt>String</tt> con la ruta del directorio donde se guardar&aacute; el
	 *                   	PDF generado. El contenido de esta variable, debe ser el mismo que el que se
	 *								encuentra en la variable <tt>"global"</tt> <tt>strDirectorioTemp</tt>.
	 * @param directorioPublicacion <tt>String</tt> con la ruta del directorio f&iacute;sica del <tt>context root</tt>.
	 *                 	 	El contenido de esta variable, debe ser el mismo que el que se
	 *								encuentra en la variable <tt>"global"</tt> <tt>strDirectorioPublicacion</tt>.
	 * @param parametrosUsuario <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			  <br>
	 *		 	  Tabla 14. Contenido de los par&aacute;metros del usuario.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"loginUsuario"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Login del usuario. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en la variable <tt>"global"</tt> <tt>iNoUsuario</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombreUsuario"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del Usuario. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strNombreUsuario</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"logo"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del archivo de imagen que se utilizar&aacute; como logo. El contenido de esta variable, debe ser el mismo
	 *						que el que se encuentra en el atributo de sesi&oacute;n <tt>strLogo</tt>.
	 *						</td>
	 *					</tr>
	 *			  </table>
	 *			  <br>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *			  <br>
	 *		 	  Tabla 15. Contenido de la respuesta.<br>
	 *			  <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"exito"</td>
	 *						<td><tt>String</tt></td>
	 *						<td> Con <tt>"true"</tt> indica que la operaci&oacute;n tuvo &eacute;xito, y con <tt>"false"</tt> indica que no.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"cifrasDeControl"</td>
	 *						<td><tt>HashMap</tt></td>
	 *						<td>Cifras de control. Para ver una descripci&oacute;n m&aacute;s detalla del contenido de este objeto ir a la Tabla 16.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalNumeroDoctos"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>N&uacute;mero total de documentos que fueron seleccionados. En caso de que se presente un error inesperado, este campo viene en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalImporte"</td>
	 *						<td><tt>String</tt></td>
	 *						<td> Suma de los importes de todos los documentos. En caso de que se presente un error inesperado, este campo viene en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalImporteRecibir"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Suma de todos los importes a recibir de todos los documentos. En caso de que se presente un error inesperado, este campo viene en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalNumeroDoctosFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>N&uacute;mero total de documentos con formato de miles. En caso de que se presente un error inesperado, este campo viene en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalImporteFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Suma de todos los importes con formato de moneda. En caso de que se presente un error inesperado, este campo viene formateado en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"totalImporteRecibirFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Suma de todos los importes a recibir con formato de moneda. En caso de que se presente un error inesperado, este campo viene formateado en cero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombreMoneda"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Nombre de la moneda de los documentos.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"nombreEpo"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Nombre Comercial de la EPO.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"detalleDoctosSeleccionados"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *						<td><tt>ArrayList</tt> de <tt>HashMap</tt> con los datos de una cantidad registringida ({@link  #LIMITE_DOCTOS_MOSTRADOS})
	 *						de los documentos seleccionados. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>,
	 *						ver Tabla 17.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Viene en <tt>"true"</tt> si hay un mensaje que mostrar, en caso contrario, viene en <tt>"false"</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensajeSeleccion"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *						<td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensajeSeleccion" == "true"</tt>, en caso contrario se env&iacute;a como un <tt>ArrayList</tt> vac&iacute;o.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *						<td>Si se present&oacute; un error inesperado viene en <tt>"true"</tt>, en caso contrario viene en <tt>"false"</tt>. Cuando se presenta un error,
	 *                	la descripci&oacute;n de este se env&iacute;a en el campo "mensajeSeleccion" y el campo: "exito" se pone en "false", "cifrasDeControl"
	 *						   se env&iacute;a como un <tt>HashMap</tt> vac&iacute;o, todos los totales se env&iacute;an en ceros, "nombreMoneda" y "nombreEpo" se env&iacute;an como cadena vac&iacute;a, "detalleDoctosSeleccionados"
	 *							se env&iacute;a como una lista vac&iacute;a, <tt>"noSeMuestraDetalleDeTodosLosDoctos"</tt>,
	 *							<tt>"falloCreacionArchivoPDF"</tt> y <tt>"falloEnvioDeCorreo"</tt> se env&iacute;an en <tt>"false"</tt>.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                   la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"noSeMuestraDetalleDeTodosLosDoctos"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Si viene en <tt>"true"</tt>, indica que hay documentos cuyo detalle no se pudo presentar, debido a que
	 *						estos exceden la cantidad l&iacute;mite de documentos que se permite mostrar({@link #LIMITE_DOCTOS_MOSTRADOS});
	 *						viene en <tt>"false"</tt> en caso contrario.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"falloCreacionArchivoPDF"</td>
	 *						<td><tt>String</tt></td>
	 *						<td> Si viene en <tt>"true"</tt> indica que no se pudo crear el archivo PDF con el detalle de la selecci&oacute;n de
	 *						documentos, en caso contrario viene en <tt>"false"</tt>. Un fallo de este tipo no se reporta como error inesperado.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"falloEnvioDeCorreo"</td>
	 *						<td><tt>String</tt></td>
	 *						<td> Si viene en <tt>"true"</tt> indica que no se pudo enviar por correo el archivo PDF con el detalle de la selecci&oacute;n de
	 *						documentos, en caso contrario viene en <tt>"false"</tt>. Un fallo de este tipo no se reporta como error inesperado.
	 *						</td>
	 *					</tr>
	 *				</table>
	 *				<br>
	 *				Tabla 16. Contenido de cada uno de los elementos del <tt>HashMap</tt> "cifrasDeControl".<br>
	 *			   <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numeroDeAcuse"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>N&uacute;mero de Acuse.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"fechaDeOperacion"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Fecha de la Operaci&oacute;n con formato <tt>dd/mm/aaaa</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"horaDeOperacion"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Hora de la Operaci&oacute;n con formato <tt>hh:mm:ss am/pm</tt>.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"intermediarioFinanciero"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Nombre del Intermediario Financiero.
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"usuarioDeCaptura"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Login y Nombre del usuario que realiz&oacute; la selecci&oacute;n.
	 *						</td>
	 *					</tr>
	 *			   </table>
	 *				<br>
	 *				Tabla 17. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "detalleDoctosSeleccionados".</tt><br>
	 *			   <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"numeroDocto"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>N&uacute;mero del Documento. Este campo(<tt>com_documento.ig_numero_docto</tt>) es especificado por la EPO cuando publica el documento.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe del Documento con formato de moneda.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"importeRecibirFormateado"</td>
	 *						<td><tt>String</tt></td>
	 *             	<td>Importe a Recibir del documento con formato de moneda.</td>
	 *					</tr>
	 *			  </table>
	 *
	 */
	public HashMap seleccionarDocumentosParaDescuento(
			String 	claveEpo,
			String 	claveIF,
			String 	clavePyme,
			String 	claveMoneda,
			String 	fechaSiguienteDiaHabil,
			String[] submitString,
			String 	loginUsuario,
			String 	correoUsuario,
			String 	directorioTemporal,
			String 	directorioPublicacion,
			HashMap 	parametrosUsuario
	){

		log.info("seleccionarDocumentosParaDescuento(E)");
		boolean				exito											= true;
		HashMap 				respuesta									= new HashMap();

		boolean 				correoConfirmado							= false;
		boolean 				hayMensaje									= false;
		ArrayList			mensajeSeleccion  						= new ArrayList();
		boolean 				hayError										= false;

		boolean 				falloCreacionArchivoPDF 				= false;
		boolean 				falloEnvioDeCorreo 						= false;

		Acuse 				acuse	 										= null;
		String 				nombreMoneda								= null;
		String 				nombreEpo									= null;

		HashMap				cifrasDeControl							= null;
		ArrayList			detalleDoctosSeleccionados				= new ArrayList();

		SimpleDateFormat 	fechaDeCarga								= null;
		SimpleDateFormat 	horaDeCarga 								= null;

		boolean				noSeMuestraDetalleDeTodosLosDoctos 	= false;
		String 				idDocumento 								= null;

		try {

			// I. Validar parametros de entrada

			// 1. Validar que la clave de la epo proporcionada sea un numero valido
			int icEpo = 0;
			try {

           icEpo = Integer.parseInt(claveEpo);
           if(icEpo < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave EPO no es v�lida.");
			}


			// 2. Validar que la clave del Intermediario Financiero proporcionado sea un numero valido
			int icIF = 0;
			try {

           icIF = Integer.parseInt(claveIF);
           if(icIF < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave Intermediario Financiero no es v�lida.");
			}

			// 3. Validar que la clave de la pyme proporcionada sea un numero valido
			int icPyme = 0;
			try {

           icPyme = Integer.parseInt(clavePyme);
           if(icPyme < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave PYME no es v�lida.");
			}

			// 4. Validar que la clave de la Moneda se valida
			int icMoneda = 0;
			try {

           icMoneda = Integer.parseInt(claveMoneda);
           if(
               icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
               icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
            ){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La clave de la moneda no es v�lida.");
			}

			// 5. Validar la fecha del siguiente d�a h�bil
			try {

           if(
              !"".equals(fechaSiguienteDiaHabil) && !Comunes.esFechaValida(fechaSiguienteDiaHabil,"dd/MM/yyyy")
           ){
              throw new Exception();
			  }

			}catch(Exception e){
				throw new AppException("La fecha proporcionada no es v�lida, esta debe tener el formato: \"dd/mm/aaaa\" o venir como cadena vac�a.");
			}

			// 6. Validar que en el submitString se hayan proporcionado documentos
			if( submitString == null 				|| submitString.length == 0						){
				throw new AppException("No se especific� ning�n documento.");
			}

			// 7. Validar que se haya proporcionado el login del usuario
			if( loginUsuario == null 				|| loginUsuario.trim().equals("") 				){
				throw new AppException("El login del usuario es requerido");
			}

			// 8. Validar que se haya especificado el directorio temporal
			if( directorioTemporal == null 		|| directorioTemporal.trim().equals("") 		){
				throw new AppException("El directorio temporal es requerido.");
			}

			// 9. Validar que se haya especificado el directorio de publicacion
			if( directorioPublicacion == null 	|| directorioPublicacion.trim().equals("") 	){
				throw new AppException("El directorio de publicaci�n es requerido.");
			}

			// 10. Validar que se haya especificado el HashMap con los parametros del usuario
			if( parametrosUsuario == null 		|| parametrosUsuario.size() == 0					){
				throw new AppException("Los datos del usuario son requeridos.");
			}

			// 10.1 Obtener parametros externos adicionales
			parametrosUsuario.put("pais", 						getNombrePaisDeLaPyme()						);
			parametrosUsuario.put("numNafinElectronico", 	getNumeroNafinElectronico(clavePyme)	);
			parametrosUsuario.put("externo",						getAtributoExternoDeLaPyme(clavePyme)	);
			parametrosUsuario.put("nombre",						getNombrePyme(clavePyme)					);

			// II. Obtener instancias de EJB

			// 1. Obtener instancia del EJB de SeleccionDocumento
			ISeleccionDocumento	seleccionDocumento		= null;
			try {
				seleccionDocumento 		= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(SeleccionDocumentoEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Selecci�n Documento");

			}

			// 2. Obtener instancia del EJB de Afiliacion
			Afiliacion afiliacion = null;
			try {

				afiliacion 		= ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(AfiliacionEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Afiliaci�n");

			}

			// 3. Obtener instancia del EJB de Parametros Descuento
			ParametrosDescuento parametrosDescuento = null;
			try {

				parametrosDescuento 		= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(ParametrosDescuentoEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Parametros Descuento");

			}

			// 4. Obtener EJB de Seguridad
			Seguridad	seguridad	= null;
			try {

				seguridad 		= ServiceLocator.getInstance().lookup("SeguridadEJB",Seguridad.class);

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(SeguridadEJB Exception)");
				e.printStackTrace();
				throw new AppException("ERROR INESPERADO:Ocurri� un error al obtener instancia del EJB de Seguridad");

			}

			// III. Validar que se cumplan las condiciones de selecci�n

			// 1. Verificar si la pyme esta bloqueada para el producto descuento electronico
			boolean esPymeBloqueda = false;
			try {

				esPymeBloqueda = afiliacion.pymeBloqueada(clavePyme,SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO);

				if(esPymeBloqueda){
					throw new AppException("No es posible seleccionar documentos debido a que la PYME se encuentra bloqueada");
				}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME se encuentra bloqueda.");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			//Fodea 034-2014 (E)
		   String  esPEntidadGobierno = "N";
        try {

            esPEntidadGobierno = afiliacion.getPyme_EntidaGobierno(clavePyme);

            if("S".equals(esPEntidadGobierno)){
               throw new AppException("No es posible realizar el descuento del documento. Por favor comun�quese al Centro de Atenci�n a Clientes Cd. M�xico 50-89-61-07. Sin costo desde el interior al 01-800-NAFINSA (01-800-623-4672)");
            }

        } catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getResumenDoctosNegociablesPorPyme(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar si la PYME es Entidad gobierno");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{
				  throw e;
				}

        }
		   //Fodea 034-2014 (S)


			// 2. Validar Horario de Descuento Electronico
			String mensajeDiaHabilSiguiente 			= "";
			String tipoServicio 							= "";
			String fechaSiguienteDiaHabilOriginal	= fechaSiguienteDiaHabil;
			try{

				// Determina bajo que modalidad se encuentra el servicio de descuento electronico ofrecido por la epo
				// AB = abierto, SH="siguiente dia h&aacute;bil", excepcion en caso de que el servicio no este abierto
				tipoServicio = Horario.validarHorarioDescElec(Integer.parseInt(SeleccionMovilDoctosBean.DESCUENTO_ELECTRONICO), "PYME", claveEpo);

				// Si el descuento electronico parametrizado para la epo se encuentra bajo la modalidad de "siguiente dia h&aacute;bil"
				if("SH".equals(tipoServicio)){

					// Obtener fecha del siguiente dia h&aacute;bil
					int numeroDias =1;
					String fechaActual = Fecha.getFechaActual();
					while(!Horario.validarDiaSigHabil("PYME", claveEpo, numeroDias)){
						numeroDias +=1;
					}
					fechaSiguienteDiaHabil = Fecha.sumaFechaDias(fechaActual,numeroDias);

					// Validar que la epo y el if operen factoraje 24 hrs
					if (!seleccionDocumento.operaEpoFactoraje24hrs(claveEpo)){
						throw new AppException("La EPO seleccionada no Opera Factoraje 24hrs.");
					}else if (!seleccionDocumento.operaIfFactoraje24hrs(claveIF)){
						throw new AppException("El Intermediario Financiero seleccionado no Opera Factoraje 24hrs.");
					}

					// Agregar mensaje indicando que la operacion de descuento electronico sera realizada
					// en el siguiente dia h&aacute;bil
					mensajeDiaHabilSiguiente="Esta operaci�n ser� programada para el Siguiente D�a H�bil "+fechaSiguienteDiaHabil;
					mensajeSeleccion.add(mensajeDiaHabilSiguiente);

				}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar el horario de servicio");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// Si la fecha del siguiente d�a h�bil no coincide con la obtenida en la pantalla de consulta de documentos
			// Mandar siguiente error
			if( !fechaSiguienteDiaHabilOriginal.equals(fechaSiguienteDiaHabil) ){
				throw new AppException("ERROR INESPERADO: El horario de servicio ha cambiado, por favor vuelva a consultar los documentos.");
			}

			// 3. Revisar si la EPO seleccionada Opera con Fecha de Vencimiento PYME
			boolean 	operaFechaVencimientoPyme 		= false;
			String 	mensajeFechaVencimientoPyme 	= "";
			try {

					// Revisar si la epo seleccionada opera con fecha de vencimiento pyme para el producto descuento electronico
					// si la epo opera con fecha de vencimiento pyme, seleccionDocumento.operaFechaVencPyme = "_pyme", en
					// caso contrario seleccionDocumento.operaFechaVencPyme = ""
					operaFechaVencimientoPyme = !"".equals(seleccionDocumento.operaFechaVencPyme(claveEpo))?true:false;

					// Si es asi, avisar que para las operaciones seleccionadas el intermediario financiero cobrara una comision
					if(operaFechaVencimientoPyme) {
						mensajeFechaVencimientoPyme = "Importante: Las operaciones seleccionadas se les aplicar� una comisi�n por parte del Intermediario Financiero con el que realice la transacci�n.";
						mensajeSeleccion.add(mensajeFechaVencimientoPyme);
					}

			} catch(Exception e) {

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al revisar la operaci�n con fecha de vencimiento.");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// IV. Leer campos de los documentos que se enviar�n al m�todo de selecci�n

			// 1. Leer el correo del usuario, en caso de que este lo haya confirmado
			boolean 	correoSinConfirmar 				= false;
			try {

				correoSinConfirmar 						= seguridad.vvalidarEstatusCorreo(loginUsuario);

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(Seguridad.vvalidarEstatusCorreo Exception)");
				e.printStackTrace();

				throw new AppException("ERROR INESPERADO: Ocurri� un error al consultar el estatus del correo del usuario.");

			}

			correoConfirmado				         	= correoSinConfirmar?false:true;
			if( correoConfirmado && ( correoUsuario == null || correoUsuario.trim().equals("") ) ){
				throw new AppException("El correo del usuario es requerido.");
			}

			// 2. Obtener el Tipo de Limite y el Nombre de la Cuenta
			String tipoLimite								= null;
			String nombreCuenta							= null;
			String nombreIF								= null;
			Vector listaIntermediariosFinancieros 	= null;
			try {

				listaIntermediariosFinancieros 	= seleccionDocumento.ovgetIntermediariosRelacionados(clavePyme, claveEpo, claveMoneda, claveIF, "", SeleccionMovilDoctosBean.FACTORAJE_NORMAL);
				for(int i = 0; i < listaIntermediariosFinancieros.size(); i++) {
					Vector intermediarioFinanciero = (Vector) listaIntermediariosFinancieros.get(i);
					if( claveIF.equals( (String) intermediarioFinanciero.get(0) ) ){
						tipoLimite 		= (String) intermediarioFinanciero.get(7);
						nombreCuenta	= (String) intermediarioFinanciero.get(1);
						nombreIF			= (String) intermediarioFinanciero.get(9);
						break;
					}
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(SeleccionDocumento::ovgetIntermediariosRelacionados Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los datos del intermediario financiero.");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// Validar que el tipo de limite exista
			if( tipoLimite 	== null || tipoLimite.trim().equals("") 	){
				throw new AppException("ERROR INESPERADO: No se pudo determinar el tipo de limite.");
			}
			// Validar que el nombre de la cuenta exista
			if( nombreCuenta 	== null || nombreCuenta.trim().equals("") ){
				throw new AppException("ERROR INESPERADO: El nombre de la cuenta no existe.");
			}
			// Validar que el nombre del IF exista
			if( nombreIF 		== null || nombreIF.trim().equals("") ){
				throw new AppException("ERROR INESPERADO: El nombre del intermediario financiero no existe.");
			}

			// 3. Obtener el valor del tipo de cambio
			String valorTipoDeCambio 	= null;
			try {

				valorTipoDeCambio 		= String.valueOf(seleccionDocumento.fgetTipoCambio(claveMoneda));

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(SeleccionDocumento::fgetTipoCambio Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO: Ocurri� un error al consultar el valor del tipo de cambio.");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  log.error("seleccionarDocumentosParaDescuento(SeleccionDocumento::fgetTipoCambio Exception)");
				  throw e;

				}


			}

			// 4. Obtener tasa plazo para el IF seleccionado
			Vector 		vTasaPlazoIF 	= new Vector();
			try {

				Vector 	vTasaAceptada 	= new Vector();

				// Leer valor de la tasa aceptada para la epo, cliente, moneda, intermediario financiero y siguiente dia h&aacute;bil parametrizado
				vTasaAceptada = seleccionDocumento.ovgetTasaAceptada(clavePyme, claveEpo, claveMoneda, claveIF, fechaSiguienteDiaHabil);

				// Leer tasas aceptadas para el intermediario: leer tasa plazo para el if seleccionado
				for (int i= 0; i<vTasaAceptada.size(); i++) {
					Vector vDatos = (Vector) vTasaAceptada.get(i);
					if (claveIF.equals(vDatos.get(2).toString())) {
						// Leer tasa plazo para el if seleccionado
						vTasaPlazoIF.addElement(vDatos);
					}
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("seleccionarDocumentosParaDescuento(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los plazos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 5. Consultar los documentos a descontar
			Vector 	vDocumentosDescontar = new Vector();
			boolean	fechaLimiteVencida	= false;
			try{

				// Revisar si la busqueda excede o es igual a una fecha limite
				// Si se ha vencido la fecha limite no se consultan los documentos
				fechaLimiteVencida =  "S".equals(parametrosDescuento.getValidarFechaLimite( claveEpo, claveIF ))?true:false;
				// Si es as�, cancelar consulta de documentos y h&aacute;bilitar bandera indicando que la linea de credito ha vencido
				if(fechaLimiteVencida){

					throw new NafinException("DSCT0103");

				// Consultar los documentos a descontar
				}else{

					// Si no se ha vencido la fecha limite, consultar los documentosa descontar
					String txtFechaVencDe	= "";	// Inicio fecha de vencimiento
					String txtFechaVenca 	= "";	// Fin fecha de vencimiento

					String sTipoPiso 			= getTipoPiso(claveIF); // comcat_if.ig_tipo_piso
					vDocumentosDescontar 	= seleccionDocumento.ovgetDocumentosDescontar(
								clavePyme,  		// Clave de la pyme
								claveMoneda,  		// Clave de la moneda
								claveEpo,  			// Clave de la epo
								vTasaPlazoIF,  	// Tasa plazo if
								txtFechaVencDe, 	// Inicio fecha de vencimiento
								txtFechaVenca, 	// Fin fecha de vencimiento
								claveIF, 			// Clave del intermediario financiero
								sTipoPiso			// Tipo de piso
							);
				}

			}catch(Exception e){

				if(e instanceof NafinException){

					NafinException nafinException = (NafinException) e;
					if("SIST0001".equals(nafinException.getCodError())){

						log.error("getDocumentosNegociablesAgrupadosPorTasa(Exception)");
						e.printStackTrace();

						throw new AppException("ERROR INESPERADO:Ocurri� un error al consultar los documentos");

					}else{

						throw new AppException(e.getMessage());

					}

				}else{

				  throw e;

				}

			}

			// 6. Obtener lista de ids de los documentos seleccionados por el usuario
			ArrayList listaIdsDoctosSeleccionados 	= new ArrayList();
			try {

				for(int i=0;i<submitString.length;i++){

					String datos[] 							= submitString[i].split("\\|");
					String documentos[]						= datos[4].split(";");

					for(int j=0;j<documentos.length;j++){
						listaIdsDoctosSeleccionados.add(documentos[j]);
					}

				}

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(Exception)");
				e.printStackTrace();

				throw new AppException("ERROR INESPERADO: Ocurri� un error al leer los ids de los documentos seleccionados.");

			}

			// 7. Extraer una cantidad limitada de documentos, indicada por la variable SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS,
			// para ser mostrados al usuario en el resumen.
			for (int i=0; i<vDocumentosDescontar.size(); i++){

				// Leer datos del documento a descontar
				Vector doctosDescontar  = (Vector) vDocumentosDescontar.get(i);

				// Leer Id del documento
				idDocumento 				= doctosDescontar.get(9).toString();

				if( listaIdsDoctosSeleccionados.contains(idDocumento) ){

					HashMap documento = new HashMap();

					documento.put("numeroDocto", 					doctosDescontar.get(0).toString());
					documento.put("importeFormateado",			"$"+Comunes.formatoDecimal(doctosDescontar.get(5).toString(),	2,true));
					documento.put("importeRecibirFormateado",	"$"+Comunes.formatoDecimal(doctosDescontar.get(8).toString(),	2,true));

					detalleDoctosSeleccionados.add(documento);

					if(detalleDoctosSeleccionados.size() == SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS ){
						break;
					}

				}

			}

			// 8. Construir arreglo de "hiddens"
			String 	seleccionados[]		= new String[listaIdsDoctosSeleccionados.size()];
			int 		index						= 0;

			String 	listaNotasDeCredito 	= ""; // Nota: En esta primera versi�n no est�n soportadas la notas de credito
														   // as� que este campo se env�a vac�o
			String   montosAplicados		= ""; // Nota: Monto de la Nota de Credito Multiple que fue aplicado al documento;
															// este campo se env�a vac�o debido que en esta primera versi�n no se tiene
															// soporte para las notas de credito.
			String 	saldoDocumento			= ""; // Nota: Saldo del Documento, despues de que le fue aplicada una(s) nota(s)
															// de credito multiple; este campo se env�a vac�o debido a que en esta primera
															// versi�n, no se tiene soporte para notas de cr�dito.

			for(int i = 0; i < vDocumentosDescontar.size(); i++){

				Vector vDatosDocumentosDescontar	= (Vector) vDocumentosDescontar.get(i);
				idDocumento						      = vDatosDocumentosDescontar.get(9).toString();

				if( listaIdsDoctosSeleccionados.contains(idDocumento) ){

					// Agregar campos del documento
					seleccionados[index] =
						vDatosDocumentosDescontar.get(9).toString()	+"|"+
						vDatosDocumentosDescontar.get(6).toString()	+"|"+
						vDatosDocumentosDescontar.get(7).toString()	+"|"+
						vDatosDocumentosDescontar.get(8).toString()	+"|"+
						vDatosDocumentosDescontar.get(10).toString()	+"|"+
						claveIF													+"|"+ //sIntermediario
						vDatosDocumentosDescontar.get(0).toString()	+"|"+
						vDatosDocumentosDescontar.get(3).toString()	+"|"+
						vDatosDocumentosDescontar.get(5).toString()	+"|"+
						vDatosDocumentosDescontar.get(11).toString()	+"|"+
						vDatosDocumentosDescontar.get(12).toString()	+"|"+
						vDatosDocumentosDescontar.get(13).toString()	+"|"+
						vDatosDocumentosDescontar.get(14).toString()	+"|"+
						vDatosDocumentosDescontar.get(15).toString()	+"|"+
						vDatosDocumentosDescontar.get(16).toString()	+"|"+
						listaNotasDeCredito									+" |"+
						vDatosDocumentosDescontar.get(19).toString()	+"| |"+
						montosAplicados										+" |"+
						saldoDocumento											+" ";

					// Incrementar contador	de documentos
					index++;

				}

			}

			// Calcular numero total de los documentos a partir del submitstring

			BigDecimal numeroTotalDocumentos					= new BigDecimal("0");
			BigDecimal importeTotalDocumentos				= new BigDecimal("0.00");
			BigDecimal importeTotalRecibirDocumentos		= new BigDecimal("0.00");

			for(int i=0;i<submitString.length;i++){

				//	Obtener los resumenes de los documentos seleccinados para los
				// cuales se realizara la suma.
				String 		datos[] 								= submitString[i].split("\\|");

				BigDecimal 	numeroDocumentos 					= new BigDecimal(datos[0]);
				BigDecimal 	importeDocumentos 				= new BigDecimal(datos[1]);
				BigDecimal	importeRecibirDocumentos		= new BigDecimal(datos[2]);

				//	Agrupar datos
				//	Sumar numero de documentos
				numeroTotalDocumentos 			= numeroTotalDocumentos.add(				numeroDocumentos					);
				//	Sumar importes por moneda
				importeTotalDocumentos 			= importeTotalDocumentos.add(				importeDocumentos					);
				//	Sumar importes a recibir por moneda
				importeTotalRecibirDocumentos	= importeTotalRecibirDocumentos.add(	importeRecibirDocumentos		);

				//	Sumar numero de documentos
				numeroTotalDocumentos 			= numeroTotalDocumentos.setScale(				0,BigDecimal.ROUND_HALF_UP );
				//	Sumar importes por moneda
				importeTotalDocumentos 			= importeTotalDocumentos.setScale(				2,BigDecimal.ROUND_HALF_UP );
				//	Sumar importes a recibir por moneda
				importeTotalRecibirDocumentos	= importeTotalRecibirDocumentos.setScale(		2,BigDecimal.ROUND_HALF_UP );


			}

			// 9. Obtener Monto Total, Total del Descuento, Total de Intereses y Total a Recibir.
			BigDecimal 		totalMonto 						= new BigDecimal("0.00");
			BigDecimal 		totalDescuento 				= new BigDecimal("0.00");
			BigDecimal 		totalIntereses 				= new BigDecimal("0.00");
			BigDecimal 		totalRecibir 					= new BigDecimal("0.00");

			BigDecimal 		montos							= null;
			BigDecimal 		descuentos						= null;
			BigDecimal 		intereses						= null;
			BigDecimal 		recibir							= null;

			Vector 			vDatosDocumentosDescontar 	= null;

			int				ctaDoctos						= 0;

			for(int i = 0; i < vDocumentosDescontar.size(); i++){

				vDatosDocumentosDescontar	= (Vector) vDocumentosDescontar.get(i);
				idDocumento						= vDatosDocumentosDescontar.get(9).toString();

				if( listaIdsDoctosSeleccionados.contains(idDocumento) ){

					montos 			= new BigDecimal( vDatosDocumentosDescontar.get(5).toString() );
					descuentos 		= new BigDecimal( vDatosDocumentosDescontar.get(6).toString() );
					intereses 		= new BigDecimal( vDatosDocumentosDescontar.get(7).toString() );
					recibir 			= new BigDecimal(	vDatosDocumentosDescontar.get(8).toString() );

					totalMonto 		= totalMonto.add(			montos			);
					totalDescuento = totalDescuento.add(	descuentos		);
					totalIntereses = totalIntereses.add(	intereses		);
					totalRecibir 	= totalRecibir.add(		recibir			);

					totalMonto 		= totalMonto.setScale(		2,BigDecimal.ROUND_HALF_UP );
					totalDescuento = totalDescuento.setScale(	2,BigDecimal.ROUND_HALF_UP );
					totalIntereses = totalIntereses.setScale(	2,BigDecimal.ROUND_HALF_UP );
					totalRecibir 	= totalRecibir.setScale(	2,BigDecimal.ROUND_HALF_UP );

					ctaDoctos++;

				}

			}

			// Revisar que haya sincronia con la seleccion realizada
			if(numeroTotalDocumentos.intValue() != ctaDoctos){
				throw new AppException("ERROR INESPERADO: El n�mero de documentos seleccionados cambi�, por favor vuelva a realizar la consulta.");
			}else if( importeTotalDocumentos.compareTo(totalMonto) 				!= 0 ){
				throw new AppException("ERROR INESPERADO: El n�mero de documentos seleccionados cambi�, por favor vuelva a realizar la consulta.");
			}else if( importeTotalRecibirDocumentos.compareTo(totalRecibir) 	!= 0 ){
				throw new AppException("ERROR INESPERADO: El n�mero de documentos seleccionados cambi�, por favor vuelva a realizar la consulta.");
			}

			nombreMoneda	= getNombreMoneda(claveMoneda);
			nombreEpo		= getNombreEpo(claveEpo);

			acuse 			= new Acuse(Acuse.ACUSE_PYME,"1");

			try {

				// Obtener fecha y hora en que se confirm� la selecci�n
				fechaDeCarga	= new SimpleDateFormat ("dd/MM/yyyy");
				horaDeCarga 	= new SimpleDateFormat ("hh:mm:ss a");

				// Confirmar la seleccion de los documentos
				seleccionDocumento.vrealizarSeleccion(
										claveMoneda,
										loginUsuario,
										acuse.toString(),
										totalDescuento.toPlainString(),
										totalIntereses.toPlainString(),
										totalRecibir.toPlainString(),
										seleccionados,
										valorTipoDeCambio,
										claveEpo,
										tipoLimite
									);

			// Si llegara a haber una excepcion, cualquiera que esta haya sido y si es pyme se manda el siguiente mensaje de
			// error generico:
			// 	"No tiene Linea de Fondeo disponible con el Intermediario Seleccionado, contacte al Administrador del Sistema";
			} catch(NafinException nfException) {

				log.error("seleccionarDocumentosParaDescuento(Excepcion al seleccion los documentos para descuento)");
				nfException.printStackTrace();

				throw new AppException("No tiene Linea de Fondeo disponible con el Intermediario Seleccionado, contacte al Administrador del Sistema");

			} catch(Exception e){

				throw e;

			}

			// Actualizar acuse para indicar que la operacion se realizo desde un dispositivo m�vil
			try {

				registraOperacionMovil(acuse.toString());

			}catch(Exception e){

				log.error("seleccionarDocumentosParaDescuento(Exception al registrar operacion movil)");
				log.error(" La operaci�n con n�mero de acuse: " + acuse.toString() +", no pudo ser registrada como operaci�n m�vil");
				e.printStackTrace();

				mensajeSeleccion.add("La operaci�n no pudo ser registrada como m�vil");

			}

			// Si el usuario confirmo su correo, enviarle el detalle de los documentos seleccionados para descuento
			if(correoConfirmado){

				log.debug("seleccionarDocumentosParaDescuento: Se enviara detalle de los documentos seleccionados por el usuario: "+ loginUsuario+", a la siguiente direcci�n: " + correoUsuario);

				// Generar el archivo pdf con el detalle de la selecci�n
				String nombreArchivo = null;
				try {
						nombreArchivo = creaPDFDetalleSeleccion(
							claveEpo,
							directorioTemporal,
							directorioPublicacion,
							fechaDeCarga.format(new java.util.Date()),
							horaDeCarga.format(new java.util.Date()),
							acuse.formatear(),
							parametrosUsuario,
							seleccionados,
							nombreCuenta,
							nombreEpo,
							totalMonto.toPlainString(),
							totalDescuento.toPlainString(),
							totalIntereses.toPlainString(),
							totalRecibir.toPlainString(),
							mensajeDiaHabilSiguiente,
                     nombreMoneda,
                     clavePyme
						);

				}catch(Exception e){

					log.error("seleccionarDocumentosParaDescuento(Exception at creating PDF Detail File)");
					log.error("No se pudo crear el Archivo PDF con el detalle de la operaci�n: " + acuse.toString() + ", para ser enviada al usuario " +loginUsuario + " con direccion de correo: "+correoUsuario);
					e.printStackTrace();

					falloCreacionArchivoPDF = true;

				}

				// Enviar correo con el detalle de la operaci�n
				if(!falloCreacionArchivoPDF){

					try{

						// Definir cuerpo del correo
						String cuerpoCorreo =
							"Estimado(a): " + (String) parametrosUsuario.get("nombre") + "\n\n" +
							"La(s) siguiente(s) operaci�n(es) ha(n) sido registrada(s) en el servicio de Cadenas Productivas de " +
							"Nacional Financiera,S.N.C. por internet:\n\n" ;

						// En caso de que la operaci�n fue programada para el d�a habil siguiente:
						if(!"".equals(mensajeDiaHabilSiguiente)){
							cuerpoCorreo +="\tProgramado para el dia "+fechaSiguienteDiaHabil+
									"\n\n Se anexa archivo de ACUSE DE RECIBO.\n\n" +
									"Atentamente \n\n Nacional Financiera, S.N.C.";
						}else{
							cuerpoCorreo +="\tOperado(s) el d�a " + fechaDeCarga.format(new java.util.Date()) +" a las " + horaDeCarga.format(new java.util.Date()) + " horas " +
									"\n\nSe anexa archivo con el detalle de la operaci�n." +
									"\n\nPara cualquier duda o aclaraci�n favor de comunicarse al 5089-6107 en el D.F., o "  +
									"del interior, sin costo al  01-800- NAFINSA (01 800 623-4672), en un horario de "  +
									"atenci�n de lunes a viernes de 08:00 a 19:00 horas. " +
									"\n\nAtentamente "+
									"\n\nNacional Financiera, S.N.C." +
									"\n\nNota: la fecha y hora de este mensaje ('Enviado el') podr� variar respecto a la "  +
									"fecha/hora real de operaci�n de su transacci�n ('Operado el'). Esto depender� "  +
									"de la configuraci�n de sus servicios de correo y/o computadora en cuanto a su "  +
									"zona horaria. ";
						}

						// Enviar detalle
						CorreoArchivoAdjunto.AttachFile(
							"cadenas@nafin.gob.mx",
							correoUsuario,
							"Documentos a descontar",
							cuerpoCorreo,
							nombreArchivo,
							directorioTemporal
						);

					}catch(Exception e){

						log.error("seleccionarDocumentosParaDescuento( CorreoArchivoAdjunto.AttachFile Exception )");
						log.error("No se pudo enviar al usuario " +loginUsuario + " con direccion de correo: "+correoUsuario+", el archivo con el detalle de los documentos seleccionados:  "+ nombreArchivo );
						e.printStackTrace();

						falloEnvioDeCorreo = true;

					}

				}

			}else{

				log.debug("seleccionarDocumentosParaDescuento: El usuario "+loginUsuario+", no ha confirmado su correo por lo que no es posible enviarle el detalle de los documentos seleccionados para descuento.");

			}

			// Construir cifras de control
			cifrasDeControl = new HashMap();
			cifrasDeControl.put("numeroDeAcuse",				acuse.formatear()										);
			cifrasDeControl.put("fechaDeOperacion",			fechaDeCarga.format(new java.util.Date())		);
			cifrasDeControl.put("horaDeOperacion",				horaDeCarga.format(new java.util.Date())		);
			cifrasDeControl.put("intermediarioFinanciero",	nombreIF													);
			cifrasDeControl.put("usuarioDeCaptura",			(String) parametrosUsuario.get("loginUsuario") + " - " + (String) parametrosUsuario.get("nombreUsuario") );

			// Enviar totales
			respuesta.put("cifrasDeControl",						cifrasDeControl);
			respuesta.put("totalNumeroDoctos",					String.valueOf(listaIdsDoctosSeleccionados.size()));
			respuesta.put("totalImporte",							totalMonto.toPlainString());
			respuesta.put("totalImporteRecibir",				totalRecibir.toPlainString());

			// Enviar totales con formato
			respuesta.put("totalNumeroDoctosFormateado",		Comunes.formatoDecimal(String.valueOf(listaIdsDoctosSeleccionados.size()),0,true)	);
			respuesta.put("totalImporteFormateado",			"$" + Comunes.formatoDecimal(totalMonto.toPlainString(),2,true)									);
			respuesta.put("totalImporteRecibirFormateado",	"$" + Comunes.formatoDecimal(totalRecibir.toPlainString(),2,true)								);

			// Enviar nombre de la moneda
			respuesta.put("nombreMoneda",							nombreMoneda																								);
			respuesta.put("nombreEpo",								nombreEpo																									);

			// Agregar detalle de los documentos seleccionados
			respuesta.put("detalleDoctosSeleccionados", 		detalleDoctosSeleccionados																				);

			// Si la cantidad de documentos seleccionados excede el numero limite de documentos individuales
			// cuyo detalle puede ser mostrado en pantalla.
			if( detalleDoctosSeleccionados.size() > SeleccionMovilDoctosBean.LIMITE_DOCTOS_MOSTRADOS ){
				noSeMuestraDetalleDeTodosLosDoctos = true;
			}
			// Revisar si hay algun mensaje a mostrar
			hayMensaje = mensajeSeleccion.size() > 0?true:false;

		}catch(Throwable e){

			mensajeSeleccion	= new ArrayList();
			if(e instanceof AppException){

				String 	msg					= e.getMessage();
				boolean 	esErrorInesperado = ( msg.length() > 17 && "ERROR INESPERADO:".equals(msg.substring(0,17)) )?true:false;
				if( esErrorInesperado ){

					log.error("seleccionarDocumentosParaDescuento(Exception)");
					log.error("seleccionarDocumentosParaDescuento.claveEpo               = <" + claveEpo               + ">");
					log.error("seleccionarDocumentosParaDescuento.claveIF                = <" + claveIF                + ">");
					log.error("seleccionarDocumentosParaDescuento.clavePyme              = <" + clavePyme              + ">");
					log.error("seleccionarDocumentosParaDescuento.claveMoneda            = <" + claveMoneda            + ">");
					log.error("seleccionarDocumentosParaDescuento.fechaSiguienteDiaHabil = <" + fechaSiguienteDiaHabil + ">");
					log.error("seleccionarDocumentosParaDescuento.submitString           = <" + submitString           + ">");
					log.error("seleccionarDocumentosParaDescuento.loginUsuario           = <" + loginUsuario           + ">");
					log.error("seleccionarDocumentosParaDescuento.correoUsuario				= <" + correoUsuario				+ ">");
					log.error("seleccionarDocumentosParaDescuento.directorioTemporal		= <" + directorioTemporal		+ ">");
					log.error("seleccionarDocumentosParaDescuento.directorioPublicacion	= <" + directorioPublicacion	+ ">");
					log.error("seleccionarDocumentosParaDescuento.parametrosUsuario		= <" + parametrosUsuario		+ ">");
					e.printStackTrace();

					msg 		= msg.substring(17,msg.length());
					hayError = true;

				}

				mensajeSeleccion.add(msg);

			}else{

				log.error("seleccionarDocumentosParaDescuento(Exception)");
				log.error("seleccionarDocumentosParaDescuento.claveEpo               = <" + claveEpo               + ">");
				log.error("seleccionarDocumentosParaDescuento.claveIF                = <" + claveIF                + ">");
				log.error("seleccionarDocumentosParaDescuento.clavePyme              = <" + clavePyme              + ">");
				log.error("seleccionarDocumentosParaDescuento.claveMoneda            = <" + claveMoneda            + ">");
				log.error("seleccionarDocumentosParaDescuento.fechaSiguienteDiaHabil = <" + fechaSiguienteDiaHabil + ">");
				log.error("seleccionarDocumentosParaDescuento.submitString           = <" + submitString           + ">");
				log.error("seleccionarDocumentosParaDescuento.loginUsuario           = <" + loginUsuario           + ">");
				log.error("seleccionarDocumentosParaDescuento.correoUsuario				= <" + correoUsuario				+ ">");
				log.error("seleccionarDocumentosParaDescuento.directorioTemporal		= <" + directorioTemporal		+ ">");
				log.error("seleccionarDocumentosParaDescuento.directorioPublicacion	= <" + directorioPublicacion	+ ">");
				log.error("seleccionarDocumentosParaDescuento.parametrosUsuario		= <" + parametrosUsuario		+ ">");
				e.printStackTrace();

				mensajeSeleccion.add("Ocurri� un error al seleccionar los documentos para descuento. El proceso ha sido cancelado.");
				hayError			= true;

			}

			log.debug("seleccionarDocumentosParaDescuento: Se presentaron problemas al confirmar la selecci�n de documentos para descuento.");
			hayMensaje = true;

			// Resetear parametros de consulta
			respuesta.put("cifrasDeControl",								new HashMap() 		);
			respuesta.put("totalNumeroDoctos",							"0"					);
			respuesta.put("totalImporte",									"0.00"				);
			respuesta.put("totalImporteRecibir",						"0.00"				);

			respuesta.put("totalNumeroDoctosFormateado",				"0"					);
			respuesta.put("totalImporteFormateado",					"$0.00"				);
			respuesta.put("totalImporteRecibirFormateado",			"$0.00"				);

			respuesta.put("nombreMoneda",									""						);
			respuesta.put("nombreEpo",										""						);

			respuesta.put("detalleDoctosSeleccionados", 				new ArrayList()	);

			noSeMuestraDetalleDeTodosLosDoctos 							= false;
			exito 																= false;

		}finally{

			respuesta.put("exito", 											String.valueOf(exito)										);
			respuesta.put("hayMensaje", 									String.valueOf(hayMensaje)									);
			respuesta.put("mensajeSeleccion", 							mensajeSeleccion												);
			respuesta.put("hayError", 										String.valueOf(hayError)									);
			respuesta.put("noSeMuestraDetalleDeTodosLosDoctos",	String.valueOf(noSeMuestraDetalleDeTodosLosDoctos)	);
			respuesta.put("falloCreacionArchivoPDF",					String.valueOf(falloCreacionArchivoPDF)				);
			respuesta.put("falloEnvioDeCorreo",							String.valueOf(falloEnvioDeCorreo)						);

			log.info("seleccionarDocumentosParaDescuento(S)");

		}

		return respuesta;

	}

	// 7. METODOS CORRESPONDIENTES AL MODULO 7 { PERFIL: ADMIN IF ( RESUMEN DE OPERACIONES ) }
	/**
	 *
	 * Devuelve el resumen de operaciones realizadas para una periodo de tiempo con Intermediario Financiero y Moneda espec&iacute;ficas.
	 * En caso de que no se especifique la fecha inicial y ni la fecha final la consulta devuelve las operaciones del d&iacute;a.
	 * El rango m&aacute;ximo de la consulta se encuentra definido por el atributo: {@link  #RANGO_MAXIMO_FECHAS} que actualmente es de 15 d&iacute;as.
	 *
	 * @param claveIF      <tt>String</tt> con el ID(<tt>comcat_if.ic_if</tt>) del IF. N&uacute;mero entero mayor a cero.
	 * @param fechaInicial <tt>String</tt> con la fecha de operaci&oacute;n inicial; la fecha debe tener el formato:
	 *                     <tt>dd/mm/aaaa</tt>.
	 * @param fechaFinal   <tt>String</tt> con la fecha de operaci&oacute;n final; la fecha debe tener el formato:
	 *                     <tt>dd/mm/aaaa</tt>.
	 * @param claveMoneda  <tt>String</tt> con el ID(<tt>comcat_moneda.ic_moneda</tt> de la Moneda.
	 *                     Valores permitidos:
    *                     <ul>
    *                     	 <li>Moneda Nacional: 1</li>
    *                       <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *				<br>
	 *			 Tabla 19. Contenido de la Respuesta.<br>
	 *			 <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"exito"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Con <tt>"true"</tt> indica que la consulta se realiz&oacute; con &eacute;xito, en caso contrario viene en <tt>"false"</tt>.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje con la descripci&oacute;n de &eacute;ste se env&iacute;a en <tt>"mensaje"</tt>.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                	la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado( <tt>listaOperaciones</tt> se env&iacute;a vac&iacute;a).
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"listaOperaciones"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td><tt>ArrayList</tt> de <tt>HashMap</tt> con la lista de las operaciones realizadas. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>, ver Tabla 20.</td>
	 *					</tr>
	 *			</table>
	 * 		<br>
	 *			Tabla 20. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "listaIntermediarios".</tt><br>
	 *			<table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td colspan="2"><b>Llave</b></td>
	 *					<td colspan="2"><b>Valor</b></td>
	 *				</tr>
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Contenido</b></td>
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Descripci&oacute;n</b></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"tipoCadena"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Tipo de Cadena, como aparece en el cat�logo: <tt>comcat_tipo_epo.cg_descripcion</tt>.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroDoctos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de documentos.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"montoTotal"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Monto total de documentos.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroPymes"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de PYMES.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroDoctosFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de documentos con formato de miles .</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"montoTotalFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Monto total de documentos con formato de moneda.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroPymesFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de PYMES con formato de miles.</td>
	 *				</tr>
	 *			</table>
	 */
	public HashMap getResumenOperacionesRealizadasPorIF(
		String claveIF,
		String fechaInicial,
		String fechaFinal,
		String claveMoneda){

		log.info("getResumenOperacionesRealizadasPorIF(E)");

		AccesoDB 			con 			= new AccesoDB();
		StringBuffer		query			= new StringBuffer();
		PreparedStatement ps				= null;
		ResultSet			rs				= null;

		HashMap 				resultado 	= new HashMap();
		ArrayList			registros	= new ArrayList();
		boolean				exito			= true;
		StringBuffer		mensaje		= null;
		boolean				hayMensaje	= false;
		boolean				hayError		= false;

		try {

			// 1. Validar que se hayan especificado los parametros de entrada
			if(claveIF == null || claveIF.trim().equals("")){
				throw new AppException("La Clave del IF es requerida");
			}

			if(claveMoneda == null || claveMoneda.trim().equals("")){
				throw new AppException("La Clave de la Moneda es requerida");
			}

			boolean hayFechaInicial = fechaInicial == null || fechaInicial.trim().equals("")	?false:true;
			boolean hayFechaFinal	= fechaFinal 	== null || fechaFinal.trim().equals("")	?false:true;

			if( !hayFechaInicial && !hayFechaFinal ){

				fechaInicial 	= Fecha.getFechaActual();
				fechaFinal 		= Fecha.getFechaActual();

			} else if( !hayFechaInicial ){

				throw new AppException("La Fecha Inicial es requerida");

			} else if( !hayFechaFinal   ){

				throw new AppException("La Fecha Final es requerida");

			}

			// 2. Validar tipo de dato de los parametros

			// Validar que claveIF sea un numero valido
			int icIF = 0;
			try {

           icIF = Integer.parseInt(claveIF);
           if(icIF < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La Clave del IF no es un n�mero v�lido.");
			}

			// Validar que la clave moneda sea valida: 1 o 54
			int icMoneda = 0;
			try {

				icMoneda = Integer.parseInt(claveMoneda);
				if(
					icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
					icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
				){
              throw new Exception();
            }

         }catch(Exception e){
           throw new AppException("La Clave de la Moneda no es v�lida");
         }

			// Validar que la fecha inicial sea valida
			if(!Comunes.esFechaValida(fechaInicial, "dd/MM/yyyy")){
				throw new AppException("La Fecha Inicial proporcionada, no es v�lida. Esta debe ser de la forma: DD/MM/AAAA");
			}
			// Validar que la fecha final sea valida
			if(!Comunes.esFechaValida(fechaFinal,   "dd/MM/yyyy")){
				throw new AppException("La Fecha Final proporcionada, no es v�lida. Esta debe ser de la forma: DD/MM/AAAA");
			}

			// 3. Validar de fechas

			// Validar que el rango de fecha no exceda los 15 d�as
			int diferenciaDias = Fecha.restaFechas(fechaInicial, fechaFinal);
			if( diferenciaDias > SeleccionMovilDoctosBean.RANGO_MAXIMO_FECHAS ){
				throw new AppException("El rango m�ximo de fechas permitido es de un periodo de "+SeleccionMovilDoctosBean.RANGO_MAXIMO_FECHAS+" d�as.");
			}

			// 4. Realizar consulta
			con.conexionDB();

			query.append(
				"SELECT                                                       "  +
				"	te.cg_descripcion           AS tipo_cadena,                "  +
				"	COUNT (s.ic_documento)      AS numero_doctos,              "  +
				"	SUM (fn_monto_dscto)        AS monto_total,                "  +
				"	COUNT( DISTINCT IC_PYME)    AS numero_pymes,               "  +
				"	d.ic_if                     AS clave_if                    "  +
				"FROM                                                         "  +
				"	com_solicitud   s,                                         "  +
				"	com_documento   d,                                         "  +
				"	comcat_epo      e,                                         "  +
				"	comcat_tipo_epo te                                         "  +
				"WHERE                                                        "  +
				"	s.ic_documento 		=  d.ic_documento                AND  "  +
				"	d.ic_epo					=  e.ic_epo                      AND  "  +
				"	e.ic_tipo_epo 			=  te.ic_tipo_epo (+)            AND  "  +
				"	s.df_fecha_solicitud >= TO_DATE (?, 'DD/MM/YYYY')     AND  "  +
				"	s.df_fecha_solicitud <  TO_DATE (?, 'DD/MM/YYYY') + 1 AND  "  +
				"	d.ic_moneda 			=  ?                             AND  "  +
				"	d.ic_if 					=  ?                                  "  +
				"GROUP BY                                                     "  +
				"	d.ic_if, e.cg_razon_social, te.cg_descripcion              "

			);

			ps = con.queryPrecompilado(query.toString());
			ps.setString(	1, fechaInicial	);
			ps.setString(	2, fechaFinal		);
			ps.setInt(		3, icMoneda			);
			ps.setInt(		4, icIF				);
			rs = ps.executeQuery();

			while(rs.next()){

				HashMap 	registro			= new HashMap();

				String 	tipoCadena 		= (rs.getString("TIPO_CADENA")		== null)?"SIN DESCRIPCION"	:rs.getString("TIPO_CADENA");
				String 	numeroDoctos 	= (rs.getString("NUMERO_DOCTOS")		== null)?"0"					:rs.getString("NUMERO_DOCTOS");
				String 	montoTotal 		= (rs.getString("MONTO_TOTAL")		== null)?"0"					:rs.getString("MONTO_TOTAL");
				String 	numeroPymes 	= (rs.getString("NUMERO_PYMES")		== null)?"0"					:rs.getString("NUMERO_PYMES");

				registro.put("tipoCadena",					tipoCadena				);
				registro.put("numeroDoctos",				numeroDoctos 			);
				registro.put("montoTotal",					montoTotal				);
				registro.put("numeroPymes",  				numeroPymes				);
				registro.put("numeroDoctosFormateado",	Comunes.formatoDecimal(numeroDoctos,0,true) 			);
				registro.put("montoTotalFormateado",	"$" + Comunes.formatoDecimal(montoTotal,2,true)		);
				registro.put("numeroPymesFormateado",  Comunes.formatoDecimal(numeroPymes,0,true)			);

				registros.add(registro);

			}

		}catch(Throwable e){

			log.error("getResumenOperacionesRealizadasPorIF(Exception)");
			log.error("getResumenOperacionesRealizadasPorIF.claveIF      = <" + claveIF      + ">");
			log.error("getResumenOperacionesRealizadasPorIF.fechaInicial = <" + fechaInicial + ">");
			log.error("getResumenOperacionesRealizadasPorIF.fechaFinal   = <" + fechaFinal   + ">");
			log.error("getResumenOperacionesRealizadasPorIF.claveMoneda  = <" + claveMoneda  + ">");
			e.printStackTrace();

			exito 		= false;
			hayMensaje	= true;
			mensaje		= new StringBuffer();
			if( e instanceof AppException ){
				mensaje.append(e.getMessage());
			}else{
				hayError = true;
				mensaje.append("Ocurri� un error al obtener el resumen de las operaciones realizadas: " + e.getMessage() );
			}
			registros = new ArrayList();

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			resultado.put("exito",					String.valueOf(exito)						);
			resultado.put("hayMensaje",			String.valueOf(hayMensaje)					);
			resultado.put("mensaje",				mensaje == null?"":mensaje.toString()	);
			resultado.put("hayError",				String.valueOf(hayError)					);
			resultado.put("listaOperaciones",	registros										);

			log.info("getResumenOperacionesRealizadasPorIF(S)");

		}

		return resultado;

	}

	// 8. METODOS CORRESPONDIENTES AL MODULO 8 { PERFIL: ADMIN EPO ( RESUMEN DE OPERACIONES ) }
	/**
	 *
	 * Devuelve el resumen de operaciones realizadas para una periodo de tiempo con una EPO y Moneda espec&iacute;ficas.
	 * En caso de que no se especifique la fecha inicial y ni la fecha final la consulta devuelve las operaciones del d&iacute;a.
	 * El rango m&aacute;ximo de la consulta se encuentra definido por el atributo: {@link  #RANGO_MAXIMO_FECHAS} que actualmente es de 15 d&iacute;as.
	 *
	 * @param claveEPO     <tt>String</tt> con el ID(<tt>comcat_epo.ic_epo</tt>) del EPO. N&uacute;mero entero mayor a cero.
	 * @param fechaInicial <tt>String</tt> con la fecha de operaci&oacute;n inicial; la fecha debe tener el formato:
	 *                     <tt>dd/mm/aaaa</tt>.
	 * @param fechaFinal   <tt>String</tt> con la fecha de operaci&oacute;n final; la fecha debe tener el formato:
	 *                     <tt>dd/mm/aaaa</tt>.
	 * @param claveMoneda  <tt>String</tt> con el ID(<tt>comcat_moneda.ic_moneda</tt> de la Moneda.
	 *                     Valores permitidos:
    *                     <ul>
    *                     	 <li>Moneda Nacional: 1</li>
    *                       <li>D&oacute;lar Americano: 54</li>
    *                     </ul>
	 *
	 * @return <tt>HashMap</tt> con los siguientes elementos:<br>
	 *				<br>
	 *			 Tabla 21. Contenido de la Respuesta.<br>
	 *			 <table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td colspan="2"><b>Llave</b></td>
	 *						<td colspan="2"><b>Valor</b></td>
	 *					</tr>
	 *					<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Contenido</b></td>
	 *						<td><b>Tipo Objeto</b></td>
	 *						<td><b>Descripci&oacute;n</b></td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"exito"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Con <tt>"true"</tt> indica que la consulta se realiz&oacute; con &eacute;xito, en caso contrario viene en <tt>"false"</tt>.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayMensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hay o no un mensaje.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"mensaje"</td>
	 *						<td><tt>String</tt></td>
	 *                <td>Contenido del mensaje. Este campo s&oacute;lo tiene contenido si <tt>"hayMensaje" == "true"</tt>, en caso contrario se env&iacute;a como una cadena vac&iacute;a.</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"hayError"</td>
	 *						<td><tt>String</tt></td>
	 *                <td><tt>"true"</tt> o <tt>"false"</tt> que indica si hubo o no un error inesperado.<br>Si hubo un error, el mensaje con la descripci&oacute;n de &eacute;ste se env&iacute;a en <tt>"mensaje"</tt>.
	 *							<br>Nota: Hay excepciones de tipo <tt>AppException</tt>, que no son cosideradas como errores inesperados, pero a&uacute;n as&iacute; se sigue
	 *                	la misma l&oacute;gica de resetear los par&aacute;metros de la respuesta como si se hubiera presentado un error inesperado( <tt>listaOperaciones</tt> se env&iacute;a vac&iacute;a).
	 *						</td>
	 *					</tr>
	 *					<tr bgcolor="white" class="TableRowColor">
	 *						<td><tt>String</tt></td>
	 *						<td>"listaOperaciones"</td>
	 *						<td><tt>ArrayList</tt></td>
	 *                <td><tt>ArrayList</tt> de <tt>HashMap</tt> con la lista de las operaciones realizadas. Para ver una descripci&oacute;n del contenido de los <tt>HashMap</tt>, ver Tabla 22.</td>
	 *					</tr>
	 *			</table>
	 * 		<br>
	 *			Tabla 22. Contenido de cada uno de los elementos <tt>HashMap</tt> del <tt>ArrayList "listaIntermediarios".</tt><br>
	 *			<table border="1" width="95%" cellpadding="3" cellspacing="0" >
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td colspan="2"><b>Llave</b></td>
	 *					<td colspan="2"><b>Valor</b></td>
	 *				</tr>
	 *				<tr bgcolor="#ccccff" class="TableSubHeadingColor" >
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Contenido</b></td>
	 *					<td><b>Tipo Objeto</b></td>
	 *					<td><b>Descripci&oacute;n</b></td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"claveIF"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>ID(<tt>comcat_if.ic_if</tt>) del Intermediario Financiero.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"nombreIF"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Nombre Comercial del Intermediario Financiero.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroDoctos"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de documentos.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"montoTotal"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Monto total de documentos.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroPymes"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de PYMES.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroDoctosFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de documentos con formato de miles .</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"montoTotalFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>Monto total de documentos con formato de moneda.</td>
	 *				</tr>
	 *				<tr bgcolor="white" class="TableRowColor">
	 *					<td><tt>String</tt></td>
	 *					<td>"numeroPymesFormateado"</td>
	 *					<td><tt>String</tt></td>
	 *             <td>N�mero total de PYMES con formato de miles.</td>
	 *				</tr>
	 *			</table>
	 */
	public HashMap getResumenOperacionesRealizadasPorEPO(
		String claveEPO,
		String fechaInicial,
		String fechaFinal,
		String claveMoneda){

		log.info("getResumenOperacionesRealizadasPorEPO(E)");

		AccesoDB 			con 			= new AccesoDB();
		StringBuffer		query			= new StringBuffer();
		PreparedStatement ps				= null;
		ResultSet			rs				= null;

		HashMap 				resultado 	= new HashMap();
		ArrayList			registros	= new ArrayList();
		boolean				exito			= true;
		StringBuffer		mensaje		= null;
		boolean				hayMensaje	= false;
		boolean				hayError		= false;

		try {

			// 1. Validar que se hayan especificado los parametros de entrada
			if(claveEPO == null || claveEPO.trim().equals("")){
				throw new AppException("La Clave de la EPO es requerida");
			}

			if(claveMoneda == null || claveMoneda.trim().equals("")){
				throw new AppException("La Clave de la Moneda es requerida");
			}

			boolean hayFechaInicial = fechaInicial == null || fechaInicial.trim().equals("")	?false:true;
			boolean hayFechaFinal	= fechaFinal 	== null || fechaFinal.trim().equals("")	?false:true;

			if( !hayFechaInicial && !hayFechaFinal ){

				fechaInicial 	= Fecha.getFechaActual();
				fechaFinal 		= Fecha.getFechaActual();

			} else if( !hayFechaInicial ){

				throw new AppException("La Fecha Inicial es requerida");

			} else if( !hayFechaFinal   ){

				throw new AppException("La Fecha Final es requerida");

			}

			// 2. Validar tipo de dato de los parametros

			// Validar que claveEPO sea un numero valido
			int icEPO = 0;
			try {

           icEPO = Integer.parseInt(claveEPO);
           if(icEPO < 1){
              throw new Exception();
           }

			}catch(Exception e){
           throw new AppException("La Clave de la EPO no es un n�mero v�lido.");
			}

			// Validar que la clave moneda sea valida: 1 o 54
			int icMoneda = 0;
			try {

				icMoneda = Integer.parseInt(claveMoneda);
				if(
					icMoneda != SeleccionMovilDoctosBean.DOLAR_AMERICANO &&
					icMoneda != SeleccionMovilDoctosBean.MONEDA_NACIONAL
				){
              throw new Exception();
            }

         }catch(Exception e){
           throw new AppException("La Clave de la Moneda no es v�lida");
         }

			// Validar que la fecha inicial sea valida
			if(!Comunes.esFechaValida(fechaInicial, "dd/MM/yyyy")){
				throw new AppException("La Fecha Inicial proporcionada, no es v�lida. Esta debe ser de la forma: DD/MM/AAAA");
			}
			// Validar que la fecha final sea valida
			if(!Comunes.esFechaValida(fechaFinal,   "dd/MM/yyyy")){
				throw new AppException("La Fecha Final proporcionada, no es v�lida. Esta debe ser de la forma: DD/MM/AAAA");
			}

			// 3. Validar de fechas

			// Validar que el rango de fecha no exceda los 15 d�as
			int diferenciaDias = Fecha.restaFechas(fechaInicial, fechaFinal);
			if( diferenciaDias > SeleccionMovilDoctosBean.RANGO_MAXIMO_FECHAS ){
				throw new AppException("El rango m�ximo de fechas permitido es de un periodo de "+SeleccionMovilDoctosBean.RANGO_MAXIMO_FECHAS+" d�as.");
			}

			// 4. Realizar consulta
			con.conexionDB();

			query.append(
				"SELECT                                                       "  +
				"		d.ic_if                  AS clave_if,                   "  +
				"		i.cg_nombre_comercial    AS nombre_comercial_if,        "  +
				"		COUNT (s.ic_documento)   AS numero_doctos,              "  +
				"		SUM (fn_monto_dscto)     AS monto_total,                "  +
				"		COUNT( DISTINCT ic_pyme) AS numero_pymes,               "  +
				"		d.ic_epo                                                "  +
				" FROM                                                        "  +
				" 		com_solicitud s,                                        "  +
				" 		com_documento d,                                        "  +
				" 		comcat_if     i                                         "  +
				"WHERE                                                        "  +
				"	s.ic_documento       =  d.ic_documento                AND  "  +
				"	d.ic_if              =  i.ic_if                       AND  "  +
				"	s.df_fecha_solicitud >= TO_DATE (?, 'DD/MM/YYYY')     AND  "  +
				"	s.df_fecha_solicitud <  TO_DATE (?, 'DD/MM/YYYY') + 1 AND  "  +
				"	d.ic_moneda          = ?                              AND  "  +
				"	d.ic_epo             = ?                                   "  +
				"GROUP BY                                                     "  +
				"	d.ic_if, i.cg_nombre_comercial, d.ic_epo                       "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, fechaInicial	);
			ps.setString(2, fechaFinal		);
			ps.setInt(3,	 icMoneda		);
			ps.setInt(4,	 icEPO			);
			rs = ps.executeQuery();

			while(rs.next()){

				HashMap	registro				= new HashMap();

				String 	claveIF 				= (rs.getString("CLAVE_IF")				== null)?"0"								:rs.getString("CLAVE_IF");
				String	nombreComercialIF = (rs.getString("NOMBRE_COMERCIAL_IF")	== null)?"IF "+claveIF+" SIN NOMBRE":rs.getString("NOMBRE_COMERCIAL_IF");
				String	numeroDoctos 		= (rs.getString("NUMERO_DOCTOS")			== null)?"0"								:rs.getString("NUMERO_DOCTOS");
				String	montoTotal 			= (rs.getString("MONTO_TOTAL")			== null)?"0"								:rs.getString("MONTO_TOTAL");
				String	numeroPymes 		= (rs.getString("NUMERO_PYMES")			== null)?"0"								:rs.getString("NUMERO_PYMES");

				registro.put("claveIF", 					claveIF);
				registro.put("nombreIF",					nombreComercialIF);
				registro.put("numeroDoctos",				numeroDoctos);
				registro.put("montoTotal",					montoTotal);
				registro.put("numeroPymes",				numeroPymes);
				registro.put("numeroDoctosFormateado",	Comunes.formatoDecimal(numeroDoctos,     0,true));
				registro.put("montoTotalFormateado",	"$"+ Comunes.formatoDecimal(montoTotal,  2,true));
				registro.put("numeroPymesFormateado",	Comunes.formatoDecimal(numeroPymes,      0,true));

				registros.add(registro);

			}

		}catch(Throwable e){

			log.info("getResumenOperacionesRealizadasPorEPO(Exception)");
			log.error("getResumenOperacionesRealizadasPorEPO.claveEPO     = <" + claveEPO     + ">");
			log.error("getResumenOperacionesRealizadasPorEPO.fechaInicial = <" + fechaInicial + ">");
			log.error("getResumenOperacionesRealizadasPorEPO.fechaFinal   = <" + fechaFinal   + ">");
			log.error("getResumenOperacionesRealizadasPorEPO.claveMoneda  = <" + claveMoneda  + ">");
			e.printStackTrace();

			exito 		= false;
			hayMensaje	= true;
			mensaje		= new StringBuffer();
			if( e instanceof AppException ){
				mensaje.append(e.getMessage());
			}else{
				hayError = true;
				mensaje.append("Ocurri� un error al obtener el resumen de las operaciones realizadas: " + e.getMessage() );
			}
			registros = new ArrayList();

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			resultado.put("exito",					String.valueOf(exito)						);
			resultado.put("hayMensaje",			String.valueOf(hayMensaje)					);
			resultado.put("mensaje",				mensaje == null?"":mensaje.toString()	);
			resultado.put("hayError",				String.valueOf(hayError)					);
			resultado.put("listaOperaciones",	registros										);

			log.info("getResumenOperacionesRealizadasPorEPO(S)");

		}

		return resultado;

	}
}