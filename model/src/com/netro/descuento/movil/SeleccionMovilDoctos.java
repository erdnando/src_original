package com.netro.descuento.movil;

import java.util.HashMap;
import java.util.ArrayList;
import javax.ejb.Remote;

@Remote
public interface SeleccionMovilDoctos {

    public HashMap getResumenDoctosNegociablesPorPyme(String clavePyme,String claveMoneda);

    public HashMap getListaIntermediariosFinancieros(String claveEpo, String clavePyme, String claveMoneda, String fechaSiguienteDiaHabil);

    public HashMap getDocumentosNegociablesAgrupadosPorTasa(String claveEpo, String claveIF, String clavePyme, String claveMoneda);

    public HashMap getMensajeResumenDoctosSeleccionados(String[] submitString );

    public HashMap getResumenDeLaOperacion(String claveEpo, String claveIF, String clavePyme, String claveMoneda, String fechaSiguienteDiaHabil, String[] submitString);

    public HashMap getParametrosClaveConfirmacion(String loginUsuario);

    public HashMap guardarClaveConfirmacion(String loginUsuario, String claveCesion);

    public HashMap validaParametrosClaveConfirmacion(String loginUsuario, ArrayList posicionesContrasena, boolean confirmarEmail);

    public HashMap seleccionarDocumentosParaDescuento(
	    String claveEpo,
	    String claveIF,
	    String clavePyme,
	    String claveMoneda,
	    String fechaSiguienteDiaHabil,
	    String[] submitString,
	    String loginUsuario,
	    String correoUsuario,
	    String directorioTemporal,
	    String directorioPublicacion,
	    HashMap parametrosUsuario
    );

    public HashMap getResumenOperacionesRealizadasPorIF( String claveIF, String fechaInicial, String fechaFinal, String claveMoneda);

    public HashMap getResumenOperacionesRealizadasPorEPO( String claveEPO, String fechaInicial, String fechaFinal, String claveMoneda);

}