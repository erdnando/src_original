package com.nafin.ws;


public class CargaDocDistribuidoresWSInfo implements java.io.Serializable  {
	public CargaDocDistribuidoresWSInfo() {}
	
	public Integer getCodigoEjecucion() {
		return this.codigoEjecucion;
	}
	
	public Integer getEstatusCarga() {
		return this.estatusCarga;
	}

	public String getResumenEjecucion() {
		return this.resumenEjecucion;
	}
	
	public void setCodigoEjecucion(Integer codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}
	
	public void setEstatusCarga(Integer estatusCarga) {
		this.estatusCarga = estatusCarga;
	}

	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}
	
	public String toString() {
		return 
			"\n" +
			"Codigo Ejecucion: " + codigoEjecucion + "\n" +
			"Estatus Carga de Documentos: " + estatusCarga + "\n" +
			"Resumen de ejecucion " + resumenEjecucion + "\n";
	}

	private Integer codigoEjecucion;
	private Integer estatusCarga;
	private String resumenEjecucion = "";
	
}