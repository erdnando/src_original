package com.nafin.docgarantias;

import java.io.Serializable;

public class MovimientoLinea implements Serializable {
    @SuppressWarnings("compatibility:-6692106404881018293")
    private static final long serialVersionUID = 1L;


    private String nombrePortafolio;
    private String nombreProducto;
    private String nombreIF;
    private double lineaProductoAsignada;
    private double lineaProductoUtilizada;
    private double lineaIFUtilizada;
    private double consumoLineaProducto;
    private double consumoLineaIF;
    private String fechaVigencia;
    private String baseOperacion;
        
    
    
    public MovimientoLinea() {
        super();
    }

    public String getNombrePortafolio() {
        return nombrePortafolio;
    }

    public void setNombrePortafolio(String nombrePortafolio) {
        this.nombrePortafolio = nombrePortafolio;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getNombreIF() {
        return nombreIF;
    }

    public void setNombreIF(String nombreIF) {
        this.nombreIF = nombreIF;
    }

    public double getLineaProductoAsignada() {
        return lineaProductoAsignada;
    }

    public void setLineaProductoAsignada(double lineaProductoAsignada) {
        this.lineaProductoAsignada = lineaProductoAsignada;
    }

    public double getLineaProductoUtilizada() {
        return lineaProductoUtilizada;
    }

    public void setLineaProductoUtilizada(double lineaProductoUtilizada) {
        this.lineaProductoUtilizada = lineaProductoUtilizada;
    }

    public double getLineaIFUtilizada() {
        return lineaIFUtilizada;
    }

    public void setLineaIFUtilizada(double lineaIFUtilizada) {
        this.lineaIFUtilizada = lineaIFUtilizada;
    }

    public double getConsumoLineaProducto() {
        return consumoLineaProducto;
    }

    public void setConsumoLineaProducto(double consumoLineaProducto) {
        this.consumoLineaProducto = consumoLineaProducto;
    }

    public double getConsumoLineaIF() {
        return consumoLineaIF;
    }

    public void setConsumoLineaIF(double consumoLineaIF) {
        this.consumoLineaIF = consumoLineaIF;
    }

    public String getFechaVigencia() {
        return fechaVigencia;
    }

    public void setFechaVigencia(String fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    public String getBaseOperacion() {
        return baseOperacion;
    }

    public void setBaseOperacion(String baseOperacion) {
        this.baseOperacion = baseOperacion;
    }
}
