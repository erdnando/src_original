package com.nafin.docgarantias;

import java.util.ArrayList;

import javax.ejb.Remote;

@Remote
public interface Files2OnBase {

    public ArchivoDocumento guardaArchivo(ArchivoDocumento archivo, String path);
    
    public ArchivoDocumento guardaArchivoDoc(ArchivoDocumento archivo, String path, Integer idDocumento);
    
    public ArchivoDocumento guardaArchivoSol(ArchivoDocumento archivo, String path, Integer idSolicitud);
    
    public ArchivoDocumento guardaArchivoIF(ArchivoDocumento archivo, String path, Integer icDocumento,Integer icIF);
    
    public boolean eliminaArchivo(Integer icArchivo);
    public boolean eliminaArchivoEnTabla(Integer icArchivo);
    public ArchivoDocumento eliminaArchivoSolicitud(Integer icArchivo);
    
    public boolean eliminaArchivosSolicitud(ArrayList<Integer> icArchivos);
    
    public boolean eliminaArchivoDocumento(Integer icArchivo);
    
    public ArchivoDocumento getArchivo(Integer icArchivo, String path);
    
    public boolean eliminaArchivoRelacionIFDoc(Integer icArchivo, Integer icIF, Integer icDocumento);
    
    public ArchivoDocumento eliminaArchivoConvocatoria(Integer icArchivo);
    
    public ArchivoDocumento eliminaArchivoSConv(Integer icConvocatoria);
    
}
