package com.nafin.docgarantias.formalizacion;

import java.io.Serializable;

public class DocumentoIFArchivo implements Serializable{
    @SuppressWarnings("compatibility:-2441350983961496638")
    private static final long serialVersionUID = 1L;

    private String nombreArchivo;
    private Integer icArchivo;
    private Integer icIF;
    private Integer icDocumento;

    public DocumentoIFArchivo(String nombreArchivo, Integer icArchivo, Integer icIF, Integer icDocumento) {
        super();
        this.nombreArchivo = nombreArchivo;
        this.icArchivo = icArchivo;
        this.icIF = icIF;
        this.icDocumento = icDocumento;
    }


    public DocumentoIFArchivo() {
        super();
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }
}
