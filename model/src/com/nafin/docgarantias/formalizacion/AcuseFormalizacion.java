package com.nafin.docgarantias.formalizacion;

import java.io.Serializable;

public class AcuseFormalizacion implements Serializable {
    
    @SuppressWarnings("compatibility:3484484371739929987")
    private static final long serialVersionUID = 1L;

    
    private String nombreUsuario;
    private String institucion;
    private String folioPki;
    private String cadenaDatos;
    private String fechaHora;
    private int tipoFirma;
    private String tipoAccion;
    
    

    public AcuseFormalizacion() {
        super();
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getFolioPki() {
        return folioPki;
    }

    public void setFolioPki(String folioPki) {
        this.folioPki = folioPki;
    }

    public String getCadenaDatos() {
        return cadenaDatos;
    }

    public void setCadenaDatos(String cadenaDatos) {
        this.cadenaDatos = cadenaDatos;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public int getTipoFirma() {
        return tipoFirma;
    }

    public void setTipoFirma(int tipoFirma) {
        this.tipoFirma = tipoFirma;
    }

    public String getTipoAccion() {
        return tipoAccion;
    }

    public void setTipoAccion(String tipoAccion) {
        this.tipoAccion = tipoAccion;
    }
}
