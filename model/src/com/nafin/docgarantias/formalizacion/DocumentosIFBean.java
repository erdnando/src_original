package com.nafin.docgarantias.formalizacion;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "DocumentosIF", mappedName = "DocumentosIF")
@TransactionManagement(TransactionManagementType.BEAN)
public class DocumentosIFBean implements DocumentosIF{
    
    private static final Log log = ServiceLocator.getInstance().getLog(DocumentosIFBean.class);
    
    public DocumentosIFBean() {
        super();
    }


    @Override
    public DocumentoIF getDocumentoIF(Integer icDocumento, Integer icIF) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        DocumentoIF doc = null;
        
        try {
            con.conexionDB();
            strSQL.append(" SELECT d.IC_DOCUMENTO ,  d.IC_TIPO_DOCUMENTO ,  d.IC_SOLICITUD ,  TO_CHAR(d.DT_FECHA_REGISTRO,'DD/MM/YYYY') as DT_FECHA_REGISTRO ,  d.IC_ARCHIVO ,  d.IC_VERSION ,\n" + 
                "  d.IC_DOCUMENTO_ORIGEN ,  d.IC_MOVIMIENTO ,  d.TG_NOMBRE ,  df.IC_IF ,  df.IC_ARCHIVO ,  df.IC_ESTATUS\n" + 
                "FROM gdoc_documento d, \n" + 
                "  gdocrel_documento_if df \n" + 
                "WHERE d.ic_documento = df.ic_documento \n" + 
                "AND df.ic_if         = ? \n" + 
                "AND d.ic_documento   = ?  "); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            log.info("Params: 1-"+icIF);
            log.info("Params: 2-"+icDocumento);
            ps.setInt(1, icIF);
            ps.setInt(2, icDocumento);
            log.info(strSQL.toString());
            rs = ps.executeQuery();
            
            while (rs.next()){
                doc = new DocumentoIF();
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO")) ;
                doc.setIcIF(rs.getInt("IC_IF"));
                doc.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                doc.setFechaRegistro(rs.getString("DT_FECHA_REGISTRO"));
                doc.setIcDocumentoOrigen(rs.getInt("IC_DOCUMENTO_ORIGEN"));
                doc.setIcEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcMovimiento(rs.getInt("IC_MOVIMIENTO"));
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                doc.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                doc.setIcVersion(rs.getInt("IC_VERSION"));
                doc.setNombre(rs.getString("TG_NOMBRE"));
            }       
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información del Documento del IF", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }        
        return doc;
    }
}
