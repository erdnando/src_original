package com.nafin.docgarantias.formalizacion;


public class ConstantesFormalizacion {
    
    /** Parametros referentes a la firma de los Documentos */
    public static final String PARAM_ICARCHIVO = "icArchivo";
    public static final String PARAM_IC_IF = "icIF";
    public static final String PARAM_IC_USUARIO = "icUsuario";
    public static final String PARAM_IC_DOCUMENTO = "icDocumento";
    public static final String PARAM_FOLIO_PKI = "folioPki";
    public static final String PARAM_CADENA_FIRMADA = "cadenaFirmada";
    public static final String PARAM_TIPO_FIRMA = "icTipoFirma";
    public static final String PARAM_FECHA_LIBERACION_INICIO = "fechaLiberacion1";
    public static final String PARAM_FECHA_LIBERACION_FIN = "fechaLiberacion2";
    
    /** Tipo de Firma de los Documentos */
    public static final int FIRMA_JURIDICO = 1;
    public static final int FIRMA_IF = 2;
    public static final int FIRMA_VOBO_NAFIN = 3;
    public static final int FIRMA_NAFIN = 4;
    
    
    /** Filtro de las busquedas de Documentos */
    public static final String FILTRO_FOLIO = "folio";
    public static final String FILTRO_TIPO_DOCUMENTO = "tipoDoc";
    public static final String FILTRO_NOMBR_PRODUCTO = "nombreProducto";
    public static final String FILTRO_ESTATUS = "estatus";
    public static final String FILTRO_EJECUTIVO = "usuarioEjecutivo";
    public static final String FILTRO_IF = "intermediario";
    public static final String FILTRO_FECHA_LIBERACION_INICIO = "fechaLiberacion1";
    public static final String FILTRO_FECHA_LIBERACION_FIN = "fechaLiberacion2";
    
}
