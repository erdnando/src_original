package com.nafin.docgarantias.formalizacion;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

class FooterFirma extends PdfPageEventHelper {
    
    private static final int LIMITE_PARRAFO = 165;
    private static final int MARGEN_INFERIOR = 70;
    private static final int SIZE_LETRA = 5;
    
    private Font ffont;
    private String footer;
    private int limitePaginasConFooter;
    
    public FooterFirma(String footer, int totalPaginasDocumento){
        super();    
        this.footer = footer.toUpperCase();
        this.ffont = new Font(Font.FontFamily.COURIER, SIZE_LETRA, Font.NORMAL);
        this.limitePaginasConFooter = totalPaginasDocumento;
    }

    /** Agrega el footer con el texto enviado en el constructor quitando espacios en blanco y todo en mayusculas
     *  en lineas de LIMITE_PARRAFO cada una, como footer del documento
     * */
    public void onEndPage(PdfWriter writer, Document document) {
        if (writer.getPageNumber() <= this.limitePaginasConFooter){
            PdfContentByte cb = writer.getDirectContent();      
            int renglon = 0;
            int inicio = 0;
            float coordenadaY = 0;
            int fin = this.footer.length();
            int limiteCadena = this.footer.length();
            while( limiteCadena > LIMITE_PARRAFO){
                inicio = renglon*LIMITE_PARRAFO;
                fin = inicio+ LIMITE_PARRAFO;
                String renglonCadena = this.footer.substring(inicio, fin);
                Paragraph pfooter = new Paragraph(renglonCadena.trim(), ffont);
                coordenadaY = MARGEN_INFERIOR-(renglon*SIZE_LETRA);
                ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, pfooter, 50, coordenadaY, 0);        
                renglon ++;
                limiteCadena = limiteCadena - LIMITE_PARRAFO;
            }
            inicio = renglon*LIMITE_PARRAFO;
            coordenadaY = MARGEN_INFERIOR-(renglon*SIZE_LETRA);
            String renglonCadena = this.footer.substring(inicio, this.footer.length());            
            Paragraph pfooter = new Paragraph(renglonCadena, ffont);
            ColumnText.showTextAligned(cb, Element.ALIGN_LEFT, pfooter, 50, coordenadaY, 0);        
        }
    }
       
}