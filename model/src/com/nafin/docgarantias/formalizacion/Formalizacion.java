package com.nafin.docgarantias.formalizacion;

import com.nafin.docgarantias.DocValidacion;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;


@Remote
public interface Formalizacion {
    
    public String FirmaDocumento(String path, Map<String, String> param);
    public boolean guardarAcuse(Map<String,String> params);
    public ArrayList<DocValidacion> getDocFormalizacionIF(Map<String, String> params);
    public ArrayList<DocValidacion> getDocVistoBuenoNAFIN(Map<String, String> params);
    public ArrayList<DocValidacion> getDocFormalizacionNAFIN(Map<String, String> params);
    public ArrayList<ElementoCatalogo> getFirmatesIFDocumento(Integer icIF, Integer icDocumento);
    public  boolean usuarioNafinPuedeFirmarDocumento(Integer ic_if, Integer ic_documento, String ic_usuario);
    public  boolean usuarioIFPuedeFirmarDocumento(Integer ic_if, Integer ic_documento, String ic_usuario);
    public  boolean validaPrelacionDocumentoParaFirmar(Integer icDocumento, Integer icIF);
    public ArrayList<ElementoCatalogo> getCatFolioDocumentoIF(Integer icIF, Integer estatusInicial, Integer estatusFinal);
    public ArrayList<ElementoCatalogo> getCatEstatusDocumento(Integer estatusInicial, Integer estatusFinal);
    public ArrayList<ElementoCatalogo> getCatNombreProductoEmpresa(Integer icEstatusInicial, Integer icIF);
    public ArrayList<ElementoCatalogo> getCatEjecutivos( Integer icEstatusInicial);
    public ArrayList<ElementoCatalogo> getCatIntermediarioDocFormalizacion();
    public ArrayList<ElementoCatalogo> getCatIntermediarios();
    public Map<String,ArrayList> getFilesSoporteDocumento(Integer icDocumento);
    public ArrayList<ElementoCatalogo> getCatFirmantesDocumento(Integer icTipoDocumento);
    public boolean guardaVistoBuenoDoc(Integer icDocumento, Integer icIF, ArrayList<String> usuariosFirmantes, String usuario);
    public boolean guardaRechazo(Integer icDocumento, Integer icIF, String motivo, String usuario);
    public boolean guardaFechaProrroga(String nuevaFecha, Integer icDocumento, Integer icIF);
    public boolean guardaPrelacionDocumentosIF(Integer icIF, Integer icTipoDocumentoFormalizado);
}
