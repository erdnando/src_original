package com.nafin.docgarantias.formalizacion;

import javax.ejb.Remote;


@Remote
public interface DocumentosIF {
    
    public DocumentoIF getDocumentoIF(Integer icDocumento, Integer icIF);

}
