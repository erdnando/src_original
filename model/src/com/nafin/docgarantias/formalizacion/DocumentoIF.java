package com.nafin.docgarantias.formalizacion;

import java.io.Serializable;

public class DocumentoIF implements Serializable {
    @SuppressWarnings("compatibility:-7063684213713207302")
    private static final long serialVersionUID = 1L;

    private Integer icDocumento;
    private Integer icIF;
    private Integer icEstatus;
    private Integer icArchivo;
    private Integer icTipoDocumento;
    private Integer icSolicitud;
    private String fechaRegistro;
    private Integer icVersion;
    private Integer icDocumentoOrigen;
    private Integer icMovimiento;
    private String nombre;

    public DocumentoIF() {
        super();
    }


    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public Integer getIcEstatus() {
        return icEstatus;
    }

    public void setIcEstatus(Integer icEstatus) {
        this.icEstatus = icEstatus;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchivo) {
        this.icArchivo = icArchivo;
    }

    public Integer getIcTipoDocumento() {
        return icTipoDocumento;
    }

    public void setIcTipoDocumento(Integer icTipoDocumento) {
        this.icTipoDocumento = icTipoDocumento;
    }

    public Integer getIcSolicitud() {
        return icSolicitud;
    }

    public void setIcSolicitud(Integer icSolicitud) {
        this.icSolicitud = icSolicitud;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIcVersion() {
        return icVersion;
    }

    public void setIcVersion(Integer icVersion) {
        this.icVersion = icVersion;
    }

    public Integer getIcDocumentoOrigen() {
        return icDocumentoOrigen;
    }

    public void setIcDocumentoOrigen(Integer icDocumentoOrigen) {
        this.icDocumentoOrigen = icDocumentoOrigen;
    }

    public Integer getIcMovimiento() {
        return icMovimiento;
    }

    public void setIcMovimiento(Integer icMovimiento) {
        this.icMovimiento = icMovimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
