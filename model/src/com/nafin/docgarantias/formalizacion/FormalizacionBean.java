package com.nafin.docgarantias.formalizacion;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import com.nafin.docgarantias.ArchivoDocumento;
import com.nafin.docgarantias.DocValidacion;
import com.nafin.docgarantias.Files2OnBase;
import com.nafin.docgarantias.FlujoDocumentos;
import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.io.FileOutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


@Stateless(name = "Formalizacion", mappedName = "Formalizacion")
@TransactionManagement(TransactionManagementType.BEAN)
public class FormalizacionBean implements Formalizacion{


    @Resource
    SessionContext sessionContext;

    private static final Log log = ServiceLocator.getInstance().getLog(FormalizacionBean.class);
    private static final int TERMINOS_CONDICIONES   = 1001;
    private static final int REGLAMENTO_SELECTIVA   = 1002;
    private static final int REGLAMENTO_AUTOMATICA  = 1003;
    private static final int FICHA_NAFIN_GA         = 1004;
    private static final int FICHA_IF_GA            = 1005;
    private static final int ANEXO_IF               = 1006;
    private static final int FICHA_GARANTIA_SELECTIVA= 1007;
    
    private static final int FORMALIZADO_TERMINOS_CONDICIONES= 1;
    private static final int FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_SELECTIVA= 3;
    private static final int FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_AUTOMATICA= 5;
    private static final int FORMALIZADO_TODOS = 7;
    


    public FormalizacionBean() {
        super();
    }

    @SuppressWarnings("oracle.jdeveloper.java.semantic-warning")
    public String FirmaDocumento(String path, Map<String,String> params ) {
        String p_icArchivo = params.get(ConstantesFormalizacion.PARAM_ICARCHIVO);
        String p_icIF = params.get(ConstantesFormalizacion.PARAM_IC_IF);
        String p_icDocumento = params.get(ConstantesFormalizacion.PARAM_IC_DOCUMENTO);
        String rutaDestinoSignedDoc = null;
        String rutaRelativa = "";
        try {
            Files2OnBase fileOnBase = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            Integer icArchivo= Integer.parseInt(p_icArchivo);
            Integer icIF = Integer.parseInt(p_icIF);
            Integer icDocumento = Integer.parseInt(p_icDocumento);
            ArchivoDocumento documento = fileOnBase.getArchivo(icArchivo, path);
            String rutaDocumento = path +"00tmp/41docgarant/"+ documento.getNombreArchivo();
            rutaRelativa = "00tmp/41docgarant/"+ Comunes.cadenaAleatoria(15)+".pdf";
            rutaDestinoSignedDoc = path + rutaRelativa;
            Document document  = new Document(PageSize.LETTER, 0, 0, 0 , 0);
            FileOutputStream fos = new FileOutputStream(rutaDestinoSignedDoc);
            PdfWriter writer = PdfWriter.getInstance(document,  fos);
            PdfReader reader = new PdfReader(rutaDocumento);
            document.open();
            int numeroTotalPaginas = reader.getNumberOfPages();
            // Se agrega el footer con la cadena de datos 
            ArrayList<AcuseFormalizacion> acuse = this.getCadenaFirmaFooter(icDocumento, icIF);
            writer.setPageEvent(new FooterFirma(this.getEstructuraFooter(acuse),numeroTotalPaginas));
            PdfContentByte contentByte = writer.getDirectContent();
            //reader.unethicalreading = true;
            
            int contadorPaginas = 1;
            while(contadorPaginas <= numeroTotalPaginas){
                PdfImportedPage pagina = writer.getImportedPage(reader, contadorPaginas);
                contentByte.addTemplate(pagina, 0, 0);       
                contadorPaginas ++;
                document.newPage();
            }            
            // Agregar la firma al documento
            document.setMargins(50, 50, 50, 80);
            Font fuente = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
            document.newPage();
            Paragraph parrafo = new Paragraph(this.getCadenaFirmaCompleta(acuse), fuente);
            document.add(parrafo); 
            document.close();
            fos.close();
            writer.close();
            reader.close();
            log.info("Se genero el documento corrrectamente");
        } catch (Throwable e) {
            e.printStackTrace();
            throw new AppException("Error al generar el archivo PDF", e);
        }
        return "/"+rutaRelativa;
    }

    /** Obtiene la cadena de datos para firmar en el FOOTER del documento, busca los datos en la BD del documento e Intermediarios enviados
     * */
    public ArrayList<AcuseFormalizacion> getCadenaFirmaFooter(Integer icDocumento, Integer icIf){ 
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ArrayList<AcuseFormalizacion> acuse = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            String sql = "SELECT IC_USUARIO_REGISTRO, tipoAccion, TO_CHAR(DT_FECHA_MOVIMIENTO, 'DD/MM/YYYY-HH24:MI:SS') as DT_FECHA_MOVIMIENTO from (\n" + 
            "SELECT max(DT_FECHA_MOVIMIENTO) as DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, 'Solicit�: ' as tipoAccion FROM " +
                "GDOC_BITACORA_DOC WHERE IC_ESTATUS = 110 AND IC_DOCUMENTO = ?  group by IC_USUARIO_REGISTRO union \n" + 
            "SELECT max(DT_FECHA_MOVIMIENTO) as DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, 'Elabor�: ' as tipoAccion FROM " +
                "GDOC_BITACORA_DOC WHERE IC_ESTATUS = 120 AND IC_DOCUMENTO = ? group by IC_USUARIO_REGISTRO union \n" + 
            "SELECT max(DT_FECHA_MOVIMIENTO) as DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, 'Supervis�: ' as tipoAccion FROM " +
                "GDOC_BITACORA_DOC WHERE IC_ESTATUS = 130 AND IC_DOCUMENTO = ? group by IC_USUARIO_REGISTRO union \n" + 
            "SELECT max(DT_FECHA_MOVIMIENTO) as DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, 'Supervis�: ' as tipoAccion FROM " +
                "GDOC_BITACORA_DOC WHERE IC_ESTATUS = 140 AND IC_DOCUMENTO = ? group by IC_USUARIO_REGISTRO) ";
            ps = con.queryPrecompilado(sql);
            ps.setInt(1, icDocumento);
            ps.setInt(2, icDocumento);
            ps.setInt(3, icDocumento);
            ps.setInt(4, icDocumento);
            rs1 = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while(rs1.next()){
                AcuseFormalizacion ac = new AcuseFormalizacion();
                ac.setTipoFirma(0);
                ac.setCadenaDatos("");
                ac.setFolioPki("");
                ac.setInstitucion("");                
                ac.setFechaHora(rs1.getString("DT_FECHA_MOVIMIENTO"));
                Usuario usuario = utilUsr.getUsuario(rs1.getString("IC_USUARIO_REGISTRO"));
                ac.setNombreUsuario(usuario.getNombreCompleto());
                ac.setTipoAccion(rs1.getString("tipoAccion"));
                acuse.add(ac);
            }            
            
            strSQL.append(" SELECT f.TG_FOLIO_PKI, f.IC_IF, TO_CHAR(f.DT_FECHA_FIRMA, 'DD/MM/YYYY-HH24:MI:SS') as DT_FECHA_FIRMA," +
                " f.IC_USUARIO, I.CG_RAZON_SOCIAL as NOMBRE_IF, f.IC_DOCUMENTO, f.TG_CADENA_FIRMA, f.IC_TIPO_FIRMA \n" + 
                " FROM GDOC_ACUSE_FORMALIZACION f, COMCAT_IF i\n" + 
                " WHERE f.ic_if = i.ic_if  AND f.IC_IF = ? AND f.IC_DOCUMENTO = ? \n" + 
                " ORDER BY f.IC_TIPO_FIRMA, f.DT_FECHA_FIRMA ASC ");
            log.info("ConsultaFirma:"+strSQL.toString());
            log.info("Parametros: "+ icIf+", "+icDocumento);
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, icIf);
            ps.setInt(2, icDocumento);
            rs2 = ps.executeQuery();
            
            while (rs2.next()) {
                AcuseFormalizacion ac = new AcuseFormalizacion();
                ac.setCadenaDatos(rs2.getString("TG_CADENA_FIRMA"));
                ac.setFechaHora(rs2.getString("DT_FECHA_FIRMA"));
                ac.setFolioPki(rs2.getString("TG_FOLIO_PKI"));
                Usuario usuario = utilUsr.getUsuario(rs2.getString("IC_USUARIO"));
                ac.setNombreUsuario(usuario.getNombreCompleto());
                ac.setInstitucion(usuario.getNombreOrganizacionUsuario());
                ac.setTipoFirma(rs2.getInt("IC_TIPO_FIRMA"));
                if (rs2.getInt("IC_TIPO_FIRMA") == ConstantesFormalizacion.FIRMA_JURIDICO){
                    ac.setTipoAccion("Visto Bueno Jur�dico Nafin: ");
                }
                else if(rs2.getInt("IC_TIPO_FIRMA") == ConstantesFormalizacion.FIRMA_NAFIN){
                    ac.setTipoAccion("Representante Nafin/1148: ");
                }
                else{
                    ac.setTipoAccion("Representante "+rs2.getString("NOMBRE_IF")+": ");
                }
                acuse.add(ac);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs1);
            AccesoDB.cerrarResultSet(rs2);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return acuse;
    }


    @Override
    public boolean guardarAcuse(Map<String, String> params) {
        log.info("guardarAcuseGDOC(E)");
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        boolean exito = false;
        Integer icIF = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_IC_IF));
        Integer icDocumento = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_IC_DOCUMENTO));
        String folioPKI = params.get(ConstantesFormalizacion.PARAM_FOLIO_PKI);
        String icUsuario = params.get(ConstantesFormalizacion.PARAM_IC_USUARIO);
        String cadenaFirmada = params.get(ConstantesFormalizacion.PARAM_CADENA_FIRMADA);
        Integer icTipoFirma = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_TIPO_FIRMA));
        boolean procederFirma = false;
        try {
            if (!existeFirmaUsuarioDocumento(icIF, icDocumento, icUsuario)){
                procederFirma = true;
                con.conexionDB();
                String sql = "INSERT INTO GDOC_ACUSE_FORMALIZACION (TG_FOLIO_PKI, IC_IF, DT_FECHA_FIRMA, IC_USUARIO, IC_DOCUMENTO, TG_CADENA_FIRMA, IC_TIPO_FIRMA) \n" + 
                "           SELECT  ?, ?, SYSDATE, ?, ?, ?, ? FROM DUAL WHERE NOT EXISTS \n" + 
                "           (SELECT * FROM GDOC_ACUSE_FORMALIZACION WHERE IC_DOCUMENTO = ? AND IC_USUARIO = ? AND IC_IF = ?) ";
                ps = con.queryPrecompilado(sql);
                ps.setString(1, folioPKI);
                ps.setInt(2, icIF);
                ps.setString(3, icUsuario);
                ps.setInt(4, icDocumento);
                ps.setString(5, cadenaFirmada);
                ps.setInt(6, icTipoFirma);
                ps.setInt(7, icDocumento);
                ps.setString(8, icUsuario);
                ps.setInt(9, icIF);
                int updated = ps.executeUpdate();
                exito = updated == 1 ;                
            }
            else{
                exito = false;
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al guardar el AcuseFormalizacion en la Base de Datos", e);
        } finally {
            if(procederFirma){
                con.terminaTransaccion(exito);
            }
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("guardarAcuseGDOC(S)");
        }   
        return exito;
    }
    
    
    /** Regresa la cadena del Footer del PDF con los acuses enviados (los registros de las firmas con pki)
     *  Se agregan los siguientes usuarios que realizaron las acciones : Elaboro, Solicito, Superviso
     * */
    private String getEstructuraFooter(ArrayList<AcuseFormalizacion> acuses){
        StringBuilder footer = new StringBuilder();
        for(AcuseFormalizacion acuse : acuses){
            if (acuse.getInstitucion().isEmpty()){
                footer.append(acuse.getTipoAccion()+ acuse.getNombreUsuario()+","+acuse.getFechaHora()+"||");
            }
            else{
                footer.append(acuse.getTipoAccion()+ acuse.getNombreUsuario()+","+acuse.getInstitucion()+","+acuse.getFechaHora()+","+acuse.getFolioPki()+"||");    
                }
        }
    return footer.toString();    
    }
    

    /** Obtiene la cadena de datos para firmar en la HOJA DE FIRMAS del documento, busca los datos en la BD del documento e Intermediarios enviados
     * Si icVersion es NULL, se busca la ultima version del documento
     * */    
    private String getCadenaFirmaCompleta(ArrayList<AcuseFormalizacion> acuses){ 
        StringBuilder cadenaFirmasCompletas = new StringBuilder();
        for(AcuseFormalizacion acuse : acuses){
            if (acuse.getInstitucion().isEmpty()){
                cadenaFirmasCompletas.append(acuse.getTipoAccion()+ acuse.getNombreUsuario()+" "+acuse.getFechaHora()+"\n");
            }
            else{
                cadenaFirmasCompletas.append(acuse.getTipoAccion()+ acuse.getNombreUsuario()+" "+acuse.getInstitucion()+" "+
                                acuse.getFechaHora()+" "+acuse.getFolioPki()+"\n"+acuse.getCadenaDatos().replaceAll("\\s+","")+"\n\n");    
            }
        }       
        return cadenaFirmasCompletas.toString();
    }
    
    
    /** Regresa un ArrayList con los documentos del IF que est�n listos para su formalizacion 
     * En el mapa de parametrpos deben llegar:
     * - PARAM_IC_IF
     * - PARAM_IC_USUARIO
     * */
    @Override
    public ArrayList<DocValidacion> getDocFormalizacionIF(Map<String, String> params) {
        Integer icIF = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_IC_IF));
        String icUsuario = params.get(ConstantesFormalizacion.PARAM_IC_USUARIO);
        ArrayList<DocValidacion> retorno = new ArrayList<>();
        UtilUsr utilUsr = new UtilUsr();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT s.ic_solicitud, d.ic_documento, f.ic_archivo as IC_FICHA,\n" + 
                    "  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,\n" + 
                    "  d.ic_version, cd.ic_tipo_documento, \n" + 
                    "  cf.CG_RAZON_SOCIAL AS nombreIF, cf.ic_if as icIF, \n" + 
                    "  Cd.Cd_Nombre_Documento,\n" + 
                    "  NVL(D.Tg_Nombre, s.TG_NOMBRE) AS nombreProducto,\n" + 
                    "  e.Cd_Nombre_Estatus, e.ic_estatus, P.Cd_Nombre_Portafolio as nombrePortafolio,\n" + 
                    "  D.IC_ARCHIVO, \n" + 
                    " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM gdoc_bitacora_doc b where b.ic_estatus = 140 and " +
                    " b.ic_solicitud = s.ic_solicitud) as FECHA_LIBERACION, "+
                    " (select Ic_Usuario_Ejecutivo from gdoc_parametro_if pi where pi.ic_if= cf.ic_if) as icUsuarioAtencion, "+
                    " (select In_Firmas_Requerido from gdoc_parametro_if pi where pi.ic_if= cf.ic_if) as totalFirmasRequeridas, "+
                    " (select IN_DIAS_LIMITE_FIRMA from gdoc_parametro_if pi where pi.ic_if= cf.ic_if) as diasLimiteFormalizacion, "+
                    "  ( SELECT LISTAGG(IC_USUARIO, '/') WITHIN GROUP (ORDER BY IC_USUARIO)  LISTA_NOMBRES \n" + 
                    "       FROM GDOC_ACUSE_FORMALIZACION WHERE IC_IF = f.IC_IF AND IC_DOCUMENTO= D.IC_DOCUMENTO and IC_TIPO_FIRMA = 2) as firmasRegistradas, " + 
                    " DECODE((SELECT count(ic_usuario) FROM GDOC_ACUSE_FORMALIZACION WHERE IC_IF = cf.ic_if AND IC_DOCUMENTO = d.ic_documento and ic_tipo_firma = 2 and ic_usuario = ?), 0, '1', '0') as cumpleAFU, "+
                    " (select IC_ARCHIVO from GDOC_BASE_OPERACION where IC_ESTATUS > 1 and IC_DOCUMENTO = d.ic_documento and IC_IF = f.ic_if) as icArchivoBO "+
                    " FROM gdoc_documento d,\n" + 
                    "  gdoc_solicitud s,\n" + 
                    "  GDOCREL_DOCUMENTO_IF f,\n" + 
                    "  COMCAT_IF cf,\n" + 
                    "  gdoccat_documento cd,\n" + 
                    "  gdoccat_estatus e, gdoccat_portafolio p\n" + 
                    "WHERE d.ic_Solicitud    = s.ic_solicitud\n" + 
                    "AND d.ic_documento      = f.ic_documento\n" + 
                    "AND F.IC_IF             = cf.ic_if\n" + 
                    "AND d.ic_tipo_documento = cd.ic_tipo_documento\n" + 
                    "AND f.ic_estatus        = e.ic_estatus\n" + 
                    " AND p.ic_portafolio = s.ic_portafolio "+
                    "AND f.ic_estatus        > 130" +
                    " AND d.IC_TIPO_DOCUMENTO  IN (select Ic_Tipo_Documento from gdoc_usuario_documento WHERE IS_VERSION_ACTUAL = 1 AND IC_USUARIO = ?)"+
                    " AND s.ic_portafolio in (select ic_portafolio from gdoc_usuario_portafolio where is_version_actual=1 and ic_usuario = ?) "+
                    " AND f.ic_if = ? ORDER BY d.ic_documento DESC \n"); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            log.info("Params= icIF: "+icIF+", IC_USUARIO: "+icUsuario);
            ps.setString(1, icUsuario.trim());
            ps.setString(2, String.format("%1$-9s", icUsuario.trim()));
            ps.setString(3, String.format("%1$-9s", icUsuario.trim()));
            ps.setInt(4, icIF);
            rs = ps.executeQuery();
            while (rs.next()){
                DocValidacion doc = new DocValidacion();
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                doc.setFolioSolicitud(rs.getString("FOLIO"));
                doc.setVersion(rs.getString("IC_VERSION"));
                doc.setNombreIF(rs.getString("nombreIF"));
                doc.setTipoDocumento(rs.getString("cd_nombre_documento"));
                doc.setNombreProducto(rs.getString("nombreProducto"));
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                doc.setIcTipoDocumento(rs.getInt("ic_tipo_documento"));
                doc.setIcEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                doc.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                doc.setFechaLiberacion(rs.getString("FECHA_LIBERACION"));
                doc.setFirmasRequeridas(rs.getInt("totalFirmasRequeridas"));
                doc.setIcArchivoBO(rs.getInt("icArchivoBO"));
                doc.setIcFicha(rs.getInt("IC_FICHA"));
                doc.setCumpleAFU(rs.getBoolean("cumpleAFU"));
                log.info(rs.getString("cumpleAFU")+"CumpleAFU: "+rs.getBoolean("cumpleAFU"));
                String firmantes = rs.getString("firmasRegistradas");
                if(firmantes != null){
                    doc.setIcUsuariosFirmantes(firmantes);
                    String[] listafirmantes = firmantes.split("/");
                    doc.setFirmasRegistradas(listafirmantes.length);
                }
                else{
                    doc.setFirmasRegistradas(0);    
                    }
                doc.setIcIF(rs.getInt("icIF"));
                doc.setNombrePortafolio(rs.getString("nombrePortafolio"));
                String icUsuarioAtencion = rs.getString("icUsuarioAtencion");
                if (icUsuarioAtencion != null){
                    Usuario usrAtencion = utilUsr.getUsuario(icUsuarioAtencion);
                        doc.setNombreEjecutivoAtencion(usrAtencion != null ? usrAtencion.getNombreCompleto() : "no definido");
                }else{
                    doc.setNombreEjecutivoAtencion("Sin parametrizar");
                }
                // Calculo de fecha maxima de formalizacion 
                int diasLimiteFormalizacion = rs.getInt("diasLimiteFormalizacion");
                String fechaInicial = rs.getString("FECHA_LIBERACION");
                String fechaMaxFormalizacion = Fecha.sumaFechaDiasHabiles(fechaInicial, "dd/MM/yyyy", diasLimiteFormalizacion );
                doc.setFechaMaximaFormalizacion(fechaMaxFormalizacion);
                if(doc.getIcTipoDocumento().equals(TERMINOS_CONDICIONES)){
                    doc.setCumplePrelacionIF(true);
                }
                else{
                    doc.setCumplePrelacionIF(this.validaPrelacionDocumentoParaFirmar(doc.getIcDocumento(), icIF));
                }
                retorno.add(doc);
            }       
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }
        return retorno;
    }


    /** Regresa un ArrayList con los documentos que est�n listos para que el administrador Nafin de su visto bueno y 
     *  pasen a la Formalizacion por parte de los apoderados
     * */
    @Override
    public ArrayList<DocValidacion> getDocVistoBuenoNAFIN(Map<String, String> params) {
        Integer icIF = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_IC_IF));
        ArrayList<DocValidacion> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        
        String filtros = "";
        String fechaLiberacion1 = params.get(ConstantesFormalizacion.PARAM_FECHA_LIBERACION_INICIO);
        String fechaLiberacion2 = params.get(ConstantesFormalizacion.PARAM_FECHA_LIBERACION_FIN);
        if (fechaLiberacion1 != null || icIF != null ) filtros += " WHERE ";
        if (fechaLiberacion1 != null){
            filtros += "  a.FECHA_RECEPCION BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') " ;
        }
        if (icIF != null && icIF != 0) { 
            if ( fechaLiberacion1 != null) filtros+= " AND ";
            filtros += " a.ic_IF = ? ";}
        ResultSet rs = null;
        
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(
            " SELECT                                                                                           "+
            "     a.ic_solicitud,                                                                              "+
            "     a.ic_documento,                                                                              "+
            "     a.folio,                                                                                     "+
            "     a.ic_version,                                                                                "+
            "     a.ic_tipo_documento,                                                                         "+
            "     a.nombreif,                                                                                  "+
            "     a.icif,                                                                                      "+
            "     a.cd_nombre_documento,                                                                       "+
            "     TO_CHAR(a.dt_fecha_prorroga, 'DD/MM/YYYY') AS fecha_prorroga,                                "+
            "     a.nombreproducto,                                                                            "+
            "     a.cd_nombre_estatus,                                                                         "+
            "     a.ic_estatus,                                                                                "+
            "     a.nombreportafolio,                                                                          "+
            "     a.ic_archivo,                                                                                "+
            "     TO_CHAR(a.fecha_recepcion, 'DD/MM/YYYY') AS FECHA_RECEPCION,                                 "+
            "     a.ic_usuario_ejecutivo,                                                                      "+
            "     a.nombre_ejecutivo,                                                                          "+
            "     a.totalfirmasrequeridas,                                                                     "+
            "     a.dias_limite_firma,                                                                         "+
            "     a.firmasregistradas                                                                          "+
            " FROM                                                                                             "+
            "     (                                                                                            "+
            "         SELECT                                                                                   "+
            "             s.ic_solicitud,                                                                      "+
            "             d.ic_documento,                                                                      "+
            "             concat(concat(EXTRACT(YEAR FROM s.dt_fecha_creacion), '-'), s.ic_folio) AS folio,    "+
            "             d.ic_version,                                                                        "+
            "             cd.ic_tipo_documento,                                                                "+
            "             cf.cg_razon_social       AS nombreif,                                                "+
            "             cf.ic_if                 AS icif,                                                    "+
            "             cd.cd_nombre_documento,                                                              "+
            "             f.dt_fecha_prorroga,                                                                 "+
            "             nvl(d.tg_nombre, s.tg_nombre) AS nombreproducto,                                     "+
            "             e.cd_nombre_estatus,                                                                 "+
            "             e.ic_estatus,                                                                        "+
            "             p.cd_nombre_portafolio   AS nombreportafolio,                                        "+
            "             d.ic_archivo,                                                                        "+
            "             (                                                                                    "+
            "                 SELECT                                                                           "+
            "                     MAX(dt_fecha_firma)                                                          "+
            "                 FROM                                                                             "+
            "                     gdoc_acuse_formalizacion ac                                                  "+
            "                 WHERE                                                                            "+
            "                     ac.ic_if = cf.ic_if                                                          "+
            "                     AND ac.ic_documento = d.ic_documento                                         "+
            "                     AND ac.ic_tipo_firma = 2                                                     "+
            "             ) AS fecha_recepcion,                                                                "+
            "             s.ic_usuario_ejecutivo,                                                              "+
            "             (                                                                                    "+
            "                 SELECT                                                                           "+
            "                     cd_nombre_usuario                                                            "+
            "                 FROM                                                                             "+
            "                     gdoc_nombre_usuario                                                          "+
            "                 WHERE                                                                            "+
            "                     ic_usuario = TRIM(s.ic_usuario_ejecutivo)                                    "+
            "             ) AS nombre_ejecutivo,                                                               "+
            "             (                                                                                    "+
            "                 SELECT                                                                           "+
            "                     in_firmas_requerido                                                          "+
            "                 FROM                                                                             "+
            "                     gdoc_parametro_if pi                                                         "+
            "                 WHERE                                                                            "+
            "                     pi.ic_if = cf.ic_if                                                          "+
            "             ) AS totalfirmasrequeridas,                                                          "+
            "             (                                                                                    "+
            "                 SELECT                                                                           "+
            "                     in_dias_limite_firma                                                         "+
            "                 FROM                                                                             "+
            "                     gdoc_parametro_if pi                                                         "+
            "                 WHERE                                                                            "+
            "                     pi.ic_if = cf.ic_if                                                          "+
            "             ) AS dias_limite_firma,                                                              "+
            "             (                                                                                    "+
            "                 SELECT                                                                           "+
            "                     COUNT(tg_folio_pki)                                                          "+
            "                 FROM                                                                             "+
            "                     gdoc_acuse_formalizacion                                                     "+
            "                 WHERE                                                                            "+
            "                     ic_if = cf.ic_if                                                             "+
            "                     AND ic_documento = d.ic_documento                                            "+
            "                     AND ic_tipo_firma = 2                                                        "+
            "             ) AS firmasregistradas                                                               "+
            "         FROM                                                                                     "+
            "             gdoc_documento         d,                                                            "+
            "             gdoc_solicitud         s,                                                            "+
            "             gdocrel_documento_if   f,                                                            "+
            "             comcat_if              cf,                                                           "+
            "             gdoccat_documento      cd,                                                           "+
            "             gdoccat_estatus        e,                                                            "+
            "             gdoccat_portafolio     p                                                             "+
            "         WHERE                                                                                    "+
            "             d.ic_solicitud = s.ic_solicitud                                                      "+
            "             AND d.ic_documento = f.ic_documento                                                  "+
            "             AND f.ic_if = cf.ic_if                                                               "+
            "             AND d.ic_tipo_documento = cd.ic_tipo_documento                                       "+
            "             AND f.ic_estatus = e.ic_estatus                                                      "+
            "             AND p.ic_portafolio = s.ic_portafolio                                                "+
            "             AND f.ic_estatus > 140                                                               "+
            "         ORDER BY                                                                                 "+
            "             s.ic_solicitud DESC                                                                  "+
            "     ) a                                                                                          "+
            filtros );
            int numParams = 1;
            ps = con.queryPrecompilado(strSQL.toString()); 
            if (fechaLiberacion1 != null){
                ps.setString(1, fechaLiberacion1 );    
                ps.setString(2, fechaLiberacion2);    
                numParams = 3;
            }
            if (icIF != null && icIF != 0) {
                ps.setInt(numParams, icIF);    
            }
            log.info(strSQL.toString());
            log.info(fechaLiberacion1+", "+fechaLiberacion2);
            rs = ps.executeQuery();
            while (rs.next()){
                DocValidacion doc = new DocValidacion();
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                doc.setFolioSolicitud(rs.getString("FOLIO"));
                doc.setVersion(rs.getString("IC_VERSION"));
                doc.setNombreIF(rs.getString("nombreIF"));
                doc.setTipoDocumento(rs.getString("cd_nombre_documento"));
                doc.setNombreProducto(rs.getString("nombreProducto"));
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                doc.setIcTipoDocumento(rs.getInt("ic_tipo_documento"));
                doc.setIcEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                doc.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                doc.setFechaLiberacion(rs.getString("FECHA_RECEPCION"));
                doc.setFirmasRequeridas(rs.getInt("totalFirmasRequeridas"));
                doc.setFirmasRegistradas(rs.getInt("firmasRegistradas"));
                doc.setIcIF(rs.getInt("icIF"));
                doc.setNombrePortafolio(rs.getString("nombrePortafolio"));
                String icEjecutivo = rs.getString("IC_USUARIO_EJECUTIVO");
                if (icEjecutivo != null){
                    doc.setNombreEjecutivoAtencion(rs.getString("nombre_ejecutivo"));
                }else{
                    doc.setNombreEjecutivoAtencion("Sin parametrizar");
                }
                // Calculo de fecha maxima de formalizacion 
                if (rs.getString("FECHA_PRORROGA") == null){
                    int diasLimiteFormalizacion = rs.getInt("DIAS_LIMITE_FIRMA");
                    String fechaInicial = rs.getString("FECHA_RECEPCION");
                    String fechaMaxFormalizacion = Fecha.sumaFechaDiasHabiles(fechaInicial, "dd/mm/yyyy", diasLimiteFormalizacion );
                    doc.setFechaMaximaFormalizacion(fechaMaxFormalizacion);
                }
                else{
                    doc.setFechaMaximaFormalizacion(rs.getString("FECHA_PRORROGA"));
                }
                doc.setCumplePrelacionIF(this.validaPrelacionDocumentoParaFirmar(doc.getIcDocumento(), icIF));
                retorno.add(doc);
            }       
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }
        return retorno;
    }    
    
    /** Valida que el usuario enviado este registraedo en la tabla de firmantes requeridos y que no haya firmado el documento previamente
     *  Esto hace que ningun usuario pueda firmar dos veces el mismo documento
     * */
    public  boolean usuarioNafinPuedeFirmarDocumento(Integer ic_if, Integer ic_documento, String ic_usuario){
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        NamedParameterStatement nps = null;
        String sql = " SELECT ic_usuario FROM GDOC_FIRMANTES_REQUERIDOS WHERE IC_IF= :icIF AND IC_DOCUMENTO = :icDOC and ic_usuario not in \n" + 
        "( SELECT ic_usuario FROM GDOC_ACUSE_FORMALIZACION WHERE  ic_tipo_firma = 4 and IC_IF = :icIF AND IC_DOCUMENTO = :icDOC)  ";
        log.info(sql);
        log.info("params: "+ ic_if+" "+ic_documento);
        try {
            con.conexionDB();
            nps = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            nps.setInt("icIF", ic_if);
            nps.setInt("icDOC", ic_documento);
            rs = nps.executeQuery();
            while(rs.next()){
                String usuarioBD = rs.getString("ic_usuario");
                if(usuarioBD.equals(ic_usuario)){
                    exito = true;
                    break;
                }
            }
            nps.close();
        }
        catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();            
            throw new AppException("Error inesperado al validar el usuario para firmar", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }        
        return exito;    
    }
    
    
    /** Regresa un ArrayList de los documentos que estan listos para Formalizarse por parte de los apoderados Nafin
     * 
     * */
    @Override
    public ArrayList<DocValidacion> getDocFormalizacionNAFIN(Map<String, String> params) {
        //Integer icIF = Integer.parseInt(params.get(ConstantesFormalizacion.PARAM_IC_IF));
        String icUsuario = params.get(ConstantesFormalizacion.PARAM_IC_USUARIO);
        ArrayList<DocValidacion> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();

            strSQL.append(
            " SELECT /*+use_nl(d,s,f,cf,cd,e,p,b) index(d) index(b)*/                                             "+
            "     s.ic_solicitud,                                                                                 "+
            "     d.ic_documento,                                                                                 "+
            "     concat(concat(EXTRACT(YEAR FROM s.dt_fecha_creacion), '-'), s.ic_folio) AS folio,               "+
            "     d.ic_version,                                                                                   "+
            "     cd.ic_tipo_documento,                                                                           "+
            "     cf.cg_razon_social       AS nombreif,                                                           "+
            "     cf.ic_if                 AS icif,                                                               "+
            "     cd.cd_nombre_documento,                                                                         "+
            "     nvl(d.tg_nombre, s.tg_nombre) AS nombreproducto,                                                "+
            "     e.cd_nombre_estatus,                                                                            "+
            "     e.ic_estatus,                                                                                   "+
            "     p.cd_nombre_portafolio   AS nombreportafolio,                                                   "+
            "     d.ic_archivo,                                                                                   "+
            "     (                                                                                               "+
            "        SELECT                                                                                       "+
            "            TO_CHAR(MAX(dt_fecha_movimiento), 'DD/MM/YYYY')                                          "+
            "        FROM                                                                                         "+
            "            gdoc_bitacora_doc bd                                                                     "+
            "        WHERE                                                                                        "+
            "            bd.ic_if = cf.ic_if                                                                      "+
            "            AND bd.ic_estatus = 160                                                                  "+
            "            AND bd.ic_documento = d.ic_documento                                                     "+
            "     ) AS fecha_recepcion,                                                                           "+
            "     s.ic_usuario_ejecutivo,                                                                         "+
            "    (                                                                                                "+
            "         SELECT LISTAGG(ic_usuario, '|') WITHIN GROUP(ORDER BY  ic_usuario)                          "+
            "         FROM                                                                                        "+
            "             gdoc_firmantes_requeridos fr                                                            "+
            "         WHERE                                                                                       "+
            "             fr.ic_if = cf.ic_if AND fr.ic_documento = d.ic_documento                                "+
            "     ) AS firmantesrequeridos,                                                                       "+
            "     (                                                                                               "+
            "         SELECT LISTAGG(acf.ic_usuario, '|') WITHIN GROUP(ORDER BY acf.ic_usuario)                   "+
            "         FROM gdoc_acuse_formalizacion acf                                                           "+
            "         WHERE  acf.ic_documento = d.ic_documento AND acf.ic_if = cf.ic_if AND acf.ic_tipo_firma = 4 "+
            "     ) AS usuariosfirmaron,                                                                          "+
            "     DECODE((                                                                                        "+
            "         SELECT COUNT(1)                                                                             "+
            "         FROM (SELECT ic_usuario FROM gdoc_firmantes_requeridos                                      "+
            "               WHERE                                                                                 "+
            "                   ic_if = cf.ic_if AND ic_documento = d.ic_documento AND ic_usuario =  ?            "+
            "               MINUS                                                                                 "+
            "               SELECT ic_usuario                                                                     "+
            "               FROM  gdoc_acuse_formalizacion                                                        "+
            "               WHERE                                                                                 "+
            "                     ic_documento = d.ic_documento                                                   "+
            "                     AND ic_if = cf.ic_if                                                            "+
            "                     AND ic_tipo_firma = 4)                                                          "+
            "             ), 1, 1, 0) AS cumpleafu                                                                "+
            " FROM                                                                                                "+
            "     gdoc_documento         d,                                                                       "+
            "     gdoc_solicitud         s,                                                                       "+
            "     gdocrel_documento_if   f,                                                                       "+
            "     comcat_if              cf,                                                                      "+
            "     gdoccat_documento      cd,                                                                      "+
            "     gdoccat_estatus        e,                                                                       "+
            "     gdoccat_portafolio     p,                                                                       "+
            "     gdoc_bitacora_doc      b                                                                        "+
            " WHERE                                                                                               "+
            "     d.ic_solicitud = s.ic_solicitud                                                                 "+
            "     AND d.ic_documento = f.ic_documento                                                             "+
            "     AND f.ic_if = cf.ic_if                                                                          "+
            "     AND d.ic_tipo_documento = cd.ic_tipo_documento                                                  "+
            "     AND f.ic_estatus = e.ic_estatus                                                                 "+
            "     AND d.ic_tipo_documento IN (                                                                    "+
            "         SELECT ic_tipo_documento  FROM gdoc_usuario_documento                                       "+
            "         WHERE is_version_actual = 1 AND ic_usuario = ?                                              "+
            "     )                                                                                               "+
            "     AND f.ic_estatus = b.ic_estatus                                                                 "+
            "     AND p.ic_portafolio = s.ic_portafolio                                                           "+
            "     and d.ic_documento = b.ic_documento                                                             "+
            "     and b.ic_estatus in (170,175)                                                                   "+
            "     and b.DT_FECHA_MOVIMIENTO > (SYSDATE-30)                                                        "+
            " union                                                                                               "+
            " SELECT /*+use_nl(d,s,f,cf,cd,e,p) index(d) index(b)*/                                               "+
            "     s.ic_solicitud,                                                                                 "+
            "     d.ic_documento,                                                                                 "+
            "     concat(concat(EXTRACT(YEAR FROM s.dt_fecha_creacion), '-'), s.ic_folio) AS folio,               "+
            "     d.ic_version,                                                                                   "+
            "     cd.ic_tipo_documento,                                                                           "+
            "     cf.cg_razon_social       AS nombreif,                                                           "+
            "     cf.ic_if                 AS icif,                                                               "+
            "     cd.cd_nombre_documento,                                                                         "+
            "     nvl(d.tg_nombre, s.tg_nombre) AS nombreproducto,                                                "+
            "     e.cd_nombre_estatus,                                                                            "+
            "     e.ic_estatus,                                                                                   "+
            "     p.cd_nombre_portafolio   AS nombreportafolio,                                                   "+
            "     d.ic_archivo,                                                                                   "+
            "     (                                                                                               "+
            "        SELECT                                                                                       "+
            "            TO_CHAR(MAX(dt_fecha_movimiento), 'DD/MM/YYYY')                                          "+
            "        FROM                                                                                         "+
            "            gdoc_bitacora_doc bd                                                                     "+
            "        WHERE                                                                                        "+
            "            bd.ic_if = cf.ic_if                                                                      "+
            "            AND bd.ic_estatus = 160                                                                  "+
            "            AND bd.ic_documento = d.ic_documento                                                     "+
            "     ) AS fecha_recepcion,                                                                           "+
            "     s.ic_usuario_ejecutivo,                                                                         "+
            "    (                                                                                                "+
            "         SELECT LISTAGG(ic_usuario, '|') WITHIN GROUP( ORDER BY ic_usuario)                          "+
            "         FROM gdoc_firmantes_requeridos fr WHERE                                                     "+
            "             fr.ic_if = cf.ic_if                                                                     "+
            "             AND fr.ic_documento = d.ic_documento                                                    "+
            "     ) AS firmantesrequeridos,                                                                       "+
            "     (                                                                                               "+
            "         SELECT LISTAGG(acf.ic_usuario, '|') WITHIN GROUP( ORDER BY acf.ic_usuario )                 "+
            "         FROM gdoc_acuse_formalizacion acf                                                           "+
            "         WHERE                                                                                       "+
            "             acf.ic_documento = d.ic_documento                                                       "+
            "             AND acf.ic_if = cf.ic_if                                                                "+
            "             AND acf.ic_tipo_firma = 4                                                               "+
            "     ) AS usuariosfirmaron,                                                                          "+
            "     DECODE((                                                                                        "+
            "         SELECT COUNT(1)                                                                             "+
            "         FROM (SELECT ic_usuario FROM gdoc_firmantes_requeridos  WHERE                               "+
            "                   ic_if = cf.ic_if  AND ic_documento = d.ic_documento AND ic_usuario = ?            "+
            "               MINUS                                                                                 "+
            "               SELECT ic_usuario  FROM  gdoc_acuse_formalizacion                                     "+
            "               WHERE  ic_documento = d.ic_documento                                                  "+
            "                     AND ic_if = cf.ic_if  AND ic_tipo_firma = 4)                                    "+
            "             ), 1, 1, 0) AS cumpleafu                                                                "+
            " FROM                                                                                                "+
            "     gdoc_documento         d,                                                                       "+
            "     gdoc_solicitud         s,                                                                       "+
            "     gdocrel_documento_if   f,                                                                       "+
            "     comcat_if              cf,                                                                      "+
            "     gdoccat_documento      cd,                                                                      "+
            "     gdoccat_estatus        e,                                                                       "+
            "     gdoccat_portafolio     p                                                                        "+
            " WHERE                                                                                               "+
            "     d.ic_solicitud = s.ic_solicitud                                                                 "+
            "     AND d.ic_documento = f.ic_documento                                                             "+
            "     AND f.ic_if = cf.ic_if                                                                          "+
            "     AND d.ic_tipo_documento = cd.ic_tipo_documento                                                  "+
            "     AND f.ic_estatus = e.ic_estatus                                                                 "+
            "     AND d.ic_tipo_documento IN (                                                                    "+
            "         SELECT ic_tipo_documento                                                                    "+
            "         FROM gdoc_usuario_documento                                                                 "+
            "         WHERE is_version_actual = 1 AND ic_usuario = ?                                              "+
            "     )                                                                                               "+
            "     AND p.ic_portafolio = s.ic_portafolio AND f.ic_estatus = 160                                    "+
            " ORDER BY  1 DESC                                                                                    ");

            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            log.info("Params= "+  icUsuario);
            ps.setString(1, icUsuario);                                 //gdoc_firmantes_requeridos
            ps.setString(2, String.format("%1$-9s", icUsuario.trim()));//gdoc_usuario_documento
            ps.setString(3, icUsuario);                                  //gdoc_firmantes_requeridos
            ps.setString(4, String.format("%1$-9s", icUsuario.trim())); //gdoc_usuario_documento
            rs = ps.executeQuery();
            while (rs.next()){
                UtilUsr utilUsr = new UtilUsr();
                DocValidacion doc = new DocValidacion();
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                doc.setFolioSolicitud(rs.getString("FOLIO"));
                doc.setVersion(rs.getString("IC_VERSION"));
                doc.setNombreIF(rs.getString("nombreIF"));
                doc.setTipoDocumento(rs.getString("cd_nombre_documento"));
                doc.setNombreProducto(rs.getString("nombreProducto"));
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                doc.setIcTipoDocumento(rs.getInt("ic_tipo_documento"));
                doc.setIcEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                doc.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                doc.setFechaLiberacion(rs.getString("FECHA_RECEPCION"));
                doc.setCumpleAFU(rs.getBoolean("cumpleAFU"));
                String firmantes = rs.getString("firmantesRequeridos");
                firmantes = firmantes != null ? firmantes : "";
                String arrFirmantes[] = firmantes.trim().split("\\|");
                String arrUsuariosYaFirmaron = rs.getString("usuariosFirmaron");
                ArrayList<ElementoCatalogo> firmantesRequeridos = new ArrayList<>();
                for(String usu : arrFirmantes){
                    Usuario usuarillo = utilUsr.getUsuario(usu);
                    firmantesRequeridos.add(new ElementoCatalogo(usu, usuarillo.getNombreCompleto()));
                }
                doc.setUsuarioFirmantesRequeridosNafin(firmantesRequeridos);
                
                ArrayList<String> yaFirmaron = new ArrayList<String>(Arrays.asList(arrUsuariosYaFirmaron));
                doc.setIcUsuariosNafinFirmaron(yaFirmaron);
                doc.setIcIF(rs.getInt("icIF"));
                doc.setNombrePortafolio(rs.getString("nombrePortafolio"));
                String icEjecutivo = rs.getString("IC_USUARIO_EJECUTIVO");
                if (icEjecutivo != null){
                    Usuario usrEjecutivo = utilUsr.getUsuario(icEjecutivo);
                    doc.setNombreEjecutivoAtencion(usrEjecutivo.getNombreCompleto());
                }else{
                    doc.setNombreEjecutivoAtencion("Sin parametrizar");
                }
                retorno.add(doc);
            }       
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }
        return retorno;
    }
    
    @Override
    public ArrayList<ElementoCatalogo> getFirmatesIFDocumento(Integer icIF, Integer icDocumento){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        ArrayList<ElementoCatalogo>resultado = new ArrayList<>();
        try {
            con.conexionDB();
            strSQL.append("SELECT TO_CHAR(DT_FECHA_FIRMA,'DD/MM/YYYY HH24:MI:SS') as DT_FECHA_FIRMA, IC_USUARIO  \n" + 
                "  FROM GDOC_ACUSE_FORMALIZACION WHERE IC_DOCUMENTO = ? and IC_IF = ? and IC_TIPO_FIRMA = 2" + 
                " order by dt_fecha_firma desc "); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icDocumento);
            ps.setInt(2, icIF);
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                Usuario usuario = utilUsr.getUsuario(rs.getString("IC_USUARIO"));
                resultado.add(new ElementoCatalogo(usuario.getNombreCompleto(), rs.getString("DT_FECHA_FIRMA")));
            }       
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n de los firmantes del Intermediario", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }        
        return resultado;
    }
    
    /**
     *  Valida que el documento enviado se pueda Firmar, tomando encuenta la Prelacion de documentos establecida
     *  1. Terminos y Condiciones
     *      1.1 Reglamento Operativo de Garantia Selectiva
     *          1.1.1 Todas las fichas del Potafolio 
     *              Selectiva
     *      1.2 Reglamento Operativo de Garantia Automatica
     *          1.2.1 Todas las Fichas del Portafolio:
     *              Empresarial
     *              Emergencias
     *              Subastas
     *              Especiales
     *   
     *   Esto quiere decir que no se puede firmar una Ficha Del portafolio Empresarial si no se ha firmado el Reglamento de Garantia Automatica
     *   Y el Reglamento de Garantia Automatica no se puede firmar si no esta firmado el Documento de Terminos y condiciones
     *   
     *   El metodo regresa true cuando el documento enviado se puede firmar y false en caso contrario
     * */
    public  boolean validaPrelacionDocumentoParaFirmar(Integer icDocumento, Integer icIF){
        boolean resultado = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT d.IC_TIPO_DOCUMENTO,  s.IC_PORTAFOLIO,\n" + 
                "  NVL(\n" + 
                "  (SELECT IS_PRELACION FROM gdoc_prelacion_if WHERE ic_if = ? \n" + 
                "  ), 0) AS PRELACION\n" + 
                "FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d\n" + 
                "WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "AND d.IC_DOCUMENTO   = ? "); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icIF);
            ps.setInt(2, icDocumento);            
            //log.info(strSQL.toString());
            rs = ps.executeQuery();
            Integer portafolio = 0;
            Integer tipoDocumento = 0;
            Integer prelacionIF =0;
            while (rs.next()){
                tipoDocumento = rs.getInt("IC_TIPO_DOCUMENTO");
                portafolio = rs.getInt("IC_PORTAFOLIO");
                prelacionIF = rs.getInt("PRELACION");
            }       
            switch (tipoDocumento) {
                case TERMINOS_CONDICIONES:
                    resultado = true;
                    break;
                case REGLAMENTO_SELECTIVA:
                case REGLAMENTO_AUTOMATICA:
                    resultado = prelacionIF >= FORMALIZADO_TERMINOS_CONDICIONES;
                    break;
                case FICHA_NAFIN_GA:
                case FICHA_IF_GA:
                    resultado = prelacionIF >= FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_AUTOMATICA;
                    break;
                case FICHA_GARANTIA_SELECTIVA:
                case ANEXO_IF:
                    resultado = prelacionIF >= FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_SELECTIVA;
                    break;
            }
            log.info("validaPrelacion :"+ tipoDocumento +" "+prelacionIF);
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n de los firmantes del Intermediario", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }             
        return resultado;   
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatEstatusDocumento(Integer estatusInicial, Integer estatusFinal) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_ESTATUS, CD_NOMBRE_ESTATUS FROM GDOCCAT_ESTATUS WHERE IC_ESTATUS BETWEEN  ? AND ? ORDER BY IC_ESTATUS ASC  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, estatusInicial);
            ps.setInt(2, estatusFinal);
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("IC_ESTATUS");
                String descripcion = rs.getString("CD_NOMBRE_ESTATUS");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    @Override
    public ArrayList<ElementoCatalogo> getCatFolioDocumentoIF(Integer icIF, Integer estatusInicial, Integer estatusFinal) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        
        try {
            con.conexionDB();
            String filtroIF = icIF != null ? " AND i.IC_IF  = ? " : "";
            strSQL.append(" SELECT i.IC_DOCUMENTO,\n" + 
                "  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO\n" + 
                "FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCREL_DOCUMENTO_IF i\n" + 
                "WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "AND d.IC_DOCUMENTO   = i.IC_DOCUMENTO\n" + 
                "AND i.IC_ESTATUS BETWEEN ? AND ? \n" + 
                filtroIF+
                "ORDER BY s.dt_fecha_creacion DESC  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, estatusInicial);
            ps.setInt(2, estatusFinal);
            if (icIF != null){
                ps.setInt(3, icIF);
            }
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("IC_DOCUMENTO");
                String descripcion = rs.getString("FOLIO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }


    @Override
    public ArrayList<ElementoCatalogo> getCatNombreProductoEmpresa( Integer icEstatusInicial, Integer icIF) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        
        try {
            con.conexionDB();
            String sqlFiltroIF = icIF != null ? " and di.IC_IF =  ? " : "";
            strSQL.append(" SELECT d.TG_NOMBRE FROM GDOC_DOCUMENTO d, GDOCREL_DOCUMENTO_IF di WHERE d.IC_DOCUMENTO = di.IC_DOCUMENTO AND  di.IC_ESTATUS >= ? "+ sqlFiltroIF+" ORDER BY TG_NOMBRE  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icEstatusInicial);
            if (icIF != null){
                ps.setInt(2, icIF);
            }
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("TG_NOMBRE");
                String descripcion = rs.getString("TG_NOMBRE");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n getCatNombreProductoEmpresa", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    /** Regeresa el catalofo de Folio, icDpocumento */
    @Override
    public ArrayList<ElementoCatalogo> getCatIntermediarioDocFormalizacion() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        
        try {
            con.conexionDB();
            strSQL.append("SELECT DISTINCT( i.ic_if) icIF, f.cg_razon_social\n" + 
                "	FROM \n" + 
                "	  GDOCREL_DOCUMENTO_IF i ,\n" + 
                "	  COMCAT_IF f\n" + 
                "	WHERE \n" + 
                "	 i.IC_ESTATUS > 130 and\n" + 
                "	 i.IC_IF = f.IC_IF\n" + 
                "	ORDER BY f.cg_razon_social   ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("icIF");
                String descripcion = rs.getString("cg_razon_social");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    @Override
    public ArrayList<ElementoCatalogo> getCatEjecutivos( Integer icEstatusInicial) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        
        try {
            con.conexionDB();

            strSQL.append(" SELECT DISTINCT (s.IC_USUARIO_EJECUTIVO) FROM GDOC_DOCUMENTO d, GDOCREL_DOCUMENTO_IF di, GDOC_solicitud s" +
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD AND d.IC_DOCUMENTO = di.IC_DOCUMENTO AND  di.IC_ESTATUS >= ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icEstatusInicial);
            rs = ps.executeQuery();
            UtilUsr utilUsr =  new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO_EJECUTIVO");
                Usuario usu = utilUsr.getUsuario(val);
                ElementoCatalogo elcat = new ElementoCatalogo(val, usu.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n getCatNombreProductoEmpresa", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }    

    @Override
    public Map<String, ArrayList> getFilesSoporteDocumento(Integer icDocumento) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> archivosSoporte = new ArrayList<>();
        ArrayList<ElementoCatalogo> archivosFormalizar = new ArrayList<>();
        Map<String,ArrayList> retorno = new HashMap<>();
        String sqlSoporte = "";
        String sqlFormalizar = "";
        try {
            con.conexionDB();
            sqlSoporte = " select a.CD_NOMBRE_ARCHIVO, a.ic_archivo from Gdocrel_Solicitud_Archivo sa, GDOC_DOCUMENTO d, GDOC_ARCHIVO a\n" + 
                " WHERE a.ic_archivo = sa.ic_archivo AND sa.ic_solicitud = d.ic_solicitud AND d.ic_documento= ? ";
            ps = con.queryPrecompilado(sqlSoporte); 
            //log.info(sqlSoporte);
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("ic_archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                archivosSoporte.add(elcat);
            }        

            sqlFormalizar = "  select a.CD_NOMBRE_ARCHIVO, a.ic_archivo   from  GDOC_DOCUMENTO d , GDOC_ARCHIVO a\n" + 
                " WHERE   a.ic_archivo = d.ic_archivo AND  d.ic_documento= ? ";
            ps = con.queryPrecompilado(sqlFormalizar); 
            //log.info(sqlFormalizar);
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("ic_archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                archivosFormalizar.add(elcat);
            }               

            String sqlIcFicha = " select f.CD_NOMBRE_ARCHIVO, d.ic_archivo from gdoc_archivo f, gdocrel_documento_if d " +
                    " where d.ic_archivo = f.ic_archivo and d.ic_documento = ? ";
            ps = con.queryPrecompilado(sqlIcFicha);
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();
            while(rs.next()){
                String nombre = rs.getString("CD_NOMBRE_ARCHIVO");
                String icVal = rs.getString("IC_ARCHIVO");
                ElementoCatalogo ele = new ElementoCatalogo(icVal, nombre);
                archivosFormalizar.add(ele);
            }
                    
            retorno.put("documentosSoporte", archivosSoporte);
            retorno.put("documentosFormalizar", archivosFormalizar);
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }


    @Override
    public ArrayList<ElementoCatalogo> getCatFirmantesDocumento(Integer icTipoDocumento) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        String sqlFirmantes = "";
        UtilUsr utilUsr = new UtilUsr();
        try {
            con.conexionDB();
            sqlFirmantes = " SELECT DISTINCT p.IC_USUARIO,\n" + 
            "NVL((select r.ic_usuario  from  GDOC_USUARIO_RELACION r\n" + 
            "WHERE r.IC_TIPO_RELACION =1 AND r.IS_VERSION_ACTUAL =1 AND r.ic_usuario_asociado = p.ic_usuario), '0') as usuario_Mancomunado " + 
            "FROM GDOC_PARAMETROS_USUARIO p, GDOC_USUARIO_DOCUMENTO ud   \n" + 
            "                WHERE p.ic_usuario       = ud.ic_usuario AND ud.Is_Version_Actual = 1  \n" + 
            "                AND p.IC_PERFIL          ='AUTO GESTGAR' AND ud.Ic_Tipo_Documento = ? ";
            ps = con.queryPrecompilado(sqlFirmantes); 
            //log.info(sqlFirmantes);
            ps.setInt(1, icTipoDocumento);
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO").trim();
                Usuario usuario = utilUsr.getUsuario(val);
                String manco = rs.getString("usuario_Mancomunado").trim();
                if (manco.equals("0")){
                    val+="|0";
                }
                else{
                    val+="|"+manco;
                }
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado  en getCatFirmantesDocumento()", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public boolean guardaVistoBuenoDoc(Integer icDocumento, Integer icIF, ArrayList<String> usuariosFirmantes, String usuario) {
        AccesoDB con = new AccesoDB();
        boolean exito = false;
        PreparedStatement ps = null;
        String sqlFirmantes = "";
        String registros = "";
        try {
            con.conexionDB();
            for (int i=0; i < usuariosFirmantes.size(); i++){
                registros +="  INTO GDOC_FIRMANTES_REQUERIDOS (IC_DOCUMENTO, IC_IF, IC_USUARIO, DT_FECHA_REGISTRO) VALUES (? , ?, ? , SYSDATE ) " ; 
            }
            sqlFirmantes = " INSERT ALL \n" + registros  +"  SELECT * FROM DUAL ";
            ps = con.queryPrecompilado(sqlFirmantes); 
            //log.info(sqlFirmantes);
            int indice = 1;
            for (String icUsuario : usuariosFirmantes){
                ps.setInt(indice, icDocumento);    
                indice++;
                ps.setInt(indice, icIF);
                indice++;
                ps.setString(indice, icUsuario);
                indice++;
            }
            int insertados = ps.executeUpdate();
            log.info(insertados+" usuarios Firmantes Requeridos Insertados");
            exito = insertados == usuariosFirmantes.size();
            con.terminaTransaccion(exito);
            if (exito){
                FlujoDocumentos flujoBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
                exito = flujoBean.avanzarDocumentoVistoBuenoNafin(icDocumento, usuario, icIF, null, usuariosFirmantes);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            log.info(sqlFirmantes);
            e.printStackTrace();
            throw new AppException("Error inesperado  en guardaFirmantesRequeridosDocumento()", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return exito;
    }

    @Override
    public boolean guardaRechazo(Integer icDocumento, Integer icIF, String motivo, String usuario) {
        FlujoDocumentos flujoBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
        return flujoBean.avanzarDocumentoFormalizacionNafin(icDocumento, usuario, icIF, motivo);
    }
    
    
    
    
    public boolean guardaFechaProrroga(String nuevaFecha, Integer icDocumento, Integer icIF){
        AccesoDB con = new AccesoDB();
        boolean exito = false;
        PreparedStatement ps = null;
        String sqlUpdateFecha = "";
        try {
            con.conexionDB();
            sqlUpdateFecha =" UPDATE GDOCREL_DOCUMENTO_IF SET DT_FECHA_PRORROGA = TO_DATE(?, 'DD/MM/YYYY HH24:mi:ss') WHERE IC_DOCUMENTO = ? AND IC_IF = ? ";
            ps = con.queryPrecompilado(sqlUpdateFecha); 
            log.info("nueva Fecha: "+nuevaFecha);
            ps.setString(1, nuevaFecha+" 12:00:00");
            ps.setInt(2, icDocumento);
            ps.setInt(3, icIF);
            ps.executeUpdate();
            exito = true;
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado  en guardaFechaProrroga", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
    return exito;    
    }
    
    
    /**
     * Guarda en la tabla gdoc_prelacion_if el nivel de prelacion, dependiendo del documento formalizado que se envia
     * */
    public boolean guardaPrelacionDocumentosIF(Integer icIF, Integer icTipoDocumentoFormalizado){
        boolean exito = false;    
        if (icTipoDocumentoFormalizado.equals(TERMINOS_CONDICIONES) || icTipoDocumentoFormalizado.equals(REGLAMENTO_SELECTIVA) || 
            icTipoDocumentoFormalizado.equals(REGLAMENTO_AUTOMATICA)){
            AccesoDB con = new AccesoDB();
            PreparedStatement ps = null;
            ResultSet rs = null;
            String sqlPrelacionActual = "";
            try {
                con.conexionDB();
                sqlPrelacionActual =" SELECT IS_PRELACION FROM GDOC_PRELACION_IF WHERE IC_IF  = ? ";
                ps = con.queryPrecompilado(sqlPrelacionActual); 
                ps.setInt(1, icIF);
                rs = ps.executeQuery();
                Integer prelacionActual = 0;
                while(rs.next()){
                    prelacionActual = rs.getInt("IS_PRELACION");                    
                }
                
                String sqlUpdatePrelacionIF = "UPDATE  GDOC_PRELACION_IF  SET IS_PRELACION = ? WHERE IC_IF = ?";
                if  (icTipoDocumentoFormalizado.equals(TERMINOS_CONDICIONES)){
                    if (prelacionActual == 0){
                        sqlUpdatePrelacionIF = " INSERT INTO GDOC_PRELACION_IF (IC_IF, IS_PRELACION) VALUES (? , ?)";
                        ps = con.queryPrecompilado(sqlUpdatePrelacionIF);
                        ps.setInt(1, icIF);
                        ps.setInt(2, FORMALIZADO_TERMINOS_CONDICIONES);
                    }
                }
                else if(icTipoDocumentoFormalizado.equals(REGLAMENTO_SELECTIVA)){
                    ps = con.queryPrecompilado(sqlUpdatePrelacionIF);
                    if (prelacionActual == FORMALIZADO_TERMINOS_CONDICIONES) {
                        ps.setInt(1, FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_SELECTIVA);
                    }
                    else if (prelacionActual == FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_AUTOMATICA){
                        ps.setInt(1, FORMALIZADO_TODOS);
                    }
                    else{
                        ps.setInt(1, prelacionActual);
                    }
                    ps.setInt(2, icIF);
                }
                else{
                    ps = con.queryPrecompilado(sqlUpdatePrelacionIF);
                    if (prelacionActual == FORMALIZADO_TERMINOS_CONDICIONES){
                        ps.setInt(1, FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_AUTOMATICA);
                    }                                        
                    else if (prelacionActual == FORMALIZADO_TERMINOS_CONDICIONES_Y_REGLAMENTO_SELECTIVA){
                        ps.setInt(1, FORMALIZADO_TODOS);
                    }
                    else{
                        ps.setInt(1, prelacionActual);
                }
                    ps.setInt(2, icIF);
                }
                ps.executeUpdate();
                exito = true;
            } catch (Throwable e) {
                log.error(e.getMessage());
                e.printStackTrace();
                throw new AppException("Error inesperado en guardaPrelacionDocumentosIF", e);
            } finally {
                con.terminaTransaccion(exito);
                AccesoDB.cerrarStatement(ps);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }             
            }               
        }
        else{
            exito = true;    
        }
        return exito;
    }

    @Override
    public boolean usuarioIFPuedeFirmarDocumento(Integer ic_if, Integer ic_documento, String ic_usuario) {
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        NamedParameterStatement nps = null;
        String sql = " SELECT BITAND (\n" + 
            " DECODE((SELECT count(ic_usuario) FROM GDOC_ACUSE_FORMALIZACION WHERE IC_IF = :icIF AND IC_DOCUMENTO = :icDOC and ic_tipo_firma = 2 and ic_usuario = :icUsuario), 0, 1, 0),\n" + 
            " DECODE ((select count(1) from GDOC_USUARIO_DOCUMENTO ud , GDOC_DOCUMENTO d where  ud.ic_tipo_documento = d.ic_tipo_documento and ud.ic_if = :icIF and " +
            " ud.is_version_actual = 1 and ud.ic_usuario = :icUsuario  and d.ic_documento = :icDOC), 1,1,0)) as cumpleAFU FROM DUAL";
        log.info(sql);
        try {
            con.conexionDB();
            nps = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            nps.setInt("icIF", ic_if);
            nps.setInt("icDOC", ic_documento);
            nps.setString("icUsuario", String.format("%1$-9s", ic_usuario.trim()));
            rs = nps.executeQuery();
            while(rs.next()){
                int cumpleAFU = rs.getInt("cumpleAFU");
                    exito = cumpleAFU == 1;
            }
            nps.close();
        }
        catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();            
            throw new AppException("Error inesperado al validar el usuarioIF para firmar", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }        
        return exito;  
    }
    
    
    public ArrayList<ElementoCatalogo> getCatIntermediarios(){
        ArrayList<ElementoCatalogo>  lista = new ArrayList<>(); 
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String sql = " select ic_if, cg_razon_social from comcat_if where ic_if  in \n" + 
        " (select distinct ic_if from gdocrel_documento_if where ic_estatus = 160) " ;
        log.info(sql);
        try {
            con.conexionDB();
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                lista.add(new ElementoCatalogo(rs.getInt("ic_if")+"", rs.getString("cg_razon_social")));
            }
        }
        catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();            
            throw new AppException("Error inesperado al obtener el catalogo de Intermediarios", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }        
        return lista;
    }
    
    /**
     * Regresa true si el usuario ya ha firmado el doumento para el IF envidao
     * */
    private boolean existeFirmaUsuarioDocumento(Integer ic_if, Integer ic_documento, String ic_usuario) {
        log.info("existeFirmaUsuarioDocumento(E)");
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        NamedParameterStatement nps = null;
        String sql = " SELECT COUNT(TG_FOLIO_PKI) FIRMAS FROM GDOC_ACUSE_FORMALIZACION WHERE IC_DOCUMENTO = :icDoc AND IC_IF = :icIF  AND IC_USUARIO = :icUsuario ";
        log.info(sql+ " ["+ic_documento+", "+ic_usuario+", "+ic_if+"]");
        try {
            con.conexionDB();
            nps = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            nps.setInt("icIF", ic_if);
            nps.setInt("icDoc", ic_documento);
            nps.setString("icUsuario", ic_usuario);
            rs = nps.executeQuery();
            int numeroFirmas = 0;
            while(rs.next()){
                numeroFirmas = rs.getInt("FIRMAS");
            }
            nps.close();
            log.info("FIRMAS EXISTENTES: "+numeroFirmas);
            exito = numeroFirmas > 0;
        }
        catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();            
            throw new AppException("Error inesperado al validar las firmas del documento", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }     
        log.info("existeFirmaUsuarioDocumento(S)");
        return exito;  
    }
}
