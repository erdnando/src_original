package com.nafin.docgarantias;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "DocGarantias", mappedName = "DocGarantias")
@TransactionManagement(TransactionManagementType.BEAN)
public class DocGarantiasBean implements DocGarantias {

    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(DocGarantiasBean.class);

    public DocGarantiasBean() {}


    @Override
    public ArrayList<MovimientoLinea> getMonitorLineas(ArrayList<String> param) {
        log.info("getMonitorLineas(E)");                
        log.info("Parametros Recibidos: "+param);
        
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MovimientoLinea> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        StringBuilder sqlParams = new StringBuilder();
        int paramsPorSetear = 0;
        if (!param.get(ConstantesMonitor.PORTAFOLIO).isEmpty()){ 
            sqlParams.append(" AND m.CVE_PORTAFOLIO = ? "); 
            sqlParams.append(" AND m.CVE_CARTERA = ? "); 
            paramsPorSetear = paramsPorSetear |3;
        }
        if (!param.get(ConstantesMonitor.PRODUCTO).isEmpty()){ sqlParams.append("AND m.CLAVE_PROYECTO = ? "); paramsPorSetear=paramsPorSetear|4;}
        if (!param.get(ConstantesMonitor.INTERMEDIARIO).isEmpty()){ sqlParams.append("AND m.INTER_CLAVE = ? "); paramsPorSetear=paramsPorSetear|8;}
        if (!param.get(ConstantesMonitor.BASE_OPERACION).isEmpty()){ sqlParams.append("AND m.TPRO_CLAVE = ? ");paramsPorSetear=paramsPorSetear|16;}    
        if (!param.get(ConstantesMonitor.FECHA_VIGENCIA).isEmpty()){ 
            sqlParams.append(" AND trunc(sp.FECHA_VIGENCIA) = TO_DATE( ? ,'DD/MM/YYYY') "); 
            paramsPorSetear=paramsPorSetear|32;
        }
        if (!param.get(ConstantesMonitor.ESTATUS_LINEA).isEmpty()){ 
            if(ConstantesMonitor.ESTATUS_VERDE.equals(Integer.parseInt(param.get(ConstantesMonitor.ESTATUS_LINEA))) ){
                sqlParams.append(" AND ((NVL(m.CONSUMO_PORTAFOLIO,0)/m.LIMITE_PORTAFOLIO)*100) BETWEEN 0.0 AND 33.334 "); 
            }
            if(ConstantesMonitor.ESTATUS_AMARILLO.equals(Integer.parseInt(param.get(ConstantesMonitor.ESTATUS_LINEA))) ){
                sqlParams.append(" AND ((NVL(m.CONSUMO_PORTAFOLIO,0)/m.LIMITE_PORTAFOLIO)*100) BETWEEN 33.334 AND 66.667 ");      
            }
            if(ConstantesMonitor.ESTATUS_ROJO.equals(Integer.parseInt(param.get(ConstantesMonitor.ESTATUS_LINEA))) ){
                sqlParams.append(" AND ((NVL(m.CONSUMO_PORTAFOLIO,0)/m.LIMITE_PORTAFOLIO)*100) BETWEEN 66.667 AND 100.0 "); 
            }            
            paramsPorSetear=paramsPorSetear|64;
        }

        try {
            con.conexionDB();
            strSQL.append(
            " SELECT   \n" + 
            "	f.Cg_Descripcion as Nombre_if,   \n" + 
            "	p.Cg_Descripcion as Nombre_Portafolio,   \n" + 
            "			sp.DESCRIPCION as nombreProyecto,   \n" + 
            "			m.TPRO_CLAVE,   \n" + 
            "           TO_CHAR(sp.FECHA_VIGENCIA,'DD/MM/YYYY') FECHA_VIGENCIA,     \n" + 
            "			m.LIMITE_PORTAFOLIO,   \n" + 
            "			m.CONSUMO_PORTAFOLIO,   \n" + 
            "			m.CONSUMO_TPRO_CLAVE   \n" + 
            " FROM   \n" + 
            "	SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m,   \n" + 
            "	gti_if_siag f,   \n" + 
            "	GTICAT_PORTAFOLIOS p,   \n" + 
            "   SIAG_PROYECTOS@NAFELE_SIAG sp  \n" + 
            " WHERE   \n" + 
            "	f.ic_if_siag = m.INTER_CLAVE and   \n" + 
            "	p.ic_cartera = m.CVE_CARTERA and   \n" + 
            "	p.ic_portafolio = m.CVE_PORTAFOLIO and   \n" + 
            "    sp.CLAVE_PROYECTO  = m.CLAVE_PROYECTO \n"+
            sqlParams+
            " ORDER BY \n" + 
            "	m.INTER_CLAVE, m.FECHA_REGISTRO  ");
            
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());            

            if (!param.get(ConstantesMonitor.PORTAFOLIO).isEmpty()){ 
                    String[] splitPortafolio = param.get(ConstantesMonitor.PORTAFOLIO).split("-");
                    ps.setInt(1, Integer.parseInt(splitPortafolio[0]));
                    log.info(Integer.parseInt(splitPortafolio[0]));
                    ps.setInt(2, Integer.parseInt(splitPortafolio[1]));
                    log.info(Integer.parseInt(splitPortafolio[1]));
                }
            if (!param.get(ConstantesMonitor.PRODUCTO).isEmpty()){ 
                ps.setInt(Integer.bitCount(paramsPorSetear & 7), Integer.parseInt(param.get(ConstantesMonitor.PRODUCTO)) );
                log.info(Integer.parseInt(param.get(ConstantesMonitor.PRODUCTO)));
            }            
            if (!param.get(ConstantesMonitor.INTERMEDIARIO).isEmpty()){ 
                ps.setInt(Integer.bitCount(paramsPorSetear & 15), Integer.parseInt(param.get(ConstantesMonitor.INTERMEDIARIO)) );
                log.info(Integer.parseInt(param.get(ConstantesMonitor.INTERMEDIARIO)));
            }
            if (!param.get(ConstantesMonitor.BASE_OPERACION).isEmpty()){ 
                ps.setInt(Integer.bitCount(paramsPorSetear & 31), Integer.parseInt(param.get(ConstantesMonitor.BASE_OPERACION)) ); 
                log.info(Integer.parseInt(param.get(ConstantesMonitor.BASE_OPERACION)));
            }        
            if (!param.get(ConstantesMonitor.FECHA_VIGENCIA).isEmpty()){
                ps.setString(Integer.bitCount(paramsPorSetear &63), param.get(ConstantesMonitor.FECHA_VIGENCIA));
                log.info(param.get(ConstantesMonitor.FECHA_VIGENCIA));
            }           
            rs = ps.executeQuery();
           
            while (rs.next()){
                MovimientoLinea mov = new MovimientoLinea();
                mov.setBaseOperacion(rs.getInt("TPRO_CLAVE")+"");
                mov.setNombreIF(rs.getString("Nombre_if"));
                mov.setNombrePortafolio(rs.getString("Nombre_Portafolio"));
                mov.setNombreProducto(rs.getString("nombreProyecto"));
                mov.setLineaProductoAsignada(rs.getDouble("LIMITE_PORTAFOLIO"));
                mov.setLineaProductoUtilizada(rs.getDouble("CONSUMO_PORTAFOLIO"));                
                mov.setLineaIFUtilizada(rs.getDouble("CONSUMO_TPRO_CLAVE"));
                mov.setConsumoLineaProducto(rs.getDouble("CONSUMO_PORTAFOLIO"));
                mov.setConsumoLineaIF(rs.getDouble("CONSUMO_TPRO_CLAVE")); 
                mov.setFechaVigencia(rs.getString("FECHA_VIGENCIA"));
                retorno.add(mov);
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getMonitorLineas(S)");                
        }
        return retorno;
    }


    @Override
    public ArrayList<ElementoCatalogo> getIFs(String portafolio, Integer idProyecto) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String sqlFiltro = "";
        boolean portafolioVacio  = portafolio.trim().isEmpty();
        if (!portafolioVacio){
            sqlFiltro +=" WHERE m.CVE_PORTAFOLIO = ? ";
        }
        if (idProyecto != null){
            sqlFiltro += portafolioVacio ?  " WHERE  " : " AND ";
            sqlFiltro += " m.CLAVE_PROYECTO = ? ";
        }
        try {
            con.conexionDB();
            strSQL.append(" SELECT  f.ic_if_siag,  f.Cg_Descripcion as Nombre_if " +
                "FROM gti_if_siag f WHERE f.ic_if_siag in ( select distinct (m.INTER_CLAVE) from  SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m "+ sqlFiltro+" )\n" + 
                "ORDER BY f.Cg_Descripcion ");
            
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());
            if (!portafolioVacio){
                String[] arrParamPortafolio = portafolio.trim().split("-");
                Integer idPortafolio = Integer.parseInt(arrParamPortafolio[0]);
                ps.setInt(1, idPortafolio);
            }
            if (idProyecto != null){
                ps.setInt(portafolioVacio ? 1 : 2, idProyecto);    
            }
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo("", "Todos los Intermediarios"));
            while (rs.next()) {
                Integer icIF = rs.getInt("ic_if_siag");
                String nombreIF = rs.getString("Nombre_if");
                retorno.add(new ElementoCatalogo(icIF+"", nombreIF));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;
    }


    @Override
    public ArrayList<ElementoCatalogo> getFechaVigenciaMonitorLineas(Integer idIF, String portafolio, Integer producto) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String filtroIF = idIF == null ? " " : " AND m.INTER_CLAVE = ?  \n";
        if (!portafolio.trim().isEmpty()){            
            filtroIF += " AND m.CVE_PORTAFOLIO = ? ";
        }
        try {
            con.conexionDB();
            strSQL.append(" SELECT fecha_formato FROM  (SELECT DISTINCT TRUNC(p.FECHA_VIGENCIA), TO_CHAR(p.FECHA_VIGENCIA ,'DD/MM/YYYY') AS fecha_formato\n" + 
            "                 FROM SIAG_PROYECTOS@NAFELE_SIAG p, SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m WHERE p.FECHA_VIGENCIA IS NOT NULL AND m.CLAVE_PROYECTO = p.CLAVE_PROYECTO \n" + 
                          filtroIF + " ) "+
            "                 ORDER BY fecha_formato DESC ");
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());
            if (idIF != null){
                ps.setInt(1, idIF);
            }
            if (!portafolio.trim().isEmpty()){
                String[] arrParamPortafolio = portafolio.trim().split("-");
                Integer idPortafolio = Integer.parseInt(arrParamPortafolio[0]);
                ps.setInt(idIF != null ? 2: 1, idPortafolio);
            }
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo("", "Todas las fechas"));
            while (rs.next()) {
                String fechaVigencia = rs.getString("fecha_formato");
                retorno.add(new ElementoCatalogo(fechaVigencia, fechaVigencia));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;               
    }



    @Override
    public ArrayList<ElementoCatalogo> getNombreProductosMonitor(Integer idIF, String portafolio) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String filtroIF = idIF == null ? " " : " AND m.INTER_CLAVE = ?  \n";
        log.info("portafolio: " + portafolio);
        if (!portafolio.trim().isEmpty()){
            filtroIF += " AND m.CVE_PORTAFOLIO = ? ";
        }
        try {
            con.conexionDB();
            strSQL.append(" select DISTINCT (sp.CLAVE_PROYECTO), sp.DESCRIPCION  from SIAG_PROYECTOS@NAFELE_SIAG sp, SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m \n" + 
                "where m.CLAVE_PROYECTO = sp.CLAVE_PROYECTO \n" + filtroIF +
                "ORDER BY sp.DESCRIPCION ASC ");
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());
            if (idIF != null){
                ps.setInt(1, idIF);
            }
            if (!portafolio.trim().isEmpty()){
                String[] arrParamPortafolio = portafolio.trim().split("-");
                log.info(arrParamPortafolio[0]);
                Integer idPortafolio = Integer.parseInt(arrParamPortafolio[0]);
                //Integer idCartera = Integer.parseInt(arrParamPortafolio[1]);
                int indiceConsulta  = idIF == null ? 1 : 2 ;
                ps.setInt(indiceConsulta, idPortafolio);    
            }
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo("", "Todos los Productos"));
            while (rs.next()) {
                Integer clave = rs.getInt("CLAVE_PROYECTO");
                String descripcion = rs.getString("DESCRIPCION");
                retorno.add(new ElementoCatalogo(clave+"", descripcion));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;     
    }


    @Override
    public ArrayList<ElementoCatalogo> getPortafolioMonitor(Integer idIF) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String filtroIF = idIF == null ? " " : " AND m.INTER_CLAVE = ?  \n";
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT ( p.Cg_Descripcion ) , \n" + 
                "  p.ic_portafolio, \n" + 
                "  p.ic_cartera \n" + 
                " FROM SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m, \n" + 
                "  GTICAT_PORTAFOLIOS p \n" + 
                " WHERE p.ic_cartera  = m.CVE_CARTERA \n" + 
                " AND p.ic_portafolio = m.CVE_PORTAFOLIO \n" + 
                filtroIF +
                " ORDER BY p.Cg_Descripcion ");
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());
            if (idIF != null){
                ps.setInt(1, idIF);
            }            
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo("", "Todos los Portafolios"));
            while (rs.next()) {
                String clave = rs.getInt("ic_portafolio")+"-"+rs.getString("ic_cartera");
                String descripcion = rs.getString("Cg_Descripcion");
                retorno.add(new ElementoCatalogo(clave, descripcion));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;  
    }


    @Override
    public ArrayList<ElementoCatalogo> getBaseOperacionesMonitor(String portafolio, Integer idProyecto, Integer idIF, String fechaVigencia) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String filtroSQL = "";
        boolean portafolioVacio = portafolio.trim().isEmpty();
        boolean fechaVigenciaVacia = fechaVigencia.trim().isEmpty();
        
        if (!portafolioVacio){            
            filtroSQL += " AND m.CVE_PORTAFOLIO = ? ";
        }
        if (idProyecto!= null){
            filtroSQL+= " AND sp.CLAVE_PROYECTO = ? " ;
        }
        if (idIF != null){
            filtroSQL+= " AND m.INTER_CLAVE = ? ";
        }
        if (!fechaVigenciaVacia){
            filtroSQL += "  AND trunc(sp.FECHA_VIGENCIA) = TO_DATE( ? ,'DD/MM/YYYY') ";
        }
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT m.TPRO_CLAVE FROM SIAG_VW_MONITOR_LIM_PROYECTO@NAFELE_SIAG m, SIAG_PROYECTOS@NAFELE_SIAG sp "+
                " WHERE sp.CLAVE_PROYECTO  = m.CLAVE_PROYECTO "+ filtroSQL +" order by m.TPRO_CLAVE ");
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString());
            int indiceSQL = 0;
            if (!portafolioVacio){
                indiceSQL++;
                String[] arrParamPortafolio = portafolio.trim().split("-");
                Integer idPortafolio = Integer.parseInt(arrParamPortafolio[0]);
                ps.setInt(indiceSQL, idPortafolio);
            }
            if (idProyecto != null){
                indiceSQL++;
                ps.setInt(indiceSQL, idProyecto);    
            }
            if (idIF != null){
                indiceSQL++;
                ps.setInt(indiceSQL ,idIF);
            }
            if (!fechaVigenciaVacia){
                indiceSQL++;
                ps.setString(indiceSQL, fechaVigencia);
            }
            
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo("", "Todas las bases de operaci�n"));
            while (rs.next()) {
                String descripcion = rs.getString("TPRO_CLAVE");
                retorno.add(new ElementoCatalogo(descripcion, descripcion));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;  
    }

    
        @Override
    public String getMonitorPDF_XLS(HttpServletRequest request, String path, ArrayList<String> param, boolean esIF, String tipoArchivo) {
            String nombreArchivo = "";
            ArrayList<MovimientoLinea> resultado =  this.getMonitorLineas(param);
            String pathFile = path +"00tmp/41docgarant/";
            String paramProtafolio = "Todos";
                if (!param.get(0).trim().isEmpty()){
                    String[] arrParamPortafolio = param.get(0).trim().split("-");
                    Integer idPortafolio = Integer.parseInt(arrParamPortafolio[0]);
                    Integer idCartera = Integer.parseInt(arrParamPortafolio[1]);
                    paramProtafolio = this.getDescripcionPortafolio(idCartera, idPortafolio);
                }
                String paramProducto  = param.get(1).isEmpty() ? "Todos": this.getDescripcionProducto(Integer.parseInt(param.get(1)));
                String paramIF  = param.get(2).isEmpty() ? "Todos": this.getDescripcionIF(Integer.parseInt(param.get(2)));
                String paramVigencia = param.get(3).isEmpty() ? "Todas": param.get(3);
                String paramEstatus = param.get(4).isEmpty() ? "Todos": param.get(4).equals("1") ? "Verde" : param.get(4).equals("2") ? "Amarillo" : "Rojo";
                String paramBasesOperacion = param.get(5).isEmpty() ? "Todas": param.get(5);

                
                if (tipoArchivo.equals(ConstantesMonitor.ARCHIVO_XLS)){
                    try {
                        CreaArchivo archivo     = new CreaArchivo();
                        nombreArchivo      = archivo.nombreArchivo()+".xls";
                        ComunesXLS xlsDoc   = new ComunesXLS(pathFile+nombreArchivo);
                        xlsDoc.agregaTitulo("Monitor de L�neas",9);                
                        // Tabla de parametros enviados
                        xlsDoc.setTabla(2);
                        xlsDoc.setCelda("Portafolio","formasb",ComunesXLS.LEFT);
                        xlsDoc.setCelda(paramProtafolio,"formas",ComunesXLS.LEFT);
                        if(!esIF){
                            xlsDoc.setCelda("Estatus de L�nea","formasb",ComunesXLS.LEFT);
                            xlsDoc.setCelda(paramEstatus,"formas",ComunesXLS.LEFT);                
                        }                
                        xlsDoc.setCelda("Nombre del Producto","formasb",ComunesXLS.LEFT);
                        xlsDoc.setCelda(paramProducto,"formas",ComunesXLS.LEFT);
                        if (!esIF){
                            xlsDoc.setCelda("Bases de Operaci�n","formasb",ComunesXLS.LEFT);
                            xlsDoc.setCelda(paramBasesOperacion,"formas",ComunesXLS.LEFT);                    
                            xlsDoc.setCelda("Intermediario Financiero","formasb",ComunesXLS.LEFT);
                            xlsDoc.setCelda(paramIF,"formas",ComunesXLS.LEFT);           
                        }
                        xlsDoc.setCelda("Fecha de Vigencia","formasb",ComunesXLS.LEFT);
                        xlsDoc.setCelda(paramVigencia,"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda("","formas",ComunesXLS.LEFT);                
                        xlsDoc.cierraTabla();
                        xlsDoc.setTabla(10);
                        xlsDoc.setCelda("Nombre del Portafolio","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("Nombre del Producto","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("Intermediario Financiero","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("L�nea de producto asignada","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("L�nea de producto consumida","formasb",ComunesXLS.CENTER);    
                        xlsDoc.setCelda("L�nea Utilizada IF","formasb",ComunesPDF.CENTER);  
                        xlsDoc.setCelda("% de Consumo L�nea de Producto","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("% de Consumo IF","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("Fecha de Vigencia","formasb",ComunesXLS.CENTER);
                        xlsDoc.setCelda("Bases de Operaci�n","formasb",ComunesXLS.CENTER);                  
                        for (MovimientoLinea mov : resultado){
                            xlsDoc.setCelda(mov.getNombrePortafolio(),"formas",ComunesXLS.LEFT);
                            xlsDoc.setCelda(mov.getNombreProducto(),"formas",ComunesXLS.LEFT);
                            xlsDoc.setCelda(mov.getNombreIF(),"formas",ComunesXLS.LEFT);
                            xlsDoc.setCelda(mov.getLineaProductoAsignada(),"formas",ComunesXLS.RIGHT);
                            xlsDoc.setCelda(mov.getConsumoLineaProducto(),"formas",ComunesXLS.RIGHT);
                            xlsDoc.setCelda(mov.getConsumoLineaIF(),"formas",ComunesXLS.RIGHT);
                            double porcentajeUsadoProducto = (mov.getConsumoLineaProducto()/mov.getLineaProductoAsignada())*100;
                            double porcentajeUsadoIF = (mov.getConsumoLineaIF()/mov.getLineaProductoAsignada())*100;
                            xlsDoc.setCelda(Comunes.formatoDecimal(porcentajeUsadoProducto,2),"formas",ComunesXLS.CENTER);
                            xlsDoc.setCelda(Comunes.formatoDecimal(porcentajeUsadoIF,2),"formas",ComunesXLS.CENTER);
                            xlsDoc.setCelda(mov.getFechaVigencia(),"formas",ComunesXLS.CENTER);
                            xlsDoc.setCelda(mov.getBaseOperacion()+"","formas",ComunesXLS.CENTER);
                        }                
                        xlsDoc.cierraTabla();
                        xlsDoc.cierraXLS();                
                    } catch (Throwable e) 
                    {
                        throw new AppException("Error al generar el archivo ", e);
                    }                 
                }
                else if(tipoArchivo.equals(ConstantesMonitor.ARCHIVO_PDF)){
                    try {
                        ComunesPDF pdfDoc = new ComunesPDF();
                        nombreArchivo = Comunes.cadenaAleatoria(18) + ".pdf";
                        pdfDoc = new ComunesPDF();
                        pdfDoc.creaPDF(2, pathFile+ nombreArchivo, "P�gina ", false, true, false);
                        pdfDoc.setEncabezado("MEXICO","","","","","nafin.gif",path,"Monitor de Lineas de Credito");     
                        // Tabla de parametros enviados
                        pdfDoc.setLTable(4, 90);
                        pdfDoc.setLCell("Portafolio","celda01",ComunesPDF.LEFT);
                        pdfDoc.setLCell(paramProtafolio,"formas",ComunesPDF.LEFT);
                        if(!esIF){
                            pdfDoc.setLCell("Estatus de L�nea","celda01",ComunesPDF.LEFT);
                            pdfDoc.setLCell(paramEstatus,"formas",ComunesPDF.LEFT);                
                        }
                        
                        pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.LEFT);
                        pdfDoc.setLCell(paramProducto,"formas",ComunesPDF.LEFT);
                        if (!esIF){
                            pdfDoc.setLCell("Bases de Operaci�n","celda01",ComunesPDF.LEFT);
                            pdfDoc.setLCell(paramBasesOperacion,"formas",ComunesPDF.LEFT);                    
                            pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.LEFT);
                            pdfDoc.setLCell(paramIF,"formas",ComunesPDF.LEFT);           
                        }
                        pdfDoc.setLCell("Fecha de Vigencia","celda01",ComunesPDF.LEFT);
                        pdfDoc.setLCell(paramVigencia,"formas",ComunesPDF.LEFT);
                        pdfDoc.setLCell("","celda01",ComunesPDF.LEFT);
                        pdfDoc.setLCell("","formas",ComunesPDF.LEFT);                
                        pdfDoc.addLTable();
                        
                        pdfDoc.addText("\n","formas", ComunesPDF.LEFT);        
                        pdfDoc.setLTable(10, 90);
                        pdfDoc.setLCell("Nombre del Portafolio","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("L�nea de producto asignada","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("L�nea de producto consumida","celda01",ComunesPDF.CENTER);                           
                        pdfDoc.setLCell("L�nea Utilizada IF","celda01",ComunesPDF.CENTER);      
                        pdfDoc.setLCell("% de Consumo L�nea de Producto","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("% de Consumo IF","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("Fecha de Vigencia","celda01",ComunesPDF.CENTER);
                        pdfDoc.setLCell("Bases de Operaci�n","celda01",ComunesPDF.CENTER);              
                        for (MovimientoLinea mov : resultado){
                            pdfDoc.setLCell(mov.getNombrePortafolio(),"formas",ComunesPDF.LEFT);
                            pdfDoc.setLCell(mov.getNombreProducto(),"formas",ComunesPDF.LEFT);
                            pdfDoc.setLCell(mov.getNombreIF(),"formas",ComunesPDF.LEFT);
                            pdfDoc.setLCell("$"+Comunes.formatoDecimal(mov.getLineaProductoAsignada(),2),"formas",ComunesPDF.RIGHT);
                            pdfDoc.setLCell("$"+Comunes.formatoDecimal(mov.getConsumoLineaProducto(),2),"formas",ComunesPDF.RIGHT);
                            pdfDoc.setLCell("$"+Comunes.formatoDecimal(mov.getConsumoLineaIF(),2),"formas",ComunesPDF.RIGHT);
                            double porcentajeUsadoProducto = (mov.getConsumoLineaProducto()/mov.getLineaProductoAsignada())*100;
                            double porcentajeUsadoIF = (mov.getConsumoLineaIF()/mov.getLineaProductoAsignada())*100;
                            pdfDoc.setLCell(Comunes.formatoDecimal(porcentajeUsadoProducto,2),"formas",ComunesPDF.CENTER);
                            pdfDoc.setLCell(Comunes.formatoDecimal(porcentajeUsadoIF,2),"formas",ComunesPDF.CENTER);
                            pdfDoc.setLCell(mov.getFechaVigencia(),"formas",ComunesPDF.CENTER);
                            pdfDoc.setLCell(mov.getBaseOperacion()+"","formas",ComunesPDF.CENTER);
                        }                                                                           
                        pdfDoc.addLTable();
                        pdfDoc.endDocument();
                    } catch(Throwable e) {
                            e.printStackTrace();
                            throw new AppException("Error al generar el archivo ", e);
                    }                 
                }
            return  nombreArchivo;
        }

    @Override
    public String getDescripcionProducto(Integer idProducto) {       
        String retorno = null;
        StringBuilder strSQL = new StringBuilder();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            strSQL.append(" SELECT DESCRIPCION from SIAG_PROYECTOS@NAFELE_SIAG WHERE CLAVE_PROYECTO = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, idProducto);
            rs = ps.executeQuery();
            rs.next();
            retorno  = rs.getString("DESCRIPCION");
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;  
    }


    @Override
    public String getDescripcionIF(Integer idIF) {
        String retorno = null;
        StringBuilder strSQL = new StringBuilder();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            strSQL.append(" SELECT Cg_Descripcion FROM gti_if_siag WHERE ic_if_siag =  ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, idIF);
            rs = ps.executeQuery();
            rs.next();
            retorno  = rs.getString("Cg_Descripcion");
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;  
    }

    @Override
    public String getDescripcionPortafolio(Integer idCartera, Integer idPortafolio) {
        String retorno = null;
        StringBuilder strSQL = new StringBuilder();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            strSQL.append(" SELECT DESCRIPCION from SIAG_PORTAFOLIOS@NAFELE_SIAG WHERE CVE_CARTERA = ? AND CVE_PORTAFOLIO = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, idCartera);
            ps.setInt(2, idPortafolio);
            rs = ps.executeQuery();
            rs.next();
            retorno  = rs.getString("DESCRIPCION");
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;  
    }
    
    /** Regresa el numero del Intermediario Financiero del catalogo del SIAG por el IC_IF de NE */
    public Integer getClaveIF_SIAG(Integer ic_if){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Integer retorno = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_IF_SIAG, CG_RAZON_SOCIAL FROM COMCAT_IF WHERE IC_IF = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, ic_if);
            rs = ps.executeQuery();
            String descripcion = null;
            while (rs.next()) {
                descripcion = rs.getString("CG_RAZON_SOCIAL");
                retorno     = rs.getInt("IC_IF_SIAG"); 
            }
            log.info("IF_SIAG: "+ retorno+ " " + descripcion+ " correspondiente al IC_IF: "+ ic_if);
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;   
    }
    
    
    
}
