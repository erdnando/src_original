package com.nafin.docgarantias.parametrizacion.usuarios;

import com.nafin.docgarantias.parametrizacion.usuarios.model.GridConsultaDocumentosParametrizadosVo;
import com.nafin.docgarantias.parametrizacion.usuarios.model.GridConsultaVO;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import javax.naming.NamingException;

import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.usuarios.Usuario;

@Remote
public interface ParametrizacionUsuarios {
    
    public void saveOrUpdateUser(Map parametrosUsuarios)throws SQLException;    
    public Map getParametrosUsuario(String usuario) throws SQLException, NamingException;
    public ArrayList<ElementoCatalogo> getIFs(String tipo, ArrayList<String> seleccionados, String usuario);
    public ArrayList<GridConsultaVO> getGrid(Map parametros)throws SQLException;
    public ArrayList<String> getUsuariosPorInstitucion() throws SQLException; 
    public ArrayList<Usuario> getUsuariosNafinPorPerfil(String perfil);
    public ArrayList<Usuario> getUsuariosIFPorPerfil(String perfil, Integer icIF);
    public Map getParametrosUsuarioString(String usuario) throws SQLException, NamingException;
    public ArrayList<ElementoCatalogo> getUsuariosPorIntermediario(String intermediario);
    public ArrayList<GridConsultaDocumentosParametrizadosVo> getUsuariosParametrizadosPorIntermediario(String intermediario) throws SQLException, NamingException;
    public boolean updateDocumentoIf(String usuario, String documento);
    public ArrayList<ElementoCatalogo> getInstitucionesUsuarioParams() throws SQLException, NamingException;
    public ParametrosIF getParametrosIF(Integer icIF, boolean fullName);
    public ArrayList<Usuario> updateNombresDeUsuarios();
}

