package com.nafin.docgarantias.parametrizacion.usuarios;

import com.nafin.docgarantias.parametrizacion.usuarios.model.GridConsultaDocumentosParametrizadosVo;
import com.nafin.docgarantias.parametrizacion.usuarios.model.GridConsultaVO;
import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "ParametrizacionUsuariosBean", mappedName = "ParametrizacionUsuariosBean")
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrizacionUsuariosBean implements ParametrizacionUsuarios {

    private static final String RELACION_FIRMA_CONJUNTA = "1" ;
    private static final String RELACION_SUPLENTE = "2";
    private static final String RELACION_SUPERVISOR = "3";
    private static final Integer VERSION_ACTUAL = 1;
    private static final Integer VERSION_ANTERIOR = 0;
    @Resource
    SessionContext sessionContext;
    
    private static final Log log = ServiceLocator.getInstance().getLog(ParametrizacionUsuariosBean.class);
    AccesoDB con = new AccesoDB();
    
    public ParametrizacionUsuariosBean() {
        super();
    }
    
    @Override
    public void saveOrUpdateUser(Map parametrosUsuarios) throws SQLException {
        boolean exito = false;
        try {
        con.conexionDB();            
        Map usuario = (Map)parametrosUsuarios.get("usuario");
        Map parametros = (Map)parametrosUsuarios.get("parametros");
        ResultSet rs;

            List documentosList = new ArrayList();
            List portafoliosList = new ArrayList();
            if(!usuario.get("txt_perfil").toString().equals("ADMIN IF GARANT")){     
                String perfil = "OPER GESTGAR";
                if(usuario.get("txt_perfil").toString().equals("AUTO GESTGAR")){
                    perfil = "AUTO GESTGAR";
                }
                log.info("Actualizando GDOC_PARAMETROS_USUARIO");
                String juridico = parametros.get("isUsuarioJuridico")!=null ? parametros.get("isUsuarioJuridico").toString(): "0";
                this.mergeParametrosUsuario(usuario.get("txt_usuario").toString(), Integer.valueOf(juridico), null, perfil);
               
                List intermediariosFinancierosList = new ArrayList();
                    Iterator it = parametros.keySet().iterator();
                    while(it.hasNext()){
                        String item = it.next().toString();
                        if(item.endsWith("_doc")){
                            documentosList.add(parametros.get(item));
                        }
                        if(item.endsWith("_portafolio")){
                            portafoliosList.add(parametros.get(item));
                        }
                        
                        if(item.contains("sl_opergestgar_perfil_supervisor")){
                            if(parametros.get("sl_opergestgar_usuario_supervisor").toString().length()>0){
                                crerRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), parametros.get("sl_opergestgar_usuario_supervisor").toString(), RELACION_SUPERVISOR);
                            }else{
                                eliminarRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), "usuarioAsociado", RELACION_SUPERVISOR);
                            }
                        }
                        
                        if(item.endsWith("_bandera")){
                            if(item.contains("firmaConjunta")){
                                if(parametros.get("firmaConjunta_bandera").equals("1")){
                                    crerRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), parametros.get("sl_autogestgar_usuario_fc_relacionUsuario").toString(), RELACION_FIRMA_CONJUNTA);
                                }else{
                                    eliminarRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), parametros.get("sl_autogestgar_usuario_fc_relacionUsuario").toString(), RELACION_FIRMA_CONJUNTA);
                                }
                            }                    
                            if(item.contains("firma_suplente")){
                                if(parametros.get("rb_autogestgar_firma_suplente_bandera").equals("1")){
                                    crerRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), parametros.get("sl_autogestgar_usuario_suplente").toString(), RELACION_SUPLENTE);
                                }else{
                                    eliminarRelacion(con.getConnection(), usuario.get("txt_usuario").toString(), parametros.get("sl_autogestgar_usuario_suplente").toString(), RELACION_SUPLENTE);
                                }
                                
                            }
                            
                            if(item.contains("itemSelectorIf")){
                                intermediariosFinancierosList = Arrays.asList(parametros.get("itemSelectorIf"));
                            }
                            
                        }                
                    
                        
                    }
                /*DOCUMENTOS*/
                NamedParameterStatement namedParameterStatement = new NamedParameterStatement(con.getConnection(), "SELECT COUNT(IC_TIPO_DOCUMENTO) apariciones FROM GDOC_USUARIO_DOCUMENTO WHERE  RTRIM(IC_USUARIO) =:IC_USUARIO", QUERYTYPE.QUERY);
                namedParameterStatement.setChar("IC_USUARIO", String.valueOf(usuario.get("txt_usuario")));
                rs = namedParameterStatement.executeQuery();
                if(rs.next() && rs.getInt(1)>0){                            
                    deleteItem(con.getConnection(), "todosDocumentos", usuario.get("txt_usuario").toString(), "1", "0");
                    updateItem(con.getConnection(), "todosDocumentos", usuario.get("txt_usuario").toString(), "1", "0"); 
                    for(Object o :documentosList){
                        insertItem(con.getConnection(), "documento", usuario.get("txt_usuario").toString(), o.toString(), VERSION_ACTUAL, null);
                    }
                }else{
                    for(Object o :documentosList){
                        log.info("Datos a Insertar: 'documento'"+ usuario.get("txt_usuario").toString()+" "+o.toString()+ " 1");
                        insertItem(con.getConnection(), "documento", usuario.get("txt_usuario").toString(), o.toString(), VERSION_ACTUAL, null);
                    }
                }
                
                /*PORTAFOLIO*/
                namedParameterStatement = new NamedParameterStatement(con.getConnection(), "SELECT COUNT( IC_PORTAFOLIO ) apariciones FROM GDOC_USUARIO_PORTAFOLIO WHERE RTRIM(IC_USUARIO) =:IC_USUARIO", QUERYTYPE.QUERY);
                namedParameterStatement.setChar("IC_USUARIO", usuario.get("txt_usuario").toString());
                rs = namedParameterStatement.executeQuery();
                if(rs.next() && rs.getInt(1)>0){                            
                    deleteItem(con.getConnection(), "todosPortafolios", usuario.get("txt_usuario").toString(), "1", "0");
                    updateItem(con.getConnection(), "todosPortafolios", usuario.get("txt_usuario").toString(), "1", "0"); 
                    for(Object o :portafoliosList){
                        insertItem(con.getConnection(), "portafolio", usuario.get("txt_usuario").toString(), o.toString(), VERSION_ACTUAL, null);
                    }
                }else{
                    for(Object o :portafoliosList){
                        insertItem(con.getConnection(), "portafolio", usuario.get("txt_usuario").toString(), o.toString(), VERSION_ACTUAL, null);
                    }
                }
                
                /*Intermediarios finanicieros*/
                    if(parametros.containsKey("itemSelectorIf")){
                        String[] intermediarios = parametros.get("itemSelectorIf").toString().split(",");
                        deleteItem(con.getConnection(), "intermediario", usuario.get("txt_usuario").toString(), "1", "0");
                        updateItem(con.getConnection(), "intermediario", usuario.get("txt_usuario").toString(), "1", "0"); 
                        for(String intermediario : intermediarios){
                            if(!intermediario.isEmpty()){
                                insertItem(con.getConnection(), "intermediario", usuario.get("txt_usuario").toString(), intermediario, VERSION_ACTUAL, null);
                            }
                        }
                    }
            }else{ // Usuarios de perfil IF 
                String sqlMerge =  " merge INTO gdoc_parametro_if tgt \n" + 
                    " USING (SELECT Count(ic_if) AS total \n" + 
                    "       FROM   gdoc_parametro_if \n" + 
                    "       WHERE  ic_if = :IC_IF) src \n" + 
                    " ON (src.total >0) \n" + 
                    " WHEN matched THEN \n" + 
                    "  UPDATE SET tgt.ic_usuario_ejecutivo = :IC_USUARIO_EJECUTIVO, \n" + 
                    "             tgt.in_firmas_requerido = :IN_FIRMAS_REQUERIDO, \n" + 
                    "             tgt.in_dias_limite_firma = :IN_DIAS_LIMITE_FIRMA, \n" + 
                    "             ic_archivo = :IC_ARCHIVO WHERE ic_if=:IC_IF \n" + 
                    " WHEN NOT matched THEN \n" + 
                    "  INSERT (ic_if, \n" + 
                    "          ic_usuario_ejecutivo, \n" + 
                    "          in_firmas_requerido, \n" + 
                    "          in_dias_limite_firma, \n" + 
                    "          ic_archivo) \n" + 
                    "  VALUES (:IC_IF, \n" + 
                    "          :IC_USUARIO_EJECUTIVO, \n" + 
                    "          :IN_FIRMAS_REQUERIDO, \n" + 
                    "          :IN_DIAS_LIMITE_FIRMA, \n" + 
                    "          :IC_ARCHIVO) ";
                
                NamedParameterStatement npStatement = new NamedParameterStatement(con.getConnection(), sqlMerge, QUERYTYPE.QUERY);
                npStatement.setChar("IC_IF", usuario.get("txt_usuario").toString());
                String usuarioEjecutivo = usuario.get("txt_ejecutivoAtencion").toString();
                npStatement.setString("IC_USUARIO_EJECUTIVO", usuarioEjecutivo.trim());
                if(parametros.containsKey("txt_numeroFirmas") && !parametros.get("txt_numeroFirmas").equals("")){
                    npStatement.setInt("IN_FIRMAS_REQUERIDO",Integer.valueOf(parametros.get("txt_numeroFirmas").toString()));
                }else{
                    npStatement.setInt("IN_FIRMAS_REQUERIDO",1);
                }
                
                if(parametros.containsKey("txt_diasHabiles") && !parametros.get("txt_diasHabiles").equals("")){                   
                    npStatement.setInt("IN_DIAS_LIMITE_FIRMA", Integer.valueOf(parametros.get("txt_diasHabiles").toString()));
                }else{
                    npStatement.setInt("IN_DIAS_LIMITE_FIRMA",1);
                }
                
                
                if(parametros.containsKey("icDocumento") && !parametros.get("icDocumento").equals("") && !parametros.get("icDocumento").equals("0")){
                    npStatement.setInt("IC_ARCHIVO", Integer.valueOf(parametros.get("icDocumento").toString()));
                }else{
                    npStatement.setNull("IC_ARCHIVO", Types.INTEGER);
                }
                
                npStatement.execute();
               
                Iterator it = parametros.keySet().iterator();
                deleteItem(con.getConnection(), "documentosIntermediario", usuario.get("txt_usuario").toString(), "null", "");
                deleteItem(con.getConnection(), "portafoliosIntermediario", usuario.get("txt_usuario").toString(), "null", "");
                deleteItem(con.getConnection(), "documentosIntermediarioUsuario", usuario.get("txt_usuario").toString(), "", "");
                deleteItem(con.getConnection(), "portafoliosIntermediarioUsuario", usuario.get("txt_usuario").toString(), "", "");
                updateItem(con.getConnection(), "documentosIFusuarios", usuario.get("txt_usuario").toString(), "", "");
                updateItem(con.getConnection(), "portafoliosIFusuarios", usuario.get("txt_usuario").toString(), "", "");
                while(it.hasNext()){
                    String item = it.next().toString();
                    log.info(item);
                    if(item.endsWith("_doc")){
                        if(item.contains("adminif") && !item.contains("adminifUsuario")){
                            insertItem(con.getConnection(), "documentoIF", usuario.get("txt_usuario").toString(), parametros.get(item).toString(), VERSION_ACTUAL, Integer.valueOf(""+usuario.get("txt_usuario")) );
                        }
                    }
                    //XXX
                    if(item.endsWith("_portafolio")){                        
                        if(item.contains("adminif") && !item.contains("adminifUsuario")){
                            insertItem(con.getConnection(), "portafolioIf", usuario.get("txt_usuario").toString(), parametros.get(item).toString(), VERSION_ACTUAL, Integer.valueOf(""+usuario.get("txt_usuario")));
                        }
                        /*else if (item.contains("adminifUsuario")){
                            insertItem(con.getConnection(), "portafolioIfUsuario   ", parametros.get("cb_adminifusuario").toString(), parametros.get(item).toString(), "1");
                        }*/
                    }   
                    if(item.contains("usuarioDocPor")){
                        Map mapUsuarioDocPor = new HashMap();
                        mapUsuarioDocPor = (Map) parametros.get(item.toString());
                        Iterator itusuDoc = mapUsuarioDocPor.keySet().iterator();
                        while(itusuDoc.hasNext()){
                            String usuarioDoc = itusuDoc.next().toString();
                            JSONArray documentos = (JSONArray)mapUsuarioDocPor.get(usuarioDoc);
                            
                            
                            if (usuarioDoc != null){
                                // Guardar en GDOC_PARAMETRO_USUARIO
                                log.info("Guardando usuario: "+ usuarioDoc);
                                this.mergeParametrosUsuario(usuarioDoc , 0, Integer.valueOf(""+usuario.get("txt_usuario")), "ADMIN IF GARANT");
                            }
                            
                            for(int i = 0; i<documentos.size();i++){
                                String documento = (String) documentos.get(i);
                                if(documento.startsWith("10")){
                                    insertItem(con.getConnection(), "documentoIFUsuario", usuarioDoc, documento, VERSION_ACTUAL, Integer.valueOf(""+usuario.get("txt_usuario")));
                                }
                                
                                if(documento.startsWith("20")){
                                    insertItem(con.getConnection(), "portafolioIfUsuario", usuarioDoc, documento, VERSION_ACTUAL, Integer.valueOf(""+usuario.get("txt_usuario")));
                                }
                            }
                        }
                        //log.info(mapUsuarioDocPor);
                    }
                }
                npStatement.close();  
            }
            exito = true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        }finally{
            con.terminaTransaccion(exito);
            con.cierraConexionDB();
        }
    }
    
    
    
    @Override
    public ArrayList<ElementoCatalogo> getUsuariosPorIntermediario(String icIF){
        ArrayList<ElementoCatalogo> usuariosPorIntermediario = new ArrayList<>();
            List<Usuario> listaClavesUsuarios = getUsuariosIFPorPerfil("ADMIN IF GARANT", Integer.valueOf(icIF));
            for (Usuario usuario : listaClavesUsuarios){
                    usuariosPorIntermediario.add(new ElementoCatalogo(usuario.getLogin(), usuario.getNombreCompleto()));
            }  
        return usuariosPorIntermediario;
    }
    
    @Override
    public ArrayList<GridConsultaDocumentosParametrizadosVo> getUsuariosParametrizadosPorIntermediario(String intermediario) throws SQLException, NamingException {
        log.info("getUsuariosParametrizadosPorIntermediario(E)");
        String query = "SELECT DISTINCT ( gud.ic_usuario ), \n" + 
        "                (SELECT Listagg(documentos, '/') \n" + 
        "                          within GROUP( ORDER BY documentos ) doc \n" + 
        "                 FROM   (SELECT Listagg(Trim(doc.cd_nombre_documento), '/') \n" + 
        "                                  within GROUP( ORDER BY doc.ic_tipo_documento ) \n" + 
        "                                documentos \n" + 
        "                         FROM   gdoccat_documento doc, \n" + 
        "                                gdoc_usuario_documento gudd \n" + 
        "                         WHERE  gudd.ic_tipo_documento = doc.ic_tipo_documento \n" + 
        "                                AND gudd.ic_usuario = gud.ic_usuario \n" + 
        "                                AND gudd.is_version_actual = 1)) \n" + 
        "                documentosparam, \n" + 
        "                (SELECT Listagg(documentos, '/') \n" + 
        "                          within GROUP( ORDER BY documentos ) doc \n" + 
        "                 FROM   (SELECT Listagg(Trim(doc.cd_nombre_portafolio), '/') \n" + 
        "                                  within GROUP( ORDER BY doc.ic_portafolio ) \n" + 
        "                                documentos \n" + 
        "                         FROM   gdoccat_portafolio doc, \n" + 
        "                                gdoc_usuario_portafolio gudd \n" + 
        "                         WHERE  gudd.ic_portafolio = doc.ic_portafolio \n" + 
        "                                AND gudd.ic_usuario = gud.ic_usuario \n" + 
        "                                AND gudd.is_version_actual = 1)) \n" + 
        "                portafolioparam, \n" + 
        "                (SELECT Listagg(documentos, '/') \n" + 
        "                          within GROUP( ORDER BY documentos ) doc \n" + 
        "                 FROM   (SELECT Listagg(doc.ic_portafolio, ', ') \n" + 
        "                                  within GROUP( ORDER BY doc.ic_portafolio ) \n" + 
        "                                documentos \n" + 
        "                         FROM   gdoccat_portafolio doc \n" + 
        "                                join gdoc_usuario_portafolio gudd \n" + 
        "                                  ON gudd.ic_portafolio = doc.ic_portafolio \n" + 
        "                                     AND gudd.ic_usuario = gud.ic_usuario \n" + 
        "                                     AND gudd.is_version_actual = 1 \n" + 
        "                         UNION \n" + 
        "                         SELECT Listagg(doc.ic_tipo_documento, '/') \n" + 
        "                                  within GROUP( ORDER BY doc.ic_tipo_documento ) \n" + 
        "                                documentos \n" + 
        "                         FROM   gdoccat_documento doc \n" + 
        "                                join gdoc_usuario_documento gudd \n" + 
        "                                  ON gudd.ic_tipo_documento = \n" + 
        "                                     doc.ic_tipo_documento \n" + 
        "                                     AND gudd.ic_usuario = gud.ic_usuario \n" + 
        "                                     AND gudd.is_version_actual = 1)) docid \n" + 
        "FROM   gdoc_usuario_documento gud \n" + 
        "WHERE  gud.ic_if = :clave_afiliado ";
        ResultSet rs;
        log.info(query);
        log.info("clave_afiliado: "+intermediario);
        ArrayList<GridConsultaDocumentosParametrizadosVo> resultado = new ArrayList<>();
        try {
            con.conexionDB();   
            NamedParameterStatement namedParameterStatement = new NamedParameterStatement(con.getConnection(), query, QUERYTYPE.QUERY);
           namedParameterStatement.setString("clave_afiliado", intermediario);
            rs = namedParameterStatement.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while(rs.next()){
                String claveUsuario = rs.getString("IC_USUARIO");
                Usuario usu = utilUsr.getUsuario(claveUsuario);
                if (usu != null){                    
                    String documentosParam = rs.getString("documentosparam") == null ? "" : rs.getString("documentosparam");
                    String porafoliosParam = rs.getString("portafolioparam") == null ? "": rs.getString("portafolioparam");
                    resultado.add(new GridConsultaDocumentosParametrizadosVo(
                            rs.getString("IC_USUARIO"),
                            usu.getNombreCompleto(),
                            documentosParam.trim().replace("/", ", "),
                            porafoliosParam.trim().replace("/", ", "),
                            rs.getString("DOCID"),
                            usu.getEmail()));
                }
                else{
                    log.error("Usuario no Encontrado: "+ claveUsuario);    
                }
            }           
        } catch (Throwable e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
        }finally{
            con.cierraConexionDB();
        }
        log.info("getUsuariosParametrizadosPorIntermediario(S)");
        return resultado;
    }
    
    @Override
    public Map getParametrosUsuario(String usuario) throws SQLException, NamingException {
        Map configuracionUsuario = new HashMap();
        try {
            con.conexionDB();
            NamedParameterStatement namedParameterStatementJuridico =
                new NamedParameterStatement(con.getConnection(),
                                            "SELECT ES_JURIDICO FROM GDOC_PARAMETROS_USUARIO WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IS_VERSION_ACTUAL = 1",
                                            QUERYTYPE.QUERY);
            NamedParameterStatement namedParameterStatementDocumentos =
                new NamedParameterStatement(con.getConnection(),
                                            "SELECT IC_TIPO_DOCUMENTO FROM GDOC_USUARIO_DOCUMENTO  WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IS_VERSION_ACTUAL = 1",
                                            QUERYTYPE.QUERY);
            namedParameterStatementJuridico.setChar("IC_USUARIO", usuario);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);

            ResultSet rs = namedParameterStatementJuridico.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("ES_JURIDICO", rs.getInt(1));
            }

            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("DOCUMENTO_" + rs.getInt(1), rs.getInt(1));
            }
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT IC_PORTAFOLIO FROM GDOC_USUARIO_PORTAFOLIO WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IS_VERSION_ACTUAL = 1", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("PORTAFOLIO_" + rs.getInt(1), rs.getInt(1));
            }
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT RTRIM(gur.IC_USUARIO_ASOCIADO) IC_USUARIO_ASOCIADO, gtr.IC_TIPO_RELACION FROM GDOC_USUARIO_RELACION gur JOIN GDOCCAT_TIPO_RELACION gtr ON gtr.IC_TIPO_RELACION = gur.IC_TIPO_RELACION and gur.IS_VERSION_ACTUAL = 1 AND RTRIM(gur.IC_USUARIO) =:IC_USUARIO", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                log.info(rs.getRow());
                configuracionUsuario.put("usuario"+rs.getRow()+"_"+rs.getString("IC_USUARIO_ASOCIADO"), "relacion_"+rs.getString("IC_TIPO_RELACION"));
            }
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT LISTAGG(IC_IF, ',') WITHIN GROUP (ORDER BY IC_IF) AS IF FROM GDOC_USUARIO_IF WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IS_VERSION_ACTUAL = 1", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                if(rs.getString(1)!=null && !rs.getString(1).equals("NULL")){
                    configuracionUsuario.put("intermediariosFinancieros", rs.getString(1));
                }
            }
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT IC_IF, RTRIM(IC_USUARIO_EJECUTIVO) IC_USUARIO_EJECUTIVO, IN_FIRMAS_REQUERIDO, IN_DIAS_LIMITE_FIRMA, IC_ARCHIVO, ( SELECT CD_NOMBRE_ARCHIVO FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = gpi.IC_ARCHIVO )CD_NOMBRE_ARCHIVO FROM GDOC_PARAMETRO_IF gpi WHERE RTRIM(IC_IF) = :IC_IF", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_IF", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("ic_if", rs.getString("IC_IF"));
                configuracionUsuario.put("ejecutivoAtencion", rs.getString("IC_USUARIO_EJECUTIVO"));
                configuracionUsuario.put("firmasRequeridas", rs.getString("IN_FIRMAS_REQUERIDO"));
                configuracionUsuario.put("diasFirma", rs.getString("IN_DIAS_LIMITE_FIRMA"));
                configuracionUsuario.put("archivo", rs.getString("IC_ARCHIVO"));
                configuracionUsuario.put("nombreArchivo", rs.getString("CD_NOMBRE_ARCHIVO"));
            }
            
            /***** INTERMEDIARIO FINANCIERO ******/
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT RTRIM(IC_USUARIO_ASOCIADO) IC_USUARIO_ASOCIADO FROM GDOC_USUARIO_RELACION WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL = 1 AND IC_TIPO_RELACION = 3", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                if(!rs.getString(1).equals("NULL")){
                    configuracionUsuario.put("usuarioSuperOperador", rs.getString(1));
                }
            }            
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT IC_TIPO_DOCUMENTO FROM GDOC_TIPO_FIRMADOC_IF  WHERE RTRIM(IC_IF) =:IC_IF", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_IF", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            ArrayList<String> listaDocumentoIF = new ArrayList<>();
            while (rs.next()) {
                listaDocumentoIF.add(rs.getString(1));
            }
            configuracionUsuario.put("doIF_", listaDocumentoIF);
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT IC_PORTAFOLIO FROM GDOC_TIPO_FIRMAPOR_IF  WHERE RTRIM(IC_IF) =:IC_IF", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_IF", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            ArrayList<String> listaPortafolioIF = new ArrayList<>();
            while (rs.next()) {
                listaPortafolioIF.add(rs.getString(1));
            }
            configuracionUsuario.put("porIF_", listaPortafolioIF);
            namedParameterStatementDocumentos.close();
            
        } catch (Exception sqle) {
            sqle.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", sqle);
        } finally {
            con.cierraConexionDB();
        }
        return configuracionUsuario;
    }
    
    @Override
    public Map getParametrosUsuarioString(String usuario) throws SQLException, NamingException {
        Map configuracionUsuario = new HashMap();
        try {
            con.conexionDB();
            NamedParameterStatement namedParameterStatementJuridico =
                new NamedParameterStatement(con.getConnection(),
                                            "SELECT CASE ES_JURIDICO WHEN 0 THEN 'No' WHEN 1 THEN 'Si' END AS juridico FROM GDOC_PARAMETROS_USUARIO WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IS_VERSION_ACTUAL = 0 ",
                                            QUERYTYPE.QUERY);
            NamedParameterStatement namedParameterStatementDocumentos =
                new NamedParameterStatement(con.getConnection(),
                                            "SELECT Listagg(documentos, '/') \n" + 
                                            "         within GROUP( ORDER BY documentos ) \n" + 
                                            "FROM   (SELECT Listagg(doc.cd_nombre_documento, ', ') \n" + 
                                            "                 within GROUP( ORDER BY doc.cd_nombre_documento ) documentos \n" + 
                                            "        FROM   gdoccat_documento doc \n" + 
                                            "               join gdoc_usuario_documento usudoc \n" + 
                                            "                 ON doc.ic_tipo_documento = usudoc.ic_tipo_documento \n" + 
                                            "                    AND Rtrim(usudoc.ic_usuario) = :IC_USUARIO \n" + 
                                            "                    AND usudoc.is_version_actual = 0 \n" + 
                                            "        UNION \n" + 
                                            "        SELECT (SELECT Listagg(doc.cd_nombre_documento, '/') \n" + 
                                            "                         within GROUP( ORDER BY doc.cd_nombre_documento ) \n" + 
                                            "                       documentos \n" + 
                                            "                FROM   gdoccat_documento doc \n" + 
                                            "                       join gdoc_usuario_documento gudd \n" + 
                                            "                         ON gudd.ic_tipo_documento = doc.ic_tipo_documento \n" + 
                                            "                            AND gudd.ic_usuario = gud.ic_usuario) documentos \n" + 
                                            "        FROM   gdoc_usuario_documento gud \n" + 
                                            "                    WHERE gud.is_version_actual = 1 \n" + 
                                            "                    AND gud.ic_usuario = :IC_USUARIO) ",
                                            QUERYTYPE.QUERY);
            namedParameterStatementJuridico.setChar("IC_USUARIO", usuario);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);

            ResultSet rs = namedParameterStatementJuridico.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("juridico", rs.getString("juridico"));
            }
            ArrayList<String> listaDocumentosIF = new ArrayList<>();
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("documentos", rs.getString(1));
            }
            ArrayList<String> listaPortafoliosIF = new ArrayList<>();
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT Listagg(portafolio, '/') \n" + 
                "         within GROUP( ORDER BY portafolio ) \n" + 
                "FROM  (SELECT Listagg(por.cd_nombre_portafolio, ', ') \n" + 
                "                within GROUP( ORDER BY por.cd_nombre_portafolio ) portafolio \n" + 
                "       FROM   gdoccat_portafolio por \n" + 
                "              join gdoc_usuario_portafolio usupor \n" + 
                "                ON por.ic_portafolio = usupor.ic_portafolio \n" + 
                "                   AND Rtrim(usupor.ic_usuario) = :IC_USUARIO \n" + 
                "                   AND usupor.is_version_actual = 0 \n" + 
                "       UNION \n" + 
                "       SELECT (SELECT Listagg(doc.cd_nombre_portafolio, '/') \n" + 
                "                        within GROUP( ORDER BY doc.cd_nombre_portafolio ) \n" + 
                "                      documentos \n" + 
                "               FROM   gdoccat_portafolio doc \n" + 
                "                      join gdoc_usuario_portafolio gudd \n" + 
                "                        ON gudd.ic_portafolio = doc.ic_portafolio \n" + 
                "                           AND gudd.ic_usuario = gud.ic_usuario) portafolio \n" + 
                "       FROM   gdoc_usuario_documento gud \n" + 
                "                   WHERE gud.is_version_actual = 1 \n" + 
                "                   AND gud.ic_usuario = :IC_USUARIO) ", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                configuracionUsuario.put("portafolio", rs.getString(1));
            }
            
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "SELECT  RTRIM(gur.IC_USUARIO_ASOCIADO) IC_USUARIO_ASOCIADO,  gtr.CD_DESCRIPCION  FROM  GDOC_USUARIO_RELACION gur  JOIN GDOCCAT_TIPO_RELACION gtr ON  gtr.IC_TIPO_RELACION = gur.IC_TIPO_RELACION  AND gur.IS_VERSION_ACTUAL = 0  AND RTRIM(gur.IC_USUARIO) =:IC_USUARIO", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            List<ElementoCatalogo> catalogoRelaciones = new ArrayList<>();
            while (rs.next()) {
               catalogoRelaciones.add(new ElementoCatalogo(rs.getString("IC_USUARIO_ASOCIADO"),rs.getString("CD_DESCRIPCION")));
            }
            configuracionUsuario.put("usuariosRelacionados", catalogoRelaciones);
            
            namedParameterStatementDocumentos = new NamedParameterStatement(con.getConnection(), "		SELECT  	LISTAGG(  		catif.CG_NOMBRE_COMERCIAL,  		','  	) WITHIN GROUP (  	ORDER BY  		catif.CG_NOMBRE_COMERCIAL  	) AS IF  FROM  	GDOC_USUARIO_IF guif  	JOIN COMCAT_IF catif ON catif.IC_IF = guif.IC_IF  WHERE  	RTRIM(IC_USUARIO) =:IC_USUARIO  	AND IS_VERSION_ACTUAL = 0  	AND catif.IC_IF_SIAG IS NOT NULL", QUERYTYPE.QUERY);
            namedParameterStatementDocumentos.setChar("IC_USUARIO", usuario);
            rs = namedParameterStatementDocumentos.executeQuery();
            while (rs.next()) {
                if(rs.getString(1)!=null && !rs.getString(1).equals("NULL")){
                    configuracionUsuario.put("intermediarios", rs.getString(1));
                }
            }
            
            namedParameterStatementDocumentos.close();
            
        } catch (Exception sqle) {
            sqle.printStackTrace();
        } finally {
            con.cierraConexionDB();

        }
        return configuracionUsuario;
    }
    
    @Override
    public ArrayList<ElementoCatalogo> getIFs(String tipo, ArrayList<String> seleccionados, String usuario) {
      
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        NamedParameterStatement namedParameterStatement;
        try {
            con.conexionDB();
            String query ="";
            if(seleccionados.size()>0){
                String seleccionadosStr = seleccionados.toString().replace("[", "").replace("]", "");
                query = "SELECT DISTINCT IC_IF , CG_NOMBRE_COMERCIAL FROM (SELECT IC_IF, Cg_Nombre_Comercial  FROM COMCAT_IF WHERE IC_IF_SIAG IS NOT NULL AND CS_TIPO =:CS_TIPO AND IC_IF NOT IN (SELECT IC_IF FROM GDOC_USUARIO_IF WHERE RTRIM(IC_USUARIO) <>:IC_USUARIO AND IS_VERSION_ACTUAL = 1) UNION SELECT IC_IF, Cg_Nombre_Comercial  FROM COMCAT_IF WHERE IC_IF_SIAG IS NOT NULL AND TO_CHAR(IC_IF) IN (" +
                    seleccionadosStr+
                    ")) ORDER BY CG_NOMBRE_COMERCIAL";              
            }else{
                query="SELECT IC_IF, Cg_Nombre_Comercial, CS_TIPO  FROM COMCAT_IF WHERE IC_IF_SIAG IS NOT NULL AND CS_TIPO =:CS_TIPO AND IC_IF NOT IN (SELECT IC_IF FROM GDOC_USUARIO_IF WHERE RTRIM(IC_USUARIO) <>:IC_USUARIO AND IS_VERSION_ACTUAL = 1) ORDER BY CG_NOMBRE_COMERCIAL";
            }
           
            if(tipo.equals("todos")){
                query = "SELECT IC_IF, Cg_Nombre_Comercial, CS_TIPO  FROM COMCAT_IF WHERE IC_IF_SIAG IS NOT NULL  ORDER BY CG_NOMBRE_COMERCIAL";
            }
           
            namedParameterStatement = new NamedParameterStatement(con.getConnection(), query, QUERYTYPE.QUERY);
           
            if(!tipo.equals("todos")){
                namedParameterStatement.setString("CS_TIPO", tipo);
                namedParameterStatement.setChar("IC_USUARIO", usuario);
            }
          
          
          
                    
            ResultSet rs = namedParameterStatement.executeQuery();
            while (rs.next()) {
                Integer icIF = rs.getInt("IC_IF");
                String nombreIF = rs.getString("Cg_Nombre_Comercial");
                retorno.add(new ElementoCatalogo(icIF+"", nombreIF));
            }
            
            namedParameterStatement.close();

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return retorno;
    }
    
    @Override
    public boolean updateDocumentoIf(String usuario, String documento) {
            NamedParameterStatement namedParameterStatement;
            boolean exito = false;
            try {
                con.conexionDB();
                namedParameterStatement = new NamedParameterStatement(con.getConnection(), "UPDATE GDOC_PARAMETRO_IF SET IC_ARCHIVO=:IC_ARCHIVO WHERE IC_IF=:IC_IF", QUERYTYPE.QUERY);
                namedParameterStatement.setString("IC_IF", usuario);
                namedParameterStatement.setString("IC_ARCHIVO", documento);
                namedParameterStatement.executeUpdate();
                namedParameterStatement.close();
                exito = true;
            } catch (SQLException | NamingException e) {
                e.printStackTrace();
                log.error(e.getMessage());
                throw new AppException("Error inesperado al cargar la información", e);
            } finally {
                con.terminaTransaccion(exito);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return true;
        }

    @Override
    public ArrayList<GridConsultaVO> getGrid(Map parametros) throws SQLException {

        String query = "SELECT Rtrim(usuario.ic_usuario) IC_USUARIO,\n" + 
            "  'USUARIO' tipo,\n" + 
            "  To_date(usuario.dt_fecha_registro) DT_FECHA_REGISTRO,\n" + 
            " usuario.ES_JURIDICO, "+
            "  (SELECT Listagg(doc.cd_nombre_documento, ', ') within GROUP(\n" + 
            "  ORDER BY doc.cd_nombre_documento ) documentos\n" + 
            "  FROM gdoccat_documento doc\n" + 
            "  JOIN gdoc_usuario_documento usudoc\n" + 
            "  ON doc.ic_tipo_documento     = usudoc.ic_tipo_documento\n" + 
            "  AND Rtrim(usudoc.ic_usuario) = Rtrim(usuario.ic_usuario)\n" + 
            "  AND usudoc.is_version_actual = 1\n" + 
            "  ) documentos,\n" + 
            "  (SELECT Listagg(por.cd_nombre_portafolio, ', ') within GROUP(\n" + 
            "  ORDER BY por.cd_nombre_portafolio ) portafolio\n" + 
            "  FROM gdoccat_portafolio por\n" + 
            "  JOIN gdoc_usuario_portafolio usupor\n" + 
            "  ON por.ic_portafolio         = usupor.ic_portafolio\n" + 
            "  AND Rtrim(usupor.ic_usuario) = Rtrim(usuario.ic_usuario)\n" + 
            "  AND usupor.is_version_actual = 1\n" + 
            "  ) portafolio ,\n" + 
            " (select Is_Version_Actual from gdoc_parametros_usuario u where u.ic_usuario= usuario.ic_usuario and U.Is_Version_Actual = 0) historico "+
            "FROM gdoc_parametros_usuario usuario\n" + 
            "WHERE usuario.is_version_actual = 1 ";
        //log.info(query);
        ResultSet rs;
        ArrayList<GridConsultaVO> resultado = new ArrayList<>();
        try {
            con.conexionDB();
        
           NamedParameterStatement namedParameterStatement = new NamedParameterStatement(con.getConnection(), query, QUERYTYPE.QUERY);
            
            rs = namedParameterStatement.executeQuery();
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
            UtilUsr utilUsr = new UtilUsr();
            while(rs.next()){
                String claveUsu = rs.getString("IC_USUARIO");
                Usuario usuario = utilUsr.getUsuario(claveUsu);
                if (usuario.getLogin() != null){
                    resultado.add(new GridConsultaVO(
                    rs.getDate("DT_FECHA_REGISTRO")!=null?sdf.format(rs.getDate("DT_FECHA_REGISTRO")):"",
                    rs.getString("IC_USUARIO"),
                    usuario.getNombreCompleto(),
                    usuario.getPerfil(), 
                    usuario.getNombreOrganizacionUsuario(),
                    usuario.getEmail(),
                    rs.getString("DOCUMENTOS")==null?"":rs.getString("DOCUMENTOS"),
                    rs.getString("PORTAFOLIO")==null?"":rs.getString("PORTAFOLIO"),
                    rs.getString("historico"),
                    rs.getString("TIPO"),
                    rs.getBoolean("ES_JURIDICO")));
                }
                else{
                    log.error("Usuario no encontrado: "+ claveUsu);
                }
            }
            //log.info("Numero de Registros: "+resultado.size());
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
                con.cierraConexionDB();
        }
        return resultado;
    }

    /**
     * Regresa una lista de los Intermediarios Financieros de los usuarios que estan parametrizados
     * 
     * */
    @Override
    public ArrayList<String> getUsuariosPorInstitucion() throws SQLException{
        ArrayList<String> retorno = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sqlQuery = "SELECT  DISTINCT cg_nombre_comercial FROM GDOC_PARAMETROS_USUARIO pu, COMCAT_IF cif " +
                " WHERE pu.ic_if = cif.ic_if and pu.ic_if is not null  ORDER BY cg_nombre_comercial";
            
            ps =con.queryPrecompilado(sqlQuery); 
            rs = ps.executeQuery();
            while (rs.next()) {
                retorno.add(rs.getString("cg_nombre_comercial"));
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }  
        }
        return retorno;
    }

    private void crerRelacion(Connection connection, String usuario, String usuarioAsociado, String tipoRelacion) throws SQLException {
        //Eliminar relaciones en 0
        NamedParameterStatement namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_RELACION WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IC_TIPO_RELACION=:IC_TIPO_RELACION AND IS_VERSION_ACTUAL=0", QUERYTYPE.QUERY);
        namedParameterStatement.setChar("IC_USUARIO", usuario);
        namedParameterStatement.setInt("IC_TIPO_RELACION", Integer.valueOf(tipoRelacion));
        namedParameterStatement.executeUpdate();
        
        //Actualiza 1->0
        connection.commit();
        namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_RELACION SET IS_VERSION_ACTUAL = 0 WHERE RTRIM(IC_USUARIO) =:IC_USUARIO AND IC_TIPO_RELACION=:IC_TIPO_RELACION", QUERYTYPE.QUERY);
        namedParameterStatement.setChar("IC_USUARIO", usuario.toString());
        namedParameterStatement.setInt("IC_TIPO_RELACION", Integer.valueOf(tipoRelacion));
        namedParameterStatement.execute();
        
        //inserta nuevo
        connection.commit();
        namedParameterStatement = new NamedParameterStatement(connection, "INSERT INTO GDOC_USUARIO_RELACION (IC_USUARIO, IC_TIPO_RELACION, IC_USUARIO_ASOCIADO, IS_VERSION_ACTUAL) VALUES(:IC_USUARIO, :IC_TIPO_RELACION, :IC_USUARIO_ASOCIADO, 1)", QUERYTYPE.QUERY);
        namedParameterStatement.setChar("IC_USUARIO", usuario);
        namedParameterStatement.setInt("IC_TIPO_RELACION", Integer.valueOf(tipoRelacion));
        namedParameterStatement.setString("IC_USUARIO_ASOCIADO", usuarioAsociado);
        namedParameterStatement.execute();
        
        namedParameterStatement.close();
    }
    
    private void eliminarRelacion(Connection connection, String usuario, String usuarioAsociado, String tipoRelacion) throws SQLException {
        //Eliminar relaciones en 0
        NamedParameterStatement namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_RELACION WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IC_TIPO_RELACION=:IC_TIPO_RELACION AND IS_VERSION_ACTUAL=0", QUERYTYPE.QUERY);
        namedParameterStatement.setChar("IC_USUARIO", usuario);
        namedParameterStatement.setInt("IC_TIPO_RELACION", Integer.valueOf(tipoRelacion));
        namedParameterStatement.execute();
        
        //Actualiza 1->0
        namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_RELACION SET IS_VERSION_ACTUAL = 0 WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IC_TIPO_RELACION=:IC_TIPO_RELACION", QUERYTYPE.QUERY);
        namedParameterStatement.setChar("IC_USUARIO", usuario);
        namedParameterStatement.setInt("IC_TIPO_RELACION", Integer.valueOf(tipoRelacion));
        namedParameterStatement.execute();
        
        namedParameterStatement.close();
    }

    private void deleteItem(Connection connection, String tipo, String usuario, String claveItem, String... extra) throws SQLException {
        NamedParameterStatement namedParameterStatement = null;
        int registrosModificados = 0;
        switch (tipo) {
        case "documento":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_DOCUMENTO WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IC_TIPO_DOCUMENTO=:IC_TIPO_DOCUMENTO AND IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosModificados = namedParameterStatement.executeUpdate();
            break;
        case "todosDocumentos":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_DOCUMENTO WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosModificados= namedParameterStatement.executeUpdate();
            break;
        case "todosPortafolios":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_PORTAFOLIO WHERE RTRIM(IC_USUARIO)=:IC_USUARIO  AND IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosModificados= namedParameterStatement.executeUpdate();
            break;
        case "intermediario":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_IF WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosModificados = namedParameterStatement.executeUpdate();
            break;
        case "documentosIntermediario":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_TIPO_FIRMADOC_IF WHERE IC_IF=:IC_IF", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            registrosModificados = namedParameterStatement.executeUpdate();
            break;
        case "portafoliosIntermediario":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_TIPO_FIRMAPOR_IF WHERE IC_IF=:IC_IF", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            registrosModificados = namedParameterStatement.executeUpdate();
            break;
        case "documentosIntermediarioUsuario":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_DOCUMENTO WHERE IC_IF =:ic_if  AND IS_VERSION_ACTUAL=0 ", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("ic_if", usuario);
            registrosModificados = namedParameterStatement.executeUpdate();
            break;
        case "portafoliosIntermediarioUsuario":
            namedParameterStatement = new NamedParameterStatement(connection, "DELETE FROM GDOC_USUARIO_PORTAFOLIO WHERE IC_IF =:ic_if AND IS_VERSION_ACTUAL=0 ", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("ic_if", usuario);            
            namedParameterStatement.executeUpdate();
            break;
        }
        if(namedParameterStatement != null){
            namedParameterStatement.close();
        }
        log.info("Registros " +tipo + " eliminados : "+registrosModificados);
    }
    
    private void updateItem(Connection connection, String tipo, String usuario, String claveItem, String... extra) throws SQLException {
        NamedParameterStatement namedParameterStatement = null;
        int registrosAlterados = 0;
        switch (tipo) {
        case "documento": namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_DOCUMENTO SET IS_VERSION_ACTUAL =:IS_VERSION_ACTUAL WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IC_TIPO_DOCUMENTO=:IC_TIPO_DOCUMENTO AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IC_TIPO_DOCUMENTO", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;
        case "portafolio": namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_PORTAFOLIO SET IS_VERSION_ACTUAL =:IS_VERSION_ACTUAL WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IC_PORTAFOLIO=:IC_PORTAFOLIO AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IC_PORTAFOLIO", Integer.valueOf(claveItem));
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;        
        case "todosDocumentos":
            namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_DOCUMENTO SET IS_VERSION_ACTUAL =:IS_VERSION_ACTUAL WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;
        case "todosPortafolios":
            namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_PORTAFOLIO SET IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;
        case "intermediario":
            namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_IF SET IS_VERSION_ACTUAL=:IS_VERSION_ACTUAL WHERE RTRIM(IC_USUARIO)=:IC_USUARIO AND IS_VERSION_ACTUAL = 1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(extra[0]));
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;     
        case "documentosIFusuarios": namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_DOCUMENTO SET IS_VERSION_ACTUAL = 0 WHERE IC_IF =:IC_IF  AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;
        case "portafoliosIFusuarios": namedParameterStatement = new NamedParameterStatement(connection, "UPDATE GDOC_USUARIO_PORTAFOLIO SET IS_VERSION_ACTUAL =0 WHERE IC_IF = :IC_IF AND IS_VERSION_ACTUAL=1", QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            registrosAlterados = namedParameterStatement.executeUpdate();
            break;          
        }
        if(namedParameterStatement != null){
            namedParameterStatement.close();
        }
        log.info("Registros "+ tipo+" Actualizados"+ registrosAlterados);

    }
    
    private void insertItem(Connection connection, String tipo, String usuario, String claveItem, Integer versionActual, Integer ic_if) throws SQLException {
        NamedParameterStatement namedParameterStatement = null;
        switch (tipo) {
        case "documento":
            namedParameterStatement =
                new NamedParameterStatement(connection,
                                            "INSERT INTO GDOC_USUARIO_DOCUMENTO (IC_USUARIO, IC_TIPO_DOCUMENTO, IS_VERSION_ACTUAL) VALUES(:IC_USUARIO, :IC_TIPO_DOCUMENTO, :IS_VERSION_ACTUAL)",
                                            QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IC_TIPO_DOCUMENTO", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", versionActual);
            namedParameterStatement.executeUpdate();
            break;
        case "portafolio":
            namedParameterStatement =
                new NamedParameterStatement(connection,
                                            "INSERT INTO GDOC_USUARIO_PORTAFOLIO (IC_USUARIO, IC_PORTAFOLIO, IS_VERSION_ACTUAL) VALUES(:IC_USUARIO, :IC_PORTAFOLIO, :IS_VERSION_ACTUAL)",
                                            QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IC_PORTAFOLIO", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", versionActual);
            namedParameterStatement.executeUpdate();
            break;        
        case "intermediario":
            namedParameterStatement =
                new NamedParameterStatement(connection,
                                            "INSERT INTO GDOC_USUARIO_IF (IC_USUARIO, IC_IF, IS_VERSION_ACTUAL) VALUES(:IC_USUARIO, :IC_IF, :IS_VERSION_ACTUAL)",
                                            QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_USUARIO", usuario);
            namedParameterStatement.setInt("IC_IF", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_VERSION_ACTUAL", versionActual);
            namedParameterStatement.executeUpdate();
            break;        
        case "documentoIF":
            namedParameterStatement =
                new NamedParameterStatement(connection,
                                            "INSERT INTO GDOC_TIPO_FIRMADOC_IF (IC_IF, IC_TIPO_DOCUMENTO, IS_FIRMA_MANCOMUNADA) VALUES(:IC_IF, :IC_TIPO_DOCUMENTO, :IS_FIRMA_MANCOMUNADA)",
                                            QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            namedParameterStatement.setInt("IC_TIPO_DOCUMENTO", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_FIRMA_MANCOMUNADA", versionActual);
            namedParameterStatement.executeUpdate();
            break;
        case "portafolioIf":
            namedParameterStatement =
                new NamedParameterStatement(connection,
                                            "INSERT INTO GDOC_TIPO_FIRMAPOR_IF (IC_IF, IC_PORTAFOLIO, IS_FIRMA_MANCOMUNADA) VALUES(:IC_IF, :IC_PORTAFOLIO, :IS_FIRMA_MANCOMUNADA)",
                                            QUERYTYPE.QUERY);
            namedParameterStatement.setChar("IC_IF", usuario);
            namedParameterStatement.setInt("IC_PORTAFOLIO", Integer.valueOf(claveItem));
            namedParameterStatement.setInt("IS_FIRMA_MANCOMUNADA", versionActual);
            namedParameterStatement.executeUpdate();
            break;
            case "documentoIFUsuario":
                namedParameterStatement =
                    new NamedParameterStatement(connection,
                                                "INSERT INTO GDOC_USUARIO_DOCUMENTO (IC_USUARIO, IC_TIPO_DOCUMENTO, IS_VERSION_ACTUAL, IC_IF) VALUES(:IC_USUARIO, :IC_TIPO_DOCUMENTO, :IS_VERSION_ACTUAL, :IC_IF)",
                                                QUERYTYPE.QUERY);
                namedParameterStatement.setChar("IC_USUARIO", usuario);
                namedParameterStatement.setInt("IC_TIPO_DOCUMENTO", Integer.valueOf(claveItem));
                namedParameterStatement.setInt("IS_VERSION_ACTUAL", Integer.valueOf(1));
                namedParameterStatement.setInt("IC_IF", ic_if);
                namedParameterStatement.executeUpdate();
                
                break;
            case "portafolioIfUsuario":
                namedParameterStatement =
                    new NamedParameterStatement(connection,
                                                "INSERT INTO GDOC_USUARIO_PORTAFOLIO (IC_USUARIO, IC_PORTAFOLIO, IS_VERSION_ACTUAL, IC_IF) VALUES(:IC_USUARIO, :IC_PORTAFOLIO, :IS_VERSION_ACTUAL, :IC_IF)",
                                                QUERYTYPE.QUERY);
                namedParameterStatement.setChar("IC_USUARIO", usuario);
                namedParameterStatement.setInt("IC_PORTAFOLIO", Integer.valueOf(claveItem));
                namedParameterStatement.setInt("IS_VERSION_ACTUAL", versionActual);
                namedParameterStatement.setInt("IC_IF", ic_if);
                namedParameterStatement.executeUpdate();
                break;        
        }
        if(namedParameterStatement != null){
            namedParameterStatement.close();
        }
        //insertItem

    }
    
    
    private boolean mergeParametrosUsuario(String icUsuario, Integer esJuridico, Integer ic_if, String icPerfil){
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String strSQL = " SELECT COUNT(1) as TOTAL FROM GDOC_PARAMETROS_USUARIO WHERE IC_USUARIO = ? ";
            ps = con.queryPrecompilado(strSQL); 
            ps.setString(1, String.format("%1$-9s", icUsuario.trim()));
            rs = ps.executeQuery();
            int totalRegistros = 0;
            while (rs.next()){
                totalRegistros = rs.getInt("TOTAL");
            }
            if (totalRegistros >= 2 ){
                String sqlDelete = " DELETE FROM GDOC_PARAMETROS_USUARIO WHERE IC_USUARIO = ? AND IS_VERSION_ACTUAL = 0 " ;
                ps = con.queryPrecompilado(sqlDelete); 
                ps.setString(1, String.format("%1$-9s", icUsuario.trim()));             
                int n = ps.executeUpdate();
                log.info("registros eliminados: "+ n);
            }
            String sqlUpdate = " UPDATE GDOC_PARAMETROS_USUARIO SET IS_VERSION_ACTUAL=0 WHERE IC_USUARIO= ? AND IS_VERSION_ACTUAL = 1 " ;
            ps = con.queryPrecompilado(sqlUpdate); 
            ps.setString(1, String.format("%1$-9s", icUsuario.trim()));            
            int k = ps.executeUpdate();
            log.info("registros actualizados: "+ k);
            String sqlInsert = " INSERT INTO GDOC_PARAMETROS_USUARIO(IC_USUARIO, IS_VERSION_ACTUAL, ES_JURIDICO, DT_FECHA_REGISTRO, IC_IF, IC_PERFIL)" +
                    " VALUES (?, 1, ?, SYSDATE, ?, ?) ";
            ps = con.queryPrecompilado(sqlInsert); 
            ps.setString(1, String.format("%1$-9s", icUsuario.trim()));
            ps.setInt(2, esJuridico);
            if (ic_if != null){
                ps.setInt(3, ic_if);
            }
            else{
                ps.setNull(3, java.sql.Types.INTEGER);
            }
            ps.setString(4, icPerfil);
            
            int j = ps.executeUpdate();
            log.info("registros insertados: "+ j);
            exito = true;
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return exito;    
    }

    @Override
    public ArrayList<ElementoCatalogo> getInstitucionesUsuarioParams() throws SQLException, NamingException {
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nafin = "NACIONAL FINANCIERA S.N.C.";
        try {
            con.conexionDB();
            String strSQL = "SELECT IC_IF, CG_RAZON_SOCIAL  FROM COMCAT_IF WHERE IC_IF in (SELECT ic_if FROM GDOC_PARAMETROS_USUARIO group by IC_IF)  ";
            ps = con.queryPrecompilado(strSQL); 
            rs = ps.executeQuery();
            retorno.add(new ElementoCatalogo(null,nafin));
            while (rs.next()){
                log.info(rs.getString("IC_IF")+" - "+rs.getString("CG_RAZON_SOCIAL"));
                retorno.add(new ElementoCatalogo(rs.getString("IC_IF"), rs.getString("CG_RAZON_SOCIAL")));
            } 
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return retorno;
    }

    @Override
    public ParametrosIF getParametrosIF(Integer icIF, boolean fullName) {
        ParametrosIF retorno = new ParametrosIF();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String strSQL = " SELECT ic_if, \n" + 
                "       ic_usuario_ejecutivo, \n" + 
                "       in_firmas_requerido, \n" + 
                "       in_dias_limite_firma, \n" + 
                "       ic_archivo \n" + 
                "FROM   gdoc_parametro_if \n" + 
                "WHERE  ic_if = ?  ";
            ps = con.queryPrecompilado(strSQL); 
            ps.setInt(1, icIF);
            rs = ps.executeQuery();
            while (rs.next()){
                retorno.setIcDocumentoAutografo(rs.getInt("ic_archivo"));
                retorno.setIcIf(icIF);
                retorno.setIcUsuarioEjecutivo(rs.getInt("ic_usuario_ejecutivo"));
                retorno.setNumeroDiasLimiteFirma(rs.getInt("in_dias_limite_firma"));
                retorno.setNumeroFirmasRequerido(rs.getInt("in_firmas_requerido"));
                if (fullName){
                    UtilUsr utilUsr = new UtilUsr();
                    Usuario usuario = utilUsr.getUsuario(rs.getString("ic_usuario_ejecutivo"));
                    if(usuario.getLogin() != null){
                        retorno.setNombreUsuarioEjecutivo(usuario.getNombreCompleto());
                    }
                    else{
                        log.error("Usuario no encontrado: "+rs.getString("ic_usuario_ejecutivo"));
                    }
                }
            } 
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getParametrosIF", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return retorno;
    }
    
    public ArrayList<Usuario> getUsuariosNafinPorPerfil(String perfil){
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        String sql = " select REPLACE(membercn, 'cn=','') ic_usuario from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N' \n" + 
            "and  UPPER(rdn) like '%"+perfil+"' ";
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        UtilUsr utilUsr = new UtilUsr();
        try{
            con.conexionDB();
            ps = con.queryPrecompilado(sql);
            log.info(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                listaUsuarios.add(utilUsr.getUsuario(rs.getString("ic_usuario")));
            }            
        }
        catch(SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosNafinPorPerfil", e);
        }    
        catch(Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosNafinPorPerfil", e);
        }  finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }          
        return listaUsuarios;
    }
    
    public ArrayList<Usuario> getUsuariosIFPorPerfil(String perfil, Integer icIF){
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        String sql =  " select REPLACE(membercn, 'cn=','') ic_usuario \n" + 
                    " from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='I' and clave_afiliado = ? \n" + 
                    " and  UPPER(rdn) like '%"+perfil+"' ";
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        UtilUsr utilUsr = new UtilUsr();
        try{
            con.conexionDB();
            ps = con.queryPrecompilado(sql);
            log.info(sql);
            ps.setInt(1, icIF);
            rs = ps.executeQuery();
            while(rs.next()){
                listaUsuarios.add(utilUsr.getUsuario(rs.getString("ic_usuario")));
            }
        }
        catch(SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosIFPorPerfil", e);
        }        
        catch(Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosIFPorPerfil", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }           
        return listaUsuarios;
    }
    
   
    public ArrayList<Usuario> updateNombresDeUsuarios(){
        return null;
    }

    
    
}
