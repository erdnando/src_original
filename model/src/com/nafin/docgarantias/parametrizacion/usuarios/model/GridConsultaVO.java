package com.nafin.docgarantias.parametrizacion.usuarios.model;

import java.io.Serializable;

public class GridConsultaVO implements Serializable {
    @SuppressWarnings("compatibility:1012696402224591275")
    private static final long serialVersionUID = 1L;

    private String fechaRegistro;
    private String usuario;
    private String nombreCompleto;
    private String perfil;
    private String institucion;
    private String correoElectronico;
    private String documentos;
    private String portafolio;
    private String historico;
    private String tipo;
    private boolean esJuridico;


    public GridConsultaVO(String fechaRegistro, String usuario, String nombreCompleto, String perfil,
                          String institucion, String correoElectronico, String documentos, String portafolio,
                          String historico, String tipo, boolean esJuridico) {
        super();
        this.fechaRegistro = fechaRegistro;
        this.usuario = usuario;
        this.nombreCompleto = nombreCompleto;
        this.perfil = perfil;
        this.institucion = institucion;
        this.correoElectronico = correoElectronico;
        this.documentos = documentos;
        this.portafolio = portafolio;
        this.historico = historico;
        this.tipo = tipo;
        this.esJuridico = esJuridico;
    }


    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setDocumentos(String documentos) {
        this.documentos = documentos;
    }

    public String getDocumentos() {
        return documentos;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getHistorico() {
        return historico;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }


    public boolean isEsJuridico() {
        return esJuridico;
    }

    public void setEsJuridico(boolean esJuridico) {
        this.esJuridico = esJuridico;
    }
}
