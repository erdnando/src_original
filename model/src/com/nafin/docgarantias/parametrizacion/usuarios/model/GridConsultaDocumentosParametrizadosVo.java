package com.nafin.docgarantias.parametrizacion.usuarios.model;

import java.io.Serializable;

public class GridConsultaDocumentosParametrizadosVo  implements Serializable{
    @SuppressWarnings("compatibility:5597838180254658615")
    private static final long serialVersionUID = 1L;
    private String icUsuario;
    private String nombreCompleto;
    private String documento;
    private String portafolio;
    private String docId;
    private String email;
    
    public GridConsultaDocumentosParametrizadosVo(String icUsuario, String nombreCompleto, String documento, String portafolio, String docId, String email) {
        super();
        this.icUsuario = icUsuario;
        this.nombreCompleto = nombreCompleto;
        this.documento = documento;
        this.portafolio = portafolio;
        this.docId = docId;
        this.email = email;
    }

    public void setIcUsuario(String icUsuario) {
        this.icUsuario = icUsuario;
    }

    public String getIcUsuario() {
        return icUsuario;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }


    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getDocId() {
        return docId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }
}
