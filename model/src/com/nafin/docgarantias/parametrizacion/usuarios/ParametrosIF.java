package com.nafin.docgarantias.parametrizacion.usuarios;

import java.io.Serializable;

public class ParametrosIF implements Serializable{
    @SuppressWarnings("compatibility:381754453505402607")
    private static final long serialVersionUID = 1L;

    private Integer icIf;     
    private Integer icUsuarioEjecutivo ;
    private String nombreUsuarioEjecutivo;
    private Integer numeroFirmasRequerido  ;
    private Integer numeroDiasLimiteFirma ;
    private Integer icDocumentoAutografo ;

    public ParametrosIF() {
        super();
    }

    public Integer getIcIf() {
        return icIf;
    }

    public void setIcIf(Integer icIf) {
        this.icIf = icIf;
    }

    public Integer getIcUsuarioEjecutivo() {
        return icUsuarioEjecutivo;
    }

    public void setIcUsuarioEjecutivo(Integer icUsuarioEjecutivo) {
        this.icUsuarioEjecutivo = icUsuarioEjecutivo;
    }

    public String getNombreUsuarioEjecutivo() {
        return nombreUsuarioEjecutivo;
    }

    public void setNombreUsuarioEjecutivo(String nombreUsuarioEjecutivo) {
        this.nombreUsuarioEjecutivo = nombreUsuarioEjecutivo;
    }

    public Integer getNumeroFirmasRequerido() {
        return numeroFirmasRequerido;
    }

    public void setNumeroFirmasRequerido(Integer numeroFirmasRequerido) {
        this.numeroFirmasRequerido = numeroFirmasRequerido;
    }

    public Integer getNumeroDiasLimiteFirma() {
        return numeroDiasLimiteFirma;
    }

    public void setNumeroDiasLimiteFirma(Integer numeroDiasLimiteFirma) {
        this.numeroDiasLimiteFirma = numeroDiasLimiteFirma;
    }

    public Integer getIcDocumentoAutografo() {
        return icDocumentoAutografo;
    }

    public void setIcDocumentoAutografo(Integer icDocumentoAutografo) {
        this.icDocumentoAutografo = icDocumentoAutografo;
    }
    
}
