package com.nafin.docgarantias.parametrizacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class NombresUsuarios {
    
    private static final Log log = ServiceLocator.getInstance().getLog(NombresUsuarios.class);
    
    public NombresUsuarios() {
        super();
    }
    
    public static void main(String argunmentos[]){        
        NombresUsuarios objeto = new NombresUsuarios();
        //System.out.print(objeto.actualizarNombresUsuarios());
        }
    
    
    /* Perfiles a buscar en el directorio de argus: 
     *  ADMIN GESTGAR
     *  SOL GESTGAR 
     *  
     *  
     *  Perfiles que son parametrizados y se obtienen de DGOC_PARAMETRO_USUARIO:
     *  ADMIN IF GARANT
     *  OPE GESTGAR
     *  AUTO GESTGAR
     *  
     *  Perfiles que no se requieren
     *  
     *  CONS GESTGAR 
     * */
    
    public ArrayList<String> BuscayactualizarNombresUsuarios(){
        log.info("actualizarNombresUsuarios(E)");
        ArrayList<String> listaUsuarios = new ArrayList<>();       
        String sqlBuscaUsuarios =  
        " SELECT                                                   "+
        "     TRIM(ic_usuario) as ic_usuario                       "+
        " FROM                                                     "+
        "     (                                                    "+
        "         SELECT                                           "+
        "             replace(membercn, 'cn=', '') ic_usuario      "+
        "         FROM                                             "+
        "             directory_group_tipo_clave                   "+
        "         WHERE                                            "+
        "             name LIKE '%SOL GESTGAR'                     "+
        "             OR name LIKE '%ADMIN GESTGAR'                "+
        "         UNION                                            "+
        "         SELECT DISTINCT                                  "+
        "             TRIM(ic_usuario)                             "+
        "         FROM                                             "+
        "             gdoc_parametros_usuario                      "+
        "         WHERE                                            "+
        "             length(TRIM(ic_usuario)) >= 8                "+
        "     )                                                    "+
        " MINUS                                                    "+
        " SELECT                                                   "+
        "     TRIM(ic_usuario) as ic_usuario                       "+
        " FROM                                                     "+
        "     gdoc_nombre_usuario                                  ";
        log.info(sqlBuscaUsuarios);
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        UtilUsr utilUsr = new UtilUsr();
        try{
            con.conexionDB();
            ps = con.queryPrecompilado(sqlBuscaUsuarios);
            rs = ps.executeQuery();
            String sqlInsertaNombre = "INSERT INTO GDOC_NOMBRE_USUARIO (IC_USUARIO, CD_NOMBRE_USUARIO) VALUES (?,?) ";    
            ps = con.queryPrecompilado(sqlInsertaNombre); 
            while(rs.next()){
                String ic_usuario = rs.getString("ic_usuario").trim();
                if (ic_usuario != null && ic_usuario.length() == 8){
                    Usuario usuario = utilUsr.getUsuario(ic_usuario);
                    if (usuario != null && usuario.getNombre() != null){
                        String nombreCompleto = usuario.getNombreCompleto().replaceAll("\\s+", " ");
                        ps.setString(1, ic_usuario);
                        ps.setString(2, replaceLatinChar(nombreCompleto));
                        ps.executeUpdate();
                        listaUsuarios.add(ic_usuario+" - "+nombreCompleto); 
                    }
                    else{
                        log.error("Usuario sin nombre o no encontrado: "+ ic_usuario);
                    }
                }
            }
        }
        catch(SQLException | NamingException e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al actualizar los nombres de los usuarios", e);
        } 
        catch(Throwable e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al actualizar los nombres de los usuarios", e);
        }          
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }           
        log.info("actualizarNombresUsuarios(S)");
        return listaUsuarios; 
    }
    
    
    public ArrayList<ElementoCatalogo> getUsuariosSinNombre(){
        log.info("getUsuariosSinNombre(E)");
        ArrayList<ElementoCatalogo> listaUsuarios = new ArrayList<>();       
        String sqlBuscaUsuarios =  
        " SELECT                                                   "+
        "     TRIM(ic_usuario) as ic_usuario                       "+
        " FROM                                                     "+
        "     (                                                    "+
        "         SELECT                                           "+
        "             replace(membercn, 'cn=', '') ic_usuario      "+
        "         FROM                                             "+
        "             directory_group_tipo_clave                   "+
        "         WHERE                                            "+
        "             name LIKE '%SOL GESTGAR'                     "+
        "             OR name LIKE '%ADMIN GESTGAR'                "+
        "         UNION                                            "+
        "         SELECT DISTINCT                                  "+
        "             TRIM(ic_usuario)                             "+
        "         FROM                                             "+
        "             gdoc_parametros_usuario                      "+
        "         WHERE                                            "+
        "             length(TRIM(ic_usuario)) >= 8                "+
        "     )                                                    "+
        " MINUS                                                    "+
        " SELECT                                                   "+
        "     TRIM(ic_usuario) as ic_usuario                       "+
        " FROM                                                     "+
        "     gdoc_nombre_usuario                                  ";
        log.info(sqlBuscaUsuarios);
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con.conexionDB();
            ps = con.queryPrecompilado(sqlBuscaUsuarios);
            rs = ps.executeQuery();
            while(rs.next()){
                if (rs.getString("ic_usuario") != null){
                    String ic_usuario = rs.getString("ic_usuario").trim();
                    listaUsuarios.add(new ElementoCatalogo(ic_usuario, ""));
                }
            }
        }
        catch(SQLException | NamingException e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar los usuarios", e);
        } 
        catch(Throwable e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar los usuarios", e);
        }          
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }           
        log.info("getUsuariosSinNombre(S)");
        return listaUsuarios; 
    } 
    
    public ArrayList<ElementoCatalogo> actualizarNombresUsuarios(){
        log.info("actualizarNombresUsuarios(E)");
        ArrayList<ElementoCatalogo> listaUsuariosNombres = new ArrayList<>();
        ArrayList<ElementoCatalogo> listaDeUsuarios = getUsuariosSinNombre();
        AccesoDB con = new AccesoDB();
        String noencontrado= "Nombre no encontrado";
        PreparedStatement ps = null;
        ResultSet rs = null;
        UtilUsr utilUsr = new UtilUsr();
        try{
            con.conexionDB();
            String sqlInsertaNombre = "INSERT INTO GDOC_NOMBRE_USUARIO (IC_USUARIO, CD_NOMBRE_USUARIO) VALUES (?,?) ";    
            ps = con.queryPrecompilado(sqlInsertaNombre); 
            for(ElementoCatalogo u : listaDeUsuarios){
                String usu = u.getClave();
                if (usu != null && usu.length() == 8){
                    Usuario usuario = utilUsr.getUsuario(usu);
                    ps.setString(1, usu);
                    if (usuario != null && usuario.getNombre() != null){
                        String nombreCompleto = usuario.getNombreCompleto().replaceAll("\\s+", " ");
                        ps.setString(2, replaceLatinChar(nombreCompleto));
                        ps.executeUpdate();
                        listaUsuariosNombres.add(new ElementoCatalogo(usu, nombreCompleto)); 
                    }
                    else{
                        ps.setString(2, noencontrado);
                        ps.executeUpdate();                        
                        listaUsuariosNombres.add(new ElementoCatalogo(usu, noencontrado)); 
                        log.error("Usuario sin nombre o no encontrado: "+ usu);
                    }
                }
            }
        }
        catch(SQLException | NamingException e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al actualizar los nombres de los usuarios", e);
        } 
        catch(Throwable e) {
            e.printStackTrace();
            throw new AppException("Error inesperado al actualizar los nombres de los usuarios", e);
        }          
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }           
        log.info("actualizarNombresUsuarios(S)");
        return listaUsuariosNombres; 
    }    
    
    public String replaceLatinChar(String textoConCaracteresLatinos){
        Map<String, String> chr = new HashMap<>();
        chr.put("�", "'||chr(193)||'");
        chr.put("�", "'||chr(201)||'");
        chr.put("�", "'||chr(205)||'");
        chr.put("�", "'||chr(211)||'");
        chr.put("�", "'||chr(218)||'");
        chr.put("�", "'||chr(209)||'");
        chr.put("�", "'||chr(225)||'");
        chr.put("�", "'||chr(233)||'");
        chr.put("�", "'||chr(237)||'");
        chr.put("�", "'||chr(243)||'");
        chr.put("�", "'||chr(250)||'");
        chr.put("�", "'||chr(241)||'");
        for (Map.Entry<String, String> c : chr.entrySet()) {
            textoConCaracteresLatinos = textoConCaracteresLatinos.replaceAll(c.getKey(),c.getValue());
        }
    return textoConCaracteresLatinos;    
    }
    
}
