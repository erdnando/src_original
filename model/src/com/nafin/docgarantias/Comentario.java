package com.nafin.docgarantias;

import java.io.Serializable;

public class Comentario implements Serializable{
    @SuppressWarnings("compatibility:6359905162385246062")
    private static final long serialVersionUID = 1L;

    private String commentario;
    private String fechaRegistro;
    private String nombreUsuario;

    public Comentario(String commentario, String fechaRegistro, String nombreUsuario) {
        super();
        this.commentario = commentario;
        this.fechaRegistro = fechaRegistro;
        this.nombreUsuario = nombreUsuario;
    }

    public Comentario() {
        super();
    }

    public String getCommentario() {
        return commentario;
    }

    public void setCommentario(String commentario) {
        this.commentario = commentario;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
}
