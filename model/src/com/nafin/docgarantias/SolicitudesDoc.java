package com.nafin.docgarantias;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface SolicitudesDoc {

    public ArrayList<SolicitudDoc> getSolicitudes(ArrayList<String> param);
    
    public ArrayList<ElementoCatalogo> getCatIF(Integer tipoIF);
    
    public ArrayList<ElementoCatalogo> getCatDocumentos();
    
    public ArrayList<ElementoCatalogo> getCatPortafolio();
    
    public String altaSolicitud(SolicitudDoc solicitud);
    
    public String updateSolicitud(SolicitudDoc solicitud, boolean esRechazo, String nuevoComentario, String usuario);
    
    public ArrayList<ElementoCatalogo> getFileSolicitud(Integer icSolicitud);
    
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes();
    
    public ArrayList<ElementoCatalogo> getCatEstatusSolicitud();
    
    public ArrayList<ElementoCatalogo> getCatEjecutivos();
    
    public ArrayList<ElementoCatalogo> getALLCatEjecutivos();
    
    public ArrayList<ElementoCatalogo> getCatSolicitantes();
    
    public ArrayList<ElementoCatalogo> getCatSolicitudesIF();
    
    public boolean eliminarSolicitud(Integer icSolicitud);
    
    public SolicitudDoc getSolicitud(Integer icSolicitud);
    
    public boolean guardarActualizarDocumento(Documento docto);
    
    public ArrayList<Comentario> getComentariosSol(Integer icSolicitud);
    
    public Documento getDocumentoByIcSolicitud(Integer icSolicitud);
    
    public Documento getDocumentoByIcDocumento(Integer icDocumento);
    
    public boolean asignarSolicitudEjecutvo(Integer icSolicitud, String usuario);
    
    public boolean isSolicitudAsignadaEjecutivo(Integer icSolicitud, String usuario);
    
    public Integer getIcDocumentoFromFolioSolicitud(Integer anio, Integer folio);
    
    public String getFolioDocSolicitudOrigen(Integer icDocumentoOrigen);
    
    public boolean setTipoMovimientoDocumento(Integer icMovimiento, Integer icDocumento);
    
    public ArrayList<DocValidacion> getDocumentosValidacion(Map<String,String> params);
 
    public ArrayList<ElementoCatalogo> getCatProducto(Integer icEstatus, Integer icIF);   
    
    public ArrayList<ElementoCatalogo> getCatEjecutivosAtencion();
    public ArrayList<ElementoCatalogo> getCatEjecutivosPorIF(Integer icIF);
    public ArrayList<ElementoCatalogo> getCatIFformalizacion();
    
    public boolean isRetorno(Integer icSolicitud, Integer estatusActual);
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento();
    public Integer getIcArchivoIFData(Integer icSolicitud, Integer icIF);
    public boolean deleteIcArchivoIF(Integer icSolicitud, Integer icIF, Integer icArchivo);
}
