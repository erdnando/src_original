package com.nafin.docgarantias;

import java.io.Serializable;

import java.util.ArrayList;

public class SolicitudDoc extends Documento implements Serializable {
    @SuppressWarnings("compatibility:37328268504827854")
    private static final long serialVersionUID = 2915711115304224676L;

    private String folio;
    private String descripcion;
    private Integer tipoSolicitud;
    private Integer portafolio;
    private String nombre;
    private Integer numeroArchivos;
    private String nombreUsuarioRegistro;
    private String usuarioRegistro;
    private String nombreUsuarioEjecutivo;
    private String usuarioEjecutivo;
    private ArrayList<Integer> sIntermediarios;
    private String nombreIntermediario;
    private ArrayList<Integer> sArchivos;
    private String fechaSolicitud;
    private String fechaLiberacion;
    private boolean esRechazo;
    private String nombreTipoDocumento;
    private String nombrePortafolio;
    private boolean soloLectura;

    public SolicitudDoc() {
        super();
        super.setIcTipoDocumento(0);
    }



    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(Integer tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public Integer getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(Integer portafolio) {
        this.portafolio = portafolio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }


    @Override
    public String toString() {
        String ifs = "[";
        if (this.getSIntermediarios() != null){
            for(Integer i : this.getSIntermediarios()) {
                    ifs =ifs+i+",";
            }
            ifs.substring(0, ifs.length()-1);            
        }
        ifs = ifs+"]";

        String returnString ="Solicitud{"
        +"descripcion    :"+descripcion     +","
        +"tipoSolicitud  :"+tipoSolicitud   +","
        +"tipoDocumento  :"+this.getIcTipoDocumento() +","
        +"portafolio     :"+portafolio      +","
        +"nombre         :"+nombre          +","
        +"numeroArchivo  :"+numeroArchivos  +","
        +"usuarioRegistro:"+usuarioRegistro +","
        +"estatus        :"+this.getEstatus()+","
        +"intermediarios :"+ifs+"}";
        return returnString;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getUsuarioEjecutivo() {
        return usuarioEjecutivo;
    }

    public void setUsuarioEjecutivo(String usuarioEjecutivo) {
        this.usuarioEjecutivo = usuarioEjecutivo;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFechaLiberacion() {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(String fechaLiberacion) {
        this.fechaLiberacion = fechaLiberacion;
    }

    public Integer getNumeroArchivos() {
        return numeroArchivos;
    }

    public void setNumeroArchivos(Integer numeroArchivos) {
        this.numeroArchivos = numeroArchivos;
    }

    public String getNombreUsuarioRegistro() {
        return nombreUsuarioRegistro;
    }

    public void setNombreUsuarioRegistro(String nombreUsuarioRegistro) {
        this.nombreUsuarioRegistro = nombreUsuarioRegistro;
    }

    public String getNombreUsuarioEjecutivo() {
        return nombreUsuarioEjecutivo;
    }

    public void setNombreUsuarioEjecutivo(String nombreUsuarioEjecutivo) {
        this.nombreUsuarioEjecutivo = nombreUsuarioEjecutivo;
    }

    public String getNombreIntermediario() {
        return nombreIntermediario;
    }

    public void setNombreIntermediario(String nombreIntermediario) {
        this.nombreIntermediario = nombreIntermediario;
    }

    public boolean isEsRechazo() {
        return esRechazo;
    }

    public void setEsRechazo(boolean esRechazo) {
        this.esRechazo = esRechazo;
    }

    public ArrayList<Integer> getSIntermediarios() {
        return sIntermediarios;
    }

    public void setSIntermediarios(ArrayList<Integer> sIntermediarios) {
        this.sIntermediarios = sIntermediarios;
    }

    public ArrayList<Integer> getSArchivos() {
        return sArchivos;
    }

    public void setSArchivos(ArrayList<Integer> sArchivos) {
        this.sArchivos = sArchivos;
    }

    public String getNombreTipoDocumento() {
        return nombreTipoDocumento;
    }

    public void setNombreTipoDocumento(String nombreTipoDocumento) {
        this.nombreTipoDocumento = nombreTipoDocumento;
    }

    public String getNombrePortafolio() {
        return nombrePortafolio;
    }

    public void setNombrePortafolio(String nombrePortafolio) {
        this.nombrePortafolio = nombrePortafolio;
    }

    public boolean isSoloLectura() {
        return soloLectura;
    }

    public void setSoloLectura(boolean soloLectura) {
        this.soloLectura = soloLectura;
    }
}
