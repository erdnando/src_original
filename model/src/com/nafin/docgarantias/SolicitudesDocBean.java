
package com.nafin.docgarantias;

import com.nafin.docgarantias.formalizacion.ConstantesFormalizacion;
import com.nafin.docgarantias.parametrizacion.usuarios.ParametrizacionUsuarios;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;


import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "SolicitudesDoc", mappedName = "SolicitudesDoc")
@TransactionManagement(TransactionManagementType.BEAN)
public class SolicitudesDocBean implements SolicitudesDoc{

    private static final int ESTATUS_POR_SOLICITAR = 100;
    private static final int ESTATUS_EN_VALIDACION = 120;
    private static final int ESTATUS_VALIDACION_JURIDICO = 130;
    
    private static final Integer NUEVA_VERSION_DOCUMENTO = 2;
    private static final String PERFIL_EJECUTIVO = "OPER GESTGAR";

    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(SolicitudesDocBean.class);
    

    public SolicitudesDocBean() {
        super();
    }

    @Override
    public ArrayList<SolicitudDoc> getSolicitudes(ArrayList<String> param) {
        AccesoDB con = new AccesoDB();
        log.info("Parametros Recibidos: "+param);
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SolicitudDoc> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        StringBuilder sqlFiltros = new StringBuilder();
        int paramsPorSetear = 0;
        String numeroUsuario = null;
        if (!param.get(0).isEmpty()){ sqlFiltros.append("AND s.IC_SOLICITUD = ? "); paramsPorSetear=paramsPorSetear|1;}
        if (!param.get(1).isEmpty()){ 
            sqlFiltros.append(" AND s.ic_solicitud in (SELECT IC_SOLICITUD FROM GDOCREL_SOLICITUD_IF WHERE IC_IF = ? )");    
            paramsPorSetear=paramsPorSetear|2;
        }
        if (!param.get(2).isEmpty()){ sqlFiltros.append("AND s.IC_USUARIO_REGISTRO = ? "); paramsPorSetear=paramsPorSetear|4;}
        if (!param.get(3).isEmpty()){ 
            String tipoEjecutivo = param.get(3);            
            if (tipoEjecutivo.indexOf("*") == 0){
                numeroUsuario = tipoEjecutivo.substring(1, tipoEjecutivo.trim().length());
                sqlFiltros.append("AND (s.IC_USUARIO_EJECUTIVO = ? OR s.IC_USUARIO_EJECUTIVO IS NULL ) ");         
            }
            else{
                sqlFiltros.append("AND s.IC_USUARIO_EJECUTIVO = ? ");    
                numeroUsuario = tipoEjecutivo.trim();
            }
            paramsPorSetear=paramsPorSetear|8;
        }
        if (!param.get(4).isEmpty()){ sqlFiltros.append("AND s.IC_ESTATUS = ? "); paramsPorSetear=paramsPorSetear|16;}
        if (!param.get(5).isEmpty()){ 
            sqlFiltros.append("AND s.DT_FECHA_CREACION BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') "); 
            paramsPorSetear=paramsPorSetear|32;
        }
        if(param.get(4).isEmpty() && !param.get(6).isEmpty()){
            sqlFiltros.append(" AND s.IC_ESTATUS >= 110 "); 
        }
                
        try {
            con.conexionDB();
            strSQL.append(" "+
            "SELECT                                                                                                         "+
            "   s.IC_SOLICITUD,                                                                                             "+
            "   s.TG_TIPO_SOLICITUD,                                                                                        "+
            "   s.IC_TIPO_DOCUMENTO,                                                                                        "+
            "   s.IC_PORTAFOLIO,                                                                                            "+
            "   s.IC_ESTATUS,                                                                                               "+
            "   s.TG_DESCRIPCION,                                                                                           "+
            "   s.TG_NOMBRE,                                                                                                "+
            "   TO_CHAR(s.DT_FECHA_CREACION,'DD/MM/YYYY') FECHA_CREACION ,                                                  "+
            "   s.DT_FECHA_LIBERACION,                                                                                      "+
            "   e.CD_NOMBRE_ESTATUS,                                                                                        "+
            "   s.IC_USUARIO_REGISTRO,                                                                                      "+
            " (SELECT CD_NOMBRE_USUARIO from gdoc_nombre_usuario where ic_usuario = trim(s.ic_usuario_registro)) as usuario_registro, "+
            "   s.IC_USUARIO_EJECUTIVO,                                                                                     "+
            " (SELECT CD_NOMBRE_USUARIO from gdoc_nombre_usuario where ic_usuario = trim(s.ic_usuario_ejecutivo)) as nombre_ejecutivo, "+
            "   CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO,                            "+
            "   (SELECT count(*) FROM GDOCREL_SOLICITUD_ARCHIVO sa WHERE s.ic_solicitud = sa.ic_solicitud)                  "+
            "       as NumeroDoumentos,                                                                                     "+
            " (SELECT  case  when wh>1 then 'Varios'                                                                        "+
            "  else (select i.cg_nombre_Comercial from comcat_if i, gdocrel_solicitud_if x where                            "+
            "           i.ic_if = x.ic_if and x.ic_solicitud = icSolicitud) end                                             "+
            "  FROM                                                                                                         "+
            " (select si.ic_solicitud as icSolicitud, count(*) as wh                                                        "+
            " FROM COMCAT_IF ci, Gdocrel_Solicitud_If si WHERE ci.ic_if = si.ic_if                                          "+
            " and si.ic_solicitud = s.ic_solicitud group by si.ic_solicitud))as nombreIF,                                   "+
            " d.IC_ARCHIVO, d.IC_MOVIMIENTO, d.IC_VERSION,                                                                  "+
            " (SELECT COUNT(IC_PROCESO_DOCTO) FROM GDOC_BITACORA_DOC WHERE IC_SOLICITUD = s.IC_SOLICITUD                    "+
            " AND IC_ESTATUS > s.IC_ESTATUS and IC_IF IS NULL ) AS ES_RECHAZO                                               "+
            " FROM                                                                                                          "+
            "     GDOC_SOLICITUD s,                                                                                         "+
            "     GDOC_DOCUMENTO d,                                                                                         "+
            "     GDOCCAT_ESTATUS e                                                                                         "+
            " WHERE                                                                                                         "+
            "   s.IC_ESTATUS = e.IC_ESTATUS AND d.IC_SOLICITUD = s.IC_SOLICITUD                                             "+
            sqlFiltros                                                                                                       +
            " ORDER BY                                                                                                      "+
            "   s.DT_FECHA_CREACION DESC                                                                                    "+      
                "    ");
           
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            
            if (!param.get(0).isEmpty()){ 
                ps.setInt(1, Integer.parseInt(param.get(0)));
                log.info(Integer.parseInt(param.get(0)));
                }
            if (!param.get(1).isEmpty()){ 
                ps.setInt(Integer.bitCount(paramsPorSetear & 3), Integer.parseInt(param.get(1)) );
                log.info("Intermediario Financiero: "+ param.get(1));                
            }
            if (!param.get(2).isEmpty()){ 
                ps.setString(Integer.bitCount(paramsPorSetear & 7), String.format("%1$-9s", param.get(2).trim()));
                log.info(param.get(2));
            }                
            if (!param.get(3).isEmpty()){ // USUARIO EJECUTIVO 
                ps.setString(Integer.bitCount(paramsPorSetear & 15), String.format("%1$-9s", numeroUsuario));
                log.info("indice:"+Integer.bitCount(paramsPorSetear & 15)+" "+numeroUsuario);
            }                
            
            if (!param.get(4).isEmpty()){ // ESTATUS
                ps.setInt(Integer.bitCount(paramsPorSetear & 31), Integer.parseInt(param.get(4)) );
                log.info("indice:"+Integer.bitCount(paramsPorSetear & 31)+" "+Integer.parseInt(param.get(4)));
            }                               
            if (!param.get(5).isEmpty()){ 
                ps.setString(Integer.bitCount(paramsPorSetear & 127), param.get(5) );
                ps.setString(Integer.bitCount(paramsPorSetear & 127)+1,param.get(6) );
                log.error("Fechas: "+param.get(5)+" - "+param.get(6));
            }
            
            
            rs = ps.executeQuery();

            while (rs.next()){
                SolicitudDoc sol = new SolicitudDoc();
                sol.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                sol.setFolio(rs.getString("FOLIO"));
                sol.setDescripcion(rs.getString("TG_DESCRIPCION"));
                sol.setNombre(rs.getString("TG_NOMBRE"));
                sol.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                sol.setFechaSolicitud(rs.getString("FECHA_CREACION"));
                sol.setEstatus(rs.getInt("IC_ESTATUS"));
                sol.setNombreEstatus(rs.getString("CD_NOMBRE_ESTATUS"));
                sol.setNumeroArchivos(rs.getInt("NumeroDoumentos"));
                sol.setNombreIntermediario(rs.getString("nombreIF"));
                sol.setFechaLiberacion(rs.getString("DT_FECHA_LIBERACION"));
                sol.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                sol.setIcMovimientoSeleccionado(rs.getInt("IC_MOVIMIENTO"));
                sol.setIcVersion(rs.getInt("IC_VERSION"));
                sol.setUsuarioRegistro(rs.getString("usuario_registro"));
                sol.setUsuarioEjecutivo(rs.getString("nombre_ejecutivo"));
                int esRechazo = rs.getInt("ES_RECHAZO");
                sol.setEsRechazo(esRechazo > 0);
                retorno.add(sol);
            
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatIF(Integer tipoIF) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        String filtroSQL = "";
        if (tipoIF != null && (tipoIF == 1 || tipoIF == 2) ) {
            filtroSQL = " AND CS_TIPO = " + (tipoIF ==1 ? "'B' " : "'NB'" );
        }
        
        try {
            con.conexionDB();
            strSQL.append("SELECT ic_if, CG_RAZON_SOCIAL FROM COMCAT_IF WHERE IC_IF_SIAG IS NOT NULL "+ filtroSQL  +" ORDER BY CG_RAZON_SOCIAL ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_if");
                String descripcion = rs.getString("CG_RAZON_SOCIAL");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatDocumentos() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT ic_tipo_documento, cd_nombre_documento FROM GDOCCAT_DOCUMENTO ORDER BY ic_tipo_documento ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_tipo_documento");
                String descripcion = rs.getString("cd_nombre_documento");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }


    @Override
    public ArrayList<ElementoCatalogo> getCatPortafolio() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT ic_portafolio, cd_nombre_portafolio FROM GDOCCAT_PORTAFOLIO order by CD_NOMBRE_PORTAFOLIO ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_portafolio");
                String descripcion = rs.getString("cd_nombre_portafolio");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public String altaSolicitud(SolicitudDoc solicitudDoc) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        Integer numeroFolio = null;
        String folioGenerado = null;
        try {
            con.conexionDB();
            Integer icSolicitud= 0;
            String sqlQueryNextVal = " SELECT SEQ_GDOC_SOLICITUD.nextval FROM dual  ";
            rs = con.queryDB(sqlQueryNextVal);
            if (rs.next()){
                icSolicitud = rs.getInt(1);
            }
            String sqlQueryVal = " SELECT  NVL(max(IC_FOLIO),0)+1 FROM gdoc_solicitud where EXTRACT(YEAR FROM DT_FECHA_CREACION) = EXTRACT(YEAR FROM SYSDATE) ";
            rs = con.queryDB(sqlQueryVal);
            if (rs.next()){
                numeroFolio = rs.getInt(1);
            }
            
            String usuarioEjecutivo = null;
            ArrayList<Integer> ifs = solicitudDoc.getSIntermediarios();
            if (ifs != null && !ifs.isEmpty() && ifs.size() == 1){
                usuarioEjecutivo = this.getUsuarioAsignado(solicitudDoc);    
            }
            log.info("Numero Folio:"+numeroFolio);
            strSQL.append(" INSERT INTO GDOC_SOLICITUD (IC_SOLICITUD,TG_TIPO_SOLICITUD,IC_TIPO_DOCUMENTO,IC_FOLIO,IC_PORTAFOLIO,TG_DESCRIPCION,TG_NOMBRE,DT_FECHA_CREACION, IC_ESTATUS,IC_USUARIO_REGISTRO,IC_USUARIO_EJECUTIVO) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, sysdate, (SELECT MIN(IC_ESTATUS) FROM GDOCCAT_ESTATUS), ? , ?) ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, icSolicitud);
            ps.setInt(2, solicitudDoc.getTipoSolicitud());
            ps.setInt(3, solicitudDoc.getIcTipoDocumento());
            ps.setInt(4, numeroFolio);
            ps.setInt(5, solicitudDoc.getPortafolio());
            ps.setString(6, solicitudDoc.getDescripcion());
            ps.setString(7, solicitudDoc.getNombre());
            ps.setString(8, solicitudDoc.getUsuarioRegistro());     
            if (usuarioEjecutivo != null){
                ps.setString(9, usuarioEjecutivo);     
            }
            else{
                ps.setNull(9, Types.VARCHAR);    
            }
            
            ps.executeUpdate();

            // Insertar el registro del documento
            String findVersion = ", (SELECT MAX(IC_VERSION)+1 FROM GDOC_DOCUMENTO WHERE IC_DOCUMENTO = ?) ";
            StringBuilder strSQLDoc = new StringBuilder();
            strSQLDoc.append(" INSERT INTO GDOC_DOCUMENTO (IC_DOCUMENTO, IC_TIPO_DOCUMENTO, IC_SOLICITUD, DT_FECHA_REGISTRO, IC_VERSION");
            if(solicitudDoc.getIcDocumentoOrigen() != null){
                strSQLDoc.append(", IC_DOCUMENTO_ORIGEN ");
                
            }
            strSQLDoc.append(") VALUES  (SEQ_GDOC_DOCUMENTO.NEXTVAL, ?, ?, SYSDATE");
            if(solicitudDoc.getIcDocumentoOrigen() != null){
                strSQLDoc.append(findVersion);
                strSQLDoc.append(", ? ");
            }
            else{
                strSQLDoc.append(",  1 ");
            }
            
            strSQLDoc.append(" ) ");
            log.info(strSQLDoc.toString());
            ps = con.queryPrecompilado(strSQLDoc.toString());
            ps.setInt(1, solicitudDoc.getIcTipoDocumento());
            ps.setInt(2, icSolicitud);
            if(solicitudDoc.getIcDocumentoOrigen() != null){
                ps.setInt(3, solicitudDoc.getIcDocumentoOrigen());
                ps.setInt(4, solicitudDoc.getIcDocumentoOrigen());
            }
            ps.executeUpdate();            
            if (!solicitudDoc.getSIntermediarios().isEmpty()){
                int numeroIFSeleccionados = solicitudDoc.getSIntermediarios().size();
                StringBuilder strSQLIF = new StringBuilder();
                strSQLIF.append(" INSERT INTO GDOCREL_SOLICITUD_IF (IC_IF, IC_SOLICITUD) VALUES (?,?)");
                for(int numeroIF =0; numeroIF< numeroIFSeleccionados; numeroIF++){
                    ps = con.queryPrecompilado(strSQLIF.toString());
                    ps.setInt(1, solicitudDoc.getSIntermediarios().get(numeroIF));
                    ps.setInt(2, icSolicitud);
                    ps.executeUpdate();
                }    
            }            
            if (!solicitudDoc.getSArchivos().isEmpty()){
                int numeroArchivosAdjuntados = solicitudDoc.getSArchivos().size();
                StringBuilder strSQLFile = new StringBuilder();
                strSQLFile.append(" INSERT INTO GDOCREL_SOLICITUD_ARCHIVO (IC_ARCHIVO, IC_SOLICITUD) VALUES (?,?) ");
                for(int fileIndice =0; fileIndice< numeroArchivosAdjuntados; fileIndice++){
                    ps = con.queryPrecompilado(strSQLFile.toString());
                    ps.setInt(1, solicitudDoc.getSArchivos().get(fileIndice));
                    ps.setInt(2, icSolicitud);
                    ps.executeUpdate();
                }    
            }
            
            resultado = true;
            folioGenerado = Calendar.getInstance().get(Calendar.YEAR)+"-"+numeroFolio;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return folioGenerado;
    }


    @Override
    public String updateSolicitud(SolicitudDoc solicitud, boolean esRechazo, String nuevoComentario, String usuario) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        String campoDescripcion = " TG_DESCRIPCION = ? , ";
        String sqlDocUpdate = "";
        String sqlUsuarioEjecutivo = "";
        try {
            con.conexionDB();
            if (esRechazo){
                campoDescripcion = ""    ;
            }
            else{
                String usuarioEjecutivo = this.getUsuarioAsignado(solicitud);
                if(usuarioEjecutivo != null)
                    sqlUsuarioEjecutivo = ",  IC_USUARIO_EJECUTIVO = '"+ usuarioEjecutivo +"' ";
            }

            strSQL.append(" UPDATE GDOC_SOLICITUD SET " +
                          " TG_TIPO_SOLICITUD = ? , " +
                          " IC_TIPO_DOCUMENTO = ? , " +
                          " IC_PORTAFOLIO = ? , " +
                           campoDescripcion +
                          " TG_NOMBRE = ? " +
                          sqlUsuarioEjecutivo +
                          " WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            
            ps.setInt(1, solicitud.getTipoSolicitud());
            ps.setInt(2, solicitud.getIcTipoDocumento());
            ps.setInt(3, solicitud.getPortafolio());
            int indiceParametroPS = 4;
            if (!esRechazo){
                    ps.setString(indiceParametroPS, solicitud.getDescripcion());
                    indiceParametroPS++;
            }
            ps.setString(indiceParametroPS, solicitud.getNombre());
            indiceParametroPS++;
            ps.setInt(indiceParametroPS, solicitud.getIcSolicitud());
            ps.executeUpdate();
            
            
            if (solicitud.getIcDocumentoOrigen() != null){
                sqlDocUpdate = " UPDATE GDOC_DOCUMENTO SET IC_DOCUMENTO_ORIGEN = ? WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlDocUpdate);
                ps.setInt(1, solicitud.getIcDocumentoOrigen());
                ps.setInt(2, solicitud.getIcDocumento());
                ps.executeUpdate();    
            }
            
            StringBuilder strSQLIFs = new StringBuilder();
            strSQLIFs.append(" DELETE FROM GDOCREL_SOLICITUD_IF WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLIFs.toString()); 
            ps.setInt(1, solicitud.getIcSolicitud());
            ps.executeUpdate();
            if (!solicitud.getSIntermediarios().isEmpty()){
                int numeroIFSeleccionados = solicitud.getSIntermediarios().size();
                StringBuilder strSQLIF = new StringBuilder();
                strSQLIF.append(" INSERT INTO GDOCREL_SOLICITUD_IF (IC_IF, IC_SOLICITUD) VALUES ( ? , ?)");
                for(int numeroIF =0; numeroIF< numeroIFSeleccionados; numeroIF++){
                    ps = con.queryPrecompilado(strSQLIF.toString());
                    ps.setInt(1, solicitud.getSIntermediarios().get(numeroIF));
                    ps.setInt(2, solicitud.getIcSolicitud());
                    ps.executeUpdate();
                }    
            }            
            
            StringBuilder strSQLFiles = new StringBuilder();
            strSQLFiles.append(" DELETE FROM GDOCREL_SOLICITUD_ARCHIVO WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLFiles.toString()); 
            ps.setInt(1, solicitud.getIcSolicitud());
            ps.executeUpdate();
            
            if (!solicitud.getSArchivos().isEmpty()){
                int numeroArchivosAdjuntados = solicitud.getSArchivos().size();
                StringBuilder strSQLFile = new StringBuilder();
                strSQLFile.append(" INSERT INTO GDOCREL_SOLICITUD_ARCHIVO (IC_ARCHIVO, IC_SOLICITUD) VALUES (?,?) ");
                for(int fileIndice =0; fileIndice< numeroArchivosAdjuntados; fileIndice++){
                    ps = con.queryPrecompilado(strSQLFile.toString());
                    ps.setInt(1, solicitud.getSArchivos().get(fileIndice));
                    ps.setInt(2, solicitud.getIcSolicitud());
                    ps.executeUpdate();
                }    
            }
            
            if(esRechazo && nuevoComentario != null && !nuevoComentario.isEmpty()){
                StringBuilder strSQLComments = new StringBuilder();
                strSQLComments.append(" INSERT INTO GDOC_COMENTARIO_SOLICITUD (IC_COMENTARIO_SOLICITUD, IC_SOLICITUD, TG_DESCRIPCION, DT_FECHA_REGISTRO, IC_USUARIO_REGISTRO, IC_ESTATUS)\n" + 
                            " VALUES (SEQ_GDOC_COMENTARIO_SOLICITUD.NEXTVAL, ?, ?, SYSDATE, ?, (SELECT IC_ESTATUS FROM GDOC_SOLICITUD WHERE IC_SOLICITUD = ? )) ");
                ps = con.queryPrecompilado(strSQLComments.toString());
                ps.setInt(1, solicitud.getIcSolicitud());
                ps.setString(2, nuevoComentario);
                ps.setString(3, usuario);
                ps.setInt(4, solicitud.getIcSolicitud());
                ps.executeUpdate();     
            }
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return solicitud.getFolio();        
    }


    @Override
    public ArrayList<ElementoCatalogo> getFileSolicitud(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append("   select a.Ic_Archivo,  a.CD_NOMBRE_ARCHIVO from GDOC_ARCHIVO a, Gdocrel_Solicitud_Archivo sa where " + 
                "  Sa.Ic_Archivo = a.Ic_Archivo and   Sa.Ic_Solicitud = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("Ic_Archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select s.ic_Solicitud, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO from GDOC_SOLICITUD s ORDER by s.dt_fecha_creacion desc  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_Solicitud");
                String descripcion = rs.getString("FOLIO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatEstatusSolicitud() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_ESTATUS, CD_NOMBRE_ESTATUS FROM GDOCCAT_ESTATUS ORDER BY IC_ESTATUS ASC  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("IC_ESTATUS");
                String descripcion = rs.getString("CD_NOMBRE_ESTATUS");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    @Override
    public ArrayList<ElementoCatalogo> getCatSolicitantes() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" Select distinct(IC_USUARIO_REGISTRO)  FROM  GDOC_SOLICITUD WHERE IC_USUARIO_REGISTRO IS NOT NULL ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO_REGISTRO");
                Usuario usuario = utilUsr.getUsuario(val);
                if (usuario.getLogin() != null){
                    ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                    retorno.add(elcat);
                }
                else{
                    log.error("Usuario no encontrado: "+ val);
                }
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    /** Regesa el ctalogo de Ejecutivos que tengan una solicitud registrada
     * **/
    @Override
    public ArrayList<ElementoCatalogo> getCatEjecutivos() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT distinct(IC_USUARIO_EJECUTIVO) FROM GDOC_SOLICITUD WHERE IC_USUARIO_EJECUTIVO IS NOT NULL ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            ElementoCatalogo catTodos = new ElementoCatalogo("0", "Todos los Ejecutivos");
            retorno.add(catTodos);
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO_EJECUTIVO");
                Usuario usuario = utilUsr.getUsuario(val);
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    /** Regresa una lista con los Ejecutivos que tengan parametrizado el/los Intermediarios Financieros
     *  De la solicitud enviada
     * */
    @Override
    public ArrayList<ElementoCatalogo> getCatEjecutivosPorIF(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT IC_USUARIO FROM GDOC_USUARIO_IF WHERE IC_IF =  ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO");
                Usuario usuario = utilUsr.getUsuario(val);
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la información getCatEjecutivosPorIF()", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    @Override
    public ArrayList<ElementoCatalogo> getALLCatEjecutivos() {
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        try {
            ParametrizacionUsuarios parametrizacionBean = ServiceLocator.getInstance().lookup("ParametrizacionUsuariosBean",ParametrizacionUsuarios.class);
            ArrayList<Usuario> listaUsuario = parametrizacionBean.getUsuariosNafinPorPerfil(PERFIL_EJECUTIVO);
            for(Usuario usu : listaUsuario){
                ElementoCatalogo catTodos = new ElementoCatalogo(usu.getLogin(),usu.getNombreCompleto());
                retorno.add(catTodos);
            }            
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error en el catalogo de Ejecutivos, getALLCatEjecutivos", e);
        }
        return retorno;
    }
    
    
    @Override
    public ArrayList<ElementoCatalogo> getCatSolicitudesIF() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT distinct(i.ic_if), i.CG_NOMBRE_COMERCIAL FROM COMCAT_IF i,  GDOCREL_SOLICITUD_IF r WHERE  r.ic_if = i.ic_if ORDER BY i.CG_NOMBRE_COMERCIAL ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getInt("ic_if")+"";
                String descripcion =  rs.getString("CG_NOMBRE_COMERCIAL");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion); 
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }


    @Override
    public boolean eliminarSolicitud(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        boolean exito = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQLBit = new StringBuilder();
        StringBuilder strSQLCom = new StringBuilder();
        StringBuilder strSQLIF = new StringBuilder();
        StringBuilder strSQLFile = new StringBuilder();
        StringBuilder strSQLSOL = new StringBuilder();
        try {
            con.conexionDB();
            strSQLBit.append(" DELETE FROM GDOC_BITACORA_DOC WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLBit.toString()); 
            ps.setInt(1, icSolicitud);
            ps.executeUpdate();
            
            strSQLCom.append(" DELETE FROM GDOC_COMENTARIO_SOLICITUD WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLCom.toString()); 
            ps.setInt(1, icSolicitud);
            ps.executeUpdate();             
            
            strSQLIF.append(" DELETE FROM GDOCREL_SOLICITUD_IF WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLIF.toString()); 
            ps.setInt(1, icSolicitud);
            ps.executeUpdate();

            strSQLFile.append(" SELECT IC_ARCHIVO FROM GDOCREL_SOLICITUD_ARCHIVO WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLFile.toString()); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            Files2OnBase fileOnBase = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            while (rs.next()){
                Integer icArchivo = rs.getInt("IC_ARCHIVO");
                fileOnBase.eliminaArchivoSolicitud(icArchivo); // elimina de GDOCREL_SOLICITUD_ARCHIVO y GDOC_ARCHIVO 
            }
           
            String sqlDoc = " DELETE FROM GDOC_DOCUMENTO WHERE IC_SOLICITUD = ? ";
            ps = con.queryPrecompilado(sqlDoc); 
            ps.setInt(1, icSolicitud);
            ps.executeUpdate();
                       
            strSQLSOL.append(" DELETE FROM GDOC_SOLICITUD WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQLSOL.toString()); 
            ps.setInt(1, icSolicitud);
            ps.executeUpdate();
            exito = true;
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return exito;
    }
    

    @Override
    public SolicitudDoc getSolicitud(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        SolicitudDoc retorno = new SolicitudDoc();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append("SELECT s.IC_SOLICITUD, ");
            strSQL.append("  s.TG_TIPO_SOLICITUD, ");
            strSQL.append("  s.IC_TIPO_DOCUMENTO, ");
            strSQL.append("  s.IC_FOLIO, ");
            strSQL.append("  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO, ");
            strSQL.append("  s.IC_PORTAFOLIO, ");
            strSQL.append("  s.TG_DESCRIPCION, ");
            strSQL.append("  s.TG_NOMBRE, ");
            strSQL.append("  TO_CHAR(DT_FECHA_CREACION,'DD/MM/YYYY') FECHA_CREACION, ");
            strSQL.append("  s.IC_ESTATUS, ");
            strSQL.append("  e.cd_nombre_estatus, ");
            strSQL.append("  s.IC_USUARIO_REGISTRO, ");
            strSQL.append("  s.IC_USUARIO_EJECUTIVO, ");
            strSQL.append("  cd.Cd_Nombre_Documento, ");
            strSQL.append("  d.IC_DOCUMENTO, ");
            strSQL.append("  d.IC_DOCUMENTO_ORIGEN, d.IC_ARCHIVO, ");
            strSQL.append("  TO_CHAR(d.DT_FECHA_REGISTRO,'DD/MM/YYYY') AS FECHA_REGISTRO_DOC, ");
            strSQL.append("  (SELECT    CASE      WHEN wh>1      THEN 'Varios'      ELSE ");
            strSQL.append("        (SELECT i.cg_nombre_Comercial        FROM comcat_if i,          gdocrel_solicitud_if x ");
            strSQL.append("        WHERE i.ic_if      = x.ic_if        AND x.ic_solicitud = icSolicitud        ) ");
            strSQL.append("    END  FROM    (SELECT si.ic_solicitud AS icSolicitud, ");
            strSQL.append("      COUNT(*)              AS wh    FROM COMCAT_IF ci,      Gdocrel_Solicitud_If si ");
            strSQL.append("    WHERE ci.ic_if      = si.ic_if    AND si.ic_solicitud = s.ic_solicitud    GROUP BY si.ic_solicitud    )  )AS nombreIF, ");
            strSQL.append("  CASE    WHEN s.Ic_Portafolio IS NOT NULL    THEN      (SELECT p.CD_NOMBRE_PORTAFOLIO ");
            strSQL.append("      FROM GDOCCAT_PORTAFOLIO p      WHERE p.IC_PORTAFOLIO = s.IC_PORTAFOLIO      )    ELSE 'NA'  END AS NOMBRE_PORTAFOLIO ");
            strSQL.append("FROM GDOC_SOLICITUD s, ");
            strSQL.append(" GDOC_DOCUMENTO d, ");
            strSQL.append("  GDOCCAT_ESTATUS e, ");
            strSQL.append("  GDOCCAT_DOCUMENTO cd ");
            strSQL.append("WHERE d.IC_SOLICITUD = s.IC_SOLICITUD ");
            strSQL.append("AND e.ic_estatus      = s.ic_estatus ");
            strSQL.append("AND s.Ic_Tipo_Documento = cd.Ic_Tipo_Documento ");
            strSQL.append("AND s.IC_SOLICITUD      = ? ");
            
            log.info("-->"+strSQL.toString());
            log.info("Parametro:" + icSolicitud);
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                retorno.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                retorno.setTipoSolicitud(rs.getInt("TG_TIPO_SOLICITUD"));
                retorno.setDescripcion(rs.getString("TG_DESCRIPCION"));
                retorno.setEstatus(rs.getInt("IC_ESTATUS"));
                retorno.setSoloLectura(rs.getInt("IC_ESTATUS") > ESTATUS_POR_SOLICITAR);
                retorno.setFechaSolicitud(rs.getString("FECHA_CREACION"));
                retorno.setFolio(rs.getString("FOLIO"));
                retorno.setNombre(rs.getString("TG_NOMBRE"));
                retorno.setPortafolio(rs.getInt("IC_PORTAFOLIO"));
                retorno.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                retorno.setNombreIntermediario(rs.getString("nombreIF"));
                retorno.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                Usuario usrSolicitud = utilUsr.getUsuario(rs.getString("IC_USUARIO_REGISTRO"));
                retorno.setUsuarioRegistro(rs.getString("IC_USUARIO_REGISTRO"));
                retorno.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                retorno.setNombreUsuarioRegistro(usrSolicitud.getNombreCompleto());
                retorno.setFechaRegistroDoc(rs.getString("FECHA_REGISTRO_DOC"));
                retorno.setNombreTipoDocumento(rs.getString("Cd_Nombre_Documento"));
                retorno.setNombrePortafolio(rs.getString("NOMBRE_PORTAFOLIO"));
                retorno.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                Integer icDocOrigen = null;
                if (rs.getInt("IC_DOCUMENTO_ORIGEN")!=0){
                    icDocOrigen = rs.getInt("IC_DOCUMENTO_ORIGEN");
                }
                retorno.setIcDocumentoOrigen(icDocOrigen);
                if (rs.getString("IC_USUARIO_EJECUTIVO") != null){
                    Usuario usrEjecutivo = utilUsr.getUsuario(rs.getString("IC_USUARIO_EJECUTIVO"));
                    retorno.setNombreUsuarioEjecutivo(usrEjecutivo.getNombreCompleto());
                    retorno.setUsuarioEjecutivo(rs.getString("IC_USUARIO_EJECUTIVO"));
                }
                else{
                    retorno.setNombreUsuarioEjecutivo("Sin asignar");
                }
            }
            String sqlIfs = " SELECT i.IC_IF FROM COMCAT_IF i, GDOCREL_SOLICITUD_IF s WHERE s.ic_if = i.ic_if  and s.ic_solicitud = ? ";
            ps = con.queryPrecompilado(sqlIfs); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();    
            ArrayList<Integer> listIF = new ArrayList<>();
            while (rs.next()){
                Integer ifseleccionado = rs.getInt("IC_IF");
                listIF.add(ifseleccionado);
            }
            retorno.setSIntermediarios(listIF);

            String sqlDocIfs = " SELECT i.IC_IF FROM COMCAT_IF i, GDOCREL_DOCUMENTO_IF d WHERE d.ic_if = i.ic_if  and d.ic_documento = ? ";
            ps = con.queryPrecompilado(sqlDocIfs); 
            ps.setInt(1, retorno.getIcDocumento());
            rs = ps.executeQuery();    
            ArrayList<Integer> listDocIF = new ArrayList<>();
            while (rs.next()){
                listDocIF.add(rs.getInt("IC_IF"));
            }
            retorno.setIcIFs(listDocIF);
            
            
            String sqlArhivos = " SELECT a.IC_ARCHIVO FROM GDOCREL_SOLICITUD_ARCHIVO sa, GDOC_ARCHIVO a WHERE sa.IC_ARCHIVO = a.IC_ARCHIVO and sa.ic_solicitud = ? ";
            ps = con.queryPrecompilado(sqlArhivos); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();    
            ArrayList<Integer> listFiles = new ArrayList<>();
            while (rs.next()){
                Integer ifseleccionado = rs.getInt("IC_ARCHIVO");
                listFiles.add(ifseleccionado);
            }
            retorno.setSArchivos(listFiles);

        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }// end getSolicitud


    @Override
    public boolean guardarActualizarDocumento(Documento docto) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            Integer icDocumento = docto.getIcDocumento();            
            Integer versionDocto = null;
            if (docto.getIcMovimientoSeleccionado() == NUEVA_VERSION_DOCUMENTO){
                StringBuilder strSQLVersion = new StringBuilder();
                strSQL.append(" SELECT MAX(IC_VERSION)+1 AS NUEVA_VERSION from  GDOC_DOCUMENTO where IC_DOCUMENTO  = ? ");
                ps = con.queryPrecompilado(strSQLVersion.toString());
                ps.setInt(1, docto.getIcDocumentoOrigen());
                rs = ps.executeQuery();
                while(rs.next()){
                    versionDocto = rs.getInt("NUEVA_VERSION");                        
                }
            }
            
            int indiceParametro = 3;
            String strVersion = "";
            if (versionDocto != null){
                strVersion = ", IC_VERSION = ? ";    
                indiceParametro = 4;
            }
            strSQL.append(" UPDATE GDOC_DOCUMENTO SET  IC_TIPO_DOCUMENTO = ? , DT_FECHA_REGISTRO = sysdate, TG_NOMBRE = ?  " + strVersion +
                " WHERE IC_DOCUMENTO = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, docto.getIcTipoDocumento());
            if (indiceParametro == 4){
                ps.setString(2, docto.getNombre());
                ps.setInt(3, versionDocto);
                ps.setInt(4, icDocumento);                
            }
            else{
                ps.setString(2, docto.getNombre());
                ps.setInt(3, icDocumento);
                
            }
            ps.executeUpdate();
            
            ArrayList<Integer> ifs = docto.getIcIFs();
            if (!ifs.isEmpty()){
                // Borrar todos los registros de IF QUE NO VENGAN EN EL ARREGLO del objeto 
                StringBuilder strSQLEliminarIF = new StringBuilder();
                strSQLEliminarIF.append(" DELETE FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ? AND IC_IF NOT IN ( ");
                for (Integer icIF : ifs ){
                    strSQLEliminarIF.append(icIF+",");
                }
                String sSQLEliminarIF = strSQLEliminarIF.substring(0, strSQLEliminarIF.length()-1);
                sSQLEliminarIF +=") ";
                log.info(sSQLEliminarIF);
                ps = con.queryPrecompilado(sSQLEliminarIF);
                ps.setInt(1, icDocumento);
                ps.executeUpdate();
                // INSERTAR LOS REGISTROS QUE NO se encuentren en bd
                if (!docto.getIcIFs().isEmpty()){
                    StringBuilder strSQLIF = new StringBuilder();
                    strSQLIF.append(" insert into GDOCREL_DOCUMENTO_IF (IC_IF, IC_DOCUMENTO)\n" + 
                        " select ?, ? from dual where not exists \n" + 
                        " (select *  from GDOCREL_DOCUMENTO_IF  where (IC_IF = ? and IC_DOCUMENTO = ?))");
                    log.info(strSQLIF.toString());
                    for (Integer icIF : ifs ){
                        ps = con.queryPrecompilado(strSQLIF.toString());
                        ps.setInt(1, icIF);
                        ps.setInt(2, icDocumento);
                        ps.setInt(3, icIF);
                        ps.setInt(4, icDocumento);                        
                        ps.executeUpdate();
                    }    
                }
            }
            else{
                StringBuilder strSQLEliminarIF = new StringBuilder();
                strSQLEliminarIF.append(" DELETE FORM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ?  ");
                ps = con.queryPrecompilado(strSQLEliminarIF.toString());
                ps.setInt(1, icDocumento);
                ps.executeUpdate();                
            }
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al guardar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return resultado;
    } // end guardarActualizarDocumento


    @Override
    public ArrayList<Comentario> getComentariosSol(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Comentario> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select TG_DESCRIPCION, DT_FECHA_REGISTRO, IC_USUARIO_REGISTRO from gdoc_comentario_solicitud WHERE IC_SOLICITUD  = ? ORDER BY DT_FECHA_REGISTRO DESC ");
            log.info(strSQL);
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info("Param: "+ icSolicitud);
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String descripcion =  rs.getString("TG_DESCRIPCION");
                Usuario usrRegistro = utilUsr.getUsuario(rs.getString("IC_USUARIO_REGISTRO"));
                String fecha = rs.getString("DT_FECHA_REGISTRO");
                Comentario comm = new Comentario(descripcion, fecha ,usrRegistro.getNombreCompleto()); 
                retorno.add(comm);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información de la Base de Datos", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }


    /**
     * Obtiene los datos del documento  por la IC_SOLICITUD asociada 
     * */
    @Override
    public Documento getDocumentoByIcSolicitud(Integer icSolicitud) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Documento retorno = new Documento();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT   \n" + 
                "      d.IC_DOCUMENTO,   \n" + 
                "      d.IC_ARCHIVO,  \n" + 
                "      d.IC_TIPO_DOCUMENTO,   \n" + 
                "      s.IC_ESTATUS,   \n" + 
                "      e.cd_nombre_estatus  \n" + 
                "      FROM GDOC_DOCUMENTO d, GDOCCAT_ESTATUS e, GDOC_SOLICITUD s WHERE \n" + 
                "      e.ic_estatus = s.ic_estatus and s.IC_SOLICITUD = d.IC_SOLICITUD and d.IC_SOLICITUD = ? "); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();
            while (rs.next()){
                retorno.setIcSolicitud(icSolicitud);
                retorno.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                retorno.setEstatus(rs.getInt("IC_ESTATUS"));
                retorno.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                retorno.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                retorno.setIcArchivo(rs.getInt("IC_ARCHIVO"));
            }
            String sqlIfs = " SELECT di.IC_IF, di.IC_ARCHIVO FROM  GDOC_DOCUMENTO d, GDOCREL_DOCUMENTO_IF di WHERE  d.ic_documento = di.ic_documento and d.ic_solicitud = ? ";
            ps = con.queryPrecompilado(sqlIfs); 
            ps.setInt(1, icSolicitud);
            rs = ps.executeQuery();    
            Map<Integer, Integer> mapFilesIF = new HashMap<>();
            ArrayList<Integer> listIF = new ArrayList<>();
            while (rs.next()){
                Integer ifseleccionado = rs.getInt("IC_IF");
                listIF.add(ifseleccionado);
                Integer icArchivo = rs.getInt("IC_ARCHIVO");
                if(icArchivo != null){
                    mapFilesIF.put(ifseleccionado, icArchivo);
                }
            }
            retorno.setIcIFs(listIF);
            retorno.setIcArchivoIF(mapFilesIF);
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;        
    }


    @Override
    public boolean asignarSolicitudEjecutvo(Integer icSolicitud, String usuario) {
        log.info("asignarSolicitudEjecutvo(E)");
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" UPDATE GDOC_SOLICITUD SET IC_USUARIO_EJECUTIVO = ?  WHERE IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setString(1, usuario);
            ps.setInt(2, icSolicitud);
            log.info(strSQL.toString());
            log.info("Parametros : "+usuario+", "+ icSolicitud);
            int actualizados = ps.executeUpdate();  
            log.info("Registros actualizados: "+ actualizados);
            exito = true; 
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        log.info("asignarSolicitudEjecutvo(S)");
        return exito;
    }

    /**
     * Regresa true cuando la solicitud esta asignada al usuario enviado */
    @Override
    public boolean isSolicitudAsignadaEjecutivo(Integer icSolicitud, String usuario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        boolean asignada = false;
        try {
            con.conexionDB();
            strSQL.append(" select COUNT(IC_SOLICITUD) total from GDOC_SOLICITUD WHERE  IC_USUARIO_EJECUTIVO = ? and IC_SOLICITUD = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setString(1, String.format("%1$-9s",usuario));
            ps.setInt(2, icSolicitud);
            rs = ps.executeQuery();
            rs.next();
            Integer total = rs.getInt("total");
            asignada = total > 0;
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return asignada;
    }


    /**
     * Regresa el IC_DOCUMENTO de la Solicitud correspondiente a su Folio (yyyy-folio)
     * */
    @Override
    public Integer getIcDocumentoFromFolioSolicitud(Integer anio, Integer folio) {
        Integer icDocumento = null;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT d.IC_DOCUMENTO FROM GDOC_SOLICITUD s, GDOC_DOCUMENTO d WHERE d.IC_SOLICITUD = s.IC_SOLICITUD \n" + 
            " AND EXTRACT(YEAR FROM s.DT_FECHA_CREACION) = ? AND  IC_FOLIO = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            log.info(anio+" "+ folio);
            ps.setInt(1, anio);
            ps.setInt(2, folio);
            rs = ps.executeQuery();  
            while (rs.next()){
                icDocumento = rs.getInt("IC_DOCUMENTO");            
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            throw new AppException("Error inesperado al realizar la consulta del IC_DOCUMENTO", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return icDocumento;
    }// end getIcDocumentoFromFolioSolicitud


    @Override
    public String getFolioDocSolicitudOrigen(Integer icDocumentoOrigen) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String folio = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO FROM GDOC_SOLICITUD s, GDOC_DOCUMENTO d WHERE \n" + 
            "  d.IC_SOLICITUD = s.IC_SOLICITUD AND d.IC_DOCUMENTO = ? ");
            log.info(strSQL);
            log.info("parametro: "+icDocumentoOrigen);
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icDocumentoOrigen);
            rs = ps.executeQuery();  
            while (rs.next()){
                folio = rs.getString("FOLIO");
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al realizar la consulta del Folio de Documento Origen", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return folio;
    }


    @Override
    public boolean setTipoMovimientoDocumento(Integer icMovimiento, Integer icDocumento) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        boolean exito = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" UPDATE GDOC_DOCUMENTO SET IC_MOVIMIENTO = ? WHERE  IC_DOCUMENTO = ? ");
            log.info(strSQL);
            log.info("parametros: "+icMovimiento+", "+icDocumento);
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icMovimiento);
            ps.setInt(2, icDocumento);
            ps.executeUpdate();
            exito = true;
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al realizar la consulta del Folio de Documento Origen", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return exito;
    }


    @Override
    public Documento getDocumentoByIcDocumento(Integer icDocumento) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Documento retorno = new Documento();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append("    SELECT   \n" + 
                "		d.IC_DOCUMENTO,   \n" + 
                "		d.IC_ARCHIVO,   \n" + 
                "		d.IC_TIPO_DOCUMENTO, TO_CHAR(d.DT_FECHA_REGISTRO,'DD/MM/YYYY') FECHA_REGISTRO,   \n" + 
                "		s.IC_ESTATUS, d.IC_VERSION,   \n" + 
                "		e.cd_nombre_estatus, d.IC_DOCUMENTO_ORIGEN, d.IC_MOVIMIENTO   \n" + 
                "		FROM GDOC_DOCUMENTO d, GDOCCAT_ESTATUS e, GDOC_SOLICITUD s WHERE \n" + 
                "    e.ic_estatus = d.ic_estatus and s.IC_SOLICITUD = d.IC_SOLICITUD and d.IC_DOCUMENTO = ? "); 
            ps = con.queryPrecompilado(strSQL.toString()); 
            //log.info(strSQL);
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();
            while (rs.next()){
                retorno.setIcSolicitud(icDocumento);
                retorno.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                retorno.setEstatus(rs.getInt("IC_ESTATUS"));
                retorno.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                retorno.setIcVersion(rs.getInt("IC_VERSION"));
                retorno.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                retorno.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                retorno.setIcDocumentoOrigen(rs.getInt("IC_DOCUMENTO_ORIGEN"));
                retorno.setIcMovimientoSeleccionado(rs.getInt("IC_MOVIMIENTO"));
                retorno.setFechaRegistroDoc(rs.getString("FECHA_REGISTRO"));
            }
            String sqlIfs = " SELECT di.IC_IF, di.IC_ARCHIVO FROM GDOC_DOCUMENTO d, GDOCREL_DOCUMENTO_IF di WHERE  d.ic_documento = di.ic_documento and d.ic_documento = ? ";
            ps = con.queryPrecompilado(sqlIfs); 
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();    
            Map<Integer, Integer> mapFilesIF = new HashMap<>();
            ArrayList<Integer> listIF = new ArrayList<>();
            while (rs.next()){
                Integer ifseleccionado = rs.getInt("IC_IF");
                listIF.add(ifseleccionado);
                Integer icArchivo = rs.getInt("IC_ARCHIVO");
                if(icArchivo != null){
                    mapFilesIF.put(ifseleccionado, icArchivo);
                }
            }
            retorno.setIcIFs(listIF);
            retorno.setIcArchivoIF(mapFilesIF);
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;        
    }

    @Override
    public ArrayList<DocValidacion> getDocumentosValidacion(Map<String, String> params) {
        ArrayList<DocValidacion> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        
        Integer icSolicitud = params.get(ConstantesFormalizacion.FILTRO_FOLIO) != null ? Integer.parseInt(params.get(ConstantesFormalizacion.FILTRO_FOLIO)): null;
        Integer tipoDoc = params.get(ConstantesFormalizacion.FILTRO_TIPO_DOCUMENTO) != null ? Integer.parseInt(params.get(ConstantesFormalizacion.FILTRO_TIPO_DOCUMENTO)) : null;
        String nombreProducto = params.get(ConstantesFormalizacion.FILTRO_NOMBR_PRODUCTO);
        Integer estatus = params.get(ConstantesFormalizacion.FILTRO_ESTATUS) != null ? Integer.parseInt(params.get(ConstantesFormalizacion.FILTRO_ESTATUS)) : null;
        String usuarioEjecutivo = params.get(ConstantesFormalizacion.FILTRO_EJECUTIVO);
        Integer icIF = params.get(ConstantesFormalizacion.FILTRO_IF) != null ? Integer.parseInt(params.get(ConstantesFormalizacion.FILTRO_IF)) : null;
        String fechaLiberacion1 = params.get(ConstantesFormalizacion.FILTRO_FECHA_LIBERACION_INICIO);
        String fechaLiberacion2 = params.get(ConstantesFormalizacion.FILTRO_FECHA_LIBERACION_FIN);
        String filtros ="";
        int indiceParametro = 0;
        if (icSolicitud != null){
            filtros +=  " AND s.ic_Solicitud  = ? ";
            indiceParametro = indiceParametro +2;
        }
        if (tipoDoc != null){
            filtros += "  AND d.ic_tipo_documento = ? ";
            indiceParametro++;
        }
        if (nombreProducto != null){
            filtros += " AND d.TG_NOMBRE  = ? ";
            indiceParametro++;
        }
        if (estatus != null){
            filtros += " AND f.ic_estatus = ? ";
            indiceParametro++;
        }
        if (usuarioEjecutivo != null){
            filtros+= " AND  and s.ic_solicitud in \n" + 
            " (select ic_solicitud from gdoc_bitacora_doc where ic_estatus = "+ ESTATUS_EN_VALIDACION +" and ic_usuario_registro = ?) ";
            indiceParametro++;
        }
        if (icIF != null){
            filtros += " AND  f.ic_if = ? ";
            indiceParametro++;
        }
        if (fechaLiberacion1 != null){
            filtros += " AND (select MAX(bb.DT_FECHA_MOVIMIENTO) from gdoc_bitacora_doc bb \n" + 
            "  where bb.ic_estatus = 130 and bb.ic_solicitud = s.ic_solicitud \n" + 
            "group by bb.ic_solicitud, bb.ic_if) \n" + 
            "BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') ";
            indiceParametro++;
        }
            
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT s.ic_solicitud, d.ic_documento, \n" + 
                    "  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,\n" + 
                    "  d.ic_version,\n" + 
                    "  cf.cg_nombre_Comercial AS nombreIF, cf.ic_if as icIF, \n" + 
                    "  Cd.Cd_Nombre_Documento,\n" + 
                    "  NVL(D.Tg_Nombre, S.tg_nombre) AS nombreProducto,\n" + 
                    "  E.Cd_Nombre_Estatus, e.ic_estatus, \n" + 
                    "  D.IC_ARCHIVO, \n" + 
                    " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM gdoc_bitacora_doc b where b.ic_estatus = 130 and " +
                    " b.ic_solicitud = s.ic_solicitud) as FECHA_LIBERACION, "+
                    " (select Ic_Usuario_Ejecutivo from gdoc_parametro_if pi where pi.ic_if= cf.ic_if) as icUsuarioAtencion, "+
                    "  (select  case when In_Firmas_Requerido = 0 THEN 1 ELSE In_Firmas_Requerido END  from gdoc_parametro_if pi where pi.ic_if= cf.ic_if) as numeroFirmasRequeridas "+
                    "FROM gdoc_documento d,\n" + 
                    "  gdoc_solicitud s,\n" + 
                    "  GDOCREL_DOCUMENTO_IF f,\n" + 
                    "  COMCAT_IF cf,\n" + 
                    "  gdoccat_documento cd,\n" + 
                    "  gdoccat_estatus e \n" + 
                    "WHERE d.ic_Solicitud    = s.ic_solicitud\n" + 
                    "AND d.ic_documento      = f.ic_documento\n" + 
                    "AND F.IC_IF             = cf.ic_if\n" + 
                    "AND d.ic_tipo_documento = cd.ic_tipo_documento\n" + 
                    "AND s.ic_estatus        = e.ic_estatus\n" + 
                    "AND s.ic_estatus        > 120\n" + 
                    "AND s.ic_estatus        < 140 AND d.ic_tipo_Documento = 1001 "); 
            strSQL.append(filtros);
            log.info(strSQL.toString());
            ps = con.queryPrecompilado(strSQL.toString()); 
            int indiceSiguiente = 1;
            if (icSolicitud != null){
                ps.setInt(1, icSolicitud);
                indiceSiguiente++;
            }
            if (tipoDoc != null){
                ps.setInt(indiceSiguiente, tipoDoc);
                indiceSiguiente ++;
            }
            if (nombreProducto != null){
                ps.setString(indiceSiguiente , nombreProducto);
                indiceSiguiente ++;
            }
            if (estatus != null){
                ps.setInt(indiceSiguiente , estatus);
                indiceSiguiente ++;
            }
            if (usuarioEjecutivo != null){
                ps.setString(indiceSiguiente, usuarioEjecutivo);
                indiceSiguiente ++;
            }
            if (icIF != null){
                ps.setInt(indiceSiguiente, icIF);
                indiceSiguiente ++;
            }
            if (fechaLiberacion1 != null){
                ps.setString(indiceSiguiente , fechaLiberacion1);
                ps.setString(indiceSiguiente+1 , fechaLiberacion2);
            }
            
            log.info(strSQL.toString());
            rs = ps.executeQuery();
            while (rs.next()){
                DocValidacion doc = new DocValidacion();
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                doc.setFolioSolicitud(rs.getString("FOLIO"));
                doc.setVersion(rs.getString("IC_VERSION"));
                doc.setNombreIF(rs.getString("nombreIF"));
                doc.setTipoDocumento(rs.getString("cd_nombre_documento"));
                doc.setNombreProducto(rs.getString("nombreProducto"));
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                doc.setIcEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                doc.setNombreEstatus(rs.getString("cd_nombre_estatus"));
                doc.setFechaLiberacion(rs.getString("FECHA_LIBERACION"));
                doc.setFirmasRequeridas(rs.getInt("numeroFirmasRequeridas"));
                doc.setIcIF(rs.getInt("icIF"));
                String icUsuarioAtencion = rs.getString("icUsuarioAtencion");
                if (icUsuarioAtencion != null){
                    UtilUsr utilUsr = new UtilUsr();
                    Usuario usrAtencion = utilUsr.getUsuario(rs.getString("icUsuarioAtencion"));
                    doc.setNombreEjecutivoAtencion(usrAtencion.getNombreCompleto());
                }else{
                    doc.setNombreEjecutivoAtencion("Sin parametrizar");
                    }
                retorno.add(doc);
                String sqlIfs = " select ic_usuario_ejecutivo from Gdoc_Parametro_If where ic_if = ? ";
            }       
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información de la Solicitud", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }         
        }
        return retorno;
    }
    
    @Override
    public ArrayList<ElementoCatalogo> getCatProducto(Integer icEstatus, Integer icIF){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT distinct d.ic_documento, d.Tg_Nombre FROM gdoc_documento d,  gdoc_solicitud s, gdocrel_documento_if r \n" + 
            "WHERE d.ic_Solicitud    = s.ic_solicitud AND r.ic_documento = d.ic_documento and s.ic_estatus > ?   ");
            if (icIF != null){
                    strSQL.append(" AND r.ic_if  = ? ");
            }
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icEstatus);
            if (icIF != null){
                ps.setInt(2, icIF);
            }
            rs = ps.executeQuery();
            ElementoCatalogo emptyCat = new ElementoCatalogo("", "Todos los Productos/Empresas");
            retorno.add(emptyCat);            
            while (rs.next()){
                String val = rs.getString("ic_documento");
                String descripcion = rs.getString("Tg_Nombre");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;     
    }
    
    /** Regresa catalogo de Ejecutivos de Atencion de IF parametrizados
     * 
     * */
    @Override
    public ArrayList<ElementoCatalogo> getCatEjecutivosAtencion(){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select DISTINCT(IC_USUARIO_EJECUTIVO) AS ejecutivo from gdoc_parametro_IF  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            ElementoCatalogo emptyCat = new ElementoCatalogo("", "Todos los Ejecutivos");
            retorno.add(emptyCat);
            while (rs.next()){
                Usuario usuario = utilUsr.getUsuario(rs.getString("ejecutivo"));
                String val = rs.getString("ejecutivo");
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;     
    }
    
    /** Regresa un Catalogo de los Intermediarios Financieros que tienen documentos en  estatus 
     * Validacion Juridico (130) o posterior 
     * */
    public ArrayList<ElementoCatalogo> getCatIFformalizacion(){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select ic_if, CG_nombre_comercial from comcat_if where ic_if in  \n" + 
            " (select DISTINCT(IC_IF) as IC_IF from gdocrel_documento_if where ic_estatus >= 130) ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            ElementoCatalogo emptyCat = new ElementoCatalogo("", "Todos los Intermediarios");
            retorno.add(emptyCat);
            while (rs.next()){
                ElementoCatalogo elcat = new ElementoCatalogo(rs.getInt("ic_if")+"", rs.getString("CG_nombre_comercial"));
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;             
    }
    
    
    /** Regresa un Catalogo de los Tipos de Movimientos para una Solicitud
     * */
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento(){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_MOVIMIENTO, CD_NOMBRE_MOVIMIENTO FROM GDOCCAT_MOVIMIENTO ORDER BY IC_MOVIMIENTO ASC ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            ElementoCatalogo emptyCat = new ElementoCatalogo("", "Todos los movimientos");
            retorno.add(emptyCat);
            while (rs.next()){
                ElementoCatalogo elcat = new ElementoCatalogo(rs.getInt("IC_MOVIMIENTO")+"", rs.getString("CD_NOMBRE_MOVIMIENTO"));
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar catalogo de Movimentos ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;             
    }
    
    
    public boolean isRetorno(Integer icSolicitud, Integer estatusActual){
        boolean valor = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select COUNT(IC_COMENTARIO_SOLICITUD) TOTAL from GDOC_COMENTARIO_SOLICITUD WHERE IC_SOLICITUD = ? AND IC_ESTATUS > ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icSolicitud);
            ps.setInt(2, estatusActual);
            rs = ps.executeQuery();
            int totalComentarios = 0;
            while (rs.next()){
                totalComentarios = rs.getInt("TOTAL");
            }
            valor = totalComentarios > 0;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return valor;   
    }
    
    
    private String getUsuarioAsignado(SolicitudDoc sol){
        StringBuilder sql = new StringBuilder();
        AccesoDB con = new AccesoDB();
        String usuario = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        try {
            con.conexionDB();
            sql.append(" SELECT d.IC_USUARIO FROM Gdoc_Usuario_Documento d, Gdoc_Usuario_Portafolio p, GDOC_USUARIO_IF f " + 
                " WHERE d.IC_USUARIo = p.ic_usuario AND d.IC_USUARIo = f.ic_usuario  " + 
                " AND p.IS_VERSION_ACTUAL=1 AND f.IS_VERSION_ACTUAL=1 and d.IS_VERSION_ACTUAL=1 " + 
                " AND d.IC_TIPO_DOCUMENTO = ? AND p.IC_PORTAFOLIO = ? AND f.IC_IF = ? ");
            ps = con.queryPrecompilado(sql.toString());
            ps.setInt(1, sol.getIcTipoDocumento());
            ps.setInt(2, sol.getPortafolio());
            ps.setInt(3, sol.getSIntermediarios().get(0));
            log.info("Buscar usuario Asignado por parametrización:\n"+sql.toString());
            log.info("params: "+sol.getIcTipoDocumento()+", "+sol.getPortafolio()+", "+ sol.getSIntermediarios().get(0));
            rs = ps.executeQuery();
            while(rs.next()){
                usuario = rs.getString("IC_USUARIO");
                if ( rs.getRow() > 1){
                    log.error("EXISTEN USUARIOS CON LA MISMA PARAMETRIZACIÓN, SOLO SE CONSIDERA EL PRIMERO");
                }
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getStackTrace());
            throw new AppException("Error al consultar la Base de Datos", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return usuario;
    }
    
    @Override
    public Integer getIcArchivoIFData(Integer icSolicitud, Integer icIF){
        StringBuilder sql = new StringBuilder();
        AccesoDB con = new AccesoDB();
        Integer icIFdocumento = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        try {
            con.conexionDB();
            sql.append(" select di.ic_archivo from gdoc_solicitud s, gdoc_documento d, gdocrel_documento_if di where \n" + 
            " s.ic_solicitud = d.ic_solicitud and d.ic_documento = di.ic_documento and s.ic_solicitud = ? and di.ic_if = ? ");
            ps = con.queryPrecompilado(sql.toString());
            ps.setInt(1, icSolicitud);
            ps.setInt(2, icIF);
            log.info(sql.toString());
            log.info("params: "+icSolicitud+", "+icIF);
            rs = ps.executeQuery();
            while(rs.next()){
                icIFdocumento = rs.getInt("IC_ARCHIVO");
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getStackTrace());
            throw new AppException("Error al consultar la Base de Datos", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }      
        return icIFdocumento;
    }
    
    
    @Override
    public boolean deleteIcArchivoIF(Integer icSolicitud, Integer icIF, Integer icArchivo){
        log.info("deleteIcArchivoIF(E)");
        StringBuilder sql = new StringBuilder();
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        Integer icIFdocumento = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        try {
            con.conexionDB();
            String sqlOnBase = "  select IC_FOLIO_ONBASE from GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ";
            ps = con.queryPrecompilado(sqlOnBase);
            ps.setInt(1, icArchivo);
            rs = ps.executeQuery();
            Integer icOnBase = null;
            while(rs.next()){
                icOnBase = rs.getInt("IC_FOLIO_ONBASE");
            }
            sql.append(" update  gdocrel_documento_if set ic_archivo = null where ic_if = ? and ic_archivo = ? and ic_documento in (\n" + 
            " Select d.ic_documento FROM  gdoc_solicitud s, gdoc_documento d WHERE\n" + 
            "    d.ic_solicitud = s.ic_solicitud  and s.ic_solicitud = ? ) ");
            ps = con.queryPrecompilado(sql.toString());
            ps.setInt(1, icIF);
            ps.setInt(2, icArchivo);
            ps.setInt(3, icSolicitud);
            log.info(sql.toString());
            log.info("params: "+icIF+", "+icArchivo+ ", "+icSolicitud);
            ps.executeUpdate();
            
            String sqlDelete = " DELETE FROM GDOCREL_SOLICITUD_ARCHIVO WHERE IC_ARCHIVO = ?  ";
            ps = con.queryPrecompilado(sqlDelete); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
            exito = true;
            Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            log.info("eliminando de OnBase: "+ icArchivo);
            exito = files2OnBaseBean.eliminaArchivo(icOnBase);        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error al consultar la Base de Datos", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }      
        log.info("deleteIcArchivoIF(S)");
        return exito;
    }
    
}// end SolicitudesBean
