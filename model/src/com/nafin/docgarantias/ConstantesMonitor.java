package com.nafin.docgarantias;

public class ConstantesMonitor {
    
    // Constantes para identificar los catalogos del Monitor de Lineas 
    public static final int PORTAFOLIO      = 0;
    public static final int PRODUCTO        = 1;
    public static final int INTERMEDIARIO   = 2;
    public static final int FECHA_VIGENCIA  = 3;
    public static final int ESTATUS_LINEA   = 4;
    public static final int BASE_OPERACION  = 5;
    

    
    public static final String ARCHIVO_PDF      = "PDF";
    public static final String ARCHIVO_XLS      = "XLS";

    public static final Integer ESTATUS_VERDE    = 1;
    public static final Integer ESTATUS_AMARILLO = 2;
    public static final Integer ESTATUS_ROJO     = 3;
    
    public ConstantesMonitor() {
        super();
    }
}
