package com.nafin.docgarantias;

import com.nafin.docgarantias.almacenador.AlmacenaLocal;
import com.nafin.docgarantias.almacenador.AlmacenaOnBase;
import com.nafin.docgarantias.almacenador.Almacenador;
import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "Files2OnBase", mappedName = "Files2OnBase")
@TransactionManagement(TransactionManagementType.BEAN)
public class Files2OnBaseBean  implements Files2OnBase {
    
    @Resource
    SessionContext sessionContext;
    
    private static final Log log = ServiceLocator.getInstance().getLog(Files2OnBaseBean.class);
    private Almacenador almacenador;
    
    public Files2OnBaseBean() {
        super();
        try {
            /** Si existe la IP del webService se implementa el amacenamiento en OnBase, 
             *  en caso contrario el almacenamiento local */
            almacenador = existeIPWebService() ? new AlmacenaOnBase() : new AlmacenaLocal();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }
    
    @Override
    public ArchivoDocumento guardaArchivo(ArchivoDocumento archivo, String path) {
        return this.guardaArchivoDocIF(archivo, path, null, null, null);
    }
    
    @Override
    public ArchivoDocumento guardaArchivoDoc(ArchivoDocumento archivo, String path, Integer icDocumento){
        return this.guardaArchivoDocIF(archivo, path, icDocumento, null, null);
    }
    
    @Override
    public ArchivoDocumento guardaArchivoSol(ArchivoDocumento archivo, String path, Integer icSolicitud){
        return this.guardaArchivoDocIF(archivo, path, null, null, icSolicitud);
    }
    
    public ArchivoDocumento guardaArchivoIF(ArchivoDocumento archivo, String path, Integer icDocumento,Integer icIF){
        return this.guardaArchivoDocIF(archivo, path, icDocumento, icIF, null);
    }
    
    /**
     * Guarda en OnBASE el archivo enviado
     * Guarda en GDOC_ARCHIVO con el nombre del archivo
     * Guarda en GDOC_DOCUMENTO si sele pasa el icDocumentro    
     * Guarda en GDOCREL_DOCUMENTO_IF si sele pasa el icDocumento y el icIF (Carga de Fichas IF)
     * Regresa el ArchivoDocumento con el itemNum generado por OnBASE
     * Regresa el IC_ARCHIVO con el que se almaceno en GDOC_ARCHIVO
     * */
    private ArchivoDocumento guardaArchivoDocIF(ArchivoDocumento archivo, String path, Integer icDocumento, Integer icIF, Integer icSolicitud) {
        boolean exito = false;
        AccesoDB con = new AccesoDB();    
        Date date = Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("yymmddhhmmss");  
        String nuevoNombre = (archivo.getNombreArchivo().length()>26) ? archivo.getNombreArchivo().substring(0, 26) : archivo.getNombreArchivo() ;
        String identificador = dateFormat.format(date)+ "_" + nuevoNombre;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            Integer itemId = almacenador.almacenarArchivo(identificador, path, archivo.getNombreArchivo());
            if (itemId == null){
                throw new AppException("Error al cargar la información");
            }
            else{
                archivo.setItemNum(itemId);
                con.conexionDB();
                Integer icArchivo = 0;
                String sqlQueryNextVal = " SELECT SEQ_GDOC_ARCHIVO.nextval FROM dual  ";
                rs = con.queryDB(sqlQueryNextVal);
                if (rs.next()){
                    icArchivo = rs.getInt(1);
                }
                StringBuilder strSQL = new StringBuilder();        
                strSQL.append(" INSERT INTO GDOC_ARCHIVO (IC_ARCHIVO, IC_FOLIO_ONBASE, IC_TIPO_ARCHIVO, CD_NOMBRE_ARCHIVO, DT_FECHA_CREACION)\n" + 
                    "VALUES(?, ?, ?, ?, SYSDATE) ");
                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, icArchivo);
                ps.setInt(2, itemId);
                ps.setInt(3, archivo.getIcTipoArchivo());
                ps.setString(4, archivo.getNombreArchivo());
                ps.executeUpdate();
                if (icDocumento != null){
                    if (icIF == null){
                        StringBuilder strSQLDoc = new StringBuilder();        
                        strSQLDoc.append(" UPDATE GDOC_DOCUMENTO SET IC_ARCHIVO = ? WHERE IC_DOCUMENTO = ? ");
                        ps = con.queryPrecompilado(strSQLDoc.toString());
                        ps.setInt(1, icArchivo);
                        ps.setInt(2, icDocumento);
                        ps.executeUpdate();                    
                    }
                    else{
                        StringBuilder strSQLDocIF = new StringBuilder();        
                        strSQLDocIF.append(" merge INTO GDOCREL_DOCUMENTO_IF tgt " + 
                            " USING (SELECT Count(*) AS total   \n" + 
                            "       FROM   gdocrel_documento_if   \n" + 
                            "       WHERE  ic_documento = :IC_DOCUMENTO AND ic_if = :IC_IF ) src   \n" + 
                            " ON (src.total >0)   WHEN matched THEN   \n" + 
                            "  UPDATE SET tgt.ic_archivo= :IC_ARCHIVO   \n" + 
                            "             WHERE ic_documento = :IC_DOCUMENTO AND ic_if=:IC_IF   \n" + 
                            " WHEN NOT matched THEN   \n" + 
                            "  INSERT (IC_DOCUMENTO, IC_IF, IC_ARCHIVO) \n" + 
                            "  VALUES (:IC_DOCUMENTO, :IC_IF, :IC_ARCHIVO) ");
                        log.info(strSQLDocIF);
                        log.info(icDocumento+" "+icIF+" "+icArchivo);
                        NamedParameterStatement npStatement = new NamedParameterStatement(con.getConnection(), strSQLDocIF.toString(), QUERYTYPE.QUERY);
                        npStatement.setInt("IC_DOCUMENTO",icDocumento);
                        npStatement.setInt("IC_IF",icIF);
                        npStatement.setInt("IC_ARCHIVO",icArchivo);
                        npStatement.execute();
                    }
                }else if (icSolicitud != null){
                    StringBuilder strSQLSol = new StringBuilder();        
                    strSQLSol.append(" INSERT INTO GDOCREL_SOLICITUD_ARCHIVO (IC_ARCHIVO, IC_SOLICITUD) VALUES");
                    ps = con.queryPrecompilado(strSQLSol.toString());
                    ps.setInt(1, icArchivo);
                    ps.setInt(2, icSolicitud);
                    ps.executeUpdate();                    
                }
                
                archivo.setIcArchivo(icArchivo);
                exito = true;
            }
        } 
        catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarResultSet(rs);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
       }          
        return archivo;
    }
    

    /** 
     * Elimina el archivo en OnBase 
     * */
    @Override
    public boolean eliminaArchivo(Integer icFolioOnBase) {
        boolean exito = false;
        try {
            exito = almacenador.eliminarArchivo(icFolioOnBase+"");
        } 
        catch (Exception e) {
            log.info(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }       
        return exito;
    }    
    

    /** 
     * Elimina el archivo en OnBase y en las tablas GDOCREL_SOLICITUD_ARCHIVO y GDOC_ARCHIVO
     * */
    @Override
    public ArchivoDocumento eliminaArchivoSolicitud(Integer icArchivo) {
        ArchivoDocumento archivo = null;
        boolean exito = true;
        AccesoDB con = new AccesoDB();  
        PreparedStatement ps = null;
        try {
            archivo = this.archivoByIcArchivo(icArchivo);
            almacenador.eliminarArchivo(archivo.getItemNum()+"");
            con.conexionDB();
            StringBuilder strSQL = new StringBuilder();        
            strSQL.append(" DELETE FROM GDOCREL_SOLICITUD_ARCHIVO WHERE IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
            
            StringBuilder strSQLArchivo = new StringBuilder();        
            strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQLArchivo.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
        } 
        catch (Exception e) {
            exito = false;
            log.error(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
        }        
        return archivo;
    }



    /** 
     * Elimina el archivo en OnBase y en las tabla GDOC_ARCHIVO
     * */
    @Override
    public boolean eliminaArchivoEnTabla(Integer icArchivo) {
        ArchivoDocumento archivo = null;
        boolean exito = false;
        AccesoDB con = new AccesoDB();  
        PreparedStatement ps = null;
        try {
            archivo = archivoByIcArchivo(icArchivo);
            if (almacenador.eliminarArchivo(archivo.getItemNum()+"")){
                log.info("Archivo Eliminado del Repositorio, itemNum: "+ archivo.getItemNum()+" icArchivo: "+icArchivo);
                con.conexionDB();            
                StringBuilder strSQLArchivo = new StringBuilder();        
                strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ?  ");
                ps = con.queryPrecompilado(strSQLArchivo.toString()); 
                ps.setInt(1, icArchivo);
                ps.executeUpdate();
                log.info("Archivo Eliminado BD, itemNum: "+ archivo.getItemNum()+" icArchivo: "+icArchivo);
                exito= true;
            }
        } 
        catch (Exception e) {
            exito = false;
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
        }        
        return exito;
    }


    /** 
     * Elimina los archivos en OnBase y en las tablas GDOCREL_SOLICITUD_ARCHIVO y GDOC_ARCHIVO
     * */
    @Override
    public boolean eliminaArchivosSolicitud(ArrayList<Integer> icArchivos) {
        boolean exito = false;
        try{
            for(Integer icArchivo : icArchivos){
                this.eliminaArchivo(icArchivo);
            }      
            exito = true;
        }
        catch(Exception e){
            log.error(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        return exito;
    }


    @Override
    public ArchivoDocumento getArchivo(Integer icArhivo,  String path) {
        return almacenador.obtenerArchivo(icArhivo, path);
    }
    
    
    private boolean existeIPWebService() throws Exception {
        boolean existe = false;
        ResultSet rs = null;
        PreparedStatement ps = null;
        StringBuilder sqlQuery = new StringBuilder();
        AccesoDB con = new AccesoDB();
        String wsDireccion = "";
        try {
            con.conexionDB();
            sqlQuery.append( " select T.SUP_DIR_WS from com_param_gral t where t.ic_param_gral = 1");
            ps = con.queryPrecompilado(sqlQuery.toString()); 
            rs = ps.executeQuery();
            while (rs.next()) {
                wsDireccion = rs.getString("SUP_DIR_WS");
            } 
            existe = wsDireccion != null;
        }  catch (Exception ex) {
            log.error(ex.getStackTrace());
            throw new AppException("Error inesperado al obtener la información", ex);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return existe;
    }
    
    
    private ArchivoDocumento archivoByIcArchivo(Integer icArhivo){
            ArchivoDocumento archivo = new ArchivoDocumento();
            AccesoDB con = new AccesoDB();
            PreparedStatement ps = null;
            ResultSet rs = null;
            StringBuilder strSQL = new StringBuilder();
            try {
                con.conexionDB();
                strSQL.append(" SELECT IC_ARCHIVO, IC_FOLIO_ONBASE, IC_TIPO_ARCHIVO, CD_NOMBRE_ARCHIVO FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ");
                log.info(strSQL.toString());
                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, icArhivo);
                rs = ps.executeQuery();
                while (rs.next()) {
                    archivo.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                    archivo.setItemNum(rs.getInt("IC_FOLIO_ONBASE"));
                    archivo.setIcTipoArchivo(rs.getInt("IC_TIPO_ARCHIVO"));
                    archivo.setNombreArchivo(rs.getString("CD_NOMBRE_ARCHIVO"));
                }
                log.info("Archivo: "+archivo.getItemNum()+", "+archivo.getNombreArchivo());
            } catch (SQLException | NamingException e) {
                log.error(e.getStackTrace());
                throw new AppException("Error inesperado al cargar la información", e);
            } finally {
                AccesoDB.cerrarResultSet(rs);
                AccesoDB.cerrarStatement(ps);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return archivo;
        }

    @Override
    public boolean eliminaArchivoRelacionIFDoc(Integer icArchivo, Integer icIF, Integer icDocumento) {
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        boolean exitoEliminacionOnBase;
        try {
            ArchivoDocumento archivo = this.archivoByIcArchivo(icArchivo);
            exitoEliminacionOnBase = this.eliminaArchivo(archivo.getItemNum());
            if(exitoEliminacionOnBase){
                String sqlEliminarRelacion = " DELETE FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ? AND IC_IF = ? AND IC_ARCHIVO = ? ";
                ps = con.queryPrecompilado(sqlEliminarRelacion); 
                ps.setInt(1, icArchivo);
                ps.executeUpdate();

                StringBuilder strSQLArchivo = new StringBuilder();
                strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ");
                ps = con.queryPrecompilado(strSQLArchivo.toString());
                ps.setInt(1, icArchivo);
                ps.executeUpdate();
                exito = true;
            }
        } 
        catch (Exception e) {
            log.info(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }       
        return exito;
    }

    /**
     * Eliminar el archivo de OnBase y de las tablas GDOC_ARCHIVO y GDOC_DOCUMENTO
     * 
     * */
    @Override
    public boolean eliminaArchivoDocumento(Integer icArchivo) {
        ArchivoDocumento archivo = null;
        boolean exito = true;
        AccesoDB con = new AccesoDB();  
        PreparedStatement ps = null;
        try {
            archivo = this.archivoByIcArchivo(icArchivo);
            almacenador.eliminarArchivo(archivo.getItemNum()+"");
            con.conexionDB();
            StringBuilder strSQL = new StringBuilder();        
            strSQL.append(" UPDATE GDOC_DOCUMENTO SET IC_ARCHIVO = NULL WHERE IC_ARCHIVO = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
            
            StringBuilder strSQLArchivo = new StringBuilder();        
            strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQLArchivo.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
        } 
        catch (Exception e) {
            exito = false;
            log.error(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
        }        
        return exito;
    }
    
    public ArchivoDocumento eliminaArchivoConvocatoria(Integer icArchivo) {
        ArchivoDocumento archivo = null;
        boolean exito = true;
        AccesoDB con = new AccesoDB();  
        PreparedStatement ps = null;
        try {
            archivo = this.archivoByIcArchivo(icArchivo);
            almacenador.eliminarArchivo(archivo.getItemNum()+"");
            con.conexionDB();
            StringBuilder strSQL = new StringBuilder();        
            strSQL.append(" UPDATE GDOC_CONVOCATORIA C\n" + 
            "   SET C.IC_ARCHIVO = NULL\n" + 
            " WHERE C.IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
            
            StringBuilder strSQLArchivo = new StringBuilder();        
            strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQLArchivo.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
        } 
        catch (Exception e) {
            exito = false;
            log.error(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
        }        
        return archivo;
    }
        
    public ArchivoDocumento eliminaArchivoSConv(Integer icConvocatoria) {
        ArchivoDocumento archivo = null;
        boolean exito = true;
        AccesoDB con = new AccesoDB();  
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        ArrayList<Integer> archivos = new ArrayList<Integer>();
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT NVL(C.IC_ARCHIVO, 0) ARCHIVOCONV\n" + 
            "  FROM GDOC_CONVOCATORIA C\n" + 
            " WHERE C.IC_CONVOCATORIA = ? ");
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            ps.setInt(1, icConvocatoria);
            
            rs = ps.executeQuery();
            Integer archivoC = 0;
            while (rs.next()){
                archivoC = 0;                
                archivoC = rs.getInt("ARCHIVOCONV");
                if(archivoC != 0){
                    archivos.add(archivoC);
                }
            }
            
            strSQL.setLength(0);
            strSQL = new StringBuilder();
            strSQL.append(
            " SELECT DISTINCT NVL(P.IC_ARCHIVO, 0) ARCHIVOPOST,\n" + 
            "    NVL(P.IC_ARCHIVO_CALIF, 0) ARCHIVOCALIF\n" + 
            " FROM GDOC_POSTURA P\n" + 
            " WHERE P.IC_CONVOCATORIA = ? ");            
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            ps.setInt(1, icConvocatoria);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                archivoC = 0;                
                archivoC = rs.getInt("ARCHIVOPOST");
                if(archivoC != 0){
                    archivos.add(archivoC);
                }
                archivoC = rs.getInt("ARCHIVOCALIF");
                if(archivoC != 0){
                    archivos.add(archivoC);
                }
            }
            
            strSQL.setLength(0);
            strSQL = new StringBuilder();
            strSQL.append(
            " SELECT DISTINCT NVL(R.IC_ARCHIVO, 0) ARCHIVORES\n" + 
            "  FROM GDOC_RESULTADO R\n" + 
            " WHERE R.IC_CONVOCATORIA = ? ");            
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            ps.setInt(1, icConvocatoria);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                archivoC = 0;                
                archivoC = rs.getInt("ARCHIVORES");
                if(archivoC != 0){
                    archivos.add(archivoC);
                }
            }
            for(int i = 0; i<archivos.size();i++){
            Integer icArchivo = archivos.get(i);
            archivo = this.archivoByIcArchivo(icArchivo);
            if(archivo!=null&&archivo.getItemNum()!=null){
                    almacenador.eliminarArchivo(archivo.getItemNum()+"");
            }
            StringBuilder strSQLArchivo = new StringBuilder();        
            strSQLArchivo.append(" DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ?  ");
            ps = con.queryPrecompilado(strSQLArchivo.toString()); 
            ps.setInt(1, icArchivo);
            ps.executeUpdate();
            }
        } 
        catch (Exception e) {
            exito = false;
            log.error(e.getStackTrace());
            throw new AppException("Error inesperado al Eliminar la información", e);
        }
        finally {
           con.terminaTransaccion(exito);
           AccesoDB.cerrarResultSet(rs);
           AccesoDB.cerrarStatement(ps);
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }             
        }        
        return archivo;
    }
    
    
}
