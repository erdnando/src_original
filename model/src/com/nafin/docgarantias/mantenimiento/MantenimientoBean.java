package com.nafin.docgarantias.mantenimiento;

import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "Mantenimiento", mappedName = "Mantenimiento")
@TransactionManagement(TransactionManagementType.BEAN)
public class MantenimientoBean implements Mantenimiento {
    
    private static final Log log = ServiceLocator.getInstance().getLog(MantenimientoBean.class);
    
    public MantenimientoBean() {
        super();
    }

    @Override
    public ArrayList<MantenimientoDoc> getDocsMantenimiento(Map<String, String> params) {
        log.info("Elementos enviados para FILTRAR: "+params.size());
        for (Map.Entry<String, String> entry : params.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        ArrayList<MantenimientoDoc> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFiltros = "";    
        if (params.get("fechaFormalizacion1") != null ) sqlFiltros += " AND TO_DATE(t.fechaFormalizacion, 'DD/MM/YYYY')  BETWEEN TO_DATE(:fechaFormalizacion1, 'DD/MM/YYYY') AND TO_DATE(:fechaFormalizacion2, 'DD/MM/YYYY') ";

        if (params.get("fechaVersion1") != null ) sqlFiltros += " AND TO_DATE(t.fecha_Anterior, 'DD/MM/YYYY') BETWEEN TO_DATE(:fechaVersion1, 'DD/MM/YYYY') AND TO_DATE(:fechaVersion2, 'DD/MM/YYYY') ";
        if (params.get("folio") != null ) sqlFiltros += " AND  t.ic_solicitud =  :folio ";
        if (params.get("intermediario") != null ) sqlFiltros += " AND t.ic_if = :intermediario ";
        if (params.get("tipoDocumento") != null ) sqlFiltros += " AND  t.ic_tipo_documento = :tipoDocumento ";
        if (params.get("portafolio") != null ) sqlFiltros += " AND  t.ic_portafolio = :portafolio ";
        if (params.get("producto") != null ) sqlFiltros += " AND t.ic_documento = :producto ";
        
        try {
            con.conexionDB();
            String sql;
            sql = " select * from (\n" + 
            " select dif.ic_estatus, dif.ic_if, dif.ic_documento, s.ic_portafolio, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,  \n" + 
            "                     s.ic_solicitud, d.ic_tipo_documento, CONCAT('v',d.ic_version) as version, NVL(d.tg_nombre,s.tg_nombre) as nombreProducto,  \n" + 
            "                    cd.cd_nombre_documento, p.cd_nombre_portafolio, d.ic_archivo, cif.cg_razon_social,   \n" + 
            "               (select TO_CHAR(max(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') from  gdoc_bitacora_doc where ic_documento = dif.ic_documento and ic_estatus = dif.ic_estatus and ic_if = dif.ic_if) fechaFormalizacion, \n" + 
            "               DECODE(d.ic_version, 1, null,  \n" + 
            "                            (select TO_CHAR(max(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') from  gdoc_bitacora_doc where ic_documento = d.ic_documento_origen and ic_if =  dif.ic_if) \n" + 
            "                            ) as fecha_Anterior "+
            "               from   \n" + 
            "                gdocrel_documento_if dif, gdoc_documento d,  gdoc_solicitud s, gdoccat_documento cd, gdoccat_portafolio p,  \n" + 
            "                comcat_if cif  \n" + 
            "               where   \n" + 
            "                dif.ic_documento = d.ic_documento and  \n" + 
            "                d.ic_solicitud = s.ic_solicitud and  \n" + 
            "                d.ic_tipo_documento = cd.ic_tipo_documento and  \n" + 
            "                s.ic_portafolio = p.ic_portafolio and  \n" + 
            "                dif.ic_if = cif.ic_if  \n" + 
            ") t WHERE t.ic_estatus in (170,175,180,190) " +    sqlFiltros+
            " order by t.ic_documento desc ";
            log.info(sql);
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                if (key.equals("fechaFormalizacion1") || key.equals("fechaFormalizacion2") || key.equals("fechaVersion1") || key.equals("fechaVersion2")  ){
                    stmt.setString(key, entry.getValue());
                }
                else{
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();            
            while(rs.next()){
                MantenimientoDoc doc = new MantenimientoDoc();
                doc.setIcDocumento(rs.getInt("ic_documento"));
                doc.setIcIF(rs.getInt("ic_if"));
                doc.setFolio(rs.getString("folio"));
                doc.setTipoDocumento(rs.getString("cd_nombre_documento"));
                doc.setPortafolio(rs.getString("cd_nombre_portafolio"));
                doc.setNombreEmpresa(rs.getString("nombreProducto")); // o empresa
                doc.setIntermediario(rs.getString("cg_razon_social"));
                doc.setVersion(rs.getString("version"));
                doc.setIcArchivo(rs.getInt("ic_archivo"));
                doc.setFechaFormalizacion(rs.getString("fechaFormalizacion"));
                String fechaAnterior = rs.getString("fecha_Anterior");
                if (fechaAnterior == null){
                    doc.setFechaFinVersion("--");
                }
                else{
                    doc.setFechaFinVersion(fechaAnterior);
                }
                retorno.add(doc);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    

    @Override
    public ArrayList<ElementoCatalogo> getCatIntermediarios() {
        ArrayList<ElementoCatalogo> retorno = new  ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            String sql = " SELECT DISTINCT (d.IC_IF), c.cg_razon_social FROM GDOCREL_DOCUMENTO_IF d, COMCAT_IF c  WHERE \n" + 
            "d.IC_IF = c.IC_IF  AND d.IC_ESTATUS IN (170,175,180,190) order by c.cg_razon_social ";
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();            
            while(rs.next()){
                ElementoCatalogo cat = new ElementoCatalogo(rs.getString("IC_IF"), rs.getString("cg_razon_social"));
                retorno.add(cat);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatFolios() {
        ArrayList<ElementoCatalogo> retorno = new  ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            String sql = " SELECT DISTINCT (d.IC_DOCUMENTO), s.IC_SOLICITUD, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO FROM \n" + 
            "GDOCREL_DOCUMENTO_IF d, GDOC_DOCUMENTO y, GDOC_SOLICITUD s  WHERE \n" + 
            "d.IC_DOCUMENTO = y.IC_DOCUMENTO AND y.IC_SOLICITUD = s.IC_SOLICITUD AND d.IC_ESTATUS IN (170,175,180,190) order by d.ic_documento asc  ";
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();            
            while(rs.next()){
                ElementoCatalogo cat = new ElementoCatalogo(rs.getString("IC_SOLICITUD"), rs.getString("FOLIO"));
                retorno.add(cat);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatPortafolio() {
        ArrayList<ElementoCatalogo> retorno = new  ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            String sql = " SELECT IC_PORTAFOLIO, CD_NOMBRE_PORTAFOLIO FROM GDOCCAT_PORTAFOLIO ORDER BY CD_NOMBRE_PORTAFOLIO ASC ";
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();            
            while(rs.next()){
                ElementoCatalogo cat = new ElementoCatalogo(rs.getString("IC_PORTAFOLIO"), rs.getString("CD_NOMBRE_PORTAFOLIO"));
                retorno.add(cat);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatNombreProducto() {
        ArrayList<ElementoCatalogo> retorno = new  ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            String sql = " select * from (SELECT dif.ic_documento ,( select NVL(d.TG_NOMBRE, s.TG_NOMBRE) FROM gdoc_solicitud s where s.ic_solicitud=d.ic_solicitud  ) as nombre\n" + 
                "FROM GDOCREL_DOCUMENTO_IF dif, gdoc_documento d WHERE dif.ic_documento = d.ic_documento and dif.ic_estatus in (170,175,180,190) ) t order by t.nombre ";
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();            
            while(rs.next()){
                ElementoCatalogo cat = new ElementoCatalogo(rs.getString("ic_documento"), rs.getString("nombre"));
                retorno.add(cat);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatTipoDocumento() {
        ArrayList<ElementoCatalogo> retorno = new  ArrayList<>();
        ResultSet rs = null;
        PreparedStatement ps = null;
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            String sql = " SELECT IC_TIPO_DOCUMENTO, CD_NOMBRE_DOCUMENTO FROM GDOCCAT_DOCUMENTO ORDER BY CD_NOMBRE_DOCUMENTO ";
            ps = con.queryPrecompilado(sql);
            rs = ps.executeQuery();            
            while(rs.next()){
                ElementoCatalogo cat = new ElementoCatalogo(rs.getString("IC_TIPO_DOCUMENTO"), rs.getString("CD_NOMBRE_DOCUMENTO"));
                retorno.add(cat);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
}
