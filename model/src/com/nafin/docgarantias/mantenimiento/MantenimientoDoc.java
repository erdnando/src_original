package com.nafin.docgarantias.mantenimiento;

import java.io.Serializable;

public class MantenimientoDoc implements Serializable {
    @SuppressWarnings("compatibility:-6341205862606140121")
    private static final long serialVersionUID = 1L;

    public MantenimientoDoc() {
        super();
    }
    
    private Integer icIF;
    private Integer icDocumento;
    private String folio;
    private String tipoDocumento;
    private String portafolio;
    private String nombreEmpresa;
    private String intermediario;
    private Integer icArchivo;
    private String version;
    private String fechaFormalizacion;
    private String fechaFinVersion;

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getIntermediario() {
        return intermediario;
    }

    public void setIntermediario(String intermediario) {
        this.intermediario = intermediario;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchivo) {
        this.icArchivo = icArchivo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFechaFormalizacion() {
        return fechaFormalizacion;
    }

    public void setFechaFormalizacion(String fechaFormalizacion) {
        this.fechaFormalizacion = fechaFormalizacion;
    }

    public String getFechaFinVersion() {
        return fechaFinVersion;
    }

    public void setFechaFinVersion(String fechaFinVersion) {
        this.fechaFinVersion = fechaFinVersion;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }
}
