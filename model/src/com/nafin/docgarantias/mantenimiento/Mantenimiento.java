package com.nafin.docgarantias.mantenimiento;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface Mantenimiento {
    
    public ArrayList<MantenimientoDoc> getDocsMantenimiento(Map<String, String> params);
    public ArrayList<ElementoCatalogo> getCatIntermediarios();
    public ArrayList<ElementoCatalogo> getCatFolios();
    public ArrayList<ElementoCatalogo> getCatPortafolio();
    public ArrayList<ElementoCatalogo> getCatNombreProducto();
    public ArrayList<ElementoCatalogo> getCatTipoDocumento();
    
    
}
