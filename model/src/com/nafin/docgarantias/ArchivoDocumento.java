package com.nafin.docgarantias;

import java.io.Serializable;

public class ArchivoDocumento implements Serializable {
    @SuppressWarnings("compatibility:-2447311031406200201")
    private static final long serialVersionUID = 1L;

    public ArchivoDocumento() {
        super();
    }


    public ArchivoDocumento(String nombreArchivo, Integer icTipoArchivo) {
        super();
        this.nombreArchivo = nombreArchivo;
        this.icTipoArchivo = icTipoArchivo;
    }
    
    private String nombreArchivo;
    
    /** Identificador generado por OnBase al guardarse */
    private Integer itemNum;
    
    /** Identificador de la tabla GDOC_ARCHIVO */
    private Integer icArchivo;

    /** Identificador del tipo de archivo catalogo GDOCCAT_ARCHIVO*/
    private Integer icTipoArchivo;
    
    private byte[] fileRaw;

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Integer getItemNum() {
        return itemNum;
    }

    public void setItemNum(Integer itemNum) {
        this.itemNum = itemNum;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchivo) {
        this.icArchivo = icArchivo;
    }

    public Integer getIcTipoArchivo() {
        return icTipoArchivo;
    }

    public void setIcTipoArchivo(Integer icTipoArchivo) {
        this.icTipoArchivo = icTipoArchivo;
    }

    public byte[] getFileRaw() {
        return fileRaw;
    }

    public void setFileRaw(byte[] fileRaw) {
        this.fileRaw = fileRaw;
    }
}
