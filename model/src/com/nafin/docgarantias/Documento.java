package com.nafin.docgarantias;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Documento implements Serializable{
    @SuppressWarnings("compatibility:-5942673490786338503")
    private static final long serialVersionUID = 2769341795355887434L;
    
    private Integer icDocumento;
    
    private String nombre;
    
    private Integer icTipoDocumento;
    
    private Integer icEstatus;
    
    private Integer icSolicitud;
    
    private Integer icArchivo;
    
    private String nombreEstatus;
    
    private ArrayList<Integer> icIFs;
    
    private Map<Integer,Integer> icArchivoIF;
    
    private Integer icDocumentoOrigen;
    
    private Integer icVersion;
    
    private String fechaRegistroDoc;
    
    private Integer icMovimientoSeleccionado; 
    
    public Documento() {
        super();
    }

    public Integer getEstatus() {
        return icEstatus;
    }

    public void setEstatus(Integer estatus) {
        this.icEstatus = estatus;
    }

    public Integer getIcSolicitud() {
        return icSolicitud;
    }

    public void setIcSolicitud(Integer icSolicitud) {
        this.icSolicitud = icSolicitud;
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }

    public ArrayList<Integer> getIcIFs() {
        return icIFs;
    }

    public void setIcIFs(ArrayList<Integer> icIFs) {
        this.icIFs = icIFs;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Map<Integer, Integer> getIcArchivoIF() {
        return icArchivoIF;
    }

    public void setIcArchivoIF(Map<Integer, Integer> icArchivoIF) {
        this.icArchivoIF = icArchivoIF;
    }
    
    public Integer getIcArchivoIF(Integer icIF){
        if (this.icArchivoIF != null){
            return this.icArchivoIF.get(icIF);
        }
        return null;
    }
    
    public void setIcArchivoIF(Integer icIF, Integer icArchivo){
        if (this.icArchivoIF == null){
            this.icArchivoIF = new HashMap<Integer, Integer>();
        }
        this.icArchivoIF.put(icIF, icArchivo);
    }


    public String getNombreEstatus() {
        return nombreEstatus;
    }

    public void setNombreEstatus(String nombreEstatus) {
        this.nombreEstatus = nombreEstatus;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchivo) {
        this.icArchivo = icArchivo;
    }

    public Integer getIcTipoDocumento() {
        return icTipoDocumento;
    }

    public void setIcTipoDocumento(Integer icTipoDocumento) {
        this.icTipoDocumento = icTipoDocumento;
    }

    public String getFechaRegistroDoc() {
        return fechaRegistroDoc;
    }

    public void setFechaRegistroDoc(String fechaRegistroDoc) {
        this.fechaRegistroDoc = fechaRegistroDoc;
    }

    public Integer getIcDocumentoOrigen() {
        return icDocumentoOrigen;
    }

    public void setIcDocumentoOrigen(Integer icDocumentoOrigen) {
        this.icDocumentoOrigen = icDocumentoOrigen;
    }

    public Integer getIcMovimientoSeleccionado() {
        return icMovimientoSeleccionado;
    }

    public void setIcMovimientoSeleccionado(Integer icMovimientoSeleccionado) {
        this.icMovimientoSeleccionado = icMovimientoSeleccionado;
    }

    public Integer getIcVersion() {
        return icVersion;
    }

    public void setIcVersion(Integer icVersion) {
        this.icVersion = icVersion;
    }
}
