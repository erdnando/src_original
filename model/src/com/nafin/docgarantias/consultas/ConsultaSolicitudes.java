package com.nafin.docgarantias.consultas;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;

@Remote
public interface ConsultaSolicitudes {


    public ArrayList<InfoSolicitud> getInfoSolicitudes(Map<String,String> filtrosBusqueda);
    
    public ArrayList<InfoSolicitud> getInfoDocumentos(Map<String,String> filtrosBusqueda);
    
    public Map<String, ArrayList> getFilesSoporteDocumento(Integer icDocumento, Integer icif);
    
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes(String tipo);
    public ArrayList<ElementoCatalogo> getCatSolicitantes(String tipo);
    public ArrayList<ElementoCatalogo> getCatEstatusSolicitud(String tipo);
    public ArrayList<ElementoCatalogo> getCatEjecutivos(String tipo);
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento(String tipo);
    public ArrayList<ElementoCatalogo> getCatSolicitudesIF(String tipo);
    
}
