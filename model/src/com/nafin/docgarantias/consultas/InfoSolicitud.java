package com.nafin.docgarantias.consultas;

import java.io.Serializable;

import java.util.ArrayList;

import netropology.utilerias.ElementoCatalogo;

public class InfoSolicitud implements Serializable{
    @SuppressWarnings("compatibility:-8534336184548396962")
    private static final long serialVersionUID = 1L;

    private String fechaSolicitud;
    private String folioSolicitud;
    private String usuarioSolicitante;
    private String intermediarioFinanciero;
    private String usuarioEjecutivo;
    private String tipoMovimiento;
    private String tipoDocumento;
    private String version;
    private String portafolio;
    private String nombreProductoEmpresa;
    private Integer icSolicitud;
    private String estatus;
    private String fechaMaximaFormalizacion;
    private String numeroFirmantes;
    private ArrayList<ElementoCatalogo> firmantes;
    private String fechaFormalizacion;
    private String baseOperacion;
    private String numeroFirmantesNafin;
    private ArrayList<ElementoCatalogo> firmantesNafin;
    private Integer icDocumento;
    private Integer icInterFin;
    private Integer icArchivoBO;
    

    public InfoSolicitud() {
        super();
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFolioSolicitud() {
        return folioSolicitud;
    }

    public void setFolioSolicitud(String folioSolicitud) {
        this.folioSolicitud = folioSolicitud;
    }

    public String getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(String usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public String getIntermediarioFinanciero() {
        return intermediarioFinanciero;
    }

    public void setIntermediarioFinanciero(String intermediarioFinanciero) {
        this.intermediarioFinanciero = intermediarioFinanciero;
    }

    public String getUsuarioEjecutivo() {
        return usuarioEjecutivo;
    }

    public void setUsuarioEjecutivo(String usuarioEjecutivo) {
        this.usuarioEjecutivo = usuarioEjecutivo;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getNombreProductoEmpresa() {
        return nombreProductoEmpresa;
    }

    public void setNombreProductoEmpresa(String nombreProductoEmpresa) {
        this.nombreProductoEmpresa = nombreProductoEmpresa;
    }

    public Integer getIcSolicitud() {
        return icSolicitud;
    }

    public void setIcSolicitud(Integer icSolicitud) {
        this.icSolicitud = icSolicitud;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public void setNumeroFirmantes(String numeroFirmantes) {
        this.numeroFirmantes = numeroFirmantes;
    }

    public String getNumeroFirmantes() {
        return numeroFirmantes;
    }

    public void setFechaFormalizacion(String fechaFormalizacion) {
        this.fechaFormalizacion = fechaFormalizacion;
    }

    public String getFechaFormalizacion() {
        return fechaFormalizacion;
    }

    public void setBaseOperacion(String baseOperacion) {
        this.baseOperacion = baseOperacion;
    }

    public String getBaseOperacion() {
        return baseOperacion;
    }

    public void setFechaMaximaFormalizacion(String fechaMaximaFormalizacion) {
        this.fechaMaximaFormalizacion = fechaMaximaFormalizacion;
    }

    public String getFechaMaximaFormalizacion() {
        return fechaMaximaFormalizacion;
    }

    public void setNumeroFirmantesNafin(String numeroFirmantesNafin) {
        this.numeroFirmantesNafin = numeroFirmantesNafin;
    }

    public String getNumeroFirmantesNafin() {
        return numeroFirmantesNafin;
    }
    
    public void setFirmantes(ArrayList<ElementoCatalogo> firmantes) {
        this.firmantes = firmantes;
    }

    public ArrayList<ElementoCatalogo> getFirmantes() {
        return firmantes;
    }

    public void setFirmantesNafin(ArrayList<ElementoCatalogo> firmantesNafin) {
        this.firmantesNafin = firmantesNafin;
    }

    public ArrayList<ElementoCatalogo> getFirmantesNafin() {
        return firmantesNafin;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }


    public void setIcInterFin(Integer icInterFin) {
        this.icInterFin = icInterFin;
    }

    public Integer getIcInterFin() {
        return icInterFin;
    }

    public Integer getIcArchivoBO() {
        return icArchivoBO;
    }

    public void setIcArchivoBO(Integer icArchivoBO) {
        this.icArchivoBO = icArchivoBO;
    }
}
