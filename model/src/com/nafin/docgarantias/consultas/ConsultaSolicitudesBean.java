package com.nafin.docgarantias.consultas;

import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "ConsultaSolicitudes", mappedName = "ConsultaSolicitudes")
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsultaSolicitudesBean implements ConsultaSolicitudes {
    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(ConsultaSolicitudesBean.class);
    
    public ConsultaSolicitudesBean() {
        super();
    }

    @Override
    public ArrayList<InfoSolicitud> getInfoSolicitudes(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        ArrayList<InfoSolicitud> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("s_fechaUno") != null) sqlFltros += " AND  s.DT_FECHA_CREACION BETWEEN TO_DATE(:s_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDos, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("s_folioSolicitud") != null)  sqlFltros += " AND s.IC_SOLICITUD = :s_folioSolicitud ";
        if (filtrosBusqueda.get("s_usuarioSolicitante") != null) sqlFltros += " AND s.IC_USUARIO_REGISTRO = :s_usuarioSolicitante ";
        if (filtrosBusqueda.get("s_tipoMovimiento") != null) sqlFltros += " AND d.IC_MOVIMIENTO = :s_tipoMovimiento ";
        if (filtrosBusqueda.get("s_estatus") != null) sqlFltros += " AND s.IC_ESTATUS = :s_estatus ";
        if (filtrosBusqueda.get("s_ejecutivo") != null) sqlFltros += " AND s.IC_USUARIO_EJECUTIVO = :s_ejecutivo ";
        if (filtrosBusqueda.get("s_intermediario") != null){
            sqlFltros += " AND s.IC_SOLICITUD IN " +
                    " (select k.IC_SOLICITUD from gdoc_solicitud k, gdocrel_solicitud_if j where j.ic_solicitud = k.ic_solicitud AND j.ic_if = :s_intermediario)  ";
        }
        if (filtrosBusqueda.get("s_documento") != null) sqlFltros += " AND d.IC_TIPO_DOCUMENTO = :s_documento ";
        if (filtrosBusqueda.get("s_portafolio") != null) sqlFltros += " AND s.IC_PORTAFOLIO = :s_portafolio ";
        if (filtrosBusqueda.get("s_productoEmpresa") != null) sqlFltros += " AND d.ic_documento =:s_productoEmpresa ";
        
        try {
            con.conexionDB();
/*
            String sql;
            sql = "SELECT d.TG_NOMBRE,\n" + 
                "  s.IC_SOLICITUD,\n" + 
                "  d.IC_VERSION,\n" + 
                "  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,\n" + 
                "  TO_CHAR(s.DT_FECHA_CREACION, 'DD/MM/YYYY')   AS FECHA_CREACION,\n" + 
                "  s.IC_USUARIO_REGISTRO,\n" + 
                "  s.IC_USUARIO_EJECUTIVO,\n" + 
                "  E.Cd_Nombre_Estatus,\n" + 
                "  Cd.Cd_Nombre_Documento,\n" + 
                "  Cm.Cd_Nombre_Movimiento,\n" + 
                "  Cp.Cd_Nombre_Portafolio,\n" + 
                "  (SELECT CASE  WHEN WH>1  THEN 'Varios' ELSE\n" + 
                "        (SELECT I.CG_NOMBRE_COMERCIAL  FROM COMCAT_IF I, GDOCREL_SOLICITUD_IF X\n" + 
                "        WHERE I.IC_IF = X.IC_IF  AND X.IC_SOLICITUD = ICSOLICITUD )  END\n" + 
                "  FROM\n" + 
                "    (SELECT SI.IC_SOLICITUD AS ICSOLICITUD, COUNT(*) AS WH FROM COMCAT_IF CI,\n" + 
                "      GDOCREL_SOLICITUD_IF SI WHERE CI.IC_IF = SI.IC_IF AND SI.IC_SOLICITUD = S.IC_SOLICITUD\n" + 
                "    GROUP BY SI.IC_SOLICITUD))AS INTERMEDIARIO,\n" +
                "    d.ic_documento \n"+  
                "FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCCAT_ESTATUS e,\n" + 
                "  GDOCCAT_DOCUMENTO cd,\n" + 
                "  GDOCCAT_MOVIMIENTO cm,\n" + 
                "  GDOCCAT_PORTAFOLIO cp\n" + 
                "WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                "AND s.IC_ESTATUS        = e.IC_ESTATUS\n" + 
                "AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO\n" + 
                "AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n " + 
                  sqlFltros+
                " ORDER BY s.IC_SOLICITUD DESC ";*/
            
            String sql = 
            " SELECT d.TG_NOMBRE,                                                                                                          "+
            "   s.IC_SOLICITUD,                                                                                                            "+
            "   d.IC_VERSION,                                                                                                              "+
            "   CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,                                           "+
            "   TO_CHAR(s.DT_FECHA_CREACION, 'DD/MM/YYYY')   AS FECHA_CREACION,                                                            "+
            "   s.IC_USUARIO_REGISTRO,                                                                                                     "+
            "   n.cd_nombre_usuario  usuario_registro,                                                                                     "+
            "   s.IC_USUARIO_EJECUTIVO,                                                                                                    "+
            "   (SELECT CD_NOMBRE_USUARIO from gdoc_nombre_usuario where ic_usuario = trim(s.ic_usuario_ejecutivo)) as nombre_ejecutivo,   "+
            "   E.Cd_Nombre_Estatus,                                                                                                       "+
            "   Cd.Cd_Nombre_Documento,                                                                                                    "+
            "   Cm.Cd_Nombre_Movimiento,                                                                                                   "+
            "   Cp.Cd_Nombre_Portafolio,                                                                                                   "+
            "   (SELECT CASE  WHEN WH>1  THEN 'Varios' ELSE                                                                                "+
            "         (SELECT I.CG_NOMBRE_COMERCIAL  FROM COMCAT_IF I, GDOCREL_SOLICITUD_IF X                                              "+
            "         WHERE I.IC_IF = X.IC_IF  AND X.IC_SOLICITUD = ICSOLICITUD )  END                                                     "+
            "   FROM                                                                                                                       "+
            "     (SELECT SI.IC_SOLICITUD AS ICSOLICITUD, COUNT(*) AS WH FROM COMCAT_IF CI,                                                "+
            "       GDOCREL_SOLICITUD_IF SI WHERE CI.IC_IF = SI.IC_IF AND SI.IC_SOLICITUD = S.IC_SOLICITUD                                 "+
            "     GROUP BY SI.IC_SOLICITUD))AS INTERMEDIARIO,                                                                              "+
            "     d.ic_documento                                                                                                           "+
            " FROM GDOC_SOLICITUD s,                                                                                                       "+
            "   GDOC_DOCUMENTO d,                                                                                                          "+
            "   GDOCCAT_ESTATUS e,                                                                                                         "+
            "   GDOCCAT_DOCUMENTO cd,                                                                                                      "+
            "   GDOCCAT_MOVIMIENTO cm,                                                                                                     "+
            "   GDOCCAT_PORTAFOLIO cp,                                                                                                     "+
            "   GDOC_NOMBRE_USUARIO n                                                                                                      "+
            " WHERE                                                                                                                        "+
            " s.IC_ESTATUS        = e.IC_ESTATUS                                                                                       "+
            " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO                                                                               "+
            " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO                                                                                   "+
            " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO                                                                                   "+
            " AND s.IC_SOLICITUD    = d.IC_SOLICITUD                                                                                       "+
            sqlFltros+
            " AND trim(s.IC_USUARIO_REGISTRO) = n.ic_usuario                                                                               "+
            "  ORDER BY s.IC_SOLICITUD DESC                                                                                                ";                
                
            log.info(sql);
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("s_folioSolicitud") || key.equals("s_tipoMovimiento")  || key.equals("s_estatus") || key.equals("s_intermediario") 
                    || key.equals("s_documento") || key.equals("s_portafolio")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    stmt.setChar(key, entry.getValue());
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            //UtilUsr utilUsr = new UtilUsr();
            while(rs.next()){
                InfoSolicitud info = new InfoSolicitud();
                info.setEstatus(rs.getString("Cd_Nombre_Estatus"));
                info.setFechaSolicitud(rs.getString("FECHA_CREACION"));
                info.setFolioSolicitud(rs.getString("FOLIO"));
                info.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                info.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                info.setIntermediarioFinanciero(rs.getString("INTERMEDIARIO"));
                info.setNombreProductoEmpresa(rs.getString("TG_NOMBRE"));
                info.setPortafolio(rs.getString("Cd_Nombre_Portafolio"));
                info.setTipoDocumento(rs.getString("Cd_Nombre_Documento"));
                info.setTipoMovimiento(rs.getString("Cd_Nombre_Movimiento"));
                /*String sejecutivo = rs.getString("IC_USUARIO_EJECUTIVO");
                if (sejecutivo  != null){
                    Usuario ejecutivo = utilUsr.getUsuario(sejecutivo);
                    info.setUsuarioEjecutivo(ejecutivo.getNombreCompleto());
                }  */
                info.setUsuarioEjecutivo(rs.getString("usuario_registro"));
                /*
                String ssolicitante = rs.getString("IC_USUARIO_REGISTRO");
                if (ssolicitante != null){
                    Usuario solicitante = utilUsr.getUsuario(ssolicitante);
                    info.setUsuarioSolicitante(solicitante.getNombreCompleto());
                }*/
                info.setUsuarioSolicitante(rs.getString("nombre_ejecutivo"));
                info.setVersion(rs.getString("IC_VERSION"));
                info.setIcInterFin(0);
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
    public ArrayList<InfoSolicitud> getInfoDocumentos(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        ArrayList<InfoSolicitud> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("s_fechaUno") != null) sqlFltros += " AND  s.DT_FECHA_CREACION BETWEEN TO_DATE(:s_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDos, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("s_folioSolicitud") != null)  sqlFltros += " AND s.IC_SOLICITUD = :s_folioSolicitud ";
        if (filtrosBusqueda.get("s_usuarioSolicitante") != null) sqlFltros += " AND s.IC_USUARIO_REGISTRO = :s_usuarioSolicitante ";
        if (filtrosBusqueda.get("s_tipoMovimiento") != null) sqlFltros += " AND d.IC_MOVIMIENTO = :s_tipoMovimiento ";
        if (filtrosBusqueda.get("s_estatus") != null) sqlFltros += " AND e.IC_ESTATUS = :s_estatus ";
        if (filtrosBusqueda.get("s_ejecutivo") != null) sqlFltros += " AND s.IC_USUARIO_EJECUTIVO = :s_ejecutivo ";
        if (filtrosBusqueda.get("s_intermediario") != null){
            sqlFltros += " AND GI.IC_IF = :s_intermediario ";
        }
        if (filtrosBusqueda.get("s_documento") != null) sqlFltros += " AND d.IC_TIPO_DOCUMENTO = :s_documento ";
        if (filtrosBusqueda.get("s_portafolio") != null) sqlFltros += " AND s.IC_PORTAFOLIO = :s_portafolio ";
        if (filtrosBusqueda.get("s_productoEmpresa") != null) sqlFltros += " AND d.ic_documento =:s_productoEmpresa ";
        
        try {
            con.conexionDB();

            String sql;
            sql = "SELECT distinct d.TG_NOMBRE,\n" + 
            "                s.IC_SOLICITUD,\n" + 
            "                d.IC_VERSION,\n" + 
            "                CONCAT(CONCAT(EXTRACT(YEAR FROM d.dt_fecha_registro), '-'),\n" + 
            "                       s.IC_FOLIO) AS FOLIO,\n" + 
            "                TO_CHAR(d.dt_fecha_registro, 'DD/MM/YYYY') AS FECHA_CREACION,\n" + 
            "                s.IC_USUARIO_REGISTRO,\n" + 
            "                s.IC_USUARIO_EJECUTIVO,\n" + 
            "                E.Cd_Nombre_Estatus,\n" + 
            "                Cd.Cd_Nombre_Documento,\n" + 
            "                Cm.Cd_Nombre_Movimiento,\n" + 
            "                Cp.Cd_Nombre_Portafolio,\n" + 
            "                d.ic_documento,\n" + 
            "                DECODE(gi.dt_fecha_prorroga, '', " +
            "                DECODE((SELECT COUNT(DT_FECHA_MOVIMIENTO)\n" + 
            "                         FROM gdoc_bitacora_doc b\n" + 
            "                        where b.ic_estatus = 140\n" + 
            "                          --and b.ic_if = gi.ic_if\n" + 
            "                          and b.ic_solicitud = s.ic_solicitud),\n" + 
            "                       0,\n" + 
            "                       '',\n" + 
            "                       To_char(SIGDIAHABIL2((SELECT TRUNC(MAX(DT_FECHA_MOVIMIENTO))\n" + 
            "                                      FROM gdoc_bitacora_doc b\n" + 
            "                                     where b.ic_estatus = 140\n" + 
            "                                       --and b.ic_if = gi.ic_if\n" + 
            "                                       and b.ic_solicitud = s.ic_solicitud),\n" + 
            "                                    1,\n" + 
            "                                    (select IN_DIAS_LIMITE_FIRMA\n" + 
            "                                       from gdoc_parametro_if pi\n" + 
            "                                      where pi.ic_if = GI.IC_IF)), 'dd/mm/yyyy') ), to_char(gi.dt_fecha_prorroga,'dd/mm/yyyy')) fechaMax1,\n" + 
            "                (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO), 'DD/MM/YYYY')\n" + 
            "                   FROM gdoc_bitacora_doc b\n" + 
            "                  where b.ic_estatus = 140\n" + 
            "                    and b.ic_documento = d.ic_documento\n" + 
            "                    and b.ic_if = gi.ic_if) as FECHA_LIBERACION,\n" + 
            "                nvl((select IN_DIAS_LIMITE_FIRMA\n" + 
            "                      from gdoc_parametro_if pi\n" + 
            "                     where pi.ic_if = GI.IC_IF),\n" + 
            "                    0) as diasLimiteFormalizacion,\n" + 
            "                nvl((select In_Firmas_Requerido\n" + 
            "                      from gdoc_parametro_if pi\n" + 
            "                     where pi.ic_if = GI.IC_IF),\n" + 
            "                    0) as totalFirmasRequeridas,\n" + 
            "                (SELECT COUNT(TG_FOLIO_PKI)\n" + 
            "                   FROM GDOC_ACUSE_FORMALIZACION\n" + 
            "                  WHERE IC_IF = GI.IC_IF\n" + 
            "                    and IC_DOCUMENTO = d.ic_documento" +
            "                and IC_TIPO_FIRMA = 2 ) as firmasRegistradasIf,\n" +            
            "                (SELECT COUNT(TG_FOLIO_PKI)\n" + 
            "                   FROM GDOC_ACUSE_FORMALIZACION\n" + 
            "                  WHERE IC_IF = GI.IC_IF\n" + 
            "                    and IC_DOCUMENTO = d.ic_documento" +
            "                and IC_TIPO_FIRMA = 4 ) as firmasRegistradasNafin,\n" +      
            "                (select TO_CHAR(MAX(DT_FECHA_FIRMA), 'DD/MM/YYYY')\n" + 
            "                       from GDOC_ACUSE_FORMALIZACION F\n" + 
            "                      WHERE F.IC_TIPO_FIRMA = 4\n" + 
            "                        AND F.IC_IF = GI.ic_if\n" + 
            "                        AND F.IC_DOCUMENTO = d.ic_documento\n) fecha_formalizacion,\n" + 
            "                (SELECT I.CG_NOMBRE_COMERCIAL\n" + 
            "                   FROM COMCAT_IF I\n" + 
            "                  WHERE I.IC_IF = gi.ic_if) AS INTERMEDIARIO,\n" + 
            "                GI.IC_IF,\n" +
            "                (SELECT LISTAGG(acf.IC_USUARIO||','||to_char(acf.dt_fecha_firma,'dd/mm/yyyy hh:mi'), '|') WITHIN GROUP (ORDER BY acf.IC_USUARIO)\n" + 
            "                FROM GDOC_ACUSE_FORMALIZACION  acf WHERE acf.IC_TIPO_FIRMA = 4 AND acf.IC_IF = gi.IC_IF AND acf.IC_DOCUMENTO = d.IC_DOCUMENTO ) AS usuariosFirNAFIN, \n" + 
            "                (SELECT LISTAGG(acf.IC_USUARIO||','||to_char(acf.dt_fecha_firma,'dd/mm/yyyy hh:mi'), '|') WITHIN GROUP (ORDER BY acf.IC_USUARIO)\n" + 
            "                FROM GDOC_ACUSE_FORMALIZACION  acf WHERE acf.IC_TIPO_FIRMA = 2 AND acf.IC_IF = gi.IC_IF AND acf.IC_DOCUMENTO = d.IC_DOCUMENTO ) AS usuariosFirIF ,"+      
            "               (select IC_ARCHIVO from GDOC_BASE_OPERACION where IC_ESTATUS > 1 and IC_DOCUMENTO = d.ic_documento and IC_IF = gi.ic_if) as icArchivoBO " +
            "  FROM GDOC_SOLICITUD       s,\n" + 
            "       GDOC_DOCUMENTO       d,\n" + 
            "       GDOCCAT_ESTATUS      e,\n" + 
            "       GDOCCAT_DOCUMENTO    cd,\n" + 
            "       GDOCCAT_MOVIMIENTO   cm,\n" + 
            "       GDOCCAT_PORTAFOLIO   cp,\n" + 
            "       GDOCREL_DOCUMENTO_IF GI\n" + 
            " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
            "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
            "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
            "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
            "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
            "   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                  sqlFltros+
                " ORDER BY s.IC_SOLICITUD DESC ";
            log.info(sql);
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("s_folioSolicitud") || key.equals("s_tipoMovimiento")  || key.equals("s_estatus") || key.equals("s_intermediario") 
                    || key.equals("s_documento") || key.equals("s_portafolio")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    stmt.setChar(key, entry.getValue());
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            
            while(rs.next()){
                InfoSolicitud info = new InfoSolicitud();
                info.setEstatus(rs.getString("Cd_Nombre_Estatus"));
                info.setFechaSolicitud(rs.getString("FECHA_CREACION"));
                info.setFolioSolicitud(rs.getString("FOLIO"));
                info.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                info.setIntermediarioFinanciero(rs.getString("INTERMEDIARIO"));
                info.setNombreProductoEmpresa(rs.getString("TG_NOMBRE"));
                info.setPortafolio(rs.getString("Cd_Nombre_Portafolio"));
                info.setTipoDocumento(rs.getString("Cd_Nombre_Documento"));
                info.setTipoMovimiento(rs.getString("Cd_Nombre_Movimiento"));
                info.setIcDocumento(rs.getInt("ic_documento"));
                info.setIcInterFin(rs.getInt("IC_IF"));
                String susuario = rs.getString("IC_USUARIO_EJECUTIVO");
                if (susuario != null){
                    Usuario ejecutivo = utilUsr.getUsuario(susuario);
                    info.setUsuarioEjecutivo(ejecutivo.getNombreCompleto());                
                }
                String ssolicitante =rs.getString("IC_USUARIO_REGISTRO");
                if (ssolicitante  != null){
                    Usuario solicitante = utilUsr.getUsuario(ssolicitante);
                    info.setUsuarioSolicitante(solicitante.getNombreCompleto());                
                }
                info.setVersion(rs.getString("IC_VERSION"));
                info.setIcArchivoBO(rs.getInt("icArchivoBO"));
                String firmantesNafin = rs.getString("usuariosFirNAFIN");
                ArrayList<ElementoCatalogo> firmantesRequeridos = new ArrayList<>();
                if(firmantesNafin != null &&!firmantesNafin.equals("")){
                    String arrFirmantes[] = firmantesNafin.trim().split("\\|");
                
                    for(String firma : arrFirmantes){
                        String[] arrFirmas = firma.split(",");
                        Usuario usuarillo = utilUsr.getUsuario(arrFirmas[0]);
                        firmantesRequeridos.add(new ElementoCatalogo(arrFirmas[0], usuarillo.getNombreCompleto()+" "+arrFirmas[1]));
                    }
                }
                
                info.setFirmantesNafin(firmantesRequeridos);
                
                String firmantes = rs.getString("usuariosFirIF");
                ArrayList<ElementoCatalogo> firmantesRequeridosIF = new ArrayList<>();
                if(firmantes != null &&!firmantes.equals("")){
                String arrFirmantesIF[] = firmantes.trim().split("\\|");
                
                for(String firmaIF : arrFirmantesIF){
                    
                    String[] arrFirmas = firmaIF.split(",");
                    
                    Usuario usuarillo = utilUsr.getUsuario(arrFirmas[0]);
                    firmantesRequeridosIF.add(new ElementoCatalogo(arrFirmas[0], usuarillo.getNombreCompleto()+" "+arrFirmas[1]));
                }
                }
                
                info.setFirmantes(firmantesRequeridosIF);
                                 
                // Calculo de fecha maxima de formalizacion 
                //int diasLimiteFormalizacion = rs.getInt("diasLimiteFormalizacion");
                //String fechaInicial = rs.getString("FECHA_LIBERACION");
                //String fechaMaxFormalizacion = Fecha.sumaFechaDiasHabiles(fechaInicial, "dd/MM/yyyy", diasLimiteFormalizacion );
                //info.setFechaMaximaFormalizacion(fechaMaxFormalizacion);
                info.setFechaMaximaFormalizacion(rs.getString("fechaMax1"));
                String totalFirmas = rs.getString("totalFirmasRequeridas");
                String noFirmantes = rs.getString("firmasRegistradasIf");    
                info.setNumeroFirmantes(noFirmantes + " de "+ totalFirmas);
                
                String noFirmantesNa = rs.getString("firmasRegistradasNafin");
                info.setNumeroFirmantesNafin(noFirmantesNa);
                
                
                
                info.setFechaFormalizacion("--");
                if(totalFirmas!=null&&noFirmantes!=null&&totalFirmas.equals(noFirmantes)){
                    info.setFechaFormalizacion(rs.getString("fecha_formalizacion"));
                }
                
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
   
    
    public Map<String, ArrayList> getFilesSoporteDocumento(Integer icDocumento, Integer icif) {
        log.info("getFilesSoporteDocumento(E)");
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> archivosSoporte = new ArrayList<>();
        ArrayList<ConsultaDocumentosValue> archivosFormalizar = new ArrayList<>();
        Map<String,ArrayList> retorno = new HashMap<>();
        String sqlSoporte = "";
        String sqlFormalizar = "";
        UtilUsr utilUsr = new UtilUsr();
        try {
            con.conexionDB();
            sqlSoporte = " select a.CD_NOMBRE_ARCHIVO, a.ic_archivo from Gdocrel_Solicitud_Archivo sa, GDOC_DOCUMENTO d, GDOC_ARCHIVO a\n" + 
                " WHERE a.ic_archivo = sa.ic_archivo AND sa.ic_solicitud = d.ic_solicitud AND d.ic_documento= ? ";
            ps = con.queryPrecompilado(sqlSoporte); 
            log.info(sqlSoporte);
            ps.setInt(1, icDocumento);
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getString("ic_archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                archivosSoporte.add(elcat);
            }        

            sqlFormalizar = "  select a.CD_NOMBRE_ARCHIVO,\n" + 
            "       a.ic_archivo,\n" + 
            "       d.ic_version, \n" + 
            "       (SELECT LISTAGG(acf.IC_USUARIO || ',' ||\n" + 
            "                       to_char(acf.dt_fecha_firma, 'dd/mm/yyyy hh:mi'),\n" + 
            "                       '|') WITHIN GROUP(ORDER BY acf.IC_USUARIO)\n" + 
            "          FROM GDOC_ACUSE_FORMALIZACION acf\n" + 
            "         WHERE acf.IC_TIPO_FIRMA = 2\n" + 
            "           AND acf.IC_IF = ?\n" + 
            "           AND acf.IC_DOCUMENTO = d.IC_DOCUMENTO) AS usuariosFirIF\n" + 
            "  from GDOC_DOCUMENTO d, GDOC_ARCHIVO a\n" + 
            " WHERE a.ic_archivo = d.ic_archivo\n" + 
            "   AND d.ic_documento = ? ";
            ps = con.queryPrecompilado(sqlFormalizar); 
            ps.setInt(1, icif);
            ps.setInt(2, icDocumento);
            rs = ps.executeQuery();
            ArrayList<ElementoCatalogo> firmantesRequeridosIF = new ArrayList<>();
            while (rs.next()){
                String val = rs.getString("ic_archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                String version  = rs.getString("ic_version");
                
                ConsultaDocumentosValue elcat = new ConsultaDocumentosValue();
                elcat.setClave(val);
                elcat.setDescripcion(descripcion);
                elcat.setVersion(version);
                
                String firmantes = rs.getString("usuariosFirIF");
                firmantesRequeridosIF.clear();
                if(firmantes != null &&!firmantes.equals("")){
                    String arrFirmantesIF[] = firmantes.trim().split("\\|");                    
                    for(String firmaIF : arrFirmantesIF){
                        String[] arrFirmas = firmaIF.split(",");
                        Usuario usuarillo = utilUsr.getUsuario(arrFirmas[0]);
                        firmantesRequeridosIF.add(new ElementoCatalogo(arrFirmas[0], usuarillo.getNombreCompleto()+" "+arrFirmas[1]));
                    }
                }
                elcat.setFirmantes(firmantesRequeridosIF);
                
                archivosFormalizar.add(elcat);
            }  
            
            String sqlFicha = " SELECT a.CD_NOMBRE_ARCHIVO, a.ic_archivo FROM GDOC_ARCHIVO A, GDOCREL_DOCUMENTO_IF d WHERE a.ic_archivo = d.ic_archivo and d.ic_documento = ? and d.ic_if = ? ";
            ps = con.queryPrecompilado(sqlFicha);
            ps.setInt(1, icDocumento);
            ps.setInt(2, icif);
            rs = ps.executeQuery();
            Integer icFicha = null;
            String nombreFicha = null;
            while (rs.next()){
                icFicha = rs.getInt("ic_archivo");
                nombreFicha = rs.getString("CD_NOMBRE_ARCHIVO");
            }
            
            ConsultaDocumentosValue ficha = new ConsultaDocumentosValue();
            if (icFicha != null){
                ficha.setClave(icFicha+"");
                ficha.setDescripcion(nombreFicha);
                ficha.setVersion(1+"");
                ficha.setFirmantes(firmantesRequeridosIF);
                archivosFormalizar.add(ficha);                
            }
            log.info(sqlFicha);
            log.info("Param: "+ icDocumento+" "+ icif);
            
            retorno.put("documentosSoporte", archivosSoporte);
            retorno.put("documentosFormalizar", archivosFormalizar);
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
        } catch(Exception ex){
            log.error(ex.getMessage());
            ex.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", ex);    
        }finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }            
            log.info("getFilesSoporteDocumento(S)");
        }
        return retorno;
    }
    
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes(String tipo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select distinct s.ic_Solicitud, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO, s.DT_FECHA_CREACION ");
            if(tipo.equals("D")){
                strSQL.append("from GDOC_SOLICITUD       s,\n" + 
                "       GDOC_DOCUMENTO       d,\n" + 
                "       GDOCCAT_ESTATUS      e,\n" + 
                "       GDOCCAT_DOCUMENTO    cd,\n" + 
                "       GDOCCAT_MOVIMIENTO   cm,\n" + 
                "       GDOCCAT_PORTAFOLIO   cp,\n" + 
                "       GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n");
             }else if(tipo.equals("S")){
                strSQL.append("FROM GDOC_SOLICITUD s, \n" + 
                "      GDOC_DOCUMENTO d, \n" + 
                "      GDOCCAT_ESTATUS e, \n" + 
                "      GDOCCAT_DOCUMENTO cd, \n" + 
                "      GDOCCAT_MOVIMIENTO cm, \n" + 
                "      GDOCCAT_PORTAFOLIO cp \n" + 
                " WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                " AND s.IC_ESTATUS        = e.IC_ESTATUS \n" + 
                " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO \n" + 
                " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO \n" + 
                " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n");        
            }else {
                strSQL.append("FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCCAT_ESTATUS e,\n" + 
                "  GDOCCAT_DOCUMENTO cd,\n" + 
                "  GDOCCAT_MOVIMIENTO cm,\n" + 
                "  GDOCCAT_PORTAFOLIO cp,\n" + 
                "  GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                "   AND GI.IC_IF ="+tipo);
            }
            
            strSQL.append("ORDER by s.dt_fecha_creacion desc  ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.debug(strSQL.toString());
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_Solicitud");
                String descripcion = rs.getString("FOLIO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }

    public ArrayList<ElementoCatalogo> getCatEstatusSolicitud(String tipo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT E.IC_ESTATUS, E.CD_NOMBRE_ESTATUS ");
            if(tipo.equals("D")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "       GDOC_DOCUMENTO       d,\n" + 
                "       GDOCCAT_ESTATUS      E,\n" + 
                "       GDOCCAT_DOCUMENTO    cd,\n" + 
                "       GDOCCAT_MOVIMIENTO   cm,\n" + 
                "       GDOCCAT_PORTAFOLIO   cp,\n" + 
                "       GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n");
             }else if(tipo.equals("S")){
                strSQL.append("FROM GDOC_SOLICITUD s, \n" + 
                "      GDOC_DOCUMENTO d, \n" + 
                "      GDOCCAT_ESTATUS E, \n" + 
                "      GDOCCAT_DOCUMENTO cd, \n" + 
                "      GDOCCAT_MOVIMIENTO cm, \n" + 
                "      GDOCCAT_PORTAFOLIO cp \n" + 
                " WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                " AND s.IC_ESTATUS        = e.IC_ESTATUS \n" + 
                " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO \n" + 
                " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO \n" + 
                " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n");        
            } else {
                strSQL.append("FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCCAT_ESTATUS e,\n" + 
                "  GDOCCAT_DOCUMENTO cd,\n" + 
                "  GDOCCAT_MOVIMIENTO cm,\n" + 
                "  GDOCCAT_PORTAFOLIO cp,\n" + 
                "  GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                "   AND GI.IC_IF ="+tipo);
            }
            
            strSQL.append("ORDER BY E.IC_ESTATUS ASC  ");
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("IC_ESTATUS");
                String descripcion = rs.getString("CD_NOMBRE_ESTATUS");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    public ArrayList<ElementoCatalogo> getCatSolicitantes(String tipo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" Select distinct(S.IC_USUARIO_REGISTRO) ");
            
            if(tipo.equals("D") || tipo.equals("I")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "       GDOC_DOCUMENTO       d,\n" + 
                "       GDOCCAT_ESTATUS      E,\n" + 
                "       GDOCCAT_DOCUMENTO    cd,\n" + 
                "       GDOCCAT_MOVIMIENTO   cm,\n" + 
                "       GDOCCAT_PORTAFOLIO   cp,\n" + 
                "       GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n");
             }else if(tipo.equals("S")){
                strSQL.append("FROM GDOC_SOLICITUD s, \n" + 
                "      GDOC_DOCUMENTO d, \n" + 
                "      GDOCCAT_ESTATUS E, \n" + 
                "      GDOCCAT_DOCUMENTO cd, \n" + 
                "      GDOCCAT_MOVIMIENTO cm, \n" + 
                "      GDOCCAT_PORTAFOLIO cp \n" + 
                " WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                " AND s.IC_ESTATUS        = e.IC_ESTATUS \n" + 
                " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO \n" + 
                " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO \n" + 
                " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n");        
            }
            
            strSQL.append("AND S.IC_USUARIO_REGISTRO IS NOT NULL ");
            
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO_REGISTRO");
                Usuario usuario = utilUsr.getUsuario(val);
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    /** Regesa el ctalogo de Ejecutivos que tengan una solicitud registrada
     *  
     * **/
    public ArrayList<ElementoCatalogo> getCatEjecutivos(String tipo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT distinct(s.IC_USUARIO_EJECUTIVO) ");
            
            if(tipo.equals("D") || tipo.equals("I")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "       GDOC_DOCUMENTO       d,\n" + 
                "       GDOCCAT_ESTATUS      E,\n" + 
                "       GDOCCAT_DOCUMENTO    cd,\n" + 
                "       GDOCCAT_MOVIMIENTO   cm,\n" + 
                "       GDOCCAT_PORTAFOLIO   cp,\n" + 
                "       GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n");
             }else if(tipo.equals("S")){
                strSQL.append("FROM GDOC_SOLICITUD s, \n" + 
                "      GDOC_DOCUMENTO d, \n" + 
                "      GDOCCAT_ESTATUS E, \n" + 
                "      GDOCCAT_DOCUMENTO cd, \n" + 
                "      GDOCCAT_MOVIMIENTO cm, \n" + 
                "      GDOCCAT_PORTAFOLIO cp \n" + 
                " WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                " AND s.IC_ESTATUS        = e.IC_ESTATUS \n" + 
                " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO \n" + 
                " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO \n" + 
                " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n");        
            }
            
            strSQL.append("AND s.IC_USUARIO_EJECUTIVO IS NOT NULL  ");
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            ElementoCatalogo catTodos = new ElementoCatalogo("0", "Todos los Ejecutivos");
            retorno.add(catTodos);
            UtilUsr utilUsr = new UtilUsr();
            while (rs.next()){
                String val = rs.getString("IC_USUARIO_EJECUTIVO");
                Usuario usuario = utilUsr.getUsuario(val);
                ElementoCatalogo elcat = new ElementoCatalogo(val, usuario.getNombreCompleto());
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    /** Regresa un Catalogo de los Tipos de Movimientos para una Solicitud
     * */
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento(String tipo){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT DISTINCT cm.IC_MOVIMIENTO, cm.CD_NOMBRE_MOVIMIENTO ");
            
            if(tipo.equals("D")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "       GDOC_DOCUMENTO       d,\n" + 
                "       GDOCCAT_ESTATUS      E,\n" + 
                "       GDOCCAT_DOCUMENTO    cd,\n" + 
                "       GDOCCAT_MOVIMIENTO   cm,\n" + 
                "       GDOCCAT_PORTAFOLIO   cp,\n" + 
                "       GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n");
             }else if(tipo.equals("S")){
                strSQL.append("FROM GDOC_SOLICITUD s, \n" + 
                "      GDOC_DOCUMENTO d, \n" + 
                "      GDOCCAT_ESTATUS E, \n" + 
                "      GDOCCAT_DOCUMENTO cd, \n" + 
                "      GDOCCAT_MOVIMIENTO cm, \n" + 
                "      GDOCCAT_PORTAFOLIO cp \n" + 
                " WHERE s.IC_SOLICITUD    = d.IC_SOLICITUD\n" + 
                " AND s.IC_ESTATUS        = e.IC_ESTATUS \n" + 
                " AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO \n" + 
                " AND d.IC_MOVIMIENTO     = cm.IC_MOVIMIENTO \n" + 
                " AND s.IC_PORTAFOLIO     = cp.IC_PORTAFOLIO\n");        
            } else {
                strSQL.append("FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCCAT_ESTATUS e,\n" + 
                "  GDOCCAT_DOCUMENTO cd,\n" + 
                "  GDOCCAT_MOVIMIENTO cm,\n" + 
                "  GDOCCAT_PORTAFOLIO cp,\n" + 
                "  GDOCREL_DOCUMENTO_IF GI\n" + 
                " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                "   AND GI.IC_IF ="+tipo);
            }
            
            strSQL.append("ORDER BY cm.IC_MOVIMIENTO ASC   ");            
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            ElementoCatalogo emptyCat = new ElementoCatalogo("", "Todos los movimientos");
            retorno.add(emptyCat);
            while (rs.next()){
                ElementoCatalogo elcat = new ElementoCatalogo(rs.getInt("IC_MOVIMIENTO")+"", rs.getString("CD_NOMBRE_MOVIMIENTO"));
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar catalogo de Movimentos ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;             
    }
    
    public ArrayList<ElementoCatalogo> getCatSolicitudesIF(String tipo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT distinct(i.ic_if), i.CG_NOMBRE_COMERCIAL ");
            
            if(tipo.equals("D")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "                       GDOC_DOCUMENTO       d,\n" + 
                "                       GDOCCAT_ESTATUS      E, \n" + 
                "                       GDOCCAT_DOCUMENTO    cd, \n" + 
                "                       GDOCCAT_MOVIMIENTO   cm, \n" + 
                "                       GDOCCAT_PORTAFOLIO   cp, \n" + 
                "                       GDOCREL_DOCUMENTO_IF GI,\n" + 
                "                       COMCAT_IF i \n" + 
                "                 WHERE s.IC_SOLICITUD = d.IC_SOLICITUD \n" + 
                "                   AND GI.IC_ESTATUS = e.IC_ESTATUS \n" + 
                "                   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "                   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "                   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO \n" + 
                "                   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                "                   AND GI.IC_IF = I.IC_IF");
             }else if(tipo.equals("S")){
                strSQL.append("from GDOC_SOLICITUD s,\n" + 
                "                       GDOC_DOCUMENTO       d,\n" + 
                "                       GDOCCAT_ESTATUS      E, \n" + 
                "                       GDOCCAT_DOCUMENTO    cd, \n" + 
                "                       GDOCCAT_MOVIMIENTO   cm, \n" + 
                "                       GDOCCAT_PORTAFOLIO   cp, \n" + 
                "                       GDOCREL_SOLICITUD_IF SF,\n" + 
                "                       COMCAT_IF i \n" + 
                "                 WHERE s.IC_SOLICITUD = d.IC_SOLICITUD \n" + 
                "                   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "                   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "                   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO \n" + 
                "                   AND SF.IC_SOLICITUD = S.IC_SOLICITUD\n" + 
                "                   AND SF.IC_IF = I.IC_IF\n");        
            }
            
            
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            while (rs.next()){
                String val = rs.getInt("ic_if")+"";
                String descripcion =  rs.getString("CG_NOMBRE_COMERCIAL");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion); 
                retorno.add(elcat);
            }        
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    } 
    
}
