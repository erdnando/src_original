package com.nafin.docgarantias.consultas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


@Stateless(name = "ConsultaDocumentos", mappedName = "ConsultaDocumentos")
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsultaDocumentosBean implements ConsultaDocumentos {
    
    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(ConsultaDocumentosBean.class);
    
    public ConsultaDocumentosBean() {
        super();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT ic_tipo_documento, cd_nombre_documento FROM GDOCCAT_DOCUMENTO ORDER BY ic_tipo_documento ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_tipo_documento");
                String descripcion = rs.getString("cd_nombre_documento");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        //return retorno;
    }
    
    
    
}
