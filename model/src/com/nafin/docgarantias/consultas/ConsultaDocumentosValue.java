package com.nafin.docgarantias.consultas;

import java.io.Serializable;

import java.util.ArrayList;

import netropology.utilerias.ElementoCatalogo;

public class ConsultaDocumentosValue implements Serializable{
    
    private String clave;
    private String descripcion;
    private String version;
    private ArrayList<ElementoCatalogo> firmantes;
    
    public ConsultaDocumentosValue() {
       
    }


    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setFirmantes(ArrayList<ElementoCatalogo> firmantes) {
        this.firmantes = firmantes;
    }

    public ArrayList<ElementoCatalogo> getFirmantes() {
        return firmantes;
    }
}
