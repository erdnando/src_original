package com.nafin.docgarantias.consultas;

import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "ConsultaSolicitudesIf", mappedName = "ConsultaSolicitudesIf")
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsultaSolicitudesIfBean implements ConsultaSolicitudesIf {
    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(ConsultaSolicitudesIfBean.class);
    
    
    public ConsultaSolicitudesIfBean() {
        super();
    }

    public ArrayList<InfoSolicitud> getInfoSolicitudes(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        
        
        
        ArrayList<InfoSolicitud> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("s_fechaUno") != null) sqlFltros += " AND  s.DT_FECHA_CREACION BETWEEN TO_DATE(:s_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDos, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("s_folioSolicitud") != null)  sqlFltros += " AND s.IC_SOLICITUD = :s_folioSolicitud ";
        if (filtrosBusqueda.get("s_usuarioSolicitante") != null) sqlFltros += " AND s.IC_USUARIO_REGISTRO = :s_usuarioSolicitante ";
        if (filtrosBusqueda.get("s_tipoMovimiento") != null) sqlFltros += " AND d.IC_MOVIMIENTO = :s_tipoMovimiento ";
        if (filtrosBusqueda.get("s_estatus") != null) sqlFltros += " AND e.IC_ESTATUS = :s_estatus ";
        if (filtrosBusqueda.get("s_ejecutivo") != null) sqlFltros += " AND s.IC_USUARIO_EJECUTIVO = :s_ejecutivo ";
        if (filtrosBusqueda.get("s_documento") != null) sqlFltros += " AND d.IC_TIPO_DOCUMENTO = :s_documento ";
        if (filtrosBusqueda.get("s_portafolio") != null) sqlFltros += " AND s.IC_PORTAFOLIO = :s_portafolio ";
        if (filtrosBusqueda.get("s_productoEmpresa") != null) sqlFltros += " AND d.TG_NOMBRE =:s_productoEmpresa ";
        
        if (filtrosBusqueda.get("s_fechaUnoMax") != null){
            sqlFltros +=   " AND DECODE((SELECT COUNT(DT_FECHA_MOVIMIENTO)\n" + 
                "                FROM gdoc_bitacora_doc b\n" + 
                "               where b.ic_estatus = 140\n" + 
                "                 and b.ic_solicitud = s.ic_solicitud),\n" + 
                "              0,\n" + 
                "              TRUNC(SYSDATE),\n" + 
                "              SIGDIAHABIL2((SELECT TRUNC(MAX(DT_FECHA_MOVIMIENTO))\n" + 
                "                             FROM gdoc_bitacora_doc b\n" + 
                "                            where b.ic_estatus = 140\n" + 
                "                              and b.ic_solicitud = s.ic_solicitud),\n" + 
                "                           1,\n" + 
                "                           (select IN_DIAS_LIMITE_FIRMA\n" + 
                "                              from gdoc_parametro_if pi\n" + 
                "                             where pi.ic_if = :s_intermediario))) BETWEEN TO_DATE(:s_fechaUnoMax, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDosMax, 'DD/MM/YYYY') ";
        }    
        if (filtrosBusqueda.get("s_fechaUnoFor") != null){ sqlFltros += " AND  (SELECT to_char(MAX(DT_FECHA_FIRMA),'dd/mm/yyyy')\n" + 
            " FROM GDOC_ACUSE_FORMALIZACION\n" + 
            " WHERE IC_IF = :s_intermediario\n" + 
            " and IC_DOCUMENTO = d.ic_documento) BETWEEN TO_DATE(:s_fechaUnoFor, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDosFor, 'DD/MM/YYYY') ";        
        }
        try {
            con.conexionDB();

            String sql;
            sql = "SELECT d.TG_NOMBRE,\n" + 
                "  s.IC_SOLICITUD,\n" + 
                "  d.IC_VERSION,\n" + 
                "  CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) AS FOLIO,\n" + 
                "  TO_CHAR(s.DT_FECHA_CREACION, 'DD/MM/YYYY')   AS FECHA_CREACION,\n" + 
                "  s.IC_USUARIO_REGISTRO,\n" + 
                "  s.IC_USUARIO_EJECUTIVO,\n" + 
                "  E.Cd_Nombre_Estatus,\n" + 
                "  Cd.Cd_Nombre_Documento,\n" + 
                "  Cm.Cd_Nombre_Movimiento,\n" + 
                "  Cp.Cd_Nombre_Portafolio,\n" + 
                "  d.ic_documento, \n"+ 
                " DECODE(gi.dt_fecha_prorroga, '', " +
                " DECODE((SELECT COUNT(DT_FECHA_MOVIMIENTO)\n" + 
                "                FROM gdoc_bitacora_doc b\n" + 
                "               where b.ic_estatus = 140\n" + 
                "                 and b.ic_solicitud = s.ic_solicitud),\n" + 
                "              0,\n" + 
                "              '',\n" + 
                "              To_char(SIGDIAHABIL2((SELECT MAX(DT_FECHA_MOVIMIENTO)\n" + 
                "                             FROM gdoc_bitacora_doc b\n" + 
                "                            where b.ic_estatus = 140\n" + 
                "                              and b.ic_solicitud = s.ic_solicitud),\n" + 
                "                           1,\n" + 
                "                           (select IN_DIAS_LIMITE_FIRMA\n" + 
                "                              from gdoc_parametro_if pi\n" + 
                "                             where pi.ic_if = :s_intermediario)), 'dd/mm/yyyy') ), to_char(gi.dt_fecha_prorroga,'dd/mm/yyyy')) fechaMax1, " +
            " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM gdoc_bitacora_doc b where b.ic_estatus = 140 and " +
            " b.ic_solicitud = s.ic_solicitud) as FECHA_LIBERACION, "+
                  
            " (select IN_DIAS_LIMITE_FIRMA from gdoc_parametro_if pi where pi.ic_if= :s_intermediario) as diasLimiteFormalizacion, "+
            " (select In_Firmas_Requerido from gdoc_parametro_if pi where pi.ic_if= :s_intermediario) as totalFirmasRequeridas, "+
            "  ( SELECT COUNT(TG_FOLIO_PKI) FROM GDOC_ACUSE_FORMALIZACION WHERE IC_IF = :s_intermediario and IC_DOCUMENTO  = d.ic_documento and IC_TIPO_FIRMA = 2) as firmasRegistradas, " +
            " (SELECT to_char(MAX(DT_FECHA_FIRMA),'dd/mm/yyyy')\n" + 
            "   FROM GDOC_ACUSE_FORMALIZACION\n" + 
            " WHERE IC_IF = :s_intermediario\n" + 
            "   and IC_DOCUMENTO = d.ic_documento\n) fecha_formalizacion,  "+      
                "  (SELECT LISTAGG(acf.IC_USUARIO||','||to_char(acf.dt_fecha_firma,'dd/mm/yyyy hh:mi'), '|') WITHIN GROUP (ORDER BY acf.IC_USUARIO)\n" + 
                "  FROM GDOC_ACUSE_FORMALIZACION  acf WHERE acf.IC_TIPO_FIRMA = 2 AND acf.IC_IF = :s_intermediario AND acf.IC_DOCUMENTO = d.IC_DOCUMENTO ) AS usuariosFirIF, "+   
                " (select IC_ARCHIVO from GDOC_BASE_OPERACION where IC_ESTATUS = 3 and IC_DOCUMENTO = d.ic_documento and IC_IF = gi.ic_if) as icArchivoBO " +
                "FROM GDOC_SOLICITUD s,\n" + 
                "  GDOC_DOCUMENTO d,\n" + 
                "  GDOCCAT_ESTATUS e,\n" + 
                "  GDOCCAT_DOCUMENTO cd,\n" + 
                "  GDOCCAT_MOVIMIENTO cm,\n" + 
                "  GDOCCAT_PORTAFOLIO cp,\n" + 
                "  GDOCREL_DOCUMENTO_IF GI\n" +   
                "WHERE s.IC_SOLICITUD = d.IC_SOLICITUD\n" + 
                "   AND d.IC_TIPO_DOCUMENTO = cd.IC_TIPO_DOCUMENTO\n" + 
                "   AND d.IC_MOVIMIENTO = cm.IC_MOVIMIENTO\n" + 
                "   AND s.IC_PORTAFOLIO = cp.IC_PORTAFOLIO\n" + 
                "   AND GI.IC_ESTATUS = e.IC_ESTATUS\n" + 
                "   AND GI.IC_DOCUMENTO = d.ic_documento\n" + 
                "   AND GI.IC_IF = :s_intermediario \n" + 
                  sqlFltros+
                " ORDER BY D.IC_DOCUMENTO DESC";
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("s_folioSolicitud") || key.equals("s_tipoMovimiento")  || key.equals("s_estatus") || key.equals("s_intermediario") 
                    || key.equals("s_documento") || key.equals("s_portafolio")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    stmt.setChar(key, entry.getValue());
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            String intermediario = filtrosBusqueda.get("s_intermediario");
            
            
            while(rs.next()){
                InfoSolicitud info = new InfoSolicitud();
                info.setEstatus(rs.getString("Cd_Nombre_Estatus"));
                info.setFechaSolicitud(rs.getString("FECHA_CREACION"));
                info.setFolioSolicitud(rs.getString("FOLIO"));
                info.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
                info.setNombreProductoEmpresa(rs.getString("TG_NOMBRE"));
                info.setPortafolio(rs.getString("Cd_Nombre_Portafolio"));
                info.setTipoDocumento(rs.getString("Cd_Nombre_Documento"));
                info.setTipoMovimiento(rs.getString("Cd_Nombre_Movimiento"));
                info.setIcDocumento(rs.getInt("ic_documento"));
                Usuario ejecutivo = utilUsr.getUsuario(rs.getString("IC_USUARIO_EJECUTIVO"));
                info.setUsuarioEjecutivo(ejecutivo.getNombreCompleto());
                Usuario solicitante = utilUsr.getUsuario(rs.getString("IC_USUARIO_REGISTRO"));
                info.setUsuarioSolicitante(solicitante.getNombreCompleto());
                info.setVersion(rs.getString("IC_VERSION"));
                info.setIcArchivoBO(rs.getInt("icArchivoBO"));
                String firmantesIF = rs.getString("usuariosFirIF");
                ArrayList<ElementoCatalogo> firmantesRequeridos = new ArrayList<>();
                if(firmantesIF != null &&!firmantesIF.equals("")){
                
                String arrFirmantes[] = firmantesIF.trim().split("\\|");
                
                for(String firma : arrFirmantes){
                    
                    String[] arrFirmas = firma.split(",");
                    
                    Usuario usuarillo = utilUsr.getUsuario(arrFirmas[0]);
                    firmantesRequeridos.add(new ElementoCatalogo(arrFirmas[0], usuarillo.getNombreCompleto()+" "+arrFirmas[1]));
                }
                
                }
                info.setFirmantes(firmantesRequeridos);
                // Calculo de fecha maxima de formalizacion 
                //int diasLimiteFormalizacion = rs.getInt("diasLimiteFormalizacion");
                //String fechaInicial = rs.getString("FECHA_LIBERACION");
                //String fechaMaxFormalizacion = Fecha.sumaFechaDiasHabiles(fechaInicial, "dd/MM/yyyy", diasLimiteFormalizacion );
                //info.setFechaMaximaFormalizacion(fechaMaxFormalizacion);
                info.setFechaMaximaFormalizacion(rs.getString("fechaMax1"));
                String totalFirmas = rs.getString("totalFirmasRequeridas");
                String noFirmantes = rs.getString("firmasRegistradas");    
                info.setNumeroFirmantes(noFirmantes + " de "+ totalFirmas);
                info.setFechaFormalizacion("--");
                if(totalFirmas.equals(noFirmantes)){
                    info.setFechaFormalizacion(rs.getString("fecha_formalizacion"));
                }
                
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
    private String obtenFirmantes(AccesoDB con, String s_intermediario, String ic_documento){
        ResultSet rs2 = null;
        NamedParameterStatement stmt2;
        String sql2 = "";
        String nombreFirma = "";
        try{
            
            sql2 = "SELECT IC_USUARIO, to_char(DT_FECHA_FIRMA,'dd/mm/yyyy hh:mi') fechaFirma "+
               " FROM GDOC_ACUSE_FORMALIZACION "+
             " WHERE IC_IF = :s_intermediario "+
               " and IC_DOCUMENTO = :ic_documento ";
                            
            stmt2 = new NamedParameterStatement(con.getConnection(), sql2, QUERYTYPE.QUERY);
            Integer entero = Integer.parseInt(s_intermediario);
            stmt2.setInt("s_intermediario", entero);
            entero = Integer.parseInt(ic_documento);
            stmt2.setInt("ic_documento", entero);
            rs2 = stmt2.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            while(rs2.next()){
                Usuario usuario = utilUsr.getUsuario(rs2.getString("IC_USUARIO"));
                nombreFirma += usuario.getNombreCompleto()+ "  "+rs2.getString("fechaFirma")+" ";
            }
            stmt2.close();
            return nombreFirma;
            
        }catch (Throwable e) {
                       log.error(e.getMessage());
                       e.printStackTrace();
                   }
                   finally{
                       AccesoDB.cerrarResultSet(rs2);                         
                   }        
        return nombreFirma;
    }
    
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes(String intermediario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" select s.ic_Solicitud, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO from GDOC_SOLICITUD s   ");
            strSQL.append( " where s.IC_SOLICITUD IN " +
                    " (select k.IC_SOLICITUD from gdoc_solicitud k, gdocrel_solicitud_if j where j.ic_solicitud = k.ic_solicitud AND j.ic_if = ?)");
            
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(intermediario));
            
            
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("ic_Solicitud");
                String descripcion = rs.getString("FOLIO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    public Integer getArchivoIF(String intermediario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        Integer ic_archivo = 0;
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_ARCHIVO FROM GDOC_PARAMETRO_IF WHERE IC_IF = ?   ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(intermediario));
            rs = ps.executeQuery();
            
            while (rs.next()){
                ic_archivo = rs.getInt("IC_ARCHIVO");
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return ic_archivo;
    }
    
    
}
