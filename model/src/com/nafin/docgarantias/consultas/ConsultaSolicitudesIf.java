package com.nafin.docgarantias.consultas;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface ConsultaSolicitudesIf {
    
    public ArrayList<InfoSolicitud> getInfoSolicitudes(Map<String,String> filtrosBusqueda);
    public ArrayList<ElementoCatalogo> getCatFolioSolicitudes(String intermediario);
    public Integer getArchivoIF(String intermediario);
    
}
