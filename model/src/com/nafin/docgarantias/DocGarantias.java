package com.nafin.docgarantias;


import java.util.ArrayList;

import javax.ejb.Remote;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface DocGarantias {
    
    
    public ArrayList<MovimientoLinea> getMonitorLineas(ArrayList<String> param);
    
    public ArrayList<ElementoCatalogo> getIFs(String portafolio, Integer idProyecto);
    
    public ArrayList<ElementoCatalogo> getFechaVigenciaMonitorLineas(Integer idIF, String portafolio, Integer idProyecto);
    
    public ArrayList<ElementoCatalogo> getNombreProductosMonitor(Integer idIF, String portafolio);
    
    public ArrayList<ElementoCatalogo> getPortafolioMonitor(Integer idIF);
    
    public ArrayList<ElementoCatalogo> getBaseOperacionesMonitor(String portafolio, Integer idProyecto, Integer idIF, String fechaVigencia);
    
    public String getMonitorPDF_XLS(HttpServletRequest request, String path, ArrayList<String> param, boolean esIF, String tipoArchivo);
    
    public String getDescripcionProducto(Integer idProyecto);
    
    public String getDescripcionIF(Integer idIF);
    
    public String getDescripcionPortafolio(Integer idCartera, Integer idPortafolio);
    
    public Integer getClaveIF_SIAG(Integer ic_if);
    
    }
