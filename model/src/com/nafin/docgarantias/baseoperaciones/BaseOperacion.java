package com.nafin.docgarantias.baseoperaciones;


import java.util.ArrayList;
import java.util.HashMap;

import javax.ejb.Remote;

@Remote
public interface BaseOperacion {
    
    public ArrayList<DocBaseOperacion> getDocsBaseOperacion();
    public ArrayList<DocBaseOperacion> getDocsBaseOperacionPorValidar();
    public boolean insertarBO(HashMap<String, String> parametros) ;
    public boolean avanzaEstatusDocto(Integer icDoc, Integer icIF, String usuario, Integer icEstatus);
    public boolean avanzaEstatusBO(Integer icDoc, Integer icIF, String usuario, Integer icEstatus);
    public boolean setNoAplicaBO(Integer icDoc, Integer icIF, String usuario);
    public boolean eliminarBO(Integer icDoc, Integer icIF, String usuario);
    public boolean rechazarBO(Integer icDoc, Integer icIF, String usuario, String motivoRechazo);  
    
    
}
