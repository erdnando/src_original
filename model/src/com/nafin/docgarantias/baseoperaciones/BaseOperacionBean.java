package com.nafin.docgarantias.baseoperaciones;


import com.nafin.docgarantias.Files2OnBase;
import com.nafin.docgarantias.FlujoDocumentos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "BaseOperacion", mappedName = "BaseOperacion")
@TransactionManagement(TransactionManagementType.BEAN)
public class BaseOperacionBean implements BaseOperacion {
    
    @Resource
    SessionContext sessionContext;

    public static final Integer ESTATUSBO_NO_APLICA = -1;
    public static final Integer ESTATUSBO_SIN_BASE_OPERACION = 0;
    public static final Integer ESTATUSBO_POR_VALIDAR_BO  = 1;
    public static final Integer ESTATUSBO_BO_VALIDADA_NAFIN = 2;
    public static final Integer ESTATUSBO_BO_OPERANDO = 3;
    
    
    public static final String PARAM_IC_DOC  = "icDocumento";
    public static final String PARAM_IC_IF ="icIF";
    public static final String PARAM_ESTATUS_BO = "icEstatusBO";
    public static final String PARAM_IC_USUARIO = "icUsuario";
    public static final String PARAM_IC_ARCHIVO = "icArchivo";
    
    
    private static final Log log = ServiceLocator.getInstance().getLog(BaseOperacionBean.class);
    
    public BaseOperacionBean() {
        super();
    }
    
    @Override
    public ArrayList<DocBaseOperacion> getDocsBaseOperacion() {
        ArrayList<DocBaseOperacion> listaDoc = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = null;
        try {
            con.conexionDB();
            strSQL = " SELECT di.IC_DOCUMENTO, d.ic_archivo, di.IC_IF, s.ic_solicitud, \n" + 
            " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM GDOC_BITACORA_DOC WHERE IC_ESTATUS = 175 and ic_documento = di.ic_documento) as fecha_Liberacion,\n" + 
            " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM GDOC_BITACORA_DOC WHERE IC_ESTATUS = 175 and ic_documento = di.ic_documento) as fecha_FORMALIZACION,\n" + 
            " (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF WHERE IC_IF = di.IC_IF) as nombreIF,\n" + 
            " s.IC_USUARIO_EJECUTIVO, s.IC_USUARIO_REGISTRO, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO,\n" + 
            " cd.CD_NOMBRE_DOCUMENTO, d.TG_NOMBRE, cp.CD_NOMBRE_PORTAFOLIO, " +
            " NVL((SELECT IC_ESTATUS FROM GDOC_BASE_OPERACION where IC_DOCUMENTO = d.IC_DOCUMENTO AND IC_IF = di.IC_IF), 0) AS EstatusBO, \n" + 
            " (SELECT IC_ARCHIVO FROM GDOC_BASE_OPERACION where IC_DOCUMENTO = d.IC_DOCUMENTO AND IC_IF = di.IC_IF) AS icArchivoBO \n"+
            " FROM GDOCREL_DOCUMENTO_IF di, GDOC_SOLICITUD s, GDOC_DOCUMENTO d, GDOCCAT_DOCUMENTO cd, GDOCCAT_PORTAFOLIO cp\n" + 
            " WHERE di.ic_documento = d.ic_documento and d.ic_solicitud = S.Ic_Solicitud and cd.ic_tipo_documento = d.ic_tipo_documento AND s.ic_portafolio = cp.ic_portafolio\n" + 
            " and  di.IC_ESTATUS  >170 and s.IC_TIPO_DOCUMENTO IN (1004,1005,1007) ORDER BY s.ic_solicitud DESC  ";
            
            log.info("SQL DocsBaseOperacion: "+strSQL);
            ps = con.queryPrecompilado(strSQL); 
            rs = ps.executeQuery();
            while (rs.next()){
                DocBaseOperacion docBO = new DocBaseOperacion();
                docBO.setEstatusBO(rs.getString("EstatusBO"));
                docBO.setFechaFormalizacion(rs.getString("fecha_Liberacion"));
                docBO.setFechaFormalizacion(rs.getString("fecha_FORMALIZACION"));
                docBO.setFolio(rs.getString("FOLIO"));
                docBO.setIcArchivo(rs.getInt("ic_archivo"));
                docBO.setIcDocumento(rs.getInt("ic_documento"));
                docBO.setIcSolicitud(rs.getInt("ic_solicitud"));
                docBO.setIcIF(rs.getInt("IC_IF"));
                docBO.setNombreIF(rs.getString("nombreIF"));
                UtilUsr utilUsr = new UtilUsr();
                Usuario ejecutivo = utilUsr.getUsuario(rs.getString("IC_USUARIO_EJECUTIVO"));
                docBO.setNombreUsuarioEjecutivo(ejecutivo.getNombreCompleto());
                Usuario solicitante = utilUsr.getUsuario(rs.getString("IC_USUARIO_REGISTRO"));
                docBO.setNombreUsuarioSolicito(solicitante.getNombreCompleto());
                docBO.setNombreProducto(rs.getString("TG_NOMBRE"));
                docBO.setPortafolio(rs.getString("CD_NOMBRE_PORTAFOLIO"));
                docBO.setTipoDocumento(rs.getString("CD_NOMBRE_DOCUMENTO"));
                docBO.setIcArchivoBO(rs.getInt("icArchivoBO"));
                listaDoc.add(docBO);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en getDocsBaseOperacion() ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return listaDoc;   
    }


    @Override
    public ArrayList<DocBaseOperacion> getDocsBaseOperacionPorValidar () {
        ArrayList<DocBaseOperacion> listaDoc = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = null;
        try {
            con.conexionDB();
            strSQL = " SELECT di.IC_DOCUMENTO, d.ic_archivo, di.IC_IF, s.ic_solicitud, \n" + 
            " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM GDOC_BITACORA_DOC WHERE IC_ESTATUS = 180 and ic_documento = di.ic_documento AND di.IC_IF = IC_IF) as fecha_Liberacion,\n" + 
            " (SELECT TO_CHAR(MAX(DT_FECHA_MOVIMIENTO),'DD/MM/YYYY') FROM GDOC_BITACORA_DOC WHERE IC_ESTATUS = 175 and ic_documento = di.ic_documento AND di.IC_IF = IC_IF) as fecha_FORMALIZACION,\n" + 
            " (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF WHERE IC_IF = di.IC_IF) as nombreIF,\n" + 
            " s.IC_USUARIO_EJECUTIVO, s.IC_USUARIO_REGISTRO, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO,\n" + 
            " cd.CD_NOMBRE_DOCUMENTO, d.TG_NOMBRE, cp.CD_NOMBRE_PORTAFOLIO, " +
            " NVL((SELECT IC_ESTATUS FROM GDOC_BASE_OPERACION where IC_DOCUMENTO = d.IC_DOCUMENTO AND IC_IF = di.IC_IF), 0) AS EstatusBO, \n" + 
            " (SELECT IC_ARCHIVO FROM GDOC_BASE_OPERACION where IC_DOCUMENTO = d.IC_DOCUMENTO AND IC_IF = di.IC_IF) AS icArchivoBO \n"+
            " FROM GDOCREL_DOCUMENTO_IF di, GDOC_SOLICITUD s, GDOC_DOCUMENTO d, GDOCCAT_DOCUMENTO cd, GDOCCAT_PORTAFOLIO cp\n" + 
            " WHERE di.ic_documento = d.ic_documento and d.ic_solicitud = S.Ic_Solicitud and cd.ic_tipo_documento = d.ic_tipo_documento AND s.ic_portafolio = cp.ic_portafolio\n" + 
            " and  di.IC_ESTATUS  = 180 and s.IC_TIPO_DOCUMENTO IN (1004,1005,1007) ORDER BY s.ic_solicitud DESC ";
            
            log.info("SQL DocsBaseOperacionPorValidar: "+strSQL);
            ps = con.queryPrecompilado(strSQL); 
            rs = ps.executeQuery();
            while (rs.next()){
                DocBaseOperacion docBO = new DocBaseOperacion();
                docBO.setEstatusBO(rs.getString("EstatusBO"));
                docBO.setFechaFormalizacion(rs.getString("fecha_Liberacion"));
                docBO.setFechaFormalizacion(rs.getString("fecha_FORMALIZACION"));
                docBO.setFolio(rs.getString("FOLIO"));
                docBO.setIcArchivo(rs.getInt("ic_archivo"));
                docBO.setIcDocumento(rs.getInt("ic_documento"));
                docBO.setIcSolicitud(rs.getInt("ic_solicitud"));
                docBO.setIcIF(rs.getInt("IC_IF"));
                docBO.setNombreIF(rs.getString("nombreIF"));
                UtilUsr utilUsr = new UtilUsr();
                Usuario ejecutivo = utilUsr.getUsuario(rs.getString("IC_USUARIO_EJECUTIVO"));
                docBO.setNombreUsuarioEjecutivo(ejecutivo.getNombreCompleto());
                Usuario solicitante = utilUsr.getUsuario(rs.getString("IC_USUARIO_REGISTRO"));
                docBO.setNombreUsuarioSolicito(solicitante.getNombreCompleto());
                docBO.setNombreProducto(rs.getString("TG_NOMBRE"));
                docBO.setPortafolio(rs.getString("CD_NOMBRE_PORTAFOLIO"));
                docBO.setTipoDocumento(rs.getString("CD_NOMBRE_DOCUMENTO"));
                docBO.setIcArchivoBO(rs.getInt("icArchivoBO"));
                listaDoc.add(docBO);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en getDocsBaseOperacion() ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return listaDoc;   
    }


    public boolean insertarBO(HashMap<String, String> parametros) {
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        Integer icDoc = Integer.valueOf(parametros.get(PARAM_IC_DOC));
        Integer icIF = Integer.valueOf(parametros.get(PARAM_IC_IF));
        Integer estatus = Integer.valueOf(parametros.get(PARAM_ESTATUS_BO));
        Integer icArchivo = null;
        if(parametros.get(PARAM_IC_ARCHIVO) != null){
            icArchivo = Integer.valueOf(parametros.get(PARAM_IC_ARCHIVO));
        }
        String icUsuario = parametros.get(PARAM_IC_USUARIO);
        String sqlInsert = null;
        try {
            con.conexionDB();
            sqlInsert =  " INSERT  INTO GDOC_BASE_OPERACION (IC_DOCUMENTO,IC_IF,USUARIO_CARGA,FECHA_CARGA,IC_ARCHIVO,IC_ESTATUS) \n" + 
                "    VALUES  (?, ?, ?, SYSDATE, ?, ? ) ";
            ps = con.queryPrecompilado(sqlInsert);
            ps.setInt(1, icDoc);
            ps.setInt(2, icIF);
            ps.setString(3, icUsuario);
            if (icArchivo != null){
                ps.setInt(4, icArchivo);
            }
            else{
                ps.setNull(4, Types.INTEGER);
            }
            ps.setInt(5, estatus);
            ps.executeUpdate();
            exito = true;
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en insertarBO() ", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return exito;
    }


    @Override
    public boolean eliminarBO(Integer icDoc, Integer icIF, String usuario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean exito = false;
        Integer icArchivo = null;
        String sqlUpdate = " SELECT ic_archivo FROM GDOC_BASE_OPERACION where ic_documento = ? and ic_if = ?  ";
        try {
            con.conexionDB();
            ps = con.queryPrecompilado(sqlUpdate);
            ps.setInt(1, icDoc);
            ps.setInt(2, icIF);
            rs = ps.executeQuery();
            while(rs.next()){
                icArchivo = rs.getInt("ic_archivo");
            }
            String sqlDelete = " DELETE FROM GDOC_BASE_OPERACION where ic_documento = ? and ic_if = ? ";
            ps = con.queryPrecompilado(sqlDelete);
            ps.setInt(1, icDoc);
            ps.setInt(2, icIF);
            ps.executeUpdate();
            if (icArchivo != null){
                Files2OnBase onBaseBean =    ServiceLocator.getInstance().lookup("Files2OnBase", Files2OnBase.class);
                onBaseBean.eliminaArchivoDocumento(icArchivo);
                exito = true;
            }
            else{
                log.info("No se encontro icArchivo en GDOC_BASE_OPERACION para icDoc="+icDoc+", icIF="+icIF);    
            }            
            exito = true;
        } 
        catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en eliminarBO() ", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return exito;
    }

    @Override
    public boolean avanzaEstatusDocto(Integer icDoc, Integer icIF, String usuario, Integer icEstatus) {
        boolean exito = false;
        FlujoDocumentos flujoBean =    ServiceLocator.getInstance().lookup("FlujoDocumentos", FlujoDocumentos.class);
        exito = flujoBean.avanzarDocumentoCargaBO(icDoc, usuario, icIF);
        if(exito){
            exito = actualizarEstatusBO(icDoc, icIF, icEstatus);
        }
        return exito;
    }

    @Override
    public boolean avanzaEstatusBO(Integer icDoc, Integer icIF, String usuario, Integer icEstatus) {
        return actualizarEstatusBO(icDoc, icIF, icEstatus);
    }

    private boolean actualizarEstatusBO(Integer icDoc, Integer icIF, Integer icEstatus){
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String sqlUpdate = " UPDATE GDOC_BASE_OPERACION SET IC_ESTATUS = ? where ic_documento = ? and ic_if = ?  ";
        try {
            con.conexionDB();
            ps = con.queryPrecompilado(sqlUpdate);
            ps.setInt(1, icEstatus);
            ps.setInt(2, icDoc);
            ps.setInt(3, icIF);
            ps.executeUpdate();
            exito = true;
        } 
        catch (SQLException | NamingException e) {
            exito = false;
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en avanzaEstatusBO() ", e);
        } finally {
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return exito;
    }

    @Override
    public boolean setNoAplicaBO(Integer icDoc, Integer icIF, String usuario) {
        boolean exito = false;        
        FlujoDocumentos flujoBean =    ServiceLocator.getInstance().lookup("FlujoDocumentos", FlujoDocumentos.class);
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_ESTATUS_BO, ESTATUSBO_NO_APLICA+"");
        params.put(PARAM_IC_ARCHIVO, null);
        params.put(PARAM_IC_DOC, icDoc+"");
        params.put(PARAM_IC_IF, icIF+"");
        params.put(PARAM_IC_USUARIO, usuario);
        exito = flujoBean.avanzarDocumentoNoAplicaCargaBO(icDoc, usuario, icIF);
        if (exito){
            exito = insertarBO(params);
        }
        return exito;
    }

    @Override
    public boolean rechazarBO(Integer icDoc, Integer icIF, String usuario, String motivoRechazo) {
        boolean exito =  false;
        FlujoDocumentos flujoBean =    ServiceLocator.getInstance().lookup("FlujoDocumentos", FlujoDocumentos.class);
        exito = flujoBean.rechazarDocumentoCargaBO(icDoc, usuario, icIF, motivoRechazo);
        if (exito){
            actualizarEstatusBO(icDoc, icIF, ESTATUSBO_SIN_BASE_OPERACION);
        }
        return exito;
    }
    

}
