package com.nafin.docgarantias.baseoperaciones;


import java.io.Serializable;

public class DocBaseOperacion implements Serializable {
    @SuppressWarnings("compatibility:731642435123431239")
    private static final long serialVersionUID = 1L;

    private Integer icDocumento;
    private Integer icArchivo;
    private Integer icArchivoBO;
    private Integer icSolicitud;
    private String icUsuarioCargo;
    private Integer icIF;
    private String nombreUsuarioEjecutivo;
    private String nombreUsuarioSolicito;
    private String estatusBO;
    private String fechaLiberacion;
    private String fechaFormalizacion;
    private String nombreIF;
    private String folio;
    private String nombreProducto;
    private String portafolio;
    private String tipoDocumento;

    public DocBaseOperacion() {
        super();
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchivo) {
        this.icArchivo = icArchivo;
    }

    public String getNombreUsuarioEjecutivo() {
        return nombreUsuarioEjecutivo;
    }

    public void setNombreUsuarioEjecutivo(String nombreUsuarioEjecutivo) {
        this.nombreUsuarioEjecutivo = nombreUsuarioEjecutivo;
    }

    public String getNombreUsuarioSolicito() {
        return nombreUsuarioSolicito;
    }

    public void setNombreUsuarioSolicito(String nombreUsuarioSolicito) {
        this.nombreUsuarioSolicito = nombreUsuarioSolicito;
    }

    public String getEstatusBO() {
        return estatusBO;
    }

    public void setEstatusBO(String estatusBO) {
        this.estatusBO = estatusBO;
    }

    public String getFechaLiberacion() {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(String fechaLiberacion) {
        this.fechaLiberacion = fechaLiberacion;
    }

    public String getFechaFormalizacion() {
        return fechaFormalizacion;
    }

    public void setFechaFormalizacion(String fechaFormalizacion) {
        this.fechaFormalizacion = fechaFormalizacion;
    }

    public String getNombreIF() {
        return nombreIF;
    }

    public void setNombreIF(String nombreIF) {
        this.nombreIF = nombreIF;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public Integer getIcArchivoBO() {
        return icArchivoBO;
    }

    public void setIcArchivoBO(Integer icArchivoBO) {
        this.icArchivoBO = icArchivoBO;
    }

    public Integer getIcSolicitud() {
        return icSolicitud;
    }

    public void setIcSolicitud(Integer icSolicitud) {
        this.icSolicitud = icSolicitud;
    }

    public String getIcUsuarioCargo() {
        return icUsuarioCargo;
    }

    public void setIcUsuarioCargo(String icUsuarioCargo) {
        this.icUsuarioCargo = icUsuarioCargo;
    }
}
