package com.nafin.docgarantias.subastas;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class InfoSubastas implements Serializable{
    @SuppressWarnings("compatibility:7038515887990938465")
    private static final long serialVersionUID = 4997638023806879959L;
    
    private String icConvocatoria;
    private String fechaPublicacion;
    private String nombreSubasta;
    private String tipoSubasta;
    private String fechaLimite;
    private String basesPublicadas;
    private String PosturasRecibidas;
    private String estatus;
    private String fechaAlta;
    private String fechaMaximaRecep;
    private String tipoIntermediario;
    private String baseSubasta;
    private String intermediarioFin;
    private String usuarioIntFin;
    private String fechaEnvioResultados;
    private String convocatoria;
    private String observaciones;
    private String resultados;
    private String fechaRecepResultados;
    private List<Integer> sintermediarios;
    private Integer idArchivo;
    private ArrayList<Integer> sArchivos;
    private String idIntermediario;
    private Integer idArchivoCalif;
    private Integer idArchivoRes;
    private String tagComentario;
    private String fechaSolventar;
    private String fechaCarga;
        
    public InfoSubastas() {
        super();
    }


    public void setIcConvocatoria(String icConvocatoria) {
        this.icConvocatoria = icConvocatoria;
    }

    public String getIcConvocatoria() {
        return icConvocatoria;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setNombreSubasta(String nombreSubasta) {
        this.nombreSubasta = nombreSubasta;
    }

    public String getNombreSubasta() {
        return nombreSubasta;
    }

    public void setTipoSubasta(String tipoSubasta) {
        this.tipoSubasta = tipoSubasta;
    }

    public String getTipoSubasta() {
        return tipoSubasta;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setBasesPublicadas(String basesPublicadas) {
        this.basesPublicadas = basesPublicadas;
    }

    public String getBasesPublicadas() {
        return basesPublicadas;
    }

    public void setPosturasRecibidas(String PosturasRecibidas) {
        this.PosturasRecibidas = PosturasRecibidas;
    }

    public String getPosturasRecibidas() {
        return PosturasRecibidas;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaMaximaRecep(String fechaMaximaRecep) {
        this.fechaMaximaRecep = fechaMaximaRecep;
    }

    public String getFechaMaximaRecep() {
        return fechaMaximaRecep;
    }

    public void setTipoIntermediario(String tipoIntermediario) {
        this.tipoIntermediario = tipoIntermediario;
    }

    public String getTipoIntermediario() {
        return tipoIntermediario;
    }

    public void setBaseSubasta(String baseSubasta) {
        this.baseSubasta = baseSubasta;
    }

    public String getBaseSubasta() {
        return baseSubasta;
    }

    public void setIntermediarioFin(String intermediarioFin) {
        this.intermediarioFin = intermediarioFin;
    }

    public String getIntermediarioFin() {
        return intermediarioFin;
    }

    public void setUsuarioIntFin(String usuarioIntFin) {
        this.usuarioIntFin = usuarioIntFin;
    }

    public String getUsuarioIntFin() {
        return usuarioIntFin;
    }

    public void setFechaEnvioResultados(String fechaEnvioResultados) {
        this.fechaEnvioResultados = fechaEnvioResultados;
    }

    public String getFechaEnvioResultados() {
        return fechaEnvioResultados;
    }

    public void setConvocatoria(String convocatoria) {
        this.convocatoria = convocatoria;
    }

    public String getConvocatoria() {
        return convocatoria;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setResultados(String resultados) {
        this.resultados = resultados;
    }

    public String getResultados() {
        return resultados;
    }

    public void setFechaRecepResultados(String fechaRecepResultados) {
        this.fechaRecepResultados = fechaRecepResultados;
    }

    public String getFechaRecepResultados() {
        return fechaRecepResultados;
    }

    public void setSintermediarios(List<Integer> sintermediarios) {
        this.sintermediarios = sintermediarios;
    }

    public List<Integer> getSintermediarios() {
        return sintermediarios;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setSArchivos(ArrayList<Integer> sArchivos) {
        this.sArchivos = sArchivos;
    }

    public ArrayList<Integer> getSArchivos() {
        return sArchivos;
    }

    public void setIdIntermediario(String idIntermediario) {
        this.idIntermediario = idIntermediario;
    }

    public String getIdIntermediario() {
        return idIntermediario;
    }

    public void setIdArchivoCalif(Integer idArchivoCalif) {
        this.idArchivoCalif = idArchivoCalif;
    }

    public Integer getIdArchivoCalif() {
        return idArchivoCalif;
    }

    public void setTagComentario(String tagComentario) {
        this.tagComentario = tagComentario;
    }

    public String getTagComentario() {
        return tagComentario;
    }

    public void setFechaSolventar(String fechaSolventar) {
        this.fechaSolventar = fechaSolventar;
    }

    public String getFechaSolventar() {
        return fechaSolventar;
    }

    public void setIdArchivoRes(Integer idArchivoRes) {
        this.idArchivoRes = idArchivoRes;
    }

    public Integer getIdArchivoRes() {
        return idArchivoRes;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

}
