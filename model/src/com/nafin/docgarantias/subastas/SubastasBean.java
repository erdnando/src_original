package com.nafin.docgarantias.subastas;

import com.nafin.docgarantias.Files2OnBase;
import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "SubastasEJB", mappedName = "SubastasEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class SubastasBean implements Subastas {
    @Resource
    SessionContext sessionContext;


    private static final Log log = ServiceLocator.getInstance().getLog(SubastasBean.class);
    

    public SubastasBean() {
        super();
    }
    
    public ArrayList<InfoSubastas> getInfoSubastas(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        ResultSet rs2 = null;
        NamedParameterStatement stmt;
        NamedParameterStatement stmt2;
        ArrayList<InfoSubastas> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("nombreSubasta") != null) sqlFltros += " AND  c.CT_NOMBRE_SUBASTA like :nombreSubasta ";
        if (filtrosBusqueda.get("tipoSubasta") != null)  sqlFltros += " AND c.IC_TIPO_SUBASTA = :tipoSubasta ";
        if (filtrosBusqueda.get("estatusSubasta") != null) sqlFltros += " AND c.CD_ESTATUS = :estatusSubasta ";
        if (filtrosBusqueda.get("fechaAlta") != null) sqlFltros += " AND trunc(c.DT_FECHA_ALTA) = TO_DATE(:fechaAlta, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("fechaLimite") != null) sqlFltros += " AND trunc(c.DT_FECHA_LIMITE_RECEPCION) = TO_DATE(:fechaLimite,'DD/MM/YYYY') ";
        
        try {
            con.conexionDB();

            String sql;
            String sql2;
            sql = "SELECT IC_CONVOCATORIA, " +
            "       TO_CHAR(C.DT_FECHA_ALTA,'DD/MM/YYYY') DT_FECHA_ALTA,\n" + 
            "       C.CT_NOMBRE_SUBASTA,\n" + 
            "       (select t.cd_nombre_subasta from gdoccat_subasta t where t.ic_tipo_subasta = C.IC_TIPO_SUBASTA) TIPO_SUBASTA,\n" + 
            "       TO_CHAR(C.DT_FECHA_LIMITE_RECEPCION,'DD/MM/YYYY') DT_FECHA_LIMITE_RECEPCION, \n" + 
            "       C.CD_ESTATUS,\n" +
            "       NVL(C.IC_ARCHIVO,0) IC_ARCHIVO, \n"+
            "       (select count(cf.ic_if)\n" + 
            "  from gdocrel_convocatoria_if cf  \n" + 
            " where cf.ic_convocatoria = c.ic_convocatoria) noIf \n"+
            "FROM GDOC_CONVOCATORIA C\n" +
            " WHERE C.IC_CONVOCATORIA IS NOT NULL "+      
                  sqlFltros+
                " ORDER BY C.IC_CONVOCATORIA DESC";
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("tipoSubasta")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    if(key.equals("nombreSubasta")){
                        stmt.setString(key, '%'+entry.getValue()+'%');
                        log.info("%"+entry.getValue()+"%");
                    }
                    stmt.setChar(key, entry.getValue());
                }
                log.info(sql);
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            int countPosturas = 0;
            
            while(rs.next()){
                InfoSubastas info = new InfoSubastas();
                info.setIcConvocatoria(rs.getString("IC_CONVOCATORIA"));
                info.setFechaAlta(rs.getString("DT_FECHA_ALTA"));
                info.setNombreSubasta(rs.getString("CT_NOMBRE_SUBASTA"));
                info.setTipoSubasta(rs.getString("TIPO_SUBASTA"));
                info.setFechaRecepResultados(rs.getString("DT_FECHA_LIMITE_RECEPCION"));
                info.setEstatus(rs.getString("CD_ESTATUS"));
                countPosturas = 0;
                sql2 = " select distinct ic_if " +
                    "  from gdoc_postura t where " +
                    "  t.ic_convocatoria = "+info.getIcConvocatoria();
                stmt2 = new NamedParameterStatement(con.getConnection(), sql2, QUERYTYPE.QUERY);
                rs2 = stmt2.executeQuery();
                
                while(rs2.next()){
                    countPosturas++;
                }
                
                info.setPosturasRecibidas(countPosturas+" de "+rs.getString("noIf"));
                info.setIdArchivo(Integer.parseInt(rs.getString("IC_ARCHIVO")));
                rs2.close();
                stmt2.close();
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarResultSet(rs2);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
    
    public ArrayList<InfoSubastas> getInfoSeguimiento(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        
        ArrayList<InfoSubastas> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("nombreSubasta") != null) sqlFltros += " AND  c.CT_NOMBRE_SUBASTA like :nombreSubasta ";
        if (filtrosBusqueda.get("tipoSubasta") != null)  sqlFltros += " AND c.IC_TIPO_SUBASTA = :tipoSubasta ";
        if (filtrosBusqueda.get("estatusSubasta") != null) sqlFltros += " AND c.CD_ESTATUS = :estatusSubasta ";
        if (filtrosBusqueda.get("interFinanciero") != null) sqlFltros += " AND P.IC_IF = :interFinanciero ";
        
        
        
        
        UtilUsr utilUsr = new UtilUsr();
        try {
            con.conexionDB();

            String sql;
            sql = "SELECT TO_CHAR(C.DT_FECHA_ALTA,'DD/MM/YYYY')             FECHA_ENVIO,\n" + 
            "       P.IC_IF                     NO_INTER,\n" +
            "       I.CG_RAZON_SOCIAL           INTERMEDIARIO,\n"+
            "       TO_CHAR(C.DT_FECHA_LIMITE_RECEPCION,'DD/MM/YYYY') FECHA_LIMITE,\n" + 
            "       --POSTURAS,\n" + 
            "       P.IC_USUARIO_CARGA   USUARIO_INTER,\n" + 
            "       TO_CHAR(P.DT_FECHA_SOLVENTAR, 'DD/MM/YYYY') FECHA_LIMITE_SOL,\n" +
            "       C.CD_ESTATUS         ESTATUS,"+
            "       C.CT_NOMBRE_SUBASTA         NOMBRE, "+      
            "       C.IC_CONVOCATORIA, "+
            "       NVL(P.IC_ARCHIVO_CALIF,0) ARCHIVO_CALIF, \n"+
            "       nvl(R.IC_ARCHIVO , 0) ARCHIVORES \n"+      
            "  FROM GDOC_CONVOCATORIA C\n" + 
            " INNER JOIN GDOC_POSTURA P\n" + 
            "    ON C.IC_CONVOCATORIA = P.IC_CONVOCATORIA\n" + 
            " LEFT JOIN GDOC_RESULTADO R\n" + 
            "    ON R.IC_CONVOCATORIA = P.IC_CONVOCATORIA\n" + 
            "   AND R.IC_IF = P.IC_IF "+
            " INNER JOIN COMCAT_IF I\n" + 
            "          ON P.IC_IF = I.ic_if  "+      
                  
                  
                  sqlFltros+
                " ORDER BY C.IC_CONVOCATORIA DESC";
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("tipoSubasta")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    if(key.equals("nombreSubasta")){
                        stmt.setString(key, '%'+entry.getValue()+'%');
                    }
                    stmt.setChar(key, entry.getValue());
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            
            while(rs.next()){
                InfoSubastas info = new InfoSubastas();
                info.setIcConvocatoria(rs.getString("IC_CONVOCATORIA"));
                info.setFechaAlta(rs.getString("FECHA_ENVIO"));
                info.setIntermediarioFin(rs.getString("INTERMEDIARIO"));
                info.setFechaRecepResultados(rs.getString("FECHA_LIMITE"));
                
                info.setUsuarioIntFin(rs.getString("USUARIO_INTER"));
                if(info.getUsuarioIntFin()!=null&&!info.getUsuarioIntFin().equals("")){
                    Usuario usuario = utilUsr.getUsuario(info.getUsuarioIntFin());
                    info.setUsuarioIntFin(usuario.getNombreCompleto());
                }                
                info.setFechaLimite(rs.getString("FECHA_LIMITE_SOL"));
                info.setEstatus(rs.getString("ESTATUS"));
                info.setNombreSubasta(rs.getString("NOMBRE"));
                info.setIdIntermediario(rs.getString("NO_INTER"));
                info.setIdArchivoCalif(rs.getInt("ARCHIVO_CALIF"));
                info.setIdArchivoRes(rs.getInt("ARCHIVORES"));
                
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
    public ArrayList<InfoSubastas> getInfoSeguimientoIf(Map<String,String> filtrosBusqueda) {
        log.info("Elementos enviados para FILTRAR: "+filtrosBusqueda.size());
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info(entry.getKey()+" - "+entry.getValue());    
        }
        ResultSet rs = null;
        NamedParameterStatement stmt;
        
        ArrayList<InfoSubastas> retorno = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        String sqlFltros = "";
        if (filtrosBusqueda.get("nombreSubasta") != null) sqlFltros += " AND  C.CT_NOMBRE_SUBASTA like :nombreSubasta ";
        if (filtrosBusqueda.get("tipoSubasta") != null)  sqlFltros += " AND C.IC_TIPO_SUBASTA = :tipoSubasta ";
        if (filtrosBusqueda.get("estatusSubasta") != null) sqlFltros += " AND C.CD_ESTATUS = :estatusSubasta ";
        if (filtrosBusqueda.get("fechaAlta") != null) sqlFltros += " AND C.DT_FECHA_ALTA = TO_DATE(:fechaAlta, 'DD/MM/YYYY') ";
        //if (filtrosBusqueda.get("fechaLimite") != null) sqlFltros += " AND C.DT_FECHA_LIMITE_RECEPCION BETWEEN TO_DATE(:d_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:d_fechaDos, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("d_fechaUno") != null) sqlFltros += " AND  C.DT_FECHA_LIMITE_RECEPCION BETWEEN TO_DATE(:d_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:d_fechaDos, 'DD/MM/YYYY') ";
        if (filtrosBusqueda.get("interFinanciero") != null) sqlFltros += " AND CI.IC_IF = :interFinanciero ";
        
        UtilUsr utilUsr = new UtilUsr();
        try {
            con.conexionDB();

            String sql;
            sql = "SELECT distinct TO_CHAR(C.DT_FECHA_ALTA, 'DD/MM/YYYY') FECHA_ENVIO,\n" + 
            "       TO_CHAR(C.DT_FECHA_LIMITE_RECEPCION, 'DD/MM/YYYY') FECHA_LIMITE,\n" + 
            "       C.CT_NOMBRE_SUBASTA,\n" + 
            "       (select t.cd_nombre_subasta\n" + 
            "          from gdoccat_subasta t\n" + 
            "         where t.ic_tipo_subasta = C.IC_TIPO_SUBASTA) TIPO_SUBASTA,\n" + 
            "       --POSTURAS,\n" + 
            "       P.IC_USUARIO_CARGA USCARGAP,\n" + 
            "       NVL(C.IC_ARCHIVO, 0) ARCHIVOCONV,\n" + 
            "       TO_CHAR(P.DT_FECHA_SOLVENTAR, 'DD/MM/YYYY') FECHA_LIMITE_SOL,\n" + 
            "       C.CD_ESTATUS ESTATUS,\n" + 
            "       C.IC_CONVOCATORIA,\n" + 
            "       NVL(P.IC_ARCHIVO_CALIF, 0) ARCHIVO_CALIF,\n" + 
            "       nvl(R.IC_ARCHIVO, 0) ARCHIVORES,\n" + 
            "       CI.IC_IF,\n" +
            "       p.tg_comentario,"+
            "       to_char(p.dt_fecha_carga,'dd/mm/yyyy hh:mi:ss') dt_fecha_carga \n"+      
            "--FECHARECRES\n" + 
            "  FROM GDOC_CONVOCATORIA C\n" + 
            " INNER JOIN GDOCREL_CONVOCATORIA_IF CI\n" + 
            "    ON CI.IC_CONVOCATORIA = C.IC_CONVOCATORIA\n" + 
            "  LEFT JOIN GDOC_POSTURA P\n" + 
            "    ON C.IC_CONVOCATORIA = P.IC_CONVOCATORIA\n" + 
            "   AND P.IC_IF = CI.IC_IF\n" + 
            "  LEFT JOIN GDOC_RESULTADO R\n" + 
            "    ON R.IC_CONVOCATORIA = P.IC_CONVOCATORIA\n" + 
            "   AND R.IC_IF = P.IC_IF\n" + 
            " INNER JOIN COMCAT_IF I\n" + 
            "    ON CI.IC_IF = I.ic_if\n" + 
            " WHERE C.IC_CONVOCATORIA IS NOT NULL "+   
                  sqlFltros+
                " ORDER BY C.IC_CONVOCATORIA DESC";
            stmt = new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            log.info(sql);
            for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
                String key = entry.getKey();
                if ( key.equals("tipoSubasta")||key.equals("interFinanciero")){
                    Integer entero = Integer.parseInt(entry.getValue());
                    stmt.setInt(key, entero);
                }
                else{
                    stmt.setChar(key, entry.getValue());
                }
                log.info("clave=" + entry.getKey() + ", valor=" + entry.getValue());
            }
            rs = stmt.executeQuery();
            
            while(rs.next()){
                InfoSubastas info = new InfoSubastas();
                info.setIcConvocatoria(rs.getString("IC_CONVOCATORIA"));
                info.setFechaAlta(rs.getString("FECHA_ENVIO"));
                info.setFechaLimite(rs.getString("FECHA_LIMITE"));
                info.setFechaRecepResultados(rs.getString("FECHA_LIMITE_SOL"));
                info.setFechaSolventar(rs.getString("FECHA_LIMITE_SOL"));
                info.setNombreSubasta(rs.getString("CT_NOMBRE_SUBASTA"));
                info.setTipoSubasta(rs.getString("TIPO_SUBASTA"));
                info.setFechaCarga(rs.getString("dt_fecha_carga"));
                info.setUsuarioIntFin(rs.getString("USCARGAP"));
                if(info.getUsuarioIntFin()!=null&&!info.getUsuarioIntFin().equals("")){
                    Usuario usuario = utilUsr.getUsuario(info.getUsuarioIntFin());
                    info.setUsuarioIntFin(usuario.getNombreCompleto()+" "+info.getFechaCarga());
                }
                info.setIdArchivo(Integer.parseInt(rs.getString("ARCHIVOCONV")));
                info.setEstatus(rs.getString("ESTATUS"));
                info.setIdIntermediario(rs.getString("IC_IF"));
                info.setIdArchivoCalif(rs.getInt("ARCHIVO_CALIF"));
                info.setIdArchivoRes(rs.getInt("ARCHIVORES"));
                info.setTagComentario(rs.getString("tg_comentario"));
                
                retorno.add(info);
            }
            stmt.close();
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        finally{
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }   
        }
        return retorno;
    }
    
    
    
    public ArrayList<ElementoCatalogo> getCatSubastas() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT IC_TIPO_SUBASTA, CD_NOMBRE_SUBASTA FROM GDOCCAT_SUBASTA S ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("IC_TIPO_SUBASTA");
                String descripcion = rs.getString("CD_NOMBRE_SUBASTA");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    public String altaSubastas(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        Integer numeroFolio = null;
        String folioGenerado = null;
        try {
            con.conexionDB();
            Integer icSubasta= 0;
            String sqlQueryNextVal = " SELECT SEQ_GDOC_SUBASTA.nextval FROM dual  ";
            rs = con.queryDB(sqlQueryNextVal);
            if (rs.next()){
                icSubasta = rs.getInt(1);
            }
           
            log.info("Numero Subasta:"+icSubasta);
            strSQL.append(" INSERT INTO GDOC_CONVOCATORIA (IC_CONVOCATORIA, IC_TIPO_SUBASTA, DT_FECHA_ALTA, DT_FECHA_LIMITE_RECEPCION, CT_NOMBRE_SUBASTA) " +
                " VALUES (?, ?, sysdate, to_date(?,'DD/MM/YYYY HH24:mi:ss'), ?) ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, icSubasta);
            ps.setString(2, subastas.getTipoSubasta());
            ps.setString(3, subastas.getFechaMaximaRecep());
            ps.setString(4, subastas.getNombreSubasta());
            //ps.setInt(5, subastas.getIdArchivo());
            ps.executeUpdate();
            
            if (!subastas.getSintermediarios().isEmpty()){
                int numeroIFSeleccionados = subastas.getSintermediarios().size();
                StringBuilder strSQLIF = new StringBuilder();
                strSQLIF.append(" INSERT INTO GDOCREL_CONVOCATORIA_IF (IC_IF, IC_CONVOCATORIA) VALUES (?,?)");
                for(int numeroIF =0; numeroIF< numeroIFSeleccionados; numeroIF++){
                    ps = con.queryPrecompilado(strSQLIF.toString());
                    ps.setInt(1, subastas.getSintermediarios().get(numeroIF));
                    ps.setInt(2, icSubasta);
                    ps.executeUpdate();
                }    
            }            

            
            resultado = true;
            folioGenerado = String.valueOf(icSubasta);
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return folioGenerado;
    }
    
    public String borrarSubastas(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        ArrayList<Integer> listaArchivos = new ArrayList<>();
        try {
            con.conexionDB();
            String sqlArchivo = " select r.ic_archivo from GDOC_RESULTADO r where  r.ic_convocatoria = ? UNION \n" + 
                " SELECT p.ic_archivo FROM GDOC_POSTURA p WHERE p.IC_CONVOCATORIA = ? UNION \n" + 
                " SELECT p.ic_archivo_calif AS IC_ARCHIVO FROM GDOC_POSTURA p WHERE p.IC_CONVOCATORIA = ? UNION \n" + 
                " SELECT IC_ARCHIVO FROM GDOC_CONVOCATORIA  WHERE IC_CONVOCATORIA = ? ";
            ps = con.queryPrecompilado(sqlArchivo);
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(3, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(4, Integer.parseInt(subastas.getIcConvocatoria()));
            rs = ps.executeQuery();
            while(rs.next()){
                Integer icFile = rs.getInt("IC_ARCHIVO");
                if(icFile != null){
                    listaArchivos.add(icFile);
                }
            }
            strSQL.append(" DELETE FROM GDOC_RESULTADO WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            strSQL.setLength(0);
            strSQL = new StringBuilder();
            strSQL.append(" DELETE FROM GDOC_POSTURA WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            StringBuilder strSQLIF = new StringBuilder();
            strSQLIF.append(" DELETE FROM GDOCREL_CONVOCATORIA_IF WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQLIF.toString());
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            strSQL.setLength(0);
            strSQL = new StringBuilder();
            strSQL.append(" DELETE FROM GDOC_CONVOCATORIA WHERE IC_CONVOCATORIA = ?");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            
            if(!listaArchivos.isEmpty()){
                for(Integer archivo : listaArchivos){
                    files2OnBaseBean.eliminaArchivoEnTabla(archivo);    
                }
            }
            resultado = true;
           
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return subastas.getIcConvocatoria();
    }


    public String updateSolicitud(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
         
            strSQL.append(" UPDATE GDOC_CONVOCATORIA SET " +
                          " IC_TIPO_SUBASTA = ?, " +
                          " DT_FECHA_LIMITE_RECEPCION = TO_DATE(?, 'DD/MM/YYYY'),"+                          
                          " CT_NOMBRE_SUBASTA = ? " +
                          //" , IC_ARCHIVO = ? " +
                          " WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            
            ps.setInt(1, Integer.parseInt(subastas.getTipoSubasta()));
            ps.setString(2, subastas.getFechaMaximaRecep());
            ps.setString(3, subastas.getNombreSubasta());
            //ps.setInt(4, subastas.getIdArchivo());
            ps.setInt(4, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            StringBuilder strSQLIFs = new StringBuilder();
            strSQLIFs.append(" DELETE FROM GDOCREL_CONVOCATORIA_IF WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQLIFs.toString()); 
            ps.setInt(1, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            if (!subastas.getSintermediarios().isEmpty()){
                int numeroIFSeleccionados = subastas.getSintermediarios().size();
                StringBuilder strSQLIF = new StringBuilder();
                strSQLIF.append(" INSERT INTO GDOCREL_CONVOCATORIA_IF (IC_IF, IC_CONVOCATORIA) VALUES (?,?)");
                for(int numeroIF =0; numeroIF< numeroIFSeleccionados; numeroIF++){
                    ps = con.queryPrecompilado(strSQLIF.toString());
                    ps.setInt(1, subastas.getSintermediarios().get(numeroIF));
                    ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
                    ps.executeUpdate();
                }    
            }            
          
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado hacer updateSubasta ", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return subastas.getIcConvocatoria();        
    }
    
    
    public String updateArchivoSubasta(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
         
            strSQL.append(" UPDATE GDOC_CONVOCATORIA SET " +
                          " IC_ARCHIVO = ? " +
                          " WHERE IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            
            ps.setInt(1, subastas.getIdArchivo());
            ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.executeUpdate();
            
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return subastas.getIcConvocatoria();        
    }
    
    

    public InfoSubastas getSubastas(Integer icSubasta) {
        AccesoDB con = new AccesoDB();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        InfoSubastas retorno = new InfoSubastas();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" "+
            "SELECT C.CT_NOMBRE_SUBASTA,\n" + 
            "       C.IC_TIPO_SUBASTA,\n" + 
            "       TO_CHAR(C.DT_FECHA_LIMITE_RECEPCION,'DD/MM/YYYY') DT_FECHA_LIMITE_RECEPCION,\n" + 
            "       C.CD_ESTATUS,\n" + 
            "       C.IC_CONVOCATORIA,\n" + 
            "       C.IC_ARCHIVO\n" + 
            "  FROM GDOC_CONVOCATORIA C\n" + 
            "  WHERE C.IC_CONVOCATORIA IS NOT NULL "+
            " AND C.IC_CONVOCATORIA = ? ");
           
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            ps.setInt(1, icSubasta);
            
            rs = ps.executeQuery();

            while (rs.next()){
                retorno.setIcConvocatoria(rs.getString("IC_CONVOCATORIA"));
                retorno.setFechaMaximaRecep(rs.getString("DT_FECHA_LIMITE_RECEPCION"));
                retorno.setNombreSubasta(rs.getString("CT_NOMBRE_SUBASTA"));
                retorno.setTipoSubasta(rs.getString("IC_TIPO_SUBASTA"));
                retorno.setEstatus(rs.getString("CD_ESTATUS"));
                retorno.setIdArchivo(rs.getInt("IC_ARCHIVO"));             
            }
            
            String sqlIfs = " SELECT i.IC_IF\n" + 
            "  FROM COMCAT_IF i, GDOCREL_CONVOCATORIA_IF s\n" + 
            " WHERE s.ic_if = i.ic_if\n" + 
            "   and s.ic_convocatoria = ? ";
            ps = con.queryPrecompilado(sqlIfs); 
            ps.setInt(1, icSubasta);
            rs = ps.executeQuery();    
            ArrayList<Integer> listIF = new ArrayList<>();
            while (rs.next()){
                Integer ifseleccionado = rs.getInt("IC_IF");
                listIF.add(ifseleccionado);
            }
            retorno.setSintermediarios(listIF);
            ArrayList<Integer> listFiles = new ArrayList<>();
            listFiles.add(retorno.getIdArchivo());
            retorno.setSArchivos(listFiles);
            
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    public ArrayList<ElementoCatalogo> getFileSubasta(Integer idArchivo) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<ElementoCatalogo> retorno = new ArrayList<>();
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append("   select a.Ic_Archivo,  a.CD_NOMBRE_ARCHIVO from GDOC_ARCHIVO a where " + 
                "  a.Ic_Archivo = ? ");
            ps = con.queryPrecompilado(strSQL.toString()); 
            ps.setInt(1, idArchivo);
            rs = ps.executeQuery();
            
            while (rs.next()){
                String val = rs.getString("Ic_Archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                retorno.add(elcat);
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return retorno;
    }
    
    
    public Boolean altaPostura(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
           
            strSQL.append(" INSERT INTO GDOC_POSTURA (IC_IF, IC_CONVOCATORIA,IC_ARCHIVO,IC_USUARIO_CARGA,DT_FECHA_CARGA) VALUES(?, ?, ?, ?, sysdate) ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, Integer.parseInt(subastas.getIdIntermediario()));
            ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(3, subastas.getIdArchivo());
            ps.setInt(4, Integer.parseInt(subastas.getUsuarioIntFin()));
            ps.executeUpdate();
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return resultado;
    }
    
    public Boolean borrarPostura(Integer icConvocatoria, Integer icIf) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            String sqlArchivo = "SELECT IC_ARCHIVO FROM GDOC_POSTURA WHERE IC_IF = ? AND IC_CONVOCATORIA = ? ";
            ps = con.queryPrecompilado(sqlArchivo);
            log.info(strSQL);
            ps.setInt(1, icIf);
            ps.setInt(2, icConvocatoria);
            rs = ps.executeQuery();
            Integer icArchivo = null;
            while(rs.next()){
                icArchivo = rs.getInt("IC_ARCHIVO");
            }
            strSQL.append(" DELETE FROM GDOC_POSTURA WHERE IC_IF = ? AND IC_CONVOCATORIA = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, icIf);
            ps.setInt(2, icConvocatoria);
            ps.executeUpdate();
            Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            resultado = files2OnBaseBean.eliminaArchivoEnTabla(icArchivo);
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al eliminar la Postura", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return resultado;
    }
    
    public String updatePostura(InfoSubastas subastas, String tipo) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
            
            if(tipo.equals("1")){
                strSQL.append(" UPDATE GDOC_POSTURA P\n" + 
                "   set P.IC_ARCHIVO_CALIF   = ?\n" + 
                " WHERE P.IC_IF = ?\n" + 
                "   AND P.IC_CONVOCATORIA = ?");
                ps = con.queryPrecompilado(strSQL.toString());
                log.info(strSQL);
                
                ps.setInt(1, subastas.getIdArchivoCalif());
                ps.setString(2, subastas.getIdIntermediario());
                ps.setInt(3, Integer.parseInt(subastas.getIcConvocatoria()));
                ps.executeUpdate();
                    
            }else if(tipo.equals("2")){
                strSQL.append(" UPDATE GDOC_POSTURA P\n" + 
                "   set P.TG_COMENTARIO      = ?,\n" + 
                "       P.DT_FECHA_SOLVENTAR = TO_DATE(?, 'DD/MM/YYYY')\n" + 
                " WHERE P.IC_IF = ?\n" + 
                "   AND P.IC_CONVOCATORIA = ?");
                ps = con.queryPrecompilado(strSQL.toString());
                log.info(strSQL);
                
                ps.setString(1, subastas.getTagComentario());
                ps.setString(2, subastas.getFechaSolventar());
                ps.setString(3, subastas.getIdIntermediario());
                ps.setInt(4, Integer.parseInt(subastas.getIcConvocatoria()));
                ps.executeUpdate();
                
            }
            
            
            
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return subastas.getIcConvocatoria();        
    }
    
    
    public Boolean altaResultado(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
           
            strSQL.append(" INSERT INTO GDOC_RESULTADO VALUES(?, ?, ?,SYSDATE) ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, Integer.parseInt(subastas.getIdIntermediario()));
            ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(3, subastas.getIdArchivo());
            ps.executeUpdate();
            resultado = true;
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return resultado;
    }
    
    public Boolean borraResultado(InfoSubastas subastas) {
        AccesoDB con = new AccesoDB();        
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resultado = false;
        StringBuilder strSQL = new StringBuilder();
        try {
            con.conexionDB();
           
            strSQL.append(" DELETE FROM GDOC_RESULTADO WHERE IC_IF = ?  AND IC_CONVOCATORIA = ? AND IC_ARCHIVO = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            ps.setInt(1, Integer.parseInt(subastas.getIdIntermediario()));
            ps.setInt(2, Integer.parseInt(subastas.getIcConvocatoria()));
            ps.setInt(3, subastas.getIdArchivoRes());
            ps.executeUpdate();
            Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
            resultado = files2OnBaseBean.eliminaArchivoEnTabla(subastas.getIdArchivoRes());
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            con.terminaTransaccion(resultado);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return resultado;
    }
    
    
   public ArrayList<ElementoCatalogo> getPosturas(String icConvocatoria, String icIf) {
        AccesoDB con = new AccesoDB();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        InfoSubastas retorno = new InfoSubastas();
        StringBuilder strSQL = new StringBuilder();
        ArrayList<ElementoCatalogo> posturaL = new ArrayList<>();
        try {
            con.conexionDB();
            strSQL.append(" "+
            " select a.Ic_Archivo,  a.CD_NOMBRE_ARCHIVO from GDOC_ARCHIVO a where a.Ic_Archivo in ("+
            " SELECT P.IC_ARCHIVO\n" +
            "  FROM GDOC_POSTURA P\n" + 
            " WHERE P.IC_IF =?\n" + 
            "   AND P.IC_CONVOCATORIA = ?)");
           
            ps = con.queryPrecompilado(strSQL.toString()); 
            log.info(strSQL.toString());
            ps.setInt(1, Integer.parseInt(icIf));
            ps.setInt(2, Integer.parseInt(icConvocatoria));
            
            rs = ps.executeQuery();

            while (rs.next()){
                String val = rs.getString("Ic_Archivo");
                String descripcion = rs.getString("CD_NOMBRE_ARCHIVO");
                ElementoCatalogo elcat = new ElementoCatalogo(val, descripcion);
                posturaL.add(elcat);
            }
            
        } catch (Throwable e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return posturaL;
    }
    
}