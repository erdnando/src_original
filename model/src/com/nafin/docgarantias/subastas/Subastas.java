package com.nafin.docgarantias.subastas;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface Subastas {
    
    public ArrayList<InfoSubastas> getInfoSubastas(Map<String,String> filtrosBusqueda);
    
    public ArrayList<ElementoCatalogo> getCatSubastas();
    
    public String altaSubastas(InfoSubastas subastas);
    
    public String updateSolicitud(InfoSubastas subastas);
    
    public String borrarSubastas(InfoSubastas subastas);
    
    public String updateArchivoSubasta(InfoSubastas subastas);
    
    public InfoSubastas getSubastas(Integer icSubasta);
    
    public ArrayList<ElementoCatalogo> getFileSubasta(Integer idArchivo);
    
    public ArrayList<InfoSubastas> getInfoSeguimiento(Map<String,String> filtrosBusqueda);
    
    public ArrayList<InfoSubastas> getInfoSeguimientoIf(Map<String,String> filtrosBusqueda);
    
    public Boolean altaPostura(InfoSubastas subastas);
        
    public String updatePostura(InfoSubastas subastas, String tipo);
    
    public Boolean borrarPostura(Integer icConvocatoria, Integer icIf);
    
    public Boolean altaResultado(InfoSubastas subastas);
    
    public Boolean borraResultado(InfoSubastas subastas);
    
    public ArrayList<ElementoCatalogo> getPosturas(String icConvocatoria, String icIf);
    
}
