package com.nafin.docgarantias;

import java.util.ArrayList;

import javax.ejb.Remote;

@Remote
public interface FlujoDocumentos {

    public boolean avanzarDocumento(Integer icDocumento, String usuario);
    public boolean avanzarDocumentoIF(Integer icDocumento, String usuario,Integer icIF, String motivo);
    public boolean avanzarDocumentoFormalizacionNafin(Integer icDocumento, String usuario, Integer icIF, String motivo);
    public boolean avanzarDocumentoVistoBuenoNafin(Integer icDocumento, String usuario, Integer icIF, String motivo, ArrayList<String> usuariosFirmantes);
    public boolean avanzarDocumento(Documento docto, String usuario);
    public boolean avanzarDocumentoNoAplicaCargaBO(Integer icDoc, String usuario, Integer icIF);
    public boolean avanzarDocumentoCargaBO(Integer icDoc, String usuario, Integer icIF);
    public boolean rechazarDocumentoCargaBO(Integer icDoc, String usuario, Integer icIF, String motivo);
    public boolean rechazarDocumento(Documento docto, String usuario, String motivo);
    public boolean recuperacionDocumento(Documento docto, String usuario);
    
}
