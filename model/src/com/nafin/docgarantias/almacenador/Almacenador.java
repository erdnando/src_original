package com.nafin.docgarantias.almacenador;

import com.nafin.docgarantias.ArchivoDocumento;

import org.tempuri.Resultado;
import org.tempuri.ResultadoBorrar;
import org.tempuri.ResultadoSalida;

public interface Almacenador {

    public Integer almacenarArchivo(String identificador, String path, String nombreArchivo);

    public boolean eliminarArchivo(String itemNum);

    public ArchivoDocumento obtenerArchivo(Integer icArchivo, String path);

}
