package com.nafin.docgarantias.almacenador;

import com.nafin.docgarantias.ArchivoDocumento;

import java.io.File;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class AlmacenaLocal implements Almacenador {
    
    private static final Log log = ServiceLocator.getInstance().getLog(AlmacenaLocal.class);
    
    public AlmacenaLocal() {
        super();
        log.info("Se está usando el almacenador de archivos LOCAL");
    }


    @Override
    public Integer almacenarArchivo(String identificador, String path, String nombreArchivo) {
        Integer identificadorAlmacenamientoLocal = null;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        StringBuilder strSQL2 = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT SEQ_GDOC_ALMACENAMIENTO.nextval  as A FROM DUAL ");
            ps = con.queryPrecompilado(strSQL.toString());
            log.info(strSQL);
            rs = ps.executeQuery();
            
            while(rs.next()){
                identificadorAlmacenamientoLocal = rs.getInt("A");
            }
            
            strSQL2.append(" INSERT INTO GDOC_ALMACENAMIENTO (IC_ALMACENAMIENTO, CG_NOMBRE_ARCHIVO) VALUES (?, ?) ");
            log.info(strSQL2.toString());
            ps = con.queryPrecompilado(strSQL2.toString());
            ps.setInt(1, identificadorAlmacenamientoLocal);
            ps.setString(2, nombreArchivo);
           int row = ps.executeUpdate();
            log.info(row);
            log.info("Archivo: "+identificadorAlmacenamientoLocal+", "+nombreArchivo);
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
            } catch (Exception e) {
                e.printStackTrace();
            } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return identificadorAlmacenamientoLocal;        
    }

    @Override
    public boolean eliminarArchivo(String itemNum) {
      
        Integer identificadorAlmacenamientoLocal = null;
        String nombreFile = null;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder strSQL = new StringBuilder();
        StringBuilder strSQL2 = new StringBuilder();
        try {
            con.conexionDB();
            strSQL.append(" SELECT CG_NOMBRE_ARCHIVO FROM GDOC_ALMACENAMIENTO where IC_ALMACENAMIENTO = ? ");
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(itemNum));
            log.info(strSQL);
            rs = ps.executeQuery();
            while(rs.next()){
                nombreFile = rs.getString("CG_NOMBRE_ARCHIVO");
            }            
            strSQL2.append(" DELETE FROM GDOC_ALMACENAMIENTO WHERE IC_ALMACENAMIENTO = ? ");
            log.info(strSQL2.toString());
            ps = con.queryPrecompilado(strSQL2.toString());
            ps.setInt(1, Integer.parseInt(itemNum));
            ps.executeUpdate();
            // DEMOS
            //String path ="/oraclei/weblogic/domains/base_domain/servers/demoscadServer_wp04/tmp/_WL_user/Nafin/9jass4/war/00tmp/41docgarant";
            // LOCAL
            String path = "E:\\Users\\vguzmanh\\AppData\\Roaming\\JDeveloper\\system12.1.3.0.41.140521.1008\\o.j2ee\\drs\\Ne10g_DESARROLLO\\nafin-webWebApp.war\\00tmp\\41docgarant";
            String rutaArchivoTemporal = path + "/" + nombreFile;
            
            File archivoUpload  = new File(rutaArchivoTemporal);
            if(archivoUpload.exists()){
                    archivoUpload.delete();
            }
            log.info("Archivo: "+nombreFile);
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return false;
    }

    @Override
    public ArchivoDocumento obtenerArchivo(Integer icArchivo, String path) {
        return archivoByIcArchivo(icArchivo);
    }
    
    private ArchivoDocumento archivoByIcArchivo(Integer icArhivo){
            ArchivoDocumento archivo = new ArchivoDocumento();
            AccesoDB con = new AccesoDB();
            PreparedStatement ps = null;
            ResultSet rs = null;
            StringBuilder strSQL = new StringBuilder();
            try {
                con.conexionDB();
                strSQL.append(" SELECT IC_ARCHIVO, IC_FOLIO_ONBASE, IC_TIPO_ARCHIVO, CD_NOMBRE_ARCHIVO FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ");
                log.info(strSQL.toString());
                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, icArhivo);
                rs = ps.executeQuery();
                while (rs.next()) {
                    archivo.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                    archivo.setItemNum(rs.getInt("IC_FOLIO_ONBASE"));
                    archivo.setIcTipoArchivo(rs.getInt("IC_TIPO_ARCHIVO"));
                    archivo.setNombreArchivo(rs.getString("CD_NOMBRE_ARCHIVO"));
                }
                log.info("Archivo: "+archivo.getItemNum()+", "+archivo.getNombreArchivo());
            } catch (SQLException | NamingException e) {
                log.error(e.getStackTrace());
                throw new AppException("Error inesperado al cargar la información", e);
            } finally {
                AccesoDB.cerrarResultSet(rs);
                AccesoDB.cerrarStatement(ps);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return archivo;
        }
    
}
