package com.nafin.docgarantias.almacenador;

import org.apache.commons.codec.binary.Base64;
import com.nafin.docgarantias.ArchivoDocumento;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.net.MalformedURLException;
import java.net.URL;

import java.nio.charset.Charset;

import java.nio.charset.StandardCharsets;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import javax.naming.NamingException;

import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.WebServiceException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

import org.tempuri.Procesos;
import org.tempuri.ProcesosSoap;
import org.tempuri.Resultado;
import org.tempuri.ResultadoBorrar;
import org.tempuri.ResultadoSalida;

public class AlmacenaOnBase implements Almacenador {
    
    private static final Log log = ServiceLocator.getInstance().getLog(AlmacenaOnBase.class);
    private static final String REPOSISTORIO_ONBASE = "GESGAR";
    private static final Charset UTF_8 = StandardCharsets.UTF_8;
    private ProcesosSoap procesosSoap;
    
    public AlmacenaOnBase() {
        super();
        URL url = null;
        WebServiceException e = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        StringBuilder sqlQuery = new StringBuilder();
        AccesoDB con = new AccesoDB();
        String wsDireccion = "";
        try {
            con.conexionDB();
            sqlQuery.append( " select T.SUP_DIR_WS from com_param_gral t where t.ic_param_gral = 1");
            ps = con.queryPrecompilado(sqlQuery.toString()); 
            rs = ps.executeQuery();
            while (rs.next()) {
                wsDireccion = rs.getString("SUP_DIR_WS");
            } 
            url = new URL(wsDireccion + "?WSDL#%7Bhttp%3A%2F%2Ftempuri.org%2F%7DProcesos");
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            e = new WebServiceException(ex);
        } catch (Exception ex1) {
            ex1.printStackTrace();
            throw new AppException("Error inesperado al obtener la información", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        Procesos procesos = new Procesos(url);
        procesosSoap = procesos.getProcesosSoap();        
    }

    @Override
    public Integer almacenarArchivo(String identificador, String path, String nombreArchivo) {
        Integer idNum = null;
        String base64;
        try {
            base64 = encodeFileToBase64Binary(path + File.separator + nombreArchivo);

        log.info("Ruta: "+path + File.separator + nombreArchivo);
        Resultado resultado = procesosSoap.ingresaArchivo(REPOSISTORIO_ONBASE, identificador, nombreArchivo, "A", base64);
        if (!resultado.getStEstatus().equals("OK")){
            log.info(resultado.getStEstatus());
            log.info("OnBase dice: "+resultado.getStError());    
            throw new AppException("Error al cargar la información a OnBase");
        }
        else{
            idNum = resultado.getItemnum();
        }
        } catch (IOException e) {
            log.info(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error al cargar la información a OnBase");
        }
        return idNum;
    }

    @Override
    public boolean eliminarArchivo(String itemNum) {
        boolean exito = false;
        ResultadoBorrar resultado = procesosSoap.borrarArchivo(REPOSISTORIO_ONBASE, "", itemNum);
        if(resultado.getStEstatus().equals("ER")){
            log.error("Error al eliminar un archivo de OnBase1");
            log.error(resultado.getStError());
        }
        else{
            exito = true;
            log.info("Se elimino correctamente de OnBase el itemNum: "+ itemNum);
        }        
        return exito;
    }

    @Override
    public ArchivoDocumento obtenerArchivo(Integer icArchivo, String path) {
        ArchivoDocumento archivo = null;
        byte[] data = null;
        try {
            archivo = archivoByIcArchivo(icArchivo);
            log.info("Buscando en OnBase...");
            ResultadoSalida resultado = procesosSoap.obtieneArchivo(REPOSISTORIO_ONBASE, "", archivo.getItemNum().toString(), "");
            log.info("...Busqueda en OnBase terminada");
            if(resultado.getStEstatus().equals("ER")){
                log.error(resultado.getStError());
                throw new AppException(resultado.getStError());               
            }
            data = Base64.decodeBase64(resultado.getStArchivoBase64().getBytes(UTF_8));
            String pathFile =path +"00tmp/41docgarant/"+ archivo.getNombreArchivo();
            log.info(pathFile);
            OutputStream stream = new FileOutputStream(pathFile);
            stream.write(data);
        } 
        catch (Exception e) {
            archivo = null;
            log.error(e.getMessage());
            throw new AppException("Error inesperado al obtener la información del sistema OnBase: "+e.getMessage(), e);
        }        
        return archivo;
    }
    
    private String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        String base64 = DatatypeConverter.printBase64Binary(bytes);

        return base64;
    }

    @SuppressWarnings("oracle.jdeveloper.java.nested-assignment")
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large

        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }

    private ArchivoDocumento archivoByIcArchivo(Integer icArhivo){
            ArchivoDocumento archivo = new ArchivoDocumento();
            AccesoDB con = new AccesoDB();
            PreparedStatement ps = null;
            ResultSet rs = null;
            StringBuilder strSQL = new StringBuilder();
            try {
                con.conexionDB();
                strSQL.append(" SELECT IC_ARCHIVO, IC_FOLIO_ONBASE, IC_TIPO_ARCHIVO, CD_NOMBRE_ARCHIVO FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ");
                log.info(strSQL.toString());
                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, icArhivo);
                rs = ps.executeQuery();
                while (rs.next()) {
                    archivo.setIcArchivo(rs.getInt("IC_ARCHIVO"));
                    archivo.setItemNum(rs.getInt("IC_FOLIO_ONBASE"));
                    archivo.setIcTipoArchivo(rs.getInt("IC_TIPO_ARCHIVO"));
                    archivo.setNombreArchivo(rs.getString("CD_NOMBRE_ARCHIVO"));
                }
                log.info("Archivo: "+archivo.getItemNum()+", "+archivo.getNombreArchivo());
            } catch (SQLException | NamingException e) {
                log.error(e.getStackTrace());
                throw new AppException("Error inesperado al cargar la información", e);
            } finally {
                AccesoDB.cerrarResultSet(rs);
                AccesoDB.cerrarStatement(ps);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return archivo;
        }
    
}
