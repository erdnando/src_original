package com.nafin.docgarantias.nivelesdeservicio;

import java.io.Serializable;

public class NivelServicioDocumento implements Serializable {
    @SuppressWarnings("compatibility:-5457284775126451385")
    private static final long serialVersionUID = 1L;



    private Integer icProcesoBitacora;
    private Integer icEstatus;
    private String fechaSolicitud;
    private String folio;
    private String nombreIF;
    private String portafolio;
    private String producto;
    private String tipoDocumento;
    private String tipoMovimiento;
    private String nombreEjecutivo;
    private String etapaProceso;
    private String fechaInicioProceso;
    private String fechaAtencion;
    private String nivelServicio;
    private String diasAtencion;
    private String estatusNivel;
    private String comentarios;
    


    public NivelServicioDocumento() {
        super();
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNombreIF() {
        return nombreIF;
    }

    public void setNombreIF(String nombreIF) {
        this.nombreIF = nombreIF;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getNombreEjecutivo() {
        return nombreEjecutivo;
    }

    public void setNombreEjecutivo(String nombreEjecutivo) {
        this.nombreEjecutivo = nombreEjecutivo;
    }

    public String getEtapaProceso() {
        return etapaProceso;
    }

    public void setEtapaProceso(String etapaProceso) {
        this.etapaProceso = etapaProceso;
    }

    public String getFechaInicioProceso() {
        return fechaInicioProceso;
    }

    public void setFechaInicioProceso(String fechaInicioProceso) {
        this.fechaInicioProceso = fechaInicioProceso;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getNivelServicio() {
        return nivelServicio;
    }

    public void setNivelServicio(String nivelServicio) {
        this.nivelServicio = nivelServicio;
    }

    public String getDiasAtencion() {
        return diasAtencion;
    }

    public void setDiasAtencion(String diasAtencion) {
        this.diasAtencion = diasAtencion;
    }

    public String getEstatusNivel() {
        return estatusNivel;
    }

    public void setEstatusNivel(String estatusNivel) {
        this.estatusNivel = estatusNivel;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Integer getIcProcesoBitacora() {
        return icProcesoBitacora;
    }

    public void setIcProcesoBitacora(Integer icProcesoBitacora) {
        this.icProcesoBitacora = icProcesoBitacora;
    }

    public Integer getIcEstatus() {
        return icEstatus;
    }

    public void setIcEstatus(Integer icEstatus) {
        this.icEstatus = icEstatus;
    }
}
