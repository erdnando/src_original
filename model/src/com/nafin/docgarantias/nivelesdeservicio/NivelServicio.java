package com.nafin.docgarantias.nivelesdeservicio;

import java.io.Serializable;

public class NivelServicio implements Serializable {
    @SuppressWarnings("compatibility:-6171072617232968528")
    private static final long serialVersionUID = 1L;


    private Integer icProceso;
    private Integer icEstatus;
    private Integer diasAtencion;
    private Integer diasAlerta;

    public NivelServicio() {
        super();
    }

    public NivelServicio(Integer icProceso, Integer icEstatus, Integer diasAtencion, Integer diasAlerta) {
        super();
        this.icProceso = icProceso;
        this.icEstatus = icEstatus;
        this.diasAtencion = diasAtencion;
        this.diasAlerta = diasAlerta;
    }


    public Integer getIcProceso() {
        return icProceso;
    }

    public void setIcProceso(Integer icProceso) {
        this.icProceso = icProceso;
    }

    public Integer getIcEstatus() {
        return icEstatus;
    }

    public void setIcEstatus(Integer icEstatus) {
        this.icEstatus = icEstatus;
    }

    public Integer getDiasAtencion() {
        return diasAtencion;
    }

    public void setDiasAtencion(Integer diasAtencion) {
        this.diasAtencion = diasAtencion;
    }

    public Integer getDiasAlerta() {
        return diasAlerta;
    }

    public void setDiasAlerta(Integer diasAlerta) {
        this.diasAlerta = diasAlerta;
    }
}
