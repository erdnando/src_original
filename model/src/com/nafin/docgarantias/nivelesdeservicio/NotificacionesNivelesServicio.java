package com.nafin.docgarantias.nivelesdeservicio;

import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class NotificacionesNivelesServicio{
    
    private static final Log log = ServiceLocator.getInstance().getLog(NotificacionesNivelesServicio.class);
    private static final Integer RECHAZO = 1;
    private static final Integer NOTIFICACION = 2;
    private static final Integer ALERTA_NIVEL_SERVICIO = 3;
    
    private static final String ADMIN_NAFIN_GESTGAR = "ADMIN GESTGAR";
    private static final String AUTO_GESTGAR = "AUTO GESTGAR";
    private static final String AUTO_GESTGAR_JURIDICO = "AUTO GESTGAR JURIDICO";
    private static final String CONS_GESTGAR = "CONS GESTGAR";
    private static final String OPER_GESTGAR = "OPER GESTGAR";
    private static final String OPER_SUBGAR = "OPER SUBGAR";
    private static final String SOL_GESTGAR = "SOL GESTGAR";
    private static final String ADMIN_IF_GARANT = "ADMIN IF GARANT";
    private static final String ADMIN_NAFIN_GARANT = "ADMIN GARANT";
    
    public NotificacionesNivelesServicio() {
        super();
    }

    
    /** Metodo que envia las notificaciones de correo electronico para alertar que esta por cumplirse el perdiodo de 
     * nivel de servicio configurado en la tabla GDOC_NIVEL_SERVICIO
     * */
    public void enviarAlertaNivelServicio() {
        log.info("enviarAlertaNivelServicio(E)");
       String sql = 
        " SELECT                                                                                         "+
        "     ic_documento, fecha_alerta,ic_if, ic_tipo_documento,ic_estatus                             "+
        " FROM                                                                                           "+
        "     (                                                                                          "+
        "         ( SELECT                                                                               "+
        "             b.ic_documento,                                                                    "+
        "             NULL AS ic_if,                                                                     "+
        "             t1.ic_tipo_documento,                                                              "+
        "             b.ic_estatus,                                                                      "+
        "             sigdiahabil2(b.dt_fecha_movimiento,                                                "+
        "                         1,(n.in_dias_atencion - n.in_dias_alerta)) AS fecha_alerta,            "+
        "             b.dt_fecha_movimiento,                                                             "+
        "             n.in_dias_atencion,                                                                "+
        "             n.in_dias_alerta                                                                   "+
        "         FROM                                                                                   "+
        "             (                                                                                  "+
        "                 SELECT                                                                         "+
        "                     ic_solicitud,                                                              "+
        "                     ic_estatus,                                                                "+
        "                     ic_tipo_documento,                                                         "+
        "                     DECODE(ic_tipo_documento, 1001, 3100, 1002, 3200, 1003, 3200,              "+
        "                             1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300) ic_nivelservicio   "+
        "                 FROM                                                                           "+
        "                     gdoc_solicitud                                                             "+
        "                 WHERE                                                                          "+
        "                     ic_estatus IN (110,120,130)                                                "+
        "             ) t1,                                                                              "+
        "             gdoc_nivel_servicio n,                                                             "+
        "             (                                                                                  "+
        "                 SELECT                                                                         "+
        "                     MAX(dt_fecha_movimiento) AS dt_fecha_movimiento,                           "+
        "                     ic_solicitud,                                                              "+
        "                     ic_estatus,                                                                "+
        "                     ic_documento                                                               "+
        "                 FROM                                                                           "+
        "                     gdoc_bitacora_doc                                                          "+
        "                 GROUP BY                                                                       "+
        "                     ic_solicitud,                                                              "+
        "                     ic_estatus,                                                                "+
        "                     ic_documento                                                               "+
        "             ) b                                                                                "+
        "         WHERE                                                                                  "+
        "             n.ic_estatus = t1.ic_estatus                                                       "+
        "             AND b.ic_estatus = t1.ic_estatus                                                   "+
        "             AND b.ic_solicitud = t1.ic_solicitud                                               "+
        "             AND n.ic_proceso_nivel_servicio = t1.ic_nivelservicio                              "+
        "         )                                                                                      "+
        "         UNION                                                                                  "+
        "         ( SELECT                                                                               "+
        "             t.ic_documento,                                                                    "+
        "             b.ic_if,                                                                           "+
        "             t.ic_tipo_documento,                                                               "+
        "             b.ic_estatus,                                                                      "+
        "             sigdiahabil2(nvl(t.dt_fecha_prorroga, b.dt_fecha_movimiento),                      "+
        "                       1,(n.in_dias_atencion - n.in_dias_alerta)) AS fecha_alerta,              "+
        "             b.dt_fecha_movimiento,                                                             "+
        "             n.in_dias_alerta,                                                                  "+
        "             n.in_dias_atencion                                                                 "+
        "         FROM                                                                                   "+
        "             (                                                                                  "+
        "                 SELECT                                                                         "+
        "                     d.ic_documento,                                                            "+
        "                     di.ic_estatus,                                                             "+
        "                     di.dt_fecha_prorroga,                                                      "+
        "                     di.ic_if,                                                                  "+
        "                     s.ic_tipo_documento,                                                       "+
        "                     DECODE(s.ic_tipo_documento, 1001, 3100, 1002, 3200, 1003, 3200,            "+
        "                             1004, 3300, 1005, 3300,1006, 3300, 1007, 3300) ic_nivelservicio    "+
        "                 FROM                                                                           "+
        "                     gdocrel_documento_if   di,                                                 "+
        "                     gdoc_documento         d,                                                  "+
        "                     gdoc_solicitud         s                                                   "+
        "                 WHERE                                                                          "+
        "                     di.ic_estatus IN (140,150,160,175,180)                                     "+
        "                     AND di.ic_documento = d.ic_documento                                       "+
        "                     AND d.ic_solicitud = s.ic_solicitud                                        "+
        "             ) t,                                                                               "+
        "             gdoc_nivel_servicio n,                                                             "+
        "             (                                                                                  "+
        "                 SELECT                                                                         "+
        "                     dt_fecha_movimiento,                                                       "+
        "                     ic_solicitud,                                                              "+
        "                     ic_documento,                                                              "+
        "                     ic_if,                                                                     "+
        "                     ic_estatus                                                                 "+
        "                 FROM                                                                           "+
        "                     gdoc_bitacora_doc                                                          "+
        "                 WHERE                                                                          "+
        "                     ic_if IS NOT NULL                                                          "+
        "                     AND ic_estatus IN (140,150,160,175,180)                                    "+
        "                                                                                                "+
        "             ) b                                                                                "+
        "         WHERE                                                                                  "+
        "             n.ic_estatus = t.ic_estatus                                                        "+
        "             AND t.ic_estatus = b.ic_estatus                                                    "+
        "             AND t.ic_if = b.ic_if                                                              "+
        "             AND t.ic_nivelservicio = n.ic_proceso_nivel_servicio                               "+
        "             AND t.ic_documento = b.ic_documento                                                "+
        "         )                                                                                      "+
        "     )                                                                                          "+
        " WHERE                                                                                          "+
        "     trunc(fecha_alerta) = trunc(SYSDATE)                                                       ";         
       // Obtner solicitudes en estatus 110,120 y 130, calcular (fechamaxima de atencion) - dias de Alerta
       // Obtener Lista Solicitudes en estatus 110,120 y 130
       // Obtener Lista Documentos en estatus 140,150,160 175 y 180
       // Obtener la fecha de liberacion de ese estatus
       // Calcular la fecha de alerta, si es el d�a actual, 
       
       //      Obtener la notificacion del estatus actual
       //        enviar notificacion por email
       AccesoDBOracle con = new AccesoDBOracle(); // conexion directa 
       PreparedStatement pstm =  null;
       ResultSet rs = null;
        log.debug(sql);
        Integer icDocumento = null;
        Integer icTipoDocumento = null;
        Integer icEstatus = null;
        Integer icIF = null;
       try {
           con.conexionDB();
           pstm = con.queryPrecompilado(sql); 
           rs = pstm.executeQuery();
           ArrayList<String> perfiles = new ArrayList<>();
           ArrayList<String> usuarios = new ArrayList<>();
           Usuario usuario = null;
           UtilUsr utilUsr = new UtilUsr();
           String mensaje = "";
           int i = 0;
           while (rs.next()){
               // Para cada documento que cumpla con el alertamiento de niveles de servicio
               i++;
               icDocumento = rs.getInt("IC_DOCUMENTO");
               icTipoDocumento = rs.getInt("IC_TIPO_DOCUMENTO");
               icEstatus = rs.getInt("IC_ESTATUS");
               icIF = rs.getInt("IC_IF");
               String mensajeOriginal = getMensajeNotificacion(icDocumento, icEstatus, ALERTA_NIVEL_SERVICIO, null);
               log.info(mensajeOriginal);
               perfiles = getPerfilNotificar(icTipoDocumento, icEstatus);
               ArrayList<String> icUsuarios = getUsuariosNotificar(perfiles, icIF, icDocumento);
               for (String icUsuario : icUsuarios){
                   mensaje = "";
                   if(icUsuario != null){
                       usuario = utilUsr.getUsuario(icUsuario);
                       if (usuario != null && usuario.getEmail() != null && usuario.getEmail() != null){
                            log.info("NotificacionGDOC: "+icUsuario+" "+ " "+ usuario.getNombreCompleto()+" "+usuario.getEmail());
                            mensaje = mensajeOriginal.replace("{nombreUsuario}", usuario.getNombreCompleto());
                            String remitente = "no_response@nafin.gob.mx";
                            String asunto = "Notificaci�n de Nafinet, Alerta Niveles de Servicio en Gesti�n de Documentos de Garant�as";
                            String para = usuario.getEmail();
                            enviarTextoHTML(remitente, para, asunto, mensaje);                             
                        }
                       else{
                           log.error("NotificacionGDOC Usuario: "+ icUsuario+" NO ENCONTRADO ");
                        }
                   }
                   else{
                       log.info("claveUsuario null: "+ icUsuario);
                    }
               }
               perfiles.clear();
               usuarios.clear();
           }
           if(i == 0){
            log.info("No se encontraron documentos para enviar alerta de notificacion");
           }
           if(rs!=null)rs.close();
           if(pstm!=null)pstm.close();           
       } catch (SQLException | NamingException e) {
           log.error(e.getMessage());
           e.printStackTrace();
           throw new AppException("Error inesperado al consultar la BD enviarAlertaNivelServicio ", e);
       } 
        catch (Throwable t) {
                  log.error(t.getMessage());
                  t.printStackTrace();
                  throw new AppException("Error inesperado al consultar la BD enviarAlertaNivelServicio ", t);
              }       
       finally {
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }   
           log.info("enviarAlertaNivelServicio(S)");
       }        
    }
    
    
    
    /** Regresa un String con el texto HTML del mensaje a ser enviado dependiendo del Estatus y el documento enviado
     *  El mensaje incluye el placeholder {nombreUsuario} para ser reemplazado por el nombre del usuario destinatario
     * */
    private String getMensajeNotificacion(Integer icDocumento, Integer icEstatus, Integer tipoMensaje, String motivoRechazo){
        AccesoDBOracle con = new AccesoDBOracle();
        ResultSet rs = null;        
        String nombreIF = "";
        String folio = "";
        try {
            con.conexionDB();
            String sql = 
                " SELECT   \n" + 
                "CASE (SELECT COUNT(CG_NOMBRE_COMERCIAL) FROM COMCAT_IF cif, GDOCREL_DOCUMENTO_IF dif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = dif.ic_if AND dif.IC_DOCUMENTO =  :ICDOCUMENTO)  \n" + 
                "WHEN  1 THEN (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF cif, GDOCREL_DOCUMENTO_IF dif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = dif.ic_if AND dif.IC_DOCUMENTO =  :ICDOCUMENTO)   \n" + 
                "WHEN 0  THEN 'Cero'  \n" + 
                "ELSE 'varios'  \n" + 
                "END AS ifDoc,  \n" + 
                "CASE (SELECT COUNT(CG_NOMBRE_COMERCIAL) FROM COMCAT_IF cif, GDOCREL_solicitud_IF sif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = sif.ic_if AND sif.IC_SOLICITUD = d.IC_SOLICITUD  AND d.IC_DOCUMENTO = :ICDOCUMENTO)  \n" + 
                "WHEN  1 THEN (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF cif, GDOCREL_solicitud_IF sif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = sif.ic_if AND sif.IC_SOLICITUD = d.IC_SOLICITUD  AND d.IC_DOCUMENTO = :ICDOCUMENTO)  \n" + 
                "WHEN 0 THEN 'Cero'  \n" + 
                "ELSE 'varios'  \n" + 
                "END AS ifSol,\n" + 
                "(SELECT CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) \n" + 
                "FROM GDOC_SOLICITUD s,  GDOC_DOCUMENTO d WHERE s.ic_solicitud = d.ic_solicitud\n" + 
                "AND d.IC_DOCUMENTO   = :ICDOCUMENTO) as FOLIO\n" + 
                "FROM dual  ";
            NamedParameterStatement npsm =  new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            npsm.setInt("ICDOCUMENTO", icDocumento);
            rs = npsm.executeQuery();
            String nombreIFSol = "";
            String nombreIFDoc = "";
            while(rs.next()){
                nombreIFSol = rs.getString("ifSol");
                nombreIFDoc = rs.getString("ifDoc");
                folio = rs.getString("FOLIO");
            }
            if (nombreIFSol.equals(nombreIFDoc) &&  nombreIFDoc.equals("varios")){
                nombreIF = " para varios Intermediarios Financieros, ";
            }
            else if (nombreIFDoc.equals("Cero") && !nombreIFSol.equals("Cero")){
                nombreIF = " para el Intermediario Financiero "+nombreIFSol+", ";
            }
            if(rs!=null) rs.close();
            if(npsm!=null) npsm.close();              
        } catch (SQLException  e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error consultar la Base de Datos, getMensajeNotificacion", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        String mensaje = "";
        if (tipoMensaje == RECHAZO || tipoMensaje == NOTIFICACION){
            String textoInicial = "Acaba de recibir una notificaci&oacute;n de ";
            if (tipoMensaje == RECHAZO){
                textoInicial = "Acaba de recibir una notificaci�n de rechazo de ";
            }
            mensaje = textoInicial;
            switch (icEstatus) {
                case 110: //Solicitud de inicio a OPER GESTGAR/ADMIN GESTGAR
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 120: //En elaboraci�n de documento a SOL GESTGAR
                    mensaje += "su solicitud con el n&uacute;mero de Folio: {1}";
                    break;
                case 140: //En VoBo del solicitante a ADMIN IF GARANT 
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 130: //En VoBo de jur�dico a ADMIN IF GARANT
                    mensaje += "la solicitud con el n&uacute;mero de Folio: {1}";
                    break;
                case 150: //En r�brica de Nafin a AUTO GESTGAR
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 160: //En formalizaci�n Nafin a OPER GESTGAR / SO GESTGAR /ADMIN NAFIN GARANT / CONS GESTGAR / SUBGESTGAR
                    mensaje += "la formalizaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 170: // Documento Formalizado ADMIN NAFIN GESTGAR / OPER GESTGAR 
                    mensaje += "la formalizaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;        
                case 175: //En elaboraci�n de Bases de Operaci�n a ADMIN NAFIN GESTGAR / OPER GESTGAR 
                    mensaje += "la Base de operaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 180: //Validaci�n de Bases de Operaci�n a ADMIN IF GARANT 
                    mensaje += "su Base de operaci&oacute;n con el n&uacute;mero de Folio: {1}";
                    break;
            }  
            if (tipoMensaje == RECHAZO){
                mensaje += "<br/><br><strong>Motivo del Rechazo:</strong> "+ motivoRechazo;
            }
        }
        else{ // Es ALERTA DE NIVEL DE SERVICIO 
            mensaje = "Le informamos que tiene pendiente por atender la solicitud {0} con Folio {1} y est� pr�xima a vencer. ";
        }
        mensaje = mensaje.replace("{0}", nombreIF);
        mensaje = mensaje.replace("{1}", folio);
        String mensajeNotificacion = getMensajeHTMLGeneral();
        mensajeNotificacion = mensajeNotificacion.replace("{mensaje}", mensaje);
        return mensajeNotificacion;    
    }
    
    
    /**  Regresa una lista de los usuarios que deben ser notificados dependiendo los perfiles enviadosk,  el IF y el tipo de Documento */
    private ArrayList<String> getUsuariosNotificar(ArrayList<String> perfiles, Integer icIF, Integer icDocumento){
        ArrayList<String> usuarios = new ArrayList<>();
        for (String perfil : perfiles){
                usuarios.addAll(getUsuariosNotificarPorPerfil(perfil, icIF, icDocumento));
        }     
        log.info("NotificacionGDOC: "+ Arrays.asList(perfiles) +", ["+Arrays.asList(usuarios)+"]");
        return usuarios;
    }
    
    /** Regresa una lista con los usuarios del perfil enviado que deben ser notificados */
    private ArrayList<String> getUsuariosNotificarPorPerfil(String perfil, Integer icIF, Integer icDocumento){
        ArrayList<String> usuarios = new ArrayList<>();
        AccesoDBOracle con = new AccesoDBOracle();
        ResultSet rs = null;
        String sql = "";
        switch (perfil) {
        case ADMIN_NAFIN_GESTGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%ADMIN GESTGAR%' ";
            break;
        case AUTO_GESTGAR:
            sql = " select distinct (ic_usuario) from gdoc_firmantes_requeridos WHERE ic_documento = :icDocumento and ic_if = :icIF ";
            break;
        case AUTO_GESTGAR_JURIDICO:
            sql = " select distinct (ud.ic_usuario)  \n" + 
                "from GDOC_USUARIO_DOCUMENTO ud, GDOC_USUARIO_PORTAFOLIO up, GDOC_PARAMETROS_USUARIO p,  gdoc_documento d, gdoc_solicitud s\n" + 
                "WHERE up.ic_usuario = ud.ic_usuario and p.ic_usuario = ud.ic_usuario \n" + 
                "and up.is_version_actual =1 and ud.is_version_actual = 1 and p.is_version_actual = 1 \n" + 
                "and d.ic_solicitud = s.ic_solicitud and p.ic_perfil = 'AUTO GESTGAR' and p.es_juridico = 1\n" + 
                "and s.ic_tipo_documento = ud.ic_tipo_documento and s.ic_portafolio = up.ic_portafolio \n" + 
                "and d.ic_documento = :icDocumento ";
            break;
        case CONS_GESTGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%CONS GESTGAR%' ";
            break;
        case OPER_GESTGAR:
            sql = " select s.ic_usuario_ejecutivo as ic_usuario from  gdoc_solicitud s, gdoc_documento d where d.ic_solicitud = s.ic_solicitud and d.ic_documento = :icDocumento ";
            break;
        case OPER_SUBGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%OPER SUBGAR%' ";
            break;
        case SOL_GESTGAR:
            sql = " select s.ic_usuario_registro as ic_usuario from  gdoc_solicitud s, gdoc_documento d where d.ic_solicitud = s.ic_solicitud and d.ic_documento = :icDocumento ";
            break;
        case ADMIN_IF_GARANT:
            sql = " select distinct (ud.ic_usuario)  \n" + 
                "from GDOC_USUARIO_DOCUMENTO ud, GDOC_USUARIO_PORTAFOLIO up,  gdoc_documento d, gdoc_solicitud s\n" + 
                "WHERE up.ic_usuario = ud.ic_usuario and up.is_version_actual =1 and ud.is_version_actual = 1 and ud.ic_if = up.ic_if and d.ic_solicitud = s.ic_solicitud \n" + 
                "and s.ic_tipo_documento = ud.ic_tipo_documento and s.ic_portafolio = up.ic_portafolio\n" + 
                "and ud.ic_if = :icIF \n" + 
                "and d.ic_documento = :icDocumento ";
            break;
        case ADMIN_NAFIN_GARANT:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%ADMIN GARANT' ";
            break;
        }
        try {
            con.conexionDB();
            log.info(sql);
            log.info(sql+ " params: icIF["+icIF+"] icDocumento["+icDocumento+"]" );
            
            NamedParameterStatement npsm =  new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            if (perfil.equals(AUTO_GESTGAR) || perfil.equals(ADMIN_IF_GARANT)){
                npsm.setInt("icIF", icIF);
            }
            if(perfil.equals(AUTO_GESTGAR) || perfil.equals(AUTO_GESTGAR_JURIDICO)  || perfil.equals(OPER_GESTGAR) 
               || perfil.equals(SOL_GESTGAR) || perfil.equals(ADMIN_IF_GARANT)){
                npsm.setInt("icDocumento", icDocumento);
            }
            rs = npsm.executeQuery();
            while (rs.next()){
                usuarios.add(rs.getString("ic_usuario"));
            }
            if(rs!=null) rs.close();
            if(npsm!=null) npsm.close();  
        } catch (SQLException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosNotificarPorPerfil", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }           
        }   
        return usuarios;
    }
    
    
    /**
     * Regresa una lista con los perfiles a los que hay que notificar dependiendo del estatus al que cambio el documento
     * */
    private ArrayList<String> getPerfilNotificar(Integer icTipoDocumento, Integer icEstatus){
        ArrayList<String>  perfilesNotificar = new ArrayList<String>();                    
        switch (icEstatus){
        case 110:
            perfilesNotificar.add(OPER_GESTGAR);
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;
        case 120:               
            perfilesNotificar.add(SOL_GESTGAR);
            break;
        case 130:
            if (icTipoDocumento ==  1001){  // Solo aplica para Terminos y Condiciones
                perfilesNotificar.add(AUTO_GESTGAR_JURIDICO);
            }
            break;
        case 140:
            perfilesNotificar.add(ADMIN_IF_GARANT);
            break;
        case 150:
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;
        case 160:
            perfilesNotificar.add(AUTO_GESTGAR);
            break;        
        case 170:
            perfilesNotificar.add(OPER_GESTGAR);
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            perfilesNotificar.add(SOL_GESTGAR);
            perfilesNotificar.add(CONS_GESTGAR);
            perfilesNotificar.add(OPER_SUBGAR);
            perfilesNotificar.add(ADMIN_IF_GARANT);
            perfilesNotificar.add(ADMIN_NAFIN_GARANT); // No est� en producci�n :S
            break;
        case 175:
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;          
        case 180:
            perfilesNotificar.add(ADMIN_IF_GARANT);
            break;      
        }
        return perfilesNotificar;    
    }
    
    /** Regresa el HTML del mensaje general a enviar, con los placeholders de {mensaje} y {nombreUsuario} */
    private String getMensajeHTMLGeneral(){
        return  "<!DOCTYPE html><html>\n" + 
        "<head><style>table, th, td {border: 0px;padding:5px;width:70%;}\n" + 
        "</style></head><body><table align=\"center\">  <tr align=\"center\">\n" + 
        "    <td><img src=\"https://www.nafin.com/portalnf/content/images/logoNAFINSA.png\" align='center' alt=\"Nacional Financiera\">\n" + 
        "    <hr size=\"2\" align=\"center\" width=\"100%\"></td>\n" + 
        "  </tr><tr><td><p style=\"font-size:10.0pt;font-family:'Arial',sans-serif;color:#000;font-weight:bold;\">Estimado(a): {nombreUsuario}</p><p style=\"font-size:10.0pt;font-family:'Arial',sans-serif;color:#484045\">\n" + 
        "  {mensaje}<br><br><br><br> \n" + 
        "ATENTAMENTE<br>Nacional Financiera, S.N.C.<br><br><br><br><br>\n" + 
        "Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada. <br>  \n" + 
        "Por favor no responda este mensaje.</p><br><br></td>\n" + 
        "  </tr><tr align=\"center\"><td><img name=\"footer_r1_c3\" src=\"https://cadenas.nafin.com.mx/nafin/home/img/gobmx.png\" width=\"212\" height=\"79\" border=\"0\" id=\"footer_r1_c3\" alt=\"\" /></td>\n" + 
        "  </tr><tr><td style=\"font-size:7.5pt;color:#999999;text-align:center\">\n" + 
        "  Copyright Nacional Financiera 2008 / info@nafin.gob.mx / 01 800 Nafinsa (623 4672) / Av. Insurgentes Sur 1971, Col. Guadalupe Inn, CP 01020 M&eacute;xico D.F.\n" + 
        "  </td>  </tr></table></body></html>";
    }
    
    /**
     * M�todo para enviar correo en codigo HTML 
     * @param from Remitente
     * @param to Destinatario
     * @param subject Titulo del mensaje
     * @param contenidoMensaje Cadena de texto correspondiente al c�digo HTML
     */
    private void enviarTextoHTML(String from, String to, String subject, String contenidoMensaje) {
        enviarTextoHTML(from, to, subject, contenidoMensaje,"");    
    }    
    
    /**
     * M�todo para enviar correo en codigo HTML 
     * @param from Remitente
     * @param to Destinatario
     * @param cc Destinatario Copia
     * @param subject Titulo del mensaje
     * @param contenidoMensaje Cadena de texto correspondiente al c�digo HTML
     */
    public void enviarTextoHTML(String from, String to, String subject,
                    String contenidoMensaje, String cc) throws AppException {
            log.info("enviarHTML(E)");
            AccesoDBOracle con = new AccesoDBOracle(); // conexion directa 
            String host = "";
            try{
                    host = getMailHost();
                    // Get system properties
                    Properties props = System.getProperties();
                    // Setup mail server
                    props.put("mail.smtp.host", host);
                    // Get session
                    Session session = Session.getInstance(props, null);
                    // Define message
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(from));
                    message.addRecipients(Message.RecipientType.TO, to);
                    if(cc != null && !cc.equals("")){
                            message.addRecipients(Message.RecipientType.CC, 
                                            InternetAddress.parse(cc, false) );                                                     
                    }
                    message.setSubject(subject, "UTF-8");
                    // Create a Multipart
                    MimeMultipart multipart = new MimeMultipart("related");
                    // Contenido
                    BodyPart messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setContent(contenidoMensaje, "text/html;charset=UTF-8");
                    multipart.addBodyPart(messageBodyPart);
                    // Poner todo junto
                    message.setContent(multipart);
                    //Transport.send(message);
                    Transport.send(message, message.getRecipients(Message.RecipientType.TO));
                    if(cc != null && !cc.equals("")){
                            Transport.send(message, message.getRecipients(Message.RecipientType.CC));
                    }
            }catch (Exception ex) {
                    log.error(ex.getMessage());
                    ex.printStackTrace();
                    throw new AppException("Error al enviar correo", ex);
            } finally {
                    System.out.println("enviarHTML(S)");
                    if (con.hayConexionAbierta()){
                            con.cierraConexionDB();
                    }
            }                   
    }

    /** Regresa la IP del serviror SMTP para envio de correo */
    private String getMailHost() throws Exception {
        AccesoDBOracle con = null;
        String qrySentencia = null;
        List<Integer> lVarBind = new ArrayList<Integer>();
        Registros registros = null;
        String host = null;
        try {
            con = new AccesoDBOracle();
            con.enableMessages();
            con.conexionDB();
            qrySentencia =
                " SELECT  CG_IP_SMTP AS HOST FROM COM_PARAM_GRAL WHERE IC_PARAM_GRAL = ? ";
            lVarBind.add(new Integer(1));
            registros = con.consultarDB(qrySentencia, lVarBind, false);
            if (registros != null && registros.next()) {
                host = registros.getString("HOST");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
        }
        return host;
    }


}
