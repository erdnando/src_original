package com.nafin.docgarantias.nivelesdeservicio;


import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import com.netro.xls.ComunesXLS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "NivelesServicio", mappedName = "NivelesServicio")
@TransactionManagement(TransactionManagementType.BEAN)
public class NivelesServicioBean implements NivelesServicio {

    private static final Integer RECHAZO = 1;
    private static final Integer NOTIFICACION = 2;
    private static final Integer ALERTA_NIVEL_SERVICIO = 3;
    
    @Resource
    SessionContext sessionContext;
    
    private static final Log log = ServiceLocator.getInstance().getLog(NivelesServicio.class);
    
    private static final String ADMIN_NAFIN_GESTGAR = "ADMIN GESTGAR";
    private static final String AUTO_GESTGAR = "AUTO GESTGAR";
    private static final String AUTO_GESTGAR_JURIDICO = "AUTO GESTGAR JURIDICO";
    private static final String CONS_GESTGAR = "CONS GESTGAR";
    private static final String OPER_GESTGAR = "OPER GESTGAR";
    private static final String OPER_SUBGAR = "OPER SUBGAR";
    private static final String SOL_GESTGAR = "SOL GESTGAR";
    private static final String ADMIN_IF_GARANT = "ADMIN IF GARANT";
    private static final String ADMIN_NAFIN_GARANT = "ADMIN GARANT";
    
    private static final String ESTATUS_NS_CON_RETRASO = "Con retraso";
    private static final String ESTATUS_NS_EN_TIEMPO = "En tiempo";

    public NivelesServicioBean() {
        super();
    }

    /** Envía la notificación cuando un documento ha cambiado de estatus */
    @Override
    public boolean enviarNotificacion(Integer icEstatus, Integer icDocumento, Integer icIF, Integer icTipoDocumento, String motivoRechazo) {
        log.info("enviarNotificacion(E)");
        ArrayList<String>  perfilesNotificar = this.getPerfilNotificar(icTipoDocumento, icEstatus);
        boolean exito = false;
        UtilUsr utilUsr = new UtilUsr();
        Integer tipoMensaje = motivoRechazo != null ? RECHAZO : NOTIFICACION;
        try {
            String mensajeOriginal = getMensajeNotificacion(icDocumento, icEstatus, tipoMensaje, motivoRechazo);
            String mensaje = null;
            Usuario usuario = null;
            for (String icUsuario : this.getUsuariosNotificar(perfilesNotificar, icIF, icDocumento)){
                usuario = utilUsr.getUsuario(icUsuario);
                log.info("NotificacionGDOC: "+icUsuario+" "+ " "+ usuario.getNombreCompleto()+" "+usuario.getEmail());
                mensaje = mensajeOriginal.replace("{nombreUsuario}", usuario.getNombreCompleto());
                Correo correo = new Correo();
                String remitente = "no_response@nafin.gob.mx";
                String asunto = "Notificación de Nafinet, Gestión de Documentos de Garantías";
                String para = usuario.getEmail();
                log.info(mensaje);
                correo.enviarTextoHTML(remitente, para, asunto, mensaje);    
            }
            exito = true;            
        } 
        catch (Throwable e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }  
        log.info("enviarNotificacion(S)");
        return exito;
    }
    
    
    public boolean enviarSubastas(Integer estatus, Integer icConvocatoria) {        
        ArrayList<String> usuarios = this.getUsuarios(icConvocatoria);
        boolean exito = false;
        UtilUsr utilUsr = new UtilUsr();
        try {
            
            String mensaje = "Se le ha invitado a la subasta";
            String mensajeNotificacion = this.getMensajeHTMLGeneral();
            mensajeNotificacion = mensajeNotificacion.replace("{mensaje}", mensaje);
            
            Usuario usuario = null;
            for (int i = 0; i<usuarios.size();i++){
                    usuario = utilUsr.getUsuario(usuarios.get(i));
                    log.info("SubastasGDOC: "+usuarios.get(i)+" "+ " "+ usuario.getNombreCompleto()+" "+usuario.getEmail());
                    mensaje = mensajeNotificacion.replace("{nombreUsuario}", usuario.getNombreCompleto());
                    Correo correo = new Correo();
                    String remitente = "no_response@nafin.gob.mx";
                    String asunto = "Notificación de Nafinet, Gestión de Documentos Subastas ";
                    String para = usuario.getEmail();
                    correo.enviarTextoHTML(remitente, para, asunto, mensaje);    
                }
            exito = true;            
        } 
        catch (Throwable e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }            
        return exito;
    }
    
    /** Regresa una lista con los usuarios del perfil enviado que deben ser notificados */
    private ArrayList<String> getUsuariosNotificarPorPerfil(String perfil, Integer icIF, Integer icDocumento){
        ArrayList<String> usuarios = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String sql = "";
        switch (perfil) {
        case ADMIN_NAFIN_GESTGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%ADMIN GESTGAR%' ";
            break;
        case AUTO_GESTGAR:
            sql = " select distinct (ic_usuario) from gdoc_firmantes_requeridos WHERE ic_documento = :icDocumento and ic_if = :icIF ";
            break;
        case AUTO_GESTGAR_JURIDICO:
            sql = " select distinct (ud.ic_usuario)  \n" + 
                "from GDOC_USUARIO_DOCUMENTO ud, GDOC_USUARIO_PORTAFOLIO up, GDOC_PARAMETROS_USUARIO p,  gdoc_documento d, gdoc_solicitud s\n" + 
                "WHERE up.ic_usuario = ud.ic_usuario and p.ic_usuario = ud.ic_usuario \n" + 
                "and up.is_version_actual =1 and ud.is_version_actual = 1 and p.is_version_actual = 1 \n" + 
                "and d.ic_solicitud = s.ic_solicitud and p.ic_perfil = 'AUTO GESTGAR' and p.es_juridico = 1\n" + 
                "and s.ic_tipo_documento = ud.ic_tipo_documento and s.ic_portafolio = up.ic_portafolio \n" + 
                "and d.ic_documento = :icDocumento ";
            break;
        case CONS_GESTGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%CONS GESTGAR%' ";
            break;
        case OPER_GESTGAR:
            sql = " select s.ic_usuario_ejecutivo as ic_usuario from  gdoc_solicitud s, gdoc_documento d where d.ic_solicitud = s.ic_solicitud and d.ic_documento = :icDocumento ";
            break;
        case OPER_SUBGAR:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%OPER SUBGAR%' ";
            break;
        case SOL_GESTGAR:
            sql = " select s.ic_usuario_registro as ic_usuario from  gdoc_solicitud s, gdoc_documento d where d.ic_solicitud = s.ic_solicitud and d.ic_documento = :icDocumento ";
            break;
        case ADMIN_IF_GARANT:
            sql = " select distinct (ud.ic_usuario)  \n" + 
                "from GDOC_USUARIO_DOCUMENTO ud, GDOC_USUARIO_PORTAFOLIO up,  gdoc_documento d, gdoc_solicitud s\n" + 
                "WHERE up.ic_usuario = ud.ic_usuario and up.is_version_actual =1 and ud.is_version_actual = 1 and ud.ic_if = up.ic_if and d.ic_solicitud = s.ic_solicitud \n" + 
                "and s.ic_tipo_documento = ud.ic_tipo_documento and s.ic_portafolio = up.ic_portafolio\n" + 
                "and ud.ic_if = :icIF \n" + 
                "and d.ic_documento = :icDocumento ";
            break;
        case ADMIN_NAFIN_GARANT:
            sql = " select replace(membercn, 'cn=','') ic_usuario \n" + 
                "from DIRECTORY_GROUP_TIPO_CLAVE where TIPO_AFILIADO ='N'\n" + 
                "and  UPPER(rdn) like '%ADMIN GARANT' ";
            break;
        }
        try {
            con.conexionDB();
            log.info(sql+ " params: icIF["+icIF+"] icDocumento["+icDocumento+"]" );
            
            NamedParameterStatement npsm =  new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            if (perfil.equals(AUTO_GESTGAR) || perfil.equals(ADMIN_IF_GARANT)){
                npsm.setInt("icIF", icIF);
            }
            if(perfil.equals(AUTO_GESTGAR) || perfil.equals(AUTO_GESTGAR_JURIDICO)  || perfil.equals(OPER_GESTGAR) 
               || perfil.equals(SOL_GESTGAR) || perfil.equals(ADMIN_IF_GARANT)){
                npsm.setInt("icDocumento", icDocumento);
            }
            rs = npsm.executeQuery();
            while (rs.next()){
                usuarios.add(rs.getString("ic_usuario"));
            }
            npsm.close();
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getUsuariosNotificarPorPerfil", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }           
        }   
        return usuarios;
    }
    
    
    /**  Regresa una lista de los usuarios que deben ser notificados dependiendo los perfiles enviadosk,  el IF y el tipo de Documento */
    private ArrayList<String> getUsuariosNotificar(ArrayList<String> perfiles, Integer icIF, Integer icDocumento){
        ArrayList<String> usuarios = new ArrayList<>();
        for (String perfil : perfiles){
                usuarios.addAll(this.getUsuariosNotificarPorPerfil(perfil, icIF, icDocumento));
        }     
        log.info("NotificacionGDOC: "+ Arrays.asList(perfiles) +", ["+Arrays.asList(usuarios)+"]");
        return usuarios;
    }
    
    
    
    /**
     * Regresa una lista con los perfiles a los que hay que notificar dependiendo del estatus al que cambio el documento
     * */
    private ArrayList<String> getPerfilNotificar(Integer icTipoDocumento, Integer icEstatus){
        ArrayList<String>  perfilesNotificar = new ArrayList<String>();                    
        switch (icEstatus){
        case 110:
            perfilesNotificar.add(OPER_GESTGAR);
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;
        case 120:		
            perfilesNotificar.add(SOL_GESTGAR);
            break;
        case 130:
            if (icTipoDocumento ==  1001){  // Solo aplica para Terminos y Condiciones
                perfilesNotificar.add(AUTO_GESTGAR_JURIDICO);
            }
            break;
        case 140:
            perfilesNotificar.add(ADMIN_IF_GARANT);
            break;
        case 150:
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;
        case 160:
            perfilesNotificar.add(AUTO_GESTGAR);
            break;        
        case 170:
            perfilesNotificar.add(OPER_GESTGAR);
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            perfilesNotificar.add(SOL_GESTGAR);
            perfilesNotificar.add(CONS_GESTGAR);
            perfilesNotificar.add(OPER_SUBGAR);
            perfilesNotificar.add(ADMIN_IF_GARANT);
            break;
        case 175:
            perfilesNotificar.add(ADMIN_NAFIN_GESTGAR);
            break;          
        case 180:
            perfilesNotificar.add(ADMIN_IF_GARANT);
            break;      
        }
        return perfilesNotificar;    
    }


    
    
    /** Regresa un String con el texto HTML del mensaje a ser enviado dependiendo del Estatus y el documento enviado
     *  El mensaje incluye el placeholder {nombreUsuario} para ser reemplazado por el nombre del usuario destinatario
     * */
    private String getMensajeNotificacion(Integer icDocumento, Integer icEstatus, Integer tipoMensaje, String motivoRechazo){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;        
        String nombreIF = "";
        String folio = "";
        try {
            con.conexionDB();
            String sql = 
                " SELECT   \n" + 
                "CASE (SELECT COUNT(CG_NOMBRE_COMERCIAL) FROM COMCAT_IF cif, GDOCREL_DOCUMENTO_IF dif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = dif.ic_if AND dif.IC_DOCUMENTO =  :ICDOCUMENTO)  \n" + 
                "WHEN  1 THEN (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF cif, GDOCREL_DOCUMENTO_IF dif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = dif.ic_if AND dif.IC_DOCUMENTO =  :ICDOCUMENTO)   \n" + 
                "WHEN 0  THEN 'Cero'  \n" + 
                "ELSE 'varios'  \n" + 
                "END AS ifDoc,  \n" + 
                "CASE (SELECT COUNT(CG_NOMBRE_COMERCIAL) FROM COMCAT_IF cif, GDOCREL_solicitud_IF sif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = sif.ic_if AND sif.IC_SOLICITUD = d.IC_SOLICITUD  AND d.IC_DOCUMENTO = :ICDOCUMENTO)  \n" + 
                "WHEN  1 THEN (SELECT CG_NOMBRE_COMERCIAL FROM COMCAT_IF cif, GDOCREL_solicitud_IF sif, GDOC_DOCUMENTO d WHERE   \n" + 
                "cif.IC_IF = sif.ic_if AND sif.IC_SOLICITUD = d.IC_SOLICITUD  AND d.IC_DOCUMENTO = :ICDOCUMENTO)  \n" + 
                "WHEN 0 THEN 'Cero'  \n" + 
                "ELSE 'varios'  \n" + 
                "END AS ifSol,\n" + 
                "(SELECT CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) \n" + 
                "FROM GDOC_SOLICITUD s,  GDOC_DOCUMENTO d WHERE s.ic_solicitud = d.ic_solicitud\n" + 
                "AND d.IC_DOCUMENTO   = :ICDOCUMENTO) as FOLIO\n" + 
                "FROM dual  ";
            NamedParameterStatement npsm =  new NamedParameterStatement(con.getConnection(), sql, QUERYTYPE.QUERY);
            npsm.setInt("ICDOCUMENTO", icDocumento);
            rs = npsm.executeQuery();
            String nombreIFSol = "";
            String nombreIFDoc = "";
            while(rs.next()){
                nombreIFSol = rs.getString("ifSol");
                nombreIFDoc = rs.getString("ifDoc");
                folio = rs.getString("FOLIO");
            }
            if (nombreIFSol.equals(nombreIFDoc) &&  nombreIFDoc.equals("varios")){
                nombreIF = " para varios Intermediarios Financieros, ";
            }
            else if (nombreIFDoc.equals("Cero") && !nombreIFSol.equals("Cero")){
                nombreIF = " para el Intermediario Financiero "+nombreIFSol+", ";
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error consultar la Base de Datos, getMensajeNotificacion", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        String mensaje = "";
        if (tipoMensaje == RECHAZO || tipoMensaje == NOTIFICACION){
            String textoInicial = "Acaba de recibir una notificaci&oacute;n de ";
            if (tipoMensaje == RECHAZO){
                textoInicial = "Acaba de recibir una notificación de rechazo de ";
            }
            mensaje = textoInicial;
            switch (icEstatus) {
                case 110: //Solicitud de inicio a OPER GESTGAR/ADMIN GESTGAR
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 120: //En elaboración de documento a SOL GESTGAR
                    mensaje += "su solicitud con el n&uacute;mero de Folio: {1}";
                    break;
                case 140: //En VoBo del solicitante a ADMIN IF GARANT 
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 130: //En VoBo de jurídico a ADMIN IF GARANT
                    mensaje += "la solicitud con el n&uacute;mero de Folio: {1}";
                    break;
                case 150: //En rúbrica de Nafin a AUTO GESTGAR
                    mensaje += "la solicitud {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 160: //En formalización Nafin a OPER GESTGAR / SO GESTGAR /ADMIN NAFIN GARANT / CONS GESTGAR / SUBGESTGAR
                    mensaje += "la formalizaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 170: // Documento Formalizado ADMIN NAFIN GESTGAR / OPER GESTGAR 
                    mensaje += "la formalizaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;        
                case 175: //En elaboración de Bases de Operación a ADMIN NAFIN GESTGAR / OPER GESTGAR 
                    mensaje += "la Base de operaci&oacute;n {0} con el n&uacute;mero de Folio: {1}";
                    break;
                case 180: //Validación de Bases de Operación a ADMIN IF GARANT 
                    mensaje += "su Base de operaci&oacute;n con el n&uacute;mero de Folio: {1}";
                    break;
            }  
            if (tipoMensaje == RECHAZO){
                mensaje += "<br/><br><strong>Motivo del Rechazo:</strong> "+ motivoRechazo;
            }
        }
        else{ // Es ALERTA DE NIVEL DE SERVICIO 
            mensaje = "Le informamos que tiene pendiente por atender la solicitud {0} con Folio {1} y está próxima a vencer. ";
        }
        mensaje = mensaje.replace("{0}", nombreIF);
        mensaje = mensaje.replace("{1}", folio);
        String mensajeNotificacion = this.getMensajeHTMLGeneral();
        mensajeNotificacion = mensajeNotificacion.replace("{mensaje}", mensaje);
        return mensajeNotificacion;    
    }
    


    @Override
    public boolean guardarNivelesServicio(Map<String, String> configuracion) {
        AccesoDB con = new AccesoDB();
        boolean exito = false;
        NamedParameterStatement npStatement = null;
        try {
            Integer numDiasAtencion = null;
            Integer numDiasAlerta = null;
            con.conexionDB();
            String sqlMerge = " MERGE INTO GDOC_NIVEL_SERVICIO tgt USING \n" + 
            "( SELECT COUNT(IN_DIAS_ATENCION) AS total FROM GDOC_NIVEL_SERVICIO WHERE IC_PROCESO_NIVEL_SERVICIO = :IC_PROCESO AND \n" + 
            " IC_ESTATUS = :IC_ESTATUS ) src ON (src.total >0)\n" + 
            " WHEN MATCHED THEN UPDATE\n" + 
            "  SET tgt.IN_DIAS_ATENCION = :DIAS_ATENCION, tgt.IN_DIAS_ALERTA = :DIAS_ALERTA\n" + 
            "  WHERE IC_PROCESO_NIVEL_SERVICIO = :IC_PROCESO AND IC_ESTATUS = :IC_ESTATUS\n" + 
            "  WHEN NOT MATCHED THEN\n" + 
            "  INSERT ( \n" + 
            "    IC_PROCESO_NIVEL_SERVICIO,\n" + 
            "    IC_ESTATUS,\n" + 
            "    IN_DIAS_ATENCION,\n" + 
            "    IN_DIAS_ALERTA\n" + 
            "    )\n" + 
            "    VALUES (\n" + 
            "    :IC_PROCESO, \n" + 
            "    :IC_ESTATUS, \n" + 
            "    :DIAS_ATENCION, \n" + 
            "    :DIAS_ALERTA ) ";
            npStatement = new NamedParameterStatement(con.getConnection(),sqlMerge , QUERYTYPE.QUERY);

            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCproceso01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCproceso02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 110);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCvalidacion01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCvalidacion02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 120);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCjuridico01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCjuridico02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 130);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCformif01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCformif02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 140);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCrubnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCrubnafin02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 150);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numTCformnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numTCformnafin02"));
            npStatement.setInt("IC_PROCESO", 3100);
            npStatement.setInt("IC_ESTATUS", 160);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();                               
            numDiasAtencion      = Integer.valueOf(configuracion.get("numROproceso01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numROproceso02"));
            npStatement.setInt("IC_PROCESO", 3200);
            npStatement.setInt("IC_ESTATUS", 110);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numROvalidacion01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numROvalidacion02"));
            npStatement.setInt("IC_PROCESO", 3200);
            npStatement.setInt("IC_ESTATUS", 120);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numROformif01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numROformif02"));
            npStatement.setInt("IC_PROCESO", 3200);
            npStatement.setInt("IC_ESTATUS", 140);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numROrubnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numROrubnafin02"));
            npStatement.setInt("IC_PROCESO", 3200);
            npStatement.setInt("IC_ESTATUS", 150);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numROformnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numROformnafin02"));
            npStatement.setInt("IC_PROCESO", 3200);
            npStatement.setInt("IC_ESTATUS", 160);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();                               
            numDiasAtencion      = Integer.valueOf(configuracion.get("numFIproceso01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numFIproceso02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 110);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numFIvalidacion01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numFIvalidacion02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 120);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numFIformif01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numFIformif02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 140);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numFIrubnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numFIrubnafin02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 150);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            numDiasAtencion      = Integer.valueOf(configuracion.get("numFIformnafin01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numFIformnafin02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 160);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();                               
            numDiasAtencion      = Integer.valueOf(configuracion.get("numBOelaboracion01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numBOelaboracion02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 175);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();            
            numDiasAtencion      = Integer.valueOf(configuracion.get("numBOvalidacion01"));
            numDiasAlerta        = Integer.valueOf(configuracion.get("numBOvalidacion02"));
            npStatement.setInt("IC_PROCESO", 3300);
            npStatement.setInt("IC_ESTATUS", 180);
            npStatement.setInt("DIAS_ATENCION", numDiasAtencion);
            npStatement.setInt("DIAS_ALERTA", numDiasAlerta);            
            npStatement.executeUpdate();
            
            npStatement.close();
            exito = true;
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado en guardarNivelesServicio()", e);
        } finally {
            con.terminaTransaccion(exito);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return exito;
    }


    @Override
    public ArrayList<com.nafin.docgarantias.nivelesdeservicio.NivelServicio> getNivelesServicio() {
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        ArrayList<com.nafin.docgarantias.nivelesdeservicio.NivelServicio> listaNS = new ArrayList<>(); 
        try {
            con.conexionDB();
            String sqlns = " SELECT n.IC_PROCESO_NIVEL_SERVICIO,  n.IC_ESTATUS,  n.IN_DIAS_ATENCION,  n.IN_DIAS_ALERTA\n" + 
                " FROM GDOC_NIVEL_SERVICIO n,  Gdoccat_Proceso_Nivel_Servicio cn WHERE n.IC_PROCESO_NIVEL_SERVICIO = cn.IC_PROCESO_NIVEL_SERVICIO ";
            pstm = con.queryPrecompilado(sqlns); 
            rs = pstm.executeQuery();
            while (rs.next()){
                listaNS.add(new com.nafin.docgarantias.nivelesdeservicio.NivelServicio(rs.getInt("IC_PROCESO_NIVEL_SERVICIO"), rs.getInt("IC_ESTATUS"), 
                            rs.getInt("IN_DIAS_ATENCION"), rs.getInt("IN_DIAS_ALERTA")));
            }                

        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD getNivelesServicio", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return listaNS;   
    }
    
    /** Metodo que envia las notificaciones de correo electronico para alertar que esta por cumplirse el perdiodo de 
     * nivel de servicio configurado en la tabla GDOC_NIVEL_SERVICIO
     * */
    @Override
    public void enviarAlertaNivelServicio() {
        NotificacionesNivelesServicio notificaciones = new NotificacionesNivelesServicio();
        notificaciones.enviarAlertaNivelServicio();
    }

    
    /** Regresa el HTML del mensaje general a enviar, con los placeholders de {mensaje} y {nombreUsuario} */
    private String getMensajeHTMLGeneral(){
        return  "<!DOCTYPE html><html>\n" + 
        "<head><style>table, th, td {border: 0px;padding:5px;width:70%;}\n" + 
        "</style></head><body><table align=\"center\">  <tr align=\"center\">\n" + 
        "    <td><img src=\"https://www.nafin.com/portalnf/content/images/logoNAFINSA.png\" align='center' alt=\"Nacional Financiera\">\n" + 
        "    <hr size=\"2\" align=\"center\" width=\"100%\"></td>\n" + 
        "  </tr><tr><td><p style=\"font-size:10.0pt;font-family:'Arial',sans-serif;color:#000;font-weight:bold;\">Estimado(a): {nombreUsuario}</p><p style=\"font-size:10.0pt;font-family:'Arial',sans-serif;color:#484045\">\n" + 
        "  {mensaje}<br><br><br><br> \n" + 
        "ATENTAMENTE<br>Nacional Financiera, S.N.C.<br><br><br><br><br>\n" + 
        "Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada. <br>  \n" + 
        "Por favor no responda este mensaje.</p><br><br></td>\n" + 
        "  </tr><tr align=\"center\"><td><img name=\"footer_r1_c3\" src=\"https://cadenas.nafin.com.mx/nafin/home/img/gobmx.png\" width=\"212\" height=\"79\" border=\"0\" id=\"footer_r1_c3\" alt=\"\" /></td>\n" + 
        "  </tr><tr><td style=\"font-size:7.5pt;color:#999999;text-align:center\">\n" + 
        "  Copyright Nacional Financiera 2008 / info@nafin.gob.mx / 01 800 Nafinsa (623 4672) / Av. Insurgentes Sur 1971, Col. Guadalupe Inn, CP 01020 M&eacute;xico D.F.\n" + 
        "  </td>  </tr></table></body></html>";
    }


    @Override
    public ArrayList<NivelServicioDocumento> getMonitorNivelServicio(Map<String,String> filtrosBusqueda) {
        for (Map.Entry<String, String> entry : filtrosBusqueda.entrySet()) {
            log.info( entry.getKey() + ", " + entry.getValue());
        }
        AccesoDB con = new AccesoDB();
        PreparedStatement ps =  null;
        ResultSet rs = null;
        ArrayList<NivelServicioDocumento> lista = new ArrayList<>();
        String sqlFiltros = "";
        if (filtrosBusqueda.get("s_fechaUno") != null) sqlFiltros += " AND  s.DT_FECHA_CREACION BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') ";
        if(filtrosBusqueda.get("s_estatus") != null) sqlFiltros += " AND b.ic_estatus = (SELECT MAX(ic_estatus) FROM gdoc_bitacora_doc where ic_solicitud = s.ic_solicitud ) ";
        try {
            con.conexionDB();
            String sql = "SELECT b.ic_proceso_docto, \n" + 
            "       b.ic_if, \n" + 
            "       b.ic_solicitud, \n" + 
            "       b.ic_documento, \n" + 
            "       b.ic_estatus, \n" + 
            "       b.ic_usuario_registro, \n" + 
            "       e.cd_nombre_estatus, \n" + 
            "       s.ic_portafolio, \n" + 
            "       s.ic_tipo_documento, \n" + 
            "       To_char(s.dt_fecha_creacion, 'dd/mm/yyyy') as dt_fecha_creacion, \n" + 
            "       s.tg_nombre, \n" + 
            "       Concat(Concat(Extract(year FROM s.dt_fecha_creacion), '-'), s.ic_folio) as FOLIO, \n" + 
            "       DECODE(s.ic_usuario_ejecutivo,null,'Ejecutivo no asignado',(SELECT CD_NOMBRE_USUARIO from gdoc_nombre_usuario where ic_usuario = trim(s.ic_usuario_ejecutivo))) as nombre_ejecutivo, "+
            "       p.cd_nombre_portafolio, \n" + 
            "       cd.cd_nombre_documento, \n" + 
            "       (SELECT CASE \n" + 
            "                 WHEN wh > 1 THEN 'Varios' \n" + 
            "                 ELSE (SELECT i.cg_nombre_comercial \n" + 
            "                       FROM   comcat_if i, \n" + 
            "                              gdocrel_solicitud_if y \n" + 
            "                       WHERE  i.ic_if = y.ic_if \n" + 
            "                              AND y.ic_solicitud = icsolicitud) \n" + 
            "               END \n" + 
            "        FROM   (SELECT si.ic_solicitud AS icSolicitud, \n" + 
            "                       Count(*)        AS wh \n" + 
            "                FROM   comcat_if ci, \n" + 
            "                       gdocrel_solicitud_if si \n" + 
            "                WHERE  ci.ic_if = si.ic_if \n" + 
            "                       AND si.ic_solicitud = s.ic_solicitud \n" + 
            "                GROUP  BY si.ic_solicitud)) AS nombreIF, \n" + 
            "       d.ic_movimiento, \n" + 
            "       ( CASE \n" + 
            "           WHEN d.ic_movimiento IS NULL THEN 'Sin definir' \n" + 
            "           WHEN d.ic_movimiento IS NOT NULL THEN (SELECT cd_nombre_movimiento \n" + 
            "                                                  FROM   gdoccat_movimiento \n" + 
            "                                                  WHERE \n" + 
            "           ic_movimiento = d.ic_movimiento) \n" + 
            "         END ) \n" + 
            "       AS nombreMovimiento, \n" + 
            "       To_char(b.dt_fecha_movimiento, 'dd/mm/yyyy') \n" + 
            "       AS fecha_inicio, \n" + 
            "       (SELECT To_char(t.dt_fecha_movimiento, 'dd/mm/yyyy') \n" + 
            "        FROM   (SELECT dt_fecha_movimiento, \n" + 
            "                       Row_number() \n" + 
            "                         OVER ( \n" + 
            "                           ORDER BY ic_proceso_docto ASC) AS row_number \n" + 
            "                FROM   gdoc_bitacora_doc \n" + 
            "                WHERE  ic_solicitud = b.ic_solicitud \n" + 
            "                       AND ic_proceso_docto > b.ic_proceso_docto) t \n" + 
            "        WHERE  t.row_number = 1) \n" + 
            "       AS Fecha_Final, \n" + 
            "       (SELECT ns.in_dias_atencion \n" + 
            "        FROM   gdoc_nivel_servicio ns \n" + 
            "        WHERE  ns.ic_estatus = b.ic_estatus \n" + 
            "               AND ns.ic_proceso_nivel_servicio = \n" + 
            "                   Decode (s.ic_tipo_documento, 1001, 3100, \n" + 
            "                                                1002, 3200, \n" + 
            "                                                1003, 3200, \n" + 
            "                                                1004, 3300, \n" + 
            "                                                1005, 3300, \n" + 
            "                                                1006, 3300, \n" + 
            "                                                1007, 3300)) AS dias_nivel_servicio, \n" + 
            "   (SELECT LISTAGG(TG_COMENTARIO, '<br/>' ) WITHIN GROUP (ORDER BY DT_FECHA_REGISTRO DESC)\n" + 
            "   FROM GDOC_COMENTARIO_PROCESO WHERE IC_PROCESO_DOCTO = b.ic_proceso_docto and IC_ESTATUS = b.ic_estatus  GROUP BY IC_ESTATUS ) as Comentarios "+
            " FROM   gdoc_bitacora_doc b, \n" + 
            "       gdoc_solicitud s, \n" + 
            "       gdoc_documento d, \n" + 
            "       gdoccat_portafolio p, \n" + 
            "       gdoccat_documento cd, \n" + 
            "       gdoccat_estatus e \n" + 
            " WHERE  b.ic_solicitud = s.ic_solicitud \n" + 
            "       AND s.ic_solicitud = d.ic_solicitud \n" + 
            "       AND s.ic_portafolio = p.ic_portafolio \n" + 
            "       AND s.ic_tipo_documento = cd.ic_tipo_documento \n" + 
            "       AND b.ic_estatus = e.ic_estatus \n" + 
            "       AND b.ic_estatus > 100 \n" + 
            "       AND b.ic_estatus < 190 \n" + 
            "       AND b.ic_estatus != 170   " +
                sqlFiltros +                         
            " ORDER  BY ic_solicitud DESC, ic_proceso_docto DESC ";
            log.info("Monitor Niveles: "+sql);
            ps = con.queryPrecompilado(sql); 
            if (filtrosBusqueda.get("s_fechaUno") != null){
                ps.setString(1, filtrosBusqueda.get("s_fechaUno"));
                ps.setString(2, filtrosBusqueda.get("s_fechaDos"));
            }
            rs = ps.executeQuery();
            while(rs.next()){
                NivelServicioDocumento obj = new NivelServicioDocumento();
                obj.setIcProcesoBitacora(rs.getInt("ic_proceso_docto"));
                obj.setIcEstatus(rs.getInt("ic_Estatus"));
                obj.setFechaSolicitud(rs.getString("Dt_Fecha_Creacion"));
                obj.setEtapaProceso(rs.getString("Cd_Nombre_Estatus"));
                String fechaFinalProceso = null;
                fechaFinalProceso =rs.getString("Fecha_Final");
                obj.setFechaAtencion(fechaFinalProceso);
                obj.setFolio(rs.getString("FOLIO"));
                String fechaInicioProceso = rs.getString("fecha_inicio");
                obj.setFechaInicioProceso(fechaInicioProceso);
                UtilUsr utilUsr = new UtilUsr();
                //String icUsuario = rs.getString("Ic_Usuario_Ejecutivo");
                /*if (icUsuario != null){
                    Usuario usu = utilUsr.getUsuario(icUsuario);
                    obj.setNombreEjecutivo(usu.getNombreCompleto());
                }*/
                obj.setNombreEjecutivo(rs.getString("nombre_ejecutivo"));
                obj.setTipoMovimiento(rs.getString("nombreMovimiento"));
                obj.setPortafolio(rs.getString("Cd_Nombre_Portafolio"));
                obj.setProducto(rs.getString("TG_NOMBRE"));
                obj.setTipoDocumento(rs.getString("CD_NOMBRE_DOCUMENTO"));
                obj.setNombreIF(rs.getString("nombreIF"));
                obj.setNivelServicio(rs.getInt("dias_nivel_servicio")+"");
                int dias = 0;
                obj.setDiasAtencion(dias+"");                
                obj.setComentarios(rs.getString("Comentarios"));
                lista.add(obj);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }  
        return lista;
    }
    
    /** Consulta para extraer los datos que se muestran en el Reporte de Niveles de Servicio, 
     * */
    @Override
    public ArrayList<ReporteNivelServicio> getMonitorNivelServicioExtendido(Map<String,String> filtrosBusqueda) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps =  null;
        ResultSet rs = null;
        ArrayList<ReporteNivelServicio> lista = new ArrayList<>();
        String sqlFiltros = "";
        if (filtrosBusqueda.get("s_fechaUno") != null) sqlFiltros += " AND  s.DT_FECHA_CREACION BETWEEN TO_DATE(:s_fechaUno, 'DD/MM/YYYY') AND TO_DATE(:s_fechaDos, 'DD/MM/YYYY') ";
        try {
            con.conexionDB();
            String reporteSQL = " SELECT (select cg_razon_social  from comcat_if where ic_if = t.ic_if) as NombreIF,\n" + 
            "    TO_CHAR(s.DT_FECHA_CREACION,'DD/MM/YYYY') as Fecha_creacion,\n" + 
            "CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as Folio,\n" + 
            "nvl(s.TG_NOMBRE, d.TG_NOMBRE) as NombreProducto,\n" + 
            "(SELECT cd_nombre_portafolio from gdoccat_portafolio where ic_portafolio = s.ic_portafolio) as Portafolio,\n" + 
            "(SELECT CD_NOMBRE_DOCUMENTO from GDOCCAT_DOCUMENTO where IC_TIPO_DOCUMENTO = d.IC_TIPO_DOCUMENTO) as tipoDocumento,\n" + 
            "(select CD_NOMBRE_MOVIMIENTO from gdoccat_movimiento where IC_MOVIMIENTO = d.IC_MOVIMIENTO) as Movimiento,\n" + 
            "s.IC_USUARIO_EJECUTIVO as ejecutivo,\n" + 
            "FN_NUM_DIAS_HABILES(u.INEnProceso, u.FINEnProceso) as DiasEnElaboracion,\n" + 
            "FN_NUM_DIAS_HABILES(u.INEnValidacion,  u.FINEnValidacion) as DiasEnValidacion,\n" + 
            "FN_NUM_DIAS_HABILES(u.INValidacionJuridico,  u.FINValidacionJuridico) as DiasEnValidacionJuridico,\n" + 
            "FN_NUM_DIAS_HABILES(u.INFormalizacionIF,  t.FINFormalizacionIF) as DiasEnFormalizacionIF,\n" + 
            "FN_NUM_DIAS_HABILES(t.INRubricaNAFIN,  t.FINRubricaNAFIN) as DiasEnRubricaNAFIN,\n" + 
            "FN_NUM_DIAS_HABILES(t.INFormalizacionNAFIN,  t.FINFormalizacionNAFIN) as DiasEnFormalizacionNAFIN,\n" + 
            "FN_NUM_DIAS_HABILES(t.INFormalizadoCargaBo,  t.FINFormalizadoCargaBo) as DiasEnFormalizadoCargaBo,\n" + 
            "FN_NUM_DIAS_HABILES(t.INFormalizadoValidaciónBO,  t.FINFormalizadoValidaciónBO) as DiasEnFormalizadoValidaciónBO,\n" + 
            "   (SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 110\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_Elaboracion,\n" + 
            "	(SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 120\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_Validacion,\n" + 
            "   (SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 130\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_Juridico,\n" + 
            "	(SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 140\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_FormalizacionIF,\n" + 
            "   (SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 150\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_RubricaNafin,\n" + 
            "	(SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 160\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_FormalizacionNafin,\n" + 
            "   (SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 175\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_CargaBO,\n" + 
            "	(SELECT in_dias_atencion FROM gdoc_nivel_servicio WHERE ic_estatus = 180\n" + 
            "	AND ic_proceso_nivel_servicio = Decode (s.ic_tipo_documento, 1001, 3100,\n" + 
            "	1002, 3200, 1003, 3200, 1004, 3300, 1005, 3300, 1006, 3300, 1007, 3300)) AS ns_ValidacionBO,\n" + 
            "    TO_CHAR(u.INPorSolicitar,        'DD/MM/YYYY') as INPorSolicitar,      \n" + 
            "    TO_CHAR(u.FINPorSolicitar,       'DD/MM/YYYY') as FINPorSolicitar,     \n" + 
            "    TO_CHAR(u.INEnProceso,           'DD/MM/YYYY') as INEnProceso,         \n" + 
            "    TO_CHAR(u.FINEnProceso,          'DD/MM/YYYY') as FINEnProceso,        \n" + 
            "    TO_CHAR(u.INEnValidacion,        'DD/MM/YYYY') as INEnValidacion,      \n" + 
            "    TO_CHAR(u.FINEnValidacion,       'DD/MM/YYYY') as FINEnValidacion,     \n" + 
            "    TO_CHAR(u.INValidacionJuridico,  'DD/MM/YYYY') as INValidacionJuridico,\n" + 
            "    TO_CHAR(u.FINValidacionJuridico, 'DD/MM/YYYY') as FINValidacionJuridico,\n" + 
            "    TO_CHAR(u.INFormalizacionIF,     'DD/MM/YYYY') as INFormalizacionIF,\n" + 
            "    TO_CHAR(t.FINFormalizacionIF,        'DD/MM/YYYY') as FINFormalizacionIF,        \n" + 
            "    TO_CHAR(t.INRubricaNAFIN,            'DD/MM/YYYY') as INRubricaNAFIN,            \n" + 
            "    TO_CHAR(t.FINRubricaNAFIN,           'DD/MM/YYYY') as FINRubricaNAFIN,           \n" + 
            "    TO_CHAR(t.INFormalizacionNAFIN,      'DD/MM/YYYY') as INFormalizacionNAFIN,      \n" + 
            "    TO_CHAR(t.FINFormalizacionNAFIN,     'DD/MM/YYYY') as FINFormalizacionNAFIN,     \n" + 
            "    TO_CHAR(t.INFormalizado,             'DD/MM/YYYY') as INFormalizado,             \n" + 
            "    TO_CHAR(t.FINFormalizado,            'DD/MM/YYYY') as FINFormalizado,            \n" + 
            "    TO_CHAR(t.INFormalizadoCargaBo,      'DD/MM/YYYY') as INFormalizadoCargaBo,      \n" + 
            "    TO_CHAR(t.FINFormalizadoCargaBo,     'DD/MM/YYYY') as FINFormalizadoCargaBo,     \n" + 
            "    TO_CHAR(t.INFormalizadoValidaciónBO, 'DD/MM/YYYY') as INFormalizadoValidaciónBO, \n" + 
            "    TO_CHAR(t.FINFormalizadoValidaciónBO,'DD/MM/YYYY') as FINFormalizadoValidaciónBO, \n" + 
            "     u.comentarioPorSolicitar, u.comentarioProceso, u.comentarioValidacion, u.comentarioJuridico,\n" + 
            "    t.comentarioFormalizacionIF, t.comentarioRubricaNafin,t.comentarioFormalizacionNafin,\n" + 
            "    t.comentarioFormalizado, t.comentarioCargaBO,t.comentarioValidacionBO, t.comentarioOperando    "+
            "FROM (\n" + 
            "select b.ic_documento,\n" + 
            "MAX(DECODE(IC_ESTATUS, 100,	DT_FECHA_MOVIMIENTO)) AS INPorSolicitar,\n" + 
            "MAX(DECODE(IC_ESTATUS, 110,	DT_FECHA_MOVIMIENTO)) AS FINPorSolicitar,\n" + 
            "MAX(DECODE(IC_ESTATUS, 110,	DT_FECHA_MOVIMIENTO)) AS INEnProceso,\n" + 
            "MAX(DECODE(IC_ESTATUS, 120,	DT_FECHA_MOVIMIENTO)) AS FINEnProceso,\n" + 
            "MAX(DECODE(IC_ESTATUS, 120,	DT_FECHA_MOVIMIENTO)) AS INEnValidacion,\n" + 
            "MAX(DECODE(IC_ESTATUS, 130,	DT_FECHA_MOVIMIENTO)) AS FINEnValidacion,\n" + 
            "MAX(DECODE(IC_ESTATUS, 130,	DT_FECHA_MOVIMIENTO)) AS INValidacionJuridico,\n" + 
            "MAX(DECODE(IC_ESTATUS, 140,	DT_FECHA_MOVIMIENTO)) AS FINValidacionJuridico,\n" + 
            "MAX(DECODE(IC_ESTATUS, 140,	DT_FECHA_MOVIMIENTO)) AS INFormalizacionIF, \n" + 
            " MAX(DECODE(IC_ESTATUS, 100, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC)  from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 100))) AS comentarioPorSolicitar,\n" + 
            "MAX(DECODE(IC_ESTATUS, 110, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC)  from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 110))) AS comentarioProceso,\n" + 
            "MAX(DECODE(IC_ESTATUS, 120, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC)  from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 120))) AS comentarioValidacion,\n" + 
            "MAX(DECODE(IC_ESTATUS, 130, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC)  from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 130))) AS comentarioJuridico  "+
            " from gdoc_bitacora_doc b WHERE B.IC_IF IS NULL GROUP BY b.ic_documento) u, \n" + 
            "(select b.ic_documento, B.IC_IF,\n" + 
            "MAX(DECODE(IC_ESTATUS, 150,	DT_FECHA_MOVIMIENTO)) AS FINFormalizacionIF,\n" + 
            "MAX(DECODE(IC_ESTATUS, 150,	DT_FECHA_MOVIMIENTO)) AS INRubricaNAFIN,\n" + 
            "MAX(DECODE(IC_ESTATUS, 160,	DT_FECHA_MOVIMIENTO)) AS FINRubricaNAFIN,\n" + 
            "MAX(DECODE(IC_ESTATUS, 160,	DT_FECHA_MOVIMIENTO)) AS INFormalizacionNAFIN,\n" + 
            "MAX(DECODE(IC_ESTATUS, 170,	DT_FECHA_MOVIMIENTO)) AS FINFormalizacionNAFIN,\n" + 
            "MAX(DECODE(IC_ESTATUS, 170,	DT_FECHA_MOVIMIENTO)) AS INFormalizado,\n" + 
            "MAX(DECODE(IC_ESTATUS, 175,	DT_FECHA_MOVIMIENTO)) AS FINFormalizado,\n" + 
            "MAX(DECODE(IC_ESTATUS, 175,	DT_FECHA_MOVIMIENTO)) AS INFormalizadoCargaBo,\n" + 
            "MAX(DECODE(IC_ESTATUS, 180,	DT_FECHA_MOVIMIENTO)) AS FINFormalizadoCargaBo,\n" + 
            "MAX(DECODE(IC_ESTATUS, 180,	DT_FECHA_MOVIMIENTO)) AS INFormalizadoValidaciónBO,\n" + 
            "MAX(DECODE(IC_ESTATUS, 190,	DT_FECHA_MOVIMIENTO)) AS FINFormalizadoValidaciónBO, \n" + 
            "MAX(DECODE(IC_ESTATUS, 140, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 140))) AS comentarioFormalizacionIF,\n" + 
            "MAX(DECODE(IC_ESTATUS, 150, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 150))) AS comentarioRubricaNafin,\n" + 
            "MAX(DECODE(IC_ESTATUS, 160, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 160))) AS comentarioFormalizacionNafin,\n" + 
            "MAX(DECODE(IC_ESTATUS, 170, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 170))) AS comentarioFormalizado,\n" + 
            "MAX(DECODE(IC_ESTATUS, 175, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 175))) AS comentarioCargaBO,\n" + 
            "MAX(DECODE(IC_ESTATUS, 180, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 180))) AS comentarioValidacionBO,\n" + 
            "MAX(DECODE(IC_ESTATUS, 190, (SELECT LISTAGG(tg_comentario, '|') WITHIN GROUP (ORDER BY dt_fecha_registro DESC) from GDOC_COMENTARIO_PROCESO where ic_proceso_docto = b.ic_proceso_docto and  ic_estatus = 190))) AS comentarioOperando "+
            " from gdoc_bitacora_doc b WHERE B.IC_IF IS NOT NULL GROUP BY b.ic_documento, B.IC_IF ) t ,\n" + 
            " GDOC_DOCUMENTO d, GDOC_SOLICITUD s\n" + 
            " WHERE u.ic_documento = t.ic_documento\n" + 
            " and t.ic_documento= d.ic_documento\n" + 
            " and d.ic_solicitud = s.ic_solicitud \n" + 
            " ORDER  BY t.ic_documento ";
            log.info("Monitor Niveles: "+reporteSQL);
            ps = con.queryPrecompilado(reporteSQL); 
            rs = ps.executeQuery();
            UtilUsr utilUsr = new UtilUsr();
            int duracion = 0;
            int nivel = 0;
            
            while(rs.next()){
                
                ReporteNivelServicio obj = new ReporteNivelServicio();
                String estatusNivelServicio ="";    
                // Datos generales 
                obj.setFechaDeSolicitud(rs.getString("Fecha_creacion"));
                obj.setFolio(rs.getString("Folio"));
                obj.setIntermediario(rs.getString("NombreIF"));
                obj.setPortafolio(rs.getString("Portafolio"));
                obj.setProducto(rs.getString("NombreProducto"));
                obj.setTipoDeDocumento(rs.getString("tipoDocumento"));
                obj.setTipoDeMovimiento(rs.getString("Movimiento"));
                String icUsuario = rs.getString("ejecutivo");
                if (icUsuario != null){
                    Usuario usu = utilUsr.getUsuario(icUsuario);
                    obj.setEjecutivo(usu.getNombreCompleto());
                }
                // En Proceso
                obj.setFechaInicioElaboracion(rs.getString("INEnProceso"));
                obj.setFechaFinElaboracion(rs.getString("FINEnProceso"));
                nivel = rs.getInt("ns_Elaboracion");
                duracion = rs.getInt("DiasEnElaboracion");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;
                obj.setNivelServicioElaboracion(rs.getString("ns_Elaboracion"));
                obj.setDiasEnElaboracion(rs.getString("DiasEnElaboracion"));
                obj.setEstatusEtapaElaboracion(estatusNivelServicio);
                obj.setObservacionesEnElaboracion(rs.getString("comentarioProceso"));
                
                // En Validacion
                nivel = rs.getInt("ns_Validacion");
                duracion = rs.getInt("DiasEnValidacion");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                
                obj.setFechaInicioValidacion(rs.getString("INEnValidacion"));
                obj.setFechaFinValidacion(rs.getString("FINEnValidacion"));
                obj.setNivelServicioValidacion(rs.getString("ns_Validacion"));
                obj.setDiasValidacion(rs.getString("DiasEnValidacion"));
                obj.setEstatusEtapaValidacion(estatusNivelServicio);
                obj.setObservacionesValidacion(rs.getString("comentarioValidacion"));
                
                // En validacion Juridico
                nivel = rs.getInt("ns_Juridico");
                duracion = rs.getInt("DiasEnValidacionJuridico");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                
                obj.setFechaInicioJuridico(rs.getString("INValidacionJuridico"));
                obj.setFechaFinJuridico(rs.getString("FINValidacionJuridico"));
                obj.setNivelServicioJuridico(rs.getString("ns_Juridico"));
                obj.setDiasEnJuridico(rs.getString("DiasEnValidacionJuridico"));
                obj.setEstatusEtapaJuridico(estatusNivelServicio);
                obj.setObservacionesJuridico(rs.getString("comentarioJuridico"));
                
                // En Formalizacion IF
                nivel = rs.getInt("ns_FormalizacionIF");
                duracion = rs.getInt("DiasEnFormalizacionIF");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                  
                obj.setFechaInicioFormalizacionIF(rs.getString("INFormalizacionIF"));
                obj.setFechaFinFormalizacionIF(rs.getString("FINFormalizacionIF"));
                obj.setNivelServicioFormalizacionIF(rs.getString("ns_FormalizacionIF"));
                obj.setDiasFormalizacionIF(rs.getString("DiasEnFormalizacionIF"));
                obj.setEstatusEtapaFormalizacionIF(estatusNivelServicio);
                obj.setObservacionesFormalizacionIF(rs.getString("comentarioFormalizacionIF"));
                
                // En Rubrica Nafin
                nivel = rs.getInt("ns_RubricaNafin");
                duracion = rs.getInt("DiasEnRubricaNAFIN");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                  
                obj.setFechaInicioRubricaNafin(rs.getString("INRubricaNAFIN"));
                obj.setFechaFinRubricaNafin(rs.getString("FINRubricaNAFIN"));
                obj.setNivelServicioRubricaNafin(rs.getString("ns_RubricaNafin"));
                obj.setDiasRubricaNafin(rs.getString("DiasEnRubricaNAFIN"));
                obj.setEstatusEtapaRubricaNafin(estatusNivelServicio);
                obj.setObservacionesRubricaNafin(rs.getString("comentarioRubricaNafin"));
                
                // En Formalizacion Nafin
                nivel = rs.getInt("ns_FormalizacionNafin");
                duracion = rs.getInt("DiasEnFormalizacionNAFIN");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                   
                obj.setFechaInicioFormalizacionNafin(rs.getString("INFormalizacionNAFIN"));
                obj.setFechaFinFormalizacionNafin(rs.getString("FINFormalizacionNAFIN"));
                obj.setNivelServicioFormalizacionNafin(rs.getString("ns_FormalizacionNafin"));
                obj.setDiasFormalizacionNafin(rs.getString("DiasEnFormalizacionNAFIN"));
                obj.setEstatusEtapaFormalizacionNafin(estatusNivelServicio);
                obj.setObservacionesFormalizacionNafin(rs.getString("comentarioFormalizacionNafin"));
                
                //  En Carga de Base de Operaciones
                nivel = rs.getInt("ns_CargaBO");
                duracion = rs.getInt("DiasEnFormalizadoCargaBo");
                estatusNivelServicio = duracion >= nivel ? ESTATUS_NS_CON_RETRASO : ESTATUS_NS_EN_TIEMPO;                 
                obj.setFechaInicioCargaBO(rs.getString("INFormalizadoCargaBo"));
                obj.setFechaFinCargaBO(rs.getString("FINFormalizadoValidaciónBO"));
                obj.setNivelServicioCargaBO(rs.getString("ns_CargaBO"));
                obj.setDiasCargaBO(rs.getString("DiasEnFormalizadoCargaBo"));
                obj.setEstatusCargaBO(estatusNivelServicio);
                obj.setObservacionesCargaBO(rs.getString("comentarioCargaBO"));
                
                lista.add(obj);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }  
        return lista;
    }    
    
    @Override
    public boolean guardarComentarioBitacoraProceso(Integer icProcesoBitacora, Integer icEstatus, String comentario, String icUsuario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        boolean exito = false;
        try {
            con.conexionDB();
            String sql = " INSERT INTO GDOC_COMENTARIO_PROCESO (IC_PROCESO_DOCTO, IC_ESTATUS, TG_COMENTARIO, IC_USUARIO, DT_FECHA_REGISTRO) " +
                " VALUES (?, ? ,?, ?, SYSDATE) ";
            pstm = con.queryPrecompilado(sql); 
            log.info(sql);
            pstm.setInt(1, icProcesoBitacora);
            pstm.setInt(2, icEstatus);
            pstm.setString(3, comentario);
            pstm.setString(4, icUsuario);
            pstm.executeUpdate();
            exito = true;
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al insertar en la BD ", e);
        } finally {
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return exito;   
    }


    @Override
    public ArrayList<ElementoCatalogo> getCatTipoDocumento() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT IC_TIPO_DOCUMENTO, CD_NOMBRE_DOCUMENTO FROM GDOCCAT_DOCUMENTO ORDER BY IC_TIPO_DOCUMENTO ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            while(rs.next()){
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("IC_TIPO_DOCUMENTO"), rs.getString("CD_NOMBRE_DOCUMENTO"));
                lista.add(obj);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT IC_MOVIMIENTO, CD_NOMBRE_MOVIMIENTO FROM GDOCCAT_MOVIMIENTO ORDER BY IC_MOVIMIENTO ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            while(rs.next()){
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("IC_MOVIMIENTO"), rs.getString("CD_NOMBRE_MOVIMIENTO"));
                lista.add(obj);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatPortafolio() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT IC_PORTAFOLIO, CD_NOMBRE_PORTAFOLIO FROM GDOCCAT_PORTAFOLIO ORDER BY IC_PORTAFOLIO ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            while(rs.next()){
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("IC_PORTAFOLIO"), rs.getString("CD_NOMBRE_PORTAFOLIO"));
                lista.add(obj);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatProceso() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT IC_ESTATUS, CD_NOMBRE_ESTATUS FROM GDOCCAT_ESTATUS ORDER BY IC_ESTATUS ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            while(rs.next()){
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("IC_ESTATUS"), rs.getString("CD_NOMBRE_ESTATUS"));
                lista.add(obj);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }

    @Override
    public ArrayList<ElementoCatalogo> getCatProducto() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT d.ic_documento,\n" + 
            "  NVL(d.TG_NOMBRE,\n" + 
            "  (SELECT TG_NOMBRE FROM GDOC_SOLICITUD WHERE d.ic_solicitud = ic_solicitud\n" + 
            "  )) AS NOMBRE\n" + 
            "FROM GDOC_DOCUMENTO d\n" + 
            "WHERE d.IC_DOCUMENTO IN\n" + 
            "  (SELECT DISTINCT IC_DOCUMENTO FROM GDOC_BITACORA_DOC) ORDER BY NOMBRE ASC ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            while(rs.next()){
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("ic_documento"), rs.getString("NOMBRE"));
                lista.add(obj);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }


    @Override
    public ArrayList<ElementoCatalogo> getCatEjecutivos() {
        ArrayList<ElementoCatalogo> lista = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " SELECT DISTINCT IC_USUARIO_EJECUTIVO, IC_SOLICITUD FROM GDOC_SOLICITUD WHERE IC_USUARIO_EJECUTIVO IS NOT NULL ";
            pstm = con.queryPrecompilado(sql); 
            rs = pstm.executeQuery();
            UtilUsr usrs = new UtilUsr();
            while(rs.next()){
                Usuario usu = usrs.getUsuario(rs.getString("IC_USUARIO_EJECUTIVO"));
                ElementoCatalogo obj = new ElementoCatalogo(rs.getString("IC_SOLICITUD"), usu.getNombreCompleto());
                lista.add(obj);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }

    @Override
    public String getReporteMonitorNiveles(String strDirectorioPublicacion, Map<String, String> param,  String path) {
        String nombreArchivo = "";
        ArrayList<ReporteNivelServicio> datos =  getMonitorNivelServicioExtendido(param);
        String pathFile = path +"00tmp/41docgarant/";
        //String paramProtafolio = "Todos";

            //String paramProducto  = param.get(1).isEmpty() ? "Todos": this.getDescripcionProducto(Integer.parseInt(param.get(1)));
            //String paramIF  = param.get(2).isEmpty() ? "Todos": this.getDescripcionIF(Integer.parseInt(param.get(2)));
            //String paramVigencia = param.get(3).isEmpty() ? "Todas": param.get(3);
            //String paramEstatus = param.get(4).isEmpty() ? "Todos": param.get(4).equals("1") ? "Verde" : param.get(4).equals("2") ? "Amarillo" : "Rojo";
            //String paramBasesOperacion = param.get(5).isEmpty() ? "Todas": param.get(5);


            try {
                CreaArchivo archivo     = new CreaArchivo();
                nombreArchivo      = archivo.nombreArchivo()+".xls";
                ComunesXLS xlsDoc   = new ComunesXLS(pathFile+nombreArchivo);
                xlsDoc.agregaTitulo("Monitor de Niveles de Servicio",9);                
                // Tabla de parametros enviados
                /*
                xlsDoc.setTabla(2);
                xlsDoc.setCelda("Portafolio","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda(paramProtafolio,"formas",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha de Vigencia","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda(paramVigencia,"formas",ComunesXLS.LEFT);
                xlsDoc.setCelda("","formas",ComunesXLS.LEFT);                
                xlsDoc.cierraTabla();
                */
                // Titulos de las Columnas 
                xlsDoc.setTabla(50);
                xlsDoc.setCelda("Fecha de Solicitud","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Folio","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Intermediario","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Portafolio","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Producto","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Tipo de documento","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Tipo de Movimiento","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Ejecutivo","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de elaboración","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de elaboración","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Servicio (días) Elaboración","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días En Elaboración","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Elaboración","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones En elaboración","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de Validación Solicitante","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Validación Solicitante","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días) Validación Solicitante","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Validación Solicitante","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Validación Solicitante","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Validación Solicitante","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de Validación Jurídico","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Validación jurídico","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días) Validación Jurídico","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Validación Jurídico","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Validación Jurídico","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Validación Jurídico","formasb",ComunesXLS.LEFT);

                xlsDoc.setCelda("Fecha de Inicio de Formalización IF","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Formalización IF","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días) Formalización IF","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Formalización IF","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Formalización IF","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Formalización IF","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de Rubrica Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Rubrica Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días) Rubrica Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Rubrica Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Rubrica Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Rubrica Nafin","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de Formalización Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Formalización Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días) Formalización Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Formalización Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Formalización Nafin","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Formalización Nafin","formasb",ComunesXLS.LEFT);
                
                xlsDoc.setCelda("Fecha de Inicio de Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Fecha fin de Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Nivel Serv (días)Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Días en Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Estatus Etapa Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                xlsDoc.setCelda("Observaciones Generación de Base de Operación","formasb",ComunesXLS.LEFT);
                
                
                for (ReporteNivelServicio obj : datos){
                    xlsDoc.setCelda(obj.getFechaDeSolicitud(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFolio(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getIntermediario(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getPortafolio(),"formas",ComunesXLS.RIGHT);
                    xlsDoc.setCelda(obj.getProducto(),"formas",ComunesXLS.RIGHT);
                    xlsDoc.setCelda(obj.getTipoDeDocumento(),"formas",ComunesXLS.RIGHT);
                    xlsDoc.setCelda(obj.getTipoDeMovimiento(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEjecutivo(),"formas",ComunesXLS.RIGHT);
                    
                    // En Proceso
                    xlsDoc.setCelda(obj.getFechaInicioElaboracion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinElaboracion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioElaboracion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasEnElaboracion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaElaboracion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesEnElaboracion(),"formas",ComunesXLS.LEFT);
                    
                    // En validacion
                    xlsDoc.setCelda(obj.getFechaInicioValidacion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinValidacion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioValidacion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasValidacion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaValidacion(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesValidacion(),"formas",ComunesXLS.LEFT);
                    
                    // En validacion Juridico
                    xlsDoc.setCelda(obj.getFechaInicioJuridico(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinJuridico(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioJuridico(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasEnJuridico(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaJuridico(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesJuridico(),"formas",ComunesXLS.LEFT);
                    
                    // En formalizacion IF
                    xlsDoc.setCelda(obj.getFechaInicioFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesFormalizacionIF(),"formas",ComunesXLS.LEFT);
                    
                    // En Rubrica Nafin
                    xlsDoc.setCelda(obj.getFechaInicioRubricaNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinRubricaNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioRubricaNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasRubricaNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaRubricaNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesRubricaNafin(),"formas",ComunesXLS.LEFT);
                                       
                    // En Formalizacion Nafin
                    xlsDoc.setCelda(obj.getFechaInicioFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusEtapaFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesFormalizacionNafin(),"formas",ComunesXLS.LEFT);
                    
                    // En Carga de BO
                    xlsDoc.setCelda(obj.getFechaInicioCargaBO(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getFechaFinCargaBO(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getNivelServicioCargaBO(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getDiasCargaBO(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getEstatusCargaBO(),"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda(obj.getObservacionesCargaBO(),"formas",ComunesXLS.LEFT);
                }                
                xlsDoc.cierraTabla();
                xlsDoc.cierraXLS();                
            } catch (Throwable e) 
            {
                throw new AppException("Error al generar el archivo ", e);
            }                 
        return  nombreArchivo;
        }
    
    
    private ArrayList<String> getUsuarios(Integer icConvocatoria){
        AccesoDB con = new AccesoDB();
        ArrayList<String> lista = new ArrayList<>();
        PreparedStatement pstm =  null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = " select DISTINCT(IC_USUARIO) usuario\n" + 
            "        from gdoc_parametros_usuario\n" + 
            "        where ic_if in (SELECT CI.IC_IF\n" + 
            "                         FROM GDOCREL_CONVOCATORIA_IF CI\n" + 
            "                        WHERE CI.IC_CONVOCATORIA = ? )" + 
            "         and is_version_actual = 1";
            pstm = con.queryPrecompilado(sql); 
            pstm.setInt(1, icConvocatoria);
            rs = pstm.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("usuario"));
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al consultar la BD ", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(pstm);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return lista;
    }
    
    
}
