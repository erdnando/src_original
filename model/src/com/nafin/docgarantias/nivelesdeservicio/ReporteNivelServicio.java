package com.nafin.docgarantias.nivelesdeservicio;

import java.io.Serializable;

public class ReporteNivelServicio implements Serializable {
    @SuppressWarnings("compatibility:-3554022065906337354")
    private static final long serialVersionUID = 1L;

    private String fechaDeSolicitud                          ;
    private String folio                                     ;
    private String intermediario                             ;
    private String portafolio                                ;
    private String producto                                  ;
    private String tipoDeDocumento                           ;
    private String tipoDeMovimiento                          ;
    private String ejecutivo                                 ;
    private String fechaInicioElaboracion                    ;
    private String fechaFinElaboracion                       ;
    private String nivelServicioElaboracion                  ;
    private String diasEnElaboracion                         ;
    private String estatusEtapaElaboracion                   ;
    private String observacionesEnElaboracion                ;
    private String fechaInicioValidacion                     ;
    private String fechaFinValidacion                        ;
    private String nivelServicioValidacion                   ;
    private String diasValidacion                            ;
    private String estatusEtapaValidacion                    ;
    private String observacionesValidacion                   ;
    private String fechaInicioJuridico                       ;
    private String fechaFinJuridico                          ;
    private String nivelServicioJuridico                     ;
    private String diasEnJuridico                            ;
    private String estatusEtapaJuridico                      ;
    private String observacionesJuridico                     ;
    private String fechaInicioFormalizacionIF                ;

    public void setFechaDeSolicitud(String fechaDeSolicitud) {
        this.fechaDeSolicitud = fechaDeSolicitud;
    }

    public String getFechaDeSolicitud() {
        return fechaDeSolicitud;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFolio() {
        return folio;
    }

    public void setIntermediario(String intermediario) {
        this.intermediario = intermediario;
    }

    public String getIntermediario() {
        return intermediario;
    }

    public void setPortafolio(String portafolio) {
        this.portafolio = portafolio;
    }

    public String getPortafolio() {
        return portafolio;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setTipoDeDocumento(String tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }

    public String getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    public void setTipoDeMovimiento(String tipoDeMovimiento) {
        this.tipoDeMovimiento = tipoDeMovimiento;
    }

    public String getTipoDeMovimiento() {
        return tipoDeMovimiento;
    }

    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public String getEjecutivo() {
        return ejecutivo;
    }

    public void setFechaInicioElaboracion(String fechaInicioElaboracion) {
        this.fechaInicioElaboracion = fechaInicioElaboracion;
    }

    public String getFechaInicioElaboracion() {
        return fechaInicioElaboracion;
    }

    public void setFechaFinElaboracion(String fechaFinElaboracion) {
        this.fechaFinElaboracion = fechaFinElaboracion;
    }

    public String getFechaFinElaboracion() {
        return fechaFinElaboracion;
    }

    public void setNivelServicioElaboracion(String nivelServicioElaboracion) {
        this.nivelServicioElaboracion = nivelServicioElaboracion;
    }

    public String getNivelServicioElaboracion() {
        return nivelServicioElaboracion;
    }

    public void setDiasEnElaboracion(String diasEnElaboracion) {
        this.diasEnElaboracion = diasEnElaboracion;
    }

    public String getDiasEnElaboracion() {
        return diasEnElaboracion;
    }

    public void setEstatusEtapaElaboracion(String estatusEtapaElaboracion) {
        this.estatusEtapaElaboracion = estatusEtapaElaboracion;
    }

    public String getEstatusEtapaElaboracion() {
        return estatusEtapaElaboracion;
    }

    public void setObservacionesEnElaboracion(String observacionesEnElaboracion) {
        this.observacionesEnElaboracion = observacionesEnElaboracion;
    }

    public String getObservacionesEnElaboracion() {
        return observacionesEnElaboracion;
    }

    public void setFechaInicioValidacion(String fechaInicioValidacion) {
        this.fechaInicioValidacion = fechaInicioValidacion;
    }

    public String getFechaInicioValidacion() {
        return fechaInicioValidacion;
    }

    public void setFechaFinValidacion(String fechaFinValidacion) {
        this.fechaFinValidacion = fechaFinValidacion;
    }

    public String getFechaFinValidacion() {
        return fechaFinValidacion;
    }

    public void setNivelServicioValidacion(String nivelServicioValidacion) {
        this.nivelServicioValidacion = nivelServicioValidacion;
    }

    public String getNivelServicioValidacion() {
        return nivelServicioValidacion;
    }

    public void setDiasValidacion(String diasValidacion) {
        this.diasValidacion = diasValidacion;
    }

    public String getDiasValidacion() {
        return diasValidacion;
    }

    public void setEstatusEtapaValidacion(String estatusEtapaValidacion) {
        this.estatusEtapaValidacion = estatusEtapaValidacion;
    }

    public String getEstatusEtapaValidacion() {
        return estatusEtapaValidacion;
    }

    public void setObservacionesValidacion(String observacionesValidacion) {
        this.observacionesValidacion = observacionesValidacion;
    }

    public String getObservacionesValidacion() {
        return observacionesValidacion;
    }

    public void setFechaInicioJuridico(String fechaInicioJuridico) {
        this.fechaInicioJuridico = fechaInicioJuridico;
    }

    public String getFechaInicioJuridico() {
        return fechaInicioJuridico;
    }

    public void setFechaFinJuridico(String fechaFinJuridico) {
        this.fechaFinJuridico = fechaFinJuridico;
    }

    public String getFechaFinJuridico() {
        return fechaFinJuridico;
    }

    public void setNivelServicioJuridico(String nivelServicioJuridico) {
        this.nivelServicioJuridico = nivelServicioJuridico;
    }

    public String getNivelServicioJuridico() {
        return nivelServicioJuridico;
    }

    public void setDiasEnJuridico(String diasEnJuridico) {
        this.diasEnJuridico = diasEnJuridico;
    }

    public String getDiasEnJuridico() {
        return diasEnJuridico;
    }

    public void setEstatusEtapaJuridico(String estatusEtapaJuridico) {
        this.estatusEtapaJuridico = estatusEtapaJuridico;
    }

    public String getEstatusEtapaJuridico() {
        return estatusEtapaJuridico;
    }

    public void setObservacionesJuridico(String observacionesJuridico) {
        this.observacionesJuridico = observacionesJuridico;
    }

    public String getObservacionesJuridico() {
        return observacionesJuridico;
    }

    public void setFechaInicioFormalizacionIF(String fechaInicioFormalizacionIF) {
        this.fechaInicioFormalizacionIF = fechaInicioFormalizacionIF;
    }

    public String getFechaInicioFormalizacionIF() {
        return fechaInicioFormalizacionIF;
    }

    public void setFechaFinFormalizacionIF(String fechaFinFormalizacionIF) {
        this.fechaFinFormalizacionIF = fechaFinFormalizacionIF;
    }

    public String getFechaFinFormalizacionIF() {
        return fechaFinFormalizacionIF;
    }

    public void setNivelServicioFormalizacionIF(String nivelServicioFormalizacionIF) {
        this.nivelServicioFormalizacionIF = nivelServicioFormalizacionIF;
    }

    public String getNivelServicioFormalizacionIF() {
        return nivelServicioFormalizacionIF;
    }

    public void setDiasFormalizacionIF(String diasFormalizacionIF) {
        this.diasFormalizacionIF = diasFormalizacionIF;
    }

    public String getDiasFormalizacionIF() {
        return diasFormalizacionIF;
    }

    public void setEstatusEtapaFormalizacionIF(String estatusEtapaFormalizacionIF) {
        this.estatusEtapaFormalizacionIF = estatusEtapaFormalizacionIF;
    }

    public String getEstatusEtapaFormalizacionIF() {
        return estatusEtapaFormalizacionIF;
    }

    public void setObservacionesFormalizacionIF(String observacionesFormalizacionIF) {
        this.observacionesFormalizacionIF = observacionesFormalizacionIF;
    }

    public String getObservacionesFormalizacionIF() {
        return observacionesFormalizacionIF;
    }

    public void setFechaInicioRubricaNafin(String fechaInicioRubricaNafin) {
        this.fechaInicioRubricaNafin = fechaInicioRubricaNafin;
    }

    public String getFechaInicioRubricaNafin() {
        return fechaInicioRubricaNafin;
    }

    public void setFechaFinRubricaNafin(String fechaFinRubricaNafin) {
        this.fechaFinRubricaNafin = fechaFinRubricaNafin;
    }

    public String getFechaFinRubricaNafin() {
        return fechaFinRubricaNafin;
    }

    public void setNivelServicioRubricaNafin(String nivelServicioRubricaNafin) {
        this.nivelServicioRubricaNafin = nivelServicioRubricaNafin;
    }

    public String getNivelServicioRubricaNafin() {
        return nivelServicioRubricaNafin;
    }

    public void setDiasRubricaNafin(String diasRubricaNafin) {
        this.diasRubricaNafin = diasRubricaNafin;
    }

    public String getDiasRubricaNafin() {
        return diasRubricaNafin;
    }

    public void setEstatusEtapaRubricaNafin(String estatusEtapaRubricaNafin) {
        this.estatusEtapaRubricaNafin = estatusEtapaRubricaNafin;
    }

    public String getEstatusEtapaRubricaNafin() {
        return estatusEtapaRubricaNafin;
    }

    public void setObservacionesRubricaNafin(String observacionesRubricaNafin) {
        this.observacionesRubricaNafin = observacionesRubricaNafin;
    }

    public String getObservacionesRubricaNafin() {
        return observacionesRubricaNafin;
    }

    public void setFechaInicioFormalizacionNafin(String fechaInicioFormalizacionNafin) {
        this.fechaInicioFormalizacionNafin = fechaInicioFormalizacionNafin;
    }

    public String getFechaInicioFormalizacionNafin() {
        return fechaInicioFormalizacionNafin;
    }

    public void setFechaFinFormalizacionNafin(String fechaFinFormalizacionNafin) {
        this.fechaFinFormalizacionNafin = fechaFinFormalizacionNafin;
    }

    public String getFechaFinFormalizacionNafin() {
        return fechaFinFormalizacionNafin;
    }

    public void setNivelServicioFormalizacionNafin(String nivelServicioFormalizacionNafin) {
        this.nivelServicioFormalizacionNafin = nivelServicioFormalizacionNafin;
    }

    public String getNivelServicioFormalizacionNafin() {
        return nivelServicioFormalizacionNafin;
    }

    public void setDiasFormalizacionNafin(String diasFormalizacionNafin) {
        this.diasFormalizacionNafin = diasFormalizacionNafin;
    }

    public String getDiasFormalizacionNafin() {
        return diasFormalizacionNafin;
    }

    public void setEstatusEtapaFormalizacionNafin(String estatusEtapaFormalizacionNafin) {
        this.estatusEtapaFormalizacionNafin = estatusEtapaFormalizacionNafin;
    }

    public String getEstatusEtapaFormalizacionNafin() {
        return estatusEtapaFormalizacionNafin;
    }

    public void setObservacionesFormalizacionNafin(String observacionesFormalizacionNafin) {
        this.observacionesFormalizacionNafin = observacionesFormalizacionNafin;
    }

    public String getObservacionesFormalizacionNafin() {
        return observacionesFormalizacionNafin;
    }

    public void setFechaInicioCargaBO(String fechaInicioCargaBO) {
        this.fechaInicioCargaBO = fechaInicioCargaBO;
    }

    public String getFechaInicioCargaBO() {
        return fechaInicioCargaBO;
    }

    public void setFechaFinCargaBO(String fechaFinCargaBO) {
        this.fechaFinCargaBO = fechaFinCargaBO;
    }

    public String getFechaFinCargaBO() {
        return fechaFinCargaBO;
    }

    public void setNivelServicioCargaBO(String nivelServicioCargaBO) {
        this.nivelServicioCargaBO = nivelServicioCargaBO;
    }

    public String getNivelServicioCargaBO() {
        return nivelServicioCargaBO;
    }

    public void setDiasCargaBO(String diasCargaBO) {
        this.diasCargaBO = diasCargaBO;
    }

    public String getDiasCargaBO() {
        return diasCargaBO;
    }

    public void setEstatusCargaBO(String estatusCargaBO) {
        this.estatusCargaBO = estatusCargaBO;
    }

    public String getEstatusCargaBO() {
        return estatusCargaBO;
    }

    public void setObservacionesCargaBO(String observacionesCargaBO) {
        this.observacionesCargaBO = observacionesCargaBO;
    }

    public String getObservacionesCargaBO() {
        return observacionesCargaBO;
    }
    private String fechaFinFormalizacionIF                   ;
    private String nivelServicioFormalizacionIF              ;
    private String diasFormalizacionIF                       ;
    private String estatusEtapaFormalizacionIF               ;
    private String observacionesFormalizacionIF              ;
    private String fechaInicioRubricaNafin                   ;
    private String fechaFinRubricaNafin                      ;
    private String nivelServicioRubricaNafin                 ;
    private String diasRubricaNafin                          ;
    private String estatusEtapaRubricaNafin                  ;
    private String observacionesRubricaNafin                 ;
    private String fechaInicioFormalizacionNafin             ;
    private String fechaFinFormalizacionNafin                ;
    private String nivelServicioFormalizacionNafin           ;
    private String diasFormalizacionNafin                    ;
    private String estatusEtapaFormalizacionNafin            ;
    private String observacionesFormalizacionNafin           ;
    private String fechaInicioCargaBO                        ;
    private String fechaFinCargaBO                           ;
    private String nivelServicioCargaBO                      ;
    private String diasCargaBO                               ;
    private String estatusCargaBO                            ;
    private String observacionesCargaBO                      ;
    
}
