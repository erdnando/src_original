package com.nafin.docgarantias.nivelesdeservicio;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.ElementoCatalogo;

@Remote
public interface NivelesServicio {

    public boolean enviarNotificacion(Integer icEstatus, Integer icDocumento, Integer icIF, Integer icTipoDocumento, String motivoRechazo);
    public boolean guardarNivelesServicio(Map<String, String> configuracion);
    public ArrayList<com.nafin.docgarantias.nivelesdeservicio.NivelServicio> getNivelesServicio();
    public void enviarAlertaNivelServicio();
    public ArrayList<NivelServicioDocumento> getMonitorNivelServicio(Map<String,String> filtrosBusqueda);
    public ArrayList<ReporteNivelServicio> getMonitorNivelServicioExtendido(Map<String,String> filtrosBusqueda);
    public boolean guardarComentarioBitacoraProceso(Integer icProcesoBitacora, Integer icEstatus, String comentario, String icUsuario);
    public ArrayList<ElementoCatalogo> getCatTipoDocumento();
    public ArrayList<ElementoCatalogo> getCatTipoMovimiento();
    public ArrayList<ElementoCatalogo> getCatPortafolio();
    public ArrayList<ElementoCatalogo> getCatProceso();
    public ArrayList<ElementoCatalogo> getCatProducto();
    public ArrayList<ElementoCatalogo> getCatEjecutivos();
    public String getReporteMonitorNiveles(String strDirectorioPublicacion, Map<String, String> filtros, String path);
    public boolean enviarSubastas(Integer estatus, Integer icConvocatoria);
}
