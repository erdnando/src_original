package com.nafin.docgarantias;

import java.io.Serializable;

import java.util.ArrayList;

import netropology.utilerias.ElementoCatalogo;

public class DocValidacion implements Serializable {
    @SuppressWarnings("compatibility:-1288001681925241828")
    private static final long serialVersionUID = 1L;

    public DocValidacion() {
        super();
    }
    
    private Integer icSolicitud;
    private Integer icDocumento;
    private Integer icArchivo;
    private Integer icIF;
    private String nombreIF;
    private String fechaLiberacion;
    private String folioSolicitud;
    private String version;
    private String tipoDocumento;
    private Integer icTipoDocumento;
    private String nombreEjecutivoAtencion;
    private Integer icEjecutivoAtencion;
    private Integer firmasRequeridas;
    private Integer firmasRegistradas;
    private String nombreProducto;
    private String nombreEstatus;
    private String nombrePortafolio;
    private String fechaMaximaFormalizacion;
    private Integer icEstatus;
    private boolean cumplePrelacionIF;
    private String icUsuariosFirmantes;
    private ArrayList<ElementoCatalogo> usuarioFirmantesRequeridosNafin;
    private ArrayList<String> icUsuariosNafinFirmaron;
    private Integer icArchivoBO;
    // cumple Asignacion de Firma Unica, 
    // El usuario debe estar Asignado para firmar 
    // El usuario solo podr� firmar por Unica ocasion
    private boolean cumpleAFU; 
    private Integer icFicha;


    public Integer getIcSolicitud() {
        return icSolicitud;
    }

    public void setIcSolicitud(Integer icSolicitud) {
        this.icSolicitud = icSolicitud;
    }

    public Integer getIcDocumento() {
        return icDocumento;
    }

    public void setIcDocumento(Integer icDocumento) {
        this.icDocumento = icDocumento;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public String getNombreIF() {
        return nombreIF;
    }

    public void setNombreIF(String nombreIF) {
        this.nombreIF = nombreIF;
    }

    public String getFechaLiberacion() {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(String fechaLiberacion) {
        this.fechaLiberacion = fechaLiberacion;
    }

    public String getFolioSolicitud() {
        return folioSolicitud;
    }

    public void setFolioSolicitud(String folioSolicitud) {
        this.folioSolicitud = folioSolicitud;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombreEjecutivoAtencion() {
        return nombreEjecutivoAtencion;
    }

    public void setNombreEjecutivoAtencion(String nombreEjecutivoAtencion) {
        this.nombreEjecutivoAtencion = nombreEjecutivoAtencion;
    }

    public Integer getIcEjecutivoAtencion() {
        return icEjecutivoAtencion;
    }

    public void setIcEjecutivoAtencion(Integer icEjecutivoAtencion) {
        this.icEjecutivoAtencion = icEjecutivoAtencion;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getNombreEstatus() {
        return nombreEstatus;
    }

    public void setNombreEstatus(String nombreEstatus) {
        this.nombreEstatus = nombreEstatus;
    }

    public Integer getIcEstatus() {
        return icEstatus;
    }

    public void setIcEstatus(Integer icEstatus) {
        this.icEstatus = icEstatus;
    }

    public Integer getIcArchivo() {
        return icArchivo;
    }

    public void setIcArchivo(Integer icArchvo) {
        this.icArchivo = icArchvo;
    }

    public Integer getFirmasRequeridas() {
        return firmasRequeridas;
    }

    public void setFirmasRequeridas(Integer firmasRequeridas) {
        this.firmasRequeridas = firmasRequeridas;
    }

    public Integer getFirmasRegistradas() {
        return firmasRegistradas;
    }

    public void setFirmasRegistradas(Integer firmasRegistradas) {
        this.firmasRegistradas = firmasRegistradas;
    }

    public String getNombrePortafolio() {
        return nombrePortafolio;
    }

    public void setNombrePortafolio(String nombrePortafolio) {
        this.nombrePortafolio = nombrePortafolio;
    }

    public String getFechaMaximaFormalizacion() {
        return fechaMaximaFormalizacion;
    }

    public void setFechaMaximaFormalizacion(String fechaMaximaFormalizacion) {
        this.fechaMaximaFormalizacion = fechaMaximaFormalizacion;
    }

    public Integer getIcTipoDocumento() {
        return icTipoDocumento;
    }

    public void setIcTipoDocumento(Integer icTipoDocumento) {
        this.icTipoDocumento = icTipoDocumento;
    }

    public boolean isCumplePrelacionIF() {
        return cumplePrelacionIF;
    }

    public void setCumplePrelacionIF(boolean cumplePrelacionIF) {
        this.cumplePrelacionIF = cumplePrelacionIF;
    }

    public String getIcUsuariosFirmantes() {
        return icUsuariosFirmantes;
    }

    public void setIcUsuariosFirmantes(String icUsuariosFirmantes) {
        this.icUsuariosFirmantes = icUsuariosFirmantes;
    }


    public ArrayList<ElementoCatalogo> getUsuarioFirmantesRequeridosNafin() {
        return usuarioFirmantesRequeridosNafin;
    }

    public void setUsuarioFirmantesRequeridosNafin(ArrayList<ElementoCatalogo> usuarioFirmantesRequeridosNafin) {
        this.usuarioFirmantesRequeridosNafin = usuarioFirmantesRequeridosNafin;
    }

    public ArrayList<String> getIcUsuariosNafinFirmaron() {
        return icUsuariosNafinFirmaron;
    }

    public void setIcUsuariosNafinFirmaron(ArrayList<String> icUsuariosNafinFirmaron) {
        this.icUsuariosNafinFirmaron = icUsuariosNafinFirmaron;
    }

    public boolean isCumpleAFU() {
        return cumpleAFU;
    }

    public void setCumpleAFU(boolean cumpleAFU) {
        this.cumpleAFU = cumpleAFU;
    }

    public Integer getIcArchivoBO() {
        return icArchivoBO;
    }

    public void setIcArchivoBO(Integer icArchivoBO) {
        this.icArchivoBO = icArchivoBO;
    }

    public Integer getIcFicha() {
        return icFicha;
    }

    public void setIcFicha(Integer icFicha) {
        this.icFicha = icFicha;
    }
}
