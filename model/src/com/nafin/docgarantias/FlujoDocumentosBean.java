package com.nafin.docgarantias;


import com.nafin.docgarantias.formalizacion.Formalizacion;
import com.nafin.docgarantias.nivelesdeservicio.NivelesServicio;
import com.nafin.util.NamedParameterStatement;
import com.nafin.util.NamedParameterStatement.QUERYTYPE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "FlujoDocumentos", mappedName = "FlujoDocumentos")
@TransactionManagement(TransactionManagementType.BEAN)
public class FlujoDocumentosBean implements FlujoDocumentos {

    
    @Resource
    SessionContext sessionContext;

    /** Tipos de Documentos */
    private static final int SOLICITUD              = 0;
    private static final int TERMINOS_CONDICIONES   = 1001;
    private static final int REGLAMENTO_SELECTIVA   = 1002;
    private static final int REGLAMENTO_AUTOMATICA  = 1003;
    private static final int FICHA_IF_GA            = 1004;
    private static final int FICHA_NAFIN_GA         = 1005;
    private static final int ANEXO_IF               = 1006;
    private static final int FICHA_GS               = 1006;
    
    /** Estatus */
    private static final int POR_SOLICITAR          = 100; 
    private static final int EN_PROCESO             = 110;
    private static final int EN_VALIDACION          = 120;
    private static final int VALIDACION_JURIDICO    = 130;
    private static final int FORMALIZACION_IF       = 140;
    private static final int RUBRICA_NAFIN          = 150;
    private static final int FORMALIZACION_NAFIN    = 160;
    private static final int FORMALIZADO            = 170;
    private static final int FORMALIZADO_CARGA_BO   = 175;
    private static final int VALIDACION_BO          = 180;
    private static final int OPERANDO               = 190;

    private static final boolean NO_ES_RECUPERACION = false;
    private static final boolean SI_ES_RECUPERACION = true;
    
    private static final Log log = ServiceLocator.getInstance().getLog(FlujoDocumentosBean.class);
    
    public FlujoDocumentosBean() {
        super();
    }

    @Override
    public boolean avanzarDocumento(Integer icDocumento, String usuario){
        return this.avanzarDocumento(this.getDocumento(icDocumento, null), usuario);
    }
    
    @Override
    public boolean avanzarDocumento(Documento docto, String usuario) {
        return this.actualizarDocumento(docto, usuario, this.getNextEstatus(docto), null, NO_ES_RECUPERACION, null, null);
    }

    @Override
    public boolean rechazarDocumento(Documento docto, String usuario, String motivo) {
        return this.actualizarDocumento(docto, usuario, this.getEstatusRechazo(docto), motivo, NO_ES_RECUPERACION, null, null);
    }
    
    /** Avanza documento cuando esta en carga de Base de Operacion y el usuario selecciona No Aplica, el sieguiente
     * estatus sera Operando */
    @Override
    public boolean avanzarDocumentoNoAplicaCargaBO(Integer icDoc, String usuario, Integer icIF) {
        Documento docto = this.getDocumento(icDoc, icIF);
        return this.actualizarDocumento(docto, usuario, OPERANDO, null, NO_ES_RECUPERACION, icIF, null);
    }

    @Override
    public boolean avanzarDocumentoCargaBO(Integer icDoc, String usuario, Integer icIF) {
        Documento docto = this.getDocumento(icDoc, icIF);
        return this.actualizarDocumento(docto, usuario, this.getNextEstatus(docto), null, NO_ES_RECUPERACION, icIF, null);
    }    
    
    @Override
    public boolean rechazarDocumentoCargaBO(Integer icDoc, String usuario, Integer icIF,  String motivo) {
        Documento docto = this.getDocumento(icDoc, icIF);
        return this.actualizarDocumento(docto, usuario, getEstatusRechazo(docto), motivo, NO_ES_RECUPERACION, icIF, null);
    }
    /** elimina el documento formalizado 
     * y regresa el documento y la solicitud al estatus en proceso
     * */
    @Override
    public boolean recuperacionDocumento(Documento docto, String usuario) {
        String motivo = "El ejecutivo realizó la recuperación del documento.";
        return this.actualizarDocumento(docto,  usuario, EN_PROCESO, motivo, SI_ES_RECUPERACION, null, null);
    }// end recuperacionDocumento
    
    /**
     * Avanza el documento en el momento en que un IF lo esta formalizando
     * Verifica que el documento pueda ser avanzado de estatus, dependiendo del numero de firmas parametrizado
     * Si se pasa motivo diferente de null, se trata como un rechazo y se regresa al estatus correspondiente
     * */
    @Override
    public boolean avanzarDocumentoIF(Integer icDocumento, String usuario, Integer icIF, String motivo){
        Documento docto = this.getDocumento(icDocumento, icIF);
        if(motivo != null){ 
            return this.actualizarDocumento(docto,  usuario,this.getEstatusRechazo(docto), motivo, NO_ES_RECUPERACION, icIF, null);
        }
        else{
            if (this.cumpleFirmasRequeridas(icDocumento, icIF)){
                return this.actualizarDocumento(docto, usuario, this.getNextEstatus(docto), null, NO_ES_RECUPERACION, icIF, null);
            }
        }
        return false;
    }    
    
    /**
     * Avanza el documento en el momento de la Formalizacion NAFIN
     * Si se pasa motivo diferente de null, se trata como un rechazo y se regresa al estatus correspondiente
     * */    
    @Override
    public boolean avanzarDocumentoFormalizacionNafin(Integer icDocumento, String usuario, Integer icIF, String motivo){
        Documento docto = this.getDocumento(icDocumento, icIF);
        if(motivo != null){ 
            return this.actualizarDocumento(docto, usuario, this.getEstatusRechazo(docto), motivo, NO_ES_RECUPERACION, icIF, null);
        }
        else{
            if (this.cumpleFirmasRequeridasNAFIN(icDocumento, icIF)){
                return this.actualizarDocumento(docto, usuario, this.getNextEstatus(docto), null, NO_ES_RECUPERACION, icIF, null);
            }
        }
        return false;
    }    
    
    /**
     * Avanza el documento en el momento en que el Administrador da si Visto Bueno para la Formalizacion NAFIN
     * Si se pasa motivo diferente de null, se trata como un rechazo y se regresa al estatus correspondiente
     * */    
    @Override
    public boolean avanzarDocumentoVistoBuenoNafin(Integer icDocumento, String usuario, Integer icIF, String motivo, ArrayList<String> usuariosFirmantes){
        Documento docto = this.getDocumento(icDocumento, icIF);
        if(motivo != null){ 
            return this.actualizarDocumento(docto, usuario, this.getEstatusRechazo(docto), motivo, NO_ES_RECUPERACION, icIF, null);
        }
        else{
            return this.actualizarDocumento(docto, usuario, this.getNextEstatus(docto), null, NO_ES_RECUPERACION, icIF, usuariosFirmantes);
        }
    }  
    
    
    private boolean actualizarDocumento(Documento docto, String usuario, Integer nextEstatus, String motivo, boolean esRecuperacion, Integer icIF, ArrayList<String> usuariosFirmantes) {
        log.info("FlujoDocumentosBean.netxEstatus(E)");
        boolean exito = false;
        // UPDATE DOCUMENTO NUEVO ESTATUS
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {            
            con.conexionDB();            
            if(motivo!= null && !nextEstatus.equals(FORMALIZADO_CARGA_BO)){
                log.info("actualizarDocumento.EsRechazo");
                StringBuilder strSQLComments = new StringBuilder();
                strSQLComments.append(" INSERT INTO GDOC_COMENTARIO_SOLICITUD (IC_COMENTARIO_SOLICITUD, IC_SOLICITUD, TG_DESCRIPCION, DT_FECHA_REGISTRO, IC_USUARIO_REGISTRO, IC_ESTATUS)\n" + 
                            " VALUES (SEQ_GDOC_COMENTARIO_SOLICITUD.NEXTVAL, ?, ?, SYSDATE, ?, (SELECT IC_ESTATUS FROM GDOC_SOLICITUD WHERE IC_SOLICITUD = ? )) ");
                ps = con.queryPrecompilado(strSQLComments.toString());
                ps.setInt(1, docto.getIcSolicitud());
                ps.setString(2, motivo);
                ps.setString(3, usuario);
                ps.setInt(4, docto.getIcSolicitud());
                ps.executeUpdate();                        
            }            
            if (icIF != null ){
                log.info("actualizarDocumento.EsIF_ESPECIFICO");                    
                String sqlAvanzaDoctoIF = " update GDOCREL_DOCUMENTO_IF set IC_ESTATUS = ? WHERE IC_DOCUMENTO = ? AND ic_IF = ? ";
                ps = con.queryPrecompilado(sqlAvanzaDoctoIF);
                ps.setInt(1, nextEstatus);
                ps.setInt(2, docto.getIcDocumento());
                ps.setInt(3, icIF);
                ps.executeUpdate();                
            }
            else{                
                log.info("actualizarDocumento.EsSolicitud");
                // ASIGNAR EJECUTIVO A LA SOLICITUD CUANDO SE LIBERA 
                String sqlUsuarioEjecutivo = "";
                if(nextEstatus.equals(EN_PROCESO) && !esRecuperacion){
                    log.info("Asignando usuario Ejecutivo");
                    SolicitudDoc solicitud = (SolicitudDoc)docto;
                    List intermediarios  = solicitud.getSIntermediarios();
                    if (intermediarios.size() == 1){
                        String usuarioAsignado = this.getUsuarioAsignado(solicitud);
                        if (usuarioAsignado != null){
                            sqlUsuarioEjecutivo = ", IC_USUARIO_EJECUTIVO =  '"+ usuarioAsignado+"' ";
                            }
                        else{
                            log.info("No se encontro usuario parametrizado para : Tipo documento: "+solicitud.getIcTipoDocumento()+ ", portafolio: "+solicitud.getPortafolio()+", IF: "+ intermediarios.get(0));
                        }
                    }
                }
                
                StringBuilder strSQL = new StringBuilder();
                strSQL.append(" UPDATE GDOC_SOLICITUD SET IC_ESTATUS = ?  "+ sqlUsuarioEjecutivo +" WHERE IC_SOLICITUD = ?  ");
                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, nextEstatus);
                ps.setInt(2, docto.getIcSolicitud());
                ps.executeUpdate();
                

                
                if(nextEstatus.equals(FORMALIZACION_IF)){
                    // realizar la dispercion del estatus para trodos los IF
                    String sqlDispersarEstatusIF = " update GDOCREL_DOCUMENTO_IF set IC_ESTATUS = ? WHERE IC_DOCUMENTO = ?  ";
                    ps = con.queryPrecompilado(sqlDispersarEstatusIF);
                    ps.setInt(1, nextEstatus);
                    ps.setInt(2, docto.getIcDocumento());
                    ps.executeUpdate();                        
                }
                
            }// FIN else
            
            if(esRecuperacion){
                log.info("Ejecutando la Recuperacion del documento");
                Files2OnBase files2OnBaseBean =    ServiceLocator.getInstance().lookup("Files2OnBase", Files2OnBase.class);
                // ELIMINAR ARCHIVOS FORMALIZADOS EN ONBASE 
                String sqlSelectArchivos = " SELECT A.IC_FOLIO_ONBASE FROM GDOC_ARCHIVO A, GDOCREL_DOCUMENTO_IF D WHERE \n" + 
                                           " D.IC_ARCHIVO = A.IC_ARCHIVO AND D.IC_DOCUMENTO = ? ";
                log.info(sqlSelectArchivos+ " params: "+ docto.getIcDocumento());
                ps = con.queryPrecompilado(sqlSelectArchivos);
                ps.setInt(1, docto.getIcDocumento());
                ps.executeQuery();
                rs = ps.executeQuery();
                while (rs.next()){
                    Integer itemNum = rs.getInt("IC_FOLIO_ONBASE");
                    if(!files2OnBaseBean.eliminaArchivo(itemNum)){
                        throw new AppException("No se pudo eliminar el archivo ["+ itemNum +"] de OnBase");
                    }else{
                        log.info("Archivo Eliminado de OnBase itemNum: "+itemNum);
                    }
                }                                             
                
                String sqlGetFArchivos = " SELECT IC_ARCHIVO FROM GDOC_DOCUMENTO WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlGetFArchivos); 
                log.info(sqlGetFArchivos+ " param: "+ docto.getIcDocumento());
                ps.setInt(1, docto.getIcDocumento());
                rs = ps.executeQuery();
                Integer archivo = null;
                while (rs.next()){
                    archivo = rs.getInt("IC_ARCHIVO");
                }
                
                String sqlGetArchivoFicha = "SELECT IC_ARCHIVO FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlGetArchivoFicha);
                ps.setInt(1, docto.getIcDocumento());
                ArrayList<Integer> icArchivosFicha = new ArrayList<>();
                rs = ps.executeQuery();
                while (rs.next()){
                    icArchivosFicha.add(rs.getInt("IC_ARCHIVO"));
                }
                
                // ELIMINAR LAS RELACIONES DE ARCHIVOS CON IF DEL DOCUMENTO Y LOS ARCHIVOS 
                String sqlDeleteDocsIF = " DELETE FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ? ";
                String sqlDeleteArchivo = " DELETE FROM GDOC_ARCHIVO WHERE IC_ARCHIVO = ? ";

                ps = con.queryPrecompilado(sqlDeleteDocsIF); 
                log.info(sqlDeleteDocsIF+ " param: "+ docto.getIcDocumento());
                ps.setInt(1, docto.getIcDocumento());
                ps.executeUpdate();
                
                if (!icArchivosFicha.isEmpty()){
                    for(Integer icArchivoFicha : icArchivosFicha){
                        log.info("Eliminando icArchivos de FichasIF: "+icArchivoFicha);
                        ps = con.queryPrecompilado(sqlDeleteArchivo); 
                        ps.setInt(1, icArchivoFicha);
                        ps.executeUpdate();
                    }    
                }
                // ELIMINAR EL DOCUMENTO
                String sqlUpdateDoc = " UPDATE GDOC_DOCUMENTO SET IC_ARCHIVO = NULL, DT_FECHA_REGISTRO = " +
                    "   ( select s.DT_FECHA_CREACION  from GDOC_DOCUMENTO d, GDOC_SOLICITUD s where " +
                    " d.IC_SOLICITUD = s.IC_SOLICITUD AND d.IC_DOCUMENTO = ? ) WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlUpdateDoc); 
                log.info(sqlUpdateDoc+ " param: "+ docto.getIcDocumento());
                ps.setInt(1, docto.getIcDocumento());
                ps.setInt(2, docto.getIcDocumento());
                ps.executeUpdate();       
                
                if (archivo != null){
                    ps = con.queryPrecompilado(sqlDeleteArchivo); 
                    log.info(sqlDeleteArchivo+ " param: "+ archivo);
                    ps.setInt(1, archivo);
                    ps.executeUpdate();                
                }
                
                log.info("Eliminando los acuse de formalizacion del documento");
                String sqlDeleteAcuse = " DELETE FROM GDOC_ACUSE_FORMALIZACION WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlDeleteAcuse); 
                log.info(sqlDeleteAcuse+ " param: "+ docto.getIcDocumento());
                ps.setInt(1, docto.getIcDocumento());
                ps.executeUpdate();  
                
                log.info("Finalizo la Recuperacion del documento");
            }
            // REGISTRAR MOVIMIENTO EN BITACORA
            // revisar los IF involicrados con el documento
            if (nextEstatus.equals(FORMALIZACION_IF)){
                log.info("Revisando Los IF para registroBitacora");
                String sqlFindIF = " SELECT IC_IF FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO = ? ";
                ps = con.queryPrecompilado(sqlFindIF); 
                ps.setInt(1, docto.getIcDocumento());
                rs = ps.executeQuery();
                String registros = " INSERT INTO GDOC_BITACORA_DOC \n" + 
                    " (IC_PROCESO_DOCTO, IC_SOLICITUD, IC_ESTATUS, IC_DOCUMENTO, DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, IC_IF)  \n" + 
                    " VALUES \n" + 
                    " (SEQ_GDOC_BITACORA_DOC.NEXTVAL, ?, ?, ?, SYSDATE, ?, ?) ";
                ps = con.queryPrecompilado(registros);
                while (rs.next()){
                    int icIntermediario = rs.getInt("IC_IF");
                    ps.setInt(1, docto.getIcSolicitud());
                    ps.setInt(2, nextEstatus);
                    ps.setInt(3, docto.getIcDocumento());
                    ps.setString(4, usuario);
                    ps.setInt(5, icIntermediario);
                    ps.executeUpdate();    
                }
            }
            else{
                StringBuilder strSQLBitacora = new StringBuilder();
                strSQLBitacora.append(" INSERT INTO GDOC_BITACORA_DOC \n" + 
                    " (IC_PROCESO_DOCTO, IC_SOLICITUD, IC_ESTATUS, IC_DOCUMENTO, DT_FECHA_MOVIMIENTO, IC_USUARIO_REGISTRO, IC_IF)  \n" + 
                    " VALUES \n" + 
                    " (SEQ_GDOC_BITACORA_DOC.NEXTVAL, ?, ?, ?, SYSDATE, ?, ?) ");
                ps = con.queryPrecompilado(strSQLBitacora.toString());
                ps.setInt(1, docto.getIcSolicitud());
                ps.setInt(2, nextEstatus);
                if (docto.getIcDocumento()==null){
                    ps.setNull(3, java.sql.Types.INTEGER);
                }
                else{
                    ps.setInt(3, docto.getIcDocumento());
                }
                ps.setString(4, usuario);
                if (icIF != null ){
                    ps.setInt(5, icIF);
                }else{
                    ps.setNull(5, java.sql.Types.INTEGER);
                }
                ps.executeUpdate();                       
            }
            exito = true;
            
            if (nextEstatus.equals(FORMALIZADO) || (nextEstatus.equals(FORMALIZADO_CARGA_BO) && motivo == null )){
                Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
                exito = formalizacionBean.guardaPrelacionDocumentosIF(icIF, docto.getIcTipoDocumento());
            }
            
            if (exito){
                // ENVIAR CORREO NOTIFICACION CORRESPONDIENTE 
                log.info("Enviando correos notificacion...");
                NivelesServicio nivelesServicioBean = ServiceLocator.getInstance().lookup("NivelesServicio",NivelesServicio.class);
                nivelesServicioBean.enviarNotificacion(nextEstatus, docto.getIcDocumento(), icIF, docto.getIcTipoDocumento(), motivo);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error guardar en Base de Datos", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
            log.info("FlujoDocumentosBean.netxEstatus(S)");
        }       
        return exito;
    } // end actualizarDocumento



    private int getNextEstatus(Documento docto){
        Integer nuevoEstatus = 0;
        int tipoDoc = docto.getIcTipoDocumento();
        switch (docto.getEstatus()) {
        case POR_SOLICITAR:         nuevoEstatus = EN_PROCESO;  break;
        case EN_PROCESO:            nuevoEstatus = EN_VALIDACION;  break;
        case EN_VALIDACION:         nuevoEstatus = 
                                       tipoDoc == TERMINOS_CONDICIONES ?  VALIDACION_JURIDICO :FORMALIZACION_IF;
                                    break;
        case VALIDACION_JURIDICO:   nuevoEstatus = FORMALIZACION_IF;  break;
        case FORMALIZACION_IF:      nuevoEstatus = RUBRICA_NAFIN;  break;
        case RUBRICA_NAFIN:         nuevoEstatus = FORMALIZACION_NAFIN;  break;
        case FORMALIZACION_NAFIN:   nuevoEstatus = 
                                        (tipoDoc == FICHA_NAFIN_GA ||
                                         tipoDoc == FICHA_IF_GA ||
                                         tipoDoc == FICHA_GS)
                                        ? FORMALIZADO_CARGA_BO : FORMALIZADO;  
                                    break;
        case FORMALIZADO:           nuevoEstatus = OPERANDO;  break;
        case FORMALIZADO_CARGA_BO:  nuevoEstatus = VALIDACION_BO;  break;
        case VALIDACION_BO:         nuevoEstatus = OPERANDO;  break;
        default:                    nuevoEstatus = 0;              break;
        }
        return nuevoEstatus;
    } // end getNextEstatus


    
    
    private int getEstatusRechazo(Documento docto){
        int nuevoEstatus = 0;
        if (docto.getEstatus() == VALIDACION_BO){
            nuevoEstatus = FORMALIZADO_CARGA_BO;
        }
        else if(docto.getEstatus() == EN_PROCESO){
            nuevoEstatus = POR_SOLICITAR;
        }
        else{
            nuevoEstatus = EN_PROCESO;    
        }   
        return nuevoEstatus;
    } // end getEstatusRechazo


    
    private String getUsuarioAsignado(SolicitudDoc sol){
        StringBuilder sql = new StringBuilder();
        AccesoDB con = new AccesoDB();
        String usuario = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        try {
            con.conexionDB();
            sql.append(" SELECT d.IC_USUARIO FROM Gdoc_Usuario_Documento d, Gdoc_Usuario_Portafolio p, GDOC_USUARIO_IF f " + 
                " WHERE d.IC_USUARIo = p.ic_usuario AND d.IC_USUARIo = f.ic_usuario  " + 
                " AND p.IS_VERSION_ACTUAL=1 AND f.IS_VERSION_ACTUAL=1 and d.IS_VERSION_ACTUAL=1 " + 
                " AND d.IC_TIPO_DOCUMENTO = ? AND p.IC_PORTAFOLIO = ? AND f.IC_IF = ? ");
            ps = con.queryPrecompilado(sql.toString());
            ps.setInt(1, sol.getIcTipoDocumento());
            ps.setInt(2, sol.getPortafolio());
            ps.setInt(3, sol.getSIntermediarios().get(0));
            log.info("Buscar usuario Asignado por parametrización:\n"+sql.toString());
            log.info("params: "+sol.getIcTipoDocumento()+", "+sol.getPortafolio()+", "+ sol.getSIntermediarios().get(0));
            rs = ps.executeQuery();
            while(rs.next()){
                usuario = rs.getString("IC_USUARIO");
                if ( rs.getRow() > 1){
                    log.error("EXISTEN USUARIOS CON LA MISMA PARAMETRIZACIÓN, SOLO SE CONSIDERA EL PRIMERO");
                }
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getStackTrace());
            throw new AppException("Error al consultar la Base de Datos", e);
        } 
        finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }       
        return usuario;
    }
    
    /** Regresa un Documento con los valores requeridos para avanzar su estado
     * 
     * */
    private Documento getDocumento(Integer icDocumento, Integer icIF){
        Documento  doc = null;
        StringBuilder sql = new StringBuilder();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;        
        try {
            con.conexionDB();
            if  (icIF == null){
                sql.append(" SELECT d.IC_DOCUMENTO, d.IC_TIPO_DOCUMENTO, s.IC_SOLICITUD, s.IC_ESTATUS, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO " +
                    " FROM GDOC_DOCUMENTO d, GDOC_SOLICITUD s \n" +
                    " WHERE s.IC_SOLICITUD = d.IC_SOLICITUD and d.IC_DOCUMENTO = ? ");
                ps = con.queryPrecompilado(sql.toString());
                ps.setInt(1, icDocumento);
            }
            else{
                sql.append("SELECT d.IC_DOCUMENTO, d.IC_TIPO_DOCUMENTO, s.IC_SOLICITUD, dif.IC_ESTATUS, CONCAT(CONCAT(EXTRACT(YEAR FROM s.DT_FECHA_CREACION), '-'),s.IC_FOLIO) as FOLIO \n" + 
                "                 FROM GDOC_DOCUMENTO d, GDOC_SOLICITUD s, GDOCREL_DOCUMENTO_IF dif \n" + 
                "                 WHERE s.IC_SOLICITUD = d.IC_SOLICITUD and d.IC_DOCUMENTO = dif.IC_DOCUMENTO AND d.IC_DOCUMENTO = ?  AND dif.IC_IF = ? ");
                ps = con.queryPrecompilado(sql.toString());
                ps.setInt(1, icDocumento);
                ps.setInt(2, icIF);
            }
            rs = ps.executeQuery();
            doc = new Documento();
            while(rs.next()){
                doc.setEstatus(rs.getInt("IC_ESTATUS"));
                doc.setIcTipoDocumento(rs.getInt("IC_TIPO_DOCUMENTO"));
                doc.setIcDocumento(rs.getInt("IC_DOCUMENTO"));
                doc.setIcSolicitud(rs.getInt("IC_SOLICITUD"));
            }
            ArrayList<Integer> intermediarios = new ArrayList<>();
            if (icIF == null){
                String sqlIFs = " SELECT IC_IF FROM GDOCREL_DOCUMENTO_IF WHERE IC_DOCUMENTO =  ? ";
                ps = con.queryPrecompilado(sqlIFs);
                ps.setInt(1, icDocumento);
                rs = ps.executeQuery();
                while(rs.next()){
                    intermediarios.add(rs.getInt("IC_IF"));
                }
            }
            else{
                intermediarios.add(icIF);
            }
            doc.setIcIFs(intermediarios);
        } catch (SQLException | NamingException e) {
            log.error(e.getStackTrace());
            throw new AppException("Error guardar en Base de Datos", e);
        }finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return doc;
    }
    
    /**
     * Valida si un el documento enviado se puede avanzar al siguiente estatus
     * aplica para cuando el IF esta formalizando el documento unicamente
     * */
    private boolean cumpleFirmasRequeridas(Integer icDocumento, Integer icIF){
        boolean avanzar = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;        
        boolean documentoEnEstatusFormalizacionIF = false;
        try {
            con.conexionDB();
            String sqlPrimeraValidacon = " SELECT COUNT(1) as TOTAL FROM GDOCREL_DOCUMENTO_IF  WHERE IC_DOCUMENTO = ? AND IC_IF = ? AND IC_ESTATUS = 140 ";
            ps = con.queryPrecompilado(sqlPrimeraValidacon);
            ps.setInt(1, icDocumento);
            ps.setInt(2, icIF);
            rs = ps.executeQuery();
            int totalEncontrado=0;
            while(rs.next()){
                totalEncontrado = rs.getInt("TOTAL");
                documentoEnEstatusFormalizacionIF = totalEncontrado > 0;
            }         

            if(documentoEnEstatusFormalizacionIF){
                int firmasRequeridasIF = 1;
                int firmados = 0;
                String sqlParametrosIF = " SELECT \n" + 
                    "(SELECT IN_FIRMAS_REQUERIDO FROM GDOC_PARAMETRO_IF WHERE IC_IF = :IC_IF ) AS REQUERIDO,\n" + 
                    "(SELECT COUNT(TG_FOLIO_PKI) FROM GDOC_ACUSE_FORMALIZACION WHERE IC_DOCUMENTO= :IC_DOCUMENTO AND IC_IF = :IC_IF and IC_TIPO_FIRMA = 2) AS FIRMADOS FROM DUAL ";
                NamedParameterStatement nps = new NamedParameterStatement(con.getConnection(), sqlParametrosIF, QUERYTYPE.QUERY);
                nps.setInt("IC_IF", icIF);
                nps.setInt("IC_DOCUMENTO", icDocumento);
                rs = nps.executeQuery();
                while(rs.next()){
                    firmasRequeridasIF = rs.getInt("REQUERIDO");
                    firmados = rs.getInt("FIRMADOS");
                }
                if (firmasRequeridasIF ==1){
                    return (firmasRequeridasIF ==  firmados);
                }
                else{
                    // verificar si el documento esta parametrizado como firma mancomunada     
                    // Por tipo de Documento
                    String sqlFirmaTipoDocumento = " SELECT COUNT(IS_FIRMA_MANCOMUNADA) REQUIERE_FIRMA_MANCOMUNADA \n" + 
                        "FROM GDOC_TIPO_FIRMADOC_IF tf,  GDOC_DOCUMENTO d \n" + 
                        "WHERE d.IC_TIPO_DOCUMENTO = tf.IC_TIPO_DOCUMENTO \n" + 
                        "AND tf.IC_IF              = :IC_IF \n" + 
                        "AND d.IC_DOCUMENTO        = :IC_DOCUMENTO ";
                    nps = new NamedParameterStatement(con.getConnection(), sqlFirmaTipoDocumento, QUERYTYPE.QUERY);
                    nps.setInt("IC_IF", icIF);
                    nps.setInt("IC_DOCUMENTO", icDocumento);
                    rs = nps.executeQuery();
                    int tipoDocumentoRequiereFirmaMancomunada = 0;
                    while(rs.next()){
                        tipoDocumentoRequiereFirmaMancomunada = rs.getInt("REQUIERE_FIRMA_MANCOMUNADA");
                    }
                    if (tipoDocumentoRequiereFirmaMancomunada > 0){
                        return (firmasRequeridasIF ==  firmados);
                    }
                    else{
                        return firmados > 0;
                    }
                    // Por tipo de portafolio
                }
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            throw new AppException("Error al consultar la Base de Datos cumpleFirmasRequeridas", e);
        }finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }                
        return avanzar;
    }
    
    /**
     * Valida si un el documento enviado se puede avanzar al siguiente estatus
     * revisa el numero de firmantes definido por el administrador al dar visto bueno
     * */
    private boolean cumpleFirmasRequeridasNAFIN(Integer icDocumento, Integer icIF){
        boolean avanzar = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;        
        boolean documentoEnEstatusFormalizacionNAFIN = false;
        try {
            con.conexionDB();
            String sqlPrimeraValidacon = " SELECT COUNT(1) as TOTAL FROM GDOCREL_DOCUMENTO_IF  WHERE IC_DOCUMENTO = ? AND IC_IF = ? AND IC_ESTATUS = 160  ";
            ps = con.queryPrecompilado(sqlPrimeraValidacon);
            ps.setInt(1, icDocumento);
            ps.setInt(2, icIF);
            rs = ps.executeQuery();
            int totalEncontrado=0;
            while(rs.next()){
                totalEncontrado = rs.getInt("TOTAL");
                documentoEnEstatusFormalizacionNAFIN = totalEncontrado > 0;
            }         

            if(documentoEnEstatusFormalizacionNAFIN){
                ArrayList<String> firmados = new ArrayList<>();
                ArrayList<String> requeridos = new ArrayList<>();
                String sqlFirmados = " select IC_USUARIO from gdoc_acuse_formalizacion where IC_TIPO_FIRMA = 4 and IC_IF = :IC_IF AND IC_DOCUMENTO = :IC_DOCUMENTO ";
                NamedParameterStatement nps = new NamedParameterStatement(con.getConnection(), sqlFirmados, QUERYTYPE.QUERY);
                nps.setInt("IC_IF", icIF);
                nps.setInt("IC_DOCUMENTO", icDocumento);
                rs = nps.executeQuery();
                while(rs.next()){
                    firmados.add(rs.getString("IC_USUARIO"));
                }
                String sqlRequeridos = " SELECT IC_USUARIO FROM  GDOC_FIRMANTES_REQUERIDOS WHERE IC_IF = :IC_IF AND IC_DOCUMENTO = :IC_DOCUMENTO ";
                nps = new NamedParameterStatement(con.getConnection(), sqlRequeridos, QUERYTYPE.QUERY);
                nps.setInt("IC_IF", icIF);
                nps.setInt("IC_DOCUMENTO", icDocumento);
                rs = nps.executeQuery();
                while(rs.next()){
                    requeridos.add(rs.getString("IC_USUARIO"));
                }
                if (requeridos.size() != firmados.size()){
                    avanzar =  false;
                }
                else {
                    avanzar = requeridos.containsAll(firmados);    
                }
            }
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            throw new AppException("Error al consultar la Base de Datos cumpleFirmasRequeridasNAFIN", e);
        }finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }                
        return avanzar;
    }
    
    
}
