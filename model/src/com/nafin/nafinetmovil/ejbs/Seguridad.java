package com.nafin.nafinetmovil.ejbs;

import com.nafin.nafinetmovil.utils.UsuarioBasico;

import javax.ejb.Remote;

@Remote
public interface Seguridad
{
  public boolean isValidUser(String Usuario, String Contrasena) throws Exception;
	public String getPerfilUsuario(String idUsuario) throws Exception;
	public UsuarioBasico getUsuario(String idUsuario) throws Exception;
}