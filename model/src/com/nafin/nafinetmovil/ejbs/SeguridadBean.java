package com.nafin.nafinetmovil.ejbs;

import com.chermansolutions.ldap.oid.user.proxy.OIDWebServiceProxy;
import com.chermansolutions.ldap.oid.user.proxy.com_chermansolutions_ldap_oid_user_UserBean;

import com.nafin.nafinetmovil.utils.Constantes;
import com.nafin.nafinetmovil.utils.UsuarioBasico;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

@Stateless(name = "SeguridadMovilEJB" , mappedName = "SeguridadMovilEJB")
@TransactionManagement(TransactionManagementType.BEAN)

public class SeguridadBean implements Seguridad {
	private static String strURL_OID_WS = null;
	private static String strINSTANCE_OID_WS = null;
	private static String oidInstance = null;
	private final static String PREFIJO_GRUPO = Constantes.PREFIX_OID_WS;
	
	public SeguridadBean(){
		String lsKey1 = "URL_OID_WS";
		String lsKey2 = "INSTANCE_OID_WS";
	
		ResourceBundle bundle = ResourceBundle.getBundle("nafinetmovil", Locale.ENGLISH);
		SeguridadBean.strURL_OID_WS = bundle.getString(lsKey1);
		//System.out.println("ejbCreate WS: "+strURL_OID_WS);
		SeguridadBean.strINSTANCE_OID_WS = bundle.getString(lsKey2);
		SeguridadBean.oidInstance = strINSTANCE_OID_WS;
	
		//System.out.println("key1 = " + lsKey1 + ", " + "value = " + strURL_OID_WS);
		//System.out.println("key2 = " + lsKey2 + ", " + "value = " + strINSTANCE_OID_WS);
	}


	/**
	 * M�todo para validar el usuario que ingresa a la aplicaci�n
	 * @param Usuario login del usuario
	 * @param Contrasena Contrase�a del usuario
	 * @return true Si la autenticacion del usuario es correcta o false de lo contrario
	 * @throws Exception
	 */
	public boolean isValidUser(String Usuario, String Contrasena) throws Exception {
		System.out.println("SeguridadBean::isValidUser(E)");
		//Boolean bValidUser = new Boolean(false); //CAMBIO VIEJO ARGUS
	
		//System.out.println("isValidUser WS: "+strURL_OID_WS);
	
		OIDWebServiceProxy proxyWSOID = new OIDWebServiceProxy(strURL_OID_WS);// ARGUS 2.5
	
		boolean bValidUser = false; //ARGUS 2.4
		//proxyWSOID._setSoapURL(strURL_OID_WS); //ARGUS VIEJITO
	
		try {
			if (Usuario == null || Usuario.equals("") || Contrasena == null || "".equals(Contrasena)) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" Usuario=" + Usuario);
			throw e;
		}
	
		try {
			//System.out.println("SeguridadBean:Usuario: "+Usuario);
			//System.out.println("SeguridadBean:Contrasena: "+Contrasena);
			//System.out.println("SeguridadBean:oidInstance: "+this.oidInstance);
			bValidUser = proxyWSOID.isValidUser(Usuario,Contrasena,SeguridadBean.oidInstance);
			System.out.println("bValidUser: "+bValidUser);
			//if (bValidUser.booleanValue()) { CAMBIO ARGUS VIEJITO
			if (bValidUser) { //ARGUS 2.5
				System.out.println("bValidUser: "+bValidUser);
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error al ejecutar el metodo isValidUser");
		}
		System.out.println("SeguridadBean::isValidUser(S)");
	
		//return bValidUser.booleanValue(); //CAMBIO ARGUS VIEJITO
		return bValidUser; //ARGUS 2.5
	}

	/**
	 * Obtiene el perfil de usuario
	 * @param idUsuario Login del usuario
	 * @return Cadena con el nombre del perfil asociado al usuario
	 * @throws Exception
	 */
	public String getPerfilUsuario(String idUsuario) throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		System.out.println("SeguridadBean::getPerfilUsuario(E)");
		OIDWebServiceProxy proxyWSOID = new OIDWebServiceProxy(strURL_OID_WS);// ARGUS 2.5
		String dnPerfil = null; 
		try {
			if (idUsuario == null || idUsuario.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
		} catch(Exception e) {
			System.out.println("El tiempo de conexion es muy lento, intente de nuevo o mas tarde. " + e.getMessage() +
					" idUsuario=" + idUsuario);
			throw e;
		}
		//***************************************************************************************
		
		try {
			dnPerfil = proxyWSOID.userRoles(idUsuario,
					PREFIJO_GRUPO, SeguridadBean.oidInstance);
			if (dnPerfil == null) {
				dnPerfil = "";
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" idUsuario=" + idUsuario);
			throw e;
		}
		System.out.println("SeguridadBean::getPerfilUsuario(S)");
		
		return this.getPerfilFormato(dnPerfil);
	}
	
	
	/**
	* Obtiene la informaci�n del usuario especificado <br>
	* Con fines de consulta el perfil del usuario puede ser obtenido de
	* dos formas: mediante {@link #getPerfilUsuario(String) getPerfilUsuario} <br>
	* o mediante getPerfil() del objeto "Usuario"
	* @param idUsuario Login del usuario
	* @return Objeto con la informaci�n del usuario
	*/
	public UsuarioBasico getUsuario(String idUsuario) throws Exception {
		System.out.println("SeguridadBean::getUsuario(E)");
		UsuarioBasico usuario = new UsuarioBasico();
		OIDWebServiceProxy proxyWSOID = new OIDWebServiceProxy(strURL_OID_WS);// ARGUS 2.5
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. "+e.getMessage()+" idUsuario=" + idUsuario);
			throw e;
		}
		
		try{
			com_chermansolutions_ldap_oid_user_UserBean userWebService =
					proxyWSOID.userInformation(idUsuario, SeguridadBean.oidInstance);
		
			/*System.out.println("Los valores que se obtienen del Webservices: ");
			System.out.println("El login: "+userWebService.getCommonName());
			System.out.println("El email: "+userWebService.getMail());
			System.out.println("El apellido paterno: "+userWebService.getMiddleName());
			System.out.println("El apellido materno: "+userWebService.getSurname());
			System.out.println("El nombre: "+userWebService.getGivenName());
			System.out.println("El CP: "+userWebService.getPostalCode());
			System.out.println("La Ciudad: "+userWebService.getCity());
			System.out.println("El Estado: "+userWebService.getState());
			System.out.println("La Calle: "+userWebService.getStreet());
			System.out.println("Tipo Afiliado: "+userWebService.getTipoafiliado());
			System.out.println("Cve Afiliado: "+userWebService.getClaveafiliado());*/
		
			//Llena los datos del objeto usuario, con aquellos provenientes del Web Service
		
			usuario.setCn(userWebService.getCommonName());
			usuario.setMail(userWebService.getMail());
			usuario.setApellidoPaterno(userWebService.getMiddleName());
			usuario.setApellidoMaterno(userWebService.getSurname());
			usuario.setNombre(userWebService.getGivenName());
			usuario.setCP(userWebService.getPostalCode());
			usuario.setCiudad(userWebService.getCity());
			usuario.setEstado(userWebService.getState());
			usuario.setCalle(userWebService.getStreet());
			usuario.setTipoAfiliado(userWebService.getTipoafiliado());
			usuario.setCveAfiliado(userWebService.getClaveafiliado());
			
			String perfil = this.getPerfilUsuario(idUsuario);
			usuario.setPerfil(perfil);
		
			//		usuario.setCn("02265084");
			//      usuario.setMail("gaparicio@servicio.nafin.gob.mx");
			//      usuario.setApellidoPaterno("APARICIO");
			//      usuario.setApellidoMaterno("GUERRERO");
			//      usuario.setNombre("GILBERTO");
			//      usuario.setCP("00000");
			//      usuario.setCiudad("DF");
			//      usuario.setEstado("MEXICO");
			//      usuario.setCalle("INSURGENTES");
			//      usuario.setTipoAfiliado("I");
			//      usuario.setCveAfiliado("11");
			//		
			//		usuario.setPerfil("NE_ADMIN IF");
		} catch(Exception e) {
			System.out.println("El tiempo de conexion es muy lento, intente de nuevo o mas tarde. "+e.getMessage()+" idUsuario=" + idUsuario);
			throw e;
		}
		System.out.println("SeguridadBean::getUsuario(S)");
		
		return usuario;
	}
	
	
	//  	private String hashPassword(String password){
	//        String hashword = null;
	//        MessageDigest md5;
	//        try {
	//            md5 = MessageDigest.getInstance("MD5");
	//            md5.update(password.getBytes());
	//            BigInteger hash = new BigInteger(1, md5.digest());
	//            hashword = hash.toString(16).toUpperCase();
	//        } catch (NoSuchAlgorithmException e) {
	//            System.out.println("ERROR hashPassword:"+e.getMessage());
	//        }
	//        return hashword;
	//    }
	
	/**
	* Convierte el perfil de formato DN a N@E
	* Por ejemplo si recibe como dnPerfil
	* cn=CAPASIS_ADMIN, cn=Groups, dn=netro, dn=com
	* lo convierte en:
	* "CAPASIS_ADMIN"
	* Este metodo asume que la variable dnPerfil no es una cadena nula
	*/
	private String getPerfilFormato(String dnPerfil) {
	
		System.out.println("SeguridadBean::getPerfilFormato(E)");
		String sPerfil = "";
		StringTokenizer st = new StringTokenizer(dnPerfil,",");
		System.out.println("SeguridadBean::getPerfilFormato::dnPerfil=" + dnPerfil);
		
		while(st.hasMoreTokens()) {
			String elemento = st.nextToken();
			int indice = elemento.indexOf(PREFIJO_GRUPO);
			if(indice != -1) {
				sPerfil = elemento.substring(indice, elemento.length());
			}
		}
		System.out.println("SeguridadBean::getPerfilFormato::sPerfil=" + sPerfil);
		System.out.println("SeguridadBean::getPerfilFormato(S)");
		return sPerfil;
	}
	
}