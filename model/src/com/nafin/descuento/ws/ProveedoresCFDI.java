package com.nafin.descuento.ws;

public class ProveedoresCFDI implements java.io.Serializable {
	private String rfcReceptor;
	private String nombreReceptor;
	private String calle;

	private String colonia;
	private String localidad;
	private String delegacion_O_Municipio;
	private String estado;
	private String pais;
	private String codigoPostal;
	private String numeroSirac;
	private String fechaOperacion;
	private String montoTotalDescuento;
	private String montoTotalInteres;
	private String montoTotalRecibir;
	private String moneda;
  
  private String claveEpo;
  private String nombreEpo;
  private String totalDocumentos;
  private String numeroDocumentos;
  private String correoElectronico;
	
	
	public ProveedoresCFDI() {
	}


	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}


	public String getRfcReceptor() {
		return rfcReceptor;
	}


	public void setNombreReceptor(String nombreReceptor) {
		this.nombreReceptor = nombreReceptor;
	}


	public String getNombreReceptor() {
		return nombreReceptor;
	}


	public void setCalle(String calle) {
		this.calle = calle;
	}


	public String getCalle() {
		return calle;
	}




	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getColonia() {
		return colonia;
	}


	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}


	public String getLocalidad() {
		return localidad;
	}


	public void setDelegacion_O_Municipio(String delegacion_O_Municipio) {
		this.delegacion_O_Municipio = delegacion_O_Municipio;
	}


	public String getDelegacion_O_Municipio() {
		return delegacion_O_Municipio;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getEstado() {
		return estado;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public String getPais() {
		return pais;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setNumeroSirac(String numeroSirac) {
		this.numeroSirac = numeroSirac;
	}


	public String getNumeroSirac() {
		return numeroSirac;
	}


	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}


	public String getFechaOperacion() {
		return fechaOperacion;
	}


	public void setMontoTotalDescuento(String montoTotalDescuento) {
		this.montoTotalDescuento = montoTotalDescuento;
	}


	public String getMontoTotalDescuento() {
		return montoTotalDescuento;
	}


	public void setMontoTotalInteres(String montoTotalInteres) {
		this.montoTotalInteres = montoTotalInteres;
	}


	public String getMontoTotalInteres() {
		return montoTotalInteres;
	}


	public void setMontoTotalRecibir(String montoTotalRecibir) {
		this.montoTotalRecibir = montoTotalRecibir;
	}


	public String getMontoTotalRecibir() {
		return montoTotalRecibir;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


  public void setClaveEpo(String claveEpo)
  {
    this.claveEpo = claveEpo;
  }


  public String getClaveEpo()
  {
    return claveEpo;
  }


  public void setNombreEpo(String nombreEpo)
  {
    this.nombreEpo = nombreEpo;
  }


  public String getNombreEpo()
  {
    return nombreEpo;
  }


  public void setTotalDocumentos(String totalDocumentos)
  {
    this.totalDocumentos = totalDocumentos;
  }


  public String getTotalDocumentos()
  {
    return totalDocumentos;
  }


  public void setNumeroDocumentos(String numeroDocumentos)
  {
    this.numeroDocumentos = numeroDocumentos;
  }


  public String getNumeroDocumentos()
  {
    return numeroDocumentos;
  }


  public void setCorreoElectronico(String correoElectronico)
  {
    this.correoElectronico = correoElectronico;
  }


  public String getCorreoElectronico()
  {
    return correoElectronico;
  }
}