package com.nafin.descuento.ws;
  
/** 
 * La clase contiene la informaci�n arrojada por los procesos del 
 * Web Service de DescuentoIFWS
 * @author Gilberto Aparicio
 */
public class ProcesoIFWSInfoV2 implements java.io.Serializable  {
	private Integer codigoEjecucion;
	private String resumenEjecucion;
	private DocumentoIFWSV2[] documentos;
	private CuentasPymeIFWS[] cuentas;
	private LimitesEpo[] limitesEpo;
	private ProveedoresCFDI[] proveedores;
	private String acuse;
	private String recibo;
	
	public ProcesoIFWSInfoV2() {
	}

	/**
	 * Obtiene el c�digo de ejecuci�n resultante.
	 * @return Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public Integer getCodigoEjecucion() {
		return codigoEjecucion;
	}

	/**
	 * Establece el c�digo de ejecuci�n resultante.
	 * @param codigoEjecucion Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public void setCodigoEjecucion(Integer codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}

	/**
	 * Obtiene el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @return Cadena con el resumen o detalle de ejecuci�n
	 */
	public String getResumenEjecucion() {
		return resumenEjecucion;
	}

	/**
	 * Establece el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @param resumenEjecucion Cadena con el resumen de ejecuci�n
	 */
	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}

	/**
	 * Obtiene el conjunto de documentos resultantes de la ejecuci�n de un
	 * proceso del Web Service de DescuentoIFWS
	 * @return Arreglo con objetos DocumentoIFWS
	 */
	public DocumentoIFWSV2[] getDocumentos() {
		return documentos;
	}

	/**
	 * Estable el conjunto de documentos resultantes de la ejecuci�n de un
	 * proceso del Web Service de DescuentoIFWS
	 * @param documentos Conjunto de documentos.
	 */
	public void setDocumentos(DocumentoIFWSV2[] documentos) {
		this.documentos = documentos;
	}


	public void setCuentas(CuentasPymeIFWS[] cuentas) {
		this.cuentas = cuentas;
	}


	public CuentasPymeIFWS[] getCuentas() {
		return cuentas;
	}


	public void setLimitesEpo(LimitesEpo[] limitesEpo) {
		this.limitesEpo = limitesEpo;
	}


	public LimitesEpo[] getLimitesEpo() {
		return limitesEpo;
	}


	public void setProveedores(ProveedoresCFDI[] proveedores) {
		this.proveedores = proveedores;
	} 
 
 
	public ProveedoresCFDI[] getProveedores() {
		return proveedores;
	}
 
	public String getAcuse() {
		return acuse;
	}  
 
	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getRecibo() {
		return recibo;
	}
 
	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}
 
}