package com.nafin.descuento.ws;
  
/**
 * La clase representa a un documento seleccionado para la operaci�n de
 * Descuento Electr�nico.
 * @author Gilberto Aparicio
 */
public class DocumentoIFWSV2 extends DocumentoIFWS implements java.io.Serializable {


	private String nombrePF;
	private String apellidoPaternoPF;
	private String apellidoMaternoPF;
	private String cuentaCLABE;
	private String emailBeneficiario;
	private String tipoPersona;
	
	//GPG
	private String acuseConfirmacion;
	private String folio;
	private String fechaAltaSolicitud;
	
	private int tipoError = 0;
	private String causaRechazo;
	private String tipoProceso = "";
	
	// AGUSTIN BAUTISTA
	private String fechaOperacion;
	private String cuentaDeposito;
	private String porcComisionFondeo;
	private String tasaFondeoNafin;
	private String numeroPrestamo;
	private String claveEstatusSolic;
	private String claveBloqueo;

	public DocumentoIFWSV2() {
	}
	
	/**
	 * Obtiene el Acuse de Selecci�n del Documento por parte del Proveedor
	 * @return Cadena con el acuse 
	 */
	public String getAcusePyme() {
		return super.getAcusePyme();
	}

	/**
	 * Establece el Acuse de Selecci�n del Documento por parte del Proveedor
	 * @param acusePyme Cadena con el acuse 
	 */
	public void setAcusePyme(String acusePyme) {
		this.acusePyme = acusePyme;
	}


	/**
	 * Obtiene la clave del Banco de Fondeo del documento.
	 * 
	 * @return Cadena con la clave del Banco de Fondeo:
	 * 		1 Nafin
	 *    2 Bancomext
	 */
	public String getClaveBancoFondeo() {
		return claveBancoFondeo;
	}

	/**
	 * Establece la clave del Banco de Fondeo del documento.
	 * 
	 * @param claveBancoFondeo Clave del Banco de Fondeo:
	 * 		1 Nafin
	 *    2 Bancomext
	 */
	public void setClaveBancoFondeo(String claveBancoFondeo) {
		this.claveBancoFondeo = claveBancoFondeo;
	}

	/**
	 * Obtiene la clave del documento (ic_documento) en N@E
	 * @return Cadena con la clave del documento
	 */
	public String getClaveDocumento() {
		return claveDocumento;
	}

	/**
	 * Establece la clave del documento (ic_documento) en N@E
	 * @param claveDocumento Clave del documento
	 */
	public void setClaveDocumento(String claveDocumento) {
		this.claveDocumento = claveDocumento;
	}

	/**
	 * Clave de la EPO en N@E (ic_epo)
	 * @return Cadena con la clave de la epo
	 */
	public String getClaveEpo() {
		return claveEpo;
	}

	/**
	 * Establece la clave de la EPO de N@E (ic_epo)
	 * @return Cadena con la clave de la epo
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}



	/**
	 * Obtiene la clave de la moneda del documento
	 * MXP Para moneda Nacional y USD para D�lares
	 * @return Cadena con la clave de la moneda
	 */
	public String getClaveMoneda() {
		return claveMoneda;
	}

	/**
	 * Establece la clave de la moneda del documento 
	 * @param claveMoneda Clave de la moneda MXP Para moneda Nacional y USD para D�lares
	 */
	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}

	/**
	 * Obtiene la clave del estatus del documento
	 * @return Cadena con el estatus del documento
	 */
	public String getClaveEstatus() {
		return claveEstatus;
	}

	/**
	 * Establece la clave del estatus del documento
	 * @param claveEstatus Clave de estatus del documento
	 */
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	/**
	 * Obtiene la fecha de emisi�n del documento en formato dd/mm/yyyy
	 * @return Cadena con la fecha de emision
	 */
	public String getFechaEmision() {
		return fechaEmision;
	}

	/**
	 * Establece la fecha de emisi�n del documento.
	 * el formato esperado es dd/mm/yyyy
	 * @param fechaEmision Fecha de emision
	 */
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	/**
	 * Obtiene la fecha de selecci�n del documento por parte de la pyme 
	 * en formato dd/mm/yyyy
	 * @return Cadena con la fecha de seleccion
	 */
	public String getFechaSeleccionPyme() {
		return fechaSeleccionPyme;
	}

	/**
	 * Establece la fecha de selecci�n del documento por parte de la pyme.
	 * el formato esperado es dd/mm/yyyy
	 * @param fechaSeleccionPyme Fecha de selecci�n
	 */
	public void setFechaSeleccionPyme(String fechaSeleccionPyme) {
		this.fechaSeleccionPyme = fechaSeleccionPyme;
	}

	/**
	 * Obtiene la fecha de publicaci�n (alta) del documento
	 * en formato dd/mm/yyyy
	 * @return Cadena con la fecha de publicacion
	 */
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}

	/**
	 * Establece la fecha de publicaci�n del documento.
	 * El formato esperado es dd/mm/yyyy
	 * @param fechaPublicacion Fecha de publicaci�n
	 */
	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	/**
	 * Obtiene la fecha de vencimiento del documento
	 * en formato dd/mm/yyyy
	 * @return Cadena con la fecha de vencimiento
	 */
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	/**
	 * Establece la fecha de vencimiento del documento.
	 * El formato esperado es dd/mm/yyyy
	 * @param fechaVencimiento Fecha de vencimiento
	 */
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/**
	 * Obtiene el monto del descuento del documento
	 * @return Cadena con el monto del descuento
	 */
	public String getMontoDescuento() {
		return montoDescuento;
	}


	/**
	 * Establece el monto del descuento del documento
	 * @param montoDescuento Monto del descuento
	 */
	public void setMontoDescuento(String montoDescuento) {
		this.montoDescuento = montoDescuento;
	}

	public String getTasaAceptada() {
		return tasaAceptada;
	}

	public void setTasaAceptada(String tasaAceptada) {
		this.tasaAceptada = tasaAceptada;
	}

	/**
	 * Obtiene el importe de intereses del descuento del documento
	 * @return Cadena con el importe del interes
	 */
	public String getImporteInteres() {
		return importeInteres;
	}

	/**
	 * Establece el importe de intereses del descuento del documento
	 * @param importeInteres Importe del interes
	 */
	public void setImporteInteres(String importeInteres) {
		this.importeInteres = importeInteres;
	}

	/**
	 * Obtiene el importe a recibir del documento por parte de la Pyme
	 * @return Cadena con el importe a recibir
	 */
	public String getImporteRecibir() {
		return importeRecibir;
	}

	/**
	 * Establece el importe a recibir del documento por parte de la Pyme
	 * @param importeRecibir Importe a recibir
	 */
	public void setImporteRecibir(String importeRecibir) {
		this.importeRecibir = importeRecibir;
	}

	/**
	 * Obtiene el nombre de la Epo que public� el documento
	 * @return Cadena con el nombre de la EPO
	 */
	public String getNombreEpo() {
		return nombreEpo;
	}

	/**
	 * Establece el nombre de la Epo que public� el documento
	 * @param nombreEpo Nombre de la EPO
	 */
	public void setNombreEpo(String nombreEpo) {
		this.nombreEpo = nombreEpo;
	}



	/**
	 * Obtiene el nombre de la moneda del documento
	 * @return Cadena con el nombre de la moneda
	 */
	public String getNombreMoneda() {
		return nombreMoneda;
	}

	/**
	 * Establece el nombre de la moneda del documento
	 * @param nombreMoneda Nombre de la moneda
	 */
	public void setNombreMoneda(String nombreMoneda) {
		this.nombreMoneda = nombreMoneda;
	}

	/**
	 * Obtiene el nombre del Proveedor al que se le public� el documento
	 * @return Cadena con el nombre del proveedor
	 */
	public String getNombrePyme() {
		return nombrePyme;
	}

	/**
	 * Establece el nombre del Proveedor al que se le public� el documento
	 * @param nombrePyme Nombre del proveedor
	 */
	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}

	/**
	 * Obtiene el n�mero/clave del documento asignado por la EPO
	 * @return Cadena col el numero de documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Establece el n�mero/clave del documento asignado por la EPO
	 * @param numeroDocumento numero de Documento
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Obtiene el n�mero/clave asignado por la EPO al proveedor.
	 * @return Cadena con el numero de documento
	 */
	public String getNumeroProveedor() {
		return numeroProveedor;
	}

	/**
	 * Establece el n�mero/clave asignado por la EPO al proveedor.
	 * @param numeroProveedor N�mero de proveedort
	 */
	public void setNumeroProveedor(String numeroProveedor) {
		this.numeroProveedor = numeroProveedor;
	}

	/**
	 * Obtiene el n�mero del proveedor en SIRAC.
	 * @return Cadena con el numero SIRAC
	 */
	public String getNumeroSirac() {
		return numeroSirac;
	}

	/**
	 * Establece el n�mero del proveedor en SIRAC.
	 * @param numeroSirac Numero SIRAC
	 */
	public void setNumeroSirac(String numeroSirac) {
		this.numeroSirac = numeroSirac;
	}

	
	/**
	 * Obtiene la clave del tipo de factoraje
	 * @return Cadena con la clave del tipo de factoraje
	 */
	public String getTipoFactoraje() {
		return tipoFactoraje;
	}

	/**
	 * Establece la clave del tipo de factoraje
	 * @param tipoFactoraje
	 */
	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}

	/**
	 * Obtiene la clave del rechazo de la solicitud de descuento, establecida por
	 * el Intermediario Financiero.
	 * @return Cadena con la clave del rechazo.
	 */
	public String getClaveRechazo() {
		return claveRechazo;
	}

	/**
	 * Establece la clave del rechazo de la solicitud de descuento por parte del
	 * Intermediario Financiero.
	 * @param claveRechazo Clave del rechazo, segun el catalogo de errores pactado
	 */
	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}

	/**
	 * Obtiene el detalle del error generado en el momento de 
	 * procesar este objeto.
	 * (Por ejemplo errores de validacion de los valores de los atributos)
	 * @return Cadena con los detalles del error
	 */
	public String getErrorDetalle() {
		return errorDetalle;
	}

	/**
	 * Establece el detalle del error generado en el momento de 
	 * procesar este objeto.
	 * (Por ejemplo errores de validacion de los valores de los atributos)
	 * @param errorDetalle Cadena con los detalles del error
	 */
	public void setErrorDetalle(String errorDetalle) {
		this.errorDetalle = errorDetalle;
	}


	/**
	 * Obtiene el valor del Campo Adicional 1.
	 * Los campos adicionales son parametrizados por la EPO.
	 * @return Cadena con el valor del campo adicional 1
	 */
	public String getCampoAdicional1() {
		return campoAdicional1;
	}


	/**
	 * Establece el valor del Campo Adicional 1
	 * Los campos adicionales son parametrizados por la EPO.
	 * @param campoAdicional1 valor del campo adicional 1
	 */
	public void setCampoAdicional1(String campoAdicional1) {
		this.campoAdicional1 = campoAdicional1;
	}

	/**
	 * Obtiene el valor del Campo Adicional 2.
	 * Los campos adicionales son parametrizados por la EPO.
	 * @return Cadena con el valor del campo adicional 2
	 */
	public String getCampoAdicional2() {
		return campoAdicional2;
	}

	/**
	 * Establece el valor del Campo Adicional 2
	 * Los campos adicionales son parametrizados por la EPO.
	 * @param campoAdicional1 valor del campo adicional 2
	 */
	public void setCampoAdicional2(String campoAdicional2) {
		this.campoAdicional2 = campoAdicional2;
	}

	/**
	 * Obtiene el valor del Campo Adicional 3.
	 * Los campos adicionales son parametrizados por la EPO.
	 * @return Cadena con el valor del campo adicional 3
	 */
	public String getCampoAdicional3() {
		return campoAdicional3;
	}

	/**
	 * Establece el valor del Campo Adicional 3
	 * Los campos adicionales son parametrizados por la EPO.
	 * @param campoAdicional1 valor del campo adicional 3
	 */
	public void setCampoAdicional3(String campoAdicional3) {
		this.campoAdicional3 = campoAdicional3;
	}

	/**
	 * Obtiene el valor del Campo Adicional 4.
	 * Los campos adicionales son parametrizados por la EPO.
	 * @return Cadena con el valor del campo adicional 4
	 */
	public String getCampoAdicional4() {
		return campoAdicional4;
	}

	/**
	 * Establece el valor del Campo Adicional 4
	 * Los campos adicionales son parametrizados por la EPO.
	 * @param campoAdicional1 valor del campo adicional 4
	 */
	public void setCampoAdicional4(String campoAdicional4) {
		this.campoAdicional4 = campoAdicional4;
	}

	/**
	 * Obtiene el valor del Campo Adicional 5.
	 * Los campos adicionales son parametrizados por la EPO.
	 * @return Cadena con el valor del campo adicional 5
	 */
	public String getCampoAdicional5() {
		return campoAdicional5;
	}

	/**
	 * Establece el valor del Campo Adicional 5
	 * Los campos adicionales son parametrizados por la EPO.
	 * @param campoAdicional1 valor del campo adicional 5
	 */
	public void setCampoAdicional5(String campoAdicional5) {
		this.campoAdicional5 = campoAdicional5;
	}	
	
	/**
	 * Genera una cadena con los valores de los atributos del documento
	 * @return Cadena que representa el objeto
	 */
	public String toString() {
		return
				"acuseIF" + "=" + acuseIF + "|"+
				"acusePyme" + "=" + acusePyme + "|" +
				"claveBancoFondeo" + "=" + claveBancoFondeo + "|" +
				"claveDocumento" + "=" + claveDocumento + "|" +
				"claveEpo" + "=" + claveEpo + "|" +
				"claveEstatus" + "=" + claveEstatus + "|" +
				"claveIF" + "=" + claveIF + "|" +
				"claveMoneda" + "=" + claveMoneda + "|" +
				"clavePYME" + "=" + clavePYME + "|" +
				"fechaEmision" + "=" + fechaEmision + "|" +
				"fechaNotificacion" + "=" + fechaNotificacion + "|"+
				"fechaPublicacion" + "=" + fechaPublicacion + "|" +
				"fechaSeleccionPyme" + "=" + fechaSeleccionPyme + "|" +
				"fechaVencimiento" + "=" + fechaVencimiento + "|" +
				"montoDescuento" + "=" + montoDescuento + "|" +
				"montoDocumento" + "=" + montoDocumento + "|" +
				"porcentajeAnticipo" + "=" + porcentajeAnticipo + "|"+
				"importeInteres" + "=" + importeInteres + "|" +
				"importeRecibir" + "=" + importeRecibir + "|" +
				"nombreEpo" + "=" + nombreEpo + "|" +
				"nombreEstatus" + "=" + nombreEstatus + "|" +
				"nombreIF" + "=" + nombreIF + "|" +
				"nombreMoneda" + "=" + nombreMoneda + "|" +
				"nombrePyme" + "=" + nombrePyme + "|" +
				"nombreTipoFactoraje" + "=" + nombreTipoFactoraje + "|"+
				"numeroDocumento" + "=" + numeroDocumento + "|" +
				"numeroProveedor" + "=" + numeroProveedor + "|" +
				"numeroSirac" + "=" + numeroSirac + "|" +
				"plazo" + "=" + plazo + "|" +
				"porcentajeDescuento" + "=" + porcentajeDescuento + "|" +
				"referencia" + "=" + referencia + "|" +
				"recursoGarantia" + "=" + recursoGarantia + "|" +
				"tasaAceptada" + "=" + tasaAceptada + "|" +
				"tipoFactoraje" + "=" + tipoFactoraje + "|" +
				"nombrePF" + "=" + nombrePF + "|" +
				"apellidoPaternoPF" + "=" + apellidoPaternoPF + "|" +
				"apellidoMaternoPF" + "=" + apellidoMaternoPF + "|" +
				"cuentaCLABE" + "=" + cuentaCLABE + "|" +
				"emailBeneficiario" + "=" + emailBeneficiario + "|" +
				"tipoPersona" + "=" + tipoPersona + "|" +
				"campoAdicional1" + "=" + campoAdicional1 + "|" +
				"campoAdicional2" + "=" + campoAdicional2 + "|" +
				"campoAdicional3" + "=" + campoAdicional3 + "|" +
				"campoAdicional4" + "=" + campoAdicional4 + "|" +
				"campoAdicional5" + "=" + campoAdicional5 + "|" +
				"claveRechazo" + "=" + claveRechazo + "|" +
				"errorDetalle" + "=" + errorDetalle ;
	}

	/**
	 * Obtiene el monto del documento
	 * @return Cadena con el monto del documento
	 */
	public String getMontoDocumento() {
		return montoDocumento;
	}

	/**
	 * Establece el monto del documento
	 * @param montoDocumento Monto del Documento
	 */
	public void setMontoDocumento(String montoDocumento) {
		this.montoDocumento = montoDocumento;
	}

	/**
	 * Obtiene el porcentaje del descuento
	 * @return Cadena con el porcentaje del descuento
	 */
	public String getPorcentajeDescuento() {
		return porcentajeDescuento;
	}

	/**
	 * Establece el porcentaje del descuento
	 * @param porcentajeDescuento Porcentaje del descuento
	 */
	public void setPorcentajeDescuento(String porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}

	/**
	 * Obtiene el monto de recurso en Garantia
	 * @return Cadena con el monto del recurso en garantia
	 */
	public String getRecursoGarantia() {
		return recursoGarantia;
	}

	/**
	 * Establece el monto del recurso en Garantia
	 * @param recursoGarantia Recurso en garantia
	 */
	public void setRecursoGarantia(String recursoGarantia) {
		this.recursoGarantia = recursoGarantia;
	}

	/**
	 * Obtiene el plazo en dias del documento
	 * @return Cadena con el plazo del documento
	 */
	public String getPlazo() {
		return plazo;
	}

	/**
	 * Establece el plazo en dias del documento
	 * @param plazo Plazo del documento
	 */
	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	/**
	 * Obtiene el nombre del estatus del documento
	 * @return Cadena con el nombre del estatus
	 */
	public String getNombreEstatus() {
		return nombreEstatus;
	}

	/**
	 * Establece el nombre del estatus del documento
	 * @param nombreEstatus Nombre del estatus
	 */
	public void setNombreEstatus(String nombreEstatus) {
		this.nombreEstatus = nombreEstatus;
	}

	/**
	 * Obtiene la referencia del documento
	 * @return Cadena con la referencia del documento
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * Establece la referencia del documento
	 * @param referencia Referencia del documento
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * Obtiene la clave del IF
	 * @return Cadena con la clave del IF
	 */
	public String getClaveIF() {
		return claveIF;
	}

	/**
	 * Establece la clave del IF
	 * @param claveIF Clave del IF
	 */
	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	/**
	 * Obtiene el nombre del IF
	 * @return Cadena con el nombre del IF
	 */
	public String getNombreIF() {
		return nombreIF;
	}

	/**
	 * Establece el nombre del IF
	 * @param nombreIF Nombre del IF
	 */
	public void setNombreIF(String nombreIF) {
		this.nombreIF = nombreIF;
	}


	/**
	 * Establece la clave (ic_pyme) de la Pyme
	 * @param clavePYME Clave de la Pyme
	 */
	public void setClavePYME(String clavePYME) {
		this.clavePYME = clavePYME;
	}

	/**
	 * Obtiene la clave (ic_pyme) de la Pyme
	 * @return Cadena con la clave de la Pyme
	 */
	public String getClavePYME() {
		return clavePYME;
	}


	/**
	 * Establece la fecha de notificacion.
	 * @param fechaNotificacion Fecha de notificacion en formato dd/mm/yyyy
	 */
	public void setFechaNotificacion(String fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}

	/**
	 * Obtiene la fecha de notificacion.
	 * @return Cadena con la fecha de notificacion en formato dd/mm/yyyy
	 */
	public String getFechaNotificacion() {
		return fechaNotificacion;
	}


	/**
	 * Establece el porcentaje de anticipo
	 * @param porcentajeAnticipo Porcentaje del Anticipo
	 */
	public void setPorcentajeAnticipo(String porcentajeAnticipo) {
		this.porcentajeAnticipo = porcentajeAnticipo;
	}

	/**
	 * Obtiene el porcentaje de anticipo
	 * @return Cadena con el porcentaje del Anticipo
	 */
	public String getPorcentajeAnticipo() {
		return this.porcentajeAnticipo;
	}


	/**
	 * Establece el nombre del tipo de factoraje
	 * @param nombreTipoFactoraje nombre del tipo de factoraje
	 */
	public void setNombreTipoFactoraje(String nombreTipoFactoraje) {
		this.nombreTipoFactoraje = nombreTipoFactoraje;
	}

	/**
	 * Obtiene el nombre del tipo de factoraje
	 * @return Cadena con el nombre del tipo de factoraje
	 */
	public String getNombreTipoFactoraje() {
		return nombreTipoFactoraje;
	}


	/**
	 * Establece el acuse del IF.
	 * @param acuseIF Acuse IF
	 */
	public void setAcuseIF(String acuseIF) {
		this.acuseIF = acuseIF;
	}


	/**
	 * Obtiene el acuse del IF
	 * @return Cadena con el acuse IF
	 */
	public String getAcuseIF() {
		return acuseIF;
	}

	/**
	 * Obtiene el Nombre en caso de ser persona Fisica
	 * @return Cadena con el nombre
	 */
	public String getNombrePF() {
		return nombrePF;
	}

	/**
	 * Establece el nombre de la persona, en caso de ser persona fisica
	 * @param nombrePF Nombre de la persona Fisica
	 */
	public void setNombrePF(String nombrePF) {
		this.nombrePF = nombrePF;
	}

	/**
	 * Obtiene el Apellido Paterno en caso de ser persona Fisica
	 * @return Cadena con el apellido Paterno
	 */
	public String getApellidoPaternoPF() {
		return apellidoPaternoPF;
	}

	/**
	 * Establece el apellido paterno de la persona, en caso de ser persona fisica
	 * @param apellidoPaternoPF Apellido paterno de la persona Fisica
	 */
	public void setApellidoPaternoPF(String apellidoPaternoPF) {
		this.apellidoPaternoPF = apellidoPaternoPF;
	}

	/**
	 * Obtiene el Apellido Materno en caso de ser persona Fisica
	 * @return Cadena con el apellido Materno
	 */
	public String getApellidoMaternoPF() {
		return apellidoMaternoPF;
	}

	/**
	 * Establece el apellido materno de la persona, en caso de ser persona fisica
	 * @param apellidoMaternoPF Apellido materno de la persona Fisica
	 */
	public void setApellidoMaternoPF(String apellidoMaternoPF) {
		this.apellidoMaternoPF = apellidoMaternoPF;
	}

	/**
	 * Obtiene La cuenta CLABE 
	 * @return Cadena con la cuenta CLABE
	 */
	public String getCuentaCLABE() {
		return cuentaCLABE;
	}

	/**
	 * Establece la cuenta CLABE bancaria
	 * @param cuentaCLABE Cuenta CLABE bancaria
	 */
	public void setCuentaCLABE(String cuentaCLABE) {
		this.cuentaCLABE = cuentaCLABE;
	}

	/**
	 * Obtiene el email del beneficiario
	 * @return Cadena con la cuenta CLABE
	 */
	public String getEmailBeneficiario() {
		return emailBeneficiario;
	}

	/**
	 * Establece el email del Beneficiario
	 * @param emailBeneficiario Email del beneficiario
	 */
	public void setEmailBeneficiario(String emailBeneficiario) {
		this.emailBeneficiario = emailBeneficiario;
	}

	/**
	 * Establece el tipo de persona
	 * @param tipoPersona F Persona Fisica, M Persona Moral
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * Obtiene el tipo de persona
	 * @return Cadena con el tipo de persona: F Persona Fisica, M Persona Moral
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}


	public void setAcuseConfirmacion(String acuseConfirmacion) {
		this.acuseConfirmacion = acuseConfirmacion;
	}


	public String getAcuseConfirmacion() {
		return acuseConfirmacion;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


	public void setFechaAltaSolicitud(String fechaAltaSolicitud) {
		this.fechaAltaSolicitud = fechaAltaSolicitud;
	}


	public String getFechaAltaSolicitud() {
		return fechaAltaSolicitud;
	}

	/**
	 * En caso de que exista un error en el procesamiento del Documento
	 * se regresa un c�digo que indica si el error es normal debido a las
	 * reglas de la l�gica de negocio � si es un error de sistema (BD, memoria, etc)
	 * @param tipoError Clave del tipo de error
	 * 	0.- Sin error
	 *    1.- Error normal (por validaci�n de negocio)
	 *    2.- Error inesperado (BD, memoria, etc.)
	 *    
	 */
	public void setTipoError(int tipoError) {
		this.tipoError = tipoError;
	}


	public int getTipoError() {
		return tipoError;
	}

	public String getCausaRechazo() {
		return causaRechazo;
	} 
	/**
	 * Establece la causa de rechazo 
	 * @param causaRechazo,  se utiliza en el la pantalla IF/Descuento / Capturas / Confirmaci�n Doctos
	 */	
	public void setCausaRechazo(String causaRechazo) {
		this.causaRechazo = causaRechazo;
	}

	/**
	 * Establece la tipoProceso
	 * @param tipoProceso,  es para identificar  el cambio se va hacer desde la pantalla
	 * IF/Descuento / Capturas / Confirmaci�n Doctos 
	 * */	
	 
	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getPorcComisionFondeo() {
		return porcComisionFondeo;
	}

	public void setPorcComisionFondeo(String porcComisionFondeo) {
		this.porcComisionFondeo = porcComisionFondeo;
	}

	public String getTasaFondeoNafin() {
		return tasaFondeoNafin;
	}

	public void setTasaFondeoNafin(String tasaFondeoNafin) {
		this.tasaFondeoNafin = tasaFondeoNafin;
	}

	public String getNumeroPrestamo() {
		return numeroPrestamo;
	}

	public void setNumeroPrestamo(String numeroPrestamo) {
		this.numeroPrestamo = numeroPrestamo;
	}

	public String getClaveEstatusSolic() {
		return claveEstatusSolic;
	}

	public void setClaveEstatusSolic(String claveEstatusSolic) {
		this.claveEstatusSolic = claveEstatusSolic;
	}

	public String getClaveBloqueo() {
		return claveBloqueo;
	}

	public void setClaveBloqueo(String claveBloqueo) {
		this.claveBloqueo = claveBloqueo;
	}

	public String getCuentaDeposito() {
		return cuentaDeposito;
	}

	public void setCuentaDeposito(String cuentaDeposito) {
		this.cuentaDeposito = cuentaDeposito;
	}

}