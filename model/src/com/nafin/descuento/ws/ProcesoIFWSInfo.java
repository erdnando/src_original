package com.nafin.descuento.ws;

/**
 * La clase contiene la informaci�n arrojada por los procesos del 
 * Web Service de DescuentoIFWS
 * @author Gilberto Aparicio
 */
public class ProcesoIFWSInfo implements java.io.Serializable  {
	private Integer codigoEjecucion;
	private String resumenEjecucion;
	private DocumentoIFWS[] documentos;
	
	public ProcesoIFWSInfo() {
	}

	/**
	 * Obtiene el c�digo de ejecuci�n resultante.
	 * @return Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public Integer getCodigoEjecucion() {
		return codigoEjecucion;
	}

	/**
	 * Establece el c�digo de ejecuci�n resultante.
	 * @param codigoEjecucion Codigo de Ejecuci�n:
	 * 		1 = Sin Errores
	 * 		2 = Con Errores en el proceso
	 */
	public void setCodigoEjecucion(Integer codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}

	/**
	 * Obtiene el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @return Cadena con el resumen o detalle de ejecuci�n
	 */
	public String getResumenEjecucion() {
		return resumenEjecucion;
	}

	/**
	 * Establece el resumen (si codigoEjecucion=1) �
	 * detalle de errores (si codigoEjecucion=2) de la ejecuci�n de un proceso
	 * del Web Service de DescuentoIFWS
	 * @param resumenEjecucion Cadena con el resumen de ejecuci�n
	 */
	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}

	/**
	 * Obtiene el conjunto de documentos resultantes de la ejecuci�n de un
	 * proceso del Web Service de DescuentoIFWS
	 * @return Arreglo con objetos DocumentoIFWS
	 */
	public DocumentoIFWS[] getDocumentos() {
		return documentos;
	}

	/**
	 * Estable el conjunto de documentos resultantes de la ejecuci�n de un
	 * proceso del Web Service de DescuentoIFWS
	 * @param documentos Conjunto de documentos.
	 */
	public void setDocumentos(DocumentoIFWS[] documentos) {
		this.documentos = documentos;
	}

}