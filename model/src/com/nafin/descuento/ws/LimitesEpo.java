package com.nafin.descuento.ws;

public class LimitesEpo implements java.io.Serializable  {
	
	private String claveEPO;
	private String nombreEPO;
	private String claveIF;
	private String limiteEPO;
	private String limiteUtilizado;
	private String activaLimite;
	private String montoComprometido;
	private String moneda;
	private String fechaLineaCredito;
	private String fechaCambioAdmin;
	private String limiteEPO_dl;
	private String limiteUtilizado_dl;
	private String montoComprometido_dl;
	private String moneda_dl;
	
	public LimitesEpo() {
	}


	public void setNombreEPO(String nombreEPO) {
		this.nombreEPO = nombreEPO;
	}

	public String getNombreEPO() {
		return nombreEPO;
	}

	public void setLimiteEPO(String limiteEPO) {
		this.limiteEPO = limiteEPO;
	}


	public String getLimiteEPO() {
		return limiteEPO;
	}


	public void setLimiteUtilizado(String limiteUtilizado) {
		this.limiteUtilizado = limiteUtilizado;
	}


	public String getLimiteUtilizado() {
		return limiteUtilizado;
	}


	public void setActivaLimite(String activaLimite) {
		this.activaLimite = activaLimite;
	}


	public String getActivaLimite() {
		return activaLimite;
	}


	public void setMontoComprometido(String montoComprometido) {
		this.montoComprometido = montoComprometido;
	}


	public String getMontoComprometido() {
		return montoComprometido;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}


	public String getClaveEPO() {
		return claveEPO;
	}


	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}


	public String getClaveIF() {
		return claveIF;
	}


	public void setFechaLineaCredito(String fechaLineaCredito) {
		this.fechaLineaCredito = fechaLineaCredito;
	}


	public String getFechaLineaCredito() {
		return fechaLineaCredito;
	}


	public void setFechaCambioAdmin(String fechaCambioAdmin) {
		this.fechaCambioAdmin = fechaCambioAdmin;
	}


	public String getFechaCambioAdmin() {
		return fechaCambioAdmin;
	}

	public String getLimiteEPO_dl() {
		return limiteEPO_dl;
	}

	public void setLimiteEPO_dl(String limiteEPO_dl) {
		this.limiteEPO_dl = limiteEPO_dl;
	}

	public String getLimiteUtilizado_dl() {
		return limiteUtilizado_dl;
	}

	public void setLimiteUtilizado_dl(String limiteUtilizado_dl) {
		this.limiteUtilizado_dl = limiteUtilizado_dl;
	}

	public String getMontoComprometido_dl() {
		return montoComprometido_dl;
	}

	public void setMontoComprometido_dl(String montoComprometido_dl) {
		this.montoComprometido_dl = montoComprometido_dl;
	}

	public String getMoneda_dl() {
		return moneda_dl;
	}

	public void setMoneda_dl(String moneda_dl) {
		this.moneda_dl = moneda_dl;
	}
}