package com.nafin.descuento.ws;

/**
 * La clase representa a una Cuenta Bancaria seleccionada para la operaci�n de
 * obtener las cuentas bancarias de los proveedores 
 * pendientes de autorizaci�n por parte del Intermediario Financiero
 * @author Gerardo P�rez
 */

public class CuentasPymeIFWS implements java.io.Serializable {
	private String  numeroSiracPyme;
	private String  rfcPyme;
	private String  nombrePyme;
	private String  claveMoneda;
	private String  descripcionMoneda;
	private String  claveEpo;
	private String  descripcionEpo;
	private String  bancoServicio;
	private String  numeroCuenta;
	private String  numeroCuentaClabe;
	private String  sucursalCuenta;
	private String  plazaCuenta;
	private String  convenioUnico;
	private String  expedienteDigital;
	private String  fechaFirmaAutografaCU;
	private String  fechaParamAutoCU;
	private String  numeroNafinElectPyme;
	private String  prioridad;
	private String  cambioCuenta;
	private String  descargaWS;
	private String  claveExpediente;
	private int tipoError;
	  
	
	private String  errorDetalle;
	private String ic_pyme;
	private String nombre_IF;

	
	public CuentasPymeIFWS() {
	}

	public void setNumeroSiracPyme(String numeroSiracPyme) {
		this.numeroSiracPyme = numeroSiracPyme;
	}

	public String getNumeroSiracPyme() {
		return numeroSiracPyme;
	}

	public void setRfcPyme(String rfcPyme) {
		this.rfcPyme = rfcPyme;
	}

	public String getRfcPyme() {
		return rfcPyme;
	}

	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}

	public String getNombrePyme() {
		return nombrePyme;
	}


	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}


	public String getClaveMoneda() {
		return claveMoneda;
	}


	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}


	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setDescripcionEpo(String descripcionEpo) {
		this.descripcionEpo = descripcionEpo;
	}


	public String getDescripcionEpo() {
		return descripcionEpo;
	}


	public void setBancoServicio(String bancoServicio) {
		this.bancoServicio = bancoServicio;
	}


	public String getBancoServicio() {
		return bancoServicio;
	}


	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}


	public String getNumeroCuenta() {
		return numeroCuenta;
	}


	public void setNumeroCuentaClabe(String numeroCuentaClabe) {
		this.numeroCuentaClabe = numeroCuentaClabe;
	}


	public String getNumeroCuentaClabe() {
		return numeroCuentaClabe;
	}


	public void setSucursalCuenta(String sucursalCuenta) {
		this.sucursalCuenta = sucursalCuenta;
	}


	public String getSucursalCuenta() {
		return sucursalCuenta;
	}


	public void setPlazaCuenta(String plazaCuenta) {
		this.plazaCuenta = plazaCuenta;
	}


	public String getPlazaCuenta() {
		return plazaCuenta;
	}


	public void setConvenioUnico(String convenioUnico) {
		this.convenioUnico = convenioUnico;
	}


	public String getConvenioUnico() {
		return convenioUnico;
	}


	public void setExpedienteDigital(String expedienteDigital) {
		this.expedienteDigital = expedienteDigital;
	}


	public String getExpedienteDigital() {
		return expedienteDigital;
	}


	public void setFechaFirmaAutografaCU(String fechaFirmaAutografaCU) {
		this.fechaFirmaAutografaCU = fechaFirmaAutografaCU;
	}


	public String getFechaFirmaAutografaCU() {
		return fechaFirmaAutografaCU;
	}


	public void setFechaParamAutoCU(String fechaParamAutoCU) {
		this.fechaParamAutoCU = fechaParamAutoCU;
	}


	public String getFechaParamAutoCU() {
		return fechaParamAutoCU;
	}


	public void setNumeroNafinElectPyme(String numeroNafinElectPyme) {
		this.numeroNafinElectPyme = numeroNafinElectPyme;
	}


	public String getNumeroNafinElectPyme() {
		return numeroNafinElectPyme;
	}


	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}


	public String getPrioridad() {
		return prioridad;
	}


	public void setCambioCuenta(String cambioCuenta) {
		this.cambioCuenta = cambioCuenta;
	}


	public String getCambioCuenta() {
		return cambioCuenta;
	}


	public void setErrorDetalle(String errorDetalle) {
		this.errorDetalle = errorDetalle;
	}


	public String getErrorDetalle() {
		return errorDetalle;
	}


	public void setDescargaWS(String descargaWS) {
		this.descargaWS = descargaWS;
	}


	public String getDescargaWS() {
		return descargaWS;
	}
	
	
	/**
	 * Genera una cadena con los valores de los atributos del documento
	 * @return Cadena que representa el objeto
	 */
	public String toString() {
		return
				"numeroSiracPyme" + "=" + numeroSiracPyme + "|"+
				"rfcPyme" + "=" + rfcPyme + "|" +
				"nombrePyme" + "=" + nombrePyme + "|" +
				"claveMoneda" + "=" + claveMoneda + "|" +
				"descripcionMoneda" + "=" + descripcionMoneda + "|" +
				"claveEpo" + "=" + claveEpo + "|" +
				"descripcionEpo" + "=" + descripcionEpo + "|" +
				"bancoServicio" + "=" + bancoServicio + "|" +
				"numeroCuenta" + "=" + numeroCuenta + "|" +
				"numeroCuentaClabe" + "=" + numeroCuentaClabe + "|"+
				"sucursalCuenta" + "=" + sucursalCuenta + "|" +
				"plazaCuenta" + "=" + plazaCuenta + "|" +
				"convenioUnico" + "=" + convenioUnico + "|" +
				"expedienteDigital" + "=" + expedienteDigital + "|" +
				"fechaFirmaAutografaCU" + "=" + fechaFirmaAutografaCU + "|" +
				"fechaParamAutoCU" + "=" + fechaParamAutoCU + "|"+
				"numeroNafinElectPyme" + "=" + numeroNafinElectPyme + "|" +
				"prioridad" + "=" + prioridad + "|" +
				"cambioCuenta" + "=" + cambioCuenta + "|" +
				"descargaWS" + "=" + descargaWS + "|" +
				"errorDetalle" + "=" + errorDetalle ;
	}


	public void setClaveExpediente(String claveExpediente) {
		this.claveExpediente = claveExpediente;
	}


	public String getClaveExpediente() {
		return claveExpediente;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getNombre_IF() {
		return nombre_IF;
	}

	public void setNombre_IF(String nombre_IF) {
		this.nombre_IF = nombre_IF;
	}

	/**
	 * En caso de que exista un error en el procesamiento del Documento
	 * se regresa un c�digo que indica si el error es normal debido a las
	 * reglas de la l�gica de negocio � si es un error de sistema (BD, memoria, etc)
	 * @param tipoError Clave del tipo de error
	 * 	0.- Sin error
	 *    1.- Error normal (por validaci�n de negocio)
	 *    2.- Error inesperado (BD, memoria, etc.)
	 *    
	 */
	public void setTipoError(int tipoError) {
		this.tipoError = tipoError;
	}


	public int getTipoError() {
		return tipoError;
	}
}