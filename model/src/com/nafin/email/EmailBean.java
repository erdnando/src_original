package com.nafin.email;

import java.util.ArrayList;

import javax.annotation.Resource;

import javax.ejb.Local;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import netropology.utilerias.Correo;

@Stateless(name = "Email", mappedName = "EmailEJB")
@Local
public class EmailBean implements Email {
    @Resource
    SessionContext sessionContext;

    public EmailBean() {
    }

    public void enviaCorreoConDatosAdjuntosBCC(String remitente, String destinatarios, String copia_destinatarios,
                                               String tituloMensaje, String contenidoMensaje,
                                               ArrayList listaDeImagenesAdjuntas, ArrayList listaDeArchivosAdjuntos,
                                               String BCC, boolean bdOracle) throws Exception {
        Correo correo = new Correo();
        if (bdOracle) {
            correo.enableUseAccesoDBOracle();
        }
        correo.disableDebug();
        correo.enviaCorreoConDatosAdjuntosBCC(remitente, destinatarios, copia_destinatarios, tituloMensaje,
                                              contenidoMensaje, listaDeImagenesAdjuntas, listaDeArchivosAdjuntos, BCC);
    }
}

