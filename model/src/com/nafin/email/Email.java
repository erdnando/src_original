package com.nafin.email;

import java.util.ArrayList;

import javax.ejb.Remote;

@Remote
public interface Email {
    void enviaCorreoConDatosAdjuntosBCC(String remitente, String destinatarios, String copia_destinatarios,
                                        String tituloMensaje, String contenidoMensaje,
                                        ArrayList listaDeImagenesAdjuntas, ArrayList listaDeArchivosAdjuntos,
                                        String BCC, boolean bdOracle) throws Exception;
}
