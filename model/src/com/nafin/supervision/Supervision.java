package com.nafin.supervision;

import com.netro.exception.NafinException;

import java.io.File;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AccesoDB;

@Remote
public interface Supervision {
    
	/**
	 *
	 * @param cveSiag
	 * @param anio
	 * @param mes
	 * @param tipoCal
	 * @param tipoInfo
	 * @return
	 */
	public boolean existeDoctosResultDictamenSup(Long cveSiag, Integer anio, Integer mes,
			String tipoCal, String tipoInfo);

	/**
	 * 
	 * @param cveSiag
	 * @param anio
	 * @param mes
	 * @param tipoCal
	 * @param tipoInfo
	 * @param usuario
	 * @param nombreArch
	 * @param path
	 * @return
	 */
	public Map<String, String> cargaDoctosResultDictamenSup(Long cveSiag, Integer anio, Integer mes, String tipoCal, 
			String tipoInfo, String usuario, String nombreArch, String path);
	
	/**
	 * Verfica que el web service de ONBASE este disponible
	 * @return
	 */
	public boolean isWebServiceDisponible();
		
    public HashMap getParametrosDeSupervision(String claveIF)
            throws NafinException;
    
    public void setParametrosDeSupervision(String claveIF,String contactos, String ejecutivos, String cveSIAG,String contactoPrincipal, String ejecutivoPrincipal)
            throws NafinException;
    
    public boolean existeRegistroDeSupervision(String claveIF)
            throws NafinException;

    public String validaCalendario(String IC_IF, String CAL_FEC_LIM_ENT_E, String CAL_FEC_LIM_ENT_R, String CAL_FEC_LIM_ENT_A, String CAL_FEC_LIM_ENT_D, String CAL_NO_AUTORIZA, String CAL_REL_GARANTIAS, String MES, String ANIO, String TIPO_CAL) throws NafinException;
   
    public boolean insertaCalendario(String IC_IF, String CAL_FEC_LIM_ENT_E, String CAL_FEC_LIM_ENT_R, String CAL_FEC_LIM_ENT_A, String CAL_FEC_LIM_ENT_D, String CAL_NO_AUTORIZA, String CAL_REL_GARANTIAS, String MES, String ANIO, String TIPO_CAL)
            throws NafinException;
    
    public Vector getCalInsertado(String IC_IF, String CAL_MES, String CAL_ANIO, String CAL_TIPO)
      throws NafinException;
    
    public boolean updateCalendario(String MES, String ANIO, String TIPO_CAL,String acuse,String usuario,String noArchivos)
            throws NafinException;
    
    public HashMap<String,Object> insertaArchivosTemps(ArchivosCargaValue archivosValue, File f) throws NafinException;
    public void deleteArchivosTemps(ArchivosCargaValue archivosValue)throws NafinException;
    
    public Vector getDoctoPreInsertados(String mes, String anio, String tipoCarga, String usuario)
      throws NafinException;
    
    public boolean confirmaCarga(String mes, String anio, String tipoCarga, String noArchivos, String usuario, String relacionGar, String acuse, String path, String interFinan)
    throws NafinException;
    
    public List getCatalogoAnioI();
    
    public Vector getConsultaArchivosR(String interFin, String CAL_MES, String CAL_ANIO)
      throws NafinException;
    
    public Vector getObtenArchivosR(String interFin, String CAL_MES, String CAL_ANIO, String acuse,String garantia)
      throws NafinException;
    
    public ArchivosCargaValue  getArchivo(String nombreArchivo, String acuse) throws NafinException;
    
    public List obtenArchivosUsuario(ArchivosCargaValue archivosValue)throws NafinException;
    
    /**
    * @param ruta
    * @throws NafinException
    */
    public void avisosGeneradosPorNafin(String ruta)throws NafinException;
    
    public String obtenGarantias(String acuse)throws Exception;    
     
    

	public List<Map> obtieneResultadosDeSupervision(String interFinanciero, String calMes, String calAnio, String relacionGarantias, boolean hayIntermediario)
			throws NafinException;

	public List<Map> obtieneAvisosDeSupervision(String interFin, String anio, String mes)
			throws NafinException;

	public boolean guardaEnBitacoraAvisoSup(String interFin, String anio, String mes, String tipoCalendario)
			throws NafinException;

	public ArchivosCargaValue getArchivo(String tipoPerfil, String idArchivo, String nombreArchivo, String interFin, String anio, String mes, String recibo, String tipoCalendario)
			throws NafinException;
        
        public Integer validaUsuario(String usuario) throws NafinException;
        
        public boolean validaExtemporaneo(ArchivosCargaValue archivosValue) throws NafinException;
        
        
        

        /**
         * Agustin Bautista
         * Metodo para la pantalla Productos/Supervisi�n/Capturas/Alta calendario.
         * Cuando se trata de una correcci�n del archivo, se debe validar si el Intermediario Financiero ya tiene una carga
         * ordinaria, extemporanea o si no tiene un calendario cargado para el mes-anio seleccionado.
         * Este m�todo realiza las consultas necesarias regresa el tipo de calendario que se carg�, o una cadena vac�a si no hay archivos
         * @param icCveSiag
         * @param calMes
         * @param calAnio
         * @return O: Ordinario, E: Extemporaneo, cadena vac�a en otro caso.
         */
        public String validaTipoDeCalendarioACorregir(Integer icCveSiag, Integer calMes, Integer calAnio);
        
        
        
       
        /**        
         * Julio Morales
         * @param mes
         * @param anio
         * @param claveSiag
         * @param tipo
         * @return String
         */
        public String obtenerNumGarantias(String mes, String anio, String claveSiag, String tipo)  throws NafinException;
        /**        
         * Julio Morales
         * @param icCveSiag
         * @param calMes
         * @param calAnio
         * @param calTipo
         * @return String
         */
        public String obtenerCalConsecutivo(String icCveSiag, String calMes, String calAnio, String calTipo ) throws NafinException;
        /**        
         * Julio Morales
         * @param mes
         * @param anio
         * @param claveSiag
         * @param tipo
         * @return String
         */
        public String obtenerTotalGarantias(String mes, String anio, String claveSiag, String tipo) throws NafinException;
        /**        
         * Julio Morales
         * @param mes
         * @param anio
         * @param iNoCliente         
         * @return 
         */
        public void adecuarBitacoraSupervision(String mes, String anio, String iNoCliente,  AccesoDB con);
        /**        
         * Julio Morales
         * @param siag                
         * @return List
         */
        public List obtieneResultadoSIAG(String siag);
		  
		 
        
       

    /**
     * Para la pantalla Alta de calendario, actualiza un calendario cuando es de correccion
     * @param MES
     * @param ANIO
     * @param TIPO_CAL
     * @param acuse
     * @param usuario
     * @param noArchivos
     * @return
     * @throws NafinException
     */
    public boolean updateCalendarioCorreccion(String MES, String ANIO, String TIPO_CAL, String acuse, String usuario,
                                    String noArchivos) throws NafinException;
    
    /**
     * Metodo para la pantalla Productos / Supervisi�n / Monitor. Obtiene el color del sem�foro que se mostrar� en el grid
     * de Resultados de Consulta de acuerdo a una serie de reglas y validacionse desglosadas en el cuerpo del m�todo.
     * Este metodo se aplica para Resultados de supervision y para Dictamen d Supervisi�n.
     * @param fechaRealStr: Fecha en que se carg� el archivo, si es nulo, indica que no se ha realizado carga de archivos
     * @param fechaLimiteStr: Fecha limite para realizar la carga del archivo
     * @param tipoArchivo: DICTAMEN o RESOLUCION
     * @return el color que se mostrar� en el sem�foro: VERDE, AMARILLO, ROJO, cadena vacia en caso de que fechaLimite=null
     * @throws NafinException
     */
    public String obtieneSemaforoMonitordeSupervision(String fechaRealStr, String fechaLimiteStr, String tipoArchivo) throws NafinException;
}
