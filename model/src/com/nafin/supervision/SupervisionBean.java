package com.nafin.supervision;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.MalformedURLException;
import java.net.URL;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.Resource;

import javax.ejb.Schedule;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.WebServiceException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Correo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;

import org.tempuri.Procesos;
import org.tempuri.ProcesosSoap;
import org.tempuri.Resultado;
import org.tempuri.ResultadoBorrar;
import org.tempuri.ResultadoSalida;


@Stateless(name = "SupervisionEJB", mappedName = "SupervisionEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class SupervisionBean implements Supervision {
    @Resource
    SessionContext sessionContext;
    private static final String DOCUMENTS_FOLDER = "documentsUpload";
    private static final Charset UTF_8 = StandardCharsets.UTF_8;

    private static final Log LOG = ServiceLocator.getInstance().getLog(SupervisionBean.class);

    public SupervisionBean() {
    }

    @Override
    public boolean existeDoctosResultDictamenSup(Long cveSiag, Integer anio, Integer mes, String tipoCal,
                                                 String tipoInfo) {

        LOG.info("SupervisionBean::existeDoctosResultDictamenSup(I)");
        LOG.debug("SupervisionBean::existeDoctosResultDictamenSup cveSiag="+cveSiag);
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder qrySentencia = new StringBuilder("");
        boolean existeCargaDocto = false;
        try {

            con.conexionDB();

            qrySentencia.append("SELECT count(*) contador " + "  FROM sup_calendario_if " + " WHERE ic_cve_siag = ? " +
                                "   AND cal_anio = ? " + "   AND cal_mes = ? " + "   AND cal_tipo = ? ");

            if ("R".equals(tipoInfo)) {
                qrySentencia.append("   AND cal_doc_solicitud IS NOT NULL ");
            } else if ("D".equals(tipoInfo)) {
                qrySentencia.append("   AND cal_doc_dictamen IS NOT NULL ");
            }

            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setLong(1, cveSiag);
            ps.setInt(2, anio);
            ps.setInt(3, mes);
            ps.setString(4, tipoCal);

            rs = ps.executeQuery();

            if (rs != null && rs.next()) {
                existeCargaDocto = rs.getInt("contador") > 0 ? true : false;
            }

            return existeCargaDocto;

        } catch (Exception e) {
            LOG.error("SupervisionBean::existeDoctosResultDictamenSup(Error)");
            throw new AppException("Error inesperado al validar si existe docto de supervision cargado", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::existeDoctosResultDictamenSup(F)");
        }

    }

    @Override
    public Map<String, String> cargaDoctosResultDictamenSup(Long cveSiag, Integer anio, Integer mes, String tipoCal,
                                                            String tipoInfo, String usuario, String nombreArch,
                                                            String path) {

        LOG.info("SupervisionBean::cargaDoctosResultDictamenSup(I)");

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder qrySentencia = new StringBuilder("");
        Map<String, String> mResultado = new HashMap<String, String>();
        Boolean exito = false;

        try {

            con.conexionDB();

            Long icIF = 0L;

            qrySentencia.append("SELECT DISTINCT sdi.ic_if " + " FROM sup_calendario_if sci JOIN sup_datos_if sdi " +
                                "      ON sci.ic_cve_siag = sdi.ic_cve_siag " + "WHERE sci.ic_cve_siag = ? ");

            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setLong(1, cveSiag);
            rs = ps.executeQuery();

            if (rs.next()) {
                icIF = rs.getLong("ic_if");
            }

            rs.close();
            ps.close();

            //Si es alguna correcci�n obtengo la fecha limite de resultado de supervision o dictamen.
            //Si la correcci�n se realiza antes o el d�a de la fecha limite, se debe actualizar en bitacora la fecha de la carga
            boolean actualizaBitacora = false;
            if("R".equals(tipoInfo) || "D".equals(tipoInfo)) {
                actualizaBitacora = true;
            }  else if("A".equals(tipoInfo) || "B".equals(tipoInfo)) {
                Date fechaLimiteDate = null;
                String fechaLimite = "";
                if("A".equals(tipoInfo)) {
                    fechaLimite = "CAL_FEC_LIM_ENT_R";
                } else if("B".equals(tipoInfo)) {
                    fechaLimite = "CAL_FEC_LIM_ENT_D";
                }
                qrySentencia =
                    new StringBuilder("SELECT " + fechaLimite + " FROM SUP_CALENDARIO_IF " +
                                        "WHERE CAL_ANIO=? and CAL_MES=? and IC_CVE_SIAG=? and CAL_TIPO=? and CAL_ESTATUS='A'");
                ps = con.queryPrecompilado(qrySentencia.toString());
                ps.setInt(1, anio);
                ps.setInt(2, mes);
                ps.setLong(3, cveSiag);
                ps.setString(4, tipoCal);
                rs = ps.executeQuery();
                if (rs.next()) {
                    fechaLimiteDate = rs.getDate(1);
                }
                rs.close();
                ps.close();
                if(null!=fechaLimiteDate) {
                    if(fechaLimiteDate.compareTo(new Date()) > 0) {
                        actualizaBitacora = true;
                    }
                }
            }

            Date fechaHora = new Date();
            SimpleDateFormat formatoFechaHora = new SimpleDateFormat("ddMMYYYYHHmmss");
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
            SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

            String fechaAcuse = formatoFecha.format(fechaHora);
            String horaAcuse = formatoHora.format(fechaHora);
            String acuse = String.valueOf(icIF) + tipoInfo + formatoFechaHora.format(fechaHora);
            String identificador = acuse + nombreArch.substring(nombreArch.lastIndexOf("."));
            String campoTipoInfo = "";
            String campoBitSup = "";

            //Verifico que voy a actualizar, resultados o dictamen
            if ("R".equals(tipoInfo) || "A".equals(tipoInfo)) {
                campoTipoInfo = "cal_doc_solicitud";
                campoBitSup = "bit_fec_pub_res_sup";
            } else if ("D".equals(tipoInfo) || "B".equals(tipoInfo)) {
                campoTipoInfo = "cal_doc_dictamen";
                campoBitSup = "bit_fec_pub_dic";
            }

            //Armo el update
            String sentencia = "UPDATE sup_calendario_if " + "   SET " + campoTipoInfo + " = ? ";
            
            //Si es alguna correccion, se debe actualizar los campos CAL_DOC_SOLICTUD_ACT o CAL_DOC_DICTAMEN_ACT
            if("A".equals(tipoInfo)){
                sentencia += ", CAL_DOC_SOLICTUD_ACT = SYSDATE ";
            } else if("B".equals(tipoInfo)) {
                sentencia += ", CAL_DOC_DICTAMEN_ACT = SYSDATE ";
            }

            sentencia += " WHERE ic_cve_siag = ? " + "   AND cal_anio = ? " + "   AND cal_mes = ? " +
                         "   AND cal_tipo = ? ";
            
            //Si es resultado o dictamen, debe validar que el campo cal_doc_solicitud o cal_doc_dictamen respectivamente sean nulos
            //En el caso de una correcci�n se estaria sobreescribiendo el valor.
            if("R".equals(tipoInfo) || "D".equals(tipoInfo)) {
                sentencia += "   AND " + campoTipoInfo + " IS NULL ";
            }

            ps = con.queryPrecompilado(sentencia);
            ps.setString(1, identificador);
            ps.setLong(2, cveSiag);
            ps.setInt(3, anio);
            ps.setInt(4, mes);
            ps.setString(5, tipoCal);

            ps.executeUpdate();
            ps.close();

            qrySentencia =
                new StringBuilder("INSERT INTO sup_acuse_cal " +
                                  "            (cc_acuse, ic_usuario, acc_mes_pago, acc_anio_pago, " +
                                  "             acc_no_archivos, acc_tipo_carga, acc_fecha_acuse " + "            ) " +
                                  "     VALUES (?, ?, ?, ?, " +
                                  "             ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:mi:ss') " + "            )");

            ps = con.queryPrecompilado(qrySentencia.toString());
            ps.setString(1, acuse);
            ps.setString(2, usuario);
            ps.setInt(3, mes);
            ps.setInt(4, anio);
            ps.setInt(5, 1);
            ps.setString(6, tipoCal);
            ps.setString(7, fechaAcuse + " " + horaAcuse);
            ps.executeUpdate();
            ps.close();

            if(actualizaBitacora==true) {
                qrySentencia =
                    new StringBuilder("UPDATE bit_supervision " + "   SET " + campoBitSup +
                                  " = TO_DATE(?, 'DD/MM/YYYY HH24:mi:ss')  " +
                                  " WHERE ic_if = ? AND cal_anio = ? AND cal_mes = ? AND bit_tipo = ? ");

                ps = con.queryPrecompilado(qrySentencia.toString());
                ps.setString(1, fechaAcuse + " " + horaAcuse);
                ps.setLong(2, icIF);
                ps.setInt(3, anio);
                ps.setInt(4, mes);
                ps.setString(5, tipoCal);
                ps.executeUpdate();
                ps.close();
            }
            
            String base64 = this.encodeFileToBase64Binary(path + File.separator + nombreArch);
            URL url = this.getWSDireccion();
            Procesos procesos = new Procesos(url);
            ProcesosSoap procesosSoap = procesos.getProcesosSoap();
            Resultado resultado = procesosSoap.ingresaArchivo("NESUPGAR", identificador, nombreArch, "A", base64);

            LOG.info(resultado.getStEstatus());
            LOG.info(resultado.getStError());

            if ("OK".equals(resultado.getStEstatus())) {
                exito = true;
            }
            //exito = true;
            mResultado.put("CARGA_ACUSE", acuse);
            mResultado.put("CARGA_FECHA_ACUSE", fechaAcuse);
            mResultado.put("CARGA_HORA_ACUSE", horaAcuse);
            mResultado.put("CARGA_EXITO", exito.toString());

            return mResultado;

        } catch (Exception e) {
            LOG.error("SupervisionBean::cargaDoctosResultDictamenSup(Error)");
            throw new AppException("Error inesperado al cargar archivo en ONBASE ", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::cargaDoctosResultDictamenSup(F)");
        }
    }

    @Override
    public boolean isWebServiceDisponible() {
        boolean result = false;
        BufferedReader serviceResponse = null;
        String responseLine = null;
        LOG.info("Revisando conexion Web Service ONBASE ...");

        try {
            URL webServiceCheckURL = this.getWSDireccion();
            InputStreamReader inpStream = new InputStreamReader(webServiceCheckURL.openStream());
            serviceResponse = new BufferedReader(inpStream);

            responseLine = serviceResponse.readLine();

            while (responseLine != null) {
                responseLine = serviceResponse.readLine();
                break;
            }

            result = true;

            LOG.info("Connection Web Service ONBASE OK!");

            if (inpStream != null) {
                inpStream.close();
            }

            if (serviceResponse != null) {
                serviceResponse.close();
            }

        } catch (Exception e) {
            LOG.fatal("Web Service ONBASE no disponible!", e);
        }


        return result;
    }

    /**
     * Este metodo devuelve los parametros de Dispersion: Clave de la PYME, Cuentas de Correo NAFIN y
     * Cuentas para una EPO en particular.
     */
    public HashMap getParametrosDeSupervision(String claveIF) throws NafinException {
        LOG.info("SupervisionBean::getParametrosDeDispersion(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;

        HashMap resultado = new HashMap();
        String[] cuentasSocios = null;
        String[] cuentasEjecutivos = null;
        List cuentas = new ArrayList();
        ResultSet rs = null;
        String cveSIAG = "";
        String ejecutivoPrincipal = "";
        String socioPrincipal = "";
        try {
            con.conexionDB();
            qrySentencia =
                "select IC_CLIENTE, IC_TIPO, IC_CVE_SIAG, CS_PRINCIPAL \n" + "  from sup_datos_if t\n" +
                " WHERE IC_IF = " + claveIF + "   AND IC_TIPO = 'C' ";

            PreparedStatement ps = con.queryPrecompilado(qrySentencia);

            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                //cuentasSocios[i] = rs.getString("IC_CLIENTE");
                cuentas.add(rs.getString("IC_CLIENTE"));
                cveSIAG = rs.getString("IC_CVE_SIAG");
                if (rs.getString("CS_PRINCIPAL").equals("S")) {
                    socioPrincipal = rs.getString("IC_CLIENTE");
                }
            }
            cuentasSocios = new String[cuentas.size()];
            cuentasSocios = (String[]) cuentas.toArray(cuentasSocios);

            ps.clearParameters();


            qrySentencia =
                "select IC_CLIENTE, IC_TIPO , IC_CVE_SIAG, CS_PRINCIPAL \n" + "  from sup_datos_if t\n" +
                " WHERE IC_IF = " + claveIF + "   AND IC_TIPO = 'E' ";

            ps = con.queryPrecompilado(qrySentencia);

            rs = ps.executeQuery();

            int x = 0;
            cuentas = new ArrayList();
            while (rs.next()) {
                cuentas.add(rs.getString("IC_CLIENTE"));
                cveSIAG = rs.getString("IC_CVE_SIAG");
                if (rs.getString("CS_PRINCIPAL").equals("S")) {
                    ejecutivoPrincipal = rs.getString("IC_CLIENTE");
                }
            }
            cuentasEjecutivos = new String[cuentas.size()];
            cuentasEjecutivos = (String[]) cuentas.toArray(cuentasEjecutivos);


            ps.clearParameters();
            resultado.put("CUENTAS_SOCIOS", cuentasSocios);
            resultado.put("CUENTAS_EJECUTIVOS", cuentasEjecutivos);
            resultado.put("CVE_SIAG", cveSIAG);
            resultado.put("SOCIO_PRINCIPAL", socioPrincipal);
            resultado.put("EJECUTIVO_PRINCIPAL", ejecutivoPrincipal);

            if (ps != null)
                ps.close();

            rs.close();

        } catch (Exception e) {
            LOG.error("SupervisionBean::getParametrosDeDispersion(Exception)");
            LOG.error("claveIF  = " + claveIF);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("SupervisionBean::getParametrosDeDispersion(S)");
        }

        return resultado;
    }


    /**
     * Este metodo inserta en la tabla COM_PARAMETRIZACION_DISPERSION las cuentas de correo NAFIN y EPO asi como
     * de la PYME para el servicio de Dispersion
     */
    public void setParametrosDeSupervision(String claveIF, String contactos, String ejecutivos, String cveSIAG,
                                           String contactoPrincipal, String ejecutivoPrincipal) throws NafinException {

        LOG.info("SupervisionBean::setParametrosDeDispersion(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        List lVarBind = new ArrayList();
        boolean lbOK = true;

        try {

            con.conexionDB();

            String contactosN = (contactos == null) ? "" : contactos;
            String ejecutivosN = (ejecutivos == null) ? "" : ejecutivos;
            String contactosA[] = contactosN.split(",");
            String ejecutivosA[] = ejecutivosN.split(",");

            if (existeRegistroDeSupervision(claveIF)) {
                qrySentencia = "DELETE FROM sup_datos_if WHERE IC_IF = ?";
                lVarBind.add(new Integer(claveIF));
                con.ejecutaUpdateDB(qrySentencia, lVarBind);
            }
            lVarBind = new ArrayList();
            for (int i = 0; i < contactosA.length; i++) {
                if (!contactosA[i].equals("")) {
                    lVarBind = new ArrayList();
                    qrySentencia =
                        "INSERT INTO " + "       sup_datos_if " + "               ( " +
                        "                       IC_IF, " + "                       IC_CLIENTE, " +
                        "                       IC_TIPO," + "                       IC_CVE_SIAG, " +
                        "                       CS_PRINCIPAL" + "               ) " + "VALUES " + "               ( " +
                        "                       ?, " + "                       ?, " + "                       ?," +
                        "                       ?, " + "                       ? " + "               ) ";

                    lVarBind.add(new Integer(claveIF));
                    lVarBind.add(contactosA[i]);
                    lVarBind.add("C");
                    lVarBind.add(cveSIAG);
                    lVarBind.add(contactosA[i].equals(contactoPrincipal) ? "S" : "N");
                    con.ejecutaUpdateDB(qrySentencia, lVarBind);
                }
            }


            for (int i = 0; i < ejecutivosA.length; i++) {
                if (!ejecutivosA[i].equals("")) {
                    lVarBind = new ArrayList();
                    qrySentencia =

                        "INSERT INTO " + "       sup_datos_if " + "               ( " +
                        "                       IC_IF, " + "                       IC_CLIENTE, " +
                        "                       IC_TIPO, " + "                       IC_CVE_SIAG, " +
                        "                       CS_PRINCIPAL" + "               ) " + "VALUES " + "               ( " +
                        "                       ?, " + "                       ?, " + "                       ?,  " +
                        "                       ?, " + "                       ? " + "               ) ";

                    lVarBind.add(new Integer(claveIF));
                    lVarBind.add(ejecutivosA[i]);
                    lVarBind.add("E");
                    lVarBind.add(cveSIAG);
                    lVarBind.add(ejecutivosA[i].equals(ejecutivoPrincipal) ? "S" : "N");
                    con.ejecutaUpdateDB(qrySentencia, lVarBind);
                }
            }


        } catch (Exception e) {
            LOG.error("SupervisionBean::setParametrosDeDispersion(Exception)");
            LOG.error("claveIF  = " + claveIF);
            LOG.error("contactos = " + contactos);
            LOG.error("ejecutivos   = " + ejecutivos);
            LOG.error(e);
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.error("SupervisionBean::setParametrosDeSupervision(S)");
        }
    }


    /**
     * Este metodo se encarga de revisar si ya se habian parametrizado datos
     * dispersion para la epo correspondiente.
     */
    public boolean existeRegistroDeSupervision(String claveIF) throws NafinException {

        LOG.info("SupervisionBean::existeRegistroDeSupervision(E)");

        AccesoDB con = new AccesoDB();
        String qrySentencia = null;
        List lVarBind = new ArrayList();
        Registros registros = null;
        boolean existeRegistro = false;

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT                                                           " +
                "     DECODE(COUNT(1),0,'false','true') AS EXISTE_SUPERVISION " +
                "FROM                                                             " +
                "       sup_datos_if                                      " +
                "WHERE                                                            " +
                "       IC_IF  = ?                                                   ";

            lVarBind.add(claveIF);

            registros = con.consultarDB(qrySentencia, lVarBind, false);
            if (registros != null && registros.next()) {
                existeRegistro = registros.getString("EXISTE_SUPERVISION").equals("true") ? true : false;
            }

        } catch (Exception e) {
            LOG.error("SupervisionBean::existeRegistroDeSupervision(Exception)");
            LOG.error("claveIF  = " + claveIF);
            e.printStackTrace();
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("SupervisionBean::existeRegistroDeSupervision(S)");
        }

        return existeRegistro;
    }


    public String validaCalendario(String cveSIAG, String CAL_FEC_LIM_ENT_E, String CAL_FEC_LIM_ENT_R, String CAL_FEC_LIM_ENT_A,
                                   String CAL_FEC_LIM_ENT_D, String CAL_NO_AUTORIZA, String CAL_REL_GARANTIAS,
                                   String MES, String ANIO, String TIPO_CAL) throws NafinException {
        LOG.info("SupervisionBean::validaCalendario(E)");
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String qrySentencia = "";
        int existe = 0;
        int existeIF = 0;
        String ERROR = "";
        boolean lbOK = true;
        try {
            con.conexionDB();
            qrySentencia =
                "SELECT count(*) existe " + 
                "  FROM sup_calendario_if " + 
                " WHERE IC_CVE_SIAG = " + cveSIAG +
                "   AND CAL_MES = " + MES +
                "   AND CAL_ANIO = " + ANIO + 
                "   AND CAL_TIPO = '" + TIPO_CAL + "' " +
                "   AND CAL_ESTATUS = 'A'";
            rs = con.queryDB(qrySentencia);
            LOG.debug("qrySentencia" + qrySentencia);
            while (rs.next()) {
                existe = rs.getInt("existe");
            }
            rs.close();
            
            qrySentencia = 
                "SELECT count(*) existeIF " + 
                "  FROM sup_datos_if " + 
                " WHERE IC_CVE_SIAG = " + cveSIAG;
            rs = con.queryDB(qrySentencia);
            LOG.debug("qrySentencia" + qrySentencia);
            while (rs.next()) {
                existeIF = rs.getInt("existeIF");
            }
            
            if (existe > 0) {
                ERROR = "Ya existe una carga previa del mismo intermediario, mes, a�o y tipo";
            } else {
                if (TIPO_CAL.equals("E") && (CAL_NO_AUTORIZA == null || CAL_NO_AUTORIZA.equals(""))) {
                    ERROR = "En carga extemporanea debe venir el campo No. de Autorizacion";
                } else {
                    if (existeIF == 0) {
                        ERROR = "El intermediario no existe en la parametrizacion";
                    }
                }

            }

        } catch (Exception e) {
            LOG.error("SupervisionBean::validaCalendario(Exception)");
            LOG.error("claveIF  = " + cveSIAG);
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.error("SupervisionBean::validaCalendario(S)");
        }

        return ERROR;
    }

    public boolean insertaCalendario(String cveSIAG, String CAL_FEC_LIM_ENT_E, String CAL_FEC_LIM_ENT_R,String CAL_FEC_LIM_ENT_A,
                                     String CAL_FEC_LIM_ENT_D, String CAL_NO_AUTORIZA, String CAL_REL_GARANTIAS,
                                     String MES, String ANIO, String TIPO_CAL) throws NafinException {
        AccesoDB con = new AccesoDB();
        String qrySentencia = "";
        List lVarBind = new ArrayList();
        ResultSet rs = null;
        boolean lbOK = true;
        int cal_consecutivo = 0;
        int cal_consecutivoAnt = 0;
        LOG.info("SupervisionBean::insertaCalendario(E)");
        try {
            con.conexionDB();

            qrySentencia =
                "SELECT  CAL_CONSECUTIVO  FROM SUP_CALENDARIO_IF WHERE IC_CVE_SIAG = " + cveSIAG + " AND  CAL_MES = " +
                MES + " AND CAL_ANIO = " + ANIO + " AND CAL_TIPO = '" + TIPO_CAL + "' " + " AND CAL_ESTATUS = 'P'";
            rs = con.queryDB(qrySentencia);
            if (rs.next())
                cal_consecutivoAnt = rs.getInt(1);
            con.cierraStatement();

            qrySentencia =
                "DELETE FROM SUP_CALENDARIO_DETALLE WHERE IC_CVE_SIAG = " + cveSIAG + " AND  CAL_CONSECUTIVO = " +
                cal_consecutivoAnt;

            con.ejecutaUpdateDB(qrySentencia, lVarBind);

            qrySentencia =
                "DELETE FROM SUP_CALENDARIO_IF WHERE IC_CVE_SIAG = " + cveSIAG + " AND  CAL_MES = " + MES +
                " AND CAL_ANIO = " + ANIO + " AND CAL_TIPO = '" + TIPO_CAL + "' " + " AND CAL_ESTATUS = 'P'";

            con.ejecutaUpdateDB(qrySentencia, lVarBind);


            qrySentencia =
                "select nvl(MAX(CAL_CONSECUTIVO),0)+1" + " from SUP_CALENDARIO_IF " + " where IC_CVE_SIAG =" + cveSIAG;
            rs = con.queryDB(qrySentencia);
            if (rs.next())
                cal_consecutivo = rs.getInt(1);
            con.cierraStatement();

            qrySentencia =
                "INSERT INTO SUP_CALENDARIO_IF (" + 
                "CAL_CONSECUTIVO, " + 
                "IC_CVE_SIAG, " + 
                "CAL_FEC_LIM_ENT_E, CAL_FEC_LIM_ENT_R, " +
                "CAL_FEC_LIM_ENT_A, " + 
                "CAL_FEC_LIM_ENT_D, " + 
                "CAL_NO_AUTORIZA, " +
                //"CAL_REL_GARANTIAS," +
                "CAL_MES,  " + 
                "CAL_ANIO,  " + 
                "CAL_TIPO";
            
            if("C".equals(TIPO_CAL)) {
                qrySentencia += ",  CAL_FECHA_ACT"; 
            }
            
            qrySentencia +=
                " ) VALUES (" + 
                "?,  " + 
                "?,  " +
                "TO_DATE(?, 'DD/MM/YYYY'),  " + 
                "TO_DATE(?, 'DD/MM/YYYY'),  " + 
                "TO_DATE(?, 'DD/MM/YYYY'), TO_DATE(?, 'DD/MM/YYYY'), " +
                "?,  " + 
                "?," + 
                "?," + 
                "?";

            if("C".equals(TIPO_CAL)) {
                qrySentencia += ", SYSDATE"; 
            }
            qrySentencia +=
                ") ";

            lVarBind.add(cal_consecutivo);
            lVarBind.add(Integer.parseInt(cveSIAG));
            lVarBind.add(CAL_FEC_LIM_ENT_E);
            lVarBind.add(CAL_FEC_LIM_ENT_R);
            lVarBind.add(CAL_FEC_LIM_ENT_A);
            lVarBind.add(CAL_FEC_LIM_ENT_D);
            lVarBind.add(CAL_NO_AUTORIZA);
            //lVarBind.add(CAL_REL_GARANTIAS);
            lVarBind.add(MES);
            lVarBind.add(ANIO);
            lVarBind.add(TIPO_CAL);

            LOG.debug ("qrySentencia  "+qrySentencia+" \n  lVarBind  "+lVarBind);
    
            con.ejecutaUpdateDB(qrySentencia, lVarBind);

            this.insertaDetalleCal(cal_consecutivo, cveSIAG, CAL_REL_GARANTIAS, con);
            
        } catch (Exception e) {
            LOG.error("SupervisionBean::insertaCalendario(Exception)");
            LOG.error("claveIF  = " + cveSIAG);
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("SupervisionBean::insertaCalendario(S)");
        }
        return true;
    }

    private boolean insertaDetalleCal(int cal_consecutivo, String cveSIAG, String CAL_REL_GARANTIAS,
                                      AccesoDB con) throws NafinException {
        //AccesoDB                con                             = new AccesoDB();
        String qrySentencia = "";
        List lVarBind = new ArrayList();
        ResultSet rs = null;
        boolean lbOK = true;
        int calConseDet = 0;
        LOG.info("Supervision::insertaDetalleCal (E)");
        try {
            //con.conexionDB();


            int cveSIAGE = Integer.parseInt(cveSIAG);

            String[] relaciongar = CAL_REL_GARANTIAS.split(",");

            for (int i = 0; i < relaciongar.length; i++) {
                if (relaciongar[i] != null && !relaciongar[i].trim().equals("")) {
                    qrySentencia =
                        "select nvl(MAX(CAL_DET_CONSECUTIVO),0)+1" + " from SUP_CALENDARIO_DETALLE " +
                        " where IC_CVE_SIAG =" + cveSIAG + " AND CAL_CONSECUTIVO = " + cal_consecutivo;
                    rs = con.queryDB(qrySentencia);
                    if (rs.next())
                        calConseDet = rs.getInt(1);

                    rs.close();
                    con.cierraStatement();


                    /*qrySentencia = "INSERT INTO SUP_CALENDARIO_DETALLE (" +
                            "CAL_DET_CONSECUTIVO,\n" +
                            "CAL_CONSECUTIVO,\n" +
                            "IC_CVE_SIAG,\n" +
                            "CAL_REL_GARANTIAS ) VALUES (" +
                            "?, \n" +
                            "?, \n" +
                            "?, \n" +
                            "?) \n";
                            lVarBind                        = new ArrayList();
                            lVarBind.add(calConseDet);
                            lVarBind.add(cal_consecutivo);
                            lVarBind.add(cveSIAGE);
                            lVarBind.add(relaciongar[i]);*/

                    qrySentencia =
                        "INSERT INTO SUP_CALENDARIO_DETALLE (" + "CAL_DET_CONSECUTIVO,\n" + "CAL_CONSECUTIVO,\n" +
                        "IC_CVE_SIAG,\n" + "CAL_REL_GARANTIAS ) VALUES (" + "  " + calConseDet + ",\n" + "          " +
                        cal_consecutivo + ",\n" + "          " + cveSIAGE + ",\n" + "          '" + relaciongar[i] +
                        "')";
                    //LOG.info("query especial "+qrySentencia);
                    con.ejecutaSQL(qrySentencia);


                    //con.ejecutaUpdateDB(qrySentencia, lVarBind);
                }
            }


        } catch (Exception e) {
            LOG.error("SupervisionBean::insertaDetalleCal(Exception)");
            LOG.error("claveIF  = " + cveSIAG);
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            //con.cierraStatement();
            //con.terminaTransaccion(lbOK);
            // if(con.hayConexionAbierta())
            //       con.cierraConexionDB();
            LOG.info("SupervisionBean::insertaDetalleCal(S)");
        }
        return true;

    }


    public Vector getCalInsertado(String CVE_SIAG, String CAL_MES, String CAL_ANIO,
                                  String CAL_TIPO) throws NafinException {
        LOG.info("SupervisionBean::getCalInsertado(E)");
        boolean resultado = true;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String qrySentencia = "";
        Vector vecRenglones = new Vector();
        Vector vecColumnas = new Vector();
        try {
            con.conexionDB();

            qrySentencia =
                "SELECT CAL_CONSECUTIVO, " + 
                "       IC_CVE_SIAG, " +
                "       TO_CHAR(CAL_FEC_LIM_ENT_E,'DD/MM/YYYY') CAL_FEC_LIM_ENT_E, " +
                "       TO_CHAR(CAL_FEC_LIM_ENT_R,'DD/MM/YYYY') CAL_FEC_LIM_ENT_R, " +
                "       TO_CHAR(CAL_FEC_LIM_ENT_A,'DD/MM/YYYY') CAL_FEC_LIM_ENT_A, " +
                "       TO_CHAR(CAL_FEC_LIM_ENT_D,'DD/MM/YYYY')CAL_FEC_LIM_ENT_D, " + 
                "       CAL_NO_AUTORIZA, " +
                "       CAL_REL_GARANTIAS, " + 
                "       CAL_ESTATUS, " + 
                "       CAL_FECHA_INSER, " +
                "       decode(cal_mes, 1, 'ENE', 2, 'FEB', 3, 'MAR', 4, 'ABR', 5, 'MAY', 6, 'JUN', 7, 'JUL', 8, 'AGO', 9, 'SEP', 10, 'OCT', 11, 'NOV', 12, 'DIC',  13, '1er SEM', 14, '2do SEM') CAL_MES, " +
                "       CAL_ANIO " + 
                "  FROM sup_calendario_if " + 
                " WHERE " + 
                "   CAL_MES = " + CAL_MES +
                "   AND CAL_ANIO = " + CAL_ANIO + 
                "   AND CAL_TIPO = '" + CAL_TIPO + "' " + 
                "   AND IC_CVE_SIAG = " + CVE_SIAG + 
                "   AND CAL_ESTATUS = 'P'";
            rs = con.queryDB(qrySentencia);
            LOG.debug("qrySentencia:" + qrySentencia);
            while (rs.next()) {
                vecColumnas = new Vector();
                /*0*/vecColumnas.addElement((rs.getString("CAL_CONSECUTIVO") == null) ? "" :
                                            rs.getString("CAL_CONSECUTIVO")); // 0
                /*1*/vecColumnas.addElement((rs.getString("IC_CVE_SIAG") == null) ? "" :
                                            rs.getString("IC_CVE_SIAG")); // 1 nombre de la pyme
                /*4*/vecColumnas.addElement((rs.getString("CAL_FEC_LIM_ENT_E") == null) ? "" :
                                            rs.getString("CAL_FEC_LIM_ENT_E")); // 4
                /*5*/vecColumnas.addElement((rs.getString("CAL_FEC_LIM_ENT_R") == null) ? "" :
                                            rs.getString("CAL_FEC_LIM_ENT_R")); // 4
                /*6*/vecColumnas.addElement((rs.getString("CAL_FEC_LIM_ENT_A") == null) ? "" :
                                            rs.getString("CAL_FEC_LIM_ENT_A")); // 5 nombre de la moneda
                /*7*/vecColumnas.addElement((rs.getString("CAL_FEC_LIM_ENT_D") == null) ? "" :
                                            rs.getString("CAL_FEC_LIM_ENT_D")); // 6
                /*8*/vecColumnas.addElement((rs.getString("CAL_REL_GARANTIAS") == null) ? "" :
                                            rs.getString("CAL_REL_GARANTIAS")); // 7
                vecColumnas.addElement((rs.getString("CAL_MES") == null) ? "" : rs.getString("CAL_MES")); // 8
                vecColumnas.addElement((rs.getString("CAL_ANIO") == null) ? "" : rs.getString("CAL_ANIO")); // 9
                vecRenglones.addElement(vecColumnas);
            } //while
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::getCalInsertado Exception " + e);
            resultado = false;
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::getCalInsertado(S)");
        }
        return vecRenglones;
    }

    public boolean updateCalendario(String MES, String ANIO, String TIPO_CAL, String acuse, String usuario,
                                    String noArchivos) throws NafinException {
        AccesoDB con = new AccesoDB();
        String qrySentencia = "";
        boolean lbOK = true;
        try {
            con.conexionDB();

            qrySentencia =
                "UPDATE SUP_CALENDARIO_IF SET " + " CAL_ESTATUS = 'A',   " + " CC_ACUSE = '" + acuse + "' " +
                " WHERE \n" + "   CAL_MES = " + MES + "   AND CAL_ANIO = " + ANIO + "   AND CAL_TIPO = '" + TIPO_CAL +
                "' " + "   AND CAL_ESTATUS = 'P' ";

            con.ejecutaUpdateDB(qrySentencia);
            creaAcuseCal(acuse, usuario, noArchivos, TIPO_CAL, MES, ANIO, con);
        } catch (Exception e) {
            LOG.error("SupervisionBean::updateCalendario(Exception)");
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("SupervisionBean::updateCalendario(S)");
        }
        return true;


    }


    public List obtenArchivosUsuario(ArchivosCargaValue archivosValue) throws NafinException {
        LOG.info("SupervisionBean::obtenArchivosUsuario(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        ResultSet rs = null;
        boolean ok = true;

        List archivosNom = new ArrayList();

        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                "SELECT ARCT_NOMBRE FROM sup_archivos_temp_if " + " WHERE ic_usuario = '" + archivosValue.getUsuario() +
                "' " + " AND ARCT_ESTATUS = 'P'";
            LOG.debug("qrySentencia" + qrySentencia);
            rs = con.queryDB(qrySentencia);
            while (rs.next()) {
                archivosNom.add(rs.getString(1));
            }
            rs.close();
            con.cierraConexionDB();
        } catch (Exception e) {
            LOG.error("SupervisionBean::obtenArchivosUsuario(Exception) "+e);
            ok = false;
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::obtenArchivosUsuario(S)");
        }
        return archivosNom;
    }

    public void deleteArchivosTemps(ArchivosCargaValue archivosValue) throws NafinException {
        LOG.info("SupervisionBean::deleteArchivosTemps(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        boolean ok = true;
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                "DELETE FROM sup_archivos_temp_if " + 
                " WHERE ic_usuario = '" + archivosValue.getUsuario() + "' " +
                " AND ARCT_ESTATUS = 'P'";
            LOG.debug("qrySentencia:"+qrySentencia);
            con.ejecutaSQL(qrySentencia);
        } catch (Exception e) {
            LOG.error("SupervisionBean::deleteArchivosTemps(Exception) "+e);
            ok = false;
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::deleteArchivosTemps(S)");
        }
    }


    public HashMap<String, Object> insertaArchivosTemps(ArchivosCargaValue archivosValue,
                                                        File f) throws NafinException {
        LOG.info("SupervisionBean::insertaArchivosTemps(E)");
        AccesoDB con = null;
        String qrySentencia = "";
        boolean ok = true;
        String mensaje = "OK";
        HashMap<String, Object> validaArchivos = null;
        try {
            con = new AccesoDB();
            con.conexionDB();
            String extension = f.getName().substring(f.getName().lastIndexOf("."));

            //validaciones
            List relacionGarantias = new ArrayList();

            validaArchivos = this.ValidaArchivos(archivosValue, f, relacionGarantias, con);

            if ((Integer) validaArchivos.get("validaRegreso") != 0) {
                switch ((Integer) validaArchivos.get("validaRegreso")) {
                case 1:
                    mensaje = "1";
                    break;
                case 2:
                    mensaje = "2";
                    break;
                case 3:
                    mensaje = "3";
                    break;
                case 4:
                    mensaje = "4";
                    break;
                default:
                    mensaje = "5";
                    break;
                }
            } else {
                String garantias = "";
                for (int r = 0; r < relacionGarantias.size(); r++) {
                    garantias += relacionGarantias.get(r) + "|";
                }


                qrySentencia =
                    "DELETE FROM sup_archivos_temp_if " + " WHERE arct_mes_pago = " + archivosValue.getMes() +
                    " AND arct_anio_pago = " + archivosValue.getAnio() +
                    //" AND ic_usuario = '"+archivosValue.getUsuario()+"' " +
                    " AND arct_nombre = '" + f.getName() + "' " + " AND ARCT_ESTATUS = 'P'";
                LOG.debug(qrySentencia);
                con.ejecutaSQL(qrySentencia);
                
                qrySentencia =
                    "INSERT INTO sup_archivos_temp_if " + 
                    "  (arct_mes_pago, " + 
                    "   arct_anio_pago, " +
                    "   arct_tipo_carga, " + 
                    "   arct_no_archivos, " + 
                    "   arct_nombre, " + 
                    "   arct_extension, " +
                    "   ic_usuario, " + 
                    "   rel_garantias, " + 
                    "   IC_IF) " + 
                    " values " + 
                    "  ( " +
                    archivosValue.getMes() + "," + 
                    archivosValue.getAnio() + "," +
                    "'" + archivosValue.getTipoCarga() + "'," +
                    archivosValue.getNoArchivos() + "," +
                    "'" + f.getName() + "'," + 
                    "'" + extension + "'," + 
                    "'" + archivosValue.getUsuario() + "'," + 
                    "'" + garantias + "'," +
                    archivosValue.getInterFinan() + 
                    ")";
                LOG.debug(qrySentencia);
                con.ejecutaSQL(qrySentencia);
            }
        } catch (Exception e) {
            LOG.error("SupervisionBean::insertaArchivosTemps(Exception) "+e);
            ok = false;
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::insertaArchivosTemps(S)");
        }
        validaArchivos.put("mensaje", mensaje);
        return validaArchivos;

    }

    private HashMap<String, Object> ValidaArchivos(ArchivosCargaValue archivosValue, File f, List relacionGarantias,
                                                   AccesoDB conn) throws Exception {
        LOG.info("SupervisionBean::ValidaArchivos(E)");
        String qrySentencia = "";
        boolean ok = true;
        ResultSet rs = null;
        String garantia = "";
        HashMap<String, Object> validaRegreso = new HashMap<>();
        validaRegreso.put("validaRegreso", 0);
        int noExistePDF = 1;
        try {
            String extension = f.getName().substring(f.getName().lastIndexOf("."));
            archivosValue.setExtension(extension);
            if (archivosValue.getExtension().indexOf("zip") > -1) {
                try {

                    //crea un buffer temporal para el archivo que se va descomprimir
                    ZipInputStream zis = new ZipInputStream(new FileInputStream(f));
                    ZipEntry salida;
                    //recorre todo el buffer extrayendo uno a uno cada archivo.zip y cre�ndolos de nuevo en su archivo original
                    while (null != (salida = zis.getNextEntry())) {
                        if (salida.isDirectory()) {
                            continue;
                        }

                        LOG.trace("Nombre del Archivo: " + salida.getName());
                        if (salida.getName().indexOf(".pdf") == -1) {
                            noExistePDF = 0;
                            break;
                        }
                        garantia = salida.getName().substring(0, salida.getName().lastIndexOf("."));

                        if (garantia.indexOf("/") > -1) {
                            garantia = garantia.substring(garantia.lastIndexOf("/") + 1);
                            
                        }

                        relacionGarantias.add(garantia);
                        zis.closeEntry();
                    }
                    zis.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                garantia = f.getName().substring(0, f.getName().lastIndexOf("."));
                archivosValue.setNombreArchivo(f.getName());
                relacionGarantias.add(garantia);
            }

            //Se valida la longitud de las garantias que se van a guardar
            String rGarantias = StringUtils.join(relacionGarantias.toArray(), ",");

            if (rGarantias.length() > 250) {

                String maximo = rGarantias.substring(0, 250);
                String maximo2 = maximo.substring(0, maximo.lastIndexOf(","));
                int maximoNum = maximo2.split(",").length;
                validaRegreso.put("validaRegreso", 5);
                validaRegreso.put("maximo", maximoNum);
                validaRegreso.put("margen", maximoNum - 3);
                maximo = null;
                maximo2 = null;
                rGarantias = null;
                return validaRegreso;
            }

            if (noExistePDF == 0) {
                validaRegreso.put("validaRegreso", 4);
                return validaRegreso;
            }

            qrySentencia =
                    "select s.ic_cve_siag " + 
                    "  from sup_datos_if s " + 
                    " where s.ic_cliente = '" +archivosValue.getUsuario() + "'" + 
                    "   and s.ic_if = " + archivosValue.getInterFinan();
            LOG.debug("qrySentencia:"+qrySentencia);
            rs = conn.queryDB(qrySentencia);
            String cveSIAG = "";
            while (rs.next()) {
                cveSIAG = rs.getString(1);
            }
            
            qrySentencia =
                "select count(t.CAL_CONSECUTIVO) " + 
                "  from sup_calendario_if t " + 
                " where t.cal_mes = " +archivosValue.getMes() + 
                "   and t.cal_anio = " + archivosValue.getAnio() +
                "   and t.cal_estatus = 'A' " + 
                "   and t.ic_cve_siag = " + cveSIAG;
            LOG.trace("getTipoCarga:"+archivosValue.getTipoCarga());
            if ("E".equals(archivosValue.getTipoCarga())) {
                qrySentencia += "   and t.cal_tipo = 'E' ";
            } else if("O".equals(archivosValue.getTipoCarga())) {
                qrySentencia += "   and t.cal_tipo = 'O' ";
            } else if("A".equals(archivosValue.getTipoCarga())) {
                qrySentencia += "   and trunc(sysdate) <= t.cal_fec_lim_ent_a ";
            } else {
                qrySentencia += " and trunc(sysdate) <= t.cal_fec_lim_ent_e ";
            }
            LOG.debug("qrySentencia:"+qrySentencia);
            rs = conn.queryDB(qrySentencia);
            int garantias = 0;
            while (rs.next()) {
                garantias = rs.getInt(1);
            }
            LOG.trace("garantias:"+garantias);

            qrySentencia =
                "select t.rel_garantias " + 
                "  from sup_archivos_temp_if t " + 
                " where t.ARCT_MES_PAGO = " + archivosValue.getMes() + 
                "   and t.ARCT_ANIO_PAGO = " + archivosValue.getAnio() +
                "   and t.ARCT_ESTATUS = 'C' ";
            LOG.debug("qrySentencia:"+qrySentencia);
            rs = conn.queryDB(qrySentencia  );
            String gartemp = "";
            while (rs.next()) {
                gartemp += rs.getString(1);
            }

            if (garantias > 0) {
                //String[] garantiasA = garantias.split(",");

                for (int i = 0; i < relacionGarantias.size(); i++) {
                    String nombreArchivo = (String) relacionGarantias.get(i);
                    String nombreArchivoT = nombreArchivo;

                    if (nombreArchivo.indexOf("_C") > -1) {
                        nombreArchivo = nombreArchivo.substring(0, nombreArchivo.indexOf("_C"));
                    }

                    String sqlQuery = " ";
                    //if (!archivosValue.getTipoCarga().equals("E")) {
                        sqlQuery =
                            "select count(1) " + 
                            "  from sup_calendario_detalle d " +
                            " INNER JOIN sup_calendario_if f " + 
                            "    on f.cal_consecutivo = d.cal_consecutivo " +
                            "   and f.cal_estatus = 'A' " + 
                            "   AND F.IC_CVE_SIAG = D.IC_CVE_SIAG " +
                            "  AND f.CAL_MES = " + archivosValue.getMes() + 
                            "  where d.cal_rel_garantias = '" + nombreArchivo + "'";
                    /*
                    } else {
                        sqlQuery =
                            "select count(*)\n" + "  from sup_calendario_if d\n" + " where d.CAL_NO_AUTORIZA = '" +
                            nombreArchivo + "'" + "   and d.cal_estatus = 'A'\n" + "  and   d.CAL_MES = " +
                            archivosValue.getMes();
                    }
                    */

                    LOG.debug("sqlQuery:"+sqlQuery);
                    rs = conn.queryDB(sqlQuery);
                    int validaGar = 0;
                    while (rs.next()) {
                        validaGar = rs.getInt(1);
                    }

                    if (validaGar == 0) {
                        validaRegreso.put("validaRegreso", 1);
                        break;
                    }

                    if (gartemp.indexOf(nombreArchivoT + "|") > -1) {

                        validaRegreso.put("validaRegreso", 3);
                        break;
                    }


                }

            } else {
                validaRegreso.put("validaRegreso", 2);
            }

        } catch (Exception e) {
            LOG.error("SupervisionBean::ValidaArchivos(Exception) "+e);
            ok = false;
            throw new NafinException("SIST0001");
        } finally {
            LOG.info("SupervisionBean::ValidaArchivos(S)");
        }

        return validaRegreso;
    }


    //DEVUELVE UN VECTOR PARA MOSTRAR LA CONSULTA DE LOS DATOS INSERTADOS
    // EN LAS TABLAS PERMANENTES
    public Vector getDoctoPreInsertados(String mes, String anio, String tipoCarga,
                                        String usuario) throws NafinException {
        LOG.info("SupervisionBean::getDoctoPreInsertados(E)");
        boolean resultado = true;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String qrySentencia = "";
        Vector vecRenglones = new Vector();
        Vector vecColumnas = new Vector();

        try {
            con.conexionDB();
            qrySentencia =
                "SELECT F.ARCT_MES_PAGO ARCT_MES_PAGO," + 
                "       F.ARCT_ANIO_PAGO ARCT_ANIO_PAGO," +
                "       DECODE(F.ARCT_TIPO_CARGA,'P', 'Carga Parcial', " + 
                "             'F', 'Carga Final'," +
                "             'A', 'Carga Aclaraciones', " +
                "             'E', 'Carga Extemporanea', " +
                "             'S', 'Semestral') ARCT_TIPO_CARGA," +
                "       F.ARCT_NO_ARCHIVOS ARCT_NO_ARCHIVOS," + 
                "       F.ARCT_NOMBRE ARCT_NOMBRE," +
                "       F.ARCT_EXTENSION ARCT_EXTENSION," + 
                "       F.REL_GARANTIAS REL_GARANTIAS" +
                "  FROM SUP_ARCHIVOS_TEMP_IF F" + 
                "  WHERE F.ARCT_ESTATUS = 'P' " + 
                "  AND ARCT_MES_PAGO = " + mes +
                "  AND ARCT_ANIO_PAGO = " + anio + 
                "  AND ARCT_TIPO_CARGA = '" + tipoCarga + "' " +
                "  AND IC_USUARIO = '" + usuario + "' "; //F032-2014
            LOG.info("qrySentencia" + qrySentencia);
            rs = con.queryDB(qrySentencia);            
            while (rs.next()) {
                vecColumnas = new Vector();
                /*0*/vecColumnas.addElement((rs.getString("ARCT_MES_PAGO") == null) ? "" :
                                            rs.getString("ARCT_MES_PAGO")); // 0
                /*1*/vecColumnas.addElement((rs.getString("ARCT_ANIO_PAGO") == null) ? "" :
                                            rs.getString("ARCT_ANIO_PAGO")); // 1 nombre de la pyme
                /*2*/vecColumnas.addElement((rs.getString("ARCT_TIPO_CARGA") == null) ? "" :
                                            rs.getString("ARCT_TIPO_CARGA")); // 2
                /*3*/vecColumnas.addElement((rs.getString("ARCT_NO_ARCHIVOS") == null) ? "" :
                                            rs.getString("ARCT_NO_ARCHIVOS")); // 3
                /*4*/vecColumnas.addElement((rs.getString("ARCT_NOMBRE") == null) ? "" :
                                            rs.getString("ARCT_NOMBRE")); // 4
                /*5*/vecColumnas.addElement((rs.getString("ARCT_EXTENSION") == null) ? "" :
                                            rs.getString("ARCT_EXTENSION")); // 5 nombre de la moneda
                /*6*/vecColumnas.addElement((rs.getString("REL_GARANTIAS") == null) ? "" :
                                            rs.getString("REL_GARANTIAS")); // 5 nombre de la moneda
                vecRenglones.addElement(vecColumnas);
            } //while
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::getDoctoPreInsertados(Exception) " + e);
            resultado = false;
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::getDoctoPreInsertados(S)");
        }
        return vecRenglones;
    }

    /**
     * Metodo para la confirmacion de carga de expedientes
     * @param mes
     * @param anio
     * @param tipoCarga
     * @param noArchivos
     * @param usuario
     * @param relacionGar
     * @param acuse
     * @param path
     * @param interFinan
     * @return
     * @throws NafinException
     */
    public boolean confirmaCarga(String mes, String anio, String tipoCarga, String noArchivos, String usuario,
                                 String relacionGar, String acuse, String path,
                                 String interFinan) throws NafinException {

        LOG.info("SupervisionBean::confirmaCarga (E)");
        AccesoDB con = null;
        String qrySentencia = "";
        boolean ok = true;
        String qrySentencia2 = "";
        //Integer     arcCons = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = new AccesoDB();
            con.conexionDB();
            creaAcuse(acuse, usuario, noArchivos, tipoCarga, mes, anio, relacionGar, interFinan, con);

            /* qrySentencia = "select nvl(MAX(ARC_CONSEC),0)+1 \n" +
				"from SUP_ARCHIVOS_CARGA_IF\n" +
				"where CC_ACUSE ='"+acuse+"'";
				rs = con.queryDB(qrySentencia);
				if(rs.next())
				arcCons = rs.getInt(1);
				con.cierraStatement();
				*/
            qrySentencia =
                " INSERT INTO sup_archivos_carga_if ( " + "     arc_mes_pago, " + "     arc_anio_pago, " +
                "     arc_tipo_carga, " + "     arc_no_archivos, " + "     arc_nombre, " + "     arc_extension, " +
                "     ic_usuario, " + "     cc_acuse, " + "     arc_consec, " + "     ic_if, " +
                "     rel_garantias, " + "     ic_cve_siag " + " ) SELECT tmp.arct_mes_pago, " +
                "        tmp.arct_anio_pago, " + "        tmp.arct_tipo_carga, " + "        " + noArchivos + ", " +
                "        tmp.arct_nombre, " + "        tmp.arct_extension, " + "        tmp.ic_usuario, " +
                "        '" + acuse + "', " + "        ROWNUM, " + "        tmp.ic_if, " +
                "        tmp.rel_garantias, " + "        dat.ic_cve_siag " + " FROM sup_archivos_temp_if tmp, " +
                "      sup_datos_if dat " + " WHERE tmp.ic_if = dat.ic_if " +
                "     AND tmp.ic_usuario = dat.ic_cliente " + "     AND tmp.ic_usuario = '" + usuario + "' " +
                "     AND tmp.arct_mes_pago = " + mes + " " + "     AND tmp.arct_anio_pago = " + anio + " " +
                "     AND tmp.arct_tipo_carga = '" + tipoCarga + "' " + "     AND tmp.arct_estatus = 'P' ";
            /*
				" INSERT INTO sup_archivos_carga_if\n" +
				"  (arc_mes_pago,\n" +
				"   Arc_Anio_Pago,\n" +
				"   arc_tipo_carga,\n" +
				"   arc_no_archivos,\n" +
				"   arc_nombre,\n" +
				"   arc_extension," +
				"   IC_USUARIO," +
				"   CC_ACUSE," +
				"   ARC_CONSEC," +
				"   IC_IF)\n" +
				"  (SELECT arct_mes_pago,\n" +
				"          arct_anio_pago,\n" +
				"          arct_tipo_carga,\n" +
				"          "+noArchivos+",\n" +
				"          arct_nombre,\n" +
				"          arct_extension,\n " +
				"          IC_USUARIO," +
				"          '"+acuse+"',\n" +
				"          rownum, \n" +
				"          IC_IF "+
				"     FROM sup_archivos_temp_if\n" +
				"    where arct_mes_pago ="+mes+"\n" +
				"      and arct_anio_pago ="+anio+"\n" +
				"      and arct_tipo_carga ='"+tipoCarga+"' " +
				"      and ARCT_ESTATUS = 'P')";
				*/
            con.ejecutaSQL(qrySentencia);

            qrySentencia2 =
                " update sup_archivos_temp_if    " + " set ARCT_ESTATUS = 'C' " + "  WHERE  arct_mes_pago =" + mes +
                "\n" + "  and arct_anio_pago =" + anio + "\n" + "  and arct_tipo_carga ='" + tipoCarga + "'\n" +
                "  and ic_usuario ='" + usuario + "'\n" + "  and ARCT_ESTATUS = 'P' ";

            ps = con.queryPrecompilado(qrySentencia2);
            ps.executeUpdate();
            ps.close();
            
            adecuarBitacoraSupervision(mes, anio, interFinan, con);
        
            ok = guardaArchivo(acuse, path, con);

        } catch (Exception e) {
            LOG.error("SupervisionBean::confirmaCarga(Exception) " + e);
            ok = false;
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(ok);
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::confirmaCarga (S)");
        }
        return ok;
    }


    private void creaAcuse(String acuse, String usuario, String noArchivos, String tipoCarga, String mes, String anio,
                           String relacionGar, String interFinan, AccesoDB con) throws NafinException {
        LOG.info("SupervisionBean::creaAcuse (E)");
        String qrySentencia = "";
        boolean ok = true;
        try {
            qrySentencia =
                " insert into SUP_ACUSE" + "(cc_acuse,IC_USUARIO,AC_MES_PAGO,AC_ANIO_PAGO" +
                " ,AC_NO_ARCHIVOS, AC_TIPO_CARGA, AC_FECHA_ACUSE, IC_IF)" + "values('" + acuse + "','" + usuario +
                "'," + mes + "," + anio + " ," + noArchivos + ", '" + tipoCarga + "', sysdate," + interFinan + ")";
            con.ejecutaSQL(qrySentencia);
        } catch (Exception e) {
            ok = false;
            throw new NafinException("SIST0001");
        }
        LOG.info("SupervisionBean::creaAcuse (S)");
    }

    private void creaAcuseCal(String acuse, String usuario, String noArchivos, String tipoCarga, String mes,
                              String anio, AccesoDB con) throws NafinException {
        LOG.info("SupervisionBean::creaAcuse (E)");
        String qrySentencia = "";
        boolean ok = true;
        try {
            qrySentencia =
                " insert into SUP_ACUSE_CAL" + "(cc_acuse,IC_USUARIO,ACC_MES_PAGO,ACC_ANIO_PAGO" +
                " ,ACC_NO_ARCHIVOS, ACC_TIPO_CARGA, ACC_FECHA_ACUSE)" + "values('" + acuse + "','" + usuario + "'," +
                mes + "," + anio + " ," + noArchivos + ", '" + tipoCarga + "', sysdate)";
            con.ejecutaSQL(qrySentencia);
        } catch (Exception e) {
            ok = false;
            throw new NafinException("SIST0001");
        }
        LOG.info("SupervisionBean::creaAcuse (S)");
    }

    public List getCatalogoAnioI() {
        List catalogoAnioI = new ArrayList();
        Calendar cal = Calendar.getInstance();
        int iAnioActual = cal.get(Calendar.YEAR);

        for (int anio = 2017; anio <= iAnioActual; anio++) {
            ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
            elementoCatalogo.setClave(String.valueOf(anio));
            elementoCatalogo.setDescripcion(String.valueOf(anio));
            catalogoAnioI.add(elementoCatalogo);
        }

        return catalogoAnioI;
    }


    /**
     * Metodo modificado para que la consulta no incluya la tabla sup_archivos_temp_if.
     * Si se quiere la consulta original, solo se debe descomentar
     * @param interFin
     * @param calMes
     * @param calAnio
     * @return
     * @throws NafinException
     */
    public Vector getConsultaArchivosR(String interFin, String calMes, String calAnio) throws NafinException {
        LOG.info("SupervisionBean::getConsultaArchivosR (E)");
        boolean resultado = true;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String qrySentencia = "";
        Vector vecRenglones = new Vector();
        Vector vecColumnas = new Vector();
        try {
            con.conexionDB();
            //Consulta original
            /*qrySentencia=   "select distinct c.cal_mes || ' - ' || c.cal_anio MES_ANIO,\n" +
			"       DECODE(te.arct_tipo_carga,\n" +
			"              'P',\n" +
			"              'Carga Parcial',\n" +
			"              'F',\n" +
			"              'Carga Final',\n" +
			"              'A',\n" +
			"              'Carga Aclaraciones',\n" +
			"              'E',\n" +
			"              'Carga Extemporanea') ARCT_TIPO_CARGA,\n" +
			"       ac.cc_acuse ACUSE,\n" +
			"       A.ARC_NO_ARCHIVOS NO_ARCHIVOS,\n" +
			"       TO_CHAR(ac.ac_fecha_acuse, 'DD/MM/YYYY HH24:MI:SS') FECHA_ACUSE,\n" +
			"       TO_CHAR(c.cal_fec_lim_ent_e, 'DD/MM/YYYY HH24:MI:SS') FECHA_LIMITE,\n" +
			"       i.ic_cve_siag IC_IF," +
			"       c.cal_mes MES," +
			"       c.cal_anio ANIO \n" +
			"  from sup_datos_if i\n" +
			"  inner join sup_calendario_if c\n" +
			"    on c.ic_cve_siag = i.ic_cve_siag\n" +
			"  left join sup_archivos_temp_if te\n" +
			"    on te.arct_mes_pago = c.cal_mes\n" +
			"   and te.arct_anio_pago = c.cal_anio\n" +
			//"   and te.arct_tipo_carga = c.cal_tipo\n" +
			"   and te.arct_estatus = 'C'\n" +
			"  left join sup_archivos_carga_if a\n" +
			"    on a.arc_mes_pago = te.arct_mes_pago\n" +
			"   and a.arc_anio_pago = te.arct_anio_pago\n" +
			"   and a.arc_tipo_carga = te.arct_tipo_carga\n" +
			"  left join sup_acuse ac\n" +
			"    on ac.ic_usuario = te.ic_usuario\n" +
			"   and ac.ac_mes_pago = te.arct_mes_pago\n" +
			"   and ac.ac_anio_pago = te.arct_anio_pago\n" +
			"   and ac.ac_tipo_carga = te.arct_tipo_carga" +
			"   and ac.cc_acuse = a.cc_acuse "+
			" WHERE " +
			"   CAL_MES = "+CAL_MES+
			"   AND CAL_ANIO = "+CAL_ANIO+
			"   AND CAL_ESTATUS = 'A'" +
			"   AND  i.ic_if ="+interFin+
			" ORDER BY ac.cc_acuse";*/

            //Consulta modificada
            qrySentencia =
                "SELECT DISTINCT C.CAL_MES || ' - ' || C.CAL_ANIO MES_ANIO \n" + ",DECODE(A.ARC_TIPO_CARGA, \n" +
                "              'P', \n" + "              'Carga Parcial', \n" + "              'F', \n" +
                "              'Carga Final', \n" + "              'A', \n" + "              'Carga Aclaraciones', \n" +
                "              'E', \n" + "              'Carga Extemporanea') ARCT_TIPO_CARGA \n" +
                ",A.CC_ACUSE ACUSE \n" + ",A.ARC_NO_ARCHIVOS NO_ARCHIVOS \n" +
                ",TO_CHAR(B.AC_FECHA_ACUSE, 'DD/MM/YYYY HH24:MI:SS') FECHA_ACUSE \n" +
                ",TO_CHAR(C.CAL_FEC_LIM_ENT_E, 'DD/MM/YYYY HH24:MI:SS') FECHA_LIMITE \n" + ",A.IC_CVE_SIAG IC_IF \n" +
                ",A.ARC_MES_PAGO MES \n" + ",A.ARC_ANIO_PAGO ANIO \n" + "FROM SUP_ARCHIVOS_CARGA_IF A \n" +
                ",SUP_ACUSE B \n" + ",SUP_CALENDARIO_IF C \n" + "WHERE A.CC_ACUSE = B.CC_ACUSE \n" +
                "AND A.IC_CVE_SIAG = C.IC_CVE_SIAG \n" + "AND A.ARC_MES_PAGO = C.CAL_MES \n" +
                "AND A.ARC_ANIO_PAGO = C.CAL_ANIO \n" + "AND C.CAL_ESTATUS ='A' \n" + "AND C.CAL_MES = " + calMes +
                " \n" + "AND C.CAL_ANIO = " + calAnio + " \n" + "AND A.IC_CVE_SIAG = " + interFin + " \n" +
                "ORDER BY A.CC_ACUSE";

            rs = con.queryDB(qrySentencia);
            LOG.info("qrySentencia" + qrySentencia);
            while (rs.next()) {
                vecColumnas = new Vector();
                vecColumnas.addElement((rs.getString("MES_ANIO") == null) ? "" : rs.getString("MES_ANIO"));
                vecColumnas.addElement((rs.getString("ARCT_TIPO_CARGA") == null) ? "" :
                                       rs.getString("ARCT_TIPO_CARGA"));
                vecColumnas.addElement((rs.getString("ACUSE") == null) ? "" : rs.getString("ACUSE"));
                vecColumnas.addElement((rs.getString("NO_ARCHIVOS") == null) ? "" : rs.getString("NO_ARCHIVOS"));
                vecColumnas.addElement((rs.getString("FECHA_ACUSE") == null) ? "" : rs.getString("FECHA_ACUSE"));
                vecColumnas.addElement((rs.getString("FECHA_LIMITE") == null) ? "" : rs.getString("FECHA_LIMITE"));
                vecColumnas.addElement((rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF"));
                vecColumnas.addElement((rs.getString("MES") == null) ? "" : rs.getString("MES"));
                vecColumnas.addElement((rs.getString("ANIO") == null) ? "" : rs.getString("ANIO"));
                vecRenglones.addElement(vecColumnas);
            }
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::getConsultaArchivosR Exception " + e);
            resultado = false;
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::getConsultaArchivosR (S)");
        }
        return vecRenglones;
    }

    /**
     * Metodo modificado para que la consulta no incluya la tabla sup_archivos_temp_if.
     * Si se quiere la consulta original, solo se debe descomentar
     * @param interFin
     * @param calMes
     * @param calAnio
     * @param acuse
     * @return
     * @throws NafinException
     */
    public Vector getObtenArchivosR(String interFin, String calMes, String calAnio, String acuse,
                                    String garantia) throws NafinException {
        LOG.info("SupervisionBean::getObtenArchivosR (E)");
        boolean resultado = true;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String qrySentencia = "";
        Vector vecRenglones = new Vector();
        Vector vecColumnas = new Vector();

        try {
            con.conexionDB();
            //Consulta original
            /*qrySentencia=   "select distinct a.ARC_NOMBRE  ARC_NOMBRE,\n" +
			"       te.REL_GARANTIAS,\n" +
			"       i.ic_cve_siag IC_IF," +
			"       c.cal_mes MES," +
			"       c.cal_anio ANIO \n" +
			"  from sup_datos_if i\n" +
			"  inner join sup_calendario_if c\n" +
			"    on c.ic_cve_siag = i.ic_cve_siag\n" +
			"  inner join sup_archivos_carga_if a\n" +
			"    on a.arc_mes_pago = c.cal_mes\n" +
			"   and a.arc_anio_pago = c.cal_anio\n" +
			"  inner join sup_archivos_temp_if te\n" +
			"    on te.arct_mes_pago = a.arc_mes_pago\n" +
			"   and te.arct_anio_pago = a.arc_anio_pago\n" +
			"   and a.arc_tipo_carga = te.arct_tipo_carga\n" +
			"   and a.arc_nombre = te.arct_nombre\n" +
			"   and te.arct_estatus = 'C' " +
			"  left join sup_acuse ac\n" +
			"    on ac.ic_usuario = te.ic_usuario\n" +
			"   and ac.ac_mes_pago = te.arct_mes_pago\n" +
			"   and ac.ac_anio_pago = te.arct_anio_pago\n" +
			"   and ac.ac_tipo_carga = te.arct_tipo_carga" +
			"   and ac.cc_acuse = a.cc_acuse "+
			" WHERE " +
			"   CAL_MES = "+CAL_MES+
			"   AND CAL_ANIO = "+CAL_ANIO+
			"   AND CAL_ESTATUS = 'A'" +
			"   AND i.ic_if ="+interFin+
			"   AND A.CC_ACUSE = '"+acuse+"' ";*/

            //Consulta modificada
            qrySentencia =
                "SELECT A.ARC_NOMBRE, \n" + "A.REL_GARANTIAS, \n" + "A.IC_CVE_SIAG IC_IF, \n" + "C.CAL_MES MES, \n" +
                "C.CAL_ANIO ANIO \n" + "FROM SUP_ARCHIVOS_CARGA_IF A \n" + ",SUP_ACUSE B \n" +
                ",SUP_CALENDARIO_IF C \n" + "WHERE A.CC_ACUSE = B.CC_ACUSE \n" +
                "AND A.IC_CVE_SIAG = C.IC_CVE_SIAG \n" + "AND A.ARC_MES_PAGO = C.CAL_MES \n" +
                "AND A.ARC_ANIO_PAGO = C.CAL_ANIO \n" + "AND C.CAL_ESTATUS ='A' \n" + "AND C.CAL_MES = " + calMes +
                " \n" + "AND C.CAL_ANIO = " + calAnio + " \n" + "AND A.IC_CVE_SIAG = " + interFin + " \n" +
                "AND B.CC_ACUSE = '" + acuse + "' \n";

            if (!garantia.equals(-1)) {
                qrySentencia += "AND A.REL_GARANTIAS LIKE'%" + garantia + "%'";
            }

            qrySentencia += "ORDER BY A.ARC_NOMBRE";

            rs = con.queryDB(qrySentencia);
            LOG.info("qrySentencia: " + qrySentencia);
            while (rs.next()) {
                vecColumnas = new Vector();
                vecColumnas.addElement((rs.getString("ARC_NOMBRE") == null) ? "" : rs.getString("ARC_NOMBRE"));
                vecColumnas.addElement((rs.getString("REL_GARANTIAS") == null) ? "" :
                                       rs.getString("REL_GARANTIAS")); // 1 nombre de la pyme
                vecColumnas.addElement((rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF")); // 3
                vecColumnas.addElement((rs.getString("MES") == null) ? "" : rs.getString("MES")); // 4
                vecColumnas.addElement((rs.getString("ANIO") == null) ? "" : rs.getString("ANIO"));
                vecRenglones.addElement(vecColumnas);
            }
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::getObtenArchivosR Exception " + e);
            resultado = false;
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::getObtenArchivosR (S)");
        }
        return vecRenglones;
    }


    public boolean guardaArchivo(String acuse, String path, AccesoDB conn) throws NafinException {
        LOG.info("SupervisionBean::guardaArchivo (E)");
        boolean resultadoG = false;
        ResultSet rs = null;
        String qrySentencia = "";
        String nombreArchivo = "";
        String extension = "";


        try {

            qrySentencia =
                "select arc_nombre, arc_extension\n" + "  from sup_archivos_carga_if\n" + " where cc_acuse =" + acuse;


            rs = conn.queryDB(qrySentencia);
            LOG.info("qrySentencia" + qrySentencia);
            String filePath = null;
            filePath = path + File.separator + DOCUMENTS_FOLDER;

            while (rs.next()) {
                nombreArchivo = rs.getString(1);
                extension = rs.getString(2);
                String base64 = this.encodeFileToBase64Binary(filePath + "/" + nombreArchivo);
                String identificador = acuse + "-" + nombreArchivo;
                URL url = this.getWSDireccion();
                Procesos procesos = new Procesos(url);
                ProcesosSoap procesosSoap = procesos.getProcesosSoap();
                Resultado resultado =
                    procesosSoap.ingresaArchivo("NESUPGAR", identificador, nombreArchivo, "A", base64);
                LOG.info(resultado.getStEstatus());
                LOG.info(resultado.getStError());
                if (resultado.getStEstatus().equals("OK")) {
                    File file = new File(filePath + "/" + nombreArchivo);
                    if (file.exists()) {
                        try {
                            file.delete();
                        } catch (SecurityException x) {
                            //logger.info("No se pudo eliminar el archivo "+f.getName());
                        }
                    }
                    resultadoG = true;
                } else {
                    resultadoG = false;
                    break;
                }


            } //while


            if (!resultadoG) {
                rs = conn.queryDB(qrySentencia);
                while (rs.next()) {
                    nombreArchivo = rs.getString(1);
                    extension = rs.getString(2);
                    String identificador = acuse + "-" + nombreArchivo;
                    URL url = this.getWSDireccion();
                    Procesos procesos = new Procesos(url);
                    ProcesosSoap procesosSoap = procesos.getProcesosSoap();
                    ResultadoBorrar resultado = procesosSoap.borrarArchivo("NESUPGAR", identificador, "");
                    LOG.info(resultado.getStEstatus());
                    LOG.info(resultado.getStError());
                } //while


            }


            rs.close();
        } catch (Exception e) {
            LOG.error("SupervisionBean::guardaArchivo Exception " + e);
            resultadoG = false;
            throw new NafinException("SIST0001");
        } finally {
            LOG.info("SupervisionBean::guardaArchivo (S)");
        }
        return resultadoG;
    }

    private String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        String base64 = DatatypeConverter.printBase64Binary(bytes);

        return base64;
    }


    private String encodeFileToBase64Binary2(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        //BASE64Encoder encoder = new BASE64Encoder();
        String base64 = "";

        Long tam = file.length();

        StringBuilder sb = new StringBuilder(tam.intValue());

        FileInputStream fin = null;
        try {
            fin = new FileInputStream(fileName);
            // Max size of buffer
            int bSize = 12 * 512;
            // Buffer
            byte[] buf = new byte[bSize];
            // Actual size of buffer
            int len = 0;

            while ((len = fin.read(buf)) != -1) {

                // Although you might want to write the encoded bytes to another
                // stream, otherwise you'll run into the same problem again.
                sb.append(new String(buf, 0, len));
            }
        } catch (IOException e) {
            if (null != fin) {
                fin.close();
            }
        }

        base64 = sb.toString();


        /* try {
                            LOG.info("eston son los caracteres: "+bytes.toString());
                            base64 = encoder.encode(bytes);
                        }catch(Exception e){
                                e.printStackTrace();
                                throw e;
                        }*/
        return base64;
    }


    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large

        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }

    public ArchivosCargaValue getArchivo(String nombreArchivo, String acuse) throws NafinException {

        ArchivosCargaValue archivosValue = new ArchivosCargaValue();
        byte[] buf = null;
        try {

            LOG.info("SupervisionBean::getArchivo (E)");
            String identificador = "";
            //Si el acuse es vacio o nulo, el identificador se forma solo por el nombre del archivo.
            //Esta condici�n es para descargar tambi�n los archivos de resultados y dictamen de supervisi�n.
            if (null == acuse || "".equals(acuse)) {
                identificador = nombreArchivo;
            } else {
                identificador = acuse + "-" + nombreArchivo;
            }
            LOG.info("ENTRO A CLASE ONBASE DE getArchivo ");
            URL url = this.getWSDireccion();
            Procesos procesos = new Procesos(url);
            ProcesosSoap procesosSoap = procesos.getProcesosSoap();
            ResultadoSalida resultado = procesosSoap.obtieneArchivo("NESUPGAR", identificador, "", "");
            LOG.info("SALIO DE CLASE ONBASE DE getArchivo ");
            buf = Base64.decodeBase64(resultado.getStArchivoBase64().getBytes(UTF_8));
            LOG.info("RESULTADO " + resultado.getStEstatus());
            LOG.info("RESULTADO ONBASE " + resultado.getStError());
            archivosValue.setARCHIVO_ESP(buf);

        } catch (Exception ex) {
            LOG.error("SupervisionBean::getArchivo Exception " + ex);
            throw new NafinException("SIST0001");
        } finally {
            LOG.info("SupervisionBean::getArchivo (S)");
        }
        return archivosValue;
    }


    /**
     * autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    public void avisosGeneradosPorNafin(String ruta) {
        LOG.info("SupervisionBean::avisosGeneradosPorNafin(E)");
        boolean ejecutar = true;
        while (ejecutar) {
            try {
                //obtengo el horario Final para ejecuci�n del proceso y su periodo de ejecuci�n
                String periodo = this.ejecutarProceso("PERIODO_EJECUCION");
                String horaFin = this.ejecutarProceso("FIN_PERIODO_EJECUCION");

                LOG.trace("periodo  " + periodo + "   horaFin  " + horaFin);

                if (!"".equals(periodo) && !"".equals(horaFin)) {

                    String horaServidor =
                        (new SimpleDateFormat("HH:mm")).format(new java.util.Date()); //hora del Sevidor en 24 horas

                    int periodoF = Integer.parseInt(periodo);

                    DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                    Date hrServdor;
                    Date hrFin;
                    hrServdor = dateFormat.parse(horaServidor); //hora del Sevidor en 24 horas
                    hrFin = dateFormat.parse(horaFin); //horario Final parametrizado
                    int resul = hrServdor.compareTo(hrFin);
                    
                    LOG.trace("ejecuta por periodo " + periodoF + "   Hora Servidor  " + hrServdor + "Hora Final  " + hrFin + "   resul " + resul);
                    
                    if (resul == 0 || resul == 1) {
                        LOG.debug("Termina ");
                        ejecutar = false;
                    } else if (resul == -1) {
                        procesosaEjecutar(ruta);
                        LOG.debug("Continua ");
                        Thread.sleep(1000 * 60 * periodoF);
                        continue; //Vuelve a evaluar la condici�n del while(ejecutar)
                    }
                } else {// se ejecuta solo una vez cuando no se tiene registos de la hora y tiempo de ejecuci�n
                    procesosaEjecutar(ruta);
                    ejecutar = false;
                }
            } catch (Exception t) {
                LOG.error("SupervisionBean::avisosGeneradosPorNafin(Exception) "+t);
                try {
                    Thread.sleep((1000 * 60) * 5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                t.printStackTrace();
                //throw new AppException("Error al enviar  avisosGeneradosPorNafin ", t);
            }
        }
        LOG.info("SupervisionBean::avisosGeneradosPorNafin(S)");
    }
    
    
    /**
     *  autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    private void procesosaEjecutar(String ruta) throws NafinException {
        //XXX
        try {
            //Calendario de Supervision
            if (this.hayRegParaProcesar("CS") > 0) {					
                this.avisoCalendarioSupervision(ruta);
            }

            //Carga masiva de Expedientes para la Supervisi�n
            if (this.hayRegParaProcesar("CME") > 0) {
                this.avisoCargaExpedientes(ruta);
            }

            //Reporte de Resultados de Supervisi�n
            if (this.hayRegParaProcesar("RRS") > 0) {
                this.avisoReporteResultadoSupervision(ruta);
            }

            // Dictamen de Supervisi�n.
            if (this.hayRegParaProcesar("DS") > 0) {
                this.avisoDictamenSupervision(ruta);
            }
            
            //Vencimiento de la entrega de expedientes de los IF
            if(this.hayRegParaProcesar("NVEX") > 0){
                this.alertaVencimientoEEIF(ruta);
            }
        } catch (Exception t) {
            throw new AppException("Error al enviar  avisosGeneradosPorNafin ", t);
        }


    }

    /**
     * metodo para verificar si hay registros para procesar
     * autor =  QC-DLHC
     * @param tipoNotifiacion
     * @return
     * @throws NafinException
     */
    private int hayRegParaProcesar(String tipoNotifiacion) throws NafinException {
        LOG.info("SupervisionBean::hayRegParaProcesar(E)");
        
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int total = 0;

        try {
            con.conexionDB();
            
            qry = new StringBuilder();
            qry.append(" Select COUNT    (*)   TOTAL  FROM SUP_CALENDARIO_IF    WHERE  CAL_ESTATUS = 'A' ");

            if ("CS".equals(tipoNotifiacion)) { //Calendario de Supervision
                qry.append("AND  noti_carga_calen  = ?  ");
            } else if ("CME".equals(tipoNotifiacion)) { //Carga masiva de Expedientes para la Supervisi�n
                qry.append("AND  noti_carga_exp = ?  ");
            } else if ("RRS".equals(tipoNotifiacion)) { //Reporte de Resultados de Supervisi�n
                qry.append("AND  noti_resul_sup = ?  ");
            } else if ("DS".equals(tipoNotifiacion)) { //Dictamen de Supervisi�n
                qry.append("AND noti_doc_dic_sup = ?  ");
            }else if("NVEX".equals(tipoNotifiacion)){//Notificaion de Vencimiento de Entrega de Expediente
                qry.append(" AND NOTI_VENC_ENTRE_EXP = ?");
            }

            lVarBind = new ArrayList();
            lVarBind.add("N");
            LOG.debug("qry  " + qry + "  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt("TOTAL");
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("SupervisionBean::hayRegParaProcesar(Exception) "+e);
            throw new AppException("Error hayRegParaProcesar   ");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::hayRegParaProcesar(S)");
        }
        return total;
    }

    /**
     * Calendario de Supervisi�n.
     * autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    private void avisoCalendarioSupervision(String ruta) throws NafinException {
        LOG.info("SupervisionBean::avisoCalendarioSupervision(E)");
        try {

            Correo correo = new Correo();
            String remitente = "no_response@nafin.gob.mx";
            String asunto = "Aviso de Calendario de Supervisi�n. ";
            StringBuilder msg = new StringBuilder();
            StringBuilder emailContactoIFCC = new StringBuilder();
            ArrayList info = new ArrayList();
            ArrayList contac = new ArrayList();
            ArrayList detalle = new ArrayList();
            ArrayList ejecutivos = new ArrayList();
            StringBuilder emailEjecutivoCC = new StringBuilder();

            info = this.listaIFsiag("C", ""); //Obtengo la lista de IF�s   con su clave de siag
            for (int i = 0; i < info.size(); i++) {

                HashMap datCE = (HashMap) info.get(i);
                String nombreIF = datCE.get("NOMBREIF").toString();
                String claveSiag = datCE.get("CVESIAG").toString();
                String claveIF = datCE.get("ICIF").toString();

                emailContactoIFCC = new StringBuilder();
                String emailContactoIF = "";
                String nombreContactoIF = "";
                String loginEjecutivo = "";
                String nombreEjecutivo = "";

                //Obtenego los Contacto del IF con el Banco
                contac = new ArrayList();
                contac = contactosyEjecutivos(claveSiag, "C", claveIF);
                for (int c = 0; c < contac.size(); c++) {

                    HashMap datcontac = (HashMap) contac.get(c);
                    String principal = datcontac.get("PRINCIPAL").toString();
                    if ("S".equals(principal)) {
                        nombreContactoIF = datcontac.get("NOMBRE").toString();
                        emailContactoIF = datcontac.get("EMAILS").toString();
                    } else if ("N".equals(principal)) {
                        emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                                 "").append(datcontac.get("EMAILS").toString());
                    }
                } // for (int c = 0; c <contac.size(); c++)

                //obtengo el Ejecutivo principal para insertar en bitacora
                ejecutivos = new ArrayList();
                ejecutivos = contactosyEjecutivos(claveSiag, "E", claveIF);
                emailEjecutivoCC = new StringBuilder();
                for (int e = 0; e < ejecutivos.size(); e++) {
                    HashMap datcontac = (HashMap) ejecutivos.get(e);
                    String principal = datcontac.get("PRINCIPAL").toString();
                    emailEjecutivoCC.append(emailEjecutivoCC.length() > 0 ? ", " :
                                            "").append(datcontac.get("EMAILS").toString());
                    if ("S".equals(principal)) {
                        loginEjecutivo = datcontac.get("LOGIN").toString();
                        nombreEjecutivo = datcontac.get("NOMBRE").toString();
                    }
                } // for (int c = 0; c <contac.size(); c++)

                //  se agregan a los correos de copia del banco los de Ejecutivos Nafin
                emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                         "").append(emailEjecutivoCC.toString());

                // se obtiene el detalle en la tabla de SUP_CALENDARIO_IF y SUP_CALENDARIO_DETALLE
                detalle = this.datosCalendarioSupervision(claveSiag, claveIF);

                if (detalle.size() > 0) {
                    for (int x = 0; x < detalle.size(); x++) {

                        HashMap dat = (HashMap) detalle.get(x);

                        String consecutivo = dat.get("CONSECUTIVO").toString();
                        String mesAnio = dat.get("MESANIO").toString();
                        String mes = dat.get("MES").toString();
                        String anio = dat.get("ANIO").toString();
                        String fecha = dat.get("FECHA").toString();
                        String numGarantias = dat.get("NUMGARANTIAS").toString();
                        String fEntregaExpediente = dat.get("FECHA_EXPEDIENTE").toString();
                        String fEntregaAclaraciones = dat.get("FECHA_ACLARACIONES").toString();
                        String fDictamen = dat.get("FECHA_DICTAMEN").toString();
                        String listadeEjecutivo = dat.get("EJECUTIVOS").toString();
                        String tipo = dat.get("TIPO").toString();

                        msg = new StringBuilder();
                        msg.append(" <table width=\"400px\" border=\"0\"> ");
                        msg.append("<tr><center><img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> </center></tr>");

                        msg.append(" <tr>");
                        msg.append("<br>   <b>" + nombreContactoIF +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha:" +
                                   fecha + " </b>");
                        msg.append("<br>   <b>" + nombreIF + "</b>");
                        
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET el Aviso de <br>");
                        if("S".equals(tipo)) {
                            msg.append(" Supervisi�n correspondiente al " + mesAnio);
                        } else {
                            msg.append(" Supervisi�n correspondiente al mes de " + mesAnio);
                        }
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append(" * N�mero de garant�as a supervisar:  " + numGarantias + "<br>   ");
                        msg.append(" * Fecha l�mite de entrega de expedientes:     " + fEntregaExpediente + "<br>   ");
                        if(!"S".equals(tipo)) {
                            msg.append(" * Fecha l�mite de entrega de aclaraciones:    " + fEntregaAclaraciones + "<br>   ");
                        }                        
                        msg.append(" * Fecha l�mite de dictamen:                   " + fDictamen + "<br>   ");
                        msg.append(" * Nombre del Ejecutivo:                       " + listadeEjecutivo + "<br>   ");
                        msg.append("<br>   ");
                        msg.append("Favor de ingresar a NAFINET m�dulo de Supervisi�n y Seguimiento de Garant�as para su consulta y atenci�n.");
                        msg.append(" <br> <b> en la siguiente link: https://cadenas.nafin.com.mx  </b>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("Nota de seguridad: Nacional Financiera, S.N.C. jam�s solicitara tu contrase�a por correo electr�nico");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<center><img src=\"cid:nafin_id2\" height=\"85\" width=\"475\" alt=\"\" border=\"0\"> </center>");
                        msg.append("</tr>");
                        msg.append("</table> ");

                        String rutaImagen = ruta + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif";
                        String rutaImagen2 = ruta + "00archivos/15cadenas/15archcadenas/logos/bg_footer.jpg";

                        ArrayList listaDeImagenes = new ArrayList();
                        HashMap imagenNafin = new HashMap();
                        imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen);
                        imagenNafin.put("FILE_ID", "nafin_id");
                        listaDeImagenes.add(imagenNafin);

                        imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen2);
                        imagenNafin.put("FILE_ID", "nafin_id2");

                        listaDeImagenes.add(imagenNafin);

                        try { //Si el env�o del correo falla se ignora.

                            //si no tiene el banco Ejecutivo Nafin no se activa la bandera de  Calendario de Supervision
                            if (!"".equals(loginEjecutivo)) {

                                correo.setCodificacion("charset=UTF-8");
                                correo.enviaCorreoConDatosAdjuntos(remitente, emailContactoIF,
                                                                   emailContactoIFCC.toString(), asunto, msg.toString(),
                                                                   listaDeImagenes, null);

                                Map<String, Object> act = new HashMap<>();
                                act.put("mes", mes);
                                act.put("anio", anio);
                                act.put("claveIF", claveIF);
                                act.put("consecutivo", consecutivo);
                                act.put("tipoNotifiacion", "CS");
                                act.put("claveSiag", claveSiag);

                                this.actualizaBandera(act);
                            }

                            Map<String, Object> bi = new HashMap<>();
                            bi.put("mes", mes);
                            bi.put("anio", anio);
                            bi.put("claveIF", claveIF);
                            bi.put("fechaPublicacion", fecha);
                            bi.put("fEntregaExpediente", fEntregaExpediente);
                            bi.put("fEntregaAclaraciones", fEntregaAclaraciones);
                            bi.put("loginEjecutivo", loginEjecutivo);
                            bi.put("nombreEjecutivo", nombreEjecutivo);
                            bi.put("tipo", tipo);

                            this.insertBitacora(bi);

                        } catch (Exception t) {
                            LOG.error("Error al enviar correo " + t);
                        }

                    } //for (int x = 0; x <detalle.size(); x++)
                }
            } //for (int i = 0; i <=info.size(); i++)
            
        } catch (Exception t) {
            LOG.error("SupervisionBean::avisoCalendarioSupervision(Exception) "+t);
            throw new AppException("Error al enviar  avisoCalendarioSupervision ", t);
        } finally {
            LOG.info("SupervisionBean::avisoCalendarioSupervision(S)");
        }
    }


    /**
     * Carga masiva de Expedientes para la Supervisi�n.
     * autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    private void avisoCargaExpedientes(String ruta) throws NafinException {
        LOG.info("avisoCargaExpedientes (e)");

        try {

            Correo correo = new Correo();

            String remitente = "no_response@nafin.gob.mx";
            String asunto = "Aviso de Carga masiva de Expedientes para la Supervisi�n.";
            StringBuilder msg = new StringBuilder();
            StringBuilder emailEjecutivoCC = new StringBuilder();
            ArrayList detalle = new ArrayList();
            ArrayList ejecutivos = new ArrayList();
            ArrayList info = new ArrayList();

            info = this.listaIFsiag("E", "Bitacora"); //Obtengo la lista de IF�s   con su clave de siag
            for (int i = 0; i < info.size(); i++) {

                HashMap datCE = (HashMap) info.get(i);
                String nombreIF = datCE.get("NOMBREIF").toString();
                String claveSiag = datCE.get("CVESIAG").toString();
                String claveIF = datCE.get("ICIF").toString();

                // se obtiene el detalle en la tabla de SUP_CALENDARIO_IF  Y sup_archivos_carga_if
                detalle = this.datosCargaExpedientes(claveSiag);

                if (detalle.size() > 0) {
                    for (int x = 0; x < detalle.size(); x++) {

                        HashMap dat = (HashMap) detalle.get(x);
                        String consecutivo = (String) dat.get("CONSECUTIVO");
                        String mesAnio = (String) dat.get("MESANIO");
                        String fecha = (String) dat.get("FECHA");
                        String numExpedientes = (String) dat.get("NUM_EXPEDIENTES");
                        String numFolioNafin = (String) dat.get("NUMFOLIO");
                        String tipoInformacion = (String) dat.get("TIPOINFORMACION");
                        String mes = (String) dat.get("MES");
                        String anio = (String) dat.get("ANIO");
                        String datGarantias = (String) dat.get("GARANTIAS");
                        String tipoCarga = (String) dat.get("TIPOCARGA");
                        String fechaAcuse = (String)dat.get("FECHACUSE");

                        //obtengo el Ejecutivo para el envio de correo con copia
                        ejecutivos = new ArrayList();
                        ejecutivos = contactosyEjecutivos(claveSiag, "E", claveIF);
                        emailEjecutivoCC = new StringBuilder();

                        for (int e = 0; e < ejecutivos.size(); e++) {
                            HashMap datcontac = (HashMap) ejecutivos.get(e);
                            String principal = datcontac.get("PRINCIPAL").toString();
                            if ("N".equals(principal)) {
                                emailEjecutivoCC.append(emailEjecutivoCC.length() > 0 ? ", " :
                                                        "").append(datcontac.get("EMAILS").toString());
                            }
                        } // for (int c = 0; c <contac.size(); c++)


                        //datos de la bitacora para obtener el ejecutivo
                        Map<String, Object> datejec = new HashMap<>();
                        datejec = this.ejecutivoNafinBita(claveIF, mes, anio);
                        String nombreEjecutivo = (String) datejec.get("NOMBRE_EJECUTIVO");
                        String emailEjecutivo = (String) datejec.get("EMAILS");
                        String fechaCargaExpediente = (String) datejec.get("FechaAcuse");


                        msg = new StringBuilder();
                        msg.append(" <table width=\"600px\" border=\"0\"> <tr>");

                        msg.append("<center><img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> </center>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<br>   <b>" + nombreEjecutivo +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha:" +
                                   fechaAcuse + " </b>");
                        msg.append("<br>   <b>Nacional Financiera S.N.C. </b>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        if ("S".equals(tipoCarga)) {
                            msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET los <br>" +
                                       " expedientes de supervisi�n correspondiente al " + mesAnio);
                        } else if (!"A".equals(tipoCarga)) {
                            msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET los <br>" +
                                       " expedientes de supervisi�n correspondiente al mes de " + mesAnio);
                        } else if ("A".equals(tipoCarga)) {
                            msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET los <br>" +
                                       " expedientes para las aclaraciones correspondiente al mes de " + mesAnio);
                        }
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append(" * Fecha de carga de expediente: " + fechaAcuse + "<br>   ");
                        msg.append(" * N�mero de expedientes transmitidos:  " + numExpedientes + "<br>   ");
                        msg.append(" * N�mero de Folio Nafin:     " + numFolioNafin + "<br>   ");
                        msg.append(" * Tipo de Informaci�n:    " + tipoInformacion + "<br>   ");
                        msg.append(" * Garant�a:                   " + datGarantias + "<br>   ");

                        msg.append("<br>   ");
                        msg.append("Favor de ingresar a NAFINET m�dulo de Supervisi�n y Seguimiento de Garant�as para su consulta y atenci�n.");
                        msg.append("<br>   ");
                        msg.append("<br>   ");

                        msg.append("<br> <center>  <b>" + nombreIF + "</b></center>");
                        msg.append("<br>   ");
                        msg.append("Nota de seguridad: Nacional Financiera, S.N.C. jam�s solicitara tu contrase�a por correo electr�nico");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
							   msg.append("<center><img src=\"cid:nafin_id2\" height=\"85\" width=\"475\" alt=\"\" border=\"0\"> </center>");
                        msg.append("</tr>    </table> ");

                        String rutaImagen = ruta + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif";

                        ArrayList listaDeImagenes = new ArrayList();
                        HashMap imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen);
                        imagenNafin.put("FILE_ID", "nafin_id");
                        listaDeImagenes.add(imagenNafin);

                        try { //Si el env�o del correo falla se ignora.
                            correo.setCodificacion("charset=UTF-8");
                            correo.enviaCorreoConDatosAdjuntos(remitente, emailEjecutivo, emailEjecutivoCC.toString(),
                                                               asunto, msg.toString(), listaDeImagenes, null);


                            Map<String, Object> act = new HashMap<>();
                            act.put("mes", mes);
                            act.put("anio", anio);
                            act.put("claveIF", claveIF);
                            act.put("consecutivo", consecutivo);
                            act.put("tipoNotifiacion", "CME");
                            act.put("claveSiag", claveSiag);

                            this.actualizaBandera(act);

                        } catch (Exception t) {
                            LOG.error("Error al enviar correo " + t);
                        }

                    } //for (int x = 0; x <detalle.size(); x++)
                }

            } //for (int i = 0; i <info.size(); i++)


        } catch (Exception t) {
            throw new AppException("Error al enviar  avisoCargaExpedientes ", t);
        } finally {
            LOG.info("avisoCargaExpedientes (s)");
        }

    }


    /**
     * Reporte de Resultados de Supervisi�n.
     * autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    private void avisoReporteResultadoSupervision(String ruta) throws NafinException {
        LOG.info("avisoReporteResultadoSupervision (e)");

        try {

            Correo correo = new Correo();

            String remitente = "no_response@nafin.gob.mx";
            String asunto = "Aviso de Reporte de Resultados de Supervisi�n.";
            StringBuilder msg = new StringBuilder();
            StringBuilder emailContactoIFCC = new StringBuilder();
            ArrayList detalle = new ArrayList();
            ArrayList contactos = new ArrayList();
            ArrayList info = new ArrayList();
            ArrayList ejecutivos = new ArrayList();
            StringBuilder emailEjecutivoCC = new StringBuilder();


            info = this.listaIFsiag("C", "ResulSupervision"); //Obtengo la lista de IF�s   con su clave de siag
            for (int i = 0; i < info.size(); i++) {

                HashMap datCE = (HashMap) info.get(i);
                String nombreIF = datCE.get("NOMBREIF").toString();
                String claveSiag = datCE.get("CVESIAG").toString();
                String claveIF = datCE.get("ICIF").toString();

                // se obtiene el detalle en la tabla de SUP_CALENDARIO_IF
                detalle = this.datosResultadoSupervision(claveSiag);

                if (detalle.size() > 0) {
                    for (int x = 0; x < detalle.size(); x++) {

                        HashMap dat = (HashMap) detalle.get(x);
                        String consecutivo = (String) dat.get("CONSECUTIVO");
                        String mesAnio = (String) dat.get("MESANIO");
                        String mes = (String) dat.get("MES");
                        String anio = (String) dat.get("ANIO");
                        String fechaEntrega = (String) dat.get("FECHA_ENTEGA_ACLA");

                        //obtengo el Ejecutivo Intermediario para el envio de correo con copia
                        String nombreContactoIF = "";
                        String emailContactoIF = "";
                        emailContactoIFCC = new StringBuilder();
                        emailEjecutivoCC = new StringBuilder();

                        contactos = new ArrayList();
                        contactos = contactosyEjecutivos(claveSiag, "C", claveIF);

                        for (int e = 0; e < contactos.size(); e++) {
                            HashMap datcontac = (HashMap) contactos.get(e);
                            String principal = datcontac.get("PRINCIPAL").toString();
                            if ("S".equals(principal)) {
                                nombreContactoIF = datcontac.get("NOMBRE").toString();
                                emailContactoIF = datcontac.get("EMAILS").toString();
                            } else if ("N".equals(principal)) {
                                emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                                         "").append(datcontac.get("EMAILS").toString());
                            }
                        } // for (int c = 0; c <contac.size(); c++)

                        // Se obtienen los ejecutivos de Nafin
                        ejecutivos = new ArrayList();
                        ejecutivos = contactosyEjecutivos(claveSiag, "E", claveIF);
                        for (int e = 0; e < ejecutivos.size(); e++) {
                            HashMap datcontac = (HashMap) ejecutivos.get(e);
                            emailEjecutivoCC.append(emailEjecutivoCC.length() > 0 ? ", " :
                                                    "").append(datcontac.get("EMAILS").toString());
                        } // for (int c = 0; c <contac.size(); c++)


                        //  se agregan a los correos de copia del banco los de Ejecutivos Nafin
                        emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                                 "").append(emailEjecutivoCC.toString());

                        //datos de la bitacora para obtener el ejecutivo
                        Map<String, Object> datejec = new HashMap<>();
                        datejec = this.ejecutivoNafinBita(claveIF, mes, anio);
                        String nombreEjecutivoNafin = (String) datejec.get("NOMBRE_EJECUTIVO");
                        String fecha = (String) datejec.get("FECHA_RESP_SUP");

                        msg = new StringBuilder();
                        msg.append(" <table width=\"400px\" border=\"0\"> <tr>");
                        msg.append("<center><img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> </center>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<br>   <b>" + nombreContactoIF +
                                   " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha:" +
                                   fecha + " </b>");
                        msg.append("<br>   <b>" + nombreIF + " </b>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET los resultados de la  <br>" +
                                   " supervisi�n correspondiente al mes de " + mesAnio);
                        msg.append("<br>   ");
                        msg.append("<br>   * Fecha L�mite de Entrega de Aclaraciones: " + fechaEntrega + "");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("Favor de ingresar a NAFINET m�dulo de Supervisi�n y Seguimiento de Garant�as para su consulta y atenci�n.");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<br>    Atentamente ");
                        msg.append("<br>  " + nombreEjecutivoNafin + "</center>");
                        msg.append("<br>   ");
                        msg.append("Nota de seguridad: Nacional Financiera, S.N.C. jam�s solicitara tu contrase�a por correo electr�nico");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<center><img src=\"cid:nafin_id2\" height=\"85\" width=\"475\" alt=\"\" border=\"0\"> </center>");
                        msg.append("</tr>    </table> ");

                        String rutaImagen = ruta + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif";
                        String rutaImagen2 = ruta + "00archivos/15cadenas/15archcadenas/logos/bg_footer.jpg";

                        ArrayList listaDeImagenes = new ArrayList();
                        HashMap imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen);
                        imagenNafin.put("FILE_ID", "nafin_id");
                        listaDeImagenes.add(imagenNafin);
                        imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen2);
                        imagenNafin.put("FILE_ID", "nafin_id2");

                        listaDeImagenes.add(imagenNafin);

                        try { //Si el env�o del correo falla se ignora.
                            correo.setCodificacion("charset=UTF-8");
                            correo.enviaCorreoConDatosAdjuntos(remitente, emailContactoIF, emailContactoIFCC.toString(),
                                                               asunto, msg.toString(), listaDeImagenes, null);


                            Map<String, Object> act = new HashMap<>();
                            act.put("mes", mes);
                            act.put("anio", anio);
                            act.put("claveIF", claveIF);
                            act.put("consecutivo", consecutivo);
                            act.put("tipoNotifiacion", "RRS");
                            act.put("claveSiag", claveSiag);

                            this.actualizaBandera(act);

                        } catch (Exception t) {
                            LOG.error("Error al enviar correo " + t);
                        }

                    } //for (int x = 0; x <detalle.size(); x++)
                }

            } //for (int i = 0; i <info.size(); i++)


        } catch (Exception t) {
            throw new AppException("Error al enviar  avisoCargaExpedientes ", t);
        } finally {
            LOG.info("avisoReporteResultadoSupervision (s)");
        }
    }


    /**
     * Dictamen de Supervisi�n
     * autor =  QC-DLHC
     * @param ruta
     * @throws NafinException
     */
    private void avisoDictamenSupervision(String ruta) throws NafinException {
        LOG.info("avisoDictamenSupervision (e)");

        try {
            Correo correo = new Correo();

            String remitente = "no_response@nafin.gob.mx";
            String asunto = " Aviso de  Dictamen de Supervisi�n ";
            StringBuilder msg = new StringBuilder();
            StringBuilder emailContactoIFCC = new StringBuilder();
            ArrayList info = new ArrayList();
            ArrayList contac = new ArrayList();
            ArrayList detalle = new ArrayList();
            ArrayList ejecutivos = new ArrayList();
            StringBuilder emailEjecutivoCC = new StringBuilder();

            info = this.listaIFsiag("C", "Dictamente"); //Obtengo la lista de IF�s   con su clave de siag
            for (int i = 0; i < info.size(); i++) {

                HashMap datCE = (HashMap) info.get(i);
                String nombreIF = datCE.get("NOMBREIF").toString();
                String claveSiag = datCE.get("CVESIAG").toString();
                String claveIF = datCE.get("ICIF").toString();

                String nombreContactoIF = "";
                String emailContactoIF = "";
                emailContactoIFCC = new StringBuilder();
                emailEjecutivoCC = new StringBuilder();

                //Obtenego los Contacto del IF con el Banco
                contac = new ArrayList();
                contac = contactosyEjecutivos(claveSiag, "C", claveIF);
                for (int c = 0; c < contac.size(); c++) {
                    HashMap datcontac = (HashMap) contac.get(c);
                    String principal = datcontac.get("PRINCIPAL").toString();
                    if ("S".equals(principal)) {
                        nombreContactoIF = datcontac.get("NOMBRE").toString();
                        emailContactoIF = datcontac.get("EMAILS").toString();
                    } else if ("N".equals(principal)) {
                        emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                                 "").append(datcontac.get("EMAILS").toString());
                    }
                } // for (int c = 0; c <contac.size(); c++)

                //Obtenego los correos de Contacto de Nafin
                ejecutivos = new ArrayList();
                ejecutivos = contactosyEjecutivos(claveSiag, "E", claveIF);
                for (int c = 0; c < ejecutivos.size(); c++) {
                    HashMap datcontac = (HashMap) ejecutivos.get(c);
                    emailEjecutivoCC.append(emailEjecutivoCC.length() > 0 ? ", " :
                                            "").append(datcontac.get("EMAILS").toString());

                } // for (int c = 0; c <contac.size(); c++)

                //  se agregan a los correos de copia del banco los de Ejecutivos Nafin
                emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                         "").append(emailEjecutivoCC.toString());

                // se obtiene el detalle en la tabla de SUP_CALENDARIO_IF
                detalle = new ArrayList();
                detalle = this.datosDictamenSupervision(claveSiag);
                if (detalle.size() > 0) {
                    for (int x = 0; x < detalle.size(); x++) {

                        HashMap dat = (HashMap) detalle.get(x);

                        String consecutivo = dat.get("CONSECUTIVO").toString();
                        String mesAnio = dat.get("MESANIO").toString();
                        String mes = dat.get("MES").toString();
                        String anio = dat.get("ANIO").toString();
                        String tipo = dat.get("TIPO").toString();

                        //datos de la bitacora para obtener el ejecutivo
                        Map<String, Object> datejec = new HashMap<>();
                        datejec = this.ejecutivoNafinBita(claveIF, mes, anio);
                        String nombreEjecutivoNafin = (String) datejec.get("NOMBRE_EJECUTIVO");
                        String fecha = (String) datejec.get("FECHA_PUBLIC_DIC");


                        msg = new StringBuilder();
                        msg.append("<center><img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> </center>");
                        msg.append("<br>   <b>" + nombreContactoIF +
                                   " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha:" +
                                   fecha + " </b>");
                        msg.append("<br>   <b>" + nombreIF + " </b>");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append(" Por medio de este Conducto le informamos que se encuentra disponible en NAFINET el  ");
                        if("S".equals(tipo)) {
                            msg.append(" Dictamen Final correspondiente al " + mesAnio);
                        } else {
                            msg.append(" Dictamen Final correspondiente al mes de " + mesAnio);
                        }
                        msg.append("<br>   ");
                        msg.append("<br>   ");

                        msg.append("Favor de ingresar a NAFINET m�dulo de Supervisi�n y Seguimiento de Garant�as para su consulta.");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<br>Atentamente ");
                        msg.append("<br>" + nombreEjecutivoNafin + "<br>   ");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("Nota de seguridad: Nacional Financiera, S.N.C. jam�s solicitara tu contrase�a por correo electr�nico");
                        msg.append("<br>   ");
                        msg.append("<br>   ");
                        msg.append("<center><img src=\"cid:nafin_id2\" height=\"85\" width=\"475\" alt=\"\" border=\"0\"> </center>");
							   msg.append("</tr>");
                        msg.append("</table> ");

                        String rutaImagen = ruta + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif";
                        String rutaImagen2 = ruta + "00archivos/15cadenas/15archcadenas/logos/bg_footer.jpg";

                        ArrayList listaDeImagenes = new ArrayList();
                        HashMap imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen);
                        imagenNafin.put("FILE_ID", "nafin_id");
                        listaDeImagenes.add(imagenNafin);

                        imagenNafin = new HashMap();
                        imagenNafin.put("FILE_FULL_PATH", rutaImagen2);
                        imagenNafin.put("FILE_ID", "nafin_id2");

                        listaDeImagenes.add(imagenNafin);

                        try { //Si el env�o del correo falla se ignora.
                            correo.setCodificacion("charset=UTF-8");
                            correo.enviaCorreoConDatosAdjuntos(remitente, emailContactoIF, emailContactoIFCC.toString(),
                                                               asunto, msg.toString(), listaDeImagenes, null);

                            Map<String, Object> act = new HashMap<>();
                            act.put("mes", mes);
                            act.put("anio", anio);
                            act.put("claveIF", claveIF);
                            act.put("consecutivo", consecutivo);
                            act.put("tipoNotifiacion", "DS");
                            act.put("claveSiag", claveSiag);

                            this.actualizaBandera(act);

                        } catch (Exception t) {
                            LOG.error("Error al enviar correo " + t);
                        }
                    }
                } //for (int i = 0; i <=info.size(); i++)
            }

        } catch (Exception t) {
            throw new AppException("Error al enviar  avisoDictamenSupervision ", t);
        } finally {
            LOG.info("avisoDictamenSupervision (s) ");
        }
    }

    /**
     * metodo que obtiene lista de IF�s para notificarlos avisos nafin
     * autor =  QC-DLHC
     * @param tipoC  E = Ejecutivos Nafin  C = Contactos del Banco
     * @param bit   Bitacora
     * @return
     * @throws NafinException
     */
    private ArrayList listaIFsiag(String tipoC, String bit) throws NafinException {

        LOG.info("listaIFsiag (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, Object> informacion = new HashMap<>();
        ArrayList respuesta = new ArrayList();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" select distinct IC_CVE_SIAG  as CVE_SIAG , " + " ie.IC_IF  AS ICIF,  " +
                       " ie.CG_RAZON_SOCIAL as NOMBRE_IF  " + " from sup_datos_if i,  comcat_if ie  " +
                       " where  ie.IC_IF = i.IC_IF " + " AND i.IC_TIPO = ? " + " and i.IC_CVE_SIAG  is not null ");

            if ("Dictamente".equals(bit)) { // esto es para solo obtener los If�s que estan en bitacora
                qry.append(" and i.ic_if in ( select distinct  ic_if from bit_supervision  where BIT_EJECUTIVO_LOGIN is not null     ");
                qry.append(" and  bit_fec_pub_dic is not null   )   ");

            } else if ("ResulSupervision".equals(bit)) {
                qry.append(" and i.ic_if in (select distinct  ic_if from bit_supervision  where BIT_EJECUTIVO_LOGIN is not null    ");
                qry.append(" and bit_fec_pub_res_sup is not null   )   ");

            } else if ("Bitacora".equals(bit)) {
                qry.append(" and i.ic_if in (select distinct  ic_if from bit_supervision  where BIT_EJECUTIVO_LOGIN is not null  )   ");
            }
            
            lVarBind = new ArrayList();
            lVarBind.add(tipoC);
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = new HashMap<>();
                informacion.put("CVESIAG", rs.getString("CVE_SIAG") == null ? "" : rs.getString("CVE_SIAG"));
                informacion.put("ICIF", rs.getString("ICIF") == null ? "" : rs.getString("ICIF"));
                informacion.put("NOMBREIF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                respuesta.add(informacion);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en listaIFsiag. " + e);
            throw new AppException("Error listaIFsiag   ");
        } finally {
            LOG.info("listaIFsiag (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;

    }

    /**
     * Obtengo  los datos de contacto por banco � los ejecutivos de nafin que tiene
     * asigandos el banco
     * autor =  QC-DLHC
     * @param claveSiag
     * @param tipoUsuario
     * @param claveIF
     * @return
     * @throws NafinException
     */
    private ArrayList contactosyEjecutivos(String claveSiag, String tipoUsuario, String claveIF) throws NafinException {
        LOG.info("SupervisionBean::contactosyEjecutivos(E)");
        
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        UtilUsr utilUsr = new UtilUsr();
        Map<String, Object> informacion = new HashMap<>();
        ArrayList resp = new ArrayList();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" select IC_CLIENTE  , CS_PRINCIPAL " + " from  sup_datos_if " + " where ic_cve_siag = ? " +
                       " and  IC_TIPO = ?   " + " and  IC_IF = ?  " + " ORDER BY CS_PRINCIPAL DESC ");

            lVarBind = new ArrayList();
            lVarBind.add(claveSiag);
            lVarBind.add(tipoUsuario);
            lVarBind.add(claveIF);
            
            LOG.debug ("(contactosyEjecutivos)qry  "+qry+" \n  lVarBind  "+lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = new HashMap<>();
                String cliente = rs.getString("IC_CLIENTE") == null ? "" : rs.getString("IC_CLIENTE");
                String nombre="", correo="";
                try {
                    Usuario usuario = utilUsr.getUsuario(cliente);
                    //LOG.trace(":usuario:"+usuario);
                    if(usuario.getEmail()!=null) {
                        nombre = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " +usuario.getApellidoMaterno();
                        correo = usuario.getEmail();
                    }
                } catch(Exception u) {
                    LOG.error("Usuario Exception:"+u);
                }
                informacion.put("LOGIN", cliente);
                informacion.put("NOMBRE", nombre);
                informacion.put("EMAILS", correo);
                informacion.put("PRINCIPAL", rs.getString("CS_PRINCIPAL") == null ? "" : rs.getString("CS_PRINCIPAL"));
                resp.add(informacion);
            }
            LOG.trace("resp:"+resp);
            rs.close();
            ps.close();
            
        } catch (Exception e) {
            LOG.error("SupervisionBean::contactosyEjecutivos(Exception) "+e);
            throw new AppException("Error contactosyEjecutivos   ");
        } finally {
            LOG.info("SupervisionBean::contactosyEjecutivos(S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return resp;
    }

    /**
     * metodo para obtener el detalle en la tabla de  SUP_CALENDARIO_IF y SUP_CALENDARIO_DETALLE
     * autor =  QC-DLHC
     * @param claveSiag
     * @param  claveIF
     * @return
     * @throws NafinException
     */
    private ArrayList datosCalendarioSupervision(String claveSiag, String claveIF) throws NafinException {
        LOG.info("SupervisionBean::datosCalendarioSupervision(E)");
        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList respuesta = new ArrayList();


        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT  DISTINCT " + 
                       " c.CAL_CONSECUTIVO as CONSECUTIVO ," +
                       " DECODE(c.CAL_MES, '1', 'Enero', '2','Febrero' ,'3','Marzo', '4','Abril', " +
                       "'5','Mayo','6','Junio','7','Julio','8','Agosto',"+
                       "'9','Septiembre','10','Octubre','11','Noviembre', '12','Diciembre',"+
                       "'13','1er Semestre', '14','2do Semestre')  || ' de '|| c.cal_anio as  MESANIO , " +
                       " c.CAL_MES AS MES,  " + 
                       " c.cal_anio as  ANIO , " +
                       " TO_CHAR(c.cal_fecha_inser ,'DD/MM/YYYY')  AS  FECHA  ," +
                       " TO_CHAR(c.CAL_FEC_LIM_ENT_E,'DD/MM/YYYY')   AS  FECHA_EXPEDIENTE,   " +
                       " TO_CHAR(c.CAL_FEC_LIM_ENT_A,'DD/MM/YYYY')   AS  FECHA_ACLARACIONES, " +
                       " TO_CHAR(c.CAL_FEC_LIM_ENT_D,'DD/MM/YYYY')   AS  FECHA_DICTAMEN,     " +
                       " c.CAL_TIPO  as TIPO  " + 
                       " FROM SUP_CALENDARIO_IF   c , SUP_CALENDARIO_DETALLE  d  " +
                       " WHERE c.NOTI_CARGA_CALEN = ?   " + 
                       " AND c.IC_CVE_SIAG = ? " +
                       " AND c.CAL_CONSECUTIVO  = d.CAL_CONSECUTIVO " + 
                       " AND c.IC_CVE_SIAG = d.IC_CVE_SIAG " +
                       " AND c.CAL_ESTATUS = ? " + 
                       " ORDER BY  c.CAL_CONSECUTIVO ASC  ");
            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(claveSiag);
            lVarBind.add("A");
            LOG.debug("qry  " + qry + " lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap datos = new HashMap();
                datos.put("CONSECUTIVO", rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO"));
                datos.put("MESANIO", rs.getString("MESANIO") == null ? "" : rs.getString("MESANIO"));
                datos.put("MES", rs.getString("MES") == null ? "" : rs.getString("MES"));
                datos.put("ANIO", rs.getString("ANIO") == null ? "" : rs.getString("ANIO"));
                datos.put("FECHA_EXPEDIENTE",
                          rs.getString("FECHA_EXPEDIENTE") == null ? "" : rs.getString("FECHA_EXPEDIENTE"));
                datos.put("FECHA_ACLARACIONES",
                          rs.getString("FECHA_ACLARACIONES") == null ? "" : rs.getString("FECHA_ACLARACIONES"));
                datos.put("FECHA_DICTAMEN",
                          rs.getString("FECHA_DICTAMEN") == null ? "" : rs.getString("FECHA_DICTAMEN"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                datos.put("TIPO", rs.getString("TIPO") == null ? "" : rs.getString("TIPO"));

                String consecutivo = rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO");
                datos.put("NUMGARANTIAS", this.totalgarantias(claveSiag, consecutivo));
                datos.put("EJECUTIVOS", this.nombreEjecutivoPrincipal(claveSiag, "E", claveIF, "S"));
                respuesta.add(datos);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            LOG.error("SupervisionBean::datosCalendarioSupervision(Exception) "+e);
            throw new AppException("Error datosCalendarioSupervision   ");
        } finally {
            LOG.info("SupervisionBean::datosCalendarioSupervision(S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;
    }


    /**
     *  metodo para obtener los nombre de los ejecutivos
     *  autor QC-DLHC
     * @param claveSiag
     * @param tipoUsuario  E = Ejecutivos Nafin  C = Contactos el Banco
     * @param claveIF
     * @return
     * @throws NafinException
     */
    private String nombreEjecutivo(String claveSiag, String tipoUsuario, String claveIF) throws NafinException {
        LOG.info("nombreEjecutivo (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder nombre = new StringBuilder();
        UtilUsr utilUsr = new UtilUsr();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" select IC_CLIENTE " + " from  sup_datos_if " + " where ic_cve_siag = ? " +
                       " and  IC_TIPO = ?   " + " and  ic_if = ?  ");

            lVarBind = new ArrayList();
            lVarBind.add(claveSiag);
            lVarBind.add(tipoUsuario);
            lVarBind.add(claveIF);
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                String cliente = rs.getString("IC_CLIENTE") == null ? "" : rs.getString("IC_CLIENTE");
                Usuario usuario = utilUsr.getUsuario(cliente);
                String nombreU =
                    usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
                nombre.append(nombre.length() > 0 ? ", " : "").append(nombreU);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en nombreEjecutivo. " + e);
            throw new AppException("Error nombreEjecutivo   ");
        } finally {
            LOG.info("nombreEjecutivo (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return nombre.toString();

    }


    /**
     * M�todo para guardar en bitacora
     * autor QC-DLHC
     * @param datos
     * @throws NafinException
     */
    private void insertBitacora(Map<String, Object> datos) throws NafinException {
        LOG.info("insertBitacora (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean exito = false;
        int hayReg = 0;
        StringBuilder cadenaError = new StringBuilder();

        try {
            con.conexionDB();

            String fecha = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            cadenaError = new StringBuilder();
            cadenaError.append(" El IF no tiene un Ejecutivo nafin parametrizado " +
                               " por lo cual no se activo la bandera de Calendario de Supervision " +
                               " Fecha Ejecuci�n Avisos:" + fecha);


            qry = new StringBuilder();
            qry.append(" SELECT count(*) as TOTAL " + " FROM bit_supervision " + " WHERE CAL_MES = ?  " +
                       " AND CAL_ANIO = ? " + " AND IC_IF  = ? " + " AND BIT_TIPO  = ? ");
            lVarBind = new ArrayList();
            lVarBind.add((String) datos.get("mes"));
            lVarBind.add((String) datos.get("anio"));
            lVarBind.add((String) datos.get("claveIF"));
            lVarBind.add((String) datos.get("tipo"));

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                hayReg = rs.getInt("TOTAL");
            }
            rs.close();
            ps.close();

            if (hayReg == 0) {

                qry = new StringBuilder();
                qry.append("INSERT INTO bit_supervision  " + " ( BIT_ID , CAL_MES, CAL_ANIO , IC_IF , " +
                           "  BIT_FEC_PUB_CAL, BIT_FEC_LIM_ENT_EXP , BIT_FEC_LIM_ENT_INF_ACL , " +
                           " BIT_EJECUTIVO_LOGIN,  BIT_EJECUTIVO_NOMBRE , BIT_TIPO, CG_ERROR  ");

                if (!"".equals((String) datos.get("loginEjecutivo"))) {
                    qry.append(" ,BIT_FEC_PUB_AVI ");
                }
                qry.append(" ) ");

                qry.append(" VALUES ( bit_supervision_seq.nextval, ?, ? , ?, " +
                           " TO_DATE(?,'DD/MM/YYYY'),  TO_DATE(?,'DD/MM/YYYY'),  TO_DATE(?,'DD/MM/YYYY') , ?, ? , ?, ?   ");

                if (!"".equals((String) datos.get("loginEjecutivo"))) {
                    qry.append(" , SYSDATE ");
                }
                qry.append(" ) ");

                lVarBind = new ArrayList();
                lVarBind.add((String) datos.get("mes"));
                lVarBind.add((String) datos.get("anio"));
                lVarBind.add((String) datos.get("claveIF"));
                lVarBind.add((String) datos.get("fechaPublicacion"));
                lVarBind.add((String) datos.get("fEntregaExpediente"));
                lVarBind.add((String) datos.get("fEntregaAclaraciones"));
                lVarBind.add((String) datos.get("loginEjecutivo"));
                lVarBind.add((String) datos.get("nombreEjecutivo"));
                lVarBind.add((String) datos.get("tipo"));

                if ("".equals((String) datos.get("loginEjecutivo"))) {
                    lVarBind.add(cadenaError.toString());
                } else {
                    lVarBind.add("");
                }

                LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

                ps = con.queryPrecompilado(qry.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();

            } else {

                qry = new StringBuilder();
                qry.append("Update  bit_supervision  " + " set BIT_EJECUTIVO_LOGIN =  ?   " +
                           " ,BIT_EJECUTIVO_NOMBRE  = ?  ");

                if (!"".equals((String) datos.get("loginEjecutivo"))) {
                    qry.append(" , BIT_FEC_PUB_AVI = SYSDATE ");
                }

                if ("".equals((String) datos.get("loginEjecutivo"))) {
                    qry.append(" , CG_ERROR = ? ");
                }

                qry.append(" WHERE CAL_MES = ?  " + " AND CAL_ANIO = ?" + " AND IC_IF = ?  " + " AND BIT_TIPO  = ? ");

                lVarBind = new ArrayList();
                lVarBind.add((String) datos.get("loginEjecutivo"));
                lVarBind.add((String) datos.get("nombreEjecutivo"));
                if ("".equals((String) datos.get("loginEjecutivo"))) {
                    lVarBind.add(cadenaError.toString());
                }

                lVarBind.add((String) datos.get("mes"));
                lVarBind.add((String) datos.get("anio"));
                lVarBind.add((String) datos.get("claveIF"));
                lVarBind.add((String) datos.get("tipo"));

                LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

                ps = con.queryPrecompilado(qry.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


            }
            exito = true;

        } catch (Exception e) {
            LOG.error("Exception en insertBitacora. " + e);
            throw new AppException("Error insertBitacora   ");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }
        LOG.info("insertBitacora (S)");
    }

    /**
     * Metodo que actualiza  con el valor en 'S' los campos
     * noti_carga_calen = Calendario de Supervision
     *  noti_carga_exp  = Carga masiva de Expedientes
     *  noti_resul_sup  = Reporte de Resultados de Supervisi�n
     *  noti_doc_dic_sup  =  Dictamen de Supervisi�n
     *  para cuando el correo fue enviado
     * autor =  QC-DLHC
     * @param consecutivo
     * @param claveSiag
     * @param tipoNotifiacion CS = Calendario de Supervision, CME = Carga masiva de Expedientes
     * RRS = Reporte de Resultados de Supervisi�n,   DS =  Dictamen de Supervisi�n
     * @throws NafinException
     */
    private void actualizaBandera(Map<String, Object> datos) throws NafinException {
        LOG.info("actualizaBandera (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        PreparedStatement psc = null;
        String campoActualiza = "";
        boolean exito = false;
        try {

            con.conexionDB();


            String consecutivo = (String) datos.get("consecutivo");
            String tipoNotifiacion = (String) datos.get("tipoNotifiacion");
            String claveSiag = (String) datos.get("claveSiag");


            if ("CS".equals(tipoNotifiacion)) { //Calendario de Supervision
                campoActualiza = "noti_carga_calen";
            } else if ("CME".equals(tipoNotifiacion)) { //Carga masiva de Expedientes
                campoActualiza = "noti_carga_exp";
            } else if ("RRS".equals(tipoNotifiacion)) { //Reporte de Resultados de Supervisi�n
                campoActualiza = "noti_resul_sup";
            } else if ("DS".equals(tipoNotifiacion)) { //Dictamen de Supervisi�n
                campoActualiza = "noti_doc_dic_sup";
            }

            qry = new StringBuilder();
            qry.append(" update  sup_calendario_if " + " set " + campoActualiza + "   = ?  " +
                       " where  CAL_CONSECUTIVO  = ? " + " AND  IC_CVE_SIAG =?  ");
            lVarBind = new ArrayList();
            lVarBind.add("S");
            lVarBind.add(consecutivo);
            lVarBind.add(claveSiag);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


            //Carga masiva de Expedientes
            if ("CME".equals(tipoNotifiacion)) {

                // esto solo apsup_archivos_carga_if
                qry = new StringBuilder();
                qry.append(" update  sup_archivos_carga_if " + " set NOTI_CARGA_EXP  = ?  " +
                           " where  ARC_MES_PAGO  = ? " + " and ARC_ANIO_PAGO = ?   " + " and IC_IF = ?  ");
                lVarBind = new ArrayList();
                lVarBind.add("S");
                lVarBind.add((String) datos.get("mes"));
                lVarBind.add((String) datos.get("anio"));
                lVarBind.add((String) datos.get("claveIF"));

                LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

                psc = con.queryPrecompilado(qry.toString(), lVarBind);
                psc.executeUpdate();
                psc.close();


            }


            exito = true;
        } catch (Exception e) {
            LOG.error("Exception en actualizaBandera. " + e);
            throw new AppException("Error actualizaBandera   ");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }
        LOG.info("actualizaBandera (S)");

    }


    /**
     * metodo que obtiene el detalle de la carga de expedientes
     * autor =  QC-DLHC
     * @param claveSiag
     * @return
     * @throws NafinException
     */
    private ArrayList datosCargaExpedientes(String claveSiag) throws NafinException {
        LOG.info("SupervisionBean::datosCargaExpedientes(E)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList respuesta = new ArrayList();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append("SELECT  DISTINCT  c.ic_cve_siag as CLAVE_SIRAG, " + 
            "c.CAL_CONSECUTIVO as CONSECUTIVO , " + 
            "DECODE(c.CAL_MES, '1', 'Enero', '2','Febrero' ,'3','Marzo', '4','Abril','5','Mayo','6','Junio','7','Julio','8','Agosto','9','Septiembre','10','Octubre','11','Noviembre', '12','Diciembre', '13','1er Semestre', '14','2do Semestre')  || ' de '|| c.cal_anio as  MESANIO ,  " + 
            "c.CAL_MES AS MES,   c.cal_anio as  ANIO ,  " + 
            "TO_CHAR(c.cal_fecha_inser ,'DD/MM/YYYY')  AS  FECHA  , " + 
            "ca.arc_no_archivos    as NUM_EXPEDIENTES, ca.cc_acuse as  NUMFOLIO,  " + 
            "ca.arc_tipo_carga AS TIPOCARGA ,  " + 
            "decode (ca.arc_tipo_carga , 'P', 'Carga Parcial', 'F', 'Carga Final', 'A', 'Carga de Aclaraciones', 'E', 'Carga Extempor�nea')  as TIPOINFORMACION , " + 
            "TO_CHAR(sac.ac_fecha_acuse ,'DD/MM/YYYY')  AS FECHACUSE " + 
            "FROM SUP_CALENDARIO_IF   c  , sup_archivos_carga_if  ca , sup_acuse_cal sa , sup_acuse sac " + 
            "WHERE c.noti_carga_exp  = ?   and c.ic_cve_siag   = ca.ic_cve_siag and sa.cc_acuse = c.cc_acuse and sac.cc_acuse = ca.cc_acuse " + 
            "and ca.ARC_MES_PAGO = c.CAL_MES   and ca.ARC_ANIO_PAGO =c.CAL_ANIO   " + 
            "and c.ic_cve_siag = ?  and ca.noti_carga_exp  = ?   " + 
            " AND c.CAL_ESTATUS = ? " + 
            "ORDER BY c.IC_CVE_SIAG, c.CAL_CONSECUTIVO ASC ");
            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(claveSiag);
            lVarBind.add("N");
            lVarBind.add("A");
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap datos = new HashMap();
                datos.put("CONSECUTIVO", rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO"));
                datos.put("CLAVE_SIRAG", rs.getString("CLAVE_SIRAG") == null ? "" : rs.getString("CLAVE_SIRAG"));
                datos.put("MESANIO", rs.getString("MESANIO") == null ? "" : rs.getString("MESANIO"));
                datos.put("MES", rs.getString("MES") == null ? "" : rs.getString("MES"));
                datos.put("ANIO", rs.getString("ANIO") == null ? "" : rs.getString("ANIO"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                datos.put("NUM_EXPEDIENTES",
                          rs.getString("NUM_EXPEDIENTES") == null ? "" : rs.getString("NUM_EXPEDIENTES"));
                datos.put("NUMFOLIO", rs.getString("NUMFOLIO") == null ? "" : rs.getString("NUMFOLIO"));
                datos.put("TIPOINFORMACION",
                          rs.getString("TIPOINFORMACION") == null ? "" : rs.getString("TIPOINFORMACION"));
                datos.put("TIPOCARGA", rs.getString("TIPOCARGA") == null ? "" : rs.getString("TIPOCARGA"));
					datos.put("FECHACUSE", rs.getString("FECHACUSE")== null ? "" : rs.getString("FECHACUSE"));

                String mes = rs.getString("MES") == null ? "" : rs.getString("MES");
                String anio = rs.getString("ANIO") == null ? "" : rs.getString("ANIO");
                String consecutivo = rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO");

                String desde = numgarantias(mes, anio, claveSiag).get("totalGarantias").toString();
                String hasta = totalgarantias(claveSiag, consecutivo);

                datos.put("GARANTIAS", desde + " de " + hasta);
                respuesta.add(datos);
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            LOG.error("SupervisionBean::datosCargaExpedientes(Exception) "+e);
            throw new AppException("Error datosCargaExpedientes   ");
        } finally {
            LOG.info("SupervisionBean::datosCargaExpedientes(S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;

    }

    /**
     *  metodo que obtiene los datos del ejecutivo desde bitacora
     * autor =  QC-DLHC
     * @param icIF
     * @param mes
     * @param anio
     * @return
     * @throws NafinException
     */
    private Map<String, Object> ejecutivoNafinBita(String icIF, String mes, String anio) throws NafinException {
        LOG.info("lisTEjecutivoNafinBita (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, Object> informacion = new HashMap<>();
        UtilUsr utilUsr = new UtilUsr();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT  BIT_ID , BIT_EJECUTIVO_LOGIN as LOGIN_EJEC,  " +
                       " BIT_EJECUTIVO_NOMBRE AS NOMBRE_EJECUTIVO,  " +
                       " TO_CHAR( bit_fec_ent_exp ,'DD/MM/YYYY') as FECHA_EXPEDIENTE, " +
                       " TO_CHAR( bit_fec_pub_res_sup ,'DD/MM/YYYY') as FECHA_RESP_SUP, " +
                       " TO_CHAR( bit_fec_pub_dic ,'DD/MM/YYYY') as FECHA_PUBLIC_DIC  " + " FROM  bit_supervision " +
                       " WHERE IC_IF = ? " + " AND CAL_MES  = ? " + " AND  CAL_ANIO  =? " +
                       " order by  CAL_MES , CAL_ANIO  asc ");

            lVarBind = new ArrayList();
            lVarBind.add(icIF);
            lVarBind.add(mes);
            lVarBind.add(anio);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                informacion = new HashMap<>();
                informacion.put("BIT_ID", rs.getString("BIT_ID") == null ? "" : rs.getString("BIT_ID"));
                informacion.put("LOGIN_EJEC", rs.getString("LOGIN_EJEC") == null ? "" : rs.getString("LOGIN_EJEC"));
                informacion.put("NOMBRE_EJECUTIVO",
                                rs.getString("NOMBRE_EJECUTIVO") == null ? "" : rs.getString("NOMBRE_EJECUTIVO"));
                informacion.put("FECHA_EXPEDIENTE",
                                rs.getString("FECHA_EXPEDIENTE") == null ? "" : rs.getString("FECHA_EXPEDIENTE"));
                informacion.put("FECHA_RESP_SUP",
                                rs.getString("FECHA_RESP_SUP") == null ? "" : rs.getString("FECHA_RESP_SUP"));
                informacion.put("FECHA_PUBLIC_DIC",
                                rs.getString("FECHA_PUBLIC_DIC") == null ? "" : rs.getString("FECHA_PUBLIC_DIC"));
                String login = rs.getString("LOGIN_EJEC") == null ? "" : rs.getString("LOGIN_EJEC");
                Usuario usuario = utilUsr.getUsuario(login);
                informacion.put("EMAILS", usuario.getEmail());
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            LOG.error("Exception en lisTEjecutivoNafinBita. " + e);
            throw new AppException("Error lisTEjecutivoNafinBita   ");
        } finally {
            LOG.info("lisTEjecutivoNafinBita (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;

    }


    /**
     * metodo para obtener el detalle de resultado de supervisi�n
     * autor =  QC-DLHC
     * @param claveSiag
     * @return
     * @throws NafinException
     */
    private ArrayList datosResultadoSupervision(String claveSiag) throws NafinException {
        LOG.info("datosResultadoSupervision (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList respuesta = new ArrayList();


        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT  DISTINCT  c.ic_cve_siag as CLAVE_SIRAG,   " + " c.CAL_CONSECUTIVO as CONSECUTIVO ," +
                       " DECODE(c.CAL_MES, '1', 'Enero', '2','Febrero' ,'3','Marzo', '4','Abril','5','Mayo','6','Junio','7','Julio','8','Agosto','9','Septiembre','10','Octubre','11','Noviembre', '12','Diciembre')  || ' de '|| c.cal_anio as  MESANIO , " +
                       " c.CAL_MES AS MES,  " + " c.cal_anio as  ANIO , " +
                       " TO_CHAR( cal_fec_lim_ent_a ,'DD/MM/YYYY') as FECHA_ENTEGA_ACLA  " +
                       " FROM SUP_CALENDARIO_IF   c " + " WHERE c.noti_resul_sup  = ?   " + " AND c.IC_CVE_SIAG = ? " +
                       " AND c.CAL_DOC_SOLICITUD is not null " + " ORDER BY c.IC_CVE_SIAG, c.CAL_CONSECUTIVO ASC  ");
            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(claveSiag);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap datos = new HashMap();
                datos.put("CONSECUTIVO", rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO"));
                datos.put("CLAVE_SIRAG", rs.getString("CLAVE_SIRAG") == null ? "" : rs.getString("CLAVE_SIRAG"));
                datos.put("MESANIO", rs.getString("MESANIO") == null ? "" : rs.getString("MESANIO"));
                datos.put("MES", rs.getString("MES") == null ? "" : rs.getString("MES"));
                datos.put("ANIO", rs.getString("ANIO") == null ? "" : rs.getString("ANIO"));
                datos.put("FECHA_ENTEGA_ACLA",
                          rs.getString("FECHA_ENTEGA_ACLA") == null ? "" : rs.getString("FECHA_ENTEGA_ACLA"));
                respuesta.add(datos);

            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            LOG.error("Exception en datosResultadoSupervision. " + e);
            throw new AppException("Error datosResultadoSupervision   ");
        } finally {
            LOG.info("datosResultadoSupervision (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;

    }


    /**
     *  metodo para obtener el detallde del dictamen de supervisi�n
     * autor =  QC-DLHC
     * @param claveSiag
     * @return
     * @throws NafinException
     */
    private ArrayList datosDictamenSupervision(String claveSiag) throws NafinException {
        LOG.info("datosDictamenSupervision (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList respuesta = new ArrayList();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(
                " SELECT  DISTINCT " + 
                " c.ic_cve_siag as CLAVE_SIRAG,   " +
                " c.CAL_CONSECUTIVO as CONSECUTIVO ," +
                " DECODE(c.CAL_MES, '1', 'Enero', '2','Febrero' ,'3','Marzo', '4','Abril','5','Mayo','6','Junio','7','Julio','8','Agosto','9','Septiembre','10','Octubre','11','Noviembre', '12','Diciembre', '13','1er Semestre', '14','2do Semestre')  || ' de '|| c.cal_anio as  MESANIO , " +
                " c.CAL_MES AS MES,  " + 
                " c.cal_anio as  ANIO,  " + 
                " c.CAL_TIPO  as TIPO  " + 
                " FROM SUP_CALENDARIO_IF   c " +
                " WHERE c.noti_doc_dic_sup  = ?   " + 
                " AND c.IC_CVE_SIAG = ? " +
                " AND c.CAL_DOC_DICTAMEN is not null " + 
                " AND c.CAL_ESTATUS = ? " + 
                " ORDER BY c.IC_CVE_SIAG, c.CAL_CONSECUTIVO ASC  ");
            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(claveSiag);
            lVarBind.add("A");
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap datos = new HashMap();
                datos.put("CONSECUTIVO", rs.getString("CONSECUTIVO") == null ? "" : rs.getString("CONSECUTIVO"));
                datos.put("CLAVE_SIRAG", rs.getString("CLAVE_SIRAG") == null ? "" : rs.getString("CLAVE_SIRAG"));
                datos.put("MESANIO", rs.getString("MESANIO") == null ? "" : rs.getString("MESANIO"));
                datos.put("MES", rs.getString("MES") == null ? "" : rs.getString("MES"));
                datos.put("ANIO", rs.getString("ANIO") == null ? "" : rs.getString("ANIO"));
                datos.put("TIPO", rs.getString("TIPO") == null ? "" : rs.getString("TIPO"));
                respuesta.add(datos);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            LOG.error("Exception en datosDictamenSupervision. " + e);
            throw new AppException("Error datosDictamenSupervision   ");
        } finally {
            LOG.info("datosDictamenSupervision (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;

    }


    /**
     *  metodo para obtener el total de las garantias
     * autor =  QC-DLHC
     * @param claveSiag
     * @param consecutivo
     * @return
     * @throws NafinException
     */
    private String totalgarantias(String claveSiag, String consecutivo) throws NafinException {
        LOG.info("totalgarantias (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String totalGarantias = "0";


        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT count(distinct CAL_REL_GARANTIAS) as TOTAL " + " FROM SUP_CALENDARIO_DETALLE " +
                       " WHERE CAL_CONSECUTIVO = ?  " + " AND IC_CVE_SIAG = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(consecutivo);
            lVarBind.add(claveSiag);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                totalGarantias = rs.getString("TOTAL") == null ? "0" : rs.getString("TOTAL");
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            LOG.error("Exception en totalgarantias. " + e);
            throw new AppException("Error totalgarantias   ");
        } finally {
            LOG.info("totalgarantias (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return totalGarantias;

    }

    /**
     *
     * autor =  QC-DLHC
     * @param mes
     * @param anio
     * @param claveSiag
     * @return
     * @throws NafinException
     */
    private Map<String, Object> numgarantias(String mes, String anio, String claveSiag) throws NafinException {
        LOG.info("SupervisionBean::numgarantias(E)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String numgarantias = "0";
        int totalGarantias = 0;
        int totalComplementos = 0;
        Map<String, Object> informacionGarantiasComplementos;

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT distinct REL_GARANTIAS  " + " FROM SUP_ARCHIVOS_CARGA_IF " +
                       " WHERE ARC_MES_PAGO  = ?  " + " AND ARC_ANIO_PAGO = ? " + " AND IC_CVE_SIAG  = ? " +
                       " ORDER BY REL_GARANTIAS DESC ");
            lVarBind = new ArrayList();
            lVarBind.add(mes);
            lVarBind.add(anio);
            lVarBind.add(claveSiag);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();

            Set setGarantias = new HashSet();
            Set setComplementos = new HashSet();
            informacionGarantiasComplementos = new HashMap<>();
            while (rs.next()) {
                String garantia = rs.getString("REL_GARANTIAS") == null ? "" : rs.getString("REL_GARANTIAS");
                int vacio = 0;

                String todasLasGarantias[] = garantia.split("\\|");
                for (int i = 0; i < todasLasGarantias.length; i++) {    
                    if (todasLasGarantias[i].indexOf("_") == -1) {
                        LOG.trace("###### Garantias: "+todasLasGarantias[i].toString());
                        
                        totalGarantias++;
                        setGarantias.add(todasLasGarantias[i]);
                    } else {
                        totalComplementos++;
                        setComplementos.add(todasLasGarantias[i]);
                    }
                }
                todasLasGarantias = null;
            }
            rs.close();
            ps.close();

            informacionGarantiasComplementos.put("totalGarantias", totalGarantias);
            informacionGarantiasComplementos.put("totalComplementos", totalComplementos);
            informacionGarantiasComplementos.put("relacionGarantias", setGarantias);
            informacionGarantiasComplementos.put("relacionComplementos", setComplementos);

            numgarantias = String.valueOf(totalGarantias);

        } catch (Exception e) {
            LOG.error("SupervisionBean::numgarantias(Exception) "+e);
            throw new AppException("Error numgarantias   "+e);
        } finally {
            LOG.info("SupervisionBean::numgarantias(S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacionGarantiasComplementos;
    }


    /**
     * Metodo para saber el periodo en el que se ejecutara el proceso  y su hora de fin
     * autor =  QC-DLHC
     * @param parametro
     * @return
     * @throws NafinException
     */
    private String ejecutarProceso(String parametro) throws NafinException {
        LOG.info("ejecutarProceso (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String valorParametro = "";

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" SELECT  VALOR   " + " FROM SUP_PARAMETROS_PROCESO  " + " WHERE  PARAM_GRAL  = ?    ");


            lVarBind.add(parametro);

            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                valorParametro = rs.getString("VALOR") == null ? "" : rs.getString("VALOR");
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            LOG.error("Exception en ejecutarProceso. " + e);
            throw new AppException("Error ejecutarProceso   ");
        } finally {
            LOG.info("ejecutarProceso (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return valorParametro;

    }


    public String obtenGarantias(String acuse) throws Exception {
        LOG.info("SupervisionBean::obtenGarantias (E)");
        String garantias = "";
        ResultSet rs = null;
        String qrySentencia = "";
        AccesoDB conn = new AccesoDB();

        try {
            conn.conexionDB();
            qrySentencia =
                " SELECT Substr(t.arc_nombre, 0, Length(t.arc_nombre) - 4) garantia \n" +
                "    FROM   sup_archivos_carga_if t \n" + "    WHERE  t.cc_acuse =" + acuse;


            rs = conn.queryDB(qrySentencia);
            LOG.info("qrySentencia" + qrySentencia);

            while (rs.next()) {
                garantias += rs.getString(1) + ",";
            } //while

            rs.close();
        } catch (Exception e) {
            LOG.error("SupervisionBean::obtenGarantias Exception " + e);
            throw new NafinException("SIST0001");
        } finally {
            conn.cierraConexionDB();
            LOG.info("SupervisionBean::obtenGarantias (S)");
        }
        return garantias;
    }

    private URL getWSDireccion() throws Exception {
        URL url = null;
        WebServiceException e = null;
        LOG.info("SupervisionBean::getWSDireccion (E)");
        ResultSet rs = null;
        String qrySentencia = "";
        AccesoDB conn = new AccesoDB();
        String wsDireccion = "";
        try {
            conn.conexionDB();
            qrySentencia = "select T.SUP_DIR_WS from com_param_gral t where t.ic_param_gral = 1";

            rs = conn.queryDB(qrySentencia);
            LOG.info("qrySentencia" + qrySentencia);

            while (rs.next()) {
                wsDireccion = rs.getString(1);
            } //while

            rs.close();

            LOG.info("Esta es la direccion " + wsDireccion);

            url = new URL(wsDireccion + "?WSDL#%7Bhttp%3A%2F%2Ftempuri.org%2F%7DProcesos");


        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        } catch (Exception ex1) {
            LOG.error("SupervisionBean::getWSDireccion Exception " + ex1);
            throw new NafinException("SIST0001");
        } finally {
            if (conn.hayConexionAbierta()) {
                conn.cierraConexionDB();
            }
            LOG.info("SupervisionBean::getWSDireccion (S)");
        }
        return url;
    }


    /**
     * M�todo para la pantalla: Procesos de Supervision
     * @param interFinanciero, dependiendo del perfil puede ser el ic_cve siag o el usuario
     * @param calMes
     * @param calAnio
     * @param hayIntermediario. Si es false, el perfil que realiza la consulta es IF SUPGARANT, el query se ajusta.
     * @return Lista de HashMap con los resultados de la consulta
     * @throws NafinException
     */
    public List<Map> obtieneResultadosDeSupervision(String interFinanciero, String calMes, String calAnio,
                                                    String relacionGarantias,
                                                    boolean hayIntermediario) throws NafinException {

        LOG.info("SupervisionBean::obtieneResultadosDeSupervision (E)");

        List<Map> resultado = new ArrayList<>();
        Map<String, Object> registro = new HashMap<>();
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        String consulta = "";
        String consultaPrimeraVez = "";
        String subConsulta = "";

        if (hayIntermediario == true) {
            subConsulta = "AND A.IC_CVE_SIAG = " + interFinanciero + "  ";
        } else {
            subConsulta =
                "AND A.IC_CVE_SIAG IN (SELECT IC_CVE_SIAG FROM SUP_DATOS_IF WHERE IC_CLIENTE='" + interFinanciero +
                "')  ";
        }

        consulta =
            "SELECT DISTINCT decode(c.cal_mes, 13, '1erSem', 14, '2doSem', c.cal_mes) || ' - ' || C.CAL_ANIO AS MES_PAGO  " + ",DECODE(A.ARC_TIPO_CARGA,  " +
            "'P',  " + "'Carga Parcial',  " + "'F',  " + "'Carga Final',  " + "'A',  " +
            "'Carga Aclaraciones',  " + "'E',  " + "'Carga Extemporanea', 'S', 'Semestral') AS TIPO_CARGA  " +
            ",DECODE(C.CAL_TIPO, 'O', 'Ordinario', 'E','Extempor�neo', 'S', 'Semestral') AS TIPO_CALENDARIO  " +
            ",A.CC_ACUSE AS FOLIO_OPERACION  " + ",A.ARC_NO_ARCHIVOS AS NO_EXPEDIENTES  " +
            ",TO_CHAR(B.AC_FECHA_ACUSE, 'DD/MM/YYYY HH24:MI:SS') AS FECHA_HORA  " +
            ",TO_CHAR(C.CAL_FEC_LIM_ENT_E, 'DD/MM/YYYY') AS FECHA_LIMITE  " + ", c.cal_mes AS MES  " +
            ",A.ARC_ANIO_PAGO AS ANIO  " + ",A.IC_CVE_SIAG AS CVE_SIAG  " +
            ",CASE WHEN A.ARC_NO_ARCHIVOS > 0 THEN '1' ELSE '0' END AS ARCHIVOS_CARGADOS  " +
            ",C.NOTI_CARGA_CALEN AS AVISO_SUPERVISION  " + ",C.CAL_DOC_SOLICITUD AS RESULTADOS_SUPERVISION  " +
            ",C.CAL_DOC_DICTAMEN AS DICTAMEN_SUPERVISION  " + ",C.CAL_CONS_AVISO_SUP AS CONSULTA_AVISO_SUP  " +
            ",C.CAL_DOC_SOLICITUD AS CAL_DOC_SOLICITUD  " + ",C.CAL_DOC_DICTAMEN AS CAL_DOC_DICTAMEN , " +
            " C.CAL_CONSECUTIVO AS CONSECUTIVO " + "FROM SUP_ARCHIVOS_CARGA_IF A LEFT JOIN SUP_CALENDARIO_IF C  " +
            "ON  A.IC_CVE_SIAG = C.IC_CVE_SIAG  " + "AND A.ARC_MES_PAGO = C.CAL_MES  " +
            "AND A.ARC_ANIO_PAGO = C.CAL_ANIO  " + "LEFT JOIN SUP_ACUSE B ON A.CC_ACUSE = B.CC_ACUSE  " +
            "WHERE A.IC_CVE_SIAG = C.IC_CVE_SIAG  " + "AND A.ARC_MES_PAGO = C.CAL_MES  " +
            "AND A.ARC_ANIO_PAGO = C.CAL_ANIO  " + "AND C.CAL_ESTATUS ='A'  " + "AND C.CAL_MES = " + calMes + "  " +
            "AND C.CAL_ANIO = " + calAnio + "  ";
        
        if (!relacionGarantias.equals("-1")) {
            consulta += " AND A.REL_GARANTIAS LIKE '%" + relacionGarantias + "%' ";
        }
        
        consulta += subConsulta + "ORDER BY A.CC_ACUSE";
        
        consultaPrimeraVez =
            "SELECT DISTINCT decode(A.CAL_MES, 13, '1erSem', 14, '2doSem', A.CAL_MES) || ' - ' || A.CAL_ANIO AS MES_PAGO,  " + "' - ' AS TIPO_CARGA, " +
            "DECODE(A.CAL_TIPO, 'O', 'Ordinario', 'E','Extempor�neo', 'S', 'Semestral') AS TIPO_CALENDARIO, " +
            "' - ' AS FOLIO_OPERACION, " + "' - ' AS NO_EXPEDIENTES, " + "' - ' AS FECHA_HORA, " +
            "TO_CHAR(A.CAL_FEC_LIM_ENT_E, 'DD/MM/YYYY') AS FECHA_LIMITE, " + " A.CAL_MES AS MES, " +
            "A.CAL_ANIO AS ANIO, " + "A.IC_CVE_SIAG AS CVE_SIAG, " + "'0' AS ARCHIVOS_CARGADOS, " +
            "A.NOTI_CARGA_CALEN AS AVISO_SUPERVISION, " + "A.CAL_DOC_SOLICITUD AS RESULTADOS_SUPERVISION, " +
            "A.CAL_DOC_DICTAMEN AS DICTAMEN_SUPERVISION,  " + "A.CAL_CONS_AVISO_SUP AS CONSULTA_AVISO_SUP,  " +
            "A.CAL_DOC_SOLICITUD AS CAL_DOC_SOLICITUD,  " + "A.CAL_DOC_DICTAMEN AS CAL_DOC_DICTAMEN, " +
            " A.CAL_CONSECUTIVO AS CONSECUTIVO " + "FROM SUP_CALENDARIO_IF A " + "WHERE A.CAL_ESTATUS ='A'  " +
            "AND A.CAL_MES = " + calMes + "  " + "AND A.CAL_ANIO = " + calAnio + "  " + subConsulta;

        try {
            con.conexionDB();
            rs = con.queryDB(consulta);
            LOG.info("Consulta: " + consulta);
            
            if (!rs.isBeforeFirst()) {
                LOG.info("consultaPrimeraVez: " + consultaPrimeraVez);
                rs = con.queryDB(consultaPrimeraVez);
            }

            while (rs.next()) {
                registro = new HashMap<>();
                registro.put("MES_PAGO", (rs.getString("MES_PAGO") == null) ? "" : rs.getString("MES_PAGO"));
                registro.put("TIPO_CARGA", (rs.getString("TIPO_CARGA") == null) ? "" : rs.getString("TIPO_CARGA"));
                registro.put("TIPO_CALENDARIO",
                             (rs.getString("TIPO_CALENDARIO") == null) ? "" : rs.getString("TIPO_CALENDARIO"));
                registro.put("FOLIO_OPERACION",
                             (rs.getString("FOLIO_OPERACION") == null) ? "" : rs.getString("FOLIO_OPERACION"));
                registro.put("NO_EXPEDIENTES",
                             (rs.getString("NO_EXPEDIENTES") == null) ? "" : rs.getString("NO_EXPEDIENTES"));
                registro.put("FECHA_HORA", (rs.getString("FECHA_HORA") == null) ? "" : rs.getString("FECHA_HORA"));
                registro.put("FECHA_LIMITE",
                             (rs.getString("FECHA_LIMITE") == null) ? "" : rs.getString("FECHA_LIMITE"));
                registro.put("MES", (rs.getString("MES") == null) ? "" : rs.getString("MES"));
                registro.put("ANIO", (rs.getString("ANIO") == null) ? "" : rs.getString("ANIO"));
                registro.put("CVE_SIAG", (rs.getString("CVE_SIAG") == null) ? "" : rs.getString("CVE_SIAG"));
                registro.put("ARCHIVOS_CARGADOS",
                             (rs.getString("ARCHIVOS_CARGADOS") == null) ? "" : rs.getString("ARCHIVOS_CARGADOS"));
                registro.put("AVISO_SUPERVISION",
                             (rs.getString("AVISO_SUPERVISION") == null) ? "" : rs.getString("AVISO_SUPERVISION"));
                registro.put("RESULTADOS_SUPERVISION",
                             (rs.getString("RESULTADOS_SUPERVISION") == null) ? "" :
                             rs.getString("RESULTADOS_SUPERVISION"));
                registro.put("DICTAMEN_SUPERVISION",
                             (rs.getString("DICTAMEN_SUPERVISION") == null) ? "" :
                             rs.getString("DICTAMEN_SUPERVISION"));
                registro.put("CONSULTA_AVISO_SUP",
                             (rs.getString("CONSULTA_AVISO_SUP") == null) ? "" : rs.getString("CONSULTA_AVISO_SUP"));
                registro.put("CAL_DOC_SOLICITUD",
                             (rs.getString("CAL_DOC_SOLICITUD") == null) ? "" : rs.getString("CAL_DOC_SOLICITUD"));
                registro.put("CAL_DOC_DICTAMEN",
                             (rs.getString("CAL_DOC_DICTAMEN") == null) ? "" : rs.getString("CAL_DOC_DICTAMEN"));


                Map<String, Object> garantiasCompleto =
                    numgarantias(calMes, calAnio, rs.getString("CVE_SIAG").toString());

                String desde = garantiasCompleto.get("totalGarantias").toString();
                String hasta =
                    totalgarantias(rs.getString("CVE_SIAG").toString(), rs.getString("CONSECUTIVO").toString());

                registro.put("PROGRESO", "" + desde + " de " + hasta);
                registro.put("PROGRESOCOMPLETO", garantiasCompleto);

                resultado.add(registro);
            }
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::obtieneResultadosDeSupervision Exception " + e);
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::obtieneResultadosDeSupervision (S)");
        }
        return resultado;
    }

    /**
     * Obtiene los avisos de supervision
     * @param interFin
     * @param anio
     * @param mes
     * @return Lista de HashMap con los resultados de la consulta
     * @throws NafinException
     */
    public List<Map> obtieneAvisosDeSupervision(String interFin, String anio, String mes) throws NafinException {

        LOG.info("SupervisionBean::obtieneAvisosDeSupervision (E)");
        List<Map> resultado = new ArrayList<>();
        Map<String, String> registro = new HashMap<>();
        AccesoDB con = new AccesoDB();
        StringBuilder relGarant = new StringBuilder();
        ResultSet rs = null;
        ResultSet rs1 = null;
        String consulta = "";
        String consGarant = "";
        String numGarant = "";
        String consecutivo = "";

        consulta =
            "SELECT A.CAL_CONSECUTIVO \n" + ", A.IC_CVE_SIAG \n" + ", A.CAL_MES \n" + ", A.CAL_ANIO \n" +
            ", TO_CHAR(A.CAL_FEC_LIM_ENT_E,'DD/MM/YYYY') AS FECHA_EXP \n" +
            ", TO_CHAR(A.CAL_FEC_LIM_ENT_A,'DD/MM/YYYY') AS FECHA_ACL \n" +
            ", TO_CHAR(A.CAL_FEC_LIM_ENT_D,'DD/MM/YYYY') AS FECHA_DIC \n" +
            ", DECODE(A.CAL_TIPO, 'O', 'Ordinario', 'E','Extempor�neo', 'S', 'Semestral') CAL_TIPO \n" +
            ", A.CAL_REL_GARANTIAS AS REL_GARANTIAS \n" + "FROM SUP_CALENDARIO_IF A \n" + "WHERE A.IC_CVE_SIAG = " +
            interFin + " \n" + "AND A.CAL_ANIO = " + anio + " \n" + "AND A.CAL_MES = " + mes;

        try {
            con.conexionDB();
            rs = con.queryDB(consulta);
            LOG.info("Consulta:\n" + consulta);
            while (rs.next()) {

                //Obtengo la relacion de garantias de la tabla SUP_CALENDARIO_DETALLE
                consecutivo = (rs.getString("CAL_CONSECUTIVO") == null) ? "" : (rs.getString("CAL_CONSECUTIVO"));
                consGarant =
                    "SELECT CAL_REL_GARANTIAS FROM SUP_CALENDARIO_DETALLE WHERE CAL_CONSECUTIVO = " + consecutivo +
                    " AND IC_CVE_SIAG = " + interFin;
                rs1 = con.queryDB(consGarant);
                registro = new HashMap<>();
                int cont = 0;
                while (rs1.next()) {
                    if (cont > 0) {
                        relGarant.append(",");
                    }
                    relGarant.append((rs1.getString("CAL_REL_GARANTIAS") == null) ? "" :
                                     (rs1.getString("CAL_REL_GARANTIAS")));
                    cont++;
                }

                //Si hay relacion de garantias, se cuentan para mostrarlas en el visor
                if (!"".equals(relGarant.toString())) {
                    String[] arrGarant = relGarant.toString().split(",");
                    numGarant = "" + arrGarant.length;
                } else {
                    numGarant = "";
                }

                registro.put("CLAVE_SIAG", (rs.getString("IC_CVE_SIAG") == null) ? "" : rs.getString("IC_CVE_SIAG"));
                registro.put("MES_PAGO", (rs.getString("CAL_MES") == null) ? "" : rs.getString("CAL_MES"));
                registro.put("ANIO_PAGO", (rs.getString("CAL_ANIO") == null) ? "" : rs.getString("CAL_ANIO"));
                registro.put("FECHA_LIMITE_EXP", (rs.getString("FECHA_EXP") == null) ? "" : rs.getString("FECHA_EXP"));
                registro.put("FECHA_LIMITE_ACL", (rs.getString("FECHA_ACL") == null) ? "" : rs.getString("FECHA_ACL"));
                registro.put("FECHA_LIMITE_DIC", (rs.getString("FECHA_DIC") == null) ? "" : rs.getString("FECHA_DIC"));
                registro.put("TIPO_CALENDARIO", (rs.getString("CAL_TIPO") == null) ? "" : rs.getString("CAL_TIPO"));
                registro.put("NUM_GARANTIAS", numGarant);
                registro.put("REL_GARANTIAS", relGarant.toString());
                resultado.add(registro);

            }
            rs1.close();
            rs.close();
            con.cierraStatement();
        } catch (Exception e) {
            LOG.error("SupervisionBean::obtieneAvisosDeSupervision Exception " + e);
            throw new NafinException("SIST0001");
        } finally {
            con.cierraConexionDB();
            LOG.info("SupervisionBean::obtieneAvisosDeSupervision (S)");
        }
        return resultado;
    }

    /**
     * Si es la primera vez que el Intermediario Financiero consulta el Aviso de Supervisi�n,
     * se guardar� la fecha de consulta en la Bit�cora. (Solo aplica para la consulta del IF)
     * @param interFin
     * @param anio
     * @param mes
     * @param tipoCalendario
     * @return boleano que indica si el metodo concluy� exitosamente o no
     * @throws NafinException
     */
    public boolean guardaEnBitacoraAvisoSup(String interFin, String anio, String mes,
                                            String tipoCalendario) throws NafinException {

        LOG.info("SupervisionBean::guardaEnBitacoraAvisoSup (E)");

        boolean exito = false;
        AccesoDB con = new AccesoDB();
        List lVarBind = new ArrayList();
        String queryBit = "";
        String queryCal = "";

        queryBit =
                "UPDATE BIT_SUPERVISION SET BIT_FEC_CON_AVI = SYSDATE WHERE CAL_MES = ? AND CAL_ANIO = ? AND BIT_TIPO = ? AND IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?)";
        queryCal =
            "UPDATE SUP_CALENDARIO_IF SET CAL_CONS_AVISO_SUP = 'S' WHERE CAL_MES = ? AND CAL_ANIO = ? AND CAL_TIPO = ? AND IC_CVE_SIAG = ?";

        lVarBind.add(new Integer(mes));
        lVarBind.add(new Integer(anio));
        lVarBind.add(tipoCalendario);
        lVarBind.add(new Integer(interFin));

        try {
            con.conexionDB();
            //Guardo en bit�cora la fecha en que se consult� el aviso
            LOG.debug("queryBit:"+queryBit);
            LOG.trace("lVarBind:"+lVarBind);
            con.ejecutaUpdateDB(queryBit, lVarBind);
            exito = true;

            //Una vez guardado, actualizo el registro que indica que ya se consult� por primera vez
            if (exito == true) {
                LOG.debug("queryCal:"+queryCal);
                LOG.trace("lVarBind:"+lVarBind);
                con.ejecutaUpdateDB(queryCal, lVarBind);
                exito = true;
            }

        } catch (Exception e) {
            exito = false;
            LOG.error("SupervisionBean::guardaEnBitacoraAvisoSup Exception " + e);
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(exito);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::guardaEnBitacoraAvisoSup (S)");
        }
        return exito;
    }

    /**
     * Si es la primera vez que el Intermediario Financiero consulta Resultados de Supervisi�n o el Dictamen Supervisi�n,
     * se guardar� la fecha de consulta en la Bit�cora
     * @param idArchivo
     * @param interFin
     * @param anio
     * @param mes
     * @param recibo
     * @param tipoCalendario
     * @return
     * @throws NafinException
     */
    public boolean guardaEnBitacoraArchivosSup(String idArchivo, String interFin, String anio, String mes,
                                               String recibo, String tipoCalendario) throws NafinException {

        LOG.info("SupervisionBean::guardaEnBitacoraArchivosSup (E)");

        boolean exito = false;
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        List lVarBind = new ArrayList();
        String tipoCal = "";
        String querySup = "";
        String queryDic = "";
        String querySupIF = "";
        String queryDicIF = "";
        Integer campo = 0;

        if (null != tipoCalendario && !"".equals(tipoCalendario)) {
            if ("Ordinario".equals(tipoCalendario)) {
                tipoCal = "O";
            } else if ("Extempor�neo".equals(tipoCalendario)) {
                tipoCal = "E";
            }
        }

        querySup =
            "SELECT DISTINCT BIT_FOLIO_CON_RES FROM BIT_SUPERVISION WHERE CAL_MES = " + mes + " AND CAL_ANIO = " +
            anio + " AND BIT_TIPO = '" + tipoCal +
            "' AND IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = " + interFin + ")";
        queryDic =
            "SELECT DISTINCT BIT_FOLIO_CON_DIC FROM BIT_SUPERVISION WHERE CAL_MES = " + mes + " AND CAL_ANIO = " +
            anio + " AND BIT_TIPO = '" + tipoCal +
            "' AND IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = " + interFin + ")";
        querySupIF =
            "UPDATE BIT_SUPERVISION SET BIT_FEC_CON_RES_SUP = SYSDATE, BIT_FOLIO_CON_RES = ? WHERE CAL_MES = ? AND CAL_ANIO = ? AND BIT_TIPO = ? AND IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?)";
        queryDicIF =
            "UPDATE BIT_SUPERVISION SET BIT_FEC_CON_RES_DIC = SYSDATE, BIT_FOLIO_CON_DIC = ? WHERE CAL_MES = ? AND CAL_ANIO = ?  AND BIT_TIPO = ? AND IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?)";

        try {
            con.conexionDB();
            if ("RESULTADOS_SUPERVISION".equals(idArchivo)) {
                rs = con.queryDB(querySup);
                while (rs.next()) {
                    campo = rs.getInt("BIT_FOLIO_CON_RES");
                }
                //Si no entra al if entonces la acci�n ya existe en bit�cora
                if (null == campo || campo < 1) {
                    lVarBind.add(new Integer(recibo));
                    lVarBind.add(new Integer(mes));
                    lVarBind.add(new Integer(anio));
                    lVarBind.add(tipoCal);
                    lVarBind.add(new Integer(interFin));
                    con.ejecutaUpdateDB(querySupIF, lVarBind);
                    exito = true;
                }

            } else if ("DICTAMEN_SUPERVISION".equals(idArchivo)) {
                rs = con.queryDB(queryDic);
                while (rs.next()) {
                    campo = rs.getInt("BIT_FOLIO_CON_DIC");
                }
                //Si no entra al if entonces la acci�n ya existe en bit�cora
                if (null == campo || campo < 1) {
                    lVarBind.add(new Integer(recibo));
                    lVarBind.add(new Integer(mes));
                    lVarBind.add(new Integer(anio));
                    lVarBind.add(tipoCal);
                    lVarBind.add(new Integer(interFin));
                    con.ejecutaUpdateDB(queryDicIF, lVarBind);
                    exito = true;
                }
            }
        } catch (Exception e) {
            exito = false;
            LOG.error("SupervisionBean::guardaEnBitacoraArchivosSup Exception " + e);
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            LOG.info("SupervisionBean::guardaEnBitacoraArchivosSup (S)");
        }
        return exito;
    }

    /**
     * Obtiene un archivo desde el servicio web, en caso de que el usuario que descarg� el archivo sea un IF, la acci�n se registra en bit�cora
     * @param tipoPerfil
     * @param idArchivo
     * @param nombreArchivo
     * @param interFin
     * @param anio
     * @param mes
     * @param recibo
     * @param tipoCalendario
     * @return
     * @throws NafinException
     */
    @Override
    public ArchivosCargaValue getArchivo(String tipoPerfil, String idArchivo, String nombreArchivo, String interFin,
                                         String anio, String mes, String recibo,
                                         String tipoCalendario) throws NafinException {

        ArchivosCargaValue archivosValue = new ArchivosCargaValue();
        byte[] buf = null;

        try {
            LOG.info("SupervisionBean::getArchivo (E)");
            String identificador = nombreArchivo;

            LOG.info("ENTRO A CLASE ONBASE DE getArchivo ");
            URL url = this.getWSDireccion();
            Procesos procesos = new Procesos(url);
            ProcesosSoap procesosSoap = procesos.getProcesosSoap();
            ResultadoSalida resultado = procesosSoap.obtieneArchivo("NESUPGAR", identificador, "", "");
            LOG.info("SALIO DE CLASE ONBASE DE getArchivo ");
            buf = Base64.decodeBase64(resultado.getStArchivoBase64().getBytes(UTF_8));
            LOG.info("RESULTADO " + resultado.getStEstatus());
            LOG.info("RESULTADO ONBASE " + resultado.getStError());
            archivosValue.setARCHIVO_ESP(buf);
            //Ya que obtuve el archivo, actualizo en bitacora (Si el usuario que descarg� es un IF)
            guardaEnBitacoraArchivosSup(idArchivo, interFin, anio, mes, recibo, tipoCalendario);


        } catch (Exception ex) {
            LOG.error("SupervisionBean::getArchivo Exception " + ex);
            throw new NafinException("SIST0001");
        } finally {
            LOG.info("SupervisionBean::getArchivo (S)");
        }
        return archivosValue;
    }

    public Integer validaUsuario(String usuario) throws NafinException {
        LOG.info("SupervisionBean::validaUsuario(E)");
        boolean resultadoG = false;
        ResultSet rs = null;
        String qrySentencia = "";
        AccesoDB conn = new AccesoDB();
        int existeUsuario = 0;
        try {
            conn.conexionDB();
            qrySentencia = "SELECT COUNT(U.IC_IF) FROM SUP_DATOS_IF U WHERE U.IC_CLIENTE = '" + usuario + "'";
            LOG.debug("qrySentencia" + qrySentencia);
            rs = conn.queryDB(qrySentencia);
            while (rs.next()) {
                existeUsuario = rs.getInt(1);
            } //while
            rs.close();
            conn.cierraConexionDB();
        } catch (Exception e) {
            LOG.error("SupervisionBean::validaUsuario(Exception) "+e);
            resultadoG = false;
            throw new NafinException("SIST0001");
        } finally {
            conn.cierraConexionDB();
            LOG.info("SupervisionBean::validaUsuario(S)");
        }
        return existeUsuario;
    }

    public boolean validaExtemporaneo(ArchivosCargaValue archivosValue) throws NafinException {
        LOG.info("SupervisionBean::validaExtemporaneo(E)");
        boolean resultadoG = true;
        ResultSet rs = null;
        String qrySentencia = "";
        AccesoDB conn = new AccesoDB();
        int existeUsuario = 0;
        try {
            if("E".equals(archivosValue.getTipoCarga())) {
                conn.conexionDB();
                qrySentencia = 
                    "select count(1) " + 
                    "  from sup_calendario_if d " + 
                    " where d.CAL_NO_AUTORIZA = '" +archivosValue.getAcuerdo()+ "'" + 
                    "   and d.cal_estatus = 'A' " + 
                    "   and d.cal_tipo = 'E' " + 
                    "  and   d.CAL_MES = " +archivosValue.getMes()+
                    "  and   d.CAL_ANIO = " +archivosValue.getAnio();
                LOG.debug("qrySentencia:" + qrySentencia);
                rs = conn.queryDB(qrySentencia);
                if(rs.next()) {
                    existeUsuario = rs.getInt(1);
                } //while
                rs.close();
                conn.cierraConexionDB();
                if(existeUsuario<1) 
                    resultadoG = false;
            }
            LOG.trace("resultadoG:"+resultadoG);
        } catch (Exception e) {
            LOG.error("SupervisionBean::validaExtemporaneo(Exception): "+e);
            resultadoG = true;
            throw new NafinException("SIST0001");
        } finally {
            if (conn.hayConexionAbierta()) {
                conn.cierraConexionDB();
            }
            LOG.info("SupervisionBean::validaExtemporaneo(E)");
        }
        return resultadoG;
    }
    
    
    private String nombreEjecutivoPrincipal(String claveSiag, String tipoUsuario, String claveIF, String csPrincipal) throws NafinException {
        LOG.info("nombreEjecutivoPrincipal (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder nombre = new StringBuilder();
        UtilUsr utilUsr = new UtilUsr();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" select IC_CLIENTE " + " from  sup_datos_if " + " where ic_cve_siag = ? " +
                       " and  IC_TIPO = ?   " + " and  ic_if = ?  and cs_principal = ? ");

            lVarBind = new ArrayList();
            lVarBind.add(claveSiag);
            lVarBind.add(tipoUsuario);
            lVarBind.add(claveIF);
            lVarBind.add(csPrincipal);
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                String cliente = rs.getString("IC_CLIENTE") == null ? "" : rs.getString("IC_CLIENTE");
                Usuario usuario = utilUsr.getUsuario(cliente);
                String nombreU =
                    usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
                nombre.append(nombre.length() > 0 ? ", " : "").append(nombreU);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en nombreEjecutivoPrincipal. " + e);
            throw new AppException("Error nombreEjecutivoPrincipal   ");
        } finally {
            LOG.info("nombreEjecutivo (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return nombre.toString();

    }
    
    private String obtenerFechaSupAcuse(String numfolio, String icif, String ic_usuario) throws NafinException {
                   LOG.info("obtenerFechaSupAcuse (e)");

                   AccesoDB con = new AccesoDB();
                   StringBuilder qry = new StringBuilder();
                   List lVarBind = new ArrayList();
                   PreparedStatement ps = null;
                   ResultSet rs = null;
                   String fechaAcuse = "";

                   try {
                       con.conexionDB();

                       qry = new StringBuilder();
                       qry.append(" SELECT\n" + 
                       "    ac_fecha_acuse\n" + 
                       "FROM\n" + 
                       "    sup_archivos_carga_if   sac,\n" + 
                       "    sup_acuse               sa\n" + 
                       "WHERE\n" + 
                       "    sa.cc_acuse = sac.cc_acuse\n" + 
                       "    AND sa.ic_if = sac.ic_if\n" + 
                       "    AND sa.ic_usuario = sac.ic_usuario\n" + 
                       "    AND sa.cc_acuse = ? \n" + 
                       "    AND sa.ic_if = ? \n" + 
                       "    AND sa.ic_usuario = ? ");


                       lVarBind = new ArrayList();
                       lVarBind.add(numfolio);
                       lVarBind.add(icif);
                       lVarBind.add(ic_usuario);

                       //LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);

                       ps = con.queryPrecompilado(qry.toString(), lVarBind);
                       rs = ps.executeQuery();
                       while (rs.next()) {
                           fechaAcuse = rs.getString("ac_fecha_acuse");               
                       }
                       rs.close();
                       ps.close();


                   } catch (Exception e) {
                       LOG.error("Exception en obtenerFechaSupAcuse. " + e);
                       throw new AppException("Error obtenerFechaSupAcuse   ");
                   } finally {
                       LOG.info("obtenerFechaSupAcuse (S)");
                       if (con.hayConexionAbierta()) {
                           con.cierraConexionDB();
                       }
                   }
                   return fechaAcuse;

               }

/*Metodo que implementa el email de la alerta previo al vencimiento de la entrega de expedientes
* de los intermediarios financieros
* autor =  julio morales
* */
        private void alertaVencimientoEEIF(String ruta){
        LOG.info("alertaVencimientoEEIF (e)");
        
        List<String> diasH = new ArrayList<>();
       
        String calFechaLimE = "";
        String calFechaLimA = "";                       
        String calconse = "";
        String icsiag = "";
        String calMes = "";
        String calAnio = "";        
        int numP = obtenerDiasPrevNoti();       
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        Date date = c.getTime();
        Date diaD = new Date();
        String diaH = sdf.format(diaD);
        String diaA = sdf.format(date);        
        
        String nm = Fecha.sumaFechaDiasHabiles(diaA, "dd/MM/yyyy", 1);            
        LOG.info("Es dia Habil Hoy : "+diaH.equals(nm));
        if(diaH.equals(nm)){ 
            diasH.add(diaH);
            
            for(int j = 1; j <= numP; j++){                
               diasH.add(Fecha.sumaFechaDiasHabilesPorAno(diasH.get(diasH.size()-1), "dd/MM/yyyy", 1));
            }
            
            for(int j = 0; j < diasH.size(); j++){
                List<Map> diasHabiles = obtenerFechasHabiles(diasH.get(j));
                
                for(int k = 0; k < diasHabiles.size(); k++){
                     Map<String, String> fechasNotificacion = diasHabiles.get(k);
                    icsiag = fechasNotificacion.get("IC_SIAG").toString();
                    calconse = fechasNotificacion.get("CAL_CONSECUTIVO").toString();                    
                    calMes = Fecha.getNombreDelMes(Integer.parseInt(fechasNotificacion.get("CAL_MES")));
                    calAnio = fechasNotificacion.get("CAL_ANIO").toString();
                    calFechaLimE = fechasNotificacion.get("CalFechaLimiEntE").toString();
                    calFechaLimA = fechasNotificacion.get("CalFechaLimiEntA").toString();  
                    
                    int num = Fecha.restaFechas(diaH, calFechaLimE);
                    
                    if(num >= 0){                        
                        armadoEmailPVEEIF(ruta, calFechaLimE, diaH, calMes, calAnio, icsiag);
                        actualizarBanderaVencimientoEEIF(diaH, calconse, icsiag); 
                    }else{
                        
                        armadoEmailPVEEIF(ruta, calFechaLimA, diaH, calMes, calAnio, icsiag);
                        actualizarBanderaVencimientoEEIF(diaH, calconse, icsiag); 
                    }
                }
            }
        }else{            
            LOG.info("Hoy es un Dia inhabil ");            
        }
        LOG.info("Aqui actualiza las banderas a No enviado, del dia siguiente");
        for(int m = 0; m < diasH.size(); m++){
            actualizarBanderaVencimientoEEIFS(diasH.get(0), diasH.get(m));
        }        
        LOG.info("alertaVencimientoEEIF (e)");        
    }
   
    private int obtenerDiasPrevNoti() {
            
            AccesoDB con = new AccesoDB();
            StringBuilder qry = new StringBuilder();
            List lVarBind = new ArrayList();
            PreparedStatement ps = null;
            ResultSet rs = null;
            int num  = 0;           

            try {
                con.conexionDB();               

                qry = new StringBuilder();
                qry.append(" select valor from sup_parametros_proceso where param_gral =? ");

                lVarBind = new ArrayList();
                lVarBind.add("DIAS_PREV_NOTI");                
                ps = con.queryPrecompilado(qry.toString(), lVarBind);
                rs = ps.executeQuery();
                if (rs.next()) {
                    num = rs.getInt("valor");                    
                }
                
                rs.close();
                ps.close();
            } catch (Exception e) {
               
                throw new AppException("Error al obtener el parametro de la tabla sup_parametros_proceso (registrarActualizarFecha) "+e);
            } finally {
                
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return num;
        }
    
   
   private List<Map> obtenerFechasHabiles(String dia){
       LOG.info("obtenerFechasHabiles (e)");
       List<Map> listFechas = new ArrayList<>();
       AccesoDB con = new AccesoDB();
       StringBuilder qry = new StringBuilder();
       List lVarBind = new ArrayList();       
       PreparedStatement ps = null;
       ResultSet rs = null;       
       Map<String, String> fechas = new HashMap<>();     
       

       try {
           con.conexionDB();

           qry = new StringBuilder();
           qry.append("select \n" + 
           "        ic_cve_siag,\n" + 
           "        cal_consecutivo, \n" + 
           "        cal_mes, \n" + 
           "        cal_anio,         \n" + 
           "        TO_CHAR(cal_fec_lim_ent_e,'dd/mm/yyyy') as cal_fec_lim_ent_e,          \n" + 
           "        TO_CHAR(cal_fec_lim_ent_a, 'dd/mm/yyyy') as cal_fec_lim_ent_a        \n" +                     
           "        from sup_calendario_if\n" + 
           "        where noti_venc_entre_exp = ? \n" + 
           "            AND    \n" + 
           "                (" +
           "                    (cal_fec_lim_ent_e = TO_DATE(?, 'DD/MM/YYYY')) \n" + 
           "                OR\n" + 
           "                    (cal_fec_lim_ent_a = TO_DATE(?, 'DD/MM/YYYY'))" +
           "                )");

           lVarBind = new ArrayList();
           lVarBind.add("N");
           lVarBind.add(dia);           
           lVarBind.add(dia);    
           
           ps = con.queryPrecompilado(qry.toString(), lVarBind);
           rs = ps.executeQuery();           
          
           while (rs.next()) {
               fechas = new HashMap<>();
               fechas.put("IC_SIAG", rs.getString("ic_cve_siag"));
               fechas.put("CAL_CONSECUTIVO", rs.getString("cal_consecutivo"));
               fechas.put("CAL_MES", rs.getString("cal_mes"));
               fechas.put("CAL_ANIO", rs.getString("cal_anio"));               
               fechas.put("CalFechaLimiEntE", rs.getString("cal_fec_lim_ent_e"));               
               fechas.put("CalFechaLimiEntA", rs.getString("cal_fec_lim_ent_a"));               
               
               listFechas.add(fechas);               
           }   
           
           rs.close();
           ps.close();

       } catch (Exception e) {
           LOG.error("Exception en obtenerFechasHabiles. " + e);
           throw new AppException("Error obtenerFechasHabiles   "+e);
       } finally {
           LOG.info("obtenerFechasHabiles (S)");
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }
       }       
       return listFechas;           
   }     

    /*Metodo que arma el email para la alerta previo al vencimiento de la entrega de expedientes
* de los intermediarios financieros
    * */
    private void armadoEmailPVEEIF(String ruta, String fechaE, String fechaH, String calMes,
                                         String cal_anio, String siag) {
              LOG.info("armadoEmailPVEEIF (e)");
              try {
                  Correo correo = new Correo();
                  String remitente = "no_response@nafin.gob.mx";
                  String asunto = "Aviso de Vencimiento de Entrega de Expedientes ";
                  StringBuilder msg = new StringBuilder();
                  StringBuilder emailContactoIFCC = new StringBuilder();
                  ArrayList info = new ArrayList();
                  ArrayList contac = new ArrayList();
                  
                  ArrayList ejecutivos = new ArrayList();
                  StringBuilder emailEjecutivoCC = new StringBuilder();

                  info = this.listaIFs("C", siag); //Obtengo la lista de IF�s   con su clave de siag
                  
                  for (int i = 0; i < info.size(); i++) {                      

                      HashMap datCE = (HashMap) info.get(i);
                      String nombreIF = datCE.get("NOMBREIF").toString();
                      
                      String claveIF = datCE.get("ICIF").toString();

                      emailContactoIFCC = new StringBuilder();
                      String emailContactoIF = "";
                      String nombreContactoIF = "";
                      String loginEjecutivo = "";
                      String nombreEjecutivo = "";

                      //Obtenego los Contacto del IF con el Banco
                      contac = new ArrayList();
                      contac = contactosyEjecutivos(siag, "C", claveIF);
                      
                      for (int c = 0; c < contac.size(); c++) {

                          HashMap datcontac = (HashMap) contac.get(c);
                          String principal = datcontac.get("PRINCIPAL").toString();
                          if ("S".equals(principal)) {
                              nombreContactoIF = datcontac.get("NOMBRE").toString();
                              emailContactoIF = datcontac.get("EMAILS").toString();
                          } else if ("N".equals(principal)) {
                              emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                                       " ").append(datcontac.get("EMAILS").toString());
                          }
                      } // for (int c = 0; c <contac.size(); c++)

                      //obtengo el Ejecutivo principal para insertar en bitacora
                      ejecutivos = new ArrayList();
                      ejecutivos = contactosyEjecutivos(siag, "E", claveIF);
                      
                      emailEjecutivoCC = new StringBuilder();
                      for (int e = 0; e < ejecutivos.size(); e++) {
                          HashMap datcontac = (HashMap) ejecutivos.get(e);
                          String principal = datcontac.get("PRINCIPAL").toString();
                          emailEjecutivoCC.append(emailEjecutivoCC.length() > 0 ? ", " :
                                                  "").append(datcontac.get("EMAILS").toString());
                          if ("S".equals(principal)) {
                              loginEjecutivo = datcontac.get("LOGIN").toString();
                              nombreEjecutivo = datcontac.get("NOMBRE").toString();
                          }
                      } // for (int c = 0; c <contac.size(); c++)

                      //  se obtienen los correos del ADMIN SUPGARANT
                      UtilUsr utilUsr = new UtilUsr();
                      List lista = utilUsr.getUsuariosxAfiliado("ADMIN SUPGARANT", "N");
                      
                      StringBuilder emailAdminSUP = new StringBuilder();
                      

                      for (int j = 0; j < lista.size(); j++) {
                          String loginUsuario = (String) lista.get(i);
                          if (loginUsuario != null) {
                              Usuario admin = utilUsr.getUsuario(loginUsuario);
                              String email = admin.getEmail();
                              emailAdminSUP.append(emailAdminSUP.length() > 0 ? ", " : " ").append(email);
                          }
                      } //for(int j = 0; j < lista.size(); j++)

                      //  se agregan a los correos de copia del banco los de Ejecutivos Nafin
                      emailContactoIFCC.append(emailContactoIFCC.length() > 0 ? ", " :
                                               " ").append(emailEjecutivoCC.toString()).append(emailAdminSUP.toString());
                      

                      msg = new StringBuilder();
                      msg.append(" <table width=\"550px\" border=\"0\"> ");
                      msg.append("<tr><center><img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> </center></tr>");

                      msg.append("<tr>");
                      msg.append("<br>   <b>" + nombreContactoIF +
                                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " +
                                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha: " +
                                 fechaH + " </b>");
                      msg.append("<br>   <b>" + nombreIF + "</b>");
                      msg.append("<br>   ");
                      msg.append("<br>   ");
                      msg.append(" Se le recuerda que la fecha l�mite para la entrega de expedientes de supervisi�n o de aclaraciones correspondiente al mes de " +
                                 calMes.toLowerCase() + " de " + cal_anio + " vence el pr�ximo: " + fechaE + "<br>");
                      msg.append("<br>Si ya realiz� la carga, hacer caso omiso a este comunicado.");
                      msg.append("<br>   ");
                      msg.append("<br>   ");
                      msg.append("Nota de seguridad: Nacional Financiera, S.N.C. jam�s solicitara tu contrase�a por correo electr�nico");
                      msg.append("<br>   ");
                      msg.append("<br>   ");
                      msg.append("<center><img src=\"cid:nafin_id2\" height=\"85\" width=\"475\" alt=\"\" border=\"0\"> </center>");
                      msg.append("</tr>");
                      msg.append("</table> ");
							
								

                      String rutaImagen = ruta + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif";
                      String rutaImagen2 = ruta + "00archivos/15cadenas/15archcadenas/logos/bg_footer.jpg";
							

                      ArrayList listaDeImagenes = new ArrayList();
                      HashMap imagenNafin = new HashMap();
                      imagenNafin = new HashMap();
                      imagenNafin.put("FILE_FULL_PATH", rutaImagen);
                      imagenNafin.put("FILE_ID", "nafin_id");
                      listaDeImagenes.add(imagenNafin);

                      imagenNafin = new HashMap();
                      imagenNafin.put("FILE_FULL_PATH", rutaImagen2);
                      imagenNafin.put("FILE_ID", "nafin_id2");

                      listaDeImagenes.add(imagenNafin);                  
                     
                      try {                              
                          correo.setCodificacion("charset=UTF-8");
                          correo.enviaCorreoConDatosAdjuntos(remitente, emailContactoIF, emailContactoIFCC.toString(),
                                                             asunto, msg.toString(), listaDeImagenes, null);                                                              

                      } catch (Exception t) {
                          LOG.error("Error al enviar correo armadoEmailPVEEIF" + t);
                          throw new AppException("Error al enviar  armadoEmailPVEEIF ", t);
                      }

                  } //for (int i = 0; i <=info.size(); i++)

              } catch (Exception t) {                  
                  throw new AppException("Error al enviar  armadoEmailPVEEIF ", t);
                 
              } finally {
                  LOG.info("armadoEmailPVEEIF (s)");
              }             
             
              
          }
    /*
     * M�todo que actualiza la bandera de la columna NOTI_VENC_ENTRE_EXP de la table sup_calendario_if
     * al momento de enviar los correos electr�nicos
     * */
    private void actualizarBanderaVencimientoEEIFS(String fechaH, String fechas) {
        LOG.info("actualizarBanderaVencimientoEEIFS (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;          
        
        boolean exito = false;
        try {

            con.conexionDB();
            qry = new StringBuilder();
            qry.append("update sup_calendario_if set noti_venc_entre_exp = ? " +
                "where fech_notif_band != TO_DATE(?, 'DD/MM/YYYY') " +
                "and " +
                "(cal_fec_lim_ent_e = TO_DATE(?, 'DD/MM/YYYY') or cal_fec_lim_ent_a = TO_DATE(?, 'DD/MM/YYYY'))");            

            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(fechaH);
            lVarBind.add(fechas);
            lVarBind.add(fechas);

            LOG.debug("########## Actualizacion tabla sup_calendario_if  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();
            
            exito = true;
        } catch (Exception e) {
            LOG.error("Exception en actualizarBanderaVencimientoEEIFS. " + e);
            throw new AppException("Error actualizarBanderaVencimientoEEIFS   "+e);
        } finally {
                con.terminaTransaccion(exito);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
        }
        LOG.info("actualizarBanderaVencimientoEEIFS (S)");
    }
    
    private void actualizarBanderaVencimientoEEIF(String fechaN, String calConse, String icsiag) {
        LOG.info("actualizarBanderaVencimientoEEIF (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;       
        
        boolean exito = false;
        try {

            con.conexionDB();
            qry = new StringBuilder();
            qry.append(" update sup_calendario_if set noti_venc_entre_exp = ?, fech_notif_band = TO_DATE(?, 'DD/MM/YYYY')  " +
                       " where cal_consecutivo = ?  AND ic_cve_siag =? ");

            lVarBind = new ArrayList();
            lVarBind.add("S");
            lVarBind.add(fechaN);
            lVarBind.add(calConse);
            lVarBind.add(icsiag);         

            LOG.debug("########## Actualizacion tabla sup_calendario_if  " + qry + " \n  lVarBind  " + lVarBind);
    
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();

            exito = true;
        } catch (Exception e) {
            LOG.error("Exception en actualizarBanderaVencimientoEEIF. " + e);
            throw new AppException("Error actualizarBanderaVencimientoEEIF   "+e);
        } finally {
                con.terminaTransaccion(exito);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
        }
        LOG.info("actualizarBanderaVencimientoEEIF (S)");
    }
    
    
    private Map obtenerFechasLE(String id, String mes, String anio) {
            
            AccesoDB con = new AccesoDB();
            StringBuilder qry = new StringBuilder();
            List lVarBind = new ArrayList();
            PreparedStatement ps = null;
            ResultSet rs = null;
            
            Map<String, Object> informacion = new HashMap<>();
            

            try {
                con.conexionDB();
                

                qry = new StringBuilder();
                    qry.append(" select bit_fec_lim_ent_exp, bit_fec_ent_exp, bit_fec_lim_ent_inf_acl " +
                        "from bit_supervision " +
                        "where bit_ejecutivo_login = ? and cal_mes = ? and cal_anio = ?");

                lVarBind = new ArrayList();
                lVarBind.add(id);
                lVarBind.add(mes);
                lVarBind.add(anio);
                
                ps = con.queryPrecompilado(qry.toString(), lVarBind);
                rs = ps.executeQuery();
                while (rs.next()) {
                    informacion = new HashMap<>();

                    informacion.put("bit_fec_lim_ent_exp", rs.getString("bit_fec_lim_ent_exp"));
                    informacion.put("bit_fec_ent_exp", rs.getString("bit_fec_ent_exp"));
                    informacion.put("bit_fec_lim_ent_inf_acl", rs.getString("bit_fec_lim_ent_inf_acl"));                
                    
                }
                rs.close();
                ps.close();


            } catch (Exception e) {
               
                throw new AppException(" Error registrarActualizarFecha "+e);
            } finally {
                
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
            return informacion;

        }

    public String validaTipoDeCalendarioACorregir(Integer icCveSiag, Integer calMes, Integer calAnio) {
        LOG.info("validaTipoDeCalendarioACorregir (E)");

        String           tipoCal   = "";
        AccesoDB          con      = new AccesoDB();
        StringBuilder     qry      = new StringBuilder();
        List              lVarBind = new ArrayList();
        PreparedStatement ps       = null;
        ResultSet         rs       = null;

        try {
            con.conexionDB();

            qry.append("SELECT CAL_TIPO \n" + 
            "FROM SUP_CALENDARIO_IF  \n" + 
            "WHERE IC_CVE_SIAG =? AND CAL_CONSECUTIVO = (SELECT MAX(CAL_CONSECUTIVO)\n" + 
            "FROM SUP_CALENDARIO_IF\n" + 
            "WHERE CAL_ESTATUS='A'\n" + 
            "AND CC_ACUSE IS NOT NULL\n" + 
            "AND CAL_TIPO<>'C' \n" + 
            "AND CAL_MES=? \n" + 
            "AND CAL_ANIO=?\n" + 
            "AND IC_CVE_SIAG =?)");

            lVarBind = new ArrayList();
            lVarBind.add(icCveSiag);
            lVarBind.add(calMes);
            lVarBind.add(calAnio);
            lVarBind.add(icCveSiag);

            LOG.debug ("qry  "+qry+" \n  lVarBind  "+lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                tipoCal = rs.getString("CAL_TIPO");               
            }
            rs.close();
            ps.close();
            //El siguiente if es por si a caso el dato es nulo.
            if(null==tipoCal){
                tipoCal = "";
            }
        } catch (Exception e) {
            LOG.error("Exception en validaTipoDeCalendarioACorregir. " + e);
            throw new AppException("Error validaTipoDeCalendarioACorregir "+e);
        } finally {
            LOG.info("validaTipoDeCalendarioACorregir(S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return tipoCal;
    }


   

    @Override
    public String obtenerCalConsecutivo(String icCveSiag, String calMes, String calAnio, String caltipo
                                        ) throws NafinException {
        LOG.info("obtenerCalConsecutivo (e)");

               AccesoDB con = new AccesoDB();
               StringBuilder qry = new StringBuilder();
               List lVarBind = new ArrayList();
               PreparedStatement ps = null;
               ResultSet rs = null;               
               String consecutivo = "";               
               
               try {
                   con.conexionDB();

                   qry = new StringBuilder();
                   qry.append(" select cal_consecutivo from  sup_calendario_if " + " where ic_cve_siag = ? " +
                              " and  cal_mes = ?  and  cal_anio = ? and cal_tipo = ?");

                   lVarBind = new ArrayList();
                   lVarBind.add(icCveSiag);
                   lVarBind.add(calMes);
                   lVarBind.add(calAnio);
                   lVarBind.add(caltipo);
                   
                   LOG.debug ("qry (obtenerCalConsecutivo) "+qry+" \n  lVarBind  "+lVarBind);
                   ps = con.queryPrecompilado(qry.toString(), lVarBind);
                   rs = ps.executeQuery();
                   if (rs.next()) { 
                       consecutivo = rs.getString("cal_consecutivo");

                   }
                   rs.close();
                   ps.close();


               } catch (Exception e) {
                   LOG.error("Exception en obtenerCalConsecutivo. " + e);
                   throw new AppException("Error obtenerCalConsecutivo   "+e);
               } finally {
                   LOG.info("contactosyEjecutivos (S)");
                   if (con.hayConexionAbierta()) {
                       con.cierraConexionDB();
                   }
               }
               return consecutivo;
    }

    @Override
    public String obtenerNumGarantias(String mes, String anio, String claveSiag, String tipo) throws NafinException {
        String  consecutivo = obtenerCalConsecutivo(claveSiag, mes, anio, tipo);
        
        String numG = numgarantias(mes, anio, claveSiag).get("totalGarantias").toString();
        String totG = totalgarantias(claveSiag, consecutivo);        
        
        return numG + " - "+totG;
    }
    
    @Override
    public String obtenerTotalGarantias(String mes, String anio, String claveSiag, String tipo) throws NafinException {
        String  consecutivo = obtenerCalConsecutivo(claveSiag, mes, anio, tipo);                
        
        return totalgarantias(claveSiag, consecutivo);
    }


    
    
    private ArrayList listaIFs(String tipoC, String siag) throws NafinException {

        LOG.info("listaIFs (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, Object> informacion = new HashMap<>();
        ArrayList respuesta = new ArrayList();

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append(" select distinct IC_CVE_SIAG  as CVE_SIAG , " + " ie.IC_IF  AS ICIF,  " +
                       " ie.CG_RAZON_SOCIAL as NOMBRE_IF  " + " from sup_datos_if i,  comcat_if ie  " +
                       " where  ie.IC_IF = i.IC_IF " + " AND i.IC_TIPO = ? " + " and i.IC_CVE_SIAG = ? ");

            lVarBind = new ArrayList();
            lVarBind.add(tipoC);
            lVarBind.add(siag);
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = new HashMap<>();
                informacion.put("CVESIAG", rs.getString("CVE_SIAG") == null ? "" : rs.getString("CVE_SIAG"));
                informacion.put("ICIF", rs.getString("ICIF") == null ? "" : rs.getString("ICIF"));
                informacion.put("NOMBREIF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                respuesta.add(informacion);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en listaIFs. " + e);
            throw new AppException("Error listaIFs   "+e);
        } finally {
            LOG.info("listaIFs (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;

    }

    @Override
    public boolean updateCalendarioCorreccion(String MES, String ANIO, String TIPO_CAL, String acuse, String usuario,
                                    String noArchivos) throws NafinException {
            
        LOG.info("SupervisionBean::updateCalendarioCorreccion(E)");
        AccesoDB con = new AccesoDB();
        String qrySentencia = "";
        boolean lbOK = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Integer cveSIAG = 0;
        Integer consecutivo = 0;
        List lVarBind = new ArrayList();
        try {
            con.conexionDB();

            //Paso 1. Obtengo IC_CVE_SIAG y el consecutivo que voy a actualizar
            qrySentencia = "SELECT MAX(CAL_CONSECUTIVO) CAL_CONSECUTIVO, IC_CVE_SIAG\n" + 
                            "FROM SUP_CALENDARIO_IF \n" + 
                            "WHERE CAL_ESTATUS='A'\n" + 
                            "AND CAL_TIPO<>'C'\n" + 
                            "AND CAL_MES=?\n" + 
                            "AND CAL_ANIO=?\n" + 
                            "AND IC_CVE_SIAG = (SELECT IC_CVE_SIAG FROM SUP_CALENDARIO_IF WHERE CAL_MES=? AND CAL_ANIO=? AND CAL_TIPO='C' AND CAL_ESTATUS='P')\n" + 
                            "GROUP BY IC_CVE_SIAG";
            lVarBind.add(MES);
            lVarBind.add(ANIO);
            lVarBind.add(MES);
            lVarBind.add(ANIO);
            LOG.info("SupervisionBean::Paso 1 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) { 
                cveSIAG = rs.getInt("IC_CVE_SIAG");
                consecutivo = rs.getInt("CAL_CONSECUTIVO");
            }
            rs.close();
            ps.close();
            
            //Paso 2. Obtengo los datos del registro de correccion
            Integer calConsecutivo=null;
            String calFecLimEntE=null;
            String calFecLimEntA=null;
            String calFecLimEntD=null;
            String calNoAutoriza=null;
            String calFechaAct=null;
            String calFecLimEntR=null;
            qrySentencia = "SELECT\n" + 
                            "CAL_CONSECUTIVO\n" + 
                            ",TO_CHAR(CAL_FEC_LIM_ENT_E,'DD/MM/YYYY HH24:MI:SS') CAL_FEC_LIM_ENT_E\n" + 
                            ",TO_CHAR(CAL_FEC_LIM_ENT_A,'DD/MM/YYYY HH24:MI:SS') CAL_FEC_LIM_ENT_A\n" + 
                            ",TO_CHAR(CAL_FEC_LIM_ENT_D,'DD/MM/YYYY HH24:MI:SS') CAL_FEC_LIM_ENT_D\n" + 
                            ",CAL_NO_AUTORIZA\n" + 
                            ",TO_CHAR(CAL_FECHA_ACT,'DD/MM/YYYY HH24:MI:SS') CAL_FECHA_ACT\n" + 
                            ",TO_CHAR(CAL_FEC_LIM_ENT_R,'DD/MM/YYYY HH24:MI:SS') CAL_FEC_LIM_ENT_R\n" + 
                            "FROM SUP_CALENDARIO_IF\n" + 
                            "WHERE CAL_MES=?\n" + 
                            "AND CAL_ANIO=?\n" + 
                            "AND CAL_TIPO='C'\n" + 
                            "AND CAL_ESTATUS='P'";
            lVarBind = new ArrayList();
            lVarBind.add(MES);
            lVarBind.add(ANIO);
            LOG.info("SupervisionBean::Paso 2 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) { 
                calConsecutivo = rs.getInt("CAL_CONSECUTIVO");
                calFecLimEntE = rs.getString("CAL_FEC_LIM_ENT_E");
                calFecLimEntA = rs.getString("CAL_FEC_LIM_ENT_A");
                calFecLimEntD = rs.getString("CAL_FEC_LIM_ENT_D");
                calNoAutoriza = rs.getString("CAL_NO_AUTORIZA");
                calFechaAct = rs.getString("CAL_FECHA_ACT");
                calFecLimEntR = rs.getString("CAL_FEC_LIM_ENT_R");
            }
            rs.close();
            ps.close();

            //Paso 3. Inserto los datos del registro de correcci�n en el registro del calendario a corregir
            qrySentencia =
                "UPDATE SUP_CALENDARIO_IF SET\n" + 
                "CAL_FEC_LIM_ENT_E=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n" + 
                ",CAL_FEC_LIM_ENT_A=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n" + 
                ",CAL_FEC_LIM_ENT_D=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n" + 
                ",CC_ACUSE=? \n" ; 
                //",CAL_NO_AUTORIZA=? \n" ;
            if(null != calNoAutoriza){
                qrySentencia += ",CAL_NO_AUTORIZA=? \n";
            }
                qrySentencia+=",CAL_FECHA_ACT=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n"; 
            if(null!=calFecLimEntR) {
                qrySentencia += ",CAL_FEC_LIM_ENT_R=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n";
            }
                qrySentencia += "WHERE CAL_CONSECUTIVO=? \n" + 
                "AND IC_CVE_SIAG=?\n" + 
                "AND CAL_ESTATUS='A'";
            lVarBind = new ArrayList();
            lVarBind.add(calFecLimEntE);
            lVarBind.add(calFecLimEntA);
            lVarBind.add(calFecLimEntD);
            lVarBind.add(acuse);
            //lVarBind.add(calNoAutoriza);
            if(null!=calNoAutoriza) {
                lVarBind.add(calNoAutoriza);
            }
            lVarBind.add(calFechaAct);
            if(null!=calFecLimEntR) {
                lVarBind.add(calFecLimEntR);
            }
            lVarBind.add(consecutivo);
            lVarBind.add(cveSIAG);
            LOG.info("SupervisionBean::Paso 3 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            ps.executeUpdate();
            ps.close();
            
            //Paso 3.1 Actualzar Fecha de Entrega de Expediente y de Aclaraciones  y de Dictamen en la Bitacora
            qrySentencia =
                "UPDATE BIT_SUPERVISION SET\n" + 
                "BIT_FEC_LIM_ENT_EXP=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') \n" + 
                ",BIT_FEC_LIM_ENT_INF_ACL=TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') WHERE \n" +                 
                " CAL_MES=? \n" + 
                " AND CAL_ANIO=? \n" + 
                " AND IC_IF = (SELECT DISTINCT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?)\n"+ 
                " AND BIT_TIPO = (SELECT CAL_TIPO FROM SUP_CALENDARIO_IF WHERE CAL_CONSECUTIVO = ? AND IC_CVE_SIAG = ? AND CAL_ESTATUS = ?) \n";             
                
            lVarBind = new ArrayList();
            lVarBind.add(calFecLimEntE);
            lVarBind.add(calFecLimEntA);
            lVarBind.add(MES);
            lVarBind.add(ANIO);
            lVarBind.add(cveSIAG);
            lVarBind.add(consecutivo);
            lVarBind.add(cveSIAG);
            lVarBind.add("A");
            
            LOG.info("SupervisionBean::Paso 3.1 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            ps.executeUpdate();
            ps.close();
            
            
            

            //Paso 4. Elimino el detalle del registro actualizado
            qrySentencia = "DELETE FROM SUP_CALENDARIO_DETALLE WHERE IC_CVE_SIAG = ? AND  CAL_CONSECUTIVO = ?";
            lVarBind = new ArrayList();
            lVarBind.add(cveSIAG);
            lVarBind.add(consecutivo);
            LOG.info("SupervisionBean::Paso 4 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            ps.executeUpdate();
            ps.close();
            
            //Paso 5. Obtengo las garantias que insert� del archivo de correcci�n
            List<String> lista = new ArrayList<>();
            qrySentencia = "SELECT CAL_CONSECUTIVO,IC_CVE_SIAG,CAL_REL_GARANTIAS " +
                            "FROM SUP_CALENDARIO_DETALLE WHERE CAL_CONSECUTIVO=? AND IC_CVE_SIAG=? " +
                            "ORDER BY CAL_DET_CONSECUTIVO ASC";
            lVarBind = new ArrayList();
            lVarBind.add(calConsecutivo);
            lVarBind.add(cveSIAG);
            LOG.info("SupervisionBean::Paso 5 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("CAL_REL_GARANTIAS"));
            }
            rs.close();
            ps.close();
            
            //Paso 6. Inserto las garantias con el consecutivo y cve_siag del calendario que estoy actualizando
            if(!lista.isEmpty()) {
                Integer calConseDet = null;
                for(String s: lista) {
                    qrySentencia = "select nvl(MAX(CAL_DET_CONSECUTIVO),0)+1" + " from SUP_CALENDARIO_DETALLE " +
                                    " where IC_CVE_SIAG=? AND CAL_CONSECUTIVO=?";
                    lVarBind = new ArrayList();
                    lVarBind.add(cveSIAG);
                    lVarBind.add(consecutivo);
                    ps = con.queryPrecompilado(qrySentencia, lVarBind);
                    rs = ps.executeQuery();
                    if (rs.next())
                        calConseDet = rs.getInt(1);
                    rs.close();
                    ps.close();
                    
                    qrySentencia = "INSERT INTO SUP_CALENDARIO_DETALLE (CAL_DET_CONSECUTIVO, CAL_CONSECUTIVO, IC_CVE_SIAG, CAL_REL_GARANTIAS )"+
                        "VALUES (?,?,?,?)";
                    lVarBind = new ArrayList();
                    lVarBind.add(calConseDet);
                    lVarBind.add(consecutivo);
                    lVarBind.add(cveSIAG);
                    lVarBind.add(s);
            LOG.info("SupervisionBean::Paso 6 " + qrySentencia + "\n" + lVarBind);
                    ps = con.queryPrecompilado(qrySentencia, lVarBind);
                    ps.executeUpdate();
                    ps.close();
                }
            }

            creaAcuseCal(acuse, usuario, noArchivos, TIPO_CAL, MES, ANIO, con);

            //Paso 8. Elimino el detalle del calendario de correccion
            qrySentencia = "DELETE FROM SUP_CALENDARIO_DETALLE WHERE IC_CVE_SIAG = ? AND  CAL_CONSECUTIVO = ?";
            lVarBind = new ArrayList();
            lVarBind.add(cveSIAG);
            lVarBind.add(calConsecutivo);
            LOG.info("SupervisionBean::Paso 8 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            ps.executeUpdate();
            ps.close();
            
            //Paso 9. Elimino el calendario de correccion
            qrySentencia = "DELETE FROM SUP_CALENDARIO_IF WHERE IC_CVE_SIAG = ? AND  CAL_CONSECUTIVO = ? AND CAL_TIPO='C'";
            lVarBind = new ArrayList();
            lVarBind.add(cveSIAG);
            lVarBind.add(calConsecutivo);

            LOG.info("SupervisionBean::Paso 9 " + qrySentencia + "\n" + lVarBind);
            ps = con.queryPrecompilado(qrySentencia, lVarBind);
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception e) {
            LOG.error("SupervisionBean::updateCalendarioCorreccion(Exception)");
            e.printStackTrace();
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("SupervisionBean::updateCalendarioCorreccion(S)");
        }
        return true;


    }

    
    private String obtenerClave(String iNoCliente) {
        LOG.info("obtenerClave (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String icif = "";

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append("select ic_if from sup_datos_if where ic_cliente = ?");

            lVarBind = new ArrayList();
            lVarBind.add(iNoCliente);
            
            //LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                //rs.getString("bit_fec_ent_exp") == null ? "" : rs.getString("bit_fec_ent_exp")
                icif = rs.getString("ic_if") == null ? "vacio" :rs.getString("ic_if");
                
            }
            
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en obtenerClave. " + e);
            throw new AppException("Error obtenerClave   "+e);
        } finally {
            LOG.info("obtenerClave (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return icif;
    }

    @Override
    public void adecuarBitacoraSupervision(String mes, String anio, String icif, AccesoDB con) {    
        LOG.info("adecuarBitacoraSupervision (e)");
        List lista = obtenerTipo(mes, anio, icif);
        
        for(int i = 0; i < lista.size(); i++){
            HashMap dat = (HashMap) lista.get(i);
            String tipo = dat.get("bit_tipo").toString();
            String bitFecLimEntExp = dat.get("bit_fec_lim_ent_exp").toString();
            String bitFecLimEntInfAcl = dat.get("bit_fec_lim_ent_inf_acl").toString();
            String fechaHoy = dat.get("FechaHoy").toString();
            
            int num = Fecha.restaFechas(fechaHoy, bitFecLimEntExp);
            int num2 = Fecha.restaFechas(fechaHoy, bitFecLimEntInfAcl);
            LOG.info("Fecha Hoy: "+num+",  (fechaHoy - bitFecLimEntExp): "+num+",  (fechaHoy - bitFecLimEntExp): "+num2+" --- (bitFecLimEntExp): "+bitFecLimEntExp+" ---(bitFecLimEntInfAcl): "+bitFecLimEntInfAcl+"  - Tipo "+tipo);
            if("O".equals(tipo)){                
                if(num >= 0){
                    LOG.info("Cumple con la condicion - (bitFecLimEntExp)");
                    actualizarFecEntExp(mes, anio, icif, fechaHoy, tipo, con);
                }else if( num2  >= 0 ){
                    LOG.info("Cumple con la condicion - (bitFecLimEntInfAcl)");
                    actualizarFecExpAcl(mes, anio, icif, fechaHoy, tipo, con);
                }
                
            }else if("E".equals(tipo)){
                if(num >= 0){
                    LOG.info("Cumple con la condicion - (bitFecLimEntExp)");
                    actualizarFecEntExp(mes, anio, icif, fechaHoy, tipo, con);
                }else if( num2  >= 0 ){
                    LOG.info("Cumple con la condicion - (bitFecLimEntInfAcl)");
                    actualizarFecExpAcl(mes, anio, icif, fechaHoy, tipo, con);
                }
                
            }
        }
        
        LOG.info("adecuarBitacoraSupervision (s)");

    }

     private Map consultaFechas(String mes, String anio, String icif) {
        LOG.info("consultaFechas (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, String> informacion = new HashMap<>();        

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append("select TO_CHAR(bit_fec_lim_ent_exp,'dd/mm/yyyy')as bit_fec_lim_ent_exp, TO_CHAR(bit_fec_lim_ent_inf_acl,'dd/mm/yyyy') as bit_fec_lim_ent_inf_acl, TO_CHAR(TRUNC(SYSDATE), 'dd/mm/yyyy') as FechaHoy " +
                "from bit_supervision where cal_mes = ?  and cal_anio = ? and ic_if =?");

            lVarBind = new ArrayList();
            lVarBind.add(mes);
            lVarBind.add(anio);
            lVarBind.add(icif);            
            
            LOG.debug("qry  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                informacion.put("bit_fec_lim_ent_exp", rs.getString("bit_fec_lim_ent_exp") == null ? "" : rs.getString("bit_fec_lim_ent_exp"));
                informacion.put("bit_fec_lim_ent_inf_acl", rs.getString("bit_fec_lim_ent_inf_acl") == null ? "" : rs.getString("bit_fec_lim_ent_inf_acl"));
                informacion.put("FechaHoy", rs.getString("FechaHoy") == null ? "" : rs.getString("FechaHoy"));                              
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en consultaFechas. " + e);
            throw new AppException("Error consultaFechas   "+e);
        } finally {
            LOG.info("consultaFechas (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;

    }
    private void actualizarFecEntExp(String mes, String anio, String icif, String fechaH, String tipo, AccesoDB con) {
        LOG.info("actualizarFecEntExp (e)");
       
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        
        try {
            con.conexionDB();
            qry = new StringBuilder();
            
            qry.append("UPDATE  bit_supervision SET bit_fec_ent_exp = TO_DATE(?, 'DD/MM/YYYY') " +
                "where cal_mes = ?  and cal_anio = ? and ic_if =? and bit_tipo = ?");            
            
            lVarBind = new ArrayList();
            lVarBind.add(fechaH);
            lVarBind.add(mes);
            lVarBind.add(anio);
            lVarBind.add(icif);
            lVarBind.add(tipo);
            
            LOG.info("qry  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            ps.executeUpdate();            
            
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en actualizarFecEntExp. " + e);
            throw new AppException("Error actualizarFecEntExp   "+e);
        }     
        LOG.info("actualizarFecExpAcl (s)");
    }
    
    private void actualizarFecExpAcl(String mes, String anio, String icif, String fechaH, String tipo, AccesoDB con) {
        LOG.info("actualizarFecExpAcl (e)");
        
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;        

        try {
            con.conexionDB();
            qry = new StringBuilder();
            
            qry.append("UPDATE bit_supervision SET bit_fec_ent_inf_acl = TO_DATE(?, 'DD/MM/YYYY') " +
                "where cal_mes = ?  and cal_anio = ? and ic_if =? and bit_tipo = ?");

            lVarBind = new ArrayList();
            lVarBind.add(fechaH);
            lVarBind.add(mes);
            lVarBind.add(anio);
            lVarBind.add(icif);
            lVarBind.add(tipo);
            
            LOG.info("qry  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            ps.executeUpdate();            
            
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en actualizarFecExpAcl. " + e);
            throw new AppException("Error actualizarFecExpAcl   "+e);
        }
        LOG.info("actualizarFecExpAcl (s)");
    }

    private List obtenerTipo(String mes, String anio, String icif) {
       LOG.info("obtenerTipo (e)");

       AccesoDB con = new AccesoDB();
       StringBuilder qry = new StringBuilder();
       List lVarBind = new ArrayList();
       PreparedStatement ps = null;
       ResultSet rs = null;
       List lista = new ArrayList();
       Map<String, String> informacion = new HashMap<>();
     

       try {
           con.conexionDB();

           qry = new StringBuilder();
           qry.append("select TO_CHAR(bit_fec_lim_ent_exp,'dd/mm/yyyy')as bit_fec_lim_ent_exp, TO_CHAR(bit_fec_lim_ent_inf_acl,'dd/mm/yyyy') as bit_fec_lim_ent_inf_acl, TO_CHAR(TRUNC(SYSDATE), 'dd/mm/yyyy') as FechaHoy, bit_tipo " +
               "from bit_supervision where cal_mes = ?  and cal_anio = ? and ic_if =?");

           lVarBind = new ArrayList();
           lVarBind.add(mes);
           lVarBind.add(anio);
           lVarBind.add(icif);
           
           
           LOG.info("qry  " + qry + " \n  lVarBind  " + lVarBind);
           
           ps = con.queryPrecompilado(qry.toString(), lVarBind);
           rs = ps.executeQuery();
           while (rs.next()) {
               informacion.put("bit_fec_lim_ent_exp", rs.getString("bit_fec_lim_ent_exp") == null ? "" : rs.getString("bit_fec_lim_ent_exp"));
               informacion.put("bit_fec_lim_ent_inf_acl", rs.getString("bit_fec_lim_ent_inf_acl") == null ? "" : rs.getString("bit_fec_lim_ent_inf_acl"));
               informacion.put("FechaHoy", rs.getString("FechaHoy") == null ? "" : rs.getString("FechaHoy"));                            
               informacion.put("bit_tipo", rs.getString("bit_tipo") == null ? "" : rs.getString("bit_tipo"));                              
               lista.add(informacion);
           }
           rs.close();
           ps.close();

       } catch (Exception e) {
           LOG.error("Exception en obtenerTipo. " + e);
           throw new AppException("Error obtenerTipo   "+e);
       } finally {
           LOG.info("obtenerTipo (S)");
           if (con.hayConexionAbierta()) {
               con.cierraConexionDB();
           }
       }
       return lista;

    }


    @Override
    public List obtieneResultadoSIAG(String siag) {
        LOG.info("obtieneResultadoSIAG (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qry = new StringBuilder();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lista = new ArrayList();
        Map<String, String> informacion = new HashMap<>();       

        try {
            con.conexionDB();

            qry = new StringBuilder();
            qry.append("SELECT        \n" + 
            "A.BIT_ID, \n" + 
            "		A.IC_IF,\n" + 
            "		B.CG_RAZON_SOCIAL AS NOMBRE_IF,\n" + 
            "		A.BIT_EJECUTIVO_NOMBRE,		\n" + 
            "        null as NGR, \n" + 
            "        TO_CHAR(A.BIT_FEC_PUB_RES_SUP,'DD/MM/YYYY') BIT_FEC_PUB_RES_SUP,\n" + 
            "        TO_CHAR(A.BIT_FEC_PUB_DIC,'DD/MM/YYYY') BIT_FEC_PUB_DIC,	\n" + 
            "        A.CAL_ANIO AS ANIO,\n" + 
            "        A.CAL_MES AS MES,\n" + 
            "        concat(A.CAL_MES, concat(' - ', A.CAL_ANIO)) as MESANIOG,              \n" + 
            "        DECODE(A.BIT_TIPO, 'O', 'Ordinario', 'E', 'Extempor�neo') AS BIT_TIPO,\n" + 
            "        null AS CLAVESIAG,          \n" + 
            "        B.IC_IF_SIAG AS IC_SIAG,\n" + 
            "        TO_CHAR(sci.cal_fec_lim_ent_r,'DD/MM/YYYY') cal_fec_lim_ent_r,\n" + 
            "        TO_CHAR(sci.cal_fec_lim_ent_d,'DD/MM/YYYY') cal_fec_lim_ent_d, \n" + 
            "        null as tipoCal \n" + 
            "                                                                   FROM BIT_SUPERVISION A, COMCAT_IF B, sup_calendario_if SCI  \n" + 
            "                    WHERE A.IC_IF = B.IC_IF \n" + 
            "                    AND b.ic_if_siag = sci.ic_cve_siag \n" + 
            "                    AND a.cal_mes = sci.cal_mes \n" + 
            "                    AND a.cal_anio = sci.cal_anio\n" + 
            "                    AND a.bit_tipo = sci.cal_tipo\n" + 
            "		AND 1 = 1 \n" + 
            "AND A.IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?) ");

            lVarBind = new ArrayList();
            lVarBind.add(siag);
                        
            LOG.debug("obtieneResultadoSIAG -- qry  " + qry + " \n  lVarBind  " + lVarBind);
            
            ps = con.queryPrecompilado(qry.toString(), lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion.put("BIT_ID", rs.getString("BIT_ID") == null ? "" : rs.getString("BIT_ID"));
                informacion.put("IC_IF", rs.getString("IC_IF") == null ? "" : rs.getString("IC_IF"));
                informacion.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));                            
                informacion.put("BIT_FEC_PUB_DIC", rs.getString("BIT_FEC_PUB_DIC") == null ? "" : rs.getString("BIT_FEC_PUB_DIC"));
                informacion.put("BIT_FEC_PUB_RES_SUP", rs.getString("BIT_FEC_PUB_RES_SUP") == null ? "" : rs.getString("BIT_FEC_PUB_RES_SUP"));
                informacion.put("ANIO", rs.getString("ANIO") == null ? "" : rs.getString("ANIO"));
                informacion.put("MES", rs.getString("MES") == null ? "" : rs.getString("MES"));
                informacion.put("MESANIOG", rs.getString("MESANIOG") == null ? "" : rs.getString("MESANIOG"));
                informacion.put("BIT_TIPO", rs.getString("BIT_TIPO") == null ? "" : rs.getString("BIT_TIPO"));
                informacion.put("CLAVESIAG", rs.getString("CLAVESIAG") == null ? "" : rs.getString("CLAVESIAG"));
                informacion.put("IC_SIAG", rs.getString("IC_SIAG") == null ? "" : rs.getString("IC_SIAG"));
                informacion.put("cal_fec_lim_ent_r", rs.getString("cal_fec_lim_ent_r") == null ? "" : rs.getString("cal_fec_lim_ent_r"));
                informacion.put("cal_fec_lim_ent_d", rs.getString("cal_fec_lim_ent_d") == null ? "" : rs.getString("cal_fec_lim_ent_d"));
                informacion.put("tipoCal", rs.getString("tipoCal") == null ? "" : rs.getString("tipoCal"));                
                lista.add(informacion);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            LOG.error("Exception en obtieneResultadoSIAG. " + e);
            throw new AppException("Error obtieneResultadoSIAG   "+e);
        } finally {
            LOG.info("obtieneResultadoSIAG (S)");
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return lista;
    }

    @Override
    public String obtieneSemaforoMonitordeSupervision(String fechaRealStr, String fechaLimiteStr, String tipoArchivo) throws NafinException {
        LOG.info("obtieneSemaforoMonitordeSupervision (E) fechaRealStr="+fechaRealStr+". fechaLimiteStr="+fechaLimiteStr+". tipoArchivo="+tipoArchivo);
        

        String semaforo = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String fechaActualStr = dateFormat.format(new Date());
        Date fechaReal = null;
        Date fechaLimite = null;

        try {
            // Valida nulos
            if (null == fechaLimiteStr || fechaLimiteStr.isEmpty()) {
                // Si no hay fecha limite, se regresa una cadena vacia, i.e. no pinta el semaforo
                semaforo = "";
            } else {
                // Convierto la fecha limite a Date
                fechaLimite = dateFormat.parse(fechaLimiteStr);
                // Valida si la fecha real es nula
                if (null == fechaRealStr || fechaRealStr.isEmpty()) {
                    // Si no hay fecha real entonces no se ha cargado el archivo.
                    // Comparo la fecha en que realizo la consulta (fechaActual) contra la fecha limite para saber cuantos dias me quedan para cargar el archivo
                    Date fechaActual = dateFormat.parse(fechaActualStr);
                    semaforo = obtieneColorSemaforo(fechaActual, fechaLimite, tipoArchivo);
                } else {
                    // Convierto la fecha real a Date
                    fechaReal = dateFormat.parse(fechaRealStr);
                    //Si la fecha real es anterior o igual a la fecha limite, entonces se pinta el semaforo de verde
                    //En caso contrario, se pinta de rojo
                    if(fechaReal.compareTo(fechaLimite) <= 0){//F1=F2:0, F1<F2:-1, F1>F2:1
                        semaforo = "VERDE";
                    } else {
                        semaforo = "ROJO";
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("Exception en obtieneSemaforoMonitordeSupervision. " + e);
            throw new AppException("Error obtieneSemaforoMonitordeSupervision "+e);
        } finally {
            LOG.info("obtieneSemaforoMonitordeSupervision (S)");
        }

        LOG.info("obtieneSemaforoMonitordeSupervision (S)");
        return semaforo;
    }

   /**
     * Para el semaforo del monitor Monitor, una vez que identifico las fechas a evaluar y fecha limite, obtengo los dias habiles
     * que existen entre ambas fechas y a partir de esos dias identifico el color a mostrar en el semaforo.
     * @param fechaAEvaluar: fecha de consulta del monitor o fecha de carga del archivo
     * @param fechaLimite
     * @param tipoArchivo: DICTAMEN o RESOLUCION
     * @return cadena de texto con el color a mostrar en el semaforo
     */
    public String obtieneColorSemaforo(Date fechaAEvaluar, Date fechaLimite, String tipoArchivo) {

        LOG.info("obtieneColorSemaforo (E) fechaAEvaluar="+fechaAEvaluar+". fechaLimite=" + fechaLimite +". tipoArchivo="+tipoArchivo);
        String semaforo = "";
        String fechaCalculadaStr = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        int diferenciaDias = 0;
        int diasPendientesHabiles = 0;
        int verde = 0;
        int amarillo = 0;
        int rojo = 0;

        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qry = "";

        try {
            //Obtengo los dias parametrizados
            con.conexionDB();
            qry = "SELECT PARAM_GRAL,VALOR FROM SUP_PARAMETROS_MONITOR";
            ps = con.queryPrecompilado(qry);
            rs = ps.executeQuery();
            while (rs.next()) {
                if("DICTAMEN".equals(tipoArchivo)) {
                    LOG.info("obtieneColorSemaforo PARAM_GRAL="+rs.getString("PARAM_GRAL")+"=");
                    if("ACT_SEMA_VERDE_D".equals(rs.getString("PARAM_GRAL"))){
                        verde = rs.getInt("VALOR");
                    } else if("ACT_SEMA_AMARILLO_D".equals(rs.getString("PARAM_GRAL"))){
                        amarillo = rs.getInt("VALOR");
                    } else if("ACT_SEMA_ROJO_D".equals(rs.getString("PARAM_GRAL"))){
                        rojo = rs.getInt("VALOR");
                    }
                } else {
                    if("ACT_SEMA_VERDE_R".equals(rs.getString("PARAM_GRAL"))){
                        verde = rs.getInt("VALOR");
                    } else if("ACT_SEMA_AMARILLO_R".equals(rs.getString("PARAM_GRAL"))){
                        amarillo = rs.getInt("VALOR");
                    } else if("ACT_SEMA_ROJO_R".equals(rs.getString("PARAM_GRAL"))){
                        rojo = rs.getInt("VALOR");
                    }
                }
            }
            LOG.info("obtieneColorSemaforo verde="+verde+". amarillo=" + amarillo +". rojo="+rojo);
            //Obtengo los d�as inhabiles
            List<String> lista = new ArrayList<>();
            StringBuilder diasInhabiles = new StringBuilder();
            ps = null;
            qry = "SELECT CG_DIA_INHABIL FROM COMCAT_DIA_INHABIL where CG_DIA_INHABIL is not null";
            ps = con.queryPrecompilado(qry);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("CG_DIA_INHABIL"));
            }
            ps = null;
            qry = "SELECT  TO_CHAR(DF_DIA_INHABIL,'DD/MM') AS DF_DIA_INHABIL FROM COMCAT_DIA_INHABIL where DF_DIA_INHABIL is not null and to_number(TO_CHAR(DF_DIA_INHABIL,'YYYY'))>= to_number(TO_CHAR(SYSDATE,'YYYY'))";
            ps = con.queryPrecompilado(qry);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("DF_DIA_INHABIL"));
            }
            if(!lista.isEmpty()){
                for(int i=0; i<lista.size(); i++){
                    if(i>0){
                        diasInhabiles.append(";");
                    }
                    diasInhabiles.append(lista.get(i));
                }
            }
            LOG.info("obtieneColorSemaforo diasInhabiles=" + diasInhabiles.toString());

            //Obtengo la diferencia en d�as entre las dos fechas
            diferenciaDias = (int) ((fechaLimite.getTime()-fechaAEvaluar.getTime())/86400000);
            LOG.info("obtieneColorSemaforo diferenciaDias=" + diferenciaDias);
            if (diferenciaDias <= 0) {
                // Si la fecha de la consulta es mayor a la dfecha limite (diferenciaDias es negativo) entonces se pinta de rojo
                semaforo = "ROJO";
            } else {
                // Obtengo los d�as habiles que hay de la fecha de consulta (diaActual) a la fecha limite
                for (int dias = 0; dias < diferenciaDias; dias++) {
                    //Por cada ciclo se suma un d�a a la fecha a evaluar. La fecha que regresa el metodo se valida para saber si es un d�a habio o no.
                    //En caso de que fechaTmp sea un d�i habil, se sumar� un d�a a la variable diasPendientesHabiles, 
                    //esta variable nos dice el total de dias validos que faltan para la fecha limite.
                    Date fechaTmp = sumarDiasAFecha(fechaAEvaluar, dias);
                    fechaCalculadaStr = dateFormat.format(fechaTmp);
                    if (Fecha.esDiaHabil(fechaCalculadaStr, "dd/MM/yyyy", diasInhabiles.toString())) {
                        diasPendientesHabiles++;
                    }
                }
                // Valido los d�as pendientes para saber a que color pertenecen
                LOG.info("obtieneColorSemaforo diasPendientesHabiles=" + diasPendientesHabiles);
                if (diasPendientesHabiles >= verde) {
                    semaforo = "VERDE";
                } else if (diasPendientesHabiles >= amarillo) {
                    semaforo = "AMARILLO";
                } else {
                    semaforo = "ROJO";
                }
            }
        } catch (Exception e) {
            LOG.error("Exception en obtieneColorSemaforo. " + e);
            throw new AppException("Error obtieneColorSemaforo ");
        } finally {
            LOG.info("obtieneColorSemaforo (S) semaforo="+semaforo);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return semaforo;
    }

    /**
     * A una fecha dada, se le suma un numero indicado de dias, y regresa la fecha resultante
     * @param fecha
     * @param dias
     * @return
     */
    public static Date sumarDiasAFecha(Date fecha, int dias){
        LOG.info("sumarDiasAFecha (E)");
        if (dias==0) return fecha;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); 
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        LOG.info("sumarDiasAFecha (S)");
        return calendar.getTime(); 
    }
		
		

	
}
