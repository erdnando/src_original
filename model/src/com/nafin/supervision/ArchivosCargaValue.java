package com.nafin.supervision;

import java.io.Serializable;

public class ArchivosCargaValue implements Serializable{
    @SuppressWarnings("compatibility:5104794991040688183")
    private static final long serialVersionUID = -3529706557075597976L;

    private String mes;
    private String anio;
    private String tipoCarga;
    private String noArchivos;
    private String usuario;
    private String folio;
    private String nombreArchivo;
    private String extension;
    private String interFinan;
    private String acuerdo;
    
    private byte[] _ARCHIVO_ESP;
    
    public ArchivosCargaValue() {
        super();
    }


    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getMes() {
        return mes;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getAnio() {
        return anio;
    }

    public void setTipoCarga(String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    public String getTipoCarga() {
        return tipoCarga;
    }

    public void setNoArchivos(String noArchivos) {
        this.noArchivos = noArchivos;
    }

    public String getNoArchivos() {
        return noArchivos;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFolio() {
        return folio;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public void setARCHIVO_ESP(byte[] _ARCHIVO_ESP) {
        this._ARCHIVO_ESP = _ARCHIVO_ESP;
    }

    public byte[] getARCHIVO_ESP() {
        return _ARCHIVO_ESP;
    }

    public void setInterFinan(String interFinan) {
        this.interFinan = interFinan;
    }

    public String getInterFinan() {
        return interFinan;
    }
    
    public void setAcuerdo(String acuerdo) {
        this.acuerdo = acuerdo;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

}
