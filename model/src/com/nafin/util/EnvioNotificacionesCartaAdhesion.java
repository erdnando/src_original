package com.nafin.util;

import javax.ejb.Remote;
@Remote
public interface EnvioNotificacionesCartaAdhesion {
    
    public void enviaCorreos();
}
