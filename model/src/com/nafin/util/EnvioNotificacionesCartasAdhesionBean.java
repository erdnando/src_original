package com.nafin.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "EnvioNotificacionCartasAdhesion" , mappedName = "EnvioNotificacionCartasAdhesion")
@TransactionManagement(TransactionManagementType.BEAN)
public class EnvioNotificacionesCartasAdhesionBean implements EnvioNotificacionesCartaAdhesion{

    private static final Log log = ServiceLocator.getInstance().getLog(EnvioNotificacionesCartasAdhesionBean.class);
    
    public void enviaCorreos(){
        List<String> emails = getCorreosNotificar();
        String mensaje = getMensajeNotificacion();
        String remitente = "no_response@nafin.gob.mx";
        String asunto = "Notificaci�n de Cartas de Adhesi�n formalizadas";
        for (String email : emails){
             enviarTextoHTML(remitente, email, asunto, mensaje,"");                             
         }        
    }

    private String getCeldasHTMLCartasFormalizadas(){
        AccesoDBOracle con = new AccesoDBOracle(); // conexion directa  
       String celdasHTML = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();            
            String sql = 
                " SELECT                                                                                                  "+
                "     comcat_epo.cg_razon_social   AS nombreepo,                                                          "+
                "     comcat_if.cg_razon_social    AS nombreif,                                                           "+
                "     DECODE(c.cc_tipo_factoraje, 'N', 'Factoraje Normal', 'V', 'Factoraje al Vencimiento') AS factoraje  "+
                " FROM                                                                                                    "+
                "     cad_carta_adhesion c,                                                                               "+
                "     comcat_if,                                                                                          "+
                "     comcat_epo                                                                                          "+
                " WHERE                                                                                                   "+
                "     c.ic_if = comcat_if.ic_if                                                                           "+
                "     AND c.ic_epo = comcat_epo.ic_epo                                                                    "+
                "     AND c.dt_fecha_formalizacion > trunc(SYSDATE, 'DDD')                                                "+
                "     AND c.ic_estatus_carta = 'F'                                                                        "+                
                " ORDER BY                                                                                                "+
                "     nombreepo,                                                                                          "+
                "     nombreif                                                                                            ";                
            ps = con.queryPrecompilado(sql); 
            rs = ps.executeQuery();
            boolean hayRegistros = false;
            while (rs.next()){
                celdasHTML +="<tr><td class=\"celdas\">"+rs.getString("nombreepo")+"</td>"+
                            "<td class=\"celdas\">"+rs.getString("nombreif")+"</td>"+
                            "<td class=\"celdas\">"+rs.getString("factoraje")+"</td></tr>";
                hayRegistros = true;    
            }   
            if (!hayRegistros){
                celdasHTML = "<tr><td class=\"celdas\" colspan=3>Ninguna carta de adhesi�n formalizada hoy</td></tr>";
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error al consultar la informaci�n en Base de Datos", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }    
        return celdasHTML;
    }
    
    private List<String> getCorreosNotificar(){
        List<String> emails = new ArrayList<>();
        AccesoDBOracle con = new AccesoDBOracle(); // conexion directa  
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB(); 
            String cadenaCorreos = null;
            String sql = 
                " SELECT                        "+
                "     cg_lista_emails           "+
                " FROM                          "+
                "     cad_param_notificacion    "+
                " WHERE                         "+
                "     ic_param_notificacion = 1 ";                
            ps = con.queryPrecompilado(sql); 
            rs = ps.executeQuery();
            while (rs.next()){
                cadenaCorreos =rs.getString("cg_lista_emails");
            }   
            if (cadenaCorreos != null){
                String[] correo = cadenaCorreos.split(",");
                for (String  c : correo){
                    boolean add = emails.add(c);
                }
            }
            else{
                log.error("No hay ninguna cuenta de correo");
            }
            ps.close();
            rs.close();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error al consultar la informaci�n en Base de Datos", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }          
        return emails;
    }

    private String getMensajeNotificacion() {
        String html =
        "<!DOCTYPE html>                                                                                                                                                                   "+                                               
        "<html>                                                                                                                                                                            "+                                               
        "   <head>                                                                                                                                                                         "+                                              
        "       <style>                                                                                                                                                                    "+
        "       * {font-family:\"Arial\",\"sans-serif\";}                                                                                                                                          "+
        "       .mainTable {border: 0px;padding:5px;width:70%;}                                                                                                                                 "+
        "       .footer{font-size:7.5pt;color:#999999;text-align:center}                                                                                                                   "+
        "       .nota{font-size:8pt;color:#484045}                                                                                                                                         "+
        "       .h{font-size:10.0pt;color:#000;font-weight:bold;}                                                                                                                          "+
        "       .eltexto{font-size:10.0pt;color:#484045}                                                                                                                                   "+
        "       .latabla{width:850px;font-size:10.0pt;border: solid 1px gray;border-collapse: collapse;}                                                                                   "+
        "       .celdas{  border: 1px solid #dddddd;                                                                                                                                       "+
        "                text-align: left;                                                                                                                                                 "+
        "                padding: 5px;}                                                                                                                                                    "+
        "       .titulos{ border: 1px solid #dddddd;                                                                                                                                       "+
        "                  text-align: left;                                                                                                                                               "+
        "                  padding: 5px;                                                                                                                                                   "+
        "                  background-color: #dee0e3;}                                                                                                                                     "+
        "       </style>                                                                                                                                                                   "+                                              
        "   </head>                                                                                                                                                                        "+                                              
        "   <body>                                                                                                                                                                         "+                                              
        "       <table class=\"mainTable\" align=\"center\">                                                                                                                                                   "+                                              
        "           <tr align=\"center\">                                                                                                                                                  "+                                              
        "               <td>                                                                                                                                                               "+                                              
        "                   <img src=\"https://www.nafin.com/portalnf/content/images/logoNAFINSA.png\" align=\"center\" alt=\"Nacional Financiera\">                                       "+                                              
        "                   <hr size=\"2\" align=\"center\" width=\"100%\">                                                                                                                "+                                              
        "               </td>                                                                                                                                                              "+                                              
        "           </tr>                                                                                                                                                                  "+                                              
        "           <tr>                                                                                                                                                                   "+                                              
        "               <td>                                                                                                                                                               "+                                              
        "                   <p class=\"h\">Estimado(a): Administrador de Nafinet</p>                                                                                                       "+
        "                   <p class=\"eltexto\">                                                                                                                                          "+
        "                     Por medio de la presente se le notifica que las Cartas de Adhesi�n formalizadas el d�a de hoy, son las siguientes:                                           "+
        "                   </p>                                                                                                                                                           "+
        "                                                                                                                                                                                  "+
        "                    <table class=\"latabla\">                                                                                                                                     "+
        "                      <tr>                                                                                                                                                        "+
        "                        <td class=\"titulos\" width=\"40%\">Cadena Productiva</td>                                                                                                "+
        "                        <td class=\"titulos\" width=\"40%\">Intermediario Financiero</td>                                                                                         "+
        "                        <td class=\"titulos\" width=\"20%\">Tipo de Factoraje</td>                                                                                                "+
                                getCeldasHTMLCartasFormalizadas()+        
        "                      </tr>                                                                                                                                                       "+
        "                    </table>                                                                                                                                                      "+
        "                  <p class=\"eltexto\">Favor de realizar los cambios mencionados en la nueva Carta de Adhesi�n en Nafinet y cambiar el estatus a Autorizado.<br/>                 "+
        "                    Esta consulta la podr� realizar en la pantalla Administraci�n / Cartas de Adhesi�n / Consulta Cartas Adhesi�n.<br/><br/>                                      "+
        "                    Gracias.                                                                                                                                                      "+
        "                  </p>                                                                                                                                                            "+
        "                   <br><br><br><br>                                                                                                                                               "+                                              
        "                   <p class=\"eltexto\">ATENTAMENTE<br>Nacional Financiera, S.N.C.<br><br><br><br><br></p>                                                                        "+                                                                   
        "                   <p class=\"nota\">Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada.               "+
        "                   Por favor no responda este mensaje.                                                                                                                            "+                                              
        "                   </p>                                                                                                                                                           "+                                              
        "                   <br/>                                                                                                                                                          "+                                           
        "               </td>                                                                                                                                                              "+                                              
        "           </tr>                                                                                                                                                                  "+                                              
        "           <tr align=\"center\">                                                                                                                                                  "+                                              
        "               <td><img src=\"https://cadenas.nafin.com.mx/nafin/home/img/gobmx.png\" width=\"212\" height=\"79\" border=\"0\"></td>                                              "+                                              
        "           </tr>                                                                                                                                                                  "+                                              
        "           <tr>                                                                                                                                                                   "+                                              
        "               <td class=\"footer\">                                                                                                                                              "+     
        "                   Copyright Nacional Financiera 2008 / info@nafin.gob.mx / 01 800 Nafinsa (623 4672) / Av. Insurgentes Sur 1971, Col. Guadalupe Inn, CP 01020 M&eacute;xico CDMX "+                                              
        "               </td>                                                                                                                                                              "+                                              
        "           </tr>                                                                                                                                                                  "+                                              
        "       </table>                                                                                                                                                                   "+                                              
        "   </body>                                                                                                                                                                        "+                                              
        "</html>                                                                                                                                                                           ";            
        return html;
    }

    /**
     * M�todo para enviar correo en codigo HTML 
     * @param from Remitente
     * @param to Destinatario
     * @param cc Destinatario Copia
     * @param subject Titulo del mensaje
     * @param contenidoMensaje Cadena de texto correspondiente al c�digo HTML
     */
    private void enviarTextoHTML(String from, String to, String subject,
                    String contenidoMensaje, String cc) throws AppException {
            log.debug("enviarHTML(E)");
            AccesoDBOracle con = new AccesoDBOracle(); // conexion directa 
            String host = "";
            try{
                    host = getMailHost();
                    // Get system properties
                    Properties props = System.getProperties();
                    // Setup mail server
                    props.put("mail.smtp.host", host);
                    // Get session
                    Session session = Session.getInstance(props, null);
                    // Define message
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(from));
                    message.addRecipients(Message.RecipientType.TO, to);
                    if(cc != null && !cc.equals("")){
                            message.addRecipients(Message.RecipientType.CC, 
                                            InternetAddress.parse(cc, false) );                                                     
                    }
                    message.setSubject(subject, "UTF-8");
                    // Create a Multipart
                    MimeMultipart multipart = new MimeMultipart("related");
                    // Contenido
                    BodyPart messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setContent(contenidoMensaje, "text/html;charset=UTF-8");
                    multipart.addBodyPart(messageBodyPart);
                    // Poner todo junto
                    message.setContent(multipart);
                    //Transport.send(message);
                    Transport.send(message, message.getRecipients(Message.RecipientType.TO));
                    if(cc != null && !cc.equals("")){
                            Transport.send(message, message.getRecipients(Message.RecipientType.CC));
                    }
            }catch (Exception ex) {
                    log.error(ex.getMessage());
                    ex.printStackTrace();
                    throw new AppException("Error al enviar correo", ex);
            } finally {
                    log.debug("enviarHTML(S)");
                    if (con.hayConexionAbierta()){
                            con.cierraConexionDB();
                    }
            }                   
    }

    /** Regresa la IP del serviror SMTP para envio de correo */
    private String getMailHost() throws Exception {
        AccesoDBOracle con = null;
        String qrySentencia = null;
        List<Integer> lVarBind = new ArrayList<Integer>();
        Registros registros = null;
        String host = null;
        try {
            con = new AccesoDBOracle();
            con.enableMessages();
            con.conexionDB();
            qrySentencia =
                " SELECT  CG_IP_SMTP AS HOST FROM COM_PARAM_GRAL WHERE IC_PARAM_GRAL = ? ";
            lVarBind.add(new Integer(1));
            registros = con.consultarDB(qrySentencia, lVarBind, false);
            if (registros != null && registros.next()) {
                host = registros.getString("HOST");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
        }
        return host;
    }
    
}
