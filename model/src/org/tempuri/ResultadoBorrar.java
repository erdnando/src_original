
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultadoBorrar complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ResultadoBorrar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="st_Estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_Error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultadoBorrar", propOrder = { "stEstatus", "stError" })
public class ResultadoBorrar {

    @XmlElement(name = "st_Estatus")
    protected String stEstatus;
    @XmlElement(name = "st_Error")
    protected String stError;

    /**
     * Gets the value of the stEstatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStEstatus() {
        return stEstatus;
    }

    /**
     * Sets the value of the stEstatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStEstatus(String value) {
        this.stEstatus = value;
    }

    /**
     * Gets the value of the stError property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStError() {
        return stError;
    }

    /**
     * Sets the value of the stError property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStError(String value) {
        this.stError = value;
    }

}
