
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultadoSalida complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ResultadoSalida">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="st_Estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_Error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_NombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_ExtensionArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Itemnum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="st_ArchivoBase64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultadoSalida", propOrder = {
         "stEstatus", "stError", "stNombreArchivo", "stExtensionArchivo", "itemnum", "stArchivoBase64", "version"
    })
public class ResultadoSalida {

    @XmlElement(name = "st_Estatus")
    protected String stEstatus;
    @XmlElement(name = "st_Error")
    protected String stError;
    @XmlElement(name = "st_NombreArchivo")
    protected String stNombreArchivo;
    @XmlElement(name = "st_ExtensionArchivo")
    protected String stExtensionArchivo;
    @XmlElement(name = "Itemnum")
    protected int itemnum;
    @XmlElement(name = "st_ArchivoBase64")
    protected String stArchivoBase64;
    @XmlElement(name = "Version")
    protected int version;

    /**
     * Gets the value of the stEstatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStEstatus() {
        return stEstatus;
    }

    /**
     * Sets the value of the stEstatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStEstatus(String value) {
        this.stEstatus = value;
    }

    /**
     * Gets the value of the stError property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStError() {
        return stError;
    }

    /**
     * Sets the value of the stError property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStError(String value) {
        this.stError = value;
    }

    /**
     * Gets the value of the stNombreArchivo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStNombreArchivo() {
        return stNombreArchivo;
    }

    /**
     * Sets the value of the stNombreArchivo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStNombreArchivo(String value) {
        this.stNombreArchivo = value;
    }

    /**
     * Gets the value of the stExtensionArchivo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStExtensionArchivo() {
        return stExtensionArchivo;
    }

    /**
     * Sets the value of the stExtensionArchivo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStExtensionArchivo(String value) {
        this.stExtensionArchivo = value;
    }

    /**
     * Gets the value of the itemnum property.
     *
     */
    public int getItemnum() {
        return itemnum;
    }

    /**
     * Sets the value of the itemnum property.
     *
     */
    public void setItemnum(int value) {
        this.itemnum = value;
    }

    /**
     * Gets the value of the stArchivoBase64 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStArchivoBase64() {
        return stArchivoBase64;
    }

    /**
     * Sets the value of the stArchivoBase64 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStArchivoBase64(String value) {
        this.stArchivoBase64 = value;
    }

    /**
     * Gets the value of the version property.
     *
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     *
     */
    public void setVersion(int value) {
        this.version = value;
    }

}
