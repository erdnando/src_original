
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="st_Sistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_Identificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_NombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_TipoProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_FileStringBase64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
         "stSistema", "stIdentificador", "stNombreArchivo", "stTipoProceso", "stFileStringBase64" })
@XmlRootElement(name = "IngresaArchivo")
public class IngresaArchivo {

    @XmlElement(name = "st_Sistema")
    protected String stSistema;
    @XmlElement(name = "st_Identificador")
    protected String stIdentificador;
    @XmlElement(name = "st_NombreArchivo")
    protected String stNombreArchivo;
    @XmlElement(name = "st_TipoProceso")
    protected String stTipoProceso;
    @XmlElement(name = "st_FileStringBase64")
    protected String stFileStringBase64;

    /**
     * Gets the value of the stSistema property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStSistema() {
        return stSistema;
    }

    /**
     * Sets the value of the stSistema property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStSistema(String value) {
        this.stSistema = value;
    }

    /**
     * Gets the value of the stIdentificador property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStIdentificador() {
        return stIdentificador;
    }

    /**
     * Sets the value of the stIdentificador property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStIdentificador(String value) {
        this.stIdentificador = value;
    }

    /**
     * Gets the value of the stNombreArchivo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStNombreArchivo() {
        return stNombreArchivo;
    }

    /**
     * Sets the value of the stNombreArchivo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStNombreArchivo(String value) {
        this.stNombreArchivo = value;
    }

    /**
     * Gets the value of the stTipoProceso property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStTipoProceso() {
        return stTipoProceso;
    }

    /**
     * Sets the value of the stTipoProceso property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStTipoProceso(String value) {
        this.stTipoProceso = value;
    }

    /**
     * Gets the value of the stFileStringBase64 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStFileStringBase64() {
        return stFileStringBase64;
    }

    /**
     * Sets the value of the stFileStringBase64 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStFileStringBase64(String value) {
        this.stFileStringBase64 = value;
    }

}
