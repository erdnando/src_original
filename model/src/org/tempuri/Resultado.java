
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Resultado complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Resultado">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="st_Estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_Error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Itemnum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Resultado", propOrder = { "stEstatus", "stError", "itemnum", "version" })
public class Resultado {

    @XmlElement(name = "st_Estatus")
    protected String stEstatus;
    @XmlElement(name = "st_Error")
    protected String stError;
    @XmlElement(name = "Itemnum")
    protected int itemnum;
    @XmlElement(name = "Version")
    protected int version;

    /**
     * Gets the value of the stEstatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStEstatus() {
        return stEstatus;
    }

    /**
     * Sets the value of the stEstatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStEstatus(String value) {
        this.stEstatus = value;
    }

    /**
     * Gets the value of the stError property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStError() {
        return stError;
    }

    /**
     * Sets the value of the stError property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStError(String value) {
        this.stError = value;
    }

    /**
     * Gets the value of the itemnum property.
     *
     */
    public int getItemnum() {
        return itemnum;
    }

    /**
     * Sets the value of the itemnum property.
     *
     */
    public void setItemnum(int value) {
        this.itemnum = value;
    }

    /**
     * Gets the value of the version property.
     *
     */
    public int getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     *
     */
    public void setVersion(int value) {
        this.version = value;
    }

}
