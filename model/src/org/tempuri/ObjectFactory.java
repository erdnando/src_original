
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the org.tempuri package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IngresaArchivo }
     *
     */
    public IngresaArchivo createIngresaArchivo() {
        return new IngresaArchivo();
    }

    /**
     * Create an instance of {@link ObtieneArchivo }
     *
     */
    public ObtieneArchivo createObtieneArchivo() {
        return new ObtieneArchivo();
    }

    /**
     * Create an instance of {@link IngresaArchivoResponse }
     *
     */
    public IngresaArchivoResponse createIngresaArchivoResponse() {
        return new IngresaArchivoResponse();
    }

    /**
     * Create an instance of {@link Resultado }
     *
     */
    public Resultado createResultado() {
        return new Resultado();
    }

    /**
     * Create an instance of {@link BorrarArchivo }
     *
     */
    public BorrarArchivo createBorrarArchivo() {
        return new BorrarArchivo();
    }

    /**
     * Create an instance of {@link ObtieneArchivoResponse }
     *
     */
    public ObtieneArchivoResponse createObtieneArchivoResponse() {
        return new ObtieneArchivoResponse();
    }

    /**
     * Create an instance of {@link ResultadoSalida }
     *
     */
    public ResultadoSalida createResultadoSalida() {
        return new ResultadoSalida();
    }

    /**
     * Create an instance of {@link BorrarArchivoResponse }
     *
     */
    public BorrarArchivoResponse createBorrarArchivoResponse() {
        return new BorrarArchivoResponse();
    }

    /**
     * Create an instance of {@link ResultadoBorrar }
     *
     */
    public ResultadoBorrar createResultadoBorrar() {
        return new ResultadoBorrar();
    }

}
