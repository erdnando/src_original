
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BorrarArchivoResult" type="{http://tempuri.org/}ResultadoBorrar" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "borrarArchivoResult" })
@XmlRootElement(name = "BorrarArchivoResponse")
public class BorrarArchivoResponse {

    @XmlElement(name = "BorrarArchivoResult")
    protected ResultadoBorrar borrarArchivoResult;

    /**
     * Gets the value of the borrarArchivoResult property.
     *
     * @return
     *     possible object is
     *     {@link ResultadoBorrar }
     *
     */
    public ResultadoBorrar getBorrarArchivoResult() {
        return borrarArchivoResult;
    }

    /**
     * Sets the value of the borrarArchivoResult property.
     *
     * @param value
     *     allowed object is
     *     {@link ResultadoBorrar }
     *
     */
    public void setBorrarArchivoResult(ResultadoBorrar value) {
        this.borrarArchivoResult = value;
    }

}
