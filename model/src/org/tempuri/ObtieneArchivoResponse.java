
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObtieneArchivoResult" type="{http://tempuri.org/}ResultadoSalida" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "obtieneArchivoResult" })
@XmlRootElement(name = "ObtieneArchivoResponse")
public class ObtieneArchivoResponse {

    @XmlElement(name = "ObtieneArchivoResult")
    protected ResultadoSalida obtieneArchivoResult;

    /**
     * Gets the value of the obtieneArchivoResult property.
     *
     * @return
     *     possible object is
     *     {@link ResultadoSalida }
     *
     */
    public ResultadoSalida getObtieneArchivoResult() {
        return obtieneArchivoResult;
    }

    /**
     * Sets the value of the obtieneArchivoResult property.
     *
     * @param value
     *     allowed object is
     *     {@link ResultadoSalida }
     *
     */
    public void setObtieneArchivoResult(ResultadoSalida value) {
        this.obtieneArchivoResult = value;
    }

}
