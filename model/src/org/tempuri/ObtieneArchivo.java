
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="st_Sistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="st_Identificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "stSistema", "stIdentificador", "itemNum", "numVersion" })
@XmlRootElement(name = "ObtieneArchivo")
public class ObtieneArchivo {

    @XmlElement(name = "st_Sistema")
    protected String stSistema;
    @XmlElement(name = "st_Identificador")
    protected String stIdentificador;
    @XmlElement(name = "ItemNum")
    protected String itemNum;
    @XmlElement(name = "NumVersion")
    protected String numVersion;

    /**
     * Gets the value of the stSistema property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStSistema() {
        return stSistema;
    }

    /**
     * Sets the value of the stSistema property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStSistema(String value) {
        this.stSistema = value;
    }

    /**
     * Gets the value of the stIdentificador property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStIdentificador() {
        return stIdentificador;
    }

    /**
     * Sets the value of the stIdentificador property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStIdentificador(String value) {
        this.stIdentificador = value;
    }

    /**
     * Gets the value of the itemNum property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getItemNum() {
        return itemNum;
    }

    /**
     * Sets the value of the itemNum property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setItemNum(String value) {
        this.itemNum = value;
    }

    /**
     * Gets the value of the numVersion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumVersion() {
        return numVersion;
    }

    /**
     * Sets the value of the numVersion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumVersion(String value) {
        this.numVersion = value;
    }

}
