
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IngresaArchivoResult" type="{http://tempuri.org/}Resultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "ingresaArchivoResult" })
@XmlRootElement(name = "IngresaArchivoResponse")
public class IngresaArchivoResponse {

    @XmlElement(name = "IngresaArchivoResult")
    protected Resultado ingresaArchivoResult;

    /**
     * Gets the value of the ingresaArchivoResult property.
     *
     * @return
     *     possible object is
     *     {@link Resultado }
     *
     */
    public Resultado getIngresaArchivoResult() {
        return ingresaArchivoResult;
    }

    /**
     * Sets the value of the ingresaArchivoResult property.
     *
     * @param value
     *     allowed object is
     *     {@link Resultado }
     *
     */
    public void setIngresaArchivoResult(Resultado value) {
        this.ingresaArchivoResult = value;
    }

}
