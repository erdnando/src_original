package netropology.utilerias;


/**
 * Manejo de mensajes de error.
 * @author Gilberto Aparicio
 *
 */
public class BitacoraException extends Exception {

	public BitacoraException() {}

	public BitacoraException(String mensajeError) {
		super(mensajeError);
	}
}