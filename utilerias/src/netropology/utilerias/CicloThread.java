package netropology.utilerias;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clase que se encarga de invocar al SP que realiza la carga
 * masiva de documentos para descuento electrónico
 */
public class CicloThread extends Thread {

	int 	NumProceso1 ;
	int 	ses_ic_epo1 ;
	double 	ctxtmtomindoc1 ;
	double 	ctxtmtomaxdoc1 ;
	double 	ctxtmtomindocdol1;
	double 	ctxtmtomaxdocdol1;
	int 	ctxttotdoc1;
	int 	ctxttotdocdol1;
	double 	ctxtmtodoc1;
	double 	ctxtmtodocdol1;
	String 	sesIdiomaUsuario;
	String 	strPreNegociable;
  String 	strPendiente;
  String iNoUsuario;
  String strNombreUsuario;


	public CicloThread(
			int 	NumProceso, 		int 	ses_ic_epo,
			double 	ctxtmtomindoc,		double 	ctxtmtomaxdoc,
			double 	ctxtmtomindocdol, 	double 	ctxtmtomaxdocdol,
			int 	ctxttotdoc, 		int 	ctxttotdocdol,
			double 	ctxtmtodoc, 		double 	ctxtmtodocdol,
			String 	sesIdiomaUsuario,	String 	strPreNegociable, String strPendiente,String iNoUsuario , String strNombreUsuario ) {

		this.NumProceso1       	= NumProceso ;
		this.ses_ic_epo1       	= ses_ic_epo;
		this.ctxtmtomindoc1    	= ctxtmtomindoc;
		this.ctxtmtomaxdoc1    	= ctxtmtomaxdoc;
		this.ctxtmtomindocdol1 	= ctxtmtomindocdol;
		this.ctxtmtomaxdocdol1 	= ctxtmtomaxdocdol;
		this.ctxttotdoc1       	= ctxttotdoc;
		this.ctxttotdocdol1    	= ctxttotdocdol;
		this.ctxtmtodoc1       	= ctxtmtodoc;
		this.ctxtmtodocdol1    	= ctxtmtodocdol;
		this.sesIdiomaUsuario 	= sesIdiomaUsuario;
		this.strPreNegociable 	= strPreNegociable;
		 this.strPendiente 	= strPendiente;
		this.iNoUsuario 	= iNoUsuario;
		this.strNombreUsuario 	= strNombreUsuario;

	}//CicloThread

	public void run() {
		AccesoDB con = new AccesoDB();
		CallableStatement cs=null;
		ResultSet rsStoreProc;
		System.out.println("Thread:SP_CARGA_MASIVA("+NumProceso1+","+
						ses_ic_epo1+","+ctxtmtomindoc1+","+ctxtmtomaxdoc1+","+
						ctxtmtomindocdol1+","+ctxtmtomaxdocdol1+","+ctxttotdoc1+","+
						ctxttotdocdol1+","+ctxtmtodoc1+","+ctxtmtodocdol1+","+
						sesIdiomaUsuario + iNoUsuario +","+ strNombreUsuario +")" );
		try {
			con.conexionDB();
			cs = con.ejecutaSP("SP_CARGA_MASIVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )");
			cs.setInt(		 1, NumProceso1);
			cs.setInt(		 2, ses_ic_epo1);
			cs.setDouble(	 3, ctxtmtomindoc1);
			cs.setDouble(	 4, ctxtmtomaxdoc1);
			cs.setDouble(	 5, ctxtmtomindocdol1);
			cs.setDouble(	 6, ctxtmtomaxdocdol1);
			cs.setInt(		 7, ctxttotdoc1);
			cs.setInt(		 8, ctxttotdocdol1);
			cs.setDouble(	 9, ctxtmtodoc1);
			cs.setDouble(	10, ctxtmtodocdol1);
			cs.setString(	11, sesIdiomaUsuario);
			cs.setString(	12, strPreNegociable);
			cs.setString(	13, strPendiente);
			cs.setString(	14, iNoUsuario);
			cs.setString(	15, strNombreUsuario);

			cs.execute();
			if (cs !=null) cs.close();
			con.cierraStatement();
		} catch (SQLException e) {
			System.out.println("Error en SP\n" + e );
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("CicloThread::(Exception): "+ex);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}//finally
	}//run
}//CicloThread
