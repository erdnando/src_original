package netropology.utilerias;

import java.io.File;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Clase que contiene metodos de uso comun para manejo de archivos XML.
 * @author Gilberto Aparicio
 * @since 07/2010
 */
public class ComunesXML {

	public ComunesXML() {}
	
	/**
	 * lee el xml especificado y obtiene el valor de la etiqueta 
	 * &lt;color_fondo&gt;#FFFFFF&lt;/color_fondo&gt;
	 * @param rutaArchivoXML Ruta fisica del archivo XML
	 * @return Cadena con el color obtenido.
	 * 
	 */
	public static String leerColorXML(String rutaArchivoXML) {
		DefaultHandler handler = new ComunesXML.XMLColorHandler();

		SAXParserFactory factory = SAXParserFactory.newInstance();
		String color = "#FFFFFF";
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new File(rutaArchivoXML), handler);
			color = ((ComunesXML.XMLColorHandler)handler).getColor();
			if (color == null || color.trim().equals("")) {
				color = "#FFFFFF";
			}
			//Se da por hecho que el color viene en formato hexadecimal #FFFFFF
			if (color.charAt(0) != '#') {	//Si el color no tiene el numeral al inicio, se lo agrega.
					color = "#" + color;
			}
			
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return color;
	}

	/**
	 * Clase que determina como procesar el archivo XML que contiene el 
	 * color de fondo para el menu.
	 * @author Gilberto Aparicio
	 */
	private static class XMLColorHandler extends DefaultHandler {
		StringBuffer textBuffer;
		Map m = new HashMap();
		String elemento = null;

		/**
		 * Especifica las acciones a realizar cuando detecta el inicio de un elemento
		 * @param namespaceURI
		 * @param sName Simple Name
		 * @param qName Qualified Name
		 * @param attrs atributos
		 * @throws org.xml.sax.SAXException
		 */
		public void startElement(String namespaceURI, String sName, // simple name
			String qName, // qualified name
			Attributes attrs) throws SAXException {
			if (qName != null && qName.equals("color_fondo")) {
				this.elemento = qName;
			} else {
				this.elemento = null;
			}
		}

		/**
		 * Especifica las acciones a realizar cuando detecta el fin de un elemento
		 * @param namespaceURI
		 * @param sName Simple Name
		 * @param qName Qualified Name
		 * @throws org.xml.sax.SAXException
		 */
		public void endElement(String namespaceURI, String sName, // simple name
				String qName // qualified name
				) throws SAXException {
			this.elemento = null;
		}
			
		/**
		 * Especifica las acciones a realizar cuando detecta el texto contenido 
		 * entre el inicio y fin de una etiqueta
		 * @param buf Arreglo de caracteres
		 * @param offset Offset
		 * @param len Longitud
		 * @throws org.xml.sax.SAXException
		 */
		public void characters(char[] buf, int offset, int len)
				throws SAXException {
			String s = new String(buf, offset, len);
			if (textBuffer == null) {
				textBuffer = new StringBuffer(s);
			} else {
				textBuffer.append(s);
			}
			if (this.elemento != null) {
				System.out.println(this.elemento + "=" + textBuffer.toString().trim());
				m.put(this.elemento, textBuffer.toString().trim());
			}
		}
		
		/**
		 * Obtiene la cadena leida del color de fondo
		 * @return cadena con el color obtenido
		 */
		public String getColor() {
			return (m!=null)?(String)m.get("color_fondo"):"";
		}
	} //fin static class
}







