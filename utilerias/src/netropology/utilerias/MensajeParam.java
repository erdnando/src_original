package netropology.utilerias;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * Maneja los mensajes parametrizados por idioma.
 * @author Gilberto Aparicio
 */
public class MensajeParam implements java.io.Serializable {
	
	private static Map mensajesParametrizados = new HashMap();
	
	static {
		System.out.println("Cargando Textos Parametrizados");
		MensajeParam.cargaTextosParametrizados();
	}
	
	public MensajeParam() {}
	

	/**
	 * Realiza la carga en memoria de textos parametrizados
	 *
	 */
	public static void cargaTextosParametrizados() {

		try {

			String[] arrBundles = {"mensajesEPO", "mensajesPYME", "mensajesIF"};
		
			Map mensajesEspanol = new HashMap();
			Map mensajesIngles = new HashMap();
	
			// Obtiene las llaves y la coloca en un mapa
			// si la llave tiene un _ES es espa�ol y si
			// tiene _EN es ingles

			for (int i = 0; i < arrBundles.length; i++){
				ResourceBundle bundle = ResourceBundle.getBundle(arrBundles[i]);
				Enumeration enumx = bundle.getKeys();
				while (enumx.hasMoreElements()) {
					String key = (String) enumx.nextElement();
					if (key.indexOf("_EN") != -1) {
						String objeto = (String) bundle.getObject(key);
						mensajesIngles.put(key.substring(0,key.indexOf("_EN")), objeto);
					} else if (key.indexOf("_ES") != -1) {
						String objeto = (String) bundle.getObject(key);
						mensajesEspanol.put(key.substring(0,key.indexOf("_ES")), objeto);
					}
				}
			}
			
			MensajeParam.mensajesParametrizados.put("EN", mensajesIngles);
			MensajeParam.mensajesParametrizados.put("ES", mensajesEspanol);

		} catch(RuntimeException e) {	//Si no se encuentra el properties u otro error ocurre en tiempo de ejecuci�n
			System.out.println("Error al cargar mensajes de EPO. " +
					"No se puede continuar. " + e.getMessage());
			e.printStackTrace();
			throw e;
		}
	
	}
	
	/**
	 * Obtiene el mensaje en el idioma especificado
	 * @param claveMensaje Clave del mensaje del tipo "MSG_0001"
	 * @param idioma Idioma del mensaje
	 * @return Mensaje correspondiente a la clave proporcionada
	 */
	public static String getMensaje(String claveMensaje, String idioma) {
		
		if (claveMensaje == null || idioma == null) {
			System.out.println("claveMensaje=" + claveMensaje + "\n" +
					"idioma=" + idioma);
			throw new IllegalArgumentException("MensajeParam:getMensaje():Error de parametros");
		}
		
		String texto =(String) ((Map) mensajesParametrizados.get(idioma)).get(claveMensaje);

		if (texto==null) {
			if (mensajesParametrizados.size() == 0) {
				throw new RuntimeException("ERROR FATAL: Mensajes parametrizados no cargados");
			} else {
				texto = "???" + claveMensaje + "_" + idioma + "???";
			}
		}
		
		return texto;
	}
}
	


