package netropology.utilerias.caracterescontrol;

/**
 * Interface que define los m�todos a implementar para registrar las
 * l�neas donde se detecter caracteres de control no permitidos.
 * Se usa en conjunto con el thread BuscaCaracteresControlThread.
 * <br>
 * Implementaciones conocidas: 
 * <br>
 * 	netropology.utilerias.caracterescontrol.ResumenBusqueda
 *
 * @author jshernandez
 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 18/06/2015 06:07:30 p.m.
 *
 */
public interface IRegistraLineasCaracteresControl  {

	/**
	 * Registra el n�mero de l�nea en la que se encontr� un caracter de control no permitido por
	 * el est�ndar 1.0 de XML.
	 *
	 * Se invoca s�lo una vez por l�nea. Se asume que numeroLinea = 0 para la primera l�nea 
	 * del archivo validado.
	 * 
	 * @throws Exception
	 *
	 * @param numeroLinea Linea en la cual se encontr� el caracter de control.
	 */
	void registraLinea(long numeroLinea) 
		throws Exception;
	
	/**
	 * Se invoca s�lo una vez cuando en el thread BuscaCaracteresControlThread,
	 * se habilita un n�mero l�mite de reportes de l�neas con caracteres de control:
	 *
	 * 	buscaCaracteresControlThread.setNumeroMaximoReportes(numeroMaximo)
	 *
	 * Se invoca cuando se excede este l�mite.
	 *
	 * @throws Exception
	 *
	 * @param String con el mensaje para indicar que  se alcanz� el l�mite m�ximo
	 * de casos, por ejemplo: 
	 *   "S�lo se muestran las primeras 1,000 l�neas con caracteres de control inv�lidos"
	 */
	void registraLimiteMaximo(String mensaje)
		throws Exception;
		

	/**
	 * Se invoca s�lo cuando el thread BuscaCaracteresControlThread pasa a estatus <tt>FINALIZADO</tt>,
	 * si se habilit� la funcionalidad de crear archivo detalle y si se registraron caracteres de control.
	 * En caso contrario el thread registra <tt>null</tt>.
	 * <br>
	 * Se invoca s�lo la primera vez que se detecta una l�nea con caracteres de control.
	 *
	 * @throws Exception
	 *
	 * @param String Nombre del Archivo de Carga modificado con los mensajes de error.
	 *
	 */
	void registraNombreArchivoDetalle(String nombreArchivoDetalle)
		throws Exception;
		
}