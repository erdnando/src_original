package netropology.utilerias.caracterescontrol;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import org.apache.commons.logging.Log;

import java.text.MessageFormat;

import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
/**
 *
 * Thread que se usa para validar si un archivo proporcionado posee caracteres de control
 * no permitidos por el est�ndar <tt>1.0</tt> de <tt>XML</tt>:
 * <br>
 * 	http://www.w3.org/TR/REC-xml/#charsets
 * <br>
 *    -> S�lo est�n permitidos los caracteres de control: El tabulador 
 *    horizontal (<tt>\t</tt>), el salto de l�nea (<tt>\n</tt>) y el retorno de carro: (<tt>\r</tt>).
 * <br>
 * S�lo se realiza la b�squeda si el archivo proporcionado cumple con alguna de las siguientes
 * extensiones: <tt>txt</tt> y/o <tt>zip</tt>.
 * <br>
 *   -> Por default s�lo se realiza la b�squeda en archivos (parametro <tt>rutaArchivo</tt>), cuya extensi�n
 *   es <tt>txt</tt>.
 * <br>
 *  NOTA: EN CASO DE RECIBIR UN ARCHIVO ZIP, SE ASUME QUE ESTE S�LO TRAE UN ARCHIVO COMPRIMIDO. POR LO
 *  QUE NO SE REPORTARAN CARACTERES DE CONTROL INV�LIDOS EN ARCHIVOS POSTERIORES AL PRIMERO.
 * <br>
 * IMPORTANTE:<br>
 *   COMO S�LO SE REALIZA LA VALIDACI�N SI EL ARCHIVO PROPORCIONADO CUMPLE CON ALGUNA DE LAS SIGUIENTES
 *   EXTENSIONES: <tt>TXT</tt> Y/O <tt>ZIP</tt> (ADEM�S, EN EL CASO DE QUE SE VALIDEN ARCHIVOS <tt>ZIP</tt>, LA EXTENSI�N DEL 
 *   ARCHIVO COMPRIMIDO DEBE SER <tt>TXT</tt>), NO SE REALIZAR� NINGUNA B�SQUEDA DE CARACTERES DE CONTROL INVALIDOS 
 *   POR LO QUE EN TODO DONDE SE OCUPE ESTA CLASE REVISAR QUE EN LAS VALIDACIONES ESPECIFICAS DE LA APLICACION, 
 *   SE TENGA UNA SECCI�N DONDE SE VALIDE LA EXTENSI�N. SI SE ENV�A UN <tt>ZIP</tt> ADEM�S SE DEBER� VERIFICAR QUE SE
 *   VALIDE EL FORMATO DEL ARCHIVO COMPRIMIDO, COMO POR EJEMPLO:
 *	  <code>
 *			rutaArchivo = ComunesZIP.descomprimirArchivoSimple(fis, strDirectorioTemp);
 *   		if ( rutaArchivo == null || rutaArchivo.equals("") || !"txt".equals(Comunes.getExtensionNombreArchivo(rutaArchivo)) ) {
 *				ok_file = false;	
 *			}
 *   </code>
 *   PARA RECORDAR AL PROGRAMADOR QUE DEBE REVISAR QUE SE REALICEN ESTAS VALIDACIONES, SE CRE� EL ATRIBUTO
 *   <tt>validacionPosteriorExtension</tt>, EL CUAL DEBE PONER EN <tt>true</tt> PARA EVITAR QUE SE LANCEN EXCEPCIONES
 *   Y CON EL PROPOSITO DE QUE ASUMA EL COMPROMISO DE QUE EN LA APLICACI�N ORIGINAL SE VALIDEN LAS
 *   EXTENSIONES.
 * <br>
 *
 * @author jshernandez
 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 18/06/2015 06:07:30 p.m.
 *
 */
public class BuscaCaracteresControlThread extends Thread {
	
	// Variable para enviar mensajes al log.
	private static 		Log log 			= ServiceLocator.getInstance().getLog(BuscaCaracteresControlThread.class);
	
	// Valores posibles para el numeroMaximoReportes
	private static final int SIN_LIMITE = 0;
	
	// Valores posibles para el estado del thread
	public static final int NUEVO			= 100;
	public static final int PROCESANDO 	= 200;
	public static final int FINALIZADO	= 300;
	public static final int ERROR			= 400;
	
	private String	 									rutaArchivo;
	private IRegistraLineasCaracteresControl	registrador;
	private boolean 									stopThread;
	
	private long      		cuentaLinea									= 0;
	private long	 			numeroTotalLineas							= 0;
	
	private int					numeroMaximoReportes   					= SIN_LIMITE;
	private long				cuentaReportes								= 0;
	private String  			mensajeNumeroMaximoReportes 			= "S�lo se muestran los primeros %s registros con error."; 	
	private String  			mensajeArchivoDetalleLimiteMaximo 	= "... S�lo se muestra el detalle de los primeros %s registros con error. El archivo fue truncado."; 
	private final String 	CODIFICACION_ARCHIVO						= "ISO-8859-1";
	
	private boolean 			processZip									= false;
	private boolean 			processTxt									= true;
	private boolean 			doProcessFile								= false;
	private boolean			validacionPosteriorExtension			= false;	// Para recordar al programador que debe
																								// revisar que en la aplicaci�n se valide
																								// la extensi�n del archivo proporcionado.
																						
	
	private String 			alias											= null;
	private int 				estatus 										= BuscaCaracteresControlThread.NUEVO;
	private String 			mensaje										= null;
	
	private boolean 			creaArchivoDetalle						= false;
	private String 			directorioTemporal						= null;
	private String 			separador									= null;
	private String 			mensajeArchivoDetalle					= " Se detectaron caracteres de control no permitidos (excluyendo" +
																					  " tabulador (\\t), salto de l�nea (\\n) y retorno de carro (\\r)).";
	private String 			nombreArchivoDetalle 					= null;
	
	public BuscaCaracteresControlThread(){
		super();
		alias = Comunes.cadenaAleatoria(10);
		setStatus(
			BuscaCaracteresControlThread.NUEVO,
			(String) null
		);
	}
	
	public void run(){
	
		log.info("run."+alias+"(E)");
		
		setStatus(
			BuscaCaracteresControlThread.PROCESANDO,
			null
		);
		
		ZipFile 						zipFile					= null;
		ZipArchiveEntry 			zipArchiveEntry 		= null;
		String 						linea 					= null;		
		InputStreamReader			fileReader				= null;
		BufferedReader 			br 						= null;
		
		boolean						esArchivoZip 			= false;
		String 						nombreArchivo			= null;
		
		File 							archivoDetalle			= null;
		ZipArchiveOutputStream 	archivoDetalleZos		= null;
		BufferedWriter				archivoDetalleWriter = null;
		
		try {
			
			// DETERMINAR SI SE TIENE PERMITIDO PROCESAR EL ARCHIVO
			
			// Si la extension del archivo proporcionado no se encuentra dentro de las permitidas, 
			// no hacer nada.
			
			if(        rutaArchivo != null && processZip && rutaArchivo.matches("(?i)^.+\\.zip$") ){
				doProcessFile = true;
			} else if( rutaArchivo != null && processTxt && rutaArchivo.matches("(?i)^.+\\.txt$") ){
				doProcessFile = true;
			}
			if( rutaArchivo != null && !doProcessFile ){
				
				if( !validacionPosteriorExtension ){
					log.error("run."+alias+": La extensi�n del archivo proporcionado: " + rutaArchivo + ", no es v�lida.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"La extensi�n del archivo proporcionado: " + rutaArchivo + ", no es v�lida."
					);
				} else {
					log.info("run."+alias+": El archivo proporcionado: " + rutaArchivo + " no podr� ser procesado, porque su extension no est� dentro de las permitidas.");
					setStatus(
						BuscaCaracteresControlThread.FINALIZADO,
						null
					);
				}
				return;
				
			}
			
			// Si se habilit� bandera para crear archivo de carga con el detalle de las l�neas con caracteres
			// de control, verificar, que se hayan proporcionado todos los argumentos requeridos
			
			if( this.creaArchivoDetalle ){
				
				if(        Comunes.esVacio(this.directorioTemporal) ){
					log.error("run."+alias+": El atributo 'directorioTemporal' es requerido.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"El atributo 'directorioTemporal' es requerido."
					);
					return;
				} else if( this.separador == null                   ){
					log.error("run."+alias+": El atributo 'separador' es requerido.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"El atributo 'separador' es requerido."
					);
					return;
				} else if( Comunes.esVacio(mensajeArchivoDetalle)   ){
					log.error("run."+alias+": El atributo 'mensajeArchivoDetalle' es requerido.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"El atributo 'mensajeArchivoDetalle' es requerido."
					);
					return;
				}
				
			}
				
			// DETERMINAR SI EL ARCHIVO PROPORCIONADO ES UN ARCHIVO ZIP 
			
			esArchivoZip = !Comunes.esVacio(rutaArchivo) && rutaArchivo.matches("(?i)^.+\\.zip$")?true:false;
			
			// Si es archivo ZIP inicializar zipArchiveEntry
			if(esArchivoZip){
				
				Enumeration<ZipArchiveEntry>			entries = null;
				
				zipFile 				= new ZipFile( rutaArchivo );
				entries 				= (Enumeration<ZipArchiveEntry>) zipFile.getEntries();
				
				zipArchiveEntry 	= entries.hasMoreElements() ? (ZipArchiveEntry) entries.nextElement():null;
				
			}
			
			// VALIDAR ARGUMENTOS DE ENTRADA
			
			// Validar que se haya proporcionado el atributo ruta archivo.
			if ( Comunes.esVacio(rutaArchivo) ){
				setStatus(
					BuscaCaracteresControlThread.ERROR,
					"El atributo 'rutaArchivo' es requerido."
				);
				return;
			} else if ( esArchivoZip && zipArchiveEntry == null  ){
				setStatus(
					BuscaCaracteresControlThread.FINALIZADO,
					"El archivo zip proporcionado viene vac�o."
				);
				return;
			// Validar que se haya proporcionado el atributo registrador.
			} else if( registrador == null ){
				setStatus(
					BuscaCaracteresControlThread.ERROR,
					"El atributo 'registrador' es requerido."
				);
				return;
			}
		
			// VALIDAR EL FORMATO DEL ARCHIVO PROPORCIONADO
			
			// Obtener nombre del archivo
			if( esArchivoZip && zipArchiveEntry != null ){
				nombreArchivo = zipArchiveEntry.getName();
			} else {
				nombreArchivo = (new File(rutaArchivo)).getName();
			}
			
			if( esArchivoZip && !nombreArchivo.matches("(?i)^.+\\.txt$") ){
				if( !validacionPosteriorExtension ){
					log.error("run."+alias+": La extensi�n del archivo comprimido: "+nombreArchivo+", debe ser txt.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"La extensi�n del archivo comprimido: "+nombreArchivo+", debe ser txt."
					);
				} else {	
					log.info("run."+alias+": El archivo proporcionado: " + rutaArchivo + " no podr� ser procesado. La extensi�n del archivo comprimido: "+nombreArchivo+", debe ser txt.");
					setStatus(
						BuscaCaracteresControlThread.FINALIZADO,
						null
					);
				}
				return;
			}

			// CONTAR NUMERO DE REGISTROS A VALIDAR
			
			// Obtener InputStreamReader
			if(	esArchivoZip  ){
				fileReader 	= new InputStreamReader(zipFile.getInputStream(zipArchiveEntry), CODIFICACION_ARCHIVO );
			} else {
				fileReader 	= new InputStreamReader(new FileInputStream( rutaArchivo ),CODIFICACION_ARCHIVO);
			}
			// Contar registros
			br = new BufferedReader(fileReader);
			while( (linea = br.readLine()) != null) {
				++numeroTotalLineas;
			}
			if( numeroTotalLineas == 0 ){
				log.info("run."+alias+": El archivo proporcionado: " + rutaArchivo + " no podr� ser procesado, porque viene vac�o.");
				setStatus(
					BuscaCaracteresControlThread.FINALIZADO,
					"El archivo proporcionado: " + rutaArchivo + " no podr� ser procesado, porque viene vac�o."
				);
				return;
			}
			br.close();
			
			// REALIZAR BUSQUEDA DE CARACTERES DE CONTROL

			// Obtener OutputStreamWriter del archivo de detalle
			
			if( esArchivoZip ){
				
				nombreArchivoDetalle = Comunes.cadenaAleatoria(10) + ".zip";
				archivoDetalle			= new File(directorioTemporal+nombreArchivoDetalle);
				
				archivoDetalleZos 	= new ZipArchiveOutputStream(archivoDetalle);
				ZipArchiveEntry 		archivoDetalleComprimido = new ZipArchiveEntry(nombreArchivo); // Usar el nombre del archivo comprimido original
				archivoDetalleComprimido.setTime(System.currentTimeMillis());
				archivoDetalleZos.putArchiveEntry(archivoDetalleComprimido);
				
				archivoDetalleWriter = 	new BufferedWriter( 
													new OutputStreamWriter(archivoDetalleZos,CODIFICACION_ARCHIVO),
													1024
												);
				
			} else {
				
				nombreArchivoDetalle = Comunes.cadenaAleatoria(10) + ".txt";
				archivoDetalle			= new File(directorioTemporal+nombreArchivoDetalle);
				
				FileOutputStream 		archivoDetalleOs = new FileOutputStream(archivoDetalle);
				archivoDetalleWriter = 	new BufferedWriter(
													new OutputStreamWriter(archivoDetalleOs,CODIFICACION_ARCHIVO),
													1024
												);
				
			}
			
			// Obtener InputStreamReader
			if( esArchivoZip ){
				fileReader 	= new InputStreamReader(zipFile.getInputStream(zipArchiveEntry), CODIFICACION_ARCHIVO );
			} else {
				fileReader 	= new InputStreamReader(new FileInputStream( rutaArchivo ),CODIFICACION_ARCHIVO);
			}
			br = new BufferedReader(fileReader);
			
			// Buscar caracteres de control
			int 		c 											= -1;
			int 		anterior 								= -1;
			boolean 	hayCaracterControl 					= false;
			boolean 	hayCaracterControlLineaAnterior 	= false;
			boolean	hayArchivoRegistrado 				= false;
			int 		ctaCaracteres							= 0;
			boolean	limiteExcedido							= false;
			do {
				
				// REVISAR SI SE SOLICIT� QUE SE DETENGA LA BUSQUEDA DE CARACTERES DE CONTROL
				
				if( stopThread ){
					log.info("run."+alias+": Se solicit� que el thread se detuviera.");
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"Se solicit� que el thread se detuviera."
					);
					return;
				}
			
				// LEER CARACTER A VALIDAR
				
				c = br.read();
				
				// DETERMINAR SI EL CARACTER LE�DO CORRESPONDE A UN CARACTER DE CONTROL
				
				if( !hayCaracterControl && Comunes.tieneCaracterControlNoPermitidoEnXML1(c) ){
					
					// Habilitar bandera para indicar que la l�nea actual posee caracteres de control
					// y no se reporte 2 veces.
					hayCaracterControl = true;
					// Actualizar conteo de reportes.
					cuentaReportes++;
					
					// Registrar archivo de carga donde adem�s se est� guardando el detalle de los
					// l�neas que presentan caracteres de control.
					if( !hayArchivoRegistrado ){
						registrador.registraNombreArchivoDetalle(nombreArchivoDetalle);
						hayArchivoRegistrado = true;
					}
					
					// Registrar que se ha pasado el n�mero l�mite de caracteres de control reportados
					// por l�nea.
					if( numeroMaximoReportes > 0 && cuentaReportes > numeroMaximoReportes ){

						registrador.registraLimiteMaximo(
							MessageFormat.format(
															mensajeNumeroMaximoReportes,
                                             new Object[]{
                                                Comunes.formatoDecimal(numeroMaximoReportes,0,true)
                                             }
                                          )
						);
						
						limiteExcedido	= true;
						
					// Invocar registrador para registre el caracter de control encontrado en la l�nea actual.
					} else {
						registrador.registraLinea(cuentaLinea);
					}
					
				}
				
				// AGREGAR CONTENIDO AL ARCHIVO DE DETALLE 
				
				if ( this.creaArchivoDetalle ){
					
					if( c == '\n' ){
						
						if( anterior != '\r' && hayCaracterControl && !limiteExcedido ){
							archivoDetalleWriter.write(separador);
							archivoDetalleWriter.write(mensajeArchivoDetalle);
							ctaCaracteres += separador.length() + mensajeArchivoDetalle.length();
						}
						
						archivoDetalleWriter.write(c);
						ctaCaracteres++;

					} else if( c == '\r' ){
						
						if( hayCaracterControl && !limiteExcedido ){

							archivoDetalleWriter.write(separador);
							archivoDetalleWriter.write(mensajeArchivoDetalle);
							ctaCaracteres += separador.length() + mensajeArchivoDetalle.length();
							
						}
						
						archivoDetalleWriter.write(c);
						ctaCaracteres++;
					
					} else if( c == -1 && anterior != '\r' && anterior != '\n' && anterior != -1 ){
						
						// Nota: Si c == -1 y anterior != -1   // Final de l�nea sin considerar archivo vac�o
						//       Si c == -1 y anterior != '\n' // Final de l�nea sin salto de l�nea anterior
						// 	   Si c == -1 y anterior != '\r' // Final de l�nea sin salto de l�nea anterior
						if( hayCaracterControl && !limiteExcedido ){
							archivoDetalleWriter.write(separador);
							archivoDetalleWriter.write(mensajeArchivoDetalle);
							ctaCaracteres += separador.length() + mensajeArchivoDetalle.length();
						}
						
					} else if( c != -1 ){
						
						archivoDetalleWriter.write(c);
						ctaCaracteres++;

					}
					
					// Realizar flush cuando se hayan escrito cuando m�nimo 1024 caracteres.
					if( ctaCaracteres >= 1024 ){
						archivoDetalleWriter.flush();
						ctaCaracteres = 0;
					}
						
				}
				
				// DETERMINAR SI SE HA LLEGADO AL FINAL DE LA L�NEA
				
				if( anterior != '\r' && c == '\n' ){
					cuentaLinea++;
					hayCaracterControlLineaAnterior 	= hayCaracterControl;
					hayCaracterControl 					= false;
				} else if( c == '\r' ){
					cuentaLinea++;
					hayCaracterControlLineaAnterior 	= hayCaracterControl;
					hayCaracterControl 					= false;
				} else if( c == -1 && anterior != '\r' && anterior != '\n' && anterior != -1 ){
					// Nota: Si c == -1 y anterior != -1   // Final de l�nea sin considerar archivo vac�o
					//       Si c == -1 y anterior != '\n' // Final de l�nea sin salto de l�nea anterior
					// 	   Si c == -1 y anterior != '\r' // Final de l�nea sin salto de l�nea anterior
					cuentaLinea++;
					hayCaracterControlLineaAnterior 	= hayCaracterControl;
					hayCaracterControl 					= false;
				}
				
				// SI SE HA LLEGADO AL LIMITE M�XIMO DE REPORTES Y AL FINAL DE LA L�NEA, DETENER
				// LA VALIDACION.
				
				// Detener la b�squeda cuando se llegue a cierto n�mero l�mite de
				// caracteres de control encontrados.
				if( limiteExcedido ){ //if( numeroMaximoReportes > 0 && cuentaReportes > numeroMaximoReportes ){

					if ( creaArchivoDetalle && c == '\n'  ){
						
						archivoDetalleWriter.write(
							MessageFormat.format(
															mensajeArchivoDetalleLimiteMaximo,
															new Object[]{
																Comunes.formatoDecimal(numeroMaximoReportes,0,true)
															}
														)
						);
						
						setStatus(
								BuscaCaracteresControlThread.FINALIZADO,
								MessageFormat.format(
																mensajeNumeroMaximoReportes,
																new Object[]{
																	 Comunes.formatoDecimal(numeroMaximoReportes,0,true)
																}
															)
						);
						return;
						
					} else if( creaArchivoDetalle && anterior == '\r' && c != '\n' ){
						
						archivoDetalleWriter.write(
							MessageFormat.format(
															mensajeArchivoDetalleLimiteMaximo,
															new Object[]{
																Comunes.formatoDecimal(numeroMaximoReportes,0,true)
															}
														)
						);
						
						setStatus(
								BuscaCaracteresControlThread.FINALIZADO,
								MessageFormat.format(
																mensajeNumeroMaximoReportes,
																new Object[]{
																	 Comunes.formatoDecimal(numeroMaximoReportes,0,true)
																}
															)
						);
						return;
						
					}
						
				}
				
				// GUARDAR REGISTRO DEL CARACTER DE CONTROL ANTERIOR
				
				anterior = c;
				
			} while( c != -1 );

			// Fin de la B�squeda de Caracteres de Control
			setStatus(
				BuscaCaracteresControlThread.FINALIZADO,
				null
			);
			
		} catch(Exception e){
			
			log.error("run."+alias+"(Exception):");
			log.error("run."+alias+".rutaArchivo                 = <" + rutaArchivo                 + ">");
			log.error("run."+alias+".cuentaLinea                 = <" + cuentaLinea                 + ">");
			log.error("run."+alias+".numeroTotalLineas           = <" + numeroTotalLineas           + ">");
			log.error("run."+alias+".numeroMaximoReportes        = <" + numeroMaximoReportes        + ">");
			log.error("run."+alias+".cuentaReportes              = <" + cuentaReportes              + ">");
			log.error("run."+alias+".mensajeNumeroMaximoReportes = <" + mensajeNumeroMaximoReportes + ">");
			log.error("run."+alias+".CODIFICACION_ARCHIVO        = <" + CODIFICACION_ARCHIVO        + ">");
			log.error("run."+alias+".processZip                  = <" + processZip                  + ">");
			log.error("run."+alias+".processTxt                  = <" + processTxt                  + ">");
			log.error("run."+alias+".doProcessFile               = <" + doProcessFile               + ">");
			log.error("run."+alias+".alias                       = <" + alias                       + ">");
			log.error("run."+alias+".estatus                     = <" + estatus                     + ">");
			log.error("run."+alias+".mensaje                     = <" + mensaje                     + ">");
			log.error("run."+alias+".creaArchivoDetalle          = <" + creaArchivoDetalle          + ">");
			log.error("run."+alias+".directorioTemporal          = <" + directorioTemporal          + ">");
			log.error("run."+alias+".separador                   = <" + separador                   + ">");
			log.error("run."+alias+".mensajeArchivoDetalle       = <" + mensajeArchivoDetalle       + ">");
			log.error("run."+alias+":",e); // e.printStackTrace(); sincronizado con el mensaje de error
			
			setStatus(
				BuscaCaracteresControlThread.ERROR,
				"Ocurri� un error inesperado al realizar la b�squeda de caracteres de control. Error("+alias+"): " + e
			);
			
		} finally {
			
			if( archivoDetalleWriter != null ){
				try { archivoDetalleWriter.flush(); }catch(Exception e){
					
					log.error("run."+alias+"(Exception):");
					log.error("run."+alias+":",e); // e.printStackTrace(); sincronizado con el mensaje de error
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"Ocurri� un error inesperado al realizar la b�squeda de caracteres de control. Error("+alias+"): " + e
					);
					
				}
			}
			
			if( archivoDetalleZos != null ){
				try { archivoDetalleZos.closeArchiveEntry(); }catch(Exception e){
					
					log.error("run."+alias+"(Exception):");
					log.error("run."+alias+":",e); // e.printStackTrace(); sincronizado con el mensaje de error
					setStatus(
						BuscaCaracteresControlThread.ERROR,
						"Ocurri� un error inesperado al realizar la b�squeda de caracteres de control. Error("+alias+"): " + e
					);
					
				}
			}

			if( archivoDetalleWriter  	!= null) { try { archivoDetalleWriter.close();  }catch(Exception e){} }
			if( archivoDetalleZos  		!= null) { try { archivoDetalleZos.close();  	}catch(Exception e){} }
			
			if( br      					!= null) { try { br.close();      					}catch(Exception e){} }
      	if( zipFile 					!= null) { try { zipFile.close(); 					}catch(Exception e){} }
      	
      	log.info("run."+alias+"(S)");
      	
		}
		
	}
	
	//.........................................................
	
	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public IRegistraLineasCaracteresControl getRegistrador() {
		return registrador;
	}

	public void setRegistrador(IRegistraLineasCaracteresControl registrador) {
		this.registrador = registrador;
	}

	public boolean getStopThread() {
		return stopThread;
	}

	public void setStopThread(boolean stopThread) {
		this.stopThread = stopThread;
	}
	
	public int getNumeroMaximoReportes() {
		return numeroMaximoReportes;
	}

	public void setNumeroMaximoReportes(int numeroMaximoReportes) {
		this.numeroMaximoReportes = numeroMaximoReportes;
	}
	
	public String getMensajeNumeroMaximoReportes() {
		return mensajeNumeroMaximoReportes;
	}

	public void setMensajeNumeroMaximoReportes(String mensajeNumeroMaximoReportes) {
		this.mensajeNumeroMaximoReportes = mensajeNumeroMaximoReportes;
	}
	
	//.......................................................
	
	/**
	* Indicar que en caso de que se detecten l�neas con caracteres de control,
	* se crear� una "copia" del archivo de carga, al que se le agregar� la leyenda:
	* <pre>
	*   Se detectaron caracteres de control no permitidos (excluyendo 
	*   tabulador (\t), salto de l�nea (\n) y retorno de carro (\r)).
	* </pre>
	* al final de cada l�nea donde se detecten los caracteres. 
	*
	* @param creaArchivoDetalle <tt>true</tt> si se desea que se cree el archivo y
	*                     <tt>false</tt> para deshabilitar la caracteristica.
	*/
	public void setCreaArchivoDetalle(boolean creaArchivoDetalle){
		this.creaArchivoDetalle = creaArchivoDetalle;
	}
	
	/**
	* Configurar la ruta absoluta donde se desea que se cree la copia del archivo
	* carga con el detalle de las l�neas donde se detectaron estos caracteres.
	* 
	* @param directorioTemporal <tt>String</tt> con la ruta absoluta al directorio temporal.
	*
	*/
	public void setDirectorioTemporal(String directorioTemporal){
		this.directorioTemporal = directorioTemporal;
	}
	
	/**
	* Caracter que se usa para separar los campos de cada uno de los registros del
	* archivo de carga. Se requiere para construir el mensaje de detecci�n de caracteres
	* de control.
	*
	* @param separador <tt>String</tt> con la cadena que se usa para separar campos de los registros.
	*
	*/
	public void setSeparadorCampo(String separador){
		this.separador = separador;
	}
	
	/**
	 * Mensaje que se a�adir� al final de cada l�nea de la copia del archivo de
	 * carga donde se detectaron caracteres de control. S�lo es requerido si el
	 * atributo <code>creaArchivoDetalle = true</code>
	 *
	 * @param mensajeArchivoDetalle <tt>String</tt> con el detalle del mensaje.
	 *
	 */
	public void setMensajeArchivoDetalle(String mensajeArchivoDetalle) {
		this.mensajeArchivoDetalle = mensajeArchivoDetalle;
	}
	
	//.......................................................
	
	public boolean getProcessZip() {
		return processZip;
	}

	public void setProcessZip(boolean processZip) {
		this.processZip = processZip;
	}
	
	public boolean getProcessTxt() {
		return processTxt;
	}

	public void setProcessTxt(boolean processTxt) {
		this.processTxt = processTxt;
	}

	public void setValidacionPosteriorExtension(boolean validacionPosteriorExtension) {
		this.validacionPosteriorExtension = validacionPosteriorExtension;
	}
	
	//........................................................
	
	private synchronized void setStatus(int estatus, String mensaje){
		
		this.mensaje = mensaje;
		this.estatus = estatus;

	}
	
	public synchronized  int  getStatus(String[] mensaje){
		if( mensaje != null && mensaje.length > 0 ){
			mensaje[0] = this.mensaje;
		}
		return this.estatus;
		
	}
	
	public float getPorcentajeAvance(){
		
		if( 			this.estatus 		== BuscaCaracteresControlThread.FINALIZADO ){
			return 100.00f;
		} else if( 	this.estatus 		== BuscaCaracteresControlThread.NUEVO      ){
			return   0.00f;
		} else if(  numeroTotalLineas == 0 													 ){
			return 	0.00f;
		}
		
		// Calcular porcentaje de avance
		
		float porcentajeAvance = cuentaLinea / numeroTotalLineas * 100.00f;
		// Redondear porcentaje a dos decimales
		porcentajeAvance       = Math.round(porcentajeAvance*100.0f)/100.0f;
		
		return porcentajeAvance;
		
	}
	
}