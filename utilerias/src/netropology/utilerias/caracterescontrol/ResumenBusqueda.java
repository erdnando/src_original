package netropology.utilerias.caracterescontrol;

import netropology.utilerias.Comunes;

/**
 * Clase que se usa para registrar la l�neas donde se detectaron caracteres de control
 * no permitidos por el est�ndar 1.0 de XML. Se usa en conjunto con el thread:
 * BuscaCaracteresControlThread
 * <br>
 * Se utiliza el m�todo: getMensaje(), para obtener el detalle de la l�neas con 
 * caracteres de control inv�lidos. Si no hubiera ninguna l�nea registrada 
 * -- registraLinea(long numeroLinea) -- regresa <tt>null</tt>.
 * <br>
 * Mensajes de ejemplo:<br>
 * <tt>Se detectaron caracteres de control no permitidos (excluyendo tabulador 
 * (\t), salto de l�nea (\n) y retorno de carro (\r)) en las siguiente(s) 
 * l�nea(s): 6, 36, 24 y 86. Favor de verificarlo.</tt>
 * <br>
 * Para saber si se registraron l�neas con caracteres de control, usar el m�todo:<br>
 *    hayCaracteresControl()
 * <br>
 *
 * @author jshernandez
 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 18/06/2015 06:07:30 p.m.
 *
 */
public class ResumenBusqueda implements IRegistraLineasCaracteresControl {
	
	private	String			mensajeCabecera		= 	"Se detectaron caracteres de control no permitidos (excluyendo tabulador "  +
																	"(\\t), salto de l�nea (\\n) y retorno de carro (\\r)) en las siguiente(s) l�nea(s): ";
	private	String 			mensajePie				= 	"Favor de verificarlo.";
	
	private	String 			nombreArchivoDetalle =  null;
	
	private	boolean  		hayCaracteresControl = 	false;
	private	StringBuffer 	lineas 					= 	null;
	private 	String 			mensajeLimiteMaximo 	=  null;
	private  int				bufferCapacity			=  0;
	
	public ResumenBusqueda(){
		this(512);
	}
	
	public ResumenBusqueda(int bufferCapacity){
		this.bufferCapacity = bufferCapacity;
		lineas = new StringBuffer(bufferCapacity);
	}
	
	/**
    * Registra l�nea donde se encontr� con caracter de control invalido.
    * Asume que la numeraci�n de l�neas comienza en cero.
    *
    * @param numeroLinea Numero de l�nea donde se detect� un carcteres de control
    *                    invalido.
	 */
	public void registraLinea(long numeroLinea){
		if( hayCaracteresControl ){
			lineas.append(", ");
		}
		lineas.append(numeroLinea+1);
		hayCaracteresControl = true;
	}
	
	/**
	 * En dado caso se haya configurado un l�mite m�ximo de reportes en el thread:
	 *  buscaCaracteresControlThread.setNumeroMaximoReportes(numeroMaximo)
	 * y se haya excedido este n�mero, se invocar� este m�todo para registrar
	 * el mensaje que indicar se ha excedido este l�mite, por ejemplo:
	 *  
	 *  registraLimiteMaximo("S�lo se muestran las primeras 1,000 l�neas con error.")
	 *
	 * Este mensaje se utilizar� tambi�n para construir el detelle del mensaje 
	 * completo, el cual es proporcionado por el m�todo: <tt>getMensaje()</tt>
	 *
	 *
	 * @param mensaje String que indica que se ha alcanzado el n�mero m�ximo de reportes
	 *                de l�neas con caracteres inv�lidos.
	 */
	public void registraLimiteMaximo(String mensaje){
		mensajeLimiteMaximo = mensaje;
	}
	
	/**
	 *  Retorna <tt>true</tt> si se registr� al menos una l�nea con caracteres de control
	 *  no permitidos en el est�ndar 1.0 de XML. Retorna <tt>false</tt> en caso contrario.
	 */
	public boolean hayCaracteresControl(){
		return hayCaracteresControl;
	}
	
	/**
	 *  Devuelve un mensaje con el detalle de las l�neas donde se detectaron 
	 *  caracteres de control � null (si no se registr� ninguna l�nea con
	 *  caracteres de control).
	 * 
	 *  @param String con el detalle de las l�neas registradas con caracteres 
	 *  de control.
	 */
	public String getMensaje(){
	
		if( !hayCaracteresControl ){
			return null;
		}
		
		StringBuffer mensaje = new StringBuffer(this.bufferCapacity);
		
		// 1. Agregar cabecera del mensaje
		if( !Comunes.esVacio(mensajeCabecera) ){
			mensaje.append(mensajeCabecera);
		}
		
		// 2. Agregar lista de l�neas con caracteres de control
		mensaje.append(lineas);
		
		// Si no hay mensaje de l�mite m�ximo, reemplazar �ltima coma por la conjunci�n 'y'
		if( Comunes.esVacio(mensajeLimiteMaximo) ){
				
			int primerNumero = Comunes.esVacio(mensajeCabecera)?0:mensajeCabecera.length();
			int ultimaComa	  = mensaje.lastIndexOf(",");
			if( ultimaComa != -1 && ultimaComa >= primerNumero ){
				mensaje.replace(ultimaComa, ultimaComa + 1, " y");
			}
			mensaje.append(".");
			
		// En caso contrario, agregar mensaje de l�mite m�ximo alcanzado, si este existe
		} else { 
				mensaje.append("... ");
				mensaje.append(mensajeLimiteMaximo);
		}
		// 4. Agregar pie del mensaje
		if( !Comunes.esVacio(mensajePie) ){
			mensaje.append(" ");
			mensaje.append(mensajePie);
		}
		
		return mensaje.toString();
		
	}
	
	/**
	 * Se invoca s�lo cuando el thread BuscaCaracteresControlThread pasa a estatus <tt>FINALIZADO</tt>,
	 * si se habilit� la funcionalidad de crear archivo detalle y si se registraron caracteres de control.
	 * En caso contrario el thread registra <tt>null</tt>.
	 *
	 * @param String Nombre del Archivo de Carga modificado con los mensajes de error.
	 *
	 */
	 public void registraNombreArchivoDetalle(String nombreArchivoDetalle){
	 	 this.nombreArchivoDetalle = nombreArchivoDetalle;
	 }
	
	 /**
	 * Devuelve el archivo de carga modificado con el mensaje de error: <tt>Se detectaron caracteres 
	 * de control no permitidos (excluyendo tabulador (\t), salto de l�nea (\n) y retorno de carro 
	 * (\r))</tt> al final de cada l�nea donde se detectaron caracteres de control.
	 * <br>
	 * S�lo cuando el thread est� en estatus <tt>FINALIZADO</tt> regresa el archivo:
	 *    Si se detectaron caracteres de control y si se habilit� la funcionalidad de crear
	 *    archivo de detalle.
	 * En caso contrario regresa <tt>null</tt>.
	 *
	 */
	 public String getNombreArchivoDetalle(){
	 	 return this.nombreArchivoDetalle;
	 }
	
}