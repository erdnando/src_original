package netropology.utilerias;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase sirve para depurar queries que hacen uso de variables bind.
 * Basicamente lo que que hace es reemplazar todos los receptores de la variables bind (signo de
 * interrogacion que cierra <tt>?</tt> ) en el query por sus respectivos parametros.
 * Estos parametros pueden ser proporcionados en una lista o ser agregados
 * conforme se vayan leyendo.
 * <br><br>
 * Ejemplos de uso:
 * <br><br>
 * 1. El query y la lista de parametros ha sido definidos previamente:<br>
 * <br>
 * <tt>String query = "SELECT DATOS FROM TABLA WHERE CONDICION = ? ";</tt>
 * <br>
 * <tt>List parametros = new ArrayList();</tt><br>
 * <tt>parametros.add("valor1");</tt><br>
 * <br>
 * <tt>QueryDebugger qd = new QueryDebugger(query, parametros);</tt><br>
 * <tt>System.out.println(" QUERY: " + qd.getQueryStringWithParameters());</tt><br>
 * <br>
 * Imprime:<tt> QUERY: SELECT DATOS FROM TABLA WHERE CONDICION = 'valor1'</tt><br>
 * <br>
 * 2. El query ya ha sido definido, los parametros se agregaran dinamicamente<br>
 * <br>
 * <tt>QueryDebugger qd = new QueryDebugger(query);</tt><br>
 * <tt>qd.addParameter(new Integer(1));</tt><br>
 * <tt>System.out.println(" QUERY: " + qd.getQueryStringWithParameters());</tt><br>
 * <br>
 * Imprime:<tt> QUERY: SELECT DATOS FROM TABLA WHERE CONDICION = 1</tt><br>
 * <br>
 * 3. Otros tipos de objeto que se pueden agregar<br>
 * <br>
 * 		Objeto con valor null<br>
 *		<tt>qd.addParameter(null);</tt><br><br>
 *		Objeto String<br>
 *		<tt>qd.addParameter("condicion2");</tt><br><br>			
 *		Objeto Integer<br>
 *		<tt>qd.addParameter(new Integer(1));</tt><br><br>			
 *		Objeto Short<br>
 *		<tt>qd.addParameter(new Short((short)1));</tt><br><br>			
 *		Objeto BigInteger<br>
 *		<tt>qd.addParameter(new BigInteger("1234512345"));</tt><br><br>			
 *		Objeto BigDecimal<br>
 * 		<tt>qd.addParameter(new BigDecimal("6789678936.2365"));</tt><br><br>			
 *		Objeto Float<br>
 *		<tt>qd.addParameter(new Float("16.1616167"));</tt><br><br>			
 *		Objeto Double<br>
 *		<tt>qd.addParameter(new Double("16.1616167"));</tt><br><br>			
 *		Objeto Long<br>
 *		<tt>qd.addParameter(new Long("646464646464"));</tt><br><br>			
 *		Cualquier otro tipo de objeto es convertido a String<br>
 *		<tt>qd.addParameter(new Character('A'));</tt><br><br>		
 *		Tipo de Dato int<br>
 *		<tt>qd.addIntParameter(23);</tt><br><br>
 *		Tipo de Dato float<br>
 *		<tt>qd.addFloatParameter(0.5f);</tt><br><br>
 *		Tipo de Dato double<br>
 *		<tt>qd.addDoubleParameter(0.28);</tt><br><br>
 *		Tipo de Dato long<br>
 *		<tt>qd.addLongParameter(43L);</tt><br><br>
 *		Objeto String<br>
 *		<tt>qd.addStringParameter("NTK-090927-TLA");</tt><br><br>
 *<br>
 * @author Jesus Salim Hernandez David
 *
 */
public class QueryDebugger{
	
	private String 		query;
	private ArrayList 	parameters;

	/**
	 * Inicializa el nuevo objeto <tt>QueryDebugger</tt> con sus atributos <tt>query</tt> y <tt>parameters</tt>
	 * con cadena vacia y lista de parametros vacia.
	 */
	public QueryDebugger(){
		this(null,null);
	}
	
	/**
	 * Inicializa el nuevo objeto <tt>QueryDebugger</tt> con su atributo <tt>parameters</tt>
	 * como lista vacia; inicializa el atributo <tt>query</tt> con el valor especificado por la variable <tt>query</tt> 
	 * 
	 * @param query	Cadena de texto con el query en el que se usan variables bind(<tt>?</tt>).
	 *   
	 */
	public QueryDebugger(String query){
		this(query,null);
	}
	
	
	/**
	 * Inicializa el nuevo objeto <tt>QueryDebugger</tt>; el atributo <tt>parameters</tt>
	 * se inicializa con el valor de la variable <tt>parameters</tt> y el atributo <tt>query</tt> 
	 * se inicializa con el valor especificado por la variable <tt>query</tt> 
	 * 
	 * @param query			Cadena de texto con el query en el que se usan variables bind(<tt>?</tt>).
	 * @param parameters	Lista con los parametros se sustituiran a los <tt>?</tt> en el query.
	 */
	public QueryDebugger(String query,List parameters){
		setQuery(query);
		setParameterList(parameters);
	}
	
	/**
	 * Asigna al atributo <tt>query</tt>, el valor especificado en el parametro <tt>query</tt>
	 * 
	 * @param query	Cadena de texto con el query en el que se usan variables bind(<tt>?</tt>).
	 * 
	 */
	public void setQuery(String query){
		this.query = (query == null)?"":query.trim();
	}

	/**
	 * Asigna al atributo <tt>parameters</tt> la lista de parametros especificada en el argumento
	 * <tt>parameters</tt>.
	 *  
	 * @param parameters Lista con los parametros que seran sustituidos en el query. Esta lista 
	 * puede contener los siguientes objetos: String, Integer, Short, BigInteger, BigDecimal, Float, Double 
	 * y Long. Cualquier otro tipo de objeto es convertido a String. Si el objeto tiene valor null, este sera
	 * reemplazado por la cadena de texto <tt>NULL</tt>.    
	 * 
	 */
	public void setParameterList(List parameters){
		this.parameters 		= new ArrayList();
		List tempList	= (parameters 	== null)?new ArrayList():parameters;

		for(int i=0;i<tempList.size();i++){
			addParameter(tempList.get(i));
		}
	}
		
	/**
	 * Agrega un numero de tipo <tt>int</tt> a la lista de parametros del query( atributo <tt>parameters</tt>).
	 * 
	 * @param parametro Numero entero (<tt>int</tt>).
	 * 
	 */
	public void addIntParameter(int parametro){
		this.parameters.add(String.valueOf(parametro));
	}

	/**
	 * Agrega un numero de tipo <tt>float</tt> a la lista de parametros del query( atributo <tt>parameters</tt>).
	 * 
	 * @param parametro Numero flotante (<tt>float</tt>).
	 * 
	 */	
	public void addFloatParameter(float parametro){
		this.parameters.add(String.valueOf(parametro));
	}

	/**
	 * Agrega un numero de tipo <tt>double</tt> a la lista de parametros del query (atributo <tt>parameters</tt>).
	 * 
	 * @param parametro Numero de tipo <tt>double</tt>.
	 * 
	 */		
	public void addDoubleParameter(double parametro){
		this.parameters.add(String.valueOf(parametro));
	}
	
	/**
	 * Agrega un numero de tipo <tt>long</tt> a la lista de parametros del query (atributo <tt>parameters</tt>).
	 * 
	 * @param parametro Numero de tipo <tt>long</tt>.
	 * 
	 */	
	public void addLongParameter(long parametro){
		this.parameters.add(String.valueOf(parametro));
	}
	
	/**
	 * Agrega una cadena de texto (<tt>String</tt>) a la lista de parametros del query (atributo <tt>parameters</tt>).
	 * Si la cadena tiene valor <tt>null</tt>, este es convertido a la cadena <tt>NULL</tt> y
	 * agregado a lista de parametros. A todas la cadenas que se agreguen (con la excepcion de las que tienen valor <tt>null</tt>)
	 * a la lista de parametros, se les ponen comillas simples.
	 * 
	 * @param parametro Cadena de texto (<tt>String</tt>).
	 * 
	 */	
	public void addStringParameter(String parametro){
		this.parameters.add(addSingleQuotes(parametro));
	}
	
	/**
	 * Agrega comillas simples a la cadena de texto especificada en el parametro <tt>str</tt>.
	 * Si la cadena tiene valor <tt>null</tt> no agrega comillas simples, sino que regresa la
	 * cadena <tt>NULL</tt>.
	 * 
	 * @param str Cadena de texto (<tt>String</tt>).
	 * @return Cadena de texto con comillas simples, segun corresponda.
	 */
	private String addSingleQuotes(String str){
		if(str == null) return "NULL";
		return "'"+ str + "'";
	}
	
	/**
	 * Agrega un parametro de tipo: String, Integer, Short, BigInteger, BigDecimal, Float, Double 
	 * o Long a la lista de parametros del query (atributo <tt>parameters</tt>). Si el valor de la 
	 * variable <tt>parametro</tt> es <tt>null</tt>, a la lista de parametros del query se agrega un 
	 * objeto de tipo <tt>String</tt> con la siguiente cadena de caracteres "<tt>NULL</tt>". Cualquier otro tipo
	 * de objeto que se agregue a la lista de parametros mediante este metodo sera convertido a <tt>String</tt>
	 * usuando para ello su metodo <tt>toString()</tt>. A todos los objetos de tipo <tt>String</tt> se 
	 * les agregan comillas simples al ser agregados a lista de parametros.
	 *  
	 * @param parametro Objeto a agregar a la lista de parametros.
	 * 
	 */
	public void addParameter(Object parametro){
		
		String valor = "";

		// Convert all parameters to strings
		if(parametro == null){
			valor = "NULL";	
		}else if(parametro instanceof String){
			valor = addSingleQuotes((String)parametro);
		}else if(parametro instanceof Integer){
			valor = parametro.toString();
		}else if(parametro instanceof Short){
			valor = parametro.toString();
		}else if(parametro instanceof BigInteger){
			valor = parametro.toString();
		}else if(parametro instanceof BigDecimal){
			valor = parametro.toString();
		}else if(parametro instanceof Float){
			valor = parametro.toString();
		}else if(parametro instanceof Double){
			valor = parametro.toString();
		}else if(parametro instanceof Long){ 
			valor = parametro.toString(); 
		}else{
			valor = addSingleQuotes(parametro.toString());
		}
		
		this.parameters.add(valor);
		
	}
 
	/**
	 * Este metodo devuelve el numero total de caracteres <tt>?</tt> que se encuentren en el atributo <tt>query</tt>
	 * 
	 * @return Numero de Caracteres <tt>?</tt> que se encontraron.
	 * 
	 */
	private int getQuestionMarkCount(){
		int count = 0;
		for(int i = 0; i<this.query.length();i++){
			if(this.query.charAt(i) == '?'){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Devuelve el query con los "receptores de las variables bind" (<tt>?</tt>) reemplazados por los especificados en la lista de parametros (atributo <tt>parameters</tt>).
	 * Si la lista de "receptores de las variables bind" es menor que el numero de parametros, solo se toman los parametros necesarios para reemplazar cada uno de los receptores.
	 * Si hay menos parametros que receptores, solo se reemplazan los receptores con los que alcancen los parametros.
	 * Si no se especifico ningun query se devuelve una cadena vacia. Si no se especifico ninguna lista de parametros se devuelve
	 * el query con sus receptores de variables bind sin sustituir. 
	 * 
	 * @return Regresa el query proporcionado (atributo <tt>query</tt>) con los receptores de las variables bind reemplazados por los parametros especificados en la lista de parametros (atributo <tt>parameters</tt>).
	 */
	
	public String getQueryStringWithParameters(){
	
		StringBuffer buffer = new StringBuffer();
		
		// Poner validacion cuando no hay parametros y solo se imprima el query
		if(this.query.equals("")) 			return query;
		if(this.parameters.size() == 0) 	return query;
		
		// Obtner numero de ?
		int numberOfReplaces 	= getQuestionMarkCount();
		
		// Obtner numero de parameteros
		int numberOfParameters	= this.parameters.size();
		
		// Transformar query
		for(int i = 0, j = 0;i < this.query.length();i++){
			if( ( this.query.charAt(i) == '?' ) && ( j < numberOfParameters ) && ( numberOfReplaces > 0 ) ){
				buffer.append(this.parameters.get(j));
				numberOfReplaces--;
				j++;
			}else{
				buffer.append(this.query.charAt(i));
			}
		}
		
		// Regresar query
		return buffer.toString();
		
	}
	
}

