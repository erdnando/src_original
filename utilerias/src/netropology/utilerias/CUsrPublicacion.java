package netropology.utilerias;


public class CUsrPublicacion implements java.io.Serializable
{

    public CUsrPublicacion()
    {
        m_sFechaIni = "";
        m_sFechaFin = "";
        m_sNomPublicacion = "";
        m_sTipoPublicacion = "";
        m_sUsuarios = "";
        m_iCveEPO = 0;
    }

    public CUsrPublicacion(String s, String s1, String s2, String s3, String s4, int i)
    {
        m_sFechaIni = s1;
        m_sFechaFin = s2;
        m_sNomPublicacion = s;
        m_sTipoPublicacion = s3;
        m_sUsuarios = s4;
        m_iCveEPO = i;
    }

    public int getCveEpo()
    {
        return m_iCveEPO;
    }

    public String getFechaIni()
    {
        return m_sFechaIni;
    }

    public String getFechaFin()
    {
        return m_sFechaFin;
    }

    public String getNomPublicacion()
    {
        return m_sNomPublicacion;
    }

    public String getTipoPublicacion()
    {
        return m_sTipoPublicacion;
    }

    public String getUsuarios()
    {
        return m_sUsuarios;
    }

    public void setCveEpo(int i)
    {
        m_iCveEPO = i;
    }

    public void setFechaIni(String s)
    {
        m_sFechaIni = s;
    }

    public void setFechaFin(String s)
    {
        m_sFechaFin = s;
    }

    public void setNomPublicacion(String s)
    {
        m_sNomPublicacion = s;
    }

    public void setTipoPublicacion(String s)
    {
        m_sTipoPublicacion = s;
    }

    public void setUsuarios(String s)
    {
        m_sUsuarios = s;
    }

    private String m_sFechaIni;
    private String m_sFechaFin;
    private String m_sNomPublicacion;
    private String m_sTipoPublicacion;
    private String m_sUsuarios;
    private int m_iCveEPO;
}