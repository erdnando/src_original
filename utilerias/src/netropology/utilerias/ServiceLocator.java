package netropology.utilerias;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.rmi.PortableRemoteObject;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ServiceLocator{
	private static ServiceLocator serviceLocatorRef = null;
	private static Hashtable<String, Object> jndiCache = null;
	private static Hashtable<String, DataSource> dataSourceCache = null;

	static {
		serviceLocatorRef = new ServiceLocator();
	}

	/*Constructor privado del ServiceLocator*/
	private ServiceLocator(){
		jndiCache = new Hashtable<>();
		dataSourceCache = new Hashtable<>();
		 /*
		 * 
		 * Nota by jshernandez (08/04/2015 01:22:09 p.m.):
		 * 
		 * Log4j2 realiza la inicializaci�n de forma autom�tica.
		 *  
		 * El archivo de configuraci�n: log4j2.xml debe encontrarse dentro del
		 * classpath de la aplicacion para que sea encontrado. En caso de que no
		 * se encuentre se carga la configuraci�n b�sica, con nivel de 
		 * mensajes = ERROR.
		 * 
		 * Para una correcta inicializaci�n/deinicializaci�n y tambi�n para evitar FUGA DE MEMORIA
		 * EN TODA APLICACI�N WEB que use log4j2, SE DEBER� INCLUIR LA LIBRER�A log4j-web*.jar
		 * en su classpath:
		 * 
		 *       Using Log4j 2 in Web Applications
		 * 
		 *       You must take particular care when using Log4j or any other logging framework 
		 *       within a Java EE web application. It's important for logging resources to be 
		 *       properly cleaned up (database connections closed, files closed, etc.) when the 
		 *       container shuts down or the web application is undeployed. Because of the nature 
		 *       of class loaders within web applications, Log4j resources cannot be cleaned up 
		 *       through normal means. Log4j must be "started" when the web application deploys 
		 *       and "shutdown" when the web application undeploys. How this works varies 
		 *       depending on whether your application is a Servlet 3.0 or newer or 
		 *       Servlet 2.5 web application.
		 *       
		 *       http://logging.apache.org/log4j/2.x/manual/webapp.html
		 * 
		 */
	}

	/*
	 * El ServiceLocator esta implementado como Singleton.	getInstance()
	 * regresar� una referencia est�tica al ServiceLocator almacenado
	 * dentro dela clase ServiceLocator.
	 * @return Instancia unica de la clase
	 */
	public static ServiceLocator getInstance(){
		return serviceLocatorRef;
	}


	/**
	 * Realiza el lookup remoto en arbol de recurso de JNDI.
	 * @param ejb nombre del EJB
	 * @param clazz El tipo de la interfaz para accesar al EJB
	 * @return La referencia local o remota del EJB
	 */
	public <T> T remoteLookup(String ejb, Class<T> clazz) {
		
		//String jndiString = "java:global/nafin/modelEJB/" + ejb + "!" + clazz.getName();
		//En jndi siguiente es exclusivo de Weblogic. Por conveniencia, se adopta.
		String jndiString = ejb + "#" + clazz.getName();

	   Context context = null;
	   try {
	         context = this.getRemoteInitialContext();
	         Object jndiRef = context.lookup(jndiString);
	         return clazz.cast(PortableRemoteObject.narrow(jndiRef, clazz));
	   } catch (Throwable ex) {
	      throw new ServiceLocatorException("Error de lookup: " + jndiString + " " + ex, ex);
	   } finally {
	      try {
				if (context != null) {
					context.close();
				}
	      } catch (Throwable ex) {
	         throw new ServiceLocatorException("Error al cerrar InitialContext. " + ex, ex);
	      }
	   }
	}
	
	/**
	 * Realiza el lookup en arbol de recurso de JNDI.
	 * @param ejb nombre del EJB
	 * @param clazz El tipo de la interfaz para accesar al EJB
	 * @return La referencia local o remota del EJB
	 */
	public <T> T lookup(String ejb, Class<T> clazz) {
		
		//String jndiString = "java:global/Ne10g_DESARROLLO/modelEJB/" + ejb + "!" + clazz.getName();
		//En jndi siguiente es exclusivo de Weblogic. Por conveniencia, se adopta.
		String jndiString = ejb + "#" + clazz.getName();
		Object bean = lookup(jndiString);
		
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
	}

	/**
	 * Realiza el lookup en el arbol del JNDI, a partir de la cadena especificada
	 * como clave del recurso dentro de JNDI.
	 * @param jndiName Nombre registrado en el JNDI para el recurso
	 * @return Objeto del recurso
	 */
	public Object lookup(String jndiName) {
		Context context = null;
		try {
			// Revisa en cache si se encuentra el recurso especificado con el jndiName
			if (jndiCache.containsKey(jndiName)) {
				return jndiCache.get(jndiName);
			} else {
				//Si no est� en cache lo busca y lo pone en cache
				context = this.getInitialContext();
				Object jndiRef = context.lookup(jndiName);
				jndiCache.put(jndiName, jndiRef);
				return jndiRef;
			}
		} catch (NamingException ex) {
			throw new ServiceLocatorException("Error de lookup: " + jndiName + " " + ex, ex);
		} finally {
			try {
				if (context != null) {
					context.close();
				}
			} catch (NamingException ex) {
				throw new ServiceLocatorException("Error al cerrar InitialContext. " + ex, ex);
			}
		}
	}

	
	/**
	 * Obtiene el DataSource especificado (local)
	 * @param jndiDS Nombre jndi del DS a buscar
	 */
	public DataSource getDS(String jndiDS)
		throws ServiceLocatorException{
		String poolJdbc = jndiDS;
		try {
			/*Checking to see if the requested DataSource is in the Cache*/ 
			DataSource ds = null;
			if (dataSourceCache.containsKey(poolJdbc)) {
				 ds = dataSourceCache.get(poolJdbc);
			} else {
			/*
			 * The DataSource was not in the cache.	Retrieve it from JNDI
			 * and put it in the cache.
			 */
				Context ctx = this.getInitialContext();
				ds = (DataSource) ctx.lookup(poolJdbc);
				dataSourceCache.put(poolJdbc, ds);
			}
			return ds;
		} catch(NamingException e) {
			throw new ServiceLocatorException("A JNDI Naming exception has "+
																				"occurred in "+
																				"ServiceLocator.getDBConn()" , e);
		} catch(Exception e) {
			throw new ServiceLocatorException("An exception has occurred "+
																			 "in ServiceLocator.getDBConn()"	,e);
		}
	}

	/**
	 * Obtiene el DataSource especificado de manera remota (para procesos que funcionan fuera del servidor).
 	 * @param jndiDS Nombre jndi del DS a buscar
	 */
	public DataSource getRemoteDS(String jndiDS)
		throws ServiceLocatorException{
		String poolJdbc = jndiDS;
		try {
				Context ctx = this.getRemoteInitialContext();
				return ( (DataSource) ctx.lookup(poolJdbc) );
		} catch(NamingException e) {
			throw new ServiceLocatorException("JNDI Naming exception", e);
		} catch(Exception e) {
			throw new ServiceLocatorException("Error al obtener el datasource", e);
		}
	}


	/**
	 * Interfaz a Commons Logging. 
	 * Se emplea el Log de Commonns en lugar de usar directamente las clases del Log4J.
	 * Ya que commons logging puede ajustarse a diferentes esquemas de Log, ya sea
	 * Log4J, el est�ndar de JDK 1.4, o en caso de no estar disponible 
	 * ninguno de estos dos, utiliza un mecanismo simple.
	 * 
	 * Usar este m�todo para obtener la instancia de Log de una clase en particular
	 * 
	 * @param clase Intancia de Class. 
	 *  		Debe ser la clase de la cual se desea escribir mensajes en el log
	 * @return intancia de Log (Commons Logging)
	 */
	public Log getLog(Class clase) {
		return LogFactory.getLog(clase);
	}
	
	public Log getLog(String clase) {
		return LogFactory.getLog(clase);
	}
	
	/**
	 * Obtiene el contexto inicial del JNDI.
	 * Util para buscar EJBs y Conexiones con DataSource
	 */
	private Context getInitialContext() throws NamingException {
		Context ctx = new InitialContext();
		return ctx;
	}


	/**
	 * Obtiene el contexto inicial del JNDI, usando el archivo de propiedades
	 * ContextoBundle.properties
	 * Este m�todo es invocado desde getRemoteEJBHome()
	 * 
	 * Para su uso en clientes externos a la aplicaci�n J2EE
	 */
	private Context getRemoteInitialContext() throws NamingException, IOException {
		Properties prop = this.loadParams("ContextoBundle");
		Context ctx = new InitialContext(prop);
		return ctx;
	}
	
	/**
	 * Este m�todo lee el archivo de propiedades. No se debe especificar
	 * la extensi�n del archivo.
	 * El archivo debe de estar en una ruta que este contemplada dentro
	 * del CLASSPATH, de lo contrario no ser� localizado.
	 *
	 * @param archivo nombre del archivo properties (sin la extension)
	 * @return Properties El objeto de propiedades
	 * @exception IOException si la carga de propiedades falla.
	 */
	private Properties loadParams(String file)
			throws IOException {

		// Carga un ResourceBundle y apartir de este crea un Properties
		Properties		 prop	 = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);

		// Obtiene las llaves y la coloca en el objeto Properties
		Enumeration enumx = bundle.getKeys();
		String key	= null;
		while (enumx.hasMoreElements()) {
			key = (String) enumx.nextElement();
			prop.put(key, bundle.getObject(key));
		}

		return prop;
	}

}
