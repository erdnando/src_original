
//Title:        Combos.java
//Version:      2.0
//Copyright:    Copyright (c) 2001
//Author:       Ricardo Fuentes
//Company:      Netropology
//Description:  Realiza una consulta y guarda los elementos(valor y descripcion) en vectores,
//              mismo que se utilizar� para generar un combo que se puede obtener multiples veces
//Aplicacion:   En autorizaciones cuando el mismo combo aparece muchas veces solo se realiza una consulta, pero
//              a veces es necesario que diferentes elementos sean los que esten elegidos
package netropology.utilerias;

import java.sql.ResultSet;

import java.util.Vector;

public class Combos {
private ResultSet rs           = null;
private Vector    valores      = null;
private Vector    descrip      = null;
private String    combo        = "";
private String    jsArray      = "";
private AccesoDB  con          = null;


public Combos(String tipoConsulta, String valor)
  {
  String qrySentencia = "";
  valores = new Vector();
  descrip = new Vector();
  try
    {
	qrySentencia = ConstuyeQrySentencia(tipoConsulta, valor);
	System.out.println("Combos["+qrySentencia+"]");	
    con = new AccesoDB();	
	con.conexionDB();
    rs = con.queryDB(qrySentencia);
    while(rs.next())
    {
      valores.add(rs.getString(1));
      descrip.add(rs.getString(2));
    }
     con.cierraStatement();
    }
  catch(Exception e)
   {
   System.out.println("Combos Exception "+e);
   }finally {
	if(con.hayConexionAbierta())
    con.cierraConexionDB();
   }
  }

public Combos(Vector vecFilas){
	Vector vecColumnas = null;
	int i = 0;
	try{
		valores = new Vector();
		descrip = new Vector();
		
		for (i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			valores.add((String)vecColumnas.get(0));
			descrip.add((String)vecColumnas.get(1));
		}
	}catch(Exception e){
		System.out.println("Combos Exception "+e);
	}
}

public Combos(String qrySentencia,AccesoDB conn)
  {
  try
    {
    valores = new Vector();
    descrip = new Vector();
    con = conn;
    rs = con.queryDB(qrySentencia);
    while(rs.next())
      {
      valores.add(rs.getString(1));
      descrip.add(rs.getString(2));
      }
     con.cierraStatement();
     }
  catch(Exception e)
   {
   System.out.println("Combos Exception "+e);
   }
  }
  
  
public String getCombo(String valorSelected)
  {
  String valorActual = "";
  String descActual  = "";
  String sel         = "";
  int i=0;
  try
    {
    combo = "";
     for(i=0;i<valores.size();i++)
        {
        valorActual = (String)valores.get(i);
        descActual  = (String)descrip.get(i);
        if(valorSelected.equals(valorActual))
          sel = "selected";
        else
          sel = "";
        combo += "<option value='"+valorActual+"' "+sel+">"+descActual+"</option>";
        }
     }
  catch(Exception e)
    {
    System.out.println("Combos.getCombo Exception "+e);
    combo = "Error al generar lista";
    }
  return combo;
  }

public String getCombo(String valorSelected,String valorOmitir)
  {
  String valorActual = "";
  String descActual  = "";
  String sel         = "";
  int i=0;
  try
    {
    combo = "";
     for(i=0;i<valores.size();i++)
        {
        valorActual = (String)valores.get(i);
        descActual  = (String)descrip.get(i);
        if(valorSelected.equals(valorActual))
          sel = "selected";
        else
          sel = "";

        if(valorActual.indexOf(valorOmitir)<0)
        combo += "<option value='"+valorActual+"' "+sel+">"+descActual+"</option>";
        }
     }
  catch(Exception e)
    {
    System.out.println("Combos.getCombo Exception "+e);
    combo = "Error al generar lista";
    }
  return combo;
  }


public String getJSArray(String nombreArreglo)       //devuelve una cadena que forma un arreglo de JavaScript con la descripcion en el indice valor
  {
  String valorActual = "";
  String descActual  = "";
  String sel         = "";
  int i=0;
  try
    {
     jsArray = "var "+nombreArreglo+" = new Array();\n";
     for(i=0;i<valores.size();i++)
        {
        valorActual = (String)valores.get(i);
        descActual  = (String)descrip.get(i);
    		jsArray += nombreArreglo+"["+valorActual+"] = "+descActual+";\n";
        }
     }
  catch(Exception e)
    {
    System.out.println("Combos.getJSArray Exception "+e);
    jsArray = "<!--Error al generar arreglo-->";
    }
  return jsArray;
  }

public String getElemento(String elemento)
  {
  String valorActual  = "";
  String descActual   = "";
  String el           = "";
  int i=0;
  try
    {
     for(i=0;i<valores.size();i++)
        {
        valorActual = (String)valores.get(i);
        descActual  = (String)descrip.get(i);
        if(elemento.equals(valorActual))
      		el = descActual;
        }
     }
  catch(Exception e)
    {
    System.out.println("Combos.getJSArray Exception "+e);
    el = "error al obtener razon social";
    }
  return el;
  }

/*---------------------------------*/
/* Metodos del EJB LineaCreditoEJB */
/*---------------------------------*/
	public String ConstuyeQrySentencia(String tipoConsulta, String valor)
	{
	String qrySentencia = "";
    	try {
			if(tipoConsulta.equals("getEstatus"))
			{
				qrySentencia = "SELECT IC_ESTATUS_LINEA,CD_DESCRIPCION"+
				" FROM COMCAT_ESTATUS_LINEA";
				if("I".equals(valor))
					qrySentencia += " WHERE IC_ESTATUS_LINEA IN(2,3,4)";
				else if("IBC".equals(valor)) // Obtiene el estatus validos para IF
					qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion)"+
						" FROM COMCAT_ESTATUS_LINEA" +
						" WHERE IC_ESTATUS_LINEA IN(3,8)";
				else if("NBC".equals(valor)) // Obtiene el estatus validos para NAFIN
					qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion)"+
						" FROM COMCAT_ESTATUS_LINEA" +
						" WHERE IC_ESTATUS_LINEA IN(5,8)";
				else if("BC".equals(valor)) // Obtiene el estatus para bloquear y cancelar l�neas para IF
					qrySentencia += " WHERE IC_ESTATUS_LINEA IN(7,8)";
				else if("BCN".equals(valor)) // Obtiene el estatus para bloquear y cancelar l�neas para Nafin
					qrySentencia += " WHERE IC_ESTATUS_LINEA IN(9,10)";
				else
					qrySentencia += " WHERE IC_ESTATUS_LINEA IN(5,6)";
			}
			else if(tipoConsulta.equals("getBancos"))
			{
					qrySentencia =	" SELECT IC_FINANCIERA,CD_NOMBRE FROM COMCAT_FINANCIERA WHERE IC_TIPO_FINANCIERA = 1";
		    }
			else if(tipoConsulta.equals("getPlazos"))
			{
			        qrySentencia =	" SELECT IC_PLAZO,IN_PLAZO_DIAS FROM COMCAT_PLAZO WHERE ic_plazo in (11)";
		    }
	  	}catch(Exception e)
    	{
		    System.out.println("Combos.ConstuyeQrySentencia Exception "+e);
    	}
		return qrySentencia;
    }
	
	
}     //fin de la clase

