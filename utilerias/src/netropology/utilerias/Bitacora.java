package netropology.utilerias;

import java.io.File;
import java.io.FileInputStream;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

/**
 * Metodos para el manejo de la bitacora.
 * @author Gilberto Aparicio 03/11/2005
 *
 */

public class Bitacora {	
	/**
	 * Obtiene los cambios entre la nueva informaci�n y la anterior, 
	 * generando una cadena para registro dentro de la bit�cora.
	 * Las tres listas que recibe el m�todo deben ser del mismo 
	 * tama�o y los datos deben estar en el mismo orden para que la
	 * comparaci�n sea efectiva.
	 * 
	 * Los campos de tipo cadena que sean cadenas vacias se comparan
	 * como si fueran valores nulos ("" == null)
	 * 
	 * @param datosAnteriores Lista de los datos anteriores
	 * @param datosActuales Lista de los datos actuales
	 * @param nombresCampos Arreglo con los nombres de los campos, aquellos
	 * 		que tengan valor nulo, significa que se omitir� su revisi�n.
	 * 
	 * @return Lista con los cambios detectados
	 *		0 .- Cadena con los valores anteriores a la modificaci�n
	 * 		1 .- Cadena con los valores actuales a la modificaci�n
	 *
	 */
	public static List getCambiosBitacora(
			List datosAnteriores,
			List datosActuales,
			String[] nombresCampos) throws BitacoraException {
	
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (datosAnteriores == null || datosActuales == null ||
					nombresCampos == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}

			if (nombresCampos.length != datosActuales.size() || 
				nombresCampos.length != datosAnteriores.size()) {
				System.out.println("nombresCampos.length=" + nombresCampos.length + "\n" +
						"datosActuales.size()=" + datosActuales.size() + "\n" +
						"datosAnteriores.size()=" + datosAnteriores.size());

				throw new Exception("Las listas y el arreglo deben ser " +
						"del mismo tama�o");
			}

			
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" datosAnteriores=" + datosAnteriores + "\n" +
					" datosActuales=" + datosActuales + "\n" +
					" nombresCampos=" + nombresCampos);
			throw new BitacoraException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		List<String> cambios = new ArrayList<>();
		StringBuilder valoresAnteriores = new StringBuilder();
		StringBuilder valoresActuales = new StringBuilder();
		
		Iterator itAnterior = datosAnteriores.iterator();
		Iterator itActual = datosActuales.iterator();
		for(int i=0; i < nombresCampos.length; i++) {
			
			Object valorAnterior = itAnterior.next();
			valorAnterior = (valorAnterior != null && valorAnterior.equals(""))?null:valorAnterior;
			Object valorActual = itActual.next();
			valorActual = (valorActual != null && valorActual.equals(""))?null:valorActual;
	 		
			if(	nombresCampos[i] != null &&
					((valorAnterior != null && valorActual!=null && 
							!valorAnterior.equals(valorActual)) || 
					(valorAnterior != null && valorActual==null)|| 
					(valorAnterior == null && valorActual!=null)) ){
	
				valoresAnteriores.append(nombresCampos[i] + "=" + valorAnterior + "\n");
				valoresActuales.append(nombresCampos[i] + "=" + valorActual + "\n");
			}
		}
		cambios.add(valoresAnteriores.toString());
		cambios.add(valoresActuales.toString());
		
		return cambios;
	}



	/**
	 * Da formato a los valores proporcionados, anex�ndoles
	 * el nombre del campo.
	 * 
	 * @param datos Lista de los datos
	 * @param nombresCampos Lista con los nombres de los campos.
	 * 		Si el nombre del campo viene en "null" entonces ese
	 * 		valor no se considera.
	 * 
	 * @return Cadena con los valores en el formato para almacenar en bit�cora
	 *
	 */
	public static String getValoresBitacora(
			List datos, String[] nombresCampos) throws BitacoraException {
	
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (datos == null ||nombresCampos == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}

			if (nombresCampos.length != datos.size()) {
				throw new Exception("La lista y el arreglo deben ser " +
						"del mismo tama�o");
			}

			
		} catch(Exception e) {
			System.out.print("Error en los parametros recibidos. " + e.getMessage() +
					" datos=" + datos +
					" nombresCampos=[");
			for (int i=0; i< nombresCampos.length; i++) {
				System.out.print(((i>0)?",":"") + nombresCampos[i]);
			}
			System.out.println("]");
			throw new BitacoraException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		StringBuilder valores = new StringBuilder();
		
		Iterator itDatos = datos.iterator();


		for (int i=0; i < nombresCampos.length; i++) {
			Object valor = itDatos.next();
	 		
			if (nombresCampos[i] != null) {
				valores.append(nombresCampos[i] + "=" + valor + "\n");
			}
		}
		
		return valores.toString();
	}

	/**
	 * Realiza el insert de los cambios en la bitacora.
	 * NO REALIZA commit, el cual debe ser realizado por
	 * el m�todo que lo manda llamar.
	 * Si los datos anteriores y los actuales son los mismos
	 * significa que no hubo cambio y no guarda registro.
	 * 
	 * @param con Conexion de BD
	 * @param clavePantalla Clave de la pantalla 
	 * 		desde donde se origina el evento
	 * @param tipoAfiliado Tipo de afiliado al que afecta el cambio.
	 * 		E Epo, P Pyme, PI Pyme internacional...etc.
	 * @param claveNafinElectronico Clave de Nafin Electr�nico
	 * @param claveUsuario Clave del Usuario que realiza la operaci�n
	 * @param datoAnterior Informaci�n anterior a la operaci�n
	 * @param datoActual Informaci�n despu�s del cambio
	 * @param ic_epo Clave de la Epo (Cuando aplique)
	 */
	public static void grabarEnBitacora(AccesoDB con, String clavePantalla,
			String tipoAfiliado,
			String claveNafinElectronico, String claveUsuario,
			String datoAnterior, String datoActual, String ic_epo) 
			throws BitacoraException {
		

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (con == null ||clavePantalla == null || tipoAfiliado == null ||
					claveNafinElectronico == null || claveUsuario == null ||
					 datoAnterior == null || datoActual == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			
			if ( (datoAnterior.trim()).equals(datoActual.trim()) ) {
				//Si no hubo cambios no realiza el registro en bit�cora
				return;
			}
			
			if (!con.hayConexionAbierta()) {
				throw new Exception("La conexion a BD no es valida ");
			}

			
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" con=" + con + "\n" +
					" clavePantalla=" + clavePantalla + "\n" +
					" tipoAfiliado=" + tipoAfiliado + "\n" +
					" claveNafinElectronico=" + claveNafinElectronico + "\n" +
					" claveUsuario=" + claveUsuario + "\n" +
					" datoAnterior=" + datoAnterior + "\n" +
					" datoActual=" + datoActual);
			throw new BitacoraException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		
		String nombreCompletoUsuario = "";
		try {
			UtilUsr utilUsr = new UtilUsr();
			Usuario usuario = utilUsr.getUsuario(claveUsuario);
			nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno() + " " + usuario.getNombre();
		} catch (Exception e) {
			throw new BitacoraException("Error al obtener los datos del usuario");
		}
		
		
	   String strSQLBitacora = 
	               " INSERT INTO bit_cambios_gral ( " +
	               " ic_cambio, cc_pantalla_bitacora, cg_clave_afiliado," +
	               " ic_nafin_electronico, " +
	               " ic_usuario, cg_nombre_usuario, " +
	               " cg_anterior, cg_actual ,ic_epo ) "+
	               " VALUES(SEQ_BIT_CAMBIOS_GRAL.nextval,   ? ,  ? ,  ? , ? , ?, ?, ?, ?  )  ";
	               
	            List  lVarBind    = new ArrayList(); 
	            lVarBind.add(clavePantalla);
	            lVarBind.add(tipoAfiliado); 
	            lVarBind.add(claveNafinElectronico );
	            lVarBind.add(claveUsuario );
	            lVarBind.add(nombreCompletoUsuario );
	            lVarBind.add(datoAnterior );
	            lVarBind.add(datoActual );
	            lVarBind.add(ic_epo);
	                           
	         
	         System.out.println("strSQLBitacora  "+strSQLBitacora);
	         System.out.println("lVarBind "+lVarBind);
	            
				
		try {
			con.ejecutaUpdateDB(strSQLBitacora, lVarBind); 
			System.out.println("************Se ha agregado un nuevo registro a la bitacora general**********\n " + strSQLBitacora);
			
		} catch(Exception e) {
			//throw new BitacoraException("Error al guardar datos en bitacora");
		}
	}
	
	
	public static void grabarEnBitacora(AccesoDB con, String clavePantalla,
			String tipoAfiliado,
			String claveNafinElectronico, String claveUsuario,
			String datoAnterior, String datoActual) 
			throws BitacoraException {
			
			grabarEnBitacora(con, clavePantalla, tipoAfiliado, claveNafinElectronico,
									claveUsuario, datoAnterior,  datoActual,"") ;
	}

	/**
	 * Metodo para grabar la bitacora diaria por usuario que ingresa al sistema
	 * Es importante que este m�todo sea lo m�s eficiente posible porque ser�
	 * llamado miles de veces diariamente.
	 * @param url URL del recurso al que se ingreso
	 * @param usuario	Usuario
	 * @param operacion	Nombre de la operacion invocada
	 * @param idSesion Id de la sesion, para identificar el mismo usuario con dif. sesiones
	 */
	public static void grabarLog(String url, String usuario, String operacion, String idSesion) {
		boolean bOk = true;
		String sql = 
				" INSERT INTO bit_diaria_operacion (ic_usuario," +
				" cg_sesion, cg_url, cg_operacion) " +
				" VALUES(?,?,?,?)";
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			List<Object> params = new ArrayList<>();
			params.add(usuario);
			params.add(idSesion);
			params.add(url);
			params.add( ((operacion==null)?"":operacion) );
			con.ejecutaUpdateDB(sql, params);
			bOk = true;
		} catch(Throwable e) {
			bOk = false;
			System.out.println("Bitacora: Exception. No se pudo grabar en BIT_DIARIA_OPERACION" + e.getMessage());
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}
  
  /**
	 * Realiza el insert de los cambios en la bitacora.
	 * NO REALIZA commit, el cual debe ser realizado por
	 * el m�todo que lo manda llamar.
	 * Si los datos anteriores y los actuales son los mismos
	 * significa que no hubo cambio y no guarda registro.
	 * 
	 * @param con Conexion de BD
	 * @param clavePantalla Clave de la pantalla 
	 * 		desde donde se origina el evento
	 * @param tipoAfiliado Tipo de afiliado al que afecta el cambio.
	 * 		E Epo, P Pyme, PI Pyme internacional...etc.
	 * @param claveNafinElectronico Clave de Nafin Electr�nico
	 * @param claveUsuario Clave del Usuario que realiza la operaci�n
	 * @param datoAnterior Informaci�n anterior a la operaci�n
	 * @param datoActual Informaci�n despu�s del cambio
	 */
	public static void grabarEnBitacoraProvBaja(AccesoDB con, String clavePantalla,
			String tipoAfiliado,
			String claveNafinElectronico, String claveUsuario,
			String datoAnterior, String datoActual, String rutaZip, String rutaPDF) 
			throws BitacoraException {
		

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (con == null ||clavePantalla == null || tipoAfiliado == null ||
					claveNafinElectronico == null || claveUsuario == null ||
					 datoAnterior == null || datoActual == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			
			if ( (datoAnterior.trim()).equals(datoActual.trim()) ) {
				//Si no hubo cambios no realiza el registro en bit�cora
				return;
			}
			
			if (!con.hayConexionAbierta()) {
				throw new Exception("La conexion a BD no es valida ");
			}

			
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" con=" + con + "\n" +
					" clavePantalla=" + clavePantalla + "\n" +
					" tipoAfiliado=" + tipoAfiliado + "\n" +
					" claveNafinElectronico=" + claveNafinElectronico + "\n" +
					" claveUsuario=" + claveUsuario + "\n" +
					" datoAnterior=" + datoAnterior + "\n" +
					" datoActual=" + datoActual);
			throw new BitacoraException("Error en los parametros recibidos");
		}
		//***************************************************************************************

		
		String nombreCompletoUsuario = "";
		try {
			UtilUsr utilUsr = new UtilUsr();
			Usuario usuario = utilUsr.getUsuario(claveUsuario);
			nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno() + " " + usuario.getNombre();
		} catch (Exception e) {
			throw new BitacoraException("Error al obtener los datos del usuario");
		}
		
    String seqBit = "";
    try{
      String  strSQLBitacora = " SELECT SEQ_BIT_CAMBIOS_GRAL.nextval seqbit from DUAL ";
      Registros reg = con.consultarDB(strSQLBitacora);
      if(reg.next())
      {
        seqBit = reg.getString("seqbit");
      }
    }catch(Exception e)
    {
      throw new BitacoraException("Error al guardar datos en bitacora");
    }
		String strSQLBitacora = 
				" INSERT INTO bit_cambios_gral ( " +
				" ic_cambio, cc_pantalla_bitacora, cg_clave_afiliado," +
				" ic_nafin_electronico, " +
				" ic_usuario, cg_nombre_usuario, " +
				" cg_anterior, cg_actual ,bi_pdf, bi_zip) VALUES("+seqBit+", " +
				" '" + clavePantalla + "', " + 
				" '" + tipoAfiliado + "', " + 
				" " + claveNafinElectronico + ", " +
				" '" + claveUsuario + "','" + nombreCompletoUsuario + "'," +
				" '" + datoAnterior + "','" + datoActual + "',EMPTY_BLOB(),EMPTY_BLOB())";
				
		try {
			con.ejecutaSQL(strSQLBitacora);
      
      //String fileName = (String)hmFile.get("FILE_NAME");
      //String name = fileName.substring(0,fileName.indexOf("."));
      //String extension = fileName.substring(fileName.indexOf("."));
      System.out.println("rutaZip = = = "+rutaZip);
      PreparedStatement ps = null;
      File archivoZip = new File(rutaZip);
      FileInputStream fileinputstreamZip = new FileInputStream(archivoZip);
      
      File archivoPDF = new File(rutaPDF);
      FileInputStream fileinputstreamPDF = new FileInputStream(archivoPDF);
      
      strSQLBitacora = "UPDATE bit_cambios_gral " +
          "   SET bi_zip = ?, bi_pdf = ? " +
          " WHERE ic_cambio = ? ";
      
      ps = con.queryPrecompilado(strSQLBitacora);
      ps.setBinaryStream(1, fileinputstreamZip, (int)archivoZip.length());
      ps.setBinaryStream(2, fileinputstreamPDF, (int)archivoPDF.length());
      ps.setLong(3, Long.parseLong(seqBit));
      ps.executeUpdate();
      ps.close();
      fileinputstreamZip.close();
      fileinputstreamPDF.close();
			
      System.out.println("************Se ha agregado un nuevo registro a la bitacora general**********\n " + strSQLBitacora);
		} catch(SQLException e) {
      e.printStackTrace();
			throw new BitacoraException("Error al guardar datos en bitacora");
		}catch(Exception ex)
    {
      ex.printStackTrace();
      throw new BitacoraException("Error al guardar datos en bitacora");
    }
	}
}

