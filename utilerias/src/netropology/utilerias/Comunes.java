package netropology.utilerias;
//Title:        Utiler�as
//Version:      1.0
//Copyright:    Copyright (c) 2001
//Author:       Varios :-)
//Company:      Netropology.
//Description:  Archivo con diversos m�todos comunes a las aplicaciones.
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.security.MessageDigest;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.mail.internet.InternetAddress;

public class Comunes{	

	//Se encarga de hacer truncs de los datos a dos posiciones nivel de centavos.
	public static String truncaCantidad(double cantidadTruncar,int decimales)
	{
		double potencia = Math.pow(10,decimales); 
		cantidadTruncar *= potencia;
		//Double montoTruncado = new Double(cantidadTruncar);
		cantidadTruncar = Math.floor(cantidadTruncar);// montoTruncado.intValue();
		cantidadTruncar /= 100;
		return String.valueOf(cantidadTruncar);//Regresa el valor truncado  ados decimales
	}

	//Para que haga las sumas truncadas al igual que la muestra de los totales
	public static double truncaCantidadDouble(double cantidadTruncar, int decimales)
	{
		return Double.parseDouble(truncaCantidad(cantidadTruncar,decimales));
	}
	
	public static String formatoDecimal(float flotante, int decimales, boolean agrupar)
	{
		String strFlotante = String.valueOf(flotante);
		return formatoDecimal(strFlotante,decimales,agrupar);
	}
		
	public static String formatoDecimal(double doble, int decimales, boolean agrupar)
	{
		String strDoble = String.valueOf(doble);
		return formatoDecimal(strDoble,decimales,agrupar);
	}
	
	public static String formatoDecimal(Object obj, int decimales, boolean agrupar)
	{
		String numero="";
		String numeroFormato = "";
		if (obj!=null)
			 numero = obj.toString();
		else
			numero="0";

	    if(numero.trim().equals(""))
	    	numero="0";

		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en","US"));
		nf.setGroupingUsed(agrupar);
		nf.setMaximumFractionDigits(decimales);
		nf.setMinimumFractionDigits(decimales);
		//numeroFormato="Entro al if";
		try
		{
			numeroFormato = nf.format(Double.parseDouble(numero.trim()));
		}
		catch(NumberFormatException e)
		{
			numeroFormato = "";//"Se causo la excepcion numerica";
		}
		return numeroFormato;
	}

	public static String formatoDecimal(Object obj, int decimales)
	{
		return formatoDecimal(obj, decimales, true);	//Formateo incluyendo separador de miles.
	}

	
	public static String formatoDecimalSC(Object obj, int decimales){
		String valor = formatoDecimal(obj, decimales, true);
		String digito= ""; 
		int poss=0;
		
		for(int i=0; i<valor.length(); i++){
			digito=valor.substring(i,i+1);
			
			if(digito.equals(",")){
				poss=i;
				valor = valor.substring(0,poss-1).concat(valor.substring(poss+1,valor.length()));
			}
			
		}
			
		return valor;
	}
	
	public static String formatoDecimal(float flotante, int decimales)
	{
		return formatoDecimal(flotante, decimales, true);	//Formateo incluyendo separador de miles.
	}

	public static String formatoDecimal(double doble, int decimales)
	{
		return formatoDecimal(doble, decimales, true);	//Formateo incluyendo separador de miles.
	}

	
	
  public static String rellenaCeros(String numero, int numDigitos)
  {
    String tmpNumero=numero;
    for (int i=1;i<=numDigitos-numero.length();i++)
    {
      tmpNumero="0"+tmpNumero;
    }
    return tmpNumero;
  }

// Agregado 04/12/02 		--CARP
  public static String calculoFolio( int numero, int numDigitos, String esProducto)
	{
		String lsNumero = "";
		if (esProducto.equals("5")){
			lsNumero = rellenaCeros(String.valueOf(numero),numDigitos);
			lsNumero += "5";
			return lsNumero;
		}
		else 
			return calculaFolio(String.valueOf(numero),numDigitos,false);
	}

	public static String calculaFolio( String numero, int numDigitos, String esProducto)
	{ 
		String lsNumero = "";

		if (esProducto.equals("5")){
			lsNumero = rellenaCeros(numero,numDigitos);
			lsNumero += "5";
			return lsNumero;
		}
		 else
			return calculaFolio(numero,numDigitos,false);
	}  

  public static String calculaFolio(int numero,int numDigitos)	//Genera un folio con relleno de ceros a la izquierda + digitoVerificador
	{
		return calculaFolio(String.valueOf(numero),numDigitos,false);
	}
	
  public static String calculaFolio(String numero,int numDigitos)
  {
  		return calculaFolio(numero,numDigitos,false);
  }
	
  public static String calculaFolio(String numero,int numDigitos,boolean separador)
  {
    Integer[] numArray = new Integer[numDigitos];
    int digitoVerificador = 0;
    int j=numDigitos;

    numero = rellenaCeros(numero,numDigitos); //Rellena ceros a la izquierda

    for (int i=0; i<numDigitos;i++)
    {
      j--;
      numArray[i]=new Integer(Integer.parseInt(numero.charAt(i)+"")+j);
    }
 
    for (int i=0; i<numDigitos; i++)
    {
      digitoVerificador += numArray[i].intValue();
    }
    if (separador == true)
		return numero+"-"+(digitoVerificador%10);
	else
	    return numero+(digitoVerificador%10);
  }

	
	public static String formatoMoneda2(String numero, boolean signo)
    	{
	    String numeroFormato = "";
	    if(numero==null) numero="";
	    if(!numero.trim().equals(""))
	    {
	        //NumberFormat nf = NumberFormat.getInstance();
	        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en","US"));
	        nf.setMaximumFractionDigits(2);
	        nf.setMinimumFractionDigits(2);
	        //numeroFormato="Entro al if";
	        try
	        {
	          numeroFormato = nf.format(Double.parseDouble(numero.trim()));
	
	          if(signo) numeroFormato = "$ " + numeroFormato;
	      	}
	      	catch(NumberFormatException e)
	      	{
	      		numeroFormato = "";//"Se causo la excepcion numerica";
	    	}
	    }
		return numeroFormato;
  	}

    public static String formatoMN(String cifra) {
	NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("es","MX"));
	try { return nf.format(Double.parseDouble(cifra)); }
	catch(NumberFormatException nfe) { return ""; }
    }


    public static String quitaComitasSimples(String cadena){
        String palabra = cadena;
        String letra=""; int pf=1; 
        StringBuffer resultado=new StringBuffer();
        for(int i=0; i<palabra.length(); i++) {
            letra=palabra.substring(i,pf++);
            if(!letra.equals("'"))
                resultado.append(letra);
        }
    return resultado.toString().trim();    
    }
	 
	 public static String quitaComitasSimplesyDobles(String cadena){
        String palabra = cadena;
        String letra=""; int pf=1; 
        StringBuffer resultado=new StringBuffer();
        for(int i=0; i<palabra.length(); i++) {
            letra=palabra.substring(i,pf++);
            if(!letra.equals("'") && !letra.equals("\""))
                resultado.append(letra);
        }
		  return resultado.toString().trim();    
    }
    
    /**
	 * Este metodo valida si el numero proporcionado sea un numero entero.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para el caso de
	 * numeros positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero 	Cadena de caracteres con el numero entero a validar.
	 *	@param boolean Resultado de la validacion: <tt>true</tt> si el numero proporcionado es valido,
	 *						<tt>false</tt> en caso contrario.
	 */
	 public static boolean esNumero(String numero) {
		boolean isNum = false;
      	try {
            new BigInteger(numero);
				isNum = true;
       }catch(NumberFormatException nfe) { 
		  		isNum = false; 
		 }
		 return isNum; 
    }
    
    public static boolean checaFecha(String formato) {
        String date=formato;
        StringTokenizer st=new StringTokenizer(date,"/");
        int dia=0, mes=0, ano=0;    boolean isFecha = false;
        while(st.hasMoreTokens()){
            try {
                dia = Integer.parseInt(st.nextToken());
                mes = Integer.parseInt(st.nextToken())-1;
                ano = Integer.parseInt(st.nextToken());       //System.out.println(dia+"/"+mes+"/"+ano);
                Calendar fecha = new GregorianCalendar(ano, mes, dia);
                int year = fecha.get(Calendar.YEAR);
                int month = fecha.get(Calendar.MONTH);
                int day = fecha.get(Calendar.DAY_OF_MONTH);
                //System.out.println(year+" "+month+" "+day);
                if((dia!=day) || (mes!=month) || (ano!=year))
                    isFecha = false;
                else
                    isFecha = true;
            }
            catch(NoSuchElementException nse) { isFecha = false; }
            catch(NumberFormatException nf) { isFecha = false; }
        } //while
    return isFecha;
    }

    public static Date parseDate(String formato) {
			StringTokenizer st=new StringTokenizer(formato,"/");
			int dia=0, mes=0, ano=0;
			dia = Integer.parseInt(st.nextToken());
			mes = Integer.parseInt(st.nextToken());
			ano = Integer.parseInt(st.nextToken());
			Calendar fecha = Calendar.getInstance();
			java.util.Date fecIni = new java.util.Date();
			fecIni.setTime(0);
			fecha.setTime(fecIni);
			fecha.set(ano,mes-1,dia,0,0,0);
			return fecha.getTime();
    }
    
    public static boolean esDecimal(String numero) {
 		boolean isNum = false;
		try {
			new BigDecimal(numero.trim());
			isNum = true;
 		} catch(NumberFormatException nfe) {
	 		isNum = false; 
 		}
 		return isNum;
    }


 	/**
 	 * Determina si la precision y los decimales del numero contenido
 	 * dentro de la cadena especificada como parametro, es correcta.
 	 * @param strNumero La cadena que contiene el numero a evaluar
 	 * @param precision El n�mero m�ximo de digitos que puede contener
 	 *		contando los enteros y decimales
 	 * @param decimales El numero de decimales m�ximos que puede
 	 * 		contener.
 	 * @return true si el numero especificado cumple con la precision
 	 * 		y decimales especificados.
 	 * 		false en caso de que:
 	 *		- No cumpla con la precision.
 	 * 		- strNumero sea nula � vacia.
 	 * 		- strNumero no sea un numero v�lido
 	 */
 	public static boolean precisionValida(String strNumero, 
 			int precision, int decimales) {
 		boolean valido = true;
 		
 		if (strNumero == null || strNumero.trim().equals("") || !esDecimal(strNumero)) {
 			valido = false;
 		} else {
 			String parteEntera = null;
 			String parteDecimal = null;
 			int numElementos = 0;

 			VectorTokenizer vt = new VectorTokenizer(strNumero,".");
 			Vector vElementos = vt.getValuesVector();
 			numElementos = vElementos.size();
 			
 			if (numElementos == 1) {
 				parteEntera = ((String) vElementos.get(0)).trim();
 				parteDecimal = "";
 			} else if (numElementos == 2) {
 				parteEntera = ((String) vElementos.get(0)).trim();
 				parteDecimal = ((String) vElementos.get(1)).trim();
 			}
 			
			if (parteDecimal.length() > decimales) {
 				//Error. El numero de decimales es mayor al especificado
 				valido = false;
 			} else if (parteEntera.length() > precision - decimales) {
 				//Error. El numero de enteros es mayor al permitido
 				valido = false;
 			}
 		}
 		return valido;
 	}

	
	/**
	 * Divide una cadena por el separador indicado, colocando cada uno de los
	 * elementos dentro de una lista
	 * @param separador Separador usado para dividir la cadena
	 * @param cadena Cadena a dividir
	 * @return lista con los valores divididos.
	 * 
	 */
	public static Vector explode(String separador, String cadena) {
		Vector vDatos = new Vector();
		//System.out.println(separador +" - " + cadena);
		StringTokenizer st = new StringTokenizer(cadena,separador);
		int i = 0;
		while(st.hasMoreTokens())
		{
		   vDatos.add(i, st.nextToken().trim());
		   i++;
		}
		return vDatos;
	}


	/**
	 * Divide una cadena por el separador indicado, colocando cada uno de los
	 * elementos dentro de una lista. Hace la conversi�n de cada elemento 
	 * segun la clase (soportada) especificada.
	 * por ejemplo:    Comunes.explode("," , "1,54", java.lang.Integer.class);
	 * generar� una Lista con 2 elementos, los cuales ser�n almacenados
	 * como objetos java.lang.Integer
	 * 
	 * @param separador Separador usado para dividir la cadena
	 * @param cadena Cadena a dividir
	 * @param clase Nombre completo de la clase:
	 * 		Valores soportados:
	 *       	java.lang.Integer.class - Para generar elementos Integer
	 *          java.lang.Long.class - Para generar elementos Long
	 *          java.lang.Double.class - Para generar elementos Double
	 *          java.lang.Float.class - Para generar elementos Float
	 *          java.math.BigInteger.class - Para generar elementos BigInteger
	 *          java.math.BigDecimal.class - Para generar elementos BigDecimal
	 * 		Si el nombre de clase no es soportado lo maneja como Cadena   
	 * @return lista con los valores divididos.
	 * 
	 */
	public static List explode(String separador, String cadena, Class clase) {
		List vDatos = new ArrayList();
		
		if (cadena == null) {
			return vDatos;
		}
		
		StringTokenizer st = new StringTokenizer(cadena,separador);
		int i = 0;
		while(st.hasMoreTokens()) {
			Object obj = null;
			String valor = st.nextToken().trim();
			if (clase.getName().equals("java.lang.Integer")) {
				if (valor == null || valor.equals("")) {
					obj = Integer.class;
				} else {
					obj = new Integer(valor);
				}
			} else if (clase.getName().equals("java.lang.Long")) {
				if (valor == null || valor.equals("")) {
					obj = Long.class;
				} else {
					obj = new Long(valor);
				}
			} else if (clase.getName().equals("java.lang.Double")) {
				if (valor == null || valor.equals("")) {
					obj = Double.class;
				} else {
					obj = new Double(valor);
				}
			} else if (clase.getName().equals("java.lang.Float")) {
				if (valor == null || valor.equals("")) {
					obj = Float.class;
				} else {
					obj = new Float(valor);
				}
			} else if (clase.getName().equals("java.math.BigInteger")) {
				if (valor == null || valor.equals("")) {
					obj = BigInteger.class;
				} else {
					obj = new BigInteger(valor);
				}
			} else if (clase.getName().equals("java.math.BigDecimal")) {
				if (valor == null || valor.equals("")) {
					obj = BigDecimal.class;
				} else {
					obj = new BigDecimal(valor);
				}
			} else { //java.lang.String
				obj = String.valueOf(valor);
			}
			vDatos.add(i, obj);
		   i++;
		}
		return vDatos;
	}

	/**
	 * Divide una cadena por el separador indicado, colocando cada uno de los
	 * elementos dentro de una lista. Hace la conversi�n de cada elemento 
	 * segun la clase (soportada) especificada.
	 * por ejemplo:    Comunes.explode("," , "1,54", "java.lang.Integer");
	 * generar� una Lista con 2 elementos, los cuales ser�n almacenados
	 * como objetos java.lang.Integer
	 * @deprecated usar explode(String, String, Class)
	 * @param separador Separador usado para dividir la cadena
	 * @param cadena Cadena a dividir
	 * @param clase Nombre completo de la clase:
	 * 		Valores soportados:
	 *       	java.lang.Integer - Para generar elementos Integer
	 * 		Si el nombre de clase no es soportado lo maneja como Cadena   
	 * @return lista con los valores divididos.
	 * 
	 */
	public static List explode(String separador, String cadena, String clase) {
		List vDatos = new ArrayList();
		
		if (cadena == null) {
			return vDatos;
		}
		
		//System.out.println(separador +" - " + cadena);
		StringTokenizer st = new StringTokenizer(cadena,separador);
		int i = 0;
		while(st.hasMoreTokens()) {
			String valor = st.nextToken().trim();
			if (clase.equals("java.lang.Integer")) {
				Integer integer = null;
				integer=(valor == null || valor.equals(""))?null:new Integer(valor);
				vDatos.add(i, integer); //Se guarda como Integer
			} else { //java.lang.String
				vDatos.add(i, valor); //Se guarda como Cadena
			}
		   i++;
		}
		return vDatos;
	}



	
	//Genera de los elementos de un vector una cadena en base a un separador
	public static String implode(String separador, Vector unir)
	{
		String resultado = "";
		for (int i=0; i<unir.size(); i++)
		{
		  if (i==0)
		    resultado = unir.get(i).toString();
		  else
		    resultado = resultado + separador + unir.get(i).toString();
		}	
		return resultado;
	}


	public static String implode(String separador, List unir) {
		StringBuffer resultado = new StringBuffer();
		for (int i=0; i<unir.size(); i++) {
			if (i==0) {
				resultado.append((String)unir.get(i));
			} else {
				resultado.append(separador + (String)unir.get(i));
			}
		}
		return resultado.toString();
	}

	//Genera de los elementos de un arreglo una cadena en base a un separador
	public static String implode(String separador, String unirArreglo[]) {
		StringBuffer resultado = new StringBuffer();
		for (int i=0; i < unirArreglo.length; i++) {
			if (unirArreglo[i]!=null && !unirArreglo[i].equals("")) {
				if (resultado.length()==0) {
					resultado.append(unirArreglo[i]);
				} else {
					resultado.append(separador + unirArreglo[i]);
				}
			}
		}	
		return resultado.toString();
	}	

	//Genera una cadena compuesta por los campos[] en el orden[] indicado
	//Ej. orden[]={"2","","1"};campos[]={"nombreEpo","nombrePyme","nombreIf"}
	//Al aplicar el m�todo ordenarCampos(orden[], campos[]) se obtiene:
	//								nombreIf,nombreEpo
	public static String ordenarCampos(String orden[], String campos[])
	{
		String cadena="";
		Hashtable ht = new Hashtable();		
		for (int i=0;i<campos.length;i++)
		{
			if (!orden[i].trim().equals(""))
				ht.put(orden[i],campos[i]);
		}

		Object llaves[] = ht.keySet().toArray();
		Arrays.sort(llaves);

		for (int i=0;i<llaves.length;i++)
		{
			if (i==0)
				cadena=(String)ht.get(llaves[i]);
			else
				cadena=cadena+","+ht.get(llaves[i]);
				
		}
		return cadena;
	}

	//Verifica que la cadena no sea un valor nulo, sino regresa un NULL
	//Util para insert o update
	public static String checaVacio(String valor)
	{
		String resultado = "";
		if (valor.length() == 0)
		{resultado = "NULL";}
		else
		{resultado = valor;}
		return resultado;
	}	

	public static String generapassword()
	{
		StringBuffer sb=new StringBuffer();
		String numerico[]={"0","1","2","3","4","5","6","7","8","9"};
		String alfabetico[]={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","x","y","z"};
		String alfanumerico[]={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","x","y","z","0","1","2","3","4","5","6","7","8","9"};
		
		int rnd = (new Integer((int)(Math.random()*alfabetico.length))).intValue();
		sb.append(alfabetico[rnd]);
		
		for(int i=0; i<6; i++) 
		{
			rnd = (new Integer((int)(Math.random()*alfanumerico.length))).intValue();
			sb.append(alfanumerico[rnd]);
		}
		
		rnd = (new Integer((int)(Math.random()*numerico.length))).intValue();
		sb.append(numerico[rnd]);
		
        return sb.toString();
    }
	
	

	/**
	 * Genera una cadena alfanum�rica aleatoria de n caracteres
	 * El m�todo est� pensado para generar nombres aleatorios 
	 * de archivos temporales.
	 * @param longitud Longitud de la cadena aleatoria
	 * @return Cadena con la cadena aleatoria.
	 */
	public static String cadenaAleatoria(int longitud) {
		String x[]={"9","a","Z","8","b","Y","7","c","X","6","d","W","5","e","V","4","f","U","3","g","T","2","h","S","1","i","R","0","j","Q","9","k","P","8","l","O","7","m","N","6","n","M","5","o","L","4","p","K","3","q","J","2","r","I","1","s","H","9","t","G","8","u","F","7","v","E","6","w","D","5","x","C","4","y","B","3","z","A","2","a","Z","1","b","Y","0","c","X","9","d","W","8","e","V","7","f","U","6","g","T","5"};
		StringBuffer sb=new StringBuffer();
		for(int a=1; a<=longitud; a++) {
			int i=(int)((Math.random()*100)+1);
			sb.append(x[i-1]);
		}
		return sb.toString();
	}


	
	
	public static String runCmd(String cmd){
		try{
			Runtime rt = Runtime.getRuntime();
			Process p = rt.exec(cmd);
			boolean bOk = false;
			boolean bOkErr = false;
			InputStreamReader in = new InputStreamReader(p.getInputStream());
			InputStreamReader inErr = new InputStreamReader(p.getErrorStream());

            BufferedReader reader = new BufferedReader(in);
            BufferedReader readerErr = new BufferedReader(inErr);
            StringBuffer buf = new StringBuffer();
            StringBuffer bufErr = new StringBuffer();
            String line = "";
            String newline = "\n";

			while((line = reader.readLine()) != null){
				bOk=true;
				buf.append(line);
				buf.append(newline);
			}
			reader.close();

            while((line = readerErr.readLine()) != null){
				bOkErr=true;
                bufErr.append(line);
                bufErr.append(newline);
            }
            readerErr.close();

			p.getInputStream().close();
			p.getOutputStream().close();
			p.getErrorStream().close();
			p.waitFor();

			if(bOk)
				return buf.toString();
			else if(bOkErr)
            	return bufErr.toString();
			else
                return "Desconocido";
		}
		catch(Exception e){
			return e.getMessage();
		}
	}
	
	// Formatea una cadena como fija.
	public static String formatoFijo(String cadena, int cifras, String caracter, String tipo){
		String strCadena = "";
		String strCeros = "";
		String strContenido = "";
		String strTipo = "";
		int i = 0;
		int a = 0;
		
		strContenido = (cadena == null) ? "" : cadena;
		strTipo = (tipo == null) ? "" : tipo;
		
		if (strContenido.length() >= cifras){
			strCadena = strContenido.substring(0, cifras);
		}
		else{
			a = cifras - strContenido.length();

			for (i = 1; i <= a ; i++)
				strCeros += caracter;
			
			if (strTipo.equals("A") || strTipo.equals("a"))
				strCadena += strContenido + strCeros;
			else
				strCadena += strCeros + strContenido;
		}
		
		return strCadena;
	}
	
	//Rellena una cadena con espacios hasta la longitud especificada
	public static String rellenaCadena(String campo, int longitud)
	{
		String cadenaTemp;
		char[] cadena = new char[longitud];
		Arrays.fill(cadena,' ');
		if (campo==null) {
			campo="";
		}
		int tam = (campo.length()>longitud)?longitud:campo.length();
		
		cadenaTemp = campo.substring(0,tam);
		System.arraycopy(cadenaTemp.toCharArray(),0,cadena,0,cadenaTemp.length());
		return new String(cadena);
	}


	/**
	 * Repite la cadena el numero de veces especificado separandolo por la cadena especificada
	 * Empleado por ejemplo para generar cadenas como ?,?,?,?,?,?
	 * ej.  Comunes.repetirCadenaConSeparador("?", ",", 6)
	 *
	 * @param cadenaARepetir Cadena a Repetir
	 * @param separador Separador para la cadena
	 * @param numeroDeRepeticiones Indica el numero de repeticiones
	 *
	 */
	public static String repetirCadenaConSeparador(
			String cadenaARepetir, String separador, int numeroDeRepeticiones) {
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i <= numeroDeRepeticiones; i++) {
			sb.append( ((i==1)?cadenaARepetir:separador + cadenaARepetir) );
		}
		return sb.toString();
	}
	//Rellena un numero con ceros a la derecha e izquierda y elimina el punto dec.
	public static String rellenaNumero(String numero,int enteros,int decimales)
	{
		String numeroFormato = "";
		if(numero==null) numero="";

		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en","US"));
		nf.setGroupingUsed(false);
		nf.setMaximumIntegerDigits(enteros);
		nf.setMinimumIntegerDigits(enteros);
		nf.setMaximumFractionDigits(decimales);
		nf.setMinimumFractionDigits(decimales);
		try
		{
			numeroFormato = nf.format(Double.parseDouble(numero.trim()));
		}
		catch(NumberFormatException e)
		{
			numeroFormato = "";//"Se causo la excepcion numerica";
		}
//		System.out.println("Antes de regresar valor");
		return Comunes.strtr(numeroFormato,".","");
		//return numeroFormato;
	}
	
	//Reemplaza una secuencia de caracteres (separador) por otra (nuevoSeparador)
	public static String strtr(String cadena, String separador, String nuevoSeparador)
	{
		String cadenaResultante = "";
		if (cadena==null) cadena="";

		StringTokenizer st = new StringTokenizer(cadena,separador);
		while(st.hasMoreTokens())
		{
			cadenaResultante=cadenaResultante+st.nextToken().trim()+nuevoSeparador;
		}
		return cadenaResultante;
	}
	
	/**
	 * Protege el caracter especificado en una cadena 
	 * @param cadena Cadena en donde se protegeran los caracteres
	 * @param caracterProteger Caracter a proteger
	 * @param protector Cadena con la que se protege el caracter
	 * @return cadena con caracteres protegidos
	 */
	public static String protegeCaracter(String cadena, char caracterProteger, String protector) {

		StringBuffer sb = new StringBuffer();
		sb.append(cadena);
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == caracterProteger) {
				sb.insert(i,protector);
				i+= protector.length(); ;
			}
		}
		return sb.toString();
	}

	//Reemplaza subcadenas
	/**
	 * 
	 * @return 
	 * @param psNewSeg
	 * @param psReplace
	 * @param psWord
	 */
	public static String reemplaza(String psWord, String psReplace, String psNewSeg) {
     StringBuffer lsNewStr = new StringBuffer();
     int liFound = 0;
     int liLastPointer=0;	
     do {	
       liFound = psWord.indexOf(psReplace, liLastPointer);

       if ( liFound < 0 )
          lsNewStr.append(psWord.substring(liLastPointer,psWord.length()));

       else {	
          if (liFound > liLastPointer)
             lsNewStr.append(psWord.substring(liLastPointer,liFound));	
          lsNewStr.append(psNewSeg);
          liLastPointer = liFound + psReplace.length();
       }	
     }while (liFound > -1);	
     return lsNewStr.toString();
  	}
  	
  	/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ*/
  	public static String sumaFechaDias(String fecha, int dias) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		String 		fechaDias		= fecha;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT TO_CHAR(TO_DATE (?, 'dd/mm/yyyy') + (?), 'dd/mm/yyyy')"   +
				"   FROM DUAL"  ;
			lVarBind.add(fecha);
			lVarBind.add(new Integer(dias));
			lDatos = con.consultaRegDB(qrySentencia, lVarBind, false);
			if(lDatos.size()>0) {
				fechaDias = lDatos.get(0).toString();
			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return fechaDias;
  	}
  	
	public static String sumaFechaPlazo(String fecha, int plazo) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		String 		fechaPlazo		= fecha;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT TO_CHAR(ADD_MONTHS (TO_DATE (?, 'dd/mm/yyyy'), ?), 'dd/mm/yyyy')"   +
				"   FROM DUAL"  ;
			lVarBind.add(fecha);
			lVarBind.add(new Integer(plazo));
			lDatos = con.consultaRegDB(qrySentencia, lVarBind, false);
			if(lDatos.size()>0) {
				fechaPlazo = lDatos.get(0).toString();
			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return fechaPlazo;
  	}
  	
	public static int obtienePlazo(String fechaIni, String fechaFin) {
		int 	plazo 		= 0;
		String 	fechaAux 	= "";
  		try {
  			fechaIni 	= "01/"+fechaIni.substring(3,10);
  			fechaFin 	= "01/"+fechaFin.substring(3,10);
  			fechaAux 	= fechaIni;
  			while(!fechaAux.equals(fechaFin)) {
  				fechaAux = sumaFechaPlazo(fechaAux, 1);
  				plazo++;
  			}  			
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  		return plazo;
  	}
  	
  	public static int restaFechas(String fechaIni, String fechaFin) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		int 		diasReales		= 0;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT TO_DATE (?, 'dd/mm/yyyy') - TO_DATE (?, 'dd/mm/yyyy')"   +
				"   FROM DUAL"  ;
			lVarBind.add(fechaFin);
			lVarBind.add(fechaIni);
			diasReales = con.existeDB(qrySentencia, lVarBind);
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return diasReales;
  	}
  	
	public static List<String> getListTokenizer(String linea, String delimitador) {
		String 	elem 		= "", 
				cad			= "";
		int 	possIni 	= 0, 
				possFin 	= 0, 
				countokens	= 0;
		List<String> list = new ArrayList<>();
		try {
			if (linea.indexOf(delimitador) == -1) { //No se encontro el delimitador en la cadena
				if (!linea.trim().equals("")) {	//La cadena no es vacia
					list.add(linea);	//Guarda la cadena original
				}
			} else {
				for(int i=0; i<linea.length(); i++) {
					elem=linea.substring(i,i+1);
					if(elem.equals(delimitador)) {
						possIni=(countokens==0)?0:possFin;
						possFin=i+1;
						countokens++;
						cad=linea.substring(possIni, possFin-1);
						//System.out.println(cad);
						list.add(cad);
					}
				} //for
				String Delimit1=linea.substring(possFin-1,possFin);
				String Delimit2=linea.substring(linea.length()-1,linea.length());
				if((Delimit1.equals(delimitador)) && (!Delimit2.equals(delimitador))) {
					if(possFin<linea.length())
						list.add(linea.substring(possFin,linea.length()));
				} else if (Delimit2.equals(delimitador)) {
					list.add("");
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			list = new ArrayList<>();
		}
		return list;
	}
	
	public static Registros getTokensArchivo(String path, String delimiter, List<String> layout) {
		Registros registros = new Registros(layout);
		BufferedReader    bufferedreader = null;
		try {
			java.io.File 	file 			= new java.io.File(path);
			bufferedreader 	= new BufferedReader(
													new InputStreamReader(
															new FileInputStream(file)));
			String line = "";
			while((line = bufferedreader.readLine())!=null) {
				line = line.replace('\'', ' ').replace('\"', ' ').replace('\\', ' ').trim();
				registros.addDataRow(getListTokenizer(line, delimiter));
			}//while linea
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedreader!=null) {
					bufferedreader.close();
				}
			} catch(Throwable t) {
				t.printStackTrace();
			}
		}
		return registros;
	}
	
	
	
	/**
	 * Obtiene la extensi�n del nombre del archivo especificado
	 * @param nombreArchivo Nombre del archivo
	 * @return Cadena con la extensi�n del archivo en minusculas
	 */
	public static String getExtensionNombreArchivo(String nombreArchivo) {
		try {
			if (nombreArchivo == null || nombreArchivo.trim().equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
				throw new IllegalArgumentException(
						"getExtensionNombreArchivo()::Los parametros recibidos no son validos. " + e.getMessage() + "\n" +
						"nombreArchivo=" + nombreArchivo );
		}
		List elementosArchivo = Comunes.explode(".", nombreArchivo);
		String extension = (elementosArchivo.size() > 0)?(String)elementosArchivo.get(elementosArchivo.size() -1):"";
		return extension.toLowerCase();
	}

	
  	/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<IHJ*/
	
	/**
	* Este metodo verifica que la cadena de caracteres <code>str</code>, solo contenga caracteres
	* del alfabeto espa�ol.
	* <p>
	* @param  str  		Cadena de texto a verificar.
	*	
	* @return boolean  	Regresa <code>true</code> si solo se encontraron caracteres del alfabeto 
	*							espa�ol, retorna <code>false</code> en caso contrario.
	*/
	public static boolean esAlfabetico(String str) {
		char 		caracter 		= '\0';
		
		if ( str == null || str.length() == 0)
			return(false);	
		
		for (int i=0; i<str.length(); i++){
			caracter = str.charAt(i);
			if (!(
					(caracter >= 'a' && caracter <= 'z') || 
					(caracter >= 'A' && caracter <= 'Z') || 
					(caracter == '�') || 
					(caracter == '�') 
				))
				return false;
		}
		return(true);
	}
	
	/**
	* Este metodo verifica que la cadena de caracteres <code>str</code>, solo contenga caracteres
	* del alfabeto espa�ol y caracteres especiales definidos por el usuario en la variable 
	* <code>extraChars</code>.
	* <p>
	* @param  str  		Cadena de texto a verificar
	* @param	 extraChars	Cadena de texto con los caracteres adicionales que tambien son permitidos	
	* @return boolean 	Regresa <code>true</code> si solo se encontraron los caracteres permitidos,
	*							retorna <code>false</code> en caso contrario.
	*/
	public static boolean esAlfabetico(String str, String extraChars) {
		char caracter = '\0';
		
		if ( str == null || str.length() == 0)
			return(false);
		
		if (extraChars == null)
			return(esAlfabetico(str));
		
		for (int i=0; i<str.length(); i++){
			caracter = str.charAt(i);
			if (!(
					(caracter >= 'a' && caracter <= 'z') || 
					(caracter >= 'A' && caracter <= 'Z') || 
					(caracter == '�') || 
					(caracter == '�') 
				) && extraChars.indexOf(caracter) == -1 )
				return false;
		}
		return true;
	}
	
	/**
	* Este metodo valida si la fecha proporcionada en el parametro <code>fecha</code>
	* es correcta.Solo se permiten fechas hasta el a�o 9999
	* <p>
	* @param  	fecha  		Cadena de caracteres en la que se indica la fecha validar
	* @param		formato		Formato con el cual se verificara la fecha, por ejemplo,
	*								para fechas del tipo 25/05/2008, se usaria el formato dd/MM/yyyy 
	* @return  	boolean    	Regresa un valor de tipo <code>boolean</code>, 
	*								siendo este <code>true</code> si la fecha proporcionada existe  
	*								en el calendario o <code>false</code> si esta fecha no existe.
	*/
	public static boolean esFechaValida(String fecha, String formato){
		try {
			ParsePosition 		pos	= new ParsePosition(0);
			SimpleDateFormat 	sdf	= new SimpleDateFormat(formato);
			sdf.setLenient(false);
			Date d = sdf.parse(fecha,pos);
			if( pos.getIndex() != fecha.length() ){
				throw new AppException("No se parseo la fecha completa.");
			}
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(d);
			
			if((cal.get(Calendar.YEAR)) > 9999){
				throw new AppException("A�o demasiado grande.");
			}
		}catch (Exception e) {
			return false;
		}
		return true;
  }
  
  	/**
	* Este metodo valida si la fecha proporcionada en el parametro <code>fecha</code>
	* es mayor o igual a la fecha del dia de Hoy
	* <p>
	* @param  	fecha  		Cadena de caracteres en la que se indica la fecha validar
	* @param		formato		Formato con el cual se verificara la fecha, por ejemplo,
	*								para fechas del tipo 99/12/31, se usaria el formato yyMMDD 
	* @return  	boolean    	Regresa un valor de tipo <code>boolean</code>, 
	*								siendo este <code>true</code> si la fecha proporcionada es mayor  
	*								o igual a la fecha de hoy; retorna <code>false</code> si esta 
	*								es menor al dia de hoy.
	*/
	public static boolean esFechaMayorOIgualAlaFechaDeHoy(String fecha, String formato){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha); 

			Calendar fechaDeHoyTmp 				= Calendar.getInstance();
			fechaDeHoyTmp.setTime(Fecha.getSysDate());
			Calendar fechaAValidarTmp 			= sdf.getCalendar();
						
			Calendar fechaDeHoy 				= new GregorianCalendar(fechaDeHoyTmp.get(Calendar.YEAR),fechaDeHoyTmp.get(Calendar.MONTH),fechaDeHoyTmp.get(Calendar.DAY_OF_MONTH));
			Calendar fechaAValidar	  		= new GregorianCalendar(fechaAValidarTmp.get(Calendar.YEAR),fechaAValidarTmp.get(Calendar.MONTH),fechaAValidarTmp.get(Calendar.DAY_OF_MONTH));
			
			if (fechaAValidar.equals(fechaDeHoy)) {
				return true;
			}
			if (fechaAValidar.after(fechaDeHoy)){
				return true;
      	}
			
		}catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
		
		return false;
   }
  
  	/**
	* Este metodo verifica si el RFC proporcionado en <code>strRFC</code> es valido, dependiendo
	* del tipo de persona indicada.
	* <p>
	* Este metodo no sirve para validar RFC internacionales
	* <p>
	* @param  strRFC  		Cadena de texto con el RFC a verificar
	* @param  tipoPersona 	Tipo de persona para el cual se validara el RFC, puede tomar los siguientes
	*								valores: <code>'M'</code> para persona moral, <code>'F'</code> para persona fisica.	
	* @return boolean     	Regresa un valor de tipo <code>boolean</code>, siendo <code>true</code> si el 
	*								RFC proporcionado es valido o <code>false</code> en caso contrario
	*/
	public static boolean validaRFC(String strRFC, char tipoPersona)
	{
		if( strRFC == null ) return false;
		
		// Extraer campos
		StringTokenizer 	stoken		= new StringTokenizer(strRFC,"-");
		String 				clvNombre 	= (stoken.hasMoreElements())?stoken.nextToken():"";
		String 				fecha	 		= (stoken.hasMoreElements())?stoken.nextToken():"";
		String 				homoclave	= (stoken.hasMoreElements())?stoken.nextToken():"";
		// Verificar que los campos no esten vacios
		if(clvNombre.equals("")) 	return false;
		if(fecha.equals("")) 		return false;
		if(homoclave.equals("")) 	return false;
		// Verificar que el tipo de los campos sea el correcto
		if (esAlfabetico(clvNombre,"&") 		== false)	return false; 
		if (esFechaValida(fecha,"yyMMdd") 	== false)	return false; 
		if (esAlfanumerico(homoclave) 		== false)   return false;
		// Comprobar que las longitudes de la clave del nombre, fecha y homoclave sean las correctas
		if(fecha.length()		!=6) return false;
		if(homoclave.length()!=3) return false;
		if (tipoPersona == 'M'){			
			if (clvNombre.length() != 3) return false;           
				return true;
      } else if (tipoPersona == 'F'){ 
			if (clvNombre.length() != 4) return false;
				return true;
		}
		
		return false;
	}
	
	  /**
  * Este metodo valida una direccion de email
  *
  * <P>Regresa <tt>true</tt> si y solo si  <tt>emailAddress</tt> puede convertirse en 
  * una direccion de correo de internet {@link javax.mail.internet.InternetAddress} de acuerdo 
  * con el RFC822 pero con la restriccion que no pueden ir sin el dominio
  * <p>
  *	@param emailAddress 	Direccion de correo que se validara
  *	@param boolean 		Resultado de la validacion: <tt>true</tt> si la direccion de correo
  *								es valida, <tt>false</tt> en caso contrario
  */
  public static boolean validaEmail(String emailAddress){
    if (emailAddress == null) return false;
    try {
		//La direccion de correo cumple con el RFC 822
      new InternetAddress(emailAddress);		
		//Existen nombre y dominio en la direccion
		List 					tokens		= getListTokenizer(emailAddress,"@");
		if(tokens.size()!= 2 ) return false;
		String nombre  = tokens.get(0).toString();
		if(nombre.length() == 0 ) return false;
		String dominio = tokens.get(1).toString();
		if(dominio.length() == 0 ) return false;
		// Esta seccion la dejo comentada con por causa de la carga masiva de proveedores, ya que cuando
		// no se conoce la direccion de correo se ingresa una direccion de la forma correo@correo, 
		// Lo que ocasionaria que no se pudiera dar de alta el proveedor
		
		// Verficar que haya al menos un subdominio especificado
		//tokens = getListTokenizer(dominio,".");
		//if(tokens.size() < 2) return false;
		//String campo = null;
		//for(int indice=0;indice<tokens.size();indice++){
		//	campo = tokens.get(indice).toString();
		//	if(campo.length()< 1) return false;
		//}
		// Verificar longitud del dominio superior (debe ser de entre 2 y 6 caracteres)
		//if(campo.length()<2 || campo.length()>6) return false;
		
    } catch (Exception e){
      return false;
    }
    return true;
  }
  
  	/**
	* Este metodo verifica que en la cadena de caracteres <code>str</code>, solo haya caracteres
	* del alfabeto espa�ol y caracteres numericos.
	* <p>
	* @param  str  		Cadena de texto a verificar.
	*	
	* @return boolean    Regresa <code>true</code> si solo se encontraron caracteres del alfabeto 
	*						   espa�ol, retorna <code>false</code> en caso contrario.
	*/
	public static boolean esAlfanumerico(String str) {
		char 		caracter 		= '\0';
		
		if ( str == null || str.length() == 0)
			return(false);
			
		for (int i=0; i< str.length(); i++){
			caracter=str.charAt(i);
			if (!(
					(caracter >= 'a' && caracter <= 'z') || 
					(caracter >= 'A' && caracter <= 'Z') ||
					(caracter >= '0' && caracter <= '9') ||
					(caracter == '�') || 
					(caracter == '�') 
				))
				return false;
		}
		return(true);
	}
	
	/**
	* Este metodo verifica que la cadena de caracteres <code>str</code>, solo contenga caracteres
	* del alfabeto espa�ol, caracteres numericos y  caracteres especiales definidos por el usuario
	* en la variable <code>extraChars</code>.
	* <p>
	* @param  str  			Cadena de texto a verificar.
	* @param	 extraChars		Cadena de texto con los caracteres adicionales que tambien son permitidos.	
	* @return boolean     	Regresa <code>true</code> si solo se encontraron los caracteres permitidos, 
	*								retorna <code>false</code>	en caso contrario.
	*/
	public static boolean esAlfanumerico(String str, String extraChars) {
		char 		caracter 		= '\0';
		
		if ( str == null || str.length() == 0)
			return(false);
		
		if (extraChars == null)
			return esAlfabetico(str);
		
		for (int i=0; i< str.length(); i++){
			caracter=str.charAt(i);
			if (!(
					(caracter >= 'a' && caracter <= 'z') || 
					(caracter >= 'A' && caracter <= 'Z') || 
					(caracter >= '0' && caracter <= '9') || 
					(caracter == '�') || 
					(caracter == '�') 
				) && extraChars.indexOf(caracter) == -1 )
				return false;
		}
		return(true);
	}
	
	public static String suprimeCaracteres(String str, String caracteres){
		char 				caracter 		= '\0';
		StringBuffer 	resultado		= new StringBuffer();
		
		if ( 	str == null 	||	caracteres 	== null 	)  
			return(str);
			   
		for (int i=0; i<str.length(); i++){
			caracter = str.charAt(i);
			if(caracteres.indexOf(caracter) == -1)
				resultado.append(caracter);
		}
		return resultado.toString();	
   }

	/**
	 * Compara dos numeros, segun el operador especificado.
	 * Este m�todo esta orientado a la validaci�n de numeros usando Validator de struts.
	 * No se emplea validwhen (test) ya que puede no hacer la conversi�n numerica de manera
	 * adecuada.
	 * 
	 * @param sNumero Primer numero a comparar
	 * @param operador Operador:
	 * 	eq ==
	 *    ne !=
	 *    gt >
	 *    ge >=
	 *    lt <
	 *    le <=
	 * @param sNumero2 Segundo numero a comparar
	 */
	public static boolean esComparacionNumericaValida(
			String sNumero, String operador, String sNumero2) {
		
		BigDecimal numero = null;
		BigDecimal numero2 = null;
		try {
			if (sNumero == null || sNumero.trim().equals("") ||
					operador == null || operador.trim().equals("") ||
					sNumero2 == null || sNumero2.trim().equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			
			if (!operador.equalsIgnoreCase("eq") && 
					!operador.equalsIgnoreCase("ne") &&
					!operador.equalsIgnoreCase("gt") &&
					!operador.equalsIgnoreCase("ge") &&
					!operador.equalsIgnoreCase("lt") &&
					!operador.equalsIgnoreCase("le")) {
				throw new Exception("El operador no es valido");
			}
			numero = new BigDecimal(sNumero);
			numero2 = new BigDecimal(sNumero2);
		} catch(Exception e) {
				throw new IllegalArgumentException(
						"Los parametros recibidos no son validos. " + e.getMessage() + "\n" +
						"sNumero=" + sNumero + "\n" +
						"operador=" + operador + "\n" +
						"sNumero2=" + sNumero2);
		}
		boolean resultado = false;
		if (operador.equalsIgnoreCase("eq")) {
			resultado = (numero.compareTo(numero2) == 0);
		} else if (operador.equalsIgnoreCase("ne")) {
			resultado = (numero.compareTo(numero2) != 0);
		} else if (operador.equalsIgnoreCase("gt")) {
			resultado = (numero.compareTo(numero2) > 0);
		} else if (operador.equalsIgnoreCase("ge")) {
			resultado = (numero.compareTo(numero2) >= 0);
		} else if (operador.equalsIgnoreCase("lt")) {
			resultado = (numero.compareTo(numero2) < 0);
		} else if (operador.equalsIgnoreCase("le")) {
			resultado = (numero.compareTo(numero2) <= 0);
		}
		return resultado;
	}

	/**
	 * Compara dos fechas, segun el operador especificado. 
	 * Las fechas dentro de las cadenas especificadas como par�metros deben 
	 * de ser validadas previamente, dado que este m�todo no realiza validaci�n
	 * 
	 * Este m�todo esta orientado a la validaci�n de fechas usando Validator de struts.
	 * 
	 * @param sFecha Primer fecha a comparar
	 * @param operador Operador:
	 * 	eq ==
	 *    ne !=
	 *    gt >
	 *    ge >=
	 *    lt <
	 *    le <=
	 * @param sFecha2 Segunda fecha a comparar
	 * @param formatoFecha Formato de la fecha
	 */
	public static boolean esComparacionFechaValida(
			String sFecha, String operador,
			String sFecha2, String formatoFecha) {
		//Las fechas deben de ser v�lidas ya que el SimpleDateFormat no
		//considera que la cadena "20/05/200#$@" sea una fecha incorrecta
		Date fecha = null;
		Date fecha2 = null;
		try {
			if (sFecha == null || sFecha.trim().equals("") ||
					operador == null || operador.trim().equals("") ||
					sFecha2 == null || sFecha2.trim().equals("") ||
					formatoFecha == null || formatoFecha.trim().equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}
			
			if (!operador.equalsIgnoreCase("eq") && 
					!operador.equalsIgnoreCase("ne") &&
					!operador.equalsIgnoreCase("gt") &&
					!operador.equalsIgnoreCase("ge") &&
					!operador.equalsIgnoreCase("lt") &&
					!operador.equalsIgnoreCase("le")) {
				throw new Exception("El operador no es valido");
			}
			fecha= new SimpleDateFormat(formatoFecha).parse(sFecha);
			fecha2= new SimpleDateFormat(formatoFecha).parse(sFecha2);
		} catch(Exception e) {
				throw new IllegalArgumentException(
						"Los parametros recibidos no son validos. " + e.getMessage() + "\n" +
						"sFecha=" + sFecha + "\n" +
						"operador=" + operador + "\n" +
						"sFecha2=" + sFecha2 + "\n" +
						"formatoFecha=" + formatoFecha);
		}
		boolean resultado = false;
		if (operador.equalsIgnoreCase("eq")) {
			resultado = (fecha.compareTo(fecha2) == 0);
		} else if (operador.equalsIgnoreCase("ne")) {
			resultado = (fecha.compareTo(fecha2) != 0);
		} else if (operador.equalsIgnoreCase("gt")) {
			resultado = (fecha.compareTo(fecha2) > 0);
		} else if (operador.equalsIgnoreCase("ge")) {
			resultado = (fecha.compareTo(fecha2) >= 0);
		} else if (operador.equalsIgnoreCase("lt")) {
			resultado = (fecha.compareTo(fecha2) < 0);
		} else if (operador.equalsIgnoreCase("le")) {
			resultado = (fecha.compareTo(fecha2) <= 0);
		}

		return resultado;
	}


	
	/**
	 * Determina si el nombre de archivo tiene la extensi�n especificada.
	 * @param nombreArchivo Nombre del archivo, puede contener la ruta.
	 * @param extensiones Cadena con las extensiones separadas por coma,
	 * 		por ejemplo: GIF,JPEG,JPG
	 * @return 
	 */
	public static boolean esExtensionArchivoValida(String nombreArchivo, String extensiones) {
		boolean resultado = false;
		try {
			if (nombreArchivo == null || nombreArchivo.trim().equals("") ||
					extensiones == null || extensiones.trim().equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
				throw new IllegalArgumentException(
						"Los parametros recibidos no son validos. " + e.getMessage() + "\n" +
						"nombreArchivo=" + nombreArchivo + "\n" +
						"extensiones=" + extensiones );
		}

		List listaExtensiones = Comunes.explode(",", extensiones);
		Iterator it = listaExtensiones.iterator();
		while (it.hasNext()) {
			String extension = ((String) it.next()).toUpperCase();
			if ( nombreArchivo.endsWith("." + extension) ) {
				resultado = true;
				break;
			}
		}
		return resultado;
	}
	
	/**
	 * Este metodo formatea una cadena de texto, de tal forma que el primer caracter de la cadena
	 * este en mayusculas y los demas caracteres en minusculas
	 * <p>
	 * 	Ejemplo:
	 *    	"Esta Es Una CADENA DE TEXTO" 
	 *    Se convierte en:
	 *    	"Esta es una cadena de texto"
	 * <p>  
	 * @return	Cadena de texto con los caracteres en el nuevo formato.
	 * @param in Cedena de texto a formatear.		
	 */
	public static String ucFirst(String in){
		if(in == null || in.equals("") ) return in; 
			
		StringBuffer stb = new StringBuffer(in.toLowerCase());
		stb.setCharAt(0,Character.toUpperCase(stb.charAt(0)));
		
		return stb.toString();
	}
	/**
	 * Este metodo convierte un numero decimal a letras, por ejemplo, el numero 12322221.23
	 * sera convertido a: 
	 * 	Doce  millones trescientos veintidos  mil doscientos pesos dolares 23/100 M.N.
	 * <p>
	 * @return 						Cadena de texto con el numero como se lee.
	 * @param 	numero			Numero a convertir
	 * @param	claveMoneda		Actualmente soporta dos claves de moneda: 1 para la moneda nacional
	 * 								(pesos mexicanos) y 2 para el dolar americano. Cualquiero otra
	 *                         clave proporcionada es ignorada, no apareciendo el tipo de moneda
	 *                         en la salida.
	 */
	public static String numeroEnLetras(String numero, String claveMoneda){
		if( numero == null || numero.equals("")) return numero;
		
		// Extraer la parte entera
		String []datos     = numero.split("\\.");
		String parteEntera = datos[0];
		
		// Extraer la parte decimal
		String parteFraccionaria;
		try {
			datos = Comunes.formatoDecimal(numero,2,true).split("\\.");
			parteFraccionaria = datos[1];
		}catch(Exception e){
			return numero;
		}
		
		// Obtener nombre de la moneda
		String nombreMoneda 				= null;
		String nombreMonedaSingular 	= null;
		String abreviaturaMoneda 		= null;
		if(claveMoneda.equals("1")){
			nombreMoneda 			= "pesos";
			nombreMonedaSingular = "peso";
			abreviaturaMoneda 	= "M.N.";
		} else if(claveMoneda.equals("54")){
			nombreMoneda 			= "dolares";
			nombreMonedaSingular = "dolar";
			abreviaturaMoneda 	= "U.S.D.";
		} else {
			nombreMoneda 			= "";
			nombreMonedaSingular = "";
			abreviaturaMoneda 	= "";
		}
		
		// Convertir numero a Letras
		String letras;
		try {
			letras=ucFirst(ConvierteCadena.numeroALetra(parteEntera)); 
			if(letras.equals("Un"))
				nombreMoneda = nombreMonedaSingular;
			letras += " " + nombreMoneda + " " + parteFraccionaria + "/100 " + abreviaturaMoneda; 
		}catch(Exception e){
			return numero;
		}
		
		return letras;
	}
	
	 /**
	 * Este metodo valida si el numero proporcionado sea un numero entero positivo.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para el caso de
	 * numeros positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero 	Cadena de caracteres con el numero entero a validar.
	 *	@param boolean Resultado de la validacion: <tt>true</tt> si el numero proporcionado es valido,
	 *						<tt>false</tt> en caso contrario.
	 */
	 public static boolean esNumeroEnteroPositivo(String numero) {
		boolean isNum = false;
      	try {
            BigInteger num=new BigInteger(numero);
				if(num.signum() != -1) isNum = true;
       }catch(NumberFormatException nfe) { 
		  		isNum = false; 
		 }
		 return isNum; 
    }
	 
	 /**
	 * Este metodo valida si el numero proporcionado sea un numero entero positivo y diferente de cero.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para el caso de
	 * numeros positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero 	Cadena de caracteres con el numero entero a validar.
	 *	@param boolean Resultado de la validacion: <tt>true</tt> si el numero proporcionado es valido,
	 *						<tt>false</tt> en caso contrario.
	 */
	 public static boolean esNumeroEnteroPositivoYDiferenteDeCero(String numero) {
		boolean isNum = false;
      	try {
            BigInteger num=new BigInteger(numero);
				if(num.signum() == 1) isNum = true;
       }catch(NumberFormatException nfe) { 
		  		isNum = false; 
		 }
		 return isNum; 
    }
	 /**
	 * Este metodo valida si el numero decimal proporcionado sea un numero positivo.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para el caso de
	 * numeros decimales positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero 	Cadena de caracteres con el numero a validar.
	 *	@param boolean Resultado de la validacion: <tt>true</tt> si el numero proporcionado es valido,
	 *						<tt>false</tt> en caso contrario.
	 */
	 public static boolean esDecimalPositivo(String numero) {
 		boolean isNum = false;
		try {
			BigDecimal num=new BigDecimal(numero.trim());
			if(num.signum() != -1) isNum = true;
 		} catch(NumberFormatException nfe) {
	 		isNum = false; 
 		}
 		return isNum;
    }

	 /**
	 * Este metodo valida si el numero decimal proporcionado sea un numero positivo y diferente de cero.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para el caso de
	 * numeros decimales positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero 	Cadena de caracteres con el numero a validar.
	 *	@param boolean Resultado de la validacion: <tt>true</tt> si el numero proporcionado es valido,
	 *						<tt>false</tt> en caso contrario.
	 */
	 public static boolean esDecimalPositivoYDiferenteDeCero(String numero) {
 		boolean isNum = false;
		try {
			BigDecimal num=new BigDecimal(numero.trim());
			if(num.signum() == 1) isNum = true;
 		} catch(NumberFormatException nfe) {
	 		isNum = false; 
 		}
 		return isNum;
    }
	 
	 /**
	 * Este metodo verifica si el numero proporcionado, excede el numero maximo 
	 * de digitos enteros o decimales, devolviendo <tt>true</tt>, si esta condicion 
	 * se cumple o <tt>false</tt> en caso contrario.
	 * <P>
	 * El numero proporcionado en <tt>numero</tt> no debe poseer espacios,y para 
	 * el caso de numeros decimales positivos estos no pueden llevar signo.
	 * <p>
	 *	@param numero       Cadena de caracteres con el numero a validar.
	 * @param numEnteros   Numero maximo de digitos para la parte entera.
	 * @param numDecimales Numero maximo de digitos para la parte decimal.
	 *	@param boolean      Resultado de la validacion: <tt>true</tt> si el numero 
	 *                     proporcionado excede los limites, <tt>false</tt> 
	 *                     en caso contrario.
	 */
	public static boolean excedeCantidadDeEnterosYDecimales(String numero, int numEnteros, int numDecimales){
		
		if(numero == null || numero.equals(""))
			return false;
			
		if(!esDecimal(numero)){
			return false;
		}
		
		if( numero.charAt(0) == '-' ){
			numero = numero.substring(1,numero.length());
		}
			
		numEnteros				= Math.abs(numEnteros);
		numDecimales			= Math.abs(numDecimales);
		String []datos 		= null;
		String parteEntera 	= "";
		String parteDecimal	= "";
		
		datos = numero.split("\\.");
		
		parteEntera = datos[0];
		if(datos.length > 1)
			parteDecimal = datos[1];
		
		if(parteEntera.length()>numEnteros)
			return true;
		if(parteDecimal.length()>numDecimales)
			return true;
		
		return false;
	}
	
	
	public static String getCreateFile(String query, List lVarBind, String path, String separador, String extension, String []encabezado, int numCols) 
		throws Exception {
		
		System.out.println("Comunes::getCreatFile(E)");
		
		AccesoDB				con				= null;
		CreaArchivo 		archivo 			= null;
		FileWriter 			writer 			= null;
		BufferedWriter 	buffer 			= null;
		
		Registros 			registros 		= null;
		boolean 				hayRegistros 	= false;
		
		
		try {
		
			con = new AccesoDB();
			con.conexionDB();
		
			
			archivo 	= new CreaArchivo();
			writer 	= new FileWriter(path+archivo.nombreArchivo()+"."+extension);
			buffer 	= new BufferedWriter(writer);
					
			registros = con.consultarDB(query,lVarBind,false);
			
			// Agregar encabezado
			if (encabezado != null){
				for(int i=0;i<encabezado.length;i++){
					buffer.write(encabezado[i]==null?"":encabezado[i]);
					buffer.write((separador==null)?"":separador);
				}
				buffer.write("\r\n");
			}
			
			// Extraer registros
			while(registros.next()) {
				hayRegistros = true;
				for(int i=1;i<=numCols;i++) {
					buffer.write(registros.getString(i));
					buffer.write(separador);
				}
				buffer.write("\r\n");
			}
			buffer.close();

		} catch(Exception exception) {
			System.out.println("Comunes::getCreateFile(Exception)");
			exception.printStackTrace();
			if(buffer != null) {try { buffer.close(); }catch(Exception e){}};
			throw exception;
		}finally{
			if(con.hayConexionAbierta())	con.cierraConexionDB();
		}
		
		System.out.println("Comunes::getCreatFile(S)");
		
		return hayRegistros?(archivo.nombre + "." +extension):null;
	}
	
	/**
	 * Elimina acentos de la cadena recibida
	 * @param strWord - cadena recibida
	 */
	public static String eliminaAcentos(String strWord) {
     
		 char[] strAcentos = {'�','�','�','�','�','�','�','�','�','�'};
		 char[] strSinAcen = {'a','e','i','o','u','A','E','I','O','U'};
		 if(strWord!=null && !strWord.equals(""))
		 {
			 for(int x=0; x<strAcentos.length;x++)
			 {
				 strWord = strWord.replace(strAcentos[x],strSinAcen[x]);
			 }
		 }		     
     return strWord;
		 /*String NewStr = "";     
		 NewStr = strWord.replace('�','a');
		 NewStr = strWord.replace('�','i');
		 NewStr = strWord.replace('�','i');
		 NewStr = strWord.replace('�','o');
		 NewStr = strWord.replace('�','U');
		 
		 NewStr = strWord.replace('�','A');
		 NewStr = strWord.replace('�','E');
		 NewStr = strWord.replace('�','I');
		 NewStr = strWord.replace('�','O');
		 NewStr = strWord.replace('�','U');
     */
  }
 
 

 	/**
	 * Remplaza los caracteres especificados en el parametro strBuscar, de la
	 * cadena, por lo respectivos caracteres en strReemplazar
	 * 
	 * @param cadena Cadena a evaluar
	 * @param strCharsBuscar Caracteres a reemplazar
	 * @param strCharsReemplazar Caracteres por los que se remplaza
	 * @return Cadena con los remplazos realizados
	 */
	public static String reemplazarCaracteres(String cadena, 
			String strCharsBuscar, String strCharsReemplazar) {
		if (cadena == null || cadena.equals("") || 
				strCharsBuscar == null  || strCharsBuscar.equals("") ) {
				return cadena;
		}
		if (strCharsReemplazar == null) {
			strCharsReemplazar = "";
		}
		boolean modificada = false;
		StringBuffer buf = new StringBuffer(cadena.length());
		for (int i = 0; i< cadena.length(); i++) {
			char ch = cadena.charAt(i);
			int index = strCharsBuscar.indexOf(ch);
			if (index >= 0) {
				modificada = true;
				if (index < strCharsReemplazar.length()) {
					buf.append(strCharsReemplazar.charAt(index));
				}
			} else {
				buf.append(ch);
			}
		}
		if (modificada) {
			return buf.toString();
		} else {
			return cadena;
		}
	}

 	/**
	 * Remplaza los caracteres no soportados en la firma digital, cuando esta
	 * se realiza usando el navegador Firefox. Por alguna causa el texto que se firma
	 * en firefox, ha de considerar a los caracteres especiales con una codificacion
	 * diferente a la que maneja el servidor.
	 * @param cadena Cadena a Firmar
	 * @return Cadena que se puede usar para firmar
	 */
	public static String getCadenaFirmaMozilla(String cadena) {
		StringBuffer sbCharsBuscar = new StringBuffer(256);
		StringBuffer sbCharsReemplazar = new StringBuffer(256);
		
		sbCharsBuscar.append("��������������");	//el \r se elimina de la cadena
		sbCharsReemplazar.append("aAeEiIoOuUuUnN");
		
		for (char ch = 128; ch <= 255; ch++) {
			if (sbCharsBuscar.indexOf(String.valueOf(ch)) == -1) {	//Si no esta en la lista inicial de caracteres a buscar
				sbCharsBuscar.append(ch);
				//Nos pidieron que caracteres del ascii extendido, diferentes 
				//a los ya especificados arriba en sbCharsBuscar,
				//se reemplacen por un guion medio
				sbCharsReemplazar.append('-');
			}
		}
	
		return reemplazarCaracteres(cadena, sbCharsBuscar.toString(), sbCharsReemplazar.toString());
	}
  
  /**
	*  Este metodo devuelve el hash (usando el algoritmo MD5) del archivo
	*  proporcionado en el parametro <tt>rutaArchivo</tt>.
	*
	* @since F038-2008
	* @param rutaArchivo Cadena de texto con la Ruta del Archivo cuyo Hash se calculara
	* @return String con el hash del archivo en cuestion.
	*/
	public static String getHash(String rutaArchivo)
		throws Exception {
       
      System.out.println("Comunes::getHash(E)");
       
      String             hash    = "";
      FileInputStream    fis     = null;
      MessageDigest      md      = null;
      java.io.File       f       = null;
                     
      try {
          
          f   = new java.io.File(rutaArchivo);
          fis = new FileInputStream(f);
          md  = MessageDigest.getInstance("MD5");
          
          long longitud = f.length();
                  
          byte[] bytes = new byte[(int) longitud];

          // Leer los Bytes
          md.reset();
          fis.read(bytes);
                 
          // Obtener MD5
          md.update(bytes);
         
          // Convertir a hexadecimal el resultado del algoritmo
          byte[] digest = md.digest();
          StringBuffer hexString = new StringBuffer();
          for (int i=0;i<digest.length;i++) {
             String aux = Integer.toHexString(0xFF & digest[i]);
             if(aux.length()==1)
                 aux = "0"+aux;
             hexString.append(aux);
          }
          hash = hexString.toString();

        } catch (Exception e) {
             System.out.println("Comunes::getHash(Exception)");                                     
             e.printStackTrace();
				 throw e;
        }finally{
           try { fis.close(); }catch(Exception e){};
           System.out.println("Comunes::getHash(S)");
        }
         
        return hash;     
     }

	/**
	 * Este m�todo lee el archivo de propiedades. No se debe especificar
	 * la extensi�n del archivo.
	 * El archivo debe de estar en una ruta que este contemplada dentro
	 * del CLASSPATH, de lo contrario no ser� localizado.
	 *
	 * @param archivo nombre del archivo properties (sin la extension)
	 * @return Properties El objeto de propiedades
	 * @exception IOException si la carga de propiedades falla.
	 */
	public static Properties loadParams(String file)
			throws IOException {

		// Carga un ResourceBundle y apartir de este crea un Properties
		Properties		 prop	 = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);

		// Obtiene las llaves y la coloca en el objeto Properties
		Enumeration enumx = bundle.getKeys();
		String key	= null;
		while (enumx.hasMoreElements()) {
			key = (String) enumx.nextElement();
			prop.put(key, bundle.getObject(key));
		}

		return prop;
	}

	/**
	 * M�todo que verifica que una cuenta CLABE sea v�lida.
	 * @param cuentaClabe Cadena con el n�mero de cuenta CLABE.
	 * @return clabeValida true Si la cuenta CLABE es valida, false en caso contrario.
	 * @author Alberto Cruz Flores
	 * @since FODEA 026 - 2009, 04 de Diciembre de 2009.
	 */
	 public static boolean esCuentaClabeValida(String cuentaClabe){
		boolean clabeValida = false;
		int digitoVerificador = 0;
		int digitoActual = 0;
		int digitoObtenido = 0;
		int sumaTotal = 0;
		int moduloSumaTotal = 0;
		
		if(cuentaClabe.length() == 18 && esNumeroEnteroPositivo(cuentaClabe)){
			digitoVerificador = Integer.parseInt(cuentaClabe.substring(17));
			for (int i = 0; i < 17; i++) {
				digitoActual = Integer.parseInt(cuentaClabe.substring(i, i + 1));
				if (i == 0 || i == 3 || i == 6 || i == 9 || i == 12 || i == 15) {
					sumaTotal += digitoActual * 3;
				}
				if (i == 1 || i == 4 || i == 7 || i == 10 || i == 13 || i == 16) {
					sumaTotal += digitoActual * 7;
				}
				if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14) {
					sumaTotal += digitoActual * 1;
				}
			}

			moduloSumaTotal = sumaTotal % 10;

			if (moduloSumaTotal > 0) {
				digitoObtenido = 10 - moduloSumaTotal;
			} else {
				digitoObtenido = 0;
			}

			if (digitoObtenido == digitoVerificador) {
				clabeValida = true;
			}
		}
		return clabeValida;
	 }
	 
	/**
	 * Crear los directorios en caso de ser necesario del archivo especificado.
	 * 
	 * Por ejemplo:
	 * Comunes.crearDirectoriosArchivo("/oraias/nafin10g/OC4J_Nafin/applications/nafin/nafin-web/00tmp/13descuento/miArchivo.pdf")
	 * Si no existe la ruta /00tmp/13descuento este m�todo se encarga de generarla
	 * @param archivo Ruta de Directorios + Archivo
	 */
	public static void crearDirectoriosArchivo(String rutaArchivo) {
		if(rutaArchivo!=null && !rutaArchivo.equals("")) {
			String rutaDir = rutaArchivo.substring(0,rutaArchivo.lastIndexOf("/")+1);
			Comunes.crearDirectorios(rutaDir);
		}
	}

	/**
	 * Crea los directorios de ser necesario.
	 * Util para cuando no existe un directorio temporal.
	 * @param rutaDir Ruta de Directorio
	 */
	public static void crearDirectorios(String rutaDir) {
		if(rutaDir!=null && !rutaDir.equals("")) {
			File fRuta = new File(rutaDir);
			if (!fRuta.exists()) {
				fRuta.mkdirs();
			}	
		}
	}

	/**
	 * Elimina un directorio.
	 * Util para eliminar una carpeta
	 * @param directory, la carpeta a eliminar
	 */
	public static void deleteDirectory(File directory) {
		if(directory!=null && directory.exists()) {
			File[] files = directory.listFiles();
			for(int i = 0; i < files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
			directory.delete();
		}
	}

	/**
	 *
	 * Suprime todas las comas que se presenten en un n�mero.
	 *
	 * @param numero <tt>String</tt> con el n�mero, que puede contener o no comas.
	 * @return <tt>String</tt> con las comas filtradas. O en su caso el mismo valor de 
	 * entrada si este es null o viene vac�o.
	 *
	 */
	public static String filtraComas(String numero){
	  
	  if( numero == null || numero.trim().equals("")) return numero;
	  return numero.replaceAll(",","");
	  
   }
	
	/**
	 * Determina si la cadena de texto proporcionada contine una lista de elementos 
	 * separados por comas
	 * @param cadena <tt>String</tt> con los elementos a revisar.
	 * @return <tt>true</tt> si se encontro al menos una coma. <tt>false</tt> en caso 
	 * contrario.
	 */
	public static boolean esUnaLista(String cadena){
		
		boolean validacion = false;
		if(cadena != null && cadena.indexOf(",") != -1){
			validacion = true;
		}
		return validacion;
		
	}
	
	/**
	 * Convierte la cadena de texto en un <tt>ArrayList</tt>.
	 * 
	 * @param lista <tt>String</tt> con los caracteres a convertir
	 * @param separador <tt>String</tt> con los caracteres que funcionan como delimitador 
	 * de la lista
	 * @return <tt>ArrayList</tt> con la lista requerida.
	 *
	 */
	public static ArrayList<String> stringToArrayList(String lista, String separador){
		
		ArrayList<String> resultado 		= null;
		
		if(lista == null){
			return new ArrayList<>();
		}
		
		if(separador == null){
			separador = "";
		}
		
		boolean   separadorVacio = separador.equals("")?true:false;
		
		resultado = new ArrayList<>(Arrays.asList(lista.split(separador,-1)));
		
		if(separadorVacio && resultado != null && resultado.size() > 1){
			resultado.remove(0);
			if(resultado.size() > 1){
				resultado.remove(resultado.size()-1);
			}
		}
		
		return (resultado == null?new ArrayList<String>():resultado);
	}
	
	/**
	 * Convierte un <tt>ArrayList</tt> en una cadena de texto.
	 * 
	 * @param lista <tt>String</tt> con los caracteres a convertir
	 * @param separador <tt>String</tt> con los caracteres que funcionan como delimitador 
	 * de la lista
	 * @return <tt>String</tt> con la lista requerida.
	 *
	 */
	public static String arrayListToString(ArrayList lista, String separador){
		
		StringBuffer buffer = new StringBuffer();
		
		if(separador == null){
			separador = "";
		}
		
		if(lista == null){
			lista = new ArrayList();
		}
		
		for(int i=0;i<lista.size();i++){
			if(i>0) buffer.append(separador);
			buffer.append( (String) (lista.get(i) == null?"":lista.get(i)) );
		}
		
		return buffer.toString();
	}
	
	/**
	 * Convierte caracteres que tienen significado especial en HTML por su
	 * equivalente.
	 *
	 * @param cadena <tt>StringBuffer</tt> con los caracteres especiales a codificar.
	 * @return cadena codificada.
	 * @author jshernandez
	 *
	 */
	public static StringBuffer escapaCaracteresEspeciales(StringBuffer cadena){
		
		StringBuffer resultado = new StringBuffer();
		
		if( cadena == null){
			return null;
		}
      
		if( cadena.length() == 0){
         return resultado;
      }
		
		for(int i=0;i<cadena.length();i++){
			char c = cadena.charAt(i);
			if(c == '<'){
				resultado.append("&lt;");
			}else if(c == '>'){
				resultado.append("&gt;");
			}else if(c == '"'){
				resultado.append("&quot;");
			}else if(c == '&'){
				resultado.append("&amp;");
			}else{
				resultado.append(c);
			}
		}
		
		return resultado;
		
	}
	
	/**
	 * Convierte caracteres que tienen significado especial en HTML por su
	 * equivalente.
	 *
	 * @param cadena <tt>String</tt> con los caracteres especiales a codificar.
	 * @return cadena codificada.
	 * @author jshernandez
	 *
	 */
	public static String escapaCaracteresEspeciales(String cadena){
		
		StringBuffer resultado = new StringBuffer();
		
		if( cadena == null){
			return null;
		}
		if( cadena.length() == 0){
         return resultado.toString();
      }
      
		for(int i=0;i<cadena.length();i++){
			char c = cadena.charAt(i);
			if(c == '<'){
				resultado.append("&lt;");
			}else if(c == '>'){
				resultado.append("&gt;");
			}else if(c == '"'){
				resultado.append("&quot;");
			}else if(c == '&'){
				resultado.append("&amp;");
			}else{
				resultado.append(c);
			}
		}
		
		return resultado.toString();
		
	}
	
	/**
	 * Convierte caracteres que tienen significado especial en HTML por su
	 * equivalente.
	 * @param s
	 * @return cadena
	 */
	public static final String escapeHTML(String s){
		StringBuffer sb = new StringBuffer();
		int n = s.length();
		for (int i = 0; i < n; i++) {
			char c = s.charAt(i);
			switch (c) {
				case '<': sb.append("&lt;"); break;
				case '>': sb.append("&gt;"); break;
				case '&': sb.append("&amp;"); break;
//				case '"': sb.append("&quot;"); break;
//				case '�': sb.append("&aacute;");break;
//				case '�': sb.append("&Aacute;");break;
//				case '�': sb.append("&eacute;");break;
//				case '�': sb.append("&Eacute;");break;
//				case '�': sb.append("&iacute;");break;
//				case '�': sb.append("&Iacute;");break;
//				case '�': sb.append("&oacute;");break;
//				case '�': sb.append("&Oacute;");break;
//				case '�': sb.append("&uacute;");break;
//				case '�': sb.append("&acute;");break;         
//				case '�': sb.append("&uuml;");break;
//				case '�': sb.append("&Uuml;");break;
				default:  sb.append(c); break;
			}
		}
		return sb.toString();
	}


	/**
	 * Realiza la validaci�n de una imagen en cuanto a tama�o de alto y ancho.
	 * @param file Objeto File del archivo de imagen a validar
	 * @param height Alto de la imagen en pixeles
	 * @param width Ancho de la imagen en pixeles
	 * @return true si la imagen es de las minesiones especificadas o false de lo contrario.
	 */
	public static boolean validaTamanioImg(File file, int height, int width) {
	   try {
			SimpleImageInfo imagen = new SimpleImageInfo(file);
			if(imagen.getHeight()==height&&imagen.getWidth()==width){
				return true;
			}
			return false;
		} catch(Throwable t) {
			throw new AppException("Error al realizar la validaci�n de tama�o de la imagen", t);
		}
	}

	/**
	 * Realiza la validaci�n de una imagen en cuanto a tama�o de alto y ancho.
	 * @param rutaImg Ruta y nombre del archivo de imagen a validar
	 * @param height Alto de la imagen en pixeles
	 * @param width Ancho de la imagen en pixeles
	 * @return true si la imagen es de las minesiones especificadas o false de lo contrario.
	 */
	public static boolean validaTamanioImg(String rutaImg, int height, int width) {
	   try {
			File file = new File(rutaImg);
			return validaTamanioImg(file,height,width);
		} catch(Throwable t) {
			throw new AppException("Error al realizar la validaci�n de tama�o de la imagen", t);
		}
	}
	
	/**
	 * Valida que el archivo identificado por la ruta: rutaAbsolutaArchivo no exceda la longitud m�xima permitida.
	 *
	 * @param String <tt>rutaAbsolutaArchivo</tt> ruta absoluta al archivo.
	 * @param longitudMaxima Longitud M�xima permitida del archivo.
	 *
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si se excede la tama�o m�ximo;
	 *         y <tt>false</tt> en caso contrario.
	 *
	 * @author jshernandez
	 * @since 27/11/2014 12:44:22 p.m.
	 *
	 */
	public static boolean excedeTamanioMaximo( String rutaAbsolutaArchivo, long tamanioMaximo ){
		
		boolean excedeTamanio = false;
		try {
			
			File   file			  = new File(rutaAbsolutaArchivo);
			if( file.length() > tamanioMaximo ){
				excedeTamanio = true;
			}
			
		} catch(Exception e){
			excedeTamanio = false;
		}
		
		return excedeTamanio;
		
	}
	
	/**
	 *
	 * Revisa si el String proporcionado en el parametro: <tt>campo</tt> corresponde a un espacio en blanco o
	 * a una cadena vac�a.
	 *
	 * @param campo <tt>String</tt> a validar.
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si se trata de un campo vac�o;
	 *         regresa <tt>false</tt> en caso contrario.
	 *
	 * @author Salim Hernandez
	 * @date   01/12/2014 11:16:02 a.m.
	 * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
	 *
	 */
	public static boolean esVacio(String campo){
		
		boolean espacioEnBlanco = false;
		if( campo == null || campo.matches("^\\s*$") ){
			espacioEnBlanco = true;
		}
		return espacioEnBlanco;
		
	}
	
	/**
	 * 
	 * Este m�todo compara dos fechas (<tt>fecha1</tt> y <tt>fecha2</tt>) las cuales
	 * vienen con el formato indicado en el par�metro <tt>formato</tt>.
	 *
	 * @param 	fecha1  		Cadena de caracteres en la que se indica la primera 
	 *                      fecha a comparar.
	 * @param 	fecha2  		Cadena de caracteres en la que se indica la segunda
	 *                      fecha a comparar.
	 * @param	formato		Formato con el cual se parsearan las fechas, por ejemplo,
	 *								para fechas del tipo 99/12/31, se usaria el formato yyMMDD.
	 *
	 * @return 	int  			Regresa:<br>
	 *									-1 Si  fecha1 <  fecha2            <br>
	 *                         0  Si  fecha1 == fecha2            <br>
	 * 								1  Si  fecha1 >  fecha2            <br>
	 *
	 * @throws AppException
	 * 
	 * @since  F001 - 2015 -- PAGOS - Vencimientos 1er Piso
	 * @author jshernandez
	 * @date   27/04/2015 05:08:02 p.m.
	 *
	 */
	public static int comparaFechas(String fecha1, String fecha2, String formato)
		throws AppException {
		
		int comparacion = Integer.MIN_VALUE;
		
		try {
			
			SimpleDateFormat sdf = null;
			
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha1); 
			Calendar calendarFecha1	= sdf.getCalendar();

			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha2); 
			Calendar calendarFecha2	= sdf.getCalendar();
			
			// Comparar fechas, se hace as� debido a que todav�a
			// no existe el m�todo compareTo
			if (        calendarFecha1.before(calendarFecha2) ){
				comparacion = -1;
      	} else if ( calendarFecha1.equals(calendarFecha2) ){
				comparacion =  0;
			} else if ( calendarFecha1.after(calendarFecha2)  ){
				comparacion =  1;
      	}
			
		} catch (Exception e) {
			
			System.err.println("comparaFechas(Exception)");
			System.err.println("comparaFechas.fecha1  = <" + fecha1  + ">");
			System.err.println("comparaFechas.fecha2  = <" + fecha2  + ">");
			System.err.println("comparaFechas.formato = <" + formato + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al comparar las fechas: "+e.getMessage(),e);
			
		}
		
		return comparacion;
		
   }
	
	/**
	 * Valida si el caracter proporcionado, corresponde a un caracter de control
	 * no permitido por el est�ndar 1.0 de XML:
	 * <br>
	 * 	http://www.w3.org/TR/REC-xml/#charsets
	 * <br>
	 *    -> S�lo est�n permitidos los caracteres de control: El tabulador 
	 *    horizontal (\t), el salto de l�nea (\n) y el retorno de carro: (\r).
	 *		<br>
	 *    NOTA (02/09/2015 12:52:44 p.m., by jshernandez): Se modifica validacion 
	 *    de caracteres no permitidos, de tal manera que reporte como caracter no 
	 *    permitido al caracter: "DEL" (0x7F), este caracter no tiene ningun efecto 
	 *    adverso en la creaci�n de documentos XML 1.0 ya que el est�ndar lo permite; 
	 *    se agrega para cumplir con lo indicado en la secci�n de la ayuda de las 
	 *    pantallas del F015 - 2015.
	 * <br>
	 * 	Este m�todo est� dise�ado para funcionar con sistemas basados en ASCII:
	 *    https://en.wikipedia.org/wiki/Newline#Representations, por lo que se
	 *    deber� ejecutar sobre Streams con codificaci�n, ISO-8859-1, windows-1252
	 *    (cp1252), UTF-8, etc.
	 *  
	 * @return true si es un caracter de control no permitido y false en caso 
	 * contrario.
	 * 
	 * @param caracter N�mero con el c�digo del caracter a validar.
	 * 
	 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales
	 * @author jshernandez
	 * @date   18/06/2015 08:10:19 p.m.
	 * 
	 */
	public static boolean tieneCaracterControlNoPermitidoEnXML1(int caracter){
	
			boolean noPermitido = false;

			if( caracter >= 0 && caracter < 32 ){ // El caracter proporcionado pertenece al rango de caracteres de control
				if( 
					 caracter != 9 // Tabulador horizontal \t
					&&
					 caracter != 10// Nueva l�nea:      	 \n
					&&
					 caracter != 13 // Retorno de carro  	 \r
				){
					noPermitido = true;
				}
				
			} else if( caracter == 127 ){ // 0x7F DEL Delete character 
				
				noPermitido = true;
				
			}
			
			return noPermitido;
			
	}
	
	/**
	 * Valida si al menos uno de los caracteres proporcionados, corresponde a un caracter de control
	 * no permitido por el est�ndar 1.0 de XML:
	 * <br>
	 * 	http://www.w3.org/TR/REC-xml/#charsets
	 * <br>
	 *    -> S�lo est�n permitidos los caracteres de control: El tabulador 
	 *    horizontal (\t), el salto de l�nea (\n) y el retorno de carro: (\r).
	 * <br>
	 * 	Este m�todo est� dise�ado para funcionar con sistemas basados en ASCII:
	 *    https://en.wikipedia.org/wiki/Newline#Representations, por lo que se
	 *    deber� ejecutar sobre Streams con codificaci�n, ISO-8859-1, windows-1252
	 *    (cp1252), UTF-8, etc.
	 *  
	 * @return true si se encontr� un caracter de control no permitido y false en caso 
	 * contrario.
	 * 
	 * @param linea String con los caracteres a validar.
	 * 
	 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales
	 * @author jshernandez
	 * @date   26/06/2015 07:29:28 p.m.
	 * 
	 */
	public static boolean tieneCaracterControlNoPermitidoEnXML1(String linea){
	
		boolean noPermitido = false;
			
		if( linea == null ){
			return noPermitido; // No se detectaron caracteres de control.
		}

		for(int i=0;i<linea.length();i++){
			int caracter = linea.charAt(i);
			if( Comunes.tieneCaracterControlNoPermitidoEnXML1(caracter) ){
				noPermitido = true;
				break;
			}
		}
			
		return noPermitido;
			
	}
	
	/**
	 *
	 * Revisa si el String proporcionado en el parametro: <tt>login</tt> corresponde a uno valido.
	 *
	 * @param 	login <tt>String</tt> a validar.
	 * @param 	msg String array donde en el elemento 0, si el array no es nulo, se pone la
	 *          descripci�n del mensaje de error.
	 * @return  <tt>boolean</tt> con valor <tt>true</tt> si se trata de un login valido;
	 *          regresa <tt>false</tt> en caso contrario.
	 *
	 * @author Salim Hernandez
	 * @date   10/09/2015 07:22:18 p.m.
	 * @fodea  F021 - 2015 -- ADMIN NAFIN -- Revocar Certificados Digitales Administrador
	 *
	 */
	public static boolean esLoginValido(String login, String[] msg){
		
		boolean valido = false;
		
		if( 			login != null && login.length() > 8 			){
			
			if( msg != null && msg.length > 0 ){
				msg[0] = "El tama�o m�ximo para este campo es de 8";
			}
			valido = false;
			
		} else if( 	login != null && login.matches("^[0-9]+$") 	){
			
			if( msg != null && msg.length > 0 ){
				msg[0] = null;
			}
			valido = true;
			
		} else {
			
			if( msg != null && msg.length > 0 ){
				msg[0] = "S�lo se aceptan valores num�ricos";
			}
			valido = false;
			
		}
		return valido;
		
	}
	
   
}