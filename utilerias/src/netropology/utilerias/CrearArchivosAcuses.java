package netropology.utilerias; 

import java.io.*;
import java.sql.*;
import java.util.*;

import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
 
 
public class CrearArchivosAcuses { 

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CrearArchivosAcuses.class);
	private String codigoEjecucion;
	private String estatusCarga;
	private String resumenEjecucion;
	private String recibo;	
	private String acuse;
	private String usuario;
	private String tipoUsuario;
	private String version;
	private String tabla;
	private String ic_epo;
	private String tipoArchivo;
	private String rutaArchivo;
	private String strDirectorioTemp;
	private String desError;
	  
	public String setArmaArchivoXML( ) {
		
		log.info("setArmaArchivoXML(S)");
	
		StringBuffer contenidoArchivo = new StringBuffer();
				
		try {
			contenidoArchivo.append(
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"<resultado>");
					
			if(tipoUsuario.equals("EPO")) {
				contenidoArchivo.append("<codigoEjecucion xsi:type="+"\"xsd:int\""+">"+this.codigoEjecucion +"</codigoEjecucion>");
				contenidoArchivo.append("<estatusCarga xsi:type="+"\"xsd:int\""+">"+this.estatusCarga +"</estatusCarga>");
				contenidoArchivo.append("<resumenEjecucion xsi:type="+"\"xsd:String\""+">"+this.resumenEjecucion +"</resumenEjecucion>");
			
			}else  if(tipoUsuario.equals("IF")) {
				contenidoArchivo.append("<recibo xsi:type="+"\"xsd:String\""+">"+this.recibo +"</recibo>");				
				contenidoArchivo.append("<resumenEjecucion xsi:type="+"\"xsd:String\""+">"+this.resumenEjecucion +"</resumenEjecucion>");	
			}
		contenidoArchivo.append("</resultado>");	
		
		
		}catch(Exception e){
				throw new AppException("Error al obtener el archivo de la base de datos: ", e);
			}finally{				
				log.info("setArmaArchivoXML(S)");
		}
	
		return contenidoArchivo.toString();
	}

	
	public boolean guardarArchivo( ) {
		
		log.info("guardarArchivo (E)");
		
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;		
		List varBind = new ArrayList();
		StringBuffer  strSQL = new StringBuffer();
		int existe =0;
		StringBuffer  encabezado = new StringBuffer();
		StringBuffer  datos = new StringBuffer();
		StringBuffer  info = new StringBuffer();
		StringBuffer  contenidoArch = new StringBuffer();
		String campo ="";
		
		try { 
			
			con.conexionDB();
			
			if(!"".equals(this.acuse)){					
				if(version.equals("WEBSERVICE"))  {
					contenidoArch = new StringBuffer();
					contenidoArch.append(setArmaArchivoXML());
						
					strSQL = new StringBuffer();
					strSQL.append(" INSERT INTO COM_ARCDOCTOS(CC_ACUSE,  IC_USUARIO ,  DF_FECHAHORA , BI_XML  , CC_VERSION   )  "+
									  " VALUES ( ?, ? , Sysdate , empty_clob() , ?  )");
										
													
					varBind = new ArrayList();
					varBind.add(this.acuse);  
					varBind.add(this.usuario);
					varBind.add(this.version);             
					log.info(" strSQL ===== "+strSQL.toString()  +"  varBind  = "+varBind );            
					ps = con.queryPrecompilado(strSQL.toString(), varBind);
					ps.executeUpdate();
												
					strSQL = new StringBuffer();
					strSQL.append(" SELECT  BI_XML  FROM   COM_ARCDOCTOS  "+
									  " where CC_ACUSE =  ? "+
									  " FOR UPDATE  ");
										
					 log.info(" strSQL ===== "+strSQL.toString()   );   
					 ps = con.queryPrecompilado(strSQL.toString()); 
					 ps.clearParameters();
					 ps.setString(1, acuse);
					 rs = ps.executeQuery();
					 rs.next();
					 Clob clob = rs.getClob("BI_XML");
					 clob.setString(1, contenidoArch.toString() );
					 rs.close();
					 ps.close();  
			
				}else  if(version.equals("ENLACES"))  {
				
					//PARA SACAR  LOS CAMPOS DE LAS TABLAS 
					strSQL = new StringBuffer();
					strSQL.append(" SELECT * FROM "+this.tabla  + " where ROWNUM <= 1 " );	
					log.debug(" strSQL ===== "+strSQL.toString());
					
					rs = con.queryDB(strSQL.toString());
					ResultSetMetaData	rsMD = rs.getMetaData();
					int numCols = rsMD.getColumnCount();
					int cont =0;
					int presencia =0;
					
					if(rs.next()) {				
						encabezado = new StringBuffer();				
						for(int i=1;i<=numCols;i++) {					
							if(cont==0) {
								encabezado.append( rsMD.getColumnName(i) +","); // obtengo el nombre de las columnas 
							}//if(cont==0)						
							presencia=	encabezado.indexOf("IC_EPO");	   //VERIFICO SI HAY UN CAMPO IC_EPO
						}							
					}
					rs.close();
					
					
				//PARA SACAR  LOS CAMPOS DE LAS TABLAS 			
					strSQL = new StringBuffer();
					strSQL.append(" SELECT * FROM "+this.tabla  );	
					if(presencia>0){
					 strSQL.append( " where ic_epo = "+this.ic_epo );				 
					}			
					log.debug(" strSQL ===== "+strSQL.toString()  );
					
					rs = con.queryDB(strSQL.toString());
					int numCols_1 = rsMD.getColumnCount();				
					while(rs.next()) {
						datos = new StringBuffer();				
						for(int i=1;i<=numCols_1;i++) {	
							String rs_campo = rs.getString(i)==null?"":rs.getString(i);
							datos.append(rs_campo.replace(',',' ')+",");
						}				
						info.append(datos+"\n");
					}
					rs.close();
					
					contenidoArch.append(encabezado.toString() +"\n");
					contenidoArch.append(info);
	  
					strSQL = new StringBuffer();
					strSQL.append(" INSERT INTO COM_ARCDOCTOS(CC_ACUSE,  IC_USUARIO ,  DF_FECHAHORA  , CC_VERSION  , BI_CSV  )  "+
										  " VALUES ( ?, ? , Sysdate , ? , ?   )");
								
					log.info(" strSQL ===== "+strSQL.toString()  );
					
					byte[] archivo_enlace = contenidoArch.toString().getBytes();
					
					ps = con.queryPrecompilado(strSQL.toString());
					ps.setString(1, this.acuse );			   	
					ps.setString(2, this.usuario);
					ps.setString(3, this.version); 
					ps.setBytes(4, archivo_enlace );		
					ps.executeUpdate();
					ps.close();  
					
					
				}else  if(version.equals("PANTALLA"))  {
						
				
					if(this.tipoArchivo.equals("PDF") ) { 	campo =  "BI_PDF "; 		  }
					if(this.tipoArchivo.equals("CSV") ) { 	campo =  "BI_CSV "; 		  }
					if(this.tipoArchivo.equals("XML") ) { 	campo =  "BI_XML "; 		  }
			
					strSQL = new StringBuffer();	
					strSQL.append(" SELECT count(*)  existe   FROM COM_ARCDOCTOS  WHERE CC_ACUSE = ? ");						  
					varBind = new ArrayList();
					varBind.add(acuse);   
					log.debug( strSQL.toString()  +"   "+varBind);   
					
					ps = con.queryPrecompilado(strSQL.toString(), varBind);
					rs = ps.executeQuery();
					
					if(rs.next()){			
						existe = rs.getInt("existe");			
					}
					rs.close();
					ps.close();
					
					if( existe==0) {
						strSQL = new StringBuffer();
						strSQL.append(" INSERT INTO COM_ARCDOCTOS(CC_ACUSE,  IC_USUARIO ,  DF_FECHAHORA , CC_VERSION   )  "+
										  " VALUES ( ?, ? , Sysdate, ?   )");
						varBind = new ArrayList();
						varBind.add(this.acuse);  
						varBind.add(this.usuario); 
						varBind.add(this.version); 	
					
						log.info(" strSQL ===== "+strSQL.toString()  +"  varBind  = "+varBind );
						
						ps = con.queryPrecompilado(strSQL.toString(), varBind);
						ps.executeUpdate();
						ps.close();
					}
						  
					File archivo = new File(this.rutaArchivo);
					FileInputStream fileinputstream = new FileInputStream(archivo);
					  
					strSQL = new StringBuffer();					
					strSQL.append(" UPDATE COM_ARCDOCTOS "+  
									" SET "+campo +" =  ?    " +		  	 																			
									" WHERE CC_ACUSE = ? ");
								
					log.debug("strSQL "+strSQL);
					
					ps = con.queryPrecompilado(strSQL.toString());
					ps.setBinaryStream(1, fileinputstream, (int)archivo.length());			   			  
					ps.setString(2, this.acuse); 		
					ps.executeUpdate();
					ps.close();  
									
				}
			}
		} catch(Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error ("guardarArchivo  " + e);
			throw new AppException("SIST0001");
			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardarArchivo (S) ");
			}        
		}
		return exito;
	}
	
	/**
	 * Metodo que verifica si existe o no el archivo en la base
	 * @return 
	 */
	public int existerArcAcuse() {
		log.info("existerArcAcuse(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();	
		String campo ="";
		int existe =0;		
		 
		try{
		
			con.conexionDB();
					
			if(this.tipoArchivo.equals("PDF") ) { 	campo =  "BI_PDF "; 	  }
			if(this.tipoArchivo.equals("CSV") ) { 	campo =  "BI_CSV "; 	  }
			if(this.tipoArchivo.equals("XML") ) { 	campo =  "BI_XML "; 		  }
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT LENGTH ("+campo+") as LONGITUD "+
							  " FROM COM_ARCDOCTOS "+
			              " WHERE CC_ACUSE = ? ");
			varBind = new ArrayList();					  
			varBind.add(this.acuse);   
		
			log.debug( strSQL.toString()  +"   "+varBind);   
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){			
				existe = rst.getInt("LONGITUD");			
			}
			rst.close();
			pst.close();
			
			
		}catch(Exception e){
			throw new AppException("Error verificar si existe archivo en la base  ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("existerArcAcuse(S)");
		}		
		return existe;
	}
	

	public String desArchAcuse( ) {
		log.info("desArchAcuse(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "", nombreArchivo = "", campo ="", extension="";
		try{
			con.conexionDB();
			
			if(this.tipoArchivo.equals("PDF") ) { 	campo =  "BI_PDF ";  extension = "pdf";	  }
			if(this.tipoArchivo.equals("CSV") ) { 	campo =  "BI_CSV "; 	extension = "csv";	  }
			if(this.tipoArchivo.equals("XML") ) { 	campo =  "BI_XML "; 	extension = "xml";	  }
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT "+campo +"AS ARCHIVO ,  IC_USUARIO   "+
							  " FROM COM_ARCDOCTOS "+
			              " WHERE CC_ACUSE = ?  ");   
			varBind = new ArrayList();
			varBind.add(this.acuse);		    
		
			log.debug( strSQL.toString()  +"   "+varBind);  
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){ 
			
				nombreArchivo = Comunes.cadenaAleatoria(8) + "."+extension;
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				
				String usuario = (rst.getString("IC_USUARIO") == null)?"":rst.getString("IC_USUARIO");				
				
				if(this.tipoArchivo.equals("XML") ) {
				
					Clob clob = rst.getClob("ARCHIVO");				
					
					InputStream inStream = clob.getAsciiStream();
					int size = (int)clob.length();					
					byte[] buffer = new byte[size];
					int length = -1;	
					while ((length = inStream.read(buffer)) != -1){
						fileOutputStream.write(buffer, 0, length);
					}
					
				}else if(!this.tipoArchivo.equals("XML")   ) { 
				
					Blob blob = rst.getBlob("ARCHIVO");
					
					InputStream inStream = blob.getBinaryStream();
					int size = (int)blob.length();					
					byte[] buffer = new byte[size];
					int length = -1;	
					while ((length = inStream.read(buffer)) != -1){
						fileOutputStream.write(buffer, 0, length);
					}
					
				}
				
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
			
			
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("desArchAcuse(S)");
		}		
		return nombreArchivo;
	}
	 
	/**
	 * De acuerdo al texto que devuelve la consulta, me indica que tipo de carga
	 * se realiz�, y asi puedo indicarle algrid que campos mostrar.
	 * @return PANTALLA == CSV y PDF, WEBSERVICE = XML , ENLACES = CSV 
	 */
	public String obtieneTipoCarga(){
		log.info("obtieneTipoCarga(E)");
		AccesoDB          con     = new AccesoDB();
		PreparedStatement pst     = null;
		ResultSet         rst     = null;
		StringBuffer      strSQL  = new StringBuffer();
		List              varBind = new ArrayList();
		String            version = "";

		try{

			con.conexionDB();

			strSQL.append(" SELECT CC_VERSION FROM COM_ARCDOCTOS WHERE CC_ACUSE = ? ");
			varBind.add(this.acuse);
			log.debug( strSQL.toString() +" "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if(rst.next()){
				version = rst.getString("CC_VERSION")==null?"":rst.getString("CC_VERSION");
			}
			rst.close();
			pst.close();

		}catch(Exception e){
			throw new AppException("Error al obtener el tipo de carga del archivo. ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("obtieneTipoCarga(S)");
		}
		return version;
	}

	/**
	 * 
	 * @return 
	 */
	public boolean guardaBitacoraArch( ) {
		
		log.info("guardaBitacoraArch (E)");
		
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		String estatus = "archivos generados y almacenados correctamente"; 
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		
		try { 
			
			con.conexionDB();
			
			log.info(" desError  = "+desError );
						 
			if(this.desError!=null) { estatus = "evento fallido ";  } 

			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO BI_ARCDOCTOS_ERROR(IC_BITA_ARCH, CC_ACUSE,  IC_USUARIO ,  DF_FECHAHORA , CC_VERSION  , CC_ERROR, CG_ESTATUS   )  "+
									  " VALUES ( SEQ_BI_ARC_ERROR.NEXTVAL , ?, ? , Sysdate , ? , ? , ?  )");
			varBind = new ArrayList();
			varBind.add(this.acuse);  
			varBind.add(this.usuario);
			varBind.add(this.version);
		 	varBind.add(this.desError); 
			varBind.add(estatus);
		
			log.info(" strSQL ===== "+strSQL.toString()  +"  varBind  = "+varBind );
				
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), varBind);
			ps.executeUpdate();
			ps.close();
			con.cierraStatement();
			
		} catch(Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error ("guardaBitacoraArch  " + e);
			throw new AppException("SIST0001");
			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardaBitacoraArch (S) ");
			} 			
		}
		return exito; 
	}
	

	public String getCodigoEjecucion() {
		return codigoEjecucion;
	}

	public void setCodigoEjecucion(String codigoEjecucion) {
		this.codigoEjecucion = codigoEjecucion;
	}

	public String getEstatusCarga() {
		return estatusCarga;
	}

	public void setEstatusCarga(String estatusCarga) {
		this.estatusCarga = estatusCarga;
	}

	public String getResumenEjecucion() {
		return resumenEjecucion;
	}

	public void setResumenEjecucion(String resumenEjecucion) {
		this.resumenEjecucion = resumenEjecucion;
	}

	public String getRecibo() {
		return recibo;
	}

	public void setRecibo(String recibo) {
		this.recibo = recibo;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getRutaArchivo() {
		return rutaArchivo;
	}

	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}

	public String getStrDirectorioTemp() {
		return strDirectorioTemp;
	}

	public void setStrDirectorioTemp(String strDirectorioTemp) {
		this.strDirectorioTemp = strDirectorioTemp;
	}
	
	public String getDesError() {
		return desError;
	}

	public void setDesError(String desError) {
		this.desError = desError;
	}





}