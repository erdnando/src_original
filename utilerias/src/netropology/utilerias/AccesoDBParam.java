package netropology.utilerias;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * Maneja los parametros de Conexion a Base de datos.
 * Para su uso con AccesoDBOracle
 * @see AccesoDBOracle
 * @author Gilberto Aparicio
 */
public class AccesoDBParam implements java.io.Serializable {
	
	private static Map dbParams = new HashMap();
	
	static {
		System.out.println("Cargando Parametros de BD");
		AccesoDBParam.cargaDBParametros();
	}
	
	public AccesoDBParam() {}
	

	/**
	 * Realiza la carga en memoria de los parametros de BD
	 *
	 */
	public static void cargaDBParametros() {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("accesoDBParam");
			Enumeration enumx = bundle.getKeys();
			while (enumx.hasMoreElements()) {
				String key = (String) enumx.nextElement();
				String objeto = (String) bundle.getObject(key);
				if ( objeto != null ) {
					objeto = objeto.trim();
				}
				AccesoDBParam.dbParams.put(key, objeto);
			}
		} catch(RuntimeException e) {	//Si no se encuentra el properties u otro error ocurre en tiempo de ejecución
			System.out.println("Error al cargar los parámetros. " +
					"No se puede continuar");
			throw e;
		}
	}
	
	/**
	 * Obtiene el parametro especificado
	 * @param param Nombre del parámetro
	 * @return Cadena con el valor correspondiente
	 */
	public static String getParametro(String param) {
		
		if (param == null || param.trim().equals("")) {
			System.out.println("param=" + param);
			throw new IllegalArgumentException("AccesoDBParam:getParametro():Error de parametros");
		}
		
		String texto =(String) dbParams.get(param);

		return texto;
	}
}
	


