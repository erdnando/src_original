package netropology.utilerias;

/**
 * Exception lanzada cunado existe un error al obtener algun recurso 
 * a trav�s del ServiceLocator
 * @author Gilberto Aparicio
 *
 */
public class ServiceLocatorException extends AppException {


	public ServiceLocatorException() {
		super();
	}
	
	public ServiceLocatorException(String mensaje) {
		super(mensaje);
	}
	
	public ServiceLocatorException(String mensaje, Throwable causaException){
		super(mensaje, causaException);
	}
	
	public ServiceLocatorException(Throwable causaException) {
		super(causaException);
	}
}