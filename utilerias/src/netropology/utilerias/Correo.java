package netropology.utilerias;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * La clase permite enviar correos via SMTP
 * utilizando el api de java JavaMail
 * @author
 *
 */
public class Correo {

	private boolean debug = true;
	private boolean useAccesoDBOracle = false;
	private String codificacion;
	
	/**
	 * Habilita el envio a la consola de mensajes de Depuracion
	 * en los siguientes metodos: enviaCorreoConDatosAdjuntos() y getMailHost()
	 */
	public void enableDebug(){
		this.debug = true;
	}
	/**
	 * Deshabilita el envio a la consola de mensajes de Depuracion
	 * en los siguientes metodos: enviaCorreoConDatosAdjuntos() y getMailHost()
	 */
	public void disableDebug(){
		this.debug = false;
	}
	
   /**
	 * Habilita el uso de la clase AccesoDBOracle en lugar de AccesoDB
	 * en el metodo: getMailHost()
	 */
	public void enableUseAccesoDBOracle(){
		this.useAccesoDBOracle = true;
	}
	/**
	 * Deshabilita el uso de la clase AccesoDBOracle para que se use AccesoDB
	 * en el metodo: getMailHost()
	 */
	public void disableUseAccesoDBOracle(){
		this.useAccesoDBOracle = false;
	}
	
	/**
	 * Permite enviar un correo sencillo de solo texto
	 * La direcci�n IP del servidor de SMTP se obtiene de la BD
	 * en com_param_gral.cg_ip_smtp
	 * @param to Cuenta (o cuentas separadas por coma) a la que se envia el correo
	 * @param from Cuenta que aparece como la que envia el correo
	 * @param subject Titulo del mensaje
	 * @param body Texto del mensaje
	 */
	public static void enviarMensajeTexto(
			String to, String from,
			String subject, String body) {
			enviarMensajeTexto(to, from, subject, body, "");		
			
	}
	/**
	 * Permite enviar un correo sencillo de solo texto
	 * La direcci�n IP del servidor de SMTP se obtiene de la BD
	 * en com_param_gral.cg_ip_smtp
	 * @param to Cuenta (o cuentas separadas por coma) a la que se envia el correo
	 * @param from Cuenta que aparece como la que envia el correo
	 * @param subject Titulo del mensaje
	 * @param body Texto del mensaje
	 * @param cc Cuenta a la que se le enviara una copia del correo
	 */
	public static void enviarMensajeTexto(
			String to, String from,
			String subject, String body, String cc) {
		
		AccesoDB con = new AccesoDB();
		
		try {
			con.conexionDB();
			String smtpServer = null;
	
			String strSQL = 
					" SELECT cg_ip_smtp " +
					" FROM com_param_gral " +
					" WHERE ic_param_gral = 1 ";
			ResultSet rs = con.queryDB(strSQL);
			if (rs.next()) {
				smtpServer = rs.getString("cg_ip_smtp");
			}
			rs.close();
			con.cierraStatement();
			
			Properties props = new Properties();
			props.put("mail.smtp.host", smtpServer);
			
			Session session = Session.getDefaultInstance(props, null);

			// -- Crea un nuevo mensaje --
			Message msg = new MimeMessage(session);

			// -- Establece los campos FROM y TO --
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to, false));
			
			// -- Tambi�n se pueden incluir CC recipients --
			if (cc != null && !cc.equals("")){
					msg.setRecipients(Message.RecipientType.CC
					 ,InternetAddress.parse(cc, false));
			}
			
			// -- Establece el titulo y el texto del mensaje --
			msg.setSubject(subject);
			msg.setText(body);
			
			// -- Establece otros headers --
			//msg.setHeader("X-Mailer", "LOTONtechEmail");
			msg.setSentDate(new java.util.Date());
			
			// -- Envia el mensaje --
			Transport.send(msg);
			
			System.out.println("Correo enviado.");

		} catch (MessagingException mex) {
			mex.printStackTrace();
			Exception ex = mex;
			do {
				if (ex instanceof SendFailedException) {
					SendFailedException sfex = (SendFailedException)ex;
					Address[] invalid = sfex.getInvalidAddresses();
					if (invalid != null) {
						System.out.println("    ** Direcciones invalidas");
						if (invalid != null) {
							for (int i = 0; i < invalid.length; i++) 
									System.out.println("         " + invalid[i]);
						}
					}
					Address[] validUnsent = sfex.getValidUnsentAddresses();
					if (validUnsent != null) {
						System.out.println("    ** Direcciones validas pero sin envio");
						if (validUnsent != null) {
							for (int i = 0; i < validUnsent.length; i++) 
									System.out.println("         "+validUnsent[i]);
						}
					}

					Address[] validSent = sfex.getValidSentAddresses();
					if (validSent != null) {
						System.out.println("    ** Direcciones validas con envio");
						if (validSent != null) {
							for (int i = 0; i < validSent.length; i++) 
									System.out.println("         "+validSent[i]);
						}
					}
				}
				System.out.println();
				if (ex instanceof MessagingException)
					ex = ((MessagingException)ex).getNextException();
				else
					ex = null;
			} while (ex != null);
			System.out.println("Correo::enviarMensajeTexto():::(S)");
		} catch (Exception ex) {
			System.out.println("Error. Correo::enviarMensajeTexto()");
			ex.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * 
	 * @throws java.lang.Exception
	 * @param BCC
	 * @param listaDeArchivosAdjuntos
	 * @param listaDeImagenesAdjuntas
	 * @param contenidoMensaje
	 * @param tituloMensaje
	 * @param copia_destinatarios
	 * @param destinatarios
	 * @param remitente
	 */
	public void enviaCorreoConDatosAdjuntosBCC
					(
						String 		remitente, 					// From:
						String 		destinatarios, 			// To:
						String 		copia_destinatarios,   	// CC:
						String 		tituloMensaje, 
						String 		contenidoMensaje, 
						ArrayList 	listaDeImagenesAdjuntas, 
						ArrayList 	listaDeArchivosAdjuntos,
						String BCC //El "CCO" (copia oculta) los destinatarios.
					) throws Exception {				
			
					System.out.println("BCC" +BCC);
					System.out.println("destinatarios" +destinatarios);
					try {
					
						String mailHost = getMailHost();
		
						Properties props = new Properties();
						props.put("mail.smtp.host", mailHost);
		
						Session 		mailSession = null;

					  
						mailSession = Session.getDefaultInstance(props, null);
						mailSession.setDebug(this.debug);// Debug info

						MimeMessage message = new MimeMessage(mailSession);
						message.setSubject(tituloMensaje);
						message.setFrom(new InternetAddress(remitente));					
						message.addRecipients(Message.RecipientType.BCC, 
								InternetAddress.parse(destinatarios, false) );	
						
						if(copia_destinatarios != null && !copia_destinatarios.equals("")){
							message.addRecipients(Message.RecipientType.CC, 
									InternetAddress.parse(copia_destinatarios, false) );							
						}
						MimeMultipart multipart = new MimeMultipart("related");

						// Contenido
						BodyPart messageBodyPart = new MimeBodyPart();   
						
					    if( this.codificacion !=null ){  
							messageBodyPart.setContent(contenidoMensaje, "text/html;"+this.codificacion);
						}else{
							messageBodyPart.setContent(contenidoMensaje, "text/html"); // contenidoMensaje = "<H1>Hello</H1><img src=\"cid:imagen_adjunta_id\">";
						}

						multipart.addBodyPart(messageBodyPart);
       
						// Imagenes embebidas en el codigo HTML
						if(listaDeImagenesAdjuntas != null){
							for(int indice=0;indice<listaDeImagenesAdjuntas.size();indice++){
								HashMap 	datos  = (HashMap)listaDeImagenesAdjuntas.get(indice);
								messageBodyPart = new MimeBodyPart();
								DataSource fds  = new FileDataSource((String)datos.get("FILE_FULL_PATH")); // "/home/images/images.jpg";
								messageBodyPart.setDataHandler(new DataHandler(fds));
								messageBodyPart.setHeader("Content-ID","<"+datos.get("FILE_ID")+">"); // imagen_adjunta_id
								multipart.addBodyPart(messageBodyPart);// add it
							}
						}
		  
						// Archivos adjuntos
						if(listaDeArchivosAdjuntos != null){
							for(int indice=0;indice<listaDeArchivosAdjuntos.size();indice++){
								HashMap 	datos  = (HashMap)listaDeArchivosAdjuntos.get(indice);
								messageBodyPart = new MimeBodyPart();
								DataSource fds  = new FileDataSource((String)datos.get("FILE_FULL_PATH")); // "/home/images/images.jpg";
								messageBodyPart.setDataHandler(new DataHandler(fds));
								messageBodyPart.setFileName(fds.getName()); // NOMBRE DEL ARCHIVO ADJUNTO
								multipart.addBodyPart(messageBodyPart);// add it
							}
						}
						
						// Poner todo junto
						message.setContent(multipart);

						// enviar el mensaje
						//Transport.send(message, message.getRecipients(Message.RecipientType.TO));
						Transport.send(message, message.getRecipients(Message.RecipientType.BCC));
						if(copia_destinatarios != null && !copia_destinatarios.equals("")){
							Transport.send(message, message.getRecipients(Message.RecipientType.CC));
						}
						
					}catch(Exception ex){
						System.out.println("Correo::enviaCorreoConDatosAdjuntos(Exception)");
						System.out.println("");
						System.out.println("El siguiente correo, NO SERA ENVIADO; datos del correo:");
						System.out.println("   De:     " + remitente);
						System.out.println("   Para:   "	+ destinatarios);
						System.out.println("   Copia:  " + copia_destinatarios);
						System.out.println("   Titulo: "	+ tituloMensaje);
						System.out.println("--------------------------------------------------------------------------");
						System.out.println(contenidoMensaje);
						System.out.println("--------------------------------------------------------------------------");
						System.out.println("   Imagenes embebidas en el contenido: ");
						if(listaDeImagenesAdjuntas == null || listaDeImagenesAdjuntas.size() == 0)
							System.out.println("No hay imagenes embebidas en el contenido");
						if(listaDeImagenesAdjuntas != null){
							for(int indice=0;indice<listaDeImagenesAdjuntas.size();indice++){
								HashMap 	datos  = (HashMap)listaDeImagenesAdjuntas.get(indice);
								System.out.println("\tContent-ID," +"<"+datos.get("FILE_ID")+">, Full file path:"+datos.get("FILE_FULL_PATH")); 
							}
						}
						System.out.println("   Datos adjuntos: ");
						if(listaDeArchivosAdjuntos == null || listaDeArchivosAdjuntos.size() == 0)
							System.out.println("No hay datos adjuntos");
						if(listaDeArchivosAdjuntos != null ){
							for(int indice=0;indice<listaDeArchivosAdjuntos.size();indice++){
								HashMap 	datos  = (HashMap)listaDeArchivosAdjuntos.get(indice);
								System.out.println(datos.get("FILE_ID")+", Full file path:"+datos.get("FILE_FULL_PATH")); 
							}
						}
						System.out.println("");
			
						ex.printStackTrace();	
			
						throw ex;
					}
        
				}
				

	public void enviaCorreoConDatosAdjuntos
						(
							String      remitente,              // From:
							String      destinatarios,          // To:
							String      copia_destinatarios,    // CC:
							String      tituloMensaje, 
							String      contenidoMensaje, 
							ArrayList   listaDeImagenesAdjuntas, 
							ArrayList   listaDeArchivosAdjuntos
						) throws Exception {          
				
					
						try {
						
							String mailHost = getMailHost();
			
							Properties props = new Properties();
							props.put("mail.smtp.host", mailHost);
			
							Session     mailSession = null;

						  
							mailSession = Session.getDefaultInstance(props, null);
							mailSession.setDebug(this.debug);// Debug info

							MimeMessage message = new MimeMessage(mailSession);
							message.setSubject(tituloMensaje, "UTF-8");  
							message.setFrom(new InternetAddress(remitente));
							message.addRecipients(Message.RecipientType.TO, destinatarios);
							if(copia_destinatarios != null && !copia_destinatarios.equals("")){
								message.addRecipients(Message.RecipientType.CC, 
										InternetAddress.parse(copia_destinatarios, false) );
							}
							MimeMultipart multipart = new MimeMultipart("related");

							// Contenido
							BodyPart messageBodyPart = new MimeBodyPart();  
							
							System.out.println(" this.codificacion ====="+this.codificacion );     
							
							if( this.codificacion !=null ){  
								messageBodyPart.setContent(contenidoMensaje, "text/html;"+this.codificacion);
							}else {
								messageBodyPart.setContent(contenidoMensaje, "text/html"); // contenidoMensaje = "<H1>Hello</H1><img src=\"cid:imagen_adjunta_id\">"; 
							}
							multipart.addBodyPart(messageBodyPart);
			 
							// Imagenes embebidas en el codigo HTML
							if(listaDeImagenesAdjuntas != null){
								for(int indice=0;indice<listaDeImagenesAdjuntas.size();indice++){
									HashMap  datos  = (HashMap)listaDeImagenesAdjuntas.get(indice);
									messageBodyPart = new MimeBodyPart();
									DataSource fds  = new FileDataSource((String)datos.get("FILE_FULL_PATH")); // "/home/images/images.jpg";
									messageBodyPart.setDataHandler(new DataHandler(fds));
									messageBodyPart.setHeader("Content-ID","<"+datos.get("FILE_ID")+">"); // imagen_adjunta_id
									multipart.addBodyPart(messageBodyPart);// add it
								}
							}
			  
							// Archivos adjuntos
							if(listaDeArchivosAdjuntos != null){
								for(int indice=0;indice<listaDeArchivosAdjuntos.size();indice++){
									HashMap  datos  = (HashMap)listaDeArchivosAdjuntos.get(indice);
									messageBodyPart = new MimeBodyPart();
									DataSource fds  = new FileDataSource((String)datos.get("FILE_FULL_PATH")); // "/home/images/images.jpg";
									messageBodyPart.setDataHandler(new DataHandler(fds));
									String fileName = (String) datos.get("FILE_NAME");
									if( fileName != null && !fileName.trim().equals("")){
										messageBodyPart.setFileName(fileName); // NOMBRE DEL ARCHIVO ADJUNTO
									}else{
										messageBodyPart.setFileName(fds.getName()); // NOMBRE DEL ARCHIVO ADJUNTO
									}
									multipart.addBodyPart(messageBodyPart);// add it
								}
							}
							
							// Poner todo junto
							message.setContent(multipart);

							// enviar el mensaje
							Transport.send(message, message.getRecipients(Message.RecipientType.TO));
							if(copia_destinatarios != null && !copia_destinatarios.equals("")){
								Transport.send(message, message.getRecipients(Message.RecipientType.CC));
							}
							
						}catch(Exception ex){
							System.out.println("Correo::enviaCorreoConDatosAdjuntos(Exception)");
							System.out.println("");
							System.out.println("El siguiente correo, NO SERA ENVIADO; datos del correo:");
							System.out.println("   De:     " + remitente);
							System.out.println("   Para:   " + destinatarios);
							System.out.println("   Copia:  " + copia_destinatarios);
							System.out.println("   Titulo: " + tituloMensaje);
							System.out.println("--------------------------------------------------------------------------");
							System.out.println(contenidoMensaje);
							System.out.println("--------------------------------------------------------------------------");
							System.out.println("   Imagenes embebidas en el contenido: ");
							if(listaDeImagenesAdjuntas == null || listaDeImagenesAdjuntas.size() == 0)
								System.out.println("No hay imagenes embebidas en el contenido");
							if(listaDeImagenesAdjuntas != null){
								for(int indice=0;indice<listaDeImagenesAdjuntas.size();indice++){
									HashMap  datos  = (HashMap)listaDeImagenesAdjuntas.get(indice);
									System.out.println("\tContent-ID," +"<"+datos.get("FILE_ID")+">, Full file path:"+datos.get("FILE_FULL_PATH")); 
								}
							}
							System.out.println("   Datos adjuntos: ");
							if(listaDeArchivosAdjuntos == null || listaDeArchivosAdjuntos.size() == 0)
								System.out.println("No hay datos adjuntos");
							if(listaDeArchivosAdjuntos != null ){
								for(int indice=0;indice<listaDeArchivosAdjuntos.size();indice++){
									HashMap  datos  = (HashMap)listaDeArchivosAdjuntos.get(indice);
									System.out.println(datos.get("FILE_ID")+", Full file path:"+datos.get("FILE_FULL_PATH")); 
								}
							}
							System.out.println("");
				
							ex.printStackTrace();   
				
							throw ex;
						}
			  
					}
				
		private String getMailHost() throws Exception{
			
			AccesoDB 			con1 				= null;
			AccesoDBOracle 	con2 				= null;
			String 				qrySentencia	= null;
			List 					lVarBind			= new ArrayList();
			Registros			registros		= null;
			String 				host 				= null;
							
			try {
				
				if(useAccesoDBOracle){
					con2 = new AccesoDBOracle();
				}else{
					con1 = new AccesoDB();
				}
				
				if(this.debug){
					if(useAccesoDBOracle){
						con2.enableMessages();
					}else{
						con1.enableMessages();	
					}
				}else{
					if(useAccesoDBOracle){
						con2.disableMessages();
					}else{
						con1.disableMessages();
					}
				}
				
				if(useAccesoDBOracle){
					con2.conexionDB();
				}else{
					con1.conexionDB();
				}
				
				qrySentencia = 
					"SELECT 						" + 
					"	CG_IP_SMTP AS HOST	" +
					"FROM 						" +
					"	COM_PARAM_GRAL 		" +
					"WHERE 						" +
					"	IC_PARAM_GRAL = ? 	";
				
				lVarBind.add(new Integer(1));
				
				if(useAccesoDBOracle){
					registros = con2.consultarDB(qrySentencia, lVarBind, false);
				}else{
					registros = con1.consultarDB(qrySentencia, lVarBind, false);
				}
				
				if(registros != null && registros.next() ){ 
					host = registros.getString("HOST");
				}	
			} catch(Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
				if(useAccesoDBOracle){
					if(con2.hayConexionAbierta())
						con2.cierraConexionDB();
				}else{	
					if(con1.hayConexionAbierta())
						con1.cierraConexionDB();
				}
			}
			return host;
			
		}	
		
		
	/**
	 * M�todo para enviar correo en codigo HTML 
	 * @param from Remitente
	 * @param to Destinatario
	 * @param subject Titulo del mensaje
	 * @param contenidoMensaje Cadena de texto correspondiente al c�digo HTML
	 */
	public void enviarTextoHTML(String from, String to, String subject, String contenidoMensaje) {
			
		/*AccesoDB con = new AccesoDB();
		String host = "";
		try{
			host = getMailHost();
						
			// Get system properties
			Properties props = System.getProperties();
			// Setup mail server
			props.put("mail.smtp.host", host);
			// Get session
			Session session = Session.getInstance(props, null);
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			//message.addRecipient(Message.RecipientType.TO, 
			//message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to, false));
			message.addRecipients(Message.RecipientType.TO, to);
			message.setSubject(subject, "UTF-8");
			
			// Create a Multipart
			MimeMultipart multipart = new MimeMultipart("related");

			// Contenido
			BodyPart messageBodyPart = new MimeBodyPart();   
			messageBodyPart.setContent(contenidoMensaje, "text/html"); // contenidoMensaje = "<H1>Hello</H1><img src=\"cid:imagen_adjunta_id\">";

			multipart.addBodyPart(messageBodyPart);
			
			// Poner todo junto
			message.setContent(multipart);
			
			Transport.send(message);
			*/
			
			enviarTextoHTML(from, to, subject, contenidoMensaje,"");
	} 
	
	
	/**
	 * M�todo para enviar correo en codigo HTML 
	 * @param from Remitente
	 * @param to Destinatario
	 * @param cc Destinatario Copia
	 * @param subject Titulo del mensaje
	 * @param contenidoMensaje Cadena de texto correspondiente al c�digo HTML
	 */
	public void enviarTextoHTML(
			String from, String to, String subject,
			String contenidoMensaje, String cc) 
			throws AppException {
		System.out.println("enviarHTML(E)");
		
		AccesoDB con = new AccesoDB();
		String host = "";
		try{
			host = getMailHost();
						
			// Get system properties
			Properties props = System.getProperties();
			// Setup mail server
			props.put("mail.smtp.host", host);
			// Get session
			Session session = Session.getInstance(props, null);
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipients(Message.RecipientType.TO, to);
			if(cc != null && !cc.equals("")){
				message.addRecipients(Message.RecipientType.CC, 
						InternetAddress.parse(cc, false) );							
			}
			message.setSubject(subject, "UTF-8");
			
			// Create a Multipart
			MimeMultipart multipart = new MimeMultipart("related");

			// Contenido
			BodyPart messageBodyPart = new MimeBodyPart();
			
		    if( this.codificacion !=null ){  
				messageBodyPart.setContent(contenidoMensaje, "text/html;"+this.codificacion); // contenidoMensaje = "<H1>Hello</H1><img src=\"cid:imagen_adjunta_id\">";
			}else{
			    messageBodyPart.setContent(contenidoMensaje, "text/html"); // contenidoMensaje = "<H1>Hello</H1><img src=\"cid:imagen_adjunta_id\">";
			}

			multipart.addBodyPart(messageBodyPart);
			
			// Poner todo junto
			message.setContent(multipart);
			
			//Transport.send(message);
			Transport.send(message, message.getRecipients(Message.RecipientType.TO));
			if(cc != null && !cc.equals("")){
				Transport.send(message, message.getRecipients(Message.RecipientType.CC));
			}
		}catch (Exception ex) {
			throw new AppException("Error al enviar correo", ex);
		} finally {
			System.out.println("enviarHTML(S)");
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}		    
	}

	public String getCodificacion() {
		return codificacion;
	} 

	public void setCodificacion(String codificacion) {
		this.codificacion = codificacion;
	}
	
		
}

