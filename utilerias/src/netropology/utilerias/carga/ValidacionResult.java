package netropology.utilerias.carga;


public class ValidacionResult  {

	private StringBuffer		errores 						= new StringBuffer(102400);
	private StringBuffer		correctos 					= new StringBuffer(1024);
	private StringBuffer		cifras						= new StringBuffer(512);
	private int 				num_reg_ok					= 0;
	private int					num_reg_err					= 0;
	private int					num_reg_err_bis			= 0;
	private boolean			limiteErroresAlcanzado 	= false;
	private int					num_err_cifras				= 0;
	private int					numeroMaximoErrores 		= 500;

	public void appendErrores(String valor){

		if( this.limiteErroresAlcanzado ){
			return;
		} else if(        num_reg_err_bis <=  numeroMaximoErrores ){
			errores.append(valor);
		} 

	}
	
	public void appendCorrectos(String valor){
		correctos.append(valor);
	}
	
	public void appendCifras(String valor){
		if(num_err_cifras < 200){
			cifras.append(valor);
		}else if(num_err_cifras==200){
			cifras.append("El archivo contiene demasiados errores en la Cifras de Control...\n");
		}
		num_err_cifras++;
	}
	
	public void incNumMensajesError(){
		this.num_reg_err_bis++;
		if( this.num_reg_err_bis == numeroMaximoErrores ){
			errores.append("Favor de revisar el archivo, contiene demasiados errores.\n");
			this.limiteErroresAlcanzado = true;
		}
	}
	
	public void incNumRegOk(){
		this.num_reg_ok ++;
	}
	
	public void incNumRegErr(){
		this.num_reg_err ++;
	}	

	public String getCorrectos(){
		return this.correctos.toString();
	}

	public String getCifras(){
		return this.cifras.toString();
	}
	
	public String getErrores(){
		return this.errores.toString();
	}

	public int	getNumRegOk(){
		return this.num_reg_ok;
	}
	
	public int	getNumRegErr(){
		return this.num_reg_err;
	}

	public int getNumeroMaximoErrores() {
		return numeroMaximoErrores;
	}

	public void setNumeroMaximoErrores(int numeroMaximoErrores) {
		this.numeroMaximoErrores = numeroMaximoErrores;
	}
	
	public void resetLimiteErroresAlcanzado(){
		this.limiteErroresAlcanzado = false;
	}

}