package netropology.utilerias.carga;

import java.io.BufferedReader;

public interface RenglonValidator  {

	void 					validaRegistro( int processedRegisters, BufferedReader br) throws Exception;
	void 					validaCifrasControl();
	String 				getLogin();
	void					setLogin(String login);
	void 					setProcessID(String processID); 
	String 				getProcessID(); 	
	void 					setValidacionResult(ValidacionResult resultadosGar);
	ValidacionResult 	getValidacionResult();
	boolean 				getHayError();
	void 					setHayError(boolean hayError);
	int 					getRecordsNumber();
	void 					setRecordsNumber(int recordsNumber);
	void           	leeEncabezado(BufferedReader br) throws Exception;
	boolean				inicializar(String[] msg) throws Exception;
	void 					finalizar();
	BufferedReader		createBufferedReaderArchivoValidacion() throws Exception;
	
}