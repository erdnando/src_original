package netropology.utilerias.carga;

import java.io.BufferedReader;
import java.io.Serializable;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase que se encarga validar archivo de texto
 */

public class ValidaCargaThread implements Runnable, Serializable {
	
	private static final long  serialVersionUID 	= 20141209162239L;
	
	//Variable para enviar mensajes al log.
	private static   Log 		log 					= ServiceLocator.getInstance().getLog(ValidaCargaThread.class);
	
   private volatile int 		recordsNumber;
   private volatile float		percent;
   private volatile int 	 	processedRegisters;
   
   private volatile boolean 	running;
   private volatile boolean 	error;
   private volatile boolean 	requestStop;
   
   private boolean 				started;
	private String					processID;
	private int						errorRecordsCount; 
	private StringBuffer			mensajes;
	 
	private ValidacionResult	resultados;
	private RenglonValidator	fileValidator;

   public ValidaCargaThread() {
   	
     recordsNumber 			= 0;
     percent 		   		= 0;
     started 			   	= false;
     running 			   	= false;
	  processID 	   		= null;
	  processedRegisters 	= 0;
	  error						= false;
	  errorRecordsCount		= 0;
     mensajes   				= new StringBuffer(256); // Crear un buffer donde se guardar�n los mensajes de error
		  
     resultados 				= null;
     fileValidator 			= null;
	
     requestStop 				= false;
			
   }
	 
   protected void validateRegisters(BufferedReader br) {
		
	   try {

			fileValidator.validaRegistro(this.processedRegisters+1,br);					
			processedRegisters++;
			percent = (processedRegisters/(float)recordsNumber)*100;
			
			if( fileValidator.getHayError() ){
				errorRecordsCount++;
			}
			
			if( errorRecordsCount >= 1000 ){
				// Detener el thread
				setRunning(false);
				recordsNumber = processedRegisters;
				percent				= 100F;
			}

		} catch(Exception e) {
			
			setRunning(false);
			recordsNumber 	= processedRegisters ;
			percent			= 100F;
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Ocurri� un error inesperado al validar registro "+ ( this.processedRegisters -1 )+": " + e.getMessage() + "; se aborta el proceso.");
			
			log.error("validateRegisters(Exception): Ocurri� un error inesperado al validar registro "+ ( this.processedRegisters -1 )+": " + e.getMessage() + "; se aborta el proceso.");
			log.error("validateRegisters.recordsNumber      = <" + recordsNumber      + ">");
			log.error("validateRegisters.percent            = <" + percent            + ">");
			log.error("validateRegisters.started            = <" + started            + ">");
			log.error("validateRegisters.running            = <" + running            + ">");
			log.error("validateRegisters.processID          = <" + processID          + ">");
			log.error("validateRegisters.processedRegisters = <" + processedRegisters + ">");
			log.error("validateRegisters.error              = <" + error              + ">");
			log.error("validateRegisters.errorRecordsCount  = <" + errorRecordsCount  + ">");
			log.error("validateRegisters.resultados         = <" + resultados         + ">");
			log.error("validateRegisters.fileValidator      = <" + fileValidator      + ">");
			e.printStackTrace();

			// terminar el proceso si falla la funcion de validacion para algun registro
			
		}
		
   }

   public synchronized int getRecordsNumber() {
       return recordsNumber;
   }
   
	public synchronized void setRecordsNumber(int recordsNumber) {
       this.recordsNumber = recordsNumber;
   }
	
   public synchronized float getPercent() {
       return percent;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		//return((processedRegisters == 0 && recordsNumber == 0)?false:(processedRegisters == recordsNumber));
		return (processedRegisters < recordsNumber?false:true);
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
   public synchronized String getProcessID() {
       return processID;
   }
   
	public synchronized void setProcessID(String processID) {
       this.processID = processID;
   }

   public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }

   public synchronized boolean hasError() {
       return error;
   }
   
	public synchronized void setError(boolean error) {
       this.error = error;
   }
		
   // errorRecordsCount

	public synchronized ValidacionResult getResultados() {
		return resultados;
	}

	public void setResultados(ValidacionResult resultados) {
		this.resultados = resultados;
	}

	public synchronized RenglonValidator getFileValidator() {
		return fileValidator;
	}

	public void setFileValidator(RenglonValidator fileValidator) {
		this.fileValidator = fileValidator;
	}

	public boolean inicializar(){
	
		log.info("inicializar(E)");
		boolean exito = true;
		
		// Configurar Propiedades Adicionales del File Validator
   	try {
   		
   		// Asignar processID
   		String calculatedProcessID = this.fileValidator.getLogin() + "-" + System.currentTimeMillis() + "-" + (int)( Math.random() * 1000 );
   		this.setProcessID( calculatedProcessID );
   		// Crear objeto donde se guardar�n los resultados de la validaci�n
   		this.setResultados( new ValidacionResult() );
   		// Definir el n�mero m�ximo de lineas de error a mostrar
			this.resultados.setNumeroMaximoErrores(1000);
			// Asignar processID
			this.fileValidator.setProcessID( calculatedProcessID );
			// Asignar Objeto que guardar� los resultados
			this.fileValidator.setValidacionResult( this.getResultados() );
				
			// Inicializacion adicional de campos
			String[] msg = new String[1];
			if( !this.fileValidator.inicializar(msg) ){
				exito = false;
				this.mensajes.setLength(0);
				this.mensajes.append(msg[0]);
			} else {
				// Obtener numero de registros		
				this.setRecordsNumber(this.fileValidator.getRecordsNumber());
			}
			
   	} catch (Exception e) {
			
   		log.error("inicializar(Exception): Fall� la inicializaci�n de par�metros en el Thread de validaci�n: "+e.getMessage());
			log.error("inicializar.recordsNumber  		      = <" + recordsNumber      + ">");
			log.error("inicializar.percent            		= <" + percent            + ">");
			log.error("inicializar.started            		= <" + started            + ">");
			log.error("inicializar.running            		= <" + running            + ">");
			log.error("inicializar.processID          		= <" + processID          + ">");
			log.error("inicializar.processedRegisters 		= <" + processedRegisters + ">");
			log.error("inicializar.error              		= <" + error              + ">");
			log.error("inicializar.errorRecordsCount  		= <" + errorRecordsCount  + ">");
			
			log.error("inicializar.resultados      		   = <" + resultados         + ">");
			log.error("inicializar.fileValidator      		= <" + fileValidator      + ">");
			e.printStackTrace();
			
			setError(true);
			exito = false;
			this.mensajes.setLength(0);
			this.mensajes.append("Fall� la inicializaci�n de par�metros en el Thread de validaci�n: "+e.getMessage());	
			
		} finally {
			
			log.info("inicializar(S)");
			
		}
			
		return exito;
		
	}
	
   public void run() {

		boolean lbOK 						= true;
		int	  initialRecordsNumber 	= 0;	
		
		// Verificar que no se hayan devuelto datos vacios
		if ( recordsNumber == 0 ){
			recordsNumber  				= 0;
			processedRegisters 			= 0;
			percent							= 100F;
			lbOK 								= false;
		}
		
		initialRecordsNumber 	= recordsNumber;
		
		// Obtener Buffered Reader con el Archivo Cargado
		BufferedReader br 		= null;
		try {
			
			if( lbOK ){
				
				br = this.fileValidator.createBufferedReaderArchivoValidacion();

				// SALTAR ENCABEZADO
				// .......................................................................................
				this.fileValidator.leeEncabezado(br);
				
			} else {
				
				br = null;
				
			}
			
		} catch(Exception e) {
			
			if( br != null) { try { br.close(); br=null; }catch(Exception e2){} }
			lbOK 					= false;
			setRunning(false);
			recordsNumber 		= processedRegisters;
			percent				= 100F;
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Ocurri� un error al abrir archivo para su validaci�n: "+e.getMessage());	
			
			log.error("run: Error en ValidaArchivoThread::run al extraer numero de registro de la base de datos");
			log.error("run.recordsNumber      = <" + recordsNumber      + ">");
			log.error("run.percent            = <" + percent            + ">");
			log.error("run.started            = <" + started            + ">");
			log.error("run.running            = <" + running            + ">");
			log.error("run.processID          = <" + processID          + ">");
			log.error("run.processedRegisters = <" + processedRegisters + ">");
			log.error("run.error              = <" + error              + ">");
			log.error("run.errorRecordsCount  = <" + errorRecordsCount  + ">");
			
			log.error("run.resultados         = <" + resultados         + ">");
			log.error("run.fileValidator      = <" + fileValidator      + ">");
			e.printStackTrace();
			
		}

		// Realizar la Validacion de los datos
      try {
			
			if(lbOK == false){ 
				setRunning(false);
			}else{
				setRunning(true);
			}

			while (isRunning() && !isCompleted()){
				
				// Revisar si se le ha dado la orden al thread de detenerse
			 	if(  this.getRequestStop() ){
					break;
				}
				
				validateRegisters(br);
				
				if( (processedRegisters % 350) == 0){
					log.debug("run: Validating Registers");
					log.debug("run.recordsNumber:	" + recordsNumber);
					log.debug("run.percent:    			" + percent);
					log.debug("run.started:       		" + started);
					log.debug("run.running:       		" + running);
					log.debug("run.completed:     		" + isCompleted());
					log.debug("run.processID: 			   " + processID);
					log.debug("run.processedRegisters:  " + processedRegisters );
				}
				
			}

			log.debug("run: Validating Registers");
			log.debug("run.recordsNumber:	" + recordsNumber);
			log.debug("run.percent:    			" + percent);
			log.debug("run.started:       		" + started);
			log.debug("run.running:       		" + running);
			log.debug("run.completed:     		" + isCompleted());
			log.debug("run.processID: 			   " + processID);
			log.debug("run.processedRegisters:  " + processedRegisters );
			
      } finally {
      	
      	if( br != null) { try { br.close(); }catch(Exception e){} }
			
      }
      
      try {
      	
      	if( !this.getRequestStop() ){
				
				// VALIDAR CIFRAS DE CONTROL, SOLO EN CASO NO SE HAYAN PRESENTADO ERRORES EN EL THREAD Y SE HAYAN
				// VALIDADO EL 100% DE REGISTROS QUE CONTENIA EL ARCHIVO
				// .......................................................................................
				if( !hasError() && initialRecordsNumber == recordsNumber ){
					this.fileValidator.validaCifrasControl();
				}
				
				// FINALIZAR OPERACION
				// .......................................................................................
				this.fileValidator.finalizar();
				
			}

      } finally {

			setRunning(false);
			
			log.debug("run: Validating Registers");
			log.debug("run.recordsNumber:	" + recordsNumber);
			log.debug("run.percent:    			" + percent);
			log.debug("run.started:       		" + started);
			log.debug("run.running:       		" + running);
			log.debug("run.completed:     		" + isCompleted());
			log.debug("run.processID: 			   " + processID);
			log.debug("run.processedRegisters:  " + processedRegisters );

      }

   }
	
   public String getMensajes(){
		return this.mensajes.toString();
	}
	
	public synchronized void setRequestStop(boolean requestStop){
		if( !this.running && !this.isCompleted() ){
			 setRunning(false);
			 // setCompleted(true);
			 recordsNumber = processedRegisters;
			 percent			= 100F;
		}
		this.requestStop = requestStop;
	}
	
	public synchronized boolean getRequestStop(){
		return this.requestStop;
	}

}