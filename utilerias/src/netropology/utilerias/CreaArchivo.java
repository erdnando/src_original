package netropology.utilerias;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.StringTokenizer;

public class CreaArchivo {
	private String content;
	public String ruta;
	public String nombre;

	public CreaArchivo() {}

	public boolean make(String Contenido) {
		this.content=Contenido;
		this.nombre=nombreFecha()+".log";
		this.ruta="/tmp/"+this.nombre;
//		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta)) return true; else return false;
	}

	/**
	 * 
	 * @throws NafinException
	 * @param archivo2
	 * @param archivo1
	 */
	public boolean make(String Contenido, String Ruta) {
		this.content=Contenido;
		this.nombre=nombreAleatorio()+".csv";
		this.ruta=Ruta+this.nombre;
//		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta)) return true; else return false;
	}

	/**
	 * Genera un archivo con nombre aleatorio en el directorio especificado
	 * a partir del InputStream proporcionado
	 * @return true si la creaci�n fue exitosa o false de lo contrario
	 * @param extension Extensi�n del archivo. por ejemplo .gif
	 * @param ruta Ruta donde se alamacenara el archivo
	 * @param input Stream de entrada
	 */
	public boolean make(InputStream input, String ruta, String extension) {
		this.nombre=nombreAleatorio() + extension;
		this.ruta = ruta + this.nombre;
		boolean exito = (escribeArchivo(input, this.ruta))?true:false;
		return exito;
	}

	/**
	 * Genera un archivo con ruta y nombre especificado
	 * a partir del InputStream proporcionado
	 * @return true si la creaci�n fue exitosa o false de lo contrario
	 * @param extension Extensi�n del archivo. por ejemplo .gif
	 * @param ruta Ruta donde se alamacenara el archivo
	 * @param input Stream de entrada
	 */
	public boolean make(InputStream input, String nombreArchivo) {
		boolean exito = (escribeArchivo(input, nombreArchivo))?true:false;
		return exito;
	}

	public boolean make(String Contenido, StringBuffer Ruta) {
		this.content=Contenido;
		this.nombre=nombreFecha()+".csv";
 		this.ruta=Ruta.toString()+this.nombre;
//		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta)) return true; else return false;
	}

	public boolean make(String Contenido, String Ruta, String Extension) {
		this.content=Contenido;
		this.nombre=nombreAleatorio()+Extension;
		this.ruta=Ruta+this.nombre;
//		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta)) return true; else return false;
	}

	public boolean make(String Contenido, String Ruta, String Extension, StringBuffer sEOL) {
		this.content=Contenido;
		this.nombre=nombreAleatorio()+Extension;
		this.ruta=Ruta+this.nombre;
		String ruta2 = Ruta+nombreAleatorio()+Extension;
//		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta, ruta2, sEOL)) return true; else return false;
	}

	/**
	 * Genera un archivo con el contenido y nombre especificado.
	 * @return 
	 * @param Extension
	 * @param Nombre
	 * @param Ruta
	 * @param Contenido
	 */
	public boolean make(String Contenido, String Ruta, String Nombre, String Extension) {
		this.content=Contenido;
		this.nombre=Nombre+Extension;
		this.ruta=Ruta+this.nombre;
		System.out.println(this.ruta);
		if(escribeArchivo(this.ruta))
			return true;
		else
			return false;
	}

	private String nombreFecha() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddhhmmss");
		return sdf.format(new Date());
	}

	public String nombreArchivo() {
		this.nombre=nombreAleatorio();
		return this.nombre;
	}

	/**
	 * Escribe un archivo a partir del Stream especificado
	 * @param is Input Stream del archivo origen
	 * @param archivoDestino Ruta absoluta y nombre del archivo
	 * 
	 */
	private boolean escribeArchivo(InputStream is, String archivoDestino) {
		boolean exito = true;
		try{
			OutputStream os = new FileOutputStream(archivoDestino);
			// Transfer bytes from in to out
			byte[] buf = new byte[4098];
			int len;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
			}
			is.close();
			os.close();
			exito= true;
		}catch(Exception e){
			exito = false;
			String strErr = e.toString();
			System.out.println("strErr"+strErr);
		}
		return exito;
	}//

	private boolean escribeArchivo(String path) {
	String linea="";
	try {
		DataOutputStream dos=new DataOutputStream(new FileOutputStream(new File(path)));
		StringTokenizer st=new StringTokenizer(this.content,"\n");
		if(st.countTokens() >=0) {
			while(st.hasMoreTokens()) {
				linea=st.nextToken().trim();
				dos.writeBytes(linea +"\r\n");
				//System.out.println(linea);
			}
		} else {
			dos.writeBytes(this.content);
		}
		dos.close();
		return true;
	} //try
	catch(IOException ioe) {
		System.out.println("Error de IO: "+ioe); return false;}
	} //escribeArchivo

	private boolean escribeArchivo(String path, String path2, StringBuffer sEOL) {
	String linea="";
	try {
		DataOutputStream dos=new DataOutputStream(new FileOutputStream(new File(path2)));
		StringTokenizer st=new StringTokenizer(this.content,"\n");
		System.out.println("TOKENS:" +st.countTokens());
		if(st.countTokens() >=0) {
			while(st.hasMoreTokens()) {
				linea=st.nextToken().trim();
				dos.writeBytes(linea +sEOL.toString());
				//System.out.println(linea);
			}
		} else {
			dos.writeBytes(this.content);
		}
		dos.close();

		FileWriter fileWriter = new FileWriter(path);
		BufferedWriter fwr = new BufferedWriter( fileWriter );
		FileReader frdr = new FileReader(path2);
		BufferedReader buff = new BufferedReader( frdr );
		int bt = buff.read();
		char c = (char)bt;
		while(bt > -1)
		{
			//System.out.print("Char " + c + " = ");
			//System.out.println((int)c);
			if (bt != 13 )
				fwr.write(c);
			bt = buff.read();
			c = (char)bt;
		}
		//fwr.write(c);
		//if( c != '\n' ) fwr.write('\n');
		fwr.close();
		frdr.close();

		return true;
	} catch(IOException ioe) {
		System.out.println("Error de IO: "+ioe); return false;}
	} //escribeArchivo

	private String nombreAleatorio() {
	String x[]={"9","a","Z","8","b","Y","7","c","X","6","d","W","5","e","V","4","f","U","3","g","T","2","h","S","1","i","R","0","j","Q","9","k","P","8","l","O","7","m","N","6","n","M","5","o","L","4","p","K","3","q","J","2","r","I","1","s","H","9","t","G","8","u","F","7","v","E","6","w","D","5","x","C","4","y","B","3","z","A","2","a","Z","1","b","Y","0","c","X","9","d","W","8","e","V","7","f","U","6","g","T","5"};
	StringBuffer sb=new StringBuffer();
	for(int a=1; a<=8; a++) {
		int i=(int)((Math.random()*100)+1);
		sb.append(x[i-1]);
	}
			 	return sb.toString();
	}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public String getRuta()
	{
		return ruta;
	}

	public void setRuta(String ruta)
	{
		this.ruta = ruta;
	}



/*	private String checaExtension(String laExten) {
		String punto=laExten.substring(0,1);
		System.out.println(punto);
		String ext=(punto.equals("."))?punto:"."+laExten;
		return ext;
	}*/

} //class
