package netropology.utilerias;


public class Acuse3 extends Acuse implements java.io.Serializable {

	public Acuse3(int tipoAcuse, String prodNafin) {
		super(tipoAcuse, prodNafin);
	}

	public String toString() {
		return super.getAcuse();
	}

	/**
	 * Establece el monto de MN
	 * @param strMontoMN Monto MN
	 */
	public void setMontoMN(String strMontoMN){
		this.montoMN = strMontoMN;
	}

	/**
	 * Obtiene el monto de MN
	 * @return Monto MN
	 */
	public String getMontoMN(){
		return this.montoMN;
	}

	/**
	 * Establece el monto de DL
	 * @param strMontoDL Monto DL
	 */
	public void setMontoDL(String strMontoDL){
		this.montoDL = strMontoDL;
	}

	/**
	 * Obtiene el monto de DL
	 * @return Monto DL
	 */
	public String getMontoDL(){
		return this.montoDL;
	}


	/**
	 * Establece el monto de Interes MN
	 * @param strMontoMN Monto Interes MN
	 */
	public void setMontoInteresMN(String strMontoInteresMN){
		this.montoInteresMN = strMontoInteresMN;
	}

	/**
	 * Obtiene el monto de Interes MN
	 * @return Monto Interes MN
	 */
	public String getMontoInteresMN(){
		return this.montoInteresMN;
	}

	/**
	 * Establece el monto de Interes DL
	 * @param strMontoDL Monto Interes DL
	 */
	public void setMontoInteresDL(String strMontoInteresDL){
		this.montoInteresDL = strMontoInteresDL;
	}

	/**
	 * Obtiene el monto de Interes DL
	 * @return Monto Interes DL
	 */
	public String getMontoInteresDL(){
		return this.montoInteresDL;
	}


	/**
	 * Establece el monto de Descuento MN
	 * @param strMontoMN Monto Descuento MN
	 */
	public void setMontoDescuentoMN(String strMontoDescuentoMN){
		this.montoDescuentoMN = strMontoDescuentoMN;
	}

	/**
	 * Obtiene el monto de Descuento MN
	 * @return Monto Descuento MN
	 */
	public String getMontoDescuentoMN(){
		return this.montoDescuentoMN;
	}

	/**
	 * Establece el monto de Descuento DL
	 * @param strMontoDL Monto Descuento DL
	 */
	public void setMontoDescuentoDL(String strMontoDescuentoDL){
		this.montoDescuentoDL = strMontoDescuentoDL;
	}

	/**
	 * Obtiene el monto de Descuento DL
	 * @return Monto Descuento DL
	 */
	public String getMontoDescuentoDL(){
		return this.montoDescuentoDL;
	}


	private String montoMN;
	private String montoDL;
	private String montoInteresMN;
	private String montoInteresDL;
	private String montoDescuentoMN;
	private String montoDescuentoDL;



}	//fin de la clase
