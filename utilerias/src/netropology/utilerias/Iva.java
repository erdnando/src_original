/**
 * Clase para Consultar el Valor del IVA. Fodea 001 - 2010.
 * Fecha: 	21/01/2009 
 *
 * @author 	Jesus Salim Hernandez David 
 * 
 */
package netropology.utilerias;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;

public class Iva{
  
	private final static Log log = ServiceLocator.getInstance().getLog(Iva.class);
	
	 /**
	  * Devuelve el valor del iva asociado a una fecha.
	  *   @param fecha Cadena de Texto con formato "NUMERO DIA/NUMERO MES/NUMERO A�O"
	  * 	@return Cadena de Texto con el Valor del IVA 
	  */
	public static String getPorcentaje(String fecha) throws AppException {
		
		log.info("getPorcentaje(String fecha)(E)");
		
		if(fecha == null || fecha.trim().equals("")){
			throw new AppException("getPorcentaje(Exception): La Fecha no puede venir vacia");	
		}
		
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			porcentajeIva 	= "0";
		
		try {	
 
			con.conexionDB();
			qrySentencia =
				"SELECT " + 
				"  FN_PORCENTAJE_IVA AS PORCENTAJE_IVA "  +
				"FROM "  + 
				"  comcat_iva "  +
				"WHERE "  + 
				"  DF_APLICACION <= TO_DATE(?,'DD/MM/YYYY') "  + 
				"ORDER BY DF_APLICACION DESC";
				
			lVarBind.add(fecha);	
			registros = con.consultarDB(qrySentencia, lVarBind, false);	
			
			log.debug("qrySentencia::  "+ qrySentencia);
			
			if(registros != null && registros.next() ){ 
				porcentajeIva = registros.getString("PORCENTAJE_IVA");
			}else{
				throw new Exception("No se encuentra parametrizado el IVA para la fecha que se esta consultando.");
			}
			
		} catch(Exception e) {
			log.error("getPorcentaje(String fecha){Exception}");
			log.error("getPorcentaje(String fecha).qrySentencia   = <" + qrySentencia   + ">");
			log.error("getPorcentaje(String fecha).fecha          = <" + fecha          + ">");
			e.printStackTrace();
			throw new AppException("getPorcentaje(String fecha){Exception}: Error al obtener el valor del porcentaje del iva");
		} finally {
			if(con.hayConexionAbierta()){	con.cierraConexionDB(); }
			log.info("getPorcentaje(String fecha)(S)");
		}
		
		return porcentajeIva;
	}
	
	/**
	 * Devuelve el iva correspodiente a la fecha actual.
	 * 
	 * @return Cadena de Texto con el Valor del IVA en Formato Decimal
	 */
	public static String getPorcentaje() throws AppException {
		
			log.info("getPorcentaje()(E)");
			
			String fechaActual = Fecha.getFechaActual();
      
      log.info("getPorcentaje()(E)");
      
			return getPorcentaje(fechaActual);
			
			
	}
	
	/**
	 *  Devuelve el valor del IVA en Formato Decimal para una fecha en especifico.
	 *    @param fecha Cadena de Texto con formato "NUMERO DIA/NUMERO MES/NUMERO A�O"
	 * 	@return	BigDecimal con el valor del IVA en Formato Decimal. 
	 */
	public static BigDecimal getValor(String fecha) 
		throws AppException{
			
			log.info("getValor(String fecha)(E)");
			
			BigDecimal tasa 	= new BigDecimal(getPorcentaje(fecha));
			BigDecimal iva 	= tasa.divide(new BigDecimal("100"),4,BigDecimal.ROUND_HALF_UP);
			log.info("getValor(String fecha)(S)");
			
			return iva;
	}
	
	/**
	 *  Devuelve el valor del IVA en Formato Decimal correspondiente a la fecha actual.
	 * 	@return	BigDecimal con el valor del IVA en Formato Decimal. 
	 */
	public static BigDecimal getValor() 
		throws AppException{
			log.info("getValor()(E)");
			String 		fechaActual = Fecha.getFechaActual(); 
			BigDecimal 	iva 			= getValor(fechaActual);
			log.info("getValor()(S)");
			return iva;
	}


}