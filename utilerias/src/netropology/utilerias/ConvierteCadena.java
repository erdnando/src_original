package netropology.utilerias;

import java.io.IOException;

public class ConvierteCadena
{
	public ConvierteCadena()
    {
    }

	public static String numeroALetra(String strTotal)
        	throws IOException{
        	return numeroALetra(Double.parseDouble(strTotal));
        }
	public static String numeroALetra(double Total)
		throws IOException
	{
		String cadena = new String();
		String CantidadLetra = new String();
		String aux = new String();
		char num;
		int fact = 0;
		int modulo = 0;
		int longitud = 0;
		int cont = 0, cont1 = 0;
		String x = "00";
    String validaMillares = "";

		try
        {
			if (Total < 0)
				Total = Total * -1;
			if (Total == 0)
				CantidadLetra = "cero ";
			else{
			fact = (int) Total;
			CantidadLetra = "";
			cadena = String.valueOf(fact);
			modulo = cadena.length() % 3;
			if (modulo == 1)
				cadena = "00" + cadena;
			else
				if (modulo == 2)
					cadena = "0" + cadena;
			modulo = 3;
			longitud = cadena.length();
      if(longitud>6)
        validaMillares = cadena.substring(cadena.length()-6,cadena.length()-3);
			for (cont = 0; cont < cadena.length(); cont += modulo) {
				aux = cadena.substring(cont, (cont + modulo));
				for (cont1 = 0; cont1 < 3; cont1++) {
					num = aux.charAt(cont1);
					switch (aux.charAt(cont1)) {
						case '0' :
							break;
						case '1' :
							if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '0')) {
								CantidadLetra += "diez ";
								cont1++;
							}
							else
								if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '1')) {
									CantidadLetra += "once ";
									cont1++;
								}
								else
									if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '2')) {
										CantidadLetra += "doce ";
										cont1++;
									}
									else
										if ((cont1== 1) && (aux.charAt(cont1 + 1) == '3')) {
											CantidadLetra += "trece ";
											cont1++;
										}
										else
										if((cont1 == 1) && (aux.charAt(cont1 + 1) == '4')) {
											CantidadLetra += "catorce ";
											cont1++;
										}
										else
											if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '5')) {
												CantidadLetra += "quince ";
												cont1++;
											}
											else
												if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '6')) {
													CantidadLetra += "dieciseis ";
													cont1++;
												}
												else
												if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '7')) {
													CantidadLetra += "diecisiete ";
													cont1++;
												}
												else
												if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '8')) {
													CantidadLetra += "dieciocho ";
													cont1++;
												}
												else
												if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '9')) {
													CantidadLetra += "diecinueve ";
													cont1++;
												}
												else
													if (cont1 == 2)
														CantidadLetra += "un";
													else
														if (cont1 == 0) {
															if ( aux.charAt(cont1 + 1) == '0' && aux.charAt(cont1 + 2) == '0')
																CantidadLetra += "cien ";
															else
																CantidadLetra += "ciento ";
														}
												break;
						case '2' :
							if (cont1 == 2)
								CantidadLetra += "dos ";
							else
								if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
									CantidadLetra +="veinte ";
								else
									if (cont1 == 1)
										CantidadLetra += "veinti";
									else
										if (cont1 ==0)
											CantidadLetra += "doscientos ";
									break;
						case '3' :
							if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "treinta ";
							else
								if (cont1 == 2)
									CantidadLetra +="tres ";
								else
									if (cont1 == 1)
										CantidadLetra += "treinta y ";
									else
										if (cont1 ==0)
											CantidadLetra += "trescientos ";
							break;
						case '4' :
							if (cont1 == 2)
								CantidadLetra += "cuatro ";
							else
								if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
									CantidadLetra +="cuarenta ";
								else
									if (cont1 == 1)
										CantidadLetra += "cuarenta y ";
									else
										if (cont1 ==0)
											CantidadLetra += "cuatrocientos ";
							break;
						case '5' :
							if (cont1 == 2)
								CantidadLetra += "cinco ";
							if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "cincuenta";
							else
								if (cont1 == 1)
									CantidadLetra +="cincuenta y ";
								else
									if (cont1 == 0)
										CantidadLetra += "quinientos ";
							break;
						case '6' :
							if (cont1 == 2)
								CantidadLetra += "seis ";
							if ((cont1 == 1) && (aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "sesenta ";
							else
								if (cont1 == 1)
									CantidadLetra += "sesenta y ";
								else
									if (cont1 == 0)
										CantidadLetra += "seiscientos ";
							break;
						case '7' :
							if (cont1 == 2)
								CantidadLetra += "siete ";
							if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "setenta ";
							else
								if (cont1 == 1)
									CantidadLetra +="setenta y ";
								else
									if (cont1 == 0)
										CantidadLetra += "setecientos ";
							break;
						case '8' :
							if (cont1 == 2)
								CantidadLetra += "ocho ";
							if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "ochenta ";
							else
								if (cont1 == 1)
									CantidadLetra +="ochenta y ";
								else
									if (cont1 == 0)
									CantidadLetra += "ochocientos ";
							break;
						case '9' :
							if (cont1 == 2)
								CantidadLetra += "nueve ";
							if ((cont1 == 1) &&(aux.charAt(cont1 + 1) == '0'))
								CantidadLetra += "noventa ";
							else
								if (cont1 == 1)
									CantidadLetra +="noventa y ";
								else
									if (cont1 == 0)
										CantidadLetra += "novecientos ";
							break;
					}
				}
				if (longitud == 9) {
					if (CantidadLetra.equals("un"))
						CantidadLetra = CantidadLetra + " millon ";
					else
						CantidadLetra = CantidadLetra + " millones ";
					longitud -= modulo;
				}
				else
					if ((longitud == 6)) {
            if(!"000".equals(validaMillares)){            
              CantidadLetra = CantidadLetra + " mil ";
            }
						longitud -= modulo;
					}
				if (longitud == 3);
			}
						}
			//CantidadLetra = CantidadLetra + " pesos";
		}
		catch(Exception e) {
            System.out.println("Error es ".concat(String.valueOf(String.valueOf(e))));
    }
        return CantidadLetra.toUpperCase();
	}
}

