
/***********************************************************************************************************
*
*	Title:        Utiler�as
*	Version:      1.0
*	Copyright:    Copyright (c) 2002
*	Author:       Manuel Ramos Mendoza
*	Company:      Netropology.
*	Description:  Archivo de importacion de datos a Tablas Dinamicas
*
***********************************************************************************************************/


package netropology.utilerias;

import java.io.FileInputStream;
import java.io.InputStream;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.StringTokenizer;
import java.util.Vector;


public class CImportaReg	{
	
	private String 	DELIMITADOR_REGISTRO = "\n",
					DELIMITADOR_CAMPO    = "|",
					CADENA_CONEXION       = "seguridadData";  //descuentoDS
	
	
	
	
	/************************************************************************************************
	*	IMPORTA DATOS DE TIPO FIJOS
	************************************************************************************************/
	
	public void importaDatosFijos( String lsPathArchivo, String lsNumPublicacion)	{
		
		Vector 	lVTipoDatos = obtenTipoDatos( lsNumPublicacion, "F" ),
				lVValoresDa = new Vector();
		
		if ( lVTipoDatos.size() == 0 ) {
			return;
		}
	   
		InputStream fImportar = null;
		try	{
			fImportar          = new FileInputStream( lsPathArchivo );
			int 		liNumBytes         = fImportar.available();
			char[] 		laContenido        = new char[liNumBytes];
			String		lsContenidoArchImp = "",
					lsInsertDatos      = "",
					lsRegistro         = "",
					lsCampo            = "";
			
			StringTokenizer lstTramaRegis,
							lstTramaCampo;
			
			
			
		    for (int i=0; i<liNumBytes; i++)	laContenido[i] = (char) fImportar.read();
			
		    lsContenidoArchImp = String.valueOf(laContenido);
			
	
			
			/**************************************~
			*	Desentramando Registros
			******/
			lstTramaRegis = new StringTokenizer(lsContenidoArchImp, DELIMITADOR_REGISTRO);
			while ( lstTramaRegis.hasMoreTokens() ) {
				
				lsRegistro = lstTramaRegis.nextToken();
				lsRegistro = lsRegistro.substring( 0, lsRegistro.length()-1 );
				String lsUltCaracter = lsRegistro.substring( lsRegistro.length()-1);
				if (  !lsUltCaracter.equals("|")  )	lsRegistro += "|";
				
	    		/**************************************~
				*	Desentramando Campos
				******/
				lstTramaCampo = new StringTokenizer( lsRegistro, DELIMITADOR_CAMPO );
				lVValoresDa = new Vector();
				while ( lstTramaCampo.hasMoreTokens() )	{
					lsCampo = lstTramaCampo.nextToken();
					lVValoresDa.addElement( lsCampo );
				}
				
				if (   insertaCampoFijo( lVValoresDa, lVTipoDatos, lsNumPublicacion )   )
					System.out.println(" ************  SE INSERTO..");
				else
					System.out.println(" ************  NO SE INSERTO EL REGISTRO.");
			}
		} catch (Exception err)	{
		   err.printStackTrace();
		} finally {
		   try {
		      if (fImportar!=null) {
		         fImportar.close();
		      }
		   } catch(Throwable t) {
		      t.printStackTrace();
		   }
		}
	}
	
	
	
	
	
	
	
	/************************************************************************************************
	*
	*	IMPORTA DATOS DE TIPO DETALLE
	*
	************************************************************************************************/
	
	public void importaDatosDetalle( String lsPathArchivo, String lsNumPublicacion)	{
	
		Vector 	lVTipoDatos = obtenTipoDatos( lsNumPublicacion, "D" ),
				lVValoresDa = new Vector();
		//int		liNumId     = obtenNumIdSig( lsNumPublicacion, 1 );
		
		if ( lVTipoDatos.size() == 0 ) {
			return;
		}
	   
		InputStream fImportar = null;
		try	{
			fImportar          = new FileInputStream( lsPathArchivo );
		    int 		liNumBytes         = fImportar.available();
		    char[] 		laContenido        = new char[liNumBytes];
			String		lsContenidoArchImp = "",
						lsInsertDatos      = "",
						lsRegistro         = "",
						lsCampo            = "";
			
			StringTokenizer lstTramaRegis,
							lstTramaCampo;
			
			
			
		    for (int i=0; i<liNumBytes; i++)	laContenido[i] = (char) fImportar.read();
			
		    lsContenidoArchImp = String.valueOf(laContenido);
			
	
			
			/**************************************~
			*	Desentramando Registros
			******/
			lstTramaRegis = new StringTokenizer(lsContenidoArchImp, DELIMITADOR_REGISTRO);
			while ( lstTramaRegis.hasMoreTokens() ) {
				
				lsRegistro    = lstTramaRegis.nextToken();
				
	    		/**************************************~
				*	Desentramando Campos
				******/
				lstTramaCampo = new StringTokenizer( lsRegistro, DELIMITADOR_CAMPO );
				lVValoresDa = new Vector();
				while ( lstTramaCampo.hasMoreTokens() )	{
					lsCampo = lstTramaCampo.nextToken();
					lVValoresDa.addElement( lsCampo );
				}
				
				if (   insertaCampoDetalle( lVValoresDa, lVTipoDatos, lsNumPublicacion )   )
					System.out.println(" ************  SE INSERTO..");
				else
					System.out.println(" ************  NO SE INSERTO EL REGISTRO.");
			}
		}
		catch (Exception err)	{
			System.out.println("ERROR: " + err);
		} finally {
		   try {
		      if (fImportar!=null) {
		         fImportar.close();
		      }
		   } catch(Throwable t) {
		      t.printStackTrace();
		   }
		}
	}
	
	
	/************************************************************************************************
	*
	*	INSERTA CAMPOS FIJOS
	*
	************************************************************************************************/
	
	public boolean insertaCampoFijo( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion )	{
	
		if ( lVValoresDatos.size() == 0 || lVTiposDatos.size() == 0 ) return false;
		if ( !checaPseudoLlave(lsNumPublicacion, (String)lVValoresDatos.elementAt(0), (String)lVValoresDatos.elementAt(1)) )	{
			System.out.println("LLAVE REPETIDA: '" + (String)lVValoresDatos.elementAt(0)  + "' y '" + (String)lVValoresDatos.elementAt(1) + "'");
			return false;
		}
		
		int		 liNumId       = obtenNumIdSig( lsNumPublicacion, "F" );
		boolean  lbNoError     = true;
		AccesoDB lobjConexion  = new AccesoDB();
		String 	 lsInsertDatos = "INSERT INTO tabla_"+lsNumPublicacion+" VALUES (" + liNumId + ", ",
				 lsCampo       = "";
		
		try	{
			lobjConexion.conexionDB();
			
			/***********************************~
			*	ARMANDO INSERT
			******/
			
			for (int i = 0; i<=lVTiposDatos.size(); i++)	{
				
				lsCampo = (String) lVValoresDatos.elementAt(i);
				
				if (i == 0)	lsInsertDatos += "'" + lsCampo + "'";   // EL PRIMER CAMPO CORRESPONDE AL NUM PROVEE-DISTRI
				else	{
					if (   ((String)lVTiposDatos.elementAt(i-1)).equals("alfanumerico")   
									||   ((String)lVTiposDatos.elementAt(i-1)).equals("seleccion")   )
						lsInsertDatos += "'" + lsCampo + "'";
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("numerico")    )
						lsInsertDatos += lsCampo;
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("hora")    )
						lsInsertDatos += "TO_DATE('" + lsCampo + "', 'hh24:mi')";
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("fecha")    )
						lsInsertDatos += "TO_DATE('" + lsCampo + "', 'dd/mm/yyyy')";
				}
				if ( i < lVTiposDatos.size() )	// SI NO ES EL ULTIMO CAMPO SE PONE COMA..
						lsInsertDatos += ", ";
						
			}	// FIN DEL FOR
			lsInsertDatos += ")";
			
			lobjConexion.ejecutaSQL( lsInsertDatos );
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg en la clase insertaCampoFijo.");
			System.out.println("ERROR: " + errSql.getMessage());
			lbNoError = false;
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg en la clase insertaCampoFijo.");
			System.out.println("ERROR: " + err.getMessage());
			
			System.out.print("\t " + lVValoresDatos + "                " +  lVTiposDatos);
			lbNoError = false;
		}
		finally	{
			if ( lobjConexion != null )	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
		
		return lbNoError;
	}
	
	
	
	
	
	
	
	/************************************************************************************************
	*
	*	INSERTA CAMPOS DE DETALLE     (POR REGISTRO)
	*
	************************************************************************************************/
	
	public boolean insertaCampoDetalle( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion )	{
		
		if ( lVValoresDatos.size() == 0 || lVTiposDatos.size() == 0 ) return false;
		
		int		 liNumId       = obtenNumId( lsNumPublicacion, (String)lVValoresDatos.elementAt(0), (String)lVValoresDatos.elementAt(1) ),
				 liNumId_d     = obtenNumIdSig( lsNumPublicacion, liNumId );
		boolean  lbNoError     = true;
		AccesoDB lobjConexion  = new AccesoDB();
		String 	 lsInsertDatos = "INSERT INTO tabla_d_"+lsNumPublicacion+" VALUES (" + liNumId + ", " + liNumId_d + ", ",
				 lsCampo       = "",
				 lsTipoDato    = "";
		
		
		if ( liNumId == -1 )	{
			/******************************************************************************
			*	SI REGRESA CERO ES PORKE NO HAY RELACION EN LA TABLA DE CAMPOS FIJOS
			********/
			System.out.println("NO HAY RELACION DE LOS CAMPOS (" + (String)lVValoresDatos.elementAt(0) + " Y " + (String)lVValoresDatos.elementAt(1) + ")");
			return false;
		}
		
		try	{
			lobjConexion.conexionDB();
			
			/***********************************~
			*	ARMANDO INSERT
			******/
			
			for (int i = 0; i<lVTiposDatos.size(); i++)	{
				
				lsCampo    = (String) lVValoresDatos.elementAt(i+2);
				lsTipoDato = (String) lVTiposDatos.elementAt(i);
				
				if ( lsTipoDato.equals("alfanumerico")  ||  lsTipoDato.equals("seleccion")   )
					lsInsertDatos += "'" + lsCampo + "'";
				else if ( lsTipoDato.equals("numerico")    )
					lsInsertDatos += lsCampo;
				else if ( lsTipoDato.equals("hora")    )
					lsInsertDatos += "TO_DATE('" + lsCampo + "', 'hh24:mi')";
				else if ( lsTipoDato.equals("fecha")    )
					lsInsertDatos += "TO_DATE('" + lsCampo + "', 'dd/mm/yyyy')";
				
				if ( i < lVTiposDatos.size()-1 )	// SI NO ES EL ULTIMO CAMPO SE PONE COMA..
						lsInsertDatos += ", ";
						
			}	// FIN DEL FOR
			lsInsertDatos += ")";
			
			//System.out.println("\n lsInsertDatos: " + lsInsertDatos + "\n");
			lobjConexion.ejecutaSQL( lsInsertDatos );
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg en la clase insertaCampoDetalle.");
			System.out.println("ERROR: " + errSql.getMessage());
			lbNoError = false;
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg en la clase insertaCampoDetalle.");
			System.out.println("ERROR: " + err.getMessage());
			
			System.out.print("\t " + lVValoresDatos + "                " +  lVTiposDatos);
			lbNoError = false;
		}
		finally	{
			if ( lobjConexion != null )	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
		
		return lbNoError;
	}
	
	
	
	
	
	
	
	
	/************************************************************************************************
	*
	*	CHECA PSEUDOLLAVE
	*
	************************************************************************************************/
	public boolean checaPseudoLlave(String lsNumPublicacion, String lsNumProv, String lsCampo1)	{
		boolean  lbNoError  = true;
		String	 lsQryLlave = "SELECT id FROM tabla_"+lsNumPublicacion+" WHERE campo0 = '"+lsNumProv+"' AND campo1 = '"+lsCampo1+"'";
		AccesoDB lobjConexion = new AccesoDB();
		
		try	{
			lobjConexion.conexionDB();
			ResultSet lCursor = lobjConexion.queryDB( lsQryLlave );
			if ( lCursor.next() )	lbNoError = false;
			lCursor.close();
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			if ( lobjConexion != null )	lobjConexion.cierraConexionDB();
		}
		
		return lbNoError;
	}
	
	
	
	
	
	
	/************************************************************************************************
	*
	*	REGRESA EL NUMERO DE ID SIGUIENTE DE CAMPOS FIJOS O EDITABLES
	*
	************************************************************************************************/
	
	public int obtenNumIdSig( String lsNumPublicacion, String lsCaracter )	{
		String 	 lsQryNumId = "SELECT NVL(MAX(id)+1, 0) AS num_id_sig FROM tabla_" + lsNumPublicacion;
		int 	 liNumId = 0;
		AccesoDB lobjConexion = new AccesoDB();
		
		try	{
			lobjConexion.conexionDB();
			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("num_id_sig");
			lCurNumId.close();
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			if ( lobjConexion != null )	lobjConexion.cierraConexionDB();
		}
		
		return liNumId;
	}
	
	
	
	
	
	
	/************************************************************************************************
	*	REGRESA EL NUMERO DE ID KE LE CORRESPONDE A LA PSEUDOLLAVE    
	*	SE USA PARA RELACIONES CON LOS CAMPOS DE DETALLE
	************************************************************************************************/
	
	public int obtenNumId( String lsNumPublicacion, String lsNumProv, String lsCampo1 )	{
	
		String 	 lsQryNumId = "SELECT id FROM tabla_"+lsNumPublicacion+" WHERE campo0 = '"+lsNumProv+"' AND campo1 = '"+lsCampo1+"'";
		int 	 liNumId = -1;
		AccesoDB lobjConexion = new AccesoDB();
		
		try	{
			lobjConexion.conexionDB();
			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("id");
			lCurNumId.close();
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero id de la pseudollave.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero id de la pseudollave.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			if ( lobjConexion != null )	lobjConexion.cierraConexionDB();
		}
		
		return liNumId;
	}
	
	
	
	
	/************************************************************************************************
	*
	*	REGRESA EL NUMERO DE ID SIGUIENTE DE CAMPOS DETALLE
	*
	************************************************************************************************/
	
	public int obtenNumIdSig( String lsNumPublicacion, int liId )	{
		String 	 lsQryNumId = "SELECT NVL(MAX(id_d)+1, 0) AS num_id_sig FROM tabla_d_"+lsNumPublicacion+" WHERE id = " + liId;
		int 	 liNumId = 0;
		AccesoDB lobjConexion = new AccesoDB();
		
		try	{
			lobjConexion.conexionDB();
			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("num_id_sig");
			lCurNumId.close();
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener el numero de id siguiente.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			if ( lobjConexion != null )	lobjConexion.cierraConexionDB();
		}
		
		return liNumId;
	}
	
	
	
	
	/************************************************************************************************
	*
	*	REGRESA UN VECTOR DE DE LOS TIPOS DE DATOS FIJOS
	*
	************************************************************************************************/
	
	public Vector obtenTipoDatos( String lsNumPublicacion, String lsCaracterCampo )	{
		
		String lsQryTipoDatos = "";
		Vector lVTipoDatos    = new Vector();
		AccesoDB lobjConexion = new AccesoDB();
		
		if ( lsCaracterCampo.equals("F") || lsCaracterCampo.equals("E"))
			lsQryTipoDatos = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib " +
							"WHERE cs_tipo_atrib = '"+lsCaracterCampo+"' AND ic_publicacion = "+lsNumPublicacion+" " +
							"ORDER BY ic_atributo";
		else if ( lsCaracterCampo.equals("D") )
			lsQryTipoDatos = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib_d " +
							"WHERE ic_publicacion = "+lsNumPublicacion+" " +
							"ORDER BY ic_atributo_d";
		
		try	{
			lobjConexion.conexionDB();
			ResultSet lCurTDatos = lobjConexion.queryDB( lsQryTipoDatos );
			while ( lCurTDatos.next() )
				lVTipoDatos.addElement( lCurTDatos.getString("cg_tipo_dato") );
			lCurTDatos.close();
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener los tipos de datos.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg al intentar obtener los tipos de datos.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
		}
		
		return lVTipoDatos;
	}
	
	
	
	
	
	/************************************************************************************************
	*
	*	VERIFICA SI EXISTE UNA TALBA
	*
	************************************************************************************************/
	
	public boolean existeTabla( String lsNumPublicacion, String lsCaracterTabla )	{
		String 	 lsQryExisTabla = "";
		boolean  lbExiste       = false;
		
		if ( lsCaracterTabla.equals("D") )
			lsQryExisTabla = "SELECT * FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + lsNumPublicacion;
		else
			lsQryExisTabla = "SELECT * FROM com_tabla_pub_atrib " +
						 	 "WHERE cs_tipo_atrib = '"+lsCaracterTabla+"' AND ic_publicacion = " + lsNumPublicacion;
		
		System.out.print("\t\nlsQryExisTabla: " + lsQryExisTabla + "\n");
		
		AccesoDB lobjConexion = new AccesoDB();
		//AccesoDB lobjConexion = new AccesoDB();
		try	{
			
			lobjConexion.conexionDB();
			//lobjConexion.conectarDB();
			
			ResultSet lCurExiteTabla = lobjConexion.queryDB( lsQryExisTabla );
			if ( lCurExiteTabla.next() )	lbExiste = true;
			lCurExiteTabla.close();
			lobjConexion.cierraStatement();
			
			System.out.print("\t\n cerrando rs \n");
			
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg de la funcion existeTabla.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en la clase CImportaReg de la funcion existeTabla.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			System.out.print("\t\n EN FINALLY \n");
			lobjConexion.cerrarDB(true);
			
			if ( lobjConexion.hayConexionAbierta() ) {
				lobjConexion.cierraConexionDB();
			}
		}
		
		return lbExiste;
	}
	
}