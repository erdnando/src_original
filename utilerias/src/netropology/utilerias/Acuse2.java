package netropology.utilerias;


public class Acuse2 extends Acuse implements java.io.Serializable {

	public Acuse2(int tipoAcuse, String prodNafin) {
		super(tipoAcuse, prodNafin);
	}

	public String toString() {
		return super.getAcuse();
	}

	/**
	 * Establece el monto de MN
	 * @param strMontoMN Monto MN
	 */
	public void setMontoMN(String strMontoMN){
		this.montoMN = strMontoMN;
	}

	/**
	 * Obtiene el monto de MN
	 * @return Monto MN
	 */
	public String getMontoMN(){
		return this.montoMN;
	}

	/**
	 * Establece el monto de DL
	 * @param strMontoDL Monto DL
	 */
	public void setMontoDL(String strMontoDL){
		this.montoDL = strMontoDL;
	}

	/**
	 * Obtiene el monto de DL
	 * @return Monto DL
	 */
	public String getMontoDL(){
		return this.montoDL;
	}


	/**
	 * Establece el monto de Interes MN
	 * @param strMontoMN Monto Interes MN
	 */
	public void setMontoInteresMN(String strMontoInteresMN){
		this.montoInteresMN = strMontoInteresMN;
	}

	/**
	 * Obtiene el monto de Interes MN
	 * @return Monto Interes MN
	 */
	public String getMontoInteresMN(){
		return this.montoInteresMN;
	}

	/**
	 * Establece el monto de Interes DL
	 * @param strMontoDL Monto Interes DL
	 */
	public void setMontoInteresDL(String strMontoInteresDL){
		this.montoInteresDL = strMontoInteresDL;
	}

	/**
	 * Obtiene el monto de Interes DL
	 * @return Monto Interes DL
	 */
	public String getMontoInteresDL(){
		return this.montoInteresDL;
	}

	/**
	 * Establece el numero de documentos de MN
	 * @param strDocumentosMN N�mero de documentos en MN
	 */
	public void setDocumentosMN(String strDocumentosMN){
		this.documentosMN = strDocumentosMN;
	}

	/**
	 * Obtiene  el numero de documentos de MN
	 * @return Numero de docuemntos de MN
	 */
	public String getDocumentosMN(){
		return this.documentosMN;
	}

	/**
	 * Establece el numero de documentos de DL
	 * @param strDocumentosDL N�mero de documentos en DL
	 */
	public void setDocumentosDL(String strDocumentosDL){
		this.documentosDL = strDocumentosDL;
	}

	/**
	 * Obtiene  el numero de documentos de DL
	 * @return Numero de docueDLtos de DL
	 */
	public String getDocumentosDL(){
		return this.documentosDL;
	}

	private String montoMN;
	private String montoDL;
	private String montoInteresMN;
	private String montoInteresDL;
	private String documentosMN;
	private String documentosDL;

}	//fin de la clase
