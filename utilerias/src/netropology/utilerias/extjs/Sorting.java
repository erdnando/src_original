package netropology.utilerias.extjs;

import java.util.ArrayList;
import java.util.HashMap;

public class Sorting {

	public String 		oldSort;
	public String 		oldDir;
	public String 		sort;
	public String 		dir;
	
	public ArrayList 	sortOrder;
	public HashMap 	columnMapping;
	
	
	public String getOldSort(){
		return oldSort;
	}
 
	public void setOldSort(String oldSort){
		this.oldSort = oldSort;
	}
 
	public String getOldDir(){
		return oldDir;
	}
 
	public void setOldDir(String oldDir){
		this.oldDir = oldDir;
	}
 
	public String getSort(){
		return sort;
	}

	public void setSort(String sort){
		this.sort = sort;
	}

	public String getDir(){
		return dir;
	}
 
	public void setDir(String dir){
		this.dir = dir;
	}

	public ArrayList getSortOrder(){
		return sortOrder;
	}


	public void setSortOrder(ArrayList sortOrder){
		this.sortOrder = sortOrder;
	}
 
	public HashMap getColumnMapping(){
		return columnMapping;
	}

 
	public void setColumnMapping(HashMap columnMapping){
		this.columnMapping = columnMapping;
	}
 
	public boolean recalculatePK(){
		
		if(this.sort == null || this.oldSort == null) 	return true;
		if(this.dir  == null || this.oldDir  == null) 	return true;
		if(!sort.equals(oldSort))								return true;
		if(!dir.equals(oldDir))									return true;
		
		return false;
	}
	
	public String getSortString(){
		
		StringBuffer sortString = new StringBuffer();
		
		if(this.sort == null) return "";
		if(this.dir  == null) return "";
		if(this.columnMapping 	== null || this.columnMapping.size() 	== 0 ) return "";
		if(this.sortOrder 		== null || this.sortOrder.size() 		== 0 ) return "";
		
		boolean sortStringFound = false;
		
		for(int i=0;i<sortOrder.size();i++){
			
			String s = (String) sortOrder.get(i);
			
			if(s != null && s.equals(this.sort)){
				if( columnMapping.get(this.sort) != null ){
						sortStringFound = true;
						break;
				}
			}
			
		}
		
		sortString.append(" ORDER BY ");
		

		if(sortStringFound){
			
			sortString.append((String) columnMapping.get(this.sort));

			/*for(int i=0;i<sortOrder.size();i++){
				
				String key = (String) sortOrder.get(i);
				
				if( key.equals(this.sort)) continue;
					
				sortString.append(", ");
				sortString.append((String) columnMapping.get(key));			
			}*/
			
		}else{
			
			for(int i=0;i<sortOrder.size();i++){
				String key = (String) sortOrder.get(i);
				if(i> 0) sortString.append(", ");
				sortString.append((String) columnMapping.get(key));			
			}
			
		}
		
		sortString.append(" " );
		sortString.append( "ASC".equals(this.dir)?"ASC":("DESC".equals(this.dir)?"DESC":"ASC") );
		
		return sortString.toString();
	}
	
}
