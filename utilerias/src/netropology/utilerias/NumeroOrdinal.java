package netropology.utilerias;

import java.util.HashMap;

public class NumeroOrdinal{

  public static String getNombre(String numero){
  
  	  	String nombre = "";
  	  
  	  	// Unidades:
  	  	HashMap unidad = new HashMap();
		unidad.put("0","");
		unidad.put("1","Primer");
		unidad.put("2","Segundo");
		unidad.put("3","Tercer");
		unidad.put("4","Cuarto");
		unidad.put("5","Quinto");
		unidad.put("6","Sexto");
		unidad.put("7","Séptimo");
		unidad.put("8","Octavo");
		unidad.put("9","Noveno");
  	  	
  	  	// De los múltiplos de diez:
  	  	HashMap multiplosDiez = new HashMap();
		multiplosDiez.put("0","");
		multiplosDiez.put("1","Décimo");
		multiplosDiez.put("2","Vigésimo");
		multiplosDiez.put("3","Trigésimo");
		multiplosDiez.put("4","Cuadragésimo");
		multiplosDiez.put("5","Quincuagésimo");
		multiplosDiez.put("6","Sexagésimo");
		multiplosDiez.put("7","Septuagésimo");
		multiplosDiez.put("8","Octogésimo");
		multiplosDiez.put("9","Nonagésimo");
		
		// De los múltiplos de cien:
		HashMap multiplosCien = new HashMap();
		multiplosCien.put("0","");
		multiplosCien.put("1","Centésimo");
		multiplosCien.put("2","Ducentésimo");
		multiplosCien.put("3","Tricentésimo");
		multiplosCien.put("4","Cuadringentésimo");
		multiplosCien.put("5","Quingentésimo");
		multiplosCien.put("6","Sexagentésimo");
		multiplosCien.put("7","Septingentésimo");
		multiplosCien.put("8","Octingentésimo");
		multiplosCien.put("9","Noningentésimo");  
  	  
		try {
  	  	  int entero = Integer.parseInt(numero);  
  	  	  
  	  	  if(entero<1){
  	  	  	  nombre = "Ningún";
  	  	  }else if(entero >= 1 && entero < 20){
  	  	  	   switch(entero){
					case 1: 	nombre = "Primer"; 			break;
					case 2: 	nombre = "Segundo"; 			break;
					case 3: 	nombre = "Tercer"; 			break;
					case 4: 	nombre = "Cuarto"; 			break;
					case 5: 	nombre = "Quinto"; 			break;
					case 6: 	nombre = "Sexto"; 			break;
					case 7: 	nombre = "Séptimo"; 			break;
					case 8: 	nombre = "Octavo"; 			break;
					case 9: 	nombre = "Noveno"; 			break;
					case 10: nombre = "Décimo"; 			break;
					case 11: nombre = "Undécimo"; 		break;
					case 12: nombre = "Duodécimo"; 		break;
					case 13: nombre = "Decimotercero"; 	break;
					case 14: nombre = "Decimocuarto"; 	break;
					case 15: nombre = "Decimoquinto"; 	break;
					case 16: nombre = "Decimosexto"; 	break;
					case 17: nombre = "Decimoséptimo"; 	break;
					case 18: nombre = "Decimoctavo"; 	break;
					case 19: nombre = "Decimonoveno"; 	break;
            }
            
  	  	  }else if(entero >= 20 && entero < 1000){
  	  	  	  
  	  	  	  String []digitos = null;
  	  	  	  
  	  	  	  numero 	= reverse(numero);
  	  	  	  digitos 	= numero.split("");
 
  	  	  	  nombre = digitos.length > 1?(String) unidad.get(digitos[1])								:"";
  	  	  	  nombre = digitos.length > 2?(String) multiplosDiez.get(digitos[2]) + " " + nombre	:nombre;
  	  	  	  nombre = digitos.length > 3?(String) multiplosCien.get(digitos[3]) + " " + nombre	:nombre;

 
  	  	  }else{
  	  	  		nombre = String.valueOf(numero) + "°"; 
  	  	  }
  	  	  
  	  }catch(Exception e){
  	  	  return "Ningún";
  	  }
  	  
  	  return nombre;
  }

  public static String reverse(String str){
  	  
  	 StringBuffer 	buffer	= null;
  	 int 				i			= 0;
  	 int 				longitud = 0; 
  	 
  	 try {
  	 	 
		 i	 			= str.length(); 
		 longitud 	= str.length();
		 buffer 		= new StringBuffer(longitud);
	
		 for (i = (longitud - 1); i >= 0; i--){
			buffer.append(str.charAt(i));
		 }
		 
    }catch(Exception e){
    	 return str;
    }
    
    return buffer.toString();
    
  }

  
}