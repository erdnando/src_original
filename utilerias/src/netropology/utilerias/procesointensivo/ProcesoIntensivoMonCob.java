package netropology.utilerias.procesointensivo;

import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.util.Vector;

import netropology.utilerias.AccesoDB;
//import com.netro.descuento.*;

/**
 * Clase para manejo de procesos intensivos que puedan llevar a un error de
 * timeout por la gran cantidad de tiempo que toma en terminar el proceso y
 * contestar al usuario.
 * @author Israel Herrera IHJ
 * @author Gilberto Aparicio GEAG
 */
public class ProcesoIntensivoMonCob implements Runnable, Serializable {
    
	private int porcentajeProgreso = 0;
	private int registrosProcesados = 0;
	private int claveProducto = 0;

	public ProcesoIntensivoMonCob() {}

	/**
	 * Obtiene el porcentaje de avance de la tarea
	 * @return porcentaje de avance
	 */
 	public int getPorcentajeProgreso() {
		return porcentajeProgreso;
	}

	/**
	 * Obtiene el numero de registros procesados por la tarea
	 * @return registros procesados
	 */
 	public int getRegistrosProcesados() {
		return registrosProcesados;
	}

	/**
	 * Obtiene la clave del producto
	 * @return entero con la clave del producto
	 */
 	public int getClaveProducto() {
		return claveProducto;
	}

	/**
	 * Establece la clave del producto
	 * @param claveProducto Clave del producto:
	 * 	2 Pedidos y 5 para credicadenas
	 */
	public void setClaveProducto(int claveProducto) {
		this.claveProducto = claveProducto;
	}
   
	/**
	 * Metodo principal que correra en un hilo por separado
	 */

	public void run() {
		System.out.println("inicia proceso intensivo");
		this.setPorcentajeProgreso(0);
		try {
			if (getClaveProducto() == 2 || getClaveProducto() == 0) {
				this.procesoValidaSiracPedidos();
			}	
			if (getClaveProducto() == 5 || getClaveProducto() == 0) {
				this.procesoValidaSiracCredicadenas();
			}
		} finally {
			System.out.println("termina proceso intensivo");
			this.setPorcentajeProgreso(100);
		}
	}


	//****************** METODOS PRIVADOS ***************************
	/**
	 * Establece el porcentaje de avance de la tarea
	 */
	private void setPorcentajeProgreso(int porcentajeProceso) {
		this.porcentajeProgreso = porcentajeProceso;
	}

	/**
	 * Establece el numero de registros procesados por la tarea
	 * @param registrosProcesados Registros procesados
	 */
 	private void setRegistrosProcesados(int registrosProcesados) {
		this.registrosProcesados = registrosProcesados;
	}


	private void setMonitorCob(String ig_numero_prestamo, String ic_producto_nafin, 
			String ic_epo, String ic_pyme, String ic_if, String ic_pedido, String cg_pedido_disp, 
			String ic_moneda, String df_fecha_inicob, 
			double ig_capital_pagado_ne, double ig_saldo_capital,
			double ig_interes_pagado_ne, double ig_saldo_interes,
			double ig_interesm_pagado_ne, double ig_saldo_interesm, 
			double ig_saldo_total_ne, double ig_saldo_total, 
			String estatusSirac, AccesoDB con) {
		System.out.println("::setMonitorCob(E)");
		String				qrySentencia 	= "";
		String 				condicion		= "";
		PreparedStatement	ps				= null;
		boolean				bOk				= true;
		Vector				renglones		= new Vector();
		try {

			qrySentencia = 
				" INSERT INTO com_monitor_cob"   +
				"             (ig_numero_prestamo, ic_producto_nafin, ic_epo,"   +
				"              ic_pyme, ic_if, ic_pedido, cg_pedido_disp,"   +
				"              ic_moneda, df_inicio_cobro, ig_capital_pagado_ne,"   +
				"              ig_saldo_capital, ig_interes_pagado_ne, ig_saldo_interes,"   +
				"              ig_interesm_pagado_ne, ig_saldo_interesm,"   +
				"              ig_saldo_total_ne, ig_saldo_total,"   +				
				"              df_ejecucion_proceso, cg_inconsistencia)"   +
				"      VALUES (?, ?, ?, ?, ?, ?, ?, ?, to_date(?, 'dd/mm/yyyy'), ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?)"  ;
			//System.out.println("\n qrySentencia : "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong( 1, Long.parseLong(ig_numero_prestamo));
			ps.setInt( 2, Integer.parseInt(ic_producto_nafin));
			ps.setInt( 3, Integer.parseInt(ic_epo));
			ps.setInt( 4, Integer.parseInt(ic_pyme));
			ps.setInt( 5, Integer.parseInt(ic_if));
			if("NULL".equals(ic_pedido))
				ps.setNull( 6, Types.INTEGER);
			else
				ps.setInt( 6, Integer.parseInt(ic_pedido));
			ps.setString( 7, cg_pedido_disp);
			ps.setInt( 8, Integer.parseInt(ic_moneda));
			ps.setString( 9, df_fecha_inicob);
			ps.setDouble(10, ig_capital_pagado_ne);
			ps.setDouble(12, ig_interes_pagado_ne);
			ps.setDouble(14, ig_interesm_pagado_ne);
			if("I".equals(estatusSirac)) {
				ps.setNull(11, Types.DOUBLE);
				ps.setNull(13, Types.DOUBLE);
				ps.setNull(15, Types.DOUBLE);
				ps.setNull(17, Types.DOUBLE);
				ps.setString(18, estatusSirac);
			} else {
				ps.setDouble(11, ig_saldo_capital);
				ps.setDouble(13, ig_saldo_interes);
				ps.setDouble(15, ig_saldo_interesm);
				ps.setDouble(17, ig_saldo_total);
				ps.setNull(18, Types.VARCHAR);
			}
			ps.setDouble(16, ig_saldo_total_ne);
			ps.executeUpdate();
			ps.close();

		} catch(Exception e) { 
			bOk = false;
			System.out.println("::setMonitorCob(Exception) "+e);
			e.printStackTrace();
		} finally { 
			con.terminaTransaccion(bOk);
			System.out.println("::setMonitorCob(S)");
		}
	}//setMonitorCob



	private Vector getDatosSirac(String ig_numero_prestamo, AccesoDB con) {
		System.out.println("::getDatosSirac(E)");
		String				qrySentencia 	= "";
		String 				condicion		= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		boolean				bOk				= true;
		Vector				renglones		= new Vector();
		try {
			qrySentencia = 
				" SELECT   /*+ index(spagos PR_PK_SAL_PLA_PAG)*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'C', "   +
				"         DECODE (tip.estatus_saldo, 1, spagos.valor, /*Capital Vigente*/"   +
				"                                    2, spagos.valor, 0), 0), 0)), 0) AS capital_total, /*Capital Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'C', "   +
				"         DECODE (tip.estatus_saldo, 1, spagos.valor_pagado, /*Capital Vigente*/"   +
				"                                    2, spagos.valor_pagado, 0), 0), 0)), 0) AS capital_pagado, /*Capital Vencido*/"   +
				"        NVL (SUM (NVL ("   +
				"         DECODE (tip.tipo_abono, 'I', DECODE (tip.estatus_saldo, 1, spagos.valor, /*Interes Vigente*/"   +
				"                                                                 2, spagos.valor, 0), 0), 0)), 0) AS int_normal, /*Interes Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', DECODE (tip.estatus_saldo, 1, spagos.valor_pagado, /*Interes Vigente*/"   +
				"                                                                               2, spagos.valor_pagado, 0), 0), 0)), 0) AS int_normal_pagado, /*Interes Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', "   +
				"         DECODE (tip.estatus_saldo, 3, spagos.valor, /*Moratorio*/"   +
				"                                    4, spagos.valor, 0), 0), 0)), 0) AS int_moratorio, /*Sobretasa Moraratoria*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', "   +
				"         DECODE (tip.estatus_saldo, 3, spagos.valor_pagado, /*Moratorio*/"   +
				"                                    4, spagos.valor_pagado, 0), 0), 0)), 0) AS int_moratorio_pagado, /*Sobretasa Moraratoria*/"   +
				"        TO_CHAR (pre.fecha_apertura, 'dd/mm/yyyy') AS fecha_apertura"   +
				"   FROM pr_saldos_plan_pago spagos, pr_tipos_saldo tip, pr_prestamos pre"   +
				"  WHERE spagos.codigo_tipo_saldo = tip.codigo_tipo_saldo"   +
				"    AND spagos.codigo_empresa = pre.codigo_empresa"   +
				"    AND spagos.codigo_agencia = pre.codigo_agencia"   +
				"    AND spagos.codigo_sub_aplicacion = pre.codigo_sub_aplicacion"   +
				"    AND spagos.numero_prestamo = pre.numero_prestamo"   +
				"    AND pre.numero_prestamo = ?"   +
				"  GROUP BY TO_CHAR (pre.fecha_apertura, 'dd/mm/yyyy')"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ig_numero_prestamo));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				double totCapital	 = rs.getDouble("capital_total") - rs.getDouble("capital_pagado");
				double totInteres	 = rs.getDouble("int_normal") - rs.getDouble("int_normal_pagado");
				double totInteresM	 = rs.getDouble("int_moratorio") - rs.getDouble("int_moratorio_pagado");
				double diferenciaTot = totCapital + totInteres + totInteresM;
				renglones.addElement(new Double(totCapital));
				renglones.addElement(new Double(totInteres));
				renglones.addElement(new Double(totInteresM));
				renglones.addElement(new Double(diferenciaTot));
			}
			rs.close();
			ps.close();
		} catch(Exception e) { 
			bOk = false;
			System.out.println("::getDatosSirac(Exception) "+e);
			e.printStackTrace();
		} finally { 
			con.terminaTransaccion(bOk);
			System.out.println("::getDatosSirac(S)");
		}
		return renglones;
	}//getDatosSirac



	private void procesoValidaSiracPedidos() {
		System.out.println("::procesoValidaSiracPedidos(E)");
		String				qrySentencia 	= "";
		String 				condicion		= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		AccesoDB 			con				= null;
		boolean				bOk				= true;
		Vector				renglones		= new Vector();
		int numRegistrosProcesadosPedidos = this.getRegistrosProcesados();
		try {
			con = new AccesoDB();
			con.conexionDB();

			//EJECUTAR DE ACTUALIZACION DE SALDOS NORMAL PEDIDOS
			this.setPorcentajeProgreso(10);
			this.setPorcentajeProgreso(40);
			qrySentencia = 
				" SELECT   /*+index(ant) index(sel) index(lc) use_nl(ped sel ant lc pym cif epo mon)*/"   +
				"         ped.ic_pedido,"   +
				"         ped.ic_epo, "   +
				"         ped.ic_pyme,"   +
				"         ant.ig_numero_prestamo, "   +
				"         ped.ig_numero_pedido,"   +
				"         lc.ic_moneda,"   +
				"         NVL(sel.fn_credito_recibir,0) - NVL(sel.fn_saldo_credito,0) AS capital_pagado,"   +
				"         NVL(sel.fn_interes_pagado,0) as fn_interes_pagado, "   +
				"         NVL(sel.fn_interesm_pagado,0) as fn_interesm_pagado, "   +
				"         to_char(ant.df_operacion, 'dd/mm/yyyy') as df_operacion"   +
				"         ,lc.ic_if "   +
				"         ,ped.ic_pedido "   +
				"         ,NVL(sel.fn_saldo_credito,0) as fn_saldo_credito "   +
				"         ,NVL(sel.fn_credito_interes,0) - NVL(sel.fn_interes_pagado,0) AS SaldoInteres "   +
				"         ,NVL(sel.fn_credito_interesm,0) - NVL(sel.fn_interesm_pagado,0) AS SaldoInteresM "   +
				"         ,to_char(sel.df_cobro_cr, 'dd/mm/yyyy') as fechainiciocobr"   +
				"     FROM com_pedido ped,"   +
				"          com_pedido_seleccionado sel,"   +
				"          com_anticipo ant,"   +
				"          com_linea_credito lc,"   +
				"          comcat_if cif"   +
				"    WHERE ped.ic_pedido = sel.ic_pedido"   +
				"      AND sel.ic_pedido = ant.ic_pedido"   +
				"      AND sel.ic_linea_credito = lc.ic_linea_credito"   +
				"      AND lc.ic_if = cif.ic_if"   +
				"      AND ant.ig_numero_prestamo IS NOT NULL"   +
				"      AND NOT EXISTS (SELECT 1"   +
				"                        FROM com_monitor_cob"   +
				"                       WHERE ig_numero_prestamo = ant.ig_numero_prestamo)	 "   +
				"      AND cif.ig_tipo_piso = 1"   +
				" ORDER BY ant.df_operacion"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			this.setPorcentajeProgreso(60);
			while(rs.next()){
				String estatus = "";
				String rs_pedido			= rs.getString("ic_pedido")==null?"":rs.getString("ic_pedido");
				String rs_epo 				= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String rs_pyme	 			= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_numPrestamo 		= rs.getString("ig_numero_prestamo")==null?"":rs.getString("ig_numero_prestamo");
				String rs_numPedido 		= rs.getString("ig_numero_pedido")==null?"":rs.getString("ig_numero_pedido");
				String rs_moneda 			= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String rs_fechaOperacion 	= rs.getString("df_operacion")==null?"":rs.getString("df_operacion");
				String rs_if 				= rs.getString("ic_if")==null?"":rs.getString("ic_if");
				String rs_saldo_credito		= rs.getString("fn_saldo_credito")==null?"0":rs.getString("fn_saldo_credito");
				String rs_saldo_interes		= rs.getString("SaldoInteres")==null?"0":rs.getString("SaldoInteres");
				String rs_saldo_interesM	= rs.getString("SaldoInteresM")==null?"0":rs.getString("SaldoInteresM");
				String rs_fecha_inicio_cbr	= rs.getString("fechainiciocobr")==null?"":rs.getString("fechainiciocobr");

				double rs_capitalPagado	 	= rs.getDouble("capital_pagado");
				double rs_interesPagado		= rs.getDouble("fn_interes_pagado");
				double rs_interesMPagado	= rs.getDouble("fn_interesm_pagado");
				

				
//				double rs_diferenciaTot		= rs_capitalPagado + rs_interesPagado + rs_interesMPagado;

				double saldoTotalNE			= Double.parseDouble(rs_saldo_credito)+Double.parseDouble(rs_saldo_interes)+Double.parseDouble(rs_saldo_interesM);

				Vector datSirac = getDatosSirac(rs_numPrestamo, con);
					
				double sirac_capitalPagado	 	= 0;
				double sirac_interesPagado		= 0;
				double sirac_interesMPagado		= 0;
				double sirac_diferenciaTot		= 0;
				String infoSirac				= "";

				if(datSirac.size()>0) {
					sirac_capitalPagado	 	= ((Double)datSirac.get(0)).doubleValue();
					sirac_interesPagado		= ((Double)datSirac.get(1)).doubleValue();
					sirac_interesMPagado	= ((Double)datSirac.get(2)).doubleValue();
					sirac_diferenciaTot		= ((Double)datSirac.get(3)).doubleValue();
				}//if(datSirac.size()>0)
				else {
					infoSirac = "I";
				}
				
				this.setRegistrosProcesados(++numRegistrosProcesadosPedidos);
				
				setMonitorCob(rs_numPrestamo, "2", rs_epo, rs_pyme, rs_if, rs_pedido, rs_numPedido, rs_moneda, rs_fecha_inicio_cbr,
						Double.parseDouble(rs_saldo_credito),	sirac_capitalPagado,
						Double.parseDouble(rs_saldo_interes),	sirac_interesPagado,
						Double.parseDouble(rs_saldo_interesM),	sirac_interesMPagado, 
						saldoTotalNE, 							sirac_diferenciaTot, infoSirac, con);

			}//while(rs.next())
			rs.close();
			ps.close();
		} catch(Exception e) { 
			bOk = false;
			System.out.println("::procesoValidaSiracPedidos(Exception) "+e);
			e.printStackTrace();
		} finally { 
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("::procesoValidaSiracPedidos(S)");
		}
	}//procesoValidaSiracPedidos



	private void procesoValidaSiracCredicadenas() {
		System.out.println("::procesoValidaSiracCredicadenas(E)");
		String				qrySentencia 	= "";
		String 				condicion		= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		AccesoDB 			con				= null;
		boolean				bOk				= true;
		Vector				renglones		= new Vector();
		int numRegistrosProcesadosCredicadenas = this.getRegistrosProcesados();
		try {
			con = new AccesoDB();
			con.conexionDB();

			this.setPorcentajeProgreso(10);
			qrySentencia = 
				" SELECT /*+index(dis) index(sol) index(lc) use_nl(dis sol lc cif)*/"   +
				"         dis.cc_disposicion,"   +
				"         dis.ic_epo, dis.ic_pyme, sol.ig_numero_prestamo, "   +
				"         lc.ic_moneda, "   +
				"         NVL(dis.fn_capital_pagado,0) as fn_capital_pagado, NVL(dis.fn_interes_pagado,0) as fn_interes_pagado, NVL(dis.fn_interesm_pagado,0) as fn_interesm_pagado,"   +
				"         TO_CHAR (sol.df_operacion, 'dd/mm/yyyy') AS df_operacion"   +
				"         ,lc.ic_if "   +
				"         ,NVL(dis.fn_monto_credito,0) - NVL(dis.fn_capital_pagado,0) as saldoCredito"   +
				"         ,NVL(dis.fn_interes_generado,0) - NVL(dis.fn_interes_pagado,0) as saldoInteres"   +
				"         ,NVL(dis.fn_interesm_generado,0) - NVL(dis.fn_interesm_pagado,0) as saldoInteresM "   +
				"         ,to_char(dis.df_inicio_cobro, 'dd/mm/yyyy') as fechainiciocobr"   +
				"   FROM inv_disposicion dis,"   +
				"        inv_solicitud sol,"   +
				"        com_linea_credito lc,"   +
				"        comcat_if cif"   +
				"  WHERE dis.cc_disposicion = sol.cc_disposicion"   +
				"    AND dis.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND sol.ig_numero_prestamo IS NOT NULL"   +
				"    AND NOT EXISTS (SELECT 1"   +
				"                      FROM com_monitor_cob"   +
				"                     WHERE ig_numero_prestamo = sol.ig_numero_prestamo)"   +
				"    AND cif.ig_tipo_piso = 1"  ;

			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			this.setPorcentajeProgreso(60);
			while(rs.next()){
				String estatus = "";
				String rs_disp	 			= rs.getString("cc_disposicion")==null?"":rs.getString("cc_disposicion");
				String rs_epo 				= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String rs_pyme	 			= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_numPrestamo 		= rs.getString("ig_numero_prestamo")==null?"":rs.getString("ig_numero_prestamo");
				String rs_moneda 			= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String rs_fechaOperacion 	= rs.getString("df_operacion")==null?"":rs.getString("df_operacion");
				String rs_if			 	= rs.getString("ic_if")==null?"":rs.getString("ic_if");
				String rs_saldo_credito		= rs.getString("saldoCredito")==null?"0":rs.getString("saldoCredito");
				String rs_saldo_interes		= rs.getString("saldoInteres")==null?"0":rs.getString("saldoInteres");
				String rs_saldo_interesM	= rs.getString("saldoInteresM")==null?"0":rs.getString("saldoInteresM");
				String rs_fecha_inicio_cbr	= rs.getString("fechainiciocobr")==null?"":rs.getString("fechainiciocobr");

				double rs_capitalPagado	 	= rs.getDouble("fn_capital_pagado");
				double rs_interesPagado		= rs.getDouble("fn_interes_pagado");
				double rs_interesMPagado	= rs.getDouble("fn_interesm_pagado");
//				double rs_diferenciaTot		= rs_capitalPagado + rs_interesPagado + rs_interesMPagado;

				double saldoTotalNE				= Double.parseDouble(rs_saldo_credito)+Double.parseDouble(rs_saldo_interes)+Double.parseDouble(rs_saldo_interesM);
				
				Vector datSirac = getDatosSirac(rs_numPrestamo, con);
				
				double sirac_capitalPagado	 	= 0;
				double sirac_interesPagado		= 0;
				double sirac_interesMPagado		= 0;
				double sirac_diferenciaTot		= 0;
				String infoSirac				= "";

				if(datSirac.size()>0) {
					sirac_capitalPagado	 	= ((Double)datSirac.get(0)).doubleValue();
					sirac_interesPagado		= ((Double)datSirac.get(1)).doubleValue();
					sirac_interesMPagado	= ((Double)datSirac.get(2)).doubleValue();
					sirac_diferenciaTot		= ((Double)datSirac.get(3)).doubleValue();
				}//if(datSirac.size()>0)
				else {
					infoSirac = "I";
				}
				
				this.setRegistrosProcesados(++numRegistrosProcesadosCredicadenas);
				setMonitorCob(rs_numPrestamo, "5", rs_epo, rs_pyme, rs_if, "NULL", rs_disp, rs_moneda, rs_fecha_inicio_cbr, 
						Double.parseDouble(rs_saldo_credito),	sirac_capitalPagado,
						Double.parseDouble(rs_saldo_interes),	sirac_interesPagado,
						Double.parseDouble(rs_saldo_interesM),	sirac_interesMPagado, 				
						saldoTotalNE, 							sirac_diferenciaTot, infoSirac, con);
				
			}//while(rs.next())
			rs.close();
			ps.close();
		} catch(Exception e) { 
			bOk = false;
			System.out.println("::procesoValidaSiracCredicadenas(Exception) "+e);
			e.printStackTrace();
		} finally { 
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("::procesoValidaSiracCredicadenas(S)");
		}
	}//procesoValidaSiracPedidos
}
