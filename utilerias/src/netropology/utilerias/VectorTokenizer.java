package netropology.utilerias;

import java.util.Vector;

public class VectorTokenizer {
	String linea;
	String delimitador;

	public VectorTokenizer() {}

	public VectorTokenizer(String line, String delimit) {
	this.linea=line;
	this.delimitador=delimit;
	}

	
	
	/**
	 * Obtiene un vector con los elementos obtenidos de dividir
	 * la cadena contenida en "linea" utilizando la cadena "delimitador"
	 * @return Vector con los elementos obtenidos.
	 * 		En caso de que la cadena "linea" no contenga el delimitador, 
	 * 		se regresa la cadena original en el elemento 0 del vector.
	 * 
	 *		En el caso de que la cadena "linea" este conformada �nicamente por
	 * 		espacios en blanco o sea una cadena vacia, el vector obtenido sera 
	 * 		un vector vacio, m�s nunca null.
	 */
	public Vector getValuesVector() {
		String elem="", cad="", Linea=this.linea, Separador=this.delimitador; 
		int possIni=0, possFin=0, countokens=0;
		Vector vec=new Vector();
        
		if (Linea.indexOf(Separador) == -1) { //No se encontro el delimitador en la cadena
			if (!Linea.trim().equals("")) {	//La cadena no es vacia
				vec.addElement(Linea);	//Guarda la cadena original
			}
		} else {
			for(int i=0; i<Linea.length(); i++) {
				elem=Linea.substring(i,i+1);
				if(elem.equals(Separador)) {
					possIni=(countokens==0)?0:possFin;
					possFin=i+1;
					countokens++;
					cad=Linea.substring(possIni, possFin-1);
					//System.out.println(cad);
					vec.addElement(cad);
				}
			} //for
			String Delimit1=Linea.substring(possFin-1,possFin);
			String Delimit2=Linea.substring(Linea.length()-1,Linea.length());
			if((Delimit1.equals(Separador)) && (!Delimit2.equals(Separador))) {
				if(possFin<Linea.length())
					vec.addElement(Linea.substring(possFin,Linea.length()));
			} else if (Delimit2.equals(Separador)) {
				vec.addElement("");
			}
		}
		return vec;
	}
} //class
