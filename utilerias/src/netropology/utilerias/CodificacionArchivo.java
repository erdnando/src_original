package netropology.utilerias;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;

import java.util.Enumeration;

import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.logging.Log;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.apache.tika.parser.txt.UniversalEncodingDetector;
import org.apache.tika.utils.CharsetUtils;

/**
 *
 * Clase que permite validar si un archivo proporcionado cumple con la codificaciones
 * permitidas: ISO-8859-1 y windows-1252.
 * <br>
 * Se usa en pantallas espec�ficas del m�dulo de Descuento Electr�nico
 * y de Distribuidores.
 * <br>
 * Debido a la librer�a que se usa para detectar la codificaci�n (Apache Tika), s�lo se toman en cuenta
 * los primeros 12 KB.
 * <br>
 * S�lo se realiza la validaci�n si el archivo proporcionado cumple con alguna de las siguientes
 * extensiones: <tt>txt</tt> y/o <tt>zip</tt>.
 * <br>
 *   -> Por default s�lo se realiza la b�squeda en archivos (parametro <tt>rutaArchivo</tt>),
 *   cuya extensi�n es <tt>txt</tt>.<br>
 *   Los archivos ZIP no se procesan por default, por lo que se deber� habilitar la bandera <tt>processZip</tt>
 *   para que se analice la codificaci�n.
 * <br>
 *  NOTA: EN CASO DE RECIBIR UN ARCHIVO ZIP, SE ASUME QUE ESTE S�LO TRAE UN ARCHIVO COMPRIMIDO. POR LO
 *  QUE NO SE REPORTARAN CODIFICACIONES NO PERMITIDAS DE ARCHIVOS POSTERIORES AL PRIMERO.
 * <br>
 * IMPORTANTE:<br>
 *   COMO S�LO SE REALIZA LA VALIDACI�N SI EL ARCHIVO PROPORCIONADO CUMPLE CON ALGUNA DE LAS SIGUIENTES
 *   EXTENSIONES: <tt>TXT</tt> Y/O <tt>ZIP</tt> (ADEM�S, EN EL CASO DE QUE SE VALIDEN ARCHIVOS <tt>ZIP</tt>, LA EXTENSI�N DEL
 *   ARCHIVO COMPRIMIDO DEBE SER <tt>TXT</tt>), REVISAR QUE EN DONDE SE OCUPE ESTA CLASE, EN LA SECCION DE LAS VALIDACIONES
 *   PROPIAS DE LA APLICACION, SE VALIDE QUE EL ARCHIVO VENGA CON LA EXTENSION CORRECTA: SI SE ENV�A UN <tt>ZIP</tt>, ADEM�S,
 *    SE DEBER� VERIFICAR QUE SE VALIDE EL FORMATO DEL ARCHIVO COMPRIMIDO, COMO POR EJEMPLO:
 *	  <code>
 *			rutaArchivo = ComunesZIP.descomprimirArchivoSimple(fis, strDirectorioTemp);
 *   		if ( rutaArchivo == null || rutaArchivo.equals("") || !"txt".equals(Comunes.getExtensionNombreArchivo(rutaArchivo)) ) {
 *				ok_file = false;	
 *			}
 *   </code>
 *   PARA RECORDAR AL PROGRAMADOR QUE DEBE REVISAR QUE SE REALICEN ESTAS VALIDACIONES, SE CRE� EL ATRIBUTO
 *   <tt>validacionPosteriorExtension</tt>, EL CUAL DEBE PONER EN <tt>true</tt> (PARA EVITAR QUE SE LANCEN EXCEPCIONES
 *   CUANDO SE RECIBA ARCHIVO CON UNA EXTENSION INVALIDA) Y CON EL PROPOSITO DE QUE ASUMA EL COMPROMISO DE QUE EN
 *   LA APLICACI�N ORIGINAL SE VALIDEN LAS EXTENSIONES PERMITIDAS.
 * <br>
 *
 * @author jshernandez
 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 25/06/2015 12:21:30 p.m.
 *
 */
public class CodificacionArchivo {

	// Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CodificacionArchivo.class);
	
	private final static String[]  	CODIFICACIONES_PERMITIDAS	= { "(?i)ISO-8859-[1-9]", "(?i)ISO-8859-1[0-6]", "(?i)windows-125[0-8]" };

	private boolean 						processZip						= false;
	private boolean 						processTxt						= true;
	private boolean 						doProcessFile					= false;
	
	private boolean						validacionPosteriorExtension = true;	// Para forzar al programador a implementar la validaci�n 
																									// de extensi�n de archivos y entonces poner en true
																									// este atributo.
	
	/**
	 * Detecta si la codificaci�n de un archivo se encuentra dentro de las codificaciones permitidas:
	 *  [ "ISO-8859-1", "windows-1252" ]
	 * <br>
	 * Se usa en pantallas espec�ficas del m�dulo de Descuento Electr�nico y de Distribuidores.
	 * <br>
	 * Debido a la librer�a que se usa para detectar la codificaci�n (Apache Tika), s�lo se toman en cuenta
	 * los primeros 16 KB.
	 * <br>
	 * S�lo se realiza la validaci�n si el archivo proporcionado cumple con alguna de las siguientes
	 * extensiones: <tt>txt</tt> y/o <tt>zip</tt>.
	 * <br>
	 *   -> Por default s�lo se realiza la b�squeda en archivos (parametro <tt>rutaArchivo</tt>), 
	 *   cuya extensi�n es <tt>txt</tt>.<br> 
	 *   Los archivos ZIP no se procesan por default, por lo que se deber� habilitar la bandera <tt>processZip</tt>
	 *   para que se analice la codificaci�n.
	 * <br>
	 *  NOTA: EN CASO DE RECIBIR UN ARCHIVO ZIP, SE ASUME QUE ESTE S�LO TRAE UN ARCHIVO COMPRIMIDO. POR LO
	 *  QUE NO SE REPORTARAN CODIFICACIONES NO PERMITIDAS DE ARCHIVOS POSTERIORES AL PRIMERO.
	 * <br>
	 * IMPORTANTE:<br>
	 *   COMO S�LO SE REALIZA LA VALIDACI�N SI EL ARCHIVO PROPORCIONADO CUMPLE CON ALGUNA DE LAS SIGUIENTES
	 *   EXTENSIONES: <tt>TXT</tt> Y/O <tt>ZIP</tt> (ADEM�S, EN EL CASO DE QUE SE VALIDEN ARCHIVOS <tt>ZIP</tt>, LA EXTENSI�N DEL 
	 *   ARCHIVO COMPRIMIDO DEBE SER <tt>TXT</tt>), REVISAR QUE EN DONDE SE OCUPE ESTA CLASE, EN LA SECCION DE LAS VALIDACIONES 
	 *   PROPIAS DE LA APLICACION, SE VALIDE QUE EL ARCHIVO VENGA CON LA EXTENSION CORRECTA: SI SE ENV�A UN <tt>ZIP</tt>, ADEM�S, 
	*    SE DEBER� VERIFICAR QUE SE VALIDE EL FORMATO DEL ARCHIVO COMPRIMIDO, COMO POR EJEMPLO:
	 *	  <code>
	 *			rutaArchivo = ComunesZIP.descomprimirArchivoSimple(fis, strDirectorioTemp);
	 *   		if ( rutaArchivo == null || rutaArchivo.equals("") || !"txt".equals(Comunes.getExtensionNombreArchivo(rutaArchivo)) ) {
	 *				ok_file = false;	
	 *			}
	 *   </code>
	 *   PARA RECORDAR AL PROGRAMADOR QUE DEBE REVISAR QUE SE REALICEN ESTAS VALIDACIONES, SE CRE� EL ATRIBUTO
	 *   <tt>validacionPosteriorExtension</tt>, EL CUAL DEBE PONER EN <tt>true</tt> (PARA EVITAR QUE SE LANCEN EXCEPCIONES
	 *   CUANDO SE RECIBA ARCHIVO CON UNA EXTENSION INVALIDA) Y CON EL PROPOSITO DE QUE ASUMA EL COMPROMISO DE QUE EN 
	 *   LA APLICACI�N ORIGINAL SE VALIDEN LAS EXTENSIONES PERMITIDAS.
	 * <br>
	 *
	 * @author jshernandez
	 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 25/06/2015 12:21:30 p.m.
	 *
	 */
	public boolean esCharsetNoSoportado(String rutaArchivo, String[] codificacionDetectada )
		throws Exception {
		
		log.info("esCharsetNoSoportado(E)");
		
		File 						archivo				= null;
		
		ZipFile 					zipFile				= null;
		ZipArchiveEntry 		zipArchiveEntry 	= null;
		BufferedInputStream 	inputStream			= null;
		
		boolean					esArchivoZip 		= false;
		String 					nombreArchivo		= null;
		try {
			
			// DETERMINAR SI SE TIENE PERMITIDO PROCESAR EL ARCHIVO
			
			// Si la extension del archivo proporcionado no se encuentra dentro de las permitidas, 
			// no hacer nada.
			
			if(        rutaArchivo != null && processZip && rutaArchivo.matches("(?i)^.+\\.zip$") ){
				doProcessFile = true;
			} else if( rutaArchivo != null && processTxt && rutaArchivo.matches("(?i)^.+\\.txt$") ){
				doProcessFile = true;
			}
			if( rutaArchivo != null && !doProcessFile ){
				
				if( !validacionPosteriorExtension ){
					log.error("esCharsetNoSoportado(): La extensi�n del archivo proporcionado: " + rutaArchivo + ", no es v�lida.");
					throw new IllegalArgumentException("La extensi�n del archivo proporcionado: " + rutaArchivo + ", no es v�lida.");
				} else {
					log.info("esCharsetNoSoportado(): El archivo proporcionado: " + rutaArchivo + " no podr� ser procesado, porque su extension no est� dentro de las permitidas.");
					if( codificacionDetectada != null && codificacionDetectada.length > 0 ){
						codificacionDetectada[0] = null; // Si el archivo tiene formato no soportado, no se env�a ninguna codificaci�n.
					}
					return false; // Se dejo pasar al archivo ya que el formato no est� soportado.
				}
				
			}
			
			// Validar que el archivo proporcionado exista.
			archivo = !Comunes.esVacio(rutaArchivo) ? new File(rutaArchivo): null;
			if( archivo != null && !archivo.exists() ){
				throw new FileNotFoundException();
			}
			
			// DETERMINAR SI EL ARCHIVO PROPORCIONADO ES UN ARCHIVO ZIP 
			esArchivoZip = !Comunes.esVacio(rutaArchivo) && rutaArchivo.matches("(?i)^.+\\.zip$")?true:false;
			
			// Si es archivo ZIP inicializar zipArchiveEntry
			if(esArchivoZip){
				
				Enumeration<ZipArchiveEntry> entries = null;
				
				zipFile 				= new ZipFile( rutaArchivo );
				entries 				= (Enumeration<ZipArchiveEntry>) zipFile.getEntries();
				
				zipArchiveEntry 	= entries.hasMoreElements()?entries.nextElement():null;
				
			} 
			
			// VALIDAR ARGUMENTOS DE ENTRADA
			//archivo = new File(rutaArchivo);
			
			// Validar que se haya proporcionado el atributo ruta archivo.
			if( Comunes.esVacio(rutaArchivo) ){
				log.error("esCharsetNoSoportado(): El atributo 'rutaArchivo' es requerido.");
				throw new IllegalArgumentException("El atributo 'rutaArchivo' es requerido.");
			} else if ( esArchivoZip && zipArchiveEntry == null  ){
				log.info("esCharsetNoSoportado(): El archivo zip proporcionado no tiene nada.");
				return false; // Se dejo pasar al archivo zip proporcionado porque no tiene nada.
			} 
			
		   if( esArchivoZip && zipArchiveEntry != null && zipArchiveEntry.getSize() == 0L ){
		      log.info("esCharsetNoSoportado(): El archivo zip proporcionado no tiene nada.");
		      if( codificacionDetectada != null && codificacionDetectada.length > 0 ){
					codificacionDetectada[0] = null; // Enviar Nombre de la codificaci�n detectada
		      }
				return false;
		   } else if( archivo.length() == 0L ){
		      log.info("esCharsetNoSoportado(): El archivo txt proporcionado no tiene nada.");
		      if( codificacionDetectada != null && codificacionDetectada.length > 0 ){
		         codificacionDetectada[0] = null; // Enviar Nombre de la codificaci�n detectada
		      }
				return false;
		   }
			
			// VALIDAR EL FORMATO DEL ARCHIVO PROPORCIONADO
			
			// Obtener nombre del archivo
			if( esArchivoZip && zipArchiveEntry != null ){
				nombreArchivo = zipArchiveEntry.getName();
			} else {
				nombreArchivo = archivo.getName();
			}
			
			if( esArchivoZip && !nombreArchivo.matches("(?i)^.+\\.txt$") ){
				if( !validacionPosteriorExtension ){
					log.error("esCharsetNoSoportado(): La extensi�n del archivo comprimido: "+nombreArchivo+", debe ser txt.");
					throw new IllegalArgumentException("La extensi�n del archivo comprimido: "+nombreArchivo+", debe ser txt.");
				} else {	
					log.info("esCharsetNoSoportado(): El archivo comprimido proporcionado: " + rutaArchivo + " no podr� ser validado, porque su extensi�n no est� dentro de las permitidas.");
					return false; // Se dejo pasar al archivo ya que el formato no est� soportado.
				}
			}
			
			// ADIVINAR CODIFICACI�N DEL ARCHIVO
			
			// Obtener InputStreamReader
			if(	esArchivoZip  ){
				inputStream 	= 	new BufferedInputStream( 
											zipFile.getInputStream(zipArchiveEntry)
										);
			} else {
				inputStream 	= 	new BufferedInputStream( 
											new FileInputStream( rutaArchivo )
										);
			}

			Charset	charset  	= null;
			String 	charsetName = null;
			
			// Utilizar UniversalEncodingDetector
			
			UniversalEncodingDetector 	universalEncodingDetector 	= new UniversalEncodingDetector();
			charset  															= universalEncodingDetector.detect(inputStream, new Metadata());
			if( charset != null ){
				charsetName = charset.name();
			}
			
			// Utilizar Icu4jEncodingDetector si la detecci�n anterior no fue exitosa.
			
			if( charsetName == null ){
				
				CharsetDetector detector = new CharsetDetector();
				detector.enableInputFilter(false); // Asumir que no se trata de un archivo HTML, XML, etc.
				detector.setText(inputStream);
							
				for (CharsetMatch match: detector.detectAll()) {
					try {
						charset 		= CharsetUtils.forName(match.getName());
						charsetName = charset.name();
						break; // Se toma el primero que haga match exitoso
					} catch (Exception e) {
						e.printStackTrace();
						charsetName = null;
					}
				}
				
			}

			if( charsetName == null ){
				charsetName = "DESCONOCIDO";
			}
			if( codificacionDetectada != null && codificacionDetectada.length > 0 ){
				codificacionDetectada[0] = charsetName; // Enviar Nombre de la codificaci�n detectada
			}
				
			boolean invalido = true;
			for(int i=0; charsetName != null && i < CODIFICACIONES_PERMITIDAS.length ; i++){
				if( charsetName.matches(CODIFICACIONES_PERMITIDAS[i]) ){
					invalido = false;
					break;
				}
			}
			if(invalido){
				return true;
			}
			
		} catch(Exception e){
			
			log.error("esCharsetNoSoportado(Exception)");
			log.error("esCharsetNoSoportado.rutaArchivo                  = <" + rutaArchivo                  + ">");
			log.error("esCharsetNoSoportado.codificacion                 = <" + codificacionDetectada        + ">");
			log.error("esCharsetNoSoportado.validacionPosteriorExtension = <" + validacionPosteriorExtension + ">");
			log.error("esCharsetNoSoportado(Exception):",e);
			
			throw e;
			
		} finally {
			
			if( inputStream	!= null) { try { inputStream.close();	}catch(Exception e){} }
      	if( zipFile 		!= null) { try { zipFile.close(); 		}catch(Exception e){} }
      	
      	log.info("esCharsetNoSoportado(S)");
      	
		}
		
		return false;
	}
	
	//.......................................................
	
	public boolean getProcessZip() {
		return processZip;
	}

	public void setProcessZip(boolean processZip) {
		this.processZip = processZip;
	}
	
	public boolean getProcessTxt() {
		return processTxt;
	}

	public void setProcessTxt(boolean processTxt) {
		this.processTxt = processTxt;
	}

	public void setValidacionPosteriorExtension(boolean validacionPosteriorExtension) {
		this.validacionPosteriorExtension = validacionPosteriorExtension;
	}
	
}

