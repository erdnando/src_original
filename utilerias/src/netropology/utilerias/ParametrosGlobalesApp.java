package netropology.utilerias;

import java.io.IOException;

import java.util.Properties;

/**
 * Esta clase tiene por objeto mantener el valor de los parámetros
 * Globales de la Aplicación, los cuales están enfocados básicamente
 * a evitar cierta funcionalidad para fines de desarrollo.
 *
 * Implementada como singleton. Su uso requiere obtener la instancia
 * mediante ParametrosGlobalesApp.getInstance();
 */
public class ParametrosGlobalesApp implements java.io.Serializable {
	private static ParametrosGlobalesApp singleton = null;
	private static Properties parametros = null;
	
	private ParametrosGlobalesApp() {}
	
	public static synchronized ParametrosGlobalesApp getInstance() {
		 if (singleton == null ) {
			 singleton = new ParametrosGlobalesApp();
		 }
		 return singleton;
	}
	
	/**
	 * Obtiene el valor del parámetro especificado.
	 * Si el parámetro no existe regresa una cadena vacia
	 * @return Cadena con el valor del parámetro
	 * @param clave Clave del parametro como esta 
	 * 		definido en ParametrosGlobalesApp.properties
	 */
	public String getParametro(String clave) {
		if (parametros == null) {
			try {
				parametros = Comunes.loadParams("ParametrosGlobalesApp");
			} catch(IOException e) {
				//Se ignora el error y se crea un Porperties vacio
				parametros = new Properties();
			}
		}
		//si el valor no se encuentra, se regresa una cadena vacia
		String valorParametro = parametros.getProperty(clave, "");
		return valorParametro;
	}
}