/*************************************************
 *Title:		Utiler�as
 *Version:		1.0
 *Copyright:	Copyright (c) 2001
 *Author:		Varios :-)
 *Company:		Netropology.
 *Description:	Clase de acceso y ejecuci�n de SQL.
 *************************************************/
package netropology.utilerias;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;

/**
 * Esta clase se desarrollo debido a que las Interfases de Nafin Electr�nico
 * tenian problemas al usar la conexi�n a base de datos a trav�s del pool
 * de conexiones.
 * Porque cada ciclo se iban incrementando las conexiones y no se cerraban
 * llegando a un punto donde no se podia realizar ninguna conexion a la base.
 * @author Gilberto Aparicio
 * @since Migracion a iAS 10g

 */
public class AccesoDBOracle {
	
    private final static Log log = ServiceLocator.getInstance().getLog(AccesoDBOracle.class);
    
	public AccesoDBOracle() {}//AccesoDBOracle()

	private boolean showMessages = true;
	
	/**
	 * Deshabilita el envio de mensajes a la consola para los siguientes metodos:
	 * conexionDB(), consultarDB(), cierraConexionDB() y terminaTransaccion()
	 */
	public void enableMessages(){
		this.showMessages = true;
	}
	
	/**
	 * Habilita el envio de mensajes a la consola para los siguientes metodos:
	 * conexionDB(), consultarDB(), cierraConexionDB() y terminaTransaccion()
	 */
	public void disableMessages(){
		this.showMessages = false;
	}
	
	/**
	 * El m�todo establece una conexi�n a la base de datos
	 * La conexion s�lo se establece si no hay una actualmente
	 * abierta.
	 */
	public void conexionDB() throws SQLException {
		if(this.showMessages) log.info("AccesoDBOracle::conexionDB(E)");
		//La siguiente condici�n es para evitar que se abra 
		//una conexion estando una ya abierta
		//lo cual genera problema de "Leaked Connections".
		if (m_conn != null && !m_conn.isClosed()) {
			if(this.showMessages)  log.info("AccesoDBOracle::conexionDB()::ERROR. Ya hay una conexion Activa");
		} else {
			String ip = AccesoDBParam.getParametro("ORACLE_IP");
			String puerto = AccesoDBParam.getParametro("ORACLE_PORT");
			String servicio = AccesoDBParam.getParametro("ORACLE_SERVICE");
			String usuario = AccesoDBParam.getParametro("ORACLE_USER");
			String password = AccesoDBParam.getParametro("ORACLE_PASSWORD");
			
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			this.m_conn = DriverManager.getConnection ("jdbc:oracle:thin:@" + ip +
					":" + puerto + ":" + servicio, usuario, password);
			this.m_conn.setAutoCommit(false);
			this.m_conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		}

		if(this.showMessages) log.info("AccesoDBOracle::conexionDB(S)");
	}//conexionDB

	/**
	 * El m�todo establece una conexi�n a la base de datos
	 * La conexion s�lo se establece si no hay una actualmente
	 * abierta.
	 * @param user Usuario de la Base de Datos
	 * @param passwd Contrase�a del Usuario para conexion a la Base de Datos
	 */
	public void conexionDB(String user, String passwd) throws SQLException {
		if(this.showMessages) log.info("AccesoDBOracle::conexionDB(E)");
		//La siguiente condici�n es para evitar que se abra 
		//una conexion estando una ya abierta
		//lo cual genera problema de "Leaked Connections".
		if (m_conn != null && !m_conn.isClosed()) {
			if(this.showMessages) log.info("AccesoDBOracle::conexionDB()::ERROR. Ya hay una conexion Activa");
		} else {
			String ip = AccesoDBParam.getParametro("ORACLE_IP");
			String puerto = AccesoDBParam.getParametro("ORACLE_PORT");
			String servicio = AccesoDBParam.getParametro("ORACLE_SERVICE");
			String usuario = ((user!=null)?user:AccesoDBParam.getParametro("ORACLE_USER"));
			String password =((passwd!=null)?passwd:AccesoDBParam.getParametro("ORACLE_PASSWORD"));
			
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			this.m_conn = DriverManager.getConnection ("jdbc:oracle:thin:@" + ip +
					":" + puerto + ":" + servicio, usuario, password);
			this.m_conn.setAutoCommit(false);
			this.m_conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		}

		if(this.showMessages) log.info("AccesoDBOracle::conexionDB(S)");
	}//conexionDB
	
	public PreparedStatement queryPrecompilado(String qrySentencia, List lVarBind, int numMaxRegistros) throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = m_conn.prepareStatement(qrySentencia);
			for(int i=0;i<lVarBind.size();i++) {
				Object object = lVarBind.get(i);
				String contenido = object.toString().trim();
				if(object instanceof String) {
					ps.setString(i+1, contenido);
				} else if(object instanceof Integer) {
					ps.setInt(i+1, new Integer(contenido).intValue());
				} else if(object instanceof Long) {
					ps.setLong(i+1, new Long(contenido).longValue());
				} else if(object instanceof Double) {
					ps.setDouble(i+1, new Double(contenido).doubleValue());
				} else if(object instanceof BigDecimal) {
					ps.setBigDecimal(i+1, new BigDecimal(contenido));
				} else if(object instanceof java.util.Calendar) {
					ps.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Calendar)object).getTime().getTime()), Calendar.getInstance());
				} else if(object instanceof java.util.Date) {
					ps.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Date)object).getTime()), Calendar.getInstance());
				} else if(object instanceof Fecha) {
					ps.setNull(i+1, Types.TIMESTAMP);
				} else if(object instanceof Class) {
					if("int".equals(contenido)) {
						ps.setNull(i+1, Types.INTEGER);
					} else if("long".equals(contenido)) {
						ps.setNull(i+1, Types.BIGINT);
					} else if("double".equals(contenido)) {
						ps.setNull(i+1, Types.DOUBLE);
					}					
				}
			}//for(int i=0;i<lVarBind.size();i++)
			if(numMaxRegistros>0) {
				ps.setMaxRows(numMaxRegistros);
			}
		} catch (SQLException sql) {
			if(ps!=null)
				ps.close();
			log.info("AccesoDBOracle::queryPrecompilado(SQLException) "+sql);
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.info("qrySentencia:"+qrySentencia);
			log.info("lVarBind:"+lVarBind);			
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			sql.printStackTrace();
			throw sql;
		} catch (Exception e) {
			if(ps!=null)
				ps.close();
			log.info("AccesoDBOracle::queryPrecompilado(Exception) "+e);
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.info("qrySentencia:"+qrySentencia);
			log.info("lVarBind:"+lVarBind);			
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
		}
		return ps;
	}

	public PreparedStatement queryPrecompilado(String query, List lVarBind) throws SQLException {
		return queryPrecompilado(query, lVarBind, 0);
	}

	public PreparedStatement queryPrecompilado(String query) throws SQLException {
		return queryPrecompilado(query, new ArrayList());
	}

	//////////////////////////////////////////////////
	// Ejecuta cualquier SQL dinamicamente
	//////////////////////////////////////////////////
	public CallableStatement ejecutaSP(String nombre_sp) throws SQLException {
		CallableStatement cstm=null;
		try {
			cstm = m_conn.prepareCall("{call " + nombre_sp + "}");
		} catch (SQLException sqle) {
			error.append("AccesoDBOracle.ejecutaSP(SQLException): " + sqle.getMessage() + " Error en el Store Procedure: " + nombre_sp);
			log.info(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
		return cstm;
	}

	//////////////////////////////////////////////////
	// Da commit o rollback a una transacci�n
	//////////////////////////////////////////////////
	public void terminaTransaccion(boolean todo_ok) {
		try {
			if (todo_ok) {
				m_conn.commit();
				if(this.showMessages) log.info("terminaTransaccion(). Commit");
			}
			else {
				m_conn.rollback();
				if(this.showMessages) log.info("terminaTransaccion(). Rollback");
			}
		}
		catch (SQLException sqle) {
			error.append("AccesoDBOracle.terminaTransaccion(SQLException): " + sqle.getMessage() );
			log.info(mostrarError());
			sqle.printStackTrace();
		}
	}

	/* Regresa la Instancia de la Conexion */
	public Connection getConnection() {
		return this.m_conn;
	}

	/* Guarda Cualquier Mensaje de Error de Base de Datos. */
	public String mostrarError() {
		String msg_error = this.error.toString();
		this.error.delete(0,this.error.length());
		return msg_error;
	}

	//////////////////////////////////////////////////
	// Cierra la conexion a la base de datos
	//////////////////////////////////////////////////
	public void cierraConexionDB() {
		try {
			if (stm != null) {
				//Llamar close en un statement previamente cerrado no genera ningun error,
				//por lo cual se llama el close() por si no hubiera sido cerrada anteriormente.
				stm.close();
			}
			if (m_conn != null)	{
				m_conn.close();
				if(this.showMessages) log.info("AccesoDBOracle::cierraConexion()");
			} else {
				if(this.showMessages) log.info("cierraConexion(). No existe ninguna conexion.");
			}
		} catch (SQLException sqle) {
			error.append("AccesoDBOracle.terminaTransaccion(SQLException): " + sqle.getMessage() );
			log.info(mostrarError());
			sqle.printStackTrace();
	 	}
		m_conn = null;
	}

	public void cierraStatement() throws SQLException {
		try {
			if (stm != null) {
				stm.close();
			}
		}
		catch(SQLException sqle) {
			error.append("AccesoDBOracle.cierraStatement(SQLException): " + sqle.getMessage() );
			log.info(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}

// El m�todo indica si hay una conexion abierta o no.
	public boolean hayConexionAbierta() {
		if (m_conn != null) {
			return true;
		} else {
			return false;
		}
	}


	public Registros consultarDB(String qrySentencia, List lVarBind, boolean bGetDataType, int numMaxRegistros) throws Exception{
		if(this.showMessages) log.info("AccesoDBOracle::consultarDB(E)");
		PreparedStatement 	ps 			= null;
		ResultSet			rs			= null;
		ResultSetMetaData	rsMD		= null;	
		List				nombres		= new ArrayList();
		List				renglones	= new ArrayList();
		List				columnas	= null;
		Registros			registros	= new Registros();
		try {
			ps = queryPrecompilado(qrySentencia, lVarBind, numMaxRegistros);
			rs = ps.executeQuery();
			ps.clearParameters();
			rsMD = rs.getMetaData();
			int numCols = rsMD.getColumnCount();
			int cont = 0;
			while(rs.next()) {
				columnas = new ArrayList();
				for(int i=1;i<=numCols;i++) {
					if(cont==0) {
						nombres.add(rsMD.getColumnName(i));
					}//if(cont==0)
					if(bGetDataType) {
						int colType = rsMD.getColumnType(i);
						switch(colType) {
							case Types.NUMERIC:
							case Types.DECIMAL:
							case Types.INTEGER:
								BigDecimal rs_numeric = rs.getBigDecimal(i)==null?new BigDecimal("0"):rs.getBigDecimal(i);
								columnas.add(rs_numeric.toPlainString());
								break;
							case Types.DATE:
							case Types.TIME:
							case Types.TIMESTAMP:
								Timestamp rs_fecha = rs.getTimestamp(i);
								columnas.add(rs_fecha);
								break;
							default:
								String rs_campo = rs.getString(i)==null?"":rs.getString(i);
								columnas.add(rs_campo);
								break;
						}//switch(colType)
					} else {
						String rs_campo = rs.getString(i)==null?"":rs.getString(i);
						columnas.add(rs_campo);
					}
				}//for(i=1;i<=numCols;i++)
				renglones.add(columnas);
				cont++;
			}//while(rs.next())
			if(ps!=null)
				ps.close();	
			if(rs!=null)
				rs.close();
			registros.setNombreColumnas(nombres);
			registros.setData(renglones);
		} catch(Exception e) { 
			if(ps!=null)
				ps.close();	
			if(rs!=null)
				rs.close();
			log.info("AccesoDBOracle::consultarDB(Exception) "+e);
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.info("qrySentencia:"+qrySentencia);
			log.info("lVarBind:"+lVarBind);			
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
			throw e;
		} finally { 
			if(this.showMessages) log.info("AccesoDBOracle::consultarDB(S)");
		}
		return registros;
	}//consultarDB
	
	public Registros consultarDB(String qrySentencia, List lVarBind, boolean bGetDataType) throws Exception{
		return consultarDB(qrySentencia, lVarBind, bGetDataType, 0);
	}//consultaDB
	
	public Registros consultarDB(String qrySentencia, List lVarBind) throws Exception{
		return consultarDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	public Registros consultarDB(String qrySentencia) throws Exception{
		return consultarDB(qrySentencia, new ArrayList());
	}//consultaDB
	
	public List consultaDB(String qrySentencia, List lVarBind, boolean bGetDataType) throws Exception{
		Registros 	registros = consultarDB(qrySentencia, lVarBind, bGetDataType);
		List		renglones = (registros.getNumeroRegistros()==0)?new ArrayList():registros.getData();
		return renglones;
	}//consultaDB
	
	public List consultaDB(String qrySentencia, List lVarBind) throws Exception{
		return consultaDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	public List consultaDB(String qrySentencia) throws Exception{
		return consultaDB(qrySentencia, new ArrayList());
	}//consultaDB

	public List consultaRegDB(String qrySentencia, List lVarBind, boolean bGetDataType) throws Exception{
		List	renglones = consultaDB(qrySentencia, lVarBind, bGetDataType);
		if(renglones.size()>0) {
			renglones = (ArrayList)renglones.get(0);
		}
		return renglones;
	}//consultaDB
	
	public List consultaRegDB(String qrySentencia, List lVarBind) throws Exception{
		return consultaRegDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	public List consultaRegDB(String qrySentencia) throws Exception{
		return consultaRegDB(qrySentencia, new ArrayList());
	}//consultaDB
	
	public int existeDB(String qrySentencia, List lVarBind) throws Exception{
		int existe = 0;
		List renglones = consultaRegDB(qrySentencia, lVarBind, true);
		if(renglones.size()>0) {
			BigDecimal big = (BigDecimal)renglones.get(0);
			existe = big.intValue();
		}
		return existe;
	}//existeDB
	
	public int ejecutaUpdateDB(String qrySentencia, List lVarBind) throws Exception {
		log.info("AccesoDBOracle::ejecutaUpdateDB(E)");
		PreparedStatement 	ps				= null;
		int					registros		= 0;
		try {
			ps = queryPrecompilado(qrySentencia, lVarBind);
			registros = ps.executeUpdate();
			ps.close();
		} catch(Exception e) { 
			log.info("AccesoDBOracle::ejecutaUpdateDB(Exception) "+e);
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.info("qrySentencia:"+qrySentencia);
			log.info("lVarBind:"+lVarBind);			
			log.info("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
			throw e;
		} finally { 
			log.info("AccesoDBOracle::ejecutaUpdateDB(S)");
		}
		return registros;
	}//ejecutaQuery
	
	public int ejecutaUpdateDB(String qrySentencia) throws Exception{
		return ejecutaUpdateDB(qrySentencia, new ArrayList());
	}//ejecutaQuery

	/*METODO PARA EJECUTAR UN QUERY LLENADO LOS DATO EN UN VECTOR RJV 30052002*/
	public void ejecutarQueryDB(String sql, int inumcols, Vector svTabla) throws Exception, NullPointerException, SQLException {
		int liNumCols = 0;
		int	liTipoDato = 0;
		String lsTmp = "";
		Time ltmTmp = null;
		java.sql.Date ldtTmp = null;
		Vector lvRegistro = null;
		ResultSetMetaData rsMetaData = null;
		ResultSet lrsResultSet = null;

		String lsCampo = new String();
		Integer loiCampo ;
		int liCampo = 0;

		//log.info("\tAccesoDBOracle::EjecutarQueryBD");
		//log.info("\t\tSQL(" + sql + ")");

		Statement statement = m_conn.createStatement();

		lrsResultSet = statement.executeQuery(sql);

		rsMetaData = lrsResultSet.getMetaData();

		liNumCols = rsMetaData.getColumnCount() ;

		//log.info("\t Numero de Columnas(" + rsMetaData.getColumnCount() + ")");

		while (lrsResultSet.next()) {
			lvRegistro = new Vector();
			for (int i = 1; i <= liNumCols ; i++) {
				liTipoDato = rsMetaData.getColumnType(i);

				//log.info("\t Tipo de Dato (" + liTipoDato + ")");

				if (liTipoDato == CConst.TIPO_VARCHAR || liTipoDato == CConst.TIPO_CHAR){
					lsCampo = lrsResultSet.getString(i);
					if (lsCampo == null)
						lsCampo = "";
					lvRegistro.addElement(lsCampo.trim());
			}else if (liTipoDato == CConst.TIPO_FECHA){
					ldtTmp = lrsResultSet.getDate(i);
					ltmTmp = lrsResultSet.getTime(i);
					if (ldtTmp == null)
						lsCampo = "";
					else
						lsCampo = ldtTmp.toString() + " " + ltmTmp.toString();

					lvRegistro.addElement(lsCampo.trim());
				}else if(liTipoDato == CConst.TIPO_LONG || liTipoDato == CConst.TIPO_SMALLINT || liTipoDato == CConst.TIPO_NUMBER || liTipoDato == CConst.TIPO_RARO || liTipoDato == CConst.TIPO_RARO_2){
 					liCampo = lrsResultSet.getInt(i);
					loiCampo = new Integer(liCampo);
					lvRegistro.addElement(loiCampo);
				}

				if(liTipoDato == CConst.TIPO_LONG || liTipoDato == CConst.TIPO_NUMBER)
					System.out.print(" " + i + "[" + liCampo + "]");
				else
					System.out.print(" " + i + "(" + lsCampo + ")");
			}
			svTabla.addElement(lvRegistro);
			log.info("\n ");
		}

		statement.close();
		lrsResultSet.close();

	}

	//throws SQLException
	public void cerrarDB(boolean ebResultado) {
		//log.info("AccesoDBOracle::cerrarDB(E)");
		if (m_conn != null)	{
			try	{
				if (ebResultado)	m_conn.commit();
				else				m_conn.rollback();
			}catch(SQLException sqle)	{
				error.append("AccesoDBOracle.cierraStatement(SQLException): " + sqle.getMessage() );
				log.info(mostrarError());
				sqle.printStackTrace();
			}
			try	{
				m_conn.close();
			}catch(SQLException sqle)	{
				error.append("AccesoDBOracle.cierraStatement(SQLException): " + sqle.getMessage() );
				log.info(mostrarError());
				sqle.printStackTrace();
			}
		}
		//log.info("Acceso::cerrarDB(S)");
	}
	
	
	public ResultSet queryDB(String query) throws SQLException {
		try {
			log.info("AccesoDBOracle.queryDB(E)");
			stm = m_conn.createStatement();
			ResultSet rs = stm.executeQuery(query);
			log.info("AccesoDBOracle::queryDB(S)");
			return rs;
		} catch (SQLException sqle) {
			error.append("AccesoDBOracle.queryDB(SQLException): " + sqle.getMessage()+" Query:*****" + query + "*****");
			log.info(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}
	
	public ResultSet queryDB(String query, int maxSize) throws SQLException {
		try {
			log.info("AccesoDBOracle.queryDB(E)");
			stm = m_conn.createStatement();
			stm.setMaxRows(maxSize);
			ResultSet rs = stm.executeQuery(query);
			log.info("AccesoDBOracle::queryDB(S)");
			return rs;
		} catch (SQLException sqle) {
			error.append("AccesoDBOracle.queryDB(SQLException): " + sqle.getMessage() + " Query:*****" + query + "*****");
			log.info(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}
	
	//////////////////////////////////////////////////
	// Ejecuta cualquier SQL dinamicamente
	//////////////////////////////////////////////////
	public void ejecutaSQL(String sql) throws SQLException {
		Statement stm = null;
		try {
			stm = m_conn.createStatement();

			stm.executeUpdate(sql);
			stm.close();
			stm = null;
		 }
		 catch (SQLException sqle) {
			error.append("AccesoDBOracle.queryDB(ejecutaSQL): " + sqle.getMessage()+" Query:*****"+sql+"*****");
			log.info(mostrarError());
			if (stm != null) {
				stm.close();
			}
			sqle.printStackTrace();
			throw sqle;
		 }
	}



	private Connection	m_conn;
	private Statement 			stm;
	private StringBuilder	error = new StringBuilder();

}
