package netropology.utilerias;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;
//import org.apache.commons.codec.binary.*;
/**
 * Clase que realiza las codificacion Paramatros NetPay
 */
public class CRutaFisicaWS  {
	public CRutaFisicaWS() {
		try {
			Properties prop = this.loadParams("cRutaFisicaWS");
			rutaFisica = (String) prop.getProperty("rutaFisica");
      urlResponse = (String) prop.getProperty("urlResponse");
			SID = (String) prop.getProperty("SID");
		} catch(Throwable e) {
			System.out.println("ServicioSMS(Exception)::" +
					"urlNetPay=" + rutaFisica + "\n" +
					"SID=" + SID + "\n" +
					"Error = " + e.getMessage());
			e.printStackTrace();
			throw new RuntimeException("Error al inicializar el servicio de NetPay");
		}
	}
  


  /**
	 * Este m�todo lee el archivo de propiedades
	 *
	 * @param archivo nombre del archivo (sin el .properties)
	 * @return Properties El objeto de propiedades
	 * @exception IOException si la carga de propiedades falla.
	 */
	private Properties loadParams(String file)
			throws IOException {

		// Carga un ResourceBundle y apartir de este crea un Properties
		Properties		 prop	 = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);

		// Obtiene las llaves y la coloca en el objeto Properties
		Enumeration enumx = bundle.getKeys();
		String key	= null;
		while (enumx.hasMoreElements()) {
			key = (String) enumx.nextElement();
			prop.put(key, bundle.getObject(key));
		}

		return prop;
	}
	
	private String rutaFisica;
  private String urlResponse;
	private String fechaTrans;
	private String horaTrans;
	private String numOrden;
  private String SID;


  public String getFechaTrans()
  {
    return fechaTrans;
  }


  public String getHoraTrans()
  {
    return horaTrans;
  }


  public String getUrlResponse()
  {
    return urlResponse;
  }


  public String getRutaFisica()
  {
    return rutaFisica;
  }
}