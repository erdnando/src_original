package netropology.utilerias;
//Title:        Utiler�as
//Version:      1.0
//Copyright:    Copyright (c) 2001
//Author:       Varios :-)
//Company:      Netropology.
//Description:  Archivo con diversos m�todos comunes a las aplicaciones.
import java.text.SimpleDateFormat;

import java.util.Date;

public class VerificaRFC {	

    public VerificaRFC() {}

 	public boolean validaRFC(String rfc){
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaHoy = formatoFecha.format(new Date()).toString();
		//System.out.println(fechaHoy);
		int dia = Integer.parseInt(fechaHoy.substring(0,2));
		int mes = Integer.parseInt(fechaHoy.substring(3,5));
		int anio = Integer.parseInt(fechaHoy.substring(6,10));
		boolean bandera = true;
	
		/*System.out.println("anio String: "+fechaHoy.substring(0,2));
		System.out.println("mes String: "+fechaHoy.substring(3,5));
		System.out.println("dia String: "+fechaHoy.substring(6,10));*/
		//System.out.println("rfc: "+rfc);

		/*System.out.println("anio: "+Integer.parseInt(rfc.substring(5,7)) + "-" + anio);
		System.out.println("mes: "+Integer.parseInt(rfc.substring(6,8)) + "-" + mes);
		System.out.println("dia: "+Integer.parseInt(rfc.substring(8,10)) + "-" + dia);

		System.out.println("Tama�o del RFC: "+rfc.length());*/
		if(rfc.length()!=14 && rfc.length()!=15)
			bandera=false;
		if(rfc.length()==14){
			//System.out.println("Entre a 14");
			//System.out.println("El RFC: "+rfc.substring(0,3)+" - "+rfc.substring(4,6)+" - " +rfc.substring(6,8)+" - "+rfc.substring(8,10));
			if(!esCaracter(rfc.substring(0,3)))
				 bandera=false;
			if(rfc.charAt(3) != '-')
				 bandera=false;
			if(rfc.charAt(10) != '-')
				 bandera=false;
			if(Comunes.esNumero(rfc.substring(4,10))){
				/*if( Integer.parseInt(rfc.substring(4,6)) > anio)
					bandera=false;*/
				if( Integer.parseInt(rfc.substring(6,8)) <0 && Integer.parseInt(rfc.substring(6,8)) > 12 )
					bandera=false;
				if( Integer.parseInt(rfc.substring(8,10)) <0 && Integer.parseInt(rfc.substring(8,10)) > 31 )
					bandera=false;
			}
			else
				bandera=false;
			
			if( Integer.parseInt(rfc.substring(4,6)) == anio ){
				if(Integer.parseInt(rfc.substring(6,8)) > mes)
					bandera=false;
				else{
					if(Integer.parseInt(rfc.substring(6,8)) == mes ){
						if(Integer.parseInt(rfc.substring(8,10)) >dia)
							bandera=false;
					}
				}
			}
		}
		
		if(rfc.length()==15){
		/*System.out.println("Entre a 15");
		System.out.println("El RFC: "+rfc.substring(0,4)+" - "+rfc.substring(5,7)+" - " +rfc.substring(7,9)+" - "+rfc.substring(9,11));
		System.out.println("Valor de Comunes: "+Comunes.esNumero(rfc.substring(0,4)));*/
			if(!esCaracter(rfc.substring(0,4)))
				 bandera=false;
			if(rfc.charAt(4) != '-')
				 bandera=false;
			if(rfc.charAt(11) != '-')
				 bandera=false;
			if(Comunes.esNumero(rfc.substring(5,11))){
				/*if( Integer.parseInt(rfc.substring(5,7)) > anio)  // desde 1921 a�o permitido en RFC
					bandera=false;*/
				if( Integer.parseInt(rfc.substring(7,9)) <0 && Integer.parseInt(rfc.substring(7,9)) > 12 )
					bandera=false;
				if( Integer.parseInt(rfc.substring(9,11)) <0 && Integer.parseInt(rfc.substring(9,11)) > 31 )
					bandera=false;
			} 
			else
				bandera=false;
			
			if( Integer.parseInt(rfc.substring(5,7)) == anio )
			{
				if( Integer.parseInt(rfc.substring(7,9)) > mes )
					bandera=false;
				else{
					if(Integer.parseInt(rfc.substring(7,9)) == mes ){
						if(Integer.parseInt(rfc.substring(9,11)) >dia)
							bandera=false;
					}
				}
			}

		}
     return bandera;
     }
	 
	 public boolean esCaracter(String cadena) {
	 	boolean Sies=true;
	 	for(int i=0; i<cadena.length(); i++) {
			if(Character.isDigit(cadena.charAt(i)))
				Sies=false;
		}
		return Sies;
	 }

}
