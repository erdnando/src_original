package netropology.utilerias;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * La clase permite obtener la información de una lista de listas que representa una
 * tabla, a através del nombre que se le asocie a cada una de las posiciones de la lista
 *
 * Por ejemplo. Para almacenar el resultado de una consulta, dentro de Listas de Listas
 * y aun así poder recuperar la información a través del nombre de la columna.
 *
 * @author Gilberto Aparicio
 *
 */
public class Registros implements java.io.Serializable {

	public Registros() {}

	public Registros(List<String> colNames) {
		setNombreColumnas(colNames);
	}

	/**
	 * Obtiene el nombre de las columnas del resultado.
	 * @return 
	 */
	public List getNombreColumnas() {
		return this.colnames;
	}
	
	/**
	 * Estable el nombre de las columnas
	 * @param colnames Lista con el nombre de las columnas de la información
	 */
	public void setNombreColumnas(List<String> colnames) {
		this.colnames = colnames;
		for(int i=0;i<this.colnames.size();i++) {
			this.colnames.set(i, this.colnames.get(i).toString().toUpperCase());
		}//for(int i=0;i<this.colnames.size();i++)
	}
	
	/**
	 * Establece la información.
	 * @param data Es una listas de listas que representan una 
	 * 		tabla de información
	 *
	 */
	public void setData(List data) {
		this.data = (data.isEmpty())?null:data;
	}
	
	/**
	 * Obtiene toda la información de los registros y la regresa en formato JSON
	 * @return Cadena JSON
	 */
	public String getJSONData() {
		JSONArray jsonArr = new JSONArray();
		while(this.next()) {
			JSONObject jsonObj = new JSONObject();
			for (int i = 0 ; i < this.colnames.size(); i++) {
				if(this.getObject(i+1) instanceof java.math.BigDecimal){
				    jsonObj.put(colnames.get(i),((java.math.BigDecimal)this.getObject(i+1)).toPlainString());
				}else{
					jsonObj.put(colnames.get(i),this.getObject(i+1));
				}
			}
			jsonArr.add(jsonObj);
		}
		return jsonArr.toString();
	}
	
	/**
	 * Obtiene la información.
	 *
	 */
	public List getData() {
		return this.data;
	}

	/**
	 * Obtiene la información.
	 *
	 */
	public List getData(int indice) {
		return (List)this.data.get(indice);
	}
	
	/**
	 * Agrega un renglon a la información.
	 * @param datarow Es una listas que representan un 
	 * 		renglon de una tabla de información
	 *
	 */
	public void addDataRow(List datarow) {
		if(this.data!=null) {
			this.data.add(datarow);
		} else {
			this.data = new ArrayList();
			this.data.add(datarow);
		}
	}

	/**
	 * Obtiene el campo especificado como Cadena
	 *
	 */
	public String getString(String nombreCampo) {
		int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
		if (indice == -1 )  {
			throw new RuntimeException("El campo " + nombreCampo + " no existe");
		}
		List renglon = (List) this.data.get(this.numRegistro);
		return ( renglon == null)?null:renglon.get(indice).toString();
	}

	/**
	 * Obtiene el campo especificado como Cadena
	 * @param posicion Posicion del campo (Primer campo es 1)
	 * @return Cadena con el valor del campo
	 */
	public String getString(int posicion) {
		int indice = posicion - 1;
		List renglon = (List) this.data.get(this.numRegistro);
		return ( renglon == null)?null:renglon.get(indice).toString();

	}

	/**
	 * Obtiene el valor del campo especificado, en el registro en turno, como Objeto
	 *
	 */
	public Object getObject(String nombreCampo) {
		int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
		if (indice == -1 )  {
			throw new RuntimeException("El campo " + nombreCampo + " no existe");
		}

		List renglon = (List) this.data.get(this.numRegistro);

		return renglon.get(indice);
	}

	/**
	 * Obtiene el valor del campo especificado, en el registro en turno, como Objeto
	 * @param posicion Posicion del campo (Primer campo es 1)
	 * @return Cadena con el valor del campo
	 */
	public Object getObject(int posicion) {
		int indice = posicion - 1;
		List renglon = (List) this.data.get(this.numRegistro);

		return renglon.get(indice);
	}

	/**
	 * Establece un nuevo valor del campo especificado, en el registro en turno
	 * @param nombreCampo nombre de la columna a establecer
	 * @param value valor del objeto a establecer
	 */
	public void setObject(String nombreCampo, Object value) {
		int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
		if (indice == -1 )  {
			throw new RuntimeException("El campo " + nombreCampo + " no existe");
		}

		List renglon = (List) this.data.get(this.numRegistro);

		renglon.set(indice, value);
	}

	/**
	 * Establece un nuevo valor del campo especificado, en el registro en turno
	 * @param posicion Posicion del campo (Primer campo es 1)
	 * @return Cadena con el valor del campo
	 */
	public void setObject(int posicion, Object value) {
		int indice = posicion - 1;
		List renglon = (List) this.data.get(this.numRegistro);

		renglon.set(indice, value);
	}


	/**
	 * Determina si hay más registros dentro del conjunto de datos contenidos
	 * @return true si existe más renglones o false de lo contrario
	 */
	public boolean next() {
		boolean haySiguiente = false;
		
		if (this.data != null && this.data.size() >0) {
			this.numRegistro++;
			if (this.numRegistro < this.data.size() ) {
				haySiguiente = true;
			} else { //End Of Data
				haySiguiente = false;
				this.numRegistro = -1;
			}
		}
		return haySiguiente;
	}
	
	/**
	 * Resetea el indice de registros para otra vez comenzar a leer desde el primero
	 */
	public void rewind(){
		numRegistro = -1;
	}
	
	/**
	 * Regresa el numero de registros contenidos
	 * @return Numero de registros
	 */
	public int getNumeroRegistros() {
		return (data == null)?0:data.size();
	}
	
	/**
	 * Listado de metadatos de las columnas del registro
	 */
	private List<String> colnames;
	
	/**
	 * Lista de datos de las columnas del registro
	 */
	private List data;
	
	/**
	 * Numero de Registro
	 */
	int numRegistro = -1;


	/**
	 * Establece el numero de registro en turno
	 * @param numRegistro
	 */
	public void setNumRegistro(int numRegistro) {
		this.numRegistro = numRegistro;
	}

	/**
	 * Obtiene el numero de registro en turno
	 * @return indice del registro
	 */
	public int getNumRegistro() {
		return this.numRegistro;
	}
	
	public void remove(int numeroRegistro) {
		this.data.remove(numeroRegistro);
	}

	public void remove() {
		this.data.remove(this.numRegistro);
	}
	
}