package netropology.utilerias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Random;

import org.apache.commons.logging.Log;

/**
 * Generaci�n y manejo de acuses. El formato del acuse cambia de acuerdo al tipo de acuse a generar.
 * 
 */
public class Acuse implements java.io.Serializable {
	
	private static final Log LOG = ServiceLocator.getInstance().getLog(Acuse.class);
	
	public static final int ACUSE_EPO=1;
	public static final int ACUSE_PYME=2;
	public static final int ACUSE_IF=3;
	public static final int ACUSE_EPO_MODIF=4;
	public static final int ACUSE_CTE=5;
	public static final int ACUSE_DIST=6;
 	public static final int ACUSE_PYME_AUT=7;
	public static final int ACUSE_NAFIN_OPER=8;
	public static final int ACUSE_NAFIN_COB=9;
	public static final int ACUSE_IF_PIG=10;

	/**
	 * Genera un objeto acuse a partir del valor que recibe.
	 * @param acuse Cadena con el valor de acuse
	 */
	public Acuse(String acuse) {
		this.acuse = acuse;
	}

	/**
	 * Genera Acuse con base en el tipo de acuse solicitado.
	 * Considera de manera predeterminada que el acuse es para el producto de Descuento Electr�nico (1)
	 * @param tipoAcuse Ver constantes de esta clase que comienzan con ACUSE_
	 */
	public Acuse(int tipoAcuse) {
		this.setAcuse(tipoAcuse);
	}

	/**
	 * Genera un acuse considerando el tipo de acuse y el tipo de producto para el que se genera.
	 * @param tipoAcuse Ver constantes de esta clase que comienzan con ACUSE_
	 * @param prodNafin Clave de producto
	 */
	public Acuse(int tipoAcuse, String prodNafin) {
		this.icProductoNafin = prodNafin;
		this.setAcuse(tipoAcuse);
	}

	/**
	 * Genera un acuse considerando el tipo de acuse y el tipo de producto para el que se genera.
	 * @param tipoAcuse Ver constantes de esta clase que comienzan con ACUSE_
	 * @param prodNafin Clave de producto
	 * @param prefijoTabla prefijo de la tabla de acuses ejempo: com (com_acuse), dis (dis_acuse)
	 * @deprecated Se ajusto la forma de calcular el consecutivo, por lo que prefijoTabla es irrelevante
	 */
	public Acuse(int tipoAcuse, String prodNafin,String prefijoTabla) {
		this(tipoAcuse, prodNafin);
	}
	

	/**
	 * Genera un acuse considerando el tipo de acuse y el tipo de producto para el que se genera.
	 * @param tipoAcuse Ver constantes de esta clase que comienzan con ACUSE_
	 * @param prodNafin Clave de producto
	 * @param con Objeto para manejo de conexion de BD
	 * @deprecated Usar los constructores que no reciben AccesoDB como par�metro
	 */
	public Acuse(int tipoAcuse, String prodNafin, AccesoDB con) {
		this(tipoAcuse, prodNafin);
	}

	/**
	 * Genera un acuse considerando el tipo de acuse y el tipo de producto para el que se genera.
	 * @param tipoAcuse Ver constantes de esta clase que comienzan con ACUSE_
	 * @param prodNafin Clave de producto
	 * @param prefijoTabla prefijo de la tabla de acuses ejempo: com (com_acuse), dis (dis_acuse)
	 * @param con Objeto para manejo de conexion de BD
	 * @deprecated  Usar los constructores que no reciben AccesoDB como par�metro
	 */
	public Acuse(int tipoAcuse, String prodNafin, String prefijoTabla, AccesoDB con) {
		this(tipoAcuse, prodNafin);
	}

	/**
	 * Obtiene la representacion en cadena de texto del Acuse.
	 * @return Cadena con el valor generado del acuse
	 */
	@Override
	public String toString() {
		return this.acuse;
	}

	/**
	 * Realiza el formateo del acuse. Este formato varia dependiendo del tipo de producto para el que se obtuvo el acuse.
	 * @return Cadena con el acuse con formato
	 */
	public String formatear() {
		String retorno = "";
		if(icProductoNafin.equals("1")){
			retorno = acuse.substring(0,1)+"-"+acuse.substring(1,7)+"-"+acuse.substring(7,9)+"-"+acuse.substring(9,14);
		}else if(icProductoNafin.equals("2")||icProductoNafin.equals("3") || icProductoNafin.equals("5")||icProductoNafin.equals("6")) {
			retorno = acuse.substring(0,1)+"-"+acuse.substring(1,2)+"-"+acuse.substring(2,8)+"-"+acuse.substring(8,10)+"-"+acuse.substring(10,15);
		}else if(icProductoNafin.equals("4")) {
			retorno = acuse.substring(0,1)+"-"+acuse.substring(1,2)+"-"+acuse.substring(2,8)+"-"+acuse.substring(8,10)+"-"+acuse.substring(10,15);
		}
		return retorno;
	}

	/**
	 * M�todo para generar el acuse con un formato predefinido y adicionando algunos caracteres 
	 * aleatoriamente y un consecutivo
	 * @param tipoAcuse Tipo de acuse. Ver constantes de esta clase que comienzan con ACUSE_
	 */
	private void setAcuse(int tipoAcuse) {
		String fecha = (new SimpleDateFormat ("ddMMyy")).format(new java.util.Date());
		String prefijo = "";
		if (!"1".equals(this.icProductoNafin)) {		// Obt letra inicial para acuse
			prefijo = this.obtenerLetraInicial();
		}
		
		String strTipoAcuse = "";
		if(tipoAcuse==Acuse.ACUSE_IF_PIG) {
			strTipoAcuse = "P";
		} else if (tipoAcuse==Acuse.ACUSE_PYME_AUT) {
			strTipoAcuse = "5";
		} else {
			strTipoAcuse = String.valueOf(tipoAcuse);
		}

		this.acuse = prefijo+strTipoAcuse+fecha+this.obtenerCaracteresAleatorios()+this.obtenerConsecutivo();
	}

	/**
	 * Obtiene una cadena que corresponde a un consecutivo de 5 posiciones, con relleno de ceros a la izquierda.
	 * @return Cadena con el consecutivo
	 */
	private String obtenerConsecutivo() {
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String qrySentencia =
					" SELECT TO_CHAR(com_acuse_seq.NEXTVAL, 'FM00000') AS consecutivo " +	//rellena el numero con hasta 5 ceros, segun sea necesario.
					" FROM dual ";
		
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ResultSet rs = ps.executeQuery();
			rs.next();
			String consecutivo = rs.getString(1);
			rs.close();
			ps.close();
			return consecutivo;
		} catch(Exception e) {
			throw new AppException("Error al obtener el consecutivo", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene la letra inicial para anexar al acuse.
	 * @return Cadena la letra inicial
	 */
	private String obtenerLetraInicial() {
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String qrySentencia = 
					" SELECT TRIM(cg_inicial) AS cg_inicial " +
					" FROM comcat_producto_nafin " +
					" WHERE ic_producto_nafin = ? ";
			PreparedStatement psLetraIni = con.queryPrecompilado(qrySentencia);
			psLetraIni.setInt(1, Integer.parseInt(this.icProductoNafin));
			ResultSet rsLetraIni = psLetraIni.executeQuery();
			String prefijo = "";
			if(rsLetraIni.next()) {
				prefijo = rsLetraIni.getString("cg_inicial");
			}
			rsLetraIni.close();
			psLetraIni.close();
			return prefijo;
		} catch(Exception e) {
			throw new AppException("Error al obtener el prefijo", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene dos caracteres aleatorios alfanumericos que debe contener el Acuse.
	 * @return Cadena de dos caracteres alfanumericos aleatorios
	 */
	private String obtenerCaracteresAleatorios() {
		char alfanumericos[] = {'A', 'B', 'C', 'D', 'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
		
		Random random = new Random();
		
		char aleatorio1 = alfanumericos[random.nextInt(alfanumericos.length)];
		char aleatorio2 = alfanumericos[random.nextInt(alfanumericos.length)];
		return (String.valueOf(aleatorio1) + String.valueOf(aleatorio2));
	}


	/**
	 * Establece la clave del usuario que realiza el acuse.
	 * @param strClaveUsuario Clave del usuario (ic_usuario)
	 */
	public void setClaveUsuario(String strClaveUsuario){
		this.claveUsuario = strClaveUsuario;
	}

	/**
	 * Obtiene la clave del usuario que realiza el acuse.
	 * @return Clave del usuario (ic_usuario)
	 */
	public String getClaveUsuario(){
		return this.claveUsuario;
	}

	/**
	 * Establece la fecha y hora de generacion del acuse.
	 * @param strFechaHora fecha y hora de generacion del acuse
	 * 		en formato dd/mm/yyyy hh:mm:ss
	 */
	public void setFechaHora(String strFechaHora){
		this.fechaHora = strFechaHora;
	}

	/**
	 * Obtiene la fecha y hora de generacion del acuse.
	 * @return Fecha y hora de generacion del acuse
	 * 		en formato dd/mm/yyyy hh:mm:ss
	 */
	public String getFechaHora(){
		return this.fechaHora;
	}

	/**
	 * Establece el recibo electronico obtenido del Segurisign.
	 * @param strReciboElectronico Recibo electronico
	 */
	public void setReciboElectronico(String strReciboElectronico){
		this.reciboElectronico = strReciboElectronico;
	}

	/**
	 * Obtiene el recibo electronico obtenido del Segurisign.
	 * @return Recibo electronico
	 */
	public String getReciboElectronico(){
		return this.reciboElectronico;
	}

	/**
	 * Obtiene el acuse generado.
	 * @return acuse
	 */
	public String getAcuse(){
		return this.acuse;
	}

	private String acuse;
	private String claveUsuario;
	private String fechaHora;
	private String reciboElectronico;
	private String icProductoNafin = "1";

}	//fin de la clase
