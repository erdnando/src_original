package netropology.utilerias;


/**
 * Exception gen�rica para cuando se genere un error dentro de la aplicacion.
 *
 * @author Gilberto Aparicio
 */
public class AppException extends RuntimeException {
    
	public AppException() {
		super();
	}
	
	public AppException(String mensaje) {
		super(mensaje);
	}
	
	public AppException(String mensaje, Throwable causaException){
		super(mensaje, causaException);
	}
	
	public AppException(Throwable causaException) {
		super(causaException);
	}
}

