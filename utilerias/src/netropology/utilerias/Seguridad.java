package netropology.utilerias;

import SSign.SSign;

import org.apache.commons.logging.Log;


/**
 * Clase que permite realizar la validación de la firma digital PKCS7.
 *
 */
public class Seguridad {
    
    private static Log log = ServiceLocator.getInstance().getLog(Seguridad.class);
    
	private String sequence;
	private String errorMessage; 		//Mensaje de Error --Salida

	private String ipSeguriSign;
	private int puertoSeguriSign;

	public Seguridad() {
		if("N".equals(ParametrosGlobalesApp.getInstance().getParametro("FIRMA_DIGITAL"))) {
		    //Nada que hacer. Firma Digital deshabilitada (para ambientes de DESARROLLO).
            log.info("Firma Digital deshabilitada");
		} else {
			AccesoDB con = new AccesoDB();
			try{
				con.conexionDB();
                
				String qrySentencia =
						" SELECT cg_ip_segurisign, cg_puerto_segurisign "+
						" FROM com_param_gral ";
                
				Registros reg = con.consultarDB(qrySentencia);
                
				if(reg.next()){
					this.ipSeguriSign 		= reg.getString("CG_IP_SEGURISIGN");
					this.puertoSeguriSign 	= (reg.getString("CG_PUERTO_SEGURISIGN")!=null)?Integer.parseInt(reg.getString("CG_PUERTO_SEGURISIGN")):7920;
					if (this.ipSeguriSign == null) {
						throw new AppException("No se pudo obtener el valor de los parámetros del SEGURISIGN");
					}
				}
                
			    log.info("Firma Digital habilitada");
				log.debug("IP SeguriSign Establecida: " + this.ipSeguriSign);
				log.debug("Puerto SeguriSign Establecido: " + this.puertoSeguriSign);
			} catch(Throwable t) {
                log.error("Seguridad::Seguridad(Error) "+t);
				throw new AppException("Error al inicializar el componente de firma digital.", t);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
		}
	}

	/**
	 * Realiza la autenticación de la cadena firmada usando el SeguriSign.
	 * @param folio Folio del contenido firmado
	 * @param serial Numero de Serie del certificado usado para firmar
	 * @param pkcs7 Cadena firmada digitalmente en formato PKCS7
	 * @param externContent Cadena original
	 * @param getReceipt Indicador que determina si se necesita un numero de recibo o no (Y o N)
	 * @return true Si la autenticación se llevo a cabo exitosamente o false de lo contrario
	 */
	public boolean autenticar(String folio, String serial, String pkcs7, String externContent, char getReceipt) {
        log.info("Seguridad::autenticar(E)");
		if("N".equals(ParametrosGlobalesApp.getInstance().getParametro("FIRMA_DIGITAL"))) {
            AccesoDB con = new AccesoDB();
            try {
                con.conexionDB();
                String qrySentencia=
                    " SELECT seq_dummy.nextval as secuencia "+
                    " FROM DUAL ";
                Registros reg = con.consultarDB(qrySentencia);
                reg.next();
                this.sequence = reg.getString("secuencia");
                return true;
            } catch(Throwable e) {
                log.error("Seguridad::autenticar(Throwable) "+e);
                throw new AppException("Error al obtener el acuse(desarrollo).");
            } finally {
                log.info("Seguridad::autenticar(folioNE) "+this.sequence);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
	   } else {
            SSign ss = new SSign();
		    int resultado = 0;
            try {
            	log.debug("AuthenticatePKCS7(E)");
                resultado = ss.AuthenticatePKCS7(
                                                    this.ipSeguriSign,      this.puertoSeguriSign,
                                                    folio,              folio.length(),
                                                    serial,                 serial.length(),
                                                    "texto.txt",        "FALSE",
                                                    pkcs7,              pkcs7.length(),
                                                    externContent,  externContent.length(),
                                                    getReceipt
                                                    );
				log.debug("AuthenticatePKCS7(S)");
                if (resultado==1) {     //Autenticación Exitosa?
                    this.sequence = ss.GetSequence();
                    log.debug("this.sequence:"+this.sequence);
                    return true;
                } else {                //Autenticación Fallida (resultado==0)
                    this.errorMessage = ss.getError();
                    log.error("Seguridad::autenticar(errorMessage) "+this.errorMessage);
                    return false;
                }                
            } catch(Exception e) {
                log.error("Seguridad::autenticar(Exception) "+e.getMessage());
                e.printStackTrace();
                return false;
            }
		}
	}//autenticar
    
	/**
	 * Regresa el mensaje de error en el proceso de validacion de la firma digital
	 * @return Cadena con el mensaje de error
	 */
	public String mostrarError(){
		return this.errorMessage;
	}

	/**
	 * Obtiene el numero de acuse que regreso el seguriServer
	 * @return Cadena con el numero de acuse de la validacion de la firma digital.
	 */
	public String getAcuse(){
		return this.sequence;
	}

}//Seguridad