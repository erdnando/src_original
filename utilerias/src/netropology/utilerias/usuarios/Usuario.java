package netropology.utilerias.usuarios;

import java.util.Date;

/**
 * Usuario tiene como finalidad, la de contener la información del usuario
 * @author Gilberto Aparicio - GEAG
 */
public class Usuario implements java.io.Serializable, Comparable  {
	public Usuario(){}

	/**
	 * Establece el correo electronico del usuario
	 * @param email Correro electronico
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	/**
	 * Establece el apellido paterno del usuario
	 * @param apellidoPaterno Apellido paterno
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	
	
	/**
	 * Establece el apellido materno del usuario
	 * @param apellidoMaterno Apellido Materno
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	
	/**
	 * Establece el nombre del usuario
	 * @param nombre Nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Establece la clave interna del afiliado al que pertenece el usuario
	 * @param claveAfiliado Clave del Afiliado
	 */
	public void setClaveAfiliado(String claveAfiliado) {
		this.claveAfiliado = claveAfiliado;
	}


	/**
	 * Establece el tipo de afiliado al que pertenece el usuario
	 * @param tipoAfiliado Tipo de Afiliado
	 */
	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}

	/**
	 * Establece el perfil (grupo) del usuario
	 * @param perfil
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * Obtiene el correo electrónico del usuario
	 * @return Cadena con el correo electrónico
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Obtiene el apellido paterno del usuario
	 * @return Cadena con el apellido paterno
	 */
	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	/**
	 * Obtiene el apellido materno del usuario
	 * @return Cadena con el apellido materno
	 */
	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	/**
	 * Obtiene el nombre del usuario
	 * @return Cadena con el nombre
	 */
	public String getNombre() {
		return this.nombre;
	}

	/**
	 * Método de conveniencia para obtener Apellido Paterno, Materno y Nombre 
	 * del Usuario
	 * @return Cadena con el nombre completo
	 */
	public String getNombreCompleto() {
		return this.apellidoPaterno + " " + this.apellidoMaterno + " " + this.nombre;
	}


/**
 * Obtiene la clave la clave del IF o EPO o Pyme a la que el usuario pertenece
 * Para el caso especifico de Nafin el valor obtenido sera la clave SIRAC
 */
	public String getClaveAfiliado() {
		return this.claveAfiliado;
	}

/**
 * Obtiene el tipo de Afiliado
 * @return Cadena con cualquiera de los siguientes valores:
 * 		"N" Nafin
 *       "I" If
 *       "P" Pyme
 *       "E" Epo
 */
	public String getTipoAfiliado() {
		return this.tipoAfiliado;
	}

/**
 * Obtiene el perfil del Usuario. Se asume que el usuario solo puede 
 * tener un perfil asociado.
 */
	public String getPerfil() {
		return this.perfil;
	}


	/**
	 * Obtien el RFC del usuario
	 * @return RFC del usuario
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * Establece el RFc del usuario
	 * @param rfc RFC del usuario
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


/**
 * Obtiene la fecha de nacimiento del Usuario. Se asume que el 
 * usuario solo puede tener un perfil asociado.
 */
	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String toString() {
		String objeto = 
			"login=" + this.login + "\n" +
			"apellidoPaterno=" + this.apellidoPaterno + "\n" +
			"apellidoMaterno=" + this.apellidoMaterno + "\n" +
			"nombre=" + this.nombre + "\n" +
			
			"fechaNacimiento=" + this.fechaNacimiento + "\n" +
			
			"email=" + this.email + "\n" +
			"claveAfiliado=" + this.claveAfiliado + "\n" +
			"tipoAfiliado=" + this.tipoAfiliado + "\n" +
			"perfil=" + this.perfil + "\n" +
			"rfc=" + this.rfc + "\n" +
			"nombreOrganizacionUsuario=" + this.nombreOrganizacionUsuario + "\n";
		
		return objeto;
	}
	
	public int compareTo(Object o) {
     Usuario otroUsuario = (Usuario) o;
	  int 		result 		=	0;
     result = perfil.compareTo(otroUsuario.getPerfil());
	  if(result == 0)
		 	result = login.compareTo(otroUsuario.getLogin());
	  return result;
   }

	private String login;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombre;

	private Date fechaNacimiento;
	
	private String email;
	private String claveAfiliado;
	private String tipoAfiliado;
	private String perfil;
	private String rfc;
	private String nombreOrganizacionUsuario;

	
	/**
	 * Obtiene el nombre de la organización a la que pertenece el usuario.
	 * @return Cadena con la razon social de la organización a la que pertenece el usuario
	 */
	public String getNombreOrganizacionUsuario() {
		return nombreOrganizacionUsuario;
	}

	public void setNombreOrganizacionUsuario(String nombreOrganizacionUsuario) {
		this.nombreOrganizacionUsuario = nombreOrganizacionUsuario;
	}




}