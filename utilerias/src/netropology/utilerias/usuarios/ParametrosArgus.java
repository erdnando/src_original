package netropology.utilerias.usuarios;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Contiene variables que definen los parametros de conexion para el
 * Oracle Internet Directory. Los parametetros son cargados del archivo
 * ParametrosArgus.properties
 *
 */
public class ParametrosArgus {
	public ParametrosArgus() {	}

	// Servicio web service OIDWebService
	public static String urlOIDWebService	= null;
	// Servicio web service ServicesOID
	public static String urlServicesOID	= null;
	// Default Root Context
	public static String idSubscriptor	= null;
	// Nombre de la instancia. Los valores posibles est�n dados por ARGUS.
	public static String oidInstance = null;
	// SITE ID
	public static String siteId = null;
	// SITE ID FIADO
	public static String siteIdFiado = null;


	// Load parameters from Connection.properties
	static {

		if( idSubscriptor == null ) {
			Properties conValues = null;

			try {

				// Obtiene los valores de Conexion.properties
				conValues = loadParams("ParametrosArgus");

				// Obtiene los detalles del archivo de propiedades.
				urlOIDWebService	= conValues.getProperty("OIDWEBSERVICE");
				urlServicesOID	= conValues.getProperty("SERVICESOID");

				idSubscriptor = conValues.getProperty("ID_SUBSCRIPTOR");
				oidInstance = conValues.getProperty("OID_INSTANCE");

				siteId = conValues.getProperty("SITE_ID");
				siteIdFiado = conValues.getProperty("SITE_ID_FIADO");
				
			} catch (Exception ex) {
					System.out.println(" Error Fatal: No fue posible leer el archivo Properties: " +
							ex.getMessage());
			} finally {
				// Limpia propiedades
				conValues.clear();
				conValues = null;
			}
		}
	}


	/**
	 * Este m�todo lee el archivo de propiedades
	 *
	 * @param archivo Ruta del archivo
	 * @return Properties El objeto de propiedades
	 * @exception IOException si la carga de propiedades falla.
	 */
	public static Properties loadParams(String file)
			throws IOException {

		// Carga un ResourceBundle y apartir de este crea un Properties
		Properties		 prop	 = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);

		// Obtiene las llaves y la coloca en el objeto Properties
		Enumeration enumx = bundle.getKeys();
		String key	= null;
		while (enumx.hasMoreElements()) {
			key = (String) enumx.nextElement();
			prop.put(key, bundle.getObject(key));
		}

		return prop;
	}
}