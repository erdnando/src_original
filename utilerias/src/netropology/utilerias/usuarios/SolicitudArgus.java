package netropology.utilerias.usuarios;


/**
 * Define la solicitud de alta de usuarios dentro del Sistema de Administraci�n
 * Centralizada de Usuarios (ARGUS).
 * Los datos de Domicilio, RFC y Telefono, son los de la Compa��a
 * a la que pertenece el usuario.
 *
 * De manera predeterminada una solicitud es preautorizada, esto significa
 * que el ARGUS no requerira que un usuario autorice manualmente la solicitud.
 * Este valor predeterminado se puede cambiar mediante el m�todo
 * setPreautorizacion(false) y as� obligar a que la solicitud sea autorizada
 * a trav�s del m�dulo del ARGUS.
 *
 * "F078-2005 Modificaciones al Argus". Las solicitudes pueden ser bloqueadas
 * cuando se quiera demorar su procesamiento por el ARGUS.
 *
 * @author Gilberto Aparicio
 */
public class SolicitudArgus implements java.io.Serializable {
	public SolicitudArgus() {}

	public Usuario getUsuario() {
		return this.usuario;
	}

	/**
	 * Establece el usuario.
	 * @param  usuario Objeto de la clase "Usuario" el cual contiene la
	 * informaci�n referente a este dentro de la solicitud.
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getCompania()	{
		return this.compania;
	}

	/**
	 * Establece el nombre de la compa�ia a la que pertenece el usuario.
	 * @param  compania El equivalente en N@E es la razon social de los
	 * afiliados (cg_razon_social)
	 */
	public void setCompania(String compania)	{
		this.compania = compania;
	}

	/**
	 * Establece la respuesta Secreta para recuperaci�n de contrase�a del usuario
	 * @param  respuestaSecreta Respuesta secreta para recuperaci�n de contrase�a
	 */
	public void setRespuestaSecreta(String respuestaSecreta)	{
		this.respuestaSecreta = respuestaSecreta;
	}

	/**
	 * Obtiene la respuesta Secreta para recuperaci�n de contrase�a del usuario
	 * @return Cadena con la respuesta secreta para recuperaci�n de contrase�a
	 */
	public String getRespuestaSecreta()	{
		return this.respuestaSecreta;
	}


	public String getRFC()	{
		return this.rfc;
	}

	/**
	 * Establece el RFC de la compa�ia a la que pertenece el usuario.
	 * @param  rfc El equivalente en N@E es el RFC de los afiliados 
	 * (cg_rfc)
	 */
	public void setRFC(String rfc)	{
		this.rfc = rfc;
	}

	public String getDireccion()	{
		return this.direccion;
	}

	/**
	 * Establece el la calle y numero de la compa�ia a la que 
	 * pertenece el usuario.
	 * @param  direccion El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setDireccion(String direccion)	{
		this.direccion = direccion;
	}


	public String getEstado()	{
		return this.estado;
	}

	/**
	 * Establece el estado en donde se ubica la compa�ia a la que 
	 * pertenece el usuario.
	 * @param  estado El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setEstado(String estado)	{
		this.estado = estado;
	}

	public String getMunicipio()	{
		return this.municipio;
	}

	/**
	 * Establece la delegaci�n o municipio en donde se ubica la 
	 * compa�ia a la que pertenece el usuario.
	 * @param  municipio El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setMunicipio(String municipio)	{
		this.municipio = municipio;
	}

	public String getTelefono()	{
		return this.telefono;
	}

	/**
	 * Establece el telefono de la compa�ia a la que pertenece el usuario.
	 * @param  telefono El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setTelefono(String telefono)	{
		this.telefono = telefono;
	}

	public boolean getPreautorizacion()	{
		return this.preautorizacion;
	}
	
	
	/** 
	 * Establece si las solicitudes ser�n detenidas hasta que 
	 * otro proceso las desbloquee y entonces si puedan ser procesadas
	 * por el ARGUS
	 * @param bloqueo Si requiere bloqueo establecer a true de 
	 * 		lo contrario false
	 */
	public boolean getBloqueo()	{
		return this.bloqueo;
	}


	/** 
	 * Establece si las solicitudes requieren o no
	 * intervenci�n por parte de un usuario para ser procesadas
	 * dentro del sistema ARGUS.
	 * @param preautorizacion Si son preautorizadas establecer a true de 
	 * 		lo contrario false
	 */
	public void setPreautorizacion(boolean preautorizacion)	{
		this.preautorizacion = preautorizacion;
	}


	/** 
	 * Establece si las solicitudes ser�n detenidas hasta que 
	 * otro proceso las desbloquee y entonces si puedan ser procesadas
	 * por el ARGUS
	 * @param bloqueo Si requiere bloqueo establecer a true de 
	 * 		lo contrario false
	 */
	public void setBloqueo(boolean bloqueo)	{
		this.bloqueo = bloqueo;
	}


	/**
	 * Establece la pregunta Secreta para recuperaci�n de contrase�a del usuario
	 * @param  preguntaSecreta Pregunta secreta para recuperaci�n de contrase�a
	 */
	public void setPreguntaSecreta(String preguntaSecreta)	{
		this.preguntaSecreta = preguntaSecreta;
	}

	/**
	 * Obtiene la pregunta Secreta para recuperaci�n de contrase�a del usuario
	 * @return Cadena con la pregunta secreta para recuperaci�n de contrase�a
	 */
	public String getPreguntaSecreta()	{
		return this.preguntaSecreta;
	}


	public String getCodigoPostal()	{
		return this.codigoPostal;
	}

	/**
	 * Establece el c�digo postal de la compa�ia a la que pertenece el usuario.
	 * @param  codigoPostal El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setCodigoPostal(String codigoPostal)	{
		this.codigoPostal = codigoPostal;
	}

	public String getColonia()	{
		return this.colonia;
	}

	/**
	 * Establece la colonia donde se ubica la compa�ia a la que 
	 * pertenece el usuario.
	 * @param  colonia El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setColonia(String colonia)	{
		this.colonia = colonia;
	}


	/**
	 * Establece el fax de la compa�ia a la que pertenece el usuario.
	 * @param  fax El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setFax(String fax)	{
		this.fax = fax;
	}

	public String getFax()	{
		return this.fax;
	}

	/**
	 * Establece el pais de la compa�ia a la que pertenece el usuario.
	 * @param  pais El equivalente en N@E son los datos del domicilio
	 * (ver tabla com_domicilio)
	 */
	public void setPais(String pais)	{
		this.pais = pais;
	}

	public String getPais()	{
		return this.pais;
	}


	/**
	 * Valida los datos de la solicitud.
	 * @param operacion Tipo de operacion:
	 * 		AGREGAR. Determina si la solicitud es v�lida para agregar
	 *       MODIFICAR. Determina si la solicitud es v�lida para realizar la actualizacion
	 * @return true si es valida la solicitud o false de lo contrario
	 */
	public boolean esSolicitudValida(String operacion) {
		Usuario usuario = this.getUsuario();
		boolean validez = false;
		
		if (operacion.equals("AGREGAR")) {								
			if (this.usuario == null ||
					usuario.getApellidoPaterno() == null ||
					usuario.getApellidoMaterno() == null ||
					usuario.getNombre() == null ||
					usuario.getClaveAfiliado() == null ||
					usuario.getEmail() == null ||
	// EL login pueder ir nulo cuando es para agregar				usuario.getLogin() == null ||

					usuario.getTipoAfiliado() == null ||
					this.getCodigoPostal() == null ||
					this.getColonia() == null ||
					this.getCompania() == null ||
					this.getDireccion() == null ||
					this.getEstado() == null ||
					this.getMunicipio() == null ||
					this.getRFC() == null ||
					this.getTelefono() == null ||
					usuario.getPerfil() == null
					) {
				validez =  false; //No es valida la solicitud.
			} else {
				if(this.getPreautorizacion() == false && 
						(this.getPreguntaSecreta() == null ||
						this.getRespuestaSecreta() == null)) {
					validez = false;
				} else {
					validez = true; //Solicitud v�lida
				}
			}
		} else if (operacion.equals("MODIFICAR")) {
			if (this.usuario == null ||
					usuario.getLogin() == null || usuario.getLogin().equals("")) {
				validez =  false; //No es valida la solicitud.
			} else {
				validez = true; //Solicitud v�lida
			}
		}

		return validez;
	}

	public String toString() {
		String objeto = 
			"usuario=[" + this.usuario + "]\n" +
			"compania=" + this.compania + "\n" +
			"rfc=" + this.rfc + "\n" +
			"direccion=" + this.direccion + "\n" +
			"estado=" + this.estado + "\n" +
			"municipio=" + this.municipio + "\n" +
			"pais=" + this.pais + "\n" +
			"telefono=" + this.telefono + "\n" +
			"fax=" + this.fax + "\n" +
			"preautorizacion=" + this.preautorizacion + "\n" +
			"codigoPostal=" + this.codigoPostal + "\n" +
			"colonia=" + this.colonia + "\n" +
			"preguntaSecreta=" + this.preguntaSecreta + "\n" +
			"respuestaSecreta=" + this.respuestaSecreta + "\n";			
		
		return objeto;
	}
	
	private Usuario usuario;
	private String compania;
	private String rfc;
	private String direccion;
	private String estado;
	private String municipio;
	private String telefono;

	private String fax;
	private String pais;

	private boolean preautorizacion = true;
	private String codigoPostal;
	private String colonia;
	private String preguntaSecreta;
	private String respuestaSecreta;
	
	private boolean bloqueo = false;
}