package netropology.utilerias.usuarios;

import com.chermansolutions.ldap.oid.group.proxy.ServicesOIDProxy;
import com.chermansolutions.ldap.oid.user.proxy.OIDWebServiceProxy;
import com.chermansolutions.ldap.oid.user.proxy.com_chermansolutions_ldap_oid_user_UserBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import netropology.utilerias.ValidaServicio;

import org.apache.commons.logging.Log;


/**
 * API para interactuar con el Sistema Centralizado
 * de Administracion de Usuarios (ARGUS).
 *
 * Contiene utilerias para manejo de aspectos relacionados con el usuario, como
 * comprobar si es un usuario valido, si pertenece a un determinado grupo,
 * para cambiar los datos del usuario, etc.
 *
 * @author Gilberto Aparicio - GEAG
 */
public class UtilUsr {

	private static Log log = ServiceLocator.getInstance().getLog(UtilUsr.class);

	/**
	 * Constructor Predeterminado.
	 * Establece los URL del webservice a conectarse tanto para el servicio
	 * OIDWebService como para ServicesOID.
	 */
	public UtilUsr() {
            /*
            proxyWebService.setEndpoint(ParametrosArgus.urlOIDWebService);
            proxyWebService2.setEndpoint(ParametrosArgus.urlServicesOID);
            */            
	}

	/**
	 * Actualiza la informaci�n del usuario con los datos obtenidos del bean
	 * de solicitud dentro del OID as� como en AG_PERSON.  <br>
	 * Es requerido que el idUsuario dentro del Usuario de la solicitud, sea
	 * especificado, ya que con base a este se determina el registro a modificar <br>
	 * <br>
	 * S�lo los siguientes datos de la solicitud (que tengan un valor establecido)
	 * son considerados para actualizar: <br>
	 * 	Email <br>
	 *    Apellido Paterno <br>
	 *    Apellido Materno <br>
	 *    Nombre <br>
	 *    Tipo de Afiliado <br>
	 *    Clave de afiliado <br>
	 * Se pueden actualizar tambien los datos de la empresa a la
	 * que pertenece el usuario: <br>
	 *    Datos de la empresa (Nombre, Direcci�n, telefono, RFC, C.P., etc) <br>
	 * <br><br>
	 * A trav�s de este m�todo no se puede actualizar el perfil del usuario.
	 * Usar en su lugar
	 * {@link #setPerfilUsuario(String, String) setPerfilUsuario} <br>
	 * <br>
	 * Si unicamente se requiere actualizar el email del usuario, por ejemplo,
	 * se utilizaria el siguiente codigo <br>
	 * <code>
	 * 	SolicitudArgus solicitud = new SolicitudArgus(); <br>
	 * 	Usuario usuario = new Usuario(); <br>
	 * 	usuario.setLogin("00000012"); <br>
	 * 	usuario.setEmail("nuevo_mail@nafin.gob.mx"); <br>
	 * 	solicitud.setUsuario(usuario); <br>
	 *    UtilUsr utilUsr = new UtilUsr();<br>
	 *    utilUsr.actualizarUsuario(solicitud); <br>
	 * </code>
	 *
	 * @param solicitudArgus solicitud con los valores a actualizar.
	 */

	public void actualizarUsuario(SolicitudArgus solicitud) throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (solicitud == null || !solicitud.esSolicitudValida("MODIFICAR")) {
				throw new Exception("La solicitud no es valida");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +	" solicitud=" + solicitud);
			throw e;
		}

		//***************************************************************************************
		AccesoDB con  = new AccesoDB();
		List<String> campos = new ArrayList<>();
		Usuario usuario = solicitud.getUsuario();
		Usuario usuarioAnterior = this.getUsuario(usuario.getLogin());

		com_chermansolutions_ldap_oid_user_UserBean userWebService = new com_chermansolutions_ldap_oid_user_UserBean();

		userWebService.setCommonName(usuario.getLogin());

		//  Solo los siguientes datos pueden ser considerados en una actualizacion
		if (usuario.getEmail() != null) {
			userWebService.setMail(usuario.getEmail());
			campos.add("email = '" + usuario.getEmail() + "'");
		}

		if (usuario.getApellidoPaterno() != null) {
			//Esta modificacion solo aplica para prepro/produccion ya que en desarrollo
      		//userWebService.setMiddleName(usuario.getApellidoPaterno());
			userWebService.setLastName(usuario.getApellidoPaterno());
			campos.add("middle_name = '" + usuario.getApellidoPaterno() + "'");
		}

		if (usuario.getApellidoMaterno() != null) {
			userWebService.setSurname(usuario.getApellidoMaterno());
			campos.add("last_name = '" + usuario.getApellidoMaterno() + "'");
		}

		if (usuario.getNombre() != null) {
			userWebService.setGivenName(usuario.getNombre());
			campos.add("first_name = '" + usuario.getNombre() + "'");
		}

		if (usuario.getNombre() != null || usuario.getApellidoMaterno() != null ||
				usuario.getApellidoPaterno() != null) {
			userWebService.setDisplayName(usuario.getNombre() + " " +
					usuario.getApellidoPaterno() + " " +
					usuario.getApellidoMaterno());
		}


		if (usuario.getFechaNacimiento() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat ("dd/MM/yyyy");
			userWebService.setOrclDateofBirth(usuario.getFechaNacimiento().toString());
			campos.add("date_of_birth = TO_DATE('" +
					formatter.format(usuario.getFechaNacimiento()) + "','dd/mm/yyyy')");
		}

		if (usuario.getTipoAfiliado() != null && usuario.getTipoAfiliado().length()>0) {
			userWebService.setTipoafiliado(usuario.getTipoAfiliado());
			campos.add("tipo_afiliado = '" + usuario.getTipoAfiliado() + "'");
		}
		if (usuario.getClaveAfiliado() != null  && usuario.getClaveAfiliado().length()>0) {
			userWebService.setClaveafiliado(usuario.getClaveAfiliado());
			campos.add("clave_afiliado = '" + usuario.getClaveAfiliado() + "'");
		}



		if (solicitud.getCompania() != null) {
			userWebService.setOrganization(solicitud.getCompania());
			campos.add("organization = '" + solicitud.getCompania() + "'");
		}

		if (solicitud.getMunicipio() != null) {
			userWebService.setPostOfficeBox(solicitud.getMunicipio());
			campos.add("office_addr3 = '" + solicitud.getMunicipio() + "'");
		}

		if (solicitud.getColonia() != null) {
			userWebService.setPostalAddress(solicitud.getColonia());
			campos.add("office_addr2 = '" + solicitud.getColonia() + "'");
		}

		if (solicitud.getCodigoPostal() != null) {
			userWebService.setPostalCode(solicitud.getCodigoPostal());
			campos.add("office_zip = '" + solicitud.getCodigoPostal() + "'");
		}


		if (solicitud.getRFC() != null) {
			userWebService.setRfc(solicitud.getRFC());
			campos.add("rfc = '" + solicitud.getRFC() + "'");
		}

		if (solicitud.getEstado() != null) {
			userWebService.setState(solicitud.getEstado());
			campos.add("office_state = '" + solicitud.getEstado() + "'");

		}

		if (solicitud.getDireccion() != null) {
			userWebService.setStreet(solicitud.getDireccion());
			campos.add("office_addr1 = '" + solicitud.getDireccion() + "'");
		}

		if (solicitud.getPais() != null) {
			userWebService.setCountry(solicitud.getPais());
			campos.add("office_country = '" + solicitud.getPais() + "'");
		}

		if (solicitud.getTelefono() != null) {
			userWebService.setTelephoneNumber(solicitud.getTelefono());
			campos.add("work_phone = '" + solicitud.getTelefono() + "'");
		}

		if (solicitud.getFax() != null) {
			userWebService.setFacsimileTelephoneNumber(solicitud.getFax());
			campos.add("fax = '" + solicitud.getFax() + "'");
		}

		this.proxyWebService.updateUserAttributes(userWebService,
				this.oidInstance);

		boolean exito = true;
		try {
			con.conexionDB();
			if (campos.size()>0) {
				String strSQL = "UPDATE ag_person SET " +
						Comunes.implode(",", campos) +
						" WHERE user_name = '" + usuario.getLogin() + "'";
				con.ejecutaSQL(strSQL);
			}
		} catch (Exception e) {
			exito = false;
			throw e;
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Elimina los par�metros exclusivos de N@E del usuario especificado y
	 * el perfil de N@E
	 *
	 * @param login Login del usuario
	 */

	public void inhabilitarUsuario(String login) throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (login == null || login.equals("")) {
				throw new Exception("Los par�metros son requeridos");
			}
			Usuario usr = this.getUsuario(login);
			if (usr == null || usr.getLogin() == null || usr.getLogin().equals("")) {
				throw new Exception("El usuario especificado no existe");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +	" login=" + login);
			throw e;
		}

		//***************************************************************************************
		com_chermansolutions_ldap_oid_user_UserBean userWebService = new com_chermansolutions_ldap_oid_user_UserBean();

		userWebService.setCommonName(login);

		//No se puede mandar cadena vacia... no hace el cambio... por eso se coloca - y 0
		userWebService.setTipoafiliado("-");
		userWebService.setClaveafiliado("0");

		this.proxyWebService.updateUserAttributes(userWebService,
				this.oidInstance);

		this.eliminarPerfilUsuario(login);
	}


	/**
	 * Habilita a una cuenta del OID para que pueda ingresar a N@E
	 *
	 * @param login Login del usuario
	 * @param claveAfiliado Clave del afiliado de N@E a asignar
	 * @param tipoAfiliado Tipo del afiliado en N@E a asignar
	 * @param perfil Perfil de N@E a asignar
	 *
	 */

	public void habilitarUsuario(String login, String claveAfiliado,
			String tipoAfiliado, String perfil) throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if ( login == null || login.equals("") ||
					claveAfiliado == null || claveAfiliado.equals("") ||
					tipoAfiliado == null || tipoAfiliado.equals("") ||
					perfil == null || perfil.equals("") ) {
				throw new Exception("Los par�metros son requeridos");
			}
			Usuario usr = this.getUsuario(login);
			if (usr == null || usr.getLogin() == null || usr.getLogin().equals("")) {
				throw new Exception("El usuario especificado no existe");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +	"\n" +
					" login=" + login + "\n" +
					" claveAfiliado=" + claveAfiliado + "\n" +
					" tipoAfiliado=" + tipoAfiliado + "\n" +
					" perfil=" + perfil + "\n");
			throw e;
		}

		//***************************************************************************************
		com_chermansolutions_ldap_oid_user_UserBean userWebService = new com_chermansolutions_ldap_oid_user_UserBean();

		userWebService.setCommonName(login);

		userWebService.setTipoafiliado(tipoAfiliado);
		userWebService.setClaveafiliado(claveAfiliado);

		this.proxyWebService.updateUserAttributes(userWebService,
				this.oidInstance);

		this.setPerfilUsuario(login, perfil);
	}



	/**
	 * Determina si un usuario se encuentra dentro del perfil especificado.
	 * En N@E el t�rmino "grupo" es equivalente a "perfil".
	 * @param idUsuario Login del usuario
	 * @param perfil Nombre del perfil
	 *
	 * @return true si pertenece al grupo o false de lo contrario
	 */
	public boolean esUsuarioConPerfil(String idUsuario, String perfil)
			throws Exception {

		boolean esUsuarioEnGrupo = false;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null || perfil == null ||
					idUsuario.equals("") || perfil.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos o vacios");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" idUsuario=" + idUsuario +
					" perfil=" + perfil );
			throw e;
		}
		//***************************************************************************************

		//No requiere convertirlo a DN
		//String dnPerfil = this.getPerfilFormatoDN(perfil);
		//esUsuarioEnGrupo = (this.proxyWebService.isMemberShipGroup(idUsuario, PREFIJO_GRUPO_NE + perfil, this.oidInstance)).booleanValue();
		esUsuarioEnGrupo = this.proxyWebService.isMemberShipGroup(idUsuario, PREFIJO_GRUPO_NE + perfil, this.oidInstance);

		return esUsuarioEnGrupo;
	}

	/**
	 * Determina si el usuario existe y la contrase�a proporcionada es
	 * correcta.
	 *
	 * @param idUsuario Login del usuario
	 * @param pwd Contrase�a del usuario
	 *
	 * @return true si es un usuario v�lido. false si ocurre una excepci�n de
	 * 	cualquier tipo o el usuario no es v�lido
	 */
	public boolean esUsuarioValido(String idUsuario, String pwd)
			throws Exception {
		boolean esUsuarioValido = false;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null || pwd == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" idUsuario=" + idUsuario +
					" pwd=" + pwd );
			throw e;
		}

		//***************************************************************************************
		esUsuarioValido = this.proxyWebService.isValidUser(idUsuario, pwd, this.oidInstance);

		return esUsuarioValido;

	}

	/**
	 * Cambia la contrase�an de un usuario dentro del OID
	 *
	 * @param idUsuario Clave del usuario
	 * @param passwordAnterior Password Anterior
	 * @param passwordNuevo Password nuevo
	 * @return Mensaje de error en caso de que lo hubiese
	 */

	public String cambiarPassword(String idUsuario,
				String passwordAnterior, String passwordNuevo)
			throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null || passwordAnterior == null ||
					passwordNuevo == null) {
				throw new Exception("La solicitud no es valida");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() + "\n" +
					" idUsuario=" + idUsuario + "\n"+
					" passwordAnterior=" + passwordAnterior + "\n"+
					" passwordNuevo=" + passwordNuevo);
			throw e;
		}
		//***************************************************************************************

		return this.proxyWebService2.changePassword(idUsuario,
				passwordAnterior, passwordNuevo, this.oidInstance);
	}


	/**
	 * Complementa los datos de la solicitud (objeto solicitud),
	 * con base en la clave y tipo de afiliado
	 * @param solicitud Solicitud para enviar al sistema Argus. Sobre este objeto se
	 * 		complementan los datos.
	 * @param tipoAfiliado Tipo de afiliado: N Nafin, E Epo, I IF, D Distribuidor de fondos, P Pyme
	 * @param claveAfiliado Clave del Afiliado:
	 * 		Nafin. Numero Sirac
	 * 		Epo. ic_epo Clave de la Epo
	 * 		IF. ic_if Clave del IF
	 *    Pyme. ic_pyme Clave de la Pyme
	 */
	public String complementarSolicitudArgus(SolicitudArgus solicitud,
			String tipoAfiliado, String claveAfiliado) throws Exception {

		AccesoDB con = new AccesoDB();
		String mensaje = "";
		String compania = null;
		String rfc = null;
		String direccion = null;
		String colonia = null;
		String municipio = null;
		String estado = null;
		String codigoPostal = null;
		String telefono = null;
		String fax = null;
		String pais = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (solicitud == null || tipoAfiliado == null ||
					claveAfiliado == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}

		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" solicitud=" + solicitud +
					" tipoAfiliado=" + tipoAfiliado +
					" claveAfiliado=" + claveAfiliado );
			throw e;
		}


		try {
			con.conexionDB();
			if (tipoAfiliado.equals("N")) {	//Nafin
				compania = "NACIONAL FINANCIERA S.N.C.";
				rfc = "NFI-340630-5TO";
				direccion = "INSURGENTES SUR 1971";
				colonia = "GUADALUPE INN";
				codigoPostal = "01020";
				municipio = "ALVARO OBREGON";
				estado = "DISTR. FEDERAL";
				telefono = "018006234672";
				pais = "MEXICO";
			} else if (tipoAfiliado.equals("I")) {	//IF
				String query =
						" SELECT i.cg_razon_social, " +
						" i.cg_rfc, " +
						" d.cg_calle || ' ' || d.cg_numero_ext || ' ' || d.cg_numero_int AS cg_calle_numero, " +
						" d.cg_colonia, m.cd_nombre as municipio, edo.cd_nombre as estado, " +
						" d.cn_cp, d.cg_telefono1, d.cg_fax, p.cd_descripcion as pais  " +
						" FROM comcat_if i, com_domicilio d, comcat_estado edo, comcat_municipio m, " +
						" 	comcat_pais p " +
						" WHERE i.ic_if = d.ic_if " +
						" AND d.cs_fiscal = 'S' " +
						" AND d.ic_municipio = m.ic_municipio " +
						" AND d.ic_estado = m.ic_estado " +
						" AND d.ic_pais = m.ic_pais " +
						" AND d.ic_pais = p.ic_pais " +
						" AND d.ic_estado = edo.ic_estado "+
						" AND i.ic_if = ? ";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de el Intermediario "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de el Intermediario "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de el Intermediario "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de el Intermediario "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de el Intermediario "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de el Intermediario "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();
			} else if (tipoAfiliado.equals("E")) {	//EPO
				String query =
						" SELECT e.cg_razon_social, " +
						" e.cg_rfc, " +
						" d.cg_calle || ' ' || d.cg_numero_ext || ' ' || d.cg_numero_int AS cg_calle_numero, " +
						" d.cg_colonia, m.cd_nombre as municipio, edo.cd_nombre as estado, " +
						" d.cn_cp, d.cg_telefono1, d.cg_fax, p.cd_descripcion as pais " +
						" FROM comcat_epo e, com_domicilio d, comcat_estado edo, comcat_municipio m, " +
						" 		comcat_pais p " +
						" WHERE e.ic_epo = d.ic_epo " +
						" AND d.cs_fiscal = 'S' " +
						" AND d.ic_municipio = m.ic_municipio " +
						" AND d.ic_estado = m.ic_estado " +
						" AND d.ic_pais = m.ic_pais " +
						" AND d.ic_pais = p.ic_pais " +
						" AND d.ic_estado = edo.ic_estado "+
						" AND e.ic_epo = ? ";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de la Epo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de la Epo "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de la Epo "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de la Epo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de la Epo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de la Epo "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();
			} else if (tipoAfiliado.equals("D")) {	//Distribuidor
				String query =
						" SELECT dis.cg_razon_social, " +
						" dis.cg_rfc, " +
						" d.cg_calle || ' ' || d.cg_numero_ext || ' ' || d.cg_numero_int as cg_calle_numero, " +
						" d.cg_colonia, m.cd_nombre as municipio, edo.cd_nombre as estado, " +
						" d.cn_cp, d.cg_telefono1, d.cg_fax, p.cd_descripcion as pais " +
						" from foncat_distribuidor dis, com_domicilio d, comcat_estado edo," +
						" 		comcat_pais p, comcat_municipio m " +
						" where dis.ic_distribuidor = d.ic_distribuidor " +
						" and d.cs_fiscal = 'S' " +
						" and d.ic_municipio = m.ic_municipio " +
						" and d.ic_estado = m.ic_estado " +
						" and d.ic_pais = m.ic_pais " +
						" and d.ic_pais = p.ic_pais " +
						" and d.ic_estado = edo.ic_estado " +
						" and dis.ic_distribuidor = ? ";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de el Distribuidor "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de el Distribuidor "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de el Distribuidor "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();

			}  else if (tipoAfiliado.equals("M")) {	//MANDANTE


				String query = "	select M.CG_RAZON_SOCIAL as RAZONSOCIAL, "+
									"  M.CG_RFC AS RFC,  "+
									"	 M.CG_CALLE AS CALLE, "+
									"	 M.CG_COLONIA AS COLONIA, "+
									"	 MU.CD_NOMBRE AS MUNICIPIO, "+
									"	 E.CD_NOMBRE AS ESTADO, "+
									"	 M.CG_CODIGOPOSTAL AS CODIGOPOSTAL, "+
									"	 M.CG_TELEFONO AS TELEFONO, "+
									"	 M.CG_FAX AS FAX, "+
									"	 M.CG_CODIGOPOSTAL AS CODIGOPOSTAL, "+
									"	 M.CG_TELEFONO AS TELEFONO, "+
									"	 M.CG_FAX   AS FAX, "+
									"	 P.CD_DESCRIPCION AS PAIS "+
									"	 FROM COMCAT_MANDANTE M, comcat_estado E, COMCAT_MUNICIPIO MU, COMCAT_PAIS P  "+
									"	 WHERE M.IC_MANDANTE  = ?  "+
									"	 AND M.IC_ESTADO = E.IC_ESTADO "+
									"	 AND M.IC_PAIS_ORIGEN = p.ic_pais  "+
									"	 AND M.IC_DELEGACION =  MU.IC_MUNICIPIO "+
									"	 AND E.IC_ESTADO =  MU.IC_ESTADO "+
									"	 AND P.ic_pais =  MU.ic_pais ";



				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("RAZONSOCIAL");
				rfc = rs.getString("RFC");
				direccion = rs.getString("CALLE");
				colonia = rs.getString("COLONIA");
				municipio = rs.getString("MUNICIPIO");
				estado = rs.getString("ESTADO");
				pais = rs.getString("PAIS");
				codigoPostal = rs.getString("CODIGOPOSTAL");
				telefono = rs.getString("TELEFONO");
				fax = rs.getString("FAX");

				mensaje = (rfc==null)?" El rfc de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de el Distribuidor "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de el Distribuidor "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de el Distribuidor "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de el Distribuidor "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();


			} else if (tipoAfiliado.equals("P")) {	//Pyme
				String query =
						" SELECT nvl(py.cg_razon_social,py.cg_appat || ' ' || py.cg_apmat|| ' ' || py.cg_nombre) as cg_razon_social, " +
						" py.cg_rfc, " +
						" d.cg_calle || ' ' || d.cg_numero_ext || ' ' || d.cg_numero_int AS cg_calle_numero, " +
						" d.cg_colonia, m.cd_nombre as municipio, edo.cd_nombre as estado, " +
						" d.cn_cp, d.cg_telefono1, d.cg_fax, p.cd_descripcion as pais " +
						" FROM comcat_pyme py, com_domicilio d, comcat_estado edo, comcat_municipio m, " +
						" 		comcat_pais p " +
						" WHERE py.ic_pyme = d.ic_pyme " +
						" AND d.cs_fiscal = 'S' " +
						" AND d.ic_municipio = m.ic_municipio " +
						" AND d.ic_estado = m.ic_estado " +
						" AND d.ic_pais = m.ic_pais " +
						" AND d.ic_pais = p.ic_pais " +
						" AND d.ic_estado = edo.ic_estado "+
						" AND py.ic_pyme = ? ";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de la Pyme "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de la Pyme "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de la Pyme "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de la Pyme "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de la Pyme "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de la Pyme "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();
			} else if (tipoAfiliado.equals("A")) {	//Afianzadora
				String query =
						" SELECT afia.cg_razon_social AS cg_razon_social,"+
						" afia.cg_rfc AS cg_rfc,"+
						" dom.cg_calle || ' ' || dom.cg_numero_ext || ' ' || dom.cg_numero_int AS cg_calle_numero,"+
						" dom.cg_colonia AS cg_colonia,"+
						" mun.cd_nombre AS municipio,"+
						" edo.cd_nombre AS estado,"+
						" dom.cn_cp AS cn_cp,"+
						" dom.cg_telefono1 AS cg_telefono1,"+
						" dom.cg_fax AS cg_fax,"+
						" pais.cd_descripcion AS pais"+
						" FROM fe_afianzadora afia"+
						", com_domicilio dom"+
						", comcat_estado edo"+
						", comcat_municipio mun"+
						", comcat_pais pais"+
						" WHERE afia.ic_afianzadora = dom.ic_afianzadora"+
						" AND dom.ic_pais = pais.ic_pais"+
						" AND dom.ic_estado = edo.ic_estado"+
						" AND dom.ic_pais = edo.ic_pais"+
						" AND dom.ic_municipio = mun.ic_municipio"+
						" AND dom.ic_estado = mun.ic_estado"+
						" AND dom.ic_pais = mun.ic_pais"+
						" AND dom.cs_fiscal = 'S'"+
						" AND afia.ic_afianzadora = ?";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de la Afianzadora "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de la Afianzadora "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de la Afianzadora "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de la Afianzadora "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de la Afianzadora "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de la Afianzadora "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();
			} else if (tipoAfiliado.equals("F")) {	//Fiado
				String query =
						"SELECT f.cg_nmombre AS cg_razon_social, f.cg_rfc AS cg_rfc, " +
						"          f.cg_calle " +
						"       || ' ' " +
						"       || f.cg_noexterior " +
						"       || ' ' " +
						"       || f.cg_nointerior AS cg_calle_numero, " +
						"       f.cg_colonia AS cg_colonia, f.cg_municipio AS municipio, " +
						"       f.cg_estado AS estado, f.cg_cp AS cn_cp, " +
						"       f.cg_rl_telefono AS cg_telefono1, f.cg_pais AS pais " +
						"  FROM fe_fiado f " +
						" WHERE f.ic_fiado = ? ";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = "";//rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc del Fiado "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion del Fiado "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia del Fiado "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado del Fiado "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal del Fiado "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono del Fiado "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close();
				
			} else if (tipoAfiliado.equals("U")) {	//Universidad
				String query =
						" SELECT u.cg_razon_social AS cg_razon_social,"+
						" u.cg_rfc AS cg_rfc,"+
						" dom.cg_calle || ' ' || dom.cg_numero_ext || ' ' || dom.cg_numero_int AS cg_calle_numero,"+
						" dom.cg_colonia AS cg_colonia,"+
						" mun.cd_nombre AS municipio,"+
						" edo.cd_nombre AS estado,"+
						" dom.cn_cp AS cn_cp,"+
						" dom.cg_telefono1 AS cg_telefono1,"+
						" dom.cg_fax AS cg_fax,"+
						" pais.cd_descripcion AS pais"+
						" FROM COMCAT_UNIVERSIDAD u"+
						", com_domicilio dom"+
						", comcat_estado edo"+ 
						", comcat_municipio mun"+
						", comcat_pais pais"+
						" WHERE u.ic_universidad = dom.ic_universidad"+
						" AND dom.ic_pais = pais.ic_pais"+
						" AND dom.ic_estado = edo.ic_estado"+
						" AND dom.ic_pais = edo.ic_pais"+
						" AND dom.ic_municipio = mun.ic_municipio"+
						" AND dom.ic_estado = mun.ic_estado"+
						" AND dom.ic_pais = mun.ic_pais"+
						" AND dom.cs_fiscal = 'S'"+
						" AND u.ic_universidad = ?";

				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();

				compania = rs.getString("cg_razon_social");
				rfc = rs.getString("cg_rfc");
				direccion = rs.getString("cg_calle_numero");
				colonia = rs.getString("cg_colonia");
				municipio = rs.getString("municipio");
				estado = rs.getString("estado");
				pais = rs.getString("pais");
				codigoPostal = rs.getString("cn_cp");
				telefono = rs.getString("cg_telefono1");
				fax = rs.getString("cg_fax");

				mensaje = (rfc==null)?" El rfc de la Universidad "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion de la Universidad "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia de la Universidad "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado de la Universidad "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal de la Universidad "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono de la Universidad "+compania+" es nulo, debes capturalo. ":"";

				rs.close();
				ps.close(); 
		
		} else if (tipoAfiliado.equals("C")) {	//Cliente Externo  F01-2015 
				String query = " SELECT     "+
					"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO,    "+
					"	DECODE (c.CS_TIPO_PERSONA,  'F', c.CG_NOMBRE || ' ' || c.CG_APPAT || ' ' || c.CG_APMAT ,    c.CG_RAZON_SOCIAL  ) AS NOMBRE_RAZON_SOCIAL,  "+  
				   " 	c.CG_RFC AS  RFC ,   "+
					"	d.CG_CALLE  as CALLE, "+
					"  d.CG_COLONIA  as  COLONIA,  "+
					"	e.CD_NOMBRE as  ESTADO ,  "+
					" 	d.cg_telefono1 AS TELEFONO , "+ 
				   " 	d.cn_cp as codigo_postal ,      "+
					"  m.CD_NOMBRE as municipio ,  "+
					"  p.CD_DESCRIPCION as PAIS,  "+
					"  d.CG_FAX as FAX "+
					" 	FROM COMCAT_CLI_EXTERNO  c,   "+
					" 	com_domicilio d  , "+
					" 	comcat_estado e , "+
					" 	comcat_municipio m, "+
					" comcat_pais p  "+
					" WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
					"	AND d.ic_pais = p.ic_pais  "+
					 "	AND d.ic_estado = e.ic_estado  "+
					 "	AND d.ic_pais = e.ic_pais  "+
					 "	AND d.ic_municipio = m.ic_municipio "+
					 "	AND d.ic_estado = m.ic_estado  "+
					 "	AND d.ic_pais = m.ic_pais  "+
					 " and  d.IC_NAFIN_ELECTRONICO = ? ";
				
				log.debug("query === "+query);
				log.debug("claveAfiliado === "+claveAfiliado);   
				
				PreparedStatement ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(claveAfiliado));
				ResultSet rs = ps.executeQuery();
				rs.next();
				compania = rs.getString("NOMBRE_RAZON_SOCIAL");
				rfc = rs.getString("RFC");
				direccion = rs.getString("CALLE");
				colonia = rs.getString("COLONIA");
				municipio = rs.getString("municipio");
				estado = rs.getString("ESTADO");
				pais = rs.getString("PAIS");
				codigoPostal = rs.getString("codigo_postal");
				telefono = rs.getString("TELEFONO");
				fax = rs.getString("FAX");
				mensaje = (rfc==null)?" El rfc del Cliente Externo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (direccion==null)?" La direccion del Cliente Externo "+compania+" es nula, debes capturala ":"";
				mensaje += (colonia==null)?" El colonia del Cliente Externo "+compania+" es nula, debes capturala ":"";
				mensaje += (estado==null)?" El estado del Cliente Externo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (codigoPostal==null)?" El codigo postal del Cliente Externo "+compania+" es nulo, debes capturalo ":"";
				mensaje += (telefono==null)?" El tel�fono del Cliente Externo "+compania+" es nulo, debes capturalo. ":"";
				rs.close(); 
				ps.close();
			}
                        else if (tipoAfiliado.equals("BXT")){
                                compania = "NACIONAL FINANCIERA S.N.C.";
                                rfc = "NFI-340630-5TO";
                                direccion = "INSURGENTES SUR 1971";
                                colonia = "GUADALUPE INN";
                                codigoPostal = "01020";
                                municipio = "ALVARO OBREGON";
                                estado = "DISTR. FEDERAL";
                                telefono = "018006234672";
                                pais = "MEXICO";
                        }
                        else {
				throw new Exception("El tipo de Afiliado " + tipoAfiliado + " no es soportado");
			}
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		solicitud.setCompania(compania);
		solicitud.setRFC(rfc);
		solicitud.setDireccion(direccion);
		solicitud.setColonia(colonia);
		solicitud.setCodigoPostal(codigoPostal);
		solicitud.setMunicipio(municipio);
		solicitud.setEstado(estado);
		solicitud.setPais(pais);
		solicitud.setTelefono(telefono);
		solicitud.setFax(fax);

		return mensaje;
	}

	/**
	 * Obtiene el perfil del usuario especificado. <br>
	 * Este metodo asume que el usuario solo tiene un perfil asociado
	 *
	 * @param idUsuario Login del usuario
	 *
	 */
	public String getPerfilUsuario(String idUsuario) throws Exception {

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null || idUsuario.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" idUsuario=" + idUsuario);
			throw e;
		}

		//***************************************************************************************

		String dnPerfil = null;

		//Si el usuario es enlac_aut o proc_aut no se debe realizar la busqueda en el OID
		//ya que este usuario es solo de uso interno para identificar que
		//fue realizado por un proceso automatico
		if (idUsuario.equals("enlac_aut") || idUsuario.equals("proc_aut")) {
			dnPerfil = null;
		} else {
			dnPerfil = this.proxyWebService.userRoles(idUsuario,
				PREFIJO_GRUPO_NE, this.oidInstance) ;
		}
		if (dnPerfil == null) {
			dnPerfil = "";
		}

		return this.getPerfilFormatoNE(dnPerfil);
	}

	/**
	 * Obtiene los perfiles que tienen como prefijo el valor prefijoPerfil
	 * del usuario especificado
	 *
	 * @param idUsuario Login del usuario
	 * @param prefijoPerfil Prefijo del perfil
	 * @return Lista de perfiles que cumplen con el prefijo especificado
	 *
	 */
	public List<String> getPerfilUsuarioConPrefijo(String idUsuario,
			String prefijoPerfil) throws Exception {

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null || idUsuario.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" idUsuario=" + idUsuario);
			throw e;
		}

		//***************************************************************************************

		String dnPerfil = this.proxyWebService.userRoles(idUsuario,
				prefijoPerfil, this.oidInstance) ;  //el parametro prefijo de userRoles no funciona en el Webservice del ARGUS. No importa el valor que se pasa
		if (dnPerfil == null) {
			dnPerfil = "";
		}

		return this.getPerfilXPrefijo(dnPerfil, prefijoPerfil);
	}

	/**
	 * Obtiene la informaci�n del usuario especificado <br>
	 * Con fines de consulta el perfil del usuario puede ser obtenido de
	 * dos formas: mediante {@link #getPerfilUsuario(String) getPerfilUsuario} <br>
	 * o mediante getPerfil() del objeto "Usuario"
	 * Si el idUsuario no existe, regresa un objeto usuario vacio (no nulo).
	 * Si el usuario es enlac_aut o proc_aut siempre regresa un objeto vacio
	 * @param idUsuario Login del usuario
	 * @return Objeto con la informaci�n del usuario
	 */
	/**
	public Usuario getUsuario(String idUsuario) throws Exception {
		Usuario usuario = new Usuario();
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (idUsuario == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
                        e.getMessage()+" idUsuario=" + idUsuario);
			throw e;
		}

		//***************************************************************************************
		//Si el usuario es enlac_aut o proc_aut, no se realiza la peticion al OID
		//ya que estos usuarios son unicamente de uso interno para identificar un
		//proceso automatico
		if (!idUsuario.equals("enlac_aut") && !idUsuario.equals("proc_aut")) {
			try{
                                log.info("####### this.oidInstance: "+this.oidInstance);
                                com_chermansolutions_ldap_oid_user_UserBean	userWebService = this.proxyWebService.userInformation(idUsuario, this.oidInstance);

		//Llena los datos del objeto usuario, con aquellos provenientes del Web Service
				if(userWebService!=null){ // Si el SI usuario exite
					usuario = new Usuario();
					usuario.setLogin(userWebService.getCommonName()==null?"":userWebService.getCommonName());
					usuario.setEmail(userWebService.getMail()==null?"":userWebService.getMail());
					//Esta modificacion no aplica para desarrollo ya que ese ambiente no se cuenta con el mismo WS
					//usuario.setApellidoPaterno(userWebService.getMiddleName());
					usuario.setApellidoPaterno(userWebService.getSurname()==null?"":userWebService.getSurname());
					usuario.setApellidoMaterno(userWebService.getLastName()==null?"":userWebService.getLastName());
					usuario.setNombre(userWebService.getGivenName()==null?"":userWebService.getGivenName());
					usuario.setClaveAfiliado(userWebService.getClaveafiliado()==null?"":userWebService.getClaveafiliado());
					usuario.setTipoAfiliado(userWebService.getTipoafiliado()==null?"":userWebService.getTipoafiliado());
					usuario.setRfc(userWebService.getRfc() == null?"":userWebService.getRfc());
					usuario.setNombreOrganizacionUsuario(userWebService.getOrganization()==null?"":userWebService.getOrganization());

		SimpleDateFormat formatter = new SimpleDateFormat ("dd/MM/yyyy");

		//Si el formato no concuerda con dd/mm/yyyy se obtiene null
		java.util.Date fechaNacimiento = formatter.parse(
					userWebService.getOrclDateofBirth(), new ParsePosition(0));
		usuario.setFechaNacimiento(fechaNacimiento);

		//NE_ es el prefijo asignado en ARGUS, a los perfiles de N@E.
		String perfil = this.getPerfilUsuario(idUsuario);

		usuario.setPerfil(perfil);
		}
			}catch(Exception e){
                                //e.printStackTrace();
				//log.error(e);
			}
		}
		return usuario;

	}

	 */
        
 /**
     *iMPLEMENTACION TEMPORAL PARA DESARROLLO REMOTO BLUEPEOPLE
     */
    public Usuario getUsuario(String idUsuario) throws Exception {
        Usuario usuario = new Usuario();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query =
            "SELECT NAME,CLAVE_AFILIADO,TIPO_AFILIADO,EMAIL,NOMBRE,APELLIDOMATERNO,APELLIDOPATERNO FROM DIRECTORY_GROUP_TIPO_CLAVE WHERE MEMBERCN = ?";
        //**********************************Validaci�n de parametros:*****************************
        try {
            if (idUsuario == null) {
                throw new Exception("Los parametros no pueden ser nulos");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error en los parametros recibidos. " + e.getMessage() + " idUsuario=" + idUsuario);
            throw e;
        }
        if (!idUsuario.equals("enlac_aut") && !idUsuario.equals("proc_aut")) {
            try {

                con.conexionDB();
                ps = con.queryPrecompilado(query);
                log.info(query);
                log.info("idUsuario= " + idUsuario);
                ps.setString(1, "cn=".concat(idUsuario.trim()));
                rs = ps.executeQuery();
                if (rs.next()) {
                    usuario.setEmail(rs.getString("EMAIL"));
                    usuario.setApellidoPaterno(rs.getString("APELLIDOPATERNO"));
                    usuario.setApellidoMaterno(rs.getString("APELLIDOMATERNO"));
                    usuario.setNombre(rs.getString("NOMBRE"));
                    usuario.setClaveAfiliado(rs.getString("CLAVE_AFILIADO"));
                    usuario.setTipoAfiliado(rs.getString("TIPO_AFILIADO"));
                    String perfil = corrigePerfil(rs.getString("NAME"));
                    log.info("SET PERFIL= " + perfil);
                    usuario.setPerfil(perfil);
                    //usuario.setRfc();
                    //usuario.setNombreOrganizacionUsuario();
                    //usuario.setFechaNacimiento(fechaNacimiento);

                } else {
                    log.error("El usuarion no existe en la BD idususuario = " + idUsuario);
                }

            } catch (SQLException | NamingException e) {
                log.error(e.getMessage());
                e.printStackTrace();
                throw new AppException("Error inesperado al realizar login", e);
            } finally {
                AccesoDB.cerrarResultSet(rs);
                AccesoDB.cerrarStatement(ps);
                if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
            }
        }
        return usuario;

    }

    public String corrigePerfil(String perfil) {
        if (perfil.startsWith("NE_")) {
            return perfil.substring(3, perfil.length()).replace("_", " ");
        } else if (perfil.startsWith("Grupo ")) {
            return perfil.substring(6, perfil.length()).replace("_", " ");
        } else {
            return perfil;
        }
    }


    /**
     * Obtiene un arreglo de las claves de los usuarios que pertenecen al
     * afiliado especificado
     *
     * @param claveAfiliado Clave del Afiliado
     * @param tipoAfiliado Tipo de afiliado (N Nafin, E EPO, I If, D Distribuidor, P Pyme � Distribuidor)
     *
     * @return Arreglo que contiene el login de cada uno de los usuarios que
     * 		pertenecen al afiliado. Si no hay usuarios regresa una lista
     *       vacia, nunca null.
     */
	public List<String> getUsuariosxAfiliado(String claveAfiliado, String tipoAfiliado)
			throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveAfiliado == null || tipoAfiliado == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +	" claveAfiliado=" + claveAfiliado +
					" tipoAfiliado=" + tipoAfiliado);
			throw e;
		}

		//***************************************************************************************
                
		ValidaServicio vs = new ValidaServicio();
		List<String> listaLoginUsuarios = new ArrayList<>();
		String[] loginUsuarios ;
		try {
                    if(vs.isWebServiceAvailable(ParametrosArgus.urlOIDWebService)){
                        if( tipoAfiliado.equals("N") && "".equals(claveAfiliado) ) {
                            loginUsuarios = this.proxyWebService.lookupCommonName(
                                "tipoafiliado="+ tipoAfiliado , this.oidInstance);
                        } else {
                            loginUsuarios = this.proxyWebService.lookupCommonName(
                                "(&(claveafiliado=" + claveAfiliado + ")(tipoafiliado=" +
                                tipoAfiliado + "))", this.oidInstance);
                        }
                        if (loginUsuarios != null) {
                            for (int i = 0; i < loginUsuarios.length; i++) {
                                if (loginUsuarios[i]!=null && !loginUsuarios[i].equals("java.lang.ArrayIndexOutOfBoundsException")) {
                                    listaLoginUsuarios.add(loginUsuarios[i]);
                                }
                            }
                        }
                    } else
                        throw new Exception("OIDWebService no disponible (isValidUser). "+ParametrosArgus.urlOIDWebService);
		} catch(Throwable t) {
                    t.printStackTrace();
		}
		return listaLoginUsuarios;
	}
        
    /**
     * Obtiene un arreglo de las claves de los usuarios que pertenecen al
     * afiliado especificado por perfil
     *
     * @param claveAfiliado Clave del Afiliado
     * @param tipoAfiliado Tipo de afiliado (N Nafin, E EPO, I If, D Distribuidor, P Pyme � Distribuidor)
     * @param perfilAfiliado Perfil Afiliado
     *
     * @return Arreglo que contiene el login de cada uno de los usuarios que
     *              pertenecen al afiliado. Si no hay usuarios regresa una lista
     *       vacia, nunca null.
     */
        public List<String> getUsuariosxAfiliadoxPerfil(String claveAfiliado, String tipoAfiliado, String perfilAfiliado) throws Exception {
            //**********************************Validaci�n de parametros:*****************************
            try {
                if (claveAfiliado == null || tipoAfiliado == null|| perfilAfiliado == null) {
                    throw new Exception("Los parametros no pueden ser nulos");
                }
            } catch(Exception e) {
                e.printStackTrace();
                log.error("Error en los parametros recibidos. " + e.getMessage() + " claveAfiliado=" + claveAfiliado + " tipoAfiliado=" + tipoAfiliado + " perfilAfiliado=" + perfilAfiliado);
                throw e;
            }
            //***************************************************************************************
            AccesoDB con = new AccesoDB();
            List<String> listaLoginUsuarios = new ArrayList<>();
            String[] loginUsuarios ;
            try {
                con.conexionDB();
                String strSQL =
                    "SELECT "+
                    "    replace(membercn, 'cn=', '') ic_usuario  "+
                    "  FROM "+
                    "    directory_group_tipo_clave "+
                    " WHERE "+
                    "        tipo_afiliado = ? ";
                if(!"".equals(claveAfiliado)) {
                    strSQL+= " AND clave_afiliado = ? ";
                }
                if(!"".equals(perfilAfiliado)) {
                    strSQL+= " AND rdn = ? ";
                }
                PreparedStatement ps = con.queryPrecompilado(strSQL);
                ps.clearParameters();
                int cont=1;
                ps.setString(cont, tipoAfiliado);cont++;
                if(!"".equals(claveAfiliado)) {
                    ps.setString(cont, claveAfiliado);cont++;
                }
                if(!"".equals(perfilAfiliado)) {
                    ps.setString(cont, "cn=ne_"+perfilAfiliado.toLowerCase());
                }
                ResultSet rs = ps.executeQuery();
                while(rs.next()) {
                    String usuario = rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
                    if(!"".equals(usuario))
                        listaLoginUsuarios.add(usuario);
                }
                rs.close();ps.close();
                con.cierraConexionDB();
            } catch(Throwable t) {
                t.printStackTrace();
            } finally {
                if (con.hayConexionAbierta())
                    con.cierraConexionDB();
            }
            return listaLoginUsuarios;
        }


            /** @deprecated No USAR Todo SALE MAL */
         public List<String> getUsuariosxPerfil(String perfil) throws Exception {

            //**********************************Validaci�n de parametros:*****************************
            try {
                    if (perfil == null || perfil == null) {
                            throw new Exception("Los parametros no pueden ser nulos");
                    }
            } catch(Exception e) {
                    e.printStackTrace();
                    log.error("Error en los parametros recibidos. " + e.getMessage() +        " perfil=" + perfil);
                    throw e;
            }

            //***************************************************************************************

            List<String> listaLoginUsuarios = new ArrayList<>();
            String[] loginUsuarios ;
            try{
            
               loginUsuarios = this.proxyWebService.lookupCommonName("(&(userperfil="+perfil+"))", this.oidInstance);
            
            if (loginUsuarios != null) {
                    for (int i = 0; i < loginUsuarios.length; i++) {
                        if (loginUsuarios[i]!=null && !loginUsuarios[i].equals("java.lang.ArrayIndexOutOfBoundsException")) {
                            listaLoginUsuarios.add(loginUsuarios[i]);
                    }
            }
                    }
            }catch(Throwable t){
                    t.printStackTrace();
            }
            return listaLoginUsuarios;
    }

	/**
	 * Cambia perfil de la solicitud de alta de usuario para N@E
	 * para el afiliado especificado, siempre y cuando la solicitud
	 * aun est� en proceso (estatus=1)
	 *
	 * El metodo se emplea por ejemplo cuando se hace una solcitud de cuenta de consulta
	 * y antes de que se genera la cuenta en el OID, se pasa por otro proceso que la
	 * debe cambiar a cuenta de operaci�n.
	 *
	 *
	 * @param claveAfiliado Clave del afiliado (ic_pyme, ic_epo, ic_if, etc)
	 * @param tipoAfiliado Tipo de afiliado P Pyme I If E Epo etc.
	 */
	public void setPerfilSolicitudAltaUsuario(String claveAfiliado,
			String tipoAfiliado, String perfil) throws Exception {

		AccesoDB con = new AccesoDB();
		boolean exito = true;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveAfiliado == null || tipoAfiliado == null || perfil == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(claveAfiliado);
			if (perfil.equals("")) {
				throw new Exception("El perfil debe ser especificado");
			}

		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " +
					e.getMessage() + "\n " +
					"claveAfiliado=" + claveAfiliado + "\n" +
					"tipoAfiliado=" + tipoAfiliado + "\n" +
					"perfil=" + perfil);
			throw e;
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" UPDATE ag_application_rol " +
					" SET dn = ? " +
					" WHERE status = ? " +
					" 	AND id_person in ( " +
					" 		SELECT id_person " +
					" 		FROM ag_person p " +
					" 		WHERE clave_afiliado = ? " +
					" 			AND tipo_afiliado = ? )";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, this.getPerfilFormatoDN(perfil));	//DN del perfil
			ps.setString(2, "1");	//en proceso
			ps.setString(3, claveAfiliado);
			ps.setString(4, tipoAfiliado);

			ps.executeUpdate();
			ps.close();
		} catch(Exception e ) {
			exito = false;
			e.printStackTrace();
			throw e;
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Determina si ya existe alguna solicitud de ARGUS � ya existe la
	 * cuenta dentro del OID para el afiliado especificado,
	 *
	 * Es posible que este dado de alta dentro del OID y que no haya solicitud,
	 * debido a que varios afiliados fueron cargados via ldif inicialmente al OID.
	 *
	 * @param claveAfiliado Clave del afiliado (ic_pyme, ic_epo, ic_if, etc)
	 * @param tipoAfiliado Tipo de afiliado P Pyme I If E Epo etc.
	 * @return true si existe la solicitud o false de lo contrario
	 */
	public boolean getExisteSolicitudAltaUsuario(String claveAfiliado,
			String tipoAfiliado) throws Exception {

		AccesoDB con = new AccesoDB();
		boolean existe = false;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveAfiliado == null || tipoAfiliado == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(claveAfiliado);

		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() + "\n " +
					"claveAfiliado=" + claveAfiliado + "\n" +
					"tipoAfiliado=" + tipoAfiliado );
			throw e;
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			List usuariosxAfiliado = this.getUsuariosxAfiliado(claveAfiliado, tipoAfiliado);

			if (!usuariosxAfiliado.isEmpty()) { //Hay usuarios en el OID para el afiliado
				existe = true;
			} else {

				String strSQL =
						" SELECT count(*) as numRegistros " +
						" FROM ag_person " +
						" WHERE clave_afiliado = ? " +
						" 	AND tipo_afiliado = ? ";						

				PreparedStatement ps = con.queryPrecompilado(strSQL);
				ps.setString(1, claveAfiliado);
				ps.setString(2, tipoAfiliado);

				ResultSet rs = ps.executeQuery();
				rs.next();
				existe = (rs.getInt("numRegistros") == 0)?false:true;
				rs.close();
				ps.close();
			}
			return existe;
		} catch(Exception e ) {
			e.printStackTrace();
			throw e;
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Agrega un grupo dentro del OID
	 *
	 * @param grupo Nombre del grupo.
	 * @param descripcion Descripcion del grupo.
	 * @return Cadena con el mensaje de error en caso de que hubiese
	 */

	public String registrarGrupo(String grupo)
			throws Exception {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (grupo == null ) {
				throw new Exception("La solicitud no es valida");
			}
		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() + "\n" +
					" grupo=" + grupo);
			throw e;
		}
		//***************************************************************************************

		return this.proxyWebService2.addNewGroupToOID(PREFIJO_GRUPO_NE + grupo,
				"Grupo " + grupo, this.oidInstance);
	}



	/**
	 * Obtiene la cuenta de usuario asignada al proveedor especificado.
	 * @param clavePyme Clave de la Pyme
	 *
	 * @return loginUsuario. Login de la cuenta asignada.
	 * Regresa null, si no existe una cuenta asociada
	 */
	public String getCuentaUsuarioAsociada(String clavePyme)
			throws Exception {

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String cuentaUsuarioAsociada = null;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (clavePyme == null || clavePyme.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			Integer.parseInt(clavePyme);

		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +
					"\n clavePyme = " + clavePyme );
			throw e;
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQL =
					" SELECT cg_cuenta_usr_asociada " +
					" FROM comcat_pyme " +
					" WHERE ic_pyme = ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1,Integer.parseInt(clavePyme));

			rs = ps.executeQuery();

			if (rs.next()) {
				cuentaUsuarioAsociada = rs.getString("cg_cuenta_usr_asociada");
			}
			rs.close();
			ps.close();

		} catch(SQLException sqle) {
			throw sqle;
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return cuentaUsuarioAsociada;
	}



	/**
	 * Genera la solicitud de alta de usuario dentro del ARGUS
	 * (ARGUS es un sistema de administraci�n centralizada de usuarios)
	 * utilizando la informaci�n del objeto "solicitud".
	 * Si el login de 8 digitos no fue enviado genera el login.
	 * @param solicitud Objeto de la clase SolicitudArgus. Contiene la
	 * informaci�n necesaria para registrar un usuario dentro del ARGUS
	 *
	 * @return loginUsuario. Login asignado a la solicitud realizada. Si la
	 * 		solicitud es procesada exitosamente en el ARGUS, se creara un
	 * 		usuario con dicho login
	 */
	public String registrarSolicitudAltaUsuario(SolicitudArgus solicitud)
			throws Exception {

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean exito = true;

		Usuario usuario = null;
		String apellidoPaterno = "";
		String apellidoMaterno = "";
		String nombre = "";

		java.util.Date fechaNacimiento = null;

		String email = "";
		String claveAfiliado = "";
		String tipoAfiliado = "";
		String perfil = "";

		String rfc = "";
		String telefono = "";

		String fax = "";

		String direccion = "";
		String colonia = "";
		String municipio = "";
		String codigoPostal = "";
		String estado = "";

		String pais = "";

		String compania = "";
		String tipoProceso = "";
		String preguntaSecreta = "";
		String respuestaSecreta = "";

		String idUsuario = "";

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (solicitud == null || !solicitud.esSolicitudValida("AGREGAR")) {
				throw new Exception("Los valores de la solicitud estan incompletos");
			}

			usuario = solicitud.getUsuario();

			// Datos del Usuarios
			apellidoPaterno = usuario.getApellidoPaterno();
			apellidoMaterno = usuario.getApellidoMaterno();

			//(2005-06-23)
			//La afiliaci�n deja pasar los usuarios con un espacio en blanco en apellido materno
			//Lo que ocasiona que truene cuando se quiere dar de alta en el OID
			//Por lo cual se realiza la siguiente verificaci�n:
			if (apellidoMaterno.trim().equals("")) {
				apellidoMaterno = "X";
			}

			nombre = usuario.getNombre();
			fechaNacimiento = usuario.getFechaNacimiento();
			email = usuario.getEmail();
			claveAfiliado = usuario.getClaveAfiliado();
			tipoAfiliado = usuario.getTipoAfiliado();

			perfil = usuario.getPerfil();
			// Datos complementarios de la solicitud (De la compania)
			rfc = solicitud.getRFC();
			telefono = solicitud.getTelefono();
			fax = solicitud.getFax();

			direccion = solicitud.getDireccion();
			colonia = solicitud.getColonia();
			municipio = solicitud.getMunicipio();
			estado = solicitud.getEstado();
			pais = solicitud.getPais();
			codigoPostal = solicitud.getCodigoPostal();
			compania = solicitud.getCompania();

			//1 Preautorizada. 0 Requiere autorizacion del usuario administrador.
			if (solicitud.getPreautorizacion() == false) {
				tipoProceso = "0";	//Manual/
				preguntaSecreta = solicitud.getPreguntaSecreta();
				respuestaSecreta = solicitud.getRespuestaSecreta();
			} else {
				tipoProceso = "1";	//Automatico
			}

		} catch(Exception e) {
                        e.printStackTrace();
			log.error("Error en los parametros recibidos. " +
					e.getMessage() +
					"\n solicitud = " + solicitud );
			throw e;
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			String strSQLClave = "SELECT SQ_PERSON.nextval as id_person " +
					" FROM DUAL ";
			rs = con.queryDB(strSQLClave);
			rs.next();
			String idPerson = rs.getString("ID_PERSON");
			rs.close();
			con.cierraStatement();
			//Dado que SQ_PERSON es un objeto en otra base de datos
			//Se requiere terminar transacci�n para que no marque errores
			con.terminaTransaccion(true);

      if(usuario.getLogin()!=null && !"".equals(usuario.getLogin()) && usuario.getLogin().length()==8)
      {
        idUsuario = usuario.getLogin();
      }
      else{
			String strSQLClaveUsuario = "SELECT SQ_PERSON_NETRO.nextval as id_usuario " +
					" FROM DUAL ";
			rs = con.queryDB(strSQLClaveUsuario);
			rs.next();
			idUsuario = rs.getString("ID_USUARIO");
			rs.close();
			con.cierraStatement();
			//Dado que SQ_PERSON_NETRO es una objeto en otra base de datos
			//Se requiere terminar transacci�n para que no marque errores
			con.terminaTransaccion(true);

			idUsuario = Comunes.calculaFolio(idUsuario, 7);
      }
			String strSQL =
					"insert into AG_PERSON " +
					" (id_person, user_name, rfc, " +
					" first_name, middle_name, last_name, email, " +
					" work_phone, office_addr1, office_addr2, " +
					" office_addr3, office_zip, " +
					" office_state, organization, " +
					" password, clave_afiliado, tipo_afiliado," +
					" status, user_type, " +
					" PASSWD_HINT_QUESTION, PASSWD_HINT_ANSWER," +
					" date_of_birth, office_country, fax)" +
					" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(idPerson));
			ps.setString(2, idUsuario);
			ps.setString(3, rfc);
			ps.setString(4, nombre);            // first_name
			ps.setString(5, apellidoPaterno);   // middle_name
			ps.setString(6, apellidoMaterno);   // last_name
			ps.setString(7, email);
			ps.setString(8, telefono);
			ps.setString(9, direccion);
			ps.setString(10, colonia);
			ps.setString(11, municipio);
			ps.setString(12, codigoPostal);
			ps.setString(13, estado);
			ps.setString(14, compania);
			ps.setString(15, "POR GENERAR");	//Valor definido por Nafin (ARGUS)
			ps.setString(16, claveAfiliado);
			ps.setString(17, tipoAfiliado);
			ps.setInt(18, 1); //Valor definido por Nafin (ARGUS)
			ps.setInt(19, 0); //Valor definido por Nafin (ARGUS)
			if(solicitud.getPreautorizacion() == true) {
				ps.setNull(20, Types.VARCHAR);
				ps.setNull(21, Types.VARCHAR);
			} else {
				ps.setString(20, preguntaSecreta);
				ps.setString(21, respuestaSecreta);
			}
			if(fechaNacimiento == null) {
				ps.setNull(22, Types.DATE);
			} else {
				ps.setDate(22, new java.sql.Date(fechaNacimiento.getTime()),Calendar.getInstance());
			}
			if(pais == null || pais.equals("")) {
				ps.setNull(23, Types.VARCHAR);
			} else {
				ps.setString(23, pais);
			}
			if(fax == null || fax.equals("")) {
				ps.setNull(24, Types.VARCHAR);
			} else {
				ps.setString(24, fax);
			}

			ps.executeUpdate();
			ps.close();

			String estatusSolicitud = null; //Por procesar
			if (solicitud.getBloqueo() == true) {
				estatusSolicitud = "3";	//Bloqueado
			} else {
				estatusSolicitud = "1";	//Por procesar
			}

			if("F".equals(tipoAfiliado))
				siteId = ParametrosArgus.siteIdFiado;
			
			
			strSQL = "insert into AG_APPLICATION_ROL " +
					" (id_approl, id_person, id_rol, " +
					" dn, application, date_of_request, " +
					" status, functions, contract, " +
					" term_conditions, operation, site_id, " +
					" tipo_proceso) " +
					" values(SQ_APPLICATION_ROL.nextval, " +
					" ?,?,?,?,sysdate,?,?,?,?,?,?,?) ";
			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(idPerson));
			ps.setLong(2, 000000000000000000000000000000000000000000000);		//La clave numerica del perfil esta por definirse
			ps.setString(3, getPerfilFormatoDN(perfil));
			ps.setString(4, "Nafin Electronico");//// Ver que va en el application
			ps.setString(5, estatusSolicitud);	//Valor definido por Nafin (ARGUS)
			ps.setString(6, "ACCESO N@E");
			ps.setString(7, "00000"); //Valor definido por Nafin (ARGUS)
			ps.setString(8, "1"); //Valor definido por Nafin (ARGUS)
			ps.setString(9, "0"); //Valor definido por Nafin (ARGUS)
			ps.setString(10, this.siteId); //site_id
			ps.setString(11, tipoProceso); //Tipo de proceso de autorizacion: 1 automatico, 0 manual, (F2005-078) 2 Bloqueada

			ps.executeUpdate();
			exito = true;
			ps.close();

		} catch(SQLException sqle) {
			exito = false;
			throw sqle;
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return idUsuario;
	}

/**
 * Establece el perfil al usuario especificado.
 * @param idUsuario Login del usuario
 * @param perfil Nombre del perfil
 *
 */
public void setPerfilUsuario(String idUsuario, String perfil)
		throws Exception {

	try {
		if(idUsuario == null || perfil == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("Error en los parametros recibidos. " +
				e.getMessage() + " idUsuario=" +
				idUsuario + " perfil=" + perfil);
		throw e;
	}

	String perfilAnterior = this.getPerfilUsuario(idUsuario);

	if(perfilAnterior != null && !perfilAnterior.equals("")) {
		proxyWebService.removeMemberShipGroup(idUsuario,
				getPerfilFormatoDN(perfilAnterior), this.oidInstance);
	}

	proxyWebService.addMemberShipGroup(idUsuario, getPerfilFormatoDN(perfil),
			this.oidInstance);
}


/**
 * Elimina el perfil de N@E al usuario especificado.
 * @param idUsuario Login del usuario
 *
 */
public void eliminarPerfilUsuario(String idUsuario)
		throws Exception {

	try {
		if(idUsuario == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("Error en los parametros recibidos. " + e.getMessage() + "\n" +
				"idUsuario=" +	idUsuario);
		throw e;
	}

	String perfilAnterior = this.getPerfilUsuario(idUsuario);

	if(perfilAnterior != null && !perfilAnterior.equals("")) {
		proxyWebService.removeMemberShipGroup(idUsuario,
				getPerfilFormatoDN(perfilAnterior), this.oidInstance);
	}
}


/**
 * Establece el perfil al usuario especificado.
 * @param idUsuario Login del usuario
 * @param perfil Nombre del perfil
 * @param mensaje de Correo para el aviso de cambio de perfil
 */
public void setPerfilUsuario(String idUsuario, String perfil, String mensajeDeAviso)
		throws Exception {

	try {
		if(idUsuario == null || perfil == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("Error en los parametros recibidos. " +
				e.getMessage() + " idUsuario=" +
				idUsuario + " perfil=" + perfil);
		throw e;
	}

	String perfilAnterior = this.getPerfilUsuario(idUsuario);

	if(perfilAnterior != null && !perfilAnterior.equals("")) {
		proxyWebService.removeMemberShipGroup(idUsuario,
		getPerfilFormatoDN(perfilAnterior), this.oidInstance);
	}

	proxyWebService.addMemberShipGroupNotifica(idUsuario, getPerfilFormatoDN(perfil),
												mensajeDeAviso, this.oidInstance);
}



/**
 * Obtiene una lista de las claves de los usuarios por RFC
 * determinado a partir del N@E especificado
 * Filtra que el usuario tenga al menos uno de los perfiles con los
 * prefijos especificados
 * @param numeroNafinElectronico Numero de N@E
 * @param arrPrefijosePerfil Prefijos de perfil.
 *
 * @return Listado que contiene el login de cada uno de los
 * 		usuarios encontrados
 * 		Si no hay usuarios regresa una lista vacia, nunca null.
 */
public List<String> getUsuariosXNE(String numeroNafinElectronico, String[] arrPrefijosPerfil)
		throws Exception {

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (numeroNafinElectronico == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
		Integer.parseInt(numeroNafinElectronico);
	} catch(Exception e) {
                e.printStackTrace();
		log.error("getUsuariosXNE(Error). Error en los parametros recibidos. " +
				e.getMessage() +	" numeroNafinElectronico=" + numeroNafinElectronico);
		throw e;
	}

	//***************************************************************************************

	String sql =
			" SELECT ic_epo_pyme_if, cg_tipo " +
			" FROM comrel_nafin " +
			" WHERE ic_nafin_electronico = ? " ;
	String strSQLPyme =
			" SELECT cg_rfc " +
			" FROM comcat_pyme " +
			" WHERE ic_pyme = ? ";
	String strSQLEpo =
			" SELECT cg_rfc " +
			" FROM comcat_epo " +
			" WHERE ic_epo = ? ";
	String strSQLIf =
			" SELECT cg_rfc " +
			" FROM comcat_if " +
			" WHERE ic_if = ? ";
	AccesoDB con = new AccesoDB();
	String rfcAfiliado = null;
	try	{
		con.conexionDB();
		PreparedStatement ps = con.queryPrecompilado(sql);
		ps.setInt(1, Integer.parseInt(numeroNafinElectronico));
		ResultSet rs = ps.executeQuery();
		int claveAfiliado = 0;
		String tipoAfiliado = "";
		if (rs.next()) {
			claveAfiliado = rs.getInt("ic_epo_pyme_if");
			tipoAfiliado = rs.getString("cg_tipo");
		}
		rs.close();
		ps.close();

		if (tipoAfiliado.equals("P")) {
			ps = con.queryPrecompilado(strSQLPyme);
			ps.setInt(1,claveAfiliado);
		} else if (tipoAfiliado.equals("E")) {
			ps = con.queryPrecompilado(strSQLEpo);
			ps.setInt(1,claveAfiliado);
		} else if (tipoAfiliado.equals("I")) {
			ps = con.queryPrecompilado(strSQLIf);
			ps.setInt(1,claveAfiliado);
		} else {
			throw new Exception("getUsuariosXNE(Error). Tipo de afiliado no implementado: " + claveAfiliado);
		}
		rs = ps.executeQuery();
		if (rs.next()) {
			rfcAfiliado = rs.getString("cg_rfc");
		}
		rs.close();
		ps.close();
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}

	return getUsuariosXRFC(rfcAfiliado, arrPrefijosPerfil);
}


/**
 * Obtiene una lista de las claves de los usuarios por RFC
 * especificado y que tenga al menos uno de los perfiles con los
 * prefijos especificados
 * @param rfcAfiliado RFC del Afiliado
 * @param arrPrefijosePerfil Prefijos de perfil.
 *
 * @return Listado que contiene el login de cada uno de los
 * 		usuarios encontrados
 * 		Si no hay usuarios regresa una lista vacia, nunca null.
 */
public List<String> getUsuariosXRFC(String rfcAfiliado, String[] arrPrefijosPerfil)
		throws Exception {

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (rfcAfiliado == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("getUsuariosXRFC(Error). Error en los parametros recibidos. " +
				e.getMessage() +	" rfcAfiliado=" + rfcAfiliado);
		throw e;
	}

	//***************************************************************************************

	List<String> listaLoginUsuarios = new ArrayList<>();
	String[] loginUsuarios ;

	 loginUsuarios = this.proxyWebService.lookupCommonName(
			"(description=" + rfcAfiliado + ")"
			, this.oidInstance);

	if (loginUsuarios != null) {
		for (int iUsr = 0; iUsr < loginUsuarios.length; iUsr++) {
			String login = loginUsuarios[iUsr];

			boolean agregar = false;
			if (arrPrefijosPerfil!= null && arrPrefijosPerfil.length > 0) {
				for (int i = 0; i < arrPrefijosPerfil.length; i++) {
					if (arrPrefijosPerfil[i] != null && !arrPrefijosPerfil[i].equals("") &&
							this.isUsuarioConPrefijoPerfil(login, arrPrefijosPerfil[i])) {
						agregar = true;
						break;
					}
				}
			} else { //si no hay prefijos siempre se agrega la cuenta encontrada
				agregar = true;
			}

			if(agregar) {
				listaLoginUsuarios.add(login);
			}
		}
	}
	return listaLoginUsuarios;
}

/**
 * Determina si la cuenta especificada tiene un perfil con el
 * prefijo especificado. <br>
 * @param idUsuario Login del usuario
 * @param prefijo Prefijo de perfil
 * @return true Si tiene perfil con el prefijo especificado o false de lo contrario
 *
 */
public boolean isUsuarioConPrefijoPerfil(String idUsuario, String prefijo)
		throws Exception {

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (idUsuario == null || idUsuario.equals("")) {
			throw new Exception("Los parametros no pueden ser nulos ni vacios");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("isUsuarioConPrefijoPerfil(Error). Error en los parametros recibidos. " + e.getMessage() +
				" idUsuario=" + idUsuario);
		throw e;
	}

	//***************************************************************************************

	String dnPerfil = this.proxyWebService.userRoles(idUsuario,
			prefijo, this.oidInstance) ; //el filtro por perfil en el WS no sirve...

	return (this.getPerfilXPrefijo(dnPerfil, prefijo).size()==0)?false:true;
}

/**
 * Determina si la cuenta especificada NO tiene un perfil con el
 * prefijo especificado. <br>
 * @param idUsuario Login del usuario
 * @param prefijo Prefijo de perfil
 * @return true Si NO tiene perfil con el prefijo especificado o false de lo contrario
 *
 */
public boolean isUsuarioSinPrefijoPerfil(String idUsuario, String prefijo)
		throws Exception {

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (idUsuario == null || idUsuario.equals("")) {
			throw new Exception("Los parametros no pueden ser nulos ni vacios");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("isUsuarioConPrefijoPerfil(Error). Error en los parametros recibidos. " + e.getMessage() +
				" idUsuario=" + idUsuario);
		throw e;
	}

	//***************************************************************************************

	String dnPerfil = this.proxyWebService.userRoles(idUsuario,
			prefijo, this.oidInstance) ;

	return (this.getPerfilXPrefijo(dnPerfil, prefijo).size()==0)?true:false;
}



/**
 * Obtiene una lista de las claves de los usuarios que pueden ser reutilizadas
 * para N@E
 * @param rfcAfiliado RFC del Afiliado
 *
 * @return Listado que contiene el login de cada uno de los
 * 		usuarios encontrados
 * 		Si no hay usuarios regresa una lista vacia, nunca null.
 */
public List<String> getCuentasCandidatasAReutilizar(String rfcAfiliado)
		throws Exception {

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (rfcAfiliado == null) {
			throw new Exception("Los parametros no pueden ser nulos");
		}
	} catch(Exception e) {
                e.printStackTrace();
		log.error("getCuentasCandidatasAReutilizar(Error). Error en los parametros recibidos. " +
				e.getMessage() +	" rfcAfiliado=" + rfcAfiliado);
		throw e;
	}

	//***************************************************************************************

	List listaLoginUsuarios = this.getUsuariosXRFC(rfcAfiliado, new String[] {PREFIJO_GRUPO_COMPRAS});
	List<String> listaLoginUsuariosCandidatos = new ArrayList<>();

	Iterator it = listaLoginUsuarios.iterator();

	while (it.hasNext()) {
		String login = (String)it.next();
		if (this.isUsuarioSinPrefijoPerfil(login, PREFIJO_GRUPO_NE)) {
				//Del listado de cuentas obtenido por RFC se eliminan las que son de N@E
				//Por si hubiera una cuenta que tien perfil de compras y de N@E, no se mostrar�a
			listaLoginUsuariosCandidatos.add(login);
		}
	}
	return listaLoginUsuariosCandidatos;
}


	// ********************* Secci�n de miembros privados *******************

	/**
	 * Convierte el perfil de formato N@E a DN
	 * Por ejemplo si recibe como perfil "NAFIN/ADMIN NAFIN" lo convierte en:
	 * cn=NE_ADMIN NAFIN, cn=NE_NAFIN, cn=Groups, dn=netro, dn=com
 	 * Es importante notar que agrega el prefijo del nombre del grupo (NE_)
	 * NE_ es un prefijo especificado por ARGUS
	 *
	 * Este metodo asume que la variable perfil tiene el nombre de un perfil
	 * y no una cadena vacia o nulo
	 */
	private String getPerfilFormatoDN(String perfil) {
		String strDnGrupo = null;
		List listaPerfil = Comunes.explode("/", perfil);
		StringBuffer dnGrupo = new StringBuffer();

		//Se cambia de orden los elementos
		for(int i = listaPerfil.size() - 1; i >= 0; i-- ) {
			dnGrupo.append("cn=" + PREFIJO_GRUPO_NE + listaPerfil.get(i) + ",");
		}
		dnGrupo.append("cn=Groups," + this.idSubscriptor);
		strDnGrupo = dnGrupo.toString();

		return strDnGrupo;
	}



	/**
	 * Convierte el perfil de formato DN a N@E
	 * Por ejemplo si recibe como dnPerfil
	 * cn=NE_ADMIN NAFIN, cn=NE_NAFIN, cn=Groups, dn=netro, dn=com
	 * lo convierte en:
	 * "NAFIN/ADMIN NAFIN"
	 * Es importante notar que elimina el prefijo del nombre del grupo (NE_)
	 *
	 * Este metodo asume que la variable dnPerfil no es una cadena nula
	 */
	private String getPerfilFormatoNE(String dnPerfil) {
		List<String> listaPerfil = new ArrayList<>();
		List elementos = Comunes.explode(",", dnPerfil);

//		log.info("elementos=" + elementos);

		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			String elemento = (String) it.next();
			int indiceNE = elemento.indexOf(PREFIJO_GRUPO_NE);
			if(indiceNE != -1) {
				//por ejemplo de "cn=NE_ADMIN NAFIN" se obtiene "ADMIN NAFIN"
				listaPerfil.add(elemento.substring(indiceNE + 3));
			}
		}

//		log.info("listaPerfil=" + listaPerfil);

		StringBuffer sbPerfil = new StringBuffer();
		for(int i = listaPerfil.size() - 1; i >= 0; i-- ) {
			if(sbPerfil.length() > 0 ) {
				sbPerfil.append("/");
			}
			sbPerfil.append(listaPerfil.get(i));
		}

//		log.info("sbPerfil=" + sbPerfil);
		return sbPerfil.toString();
	}

	/**
	 * El metodo userRoles del webservice recibe un parametro "prefix"
	 * sin embargo no hace nada con ese parametro.
	 * Por lo tanto para filtrar perfiles con un prefijo se crea este m�todo.
	 * Por ejemplo si recibe como dnPerfil
	 * cn=NE_ADMIN NAFIN,cn=Groups, dn=netro, dn=com:cn=NE_NAFIN, cn=Groups, dn=netro, dn=com
	 * genera una lista con los perfiles
	 * Es importante notar que elimina el prefijo del nombre del grupo
	 *
	 */
	private List<String> getPerfilXPrefijo(String dnPerfil, String prefijo) {

		List<String> listaPerfil = new ArrayList<>();
		if (dnPerfil==null || dnPerfil.trim().equals("")) {
			return listaPerfil;
		}
		// ":" es el separador de perfiles
		List elementos = Comunes.explode(":", dnPerfil);

		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			String elemento = (String) it.next();

			if (prefijo!=null && !prefijo.equals("")) {
				int indiceFinPrefijo = elemento.indexOf(prefijo) + prefijo.length();
				if(elemento.indexOf(prefijo) != -1) {
					//por ejemplo de "cn=NE_ADMIN NAFIN,cn=Groups, dn=netro, dn=com" se obtiene "ADMIN NAFIN"
					listaPerfil.add(elemento.substring(indiceFinPrefijo, elemento.indexOf(",") ).trim());
				}
			} else {
				//si el filtro de prefijo viene vacio, no se recorta del nombre del perfil
				listaPerfil.add(elemento.substring(elemento.indexOf("cn=") + 3, elemento.indexOf(",")).trim());
			}
		}
		return listaPerfil;
	}


	/**
	 * Especifica el id del site, con fines de env�o correcto de los correos
	 * del Argus.
	 *
	 */
	private String siteId = ParametrosArgus.siteId;
	//private String siteIdFiado = ParametrosArgus.siteIdFiado;

	/**
	 * Especifica la instancia de OID a la que se conectar�
	 * La equivalencia entre el nombre "netro" y la IP, y puerto del servidor
	 * de OID, est� declarada del lado del WebService (ARGUS)
	 *
	 */
	private String oidInstance = ParametrosArgus.oidInstance;

	/**
	 * Especifica el subscriptor del OID. (Contexto donde se
	 * guardan usuario y grupos)
	 *
	 */
	private String idSubscriptor = ParametrosArgus.idSubscriptor;

	/**
	 * Proxy que nos permite conectarnos al Web Services
	 */
	private OIDWebServiceProxy proxyWebService = new OIDWebServiceProxy(ParametrosArgus.urlOIDWebService);

	/**
	 * Proxy que nos permite conectarnos al Web Services. Cambio de contrase�a y agregar perfil.
	 */
	private ServicesOIDProxy proxyWebService2 = new ServicesOIDProxy(ParametrosArgus.urlServicesOID);

	/**
	 * Contiene el prefijo de grupos definido por ARGUS
	 */
	private final String PREFIJO_GRUPO_NE = "NE_";

	/**
	 * Contiene el prefijo de grupos definido por ARGUS para Sistema de Compras
	 */
	private final String PREFIJO_GRUPO_COMPRAS = "COMPGOB_";


//Fodea 024-2011 (E)
 /**
	 *  metodo que Almacenar�  las cuentas a dar de baja  en las tablas de la base de datos de la Aplicacion Argus
	 * @throws java.lang.Exception
	 * @param solicitud
	 */
public void eliminarUsuario(SolicitudArgus solicitud) throws Exception {

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exito = true;
		String apellidoPaterno = "",  apellidoMaterno = "";
		String nombre = "" , email = "", login ="";
		StringBuffer sQuery	= new StringBuffer();

			// Datos del Usuarios
		  Usuario	usuario = solicitud.getUsuario();
			login  = usuario.getLogin();
			nombre = usuario.getNombre();
			apellidoPaterno = usuario.getApellidoPaterno();
			apellidoMaterno = usuario.getApellidoMaterno();
			email = usuario.getEmail();


		try {
			con.conexionDB();
			String consecutivo ="";

			String strSQL =	" SELECT SEQ_ag_deleteUser.nextval as valor  from DUAL " ;
			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();
			if(rs.next()) {
				consecutivo =  rs.getString("valor"); 
			}
				rs.close();
				ps.close();

		  log.info("consecutivo )"+consecutivo );
			
	   	sQuery	= new StringBuffer();
			sQuery.append( " insert into AG_DELETEUSER " );
			sQuery.append( " ( id_userdel, user_name, first_name,  middle_name,  last_name,  date_of_request ");
			sQuery.append( " , email, status, motivo) ");
			sQuery.append( " values(?,?,?,?,?,Sysdate,?,?,?)");

			log.info("sQuery.toString())"+sQuery.toString() );
			log.info("login)"+login );
			log.info("nombre)"+nombre );
			log.info("apellidoPaterno)"+apellidoPaterno );
			log.info("apellidoMaterno)"+apellidoMaterno );
			log.info("email)"+email );
				
				

			ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, Integer.parseInt(consecutivo));	 //Baja de Usuario
			ps.setString(2, login);
			ps.setString(3, nombre);
			ps.setString(4, apellidoPaterno);
			ps.setString(5, apellidoMaterno);
			ps.setString(6, email);
			ps.setInt(7, 1);	 //Baja de Usuario
			ps.setString(8, "Baja desde Nafin Electr�nico ");

			ps.executeUpdate();
			ps.close();


		} catch(SQLException sqle) {
			exito = false;
                        sqle.printStackTrace();
			log.error("Error  al  insertar  los usuarios en AG_DELETEUSER "+sqle);
			throw sqle;

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}

	}
	//Fodea 024-2011 (S)

}