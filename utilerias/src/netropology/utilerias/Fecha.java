package netropology.utilerias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;

public class Fecha {

	public Fecha() {}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(Fecha.class);

	public static int getAnyo(String fecha) {
		return Integer.parseInt(fecha.substring(6, 10));
	}

	public static int getMes(String fecha) {
		return Integer.parseInt(fecha.substring(3, 5));
	}

	public static String getMesNombre(String fecha) {
		String meses[] = 	{	"ENERO", 		"FEBRERO", 		"MARZO",
								"ABRIL", 		"MAYO", 		"JUNIO",
								"JULIO", 		"AGOSTO", 		"SEPTIEMBRE",
								"OCTUBRE", 		"NOVIEMBRE", 	"DICIEMBRE"	};
		return meses[Integer.parseInt(fecha.substring(3, 5))-1];
	}

	public static int getDia(String fecha) {
		return Integer.parseInt(fecha.substring(0, 2));
	}

	public static String getFormatCalendar(Calendar calendar, String formato) {
		String fecha;
		try{
			fecha = new java.text.SimpleDateFormat(formato).format(calendar.getTime());
		}catch(Exception e){
			e.printStackTrace();
			fecha = null;
		}
		return fecha;
	}

	public static Calendar getCalendar(String fecha) {
		Calendar calendar = Calendar.getInstance();
		if(Comunes.checaFecha(fecha)) {
			calendar.set(
						getAnyo(fecha),
						getMes(fecha)-1,
						getDia(fecha),
						0,
						0,
						0);
			calendar.set(Calendar.MILLISECOND, 0);
		} else {
			calendar = null;
		}
		return calendar;
	}//getCalendar

	public static Date getSysDate() {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		Registros 	registros		= new Registros();
		Date		date			= new Date();
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT SYSDATE FROM DUAL"  ;
			registros = con.consultarDB(qrySentencia, new ArrayList(), true);
			if(registros.next()) {
				date = new java.util.Date(((Timestamp)registros.getObject("SYSDATE")).getTime());
			}
  		} catch (Exception e) {
  			e.printStackTrace();
  			date = null;
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return date;
	}//getSysDate

	public static Calendar getSysCalendar() {
		Calendar calendar = Calendar.getInstance();
		try {
			Date date = getSysDate();
			calendar.setTime(date);
		} catch(Exception e) {
			e.printStackTrace();
			calendar = null;
		}
		return calendar;
	}//getSysCalendar

  	public static String sumaFechaDias(String fecha, int dias) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		Registros	registros		= new Registros();
		String 		fechaDias		= fecha;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT TO_CHAR(TO_DATE (?, 'dd/mm/yyyy') + (?), 'dd/mm/yyyy') fecha"   +
				"   FROM DUAL"  ;
			lVarBind.add(fecha);
			lVarBind.add(new Integer(dias));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				fechaDias = registros.getString("fecha");
			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return fechaDias;
  	}//sumaFechaDias

	public static String sumaFechaPlazo(String fecha, int plazo) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		Registros	registros		= new Registros();
		String 		fechaPlazo		= fecha;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT TO_CHAR(ADD_MONTHS (TO_DATE (?, 'dd/mm/yyyy'), ?), 'dd/mm/yyyy') fecha"   +
				"   FROM DUAL"  ;
			lVarBind.add(fecha);
			lVarBind.add(new Integer(plazo));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				fechaPlazo = registros.getString("fecha");
			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return fechaPlazo;
  	}//sumaFechaPlazo

	public static int obtienePlazo(String fechaIni, String fechaFin) {
		int 	plazo 		= 0;
		String 	fechaAux 	= "";
  		try {
  			fechaIni 	= "01/"+fechaIni.substring(3,10);
  			fechaFin 	= "01/"+fechaFin.substring(3,10);
  			fechaAux 	= fechaIni;
  			while(!fechaAux.equals(fechaFin)) {
  				fechaAux = sumaFechaPlazo(fechaAux, 1);
  				plazo++;
  			}
  		} catch (Exception e) {
  			e.printStackTrace();
  		}
  		return plazo;
  	}//obtienePlazo

  	public static int restaFechas(String fechaIni, String fechaFin) {
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		int 		diasReales		= 0;
  		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT TO_DATE (?, 'dd/mm/yyyy') - TO_DATE (?, 'dd/mm/yyyy')"   +
				"   FROM DUAL"  ;
			lVarBind.add(fechaFin);
			lVarBind.add(fechaIni);
			diasReales = con.existeDB(qrySentencia, lVarBind);
  		} catch (Exception e) {
  			e.printStackTrace();
  		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
  		}
  		return diasReales;
  	}//restaFechas
	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha
	 * con el siguiente formato: dd/mm/yyyy, sustituyendo el numero de mes por su abreviatura
	 * y el delimitador por el indicado en el parametro: <tt>delimiter</tt>
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.fechaConNombreAbbrDelMes("21/01/2000","-")
	 * 		El resultado sera: "21-ENE-2000"
	 * <p>
	 * @return La fecha en el numero formato
	 * @param delimitador	Delimitador a ultilizar en el nuevo formato de la fecha
	 * @param fecha			Fecha cuyo numero de mes sera reemplazado por la abreviatura
	 * 							de su nombre
	 *
	 */
	public static String getFechaConNombreAbbrDelMes(String fecha, char delimitador){
		if (fecha == null) return fecha;
		int numMes = 0;

		String []datos=fecha.split("\\/");
		if(datos.length != 3) return fecha;

		String dia 	= datos[0];
		String mes 	= datos[1];
		String anio = datos[2];

		try {
			numMes = Integer.parseInt(mes);
		} catch(Exception e) {
			return fecha;
		}

		switch(numMes){
			case 1:  mes = "ENE"; break;
			case 2:  mes = "FEB"; break;
			case 3:  mes = "MAR"; break;
			case 4:  mes = "ABR"; break;
			case 5:  mes = "MAY"; break;
			case 6:  mes = "JUN"; break;
			case 7:  mes = "JUL"; break;
			case 8:  mes = "AGO"; break;
			case 9:  mes = "SEP"; break;
			case 10: mes = "OCT"; break;
			case 11: mes = "NOV"; break;
			case 12: mes = "DIC"; break;
			default: return fecha;
		}

		return dia + delimitador + mes + delimitador + anio;
	}

	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha,
	 * cuyo formato se indica en el parametro <tt>formato</tt>, devolviendo una cadena de texto
	 * correspondiente a la fecha del ultimo dia del mes al que pertenece la fecha
	 * proporcionada.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.getUltimoDiaDelMes("05/02/1997","dd/MM/yyyy")
	 * 		El resultado sera: "29/02/1997"
	 * <p>
	 * @return 			La fecha del ultimo dia del mes al que pertenece la fecha proporcionada
	 * @param fecha	Fecha
	 * @param formato	Formato de la fecha con las mismas convenciones que el usado en
	 * 					la clase SimpleDateFormat
	 *
	 */
	public static String getUltimoDiaDelMes(String fecha, String formato){

		SimpleDateFormat 	sdf = null;
		Calendar 			cal = null;

		try {
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);
		}catch (Exception e) {
			return "";
		}
		cal = sdf.getCalendar();

		cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));

		return sdf.format(cal.getTime());
	}

	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha,
	 * cuyo formato se indica en el parametro <tt>formato</tt>, devolviendo una cadena de texto
	 * correspondiente a la fecha del primer d�a h�bil del mes al que pertenece la fecha
	 * proporcionada.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.getPrimerDiaHabilDelMes("15/01/2014","dd/MM/yyyy")
	 * 		El resultado sera: "02/01/2014"
	 * <p>
	 * @return 			La fecha del primer dia h�bil del mes al que pertenece la fecha proporcionada
	 * @param fecha	Fecha
	 * @param formato	Formato de la fecha con las mismas convenciones que el usado en
	 * 					la clase SimpleDateFormat
	 *
	 */
	public static String getPrimerDiaHabilDelMes(String fecha, String formato)
		throws Exception {

		String 				primerDiaHabilDelMes			= null;
		
		// Obtener Primer D�a del Mes
		String 				primerDiaDelMes 				= getPrimerDiaDelMes(fecha, formato);
		
		// Obtener lista de d�as inh�biles del mes
		AccesoDB 			con 								= new AccesoDB();
		PreparedStatement ps 								= null;
		ResultSet 			rs									= null;
		StringBuffer 		diasInhabilesAdicionales	= new StringBuffer();
		try {
			
			con.conexionDB();

			String strSQL = "SELECT cg_dia_inhabil FROM comcat_dia_inhabil where cg_dia_inhabil is not null ";
			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery(strSQL);
			while(rs.next()){
				diasInhabilesAdicionales.append(rs.getString(1) + ";");
			}
			
		} catch(Exception e) {
			
			log.error("getPrimerDiaHabilDelMes(Exception)");
			log.error("getPrimerDiaHabilDelMes.fecha   = <" + fecha   + ">");
			log.error("getPrimerDiaHabilDelMes.formato = <" + formato + ">");
			e.printStackTrace();
			throw new AppException("Se presento un error al obtener la lista de los dias inhabiles: " + e.getMessage()); 
			
		} finally {
			
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}
			
		// En caso de que el primer d�a del mes no sea h�bil, obtener fecha del siguiente d�a h�bil
		if(!esDiaHabil(primerDiaDelMes, formato, diasInhabilesAdicionales.toString())){
			primerDiaHabilDelMes = Fecha.sumaFechaDiasHabiles(primerDiaDelMes,formato,1);
		} else {
			primerDiaHabilDelMes = primerDiaDelMes;
		}
		
		// Enviar primer d�a h�bil del mes
		return primerDiaHabilDelMes;
		
	}
	
	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha,
	 * cuyo formato se indica en el parametro <tt>formato</tt>, devolviendo una cadena de texto
	 * correspondiente a la fecha del primer dia del mes al que pertenece la fecha
	 * proporcionada.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.getPrimerDiaDelMes("05/02/1997","dd/MM/yyyy")
	 * 		El resultado sera: "01/02/1997"
	 * <p>
	 * @return 			La fecha del primer dia del mes al que pertenece la fecha proporcionada
	 * @param fecha	Fecha
	 * @param formato	Formato de la fecha con las mismas convenciones que el usado en
	 * 					la clase SimpleDateFormat
	 *
	 */
	public static String getPrimerDiaDelMes(String fecha, String formato){

		SimpleDateFormat 	sdf = null;
		Calendar 			cal = null;

		try {
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);
		}catch (Exception e) {
			return "";
		}
		cal = sdf.getCalendar();

		cal.set(Calendar.DAY_OF_MONTH,1);

		return sdf.format(cal.getTime());
	}

	/**
	 * Este metodo recibe en el parametro <tt>numeroDelMes</tt> un numero entero entre
	 * el 1 y 12 corresppondiente al numero del mes; devuelvolviendo el nombre del mes
	 * en mayusculas si en la variable <tt>tipo</tt> se indica una 'M', en caso contrario
	 * el nombre del mes sera proporcionado con la primera letra en  mayuscula y las
	 * demas letras en miniscula.
	 * <p>
	 * Ejemplos:<br>
	 *  		Fecha.getNombreDelMes(1,'M')
	 * 		El resultado sera: "ENERO"
	 *			<br>
	 *	 		Fecha.getNombreDelMes(1,'n')
	 *			El resultado sera: "Enero"
	 * <p>
	 * @return	Nombre del mes
	 * @param 	numeroDelMes	Numero entero, perteneciente al numero del mes asociado.
	 * @param 	tipo				Caracter en el que se indica el tipo de formato con el
	 *									que sera devuelto el nombre del mes, con una 'M', el nombre sera
	 *									devuelto en mayusculas, y con un caracter diferente de 'M',
	 *                      	el nombre del mes sera devuelto en mayusculas y minusculas.
	 */
	public static String getNombreDelMes(int numeroDelMes, char tipo){
		String mes = "";

		switch(numeroDelMes){
			case 1:  mes = "ENERO"; 		break;
			case 2:  mes = "FEBRERO"; 		break;
			case 3:  mes = "MARZO"; 		break;
			case 4:  mes = "ABRIL"; 		break;
			case 5:  mes = "MAYO"; 			break;
			case 6:  mes = "JUNIO"; 		break;
			case 7:  mes = "JULIO"; 		break;
			case 8:  mes = "AGOSTO"; 		break;
			case 9:  mes = "SEPTIEMBRE"; 	break;
			case 10: mes = "OCTUBRE"; 		break;
			case 11: mes = "NOVIEMBRE"; 	break;
			case 12: mes = "DICIEMBRE"; 	break;
		}

		if(tipo != 'M'){
			mes=Comunes.ucFirst(mes);
		}

		return mes;
	}

	/**
	 * Este metodo recibe en el parametro <tt>numeroDelMes</tt> un numero entero entre
	 * el 1 y 12 corresppondiente al numero del mes; devuelvolviendo el nombre del mes
	 * con la primera letra en  mayuscula y las demas letras en miniscula.
	 * <p>
	 * Ejemplo:<br>
	 *	 		Fecha.getNombreDelMes(1)
	 *			El resultado sera: "Enero"
	 * <p>
	 * @return	Nombre del mes
	 * @param 	numeroDelMes	Numero entero, perteneciente al numero del mes asociado.
	 *
	 */
	public static String getNombreDelMes(int numeroDelMes){
		return getNombreDelMes(numeroDelMes,'n');
	}

	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha,
	 * cuyo formato se indica en el parametro <tt>formato</tt>, devolviendo un numero entero
	 * correspondiente al numero de dias del mes de la fecha en cuestion.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.getNumeroDeDiasDelMes("22/02/2000","dd/MM/yyyy")
	 * 		El resultado sera: 29
	 * <p>
	 * @return 			Numero entero correspondiente al numero del ultimo dia del mes
	 * @param fecha	Fecha
	 * @param formato	Formato de la fecha con las mismas convenciones que el usado en
	 * 					la clase SimpleDateFormat
	 *
	 */
	 public static int getNumeroDeDiasDelMes(String fecha, String formato){
		SimpleDateFormat 	sdf = null;
		Calendar 			cal = null;

		try {
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);
		}catch (Exception e) {
			return 0;
		}
		cal = sdf.getCalendar();

		cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));

		return cal.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Este metodo suma a la fecha proporcionada en la parametro <tt>fecha</tt>,
	 * el numero de dias que se le indique en el parametro <tt>dias</tt>,
	 * devolviendo una cadena de texto correspondiente a la nueva fecha.
	 * El formato de la fecha se indica en el parametro <tt>formato</tt>.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.sumaFechaDias("22/02/2000","dd/MM/yyyy",45)
	 * 		El resultado sera: 07/04/2000
	 * <p>
	 * @return 			Cadena de texto con la nueva fecha calculada.
	 * @param fecha   Cadena de texto con la fecha a operar.
	 * @param formato Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *						que el usado en la clase SimpleDateFormat.
	 * @param dias    Numero entero en el que se especifica el numero de dias
	 *                a agregar.
	 */
	 public static String sumaFechaDias(String fecha, String formato, int dias){
		SimpleDateFormat 	sdf = null;
		Calendar 			cal = null;

		try {
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);

			cal = sdf.getCalendar();
			cal.add(Calendar.DATE,dias);

		}catch (Exception e) {
			return fecha;
		}

		return sdf.format(cal.getTime());

	}

	/**
	 * Este metodo devuelve <tt>true</tt> si la fecha proporcionada pertenece
	 * a un dia habil. Devuel <tt>false</tt> en caso contrario.
	 * <p>
	 * Ejemplo:<br>
	 *			String diasInhabilesAdicionales 		= CargaDocumentos.getDiasInhabiles(claveEPO); // CargaDocumentosEJB<br>
	 *				// => diasInhabilesAdicionales 	= "01/01;01/05;01/12;04/02;10/05;12/12;14/04;16/09;21/03;25/12;31/12;";<br>
	 *			<br>
	 *  		Fecha.esDiaHabil("14/04/2008","dd/MM/yyyy",diasInhabilesAdicionales);<br>
	 * 		El resultado sera: <tt>false</tt><br>
	 * <p>
	 * @return 			boolean
	 * @param fecha   		Cadena de texto con la fecha a operar.
	 * @param formato 		Formato del parametro <tt>fecha</tt> con las mismas convenciones *													que el usado en la clase SimpleDateFormat.
	 * @param diasInhabilesAdicionales    	Cadena de texto con una lista la lista de fechas adicionales
	 *					que se consideraran inhabiles. El formato de la lista es:
	 *					numeroDeDiaDelMes + "/" + numeroDelMes + ";" asi, por ejemplo: "21/01;"
	 *				        tiene numeroDeDiaDelMes = 21 y numeroDelMes = 01. Los numeros siempre
	 *					deben de ser de dos digitos, si estos fueran de uno solo, rellenar con un
	 *					cero a la izquierda.
	 */
	public static boolean esDiaHabil(String fecha, String formato,String diasInhabilesAdicionales)
		throws Exception{

		//System.out.println("Fecha::esDiaHabil(E)");

		SimpleDateFormat 	sdf 			= null;
		Calendar 			cal 			= null;
		boolean 				resultado	= true;

		try {
			sdf = new SimpleDateFormat(formato,Locale.US);
			sdf.setLenient(false);
			sdf.parse(fecha);

			cal = sdf.getCalendar();

			int dia  			= cal.get(Calendar.DAY_OF_MONTH);
			int mes  			= cal.get(Calendar.MONTH)+1;

			int numeroDeDiaDeLaSemana 	= cal.get(Calendar.DAY_OF_WEEK);

			String strDia 		= (dia < 10 )? "0" + dia:Integer.toString(dia);
			String strMes 		= (mes < 10 )? "0" + mes:Integer.toString(mes);

			if(numeroDeDiaDeLaSemana  == 1 || numeroDeDiaDeLaSemana == 7){
				resultado = false;
			}else if(diasInhabilesAdicionales != null && diasInhabilesAdicionales.indexOf(strDia+"/"+strMes) != -1){
				resultado = false;
			}

		} catch (Exception e) {
			System.out.println("Fecha::esDiaHabil(Exception)");
			System.out.println("  diasInhabilesAdicionales = " + diasInhabilesAdicionales);
			System.out.println("  fecha                    = " + fecha);
			System.out.println("  formato			           = " + formato);
			e.printStackTrace();
			throw e;
		}

		//System.out.println("Fecha::esDiaHabil(S)");

		return resultado;
	}
	/**
	 * Este metodo devuelve la fecha del ultimo dia habil previo al dia proporcionado
	 * en el parametro <tt>fecha</tt>
	 * <p>
	 * Ejemplo:<br>
	 *			String diasInhabilesAdicionales 		= CargaDocumentos.getDiasInhabiles(claveEPO); // CargaDocumentosEJB<br>
	 *				// => diasInhabilesAdicionales 	= "01/01;01/05;01/12;04/02;10/05;12/12;14/04;16/09;21/03;25/12;31/12;";<br>
	 *			<br>
	 *  		Fecha.getDiaHabilAnterior("14/04/2008","dd/MM/yyyy",diasInhabilesAdicionales);
	 * 		El resultado sera: 11/04/2008
	 * <p>
	 * @return 										Cadena de texto con la nueva fecha calculada.
	 * @param fecha   							Cadena de texto con la fecha a operar.
	 * @param formato 							Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *													que el usado en la clase SimpleDateFormat.
	 * @param diasInhabilesAdicionales    	Cadena de texto con una lista la lista de fechas adicionales
	 *													que se consideraran inhabiles. El formato de la lista es:
	 *													numeroDeDiaDelMes + "/" + numeroDelMes + ";" asi, por ejemplo: "21/01;"
	 *													tiene numeroDeDiaDelMes = 21 y numeroDelMes = 01. Los numeros siempre
	 *													deben de ser de dos digitos, si estos fueran de uno solo, rellenar con un
	 *													cero a la izquierda.
	 */
	public static String getDiaHabilAnterior(String fecha, String formato,String diasInhabilesAdicionales)
		throws Exception{

		System.out.println("Fecha::getDiaHabilAnterior(E)");

		boolean	buscaDia 				= true;
		String	dia						= null;
		String 	diaInhabilAnterior 	= fecha;

		try {

			while(buscaDia){
				dia = sumaFechaDias(diaInhabilAnterior, formato, -1);
				if(esDiaHabil(dia,formato,diasInhabilesAdicionales)){
					buscaDia = false;
				}else{
					diaInhabilAnterior = dia;
				}
			}

		}catch (Exception e) {
			System.out.println("Fecha::getDiaHabilAnterior(Exception)");
			System.out.println("  diasInhabilesAdicionales 	= " + diasInhabilesAdicionales);
			System.out.println("  fecha               		= " + fecha);
			System.out.println("  formato			  				= " + formato);
			e.printStackTrace();
			throw e;
		}

		System.out.println("Fecha::getDiaHabilAnterior(S)");

		return dia;
	}

	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha
	 * con el siguiente formato: dd/mm/yyyy, sustituyendo el numero de mes por su nombre
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.fechaConNombreDelMes("21/01/2000")
	 * 		El resultado sera: "21 de enero de 2000"
	 * <p>
	 * @return La fecha en el numero formato
	 * @param fecha			Fecha
	 *
	 */
	public static String getFechaConNombreDelMes(String fecha){
		return getFechaConNombreDelMes(fecha," de ");
	}

	/**
	 * Este metodo recibe en el parametro <tt>fecha</tt> una cadena de texto con una fecha
	 * con el siguiente formato: dd/mm/yyyy, sustituyendo el numero de mes por su nombre
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.fechaConNombreDelMes("21/01/2000","-")
	 * 		El resultado sera: "21-enero-2000"
	 * <p>
	 * @return La fecha en el numero formato
	 * @param fecha		   Fecha
	 * @param delimitador   Delimitador de los campos de la fecha
	 *
	 */
	public static String getFechaConNombreDelMes(String fecha, String delimitador){
		if (fecha == null) return fecha;
		if (delimitador == null) delimitador = "";
		int numMes = 0;

		String []datos=fecha.split("\\/");
		if(datos.length != 3) return fecha;

		String dia 	= datos[0];
		String mes 	= datos[1];
		String anio = datos[2];

		try {
			numMes = Integer.parseInt(mes);
		} catch(Exception e) {
			return fecha;
		}

		switch(numMes){
			case 1:  mes = "enero"; break;
			case 2:  mes = "febrero"; break;
			case 3:  mes = "marzo"; break;
			case 4:  mes = "abril"; break;
			case 5:  mes = "mayo"; break;
			case 6:  mes = "junio"; break;
			case 7:  mes = "julio"; break;
			case 8:  mes = "agosto"; break;
			case 9:  mes = "septiembre"; break;
			case 10: mes = "octubre"; break;
			case 11: mes = "noviembre"; break;
			case 12: mes = "diciembre"; break;
			default: return fecha;
		}

		return dia + delimitador + mes + delimitador + anio;

	}

	/**
	 * Este metodo suma a la fecha proporcionada en el parametro <tt>fecha</tt>,
	 * el numero de dias h�biles que se le indique en el parametro <tt>dias</tt>,
	 * devolviendo una cadena de texto correspondiente a la nueva fecha.
	 * El formato de la fecha se indica en el parametro <tt>formato</tt>.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.sumaFechaDiasHabiles("28/04/2008", "dd/MM/yyyy", 10)
	 * 		El resultado sera: 13/05/2008
	 * <p>
	 * @return 			Cadena de texto con la nueva fecha calculada.
	 * @param fecha   Cadena de texto con la fecha a operar.
	 * @param formato Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *								que el usado en la clase SimpleDateFormat.
	 * @param dias    Numero entero en el que se especifica el numero de dias h�biles
	 *                a agregar.
	 */
	 public static String sumaFechaDiasHabiles(String fecha, String formato, int dias){
		AccesoDB con = new AccesoDB();
		StringBuffer diasInhabilesAdicionales = new StringBuffer();
		ResultSet rst = null;
		SimpleDateFormat sdf = null;
		Calendar cal = null;
		int contador_dias = 0;

		try {
			con.conexionDB();

			String strSQL = "SELECT cg_dia_inhabil FROM comcat_dia_inhabil where cg_dia_inhabil is not null ";
			rst = con.queryDB(strSQL);
			while(rst.next()){diasInhabilesAdicionales.append(rst.getString(1) + ";");}
			rst.close();

			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);

			cal = sdf.getCalendar();

			while(contador_dias < dias){
				cal.add(Calendar.DATE, 1);
				if(esDiaHabil(sdf.format(cal.getTime()), formato, diasInhabilesAdicionales.toString())){
					contador_dias++;
				}
			}
		}catch (Exception e){
			return fecha;
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return sdf.format(cal.getTime());
	}

	/**
	 * Este metodo es igual a sumaFechaDiasHabiles
	 * con la diferencia que los dias inhabiles los determina de la
	 * base de datos del SIAG
	 * @return 			Cadena de texto con la nueva fecha calculada.
	 * @param fecha   Cadena de texto con la fecha a operar.
	 * @param formato Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *								que el usado en la clase SimpleDateFormat.
	 * @param dias    Numero entero en el que se especifica el numero de dias h�biles
	 *                a agregar.
	 */
	 public static String sumaFechaDiasHabilesSIAG(String fecha, String formato, int dias){
		AccesoDB con = new AccesoDB();
		StringBuffer diasInhabilesAdicionales = new StringBuffer();
		ResultSet rst = null;
		SimpleDateFormat sdf = null;
		Calendar cal = null;
		int contador_dias = 0;

		try {
			con.conexionDB();

			String strSQL = "SELECT TO_CHAR(FECHA, 'DD/MM') as dia_inhabil from gia_dias_feriados WHERE FECHA >= TRUNC(SYSDATE)";
			rst = con.queryDB(strSQL);
			while(rst.next()){diasInhabilesAdicionales.append(rst.getString(1) + ";");}
			rst.close();

			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);

			cal = sdf.getCalendar();

			while(contador_dias < dias){
				cal.add(Calendar.DATE, 1);
				if(esDiaHabil(sdf.format(cal.getTime()), formato, diasInhabilesAdicionales.toString())){
					contador_dias++;
				}
			}
		}catch (Exception e){
			throw new AppException("Error al sumar los dias habiles a la fecha especificada" , e);
		}finally{
			if(con.hayConexionAbierta()){
        con.terminaTransaccion(false);
				con.cierraConexionDB();
			}
		}
		return sdf.format(cal.getTime());
	}

	/**
	 * Devuelve la fecha actual haciendo consulta a la base de datos
	 *
	 * @return Cadena de Texto con la Fecha Actual en formato DD/MM/YYYY.
	 * @author jshernandez
	 *
	 */
	public static String getFechaActual()
	 	throws AppException{

			log.info("getFechaActual(E)");
			// Obtener mes actual y a�o actual
			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			List 				lVarBind			= new ArrayList();
			Registros		registros		= null;
			String 			fecha 			= null;
			try {
				con.conexionDB();
				qrySentencia = "SELECT TO_CHAR(TRUNC(SYSDATE),'DD/MM/YYYY') FECHA_ACTUAL FROM DUAL";
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					fecha = registros.getString("FECHA_ACTUAL");
				}
			} catch(Exception e) {
				log.error("getFechaActual(Exception)");
				log.error("getFechaActual.qrySentencia = <" + qrySentencia + ">");
				e.printStackTrace();
				throw new AppException("getFechaActual(Exception): Error al obtener la fecha actual. \n" + e.getMessage());
			} finally {
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
			log.info("getFechaActual(S)");
			return fecha;
	}

  	/**
	 * Devuelve la Hora actual haciendo consulta a la base de datos
	 * @param   formatoHora Formato de la hora, el cual recibira la funcion
   *          TO_CHAR de oracle
	 * @return  Cadena de Texto con la Fecha Actual
	 * @author jshernandez
	 *
	 */
	public static String getHoraActual(String formatoHora)
	 	throws AppException{

			log.info("getHoraActual(E)");
			// Obtener mes actual y a�o actual
			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			List 				lVarBind			= new ArrayList();
			Registros		registros		= null;
			String 			hora 			= null;
			try {
				con.conexionDB();
				qrySentencia = "SELECT TO_CHAR(SYSDATE,'"+formatoHora+"') HORA_ACTUAL FROM DUAL";
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					hora = registros.getString("HORA_ACTUAL");
				}
			} catch(Exception e) {
				log.error("getHoraActual(Exception)");
				log.error("getHoraActual.qrySentencia = <" + qrySentencia + ">");
        log.error("getHoraActual.formatoHora  = <" + formatoHora  + ">");
				e.printStackTrace();
				throw new AppException("getHoraActual(Exception): Error al obtener la hora actual. \n" + e.getMessage());
			} finally {
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
			log.info("getHoraActual(S)");
			return hora;
	}

	/**
	 * Devuelve el mes actual configurado en la base de datos.
	 *
	 * @return Numero entero con el valor del mes actual.
	 * @author jshernandez
	 *
	 */
	public static int getMesActual()
	 	throws AppException{
			// Obtener mes actual y a�o actual
			log.info("getMesActual(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			List 				lVarBind			= new ArrayList();
			Registros		registros		= null;
			String			mesActual 		= "0";

			try {
				con.conexionDB();
				qrySentencia = "SELECT TO_CHAR(TRUNC(SYSDATE),'MM') MES_ACTUAL FROM DUAL";
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					mesActual = registros.getString("MES_ACTUAL");
				}
			} catch(Exception e) {
				log.error("getMesActual(Exception)");
				log.error("getMesActual.qrySentencia = <" + qrySentencia + ">");
				e.printStackTrace();
				throw new AppException("getMesActual(Exception): Error al obtener el mes actual. \n" + e.getMessage());
			} finally {
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}

			log.info("getMesActual(S)");
			return Integer.parseInt(mesActual);
	}

	/**
	 * Devuelve el a�o actual configurado en la base de datos
	 *
	 * @return Numero entero con el valor del a�o actual
	 * @author jshernandez
	 *
	 */
	public static int getAnioActual()
	 	throws AppException{
			// Obtener mes actual y a�o actual
			log.info("getAnioActual(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			List 				lVarBind			= new ArrayList();
			Registros		registros		= null;
			String 			anioActual 		= "0";

			try {
				con.conexionDB();
				qrySentencia = "SELECT TO_CHAR(TRUNC(SYSDATE),'YYYY') ANIO_ACTUAL FROM DUAL";
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					anioActual = registros.getString("ANIO_ACTUAL");
				}
			} catch(Exception e) {
				log.error("getAnioActual(Exception)");
				log.error("getAnioActual.qrySentencia = <" + qrySentencia + ">");
				e.printStackTrace();
				throw new AppException("getAnioActual(Exception): Error al obtener el anio actual. \n" + e.getMessage());
			} finally {
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
			log.info("getAnioActual(S)");

			return Integer.parseInt(anioActual);
	}

   /**
	 *  Este metodo devuelve un proximo dia habil para la EPO indicada.
	 *
	 *	 @param fechaActual La Fecha a partir de la cual se calculara el dia habil proximo
	 *  @param formatoFecha El formato de la fecha actual y como se usa en la funcion TO_DATE de ORACLE
	 *  @param numeroDias Numero de Dias Habiles que se sumaran a la fecha actual
	 *  @param ic_epo Clave de la EPO con respecto a la cual se obtendra el dia habil siguiente
	 *
	 *  @return Cadena de texto con la fecha del proximo dia habil cuyo formato se especifica en el
	 *          parametro <tt>formatoFecha</tt>
	 *
	 *  @author jshernandez
	 */
	public static String getProximoDiaHabilPorEPO(
				String 	fechaDiaActual,
				String 	formatoFecha,
				int 		numeroDias,
				String 	ic_epo
			)throws AppException{

			log.info("getProximoDiaHabilPorEPO(E)");

			AccesoDB 			con 				= new AccesoDB();
			String 				qrySentencia	= null;
			List 					lVarBind			= new ArrayList();
			Registros			registros		= null;
			String 				fechaSiguiente = "";
			PreparedStatement ps					= null;

			try {
				con.conexionDB();

				qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
				con.terminaTransaccion(true);

				qrySentencia =
					"SELECT "  +
					"  TO_CHAR(SIGFECHAHABILXEPO ( "  +
					"		?, "  + // Clave de la EPO
					"		TO_DATE(?,?), "  + // Fecha Actual, Formato de la Fecha
					"		?, "  + // Numero de Dias que se sumaran
					"		'+' "  +
					"  ),?) AS DIA_SIGUIENTE "  + // Formato de la Fecha
					"FROM "  +
					"  DUAL";

				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(fechaDiaActual);
				lVarBind.add(formatoFecha);
				lVarBind.add(new Integer(numeroDias));
				lVarBind.add(formatoFecha);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next() ){
					fechaSiguiente = registros.getString("DIA_SIGUIENTE");
				}

				if(fechaSiguiente == null || fechaSiguiente.trim().equals("")){
					throw new Exception("La Fecha no puede venir vacia");
				}

			} catch(Exception e) {
				log.error("getProximoDiaHabilPorEPO(Exception)");
				log.error("getProximoDiaHabilPorEPO.qrySentencia   = <" + qrySentencia   + ">");
				log.error("getProximoDiaHabilPorEPO.fechaDiaActual = <" + fechaDiaActual + ">");
				log.error("getProximoDiaHabilPorEPO.formatoFecha   = <" + formatoFecha   + ">");
				log.error("getProximoDiaHabilPorEPO.numeroDias     = <" + numeroDias     + ">");
				log.error("getProximoDiaHabilPorEPO.ic_epo         = <" + ic_epo         + ">");
				e.printStackTrace();
				throw new AppException("getProximoDiaHabilPorEPO(Exception): Error al obtener la fecha del proximo dia habil por EPO");
			} finally {
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
			log.info("getProximoDiaHabilPorEPO(S)");

			return fechaSiguiente;
	}

	 /**
	 *  Este metodo devuelve un dia habil anterior para la EPO indicada.
	 *
	 *	 @param fechaActual La Fecha a partir de la cual se calculara el dia habil anterior
	 *  @param formatoFecha El formato de la fecha actual y como se usa en la funcion TO_DATE de ORACLE
	 *  @param numeroDias Numero de Dias Habiles que se restaran a la fecha actual
	 *  @param ic_epo Clave de la EPO con respecto a la cual se obtendra el dia habil anterior
	 *
	 *  @return Cadena de texto con la fecha del dia habil anterior cuyo formato se especifica en el
	 *          parametro <tt>formatoFecha</tt>
	 *
	 *  @author jshernandez
	 */
	public static String getDiaHabilAnteriorPorEPO(
				String 	fechaDiaActual,
				String 	formatoFecha,
				int 		numeroDias,
				String 	ic_epo
			)throws AppException{

			log.info("getDiaHabilAnteriorPorEPO(E)");

			AccesoDB 			con 				= new AccesoDB();
			String 				qrySentencia	= null;
			List 					lVarBind			= new ArrayList();
			Registros			registros		= null;
			String 				fechaSiguiente = "";
			PreparedStatement ps					= null;

			try {
				con.conexionDB();

				qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
				con.terminaTransaccion(true);

				qrySentencia =
					"SELECT "  +
					"  TO_CHAR(SIGFECHAHABILXEPO ( "  +
					"		?, "  + // Clave de la EPO
					"		TO_DATE(?,?), "  + // Fecha Actual, Formato de la Fecha
					"		?, "  + // Numero de Dias que se restaran
					"		'-' "  +
					"  ),?) AS DIA_SIGUIENTE "  + // Formato de la Fecha
					"FROM "  +
					"  DUAL";

				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(fechaDiaActual);
				lVarBind.add(formatoFecha);
				lVarBind.add(new Integer(numeroDias));
				lVarBind.add(formatoFecha);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next() ){
					fechaSiguiente = registros.getString("DIA_SIGUIENTE");
				}

				if(fechaSiguiente == null || fechaSiguiente.trim().equals("")){
					throw new Exception("La Fecha no puede venir vacia");
				}

			} catch(Exception e) {
				log.error("getDiaHabilAnteriorPorEPO(Exception)");
				log.error("getDiaHabilAnteriorPorEPO.qrySentencia   = <" + qrySentencia   + ">");
				log.error("getDiaHabilAnteriorPorEPO.fechaDiaActual = <" + fechaDiaActual + ">");
				log.error("getDiaHabilAnteriorPorEPO.formatoFecha   = <" + formatoFecha   + ">");
				log.error("getDiaHabilAnteriorPorEPO.numeroDias     = <" + numeroDias     + ">");
				log.error("getDiaHabilAnteriorPorEPO.ic_epo         = <" + ic_epo         + ">");
				e.printStackTrace();
				throw new AppException("getDiaHabilAnteriorPorEPO(Exception): Error al obtener la fecha del dia habil anterior por EPO");
			} finally {
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
			log.info("getDiaHabilAnteriorPorEPO(S)");

			return fechaSiguiente;
	}

	/**
	 *  Este metodo devuelve una lista separada por ";" con los dias inhabiles para la EPO.
	 *  En esta lista se consideran los dias inhabiles generales, los cuales estan especificados en la
	 *  la tabla: <tt>COMCAT_DIA_INHABIL</tt> y los dias especificos para la EPO: los cuales se encuentran
	 *  especificados en la tabla: <tt>COMREL_DIA_INHABIL_X_EPO</tt>.
    *
	 *  @param claveEpo Clave de la EPO.
	 *
	 *  @return <tt>String</tt> con la lista de los dias inhabiles por EPO.
	 *
	 *  @author jshernandez
	 */
	public static String getListaDeDiasInhabilesPorEpo(String claveEpo)
		throws AppException {

		log.info("getListaDeDiasInhabilesPorEpo(E)");

		AccesoDB 			con 					= new AccesoDB();
		StringBuffer		query					= new StringBuffer();
		PreparedStatement	ps						= null;
		ResultSet			rs						= null;

		StringBuffer		lista					= new StringBuffer();

		try{
			con.conexionDB();
			query.append(
				"SELECT 								"  +
				"	CG_DIA_INHABIL 				"  +
				"FROM 								"  +
				"	COMCAT_DIA_INHABIL 			"  +
				" WHERE CG_DIA_INHABIL IS NOT NULL "+
				"UNION 								"  +
				"SELECT 								"  +
				"	CG_DIA_INHABIL 				"  +
				"FROM 								"  +
				"	COMREL_DIA_INHABIL_X_EPO 	"  +
				" WHERE cg_dia_inhabil is not null "+
				" AND 								"  +
				"	IC_EPO = ? 						"
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(claveEpo) );
			rs = ps.executeQuery();

			while(rs != null && rs.next()){
				String diaInhabil = rs.getString(1);
				if( diaInhabil != null && !diaInhabil.trim().equals("") ){
					lista.append(diaInhabil+";");
				}
			}

		}catch(Exception e) {
			log.error("getListaDeDiasInhabilesPorEpo(Exception)");
			log.error("getListaDeDiasInhabilesPorEpo.claveEpo = <"+claveEpo+">");
			e.printStackTrace();
			throw new AppException("Se presento un error al obtener la lista de los dias inhabiles por EPO");
		}finally {
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.error("getListaDeDiasInhabilesPorEpo(S)");
		}

		return lista.toString();
	}

	/**
	 * Este metodo devuelve <tt>true</tt> si la fecha proporcionada pertenece
	 * a un dia inhabil para la EPO especificada. Devuelve <tt>false</tt> en caso contrario.
	 * <p>
	 * Ejemplo:<br>
	 *  		Fecha.esDiaInhabilPorEpo("658","02/11/2010","dd/MM/yyyy");<br>
	 * 		El resultado sera: <tt>true</tt><br>
	 * <p>
	 * @param claveEpo    	Clave de la EPO.
	 * @param fecha   		Cadena de texto con la fecha a validar.
	 * @param formato 		Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *								que el usado en la clase SimpleDateFormat.
	 *
	 * @return <tt>boolean</tt>
	 *
	 * @author jshernandez
	 *
	 */
	public static boolean esDiaInhabilPorEpo(String claveEpo, String fecha, String formato)
		throws AppException{
		log.info("esDiaInhabilPorEpo(E)");
		boolean esDiaInHabil = false;
		try {
			String diasInhabilesAdicionales 		= getListaDeDiasInhabilesPorEpo(claveEpo);
			esDiaInHabil = !esDiaHabil(fecha,formato,diasInhabilesAdicionales);
		}catch(Exception e){
			log.error("esDiaInhabilPorEpo(Exception)");
			log.error("esDiaInhabilPorEpo.claveEpo 	= <"+claveEpo+">");
			log.error("esDiaInhabilPorEpo.fecha 	   = <"+fecha+">");
			log.error("esDiaInhabilPorEpo.formato 	   = <"+formato+">");
			e.printStackTrace();
			throw new AppException("Error al revisar si la fecha proporcionada cae en un dia inhabil");
		}
		log.info("esDiaInhabilPorEpo(S)");
		return esDiaInHabil;
	}

	/**
	 * Extrae y formatea la fecha ISO como dd/mm/yyyy
	 * @param fechaISO Fecha/Hora en formato ISO (p.e. 2011-04-25T00:00:00)
	 * @return Cadena con la fecha en formato dd/mm/yyyy
	 */
	public static String getFechaDeISO(String fechaISO) {
		if (fechaISO == null || fechaISO.equals("")) {
			return fechaISO;
		}
		String anio = null;
		String mes = null;
		String dia = null;

		try {
			anio = fechaISO.substring(0,4);
			mes = fechaISO.substring(5,7);
			dia = fechaISO.substring(8,10);
		} catch (Exception e) {
			return null;
		}
		return dia + "/" + mes + "/"+ anio;
	}
	
	/**
	 * Este m�todo suma a la fecha proporcionada en el argumento <tt>fecha</tt>,
	 * el n�mero de d�as h�biles que se le indique en el argumento <tt>dias</tt>,
	 * devolviendo una cadena de texto correspondiente a la nueva fecha. Se excluyen
	 * del c�lculo de d�as h�biles, lo d�as que se indiquen en el argumento: 
	 * <tt>diasInhabilesAdicionales</tt>. El formato de la fecha se indica en 
	 * el argumento <tt>formato</tt>.
	   <p>
	 * <strong>IMPORTANTE:</strong> Debido al estilo actual que se maneja en el cat�logo de d�as inabiles: COMCAT_DIA_INHABIL
	 * La precisi�n de la fecha presentada est� restringida al a�o para el cual se realiza el
	 * c�lculo (diasInhabilesAdicionales tiene un rango de 1 a�o).
	 *
	 * <p>
	 * Ejemplo:
	 * <p>
	 * <blockquote>
	 *			<code>String diasInhabilesAdicionales = "01/01;01/05;02/02;02/04;02/11;03/04;12/12;16/03;16/09;16/11;17/11;25/12;";</code><br>
	 *  		<code>Fecha.calculaDiaHabil("17/04/2015", "dd/MM/yyyy", 4, diasInhabilesAdicionales);</code><br><br>
	 * 		El resultado sera: 23/04/2015
	 * </blockquote>
	 * <p>
	 *
	 * @param fecha   Cadena de texto con la fecha a operar.
	 * @param formato Formato del parametro <tt>fecha</tt> con las mismas convenciones
	 *								que el usado en la clase SimpleDateFormat.
	 * @param dias    Numero entero en el que se especifica el n�mero de dias h�biles
	 *                a agregar/restar.
	 * @param diasInhabilesAdicionales    	Cadena de texto con una lista la lista de fechas adicionales
	 *													que se consideraran inhabiles. El formato de la lista es:
	 *													numeroDeDiaDelMes + "/" + numeroDelMes + ";" asi, por ejemplo: "21/01;"
	 *													tiene numeroDeDiaDelMes = 21 y numeroDelMes = 01. Los numeros siempre
	 *													deben de ser de dos d&iacute;gitos, si estos fueran de un s&oacute;lo
	 *													d&iacute;gito, rellenar con un cero a la izquierda.
	 *
	 * @return 			Cadena de texto con la nueva fecha calculada.
	 * 
	 * @throws AppException
	 * 
	 * @since  F001 - 2015 -- PAGOS - Vencimientos 1er Piso
	 * @author jshernandez
	 * @date   27/04/2015 04:51:27 p.m.
	 *
	 */
	 public static String calculaDiaHabil(String fecha, String formato, int dias, String diasInhabilesAdicionales)
	 	throws AppException {

	 	log.info("calculaDiaHabil(E)");
	 	
		SimpleDateFormat 	sdf 								= null;
		Calendar 			cal 								= null;
		int 					contador_dias 					= 0;
		int 					incremento						= 0;
		int					numeroDias						= 0;
		try {
			
			incremento = dias < 0 ?-1:1;
			numeroDias = Math.abs(dias);
			
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);
			
			cal = sdf.getCalendar();
			
			while(contador_dias < numeroDias){
				cal.add(Calendar.DATE, incremento);
				if(esDiaHabil(sdf.format(cal.getTime()), formato, diasInhabilesAdicionales)){
					contador_dias++;
				}
			}
			
		} catch (Exception e) {
			
			log.error("calculaDiaHabil(Exception)");
			log.error("calculaDiaHabil.fecha                    = <" + fecha                   + ">");
			log.error("calculaDiaHabil.formato                  = <" + formato                 + ">");
			log.error("calculaDiaHabil.dias                     = <" + dias                    + ">"); 
			log.error("calculaDiaHabil.diasInhabilesAdicionales = <" + diasInhabilesAdicionales + ">");
			e.printStackTrace();
			
			throw new AppException("Error al calcular siguiente d�a h�bil: " + e.getMessage(),e);
			
		} finally {
			
			log.info("calculaDiaHabil(S)");
			
		}
		
		return sdf.format(cal.getTime());
		
	}
	
	/**
	 * Este m&eacute;todo recibe en el argumento <tt>fecha</tt> una cadena de texto con una fecha,
	 * cuyo formato se indica en el argumento <tt>formato</tt>, devolviendo una cadena de texto
	 * correspondiente a la fecha del &uacute;ltimo d&iacute;a del mes. El argumento <tt>offsetMeses</tt>
	 * se usa para obtener el mes ( <code>fecha + offsetMeses</code> ) con respecto al cual se 
	 * obtendr&aacute; la fecha del &uacute;ltimo d&iacute;a.
	 *<p>
	 * Ejemplos:
	 *<p>
	 *       Calcular &uacute;ltimo d&iacute;a del mes anterior:
	 *<p>
	 *<blockquote>
	 *  		<code>Fecha.calculaUltimoDiaDelMes("28/04/2015","dd/MM/yyyy",-1);</code><br>
	 * 		El resultado sera: "31/03/2015"
	 *</blockquote>
	 *</p>
	 *       Calcular &uacute;ltimo d&iacute;a del segundo mes pr&oacute;ximo:
	 *<p>
	 *<blockquote>
	 *  		<code>Fecha.calculaUltimoDiaDelMes("28/04/2015","dd/MM/yyyy",2);</code><br>
	 * 		El resultado sera: "30/06/2015"
	 *</blockquote>
	 *<p>
	 *       Calcular &uacute;ltimo d&iacute;a del mes en curso:
	 *<p>
	 *<blockquote>
	 *  		<code>Fecha.calculaUltimoDiaDelMes("28/04/2015","dd/MM/yyyy",0);</code><br>
	 * 		El resultado sera: "30/04/2015"
	 *</blockquote>
	 *<p>
	 *
	 * @param fecha   	 Fecha.
	 * @param formato 	 Formato de la fecha con las mismas convenciones que el usado en
	 * 						 la clase SimpleDateFormat
	 * @param offsetMeses N&uacute;mero entero (positivo &oacute; negativo), con el n&uacute;mero de meses que
	 *                    se a&ntilde;adir&aacute;n a la <tt>fecha</tt>, para obtener el mes a partir
	 *							 del cual se calcular&aacute; la fecha del &uacute;ltimo d&iacute;a.
	 * @return 				 La fecha del &uacute;ltimo d&iacute;a del mes.
	 *
	 * @throws AppException
	 * 
	 * @since  F001 - 2015 -- PAGOS - Vencimientos 1er Piso
	 * @author jshernandez
	 * @date   28/04/2015 11:49:55 a.m.
	 *
	 */
	public static String calculaUltimoDiaDelMes(String fecha, String formato, int offsetMeses)
		throws AppException {

		log.info("calculaUltimoDiaDelMes(E)");
		
		SimpleDateFormat 	sdf 				= null;
		Calendar 			cal 				= null;
		String 				ultimoDiaMes 	= "";
		
		try {
			
			sdf = new SimpleDateFormat(formato);
			sdf.setLenient(false);
			sdf.parse(fecha);
			
			cal = sdf.getCalendar();
			cal.add(Calendar.MONTH,offsetMeses);
			cal.set(Calendar.DATE,cal.getActualMaximum(Calendar.DATE));
		
			ultimoDiaMes = sdf.format(cal.getTime());
			
		} catch (Exception e) {
			
			log.error("calculaUltimoDiaDelMes(Exception)");
			log.error("calculaUltimoDiaDelMes.fecha       = <" + fecha       + ">");
			log.error("calculaUltimoDiaDelMes.formato     = <" + formato     + ">");
			log.error("calculaUltimoDiaDelMes.offsetMeses = <" + offsetMeses + ">");
			e.printStackTrace();
			
			throw new AppException("Error al calcular �ltimo d�a del mes: " + e.getMessage(),e);
			
		} finally {
			
			log.info("calculaUltimoDiaDelMes(S)");
			
		}

		return ultimoDiaMes;
		
	}
        public static Date sumarRestarDiasFecha(Date fecha,int field, int dias){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);
            calendar.add(field, dias);
            return calendar.getTime();
        }
        
        
    public static String sumaFechaDiasHabilesPorAno(String fecha, String formato, int dias){
           AccesoDB con = new AccesoDB();
           StringBuffer diasInhabilesAdicionales = new StringBuffer();
           ResultSet rst = null;
           SimpleDateFormat sdf = null;
           Calendar cal = null;
           int contador_dias = 0;

           try {
                   con.conexionDB();

                   String strSQL = "SELECT to_char(df_dia_inhabil, 'dd/MM')\n" + 
                   "FROM comcat_dia_inhabil where df_dia_inhabil is not null and \n" + 
                   "    (\n" + 
                   "        extract(year from df_dia_inhabil) = extract(year from sysdate) \n" + 
                   "    or extract(year from df_dia_inhabil) = extract(year from (add_months(sysdate, 12)))) order by df_dia_inhabil";
                   rst = con.queryDB(strSQL);
                   while(rst.next()){
                       diasInhabilesAdicionales.append(rst.getString(1) + ";");
                }
                   rst.close();
               
                String strSQL2 = "SELECT cg_dia_inhabil FROM comcat_dia_inhabil where cg_dia_inhabil is not null ";
                rst = con.queryDB(strSQL2);
                while(rst.next()){diasInhabilesAdicionales.append(rst.getString(1) + ";");}
                rst.close();

                   sdf = new SimpleDateFormat(formato);
                   sdf.setLenient(false);
                   sdf.parse(fecha);

                   cal = sdf.getCalendar();
               
                     while(contador_dias < dias){
                        cal.add(Calendar.DATE, 1);
                        if(esDiaHabil(sdf.format(cal.getTime()), formato, diasInhabilesAdicionales.toString())){
                            contador_dias++;
                        }
                    }

                   while(contador_dias < dias){
                           cal.add(Calendar.DATE, 1);
                           if(esDiaHabil(sdf.format(cal.getTime()), formato, diasInhabilesAdicionales.toString())){
                                   contador_dias++;
                           }
                   }
           }catch (Exception e){
                   return fecha;
           }finally{
                   if(con.hayConexionAbierta()){
                           con.cierraConexionDB();
                   }
           }
           return sdf.format(cal.getTime());
    }

	
}//Fecha

