package netropology.utilerias;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CicloBackGroundSP extends Thread {

	private String 	nombreSP	= "";
	private String 	parametros	= "";
	private	List	lVarBind	= new ArrayList();

	public CicloBackGroundSP(String nombreSP, List lVarBind) {
		this.nombreSP	= nombreSP;
		this.lVarBind	= lVarBind;
		for(int i=0;i<lVarBind.size();i++) {
			if(!"".equals(parametros)) {
				this.parametros += ",";
			}
			this.parametros += "?";
		}//for(int i=0;i<lVarBind.size();i++)
	}

	public void run() {
		System.out.println("CicloBackGroundSP::run(E)");
		System.out.println("NombreSP:"+nombreSP);
		System.out.println("ParametrosSP:"+lVarBind);

		AccesoDB 			con 			= new AccesoDB();
		CallableStatement 	cs				= null;
		ResultSet 			rs				= null;
		String				qrySentencia	= "";
		try {
			con.conexionDB();

			qrySentencia =
				nombreSP+"("+this.parametros+")";
			System.out.println("qrySentencia: "+qrySentencia);
			cs = con.ejecutaSP(qrySentencia);

            for(int i=0;i<lVarBind.size();i++) {
            	Object object = lVarBind.get(i);
            	String contenido = object.toString().trim();
				if(object instanceof String) {
					cs.setString(i+1, contenido);
				} else if(object instanceof Integer) {
					cs.setInt(i+1, new Integer(contenido).intValue());
				} else if(object instanceof Long) {
					cs.setLong(i+1, new Long(contenido).longValue());
				} else if(object instanceof Double) {
					cs.setDouble(i+1, new Double(contenido).doubleValue());
				} else if(object instanceof BigDecimal) {
					cs.setBigDecimal(i+1, new BigDecimal(contenido));
				} else if(object instanceof java.util.Date) {
					cs.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Date)object).getTime()), Calendar.getInstance());
				} else if(object instanceof Class) {
					if("int".equals(contenido)) {
						cs.setNull(i+1, Types.INTEGER);
					} else if("long".equals(contenido)) {
						cs.setNull(i+1, Types.BIGINT);
					} else if("double".equals(contenido)) {
						cs.setNull(i+1, Types.DOUBLE);
					}
				}
			}//for(int i=0;i<lVarBind.size();i++)

			cs.execute();
			cs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("CicloBackGroundSP::run(Exception) "+e);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("CicloBackGroundSP::run(Exception) "+ex);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("CicloBackGroundSP::run(S)");
		}//finally
	}//run

}//CicloBackGroundSP
