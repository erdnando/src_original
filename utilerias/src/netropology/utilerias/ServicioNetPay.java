package netropology.utilerias;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Clase que realiza las codificacion Paramatros NetPay
 */
public class ServicioNetPay  {
	public ServicioNetPay() {
		try {
			Properties prop = this.loadParams("servicioNetPay");
			urlNetPay = (String) prop.getProperty("urlNetPay");
      urlResponse = (String) prop.getProperty("urlResponse");
			SID = (String) prop.getProperty("SID");
		} catch(Throwable e) {
			System.out.println("ServicioSMS(Exception)::" +
					"urlNetPay=" + urlNetPay + "\n" +
					"SID=" + SID + "\n" +
					"Error = " + e.getMessage());
			e.printStackTrace();
			throw new RuntimeException("Error al inicializar el servicio de NetPay");
		}
	}
	
	/**
	 * Metodo para enviar un mensaje SMS
	 * @param mensaje Mensaje SMS a enviar
	 * @param telefono Telefono
	 * @return C�digo de salida que regresa la invocacion al WS de SMS.
	 */
	public static String hmacSha1(String value, String keyString) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException{
		
		SecretKeySpec keyHmac = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA1");
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(keyHmac);
		byte[] bytesValue = mac.doFinal(value.getBytes("UTF-8"));
		
		return new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytesValue));
    //retrun "";
	}
  
  public String getUrlCodificadaB64()
  {
    byte[] bytesBase64 = null;
    try{
      //String url = "http://130.0.12.47:8980/nafin/00utils/responsePagoEnLinea.jsp";
      bytesBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(urlResponse.getBytes("UTF-8"));
      System.out.println("url Base64 = "+new String( bytesBase64));
      
    }catch(Exception e){
      System.out.println("Error al codificar el url a Base 64");
    }
    
    return new String( bytesBase64);
  }
  
  public String getNumOrdenTrans()
  {
    Calendar fecha = new GregorianCalendar();
      int a�o = fecha.get(Calendar.YEAR);
      int mes = fecha.get(Calendar.MONTH)+1;
      int dia = fecha.get(Calendar.DAY_OF_MONTH);
      int hora = fecha.get(Calendar.HOUR_OF_DAY);
      int minuto = fecha.get(Calendar.MINUTE);
      int segundo = fecha.get(Calendar.SECOND);
      System.out.println("Fecha Actual: " + dia + "/" + (mes+1) + "/" + a�o);
      System.out.println("Hora Actual: "+hora+":"+minuto+":"+segundo);
      //String SID = "8006";
      fechaTrans = ((dia)<10?("0"+(dia)):(""+dia))+"/"+((mes)<10?("0"+(mes)):(""+mes))+"/"+a�o;
      horaTrans = ((hora)<10?("0"+(hora)):(""+hora))+":"+((minuto)<10?("0"+(minuto)):(""+minuto))+":"+((segundo)<10?("0"+(segundo)):(""+segundo));
      numOrden = SID+a�o+
              ((mes)<10?("0"+(mes)):(""+mes))+
              ((dia)<10?("0"+(dia)):(""+dia))+
              ((hora)<10?("0"+(hora)):(""+hora))+
              ((minuto)<10?("0"+(minuto)):(""+minuto))+
              ((segundo)<10?("0"+(segundo)):(""+segundo))+"1003";

      return numOrden;
  }
  
  public String getCodificaSha1(String montoTotTC, String val2) {
		String resultSha1 = "";
    try{
      
              
  
      String cadenaSha1 = SID+"A"+montoTotTC+"A"+numOrden;
      resultSha1 = hmacSha1(cadenaSha1, "adm0n2");
      //return resultSha1;
    }catch(Exception e){
      System.out.println("Error en la codificacion del Url");
    }
    return resultSha1;
	}


  /**
	 * Este m�todo lee el archivo de propiedades
	 *
	 * @param archivo nombre del archivo (sin el .properties)
	 * @return Properties El objeto de propiedades
	 * @exception IOException si la carga de propiedades falla.
	 */
	private Properties loadParams(String file)
			throws IOException {

		// Carga un ResourceBundle y apartir de este crea un Properties
		Properties		 prop	 = new Properties();
		ResourceBundle bundle = ResourceBundle.getBundle(file);

		// Obtiene las llaves y la coloca en el objeto Properties
		Enumeration enumx = bundle.getKeys();
		String key	= null;
		while (enumx.hasMoreElements()) {
			key = (String) enumx.nextElement();
			prop.put(key, bundle.getObject(key));
		}

		return prop;
	}
	
	private String urlNetPay;
  private String urlResponse;
	private String fechaTrans;
	private String horaTrans;
	private String numOrden;
  private String SID;


  public String getFechaTrans()
  {
    return fechaTrans;
  }


  public String getHoraTrans()
  {
    return horaTrans;
  }


  public String getUrlResponse()
  {
    return urlResponse;
  }


  public String getUrlNetPay()
  {
    return urlNetPay;
  }


  public String getSID()
  {
    return SID;
  }
}