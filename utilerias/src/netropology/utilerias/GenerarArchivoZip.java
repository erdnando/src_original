package netropology.utilerias;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		GenerarArchivoZip.java
*
*	Proposito:	Clase GenerarArchivoZip
*
*	Lenguaje:	Java
*
*	Autor:		Salvador Saldivar Barajas
*
*	Fecha Creacion:	18/09/2006
*
****************************************************************************************************************/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class GenerarArchivoZip{
/**
 * Metodo que generar� el archivo zip
 * @author Salvador Saldivar Barajas
 * @since 18 de septiembre de 2006
 * @param rutaorigen Ruta de donde se copiara el archivo
 * @param rutadestino Ruta donde se crear� el archivo zip
 * @param archivo Nombre del archivo a comprimir
 * @return Cadena con la descripci�n del estatus resultante de la ejecuci�n del proceso
 */
	public void generandoZip(String rutaorigen, String rutadestino, String archivo){
		try{			
			String filename = archivo+".txt";
			rutaorigen = rutaorigen+"/"+archivo+".txt";
			String filezip = rutadestino+"/"+archivo+".zip";
			BufferedInputStream origin = null;			
			FileOutputStream dest = new FileOutputStream(filezip);			
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));			
			byte[] data = new byte[8192];			
			FileInputStream fi = new FileInputStream(rutaorigen);
			origin = new BufferedInputStream(fi, 8192);			
			ZipEntry entry = new ZipEntry(filename);
			out.putNextEntry(entry);			
			int count;
			while((count = origin.read(data, 0, 8192)) != -1){
				out.write(data, 0, count);
			}			
			origin.close();		
			out.close();
		}
		catch( Exception e ){
			e.printStackTrace();
		}
	}
	
}