package netropology.utilerias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import mx.gob.nafin.ws.sms.ServicioSMS;

/**
 * Esta clase permite el env�o de mensajes cortos de texto
 * utilizando un servidor independiente para brindar SMS.
 * @author Alberto Cruz Flores
 * @since FODEA 048-2008 10/10/2008
 */
public class SMS {
	/**
	 * Este m�todo permite enviar el c�digo de verificaci�n al usuario para 
	 * la activaci�n del servicio de factoraje m�vil.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el c�digo de verificaci�n.
	 * @param codigoVerificacion es el codigo que se le envia al usuario en el mensaje.
	 */
	public void enviarCodigoVerificacion(String numeroCelular, String codigoVerificacion) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";		
		
		try{
			con.conexionDB();
	
			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
			
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_CODIGO_VERIFICACION");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			 
			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", codigoVerificacion);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			ServicioSMS sms = new ServicioSMS();
			int numero =   sms.envioMensaje(numeroCelular, mensajeSMS);			
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){			
			System.out.println("SMS(Exception):: Error al enviar el codigo de verificacion" +e);
			e.printStackTrace();
			throw new AppException("SMS(Exception):: Error al enviar el codigo de verificacion"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}		
		
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * que su activaci�n del servicio de factoraje m�vil se realiz� correctamente
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 */
	public void enviarRegistroExitoso(String numeroCelular) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();
			
			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_REGISTRO_PYME");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			ServicioSMS sms = new ServicioSMS();
			int numero =  sms.envioMensaje(numeroCelular, mensajeSMS);					
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}


		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar notificacion de registro");
			e.printStackTrace();
			throw new AppException("SMS(Exception)::Error al enviar notificacion de registro"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el n�mero de documentos que le han publicado.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param numeroDocumentos es el n�mero de documentos que tiene publicados la pyme.
	 */
	public void enviarNumeroDocumentosPublicados(String numeroCelular, String numeroDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_NUM_DOCTOS_PUB");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();

			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", numeroDocumentos);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");

			ServicioSMS sms = new ServicioSMS();
			int numero =  sms.envioMensaje(numeroCelular, mensajeSMS);					
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero); 
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion de documentos publicados");
			e.printStackTrace();
			throw new AppException("SMS(Exception):: Error al enviar la notificacion de documentos publicados"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el monto de los documentos que le han publicado.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param montoDocumentos es el monto total de los documentos que tiene publicados la pyme.
	 */
	public void enviarMontoDocumentosPublicados(String numeroCelular, String montoDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_MONTO_DOCTOS_PUB");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			
			if(montoDocumentos.indexOf(",") != -1){montoDocumentos.replaceAll(",", "\\,");}
			
			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", montoDocumentos);			
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
		
			ServicioSMS sms = new ServicioSMS();
			int numero =  sms.envioMensaje(numeroCelular, mensajeSMS);					
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion del monto de documentos publicados");
			e.printStackTrace();
			throw new AppException("SMS(Exception): Error al enviar la notificacion del monto de documentos publicados"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el n�mero y monto de los documentos que le han publicado.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param numeroDocumentos es el n�mero de documentos que tiene publicados la pyme.
	 * @param montoDocumentos es el monto total de los documentos que tiene publicados la pyme.
	 */
	public void enviarNumeroMontoDoctosPublicados(String numeroCelular, String numeroDocumentos, String montoDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_NUM_MONTO_DOCTOS_PUB");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			
			if(montoDocumentos.indexOf(",") != -1){montoDocumentos.replaceAll(",", "\\,");}
			
			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", numeroDocumentos);
			mensajeSMS = mensajeSMS.replaceAll("\\[2\\]", montoDocumentos);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");

			ServicioSMS sms = new ServicioSMS();
			int  numero =  sms.envioMensaje(numeroCelular, mensajeSMS);			
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion del numero y monto de documentos publicados");
			e.printStackTrace();
			throw new AppException("SMS(Exception):Error al enviar la notificacion del numero y monto de documentos publicado"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el n�mero de documentos que tiene por vencerse.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param numeroDocumentos es el n�mero de documentos que tiene por vencer.
	 */
	public void enviarNumeroDoctosPorVencer(String numeroCelular, String numeroDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_NUM_DOCTOS_VENC");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();

			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", numeroDocumentos);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			ServicioSMS sms = new ServicioSMS();
			int numero =  sms.envioMensaje(numeroCelular, mensajeSMS);						
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion de numero de documentos por vencer");
			e.printStackTrace();
			throw new AppException("SMS(Exception):Error al enviar la notificacion de numero de documentos por vencer"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el monto de los documentos que tiene por vencer.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param montoDocumentos es el monto total de los documentos que tiene por vencer.
	 */
	public void enviarMontoDoctosPorVencer(String numeroCelular, String montoDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_MONTO_DOCTOS_VENC");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			
			if(montoDocumentos.indexOf(",") != -1){montoDocumentos.replaceAll(",", "\\,");}
			
			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", montoDocumentos);			
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			ServicioSMS sms = new ServicioSMS();
			int numero = sms.envioMensaje(numeroCelular, mensajeSMS);						
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion del monto de documentos por vencer");
			e.printStackTrace();
			throw new AppException("SMS(Exception):Error al enviar la notificacion del monto de documentos por vencer"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el n�mero y monto de los documentos que tiene por vencer.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param numeroDocumentos es el n�mero de documentos que tiene por vencer la pyme.
	 * @param montoDocumentos es el monto total de los documentos que tiene por vencer la pyme.
	 */
	public void enviarNumeroMontoDoctosPorVencer(String numeroCelular, String numeroDocumentos, String montoDocumentos) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_NUM_MONTO_DOCTOS_VENC");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();
			
			if(montoDocumentos.indexOf(",") != -1){montoDocumentos.replaceAll(",", "\\,");}
			
			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", numeroDocumentos);
			mensajeSMS = mensajeSMS.replaceAll("\\[2\\]", montoDocumentos);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			ServicioSMS sms = new ServicioSMS();
			int numero =	sms.envioMensaje(numeroCelular, mensajeSMS);
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}


		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion del numero y monto de documentos por vencer");
			e.printStackTrace();
			throw new AppException("SMS(Exception):Error al enviar la notificacion del numero y monto de documentos por vencer"+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Este m�todo obtiene el par�metro COSTO_SMS de la base de datos.
	 * @return costo_sms Es el costo del mensaje SMS.
	 */
	public String getCostoSMS() throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String costo_sms = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM com_parametros_sms WHERE cc_param_sms = ?");

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "COSTO_SMS");

			ResultSet rst = pst.executeQuery();

			while(rst.next()){costo_sms = rst.getString(1)==null?"":rst.getString(1);}

			rst.close();
			pst.close();
		}catch(Exception e){
			System.out.println("..:: ERROR : SMS : getCostoSMS() ::..");
			e.printStackTrace();
			throw new AppException("SMS(Exception):  SMS : getCostoSMS()  "+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return costo_sms;
	}
	
	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * el monto de los documentos que tiene por vencer.
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param montoDocumentos es el monto total de los documentos que tiene por vencer.
	 */
	public void enviarNotificacionOfertaTasas(String numeroCelular, String nombreEPO) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_AVISO_DOCTOS_OFERTA");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){mensajeSMS = rst.getString(1)==null?"":rst.getString(1);}
			
			rst.close();
			pst.close();

			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", nombreEPO);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");

			ServicioSMS sms = new ServicioSMS();
			int  numero =  sms.envioMensaje(numeroCelular, mensajeSMS);						
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero); 
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion de oferta de tasas");
			e.printStackTrace();
			throw new AppException("SMS(Exception):  Error al enviar la notificacion de oferta de tasas  "+e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Este m�todo permite enviar al usuario un mensaje para notificarle 
	 * ??????
	 * @param numeroCelular es el n�mero de celular del usuario al que se le enviar� el mensaje de notificaci�n.
	 * @param numeroDispersiones es el n�mero de dispersiones pendientes de enviar al flujo de fondos
	 */
	public void enviarDispersionesPendientes(String numeroCelular, String numeroDispersiones,String fechaVencimiento) throws Exception{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		String mensajeSMS = "";
		
		try{
			con.conexionDB();

			strSQL.append("SELECT cg_valor FROM comcat_frases_sms WHERE cc_frase_sms = ?");
						
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setString(1, "SMS_NUM_EPOS_DISPERSION");
			
			ResultSet rst = pst.executeQuery();
			
			while(rst.next()){
				mensajeSMS = rst.getString(1)==null?"":rst.getString(1);
			}
			
			rst.close();
			pst.close();

			mensajeSMS = mensajeSMS.replaceAll("\\[1\\]", numeroDispersiones);
			mensajeSMS = mensajeSMS.replaceAll("\\[2\\]", fechaVencimiento);
			//mensajeSMS = URLEncoder.encode(mensajeSMS, "UTF-8");
			
			  
			ServicioSMS sms = new ServicioSMS();
			int numero =  sms.envioMensaje(numeroCelular, mensajeSMS);						
			if(numero!=3)  {
				throw new Exception("Error al enviar SMS "+numero);
			}

		}catch(Exception e){
			System.out.println("SMS(Exception):: Error al enviar la notificacion de oferta de tasas");
			e.printStackTrace();
			throw new AppException("Error al enviar la notificacion de oferta de tasas", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		} 
	}
}