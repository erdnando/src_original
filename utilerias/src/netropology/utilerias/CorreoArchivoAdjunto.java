package netropology.utilerias;

import java.sql.ResultSet;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class CorreoArchivoAdjunto {
	
    public static void AttachFile(String from, String to, String subject,String msg, String file, String ruta){
    	AccesoDB con = new AccesoDB();
		String host = "";
    	try{
    		con.conexionDB();
			String strSQL = 
					" SELECT cg_ip_smtp " +
					" FROM com_param_gral " +
					" WHERE ic_param_gral = 1 ";
			ResultSet rs = con.queryDB(strSQL);
			if (rs.next()) {
				host = rs.getString("cg_ip_smtp");
			}
			rs.close();
			con.cierraStatement();
			
    		String filename = ruta+file;
		    // Get system properties
		    Properties props = System.getProperties();
		    // Setup mail server
		    props.put("mail.smtp.host", host);
		    // Get session
		    Session session = Session.getInstance(props, null);
		    // Define message
		    Message message = new MimeMessage(session);
		    message.setFrom(new InternetAddress(from));
		    message.setRecipients(Message.RecipientType.TO, 		    
					InternetAddress.parse(to, false));
					
		    message.setSubject(subject);
		    // Create the message part 
		    BodyPart messageBodyPart = new MimeBodyPart();
		    // Fill the message
		    messageBodyPart.setText(msg);
		    // Create a Multipart
		    Multipart multipart = new MimeMultipart();
		    // Add part one
		    multipart.addBodyPart(messageBodyPart);
		    //
		    // Part two is attachment
		    //
		    // Create second body part
		    messageBodyPart = new MimeBodyPart();
		    // Get the attachment
		    DataSource source = new FileDataSource(filename);
		    // Set the data handler to the attachment
		    messageBodyPart.setDataHandler(new DataHandler(source));
		    // Set the filename
		    messageBodyPart.setFileName(file);
		    // Add part two
		    multipart.addBodyPart(messageBodyPart);
		    // Put parts in message
		    message.setContent(multipart);
		    // Send the message
		    Transport.send(message);
    	}catch (Exception ex) {
			System.out.println("Error. Correo::enviarMensajeTexto()");
			ex.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}		    
	}    
}