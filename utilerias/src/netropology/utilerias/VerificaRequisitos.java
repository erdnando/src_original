package netropology.utilerias;

import java.sql.ResultSet;

public class VerificaRequisitos {	

	public VerificaRequisitos() { }

	public boolean validaRequisitos(int iNoEPO, int ic_pyme){
		int i=0;
		String qrySentencia = "";
		String aceptacion = "";
		boolean ok_requisito=true;
		ResultSet rs;
		AccesoDB con = new AccesoDB();
		
		try{
			con.conexionDB();
			qrySentencia =  " SELECT cs_habilitado FROM comrel_pyme_epo_x_producto "+
				" WHERE ic_epo = "+iNoEPO+" AND ic_pyme = "+ic_pyme+" and ic_producto_nafin = 2";
				rs = con.queryDB(qrySentencia);
			if(rs.next())
				aceptacion = rs.getString(1).trim();
			rs.close();
			con.cierraStatement();
			if("".equals(aceptacion) && !"H".equals(aceptacion))
				ok_requisito = false;
			
			qrySentencia="select pep.cs_aceptacion_pyme from comrel_pyme_epo_x_producto pep, comcat_epo e "+
				"where pep.ic_epo = e.ic_epo and pep.ic_pyme = "+ic_pyme+" and pep.ic_epo="+iNoEPO+
				" and pep.ic_producto_nafin = 2 ";
				
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				aceptacion = rs.getString(1).trim();
			rs.close();
			con.cierraStatement();
			if("".equals(aceptacion) && !"S".equals(aceptacion))
				ok_requisito = false;
			
			qrySentencia=" Select count(e.cg_razon_social) from comcat_epo e,com_linea_credito lc,comcat_if i "+
				"where lc.ic_epo = e.ic_epo and lc.ic_if = i.ic_if and lc.df_vencimiento >= SYSDATE "+
				"and lc.ic_pyme = "+ic_pyme+" and lc.cg_tipo_solicitud in ('I','A') and lc.ic_epo in "+
				"(select ic_epo from comrel_pyme_epo where ic_pyme = "+ic_pyme+") and lc.ic_producto_nafin = 2 ";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				aceptacion = rs.getString(1).trim();
			rs.close();
			con.cierraStatement();
			if("".equals(aceptacion) && "0".equals(aceptacion))
				ok_requisito = false;
					
		}
        catch (Exception e) {
			String err = e.toString();
			System.out.println("<br>Error:" + err);
			ok_requisito = false;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
   	    return ok_requisito; 
	}

	public boolean validaRequisitos(int iNoEPO, int ic_pyme, String producto){
		int i=0;
		String qrySentencia = "";
		String aceptacion = "";
		boolean ok_requisito=true;
		ResultSet rs;
		AccesoDB con = new AccesoDB();
		
		try{
			con.conexionDB();
			qrySentencia =  " SELECT cs_habilitado FROM comrel_pyme_epo_x_producto "+
				" WHERE ic_epo = "+iNoEPO+" AND ic_pyme = "+ic_pyme+" and ic_producto_nafin = " + producto;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				aceptacion = rs.getString(1).trim();
			rs.close();
			con.cierraStatement();
			if("H".equals(aceptacion)) {
				qrySentencia="select pep.cs_aceptacion_pyme from comrel_pyme_epo_x_producto pep, comcat_epo e "+
					"where pep.ic_epo = e.ic_epo and pep.ic_pyme = "+ic_pyme+" and pep.ic_epo="+iNoEPO+
					" and pep.ic_producto_nafin = "+producto;
					
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					aceptacion = rs.getString(1).trim();
				rs.close();
				con.cierraStatement();

				if("S".equals(aceptacion)) {
					qrySentencia=" Select count(e.cg_razon_social) from comcat_epo e,com_linea_credito lc,comcat_if i "+
						"where lc.ic_epo = e.ic_epo and lc.ic_if = i.ic_if and lc.df_vencimiento_adicional >= SYSDATE "+
						"and lc.ic_pyme = "+ic_pyme+" and lc.cg_tipo_solicitud in ('I','R') and lc.ic_epo in "+
						"(select ic_epo from comrel_pyme_epo where ic_pyme = "+ic_pyme+") and lc.ic_producto_nafin =  "+producto;
					rs = con.queryDB(qrySentencia);
					if(rs.next())
						aceptacion = rs.getString(1).trim();
					rs.close();
					con.cierraStatement();
					if("0".equals(aceptacion))
						ok_requisito = false;
				} else {
					ok_requisito = false;
				}
			} else {
				ok_requisito = false;
			}

		} catch (Exception e) {
			String err = e.toString();
			System.out.println("<br>Error:" + err);
			ok_requisito = false;
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
   	    return ok_requisito; 
	}
	
}
