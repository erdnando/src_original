package netropology.utilerias;

import java.util.ArrayList;
import java.util.List;

public class Cursores{

	/**
	*	Muestra cursores por maquina
	*	@param maquina Nombre de la maquina cuyos cursores se mostraran
	*/
	public static void muestra(String maquina){
		AccesoDB 	con 			= new AccesoDB();
		List 			lvarbind 	= new ArrayList();
		Registros 	registros 	= null;
		
		// Realizar consulta de cursores
		String query =
			"select sum(a.value) total_cur, avg(a.value) avg_cur, max(a.value) max_cur, " + 
			"s.username, s.machine " +
			"from v$sesstat a, v$statname b, v$session s " + 
			"where a.statistic# = b.statistic#  and s.sid=a.sid and s.machine = ? " +
			"and b.name = ? " + 
			"group by s.username, s.machine " +
			"order by 1 desc";
		lvarbind.add(maquina);
		lvarbind.add("opened cursors current");
		
		try {
			con.conexionDB();
			registros = con.consultarDB(query,lvarbind,false);	
		}catch (Exception e){
			System.out.println("Ocurrio una excepcion en Cursores::porMaquina() ");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		
		// Imprimir estadisticas de los cursores
		boolean haEntradoAlCiclo = false;
		while(registros != null && registros.next()){
			if(!haEntradoAlCiclo){
				System.out.println("TOTAL_CUR\tAVG_CUR\tMAX_CUR\tUSERNAME\t\tMACHINE");
				System.out.println("-----------------------------------------------------------------------------");
				haEntradoAlCiclo = true;
			}
			System.out.print(registros.getString("TOTAL_CUR")+"\t");
			System.out.print(registros.getString("AVG_CUR")  +"\t");
			System.out.print(registros.getString("MAX_CUR")	 +"\t");
			System.out.print(registros.getString("USERNAME") +"\t\t");
			System.out.print(registros.getString("MACHINE")  +"\n");
		}
			
	}
	
	/**
	 *   Muestra todos los cursores abiertos
	 */
	public static void muestraTodos(){
		AccesoDB 	con 			= new AccesoDB();
		List 			lvarbind 	= new ArrayList();
		Registros 	registros 	= null;
		
		// Realizar consulta de cursores
		String query =
			"select sum(a.value) total_cur, avg(a.value) avg_cur, max(a.value) max_cur, " + 
			"s.username, s.machine " +
			"from v$sesstat a, v$statname b, v$session s " + 
			"where a.statistic# = b.statistic#  and s.sid=a.sid  " +
			"and b.name = ? " + 
			"group by s.username, s.machine " +
			"order by 1 desc";	
		lvarbind.add("opened cursors current");
		try {
			con.conexionDB();
			registros = con.consultarDB(query,lvarbind,false);	
		}catch (Exception e){
			System.out.println("Ocurrio una excepcion en Cursores::muestraTodos() ");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		
		// Imprimir estadisticas de los cursores
		boolean haEntradoAlCiclo = false;
		while(registros != null && registros.next()){
			if(!haEntradoAlCiclo){
				System.out.println("TOTAL_CUR\tAVG_CUR\tMAX_CUR\tUSERNAME\t\tMACHINE");
				System.out.println("-----------------------------------------------------------------------------");
				haEntradoAlCiclo = true;
			}
			System.out.print(registros.getString("TOTAL_CUR")+"\t");
			System.out.print(registros.getString("AVG_CUR")  +"\t");
			System.out.print(registros.getString("MAX_CUR")	 +"\t");
			System.out.print(registros.getString("USERNAME") +"\t\t");
			System.out.print(registros.getString("MACHINE")  +"\n");
		}
	}
}

		
	