package netropology.utilerias;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CicloThreadCMD extends Thread {

	int NumProceso1 ;
	int ses_ic_epo1 ;
	int NumCampos1;
	String totdoc = "";
	String mtodoc = "";
	String mtomindoc = "";
	String mtomaxdoc = "";
	String totdocdol = "";
	String mtodocdol = "";
	String mtomindocdol = "";
	String mtomaxdocdol = "";
	String sesIdiomaUsuario;
	AccesoDB con1;

	public CicloThreadCMD(int NumProceso, int ses_ic_epo, int NumCampos,
			String txttotdoc, String txtmtodoc, String txtmtomindoc,
			String txtmtomaxdoc, String txttotdocdol, String txtmtodocdol,
			String txtmtomindocdol, String txtmtomaxdocdol,
			String sesIdiomaUsuario, AccesoDB con) {

		this.NumProceso1		= NumProceso ;
		this.ses_ic_epo1		= ses_ic_epo;
		this.NumCampos1		= NumCampos;
		this.totdoc			= "".equals(txttotdoc)?"0":txttotdoc;
		this.mtodoc			= "".equals(txtmtodoc)?"0":txtmtodoc;
		this.mtomindoc		= "".equals(txtmtomindoc)?"0":txtmtomindoc;
		this.mtomaxdoc		= "".equals(txtmtomaxdoc)?"0":txtmtomaxdoc;
		this.totdocdol		= "".equals(txttotdocdol)?"0":txttotdocdol;
		this.mtodocdol		= "".equals(txtmtodocdol)?"0":txtmtodocdol;
		this.mtomindocdol	= "".equals(txtmtomindocdol)?"0":txtmtomindocdol;
		this.mtomaxdocdol	= "".equals(txtmtomaxdocdol)?"0":txtmtomaxdocdol;
		this.sesIdiomaUsuario = sesIdiomaUsuario;
		this.con1 = con;
	}

	public void run() {

		CallableStatement cs=null;
		ResultSet rsStoreProc;

		System.out.println("Thread:SP_CARGA_MASIVADET("+
				NumProceso1+", "+
				ses_ic_epo1+", "+
				NumCampos1+", "+
				totdoc+", "+
				mtodoc+", "+
				mtomindoc+", "+
				mtomaxdoc+", "+
				totdocdol+", "+
				mtodocdol+", "+
				mtomindocdol+", "+
				mtomaxdocdol+", "+
				sesIdiomaUsuario+ ")" );


		try {
			cs = con1.ejecutaSP("SP_CARGA_MASIVADET(?,?,?,?,?,?,?,?,?,?,?,?)");
			cs.setInt(1,NumProceso1);
			cs.setInt(2,ses_ic_epo1);
			cs.setInt(3,NumCampos1);
			cs.setInt(4,Integer.parseInt(totdoc));
			cs.setDouble(5,Double.parseDouble(mtodoc));
			cs.setDouble(6,Double.parseDouble(mtomindoc));
			cs.setDouble(7,Double.parseDouble(mtomaxdoc));
			cs.setInt(8,Integer.parseInt(totdocdol));
			cs.setDouble(9,Double.parseDouble(mtodocdol));
			cs.setDouble(10,Double.parseDouble(mtomindocdol));
			cs.setDouble(11,Double.parseDouble(mtomaxdocdol));
			cs.setString(12,sesIdiomaUsuario);
			cs.execute();
			if (cs !=null) cs.close();
		} catch (SQLException e) {
			System.out.println("Error en SP_CARGA_MASIVADET()\n" + e.getMessage() );
			//throw e;
		} finally {
			if (con1.hayConexionAbierta()) {
				con1.cierraConexionDB();
			}
		}
	}
}
