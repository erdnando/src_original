package netropology.utilerias;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.naming.NamingException;

import org.apache.commons.logging.Log;

/**
 * Clase para manejar el acceso a la Base de Datos
 *
 */
public class AccesoDB {
	
        private static Log log = ServiceLocator.getInstance().getLog(AccesoDB.class);
        
	//Define que el datasource requerido es remoto. (Para el caso de que esta clase sea invocada en procesos externos al servidor.
	public static final int USAR_DATASOURCE_REMOTO = 1;
	//Define que el datasource requerido es local.
	public static final int USAR_DATASOURCE_LOCAL = 2;
	public static final String	DEFAULT_JNDI_DS = "seguridadData";	//JNDI del DS predeterminado es seguridadData
	/**
	 * Deshabilita el envio de mensajes a la consola para los siguientes metodos:
	 * conexionDB(), consultarDB(), cierraConexionDB() y terminaTransaccion()
	 */
	public void enableMessages(){
		this.showMessages = true;
	}
	
	/**
	 * Habilita el envio de mensajes a la consola para los siguientes metodos:
	 * conexionDB(), consultarDB(), cierraConexionDB() y terminaTransaccion()
	 */
	public void disableMessages(){
		this.showMessages = false;
	}
	
	/**
	 * Constructor de la clase que inicializa el datasource predeterminado (local).
	 */
	public AccesoDB() {
		this(AccesoDB.USAR_DATASOURCE_LOCAL);
	}

	/**
	 * Constructor de la clase que inicializa el datasource predeterminado, ya sea remoto o local.
	 * En caso de requerirse un datasource de origen remoto es necesario especificarlo en 
	 * este constructor usando AccesoDB.USAR_DATASOURCE_REMOTO.
	 * Especificar AccesoDB.USAR_DATASOURCE_LOCAL equivale a invocar AccesoDB()
	 * @param tipoDatasourceAUsar Usar AccesoDB.USAR_DATASOURCE_REMOTO si es acceso remoto, 
	 * 		AccesoDB.USAR_DATASOURCE_LOCAL si es local
	 */
	public AccesoDB(int tipoDatasourceAUsar) {
		try {
			if (tipoDatasourceAUsar == AccesoDB.USAR_DATASOURCE_REMOTO) {
				ds = ServiceLocator.getInstance().getRemoteDS(AccesoDB.DEFAULT_JNDI_DS);
			} else if (tipoDatasourceAUsar == AccesoDB.USAR_DATASOURCE_LOCAL){
				ds = ServiceLocator.getInstance().getDS(AccesoDB.DEFAULT_JNDI_DS); 
			} else {
				throw new AppException("El tipo de datasource a emplear no es valido");
			}
		} catch(Exception e) {
			throw new AppException("Error al inicializar la conexion de datos", e);
		}
	}


	/**
	 * El m�todo establece una conexi�n a la base de datos a trav�s de un
	 * datasource. La conexion s�lo se establece si no hay una actualmente
	 * abierta.
	 */
	public void conexionDB() throws SQLException, NamingException {
		//if(this.showMessages) log.info("AccesoDB::conexionDB(E)");
		//La siguiente condici�n es para evitar que se abra 
		//una conexion estando una ya abierta
		//lo cual genera problema de "Leaked Connections".
		if (m_conn != null && !m_conn.isClosed()) {
			if(this.showMessages) log.error("AccesoDB::conexionDB()::ERROR. Ya hay una conexion Activa");
		} else {
			m_conn = ds.getConnection();
			m_conn.setAutoCommit(false);
			m_conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	
			PreparedStatement ps = queryPrecompilado("ALTER SESSION SET NLS_TERRITORY = AMERICA");
			try{
				ps.executeUpdate();	
			}finally{
				ps.close();
			}
			m_conn.commit();
			//if(this.showMessages) log.info("AccesoDB::conexionDB(Establece SESSION NLS_TERRITORY = AMERICA)");
		}
		//if(this.showMessages) log.info("AccesoDB::conexionDB(S)");
	}

	public PreparedStatement queryPrecompilado(String qrySentencia, List lVarBind, int numMaxRegistros) throws SQLException {
		PreparedStatement ps = null;
		try {
			ps = m_conn.prepareStatement(qrySentencia);
			for(int i=0;i<lVarBind.size();i++) {
				Object object = lVarBind.get(i);
				String contenido = object.toString().trim();
				if(object instanceof String) {
					ps.setString(i+1, contenido);
				} else if(object instanceof Integer) {
					ps.setInt(i+1, new Integer(contenido).intValue());
				} else if(object instanceof Long) {
					ps.setLong(i+1, new Long(contenido).longValue());
				} else if(object instanceof Double) {
					ps.setDouble(i+1, new Double(contenido).doubleValue());
				} else if(object instanceof BigDecimal) {
					ps.setBigDecimal(i+1, new BigDecimal(contenido));
				} else if(object instanceof java.util.Calendar) {
					ps.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Calendar)object).getTime().getTime()), Calendar.getInstance());
				} else if(object instanceof java.util.Date) {
					ps.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Date)object).getTime()), Calendar.getInstance());
				} else if(object instanceof Fecha) {
					ps.setNull(i+1, Types.TIMESTAMP);
				} else if(object instanceof Class) {
					if("int".equals(contenido)) {
						ps.setNull(i+1, Types.INTEGER);
					} else if("long".equals(contenido)) {
						ps.setNull(i+1, Types.BIGINT);
					} else if("double".equals(contenido)) {
						ps.setNull(i+1, Types.DOUBLE);
					}					
				}
			}//for(int i=0;i<lVarBind.size();i++)
			if(numMaxRegistros>0) {
				ps.setMaxRows(numMaxRegistros);
			}
		} catch (SQLException sql) {
			if(ps!=null)
				ps.close();
			log.error("AccesoDB::queryPrecompilado(SQLException) "+sql);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+qrySentencia);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			sql.printStackTrace();
			throw sql;
		} catch (Exception e) {
			if(ps!=null)
				ps.close();
			log.error("AccesoDB::queryPrecompilado(Exception) "+e);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+qrySentencia);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
		}
		return ps;
	}

	public PreparedStatement queryPrecompilado(String query, List lVarBind) throws SQLException {
		return queryPrecompilado(query, lVarBind, 0);
	}

	public PreparedStatement queryPrecompilado(String query) throws SQLException {
		return queryPrecompilado(query, new ArrayList());
	}

	//////////////////////////////////////////////////
	// Ejecuta cualquier SQL dinamicamente
	//////////////////////////////////////////////////
	public CallableStatement ejecutaSP(String nombre_sp) throws SQLException {
		CallableStatement cstm=null;
		try {
			cstm = m_conn.prepareCall("{call " + nombre_sp + "}");
		} catch (SQLException sqle) {
			error.append("AccesoDB.ejecutaSP(SQLException): " + sqle.getMessage() + " Error en el Store Procedure: " + nombre_sp);
			log.error(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
		return cstm;
	}
	
	public CallableStatement procesoPrecompilado(String proceso, List lVarBind) throws SQLException {
		CallableStatement cs = null;
		try {
			cs = this.ejecutaSP(proceso);
			for(int i=0;i<lVarBind.size();i++) {
				Object object = lVarBind.get(i);
				String contenido = object.toString().trim();
				if(object instanceof String) {
					cs.setString(i+1, contenido);
				} else if(object instanceof Integer) {
					cs.setInt(i+1, new Integer(contenido).intValue());
				} else if(object instanceof Long) {
					cs.setLong(i+1, new Long(contenido).longValue());
				} else if(object instanceof Double) {
					cs.setDouble(i+1, new Double(contenido).doubleValue());
				} else if(object instanceof BigDecimal) {
					cs.setBigDecimal(i+1, new BigDecimal(contenido));
				} else if(object instanceof java.util.Calendar) {
					cs.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Calendar)object).getTime().getTime()), Calendar.getInstance());
				} else if(object instanceof java.util.Date) {
					cs.setTimestamp(i+1, new java.sql.Timestamp(((java.util.Date)object).getTime()), Calendar.getInstance());
				} else if(object instanceof Fecha) {
					cs.setNull(i+1, Types.TIMESTAMP);
				} else if(object instanceof Class) {
					if("int".equals(contenido)) {
						cs.setNull(i+1, Types.INTEGER);
					} else if("long".equals(contenido)) {
						cs.setNull(i+1, Types.BIGINT);
					} else if("double".equals(contenido)) {
						cs.setNull(i+1, Types.DOUBLE);
					}					
				}
			}//for(int i=0;i<lVarBind.size();i++)
		} catch (SQLException sql) {
			if(cs!=null)
				cs.close();
			log.error("AccesoDB::procesoPrecompilado(SQLException) "+sql);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+proceso);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			sql.printStackTrace();
			throw sql;
		} catch (Exception e) {
			if(cs!=null)
				cs.close();
			log.error("AccesoDB::procesoPrecompilado(Exception) "+e);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+proceso);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
		}
		return cs;
	}

	//////////////////////////////////////////////////
	// Da commit o rollback a una transacci�n
	//////////////////////////////////////////////////
	public void terminaTransaccion(boolean todo_ok) {
		try {
			if (todo_ok) {
				m_conn.commit();
				if(this.showMessages) log.info("terminaTransaccion(). Commit");
			}
			else {
				m_conn.rollback();
				if(this.showMessages) log.info("terminaTransaccion(). Rollback");
			}
		}
		catch (SQLException sqle) {
			error.append("AccesoDB.terminaTransaccion(SQLException): " + sqle.getMessage() );
			log.error(mostrarError());
			sqle.printStackTrace();
		}
	}

	/* Regresa la Instancia de la Conexion */
	public Connection getConnection() {
		return this.m_conn;
	}

	/* Guarda Cualquier Mensaje de Error de Base de Datos. */
	public String mostrarError() {
		String msg_error = this.error.toString();
		this.error.delete(0,this.error.length());
		return msg_error;
	}

	//////////////////////////////////////////////////
	// Cierra la conexion a la base de datos
	//////////////////////////////////////////////////
	public void cierraConexionDB() {
		try {
			if (stm != null) {
				//Llamar close en un statement previamente cerrado no genera ningun error,
				//por lo cual se llama el close() por si no hubiera sido cerrada anteriormente.
				stm.close();
			}
			if (m_conn != null)	{
				m_conn.close();
				if(this.showMessages) log.info("AccesoDB::cierraConexion()");
			} else {
				if(this.showMessages) log.info("cierraConexion(). No existe ninguna conexion.");
			}
		} catch (SQLException sqle) {
			error.append("AccesoDB.cierraConexionDB(SQLException): " + sqle.getMessage() );
			log.error(mostrarError());
			sqle.printStackTrace();
	 	}
		m_conn = null;
	}

	public void cierraStatement() throws SQLException {
		try {
			if (stm != null) {
				stm.close();
			}
		}
		catch(SQLException sqle) {
			error.append("AccesoDB.cierraStatement(SQLException): " + sqle.getMessage() );
			log.error(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}

// El m�todo indica si hay una conexion abierta o no.
	public boolean hayConexionAbierta() {
		if (m_conn != null) {
			return true;
		} else {
			return false;
		}
	}


	public void conectarDB() throws SQLException, Exception{
		//log.info("\tAccesoDB::ConectarDB");

/*		Class.forName("weblogic.jdbc20.t3.Driver").newInstance();
		m_cConexion =
			DriverManager.getConnection(CConst.CADENA_CONEXION_DB, null);

		m_cConexion.setAutoCommit(false);
		m_cConexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
*/
		javax.naming.Context naming = new javax.naming.InitialContext();
	 	javax.sql.DataSource ds = (javax.sql.DataSource) naming.lookup("seguridadData");
		m_conn = ds.getConnection();


		m_conn.setAutoCommit(false);
		m_conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

	}
	
	
	/**
	 * Realiza una consulta a la base de datos.
	 * @param qrySentencia Cadena con la consulta SQL
	 * @param lVarBind Valores de las Variables bind
	 * @param bGetDataType Indicador de los tipo de datos de los resultados 
	 * 		de la busqueda.
	 *       true Manejarlos de acuerdo a su tipo de dato en base de datos
	 *       false Manejados como cadenas
	 * @param numMaxRegistros N�mero m�ximo de registros a traer.
	 *  		0 significa que no hay restricciones.
	 * 
	 * @return Objeto Registros que contiene la informaci�n 
	 *  		resultante de la consulta
	 * @throws java.lang.Exception
	 */
	public Registros consultarDB(String qrySentencia, List lVarBind, 
                boolean bGetDataType, int numMaxRegistros) throws Exception{
            
		if(this.showMessages) log.info("AccesoDB::consultarDB(E)");
		
                PreparedStatement ps = null;
		ResultSet rs = null;

		Registros registros = new Registros();
		
		if (lVarBind == null) {
			lVarBind = new ArrayList();
		}
		try {
			ps = queryPrecompilado(qrySentencia, lVarBind, numMaxRegistros);
			rs = ps.executeQuery();
			ps.clearParameters();
			registros = this.getRegistrosFromRS(rs, bGetDataType);
			if(rs!=null) {
				rs.close();
			}
			if(ps!=null) {
				ps.close();
			}
		} catch(Exception e) { 
			if(rs!=null)
				rs.close();
			if(ps!=null)
				ps.close();	

			log.error("AccesoDB::consultarDB(Exception) "+e);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+qrySentencia);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
			throw e;
		} finally { 
			if(this.showMessages) log.info("AccesoDB::consultarDB(S)");
		}
		return registros;
	}//consultarDB
	
	/**
	 * Convierte un objeto ResultSet a un Objeto Registro, SOLO usar si el
	 * numero de registros regresado en el ResultSet es peque�o.
	 * Por ejemplo en una paginaci�n.
	 * Es responsabilidad del codigo que manda a llamar este metodo, el de
	 * cerrar el resultset. Solo en caso de que se presente una excepcion
	 * se cierra el resultset
	 * @param rs ResultSet
	 * @param bGetDataType Determina si los registros seran manejados 
	 * 	de acuerdo al tipo de dato en BD (bGetDataType = true) 
	 * 	o como String (bGetDataType = false)
	 * @return Objeto con los registros obtenidos del ResultSet
	 */
	public Registros getRegistrosFromRS(ResultSet rs, boolean bGetDataType){
		ResultSetMetaData	rsMD		= null;	
		List				nombres		= new ArrayList();
		List				renglones	= new ArrayList();
		List				columnas	= null;
		Registros		registros	= new Registros();
	
		try {
			rsMD = rs.getMetaData();
			int numCols = rsMD.getColumnCount();
			int cont = 0;
			while(rs.next()) {
				columnas = new ArrayList();
				for(int i=1;i<=numCols;i++) {
					if(cont==0) {
						nombres.add(rsMD.getColumnName(i));
					}//if(cont==0)
					if(bGetDataType) {
						int colType = rsMD.getColumnType(i);
						switch(colType) {
							case Types.NUMERIC:
							case Types.DECIMAL:
							case Types.INTEGER:
								BigDecimal rs_numeric = rs.getBigDecimal(i)==null?new BigDecimal("0"):rs.getBigDecimal(i);
								columnas.add(rs_numeric);
								break;
							case Types.DATE:
							case Types.TIME:
							case Types.TIMESTAMP:
								Timestamp rs_fecha = rs.getTimestamp(i);
								columnas.add(rs_fecha);
								break;
							default:
								String rs_campo = rs.getString(i)==null?"":rs.getString(i);
								columnas.add(rs_campo);
								break;
						}//switch(colType)
					} else {
						String rs_campo = rs.getString(i)==null?"":rs.getString(i);
						columnas.add(rs_campo);
					}
				}//for(i=1;i<=numCols;i++)
				renglones.add(columnas);
				cont++;
			}//while(rs.next())
			registros.setNombreColumnas(nombres);
			registros.setData(renglones);
			return registros;
		} catch (Exception e) {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch(Exception t) {
				log.error("AccesoDB.getRegistrosFromRS():: Error al cerrar ResultSet: " + t.getMessage());
			}
			throw new AppException("Error al convertir el ResultSet en objeto Registros", e);
		}
	}



	/**
	 * Ejecuta una consulta SQL y los resultados de esta son almacenados
	 * en un objeto de tipo Registros. En eset caso es necesario especificar
	 * una lista con los valores de las variables bind y un identificador que 
	 * determina si los valores del registro se guardan como String o como
	 * el tipo de dato que correponda.
	 * 
	 * @param qrySentencia Instrucci�n SQL
 	 * @param lVarBind Valores de las variables bind
 	 * @param bGetDataType true si se desea manejar los registros con el
	 * 	tipo de dato que corresponda o false para manejarlo como cadena
	 * @throws java.lang.Exception
	 * @return Resultados de la consulta en un objeto de tipo Registros
	 */

	public Registros consultarDB(String qrySentencia, List lVarBind, 
			boolean bGetDataType) throws Exception{
		return consultarDB(qrySentencia, lVarBind, bGetDataType, 0);
	}//consultaDB
	
	/**
	 * Ejecuta una consulta SQL y los resultados de esta son almacenados
	 * en un objeto de tipo Registros. En eset caso es necesario especificar
	 * una lista con los valores de las variables bind.
	 * Los valores son guardados como String
	 * 
	 * @param qrySentencia Instrucci�n SQL
	 * @throws java.lang.Exception
	 * @return Resultados de la consulta en un objeto de tipo Registros
	 */
	public Registros consultarDB(String qrySentencia, 
			List lVarBind) throws Exception{
		return consultarDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	/**
	 * Ejecuta una consulta SQL y los resultados de esta son almacenados
	 * en un objeto de tipo Registros. 
	 * Los valores del registro son guardados como String
	 * 
	 * @param qrySentencia Instrucci�n SQL
	 * @throws java.lang.Exception
	 * @return Resultados de la consulta en un objeto de tipo Registros
	 */
	public Registros consultarDB(String qrySentencia) throws Exception{
		return consultarDB(qrySentencia, new ArrayList());
	}//consultaDB
	
	public List consultaDB(String qrySentencia, List lVarBind, boolean bGetDataType) throws Exception{
		Registros 	registros = consultarDB(qrySentencia, lVarBind, bGetDataType);
		List		renglones = (registros.getNumeroRegistros()==0)?new ArrayList():registros.getData();
		return renglones;
	}//consultaDB
	
	public List consultaDB(String qrySentencia, List lVarBind) throws Exception{
		return consultaDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	public List consultaDB(String qrySentencia) throws Exception{
		return consultaDB(qrySentencia, new ArrayList());
	}//consultaDB

	public List consultaRegDB(String qrySentencia, List lVarBind, boolean bGetDataType) throws Exception{
		List	renglones = consultaDB(qrySentencia, lVarBind, bGetDataType);
		if(renglones.size()>0) {
			renglones = (ArrayList)renglones.get(0);
		}
		return renglones;
	}//consultaDB
	
	public List consultaRegDB(String qrySentencia, List lVarBind) throws Exception{
		return consultaRegDB(qrySentencia, lVarBind, false);
	}//consultaDB
	
	public List consultaRegDB(String qrySentencia) throws Exception{
		return consultaRegDB(qrySentencia, new ArrayList());
	}//consultaDB
	
	
	/**
	 * Regresa el valor de ejecutar una sentencia SQL con un count()
	 */
	public int existeDB(String qrySentencia, List lVarBind) throws Exception{
		int existe = 0;
		List renglones = consultaRegDB(qrySentencia, lVarBind, true);
		if(renglones.size()>0) {
			BigDecimal big = (BigDecimal)renglones.get(0);
			existe = big.intValue();
		}
		return existe;
	}//existeDB
	
	public int ejecutaUpdateDB(String qrySentencia, List lVarBind) throws Exception {
		log.info("AccesoDB::ejecutaUpdateDB(E)");
		PreparedStatement 	ps				= null;
		int					registros		= 0;
		try {
			ps = queryPrecompilado(qrySentencia, lVarBind);
			registros = ps.executeUpdate();
			ps.close();
		} catch(Exception e) { 
			if(ps!=null)
				ps.close();
			log.error("AccesoDB::ejecutaUpdateDB(Exception) "+e);
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			log.error("qrySentencia:"+qrySentencia);
			log.error("lVarBind:"+lVarBind);			
			log.error("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
			e.printStackTrace();
			throw e;
		} finally { 
			log.info("AccesoDB::ejecutaUpdateDB(S)");
		}
		return registros;
	}//ejecutaQuery
	
	public int ejecutaUpdateDB(String qrySentencia) throws Exception{
		return ejecutaUpdateDB(qrySentencia, new ArrayList());
	}//ejecutaQuery

	/*METODO PARA EJECUTAR UN QUERY LLENADO LOS DATO EN UN VECTOR RJV 30052002*/
	public void ejecutarQueryDB(String sql, int inumcols, Vector svTabla) throws Exception, NullPointerException, SQLException {
		int liNumCols = 0;
		int	liTipoDato = 0;
		Time ltmTmp = null;
		java.sql.Date ldtTmp = null;
		Vector lvRegistro = null;
		ResultSetMetaData rsMetaData = null;
		ResultSet lrsResultSet = null;

		String lsCampo = "";
		Integer loiCampo ;
		int liCampo = 0;

		Statement statement = m_conn.createStatement();

		lrsResultSet = statement.executeQuery(sql);

		rsMetaData = lrsResultSet.getMetaData();

		liNumCols = rsMetaData.getColumnCount() ;

		//log.info("\t Numero de Columnas(" + rsMetaData.getColumnCount() + ")");

		while (lrsResultSet.next()) {
			lvRegistro = new Vector();
			for (int i = 1; i <= liNumCols ; i++) {
				liTipoDato = rsMetaData.getColumnType(i);

				//log.info("\t Tipo de Dato (" + liTipoDato + ")");

				if (liTipoDato == CConst.TIPO_VARCHAR || liTipoDato == CConst.TIPO_CHAR){
					lsCampo = lrsResultSet.getString(i);
					if (lsCampo == null)
						lsCampo = "";
					lvRegistro.addElement(lsCampo.trim());
			}else if (liTipoDato == CConst.TIPO_FECHA){
					ldtTmp = lrsResultSet.getDate(i);
					ltmTmp = lrsResultSet.getTime(i);
					if (ldtTmp == null)
						lsCampo = "";
					else
						lsCampo = ldtTmp.toString() + " " + ltmTmp.toString();

					lvRegistro.addElement(lsCampo.trim());
				}else if(liTipoDato == CConst.TIPO_LONG || liTipoDato == CConst.TIPO_SMALLINT || liTipoDato == CConst.TIPO_NUMBER || liTipoDato == CConst.TIPO_RARO || liTipoDato == CConst.TIPO_RARO_2){
 					liCampo = lrsResultSet.getInt(i);
					loiCampo = new Integer(liCampo);
					lvRegistro.addElement(loiCampo);
				}

				if(liTipoDato == CConst.TIPO_LONG || liTipoDato == CConst.TIPO_NUMBER)
					log.info(" " + i + "[" + liCampo + "]");
				else
					log.info(" " + i + "(" + lsCampo + ")");
			}
			svTabla.addElement(lvRegistro);
			log.info("\n ");
		}

		statement.close();
		lrsResultSet.close();

	}

	//throws SQLException
	public void cerrarDB(boolean ebResultado) {
		//log.info("AccesoDB::cerrarDB(E)");
		if (m_conn != null)	{
			try	{
				if (ebResultado)	m_conn.commit();
				else				m_conn.rollback();
			}catch(SQLException sqle)	{
				error.append("AccesoDB.cierraStatement(SQLException): " + sqle.getMessage() );
				log.error(mostrarError());
				sqle.printStackTrace();
			}
			try	{
				m_conn.close();
			}catch(SQLException sqle)	{
				error.append("AccesoDB.cierraStatement(SQLException): " + sqle.getMessage() );
				log.error(mostrarError());
				sqle.printStackTrace();
			}
		}
		//log.info("Acceso::cerrarDB(S)");
	}
	
	
	public ResultSet queryDB(String query) throws SQLException {
		try {
			log.info("AccesoDB.queryDB(E)");
			stm = m_conn.createStatement();
			ResultSet rs = stm.executeQuery(query);
			log.info("AccesoDB::queryDB(S)");
			return rs;
		} catch (SQLException sqle) {
			error.append("AccesoDB.queryDB(SQLException): " + sqle.getMessage()+" Query:*****" + query + "*****");
			log.error(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}
	
	public ResultSet queryDB(String query, int maxSize) throws SQLException {
		try {
			log.info("AccesoDB.queryDB(E)");
			stm = m_conn.createStatement();
			stm.setMaxRows(maxSize);
			ResultSet rs = stm.executeQuery(query);
			log.info("AccesoDB::queryDB(S)");
			return rs;
		} catch (SQLException sqle) {
			error.append("AccesoDB.queryDB(SQLException): " + sqle.getMessage() + " Query:*****" + query + "*****");
			log.error(mostrarError());
			sqle.printStackTrace();
			throw sqle;
		}
	}
	
	//////////////////////////////////////////////////
	// Ejecuta cualquier SQL dinamicamente
	//////////////////////////////////////////////////
	public void ejecutaSQL(String sql) throws SQLException {
		Statement stm = null;
		try {
			stm = m_conn.createStatement();

			stm.executeUpdate(sql);
			stm.close();
			stm = null;
		 }
		 catch (SQLException sqle) {
			error.append("AccesoDB.queryDB(ejecutaSQL): " + sqle.getMessage()+" Query:*****"+sql+"*****");
			log.error(mostrarError());
			if (stm != null) {
				stm.close();
			}
			sqle.printStackTrace();
			throw sqle;
		 }
	}
	
	/**
		 * M�todo de utilidad para cerrar un statement.
		 * Si existe algun error, este solo se desplegara en consola.
		 * Este m�todo es util para llamar en el finally.
		 * @param statement Statement a cerrar
		 */
		public static void cerrarStatement(Statement statement) {
			if (statement == null) {
				return;
			} else {
				try {
					statement.close();
				} catch(Throwable t) {
					log.error("Error al cerrar el statement", t);
				}
			}
		}

		/**
		 * M�todo de utilidad para cerrar un ResultSet.
		 * Si existe algun error, este solo se desplegara en consola.
		 * Este m�todo es util para llamar en el finally.
		 * @param rs ResultSet a cerrar
		 */
		public static void cerrarResultSet(ResultSet rs) {
			if (rs == null) {
				return;
			} else {
				try {
					rs.close();
				} catch(Throwable t) {
					log.error("Error al cerrar el ResultSet", t);
				}
			}
		}

	
	private Connection	m_conn;
	private Statement 			stm;
	private StringBuilder	error = new StringBuilder();
	private javax.sql.DataSource ds = null;
	private int tipoDS = AccesoDB.USAR_DATASOURCE_LOCAL;	//Predeterminado el DS es local
	private boolean showMessages = true;


}
