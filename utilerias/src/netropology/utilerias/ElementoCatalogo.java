package netropology.utilerias;

import java.io.Serializable;

/**
 * Esta clase permite almacenar los valores de la clave y descripci�n
 * de un cat�logo. Est� pensada para ayudar a generar las opciones de
 * una lista despleglable (drop down list)
 * @author Gilberto Aparicio
 *
 */
public class ElementoCatalogo implements Serializable
{
	private String clave;
	private String descripcion;


    public ElementoCatalogo(String clave, String descripcion){
        this.clave = clave;
        this.descripcion = descripcion;
    }
    
    
	public ElementoCatalogo()
	{
	}

	/**
	 * Obtiene la clave
	 * @return Valor de la clave del catalogo
	 */
	public String getClave()
	{
		return clave;
	}

	/**
	 * Establece la clave del elemento
	 * @param clave Establece el valor de la clave del catalogo
	 */
	public void setClave(String clave)
	{
		this.clave = clave;
	}

	/**
	 * Obtiene la descripci�n del elemento
	 * @return Valor de la descripci�n del elemento de cat�logo
	 */
	public String getDescripcion()
	{
		return descripcion;
	}

	/**
	 * Establece la descripcion del elemento
	 * @param descripcion Establece el valor de la descripci�n del elemento de cat�logo
	 */
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	public String toString() {
		return  this.clave + "-" + this.descripcion;
	}
}