package netropology.utilerias;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.URL;

import org.apache.commons.logging.Log;

public class ValidaServicio {
    
    private static Log log = ServiceLocator.getInstance().getLog(ValidaServicio.class);    
    private String serviceCheckURL;
    
    public ValidaServicio() {
        super();
    }
    
    public boolean isWebServiceAvailable(String serviceCheckURL) throws Exception {
        this.serviceCheckURL = serviceCheckURL;
        return isWebServiceAvailable();
    }
    
    public boolean isWebServiceAvailable() throws Exception {
        boolean result = false;
        
        URL webServiceCheckURL = null;
        BufferedReader serviceResponse = null;
        String responseLine = null;
        
        log.debug("Validando conexi�n del Web Service...");
        
        if (this.serviceCheckURL == null || "".equals(this.serviceCheckURL)) {
            throw new Exception("La URL del Web Service no esta definida.");
        } else {
            try {
                log.debug("URL conexi�n: "+serviceCheckURL);
                webServiceCheckURL = new URL(serviceCheckURL);
                serviceResponse = new BufferedReader(new InputStreamReader(webServiceCheckURL.openStream()));
                while ((responseLine = serviceResponse.readLine()) != null) {
                    log.trace(responseLine);
                    break;
                }
                result = true;
                log.debug("Web Service conexi�n OK!");
            } catch(Exception e) {
                log.error("ValidaServicio::isWebServiceAvailable(Exception) "+e);
                //e.printStackTrace();
                throw new Exception("Web Service no disponible. "+e);
            }
        }
        
        responseLine = null;
        webServiceCheckURL = null;
        serviceResponse = null;
        
        return result;
    }
    
    public void setServiceCheckURL(String serviceCheckURL) {
        this.serviceCheckURL = serviceCheckURL;
    }

    public String getServiceCheckURL() {
        return serviceCheckURL;
    }
    
}//ValidaServicio