package com.netro.xlsx;

import com.netro.zip.ComunesZIP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import netropology.utilerias.CreaArchivo;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;

public class ComunesXLSX {
 	
	public static final String    XML_ENCODING = "UTF-8";
	
	private Writer 					xml;
   private int 						indiceRenglon;
   private boolean 					datosHojaInicializados;
   private HashMap 					estilos;
   private String 					directorioTemporal;
   private String[]					longitudesColumna;
   private StringBuffer				columnasCombinadas;  
	private ArrayList             listaHojas = new ArrayList();
   											/* 
   												En esta version, columnasCombinadas no esta pensada para manejar una cantidad 
   												gigantezca de registros, por lo que si as� se decidiera, habria que guardar
   												los datos de esta en un archivo temporal y luego copiar la informacion a la
   												hoja principal.
   											*/
   private int							cuentaColumnasCombinadas;
   private CreaArchivo				archivo;
   private String 					hoja;
   private int     					cuentaHojas;

	public ComunesXLSX(){}

	public ComunesXLSX( String directorioTemporal, HashMap estilos ){
		
		this.estilos						= estilos;	
		this.directorioTemporal			= directorioTemporal;
		 
		this.archivo 						= new CreaArchivo();
		this.cuentaHojas					= 0;
 
		this.datosHojaInicializados 	= false;
		this.columnasCombinadas			= new StringBuffer(256);
		this.cuentaColumnasCombinadas	= 0;
		 
	}
 
	public void setLongitudesColumna(String[] longitudesColumna){
		this.longitudesColumna = longitudesColumna;
	}
	
	public void flush()
		throws IOException {
		xml.flush();	
	}
	
	public Writer creaHoja() 
		throws Exception {
		
		cuentaHojas++;
		if( cuentaHojas > 10 ){
			throw new Exception("La versi�n actual no tiene soporte para agregar m�s de una hoja.");
		}

		datosHojaInicializados = false;
		cuentaColumnasCombinadas = 0;
		this.columnasCombinadas.setLength(0);

		hoja = "sheet_" + archivo.nombreArchivo()+".xml";
		xml  = new OutputStreamWriter(new FileOutputStream(directorioTemporal+hoja), XML_ENCODING );
		listaHojas.add(hoja);
      xml.write(
      	"<?xml version=\"1.0\" encoding=\""+XML_ENCODING+"\"?>" +
         "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">" 
      );
      
      return xml;
      
   }

   private void escribeLongitudesColumnas()
		throws IOException {
   	
   	if( longitudesColumna == null || longitudesColumna.length == 0 ) return;
   	
   	xml.write("<cols>");
   	for(int i=0,columna=1;i<longitudesColumna.length;i++,columna++){
   		xml.write("<col min=\"");xml.write(String.valueOf(columna));xml.write("\" max=\"");xml.write(String.valueOf(columna));xml.write("\" width=\"");
   		xml.write(longitudesColumna[i]);
   		xml.write("\" bestFit=\"1\" customWidth=\"1\"/>");
   	}
		xml.write("</cols>");
		
   }
 
   public void finalizaHoja() 
   	throws IOException {
   	if(!datosHojaInicializados){ 
   		escribeLongitudesColumnas();
   		xml.write("<sheetData>\n"); 
   		datosHojaInicializados = true;
   	}
      xml.write("</sheetData>");
      finalizaCombinacionColumnas();
      xml.write("</worksheet>");
		xml.flush();
		xml.close();
   }

   public String finalizaDocumento(String plantilla) 
   	throws Exception {
   	
   	List 		lista				= new ArrayList();
		for(int x=0; x<listaHojas.size();x++){
			HashMap 	updatedFile 	= new HashMap();
			updatedFile.put("NAME", "xl/worksheets/sheet"+String.valueOf(x+1)+".xml");
			updatedFile.put("PATH", directorioTemporal+listaHojas.get(x));
			lista.add(updatedFile);
		}
		String archivoXLSX = archivo.nombreArchivo()+".xlsx";
		ComunesZIP.updateFiles(plantilla, lista, directorioTemporal + archivoXLSX );
		
		return archivoXLSX;
		
   }
	
	  public String finalizaDocumento(String plantilla, String nombreArchivo )   	throws Exception {
   	
   	List 		lista				= new ArrayList();
		
		HashMap 	updatedFile 	= new HashMap();
		updatedFile.put("NAME", "xl/worksheets/sheet1.xml");
		updatedFile.put("PATH", directorioTemporal+hoja);
		lista.add(updatedFile);
		
		String archivoXLSX = nombreArchivo+".xlsx"; 
		ComunesZIP.updateFiles(plantilla, lista, directorioTemporal + archivoXLSX );
		
		return archivoXLSX;
		
   }
	
   
   public void agregaRenglon(int rownum) 
   	throws IOException {
   	if(!datosHojaInicializados){
   		escribeLongitudesColumnas();
   		xml.write("<sheetData>\n"); 
   		datosHojaInicializados = true;
   	}
      xml.write("<row r=\""+(rownum+1)+"\">\n");
      this.indiceRenglon = rownum;
   }
 
   public void finalizaRenglon() 
   	throws IOException {
      xml.write("</row>\n");
   }

   public void agregaCelda(int indiceColumna, String valor, String estilo, int colspan ) 
   	throws Exception {
   		
      String   ref 				= getClaveReferencia(indiceRenglon, indiceColumna);
      String 	indiceEstilo	= (String) estilos.get(estilo);
      
      xml.write("<c r=\"");xml.write(ref);xml.write("\" t=\"inlineStr\"");
      if(indiceEstilo != null){ 
       	xml.write(" s=\"");xml.write(indiceEstilo);xml.write("\"");
      }
      xml.write(">");
      xml.write("<is><t>");escapaCaracteresEspeciales(valor);xml.write("</t></is>");
      xml.write("</c>");
      
      if(colspan>1){
      	combinaColumnas(indiceRenglon, indiceColumna, colspan);
      	// Agregar nodos fantasma
      	for(int i=1;i<colspan;i++){
      		ref = getClaveReferencia(indiceRenglon, indiceColumna+i);
      		xml.write("<c r=\"");xml.write(ref);xml.write("\" t=\"inlineStr\"");
      		if(indiceEstilo != null){ 
					xml.write(" s=\"");xml.write(indiceEstilo);xml.write("\"");
				}
      		xml.write(">");
      		xml.write("<is><t>");xml.write("");xml.write("</t></is>");
      		xml.write("</c>");
      	}
      }
      
   }
	
	public void agregaCelda(int indiceColumna, String valor, String estilo ) 
   	throws Exception {
      agregaCelda( indiceColumna,  valor, estilo, 1 );
   }

   public String getClaveReferencia(int indiceRenglon, int indiceColumna)
   	throws Exception {
   		
   	StringBuffer  	buffer 	= new StringBuffer();
   	int 				valorA  	= (int) 'A';
   	if( indiceColumna > 25 ){ // Solo rango A - Z
   		throw new Exception("No se como determinar la clave de la siguiente columna");
   	}

   	buffer.append((char) ( valorA + indiceColumna ));
   	buffer.append(String.valueOf(indiceRenglon+1));
   	return buffer.toString();
   	
   }
   
   private void combinaColumnas(int indiceRenglon, int indiceColumna, int colspan)
   	throws Exception {
   	
   	colspan = colspan - 1;

   	String inicio	= getClaveReferencia(indiceRenglon, indiceColumna);
   	String fin	   = getClaveReferencia(indiceRenglon, indiceColumna+colspan);
 
		columnasCombinadas.append("<mergeCell ref=\"");
		columnasCombinadas.append(inicio);columnasCombinadas.append(":");columnasCombinadas.append(fin);
		columnasCombinadas.append("\"/>");
		cuentaColumnasCombinadas++;
 
   }
   
   private void finalizaCombinacionColumnas()
		throws IOException {
   	if( cuentaColumnasCombinadas > 0 ){
   		xml.write("<mergeCells count=\"");xml.write(String.valueOf(cuentaColumnasCombinadas));xml.write("\">");
   			xml.write(columnasCombinadas.toString());
   		xml.write("</mergeCells>");
   	}
   }
   
   public void agregaCelda(int indiceColumna, String valor) 
     	throws Exception {
         agregaCelda(indiceColumna, valor, "", 1 );
   }
 
	
	public void escapaCaracteresEspeciales(String cadena)
		throws IOException {
 
		if( cadena == null || cadena.length() == 0){
			return;
		}
		
		for(int i=0;i<cadena.length();i++){
			char c = cadena.charAt(i);
			if(c == '<'){
				xml.write("&lt;");
			}else if(c == '>'){
				xml.write("&gt;");
			}else if(c == '"'){
				xml.write("&quot;");
			}else if(c == '&'){
				xml.write("&amp;");
			}else{
				xml.write(c);
			}
		}
 
	}

	public List<List> leerExcel(String filename) throws IOException {

		System.out.println("ComunesXLSX.leerExcel (E)");

		List<List>   filas    = new ArrayList<List>();
		List<String> columnas = new ArrayList<String>();
		String       cadena   = "";
		XSSFWorkbook workbook = null;

		try {
			FileInputStream file = new FileInputStream(new File(filename));
			// Crear el objeto que tendra el libro de Excel
			workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;

			while (rowIterator.hasNext()){
				row = rowIterator.next();
				// Obtenemos el iterator que permite recorres todas las celdas de una fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell celda;
				columnas = new ArrayList<String>();
				while (cellIterator.hasNext()){
					celda = cellIterator.next();
					// Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...
					switch(celda.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							if( DateUtil.isCellDateFormatted(celda) ){
								//System.out.println(celda.getDateCellValue());
								cadena = "" + celda.getDateCellValue();
							}else{
								//System.out.println(celda.getNumericCellValue());
								cadena = "" + celda.getNumericCellValue();
							}
							break;
						case Cell.CELL_TYPE_STRING:
							//System.out.println(celda.getStringCellValue());
							cadena = "" + celda.getStringCellValue();
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							//System.out.println(celda.getBooleanCellValue());
							cadena = "" + celda.getBooleanCellValue();
							break;
					}
					columnas.add(cadena);
				}
				filas.add(columnas);
			}
		} catch (Exception e){
			System.err.println("Error al leer el archivo. " + e);
		} finally{
			// Se cierra el libro excel
			workbook.close();
		}

		System.out.println("ComunesXLSX.leerExcel (S)");

		return filas;
	}
 
}