package com.netro.xls;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ComunesXLSV2{
	public ComunesXLSV2(){}
	
	public ComunesXLSV2(String nombre){
		this.nombre = nombre;
	}

	public ComunesXLSV2(String nombre,String orientacion){
		this.nombre = nombre;
		this.orientacion = orientacion;
	}

	public ComunesXLSV2(String nombre,String orientacion,boolean mostrarGrid,
        int tipoPapel,String alineacionHorizontalPagina,String alineacionVerticalPagina,
				String encabezadoPagina,String piePagina,int zoom,
        int escalaImpresion,int fila_inicioHeader,int fila_finHeader){
		this.nombre = nombre;
		this.orientacion = orientacion;
		this.mostrarGrid = mostrarGrid;
		this.tipoPapel = tipoPapel;
		ComunesXLSV2.alineacionHorizontalPagina = alineacionHorizontalPagina;
		ComunesXLSV2.alineacionVerticalPagina = alineacionVerticalPagina;
		ComunesXLSV2.encabezadoPagina = encabezadoPagina;
		ComunesXLSV2.piePagina = piePagina;		
		this.zoom = zoom;
		this.escalaImpresion = escalaImpresion;
		this.fila_inicioHeader = fila_inicioHeader;
		this.fila_finHeader = fila_finHeader;
	}	

	public void agregarTitulo(String titulo,int colspan){
		Fila fila = fila = new Fila();
		Celda celda = new Celda(titulo,"titulo_1",colspan);		
		fila.AgregarCelda(celda);
		this.agregarFila(fila);
	}	

	public void agregarSubtitulo(String subtitulo,int colspan){
		Fila fila = fila = new Fila();
		Celda celda = new Celda(subtitulo,"titulo_2",colspan);		
		fila.AgregarCelda(celda);
		this.agregarFila(fila);
	}	

	public void agregarSubtitulo(String subtitulo,int colspan,String alineacion){
		Fila fila = fila = new Fila();
      String estilo = "";
    if(alineacion.toLowerCase().equals("center")){
      estilo="titulo_2";
    }else if(alineacion.toLowerCase().equals("left")){
      estilo="titulo_2Left";
    }else if(alineacion.toLowerCase().equals("right")){
      estilo="titulo_2Right";
    }else{
      estilo="titulo_2";
    }
		Celda celda = new Celda(subtitulo,estilo,colspan);		
		fila.AgregarCelda(celda);
		this.agregarFila(fila);
	}	

	public void rangoRepetirImpresion(String rangoARepetirImpresion){
		this.rangoARepetirImpresion = rangoARepetirImpresion;
	}

	public void agregarFechaGeneracion(String fechaGeneracion,int colspan){
		Fila fila = fila = new Fila();
		Celda celda = new Celda(fechaGeneracion,"titulo_3",colspan);		
		fila.AgregarCelda(celda);
		this.agregarFila(fila);
	}

	public void agregarTitulo(String titulo,String estiloTitulo){
		this.estiloTitulo = estiloTitulo;
	}	

	public void agregarFila(){
		this.contenidoHoja += "  <tr></tr>\n";
	}

	public void agregarFila(Fila fila){
		this.contenidoHoja += fila.ObtenerContenidoFila();
	}

	
  //Tabla de Lista de Listas            
  
	public void agregarTabla(ComunesXLSV2 hoja,String []encabezados,String estiloEncabezados,List lRegistros,String []estiloCampo,int []celwhidth ) throws Exception {	
    int []colspanEncabezados =new int[encabezados.length];
    for(int i = 0; i<encabezados.length;i++ )
      colspanEncabezados[i] = 1;
    agregarTabla(hoja,encabezados,colspanEncabezados,estiloEncabezados,lRegistros,estiloCampo,celwhidth);
	}  
  
	public void agregarTabla(ComunesXLSV2 hoja,String []encabezados,int []colspanEncabezados,String estiloEncabezados,List lRegistros,String []estiloCampo,int []celwhidth ){	
		int numCol = encabezados.length;
		List lCampos = null;
		Fila fila =  null;
		Celda celda = null;		
		fila = new Fila();
		//Encabezados. 		
		for(int i=0;i<numCol;i++){			 
			celda = new Celda(encabezados[i],estiloEncabezados,1,celwhidth[i]);
			fila.AgregarCelda(celda);				
		}//for

		if(numCol>0){
			hoja.agregarFila(fila);
		}		
		//DATOS.
		for(int i=0;i<lRegistros.size();i++){
			fila = new Fila();
			lCampos = (ArrayList) lRegistros.get(i);
			for(int j=0;j<lCampos.size();j++){			 
				celda = new Celda((String) lCampos.get(j),estiloCampo[j]);
				fila.AgregarCelda(celda);			
			}//for1
			hoja.agregarFila(fila);
		}		
	}  
  
  /*
	public void agregarTabla(ComunesXLSV2 hoja,String []encabezados,int []colspanEncabezados,String estiloEncabezados,List lRegistros,String []estiloCampo) throws Exception {	
		int numCol = encabezados.length;
		List lCampos = null;
		Fila fila =  null;
		Celda celda = null;		
		fila = new Fila();
    try{
      System.out.println("agregarTabla(E)");
      //Encabezados
      for(int i=0;i<numCol;i++){
        if(colspanEncabezados[i]<1){
         throw new Exception("Error El colspan no puedes ser menor a 1"); 
        }
        celda = new Celda(encabezados[i],estiloEncabezados,colspanEncabezados[i]);
        fila.AgregarCelda(celda);
      }
      if(numCol>0){
        hoja.agregarFila(fila);
      }
      //Datos
      for(int i=0;i<lRegistros.size();i++){
        fila = new Fila();
        lCampos = (ArrayList) lRegistros.get(i);
        for(int j=0;j<lCampos.size();j++){
          celda = new Celda((String) lCampos.get(j),estiloCampo[j],colspanEncabezados[j]);
          fila.AgregarCelda(celda);
        }
        hoja.agregarFila(fila);
      }
    }catch(Exception e){
      System.out.println("agregarTabla-Exception"+e);      
      e.printStackTrace();
      throw e;
    }finally{
      System.out.println("agregarTabla(S)");
    }
	}*/
  
  /*
	//Tabla de Lista de Listas
	public void agregarTabla(ComunesXLSV2 hoja,String []encabezados,String estiloEncabezados,List lRegistros,String []estiloCampo){	
		int numCol = encabezados.length;
		List lCampos = null;
		Fila fila =  null;
		Celda celda = null;		
		fila = new Fila();
		//Encabezados. Si se nececitan celdas con mas columnas por default Agregar un nuevo IF con el numero de columnas que se necesita abarque la celda 		
		for(int i=0;i<numCol;i++){
			if(i == 0){
			celda = new Celda(encabezados[i],estiloEncabezados,2);
			fila.AgregarCelda(celda);
			}else{
			if(i == 1){
			celda = new Celda(encabezados[i],estiloEncabezados,2);
			fila.AgregarCelda(celda);
			}else{
			if(i == 2){
				celda = new Celda(encabezados[i],estiloEncabezados,2);
			fila.AgregarCelda(celda);
			}else{
			if(i == 3 ){
			celda = new Celda(encabezados[i],estiloEncabezados,2);
			fila.AgregarCelda(celda);
			}else{ 
			celda = new Celda(encabezados[i],estiloEncabezados);
			fila.AgregarCelda(celda);
		}}}}}
		if(numCol>0){
			hoja.agregarFila(fila);
		}		
		//DATOS. Si se nececitan celdas con mas columnas por default Agregar un nuevo IF con el numero de columnas que se necesita abarque la celda
		for(int i=0;i<lRegistros.size();i++){
			fila = new Fila();
			lCampos = (ArrayList) lRegistros.get(i);
			for(int j=0;j<lCampos.size();j++){
				if(j==0){
				celda = new Celda((String) lCampos.get(j),estiloCampo[j],2);
				fila.AgregarCelda(celda);
				}else{
				if(j==1){
				celda = new Celda((String) lCampos.get(j),estiloCampo[j],2);
				fila.AgregarCelda(celda);
				}else{
				if(j==2){
				celda = new Celda((String) lCampos.get(j),estiloCampo[j],2);
				fila.AgregarCelda(celda);
				}else{
				if(j == 3 ){
				celda = new Celda((String) lCampos.get(j),estiloCampo[j],2);
				fila.AgregarCelda(celda);
				}else{ 
				celda = new Celda((String) lCampos.get(j),estiloCampo[j]);
				fila.AgregarCelda(celda);
				}}}}
			}
			hoja.agregarFila(fila);
		}
		//
	}  */
  

	//Tabla de Registros separados por saltos de linea y comas
	public void agregarTabla(ComunesXLSV2 hoja,String []encabezados,String estiloEncabezados,String lRegistros,String []estiloCampo){
		int numCol = encabezados.length;
		StringTokenizer stRegistros = new StringTokenizer(lRegistros,"\n");
		StringTokenizer stCampos = null;
		int numFilas= stRegistros.countTokens();
		int numCols = 0;
		List lCampos = null;
		Fila fila =  null;
		Celda celda = null;
		fila = new Fila();
		//Encabezados
		for(int i=0;i<numCol;i++){
			celda = new Celda(encabezados[i],estiloEncabezados);
			fila.AgregarCelda(celda);
		}
		if(numCol>0){
			hoja.agregarFila(fila);
		}
		//Datos
		for(int i=0;i<numFilas;i++){
			fila = new Fila();
			stCampos = new StringTokenizer(stRegistros.nextToken(),"|");
			numCols = stCampos.countTokens();
			for(int j=0;j<numCols;j++){				
				celda = new Celda(stCampos.nextToken(),estiloCampo[j]);
				fila.AgregarCelda(celda);
			}
			hoja.agregarFila(fila);
		}
	}

	public String obtenerHoja(){
		return  (this.datosDefault + 
				 this.headPage1 +
				 "<style>\n"+
				 "<!--table\n"+
				 this.obtenerPropiedadesPagina()+
				 this.estilosDefault+
				 this.obtenerPropiedadesHoja()+
				 "<body link=blue vlink=purple> \n"+
				 "<table border=0 cellpadding=0 cellspacing=0 width=1267 style=\"border-collapse: collapse;table-layout:fixed;width:952pt\" >\n"+
				 this.contenidoHoja+
				 "</table>\n"+
				 "</body>\n</html>\n");
	}

	private String nombre = "Hoja1";
	private String titulo = "Titulo";
	private String contenidoHoja = "";
	private String estiloTitulo = "defaultTitle";
	private String tabla = "";
	private boolean mostrarGrid = true;
	private String propiedadesPagina = "";
	private String rangoARepetirImpresion = "";

	public static String VERTICAL = "portrait";
	public static String HORIZONTAL = "landscape";
	private String orientacion = "";

	public static String PAGINA_H_LEFT = "left";
	public static String PAGINA_H_CENTER = "center";
	public static String PAGINA_H_RIGHT = "right";
	private static String alineacionHorizontalPagina = "";	

	public static String PAGINA_V_TOP = "top";
	public static String PAGINA_V_CENTER = "center";
	public static String PAGINA_V_BOTTOM = "bottom";
	public static String PAGINA_V_JUSTIFIED = "justified";	
	private static String alineacionVerticalPagina = "";	

	public static String ENCABEZADO_PAGINA_DEFAULT = "";
	public static String PIE_PAGINA_DEFAULT = "\"&C&P de &\\#\"";
	
	private static String encabezadoPagina = "";
	private static String piePagina = "";	
	
	public static int PAPEL_CARTA = 18;
	public static int PAPEL_OFICIO = 5;
	private int tipoPapel = 18;
  
	public int zoom = 50;
	public int escalaImpresion = 60;
  
	public int fila_inicioHeader = 1;
	public int fila_finHeader = 2;  
	
	private String datosDefault = 
				"<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n"+
				"xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n"+
				"xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
	private String headPage1 = 
				"<head>\n"+
				"<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n"+
				"<meta name=ProgId content=Excel.Sheet>\n";

	public String obtenerPropiedadesPagina(){

		this.propiedadesPagina = "@page {\n";    
		if(!ComunesXLSV2.encabezadoPagina.equals(""))
			this.propiedadesPagina += "	 mso-header-data:\""+ComunesXLSV2.encabezadoPagina+"\";\n";
		if(!ComunesXLSV2.piePagina.equals(""))
			this.propiedadesPagina += "	 mso-footer-data:"+ComunesXLSV2.piePagina+";\n";
			
		this.propiedadesPagina += " 	 margin:.50in .50in .50in .50in;\n"+
								  " 	 mso-header-margin:0in;\n"+
								  "		 mso-footer-margin:0in;\n";
		
		if(!this.orientacion.equals(""))
			this.propiedadesPagina += "	mso-page-orientation:"+this.orientacion+";\n";

		if(!ComunesXLSV2.alineacionHorizontalPagina.equals(""))
			this.propiedadesPagina += "  mso-horizontal-page-align:"+ComunesXLSV2.alineacionHorizontalPagina+";\n";
		
		if(!ComunesXLSV2.alineacionVerticalPagina.equals(""))
			this.propiedadesPagina += "  mso-vertical-page-align:"+ComunesXLSV2.alineacionVerticalPagina+";\n";
			
		this.propiedadesPagina += "} \n";

		return this.propiedadesPagina;
	}

	public String obtenerPropiedadesHoja(){
		/*
			Dependiendo la alineacion de la hoja vertical u horizontal, hay que ajustar los parametros de TAMA�O DE LAS COLUMNAS
			 y ESCALA DE IMPRESION para ajustar la pagina.
		*/
		return  "<!--[if gte mso 9]><xml> \n"+
				" <x:ExcelWorkbook> \n"+
				"  <x:ExcelWorksheets> \n"+
				"   <x:ExcelWorksheet> \n"+
				"    <x:Name>" +  this.nombre +"</x:Name> \n"+ //-------> NOMBRE WORKSHEET
				"    <x:WorksheetOptions> \n"+				
				//"     <x:DefaultColWidth>23</x:DefaultColWidth> \n"+ //TAMA�O DE LAS COLUMNAS 
				"     <x:Print> \n"+
				"      <x:ValidPrinterInfo/> \n"+
        "      <x:ValidPrinterInfo/> \n"+                
        "      <x:PaperSizeIndex>"+tipoPapel+"</x:PaperSizeIndex> \n"+        
        "      <x:Scale>"+ this.escalaImpresion +"</x:Scale> \n"+ //-------> ESCALA IMPRESION %
				"      <x:HorizontalResolution>600</x:HorizontalResolution> \n"+
				"      <x:VerticalResolution>600</x:VerticalResolution> \n"+
				"     </x:Print> \n"+
				//"     <x:Zoom>80</x:Zoom> \n"+		 //-------> ZOOM WORKSHEET %
        "     <x:Zoom>"+ this.zoom + "</x:Zoom> \n"+		 //-------> ZOOM WORKSHEET %
				"     <x:Selected/> \n"+
				((mostrarGrid)?"     <x:Gridlines/>\n":"     <x:DoNotDisplayGridlines/> \n")+
				"     <x:Panes> \n"+
				"      <x:Pane> \n"+
				"       <x:Number>3</x:Number> \n"+
				//"       <x:ActiveRow>17</x:ActiveRow> \n"+
				//"       <x:ActiveCol>9</x:ActiveCol> \n"+
				"      </x:Pane> \n"+
				"     </x:Panes> \n"+
				"     <x:ProtectContents>False</x:ProtectContents> \n"+
				"     <x:ProtectObjects>False</x:ProtectObjects> \n"+
				"     <x:ProtectScenarios>False</x:ProtectScenarios> \n"+
				"    </x:WorksheetOptions> \n"+
				"   </x:ExcelWorksheet> \n"+
				"  </x:ExcelWorksheets> \n"+
				//"  <x:WindowHeight>12405</x:WindowHeight> \n"+
				//"  <x:WindowWidth>18780</x:WindowWidth> \n"+
				"  <x:WindowTopX>120</x:WindowTopX> \n"+
				"  <x:WindowTopY>45</x:WindowTopY> \n"+
				"  <x:ProtectStructure>False</x:ProtectStructure> \n"+
				"  <x:ProtectWindows>False</x:ProtectWindows> \n"+
				" </x:ExcelWorkbook> \n"+	
				
				(((this.fila_inicioHeader<=0 || this.fila_finHeader <=0) || this.fila_inicioHeader > this.fila_finHeader )?"":
				" <x:ExcelName> \n"+
				"	<x:Name>Print_Titles</x:Name> \n"+
				"	<x:SheetIndex>1</x:SheetIndex> \n"+
				//"	<x:Formula>=\'" +  this.nombre +"\'"+this.rangoARepetirImpresion+"</x:Formula> \n"+  //-------> DEFINIR CELDAS SUPERIORES A REPETIR EN IMPRESION $1:$7
        "	<x:Formula>=\'" +  this.nombre +"\'" + "!$" +this.fila_inicioHeader+ ":$" +this.fila_finHeader+ "</x:Formula> \n"+  
				" </x:ExcelName> \n")+
				
				" </xml><![endif]--><!--[if gte mso 9]><xml> \n"+
				"	<o:shapedefaults v:ext=\"edit\" spidmax=\"2050\"/> \n"+
				"	</xml><![endif]--><!--[if gte mso 9]><xml> \n"+
				"	<o:shapelayout v:ext=\"edit\"> \n"+
				"	<o:idmap v:ext=\"edit\" data=\"2\"/> \n"+
				" </o:shapelayout></xml><![endif]--> \n"+
				"</head> \n";	
	}
				
	private String estilosDefault =
				//Filas
				"tr \n"+
				"	{mso-height-source:auto;}	\n"+
				//Columnas
				//"col	\n"+
				//"	{mso-width-source:auto;} \n"+
				//Nueva Fila
				"br \n"+
				"	{mso-data-placement:same-cell;}\n"+				
				//Estilos style0
				".style0	\n"+
				"	{mso-number-format:General;	\n"+
				"	text-align:general;	\n"+
				"	vertical-align:bottom; \n"+
				"	white-space:nowrap;\n"+
				"	mso-rotate:0;	\n"+
				"	mso-background-source:auto;	\n"+
				"	mso-pattern:auto;	\n"+
				"	color:windowtext;	\n"+
				"	font-size:10.0pt;	\n"+
				"	font-weight:400; \n"+
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	mso-font-charset:0; \n"+
				"	border:none; \n"+
				"	mso-protection:locked visible; \n"+
				"	mso-style-name:Normal; \n"+
				"	mso-style-id:0;} \n"+
				//Celda
				"td \n"+
				"	{mso-style-parent:style0; \n"+
				"	padding-top:12px; \n"+
				"	padding-right:30px; \n"+
				"	padding-left:9px;	\n"+
			//	"	mso-ignore:padding; \n"+	
				"	color:windowtext; \n"+
				"	font-size:10.0pt; \n"+
				"	font-weight:400; \n"+
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	mso-font-charset:0; \n"+
				"	mso-number-format:General; \n"+
				"	text-align:general; \n"+
				"	vertical-align:bottom; \n"+
				"	border:none; \n"+
				"	mso-background-source:auto; \n"+
				"	mso-pattern:auto; \n"+
				"	mso-protection:locked visible; \n"+
				"	white-space:nowrap; \n"+
				"	mso-rotate:0;} \n"+						
				//Estilos registroNumerico
				".registroNumerico \n"+
				"	{mso-style-parent:style0; \n"+
        "	mso-number-format:0; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:.5pt solid gray; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	white-space:normal;} \n"+				
				//Estilos registroFecha
				".registroFecha \n"+
				"	{mso-style-parent:style0; \n"+
				"	mso-number-format:\"Short Date\"; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	font-size:8.0pt; \n"+
				"	font-weight:400; \n"+
				"	white-space:normal;} \n"+
				//Estilos registroFecha9
				".registroFecha9 \n"+
				"	{mso-style-parent:style0; \n"+
				"	mso-number-format:\"Short Date\"; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	font-size:9.0pt; \n"+
				"	font-weight:400; \n"+
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	white-space:normal;} \n"+
				//Estilos registroFecha10
				".registroFecha10 \n"+
				"	{mso-style-parent:style0; \n"+
				"	mso-number-format:\"Short Date\"; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	font-size:10.0pt; \n"+
				"	font-weight:400; \n"+
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	white-space:normal;} \n"+
				//Estilos registroFecha12
				".registroFecha12 \n"+
				"	{mso-style-parent:style0; \n"+
				"	mso-number-format:\"Short Date\"; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:400; \n"+
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	white-space:normal;} \n"+        
			//Estilos registroTxt
				".registroTxt \n"+
				"	{mso-style-parent:style0; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	vertical-align:middle; \n"+
				"	padding-right:125px; \n"+
				"	font-size:7.0pt; \n"+
				"	font-weight:400; \n"+	
				"	white-space:normal;} \n"+	
				//Estilos registroTxt12
				".registroTxt12 \n"+
				"	{mso-style-parent:style0; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	vertical-align:middle; \n"+
				"	padding-right:125px; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:400; \n"+	
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Arial; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	white-space:normal;} \n"+
				//Estilos registroTxt12_Times New Roman
				".registroTxt12_Times_New_Roman \n"+
				"	{mso-style-parent:style0; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	vertical-align:middle; \n"+
				"	padding-right:125px; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:400; \n"+	
				"	font-style:normal; \n"+
				"	text-decoration:none; \n"+
				"	font-family:Times New Roman; \n"+
				"	mso-generic-font-family:auto; \n"+
				"	white-space:normal;} \n"+
				//Estilos registroHora
				".registroHora \n"+
				"	{mso-style-parent:style0; \n"+
				"	mso-number-format:\"Short Time\"; \n"+
				"	border-top:none; \n"+
				"	border-right:.5pt solid gray; \n"+
				"	border-bottom:.5pt solid gray; \n"+
				"	border-left:none; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	white-space:normal;} \n"+
				//Estilos xl28
				".xl28 \n"+
				"	{mso-style-parent:style0; \n"+
				"	text-align:center;} \n"+
				//Estilos titulo_1
				".titulo_1 \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:20.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle;} \n"+
				//Estilos titulo_2
				".titulo_2 \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:center;} \n"+
				//Estilos titulo_2Right
				".titulo_2Right \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:right;} \n"+        
				//Estilos titulo_2Left
				".titulo_2Left \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:12.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:left;} \n"+                
				//Estilos titulo_3
				".titulo_3 \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:9.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:center;} \n"+
					//Estilos horaActual_10
				".horaActual \n"+
				"	{mso-style-parent:style0; \n"+
				"	font-size:10.0pt; \n"+
				"	font-weight:700; \n"+
				"	font-family:Arial, sans-serif; \n"+
				"	mso-font-charset:0; \n"+
				"	vertical-align:middle; \n"+
				"	text-align:center;} \n"+
				//Estilos headerReport
				".headerReport \n"+
				"	{mso-style-parent:style0; \n"+
				"	color:white; \n"+
				"	font-weight:300; \n"+
				"	text-align:center; \n"+
				"	vertical-align:middle; \n"+
				"	border:.5pt solid windowtext; \n"+
				"	background:#333399; \n"+
				"	mso-pattern:auto none; \n"+
				"	white-space:normal;} \n"+
				"--> \n"+
				"</style> \n";				
}