package com.netro.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFRegionUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;

public class ComunesXLS {

    private int sheetNum	= 0;
    private int maxColum	= 0;
    private int iniColum	= 0;
    private boolean borde	= false;  // @deprecated usar EstiloCelda
    int 		lastCol		= 0,
    			lastRow		= 0,
    			columnaAct	= 0;

	private FileOutputStream salida;
	private HSSFWorkbook libro;
	private HSSFSheet hoja;

	private HSSFCellStyle cs;
	private HSSFRow fila;
	private HSSFCell celda; // @deprecated usar EstiloCelda

	private HSSFFont fArl10;
	private HSSFFont fArl10r;
	private HSSFFont fArlB10;
	private HSSFFont fArl12;
	private HSSFFont fArlB12;
	private HSSFFont ftitulo;
	private HSSFFont fArl10t;

	private EstiloCelda estiloCelda = null;


	public static int	LEFT 		= HSSFCellStyle.ALIGN_LEFT;
	public static int	CENTER 	= HSSFCellStyle.ALIGN_CENTER;
	public static int	RIGHT 	= HSSFCellStyle.ALIGN_RIGHT;

	public static int	VERTICAL_TOP 		= HSSFCellStyle.VERTICAL_TOP;
	public static int	VERTICAL_CENTER 	= HSSFCellStyle.VERTICAL_CENTER;
	public static int	VERTICAL_BOTTOM 	= HSSFCellStyle.VERTICAL_BOTTOM;
	public static int	VERTICAL_JUSTIFY 	= HSSFCellStyle.VERTICAL_JUSTIFY;

	public static short	tipoBold 	= HSSFFont.BOLDWEIGHT_BOLD;
	public static short tipoNormal	= HSSFFont.BOLDWEIGHT_NORMAL;

	public static short colorAutom	= HSSFFont.COLOR_NORMAL;
	public static short colorRojo	= HSSFFont.COLOR_RED;


	public ComunesXLS()	{}

	public ComunesXLS(String rutaNomArh) {
		this(rutaNomArh, "Hoja1");
	}//ComunesXLS

	public ComunesXLS(String rutaNomArh, String nomHoja) {
		creaXLS(rutaNomArh, nomHoja);
		iniciaFuentes();
	}//ComunesXLS

	public void creaXLS(String rutaNomArh, String nomHoja) {
		try {
			salida = new FileOutputStream(rutaNomArh);
			libro = new HSSFWorkbook();
			hoja 	= libro.createSheet(nomHoja);
			hoja.setPrintGridlines(true);
			estiloCelda = new EstiloCelda(libro);
		} catch (Exception e) {
			System.out.println("ComunesPDF::creaXLS(Exception)");
			e.printStackTrace();
		}
	}//creaXLS

	public void creaHoja(String nomHoja) {
		try {
			hoja = libro.createSheet(nomHoja);
			hoja.setPrintGridlines(true);
			lastRow = 0;
		} catch (Exception e) {
			System.out.println("ComunesPDF::creaXLS(Exception)");
			e.printStackTrace();
		}
	}

	/**
	 *  @deprecated usar la clase EstiloCelda
	 */
	public void iniciaFuentes() {
		try {
			fArl10 	= creaFuente((short) 10, colorAutom, tipoNormal, "Arial");
			fArl10r 	= creaFuente((short) 10, HSSFColor.RED.index, tipoNormal, "Arial");
			fArlB10 = creaFuente((short) 10, colorAutom, tipoBold, "Arial");
			fArl12 	= creaFuente((short) 12, colorAutom, tipoNormal, "Arial");
			fArlB12	= creaFuente((short) 12, colorAutom, tipoBold, "Arial");
			ftitulo = creaFuente((short) 12, HSSFColor.BLUE.index, tipoBold, "Arial");
			fArl10t = creaFuente((short) 10, HSSFColor.TEAL.index, tipoNormal, "Arial");
		} catch (Exception e) {
			System.out.println("ComunesPDF::iniciaFuentes(Exception)");
			e.printStackTrace();
		}
	}//iniciaFuentes

	private HSSFFont creaFuente(short puntos, short color, short tipo, String nombre) {
		HSSFFont fuente = this.libro.createFont();
		try {
			fuente.setFontHeightInPoints(puntos);
			fuente.setColor(color);
			fuente.setBoldweight(tipo);
			fuente.setFontName(nombre);
		} catch (Exception e) {
			System.out.println("ComunesPDF::creaFuente(Exception)");
			e.printStackTrace();
		}
		return fuente;
	}//creaFuente

	/**
	 *  @deprecated usar la clase EstiloCelda
	 */
	private void setEstiloCelda(String clase) {
		try {
			cs = this.libro.createCellStyle();
			if(borde) {
				cs.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				cs.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				cs.setBorderRight(HSSFCellStyle.BORDER_THIN);
				cs.setBorderTop(HSSFCellStyle.BORDER_THIN);
			}
			if("formas".equals(clase))
				cs.setFont(fArl10);
			else if("formast".equals(clase))
				cs.setFont(fArl10t);
			else if("formasb".equals(clase))
				cs.setFont(fArlB10);
			else if("formast".equals(clase))
				cs.setFont(fArl12);
			else if("formastb".equals(clase))
				cs.setFont(fArlB12);
			else if("formasr".equals(clase))
				cs.setFont(fArl10r);
			else if("titulo".equals(clase))
				cs.setFont(ftitulo);
			else if("celda01".equals(clase)) {
				//cs.setFillBackgroundColor(HSSFColor.AQUA.index);
				//cs.setFillPattern(HSSFCellStyle.BIG_SPOTS);
				cs.setFont(fArlB10);
				cs.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			} else
				cs.setFont(fArl10);

		} catch (Exception e) {
			System.out.println("ComunesPDF::setEstiloCelda(Exception)");
			e.printStackTrace();
		}
	}//setEstiloCelda

	public void agregaTitulo(String contenido, int colspan) {
		try {
			creaFila();
			setCelda(contenido, "titulo", CENTER, colspan);
		} catch (Exception e) {
			System.out.println("ComunesPDF::agregaTitulo(Exception)");
			e.printStackTrace();
		}
	}//agregaTitulo

	public void creaFila() {
		try {
			fila = hoja.createRow(lastRow);
			lastRow++;
			columnaAct = iniColum;
		} catch (Exception e) {
			System.out.println("ComunesPDF::creaFila(Exception)");
			e.printStackTrace();
		}
	}//creaFila

	public void setColumnaIni(int columna) {
		try {
			iniColum = columna;
		} catch (Exception e) {
			System.out.println("ComunesPDF::setColumnaIni(Exception)");
			e.printStackTrace();
		}
	}//setColumnaIni

	public void cierraTabla() {
		try {
			iniColum = 0;
			maxColum = 0;
			estiloCelda.setBorde(false);//borde = false;
		} catch (Exception e) {
			System.out.println("ComunesPDF::setColumnaIni(Exception)");
			e.printStackTrace();
		}
	}//setColumnaIni

	/**
	 *  @deprecated usar la clase EstiloCelda
	 */
	public void setBorde(boolean borde) {
		try {
			this.borde = borde;
		} catch (Exception e) {
			System.out.println("ComunesPDF::setBorde(Exception)");
			e.printStackTrace();
		}
	}//setBorde

	public void setCelda(Object contenido, String clase, int alineacion, int colspan) {
		try {
			String auxContenido = "";

			if(hoja.getLastRowNum()==0)
				creaFila();
			celda = fila.createCell((short) columnaAct);

			/*if(!"".equals(clase)) {
				setEstiloCelda(clase);
			}

			cs.setAlignment((short) alineacion);*/
			HSSFCellStyle cs = estiloCelda.getCellStyle(clase,alineacion,VERTICAL_CENTER);
			celda.setCellStyle(cs);
			if(contenido instanceof String)
				celda.setCellValue(contenido.toString());
			if(contenido instanceof Double)
				celda.setCellValue((new Double(contenido.toString())).doubleValue());

			if(colspan>1) {
				CellRangeAddress region = new CellRangeAddress((lastRow-1), (lastRow-1), (short) columnaAct, (short) (columnaAct+(colspan-1)));
				hoja.addMergedRegion(region);
				if(estiloCelda.getBorde()) {
					HSSFRegionUtil.setBorderBottom(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
					HSSFRegionUtil.setBorderTop(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
					HSSFRegionUtil.setBorderLeft(HSSFCellStyle.BORDER_THIN,region, hoja, libro);
					HSSFRegionUtil.setBorderRight(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
				}//if(borde)


				columnaAct+=(colspan-1);
			}
			columnaAct++;

			if(columnaAct>((maxColum+iniColum)-1))
				creaFila();

		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}//setCelda

	public void setCelda(Object contenido, String clase, int alineacionHor, int alineacionVer, int colspan, int rowspan) {
		try {
			String auxContenido = "";

			if(hoja.getLastRowNum()==0)
				creaFila();
			celda = fila.createCell((short) columnaAct);

			/*if(!"".equals(clase)) {
				setEstiloCelda(clase);
			}

			cs.setAlignment((short)alineacionHor);
			cs.setVerticalAlignment((short)alineacionVer);*/

			HSSFCellStyle cs = estiloCelda.getCellStyle(clase,alineacionHor,alineacionVer);
			celda.setCellStyle(cs);
			if(contenido instanceof String)
				celda.setCellValue(contenido.toString());
			if(contenido instanceof Double)
				celda.setCellValue((new Double(contenido.toString())).doubleValue());

			if( colspan > 1 || rowspan > 1) {
				CellRangeAddress region = new CellRangeAddress((lastRow-1), (lastRow-1)+(rowspan-1),(short) columnaAct,(short) (columnaAct+(colspan-1)));
				hoja.addMergedRegion(region);
				if(estiloCelda.getBorde()) {
					HSSFRegionUtil.setBorderBottom(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
					HSSFRegionUtil.setBorderTop(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
					HSSFRegionUtil.setBorderLeft(HSSFCellStyle.BORDER_THIN,region, hoja, libro);
					HSSFRegionUtil.setBorderRight(HSSFCellStyle.BORDER_THIN, region, hoja, libro);
				}//if(borde)

				columnaAct+=(colspan-1);
			}
			columnaAct++;

			if(columnaAct>((maxColum+iniColum)-1))
				creaFila();

		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}

	public void setCelda(Object contenido, String clase, int alineacion) {
		try {
			setCelda(contenido, clase, alineacion, 1);
		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}//setCelda

	public void setCelda(Object contenido, String clase) {
		try {
			setCelda(contenido, clase, LEFT);
		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}//setCelda

	public void setCelda(Object contenido) {
		try {
			setCelda(contenido, "formas");
		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}//setCelda

	public void cierraXLS() {
		try {
			libro.write(salida);
			salida.close();

		} catch (Exception e) {
			System.out.println("ComunesPDF::setCelda(Exception)");
			e.printStackTrace();
		}
	}//cierraXLS

	public void setTabla(int cols, int colIni) {
		try {
			maxColum = cols;
			iniColum = colIni;
			creaFila();
		} catch (Exception e) {
			System.out.println("ComunesPDF::setTabla(Exception)");
			e.printStackTrace();
		}//setTabla
	}

	public void setTabla(int cols) {
		try {
			setTabla(cols, 0);
		} catch (Exception e) {
			System.out.println("ComunesPDF::setTabla(Exception)");
			e.printStackTrace();
		}//setTabla
	}

	public String generarXLS(String nombre_hoja, String titulo, String []encabezados, List lDatos, String ruta){
		System.out.println("ComunesXLS::generarXLS(E)");
		String mensaje="";
		try{

			creaXLS(ruta, nombre_hoja);
			//iniciaFuentes(); // Debug info

			int numCol = encabezados.length;

			setTabla(numCol);
			if(!titulo.equals("")){
				setCelda(titulo, "titulo", CENTER, numCol-1);
			}

			for(int i=0;i<(numCol);i++){
				setCelda(encabezados[i], "formasb", CENTER);
			}

			//setEstiloCelda("formas"); Debug info
			HSSFCellStyle cs = estiloCelda.getCellStyle("formas",LEFT,VERTICAL_CENTER);
			celda.setCellStyle(cs);

			for (int i = 0; i < lDatos.size(); i++){
				List lInformacion = (List) lDatos.get(i);
				for (int j = 0;j<(numCol); j++) {
					setCelda(((lInformacion.get(j)==null)?"":lInformacion.get(j).toString()), "");
				}
			}

			cierraXLS();
		}catch(Exception e){
			e.printStackTrace();
			mensaje = "Ocurrio un error al tratar de generar el archivo";
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
		}finally{
			System.out.println("ComunesXLS::generarXLS(S)");
		}
		return mensaje;
	}//generarXLS

	public ArrayList LeerXLS(String filename,boolean firstRowContainsColNames) {
        System.out.println("ComunesXLS::LeerXLS(E)");
        ArrayList dataArray = new ArrayList();
        try{
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filename));
            HSSFWorkbook wb = new HSSFWorkbook(fs);

            /*
            if(wb.getNumberOfSheets()>1) {
                throw new Exception("El archivo contiene m�s de una hoja");
            }*/

            HSSFSheet sheet = wb.getSheetAt(sheetNum);
            //Numero de Filas
            this.lastRow = sheet.getLastRowNum();
            if(this.lastRow == 0){
                throw new Exception("La hoja:" + sheetNum+ " No contiene informaci�n");
            }
            // Numero de Columnas
            HSSFRow firstRow = sheet.getRow(0);
            this.lastCol = firstRow.getLastCellNum();
            ArrayList currRowList = null;
            HSSFRow currRow = null;
            Iterator cellIt = null;
            HSSFCell currCell = null;
            String currCellVal = null;
            int cellType;
            Iterator it = sheet.rowIterator();
			if(firstRowContainsColNames)//Primer fila - nombres de las columnas
				it.next();
            while (it.hasNext()) {
                currRow = (HSSFRow) it.next();
                currRowList = new ArrayList();
                this.lastCol = currRow.getLastCellNum();
                String currVal = null;
                for (int i = 0; i < lastCol; i++) {
                    currCell = currRow.getCell((short) i);
                    if (currCell != null) {
						//System.out.print(currRow.getRowNum() + ":"      + currCell.getCellNum() + "*");
                        currVal = getCellValue(currCell);
						//System.out.println(currVal+"-");
                        currRowList.add(currVal);
                    } else {
                        currRowList.add("");
                    }
                }
                dataArray.add(currRowList);
            }
        } catch (NumberFormatException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (FileNotFoundException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (IOException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (Exception e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } finally {
			System.out.println("ComunesXLS::generarXLS(S)");
        }
        return dataArray;
    }//LeerXLS

    public ArrayList LeerXLS(InputStream filename,boolean firstRowContainsColNames) {
        System.out.println("ComunesXLS::LeerXLS(E)");
        ArrayList dataArray = new ArrayList();
        try{
            POIFSFileSystem fs = new POIFSFileSystem(filename);
            HSSFWorkbook wb = new HSSFWorkbook(fs);

            /*
            if(wb.getNumberOfSheets()>1) {
                throw new Exception("El archivo contiene m�s de una hoja");
            }*/

            HSSFSheet sheet = wb.getSheetAt(sheetNum);
            //Numero de Filas
            this.lastRow = sheet.getLastRowNum();
            if(this.lastRow == 0){
                throw new Exception("La hoja:" + sheetNum+ " No contiene informaci�n");
            }
            // Numero de Columnas
            HSSFRow firstRow = sheet.getRow(0);
            this.lastCol = firstRow.getLastCellNum();
            ArrayList currRowList = null;
            HSSFRow currRow = null;
            Iterator cellIt = null;
            HSSFCell currCell = null;
            String currCellVal = null;
            int cellType;
            Iterator it = sheet.rowIterator();
			if(firstRowContainsColNames)//Primer fila - nombres de las columnas
				it.next();
            while (it.hasNext()) {
                currRow = (HSSFRow) it.next();
                currRowList = new ArrayList();
                this.lastCol = currRow.getLastCellNum();
                String currVal = null;
                for (int i = 0; i < lastCol; i++) {
                    currCell = currRow.getCell((short) i);
                    if (currCell != null) {
						//System.out.print(currRow.getRowNum() + ":"      + currCell.getCellNum() + "*");
                        currVal = getCellValue(currCell);
						//System.out.println(currVal+"-");
                        currRowList.add(currVal);
                    } else {
                        currRowList.add("");
                    }
                }
                dataArray.add(currRowList);
            }
        } catch (NumberFormatException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (FileNotFoundException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (IOException e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } catch (Exception e) {
			System.out.println("ComunesXLS::generarXLS(Exception) - error:"+e);
        } finally {
			System.out.println("ComunesXLS::generarXLS(S)");
        }
        return dataArray;
    }//LeerXLS

    private String getCellValue(HSSFCell cell) {
        String value = null;
		int cellType = 0;
        if (cell != null) {
            cellType = cell.getCellType();
            switch (cellType) {
				case HSSFCell.CELL_TYPE_BOOLEAN: {
					value = String.valueOf(cell.getBooleanCellValue());
					break;
				}
				case HSSFCell.CELL_TYPE_STRING: {
					value = cell.getStringCellValue();
					break;
				}
				case HSSFCell.CELL_TYPE_NUMERIC: {
					double tempCellVal = cell.getNumericCellValue();
					if(HSSFDateUtil.isCellDateFormatted(cell)){
						java.util.Date date = HSSFDateUtil.getJavaDate(tempCellVal);
						value = new java.text.SimpleDateFormat("dd/MM/yyyy").format(date);
					} else {
						Double dcurrCellVal = new Double(tempCellVal);
						double valFloor = Math.floor(tempCellVal);
						if((tempCellVal - valFloor) > 0) {
							String tempvalue = tempCellVal + "";
							value = new Double(tempCellVal).toString();
						} else {
							value = new Double(valFloor).longValue()  + "";
//							value = new Integer(new Double(tempCellVal).intValue()).toString();
						}
					}
					break;
				}
				case HSSFCell.CELL_TYPE_FORMULA:
				case HSSFCell.CELL_TYPE_ERROR:
				case HSSFCell.CELL_TYPE_BLANK:
				default:{
					value = "";
					break;
				}
            }
        } else {
            value = "";
        }
        return value;
    }//getCellValue

	/**
	 * M�todo creado por Agust�n Bautista Ruiz
	 * Es una versi�n m�s sencilla del m�todo LeerXLS(ruta, false).
	 * No contempla los encabezados
	 * @return Lista con los datos leidos del archivo
	 * @throws IOException
	 */
	public List<List> leerExcel(String filename) throws IOException {

		System.out.println("ComunesXLS.leerExcel (E)");

		List<List>   filas    = new ArrayList<List>();
		List<String> columnas = new ArrayList<String>();
		HSSFWorkbook workbook = null;
		String       cadena   = "";

		try {

			FileInputStream file = new FileInputStream(new File(filename));

			// Crear el objeto que tendra el libro de Excel
			workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;

			// Se recorren todas las filas para mostrar el contenido de cada celda
			while (rowIterator.hasNext()){

				row = rowIterator.next();
				// Obtenemos el iterator que permite recorres todas las celdas de una fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell celda;
				columnas = new ArrayList<String>();

				while (cellIterator.hasNext()){

					celda = cellIterator.next();
					// Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...
					switch(celda.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							if( HSSFDateUtil.isCellDateFormatted(celda) ){
								//System.out.println(celda.getDateCellValue());
							} else{
								//System.out.println(celda.getNumericCellValue());
							}
							//System.out.println(celda.getNumericCellValue());
							cadena = "" + celda.getNumericCellValue();
							break;
						case Cell.CELL_TYPE_STRING:
							//System.out.println(celda.getStringCellValue());
							cadena = celda.getStringCellValue();
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							//System.out.println(celda.getBooleanCellValue());
							cadena = "" + celda.getBooleanCellValue();
							break;
					}
					columnas.add(cadena);
				}
				filas.add(columnas);
			}

		} catch (Exception e){
			System.err.println("Error al leer el archivo. " + e);
		} finally{
			// Se cierra el libro excel
			workbook.close();
		}
		System.out.println("ComunesXLS.leerExcel (S)");

		return filas;
	}

}//ComunesXLS