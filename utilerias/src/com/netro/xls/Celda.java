package com.netro.xls;


public class Celda{
	public Celda(){}

	public Celda(int colspan){
		this.colspan = colspan;
	}

	public Celda(String contenido){
		this.contenidoCelda = contenido;
	}	

	public Celda(String contenido,int colspan){
		this.contenidoCelda = contenido;
		this.colspan = colspan;
	}

	public Celda(String contenido,String estilo){
		this.contenidoCelda = contenido;
		this.estilo = estilo;
	}

	public Celda(String contenido,String estilo,int colspan){
		this.contenidoCelda = contenido;
		this.estilo = estilo;
		this.colspan = colspan;
	}

  /*
	public Celda(String contenido,String estilo,int colspan,int rowspan){
		this.contenidoCelda = contenido;
		this.estilo = estilo;
		this.colspan = colspan;
		this.rowspan = rowspan;		
	}*/

	public Celda(String contenido,String estilo,int colspan,int widthCelda){
		this.contenidoCelda = contenido;
		this.estilo = estilo;
		this.colspan = colspan;
		this.widthCelda = widthCelda;
	}

	public Celda(String contenido,String estilo,int colspan,String Halineacion){
		this.contenidoCelda = contenido;
		this.estilo = estilo;
		this.colspan = colspan;
		this.Halineacion = Halineacion;
	}

	public String obtenerCelda(){
		this.contenidoCelda = "    <td class=\""+this.estilo+"\""+
							  "		rowspan=\""+this.rowspan+"\""+
							  "		colspan=\""+this.colspan+"\""+
							  "		width=\""+this.widthCelda+"\""+				     		 
							  "		align=\""+this.Halineacion+"\" >"+
									this.contenidoCelda+
							  "		</td>"+
							  "\n";//Para la presentacion
		return this.contenidoCelda;		
	}  

	private int colspan = 1;
	private int rowspan = 1;
  private int widthCelda = 100;
	private String estilo = "defaultCell";
	private String Halineacion = "left";	
	private String contenidoCelda = "&nbsp;";	
}