package com.netro.xls;


public class Fila{

	public Fila(){}

	public Fila(int rowspan){
		this.rowspan = rowspan;
	}

	public Fila(int rowspan,String estilo){
		this.rowspan = rowspan;
	}

	public String ObtenerContenidoFila(){
		this.contenidoFila =
			"  <tr "+
			" rowspan=\""+this.rowspan+"\""+
			" class=\""+this.estilo+"\" > \n" +
			this.contenidoFila +
			"  </tr> \n";//Para la presentacion			
		return this.contenidoFila;
	}

	public void AgregarCelda(Celda celda){
		this.contenidoFila += celda.obtenerCelda();

	}

	private int rowspan = 1;
	private String estilo = "";
	private String contenidoFila = "";	
}