package com.netro.xls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;


public class EstiloCelda{

	private 	HashMap 			cellStyleMatrix	= null;
	private	ArrayList		horizontal 			= null;
	private	ArrayList		vertical				= null;
	private 	boolean 			borde					= false;
	private  HSSFWorkbook	workbook				= null;
	public 	int				numberOfStyles		= 0;
	
	// fuentes
	private HSSFFont fArl10;
	private HSSFFont fArl10r;
	private HSSFFont fArlB10;
	private HSSFFont fArl12;
	private HSSFFont fArlB12;
	private HSSFFont ftitulo;
	private HSSFFont fArl10t;
	
	private 	static 	short	tipoBold 	= HSSFFont.BOLDWEIGHT_BOLD;
	public 	static	short tipoNormal	= HSSFFont.BOLDWEIGHT_NORMAL;
	private 	static	short colorAutom	= HSSFFont.COLOR_NORMAL;
	private 	static 	short colorRojo	= HSSFFont.COLOR_RED;

	
	public EstiloCelda(HSSFWorkbook workbook){
		
		cellStyleMatrix = new HashMap();
		
		this.workbook = workbook;
		
		iniciaFuentes();
		
		horizontal = new ArrayList();
		horizontal.add(Integer.toString(HSSFCellStyle.ALIGN_LEFT));
		horizontal.add(Integer.toString(HSSFCellStyle.ALIGN_CENTER));
		horizontal.add(Integer.toString(HSSFCellStyle.ALIGN_RIGHT));
	
		vertical = new ArrayList();
		vertical.add(Integer.toString(HSSFCellStyle.VERTICAL_TOP));
		vertical.add(Integer.toString(HSSFCellStyle.VERTICAL_CENTER));
		vertical.add(Integer.toString(HSSFCellStyle.VERTICAL_BOTTOM));
		vertical.add(Integer.toString(HSSFCellStyle.VERTICAL_JUSTIFY));
		
		// initialize rows
		Iterator horizontalIterator = horizontal.iterator();
		while(horizontalIterator.hasNext()){
			
			HashMap row = new HashMap();
			cellStyleMatrix.put(horizontalIterator.next(),row);
			
			// initialize columns
			Iterator verticalIterator = vertical.iterator();
			while(verticalIterator.hasNext()){
				row.put(verticalIterator.next(),new HashMap());
			}
		}

	}
	
	private boolean existeElEstiloDeCelda(String clase,int horAlignment,int verAlignment){
		
		HashMap 	row = (HashMap)cellStyleMatrix.get(Integer.toString(horAlignment));
		HashMap	col = (HashMap)row.get(Integer.toString(verAlignment));
		
		if(col.get(clase) == null)
			return false;
		
		return true;
	}
	
	private void setNewCellStyle(String clase,int horAlignment,int verAlignment){
		
		HashMap 	row = (HashMap)cellStyleMatrix.get(Integer.toString(horAlignment));
		HashMap	col = (HashMap)row.get(Integer.toString(verAlignment));
		
		HSSFCellStyle cs = getCellStyle(clase);
		cs.setAlignment((short) horAlignment);
		cs.setVerticalAlignment((short)verAlignment);
		col.put(clase,cs);
	}
	
	public HSSFCellStyle getCellStyle(String clase,int horAlignment,int verAlignment){
		
		if(!existeElEstiloDeCelda(clase,horAlignment,verAlignment))
			setNewCellStyle(clase,horAlignment,verAlignment);
		
		HashMap 	row = (HashMap)cellStyleMatrix.get(Integer.toString(horAlignment));
		HashMap	col = (HashMap)row.get(Integer.toString(verAlignment));
		
		return (HSSFCellStyle) col.get(clase);
	}
	
	private HSSFCellStyle getCellStyle(String clase) {
		
		HSSFCellStyle cs = null; 
		
		try {
			cs = workbook.createCellStyle();
			if(clase.equals("encabezado")) borde = true;
			if(borde) {
				cs.setBorderBottom(	HSSFCellStyle.BORDER_THIN);
				cs.setBorderLeft(		HSSFCellStyle.BORDER_THIN);
				cs.setBorderRight(	HSSFCellStyle.BORDER_THIN);
				cs.setBorderTop(		HSSFCellStyle.BORDER_THIN);
			}					
			if("formas".equals(clase))
				cs.setFont(fArl10);
			else if("formast".equals(clase))
				cs.setFont(fArl10t);				
			else if("formasb".equals(clase))
				cs.setFont(fArlB10);
			else if("formast".equals(clase))
				cs.setFont(fArl12);
			else if("formastb".equals(clase))
				cs.setFont(fArlB12);
			else if("formasr".equals(clase))
				cs.setFont(fArl10r);				
			else if("titulo".equals(clase))
				cs.setFont(ftitulo);
			else if("celda01".equals(clase)) {			
				cs.setFont(fArlB10);				
				cs.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			}
			else if("encabezado".equals(clase)) {
				cs.setFont(fArlB10);				
				cs.setFillForegroundColor(HSSFColor.YELLOW.index);
				cs.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				borde = false;
			}
			else 
				cs.setFont(fArl10);			
			
		} catch (Exception e) {
			System.out.println("Estilo::getCellStyle(Exception)");
			e.printStackTrace();
		}
		return cs;
	}
	
	private void iniciaFuentes() {
		try {
			fArl10 	= creaFuente((short) 10, colorAutom, 				tipoNormal, 	"Arial");
			fArl10r 	= creaFuente((short) 10, HSSFColor.RED.index, 	tipoNormal, 	"Arial");
			fArlB10 	= creaFuente((short) 10, colorAutom, 				tipoBold, 		"Arial");
			fArl12 	= creaFuente((short) 12, colorAutom, 				tipoNormal, 	"Arial");
			fArlB12	= creaFuente((short) 12, colorAutom, 				tipoBold, 		"Arial");
			ftitulo 	= creaFuente((short) 12, HSSFColor.BLUE.index, 	tipoBold, 		"Arial");
			fArl10t 	= creaFuente((short) 10, HSSFColor.TEAL.index, 	tipoNormal, 	"Arial");			
		} catch (Exception e) {
			System.out.println("Estilo::iniciaFuentes(Exception)");
			e.printStackTrace();
		}
	}
	
	private HSSFFont creaFuente(short puntos, short color, short tipo, String nombre) {
		HSSFFont fuente = workbook.createFont();
		try {
			fuente.setFontHeightInPoints(puntos);
			fuente.setColor(color);
			fuente.setBoldweight(tipo);
			fuente.setFontName(nombre);
		} catch (Exception e) {
			System.out.println("Estilo::creaFuente(Exception)");
			e.printStackTrace();
		}
		return fuente;
	}
	
	public void setBorde(boolean borde) {
		this.borde = borde;
	}
	
	public boolean getBorde() {
		return borde;
	}
}
