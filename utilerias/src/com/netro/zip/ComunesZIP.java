package com.netro.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.logging.Log;


public class ComunesZIP {
	
	private static Log log = ServiceLocator.getInstance().getLog(ComunesZIP.class);
	
	public ComunesZIP() { }
	
	public static List descomprimir(InputStream inStream, String rutaFisica) {
		List<String> 			lEntradas 		= new ArrayList<>();
		File 			file 			= null;
		ZipArchiveInputStream 	zipInStream 	= null;
		ZipArchiveEntry 		zipEntry 		= null;
		byte 			arrByte[] 		= new byte[4096];
		try {
			zipInStream = new ZipArchiveInputStream(inStream);
			
			while ((zipEntry = zipInStream.getNextZipEntry()) != null) {
				
				if(zipEntry.isDirectory()) {
					continue; 
				}//if(zipEntry.isDirectory())
			
				String entry = zipEntry.getName();
				
				lEntradas.add(entry);
				
				int indice = entry.lastIndexOf("/");
				if (indice>0) {
					String directorio = entry.substring(0, indice);
					file = new File(directorio);
					if(!file.isDirectory()) {
						file.mkdirs();
					}					
				}
				
				OutputStream outstream = 
						new BufferedOutputStream(
								new FileOutputStream(rutaFisica+entry));
			
				int i = 0;
				while((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
					outstream.write(arrByte, 0, i);
				}
				
				outstream.close();
				
			}//while ((zipEntry = zipInStream.getNextEntry()) != null)
		} catch(Exception e) {
			e.printStackTrace();
		}
		return lEntradas;
	}//descomprimir
	
	public static List descomprimir(String rutaZip, String rutaFisica) {
		List lEntradas = new ArrayList();		
		try {
			InputStream inStream = new FileInputStream(rutaZip);
			lEntradas = descomprimir(inStream, rutaFisica);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return lEntradas;
	}//descomprimir
	
	
	/**
	 * Comprime cada uno de los archivos origen especificados dentro del 
	 * archivo destino especificado. Los archivos dentro del zip
	 * no tienen la ruta absoluta, sino relativa a dirBase
	 * 
	 * @param lArchivos lista de archivos
	 * @param dirBase Directorio a partir de donde se buscan los 
	 * 		archivos de la lista. ATENCION! esta ruta debe terminar en /
	 * @param nombreArchivoZip Nombre del archivo zip destino
	 * 
	 * 
	 */
	public static void comprimir(List lArchivos, 
			String dirBase, String nombreArchivoZip) throws java.io.IOException {
		System.out.println("ComunesZIP::comprimir(E)");
		if (lArchivos == null || lArchivos.size() == 0 || 
				dirBase == null || dirBase.trim().equals("") || 
				dirBase.charAt(dirBase.length() - 1 ) != '/' ||  
				nombreArchivoZip == null || nombreArchivoZip.equals("")) {
			throw new IllegalArgumentException(
					"ComunesZIP::comprimir:: Los parametros recibidos no son validos " + 
					"lFiles=" + lArchivos + " dirBase=" + dirBase + " nombreArchivoZip=" + nombreArchivoZip);
		}

//		try {
			ZipArchiveOutputStream zipOutputStream = 
								new ZipArchiveOutputStream(
									new BufferedOutputStream(
										new FileOutputStream(nombreArchivoZip)));
			for(int f=0;f<lArchivos.size();f++) {
				String nombre = dirBase+lArchivos.get(f).toString();
				File file = new File(nombre);
				boolean bFile = file.isFile();
				//System.out.print("\n("+bFile+")\t"+nombre);
				if(!bFile) {
					throw new FileNotFoundException("El archivo " + nombre + " no existe");
				} else {
					long	lastmodified 	= file.lastModified();
					String zipPath = lArchivos.get(f).toString();
					byte data[] = new byte[15360];
					BufferedInputStream bufferedInputStream = 
											new BufferedInputStream(
												new FileInputStream(file));
					ZipArchiveEntry zipEntry = new ZipArchiveEntry(zipPath);
					zipEntry.setTime(lastmodified);
					zipOutputStream.putArchiveEntry(zipEntry);
					int count;
					while( ( count = bufferedInputStream.read(data, 0, data.length ) ) != -1 ) {
						zipOutputStream.write(data, 0, count);
					}
					zipOutputStream.closeArchiveEntry();
					bufferedInputStream.close();
				}//if(bFile)
			}//for(int f=0;f<list.size();f++)
			zipOutputStream.close();
			System.out.println("ComunesZIP::comprimir(S)");
//		} catch(Exception e) {
//			System.out.println("\nCreaZip::main(Exception) "+e);
//			e.printStackTrace();
//		}
	}
	
 /**
  *  Actualiza en un nuevo archivo: <tt>outZipFile</tt>, los archivos indicados en 
  *  la lista: <tt>updatedFiles</tt> procedentes del archivo <tt>zipFile</tt>.
  *  
  *  Ejemplo de su uso:
  *  
  *      List 	lista 		= new ArrayList();
  *	
  *      HashMap updatedFile = new HashMap();
  *      updatedFile.put("NAME", "xl/worksheets/sheet1.xml");
  *      updatedFile.put("PATH", "recursos/hola.xml");
  *    
  *      lista.add(updatedFile);
  *    
  *      ComunesZIP.updateFiles("recursos/pruebas.zip", lista, "recursos/x1cH3xVdg.zip");
  *      
  *  @since Fodea 035 - 2011
  *  @author jshernandez (Jesus Salim)
  *  
  *  @throws Exception
  *  
  *  @param zipFile <tt>String</tt> con la ruta del archivo cuyos archivos componentes
  *                 seran actualizados.
  *  @param updatedFiles <tt>List</tt> con los archivos a actualizar.
  *  @param outZipFile <tt>String</tt> con la ruta del nuevo archivo zip que contendra
  *                    los archivos actualizados. 
  *    
  */
	public static void updateFiles(String zipFile, List updatedFiles, String outputZipFileName)
			throws Exception{
	  
		// Zip Input Stream
		ZipArchiveInputStream  inZip  = 	new ZipArchiveInputStream(
									new BufferedInputStream(
											new FileInputStream(zipFile)
									)
	  							);
		// New Zip File 
		ZipArchiveOutputStream outZip = 	new ZipArchiveOutputStream(
									new FileOutputStream(outputZipFileName)
								);
	  
		// Create a buffer for copying
		byte[] buffer = new byte[1024]; 
		int bytesRead;
		
		// Get File to Update
		HashMap file2Update 		= (HashMap) updatedFiles.get(0);
		String  updatedFileName	= (String) file2Update.get("NAME");
		String  updatedFilePath	= (String) file2Update.get("PATH");
	  
		for (ZipArchiveEntry in; (in = inZip.getNextZipEntry()) != null;){
 
			// Update entry 
			if (updatedFiles.size() > 0 && in.getName().equals(updatedFileName)) {
			
				// System.out.println("updating entry = <" + in.getName() + ">" );
				
				// Read Updated File
				File  			newFile 	= new File(updatedFilePath);
				FileInputStream inputStream = new FileInputStream(newFile);
					
				// Make a ZipEntry for the updated file
				ZipArchiveEntry 		entry 		= new ZipArchiveEntry(updatedFileName);
					
				// Store entry
				outZip.putArchiveEntry(entry); 
				while ((bytesRead = inputStream.read(buffer)) != -1){
					outZip.write(buffer, 0, bytesRead);
					outZip.flush();
				}
				inputStream.close(); 
				outZip.closeArchiveEntry();
			
				// Get the next file to update
				updatedFiles.remove(0);
				if(updatedFiles.size() > 0){
					file2Update 	= (HashMap) updatedFiles.get(0);
					updatedFileName	= (String) file2Update.get("NAME");
					updatedFilePath	= (String) file2Update.get("PATH");
				}
				// Copy Entry	
			} else {
			
				// System.out.println("copying entry = <" + in.getName() + ">" );
				
				ZipArchiveEntry 		entry 		= new ZipArchiveEntry(in.getName());
				outZip.putArchiveEntry(entry);
				while ((bytesRead = inZip.read(buffer)) != -1){
					outZip.write(buffer, 0, bytesRead);
					outZip.flush();
				}
				outZip.closeArchiveEntry();
	
			}
		}
		
		outZip.finish();
		outZip.close();
		inZip.close();
	}

	/**
	 * Este m�todo est� pensado para descomprimir un unico archivo dentro de un 
	 * archivo zip, al cual le asignara un nombre aleatorio el cual sera
	 * regresado por el metodo.
	 * Es para empleo de cargas masivas, en donde se desee procesar un archivo
	 * de carga masiva comprimido.
	 * @param inStream Stream del archivo comprimido
	 * @param rutaFisica Ruta para descomprimir el archivo
	 * @return String ruta y nombre del archivo descomprimido
	 */
	public static String descomprimirArchivoSimple(InputStream inStream, String rutaFisica) {
		String archivo = "";
		ZipArchiveInputStream zipInStream = null;
		ZipArchiveEntry zipEntry = null;
		byte arrByte[] = new byte[4096];
		try {
			zipInStream = new ZipArchiveInputStream(inStream);
			
			while ((zipEntry = zipInStream.getNextZipEntry()) != null) {
				
				if(zipEntry.isDirectory()) {
					continue; 
				}//if(zipEntry.isDirectory())
			
				String entry = zipEntry.getName();
				
				int indice = entry.lastIndexOf(".");
				String extension = "";
				if (indice > 0) {
					extension = entry.substring(indice);
				}
				
				archivo = rutaFisica+Comunes.cadenaAleatoria(16)+extension;
				OutputStream outstream = 
						new BufferedOutputStream(
								new FileOutputStream(archivo));
			
				int i = 0;
				while((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
					outstream.write(arrByte, 0, i);
				}
				
				outstream.close();
				break;	//Sale despu�s del primer archivo que encuentre en el zip
			}//while ((zipEntry = zipInStream.getNextEntry()) != null)
			return archivo;
		} catch(Throwable e) {
			throw new AppException("Error al descomprimir el archivo ZIP", e);
		}
	}//descomprimir

	public static boolean contieneDirectorio( String archivoZIP )
		throws AppException {
			
		log.info("contieneDirectorio(E)");
				
		InputStream 		inStream 				= null;
		ZipArchiveInputStream 	zipInStream 			= null;
		ZipArchiveEntry 			zipEntry 				= null;

		boolean 				directorioEncontrado = false;
		
		try {
				
			inStream 		= new FileInputStream(archivoZIP);
			zipInStream 	= new ZipArchiveInputStream(inStream);
				
			while ( (zipEntry = zipInStream.getNextZipEntry()) != null ) {
				
				if(zipEntry.isDirectory()) {
					directorioEncontrado = true;
					break;
				} else if( zipEntry.getName() != null && zipEntry.getName().indexOf("/") != -1 ){
					directorioEncontrado = true;
					break;
				}

			}
			
		} catch(Exception e){
				
			log.error("contieneDirectorio(Exception)");
			log.error("contieneDirectorio.archivoZIP         = <" + archivoZIP         + ">"); 
				
			e.printStackTrace();
				
			throw new AppException("No se pudo revisar si el archivo tiene directorios en su estructura: " + e.getMessage() );
				
		} finally {
				
			if( zipInStream != null ){ try { zipInStream.close(); } catch(Exception e){} }
				
			log.info("contieneDirectorio(S)");
				
		}
		
		return directorioEncontrado;
		
	}
	
	/*
	 * Nota: En el conteo no se consideran los directorios incluidos
	 */
	public static boolean contieneMasDeUnArchivo( String archivoZIP )
		throws AppException {
			
		log.info("contieneMasDeUnArchivo(E)");
				
		InputStream 		inStream 				= null;
		ZipArchiveInputStream 	zipInStream 			= null;
		ZipArchiveEntry 			zipEntry 				= null;

		boolean 				hayMasDeUnArchivo 	= false;
		
		try {
				
			inStream 		= new FileInputStream(archivoZIP);
			zipInStream 	= new ZipArchiveInputStream(inStream);
				
			int	cuentaArchivos = 0;
			while ( (zipEntry = zipInStream.getNextZipEntry()) != null ) {
				if(zipEntry.isDirectory()) {
					continue;
				}
				cuentaArchivos++;
				if(cuentaArchivos>1){
					hayMasDeUnArchivo = true;
					break;
				}
			}
			
		} catch(Exception e){
				
			log.error("contieneMasDeUnArchivo(Exception)");
			log.error("contieneMasDeUnArchivo.archivoZIP         = <" + archivoZIP         + ">"); 
				
			e.printStackTrace();
				
			throw new AppException("No se pudo verificar el n�mero de archivos comprimidos: " + e.getMessage() );
				
		} finally {
				
			if( zipInStream != null ){ try { zipInStream.close(); } catch(Exception e){} }
				
			log.info("contieneMasDeUnArchivo(S)");
				
		}
		
		return hayMasDeUnArchivo;
		
	}
	
			
}//ComunesZIP