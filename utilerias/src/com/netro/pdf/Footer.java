package com.netro.pdf;

import com.itextpdf.text.Phrase;

public class Footer {
	
	private Phrase    before;
	private boolean   numbered;
	private int       border;
	private int			alignment;
	
	public Footer(Phrase before, boolean numbered){ 
		this.before 	= before;
		this.numbered 	= numbered;
	}

	public Phrase getBefore(){
		return this.before;
	}
	
	public boolean  isNumbered() {
		return this.numbered;
	}
	
	public void setBorder(int value){
		this.border = value;
	}
	
	public int getBorder(){
		return this.border;
	}

	public void setAlignment(int alignment){
		this.alignment = alignment;
	}
	
	protected int getAlignment(){
		return this.alignment;
	}
	
}
