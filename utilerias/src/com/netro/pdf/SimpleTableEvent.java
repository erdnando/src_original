package com.netro.pdf;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableEvent;

public class SimpleTableEvent extends Rectangle implements PdfPTableEvent  {

	/**
	 * A RectangleCell is always constructed without any dimensions.
	 * Dimensions are defined after creation.
	 */
	public SimpleTableEvent() {
		super(0f, 0f, 0f, 0f);
		setBorder(BOX);
		setBorderWidth(2f);
	}
	
	/** the spacing of the Cells. */
	private float cellspacing;
	
	/**
	 * @return Returns the cellspacing.
	 */
	public float getCellspacing() {
		return cellspacing;
	}
	
	/**
	 * @param cellspacing The cellspacing to set.
	 */
	public void setCellspacing(float cellspacing) {
		this.cellspacing = cellspacing;
	}
	
	/**
	 * @param rectangle
	 * @param spacing
	 * @return a rectangle
	 */
	public static SimpleTableEvent getDimensionlessInstance(Rectangle rectangle, float spacing) {
		SimpleTableEvent event = new SimpleTableEvent();
		event.cloneNonPositionParameters(rectangle);
		event.setCellspacing(spacing);
		return event;
	}
	
	@Override
	public void tableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases) {
		float[] width = widths[0];
		Rectangle rect = new Rectangle(width[0], heights[heights.length - 1], width[width.length - 1], heights[0]);
		rect.cloneNonPositionParameters(this);
        int bd = rect.getBorder();
        rect.setBorder(Rectangle.NO_BORDER);
		canvases[PdfPTable.BACKGROUNDCANVAS].rectangle(rect);
        rect.setBorder(bd);
		rect.setBackgroundColor(null);
		canvases[PdfPTable.LINECANVAS].rectangle(rect);
	}
	
}
