package com.netro.pdf;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SimpleCellEvent extends Rectangle implements PdfPCellEvent {
	
	/**
	 * A CellAttributes object is always constructed without any dimensions.
	 * Dimensions are defined after creation.
	 * @param row only true if the CellAttributes object represents a row.
	 */
	public SimpleCellEvent() {
		super(0f, 0f, 0f, 0f);
		setBorder(BOX);
	}
	
	/** an extra spacing variable */
	private float spacing_left = Float.NaN;
	/** an extra spacing variable */
	private float spacing_right = Float.NaN;
	/** an extra spacing variable */
	private float spacing_top = Float.NaN;
	/** an extra spacing variable */
	private float spacing_bottom = Float.NaN;
	
	/**
	 * @param rectangle
	 * @param spacing
	 * @return a rectangle
	 */
	public static SimpleCellEvent getDimensionlessInstance(Rectangle rectangle, float spacing) {
		SimpleCellEvent event = new SimpleCellEvent();
		event.cloneNonPositionParameters(rectangle);
		event.setSpacing(spacing * 2f);
		return event;
	}
	
	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
		
		// System.out.println("cell.getWidth="+cell.getWidth());
		// IMPORTANTE: La l�nea anterior solo podr� estar descomentada para desarrollo local.
		// Nota: No quitar la l�nea anterior ya que sirve para determinar el ancho que itext les
		// asigna a las celdas, lo cual se puede usar para determinar la longitud m�xima que una
		// imagen dentro de una celda puede tomar.
		
		float sp_left = spacing_left;
		if (Float.isNaN(sp_left)) sp_left = 0f;
		float sp_right = spacing_right;
		if (Float.isNaN(sp_right)) sp_right = 0f;
		float sp_top = spacing_top;
		if (Float.isNaN(sp_top)) sp_top = 0f;
		float sp_bottom = spacing_bottom;
		if (Float.isNaN(sp_bottom)) sp_bottom = 0f;
		Rectangle rect = new Rectangle(position.getLeft(sp_left), position.getBottom(sp_bottom), position.getRight(sp_right), position.getTop(sp_top));
		rect.cloneNonPositionParameters(this);
		canvases[PdfPTable.BACKGROUNDCANVAS].rectangle(rect);
		rect.setBackgroundColor(null);
		canvases[PdfPTable.LINECANVAS].rectangle(rect);
	}
	
	/**
	 * @param spacing The spacing to set.
	 */
	public void setSpacing(float spacing) {
		this.spacing_left = spacing;
		this.spacing_right = spacing;
		this.spacing_top = spacing;
		this.spacing_bottom = spacing;
	}
	
}
