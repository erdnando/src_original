package com.netro.pdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.SimpleBookmark;

import com.itextpdf.text.BaseColor;

import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPRow;

import com.itextpdf.text.pdf.PdfSmartCopy;

import java.io.FileOutputStream;
import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.TabSettings;


public class ComunesPDF extends PdfPageEventHelper {

	private static Log log = ServiceLocator.getInstance().getLog(ComunesPDF.class);

    public PdfGState gstate;

	private PdfWriter writer;
	private Document	document;
	private Paragraph	paragraph;
	private Phrase		phrase;
	private Chunk		chunk;
	private Image		image;
	private Footer		footer;
	public  PdfPTable linea;

	private PdfTemplate template;
	private PdfPTable		table;
	private PdfPTable 	head;
	private PdfPTable	ltable;
	private PdfPCell	cell;
	private PdfPCell	lcell;
	private int 		iLeading 		= 11;
	private float 		padding 		=  2;
	private int 		fragmentsize 	= 100;
	private boolean		bCancelado 		= false;
	private boolean		bPiePagina 		= false; 			//Numeraci�n al pie de pagina
	private boolean		bEncabezado		= false;
	private int 			countCells 		= 0;//optimizacion pdf muy grandes
	private int 			countLCells 	= 0;//optimizacion pdf muy grandes
	
	private String 		strFecha;
	private String 		strTitulo;
	private String 		strDatosUsuario;
	private String 		strLogo;
	private String 		strBanner;

	public static final int HOJA_VERTICAL     = 1;
	public static final int HOJA_HORIZONTAL   = 2;
	
	private int 			orientacionPagina 	= HOJA_VERTICAL;
		
	private boolean 		bEncabezadoPersonalizado = false;//Encabezado personalizado

	private List			encabezadoEdoCuenta = new ArrayList();

	private	BaseColor		color5 = new BaseColor(255, 255, 255);
	private	BaseColor		color4 = new BaseColor(204, 204, 204);
	private	BaseColor		color3 = new BaseColor(192, 192, 192);
	private	BaseColor		color2 = new BaseColor(153, 153, 153);
	private	BaseColor		color1 = new BaseColor(0, 0, 0);

	private Font		font1 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL);
	private Font		font2 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	private Font		font3 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL);
	private Font		font4 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	private Font		font5 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL, color5);
	private Font		font6 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD, color5);
	private Font		font7 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL);
	private Font		font8 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	private Font		font9 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	private Font		font10 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
	private Font    	font11 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.UNDERLINE);
	private Font		font12 = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.NORMAL);
	private Font		font13 = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD);
	private Font		font14 = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.NORMAL, color5);
	private Font		font15 = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD,   color5);
	private Font		font16 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL, color5);
	private Font		font17 = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD,   color5);
	private Font		font18 = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL); // Estilo Leyenda Contratos CESION

	private final float FOOTER_HEIGHT = 7 + padding * 2; // 7 = MAX FONT SIZE
	
	public static int	LEFT 			= Element.ALIGN_LEFT;
	public static int	CENTER 		= Element.ALIGN_CENTER;
	public static int	RIGHT 		= Element.ALIGN_RIGHT;
	public static int	JUSTIFIED 	= Element.ALIGN_JUSTIFIED;

	public ComunesPDF() {}//ComunesPDF()

	public ComunesPDF(int tipo, String ruta) {
		creaPDF(tipo, ruta, "Pagina ", false, false, false);
	}//ComunesPDF(int tipo, String ruta)

	public ComunesPDF(int tipo, String ruta, String foot) {
		creaPDF(tipo, ruta, foot, false, false, false);
	}//ComunesPDF(int tipo, String ruta)

	public ComunesPDF(int tipo, String ruta, String foot, boolean bCancelado) {
		creaPDF(tipo, ruta, foot, bCancelado, false, false);
	}//ComunesPDF(int tipo, String ruta)

	public ComunesPDF(int tipo, String ruta, String foot, boolean bCancelado, boolean bEncabezado) {
		creaPDF(tipo, ruta, foot, bCancelado, bEncabezado, false);
	}//ComunesPDF(int tipo, String ruta)

	public ComunesPDF(int tipo, String ruta, String foot, boolean bCancelado, boolean bEncabezado, boolean bPiePagina) {
		creaPDF(tipo, ruta, foot, bCancelado, bEncabezado, bPiePagina);
	}//ComunesPDF(int tipo, String ruta)

	public ComunesPDF(int tipo, String ruta, String foot, boolean bCancelado, boolean bEncabezado, boolean bPiePagina, boolean bEncabezadoPersonalizado) {
		creaPDF(tipo, ruta, foot, false, false, false, true);
	}//ComunesPDF(int tipo, String ruta)

	public void creaPDF(int tipo, String ruta, String foot, boolean bCancelado, boolean bEncabezado, boolean bPiePagina) {
		try {
			float fTopMarg = 0;

			this.bCancelado 	= bCancelado;
			this.bEncabezado 	= bEncabezado;
			this.bPiePagina 	= bPiePagina;

			if(tipo==1) {			//Tipo 1: Tama�o Carta Vertical
				if(this.bEncabezado) {
					fTopMarg = 135;
				}
			   this.orientacionPagina = HOJA_VERTICAL;
				this.document = new Document(PageSize.A4, 36, 36, 10 + fTopMarg, 10 + FOOTER_HEIGHT );//125
			}  else if(tipo==2) {	//Tipo 2: Tama�o Carta Horizotal
				log.debug("creaPDF::Es tipo 2");
				if(this.bEncabezado) {
					fTopMarg = 135;
				}
			   this.orientacionPagina = HOJA_HORIZONTAL;
				this.document = new Document(PageSize.A4.rotate(), 36, 36, 10 + fTopMarg, 10 + FOOTER_HEIGHT);//165
			}

			writer = PdfWriter.getInstance(this.document, new FileOutputStream(ruta));
			writer.setPageEvent(this);
			if(!this.bPiePagina)
				setFooter(foot);
 			document.open();
			document.newPage();
		} catch (Exception e) {
			log.error("creaPDF(Exception)", e);
		}
	}//ComunesPDF(int tipo, String ruta)

	public void creaPDF(int tipo, String ruta, String foot, boolean bCancelado, boolean bEncabezado, boolean bPiePagina, boolean bEncabezadoPersonalizado) {
		try {
			float fTopMarg = 0;

			this.bCancelado 	= bCancelado;
			this.bEncabezado 	= false;
			this.bPiePagina 	= bPiePagina;
			this.bEncabezadoPersonalizado 		= bEncabezadoPersonalizado;

			if(tipo==1) {			//Tipo 1: Tama�o Carta Vertical
				if(this.bEncabezadoPersonalizado) {
					fTopMarg = 135;
				}
			   this.orientacionPagina = HOJA_VERTICAL;
				this.document = new Document(PageSize.A4, 36, 36, 10 + fTopMarg, 10 + FOOTER_HEIGHT );//125
			}  else if(tipo==2) {	//Tipo 2: Tama�o Carta Horizotal
				if(this.bEncabezadoPersonalizado) {
					fTopMarg = 135;
				}
			   this.orientacionPagina = HOJA_HORIZONTAL;
				this.document = new Document(PageSize.A4.rotate(), 36, 36, 10 + fTopMarg, 10 + FOOTER_HEIGHT );//165
			}

			writer = PdfWriter.getInstance(this.document, new FileOutputStream(ruta));
			writer.setPageEvent(this);

			setFooter("Fecha de emisi�n: " + (new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss")).format(new java.util.Date()), false, "CENTER");
 			document.open();
			document.newPage();

		} catch (Exception e) {
			log.error("creaArchivo(Exception)", e);
		}
	}//ComunesPDF(int tipo, String ruta)

	public void setEncabezado(
					String 	strPais,				String iNoNafinElectronico,
					String 	sesExterno,				String strNombre,
					String 	strNombreUsuario,		String strLogo,
					String 	strDirectorio, 			String strTitulo) {
		try {
			this.bEncabezado = true;
			this.strTitulo = strTitulo;

			strPais 				= strPais==null?"":strPais;
			iNoNafinElectronico 	= iNoNafinElectronico==null?"":iNoNafinElectronico;
			sesExterno 				= sesExterno==null?"":sesExterno;
			strNombre 				= strNombre==null?"":strNombre;
			strNombreUsuario 		= strNombreUsuario==null?"":strNombreUsuario;
			strLogo 				= strLogo==null?"":strLogo;

			this.strDatosUsuario =
				"\n"+ (("MEXICO".equals(strPais))?"":strPais)+
				"\n"+ iNoNafinElectronico+
				"\n"+ (("S".equals(sesExterno))?"":strNombre)+
				"\n"+ strNombreUsuario;

			String rutaLogo =  "/00archivos/15cadenas/15archcadenas/logos/";	
			String rutaBaners = "/00archivos/15cadenas/15archcadenas/banner/ba_"+strLogo;
			String prefigo = strLogo.substring(0,3);
			
			if(prefigo.equals("IF_")) {
				 strLogo = strLogo.substring(3,strLogo.length());				
				 rutaLogo  = "/00archivos/if/logos/";
				rutaBaners = "/00archivos/15cadenas/15archcadenas/banner/ba_nafin.gif";
			}			
			
			this.strLogo =	strDirectorio + rutaLogo+ 	strLogo;

			this.strBanner = 	strDirectorio + rutaBaners;

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			this.strFecha =
				"M�xico, D.F. a " +
				diaActual +
				" de " +
				mesActual +
				" del " +
				anioActual +
				" ----------------------------- " +
				horaActual;

		} catch(Exception e) {
			log.error("setEncabezado(Exception) ", e);
		}
	}

	public void setStrTitulo(String strTitulo) {
		this.strTitulo = strTitulo;
	}

	public void setFooter(String foot, boolean bNumero) {
		try {
			Footer footer = new Footer(new Phrase(foot ,getFont("formas")), bNumero);
			footer.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(RIGHT);
			this.footer = footer;			
		} catch(Exception e) {
			log.error("setFooter(Exception)", e);
		}
	}

	public void setFooter(String foot) {
		setFooter(foot, true);
	}

	//Pie de Pagina alineado
	public void setFooter(String foot, boolean bNumero, String align) {
		try {
			Footer footer = new Footer(new Phrase(foot ,getFont("formas")), bNumero);
			footer.setBorder(Rectangle.NO_BORDER);
			if(align.equals("LEFT"))
				footer.setAlignment(LEFT);
			else if(align.equals("CENTER"))
				footer.setAlignment(CENTER);
			else if(align.equals("RIGHT"))
				footer.setAlignment(RIGHT);
			this.footer = footer;
		} catch(Exception e) {
			log.error("setFooter(Exception)", e);
		}
	}


    public void onOpenDocument(PdfWriter writer, Document document) {
		try {
            template = writer.getDirectContent().createTemplate(100, 100);
            template.setBoundingBox(new Rectangle(-20, -20, 100, 100));
        } catch(Exception e) {
					log.error("onOpenDocument()", e);
        }
    }
/*
 	public void onStartPage(PdfWriter writer, Document document) {
 		try {
			document.setMargins(document.left(), document.left(), document.top()+50, document.bottom());
 		} catch(Exception e) {
			System.out.println("ComunesPDF::onStartPage()");
			e.printStackTrace();
 		}
    }
*/

	public void onEndPage(PdfWriter writer, Document document)	{
		try {
			
		   if( this.footer != null ){
		      
		   	// Determinar la phrase que se mostrar� en el pie de p�gina
		      Phrase before = null;
		      if(this.footer.isNumbered()){
		         before = new Phrase(this.footer.getBefore());
		         before.add(String.valueOf(writer.getPageNumber()));
		      } else {
		         before = this.footer.getBefore();
		      }

		      // Determinar el punto de referencia que se usar� para mostrar el pie de p�gina
		      float x0 = 0;
            float y0 = 0;
		      if(        this.footer.getAlignment() == CENTER ) {
		      	x0 = (document.left() + document.right()) / 2.0f;
            	y0 = document.bottom() - FOOTER_HEIGHT;
		      } else if( this.footer.getAlignment() == RIGHT  ) {
		      	x0 = document.right();
            	y0 = document.bottom() - FOOTER_HEIGHT;
            // this.footer.getAlignment() == LEFT  y para cualquier otra alineaci�n.
		      } else {
		      	x0 = document.left();
            	y0 = document.bottom() - FOOTER_HEIGHT;
		      }
		      
		      ColumnText.showTextAligned(
		         writer.getDirectContent(),
		         this.footer.getAlignment(),
		         before,
		         x0,
		         y0, 
		         0
		      );
		      
		   }
		  
        	if (this.bEncabezado) {
		        PdfContentByte cb = writer.getDirectContent();
	        	cb.saveState();

	            Rectangle page = document.getPageSize();
	            PdfPTable head = new PdfPTable(3);
				int width[] = {1, 1, 2};
				head.setWidths(width);
	            PdfPCell hcell;

				String prefigo = strLogo.substring(0,3);			
				if(prefigo.equals("IF_")) {
					 strLogo = strLogo.substring(3,strLogo.length());									
				}	
				
				image = Image.getInstance(strLogo);
				image.setAlignment(ComunesPDF.CENTER);
				image.scalePercent(100);
				hcell =  new PdfPCell(image);
				hcell.setBorder(0);
				hcell.setHorizontalAlignment(ComunesPDF.CENTER);
				head.addCell(hcell);


				hcell =  new PdfPCell(new Phrase(strDatosUsuario ,getFont("formas")));
				hcell.setBorder(0);
				head.addCell(hcell);

				image = Image.getInstance(strBanner);
				image.setAlignment(ComunesPDF.CENTER);
				image.scalePercent(100);
				hcell =  new PdfPCell(image);
				hcell.setBorder(0);
				hcell.setHorizontalAlignment(ComunesPDF.CENTER);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase(this.strFecha ,getFont("formasB")));
				hcell.setHorizontalAlignment(ComunesPDF.RIGHT);
				hcell.setBorder(0);
				hcell.setColspan(3);
				head.addCell(hcell);

				if(!"".equals(this.strTitulo)) {
					hcell =  new PdfPCell(new Phrase(this.strTitulo ,getFont("formasB")));
					hcell.setHorizontalAlignment(ComunesPDF.CENTER);
					hcell.setBorder(0);
					hcell.setColspan(3);
					head.addCell(hcell);
				}

		        head.setTotalWidth(document.right() - document.left());
		        head.writeSelectedRows(0, -1, document.left(), document.getPageSize().getHeight() - 50, cb);
		        cb.restoreState();
			}

			if(this.bPiePagina) {
		        PdfContentByte cb = writer.getDirectContent();
	        	cb.saveState();
		        String text = "Pagina " + writer.getPageNumber() + " de ";
		        BaseFont helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
		        float textSize = helv.getWidthPoint(text, 9);
		        float textBase = document.bottom() - 20;
	            float adjust = helv.getWidthPoint("0", 9);
		        cb.beginText();
		        cb.setFontAndSize(helv, 9);
	            cb.setTextMatrix(document.right() - textSize - adjust, textBase);
	            cb.showText(text);
	            cb.endText();
	            cb.addTemplate(template, document.right() - adjust, textBase);
		        cb.restoreState();
			}

			if(this.bEncabezadoPersonalizado)
			{
				PdfContentByte cb = writer.getDirectContent();
	        	cb.saveState();
		      String text = "Hoja " + writer.getPageNumber() + " de ";
		      BaseFont helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
		      float textSize = helv.getWidthPoint(text, 9);
		      float textBase = document.top() + 120;
	         float adjust = helv.getWidthPoint("0", 9);
		      cb.beginText();
		      cb.setFontAndSize(helv, 9);
	         cb.setTextMatrix(document.right() - textSize - adjust, textBase);
	         cb.showText(text);
	         cb.endText();
	         cb.addTemplate(template, document.right() - adjust, textBase);
		      cb.saveState();

				Rectangle page = document.getPageSize();
	            PdfPTable head = new PdfPTable(18);
				float width[] = new float[18];
				for(int j=0;j<18;j++) {
					width[j] = 1;
				}//for
				head.setWidths(width);
	            PdfPCell hcell;

				image = Image.getInstance(strLogo);
				image.setAlignment(ComunesPDF.CENTER);
				image.scalePercent(80);
				hcell =  new PdfPCell(image);
				hcell.setColspan(6);
				hcell.setBorder(0);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);


				hcell =  new PdfPCell(new Phrase(strDatosUsuario ,getFont("formas")));
				hcell.setBorder(0);
				hcell.setColspan(6);
				hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("" ,getFont("formas")));
				hcell.setBorder(0);
				hcell.setColspan(6);
				hcell.setHorizontalAlignment(ComunesPDF.RIGHT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)this.encabezadoEdoCuenta.get(0) ,getFont("formasB")));
				hcell.setBorder(0);
				hcell.setColspan(18);
				hcell.setHorizontalAlignment(ComunesPDF.CENTER);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("" ,getFont("formas")));
				hcell.setBorder(0);
				hcell.setColspan(18);
				hcell.setHorizontalAlignment(ComunesPDF.CENTER);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)this.encabezadoEdoCuenta.get(1) ,getFont("formas")));
				hcell.setBorder(0);
				hcell.setColspan(18);
				hcell.setHorizontalAlignment(ComunesPDF.CENTER);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("MONEDA: " ,getFont("formasrepB")));
				hcell.setBorder(PdfPCell.TOP);
				hcell.setColspan(2);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)(this.encabezadoEdoCuenta.get(2)==null?" ":this.encabezadoEdoCuenta.get(2)) ,getFont("formasmen")));
				hcell.setBorder(PdfPCell.TOP);
				hcell.setColspan(8);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("N�mero de Cliente: " ,getFont("formasrepB")));
				hcell.setBorder(PdfPCell.TOP);
				hcell.setColspan(6);
				hcell.setHorizontalAlignment(ComunesPDF.RIGHT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)(this.encabezadoEdoCuenta.get(3)==null?" ":this.encabezadoEdoCuenta.get(3)) ,getFont("formasmen")));
				hcell.setBorder(PdfPCell.TOP);
				hcell.setColspan(2);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("CLIENTE: " ,getFont("formasrepB")));
				hcell.setBorder(0);
				hcell.setColspan(2);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)(this.encabezadoEdoCuenta.get(4)==null?" ":this.encabezadoEdoCuenta.get(4)) ,getFont("formasmen")));
				hcell.setBorder(0);
				hcell.setColspan(8);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("Registro Federal de Causantes: " ,getFont("formasrepB")));
				hcell.setBorder(0);
				hcell.setColspan(6);
				hcell.setHorizontalAlignment(ComunesPDF.RIGHT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)(this.encabezadoEdoCuenta.get(5)==null?" ":this.encabezadoEdoCuenta.get(5)) ,getFont("formasmen")));
				hcell.setBorder(0);
				hcell.setColspan(2);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase("DOMICILIO: " ,getFont("formasrepB")));
				hcell.setBorder(PdfPCell.BOTTOM);
				hcell.setColspan(2);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

				hcell =  new PdfPCell(new Phrase((String)(this.encabezadoEdoCuenta.get(6)==null?" ":this.encabezadoEdoCuenta.get(6)) ,getFont("formasmen")));
				hcell.setBorder(PdfPCell.BOTTOM);
				hcell.setColspan(16);
				hcell.setHorizontalAlignment(ComunesPDF.LEFT);
				head.addCell(hcell);

		      head.setTotalWidth(document.right() - document.left());
		      head.writeSelectedRows(0, -1, document.left(), document.getPageSize().getHeight() - 50, cb);
				
			   cb.restoreState();
			   cb.restoreState();
			}

			if(this.bCancelado) {
				addCancelado(writer);
			}
			
		} catch (Exception e) {
			log.error("onEndPage(Exception)", e);
		}
	}

	public void onCloseDocument(PdfWriter writer, Document document) {
		try {
			if(this.bPiePagina) {
				String text = "" + (writer.getPageNumber() - 1);
				BaseFont helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
		        float textSize = helv.getWidthPoint(text, 9);
		        float textBase = document.bottom() - 20;
	            float adjust = helv.getWidthPoint("0", 9);
				template.beginText();
				template.setFontAndSize(helv, 9);
				template.setTextMatrix(0, 0);
				template.showText(text);
				template.endText();
			}
			if(this.bEncabezadoPersonalizado) {
				String text = "" + (writer.getPageNumber() - 1);
				BaseFont helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
		        float textSize = helv.getWidthPoint(text, 9);
		        float textBase = document.bottom() - 20;
	            float adjust = helv.getWidthPoint("0", 9);
				template.beginText();
				template.setFontAndSize(helv, 9);
				template.setTextMatrix(0, 0);
				template.showText(text);
				template.endText();
			}
		} catch(Exception e) {
			log.error("onCloseDocument(Exception)", e);
		}
	}

	public void setLeading(int iLeading) {
		this.iLeading = iLeading;
	}//setLeading(int iLeading)

	public void setPadding(float padding) {
		this.padding = padding;
	}//setPadding(float padding)

	public void addText(String content, String clase, int align) {
		try {
			chunk = new Chunk(content);
			chunk.setFont(getFont(clase));
			chunk.setBackground(getColor(clase));
			paragraph = new Paragraph(getFont(clase).getCalculatedLeading((float)1.2), chunk);
			paragraph.setAlignment(align);
			paragraph.setSpacingBefore(padding);
                        paragraph.add(Chunk.TABBING);
			paragraph.setSpacingAfter(padding);
			document.add(paragraph);
		} catch (Exception e) {
			log.error("addText(ALIGN - Exception)", e);
		}
	}//addText(String content, String clase, int align)

	public void addTextWTabs(String content) {
	        try {
                    content = content.replaceAll("(\n)+", "\n");
                    String[] splitted = content.split("(\n)+");
                    for (String l : splitted){
                        if(!l.startsWith("\n")){
                            if (l.indexOf("\t") == -1){
                                addText(l,"contrato", JUSTIFIED);
                            }
                            else{
                                float anchos[] = {220.0f, 500.0f};
                                String[] texto = l.split("(\t)+");
                                if(texto.length == 2){
                                    setLTableTabs(2, 100, anchos);
                                    setLCellTab(texto[0]);
                                    setLCellTab(texto[1]);
                                    addLTable();                                    
                                }
                            }
                        }
                    }
	        } catch (Exception e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
	        }
	}//addText(String content, String clase, int align)

	//Texto subrayado
	public void addText(String content, String clase, int align, int subrayado) {
		try {
			chunk = new Chunk(content);
			Font fuente = getFont(clase);
			fuente.setStyle("underline");
			chunk.setFont(fuente);
			chunk.setBackground(getColor(clase));
			paragraph = new Paragraph(getFont(clase).getCalculatedLeading((float)1.2), chunk);
			paragraph.setAlignment(align);
			paragraph.setSpacingBefore(padding);
			paragraph.setSpacingAfter(padding);
			document.add(paragraph);
		} catch (Exception e) {
			log.error("addText(ALIGN - Exception)", e);
		}
	}//

	public void addText(String content, String clase) {
		try {
			addText(content, clase, LEFT);
		} catch (Exception e) {
			log.error("addText(Exception)", e);
		}
	}//addText(String content)

	public void addText(String content) {
		try {
			addText(content, "formas");
		} catch (Exception e) {
			log.error("addText(Exception)", e);
		}
	}//addText(String content)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void addTable() {
		try {
			
			// Se "copia" funcionalidad de la versi�n anterior que permit�a
			// agregar celdas faltantes por default.
		   table.completeRow();
			
			table.setComplete(true);//optimizacion pdf muy grandes
			
			// Agregar tabla al documento
			document.add(table);
			
		} catch (Exception e) {
			log.error("addTable(Exception)", e);
		}
	}//addTable()

	public void addLTable() {
		try {
			ltable.setComplete(true);//optimizacion pdf muy grandes
			document.add(ltable);
		} catch (Exception e) {
			log.error("addLTable(Exception)", e);
		}
	}//addTable()

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(java.util.List titulos, String clase, int width, float widths[]) {
		int i = 0;
		try {
			countCells = 0;//optimizacion pdf muy grandes
			Rectangle rectangle = new Rectangle(0f,0f);
			rectangle.setBorderWidth(1.0f);
			rectangle.setBorder(Rectangle.NO_BORDER);

			table = new PdfPTable(titulos.size());
			table.setComplete(false);//optimizacion pdf muy grandes
			table.setWidths(widths);
			table.setTableEvent(SimpleTableEvent.getDimensionlessInstance(rectangle, 0.0f));
			table.setSplitLate(true);
			table.setKeepTogether(false);
			//table.setSpacingBefore(Float.NaN);
			table.setHorizontalAlignment(CENTER);
			table.setWidthPercentage(width);

		   // Configuracion por default que traeran las celdas faltantes
		   // que se agreguen en caso hagan falta para completar la l�nea.
		   table.getDefaultCell().cloneNonPositionParameters(rectangle);
			
			for(i=0;i<titulos.size();i++) { // TITULOS
				setCell((String)titulos.get(i), clase, CENTER);
			}//for
			
			// Repetir los headers de la tabla si esta abarca m�s de una hoja.
			ArrayList<PdfPRow> rows = table.getRows(); 
			int numHeaderRows = rows != null?rows.size():0;
			table.setHeaderRows(numHeaderRows);
			
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}//setTable(String titulos[], String clase, int width)
	
	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */	
	public void setTable(java.util.List titulos, String clase, int width) {
		try {
			float widths[] = new float[titulos.size()];
			for(int i=0;i<titulos.size();i++) {
				widths[i] = 1;
			}//for
			setTable(titulos, clase, width, widths);
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}//setTable(String titulos[], String clase, int width)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(java.util.List titulos, Vector renglones) {
		int i = 0; int j = 0;
		Vector columnas;
		try{
			setTable(titulos,"celda01",100);//TITULOS
			for(i=0;i<renglones.size();i++) { // CONTENIDO
				columnas = (Vector)renglones.get(i);
				for(j=0;j<columnas.size();j++) {
					String clase = "formas";
					if(i%5==4)
						clase = "celda02";
					setCell((String)columnas.get(j), clase);
				}//for j
			}//for	i
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(int numCols, int width, float widths[]) {
		try{
			countCells = 0;//optimizacion pdf muy grandes
			Rectangle rectangle = new Rectangle(0f,0f);
			rectangle.setBorderWidth(1.0f);
			rectangle.setBorder(Rectangle.NO_BORDER);
				
			table = new PdfPTable(numCols);
			table.setComplete(false);//optimizacion pdf muy grandes
			table.setTableEvent(SimpleTableEvent.getDimensionlessInstance(rectangle, 0.0f));
			table.setSplitLate(true);
			table.setKeepTogether(false);
			//table.setSpacingBefore(Float.NaN);
			table.setHorizontalAlignment(CENTER);
			table.setWidthPercentage(width);
			table.setWidths(widths);
			
			// Configuracion por default que traeran las celdas faltantes
			// que se agreguen en caso hagan falta para completar la l�nea.
			table.getDefaultCell().cloneNonPositionParameters(rectangle);
				
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}
		
	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(int numCols, int width) {
		try{
			float widths[] = new float[numCols];
			for(int i=0;i<numCols;i++) {
				widths[i] = 1;
			}//for
			setTable(numCols, width, widths);
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(int numCols) {
		try{
			setTable(numCols, 100);
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
	}

	public void setLTable(int numCols, int width, float widths[]) {
		try{
			countLCells = 0;//optimizacion pdf muy grandes
			ltable = new PdfPTable(numCols);
			ltable.setComplete(false);//optimizacion pdf muy grandes
			ltable.setHeaderRows(0);
//			ltable.setKeepTogether(true); v2.0
//			ltable.setBorder(Rectangle.NO_BORDER);
			ltable.setWidthPercentage(width);
			ltable.setWidths(widths);
//			ltable.setPadding(padding);
		} catch (Exception e) {
			log.error("setLTable(Exception)", e);
		}
	}
        
    public void setLTableTabs(int numCols, int width, float widths[]) {
            try{
                    countLCells = 0;//optimizacion pdf muy grandes
                    ltable = new PdfPTable(numCols);
                    ltable.setComplete(false);//optimizacion pdf muy grandes
                    ltable.setHeaderRows(0);
    //                //      ltable.setKeepTogether(true); v2.0
                        //  ltable.setBorder(Rectangle.NO_BORDER);
                    ltable.setWidthPercentage(width);
                    ltable.setWidths(widths);
                    //ltable.setPadding(padding);
            } catch (Exception e) {
                    log.error("setLTable(Exception)", e);
            }
    }        

	public void setLTable(int numCols, int width) {
		float widths[] = new float[numCols];
		for(int i=0;i<numCols;i++) {
			widths[i] = 1;
		}//for
		setLTable(numCols, width, widths);
	}

	public void setLTable(int numCols) {
		setLTable(numCols, 100);
	}

	/**
	 * Cuando se ejecuta, repite los headers de la tabla en cada una de las hojas
	 * que abarque esta.
	 *
	 * @deprecated
	 * Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setHeaders() {
		try {
			ArrayList<PdfPRow> 	rows 				= this.table.getRows(); 
			int 						numHeaderRows 	= rows != null?rows.size():0;
			this.table.setHeaderRows(numHeaderRows);
		} catch (Exception e) {
			log.error("setHeaders(Exception)", e);
		}
	}//setHeaders()

	public void setLHeaders() {
		try {
			int headerColumns = this.ltable.size();
			this.ltable.setHeaderRows(headerColumns);
		} catch (Exception e) {
			log.error("setLHeaders(Exception)", e);
		}
	}//setLHeaders()
	
	/**
	 * @deprecated
	 * Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content, String clase, int align, int colspan, int rowspan, int border) {
		
		try {
			
		   chunk = new Chunk(content, getFont(clase));
			Rectangle rectangle = new Rectangle(0f,0f);
			if(border != 1) {
				rectangle.setBorder(Rectangle.NO_BORDER);
			} else {
				rectangle.setBorder( Rectangle.LEFT | Rectangle.RIGHT | Rectangle.TOP | Rectangle.BOTTOM );
			}
			rectangle.setBorderWidth(0.5f);
			rectangle.setBackgroundColor(getColor(clase));
			
			Paragraph p = new Paragraph(chunk);
	      p.setAlignment(align);
	      p.setLeading(iLeading);
	      p.setSpacingBefore(padding);
	      p.setSpacingAfter(padding);
		   
			PdfPCell cell = new PdfPCell();
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(align);
			cell.setColspan(colspan);
			cell.setUseBorderPadding(false);
			cell.setUseDescender(false);
			cell.setLeading(iLeading, 0);
			cell.cloneNonPositionParameters(rectangle);
			cell.setNoWrap(false);
			cell.setBackgroundColor(getColor(clase));
			cell.setRowspan(rowspan);
			cell.addElement(p);
			
			cell.setPadding(padding + 0.0f / 2f);
			cell.setCellEvent(SimpleCellEvent.getDimensionlessInstance(rectangle, 0.0f));

			table.addCell(cell);
			
			countCells += colspan;//optimizacion pdf muy grandes
			if( countCells >= table.getNumberOfColumns() * fragmentsize  ){//optimizacion pdf muy grandes
				countCells=0;//optimizacion pdf muy grandes
				this.document.add(table);//optimizacion pdf muy grandes
				System.gc(); // Debido a que itext genera mucha basura
			}
			
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase, int align, int colspan, int rowspan)
	
	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content, String clase, int align, int colspan, int rowspan) {
		try {
      		setCell(content,clase,align,colspan,rowspan,1);
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase, int align, int colspan, int rowspan)
	
	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content, String clase, int align, int colspan) {
		try {
			setCell(content,clase,align,colspan,1);
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase, int align, int colspan)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content, String clase, int align) {
		try {
			setCell(content,clase,align,1);
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase, int align)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content, String clase) {
		try {

			setCell(content,clase,LEFT);
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(String content) {
		try {
			setCell(content,"formas");
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content)

	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCell(PdfPTable tabla) {
		try {
			
			Rectangle rectangle = new Rectangle(0f,0f);
			rectangle.setBorder(Rectangle.NO_BORDER);
			rectangle.setBorderWidth(0.5f);
			
			cell = new PdfPCell(tabla);
			cell.setPadding(padding + 0.0f / 2f);
			cell.setPaddingTop(padding * 2.0f ); // Nota: Para compensar algun bug raro
		   cell.setPaddingBottom(padding * 2.0f ); // Nota: Para compensar algun bug raro
			cell.setCellEvent(SimpleCellEvent.getDimensionlessInstance(rectangle, 0.0f));
			cell.setBorder(Rectangle.NO_BORDER);
			
			table.addCell(cell);
			
			countCells += 1;//optimizacion pdf muy grandes
			if( countCells >= table.getNumberOfColumns() * fragmentsize ){//optimizacion pdf muy grandes
				countCells=0;//optimizacion pdf muy grandes
				this.document.add(table);//optimizacion pdf muy grandes
				System.gc(); // Debido a que itext genera mucha basura
			}
			
		} catch (Exception e) {
			log.error("setCell(Exception)", e);
		}
	}//setCell(String content, String clase)
	
	/**
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setTable(int numCols, int width, float widths[],int border) {
		
		try{
			countCells = 0;//optimizacion pdf muy grandes
			Rectangle rectangle = new Rectangle(0f,0f);
			if(border==0)
				rectangle.setBorder(Rectangle.TOP | Rectangle.BOTTOM);
			if(border==1)
				rectangle.setBorder(Rectangle.BOTTOM);
			if(border==2)
				rectangle.setBorder(Rectangle.TOP);
			rectangle.setBorderWidth(1.0f);
			
		   table = new PdfPTable(numCols);
		   table.setComplete(false);//optimizacion pdf muy grandes
			table.setTableEvent(SimpleTableEvent.getDimensionlessInstance(rectangle, 0.0f));
			table.setSplitLate(true);
			table.setKeepTogether(false);
			//table.setSpacingBefore(Float.NaN);
			table.setHorizontalAlignment(CENTER);
			table.setWidthPercentage(width);
			table.setWidths(widths);
			
		   // Configuracion por default que traeran las celdas faltantes
		   // que se agreguen en caso hagan falta para completar la l�nea.
			table.getDefaultCell().cloneNonPositionParameters(rectangle);
			
		} catch (Exception e) {
			log.error("setTable(Exception)", e);
		}
		
	}//setTable(int numCols, int width, float widths[],int border)

	/*
	
		Nota By jshernandez (27/03/2015 11:28:33 a.m.):
		
		Debido a un bug en la version 5 de itext que redimensiona cualquier imagen
		sin excluir  el redimensionamiento cuando el ancho de esta es menor 
		al ancho de la celda:
		
		Y pues que no se encontr� un reemplazo aceptable con la nueva versi�n 
		de la librer�a, se decide suprimir los siguientes m�todos:
		
		public void setCellImage(String path, int alignment)
		public void setCellImage(String path)

		Con respecto al m�todo:
		
		setCellImage(String path, int alignment, float percent) 
		
		Se modific� para que reciba una longitud m�xima en pixeles, de tal
		manera que si excede esta, la imagen ser� escalada para que no se salga
		de la celda.
		
	*/
	/**
	 * @param maximumImageWidth Longitud m�xima de la imagen en pixeles, de tal manera
	 * que si la imagene excede la longitud m�xima, esta ser� escalada para que no
	 * la exceda.
	 * Una manera practica de determinar la longitud m�xima de una imagen en una celda
	 * es usando la siguiente formula:
	 * 	cell.getWidth() - padding x 2
	 * 
	 * El cell.getWidth() no se puede determinar inmediatamente, por lo que deber�
	 * usarse el metodo:
	 * 	cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
	 * de la clase:
	 *		com.netro.pdf.SimpleCellEvent
	 *
	 * Basicamente hay que descomentar la l�nea:
	 *    // System.out.println("cell.getWidth="+cell.getWidth());
	 * Ejecutar el metodo para crear el archivo PDF y en la consola
	 * buscar el ancho:
	 *
	 * IMPORTANTE: Una vez que se determine el ancho, volver a comentar la l�nea para
	 * evitar degradaci�n en el performance.
	 *
	 * @deprecated
	 *  Nota By jshernandez (25/03/2015 12:51:59 p.m):
	 *
	 * Se desaconseja utilizar este m�todo, ya que emula clases obsoletas.
	 * Los documentos generados con este m�todo son un 40% mas grandes que
	 * la versi�n que usa "LTables".
	 */
	public void setCellImage(String path, int alignment, int maximumWidth ) {
		try {
			
			image = Image.getInstance(path);
			image.setAlignment(alignment);
			if( image.getWidth() > maximumWidth ){
				image.scalePercent(maximumWidth*100f/image.getWidth());
			} else {
			   image.scalePercent(100f);
			}
			
			Rectangle rectangle = new Rectangle(0f,0f);
		   rectangle.setBorder(Rectangle.NO_BORDER);
			rectangle.setBorderWidth(0.5f);
			
			cell = new PdfPCell();
			cell.setVerticalAlignment(Element.ALIGN_UNDEFINED);
			cell.setHorizontalAlignment(Element.ALIGN_UNDEFINED);
			cell.setColspan(1);
			cell.setUseBorderPadding(false);
			cell.setUseDescender(false);
			cell.setLeading(16.0f, 0);
			cell.cloneNonPositionParameters(rectangle);
			cell.setNoWrap(false);
			cell.addElement(image);
			cell.setPadding(padding + 0.0f / 2f);
			cell.setPaddingTop(padding * 2.0f ); // Nota: Para compensar algun bug raro
		   cell.setPaddingBottom(padding * 2.0f ); // Nota: Para compensar algun bug raro
			cell.setCellEvent(SimpleCellEvent.getDimensionlessInstance(rectangle, 0.0f));
		   table.addCell(cell);
		   
		   countCells += 1;//optimizacion pdf muy grandes
			if( countCells >= table.getNumberOfColumns() * fragmentsize  ){//optimizacion pdf muy grandes
				countCells=0;//optimizacion pdf muy grandes
				this.document.add(table);//optimizacion pdf muy grandes
				System.gc(); // Debido a que itext genera mucha basura
			}
				
		} catch (Exception e) {
			log.error("setCellImage(Exception)", e);
		}
	}
	
	public void setLCell(String content, String clase, int align, int colspan, int rowspan, int border) {
		try {
			
			chunk = new Chunk(content, getFont(clase));
			phrase = new Phrase(chunk);
			lcell = new PdfPCell(phrase);
			if(border != 1)
        		cell.setBorder(Rectangle.NO_BORDER); // Se deja "bug" para mantener la compatibilidad con versiones anteriores. By jshernandez (25/03/2015 12:59:06 p.m.)
			lcell.setHorizontalAlignment(align);
                        lcell.setBorder(0);
			lcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			lcell.setBackgroundColor(getColor(clase));
//			lcell.setLeading(iLeading);
			lcell.setColspan(colspan);
//			lcell.setRowspan(rowspan);
			ltable.addCell(lcell);
			
			countLCells += colspan;//optimizacion pdf muy grandes
			if( countLCells >= ltable.getNumberOfColumns() * fragmentsize ){//optimizacion pdf muy grandes
				countLCells=0;//optimizacion pdf muy grandes
				this.document.add(ltable);//optimizacion pdf muy grandes
				System.gc(); // Debido a que itext genera mucha basura
			}
			
		} catch (Exception e) {
			log.error("setLCell(Exception)", e);
		}
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void setLCellTab(String content) {
	        try {
                    chunk = new Chunk(content, font18);
                    phrase = new Phrase(chunk);
                    lcell = new PdfPCell(phrase);
                    //cell.setBorder(Rectangle.NO_BORDER); // Se deja "bug" para mantener la compatibilidad con versiones anteriores. By jshernandez (25/03/2015 12:59:06 p.m.)
                    lcell.setHorizontalAlignment(LEFT);
                    lcell.setBorder(0);
                    lcell.setVerticalAlignment(Element.ALIGN_TOP);
                    lcell.setBackgroundColor(color5);
                    lcell.setColspan(1);
                    lcell.setPaddingBottom(3f);
                    ltable.addCell(lcell);	                
                    countLCells += 1;
                    if( countLCells >= ltable.getNumberOfColumns() * fragmentsize ){
                        countLCells=0;
                        this.document.add(ltable);
                        System.gc();
                    }
	        } catch (Exception e) {
                        log.error(e.getMessage());
                        e.printStackTrace();
	                log.error("setLCellTab(Exception)", e);
	        }
	}

	public void setLCell(String content, String clase, int align, int colspan, int rowspan) {
		setLCell(content, clase, align, colspan, rowspan, 1);
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void setLCell(String content, String clase, int align, int colspan) {
		setLCell(content, clase, align, colspan, 1);
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void setLCell(String content, String clase, int align) {
		setLCell(content, clase, align, 1);
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void setLCell(String content, String clase) {
		setLCell(content, clase, LEFT);
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void setLCell(String content) {
		setLCell(content, "formas");
	}//setCell(String content, String clase, int align, int colspan, int rowspan)

	public void addCancelado(PdfWriter writer) {
	   log.debug("addCancelado (E) ");
		try {
			   BaseFont helv = BaseFont.createFont("Helvetica", BaseFont.WINANSI, false);
            PdfContentByte cb = writer.getDirectContent();
          
				Phrase watermark = new Phrase("C A N C E L A D O", new Font(FontFamily.TIMES_ROMAN, 70, Font.NORMAL, BaseColor.RED	));
				ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, watermark, 300, 350, 50);
				
		   log.debug("addCancelado (S) ");
		} catch (Exception e) {
			log.error("addCancelado(Exception)", e);
		}
	}//public void addImageAWT(String path)

	public void addImage(String path, int alignment, float percent) {
		try {
			image = Image.getInstance(path);
			image.setAlignment(alignment);
			image.scalePercent(percent);
			document.add(image);
		} catch (Exception e) {
			log.error("addImage(Exception)", e);
		}
	}//public void addImage()

	public void addImage(String path, int alignment) {
		addImage(path, alignment, 100);
	}//public void addImage()

	public void addImage(String path) {
		addImage(path, ComunesPDF.CENTER);
	}//public void addImage()

	public Font getFont(String clase) {
		Font fuente = font1;
		if("formasB".equals(clase) || "celda01".equals(clase))
			fuente = font2;
		else if("celda04".equals(clase))
			fuente = font5;
		else if("celda05".equals(clase))
			fuente = font6;
		else if("formasrep".equals(clase))
			fuente = font3;
		else if("formasrepB".equals(clase) || "celda01rep".equals(clase))
			fuente = font4;
		else if("formasmen".equals(clase))
			fuente = font7;
		else if("formasmenB".equals(clase))
			fuente = font8;
		else if("formasG".equals(clase))
			fuente = font9;
		else if("celda02G".equals(clase))
			fuente = font10;
		else if("subrayado".equals(clase))
			fuente = font11;
		else if("formasMini".equals(clase))
			fuente = font12;
		else if("formasMiniB".equals(clase))
			fuente = font13;
      else if("celdaGrisN".equals(clase))
			fuente = font5;
		else if("celdaGrisB".equals(clase))
			fuente = font6;
		else if("celdaGrisNSmall".equals(clase))
			fuente = font14;
		else if("celdaGrisBSmall".equals(clase))
			fuente = font15;
		else if("celdaGrisNMedium".equals(clase))
			fuente = font16;
		else if("celdaGrisBMedium".equals(clase))
			fuente = font17;
		else if("celda01repNSmall".equals(clase))
			fuente = font12;
		else if("celda01repBSmall".equals(clase))
			fuente = font13;
		else if("formasrepNSmall".equals(clase))
			fuente = font12;
		else if("formasrepBSmall".equals(clase))
			fuente = font13;
		else if("formasNSmall".equals(clase))
			fuente = font12;
		else if("formasBSmall".equals(clase))
			fuente = font13;
		else if("contrato".equals(clase)) // Estilo Leyenda Contratos CESION
			fuente = font18; 
		
			return fuente;
	}//getFont(String clase)

	public BaseColor getColor(String clase) {

		BaseColor color = color5;
		if("celda01".equals(clase) || "celda01rep".equals(clase) || "formasmenB".equals(clase) || "celda02G".equals(clase) )
			color = color3;
		else if("celda02".equals(clase))
			color = color4;
		else if("celda03".equals(clase))
			color = color2;
		else if("celda04".equals(clase)    || "celda05".equals(clase))
			color = color1;
		else if("celdaGrisN".equals(clase) || "celdaGrisB".equals(clase))
			color = color3;
		else if("celdaGrisNSmall".equals(clase)  || "celdaGrisBSmall".equals(clase)  )
			color = color3;
		else if("celdaGrisNMedium".equals(clase)  || "celdaGrisBMedium".equals(clase)  )
			color = color3;
		else if("celda01repNSmall".equals(clase) || "celda01repBSmall".equals(clase) )
			color = color3;
		else if("formasrepNSmall".equals(clase)  || "formasrepBSmall".equals(clase)  )
			color = color5;
		else if("formasNSmall".equals(clase)     || "formasBSmall".equals(clase)     )
			color = color5;

		return color;
	}//getColor(String clase)

	public PdfPTable getTable() {
		return this.table;

	}//getTablaReporte

	public void endDocument() {
		try {
			document.close();
		} catch (Exception e) {
			log.error("endDocument(Exception)", e);
		}
	}//endDocument

	public void encabezadoConImagenes(ComunesPDF pdfDoc_aux,String strPais_aux,
									  String iNoNafinElectronico_aux,String sesExterno_aux,
									  String strNombre_aux,String strNombreUsuario_aux,
									  String strLogo_aux,String strDirectorioPublicacion){
		String datosUsuario =
			"\n"+ (("MEXICO".equals(strPais_aux))?"":strPais_aux)+
			"\n"+ (iNoNafinElectronico_aux == null?"":iNoNafinElectronico_aux)+
			"\n"+ (("S".equals(sesExterno_aux))?"":strNombre_aux)+
			"\n"+ strNombreUsuario_aux;
			
			String rutaLogo =  "/00archivos/15cadenas/15archcadenas/logos/";	
			String prefigo = strLogo_aux.substring(0,3);
			String rutaBaners = "/00archivos/15cadenas/15archcadenas/banner/ba_"+strLogo_aux;
			
			if(prefigo.equals("IF_")) {
				 strLogo_aux = strLogo_aux.substring(3,strLogo_aux.length());				
				 rutaLogo  = "/00archivos/if/logos/";
				rutaBaners = "/00archivos/15cadenas/15archcadenas/banner/ba_nafin.gif";
			}
		
		int maximumImageWidth 	= 0;
		if( 			this.orientacionPagina == HOJA_VERTICAL ){
		   maximumImageWidth 	= 173-(int)Math.ceil(padding)*2;
		} else if( 	this.orientacionPagina == HOJA_HORIZONTAL ) {
		   maximumImageWidth 	= 256-(int)Math.ceil(padding)*2;
		}
			
		pdfDoc_aux.setTable(3);
		pdfDoc_aux.setCellImage(strDirectorioPublicacion+rutaLogo+strLogo_aux,ComunesPDF.LEFT,maximumImageWidth);
		pdfDoc_aux.setCell(datosUsuario,"formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
		pdfDoc_aux.setCellImage(strDirectorioPublicacion+rutaBaners,ComunesPDF.RIGHT,maximumImageWidth);
		pdfDoc_aux.addTable();

   }//encabezadoConImagenes

		public void encabezadoConImagenesPersonalizado(ComunesPDF pdfDoc_aux,String strPais_aux,
																	  String iNoNafinElectronico_aux,String sesExterno_aux,
																	  String strNombre_aux,String strNombreUsuario_aux,
																	  String strLogo1_aux, String strLogo2_aux,
																	  String strDirectorioPublicacion){
		String datosUsuario =
			"\n"+ (("MEXICO".equals(strPais_aux))?"":strPais_aux)+
			"\n"+ (iNoNafinElectronico_aux == null?"":iNoNafinElectronico_aux)+
			"\n"+ (("S".equals(sesExterno_aux))?"":strNombre_aux)+
			"\n"+ strNombreUsuario_aux;
				
		String prefigo = strLogo1_aux.substring(0,3);
		if(prefigo.equals("IF_")) {
			strLogo1_aux = strLogo1_aux.substring(3,strLogo1_aux.length());
			strLogo2_aux = strLogo2_aux.substring(3,strLogo2_aux.length());	
		}
		
		int maximumImageWidth 	= 0;
		if( 			this.orientacionPagina == HOJA_VERTICAL ){
		   maximumImageWidth 	= 173-(int)Math.ceil(padding)*2;
		} else if( 	this.orientacionPagina == HOJA_HORIZONTAL ) {
		   maximumImageWidth 	= 256-(int)Math.ceil(padding)*2;
		}
		
		pdfDoc_aux.setTable(3);
		pdfDoc_aux.setCellImage(strDirectorioPublicacion+strLogo1_aux,ComunesPDF.LEFT,maximumImageWidth);
		pdfDoc_aux.setCell(datosUsuario,"formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
		pdfDoc_aux.setCellImage(strDirectorioPublicacion+strLogo2_aux,ComunesPDF.RIGHT,maximumImageWidth);
		pdfDoc_aux.addTable();

   }//encabezadoConImagenesPersonalizado

	public void encabezadoPersonalizado(
					String 	strDatosUsuario, 	String strLogo,
					String 	strDirectorio, List encabezadoEdoCuenta) {
		try {
			this.strDatosUsuario = strDatosUsuario;

			String prefigo = strLogo.substring(0,3);
			if(prefigo.equals("IF_")) {
				strLogo = strLogo.substring(3,strLogo.length());						 
			}
			this.strLogo =	strDirectorio +	"00archivos/15cadenas/15archcadenas/logos/" +strLogo;
			//this.bPiePagina = true;

			this.encabezadoEdoCuenta = encabezadoEdoCuenta;

		} catch(Exception e) {
			log.error("encabezado(Exception) ", e);
		}
	}

	public boolean newPage(){
		boolean retorno = false;
		try{
			retorno =  document.newPage();
		}catch(Exception e){
			log.error("newPage(Exception) ", e);
		}
		return retorno;
	}

	/**
	 * Metodo que funciona para concatenar archivos PDF por p�ginas
	 * El ultimo elemento del arreglo es el resultado de la concatenaci�n de PDFs
	 * @param args Arreglo de nombres de los archivos a combinar (con la ruta absoluta)
	 *
	 **/
	public static void doMerge(String args[])
            throws DocumentException, IOException {

		  try {
			  
         if (args.length < 2) {
           log.error
             ("Usage: ConcatPDFFiles file1.pdf [file2.pdf... fileN.pdf] out.pdf");
           //System.exit(1);
         }

         ArrayList<HashMap<String, Object>> 	allBookmarks 	= new ArrayList<HashMap<String, Object>>();
         int 			f 					= 0;
         String 		outFile 			= args[args.length - 1];
         Document 	document 		= null;
         PdfCopy 		writer 			= null;
			  
			int 			pageOffset 	= 0;
			  
         while (f < args.length - 1) {
				
            PdfReader reader = new PdfReader(args[f]);

				reader.consolidateNamedDestinations();
				int n = reader.getNumberOfPages();
				List<HashMap<String, Object>> bookmarks = SimpleBookmark.getBookmark(reader);
				if (bookmarks != null) {
					if (pageOffset != 0) {
						SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
					}
					allBookmarks.addAll(bookmarks);
				}
				pageOffset += n;
				
            if (f == 0) {
              document = new Document();
              writer =  new PdfSmartCopy(document, new FileOutputStream(outFile));
								// Nota By jshernandez(25/03/2015 01:00:36 p.m.): Se usa PdfSmartCopy en lugar de PdfCopy 
								// por un bug en PdfCopy que hac�a que las anotaciones no se mostraran
								// en su estatus original.
				  writer.setMergeFields();
              document.open();
            }
				
				writer.addDocument(reader);
				
				// Actualizar indice de archivos a concatenar.
            f++;
				
         }
			  
			// Add the merged bookmarks
			if( writer != null ){
				writer.setOutlines(allBookmarks);
			}
			  
			// Cerrar documento
         document.close();
			  
      } catch (Exception e) {
        e.printStackTrace();
      }

    }


}//class TablaReporte