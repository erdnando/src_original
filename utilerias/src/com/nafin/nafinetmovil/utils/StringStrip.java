package com.nafin.nafinetmovil.utils;

public class StringStrip
{
  public StringStrip()
  {
  }

  public static String replaceHTMLAcutes(String str) {
    if(str!=null){
      str = str.replaceAll("�|�","&aacute;");
      str = str.replaceAll("�|�","&eacute;");
      str = str.replaceAll("�|�","&iacute;");
      str = str.replaceAll("�|�","&oacute;");
      str = str.replaceAll("�|�","&uacute;");
      str = str.replaceAll("�|�","&Aacute;");
      str = str.replaceAll("�|�","&Eacute;");
      str = str.replaceAll("�|�","&Iacute;");
      str = str.replaceAll("�|�","&Oacute;");
      str = str.replaceAll("�|�","&Uacute;");
      str = str.replaceAll("�","&ntilde;");
      str = str.replaceAll("�","&Ntilde;");
      str = str.replaceAll("�","&uuml;");
      str = str.replaceAll("�","&Uuml;");
      str = str.replaceAll("�","&yacute;");
      str = str.replaceAll("�","&yacute;");
      str = str.replaceAll("\"","&quot;");
    }
    return str;
  }
  public static String replaceHTMLAcutes2(String str) {
    if(str!=null){
      str = str.replaceAll("�|�","&aacute;");
      str = str.replaceAll("�|�","&eacute;");
      str = str.replaceAll("�|�","&iacute;");
      str = str.replaceAll("�|�","&oacute;");
      str = str.replaceAll("�|�","&uacute;");
      str = str.replaceAll("�|�","&Aacute;");
      str = str.replaceAll("�|�","&Eacute;");
      str = str.replaceAll("�|�","&Iacute;");
      str = str.replaceAll("�|�","&Oacute;");
      str = str.replaceAll("�|�","&Uacute;");
      str = str.replaceAll("�","&ntilde;");
      str = str.replaceAll("�","&Ntilde;");
      str = str.replaceAll("�","&uuml;");
      str = str.replaceAll("�","&Uuml;");
      str = str.replaceAll("�","&yacute;");
      str = str.replaceAll("�","&yacute;");
    }
    return str;
  }
}