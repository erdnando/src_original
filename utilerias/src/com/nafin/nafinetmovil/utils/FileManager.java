package com.nafin.nafinetmovil.utils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileManager 
{
  public FileManager()
  {
  }
  
  public static void write(InputStream inputStream, OutputStream outputStream) throws IOException {
		int data = 0;
		while (true) {
			data = inputStream.read();
			if (data == -1) {
				break;
			}
			outputStream.write(data);
		}
	}
  public static InputStream getStream(String fullPath) throws FileNotFoundException{
		InputStream inputStream = new FileInputStream(fullPath);
		return inputStream;
	}
  
  public static boolean existeArchivo(String sRealPath)throws Exception {
    boolean resultado = false;
    try {
      File file=new File(sRealPath); 
      if(file.exists() && file.length()>0) { 
        resultado = true;
      }
    } catch(Exception e) {
      throw e;
    }
    return resultado;
  }
  
  public static File getArchivo(String sRealPath)throws Exception {
    File resultado = null;
    try {
      File file=new File(sRealPath); 
      if(file.exists() && file.length()>0) { 
        resultado = file;
      }
    } catch(Exception e) {
      throw e;
    }
    return resultado;
  }
  
  public static void copy(File inputFile,File outputFile)throws Exception {
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputFile));
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFile));
           int theChar;
           while ((theChar = bis.read()) != -1) {
              bos.write(theChar);
           }
        bos.close();
        bis.close();
  }
  
  public static void cut(File inputFile,File outputFile)throws Exception {
      
      BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputFile));
      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFile));
           int theChar;
           while ((theChar = bis.read()) != -1) {
              bos.write(theChar);
           }
        bos.close();
        bis.close();
       inputFile.delete();
  }

 public static File deleteArchivos(String sRealPath,String patron)throws Exception {
    File resultado = null;
    try {
      File file=new File(sRealPath); 
      if(file.exists() && file.isDirectory()) { 
        File [] hijos=file.listFiles();
        for(int i=0;i<hijos.length;i++){
          if(hijos[i].getName().indexOf(patron)!=-1){
            hijos[i].delete();
          }
        }
      }
    } catch(Exception e) {
      throw e;
    }
    return resultado;
  }

 public static File deleteFirstArchivo(String sRealPath,String patron)throws Exception {
    File resultado = null;
    try {
      File file=new File(sRealPath); 
      if(file.exists() && file.isDirectory()) { 
        File [] hijos=file.listFiles();
        for(int i=0;i<hijos.length;i++){
          if(hijos[i].getName().indexOf(patron)!=-1){
            hijos[i].delete();
            break;
          }
        }
      }
    } catch(Exception e) {
      throw e;
    }
    return resultado;
  }


  public static void deleteArchivo(String sRealPath)throws Exception {
    try {
      File file=new File(sRealPath); 
      if(file.exists() && file.length()>0) { 
        file.delete();
      }
    } catch(Exception e) {
      throw e;
    }
  }
  
}