package com.nafin.nafinetmovil.utils;
import java.io.Serializable;
import java.util.ArrayList;

public class Paginado implements Serializable
{
  int listSize;
  int paginaTamano=0;
  int pagina;
  int paginasTotal;
  int totalElementos;
  ArrayList datos;
  public  Paginado(int listSize,int pagina){
    this.listSize=listSize;
    this.paginaTamano=5;
    this.pagina=pagina;
    this.totalElementos=listSize;
    this.paginasTotal=getPaginasTotal();
  }
  public  int getPaginasTotal(){
    int base = totalElementos / paginaTamano;
    int mod = totalElementos % paginaTamano;
    return base + (mod > 0?1:0);
  }

  public int getPrimerRenglon(){
    return(((pagina-1)*paginaTamano))+1;
  }

  public int getUltimoRenglon(){
  int resultado=0;
  if(pagina!=paginasTotal){resultado=(paginaTamano*(pagina));}
  else{resultado=listSize;}
  return resultado;
  }

   public String printPie(String URL){
    String piePaginacion="";
    piePaginacion+=println("<div class=\"page\">"+getUltimaPagina(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPaginaSiguiente(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+pagina+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPaginaAnterior(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPrimeraPagina(URL)+"</div>" );
    return piePaginacion;
  }

  private String getPaginaAnterior(String URL){
    String resultado="";
    if(paginasTotal>1 && pagina!=1)
    {
       resultado="     <a href='"+URL+"&amp;page="+(pagina-1)+"'>&lt;&lt;</a>";//<img align=middle src='"+dirImagenAnterior+"' border='0'/></a>";
    }
    return resultado;
  }

  private String getPaginaSiguiente(String URL){
    String resultado="";
    if(pagina<paginasTotal && paginasTotal>1){
        resultado="     <a href='"+URL+"&amp;page="+(pagina+1)+"'>&gt;&gt;</a>";//<img align='middle' src='"+dirImagenSiguiente+"' border='0'/></a>";
    }
    return resultado;
  }

  private String getPrimeraPagina(String URL){
   String resultado="";
    if(paginasTotal>1 && pagina!=1){
        resultado="     <a href='"+URL+"&amp;page="+(1)+"'>|&lt;&lt;</a>";//<img align='middle' src='"+dirImagenPrimera+"' border='0'/></a>";
    }
    return resultado;
  }

   private String getUltimaPagina(String URL){
   String resultado="";
    if(pagina<paginasTotal && paginasTotal>1){
        resultado="     <a href='"+URL+"&amp;page="+(paginasTotal)+"'>&gt;&gt;|</a>";//<img align='middle' src='"+dirImagenUltima+"' border='0'/></a>";
    }
    return resultado;
  }

  public String printPieSimple(String URL){
    String piePaginacion="";
    piePaginacion+=println("<div class=\"page\">"+getUltimaPaginaSimple(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPaginaSiguienteSimple(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+pagina+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPaginaAnteriorSimple(URL)+"</div>" );
    piePaginacion+=println("<div class=\"page\">"+getPrimeraPaginaSimple(URL)+"</div>" );
  return piePaginacion;
  }

  private String getPrimeraPaginaSimple(String URL){
   String resultado="";
    if(paginasTotal>1 && pagina!=1){
        resultado="     <a href='"+URL+"?page="+(1)+"'>|&lt;&lt;</a>";
    }
    return resultado;
  }

  private String getPaginaAnteriorSimple(String URL){
    String resultado="";
    if(paginasTotal>1 && pagina!=1)
    {
       resultado="     <a href='"+URL+"?page="+(pagina-1)+"'>&lt;&lt;</a>";
    }
    return resultado;
  }

  private String getPaginaSiguienteSimple(String URL){
    String resultado="";
    if(pagina<paginasTotal && paginasTotal>1){
        resultado="     <a href='"+URL+"?page="+(pagina+1)+"'>&gt;&gt;</a>";
    }
    return resultado;
  }

   private String getUltimaPaginaSimple(String URL){
   String resultado="";
    if(pagina<paginasTotal && paginasTotal>1){
        resultado="     <a href='"+URL+"?page="+(paginasTotal)+"'>&gt;&gt;|</a>";
    }
    return resultado;
  }

  private String println(String cadena){
    return cadena+"\n";
  }

  public String getUrlAnterior(String URL){
  String resultado="";
    if(paginasTotal>1 && pagina!=1)
    {
       resultado=URL+"&amp;page="+(pagina-1);
    }
    return resultado;
  }
  public String getUrlSiguiente(String URL){
    String resultado="";
    if(pagina<paginasTotal && paginasTotal>1){
        resultado=URL+"&amp;page="+(pagina+1);
    }
    return resultado;
  }

  public int getPaginaActual(){
      return this.pagina;
  }


  public void setDatos(ArrayList datos)
  {
    this.datos = datos;
  }


  public ArrayList getDatos()
  {
    return datos;
  }

}