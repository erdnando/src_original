package com.nafin.nafinetmovil.utils;


public class Constantes{

   //public static final String XHTML_CACHE_PATH="/xhtml/";
   //public static final String IMAGES_PATH="/opt/oraias/portalnf/portatil_img/";
   //public static final String IMAGES_PATH="C:/Users/Robin/Documents/Nafin/Movil/jdev/NafinMovil/WebApp/public_html/";
   //public static final String IMAGES_PATH="/oraclei/portalnf/imagenes/imagesmovil/";
   public static final String RUTA_ERROR_PAGE="/nafinetmovil/transaccional/error.jsp";
   //public static final String IMAGE_JPG_CONTENT_TYPE = "image/jpeg";
   //public static final String IMAGE_GIF_CONTENT_TYPE = "image/gif";
   //public static final String GENERIC_FILE_CONTENT_TYPE = "application/octet-stream";
   //public static final int CONTENIDO_TEMPLATE=3;
   //public static final int SECCION_TEMPLATE=2;
   //public static final String SECCION_TIPO="S";
   //public static final String IMAGE_UPLOAD_PATH="/admin/upload/";
   //public static final String IMAGE_SERVLET="../GetImageS?path="+IMAGE_UPLOAD_PATH;
   //public static final String IMAGE_SERVLET_NAME="../GetImageS?name=";
   //public static final long IMAGE_SIZE=20 * 1024;
   public static final String FOOTER="Copyright Nacional Financiera 2012 / info@nafin.gob.mx / </a> <a class=\"nafin_phone\" href=\"tel:018006234672\"> 01 800 Nafinsa </a> <a> / Av. Insurgentes Sur 1971, Col. Guadalupe Inn, CP 01020, M&eacute;xico D.F.";
   public static final String SITE_TITLE="Nacional Financiera :: Nafinet M&oacute;vil";
   public static final String HOME_PAGE="/nafinetmovil/transaccional/login.xhtml";
   //public static final String AGREGAR_COMENTARIO="agregar_comentario.xhtml";
   //public static final String DETALLE_AVISO="detalle_aviso.xhtml";
   //public static final String EMAIL_HOST="130.0.24.41";
   //public static final String EMAIL_ADDRESS="webmaster@nafin.gob.mx";
   //public static final String LOGO_NAFIN="images/header.jpg";
   //public static final String LOGO_NAFIN_IPAD="images/header_iPad.jpg";
   public static final String LOGO_NAFINET="images/header.jpg";
   public static final String LOGO_NAFINET_IPAD="images/header_iPad.jpg";
   //public static final String NO_DATA_LICITACION_MSG="No se encontraron licitaciones";
   //public static final String NO_DATA_FORO_TEMA_MSG="No existen temas";
   //public static final String NO_DATA_FORO_COMEN_MSG="No existen comentarios en este tema";
   //public static final String NO_DATA_SOLICITUD_MSG="No se encontraron solicitudes";
   //public static final String NO_DATA_OFRECIMIENTO_MSG="No se encontraron ofrecimientos";
   public static final String PREFIX_OID_WS="NE_";
}