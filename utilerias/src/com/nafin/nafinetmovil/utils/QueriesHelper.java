package com.nafin.nafinetmovil.utils;

public class QueriesHelper
{
  public static final String DELETE_JERARQUIA_SECCION="DELETE PM_JERARQUIA_SECCIONES WHERE SEC_ID=?";

  //-----------------------       Seguridad--------------------------------------------------
  public static final String GET_USER=" SELECT * FROM PM_USUARIO"+
                                   " WHERE USU_CORREO=? AND USU_CONTRASENA=?";

  public static final String INSERT_USER= "INSERT INTO PM_USUARIO\n" +
                                          "  (USU_ID, USU_NOMBRE, USU_CORREO, USU_CONTRASENA, USU_VIGENCIA, USU_WEB)  \n" +
                                          "VALUES(?,?,?,?,?,?)";

  public static final String INSERT_USER_SELECT="  SELECT PM_SEQ_USUARIO.NEXTVAL,\n" +
                                                "       USER_NAME||' '||USER_LASTNAME1||' '||USER_LASTNAME2 NOMBRE, \n" +
                                                "       USER_EMAIL CORREO,\n" +
                                                "       USER_PASSWORD CONTRASENA,\n" +
                                                "       DECODE(P.PERMISO,1,'S','N') VIGENCIA,\n" +
                                                "       USER_ID\n" +
                                                "  FROM POR_USER U,\n" +
                                                "  POR_PERMISOS_ADMINISTRADOR P\n" +
                                                " WHERE USER_EMAIL LIKE ?\n" +
                                                "  AND USER_PASSWORD=?\n" +
                                                " AND U.PROFILE_ID=P.PROFILE_ID\n" +
                                                " AND P.MODULE_ID=(SELECT MODULE_ID \n" +
                                                "                  FROM POR_MODULE\n" +
                                                "                  WHERE MODULE_NAME='CMS Movil'\n" +
                                                "                  )" ;

  //-----------------------       Foros--------------------------------------------------
  public static final String ACTIVOS_ORDENADOS_FORO=" SELECT fcategory_id,\n" +
                                            " fcategory_name,\n" +
                                            " fcategory_desc,\n" +
                                            " fcategory_order,\n" +
                                            " TO_CHAR(fcategory_creation,'dd/mm/yyyy') AS FCATEGORY_CREATION,\n" +
                                            " fcategory_activo\n" +
                                            " FROM por_fcategory\n" +
                                            " WHERE fcategory_activo=1\n" +
                                            " ORDER BY fcategory_creation DESC\n";
  public static final String CATEGORIA_FORO=" select fcategory_id,\n" +
                                            " fcategory_name,\n" +
                                            " fcategory_desc,\n" +
                                            " fcategory_order,\n" +
                                            " TO_CHAR(fcategory_creation,'dd/mm/yyyy') AS FCATEGORY_CREATION,\n" +
                                            " fcategory_activo\n" +
                                            " from por_fcategory\n";
  public static final String COMENTARIOS_FORO="SELECT FORO_ID,\n" +
                                              "  COMEN_ID,\n" +
                                              "  COMEN_AUTOR,\n" +
                                              "  COMEN_TEXTO,\n" +
                                              "  TO_CHAR(COMEN_FECHA,'dd/mm/yyyy') COMEN_FECHA,\n" +
                                              "  COMEN_AUTORIZADO\n" +
                                              "FROM PM_FORO_COMENTARIOS";
  public static final String SELECT_COMENTARIOS_FORO="SELECT * FROM\n" +
                                              "(\n"+
                                              "  SELECT FORO_ID,\n" +
                                              "  COMEN_ID,\n" +
                                              "  COMEN_AUTOR,\n" +
                                              "  COMEN_TEXTO,\n" +
                                              "  TO_CHAR(COMEN_FECHA,'dd/mm/yyyy') COMEN_FECHA,\n" +
                                              "  COMEN_AUTORIZADO,\n" +
                                              "  'MOVIL' FORO_TIPO\n" +
                                              "  FROM PM_FORO_COMENTARIOS\n" +
                                              "  WHERE FORO_ID=?\n" +
                                              "  AND COMEN_AUTORIZADO='S'\n" +
                                              "UNION ALL\n" +
                                              "  SELECT a.fcategory_id FORO_ID,\n"+
                                              "  a.fcomment_id COMEN_ID,\n"+
                                              "  b.user_name || ' ' || b.user_lastname1 || ' ' || b.user_lastname2 COMEN_AUTOR,\n"+
                                              "  a.fcomment_text COMEN_TEXTO,\n"+
                                              "  TO_CHAR(a.fcomment_date,'dd/mm/yyyy') COMEN_FECHA,\n"+
                                              "  DECODE(a.fcomment_authorized,1,'S','N') COMEN_AUTORIZADO,\n"+
                                              "  'WEB' FORO_TIPO\n" +
                                              "  FROM POR_FCOMMENT a\n"+
                                              "  INNER JOIN por_user b ON a.user_id=b.user_id\n"+
                                              "  WHERE a.fcategory_id=?\n" +
                                              "  AND a.fcomment_authorized=1\n" +
                                              ")\n" +
                                              "ORDER BY TO_DATE(COMEN_FECHA,'dd/mm/yyyy') DESC";
  public static final String SELECT_COMENTARIO_FORO_MOVIL="  SELECT FORO_ID,\n" +
                                              "  COMEN_ID,\n" +
                                              "  COMEN_AUTOR,\n" +
                                              "  COMEN_TEXTO,\n" +
                                              "  TO_CHAR(COMEN_FECHA,'dd/mm/yyyy') COMEN_FECHA,\n" +
                                              "  'MOVIL' FORO_TIPO" +
                                              "  FROM PM_FORO_COMENTARIOS\n" +
                                              "  WHERE FORO_ID=?" +
                                              "  AND COMEN_ID=?";
  public static final String SELECT_COMENTARIO_FORO_WEB="  SELECT a.fcategory_id FORO_ID,\n"+
                                              "  a.fcomment_id COMEN_ID,\n"+
                                              "  b.user_name || ' ' || b.user_lastname1 || ' ' || b.user_lastname2 COMEN_AUTOR,\n"+
                                              "  a.fcomment_text COMEN_TEXTO,\n"+
                                              "  TO_CHAR(a.fcomment_date,'dd/mm/yyyy') COMEN_FECHA,\n"+
                                              "  'WEB' FORO_TIPO" +
                                              "  FROM POR_FCOMMENT a\n"+
                                              "  INNER JOIN por_user b ON a.user_id=b.user_id\n"+
                                              "  WHERE a.fcategory_id=?" +
                                              "  AND a.fcomment_id=?";
   public static final String INSERT_FORUM_COMMENT=   "INSERT\n" +
                                    "INTO PM_FORO_COMENTARIOS\n" +
                                    "  (\n" +
                                    "    FORO_ID,\n" + //1
                                    "    COMEN_ID,\n" +
                                    "    COMEN_AUTOR,\n" + //2
                                    "    COMEN_TEXTO,\n" + //3
                                    "    COMEN_FECHA,\n" +
                                    "    COMEN_AUTORIZADO\n" +
                                    "  )\n" +
                                    "  VALUES\n" +
                                    "  (?,"+//1
                                    "   PM_SEQ_FORO_COMEN.NEXTVAL,"+
                                    "   ?,"+//2
                                    "   ?,"+//3
                                    "   SYSDATE,"+
                                    "   'N')";
  public static final String SELECT_TITULO_FORO="SELECT FCATEGORY_NAME\n" +
                                              "  FROM POR_FCATEGORY\n" +
                                              "  WHERE FCATEGORY_ID=?\n";
  public static final String CONTACTO="SELECT CONTAC_ID,\n" +
                                      "  CONTAC_NOMBRE,\n" +
                                      "  CONTAC_TELEFONO,\n" +
                                      "  CONTAC_CORREO,\n" +
                                      "  CONTAC_EMPRESA,\n" +
                                      "  CONTAC_ASUNTO,\n" +
                                      "  CONTAC_COMENTARIO,\n" +
                                      "  TO_CHAR(CONTAC_FECHA,'dd/mm/yyyy') CONTAC_FECHA,\n" +
                                      "  CONTAC_ATENDIDO\n" +
                                      "FROM PM_CONTACTO";
   public static final String INSERT_CONTACT=   "INSERT\n" +
                                    "INTO PM_CONTACTO\n" +
                                    "  (\n" +
                                    "    CONTAC_ID,\n" +
                                    "    CONTAC_NOMBRE,\n" +    //1
                                    "    CONTAC_TELEFONO,\n" +  //2
                                    "    CONTAC_CORREO,\n" +    //3
                                    "    CONTAC_EMPRESA,\n" +   //4
                                    "    CONTAC_ASUNTO,\n" +     //5
                                    "    CONTAC_COMENTARIO,\n" + //6
                                    "    CONTAC_FECHA,\n" +
                                    "    CONTAC_ATENDIDO" +
                                    "  )\n" +
                                    "  VALUES\n" +
                                    "  (PM_SEQ_CONTACTO.NEXTVAL,"+
                                    "   ?,"+                    //1
                                    "   ?,"+                    //2
                                    "   ?,"+                    //3
                                    "   ?,"+                    //4
                                    "   ?,"+                    //5
                                    "   ?,"+                    //6
                                    "   SYSDATE,"+
                                    "   'N')";
   public static final String INSERT_JERARQUIA_SEC=   "INSERT\n" +
                                    "INTO PM_JERARQUIA_SECCIONES\n" +
                                    "  (\n" +
                                    "    SEC_ID,\n" + //1
                                    "    SEC_TITULO,\n" + //2
                                    "    SEC_TIPO,\n" + //3
                                    "    SEC_PADRE_ID,\n" + //4
                                    "    SEC_USU_CREA,\n" + //5
                                    "    SEC_FECHA_CREA,\n" +
                                    "    SEC_ACTIVA,\n" +//6
                                    "    SEC_ORDEN,\n" +//7
                                    "    SEC_PLANTILLA,\n" +//8
                                    "    SEC_EDITABLE,\n" +
                                    "    SEC_PERMITE_HIJOS,\n" +
                                    "    SEC_BORRABLE,\n" +
                                    "    SEC_URL\n" +
                                    "  )\n" +
                                    "  VALUES\n" +
                                    "  (?,"+//1
                                    "   ?,"+//2
                                    "   ?,"+//3
                                    "   ?,"+//4
                                    "   ?,"+//5
                                    "   SYSDATE,"+
                                    "   ?,"+//6
                                    "   ?,"+//7
                                    "   ?,"+//8
                                    "   'S',"+
                                    "   'S',"+
                                    "   'S',"+
                                    "    ?)";
   public static final String INSERT_CONTENIDO0=  "INSERT\n" +
                                                  "INTO PM_CONTENIDO\n" +
                                                  "  (\n" +
                                                  "    CON_ID,\n" +
                                                  "    CON_CONTENIDO\n" +
                                                  "  )\n" +
                                                  "  VALUES\n" +
                                                  "  (\n" +
                                                  "   ?,"+
                                                  "   ?"+
                                                  "  )";
   public static final String SELECT_JERARQUIA_ID="SELECT PM_SEQ_JERARQUIA_SEC.NEXTVAL FROM DUAL";

   public static final String SELECT_MAX_ORDEN=" SELECT MAX(SEC_ORDEN)+1 FROM PM_JERARQUIA_SECCIONES"+
                                                  " WHERE SEC_PADRE_ID=?";

   public static final String SELECT_JERARQUIA_SEC= " SELECT JER.SEC_ID,\n" +
                                                    "       JER.SEC_TITULO,\n" +
                                                    "       JER.SEC_TIPO,\n" +
                                                    "       JER.SEC_PADRE_ID,\n" +
                                                    "       JER.SEC_USU_CREA,\n" +
                                                    "       JER.SEC_FECHA_CREA,\n" +
                                                    "       JER.SEC_USU_MODIFICA,\n" +
                                                    "       JER.SEC_FECHA_MODIFICA,\n" +
                                                    "       JER.SEC_ACTIVA,\n" +
                                                    "       JER.SEC_ORDEN,\n" +
                                                    "       JER.SEC_PLANTILLA,\n" +
                                                    "       PLAN.PLAN_URL,"+
                                                    "       JER.SEC_EDITABLE,\n" +
                                                    "       JER.SEC_PERMITE_HIJOS,\n" +
                                                    "       JER.SEC_BORRABLE,\n" +
                                                    "       JER.SEC_URL\n" +
                                                    "  FROM PM_JERARQUIA_SECCIONES JER,"+
                                                    "       PM_PLANTILLAS PLAN "+
                                                    "  WHERE SEC_ID=? "+
                                                    "  AND  JER.SEC_PLANTILLA=PLAN.PLAN_ID ";
   public static final String SELECT_CONTENIDO= "select CON_ID, CON_CONTENIDO from pm_contenido where con_id=?";

   public static final String UPDATE_CONTENIDO= "UPDATE PM_CONTENIDO\n" +
                                                "   SET  \n" +
                                                "       CON_CONTENIDO = ?\n" +
                                                " WHERE CON_ID = ?";

   public static final String UPDATE_JERARQUIA= "UPDATE PM_JERARQUIA_SECCIONES\n" +
                                                "   SET \n" +
                                                "       SEC_TITULO = ?,\n" +
                                                "       SEC_USU_MODIFICA = ?,\n" +
                                                "       SEC_FECHA_MODIFICA = SYSDATE,\n" +
                                                "       SEC_ACTIVA = ?,\n" +
                                                "       SEC_URL = ?\n" +
                                                " WHERE SEC_ID = ?";

   public static final String SELECT_SECCION="SELECT SEC_ID, SEC_DESCRIPCION, SEC_IMAGEN_URL FROM PM_SECCION WHERE SEC_ID=?";
   public static final String INSERT_SECCION=" INSERT INTO PM_SECCION\n" +
                                             "  (SEC_ID, SEC_DESCRIPCION, SEC_IMAGEN_URL)\n" +
                                             "VALUES\n" +
                                             "  (?, ?, ?)";
   public static final String UPDATE_SECCION= "UPDATE PM_SECCION\n" +
                                              "   SET \n" +
                                              "       SEC_DESCRIPCION =?,\n" +
                                              "       SEC_IMAGEN_URL = ?\n" +
                                              " WHERE SEC_ID = ?";


   public static final String SELEC_HIJOS_SECCION=" SELECT JER.SEC_ID,\n" +
                                                    "       JER.SEC_TITULO,\n" +
                                                    "       JER.SEC_TIPO,\n" +
                                                    "       JER.SEC_PADRE_ID,\n" +
                                                    "       JER.SEC_USU_CREA,\n" +
                                                    "       JER.SEC_FECHA_CREA,\n" +
                                                    "       JER.SEC_USU_MODIFICA,\n" +
                                                    "       JER.SEC_FECHA_MODIFICA,\n" +
                                                    "       JER.SEC_ACTIVA,\n" +
                                                    "       JER.SEC_ORDEN,\n" +
                                                    "       JER.SEC_PLANTILLA,\n" +
                                                    "       PLAN.PLAN_URL,"+
                                                    "       JER.SEC_EDITABLE,\n" +
                                                    "       JER.SEC_PERMITE_HIJOS,\n" +
                                                    "       JER.SEC_BORRABLE,\n" +
                                                    "       JER.SEC_URL\n" +
                                                    "  FROM PM_JERARQUIA_SECCIONES JER,"+
                                                    "       PM_PLANTILLAS PLAN "+
                                                    "  WHERE SEC_PADRE_ID=? "+
                                                    "  AND  JER.SEC_PLANTILLA=PLAN.PLAN_ID "+
                                                    "  AND  JER.SEC_ACTIVA='S'" +
                                                    "  ORDER BY SEC_ORDEN ASC";
   public static final String SELECT_PAGINADO_SECCION="SELECT SEC_ID SECCIONID,\n"+
                                                      "       SEC_TITULO TITULO,\n"+
                                                      "       PREVVAL,\n"+
                                                      "       NEXTVAL \n"+
                                                      "FROM\n"+
                                                      "(\n" +
                                                      "  SELECT SEC_ID,\n" +
                                                      "         SEC_TITULO,\n" +
                                                      "         LAG(SEC_ID) over (order by SEC_ORDEN) PrevVal,\n" +
                                                      "         LEAD(SEC_ID) over (order by SEC_ORDEN) NextVal\n" +
                                                      "  FROM\n" +
                                                      "  (\n" +
                                                      "    SELECT SEC_ID,\n" +
                                                      "           SEC_TITULO, \n" +
                                                      "           SEC_ORDEN\n" +
                                                      "    FROM PM_JERARQUIA_SECCIONES \n" +
                                                      "    WHERE sec_padre_id=?\n"+
                                                      "    AND sec_activa='S'\n" +
                                                      "    ORDER BY sec_orden\n" +
                                                      "  )\n" +
                                                      ")\n" +
                                                      "WHERE SEC_ID=?";
   public static final String SELECT_SEC_PATH="SELECT SYS_CONNECT_BY_PATH(SEC_TITULO, '/') AS PATH_TITULO,\n" +
                                              "       SYS_CONNECT_BY_PATH(SEC_ID, '/') AS PATH_IDS,    \n" +
                                              "       SYS_CONNECT_BY_PATH(SEC_URL, '/') AS PATH_IDS    \n" +
                                              "  FROM PM_JERARQUIA_SECCIONES\n" +
                                              "  START WITH SEC_ID=?\n" +
                                              "  CONNECT BY PRIOR SEC_PADRE_ID=SEC_ID\n" +
                                              "  ORDER BY LEVEL DESC";

   public static final String SELECT_SEC_CHILDS="SELECT SEC_ID"+
                                              "  FROM PM_JERARQUIA_SECCIONES\n" +
                                              "  START WITH SEC_ID=?\n" +
                                              "  CONNECT BY PRIOR SEC_ID=SEC_PADRE_ID\n" ;

   public static final String UPDATE_JERARQUIA_ACTIVA= "UPDATE PM_JERARQUIA_SECCIONES\n" +
                                                "   SET \n" +
                                                "       SEC_ACTIVA = ?\n" +
                                                " WHERE SEC_ID IN ("+SELECT_SEC_CHILDS+")";
   public static final String UPDATE_JERARQUIA_ORDEN= "UPDATE PM_JERARQUIA_SECCIONES\n" +
                                                "   SET \n" +
                                                "       SEC_ORDEN = ?\n" +
                                                " WHERE SEC_ID = ?";
   public static final String UPDATE_JERARQUIA_PADRE= "UPDATE PM_JERARQUIA_SECCIONES\n" +
                                                "   SET \n" +
                                                "   SEC_ORDEN=("+SELECT_MAX_ORDEN+"),"+
                                                "       SEC_PADRE_ID = ?\n" +
                                                " WHERE SEC_ID = ?";

   public static final String SELECT_BEFORE_ORDEN= "SELECT  SEC_ID,SEC_ORDEN\n" +
                                                "FROM PM_JERARQUIA_SECCIONES\n" +
                                                "WHERE SEC_PADRE_ID=?\n" +
                                                "AND SEC_ORDEN<?\n"+
                                                "ORDER BY SEC_ORDEN DESC";

   public static final String SELECT_NEXT_ORDEN=" SELECT  SEC_ID,SEC_ORDEN\n" +
                                                "FROM PM_JERARQUIA_SECCIONES\n" +
                                                "WHERE SEC_PADRE_ID=?\n" +
                                                "AND SEC_ORDEN>?\n" +
                                                "ORDER BY SEC_ORDEN ASC";


   public static final String SELECT_SEC_CHIILDS_ORDEN=" SELECT *"+
                                                       " FROM PM_JERARQUIA_SECCIONES"+
                                                       " WHERE SEC_PADRE_ID=?"+
                                                       " ORDER BY SEC_ORDEN";

   public static final String DELETE_COMENTARIO=" DELETE PM_FORO_COMENTARIOS "+
                                                " WHERE FORO_ID=? "+
                                                " AND COMEN_ID=?";
   public static final String AUTORIZAR_COMENTARIO=" UPDATE PM_FORO_COMENTARIOS "+
                                                   " SET COMEN_AUTORIZADO=?"+
                                                   " WHERE FORO_ID=? "+
                                                   " AND COMEN_ID=?";


   public static final String DELETE_CONTACTO=" DELETE FROM PM_CONTACTO"+
                                              " WHERE CONTAC_ID=?";

   public static final String UPDATE_CONTACTO=" UPDATE PM_CONTACTO"+
                                              " SET CONTAC_ATENDIDO=?"+
                                              " WHERE CONTAC_ID=?";



   public static final String BUSCA_LICITACIONES= "SELECT ID_LICITACION,\n" +
                                                  "       DEPENDENCIA,\n" +
                                                  "       TRS,\n" +
                                                  "       LICITACION,\n" +
                                                  "       TO_CHAR(FEC_PUB,'DD/MM/YYYY') FEC_PUB,\n" +
                                                  "       TO_CHAR(FEC_LIM,'DD/MM/YYYY') FEC_LIM,\n" +
                                                  "       CLAVE_CABMS,\n" +
                                                  "       DESCRIPCION,\n" +
                                                  "       COSTO,\n" +
                                                  "       LIGA,\n" +
                                                  "       PARTIDAS,\n" +
                                                  "       TO_CHAR(FECHA_REG,'DD/MM/YYYY') FECHA_REG,\n" +
                                                  "       STATUS,\n" +
                                                  "       ID_PROCESO,\n" +
                                                  "       FAMILIA_ID_ANT,\n" +
                                                  "       DEPENDENCIA_ID\n" +
                                                  "  FROM RED_LICITACIONES_EXTERNAS\n" +
                                                  "WHERE SYSDATE<=FEC_LIM\n" +
                                                  "AND (UPPER(DEPENDENCIA) LIKE ?\n" +
                                                  "OR UPPER(DESCRIPCION) LIKE ?\n" +
                                                  ")"+
                                                  " ORDER BY DEPENDENCIA";

    public static final String SELECT_LICITACION="SELECT ID_LICITACION,\n" +
                                                  "       DEPENDENCIA,\n" +
                                                  "       TRS,\n" +
                                                  "       LICITACION,\n" +
                                                  "       TO_CHAR(FEC_PUB,'DD/MM/YYYY') FEC_PUB,\n" +
                                                  "       TO_CHAR(FEC_LIM,'DD/MM/YYYY') FEC_LIM,\n" +
                                                  "       CLAVE_CABMS,\n" +
                                                  "       DESCRIPCION,\n" +
                                                  "       COSTO,\n" +
                                                  "       LIGA,\n" +
                                                  "       PARTIDAS,\n" +
                                                  "       TO_CHAR(FECHA_REG,'DD/MM/YYYY') FECHA_REG,\n" +
                                                  "       STATUS,\n" +
                                                  "       ID_PROCESO,\n" +
                                                  "       FAMILIA_ID_ANT,\n" +
                                                  "       DEPENDENCIA_ID\n" +
                                                  "  FROM RED_LICITACIONES_EXTERNAS\n" +
                                                  " WHERE ID_LICITACION=?\n" ;
  public static final String BUSCA_AVISO= "SELECT  /*+ INDEX(A) INDEX(C) */\n" +
                                          " A.OPPORTUNITY_ID AVISOID, \n" +
                                          " A.OPPORTUNITY_SHORTDESC SHORTDESC, \n" +
                                          " A.OPPORTUNITY_DETAILSDESC DESCRIPTION, \n" +
                                          " TO_CHAR(A.OPPORTUNITY_DATEPUBLICATION,'DD/MM/YYYY') DATEPUB, \n" +
                                          " C.COMPANY_NOMBRE COMPANY, \n" +
                                          " C.COMPANY_EMAIL EMAIL, \n" +
                                          " C.COMPANY_TELEFONO PHONE \n" +
                                          "FROM POR_OPPORTUNITY A \n" +
                                          "INNER JOIN POR_COMPANY C ON A.COMPANY_ID=C.COMPANY_ID \n" +
                                          "WHERE A.OPPORTUNITY_TYPE=? \n" +
                                          "AND  A.OPPORTUNITY_AUTHORIZED=1\n" +
                                          "AND UPPER(A.OPPORTUNITY_SHORTDESC) LIKE UPPER(?) \n" +
                                          //"AND  A.OPPORTUNITY_DATEPUBLICATION>SYSDATE-60\n" +
                                          "ORDER BY A.OPPORTUNITY_DATEPUBLICATION DESC";
   public static final String AVISOS_RECIENTES= "SELECT * FROM \n" +
                                                "( \n" +
                                                "  SELECT  /*+ FIRST_ROWS(3) INDEX(A) INDEX(C) */\n" +
                                                "    A.OPPORTUNITY_ID AVISOID, \n" +
                                                "    A.OPPORTUNITY_SHORTDESC SHORTDESC, \n" +
                                                "    A.OPPORTUNITY_DETAILSDESC DESCRIPTION, \n" +
                                                "    TO_CHAR(A.OPPORTUNITY_DATEPUBLICATION,'DD/MM/YYYY') DATEPUB, \n" +
                                                "    C.COMPANY_NOMBRE COMPANY, \n" +
                                                "    C.COMPANY_EMAIL EMAIL, \n" +
                                                "    C.COMPANY_TELEFONO PHONE \n" +
                                                "  FROM POR_OPPORTUNITY A \n" +
                                                "  INNER JOIN POR_COMPANY C ON A.COMPANY_ID=C.COMPANY_ID \n" +
                                                "  WHERE A.OPPORTUNITY_TYPE=1\n" +
                                                "   AND  A.OPPORTUNITY_AUTHORIZED=1\n" +
                                                //"   AND  A.OPPORTUNITY_DATEPUBLICATION>SYSDATE-60\n" +
                                                "  ORDER BY A.OPPORTUNITY_DATEPUBLICATION DESC \n" +
                                                ") \n" +
                                                "WHERE ROWNUM<=2 \n" +
                                                "UNION ALL \n" +
                                                "SELECT * FROM \n" +
                                                "( \n" +
                                                "  SELECT  /*+ FIRST_ROWS(3) INDEX(A) INDEX(C) */\n" +
                                                "    A.OPPORTUNITY_ID AVISOID, \n" +
                                                "    A.OPPORTUNITY_SHORTDESC SHORTDESC, \n" +
                                                "    A.OPPORTUNITY_DETAILSDESC DESCRIPTION, \n" +
                                                "    TO_CHAR(A.OPPORTUNITY_DATEPUBLICATION,'DD/MM/YYYY') DATEPUB, \n" +
                                                "    C.COMPANY_NOMBRE COMPANY, \n" +
                                                "    C.COMPANY_EMAIL EMAIL, \n" +
                                                "    C.COMPANY_TELEFONO PHONE \n" +
                                                "  FROM POR_OPPORTUNITY A \n" +
                                                "  INNER JOIN POR_COMPANY C ON A.COMPANY_ID=C.COMPANY_ID \n" +
                                                "  WHERE A.OPPORTUNITY_TYPE=2\n" +
                                                "   AND  A.OPPORTUNITY_AUTHORIZED=1\n" +
                                                //"   AND  A.OPPORTUNITY_DATEPUBLICATION>SYSDATE-60\n" +
                                                "  ORDER BY A.OPPORTUNITY_DATEPUBLICATION DESC \n" +
                                                ") \n" +
                                                "WHERE ROWNUM<=2";

  public static final String BUSCA_AVISODETALLE="SELECT  /* INDEX(A) INDEX(C) */\n" +
                                                "   A.OPPORTUNITY_ID AVISOID, \n" +
                                                "   A.OPPORTUNITY_SHORTDESC SHORTDESC, \n" +
                                                "   A.OPPORTUNITY_DETAILSDESC DESCRIPTION, \n" +
                                                "   TO_CHAR(A.OPPORTUNITY_DATEPUBLICATION,'DD/MM/YYYY') DATEPUB, \n" +
                                                "   C.COMPANY_NOMBRE COMPANY, \n" +
                                                "   C.COMPANY_EMAIL EMAIL, \n" +
                                                "   C.COMPANY_TELEFONO PHONE \n" +
                                                " FROM POR_OPPORTUNITY A \n" +
                                                " INNER JOIN POR_COMPANY C ON A.COMPANY_ID=C.COMPANY_ID \n" +
                                                " WHERE A.OPPORTUNITY_TYPE=? \n" +//1
                                                "  AND A.OPPORTUNITY_ID=?  \n" + //2
                                                " AND  A.OPPORTUNITY_AUTHORIZED=1" ;

   public static final String SELECT_SEC_ID= " SELECT SEC_ID "+
                                             " FROM PM_JERARQUIA_SECCIONES"+
                                             " WHERE SEC_URL=?";


}
