package com.nafin.nafinetmovil.utils;


import java.text.*;
import java.io.*;
import java.util.*;


/**
 * La clase UsuarioBasico, permite establecer y obtener datos del usuario.
 * Es un bean que trabaja en conjunto con UtilUsr para consultar, agregar,
 * modificar y eliminar usuarios del OID (Oracle Internet Directory)
 *
 * @author	Gilberto E. Aparicio Guerrero
 */

public class UsuarioBasico implements Serializable, Comparable {

	private String cn; //Clave unica del usuario
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombre;
	private String rfc;
	private String mail;
	private String cp;
	private String ciudad;
	private String estado;
	private String calle;
	private String perfil;
	private String tipoAfiliado;
	private String cveAfiliado;

	public UsuarioBasico() {	}

	public String getCn() {
		return this.cn;
	}
	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}
	public String getNombre() {
		return this.nombre;
	}
	public String getRFC() {
		return this.rfc;
	}
	public String getMail() {
		return this.mail;
	}
	public String getCP() {
		return this.cp;
	}
	public String getCiudad() {
		return this.ciudad;
	}
	public String getEstado() {
		return this.estado;
	}
	public String getCalle() {
		return this.calle;
	}
	public String getPerfil() {
		return this.perfil;
	}
	public String getTipoAfiliado() {
		return this.tipoAfiliado;
	}
	public String getCveAfiliado() {
		return this.cveAfiliado;
	}


	public void setCn(String cn) {
		if (cn == null) {
			throw new RuntimeException("La clave del usuario (cn) no puede ser nula");
		}
		this.cn = cn;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setRFC(String rfc) {
		this.rfc = rfc;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public void setCP(String cp) {
		this.cp = cp;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}
	public void setCveAfiliado(String cveAfiliado) {
		this.cveAfiliado = cveAfiliado;
	}
	/**
	 * Dicta el orden natural de los objetos de esta clase.
	 *
	 * El ordenamiento se realiza con base en los apellidos y nombre de
	 * los usuarios
	 *
	 */

	public int compareTo(Object o) {

		UsuarioBasico usuario = (UsuarioBasico) o;
		//this.cn nunca es null
		return this.cn.compareTo(usuario.cn);
	}

	/**
	 * Determina la igualdad de dos usuarios de OID
	 * Se basa en la igualdad de los identificadores (cn)
	 * de los usuarios.
	 */
	public boolean equals(Object o) {
		UsuarioBasico usuario = null;

		if (o == null || this.cn == null) {
			return false;
		}

		try {
			usuario = (UsuarioBasico) o;
		} catch(ClassCastException e) {
			return false;
		}

		if (this.cn.equals(usuario.cn))	{
			return true;
		} else {
			return false;
		}
	}

	public int hashCode() {
		if (this.cn != null) {
			return cn.hashCode();	//regresa el hasCode de la cadena;
		} else {
			return 0;
		}
	}

	public static Comparator getComparator() {
		return new UsuarioBasico.Ordenamiento();
	}


	private static class Ordenamiento implements Comparator {

		public int compare(Object o1, Object o2) {
			Locale locale = new Locale ("es","MX");
			Collator collator = Collator.getInstance(locale);

			CollationKey key1 = null;
			CollationKey key2 = null;


			UsuarioBasico usuario1 = (UsuarioBasico) o1;
			UsuarioBasico usuario2 = (UsuarioBasico) o2;

			key1 = collator.getCollationKey(usuario1.apellidoPaterno + " " +
					usuario1.apellidoMaterno + " " + usuario1.nombre);

			key2 = collator.getCollationKey(usuario2.apellidoPaterno + " " +
					usuario2.apellidoMaterno + " " + usuario2.nombre);

			return key1.compareTo( key2 );
		}
	}
}


