package com.chermansolutions.argus.ws.client;

import java.net.URL;

import java.util.Properties;
import java.util.Vector;

import oracle.soap.transport.http.OracleSOAPHTTPConnection;

import org.apache.soap.Fault;
import org.apache.soap.SOAPException;
import org.apache.soap.encoding.SOAPMappingRegistry;
import org.apache.soap.rpc.Call;
import org.apache.soap.rpc.Parameter;
import org.apache.soap.rpc.Response;

/**
 * 
 * @author chermansolutions Ultima modificacion: Gerardo Hernandez Rojas gerardohr09@gmail.com
 */
public class AuditSessionsStub  {

  static Class class$java$lang$String; /* synthetic field */
  private String _endpoint;
  private OracleSOAPHTTPConnection m_httpConnection;
  private SOAPMappingRegistry m_smr;

  static Class _mthclass$(String a) {
    try {
      Class class1 = Class.forName(a);
      return class1;
    } catch(ClassNotFoundException e) {
      throw new NoClassDefFoundError(e.getMessage());
    }
  }

  public AuditSessionsStub() {
    _endpoint = "http://cherman01:8888/ArgusServices/AuditSessions";
    m_httpConnection = null;
    m_smr = null;
    m_httpConnection = new OracleSOAPHTTPConnection();
    m_smr = new SOAPMappingRegistry();
  }

  public static void main(String args[]) {
    AuditSessionsStub auditsessionsstub;
    
    try {
      auditsessionsstub = new AuditSessionsStub();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  public String getEndpoint() {
    return _endpoint;
  }

  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
  }
  
  public void clearAuditSSOSession(String username) throws Exception {
    URL endpointURL = new URL(_endpoint);
    
    Call call = new Call();
    
    call.setSOAPTransport(m_httpConnection);
    call.setTargetObjectURI("AuditSessions");
    call.setMethodName("clearAuditSSOSession");
    call.setEncodingStyleURI("http://schemas.xmlsoap.org/soap/encoding/");
    
    Vector params = new Vector();
    
    params.addElement(new Parameter("username", class$java$lang$String != null ? class$java$lang$String : (class$java$lang$String = _mthclass$("java.lang.String")), username, null));
    
    call.setParams(params);
    call.setSOAPMappingRegistry(m_smr);
    
    Response response = call.invoke(endpointURL, "");
    
    Parameter parameter;
    
    if(!response.generatedFault()) {
      parameter = response.getReturnValue();
    } else {
      Fault fault = response.getFault();
      throw new SOAPException(fault.getFaultCode(), fault.getFaultString());
    }
  }
  
  public void setMaintainSession(boolean maintainSession) {
    m_httpConnection.setMaintainSession(maintainSession);
  }
  
  public boolean getMaintainSession() {
    return m_httpConnection.getMaintainSession();
  }
  
  public void setTransportProperties(Properties props) {
    m_httpConnection.setProperties(props);
  }
  
  public Properties getTransportProperties() {
    return m_httpConnection.getProperties();
  }
}
