package com.chermansolutions.oracle.sso.partnerapp.beans;

import com.chermansolutions.argus.ws.client.AuditSessionsStub;

import java.net.InetAddress;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUtils;

import javax.sql.DataSource;

import oracle.security.sso.enabler.SSOEnabler;
import oracle.security.sso.enabler.SSOEnablerConfig;
import oracle.security.sso.enabler.SSOEnablerConfigMgr;
import oracle.security.sso.enabler.SSOEnablerException;
import oracle.security.sso.enabler.SSOEnablerUtil;
import oracle.security.sso.enabler.SSOUserInfo;

/**
 *
 * @author chermansolutions Ultima modificacion: Gerardo Hernandez Rojas gerardohr09@gmail.com
 */
public class SSOEnablerBean  {

  private String m_pappCookieName;
  
  public SSOEnablerBean() {
    m_pappCookieName = null;
  }
  
  public void setAppCookieInfo(String p_name) {
    m_pappCookieName = p_name;
  }
  
  public void setDbConnectionInfo(String s4, String s5, String s6, int k, String s7, int l) {
  }
  
  public String getSSOUserInfo(HttpServletRequest p_request, HttpServletResponse p_response, String datasource) throws SSOEnablerException {
    
    String l_userName = null;
  
    if(p_request == null || p_response == null) {
      throw new SSOEnablerException("Http objects are null");
    }
    
    Connection l_db_con = null;
  
    try {
      l_db_con = getConnection(datasource);

      SSOEnabler l_ssoEnabler = new SSOEnabler(l_db_con);

      l_userName = getUserInfo(p_request);

      System.out.println("getSSOUserInfo:l_userName: " + l_userName);

      String l_listener_token = getListenerToken(p_request);

      System.out.println("getSSOUserInfo:Listener token: " + l_listener_token);

      if(l_userName == null) {
  
        String l_requested_url = getRequestedUrl(p_request);
        System.out.println("getSSOUserInfo:getRequestedUrl: " + l_requested_url);
  
        String l_cancel_url = getCancelUrl(p_request);
        System.out.println("getSSOUserInfo:getCancelUrl: " + l_requested_url);
  
        String l_redirectUrl = l_ssoEnabler.generateRedirect(l_listener_token, l_requested_url, l_cancel_url, false);
        System.out.println("getSSOUserInfo:Genera Redirect: " + l_redirectUrl);
  
        p_response.sendRedirect(l_redirectUrl);
        l_userName = null;
  
      } else {
        l_userName = l_ssoEnabler.decryptCookie(l_listener_token, l_userName);
        System.out.println("Existe la cookie, getSSOUserInfo:l_ssoEnabler.decryptCookie --l_userName--: " + l_userName);
      }
    
      l_db_con.close();
      String s = l_userName;
      String s1 = s;
      String s2 = s1;
      
      return s2;
    } catch(Exception e) {
		e.printStackTrace();
      throw new SSOEnablerException(e.toString() + " >> " + getListenerToken(p_request));
    } finally {
      try {
        if(l_db_con != null) { l_db_con.close(); }
      } catch(Exception exception1) { }
    }
  }
  
  private String getUserInfo(HttpServletRequest p_request) throws SSOEnablerException {
    boolean l_gotPappCookie = false;
    String l_userInfo = null;
    
    if(m_pappCookieName == null) {
      throw new SSOEnablerException("Cookie name is null");
    }
    
    try {
      Cookie l_cookies[] = p_request.getCookies();
      int i = 0;
      
      do {
      
        if(i >= l_cookies.length) { break; }
        
        Cookie l_pappCookie = l_cookies[i];
        System.out.println("COOKIE: " + l_pappCookie.getName() + " COOKIE VALUE: " + l_pappCookie.getValue());
        System.out.println("Checa si existe cookie de la aplicaci\363n: " + m_pappCookieName);
        
        if(l_pappCookie.getName().equals(m_pappCookieName)) {        
          l_gotPappCookie = true;
          System.out.println("Valor de la cookie: " + m_pappCookieName);          
          l_userInfo = l_pappCookie.getValue();
          break;
        }
        
        i++;
      } while(true);
      
    } catch(Exception e) {
      String s = null;
      String s1 = s;
      String s2 = s1;
      return s2;
    }
    
    if(l_userInfo != null && l_userInfo.length() > 0) {
      return l_userInfo;
    } else {
      return null;
    }
  }
  
  public void setPartnerAppCookie(HttpServletRequest p_request, HttpServletResponse p_response, String datasource) throws SSOEnablerException {
    if(p_request == null || p_response == null) {
      throw new SSOEnablerException("Http objects are null");
    } if(m_pappCookieName == null) {
      throw new SSOEnablerException("Application cookie information is not available");
    }
    
    SSOUserInfo l_ssoUserInfo = null;
    Connection l_db_con = null;

    try {
      String l_urlParam = p_request.getParameterValues("urlc")[0];
      
      if(l_urlParam != null) {
        l_db_con = getConnection(datasource);
        
        SSOEnabler l_ssoEnabler = new SSOEnabler(l_db_con);
        
        String l_listener_token = getListenerToken(p_request);
        
        InetAddress l_clientIp = InetAddress.getByName(p_request.getRemoteAddr());
        
        l_ssoUserInfo = l_ssoEnabler.getSSOUserInfo(l_listener_token, l_urlParam, l_clientIp);
        
        String l_cookie_info = l_ssoUserInfo.getSSOUserName() + "/" + l_ssoUserInfo.getSubscriberName();
        String l_bakedAppCookie = l_ssoEnabler.encryptCookie(l_listener_token, l_cookie_info);
        
        l_db_con.close();
        
        System.out.println("setPartnerAppCookie:PARTNETAPP COOKIE (" + m_pappCookieName + "): " + l_cookie_info + " ENCRIPTADA: " + l_bakedAppCookie);
        
        Cookie l_AppCookie = new Cookie(m_pappCookieName, l_bakedAppCookie);
        
        l_AppCookie.setMaxAge(-1);
        p_response.addCookie(l_AppCookie);
        
        System.out.println("setPartnerAppCookie:sendRedirect: " + l_ssoUserInfo.getUrlRequested());
        p_response.sendRedirect(l_ssoUserInfo.getUrlRequested());
      } else {      
        try { if(l_db_con != null) l_db_con.close(); } catch(Exception exception) { }
        throw new SSOEnablerException("SSO server returned null user information");
      }
    } catch(Exception e) {
      throw new SSOEnablerException(e.toString());
    } finally {
      try { if(l_db_con != null) l_db_con.close(); } catch(Exception exception2) { }
    }
  }
  
  public void removeAppCookie(HttpServletResponse p_response) throws SSOEnablerException {
    
    if(p_response == null) throw new SSOEnablerException("HttpServletResponse is null");
    if(m_pappCookieName == null) throw new SSOEnablerException("Application cookie information is not available");
    
    try {
      Cookie l_AppCookie = new Cookie(m_pappCookieName, "");
      l_AppCookie.setMaxAge(0);
      p_response.addCookie(l_AppCookie);
      p_response.setContentType("image/gif");
      p_response.setHeader("Expires", "Thu, 29 Oct 2000 17:04:19 GMT");
      p_response.setHeader("Pragma", "no-cache");
      p_response.setHeader("Cache-Control", "no-cache");
      ServletOutputStream l_sos = p_response.getOutputStream();
      l_sos.write(SSOEnablerUtil.getLogoutImage());
      l_sos.flush();
    } catch(Exception e) {
      throw new SSOEnablerException(e.toString());
    }
  }
  
  public void removeCookies(HttpServletRequest request, HttpServletResponse response) throws SSOEnablerException {
    if(response == null) throw new SSOEnablerException("HttpServletResponse is null");
  
    try {
      Cookie cookies[] = request.getCookies();
      
      if(cookies != null) {
      
        for(int i = 0; i < cookies.length; i++) {
          String name = cookies[i].getName();
          String value = cookies[i].getValue();
          Cookie l_AppCookie = new Cookie(name, "");
          l_AppCookie.setMaxAge(0);
          response.addCookie(l_AppCookie);
        }
      
        response.setContentType("text/html; charset=UTF-8");
        response.setHeader("Expires", "Thu, 29 Oct 2000 17:04:19 GMT");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
      }
    } catch(Exception e) {
      throw new SSOEnablerException(e.toString());
    }
  }
  
  public void removeSession(String username) {
    try {
      AuditSessionsStub stub = new AuditSessionsStub();
      stub.setEndpoint(PartnerParams.urlRemoveSession);
      stub.clearAuditSSOSession(username);
    } catch(Exception e) {
      System.out.println(e.toString());
    }
  }
  
  public void removeAppCookieWI(HttpServletResponse p_response) throws SSOEnablerException {
    if(p_response == null) throw new SSOEnablerException("HttpServletResponse is null");
    if(m_pappCookieName == null) throw new SSOEnablerException("Application cookie information is not available");
  
    try
    {
      Cookie l_AppCookie = new Cookie(m_pappCookieName, "");
      l_AppCookie.setMaxAge(0);
      p_response.addCookie(l_AppCookie);
    } catch(Exception e) {
      throw new SSOEnablerException(e.toString());
    }
  }
  
  public String getRequestedUrl(HttpServletRequest p_request) {
    return HttpUtils.getRequestURL(p_request).toString();
  }
  
  private String getCancelUrl(HttpServletRequest p_request) {
    String l_server_name = p_request.getServerName();
    int l_port = p_request.getServerPort();
    String l_scheme = p_request.getScheme();
    String l_cancel_url = null;
  
    if(l_port == 80 || l_port == 443) {
      l_cancel_url = l_scheme + "://" + l_server_name;
    } else {
      l_cancel_url = l_scheme + "://" + l_server_name + ":" + l_port;
    }
  
    return l_cancel_url;
  }
  
  private String getListenerToken(HttpServletRequest p_request) {
    
    String l_server_name = p_request.getServerName();
    int l_port = p_request.getServerPort();
    
    if(l_port == 80 || l_port == 443) {
      return l_server_name;
    } else {
      return l_server_name + ":" + l_port;
    }
  }
  
  public void destroy() {
  }
  
  public String getSingleSignOffUrl(HttpServletRequest p_request, String datasource) throws SSOEnablerException {
    Connection l_db_con = null;
    
    try {
      l_db_con = getConnection(datasource);
      
      SSOEnablerConfigMgr l_cfgMgr = new SSOEnablerConfigMgr(l_db_con);
      SSOEnablerConfig l_cfg = l_cfgMgr.getEnablerConfig(getListenerToken(p_request));
      
      String s = l_cfg.getLogoutUrl();
      String s1 = s;
      String s2 = s1;
      
      return s2;
      
    } catch(Exception e) {
      throw new SSOEnablerException(e.toString());
    } finally {
      try { if(l_db_con != null) l_db_con.close(); } catch(Exception exception1) { }
    }
  }
  
  public static Connection getConnection(String datasource) throws SQLException {
    Connection connection = null;
    DataSource dataSource = null;
    
    try {
      
      if(dataSource == null) {
        dataSource = (DataSource)(new InitialContext()).lookup(datasource);
      }
      
      connection = dataSource.getConnection();
      
    } catch(NamingException nameException) {
      System.out.println("Error during DataSource lookup : " + nameException.toString());
    } catch(SQLException sqlException) {
      System.out.println("Error Connecting to Datasource : " + sqlException.toString());
    }
    
    return connection;
  }
  
  /**
   * Metodo que se utiliza para obtener la informacion de un sitio protegido por el SSO
   * @throws oracle.security.sso.enabler.SSOEnablerException Cuando no se puede establecer conexion a la base de datos del SSO u ocurre un error interno.
   * @return Un bean @see SiteInfoBean que contiene la informacion necesaria del sitio
   * @param datasource El nombre del data source para establecer conexion a la base de datos del SSO
   * @param request El request de HTTP de la pagina
   */
  public SiteInfoBean getSiteInfo(HttpServletRequest request, String datasource) throws SSOEnablerException {

    Connection cn = null;
    SiteInfoBean siteInfo = new SiteInfoBean();
    
    try {
      cn = getConnection(datasource);

      SSOEnabler ssoEnabler = new SSOEnabler(cn);
      
      String listenerToken = getListenerToken(request);
      
      String requested_url = getRequestedUrl(request);
      System.out.println("getSSOUserInfo:getRequestedUrl: " + requested_url);
  
      String cancel_url = getCancelUrl(request);
      System.out.println("getSSOUserInfo:getCancelUrl: " + cancel_url);
  
      String redirectUrl = ssoEnabler.generateRedirect(listenerToken, requested_url, cancel_url, false);

      String siteToken = redirectUrl.substring(redirectUrl.indexOf("v1."));
      
      siteInfo.setLoginURL(redirectUrl);
      siteInfo.setRequestedURL(requested_url);
      siteInfo.setCancelURL(cancel_url);
      siteInfo.setSiteToken(siteToken);
      
    } catch(Exception e) {
      throw new SSOEnablerException(e.getMessage());
    } finally {
      if(cn != null) try { cn.close(); } catch(Exception e) {}
      cn = null;
    }
    
    return siteInfo;
  }
}