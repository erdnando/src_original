package com.chermansolutions.oracle.sso.partnerapp.beans;

/**
 * Bean que sirve para mantener informacion del sitio para la autentificacion contra el SSO.
 * @author Gerardo Hernandez Rojas gerardohr09@gmail.com
 */
public class SiteInfoBean  {

  /**
   * Token del sitio para realizar la autentificacion contra el sso
   */
  private String siteToken;
  
  /**
   * URL del sitio (pagina) al que se redireccionara despues de la autentificacion
   */
  private String requestedURL;
  
  /**
   * URL del sitio (pagina) al que se redireccionara en caso de cancelar
   */
  private String cancelURL;
  
  /**
   * URL del sitio de SSO en el que se realizara el login.
   */
  private String loginURL;

  public SiteInfoBean() {
  }
  
  public SiteInfoBean(String siteToken, String requestedURL, String cancelURL, String loginURL) {
    this.siteToken = siteToken;
    this.requestedURL = requestedURL;
    this.cancelURL = cancelURL;
    this.loginURL = loginURL;
  }  


  public void setSiteToken(String siteToken) {
    this.siteToken = siteToken;
  }


  public String getSiteToken() {
    return siteToken;
  }


  public void setRequestedURL(String requestedURL) {
    this.requestedURL = requestedURL;
  }


  public String getRequestedURL() {
    return requestedURL;
  }


  public void setCancelURL(String cancelURL) {
    this.cancelURL = cancelURL;
  }


  public String getCancelURL() {
    return cancelURL;
  }


  public void setLoginURL(String loginURL) {
    this.loginURL = loginURL;
  }


  public String getLoginURL() {
    return loginURL;
  }
}