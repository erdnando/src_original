package com.chermansolutions.oracle.sso.partnerapp.beans;

import java.io.IOException;

import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 * @author chermansolutions Ultima modificacion: Gerardo Hernandez Rojas gerardohr09@gmail.com
 */
public class PartnerParams  {
  //Variable para enviar mensajes al log.
  private final static Log log = ServiceLocator.getInstance().getLog(PartnerParams.class);

  public static String cookieName;
  public static String datasource;
  public static String urlRemoveSession;
  
  public PartnerParams() {
  }
  
  public static Properties loadParams(String file) throws IOException {
  
    Properties prop = new Properties();
    String key = null;
    
    ResourceBundle bundle = ResourceBundle.getBundle(file);
    Enumeration enumx = bundle.getKeys();
    
    for(; enumx.hasMoreElements(); prop.put(key, bundle.getObject(key))) {
      key = (String)enumx.nextElement();
    }
    
    return prop;
  }
  
  static {
    Properties conValues = null;
      
    try {
      conValues = loadParams("partnerApp");
      cookieName = conValues.getProperty("cookieName");
      datasource = conValues.getProperty("datasource");
      urlRemoveSession = conValues.getProperty("urlremovesession");
    } catch(Exception ex) {
      log.error(" Error : No se puede leer el archivo de propiedades : ", ex);
    } finally {
      if(conValues != null) {
        conValues.clear();
        conValues = null;
      }
    }
  }
}