package com.chermansolutions.oracle.sso.partnerapp.beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.security.sso.enabler.SSOEnablerException;

/**
 *
 * @author chermansolutions Ultima modificacion: Gerardo Hernandez Rojas gerardohr09@gmail.com
 */
public class SSOEnablerJspBean  {

  private static String m_cookieName = "v1.2";
  private static String datasource = "jdbc/ssoAppsDS";
  private SSOEnablerBean m_enablerBean;

  public SSOEnablerJspBean() {
    m_cookieName = PartnerParams.cookieName;
    m_enablerBean = new SSOEnablerBean();
    m_enablerBean.setAppCookieInfo(m_cookieName);
    datasource = PartnerParams.datasource;
  }

  public String getSSOUserInfo(HttpServletRequest p_request, HttpServletResponse p_response) throws SSOEnablerException {
    return m_enablerBean.getSSOUserInfo(p_request, p_response, datasource);
  }

  public void setPartnerAppCookie(HttpServletRequest p_request, HttpServletResponse p_response) throws SSOEnablerException {
    m_enablerBean.setPartnerAppCookie(p_request, p_response, datasource);
  }

  public void removeJspAppCookies(HttpServletRequest p_request, HttpServletResponse p_response) throws SSOEnablerException {
    m_enablerBean.removeCookies(p_request, p_response);
  }

  public void removeSession(String username) throws SSOEnablerException {
    m_enablerBean.removeSession(username);
  }

  public void removeJspAppCookie(HttpServletResponse p_response) throws SSOEnablerException {
    m_enablerBean.removeAppCookie(p_response);
  }

  public void removeJspAppCookieWI(HttpServletResponse p_response) throws SSOEnablerException {
    m_enablerBean.removeAppCookieWI(p_response);
  }

  public String getSingleSignOffUrl(HttpServletRequest p_request) throws SSOEnablerException {
    return m_enablerBean.getSingleSignOffUrl(p_request, datasource);
  }

  public String getRequestedUrl(HttpServletRequest p_request) throws SSOEnablerException {
    return m_enablerBean.getRequestedUrl(p_request);
  }

  public void destroy() {
    m_enablerBean.destroy();
  }

  public String getDatasource() {
    return datasource;
  }

  public void setDatasource(String value) {
    datasource = "jdbc/" + value + "DS";
  }

  public SiteInfoBean getSiteInfo(HttpServletRequest p_request) throws SSOEnablerException {
    return m_enablerBean.getSiteInfo(p_request, datasource);
  }
}