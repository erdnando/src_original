package mx.gob.nafin.procesosexternos;


import com.netro.afiliacion.Afiliacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import netropology.utilerias.ServiceLocator;


public class ReafiliaAutIFEJBCliente{

	public static void main(String[] arg) {
		int codigoSalida = 0;
		try {
			procesoReafilia(arg[0]);
		} catch (Throwable e) {
			codigoSalida = 1;
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
	}

	static void procesoReafilia(String ruta) throws Throwable {
			StringBuffer log = new StringBuffer();
			String linea = "";
			String rutArchivo = "";
			System.out.println("Inicio Conexion");
			Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);
			log = afiliacion.reafiliaAutIfs(ruta);
			rutArchivo = log.toString().substring(0,log.toString().indexOf("|"));
			File flog = new File(rutArchivo);

			BufferedReader brt = new BufferedReader(new InputStreamReader(new FileInputStream(flog)));

			while((linea=brt.readLine())!=null) {
				System.out.println(linea);
			}
			System.out.println(log.toString().substring(log.toString().indexOf("|")));

			if(brt != null ) brt.close();
	} //fin main proceso
}