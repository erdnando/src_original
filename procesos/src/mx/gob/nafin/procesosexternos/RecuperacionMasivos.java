package mx.gob.nafin.procesosexternos;

import com.netro.garantias.Garantias;

import netropology.utilerias.ServiceLocator;

public class RecuperacionMasivos {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String 	ic_if 			= null;

		try {
			ic_if	= args[0];

			System.out.println("incio procesarRecuperacionDesembolsos:::::");
			Garantias garantias = obtenerEJB();
			System.out.println("llamdo a procesarRecuperacionDesembolsos:::::");
			garantias.procesarRecuperacionDesembolsos();
			System.out.println("finaliza procesarRecuperacionDesembolsos:::::");

		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("procesarRecuperacionDesembolsos [ic_if]");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("procesarRecuperacionDesembolsos::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{

			System.exit(codigoSalida);
		}
    }//main
    private static Garantias obtenerEJB() throws Throwable {
            System.out.println("Inicio Conexion");
            Garantias dispersionS = ServiceLocator.getInstance().remoteLookup("GarantiasEJB", Garantias.class);
            return dispersionS;
    }

}//RegistroSaldosFIDECliente