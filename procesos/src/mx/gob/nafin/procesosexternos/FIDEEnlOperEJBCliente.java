package mx.gob.nafin.procesosexternos;

import com.netro.credito.CargaCredito;
import com.netro.credito.FIDEEnlOperados;

import netropology.utilerias.ServiceLocator;

public class FIDEEnlOperEJBCliente {

	public static void main(String [] args)  {
	int codigoSalida = 0;
		String 	ic_if 			= null;

		try {
				ic_if			= args[0];

				CargaCredito cargaCredito = ServiceLocator.getInstance().remoteLookup("CargaCreditoEJB", CargaCredito.class);

				cargaCredito.procesoSolicitudes(new FIDEEnlOperados(ic_if));

		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("FIDEEnlOperEJBCliente [IF]");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("FIDEEnlOperEJBCliente::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		}finally{
			System.exit(codigoSalida);
		}
    }//main

}//FIDEEJBCliente
