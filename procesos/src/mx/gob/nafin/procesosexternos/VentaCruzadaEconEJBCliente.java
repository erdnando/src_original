package mx.gob.nafin.procesosexternos;

import com.netro.afiliacion.Afiliacion;

import netropology.utilerias.ServiceLocator;

public class VentaCruzadaEconEJBCliente {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String 	rutaTmp 			= null;

		try {
			rutaTmp	= args[0];

			System.out.println("incio VentaCruzadaEconEJBCliente:::::"+rutaTmp);
			Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);
			System.out.println("incio VentaCruzadaEconEJBCliente:::::"+rutaTmp);

			afiliacion.procesoBachVentaCruzadaEcon(rutaTmp);

		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("VentaCruzadaEconEJBCliente [rutaArch]");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("VentaCruzadaEconEJBCliente::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{

			System.exit(codigoSalida);
		}
    }//main

}//FIDEEnlOperTortillerosEJBCliente
