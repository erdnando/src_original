package mx.gob.nafin.procesosexternos;

/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	15/Julio/2004
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 042 - 2004, Plan de Pagos
*
*
*************************************************************************************/

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Calendar;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;


public class DisposicionesE
{
	public static void main(String args[])
	{
		String usuario = args[0];
		ResultSet rs;
		ResultSet rs1;
		ResultSet rs2;
		String cadena = "";

		CallableStatement cs=null;
		String sentenciaSQL;

		String folio = "", tipoRenta = null;
		double montoOperacion, montoAmortRec;
		int codigoSubaplicacion;
		long numPrestamo;
		java.sql.Date fechaAdicion = null;
		java.sql.Date fechaCaptura;
		java.sql.Time fechaHoraCaptura;

		String fechaTemp="";

		String estatus="";
		boolean errorDesconocido=false;

		String codigoAplicacion="";
		int existe;
		boolean errorTransaccion = false;
		String amortizaciones = null;
		int tipoPiso;
		long numeroSirac = 0;
		int codigoTipoPago=0;
		int codigoCuenta=0;


		String querySolicPortal = ""; //FODEA 011-2010 FVR

		String paramStore = "PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		AccesoDBOracle con = new AccesoDBOracle();
		try {
			System.out.println(":::: Antes de abrir la conexion ::::");
			con.conexionDB();
		} catch (Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try{
			sentenciaSQL = "select fecha_hoy as fechaAdicion from mg_calendario where codigo_aplicacion = 'BPR' AND CODIGO_EMPRESA = 1 ";
			try {
				System.out.println(":::: Antes de primer query ::::");
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("DisposicionesE.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			if (rs.next()) {
				fechaAdicion = rs.getDate("FECHAADICION");
			}
			rs.close();
			con.cierraStatement();

			sentenciaSQL = " SELECT /*+	ordered	index (S IN_COM_SOLIC_PORTAL_01_NUK) use_nl(s i int)*/ " +
						"   s.ic_solic_portal " +
						"   , i.ig_numero_prestamo " +
						"   , i.fn_monto_operacion " +
						"   , TO_CHAR (SYSDATE, 'yyyy-mm-dd') AS fechacaptura " +
						"   , s.df_carga AS horacaptura, trunc(SYSDATE) AS fechacapt " +
						"   , s.ic_tipo_credito " +
						"	, s.CS_TIPO_RENTA" +
						"	, SUM(decode(a.cg_tipo_amort, 'R',FN_MONTO, 0)) as FN_MONTO_AMORT_REC " +
						"	, int.ig_tipo_piso " +
						"	, int.ic_financiera " +
						"	, s.ig_clave_sirac " +
						"	, int.ic_if " +
						"	, i.ig_numero_linea_credito " +
						"	, i.ig_numero_contrato " +
						"	, 'DisposicionesE.java' as pantalla " +
						"     FROM com_solic_portal s " +
						"	, com_interfase i " +
						"	, com_amort_portal a " +
						"	, comcat_if int " +
						"    WHERE s.ic_solic_portal = i.ic_solic_portal " +
						"	   AND s.ic_solic_portal = a.ic_solic_portal (+) " +
						"	   AND s.ic_if = int.ic_if " +
						"      AND i.ig_numero_prestamo IS NOT NULL " +
						"      AND i.ig_numero_contrato IS NOT NULL " +
						"      AND i.ig_numero_linea_credito IS NOT NULL " +
						"      AND i.cc_codigo_aplicacion IS NULL " +
						"      AND i.ic_error_proceso IS NULL " +
						"	   AND i.cg_amortizacion in ('N', 'R') " +
						"      AND s.ic_estatus_solic = 2 " +
						"      AND s.ic_bloqueo = 3 " +
						"	  GROUP BY s.ic_solic_portal " +
						"		, i.ig_numero_prestamo " +
						" 		, i.fn_monto_operacion " +
						"		, s.df_carga "  +
						"		, s.ic_tipo_credito " +
						" 		, s.CS_TIPO_RENTA " +
						" 		, int.ig_tipo_piso " +
						"		, int.ic_financiera " +
						"		, s.ig_clave_sirac " +
						"		, int.ic_if " +
						"		, i.ig_numero_linea_credito " +
						"		, i.ig_numero_contrato";
			//System.out.println(sentenciaSQL);

			try {
				System.out.println(":::: Antes de segundo query ::::");
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("DisposicionesE.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()){
				errorDesconocido=false;
				errorTransaccion = false;
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_SOLIC_PORTAL");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				numPrestamo = rs.getLong("IG_NUMERO_PRESTAMO"); //F084-2006
				codigoSubaplicacion = rs.getInt("IC_TIPO_CREDITO");

				String sFechaCaptura = rs.getString("FECHACAPTURA");
				fechaCaptura = rs.getDate("FECHACAPT");
				fechaHoraCaptura = rs.getTime("FECHACAPT");
				//fechaAdicion = rs.getDate("FECHAADICION");
				tipoRenta = rs.getString("CS_TIPO_RENTA");
				montoAmortRec = rs.getDouble("FN_MONTO_AMORT_REC");

				//cs = con.ejecutaSP("PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("DisposicionesE.java(paramStore): " + paramStore );
					throw sqle;
				}

				cs.setInt(1, 90);								//01.- CODIGO_AGENCIA
				cs.setInt(2, codigoSubaplicacion);				//02.- CODIGO_SUB_APLICACION
				cs.setLong(3, numPrestamo);						//03.- NUMERO_PRESTAMO
				cs.setDate(4, fechaCaptura, Calendar.getInstance());					//04.- FECHA_CAPTURA
				cs.setDouble(5, montoOperacion);				//05.- VALOR_DESEMBOLSO
				cs.setString(6, usuario);						//06.- ADICIONADO_POR
				cs.setDate(7, fechaAdicion, Calendar.getInstance());					//07.- FECHA_ADICION (fecha del sistema)
				cs.setDouble(8, (double)0);						//08.- MONTO_AMORTIZACION_AJUSTE
				//09.- CODIGO_TIPO_PAGO
				//10.- CODIGO_CUENTA
				if (tipoPiso == 1) {
					numeroSirac = (rs.getInt("IC_IF")== 12)? rs.getLong("IG_CLAVE_SIRAC"): rs.getLong("IC_FINANCIERA");
					String tipoCliente = (rs.getInt("IC_IF")== 12)? "P":"I";

						String queri2 = "SELECT IG_TIPO_PAGO, IG_CODIGO_CUENTA "+
									" FROM comcat_tipo_pago " +
									" WHERE IG_NUMERO_SIRAC = " + numeroSirac +
									" AND CG_TIPO_CLIENTE = '" + tipoCliente + "'";
						try {
							rs2=con.queryDB(queri2);
						} catch(SQLException sqle) {
							System.out.println("DisposicionesE.java(queri2): " + queri2 );
							throw sqle;
						}
						if (rs2.next()) {
							codigoTipoPago = rs2.getInt("IG_TIPO_PAGO");
							codigoCuenta = rs2.getInt("IG_CODIGO_CUENTA");
						}
						rs2.close();
						con.cierraStatement();
					cs.setInt(9, codigoTipoPago);
					cs.setInt(10, codigoCuenta);


				} else {
					cs.setNull(9, Types.INTEGER);
					cs.setNull(10, Types.INTEGER);
				}

				cs.setDouble(11, montoOperacion);				//11.- VALOR_PAGO
				cs.setNull(12, Types.INTEGER);					//12.- NUMERO_DE_CHEQUE
				cs.setNull(13, Types.INTEGER);					//13.- CODIGO_EMPRESA_PAGO
				cs.setNull(14, Types.INTEGER);					//14.- CODIGO_AGENCIA_PAGO
				cs.setNull(15, Types.INTEGER);					//15.- CODIGO_SUB_APLICACION_PAGO
				cs.setNull(16, Types.INTEGER);					//16.- PRESTAMO_PAGO
				cs.setNull(17, Types.INTEGER);					//17.- PORCENTAJE_PAGO
				cs.setNull(18, Types.INTEGER);					//18.- TIPO_PAGO_2
				cs.setNull(19, Types.INTEGER);					//19.- CODIGO_CUENTA_PAGO

																//20.- SPREAD
																//21.- Relación Matemática
				if ("S".equals(tipoRenta)){
					cs.setDouble(20, 1);
					cs.setString(21, "*");
				} else {
					cs.setNull(20, Types.DOUBLE);
					cs.setString(21, null);
				}

																//22.- Monto Amortizaciones No Recortadas
				if ("N".equals(tipoRenta)){
					cs.setDouble(22, montoAmortRec);
				} else {
					cs.setNull(22, Types.INTEGER);
				}

				cs.registerOutParameter(23, Types.INTEGER);		//23.- ESTATUS (out)

				cadena = "p1:=90;"+"\n"+
					"p2:="+codigoSubaplicacion+";"+"\n"+
					"p3:="+numPrestamo+";"+"\n"+
					"p4:=to_date('"+fechaCaptura+"','yyyy-mm-dd');"+"\n"+
					"p5:="+montoOperacion+";"+"\n"+
					"p6:='"+usuario+"';"+"\n"+
					"p7:=to_date('"+fechaAdicion+"','yyyy-mm-dd');"+"\n"+
					"p8:=0;"+"\n";
				if (tipoPiso == 1) {
					cadena += "p9:="+codigoTipoPago+";"+"\n"+
						"p10:="+codigoCuenta+";"+"\n";
				} else {
					cadena += "p9:=null;"+"\n"+
						"p10:=null;"+"\n";
				}
				cadena += "p11:="+montoOperacion+";"+"\n"+
					"p12:=null;"+"\n"+
					"p13:=null;"+"\n"+
					"p14:=null;"+"\n"+
					"p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n"+
					"p18:=null;"+"\n"+
					"p19:=null;"+"\n";
				if ("S".equals(tipoRenta)){
					cadena += "p20:=1;"+"\n"+
						"p21:='*';"+"\n";
				} else {
					cadena += "p20:=null;"+"\n"+
						"p21:=null;"+"\n";
				}
				if ("N".equals(tipoRenta)){
					cadena += "p22:="+montoAmortRec+";"+"\n";
				} else {
					cadena += "p22:=null;"+"\n";
				}


					cadena += "p23:=null;"+"\n";

				System.out.println(cadena);

				try {
					System.out.println(":::: Antes de ejecutar el PS ::::");
					cs.execute();
						estatus = cs.getString(23);			//Lo que regresa debe ser un entero o nulo...
						//fechaTemp = cs.getString(14);			//Lo que regresa debe ser un entero o nulo...
						//System.out.println("La fecha temp: " + fechaTemp);
						if (estatus == null) {
							System.out.println("estatus fue nulo");
							errorDesconocido = true;
							estatus="8";
						} else {
							System.out.println("Estatus: "+estatus);
							cadena += "Estatus: "+estatus;
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}

				if(!estatus.equals("0")) {		//Error
					amortizaciones = ("N".equals(tipoRenta))?"'S'": "cg_amortizacion";

					con.terminaTransaccion(false);
					if (!errorDesconocido)
					{
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='DS'";
						//System.out.println(sentenciaSQL+"/n");
						try {
							System.out.println(":::: Antes de tercer query ::::");
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("DisposicionesE.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_interfase where ic_solic_portal='"+folio+"'";
					try {
						System.out.println(":::: Antes de cuarto query ::::");
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("DisposicionesE.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) { // Se valida la concurrencia
						sentenciaSQL = "update com_interfase " +
								"	set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
								" 	, ic_error_proceso="+estatus+
								"   , df_hora_proceso=SYSDATE"+
								"   , ig_proceso_numero = 4"+
								"	, cg_amortizacion = " + amortizaciones +
								"   , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where ic_solic_portal="+folio;

						//FODEA 011-2010 FVR-INI
						querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET ic_error_proceso = ? " +
							" , cc_codigo_aplicacion = ? "+
							" WHERE ic_solic_portal = ? ";
						//FODEA 011-2010 FVR-FIN
					}//fin de else

					try {
						System.out.println(":::: Antes de quinto query ::::");
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println(sentenciaSQL+"/n");
						System.out.println("Error al Actualizar los Datos de error en com_interfase.");
						errorTransaccion = true;
					}

					//FODEA 011-2010 FRV-INI
					try {
						PreparedStatement ps = con.queryPrecompilado(querySolicPortal);
						ps.setInt(1, Integer.parseInt(estatus));
						ps.setString(2,codigoAplicacion);
						ps.setInt(3, Integer.parseInt(folio));
						ps.executeUpdate();
						ps.close();
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_solic_portal");
						errorTransaccion = true;
					}
					//FODEA 011-2010 FRV-FIN

				} else {	//no hubo error  (error=0)

					sentenciaSQL = "update com_interfase " +
							"	set df_hora_proceso=SYSDATE"+
							"   , ig_proceso_numero = 4"+
							"   , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" where ic_solic_portal="+folio;
					try {
						System.out.println(":::: Antes de sexto query ::::");
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_solic_portal set ic_estatus_solic = 3"+ //3 = Operada
							" , ic_bloqueo = null "+
							" , ig_numero_prestamo = "+numPrestamo+
							" , ig_numero_linea_credito = " +rs.getLong("ig_numero_linea_credito") + //F084-2006
							" , ig_numero_contrato = " + rs.getLong("ig_numero_contrato")+ //F084-2006
							" , df_operacion = (select FECHA_APERTURA from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
              //" , ig_base_operacion = (select codigo_base_operacion from pr_prestamos_2 where NUMERO_PRESTAMO = "+numPrestamo+") "+//FODEA 013 - 2009 ACF - Se cambio al Api de Proyectos
              " , ic_estado = (select codigo_departamento from mg_clientes where CODIGO_CLIENTE = "+numeroSirac+") "+//FODEA 013 - 2009 ACF
              " , cs_tipo_persona = (select decode(tipo_de_persona, 'J', 'M', 'N', 'F') from mg_clientes where CODIGO_CLIENTE = "+numeroSirac+") "+//FODEA 013 - 2009 ACF
							" where ic_solic_portal = "+folio;

						try {
							System.out.println(":::: Antes de septimo query ::::");
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al actualizar estatus de solicitud final");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar estatus interfase final");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
		} catch(Exception e) {//fin try
			System.out.println("Error: "+e);
			e.printStackTrace();
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			System.out.println(":::: finally ::::");
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
				System.out.println(":::: finally cierra conexion ::::");
			}
		}
	} //fin del main
}	//fin clase
