package mx.gob.nafin.procesosexternos;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import netropology.utilerias.AccesoDBOracle;


public class DiversosE {
	public static void main(String args[]) {
		boolean ejecutar;

		ResultSet rs;
		ResultSet rs1;
		ResultSet rs2;
		String sentenciaSQL;
		String sentenciaSQL1;
		String sentenciaSQL2;
		String tipoCambioDl = args[0];

		String folio = "";
		String importeRecibir = "";
		String importeRecibirCalc = "";

		int codigoFinanciera;
		int tipoFinanciera;
		int numLineasInter;
		int numVigentes;
		int numDisponibValidas;
		int plazo = 0;
		int icIF = 0;

		boolean errorDiversos = false;
		boolean errorTransaccion = false;

		String facultades="";
		String aprobadoPor="";
		String descripcion="";
		String estatus="";
		String codigoAgencia="";
		String codigoSubAplicacion="";
		String numLineaFondeo="";
		String strProducto = null;
		String strCsFacultades = null;

		String icErrorProceso = "";//FODEA 011-2010 FVR
		String querySolicPortal = "";//FODEA 011-2010 FVR

		AccesoDBOracle con = new AccesoDBOracle();

		try {
			con.conexionDB();
		} catch(Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try {

			sentenciaSQL = "select /*+ ordered use_nl(s,if,fi) index(s IN_COM_SOLIC_PORTAL_01_NUK) */ " +
							"	 s.ic_solic_portal " +
							"    , s.ic_if " +
							"    , if.ic_financiera " +
							"	 , fi.ic_tipo_financiera " +
							"    , s.fn_importe_dscto " +
							"    , s.fn_importe_docto " +
							"	 , s.ic_tabla_amort " +
							"	 , s.in_numero_amort_rec " +
							"	 , if.ic_if " +
							"	 , s.IC_TIPO_CREDITO " +
							"	 , s.cs_tipo_plazo " +
							"	 , NVL(if.CS_FACULTADES,'N') as CS_FACULTADES " +
							"	 , if.IC_MONEDA_FACULTADES " +
							"	 , if.FN_MONTO_FACULTADES " +
							"    , DECODE(s.ic_moneda,54, ROUND((s.fn_importe_dscto * "+tipoCambioDl+"),2), s.fn_importe_dscto) fn_importe_dscto_calc " +
							"    , DECODE(s.ic_moneda,54, ROUND((s.fn_importe_docto * "+tipoCambioDl+"),2), s.fn_importe_docto) fn_importe_docto_calc  " +
							" FROM com_solic_portal s " +
							"    , comcat_if if " +
							"    , comcat_financiera fi " +
							" WHERE s.ic_estatus_solic = 1  " +
							"    AND s.ic_bloqueo is null " +
							"	AND s.ic_if = if.ic_if " +
							"	AND if.ic_financiera = fi.ic_financiera";

//				System.out.println(sentenciaSQL);
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
				throw sqle;
			}


			while(rs.next()) {
				errorDiversos = false;
				errorTransaccion = false;

				folio = rs.getString("IC_SOLIC_PORTAL");
				codigoFinanciera = rs.getInt("IC_FINANCIERA");
				tipoFinanciera = rs.getInt("IC_TIPO_FINANCIERA");
//					importeRecibir = rs.getString("FN_IMPORTE_DSCTO");
				importeRecibir = (rs.getInt("IN_NUMERO_AMORT_REC") == 0)? rs.getString("FN_IMPORTE_DSCTO") : rs.getString("FN_IMPORTE_DOCTO");
				importeRecibirCalc = (rs.getInt("IN_NUMERO_AMORT_REC") == 0)? rs.getString("FN_IMPORTE_DSCTO_CALC") : rs.getString("FN_IMPORTE_DOCTO_CALC");
				icIF = rs.getInt("IC_IF");
				strProducto = "0"+rs.getString("CS_TIPO_PLAZO");
				strCsFacultades = rs.getString("CS_FACULTADES");

//					System.out.println(codigoFinanciera);
				if ( icIF == 12) {
					descripcion="Ingresado por Nafin electr�nico";
					codigoAgencia = "90";
					codigoSubAplicacion = rs.getString("IC_TIPO_CREDITO");

					System.out.println("Se marca la solicitud "+folio+" como 'En proceso' 1er Piso");
					//ic_estatus_solic = 2   EN PROCESO
					//ic_bloqueo = 3   EN PROCESO POR INTERFASE
					sentenciaSQL = "update com_solic_portal set ic_estatus_solic = 2, ic_bloqueo = 3"+
						" where ic_solic_portal = '"+folio+"'";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "insert into com_interfase (ic_solic_portal, ig_codigo_agencia"+
							" , ig_codigo_sub_aplicacion , cg_descripcion, fn_monto_operacion, df_hora_proceso, CC_PRODUCTO)"+
							" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
							" , '"+descripcion+"',"+importeRecibir+", " + " SYSDATE, '"+strProducto+"')";
//									System.out.println(sentenciaSQL);
						con.ejecutaSQL(sentenciaSQL);
						con.terminaTransaccion(true);
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}

				} else {

						//F014-2011(E)
						String numLineaFondeoIF ="";
							sentenciaSQL =
								"  SELECT  IG_NUMERO_LINEA_FONDEO   FROM  COMREL_PRODUCTO_IF "+
								" WHERE IC_IF = "+icIF+
								" and ic_producto_nafin =0 ";	//0 Credito Electronico
								rs1 = con.queryDB(sentenciaSQL);
							  if(rs1.next())		{
									numLineaFondeoIF = rs1.getString("IG_NUMERO_LINEA_FONDEO")==null?"":rs1.getString("IG_NUMERO_LINEA_FONDEO");

								}else{
									numLineaFondeoIF ="";
								}
								rs1.close();
								con.cierraStatement();
							//F014-2011(S)

					sentenciaSQL = "SELECT count(1) numLineasInter" +
						" FROM LC_LINEAS_FONDEO_INTER" +
						" WHERE tipo_financiera = " + tipoFinanciera +
						" AND codigo_financiera = " + codigoFinanciera;
						if(!numLineaFondeoIF.equals("")){
							sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
						}

				//System.out.println(sentenciaSQL);
					try {
						rs1 = con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
						throw sqle;
					}
					rs1.next();
					numLineasInter = rs1.getInt("NUMLINEASINTER");
					rs1.close();
					con.cierraStatement();

					if (numLineasInter < 1)	{ //No hay Lineas de intermediario
						System.out.println("Error codigo 1\nic_estatus_solic=1 y ic_bloqueo=2");
						sentenciaSQL = "insert into com_interfase "+
							" (ic_solic_portal, ic_error_proceso, cc_codigo_aplicacion, df_hora_proceso, CC_PRODUCTO)"+
							" values ('"+folio+"',1,'NE', SYSDATE, '"+strProducto+"')";
						try {
							con.ejecutaSQL(sentenciaSQL);
							sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
								" where ic_solic_portal='"+folio+"'";
							con.ejecutaSQL(sentenciaSQL);
							icErrorProceso = "1";//FODEA 011-2010 FVR
							errorDiversos = true;
						} catch(SQLException sqle) {
							errorTransaccion = true;
						}
					} else {//Si hay L�neas de intermediario
						sentenciaSQL = "SELECT count(1) as numVigentes" +
							" FROM LC_LINEAS_FONDEO_INTER" +
							" WHERE fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
							" AND tipo_financiera = " + tipoFinanciera +
							" AND codigo_financiera = " + codigoFinanciera;
							if(!numLineaFondeoIF.equals("")){
							sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
						}

						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
							throw sqle;
						}
						rs1.next();
						numVigentes = rs1.getInt("NUMVIGENTES");
						rs1.close();
						con.cierraStatement();

						if (numVigentes <1) { //No hay Lineas vigentes
							System.out.println("Error codigo 2\nic_estatus_solic=1 y ic_bloqueo=2");
							sentenciaSQL = "insert into com_interfase "+
								" (ic_solic_portal, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
								" values ('"+folio+"', SYSDATE, 2, 'NE', '"+strProducto+"')";
							try {
								con.ejecutaSQL(sentenciaSQL);
								sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
									" where ic_solic_portal='"+folio+"'";
								con.ejecutaSQL(sentenciaSQL);
								icErrorProceso = "2";//FODEA 011-2010 FVR
								errorDiversos = true;
							} catch(SQLException sqle) {
								errorTransaccion = true;
							}
						}
						else	//Si hay l�neas vigentes
						{
							sentenciaSQL =
								"SELECT * FROM ("+
								"	SELECT l.codigo_agencia, l.codigo_sub_aplicacion"+
								" 	, l.numero_linea_fondeo"+
								" 	, (l.monto_asignado - (nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))) as disponible" +
								" 	FROM LC_LINEAS_FONDEO_INTER l" +
								" 	WHERE l.fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
								" 	AND l.tipo_financiera = " + tipoFinanciera +
								" 	AND l.codigo_financiera = " + codigoFinanciera +
								" 	AND l.codigo_estado = 1 "+
								" ) WHERE disponible >=	"+importeRecibirCalc;
								if(!numLineaFondeoIF.equals("")){
									sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
								}
								sentenciaSQL +=	" 	AND rownum = 1 " +
								" order by disponible";
							try {
								rs1 = con.queryDB(sentenciaSQL);
							} catch(SQLException sqle) {
								System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
								throw sqle;
							}
							numDisponibValidas = 0;

							while(rs1.next())
							{
								numDisponibValidas++;
								codigoAgencia = rs1.getString("CODIGO_AGENCIA");
								codigoSubAplicacion = rs1.getString("CODIGO_SUB_APLICACION");
								numLineaFondeo = rs1.getString("NUMERO_LINEA_FONDEO");
								//Este instrucci�n se aplica ya que nos interesa �nicamente el
								//primer registro (El de disponilidad menor)
								continue;
							}
							rs1.close();
							con.cierraStatement();
							//codigoAgencia = "1";
							//codigoSubAplicacion = "1";
							//numLineaFondeo = "1";
							//numDisponibValidas = 1;

							if (numDisponibValidas<1)	//Verif. Monto vs Disponibilidad
							{
								System.out.println("Error codigo 3\nic_estatus_solic=1 y ic_bloqueo=2");
								sentenciaSQL = "insert into com_interfase "+
									" (ic_solic_portal, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
									" values ('"+folio+"', SYSDATE, 3, 'NE', '"+strProducto+"')";
								try {
									con.ejecutaSQL(sentenciaSQL);
									sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
										" where ic_solic_portal='"+folio+"'";
									con.ejecutaSQL(sentenciaSQL);
									icErrorProceso = "3";//FODEA 011-2010 FVR
									errorDiversos = true;
								} catch(SQLException sqle) {
									errorTransaccion = true;
								}
							}
							else	//Si hay disponibilidad >= importe recibir
							{
//								System.out.println("Si hay disponibilidad");
								System.out.println("Codigo Agencia="+codigoAgencia);
								System.out.println("Codigo sub aplicacion="+codigoSubAplicacion);
								System.out.println("Codigo num L�nea fondeo="+numLineaFondeo);

								sentenciaSQL = "select tipo_financiera, codigo_financiera" +
									" , facultades " +
									" from mg_financieras" +
									" where tipo_financiera = " + tipoFinanciera+
									" AND codigo_financiera = " + codigoFinanciera;
								try {
									rs1 = con.queryDB(sentenciaSQL);
								} catch(SQLException sqle) {
									System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
									throw sqle;
								}
								if(rs1.next()) {	//Hay aprobaciones intermediarios?
									facultades = rs1.getString ("FACULTADES");
								} else {
									System.out.println("Error codigo 4\nic_estatus_solic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into com_interfase "+
										" (ic_solic_portal, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
										" values ('"+folio+"', SYSDATE, 4, 'NE', '"+strProducto+"')";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
											" where ic_solic_portal = '"+folio+"'";
										con.ejecutaSQL(sentenciaSQL);
										icErrorProceso = "4";//FODEA 011-2010 FVR
										errorDiversos = true;
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								}
								rs1.close();
								con.cierraStatement();

								if (new BigDecimal(facultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {	//facultades < importeRecibirCalc?
									//Buscar aprobaciones por rangos
									sentenciaSQL = "SELECT codigo_aprobacion, cupo_minimo, cupo_limite"+
										" FROM mg_aprobacion"+
										" WHERE "+importeRecibirCalc+" between cupo_minimo and cupo_limite ";
									//System.out.println(sentenciaSQL);
									try {
										rs1 = con.queryDB(sentenciaSQL);
									}	catch(SQLException sqle) {
										System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
										throw sqle;
									}
									if(rs1.next())	//No est� fuera de rango?
									{
										if ("S".equals(strCsFacultades)) {
											sentenciaSQL = "SELECT ROUND(fn_valor_compra * "+rs.getDouble("FN_MONTO_FACULTADES")+ ",2) "+
													" FROM com_tipo_cambio " +
													" WHERE ic_moneda = " + rs.getInt("IC_MONEDA_FACULTADES") +
													" AND ROWNUM = 1 ORDER BY dc_fecha DESC";
											//System.out.println(sentenciaSQL);
											try {
												rs2 = con.queryDB(sentenciaSQL);
											}	catch(SQLException sqle) {
												System.out.println("DiversosE.java(sentenciaSQL): "+ sentenciaSQL);
												throw sqle;
											}
											String montoFacultades = "0";
											if(rs2.next()) {
												montoFacultades = rs2.getString(1);
											}
											rs2.close();
											con.cierraStatement();
											if (new BigDecimal(montoFacultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {

												System.out.println("Error codigo 14\n\nic_estatus_solic=1 y ic_bloqueo=2");

												sentenciaSQL = "insert into com_interfase "+
													" (ic_solic_portal, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
													" values ('"+folio+"', SYSDATE, 14, 'NE', '"+strProducto+"')";
												try {
													con.ejecutaSQL(sentenciaSQL);
													sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
														" where ic_solic_portal = '"+folio+"'";
													con.ejecutaSQL(sentenciaSQL);
													icErrorProceso = "14";//FODEA 011-2010 FVR
													errorDiversos = true;
												} catch(SQLException sqle) {
													errorTransaccion = true;
												}
											}else {
												aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
												descripcion = "Descripci�n especial";
												System.out.println("Error codigo 7\n codigo_aprobacion"+aprobadoPor+"\nic_estatus_solic=2 y ic_bloqueo=3");

												sentenciaSQL = "insert into com_interfase (ic_solic_portal, ig_codigo_agencia"+
													" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
													" , ig_aprobado_por, cg_descripcion, fn_monto_operacion " +
													" , df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO) "+
													" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
													" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
													" , '"+descripcion+"',"+importeRecibir +", SYSDATE, 7, 'NE', '"+strProducto+"')";
												try {
													con.ejecutaSQL(sentenciaSQL);
													sentenciaSQL = "update com_solic_portal set ic_estatus_solic = 2, ic_bloqueo=3"+
														" where ic_solic_portal = '"+folio+"'";
													con.ejecutaSQL(sentenciaSQL);
													icErrorProceso = "7";//FODEA 011-2010 FVR
													errorDiversos = true;
												} catch(SQLException sqle) {
													errorTransaccion = true;
												}
											}
										} else {
											aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
											descripcion = "Descripci�n especial";
											System.out.println("Error codigo 7\n codigo_aprobacion"+aprobadoPor+"\nic_estatus_solic=2 y ic_bloqueo=3");

											sentenciaSQL = "insert into com_interfase (ic_solic_portal, ig_codigo_agencia"+
												" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
												" , ig_aprobado_por, cg_descripcion, fn_monto_operacion " +
												" , df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO) "+
												" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
												" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
												" , '"+descripcion+"',"+importeRecibir +", SYSDATE, 7, 'NE', '"+strProducto+"')";
											try {
												con.ejecutaSQL(sentenciaSQL);
												sentenciaSQL = "update com_solic_portal set ic_estatus_solic = 2, ic_bloqueo=3"+
													" where ic_solic_portal = '"+folio+"'";
												con.ejecutaSQL(sentenciaSQL);
												icErrorProceso = "7";//FODEA 011-2010 FVR
												errorDiversos = true;
											} catch(SQLException sqle) {
												errorTransaccion = true;
											}
										}

									}
									else
									{
										System.out.println("Error codigo 5\nic_estatus_solic=1 y ic_bloqueo=2");
										sentenciaSQL = "insert into com_interfase "+
											" (ic_solic_portal, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
											" values ('"+folio+"', SYSDATE, 5, 'NE', '"+strProducto+"')";
										try {
											con.ejecutaSQL(sentenciaSQL);
											sentenciaSQL = "update com_solic_portal set ic_bloqueo=2"+
												" where ic_solic_portal = '"+folio+"'";
											con.ejecutaSQL(sentenciaSQL);
											icErrorProceso = "5";//FODEA 011-2010 FVR
											errorDiversos = true;
										} catch(SQLException sqle) {
											errorTransaccion = true;
										}
									}
									rs1.close();
									con.cierraStatement();
								}//fin de facultades < importeRecibirCalc?
								else
								{
									aprobadoPor="NULL";
									descripcion="Ingresado por Nafin electr�nico";
									//estatus = "S";
								}

								if (!errorDiversos) {
									System.out.println("Se marca la solicitud "+folio+" como 'En proceso' ---- NumError "+icErrorProceso);
									//System.out.println("Aprobado por: "+aprobadoPor);
									//System.out.println("Descripcion: "+descripcion);
									//System.out.println("Estatus: "+estatus);
									//ic_estatus_solic = 2   EN PROCESO
									//ic_bloqueo = 3   EN PROCESO POR INTERFASE
									sentenciaSQL = "update com_solic_portal set ic_estatus_solic = 2, ic_bloqueo = 3"+
										" where ic_solic_portal = '"+folio+"'";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "insert into com_interfase (ic_solic_portal, ig_codigo_agencia"+
											" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
											" , ig_aprobado_por, cg_descripcion, fn_monto_operacion, df_hora_proceso, df_diversos, CC_PRODUCTO)"+
											" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
											" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
											" , '"+descripcion+"',"+importeRecibir+", " + " SYSDATE, SYSDATE, '"+strProducto+"')";
	//									System.out.println(sentenciaSQL);
										con.ejecutaSQL(sentenciaSQL);
										con.terminaTransaccion(true);
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								}
							}//fin else Si hay disponibilidad >= importe recibir
						}//fin else Si hay l�neas vigentes
					}//fin else Si hay L�neas de intermediario

					//FODEA 011-2010 FVR-INI
					if(errorDiversos){
						querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET ic_error_proceso = ? " +
							" , cc_codigo_aplicacion = ? "+
							" WHERE ic_solic_portal = ? ";


						try {
							System.out.println("querySolicPortal>>>>>><"+querySolicPortal);
							PreparedStatement ps = con.queryPrecompilado(querySolicPortal);
							ps.setInt(1, Integer.parseInt(icErrorProceso));
							ps.setString(2,"NE");
							ps.setInt(3, Integer.parseInt(folio));
							ps.executeUpdate();
							ps.close();
							con.terminaTransaccion(true);
						} catch(SQLException sqle) {
							System.out.println("Error al Actualizar los Datos de error en com_solic_portal");
							errorTransaccion = true;
						}
					}
					//FODEA 011-2010 FVR-FIN
					if (errorDiversos && !errorTransaccion ) {
						con.terminaTransaccion(true);
					} else {
						con.terminaTransaccion(false);
					}
				}
			}//fin while
		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			System.out.println(con.mostrarError());
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}//Fin del main()
}//Fin de class
