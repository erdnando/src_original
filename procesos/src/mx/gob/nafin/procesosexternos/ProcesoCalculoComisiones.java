package mx.gob.nafin.procesosexternos;

import com.netro.fondeopropio.ComisionFondeoPropio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Proceso mediante el cual se calculan las comisiones de los IFS
 * autor: Deysi Laura Hern�ndez Contreras
 * Fodea 20-2014  DIST-Venta de Cartera

 */

public class ProcesoCalculoComisiones {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ProcesoCalculoComisiones.class);

	public static void main(String [] args)  {
		int codigoSalida = 0;
		int proceso = 0;
		String ruta = "";
		log.info("Inicia Prorceo ProcesoCalculoComisiones  (E)");

	try{
			ruta = args[0];//Este parametro es la ruta de publicacion del servidor

			procesoCalcualoComisiones ();

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Inicia Prorceo ProcesoCalculoComisiones  (E)  "+e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			log.info("Inicia Prorceo ProcesoCalculoComisiones  (E)" +codigoSalida);
		}
	}



	private static void procesoCalcualoComisiones() throws Exception {
		log.info("procesoCalcualoComisiones (E)");

		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDBOracle	 con 	     =   new AccesoDBOracle();
		StringBuffer qrySentencia = new StringBuffer();
		List  lVarBind 		= new ArrayList();
		boolean resultado = true;
		boolean variosTiposComision = false;


		Calendar calendar = Calendar.getInstance();
		int  anio_calculo = calendar.get(Calendar.YEAR);
      int mes_calculo = calendar.get(Calendar.MONTH)+1;

		List monedas = new ArrayList();
		monedas.add("1");
		monedas.add("54");

		ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().remoteLookup("ComisionFondeoPropioEJB", ComisionFondeoPropio.class);


		try {

			con.conexionDB();

				///Obtener todos los if segun el perfil que sea (ADMIN NAFIN||ADMIN BANCOMEXT)
			qrySentencia = new StringBuffer();
			qrySentencia.append("  SELECT distinct ci.ic_if as ic_if  "+
                          " FROM comcat_if ci, comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf"+
                          " WHERE ci.ic_if = rie.ic_if"+
												 "  AND rie.cs_tipo_fondeo in('P', 'M') " +
                          " AND rie.ic_epo = epo.ic_epo" +
                          " AND ci.ig_mes_ajuste_de IS NOT null"+
                          " AND ci.ig_mes_ajuste_a IS NOT null"+
                          " AND ci.ig_anio_ajuste IS NOT null"+
                          " ORDER BY ci.ic_if " );


			log.debug("qrySentencia.toString()  "+qrySentencia.toString());
			log.debug("lVarBind  "+lVarBind );

			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();

			while (rs.next()) {

				String  ic_if = rs.getString("ic_if")==null?"":rs.getString("ic_if");

				for(int x=0; x < monedas.size(); x++) {
					String  ic_moneda =  monedas.get(x).toString();
					//log.debug(ic_if +"--------ic_moneda -----------> "+ic_moneda);

					comisionBean.calcularComisionMensual(ic_if, String.valueOf(mes_calculo-1), String.valueOf(anio_calculo) , "ADMIN NAFIN", ic_moneda);//Este m�todo realiza el c�lculo de la comision mensual, si ya hay un claculo previo, no hace nada :P
					variosTiposComision = comisionBean.manejaVariosTiposComision(ic_if, String.valueOf(mes_calculo), String.valueOf(anio_calculo), "ADMIN NAFIN", ic_moneda, false);//Este m�todo verifica si el if maneja varios tipos de comisi�n, de ser as� despliega el resultado por cada EPO
				}
			}
			rs.close();
			ps.close();

	}catch(Exception e){
			resultado = false;
			log.error("procesoCalcualoComisiones (Exception) " + e);
			throw new AppException("procesoCalcualoComisiones(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("procesoCalcualoComisiones (S)");

	}

}
