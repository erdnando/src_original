package mx.gob.nafin.procesosexternos;

import com.netro.cadenas.InteligenciaComercial;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator; // Ruta donde se encuentra el beanF

public class InteligenciaComercialEJBCliente {

	public static void main(String [] args)  {
		  int codigoSalida = 0;
		try{
			//args[1] contiene la ruta f�sica de la aplicaci�n.
			if("diario".equals(args[0]))
				envioDiario(args[1]);

			if("mensual".equals(args[0]))
				generarArchivoMensual(args[1]);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		}finally{
		System.exit(codigoSalida);
		}
    }

	private static void envioDiario(String rutaApp) throws Exception {
	//	try {
			InteligenciaComercial ic = obtenerEJB();
			
			System.out.println("Bean cargado...");

			Properties props = loadParams("ParametrosGlobalesApp");
	   
					
	      String rutaArchivo =  rutaApp + "/16archivos/credicadenas/";
	      String rutaVirtual = (String)props.get("ruta_virtual");
   
		  ic.getEnvioCorreoDiario( rutaArchivo  ,   rutaVirtual ); 
			
	  /* } catch(Exception ex) {
			String mensaje = ex.getMessage();
	       if("PARAMETRO NO ACTIVO".equals(mensaje)||"NINGUNA PYME RESPONDIO".equals(mensaje)){
				System.out.println(mensaje);
			}else{
				throw ex;
			}
		}	 */

	}

	/**
	 * Generaci�n de reporte mensual. consultaClientesPotenciales de Credicadenas
	 * @throws java.lang.Exception
	 * @param rutaApp Ruta fisica de la aplicaci�n.
	 */
	private static void generarArchivoMensual(String rutaApp) throws Exception {
		InteligenciaComercial ic = obtenerEJB();
		System.out.println("generarArchivoMensual::Bean cargado...");

		Properties props = loadParams("ParametrosGlobalesApp");

		String rutaArchivo =  rutaApp + "/16archivos/credicadenas/";
		String rutaVirtual = (String)props.get("ruta_virtual");
		

		String detalleEjecucion = ic.consultaClientesPotenciales(rutaArchivo, rutaVirtual);
		System.out.println(detalleEjecucion);
	}

	private static InteligenciaComercial obtenerEJB() throws Exception {
		try{
			System.out.println("Inicio Conexion");
			InteligenciaComercial ic = ServiceLocator.getInstance().remoteLookup("InteligenciaComercialEJB", InteligenciaComercial.class);
			return ic;
		}catch(Throwable t){
			throw new Exception("Error al obtener el EJB");
		}
	}


	public static Properties loadParams(String file){


		Properties		 prop	 = new Properties();
		try{
			ResourceBundle bundle = ResourceBundle.getBundle(file);

			// Obtiene las llaves y la coloca en el objeto Properties
			Enumeration enumx = bundle.getKeys();
			String key	= null;
			while (enumx.hasMoreElements()) {
				key = (String) enumx.nextElement();
				prop.put(key, bundle.getObject(key));
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return prop;
	}


}
