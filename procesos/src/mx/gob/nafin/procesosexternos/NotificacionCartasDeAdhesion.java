package mx.gob.nafin.procesosexternos;

import com.nafin.util.EnvioNotificacionesCartaAdhesion;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NotificacionCartasDeAdhesion {

    private static final Log LOG = LogFactory.getLog(NotificacionCartasDeAdhesion.class);

    public NotificacionCartasDeAdhesion() {
        super();
    }

    public static void main(String[] args) {
        LOG.info("NotificacionCartasDeAdhesion(E)");
        int codigoSalida = 0;
        try {
            EnvioNotificacionesCartaAdhesion envioNoti = ServiceLocator.getInstance().remoteLookup("EnvioNotificacionCartasAdhesion", EnvioNotificacionesCartaAdhesion.class);
            envioNoti.enviaCorreos();
        } catch (Exception e) {
            System.out.println("NotificacionCartasDeAdhesion::main(Exception)" + e);
            e.printStackTrace();
            System.out.println(e.getMessage());
            codigoSalida = 1;
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            codigoSalida = 1;
        } finally {
            LOG.info("NotificacionCartasDeAdhesion(S)");
            System.exit(codigoSalida);
        }
    }
}
