package mx.gob.nafin.procesosexternos;


import com.netro.descuento.PemexEnlOperados;

public class PemexEnlOpeCliente {

	public static void main(String args[]) {
		//DocumentosEnl doctosEnl = new DocumentosEnl();
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();

		try {

			/*

				Nota (27/08/2015 07:26:31 p.m., by jshernandez): Se simplifica la ejecución anterior:

				cargaDoctos.procesoDoctos(args[0], new PemexEnlOperados(args[0]), "O");
				cargaDoctos.procesoDoctos(args[0], new PemexEnlOperados(args[0]), "B");
				cargaDoctos.procesoDoctos(args[0], new PemexEnlOperados(args[0]), "V");

			*/
			cargaDoctos.procesoDoctos(args[0], new PemexEnlOperados(args[0]), "");
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java PemexEnlOpeCliente <IC_EPO>. Donde IC_EPO es la clave de la EPO.");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
	}

}