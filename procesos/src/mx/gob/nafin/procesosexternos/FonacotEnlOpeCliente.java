package mx.gob.nafin.procesosexternos;


import com.netro.descuento.FonacotEnlOperados;

public class FonacotEnlOpeCliente {

	public static void main(String args[]) {
		//DocumentosEnl doctosEnl = new DocumentosEnl();
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();

		try {
			//doctosEnl.proceso(args[0], new FonacotEnlOperados(args[0]));
			CargaDoctosEnl.procesoDoctos(args[0], new FonacotEnlOperados(args[0]), "");
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java FonacotEnlOpeCliente 8. El 8 es el n�mero de la EPO para Fonacot.");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
	}

}