package mx.gob.nafin.procesosexternos;

import com.netro.dispersion.Dispersion;

import netropology.utilerias.ServiceLocator; // Ruta donde se encuentra el bean

public class GeneraEstadoCuentaINFONAVITCliente {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		try {
			String rutaApp="";
			String cveEpo="";
			if(args.length>0)
				rutaApp = args[0];

			if(args.length>1)
				cveEpo = args[1];

			generaEstadoCuenta(rutaApp, cveEpo);
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
	    }finally{
			System.exit(codigoSalida);
		}
    }

	private static void generaEstadoCuenta(String rutaApp, String cveEpo) throws Throwable {
		Dispersion aif = obtenerEJB();
		if(cveEpo==null || "".equals(cveEpo))
			cveEpo="396";
		System.out.println("Bean cargado...epo="+cveEpo);
		System.out.println("RUTA ARCHIVO ="+rutaApp);
		aif.generaEstadoCuentatxt(cveEpo, "1",rutaApp);
		System.out.println("Fin");
	}

	private static Dispersion obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Dispersion aifS = ServiceLocator.getInstance().remoteLookup("DispersionEJB", Dispersion.class);
		return aifS;
	}

}
