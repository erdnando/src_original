package mx.gob.nafin.procesosexternos;

import com.netro.afiliacion.Afiliacion;
import com.netro.afiliacion.FIDEEnlaceCE;
import com.netro.afiliacion.IFEnlaceCE;

import netropology.utilerias.ServiceLocator;


public class FIDEEnlAltaCliente {

	public static void main(String args[]) {
		int codigoSalida = 0;
		try {
			proceso(args[0], new FIDEEnlaceCE());
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java FIDEEnlAltaCliente 8. El 8 es el n�mero del Intermediario para FIDE.");
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
		System.exit(codigoSalida);
		}
	}

	static void proceso(String intermediario,IFEnlaceCE ifEnl) throws Throwable {
		StringBuffer log = new StringBuffer();
		System.out.println("Inicio Conexion");
		Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);
		log = afiliacion.procesoCE(intermediario, ifEnl);
		System.out.println(log);
	} //fin main procesoIMSS
}