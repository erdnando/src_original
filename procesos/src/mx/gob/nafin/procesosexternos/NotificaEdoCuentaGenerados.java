package mx.gob.nafin.procesosexternos;

import com.netro.dispersion.notificaciones.EdoCuentaLineaCredito;

import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 * Clase que se encarga de realizar las siguientes notificaciones:
 *
 * 1. Notifica v�a correo electr�nico a los usuarios NAFIN el listado de Clientes
 * que ya cuentan con estado de cuenta generado.
 *
 * 2. Notifica v�a correo electr�nico a los clientes de L�nea Cr�dito
 * ( Clientes Externos, Intermediarios Financieros e Intermediarios Financieros No Bancarios)
 * que su �ltimo Estado de Cuenta ya fue generado y registrado en NAFINET para su consulta.
 *
 *
 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
 *				Proceso: Notificar Estados de Cuenta generados.
 * @author  jshernandez
 * @date    17/04/2015 04:31:11 p.m.
 *
 */
public class NotificaEdoCuentaGenerados  {

	private static Log log = ServiceLocator.getInstance().getLog(NotificaEdoCuentaGenerados.class);

	public static void main(String[] args){

		int 		codigoSalida 	= 0;

		log.info("--------------------------------------------------------------------------------");
		log.info("NotificaEdoCuentaGenerados(E)");

		try {

			// 1. Validar argumentos
			log.info("NotificaEdoCuentaGenerados(): Validaci�n de Argumentos.");
			String fechaActual 					= null;
			String fechaUltimoDiaMesAnterior = null;
			try {
				if(args.length != 0 && args.length != 2 ){
					throw new Exception("N�mero invalido de argumentos.");
				} else if(  args.length == 2 ){
					fechaActual = args[0].trim();
					if(!Comunes.esFechaValida(fechaActual, "dd/MM/yyyy")){
						throw new Exception("La fecha que se asumir� como la del d�a actual no es v�lida, debe tener formato dd/MM/yyyy.");
					}
					fechaUltimoDiaMesAnterior = args[1].trim();
					if(!Comunes.esFechaValida(fechaUltimoDiaMesAnterior, "dd/MM/yyyy")){
						throw new Exception("La fecha que se asumir� como la del mes anterior no es v�lida, debe tener formato dd/MM/yyyy.");
					}
				}
			}catch(Exception e){

				log.error("ERROR en el n�mero de argumentos proporcionados.");
				log.error("Ejemplo:");
				log.error(" NotificaEdoCuentaGenerados [<fechaActual> <fechaUltimoDiaMesAnterior>]");
				log.error("   fechaActual: Cadena de texto con fecha en formato dd/mm/aaaa, con la que ");
				log.error("                el proceso asumir� como d�a actual.");
				log.error("   fechaUltimoDiaMesAnterior: Cadena de texto con la fecha del �ltimo d�a ");
				log.error("                del mes anterior en formato dd/mm/aaaa, y con respecto a la");
				log.error("                que corresponden los Estados de Cuenta generados.");
				log.error("Nota: La opci�n con par�metros s�lo se debe utilizar para cuando no se haya ");
				log.error("podido realizar la notificaci�n en el d�a que corresponda.");

				e.printStackTrace();
				throw e;
			}

			// 2. Obtener EJB de EdoCuentaLineaCredito que realizar� las notificaciones
			log.info("NotificaEdoCuentaGenerados(): Obtenci�n de EJB de EdoCuentaLineaCredito.");
			EdoCuentaLineaCredito 		edoCuentaLineaCredito 		= ServiceLocator.getInstance().remoteLookup("EdoCuentaLineaCreditoEJB", EdoCuentaLineaCredito.class);

			// 3. Notificar Estados de Cuenta generados a Clientes NAFIN de L�nea de
			// Cr�dito y Usuarios NAFIN.
			// ..........................................................................................
			log.info("NotificaEdoCuentaGenerados(): Notificando Estados de Cuenta generados a Clientes NAFIN de L�nea de Cr�dito y Usuarios NAFIN.");
			edoCuentaLineaCredito.notificarEdoCuentaGeneradoNafin(fechaActual,fechaUltimoDiaMesAnterior);

		} catch(Throwable e) {

			log.error("ERROR: " + e.getMessage());
			e.printStackTrace();
			codigoSalida 	= 1;

		} finally {
			log.info("NotificaEdoCuentaGenerados(S)");
		}

		System.exit(codigoSalida);

	}

}