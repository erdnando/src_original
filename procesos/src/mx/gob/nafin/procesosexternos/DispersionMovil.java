package mx.gob.nafin.procesosexternos;

/*************************************************************************************
* @since 29 de Octubre de 2008
* @author Gerardo P�rez	Garc�a
*   Dispersion M�vil
* Esta clase se encarga de ejecutar los procesos de notificaci�n via SMS a los
* Usuarios NAFIN que tienen parametrizado el servicio mediante el cual, con un mensaje de texto
* enviado a su tel�fono celular, pueden recibir informaci�n de cadenas productivas
* no enviadas al fondo de flujos
*
*
*************************************************************************************/
import com.netro.dispersion.Dispersion;

import netropology.utilerias.ServiceLocator;

public class DispersionMovil {
	public static void main(String args[])  {
		int codigoSalida = 0;
		try{

				procesoDispersionMovilPendientes();

		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: No se proporciono argumento");
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
   }



	private static void procesoDispersionMovilPendientes() throws Throwable {
		Dispersion dispersion = obtenerEJB();
		dispersion.enviaSMSDispersionesPendientes();
		System.out.println("Ejecuto m�todo procesoFactorajeMovilOfertaTasas()");
		boolean lbOk = true;
	}

	private static Dispersion obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Dispersion dispersion = ServiceLocator.getInstance().remoteLookup("DispersionEJB", Dispersion.class);
		return dispersion;
	}
}