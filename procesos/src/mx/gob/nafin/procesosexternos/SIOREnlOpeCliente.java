package mx.gob.nafin.procesosexternos;

import com.netro.descuento.SIOREnlOperados;

public class SIOREnlOpeCliente{

	public static void main(String args[]){
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();
		String tipoProc = args[1];
		try {
			tipoProc = tipoProc.toUpperCase();
			if(tipoProc.equals("O")) {
				System.out.println("..:::::::::: Documentos Operados");
			} else if(tipoProc.equals("V")) {
				System.out.println("..:::::::::: Documentos Vencidos sin operar");
			} else {
				throw new Exception("Tipo de Operacion: O-Operados, V-Vencidos sin Operar");
			}
			CargaDoctosEnl.procesoDoctos(args[0], new SIOREnlOperados(args[0]), tipoProc);
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			codigoSalida = 1;
		} catch(Exception e) {
			e.printStackTrace();
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
	}

}//SIOREnlOpeCliente