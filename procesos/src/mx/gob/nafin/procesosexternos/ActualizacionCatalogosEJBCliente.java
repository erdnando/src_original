package mx.gob.nafin.procesosexternos;

import com.netro.catalogos.ActualizacionCatalogos;

import netropology.utilerias.ServiceLocator;

public class ActualizacionCatalogosEJBCliente {

	public static void main(String [] args)  {
		int codigoSalida =0;
		try {
			ActualizaCatalogos();
		} catch(Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
	}

	/**
	 * Tiene por finalidad, llamar el metodo del EJB para
	 * actualizar el catalogo de tipo de cambio
	 */
 	private static void ActualizaCatalogos() throws Throwable  {
		ActualizacionCatalogos actualizaCat = obtenerEJB();
		actualizaCat.execProcActualizaTipoCambio();
		System.out.println("ActualizacionCatalogosEJBCliente::Finalizado con exito");
	}

	private static ActualizacionCatalogos obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		ActualizacionCatalogos actualizaCat = ServiceLocator.getInstance().remoteLookup("ActualizacionCatalogosEJB", ActualizacionCatalogos.class);
		return actualizaCat;
	}

}
