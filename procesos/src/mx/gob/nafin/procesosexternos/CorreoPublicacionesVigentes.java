package mx.gob.nafin.procesosexternos;

import com.nafin.email.Email;

import com.netro.cadenas.PublicacionesVigentes;


import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import netropology.utilerias.ServiceLocator;

public class CorreoPublicacionesVigentes {

	public static void main(String [] args)  {
		  int codigoSalida = 0;
		try{
			String directorio = args[0];//Este parametro es la ruta de publicacion del servidor
			//String directorio = "D:/desane10g_local/nafin-web/nafin-web/";//Este dato solo es para trabajar de localmente.
			enviaCorreoPublicacionesVigentes(directorio);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
  }

	private static void enviaCorreoPublicacionesVigentes(String rutaApp) throws Exception {
		System.out.println("enviaCorreoPublicacionesVigentes(E) ::..");
	    System.out.println("Correo correo = new Correo()");
		try {
                        Email email = ServiceLocator.getInstance().remoteLookup("EmailEJB", Email.class);
                        //Correo correo = new Correo();
                        System.out.println("Correo correo = new Correo()");
                        //correo.disableDebug();
			//correo.enableUseAccesoDBOracle();
			PublicacionesVigentes publicacionesVigentes = new PublicacionesVigentes();
                        System.out.println("PublicacionesVigentes publicacionesVigentes = new PublicacionesVigentes()");
			ArrayList archivosAdjuntos = new ArrayList();
			HashMap hmArchivoAdjunto = new HashMap();
			String fechaActual = publicacionesVigentes.getFechaConNombreDelMes(new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));
			String correosDestinatarios = publicacionesVigentes.obtenerDestinatariosPubVigentes();
                        System.out.println("String correosDestinatarios = publicacionesVigentes.obtenerDestinatariosPubVigentes()");
			String archivoAdjunto = rutaApp + "/00tmp/15cadenas/" + publicacionesVigentes.generaArchivoPublicacionesVigentes(rutaApp);
			System.out.println("..:: archivoAdjunto: "+archivoAdjunto);
			hmArchivoAdjunto.put("FILE_FULL_PATH", archivoAdjunto);
			archivosAdjuntos.add(hmArchivoAdjunto);
                        System.out.println("archivosAdjuntos.add(hmArchivoAdjunto)");
			String  BCC ="BCC";

			if (!correosDestinatarios.equals("")) {
				email.enviaCorreoConDatosAdjuntosBCC("noresponse@nafin.gob.mx",correosDestinatarios,"","Publicaciones Vigentes " + fechaActual,"<b>Publicaciones Vigentes al " + fechaActual + "</b>",null, archivosAdjuntos, BCC,true);
			} else {
				System.out.println("..:: No hay cuentas de correo de destinatario parametrizadas");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
	  } finally {
			System.out.println("enviaCorreoPublicacionesVigentes(S) ::..");
		}
	}
}
