package mx.gob.nafin.procesosexternos;

/*************************************************************************************
*
* Nombre de Clase o de Archivo: ContratosE.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 2002
*
* Autor:          Edgar Gonzalez Bautista
*
* Descripci�n de Clase: Clase que realiza el proceso de Contratos para Interfases a SIRAC de Credito Electronico.
*
*************************************************************************************/


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Calendar;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;


public class ContratosE
{
	public static void main(String args[])
	{
		String usuario = args[0];
		ResultSet rs;
		ResultSet rs1;
		ResultSet rs2;

		CallableStatement cs=null;
		String sentenciaSQL;
		String sentenciaSQL1;

		String cadena="";

		String folio = "";
		int tasaClave;
		java.sql.Date fecha, fechaDesembolso;
		int codigoAgenciaProy;
		int codigoSubAplicacionProy;
		long numLineaCredito;
		long numSirac;
		String tipoContrato = "D";
		String claseContrato = "O";
		String observaciones ="";
		double montoOperacion;
		String codigoAgencia="";
		String codigoSubAplicacion="";
		String numLineaFondeo="";
		String codigoFinanciera="";
		String aprobadoPor="";
		int plazo;
		int cod_sub_apli_det;           				/* Valor 29 */

		long numeroContrato=0; //F084-2006 long
		String estatus="";
		boolean errorDesconocido=false;
		boolean errorTransaccion = false;

		String codigoAplicacion="";
		int existe;

		String credito_restruc ="";

		String querySolicPortal = "";

		AccesoDBOracle con = new AccesoDBOracle();
		try {
			con.conexionDB();
		} catch (Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try
		{
			sentenciaSQL = "select i.ic_solic_portal " +
						"    , s.ic_tasaif " +
						"	, i.ig_codigo_agencia_proy " +
						"    , i.ig_codigo_sub_aplicacion_proy " +
						"	, i.ig_numero_linea_credito " +
						"    , i.cg_descripcion " +
						"    , i.fn_monto_operacion " +
						"	, i.ig_codigo_agencia " +
						"    , i.ig_codigo_sub_aplicacion " +
						"	, i.ig_numero_linea_fondeo " +
						"    , i.ig_codigo_financiera " +
						"    , i.ig_aprobado_por " +
						"	, s.ig_clave_sirac as in_numero_sirac " +
						"	, trunc(s.df_v_descuento) - trunc(s.df_carga) as ig_plazo  " +
						"	, s.ic_tipo_credito " +
						"	, t.cs_restructura " +
						" FROM com_solic_portal s " +
						"    , com_interfase i " +
						"	  , comcat_tipo_credito t " +
						" WHERE s.ic_solic_portal = i.ic_solic_portal  " +
						"	  AND t.ic_tipo_credito = s.ic_tipo_credito " +
						"    AND i.ig_numero_contrato is null " +
						"    AND i.ig_numero_linea_credito is not null  " +
						"    AND i.cc_codigo_aplicacion is null " +
						"    AND i.ic_error_proceso is null " +
						"    AND s.ic_estatus_solic = 2  " +
						"    AND s.ic_bloqueo = 3";


//				System.out.println(sentenciaSQL);
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("ContratosE.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			int tipoPiso;
			while(rs.next()) {
        claseContrato = "O";
				errorTransaccion = false;
				errorDesconocido=false;
				folio = rs.getString("IC_SOLIC_PORTAL");
				tasaClave = rs.getInt("IC_TASAIF");

				codigoAgenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				codigoSubAplicacionProy = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				numLineaCredito = rs.getLong("IG_NUMERO_LINEA_CREDITO"); //F084-2006
				observaciones = rs.getString("CG_DESCRIPCION");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				numSirac = rs.getLong("IN_NUMERO_SIRAC");

				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				aprobadoPor = (aprobadoPor==null)?"NULL":"'"+aprobadoPor+"'";
				plazo = rs.getInt("IG_PLAZO");
				cod_sub_apli_det = rs.getInt("IC_TIPO_CREDITO");
				credito_restruc = rs.getString("CS_RESTRUCTURA");

				sentenciaSQL = "select fecha_hoy + "+plazo+", fecha_hoy from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR'";
//					System.out.println(sentenciaSQL);
				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("ContratosE.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				fecha = rs1.getDate(1);
				fechaDesembolso = rs1.getDate(2);
				rs1.close();
				con.cierraStatement();

				//modificacion parametro 17 04/2009

				if(credito_restruc.equals("S")){
					claseContrato = "I";
				}


				try {
					cs = con.ejecutaSP("LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
				} catch(SQLException sqle) {
					System.out.println("ContratosE.java(sentenciaSQL): " + "LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
					throw sqle;
				}


				cs.setInt(1, 90);								//01.- CODIGO_AGENCIA
				cs.setInt(2, cod_sub_apli_det);					//02.- CODIGO_SUB_APLICACION
				cs.setNull(3, Types.INTEGER);					//03.- NUMERO_CONTRATO (out)
				cs.registerOutParameter(3, Types.INTEGER);
				//F084-2006
				cs.setLong(4, numSirac);							//04.- NUMERO SIRAC
				cs.setDate(5, null, Calendar.getInstance());							//05.- FECHA_APERTURA
				cs.setString(6, usuario);						//06.- CODIGO_EJECUTIVO
				cs.setString(7, null);							//07.- CODIGO_LINEA_FINANCIERA
				cs.setInt(8, tasaClave);						//08.- CODIGO_VALOR_TASA_CARTERA
				cs.setInt(9, 90);								//09.- CODIGO_AGENCIA_LC
				cs.setInt(10, codigoSubAplicacionProy);			//10.- CODIGO_SUB_APLICACION_LC
				cs.setLong(11, numLineaCredito);					//11.- CODIGO_LINEA_CREDITO
				cs.setString(12, null);							//12.- BLOQUEO
				cs.setDate(13, fecha,Calendar.getInstance());							//13.- FECHA_VENCIMIENTO
				cs.setString(14, observaciones);				//14.- OBSERVACIONES
				cs.setDouble(15, montoOperacion);				//15.- MONTO_APROBADO
				cs.setString(16, tipoContrato);							//16.- TIPO_CONTRATO
				cs.setString(17, claseContrato);							//17.- CLASE_CONTRATO
				cs.setNull(18, Types.INTEGER);					//18.- CODIGO_GRUPO_COMISION
				cs.setDate(19, fechaDesembolso,Calendar.getInstance());				//19.- FECHA_DESEMBOLSO
				cs.setDouble(20, (double)100);					//20.- PORCENTAJR_AVANCE_PROYECTADO
				cs.setDouble(21, montoOperacion);				//21.- VALOR_DESEMBOLSO_PROYECTADO
				cs.setNull(22, Types.INTEGER);					//22.- CODIGO_TIPO_GARANTIA
				cs.setString(23, null);							//23.- DESCRIPCION
				cs.setNull(24, Types.INTEGER);					//24.- CODIGO_COMISION
				cs.setNull(25, Types.DOUBLE);					//25.- PORCENTAJE_GARANTIA
				cs.setNull(26, Types.DOUBLE);					//26.- PORCENTAJE_COMISION
				cs.setNull(27, Types.INTEGER);					//27.- ESTATUS (out)
				cs.registerOutParameter(27, Types.INTEGER);



				cadena =	"p1:=90;"+"\n"+
				"p2:="+cod_sub_apli_det+";"+"\n"+
				"p3:=NULL;"+"\n"+
				"p4:="+numSirac+";"+"\n"+
				"p5:=NULL;"+"\n"+
				"p6:='"+usuario+"';"+"\n"+
				"p7:=NULL;"+"\n"+
				"p8:="+tasaClave+";"+"\n"+
				"p9:=90;"+"\n"+
				"p10:="+codigoSubAplicacionProy+";"+"\n"+
				"p11:="+numLineaCredito+";"+"\n"+
				"p12:=NULL;"+"\n"+
				"p13:=to_date('"+fecha+"','yyyy-mm-dd');"+"\n"+
				"p14:='"+observaciones+"';"+"\n"+
				"p15:="+montoOperacion+";"+"\n"+
				"p16:='"+tipoContrato+"';"+"\n"+
				"p17:='"+claseContrato+"';"+"\n"+
				"p18:=NULL;"+"\n"+
				"p19:=to_date('"+fechaDesembolso+"','yyyy-mm-dd');"+"\n"+
				"P20:=100;"+"\n"+
				"p21:="+montoOperacion+";"+"\n"+
				"p22:=NULL;"+"\n"+
				"p23:=NULL;"+"\n"+
				"p24:=NULL;"+"\n"+
				"p25:=NULL;"+"\n"+
				"p26:=NULL;"+"\n"+
				"p27:=NULL;"+"\n";

				System.out.println(cadena);

				try {
					cs.execute();
						numeroContrato = cs.getLong(3);
						estatus = cs.getString(27);		//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							errorDesconocido = true;
							estatus="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}


				System.out.println("El Valor de Contrato: "+numeroContrato+" y Estatus "+estatus);
				cadena += "El Valor de Contrato: "+numeroContrato+" y Estatus "+estatus;
				if(!estatus.equals("0"))		//Error
				{
					con.terminaTransaccion(false);
					if (!errorDesconocido)
					{
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='CN'";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("ContratosE.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_interfase where ic_solic_portal="+folio;
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("ContratosE.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) { // Se valida la concurrencia
						sentenciaSQL = "update com_interfase " +
									"	set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
									" 	, ic_error_proceso="+estatus+
									"   , df_hora_proceso=SYSDATE"+
									"   , ig_proceso_numero = 2"+
									"   , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									" where ic_solic_portal="+folio;

						//FODEA 011-2010 FVR-INI
						querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET ic_error_proceso = ? " +
							" , cc_codigo_aplicacion = ? "+
							" WHERE ic_solic_portal = ? ";
						//FODEA 011-2010 FVR-FIN
					}
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos de error en com_interfase.");
						errorTransaccion = true;
					}
					//FODEA 011-2010 FRV-INI
					try {
						PreparedStatement ps = con.queryPrecompilado(querySolicPortal);
						ps.setInt(1, Integer.parseInt(estatus));
						ps.setString(2,codigoAplicacion);
						ps.setInt(3, Integer.parseInt(folio));
						ps.executeUpdate();
						ps.close();
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_solic_portal");
						errorTransaccion = true;
					}
					//FODEA 011-2010 FRV-FIN
				} else { //no hay error

					sentenciaSQL = "Update com_interfase " +
							"	set ig_numero_contrato = " + numeroContrato +
							"   , df_hora_proceso=SYSDATE "+
							"   , DF_CONTRATOS=SYSDATE "+
							"   , ig_proceso_numero = 2"+
							"   , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" WHERE ic_solic_portal = " + folio ;

//						System.out.println(sentenciaSQL);
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el numero de contrato");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
					//System.out.println("Se realiza el commit");
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}	//Fin del main();
}//fin de la clase
