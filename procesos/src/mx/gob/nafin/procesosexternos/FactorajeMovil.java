package mx.gob.nafin.procesosexternos;

/*************************************************************************************
* @since 29 de Octubre de 2008
* @author Alberto Cruz Flores
* FODEA 048 - 2008 Factoraje M�vil
* Esta clase se encarga de ejecutar los procesos de notificaci�n via SMS a la pymes
* que tienen parametrizado el servicio mediante el cual, con un mensaje de texto
* enviado a su tel�fono celular, pueden recibir informaci�n del n�mero y/o monto de
* los documentos que tienen publicados y/o de los documentos no operados pr�ximos
* vencer.
*
*************************************************************************************/
import com.netro.servicios.Servicios;

import netropology.utilerias.ServiceLocator;

public class FactorajeMovil {
	public static void main(String args[])  {
		int codigoSalida = 0;
		try{
			if(args[0].equals("P")){
				procesoFactorajeMovilPublicacion();
			}else if(args[0].equals("V")){
				procesoFactorajeMovilVencimiento();
			}else if(args[0].equals("O")){
				procesoFactorajeMovilOfertaTasas();
			}
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: No se proporciono argumento");
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
   }

	private static void procesoFactorajeMovilPublicacion() throws Throwable {
		Servicios servicios = obtenerEJB();
		servicios.enviaSMSFactorajeMovilPublicacion();
		System.out.println("Ejecuto m�todo procesoFactorajeMovilPublicacion()");
		boolean lbOk = true;
	}

	private static void procesoFactorajeMovilVencimiento() throws Throwable {
		Servicios servicios = obtenerEJB();
		servicios.enviaSMSFactorajeMovilVencimiento();
		System.out.println("Ejecuto m�todo procesoFactorajeMovilVencimiento()");
		boolean lbOk = true;
	}

	private static void procesoFactorajeMovilOfertaTasas() throws Throwable {
		Servicios servicios = obtenerEJB();
		servicios.enviaSMSFactorajeMovilOfertaTasas();
		System.out.println("Ejecuto m�todo procesoFactorajeMovilOfertaTasas()");
		boolean lbOk = true;
	}

	private static Servicios obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Servicios servicios = ServiceLocator.getInstance().remoteLookup("ServiciosEJB", Servicios.class);
		return servicios;
	}
}