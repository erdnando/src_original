package mx.gob.nafin.procesosexternos;

/**
 *	Esta clase se encarga de procesar el reporte SIAFF de manera autom�tica.
 * @author Alberto Cruz Flores
 * @since FODEA 034 - 2009
 */
import com.netro.descuento.InformacionTOIC;

import java.io.File;
import java.io.Serializable;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.List;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

public class CargaAutomaticaReporteSIAFF implements Serializable{
	public static void main(String args[]){
		int codigoSalida = 0;
		try{
			SimpleDateFormat formato = new SimpleDateFormat("ddMMyyyy");
			Calendar calendario = Calendar.getInstance();
			String directorio = args[0];//Este parametro es la ruta donde se encuentra el archivo en el directorio del servidor.
			//String directorio = "D:/desane10g_local/nafin-web/nafin-web/FileTransfer/SIAFF/";//Este dato solo es para trabajar de localmente.
			String nombre_archivo_entrada = "E" + formato.format(calendario.getTime()) + ".txt";
			String nombre_archivo_salida = "S" + formato.format(calendario.getTime());
			String ruta_archivo_entrada = directorio + nombre_archivo_entrada;

			File archivo_carga = new File(ruta_archivo_entrada);

			if(archivo_carga.exists()){
				int numero_proceso = getNumeroProcesoCargaSIAFF();
				guardaReporteSIAFFTmp(ruta_archivo_entrada, numero_proceso);
				int numero_carga_siaff = getNumeroCargaSIAFF();
				procesaReporteSIAFF(numero_proceso, numero_carga_siaff, nombre_archivo_entrada);
				if(hayErroresCargaSIAFF(numero_carga_siaff)){
					generaArchivoSalidaCargaSIAFF(numero_carga_siaff, directorio, nombre_archivo_salida);
				}
			}else{
				System.out.println("NO SE PROPORCIONO EL ARCHIVO DE CARGA");
			}
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: No se proporciono argumento");
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
   }
	/**
	 * Este m�todo se encarga de generar la instancia del EJB.
	 */
	private static InformacionTOIC obtenerEJB() throws Throwable {
		InformacionTOIC informacionTOIC = ServiceLocator.getInstance().remoteLookup("InformacionTOICEJB", InformacionTOIC.class);
		return informacionTOIC;
	}

	/**
	 * Este m�todo obtiene el numero de proceso con el que se idenficara el flujo de carga
	 * actual.
	 */
	private static int getNumeroProcesoCargaSIAFF() throws Throwable{
		InformacionTOIC informacionTOIC = obtenerEJB();
		int numero_proceso = informacionTOIC.getNumeroProcesoCargaSIAFF();
		return numero_proceso;
	}

	/**
	 * Este m�todo obtiene el numero de carga SIAFF.
	 */
	private static int getNumeroCargaSIAFF() throws Throwable{
		InformacionTOIC informacionTOIC = obtenerEJB();
		int numero_carga_siaff = informacionTOIC.getNumeroCargaSIAFF();
		return numero_carga_siaff;
	}

	/**
	 * Este m�todo indica si se encontraron registros con errores en la carga del reporte SIAFF.
	 */
	private static boolean hayErroresCargaSIAFF(int numero_carga_siaff) throws Throwable{
		InformacionTOIC informacionTOIC = obtenerEJB();
		boolean errores_carga_siaff = informacionTOIC.existenErroresCargaSIAFF(numero_carga_siaff);
		return errores_carga_siaff;
	}

	/**
	 * Este m�todo se encarga de leer el archivo de reporte SIAFF y almacenar la
	 * informaci�n en la tabla temporal.
	 */
	private static void guardaReporteSIAFFTmp(String ruta_archivo, int numero_proceso) throws Throwable{
		String tipo_carga = "A";
		InformacionTOIC informacionTOIC = obtenerEJB();
		informacionTOIC.setCargaTmpMasivaSIAFF(ruta_archivo, numero_proceso, tipo_carga);
	}

	/**
	 * Este m�todo se encarga de procesar la informaci�n almacenada en la tabla temporal y de generar el
	 * resumen de la carga del reporte SIAFF.
	 */
	private static void procesaReporteSIAFF(int numero_proceso, int numero_carga_siaff, String nombre_archivo_entrada) throws Throwable{
		InformacionTOIC informacionTOIC = obtenerEJB();
		informacionTOIC.procesarCargaSIAFF(numero_proceso, numero_carga_siaff, "", nombre_archivo_entrada);
	}

	/**
	 * Este m�todo se encarga de procesar la informaci�n almacenada en la tabla temporal y de generar el
	 * resumen de la carga del reporte SIAFF.
	 */
	private static void generaArchivoSalidaCargaSIAFF(int numero_carga_siaff, String ruta_archivo, String nombre_archivo_salida) throws Throwable{
		CreaArchivo crea_archivo = new CreaArchivo();
		InformacionTOIC informacionTOIC = obtenerEJB();
		StringBuffer contenido_archivo = new StringBuffer();
		List registros_erroneos = informacionTOIC.getRegistrosErroneosCargaSIAFF(numero_carga_siaff);

		for(int i = 0; i < registros_erroneos.size(); i++){
			contenido_archivo.append(registros_erroneos.get(i).toString());
		}

		if(contenido_archivo.length() > 0){
			crea_archivo.make(contenido_archivo.toString(), ruta_archivo, nombre_archivo_salida, ".txt");
		}
	}
}