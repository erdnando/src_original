package mx.gob.nafin.procesosexternos;


import com.nafin.docgarantias.nivelesdeservicio.NivelesServicio;
import com.nafin.supervision.Supervision;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase para la ejecuci�n del proceso de Avisos de Generador por Nafin
 * @author Deysi Laura Hern�ndez Contreras
 */
public class AvisosGeneradosPorNafin {

    private static final Log LOG = ServiceLocator.getInstance().getLog(AvisosGeneradosPorNafin.class);
    
    /**
     * construcctor 
     */
    private AvisosGeneradosPorNafin() {
    
    }
    
    /**
     *
     * @param args
     */
    public static void main(String [] args)  {
        System.out.println("Entrando a AvisosGeneradosPorNafin");
        int codigoSalida = 0;
        String ruta = "";     
        try {
            ruta = args[0];       
            Supervision supervision = ServiceLocator.getInstance().remoteLookup("SupervisionEJB", Supervision.class);
            supervision.avisosGeneradosPorNafin(ruta);    
            
            LOG.info("llamando al bean de Niveles de Servicio...");
            NivelesServicio nivelServicio = ServiceLocator.getInstance().remoteLookup("NivelesServicio", NivelesServicio.class);
            LOG.info("ok, ejecutando el env�o de alertas...");
            nivelServicio.enviarAlertaNivelServicio();
            LOG.info("Envio de Alertas de Nivel de servicio terminado");
            System.out.println("Salio de Niveles de Servicio");            
            
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Error :::"+e.getMessage());
            codigoSalida = 1;
        } catch(Throwable ex) {
            ex.printStackTrace();
            codigoSalida = 1;
            LOG.error("Error :::"+ex.getMessage());                     
	} finally {
            LOG.info("codigo de Salida  "+codigoSalida);
            System.exit(codigoSalida);
        }
    }
               
}
