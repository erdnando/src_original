package mx.gob.nafin.procesosexternos;

// Elaboro: Edgar Gonz�lez Bautista, Foda 41, Fecha 11/05/2004.

import com.netro.descuento.CargaDocumento;
import com.netro.descuento.EpoEnlOperados;
import com.netro.descuento.EpoEnlace;

import java.math.BigDecimal;

import java.util.Vector;

import netropology.utilerias.ServiceLocator;


public class CargaDoctosEnl {

	public static void proceso(String EPO,EpoEnlace epoEnl) throws Exception{
		StringBuffer log = new StringBuffer();
		System.out.println("Inicio Conexion");
		CargaDocumento cargaDoctoS = ServiceLocator.getInstance().remoteLookup("CargaDocumentoEJB", CargaDocumento.class);
		log = cargaDoctoS.proceso(EPO,epoEnl);
		System.out.println(log.toString());
	}


	public static void procesoDoctos(String EPO, EpoEnlOperados epoEnlOperados, String status) throws Exception{
		StringBuffer log = new StringBuffer();
		System.out.println("Inicio Conexion");
		CargaDocumento cargaDoctoS = ServiceLocator.getInstance().remoteLookup("CargaDocumentoEJB", CargaDocumento.class);
		log = cargaDoctoS.procesoDocumentos(EPO,epoEnlOperados, status);
		System.out.println(log.toString());
	}

	private static boolean esNumero(String numero) {
		boolean isNum = false;
		try {
			BigDecimal num=new BigDecimal(numero.trim());
			isNum = true;
		} catch(NumberFormatException nfe) { isNum = false; }
		return isNum;
	}

	private static String dobleComitas(String frase) {
		StringBuffer sb=new StringBuffer(frase);
		Vector v=new Vector();
		for(int i=0; i<sb.length(); i++) {
			if(sb.substring(i,i+1).equals("'")) {
				v.addElement(Integer.toString(i));
			}
		}
		int indx=0;
		for(int h=v.size(); h>0; h--) {
			indx=Integer.parseInt(v.get(h-1).toString());
			sb.replace(indx, indx+1, "''");
		}
		return sb.toString().trim();
	}

}
