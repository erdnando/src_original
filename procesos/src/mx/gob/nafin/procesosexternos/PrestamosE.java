package mx.gob.nafin.procesosexternos;

/*************************************************************************************
* Modificaci�n
*
* Fecha �ltima. Modificaci�n:	15/Julio/2004
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 042 - 2004, Plan de Pagos
*
*
*************************************************************************************/

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Calendar;
import java.util.GregorianCalendar;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;

public class PrestamosE {

	public static void main(String args[]) {
		ejecutaPrestamos(args[0]);
	}

	public static void ejecutaPrestamos(String usuario){
		String query="", queri="", cod_tipo_amort="", codigo_aplicacion="", folio="", folioSD="";
		String cadena = "", status = "";;
		int no_doctos=0, cod_sub_apli_proy=0, frecuencia_interes=0, frecuencia_capital=0;
		int tipo_frecuencia_interes=0, tipo_frecuencia_capital=0;
		int no_electronico=0, dia_pago_interes=0, dia_pago_capital=0, dia_revision_tasa=0, ic_plazo=0;
		long no_contrato=0, no_prestamo=0; //F084-2006
		double valor_inicial=0.0;
		java.sql.Date fecha_ppi=null, fecha_ppc=null, fecha_venc=null, fecha_revi_int=null, fecha_apertura=null, fecha_adicion=null;
		int valor_tasa_cartera=0;
		String calcular_mora="S";					/* Valor 6) Calcular Mora */
		String intereses_vencidos="S";				/* Valor 9) Intereses Vencidos */
		String intereses_normales="S";				/* Valor 10) Intereses Normales */
		String tipo_tasa=null;						/* Valor 13) Tipo Tasa */
		String clase_tasa="A";						/* Valor 14) Clase Tasa */
		int spreadMora = 0;							/* Valor 16) Spread Mora */
		double spreadCorriente_1 = 0;				/* Valor 17) Spread_Corriente_1 */
		String rela_mate_1=null;					/* Valor 19) Relacion_Matematica_1 */
		String rela_mate_2=null;					/* Valor 20) Relacion_Matematica_2 */
		String rela_mate_mora=null;					/* Valor 21) Relacion_Matematica_Mora */
		String observaciones="Ingresado por Nafin Electr�nico";	/* Valor 22) Observaciones */
		double comision=0.0;						/* Valor 26) Comisi�n */
		String diciembre="0";						/* Valor 27) Diciembre */
		int dias_gracia_mora=0;						/* Valor 28) Dias_Gracia_Mora */
		String tipo_liqui_mes="1";					/* Valor 29) Tipo_Liquidacion_Mes */
		String tipo_liqui_ano="2";					/* Valor 30) Tipo_Liquidacion_Ano */
		String adicionado_por=usuario;				/* Valor 31) Adicionado_Por */
		String cobrar_iva="N";						/* Valor 35) Cobrar_Iva */
		//int codigoTasaMora = 0;						/* Valor 36) Codigo Tasa Mora */
		String codigoTasaMora = "0";						/* Valor 36) Codigo Tasa Mora */
		int tipo_frecuencia_revision=2;				/* Valor 37) Tipo_Frecuencia_Revision */
		String rela_mate_3=null;					/* Valor 38) Relacion_Matematica_3 */
		String tipo_redondeo="0";					/* Valor 43) Tipo_Redondeo */
		String tipo_calculo_mora="1";				/* Valor 44) Tipo_Calculo_Mora */
		int rec_frescos=0;							/* Valor 45) Recursos_Frescos */
		String amort_ult_prim=null;					/* Valor 46) Amortizacion_Ultima_Primera */
		double amort_ajuste=0.0;					/* Valor 26) Comisi�n */
		String forma_redondeo="0";					/* Valor 48) Forma_Redondeo */
		String desgloce_tasas=null;					/* Valor 49) Desgloce_Tasas */
		String cobro_int_ES="1";					/* Valor 50) Cobro_Int_Entrada_Salida */
		String clausula_venci="3";					/* Valor 52) Clausula_Vencimiento */
		String tipo_interes="";						/* Valor 53) Tipo_Interes */
		int periodo_capitalizacion;					/* Valor 54) Periodo_Capitalizacion */
		String valor_presente="N";					/* Valor 55) Valor_Presente */
		String pago_masivo="S";						/* Valor 58) Pago_Masivo */
		String tipo_renta=null;						/* Valor 59) Tipo_Renta */
		int no_cuotas_recortadas=0;					/* Valor 60) Numero_Cuotas_Recortadas */
		int tipo_mora=1;							/* Valor 61) Tipo_Mora */
		String tipoFrecuenciaRevIntMora = "null";	/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
		int frecuenciaRevIntMora = 0;				/* Valor 64) Frecuencia_Rev_Int_Mora */
		String tipo_tasa_int_mora=null;				/* Valor 67) Tipo_Tasa_Int_Mora */
		String amortizaciones;
		int plazoPPI = 0, moneda = 0;
		String credito_restruc = "";
		ResultSet rs, rs1;
		CallableStatement cs=null;
		boolean errorTransaccion = false;
		boolean errorDesconocido = false;
		String numeroClienteTroya = null, numeroSolicitudTroya = null;
		String cotizacionEspecifica = null, spred;
		String paramStore = "PR_K_Apertura_Prestamo.Pr_Alta_Prestamo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int ultimoDiaDelMesVenc = 0, ultimoDiaDelMes = 0;
		Calendar cal = new GregorianCalendar();
    int rs_codigo_base_operacion = 0;


		String querySolicPortal = "";//FODEA 011-2010 FVR

		AccesoDBOracle con=new AccesoDBOracle();
		try {
			con.conexionDB();
		} catch (Exception e){
			System.out.println("Error al conectar la Base de Datos");
		}


		try {
			query = "select i.ic_solic_portal " +
				"	, i.fn_monto_operacion " +
				"   , i.ig_codigo_sub_aplicacion_proy " +
				"   , i.ig_numero_contrato " +
				"   , s.ic_tabla_amort " +
				"   , s.df_ppi " +
				"   , s.df_ppc " +
				"   , s.df_v_descuento " +
				"   , trunc(SYSDATE) as df_adicion " +
				"   , trunc(s.df_v_descuento) - trunc(s.df_carga) as ig_plazo " +
				"	, s.ic_tabla_amort " +
				"	, s.ic_periodicidad_c " +
				"	, s.ic_periodicidad_i " +
				"	, s.cs_amort_ajuste " +
				"	, s.fg_importe_ajuste " +
				"	, trunc(s.df_ppi) - trunc(sysdate) as plazo_ppi " +
				"	, s.CS_TIPO_RENTA" +
				"	, s.IN_NUMERO_AMORT_REC" +
				"	, s.IC_MONEDA " +
				"	, s.CS_PLAZO_MENSUAL" +
				"	, s.IC_TASAIF" +
				"	, int.IC_FINANCIERA" +
				"	, decode(s.ic_producto_nafin, 7, decode(int.ig_tipo_piso, 1, p.IN_NUMERO_TROYA, null), null) as IN_NUMERO_TROYA " +
				"	, s.IG_NUMERO_SOLIC_TROYA " +
				"	, s.IC_SOLIC_ESP " +
				"	, s.FG_STIF " +
				"	, s.CG_RMIF " +
				"	, i.IG_NUMERO_LINEA_CREDITO as NUMERO_PROYECTO " +
				//"	, instr(CG_PROYECTOS_LOG,'p19:=9005') as FIDEPAFF" + // Para determinar si se trata de operaciones del FODEA 010-2009
				"  , s.ic_tipo_credito " +
				"	, t.cs_restructura " +
				"	, s.cg_tipo_interes "+//SE AGREGA POR F004-2010 FVR
				"	, 'PrestamosE.class' ORIGENQUERY " +
				" FROM com_interfase i " +
				"    , com_solic_portal s " +
				"	 , comcat_if int " +
				"	 , comcat_pyme p " +
				"	 , comcat_tipo_credito t " +
				" WHERE s.ic_solic_portal = i.ic_solic_portal " +
				"	  AND t.ic_tipo_credito = s.ic_tipo_credito " +
				"    AND s.ic_if = int.ic_if " +
				"	 AND s.ig_clave_sirac = p.in_numero_sirac (+) " +
				"    AND i.ig_numero_prestamo is null " +
				"    AND i.ig_numero_contrato is not null " +
				"    AND i.ig_numero_linea_credito is not null " +
				"    AND i.cc_codigo_aplicacion is null " +
				"    AND i.ic_error_proceso is null " +
				"    AND s.ic_estatus_solic = 2 " +
				"    AND s.ic_bloqueo = 3";
			//System.out.println(query);
			try {				rs=con.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("PrestamosE.java(query): " + query );
				throw sqle;
			}
			while(rs.next()) {
        rec_frescos = 0;
        frecuenciaRevIntMora = 0;
				tipo_tasa_int_mora=null;
				tipo_liqui_ano = "2";
				tipo_liqui_mes = "1";
				amortizaciones = "N";
				intereses_normales = "S";
				errorTransaccion = false;
				errorDesconocido=false;
				tipo_renta=null;
				folio=rs.getString("IC_SOLIC_PORTAL");
				cod_tipo_amort=rs.getString("IC_TABLA_AMORT");	/* Valor 3) Codigo_Tipo_Amortizacion */
				no_contrato=rs.getLong("IG_NUMERO_CONTRATO");			/* Valor 4) Numero_Contrato */ //F084-2006
				valor_inicial=rs.getDouble("FN_MONTO_OPERACION");		/* Valor 5) Valor_Inicial */
				fecha_ppi=rs.getDate("DF_PPI");			/* Valor 7) Fecha_Primer_Pago_Interes */
				fecha_ppc=rs.getDate("DF_PPC");			/* Valor 8) Fecha_Primer_Pago_Capital */
				fecha_venc=rs.getDate("DF_V_DESCUENTO");			/* Valor 23) Fecha_Vencimiento */
				fecha_adicion=rs.getDate("DF_ADICION");		/* Valor 32) Fecha_Adicion */
				ic_plazo=rs.getInt("IG_PLAZO");
				frecuencia_interes = rs.getInt("IC_PERIODICIDAD_I");					/* Valor 11) Frecuencia Interes */
				frecuencia_capital = rs.getInt("IC_PERIODICIDAD_C");					/* Valor 12) Frecuencia Capital */
				amort_ult_prim = rs.getString("CS_AMORT_AJUSTE");
				amort_ajuste = rs.getDouble("FG_IMPORTE_AJUSTE");
				no_electronico = rs.getInt("IC_SOLIC_PORTAL");		/* Valor 57) Numero_Electr�nico */
				//sub_apli_moneda = rs.getInt("SUB_APLI_MONEDA");
				moneda = rs.getInt("IC_MONEDA");
				plazoPPI = rs.getInt("PLAZO_PPI");
				tipo_renta = rs.getString("CS_TIPO_RENTA");
				valor_tasa_cartera = rs.getInt("IC_TASAIF");
				numeroClienteTroya = rs.getString("IN_NUMERO_TROYA");
				numeroSolicitudTroya = rs.getString("IG_NUMERO_SOLIC_TROYA");
				cotizacionEspecifica = (rs.getString("IC_SOLIC_ESP")==null)?"":rs.getString("IC_SOLIC_ESP");
				spreadCorriente_1 = rs.getDouble("FG_STIF");
				rela_mate_1 = (!"".equals(cotizacionEspecifica))?rs.getString("CG_RMIF"):null;

				credito_restruc = rs.getString("cs_restructura");//CODIGO POR MODIFICACION PARA PARAMETRO 45

				tipo_interes = rs.getString("cg_tipo_interes")==null?"":rs.getString("cg_tipo_interes");//SE AGREGA POR F004-2010 FVR

				if(credito_restruc.equals("S")){
					rec_frescos = 1;
				}

				if (cod_tipo_amort.equals("5")) {
					if (frecuencia_interes == 7 || frecuencia_interes == 14 ) {
						tipo_liqui_ano = "1";
					}

					//SE REALIZA AJUSTE SOLOCITADO POR OTHON 03/05/2010 SE AGREGA A ESTA CONDICION LA FINANCIERA 60044
					if (frecuencia_interes != 7 && frecuencia_interes != 14 && (rs.getInt("IC_FINANCIERA") == 60017 || rs.getInt("IC_FINANCIERA") == 60044)) {
						tipo_liqui_mes = "2";
					}

					if(tipo_interes.equals("")){//SE AGREGA POR F004-2010 FVR
						if (moneda == 1 ) {
							if (valor_tasa_cartera == 33) {
								tipo_interes = "S";
							} else {
								if ("N".equals(tipo_renta)){
									if ("S".equals(rs.getString("CS_PLAZO_MENSUAL"))) {
										tipo_interes = "S";
									} else {
										tipo_interes = "C";
									}
								} else {
									if (plazoPPI > 31) {
										tipo_interes = "C";
									} else {
										tipo_interes = "S";
									}

								}
							}
						} else {
							tipo_interes = "S";
						}
					}//cierre if(tipo_interes)- F004-2010 FVR
				} else {
					if(tipo_interes.equals("")){//SE AGREGA POR F004-2010 FVR
						if (moneda == 1 && cod_tipo_amort.equals("1")) {
							tipo_interes = (ic_plazo > 31)?"C":"S";		//Si el plazo es mayor a 31 d�as es Compuesto de lo contrario es simple
						}else if (moneda == 1 && cod_tipo_amort.equals("2")) {
							tipo_interes = (plazoPPI > 31)?"C":"S";		//Si el plazo es mayor a 31 d�as es Compuesto de lo contrario es simple
						} else if (moneda == 1 && !cod_tipo_amort.equals("1") && frecuencia_interes == 1) {
							tipo_interes = "S";
						} else if (moneda == 1 && !cod_tipo_amort.equals("1") && frecuencia_interes > 1) {
							tipo_interes = "C";
						}
						if (moneda == 1 && plazoPPI > 31) {
							tipo_interes = "C";
						}

						if(moneda==54)
							tipo_interes = "S";
					}//cierre if(tipo_interes)- F004-2010 FVR

				}
        //AJUSTE SOLICITADO POR GORE PARA FIDE Y SE/FIDE
        if (cod_tipo_amort.equals("3") && (rs.getInt("IC_FINANCIERA") == 60017 || rs.getInt("IC_FINANCIERA") == 60044)) {
          tipo_liqui_mes = "2";
        }

        queri = "SELECT /*+ index(proy lc_pk_pro)*/ codigo_base_operacion " +
							" FROM lc_proyecto WHERE numero_linea_credito = " + rs.getInt("NUMERO_PROYECTO");
        try {
          rs1=con.queryDB(queri);
          if(rs1.next()) {
            rs_codigo_base_operacion = rs1.getInt("codigo_base_operacion");
          }//if(rs1.next())
          rs1.close();
          con.cierraStatement();
        } catch(SQLException sqle) {
          System.out.println("PrestamosE.java(queri): " + queri );
          throw sqle;
        }

				if(tipo_interes.equals("")){//SE AGREGA POR F004-2010 FVR
					// MODIFICACIONES PARA EL PROGRAMA EMERGENTE DE TABASCO
					// BY EGB 08/01/2007
					if (valor_tasa_cartera==71 || valor_tasa_cartera==26){
						if (
						rs_codigo_base_operacion == 7810 ||
						rs_codigo_base_operacion == 7811 ||
						rs_codigo_base_operacion == 7812 ||
						rs_codigo_base_operacion == 7813 ||
						rs_codigo_base_operacion == 25102
						) {
						  tipo_interes = "S";
						}//if
					}

					// MODIFICACIONES PARA EL PROGRAMA EMERGENTE DE INFLUENZA
					// BY FVR 14/05/2009
					if (valor_tasa_cartera==20){
						if (rs_codigo_base_operacion == 7818) {
							tipo_interes = "S";
						}//if
					}
				}//CIERRE IF(TIPO_INTERES) F004-2010 FVR

				periodo_capitalizacion = (tipo_interes.equals("C"))?1:0;


				int diaFechaVenc = Integer.parseInt(fecha_venc.toString().substring(8,10));
				int mesFechaVenc = Integer.parseInt(fecha_venc.toString().substring(5,7));
				int anioFechaVenc = Integer.parseInt(fecha_venc.toString().substring(0,4));


				int diaFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(8,10));
				int mesFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(5,7));
				int anioFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(0,4));

				int diaFechaPPC =  Integer.parseInt(fecha_ppc.toString().substring(8,10));
				int mesFechaPPC = Integer.parseInt(fecha_ppc.toString().substring(5,7));
				int anioFechaPPC = Integer.parseInt(fecha_ppc.toString().substring(0,4));

				int ultimoDiaMesPPI = 0, ultimoDiaMesPPC = 0 ;
				if(mesFechaPPI==2){
					Calendar calPPI = new GregorianCalendar();
					calPPI.setTime(fecha_ppi);
					ultimoDiaMesPPI = calPPI.getActualMaximum(Calendar.DAY_OF_MONTH);
				}

				if(mesFechaPPC==2){
					Calendar calPPC = new GregorianCalendar();
					calPPC.setTime(fecha_ppc);
					ultimoDiaMesPPC = calPPC.getActualMaximum(Calendar.DAY_OF_MONTH);
				}


				if (cod_tipo_amort.equals("5")) {
					if ("N".equals(tipo_renta)){
						if (diaFechaVenc!=diaFechaPPI){
							intereses_normales = "N";
						} else if (diaFechaVenc==diaFechaPPI){
							intereses_normales = "S";
						}
					}else if ("S".equals(tipo_renta)){
						intereses_normales = "S";
					}

				}


				/*if (diaFechaPPI == 30 || diaFechaPPI == 31 || (mesFechaPPI==2&&diaFechaPPI==ultimoDiaMesPPI)) {
					// JRFH  GAG PCS 22/10/2004
					// EGB 16/11/2004
					ultimoDiaDelMes = 0;
					if (mesFechaVenc == 2) {
						Calendar cal = new GregorianCalendar();
						cal.set(Calendar.YEAR, anioFechaVenc);
						cal.set(Calendar.MONTH, mesFechaVenc - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
						cal.set(Calendar.DATE, 1);
						ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					}

					if (diaFechaPPI==30&&(mesFechaVenc != 2 && diaFechaVenc == 30) || (mesFechaVenc == 2 && diaFechaVenc == ultimoDiaDelMes )) {
						//dia_revision_tasa = dia_pago_interes = dia_pago_capital =30;
						dia_revision_tasa = dia_pago_interes = 30;
					} else {
						dia_revision_tasa = dia_pago_interes = dia_pago_capital =31;
					}
				}*/

				/* Valor 40) Dia_Pago_Interes */
				// EGB 11/04/2006
				if (diaFechaPPI == 30 || diaFechaPPI == 31 || (mesFechaPPI==2 && diaFechaPPI==ultimoDiaMesPPI) ) {
					if (cod_tipo_amort.equals("5") && "N".equals(tipo_renta)) { // PLAN DE PAGOS
						dia_pago_interes=diaFechaPPI;
					} else {

						cal.set(Calendar.YEAR, anioFechaVenc);
						cal.set(Calendar.MONTH, mesFechaVenc - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
						cal.set(Calendar.DATE, 1);
						ultimoDiaDelMesVenc = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

						cal.set(Calendar.YEAR, anioFechaPPI);
						cal.set(Calendar.MONTH, mesFechaPPI - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
						cal.set(Calendar.DATE, 1);
						ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);


						if (diaFechaPPI == ultimoDiaDelMes && diaFechaVenc == ultimoDiaDelMesVenc ) {
							dia_pago_interes = 31;
						} else {
							dia_pago_interes = 30;
						}
					}
				} else {
					dia_pago_interes=diaFechaPPI;
				}


				/* Valor 41) Dia_Pago_Capital */
				// EGB 11/04/2006
				if (diaFechaPPC == 30 || diaFechaPPC == 31 || (mesFechaPPC==2 && diaFechaPPC==ultimoDiaMesPPC) ) {
					if (cod_tipo_amort.equals("5") && "N".equals(tipo_renta)) { // PLAN DE PAGOS
						dia_pago_capital=diaFechaPPC;
					} else {
						cal.set(Calendar.YEAR, anioFechaVenc);
						cal.set(Calendar.MONTH, mesFechaVenc - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
						cal.set(Calendar.DATE, 1);
						ultimoDiaDelMesVenc = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

						cal.set(Calendar.YEAR, anioFechaPPC);
						cal.set(Calendar.MONTH, mesFechaPPC - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
						cal.set(Calendar.DATE, 1);
						ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

						if (diaFechaPPC == ultimoDiaDelMes && diaFechaVenc == ultimoDiaDelMesVenc ) {
							dia_pago_capital = 31;
						} else {
							dia_pago_capital = 30;
						}
					}
				} else {
					dia_pago_capital=diaFechaPPC;
				}

				// EGB 15/08/2006
				dia_revision_tasa=dia_pago_interes;	/* Valor 42) Dia_Revision_Tasa */

				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("PrestamosE.java(paramStore): " + paramStore );
					throw sqle;
				}

				cs.setNull(1, Types.INTEGER);				/* Valor 1) Numero_Prestamo */
				cs.registerOutParameter(1, Types.INTEGER);
				//cs.setInt(2, valor_tasa_cartera);
				cs.setNull(2, Types.INTEGER);
				cs.setString(3, cod_tipo_amort);
				cs.setLong(4, no_contrato); //F084-2006
				cs.setDouble(5, valor_inicial);
				cs.setString(6, calcular_mora);
				cs.setDate(7, fecha_ppi,Calendar.getInstance());
				cs.setDate(8, fecha_ppc,Calendar.getInstance());
				cs.setString(9, intereses_vencidos);
				cs.setString(10, intereses_normales);
				/* Foda 044*/
				if (cod_tipo_amort.equals("5") && "N".equals(tipo_renta)) {
					frecuencia_capital = 0;
				}

				cs.setInt(11, frecuencia_interes);
				cs.setInt(12, frecuencia_capital);
				cs.setString(13, tipo_tasa);
				cs.setString(14, clase_tasa);
				cs.setNull(15, Types.INTEGER);				/* Valor 15) Periodo_Tasa */
				/* Foda 044*/
				/* Valor 16) Spread_Mora */
				if (cod_tipo_amort.equals("5") ) {
					spreadMora = 2;
					rela_mate_mora = "*";
					cs.setInt(16, spreadMora);
				} else {
					rela_mate_mora = null;
					cs.setNull(16, Types.DOUBLE);
				}

				/* Valor 17) Spread_Corriente_1 */
				if (!"".equals(cotizacionEspecifica)) {
					cs.setDouble(17, spreadCorriente_1);
				} else {
					cs.setNull(17, Types.DOUBLE);
				}


				cs.setNull(18, Types.DOUBLE);			/* Valor 18) Spread_Corriente_2 */
				cs.setString(19, rela_mate_1);
				cs.setString(20, rela_mate_2);
				cs.setString(21, rela_mate_mora);
				cs.setString(22, observaciones);
				cs.setDate(23, fecha_venc,Calendar.getInstance());
				//if (cod_tipo_amort.equals("5") ) {
				queri = "select fecha_hoy from mg_calendario where codigo_empresa=1 AND codigo_aplicacion = 'BPR'  ";
				try {
					rs1=con.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("PrestamosE.java(queri): " + queri );
					throw sqle;
				}
				if(rs1.next())
					fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
				rs1.close();
				con.cierraStatement();
				//} else {
				//	fecha_revi_int = null;
				//}


				cs.setDate(24, fecha_revi_int,Calendar.getInstance());
				cs.setDate(25, fecha_apertura,Calendar.getInstance());
				cs.setDouble(26, comision);
				cs.setString(27, diciembre);
				cs.setInt(28, dias_gracia_mora);
				cs.setString(29, tipo_liqui_mes);
				cs.setString(30, tipo_liqui_ano);
				cs.setString(31, adicionado_por);
				cs.setDate(32, fecha_adicion,Calendar.getInstance());

				/* Valor 33) Tipo_Frecuencia_Capital */
				if (cod_tipo_amort.equals("1") || cod_tipo_amort.equals("2")) {
					cs.setNull(33, Types.INTEGER);
				} else if (cod_tipo_amort.equals("5") && rs.getInt("IC_PERIODICIDAD_I")==14 && rs.getInt("IC_PERIODICIDAD_C") == 14 && "N".equals(tipo_renta) ){
					tipo_frecuencia_capital = 1;
					cs.setInt(33, tipo_frecuencia_capital);
				} else {
					tipo_frecuencia_capital = 2;
					cs.setInt(33, tipo_frecuencia_capital);
				}
				/* Valor 34) Tipo_Frecuencia_Interes */
				if (cod_tipo_amort.equals("1")) {
					cs.setNull(34, Types.INTEGER);
				} else if (cod_tipo_amort.equals("5") && rs.getInt("IC_PERIODICIDAD_I")==14 && rs.getInt("IC_PERIODICIDAD_C") == 14 && "N".equals(tipo_renta) ){
					tipo_frecuencia_interes = 1;
					cs.setInt(34, tipo_frecuencia_interes);
				} else {
					tipo_frecuencia_interes = 2;
					cs.setInt(34, tipo_frecuencia_interes);
				}
				cs.setString(35, cobrar_iva);
				/* Foda 044*/
				/* Valor 36) Codigo_Valor_Tasa_Mora */
				codigoTasaMora = (valor_tasa_cartera == 34 )?"20":"null";
				tipo_calculo_mora = (valor_tasa_cartera != 33 && valor_tasa_cartera != 34 )?"1":"2";
				tipo_calculo_mora = (rs_codigo_base_operacion == 9005)?tipo_calculo_mora:"2";// Solo cuando es del fodea 010-2009 se env�a 2

				if (cod_tipo_amort.equals("5") ) {
					if(moneda==1) {
						codigoTasaMora = "20";
						cs.setInt(36, Integer.parseInt(codigoTasaMora));
					} else {
						codigoTasaMora = "null";
						cs.setNull(36, Types.INTEGER);
					}
				} else {
					//tipo_calculo_mora = "1";
					if (valor_tasa_cartera == 34 ) {
						cs.setInt(36, Integer.parseInt(codigoTasaMora));
					} else {
						cs.setNull(36, Types.INTEGER);
					}
				}
				cs.setInt(37, tipo_frecuencia_revision);
				cs.setString(38, rela_mate_3);
				cs.setNull(39, Types.DOUBLE);		/* Valor 39) Spread_Corriente_3 */
				cs.setInt(40, dia_pago_interes);
				cs.setInt(41, dia_pago_capital);
				cs.setInt(42, dia_revision_tasa);
				cs.setString(43, tipo_redondeo);
				cs.setString(44, tipo_calculo_mora);
				cs.setInt(45, rec_frescos);
				cs.setString(46, amort_ult_prim);
				/* Valor 47) Monto_Amortizacion_Ajuste */
				if (amort_ult_prim == null) {
					cs.setNull(47, Types.DOUBLE);
				} else {
					cs.setDouble(47, amort_ajuste);
				}
				cs.setString(48, forma_redondeo);
				cs.setString(49, desgloce_tasas);
				cs.setString(50, cobro_int_ES);
				cs.setNull(51, Types.DOUBLE);				/* Valor 51) Tasa_Total_Moratoria */
				cs.setString(52, clausula_venci);

		  if(tipo_interes.equals("")){//SE AGREGA POR F004-2010 FVR
			  //MODIFICACIONES PARA EL PROGRAMA PFAE II
			  // BY EGB
			  if (rs_codigo_base_operacion == 9005 && frecuencia_interes == 2 && frecuencia_capital == 2) {
					tipo_interes = "C";
					periodo_capitalizacion = 1;
			  }//if
			  if (rs_codigo_base_operacion == 9005 && frecuencia_interes == 1 && frecuencia_capital == 1) {
					tipo_interes = "S";
					periodo_capitalizacion = 0;
			  }//if
		  }//CIERRE IF(TIPO_INTERES) F004-2010 FVR


				cs.setString(53, tipo_interes);
				cs.setInt(54, periodo_capitalizacion);
				cs.setString(55, valor_presente);
				cs.setNull(56, Types.INTEGER);				/* Valor 56) Codigo_Comision */
				cs.setInt(57, no_electronico);
				cs.setString(58, pago_masivo);

				// Foda 044 - 2004
				if (cod_tipo_amort.equals("5")) {
					no_cuotas_recortadas = rs.getInt("IN_NUMERO_AMORT_REC");
					if ("S".equals(tipo_renta)) {
						cs.setNull(60, Types.INTEGER);
					} else {
						amortizaciones = "S";  // Determina si la operacion tiene amortizaciones por registrar
						cs.setInt(60, no_cuotas_recortadas);
					}

				} else {
					tipo_renta = null;
					//no_cuotas_recortadas = 0;
					cs.setNull(60, Types.INTEGER);
					//cs.setInt(60, no_cuotas_recortadas);
				}

				cs.setString(59, tipo_renta);
				//cs.setInt(60, no_cuotas_recortadas);
				cs.setInt(61, tipo_mora);
				cs.setNull(62, Types.INTEGER);				/* Valor 62) Codigo_Grupo_Comisi�n */
				// Foda 044 - 2004
				/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
				/* Valor 64) Frecuencia_Rev_Int_Mora */
				/* Valor 65) Dia_Revision_Tasa_Mora */
				/* Valor 66) Fecha_revision_mora */
				tipoFrecuenciaRevIntMora = (valor_tasa_cartera == 34 )?"2":"null";
				//tipo_tasa_int_mora = (frecuencia_interes == 14)?"5":null;
				tipo_tasa_int_mora = (valor_tasa_cartera == 33 || valor_tasa_cartera == 34 || frecuencia_interes == 14)?"5":tipo_tasa_int_mora;

				if (cod_tipo_amort.equals("5")) {
					tipoFrecuenciaRevIntMora = (frecuencia_interes==14)?"1":"2";
					frecuenciaRevIntMora = (frecuencia_interes==14)?14:1;
					// JRFH  GAG PCS 22/10/2004
					if("2".equals(tipoFrecuenciaRevIntMora))
						frecuenciaRevIntMora = 1;

					cs.setInt(63, Integer.parseInt(tipoFrecuenciaRevIntMora));
					cs.setInt(64, frecuenciaRevIntMora);
					cs.setInt(65, dia_pago_interes);
					cs.setDate(66, fecha_revi_int,Calendar.getInstance());
				} else {
					if (valor_tasa_cartera == 34 ) {
						cs.setInt(63, Integer.parseInt(tipoFrecuenciaRevIntMora));
					// JRFH  GAG PCS 22/10/2004
						if("2".equals(tipoFrecuenciaRevIntMora)){
							frecuenciaRevIntMora = 1;
							cs.setInt(64, frecuenciaRevIntMora);
						}else{
							cs.setNull(64, Types.INTEGER);
						}
					} else {
						cs.setNull(63, Types.INTEGER);
						cs.setNull(64,Types.INTEGER);
					}
					//cs.setNull(64, Types.INTEGER);
					if (valor_tasa_cartera == 34 ) {
						cs.setInt(65, dia_pago_interes);
					} else {
						cs.setNull(65, Types.INTEGER);
					}
					if ("5".equals(tipo_tasa_int_mora)) {
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
					} else {
						cs.setNull(66, Types.DATE);
					}
				}
				cs.setString(67, tipo_tasa_int_mora);		/* Valor 67) Tipo_Tasa_Int_Mora */

				//FODA 099 - Interfase N@E-Troya
				cs.setString(68, numeroClienteTroya);		/* Valor 68) Numero Cliente Troya */
				cs.setString(69, numeroSolicitudTroya);		/* Valor 69) N�mero Sol Troya Nafin */
				cs.setNull(70, Types.INTEGER);				/* Valor 70) Numero_epo */
				cs.setNull(71, Types.INTEGER);				//SOLO PARA NAFIN EMPRESARIAL
				cs.setNull(72, Types.INTEGER);				/* Valor 72) Estatus */
				cs.registerOutParameter(72, Types.INTEGER);

				cadena="P1:=null;"+"\n"+
					"P2:=null;\n"+
					"P3:='"+cod_tipo_amort+"';\n"+
					"P4:="+no_contrato+";\n"+
					"P5:="+valor_inicial+";\n"+
					"P6:='"+calcular_mora+"';\n"+
					"P7:=to_date('"+fecha_ppi+"','yyyy-mm-dd');\n"+
					"P8:=to_date('"+fecha_ppc+"','yyyy-mm-dd');\n"+
					"P9:='"+intereses_vencidos+"';\n"+
					"P10:='"+intereses_normales+"';\n"+
					"P11:="+frecuencia_interes+";\n";
				/* Foda 044*/
				if (cod_tipo_amort.equals("5") && "N".equals(tipo_renta) && rs.getInt("IC_PERIODICIDAD_I")==14 && rs.getInt("IC_PERIODICIDAD_C") == 14 ) {
					cadena+="P12:=0;\n";
				} else {
					cadena+="P12:="+frecuencia_capital+";\n";
				}
				cadena+="P13:="+tipo_tasa+";\n"+
					"P14:='"+clase_tasa+"';\n"+
					"P15:=null;"+"\n";
				if (cod_tipo_amort.equals("5") ) {
					cadena+="P16:="+spreadMora+";\n";
				} else {
					cadena+="P16:=null;\n";
				}
				if (!"".equals(cotizacionEspecifica)) {
					cadena+="P17:="+spreadCorriente_1+";\n";
				} else {
					cadena+="P17:=null;\n";
				}

					cadena+="P18:=null;\n"+
					"P19:="+rela_mate_1+";\n"+
					"P20:="+rela_mate_2+";\n"+
					"P21:="+rela_mate_mora+";\n"+
					"P22:='"+observaciones+"';\n"+
					"P23:=to_date('"+fecha_venc+"','yyyy-mm-dd');\n";
				if (cod_tipo_amort.equals("5") ) {
					cadena+="P24:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n";
				} else {
					cadena+="P24:="+fecha_revi_int+";\n";
				}
				cadena+="P25:="+fecha_apertura+";\n"+
					"P26:="+comision+";\n"+
					"P27:='"+diciembre+"';\n"+
					"P28:="+dias_gracia_mora+";\n"+
					"P29:='"+tipo_liqui_mes+"';\n"+
					"P30:='"+tipo_liqui_ano+"';\n"+
					"P31:='"+adicionado_por+"';\n"+
					"P32:=to_date('"+fecha_adicion+"','yyyy-mm-dd');\n";
				if (cod_tipo_amort.equals("1") || cod_tipo_amort.equals("2")) {
					cadena+="P33:=null;\n";
				} else {
					cadena+="P33:="+tipo_frecuencia_capital+";\n";
				}
				if (cod_tipo_amort.equals("1")) {
					cadena+="P34:=null;\n";
				} else {
					cadena+="P34:="+tipo_frecuencia_interes+";\n";
				}
				cadena+="P35:='"+cobrar_iva+"';\n" +
					"P36:="+codigoTasaMora+";\n" +
					"P37:="+tipo_frecuencia_revision+";\n"+
					"P38:="+rela_mate_3+";\n"+
					"P39:=null;\n"+
					"P40:="+dia_pago_interes+";\n"+
					"P41:="+dia_pago_capital+";\n"+
					"P42:="+dia_revision_tasa+";\n"+
					"P43:='"+tipo_redondeo+"';\n"+
					"P44:='"+tipo_calculo_mora+"';\n"+
					"P45:="+rec_frescos+";\n"+
					"P46:="+amort_ult_prim+";\n";
				if (amort_ult_prim == null) {
					cadena+="P47:=null;\n";
				} else {
					cadena+="P47:="+amort_ajuste+";\n";
				}
				cadena+="P48:='"+forma_redondeo+"';\n"+
					"P49:="+desgloce_tasas+";\n"+
					"P50:='"+cobro_int_ES+"';\n"+
					"P51:=null;\n"+
					"P52:='"+clausula_venci+"';\n"+
					"P53:='"+tipo_interes+"';\n"+
					"P54:="+periodo_capitalizacion+";\n"+
					"P55:='"+valor_presente+"';\n"+
					"P56:=null;"+"\n"+
					"P57:="+no_electronico+";\n"+
					"P58:='"+pago_masivo+"';\n"+
					"P59:="+tipo_renta+";\n";
				if ("5".equals(cod_tipo_amort)) {
					if ("S".equals(tipo_renta)) {
						cadena+="P60:=null;\n";
					} else {
						cadena+="P60:="+no_cuotas_recortadas+";\n";
					}
				} else {
					cadena+="P60:=null;\n";
				}

				cadena+="P61:="+tipo_mora+";\n"+
						"P62:=null;\n"+
						"P63:="+tipoFrecuenciaRevIntMora+";\n"+
						"P64:="+frecuenciaRevIntMora+";\n";
				if (cod_tipo_amort.equals("5") ) {
					//cadena+="P64:="+frecuenciaRevIntMora+";"+"\n"+
					  cadena+=
						"P65:="+dia_pago_interes+";\n"+
						"P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n";
				} else {
					//cadena+="P64:=null;"+"\n";
					if (valor_tasa_cartera == 34 ) {
						cadena+="P65:="+dia_pago_interes+";\n";
					} else {
						cadena+="P65:=null;\n";
					}
					if ("5".equals(tipo_tasa_int_mora)) {
						cadena+="P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n";
					} else {
						cadena+="P66:=null;\n";
					}
				}
				cadena+="P67:="+tipo_tasa_int_mora+";\n"+
						"P68:="+numeroClienteTroya+";\n"+
						"P69:="+numeroSolicitudTroya+";\n"+
						"P70:=null;\n"+
						"P71:=null;\n"+
						"P72:=null;\n";
				System.out.println(cadena);

				// TEMPORAL
				// ELIMINAR CUANDO SE DESARROLLE TIPO RENTA EN SIRAC
				// PARA OPERACIONES DE COTIZACIONES ESPECIFICAS

				if ("S".equals(tipo_renta) && !"".equals(cotizacionEspecifica)) {
					status = "9999";
					cadena+= "****OPERACION TIPO RENTA DE COTIZACION DE TESORERIA BLOQUEADA****\n";
				} else {

					try {
						cs.execute();
							no_prestamo = cs.getLong(1); //F084-2006
							status = cs.getString(72);
							if (status == null) {
								errorDesconocido = true;
								status="8";
							}
					} catch(SQLException sqle){
						sqle.printStackTrace();
						System.out.println(sqle);
						cadena += sqle;
						errorDesconocido = true;
						status="9";
					}
				}

				System.out.println("No Prestamo: "+no_prestamo+" Estatus: "+status);
				cadena += "No Prestamo: "+no_prestamo+" Estatus: "+status;

									/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					con.terminaTransaccion(false); // El rollback al Store Procedure

					if (!errorDesconocido) {
						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PR'";
						try {
							rs1=con.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("PrestamosE.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}

					queri = "select count(1) from com_interfase where ic_solic_portal="+folio;
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("PrestamosE.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); con.cierraStatement();

					if(no_doctos!=0) { // Se valida la concurrencia
						queri = "update com_interfase " +
								"	set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" 	, ic_error_proceso="+status+
								"   , df_hora_proceso=SYSDATE"+
								"   , ig_proceso_numero = 3"+
								"   , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where ic_solic_portal="+folio;

						//FODEA 011-2010 FVR-INI
						querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET ic_error_proceso = ? " +
							" , cc_codigo_aplicacion = ? "+
							" WHERE ic_solic_portal = ? ";
						//FODEA 011-2010 FVR-FIN
					}
//						System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos de error en com_interfase.");
						errorTransaccion = true;
					}

					//FODEA 011-2010 FRV-INI
					try {
						PreparedStatement ps = con.queryPrecompilado(querySolicPortal);
						ps.setInt(1, Integer.parseInt(status));
						ps.setString(2,codigo_aplicacion);
						ps.setInt(3, Integer.parseInt(folio));
						ps.executeUpdate();
						ps.close();
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_solic_portal");
						errorTransaccion = true;
					}
					//FODEA 011-2010 FRV-FIN
				} else {
					/* Si no hubo ningun error se actualiza el n�mero de pr�stamo en com_interfase */

					queri = "Update com_interfase " +
							"	set ig_numero_prestamo = " + no_prestamo +
							"   , df_hora_proceso=SYSDATE "+
							"   , DF_PRESTAMOS=SYSDATE "+
							"   , ig_proceso_numero = 3"+
							"	, cg_amortizacion = '" +amortizaciones+ "'"+
							"   , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" WHERE ic_solic_portal = " + folio ;

//							System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el pr�stamo en com_interfase");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//while
			if (cs !=null) cs.close();
			rs.close(); con.cierraStatement();


			//*****************************************//
			// SECCION LLENADO TABLAS TEMPORALES EN SIRAC
			// FODA 044 - 2004
			// AMORTIZACION 5 (PLAN DE PAGOS)
			//*****************************************//

			int numeroCuota = 0;
			no_prestamo = 0;
			int agenciaProy = 0;
			int subAplicacion = 0;
			String fechaVencAmort = null, tipoAmort = null, pagado = null;
			String fechaPagada = null;
			String montoAmort = null;
			String montoAmortPagado = null;


			query = " SELECT /*+	ordered	index (S IN_COM_SOLIC_PORTAL_01_NUK) use_nl(s i)*/ " +
						"   s.ic_solic_portal " +
						"   , i.ig_numero_prestamo " +
						"   , i.ig_codigo_agencia_proy " +
						"   , s.ic_tipo_credito " +
						"     FROM com_solic_portal s, com_interfase i " +
						"    WHERE s.ic_solic_portal = i.ic_solic_portal " +
						"      AND i.ig_numero_prestamo IS NOT NULL " +
						"      AND i.ig_numero_contrato IS NOT NULL " +
						"      AND i.ig_numero_linea_credito IS NOT NULL " +
						"      AND i.cc_codigo_aplicacion IS NULL " +
						"      AND i.ic_error_proceso IS NULL " +
						"      AND s.ic_estatus_solic = 2 " +
						"      AND s.ic_bloqueo = 3 " +
						"	   AND i.cg_amortizacion = 'S' ";
			//System.out.println(query);
			try {
				rs=con.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("PrestamosE.java(query): " + query );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				folio = rs.getString("IC_SOLIC_PORTAL");
				no_prestamo = rs.getLong("IG_NUMERO_PRESTAMO"); //F084-2006
				agenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				subAplicacion = rs.getInt("IC_TIPO_CREDITO");

				queri = "DELETE PR_TMP_PLAN_PAGO_APLICAR where NUMERO_PRESTAMO = " + no_prestamo;
				con.ejecutaSQL(queri);
				queri = "DELETE PR_TMP_SALDOS_PLAN_PAGO_APL where NUMERO_PRESTAMO = " + no_prestamo;
				con.ejecutaSQL(queri);

				queri = "SELECT IC_AMORT_PORTAL " +
						" , CG_TIPO_AMORT " +
						" , FN_MONTO " +
						" , TO_CHAR(DF_VENCIMIENTO, 'DD/MM/YYYY') as DF_VENCIMIENTO " +
						" FROM com_amort_portal " +
						" WHERE ic_solic_portal="+folio +
						" order by ic_amort_portal ";
				try {
					rs1=con.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("PrestamosE.java(queri): " + queri );
					throw sqle;
				}
				while(rs1.next()) {
					numeroCuota = rs1.getInt("IC_AMORT_PORTAL");
					fechaVencAmort = "to_date('"+ rs1.getString("DF_VENCIMIENTO")+"','DD/MM/YYYY')";
					tipoAmort = rs1.getString("CG_TIPO_AMORT");
					pagado = ("R".equals(tipoAmort))?"S":"N";
					fechaPagada = ("R".equals(tipoAmort))?fechaVencAmort:null;
					montoAmort = rs1.getString("FN_MONTO");
					montoAmortPagado = ("R".equals(tipoAmort))?montoAmort:null;

					queri = "Insert Into PR_TMP_PLAN_PAGO_APLICAR (" +
							"SECUENCIA_DESEMBOLSO, NUMERO_CUOTA, NUMERO_PRESTAMO, CODIGO_EMPRESA, CODIGO_AGENCIA, CODIGO_SUB_APLICACION " +
							", FECHA_VENCE, TIPO_CUOTA, PAGADO, FECHA_PAGADA, PORCENTAJE_EXIGIBILIDAD, CODIGO_ESTADO_CARTERA_CUOTA " +
							", PAGADO_FIN_MES, CODIGO_ESTADO_CARTERA_CUOTA_FM, ADICIONADO_POR, FECHA_ADICION, MODIFICADO_POR, FECHA_MODIFICACION " +
							", TASA_TOTAL_MORATORIA, CODIGO_TIPO_AMORTIZACION, TIPO_CREDITO, SOBRETASA_MORATORIA, FECHA_REAL_PAGO ) "+
							" values (1, "+numeroCuota+", " + no_prestamo + ", 1, " + agenciaProy + ", " + subAplicacion +
							" , "+fechaVencAmort+", '"+tipoAmort+"', '"+pagado+"', "+fechaPagada+", null, 'A' "+
							" , null, null, '"+adicionado_por+"', SYSDATE, null, null"+
							" , null, null, null, null, null )";

//							System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
						queri = "Insert Into PR_TMP_SALDOS_PLAN_PAGO_APL ("+
								"SECUENCIA_DESEMBOLSO, NUMERO_CUOTA, NUMERO_PRESTAMO, CODIGO_EMPRESA "+
								", CODIGO_AGENCIA, CODIGO_SUB_APLICACION, CODIGO_TIPO_SALDO, VALOR "+
								", VALOR_PAGADO, CODIGO_TIPO_SALDO_FIN_MES, VALOR_FIN_MES )"+
								" values (1, "+numeroCuota+ ", "+ no_prestamo + ", 1 " +
								" , " + agenciaProy + ", " + subAplicacion+", 1, " + montoAmort +
								" , " + montoAmortPagado +", null, null )";

//							System.out.println(queri);
						con.ejecutaSQL(queri);
						queri = "update com_interfase set cg_amortizacion = 'R' WHERE ic_solic_portal = " + folio;
						con.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el pr�stamo en com_interfase");
						errorTransaccion = true;
					}

				}
				rs1.close(); con.cierraStatement();

				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}

			}
			rs.close(); con.cierraStatement();


		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			System.out.println(con.mostrarError());
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
}
