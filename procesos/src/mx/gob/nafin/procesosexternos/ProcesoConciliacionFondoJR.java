package mx.gob.nafin.procesosexternos;

import com.netro.fondojr.FondoJunior;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ProcesoConciliacionFondoJR {

private final static Log log = ServiceLocator.getInstance().getLog(ProcesoConciliacionFondoJR.class);

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String ruta = "";

	try{
			ruta = args[0];//Este parametro es la ruta de publicacion del servidor
			FondoJunior fondoJunior = ServiceLocator.getInstance().remoteLookup("FondoJuniorEJB", FondoJunior.class);

			String procesoC  = fondoJunior.proceso();

			System.out.println("procesoC --->"+procesoC);


		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}


	}


}
