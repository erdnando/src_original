package mx.gob.nafin.procesosexternos;

import com.netro.dispersion.Dispersion;
import com.netro.electronica.OperacionElectronica;

import netropology.utilerias.ServiceLocator;

public class ProcesoSolicitudAutorizacionOPE {
	public static void main(String [] args)  {
		int codigoSalida = 0;
		int proceso = 0;
		String ruta = "", estatus ="";
		String directorioPlantilla = "";
	try{
			ruta = args[0];//Este parametro es la ruta de publicacion del servidor
			directorioPlantilla = ruta + "/00archivos/plantillas/38operaElectronica/";
			ruta +="/00tmp/38OperaElectronica/";
                        OperacionElectronica OperElec = obtenerEJB();
                        OperElec.envioCorreoConcentrado(ruta,directorioPlantilla);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        System.out.println(ex.getMessage());
		        ex.printStackTrace();
		        codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
		
		
	}
    private static OperacionElectronica obtenerEJB() throws Throwable {
            System.out.println("Inicio Conexion");
            OperacionElectronica OperElec = ServiceLocator.getInstance().remoteLookup("OperacionElectronicaEJB", OperacionElectronica.class);
            return OperElec;
    }

}