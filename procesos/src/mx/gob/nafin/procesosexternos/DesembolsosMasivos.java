package mx.gob.nafin.procesosexternos;

import com.netro.garantias.Garantias;

import netropology.utilerias.ServiceLocator;

public class DesembolsosMasivos {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String 	ic_if 			= null;

		try {
			ic_if	= args[0];

			System.out.println("incio procesarDesembolsosMasivos:::::");
			Garantias garantias = ServiceLocator.getInstance().remoteLookup("GarantiasEJB", Garantias.class);
			System.out.println("llamdo a procesarDesembolsosMasivos:::::");
			garantias.procesarDesembolsosMasivos();
			System.out.println("finaliza procesarDesembolsosMasivos:::::");

		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("procesarDesembolsosMasivos [ic_if]");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("procesarDesembolsosMasivos::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{

			System.exit(codigoSalida);
		}
    }//main

}//RegistroSaldosFIDECliente
