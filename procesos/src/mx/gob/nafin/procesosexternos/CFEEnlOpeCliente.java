package mx.gob.nafin.procesosexternos;

import com.netro.descuento.CFEEnlOperados;

public class CFEEnlOpeCliente {

	public static void main(String args[]) {
		//DocumentosEnl doctosEnl = new DocumentosEnl();
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();
		
		try {
			//doctosEnl.proceso(args[0], new CFEEnlOperados(args[0]),args[1]);
			cargaDoctos.procesoDoctos(args[0], new CFEEnlOperados(args[0]), "");
//			System.out.println("args[1]" + args[1]);
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java CFEEnlOpeCliente 8 O. El 8 es el n�mero de la EPO para CFE.");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
	}

}