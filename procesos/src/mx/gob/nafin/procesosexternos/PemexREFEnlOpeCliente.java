package mx.gob.nafin.procesosexternos;

import com.netro.descuento.PemexREFEnlOperados;

public class PemexREFEnlOpeCliente{

	public static void main(String args[]){
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();

		try {
			CargaDoctosEnl.procesoDoctos(args[0], new PemexREFEnlOperados(args[0]), "");
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
	}

}