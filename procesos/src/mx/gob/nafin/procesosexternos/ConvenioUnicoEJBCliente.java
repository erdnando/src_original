package mx.gob.nafin.procesosexternos;


import com.netro.afiliacion.Afiliacion;

import netropology.utilerias.ServiceLocator;


public class ConvenioUnicoEJBCliente{

	public static void main(String[] arg) {
		int codigoSalida = 0;
		try {
			procesoReafilia(arg[0]);
		} catch (Throwable e) {
			codigoSalida = 1;
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
	}

	static void procesoReafilia(String ruta) throws Throwable {
			StringBuffer log = new StringBuffer();
			String linea = "", rutArchivo = "";
			System.out.println("Inicio Conexion ");
			Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);
			log = afiliacion.reafiliacionesConvenioUnico(ruta);
	} //fin main proceso
}