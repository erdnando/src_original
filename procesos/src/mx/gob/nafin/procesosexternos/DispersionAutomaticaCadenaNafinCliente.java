package mx.gob.nafin.procesosexternos;

import com.netro.dispersion.Dispersion;

import netropology.utilerias.ServiceLocator;

public class DispersionAutomaticaCadenaNafinCliente {

	public static void main(String [] args)  {

		int codigoSalida = 0;

		try {

			String tipoDoctoDispersion = "";
			if(args.length > 0){
				tipoDoctoDispersion = args[0];
			}

			System.out.println("incio DispersionAutomaticaCadenaNafinCliente:::::");
			Dispersion dispersion = ServiceLocator.getInstance().remoteLookup("DispersionEJB", Dispersion.class);

			if(tipoDoctoDispersion.equals("DOCTOS_VENCIDOS")){
				System.out.println("Invocando la Dispersion Automatica de Documentos Vencidos para la Cadena Nafin");
				dispersion.realizaDispersionAutomaticaDoctosVencidosCadenaNafin();
			}else{
				System.out.println("Invocando la Dispersion Automatica de Documentos para la Cadena Nafin");
				dispersion.realizaDispersionAutomaticaDoctosCadenaNafin();
			}

		} catch (Exception e) {
			System.out.println("DispersionAutomaticaCadenaNafinCliente::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{

			System.exit(codigoSalida);
		}

    }

}
