package mx.gob.nafin.procesosexternos;

import com.netro.cesion.CesionEJB;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import netropology.utilerias.ServiceLocator;

public class NotifRegExpCesion  {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String 	fec_venc = "";

		try {

			SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy");
			Calendar calendar = Calendar.getInstance();
			calendar.add(calendar.DAY_OF_MONTH, 1);

			fec_venc = fHora.format(calendar.getTime());
			System.out.println("fec_Actual>>>>>>>>"+fec_venc);

			System.out.println("incio NotifRegExpCesion:::::");
		    CesionEJB cesion = ServiceLocator.getInstance().remoteLookup("CesionEJB", CesionEJB.class);
			System.out.println("llamado a NotifRegExpCesion:::::");

			cesion.procesoSolicRech();
			System.out.println("finaliza procesoSolicRech:::::");

		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("NotifRegExpCesion [] ");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("NotifRegExpCesion::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
    }//main
}