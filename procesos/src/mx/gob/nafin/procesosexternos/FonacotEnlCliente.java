package mx.gob.nafin.procesosexternos;


import com.netro.descuento.FonacotEnlace;

public class FonacotEnlCliente {

	public static void main(String args[]) {
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();
		try {
			CargaDoctosEnl.proceso(args[0], new FonacotEnlace(args[0]));
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java FonacotEnlCliente 8. El 8 es el n�mero de la EPO para Fonacot.");
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
	}

}