package mx.gob.nafin.procesosexternos;

import com.netro.credito.FIDECargaCE;

public class FIDEEnlAltaOperEJBCliente {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String 	ic_if 			= null;
		try {
				ic_if			= args[0];
				CargaCreditoEnl cargaCreditoEnl = new CargaCreditoEnl();
				CargaCreditoEnl.cargaCE(ic_if, new FIDECargaCE());
		} catch (ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("CargaFIDEEJBCliente [IF]");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Exception e) {
			System.out.println("CargaFIDEEJBCliente::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		}finally{
			System.exit(codigoSalida);
		}

    }//main

}//FIDEEJBCliente
