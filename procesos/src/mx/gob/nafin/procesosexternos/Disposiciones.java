package mx.gob.nafin.procesosexternos;

/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 10/Sept/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Financiamiento a Inventarios 1er Piso FODA 075
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	2/Oct/2003
*
* Autor Modificacion:		L.I. Hugo Díaz García
*
* Descripción: Implementación de Financiamiento a Distribuidores 2o Piso
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	10/Julio/2004
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 062 - 2004, Oficina Tramitadora
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	23/Julio/2004
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 044 - 2004, Amortizacion 5
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	06/Septiembre/2005
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 058 - 2005, Distribuidores Dólares
*
*
*************************************************************************************/
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;


public class Disposiciones
{
	public static void main(String args[])
	{
		String usuario = args[0];
		ResultSet rs;
		ResultSet rs1;
		ResultSet rs2;
		String cadena = "";

		PreparedStatement ps=null;
		CallableStatement cs=null;
		String sentenciaSQL;

		String folio = "";
		double montoOperacion;
		long numPrestamo;
		java.sql.Date fechaAdicion=null;
		java.sql.Date fechaCaptura;
		java.sql.Time fechaHoraCaptura;

		String codigoAgencia="";
		String codigoSubAplicacion="";
		String numLineaFondeo="";
		String codigoFinanciera="";
		String aprobadoPor="";
		String observaciones ="";
		String numLineaCredito="";
		String numLineaCreditoEmp="";
		String codigoAgenciaProy="";
		String codigoSubAplicacionProy="";
		String numContrato="";
		String fechaTemp="";
		String numeroCuenta="";
		int tipoPiso;
		int subAplicacion=211;
		int codigoTipoPago=0;
		int codigoCuenta=0;
		boolean existeCuenta;
		int cod_agencia=90;                 			/* Valor 1 */

		/* Variables cobranza */
		int codigoEmpresaPago=1;
		int codigoAgenciaPago=0;
		int codigoSubAplicacionPago=0;
		long prestamoPago=0;
		int porcentajePago=100;
		int tipoPago2=0;
		int icMoneda;
		int ic_epo = 0;
		String strProducto = null;
		String strFechaDiversos = null;
		String strFechaProyectos = null;
		String strFechaContratos = null;
		String strFechaPrestamos = null;

		String estatus="";
		boolean errorDesconocido = false;
		boolean errorTransaccion = false;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		String numPrestamoPedido="";

		String codigoAplicacion="";
		int existe;
		Hashtable oficTramXProd = new Hashtable();
		String strFrom, strCondiciones, strSelect;
		String paramStore = "PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Interfaz  inter = new Interfaz(); // F002-2016 
		String csFloating="N"; //NE_2018_01 QC

		AccesoDBOracle con = new AccesoDBOracle();
		try {
			con.conexionDB();
		} catch (Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try {
			sentenciaSQL = "select ic_producto_nafin||cc_tipo as llave, cg_agencia_sirac from comrel_cartera_x_producto";
			rs=con.queryDB(sentenciaSQL);
			while(rs.next()) {
				oficTramXProd.put(rs.getString("LLAVE"), rs.getString("CG_AGENCIA_SIRAC"));
			}
			rs.close();
			con.cierraStatement();

		} catch (Exception e){
			oficTramXProd.put("1F", "M");
			oficTramXProd.put("1C", "F");
			oficTramXProd.put("2-", "F");
			oficTramXProd.put("4-", "F");
			oficTramXProd.put("5-", "F");
			System.out.println("Exception. Error al obtener la parametrizacion de Oficina tramitadora, utilizando DEFAULTS: " + e);
		}


		try {

			sentenciaSQL = "select fecha_hoy as fechaAdicion from mg_calendario where codigo_aplicacion = 'BPR' AND CODIGO_EMPRESA = 1 ";
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			if (rs.next()) {
				fechaAdicion = rs.getDate("FECHAADICION");
			}
			rs.close();
			con.cierraStatement();

			sentenciaSQL = "select ig_codigo_tipo_pago2 from com_param_gral";
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			if (rs.next()) {
				tipoPago2 = rs.getInt("IG_CODIGO_TIPO_PAGO2");
			}
			rs.close();
			con.cierraStatement();

			sentenciaSQL =
				"  SELECT"   +
				"  	/*+ ordered"   +
				"  	index (s IN_COM_SOLICITUD_02_NUK)"   +
				"  	use_nl (c4 a3 ds d i DOM AG pe)*/"   +
				"  s.ic_folio     "   +
				"  , c4.ig_numero_prestamo "   +
				"  , c4.fn_monto_operacion "   +
				"  , c4.ig_codigo_agencia "   +
				"  , c4.ig_codigo_sub_aplicacion "   +
				"  , c4.ig_numero_linea_fondeo "   +
				"  , c4.ig_codigo_financiera "   +
				"  , c4.ig_aprobado_por     " +
				"  , c4.cg_descripcion"   +
				"  , c4.ig_numero_linea_credito "   +
				"  , c4.ig_codigo_agencia_proy "   +
				"  , c4.ig_codigo_sub_aplicacion_proy "   +
				"  , c4.ig_numero_contrato " +
				"  , TO_CHAR (SYSDATE, 'yyyy-mm-dd') AS fechacaptura "   +
				"  , a3.df_fecha_hora AS horacaptura " +
				"  , trunc(SYSDATE) AS fechacapt " +
				"  , i.ig_tipo_piso "   +
				"  , d.fn_monto_dscto "   +
				"  , c4.ig_numero_prestamo_pedido "   +
				"  , d.ic_moneda " +
				"  , AG.ig_agencia_sirac as ig_agencia_sirac_fiscal"   +
				"  , AG2.ig_agencia_sirac as ig_agencia_sirac_tramit"   +
				"  , ds.ic_epo"   +
				"  , ds.FN_PORC_COBRO_DSCTO " +
				"  , c4.cc_producto " +
				"  , to_char(c4.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"  , to_char(c4.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"  , to_char(c4.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"  , to_char(c4.DF_PRESTAMOS, 'dd/mm/yyyy') as DF_PRESTAMOS " +
				"  , s.df_fecha_solicitud "+
				"  , to_number(null) ic_linea_credito_emp "+
				"  , ds.CS_FLOATING   "+ ///NE_2018_01 QC
				"	  , 'Disposiciones(Descuento)' as OrigenQuery " +
				"  FROM"   +
				"  	com_solicitud s "   +
				"  	, com_control04 c4 "   +
				"  	, com_acuse3 a3 "   +
				"  	, com_docto_seleccionado ds "   +
				"  	, com_documento d "   +
				"  	, comcat_if i "   +
				"   , com_domicilio DOM"   +
				"   , comcat_agencia_sirac AG"   +
				"   , comcat_agencia_sirac AG2"   +
				"   , comrel_pyme_epo pe"   +
				"  WHERE	s.ic_folio = c4.ic_folio"   +
				"  AND s.cc_acuse = a3.cc_acuse"   +
				"  AND c4.cs_estatus = 'S'"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_documento = ds.ic_documento"   +
				"  AND ds.ic_documento = d.ic_documento"   +
				"  AND ds.ic_if = i.ic_if"   +
				"  AND ds.ic_linea_credito_emp is null "   +
				"  AND d.ic_pyme = DOM.ic_pyme"   +
				"  AND DOM.cs_fiscal = 'S'"   +
				"  AND DOM.ic_estado = AG.ic_estado (+)"   +
				"  AND d.ic_pyme = pe.ic_pyme"   +
				"  AND d.ic_epo = pe.ic_epo"   +
				"  AND pe.ic_oficina_tramitadora = AG2.ic_estado (+)"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_bloqueo = 3"   +
				"  UNION "   +
				"  SELECT"   +
				"  	/*+ ordered"   +
				"  	index (s IN_COM_SOLICITUD_02_NUK)"   +
				"  	use_nl (c4 a3 ds d i DOM AG pe)*/"   +
				"  s.ic_folio     "   +
				"  , c4.ig_numero_prestamo "   +
				"  , c4.fn_monto_operacion "   +
				"  , c4.ig_codigo_agencia "   +
				"  , c4.ig_codigo_sub_aplicacion "   +
				"  , c4.ig_numero_linea_fondeo "   +
				"  , c4.ig_codigo_financiera "   +
				"  , c4.ig_aprobado_por     " +
				"  , c4.cg_descripcion"   +
				"  , c4.ig_numero_linea_credito "   +
				"  , c4.ig_codigo_agencia_proy "   +
				"  , c4.ig_codigo_sub_aplicacion_proy "   +
				"  , c4.ig_numero_contrato " +
				"  , TO_CHAR (SYSDATE, 'yyyy-mm-dd') AS fechacaptura "   +
				"  , a3.df_fecha_hora AS horacaptura " +
				"  , trunc(SYSDATE) AS fechacapt " +
				"  , i.ig_tipo_piso "   +
				"  , d.fn_monto_dscto "   +
				"  , c4.ig_numero_prestamo_pedido "   +
				"  , d.ic_moneda " +
				"  , AG.ig_agencia_sirac as ig_agencia_sirac_fiscal"   +
				"  , AG2.ig_agencia_sirac as ig_agencia_sirac_tramit"   +
				"  , ds.ic_epo"   +
				"  , ds.FN_PORC_COBRO_DSCTO " +
				"  , c4.cc_producto " +
				"  , to_char(c4.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"  , to_char(c4.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"  , to_char(c4.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"  , to_char(c4.DF_PRESTAMOS, 'dd/mm/yyyy') as DF_PRESTAMOS " +
				"  , s.df_fecha_solicitud "+
				"  , lc.ic_linea_credito ic_linea_credito_emp "+
				"  , ds.CS_FLOATING   "+ //NE_2018_01 QC
				"	  , 'Disposiciones(Factoraje con Recurso)' as OrigenQuery " +
				"  FROM"   +
				"  	com_solicitud s "   +
				"  	, com_control04 c4 "   +
				"  	, com_acuse3 a3 "   +
				"  	, com_docto_seleccionado ds "   +
				"  	, com_documento d "   +
				"  	, emp_linea_credito lc "   +
				"  	, comcat_if i "   +
				"   , com_domicilio DOM"   +
				"   , comcat_agencia_sirac AG"   +
				"   , comcat_agencia_sirac AG2"   +
				"   , comrel_pyme_epo pe"   +
				"  WHERE	s.ic_folio = c4.ic_folio"   +
				"  AND s.cc_acuse = a3.cc_acuse"   +
				"  AND c4.cs_estatus = 'S'"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_documento = ds.ic_documento"   +
				"  AND ds.ic_documento = d.ic_documento"   +
				"  AND ds.ic_linea_credito_emp = lc.ic_linea_credito "   +
				"  AND ds.ic_if = i.ic_if"   +
				"  AND d.ic_pyme = DOM.ic_pyme"   +
				"  AND DOM.cs_fiscal = 'S'"   +
				"  AND DOM.ic_estado = AG.ic_estado (+)"   +
				"  AND d.ic_pyme = pe.ic_pyme"   +
				"  AND d.ic_epo = pe.ic_epo"   +
				"  AND pe.ic_oficina_tramitadora = AG2.ic_estado (+)"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_bloqueo = 3"   +
				"  ORDER BY ig_tipo_piso desc, df_fecha_solicitud ASC"  ;
//System.out.println(sentenciaSQL+";");

			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				existeCuenta= false;
				icMoneda = rs.getInt("IC_MONEDA");
				porcentajePago = 100;
				codigoAgenciaPago=0;

				/*
				Se asigna la variable de numero de prestamo obtenido en el credito de Financiamiento a Pedidos o la Cobranza Manual
				para determinar si se va a realizar una cobranza
				*/
				numPrestamoPedido = rs.getString("IG_NUMERO_PRESTAMO_PEDIDO");
				prestamoPago = rs.getLong("IG_NUMERO_PRESTAMO_PEDIDO");


				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_FOLIO");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				numPrestamo = rs.getLong("IG_NUMERO_PRESTAMO");

				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				observaciones = rs.getString("CG_DESCRIPCION");
				numLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO");
				codigoAgenciaProy = rs.getString("IG_CODIGO_AGENCIA_PROY");
				codigoSubAplicacionProy = rs.getString("IG_CODIGO_SUB_APLICACION_PROY");
				numContrato = rs.getString("IG_NUMERO_CONTRATO");
				String sFechaCaptura = rs.getString("FECHACAPTURA");
				fechaCaptura = rs.getDate("FECHACAPT");
				fechaHoraCaptura = rs.getTime("HORACAPTURA");
				ic_epo = rs.getInt("IC_EPO");

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");
				strFechaContratos = rs.getString("DF_CONTRATOS");
				strFechaPrestamos = rs.getString("DF_PRESTAMOS");

				numLineaCreditoEmp = rs.getString("ic_linea_credito_emp")==null?"":rs.getString("ic_linea_credito_emp");
				csFloating = rs.getString("CS_FLOATING")==null?"N":rs.getString("CS_FLOATING"); //NE_2018_01 QC

				//cs = con.ejecutaSP("PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Disposiciones.java(paramStore): " + paramStore );
					throw sqle;
				}

					//09.- CODIGO_TIPO_PAGO
					//10.- CODIGO_CUENTA
				if (tipoPiso==1) {
					/*subAplicacion = (numPrestamoPedido==null)?113:126; 
					if(!"".equals(numLineaCreditoEmp)) {
						subAplicacion = Integer.parseInt(codigoSubAplicacionProy);
					}
					if ( ic_epo == 256 && subAplicacion == 113) {
						subAplicacion = 126;
					}	*/		// COMENTO F002-2016	
					HashMap listDatoParam = (HashMap)inter.getValueParametrizado(String.valueOf(ic_epo),"1C","DS");//   F002-2016
					subAplicacion = Integer.parseInt((String)listDatoParam.get("2"));//   F002-2016
					codigoTipoPago = Integer.parseInt((String)listDatoParam.get("9"));//   F002-2016
					
					if (numPrestamoPedido==null) {
						//codigoTipoPago = 333;    F002-2016
						cs.setInt(9, codigoTipoPago);
						cs.setNull(10, Types.INTEGER);
						//Foda 062 - 2004, 068 - 2004
						// Infonavit Agencia 90
						cod_agencia = 90;
						//cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): rs.getInt("IG_AGENCIA_SIRAC_FISCAL");

					} else {
						/*
						String queri2 = "SELECT mcb.CODIGO_CUENTA, fpd.CODIGO_TIPO_PAGO "+
									" FROM MG_CUENTAS_BANCO mcb, MG_FORMA_PAGOS_DESEMBOLSO fpd"+
									" WHERE mcb.codigo_cuenta = fpd.codigo_cuenta_banco"+
									" AND NUMERO_CUENTA = (SELECT LC.cg_cuenta_clabe "+
								 	" 			   	 FROM com_anticipo a "+
									"				 	  , com_pedido_seleccionado ps "+
									"				 	  , com_linea_credito lc "+
									"				 WHERE a.ig_numero_prestamo =  " + numPrestamoPedido  +
									"				    AND a.ic_pedido = ps.ic_pedido "+
									"				    AND ps.ic_linea_credito = lc.ic_linea_credito)";
						*/

						// Modificado EGB 22/07/2004 Foda 049-2005 se accesa a sirac mediante el prestamo del crédito a pagar
						//	para obtener los datos necesarios a enviar
						String queri2 = " select /*+ index(prp pr_id8_pre)*/"   +
									" prmd.CODIGO_TIPO_PAGO, " +
									" prmd.CODIGO_CUENTA, " +
									" prp.CODIGO_SUB_APLICACION, "   +
									" prp.CODIGO_AGENCIA " +
									" from "   +
									"  pr_prestamos prp"   +
									" , pr_movimientos prm"   +
									" , pr_movimientos_desglose prmd"   +
									" where "   +
									" prp.NUMERO_PRESTAMO = prm.NUMERO_PRESTAMO"   +
									" and prp.CODIGO_EMPRESA = prm.CODIGO_EMPRESA"   +
									" and prp.CODIGO_AGENCIA = prm.CODIGO_AGENCIA "   +
									" and prp.CODIGO_SUB_APLICACION = prm.CODIGO_SUB_APLICACION"   +
									" and prm.FECHA_VALIDA = prmd.FECHA_VALIDA"   +
									" and prm.SECUENCIA = prmd.SECUENCIA"   +
									" and prm.CODIGO_USUARIO = prmd.CODIGO_USUARIO"   +
									" and prp.NUMERO_PRESTAMO = " + numPrestamoPedido +
									" and prm.CODIGO_OPERACION = 1"   +
									" AND prm.CODIGO_TIPO_TRANSACCION = 3"   +
									" and prmd.SECUENCIA_DESGLOSE = 1"  ;
						//System.out.println(queri2);

						try {
							rs2=con.queryDB(queri2);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(queri2): " + queri2 );
							throw sqle;
						}
						if (rs2.next()) {
							existeCuenta = true;
							codigoTipoPago = rs2.getInt("CODIGO_TIPO_PAGO");
							cs.setInt(9, codigoTipoPago);
							codigoCuenta = rs2.getInt("CODIGO_CUENTA");
							cs.setInt(10, codigoCuenta);
							codigoSubAplicacionPago = rs2.getInt("CODIGO_SUB_APLICACION");
							codigoAgenciaPago = rs2.getInt("CODIGO_AGENCIA");
						} else {
							cs.setNull(9, Types.INTEGER);
							cs.setNull(10, Types.INTEGER);
						}
						rs2.close();
						con.cierraStatement();
						//Foda 062 - 2004, 068 - 2004
						cod_agencia = ("T".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL"): 90;


					}
				}else {
					subAplicacion = (icMoneda==54)?221:211;
					cs.setNull(9, Types.INTEGER);
					cs.setNull(10, Types.INTEGER);
					//Foda 062 - 2004, 068 - 2004
					cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL") : 90;


				}

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setInt(2, subAplicacion);					//02.- CODIGO_SUB_APLICACION
				//F084-2006
				cs.setLong(3, numPrestamo);						//03.- NUMERO_PRESTAMO
				cs.setDate(4, fechaCaptura, Calendar.getInstance());					//04.- FECHA_CAPTURA
				cs.setDouble(5, montoOperacion);				//05.- VALOR_DESEMBOLSO
				cs.setString(6, usuario);						//06.- ADICIONADO_POR
				cs.setDate(7, fechaAdicion,Calendar.getInstance());					//07.- FECHA_ADICION (fecha del sistema)
				cs.setDouble(8, (double)0);						//08.- MONTO_AMORTIZACION_AJUSTE
				cs.setDouble(11, montoOperacion);				//11.- VALOR_PAGO
				cs.setNull(12, Types.INTEGER);					//12.- NUMERO_DE_CHEQUE
				if ( numPrestamoPedido== null ) {
					cs.setNull(13, Types.INTEGER);					//13.- CODIGO_EMPRESA_PAGO
					cs.setNull(14, Types.INTEGER);					//14.- CODIGO_AGENCIA_PAGO
					cs.setNull(15, Types.INTEGER);					//15.- CODIGO_SUB_APLICACION_PAGO
					cs.setNull(16, Types.INTEGER);					//16.- PRESTAMO_PAGO
					cs.setNull(17, Types.INTEGER);					//17.- PORCENTAJE_PAGO
					cs.setNull(18, Types.INTEGER);					//18.- TIPO_PAGO_2
				} else {

					if (rs.getString("FN_PORC_COBRO_DSCTO") == null) {
						sentenciaSQL = 	" SELECT nvl(fn_porc_docto_aplicado, 100) as fn_porc_docto_aplicado " +
										" , 'Disposiciones(Cobranza)' as proceso"   +
										" FROM com_solicitud s"   +
										" , com_cobro_credito cc"   +
										" WHERE cc.ic_documento (+) = s.ic_documento"   +
										" AND s.ic_folio = '"+folio+"'" ;
						//System.out.println(sentenciaSQL);
						ResultSet rs3 =null;
						try {
							rs3 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if (rs3.next()) {
							porcentajePago = rs3.getInt("FN_PORC_DOCTO_APLICADO");
							//codigoAgenciaPago = rs3.getInt("IG_CODIGO_AGENCIA_PROY");
							//codigoSubAplicacionPago = rs3.getInt("IG_CODIGO_SUB_APLICACION_PROY");
						}
						rs3.close();
						con.cierraStatement();
					} else {
						porcentajePago = rs.getInt("FN_PORC_COBRO_DSCTO");
					}

					cs.setInt(13, codigoEmpresaPago);				//13.- CODIGO_EMPRESA_PAGO
					cs.setInt(14, codigoAgenciaPago);				//14.- CODIGO_AGENCIA_PAGO
					cs.setInt(15, codigoSubAplicacionPago);			//15.- CODIGO_SUB_APLICACION_PAGO
					//F084-2006
					cs.setLong(16, prestamoPago);					//16.- PRESTAMO_PAGO
					cs.setInt(17, porcentajePago);					//17.- PORCENTAJE_PAGO
					cs.setInt(18, tipoPago2);						//18.- TIPO_PAGO_2
				}
				cs.setNull(19, Types.INTEGER);					//19.- CODIGO_CUENTA_PAGO
				cs.setNull(20, Types.DOUBLE);					//20.- SPREAD
				cs.setNull(21, Types.VARCHAR);					//21.- RELACION MATEMATICA
				cs.setNull(22, Types.DOUBLE);					//22.- MONTO AMORT REC
				cs.registerOutParameter(23, Types.INTEGER);		//23.- ESTATUS (out)


				cadena = "p1:="+cod_agencia+";"+"\n"+
					"p2:="+subAplicacion+";"+"\n"+
					"p3:="+numPrestamo+";"+"\n"+
					"p4:=to_date('"+fechaCaptura+"','yyyy-mm-dd');"+"\n"+
					"p5:="+montoOperacion+";"+"\n"+
					"p6:='"+usuario+"';"+"\n"+
					"p7:=to_date('"+fechaAdicion+"','yyyy-mm-dd');"+"\n"+
					"p8:=0;"+"\n";
				if (tipoPiso==1){
					if (numPrestamoPedido==null) {
						cadena += "p9:="+codigoTipoPago+";"+"\n"+
							"p10:=null;"+"\n";
					} else {
						if (existeCuenta) {
							cadena += "p9:="+codigoTipoPago+";"+"\n"+
								"p10:="+codigoCuenta+";"+"\n";
						} else {
							cadena += "p9:=null;"+"\n"+
								"p10:=null;"+"\n";
						}
					}
				} else {
					cadena += "p9:=null;"+"\n"+
						"p10:=null;"+"\n";
				}
				cadena += "p11:="+montoOperacion+";"+"\n"+
					"p12:=null;"+"\n";
				if (numPrestamoPedido==null) {
					cadena +=  "p13:=null;"+"\n"+
						"p14:=null;"+"\n"+
						"p15:=null;"+"\n"+
						"p16:=null;"+"\n"+
						"p17:=null;"+"\n"+
						"p18:=null;"+"\n";
				} else {
					cadena +=  "p13:="+codigoEmpresaPago+";"+"\n"+
						"p14:="+codigoAgenciaPago+";"+"\n"+
						"p15:="+codigoSubAplicacionPago+";"+"\n"+
						"p16:="+prestamoPago+";"+"\n"+
						"p17:="+porcentajePago+";"+"\n"+
						"p18:="+tipoPago2+";"+"\n";
				}
				cadena +=  "p19:=null;"+"\n"+
					"p20:=null;"+"\n"+
					"p21:=null;"+"\n"+
					"p22:=null;"+"\n"+
					"p23:=null;"+"\n";

//				System.out.println(cadena);


				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						estatus = cs.getString(23);			//Lo que regresa debe ser un entero o nulo...
						//fechaTemp = cs.getString(14);			//Lo que regresa debe ser un entero o nulo...
						//System.out.println("La fecha temp: " + fechaTemp);
						if (estatus == null) {
							System.out.println("estatus fue nulo");
							errorDesconocido = true;
							estatus="8";
						} else {
							System.out.println("Estatus: "+estatus);
							cadena += "Estatus: "+estatus;
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}


				if(!estatus.equals("0")) {		//Error
					con.terminaTransaccion(false);
					if (!errorDesconocido) {
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='DS'";
						//System.out.println(sentenciaSQL+"/n");
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_control05 where ic_folio='"+folio+"'";
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe==0) {
						sentenciaSQL = "insert into com_control05(ic_folio,cc_codigo_aplicacion"+
							" , ic_error_proceso, df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion"+
							" , ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por"+
							" , ig_numero_linea_credito, ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy"+
							" , ig_numero_contrato, ig_numero_prestamo, ig_proceso_numero " +
							" , cc_producto, df_diversos, df_proyectos, df_contratos, df_prestamos)"+
							" values('"+folio+"','"+codigoAplicacion+"',"+estatus+
							" , SYSDATE,"+codigoAgencia+","+codigoSubAplicacion+","+numLineaFondeo+
							" , "+codigoFinanciera+",'"+aprobadoPor+"', "+numLineaCredito+
							" , "+codigoAgenciaProy+","+codigoSubAplicacionProy+","+numContrato+", "+numPrestamo+ ", 4 " +
							" , '" + strProducto + "', to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), to_date('" + strFechaProyectos + "', 'dd/mm/yyyy') " +
							" , to_date('" + strFechaContratos + "', 'dd/mm/yyyy'), to_date('" + strFechaPrestamos + "', 'dd/mm/yyyy') )";
					} else { //Ya existe un error solo se adecua
						sentenciaSQL = "update com_control05 set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
							" , ic_error_proceso="+estatus+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+codigoAgencia+" "+
							" , ig_codigo_sub_aplicacion="+codigoSubAplicacion+" ,ig_numero_linea_fondeo="+numLineaFondeo+" "+
							" , ig_codigo_financiera="+codigoFinanciera+" ,ig_aprobado_por='"+aprobadoPor+"' "+
							" , ig_numero_linea_credito="+numLineaCredito+
							" , ig_codigo_agencia_proy="+codigoAgenciaProy+
							" , ig_codigo_sub_aplicacion_proy="+codigoSubAplicacionProy+
							" , ig_numero_contrato="+numContrato+
							" , ig_numero_prestamo="+numPrestamo+
							" , ig_proceso_numero = 4 " +
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" , df_contratos = to_date('" + strFechaContratos + "', 'dd/mm/yyyy')" +
							" , df_prestamos = to_date('" + strFechaPrestamos + "', 'dd/mm/yyyy')" +
							" where ic_folio='"+folio+"'";
					}//fin de else

					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "UPDATE com_control04"+
									" SET df_proceso = sysdate "+
									" , cs_estatus='N' " +
									" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									" WHERE ic_folio='"+folio+"'";
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error en la actualización del estatus");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println(sentenciaSQL+"/n");
						System.out.println("Error al Insertar o al Actualizar los Datos en Control05.");
						errorTransaccion = true;
					}
				} else {	//no hubo error  (error=0)
					sentenciaSQL = "UPDATE com_control04 "+
								" SET df_proceso = sysdate " +
								" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where ic_folio='"+folio+"'";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_solicitud set ic_estatus_solic = 3"+
							" , ic_bloqueo = 4 "+	//3 = Operada
							" , ig_numero_prestamo = "+numPrestamo+
							" , ig_numero_linea_credito = " + numLineaCredito+
							" , ig_numero_contrato = " + numContrato+
							" , df_operacion = (select FECHA_APERTURA from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
							" , fg_tasa_fondeo_nafin = (select TASA_TOTAL from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
							" , cg_tipo_busqueda = 'A' " +
							" , CS_FLOATING = '"+csFloating+"'"+	  //NE_2018_01 QC	
							" where ic_folio = '"+folio+"'";
						try {
//System.out.println("\n sentenciaSQL: "+sentenciaSQL);
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al actualizar estatus de solicitud");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar fecha del folio en Control04.");
						errorTransaccion = true;
					}

				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();

			/* Inicio Interfase Financiamiento a Pedidos */
			System.out.println(" Inicio Operaciones Financiamiento a Pedidos ");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("2-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and P.ic_pyme = pexp.ic_pyme"+
								" and P.ic_epo = pexp.ic_epo"+
								" and P.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("2-").toString())) {
				strFrom = " , com_domicilio D" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and P.ic_pyme = D.ic_pyme"   +
							"  and D.cs_fiscal = 'S'"   +
							"  and D.ic_estado = AG.ic_estado (+)";

			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}


			sentenciaSQL =
				" SELECT /*+  ordered use_nl(PS LC I A3) */"   +
				" A.ic_pedido"   +
				" , C4.ig_numero_prestamo"   +
				" , C4.fn_monto_operacion"   +
				" , C4.ig_codigo_agencia"   +
				" , C4.ig_codigo_sub_aplicacion"   +
				" , C4.ig_numero_linea_fondeo"   +
				" , C4.ig_codigo_financiera"   +
				" , C4.ig_aprobado_por"   +
				" , C4.cg_descripcion"   +
				" , C4.ig_numero_linea_credito"   +
				" , C4.ig_codigo_agencia_proy"   +
				" , C4.ig_codigo_sub_aplicacion_proy"   +
				" , C4.ig_numero_contrato"   +
				" , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura"   +
				" , A3.DF_FECHA_HORA as horaCaptura"   +
				" , trunc(sysdate) as fechaCapt"   +
				" , I.ig_tipo_piso"   +
				" , P.fn_monto"   +
				" , LC.cg_numero_cuenta "   +
				" , P.ic_moneda      "   +
				" , LC.cg_cuenta_clabe "   +
				" , c4.cc_producto " +
				" , to_char(c4.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				" , to_char(c4.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				" , to_char(c4.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				" , to_char(c4.DF_PRESTAMOS, 'dd/mm/yyyy') as DF_PRESTAMOS " +
				strSelect   +
				" , PS.ic_linea_credito_emp "+
				"	  , 'Disposiciones(Pedidos)' as OrigenQuery " +
				"  from com_anticipo A"   +
				" 	, com_pedido_seleccionado PS"   +
				" 	, com_linea_credito LC"   +
				" 	, comcat_if I"   +
				" 	, com_acuse3 A3"   +
				" 	, com_controlant04 C4"   +
				" 	, com_pedido P"   +
				strFrom   +
				"  WHERE A.ic_pedido = C4.ic_pedido"   +
				"  and A.cc_acuse = A3.cc_acuse"   +
				"  and C4.cs_estatus='S'"   +
				"  and A.ic_estatus_antic = 2"   +
				"  and A.ic_pedido = PS.ic_pedido"   +
				"  and PS.ic_pedido = P.ic_pedido"   +
				"  and PS.ic_linea_credito = LC.ic_linea_credito"   +
				"  and LC.ic_if = I.ic_if"   +
				strCondiciones +
				"  and A.ic_estatus_antic = 2 "+
				"  and A.ic_bloqueo = 3 "+
				" UNION "   +
				" SELECT /*+  ordered use_nl(PS LC I A3) */"   +
				" A.ic_pedido"   +
				" , C4.ig_numero_prestamo"   +
				" , C4.fn_monto_operacion"   +
				" , C4.ig_codigo_agencia"   +
				" , C4.ig_codigo_sub_aplicacion"   +
				" , C4.ig_numero_linea_fondeo"   +
				" , C4.ig_codigo_financiera"   +
				" , C4.ig_aprobado_por"   +
				" , C4.cg_descripcion"   +
				" , C4.ig_numero_linea_credito"   +
				" , C4.ig_codigo_agencia_proy"   +
				" , C4.ig_codigo_sub_aplicacion_proy"   +
				" , C4.ig_numero_contrato"   +
				" , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura"   +
				" , A3.DF_FECHA_HORA as horaCaptura"   +
				" , trunc(sysdate) as fechaCapt"   +
				" , I.ig_tipo_piso"   +
				" , P.fn_monto"   +
				" , LC.cg_cuenta_clabe "   +
				" , P.ic_moneda      "   +
				" , LC.cg_cuenta_clabe "   +
				" , c4.cc_producto " +
				" , to_char(c4.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				" , to_char(c4.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				" , to_char(c4.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				" , to_char(c4.DF_PRESTAMOS, 'dd/mm/yyyy') as DF_PRESTAMOS " +
				" , lc.ig_codigo_agencia ig_agencia_sirac "+
				" , PS.ic_linea_credito_emp "+
				"	  , 'Disposiciones(Fin Contratos)' as OrigenQuery " +
				"  from com_anticipo A"   +
				" 	, com_pedido_seleccionado PS"   +
				" 	, emp_linea_credito LC"   +
				" 	, comcat_if I"   +
				" 	, com_acuse3 A3"   +
				" 	, com_controlant04 C4"   +
				" 	, com_pedido P"   +
				strFrom   +
				"  WHERE A.ic_pedido = C4.ic_pedido"   +
				"  and A.cc_acuse = A3.cc_acuse"   +
				"  and C4.cs_estatus='S'"   +
				"  and A.ic_estatus_antic = 2"   +
				"  and A.ic_pedido = PS.ic_pedido"   +
				"  and PS.ic_pedido = P.ic_pedido"   +
				"  and PS.ic_linea_credito_emp = LC.ic_linea_credito"   +
				"  and LC.ic_if = I.ic_if"   +
				strCondiciones +
				"  and A.ic_estatus_antic = 2 "+
				"  and A.ic_bloqueo = 3 "+
				"  ORDER BY ig_tipo_piso desc";

//System.out.println(sentenciaSQL + ";");

			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			subAplicacion=211;
			while(rs.next()) {
				errorTransaccion = false;
				existeCuenta= false;
				icMoneda = rs.getInt("IC_MONEDA");

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_PEDIDO");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				numPrestamo = rs.getLong("IG_NUMERO_PRESTAMO");

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 1 */


				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				observaciones = rs.getString("CG_DESCRIPCION");
				numLineaCredito = rs.getString("IG_NUMERO_LINEA_CREDITO");
				codigoAgenciaProy = rs.getString("IG_CODIGO_AGENCIA_PROY");
				codigoSubAplicacionProy = rs.getString("IG_CODIGO_SUB_APLICACION_PROY");
				numContrato = rs.getString("IG_NUMERO_CONTRATO");
				String sFechaCaptura = rs.getString("FECHACAPTURA");
				fechaCaptura = rs.getDate("FECHACAPT");
				fechaHoraCaptura = rs.getTime("HORACAPTURA");
				//numeroCuenta = (montoOperacion >= 50000)?rs.getString("CG_NUMERO_CUENTA"):rs.getString("CG_CUENTA_CLABE");
				numeroCuenta = rs.getString("CG_CUENTA_CLABE");

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");
				strFechaContratos = rs.getString("DF_CONTRATOS");
				strFechaPrestamos = rs.getString("DF_PRESTAMOS");

				numLineaCreditoEmp = rs.getString("ic_linea_credito_emp")==null?"":rs.getString("ic_linea_credito_emp");

				//cs = con.ejecutaSP("PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Disposiciones.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					if("".equals(numLineaCreditoEmp)) {
						subAplicacion = 125;
					} else {
						subAplicacion = Integer.parseInt(codigoSubAplicacionProy);
					}
					String queri2 = "SELECT mcb.CODIGO_CUENTA, fpd.CODIGO_TIPO_PAGO "+
								" FROM MG_CUENTAS_BANCO mcb, MG_FORMA_PAGOS_DESEMBOLSO fpd"+
								" WHERE mcb.codigo_cuenta = fpd.codigo_cuenta_banco"+
								" AND NUMERO_CUENTA = '"+numeroCuenta+"'";
					try {
						rs2=con.queryDB(queri2);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(queri2): " + queri2 );
						throw sqle;
					}
					//09.- CODIGO_TIPO_PAGO
					//10.- CODIGO_CUENTA
					if (rs2.next()) {
						existeCuenta = true;
						codigoTipoPago = rs2.getInt("CODIGO_TIPO_PAGO");
						cs.setInt(9, codigoTipoPago);
						codigoCuenta = rs2.getInt("CODIGO_CUENTA");
						cs.setInt(10, codigoCuenta);
					} else {
						cs.setNull(9, Types.INTEGER);
						cs.setNull(10, Types.INTEGER);
					}
					rs2.close();
					con.cierraStatement();

				} else {
					subAplicacion = (icMoneda==54)?221:211;
					cs.setNull(9, Types.INTEGER);				//09.- CODIGO_TIPO_PAGO
					cs.setNull(10, Types.INTEGER);				//10.- CODIGO_CUENTA
				}
				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setInt(2, subAplicacion);					//02.- CODIGO_SUB_APLICACION
				//F084-2006
				cs.setLong(3, numPrestamo);						//03.- NUMERO_PRESTAMO
				cs.setDate(4, fechaCaptura,Calendar.getInstance());					//04.- FECHA_CAPTURA
				cs.setDouble(5, montoOperacion);				//05.- VALOR_DESEMBOLSO
				cs.setString(6, usuario);						//06.- ADICIONADO_POR
				cs.setDate(7, fechaAdicion, Calendar.getInstance());					//07.- FECHA_ADICION (fecha del sistema)
				cs.setDouble(8, (double)0);						//08.- MONTO_AMORTIZACION_AJUSTE
				cs.setDouble(11, montoOperacion);				//11.- VALOR_PAGO
				cs.setNull(12, Types.INTEGER);					//12.- NUMERO_DE_CHEQUE
				cs.setNull(13, Types.INTEGER);					//13.- CODIGO_EMPRESA_PAGO
				cs.setNull(14, Types.INTEGER);					//14.- CODIGO_AGENCIA_PAGO
				cs.setNull(15, Types.INTEGER);					//15.- CODIGO_SUB_APLICACION_PAGO
				cs.setNull(16, Types.INTEGER);					//16.- PRESTAMO_PAGO
				cs.setNull(17, Types.INTEGER);					//17.- PORCENTAJE_PAGO
				cs.setNull(18, Types.INTEGER);					//18.- TIPO_PAGO_2
				cs.setNull(19, Types.INTEGER);					//19.- CODIGO_CUENTA_PAGO
				cs.setNull(20, Types.DOUBLE);					//20.- SPREAD
				cs.setNull(21, Types.VARCHAR);					//21.- RELACION MATEMATICA
				cs.setNull(22, Types.DOUBLE);					//22.- MONTO AMORT REC
				cs.registerOutParameter(23, Types.INTEGER);		//23.- ESTATUS (out)

				cadena = "p1:="+cod_agencia+";"+"\n"+
					"p2:="+subAplicacion+";"+"\n"+
					"p3:="+numPrestamo+";"+"\n"+
					"p4:=to_date('"+fechaCaptura+"','yyyy-mm-dd');"+"\n"+
					"p5:="+montoOperacion+";"+"\n"+
					"p6:='"+usuario+"';"+"\n"+
					"p7:=to_date('"+fechaAdicion+"','yyyy-mm-dd');"+"\n"+
					"p8:=0;"+"\n";
				if (tipoPiso==1){
					if (existeCuenta) {
						cadena += "p9:="+codigoTipoPago+";"+"\n"+
							"p10:="+codigoCuenta+";"+"\n";
					} else {
						cadena += "p9:=null;"+"\n"+
							"p10:=null;"+"\n";
					}
				} else {
					cadena += "p9:=null;"+"\n"+
						"p10:=null;"+"\n";
				}
				cadena += "p11:="+montoOperacion+";"+"\n"+
					"p12:=null;"+"\n"+
					"p13:=null;"+"\n"+
					"p14:=null;"+"\n"+
					"p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n"+
					"p18:=null;"+"\n"+
					"p19:=null;"+"\n"+
					"p20:=null;"+"\n"+
					"p21:=null;"+"\n"+
					"p22:=null;"+"\n"+
					"p23:=null;"+"\n";


//				System.out.println(cadena);


				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						estatus = cs.getString(23);			//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							System.out.println("estatus fue nulo");
							errorDesconocido = true;
							estatus="8";
						} else {
							System.out.println("Estatus: "+estatus);
							cadena += "Estatus: "+estatus;
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += cadena;
					errorDesconocido = true;
					estatus="9";
				}


				if(!estatus.equals("0")) {		//Error
					con.terminaTransaccion(false);
					if (!errorDesconocido) {
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='DS'";
						//System.out.println(sentenciaSQL+"/n");
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_controlant05 where ic_pedido="+folio;
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe==0) {
						sentenciaSQL = "insert into com_controlant05(ic_pedido,cc_codigo_aplicacion"+
							" , ic_error_proceso, df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion"+
							" , ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por"+
							" , ig_numero_linea_credito, ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy"+
							" , ig_numero_contrato, ig_numero_prestamo, ig_proceso_numero" +
							" , cc_producto, df_diversos, df_proyectos, df_contratos, df_prestamos)"+
							" values("+folio+",'"+codigoAplicacion+"',"+estatus+
							" , SYSDATE,"+codigoAgencia+","+codigoSubAplicacion+","+numLineaFondeo+
							" , "+codigoFinanciera+",'"+aprobadoPor+"', "+numLineaCredito+
							" , "+codigoAgenciaProy+","+codigoSubAplicacionProy+","+numContrato+" , " + numPrestamo + ", 4" +
							" , '" + strProducto + "', to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), to_date('" + strFechaProyectos + "', 'dd/mm/yyyy') " +
							" , to_date('" + strFechaContratos + "', 'dd/mm/yyyy'), to_date('" + strFechaPrestamos + "', 'dd/mm/yyyy') )";
					} else { //Ya existe un error solo se adecua
						sentenciaSQL = "update com_controlant05 set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
							" , ic_error_proceso="+estatus+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+codigoAgencia+" "+
							" , ig_codigo_sub_aplicacion="+codigoSubAplicacion+" ,ig_numero_linea_fondeo="+numLineaFondeo+" "+
							" , ig_codigo_financiera="+codigoFinanciera+" ,ig_aprobado_por='"+aprobadoPor+"' "+
							" , ig_proceso_numero = 4 " +
							" , ig_numero_linea_credito="+numLineaCredito+
							" , ig_codigo_agencia_proy="+codigoAgenciaProy+
							" , ig_codigo_sub_aplicacion_proy="+codigoSubAplicacionProy+
							" , ig_numero_contrato="+numContrato+
							" , ig_numero_prestamo="+numPrestamo+
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" , df_contratos = to_date('" + strFechaContratos + "', 'dd/mm/yyyy')" +
							" , df_prestamos = to_date('" + strFechaPrestamos + "', 'dd/mm/yyyy')" +
							" where ic_pedido="+folio;
					}//fin de else

					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "UPDATE com_controlant04"+
									" SET df_proceso = sysdate"+
									" , cs_estatus='N' " +
									" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									" WHERE ic_pedido="+folio;
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error en la actualización del estatus");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
//						System.out.println(sentenciaSQL+"/n");
						System.out.println("Error al Insertar o al Actualizar los Datos en ControlAnt05.");
						errorTransaccion = true;
					}
				} else {	//no hubo error  (error=0)
					sentenciaSQL = "UPDATE com_controlant04" +
								" SET df_proceso = sysdate " +
								" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" WHERE ic_pedido="+folio;
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_anticipo set ic_estatus_antic = 3"+
							" , ic_bloqueo = 4 "+	//3 = Operada
							" , ig_numero_prestamo = "+numPrestamo+
							" , ig_numero_linea_credito = " + numLineaCredito+
							" , ig_numero_contrato = " + numContrato+
							" , df_operacion = (select FECHA_APERTURA from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
							" where ic_pedido = "+folio;
//System.out.println("\n sentenciaSQL: "+sentenciaSQL);
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al actualizar estatus de anticipo");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar fecha del folio en ControlAnt04.");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();

			System.out.println(" Fin Operaciones Financiamiento a Pedidos");
			/* Fin Interfase Financiamiento a Pedidos */


			/* Inicio Interfase 1er Piso Credicadenas  */
			System.out.println(" Inicio Operaciones 1er Piso Credicadenas");


			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("5-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("5-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and d.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";

			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}



			sentenciaSQL =
				" SELECT /*+  ordered use_nl(d lc i a3) */ "   +
				"  inv.cc_disposicion"   +
				"  , inv.ig_numero_prestamo"   +
				"  , inv.fn_monto_operacion"   +
				"  , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura"   +
				"  , a3.DF_FECHA_HORA as horaCaptura"   +
				"  , trunc(sysdate) as fechaCapt"   +
				"  , i.ig_tipo_piso"   +
				"  , d.fn_monto_credito as fn_monto"   +
				"  , lc.cg_numero_cuenta"   +
				"  , t.ic_moneda"   +
				" , DECODE(s.IC_TABLA_AMORT, 2, null, decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null)) as amort_ult_prim"   +
				//"  , decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null) as amort_ult_prim"   +
				" , inv.ig_codigo_sub_aplicacion_proy"   +
				strSelect   +
				"  , lc.cg_cuenta_clabe "   +
				"  , inv.ig_numero_linea_credito"   +
				"  , inv.ig_numero_contrato"   +
				"	  , 'Disposiciones(Credicadenas)' as OrigenQuery " +
				"   from"   +
				"   inv_solicitud s"   +
				"  	, inv_disposicion d"   +
				"  	, com_linea_credito lc"   +
				"  	, comcat_if i"   +
				"  	, com_acuse3 a3"   +
				"  	, inv_interfase inv"   +
				"   , comcat_tasa t"   +
				strFrom +
				"   WHERE s.cc_disposicion = inv.cc_disposicion"   +
				"   and s.cc_acuse = A3.cc_acuse"   +
				"   and s.cc_disposicion = d.cc_disposicion"   +
				"   and d.ic_linea_credito = lc.ic_linea_credito"   +
				"   and lc.ic_if = i.ic_if"   +
				"   and d.ic_tasa = t.ic_tasa "   +
				strCondiciones +
				"   AND inv.ig_numero_prestamo IS NOT NULL"   +
				"   AND inv.ig_numero_contrato IS NOT NULL"   +
				"   AND inv.ig_numero_linea_credito IS NOT NULL"   +
				"   AND inv.cc_codigo_aplicacion IS NULL"   +
				"   AND inv.ic_error_proceso IS NULL"   +
				"   AND s.ic_estatus_solic = 2"   +
				"   AND s.ic_bloqueo = 3"   +
				" UNION "   +
				" SELECT /*+  ordered use_nl(d lc i a3) */ "   +
				"  inv.cc_disposicion"   +
				"  , inv.ig_numero_prestamo"   +
				"  , inv.fn_monto_operacion"   +
				"  , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura"   +
				"  , a3.DF_FECHA_HORA as horaCaptura"   +
				"  , trunc(sysdate) as fechaCapt"   +
				"  , i.ig_tipo_piso"   +
				"  , d.fn_monto_credito as fn_monto"   +
				"  , lc.cg_cuenta_clabe "   +
				"  , t.ic_moneda"   +
				" , DECODE(s.IC_TABLA_AMORT, 1, null, 2, null, 'U') as amort_ult_prim"   +
//				" , DECODE(s.IC_TABLA_AMORT, 2, null, decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null)) as amort_ult_prim"   +
				//"  , decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null) as amort_ult_prim"   +
				" , inv.ig_codigo_sub_aplicacion_proy"   +
				"  , lc.ig_codigo_agencia ig_agencia_sirac "   +
				"  , lc.cg_cuenta_clabe "   +
				"  , inv.ig_numero_linea_credito"   +
				"  , inv.ig_numero_contrato"   +
				"	  , 'Disposiciones(Lib Disp - Fin Exp)' as OrigenQuery " +
				"   from"   +
				"   inv_solicitud s"   +
				"  	, inv_disposicion d"   +
				"  	, emp_linea_credito lc"   +
				"  	, comcat_if i"   +
				"  	, com_acuse3 a3"   +
				"  	, inv_interfase inv"   +
				"   , comcat_tasa t"   +
				strFrom +
				"   WHERE s.cc_disposicion = inv.cc_disposicion"   +
				"   and s.cc_acuse = A3.cc_acuse"   +
				"   and s.cc_disposicion = d.cc_disposicion"   +
				"   and d.ic_linea_credito_emp = lc.ic_linea_credito"   +
				"   and lc.ic_if = i.ic_if"   +
				"   and d.ic_tasa = t.ic_tasa "   +
				strCondiciones +
				"   AND inv.ig_numero_prestamo IS NOT NULL"   +
				"   AND inv.ig_numero_contrato IS NOT NULL"   +
				"   AND inv.ig_numero_linea_credito IS NOT NULL"   +
				"   AND inv.cc_codigo_aplicacion IS NULL"   +
				"   AND inv.ic_error_proceso IS NULL"   +
				"   AND s.ic_estatus_solic = 2"   +
				"   AND s.ic_bloqueo = 3"   +
				"   ORDER BY ig_tipo_piso desc"  ;
//System.out.println(sentenciaSQL + ";");

			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			String amort_ult_prim = null;
			double monto_amort_ajuste=0.0;

			while(rs.next()) {
				errorTransaccion = false;
				existeCuenta= false;
				monto_amort_ajuste = 0;
				icMoneda = rs.getInt("IC_MONEDA");

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("CC_DISPOSICION");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				numPrestamo = rs.getLong("IG_NUMERO_PRESTAMO");

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 1 */

				String sFechaCaptura = rs.getString("FECHACAPTURA");
				fechaCaptura = rs.getDate("FECHACAPT");
				fechaHoraCaptura = rs.getTime("HORACAPTURA");
				numeroCuenta = rs.getString("CG_CUENTA_CLABE");
				amort_ult_prim = rs.getString("AMORT_ULT_PRIM");
				subAplicacion = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");

				//cs = con.ejecutaSP("PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Disposiciones.java(paramStore): " + paramStore );
					throw sqle;
				}
				String queri2 = null;

				if (tipoPiso==1){
					//Estos parametros son fijos sólo en Dolares (Exportacion)
					if (icMoneda != 54) {
						queri2 = "SELECT mcb.CODIGO_CUENTA, fpd.CODIGO_TIPO_PAGO "+
									" FROM MG_CUENTAS_BANCO mcb, MG_FORMA_PAGOS_DESEMBOLSO fpd"+
									" WHERE mcb.codigo_cuenta = fpd.codigo_cuenta_banco"+
									" AND NUMERO_CUENTA = '"+numeroCuenta+"'";
						try {
							rs2=con.queryDB(queri2);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(queri2): " + queri2 );
							throw sqle;
						}
						//09.- CODIGO_TIPO_PAGO
						//10.- CODIGO_CUENTA
						if (rs2.next()) {
							existeCuenta = true;
							codigoTipoPago = rs2.getInt("CODIGO_TIPO_PAGO");
							cs.setInt(9, codigoTipoPago);
							codigoCuenta = rs2.getInt("CODIGO_CUENTA");
							cs.setInt(10, codigoCuenta);
						} else {
							cs.setNull(9, Types.INTEGER);
							cs.setNull(10, Types.INTEGER);
						}
						rs2.close();
						con.cierraStatement();
					} else {
						codigoTipoPago = 1214;
						codigoCuenta = 10082425;
						cs.setInt(9, codigoTipoPago);
						cs.setInt(10, codigoCuenta);
					}
				} else {
					subAplicacion = (icMoneda==54)?221:211;
					cs.setNull(9, Types.INTEGER);				//09.- CODIGO_TIPO_PAGO
					cs.setNull(10, Types.INTEGER);				//10.- CODIGO_CUENTA
				}
				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setInt(2, subAplicacion);					//02.- CODIGO_SUB_APLICACION
				//F084-2006
				cs.setLong(3, numPrestamo);						//03.- NUMERO_PRESTAMO
				cs.setDate(4, fechaCaptura, Calendar.getInstance());					//04.- FECHA_CAPTURA
				cs.setDouble(5, montoOperacion);				//05.- VALOR_DESEMBOLSO
				cs.setString(6, usuario);						//06.- ADICIONADO_POR
				cs.setDate(7, fechaAdicion, Calendar.getInstance());					//07.- FECHA_ADICION (fecha del sistema)

				//08.- MONTO_AMORTIZACION_AJUSTE
//System.out.println("amort_ult_prim: "+amort_ult_prim);
//System.out.println("monto_amort_ajuste: "+monto_amort_ajuste);
				if (amort_ult_prim == null) {
//System.out.println("11111");
					cs.setDouble(8, monto_amort_ajuste);
				} else {
					queri2 = " SELECT /*+ index(i cp_inv_mensualidad_pk)*/fn_pago_capital, ic_mensualidad "   +
							" FROM inv_mensualidad i"   +
							" WHERE cc_disposicion = '" + folio + "' " +
							" ORDER BY ic_mensualidad DESC" ;
					try {
//System.out.println("queri2:"+queri2);
						rs2=con.queryDB(queri2);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(queri2): " + queri2 );
						throw sqle;
					}
					if(rs2.next())
						monto_amort_ajuste = rs2.getDouble(1);
					rs2.close();
					con.cierraStatement();
					cs.setDouble(8, monto_amort_ajuste);
				}
//System.out.println("monto_amort_ajuste: "+monto_amort_ajuste);


				cs.setDouble(11, montoOperacion);				//11.- VALOR_PAGO
				cs.setNull(12, Types.INTEGER);					//12.- NUMERO_DE_CHEQUE
				cs.setNull(13, Types.INTEGER);					//13.- CODIGO_EMPRESA_PAGO
				cs.setNull(14, Types.INTEGER);					//14.- CODIGO_AGENCIA_PAGO
				cs.setNull(15, Types.INTEGER);					//15.- CODIGO_SUB_APLICACION_PAGO
				cs.setNull(16, Types.INTEGER);					//16.- PRESTAMO_PAGO
				cs.setNull(17, Types.INTEGER);					//17.- PORCENTAJE_PAGO
				cs.setNull(18, Types.INTEGER);					//18.- TIPO_PAGO_2
				cs.setNull(19, Types.INTEGER);					//19.- CODIGO_CUENTA_PAGO
				cs.setNull(20, Types.DOUBLE);					//20.- SPREAD
				cs.setNull(21, Types.VARCHAR);					//21.- RELACION MATEMATICA
				cs.setNull(22, Types.DOUBLE);					//22.- MONTO AMORT REC
				cs.registerOutParameter(23, Types.INTEGER);		//23.- ESTATUS (out)


				cadena = "p1:="+cod_agencia+";"+"\n"+
					"p2:="+subAplicacion+";"+"\n"+
					"p3:="+numPrestamo+";"+"\n"+
					"p4:=to_date('"+fechaCaptura+"','yyyy-mm-dd');"+"\n"+
					"p5:="+montoOperacion+";"+"\n"+
					"p6:='"+usuario+"';"+"\n"+
					"p7:=to_date('"+fechaAdicion+"','yyyy-mm-dd');"+"\n"+
					"p8:="+monto_amort_ajuste+";"+"\n";
				if (tipoPiso==1){
					if (existeCuenta) {
						cadena += "p9:="+codigoTipoPago+";"+"\n"+
							"p10:="+codigoCuenta+";"+"\n";
					} else {
						cadena += "p9:=null;"+"\n"+
							"p10:=null;"+"\n";
					}
				} else {
					cadena += "p9:=null;"+"\n"+
						"p10:=null;"+"\n";
				}
				cadena += "p11:="+montoOperacion+";"+"\n"+
					"p12:=null;"+"\n"+
					"p13:=null;"+"\n"+
					"p14:=null;"+"\n"+
					"p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n"+
					"p18:=null;"+"\n"+
					"p19:=null;"+"\n"+
					"p20:=null;"+"\n"+
					"p21:=null;"+"\n"+
					"p22:=null;"+"\n"+
					"p23:=null;"+"\n";


//				System.out.println(cadena);


				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						estatus = cs.getString(23);			//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							System.out.println("estatus fue nulo");
							errorDesconocido = true;
							estatus="8";
						} else {
							System.out.println("Estatus: "+estatus);
							cadena += "Estatus: "+estatus;
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}


				if(!estatus.equals("0")) {		//Error
					con.terminaTransaccion(false);
					if (!errorDesconocido) {
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='DS'";
						//System.out.println(sentenciaSQL+"/n");
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from inv_interfase where cc_disposicion='"+folio+"'";
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) {
						sentenciaSQL = "update inv_interfase " +
								"	set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
								" 	, ic_error_proceso="+estatus+
								"   , df_hora_proceso=SYSDATE"+
								"   , ig_proceso_numero = 4"+
								"   , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where cc_disposicion= '"+folio+"'";

						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println(sentenciaSQL+"/n");
							System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
							errorTransaccion = true;
						}
					}
				} else {	//no hubo error  (error=0)

					sentenciaSQL = "update inv_interfase " +
							"	set df_hora_proceso=SYSDATE"+
							"   , ig_proceso_numero = 4"+
							"   , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" where cc_disposicion='"+folio+"'";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update inv_solicitud set ic_estatus_solic = 3"+ //3 = Operada
							" , ic_bloqueo = 4 "+
							" , ig_numero_prestamo = "+numPrestamo+
							" , ig_numero_linea_credito = " + rs.getLong("ig_numero_linea_credito")+
							" , ig_numero_contrato = " + rs.getLong("ig_numero_contrato")+
							" , df_operacion = (select FECHA_APERTURA from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
							" where cc_disposicion='"+folio+"'";
						try {
//System.out.println("\n sentenciaSQL: "+sentenciaSQL);
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al actualizar estatus de inv_solicitud final");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar estatus interfase final");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();

			System.out.println(" Fin Operaciones 1er Piso Credicadenas");
			/* Fin Interfase 1er Piso Credicadenas */


			/* Inicia la Interfase Financiamiento a Distribuidores 2do Piso. */
			System.out.println(" Inicia la Interfase Financiamiento a Distribuidores 2do Piso.");

			String hint = "";

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("4-").toString())) {
				hint = "pexp AG";
				strFrom =
					", comrel_pyme_epo_x_producto pexp " +
					", comcat_agencia_sirac AG" ;
				strCondiciones =
					" and d.ic_pyme = pexp.ic_pyme"+
					" and d.ic_epo = pexp.ic_epo"+
					" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
					" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";
			} else if ("F".equals(oficTramXProd.get("4-").toString())) {
				hint = "DOM AG";
				strFrom =
					" , com_domicilio DOM" +
					", comcat_agencia_sirac AG" ;
				strCondiciones =
					"  and d.ic_pyme = DOM.ic_pyme"   +
					"  and DOM.cs_fiscal = ?"   +
					"  and DOM.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}
            
			//hint = " /*+index(s) use_nl(s d ds lc i a3 di rpi "+hint+")*/ ";

			// CREDITO EN CUENTA CORRIENTE
			sentenciaSQL =
				" SELECT "+hint+" di.ic_documento" +
				"   , di.ig_numero_prestamo" +
				"   , di.fn_monto_operacion" +
				"   , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura" +
				"   , a3.DF_FECHA_HORA as horaCaptura" +
				"   , trunc(sysdate) as fechaCapt" +
				"   , i.ig_tipo_piso" +
				"   , d.fn_monto" +
				"   , decode(rpi.cg_tipo_liquidacion, 'A', lc.cg_numero_cuenta, 'I', lc.cg_numero_cuenta_if) as cg_numero_cuenta" +
				"   , d.ic_moneda" +
				"	, di.ig_numero_linea_credito"   +
				"	, di.ig_numero_contrato"   +
				strSelect   +
				"	, 'Disposiciones(Distribuidores)' as proceso" +
				" FROM dis_solicitud s" +
				"    , dis_documento d" +
				"    , dis_docto_seleccionado ds" +
				"    , com_linea_credito lc" +
				"    , comcat_if i" +
				"    , com_acuse3 a3" +
				"    , dis_interfase di" +
				"    , comrel_producto_if rpi" +
				strFrom +
				" WHERE s.ic_documento = di.ic_documento" +
				"   and s.ic_documento = ds.ic_documento" +
				"   and s.ic_documento = d.ic_documento" +
				"   and s.cc_acuse = a3.cc_acuse" +
				"   and d.ic_linea_credito = lc.ic_linea_credito" +
				"   and lc.ic_if = i.ic_if" +
				"   and lc.ic_if = rpi.ic_if" +
				strCondiciones +
				"   and di.ig_numero_prestamo IS NOT NULL" +
				"   and di.ig_numero_contrato IS NOT NULL" +
				"   and di.ig_numero_linea_credito IS NOT NULL" +
				"   and di.cc_codigo_aplicacion IS NULL" +
				"   and di.ic_error_proceso IS NULL" +
				"   and s.ic_estatus_solic = ?" +					//1
				"   and s.ic_bloqueo = ?" +							//2
				"   and rpi.ic_producto_nafin = ?" +				//3
				" UNION ALL" +
				// DESCUENTO MERCANTIL
				" SELECT "+hint+" di.ic_documento" +
				"   , di.ig_numero_prestamo" +
				"   , di.fn_monto_operacion" +
				"   , TO_CHAR(sysdate,'yyyy-mm-dd') as fechaCaptura" +
				"   , a3.DF_FECHA_HORA as horaCaptura" +
				"   , trunc(sysdate) as fechaCapt" +
				"   , i.ig_tipo_piso" +
				"   , d.fn_monto" +
				"   , decode(rpi.cg_tipo_liquidacion, 'A', lc.cg_num_cuenta_epo, 'I', lc.cg_num_cuenta_if) as cg_numero_cuenta" +
				"   , d.ic_moneda" +
				"	, di.ig_numero_linea_credito"   +
				"	, di.ig_numero_contrato"   +
				strSelect   +
				"	, 'Disposiciones(Distribuidores)' as proceso" +
				" FROM dis_solicitud s" +
				"    , dis_documento d" +
				"    , dis_docto_seleccionado ds" +
				"    , dis_linea_credito_dm lc" +
				"    , comcat_if i" +
				"    , com_acuse3 a3" +
				"    , dis_interfase di" +
				"    , comrel_producto_if rpi" +
				strFrom +
				" WHERE s.ic_documento = di.ic_documento" +
				"   and s.ic_documento = ds.ic_documento" +
				"   and s.ic_documento = d.ic_documento" +
				"   and s.cc_acuse = a3.cc_acuse" +
				"   and d.ic_linea_credito_dm = lc.ic_linea_credito_dm" +
				"   and lc.ic_if = i.ic_if" +
				"   and lc.ic_if = rpi.ic_if" +
				strCondiciones +
				"   and di.ig_numero_prestamo IS NOT NULL" +
				"   and di.ig_numero_contrato IS NOT NULL" +
				"   and di.ig_numero_linea_credito IS NOT NULL" +
				"   and di.cc_codigo_aplicacion IS NULL" +
				"   and di.ic_error_proceso IS NULL" +
				"   and s.ic_estatus_solic = ?" +					//4
				"   and s.ic_bloqueo = ?" +							//5
				"   and rpi.ic_producto_nafin = ?" +				//6
				" ORDER BY ig_tipo_piso desc";
			//System.out.println(sentenciaSQL+";");
			ps = con.queryPrecompilado(sentenciaSQL);
			int cont = 0;
			if ("F".equals(oficTramXProd.get("4-").toString())) {
				cont++;ps.setString(cont, "S");
			}
			cont++;ps.setInt(cont, 2);
			cont++;ps.setInt(cont, 3);
			cont++;ps.setInt(cont, 4);
			if ("F".equals(oficTramXProd.get("4-").toString())) {
				cont++;ps.setString(cont, "S");
			}
			cont++;ps.setInt(cont, 2);
			cont++;ps.setInt(cont, 3);
			cont++;ps.setInt(cont, 4);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				errorTransaccion = false;
				existeCuenta= false;
				icMoneda = rs.getInt("IC_MONEDA");
				/*
				Se asigna la variable de numero de prestamo obtenido en el credito de Financiamiento a Pedidos
				para determinar si se va a realizar una cobranza
				*/
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_DOCUMENTO");
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				//F084-2006
				numPrestamo = rs.getLong("IG_NUMERO_PRESTAMO");
				String sFechaCaptura = rs.getString("FECHACAPTURA");
				fechaCaptura = rs.getDate("FECHACAPT");
				fechaHoraCaptura = rs.getTime("HORACAPTURA");
				cod_agencia=rs.getInt("IG_AGENCIA_SIRAC");					/* Valor 1 */

				//cs = con.ejecutaSP("PR_K_Desembolso.Pr_P_Aplica_Desembolso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Disposiciones.java(paramStore): " + paramStore );
					throw sqle;
				}



				subAplicacion = (icMoneda==54)?221:211;

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setInt(2, subAplicacion);					//02.- CODIGO_SUB_APLICACION
				cs.setLong(3, numPrestamo);						//03.- NUMERO_PRESTAMO
				cs.setDate(4, fechaCaptura, Calendar.getInstance());					//04.- FECHA_CAPTURA
				cs.setDouble(5, montoOperacion);				//05.- VALOR_DESEMBOLSO
				cs.setString(6, usuario);						//06.- ADICIONADO_POR
				cs.setDate(7, fechaAdicion, Calendar.getInstance());					//07.- FECHA_ADICION (fecha del sistema)
				//08.- MONTO_AMORTIZACION_AJUSTE
				if (icMoneda == 54) {
					cs.setDouble(8, 0);
				}else {
					cs.setNull(8, Types.DOUBLE);
				}
				cs.setNull(9, Types.INTEGER);					//09.- CODIGO_TIPO_PAGO
				cs.setNull(10, Types.INTEGER);					//10.- CODIGO_CUENTA
				cs.setDouble(11, montoOperacion);				//11.- VALOR_PAGO
				cs.setNull(12, Types.INTEGER);					//12.- NUMERO_DE_CHEQUE
				cs.setNull(13, Types.INTEGER);					//13.- CODIGO_EMPRESA_PAGO
				cs.setNull(14, Types.INTEGER);					//14.- CODIGO_AGENCIA_PAGO
				cs.setNull(15, Types.INTEGER);					//15.- CODIGO_SUB_APLICACION_PAGO
				cs.setNull(16, Types.INTEGER);					//16.- PRESTAMO_PAGO
				cs.setNull(17, Types.INTEGER);					//17.- PORCENTAJE_PAGO
				cs.setNull(18, Types.INTEGER);					//18.- TIPO_PAGO_2
				cs.setNull(19, Types.INTEGER);					//19.- CODIGO_CUENTA_PAGO
				cs.setNull(20, Types.DOUBLE);					//20.- SPREAD
				cs.setNull(21, Types.VARCHAR);					//21.- RELACION MATEMATICA
				cs.setNull(22, Types.DOUBLE);					//22.- MONTO AMORT REC
				cs.registerOutParameter(23, Types.INTEGER);		//23.- ESTATUS (out)


				cadena = "p1:="+cod_agencia+";\n"+
						"p2:="+subAplicacion+";\n"+
						"p3:="+numPrestamo+";\n"+
						"p4:=to_date('"+fechaCaptura+"','yyyy-mm-dd');\n"+
						"p5:="+montoOperacion+";\n"+
						"p6:='"+usuario+"';\n"+
						"p7:=to_date('"+fechaAdicion+"','yyyy-mm-dd');\n";
				if (icMoneda==54){
					cadena += "p8:=0;\n";
				} else {
					cadena += "p8:=null;\n";
				}
				cadena += 	"p9:=null;\n"+
							"p10:=null;\n"+
							"p11:="+montoOperacion+";\n"+
							"p12:=null;\n"+
							"p13:=null;\n"+
							"p14:=null;\n"+
							"p15:=null;\n"+
							"p16:=null;\n"+
							"p17:=null;\n"+
							"p18:=null;\n"+
							"p19:=null;\n"+
							"p20:=null;\n"+
							"p21:=null;\n"+
							"p22:=null;\n"+
							"p23:=null;\n";

//				System.out.println(cadena);


				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						estatus = cs.getString(23);			//Lo que regresa debe ser un entero o nulo...
						//fechaTemp = cs.getString(14);			//Lo que regresa debe ser un entero o nulo...
						//System.out.println("La fecha temp: " + fechaTemp);
						if (estatus == null) {
							System.out.println("estatus fue nulo");
							errorDesconocido = true;
							estatus="8";
						} else {
							System.out.println("Estatus: "+estatus);
							cadena += "Estatus: "+estatus;
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}

				if(!estatus.equals("0")) {		//Error
					con.terminaTransaccion(false);
					if (!errorDesconocido) {
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
										" where ic_error_proceso="+estatus+
										" and cc_codigo_aplicacion='DS'";
						//System.out.println(sentenciaSQL+"/n");
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from dis_interfase where ic_documento = "+folio;
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Disposiciones.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) {
						sentenciaSQL = "update dis_interfase " +
									" set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
									" , ic_error_proceso="+estatus+
									" , df_hora_proceso=SYSDATE"+
									" , ig_proceso_numero = 4"+
									" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									" where ic_documento = "+folio;

						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println(sentenciaSQL+"/n");
							System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
							errorTransaccion = true;
						}
					}
				} else {	//no hubo error  (error=0)

					sentenciaSQL = "update dis_interfase " +
								" set df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 4"+
								" , cg_disposiciones_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where ic_documento = "+folio;
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update dis_solicitud set ic_estatus_solic = 3"+ //3 = Operada
									" , ic_bloqueo = 4 "+
									" , ig_numero_prestamo = "+numPrestamo+
									" , ig_numero_linea_credito = " + rs.getLong("ig_numero_linea_credito")+
									" , ig_numero_contrato = " + rs.getLong("ig_numero_contrato")+
									" , df_operacion = (select FECHA_APERTURA from pr_prestamos where NUMERO_PRESTAMO = "+numPrestamo+") "+
									" where ic_documento = "+folio;

						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al actualizar estatus de inv_solicitud final");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar estatus interfase final");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			rs.close();
			ps.close();
			if (cs !=null) cs.close();
			System.out.println(" Fin de la Interfase Financiamiento a Distribuidores 2do Piso.");
			/* Fin de la Interfase Financiamiento a Distribuidores 2do Piso. */

		} catch(Exception e) {//fin try
			System.out.println("Error: "+e);
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	} //fin del main
}	//fin clase
