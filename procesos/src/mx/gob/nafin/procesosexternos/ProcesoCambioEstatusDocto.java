package mx.gob.nafin.procesosexternos;

import com.netro.descuento.CargaDocumento;

import netropology.utilerias.ServiceLocator;

public class ProcesoCambioEstatusDocto {


	public static void main(String [] args)  {
		int codigoSalida = 0;
		int proceso = 0;
		String ruta = "", claveEpos ="";

	try{
			ruta = args[0];//Este parametro es la ruta de publicacion del servidor
			claveEpos 			= args[1];
			System.out.println("ruta --->"+ruta);
			System.out.println("claveEpos --->"+claveEpos);

			CargaDocumento CargaDocumentos = ServiceLocator.getInstance().remoteLookup("CargaDocumentoEJB", CargaDocumento.class);

			CargaDocumentos.procesoCambioEstatus(ruta, claveEpos);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}


	}
}
