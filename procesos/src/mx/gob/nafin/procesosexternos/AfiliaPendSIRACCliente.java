package mx.gob.nafin.procesosexternos;

import com.netro.afiliacion.Afiliacion;

import netropology.utilerias.ServiceLocator;

public class AfiliaPendSIRACCliente  {
	public static void main(String [] args)  {
		int codigoSalida = 0;
		try {

			System.out.println("incio AfiliaPendSIRACCliente:::::");
			Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);

			afiliacion.batchAfiliaPendSirac();


		} catch (Exception e) {
			System.out.println("AfiliaPendSIRACCliente::main(Exception)"+e);
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
    }//main
}