package mx.gob.nafin.procesosexternos;

import com.netro.credito.CargaCredito;
import com.netro.credito.EnlInterCargaCE;

import netropology.utilerias.ServiceLocator;


public class CargaCreditoEnl {

	public static void cargaCE(String ic_if, EnlInterCargaCE epo) throws Exception {

		CargaCredito cargaCredito = ServiceLocator.getInstance().remoteLookup("CargaCreditoEJB", CargaCredito.class);

		cargaCredito.procesaCargaMasivaCE(epo, ic_if);
	}//cargaCE

}//CargaCreditoEnl
