package mx.gob.nafin.procesosexternos;

import com.netro.descuento.BANSEFIEnlOperados;

public class BANSEFIEnlOpeCliente{

	public static void main(String args[]){
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();

		try {
			CargaDoctosEnl.procesoDoctos(args[0], new BANSEFIEnlOperados(args[0]), "O");
			//FODEA 006-Feb2010 Rebos - Se agrego la validación para que tome en cuenta los documentos vencidos sin operar.
			CargaDoctosEnl.procesoDoctos(args[0], new BANSEFIEnlOperados(args[0]), "V");
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			codigoSalida = 1;
		} catch(Exception e) {
			e.printStackTrace();
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
                } finally {
			System.exit(codigoSalida);
		}
	}
}