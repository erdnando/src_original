package mx.gob.nafin.procesosexternos;

import com.netro.afiliacion.Afiliacion;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Clase para la ejecución del proceso de Avisos a los intemediarios cuando una pyme sufrio cambios
 * @author Deysi Laura Hernández Contreras
 */
public class NotificaModificaPymeaIF {

    private static final Log LOG = LogFactory.getLog(NotificaModificaPymeaIF.class);

    /**
     *
     * @param args
     */

    public static void main(String[] args) {
        LOG.info("Entrando a MotificaModificaPymeaIF (E) ");
        int codigoSalida = 0;
        String ruta = "";
        try {
            ruta = args[0];

            Afiliacion afiliacion = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);

            afiliacion.enviodeEmail(ruta);

        } catch (Exception e) {
            System.out.println("AfiliaPendSIRACCliente::main(Exception)" + e);
            e.printStackTrace();
            System.out.println(e.getMessage());
            codigoSalida = 1;
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            codigoSalida = 1;
        } finally {
            System.exit(codigoSalida);
        }
        LOG.info("Entrando a MotificaModificaPymeaIF  (S) ");
    }


}
