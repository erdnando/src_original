package mx.gob.nafin.procesosexternos;

/**
 * Esta clase realizar� el corte por PyME de los documentos operados, negociables
 * y vencidos que han sido notificados negociables y por vencer el mes anterior.
 * Nafinsa M�vil.
 * @since FODEA 052 - 2010
 * @author Janneth Rebeca Salda�a Waldo
 */
import com.netro.servicios.Servicios;

import netropology.utilerias.ServiceLocator;

public class CostoEIngresoNotificado {
	public static void main(String args[])  {
		int codigoSalida = 0;
		try{
			generaBitCostoEIngreso(args);
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: No se proporciono argumento");
			System.out.println(aiobe.getMessage());
			aiobe.printStackTrace();
			codigoSalida = 1;
		} catch(Throwable ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
   }

	private static void generaBitCostoEIngreso(String args[]) throws Throwable {
		Servicios servicios = obtenerEJB();
		String mes = null;
		String anio = null;
		if (args.length >= 2) {
			mes = args[0];
			anio = args[1];
		}

		servicios.generaBitCostoEIngreso(mes, anio);
		System.out.println("Ejecuto m�todo generaBitCostoEIngreso()");
	}

	private static Servicios obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Servicios servicios = ServiceLocator.getInstance().remoteLookup("ServiciosEJB", Servicios.class);
		return servicios;
	}
}