package mx.gob.nafin.procesosexternos;


import com.netro.descuento.Notificacion;

import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;
/*
 * @julio
 * */
public class ProcNotifCambEstatusLCre {
    private static final Log LOG = ServiceLocator.getInstance().getLog(ProcNotifCambEstatusLCre.class);
    
    public static void main(String [] args)  {
		LOG.info("Entrando a ProcNotifCambEstatusLCre (e)");
        
        int codigoSalida = 0;
        String ruta = "";     
        try {
            ruta = args[0];       
            Notificacion notificacion = ServiceLocator.getInstance().remoteLookup("NotificacionEJB", Notificacion.class);
            notificacion.procesosaEjecutar(ruta);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Error :::"+e.getMessage());
            codigoSalida = 1;
        } catch(Throwable ex) {
            ex.printStackTrace();
            codigoSalida = 1;
            LOG.error("Error :::"+ex.getMessage());                     
        } finally {			
            LOG.info("codigo de Salida  "+codigoSalida);
			LOG.info("Saliendo de ProcNotifCambEstatusLCre (s)");
        }
    }
}
