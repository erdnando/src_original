package mx.gob.nafin.procesosexternos;

import com.netro.descuento.ParametrosDescuento;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

public class ParametrizacionDescuentoAutomaticoEJBCliente {

	public static void main(String [] args)  {
		String rutaApp = "";
		//int proceso = 0;
		int codigoSalida = 0;
		try {
			if(args.length > 0){
				rutaApp = args[0];
			} else {
				rutaApp = "rutaApp vacia";
			}
			System.out.println("..:: La ruta de la aplicacion es "+rutaApp);
			ParametrizacionDescuentoAutomatico(rutaApp);
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
  }

	private static void ParametrizacionDescuentoAutomatico(String rutaApp) throws Throwable {
		CreaArchivo archivo = new CreaArchivo();
		java.text.SimpleDateFormat formatterYear = new java.text.SimpleDateFormat("_ddMMyyyy");
		java.util.Date startTime = new java.util.Date();
		String contenidoArchivo = "";

		System.out.println("Se ejecuta el proceso parametrizacion de descuento autom�tico:");
		System.out.println("ParametrizacionDescuentoAutomaticoEJBCliente.ParametrizacionDescuentoAutomatico()");
		System.out.println("Inicio Conexion");
		ParametrosDescuento sd = ServiceLocator.getInstance().remoteLookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		contenidoArchivo = sd.setParamEpoPymeIf();

		if(!archivo.make(contenidoArchivo, rutaApp, "ReporteParamDsctoAuto"+formatterYear.format(startTime), ".txt")){
			System.out.println("<---�ERROR AL GENERAR EL ARCHIVO!--->");
		}
		//sd.vRealizarDescuentoAutomatico(epo, pyme, intermediario, proceso);
		//sd.vDescAutoProgramados(epo, pyme, intermediario);
		System.out.println("Ejecuto m�todo setParamEpoPymeIf()");
		boolean lbOk = true;
	}
}