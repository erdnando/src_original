package mx.gob.nafin.procesosexternos;

import com.netro.descuento.ISeleccionDocumento;
import com.netro.distribuidores.AceptacionPyme;

import netropology.utilerias.ServiceLocator;

public class DescuentoAutomaticoEJBCliente {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		String claveEpo = null, clavePyme = null, claveIntermediario = null;
		int proceso = 0;
		String ruta = "", descuento = "";

		try {

			try {
				proceso 			= Integer.parseInt(args[0]);
			} catch (ArrayIndexOutOfBoundsException eiobe) {
				//System.out.println("DescuentoAutomaticoEJBCliente.main(ArrayIndexOutOfBoundsException): " + eiobe.toString());
			}

			try {
				claveEpo 			= args[1];
				if (claveEpo.equals("-")) {
					claveEpo = null;
				}
			} catch (ArrayIndexOutOfBoundsException eiobe) {
				//System.out.println("DescuentoAutomaticoEJBCliente.main(ArrayIndexOutOfBoundsException): " + eiobe.toString());
			}

			try {
				clavePyme 			= args[2];
				if (clavePyme.equals("-")) {
					clavePyme = null;
				}
			} catch (ArrayIndexOutOfBoundsException eiobe) {
				//System.out.println("DescuentoAutomaticoEJBCliente.main(ArrayIndexOutOfBoundsException): " + eiobe.toString());
			}

			try {
				claveIntermediario 	= args[3];
				if (claveIntermediario.equals("-")) {
					claveIntermediario = null;
				}
			} catch (ArrayIndexOutOfBoundsException eiobe) {
				//System.out.println("DescuentoAutomaticoEJBCliente.main(ArrayIndexOutOfBoundsException): " + eiobe.toString());
			}

			try {
				ruta 	= args[4];

			} catch (ArrayIndexOutOfBoundsException eiobe) {
							//System.out.println("DescuentoAutomaticoEJBCliente.main(ArrayIndexOutOfBoundsException): " + eiobe.toString());
			}

		  System.out.println("ruta ========================================= " + ruta);

			try {
				descuento 	= args[5];
				if (!descuento.equals("TODOS") && !descuento.equals("FACTORAJE") && !descuento.equals("DISTRIBUIDORES")) {
					System.out.println(" Al ejecutar el shell se debe especificar que proceso ejecutar. Ejemplos VALIDOS:");
					System.out.println(" DescuentoAutomaticoEJBCliente.sh TODOS - Ejecuta ambos descuentos autom�ticos");
					System.out.println(" DescuentoAutomaticoEJBCliente.sh FACTORAJE - Ejecuta �nicamente el descuento automatico de FACTORAJE");
					System.out.println(" DescuentoAutomaticoEJBCliente.sh DISTRIBUIDORES  - Ejecuta �nicamente el descuento automatico de DISTRIBUIDORES");
					codigoSalida = 1;
				}
			} catch (ArrayIndexOutOfBoundsException eiobe) {
				System.out.println(" Al ejecutar el shell se debe especificar que proceso ejecutar. Ejemplos VALIDOS:");
				System.out.println(" DescuentoAutomaticoEJBCliente.sh TODOS - Ejecuta ambos descuentos autom�ticos");
				System.out.println(" DescuentoAutomaticoEJBCliente.sh FACTORAJE - Ejecuta �nicamente el descuento automatico de FACTORAJE");
				System.out.println(" DescuentoAutomaticoEJBCliente.sh DISTRIBUIDORES  - Ejecuta �nicamente el descuento automatico de DISTRIBUIDORES");

				codigoSalida = 1;
			}

			if ("TODOS".equals(descuento) || "FACTORAJE".equals(descuento)) {
				DescuentoAutomatico(claveEpo, clavePyme, claveIntermediario, proceso, ruta);

			}
			if ("TODOS".equals(descuento) || "DISTRIBUIDORES".equals(descuento)) {
				DescuentoAutomaticoDistribuidores(ruta);
			}

			//DescuentoAutomaticoMandato(claveEpo, clavePyme, claveIntermediario, 2);
		} catch (NumberFormatException nfe) {
			//System.out.println("DescuentoAutomaticoEJBCliente.main(NumberFormatException): " + eiobe.toString());
			System.out.println(" El parametro no es correcto, debe ser un n�mero. Favor de verificarlo");
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
	}

	private static void DescuentoAutomatico(String epo, String pyme, String intermediario, int proceso, String ruta) throws Throwable {
		System.out.println("Se ejecuta el proceso de descuento autom�tico con los siguiente parametros:");
		System.out.println("DescuentoAutomaticoEJBCliente.DescuentoAutomatico("+epo+", " + pyme + ", " + intermediario + ", "+ proceso +")");
		ISeleccionDocumento sd = obtenerEJB();
		sd.vRealizarDescuentoAutomatico(epo, pyme, intermediario, proceso, "", "",  ruta, ruta);
		System.out.println("Ejecuto m�todo vRealizarDescuentoAutomatico()");
		boolean lbOk = true;
	}

	private static ISeleccionDocumento obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		ISeleccionDocumento SelecDocS = ServiceLocator.getInstance().remoteLookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		return SelecDocS;
	}

	private static void DescuentoAutomaticoDistribuidores(String ruta) throws Throwable {
		AceptacionPyme ap = obtenerEJBDist();
		ap.descuentoAutomaticoDist(ruta);
		System.out.println("Ejecuto m�todo descuentoAutomaticoDist()");
		boolean lbOk = true;
	}

	private static AceptacionPyme obtenerEJBDist() throws Throwable {
		System.out.println("Inicio Conexion");
		AceptacionPyme aceptPyme = ServiceLocator.getInstance().remoteLookup("AceptacionPymeEJB", AceptacionPyme.class);
		return aceptPyme;
	}

	/*
	private static void DescuentoAutomaticoMandato(String epo, String pyme, String intermediario, int proceso) throws Throwable {
		ISeleccionDocumento sd = obtenerEJB();
		sd.descuentoAutomaticoMandato(epo, pyme, intermediario, proceso);
		System.out.println("Ejecuto m�todo DescuentoAutomaticoMandato()");
		boolean lbOk = true;
	}
	*/

}
