package mx.gob.nafin.procesosexternos;

import com.netro.afiliacion.Afiliacion;

import netropology.utilerias.ServiceLocator;

public class ActLineasFondeoEJBCliente {

	public static void main(String [] args)  {
		int codigoSalida = 0;
		try {
			ejecutarProceso();
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}

	}

	private static Afiliacion obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Afiliacion afiliacionS = ServiceLocator.getInstance().remoteLookup("AfiliacionEJB", Afiliacion.class);
		return afiliacionS;
	}

	private static void ejecutarProceso( ) throws Throwable {
		System.out.println("Ejecuto metodo ActLineasFondeoEJBCliente.ejecutarProceso()");
		Afiliacion oAfiliacion = obtenerEJB();
		oAfiliacion.actualizaLineasFondeo();
		System.out.println("Ejecuto metodo ActLineasFondeoEJBCliente.ejecutarProceso()");
	}

}
