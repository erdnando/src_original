package mx.gob.nafin.procesosexternos;


/*************************************************************************************
*
* Nombre de Clase o de Archivo: Proyectos.java
*
* Versión: 		  1.0
*
* Fecha Creación: 01/junio/2001
*
* Autor:          Gilberto E. Aparicio Guerrero
*
* Descripción de Clase: Clase que realiza el proceso de Proyectos para Interfases a SIRAC.
*
*************************************************************************************/

/*************************************************************************************
* Modificación
*
* Fecha Modificación: 21/Mayo/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Dolares, 2o Piso Descuento Electrónico
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Modificación: 21/Mayo/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Ajuste Implementación de Dolares, 2o Piso Bases de Operación
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 20/Ago/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Financiamiento a Pedidos 2o Piso FODA 074
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 04/Sept/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Financiamiento a Inventarios 2o Piso FODA 075
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	29/Sept/2003
*
* Autor Modificacion:		L.I. Hugo Díaz García
*
* Descripción: Implementación de Financiamiento a Distribuidores 2o Piso
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	15/Oct/2003
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 049 - 2003, Determinar el cliente (Param 8) de acuerdo a la base
* de operacion
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	08/Julio/2004
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 062 - 2004, Oficina Tramitadora
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	01/Septiembre/2005
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 058 - 2005, Distribuidores Dólares
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	25/Abril/2006
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 011 - 2006, Credicadenas de Exportacion
*
*
*************************************************************************************/
import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;


public class Proyectos {

	public static void main(String args[]) {
		ejecutaCreditos(args[0]);
	}

	public static void ejecutaCreditos(String usuario){
		String query="", queri="", folio="", aprobado_por=null, descripcion="", estatus="", tipo_soli="", ic_docto="";
		StringBuffer cadena= new StringBuffer(2000);
		/* Valores Fijos */
		String no_linea_credito=null;       			/* Valor 1 */
		int cod_agencia=90;                 			/* Valor 2 */
		int cod_sub_aplicacion=201;         			/* Valor 3 */
		String desc="Ingresado por Nafin Electrónico";  /* Valor 4 */
		String cod_funcionario=usuario; 	 			/* Valor 6 */
		String cod_tipo_linea="D";          			/* Valor 10 */
		int frecuencia=1;                   			/* Valor 11 */
		java.sql.Date fecha_max_desem=null; 			/* Valor 13*/
		java.sql.Date fecha_max_pago=null; 				/* Valor 14 */
		String empleos_generados=null;      			/* Valor 21 */
		double vtas_exterior=0.0;          			/* Valor 22 */
		int cod_agen_interme = 0;             			/* Valor 24 */
		String reestructura=null;             			/* Valor 25 */
		String cod_grup_comision=null;      			/* Valor 26 */
		String verific_estrato="S";         			/* Valor 27 */
		double porcent_asignado=100.0;      			/* Valor 28 */
		int cod_sub_apli_det=211;           			/* Valor 29 */
		double porcent_asignado_det=100.0;  			/* Valor 31 */
		double mto_intermediario=0.0;       			/* Valor 32 */
		double mto_cliente=0.0;             			/* Valor 33 */
		boolean errorTransaccion = false;
		boolean errorDesconocido=false;
    	boolean obraPublica = false;
		String numPrestamoPedido="";
		String tipo_plazo = null;
		int icMoneda=1;
    	int clasifObjetoGasto = 0;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		/* ************* */
		ResultSet rs, rs1, rs2;
		CallableStatement cs=null;
		String numPagosCobranza="";
		Hashtable oficTramXProd = new Hashtable();
		String strFrom, strCondiciones, strSelect;
		String strProducto = null;
		String strFechaDiversos = null;
		String paramStore = "LC_K_Apertura_Proyecto.Lc_Alta_Proyecto(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String tipo_pago ="1"; 
		Interfaz  inter = new Interfaz();// Se agrego F002-2016
		String csFloating="N"; //NE_2018_01 QC
			
		AccesoDBOracle base=new AccesoDBOracle();
		try {
			base.conexionDB();
		} catch(Exception e) {
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try {
			query = "select ic_producto_nafin||cc_tipo as llave, cg_agencia_sirac from comrel_cartera_x_producto";
			rs=base.queryDB(query);
			while(rs.next()) {
				oficTramXProd.put(rs.getString("LLAVE"), rs.getString("CG_AGENCIA_SIRAC"));
			}
			rs.close();
			base.cierraStatement();

		} catch (Exception e){
			oficTramXProd.put("1F", "M");
			oficTramXProd.put("1C", "F");
			oficTramXProd.put("2-", "F");
			oficTramXProd.put("4-", "F");
			oficTramXProd.put("5-", "F");
			System.out.println("Exception. Error al obtener la parametrizacion de Oficina tramitadora, utilizando DEFAULTS: " + e);
		}



		try{
			query = "  SELECT c1.ic_folio"   +
					"      , c1.ig_codigo_agencia"   +
					"      , c1.ig_codigo_sub_aplicacion"   +
					"      , c1.ig_numero_linea_fondeo"   +
					"      , c1.ig_codigo_financiera"   +
					"      , c1.ig_aprobado_por"   +
					"      , c1.cg_descripcion"   +
					"      , c1.fn_monto_operacion"   +
					"      , c1.cg_estatus"   +
					"      , s.cs_tipo_solicitud"   +
					"      , s.ic_documento"   +
					//"      , DECODE(d.ic_moneda, 1, s.ig_plazo, 54, trunc(s.DF_V_DESCUENTO) - trunc(s.DF_FECHA_SOLICITUD) ) as ig_plazo"   +
					"	   , s.ig_plazo "	+
					"      , TO_CHAR (s.df_v_descuento, 'DD/MM/YYYY')"   +
					"      , DECODE (i.ig_tipo_piso, 2, p.in_numero_sirac, 1, DECODE (e.cs_opera_primer_piso, 'S', e.ig_numero_sirac, p.in_numero_sirac ) ) AS in_numero_sirac"   +
					"      , UPPER (e.cg_conv_prov) cg_conv_prov"   +
					"      , d.ic_epo"   +
					"      , UPPER (p.cg_desarrollo_prov) cg_desarrollo_prov"   +
					"      , i.ig_tipo_piso"   +
					"      , d.fn_monto_dscto"   +
					"      , c1.ig_numero_prestamo_pedido"   +
					"      , p.in_numero_sirac as in_numero_sirac_pyme"   +
					"      , e.ig_numero_sirac as in_numero_sirac_epo"   +
					"      , d.ic_moneda"   +
					"      , DECODE(NVL(p.fn_ventas_net_exp,0),0,1,p.fn_ventas_net_exp) fn_ventas_net_exp"   +
					"      , c1.IN_NUMERO_PAGOS numpagos"   +
					"      , AG.ig_agencia_sirac as ig_agencia_sirac_fiscal"   +
					"      , AG2.ig_agencia_sirac as ig_agencia_sirac_tramit"   +
					"	     , c1.cc_producto " +
					"	     , to_char(c1.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
					"	     , d.cg_clave_presupuestaria " +
					"	     , CASE WHEN IC_SECRETARIA IS NOT NULL AND IC_TIPO_ORGANISMO IS NOT NULL THEN 'S' ELSE 'N' END as EPO_PEF " +
					"       , ds.CS_FLOATING   "+ ///NE_2018_01 QC
					"	     , 'Proyectos.class' as Proceso " +
					"    FROM com_control01 c1"   +
					"         , com_solicitud s"   +
					"         , com_documento d"   +
					"         , comcat_pyme p"   +
					"         , comcat_epo e"   +
					"         , com_docto_seleccionado ds"   +
					"         , comcat_if i"   +
					"         , com_domicilio dom"   +
					"         , comcat_agencia_sirac AG"   +
					"         , comcat_agencia_sirac AG2"   +
					"         , comrel_pyme_epo pe"   +
					"   WHERE c1.ic_folio = s.ic_folio"   +
					"     AND s.ic_documento = ds.ic_documento"   +
					"     AND ds.ic_documento = d.ic_documento"   +
					"     AND d.ic_pyme = p.ic_pyme"   +
					"     AND d.ic_epo = e.ic_epo"   +
					"     AND ds.ic_if = i.ic_if"   +
					"     AND d.ic_pyme = dom.ic_pyme "   +
					"     AND dom.ic_estado = AG.ic_estado (+)"   +
					"     AND d.ic_pyme = pe.ic_pyme"   +
					"     AND d.ic_epo = pe.ic_epo"   +
					"     AND pe.ic_oficina_tramitadora = AG2.ic_estado (+)"   +
					"     AND s.ic_estatus_solic = 2"   +
					"     AND s.ic_bloqueo = 3"   +
					"     AND c1.cg_estatus = 'S'"   +
					"     AND dom.cs_fiscal = 'S'"   +
					"   ORDER BY i.ig_tipo_piso desc,s.df_fecha_solicitud ASC"  ;
			//System.out.println(query+";");

			String conv_proveedores="", desa_proveedores="", cod_aprobacion="", codigo_aplicacion="";
			String acreditado = "", epoPef = "";

			int cod_agen=0, cod_subapli=0, cod_finan=0, ic_plazo=0, cod_prod_banco=0, ic_epo=0, cod_base_operac=0, no_line_credit=0, dia=0, mes=0, ano=0, no_doctos=0;
			long no_lineafondeo=0, cod_cliente=0;
			String scod_base_operac = null;
			String status = "";
			double mto_operac=0.0, mto_asignado=0.0, por_asig=0.0;
			int plazo_proyecto = 0;
			int tipoPiso;
			try {
				rs=base.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Proyectos.java(query): " + query );
				throw sqle;
			}




			queri="select ig_codigo_base, cs_recurso as acreditado from comrel_base_operacion "+
					"where ic_epo= ? "+
					" and ic_producto_nafin = ? "+
					" and ? between ig_plazo_minimo and ig_plazo_maximo"+
					" and ic_moneda = ? "+	// Agregado EGB 30/06/2003
					" and cs_tipo_plazo = ? "+	// Agregado FODA 049-2003
					" and ig_tipo_cartera = ? ";
			PreparedStatement psBaseOp = base.queryPrecompilado(queri);

			queri = "select fecha_hoy + ? "+
					" , trunc(sysdate) + ? "+
					" from mg_calendario "+
					"where CODIGO_EMPRESA = ? AND codigo_aplicacion = ?";
			PreparedStatement psCal = base.queryPrecompilado(queri);


			queri = "update com_solicitud set cs_obra_publica = ? " +
					" where ic_folio=?";
			PreparedStatement psUpdSolObra = base.queryPrecompilado(queri);

			String sqlInsC05 = "insert into com_control05(ic_folio,cc_codigo_aplicacion,ic_error_proceso, "+
					"df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
					"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,ig_proceso_numero "+
					", cc_producto, df_diversos) "+
					"values(?,?,?,SYSDATE,?,?,?,?,?,?,?,to_date(?, 'dd/mm/yyyy') )";
			PreparedStatement psInsC05 = null;


			String sqlCatErr =
					"select cc_codigo_aplicacion from comcat_error_procesos "+
					"where ic_error_proceso=? " +
					" and cc_codigo_aplicacion=? ";
			PreparedStatement psCatErr = null;


			String sqlAprob = "select codigo_aprobacion, cupo_minimo, cupo_limite"+
				" from mg_aprobacion"+
				" where ? between cupo_minimo and cupo_limite";
			PreparedStatement psAprob = null;


			String sqlCntC05 = "select count(*) from com_control05 where ic_folio=? ";
			PreparedStatement psCntC05 = null;

			String sqlUpdC05 =
					"update com_control05 set cc_codigo_aplicacion=? "+
					",ic_error_proceso=? ,df_hora_proceso=SYSDATE ,ig_codigo_agencia=?"+
					",ig_codigo_sub_aplicacion=?,ig_numero_linea_fondeo=?"+
					",ig_codigo_financiera=? ,ig_aprobado_por=?"+
					", ig_proceso_numero=? "+
					", cc_producto = ?"+
					", df_diversos = to_date(?, 'dd/mm/yyyy') "+
					" where ic_folio=?";
			PreparedStatement psUpdC05 = null;

			String sqlInsC02 =
					"insert into com_control02(ic_folio,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
					"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
					"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
					"ig_codigo_sub_aplicacion_proy,cs_estatus, ig_numero_prestamo_pedido "+
					", cs_recurso, cg_proyectos_log "+
					", cc_producto, df_diversos, df_proyectos)"+
					"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,to_date(?,'dd/mm/yyyy'),SYSDATE)";
			PreparedStatement psInsC02 = null;

			String sqlUpdC01 =
					" UPDATE com_control01 " +
					" SET df_hora_proceso = sysdate "+
					" , cg_estatus=? "+
					" , ig_aprobado_por=?"+
					" , cg_descripcion=?"+
					" , cg_proyectos_log = ?"+
					" WHERE ic_folio=?";
			PreparedStatement psUpdC01 = null;

			while(rs.next()) {
				psBaseOp.clearParameters();
				psUpdSolObra.clearParameters();
				psCal.clearParameters();

				errorTransaccion = false;
				errorDesconocido = false;
        		obraPublica = false;
				/*
				Se asigna la variable de numero de prestamo obtenido en el credito de Financiamiento a Pedidos
				para determinar si se va a realizar una cobranza
				*/
				numPrestamoPedido = rs.getString("IG_NUMERO_PRESTAMO_PEDIDO");
				icMoneda = rs.getInt("IC_MONEDA");
				acreditado = "S";
				cod_base_operac = 0;
				cod_prod_banco = 0;
				cod_cliente = 0;

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio=rs.getString("IC_FOLIO");
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");					/* Valor 16 */
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");			/* Valor 17 */
				//F084-2006
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");			/* Valor 15 */
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=rs.getString("IG_APROBADO_POR");               /* Valor 5 */
				cod_aprobacion=aprobado_por;                				/* Valor 23 */
				descripcion=rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				mto_operac = (tipoPiso==1)?rs.getDouble("FN_MONTO_DSCTO"):rs.getDouble("FN_MONTO_OPERACION");			/* Valor 7 */
				mto_asignado=mto_operac;                    				/* Valor 9, Valor 30 y Valor 34 */
				estatus=rs.getString("CG_ESTATUS");
				tipo_soli=rs.getString("CS_TIPO_SOLICITUD");
				ic_docto=rs.getString("IC_DOCUMENTO");
				ic_plazo=rs.getInt("IG_PLAZO");
				//plazo_proyecto = (ic_plazo>558)? 558:ic_plazo;            /* Valor 12 */
				plazo_proyecto = (ic_plazo>180)? 180:ic_plazo;            /* Valor 12 */
				ic_epo=rs.getInt("IC_EPO");
				vtas_exterior = rs.getDouble("FN_VENTAS_NET_EXP");

				numPagosCobranza=rs.getString("NUMPAGOS");

				cod_sub_aplicacion = (icMoneda==54)?202:201;
				cod_sub_apli_det = (icMoneda==54)?221:211;

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				epoPef = rs.getString("EPO_PEF");
				csFloating = rs.getString("CS_FLOATING")==null?"N":rs.getString("CS_FLOATING"); //NE_2018_01 QC

				if ("S".equals(epoPef)){
				  try {
					clasifObjetoGasto = rs.getInt("CG_CLAVE_PRESUPUESTARIA");
					obraPublica = (clasifObjetoGasto>6000 && clasifObjetoGasto <= 6999)?true:false;
				  }catch (SQLException sqle){
					obraPublica = false;
				  }catch (NumberFormatException nfe){
					obraPublica = false;
				  }
				}


				//System.out.println(queri);
				try {
					psCal.setInt(1, plazo_proyecto);
					psCal.setInt(2, (ic_plazo>180)?ic_plazo:plazo_proyecto);
					psCal.setInt(3, 1);
					psCal.setString(4, "BLC");
					rs1=psCal.executeQuery();
				} catch(SQLException sqle) {
					//System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if (rs1.next()){
					fecha_max_desem = rs1.getDate(1);/* Valor 13 */
					fecha_max_pago = rs1.getDate(2);/* Valor 14 */
				}
				rs1.close();

				/* Valor 18 y 19*/
				conv_proveedores=rs.getString("CG_CONV_PROV");
				// INICIA FODA 049-2003
				if (tipoPiso==1) {
					tipo_plazo = (numPrestamoPedido==null)?"F":"C"; // En este producto se utiliza el tipo de plazo, si es Factoraje o Cobranza
				} else {
					tipo_plazo = (numPagosCobranza==null || numPagosCobranza.equals("") || numPagosCobranza.equals("0"))?"F":"C";
					//tipo_plazo = "F";
				}

				//FODEA 045-2009 SE AJUSTA PARA EL IDENTIFICAR SI EL FONDEO ES PARA OBRA PUBLICA, NO SE MODIFICA SI EL REGISTRO ES DE COBRANZA
				if (obraPublica && !"C".equals(tipo_plazo)){
				  tipo_plazo = "O"; //FACTORAJE OBRA PUBLICA
				}
				
				//NE_2018_01 QC (e)
				if ("S".equals(csFloating)  ) {
					tipo_plazo = "L";
				}
				//NE_2018_01 QC (S)
				
				System.out.println("*************Folio: "+folio+" TomaTipo: "+tipo_plazo);
			//System.out.println(queri);
				try {
					psBaseOp.setInt(1, ic_epo);
					psBaseOp.setInt(2, 1);
					psBaseOp.setInt(3, ic_plazo);
					psBaseOp.setInt(4, icMoneda);
					psBaseOp.setString(5, tipo_plazo);
					psBaseOp.setInt(6, tipoPiso);

					rs1=psBaseOp.executeQuery();
				} catch(SQLException sqle) {
					//System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if(rs1.next()) {
					cod_base_operac=rs1.getInt("IG_CODIGO_BASE");
					acreditado = rs1.getString("ACREDITADO");
					scod_base_operac=rs1.getString(1);
					//F084-2006
					cod_cliente = ("S".equals(acreditado))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");
				}
				rs1.close();


				if((conv_proveedores == null || conv_proveedores.equals("N")) && tipoPiso == 2) {
					if  (tipo_plazo.equals("F") ) {
						queri="select IG_BASE_OPER_GRAL from comcat_producto_nafin "+
							" where ic_producto_nafin = 1";
						//System.out.println(queri);
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Proyectos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							cod_base_operac=rs1.getInt("IG_BASE_OPER_GRAL");
							scod_base_operac=rs1.getString("IG_BASE_OPER_GRAL");
						}
						rs1.close();
						base.cierraStatement();

						//cod_base_operac=7602;		//CAMBIO DE 103302 A 7602. RJV 31/12/2002

						//F084-2006
						cod_cliente=rs.getLong("IN_NUMERO_SIRAC_PYME");
					}
				}

				if (cod_base_operac!=0) {
					cod_prod_banco = (scod_base_operac.length() > 4)? Integer.parseInt(scod_base_operac.substring(0, 2)): Integer.parseInt(scod_base_operac.substring(0, 1));
				}

				// TERMINA FODA 049-2003

				/* Valor 20 */
				if (icMoneda == 1) {
					desa_proveedores=(rs.getString("CG_DESARROLLO_PROV")==null || rs.getString("CG_DESARROLLO_PROV").equals("N"))?"N":"S";
				} else {
					desa_proveedores=(cod_prod_banco==1)?"S":"N";
				}

				/* Llama al API (Store Procedure) */
				try {
					cs = base.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					/*cod_sub_aplicacion = (numPrestamoPedido==null)?113:126;
					if ( ic_epo == 256 && cod_sub_aplicacion == 113) {
						cod_sub_aplicacion = 126;
					}*/ // Se comento F002-2016
					HashMap listDatoParam = (HashMap)inter.getValueParametrizado(String.valueOf(ic_epo),"1C","PY");//  F002-2016
					cod_sub_aplicacion = Integer.parseInt((String)listDatoParam.get("3"));//  F002-2016
					// Foda 062 - 2004, 068 - 2004
					if (numPrestamoPedido==null) {
						// Infonavit Agencia 90
						cod_agencia = 90;
						//cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): rs.getInt("IG_AGENCIA_SIRAC_FISCAL");
					} else {
						cod_agencia = ("T".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL"): 90;
					}


					cod_sub_apli_det = cod_sub_aplicacion;
					cs.setInt(3, cod_sub_aplicacion);
					cs.setNull(15, Types.INTEGER);
					cs.setNull(16, Types.INTEGER);
					cs.setNull(17, Types.INTEGER);
					cs.setNull(20, Types.INTEGER);
					cs.setNull(21, Types.INTEGER);
					cs.setNull(22, Types.INTEGER);
					cs.setNull(23, Types.INTEGER);
				} else {

					//Foda 062-2004, 068 - 2004
					cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL") : 90;



					cs.setInt(3, cod_sub_aplicacion);
					cs.setLong(15, no_lineafondeo);
					cs.setInt(16, cod_agen);
					cs.setInt(17, cod_subapli);
					cs.setString(20, desa_proveedores);
					cs.setNull(21, Types.INTEGER);
					if (icMoneda==54) {
						cs.setDouble(22, vtas_exterior);
					} else {
						cs.setNull(22, Types.INTEGER);
					}
					cs.setString(23, cod_aprobacion);
				}
				System.out.println("*************Folio: "+folio+" TomaTipo: "+tipo_plazo);
				cs.setNull(1, Types.INTEGER);
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, cod_agencia);
				cs.setString(4, desc);
				cs.setString(5, aprobado_por);
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.setString(6, cod_funcionario);
				cs.setDouble(7, mto_operac);
				//F084-2006
				cs.setLong(8, cod_cliente);
				cs.setDouble(9, mto_asignado);
				cs.setString(10, cod_tipo_linea);
				cs.setInt(11, frecuencia);
				cs.setInt(12, plazo_proyecto);
				cs.setDate(13, fecha_max_desem,Calendar.getInstance());
				cs.setDate(14, fecha_max_pago,Calendar.getInstance());
				cadena.setLength(0);
				cadena.append("p1:=null;"+"\n"+
				"p2:="+cod_agencia+";"+"\n"+
				"p3:="+cod_sub_aplicacion+";"+"\n"+
				"p4:='"+ desc +"';"+"\n"+
				"p5:="+ aprobado_por +";"+"\n"+
				"p6:='"+ cod_funcionario +"';"+"\n"+
				"p7:="+ mto_operac +";"+"\n"+
				"p8:="+ cod_cliente +";"+"\n"+
				"p9:="+ mto_asignado +";"+"\n"+
				"p10:='"+ cod_tipo_linea +"';"+"\n"+
				"p11:="+ frecuencia +";"+"\n"+
				"p12:="+ plazo_proyecto +";"+"\n"+
				"p13:=to_date('"+fecha_max_desem+"','dd/mm/yyyy');"+"\n"+
				"p14:=to_date('"+fecha_max_pago+"','dd/mm/yyyy');"+"\n");
				if (tipoPiso==1) {
					cadena.append("p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n");
				} else {
					cadena.append("p15:="+ no_lineafondeo +";"+"\n"+
					"p16:="+ cod_agen +";"+"\n"+
					"p17:="+ cod_subapli +";"+"\n");
				}

				cs.setInt(18, cod_prod_banco);
				cs.setInt(19, cod_base_operac);

				cadena.append("p18:="+ cod_prod_banco +";"+"\n"+
				"p19:="+ cod_base_operac +";"+"\n");
				if (tipoPiso==1) {
					cadena.append("p20:=null;"+"\n"+
					"p21:=null;"+"\n"+
					"p22:=null;"+"\n"+
					"p23:=null;"+"\n");
				} else {
					cadena.append("p20:='"+ desa_proveedores +"';"+"\n"+
						"p21:=null;"+"\n");
					if (icMoneda==54){
						cadena.append("p22:="+vtas_exterior+";"+"\n");
					} else {
						cadena.append("p22:=null;"+"\n");
					}
					cadena.append("p23:="+ cod_aprobacion +";"+"\n");
				}

				/* Valor 24 */
				if (tipoPiso==1) {
					cs.setNull(24, Types.INTEGER);
					mto_intermediario = 0;
					mto_cliente = 0;
					cadena.append("p24:=null;"+"\n");
				} else {
					queri="select ag.codigo_agencia_banco "+
						"from mg_agencias_generales ag, com_control01 c "+
						"where c.ic_folio = " + folio + " AND ag.codigo_financiera = c.ig_codigo_financiera ";
					//System.out.println(queri);
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next()) {
						cod_agen_interme=rs1.getInt(1);
						cs.setInt(24, cod_agen_interme);
						cadena.append("p24:="+ cod_agen_interme +";"+"\n");
					} else {
						cs.setNull(24, Types.INTEGER);
						cadena.append("p24:=null;"+"\n");
					}
					rs1.close(); base.cierraStatement();
				}
				cs.setString(25, reestructura);
				cs.setNull(26, Types.INTEGER);
				cs.setString(27, verific_estrato);
				cs.setDouble(28, porcent_asignado);
				cs.setInt(29, cod_sub_apli_det);
				cs.setDouble(30, mto_asignado);
				cs.setDouble(31, porcent_asignado_det);
				cs.registerOutParameter(31, Types.INTEGER);
				cs.setDouble(32, mto_intermediario);
				cs.setDouble(33, mto_cliente);
				cs.setDouble(34, mto_asignado);
				cs.setNull(35, Types.INTEGER);
				cs.registerOutParameter(35, Types.INTEGER);

				cadena.append(
					"p25:="+ reestructura +";"+"\n"+
					"p26:=null;"+"\n"+
					"p27:='"+ verific_estrato +"';"+"\n"+
					"p28:="+ porcent_asignado +";"+"\n"+
					"p29:="+ cod_sub_apli_det +";"+"\n"+
					"p30:="+ mto_asignado +";"+"\n"+
					"p31:="+ porcent_asignado_det +";"+"\n"+
					"p32:="+ mto_intermediario +";"+"\n"+
					"p33:="+ mto_cliente +";"+"\n"+
					"p34:="+ mto_asignado +";"+"\n"+
					"p35:=null;"+"\n");

				System.out.println(cadena.toString());
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						no_line_credit = cs.getInt(1);
						por_asig = cs.getDouble(31);
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						status = cs.getString(35);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena.append(sqle);
					errorDesconocido = true;
					status="9";
				}


				System.out.println("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				cadena.append("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					base.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						// Para la causa 43.
						if(status.equals("43")) {
							if (icMoneda==54) {
								queri = " SELECT fn_valor_compra * "  + mto_operac + " as monto_calc" +
									"	from com_tipo_cambio"  +
									"	where ic_moneda = 54"  +
									"	and dc_fecha = (select max(dc_fecha)"  +
									"                from com_tipo_cambio"  +
									"                where ic_moneda = 54) ";

								try {
									rs1=base.queryDB(queri);
								} catch(SQLException sqle) {
									System.out.println("ProyectosE.java(queri): " + queri );
									throw sqle;
								}
								if(rs1.next()) {
									mto_operac = rs1.getDouble(1);
								}
								rs1.close(); base.cierraStatement();
							}

							try {
								if(psAprob == null) {
									psAprob = base.queryPrecompilado(sqlAprob);
								} else {
									psAprob.clearParameters();
								}

								psAprob.setBigDecimal(1, new BigDecimal(mto_operac));
								rs1=psAprob.executeQuery();
							} catch(SQLException sqle) {
								//System.out.println("Proyectos.java(queri): " + queri );
								throw sqle;
							}
							if(rs1.next()) {
								aprobado_por = rs1.getString(1);
								descripcion = "Descripcion especial Causa 43";
							}
							rs1.close();
						}

						try {
							if(psCatErr == null) {
								psCatErr = base.queryPrecompilado(sqlCatErr);
							} else {
								psCatErr.clearParameters();
							}

							psCatErr.setInt(1,Integer.parseInt(status));
							psCatErr.setString(2,"PY");

							rs1=psCatErr.executeQuery();
						} catch(SQLException sqle) {
							//System.out.println("Proyectos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE"; status="6";
						}
						rs1.close();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";

					}

					try {
						if(psCntC05 == null) {
							psCntC05 = base.queryPrecompilado(sqlCntC05);
						} else {
							psCntC05.clearParameters();
						}

						psCntC05.setString(1, folio);
						rs1=psCntC05.executeQuery();
					} catch(SQLException sqle) {
						//System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); //base.cierraStatement();
					//aprobado_por=(aprobado_por==null)?"null":"'"+aprobado_por+"'";
					//System.out.println(queri);
					try {
						if(no_doctos==0) {
							if(psInsC05 == null) {
								psInsC05 = base.queryPrecompilado(sqlInsC05);
							} else {
								psInsC05.clearParameters();
							}

							if (tipoPiso==1) {

								psInsC05.setString(1, folio);
								psInsC05.setString(2, codigo_aplicacion);
								psInsC05.setInt(3, Integer.parseInt(status));
								psInsC05.setNull(4, Types.INTEGER);
								psInsC05.setNull(5, Types.INTEGER);
								psInsC05.setNull(6, Types.INTEGER);
								psInsC05.setInt(7, cod_finan);
								if(aprobado_por==null) {
									psInsC05.setNull(8, Types.INTEGER);
								} else {
									psInsC05.setInt(8, Integer.parseInt(aprobado_por));
								}
								psInsC05.setInt(9, 1);
								psInsC05.setString(10, strProducto);
								psInsC05.setString(11, strFechaDiversos);

							}
							else {
								psInsC05.setString(1, folio);
								psInsC05.setString(2, codigo_aplicacion);
								psInsC05.setInt(3, Integer.parseInt(status));
								psInsC05.setInt(4, cod_agen);
								psInsC05.setInt(5, cod_subapli);
								psInsC05.setLong(6, no_lineafondeo);
								psInsC05.setInt(7, cod_finan);
								if(aprobado_por==null) {
									psInsC05.setNull(8, Types.INTEGER);
								} else {
									psInsC05.setInt(8, Integer.parseInt(aprobado_por));
								}
								psInsC05.setInt(9, 1);
								psInsC05.setString(10, strProducto);
								psInsC05.setString(11, strFechaDiversos);

							}

							psInsC05.executeUpdate();
						} else {
							if(psUpdC05 == null) {
								psUpdC05 = base.queryPrecompilado(sqlUpdC05);
							} else {
								psUpdC05.clearParameters();
							}

							if (tipoPiso==1) {
								psUpdC05.setString(1,codigo_aplicacion);
								psUpdC05.setInt(2,Integer.parseInt(status));
								psUpdC05.setNull(3,Types.INTEGER);
								psUpdC05.setNull(4,Types.INTEGER);
								psUpdC05.setNull(5,Types.INTEGER);
								psUpdC05.setInt(6,cod_finan);
								if(aprobado_por==null) {
									psUpdC05.setNull(7, Types.INTEGER);
								} else {
									psUpdC05.setInt(7,Integer.parseInt(aprobado_por));
								}
								psUpdC05.setInt(8,1);
								psUpdC05.setString(9,strProducto);
								psUpdC05.setString(10,strFechaDiversos);
								psUpdC05.setString(11,folio);
							}
							else {
								psUpdC05.setString(1,codigo_aplicacion);
								psUpdC05.setInt(2,Integer.parseInt(status));
								psUpdC05.setInt(3,cod_agen);
								psUpdC05.setInt(4,cod_subapli);
								psUpdC05.setLong(5,no_lineafondeo);
								psUpdC05.setInt(6,cod_finan);
								if(aprobado_por==null) {
									psUpdC05.setNull(7, Types.INTEGER);
								} else {
									psUpdC05.setInt(7,Integer.parseInt(aprobado_por));
								}
								psUpdC05.setInt(8,1);
								psUpdC05.setString(9,strProducto);
								psUpdC05.setString(10,strFechaDiversos);
								psUpdC05.setString(11,folio);
							}
							psUpdC05.executeUpdate();
						}


						//descripcion=(descripcion==null)?"null":"'"+descripcion+"'";

						try {
							if(psUpdC01 == null) {
								psUpdC01 = base.queryPrecompilado(sqlUpdC01);
							} else {
								psUpdC01.clearParameters();
							}

							psUpdC01.setString(1, "E");
							if(aprobado_por==null) {
								psUpdC01.setNull(2, Types.INTEGER);
							} else {
								psUpdC01.setInt(2, Integer.parseInt(aprobado_por));
							}
							psUpdC01.setString(3, descripcion);
							psUpdC01.setString(4, cadena.toString());
							psUpdC01.setString(5, folio);

							psUpdC01.executeUpdate();
						} catch(SQLException sqle) {
							System.out.println("Error al Actualizar en Control01.");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en Control05.");
						errorTransaccion = true;
					}
				} else {
//							System.out.println(queri);
					try {
						if(psInsC02 == null) {
							psInsC02 = base.queryPrecompilado(sqlInsC02);
						} else {
							psInsC02.clearParameters();
						}

						/* Si no hubo ningun error se Insertan los valores de control01 a com_control02 */
						if (tipoPiso==1) {
							psInsC02.setString(1,folio);
							psInsC02.setNull(2,Types.INTEGER);
							psInsC02.setNull(3,Types.INTEGER);
							psInsC02.setNull(4,Types.INTEGER);
							psInsC02.setInt(5, cod_finan);
							if(aprobado_por==null) {
								psInsC02.setNull(6, Types.INTEGER);
							} else {
								psInsC02.setInt(6, Integer.parseInt(aprobado_por));
							}
							psInsC02.setString(7, descripcion);
							psInsC02.setBigDecimal(8, new BigDecimal(mto_operac));
							psInsC02.setInt(9, no_line_credit);
							psInsC02.setInt(10, cod_agencia);
							psInsC02.setInt(11, cod_sub_aplicacion);
							psInsC02.setString(12,estatus);
							if (numPrestamoPedido==null) {
								psInsC02.setNull(13, Types.INTEGER);
							} else {
								psInsC02.setInt(13, Integer.parseInt(numPrestamoPedido));
							}
							psInsC02.setString(14,acreditado);
							psInsC02.setString(15,cadena.toString());
							psInsC02.setString(16,strProducto);
							psInsC02.setString(17,strFechaDiversos);
						}
						else {
							psInsC02.setString(1,folio);
							psInsC02.setInt(2, cod_agen);
							psInsC02.setInt(3, cod_subapli);
							psInsC02.setLong(4, no_lineafondeo);
							psInsC02.setInt(5, cod_finan);
							if(aprobado_por==null) {
								psInsC02.setNull(6, Types.INTEGER);
							} else {
								psInsC02.setInt(6, Integer.parseInt(aprobado_por));
							}
							psInsC02.setString(7, descripcion);
							psInsC02.setBigDecimal(8, new BigDecimal(mto_operac));
							psInsC02.setInt(9, no_line_credit);
							psInsC02.setInt(10, cod_agencia);
							psInsC02.setInt(11, cod_sub_aplicacion);
							psInsC02.setString(12,estatus);
							if (numPrestamoPedido==null) {
								psInsC02.setNull(13, Types.INTEGER);
							} else {
								psInsC02.setInt(13, Integer.parseInt(numPrestamoPedido));
							}
							psInsC02.setString(14,acreditado);
							psInsC02.setString(15,cadena.toString());
							psInsC02.setString(16,strProducto);
							psInsC02.setString(17,strFechaDiversos);

						}
						psInsC02.executeUpdate();
						/* Borramos el Documento de control01 si esta Bien. */
						queri = "delete com_control01 where ic_folio='"+folio+"'";
//							System.out.println(queri);
						try {
							base.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Borrar los Datos en com_control01");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar los Datos en com_control02");
						errorTransaccion = true;
					}
				}

				//FODEA 045-2009 SE IDENTIFICA EL REGISTRO COMO FONDEO DE OBRA PUBLICA

				try {
					psUpdSolObra.setString(1, ("O".equals(tipo_plazo))?"S":"N");
					psUpdSolObra.setString(2,folio);
					psUpdSolObra.executeUpdate();
				} catch(SQLException sqle) {
					System.out.println("Error al actualizar los Datos de Fondeo en com_solicitud");
					errorTransaccion = true;
				}


				if (!errorTransaccion) {
					base.terminaTransaccion(true);
				} else {
					base.terminaTransaccion(false);
				}
			} //while
			if (cs !=null) cs.close();
			rs.close();
			base.cierraStatement();

			if(psInsC05 !=null) {
				psInsC05.close();
			}
			if(psUpdC05 !=null) {
				psUpdC05.close();
			}
			if(psInsC02 !=null) {
				psInsC02.close();
			}
			if(psUpdC01 !=null) {
				psUpdC01.close();
			}
			if (psCatErr != null){
				psCatErr.close();
			}
			if (psAprob != null){
				psAprob.close();
			}

			if (psCntC05 != null){
				psCntC05.close();
			}

			psCal.close();
			psUpdSolObra.close();
			psBaseOp.close();

			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////
			////I n i c i o   I n t e r f a s e   F i n a n c i a m i e n t o   a   P e d i d o s////
			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////

			System.out.println(" Inicio Operaciones Financiamiento a Pedidos ");


			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";
			if ("T".equals(oficTramXProd.get("2-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and P.ic_pyme = pexp.ic_pyme"+
								" and P.ic_epo = pexp.ic_epo"+
								" and P.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("2-").toString())) {
				strFrom = " , com_domicilio D"+
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and PY.ic_pyme = D.ic_pyme"   +
							"  and D.cs_fiscal = 'S'"   +
							"  and D.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}



			query = "SELECT C1.ic_pedido"+
				", C1.ig_codigo_agencia"+
				", C1.ig_codigo_sub_aplicacion "+
				", C1.ig_numero_linea_fondeo"+
				", C1.ig_codigo_financiera"+
				", C1.ig_aprobado_por "+
				", C1.cg_descripcion"+
				", C1.fn_monto_operacion"+
				", C1.cg_estatus "+
				", PS.ig_plazo "+
				", TO_CHAR(A.df_v_descuento,'DD/MM/YYYY') "+
				", PY.in_numero_sirac "+
				", UPPER(E.cg_conv_prov) CG_CONV_PROV "+
				", P.ic_epo "+
				", UPPER(PY.cg_desarrollo_prov) CG_DESARROLLO_PROV "+
				", I.ig_tipo_piso " +
				", P.fn_monto " +
				", P.ic_moneda " +
				", DECODE(NVL(py.fn_ventas_net_exp,0),0,1,py.fn_ventas_net_exp) fn_ventas_net_exp "  +
				", PY.in_numero_sirac as in_numero_sirac_pyme"  +
				", E.ig_numero_sirac as in_numero_sirac_epo"  +
				", c1.cc_producto " +
				", to_char(c1.df_diversos, 'dd/mm/yyyy') as df_diversos " +
				strSelect +
				" FROM com_controlant01 C1"+
				", com_anticipo A"+
				", com_pedido P"+
				", comcat_pyme PY"+
				", comcat_epo E "+
				", com_pedido_seleccionado PS"+
				", comcat_if I "+
				", com_linea_credito LC "+
				strFrom +
				" WHERE C1.cg_estatus='S' "+
				" and C1.ic_pedido = A.ic_pedido "+
				" and A.ic_pedido = PS.ic_pedido "+
				" and PS.ic_pedido = P.ic_pedido "+
				" and P.ic_pyme = PY.ic_pyme "+
				" and P.ic_epo = E.ic_epo "+
				" and PS.ic_linea_credito = LC.ic_linea_credito "+
				" and LC.ic_if = I.ic_if "+
				strCondiciones +
				" and A.ic_estatus_antic = 2 "+
				" and A.ic_bloqueo = 3 "+
				" ORDER BY I.ig_tipo_piso desc ";
			//System.out.println(query+";");

			conv_proveedores="";
			desa_proveedores="";
			cod_aprobacion="";
			codigo_aplicacion="";
			no_lineafondeo=0;
			cod_agen=0;
			cod_subapli=0;
			cod_finan=0;
			cod_cliente=0;
			ic_plazo=0;
			plazo_proyecto=0;
			cod_prod_banco=0;
			ic_epo=0;
			cod_base_operac=0;
			scod_base_operac=null;
			no_line_credit=0;
			dia=0;
			mes=0;
			ano=0;
			no_doctos=0;
			status = "";
			mto_operac=0.0;
			mto_asignado=0.0;
			por_asig=0.0;

			try {
				rs=base.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Proyectos.java(query): " + query );
				throw sqle;
			}
			while(rs.next()){
				errorTransaccion = false;
				errorDesconocido = false;
				icMoneda = rs.getInt("IC_MONEDA");
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				acreditado = "S";
				cod_base_operac = 0;
				cod_prod_banco = 0;
				cod_cliente = 0;

				folio=rs.getString("IC_PEDIDO");

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 2 */
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");			/* Valor 16 */
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");	/* Valor 17 */
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");	/* Valor 15 */
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=rs.getString("IG_APROBADO_POR"); 		/* Valor 5 */
				cod_aprobacion=aprobado_por;                		/* Valor 23 */
				descripcion=rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				mto_operac = (tipoPiso==1)?rs.getDouble("FN_MONTO_OPERACION"):rs.getDouble("FN_MONTO_OPERACION");			/* Valor 7 */
				mto_asignado=mto_operac;                    		/* Valor 9, Valor 30 y Valor 34 */
				estatus=rs.getString("CG_ESTATUS");
				ic_plazo=rs.getInt("IG_PLAZO");
				plazo_proyecto = (ic_plazo>180)? 180:ic_plazo;		/* Valor 12 */
				ic_epo=rs.getInt("IC_EPO");
				vtas_exterior = rs.getDouble("FN_VENTAS_NET_EXP");

				cod_sub_aplicacion = (icMoneda==54)?202:201;
				cod_sub_apli_det = (icMoneda==54)?221:211;

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");

				/*
				queri = "select fecha_hoy + "+ic_plazo+
					" , sysdate + "+ic_plazo+
					" from mg_calendario "+
					"where codigo_aplicacion = 'BLC'";
				*/
				queri = "select fecha_hoy + "+plazo_proyecto+
					" , trunc(sysdate) + ";
				queri += (ic_plazo>180)? " " + ic_plazo: " "+plazo_proyecto;
				queri +=" from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BLC'";

				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if (rs1.next()) {
					fecha_max_desem = rs1.getDate(1);	/* Valor 13 */
					fecha_max_pago = rs1.getDate(2);	/* Valor 14 */
				}
				rs1.close(); base.cierraStatement();

				//F084-2006
				cod_cliente=rs.getLong("IN_NUMERO_SIRAC");          /* Valor 8 */
				/* Valor 18 y 19*/

				// INICIA FODA 049-2003
				queri="select ig_codigo_base, cs_recurso as acreditado from comrel_base_operacion "+
						"where ic_epo="+ic_epo+
						" and ic_producto_nafin = 2"+
						" and "+ic_plazo+" between ig_plazo_minimo and ig_plazo_maximo"+
						" and ic_moneda = "+icMoneda+	// Agregado EGB 30/06/2003
						" and ig_tipo_cartera = " + tipoPiso;
				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if(rs1.next()) {
					cod_base_operac=rs1.getInt("IG_CODIGO_BASE");
					acreditado = rs1.getString("ACREDITADO");
					scod_base_operac=rs1.getString(1);
					//F084-2006
					cod_cliente = ("S".equals(acreditado))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");
				}
				if (cod_base_operac!=0) {
					cod_prod_banco = (scod_base_operac.length() > 4)? Integer.parseInt(scod_base_operac.substring(0, 2)): Integer.parseInt(scod_base_operac.substring(0, 1));
				}
				rs1.close();
				base.cierraStatement();
				// TERMINA FODA 049-2003

				/* Valor 20 */
				// FODA 074 - 2003
				if (icMoneda == 1) {
					desa_proveedores=(rs.getString("CG_DESARROLLO_PROV")==null || rs.getString("CG_DESARROLLO_PROV").equals("N"))?"N":"S";
				} else {
					desa_proveedores=(cod_prod_banco==15)?"S":"N";
				}


				/* Llama al API (Store Procedure) */
				try {
					cs = base.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					cod_sub_aplicacion = 125;
					cod_sub_apli_det = cod_sub_aplicacion;
					cs.setInt(3, cod_sub_aplicacion);
					cs.setNull(15, Types.INTEGER);
					cs.setNull(16, Types.INTEGER);
					cs.setNull(17, Types.INTEGER);
					cs.setNull(20, Types.INTEGER);
					cs.setNull(21, Types.INTEGER);
					cs.setNull(22, Types.INTEGER);
					cs.setNull(23, Types.INTEGER);
				} else {
					cs.setInt(3, cod_sub_aplicacion);
					cs.setLong(15, no_lineafondeo);
					cs.setInt(16, cod_agen);
					cs.setInt(17, cod_subapli);
					cs.setString(20, desa_proveedores);
					cs.setNull(21, Types.INTEGER);
					if (icMoneda==54) {
						cs.setDouble(22, vtas_exterior);
					} else {
						cs.setNull(22, Types.INTEGER);
					}
					cs.setString(23, cod_aprobacion);
				}

				cs.setNull(1, Types.INTEGER);
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, cod_agencia);
				cs.setString(4, desc);
				cs.setString(5, aprobado_por);
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.setString(6, cod_funcionario);
				cs.setDouble(7, mto_operac);
				//F084-2006
				cs.setLong(8, cod_cliente);
				cs.setDouble(9, mto_asignado);
				cs.setString(10, cod_tipo_linea);
				cs.setInt(11, frecuencia);
				cs.setInt(12, plazo_proyecto);
				cs.setDate(13, fecha_max_desem,Calendar.getInstance());
				cs.setDate(14, fecha_max_pago,Calendar.getInstance());
				cadena.setLength(0);
				cadena.append("p1:=null;"+"\n"+
				"p2:="+cod_agencia+";"+"\n"+
				"p3:="+cod_sub_aplicacion+";"+"\n"+
				"p4:='"+ desc +"';"+"\n"+
				"p5:="+ aprobado_por +";"+"\n"+
				"p6:='"+ cod_funcionario +"';"+"\n"+
				"p7:="+ mto_operac +";"+"\n"+
				"p8:="+ cod_cliente +";"+"\n"+
				"p9:="+ mto_asignado +";"+"\n"+
				"p10:='"+ cod_tipo_linea +"';"+"\n"+
				"p11:="+ frecuencia +";"+"\n"+
				"p12:="+ plazo_proyecto +";"+"\n"+
				"p13:=to_date('"+fecha_max_desem+"','dd/mm/yyyy');"+"\n"+
				"p14:=to_date('"+fecha_max_pago+"','dd/mm/yyyy');"+"\n");
				if (tipoPiso==1) {
					cadena.append("p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n");
				} else {
					cadena.append("p15:="+ no_lineafondeo +";"+"\n"+
					"p16:="+ cod_agen +";"+"\n"+
					"p17:="+ cod_subapli +";"+"\n");
				}

				cs.setInt(18, cod_prod_banco);
				cs.setInt(19, cod_base_operac);

				cadena.append("p18:="+ cod_prod_banco +";"+"\n"+
				"p19:="+ cod_base_operac +";"+"\n");
				if (tipoPiso==1) {
					cadena.append("p20:=null;"+"\n"+
					"p21:=null;"+"\n"+
					"p22:=null;"+"\n"+
					"p23:=null;"+"\n");
				} else {
					cadena.append("p20:='"+ desa_proveedores +"';"+"\n"+
					"p21:=null;"+"\n");
					if (icMoneda==54){
						cadena.append("p22:="+vtas_exterior+";"+"\n");
					} else {
						cadena.append("p22:=null;"+"\n");
					}

					cadena.append("p23:="+ cod_aprobacion +";"+"\n");
				}

				/* Valor 24 */
				if (tipoPiso==1) {
					cs.setNull(24, Types.INTEGER);
					mto_intermediario = 0;
					mto_cliente = 0;
					cadena.append("p24:=null;"+"\n");
				} else {
					queri="select ag.codigo_agencia_banco "+
						"from mg_agencias_generales ag, com_controlant01 c "+
						"where c.ic_pedido = " + folio + " AND ag.codigo_financiera = c.ig_codigo_financiera ";
					//System.out.println(queri);
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next()) {
						cod_agen_interme=rs1.getInt(1);
						cs.setInt(24, cod_agen_interme);
						cadena.append("p24:="+ cod_agen_interme +";"+"\n");
					} else {
						cs.setNull(24, Types.INTEGER);
						cadena.append("p24:=null;"+"\n");
					}
					rs1.close(); base.cierraStatement();
				}
				cs.setString(25, reestructura);
				cs.setNull(26, Types.INTEGER);
				cs.setString(27, verific_estrato);
				cs.setDouble(28, porcent_asignado);
				cs.setInt(29, cod_sub_apli_det);
				cs.setDouble(30, mto_asignado);
				cs.setDouble(31, porcent_asignado_det);
				cs.registerOutParameter(31, Types.INTEGER);
				cs.setDouble(32, mto_intermediario);
				cs.setDouble(33, mto_cliente);
				cs.setDouble(34, mto_asignado);
				cs.setNull(35, Types.INTEGER);
				cs.registerOutParameter(35, Types.INTEGER);

				cadena.append(
					"p25:="+ reestructura +";"+"\n"+
					"p26:=null;"+"\n"+
					"p27:='"+ verific_estrato +"';"+"\n"+
					"p28:="+ porcent_asignado +";"+"\n"+
					"p29:="+ cod_sub_apli_det +";"+"\n"+
					"p30:="+ mto_asignado +";"+"\n"+
					"p31:="+ porcent_asignado_det +";"+"\n"+
					"p32:="+ mto_intermediario +";"+"\n"+
					"p33:="+ mto_cliente +";"+"\n"+
					"p34:="+ mto_asignado +";"+"\n"+
					"p35:=null;"+"\n");

				System.out.println(cadena.toString());
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						no_line_credit = cs.getInt(1);
						por_asig = cs.getDouble(31);
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						status = cs.getString(35);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena.append(sqle);
					errorDesconocido = true;
					status="9";
				}

				System.out.println("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				cadena.append("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					base.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						// Para la causa 43.
						if(status.equals("43")) {
							if (icMoneda==54) {
								queri = " SELECT fn_valor_compra * "  + mto_operac + " as monto_calc" +
									"	from com_tipo_cambio"  +
									"	where ic_moneda = 54"  +
									"	and dc_fecha = (select max(dc_fecha)"  +
									"                from com_tipo_cambio"  +
									"                where ic_moneda = 54) ";

								try {
									rs1=base.queryDB(queri);
								} catch(SQLException sqle) {
									System.out.println("ProyectosE.java(queri): " + queri );
									throw sqle;
								}
								if(rs1.next()) {
									mto_operac = rs1.getDouble(1);
								}
								rs1.close(); base.cierraStatement();
							}
							queri = "select codigo_aprobacion, cupo_minimo, cupo_limite"+
									" from mg_aprobacion"+
									" where "+mto_operac+" between cupo_minimo and cupo_limite";
							try {
								rs1=base.queryDB(queri);
							} catch(SQLException sqle) {
								System.out.println("Proyectos.java(queri): " + queri );
								throw sqle;
							}
							if(rs1.next()) {
								aprobado_por = rs1.getString(1);
								descripcion = "Descripcion especial Causa 43";
							}
							rs1.close(); base.cierraStatement();
						}

						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							"where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PY'";
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Proyectos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE"; status="6";
						}
						rs1.close(); base.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from com_controlant05 where ic_pedido="+folio;
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); base.cierraStatement();
					aprobado_por=(aprobado_por==null)?"null":"'"+aprobado_por+"'";
					if(no_doctos==0) {
						if (tipoPiso==1) {
							queri = "insert into com_controlant05(ic_pedido,cc_codigo_aplicacion,ic_error_proceso, "+
								"df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
								"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,ig_proceso_numero "+
								", cc_producto, df_diversos) "+
								"values("+folio+",'"+codigo_aplicacion+"',"+status+" "+
								",SYSDATE, NULL, NULL, NULL"+
								","+cod_finan+","+aprobado_por+", 1 " +
								", '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy') )";
						}
						else {
							queri = "insert into com_controlant05(ic_pedido,cc_codigo_aplicacion,ic_error_proceso, "+
								"df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
								"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,ig_proceso_numero " +
								", cc_producto, df_diversos) "+
								"values("+folio+",'"+codigo_aplicacion+"',"+status+" "+
								",SYSDATE,"+cod_agen+","+cod_subapli+","+no_lineafondeo+" "+
								","+cod_finan+","+aprobado_por+", 1 " +
								", '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy') )";
						}
					} else {
						if (tipoPiso==1) {
							queri = "update com_controlant05 set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								",ic_error_proceso="+status+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia=NULL"+
								",ig_codigo_sub_aplicacion=NULL,ig_numero_linea_fondeo=NULL"+
								",ig_codigo_financiera="+cod_finan+" ,ig_aprobado_por="+aprobado_por+" "+
								",ig_proceso_numero = 1 "+
								", cc_producto = '"+strProducto+"'"+
								", df_diversos = to_date('"+strFechaDiversos+"', 'dd/mm/yyyy') "+
								" where ic_pedido = "+folio;
						} else {
							queri = "update com_controlant05 set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								",ic_error_proceso="+status+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+cod_agen+" "+
								",ig_codigo_sub_aplicacion="+cod_subapli+" ,ig_numero_linea_fondeo="+no_lineafondeo+" "+
								",ig_codigo_financiera="+cod_finan+" ,ig_aprobado_por="+aprobado_por+" "+
								",ig_proceso_numero = 1 "+
								", cc_producto = '"+strProducto+"'"+
								", df_diversos = to_date('"+strFechaDiversos+"', 'dd/mm/yyyy') "+
								" where ic_pedido = "+folio;
						}
					}
					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
						descripcion=(descripcion==null)?"null":"'"+descripcion+"'";
						queri = "UPDATE com_controlant01 " +
								" SET df_hora_proceso = sysdate "+
								" , cg_estatus='E' "+
								" , ig_aprobado_por = "+aprobado_por+
								" , cg_descripcion = "+descripcion+
								" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "'") + "'" +
								" WHERE ic_pedido = "+folio;
						//System.out.println(queri);
						try {
							base.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Actualizar en ControlAnt01.");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en ControlAnt05.");
						errorTransaccion = true;
					}

				} else {
					/* Si no hubo ningun error se Insertan los valores de com_controlAnt01 a com_controlAnt02 */
					if (tipoPiso==1) {
						queri = "insert into com_controlant02(ic_pedido,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
							"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
							"ig_codigo_sub_aplicacion_proy,cs_estatus,cs_recurso, cg_proyectos_log "+
							", cc_producto, df_diversos, df_proyectos) "+
							"values("+folio+", NULL, NULL,"+
							"NULL,"+cod_finan+","+aprobado_por+",'"+descripcion+"',"+
							""+mto_operac+","+no_line_credit+","+cod_agencia+","+
							""+cod_sub_aplicacion+",'"+estatus+"', '"+acreditado+"', '" + Comunes.protegeCaracter(cadena.toString(), '\'', "'") + "' "+
							", '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), SYSDATE)";
					}
					else {
						queri = "insert into com_controlant02(ic_pedido,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
							"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
							"ig_codigo_sub_aplicacion_proy,cs_estatus,cs_recurso, cg_proyectos_log "+
							", cc_producto, df_diversos, df_proyectos) "+
							"values('"+folio+"',"+cod_agen+","+cod_subapli+","+
							""+no_lineafondeo+","+cod_finan+","+aprobado_por+",'"+descripcion+"',"+
							""+mto_operac+","+no_line_credit+","+cod_agencia+","+
							""+cod_sub_aplicacion+",'"+estatus+"', '"+acreditado+"', '" + Comunes.protegeCaracter(cadena.toString(), '\'', "'") + "' "+
							", '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), SYSDATE)";
					}

					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
						/* Borramos el Documento de control01 si esta Bien. */
						queri = "delete com_controlant01 where ic_pedido = "+folio;
//							System.out.println(queri);
						try {
							base.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Borrar los Datos en com_controlAnt01");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar los Datos en com_controlAnt02");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					base.terminaTransaccion(true);
				} else {
					base.terminaTransaccion(false);
				}
			} //while
			if (cs !=null) cs.close();
			rs.close();
			base.cierraStatement();
			System.out.println(" Fin Operaciones Financiamiento a Pedidos");
			/* Fin Interfase Financiamiento a Pedidos */


			/* Inicio Interfase 1er Piso Credicadenas*/
			System.out.println(" Inicio Operaciones 1er Piso Credicadenas");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("5-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("5-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and PY.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}


			query = " SELECT int.cc_disposicion"   +
					" , int.ig_codigo_agencia"   +
					" , int.ig_codigo_sub_aplicacion"   +
					" , int.ig_numero_linea_fondeo"   +
					" , int.ig_codigo_financiera"   +
					" , int.ig_aprobado_por"   +
					" , int.cg_descripcion"   +
					" , int.fn_monto_operacion"   +
					" , DECODE(LC.CG_LINEA_TIPO, 'E', pl.in_plazo_dias, d.df_vencimiento - trunc(sysdate)) as ig_plazo "   +
					" , TO_CHAR(d.df_vencimiento,'DD/MM/YYYY')"   +
					" , PY.in_numero_sirac "   +
					" , UPPER(E.cg_conv_prov) CG_CONV_PROV"   +
					" , d.ic_epo"   +
					" , UPPER(PY.cg_desarrollo_prov) CG_DESARROLLO_PROV"   +
					" , I.ig_tipo_piso"   +
					" , d.fn_monto_credito as fn_monto"   +
					" , t.ic_moneda"   +
					" , DECODE(NVL(py.fn_ventas_net_exp,0),0,1,py.fn_ventas_net_exp) fn_ventas_net_exp"   +
					" , PY.in_numero_sirac as in_numero_sirac_pyme"  +
					" , E.ig_numero_sirac as in_numero_sirac_epo"  +
					" , s.ic_tipo_credito as CODIGO_SUB_APLICACION"+
					" , LC.CG_LINEA_TIPO as CG_TIPO_PLAZO"+
					strSelect +
					"  FROM inv_interfase int"   +
					" , inv_solicitud s"   +
					" , inv_disposicion d"   +
					" , comcat_pyme PY"   +
					" , comcat_epo E"   +
					" , comcat_if I"   +
					" , com_linea_credito LC"   +
					" , comcat_tasa t"   +
					" , comcat_plazo pl"   +
					strFrom +
					" WHERE s.cc_disposicion = int.cc_disposicion"   +
					"  AND s.cc_disposicion = d.cc_disposicion "   +
					"  and d.ic_pyme = PY.ic_pyme"   +
					"  and d.ic_epo = E.ic_epo"   +
					"  and d.ic_linea_credito = LC.ic_linea_credito"   +
					"  and LC.ic_if = I.ic_if"   +
					"  AND d.ic_tasa = t.ic_tasa"   +
					"  AND d.ic_plazo = pl.ic_plazo"   +
					strCondiciones +
					"  AND int.ig_numero_linea_credito is null"   +
					"  AND int.cc_codigo_aplicacion is null"   +
					"  AND int.ic_error_proceso is null"   +
					"  AND s.ic_estatus_solic = 2"   +
					"  AND s.ic_bloqueo = 3"   +
					" ORDER BY I.ig_tipo_piso desc"  ;
			//System.out.println(query+";");

			conv_proveedores="";
			desa_proveedores="";
			cod_aprobacion="";
			codigo_aplicacion="";
			no_lineafondeo=0;
			cod_agen=0;
			cod_subapli=0;
			cod_finan=0;
			cod_cliente=0;
			ic_plazo=0;
			plazo_proyecto=0;
			cod_prod_banco=0;
			ic_epo=0;
			cod_base_operac=0;
			scod_base_operac=null;
			no_line_credit=0;
			dia=0;
			mes=0;
			ano=0;
			no_doctos=0;
			status = "";
			mto_operac=0.0;
			mto_asignado=0.0;
			por_asig=0.0;

			try {
				rs=base.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Proyectos.java(query): " + query );
				throw sqle;
			}
			while(rs.next()){
				errorTransaccion = false;
				errorDesconocido = false;
				icMoneda = rs.getInt("IC_MONEDA");
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				acreditado = "S";
				cod_base_operac = 0;
				cod_prod_banco = 0;
				cod_cliente = 0;

				folio=rs.getString("CC_DISPOSICION");
				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 2 */
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");			/* Valor 16 */
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");	/* Valor 17 */
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");	/* Valor 15 */
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=rs.getString("IG_APROBADO_POR"); 		/* Valor 5 */
				cod_aprobacion=aprobado_por;                		/* Valor 23 */
				descripcion=rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				mto_operac = (tipoPiso==1)?rs.getDouble("FN_MONTO_OPERACION"):rs.getDouble("FN_MONTO_OPERACION");			/* Valor 7 */
				mto_asignado=mto_operac;                    		/* Valor 9, Valor 30 y Valor 34 */
				ic_plazo=rs.getInt("IG_PLAZO");
				plazo_proyecto = (ic_plazo>180)? 180:ic_plazo;		/* Valor 12 */
				ic_epo=rs.getInt("IC_EPO");
				vtas_exterior = rs.getDouble("FN_VENTAS_NET_EXP");
				tipo_plazo = rs.getString("CG_TIPO_PLAZO"); // Credicadenas o Exportacion

				cod_sub_aplicacion  = rs.getInt("CODIGO_SUB_APLICACION");
				cod_sub_apli_det 	= cod_sub_aplicacion;


				/*
				queri = "select fecha_hoy + "+ic_plazo+
					" , sysdate + "+ic_plazo+
					" from mg_calendario "+
					"where codigo_aplicacion = 'BLC'";
				*/
				queri = "select fecha_hoy + "+plazo_proyecto+
					" , trunc(sysdate) + ";
				queri += (ic_plazo>180)? " " + ic_plazo: " "+plazo_proyecto;
				queri +=" from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BLC'";


				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if (rs1.next()) {
					fecha_max_desem = rs1.getDate(1);	/* Valor 13 */
					fecha_max_pago = rs1.getDate(2);	/* Valor 14 */
				}
				rs1.close(); base.cierraStatement();

				//F084-2006
				cod_cliente=rs.getLong("IN_NUMERO_SIRAC");          /* Valor 8 */
				/* Valor 18 y 19*/
				// INICIA FODA 049-2003
				queri="select ig_codigo_base, cs_recurso as acreditado from comrel_base_operacion "+
						"where ic_epo="+ic_epo+
						" and ic_producto_nafin = 5"+
						" and "+ic_plazo+" between ig_plazo_minimo and ig_plazo_maximo"+
						" and ic_moneda = "+icMoneda+	// Agregado EGB 30/06/2003
						" and cs_tipo_plazo = '"+ tipo_plazo +"' "+	// Agregado FODA 011-2006
						" and ig_tipo_cartera = " + tipoPiso;
				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if(rs1.next()) {
					cod_base_operac=rs1.getInt("IG_CODIGO_BASE");
					acreditado = rs1.getString("ACREDITADO");
					scod_base_operac=rs1.getString(1);
					//F084-2006
					cod_cliente = ("S".equals(acreditado))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");
				}
				if (cod_base_operac!=0) {
					cod_prod_banco = (scod_base_operac.length() > 4)? Integer.parseInt(scod_base_operac.substring(0, 2)): Integer.parseInt(scod_base_operac.substring(0, 1));
				}
				rs1.close();
				base.cierraStatement();
				// TERMINA FODA 049-2003

				/* Valor 20 */
				// FODA 074 - 2003
				if (icMoneda == 1) {
					desa_proveedores=(rs.getString("CG_DESARROLLO_PROV")==null || rs.getString("CG_DESARROLLO_PROV").equals("N"))?"N":"S";
				} else {
					desa_proveedores=(cod_prod_banco==15)?"S":"N";
				}


				/* Llama al API (Store Procedure) */
				try {
					cs = base.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					cs.setNull(15, Types.INTEGER);
					cs.setNull(16, Types.INTEGER);
					cs.setNull(17, Types.INTEGER);
					cs.setNull(20, Types.INTEGER);
					cs.setNull(21, Types.INTEGER);
					if (icMoneda==54) {
						cs.setDouble(22, vtas_exterior);
					} else {
						cs.setNull(22, Types.INTEGER);
					}
					cs.setNull(23, Types.INTEGER);
				} else {
					cs.setLong(15, no_lineafondeo);
					cs.setInt(16, cod_agen);
					cs.setInt(17, cod_subapli);
					cs.setString(20, desa_proveedores);
					cs.setNull(21, Types.INTEGER);
					if (icMoneda==54) {
						cs.setDouble(22, vtas_exterior);
					} else {
						cs.setNull(22, Types.INTEGER);
					}
					cs.setString(23, cod_aprobacion);
				}

				cs.setInt(3, cod_sub_aplicacion);
				cs.setNull(1, Types.INTEGER);
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, cod_agencia);
				cs.setString(4, desc);
				cs.setString(5, aprobado_por);
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.setString(6, cod_funcionario);
				cs.setDouble(7, mto_operac);
				//F084-2006
				cs.setLong(8, cod_cliente);
				cs.setDouble(9, mto_asignado);
				cs.setString(10, cod_tipo_linea);
				cs.setInt(11, frecuencia);
				cs.setInt(12, plazo_proyecto);
				cs.setDate(13, fecha_max_desem,Calendar.getInstance());
				cs.setDate(14, fecha_max_pago,Calendar.getInstance());
				cadena.setLength(0);
				cadena.append("p1:=null;"+"\n"+
				"p2:="+cod_agencia+";"+"\n"+
				"p3:="+cod_sub_aplicacion+";"+"\n"+
				"p4:='"+ desc +"';"+"\n"+
				"p5:="+ aprobado_por +";"+"\n"+
				"p6:='"+ cod_funcionario +"';"+"\n"+
				"p7:="+ mto_operac +";"+"\n"+
				"p8:="+ cod_cliente +";"+"\n"+
				"p9:="+ mto_asignado +";"+"\n"+
				"p10:='"+ cod_tipo_linea +"';"+"\n"+
				"p11:="+ frecuencia +";"+"\n"+
				"p12:="+ plazo_proyecto +";"+"\n"+
				"p13:=to_date('"+fecha_max_desem+"','dd/mm/yyyy');"+"\n"+
				"p14:=to_date('"+fecha_max_pago+"','dd/mm/yyyy');"+"\n");
				if (tipoPiso==1) {
					cadena.append("p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n");
				} else {
					cadena.append("p15:="+ no_lineafondeo +";"+"\n"+
					"p16:="+ cod_agen +";"+"\n"+
					"p17:="+ cod_subapli +";"+"\n");
				}

				cs.setInt(18, cod_prod_banco);
				cs.setInt(19, cod_base_operac);

				cadena.append("p18:="+ cod_prod_banco +";"+"\n"+
				"p19:="+ cod_base_operac +";"+"\n");
				if (tipoPiso==1) {
					cadena.append("p20:=null;"+"\n"+
					"p21:=null;"+"\n");
					if (icMoneda==54){
						cadena.append("p22:="+vtas_exterior+";"+"\n");
					} else {
						cadena.append("p22:=null;"+"\n");
					}
					//"p22:=null;"+"\n"
					cadena.append("p23:=null;"+"\n");
				} else {
					cadena.append("p20:='"+ desa_proveedores +"';"+"\n"+
					"p21:=null;"+"\n");
					if (icMoneda==54){
						cadena.append("p22:="+vtas_exterior+";"+"\n");
					} else {
						cadena.append("p22:=null;"+"\n");
					}

					cadena.append("p23:="+ cod_aprobacion +";"+"\n");
				}

				/* Valor 24 */
				if (tipoPiso==1) {
					cs.setNull(24, Types.INTEGER);
					mto_intermediario = 0;
					mto_cliente = 0;
					cadena.append("p24:=null;"+"\n");
				} else {
					queri="select ag.codigo_agencia_banco "+
						"from mg_agencias_generales ag, com_controlant01 c "+
						"where c.ic_pedido = " + folio + " AND ag.codigo_financiera = c.ig_codigo_financiera ";
					//System.out.println(queri);
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next()) {
						cod_agen_interme=rs1.getInt(1);
						cs.setInt(24, cod_agen_interme);
						cadena.append("p24:="+ cod_agen_interme +";"+"\n");
					} else {
						cs.setNull(24, Types.INTEGER);
						cadena.append("p24:=null;"+"\n");
					}
					rs1.close(); base.cierraStatement();
				}
				cs.setString(25, reestructura);
				cs.setNull(26, Types.INTEGER);
				cs.setString(27, verific_estrato);
				cs.setDouble(28, porcent_asignado);
				cs.setInt(29, cod_sub_apli_det);
				cs.setDouble(30, mto_asignado);
				cs.setDouble(31, porcent_asignado_det);
				cs.registerOutParameter(31, Types.INTEGER);
				cs.setDouble(32, mto_intermediario);
				cs.setDouble(33, mto_cliente);
				cs.setDouble(34, mto_asignado);
				cs.setNull(35, Types.INTEGER);
				cs.registerOutParameter(35, Types.INTEGER);

				cadena.append(
					"p25:="+ reestructura +";"+"\n"+
					"p26:=null;"+"\n"+
					"p27:='"+ verific_estrato +"';"+"\n"+
					"p28:="+ porcent_asignado +";"+"\n"+
					"p29:="+ cod_sub_apli_det +";"+"\n"+
					"p30:="+ mto_asignado +";"+"\n"+
					"p31:="+ porcent_asignado_det +";"+"\n"+
					"p32:="+ mto_intermediario +";"+"\n"+
					"p33:="+ mto_cliente +";"+"\n"+
					"p34:="+ mto_asignado +";"+"\n"+
					"p35:=null;"+"\n");

				System.out.println(cadena.toString());
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						no_line_credit = cs.getInt(1);
						por_asig = cs.getDouble(31);
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						status = cs.getString(35);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena.append(sqle);
					errorDesconocido = true;
					status="9";
				}


				System.out.println("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				cadena.append("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					base.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						// Para la causa 43.
						if(status.equals("43")) {
							if (icMoneda==54) {
								queri = " SELECT fn_valor_compra * "  + mto_operac + " as monto_calc" +
									"	from com_tipo_cambio"  +
									"	where ic_moneda = 54"  +
									"	and dc_fecha = (select max(dc_fecha)"  +
									"                from com_tipo_cambio"  +
									"                where ic_moneda = 54) ";

								try {
									rs1=base.queryDB(queri);
								} catch(SQLException sqle) {
									System.out.println("ProyectosE.java(queri): " + queri );
									throw sqle;
								}
								if(rs1.next()) {
									mto_operac = rs1.getDouble(1);
								}
								rs1.close(); base.cierraStatement();
							}
							queri = "select codigo_aprobacion, cupo_minimo, cupo_limite"+
									" from mg_aprobacion"+
									" where "+mto_operac+" between cupo_minimo and cupo_limite";
							try {
								rs1=base.queryDB(queri);
							} catch(SQLException sqle) {
								System.out.println("Proyectos.java(queri): " + queri );
								throw sqle;
							}
							if(rs1.next()) {
								aprobado_por = rs1.getString(1);
								descripcion = "Descripcion especial Causa 43";
							}
							rs1.close(); base.cierraStatement();
						}

						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							"where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PY'";
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Proyectos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE"; status="6";
						}
						rs1.close(); base.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from inv_interfase where cc_disposicion='"+folio+"'";
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); base.cierraStatement();
					aprobado_por=(aprobado_por==null)?"null":"'"+aprobado_por+"'";
					descripcion=(descripcion==null)?"null":"'"+descripcion+"'";

					if(no_doctos!=0) {
						if (tipoPiso==1) {
							queri = "update inv_interfase set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" , ic_error_proceso="+status+
								" , ig_aprobado_por="+aprobado_por+
								" , cg_descripcion="+descripcion+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 1"+
								" , cs_recurso = '"+acreditado+"'"+
								" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
								" where cc_disposicion= '"+folio+"'";
						} else {
							queri = "update inv_interfase set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" , ic_error_proceso="+status+
								" , ig_aprobado_por="+aprobado_por+
								" , cg_descripcion="+descripcion+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 1"+
								" , cs_recurso = '"+acreditado+"'"+
								" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
								" where cc_disposicion= '"+folio+"'";
						}
					}
					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos de error en inv_interfase.");
						errorTransaccion = true;
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de com_controlAnt01 a com_controlAnt02 */
					queri = "Update inv_interfase " +
							"	set ig_numero_linea_credito = " + no_line_credit +
							"	, ig_codigo_agencia_proy = " + cod_agencia +
							"	, ig_codigo_sub_aplicacion_proy = " + cod_sub_aplicacion +
							"   , df_hora_proceso=SYSDATE"+
							"   , DF_PROYECTOS=SYSDATE"+
							"   , ig_proceso_numero = 1"+
							"   , cs_recurso = '"+acreditado+"'"+
							"   , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
							" WHERE cc_disposicion = '" + folio +"' ";
					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en inv_interfase");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					base.terminaTransaccion(true);
				} else {
					base.terminaTransaccion(false);
				}

			} //while
			if (cs !=null) cs.close();
			rs.close();
			base.cierraStatement();
			System.out.println(" Fin Operaciones 1er Credicadenas");
			/* Fin Interfase 1er Piso Credicadenas */


			/* Inicia la Interfase Financiamiento a Distribuidores 2do Piso. */
			System.out.println(" Inicia la Interfase Financiamiento a Distribuidores 2do Piso.");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("4-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("4-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and py.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}



			// CREDITO EN CUENTA CORRIENTE
			query = " SELECT /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds py e lc i t)*/ " +
					"	   di.ic_documento " +
					"     , di.ig_codigo_agencia " +
					"     , di.ig_codigo_sub_aplicacion " +
					"     , di.ig_numero_linea_fondeo " +
					"     , di.ig_codigo_financiera " +
					"     , di.ig_aprobado_por " +
					"     , di.cg_descripcion " +
					"     , di.fn_monto_operacion " +
					"     , d.ig_plazo_credito as ig_plazo " +
					"     , TO_CHAR(d.df_fecha_venc,'DD/MM/YYYY') " +
					"     , DECODE(i.ig_tipo_piso, 2, py.in_numero_sirac, 1, py.in_numero_sirac ) as in_numero_sirac " +
					"     , null as cg_desarrollo_prov" +
					"     , d.ic_epo " +
					"     , i.ig_tipo_piso " +
					"     , DECODE(lc.ic_tipo_cobro_interes, 1, ds.fn_importe_recibir, 2, ds.fn_importe_recibir) as fn_monto " +
					"     , t.ic_moneda " +
					"     , DECODE(NVL(py.fn_ventas_net_exp,0),0,1,py.fn_ventas_net_exp) fn_ventas_net_exp" +
					"     , i.ig_tipo_piso as tipoPiso " +
					"     , d.ic_moneda "+
					"     , di.fn_monto_operacion as fn_monto_dscto " +
					"     , py.in_numero_sirac as in_numero_sirac_pyme"  +
					"     , e.ig_numero_sirac as in_numero_sirac_epo"  +
					strSelect +
					"	  , 'Proyectos(Distribuidores)' as proceso" +
					"   ,  d.ig_tipo_pago as tipo_pago "+ //Fodea 09-2015

					" FROM dis_solicitud s " +
					"  , dis_interfase di " +
					"  , dis_documento d " +
					"  , dis_docto_seleccionado ds " +
					"  , comcat_pyme py " +
					"  , comcat_epo e " +
					"  , com_linea_credito lc " +
					"  , comcat_if i " +
					"  , comcat_tasa t " +
					"  , comrel_producto_if rpi" +
					strFrom +
					" WHERE s.ic_documento = di.ic_documento " +
					"   AND s.ic_documento = ds.ic_documento  " +
					"   AND s.ic_documento = d.ic_documento  " +
					"   AND d.ic_pyme = py.ic_pyme " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_linea_credito = lc.ic_linea_credito " +
					"   AND lc.ic_if = i.ic_if " +
					"   AND lc.ic_if = rpi.ic_if" +
					"   AND ds.ic_tasa = t.ic_tasa " +
					strCondiciones +
					"   AND di.ig_numero_linea_credito is null " +
					"   AND di.cc_codigo_aplicacion is null " +
					"   AND di.ic_error_proceso is null " +
					"   AND s.ic_estatus_solic = 2 " +
					"   AND s.ic_bloqueo = 3 " +
					"   AND rpi.ic_producto_nafin = 4" +
					" UNION ALL" +
			//	DESCUENTO MERCANTIL
					" SELECT /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds py e lc i t)*/ " +
					"	     di.ic_documento " +
					"      , di.ig_codigo_agencia " +
					"      , di.ig_codigo_sub_aplicacion " +
					"      , di.ig_numero_linea_fondeo " +
					"      , di.ig_codigo_financiera " +
					"      , di.ig_aprobado_por " +
					"      , di.cg_descripcion " +
					"      , di.fn_monto_operacion " +
					"      , d.ig_plazo_credito as ig_plazo " +
					"      , TO_CHAR(d.df_fecha_venc,'DD/MM/YYYY') " +
					"      , DECODE(i.ig_tipo_piso, 2, py.in_numero_sirac, 1, py.in_numero_sirac ) as in_numero_sirac " +
					"      , null as cg_desarrollo_prov" +
					"      , d.ic_epo " +
					"      , i.ig_tipo_piso " +
					"      , DECODE(lc.ic_tipo_cobro_interes, 1, ds.fn_importe_recibir, 2, ds.fn_importe_recibir) as fn_monto " +
					"      , t.ic_moneda " +
					"      , DECODE(NVL(py.fn_ventas_net_exp,0),0,1,py.fn_ventas_net_exp) fn_ventas_net_exp " +
					"      , i.ig_tipo_piso as tipoPiso" +
					"      , d.ic_moneda "+
					"      , di.fn_monto_operacion as fn_monto_dscto " +
					"      , py.in_numero_sirac as in_numero_sirac_pyme"  +
					"      , e.ig_numero_sirac as in_numero_sirac_epo"  +
					strSelect +
					"	   , 'Proyectos(Distribuidores)' as proceso" +
					"     ,  d.ig_tipo_pago as tipo_pago "+ //Fodea 09-2015

					"  FROM dis_solicitud s " +
					"   , dis_interfase di " +
					"   , dis_documento d " +
					"   , dis_docto_seleccionado ds " +
					"   , comcat_pyme py " +
					"   , comcat_epo e " +
					"   , dis_linea_credito_dm lc " +
					"   , comcat_if i " +
					"   , comcat_tasa t " +
					"   , comrel_producto_if rpi" +
					strFrom +
					"  WHERE s.ic_documento = di.ic_documento " +
					"    AND s.ic_documento = ds.ic_documento  " +
					"    AND s.ic_documento = d.ic_documento  " +
					"    AND d.ic_pyme = py.ic_pyme " +
					"    AND d.ic_epo = e.ic_epo " +
					"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					"    AND lc.ic_if = i.ic_if " +
					"    AND lc.ic_if = rpi.ic_if" +
					"    AND ds.ic_tasa = t.ic_tasa " +
					strCondiciones +
					"    AND di.ig_numero_linea_credito is null " +
					"    AND di.cc_codigo_aplicacion is null " +
					"    AND di.ic_error_proceso is null " +
					"    AND s.ic_estatus_solic = 2 " +
					"    AND s.ic_bloqueo = 3 " +
					"    AND rpi.ic_producto_nafin = 4" +
					"  ORDER BY tipoPiso desc ";
			//System.out.println(query+";");
			ic_docto = "";
			conv_proveedores="";
			desa_proveedores="";
			cod_aprobacion="";
			codigo_aplicacion="";
			acreditado = "";
			no_lineafondeo=0;
			cod_agen=0;
			cod_subapli=0;
			cod_finan=0;
			cod_cliente=0;
			ic_plazo=0;
			cod_prod_banco=0;
			ic_epo=0;
			cod_base_operac=0;
			no_line_credit=0;
			dia=0;
			mes=0;
			ano=0;
			no_doctos=0;
			scod_base_operac = null;
			status="";
			mto_operac=0.0;
			mto_asignado=0.0;
			por_asig=0.0;
			plazo_proyecto = 0;
			tipoPiso = 0;
			cod_agencia=90;

			try {
				rs = base.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Proyectos.java(query): " + query );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				errorDesconocido = false;
				icMoneda = rs.getInt("IC_MONEDA");
				acreditado = "S";
				cod_base_operac = 0;
				cod_prod_banco = 0;
				cod_cliente = 0;

				tipoPiso=rs.getInt("TIPOPISO");
				cod_agencia=rs.getInt("IG_AGENCIA_SIRAC");					/* Valor 2 */
				ic_docto=rs.getString("IC_DOCUMENTO");
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");					/* Valor 16 */
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");			/* Valor 17 */
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");			/* Valor 15 */
				aprobado_por=rs.getString("IG_APROBADO_POR");               /* Valor 5 */
				cod_aprobacion=aprobado_por;                				/* Valor 23 */
				descripcion=rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				mto_operac=(tipoPiso==1)?rs.getDouble("FN_MONTO_DSCTO"):rs.getDouble("FN_MONTO_OPERACION");			/* Valor 7 */
				mto_asignado=mto_operac;                    				/* Valor 9, Valor 30 y Valor 34 */
				ic_plazo=rs.getInt("IG_PLAZO");
				plazo_proyecto = (ic_plazo>180)? 180:ic_plazo;				/* Valor 12 */
				ic_epo=rs.getInt("IC_EPO");
				vtas_exterior = rs.getDouble("FN_VENTAS_NET_EXP");

				tipo_pago = rs.getString("tipo_pago")==null?"1":rs.getString("tipo_pago"); //F09-2015

				cod_sub_aplicacion = (icMoneda==54)?202:201;
				cod_sub_apli_det = (icMoneda==54)?221:211;

				/*
				queri = "select fecha_hoy + "+ic_plazo+
					" , sysdate + "+ic_plazo+
					" from mg_calendario "+
					"where codigo_aplicacion = 'BLC'";
				*/
				queri = "select fecha_hoy + "+plazo_proyecto+
					" , trunc(sysdate) + ";
				queri += (ic_plazo>180)? " " + ic_plazo: " "+plazo_proyecto;
				queri +=" from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BLC'";
				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if (rs1.next()){
					fecha_max_desem = rs1.getDate(1);/* Valor 13 */
					fecha_max_pago = rs1.getDate(2);/* Valor 14 */
				}
				rs1.close(); base.cierraStatement();

				//F084-2006
				cod_cliente=rs.getLong("IN_NUMERO_SIRAC");          /* Valor 8 */
				/* Valor 18 y 19*/
				// INICIA FODA 049-2003
				queri="select ig_codigo_base, cs_recurso as acreditado from comrel_base_operacion "+
						"where ic_epo="+ic_epo+
						" and ic_producto_nafin = 4"+
						" and "+ic_plazo+" between ig_plazo_minimo and ig_plazo_maximo"+
						" and ic_moneda = "+icMoneda+	// Agregado EGB 30/06/2003
						" and ig_tipo_cartera = " + tipoPiso +
				      " and ig_tipo_pago = " + tipo_pago;  ////F09-2015


				//System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(queri): " + queri );
					throw sqle;
				}
				if(rs1.next()) {
					cod_base_operac=rs1.getInt("IG_CODIGO_BASE");
					acreditado = rs1.getString("ACREDITADO");
					scod_base_operac=rs1.getString(1);
					//F084-2006
					cod_cliente = ("S".equals(acreditado))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");
				}
				if (cod_base_operac!=0) {
					cod_prod_banco = (scod_base_operac.length() > 4)? Integer.parseInt(scod_base_operac.substring(0, 2)): Integer.parseInt(scod_base_operac.substring(0, 1));
				}
				rs1.close();
				base.cierraStatement();
				// TERMINA FODA 049-2003

				base.cierraStatement();

				/* Valor 20 */

				if (icMoneda == 1) {
					desa_proveedores="N";
				} else {
					desa_proveedores=(cod_prod_banco==1)?"S":"N";
				}

				/* Llama al API (Store Procedure) */
				try {
					cs = base.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Proyectos.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					cod_sub_aplicacion = 113;
					cod_sub_apli_det = cod_sub_aplicacion;
					cs.setInt(3, cod_sub_aplicacion);
					cs.setNull(15, Types.INTEGER);
					cs.setNull(16, Types.INTEGER);
					cs.setNull(17, Types.INTEGER);
					cs.setNull(20, Types.INTEGER);
					cs.setNull(21, Types.INTEGER);
					cs.setNull(22, Types.INTEGER);
					cs.setNull(23, Types.INTEGER);
				} else {
					cs.setInt(3, cod_sub_aplicacion);
					cs.setLong(15, no_lineafondeo);
					cs.setInt(16, cod_agen);
					cs.setInt(17, cod_subapli);
					cs.setString(20, desa_proveedores);
					cs.setNull(21, Types.INTEGER);
					if (icMoneda==54) {
						cs.setDouble(22, vtas_exterior);
					} else {
						cs.setNull(22, Types.INTEGER);
					}
					cs.setString(23, cod_aprobacion);
				}

				cs.setNull(1, Types.INTEGER);
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, cod_agencia);
				cs.setString(4, desc);
				cs.setString(5, aprobado_por);
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.setString(6, cod_funcionario);
				cs.setDouble(7, mto_operac);
				//F084-2006
				cs.setLong(8, cod_cliente);
				cs.setDouble(9, mto_asignado);
				cs.setString(10, cod_tipo_linea);
				cs.setInt(11, frecuencia);
				cs.setInt(12, plazo_proyecto);
				cs.setDate(13, fecha_max_desem,Calendar.getInstance());
				cs.setDate(14, fecha_max_pago,Calendar.getInstance());

				cadena.setLength(0);
				cadena.append("p1:=null;\n"+
						"p2:="+cod_agencia+";\n"+
						"p3:="+cod_sub_aplicacion+";\n"+
						"p4:='"+ desc +"';\n"+
						"p5:="+ aprobado_por +";\n"+
						"p6:='"+ cod_funcionario +"';\n"+
						"p7:="+ mto_operac +";\n"+
						"p8:="+ cod_cliente +";\n"+
						"p9:="+ mto_asignado +";\n"+
						"p10:='"+ cod_tipo_linea +"';\n"+
						"p11:="+ frecuencia +";\n"+
						"p12:="+ plazo_proyecto +";\n"+
						"p13:=to_date('"+fecha_max_desem+"','dd/mm/yyyy');\n"+
						"p14:=to_date('"+fecha_max_pago+"','dd/mm/yyyy');\n");
				if (tipoPiso==1) {
					cadena.append("p15:=null;\n"+
							"p16:=null;\n"+
							"p17:=null;\n");
				} else {
					cadena.append("p15:="+ no_lineafondeo +";\n"+
							"p16:="+ cod_agen +";\n"+
							"p17:="+ cod_subapli +";\n");
				}
				cs.setInt(18, cod_prod_banco);
				cs.setInt(19, cod_base_operac);

				cadena.append("p18:="+ cod_prod_banco +";\n"+
						"p19:="+ cod_base_operac +";\n");
				if (tipoPiso==1) {
					cadena.append("p20:=null;\n"+
					"p21:=null;\n"+
					"p22:=null;\n"+
					"p23:=null;\n");
				} else {
					cadena.append("p20:='"+ desa_proveedores +"';\n"+
							"p21:=null;\n");
					if (icMoneda==54){
						cadena.append("p22:="+vtas_exterior+";\n");
					} else {
						cadena.append("p22:=null;\n");
					}
					cadena.append("p23:="+ cod_aprobacion +";\n");
				}

				/* Valor 24 */
				if (tipoPiso==1) {
					cs.setNull(24, Types.INTEGER);
					mto_intermediario = 0;
					mto_cliente = 0;
					cadena.append("p24:=null;\n");
				} else {
					queri="select ag.codigo_agencia_banco "+
						"from mg_agencias_generales ag, dis_interfase di "+
						"where di.ic_documento = " + ic_docto +
						" and ag.codigo_financiera = di.ig_codigo_financiera ";
					//System.out.println(queri);
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next()) {
						cod_agen_interme=rs1.getInt(1);
						cs.setInt(24, cod_agen_interme);
						cadena.append("p24:="+ cod_agen_interme +";\n");
					} else {
						cs.setNull(24, Types.INTEGER);
						cadena.append("p24:=null;\n");
					}
					rs1.close(); base.cierraStatement();
				}
				cs.setString(25, reestructura);
				cs.setNull(26, Types.INTEGER);
				cs.setString(27, verific_estrato);
				cs.setDouble(28, porcent_asignado);
				cs.setInt(29, cod_sub_apli_det);
				cs.setDouble(30, mto_asignado);
				cs.setDouble(31, porcent_asignado_det);
				cs.registerOutParameter(31, Types.INTEGER);
				cs.setDouble(32, mto_intermediario);
				cs.setDouble(33, mto_cliente);
				cs.setDouble(34, mto_asignado);
				cs.setNull(35, Types.INTEGER);
				cs.registerOutParameter(35, Types.INTEGER);

				cadena.append("p25:="+ reestructura +";\n"+
						"p26:=null;\n"+
						"p27:='"+ verific_estrato +"';\n"+
						"p28:="+ porcent_asignado +";\n"+
						"p29:="+ cod_sub_apli_det +";\n"+
						"p30:="+ mto_asignado +";\n"+
						"p31:="+ porcent_asignado_det +";\n"+
						"p32:="+ mto_intermediario +";\n"+
						"p33:="+ mto_cliente +";\n"+
						"p34:="+ mto_asignado +";\n"+
						"p35:=null;\n");

				System.out.println(cadena.toString());
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						no_line_credit = cs.getInt(1);
						por_asig = cs.getDouble(31);
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						status = cs.getString(35);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena.append(sqle);
					errorDesconocido = true;
					status="9";
				}

				System.out.println("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				cadena.append("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					base.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						// Para la causa 43.
						if(status.equals("43")) {
							if (icMoneda==54) {
								queri = " SELECT fn_valor_compra * "  + mto_operac + " as monto_calc" +
									"	from com_tipo_cambio"  +
									"	where ic_moneda = 54"  +
									"	and dc_fecha = (select max(dc_fecha)"  +
									"                from com_tipo_cambio"  +
									"                where ic_moneda = 54) ";

								try {
									rs1=base.queryDB(queri);
								} catch(SQLException sqle) {
									System.out.println("ProyectosE.java(queri): " + queri );
									throw sqle;
								}
								if(rs1.next()) {
									mto_operac = rs1.getDouble(1);
								}
								rs1.close(); base.cierraStatement();
							}
							queri = "select codigo_aprobacion, cupo_minimo, cupo_limite"+
									" from mg_aprobacion"+
									" where "+mto_operac+" between cupo_minimo and cupo_limite";
							try {
								rs1=base.queryDB(queri);
							} catch(SQLException sqle) {
								System.out.println("Proyectos.java(queri): " + queri );
								throw sqle;
							}
							if(rs1.next()) {
								aprobado_por = rs1.getString(1);
								descripcion = "Descripcion especial Causa 43";
							}
							rs1.close(); base.cierraStatement();
						}

						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
								"where ic_error_proceso="+status+
								" and cc_codigo_aplicacion='PY'";
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Proyectos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE"; status="6";
						}
						rs1.close(); base.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from dis_interfase where ic_documento = "+ic_docto;
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Proyectos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); base.cierraStatement();
					aprobado_por=(aprobado_por==null)?"null":"'"+aprobado_por+"'";
					descripcion=(descripcion==null)?"null":"'"+descripcion+"'";

					if(no_doctos!=0) {
						if (tipoPiso==1) {
							queri = "update dis_interfase set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" , ic_error_proceso="+status+
								" , ig_aprobado_por="+aprobado_por+
								" , cg_descripcion="+descripcion+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 1"+
								" , cs_recurso = '" + acreditado + "'" +
								" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
								" where ic_documento = "+ic_docto;
						} else {
							queri = "update dis_interfase set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" , ic_error_proceso="+status+
								" , ig_aprobado_por="+aprobado_por+
								" , cg_descripcion="+descripcion+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 1"+
								" , cs_recurso = '" + acreditado + "'" +
								" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
								" where ic_documento = "+ic_docto;
						}
					}
					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos de error en inv_interfase.");
						errorTransaccion = true;
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de com_controlAnt01 a com_controlAnt02 */
					queri = " update dis_interfase " +
							" set ig_numero_linea_credito = " + no_line_credit +
							" , ig_codigo_agencia_proy = " + cod_agencia +
							" , ig_codigo_sub_aplicacion_proy = " + cod_sub_aplicacion +
							" , df_hora_proceso=SYSDATE"+
							" , DF_PROYECTOS=SYSDATE"+
							" , ig_proceso_numero = 1"+
							" , cs_recurso = '" + acreditado + "'" +
							" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena.toString(), '\'', "\'")  + "'" +
							" WHERE ic_documento = " + ic_docto;
					//System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en inv_interfase");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					base.terminaTransaccion(true);
				} else {
					base.terminaTransaccion(false);
				}

			} //while
			if (cs !=null) cs.close();
			rs.close();
			base.cierraStatement();
			System.out.println(" Fin de la Interfase Financiamiento a Distribuidores 2do Piso.");
			/* Fin de la Interfase Financiamiento a Distribuidores 2do Piso. */

		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			System.out.println(base.mostrarError());
			base.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (base.hayConexionAbierta()){
				base.cierraConexionDB();
			}
		}
	}
}
