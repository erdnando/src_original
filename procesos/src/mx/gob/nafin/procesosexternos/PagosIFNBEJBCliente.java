package mx.gob.nafin.procesosexternos;

import com.netro.descuento.PagosIFNB;

import netropology.utilerias.ServiceLocator;

public class PagosIFNBEJBCliente {

	public static void main(String [] args){
		int codigoSalida = 0;

		try {
			if (args[0].equals("vencimientos")){
				obtenerDatosSirac(1, args[1]);
			} else if (args[0].equals("operados")){
				obtenerDatosSirac(2, args[1]);
			} else if (args[0].equals("estadoscuenta")){
				obtenerDatosSirac(3, args[1]);
			} else if (args[0].equals("zipvencimientos")){
				obtenerDatosSirac(4, args[1]);
			} else if (args[0].equals("zipoperados")){
				obtenerDatosSirac(5, args[1]);
			} else if (args[0].equals("zipestadoscuenta")){
				obtenerDatosSirac(6, args[1]);
			} else {
				System.out.println("Error: " + args[0] + " no es un argumento valido");
				codigoSalida = 1;
			}
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: no se proporcionaron argumentos");
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
    }

	private static void obtenerDatosSirac(int opc, String ruta) throws Throwable {
		PagosIFNB pagosEJB = obtenerEJB();
		if(opc == 1){
			String mensajeV = pagosEJB.obtenerVencimientosSirac();
			System.out.println("Ejecuto m�todo obtenerVencimientosSirac(). " + mensajeV);
		}
		if(opc == 2){
			String mensajeO = pagosEJB.obtenerOperadosSirac();
			System.out.println("Ejecuto m�todo obtenerOperadosSirac(). " + mensajeO);
		}
		if(opc == 3){
			String mensajeE = pagosEJB.obtenerEstadosDeCuentaSirac();
			System.out.println("Ejecuto m�todo obtenerEstadosDeCuentaSirac(). " + mensajeE);
		}
		if(opc == 4){
			String mensajeZV = pagosEJB.crearArchivosZip(1,ruta);
			System.out.println("Ejecuto m�todo crearArchivosZip(). " + mensajeZV);
		}
		if(opc == 5){
			String mensajeZO = pagosEJB.crearArchivosZip(2,ruta);
			System.out.println("Ejecuto m�todo crearArchivosZip(). " + mensajeZO);
		}
		if(opc == 6){
			String mensajeZE = pagosEJB.crearArchivosZip(3,ruta);
			System.out.println("Ejecuto m�todo crearArchivosZip(). " + mensajeZE);
		}
	}

	private static PagosIFNB obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		PagosIFNB pagosEJB = ServiceLocator.getInstance().remoteLookup("PagosIFNBEJB", PagosIFNB.class);
		return pagosEJB;
	}

}
