package mx.gob.nafin.procesosexternos;

import com.netro.distribuidores.CargaDocDist;

import netropology.utilerias.ServiceLocator;

/**
 * Proceso que liberar� la l�nea de cr�dito de los Distribuidores
 * que cuenten con documentos financiados a meses sin intereses,
 * cuando el pago mensual llegue a su vencimiento.
 * Fodea 09-2015
 */
public class LiberarLineaDist {


	public static void main(String [] args)  {
		int codigoSalida = 0;
		int proceso = 0;
		String ruta = "";

		try{

			try {
				ruta = args[0];

			} catch (ArrayIndexOutOfBoundsException eiobe) {

			}


		   CargaDocDist CargaDocumentos = ServiceLocator.getInstance().remoteLookup("CargaDocDistEJB", CargaDocDist.class);


			CargaDocumentos.setActLineaCredDist();


		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}

	}
}
