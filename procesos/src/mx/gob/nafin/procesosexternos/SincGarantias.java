package mx.gob.nafin.procesosexternos;

import com.netro.garantias.Garantias;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.ServiceLocator;

/**
 * Clase que sincroniza las solicitudes de Garantias entre
 * Nafin Electronico y SIAG.
 * @author Ricardo Fuentes
 * @author Gilberto Aparicio
 *
 */
public class SincGarantias {

	public static void main(String args[]) {

		System.out.println("--------------------------------------------------------------------------------");
		//DEBUG proceso garantias.
		String pid = "";
		int iPID = -1;
		if (args.length > 0 ) {
			pid = "(PID: " + args[0] + ") ";
			iPID = Integer.parseInt(args[0]);
		}
		//-----------------------

		CallableStatement	cs = null;
		PreparedStatement	ps = null;
		ResultSet	rs = null;


		//Las librerias de conexión a base de datos que utilizaban el pool de
		//conexiones del Application Server son remplazadas por conexiones
		//directas jdbc de Oracle.
		AccesoDBOracle con = new AccesoDBOracle();

		String qrySentencia	= "";
		int cicloTrans = 0;
		int horaActual = 0;
		String folios = null;
		boolean ejecuta = true;
		boolean depuraPrimera = true;
		boolean depuraSegunda = true;

		while(ejecuta){
			try {
			  con.conexionDB();
			} catch (Exception e) {
				System.out.println("ERROR al conectarse a la BD");
				e.printStackTrace();
				System.out.println(e.getMessage());
				System.exit(1);
			}

			if(depuraPrimera){
				try{
					System.out.print(pid + "Ejecucion de Primera Actualizacion. ");
					cs = con.ejecutaSP("SP_ACT_CAT_GTI");
					cs.execute();
					System.out.println("Ok.");
				}catch(Throwable e){
					con.terminaTransaccion(false);
					System.out.println(" ERROR. " + e.getMessage() );
				}finally{
					con.terminaTransaccion(true);
				}

				try{
					System.out.print(pid + " Depuracion SIAG - ");
					cs = con.ejecutaSP("SP_DEPURACION_SIAG");
					cs.execute();
					System.out.print("Ok.");
					con.terminaTransaccion(true);


					System.out.print(pid + " Depuracion N@E - ");
					cs = con.ejecutaSP("SP_DEPURACION_GTI");
					cs.execute();
					System.out.println("Ok.");
					con.terminaTransaccion(true);

				}catch(Throwable e){
					con.terminaTransaccion(false);
					System.out.println("ERROR. " +  e.getMessage());
				}
			}


			try {
				// OBTENEMOS EL INTERVALO DE TIEMPO ENTRE CADA CICLO
				qrySentencia =
						" SELECT ig_ciclo_transferencias"   +
						" ,TO_CHAR(SYSDATE,'HH24MI') AS horarioActual"+
						" FROM gti_parametro"   +
						" WHERE ROWNUM = 1"  ;
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				if(rs.next()){
					cicloTrans = rs.getInt("ig_ciclo_transferencias");
					horaActual = rs.getInt("horarioActual");
				}
				rs.close();
				ps.close();

				if(!depuraPrimera && depuraSegunda && horaActual>1500){
					try{
						System.out.print(pid + "Ejecucion de Segunda Actualizacion. ");
						cs = con.ejecutaSP("SP_ACT_CAT_GTI");
						cs.executeQuery();
						System.out.println("Ok.");
					}catch(Throwable e){
						con.terminaTransaccion(false);
						System.out.println("ERROR. " + e.getMessage());
					}finally{
						con.terminaTransaccion(true);
						depuraSegunda = false;
					}
				}
				depuraPrimera = false;

				System.out.print(pid + "Inicia Sincronizacion: " + horaActual);
				folios = null;
				System.out.print(" NE-->SIAG ");
				//SP_SINCRONIZA_FRONT realiza commit o en su caso lanza un error
				//Esto significa que una vez ejecutado exitosamente el store,
				//no podra ser cancelado por un rollback.
				cs = con.ejecutaSP("SP_SINCRONIZA_FRONT(?,?)");
				cs.setString(1, folios);
				cs.setInt(2, iPID);
				cs.registerOutParameter(1, Types.VARCHAR);

				cs.execute();
					folios = cs.getString(1);
				if (cs !=null) cs.close();

				System.out.print("(Ok)");

				if(folios!=null){
					System.out.print("[" + folios + "]");
					System.out.print(" ST=1 NE ");
				}
				//actualizamos las tablas de NE respecto a la informacion de SIAG
				folios = null;
				System.out.print(" SIAG --> NE ");
				//SP_SINCRONIZA_BACK realiza commit o en su caso lanza un error
				//Esto significa que una vez ejecutado exitosamente el store,
				//no podra ser cancelado por un rollback.
				cs = con.ejecutaSP("SP_SINCRONIZA_BACK(?,?)");
				cs.setString(1, folios);
				cs.setInt(2, iPID);
				cs.registerOutParameter(1, Types.VARCHAR);

				cs.execute();
					folios = cs.getString(1);
				if (cs !=null) cs.close();
				System.out.print("(Ok)");

				if(folios!=null){
					System.out.print("[" + folios + "]");
					System.out.println(" ST=1 SIAG ");
				}

				// Fodea 021 - 2010. Enviar Correo con los  Mensajes de Error de los Registros
				System.out.println("\nEjecutando Proceso Notificacion de Transacciones con Errores.");
				Garantias garantias = ServiceLocator.getInstance().remoteLookup("GarantiasEJB", Garantias.class);

				garantias.sendError();
				System.out.println("El Proceso de Notificacion de Transacciones con Errores termino exitosamente.");

				// Fodea 048 - 2010. Enviar Correo con los  Mensajes de Autorización IF de los Registros
				System.out.println("\nEjecutando Proceso Notificacion de Transacciones con Autorización IF.");
				
				garantias.sendAutorizacionIfSIAG();
				System.out.println("El Proceso de Notificacion de Transacciones con Autorización IF termino exitosamente.");

			} catch(Throwable e) {
				System.out.println("ERROR: " + e.getMessage());
				e.printStackTrace(System.out);
				if (con.hayConexionAbierta()){
					con.terminaTransaccion(false);
					con.cierraConexionDB();
				}

				//CUIDADO: El System.exit(1) no permite la ejecución del "finally"
				System.exit(1);
			} finally {
				if (con.hayConexionAbierta()){
					System.out.println("");
					con.terminaTransaccion(true);
					con.cierraConexionDB();
				}
			}
			if(horaActual > 2000){	// si ya paso el horario de ejecucion termina el proceso
				System.out.println(pid + "FIN DE HORARIO DE SINCRONIZACION, SALIENDO DEL PROCESO: " + horaActual);
				System.exit(0);
			}else{	// si no, dormir el ciclo de transferencias
				System.out.println(pid + "En espera: "+cicloTrans+" s.");
				try{Thread.sleep(1000*cicloTrans);}catch(Exception _e){System.out.println(_e.getMessage());}	//1000*60 = 1 segundo
			}
		} // fin del while
	}	//Fin del main();
}//fin de la clase