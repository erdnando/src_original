package mx.gob.nafin.procesosexternos;

import com.netro.garantias.Garantias;
import com.netro.garantias.ResultadosGar;

import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;


public class GarantiaAutomaticaEJBCliente {

	public static void main(String[] args) {
		int codigoSalida = 0;
		try {
			if (args.length == 0) {
				System.out.println("Modo de uso:\njava GarantiaAutomaticaEJBCliente <claveIF>");
				codigoSalida = 1;
				return;
			}
			ResultadosGar resultado = procesoAltaAutomaticaGarantias(args[0]);
			if (resultado != null && resultado.getNumRegErr() >0) {
				System.out.println("Errores encontrados en el proceso de alta automatica de garantias.");
				System.out.println("Registros Correctos: " + resultado.getCorrectos());
				System.out.println("Registros Errores: " + resultado.getErrores());
				System.out.println("Num de Registros Correctos: " + resultado.getNumRegOk());
				System.out.println("Num de Registros Errores: " + resultado.getNumRegErr());
				System.out.println("Sumatoria montos Correctos: " + Comunes.formatoDecimal(resultado.getSumRegOk(),2));
				System.out.println("Sumatoria montos Errores: " + Comunes.formatoDecimal(resultado.getSumRegErr(),2));
				codigoSalida = 1;
				return;
			}
			System.out.println("GarantiaAutomaticaEJBCliente ejecutado con exito.");
			System.out.println("Folio de Alta: " + resultado.getFolio());
			System.out.println("Fecha: " + resultado.getFecha());
			System.out.println("Hora: " + resultado.getHora());
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
	}

	/**
	 * Se realiza la llamada al EJB correspondiente, para realizar el
	 * alta autom�tica de Garantias.
	 * @param claveIF Clave del IF, la cual ser� procesada.
	 * @throws java.lang.Throwable
	 */
	static ResultadosGar procesoAltaAutomaticaGarantias(String claveIF) throws Throwable {
			ResultadosGar resultado = null;
			System.out.println("Inicio Conexion");
			Garantias garantias = ServiceLocator.getInstance().remoteLookup("GarantiasEJB", Garantias.class);
			resultado = garantias.generarGarantiaAutomatica(claveIF,"");
			return resultado;
	} //fin main proceso
}