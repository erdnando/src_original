package mx.gob.nafin.procesosexternos;


import com.netro.descuento.PemexEnlace;

public class PemexEnlCliente {

	public static void main(String args[]) {
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();
		try {
			cargaDoctos.proceso(args[0], new PemexEnlace(args[0]));
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java PemexEnlCliente <IC_EPO>. Donde IC_EPO es la clave de la EPO.");
			aiobe.printStackTrace();
			System.out.println(aiobe.getMessage());
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
	}

}