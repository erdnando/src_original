package mx.gob.nafin.procesosexternos;

import com.netro.procesos.ManejoServicio;

import java.util.Vector;

import netropology.utilerias.ServiceLocator;

public class ManejoServicioEJBCliente {

	public static void main(String [] args)  {
		int iEstatusProceso = 0;
		try {
		   abrirServicio();
		} catch(Throwable ex){
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			iEstatusProceso = -1;
		} finally {
			System.out.println(iEstatusProceso);
			System.exit(iEstatusProceso);
		}
   }

	private static void abrirServicio() throws Throwable  {
		ManejoServicio servicioS = obtenerEJB();
		Vector ovProcesos = new Vector();
		boolean verificarTasa = true;

		ovProcesos.add("SP_ACT_MONITOR_INTER");
		ovProcesos.add("spcom_neg_venc");
		ovProcesos.add("sp_liberaciones_venc");

		if (!servicioS.abrirServicio(ovProcesos, verificarTasa)) {
			System.out.println("Ejecuto metodo abrirServicio()");
			System.out.println(-2);
			System.exit(-2);
		}
		System.out.println("Ejecuto metodo abrirServicio()");
	}

	private static ManejoServicio obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		ManejoServicio servicioS = ServiceLocator.getInstance().remoteLookup("ManejoServicioEJB", ManejoServicio.class);
		return servicioS;
	}

}
