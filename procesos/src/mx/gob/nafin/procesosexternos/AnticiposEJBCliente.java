package mx.gob.nafin.procesosexternos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Registros;

public class AnticiposEJBCliente {

	public static void main(String [] args)  {
	int codigoSalida = 0;
		try {
			obtenerInformacionProceso();
		} catch(Throwable ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			codigoSalida = 1;
		}finally{
		System.exit(codigoSalida);
		}
    }

	/**
	 * este metodo es para insertar la informacion de las graficas
	 *  alas  tablas com_graficas_cache_det  y com_graficas_cache
	 *  NOTA: Este m�todo no deber�a de ir een esta clase. o mas bien la clase deber�a llamarse diferente
	 *
	 */
	private static void obtenerInformacionProceso() throws Throwable {
		System.out.println("obtenerInformacionProceso(E)");
		AccesoDBOracle con = new AccesoDBOracle();
		boolean bOk = true;
		String  iClaveIf = "";
		String iClaveUni = "";
		List params = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String mensaje = "",  estatusProc = "", strSQL = "", strSQLExis ="", existe ="";

		try {
			con.conexionDB();

			strSQL = " DELETE FROM com_graficas_cache_det " +
						" WHERE ic_tipo_afiliado = ? ";
			params = new ArrayList();
			params.add("I");
			con.ejecutaUpdateDB(strSQL, params);

			strSQL =
				" DELETE FROM  com_graficas_cache " +
				" WHERE ic_tipo_afiliado = ? ";
			params = new ArrayList();
			params.add("I");
			con.ejecutaUpdateDB(strSQL, params);

			con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion

						// para Verificarsi existe IF en la tabla de com_graficas_cache
			strSQLExis = " select ic_afiliado  from  com_graficas_cache "+
						    " where  ic_tipo_afiliado = ? "+
							 " and ic_afiliado =? ";


			/************************************************************
			***************** Operaci�n mensual*******************
			***************************************************************/
			strSQL = " SELECT   /*+ index (s IN_COM_SOLICITUD_13_NUK) */ "+
						" d.ic_if as IC_AFILIADO,  "+
						" TO_CHAR (df_fecha_solicitud, 'mm') as CG_ATRIBUTO, "+
						"	DECODE(d.ic_moneda, 1, 'OPERACIONMN',54, 'OPERACIONDL') AS CG_GRAFICA,  "+
						"	SUM (d.fn_monto_dscto)AS CG_VALOR  "+
						" FROM com_solicitud s, com_documento d   "+
						"	WHERE s.ic_documento = d.ic_documento  "+
						" AND s.df_fecha_solicitud >=    TO_DATE ('01/01/' || TO_CHAR (SYSDATE, 'yyyy'), 'dd/mm/yyyy')  "+
						"	AND s.df_fecha_solicitud < TRUNC (SYSDATE + 1)       "+
						" GROUP BY d.ic_moneda,  "+
						"	d.ic_if,  "+
						" TO_CHAR (df_fecha_solicitud, 'mm'),  "+
						"	TRUNC (SYSDATE)  "+
						" ORDER BY 1, d.ic_if ";

			params = new ArrayList();
			Registros regO = con.consultarDB(strSQL, params);

			String strInsert =
				" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
				" ic_afiliado, ic_tipo_afiliado, " +
				" cg_grafica, cg_atributo, cg_valor, cg_color) " +
				" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
			ps = con.queryPrecompilado(strInsert);

			while(regO.next()) {

				iClaveIf = regO.getString("IC_AFILIADO");

				params = new ArrayList();
				params.add("I");
				params.add(iClaveIf);
				Registros regExis = con.consultarDB(strSQLExis, params);
				existe ="";
				if(regExis.next()) {
					existe=regExis.getString("IC_AFILIADO");
				}
				if (existe.equals("")) {
					strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
					params = new ArrayList();
					params.add(iClaveIf);
					params.add("I");
					params.add("OK");	//En Proceso
					con.ejecutaUpdateDB(strSQL, params);
					con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
				}

				ps.clearParameters();
				ps.setString(1, regO.getString("IC_AFILIADO"));
				ps.setString(2, "I");
				ps.setString(3, regO.getString("CG_GRAFICA"));
				ps.setString(4, regO.getString("CG_ATRIBUTO"));
				ps.setString(5, regO.getString("CG_VALOR"));
				ps.setString(6, "");
				ps.executeUpdate();

			}
			ps.close();

				/****************************************************************************
				***************** Vencimientos en el mes  para  DESCUENTO *******************
				*****************************************************************************/

				strSQL = " SELECT   /*+ index (d IN_COM_DOCUMENTO_09_NUK) */ "+
							" TO_CHAR ( df_fecha_venc, 'dd')as CG_ATRIBUTO," +
							" d.ic_if as IC_AFILIADO, "+
							" DECODE(d.ic_moneda, 1, 'VENCMESMN',54, 'VENCMESDL') AS CG_GRAFICA,   "+
							" SUM (d.fn_monto_dscto) AS  CG_VALOR  "+
							" FROM com_solicitud s, com_documento d "+
							" WHERE s.ic_documento = d.ic_documento "+
							"	AND d.df_fecha_venc >=  TO_DATE ('01/' || TO_CHAR (SYSDATE, 'mm/yyyy'), 'dd/mm/yyyy')"+
							"	AND d.df_fecha_venc < last_day(trunc(SYSDATE) )+ 1  "+
							"	GROUP BY d.ic_moneda, "+
							"	d.ic_if,"+
							" df_fecha_venc "+
							"	ORDER BY 1, d.ic_if  ";

				params = new ArrayList();
				Registros regv = con.consultarDB(strSQL, params);

				String strInsertV =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertV);
				while(regv.next()) {

					iClaveIf = regv.getString("IC_AFILIADO");

					params = new ArrayList();
					params.add("I");
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);
						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}

					ps.clearParameters();
					ps.setString(1, regv.getString("IC_AFILIADO"));
					ps.setString(2, "I");
					ps.setString(3, regv.getString("CG_GRAFICA"));
					ps.setString(4, regv.getString("CG_ATRIBUTO"));
					ps.setString(5, regv.getString("CG_VALOR"));
					ps.setString(6, "");
					ps.executeUpdate();
				}
				ps.close();



				/****************************************************************************
				**************** Vencimientos en el mes  para  CREDITO ELECTRONICO ********
				*****************************************************************************/

				strSQL = " SELECT   /*+ index (s IN_COM_SOLIC_PORTAL_06_NUK) */  " +
							" TO_CHAR ( DF_V_DESCUENTO, 'dd')as CG_ATRIBUTO, " +
							"	ic_if AS IC_AFILIADO, ic_moneda,    " +
							"	DECODE(ic_moneda, 1, 'VENCMESMN_CE',54, 'VENCMESDL_CE') AS CG_GRAFICA, " +
							"	SUM (FN_IMPORTE_DSCTO)  AS CG_VALOR  " +
							" FROM com_solic_portal s  " +
							"	WHERE DF_V_DESCUENTO >=  TO_DATE ('01/' || TO_CHAR (SYSDATE, 'mm/yyyy'), 'dd/mm/yyyy') " +
							"	AND DF_V_DESCUENTO < last_day(trunc(SYSDATE) )+ 1  " +
							"	GROUP BY ic_moneda, " +
							"	ic_if,  " +
							"	DF_V_DESCUENTO  " +
							"	ORDER BY 1, ic_if " ;

				params = new ArrayList();
				Registros regvCE = con.consultarDB(strSQL, params);

				String strInsertVCE =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertVCE);

				while(regvCE.next()) {

					iClaveIf = regvCE.getString("IC_AFILIADO");

					params = new ArrayList();
					params.add("I");
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);
						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}

					ps.clearParameters();
					ps.setString(1, regvCE.getString("IC_AFILIADO"));
					ps.setString(2, "I");
					ps.setString(3, regvCE.getString("CG_GRAFICA"));
					ps.setString(4, regvCE.getString("CG_ATRIBUTO"));
					ps.setString(5, regvCE.getString("CG_VALOR"));
					ps.setString(6, "");
					ps.executeUpdate();
				}
				ps.close();

				/******************************************************************************************
				***************** FODEA024 -- Registros por Mes Credito Educativo ----> IF <----*******************
				*******************************************************************************************/

				strSQL = " SELECT    i.ic_if  as IC_AFILIADO, " +
							" 	TO_char(a.df_hr_validacion,'MM/YYYY') as MesAnio, " +
							" 	u.ic_universidad as NoUniv, " +
							" 	u.cg_razon_social as universidad, " +
							"	u.cg_nombre_campus as campus, " +
							" 	a.ic_estatus_cred_educa as estatus, " +
							"	g.cd_descripcion as grupo, " +
							" 	COUNT(*) totalAlumnos, " +
							" 	'Mes_PefilIF'as tipoGrafica " +
							" FROM CE_CONTENIDO_EDUCATIVO a, " +
							" 	COMCAT_UNIVERSIDAD u, " +
							"  CEREL_IF_UNIVERSIDAD r, " +
							"  comcat_if i, " +
							"	cecat_grupo g " +
							" WHERE a.ic_universidad = u.ic_universidad " +
							"  AND  r.ic_universidad  = u.ic_universidad " +
							"  AND  r.ic_if = i.ic_if " +
							"  AND  a.ic_if = i.ic_if " +
							"	AND  u.ic_grupo = g.ic_grupo " +
							"  AND  a.ic_estatus_cred_educa in(2,3) " +
							" GROUP BY i.ic_if, TO_char(a.df_hr_validacion,'MM/YYYY') , " +
							"  u.ic_universidad, u.cg_razon_social ,a.ic_estatus_cred_educa, " +
							"	u.cg_nombre_campus, g.cd_descripcion" +
							" ORDER BY u.ic_universidad ";


 				params = new ArrayList();
				Registros regced = con.consultarDB(strSQL, params);

				String strInsertCED =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_valorce, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertCED);

				// ----------------------------AQUI----------------------------------//
				String noUni_IF ="";
				String noIf_IF ="";
				String noEstatus_IF ="";
				String campus_IF = "";
				String grupo_IF = "";
				String noAceptados_IF ="0";
				String noRechazados_IF ="0";

				boolean repetido_IF = false;
				String noUni_IF_Ant ="";
				String noIf_IF_Ant ="";
				String noEstatus_IF_Ant ="";
				String campus_IF_Ant = "";
				String grupo_IF_Ant = "";
				String noAceptados_IF_Ant ="0";
				String noRechazados_IF_Ant ="0";

				int tama�o_IF = regced.getNumeroRegistros();
				int i_IF=0;

				while(regced.next()) {

					iClaveIf = regced.getString("IC_AFILIADO");

					params = new ArrayList();
					params.add("I");
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);

						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}


					i_IF++;
					noUni_IF 		= regced.getString("NOUNIV");
					noIf_IF  		= regced.getString("ic_afiliado");
					grupo_IF			= regced.getString("grupo");
					campus_IF		= regced.getString("campus");
					noEstatus_IF	= regced.getString("estatus");
					if (noEstatus_IF.equals("2"))			noAceptados_IF = regced.getString("totalAlumnos");
					else if (noEstatus_IF.equals("3"))	noRechazados_IF = regced.getString("totalAlumnos");

					if(i_IF == tama�o_IF ) {// sino hay otro registro--->inserto registros actuales
						ps.clearParameters();
						ps.setString(1, noIf_IF);
						ps.setString(2, "I");
						ps.setString(3, "PerfilIF"+regced.getString("MESANIO"));													//CG_GRAFICA
						ps.setString(4, grupo_IF +" - " +campus_IF);																	//CG_ATRIBUTO
						ps.setString(5, noAceptados_IF);		   																									//CG_VALOR
						ps.setString(6, noRechazados_IF);		   																								//CG_VALORCE
						ps.setString(7, "");
						ps.executeUpdate();
					}
					else {

						if(!repetido_IF) {
							noUni_IF_Ant 			= noUni_IF;
							noIf_IF_Ant 			= noIf_IF;
							grupo_IF_Ant 			= grupo_IF;
							campus_IF_Ant 			= campus_IF;
							noAceptados_IF_Ant	= noAceptados_IF;
							noRechazados_IF_Ant	= noRechazados_IF;
							repetido_IF = true;
						}
						else {
							if(noUni_IF_Ant.equals(noUni_IF) && noIf_IF_Ant.equals(noIf_IF)) { // se trata del mismo registro, do something
								ps.clearParameters();
								ps.setString(1, noIf_IF);
								ps.setString(2, "I");
								ps.setString(3, "PerfilIF"+regced.getString("MESANIO"));													//CG_GRAFICA
								ps.setString(4, grupo_IF +" - " +campus_IF);																	//CG_ATRIBUTO
								ps.setString(5, noAceptados_IF);		   																									//CG_VALOR
								ps.setString(6, noRechazados_IF);		   																								//CG_VALORCE
								ps.setString(7, "");
								ps.executeUpdate();

								noUni_IF_Ant 			= "";
								noIf_IF_Ant 			= "";
								grupo_IF_Ant 			= "";
								campus_IF_Ant 			= "";
								noAceptados_IF_Ant	= "0";
								noRechazados_IF_Ant	= "0";
								noAceptados_IF			= "0";
								noRechazados_IF		= "0";
								repetido_IF = false;
							}
							else { //sino es el mismo registro, inserta la informacion del registro anterior
								ps.clearParameters();
								ps.setString(1, noIf_IF_Ant);
								ps.setString(2, "I");
								ps.setString(3, "PerfilIF"+regced.getString("MESANIO"));													//CG_GRAFICA
								ps.setString(4, grupo_IF_Ant +" - " +campus_IF_Ant);																	//CG_ATRIBUTO
								ps.setString(5, noAceptados_IF_Ant);		   																									//CG_VALOR
								ps.setString(6, noRechazados_IF_Ant);		   																								//CG_VALORCE
								ps.setString(7, "");
								ps.executeUpdate();

								noUni_IF_Ant 			= noUni_IF;
								noIf_IF_Ant 			= noIf_IF;
								grupo_IF_Ant 			= grupo_IF;
								campus_IF_Ant 			= campus_IF;
								noAceptados_IF_Ant	= noAceptados_IF;
								noRechazados_IF_Ant	= noRechazados_IF;
							}
						}
					}
				//////////////////////////////////////////////////////////////////////
				}
				ps.close();

				/*******************************************************************************************
				***************** FODEA024 -- Grafica N�m. de Acreditados ----> IF <---- *******************
				*******************************************************************************************/

				strSQL = " SELECT  i.ic_if  as IC_AFILIADO, "+
							"	u.ic_universidad as NoUniv, "+
							"	u.cg_razon_social as UNIVERSIDAD," +
							"	g.cd_descripcion as GRUPO, " +
							"	u.cg_nombre_campus AS CAMPUS, "+
							"	sum(a.fn_financiamiento) CG_VALOR,"+
							"	count(*) totalRegistros, " +
							"	'Acre_PefilIF'as CG_GRAFICA "+
							" FROM CE_CONTENIDO_EDUCATIVO a, "+
							"	COMCAT_UNIVERSIDAD u, "+
							"	CEREL_IF_UNIVERSIDAD r, "+
							"	comcat_if i, "+
							"	cecat_grupo g " +
							" WHERE a.ic_universidad = u.ic_universidad "+
							"	AND  r.ic_universidad  = u.ic_universidad " +
							"	AND  r.ic_if = i.ic_if "+
							"	AND  a.ic_if = i.ic_if "+
							"	AND  u.ic_grupo = g.ic_grupo " +
							"	AND a.ic_estatus_cred_educa =2"+
							" GROUP BY i.ic_if,  u.ic_universidad, u.cg_razon_social,u.cg_nombre_campus, g.cd_descripcion   ";


 				params = new ArrayList();
				Registros regAcre = con.consultarDB(strSQL, params);

				String strInsertACRE =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertACRE);
				String atributo = "";
				while(regAcre.next()) {

					iClaveIf = regAcre.getString("IC_AFILIADO");

					params = new ArrayList();
					params.add("I");
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);

						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}
					atributo = regAcre.getString("GRUPO") + " - " + regAcre.getString("CAMPUS") + " / " + regAcre.getString("TOTALREGISTROS") +" ACREDITADOS" ;
					ps.clearParameters();
					ps.setString(1, regAcre.getString("IC_AFILIADO"));
					ps.setString(2, "I");
					ps.setString(3, regAcre.getString("CG_GRAFICA"));
					ps.setString(4, atributo);
					//ps.setFloat (5, Float.parseFloat(regAcre.getString("CG_VALOR")));
					ps.setString(5, regAcre.getString("CG_VALOR"));
					ps.setString(6, "");
					ps.executeUpdate();

				}
				ps.close();

				//Borra info de las tablas
				strSQL = " DELETE FROM com_graficas_cache_det " +
						" WHERE ic_tipo_afiliado = ? ";
				params = new ArrayList();
				params.add("U");
				con.ejecutaUpdateDB(strSQL, params);

				strSQL =
					" DELETE FROM  com_graficas_cache " +
					" WHERE ic_tipo_afiliado = ? ";
				params = new ArrayList();
				params.add("U");
				con.ejecutaUpdateDB(strSQL, params);

				con.terminaTransaccion(true);
				/****************************************************************************
				********* FODEA024 -- Registros por Mes -----> Universidad <-----  **********
				*****************************************************************************/

				strSQL = " SELECT i.ic_if as ic_if, " +
									" 	u.ic_universidad  as ic_universidad, " +
									" 	i.cg_razon_social as nombreIF, "+
									"		a.ic_estatus_cred_educa as estatus, "+
									"   TO_char(a.df_hr_validacion,'MM/YYYY') as MesAnio, "+
									"   COUNT(*) totalAlumnos,'Mes_PefilUNI'as tipoGrafica "+
									" FROM CE_CONTENIDO_EDUCATIVO a, "+
									"		COMCAT_UNIVERSIDAD u, "+
									"		CEREL_IF_UNIVERSIDAD r, "+
									"		comcat_if i " +
									" WHERE a.ic_universidad = u.ic_universidad " +
									"		AND  r.ic_universidad  = u.ic_universidad " +
									"		AND  r.ic_if = i.ic_if "+
									"  	AND  a.ic_if = i.ic_if " +
									"		AND a.ic_estatus_cred_educa in(2,3) "+
									" GROUP BY u.ic_universidad , TO_char(a.df_hr_validacion,'MM/YYYY'), a.ic_estatus_cred_educa, i.ic_if, i.cg_razon_social " +
									" ORDER BY  TO_char(a.df_hr_validacion,'MM/YYYY'), i.ic_if, u.ic_universidad ";

 				params = new ArrayList();
				Registros regmesU = con.consultarDB(strSQL, params);

				String strInsertMesU =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_valorce, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertMesU);

				// --------------------------------------------------------------//
				String noUni ="";
				String noIf ="";
				String nombreIF ="";
				String noEstatus ="";
				String noAceptados ="0";
				String noRechazados ="0";
				boolean repetido = false;
				String noUni_Ant ="";
				String noIf_Ant ="";
				String nombreIF_Ant ="";
				String noEstatus_Ant ="";
				String noAceptados_Ant ="0";
				String noRechazados_Ant ="0";
				int tama�o = regmesU.getNumeroRegistros();
				int i=0;

				while(regmesU.next()) {

					iClaveUni = regmesU.getString("ic_universidad");

					params = new ArrayList();
					params.add("U");
					params.add(iClaveUni);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveUni);
						params.add("U");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);

						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}

					//correcion 15.03.13
					i++;
					noUni 	= regmesU.getString("ic_universidad");
					noIf  	= regmesU.getString("ic_if");
					nombreIF = regmesU.getString("nombreIF");
					noEstatus= regmesU.getString("estatus");
					if (noEstatus.equals("2"))			noAceptados = regmesU.getString("totalAlumnos");
					else if (noEstatus.equals("3"))	noRechazados = regmesU.getString("totalAlumnos");

					if(i == tama�o ) {// sino hay otro registro--->inserto
						ps.clearParameters();
						ps.setString(1, noUni);
						ps.setString(2, "U");
						ps.setString(3, "PerfilUN"+regmesU.getString("MESANIO"));													//CG_GRAFICA
						ps.setString(4, nombreIF);																								//CG_ATRIBUTO
						ps.setString(5, noAceptados);		   																				//CG_VALOR
						ps.setString(6, noRechazados);		   																			//CG_VALORCE
						ps.setString(7, "");
						ps.executeUpdate();
					}
					else {

						if(!repetido) {
							noUni_Ant	 		= noUni;
							noIf_Ant		 		= noIf;
							nombreIF_Ant		= nombreIF;
							noEstatus_Ant     = noEstatus;
							noAceptados_Ant	= noAceptados;
							noRechazados_Ant 	= noRechazados;
							repetido = true;
						}
						else {
							if(noUni_Ant.equals(noUni) && noIf_Ant.equals(noIf)) { // se trata del mismo registro, clava los valores actuales y limpia registros anteriores
								ps.clearParameters();
								ps.setString(1, noUni);
								ps.setString(2, "U");
								ps.setString(3, "PerfilUN"+regmesU.getString("MESANIO"));													//CG_GRAFICA
								ps.setString(4, nombreIF);																								//CG_ATRIBUTO
								ps.setString(5, noAceptados);		   																				//CG_VALOR
								ps.setString(6, noRechazados);		   																			//CG_VALORCE
								ps.setString(7, "");
								ps.executeUpdate();
								noUni_Ant ="";
								noIf_Ant ="";
								nombreIF_Ant ="";
								noEstatus_Ant ="";
								noAceptados_Ant ="0";
								noRechazados_Ant ="0";
								noAceptados  = "0";
								noRechazados = "0";
								repetido = false;

							}
							else { //sino es el mismo registro, inserta la informacion del registro anterior
								ps.clearParameters();
								ps.setString(1, noUni_Ant);
								ps.setString(2, "U");
								ps.setString(3, "PerfilUN"+regmesU.getString("MESANIO"));													//CG_GRAFICA
								ps.setString(4, nombreIF_Ant);																						//CG_ATRIBUTO
								ps.setString(5, noAceptados_Ant);		   																		//CG_VALOR
								ps.setString(6, noRechazados_Ant);		   																		//CG_VALORCE
								ps.setString(7, "");
								ps.executeUpdate();


								noUni_Ant	 		= noUni;
								noIf_Ant		 		= noIf;
								nombreIF_Ant		= nombreIF;
								noEstatus_Ant     = noEstatus;
								noAceptados_Ant	= noAceptados;
								noRechazados_Ant 	= noRechazados;
							}
						}
					}

				}
				ps.close();

				/**************************************************************************** *************************
				***************** FODEA024 -- Grafica N�m. de Acreditados -----> Universidad <----- *******************
				*****************************************************************************/

				strSQL = 	" SELECT u.ic_universidad as ic_universidad, " +
									"		i.ic_if  as IC_AFILIADO, "+
									"		i.cg_razon_social as nombreIF,"+
									"		sum(a.fn_financiamiento) CG_VALOR, " +
									"		count(*) totalRegistros,  "+
									"		'Acre_PefilUNI'as CG_GRAFICA " +
									"	FROM CE_CONTENIDO_EDUCATIVO a, "+
									"		COMCAT_UNIVERSIDAD u, " +
									"		CEREL_IF_UNIVERSIDAD r, " +
									"		comcat_if i " +
									"	WHERE a.ic_universidad = u.ic_universidad " +
									"		AND  r.ic_universidad  = u.ic_universidad " +
									"		AND  r.ic_if = i.ic_if "+
									"		AND  a.ic_if = i.ic_if " +
									"		AND a.ic_estatus_cred_educa =2 "+
									" GROUP BY i.ic_if,  u.ic_universidad, i.cg_razon_social ";

 				params = new ArrayList();
				Registros regAcreUNI = con.consultarDB(strSQL, params);

				String strInsertACREuni =
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertACREuni);
				String atributoU = "";
				while(regAcreUNI.next()) {

					iClaveUni = regAcreUNI.getString("ic_universidad");

					params = new ArrayList();
					params.add("U");
					params.add(iClaveUni);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";
						params = new ArrayList();
						params.add(iClaveUni);
						params.add("U");
						params.add("OK");	//En Proceso
						con.ejecutaUpdateDB(strSQL, params);

						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}
					atributoU = regAcreUNI.getString("nombreIF") + " / " + regAcreUNI.getString("TOTALREGISTROS") +" ACREDITADOS" ;
					String valor = regAcreUNI.getString("CG_VALOR");//

					ps.clearParameters();
					ps.setString(1, regAcreUNI.getString("ic_universidad"));
					ps.setString(2, "U");
					ps.setString(3, regAcreUNI.getString("CG_GRAFICA"));
					ps.setString(4, atributoU);
					//ps.setFloat(5, Float.parseFloat(regAcreUNI.getString("CG_VALOR")));
					ps.setString(5, valor);
					ps.setString(6, "");
					ps.executeUpdate();

				}
				ps.close();
				bOk = true;
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					System.out.println("Error al cerrar recursos"+e);
				}
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("obtenerInformacionProceso(s)");
		}
	}

}