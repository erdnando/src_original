package mx.gob.nafin.procesosexternos;


import com.netro.descuento.CargaDocumento;
import com.netro.descuento.WMEnlOperados;

import netropology.utilerias.ServiceLocator;

public class WMEnlOperCliente {

	public static void main(String args[]) {
		int codigoSalida = 0;

		try {
			StringBuffer log = new StringBuffer();
			System.out.println("Inicio Conexion");
			CargaDocumento cargaDoctoS = ServiceLocator.getInstance().remoteLookup("CargaDocumentoEJB", CargaDocumento.class);
			log = cargaDoctoS.procesoDocumentos(args[0], new WMEnlOperados(args[0]));
			System.out.println(log.toString());
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Ejem. Debe ejecutarlo --> java WMEnlCliente 7. El 7 es el n�mero de la EPO para WalMart.");
			codigoSalida = 1;
		} catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
	}

}