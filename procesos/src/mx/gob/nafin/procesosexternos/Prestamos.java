package mx.gob.nafin.procesosexternos;

/*******************************************************************************************************
* Autor Modificaci�n:	Edgar Gonz�lez Bautista									Fecha Ult. Modificaci�n:
* Descripci�n: Implementaci�n de Financiamiento a Inventarios 1er Piso Foda 075		09/Sept/2003
* Descripci�n: Interfaz N@E - Troya									   Foda 099		13/Nov/2003
********************************************************************************************************
* Autor Modificacion:	L.I. Hugo D�az Garc�a									Fecha de Modificaci�n
* Descripci�n: Implementaci�n de Financiamiento a Distribuidores 2o Piso			01/Oct/2003
* Descripci�n: Modificaciones al parametro 16, 21 y 36. 				Foda 020	08/Mar/2005
*******************************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	06/Septiembre/2005
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 058 - 2005, Distribuidores D�lares
*
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	26/Abril/2006
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 011 - 2006, Credicadenas de Exportacion
*
*
*************************************************************************************/

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import java.util.HashMap;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;

public class Prestamos {

	public static void main(String args[]) {
		ejecutaPrestamos(args[0]);
	}

	public static void ejecutaPrestamos(String usuario){
		String query="", queri="", cod_tipo_amort="", codigo_aplicacion="", folio="", folioSD="", aprobado_por="", estatus="", descripcion="";
		String cadena = "", status = "";
		int no_doctos=0, cod_agen=0, cod_subapli=0, cod_finan=0,  cod_agencia_proy=0, cod_sub_apli_proy=0;
		long no_lineafondeo=0,no_linea_credito=0;
		int no_contrato=0, no_prestamo=0, dia_pago_interes=0, dia_pago_capital=0, dia_revision_tasa=0, ic_plazo=0;
		long no_electronico=0;
		double valor_inicial=0.0, mto_operac=0.0;
		java.sql.Date fecha_ppi=null, fecha_ppc=null, fecha_venc=null, fecha_revi_int=null, fecha_apertura=null, fecha_adicion=null;
		int valor_tasa_cartera;						// Valor 2) Codigo_valor_tasa_cartera
		String calcular_mora="S";					// Valor 6) Calcular Mora
		String intereses_vencidos="S";				// Valor 9) Intereses Vencidos
		String intereses_normales="S";				// Valor 10) Intereses Normales
		int frecuencia_interes=1;					// Valor 11) Frecuencia Interes
		int frecuencia_capital=1;					// Valor 12) Frecuencia Capital
		String tipo_tasa=null;						// Valor 13) Tipo Tasa
		String clase_tasa="A";						// Valor 14) Clase Tasa
		String periodo_Tasa;						// Valor 15) Periodo Tasa
		String spread_Mora;							// Valor 16) Spreat Mora
		double spread_corriente_1=0.0;				// Valor 17) Spread_Corriente_1
		double spread_corriente_2=0.0;				// Valor 18) Spread_Corriente_2
		String rela_mate_1=null;					// Valor 19) Relacion_Matematica_1
		String rela_mate_2=null;					// Valor 20) Relacion_Matematica_2
		String rela_mate_mora=null;					// Valor 21) Relacion_Matematica_Mora
		String observaciones="Ingresado por Nafin Electr�nico";	// Valor 22) Observaciones
		double comision=0.0;						// Valor 26) Comisi�n
		String diciembre="0";						// Valor 27) Diciembre
		int dias_gracia_mora=0;						// Valor 28) Dias_Gracia_Mora
		String tipo_liqui_mes="1";					// Valor 29) Tipo_Liquidacion_Mes
		String tipo_liqui_ano="2";					// Valor 30) Tipo_Liquidacion_Ano
		String adicionado_por=usuario;				// Valor 31) Adicionado_Por
		String cobrar_iva="N";						// Valor 35) Cobrar_Iva
		String codigo_Valor_Tasa_Mora;				// Valor 36) Codigo_Valor_Tasa_Mora
		String tipo_frecuencia_revision=null;			// Valor 37) Tipo_Frecuencia_Revision
		String rela_mate_3=null;					// Valor 38) Relacion_Matematica_3
		double spread_corriente_3=0.0;				// Valor 39) Spread_Corriente_3
		String tipo_redondeo="0";					// Valor 43) Tipo_Redondeo
		String tipo_calculo_mora="1";				// Valor 44) Tipo_Calculo_Mora
		int rec_frescos=0;							// Valor 45) Recursos_Frescos
		String amort_ult_prim=null;					// Valor 46) Amortizacion_Ultima_Primera
		String forma_redondeo="0";					// Valor 48) Forma_Redondeo
		String desgloce_tasas=null;					// Valor 49) Desgloce_Tasas
		String cobro_int_ES="1";					// Valor 50) Cobro_Int_Entrada_Salida
		String clausula_venci="3";					// Valor 52) Clausula_Vencimiento
		String tipo_interes="";						// Valor 53) Tipo_Interes
		int periodo_capitalizacion;					// Valor 54) Periodo_Capitalizacion
		String valor_presente="N";					// Valor 55) Valor_Presente
		String pago_masivo="S";						// Valor 58) Pago_Masivo
		String tipo_renta=null;						// Valor 59) Tipo_Renta
		int no_cuotas_recortadas=0;					// Valor 60) Numero_Cuotas_Recortadas
		int tipo_mora=1;							// Valor 61) Tipo_Mora
		int tipo_frecuencia_rev_int_mora = 2;		// Valor 63) Tipo_frecuencia_rev_int_mora
		int frecuencia_rev_int_mora = 1;			// Valor 64) Frecuencia rev int mora
		String tipo_tasa_int_mora=null;				// Valor 67) Tipo_Tasa_Int_Mora
		String numero_cliente_troya = null;			// Valor 68) Numero_cliente_troya
		String numero_solic_troya=null;				// Valor 69) Numero_solic_troya
		int numero_epo=0;							// Valor 70) Numero_de_epo
		String modalidad=null;						// Valor 71) Modalidad
													// Valor 72) Estatus


		String proyectosLog="", contratosLog="";
		String paramStore = "PR_K_Apertura_Prestamo.Pr_Alta_Prestamo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int icMoneda=1, ic_epo = 0;
		ResultSet rs=null, rs1=null;
		CallableStatement cs=null;
		boolean errorTransaccion = false;
		boolean errorDesconocido=false;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		String numPrestamoPedido= null;
		String strProducto = null;
		String strFechaDiversos = null;
		String strFechaProyectos = null;
		String strFechaContratos = null;
                
                String icPausaInterfazDsctoe="";
                String icPausaInterfazDistr="";
                
	    int tipoPiso;
	    double monto_amort_ajuste=0.0;
                
		Interfaz  inter = new Interfaz(); // F001-2016
		AccesoDBOracle con = new AccesoDBOracle();
                PreparedStatement ps=null;
                try {
                    con.conexionDB();
		} catch (Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

	    try {

	            query = "SELECT " +
	            "    ic_pausa_interfaz_dsctoe, " + 
	            "    ic_pausa_interfaz_distr " + 
	            " FROM " + 
	            "    com_param_gral";
	            rs=con.queryDB(query);
	            while(rs.next()) {
	                icPausaInterfazDsctoe = rs.getString("IC_PAUSA_INTERFAZ_DSCTOE");   //Proyectos
	                icPausaInterfazDistr = rs.getString("IC_PAUSA_INTERFAZ_DISTR");     //Proyectos
	            }
	            rs.close();
	            con.cierraStatement();
	    } catch (Exception e){
	            icPausaInterfazDsctoe="N";
	            icPausaInterfazDistr="N";
	            System.out.println("Exception. Pausa interfaz proyecto: " + e);
	    }


		try{
                    
		    ///////////////////////////////////////////////////////////////////////////////////////////////
		    ///////////////////////////////////////////////////////////////////////////////////////////////
		    ////I n i c i o   I n t e r f a s e   1 e r P i s o / F a c t o r a j e c o n R e c u r s o////
		    ///////////////////////////////////////////////////////////////////////////////////////////////
		    ///////////////////////////////////////////////////////////////////////////////////////////////
                    
		    if(icPausaInterfazDsctoe.equals("N")){
                    System.out.println(" Inicio Operaciones 1er Piso/Factoraje con Recurso "+new Date());
			query =
				" SELECT /*+index(s) use_nl(d ds s tb i t c3)*/ " +
                                "       c3.ic_folio , c3.ig_codigo_agencia"   +
				"     , c3.ig_codigo_sub_aplicacion"   +
				"     , c3.ig_numero_linea_fondeo"   +
				"     , c3.ig_codigo_financiera"   +
				"     , c3.ig_aprobado_por"   +
				"     , c3.cg_descripcion"   +
				"     , c3.fn_monto_operacion"   +
				"     , c3.ig_numero_linea_credito"   +
				"     , c3.ig_codigo_agencia_proy"   +
				"     , c3.ig_codigo_sub_aplicacion_proy"   +
				"     , c3.ig_numero_contrato"   +
				"     , c3.cs_estatus"   +
				"     , s.ic_tabla_amort"   +
				"     , s.df_ppi"   +
				"     , s.df_ppc"   +
				"     , s.df_v_descuento"   +
				"     , trunc(SYSDATE)"   +
				"     , s.ig_plazo"   +
				"     , SUBSTR (c3.ic_folio, 1, 10) numero_electronico"   +
				"     , i.ig_tipo_piso"   +
				"     , i.ic_if"   +
				"     , c3.ig_numero_prestamo_pedido"   +
				"     , ds.cg_puntos "   +
				"     , tb.ic_tasa"   +
				"     , d.ic_documento"   +
				"     , t.fg_sobretasa"   +
				"     , d.ic_moneda"   +
				"	  , d.ic_epo " +
				"	  , c3.cg_proyectos_log " +
				"	  , c3.cg_contratos_log " +
				"	  , c3.cc_producto " +
				"	  , to_char(c3.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"	  , to_char(c3.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"	  , to_char(c3.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"	  , s.df_fecha_solicitud " +
				"	  , TO_NUMBER(null) modalidad " +
				"	  , TO_NUMBER(null) fn_puntos_pref " +
				"	  , '' cg_rel_mat_pref " +
				"	  , 'Prestamos(Descuento 1er Piso)' as OrigenQuery " +
                                " FROM "+
                                "     com_documento            d, "+
                                "     com_docto_seleccionado   ds, "+
                                "     com_solicitud            s, "+
                                "     comrel_tasa_base         tb, "+
                                "     comcat_if                i, "+
                                "     comcat_tasa              t, "+
                                "     com_control03            c3 "+
                                " WHERE "+
                                "     d.ic_documento = ds.ic_documento "+
                                "     AND ds.ic_documento = s.ic_documento "+
                                "     AND s.ic_folio = c3.ic_folio "+
				"    AND ds.dc_fecha_tasa = tb.dc_fecha_tasa"   +
				"    AND tb.ic_tasa = t.ic_tasa"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND c3.cs_estatus = ? "   +
                                "    AND ds.ic_linea_credito_emp is null "   +
				"    AND s.ic_bloqueo = ? "  +
                                "    AND s.ic_estatus_solic = ? "  +
				" UNION "   +
                                " SELECT /*+index(s) use_nl(d ds s tb i t c3 lc)*/ " +
                                "       c3.ic_folio , c3.ig_codigo_agencia"   +
				"     , c3.ig_codigo_sub_aplicacion"   +
				"     , c3.ig_numero_linea_fondeo"   +
				"     , c3.ig_codigo_financiera"   +
				"     , c3.ig_aprobado_por"   +
				"     , c3.cg_descripcion"   +
				"     , c3.fn_monto_operacion"   +
				"     , c3.ig_numero_linea_credito"   +
				"     , c3.ig_codigo_agencia_proy"   +
				"     , c3.ig_codigo_sub_aplicacion_proy"   +
				"     , c3.ig_numero_contrato"   +
				"     , c3.cs_estatus"   +
				"     , s.ic_tabla_amort"   +
				"     , s.df_ppi"   +
				"     , s.df_ppc"   +
				"     , s.df_v_descuento"   +
				"     , trunc(SYSDATE)"   +
				"     , s.ig_plazo"   +
				"     , SUBSTR (c3.ic_folio, 1, 10) numero_electronico"   +
				"     , i.ig_tipo_piso"   +
				"     , i.ic_if"   +
				"     , c3.ig_numero_prestamo_pedido"   +
				"     , ds.cg_puntos "   +
				"     , tb.ic_tasa"   +
				"     , d.ic_documento"   +
				"     , t.fg_sobretasa"   +
				"     , d.ic_moneda"   +
				"	  , d.ic_epo " +
				"	  , c3.cg_proyectos_log " +
				"	  , c3.cg_contratos_log " +
//				"	  , c3.cc_producto " +
				"     , '1C' cc_producto " +
				"	  , to_char(c3.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"	  , to_char(c3.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"	  , to_char(c3.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"	  , s.df_fecha_solicitud " +
				"	  , 3 modalidad " +
				"	  , ds.fn_puntos_pref fn_puntos_pref " +
				"	  , ds.cg_rel_mat_pref cg_rel_mat_pref " +
				"	  , 'Prestamos(Factoraje con Recurso)' as OrigenQuery " +
                                " FROM "+
                                "     com_documento            d, "+
                                "     com_docto_seleccionado   ds, "+
                                "     com_solicitud            s, "+
                                "     comrel_tasa_base         tb, "+
                                "     comcat_if                i, "+
                                "     comcat_tasa              t, "+
                                "     com_control03            c3, "+
                                "     emp_linea_credito        lc "	+
                                " WHERE "+
                                "     d.ic_documento = ds.ic_documento "+
                                "     AND ds.ic_documento = s.ic_documento "+
                                "     AND s.ic_folio = c3.ic_folio "+
				"    AND ds.ic_linea_credito_emp = lc.ic_linea_credito "   +
				"    AND ds.dc_fecha_tasa = tb.dc_fecha_tasa"   +
				"    AND tb.ic_tasa = t.ic_tasa"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND c3.cs_estatus = ? "   +
				"    AND s.ic_bloqueo = ? "  +
                                "    AND s.ic_estatus_solic = ? "  +
				"  ORDER BY ig_tipo_piso desc, df_fecha_solicitud ASC";
                                //System.out.println("\nquery_PPFR:"+query);
                        try {
                            ps = con.queryPrecompilado(query);
			    ps.setString(1, "S");
			    ps.setInt(2, Integer.valueOf(3));
			    ps.setInt(3, Integer.valueOf(2));
			    ps.setString(4, "S");
			    ps.setInt(5, Integer.valueOf(3));
			    ps.setInt(6, Integer.valueOf(2));
			    rs = ps.executeQuery();
			    ps.clearParameters();
			} catch(SQLException sqle) {
				System.out.println("Prestamos.java(query): " + query );
				throw sqle;
			}
			numero_cliente_troya = null;
			numero_solic_troya = null;

			while(rs.next()) {
				errorTransaccion = false;
				errorDesconocido = false;
				diciembre = "0";
				icMoneda = rs.getInt("IC_MONEDA");
				spread_corriente_1=0.0;
				fecha_revi_int = null;
				tipo_tasa = null;
				rela_mate_mora = null;
				spread_Mora = null;
				periodo_Tasa = null;
				codigo_Valor_Tasa_Mora = null;
				/*
				Se asigna la variable de numero de prestamo obtenido en el credito de Financiamiento a Pedidos
				para determinar si se va a realizar una cobranza
				*/
				numPrestamoPedido = rs.getString("IG_NUMERO_PRESTAMO_PEDIDO");


				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio=rs.getString("IC_FOLIO");
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=(rs.getString("IG_APROBADO_POR")==null)?null:"'"+rs.getString("IG_APROBADO_POR")+"'";
				descripcion=rs.getString("CG_DESCRIPCION");
				mto_operac=rs.getDouble("FN_MONTO_OPERACION");
				no_linea_credito=rs.getLong("IG_NUMERO_LINEA_CREDITO");
				cod_agencia_proy=rs.getInt("IG_CODIGO_AGENCIA_PROY");
				cod_sub_apli_proy=rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				estatus=rs.getString("CS_ESTATUS");
				cod_tipo_amort=rs.getString("IC_TABLA_AMORT");		/* Valor 3) Codigo_Tipo_Amortizacion */
				no_contrato=rs.getInt("IG_NUMERO_CONTRATO");		/* Valor 4) Numero_Contrato */
				valor_inicial=rs.getDouble("FN_MONTO_OPERACION");	/* Valor 5) Valor_Inicial */
				fecha_ppi=rs.getDate("DF_PPI");						/* Valor 7) Fecha_Primer_Pago_Interes */
				fecha_ppc=rs.getDate("DF_PPC");						/* Valor 8) Fecha_Primer_Pago_Capital */
				fecha_venc=rs.getDate("DF_V_DESCUENTO");			/* Valor 23) Fecha_Vencimiento */
				fecha_adicion=rs.getDate(18);						/* Valor 32) Fecha_Adicion */
				no_electronico=rs.getLong("NUMERO_ELECTRONICO");	/* Valor 57) Numero_Electr�nico */
				ic_plazo=rs.getInt("IG_PLAZO");
				tipo_interes = "S";
				periodo_capitalizacion = (tipo_interes.equals("C"))?1:0;
				dia_revision_tasa=Integer.parseInt(fecha_ppi.toString().substring(8,10));	/* Valor 42) Dia_Revision_Tasa */
				dia_pago_interes=Integer.parseInt(fecha_ppi.toString().substring(8,10));	/* Valor 40) Dia_Pago_Interes */
				dia_pago_capital=Integer.parseInt(fecha_ppc.toString().substring(8,10));	/* Valor 41) Dia_Pago_Capital */
				valor_tasa_cartera = rs.getInt("IC_TASA");
				ic_epo = rs.getInt("IC_EPO");
				proyectosLog = (rs.getString("CG_PROYECTOS_LOG") == null)?"": rs.getString("CG_PROYECTOS_LOG");
				proyectosLog = Comunes.protegeCaracter(proyectosLog, '\'', "\'");
				contratosLog = (rs.getString("CG_CONTRATOS_LOG") == null)?"": rs.getString("CG_CONTRATOS_LOG");
				contratosLog = Comunes.protegeCaracter(contratosLog, '\'', "\'");
				numero_epo = ic_epo;

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");
				strFechaContratos = rs.getString("DF_CONTRATOS");

				modalidad=rs.getString("modalidad");

			//	System.out.println("(3)"+cod_tipo_amort+"(4)"+no_contrato+"(5)"+valor_inicial+"(7)"+fecha_ppi+"(8)"+fecha_ppc+"(23)"+fecha_venc+"(32)"+fecha_adicion+"(40)"+dia_pago_interes+"(41)"+dia_pago_capital+"(57)"+no_electronico);

				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Prestamos.java(paramStore): " + paramStore );
					throw sqle;
				}
				System.out.println("tipoPiso *** "+tipoPiso);
				if (tipoPiso==1) {

					calcular_mora = "S";

					cs.setString(6, calcular_mora);
					
					
					HashMap listDatoParam = (HashMap)inter.getValueParametrizado(String.valueOf(ic_epo),"1C","PR");//  F002-2016
					intereses_vencidos = (String)listDatoParam.get("9");//   F002-2016
					tipo_tasa = (String)listDatoParam.get("13");//   F002-2016
					periodo_Tasa = (String)listDatoParam.get("15");//   F002-2016
					spread_Mora = (String)listDatoParam.get("16");//   F002-2016
					rela_mate_mora = (String)listDatoParam.get("21");//   F002-2016
					//codigo_Valor_Tasa_Mora = (String)listDatoParam.get("36");//   F002-2016 se comento ya que no es parametrizable, petici�n edgar
					tipo_frecuencia_revision = (String)listDatoParam.get("37");//   F002-2016
					tipo_interes = (String)listDatoParam.get("53");//   F002-2016
					periodo_capitalizacion = Integer.parseInt((String)listDatoParam.get("54"));//   F002-2016
					
					//intereses_vencidos = "N"; SE COMENTA  F002-2016
					cs.setString(9, intereses_vencidos);
					// tipo_tasa = "0"; SE COMENTA  F002-2016
					cs.setString(13, tipo_tasa);
					// periodo_Tasa = "1"; SE COMENTA  F002-2016
					cs.setInt(15, Integer.parseInt(periodo_Tasa));		/* Valor 15) Periodo_Tasa */
					/*INICIO---MODIFICACION F039-2008*/
					if(modalidad!=null){
						spread_corriente_1 = rs.getDouble("fn_puntos_pref");/* Valor 17)  */
						rela_mate_1 = rs.getString("cg_rel_mat_pref");/* Valor 19)  */
					}else{
						spread_corriente_1 = rs.getDouble("CG_PUNTOS");
						rela_mate_1 = "+";
					}
					/*FIN---MODIFICACION F039-2008*/
					/* Valor 16) Spread_Mora */
					/* Valor 17) Spread_Corriente_1 */
					if (numPrestamoPedido == null) {
						//spread_corriente_1 = 1.75;
						//spread_corriente_1 = rs.getDouble("CG_PUNTOS");MODIFICACION F039-2008
						cs.setDouble(17, spread_corriente_1);
						//rela_mate_mora="*"; F002-2016
						// TEMPORAL
						/*if (ic_epo == 256 || ic_epo == 432) {	// IMSS (Centralizado) y EPO DE EXPORTACION
							spread_Mora = "2";
						} else if(ic_epo == 16 || ic_epo == 358 || ic_epo == 396) {	// INFONAVIT (1, 2 y 3)
							spread_Mora = "1.1";
						} else {
							spread_Mora = null;
							rela_mate_mora = null;
						}*/ // F002-2016
						//TEMPORAL

					} else {
						spread_Mora = "2"; // PREGUNTAR SI NO SE DEBE AFECTAR
						//spread_corriente_1 = rs.getDouble("CG_PUNTOS");MODIFICACION F039-2008
						cs.setDouble(17, spread_corriente_1);
						rela_mate_mora="*"; // PREGUNTAR SI NO SE DEBE AFECTAR
					}


					cs.setNull(18, Types.DOUBLE);		/* Valor 18) Spread_Corriente_2 */
					//rela_mate_1 = "+";MODIFICACION F039-2008
					cs.setString(19, rela_mate_1);
					cs.setNull(20, Types.VARCHAR);
					queri = "select fecha_hoy from mg_calendario where codigo_empresa=1 AND codigo_aplicacion = 'BPR'  ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();
					//fecha_apertura=fecha_revi_int;		/* Valor 25) Fecha_Apertura */

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());

					/* Valor 36) Codigo_Valor_Tasa_Mora */
					if (numPrestamoPedido==null) {
						codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);//Modificaci�n F002-2016 petici�n por edgar
						/*
						// TEMPORAL
						if (ic_epo == 256)	// IMSS (Centralizado)
							/*se asigna a 'codigo_Valor_Tasa_Mora' el valor de 'valor_tasa_cartera' en 
							 aquellos casos donde tiene valor "20" debido al cambio de la politica tarifaria
							
							//codigo_Valor_Tasa_Mora = "20";
							codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
						else if(ic_epo == 432) //EPO DE EXPORTACION
							codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
						else if(ic_epo == 16 || ic_epo == 358 || ic_epo == 396)	// INFONAVIT (1, 2 y 3)
							//codigo_Valor_Tasa_Mora = "20";
							codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
						else
							codigo_Valor_Tasa_Mora = null;
						// TEMPORAL
						*/ // SE COMENTO F002-2016
					} else
						codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera); // PREGUNTAR SI NO SE DEBE AFECTAR
						//codigo_Valor_Tasa_Mora = "20";
						
					cs.setInt(37, Integer.parseInt(tipo_frecuencia_revision));					/* Valor 37) Tipo_Frecuencia_Revision */ // MODIFICO F002-2016
					// cs.setInt(37, 2);					/* Valor 37) Tipo_Frecuencia_Revision */ // COMENTO F002-2016
					
					/* Valor 44) tipo_calculo_mora */
					if (numPrestamoPedido==null ) {
						//TEMPORAL
						if ( ic_epo == 256 )// IMSS (Centralizado)
							tipo_calculo_mora = "2";
						else
							tipo_calculo_mora = "2";
					} else
						tipo_calculo_mora = "2";

					/* Valor 51) Tasa_Total_Moratoria */
					if (numPrestamoPedido==null) {
						cs.setNull(51, Types.DOUBLE);
						// TEMPORAL
						/*if ( ic_epo == 256 && calcular_mora.equals("S") ) {
							cs.setNull(51, Types.DOUBLE);
						} else {
							cs.setDouble(51, 0);
						}*/
						// TEMPORAL
					} else {
						cs.setNull(51, Types.DOUBLE);
						diciembre = "2";
					}
					// tipo_interes = "S"; SE COMENTA EN EL F002-2016
					cs.setString(53, tipo_interes);
					// periodo_capitalizacion = 0; SE COMENTA EN EL F002-2016
					cs.setInt(54, periodo_capitalizacion);
					cs.setInt(63, 2);					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					cs.setInt(64, 1);					/* Valor 64) Frecuencia_Rev_Int_Mora */
					cs.setInt(65, dia_pago_interes);	/* Valor 65) Dia_Revision_Tasa_Mora */
					cs.setDate(66, fecha_revi_int, Calendar.getInstance());		/* Valor 66) Fecha_revision_mora */

					// TEMPORAL
					if (numPrestamoPedido==null) {
						if ( ic_epo == 256)
							tipo_tasa_int_mora = "5";
						else
							tipo_tasa_int_mora = "5";
					} else {
						tipo_tasa_int_mora = "5";
					}
					// TEMPORAL
					cs.setString(67, tipo_tasa_int_mora);	/* Valor 67) Tipo_Tasa_Int_Mora */

				} else {	// 2do Piso
					System.out.println("2o PISO - ");
					cs.setString(6, calcular_mora);
					cs.setString(9, intereses_vencidos);
					tipo_tasa = (icMoneda==54)?"6":null;
					cs.setString(13, tipo_tasa);
					/* Valor 15) Periodo_Tasa */
					/* Valor 16) Spread_Mora */
					if (icMoneda==54) {
						periodo_Tasa = "1";
						cs.setInt(15, Integer.parseInt(periodo_Tasa));
						spread_Mora = null;
						//cs.setNull(16, Types.INTEGER);
					} else {
						periodo_Tasa = null;
						cs.setNull(15, Types.INTEGER);
						spread_Mora = "2";
						//cs.setInt(16, Integer.parseInt(spread_Mora));
					}
					/* Valor 17) Spread_Corriente_1 */
					if (icMoneda==54) {
						spread_corriente_1 = rs.getDouble("FG_SOBRETASA");
						cs.setDouble(17, spread_corriente_1);
						rela_mate_1 = "+";
						cs.setString(19, rela_mate_1);

					} else {
						cs.setNull(17, Types.DOUBLE);
						rela_mate_1 = null;
						cs.setString(19, rela_mate_1);
					}
					cs.setNull(18, Types.DOUBLE);			/* Valor 18) Spread_Corriente_2 */
					cs.setString(20, rela_mate_2);
					rela_mate_mora = "*";
					//cs.setString(21, rela_mate_mora);
					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);	/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();

					cs.setDate(24, fecha_revi_int, Calendar.getInstance());
					diciembre = (icMoneda == 54)?null:"0";

					/* Valor 36) Codigo_Valor_Tasa_Mora */
					if (icMoneda==54){
						codigo_Valor_Tasa_Mora = null;
					}else{	// Si el plazo es mayor a 60 d�as se env�a TIIE 20 si no se env�a la tasa de comrel_tasa_base.
            if (rs.getInt("IC_IF")==113){ // Condici�n FIJA ya que para el FIDEICOMISO HOMEX se defini� por comite la tasa moratoria en todos los plazos a TIIE 04/07/2009
              valor_tasa_cartera = 20;
              codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
            }else{
              codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
            }
          }
						//codigo_Valor_Tasa_Mora = (ic_plazo>60?"20":String.valueOf(valor_tasa_cartera));

					/* Valor 37) Tipo_Frecuencia_Revision */
					cs.setNull(37, Types.INTEGER);

					/* Valor 44) tipo_calculo_mora */
					if(icMoneda==54)
						tipo_calculo_mora = "1";
					else
						tipo_calculo_mora = "2";

					/* Valor 51) Tasa_Total_Moratoria */
					cs.setNull(51, Types.DOUBLE);

					tipo_interes = (icMoneda==54)?"S":tipo_interes;

					//Solo para walmart 2o piso MN Foda 20 JRFH
					if(icMoneda==1 && ic_epo==7){
						tipo_interes = (ic_plazo > 31)?"C":"S";		//Si el plazo es mayor a 31 d�as es Compuesto de lo contrario es simple
						tipo_calculo_mora = "1";
					}

					cs.setString(53, tipo_interes);
					periodo_capitalizacion = (tipo_interes.equals("S"))?0:1;
					cs.setInt(54, periodo_capitalizacion);
					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					/* Valor 64) Frecuencia_Rev_Int_Mora */
					/* Valor 65) Dia_Revision_Tasa_Mora */
					/* Valor 66) Fecha_revision_mora */
					if (icMoneda==54) {
						cs.setInt(63, 2);
						cs.setNull(64, Types.INTEGER);
						cs.setInt(65, dia_pago_interes);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
						tipo_tasa_int_mora = null;
					} else {
						cs.setInt(63, tipo_frecuencia_rev_int_mora);
						cs.setInt(64, frecuencia_rev_int_mora);
						cs.setInt(65, dia_pago_interes);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
						tipo_tasa_int_mora = "5";
					}
					//tipo_tasa_int_mora = (icMoneda==54)?"5":null;

					cs.setString(67, tipo_tasa_int_mora);		/* Valor 67) Tipo_Tasa_Int_Mora */
				}

				cs.setNull(1, Types.INTEGER);				/* Valor 1) Numero_Prestamo */
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, valor_tasa_cartera);
				cs.setString(3, cod_tipo_amort);
				cs.setInt(4, no_contrato);
				cs.setDouble(5, valor_inicial);
				cs.setDate(7, fecha_ppi,Calendar.getInstance());
				cs.setDate(8, fecha_ppc,Calendar.getInstance());
				cs.setString(10, intereses_normales);
				cs.setInt(11, frecuencia_interes);
				cs.setInt(12, frecuencia_capital);
				cs.setString(14, clase_tasa);
				/* Valor 16) Spread_Mora */
				if(spread_Mora == null)
					cs.setNull(16, Types.INTEGER);
				else
					cs.setDouble(16, Double.parseDouble(spread_Mora));
				cs.setString(21, rela_mate_mora);
				cs.setString(22, observaciones);
				cs.setDate(23, fecha_venc,Calendar.getInstance());
				cs.setDate(25, fecha_apertura,Calendar.getInstance());
				cs.setDouble(26, comision);
				cs.setString(27, diciembre);
				cs.setInt(28, dias_gracia_mora);
				cs.setString(29, tipo_liqui_mes);
				cs.setString(30, tipo_liqui_ano);
				cs.setString(31, adicionado_por);
				cs.setDate(32, fecha_adicion,Calendar.getInstance());
				cs.setNull(33, Types.INTEGER);				/* Valor 33) Tipo_Frecuencia_Capital */
				cs.setNull(34, Types.INTEGER);				/* Valor 34) Tipo_Frecuencia_Interes */
				cs.setString(35, cobrar_iva);
				/* Valor 36) Codigo_Valor_Tasa_Mora */
				if(codigo_Valor_Tasa_Mora == null)
					cs.setNull(36, Types.INTEGER);
				else
					cs.setInt(36, Integer.parseInt(codigo_Valor_Tasa_Mora));

				cs.setString(38, rela_mate_3);
				cs.setNull(39, Types.DOUBLE);				/* Valor 39) Spread_Corriente_3 */
				cs.setInt(40, dia_pago_interes);
				cs.setInt(41, dia_pago_capital);
				cs.setInt(42, dia_revision_tasa);
				cs.setString(43, tipo_redondeo);
				cs.setString(44, tipo_calculo_mora);		/*Valor 44 tipo_calculo_mora */
				if (icMoneda==54){
					cs.setNull(45, Types.INTEGER);
				} else {
					cs.setInt(45, rec_frescos);
				}
				cs.setString(46, amort_ult_prim);
				cs.setNull(47, Types.DOUBLE);				/* Valor 47) Monto_Amortizacion_Ajuste */
				cs.setString(48, forma_redondeo);
				cs.setString(49, desgloce_tasas);
				cs.setString(50, cobro_int_ES);
				cs.setString(52, clausula_venci);
				cs.setString(55, valor_presente);
				cs.setNull(56, Types.INTEGER);				/* Valor 56) Codigo_Comision */
				cs.setLong(57, no_electronico);
				cs.setString(58, pago_masivo);
				cs.setString(59, tipo_renta);
				cs.setInt(60, no_cuotas_recortadas);
				cs.setInt(61, tipo_mora);
				cs.setNull(62, Types.INTEGER);				/* Valor 62) Codigo_Grupo_Comisi�n */
				//FODA 099 - Interfase N@E-Troya
				cs.setString(68, numero_cliente_troya);				/* Valor 68) Numero Cliente Troya */
				cs.setNull(69, Types.INTEGER);				/* Valor 69) N�mero Sol Troya Nafin */
				cs.setInt(70, numero_epo);					/* Valor 70) N�mero de EPO n@e */
				if(modalidad!=null)
					cs.setInt(71, Integer.parseInt(modalidad));					/* Valor 71) modalidad nafin empresarial */
				else
					cs.setNull(71, Types.INTEGER);
				cs.setNull(72, Types.INTEGER);				/* Valor 72) Estatus */
				cs.registerOutParameter(72, Types.INTEGER);

				cadena="P1:=null;\n"+
					"P2:="+valor_tasa_cartera+";\n"+
					"P3:='"+cod_tipo_amort+"';\n"+
					"P4:="+no_contrato+";\n"+
					"P5:="+valor_inicial+";\n"+
					"P6:='"+calcular_mora+"';\n"+
					"P7:=to_date('"+fecha_ppi+"','yyyy-mm-dd');\n"+
					"P8:=to_date('"+fecha_ppc+"','yyyy-mm-dd');\n"+
					"P9:='"+intereses_vencidos+"';\n"+
					"P10:='"+intereses_normales+"';\n"+
					"P11:="+frecuencia_interes+";\n"+
					"P12:="+frecuencia_capital+";\n"+
					"P13:="+tipo_tasa+";\n"+
					"P14:='"+clase_tasa+"';\n"+
					"P15:='"+periodo_Tasa+"';\n"+
					"P16:='"+spread_Mora+"';\n";
				if (tipoPiso==1) {
					if (numPrestamoPedido==null) {
						cadena+="P17:="+spread_corriente_1+";\n"+
								"P18:=null;\n"+
								"P19:='"+rela_mate_1+"';\n"+
								"P20:="+rela_mate_2+";\n"+
								"P21:="+rela_mate_mora+";\n";
					} else {
						cadena+="P17:="+spread_corriente_1+";\n"+
								"P18:=null;\n"+
								"P19:='"+rela_mate_1+"';\n"+
								"P20:="+rela_mate_2+";\n"+
								"P21:='"+rela_mate_mora+"';\n";
					}
				} else {
					if (icMoneda==54) {
						cadena+="P17:="+spread_corriente_1+";\n"+
								"P18:=null;\n"+
								"P19:='"+rela_mate_1+"';\n"+
								"P20:="+rela_mate_2+";\n"+
								"P21:='"+rela_mate_mora+"';\n";
					} else {
						cadena+="P17:=null;\n"+
								"P18:=null;\n"+
								"P19:="+rela_mate_1+";\n"+
								"P20:="+rela_mate_2+";\n"+
								"P21:="+rela_mate_mora+";\n";
					}
				}

				cadena+="P22:='"+observaciones+"';\n"+
						"P23:=to_date('"+fecha_venc+"','yyyy-mm-dd');\n";
				if (tipoPiso==1) {
					cadena += "P24:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n";
				} else {
					cadena += "P24:="+fecha_revi_int+";\n";
				}
				cadena+="P25:="+fecha_apertura+";\n"+
						"P26:="+comision+";\n"+
						"P27:='"+diciembre+"';\n"+
						"P28:="+dias_gracia_mora+";\n"+
						"P29:='"+tipo_liqui_mes+"';\n"+
						"P30:='"+tipo_liqui_ano+"';\n"+
						"P31:='"+adicionado_por+"';\n"+
						"P32:=to_date('"+fecha_adicion+"','yyyy-mm-dd');\n"+
						"P33:=null;\n"+
						"P34:=null;\n"+
						"P35:='"+cobrar_iva+"';\n" +
						"P36:="+codigo_Valor_Tasa_Mora+";\n";

				if (tipoPiso==1) {
					cadena += "P37:="+tipo_frecuencia_revision+";\n";
				} else {
					cadena += "P37:=null;\n";
				}
				cadena+="P38:="+rela_mate_3+";\n"+
						"P39:=null;\n"+
						"P40:="+dia_pago_interes+";\n"+
						"P41:="+dia_pago_capital+";\n"+
						"P42:="+dia_revision_tasa+";\n"+
						"P43:='"+tipo_redondeo+"';\n"+
						"P44:='"+tipo_calculo_mora+"';\n";
				if (icMoneda==54){
					cadena += "P45:=null;\n";
				} else {
					cadena += "P45:="+rec_frescos+";\n";
				}
				cadena+="P46:="+amort_ult_prim+";\n"+
						"P47:=null;\n"+
						"P48:='"+forma_redondeo+"';\n"+
						"P49:="+desgloce_tasas+";\n"+
						"P50:='"+cobro_int_ES+"';\n";

				/*if (tipoPiso==1 && numPrestamoPedido==null) {
					// TEMPORAL
					if ( ic_epo == 256 && calcular_mora.equals("S") ) {
						cadena += "P51:=null;"+"\n";
					} else {
						cadena += "P51:=0;"+"\n";
					}
					// TEMPORAL
				} else {
					cadena += "P51:=null;"+"\n";
				}*/
				cadena += "P51:=null;"+"\n";
				cadena+="P52:='"+clausula_venci+"';\n"+
						"P53:='"+tipo_interes+"';\n"+
						"P54:="+periodo_capitalizacion+";\n"+
						"P55:='"+valor_presente+"';\n"+
						"P56:=null;\n"+
						"P57:="+no_electronico+";\n"+
						"P58:='"+pago_masivo+"';\n"+
						"P59:="+tipo_renta+";\n"+
						"P60:="+no_cuotas_recortadas+";\n"+
						"P61:="+tipo_mora+";\n"+
						"P62:=null;\n";
				if (tipoPiso==1) {
					cadena += "P63:=2;\n";
					// TEMPORAL
					if ( ic_epo == 256 && calcular_mora.equals("S") ) {
						cadena += "P64:=null;\n";
					} else {
						cadena += "P64:=1;\n";
					}
					// TEMPORAL
					cadena+="P65:="+dia_pago_interes+";\n"+
						    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n"+
							"P67:='"+tipo_tasa_int_mora+"';\n";
				} else {
					if (icMoneda==54) {
						cadena+="P63:=2;\n"+
								"P64:=null;\n"+
								"P65:="+dia_pago_interes+";\n"+
							    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n"+
								"P67:='"+tipo_tasa_int_mora+"';\n";
					} else {
						cadena+="P63:="+tipo_frecuencia_rev_int_mora+";\n"+
								"P64:="+frecuencia_rev_int_mora+";\n"+
								"P65:="+dia_pago_interes+";\n"+
								"P66:="+fecha_revi_int+";\n"+
								"P67:="+tipo_tasa_int_mora+";\n";
					}
				}
				cadena+="P68:="+numero_cliente_troya+";\n"+
						"P69:="+numero_solic_troya+";\n"+
						"P70:="+numero_epo+";\n"+
						"P71:="+modalidad+";\n"+
						"P72:=null;\n";


				System.out.println(cadena);

				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						no_prestamo = cs.getInt(1);
						status = cs.getString(72);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle) {
					sqle.printStackTrace();
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					status="9";
				}

				System.out.println("No Prestamo: "+no_prestamo+" Estatus: "+status);
				cadena += "No Prestamo: "+no_prestamo+" Estatus: "+status;

									/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					con.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PR'";
						try {
							rs1=con.queryDB(queri);
						} catch(SQLException sqle) {
							sqle.printStackTrace();
							System.out.println("Prestamos.java(queri): " + queri );
							throw sqle;
						}

						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from com_control05 where ic_folio='"+folio+"'";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						sqle.printStackTrace();
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); con.cierraStatement();

					if(no_doctos==0) {
						queri = "insert into com_control05(ic_folio,cc_codigo_aplicacion,ic_error_proceso, "+
							"df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por, "+
							"ig_numero_linea_credito,ig_codigo_agencia_proy, "+
							"ig_codigo_sub_aplicacion_proy, ig_proceso_numero"+
							" ,cc_producto, df_diversos, df_proyectos, df_contratos)"+
							"values('"+folio+"','"+codigo_aplicacion+"',"+status+" "+
							",SYSDATE,"+cod_agen+","+cod_subapli+","+no_lineafondeo+" "+
							","+cod_finan+","+aprobado_por+","+no_linea_credito+" "+
							","+cod_agencia_proy+","+cod_sub_apli_proy+",3 " +
							" , '" + strProducto + "', to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), to_date('" + strFechaProyectos + "', 'dd/mm/yyyy'), to_date('" + strFechaContratos + "', 'dd/mm/yyyy') )";
					} else {
						queri = "update com_control05 set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
							",ic_error_proceso="+status+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+cod_agen+" "+
							",ig_codigo_sub_aplicacion="+cod_subapli+" ,ig_numero_linea_fondeo="+no_lineafondeo+" "+
							",ig_codigo_financiera="+cod_finan+" ,ig_aprobado_por="+aprobado_por+" "+
							",ig_numero_linea_credito="+no_linea_credito+" ,ig_codigo_agencia_proy="+cod_agencia_proy+" "+
							",ig_codigo_sub_aplicacion_proy="+cod_sub_apli_proy+
							" , ig_proceso_numero=3 "+
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" , df_contratos = to_date('" + strFechaContratos + "', 'dd/mm/yyyy')" +
							"where ic_folio='"+folio+"'";
					}
//						System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
						queri = "UPDATE com_control03 " +
						" SET df_proceso = sysdate " +
						" , cs_estatus='N' "+
						" , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
						" WHERE ic_folio='"+folio+"'";
//						System.out.println(queri);
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							sqle.printStackTrace();
							System.out.println("Error al Actualizar el folio en Control03.");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						sqle.printStackTrace();
						System.out.println("Error al Insertar o al Actualizar los Datos en Control05.");
						errorTransaccion = true;
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de control03 a com_control04 */
					queri = "insert into com_control04(ic_folio,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
						"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
						"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
						"ig_codigo_sub_aplicacion_proy,ig_numero_contrato,ig_numero_prestamo,cs_estatus"+
						", ig_numero_prestamo_pedido,cg_proyectos_log,cg_contratos_log,cg_prestamos_log "+
						", cc_producto, df_diversos, df_proyectos, df_contratos, df_prestamos) "+
						"values('"+folio+"',"+cod_agen+","+cod_subapli+","+
						""+no_lineafondeo+","+cod_finan+","+aprobado_por+",'"+descripcion+"',"+
						""+mto_operac+","+no_linea_credito+","+cod_agencia_proy+","+
						""+cod_sub_apli_proy+","+no_contrato+","+no_prestamo+",'"+estatus+"'"+
						","+numPrestamoPedido+", '"+proyectosLog+"','"+contratosLog+"','"+ Comunes.protegeCaracter(cadena, '\'', "\'") +"' "+
						" , '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), to_date('"+strFechaProyectos+"', 'dd/mm/yyyy'), to_date('"+strFechaContratos+"', 'dd/mm/yyyy'), SYSDATE)";
//System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
						/* Borramos el Documento de control03 si esta Bien. */
						queri = "delete com_control03 where ic_folio='"+folio+"'";
//System.out.println(queri);
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							sqle.printStackTrace();
							System.out.println("Error al Borrar los Datos en com_control03");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						sqle.printStackTrace();
						System.out.println("Error al Insertar los Datos en com_control04");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//while
			if (cs !=null) cs.close();
			rs.close(); ps.close();
                        //con.cierraStatement();
                    
                        System.out.println(" Fin Operaciones 1er Piso/Factoraje con Recurso "+new Date());
                        }
			/* Inicio Interfase Financiamiento a Pedidos */

			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////
			////I n i c i o   I n t e r f a s e   F i n a n c i a m i e n t o   a   P e d i d o s////
			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////

			if(icPausaInterfazDsctoe.equals("N")){
			System.out.println(" Inicio Operaciones Financiamiento a Pedidos ");

			query =
				"SELECT   C3.ic_pedido " +
				"         , C3.ig_codigo_agencia"+
				"         , C3.ig_codigo_sub_aplicacion "+
				"         , C3.ig_numero_linea_fondeo"+
				"         , C3.ig_codigo_financiera"+
				"         , C3.ig_aprobado_por "+
				"         , C3.cg_descripcion "+
				"         , C3.fn_monto_operacion "+
				"         , C3.ig_numero_linea_credito "+
				"         , C3.ig_codigo_agencia_proy "+
				"         , C3.ig_codigo_sub_aplicacion_proy"+
				"         , C3.ig_numero_contrato "+
				"         , C3.cs_estatus "+
				"         , A.ic_tabla_amort"+
				"         , A.df_ppi"+
				"         , A.df_ppc"+
				"         , A.df_v_descuento "+
				"         , trunc(SYSDATE)"+
				"         , PS.ig_plazo"+
				"         , A.ic_pedido numero_electronico"+
				"         , I.ig_tipo_piso " +
				"         , TG.ic_tasa "+
				"         , (PS.cg_puntos - NVL(PS.FN_PUNTOS_PREF, 0)) as cg_puntos "+
				"         , T.ic_moneda "+
				"         , T.fg_sobretasa "   +
				"         , py.IN_NUMERO_TROYA " +
				"         , LC.IG_NUMERO_SOLIC_TROYA "+
				"         , C3.cg_proyectos_log " +
				"         , C3.cg_contratos_log " +
				"         , cp.ic_epo " + //foda 041-2005 (Se agrego la tabla com_pedido)
				"         , c3.cc_producto " +
				"         , to_char(c3.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"         , to_char(c3.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"         , to_char(c3.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"	  , TO_NUMBER(null) modalidad " +
				"	  , TO_NUMBER(null) fn_puntos_pref " +
				"	  , '' cg_rel_mat_pref " +
				"	  , 'Prestamos(Pedidos)' as OrigenQuery " +
				"    FROM com_pedido cp, " +
				"         com_controlant03 c3, " +
				"         com_anticipo a, " +
				"         com_pedido_seleccionado ps, " +
				"         com_linea_credito lc, " +
				"         comcat_if i, " +
				"         comcat_pyme py, " +
				"         com_tasa_general tg, " +
				"         comcat_tasa t " +
				"   WHERE c3.ic_pedido = a.ic_pedido " +
				"     AND cp.ic_pedido = ps.ic_pedido " +
				"     AND c3.cs_estatus = 'S' " +
				"     AND a.ic_pedido = ps.ic_pedido " +
				"     AND ps.ic_linea_credito = lc.ic_linea_credito " +
				"     AND lc.ic_if = i.ic_if " +
				"     AND lc.ic_pyme = py.ic_pyme " +
				"     AND ps.ic_tasa_general = tg.ic_tasa_general " +
				"     AND tg.ic_tasa = t.ic_tasa " +
				"     AND a.ic_estatus_antic = 2 " +
				"     AND a.ic_bloqueo = 3 " +
				"UNION " +
				"SELECT   C3.ic_pedido " +
				"         , C3.ig_codigo_agencia"+
				"         , C3.ig_codigo_sub_aplicacion "+
				"         , C3.ig_numero_linea_fondeo"+
				"         , C3.ig_codigo_financiera"+
				"         , C3.ig_aprobado_por "+
				"         , C3.cg_descripcion "+
				"         , C3.fn_monto_operacion "+
				"         , C3.ig_numero_linea_credito "+
				"         , C3.ig_codigo_agencia_proy "+
				"         , C3.ig_codigo_sub_aplicacion_proy"+
				"         , C3.ig_numero_contrato "+
				"         , C3.cs_estatus "+
				"         , A.ic_tabla_amort"+
				"         , A.df_ppi"+
				"         , A.df_ppc"+
				"         , A.df_v_descuento "+
				"         , trunc(SYSDATE)"+
				"         , PS.ig_plazo"+
				"         , A.ic_pedido numero_electronico"+
				"         , I.ig_tipo_piso " +
				"         , TG.ic_tasa "+
				"         , (PS.cg_puntos - NVL(PS.FN_PUNTOS_PREF, 0)) as cg_puntos "+
				"         , T.ic_moneda "+
				"         , T.fg_sobretasa "   +
				"         , py.IN_NUMERO_TROYA " +
				"         , LC.IG_NUMERO_SOLIC_TROYA "+
				"         , C3.cg_proyectos_log " +
				"         , C3.cg_contratos_log " +
				"         , cp.ic_epo " + //foda 041-2005 (Se agrego la tabla com_pedido)
//				"         , c3.cc_producto " +
				"         , '2A' cc_producto " +
				"         , to_char(c3.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				"         , to_char(c3.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				"         , to_char(c3.DF_CONTRATOS, 'dd/mm/yyyy') as DF_CONTRATOS " +
				"	  , 2 modalidad " +
				"	  , ps.fn_puntos_pref fn_puntos_pref " +
				"	  , ps.cg_rel_mat_pref cg_rel_mat_pref " +
				"	  , 'Prestamos(Fin Contratos)' as OrigenQuery " +
				"    FROM com_pedido cp, " +
				"         com_controlant03 c3, " +
				"         com_anticipo a, " +
				"         com_pedido_seleccionado ps, " +
				"         emp_linea_credito lc, " +
				"         comcat_if i, " +
				"         comcat_pyme py, " +
				"         com_tasa_general tg, " +
				"         comcat_tasa t " +
				"   WHERE c3.ic_pedido = a.ic_pedido " +
				"     AND cp.ic_pedido = ps.ic_pedido " +
				"     AND c3.cs_estatus = 'S' " +
				"     AND a.ic_pedido = ps.ic_pedido " +
				"     AND ps.ic_linea_credito_emp = lc.ic_linea_credito " +
				"     AND lc.ic_if = i.ic_if " +
				"     AND lc.ic_pyme = py.ic_pyme " +
				"     AND ps.ic_tasa_general = tg.ic_tasa_general " +
				"     AND tg.ic_tasa = t.ic_tasa " +
				"     AND a.ic_estatus_antic = 2 " +
				"     AND a.ic_bloqueo = 3 " +
				"ORDER BY ig_tipo_piso DESC ";
//System.out.println(query);
			try {
				rs=con.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Prestamos.java(query): " + query );
				throw sqle;
			}
			tipoPiso=0;

			numero_epo = 0;
			while(rs.next()) {
				errorTransaccion = false;
				errorDesconocido = false;
				diciembre="0";
				icMoneda = rs.getInt("IC_MONEDA");

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio=rs.getString("IC_PEDIDO");
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");
				//F084-2006
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=(rs.getString("IG_APROBADO_POR")==null)?null:"'"+rs.getString("IG_APROBADO_POR")+"'";
				descripcion=rs.getString("CG_DESCRIPCION");
				mto_operac=rs.getDouble("FN_MONTO_OPERACION");
				//F084-2006
				no_linea_credito=rs.getLong("IG_NUMERO_LINEA_CREDITO");
				cod_agencia_proy=rs.getInt("IG_CODIGO_AGENCIA_PROY");
				cod_sub_apli_proy=rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				estatus=rs.getString("CS_ESTATUS");
				valor_tasa_cartera= rs.getInt("IC_TASA");
				cod_tipo_amort=rs.getString("IC_TABLA_AMORT");		/* Valor 3) Codigo_Tipo_Amortizacion */
				no_contrato=rs.getInt("IG_NUMERO_CONTRATO");		/* Valor 4) Numero_Contrato */
				valor_inicial=rs.getDouble("FN_MONTO_OPERACION");	/* Valor 5) Valor_Inicial */
				fecha_ppi=rs.getDate("DF_PPI");						/* Valor 7) Fecha_Primer_Pago_Interes */
				fecha_ppc=rs.getDate("DF_PPC");						/* Valor 8) Fecha_Primer_Pago_Capital */
				fecha_venc=rs.getDate("DF_V_DESCUENTO");			/* Valor 23) Fecha_Vencimiento */
				fecha_adicion=rs.getDate(18);						/* Valor 32) Fecha_Adicion */
				no_electronico=rs.getLong("NUMERO_ELECTRONICO");		/* Valor 57) Numero_Electr�nico */
				ic_plazo=rs.getInt("IG_PLAZO");

				//tipo_interes = (ic_plazo > 30)?"C":"S";		//Si el plazo es mayor a 30 d�as es Compuesto de lo contrario es simple
				//periodo_capitalizacion = (tipo_interes.equals("C"))?1:0;
				dia_revision_tasa=Integer.parseInt(fecha_ppi.toString().substring(8,10));		/* Valor 42) Dia_Revision_Tasa */
				dia_pago_interes=Integer.parseInt(fecha_ppi.toString().substring(8,10));		/* Valor 40) Dia_Pago_Interes */
				dia_pago_capital=Integer.parseInt(fecha_ppc.toString().substring(8,10));		/* Valor 41) Dia_Pago_Capital */
				proyectosLog = (rs.getString("CG_PROYECTOS_LOG") == null)?"": rs.getString("CG_PROYECTOS_LOG");
				proyectosLog = Comunes.protegeCaracter(proyectosLog, '\'', "\'");
				contratosLog = (rs.getString("CG_CONTRATOS_LOG") == null)?"": rs.getString("CG_CONTRATOS_LOG");
				contratosLog = Comunes.protegeCaracter(contratosLog, '\'', "\'");

				numero_epo = rs.getInt("IC_EPO");

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");
				strFechaContratos = rs.getString("DF_CONTRATOS");

				modalidad = rs.getString("modalidad");

//					System.out.println("(3)"+cod_tipo_amort+"(4)"+no_contrato+"(5)"+valor_inicial+"(7)"+fecha_ppi+"(8)"+fecha_ppc+"(23)"+fecha_venc+"(32)"+fecha_adicion+"(40)"+dia_pago_interes+"(41)"+dia_pago_capital+"(57)"+no_electronico);
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Prestamos.java(paramStore): " + paramStore );
					throw sqle;
				}
				calcular_mora = "S";
				intereses_vencidos = "S";
				tipo_tasa=null;
				rela_mate_1=null;
				rela_mate_mora=null;
				fecha_revi_int=null;
				tipo_calculo_mora="1";
				tipo_tasa_int_mora = null;
				spread_corriente_1 = rs.getDouble("CG_PUNTOS");
				numero_cliente_troya = (rs.getString("IN_NUMERO_TROYA")==null)?"":rs.getString("IN_NUMERO_TROYA");
				numero_solic_troya = (rs.getString("IG_NUMERO_SOLIC_TROYA")==null)?"":rs.getString("IG_NUMERO_SOLIC_TROYA");

				if (tipoPiso==1) {
					//intereses_vencidos = "S";
					/*INICIO---MODIFICACION F039-2008*/
					if(modalidad!=null){
						spread_corriente_1 = rs.getDouble("fn_puntos_pref");/* Valor 17)  */
						rela_mate_1 = rs.getString("cg_rel_mat_pref");/* Valor 19)  */
					}else{
						spread_corriente_1 = rs.getDouble("CG_PUNTOS");
						rela_mate_1 = "+";
					}
					/*FIN---MODIFICACION F039-2008*/
					cs.setString(9, intereses_vencidos);
					tipo_tasa = "0";
					cs.setString(13, tipo_tasa);
					cs.setInt(15, 1);					/* Valor 15) Periodo_Tasa */
					cs.setInt(16, 2);					/* Valor 16) Spread_Mora */
					cs.setDouble(17, spread_corriente_1);	/* Valor 17) Spread_Corriente_1 */
					cs.setDouble(18, spread_corriente_2);		/* Valor 18) Spread_Corriente_2 */
					//rela_mate_1 = "+";MODIFICACAION F039-2008
					cs.setString(19, rela_mate_1);
					cs.setNull(20, Types.VARCHAR);
					rela_mate_mora = "*";
					cs.setString(21, rela_mate_mora);
					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();
					//fecha_apertura=fecha_revi_int;		/* Valor 25) Fecha_Apertura */

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());
					cs.setInt(36, valor_tasa_cartera);	/* Valor 36) Codigo_Valor_Tasa_Mora */
					cs.setInt(37, 2);					/* Valor 37) Tipo_Frecuencia_Revision */
					cs.setNull(51, Types.DOUBLE);		/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;
					cs.setInt(54, periodo_capitalizacion);
					cs.setInt(63, 2);					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					cs.setInt(64, 1);					/* Valor 64) Frecuencia_Rev_Int_Mora */
					cs.setInt(65, dia_pago_interes);	/* Valor 65) Dia_Revision_Tasa_Mora */
					cs.setDate(66, fecha_revi_int,Calendar.getInstance());					/* Valor 66) Fecha_revision_mora */
					tipo_tasa_int_mora = "5";
					cs.setString(67, tipo_tasa_int_mora);			/* Valor 67) Tipo_Tasa_Int_Mora */
				} else {
					cs.setString(9, intereses_vencidos);
					cs.setString(13, tipo_tasa);
					/* Valor 15) Periodo_Tasa */
					if (icMoneda==54) {
						cs.setInt(15, 1);
					} else {
						cs.setNull(15, Types.INTEGER);
					}
					cs.setNull(16, Types.INTEGER);				/* Valor 16) Spread_Mora */
					/* Valor 17) Spread_Corriente_1 */
					if (icMoneda==54) {
						spread_corriente_1 = rs.getDouble("FG_SOBRETASA");
						cs.setDouble(17, spread_corriente_1);
						rela_mate_1 = "+";
						cs.setString(19, rela_mate_1);
					} else {
						cs.setNull(17, Types.DOUBLE);
						rela_mate_1 = null;
						cs.setString(19, rela_mate_1);
					}

					cs.setNull(18, Types.DOUBLE);				/* Valor 18) Spread_Corriente_2 */
					cs.setString(20, rela_mate_2);
					rela_mate_mora = (icMoneda==54)?"*":null;
					cs.setString(21, rela_mate_mora);

					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());
					if (icMoneda==54) {
						cs.setNull(36, Types.INTEGER);
					} else {
						cs.setInt(36, 20);
					}
					cs.setNull(37, Types.INTEGER);				/* Valor 37) Tipo_Frecuencia_Revision */
					cs.setNull(51, Types.DOUBLE);				/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;
					cs.setInt(54, periodo_capitalizacion);
					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					/* Valor 64) Frecuencia_Rev_Int_Mora */
					/* Valor 65) Dia_Revision_Tasa_Mora */
					/* Valor 66) Fecha_revision_mora */
					if (icMoneda==54) {
						cs.setInt(63, 2);
						//cs.setInt(64, 1);
						cs.setNull(64, Types.INTEGER);
						cs.setInt(65, dia_pago_interes);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
					} else {
						cs.setNull(63, Types.INTEGER);
						cs.setNull(64, Types.INTEGER);
						cs.setNull(65, Types.INTEGER);
						cs.setNull(66, Types.DATE);
					}
					tipo_tasa_int_mora = null;
					cs.setString(67, tipo_tasa_int_mora);		/* Valor 67) Tipo_Tasa_Int_Mora */
				}

				// Foda 17 2005 JRFH
				int diaFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(8,10));
				int mesFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(5,7));
				int diaFechaPPC =  Integer.parseInt(fecha_ppc.toString().substring(8,10));

				int diaFechaVenc = Integer.parseInt(fecha_venc.toString().substring(8,10));
				int mesFechaVenc = Integer.parseInt(fecha_venc.toString().substring(5,7));

				int ultimoDiaMesPPI = 0;
				if(mesFechaPPI==2){
					Calendar calPPI = new GregorianCalendar();
					calPPI.setTime(fecha_ppi);
					ultimoDiaMesPPI = calPPI.getActualMaximum(Calendar.DAY_OF_MONTH);
				}

				if (diaFechaPPI == 30 || diaFechaPPI == 31 || (mesFechaPPI==2&&diaFechaPPI==ultimoDiaMesPPI)) {
					int ultimoDiaDelMes = 0;
					if (mesFechaVenc == 2) {
						Calendar cal = new GregorianCalendar();
						cal.setTime(fecha_venc);
						ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					}

					if (diaFechaPPI==30&&(mesFechaVenc != 2 && diaFechaVenc == 30) || (mesFechaVenc == 2 && diaFechaVenc == ultimoDiaDelMes )) {
						dia_revision_tasa = dia_pago_interes = dia_pago_capital =30;
					} else {
						dia_revision_tasa = dia_pago_interes = dia_pago_capital =31;
					}
				}
				//fin foda 17

				cs.setNull(1, Types.INTEGER);				/* Valor 1) Numero_Prestamo */
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, valor_tasa_cartera);
				cs.setString(3, cod_tipo_amort);
				cs.setInt(4, no_contrato);
				cs.setDouble(5, valor_inicial);
				cs.setString(6, calcular_mora);
				cs.setDate(7, fecha_ppi,Calendar.getInstance());
				cs.setDate(8, fecha_ppc,Calendar.getInstance());
				cs.setString(10, intereses_normales);
				cs.setInt(11, frecuencia_interes);
				cs.setInt(12, frecuencia_capital);
				cs.setString(14, clase_tasa);
				cs.setString(22, observaciones);
				cs.setDate(23, fecha_venc,Calendar.getInstance());
				cs.setDate(25, fecha_apertura,Calendar.getInstance());
				cs.setDouble(26, comision);
				cs.setString(27, diciembre);
				cs.setInt(28, dias_gracia_mora);
				cs.setString(29, tipo_liqui_mes);
				cs.setString(30, tipo_liqui_ano);
				cs.setString(31, adicionado_por);
				cs.setDate(32, fecha_adicion,Calendar.getInstance());
				cs.setNull(33, Types.INTEGER);				/* Valor 33) Tipo_Frecuencia_Capital */
				cs.setNull(34, Types.INTEGER);				/* Valor 34) Tipo_Frecuencia_Interes */
				cs.setString(35, cobrar_iva);
				cs.setString(38, rela_mate_3);
				cs.setDouble(39, spread_corriente_3);		/* Valor 39) Spread_Corriente_3 */
				cs.setInt(40, dia_pago_interes);
				cs.setInt(41, dia_pago_capital);
				cs.setInt(42, dia_revision_tasa);
				cs.setString(43, tipo_redondeo);
				cs.setString(44, tipo_calculo_mora);
				if (icMoneda==54){
					cs.setNull(45, Types.INTEGER);
				} else {
					cs.setInt(45, rec_frescos);
				}
				cs.setString(46, amort_ult_prim);
				cs.setNull(47, Types.DOUBLE);				/* Valor 47) Monto_Amortizacion_Ajuste */
				cs.setString(48, forma_redondeo);
				cs.setString(49, desgloce_tasas);
				cs.setString(50, cobro_int_ES);
				cs.setString(52, clausula_venci);
				cs.setString(55, valor_presente);
				cs.setNull(56, Types.INTEGER);				/* Valor 56) Codigo_Comision */
				cs.setLong(57, no_electronico);
				cs.setString(58, pago_masivo);
				cs.setString(59, tipo_renta);
				cs.setInt(60, no_cuotas_recortadas);
				cs.setInt(61, tipo_mora);
				cs.setNull(62, Types.INTEGER);				/* Valor 62) Codigo_Grupo_Comisi�n */

				if(numero_cliente_troya.equals("") || numero_solic_troya.equals("") ){ //FODA 037-2005
					cs.setNull(68,Types.VARCHAR);	/* Valor 68) Numero Cliente Troya */
					cs.setNull(69,Types.VARCHAR);	/* Valor 69) N�mero Sol Troya Nafin */
				}else{
					//FODA 099 - Interfase N@E-Troya
					cs.setString(68, numero_cliente_troya);	/* Valor 68) Numero Cliente Troya */
					cs.setString(69, numero_solic_troya);	/* Valor 69) N�mero Sol Troya Nafin */
				}

				//cs.setNull(70, Types.INTEGER);				/* Valor 70) Numero_epo */
				cs.setInt(70, numero_epo);	//foda 041-2005

				if(modalidad!=null)
					cs.setInt(71, Integer.parseInt(modalidad));					/* Valor 71) modalidad nafin empresarial */
				else
					cs.setNull(71, Types.INTEGER);

				cs.setNull(72, Types.INTEGER);				/* Valor 71) Estatus */
				cs.registerOutParameter(72, Types.INTEGER);

				cadena="P1:=null;"+"\n"+
					"P2:="+valor_tasa_cartera+";\n"+
					"P3:='"+cod_tipo_amort+"';"+"\n"+
					"P4:="+no_contrato+";"+"\n"+
					"P5:="+valor_inicial+";"+"\n"+
					"P6:='"+calcular_mora+"';"+"\n"+
					"P7:=to_date('"+fecha_ppi+"','yyyy-mm-dd');"+"\n"+
					"P8:=to_date('"+fecha_ppc+"','yyyy-mm-dd');"+"\n"+
					"P9:='"+intereses_vencidos+"';"+"\n"+
					"P10:='"+intereses_normales+"';"+"\n"+
					"P11:="+frecuencia_interes+";"+"\n"+
					"P12:="+frecuencia_capital+";"+"\n"+
					"P13:="+tipo_tasa+";"+"\n"+
					"P14:='"+clase_tasa+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P15:=1;"+"\n"+
						"P16:=2;"+"\n"+
						"P17:="+spread_corriente_1+";"+"\n"+
						"P18:="+spread_corriente_2+";"+"\n"+
						"P19:='"+rela_mate_1+"';"+"\n"+
						"P20:="+rela_mate_2+";"+"\n"+
						"P21:='"+rela_mate_mora+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "P15:=1;"+"\n"+
							"P16:=null;"+"\n"+
							"P17:="+spread_corriente_1+";"+"\n"+
							"P18:=null;"+"\n"+
							"P19:='"+rela_mate_1+";"+"'\n"+
							"P20:="+rela_mate_2+";"+"\n"+
							"P21:='"+rela_mate_mora+"';"+"\n";
					} else {
						cadena += "P15:=null;"+"\n"+
							"P16:=null;"+"\n"+
							"P17:=null;"+"\n"+
							"P18:=null;"+"\n"+
							"P19:="+rela_mate_1+";"+"\n"+
							"P20:="+rela_mate_2+";"+"\n"+
							"P21:="+rela_mate_mora+";"+"\n";
					}
				}

				cadena += 	"P22:='"+observaciones+"';"+"\n"+
					"P23:=to_date('"+fecha_venc+"','yyyy-mm-dd');"+"\n";
				if (tipoPiso==1) {
					cadena += "P24:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n";
				} else {
					cadena += "P24:="+fecha_revi_int+";"+"\n";
				}
				cadena += "P25:="+fecha_apertura+";"+"\n"+
					"P26:="+comision+";"+"\n"+
					"P27:='"+diciembre+"';"+"\n"+
					"P28:="+dias_gracia_mora+";"+"\n"+
					"P29:='"+tipo_liqui_mes+"';"+"\n"+
					"P30:='"+tipo_liqui_ano+"';"+"\n"+
					"P31:='"+adicionado_por+"';"+"\n"+
					"P32:=to_date('"+fecha_adicion+"','yyyy-mm-dd');"+"\n"+
					"P33:=null;"+"\n"+
					"P34:=null;"+"\n"+
					"P35:='"+cobrar_iva+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P36:="+valor_tasa_cartera+";"+"\n"+
						"P37:=2;"+"\n";
				} else {
					//cadena += "P36:=null;"+"\n"+
					//	"P37:=null;"+"\n";
					if (icMoneda==54){
						cadena += "P36:=null;"+"\n"+
							"P37:=null;"+"\n";
					} else {
						cadena += "P36:=20;"+"\n"+
							"P37:=null;"+"\n";
					}

				}
				cadena += "P38:="+rela_mate_3+";"+"\n"+
					"P39:="+spread_corriente_3+";"+"\n"+
					"P40:="+dia_pago_interes+";"+"\n"+
					"P41:="+dia_pago_capital+";"+"\n"+
					"P42:="+dia_revision_tasa+";"+"\n"+
					"P43:='"+tipo_redondeo+"';"+"\n"+
					"P44:='"+tipo_calculo_mora+"';"+"\n";
				if (icMoneda==54){
					cadena += "P45:=null;"+"\n";
				} else {
					cadena += "P45:="+rec_frescos+";"+"\n";
				}

				cadena += "P46:="+amort_ult_prim+";"+"\n"+
					"P47:=null;"+"\n"+
					"P48:='"+forma_redondeo+"';"+"\n"+
					"P49:="+desgloce_tasas+";"+"\n"+
					"P50:='"+cobro_int_ES+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P51:=null;"+"\n";
				} else {
					cadena += "P51:=null;"+"\n";
				}
				cadena += "P52:='"+clausula_venci+"';"+"\n"+
					"P53:='"+tipo_interes+"';"+"\n"+
					"P54:="+periodo_capitalizacion+";"+"\n"+
					"P55:='"+valor_presente+"';"+"\n"+
					"P56:=null;"+"\n"+
					"P57:="+no_electronico+";"+"\n"+
					"P58:='"+pago_masivo+"';"+"\n"+
					"P59:="+tipo_renta+";"+"\n"+
					"P60:="+no_cuotas_recortadas+";"+"\n"+
					"P61:="+tipo_mora+";"+"\n"+
					"P62:=null;"+"\n";
				if (tipoPiso==1) {
					cadena += "P63:=2;"+"\n"+
						"P64:=1;"+"\n"+
						"P65:="+dia_pago_interes+";"+"\n"+
					    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n"+
						"P67:='"+tipo_tasa_int_mora+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "P63:=2;"+"\n"+
							"P64:=null;"+"\n"+
							"P65:="+dia_pago_interes+";"+"\n"+
						    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n"+
							"P67:='"+tipo_tasa_int_mora+"';"+"\n";

					} else {
						cadena += "P63:=null;"+"\n"+
							"P64:=null;"+"\n"+
							"P65:=null;"+"\n"+
							"P66:=null;"+"\n"+
							"P67:="+tipo_tasa_int_mora+";"+"\n";
					}
				}

				if(numero_cliente_troya.equals("") || numero_solic_troya.equals("") ){ //FODA 037-2005
					cadena += "P68:=null;"+"\n"+
							"P69:=null;"+"\n";
				}else{
					cadena += "P68:="+numero_cliente_troya+";"+"\n"+
							"P69:="+numero_solic_troya+";"+"\n";
				}
				cadena +=
						"P70:="+numero_epo+";\n"+ //foda 041-2005
						"P71:="+modalidad+";\n"+
						"P72:=null;"+"\n";
						 //"P70:=null;"+"\n";


				System.out.println(cadena);

				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						no_prestamo = cs.getInt(1);
						status = cs.getString(72);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle) {
					sqle.printStackTrace();
					System.out.println(sqle);
					errorDesconocido = true;
					status="9";
				}

				System.out.println("No Prestamo: "+no_prestamo+" Estatus: "+status);
				cadena += "No Prestamo: "+no_prestamo+" Estatus: "+status;

									/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					con.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PR'";
						try {
							rs1=con.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Prestamos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from com_control05 where ic_folio='"+folio+"'";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); con.cierraStatement();

					if(no_doctos==0) {
						queri = "insert into com_controlant05(ic_pedido,cc_codigo_aplicacion,ic_error_proceso, "+
							"df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por, "+
							"ig_numero_linea_credito,ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy "+
							", ig_proceso_numero, cc_producto, df_diversos, df_proyectos, df_contratos)"+
							"values("+folio+",'"+codigo_aplicacion+"',"+status+" "+
							",SYSDATE,"+cod_agen+","+cod_subapli+","+no_lineafondeo+" "+
							","+cod_finan+","+aprobado_por+","+no_linea_credito+" "+
							","+cod_agencia_proy+","+cod_sub_apli_proy+",3 " +
							" , '" + strProducto + "', to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), to_date('" + strFechaProyectos + "', 'dd/mm/yyyy'), to_date('" + strFechaContratos + "', 'dd/mm/yyyy') )";
					} else {
						queri = "update com_controlant05 set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
							",ic_error_proceso="+status+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+cod_agen+" "+
							",ig_codigo_sub_aplicacion="+cod_subapli+" ,ig_numero_linea_fondeo="+no_lineafondeo+" "+
							",ig_codigo_financiera="+cod_finan+" ,ig_aprobado_por="+aprobado_por+" "+
							",ig_numero_linea_credito="+no_linea_credito+" ,ig_codigo_agencia_proy="+cod_agencia_proy+" "+
							",ig_codigo_sub_aplicacion_proy="+cod_sub_apli_proy+
							" , ig_proceso_numero=3 "+
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" , df_contratos = to_date('" + strFechaContratos + "', 'dd/mm/yyyy')" +
							"where ic_pedido="+folio;
					}
//						System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
						queri = "update com_controlant03 set df_proceso = sysdate, cs_estatus='N' " +
						" , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
						" where ic_pedido="+folio;
//						System.out.println(queri);
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Actualizar el folio en ControlAnt03.");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en ControlAnt05.");
						errorTransaccion = true;
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de controlAnt03 a com_controlAnt04 */
					queri = "insert into com_controlAnt04(ic_pedido,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
						" ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
						" fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
						" ig_codigo_sub_aplicacion_proy,ig_numero_contrato,ig_numero_prestamo,cs_estatus, "+
						" cg_proyectos_log, cg_contratos_log, cg_prestamos_log "+
						", cc_producto, df_diversos, df_proyectos, df_contratos, df_prestamos) "+
						"values("+folio+","+cod_agen+","+cod_subapli+","+
						""+no_lineafondeo+","+cod_finan+","+aprobado_por+",'"+descripcion+"',"+
						""+mto_operac+","+no_linea_credito+","+cod_agencia_proy+","+
						""+cod_sub_apli_proy+","+no_contrato+","+no_prestamo+",'"+estatus+"'" +
						", '"+proyectosLog+"','"+contratosLog+"','"+ Comunes.protegeCaracter(cadena, '\'', "\'") +"'" +
						" , '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), to_date('"+strFechaProyectos+"', 'dd/mm/yyyy'), to_date('"+strFechaContratos+"', 'dd/mm/yyyy'), SYSDATE)";
//System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
						/* Borramos el Documento de controlAnt03 si esta Bien. */
						queri = "delete com_controlAnt03 where ic_pedido="+folio;
//System.out.println(queri);
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Borrar los Datos en com_controlAnt03");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar los Datos en com_controlAnt04");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//while
			if (cs !=null) cs.close();
			rs.close(); con.cierraStatement();

			System.out.println(" Fin Operaciones Financiamiento a Pedidos");
                        }
			/* Fin Interfase Financiamiento a Pedidos */

			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////
			////I n i c i o   I n t e r f a s e   1 e r   P i s o    C r e d i c a d e n a s     ////
			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////

			if(icPausaInterfazDsctoe.equals("N")){
			/* Inicio Interfase 1er Piso Credicadenas */
			System.out.println(" Inicio Operaciones 1er Piso Credicadenas");

			query =
				" select i.cc_disposicion"   +
				" , i.ig_codigo_agencia"   +
				" , i.ig_codigo_sub_aplicacion"   +
				" , i.ig_numero_linea_fondeo"   +
				" , i.ig_codigo_financiera"   +
				" , i.ig_aprobado_por"   +
				" , i.cg_descripcion"   +
				" , i.fn_monto_operacion"   +
				" , i.ig_numero_linea_credito"   +
				" , i.ig_codigo_agencia_proy"   +
				" , i.ig_codigo_sub_aplicacion_proy"   +
				" , i.ig_numero_contrato"   +
				" , s.ic_tabla_amort"   +
				" , d.df_proximo_pago as df_ppi"   +
				" , decode(S.IC_TABLA_AMORT, 2, d.df_vencimiento, d.df_proximo_pago) as df_ppc"   +
				//" , d.df_proximo_pago as df_ppc"   +
				" , d.df_vencimiento"   +
				" , trunc(SYSDATE) as hoy"   +
				" , pl.in_plazo_dias as ig_plazo"   +
				" , i.cc_disposicion numero_electronico"   +
				" , I.ig_tipo_piso"   +
				" , d.ic_tasa"   +
				" , d.fn_puntos as CG_PUNTOS"   +
				" , T.ic_moneda"   +
				" , T.fg_sobretasa"   +
				" , DECODE(s.IC_TABLA_AMORT, 2, null, (decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null))) as amort_ult_prim"   +
				//" , decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null) as amort_ult_prim"   +
				" , py.IN_NUMERO_TROYA " +
				" , LC.IG_NUMERO_SOLIC_TROYA "+
				" , d.IC_EPO "+ //foda 041-2005
				"	  , TO_NUMBER(null) modalidad " +
				" , TO_NUMBER(null) fn_puntos_pref "+
				" , '' cg_rel_mat_pref "+
				"	  , 'Prestamos(Credicadenas)' as OrigenQuery " +
				"  FROM inv_interfase i"   +
				" , inv_solicitud s"   +
				" , inv_disposicion d"   +
				" , com_linea_credito LC"   +
				" , comcat_pyme py"   +
				" , comcat_if I"   +
				" , comcat_tasa T"   +
				" , comcat_plazo pl"   +
				"  WHERE i.cc_disposicion = s.cc_disposicion"   +
				"  and s.cc_disposicion = d.cc_disposicion"   +
				"  and d.ic_linea_credito = LC.ic_linea_credito"   +
				"  and LC.ic_if = I.ic_if"   +
				"  and LC.ic_pyme = py.ic_pyme "+
				"  and d.ic_tasa = T.ic_tasa"   +
				"  and d.ic_plazo = pl.ic_plazo"   +
				"  AND i.ig_numero_prestamo is null"   +
				"  AND i.ig_numero_contrato is not null"   +
				"  AND i.ig_numero_linea_credito is not null"   +
				"  AND i.cc_codigo_aplicacion is null"   +
				"  AND i.ic_error_proceso is null"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_bloqueo = 3"   +
				" UNION "   +
				" select i.cc_disposicion"   +
				" , i.ig_codigo_agencia"   +
				" , i.ig_codigo_sub_aplicacion"   +
				" , i.ig_numero_linea_fondeo"   +
				" , i.ig_codigo_financiera"   +
				" , i.ig_aprobado_por"   +
				" , i.cg_descripcion"   +
				" , i.fn_monto_operacion"   +
				" , i.ig_numero_linea_credito"   +
				" , i.ig_codigo_agencia_proy"   +
				" , i.ig_codigo_sub_aplicacion_proy"   +
				" , i.ig_numero_contrato"   +
				" , s.ic_tabla_amort"   +
				" , d.df_proximo_pago as df_ppi"   +
				" , decode(S.IC_TABLA_AMORT, 2, d.df_vencimiento, d.df_proximo_pago) as df_ppc"   +
				//" , d.df_proximo_pago as df_ppc"   +
				" , d.df_vencimiento"   +
				" , trunc(SYSDATE) as hoy"   +
				" , pl.in_plazo_dias as ig_plazo"   +
				" , i.cc_disposicion numero_electronico"   +
				" , I.ig_tipo_piso"   +
				" , d.ic_tasa"   +
				" , d.fn_puntos as CG_PUNTOS"   +
				" , T.ic_moneda"   +
				" , T.fg_sobretasa"   +
				" , DECODE(s.IC_TABLA_AMORT, 1, null, 2, null, 'U') as amort_ult_prim"   +
//				" , DECODE(s.IC_TABLA_AMORT, 2, null, (decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null))) as amort_ult_prim"   +
				//" , decode(d.ig_tipo_calculo_interes, 2, 'U', 1, null) as amort_ult_prim"   +
				" , py.IN_NUMERO_TROYA " +
				" , LC.IG_NUMERO_SOLIC_TROYA "+
				" , d.IC_EPO "+ //foda 041-2005
				"	  , decode(d.cg_exp_emp,'S', 4, 1 ) modalidad " +
				" , d.fn_puntos_pref fn_puntos_pref"+
				" , d.cg_rel_mat_pref cg_rel_mat_pref"+
				"	  , 'Prestamos(Lib Disp - Fin Exp)' as OrigenQuery " +
				"  FROM inv_interfase i"   +
				" , inv_solicitud s"   +
				" , inv_disposicion d"   +
				" , emp_linea_credito LC"   +
				" , comcat_pyme py"   +
				" , comcat_if I"   +
				" , comcat_tasa T"   +
				" , comcat_plazo pl"   +
				"  WHERE i.cc_disposicion = s.cc_disposicion"   +
				"  and s.cc_disposicion = d.cc_disposicion"   +
				"  and d.ic_linea_credito_emp = LC.ic_linea_credito"   +
				"  and LC.ic_if = I.ic_if"   +
				"  and LC.ic_pyme = py.ic_pyme "+
				"  and d.ic_tasa = T.ic_tasa"   +
				"  and d.ic_plazo = pl.ic_plazo"   +
				"  AND i.ig_numero_prestamo is null"   +
				"  AND i.ig_numero_contrato is not null"   +
				"  AND i.ig_numero_linea_credito is not null"   +
				"  AND i.cc_codigo_aplicacion is null"   +
				"  AND i.ic_error_proceso is null"   +
				"  AND s.ic_estatus_solic = 2"   +
				"  AND s.ic_bloqueo = 3"   +
				"  ORDER BY ig_tipo_piso desc"  ;
//System.out.println(query);
			try {
				rs=con.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Prestamos.java(query): " + query );
				throw sqle;
			}
			tipoPiso=0;


			numero_epo = 0;
			while(rs.next()) {
				errorTransaccion = false;
				errorDesconocido = false;
				diciembre="0";
				monto_amort_ajuste=0;
				icMoneda = rs.getInt("IC_MONEDA");

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio=rs.getString("CC_DISPOSICION");
				cod_agen=rs.getInt("IG_CODIGO_AGENCIA");
				cod_subapli=rs.getInt("IG_CODIGO_SUB_APLICACION");
				//F084-2006
				no_lineafondeo=rs.getLong("IG_NUMERO_LINEA_FONDEO");
				cod_finan=rs.getInt("IG_CODIGO_FINANCIERA");
				aprobado_por=(rs.getString("IG_APROBADO_POR")==null)?null:"'"+rs.getString("IG_APROBADO_POR")+"'";
				descripcion=rs.getString("CG_DESCRIPCION");
				mto_operac=rs.getDouble("FN_MONTO_OPERACION");
				//F084-2006
				no_linea_credito=rs.getLong("IG_NUMERO_LINEA_CREDITO");
				cod_agencia_proy=rs.getInt("IG_CODIGO_AGENCIA_PROY");
				cod_sub_apli_proy=rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				valor_tasa_cartera= rs.getInt("IC_TASA");
				cod_tipo_amort=rs.getString("IC_TABLA_AMORT");		/* Valor 3) Codigo_Tipo_Amortizacion */
				no_contrato=rs.getInt("IG_NUMERO_CONTRATO");		/* Valor 4) Numero_Contrato */
				valor_inicial=rs.getDouble("FN_MONTO_OPERACION");	/* Valor 5) Valor_Inicial */
				fecha_ppi=rs.getDate("DF_PPI");						/* Valor 7) Fecha_Primer_Pago_Interes */
//System.out.println("fecha_ppi: "+fecha_ppi);
				fecha_ppc=rs.getDate("DF_PPC");						/* Valor 8) Fecha_Primer_Pago_Capital */
//System.out.println("fecha_ppc: "+fecha_ppc);
				fecha_venc=rs.getDate("DF_VENCIMIENTO");			/* Valor 23) Fecha_Vencimiento */
				fecha_adicion=rs.getDate("HOY");					/* Valor 32) Fecha_Adicion */
				no_electronico=rs.getLong("NUMERO_ELECTRONICO");		/* Valor 57) Numero_Electr�nico */
				ic_plazo=rs.getInt("IG_PLAZO");
				//tipo_interes = (ic_plazo > 30)?"C":"S";		//Si el plazo es mayor a 30 d�as es Compuesto de lo contrario es simple
				//periodo_capitalizacion = (tipo_interes.equals("C"))?1:0;
				dia_revision_tasa=Integer.parseInt(fecha_ppi.toString().substring(8,10));		/* Valor 42) Dia_Revision_Tasa */
				dia_pago_interes=Integer.parseInt(fecha_ppi.toString().substring(8,10));		/* Valor 40) Dia_Pago_Interes */
				dia_pago_capital=Integer.parseInt(fecha_ppc.toString().substring(8,10));		/* Valor 41) Dia_Pago_Capital */
				amort_ult_prim = rs.getString("AMORT_ULT_PRIM");
				numero_epo=rs.getInt("IC_EPO");;
				//System.out.println("(3)"+cod_tipo_amort+"(4)"+no_contrato+"(5)"+valor_inicial+"(7)"+fecha_ppi+"(8)"+fecha_ppc+"(23)"+fecha_venc+"(32)"+fecha_adicion+"(40)"+dia_pago_interes+"(41)"+dia_pago_capital+"(57)"+no_electronico);
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Prestamos.java(paramStore): " + paramStore );
					throw sqle;
				}
				calcular_mora = "S";
				intereses_vencidos = "S";
				tipo_tasa=null;
				rela_mate_1=null;
				rela_mate_mora=null;
				fecha_revi_int=null;
				tipo_calculo_mora="1";
				tipo_tasa_int_mora = null;
				spread_corriente_1 = rs.getDouble("CG_PUNTOS");
				numero_cliente_troya = (rs.getString("IN_NUMERO_TROYA")==null)?"":rs.getString("IN_NUMERO_TROYA");
				numero_solic_troya = (rs.getString("IG_NUMERO_SOLIC_TROYA")==null)?"":rs.getString("IG_NUMERO_SOLIC_TROYA");

				modalidad = rs.getString("modalidad");


				if (tipoPiso==1) {
					//intereses_vencidos = "S";
					/*INICIO---MODIFICACION F039-2008*/
					if(modalidad!=null){
						spread_corriente_1 = rs.getDouble("fn_puntos_pref");/* Valor 17)  */
						rela_mate_1 = rs.getString("cg_rel_mat_pref");/* Valor 19)  */
					}else{
						spread_corriente_1 = rs.getDouble("CG_PUNTOS");
						rela_mate_1 = "+";
					}
					/*FIN---MODIFICACION F039-2008*/

					cs.setString(9, intereses_vencidos);
					tipo_tasa = "0";
					cs.setString(13, tipo_tasa);
					cs.setInt(15, 1);					/* Valor 15) Periodo_Tasa */
					cs.setInt(16, 2);					/* Valor 16) Spread_Mora */
/*
System.out.println("\n modalidad: "+modalidad);
					if("1".equals(modalidad)) {
						spread_corriente_1 = 6.0;
					} else if("4".equals(modalidad)) {
						spread_corriente_1 = 4.5;
					}
*/

					cs.setDouble(17, spread_corriente_1);						/* Valor 17) Spread_Corriente_1 */
					if("1".equals(modalidad)||"4".equals(modalidad)) {
						cs.setNull(18, Types.DOUBLE);
					} else {
						cs.setDouble(18, spread_corriente_2);					/* Valor 18) Spread_Corriente_2 */
					}

					//rela_mate_1 = "+";MODIFICACION F039-2008
					rela_mate_mora = "*";

					cs.setString(19, rela_mate_1);								/* Valor 19) Relacion Mat 1 */
					cs.setNull(20, Types.VARCHAR);								/* Valor 20) Relacion Mat 2 */
					cs.setString(21, rela_mate_mora);							/* Valor 21) Relacion Mat Mora */

					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();
					//fecha_apertura=fecha_revi_int;		/* Valor 25) Fecha_Apertura */

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());
					cs.setInt(36, valor_tasa_cartera);					/* Valor 36) Codigo_Valor_Tasa_Mora */
					cs.setInt(37, 2);					/* Valor 37) Tipo_Frecuencia_Revision */
					cs.setNull(51, Types.DOUBLE);		/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;
					cs.setInt(54, periodo_capitalizacion);
					cs.setInt(63, 2);					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					cs.setInt(64, 1);					/* Valor 64) Frecuencia_Rev_Int_Mora */
					cs.setInt(65, dia_pago_interes);	/* Valor 65) Dia_Revision_Tasa_Mora */
					cs.setDate(66, fecha_revi_int,Calendar.getInstance());					/* Valor 66) Fecha_revision_mora */
					tipo_tasa_int_mora = "5";
					cs.setString(67, tipo_tasa_int_mora);			/* Valor 67) Tipo_Tasa_Int_Mora */
				} else {
					cs.setString(9, intereses_vencidos);
					cs.setString(13, tipo_tasa);
					/* Valor 15) Periodo_Tasa */
					if (icMoneda==54) {
						cs.setInt(15, 1);
					} else {
						cs.setNull(15, Types.INTEGER);
					}
					cs.setNull(16, Types.INTEGER);				/* Valor 16) Spread_Mora */
					/* Valor 17) Spread_Corriente_1 */
					if (icMoneda==54) {
						spread_corriente_1 = rs.getDouble("FG_SOBRETASA");
						cs.setDouble(17, spread_corriente_1);
						rela_mate_1 = "+";
						cs.setString(19, rela_mate_1);
					} else {
						cs.setNull(17, Types.DOUBLE);
						rela_mate_1 = null;
						cs.setString(19, rela_mate_1);
					}

					cs.setNull(18, Types.DOUBLE);				/* Valor 18) Spread_Corriente_2 */
					cs.setString(20, rela_mate_2);
					rela_mate_mora = (icMoneda==54)?"*":null;
					cs.setString(21, rela_mate_mora);

					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());
					if (icMoneda==54) {
						cs.setNull(36, Types.INTEGER);
					} else {
						cs.setInt(36, 20);
					}
					cs.setNull(37, Types.INTEGER);				/* Valor 37) Tipo_Frecuencia_Revision */
					cs.setNull(51, Types.DOUBLE);				/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;
					cs.setInt(54, periodo_capitalizacion);
					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					/* Valor 64) Frecuencia_Rev_Int_Mora */
					/* Valor 65) Dia_Revision_Tasa_Mora */
					/* Valor 66) Fecha_revision_mora */
					if (icMoneda==54) {
						cs.setInt(63, 2);
						//cs.setInt(64, 1);
						cs.setNull(64, Types.INTEGER);
						cs.setInt(65, dia_pago_interes);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
					} else {
						cs.setNull(63, Types.INTEGER);
						cs.setNull(64, Types.INTEGER);
						cs.setNull(65, Types.INTEGER);
						cs.setNull(66, Types.DATE);
					}
					tipo_tasa_int_mora = null;
					cs.setString(67, tipo_tasa_int_mora);		/* Valor 67) Tipo_Tasa_Int_Mora */
				}
				// Foda 17 2005 JRFH
				int diaFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(8,10));
				int mesFechaPPI = Integer.parseInt(fecha_ppi.toString().substring(5,7));
				int diaFechaPPC =  Integer.parseInt(fecha_ppc.toString().substring(8,10));

				int diaFechaVenc = Integer.parseInt(fecha_venc.toString().substring(8,10));
				int mesFechaVenc = Integer.parseInt(fecha_venc.toString().substring(5,7));

				int ultimoDiaMesPPI = 0;
				if(mesFechaPPI==2){
					Calendar calPPI = new GregorianCalendar();
					calPPI.setTime(fecha_ppi);
					ultimoDiaMesPPI = calPPI.getActualMaximum(Calendar.DAY_OF_MONTH);
				}

				if (diaFechaPPI == 30 || diaFechaPPI == 31 || (mesFechaPPI==2&&diaFechaPPI==ultimoDiaMesPPI)) {
					int ultimoDiaDelMes = 0;
					if (mesFechaVenc == 2) {
						Calendar cal = new GregorianCalendar();
						cal.setTime(fecha_venc);
						ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					}

					if (diaFechaPPI==30&&(mesFechaVenc != 2 && diaFechaVenc == 30) || (mesFechaVenc == 2 && diaFechaVenc == ultimoDiaDelMes )) {
						dia_revision_tasa = dia_pago_interes = dia_pago_capital =30;
					} else {
						dia_revision_tasa = dia_pago_interes = dia_pago_capital =31;
					}
				}
				//fin foda 17

				cs.setNull(1, Types.INTEGER);				/* Valor 1) Numero_Prestamo */
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, valor_tasa_cartera);
				cs.setString(3, cod_tipo_amort);
				cs.setInt(4, no_contrato);
				cs.setDouble(5, valor_inicial);
				cs.setString(6, calcular_mora);
				cs.setDate(7, fecha_ppi,Calendar.getInstance());
				cs.setDate(8, fecha_ppc,Calendar.getInstance());
				cs.setString(10, intereses_normales);
				cs.setInt(11, frecuencia_interes);
				cs.setInt(12, frecuencia_capital);
				cs.setString(14, clase_tasa);
				cs.setString(22, observaciones);
				cs.setDate(23, fecha_venc,Calendar.getInstance());
				cs.setDate(25, fecha_apertura,Calendar.getInstance());
				cs.setDouble(26, comision);
				cs.setString(27, diciembre);
				cs.setInt(28, dias_gracia_mora);
				cs.setString(29, tipo_liqui_mes);
				cs.setString(30, tipo_liqui_ano);
				cs.setString(31, adicionado_por);
				cs.setDate(32, fecha_adicion,Calendar.getInstance());
				/* Valor 33) Tipo_Frecuencia_Capital */
				/* Valor 34) Tipo_Frecuencia_Interes */
				if (tipoPiso==1) {
					if ("3".equals(cod_tipo_amort)) {
						cs.setInt(33, 2);
						cs.setInt(34, 2);
					} else {
						cs.setNull(33, Types.INTEGER);
						if (icMoneda==54) {
							cs.setInt(34, 2);
						} else {
							cs.setNull(34, Types.INTEGER);
						}
					}
				} else {
					cs.setNull(33, Types.INTEGER);
					cs.setNull(34, Types.INTEGER);
				}
				cs.setString(35, cobrar_iva);
				cs.setString(38, rela_mate_3);
				cs.setDouble(39, spread_corriente_3);		/* Valor 39) Spread_Corriente_3 */
				cs.setInt(40, dia_pago_interes);
				cs.setInt(41, dia_pago_capital);
				cs.setInt(42, dia_revision_tasa);
				cs.setString(43, tipo_redondeo);
				cs.setString(44, tipo_calculo_mora);
				if (icMoneda==54){
					cs.setNull(45, Types.INTEGER);
				} else {
					cs.setInt(45, rec_frescos);
				}
				cs.setString(46, amort_ult_prim);
				/* Valor 47) Monto_Amortizacion_Ajuste */
				if (amort_ult_prim == null) {
					cs.setNull(47, Types.DOUBLE);
				} else {
					queri = " SELECT /*+ index(i cp_inv_mensualidad_pk)*/fn_pago_capital, ic_mensualidad "   +
							" FROM inv_mensualidad i"   +
							" WHERE cc_disposicion = '" + folio + "' " +
							" ORDER BY ic_mensualidad DESC" ;
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next())
						monto_amort_ajuste = rs1.getDouble(1);
					rs1.close();
					con.cierraStatement();
					cs.setDouble(47, monto_amort_ajuste);
				}
				cs.setString(48, forma_redondeo);
				cs.setString(49, desgloce_tasas);
				cs.setString(50, cobro_int_ES);
				cs.setString(52, clausula_venci);
				cs.setString(55, valor_presente);
				cs.setNull(56, Types.INTEGER);				/* Valor 56) Codigo_Comision */
				cs.setLong(57, no_electronico);
				cs.setString(58, pago_masivo);
				cs.setString(59, tipo_renta);
				cs.setInt(60, no_cuotas_recortadas);
				cs.setInt(61, tipo_mora);
				cs.setNull(62, Types.INTEGER);				/* Valor 62) Codigo_Grupo_Comisi�n */

				if(numero_cliente_troya.equals("") || numero_solic_troya.equals("")){ //FODA 037-2005
					cs.setNull(68,Types.VARCHAR);	/* Valor 68) Numero Cliente Troya */
					cs.setNull(69,Types.VARCHAR);	/* Valor 69) N�mero Sol Troya Nafin */
				}else{
					//FODA 099 - Interfase N@E-Troya
					cs.setString(68, numero_cliente_troya);	/* Valor 68) Numero Cliente Troya */
					cs.setString(69, numero_solic_troya);	/* Valor 69) N�mero Sol Troya Nafin */
				}

				//cs.setNull(70, Types.INTEGER);				/* Valor 70) Numero_epo */
				cs.setInt(70, numero_epo);  //foda 041-2005

				if(modalidad!=null)
					cs.setInt(71, Integer.parseInt(modalidad));					/* Valor 71) modalidad nafin empresarial */
				else
					cs.setNull(71, Types.INTEGER);

				cs.setNull(72, Types.INTEGER);				/* Valor 71) Estatus */
				cs.registerOutParameter(72, Types.INTEGER);
				cadena="P1:=null;"+"\n"+
					"P2:="+valor_tasa_cartera+";\n"+
					"P3:='"+cod_tipo_amort+"';"+"\n"+
					"P4:="+no_contrato+";"+"\n"+
					"P5:="+valor_inicial+";"+"\n"+
					"P6:='"+calcular_mora+"';"+"\n"+
					"P7:=to_date('"+fecha_ppi+"','yyyy-mm-dd');"+"\n"+
					"P8:=to_date('"+fecha_ppc+"','yyyy-mm-dd');"+"\n"+
					"P9:='"+intereses_vencidos+"';"+"\n"+
					"P10:='"+intereses_normales+"';"+"\n"+
					"P11:="+frecuencia_interes+";"+"\n"+
					"P12:="+frecuencia_capital+";"+"\n"+
					"P13:="+tipo_tasa+";"+"\n"+
					"P14:='"+clase_tasa+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P15:=1;"+"\n"+
						"P16:=2;"+"\n"+
						"P17:="+spread_corriente_1+";"+"\n";

					if("1".equals(modalidad)||"4".equals(modalidad)) {
						cadena += "P18:=NULL;"+"\n";
					} else {
						cadena += "P18:="+spread_corriente_2+";"+"\n";
					}

					cadena +=
						"P19:='"+rela_mate_1+"';"+"\n"+
						"P20:="+rela_mate_2+";"+"\n"+
						"P21:='"+rela_mate_mora+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "P15:=1;"+"\n"+
							"P16:=null;"+"\n"+
							"P17:="+spread_corriente_1+";"+"\n"+
							"P18:=null;"+"\n"+
							"P19:='"+rela_mate_1+";"+"'\n"+
							"P20:="+rela_mate_2+";"+"\n"+
							"P21:='"+rela_mate_mora+"';"+"\n";
					} else {
						cadena += "P15:=null;"+"\n"+
							"P16:=null;"+"\n"+
							"P17:=null;"+"\n"+
							"P18:=null;"+"\n"+
							"P19:="+rela_mate_1+";"+"\n"+
							"P20:="+rela_mate_2+";"+"\n"+
							"P21:="+rela_mate_mora+";"+"\n";
					}
				}

				cadena += 	"P22:='"+observaciones+"';"+"\n"+
					"P23:=to_date('"+fecha_venc+"','yyyy-mm-dd');"+"\n";
				if (tipoPiso==1) {
					cadena += "P24:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n";
				} else {
					cadena += "P24:="+fecha_revi_int+";"+"\n";
				}
				cadena += "P25:="+fecha_apertura+";"+"\n"+
					"P26:="+comision+";"+"\n"+
					"P27:='"+diciembre+"';"+"\n"+
					"P28:="+dias_gracia_mora+";"+"\n"+
					"P29:='"+tipo_liqui_mes+"';"+"\n"+
					"P30:='"+tipo_liqui_ano+"';"+"\n"+
					"P31:='"+adicionado_por+"';"+"\n"+
					"P32:=to_date('"+fecha_adicion+"','yyyy-mm-dd');"+"\n";
				if (tipoPiso==1) {
					if ("3".equals(cod_tipo_amort)) {
						cadena += "P33:=2;"+"\n";
						cadena += "P34:=2;"+"\n";

					} else {
						cadena += "P33:=null;"+"\n";
						if (icMoneda==54) {
							cadena += "P34:=2;"+"\n";
						} else {
							cadena += "P34:=null;"+"\n";
						}
					}
				} else {
					cadena += "P33:=null;"+"\n"+
					"P34:=null;"+"\n";
				}

				cadena += "P35:='"+cobrar_iva+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P36:="+valor_tasa_cartera+";"+"\n"+
						"P37:=2;"+"\n";
				} else {
					if (icMoneda==54){
						cadena += "P36:=null;"+"\n"+
							"P37:=null;"+"\n";
					} else {
						cadena += "P36:=20;"+"\n"+
							"P37:=null;"+"\n";
					}

				}
				cadena += "P38:="+rela_mate_3+";"+"\n"+
					"P39:="+spread_corriente_3+";"+"\n"+
					"P40:="+dia_pago_interes+";"+"\n"+
					"P41:="+dia_pago_capital+";"+"\n"+
					"P42:="+dia_revision_tasa+";"+"\n"+
					"P43:='"+tipo_redondeo+"';"+"\n"+
					"P44:='"+tipo_calculo_mora+"';"+"\n";
				if (icMoneda==54){
					cadena += "P45:=null;"+"\n";
				} else {
					cadena += "P45:="+rec_frescos+";"+"\n";
				}

				cadena += "P46:="+amort_ult_prim+";"+"\n";
				if (amort_ult_prim == null) {
					cadena += "P47:=null;"+"\n";
				} else {
					cadena += "P47:="+monto_amort_ajuste+";"+"\n";
				}
				cadena += "P48:='"+forma_redondeo+"';"+"\n"+
					"P49:="+desgloce_tasas+";"+"\n"+
					"P50:='"+cobro_int_ES+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "P51:=null;"+"\n";
				} else {
					cadena += "P51:=null;"+"\n";
				}
				cadena += "P52:='"+clausula_venci+"';"+"\n"+
					"P53:='"+tipo_interes+"';"+"\n"+
					"P54:="+periodo_capitalizacion+";"+"\n"+
					"P55:='"+valor_presente+"';"+"\n"+
					"P56:=null;"+"\n"+
					"P57:="+no_electronico+";"+"\n"+
					"P58:='"+pago_masivo+"';"+"\n"+
					"P59:="+tipo_renta+";"+"\n"+
					"P60:="+no_cuotas_recortadas+";"+"\n"+
					"P61:="+tipo_mora+";"+"\n"+
					"P62:=null;"+"\n";
				if (tipoPiso==1) {
					cadena += "P63:=2;"+"\n"+
						"P64:=1;"+"\n"+
						"P65:="+dia_pago_interes+";"+"\n"+
					    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n"+
						"P67:='"+tipo_tasa_int_mora+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "P63:=2;"+"\n"+
							"P64:=null;"+"\n"+
							"P65:="+dia_pago_interes+";"+"\n"+
						    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');"+"\n"+
							"P67:='"+tipo_tasa_int_mora+"';"+"\n";

					} else {
						cadena += "P63:=null;"+"\n"+
							"P64:=null;"+"\n"+
							"P65:=null;"+"\n"+
							"P66:=null;"+"\n"+
							"P67:="+tipo_tasa_int_mora+";"+"\n";
					}
				}

				if(numero_cliente_troya.equals("") || numero_solic_troya.equals("")){ //FODA 037-2005
					cadena += "P68:=null;"+"\n"+
							"P69:=null;"+"\n";
				}else{
					cadena += "P68:="+numero_cliente_troya+";"+"\n"+
							"P69:="+numero_solic_troya+";"+"\n";
				}
				cadena +=//"P70:=null;"+"\n"+
						 "P70:="+numero_epo+";\n"+ //foda 041-2005
						 "P71:="+modalidad+";\n"+
						 "P72:=null;"+"\n";

System.out.println(cadena);

				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						no_prestamo = cs.getInt(1);
						status = cs.getString(72);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle) {
					sqle.printStackTrace();
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					status="9";
				}

				System.out.println("No Prestamo: "+no_prestamo+" Estatus: "+status);
				cadena += "No Prestamo: "+no_prestamo+" Estatus: "+status;

									/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					con.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PR'";
						try {
							rs1=con.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Prestamos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}


					queri = "select count(1) from inv_interfase where cc_disposicion='"+folio+"'";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); con.cierraStatement();

					if(no_doctos!=0) {
						queri = "update inv_interfase " +
								"	set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" 	, ic_error_proceso="+status+
								"   , df_hora_proceso=SYSDATE"+
								"   , ig_proceso_numero = 3"+
								"   , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where cc_disposicion= '"+folio+"'";
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
							errorTransaccion = true;
						}
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de controlAnt03 a com_controlAnt04 */
					queri = "Update inv_interfase " +
							"	set ig_numero_prestamo = " + no_prestamo +
							"   , df_hora_proceso=SYSDATE "+
							"   , DF_PRESTAMOS=SYSDATE "+
							"   , ig_proceso_numero = 3"+
							"   , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							"   , cc_producto = '5A' "+
							" where cc_disposicion= '"+folio+"'";
					try {
						con.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el numero de prestamo");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//while
			if (cs !=null) cs.close();
			rs.close(); con.cierraStatement();

			System.out.println(" Fin Operaciones 1er Piso Credicadenas");
                        }
			/* Fin Interfase 1er Piso Credicadenas */

			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////
			////I n i c i o   I n t e r f a s e   2do   Piso   Financiamiento  a   Distribuidores////
			/////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////

			/* Inicia la Interfase Financiamiento a Distribuidores 2do Piso. */
            if(icPausaInterfazDistr.equals("N")){
			System.out.println(" Inicia la Interfase Financiamiento a Distribuidores 2do Piso.");
			// CREDITO EN CUENTA CORRIENTE
			query = " select /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds lc i t)*/ " +
					"	 di.ic_documento" +
					"  , di.ig_codigo_agencia" +
					"  , di.ig_codigo_sub_aplicacion" +
					"  , di.ig_numero_linea_fondeo" +
					"  , di.ig_codigo_financiera" +
					"  , di.ig_aprobado_por" +
					"  , di.cg_descripcion" +
					"  , di.fn_monto_operacion" +
					"  , di.ig_numero_linea_credito" +
					"  , di.ig_codigo_agencia_proy" +
					"  , di.ig_codigo_sub_aplicacion_proy" +
					"  , di.ig_numero_contrato" +
					"  , s.ic_tabla_amort" +
					"  , s.df_ppi" +
					"  , s.df_ppc" +
					"  , s.df_v_credito as df_fecha_venc " +
					"  , trunc(SYSDATE) as hoy" +
					"  , d.ig_plazo_credito as ig_plazo" +
					"  , di.ic_documento as numero_electronico" +
					"  , i.ig_tipo_piso" +
					"  , ds.ic_tasa" +
					"  , ds.fn_puntos as CG_PUNTOS" +
					"  , t.ic_moneda" +
					"  , t.fg_sobretasa" +
					"  , d.ic_epo " +
					"	, 'Prestamos(Distribuidores)' as proceso" +
					" FROM dis_solicitud s" +
					"  , dis_interfase di" +
					"  , dis_documento d" +
					"  , dis_docto_seleccionado ds" +
					"  , com_linea_credito lc" +
					"  , comcat_if i" +
					"  , comcat_tasa t" +
					"  , comrel_producto_if rpi" +
					" WHERE s.ic_documento = di.ic_documento" +
					"   and s.ic_documento = ds.ic_documento" +
					"   and s.ic_documento = d.ic_documento" +
					"   and d.ic_linea_credito = lc.ic_linea_credito" +
					"   and lc.ic_if = i.ic_if" +
					"   and lc.ic_if = rpi.ic_if" +
					"   and ds.ic_tasa = t.ic_tasa" +
					"   and di.ig_numero_prestamo is null" +
					"   and di.ig_numero_contrato is not null" +
					"   and di.ig_numero_linea_credito is not null" +
					"   and di.cc_codigo_aplicacion is null" +
					"   and di.ic_error_proceso is null" +
					"   and s.ic_estatus_solic = 2" +
					"   and s.ic_bloqueo = 3" +
					"   and rpi.ic_producto_nafin = 4" +
					" UNION ALL" +
					// DESCUENTO MERCANTIL
					" select /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds lc i t)*/ " +
					"	 di.ic_documento" +
					"  , di.ig_codigo_agencia" +
					"  , di.ig_codigo_sub_aplicacion" +
					"  , di.ig_numero_linea_fondeo" +
					"  , di.ig_codigo_financiera" +
					"  , di.ig_aprobado_por" +
					"  , di.cg_descripcion" +
					"  , di.fn_monto_operacion" +
					"  , di.ig_numero_linea_credito" +
					"  , di.ig_codigo_agencia_proy" +
					"  , di.ig_codigo_sub_aplicacion_proy" +
					"  , di.ig_numero_contrato" +
					"  , s.ic_tabla_amort" +
					"  , s.df_ppi" +
					"  , s.df_ppc" +
					"  , s.df_v_credito as df_fecha_venc" +
					"  , trunc(SYSDATE) as hoy" +
					"  , d.ig_plazo_credito as ig_plazo" +
					"  , di.ic_documento as numero_electronico" +
					"  , i.ig_tipo_piso" +
					"  , ds.ic_tasa" +
					"  , ds.fn_puntos as CG_PUNTOS" +
					"  , t.ic_moneda" +
					"  , t.fg_sobretasa" +
					"  , d.ic_epo " +
					"	, 'Prestamos(Distribuidores)' as proceso" +
					" FROM dis_solicitud s" +
					"  , dis_interfase di" +
					"  , dis_documento d" +
					"  , dis_docto_seleccionado ds" +
					"  , dis_linea_credito_dm lc" +
					"  , comcat_if i" +
					"  , comcat_tasa t" +
					"  , comrel_producto_if rpi" +
					" WHERE s.ic_documento = di.ic_documento" +
					"   and s.ic_documento = ds.ic_documento" +
					"   and s.ic_documento = d.ic_documento" +
					"   and d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					"   and lc.ic_if = i.ic_if" +
					"   and lc.ic_if = rpi.ic_if" +
					"   and ds.ic_tasa = t.ic_tasa" +
					"   and di.ig_numero_prestamo is null" +
					"   and di.ig_numero_contrato is not null" +
					"   and di.ig_numero_linea_credito is not null" +
					"   and di.cc_codigo_aplicacion is null" +
					"   and di.ic_error_proceso is null" +
					"   and s.ic_estatus_solic = 2" +
					"   and s.ic_bloqueo = 3" +
					"   and rpi.ic_producto_nafin = 4" +
					" ORDER BY ig_tipo_piso desc";
			//System.out.println(query);
			try {
				rs=con.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("Prestamos.java(query): " + query );
				throw sqle;
			}
			tipoPiso=0;
			monto_amort_ajuste=0.0;
			amort_ult_prim = null;
			calcular_mora = "S";
			intereses_vencidos = "S";
			frecuencia_interes = 0;
			frecuencia_capital = 0;
			numero_cliente_troya = null;
			numero_solic_troya = null;

			while(rs.next()) {
				errorTransaccion = false;
				errorDesconocido = false;
				diciembre = "0";
				tipo_tasa = codigo_Valor_Tasa_Mora = null;
				icMoneda = rs.getInt("IC_MONEDA");
				rela_mate_mora=null;
				spread_corriente_1=0.0;
				fecha_revi_int=null;

				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio=rs.getString("IC_DOCUMENTO");
				cod_tipo_amort=rs.getString("IC_TABLA_AMORT");			/* Valor 3) Codigo_Tipo_Amortizacion */
				no_contrato=rs.getInt("IG_NUMERO_CONTRATO");			/* Valor 4) Numero_Contrato */
				valor_inicial=rs.getDouble("FN_MONTO_OPERACION");		/* Valor 5) Valor_Inicial */
				fecha_ppi=rs.getDate("DF_PPI");							/* Valor 7) Fecha_Primer_Pago_Interes */
				fecha_ppc=rs.getDate("DF_PPC");							/* Valor 8) Fecha_Primer_Pago_Capital */
				fecha_venc=rs.getDate("DF_FECHA_VENC");					/* Valor 23) Fecha_Vencimiento */
				fecha_adicion=rs.getDate("HOY");						/* Valor 32) Fecha_Adicion */
				no_electronico=rs.getLong("NUMERO_ELECTRONICO");			/* Valor 57) Numero_Electr�nico */
				ic_plazo=rs.getInt("IG_PLAZO");
				tipo_interes = "S";
				periodo_capitalizacion = 0;
				dia_revision_tasa=Integer.parseInt(fecha_ppi.toString().substring(8,10));	/* Valor 42) Dia_Revision_Tasa */
				dia_pago_interes=Integer.parseInt(fecha_ppi.toString().substring(8,10));	/* Valor 40) Dia_Pago_Interes */
				dia_pago_capital=Integer.parseInt(fecha_ppc.toString().substring(8,10));	/* Valor 41) Dia_Pago_Capital */
				valor_tasa_cartera = rs.getInt("IC_TASA");
				numero_epo = rs.getInt("IC_EPO");


			//	System.out.println("(3)"+cod_tipo_amort+"(4)"+no_contrato+"(5)"+valor_inicial+"(7)"+fecha_ppi+"(8)"+fecha_ppc+"(23)"+fecha_venc+"(32)"+fecha_adicion+"(40)"+dia_pago_interes+"(41)"+dia_pago_capital+"(57)"+no_electronico);
				try {
					cs = con.ejecutaSP(paramStore);
				} catch(SQLException sqle) {
					System.out.println("Prestamos.java(paramStore): " + paramStore );
					throw sqle;
				}

				if (tipoPiso==1) {
					calcular_mora = "N";
					cs.setString(6, calcular_mora);
					intereses_vencidos = "N";
					cs.setString(9, intereses_vencidos);
					frecuencia_interes = 0;
					frecuencia_capital = 0;
					tipo_tasa = "0";
					cs.setString(13, tipo_tasa);
					cs.setInt(15, 1);						/* Valor 15) Periodo_Tasa */
					/* Valor 16) Spread_Mora */
					/* Valor 17) Spread_Corriente_1 */
					cs.setNull(16, Types.INTEGER);
					spread_corriente_1 = 2;
					cs.setDouble(17, spread_corriente_1);

					cs.setNull(18, Types.DOUBLE);			/* Valor 18) Spread_Corriente_2 */
					rela_mate_1 = "+";
					cs.setString(19, rela_mate_1);
					rela_mate_2 = null;
					cs.setString(20, rela_mate_2);
					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);	/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();
					//fecha_apertura=fecha_revi_int;		/* Valor 25) Fecha_Apertura */

					cs.setNull(21, Types.VARCHAR);
					cs.setDate(24, fecha_revi_int,Calendar.getInstance());
					/* Valor 33) Tipo_Frecuencia_Capital */
					cs.setNull(33, Types.INTEGER);
					/* Valor 34) Tipo_Frecuencia_Interes */
					cs.setNull(34, Types.INTEGER);

					codigo_Valor_Tasa_Mora = null;
					tipo_frecuencia_revision = "2";
					tipo_calculo_mora = null;				/* Valor 44) Tipo_Calculo_Mora */

					cs.setNull(51, Types.DOUBLE);			/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;
					cs.setInt(54, periodo_capitalizacion);
					cs.setInt(63, 2);						/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					cs.setInt(64, 1);						/* Valor 64) Frecuencia_Rev_Int_Mora */
					cs.setInt(65, dia_pago_interes);		/* Valor 65) Dia_Revision_Tasa_Mora */
					cs.setDate(66, fecha_revi_int,Calendar.getInstance());			/* Valor 66) Fecha_revision_mora */
					tipo_tasa_int_mora = "5";
					cs.setString(67, tipo_tasa_int_mora);	/* Valor 67) Tipo_Tasa_Int_Mora */
				} else {	// 2do Piso
					cs.setString(6, calcular_mora);
					cs.setString(9, intereses_vencidos);
					frecuencia_interes = (icMoneda==54)?1:0;
					frecuencia_capital = (icMoneda==54)?1:0;
					tipo_tasa = (icMoneda==54)?"6":"0";
					cs.setString(13, tipo_tasa);
					/* Valor 15) Periodo_Tasa */
					cs.setInt(15, 1);

					queri = "select fecha_hoy from mg_calendario where codigo_aplicacion = 'BPR' and codigo_empresa=1 ";
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						fecha_revi_int = rs1.getDate(1);		/* Valor 24) Fecha_Revision_Int */
					rs1.close();
					con.cierraStatement();

					cs.setDate(24, fecha_revi_int,Calendar.getInstance());


					/* Valor 16) Spread_Mora */
					/* Valor 17) Spread_Corriente_1 */
					/* Valor 33) Tipo_Frecuencia_Capital */
					/* Valor 34) Tipo_Frecuencia_Interes */
					/* Valor 36) Codigo_Valor_Tasa_Mora */
					/* Valor 44) Tipo_Calculo_Mora */
					/* Valor 63) Tipo_Frecuencia_Rev_Int_Mora */
					/* Valor 64) Frecuencia_Rev_Int_Mora */
					/* Valor 65) Dia_Revision_Tasa_Mora */
					/* Valor 66) Fecha_revision_mora */

					if (icMoneda==54) {
						cs.setNull(16, Types.INTEGER);
						spread_corriente_1 = rs.getDouble("CG_PUNTOS");
						cs.setDouble(17, spread_corriente_1);
						rela_mate_1 = null;
						cs.setString(19, rela_mate_1);
						cs.setNull(33, Types.INTEGER);
						cs.setNull(34, Types.INTEGER);
						codigo_Valor_Tasa_Mora = null;
						tipo_frecuencia_revision = null;
						tipo_calculo_mora = "1";

						cs.setInt(63, 2);
						cs.setNull(64, Types.INTEGER);
						cs.setInt(65, dia_pago_interes);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());
					} else {
						cs.setInt(16, 2);
						cs.setDouble(17, spread_corriente_1);
						rela_mate_1 = "+";
						cs.setString(19, rela_mate_1);
						cs.setInt(33, 2);
						cs.setInt(34, 2);
						tipo_calculo_mora = "2";

						//codigo_Valor_Tasa_Mora = (ic_plazo>60)?"20":String.valueOf(valor_tasa_cartera);
						codigo_Valor_Tasa_Mora = String.valueOf(valor_tasa_cartera);
						tipo_frecuencia_revision = "2";

						//cs.setNull(63, Types.INTEGER);
						cs.setInt(63, 2);
						cs.setInt(64, 1);
						//cs.setNull(65, Types.INTEGER);
						cs.setInt(65, dia_pago_interes);
						//cs.setNull(66, Types.DATE);
						cs.setDate(66, fecha_revi_int,Calendar.getInstance());

					}
					cs.setNull(18, Types.DOUBLE);				/* Valor 18) Spread_Corriente_2 */
					rela_mate_2 = null;
					cs.setString(20, rela_mate_2);
					rela_mate_mora = "*";
					cs.setString(21, rela_mate_mora);
					diciembre = (icMoneda == 54)?null:"0";


					cs.setNull(51, Types.DOUBLE);				/* Valor 51) Tasa_Total_Moratoria */
					tipo_interes = "S";	//(icMoneda==54)?"S":tipo_interes;
					cs.setString(53, tipo_interes);
					periodo_capitalizacion = 0;	//(tipo_interes.equals("S"))?0:1;
					cs.setInt(54, periodo_capitalizacion);
					tipo_tasa_int_mora = (icMoneda==54)?null:"5";
					cs.setString(67, tipo_tasa_int_mora);		/* Valor 67) Tipo_Tasa_Int_Mora */
				}

				cs.setNull(1, Types.INTEGER);				/* Valor 1) Numero_Prestamo */
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, valor_tasa_cartera);
				cs.setString(3, "1");
				//cs.setString(3, cod_tipo_amort);
				cs.setInt(4, no_contrato);
				cs.setDouble(5, valor_inicial);
				cs.setDate(7, fecha_ppi,Calendar.getInstance());
				cs.setDate(8, fecha_ppc,Calendar.getInstance());
				cs.setString(10, intereses_normales);
				cs.setInt(11, frecuencia_interes);
				cs.setInt(12, frecuencia_capital);
				cs.setString(14, clase_tasa);
				cs.setString(22, observaciones);
				cs.setDate(23, fecha_venc,Calendar.getInstance());
				cs.setDate(25, fecha_apertura,Calendar.getInstance());
				cs.setDouble(26, comision);
				cs.setString(27, diciembre);
				cs.setInt(28, dias_gracia_mora);
				cs.setString(29, tipo_liqui_mes);
				cs.setString(30, tipo_liqui_ano);
				cs.setString(31, adicionado_por);
				cs.setDate(32, fecha_adicion,Calendar.getInstance());
				cs.setString(35, cobrar_iva);
				/* Valor 36) Codigo_Valor_Tasa_Mora */
				if(codigo_Valor_Tasa_Mora == null)
					cs.setNull(36, Types.INTEGER);
				else
					cs.setInt(36, Integer.parseInt(codigo_Valor_Tasa_Mora));

				if(tipo_frecuencia_revision == null)
					cs.setNull(37, Types.INTEGER);
				else
					cs.setInt(37, Integer.parseInt(tipo_frecuencia_revision));

				cs.setString(38, rela_mate_3);
				cs.setNull(39, Types.DOUBLE);		/* Valor 39) Spread_Corriente_3 */
				cs.setInt(40, dia_pago_interes);
				cs.setInt(41, dia_pago_capital);
				cs.setInt(42, dia_revision_tasa);
				cs.setString(43, tipo_redondeo);
				cs.setString(44, tipo_calculo_mora);
				if (icMoneda==54){
					cs.setNull(45, Types.INTEGER);
				} else {
					cs.setInt(45, rec_frescos);
				}
				cs.setString(46, amort_ult_prim);
				cs.setNull(47, Types.DOUBLE);				/* Valor 47) Monto_Amortizacion_Ajuste */
				cs.setString(48, forma_redondeo);
				cs.setString(49, desgloce_tasas);
				cs.setString(50, cobro_int_ES);
				cs.setString(52, clausula_venci);
				cs.setString(55, valor_presente);
				cs.setNull(56, Types.INTEGER);				/* Valor 56) Codigo_Comision */
				cs.setLong(57, no_electronico);
				cs.setString(58, pago_masivo);
				cs.setString(59, tipo_renta);
				cs.setInt(60, no_cuotas_recortadas);
				cs.setInt(61, tipo_mora);
				cs.setNull(62, Types.INTEGER);				/* Valor 62) Codigo_Grupo_Comisi�n */
				//FODA 099 - Interfase N@E-Troya
				cs.setString(68, numero_cliente_troya);				/* Valor 68) Numero Cliente Troya */
				cs.setNull(69, Types.INTEGER);				/* Valor 69) N�mero Sol Troya Nafin */
				cs.setInt(70, numero_epo);					/* Valor 70) Numero_epo */
				cs.setNull(71, Types.INTEGER);				/* Valor 71) Estatus */
				cs.setNull(72, Types.INTEGER);				/* Valor 71) Estatus */
				cs.registerOutParameter(72, Types.INTEGER);

				cadena="P1:=null;\n"+
					"P2:="+valor_tasa_cartera+";\n"+
					//"P3:='"+cod_tipo_amort+"';\n"+
					"P3:='1';\n"+
					"P4:="+no_contrato+";\n"+
					"P5:="+valor_inicial+";\n"+
					"P6:='"+calcular_mora+"';\n"+
					"P7:=to_date('"+fecha_ppi+"','yyyy-mm-dd');\n"+
					"P8:=to_date('"+fecha_ppc+"','yyyy-mm-dd');\n"+
					"P9:='"+intereses_vencidos+"';\n"+
					"P10:='"+intereses_normales+"';\n"+
					"P11:="+frecuencia_interes+";\n"+
					"P12:="+frecuencia_capital+";\n"+
					"P13:="+tipo_tasa+";\n"+
					"P14:='"+clase_tasa+"';\n";
				if (tipoPiso==1) {
					cadena += "P15:=1;\n";
					cadena += "P16:=null;\n"+
						"P17:="+spread_corriente_1+";\n"+
						"P18:=null;\n"+
						"P19:='"+rela_mate_1+"';\n"+
						"P20:="+rela_mate_2+";\n"+
						"P21:="+rela_mate_mora+";\n";
				} else {
					if (icMoneda==54) {
						cadena += "P15:=1;\n"+
							"P16:=null;\n"+
							"P17:="+spread_corriente_1+";\n"+
							"P18:=null;\n"+
							"P19:='"+rela_mate_1+"';\n"+
							"P20:="+rela_mate_2+";\n"+
							"P21:='"+rela_mate_mora+"';\n";
					} else {
						cadena += "P15:=1;\n"+
							"P16:=2;\n"+
							"P17:="+spread_corriente_1+";\n"+
							//"P17:=null;\n"+
							"P18:=null;\n"+
							"P19:="+rela_mate_1+";\n"+
							"P20:="+rela_mate_2+";\n"+
							"P21:="+rela_mate_mora+";\n";
					}
				}

				cadena += "P22:='"+observaciones+"';\n"+
						"P23:=to_date('"+fecha_venc+"','yyyy-mm-dd');\n";
				if (tipoPiso==1) {
					cadena += "P24:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n";
				} else {
					cadena += "P24:="+fecha_revi_int+";\n";
				}
				cadena += "P25:="+fecha_apertura+";\n"+
						"P26:="+comision+";\n"+
						"P27:='"+diciembre+"';\n"+
						"P28:="+dias_gracia_mora+";\n"+
						"P29:='"+tipo_liqui_mes+"';\n"+
						"P30:='"+tipo_liqui_ano+"';\n"+
						"P31:='"+adicionado_por+"';\n"+
						"P32:=to_date('"+fecha_adicion+"','yyyy-mm-dd');\n";
				if (icMoneda==54){
					cadena += "P33:=null;\n"+
						"P34:=null;\n";
				} else {
					cadena += "P33:=2;\n"+
						"P34:=2;\n";
				}

				cadena += "P35:='"+cobrar_iva+"';\n"+
						"P36:="+codigo_Valor_Tasa_Mora+";\n"+
						"P37:=2;\n"+
						"P38:="+rela_mate_3+";\n"+
						"P39:=null;\n"+
						"P40:="+dia_pago_interes+";\n"+
						"P41:="+dia_pago_capital+";\n"+
						"P42:="+dia_revision_tasa+";\n"+
						"P43:='"+tipo_redondeo+"';\n"+
						"P44:='"+tipo_calculo_mora+"';\n";
				if (icMoneda==54){
					cadena += "P45:=null;\n";
				} else {
					cadena += "P45:="+rec_frescos+";\n";
				}
				cadena += "P46:="+amort_ult_prim+";\n"+
						"P47:=null;\n"+
						"P48:='"+forma_redondeo+"';\n"+
						"P49:="+desgloce_tasas+";\n"+
						"P50:='"+cobro_int_ES+"';\n";
				cadena += "P51:=null;\n";
				cadena += "P52:='"+clausula_venci+"';\n"+
						"P53:='"+tipo_interes+"';\n"+
						"P54:="+periodo_capitalizacion+";\n"+
						"P55:='"+valor_presente+"';\n"+
						"P56:=null;\n"+
						"P57:="+no_electronico+";\n"+
						"P58:='"+pago_masivo+"';\n"+
						"P59:="+tipo_renta+";\n"+
						"P60:="+no_cuotas_recortadas+";\n"+
						"P61:="+tipo_mora+";\n"+
						"P62:=null;\n";
				if (tipoPiso==1) {
					cadena += "P63:=2;\n"+
							"P64:=1;\n"+
							"P65:="+dia_pago_interes+";\n"+
						    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n"+
							"P67:='"+tipo_tasa_int_mora+"';\n";
				} else {
					if (icMoneda==54) {
						cadena += "P63:=2;\n"+
								"P64:=null;\n"+
								"P65:="+dia_pago_interes+";\n"+
							    "P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n"+
								"P67:='"+tipo_tasa_int_mora+"';\n";
					} else {
						cadena += "P63:=null;\n"+
								"P64:=1;\n"+
								"P65:="+dia_pago_interes+";\n"+
								"P66:=to_date('"+fecha_revi_int+"','yyyy-mm-dd');\n"+
								"P67:="+tipo_tasa_int_mora+";\n";
					}
				}
				cadena += "P68:="+numero_cliente_troya+";"+"\n"+
						"P69:="+numero_solic_troya+";"+"\n"+
						"P70:="+numero_epo+";"+"\n"+
						"P70:=null;"+"\n"+
						"P71:=null;"+"\n";


				System.out.println(cadena);

				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						no_prestamo = cs.getInt(1);
						status = cs.getString(72);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle) {
					sqle.printStackTrace();
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					status="9";
				}
				System.out.println("No Prestamo: "+no_prestamo+" Estatus: "+status);
				cadena += "No Prestamo: "+no_prestamo+" Estatus: "+status;

				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					con.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
								" where ic_error_proceso="+status+
								" and cc_codigo_aplicacion='PR'";
						try {
							rs1=con.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("Prestamos.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); con.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}

					queri = "select count(1) from dis_interfase where ic_documento = "+folio;
					try {
						rs1=con.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("Prestamos.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);
					rs1.close(); con.cierraStatement();

					if(no_doctos!=0) {
						queri = "update dis_interfase " +
								" set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
								" , ic_error_proceso="+status+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 3"+
								" , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where ic_documento = "+folio;
//							System.out.println(queri);
						try {
							con.ejecutaSQL(queri);
						} catch(SQLException sqle) {
							System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
							errorTransaccion = true;
						}
					}
				} else {
					/* Si no hubo ningun error se Insertan los valores de controlAnt03 a com_controlAnt04 */
					queri = "Update dis_interfase " +
							" set ig_numero_prestamo = " + no_prestamo +
							" , df_hora_proceso=SYSDATE "+
							" , DF_PRESTAMOS=SYSDATE "+
							" , ig_proceso_numero = 3"+
							" , cg_prestamos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" where ic_documento = "+folio;
//							System.out.println(queri);
					try {
						con.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el numero de prestamo");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//while
			if (cs !=null) cs.close();
			rs.close(); con.cierraStatement();
			System.out.println(" Fin de la Interfase Financiamiento a Distribuidores 2do Piso.");
            }
			/* Fin de la Interfase Financiamiento a Distribuidores 2do Piso. */

		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			System.out.println(con.mostrarError());
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
                    try {
                        if(cs!=null) cs.close();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
		    try {
		        if(rs!=null) rs.close();
		    } catch(Exception e) {
		        e.printStackTrace();
		    }
		    try {
		        if(rs1!=null) rs1.close();
		    } catch(Exception e) {
		        e.printStackTrace();
		    }
		    try {
		        if(ps!=null) ps.close();
		    } catch(Exception e) {
		        e.printStackTrace();
		    }
                    if(con.hayConexionAbierta()) {
                        con.cierraConexionDB();
                    }
		}
	}
}
