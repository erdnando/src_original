package mx.gob.nafin.procesosexternos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netropology.utilerias.AccesoDBOracle;


public class Interfaz
{
	public static void main(String args[])
	{
		boolean ejecutar, validarFechas;
		String apiEjecutar = args[0];

		ResultSet rs;
		String sentenciaSQL;

		int horaActual;
		int horaInicio;
		int horaFin;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		String tipoCambioDl = "1";
		String fechaNE = "", fechaSirac = "";


		AccesoDBOracle con = new AccesoDBOracle();

		ejecutar = validarFechas = true;
		while (ejecutar)
		{
			try {
				con.conexionDB();
			} catch (Exception e){
				System.out.println("ERROR EN CONEXION A LA BASE DE DATOS\nProceso suspendido por 10 minutos");
				try{Thread.sleep(1000*60*10);}catch(Exception _e){}
				continue;
			}

			try
			{
				String []parametros=new String[1];
				if (apiEjecutar.equals("DIVERSOS") || apiEjecutar.equals("DIVERSOSE")) {
					sentenciaSQL = " SELECT fn_valor_compra "  +
								"	from com_tipo_cambio"  +
								"	where ic_moneda = 54"  +
								"	and dc_fecha = (select max(dc_fecha)"  +
								"                from com_tipo_cambio"  +
								"                where ic_moneda = 54) ";
	//				System.out.println(sentenciaSQL);
					try {
						rs = con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					if (rs.next()) {
						tipoCambioDl = rs.getString(1);
					}
					//System.out.println("Tipo Cambio Dolares" + tipoCambioDl);
					parametros[0] = tipoCambioDl;
				} else {
					sentenciaSQL = "select cg_usuario_interfase_sirac from com_param_gral where rownum = 1 ";
	//				System.out.println(sentenciaSQL);
					try {
						rs = con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs.next();
					parametros[0] = rs.getString(1);
				}

				try {
				  con.cierraStatement();
				} catch (SQLException sqle) {
					System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
				}

//________________________ Verificaci�n de d�a h�bil ______________________
				sentenciaSQL = "select count(*) as diaInhabil from comcat_dia_inhabil"+
					" where cg_dia_inhabil = TO_CHAR(sysdate,'dd/mm')";
				try {
					rs = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs.next();
				int diaInhabil = rs.getInt("DIAINHABIL");
				rs.close();
				try {
					con.cierraStatement();
				} catch (SQLException sqle) {
					System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
				}

				//En caso de querer ignorar si el dia es habil descomentar la sig. linea:
				//diaInhabil=0;

				if (diaInhabil!=0) {	//Es d�a inhabil?
					System.out.println("El dia es inhabil\nPrograma finalizado");
					System.exit(0);
				}

				Calendar fecha = Calendar.getInstance();
		     	int dia = fecha.get(Calendar.DAY_OF_WEEK);

				if (dia == Calendar.SUNDAY || dia == Calendar.SATURDAY) { // Es sabado o domingo
					System.out.println("El sabado o domingo, \nPrograma finalizado");
					System.exit(0);
				}


				sentenciaSQL = "select"+
					" TO_CHAR(sysdate,'HH24MI') as horarioActual"+
					" , TO_CHAR(TO_DATE(cg_hora_inicio_interfase,'HH24:MI'),'HH24MI') as horarioInicio"+
					" , TO_CHAR(TO_DATE(cg_hora_fin_interfase,'HH24:MI'),'HH24MI') as horarioFin"+
					" from com_param_gral where rownum = 1 ";

				try {
					rs = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				if (rs.next()) {
					horaActual = rs.getInt("HORARIOACTUAL");
					horaInicio = rs.getInt("HORARIOINICIO");
					horaFin = rs.getInt("HORARIOFIN");

					System.out.println(horaActual+"***"+horaInicio+"***"+horaFin);
					if (horaActual < horaInicio) {	//Actual<Inicio
						rs.close();
						try {
							con.cierraStatement();
						} catch (SQLException sqle) {
							System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
						}
						con.cierraConexionDB();
						System.out.println("Espera");
						Thread.sleep(1000*60*5);	//Dormir 5 minutos
						continue;	//Vuelve a evaluar la condici�n del while(ejecutar)
					} else  if(validarFechas) {
						// SE VERIFICA QUE SE ENCUENTREN A LA MISMA FECHA LOS SISTEMAS
						// DE SIRAC Y NAFIN ELECTRONICO
						System.out.println("Validando Fechas N@E - SIRAC");

						//sentenciaSQL = "select to_char(fecha_hoy, 'dd/mm/yyyy') as fecha_sirac, to_char(sysdate, 'dd/mm/yyyy') as fecha_ne from mg_calendario where codigo_aplicacion = 'BPR' ";
						sentenciaSQL = "select to_char(sysdate, 'dd/mm/yyyy') as fecha_sirac, to_char(sysdate, 'dd/mm/yyyy') as fecha_ne from mg_calendario where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR' ";

						ResultSet rsFechas = null;
						try {
							rsFechas = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}


						rsFechas.next();

						fechaNE = rsFechas.getString("FECHA_NE");
						fechaSirac = rsFechas.getString("FECHA_SIRAC");

						if (! fechaSirac.equals(fechaNE)) {
							rs.close();rsFechas.close();
							try {
								con.cierraStatement();
							} catch (SQLException sqle) {
								System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
							}
							con.terminaTransaccion(true);con.cierraConexionDB();

							System.out.println("PROCESO INACTIVO, FECHAS DIFERENTES ENTRE SIRAC Y NE");
							System.out.println("FECHA SIRAC: " + fechaSirac);
							System.out.println("FECHA NE: " + fechaNE);
							Thread.sleep(1000*60*3);	//Dormir 3 minutos
							continue;	//Vuelve a evaluar la condici�n del while(ejecutar)

						} else {
							validarFechas = false;
							System.out.println(" Fechas Ok!!!");
						}
						rsFechas.close();
						try {
							con.cierraStatement();
						} catch (SQLException sqle) {
							System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
						}




					}

					if (horaActual > horaFin)	//Actual>Fin
					{
						ejecutar=false;	//El proceso correra por �ltima vez!
					}
				}
				rs.close();
			    //________________________ Pausa proyectos credito electronico ______________________
                            String icPausaInterfazCrede="";
                            String sqlPausa = "SELECT " + 
                            "    ic_pausa_interfaz_crede " + 
                            " FROM " + 
                            "    com_param_gral ";
			    try {
			            rs = con.queryDB(sqlPausa);
			    } catch(SQLException sqle) {
			            System.out.println("Interfase.java(sentenciaSQL): " + sentenciaSQL );
			            throw sqle;
			    }
			    if (rs.next()) {
                                icPausaInterfazCrede = rs.getString("IC_PAUSA_INTERFAZ_CREDE");     //ProyectosE
                                //Descuento electronico y Financiamiento a distribuidores se procecesan en la misma calse java
                                /*icPausaInterfazDsctoe = rs.getString("IC_PAUSA_INTERFAZ_DSCTOE");   //Proyectos
                                icPausaInterfazDistr = rs.getString("IC_PAUSA_INTERFAZ_DISTR");*/     //Proyectos
                            }
			    rs.close();
				try {
					con.cierraStatement();
				} catch (SQLException sqle) {
					System.out.println("cierraStatement(). Error SQL: " + sqle.getMessage());
				}
			    //________________________ Pausa proyectos credito electronico ______________________

				con.cierraConexionDB();

//________________________ FIN Verificaci�n de d�a h�bil ______________________

				System.out.println("** Inicio Ciclo " + apiEjecutar);
				System.out.println(" Hora inicio ciclo:" +fHora.format(new java.util.Date()));
				if (apiEjecutar.equals("DIVERSOS")) {
					System.out.println("* Div");
					Diversos.main(parametros);
				} else if (apiEjecutar.equals("PROYECTOS")) {
					System.out.println("* Proy");
					Proyectos.main(parametros);
				} else if (apiEjecutar.equals("CONTRATOS")) {
					System.out.println("* Contr");
					Contratos.main(parametros);
				} else if (apiEjecutar.equals("PRESTAMOS")) {
					System.out.println("* Prest");
					Prestamos.main(parametros);
				} else if (apiEjecutar.equals("DISPOSICIONES")) {
					System.out.println("* Disp");
					Disposiciones.main(parametros);
				} else if (apiEjecutar.equals("DIVERSOSE")) {
					System.out.println("* Diversos Externas");
					DiversosE.main(parametros);
				} else if (apiEjecutar.equals("PROYECTOSE") && icPausaInterfazCrede.equals("N")  ) {
					System.out.println("* Proyectos Externas");
					ProyectosE.main(parametros);
				} else if (apiEjecutar.equals("CONTRATOSE")) {
					System.out.println("* Contratos Externas");
					ContratosE.main(parametros);
				} else if (apiEjecutar.equals("PRESTAMOSE")) {
					System.out.println("* Prestamos Externas");
					PrestamosE.main(parametros);
				} else if (apiEjecutar.equals("DISPOSICIONESE")) {
					System.out.println("* DISPOSICIONES Externas");
					DisposicionesE.main(parametros);
				} else {
					System.out.println("No se encontro el proceso.");
				}
				System.out.println("** Fin Ciclo " + apiEjecutar);
				System.out.println(" Hora fin ciclo:" +fHora.format(new java.util.Date()));
				Thread.sleep(1000*60*1);	//Dormir 1 minuto
			}
			catch(Exception e)
			{
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
				System.out.println("Error: "+e.getMessage()+" Proceso suspendido por 10 minutos");
				try{Thread.sleep(1000*60*10);}catch(Exception _e){}
			}

		}	//fin del while(ejecutar)
		System.out.println("El proceso ha terminado normalmente");
	}
	/**
	 * M�todo que nos devuelve una lista con los datos parametrizados por interfaz en la pantalla Administraci�n / Interfases / Parametrizaci�n Interfase Descuento 1er Piso 
	 * @param parametro clave del parametro
	 * @param piso 
	 * @param epo clave de la epo 
	 * @param producto 
	 */
	public Map getValueParametrizado(String epo, String piso,String api){
			System.out.println("getValueParametrizado(E)");
			StringBuffer qrySentencia 	= new StringBuffer();
			List conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDBOracle con = new AccesoDBOracle();
			PreparedStatement ps = null;
			ResultSet rs = null;
			HashMap listDatos = new HashMap();
			HashMap dato = new HashMap();
			String listParam = "";
			try{
				con.conexionDB();
				
				qrySentencia.append( "	SELECT IC_PARAMETRO,IG_PARAM_CAMPO "+
											"	FROM COM_PARAM_INTERFASE_DESC  "+
											"	WHERE IC_EPO = "+epo+" "+
											"	AND CC_PRODUCTO = '"+piso+"' "+
											"	AND CC_API = '"+api+"' "+
											"	ORDER BY IC_PARAMETRO "
				);
				System.out.println("getValueParametrizado "+qrySentencia);
				System.out.println("conditions "+conditions);
				ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
		      rs = ps.executeQuery();
				listDatos = new HashMap();
				while (rs.next()){
					
					listDatos.put(rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"),rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
				}
				
				rs.close();
				ps.close();
				if(listDatos.size()<1){
					qrySentencia 	= new StringBuffer();
					System.out.println(" *** CUANDO UNA EPO NO SE ENCUENTRA PARAMETRIZADA SE ENVIARAN LAS VALORES POR DEFAULT ** ");
					qrySentencia.append( "	SELECT IC_PARAMETRO,IG_PARAM_CAMPO  "+
												"	FROM com_matriz_interface  "+
												"	WHERE CC_PRODUCTO = '"+piso+"' "+
												"	AND IG_PARAM_CAMPO IS NOT NULL "+
												"	AND cc_api = '"+api+"' "+
												"	order by IC_PARAMETRO "
					);
					System.out.println("qrySentencia "+qrySentencia.toString());
					System.out.println("conditions "+conditions);
					ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
					rs = ps.executeQuery();
					listDatos = new HashMap();
					while (rs.next()){
						listDatos.put(rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"),rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
					}
					
					rs.close();
					ps.close();
				
				}
			} catch (Exception e) {
				System.out.println("getValueParametrizado  Error: " + e);
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();
				} catch (Exception t) {
					System.out.println("guardaPreguntaGral():: Error al cerrar Recursos: " + t.getMessage());
				}
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
			System.out.println("getValueParametrizado (S)  resultado "+listDatos);
			return listDatos;
	}
	
	
}
