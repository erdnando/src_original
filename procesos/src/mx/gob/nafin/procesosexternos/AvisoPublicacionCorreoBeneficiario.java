package mx.gob.nafin.procesosexternos;

/**
 * Esta clase forma parte del proceso de aviso de publicación por via correo electrónico
 * a los IFs beneficiarios.
 * @author Alberto Cruz Flores
 * @since FODEA 032 - 2010
 */
import com.netro.servicios.Servicios;

import netropology.utilerias.ServiceLocator;

public class AvisoPublicacionCorreoBeneficiario {
	public static void main(String args[])  {
		int codigoSalida = 0;
		try{
			String strDirectorioPublicacion = args[0];//Este parametro es la ruta de publicacion del servidor
			//String strDirectorioPublicacion = "D:/desane10g_local/nafin-web/nafin-web/";//Este parametro es la ruta de publicacion del contenedor local
			if(strDirectorioPublicacion != null && !strDirectorioPublicacion.equals("")){
				procesoAvisoPublicacionCorreoBenef(strDirectorioPublicacion);
			}else{
				System.out.println("..:: PROCESO NO EJECUTADO ::..");
			}
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			codigoSalida = 1;
			System.out.println("..:: ERROR: No se proporciono argumento");
			System.out.println(aiobe.getMessage());
			aiobe.printStackTrace();
		} catch(Throwable ex) {
			codigoSalida = 1;
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}
   }

	private static void procesoAvisoPublicacionCorreoBenef(String strDirectorioPublicacion) throws Throwable {
		System.out.println("procesoFactorajeMovilPublicacion(E)");
		Servicios servicios = obtenerEJB();
		servicios.enviaCorreoBeneficiarioPublicacion(strDirectorioPublicacion);
		System.out.println("procesoFactorajeMovilPublicacion(S)");
	}

	private static Servicios obtenerEJB() throws Throwable {
		System.out.println("obtenerEJB(E)");
		Servicios servicios = ServiceLocator.getInstance().remoteLookup("ServiciosEJB", Servicios.class);
		System.out.println("obtenerEJB(S)");
		return servicios;
	}
}