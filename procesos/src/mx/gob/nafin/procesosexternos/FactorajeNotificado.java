package mx.gob.nafin.procesosexternos;

/**
 * Esta clase se encarga de generar la informaci�n que se guarda en la bit�cora
 * de los documentos notificados negociables y por vencer que han sido operados
 * y vencidos el mes anterior
 * Nafinsa M�vil.
 * @since FODEA 052 - 2010
 * @author Janneth Rebeca Salda�a Waldo
 */
import com.netro.servicios.Servicios;

import netropology.utilerias.ServiceLocator;

public class FactorajeNotificado {
	public static void main(String args[])  {
		int codigoSalida = 0;
		try{
			//Para la regeneraci�n de meses anteriores, se puede pasar
			// mes y anio a generar.
			//Si no se especifican parametros se genera el que correponda.
			generaBitDoctosNotificados(args);
		} catch(ArrayIndexOutOfBoundsException aiobe) {
			System.out.println("Error: No se proporciono argumento");
			System.out.println(aiobe.getMessage());
			aiobe.printStackTrace();
			codigoSalida = 1;
		} catch(Throwable ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			codigoSalida = 1;
		} finally {
			System.exit(codigoSalida);
		}
   }

	private static void generaBitDoctosNotificados(String args[] ) throws Throwable {
		Servicios servicios = obtenerEJB();
		String mes = null;
		String anio = null;
		if (args.length >= 2) {
			mes = args[0];
			anio = args[1];
		}
		servicios.generaBitDoctosNotificados(mes, anio);
		System.out.println("Ejecuto m�todo generaBitDoctosNotificados()");
	}

	private static Servicios obtenerEJB() throws Throwable {
		System.out.println("Inicio Conexion");
		Servicios servicios = ServiceLocator.getInstance().remoteLookup("ServiciosEJB", Servicios.class);
		return servicios;
	}
}