package mx.gob.nafin.procesosexternos;

import com.netro.exception.NafinException;

import java.sql.CallableStatement;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.AccesoDBOracle;
import org.apache.commons.logging.Log;


public class ProcesoSQL {

	private static Log log = ServiceLocator.getInstance().getLog(ProcesoSQL.class);
	
	public static void main(String [] args)  {
		int codigoSalida = 0;		
	   String  nombreProceso ="";		
		try {
			
			try {
				if(args.length>0) {				
		         
					nombreProceso = args[1];
					
		         if (nombreProceso.equals("-")) {
		            nombreProceso = null;
		         }		         						
		      }
								
		    getEjecutarProceso(nombreProceso);
				
				
		   } catch (ArrayIndexOutOfBoundsException eiobe) {
		      System.out.println(" Los parametros no son los correctos,  Favor de verificarlo");
		      codigoSalida = 1;
		   }		   
			
		//} catch (NumberFormatException nfe) {	
		} catch (Exception nfe) {   		
			System.out.println(" Los parametros no son los correctos,  Favor de verificarlo");
			codigoSalida = 1;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
		}finally{
			System.exit(codigoSalida);
		}
	}

	

	static void  getEjecutarProceso(String proceso ) throws Exception{
		
		System.out.println("getEjecutarProceso(E)");
		
		AccesoDBOracle con  = new AccesoDBOracle();
		CallableStatement cs  = null;	   
		String nombreProceso =  proceso+"( ? )";
	   String retorno = null;
		try {
			
			con.conexionDB();		    
			
			System.out.print("nombreProceso: "+nombreProceso );
		    
         cs=con.ejecutaSP(nombreProceso);
		   cs.setString(1, retorno);
		   cs.registerOutParameter(1, Types.VARCHAR);
			cs.executeQuery();
		   
			retorno = cs.getString(1);
		   
		   cs.clearParameters();
		   cs.close();
			
			if(retorno!=null){
				System.out.println(" Error al ejecutar Proceso SQL.  "+retorno);
			}
		   
			
		}catch(Exception e){
			System.out.println("getEjecutarProceso "+e);
			e.printStackTrace();
			throw new Exception("Error al ejecutar Proceso SQL.");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			System.out.println("getEjecutarProceso(S)");
		}
	}
}
