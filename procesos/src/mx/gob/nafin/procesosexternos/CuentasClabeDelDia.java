package mx.gob.nafin.procesosexternos;

import com.netro.dispersion.Dispersion;

import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

public class CuentasClabeDelDia{

	public static void main(String[] args){
		int codigoSalida = 0;
		try{
			procesoGeneraArchivosCtasClabe(args[0]);
		} catch(Throwable e){
			codigoSalida = 1;
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally{
			System.exit(codigoSalida);
		}
	}

	static void procesoGeneraArchivosCtasClabe(String ruta) throws Throwable{

		boolean exito        = false;
		String nombreArchivo = "";

		nombreArchivo = Comunes.cadenaAleatoria(16);
		ruta = ruta + "00tmp/15cadenas/";
		System.out.println("ruta: " + ruta);
		try{
			Dispersion oDispersion = obtenerEJB();
			exito = oDispersion.generarArchivosCuenta("", "", ruta, nombreArchivo, "");
			System.out.println("procesoGeneraArchivosCtasClabe: " + exito);
		} catch(Exception e){
			System.err.println("procesoGeneraArchivosCtasClabe(Exception): " + e);
		}

	}
    private static Dispersion obtenerEJB() throws Throwable {
            System.out.println("Inicio Conexion");
            Dispersion dispersionS = ServiceLocator.getInstance().remoteLookup("DispersionEJB", Dispersion.class);
            return dispersionS;
    }

}