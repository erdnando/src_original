package mx.gob.nafin.procesosexternos;

import com.netro.descuento.CFEEnlace;

import java.text.SimpleDateFormat;

public class CFEEnlCliente {

	public static void main(String args[]) {
		int codigoSalida = 0;
		CargaDoctosEnl cargaDoctos = new CargaDoctosEnl();
		boolean ejecutar = true;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat fHora2 = new SimpleDateFormat ("HHmm");
		int horarioFinal = 2100;
		//int horarioFinal = 1755;
		int horarioActual;

		while (ejecutar) {
			codigoSalida = 0;
			try {
				horarioActual = Integer.parseInt(fHora2.format(new java.util.Date()));
				if (horarioFinal >= horarioActual) {
					System.out.println(" Hora inicio ciclo:" +fHora.format(new java.util.Date()));

					CargaDoctosEnl.proceso(args[0], new CFEEnlace(args[0]));

					System.out.println(" Hora fin ciclo:" +fHora.format(new java.util.Date()));
					Thread.sleep(1000*60*1);	//Dormir 1 minuto
				} else {
					ejecutar = false;
				}

			} catch(ArrayIndexOutOfBoundsException aiobe) {
				codigoSalida = 1;
				System.out.println("Ejem. Debe ejecutarlo --> java CFEEnlCliente 8. El 8 es el n�mero de la EPO para CFE.");
				aiobe.printStackTrace();
				ejecutar = false;
			}catch (Exception e) {
				codigoSalida = 1;
				System.out.println("*****************ERROR EN LA EJECUCION************************");
				e.printStackTrace();
				try{
					System.out.println("SE DETIENE 10 MINUTOS");
					Thread.sleep(1000*60*10);
				} catch(Exception _e){	}
			} catch(Throwable ex) {
                            codigoSalida = 1;
                            System.out.println(ex.getMessage());
                            ex.printStackTrace();
                        }
		} //fin while
		System.out.println("*CODIGO DE SALIDA DE ULTIMA EJECUCION DEL CICLO = " + codigoSalida + "*");
		System.exit(codigoSalida);
	}
}