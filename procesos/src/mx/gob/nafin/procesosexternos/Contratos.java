package mx.gob.nafin.procesosexternos;

/*************************************************************************************
*
* Nombre de Clase o de Archivo: Proyectos.java
*
* Versión: 		  1.0
*
* Fecha Creación: 01/junio/2001
*
* Autor:          Hugo Diaz Garcia
*
* Descripción de Clase: Clase que realiza el proceso de Contratos para Interfases a SIRAC.
*
*************************************************************************************/

/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 21/Mayo/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Dolares, 2o Piso Descuento Electrónico
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 20/Ago/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Financiamiento a Pedidos 2o Piso FODA 074
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación: 08/Sept/2003
*
* Autor Modificacion:          Edgar González Bautista
*
* Descripción: Implementación de Financiamiento a Inventarios 2o Piso FODA 075
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	30/Sept/2003
*
* Autor Modificacion:		L.I. Hugo Díaz García
*
* Descripción: Implementación de Financiamiento a Distribuidores 2o Piso
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	09/Julio/2004
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 062 - 2004, Oficina Tramitadora
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	06/Septiembre/2005
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 058 - 2005, Distribuidores Dólares
*
*
*************************************************************************************/
/*************************************************************************************
* Modificación
*
* Fecha Ult. Modificación:	26/Abril/2006
*
* Autor Modificacion:		Edgar González Bautista
*
* Descripción: Modificación FODA 011 - 2006, Credicadenas de Exportacion
*
*
*************************************************************************************/
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;


public class Contratos
{
	public static void main(String args[])
	{
		String usuario = args[0];
		ResultSet rs;
		ResultSet rs1;
		ResultSet rs2;

		CallableStatement cs=null;
		String sentenciaSQL;
		String sentenciaSQL1;

		String cadena="";

		String folio = "";
		int tasaClave;
		java.sql.Date fecha, fechaDesembolso;
		int codigoAgenciaProy;
		int codigoSubAplicacionProy;
		//int codigoSubAplicacion;
		int codigo_sub_aplicacion_lc = 0; 			//10.- CODIGO_SUB_APLICACION_LC
		long numLineaCredito;
		long numSirac;
		String observaciones ="";
		double montoOperacion;
		String codigoAgencia="";
		String codigoSubAplicacion="";
		String numLineaFondeo="";
		String codigoFinanciera="";
		String aprobadoPor="";
		String proyectosLog="";
		String lineaFinanciera = null; 				//07.- CODIGO_LINEA_FINANCIERA
		int cod_agencia = 90; 						//01.- CODIGO_AGENCIA
		int plazo;
		int icMoneda;

		long numeroContrato=0;
		String estatus="";
		boolean errorDesconocido=false;
		boolean errorTransaccion = false;
		SimpleDateFormat fHora = new SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");

		String codigoAplicacion="";
		String numPrestamoPedido="";
		int existe;
		Hashtable oficTramXProd = new Hashtable();
		String strFrom, strCondiciones, strSelect;
		String strProducto = null;
		String strFechaDiversos = null;
		String strFechaProyectos = null;
		Interfaz  inter = new Interfaz();// Se agrego F002-2016
		int ic_epo;  // Se agrego F001-2016

		AccesoDBOracle con = new AccesoDBOracle();
		try {
			con.conexionDB();
		} catch (Exception e){
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try {
			sentenciaSQL = "select ic_producto_nafin||cc_tipo as llave, cg_agencia_sirac from comrel_cartera_x_producto";
			rs=con.queryDB(sentenciaSQL);
			while(rs.next()) {
				oficTramXProd.put(rs.getString("LLAVE"), rs.getString("CG_AGENCIA_SIRAC"));
			}
			rs.close();
			con.cierraStatement();

		} catch (Exception e){
			oficTramXProd.put("1F", "M");
			oficTramXProd.put("1C", "F");
			oficTramXProd.put("2-", "F");
			oficTramXProd.put("4-", "F");
			oficTramXProd.put("5-", "F");
			System.out.println("Exception. Error al obtener la parametrizacion de Oficina tramitadora, utilizando DEFAULTS: " + e);
		}

		try {
			int tipoPiso;
			String recurso;
			sentenciaSQL = "  SELECT s.ic_folio"   +
						"      , tb.ic_tasa"   +
						"      , c2.ig_codigo_agencia_proy"   +
						"      , c2.ig_codigo_sub_aplicacion_proy"   +
						"      , c2.ig_numero_linea_credito"   +
						"      , c2.cg_descripcion"   +
						"      , c2.fn_monto_operacion"   +
						"      , c2.ig_codigo_agencia"   +
						"      , c2.ig_codigo_sub_aplicacion"   +
						"      , c2.ig_numero_linea_fondeo"   +
						"      , c2.ig_codigo_financiera"   +
						"      , c2.ig_aprobado_por"   +
						"      , DECODE (i.ig_tipo_piso, 2, p.in_numero_sirac, 1, DECODE ( e.cs_opera_primer_piso, 'S', e.ig_numero_sirac, p.in_numero_sirac ) ) AS in_numero_sirac"   +
						"      , s.ig_plazo"   +
						//"      , DECODE(d.ic_moneda, 1, s.ig_plazo, 54, trunc(s.DF_V_DESCUENTO) - trunc(s.DF_FECHA_SOLICITUD) ) as ig_plazo"   +
						"      , i.ig_tipo_piso"   +
						"      , c2.ig_numero_prestamo_pedido"   +
						"      , c2.cs_recurso"   +
						"      , p.in_numero_sirac as in_numero_sirac_pyme"   +
						"      , e.ig_numero_sirac as in_numero_sirac_epo"   +
						" 	   , d.ic_moneda"   +
						" 	   , DECODE(trim(i.cs_tipo), 'B', 210, 'NB', DECODE(i.ic_aval, NULL, 220, 216) ) linea_financiera"   +
						"      , AG.ig_agencia_sirac as ig_agencia_sirac_fiscal"   +
						"      , AG2.ig_agencia_sirac as ig_agencia_sirac_tramit"   +
						"	   , c2.cg_proyectos_log " +
						"	   , c2.cc_producto " +
						"	   , to_char(c2.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
						"	   , to_char(c2.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
						"	   , e.IC_EPO  " + // F001-2016 PARA OBTENER PARAMETRIZACIÓN QUE LE CORRESPONDE A LA EPO 
						"    FROM comrel_tasa_base tb"   +
						"         , com_docto_seleccionado ds"   +
						"         , com_solicitud s"   +
						"         , com_control02 c2"   +
						"         , com_documento d"   +
						"         , comcat_pyme p"   +
						"         , comcat_epo e"   +
						"         , comcat_if i"   +
						"         , com_domicilio dom"   +
						"         , comcat_agencia_sirac ag"   +
						"         , comcat_agencia_sirac ag2"   +
						"         , comrel_pyme_epo pe "   +
						"   WHERE s.ic_documento = ds.ic_documento"   +
						"     AND ds.dc_fecha_tasa = tb.dc_fecha_tasa"   +
						"     AND s.ic_folio = c2.ic_folio"   +
						"     AND s.ic_documento = d.ic_documento"   +
						"     AND d.ic_pyme = p.ic_pyme"   +
						"     AND s.ic_documento = ds.ic_documento"   +
						"     AND ds.ic_if = i.ic_if"   +
						"     AND d.ic_epo = e.ic_epo"   +
						"     AND d.ic_pyme = dom.ic_pyme "   +
						"     AND dom.ic_estado = ag.ic_estado (+) "   +
						"     AND d.ic_pyme = pe.ic_pyme "   +
						"     AND d.ic_epo = pe.ic_epo "   +
						"     AND pe.ic_oficina_tramitadora = ag2.ic_estado (+) "   +
						"     AND c2.cs_estatus = 'S'"   +
						"     AND s.ic_estatus_solic = 2"   +
						"     AND s.ic_bloqueo = 3"   +
						"	  AND dom.cs_fiscal = 'S' " +
						"   ORDER BY i.ig_tipo_piso desc,s.df_fecha_solicitud ASC"  ;

			//System.out.println(sentenciaSQL+";");
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				/*
				Se asigna la variable de numero de prestamo obtenido en el credito de Financiamiento a Pedidos
				para determinar si se va a realizar una cobranza
				*/
				numPrestamoPedido = rs.getString("IG_NUMERO_PRESTAMO_PEDIDO");
				recurso = rs.getString("CS_RECURSO");
				icMoneda = rs.getInt("IC_MONEDA") ;


				tipoPiso=rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_FOLIO");
				tasaClave = rs.getInt("IC_TASA");

				codigoAgenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				codigoSubAplicacionProy = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				//F084-2006
				numLineaCredito = rs.getLong("IG_NUMERO_LINEA_CREDITO");
				observaciones = rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				//F084-2006
				numSirac = ("S".equals(recurso))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");


				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				aprobadoPor = (aprobadoPor==null)?"NULL":"'"+aprobadoPor+"'";
				plazo = rs.getInt("IG_PLAZO");
				proyectosLog = (rs.getString("CG_PROYECTOS_LOG") == null)?"": rs.getString("CG_PROYECTOS_LOG");
				proyectosLog = Comunes.protegeCaracter(proyectosLog, '\'', "\'");

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");
				ic_epo = rs.getInt("IC_EPO"); //F001-2016
				lineaFinanciera = null;

				sentenciaSQL = "select fecha_hoy + "+plazo+", fecha_hoy from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR'";
				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				fecha = rs1.getDate(1);
				fechaDesembolso = rs1.getDate(2);
				rs1.close();
				con.cierraStatement();

				try {
					cs = con.ejecutaSP("LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(ejecutaSP): " + "LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
					throw sqle;
				}

				if (tipoPiso==1) {

					// Foda 062 - 2004, 068 - 2004
					if (numPrestamoPedido==null) {
						// Infonavit Agencia 90
						cod_agencia = 90;
						//cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): rs.getInt("IG_AGENCIA_SIRAC_FISCAL");
					} else {
						cod_agencia = ("T".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL"): 90;
					}
					
					HashMap listDatoParam = (HashMap)inter.getValueParametrizado(String.valueOf(ic_epo),"1C","CN");//  F002-2016
					codigoSubAplicacionProy = Integer.parseInt((String)listDatoParam.get("2"));//  F002-2016
					lineaFinanciera= (String)listDatoParam.get("7");//  F002-2016
					
					cs.setInt(2, codigoSubAplicacionProy);								//02.- CODIGO_SUB_APLICACION
					//lineaFinanciera = "011"; SE COMENTO EN EL  F002-2016
					cs.setString(7, lineaFinanciera);
					//tasaClave = 20; //Modificado Foda 069 Tasas x Plazo
					cs.setInt(8, tasaClave);								//08.- CODIGO_VALOR_TASA_CARTERA
				} else {
					//Foda 062 - 2004, 068 - 2004
					cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL") : 90;

					//02.- CODIGO_SUB_APLICACION
					if (icMoneda==54 ) {
						cs.setInt(2, 221);
					} else {
						cs.setInt(2, 211);
					}
					if (icMoneda==54) {
						lineaFinanciera = rs.getString("LINEA_FINANCIERA");
						cs.setString(7, lineaFinanciera);
					} else {
						cs.setString(7, lineaFinanciera);
					}

					cs.setString(7, null);							//07.- CODIGO_LINEA_FINANCIERA
					cs.setInt(8, tasaClave);						//08.- CODIGO_VALOR_TASA_CARTERA
				}

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setNull(3, Types.INTEGER);					//03.- NUMERO_CONTRATO (out)
				cs.registerOutParameter(3, Types.INTEGER);
				//F084-2006
				cs.setLong(4, numSirac);							//04.- NUMERO SIRAC
				cs.setDate(5, null, Calendar.getInstance());							//05.- FECHA_APERTURA
				cs.setString(6, usuario);						//06.- CODIGO_EJECUTIVO
//					cs.setInt(9, 90);								//09.- CODIGO_AGENCIA_LC
				cs.setInt(9, codigoAgenciaProy);				//09.- CODIGO_AGENCIA_LC
				cs.setInt(10, codigoSubAplicacionProy);			//10.- CODIGO_SUB_APLICACION_LC
				//F084-2006
				cs.setLong(11, numLineaCredito);					//11.- CODIGO_LINEA_CREDITO
				cs.setString(12, null);							//12.- BLOQUEO
				cs.setDate(13, fecha, Calendar.getInstance());							//13.- FECHA_VENCIMIENTO
				cs.setString(14, observaciones);				//14.- OBSERVACIONES
				cs.setDouble(15, montoOperacion);				//15.- MONTO_APROBADO
				cs.setString(16, "D");							//16.- TIPO_CONTRATO
				cs.setString(17, "O");							//17.- CLASE_CONTRATO
				cs.setNull(18, Types.INTEGER);					//18.- CODIGO_GRUPO_COMISION
				cs.setDate(19, fechaDesembolso,Calendar.getInstance());				//19.- FECHA_DESEMBOLSO
				cs.setDouble(20, (double)100);					//20.- PORCENTAJR_AVANCE_PROYECTADO
				cs.setDouble(21, montoOperacion);				//21.- VALOR_DESEMBOLSO_PROYECTADO
				cs.setNull(22, Types.INTEGER);					//22.- CODIGO_TIPO_GARANTIA
				cs.setString(23, null);							//23.- DESCRIPCION
				cs.setNull(24, Types.INTEGER);					//24.- CODIGO_COMISION
				cs.setNull(25, Types.DOUBLE);					//25.- PORCENTAJE_GARANTIA
				cs.setNull(26, Types.DOUBLE);					//26.- PORCENTAJE_COMISION
				cs.setNull(27, Types.INTEGER);					//27.- ESTATUS (out)
				cs.registerOutParameter(27, Types.INTEGER);



				cadena =	"p1:="+cod_agencia+";"+"\n";
				if (tipoPiso==1) {
					cadena += "p2:="+codigoSubAplicacionProy+";"+"\n";
				} else {
					if (icMoneda==54 ) {
						cadena += "p2:=221;"+"\n";
					} else {
						cadena += "p2:=211;"+"\n";
					}
				}
				cadena += "p3:=NULL;"+"\n"+
				"p4:="+numSirac+";"+"\n"+
				"p5:=NULL;"+"\n"+
				"p6:='"+usuario+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "p7:='"+lineaFinanciera+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "p7:='"+lineaFinanciera+"';"+"\n";
					} else {
						cadena += "p7:="+lineaFinanciera+";"+"\n";
					}
				}
				cadena += "p8:="+tasaClave+";"+"\n"+
				"p9:=90;"+"\n"+
				"p10:="+codigoSubAplicacionProy+";"+"\n"+
				"p11:="+numLineaCredito+";"+"\n"+
				"p12:=NULL;"+"\n"+
				"p13:=to_date('"+fecha+"','yyyy-mm-dd');"+"\n"+
				"p14:='"+observaciones+"';"+"\n"+
				"p15:="+montoOperacion+";"+"\n"+
				"p16:='D';"+"\n"+
				"p17:='O';"+"\n"+
				"p18:=NULL;"+"\n"+
				"p19:=to_date('"+fechaDesembolso+"','yyyy-mm-dd');"+"\n"+
				"P20:=100;"+"\n"+
				"p21:="+montoOperacion+";"+"\n"+
				"p22:=NULL;"+"\n"+
				"p23:=NULL;"+"\n"+
				"p24:=NULL;"+"\n"+
				"p25:=NULL;"+"\n"+
				"p26:=NULL;"+"\n"+
				"p27:=NULL;"+"\n";

				System.out.println(cadena);

				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				cs.execute();
				//System.out.println("Se ejecutó cs.executeQuery()");
				try {
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						numeroContrato = cs.getLong(3);
						estatus = cs.getString(27);		//Lo que regresa debe ser un entero o nulo...
						if (estatus == null)
						{
							errorDesconocido = true;
							estatus="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}



				System.out.println("El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato);
				cadena += "El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato;
				if(!estatus.equals("0"))		//Error
				{
					con.terminaTransaccion(false);
					if (!errorDesconocido)
					{
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='CN'";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next())
						{
							codigoAplicacion = rs1.getString(1);
						}
						else
						{
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} //fin if (!errorDesconocido)
					else
					{
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_control05 where ic_folio='"+folio+"'";
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe==0)
					{
						sentenciaSQL = "insert into com_control05(ic_folio,cc_codigo_aplicacion"+
							" , ic_error_proceso, df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion"+
							" , ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por"+
							" , ig_numero_linea_credito, ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy"+
							" , ig_proceso_numero, cc_producto, df_diversos, df_proyectos)"+
							" values('"+folio+"','"+codigoAplicacion+"',"+estatus+
							" , SYSDATE,"+codigoAgencia+","+codigoSubAplicacion+","+numLineaFondeo+
							" , "+codigoFinanciera+","+aprobadoPor+", "+numLineaCredito+
							" , "+codigoAgenciaProy+","+codigoSubAplicacionProy+", 2 " +
							" , '" + strProducto + "', "+ "to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), "+ "to_date('" + strFechaProyectos + "', 'dd/mm/yyyy') )";
					}
					else //Ya existe un error solo se actualiza
					{
						sentenciaSQL = "update com_control05 set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
							" , ic_error_proceso="+estatus+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+codigoAgencia+" "+
							" , ig_codigo_sub_aplicacion="+codigoSubAplicacion+" ,ig_numero_linea_fondeo="+numLineaFondeo+" "+
							" , ig_codigo_financiera="+codigoFinanciera+" ,ig_aprobado_por="+aprobadoPor+
							" , ig_numero_linea_credito="+numLineaCredito+
							" , ig_codigo_agencia_proy="+codigoAgenciaProy+
							" , ig_codigo_sub_aplicacion_proy="+codigoSubAplicacionProy+
							" , ig_proceso_numero = 2"+
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" where ic_folio='"+folio+"'";
					}
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "UPDATE com_control02 " +
								" SET df_proceso = sysdate " +
								" , cs_estatus='N' " +
								" , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" WHERE ic_folio='"+folio+"'";
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error en la actualización del estatus");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en Control05.");
						errorTransaccion = true;
					}
				}
				else	//no hay error
				{
					sentenciaSQL = "delete from com_control02 where ic_folio='"+folio+"'";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "insert into com_control03(ic_folio,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
							"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
							"ig_codigo_sub_aplicacion_proy,ig_numero_contrato,cs_estatus"+
							", ig_numero_prestamo_pedido, cg_proyectos_log, cg_contratos_log "+
							", cc_producto, df_diversos, df_proyectos, df_contratos)"+
							"values('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+","+
							" "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+",'"+observaciones+"',"+
							" "+montoOperacion+","+numLineaCredito+","+codigoAgenciaProy+","+
							" "+codigoSubAplicacionProy+","+numeroContrato+",'S'"+
							" ,"+numPrestamoPedido+", '"+proyectosLog+"','"+ Comunes.protegeCaracter(cadena, '\'', "\'") +"' " +
							" , '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), to_date('"+strFechaProyectos+"', 'dd/mm/yyyy'), SYSDATE)";

//						System.out.println(sentenciaSQL);
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al Insertar los Datos en com_control03");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Eliminar de Control01.");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
					//System.out.println("Se realiza el commit");
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();
			rs.close();
			con.cierraStatement();


			/* Inicio Interfase Financiamiento a Pedidos */
			System.out.println(" Inicio Operaciones Financiamiento a Pedidos ");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("2-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;					;
				strCondiciones = " and P.ic_pyme = pexp.ic_pyme"+
								" and P.ic_epo = pexp.ic_epo"+
								" and P.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("2-").toString())) {
				strFrom = " , com_domicilio D" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and PY.ic_pyme = D.ic_pyme"   +
							"  and D.cs_fiscal = 'S'"   +
							"  and D.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}

			sentenciaSQL = "SELECT A.ic_pedido"+
				", TG.ic_tasa"+
				", C2.ig_codigo_agencia_proy"+
				", C2.ig_codigo_sub_aplicacion_proy"+
				", C2.ig_numero_linea_credito"+
				", C2.cg_descripcion"+
				", C2.fn_monto_operacion"+
				", C2.ig_codigo_agencia"+
				", C2.ig_codigo_sub_aplicacion"+
				", C2.ig_numero_linea_fondeo"+
				", C2.ig_codigo_financiera"+
				", C2.ig_aprobado_por"+
				", C2.cs_recurso "+
				", PY.in_numero_sirac as in_numero_sirac_pyme" +
				", E.ig_numero_sirac  as in_numero_sirac_epo"  +
				", PS.ig_plazo"+
				", I.ig_tipo_piso " +
				", P.ic_moneda " +
				", DECODE(trim(i.cs_tipo), 'B', 210, 'NB', DECODE(i.ic_aval, NULL, 220, 216) ) linea_financiera "  +
				strSelect +
				", C2.cg_proyectos_log " +
				", c2.cc_producto " +
				", to_char(c2.DF_DIVERSOS, 'dd/mm/yyyy') as DF_DIVERSOS " +
				", to_char(c2.DF_PROYECTOS, 'dd/mm/yyyy') as DF_PROYECTOS " +
				" FROM com_tasa_general TG"+
				", com_pedido_seleccionado PS"+
				", com_anticipo A"+
				", com_controlant02 C2"+
				", com_pedido P"+
				", comcat_pyme PY"+
				", comcat_epo E"+
				", comcat_if I"+
				", com_linea_credito LC"+
				strFrom +
				" WHERE A.ic_pedido = PS.ic_pedido "+
				" and PS.ic_tasa_general = TG.ic_tasa_general "+
				" and A.ic_pedido = C2.ic_pedido"+
				" and C2.cs_estatus='S'"+
				" and PS.ic_pedido = P.ic_pedido"+
				" and P.ic_pyme = PY.ic_pyme"+
				" and PS.ic_linea_credito = LC.ic_linea_credito "+
				" and LC.ic_if = I.ic_if "+
				" and P.ic_epo = E.ic_epo"+
				strCondiciones +
				" and A.ic_estatus_antic = 2 "+
				" and A.ic_bloqueo = 3 "+
				" ORDER BY I.ig_tipo_piso desc ";

			//System.out.println(sentenciaSQL+";");
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				recurso = rs.getString("CS_RECURSO");
				folio = rs.getString("IC_PEDIDO");
				tasaClave = rs.getInt("IC_TASA");
				icMoneda = rs.getInt("IC_MONEDA") ;

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 1 */
				//codigoAgenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				codigoAgenciaProy = rs.getInt("IG_AGENCIA_SIRAC");

				codigoSubAplicacionProy = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				//F084-2006
				numLineaCredito = rs.getLong("IG_NUMERO_LINEA_CREDITO");
				observaciones = rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				//numSirac = rs.getInt("IN_NUMERO_SIRAC");
				//F084-2006
				numSirac = ("S".equals(recurso))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");

				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				aprobadoPor = (aprobadoPor==null)?"NULL":"'"+aprobadoPor+"'";
				plazo = rs.getInt("IG_PLAZO");

				proyectosLog = (rs.getString("CG_PROYECTOS_LOG") == null)?"": rs.getString("CG_PROYECTOS_LOG");
				proyectosLog = Comunes.protegeCaracter(proyectosLog, '\'', "\'");

				strProducto = rs.getString("CC_PRODUCTO");
				strFechaDiversos = rs.getString("DF_DIVERSOS");
				strFechaProyectos = rs.getString("DF_PROYECTOS");



				lineaFinanciera = null;

				sentenciaSQL = "select fecha_hoy + "+plazo+", fecha_hoy from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR'";
				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				fecha = rs1.getDate(1);
				fechaDesembolso = rs1.getDate(2);
				rs1.close();
				con.cierraStatement();

				try {
					cs = con.ejecutaSP("LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(ejecutaSP): " + "LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
					throw sqle;
				}

				if (tipoPiso==1) {
					lineaFinanciera = "018";
					cs.setInt(2, codigoSubAplicacionProy);			//02.- CODIGO_SUB_APLICACION
					cs.setString(7, lineaFinanciera);				//07.- CODIGO_LINEA_FINANCIERA
				} else {
					//02.- CODIGO_SUB_APLICACION
					if (icMoneda==54 ) {
						cs.setInt(2, 221);
					} else {
						cs.setInt(2, 211);
					}
					//cs.setString(7, lineaFinanciera);				//07.- CODIGO_LINEA_FINANCIERA
					//07.- CODIGO_LINEA_FINANCIERA
					if (icMoneda==54) {
						lineaFinanciera = rs.getString("LINEA_FINANCIERA");
						cs.setString(7, lineaFinanciera);
					} else {
						cs.setString(7, lineaFinanciera);
					}

				}

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setNull(3, Types.INTEGER);					//03.- NUMERO_CONTRATO (out)
				cs.registerOutParameter(3, Types.INTEGER);
				cs.setLong(4, numSirac);							//04.- NUMERO SIRAC
				cs.setDate(5, null,Calendar.getInstance());							//05.- FECHA_APERTURA
				cs.setString(6, usuario);						//06.- CODIGO_EJECUTIVO
				cs.setInt(8, tasaClave);						//08.- CODIGO_VALOR_TASA_CARTERA
				cs.setInt(9, codigoAgenciaProy);				//09.- CODIGO_AGENCIA_LC
				cs.setInt(10, codigoSubAplicacionProy);			//10.- CODIGO_SUB_APLICACION_LC
				//F084-2006
				cs.setLong(11, numLineaCredito);					//11.- CODIGO_LINEA_CREDITO
				cs.setString(12, null);							//12.- BLOQUEO
				cs.setDate(13, fecha, Calendar.getInstance());							//13.- FECHA_VENCIMIENTO
				cs.setString(14, observaciones);				//14.- OBSERVACIONES
				cs.setDouble(15, montoOperacion);				//15.- MONTO_APROBADO
				cs.setString(16, "D");							//16.- TIPO_CONTRATO
				cs.setString(17, "O");							//17.- CLASE_CONTRATO
				cs.setNull(18, Types.INTEGER);					//18.- CODIGO_GRUPO_COMISION
				cs.setDate(19, fechaDesembolso,Calendar.getInstance());				//19.- FECHA_DESEMBOLSO
				cs.setDouble(20, (double)100);					//20.- PORCENTAJR_AVANCE_PROYECTADO
				cs.setDouble(21, montoOperacion);				//21.- VALOR_DESEMBOLSO_PROYECTADO
				cs.setNull(22, Types.INTEGER);					//22.- CODIGO_TIPO_GARANTIA
				cs.setString(23, null);							//23.- DESCRIPCION
				cs.setNull(24, Types.INTEGER);					//24.- CODIGO_COMISION
				cs.setNull(25, Types.DOUBLE);					//25.- PORCENTAJE_GARANTIA
				cs.setNull(26, Types.DOUBLE);					//26.- PORCENTAJE_COMISION
				cs.setNull(27, Types.INTEGER);					//27.- ESTATUS (out)
				cs.registerOutParameter(27, Types.INTEGER);



				cadena =	"p1:="+cod_agencia+";"+"\n";
				if (tipoPiso==1) {
					cadena += "p2:="+codigoSubAplicacionProy+";"+"\n";
				} else {
					if (icMoneda==54 ) {
						cadena += "p2:=221;"+"\n";
					} else {
						cadena += "p2:=211;"+"\n";
					}
				}
				cadena += "p3:=NULL;"+"\n"+
				"p4:="+numSirac+";"+"\n"+
				"p5:=NULL;"+"\n"+
				"p6:='"+usuario+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "p7:='"+lineaFinanciera+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "p7:='"+lineaFinanciera+"';"+"\n";
					} else {
						cadena += "p7:="+lineaFinanciera+";"+"\n";
					}
				}
				cadena += "p8:="+tasaClave+";"+"\n"+
				"p9:="+codigoAgenciaProy+";"+"\n"+
				"p10:="+codigoSubAplicacionProy+";"+"\n"+
				"p11:="+numLineaCredito+";"+"\n"+
				"p12:=NULL;"+"\n"+
				"p13:=to_date('"+fecha+"','yyyy-mm-dd');"+"\n"+
				"p14:='"+observaciones+"';"+"\n"+
				"p15:="+montoOperacion+";"+"\n"+
				"p16:='D';"+"\n"+
				"p17:='O';"+"\n"+
				"p18:=NULL;"+"\n"+
				"p19:=to_date('"+fechaDesembolso+"','yyyy-mm-dd');"+"\n"+
				"P20:=100;"+"\n"+
				"p21:="+montoOperacion+";"+"\n"+
				"p22:=NULL;"+"\n"+
				"p23:=NULL;"+"\n"+
				"p24:=NULL;"+"\n"+
				"p25:=NULL;"+"\n"+
				"p26:=NULL;"+"\n"+
				"p27:=NULL;"+"\n";

				System.out.println(cadena);

				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
					System.out.println("Se ejecutó cs.executeQuery()");
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						//F084-2006
						numeroContrato = cs.getLong(3);
						estatus = cs.getString(27);		//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							errorDesconocido = true;
							estatus="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}
				System.out.println("El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato);
				cadena += "El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato;
				if(!estatus.equals("0"))		//Error
				{
					con.terminaTransaccion(false);
					if (!errorDesconocido)
					{
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='CN'";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next())
						{
							codigoAplicacion = rs1.getString(1);
						}
						else
						{
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} //fin if (!errorDesconocido)
					else
					{
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from com_controlant05 where ic_pedido = "+folio;
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe==0) {
						sentenciaSQL = "insert into com_controlant05(ic_pedido, cc_codigo_aplicacion"+
							" , ic_error_proceso, df_hora_proceso,ig_codigo_agencia,ig_codigo_sub_aplicacion"+
							" , ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por"+
							" , ig_numero_linea_credito, ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy"+
							" , ig_proceso_numero, cc_producto, df_diversos, df_proyectos)"+
							" values("+folio+",'"+codigoAplicacion+"',"+estatus+
							" , SYSDATE,"+codigoAgencia+","+codigoSubAplicacion+","+numLineaFondeo+
							" , "+codigoFinanciera+","+aprobadoPor+", "+numLineaCredito+
							" , "+codigoAgenciaProy+","+codigoSubAplicacionProy+",2 " +
							" , '" + strProducto + "', to_date('" + strFechaDiversos + "', 'dd/mm/yyyy'), to_date('" + strFechaProyectos + "', 'dd/mm/yyyy') )";

					} else { //Ya existe un error solo se actualiza
						sentenciaSQL = "update com_controlant05 set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
							" , ic_error_proceso="+estatus+" ,df_hora_proceso=SYSDATE ,ig_codigo_agencia="+codigoAgencia+" "+
							" , ig_codigo_sub_aplicacion="+codigoSubAplicacion+" ,ig_numero_linea_fondeo="+numLineaFondeo+" "+
							" , ig_codigo_financiera="+codigoFinanciera+" ,ig_aprobado_por="+aprobadoPor+
							" , ig_numero_linea_credito="+numLineaCredito+
							" , ig_codigo_agencia_proy="+codigoAgenciaProy+
							" , ig_codigo_sub_aplicacion_proy="+codigoSubAplicacionProy+
							" , ig_proceso_numero=2"+
							" , cc_producto = '" + strProducto + "'"+
							" , df_diversos = to_date('" + strFechaDiversos + "', 'dd/mm/yyyy')" +
							" , df_proyectos = to_date('" + strFechaProyectos + "', 'dd/mm/yyyy')" +
							" where ic_pedido = "+folio;
					}
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "UPDATE com_controlant02 " +
									" SET df_proceso = sysdate " +
									" , cs_estatus='N' " +
									" , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									"where ic_pedido = "+folio;
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error en la actualización del estatus");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en ControlAnt05.");
						errorTransaccion = true;
					}
				} else {	//no hay error
					sentenciaSQL = "delete from com_controlant02 where ic_pedido="+folio;
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "insert into com_controlant03(ic_pedido,ig_codigo_agencia,ig_codigo_sub_aplicacion, "+
							"ig_numero_linea_fondeo,ig_codigo_financiera,ig_aprobado_por,cg_descripcion, "+
							"fn_monto_operacion,ig_numero_linea_credito,ig_codigo_agencia_proy, "+
							"ig_codigo_sub_aplicacion_proy,ig_numero_contrato,cs_estatus,"+
							"cg_proyectos_log, cg_contratos_log "+
							", cc_producto, df_diversos, df_proyectos, df_contratos) "+
							"values("+folio+","+codigoAgencia+","+codigoSubAplicacion+","+
							" "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+",'"+observaciones+"',"+
							" "+montoOperacion+","+numLineaCredito+","+codigoAgenciaProy+","+
							" "+codigoSubAplicacionProy+","+numeroContrato+",'S'," +
							" '"+proyectosLog+"','"+ Comunes.protegeCaracter(cadena, '\'', "\'") +"' " +
							" , '"+strProducto+"', to_date('"+strFechaDiversos+"', 'dd/mm/yyyy'), to_date('"+strFechaProyectos+"', 'dd/mm/yyyy'), SYSDATE)";

//						System.out.println(sentenciaSQL);
						try {
							con.ejecutaSQL(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Error al Insertar los Datos en com_controlant03");
							errorTransaccion = true;
						}
					} catch(SQLException sqle) {
						System.out.println("Error al Eliminar de ControlAnt02.");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
					//System.out.println("Se realiza el commit");
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();
			rs.close();
			con.cierraStatement();


			System.out.println(" Fin Operaciones Financiamiento a Pedidos");
			/* Fin Interfase Financiamiento a Pedidos */


			/* Inicio Interfase 1er Piso Credicadenas*/
			System.out.println(" Inicio Operaciones 1er Piso Credicadenas");


			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("5-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and D.ic_pyme = pexp.ic_pyme"+
								" and D.ic_epo = pexp.ic_epo"+
								" and D.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("5-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and PY.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";

			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}



			sentenciaSQL = " SELECT INV.cc_disposicion"   +
							" , D.ic_tasa"   +
							" , INV.ig_codigo_agencia_proy"   +
							" , INV.ig_codigo_sub_aplicacion_proy"   +
							" , INV.ig_numero_linea_credito"   +
							" , INV.cg_descripcion"   +
							" , INV.fn_monto_operacion"   +
							" , INV.ig_codigo_agencia"   +
							" , INV.ig_codigo_sub_aplicacion"   +
							" , INV.ig_numero_linea_fondeo"   +
							" , INV.ig_codigo_financiera"   +
							" , INV.ig_aprobado_por"   +
							" , DECODE(I.ig_tipo_piso, 2, PY.in_numero_sirac, 1, PY.in_numero_sirac ) as in_numero_sirac"   +
							" , d.df_vencimiento - trunc(sysdate) as ig_plazo"   +
							" , I.ig_tipo_piso"   +
							" , T.ic_moneda"   +
							" , DECODE(trim(i.cs_tipo), 'B', 210, 'NB', DECODE(i.ic_aval, NULL, 220, 216) ) linea_financiera"   +
							" , INV.cs_recurso "+
							" , PY.in_numero_sirac as in_numero_sirac_pyme" +
							" , E.ig_numero_sirac  as in_numero_sirac_epo"  +
							" , LC.cg_linea_tipo "  +
							strSelect +
							"  FROM inv_interfase INV"   +
							" , inv_solicitud S"   +
							" , inv_disposicion D"   +
							" , comcat_pyme PY"   +
							" , comcat_epo E"   +
							" , comcat_if I"   +
							" , com_linea_credito LC"   +
							" , comcat_tasa t"   +
							" , comcat_plazo pl"   +
							strFrom   +
							"  WHERE INV.cc_disposicion = S.cc_disposicion"   +
							"  and S.cc_disposicion = D.cc_disposicion"   +
							"  and D.ic_pyme = PY.ic_pyme"   +
							"  and D.ic_plazo = pl.ic_plazo"   +
							"  and D.ic_linea_credito = LC.ic_linea_credito"   +
							"  and LC.ic_if = I.ic_if"   +
							"  and D.ic_epo = E.ic_epo"   +
							"  and D.ic_tasa = t.ic_tasa"   +
							strCondiciones +
							"  AND INV.ig_numero_contrato is null"   +
							"  AND INV.ig_numero_linea_credito is not null"   +
							"  AND INV.cc_codigo_aplicacion is null"   +
							"  AND INV.ic_error_proceso is null"   +
							"  AND S.ic_estatus_solic = 2"   +
							"  AND S.ic_bloqueo = 3"   +
							"  ORDER BY I.ig_tipo_piso desc"  ;

			//System.out.println(sentenciaSQL+";");
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				recurso = rs.getString("CS_RECURSO");
				folio = rs.getString("CC_DISPOSICION");
				tasaClave = rs.getInt("IC_TASA");
				icMoneda = rs.getInt("IC_MONEDA") ;

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");		/* Valor 1 */
				//codigoAgenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				codigoAgenciaProy = rs.getInt("IG_AGENCIA_SIRAC");

				codigoSubAplicacionProy = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				//F084-2006
				numLineaCredito = rs.getLong("IG_NUMERO_LINEA_CREDITO");
				observaciones = rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				//numSirac = rs.getInt("IN_NUMERO_SIRAC");
				//F084-2006
				numSirac = ("S".equals(recurso))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");

				codigoAgencia = rs.getString("IG_CODIGO_AGENCIA");
				codigoSubAplicacion = rs.getString("IG_CODIGO_SUB_APLICACION");
				numLineaFondeo = rs.getString("IG_NUMERO_LINEA_FONDEO");
				codigoFinanciera = rs.getString("IG_CODIGO_FINANCIERA");
				aprobadoPor = rs.getString("IG_APROBADO_POR");
				aprobadoPor = (aprobadoPor==null)?"NULL":"'"+aprobadoPor+"'";
				plazo = rs.getInt("IG_PLAZO");
				String lineaTipo = rs.getString("cg_linea_tipo");
				lineaFinanciera = null;

				sentenciaSQL = "select fecha_hoy + "+plazo+", fecha_hoy from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR'";
				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				fecha = rs1.getDate(1);
				fechaDesembolso = rs1.getDate(2);
				rs1.close();
				con.cierraStatement();

				try {
					cs = con.ejecutaSP("LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(ejecutaSP): " + "LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
					throw sqle;
				}

				if (tipoPiso==1) {
					if("E".equals(lineaTipo)) {
						lineaFinanciera = "024"; // Agregado FODA 011-2006 EGB
					} else {
						lineaFinanciera = "015";
					}
					cs.setInt(2, codigoSubAplicacionProy);			//02.- CODIGO_SUB_APLICACION
					cs.setString(7, lineaFinanciera);				//07.- CODIGO_LINEA_FINANCIERA
				} else {
					//02.- CODIGO_SUB_APLICACION
					if (icMoneda==54 ) {
						cs.setInt(2, 221);
					} else {
						cs.setInt(2, 211);
					}
					//cs.setString(7, lineaFinanciera);				//07.- CODIGO_LINEA_FINANCIERA
					//07.- CODIGO_LINEA_FINANCIERA
					if (icMoneda==54) {
						lineaFinanciera = rs.getString("LINEA_FINANCIERA");
						cs.setString(7, lineaFinanciera);
					} else {
						cs.setString(7, lineaFinanciera);
					}

				}

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setNull(3, Types.INTEGER);					//03.- NUMERO_CONTRATO (out)
				cs.registerOutParameter(3, Types.INTEGER);
				cs.setLong(4, numSirac);							//04.- NUMERO SIRAC
				cs.setDate(5, null,Calendar.getInstance());							//05.- FECHA_APERTURA
				cs.setString(6, usuario);						//06.- CODIGO_EJECUTIVO
				cs.setInt(8, tasaClave);						//08.- CODIGO_VALOR_TASA_CARTERA
				cs.setInt(9, codigoAgenciaProy);				//09.- CODIGO_AGENCIA_LC
				cs.setInt(10, codigoSubAplicacionProy);			//10.- CODIGO_SUB_APLICACION_LC
				//F084-2006
				cs.setLong(11, numLineaCredito);					//11.- CODIGO_LINEA_CREDITO
				cs.setString(12, null);							//12.- BLOQUEO
				cs.setDate(13, fecha,Calendar.getInstance());							//13.- FECHA_VENCIMIENTO
				cs.setString(14, observaciones);				//14.- OBSERVACIONES
				cs.setDouble(15, montoOperacion);				//15.- MONTO_APROBADO
				cs.setString(16, "D");							//16.- TIPO_CONTRATO
				cs.setString(17, "O");							//17.- CLASE_CONTRATO
				cs.setNull(18, Types.INTEGER);					//18.- CODIGO_GRUPO_COMISION
				cs.setDate(19, fechaDesembolso,Calendar.getInstance());				//19.- FECHA_DESEMBOLSO
				cs.setDouble(20, (double)100);					//20.- PORCENTAJR_AVANCE_PROYECTADO
				cs.setDouble(21, montoOperacion);				//21.- VALOR_DESEMBOLSO_PROYECTADO
				cs.setNull(22, Types.INTEGER);					//22.- CODIGO_TIPO_GARANTIA
				cs.setString(23, null);							//23.- DESCRIPCION
				cs.setNull(24, Types.INTEGER);					//24.- CODIGO_COMISION
				cs.setNull(25, Types.DOUBLE);					//25.- PORCENTAJE_GARANTIA
				cs.setNull(26, Types.DOUBLE);					//26.- PORCENTAJE_COMISION
				cs.setNull(27, Types.INTEGER);					//27.- ESTATUS (out)
				cs.registerOutParameter(27, Types.INTEGER);



				cadena =	"p1:="+cod_agencia+";"+"\n";
				if (tipoPiso==1) {
					cadena += "p2:="+codigoSubAplicacionProy+";"+"\n";
				} else {
					if (icMoneda==54 ) {
						cadena += "p2:=221;"+"\n";
					} else {
						cadena += "p2:=211;"+"\n";
					}
				}
				cadena += "p3:=NULL;"+"\n"+
				"p4:="+numSirac+";"+"\n"+
				"p5:=NULL;"+"\n"+
				"p6:='"+usuario+"';"+"\n";
				if (tipoPiso==1) {
					cadena += "p7:='"+lineaFinanciera+"';"+"\n";
				} else {
					if (icMoneda==54) {
						cadena += "p7:='"+lineaFinanciera+"';"+"\n";
					} else {
						cadena += "p7:="+lineaFinanciera+";"+"\n";
					}
				}
				cadena += "p8:="+tasaClave+";"+"\n"+
				"p9:="+codigoAgenciaProy+";"+"\n"+
				"p10:="+codigoSubAplicacionProy+";"+"\n"+
				"p11:="+numLineaCredito+";"+"\n"+
				"p12:=NULL;"+"\n"+
				"p13:=to_date('"+fecha+"','yyyy-mm-dd');"+"\n"+
				"p14:='"+observaciones+"';"+"\n"+
				"p15:="+montoOperacion+";"+"\n"+
				"p16:='D';"+"\n"+
				"p17:='O';"+"\n"+
				"p18:=NULL;"+"\n"+
				"p19:=to_date('"+fechaDesembolso+"','yyyy-mm-dd');"+"\n"+
				"P20:=100;"+"\n"+
				"p21:="+montoOperacion+";"+"\n"+
				"p22:=NULL;"+"\n"+
				"p23:=NULL;"+"\n"+
				"p24:=NULL;"+"\n"+
				"p25:=NULL;"+"\n"+
				"p26:=NULL;"+"\n"+
				"p27:=NULL;"+"\n";

				System.out.println(cadena);

				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				try {
					cs.execute();
					System.out.println("Se ejecutó cs.executeQuery()");
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						//F084-2006
						numeroContrato = cs.getLong(3);
						estatus = cs.getString(27);		//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							errorDesconocido = true;
							estatus="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}
				System.out.println("El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato);
				cadena += "El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato;
				if(!estatus.equals("0"))		//Error
				{
					con.terminaTransaccion(false);
					if (!errorDesconocido)
					{
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
							" where ic_error_proceso="+estatus+
							" and cc_codigo_aplicacion='CN'";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next())
						{
							codigoAplicacion = rs1.getString(1);
						}
						else
						{
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} //fin if (!errorDesconocido)
					else
					{
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from inv_interfase where cc_disposicion='"+folio+"'";
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) {
							sentenciaSQL = "update inv_interfase set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
								" , ic_error_proceso="+estatus+
								" , df_hora_proceso=SYSDATE"+
								" , ig_proceso_numero = 2"+
								" , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" where cc_disposicion= '"+folio+"'";

					}
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
						errorTransaccion = true;
					}
				} else {	//no hay error
					sentenciaSQL = "Update inv_interfase " +
							"	set ig_numero_contrato = " + numeroContrato +
							"   , df_hora_proceso=SYSDATE "+
							"   , DF_CONTRATOS=SYSDATE "+
							"   , ig_proceso_numero = 2"+
							"   , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" WHERE cc_disposicion = '"+folio+"'";
//						System.out.println(sentenciaSQL);
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el numero de contrato");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
					//System.out.println("Se realiza el commit");
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();
			rs.close();
			con.cierraStatement();

			System.out.println(" Fin Operaciones 1er Piso Credicadenas");
			/* Fin Interfase 1er Piso Credicadenas*/


			/* Inicia la Interfase Financiamiento a Distribuidores 2do Piso. */
			System.out.println(" Inicia la Interfase Financiamiento a Distribuidores 2do Piso.");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("4-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("4-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and py.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";

			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}


			// CREDITO EN CUENTA CORRIENTE
			sentenciaSQL = " SELECT /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds py e lc i t)*/ " +
						"	     di.ic_documento " +
						" , ds.ic_tasa" +
						" , di.ig_codigo_agencia_proy" +
						" , di.ig_codigo_sub_aplicacion_proy" +
						" , di.ig_numero_linea_credito" +
						" , di.cg_descripcion" +
						" , di.fn_monto_operacion" +
						" , di.ig_codigo_agencia" +
						" , di.ig_codigo_sub_aplicacion" +
						" , di.ig_numero_linea_fondeo" +
						" , di.ig_codigo_financiera" +
						" , di.ig_aprobado_por" +
						" , DECODE(i.ig_tipo_piso, 2, py.in_numero_sirac, 1, py.in_numero_sirac ) as in_numero_sirac" +
						" , d.ig_plazo_credito as ig_plazo" +
						" , i.ig_tipo_piso" +
						" , t.ic_moneda" +
						" , decode(f.ic_tipo_financiera, 1, 210, 4, 210, 6, 216, 9, 216) as linea_financiera" +
						" , DECODE(trim(i.cs_tipo), 'B', 210, 'NB', DECODE(i.ic_aval, NULL, 220, 216) ) as linea_financiera_dl " +
						" , di.cs_recurso "+
						" , py.in_numero_sirac as in_numero_sirac_pyme" +
						" , e.ig_numero_sirac  as in_numero_sirac_epo"  +
						strSelect   +
						"	   , 'Contratos(Distribuidores)' as proceso" +
						"  FROM dis_solicitud s" +
						" , dis_interfase di" +
						" , dis_documento d" +
						" , dis_docto_seleccionado ds" +
						" , comcat_pyme py" +
						" , comcat_epo e" +
						" , com_linea_credito lc" +
						" , comcat_if i" +
						" , comcat_tasa t" +
						" , comrel_producto_if rpi" +
						" , comcat_financiera f"+
						strFrom +
						"  WHERE s.ic_documento = di.ic_documento" +
						"  and s.ic_documento = ds.ic_documento" +
						"  and s.ic_documento = d.ic_documento" +
						"  and d.ic_pyme = py.ic_pyme" +
						"  and d.ic_linea_credito = lc.ic_linea_credito" +
						"  and lc.ic_if = i.ic_if" +
						"  and lc.ic_if = rpi.ic_if" +
						"  and d.ic_epo = e.ic_epo" +
						"  and ds.ic_tasa = t.ic_tasa" +
						"  and i.ic_financiera = f.ic_financiera"+
						strCondiciones +
						"  and di.ig_numero_contrato is null" +
						"  and di.ig_numero_linea_credito is not null" +
						"  and di.cc_codigo_aplicacion is null" +
						"  and di.ic_error_proceso is null" +
						"  and s.ic_estatus_solic = 2" +
						"  and s.ic_bloqueo = 3" +
						"  and rpi.ic_producto_nafin = 4" +
						"  UNION ALL "+
						// ---DESCUENTO MERCANTIL"
						" SELECT /*+ ordered index(s in_dis_solicitud_01_nuk) use_nl(s di d ds py e lc i t)*/ " +
						"	     di.ic_documento " +
						" , ds.ic_tasa" +
						" , di.ig_codigo_agencia_proy" +
						" , di.ig_codigo_sub_aplicacion_proy" +
						" , di.ig_numero_linea_credito" +
						" , di.cg_descripcion" +
						" , di.fn_monto_operacion" +
						" , di.ig_codigo_agencia" +
						" , di.ig_codigo_sub_aplicacion" +
						" , di.ig_numero_linea_fondeo" +
						" , di.ig_codigo_financiera" +
						" , di.ig_aprobado_por" +
						" , DECODE(i.ig_tipo_piso, 2, py.in_numero_sirac, 1, py.in_numero_sirac ) as in_numero_sirac" +
						" , d.ig_plazo_credito as ig_plazo" +
						" , i.ig_tipo_piso" +
						" , t.ic_moneda" +
						" , decode(f.ic_tipo_financiera, 1, 210, 4, 210, 6, 216, 9, 216) as linea_financiera " +
						" , DECODE(trim(i.cs_tipo), 'B', 210, 'NB', DECODE(i.ic_aval, NULL, 220, 216) ) as linea_financiera_dl " +
						" , di.cs_recurso "+
						" , py.in_numero_sirac as in_numero_sirac_pyme" +
						" , e.ig_numero_sirac  as in_numero_sirac_epo"  +
						strSelect   +
						"	   , 'Contratos(Distribuidores)' as proceso" +
						" FROM dis_solicitud s" +
						"  , dis_interfase di" +
						"  , dis_documento d" +
						"  , dis_docto_seleccionado ds" +
						"  , comcat_pyme py" +
						"  , comcat_epo e" +
						"  , dis_linea_credito_dm lc" +
						"  , comcat_if i" +
						"  , comcat_tasa t" +
						"  , comrel_producto_if rpi" +
						"  , comcat_financiera f"+
						strFrom +
						" WHERE s.ic_documento = di.ic_documento" +
						"   and s.ic_documento = ds.ic_documento" +
						"   and s.ic_documento = d.ic_documento" +
						"   and d.ic_pyme = py.ic_pyme" +
						"   and d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
						"   and lc.ic_if = i.ic_if" +
						"   and lc.ic_if = rpi.ic_if" +
						"   and d.ic_epo = e.ic_epo" +
						"   and ds.ic_tasa = t.ic_tasa" +
						"   and i.ic_financiera = f.ic_financiera"+
						strCondiciones +
						"   and di.ig_numero_contrato is null" +
						"   and di.ig_numero_linea_credito is not null" +
						"   and di.cc_codigo_aplicacion is null" +
						"   and di.ic_error_proceso is null" +
						"   and s.ic_estatus_solic = 2" +
						"   and s.ic_bloqueo = 3" +
						"   and rpi.ic_producto_nafin = 4" +
						" ORDER BY ig_tipo_piso desc";
			//System.out.println(sentenciaSQL+";");
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			while(rs.next()) {
				errorTransaccion = false;
				cod_agencia=rs.getInt("IG_AGENCIA_SIRAC");					/* Valor 1 */
				icMoneda = rs.getInt("IC_MONEDA");
				tipoPiso=rs.getInt("IG_TIPO_PISO");
				recurso = rs.getString("CS_RECURSO");
				folio = rs.getString("IC_DOCUMENTO");
				tasaClave = rs.getInt("IC_TASA");
				codigoAgenciaProy = rs.getInt("IG_CODIGO_AGENCIA_PROY");
				codigoSubAplicacionProy = rs.getInt("IG_CODIGO_SUB_APLICACION_PROY");
				numLineaCredito = rs.getLong("IG_NUMERO_LINEA_CREDITO");
				observaciones = rs.getString("CG_DESCRIPCION");
				/*Si es primer piso se asigna el monto del documento, de lo contrario se envian el importe a recibir */
				montoOperacion = rs.getDouble("FN_MONTO_OPERACION");
				//F084-2006
				numSirac = ("S".equals(recurso))? rs.getLong("IN_NUMERO_SIRAC_PYME"): rs.getLong("IN_NUMERO_SIRAC_EPO");
				plazo = rs.getInt("IG_PLAZO");
				lineaFinanciera = null;

				sentenciaSQL = "select fecha_hoy + "+plazo+", fecha_hoy from mg_calendario "+
								"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BPR'";
				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				fecha = rs1.getDate(1);
				fechaDesembolso = rs1.getDate(2);
				rs1.close();
				con.cierraStatement();

				try {
					cs = con.ejecutaSP("LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
				} catch(SQLException sqle) {
					System.out.println("Contratos.java(ejecutaSP): " + "LC_K_APERTURA_CONTRATO.Pr_Alta_Contrato(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?)");
					throw sqle;
				}


				if (tipoPiso==1) {
					lineaFinanciera = "011";
					codigo_sub_aplicacion_lc = codigoSubAplicacionProy;
					cs.setInt(2, codigoSubAplicacionProy);			//02.- CODIGO_SUB_APLICACION
				} else {
					//07.- CODIGO_LINEA_FINANCIERA
					if (icMoneda==54) {
						lineaFinanciera = rs.getString("LINEA_FINANCIERA_DL");
						codigo_sub_aplicacion_lc = codigoSubAplicacionProy;
						cs.setInt(2, 221);			//02.- CODIGO_SUB_APLICACION
					} else {
						lineaFinanciera = rs.getString("LINEA_FINANCIERA");
						cs.setInt(2, 211);			//02.- CODIGO_SUB_APLICACION
						codigo_sub_aplicacion_lc = 201;
					}
				}

				cs.setInt(1, cod_agencia);						//01.- CODIGO_AGENCIA
				cs.setNull(3, Types.INTEGER);					//03.- NUMERO_CONTRATO (out)
				cs.registerOutParameter(3, Types.INTEGER);
				cs.setLong(4, numSirac);							//04.- NUMERO SIRAC
				cs.setDate(5, null,Calendar.getInstance());							//05.- FECHA_APERTURA
				cs.setString(6, usuario);						//06.- CODIGO_EJECUTIVO
				cs.setString(7, lineaFinanciera);				//07.- CODIGO_LINEA_FINANCIERA
				cs.setInt(8, tasaClave);						//08.- CODIGO_VALOR_TASA_CARTERA
				cs.setInt(9, codigoAgenciaProy);				//09.- CODIGO_AGENCIA_LC
				cs.setInt(10, codigo_sub_aplicacion_lc);		//10.- CODIGO_SUB_APLICACION_LC
				//F084-2006
				cs.setLong(11, numLineaCredito);					//11.- CODIGO_LINEA_CREDITO
				cs.setString(12, null);							//12.- BLOQUEO
				cs.setDate(13, fecha,Calendar.getInstance());							//13.- FECHA_VENCIMIENTO
				cs.setString(14, observaciones);				//14.- OBSERVACIONES
				cs.setDouble(15, montoOperacion);				//15.- MONTO_APROBADO
				cs.setString(16, "D");							//16.- TIPO_CONTRATO
				cs.setString(17, "O");							//17.- CLASE_CONTRATO
				cs.setNull(18, Types.INTEGER);					//18.- CODIGO_GRUPO_COMISION
				cs.setDate(19, fechaDesembolso,Calendar.getInstance());				//19.- FECHA_DESEMBOLSO
				cs.setDouble(20, (double)100);					//20.- PORCENTAJR_AVANCE_PROYECTADO
				cs.setDouble(21, montoOperacion);				//21.- VALOR_DESEMBOLSO_PROYECTADO
				cs.setNull(22, Types.INTEGER);					//22.- CODIGO_TIPO_GARANTIA
				cs.setString(23, null);							//23.- DESCRIPCION
				cs.setNull(24, Types.INTEGER);					//24.- CODIGO_COMISION
				cs.setNull(25, Types.DOUBLE);					//25.- PORCENTAJE_GARANTIA
				cs.setNull(26, Types.DOUBLE);					//26.- PORCENTAJE_COMISION
				cs.setNull(27, Types.INTEGER);					//27.- ESTATUS (out)
				cs.registerOutParameter(27, Types.INTEGER);

				cadena = "p1:="+cod_agencia+";\n";
				if (tipoPiso==1) {
					cadena += "p2:="+codigoSubAplicacionProy+";\n";
				} else {
					if (icMoneda==54 ) {
						cadena += "p2:=221;\n";
					} else {
						cadena += "p2:=211;\n";
					}
				}
				cadena += "p3:=NULL;\n"+
						"p4:="+numSirac+";\n"+
						"p5:=NULL;\n"+
						"p6:='"+usuario+"';\n" +
						"p7:="+lineaFinanciera+";\n" +
						"p8:="+tasaClave+";\n"+
						"p9:=90;\n"+
						"p10:="+codigoSubAplicacionProy+";\n"+
						"p11:="+numLineaCredito+";\n"+
						"p12:=NULL;\n"+
						"p13:=to_date('"+fecha+"','yyyy-mm-dd');\n"+
						"p14:='"+observaciones+"';\n"+
						"p15:="+montoOperacion+";\n"+
						"p16:='D';\n"+
						"p17:='O';\n"+
						"p18:=NULL;\n"+
						"p19:=to_date('"+fechaDesembolso+"','yyyy-mm-dd');\n"+
						"P20:=100;\n"+
						"p21:="+montoOperacion+";\n"+
						"p22:=NULL;\n"+
						"p23:=NULL;\n"+
						"p24:=NULL;\n"+
						"p25:=NULL;\n"+
						"p26:=NULL;\n"+
						"p27:=NULL;\n";
				System.out.println(cadena);

				errorDesconocido=false;
				System.out.println(" Hora inicio ejecucion :" +fHora.format(new java.util.Date()));
				cs.execute();
				System.out.println("Se ejecutó cs.executeQuery()");
				try {
						System.out.println(" Hora respuesta ejecucion :" +fHora.format(new java.util.Date()));
						//F084-2006
						numeroContrato = cs.getLong(3);
						estatus = cs.getString(27);		//Lo que regresa debe ser un entero o nulo...
						if (estatus == null) {
							errorDesconocido = true;
							estatus="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					estatus="9";
				}

				System.out.println("El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato);
				cadena += "El estatus fue: "+estatus + ", Numero Contrato: "+ numeroContrato;
				if(!estatus.equals("0")) {	//Error
					con.terminaTransaccion(false);
					if (!errorDesconocido) {
						sentenciaSQL = "select cc_codigo_aplicacion from comcat_error_procesos "+
										" where ic_error_proceso="+estatus+
										" and cc_codigo_aplicacion='CN'";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						if(rs1.next()) {
							codigoAplicacion = rs1.getString(1);
						} else {
							codigoAplicacion = "NE";
							estatus = "6";
						}
						rs1.close();
						con.cierraStatement();
					} //fin if (!errorDesconocido)
					else {
						codigoAplicacion = "NE";
						if (estatus.equals(""))
							estatus = "6";
					}

					sentenciaSQL = "select count(1) from dis_interfase where ic_documento = "+folio;
					try {
						rs1=con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Contratos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					existe = rs1.getInt(1);
					rs1.close();
					con.cierraStatement();

					if(existe!=0) {
						sentenciaSQL = "update dis_interfase set cc_codigo_aplicacion='"+codigoAplicacion+"' "+
									" , ic_error_proceso="+estatus+
									" , df_hora_proceso=SYSDATE"+
									" , ig_proceso_numero = 2"+
									" , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
									" where ic_documento = "+folio;
					}
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al Insertar o al Actualizar los Datos en inv_interfase.");
						errorTransaccion = true;
					}
				} else {	//no hay error
					sentenciaSQL = "update dis_interfase " +
								" set ig_numero_contrato = " + numeroContrato +
								" , df_hora_proceso=SYSDATE "+
								" , DF_CONTRATOS=SYSDATE "+
								" , ig_proceso_numero = 2"+
								" , cg_contratos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
								" WHERE ic_documento = "+folio;
//						System.out.println(sentenciaSQL);
					try {
						con.ejecutaSQL(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Error al actualizar el numero de contrato");
						errorTransaccion = true;
					}
				}
				if (!errorTransaccion) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while
			if (cs !=null) cs.close();
			rs.close();
			con.cierraStatement();
			System.out.println(" Fin de la Interfase Financiamiento a Distribuidores 2do Piso.");
			/* Fin de la Interfase Financiamiento a Distribuidores 2do Piso. */


		} catch(Exception e) {
			System.out.println("Error: "+e);
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}	//Fin del main();
}//fin de la clase
