package mx.gob.nafin.procesosexternos;

/*************************************************************************************
*
* Nombre de Clase o de Archivo: Diversos.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 01/junio/2001
*
* Autor:          Gilberto E. Aparicio Guerrero
*
* Descripci�n de Clase: Clase que realiza el proceso de Diversos para Interfases a SIRAC.
*
*************************************************************************************/

/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 21/Mayo/2003
*
* Autor Modificacion:          Edgar Gonz�lez Bautista
*
* Descripci�n: Implementaci�n de Dolares, ajuste correspondiente al tipo de cambio en las validaciones
*
*************************************************************************************/

/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 19/Ago/2003
*
* Autor Modificacion:          Edgar Gonz�lez Bautista
*
* Descripci�n: Implementaci�n de Financiamiento a Pedidos 2o Piso FODA 074
*
*************************************************************************************/


/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 03/Sept/2003
*
* Autor Modificacion:          Edgar Gonz�lez Bautista
*
* Descripci�n: Implementaci�n de Financiamiento a Inventarios 2o Piso FODA 075
*
*************************************************************************************/

/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 29/Sept/2003
*
* Autor Modificacion:          Edgar Gonz�lez Bautista
*
* Descripci�n: Implementaci�n de Financiamiento a Distribuidores 2o Piso
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 20/Oct/2003
*
* Autor Modificacion:          Edgar Gonz�lez Bautista
*
* Descripci�n: Foda 049-2003 Bases de Operaci�n
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n: 31/Oct/2003
*
* Autor Modificacion:      Patricia Camarillo
*
* Descripci�n: Foda 085-2003 Validacion del plazo de la solicitud para cobranza de P e I
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	12/Julio/2004
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 062 - 2004, Oficina Tramitadora
*
*
*************************************************************************************/

/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	25/Agosto/2005
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 051 - 2005, Distribuidores Dolares (Descuento Mercantil)
*
*
*************************************************************************************/


import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.Date;
import java.util.Hashtable;

import netropology.utilerias.AccesoDBOracle;


public class Diversos {
	public static void main(String args[]) {
		boolean ejecutar;

		ResultSet rs = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		String sentenciaSQL;
		String sentenciaSQL1;
		String sentenciaSQL2;
		String tipoCambioDl = args[0];

		String folio = "";
		String importeRecibir = "";
		String importeRecibirCalc = "";

		int codigoFinanciera;
		int icMoneda;
		int tipoFinanciera;
		int numLineasInter;
		int numVigentes;
		int numDisponibValidas;
		int plazo = 0;
		int icEpo;
	    int numPagos;

		boolean errorDiversos = false;
		boolean errorTransaccion = false;

		String facultades 			= "";
		String aprobadoPor 			= "";
		String descripcion 			= "";
		String estatus 				= "";
		String codigoAgencia 		= "";
		String codigoSubAplicacion 	= "";
		String numLineaFondeo 		= "";
		String numPrestamoPedido 	= "";
		String fechaSolic 			= "";
		String resultadoVer 		= "";
		String actualizaFecha 		= "";
		String numLineaEmpresarial 	= "";
		String numLineaCredito 		= "";
		String numCodigoEmpresa 	= "";
		String numCodigoAgencia 	= "";
		String numCodigoSubAplic 	= "";
		String numContrato 			= "";
		Hashtable oficTramXProd = new Hashtable();
		int cod_agencia = 0;
		String strFrom, strCondiciones, strSelect;
		String strProducto = null;
		String strCsFacultades = null;
                
		AccesoDBOracle con = new AccesoDBOracle();
                PreparedStatement ps=null;
		try {
			con.conexionDB();
		} catch(Exception e) {
			System.out.println("ERROR EN CONEXION A LA BASE DE DATOS");
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		}

		try {
			sentenciaSQL = "select ic_producto_nafin||cc_tipo as llave, cg_agencia_sirac from comrel_cartera_x_producto";
			rs=con.queryDB(sentenciaSQL);
			while(rs.next()) {
				oficTramXProd.put(rs.getString("LLAVE"), rs.getString("CG_AGENCIA_SIRAC"));
			}
			rs.close();
			con.cierraStatement();

		} catch (Exception e){
			oficTramXProd.put("1F", "M");
			oficTramXProd.put("1C", "F");
			oficTramXProd.put("2-", "F");
			oficTramXProd.put("4-", "F");
			oficTramXProd.put("5-", "F");
			System.out.println("Exception. Error al obtener la parametrizacion de Oficina tramitadora, utilizando DEFAULTS: " + e);
		}

		try {
                    
                    boolean existe_emp=false;
		    sentenciaSQL = "SELECT COUNT(1) FROM emp_linea_credito WHERE ic_estatus_linea = 12";
		    rs=con.queryDB(sentenciaSQL);
                    if(rs.next())
                        if(rs.getInt(1)>0)
                            existe_emp = true;
		    rs.close(); con.cierraStatement();
                    
		    System.out.println(" Inicio Operaciones 1er Piso/Factoraje con Recurso "+new Date());
			sentenciaSQL =
				"  SELECT /*+ use_nl(d, ds, s, dom, pe, cc, ps, a, if, fi, ag, ag2) index (s IN_COM_SOLICITUD_02_NUK) */ "   +
				"      s.ic_folio"   +
				"      , ds.ic_if"   +
				"      , if.ic_financiera"   +
				"      , fi.ic_tipo_financiera"   +
				"      , ds.in_importe_recibir"   +
				"      , nvl(a.ig_numero_prestamo, ds.ig_prestamo_pago) as IG_NUMERO_PRESTAMO_PAGO"   +
				"      , d.ic_epo"   +
				"      , d.ic_moneda"   +
				"      , to_char(s.df_v_descuento,'dd/mm/yyyy') fecha"   +
				" 	   , AG.ig_agencia_sirac as ig_agencia_sirac_fiscal"   +
				" 	   , AG2.ig_agencia_sirac as ig_agencia_sirac_tramit"   +
				"       , TO_NUMBER(NULL) ic_linea_credito_emp " +
				"       ,TO_NUMBER(NULL) ig_numero_linea_credito " +
				"       ,TO_NUMBER(NULL) ig_codigo_empresa " +
				"       ,TO_NUMBER(NULL) ig_codigo_agencia " +
				"       ,TO_NUMBER(NULL) ig_codigo_sub_aplicacion " +
				"       ,TO_NUMBER(NULL) ig_numero_contrato " +
				" 	    ,'Diversos(Descuento 1er Piso)' as OrigenQuery"   +
                                " FROM "+
                                "     com_documento             d, "+
                                "     com_docto_seleccionado    ds, "+
                                "     com_solicitud             s, "+
                                "     com_domicilio             dom, "+
                                "     comrel_pyme_epo           pe, "+
                                "     com_cobro_credito         cc, "+
                                "     com_pedido_seleccionado   ps, "+
                                "     com_anticipo              a, "+
                                "     comcat_if                 if, "+
                                "     comcat_financiera         fi, "+
                                "     comcat_agencia_sirac      ag, "+
                                "     comcat_agencia_sirac      ag2 "+
                                " WHERE "+
                                "    d.ic_documento = ds.ic_documento "+
                                "    AND ds.ic_documento = s.ic_documento     "+
                                "    AND d.ic_pyme = dom.ic_pyme "+
				"     AND ds.ic_if = if.ic_if"   +
				"     AND if.ic_financiera = fi.ic_financiera"   +
				"     AND dom.ic_estado = AG.ic_estado (+)"   +
				"     AND d.ic_pyme = pe.ic_pyme"   +
				"     AND d.ic_epo = pe.ic_epo"   +
				"     AND pe.ic_oficina_tramitadora = AG2.ic_estado (+)"   +
				"     AND ds.ic_documento = cc.ic_documento (+)"   +
				"     AND cc.ic_pedido = ps.ic_pedido (+)"   +
				"     AND ps.ic_pedido = a.ic_pedido (+)"   +
                                "     AND if.ig_tipo_piso = ? "   +
                                "     AND dom.cs_fiscal = ? "  + 
                                "     AND ds.ic_linea_credito_emp is null " +
                                "     AND s.ic_bloqueo IS NULL "   +
                                "     AND s.ic_estatus_solic = ? ";
                    if(existe_emp) {
                        sentenciaSQL +=
				"  UNION "   +
				" SELECT /*+ use_nl(d, ds, s, dom, pe, cc, ps, a, IF, fi, ag, ag2, lc) index (s IN_COM_SOLICITUD_02_NUK)*/ "   +
				"         s.ic_folio,"   +
				"         ds.ic_if, IF.ic_financiera, fi.ic_tipo_financiera,"   +
				"         ds.in_importe_recibir,"   +
				"         NVL (a.ig_numero_prestamo,"   +
				"              ds.ig_prestamo_pago"   +
				"             ) AS ig_numero_prestamo_pago,"   +
				"         d.ic_epo, d.ic_moneda, TO_CHAR (s.df_v_descuento, 'dd/mm/yyyy') fecha,"   +
				"         ag.ig_agencia_sirac ig_agencia_sirac_fiscal,"   +
				"         ag2.ig_agencia_sirac ig_agencia_sirac_tramit, ds.ic_linea_credito_emp,"   +
				"         lc.ig_numero_linea_credito, lc.ig_codigo_empresa,"   +
				"         lc.ig_codigo_agencia, lc.ig_codigo_sub_aplicacion,"   +
				"         lc.ig_numero_contrato,"   +
				"         'Diversos(Factoraje con Recurso)' AS origenquery"   +
                                " FROM "+
                                "     com_documento             d, "+
                                "     com_docto_seleccionado    ds, "+
                                "     com_solicitud             s, "+
                                "     com_domicilio             dom, "+
                                "     comrel_pyme_epo           pe, "+
                                "     com_cobro_credito         cc, "+
                                "     com_pedido_seleccionado   ps, "+
                                "     com_anticipo              a, "+
                                "     comcat_if                 if, "+
                                "     comcat_financiera         fi, "+
                                "     comcat_agencia_sirac      ag, "+
                                "     comcat_agencia_sirac      ag2, "+
                                "     emp_linea_credito lc "   +
                                " WHERE "+
                                "    d.ic_documento = ds.ic_documento "+
                                "    AND ds.ic_documento = s.ic_documento     "+
                                "    AND d.ic_pyme = dom.ic_pyme "+
				"    AND d.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND IF.ic_if = ds.ic_if"   +
				"    AND IF.ic_financiera = fi.ic_financiera"   +
				"    AND dom.ic_estado = ag.ic_estado(+)"   +
				"    AND pe.ic_oficina_tramitadora = ag2.ic_estado(+)"   +
				"    AND ds.ic_documento = cc.ic_documento(+)"   +
				"    AND cc.ic_pedido = ps.ic_pedido(+)"   +
				"    AND ps.ic_pedido = a.ic_pedido(+)"   +
				"    AND ds.ic_linea_credito_emp = lc.ic_linea_credito"   +
				"    AND IF.ig_tipo_piso = ? "   +
				"    AND dom.cs_fiscal = ? "  +
                                "    AND s.ic_bloqueo IS NULL "+
                                "    AND s.ic_estatus_solic = ? ";
                    }
                        System.out.println("\n sentenciaSQLD1FR: "+sentenciaSQL);

			try {
			    ps = con.queryPrecompilado(sentenciaSQL);
			    ps.setInt(1, Integer.valueOf(1));
			    ps.setString(2, "S");
			    ps.setInt(3, Integer.valueOf(1));                            
			    if(existe_emp) {
                                ps.setInt(4, Integer.valueOf(1));
                                ps.setString(5, "S");
                                ps.setInt(6, Integer.valueOf(1));
                            }
			    rs = ps.executeQuery();
			    ps.clearParameters();
			} catch(SQLException sqle) {
				System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}

			while(rs.next()) {
				folio = rs.getString("IC_FOLIO");
				codigoFinanciera = rs.getInt("IC_FINANCIERA");
				importeRecibir = rs.getString("IN_IMPORTE_RECIBIR");
				numPrestamoPedido = rs.getString("IG_NUMERO_PRESTAMO_PAGO");
				resultadoVer="OK";
			    actualizaFecha="";
				errorDiversos = false;
				errorTransaccion = false;

				numLineaEmpresarial = rs.getString("ic_linea_credito_emp")==null?"":rs.getString("ic_linea_credito_emp");

				numLineaCredito 	= rs.getString("ig_numero_linea_credito")==null?"":rs.getString("ig_numero_linea_credito");
				numCodigoEmpresa 	= rs.getString("ig_codigo_empresa")==null?"":rs.getString("ig_codigo_empresa");
				numCodigoAgencia 	= rs.getString("ig_codigo_agencia")==null?"90":rs.getString("ig_codigo_agencia");
				numCodigoSubAplic 	= rs.getString("ig_codigo_sub_aplicacion")==null?"":rs.getString("ig_codigo_sub_aplicacion");
				numContrato 		= rs.getString("ig_numero_contrato")==null?"":rs.getString("ig_numero_contrato");


				if (numPrestamoPedido!=null && !"".equals(numPrestamoPedido)) {
					// Agregado MONITOR INTERFACES 1er Piso Cobranza
					strProducto = "1D";
					/* FODA 85: Verificar el plazo de la solicitud */
					icEpo=rs.getInt("IC_EPO");
				    icMoneda=rs.getInt("IC_MONEDA");
				    fechaSolic=rs.getString("FECHA");
					sentenciaSQL = "SELECT VERIFICAPLAZO("+icEpo+","+icMoneda+",2,1,'"+fechaSolic+"') RESULTADO FROM DUAL ";
					try {
						rs1 = con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					resultadoVer = rs1.getString("RESULTADO");
				    rs1.close();
				    con.cierraStatement();
				    if (resultadoVer.length()>2)
				    {  /* La fecha debe ser actualizada */
				    	actualizaFecha=" , df_ppc=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
				    			 	" , df_ppi=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
				    			 	" , df_v_descuento=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
				    			 	" , in_dia_pago=TO_CHAR(TO_DATE('"+resultadoVer+"','dd/mm/yyyy'),'DD') " +
				    			 	" , ig_plazo=TO_DATE('"+resultadoVer+"','dd/mm/yyyy')-TRUNC(sysdate) ";
				    	resultadoVer="OK";
					}

					// Foda 062 - 2004, 068 - 2004
					cod_agencia = ("T".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL"): 90;

				} else {
					// Infonavit Agencia 90
					cod_agencia = 90;
					// Agregado MONITOR INTERFACES 1er Piso Factoraje
					strProducto = "1C";
				}

				if(!"".equals(numLineaEmpresarial)) {
					cod_agencia = Integer.parseInt(numCodigoAgencia);
					strProducto = "1C";
				}

				if (cod_agencia == 0) {
					resultadoVer ="0"; //Error por oficina tramitadora
				}

				if (!resultadoVer.equals("OK")) {
					System.out.println("Error codigo "+resultadoVer+"\nic_estatus_solic=1 y ic_bloqueo=2");
					sentenciaSQL = "insert into com_control05 "+
						" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
						" values ('"+folio+"',sysdate,"+resultadoVer+",'NE', '"+strProducto+"')";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_solicitud set ic_bloqueo=2 "+
							" where ic_folio='"+folio+"'";
						con.ejecutaSQL(sentenciaSQL);
						errorDiversos = true;
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}
				} else {	//El plazo es correcto

					descripcion = "Ingresado por Nafin electr�nico";//*****
					estatus = "S";

					sentenciaSQL = "update com_solicitud set ic_estatus_solic = 2," +
						" ic_bloqueo = 3 "+actualizaFecha+" where ic_folio='"+folio+"'";
					try {
						con.ejecutaSQL(sentenciaSQL);
						if(!"".equals(numLineaEmpresarial)) {
							sentenciaSQL =
								"INSERT INTO com_control03 " +
								"            (ic_folio, ig_codigo_agencia, ig_codigo_sub_aplicacion, " +
								"             ig_numero_linea_fondeo, ig_codigo_financiera, ig_aprobado_por, " +
								"             cg_descripcion, fn_monto_operacion, ig_numero_linea_credito, " +
								"             ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy, " +
								"             ig_numero_contrato, cs_estatus, ig_numero_prestamo_pedido, " +
								"             cg_proyectos_log, cg_contratos_log, cc_producto, df_diversos, " +
								"             df_proyectos, df_contratos " +
								"            ) " +
								"     VALUES ('"+folio+"', "+numCodigoAgencia+", "+numCodigoSubAplic+", " +
								"             NULL, "+codigoFinanciera+", NULL, " +
								"             '"+descripcion+"', "+importeRecibir+", "+numLineaCredito+", " +
								"             "+numCodigoAgencia+", "+numCodigoSubAplic+", " +
								"             "+numContrato+", 'S', "+numPrestamoPedido+", " +
								"             'NAFIN_EMPRESARIAL', 'NAFIN_EMPRESARIAL', '8', SYSDATE, " +
								"             SYSDATE, SYSDATE " +
								"            ) ";
						} else {
							sentenciaSQL =
								"insert into com_control01 (ic_folio"+
								" , ig_codigo_financiera, cg_descripcion, fn_monto_operacion,"+
								" cg_estatus, ig_numero_prestamo_pedido, cc_producto, df_diversos)"+
								" values ('"+folio+"', "+codigoFinanciera+",'"+descripcion+
								"',"+importeRecibir+",'"+estatus+"',"+ numPrestamoPedido +
								", '"+strProducto+"', SYSDATE)";
						}
//System.out.println(sentenciaSQL);
						con.ejecutaSQL(sentenciaSQL);
						con.terminaTransaccion(true);
					} catch(SQLException sqle) {
						//System.out.println("fallo: " + sentenciaSQL);
						con.terminaTransaccion(false);
					}
			   } // Fin de la verificacion del plazo
			} // Fin del while para primer piso

			rs.close();ps.close();
			//con.cierraStatement();
                    
			System.out.println(" Fin Operaciones 1er Piso/Factoraje con Recurso "+new Date());

			System.out.println(" Inicio Operaciones 2do Piso "+new Date());

			sentenciaSQL = " SELECT /*+ ordered use_nl(ds,d,pe,ag,if,fi,dom,ag2,cc) index(s IN_COM_SOLICITUD_02_NUK) */ "   +
						"     s.ic_folio"   +
						" 	, d.IC_EPO"   +
						" 	, to_char(s.df_v_descuento,'dd/mm/yyyy') fecha "  +
						"   , ds.ic_if as ic_if"   +
						"   , if.ic_financiera"   +
						"   , fi.ic_tipo_financiera"   +
						"   , decode(ds.CS_OPERA_FISO ,'N',ds.in_importe_recibir, 'S', ds.IN_IMPORTE_RECIBIR_FONDEO) as IN_IMPORTE_RECIBIR "  +
						"   , DECODE(d.ic_moneda, 1, decode(ds.CS_OPERA_FISO ,'N',ds.in_importe_recibir, 'S', ds.IN_IMPORTE_RECIBIR_FONDEO), 54, ROUND((decode(ds.CS_OPERA_FISO ,'N',ds.in_importe_recibir, 'S', ds.IN_IMPORTE_RECIBIR_FONDEO) *" + tipoCambioDl + "),2) ) as in_importe_recibir_calc"  +
						"   , d.ic_moneda"   +
						"	, sum(decode(cc.ic_pedido,null,0,1)) numPagos "+
						"	, AG.ig_agencia_sirac as ig_agencia_sirac_fiscal " +
						"	, AG2.ig_agencia_sirac as ig_agencia_sirac_tramit " +
						"	 , NVL(if.CS_FACULTADES,'N') as CS_FACULTADES " +
						"	 , if.IC_MONEDA_FACULTADES " +
						"	 , if.FN_MONTO_FACULTADES " +
						"	,'Diversos(Descuento 2do Piso)' as OrigenQuery " +
						"   FROM com_solicitud s"   +
						"   , com_docto_seleccionado ds"   +
						"   , com_documento d"   +
						"	, comrel_pyme_epo pe " +
						"	, comcat_agencia_sirac AG " +
						"   , comcat_if if"   +
						"   , comcat_financiera fi"   +
						"	, com_domicilio dom " +
						"	, comcat_agencia_sirac AG2 " +
						"   , com_cobro_credito cc "+
						"  WHERE s.ic_documento = ds.ic_documento"   +
						"    AND ds.ic_documento = d.ic_documento"   +
						"    AND ds.ic_if = if.ic_if"   +
						"	 AND cc.ic_documento(+) = d.ic_documento "+
						"    AND if.ic_financiera = fi.ic_financiera"   +
						" 	 AND d.ic_pyme = dom.ic_pyme " +
						" 	 AND dom.ic_estado = AG.ic_estado (+) " +
						" 	 AND d.ic_pyme = pe.ic_pyme " +
						" 	 AND d.ic_epo = pe.ic_epo " +
						" 	 AND pe.ic_oficina_tramitadora = AG2.ic_estado (+) " +
						"    AND s.ic_estatus_solic = 1"   +
						"    AND if.ig_tipo_piso = 2"   +
						"    AND s.ic_bloqueo IS NULL"  +
						"   GROUP BY s.ic_folio"   +
						" 	 , d.IC_EPO "   +
						" 	 , to_char(s.df_v_descuento,'dd/mm/yyyy')"   +
						" 	 , ds.ic_if"   +
						"    , if.ic_financiera"   +
						"    , fi.ic_tipo_financiera"   +
						"    , ds.in_importe_recibir"   +
						"    , DECODE(d.ic_moneda, 1, ds.in_importe_recibir, 54, ROUND((ds.in_importe_recibir*10),2) ) "   +
						"    , d.ic_moneda" +
						"	 , AG.ig_agencia_sirac" +
						"	 , AG2.ig_agencia_sirac " +
						"	 , NVL(if.CS_FACULTADES,'N') " +
						"	 , if.IC_MONEDA_FACULTADES " +
						"	 , if.FN_MONTO_FACULTADES " +
						"   ,ds.IN_IMPORTE_RECIBIR_FONDEO  " +
						"   ,ds.CS_OPERA_FISO  ";

			//System.out.println(sentenciaSQL);
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}

			while(rs.next()) {
				folio = rs.getString("IC_FOLIO");
				codigoFinanciera = rs.getInt("IC_FINANCIERA");
				tipoFinanciera = rs.getInt("IC_TIPO_FINANCIERA");
				importeRecibir = rs.getString("IN_IMPORTE_RECIBIR");
				importeRecibirCalc = rs.getString("IN_IMPORTE_RECIBIR_CALC");
				icMoneda = rs.getInt("IC_MONEDA");
				numPagos = rs.getInt("NUMPAGOS");
				icEpo=rs.getInt("IC_EPO");
				fechaSolic=rs.getString("FECHA");
				String icIF = rs.getString("ic_if"); //F014-2011
				resultadoVer="OK";
			    actualizaFecha="";
//					System.out.println(codigoFinanciera);

				// Agregado MONITOR Factoraje
				strProducto = (icMoneda==1)?"1A":"1B";

					//F014-2011(E)
						String numLineaFondeoIF ="";
						if ("T".equals(oficTramXProd.get("4-").toString())  ||  "T".equals(oficTramXProd.get("1C").toString()) ) {
							sentenciaSQL =	"  SELECT  IG_NUMERO_LINEA_FONDEO  FROM COMREL_PRODUCTO_IF "+
															" WHERE IC_IF = "+icIF;
						if ("T".equals(oficTramXProd.get("4-").toString())) {
									sentenciaSQL +=	" and ic_producto_nafin =4 ";					//4 Distribuidores
							}
							if ("T".equals(oficTramXProd.get("1C").toString())) {
									sentenciaSQL +=	" and ic_producto_nafin =1 ";					//1 Descuento Electronico
							}
						}
						rs1 = con.queryDB(sentenciaSQL);
						if(rs1.next())		{
							numLineaFondeoIF = rs1.getString("IG_NUMERO_LINEA_FONDEO")==null?"":rs1.getString("IG_NUMERO_LINEA_FONDEO");

						}else{
							numLineaFondeoIF ="";
						}
						rs1.close();
						con.cierraStatement();
						//F014-2011(S)
						System.out.println("numLineaFondeoIF "+numLineaFondeoIF);

				sentenciaSQL = "SELECT count(1) numLineasInter" +
					" FROM LC_LINEAS_FONDEO_INTER" +
					" WHERE tipo_financiera = " + tipoFinanciera +
					" AND codigo_financiera = " + codigoFinanciera;
					if(!numLineaFondeoIF.equals("") ){
						sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
					}

				try {
					rs1 = con.queryDB(sentenciaSQL);
				} catch(SQLException sqle) {
					System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
					throw sqle;
				}
				rs1.next();
				numLineasInter = rs1.getInt("NUMLINEASINTER");
				strCsFacultades = rs.getString("CS_FACULTADES");
				rs1.close();
				con.cierraStatement();
				errorDiversos = false;
				errorTransaccion = false;

				if (numLineasInter < 1) {	//No hay Lineas de intermediario
					System.out.println("Error codigo 1\nic_estatus_solic=1 y ic_bloqueo=2");
					sentenciaSQL = "insert into com_control05 "+
						" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
						" values ('"+folio+"',sysdate,1,'NE', '"+strProducto+"')";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
							" where ic_folio='"+folio+"'";
						con.ejecutaSQL(sentenciaSQL);
						errorDiversos = true;
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}
				} else {	//Si hay L�neas de intermediario
					sentenciaSQL = "SELECT count(1) as numVigentes" +
						" FROM LC_LINEAS_FONDEO_INTER" +
						" WHERE fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
						" AND tipo_financiera = " + tipoFinanciera +
						" AND codigo_financiera = " + codigoFinanciera;
						if(!numLineaFondeoIF.equals("") ){
							sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
						}

					try {
						rs1 = con.queryDB(sentenciaSQL);
					} catch(SQLException sqle) {
						System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
						throw sqle;
					}
					rs1.next();
					numVigentes = rs1.getInt("NUMVIGENTES");
					rs1.close();
					con.cierraStatement();

					if (numVigentes <1) //No hay Lineas vigentes
					{
						System.out.println("Error codigo 2\nic_estatus_solic=1 y ic_bloqueo=2");
						sentenciaSQL = "insert into com_control05 "+
							" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
							" values ('"+folio+"',sysdate,2,'NE', '"+strProducto+"')";
						try {
							con.ejecutaSQL(sentenciaSQL);
							sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
								" where ic_folio='"+folio+"'";
							con.ejecutaSQL(sentenciaSQL);
							errorDiversos = true;
						} catch(SQLException sqle) {
							errorTransaccion = true;
						}
					}
					else	//Si hay l�neas vigentes
					{
						sentenciaSQL = "SELECT * FROM ("+
							"	SELECT l.codigo_agencia, l.codigo_sub_aplicacion"+
							" 	, l.numero_linea_fondeo"+
							" 	, (l.monto_asignado - (nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))) as disponible" +
							" 	FROM LC_LINEAS_FONDEO_INTER l" +
							" 	WHERE l.fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
//								" 	AND (l.monto_asignado - nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))>="+importeRecibir+
							" 	AND l.tipo_financiera = " + tipoFinanciera +
							" 	AND l.codigo_financiera = " + codigoFinanciera +
							" 	AND l.codigo_estado = 1 " +
							" ) WHERE disponible >=	"+importeRecibirCalc;
							if(!numLineaFondeoIF.equals("") ){
								sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
							}
							sentenciaSQL +=	" 	AND rownum = 1 " +
							" order by disponible";
						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						numDisponibValidas = 0;

						while(rs1.next())
						{
							numDisponibValidas++;
							codigoAgencia = rs1.getString("CODIGO_AGENCIA");
							codigoSubAplicacion = rs1.getString("CODIGO_SUB_APLICACION");
							numLineaFondeo = rs1.getString("NUMERO_LINEA_FONDEO");
							//Este instrucci�n se aplica ya que nos interesa �nicamente el
							//primer registro (El de disponilidad menor)
							continue;
						}
						rs1.close();
						con.cierraStatement();
						//codigoAgencia = "1";
						//codigoSubAplicacion = "1";
						//numLineaFondeo = "1";
						//numDisponibValidas = 1;

						if (numDisponibValidas<1)	//Verif. Monto vs Disponibilidad
						{
							System.out.println("Error codigo 3\nic_estatus_solic=1 y ic_bloqueo=2");
							sentenciaSQL = "insert into com_control05 "+
								" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
								" values ('"+folio+"',sysdate,3, 'NE', '"+strProducto+"')";
							try {
								con.ejecutaSQL(sentenciaSQL);
								sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
									" where ic_folio='"+folio+"'";
								con.ejecutaSQL(sentenciaSQL);
								errorDiversos = true;
							} catch(SQLException sqle) {
								errorTransaccion = true;
							}
						}
						else	//Si hay disponibilidad >= importe recibir
						{
//								System.out.println("Si hay disponibilidad");
//								System.out.println("Codigo Agencia="+codigoAgencia);
//								System.out.println("Codigo sub aplicacion="+codigoSubAplicacion);
//								System.out.println("Codigo num L�nea fondeo="+numLineaFondeo);

							sentenciaSQL = "select tipo_financiera, codigo_financiera" +
								" , facultades " +
								" from mg_financieras" +
								" where tipo_financiera = " + tipoFinanciera+
								" AND codigo_financiera = " + codigoFinanciera;
							try {
								rs1 = con.queryDB(sentenciaSQL);
							} catch(SQLException sqle) {
								System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
								throw sqle;
							}
							if(rs1.next()) {	//Hay aprobaciones intermediarios?
								facultades = rs1.getString ("FACULTADES");
							} else {
								System.out.println("Error codigo 4\nic_estatus_solic=1 y ic_bloqueo=2");
								sentenciaSQL = "insert into com_control05 "+
									" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
									" values ('"+folio+"',sysdate,4,'NE', '"+strProducto+"')";
								try {
									con.ejecutaSQL(sentenciaSQL);
									sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
										" where ic_folio='"+folio+"'";
									con.ejecutaSQL(sentenciaSQL);
									errorDiversos = true;
								} catch(SQLException sqle) {
									errorTransaccion = true;
								}
							}
							rs1.close();
							con.cierraStatement();

							if (new BigDecimal(facultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {	//facultades < importeRecibirCalc?
								//Buscar aprobaciones por rangos
								sentenciaSQL = "SELECT codigo_aprobacion, cupo_minimo, cupo_limite"+
									" FROM mg_aprobacion"+
									" WHERE "+importeRecibirCalc+" between cupo_minimo and cupo_limite ";
								//System.out.println(sentenciaSQL);
								try {
									rs1 = con.queryDB(sentenciaSQL);
								} catch(SQLException sqle) {
									System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
									throw sqle;
								}
								if(rs1.next()) {	//No est� fuera de rango?
									if ("S".equals(strCsFacultades)) {
										sentenciaSQL = "SELECT ROUND(fn_valor_compra * "+rs.getDouble("FN_MONTO_FACULTADES")+ ",2) "+
												" FROM com_tipo_cambio " +
												" WHERE ic_moneda = " + rs.getInt("IC_MONEDA_FACULTADES") +
												" AND ROWNUM = 1 ORDER BY dc_fecha DESC";
										//System.out.println(sentenciaSQL);
										try {
											rs2 = con.queryDB(sentenciaSQL);
										} catch(SQLException sqle) {
											System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
											throw sqle;
										}
										String montoFacultades = "0";
										if(rs2.next()) {
											montoFacultades = rs2.getString(1);
										}
										rs2.close();
										con.cierraStatement();
										if (new BigDecimal(montoFacultades).compareTo(new BigDecimal(importeRecibir))==-1) {

											System.out.println("Error codigo 14\n\nic_estatus_solic=1 y ic_bloqueo=2");

											System.out.println("Error codigo 14\nic_estatus_solic=1 y ic_bloqueo=2");
											sentenciaSQL = "insert into com_control05 "+
												" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
												" values ('"+folio+"',sysdate,14,'NE', '"+strProducto+"')";
											try {
												con.ejecutaSQL(sentenciaSQL);
												sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
													" where ic_folio='"+folio+"'";
												con.ejecutaSQL(sentenciaSQL);
												errorDiversos = true;
											} catch(SQLException sqle) {
												errorTransaccion = true;
											}
										} else {
											aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
											descripcion = "Descripci�n especial";
											estatus = "N";
										}
									} else {
										aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
										descripcion = "Descripci�n especial";
										estatus = "N";
									}
								} else {
									System.out.println("Error codigo 5\nic_estatus_solic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into com_control05 "+
										" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
										" values ('"+folio+"',sysdate,5,'NE', '"+strProducto+"')";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "update com_solicitud set ic_bloqueo=2"+
											" where ic_folio='"+folio+"'";
										con.ejecutaSQL(sentenciaSQL);
										errorDiversos = true;
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								}
								rs1.close();
								con.cierraStatement();
							}//fin de facultades < importeRecibirCalc?
							else
							{
								aprobadoPor="NULL";
								descripcion="Ingresado por Nafin electr�nico";
								estatus = "S";
							}

							if (!errorDiversos) {
								// Verificacion del plazo
								if (numPagos!=0) { /* FODA 85: Verificar el plazo de la solicitud para Cobranza */
									sentenciaSQL = "SELECT VERIFICAPLAZO("+icEpo+","+icMoneda+",2,2,'"+fechaSolic+"') RESULTADO FROM DUAL ";
									try {
										rs1 = con.queryDB(sentenciaSQL);
									} catch(SQLException sqle) {
										System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
										throw sqle;
									}
									rs1.next();
									resultadoVer = rs1.getString("RESULTADO");
									rs1.close();
									con.cierraStatement();
									if (resultadoVer.length()>2)
									{  /* La fecha debe ser actualizada */
										actualizaFecha=" , df_ppc=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
													" , df_ppi=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
													" , df_v_descuento=TO_DATE('"+resultadoVer+"','dd/mm/yyyy') "+
													" , in_dia_pago=TO_CHAR(TO_DATE('"+resultadoVer+"','dd/mm/yyyy'),'DD') "+
													" , ig_plazo=TO_DATE('"+resultadoVer+"','dd/mm/yyyy')-TRUNC(sysdate) ";
										resultadoVer="OK";
									}
									// Foda 062 - 2004, 068 - 2004
									cod_agencia = ("T".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1C").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL"): 90;

								} else {
									//Foda 062-2004, 068 - 2004
									cod_agencia = ("T".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_TRAMIT"): ("F".equals(oficTramXProd.get("1F").toString()) )? rs.getInt("IG_AGENCIA_SIRAC_FISCAL") : 90;
								}

								if (cod_agencia == 0) {
									resultadoVer ="0"; //Error por oficina tramitadora
								}

								if (!resultadoVer.equals("OK")) {
									System.out.println("Error codigo "+resultadoVer+"\nic_estatus_solic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into com_control05 "+
										" (ic_folio,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
										" values ('"+folio+"',sysdate,"+resultadoVer+",'NE', '"+strProducto+"')";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "update com_solicitud set ic_bloqueo=2 "+
											" where ic_folio='"+folio+"'";
										con.ejecutaSQL(sentenciaSQL);
										errorDiversos = true;
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								} else {	//El plazo es correcto
									System.out.println("Se marca la solicitud "+folio+" como 'En proceso'");
									System.out.println("Aprobado por: "+aprobadoPor);
									System.out.println("Descripcion: "+descripcion);
									System.out.println("Estatus: "+estatus);
									//ic_estatus_solic = 2   EN PROCESO
									//ic_bloqueo = 3   EN PROCESO POR INTERFASE
									sentenciaSQL = "update com_solicitud set ic_estatus_solic = 2, ic_bloqueo = 3"+actualizaFecha+
										" where ic_folio='"+folio+"'";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "delete from com_control01"+
											" where ic_folio = '" + folio + "'";
										con.ejecutaSQL(sentenciaSQL);

										sentenciaSQL = "insert into com_control01 (ic_folio,ig_codigo_agencia"+
											" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
											" , ig_aprobado_por, cg_descripcion, fn_monto_operacion, cg_estatus, in_numero_pagos"+
											" , cc_producto, df_diversos)"+
											" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
											" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
											" , '"+descripcion+"',"+importeRecibir+",'"+estatus+"',"+numPagos+
											" , '"+strProducto+"', SYSDATE )";
											con.ejecutaSQL(sentenciaSQL);
											con.terminaTransaccion(true);
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								} //Fin de la verificacion del plazo

							}//if (!errorDiversos)
						}//fin else Si hay disponibilidad >= importe recibir
					}//fin else Si hay l�neas vigentes
				}//fin else Si hay L�neas de intermediario
				if (errorDiversos && !errorTransaccion ) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}//fin while

			System.out.println(" Fin Operaciones 2do Piso "+new Date());
                    
			/* Inicio Interfase Financiamiento a Pedidos */
			System.out.println(" Inicio Operaciones Financiamiento a Pedidos ");
			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("2-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and P.ic_pyme = pexp.ic_pyme"+
								" and P.ic_epo = pexp.ic_epo"+
								" and P.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("2-").toString())) {
				strFrom = " , com_domicilio D" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and P.ic_pyme = D.ic_pyme"   +
							"  and D.cs_fiscal = 'S'"   +
							"  and D.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}


			sentenciaSQL =
				"SELECT /*+ index (a) use_nl(a ps p IF fi lc tg t pexp ag)*/ " +
				"       a.ic_pedido, lc.ic_if, IF.ic_financiera, fi.ic_tipo_financiera, " +
				"       ps.fn_credito_recibir, " +
				"       DECODE (t.ic_moneda, " +
				"               1, ps.fn_credito_recibir, " +
				"               54, ROUND ((ps.fn_credito_recibir * "+tipoCambioDl+"), 2) " +
				"              ) AS in_importe_recibir_calc, " +
				"       IF.ig_tipo_piso, TO_NUMBER (NULL) ic_linea_credito_emp " +
				strSelect+
				"       ,'2A' AS producto " +
				"       ,TO_NUMBER(NULL) ig_numero_linea_credito " +
				"       ,TO_NUMBER(NULL) ig_codigo_empresa " +
				"       ,TO_NUMBER(NULL) ig_codigo_agencia " +
				"       ,TO_NUMBER(NULL) ig_codigo_sub_aplicacion " +
				"       ,TO_NUMBER(NULL) ig_numero_contrato " +
				"       ,'Diversos(Pedidos)' as OrigenQuery " +
				"  FROM com_anticipo a, " +
				"       com_pedido_seleccionado ps, " +
				"       com_pedido p, " +
				"       comcat_if IF, " +
				"       comcat_financiera fi, " +
				"       com_linea_credito lc, " +
				"       com_tasa_general tg, " +
				"       comcat_tasa t " +
				strFrom+
				" WHERE a.ic_pedido = ps.ic_pedido " +
				"   AND ps.ic_pedido = p.ic_pedido " +
				"   AND ps.ic_linea_credito = lc.ic_linea_credito " +
				"   AND lc.ic_if = IF.ic_if " +
				"   AND IF.ic_financiera = fi.ic_financiera " +
				"   AND ps.ic_tasa_general = tg.ic_tasa_general " +
				"   AND tg.ic_tasa = t.ic_tasa " +
				strCondiciones+
				"   AND a.ic_estatus_antic = 1 " +
				"   AND a.ic_bloqueo IS NULL " +
				"UNION " +
				"SELECT /*+ index (a) use_nl(a ps p IF fi lc tg t pexp ag)*/ " +
				"       a.ic_pedido, lc.ic_if, IF.ic_financiera, fi.ic_tipo_financiera, " +
				"       ps.fn_credito_recibir, " +
				"       DECODE (t.ic_moneda, " +
				"               1, ps.fn_credito_recibir, " +
				"               54, ROUND ((ps.fn_credito_recibir * "+tipoCambioDl+"), 2) " +
				"              ) AS in_importe_recibir_calc, " +
				"       IF.ig_tipo_piso, ps.ic_linea_credito_emp " +
				strSelect+
				"       ,'2A' AS producto " +
				"       , lc.ig_numero_linea_credito " +
				"       , lc.ig_codigo_empresa " +
				"       , lc.ig_codigo_agencia " +
				"       , lc.ig_codigo_sub_aplicacion " +
				"       , lc.ig_numero_contrato " +
				"       ,'Diversos(Fin Contratos)' as OrigenQuery " +
				"  FROM com_anticipo a, " +
				"       com_pedido_seleccionado ps, " +
				"       com_pedido p, " +
				"       comcat_if IF, " +
				"       comcat_financiera fi, " +
				"       emp_linea_credito lc, " +
				"       com_tasa_general tg, " +
				"       comcat_tasa t " +
				strFrom+
				" WHERE a.ic_pedido = ps.ic_pedido " +
				"   AND ps.ic_pedido = p.ic_pedido " +
				"   AND ps.ic_linea_credito_emp = lc.ic_linea_credito " +
				"   AND lc.ic_if = IF.ic_if " +
				"   AND IF.ic_financiera = fi.ic_financiera " +
				"   AND ps.ic_tasa_general = tg.ic_tasa_general " +
				"   AND tg.ic_tasa = t.ic_tasa " +
				strCondiciones+
				"   AND a.ic_estatus_antic = 1 " +
				"   AND a.ic_bloqueo IS NULL ";
/*				"select /*+ index (a) use_nl(a ps p IF fi lc tg t pexp ag)/" +
				"	a.ic_pedido " +
				" , lc.ic_if"+
				" , if.ic_financiera " +
				" , fi.ic_tipo_financiera " +
				" , ps.fn_credito_recibir "+
				" , DECODE(t.ic_moneda, 1, ps.fn_credito_recibir, 54, ROUND((ps.fn_credito_recibir* " + tipoCambioDl + " ),2) ) as in_importe_recibir_calc"  +
				" , if.ig_tipo_piso "+
				" , ps.ic_linea_credito_emp "+
				strSelect   +
				" , 'Diversos(Pedidos)' as proceso" +
				" FROM com_anticipo a " +
				" , com_pedido_seleccionado ps "+
				" , com_pedido P " +
				" , comcat_if if " +
				" , comcat_financiera fi " +
				" , com_linea_credito lc " +
				" , com_tasa_general tg " +
				" , comcat_tasa t " +
				strFrom +
				" WHERE a.ic_pedido = ps.ic_pedido"+
				" AND ps.ic_pedido = p.ic_pedido "+
				" AND ps.ic_linea_credito = lc.ic_linea_credito "+
				" AND lc.ic_if = if.ic_if "+
				" AND if.ic_financiera = fi.ic_financiera " +
				" AND ps.ic_tasa_general = tg.ic_tasa_general " +
				" AND tg.ic_tasa = t.ic_tasa " +
				strCondiciones +
				" AND a.ic_estatus_antic = 1 "+
				" AND a.ic_bloqueo is null ";*/

			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			int tipoPiso = 0;
			int bloqueo = 0;
			int estatusAntic = 0;

			while(rs.next()) {
				errorDiversos 		= false;
				errorTransaccion 	= false;
				tipoPiso 			= rs.getInt("IG_TIPO_PISO");
				folio 				= rs.getString("IC_PEDIDO");
				codigoFinanciera 	= rs.getInt("IC_FINANCIERA");
				importeRecibir 		= rs.getString("FN_CREDITO_RECIBIR");
				tipoFinanciera 		= rs.getInt("IC_TIPO_FINANCIERA");
				importeRecibirCalc 	= rs.getString("IN_IMPORTE_RECIBIR_CALC");
				cod_agencia 		= rs.getInt("IG_AGENCIA_SIRAC");
				numLineaEmpresarial = rs.getString("ic_linea_credito_emp")==null?"":rs.getString("ic_linea_credito_emp");
				strProducto 		= rs.getString("producto");
				numLineaCredito 	= rs.getString("ig_numero_linea_credito")==null?"":rs.getString("ig_numero_linea_credito");
				numCodigoEmpresa 	= rs.getString("ig_codigo_empresa")==null?"":rs.getString("ig_codigo_empresa");
				numCodigoAgencia 	= rs.getString("ig_codigo_agencia")==null?"90":rs.getString("ig_codigo_agencia");
				numCodigoSubAplic 	= rs.getString("ig_codigo_sub_aplicacion")==null?"":rs.getString("ig_codigo_sub_aplicacion");
				numContrato 		= rs.getString("ig_numero_contrato")==null?"":rs.getString("ig_numero_contrato");

				if(!"".equals(numLineaEmpresarial)) {
					cod_agencia = Integer.parseInt(numCodigoAgencia);
				}

				if (cod_agencia == 0) {
					//Error por oficina tramitadora
					System.out.println("Error codigo 12\nic_estatus_solic=1 y ic_bloqueo=2");
					sentenciaSQL = "insert into com_controlant05 "+
						" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
						" values ("+folio+",sysdate, 0,'NE', '"+ strProducto +"')";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update com_anticipo set ic_bloqueo=2 "+
							" where ic_pedido="+folio;
						con.ejecutaSQL(sentenciaSQL);
						errorDiversos = true;
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}
				} else {
					if (tipoPiso==1) {

						bloqueo = (tipoPiso==1)?3:1;
						estatusAntic = (tipoPiso==1)?2:1;

						descripcion = "Ingresado por Nafin electr�nico";//*****
						estatus = "S";

						sentenciaSQL = "update com_anticipo set ic_estatus_antic = " + estatusAntic +
									" , ic_bloqueo = "+ bloqueo +
									" where ic_pedido = "+folio;
						try {
							con.ejecutaSQL(sentenciaSQL);

							if(!"".equals(numLineaEmpresarial)) {
								sentenciaSQL =
									"INSERT INTO com_controlant03 " +
									"            (ic_pedido, ig_codigo_agencia, ig_codigo_sub_aplicacion, " +
									"             ig_numero_linea_fondeo, ig_codigo_financiera, ig_aprobado_por, " +
									"             cg_descripcion, fn_monto_operacion, ig_numero_linea_credito, " +
									"             ig_codigo_agencia_proy, ig_codigo_sub_aplicacion_proy, " +
									"             ig_numero_contrato, cs_estatus, cg_proyectos_log, " +
									"             cg_contratos_log, cc_producto, df_diversos, df_proyectos, " +
									"             df_contratos " +
									"            ) " +
									"     VALUES ("+folio+", "+numCodigoAgencia+", "+numCodigoSubAplic+", " +
									"             NULL, "+codigoFinanciera+", NULL, " +
									"             '"+descripcion+"', "+importeRecibir+", "+numLineaCredito+", " +
									"             "+numCodigoAgencia+", "+numCodigoSubAplic+", " +
									"             "+numContrato+", 'S', 'NAFIN_EMPRESARIAL', " +
									"             'NAFIN_EMPRESARIAL', '8', SYSDATE, SYSDATE, " +
									"             SYSDATE " +
									"            ) ";

							} else {
								sentenciaSQL = "insert into com_controlant01 (ic_pedido "+
								", ig_codigo_financiera"+
								", cg_descripcion"+
								", fn_monto_operacion"+
								", cg_estatus "+
								", cc_producto, df_diversos)"+
								" values ("+folio+", "+codigoFinanciera+",'"+descripcion+
								"',"+importeRecibir+",'"+estatus+"', '"+ strProducto +"', SYSDATE)";
							}
							try {
								con.ejecutaSQL(sentenciaSQL);
								con.terminaTransaccion(true);
							} catch(SQLException sqle) {
								System.out.println("fallo 2: " + sentenciaSQL);
								errorTransaccion = true;
							}
						} catch(SQLException sqle) {
							System.out.println("fallo 1: " + sentenciaSQL);
							errorTransaccion = true;
						}
					} else { // Foda 074 - 2003
					/**************************/
					System.out.println(codigoFinanciera);


						sentenciaSQL = "SELECT count(1) numLineasInter" +
							" FROM LC_LINEAS_FONDEO_INTER" +
							" WHERE tipo_financiera = " + tipoFinanciera +
							" AND codigo_financiera = " + codigoFinanciera;

						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						rs1.next();
						numLineasInter = rs1.getInt("NUMLINEASINTER");
						rs1.close();
						con.cierraStatement();

						if (numLineasInter < 1) {	//No hay Lineas de intermediario
							System.out.println("Error codigo 1\nic_estatus_antic=1 y ic_bloqueo=2");
							sentenciaSQL = "insert into com_controlant05 "+
								" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
								" values ("+folio+", sysdate, 1, 'NE', '"+strProducto+"')";
							try {
								con.ejecutaSQL(sentenciaSQL);
								sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
									" where ic_pedido = "+folio;
								con.ejecutaSQL(sentenciaSQL);
								errorDiversos = true;
							} catch(SQLException sqle) {
								errorTransaccion = true;
							}
						} else {	//Si hay L�neas de intermediario
							sentenciaSQL = "SELECT count(*) as numVigentes" +
								" FROM LC_LINEAS_FONDEO_INTER" +
								" WHERE fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
								" AND tipo_financiera = " + tipoFinanciera +
								" AND codigo_financiera = " + codigoFinanciera;
							try {
								rs1 = con.queryDB(sentenciaSQL);
							} catch(SQLException sqle) {
								System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
								throw sqle;
							}
							rs1.next();
							numVigentes = rs1.getInt("NUMVIGENTES");
							rs1.close();
							con.cierraStatement();

							if (numVigentes <1) //No hay Lineas vigentes
							{
								System.out.println("Error codigo 2\nic_estatus_antic=1 y ic_bloqueo=2");
								sentenciaSQL = "insert into com_controlant05 "+
									" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
									" values ("+folio+",sysdate,2,'NE', '"+strProducto+"')";
								try {
									con.ejecutaSQL(sentenciaSQL);
									sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
										" where ic_pedido = "+folio;
									con.ejecutaSQL(sentenciaSQL);
									errorDiversos = true;
								} catch(SQLException sqle) {
									errorTransaccion = true;
								}
							}
							else	//Si hay l�neas vigentes
							{
								sentenciaSQL = "SELECT * FROM ("+
									"	SELECT l.codigo_agencia, l.codigo_sub_aplicacion"+
									" 	, l.numero_linea_fondeo"+
									" 	, (l.monto_asignado - (nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))) as disponible" +
									" 	FROM LC_LINEAS_FONDEO_INTER l" +
									" 	WHERE l.fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
	//								" 	AND (l.monto_asignado - nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))>="+importeRecibir+
									" 	AND l.tipo_financiera = " + tipoFinanciera +
									" 	AND l.codigo_financiera = " + codigoFinanciera +
									" 	AND l.codigo_estado = 1 " +
									") WHERE disponible >=	"+importeRecibirCalc+
									" 	AND rownum = 1 " +
									" order by disponible";

								try {
									rs1 = con.queryDB(sentenciaSQL);
								} catch(SQLException sqle) {
									System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
									throw sqle;
								}
								numDisponibValidas = 0;

								while(rs1.next())
								{
									numDisponibValidas++;
									codigoAgencia = rs1.getString("CODIGO_AGENCIA");
									codigoSubAplicacion = rs1.getString("CODIGO_SUB_APLICACION");
									numLineaFondeo = rs1.getString("NUMERO_LINEA_FONDEO");
									//Este instrucci�n se aplica ya que nos interesa �nicamente el
									//primer registro (El de disponilidad menor)
									continue;
								}
								rs1.close();
								con.cierraStatement();

								if (numDisponibValidas<1)	//Verif. Monto vs Disponibilidad
								{
									System.out.println("Error codigo 3\nic_estatus_antic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into com_controlant05 "+
										" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
										" values ("+folio+",sysdate,3, 'NE', '"+strProducto+"')";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
											" where ic_pedido = "+folio;
										con.ejecutaSQL(sentenciaSQL);
										errorDiversos = true;
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								}
								else	//Si hay disponibilidad >= importe recibir
								{
	//								System.out.println("Si hay disponibilidad");
	//								System.out.println("Codigo Agencia="+codigoAgencia);
	//								System.out.println("Codigo sub aplicacion="+codigoSubAplicacion);
	//								System.out.println("Codigo num L�nea fondeo="+numLineaFondeo);

									sentenciaSQL = "select tipo_financiera, codigo_financiera" +
										" , facultades " +
										" from mg_financieras" +
										" where tipo_financiera = " + tipoFinanciera+
										" AND codigo_financiera = " + codigoFinanciera;
									try {
										rs1 = con.queryDB(sentenciaSQL);
									} catch(SQLException sqle) {
										System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
										throw sqle;
									}
									if(rs1.next()) {	//Hay aprobaciones intermediarios?
										facultades = rs1.getString ("FACULTADES");
									} else {
										System.out.println("Error codigo 4\nic_estatus_antic=1 y ic_bloqueo=2");
										sentenciaSQL = "insert into com_controlant05 "+
											" (ic_pedido, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
											" values ("+folio+",sysdate,4,'NE', '"+strProducto+"')";
										try {
											con.ejecutaSQL(sentenciaSQL);
											sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
												" where ic_pedido = "+folio;
											con.ejecutaSQL(sentenciaSQL);
											errorDiversos = true;
										} catch(SQLException sqle) {
											errorTransaccion = true;
										}
									}
									rs1.close();
									con.cierraStatement();

									if (new BigDecimal(facultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {	//facultades < importeRecibirCalc?
										//Buscar aprobaciones por rangos
										sentenciaSQL = "SELECT codigo_aprobacion, cupo_minimo, cupo_limite"+
											" FROM mg_aprobacion"+
											" WHERE "+importeRecibirCalc+" between cupo_minimo and cupo_limite ";
										//System.out.println(sentenciaSQL);
										try {
											rs1 = con.queryDB(sentenciaSQL);
										} catch(SQLException sqle) {
											System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
											throw sqle;
										}
										if(rs1.next()) {	//No est� fuera de rango?
											aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
											descripcion = "Descripci�n especial";
											estatus = "N";
										} else {
											System.out.println("Error codigo 5\nic_estatus_antic=1 y ic_bloqueo=2");
											sentenciaSQL = "insert into com_controlant05 "+
												" (ic_pedido, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, cc_producto)"+
												" values ("+folio+",sysdate,5,'NE', '"+strProducto+"')";
											try {
												con.ejecutaSQL(sentenciaSQL);
												sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
													" where ic_pedido = "+folio;
												con.ejecutaSQL(sentenciaSQL);
												errorDiversos = true;
											} catch(SQLException sqle) {
												errorTransaccion = true;
											}
										}
										rs1.close();
										con.cierraStatement();
									}//fin de facultades < importeRecibirCalc?
									else {
										aprobadoPor="NULL";
										descripcion="Ingresado por Nafin electr�nico";
										estatus = "S";
									}

									if (!errorDiversos) {

										System.out.println("Se marca el anticipo "+folio+" como 'En proceso'");
										System.out.println("Aprobado por: "+aprobadoPor);
										System.out.println("Descripcion: "+descripcion);
										System.out.println("Estatus: "+estatus);
										//ic_estatus_antic = 2   EN PROCESO
										//ic_bloqueo = 3   EN PROCESO POR INTERFASE
										sentenciaSQL = "update com_anticipo set ic_estatus_antic = 2, ic_bloqueo = 3"+
											" where ic_pedido = "+folio;
										try {
											con.ejecutaSQL(sentenciaSQL);
											sentenciaSQL = "delete from com_controlant01"+
															" where ic_pedido = " + folio ;
											con.ejecutaSQL(sentenciaSQL);

											sentenciaSQL = "insert into com_controlant01 (ic_pedido, ig_codigo_agencia"+
												" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
												" , ig_aprobado_por, cg_descripcion, fn_monto_operacion, cg_estatus " +
												" , cc_producto, df_diversos)"+
												" values ("+folio+","+codigoAgencia+","+codigoSubAplicacion+
												" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
												" , '"+descripcion+"',"+importeRecibir+",'"+estatus+"'"+
												" , '"+strProducto+"', SYSDATE )";
											con.ejecutaSQL(sentenciaSQL);
											con.terminaTransaccion(true);
										} catch(SQLException sqle) {
											errorTransaccion = true;
										}
									}//if (!errorDiversos)
								}//fin else Si hay disponibilidad >= importe recibir
							}//fin else Si hay l�neas vigentes
						}//fin else Si hay L�neas de intermediario

					/**************************/
					}
				}
				if (errorDiversos && !errorTransaccion ) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}

			}

			rs.close();
			con.cierraStatement();

			System.out.println(" Fin Operaciones Financiamiento a Pedidos");
			/* Fin Interfase Financiamiento a Pedidos */

			/* Inicio Interfase 1er Piso Credicadenas */
			System.out.println(" Inicio Operaciones 1er Piso Credicadenas ");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("5-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("5-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;
				strCondiciones = "  and d.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}

			sentenciaSQL =
				" select s.cc_disposicion"   +
				"  , lc.ic_if"   +
				"  , if.ic_financiera"   +
				"  , fi.ic_tipo_financiera"   +
				"  , d.fn_monto_credito"   +
				"  , DECODE(t.ic_moneda, 1, d.fn_monto_credito, 54, ROUND((d.fn_monto_credito * " + tipoCambioDl + " ),2) ) as in_importe_recibir_calc"   +
				"  , if.ig_tipo_piso"   +
				"       ,'5A' AS producto " +
				"  , d.ic_linea_credito_emp "   +
				"       ,TO_NUMBER(NULL) ig_numero_linea_credito " +
				"       ,TO_NUMBER(NULL) ig_codigo_empresa " +
				"       ,TO_NUMBER(NULL) ig_codigo_agencia " +
				"       ,TO_NUMBER(NULL) ig_codigo_sub_aplicacion " +
				"       ,TO_NUMBER(NULL) ig_numero_contrato " +
				strSelect   +
				"  ,'Diversos(Credicadenas)' as OrigenQuery " +
				"  FROM inv_solicitud s " +
				"  , inv_disposicion d "   +
				"  , comcat_if if " +
				"  , comcat_financiera fi " +
				"  , com_linea_credito lc "   +
				"  , comcat_tasa t "   +
				strFrom +
				"  WHERE s.cc_disposicion = d.cc_disposicion"   +
				"  AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"  AND lc.ic_if = if.ic_if"   +
				"  AND if.ic_financiera = fi.ic_financiera"   +
				"  AND d.ic_tasa = t.ic_tasa"  +
				strCondiciones +
				"  AND s.ic_estatus_solic = 1"   +
				"  AND s.ic_bloqueo is null" +
				" UNION "   +
				" select s.cc_disposicion"   +
				"  , lc.ic_if"   +
				"  , if.ic_financiera"   +
				"  , fi.ic_tipo_financiera"   +
				"  , d.fn_monto_credito"   +
				"  , DECODE(t.ic_moneda, 1, d.fn_monto_credito, 54, ROUND((d.fn_monto_credito * " + tipoCambioDl + " ),2) ) as in_importe_recibir_calc"   +
				"  , if.ig_tipo_piso"   +
				"       ,'5A' AS producto " +
				"  , d.ic_linea_credito_emp "   +
				"       , lc.ig_numero_linea_credito " +
				"       , lc.ig_codigo_empresa " +
				"       , lc.ig_codigo_agencia " +
				"       , lc.ig_codigo_sub_aplicacion " +
				"       , lc.ig_numero_contrato " +
				strSelect   +
				"  ,'Diversos(Lib Disp - Fac Exp)' as OrigenQuery " +
				"  FROM inv_solicitud s " +
				"  , inv_disposicion d "   +
				"  , comcat_if if " +
				"  , comcat_financiera fi " +
				"  , emp_linea_credito lc "   +
				"  , comcat_tasa t "   +
				strFrom +
				"  WHERE s.cc_disposicion = d.cc_disposicion"   +
				"  AND d.ic_linea_credito_emp = lc.ic_linea_credito"   +
				"  AND lc.ic_if = if.ic_if"   +
				"  AND if.ic_financiera = fi.ic_financiera"   +
				"  AND d.ic_tasa = t.ic_tasa"  +
				strCondiciones +
				"  AND s.ic_estatus_solic = 1"   +
				"  AND s.ic_bloqueo is null" ;

//System.out.println(sentenciaSQL);
			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			tipoPiso = 0;
			bloqueo = 0;
			estatusAntic = 0;

			while(rs.next()) {
				errorDiversos 			= false;
				errorTransaccion 		= false;
				tipoPiso 				= rs.getInt("IG_TIPO_PISO");
				folio 					= rs.getString("CC_DISPOSICION");
				codigoFinanciera 		= rs.getInt("IC_FINANCIERA");
				importeRecibir 			= rs.getString("FN_MONTO_CREDITO");
				tipoFinanciera 			= rs.getInt("IC_TIPO_FINANCIERA");
				importeRecibirCalc 		= rs.getString("IN_IMPORTE_RECIBIR_CALC");
				//System.out.println(tipoPiso);
				cod_agencia 			= rs.getInt("IG_AGENCIA_SIRAC");
				numLineaEmpresarial 	= rs.getString("ic_linea_credito_emp")==null?"":rs.getString("ic_linea_credito_emp");
				numLineaCredito 		= rs.getString("ig_numero_linea_credito")==null?"":rs.getString("ig_numero_linea_credito");
				numCodigoEmpresa 		= rs.getString("ig_codigo_empresa")==null?"":rs.getString("ig_codigo_empresa");
				numCodigoAgencia 		= rs.getString("ig_codigo_agencia")==null?"":rs.getString("ig_codigo_agencia");
				numCodigoSubAplic 		= rs.getString("ig_codigo_sub_aplicacion")==null?"":rs.getString("ig_codigo_sub_aplicacion");
				numContrato 			= rs.getString("ig_numero_contrato")==null?"":rs.getString("ig_numero_contrato");

				//strProducto = (tipoPiso == 1)?"5A":"5A";
				strProducto 		= rs.getString("producto");

				if(!"".equals(numLineaEmpresarial)) {
					cod_agencia = Integer.parseInt(numCodigoAgencia);
				}

//cod_agencia = 0;

				if (cod_agencia == 0 && tipoPiso == 1) {

					System.out.println("Error codigo 12\nic_estatus_solic=1 y ic_bloqueo=2");
					sentenciaSQL =
						"insert into inv_interfase "+
						" (cc_disposicion,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
						" values ('"+folio+"', sysdate, 0, 'NE', '"+ strProducto +"')";
		 			try {
//System.out.println("sentenciaSQL: "+sentenciaSQL);
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update inv_solicitud set ic_bloqueo=2"+
							" where cc_disposicion = '"+folio+"'";
						con.ejecutaSQL(sentenciaSQL);
						errorDiversos = true;
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}
				} else {

					//if (tipoPiso==1) {

					bloqueo = (tipoPiso==1)?3:1;
					estatusAntic = (tipoPiso==1)?2:1;

					descripcion = "Ingresado por Nafin electr�nico";//*****
					estatus = "S";
					sentenciaSQL =
						"update inv_solicitud set ic_estatus_solic = " + estatusAntic +
						" , ic_bloqueo = "+ bloqueo +
						" where cc_disposicion = '"+folio+"'";
					try {
//System.out.println("\n sentenciaSQL:"+sentenciaSQL);
						con.ejecutaSQL(sentenciaSQL);
						if (tipoPiso==1) { // TEMPORAL QUITAR CUANDO SE IMPLEMENTE 2o Piso

							if("".equals(numLineaEmpresarial)) {
								sentenciaSQL = "insert into inv_interfase (cc_disposicion "+
									", ig_codigo_financiera"+
									", cg_descripcion"+
									", fn_monto_operacion"+
									", df_hora_proceso "+
									", CC_PRODUCTO "+
									", DF_DIVERSOS )"+
									" values ('"+folio+"', "+codigoFinanciera+",'"+descripcion+
									"',"+importeRecibir+", SYSDATE,'"+ strProducto +"', SYSDATE)";
							} else {
								sentenciaSQL =
									"INSERT INTO inv_interfase " +
									"            (cc_disposicion, ig_codigo_financiera, cg_descripcion, " +
									"             fn_monto_operacion, df_hora_proceso, cc_producto, df_diversos, " +
//"--PROYECTOS " +
									"             ig_numero_linea_credito, ig_codigo_agencia_proy, " +
									"             ig_codigo_sub_aplicacion_proy, df_proyectos, " +
									"             cs_recurso, cg_proyectos_log, " +
//"--CONTRATOS " +
									"             ig_numero_contrato, df_contratos, " +
									"             ig_proceso_numero, cg_contratos_log, ig_numero_prestamo " +
									"            ) " +
									"     VALUES ('"+folio+"', "+codigoFinanciera+", '"+descripcion+"', " +
									"             "+importeRecibir+", SYSDATE, '"+strProducto+"', SYSDATE, " +
//"--PROYECTOS " +
									"             "+numLineaCredito+", "+numCodigoAgencia+", " +
									"             "+numCodigoSubAplic+", SYSDATE, " +
									"             'S', 'NAFIN_EMPRESARIAL', " +
//"--CONTRATOS " +
									"             "+numContrato+", SYSDATE, " +
									"             2, 'NAFIN_EMPRESARIAL', NULL " +
									"            ) ";
							}
							try {
//System.out.println("\n sentenciaSQL:"+sentenciaSQL);
								con.ejecutaSQL(sentenciaSQL);
								con.terminaTransaccion(true);
							} catch(SQLException sqle) {
								System.out.println("fallo 2: " + sentenciaSQL);
								errorTransaccion = true;
							}
						} else {
							con.terminaTransaccion(true);
						}
					} catch(SQLException sqle) {
						System.out.println("fallo 1: " + sentenciaSQL);
						errorTransaccion = true;
					}
					/*} else {
					System.out.println(codigoFinanciera);


						sentenciaSQL = "SELECT count(*) numLineasInter" +
							" FROM LC_LINEAS_FONDEO_INTER" +
							" WHERE tipo_financiera = " + tipoFinanciera +
							" AND codigo_financiera = " + codigoFinanciera;

						rs1 = con.queryDB(sentenciaSQL);
						rs1.next();
						numLineasInter = rs1.getInt("NUMLINEASINTER");
						rs1.close();
						con.cierraStatement();
						errorDiversos = false;
						errorTransaccion = false;

						if (numLineasInter < 1) {	//No hay Lineas de intermediario
							System.out.println("Error codigo 1\nic_estatus_antic=1 y ic_bloqueo=2");
							sentenciaSQL = "insert into com_controlant05 "+
								" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion)"+
								" values ("+folio+", sysdate, 1, 'NE')";
							if (con.ejecutaSQL(sentenciaSQL)) {
								sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
									" where ic_pedido = "+folio;
								if (con.ejecutaSQL(sentenciaSQL)) {
									errorDiversos = true;
								} else {
									errorTransaccion = true;
								}
							} else {
								errorTransaccion = true;
							}
						} else {	//Si hay L�neas de intermediario
							sentenciaSQL = "SELECT count(*) as numVigentes" +
								" FROM LC_LINEAS_FONDEO_INTER" +
								" WHERE fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
								" AND tipo_financiera = " + tipoFinanciera +
								" AND codigo_financiera = " + codigoFinanciera;
							rs1 = con.queryDB(sentenciaSQL);
							rs1.next();
							numVigentes = rs1.getInt("NUMVIGENTES");
							rs1.close();
							con.cierraStatement();

							if (numVigentes <1) //No hay Lineas vigentes
							{
								System.out.println("Error codigo 2\nic_estatus_antic=1 y ic_bloqueo=2");
								sentenciaSQL = "insert into com_controlant05 "+
									" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion)"+
									" values ("+folio+",sysdate,2,'NE')";
								if (con.ejecutaSQL(sentenciaSQL)) {
									sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
										" where ic_pedido = "+folio;
									if (con.ejecutaSQL(sentenciaSQL)) {
										errorDiversos = true;
									} else {
										errorTransaccion = true;
									}
								} else {
									errorTransaccion = true;
								}
							}
							else	//Si hay l�neas vigentes
							{
								sentenciaSQL = "SELECT * FROM ("+
									"	SELECT l.codigo_agencia, l.codigo_sub_aplicacion"+
									" 	, l.numero_linea_fondeo"+
									" 	, (l.monto_asignado - (nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))) as disponible" +
									" 	FROM LC_LINEAS_FONDEO_INTER l" +
									" 	WHERE l.fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
	//								" 	AND (l.monto_asignado - nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))>="+importeRecibir+
									" 	AND l.tipo_financiera = " + tipoFinanciera +
									" 	AND l.codigo_financiera = " + codigoFinanciera +
									" 	AND l.codigo_estado = 1 " +
									") WHERE disponible >=	"+importeRecibirCalc+
									" 	AND rownum = 1 " +
									" order by disponible";

								rs1 = con.queryDB(sentenciaSQL);
								numDisponibValidas = 0;

								while(rs1.next())
								{
									numDisponibValidas++;
									codigoAgencia = rs1.getString("CODIGO_AGENCIA");
									codigoSubAplicacion = rs1.getString("CODIGO_SUB_APLICACION");
									numLineaFondeo = rs1.getString("NUMERO_LINEA_FONDEO");
									//Este instrucci�n se aplica ya que nos interesa �nicamente el
									//primer registro (El de disponilidad menor)
									continue;
								}
								rs1.close();
								con.cierraStatement();

								if (numDisponibValidas<1)	//Verif. Monto vs Disponibilidad
								{
									System.out.println("Error codigo 3\nic_estatus_antic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into com_controlant05 "+
										" (ic_pedido,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion)"+
										" values ("+folio+",sysdate,3, 'NE')";
									if (con.ejecutaSQL(sentenciaSQL)) {
										sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
											" where ic_pedido = "+folio;
										if (con.ejecutaSQL(sentenciaSQL)) {
											errorDiversos = true;
										} else {
											errorTransaccion = true;
										}
									} else {
										errorTransaccion = true;
									}
								}
								else	//Si hay disponibilidad >= importe recibir
								{
	//								System.out.println("Si hay disponibilidad");
	//								System.out.println("Codigo Agencia="+codigoAgencia);
	//								System.out.println("Codigo sub aplicacion="+codigoSubAplicacion);
	//								System.out.println("Codigo num L�nea fondeo="+numLineaFondeo);

									sentenciaSQL = "select tipo_financiera, codigo_financiera" +
										" , facultades " +
										" from mg_financieras" +
										" where tipo_financiera = " + tipoFinanciera+
										" AND codigo_financiera = " + codigoFinanciera;
									rs1 = con.queryDB(sentenciaSQL);
									if(rs1.next()) {	//Hay aprobaciones intermediarios?
										facultades = rs1.getString ("FACULTADES");
									} else {
										System.out.println("Error codigo 4\nic_estatus_antic=1 y ic_bloqueo=2");
										sentenciaSQL = "insert into com_controlant05 "+
											" (ic_pedido, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion)"+
											" values ("+folio+",sysdate,4,'NE')";
										if (con.ejecutaSQL(sentenciaSQL)) {
											sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
												" where ic_pedido = "+folio;
											if (con.ejecutaSQL(sentenciaSQL)) {
												errorDiversos = true;
											} else {
												errorTransaccion = true;
											}
										} else {
											errorTransaccion = true;
										}
									}
									rs1.close();
									con.cierraStatement();

									if (new BigDecimal(facultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {	//facultades < importeRecibirCalc?
										//Buscar aprobaciones por rangos
										sentenciaSQL = "SELECT codigo_aprobacion, cupo_minimo, cupo_limite"+
											" FROM mg_aprobacion"+
											" WHERE "+importeRecibirCalc+" between cupo_minimo and cupo_limite ";
										//System.out.println(sentenciaSQL);

										rs1 = con.queryDB(sentenciaSQL);
										if(rs1.next()) {	//No est� fuera de rango?
											aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
											descripcion = "Descripci�n especial";
											estatus = "N";
										} else {
											System.out.println("Error codigo 5\nic_estatus_antic=1 y ic_bloqueo=2");
											sentenciaSQL = "insert into com_controlant05 "+
												" (ic_pedido, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion)"+
												" values ("+folio+",sysdate,5,'NE')";
											if (con.ejecutaSQL(sentenciaSQL)){
												sentenciaSQL = "update com_anticipo set ic_bloqueo=2"+
													" where ic_pedido = "+folio;
												if (con.ejecutaSQL(sentenciaSQL)){
													errorDiversos = true;
												} else {
													errorTransaccion = true;
												}
											} else {
												errorTransaccion = true;
											}
										}
										rs1.close();
										con.cierraStatement();
									}//fin de facultades < importeRecibirCalc?
									else
									{
										aprobadoPor="NULL";
										descripcion="Ingresado por Nafin electr�nico";
										estatus = "S";
									}

									if (!errorDiversos) {
										System.out.println("Se marca el anticipo "+folio+" como 'En proceso'");
										System.out.println("Aprobado por: "+aprobadoPor);
										System.out.println("Descripcion: "+descripcion);
										System.out.println("Estatus: "+estatus);
										//ic_estatus_antic = 2   EN PROCESO
										//ic_bloqueo = 3   EN PROCESO POR INTERFASE
										sentenciaSQL = "update com_anticipo set ic_estatus_antic = 2, ic_bloqueo = 3"+
											" where ic_pedido = "+folio;
										if (!con.ejecutaSQL(sentenciaSQL)){
											errorTransaccion = true;
										} else {
											sentenciaSQL = "delete from com_controlant01"+
															" where ic_pedido = " + folio ;
											con.ejecutaSQL(sentenciaSQL);

											sentenciaSQL = "insert into com_controlant01 (ic_pedido, ig_codigo_agencia"+
												" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
												" , ig_aprobado_por, cg_descripcion, fn_monto_operacion, cg_estatus)"+
												" values ("+folio+","+codigoAgencia+","+codigoSubAplicacion+
												" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
												" , '"+descripcion+"',"+importeRecibir+",'"+estatus+"')";
											if (!con.ejecutaSQL(sentenciaSQL)){
												//System.out.println("fallo: " + sentenciaSQL);
												errorTransaccion = true;
											} else {
												con.terminaTransaccion(true);
											}
										}
									}//if (!errorDiversos)
								}//fin else Si hay disponibilidad >= importe recibir
							}//fin else Si hay l�neas vigentes
						}//fin else Si hay L�neas de intermediario

					}*/
				}
				if (errorDiversos && !errorTransaccion ) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}

			}

			rs.close();
			con.cierraStatement();

			System.out.println(" Fin Operaciones 1er Piso Credicadenas");
			/* Fin Interfase 1er Piso Credicadenas */



			/* Inicio Interfase 2o Piso Financiamiento a Distribuidores */
			System.out.println(" Inicio Operaciones 2o Piso Financiamiento a Distribuidores ");

			// Foda 062 - 2004, 068 - 2004
			strSelect = " , AG.ig_agencia_sirac";

			if ("T".equals(oficTramXProd.get("4-").toString())) {
				strFrom = ", comrel_pyme_epo_x_producto pexp " +
						", comcat_agencia_sirac AG" ;
				strCondiciones = " and d.ic_pyme = pexp.ic_pyme"+
								" and d.ic_epo = pexp.ic_epo"+
								" and d.ic_producto_nafin = pexp.ic_producto_nafin"+
								" and pexp.ic_oficina_tramitadora = AG.ic_estado (+)";

			} else if ("F".equals(oficTramXProd.get("4-").toString())) {
				strFrom = " , com_domicilio DOM" +
						", comcat_agencia_sirac AG" ;

				strCondiciones = "  and d.ic_pyme = DOM.ic_pyme"   +
							"  and DOM.cs_fiscal = 'S'"   +
							"  and DOM.ic_estado = AG.ic_estado (+)";
			} else {
				strSelect = " , 90 as ig_agencia_sirac";
				strFrom = "";
				strCondiciones = "";
			}

			sentenciaSQL = " SELECT s.ic_documento"   +
							"  , lc.ic_if as ic_if "   +
							"  , if.ic_financiera"   +
							"  , fi.ic_tipo_financiera"   +
							"  , ds.fn_importe_recibir as fn_monto_credito"   +
							"  , DECODE(lc.ic_moneda, 1, ds.fn_importe_recibir, 54, ROUND((ds.fn_importe_recibir * " + tipoCambioDl + " ),2) ) as in_importe_recibir_calc"   +
							"  , if.ig_tipo_piso"   +
							"  , d.ic_moneda " +
							strSelect   +
							"  FROM dis_solicitud s " +
							"  , dis_docto_seleccionado ds "   +
							"  , dis_documento d"   +
							"  , comcat_if if"  +
							"  , comcat_financiera fi"  +
							"  , com_linea_credito lc"  +
							"  , comcat_tasa t"   +
							strFrom +
							"  WHERE s.ic_documento = ds.ic_documento"   +
							"  AND ds.ic_documento = d.ic_documento"   +
							"  AND d.ic_linea_credito = lc.ic_linea_credito"   +
							"  AND lc.ic_if = if.ic_if"   +
							"  AND if.ic_financiera = fi.ic_financiera"   +
							"  AND ds.ic_tasa = t.ic_tasa"  +
							strCondiciones +
							"  AND s.ic_estatus_solic = 1"   +
							"  AND s.ic_bloqueo is null"   +
							" UNION "  +
							" SELECT s.ic_documento"   +
							"  , lc.ic_if"   +
							"  , if.ic_financiera"   +
							"  , fi.ic_tipo_financiera"   +
							"  , ds.fn_importe_recibir as fn_monto_credito"   +
							"  , DECODE(t.ic_moneda, 1, ds.fn_importe_recibir, 54, ROUND((ds.fn_importe_recibir * " + tipoCambioDl + " ),2) ) as in_importe_recibir_calc"   +
							"  , if.ig_tipo_piso"   +
							"  , d.ic_moneda " +
							strSelect   +
							"  FROM dis_solicitud s " +
							"  , dis_docto_seleccionado ds"   +
							"  , dis_documento d"   +
							"  , comcat_if if"  +
							"  , comcat_financiera fi"  +
							"  , dis_linea_credito_dm lc"  +
							"  , comcat_tasa t"   +
							strFrom +
							"  WHERE s.ic_documento = ds.ic_documento"   +
							"  AND ds.ic_documento = d.ic_documento"   +
							"  AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
							"  AND lc.ic_if = if.ic_if"   +
							"  AND if.ic_financiera = fi.ic_financiera"   +
							"  AND ds.ic_tasa = t.ic_tasa" +
							strCondiciones +
							"  AND s.ic_estatus_solic = 1"   +
							"  AND s.ic_bloqueo is null" ;

			System.out.println(sentenciaSQL);

			try {
				rs = con.queryDB(sentenciaSQL);
			} catch(SQLException sqle) {
				System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
				throw sqle;
			}
			tipoPiso = 0;
			bloqueo = 0;
			estatusAntic = 0;

			while(rs.next()) {

				errorDiversos = false;
				errorTransaccion = false;
				tipoPiso = rs.getInt("IG_TIPO_PISO");
				folio = rs.getString("IC_DOCUMENTO");
				codigoFinanciera = rs.getInt("IC_FINANCIERA");
				importeRecibir = rs.getString("FN_MONTO_CREDITO");
				tipoFinanciera = rs.getInt("IC_TIPO_FINANCIERA");
				importeRecibirCalc = rs.getString("IN_IMPORTE_RECIBIR_CALC");
				icMoneda = rs.getInt("IC_MONEDA");
				String icIF  = rs.getString("ic_if");
				strProducto = (icMoneda==1)?"4A":"4B";

				//System.out.println(tipoPiso);

				cod_agencia = rs.getInt("IG_AGENCIA_SIRAC");

				//F014-2011(E)
						String numLineaFondeoIF ="";
						if ("T".equals(oficTramXProd.get("4-").toString())  ||  "T".equals(oficTramXProd.get("1C").toString()) ) {
							sentenciaSQL =	"  SELECT  IG_NUMERO_LINEA_FONDEO  FROM COMREL_PRODUCTO_IF "+
															" WHERE IC_IF = "+icIF;
						if ("T".equals(oficTramXProd.get("4-").toString())) {
									sentenciaSQL +=	" and ic_producto_nafin =4 ";					//4 Distribuidores
							}
							if ("T".equals(oficTramXProd.get("1C").toString())) {
									sentenciaSQL +=	" and ic_producto_nafin =1 ";					//1 Descuento Electronico
							}
						}
						rs1 = con.queryDB(sentenciaSQL);
						if(rs1.next())		{
							numLineaFondeoIF = rs1.getString("IG_NUMERO_LINEA_FONDEO")==null?"":rs1.getString("IG_NUMERO_LINEA_FONDEO");
						}else{
							numLineaFondeoIF ="";
						}
						rs1.close();
						con.cierraStatement();
						//F014-2011(S)
						System.out.println("numLineaFondeoIF  ---"+numLineaFondeoIF);

				if (cod_agencia == 0 && tipoPiso == 2) {

					System.out.println("Error codigo 12\nic_estatus_antic=1 y ic_bloqueo=2");
					sentenciaSQL = "insert into dis_interfase "+
						" (ic_documento,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
						" values ("+folio+", sysdate, 0, 'NE', '"+ strProducto +"')";
					try {
						con.ejecutaSQL(sentenciaSQL);
						sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
							" where ic_documento = "+folio;
						con.ejecutaSQL(sentenciaSQL);
						errorDiversos = true;
					} catch(SQLException sqle) {
						errorTransaccion = true;
					}

				} else {

					if (tipoPiso==1) {


						bloqueo = 1;
						estatusAntic = 1;

						descripcion = "Ingresado por Nafin electr�nico";//*****
						estatus = "S";

						sentenciaSQL = "update dis_solicitud set ic_estatus_solic = " + estatusAntic +
									" , ic_bloqueo = "+ bloqueo +
									" where ic_documento = " + folio;
						try {
							con.ejecutaSQL(sentenciaSQL);
							con.terminaTransaccion(true);
							/*sentenciaSQL = "insert into inv_interfase (cc_disposicion "+
									", ig_codigo_financiera"+
									", cg_descripcion"+
									", fn_monto_operacion"+
									", df_hora_proceso )"+
									" values ('"+folio+"', "+codigoFinanciera+",'"+descripcion+
									"',"+importeRecibir+", SYSDATE)";
								try {
									con.ejecutaSQL(sentenciaSQL);
									con.terminaTransaccion(true);
								} catch(SQLException sqle) {
									System.out.println("fallo 2: " + sentenciaSQL);
									con.terminaTransaccion(false);
								}*/
						} catch(SQLException sqle) {
							System.out.println("fallo 1: " + sentenciaSQL);
							con.terminaTransaccion(false);
						}
					} else {
					//System.out.println(codigoFinanciera);


						sentenciaSQL = "SELECT count(1) numLineasInter" +
							" FROM LC_LINEAS_FONDEO_INTER" +
							" WHERE tipo_financiera = " + tipoFinanciera +
							" AND codigo_financiera = " + codigoFinanciera;
						if(!numLineaFondeoIF.equals("") ){
							sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
						}

						try {
							rs1 = con.queryDB(sentenciaSQL);
						} catch(SQLException sqle) {
							System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
							throw sqle;
						}
						rs1.next();
						numLineasInter = rs1.getInt("NUMLINEASINTER");
						rs1.close();
						con.cierraStatement();

						if (numLineasInter < 1) {	//No hay Lineas de intermediario
							System.out.println("Error codigo 1\nic_estatus_antic=1 y ic_bloqueo=2");
							sentenciaSQL = "insert into dis_interfase "+
								" (ic_documento,df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
								" values ("+folio+", sysdate, 1, 'NE', '"+ strProducto +"')";
							try {
								con.ejecutaSQL(sentenciaSQL);
								sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
									" where ic_documento = "+folio;
								con.ejecutaSQL(sentenciaSQL);
								errorDiversos = true;
							} catch(SQLException sqle) {
								errorTransaccion = true;
							}
						} else {	//Si hay L�neas de intermediario
							sentenciaSQL = "SELECT count(1) as numVigentes" +
								" FROM LC_LINEAS_FONDEO_INTER" +
								" WHERE fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
								" AND tipo_financiera = " + tipoFinanciera +
								" AND codigo_financiera = " + codigoFinanciera;
								if(!numLineaFondeoIF.equals("") ){
									sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
								}

							try {
								rs1 = con.queryDB(sentenciaSQL);
							} catch(SQLException sqle) {
								System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
								throw sqle;
							}
							rs1.next();
							numVigentes = rs1.getInt("NUMVIGENTES");
							rs1.close();
							con.cierraStatement();

							if (numVigentes <1) //No hay Lineas vigentes
							{
								System.out.println("Error codigo 2\nic_estatus_antic=1 y ic_bloqueo=2");
								sentenciaSQL = "insert into dis_interfase "+
									" (ic_documento, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
									" values ("+folio+",sysdate,2,'NE', '"+ strProducto +"')";
								try {
									con.ejecutaSQL(sentenciaSQL);
									sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
										" where ic_documento = "+folio;
									con.ejecutaSQL(sentenciaSQL);
									errorDiversos = true;
								} catch(SQLException sqle) {
									errorTransaccion = true;
								}
							}
							else	//Si hay l�neas vigentes
							{
								sentenciaSQL = "SELECT * FROM ("+
									"	SELECT l.codigo_agencia, l.codigo_sub_aplicacion"+
									" 	, l.numero_linea_fondeo"+
									" 	, (l.monto_asignado - (nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))) as disponible" +
									" 	FROM LC_LINEAS_FONDEO_INTER l" +
									" 	WHERE l.fecha_de_vcmto > = to_date(to_char(sysdate, 'ddmmyyyy'),'ddmmyyyy')" +
	//								" 	AND (l.monto_asignado - nvl(l.monto_utilizado,0) + nvl(l.monto_comprometido,0))>="+importeRecibir+
									" 	AND l.tipo_financiera = " + tipoFinanciera +
									" 	AND l.codigo_financiera = " + codigoFinanciera +
									" 	AND l.codigo_estado = 1 " +
									" ) WHERE disponible >=	"+importeRecibirCalc;
									if(!numLineaFondeoIF.equals("") ){
										sentenciaSQL +=	" AND NUMERO_LINEA_FONDEO = " + numLineaFondeoIF; //F014-2011
									}
									sentenciaSQL += " 	AND rownum = 1 " +
									" order by disponible";

								try {
									rs1 = con.queryDB(sentenciaSQL);
								} catch(SQLException sqle) {
									System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
									throw sqle;
								}
								numDisponibValidas = 0;

								while(rs1.next())
								{
									numDisponibValidas++;
									codigoAgencia = rs1.getString("CODIGO_AGENCIA");
									codigoSubAplicacion = rs1.getString("CODIGO_SUB_APLICACION");
									numLineaFondeo = rs1.getString("NUMERO_LINEA_FONDEO");
									//Este instrucci�n se aplica ya que nos interesa �nicamente el
									//primer registro (El de disponilidad menor)
									continue;
								}
								rs1.close();
								con.cierraStatement();

								if (numDisponibValidas<1)	//Verif. Monto vs Disponibilidad
								{
									System.out.println("Error codigo 3\nic_estatus_solic=1 y ic_bloqueo=2");
									sentenciaSQL = "insert into dis_interfase "+
										" (ic_documento, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
										" values ("+folio+",sysdate,3, 'NE', '"+ strProducto +"')";
									try {
										con.ejecutaSQL(sentenciaSQL);
										sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
											" where ic_documento = "+folio;
										con.ejecutaSQL(sentenciaSQL);
										errorDiversos = true;
									} catch(SQLException sqle) {
										errorTransaccion = true;
									}
								}
								else	//Si hay disponibilidad >= importe recibir
								{
	//								System.out.println("Si hay disponibilidad");
	//								System.out.println("Codigo Agencia="+codigoAgencia);
	//								System.out.println("Codigo sub aplicacion="+codigoSubAplicacion);
	//								System.out.println("Codigo num L�nea fondeo="+numLineaFondeo);

									sentenciaSQL = "select tipo_financiera, codigo_financiera" +
										" , facultades " +
										" from mg_financieras" +
										" where tipo_financiera = " + tipoFinanciera+
										" AND codigo_financiera = " + codigoFinanciera;
									try {
										rs1 = con.queryDB(sentenciaSQL);
									} catch(SQLException sqle) {
										System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
										throw sqle;
									}
									if(rs1.next()) {	//Hay aprobaciones intermediarios?
										facultades = rs1.getString ("FACULTADES");
									} else {
										System.out.println("Error codigo 4\nic_estatus_solic=1 y ic_bloqueo=2");
										sentenciaSQL = "insert into dis_interfase "+
											" (ic_documento, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
											" values ("+folio+",sysdate,4,'NE', '"+ strProducto +"')";
										try {
											con.ejecutaSQL(sentenciaSQL);
											sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
												" where ic_documento = "+folio;
											con.ejecutaSQL(sentenciaSQL);
											errorDiversos = true;
										} catch(SQLException sqle) {
											errorTransaccion = true;
										}
									}
									rs1.close();
									con.cierraStatement();

									if (new BigDecimal(facultades).compareTo(new BigDecimal(importeRecibirCalc))==-1) {	//facultades < importeRecibirCalc?
										//Buscar aprobaciones por rangos
										sentenciaSQL = "SELECT codigo_aprobacion, cupo_minimo, cupo_limite"+
											" FROM mg_aprobacion"+
											" WHERE "+importeRecibirCalc+" between cupo_minimo and cupo_limite ";
										//System.out.println(sentenciaSQL);

										try {
											rs1 = con.queryDB(sentenciaSQL);
										} catch(SQLException sqle) {
											System.out.println("Diversos.java(sentenciaSQL): " + sentenciaSQL );
											throw sqle;
										}
										if(rs1.next()) {	//No est� fuera de rango?
											aprobadoPor="'"+rs1.getString("CODIGO_APROBACION")+"'";
											descripcion = "Descripci�n especial";
											System.out.println("Error codigo 7\nic_estatus_solic=2 y ic_bloqueo=3");

											sentenciaSQL = "insert into dis_interfase (ic_documento, ig_codigo_agencia"+
												" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
												" , ig_aprobado_por, cg_descripcion, fn_monto_operacion " +
												" , df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO, DF_DIVERSOS) "+
												" values ('"+folio+"',"+codigoAgencia+","+codigoSubAplicacion+
												" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
												" , '"+descripcion+"',"+importeRecibir +", SYSDATE, 7, 'NE', '"+ strProducto +"', SYSDATE)";

											try {
												con.ejecutaSQL(sentenciaSQL);
												sentenciaSQL = "update dis_solicitud set ic_estatus_solic = 2, ic_bloqueo=3"+
													" where ic_documento = '"+folio+"'";
												con.ejecutaSQL(sentenciaSQL);
												errorDiversos = true;
											} catch(SQLException sqle) {
												errorTransaccion = true;
											}

										} else {
											System.out.println("Error codigo 5\nic_estatus_solic=1 y ic_bloqueo=2");
											sentenciaSQL = "insert into dis_interfase "+
												" (ic_documento, df_hora_proceso, ic_error_proceso, cc_codigo_aplicacion, CC_PRODUCTO)"+
												" values ("+folio+", SYSDATE, 5, 'NE', '"+ strProducto +"')";
											try {
												con.ejecutaSQL(sentenciaSQL);
												sentenciaSQL = "update dis_solicitud set ic_bloqueo=2"+
													" where ic_documento = '"+folio+"'";
												con.ejecutaSQL(sentenciaSQL);
												errorDiversos = true;
											} catch(SQLException sqle) {
												errorTransaccion = true;
											}
										}


										rs1.close();
										con.cierraStatement();
									}//fin de facultades < importeRecibirCalc?
									else
									{
										aprobadoPor="NULL";
										descripcion="Ingresado por Nafin electr�nico";
									}

									if (!errorDiversos) {


										System.out.println("Se marca la solicitud "+folio+" como 'En proceso'");
										System.out.println("Aprobado por: "+aprobadoPor);
										System.out.println("Descripcion: "+descripcion);
										//System.out.println("Estatus: "+estatus);
										//ic_estatus_antic = 2   EN PROCESO
										//ic_bloqueo = 3   EN PROCESO POR INTERFASE
										sentenciaSQL = "update dis_solicitud set ic_estatus_solic = 2, ic_bloqueo = 3"+
											" where ic_documento = "+folio;
										try {
											con.ejecutaSQL(sentenciaSQL);
											sentenciaSQL = "insert into dis_interfase (ic_documento, ig_codigo_agencia"+
												" , ig_codigo_sub_aplicacion, ig_numero_linea_fondeo, ig_codigo_financiera"+
												" , ig_aprobado_por, cg_descripcion, fn_monto_operacion, df_hora_proceso, CC_PRODUCTO, DF_DIVERSOS)"+
												" values ("+folio+","+codigoAgencia+","+codigoSubAplicacion+
												" , "+numLineaFondeo+","+codigoFinanciera+","+aprobadoPor+
												" , '"+descripcion+"',"+importeRecibir+", " + " SYSDATE , '"+ strProducto +"', SYSDATE)";
											con.ejecutaSQL(sentenciaSQL);
											con.terminaTransaccion(true);
										} catch(SQLException sqle) {
											errorTransaccion = true;
										}
									}//if (!errorDiversos)
								}//fin else Si hay disponibilidad >= importe recibir
							}//fin else Si hay l�neas vigentes
						}//fin else Si hay L�neas de intermediario

					}
				}
				if (errorDiversos && !errorTransaccion ) {
					con.terminaTransaccion(true);
				} else {
					con.terminaTransaccion(false);
				}
			}

			rs.close();
			con.cierraStatement();

			System.out.println(" Fin Operaciones 2o Piso Financiamiento a Distribuidores");
			/* Fin Interfase 2o Piso Financiamiento a Distribuidores */

			//con.cierraConexionDB();
		} catch(Exception e) {
			System.out.println("Error: "+e);
			System.out.println(con.mostrarError());
			con.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
                    try {
                        if(rs!=null) rs.close();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if(rs1!=null) rs1.close();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if(rs2!=null) rs2.close();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if(ps!=null) ps.close();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                    if(con.hayConexionAbierta()) {
                        con.cierraConexionDB();
                    }
                }
	}//Fin del main()
}//Fin de class
