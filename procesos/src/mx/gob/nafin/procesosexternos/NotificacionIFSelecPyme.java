package mx.gob.nafin.procesosexternos;

import java.util.*;
import javax.naming.*;
import javax.rmi.PortableRemoteObject;
import com.netro.descuento.*;
import com.netro.distribuidores.*;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;

 /**
  * Realiza el env�o de correos de notificaci�n con el listado de los documentos en estatus �Seleccionada Pyme� a los Intermediarios Financieros de Descuento Electr�nico y Financiamiento a Distribuidores.
	El par�metro de notificaci�n, debe estar activo y con al menos una cuenta de correo electr�nico para el IF que desea recibir el aviso.
  * FODEA xx-NAFINET-Automatizaci�n de funcionalidades
  *
  */
public class NotificacionIFSelecPyme {


	public static void main(String [] args)  {
		int codigoSalida = 0;
		int proceso = 0;
		String ruta = "";

		try{



			//Documentos Seleccioandos de Descuento Electronico
			ISeleccionDocumento SelecDocS = ServiceLocator.getInstance().remoteLookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);


			//Documentos Seleccioandos de Descuento Electronico
			SelecDocS.getDoctosNotifiIF_DesElec();


		   AceptacionPyme aceptPyme = ServiceLocator.getInstance().remoteLookup("AceptacionPymeEJB", AceptacionPyme.class);

				//Documentos Seleccioandos de Distribuidores
			aceptPyme.getDoctosNotifiIF_Distribuidores();





		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			codigoSalida = 1;
                } catch(Throwable ex) {
                        codigoSalida = 1;
                        System.out.println(ex.getMessage());
                        ex.printStackTrace();
		} finally {
			System.exit(codigoSalida);
		}

	}



}
