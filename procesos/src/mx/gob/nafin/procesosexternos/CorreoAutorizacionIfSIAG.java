package mx.gob.nafin.procesosexternos;

import com.netro.garantias.Garantias;

import java.sql.PreparedStatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

/**
 *
 * Clase para el envio del Correo de Notificacion donde se indican
 * los registros con Autorizaci�n IF enviados por el SIAG.
 *
 * @author JRSW alias Rebos
 * @author jshernandez (Ajuste: Actualizar Proceso CorreoErroresSIAG. 03/08/2015 12:20:40 p.m.)
 *
 */
public class CorreoAutorizacionIfSIAG {

	private static Log log = ServiceLocator.getInstance().getLog(CorreoAutorizacionIfSIAG.class);

	public void send() throws Exception{

		log.info("send(E)");
		try {

			log.info("Obteniendo GarantiasEJB");
		   Garantias garantias = ServiceLocator.getInstance().remoteLookup("GarantiasEJB", Garantias.class);

			ArrayList 		registros 		= getRegistrosConAutorizacionIf();
			UtilUsr  		utilUsr   		= new UtilUsr();
			for(int i=0;i<registros.size();i++){

				HashMap 	registro 			= (HashMap) registros.get(i);

				String 	idUsuario 			= (String) registro.get("ID_USUARIO");
				String 	folio					= (String) registro.get("FOLIO");
				String 	numeroRegistros	= (String) registro.get("NUM_REGISTROS");
				String  	operacion			= (String) registro.get("OPERACION");
				String  	icIfSiag				= (String) registro.get("IC_IF_SIAG");

				try {

					// obtiene correo Parametrizado
					List 	  	email  				= garantias.getCorreosParametrizadosPorProcesoSiag(folio, icIfSiag, "AUTORIZACION_IF_SIAG");
					// En caso de que no haya ninguna direcci�n de correo parametrizada usar la que tiene registrada
					// en el OID el usuario facultado.
					if( email.size() == 0 ){

						System.out.println("\nALERTA:");
						System.out.println(" No es posible notificar a los usuarios NAFIN que la transaccion con Numero de Folio: "+folio);
						System.out.println(" (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF, debido a que el IF no esta ");
						System.out.println(" parametrizado en GTI_CORREO_NOTIF.");

						// Obtener correo del usuario
						String 	emailUsuario = null;
						try {
							emailUsuario = utilUsr.getUsuario(idUsuario).getEmail();
						} catch(Exception e){
							emailUsuario = null;
						}

						if(Comunes.esVacio(emailUsuario)){
							System.out.println("\nALERTA:");
							System.out.println(" El usuario con ID: " +idUsuario + " no tiene una direccion de correo registrada");
							System.out.println(" por lo que no es posible notificarle que la transaccion con Numero de Folio: "+folio);
							System.out.println(" (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF.");
						}

						// Agregar direcci�n a la lista de correos.
						if(!Comunes.esVacio(emailUsuario)){
							email.add(emailUsuario);
						}

					}

					String 	emailDestinatarios = null;
					boolean 	exitoEnvioCorreos  = email.size() > 0? true : false ;
					for(int j=0;j<email.size();j++){

						emailDestinatarios = (String)email.get(j);

						if(!Comunes.esVacio(emailDestinatarios)){
							try {
								// Enviar correo de notificaci�n
								enviarCorreoDeNotificacion(emailDestinatarios,folio,numeroRegistros,operacion);
							}catch(Exception e){
								exitoEnvioCorreos = false;
								System.out.println("\nALERTA:");
								System.out.println(" No fue posible notificar a: " + emailDestinatarios + " que la transaccion ");
								System.out.println(" con Numero de Folio: " + folio + " (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF.");
								log.error(
									"\nALERTA:" +
									" No fue posible notificar a: " + emailDestinatarios + " que la transaccion" +
									" con Numero de Folio: " + folio + " (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF:"
									,e
								);
							}
						}

					}

					// Actualizar en tabla que se envio el correo con estatus 2
					if(exitoEnvioCorreos){
						try {
							setCorreoEnviado(folio,icIfSiag);
						} catch(Exception e){
							System.out.println("\nALERTA:");
							System.out.println(" No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: "+folio+" (e IC_IF_SIAG = "+icIfSiag+").");
							log.error(
								"\nALERTA:"+
								" No fue posible registrar el env�o exitoso de los correos para la transaccion con Numero de Folio: "+folio+" (e IC_IF_SIAG = "+icIfSiag+").",
								e
							);
						}
					}

				} catch(Exception e){

					System.out.println("\nALERTA:");
					System.out.println(" No fue posible notificar que la transaccion con Numero de Folio: " + folio );
					System.out.println(" (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF.");
					log.error(
						"\nALERTA:" +
						" No fue posible notificar que la transaccion con Numero de Folio: " + folio +
						" (e IC_IF_SIAG = "+icIfSiag+") tiene 1 Autorizacion IF:"
						,e
					);

				}

			}//for


		}catch(Exception e){
			log.error("send(Exception)",e);
			e.printStackTrace();
			throw e;
		}finally{
			log.info("send(S)");
		}
	}

	private ArrayList getRegistrosConAutorizacionIf() throws Exception{

		log.info("getRegistrosConAutorizacionIf(E)");

		AccesoDBOracle con 				  = new AccesoDBOracle();
		StringBuffer 	 qrySentencia	= new StringBuffer();
		Registros			 registros		= null;
		List   				 lVarBind			= new ArrayList();
		ArrayList			 resultado		= new ArrayList();

		try {
			con.disableMessages();
			con.conexionDB();

			qrySentencia.append(
				"SELECT                              	               "  +
				"  ST.IC_USUARIO_FACULTADO AS ID_USUARIO,             "  +
				"  ST.IC_FOLIO             AS FOLIO,                  "  +
				"  ST.IN_REGISTROS_RECH    AS NUM_REGISTROS,          "  +
				"  TIPO.CG_DESCRIPCION     AS OPERACION,              "  +
				"  ST.IC_IF_SIAG           AS IC_IF_SIAG              "  +
				"FROM                                                 "  +
				"  GTI_ESTATUS_SOLIC       ST,                        "  +
				"  GTICAT_TIPOOPERACION    TIPO                       "  +
				"WHERE                                                "  +
				"  ST.IC_TIPO_OPERACION = TIPO.IC_TIPO_OPERACION  AND "  +
				"  ST.IC_CORREO_ENV     = ? AND                       "  +
				"	ST.IC_TIPO_OPERACION = ? AND 								"  +
				"  ST.IC_SITUACION      = ? 									"
			);

			lVarBind.add(new Integer("0")); // 0 - (REGISTROS SIN ERROR)
			lVarBind.add(new Integer("2")); // 2 - CARGA DE SALDOS Y PAGO COMISIONES
			lVarBind.add(new Integer("4")); // 4 - AUTORIZACI�N IF

			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next()){

				HashMap 	registro 		= new HashMap();

				String 	idUsuario 		= registros.getString("ID_USUARIO");
				String 	folio 			= registros.getString("FOLIO");
				String 	numRegistros 	= registros.getString("NUM_REGISTROS");
				String 	operacion    	= registros.getString("OPERACION");
				String	icIfSiag			= registros.getString("IC_IF_SIAG");

				operacion = codificaAcentos(operacion);

				registro.put("ID_USUARIO",    idUsuario);
				registro.put("FOLIO",         folio);
				registro.put("NUM_REGISTROS", numRegistros);
				registro.put("OPERACION", 		operacion);
				registro.put("IC_IF_SIAG",		icIfSiag);

				resultado.add(registro);
			}

		}catch(Exception e){
			log.error("getRegistrosConAutorizacionIf(Exception)");
			log.error("getRegistrosConAutorizacionIf.qrySentencia = <" + qrySentencia + ">");
			log.error("getRegistrosConAutorizacionIf.exception:",e);
			e.printStackTrace();
			throw new Exception("Error al consultar Registros con Autorizaci�n IF.");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("getRegistrosConAutorizacionIf(S)");
		}

		return resultado;
	}

	private void setCorreoEnviado(String folio,String icIfSiag)
		throws Exception {

		log.info("setCorreoEnviado(E)");

		if(Comunes.esVacio(folio)){
			log.info("setCorreoEnviado(S)");
			return;
		}

		if(Comunes.esVacio(icIfSiag)){
			log.info("setCorreoEnviado(S)");
			return;
		}

		AccesoDBOracle 			con 		= new AccesoDBOracle();
		PreparedStatement ps  		= null;
		boolean 				bOk 		= true;
		StringBuffer 		strSQL 	= new StringBuffer();

		try {
			con.disableMessages();
			con.conexionDB();

			strSQL.append(
				"UPDATE                           "  +
				"   GTI_ESTATUS_SOLIC             "  +
				"SET                              "  +
				"   IC_CORREO_ENV = 2             "  + // 2 (CORREO DE NOTIFICACION ENVIADO)
				"WHERE                            "  +
				"   IC_FOLIO        		= ? AND   "  +
				"   IC_IF_SIAG 			= ?       "
			);

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, folio);
			ps.setString(2, icIfSiag);
			ps.executeUpdate();

		} catch(Exception e) {
			log.error("setCorreoEnviado(Exception)");
			log.error("setCorreoEnviado.folio    = <" + folio    + ">");
			log.error("setCorreoEnviado.icIfSiag = <" + icIfSiag + ">");
			log.error("setCorreoEnviado.strSQL   = <" + strSQL   + ">");
			log.error("setCorreoEnviado.exception:",e);
			bOk = false;
			e.printStackTrace();
			throw new Exception("Error al actualizar estatus de envio de correo");
		} finally {
			log.info("setCorreoEnviado(S)");
			if(ps != null) ps.close();
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}

	}

	private void enviarCorreoDeNotificacion(String destinatario,String folio,String numeroRegistros,String operacion) throws Exception {

		log.info("enviarCorreoDeNotificacion(E)");

		// Agregar remitente
		String remitente 							= "no_response@nafin.gob.mx";
		// Agregar copia de carbon para los siguientes destinatarios
		String ccDestinatarios					= null;
		// Agregar titulo del mensaje
		String tituloMensaje						= "Notificacion Electronica de Archivos con Autorizaci�n IF" ;
		// Agregar mensaje
		StringBuffer mensajeCorreo 			= getMensaje(folio,numeroRegistros,operacion);
		// Agregar Archivos de Imagenes

		ArrayList 		listaDeImagenes		= new ArrayList();

		HashMap 			imagenNafin 			= new HashMap();
		java.net.URL  	imgURL            	= getClass().getResource("nafinsa.gif");
		if(imgURL != null){
			imagenNafin.put("FILE_FULL_PATH",	imgURL.getPath());
			imagenNafin.put("FILE_ID","nafin_id");
      	listaDeImagenes.add(imagenNafin);
		}
		// Agregar archivo adjunto
		ArrayList listaDeArchivos	= null;
		// Enviar correo
		Correo correo = new Correo();
		correo.disableDebug();
		correo.enableUseAccesoDBOracle();
		correo.enviaCorreoConDatosAdjuntos(remitente,destinatario,ccDestinatarios,tituloMensaje,mensajeCorreo.toString(),listaDeImagenes,listaDeArchivos);

		log.info("enviarCorreoDeNotificacion(S)");
	}

	private StringBuffer getMensaje(String folio,String numeroRegistros, String operacion){

		log.info("getMensaje(E)");

		StringBuffer mensaje = new StringBuffer();
		mensaje.append(
			"<html>"  +
			"	<head>"  +
			"		<title>"  +
			"			Notificaci&oacute;n electr&oacute:nica de Archivos con Error."  +
			"		</title>"  +
			"	</head>"  +
			"	<body>"  +
			"		<table width=\"600px\" border=\"0\">"  +
			"			<tr>"  +
			"				<td height=\"30px\">&nbsp;</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">"  +
			"				Estimado usuario,"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td height=\"35px\">&nbsp;</td>"  +
			"			</tr>	"  +
			"			<tr>"  +
			"				<td style=\"text-align:justify;font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">"  +
			"					Le informamos que la transacci&oacute;n de <span style=\"font-weight:bold\">"+operacion+"</span>, "  +
			"					identificada con el n&uacute;mero de folio "+folio+" fue atendido y se encuentra en estatus Aurorizaci&oacute;n IF." +
			"					Por lo anterior, lo invitamos a que ingrese a <a href=\"http://cadenas.nafin.com.mx\">http://cadenas.nafin.com.mx</a> en la "  +
			"					Autorizaci&oacute;n de Pago de Comisiones del M�dulo de Transacciones, para que dichas operaciones se autoricen de " +
			" 				manera oportuna. " +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td height=\"50px\">&nbsp;</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td style=\"font-family:Arial;font-size:11.0pt;color:#1F497D;padding-left:11px;\">"  +
			"				Atentamente,<br>"  +
			"				Administraci&oacute;n de usuarios."  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td>&nbsp;</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td>"  +
			"					<img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\"> "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>	"  +
			"	</body>"  +
			"</html>");

		log.info("getMensaje(S)");

		return mensaje;
	}

	private String codificaAcentos(String texto){
		log.info("codificaAcentos(S)");

		if(texto == null || texto.trim().equals("")){
			log.info("codificaAcentos(S)");
			return "";
		}

		texto = texto.replaceAll("�","&Aacute;");
		texto = texto.replaceAll("�","&Eacute;");
		texto = texto.replaceAll("�","&Iacute;");
		texto = texto.replaceAll("�","&Oacute;");
		texto = texto.replaceAll("�","&Uacute;");

		texto = texto.replaceAll("�","&aacute;");
		texto = texto.replaceAll("�","&eacute;");
		texto = texto.replaceAll("�","&iacute;");
		texto = texto.replaceAll("�","&oacute;");
		texto = texto.replaceAll("�","&uacute;");

		texto = texto.replaceAll("�","&Ntilde;");
		texto = texto.replaceAll("�","&ntilde;");

		log.info("codificaAcentos(S)");
		return texto;
	}

}
