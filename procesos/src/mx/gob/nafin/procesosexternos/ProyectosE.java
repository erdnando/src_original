package mx.gob.nafin.procesosexternos;


/*************************************************************************************
*
* Nombre de Clase o de Archivo: ProyectosE.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 2002
*
* Autor:          Edgar Gonzalez Bautista
*
* Descripci�n de Clase: Clase que realiza el proceso de Proyectos para Interfases a SIRAC de Credito Electronico.
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	16/Oct/2003
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 049 - 2003, Determinar el tipo de cartera (1o y 2o Piso)
*
*************************************************************************************/
/*************************************************************************************
* Modificaci�n
*
* Fecha Ult. Modificaci�n:	23/Ago/2004
*
* Autor Modificacion:		Edgar Gonz�lez Bautista
*
* Descripci�n: Modificaci�n FODA 044 - 2004
*
*************************************************************************************/

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Calendar;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Comunes;

public class ProyectosE {

	public static void main(String args[]) {
		ejecutaCreditos(args[0]);
	}

	public static void ejecutaCreditos(String usuario){
		String query="", queri="", folio="", aprobado_por=null, estatus="";
		String cadena="";
		/* Valores Fijos */
		String no_linea_credito=null;       			/* Valor 1 */
		int cod_agencia=90;                 			/* Valor 2 */
		int cod_sub_aplicacion=201;         			/* Valor 3 */
		String descripcion="";  						/* Valor 4 */
		String cod_funcionario=usuario; 	 			/* Valor 6 */
		String cod_tipo_linea="D";          			/* Valor 10 */
		int frecuencia=1;                   			/* Valor 11 */
		java.sql.Date fecha_max_desem=null; 			/* Valor 13*/
		java.sql.Date fecha_max_pago=null; 				/* Valor 14 */
		String empleos_generados=null;      			/* Valor 21 */
		double  vtas_exterior=0;          				/* Valor 22 */
		int cod_agen_interme = 0;             			/* Valor 24 */
		String cod_agen_intermediario = null;
		String reestructura=null;             			/* Valor 25 */
		String cod_grup_comision=null;      			/* Valor 26 */
		String verific_estrato="S";         			/* Valor 27 */
		double porcent_asignado=100.0;      			/* Valor 28 */
		int cod_sub_apli_det;           				/* Valor 29 */
		double porcent_asignado_det=100.0;  			/* Valor 31 */
		double mto_intermediario=0.0;       			/* Valor 32 */
		double mto_cliente=0.0;             			/* Valor 33 */
		boolean errorTransaccion = false;
		boolean errorDesconocido = false;
		String tipo_plazo = null;
		int frecuencia_interes = 0;
		int icMoneda=0;
		int icIF = 0;

		int frecuencia_capital = 0;//SE AGREGA POR F004-2010 FVR
		String tipoInteres = "";//SE AGREGA PARAM 36 POR F004-2010 FVR
		String tipoRenta = "";//SE AGREGA POR F020-2011 FVR

		String querySolicPortal = "";

		/* ************* */
		ResultSet rs, rs1, rs2, rs3;
		CallableStatement cs=null;

		AccesoDBOracle base=new AccesoDBOracle();
		try {
			base.conexionDB();
		} catch (Exception e){
			System.out.println("Error al conectar la Base de Datos");
		}

		try{
			query = "SELECT /*+ ordered index_ffs(s IN_COM_SOLIC_PORTAL_01_NUK) use_nl(i,t,int) */ i.ic_solic_portal " +
				"	    , i.ig_codigo_agencia " +
				"	    , i.ig_codigo_sub_aplicacion " +
				"	    , i.ig_numero_linea_fondeo " +
				"	    , i.ig_codigo_financiera " +
				"	    , i.ig_aprobado_por " +
				"	    , i.cg_descripcion " +
				"	    , i.fn_monto_operacion " +
				"	    , trunc(df_v_descuento) - trunc(df_carga) as ig_plazo " +
				"	    , TO_CHAR (s.df_v_descuento, 'DD/MM/YYYY') " +
				"	    , s.ig_clave_sirac as in_numero_sirac " +
				"		, NVL(DECODE(t.ic_moneda, 1, 201, 54, 202), 0) as param3 " +
				"       , s.ic_tasaif " +
				"       , s.ic_tabla_amort " +
				"       , s.ic_tipo_credito " +
				"       , s.ic_emisor " +
				"		, s.ic_if " +
				"		, s.ig_sucursal  "+
				"		, s.cs_tipo_plazo " +
				"		, s.cs_plazo_maximo_factoraje " +
				"		, s.ic_moneda " +
				"		, int.ig_tipo_piso " +
				"		, s.ic_periodicidad_i " +
				"		, int.ic_financiera " +
				"       , DECODE(NVL(p.fn_ventas_net_exp,0),0,1,p.fn_ventas_net_exp) fn_ventas_net_exp"   +
				"		, s.ic_producto_nafin " +
				"		, int.ic_if " +
				"		, s.ic_periodicidad_c " +//SE AGREGA POR F004-2010 FVR
				"		, s.cs_tipo_renta " +//SE AGREGA POR F020-2011 FVR
				"		, s.ig_base_operacion " +
				"		, 'ProyectosE.class' ORIGENQUERY ";

				// Optimizado
				query = query +
				" FROM       " +
				"  		com_solic_portal s   " +
				"  		, com_interfase i   " +
				"  		, comcat_tasa t    " +
				"  		, comcat_if int    " +
				"		, comcat_pyme p		" +
				" WHERE  " +
				" s.ic_solic_portal = i.ic_solic_portal      AND  " +
				" s.ic_if = int.ic_if      AND  " +
				" s.ic_tasaif = t.ic_tasa    AND  " +
				" s.ig_clave_sirac = p.in_numero_sirac (+) AND	" +
				" i.ig_numero_linea_credito is null      AND     " +
				" i.cc_codigo_aplicacion is null   AND      " +
				" i.ic_error_proceso is null      AND      " +
				" s.ic_estatus_solic = 2      AND  " +
				" s.ic_bloqueo = 3     " +
				" ORDER BY 1";


			//System.out.println("Query OPTIMIZADO="+query);

			String desa_proveedores="", cod_aprobacion="", codigo_aplicacion="", productoNafin=null;
			int tasaIf = 0, tablaAmort = 0, tipoCredito = 0, emisor = 0, intermediario = 0;
			int no_lineafondeo=0, cod_agen=0, cod_subapli=0, cod_finan=0, ic_plazo=0, cod_prod_banco=0, cod_base_operac=0, dia=0, mes=0, ano=0, no_doctos=0;
			long cod_cliente=0;
			long no_line_credit=0; //F084-2006
			int plazo_proyecto = 0;
			int tipoPiso;
			int intermediarioSirac=0;
			String status = "";
			double mto_operac=0.0, mto_asignado=0.0, por_asig=0.0;
			boolean baseOperacion = false;
			String altaAutomaticaGarantia = "";
			String scod_base_operac = null;

			// Variables para almacenar el Codigo de la Base de Operaci�n Din�mica.
			// F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
			int 	 	cod_base_operac_dinamica 	= 0;
			String 	scod_base_operac_dinamica 	= null;
			boolean 	existeCodigoBaseOpDinamica	= false;

			/* Calendar fechaMin = Calendar.getInstance(); */
			try {
				rs=base.queryDB(query);
			} catch(SQLException sqle) {
				System.out.println("ProyectosE.java(query): " + query );
				throw sqle;
			}
			while(rs.next()){
				errorTransaccion = false;
				errorDesconocido = false;
				baseOperacion = false;
				cod_base_operac  = 0;
				cod_prod_banco	 = 0;
				scod_base_operac = null;
				altaAutomaticaGarantia = "N";
				folio=rs.getString(1);
				intermediarioSirac = rs.getInt("IC_FINANCIERA");
				tipoPiso = rs.getInt("IG_TIPO_PISO");
				frecuencia_interes = rs.getInt("IC_PERIODICIDAD_I");
				icIF = rs.getInt("IC_IF");
				//F084-2006
				cod_cliente=rs.getLong(11);          		/* Valor 8 */
				cod_agen=rs.getInt(2);						/* Valor 16 */
				cod_subapli=rs.getInt(3);					/* Valor 17 */
				icMoneda = rs.getInt("IC_MONEDA");

				frecuencia_capital = rs.getInt("IC_PERIODICIDAD_C"); //SE AGREGAPOR F004-2010 FVR
				tipoRenta = rs.getString("CS_TIPO_RENTA")==null?"":rs.getString("CS_TIPO_RENTA"); //SE AGREGAPOR F020-2011 FVR

				//Foda 044-2004
				/* Valor 3 */
				if (tipoPiso == 1) {
					cod_sub_aplicacion = cod_subapli;
				} else {
					cod_sub_aplicacion = (icMoneda == 1)?201:202;
				}

				no_lineafondeo=rs.getInt(4);				/* Valor 15 */
				cod_finan=rs.getInt(5);
				aprobado_por=rs.getString(6);               /* Valor 5 */
				cod_aprobacion=aprobado_por;                /* Valor 23 */
				descripcion=rs.getString(7);
				mto_operac=rs.getDouble(8);                 /* Valor 7 */
				mto_asignado=mto_operac;                    /* Valor 9, Valor 30, Valor 33 y Valor 34 */
				ic_plazo=rs.getInt(9);
				plazo_proyecto = (ic_plazo>180)? 180:ic_plazo;            /* Valor 12 */
				tasaIf = rs.getInt(13);
				tablaAmort = rs.getInt(14);
				tipoCredito = rs.getInt(15);
				cod_sub_apli_det = tipoCredito ;
				emisor = rs.getInt(16);
				intermediario = rs.getInt(17);
				cod_agen_interme = rs.getInt(18);
				cod_agen_intermediario = rs.getString(18);
				vtas_exterior = rs.getDouble("FN_VENTAS_NET_EXP");
				productoNafin = rs.getString("IC_PRODUCTO_NAFIN");

				// Leer codigo de la base de operaci�n din�mica si este fue especificado.
			   // F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
				scod_base_operac_dinamica 	= rs.getString("IG_BASE_OPERACION");
				scod_base_operac_dinamica  = scod_base_operac_dinamica == null || "".equals(scod_base_operac_dinamica.trim())?null:scod_base_operac_dinamica;
				if(scod_base_operac_dinamica  != null ){
					cod_base_operac_dinamica 	= rs.getInt("IG_BASE_OPERACION");
					existeCodigoBaseOpDinamica	= true;
				} else {
					cod_base_operac_dinamica	= 0;
					existeCodigoBaseOpDinamica = false;
				}

				/* Obtenemos la fecha */
				/*
				queri = "select fecha_hoy + "+plazo_proyecto+
					" , sysdate + "+ic_plazo+
					" from mg_calendario "+
					"where codigo_aplicacion = 'BLC'";
				*/
				queri = "select fecha_hoy + "+plazo_proyecto+
					" , trunc(sysdate) + ";
				queri += (ic_plazo>180)? " " + ic_plazo: " "+plazo_proyecto;
				queri +=" from mg_calendario "+
					"where CODIGO_EMPRESA = 1 AND codigo_aplicacion = 'BLC'";


//					System.out.println(queri);
				try {
					rs1=base.queryDB(queri);
				} catch(SQLException sqle) {
					System.out.println("ProyectosE.java(queri): " + queri );
					throw sqle;
				}
				while(rs1.next()){
					fecha_max_desem = rs1.getDate(1);/* Valor 13 */
					fecha_max_pago = rs1.getDate(2);/* Valor 14 */
				}
				rs1.close(); base.cierraStatement();
//						System.out.println(queri);

				/* Valor 18 y 19*/

				// INICIA FODA 044-2004
				// SE ELIMINA ESTA SECCION DEBIDO A SOLICITUD DE OTHON ROLDAN PARA EL FODEA 010-2009
				/*if (tablaAmort == 5 && intermediarioSirac == 60017  ) { //Caso especifico FIDE
					altaAutomaticaGarantia = "N";
					if (frecuencia_interes == 1 || frecuencia_interes == 7 || frecuencia_interes == 14 ) {
						cod_base_operac = 9005;
						//scod_base_operac = "9002";
						scod_base_operac = "9005";
					} else {
						//cod_base_operac = 9004;
						//scod_base_operac = "9004";
						cod_base_operac = 9005;
						scod_base_operac = "9005";
					}
					baseOperacion = true;

				} else {*/
					// INICIA FODA 049-2003
					if ("N".equals(rs.getString("CS_PLAZO_MAXIMO_FACTORAJE")) ) {
						tipo_plazo = rs.getString("CS_TIPO_PLAZO"); // Si es Factoraje o Credito
						queri="select op.ig_codigo_base, ig_producto_banco, cs_alta_automatica_garantia, ic_periodicidad, cg_tipo_interes, cs_tipo_renta " +
							"   FROM com_base_op_credito op "+
							"		, comcat_if i "+
							"	WHERE op.ic_if = i.ic_if"   +
							" 	    AND op.ig_tipo_cartera = i.ig_tipo_piso"   +
							"		AND op.ic_tasa = " + tasaIf +
							"	    AND op.ic_tabla_amort = " + tablaAmort +
							"	    AND op.ic_tipo_credito = " + tipoCredito +
							"	    AND op.ic_if = " + intermediario +
							"	    AND "+ic_plazo+" between op.ig_plazo_minimo AND op.ig_plazo_maximo"+
							"		AND NVL(op.ic_emisor, 0) = " + emisor +
							"	    AND op.cs_tipo_plazo = '" + tipo_plazo + "'"+
							(frecuencia_capital>=0?" AND op.ic_periodicidad = "+ frecuencia_capital:" AND op.ic_periodicidad is null ")+
							(!"".equals(tipoRenta)?" AND op.cs_tipo_renta = '"+tipoRenta+"'":" AND op.cs_tipo_renta is null ");
						if (productoNafin == null) {
							queri += "	AND op.ic_producto_nafin is null ";
						} else {
							queri += "	AND op.ic_producto_nafin = " + productoNafin;
						}
						//System.out.println(queri);
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("ProyectosE.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							// Si el codigo de la base de operaci�n din�mica fue especificado en COM_SOLIC_PORTAL,
							// utilizar este valor. F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
							if( existeCodigoBaseOpDinamica ){
								cod_base_operac  = cod_base_operac_dinamica;
								scod_base_operac = scod_base_operac_dinamica;
							} else {
								cod_base_operac  = rs1.getInt("IG_CODIGO_BASE");
								scod_base_operac = rs1.getString("IG_CODIGO_BASE");
							}
							cod_prod_banco	 = rs1.getInt("IG_PRODUCTO_BANCO");
							frecuencia_capital = rs1.getInt("IC_PERIODICIDAD");//SE AGREGA POR F004-2010 FVR
							tipoInteres = (rs1.getString("CG_TIPO_INTERES")==null?"":rs1.getString("CG_TIPO_INTERES"));//SE AGREGA POR F004-2010 FVR
							tipoRenta = (rs1.getString("CS_TIPO_RENTA")==null?"":rs1.getString("CS_TIPO_RENTA"));//SE AGREGA POR F020-2011 FVR
							baseOperacion = true;
							altaAutomaticaGarantia = rs1.getString("cs_alta_automatica_garantia");
						}else if(frecuencia_capital>=0 || !"".equals(tipoRenta)){
							queri="select op.ig_codigo_base, ig_producto_banco, cs_alta_automatica_garantia, ic_periodicidad, cg_tipo_interes " +
							"   FROM com_base_op_credito op "+
							"		, comcat_if i "+
							"	WHERE op.ic_if = i.ic_if"   +
							" 	    AND op.ig_tipo_cartera = i.ig_tipo_piso"   +
							"		AND op.ic_tasa = " + tasaIf +
							"	    AND op.ic_tabla_amort = " + tablaAmort +
							"	    AND op.ic_tipo_credito = " + tipoCredito +
							"	    AND op.ic_if = " + intermediario +
							"	    AND "+ic_plazo+" between op.ig_plazo_minimo AND op.ig_plazo_maximo"+
							"		AND NVL(op.ic_emisor, 0) = " + emisor +
							"	    AND op.cs_tipo_plazo = '" + tipo_plazo + "'"+
							"		AND op.cs_tipo_renta is null ";//SE AGREGA POR F020-2011 FVR
							if (productoNafin == null) {
								queri += "	AND op.ic_producto_nafin is null ";
							} else {
								queri += "	AND op.ic_producto_nafin = " + productoNafin;
							}

							//se realiza consulta par determinar la base de operacion
							try {
								rs3=base.queryDB(queri);
							} catch(SQLException sqle) {
								System.out.println("ProyectosE.java(queri): " + queri );
								throw sqle;
							}
							if(rs3.next()) {
								// Si el codigo de la base de operaci�n din�mica fue especificado en COM_SOLIC_PORTAL,
							   // utilizar este valor. F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
								if( existeCodigoBaseOpDinamica ){
									cod_base_operac  = cod_base_operac_dinamica;
									scod_base_operac = scod_base_operac_dinamica;
								} else {
									cod_base_operac  = rs3.getInt("IG_CODIGO_BASE");
									scod_base_operac = rs3.getString("IG_CODIGO_BASE");
								}
								cod_prod_banco	 = rs3.getInt("IG_PRODUCTO_BANCO");
								frecuencia_capital = rs3.getInt("IC_PERIODICIDAD");//SE AGREGA POR F004-2010 FVR
								tipoInteres = (rs3.getString("CG_TIPO_INTERES")==null?"":rs3.getString("CG_TIPO_INTERES"));//SE AGREGA POR F004-2010 FVR
								baseOperacion = true;
								altaAutomaticaGarantia = rs3.getString("cs_alta_automatica_garantia");
							}
							rs3.close();
						}
						//System.out.println(scod_base_operac);
						rs1.close();
						base.cierraStatement();
						//SE INTEGRA UPDATE A COM_SOLIC_PORTAL PARA EL PARAMETRO 36 TIPO_INTERES
						if(!tipoInteres.equals("")){
							queri = "update com_solic_portal set cg_tipo_interes = ? where ic_solic_portal = ? ";
							try {
								PreparedStatement psTipoInt = base.queryPrecompilado(queri);
								psTipoInt.setString(1,tipoInteres);
								psTipoInt.setInt(2, Integer.parseInt(folio));
								psTipoInt.executeUpdate();
								psTipoInt.close();
							} catch(SQLException sqle) {
								System.out.println("Error al Actualizar Tipo de Interes del proyecto en com_solic_portal");
								errorTransaccion = true;
							}
						}

					} else { // Se utiliza la base de Operaci�n Generica solo para factoraje.

						queri="select IG_BASE_OPER_GRAL from comcat_producto_nafin "+
							"where ic_producto_nafin = 1";
						//System.out.println(queri);
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("ProyectosE.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							cod_base_operac=rs1.getInt("IG_BASE_OPER_GRAL");
							scod_base_operac=rs1.getString("IG_BASE_OPER_GRAL");
							baseOperacion = true;
						}
						rs1.close();
						base.cierraStatement();

					}
				//}




				if (cod_base_operac!=0) {
					if(cod_prod_banco==0)//SE AGREGA CONDICION PARA DETERMINAR SI EL CODIGO PRODUCTO BANCO YA ESTA DEFINIDO F004-2010 FVR
						cod_prod_banco = (cod_prod_banco == 0)?(scod_base_operac.length() > 4)? Integer.parseInt(scod_base_operac.substring(0, 2)): Integer.parseInt(scod_base_operac.substring(0, 1)):cod_prod_banco;
				}

				// TERMINA FODA 049-2003

//					System.out.println(folio+" "+cod_agen+" "+cod_subapli+" "+no_lineafondeo+" "+cod_finan+" "+aprobado_por+" "+descripcion+" "+mto_operac+" "+estatus+" "+tipo_soli+" "+ic_docto+" "+ic_plazo+" "+fecha_max_desem+" "+cod_cliente+" "+cod_prod_banco+" "+ic_epo+" "+cod_base_operac+" "+desa_proveedores+" "+cod_aprobacion+" "+cod_agen_interme+" "+cod_grup_comision+" "+verific_estrato+" "+porcent_asignado+" "+cod_sub_apli_det);

				/* Llama al API (Store Procedure) */
				try {
					cs = base.ejecutaSP("LC_K_Apertura_Proyecto.Lc_Alta_Proyecto(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				} catch(SQLException sqle) {
						System.out.println("ProyectosE.java(ejecutaSP): " + "LC_K_Apertura_Proyecto.Lc_Alta_Proyecto(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						throw sqle;
				}

				cs.setNull(1, Types.INTEGER);
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, cod_agencia);
				cs.setInt(3, cod_sub_aplicacion);
				cs.setString(4, descripcion);
				cs.setString(5, aprobado_por);
				cs.registerOutParameter(5, Types.VARCHAR);
				cs.setString(6, cod_funcionario);
				cs.setDouble(7, mto_operac);
				//F084-2006
				cs.setLong(8, cod_cliente);
				cs.setDouble(9, mto_asignado);
				cs.setString(10, cod_tipo_linea);
				cs.setInt(11, frecuencia);
				cs.setInt(12, plazo_proyecto);
				cs.setDate(13, fecha_max_desem,Calendar.getInstance());
				cs.setDate(14, fecha_max_pago,Calendar.getInstance());
				if (icIF == 12) {
					cs.setNull(15, Types.INTEGER);
					cs.setNull(16, Types.INTEGER);
					cs.setNull(17, Types.INTEGER);
				} else {
					cs.setInt(15, no_lineafondeo);
					cs.setInt(16, cod_agen);
					cs.setInt(17, cod_subapli);
				}
				if (baseOperacion) {
					cs.setInt(18, cod_prod_banco);
					cs.setInt(19, cod_base_operac);
				} else {
					cs.setInt(18, Types.INTEGER);
					cs.setInt(19, Types.INTEGER);
				}
				cs.setString(20, desa_proveedores);
				cs.setNull(21, Types.INTEGER);
				if (icMoneda==54) {
					cs.setDouble(22, vtas_exterior);
				} else {
					cs.setNull(22, Types.INTEGER);
				}

				cs.setString(23, cod_aprobacion);

				cadena =	"p1:=null;"+"\n"+
				"p2:="+cod_agencia+";"+"\n"+
				"p3:="+cod_sub_aplicacion+";"+"\n"+
				"p4:='"+ descripcion +"';"+"\n"+
				"p5:="+ aprobado_por +";"+"\n"+
				"p6:='"+ cod_funcionario +"';"+"\n"+
				"p7:="+ mto_operac +";"+"\n"+
				"p8:="+ cod_cliente +";"+"\n"+
				"p9:="+ mto_asignado +";"+"\n"+
				"p10:='"+ cod_tipo_linea +"';"+"\n"+
				"p11:="+ frecuencia +";"+"\n"+
				"p12:="+ plazo_proyecto +";"+"\n"+
				"p13:=to_date('"+fecha_max_desem+"','dd/mm/yyyy');"+"\n"+
				"p14:=to_date('"+fecha_max_pago+"','dd/mm/yyyy');"+"\n";
				if (icIF == 12) {
					cadena += "p15:=null;"+"\n"+
					"p16:=null;"+"\n"+
					"p17:=null;"+"\n";
				} else {
					cadena += "p15:="+ no_lineafondeo +";"+"\n"+
					"p16:="+ cod_agen +";"+"\n"+
					"p17:="+ cod_subapli +";"+"\n";
				}

				if (baseOperacion) {
					cadena +=	"p18:="+ cod_prod_banco +";"+"\n"+
					"p19:="+ cod_base_operac +";"+"\n"+
					"p20:='"+ desa_proveedores +"';"+"\n";
				} else {
					desa_proveedores = null;
					cadena +=	"p18:=null;"+"\n"+
					"p19:=null;"+"\n"+
					"p20:="+ desa_proveedores +";"+"\n";
				}
				cadena +=	"p21:=null;"+"\n";
				if (icMoneda==54){
					cadena +=	"p22:="+vtas_exterior+";"+"\n";
				} else {
					cadena +=	"p22:=null;"+"\n";
				}

				cadena +=	"p23:="+ cod_aprobacion +";"+"\n";
				/* Valor 24 */
				if (cod_agen_intermediario == null) {
					queri="select ag.codigo_agencia_banco "+
						"from mg_agencias_generales ag, com_interfase c "+
						"where c.ic_solic_portal = " + folio + " AND ag.codigo_financiera = c.ig_codigo_financiera ";
//					System.out.println(queri);
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("ProyectosE.java(queri): " + queri );
						throw sqle;
					}
					if(rs1.next()) {
						cod_agen_interme=rs1.getInt(1);
						cs.setInt(24, cod_agen_interme);
						cadena = cadena + "p24:="+ cod_agen_interme +";"+"\n";
					} else {
						cs.setNull(24, Types.INTEGER);
						cadena = cadena + "p24:=null;"+"\n";
					}
				} else {
					cs.setInt(24, cod_agen_interme);
					cadena = cadena + "p24:="+ cod_agen_interme +";"+"\n";
				}
				rs1.close(); base.cierraStatement();

				cs.setString(25, reestructura);
				cs.setNull(26, Types.INTEGER);
				cs.setString(27, verific_estrato);
				cs.setDouble(28, porcent_asignado);
				cs.setInt(29, cod_sub_apli_det);
				cs.setDouble(30, mto_asignado);
						//cs.setNull(31, Types.INTEGER);
				cs.setDouble(31, porcent_asignado_det);
				cs.registerOutParameter(31, Types.INTEGER);
				cs.setDouble(32, mto_intermediario);
				cs.setDouble(33, mto_cliente);
				cs.setDouble(34, mto_asignado);
				cs.setNull(35, Types.INTEGER);
				cs.registerOutParameter(35, Types.INTEGER);

				cadena = cadena +
					"p25:="+ reestructura +";"+"\n"+
					"p26:=null;"+"\n"+
					"p27:='"+ verific_estrato +"';"+"\n"+
					"p28:="+ porcent_asignado +";"+"\n"+
					"p29:="+ cod_sub_apli_det +";"+"\n"+
					"p30:="+ mto_asignado +";"+"\n"+
					"p31:="+ porcent_asignado_det +";"+"\n"+
					"p32:="+ mto_intermediario +";"+"\n"+
					"p33:="+ mto_cliente +";"+"\n"+
					"p34:="+ mto_asignado +";"+"\n"+
					"p35:=null;"+"\n"+
					"p36:="+ tipoInteres +";"+"\n";//SE AGREGA POR F004-2010 FVR

	System.out.println(cadena);
				try {
					cs.execute();
						no_line_credit = cs.getLong(1); //F084-2006
						por_asig = cs.getDouble(31);
						//status = cs.getInt(35);
						status = cs.getString(35);
						if (status == null) {
							errorDesconocido = true;
							status="8";
						}
				} catch(SQLException sqle){
					System.out.println(sqle);
					cadena += sqle;
					errorDesconocido = true;
					status="9";
				}

				System.out.println("El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status);
				cadena += "El Valor de No Linea Credito: "+no_line_credit+" y Estatus "+status;
				/* Si hubo error se Inserta en com_control05 */
				if(!status.equals("0")) {
					base.terminaTransaccion(false); // El rollback al Store Procedure
					if (!errorDesconocido) {
						// Para la causa 43.
						if(status.equals("43")) {
							if (icMoneda==54) {
								queri = " SELECT fn_valor_compra * "  + mto_operac + " as monto_calc" +
									"	from com_tipo_cambio"  +
									"	where ic_moneda = 54"  +
									"	and dc_fecha = (select max(dc_fecha)"  +
									"                from com_tipo_cambio"  +
									"                where ic_moneda = 54) ";

								try {
									rs1=base.queryDB(queri);
								} catch(SQLException sqle) {
									System.out.println("ProyectosE.java(queri): " + queri );
									throw sqle;
								}
								if(rs1.next()) {
									mto_operac = rs1.getDouble(1);
								}
								rs1.close(); base.cierraStatement();
							}
							queri = "select codigo_aprobacion, cupo_minimo, cupo_limite"+
									" from mg_aprobacion"+
									" where "+mto_operac+" between cupo_minimo and cupo_limite";
							try {
								rs1=base.queryDB(queri);
							} catch(SQLException sqle) {
								System.out.println("ProyectosE.java(queri): " + queri );
								throw sqle;
							}
							if(rs1.next()) {
								aprobado_por = rs1.getString(1);
								descripcion = "Descripcion especial Causa 43";
							}
							rs1.close(); base.cierraStatement();
						}

						queri = "select cc_codigo_aplicacion from comcat_error_procesos "+
							"where ic_error_proceso="+status+
							" and cc_codigo_aplicacion='PY'";
//								System.out.println(queri);
						try {
							rs1=base.queryDB(queri);
						} catch(SQLException sqle) {
							System.out.println("ProyectosE.java(queri): " + queri );
							throw sqle;
						}
						if(rs1.next()) {
							codigo_aplicacion=rs1.getString(1);
						} else {
							codigo_aplicacion="NE";
							status="6";
						}
						rs1.close(); base.cierraStatement();
					} else { //fin if (!errorDesconocido)
						codigo_aplicacion = "NE";
						if (status.equals(""))
							status = "6";
					}

					queri = "select count(1) from com_interfase where ic_solic_portal="+folio;
					try {
						rs1=base.queryDB(queri);
					} catch(SQLException sqle) {
						System.out.println("ProyectosE.java(queri): " + queri );
						throw sqle;
					}
					while(rs1.next())
						no_doctos=rs1.getInt(1);

					rs1.close(); base.cierraStatement();
					aprobado_por=(aprobado_por==null)?"null":"'"+aprobado_por+"'";
					descripcion=(descripcion==null)?"null":"'"+descripcion+"'";

					if(no_doctos!=0) {
						queri = "update com_interfase set cc_codigo_aplicacion='"+codigo_aplicacion+"' "+
							" , ic_error_proceso="+status+
							" , ig_codigo_agencia="+cod_agen+" "+
							" , ig_codigo_sub_aplicacion="+cod_subapli+
							" , ig_numero_linea_fondeo="+no_lineafondeo+" "+
							" , ig_codigo_financiera="+cod_finan+
							" , ig_aprobado_por="+aprobado_por+
							" , cg_descripcion="+descripcion+
							" , df_hora_proceso=SYSDATE"+
							" , ig_proceso_numero = 1"+
							" , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena, '\'', "\'")  + "'" +
							" where ic_solic_portal="+folio;

						//FODEA 011-2010 FVR
						querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET ic_error_proceso = ? " +
							" , cc_codigo_aplicacion = ? "+
							" WHERE ic_solic_portal = ? ";
					}

					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos de error en com_interfase.");
						errorTransaccion = true;
					}
					//FODEA 011-2010 FRV
					try {
						PreparedStatement ps = base.queryPrecompilado(querySolicPortal);
						ps.setInt(1, Integer.parseInt(status));
						ps.setString(2,codigo_aplicacion);
						ps.setInt(3, Integer.parseInt(folio));
						ps.executeUpdate();
						ps.close();
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_solic_portal");
						errorTransaccion = true;
					}


				}
				else {
					/* Si no hubo ningun error se Actualizan los valores de con_interfase */
					queri = "Update com_interfase " +
							"	set ig_numero_linea_credito = " + no_line_credit +
							"	, ig_codigo_agencia_proy = " + cod_agencia +
							"	, ig_codigo_sub_aplicacion_proy = " + cod_sub_aplicacion +
							"   , df_hora_proceso=SYSDATE"+
							"   , DF_PROYECTOS=SYSDATE"+
							"   , ig_proceso_numero = 1"+
							"   , cg_proyectos_log = '" + Comunes.protegeCaracter(cadena, '\'', "'") + "'" +
							" WHERE ic_solic_portal = " + folio ;

					querySolicPortal =
							" UPDATE com_solic_portal " +
							" SET cs_alta_automatica_garantia = ? " +
							" , ig_base_operacion = "+cod_base_operac+" "+//FODEA 013 - 2009 ACF - EGB
							" WHERE ic_solic_portal = ? ";
//							System.out.println(queri);
					try {
						base.ejecutaSQL(queri);
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_interfase");
						errorTransaccion = true;
					}
					try {
						PreparedStatement ps = base.queryPrecompilado(querySolicPortal);
						ps.setString(1,altaAutomaticaGarantia);
						ps.setInt(2, Integer.parseInt(folio));
						ps.executeUpdate();
						ps.close();
					} catch(SQLException sqle) {
						System.out.println("Error al Actualizar los Datos del proyecto en com_solic_portal");
						errorTransaccion = true;
					}

				}
				if (!errorTransaccion) {
					base.terminaTransaccion(true);
				} else {
					base.terminaTransaccion(false);
				}
			} //while
			//System.out.println("Finalizo Query OPTIMIZADO");
		} catch(Exception e) {
			System.out.println("Error: "+e);
			e.printStackTrace();
			System.out.println(base.mostrarError());
			base.terminaTransaccion(false);
			try{Thread.sleep(1000*60*10);}catch(Exception _e){}
		} finally {
			if (base.hayConexionAbierta()){
				base.cierraConexionDB();
			}
		}
	}
}
