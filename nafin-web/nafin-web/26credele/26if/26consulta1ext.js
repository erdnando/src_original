Ext.onReady(function(){

//----------------Handlers------------------------------



var procesarConsultaData = function(store,arrRegistros,opts){
			//Ext.getCmp('btnBajarArchivo').setVisible(false);
			//Ext.getCmp('btnBajarPDF').setVisible(false);
			
			pnl.el.unmask();
			
			if(arrRegistros!=null)
			{
				if (!grid.isVisible()) 
				{ 
					grid.show();
				}
				
				
				var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
				var btnImprimirPDF 	 = Ext.getCmp('btnGenerarPDF'); 
				var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
				var btnBajarPDF 		 = Ext.getCmp('btnBajarPDF');
				var el = grid.getGridEl();
			
				btnBajarArchivo.hide();
				btnBajarPDF.hide();
			
				if(store.getTotalCount()>0)
				{
					el.unmask();
					btnGenerarArchivo.enable();
					btnImprimirPDF.enable();
					//btnBajarArchivo.setVisible(false);
					//btnBajarArchivo.enable();
					Ext.getCmp('barraPaginacion').show();
					
					
					
				}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
						Ext.getCmp('barraPaginacion').hide();
						Ext.getCmp('btnGenerarPDF').disable();
						Ext.getCmp('btnGenerarArchivo').disable();
						Ext.getCmp('btnBajarPDF').disable();
						Ext.getCmp('btnBajarArchivo').disable();
					}
			}
		}
		
var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//---------------STORES----------------------
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '26consulta1ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'SIRAC'},
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'CG_RFC'},
				{name: 'CS_TIPO_PERSONA'},
				{name: 'SE_NOMBRE'},
				{name: 'SUB_NOMBRE'},
				{name: 'R_NOMBRE'},
				{name: 'C_NOMBRE'},
				{name: 'IC_ESTRATO'},
				{name: 'IC_NAFIN_ELECTRONICO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

//-------------Componentes---------------------
	var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'N�mero de Cliente SIRAC',
						tooltip: 'N�mero de Cliente SIRAC',
						sortable: true,
						dataIndex: 'SIRAC',
						width: 120,
						align: 'center'
						},
						{
						
						header: 'Nombre Completo o Razon Social',
						tooltip: 'Nombre Completo o Razon Social',
						sortable: true,
						dataIndex: 'CG_RAZON_SOCIAL',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'RFC',
						tooltip: 'RFC',
						sortable: true,
						dataIndex: 'CG_RFC',
						width: 140,
						align: 'center'
						},
						{
						header: 'Tipo de persona',
						tooltip: 'Tipo de persona',
						sortable: true,
						dataIndex: 'CS_TIPO_PERSONA',
						width: 130,
						align: 'center'
						/*renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='F')
								return 'Fisica';
								else
								return 'Moral';
							}
							*/
						},
						{
						header: 'Sector Econ�mico',
						tooltip: 'Sector Econ�mico',
						sortable: true,
						dataIndex: 'SE_NOMBRE',
						width: 130,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						align:'center',
						header: 'Subsector',
						tooltip: 'Subsector',
						sortable: true,
						dataIndex: 'SUB_NOMBRE',
						width: 150,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},{
						
						header:'Rama',
						tooltip: 'Rama',
						sortable: true,
						dataIndex: 'R_NOMBRE',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						
						header: 'Clase',
						tooltip: 'Clase',
						sortable: true,
						dataIndex: 'C_NOMBRE',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Estrato',
						tooltip: 'Estrato',
						sortable: true,
						dataIndex: 'IC_ESTRATO',
						width: 100,
						align: 'center'
						},
						{
						header: 'Nafin Electr�nico',
						tooltip: 'Nafin Electr�nico',
						sortable: true,
						dataIndex: 'IC_NAFIN_ELECTRONICO',
						width: 120,
						align: 'center'
												
						}				
				],
				stripeRows: true,
				loadMask: false,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					//autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "",
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar PDF',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '26consulta1ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},
								
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								'-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '26consulta1ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								}
								,'-'
								]
				}
		});


var elementosForma=[
						{
							xtype: 'numberfield',
							name: 'SIRAC',
							id: 'SIRAC',
							fieldLabel: 'No. de Cliente SIRAC',
							anchor:'80%',
							maxLength: 9
						},{
							xtype: 'textfield',
							name: 'R_F_C',
							id: 'R_F_C',
							anchor:'80%',
							fieldLabel: 'R.F.C.',
							regex:/^([A-Z|a-z|&amp;]{3,4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/
							
						},{
							xtype: 'textfield',
							name: 'nomRazon',
							id: 'nomRazon',
							anchor:'80%',
							fieldLabel: 'Nombre � Razon Social'
							
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de registro de',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaReg',
									id: 'fechaReg',
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaReg2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaReg2',
									id: 'fechaReg2',
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaReg',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}

]



var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  //xtype: 'button',
			  text: 'Consultar',
			  id: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
							
							//Funcion Ajax
							
							//Ext.getCmp('btnBajarArchivo').setVisible(false);
							//Ext.getCmp('btnBajarPDF').setVisible(false);
							
							
							//////////////Para Validar que no haga la consulta sin las Fechas/////////////
							var fechaReg1 = Ext.getCmp('fechaReg');
							var fechaReg2 = Ext.getCmp('fechaReg2');
							
							if (!Ext.isEmpty(fechaReg1.getValue()) || !Ext.isEmpty(fechaReg2.getValue()) ) {
								if(Ext.isEmpty(fechaReg1.getValue()))	{
									fechaReg1.markInvalid('Debe capturar ambas fechas de registro');
									fechaReg1.focus();
									return;
								}else if (Ext.isEmpty(fechaReg2.getValue())){
									fechaReg2.markInvalid('Debe capturar ambas fechas de registro');
									fechaReg2.focus();
									return;
								}
							}
							//////////////////////////////////////////////////////////////////////////////
							if((Ext.getCmp('fechaReg').getValue()!=''&&Ext.getCmp('fechaReg2').getValue()!='')||(Ext.getCmp('SIRAC').getValue()!=''||Ext.getCmp('R_F_C').getValue()!=''||Ext.getCmp('nomRazon').getValue()!='')){
								
								pnl.el.mask('Enviando...', 'x-mask-loading');
								consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
								
							}else{
								
								if(Ext.getCmp('fechaReg').getValue()!=''||Ext.getCmp('fechaReg2').getValue()!=''){
									Ext.MessageBox.alert('Error','Debe de capturar ambas fechas para usar este crit�rio de busqueda');
								}else{
									Ext.MessageBox.alert('Error','Debe de capturar al menos un crit�rio de busqueda');
								}
							}
						
								

						
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});

//------------Principal--------------------------

var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [fp,NE.util.getEspaciador(30),grid]
  });
});