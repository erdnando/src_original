Ext.onReady(function() {

//------------------------------Handlers-------------
var procesarAltaPersona =  function(opts, success, response) {
	var cmpForma = Ext.getCmp('forma');
				cmpForma.el.unmask();
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				
				var storeInfo= Ext.util.JSON.decode(response.responseText).registros;
				
				var informe = new Ext.Window({
							width: 445,height: 300,
							frame: true,
							title: 'Afiliaci�n Cr�dito Electr�nico',
							align: 'center',
							maximizable: false,
							modal: true,
							closeAction: 'destroy',
							resizable: false,
							minWidth: 410,
							minHeight: 300,
							maximized: false,
							items: [
								elementosCoins
							],
							constrain: true}).show();		
							
							resu="";
							for(i =0;i<storeInfo.length;i++){
								
								addRow(storeInfo[i].SIRAC,storeInfo[i].NE,storeInfo[i].TROYA);
							}
							var forma = Ext.getDom('formAux');
								forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
							var forma2 = Ext.getDom('formAux2');
								forma2.action = Ext.util.JSON.decode(response.responseText).urlPDF;
									
								
		}else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
//agrega fila
function addRow(NSirac,NE,TROYA){
	var newRow=Ext.getCmp('idInforme');
			if(TROYA)
			{
				newRow.add({html: '<div align="center">'+NSirac+'</div>'},{html: '<div align="center">'+NE+'</div>'},
				{html:'<div align="center">'+TROYA+'</div>'});
			}
			else
			{
				newRow.add({html: '<div align="center">'+NSirac+'</div>'},{html: '<div align="center">'+NE+'</div>'});
			}
		newRow.doLayout();
}

	///fin de handler alta
var procesarTipoUsuario = function(response ) {
		
		if (Ext.util.JSON.decode(response.responseText).success == true) {
		
			catalogoIntermediario.load();
			catalogoPais.load();
			catalogoPais2.load();
			catalogoSectorEco.load();
			ocultaMoral();
		
			if(Ext.util.JSON.decode(response.responseText).User =='NAFIN' )
				{			
					Ext.getCmp("afiliacionPanel").show();
					Ext.getCmp("Alta_TroyaDiv").show();					
				}
				else
				{
					Ext.getCmp("afiliacionPanel").hide();
					Ext.getCmp("Alta_TroyaDiv").hide();
				}
			
		} else {
			NE.util.mostrarConnError(response);
		}
	}
	
//------------------Stores-------------------------
var storeAfiliacion = new Ext.data.SimpleStore({
    fields: ['clave', 'afiliacion'],
    data : [['EPO','Cadena Productiva'],['IF','Intermediario Financiero'],
				['D','Distribuidor de Fondos'],['2','Distribuidor'],['1','Proveedor']
				,['ACE','Afiliados Credito Electronico']]});
				
var storeTipoPersona = new Ext.data.SimpleStore({
    fields: ['clave', 'TipoPersona'],
    data : [['F','Fisica'],['M','Moral']]});


var catalogoIntermediario = new Ext.data.JsonStore
  ({
	   id: 'catIntermediario',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'intermediarioF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  var catalogoPais = new Ext.data.JsonStore
  ({
	   id: 'catPais1',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Pais1'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoPais2 = new Ext.data.JsonStore
  ({
	   id: 'catPais2',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Pais2'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
//Estado
 var catalogoEstado = new Ext.data.JsonStore
  ({
	   id: 'catEstado',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Estado'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
//Municipio
 var catalogoMunicipio= new Ext.data.JsonStore
  ({
	   id: 'catMunicipio',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Municipio'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  //Ciudad
 var catalogoCiudad= new Ext.data.JsonStore
  ({
	   id: 'catCiudad',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Ciudad'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
    //Sector economico
 var catalogoSectorEco= new Ext.data.JsonStore
  ({
	   id: 'catSectorEco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'SectorEco'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
//SubSector
 var catalogoSubSector= new Ext.data.JsonStore
  ({
	   id: 'catSubSector',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'SubSector'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  //Rama
  var catalogoRama= new Ext.data.JsonStore
  ({
	   id: 'catRama',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
		{
		 informacion: 'Rama'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  //Clase
  var catalogoClase= new Ext.data.JsonStore
  ({
	   id: 'catClase',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26forma1ext.data.jsp',
		baseParams: 
			{
				informacion: 'Clase'
			},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

  //--------------------funciones extras----------------
  ////////////////////////////////////////////////////////
 
	function ocultaFisica()
	{
		Ext.getCmp('panelFisica').hide();
	
		Ext.getCmp('Apellido_paternoF').allowBlank= true;
		Ext.getCmp('NombreF').allowBlank= true;
		Ext.getCmp('SexoF').allowBlank= true;
		Ext.getCmp('Apellido_maternoF').allowBlank= true;
		Ext.getCmp('R_F_CF').allowBlank= true;
		Ext.getCmp('Fecha_de_nacimientoF').allowBlank= true;
			
		//pesona moral
		Ext.getCmp('panelMoral').show();
		Ext.getCmp('Razon_SocialM').allowBlank= false;
		Ext.getCmp('R_F_CM').allowBlank= false;
		
	}
	function ocultaMoral()
	{
		//persona fisica
		
		Ext.getCmp('panelFisica').show();
		Ext.getCmp('Apellido_paternoF').allowBlank= false;
		Ext.getCmp('NombreF').allowBlank= false;
		Ext.getCmp('SexoF').allowBlank= false;
		Ext.getCmp('Apellido_maternoF').allowBlank= false;
		Ext.getCmp('R_F_CF').allowBlank= false;
		Ext.getCmp('Fecha_de_nacimientoF').allowBlank= false;
				
		//pesona moral
		Ext.getCmp('panelMoral').hide();
		Ext.getCmp('Razon_SocialM').allowBlank= true;
		Ext.getCmp('R_F_CM').allowBlank= true;
		}
    
/////////////////////////////////////////////////
//---------------Componentes---------------------
/////////////////////////////////////////////////

var afiliacionPanel 
 = {
		
		xtype: 'panel',
		title: 'USUARIO NAFIN',
		autoWidth: true,
		hidden:true,
		id:'afiliacionPanel',
		style: ' margin:0 auto;',
   	bodyStyle:'padding:10px',
		defaults: {
		msgTarget: 'side',
		anchor: '-20'
		},
		items:[	
		//Afiliacion
				{
					xtype: 'compositefield',
					combineErrors: false,				
					msgTarget: 'side',
					items: [
					{
					  xtype: 'combo',
					  name: 'afiliacion',
					  hiddenName :'afiliacion',
					  forceSelection: true,
					  triggerAction : 'all',
					  fieldLabel: 'afiliacion',
					  mode: 'local',
					  valueField: 'clave',
					  store: storeAfiliacion,
					  displayField:'afiliacion'
					}]
				},
				//Intermediario:
				{
					xtype: 'compositefield',
					combineErrors: false,				
					msgTarget: 'side',
					items: [
					{
					  xtype: 'combo',
					  name: 'intermediario',
					  hiddenName :'intermediario',
					  fieldLabel: 'intermediario',
					  mode: 'local',
					  triggerAction: 'all',
					  valueField: 'clave',
					  store: catalogoIntermediario,
					  displayField: 'descripcion'
					}]
				}
				]};
//Panel persona fisica
var personaFisicaPanel 
 = {
	xtype: 'panel',
	title: 'Nombre completo',
	width:'auto',
	id:'panelFisica',	
			defaults: {
	msgTarget: 'side',
	anchor: '-20'
	},
	items:[	
	 {
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{//apellido Paterno
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'Apellido_paterno',
							id: 'Apellido_paternoF',
							fieldLabel: '*Apellido paterno',
							allowBlank: false,
								width: 200}]
							
						}	,{//Apellido Materno
							layout: 'form',	
							width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'Apellido_materno',
							id: 'Apellido_maternoF',
							fieldLabel: '*Apellido Materno',
							allowBlank: false,
								width: 200}]
						}
		]},
		{
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
		items: [
					{//Nombres
							layout: 'form',
							 width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'Nombre',
							id: 'NombreF',
							fieldLabel: '*Nombres(s)',
							allowBlank: false,
								width: 200}]
							
						}	,{//RFC
							layout: 'form',	
							 width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'R_F_C',
							id: 'R_F_CF',
							fieldLabel: '* R.F.C.',
							//regex:/^([A-Z|a-z|&amp;]{4})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
							regex:	/^([A-Z,�,&]{4})-([0-9]{2}[0-1][0-9][0-3][0-9])-[0-9A-Za-z]{3}$/,	//Copiada de internet y modificada por HVC
							regexText:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones'+
							'en el formato NNNN-AAMMDD-XXX donde:<br>'+
							'NNNN:son las iniciales del nombre y apellido<br>'+
							'AAMMDD: es la fecha de nacimiento menor al dia de hoy<br>'+
							'XXX:es la homoclave',
							allowBlank: false,
								width: 200}]
						}
		]},
		{
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
		items: [
					{//sexo
							layout: 'form',
							 width:500,
							border: false,
							items:[{
							xtype: 'radiogroup',
							fieldLabel: '*Sexo',
							hiddenName:'Sexo',
							name:'Sexo',
							id: 'SexoF',
							valueField:'Sexo',
							allowBlank: false,
							items: [
								 {boxLabel: 'F', name: 'Sexo',inputValue: 'F'},
								 {boxLabel: 'M', name: 'Sexo',inputValue: 'M'}
							]
							}]
							
						}	,{
							layout: 'form',	
							 width:500,
							border: false,
							items:[{
							//Fecha de nacimiento
							xtype: 'datefield',
							name:'Fecha_de_nacimiento',
							id:'Fecha_de_nacimientoF',
							listeners:{
								blur:function(){
									
									var rfc=Ext.getCmp('R_F_CF');
									var fechaHoy = new Date();
									if(rfc.isValid(true))
									{
								if(navigator.appName =='Microsoft Internet Explorer')
				var anioHoy = fechaHoy.getYear() + 1900;
			
		else
		var anioHoy = fechaHoy.getYear();
		var finAnoHoy = parseInt(anioHoy.toString().substr(2,4));
										
						
										
									var finAnoHoy = parseInt(anioHoy.toString().substr(2,4));
										var valRFC=rfc.getValue();
										var guion = valRFC.indexOf("-") + 1;
										var fechaRFC_AAMMDD = valRFC.substr(guion,6);
										var anoDado = parseInt(fechaRFC_AAMMDD.substr(0,2));
										if(anoDado > finAnoHoy && anoDado < 99)
											var strAnio = "19"+fechaRFC_AAMMDD.substr(0,2);
										else
											var strAnio = "20"+fechaRFC_AAMMDD.substr(0,2);
										
										var strMes = fechaRFC_AAMMDD.substr(2,2);
										var strDia = fechaRFC_AAMMDD.substr(4,2);
										var strFechaNacim = strDia+"/"+strMes+"/"+strAnio
									
									var fechaFor=Ext.getCmp('Fecha_de_nacimientoF');
										if(fechaFor.getValue() != "" || fechaFor.getValue() == ""){
											fechaFor.setValue(strFechaNacim);
									}}
								}
							},
							fieldLabel: '*Fecha de nacimiento:(dd/mm/aaaa)',
							allowBlank: false,
								width: 200
							}]
						}
		]},
		{
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
		items: [
					{
							layout: 'form',
							 width:500,
							border: false,
							items:[{
							//curp
							xtype: 'textfield',
							name: 'curp',
							id: 'curpF',
							fieldLabel: 'CURP',
							allowBlank: true,
							maxLength : 18,
							minLength : 18,
							width: 200
							}]
							
						}	,{
							layout: 'form',	
							width:500,
							border: false,
							items:[{
							//FIEL
							xtype: 'textfield',
							name: 'fiel',
							id: 'fielF',
							allowBlank: true,
							fieldLabel: 'FIEL',
							maxLength : 25,
							minLength : 25,
							width: 200
							
							}]
						}
		]},
		{
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
		items: [
					{
							layout: 'form',
							 width:500,
							border: false,
							items:[{
							//Pais de origen							
							id: 'paisOrigenF',
							xtype: 'combo',
						  name: 'paisOrigen',
						   hiddenName :'paisOrigen',
						  fieldLabel: 'Pais de Origen',
						  mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  store: catalogoPais,
						  displayField: 'descripcion',
							width: 200
							}]
							
						}
		]}
	 
	 ]
};

//Panel persona Moral
var personaMoralPanel 
 = {
 xtype: 'panel',
	title: 'Nombre completo',
	width:'auto',
	
			defaults: {
	msgTarget: 'side',
	anchor: '-20'
	},	 
	 id:'panelMoral',    
	  items:[
	  {
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{//r SOCIAL
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'Razon_Social',
							id: 'Razon_SocialM',
							fieldLabel: '* Raz�n Social',
							allowBlank: false}]
							
						}	,{//rfc
							layout: 'form',	
							width:500,
							border: false,
							items:[{
							//RFC
							xtype: 'textfield',
							name: 'R_F_C',
							id: 'R_F_CM',
							fieldLabel: '* R.F.C.',
							//regex:/^([A-Z|a-z|&amp;]{3})-(([0-9]{2})([0][13456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([a-zA-Z0-9]{3}$)/,
							regex:	/^([A-Z,�,&]{3})-([0-9]{2}[0-1][0-9][0-3][0-9])-[0-9A-Za-z]{3}$/,	//Copiada de internet y modificada por HVC
							regexText:'Por favor escriba correctamente el RFC en mayusculas y separado por guiones'+
							'en el formato NNN-AAMMDD-XXX donde:<br>'+
							'NNN:son las iniciales del nombre de la empresa<br>'+
							'AAMMDD: es la fecha de creacion de la empresa menor al dia de hoy<br>'+
							'XXX:es la homoclave',
							allowBlank: false		
							}]
						}
		]},
		 {
		xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{//FIEL
							layout: 'form',	
							width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'fiel',
							id: 'fielM',
							fieldLabel: 'FIEL'}]
							
						}	,{//pAIS
							layout: 'form',	
							width:500,
							border: false,
							items:[{
							id: 'paisOrigenM',
						  xtype: 'combo',
						  name: 'paisOrigen',
						  hiddenName :'paisOrigen',
						  fieldLabel: 'Pais de Origen',
						  mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  store: catalogoPais,
						  displayField: 'descripcion',
							width: 200}]
						}
		]}
		
		///UNO
	 
						
					]
				};

//Panel Domicilio
var domicilioPanel 
 = {
	 xtype: 'panel',
	 title: 'Domicilio',
   width:'auto',
	
			defaults: {
	msgTarget: 'side',
	anchor: '-20'
	},	 
    
	 items:[
	 {
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Calle
							xtype: 'textfield',
							name: 'Calle',
							id: 'CalleD',
							fieldLabel: '* Calle, No. Exterior y No. Interior',
							allowBlank: false	}]
							
						}	,{
							layout: 'form',	
							width:500,
							border: false,
							items:[{
						//Colonia
							xtype: 'textfield',
							name: 'Colonia',
							id: 'ColoniaD',
							fieldLabel: '* Colonia',
							allowBlank: false	
							}]
						}
		]},
		{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							xtype: 'textfield',
							name: 'Codigo_postal',
							id: 'Codigo_postalD',
							fieldLabel: '* C�digo postal',
							regex:/^(\+|-)?\d+$/,
							regexText:'Este campo acepta solo n�meros',
							maxLength:5,
							minLength:5,
							allowBlank: false,
							maxLength:5	}]
							
						}	,{
						layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Pais						
						  id: 'paisOrigenD',
						  xtype: 'combo',
						  name: 'Pais',
						  hiddenName :'Pais',
						  fieldLabel: '*Pais',
						  mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  allowBlank: false,
						  store: catalogoPais2,
						  emptyText:'Seleccione...',
						  displayField: 'descripcion',
							width: 200,
							listeners: {
								select: function(combo, record, index) {
								Ext.getCmp('EstadoD').reset();
									 catalogoEstado.load();
								}
							}
							}]
						}]},//tres
						{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Estado
							xtype: 'combo',
							name: 'Estado',
							hiddenName : 'Estado',
							id: 'EstadoD',
							fieldLabel: '*Estado',
							allowBlank: false,
							mode: 'local',
							typeAhead: true,
							triggerAction: 'all',
							valueField: 'clave',
							emptyText:'Seleccione...',
							store: catalogoEstado,
							displayField: 'descripcion',
							width: 200,
							listeners: {
								select: function(combo, record, index) {
									 
									 Ext.getCmp('Delegacion_o_municipioD').reset();
									 Ext.getCmp('ciudadD').reset();
									 
									 catalogoMunicipio.load({
										params: Ext.apply({
										Est:record.json.clave
										})
									});
									
									catalogoCiudad.load({
										params: Ext.apply({
										Est:record.json.clave
										})
									});
								}
							}}]
							
						}	,{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Delegacion o municipio
							xtype: 'combo',
							name: 'Delegacion_o_municipio',
							 hiddenName :'Delegacion_o_municipio',
							id: 'Delegacion_o_municipioD',
							fieldLabel: '*Delegaci�n o Municipio',
							allowBlank: false,		
							emptyText:'Seleccione...',
						  mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  store: catalogoMunicipio,
						  displayField: 'descripcion',
							width: 200
							}]
						}]},
						//panle 4
							{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Ciudad
							xtype: 'combo',
							name: 'ciudad',
							hiddenName : 'ciudad',
							id: 'ciudadD',
							fieldLabel: 'Ciudad',
							emptyText:'Seleccione...',
							mode: 'local',
						  triggerAction: 'all',
						  valueField: 'clave',
						  store: catalogoCiudad,
						  displayField: 'descripcion',
							width: 200
							}]
							
						}	,{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//* Tel�fono
							xtype: 'textfield',
							name: 'Telefono',
							id: 'TelefonoD',
							fieldLabel: '* Tel�fono',
							anchor:'50%',
							regexText:'Este campo acepta solo n�meros',
							allowBlank: false,
							regex:/(-)*\d+$/
							}]
						}]},
						//panel 5
						{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							// E-mail
							xtype: 'textfield',
							name: 'Email',
							id: 'EmailD',
							fieldLabel: '* E-mail',
							allowBlank: false,
							vtype: 'email'
							}]
							
						}]}
						]};

//Diversos

var DiversosPanel 
 = {
  xtype: 'panel',
	 
   width:'auto',
	
			defaults: {
	msgTarget: 'side',
	anchor: '-20'
	},	 
	
	 title: 'Diversos',
   
    
	  items:[
	  {
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Ventas netas totales
							xtype: 'numberfield',
							minValue:5000,
							name: 'Ventas_netas_totales',
							id: 'Ventas_netas_totalesDiv',
							fieldLabel: '* Ventas netas totales',
							allowBlank: false	
							}]
							
						}	,{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//* N�mero de empleados
							xtype: 'textfield',
							name: 'Numero_de_empleados',
							id: 'Numero_de_empleadosDiv',
							fieldLabel: '* N�mero de empleados',
							anchor:'50%',
							regex:/^(\+|-)?\d+$/,
							allowBlank: false
							}]
						}]},
						//dos
						{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							// Sector econ�mico:
							xtype: 'combo',
							name: 'Sector_economico',
							hiddenName :'Sector_economico',
							id: 'Sector_economicoDiv',
							fieldLabel: '* Sector econ�mico',
							allowBlank: false,	
							mode: 'local',
							emptyText:'Seleccione...',
							triggerAction: 'all',
							valueField: 'clave',
							store: catalogoSectorEco,
							displayField: 'descripcion',
							width: 300,
							listeners: {
								select: function(combo, record, index) {
								Ext.getCmp('SubsectorDiv').reset();
								Ext.getCmp('ClaseDiv').reset();
								Ext.getCmp('RamaDiv').reset();
								
									catalogoSubSector.load({
										params: Ext.apply({
										Sector:record.json.clave
										})
									});
								
								}
							}
							}]
							
						}	,{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//* Subsector:
							xtype: 'combo',
							name: 'Subsector',
							hiddenName :'Subsector',
							id: 'SubsectorDiv',
							fieldLabel: '* Subsector',
							allowBlank: false,
							mode: 'local',
							triggerAction: 'all',
							emptyText:'Seleccione Sector',
							valueField: 'clave',
							store: catalogoSubSector,
							displayField: 'descripcion',
							width: 250,
							listeners: {
								select: function(combo, record, index) {
									Ext.getCmp('RamaDiv').reset();
									Ext.getCmp('ClaseDiv').reset();
								
									if(Ext.getCmp('Sector_economicoDiv').getValue()!="" && 
											record.json.clave!="")
											{
												//Hacer peticion de catalogo
												catalogoRama.load({
													params: Ext.apply({
													Sector:Ext.getCmp('Sector_economicoDiv').getValue(),
													SubSector:record.json.clave
													})
												});
											}
											
								}
							}
							}]
						}]},
						//tres
						{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//* Rama
						
							xtype:'combo',
							name: 'Rama',
							hiddenName :'Rama',
							id: 'RamaDiv',
							fieldLabel: '* Rama',
							allowBlank: false,	
							mode: 'local',
							triggerAction: 'all',
							valueField: 'clave',
							store: catalogoRama,
							emptyText:'Seleccione SubSector',
							displayField: 'descripcion',
							width: 300,
							listeners: {
								select: function(combo, record, index) {
								Ext.getCmp('ClaseDiv').reset();
								if(Ext.getCmp('Sector_economicoDiv').getValue()!="" && 
								Ext.getCmp('SubsectorDiv').getValue()!="" && 
											record.json.clave!="")
											{
												catalogoClase.load({
													params: Ext.apply({
													Sector:Ext.getCmp('Sector_economicoDiv').getValue(),
													SubSector:Ext.getCmp('SubsectorDiv').getValue(),
													Rama:record.json.clave
													})
												});
											}
								}
							}	
							}]
							
						}	,{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//* Clase
							xtype: 'combo',
							name: 'Clase',
							hiddenName :'Clase',
							id: 'ClaseDiv',
							fieldLabel: '* Clase',
							allowBlank: false,
							mode: 'local',
							triggerAction: 'all',
							valueField: 'clave',
							store: catalogoClase,
							displayField: 'descripcion',
							width: 250,
							emptyText:'Seleccione Rama'
							}]
						}]},
						//cuatro
						{
	 xtype:'panel', 
		layout:'hbox',
		border: false,
		width:'auto',
	
		items: [
					{
					 layout: 'form',
					 width:500,
							border: false,
							items:[{
							//Alta del cliente a TROYA
							 xtype: 'checkboxgroup',
							
							name: 'Alta_Troya',
							id: 'Alta_TroyaDiv',
							fieldLabel: 'Alta del cliente a TROYA',
							items: [
							{boxLabel: '', name: 'Alta_Troya'}],
							anchor:'50%',
							allowBlank: true,
							hidden:true
							}]
							
						}]}
					]};
//fin diversos


//INFORME
var elementosCoins = [
		{
			xtype: 'panel',id:'idInforme',layout:'table',width:430,border:true,layoutConfig:{ columns: 2 },
			defaults: {frame:false, border:true,width:215, height: 35,bodyStyle:'padding:8px'},
			items:[				
				{	width:430,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Resumen de carga</div>'	},
				{	width:430,	colspan:2,border:false,	frame:true,	html:'<div class="formas" align="center">La siguiente PYME qued� afiliada a Cr�dito Electronico con:</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">N�mero SIRAC:</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Nafin Electr�nico:</div>'	}
					
			]
		},
		{
			xtype: 'panel',
			width:430,
			border:true,
			buttonAlign: 'center',
			//bodyStyle: 'padding:4px',
			layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			},
			buttons: [
				{
					xtype: 'button', 
					text: 'Bajar Archivo', 
					iconCls: 'icoXls',
					id:'btnDownfile',
					handler: function(boton, evento) {
								var forma1 = Ext.getDom('formAux');
								forma1.submit();											
							}
				},
				'-',
				{
					xtype: 'button', 
					text: 'Bajar PDF', 
					id:'btnDownPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
								var forma = Ext.getDom('formAux2');
								forma.submit();											
							}
				},
				'-',
				{
					xtype: 'button', 
					text: 'Terminar', 
					id:'btnTerminar',
					handler: function(boton, evento) {
								window.location = "26forma1ext.jsp"											
							}
				}
			]
		}
		
	];
	

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		style: ' margin:0 auto;',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'},
		items: [afiliacionPanel,
					//Tipo persona
				{
					xtype: 'compositefield',
					combineErrors: false,				
					msgTarget: 'side',
					items: [
					{
					  xtype: 'combo',
					  name: 'TipoPersona',
					  id: 'TipoPersona',
					  forceSelection: true,
					  triggerAction : 'all',
					  fieldLabel: 'Tipo de Persona',
					  mode: 'local',
					  value: 'F',
					  valueField: 'clave',
					  store: storeTipoPersona,
					  displayField:'TipoPersona',
					  allowBlank : false,
					  width: 200,
					  	listeners: {
								select: function(combo, record, index) {
								if(record.json[0]=="M")
								{
									ocultaFisica();
								}
								else if(record.json[0]=="F")
								{
									ocultaMoral();
								}
							}
						}	
					}]
				},
				personaFisicaPanel,personaMoralPanel,domicilioPanel,DiversosPanel
				],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls:'aceptar',
				formBind: true,
			handler: function(boton, evento) {
				var cmpForma = Ext.getCmp('forma');
				var params = (cmpForma)?cmpForma.getForm().getValues():{};
				cmpForma.el.mask("Procesando", 'x-mask-loading');
					Ext.Ajax.request({
						url: '26forma1ext.data.jsp',
						params: Ext.apply(params,{
							informacion: 'AltaPersona'
					}),
							callback: procesarAltaPersona
						});
					}
				
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: fp
	});

		Ext.Ajax.request({
            url: '26forma1ext.data.jsp',
            success: procesarTipoUsuario,
            params: {
                informacion: 'initPage'
            } }); 
});