<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.credito.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";
String operacion        	=(request.getParameter("operacion")    != null) ?   request.getParameter("operacion") :"";
JSONObject resultado 	      = new JSONObject();

	if(informacion.equals("Consulta")|| informacion.equals("ArchivoCSV")|| informacion.equals("ArchivoPDF")){
		String Sirac=(request.getParameter("SIRAC")!=null)?request.getParameter("SIRAC"):"";
		String RFC = (request.getParameter("R_F_C")==null)?"":request.getParameter("R_F_C");
		String NombreoRS = (request.getParameter("nomRazon")==null)?"":request.getParameter("nomRazon");
		String Fecha = (request.getParameter("fechaReg")==null)?"":request.getParameter("fechaReg");
		String Fecha2 = (request.getParameter("fechaReg2")==null)?"":request.getParameter("fechaReg2");
		int start=0,limit=0;
		
		ConsultaClientesIfCE paginador = new ConsultaClientesIfCE();
		paginador.setNoDocSIRAC(Sirac);
		paginador.setRFC(RFC);
		paginador.setINoCliente(iNoCliente);
		paginador.setRazonSoc(NombreoRS);
		paginador.setFechaReg(Fecha);
		paginador.setFechaReg2(Fecha2);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
			try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
		if (operacion.equals("Generar")) {	//Nueva consulta
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		if (informacion.equals("Consulta")){
					String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
					resultado = JSONObject.fromObject(consultar);
					infoRegresar=resultado.toString();
		}else 	
		if (informacion.equals("ArchivoCSV")) {
						 try {
							System.out.println(request);
							String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
							JSONObject jsonObj = new JSONObject();
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
							} catch(Throwable e) {
							 throw new AppException("Error al generar el archivo CSV", e);
							}
						}
		else 	if (informacion.equals("ArchivoPDF")) {
						 try {
							System.out.println(request);
							String nomArchivo = cqhelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp,"PDF");
							JSONObject jsonObj = new JSONObject();
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
							} catch(Throwable e) {
							 throw new AppException("Error al generar el archivo PDF", e);
							}
						}
						//getCreatePageCustomFile
	}

%><%=infoRegresar%>