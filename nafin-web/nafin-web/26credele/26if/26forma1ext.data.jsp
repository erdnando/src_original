<%@ page contentType="application/json;charset=UTF-8" import="   java.util.*, 
java.text.*,    
com.netro.exception.*,
netropology.utilerias.*,
com.netro.afiliacion.*, 
net.sf.json.JSONArray,
net.sf.json.JSONObject,
netropology.utilerias.ElementoCatalogo,
com.netro.pdf.ComunesPDF,
com.netro.model.catalogos.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/26credele/26secsession_extjs.jspf"%>
<% 

String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultado="";
	if(informacion.equals("initPage")) {
		System.out.println(strTipoUsuario);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("User",strTipoUsuario );
		jsonObj.put("success",new Boolean(true));
		resultado=jsonObj.toString();
		
	}
	else if(informacion.equals("intermediarioF")) {
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_if");
		cat.setDistinc(false);
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		 resultado = cat.getJSONElementos();	 
	}
	else if(informacion.equals("Pais1")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("IC_PAIS");
		cat.setCampoDescripcion("CD_DESCRIPCION");
		cat.setValoresCondicionNotIn("52", Integer.class);
		resultado = cat.getJSONElementos();	
		 System.out.println(resultado);
	}
	else if(informacion.equals("Pais2")) {		
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_pais");
		cat.setCampoClave("IC_PAIS");
		cat.setCampoDescripcion("CD_DESCRIPCION");
		cat.setValoresCondicionIn("24", Integer.class);
		 resultado = cat.getJSONElementos();	
		 System.out.println(resultado);
	}
	else if(informacion.equals("Estado"))
	{
		CatalogoEstado cat = new CatalogoEstado();
		cat.setCampoClave("ic_estado");
		cat.setCampoDescripcion("CD_NOMBRE"); 
		cat.setClavePais("24");//Mexico clave 24
		cat.setOrden("CD_NOMBRE");
		resultado = cat.getJSONElementos();	
		System.out.println(resultado);
	}
	else if(informacion.equals("Municipio"))
	{
		
		String Edo= (request.getParameter("Est")==null)?"":request.getParameter("Est");
		CatalogoMunicipio cat=new CatalogoMunicipio();
		cat.setClave("IC_MUNICIPIO");
		cat.setDescripcion("CD_NOMBRE");
		cat.setPais("24");
		cat.setEstado(Edo);
		cat.setOrden("CD_NOMBRE");
		List elementos=cat.getListaElementos();
		
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		resultado= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
	}
	else if(informacion.equals("Ciudad"))
	{
	String Edo= (request.getParameter("Est")==null)?"":request.getParameter("Est");
		CatalogoCiudad cat = new CatalogoCiudad();
		cat.setTabla("MG_CIUDADES1");
		cat.setCampoClave("codigo_ciudad");
		cat.setCampoDescripcion("nombre");
		cat.setEstado(Edo);
		cat.setPais("24");
		 resultado = cat.getJSONElementos();	
		 System.out.println(resultado);
	}
	else if(informacion.equals("SectorEco"))
	{
	//query = "select IC_SECTOR_ECON, CD_NOMBRE from comcat_sector_econ where ic_sector_econ not in (1) order by cd_nombre";
	//query = "select IC_SUBSECTOR, CD_NOMBRE from comcat_subsector where ic_sector_econ = " + Sector_economico + " order by cd_nombre";
	CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_sector_econ");
		cat.setCampoClave("IC_SECTOR_ECON");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setValoresCondicionNotIn("1", Integer.class);
		 resultado = cat.getJSONElementos();	
		 System.out.println(resultado);
	}
	else if(informacion.equals("SubSector"))
	{
			String Sec= (request.getParameter("Sector")==null)?"":request.getParameter("Sector");
		CatalogoSubSector cat = new CatalogoSubSector();
			/*query = "select IC_SUBSECTOR, CD_NOMBRE from comcat_subsector 
							* 	where ic_sector_econ = " + Sector_economico + " order by cd_nombre";*/
		cat.setTabla("comcat_subsector");
		cat.setCampoClave("IC_SUBSECTOR");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setSector(Sec);
		resultado = cat.getJSONElementos();	
		System.out.println(resultado);
	}
	else if(informacion.equals("Rama"))
	{
	String Sec= (request.getParameter("Sector")==null)?"":request.getParameter("Sector");
	String SubSec= (request.getParameter("SubSector")==null)?"":request.getParameter("SubSector");
		CatalogoRama cat = new CatalogoRama();
	
		cat.setTabla("comcat_rama");
		cat.setCampoClave("ic_rama");
		cat.setCampoDescripcion("cd_nombre");
		cat.setSector(Sec);
		cat.setSubSector(SubSec);
		resultado = cat.getJSONElementos();	
		System.out.println(resultado);
		
	}
	else if(informacion.equals("Clase"))
	{
		String Sec= (request.getParameter("Sector")==null)?"":request.getParameter("Sector");
		String SubSec= (request.getParameter("SubSector")==null)?"":request.getParameter("SubSector");
		String Rama= (request.getParameter("Rama")==null)?"":request.getParameter("Rama");
		
		CatalogoClase cat = new CatalogoClase();
	
		cat.setTabla("comcat_clase");
		cat.setCampoClave("IC_CLASE");
		cat.setCampoDescripcion("CD_NOMBRE");
		cat.setSector(Sec);
		cat.setSubSector(SubSec);
		cat.setRama(Rama);
		resultado = cat.getJSONElementos();	
		System.out.println(resultado);
	}
	else if(informacion.equals("AltaPersona"))
	{
		Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
		String TipoPersona = (request.getParameter("TipoPersona")==null)?"":request.getParameter("TipoPersona"),
		//NoIf = (request.getParameter("intermediario")==null)?iNoCliente:request.getParameter("intermediario"),
		NoIf = iNoCliente,
		// Persona Física.
		Apellido_paterno = (request.getParameter("Apellido_paterno")==null)?"":request.getParameter("Apellido_paterno").trim(),
		Apellido_materno = (request.getParameter("Apellido_materno")==null)?"":request.getParameter("Apellido_materno").trim(),
		Nombre = (request.getParameter("Nombre")==null)?"":request.getParameter("Nombre").trim(),
		Sexo = (request.getParameter("Sexo")==null)?"":request.getParameter("Sexo"),
		Fecha_de_nacimiento = (request.getParameter("Fecha_de_nacimiento")==null)?"":request.getParameter("Fecha_de_nacimiento").trim(),
		// Persona Moral.
		Razon_Social = (request.getParameter("Razon_Social")==null)?"":request.getParameter("Razon_Social").trim(),
		//Nombre_Comercial = (request.getParameter("Nombre_Comercial")==null)?"":request.getParameter("Nombre_Comercial").trim(),
		// Datos Generales para ambos Tipo de Persona.
		R_F_C = (request.getParameter("R_F_C")==null)?"":request.getParameter("R_F_C").trim(),
		Calle = (request.getParameter("Calle")==null)?"":request.getParameter("Calle").trim(),
		Colonia = (request.getParameter("Colonia")==null)?"":request.getParameter("Colonia").trim(),
		Estado = (request.getParameter("Estado")==null)?"":request.getParameter("Estado"),
		Delegacion_o_municipio = (request.getParameter("Delegacion_o_municipio")==null)?"":request.getParameter("Delegacion_o_municipio"),
		Codigo_postal = (request.getParameter("Codigo_postal")==null)?"":request.getParameter("Codigo_postal").trim(),
		Pais = (request.getParameter("Pais")==null)?"":request.getParameter("Pais"),
		Ventas_netas_totales = (request.getParameter("Ventas_netas_totales")==null)?"":request.getParameter("Ventas_netas_totales").trim(),
		Numero_de_empleados = (request.getParameter("Numero_de_empleados")==null)?"":request.getParameter("Numero_de_empleados").trim(),
		Sector_economico = (request.getParameter("Sector_economico")==null)?"":request.getParameter("Sector_economico"),
		Subsector = (request.getParameter("Subsector")==null)?"":request.getParameter("Subsector"),
		Rama = (request.getParameter("Rama")==null)?"":request.getParameter("Rama"),
		Clase = (request.getParameter("Clase")==null)?"":request.getParameter("Clase"),
		Alta_Troya = (request.getParameter("Alta_Troya")==null)?"N":request.getParameter("Alta_Troya"),
		Telefono 		 = (request.getParameter("Telefono") == null) ? "" : request.getParameter("Telefono"),
		Email 		 = (request.getParameter("Email") == null) ? "" : request.getParameter("Email"),
		curp = (request.getParameter("curp") == null) ? "" : request.getParameter("curp"), // Fodea campos PLD 2010
		fiel = (request.getParameter("fiel") == null) ? "" : request.getParameter("fiel"), //Fodea campos PLD 2010
		ciudad = (request.getParameter("ciudad") == null) ? "" : request.getParameter("ciudad"), //Fodea campos PLD 2010
		paisOrigen= (request.getParameter("paisOrigen") == null) ? "" : request.getParameter("paisOrigen"); // Fodea campos PLD 2010
   	TipoPersona=TipoPersona.equals("Fisica")?"F":"M";

		String sDatosPyme =	R_F_C+"|"+Nombre+"|"+Apellido_paterno+"|"+Apellido_materno+"|"+
                     Sexo+"|"+Razon_Social+"|"+Numero_de_empleados+"|"+Ventas_netas_totales+"|"+
                     Sector_economico+"|"+Subsector+"|"+Rama+"|"+Clase+"|"+Estado+"|"+Delegacion_o_municipio+"|"+
                     Calle+"|"+Colonia+"|"+Codigo_postal+"|"+Alta_Troya+"|"+Email+"|"+Telefono+"|"+
                     curp+"|"+fiel+"|"+paisOrigen+"|"+Pais+"|"+ciudad+"\n";
                      							 
							System.out.println(sDatosPyme);							
	
							Hashtable hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sDatosPyme, TipoPersona, Fecha_de_nacimiento, "", NoIf, iNoUsuario, strTipoUsuario);
							boolean bTodoOK = new Boolean(hAfiliaCE.get("bTodoOK").toString()).booleanValue();
							String ErrorLog  =  hAfiliaCE.get("ErrorLog").toString();
							String sConError  =  hAfiliaCE.get("sConError").toString();
							String sErrorTroya = hAfiliaCE.get("sErrorTroya").toString();
								String sSirac="";					
								String sNE="";
								
							if(bTodoOK)
							{
									StringBuffer contenidoArchivo = new StringBuffer("Resumen de Carga:\nLa siguiente PYME quedo afiliada a Crédito Electrónico con:\nNúmero de SIRAC:,Número de Folio:"+(strTipoUsuario.equals("NAFIN")?",Número Cliente TROYA":"")+"\n"); 
									String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();
									Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
									
									JSONArray jsonArr = new JSONArray();
									
									for(int i=0; i<vDatosPyme.size(); i++) {
										Vector vPyme = (Vector)vDatosPyme.get(i);
										JSONObject jsonObj = new JSONObject();
										sSirac=vPyme.get(0).toString();
										sNE=vPyme.get(12).toString();
										jsonObj.put("SIRAC",vPyme.get(0));
										jsonObj.put("NE",vPyme.get(12));
																				
										if("NAFIN".equals(strTipoUsuario)){	// Solo para Nafin.
											if(sErrorTroya.equals("")){
												jsonObj.put("TROYA",vPyme.get(13));
											} else {	
													jsonObj.put("TROYA",sErrorTroya);													
												}
											}
											jsonArr.add(jsonObj);
											contenidoArchivo.append(vPyme.get(0).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?(sErrorTroya.equals("")?vPyme.get(13).toString():sErrorTroya):"")+"\n");
									}		
									String nombreArchivo = "";
									CreaArchivo archivo = new CreaArchivo();
									if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
									nombreArchivo = archivo.nombre;
								
									
									/////////////////////////////////////////////
									//---------------Hacer PDF-----------------//
									
									String nombreArchivoPDF = Comunes.cadenaAleatoria(16) + ".pdf";
									ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivoPDF);
									pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
									 
									(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(), 
									//(String)session.getAttribute("iNoNafinElectronico"),
									(String)session.getAttribute("sesExterno"),
									(String)session.getAttribute("strNombre"),
									(String)session.getAttribute("strNombreUsuario"),
									(String)session.getAttribute("strLogo"),
									(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
									
									String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
									String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
									String diaActual    = fechaActual.substring(0,2);
									String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
									String anioActual   = fechaActual.substring(6,10);
									String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
									
									pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
									 pdfDoc.addText("Resumen de carga", "formas",ComunesPDF.CENTER);
									 pdfDoc.addText("La siguiente PYME quedó afiliada a Crédito Electrónico con:", "formas",ComunesPDF.CENTER);
									
										if("NAFIN".equals(strTipoUsuario))
										{
											pdfDoc.setTable(3, 100);
											pdfDoc.setCell("Número SIRAC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Nafin Electrónico","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Número Cliente TROYA","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell(sSirac,"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(sNE,"formas",ComunesPDF.CENTER);
										}
										else
										{
											pdfDoc.setTable(2, 100);
											pdfDoc.setCell("Número SIRAC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Nafin Electrónico","celda01",ComunesPDF.CENTER);
											
											pdfDoc.setCell(sSirac,"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(sNE,"formas",ComunesPDF.CENTER);
											
										}
									
									
										pdfDoc.addTable();
										pdfDoc.endDocument();
									//
									resultado="{\"success\": true,\"urlArchivo\":\""+strDirecVirtualTemp + nombreArchivo+"\",\"urlPDF\":\""+strDirecVirtualTemp + nombreArchivoPDF+"\", \"total\": \"" + 
											jsonArr.size() + "\", \"registros\": " +
												jsonArr.toString()+"}";
							}
							else
							{
								
								JSONObject jsonObj = new JSONObject();
								jsonObj.put("success", new Boolean(false));								
								jsonObj.put("msg", ErrorLog + sConError);								
								resultado=jsonObj.toString();							
								
							}
						
	}
	
%>
<%= resultado%>
