<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		java.sql.*,
		com.netro.pdf.ComunesPDF,
		com.netro.credito.*,
		com.netro.descuento.*,
		java.text.*, java.math.*, javax.naming.*,
		com.netro.model.catalogos.*,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";
String operacion        	=(request.getParameter("operacion")    != null) ?   request.getParameter("operacion") :"";
JSONObject resultado 	      = new JSONObject();

if (informacion.equals("CatalogoEstatus")){

		String rs_epo_tipo_cobro_int	=	"1,2,3,4,12";
		CatalogoSimple cat = new CatalogoSimple();	
		cat.setCampoClave("ic_estatus_solic");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_estatus_solic");
		cat.setValoresCondicionIn(rs_epo_tipo_cobro_int, Integer.class);
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	}else if(informacion.equals("Consulta")|| informacion.equals("ArchivoCSV")|| informacion.equals("ArchivoPDF")){
		String acuse	=	(request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
		String numFolio	=	(request.getParameter("numFolio")!=null)?request.getParameter("numFolio"):"";
		String nomPrestamo	=	(request.getParameter("nomPrestamo")!=null)?request.getParameter("nomPrestamo"):"";
		String fechaIF	=	(request.getParameter("fechaIF")!=null)?request.getParameter("fechaIF"):"";
		String fechaIF2	=	(request.getParameter("fechaIF2")!=null)?request.getParameter("fechaIF2"):"";
		String fechaOper	=	(request.getParameter("fechaOper")!=null)?request.getParameter("fechaOper"):"";
		String fechaOper2	=	(request.getParameter("fechaOper2")!=null)?request.getParameter("fechaOper2"):"";
		String estatus	=	(request.getParameter("Hestatus")!=null)?request.getParameter("Hestatus"):"";
		int start=0,limit=0;
		ConsultaOperIfCE paginador = new ConsultaOperIfCE();
		paginador.setINoCliente(iNoCliente);
		paginador.setIcEstatus(estatus);
		paginador.setNumAcuse(acuse);
		paginador.setNumFolio(numFolio);
		paginador.setNumPrest(nomPrestamo);
		paginador.setFechaIF(fechaIF);
		paginador.setFechaIF2(fechaIF2);
		paginador.setFechaOper(fechaOper);
		paginador.setFechaOper2(fechaOper2);
		CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
			try{
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));	
			}catch(Exception e){
					System.out.println("Error en parametros");
			}
		if (operacion.equals("Generar")) {	//Nueva consulta
				cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		if (informacion.equals("Consulta")){
					String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
					resultado = JSONObject.fromObject(consultar);
					infoRegresar=resultado.toString();
		}else 	
		if (informacion.equals("ArchivoCSV")) {
						 try {
							System.out.println(request);
							String nomArchivo = cqhelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
							JSONObject jsonObj = new JSONObject();
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
							} catch(Throwable e) {
							 throw new AppException("Error al generar el archivo CSV", e);
							}
						}
		else 	if (informacion.equals("ArchivoPDF")) {
						 try {
							System.out.println(request);
							String nomArchivo = cqhelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp,"PDF");
							JSONObject jsonObj = new JSONObject();
							jsonObj.put("success", new Boolean(true));
							jsonObj.put("urlArchivo", strDirecVirtualTemp+nomArchivo);
							infoRegresar = jsonObj.toString();
							} catch(Throwable e) {
							 throw new AppException("Error al generar el archivo CSV", e);
							}
						}
	
	
	}else if (informacion.equals("generaOrigen")){
		String claveSolicitud = (request.getParameter("claveSolicitud") == null)?"":request.getParameter("claveSolicitud");
		AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
		String sContenidoArchivo = autorizacionSolicitud.getDatosSolicitudCredito(claveSolicitud);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(false));
		if (!sContenidoArchivo.equals("")) {
			String nombreArchivo="";
			CreaArchivo archivo = new CreaArchivo();
			if(!archivo.make(sContenidoArchivo.toString(), strDirectorioTemp, ".csv")){
				
				}
			else{
				nombreArchivo = archivo.nombre;
				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				}
			
		}
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("tablaAmort")){
		String	numPrestamo = (request.getParameter("numPrestamo")==null)?"":request.getParameter("numPrestamo");
		AccesoDB 			con 			=	null;
		ResultSet			rs				= 	null;
		PreparedStatement 	ps				= null;
		String				condicion		= "";
		String				qrySentencia	= "";
		
		//Variables de uso local
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = "";
		String contenidoArchivo="";
		
		int registros = 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			
			qrySentencia = 
				" SELECT pp.numero_cuota, TO_CHAR (pp.fecha_vence, 'dd/mm/yyyy') AS fecha_vence,"   +
				"        DECODE (pp.tipo_cuota, 'A', 'Capital e Intereses', 'I', 'Intereses', 'R', 'Recortada') AS tipo_cuota, spp.valor"   +
				"   FROM pr_plan_pago pp, pr_saldos_plan_pago spp"   +
				"  WHERE pp.numero_cuota = spp.numero_cuota"   +
				"    AND pp.numero_prestamo = spp.numero_prestamo"   +
				"    AND pp.codigo_empresa = spp.codigo_empresa"   +
				"    AND pp.codigo_agencia = spp.codigo_agencia"   +
				"    AND pp.codigo_sub_aplicacion = spp.codigo_sub_aplicacion"   +
				"    AND pp.numero_prestamo = ?"   +
				"    AND spp.codigo_tipo_saldo = 1"   +
				"  ORDER BY pp.numero_cuota"  ;
				System.out.println(qrySentencia);
				contenidoArchivo += "Número de Prestamo:,"+numPrestamo;
				contenidoArchivo += "\nTabla de Amortización";
				contenidoArchivo += "\nNumero de Amortización,Fecha de Amortización,Tipo de Cuota,Monto de Amortización";
				
				 String nombreArchivoPDF = "";
				nombreArchivoPDF = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp + nombreArchivoPDF);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("México, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
				pdfDoc.setTable(2,100);
				pdfDoc.setCell("Número de Prestamo:", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(numPrestamo, "formas", ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.setTable(1,100);
				pdfDoc.setCell("Tabla de Amortización", "celda01", ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				lEncabezados.add("Numero de Amortización");
				lEncabezados.add("Fecha de Amortización");
				lEncabezados.add("Tipo de Cuota");
				lEncabezados.add("Monto de Amortización");
				
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				List reg=new ArrayList();
				JSONObject jsonObj = new JSONObject();
				try {
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1,Integer.parseInt(numPrestamo));
					rs = ps.executeQuery();
					HashMap	datos;
					
					datos=new HashMap();
					while(rs.next()){
						String rs_numAmort		= rs.getString(1)==null?"":rs.getString(1);
						String rs_fechaAmort	= rs.getString(2)==null?"":rs.getString(2);
						String rs_tipoCuota		= rs.getString(3)==null?"":rs.getString(3);
						String rs_mtoAmort		= rs.getString(4)==null?"":rs.getString(4);
						datos.put("NUM_AMOR",rs_numAmort);
						datos.put("FECHA_AMOR",rs_fechaAmort);
						datos.put("TIPO_CUOTA",rs_tipoCuota);
						datos.put("MONTO_AMOR",rs_mtoAmort);
						reg.add(datos);
						
						
						contenidoArchivo += "\n"+
											rs_numAmort+","+
											rs_fechaAmort+","+
											rs_tipoCuota+","+
											rs_mtoAmort;
					pdfDoc.setCell(rs_numAmort,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_fechaAmort,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_tipoCuota,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(rs_mtoAmort, 2),"formas",ComunesPDF.CENTER);
						registros++;
					}
					rs.close();
					if(ps!=null) ps.close();
					if(registros==0) {
					}
				} catch(Exception e) { }
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
				if(!archivo.make(contenidoArchivo, strDirectorioTemp, ".csv"))
					out.print("<--!Error al generar el archivo-->");
				else
					nombreArchivo = archivo.nombre;
					
					
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("registros", reg);
					jsonObj.put("Totales",reg.size()+"");
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					jsonObj.put("urlArchivoPDF", strDirecVirtualTemp+nombreArchivoPDF);
					infoRegresar=jsonObj.toString();
					
					} catch(Exception e) { 
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("success", new Boolean(false));
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		
	}
%>
<%=infoRegresar%>