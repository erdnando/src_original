Ext.onReady(function() {
//----------Handlers--------------
	var sArchivoErroresJS="";
	var sPymesOKJS="";
	var NoIfJS="";

	var resultProcesoHand =  function(response) {
		if(Ext.getCmp("resultAreaCargaMas"))
			Ext.getCmp("resultAreaCargaMas").destroy();
			if (Ext.util.JSON.decode(response.responseText).success == true) {
				var storeInfo=Ext.util.JSON.decode(response.responseText).registros;
				var informe = new Ext.Window({
					width: 1000,height: 400,
					frame: true,
					title: 'Informe Carga Masiva',
					align: 'center',
					maximizable: false,
					modal: true,
					closeAction: 'destroy',
					resizable: false,
					maximized: false,
					items: [gridDiez/*,
						elemTableInfo*/
					],
					constrain: true}).show();									

					consultaDataGrid.loadData(storeInfo);
				
					var forma3 = Ext.getDom('formAux3');
						forma3.action = Ext.util.JSON.decode(response.responseText).urlArchivoError;
					var forma2 = Ext.getDom('formAux2');
						forma2.action =Ext.util.JSON.decode(response.responseText).urlPDF;
					var forma = Ext.getDom('formAux');
						forma.action =Ext.util.JSON.decode(response.responseText).urlArchivo;
					
					if (!Ext.isEmpty(sArchivoErroresJS)){
						Ext.getCmp('btnDownfileErrores').show();
					}

			}else {
				NE.util.mostrarConnError(response,opts);
			}
	}
	var  procesarSuccessFailureAltaMasiva = function(form, action){

				if(action.result.TodOk == true){
						var storeInfo= action.result.registros;
				
						var informe = new Ext.Window({
							width: 940,height: 400,
							frame: true,
							title: 'Informe Carga Masiva',
							align: 'center',
							maximizable: false,
							modal: true,
							closeAction: 'destroy',
							resizable: false,
							maximized: false,
							items: [
								gridDiez
							],
							constrain: true}).show();									

							consultaDataGrid.loadData(storeInfo);

							var forma = Ext.getDom('formAux');
								forma.action = action.result.urlArchivo;
							var forma2 = Ext.getDom('formAux2');
								forma2.action = action.result.urlPDF;
				}
				else{
					 if(action.result.sError == "" || action.result.sError == " "){
							var informe = new Ext.Window({
							frame: true,
							id:'resultAreaCargaMas',
							width: 520,height: 420,
							title: 'Informe Carga Masiva',
							align: 'center',
							//maximizable: false,
							modal: true,
							closeAction: 'destroy',
							//resizable: false,
							maximized: false,
							items: [
								elementosAreaCargaMasiva
							],
							constrain: true}).show();
							
							Ext.getCmp("sinErr").setValue(action.result.sSinError);	
							Ext.getCmp("conErr").setValue(action.result.sConError);
																
							Ext.getCmp("regCargados").setValue("Total de registros Cargados: "+action.result.totRegSiracBien);	
							Ext.getCmp("regRech").setValue("Total de registros Rechazados: " + action.result.totRegSiracMal);	
							
							var equis = (Ext.getCmp("valorregCargados").setValue( +action.result.totRegSiracBien)).getValue();
							//Ext.MessageBox.alert('Mensaje de p�gina web',equis );
							if(equis == 0)
							{
								//alert(equis);
								var btnProcesar = Ext.getCmp("btnProc");
								btnProcesar.hide();
							}
							sArchivoErroresJS=action.result.sArchivoErrores;
							sPymesOKJS=action.result.sPymesOK;
							NoIfJS=action.result.NoIf;
														
					}else{
					Ext.MessageBox.alert("Carga Masiva Error", 
						action.result.sError
						);

					}
				}
	}

function addRow(SIRAC,NOMBRERAZON,RFC,TIPO_PER,SECTOR,SUBSECTOR,RAMA,CLASE,ESTRATO,FOLIO){
	var newRow=Ext.getCmp('idInforme');

				newRow.add({html: '<div align="center" >'+SIRAC+'</div>'},{html: '<div align="center">'+NOMBRERAZON+'</div>'},
				{html: '<div align="center">'+RFC+'</div>'},{html: '<div align="center">'+TIPO_PER+'</div>'},
				{html: '<div align="center">'+SECTOR+'</div>'},{html: '<div align="center">'+SUBSECTOR+'</div>'},
				{html: '<div align="center">'+RAMA+'</div>'},{html: '<div align="center">'+CLASE+'</div>'},
				{html: '<div align="center">'+ESTRATO+'</div>'},{html: '<div align="center">'+FOLIO+'</div>'});

		newRow.doLayout();
}
//------------Strore------------
var consultaDataGrid = new Ext.data.JsonStore({
		//root : 'registros',
		fields: [		
			{name: 'SIRAC'},
			{name: 'NOMBRERAZON'},
			{name: 'RFC'},			
			{name: 'TIPO_PER'},
			{name: 'SECTOR'},
			{name: 'SUBSECTOR'},
			{name: 'RAMA'},
			{name: 'CLASE'},
			{name: 'ESTRATO'},
			{name: 'FOLIO'}
			
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false
	});
//---------Components------------

var gridDiez= {
		store: consultaDataGrid,
		xtype:'grid',
		id: 'gridDetalle',

		columns: [
			{
				header:'N�mero de  SIRAC',
				dataIndex:'SIRAC', resizable: true,
				align: 'left'
			},{
				header:'Nombre/Razon Social',
				dataIndex: 'NOMBRERAZON', resizable: true,
				width: 200,
				align: 'left'
			}
			,{
				header:'RFC',
				dataIndex: 'RFC', resizable: true,
				width: 200,
				align: 'left'
			}
			,{
				header:'Tipo Persona',
				dataIndex: 'TIPO_PER', resizable: true,
				width: 200,
				align: 'left'
			}
			,{
				header:'Sector Econ�mico',
				dataIndex: 'SECTOR', resizable: true,
				width: 200,
				align: 'left'
			}
			,{
				header:'Subsector',
				dataIndex: 'SUBSECTOR', resizable: true,
				width: 200, 
				align: 'left'
			}
			,{
				header:'Rama',
				dataIndex: 'RAMA', resizable: true,
				width: 200, 
				align: 'left'
			}
			,{
				header:'Clase',
				dataIndex: 'CLASE', resizable: true,
				width: 200, 
				align: 'left'
			}
			,{
				header:'Estrato',
				dataIndex: 'ESTRATO', resizable: true,
				width: 200, 
				align: 'left'
			}
			,{
				header:'N�mero de folio',
				dataIndex: 'FOLIO', resizable: true,
				width: 200,
				align: 'left'
			}
		],
		loadMask: true,
		height: '350',
		frame: true,
		collapsible: true,
		bbar: {
		items: [
			{
			xtype: 'button', 
			text: 'Descargar Archivo con Errores', 
			iconCls: 'icoXls',
			hidden:true,
			id:'btnDownfileErrores',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux3');
						forma.submit();											
					}
			},{
			xtype: 'button', 
			text: 'Bajar Archivo',
				iconCls: 'icoXls',
			id:'btnDownfile',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.submit();											
					}
			},{
			xtype: 'button', 
			text: 'Bajar PDF', 
			id:'btnDownPDF',
			iconCls: 'icoPdf',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux2');
						forma.submit();											
					}
			},{
			xtype: 'button', 
			text: 'Salir', 
			id:'btnSal',
			handler: function(boton, evento) {
						window.location = "26forma1bExt.jsp"												
					}
			}
				
		]
		}
	};
	
var elementosAreaCargaMasiva=[
//Resumen del proceso:
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
		items: [
					{
					xtype: 'displayfield',
					value: 'Pymes sin Errores:',
					width: 250
					
					},{
					xtype: 'displayfield',
					value: 'Pymes con Errores'
					}
		]},
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
		items: [
				{xtype: 'textarea',
				name: 'sinErr',
				id: 'sinErr',
				readOnly:true,
				anchor:'100%',
				width: 250,
				height: 300,
				value:''
			},
				{xtype: 'textarea',
				name: 'conErr',
				readOnly:true,
				id: 'conErr',
				width: 250,
				height: 300,
				//allowBlank: false,
				anchor:'100%',
				value:''
			}
		]
		},
		/*
		{
		xtype: 'compositefield',
		
		combineErrors: false,
		msgTarget: 'side',
		items: [
					{
					xtype: 'displayfield',
					value: 'Total de Registros Cargados:',
					width: 250
					},{
					xtype: 'displayfield',
					value: 'Total de Registros Rechazados:'				
					}
		]},*/
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
		items: [
					{
					xtype: 'displayfield',
					value: 'Total de Registros Cargados:',
					width: 250,
					id:'valorregCargados',
					hidden:true
					},
					{
					xtype: 'displayfield',
					value: 'Total de Registros Cargados:',
					width: 250,
					id:'regCargados'
					
					},{
					xtype: 'displayfield',
					value: 'Total de Registros Rechazados:',
					id:'regRech'
					
					}
		]},NE.util.getEspaciador(7),
		{
		//xtype: 'compositefield',
			xtype:'form', 
			width:'auto',
			border: false,
			buttonAlign: 'center',
			layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			},
			items: [
				{
					xtype: 'button',	text: 'Procesar',	id:'btnProc',	iconCls:'icoAceptar',
					hidden: false,
					handler: function(boton, evento) {
								//alert("procesando...");		
								var forma = Ext.getCmp("resultAreaCargaMas");
								forma.el.mask("Procesando", 'x-mask-loading');
								Ext.Ajax.request({  
									url: '26forma1bExt.data.jsp',
									success: resultProcesoHand,
									params: {
										informacion: 'Proceso',
										sArchivoErrores:sArchivoErroresJS,
										sPymesOK:sPymesOKJS,
										NoIf:NoIfJS
									} });
							}
				}
		]}
		
];
//Total de Registros Cargados:
//Info correcto
	var fpArchivoMas = {
		xtype:'form',
		id: 'formaEnviarArch',
		width: 600,
		fileUpload: true,
		style: ' margin:0 auto;',
		title:'Carga Masiva',
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		
		items: [
			{
				xtype: 'fileuploadfield',
				id: 'txtarchivo',
				name:'txtarchivo',
				width: 290,
				allowBlank: false,
				emptyText: 'Seleccione Archivo',
				fieldLabel: 'Archivo',
				buttonText: '',
				buttonCfg: {iconCls: 'upload-icon'},
				listeners: {
								'fileselected': function(fb, v){											
								var inicio=v.lastIndexOf('\\');
								var tope=v.indexOf('.')>0?v.indexOf('.'):v.length;
							 }
				}
			},{
				xtype: 'hidden',
				name: 'TipoPYME',
				id: 'TipoPYMEID',
				value:TPym
			},{
				xtype: 'hidden',
				name: 'NoIf',
				value:NIf,
				id: 'NoIfID'
			}
		],
		
		buttons: [	
			{
				text: 'Enviar',	id: 'btnEnviar',	formBind: true,	iconCls: 'icoAceptar',
				handler: function(boton, evento) {
				var forma = Ext.getCmp('formaEnviarArch');
						
					forma.getForm().submit({
											url: '26forma1bExt.data.jsp',
											success:procesarSuccessFailureAltaMasiva,
											failure: NE.util.mostrarSubmitError
										});
					//fin codigo submit
				} //fin handler
			},
			{
				text: 'Cancelar', iconCls:'icoRechazar',
				handler: function() {
					window.location = '26forma1bExt.jsp'
				}
			}

		],monitorValid: true
	};

//Informe correcto
var elemTableInfo = [
		{
			xtype: 'panel',id:'idInforme',layout:'table',width:1000,border:true,layoutConfig:{ columns:10},
			defaults: {frame:false, border:true,width:100, height:70,bodyStyle:'padding:8px'},
			items:[				
				{	width:1000,height: 40,colspan:10,	border:false,	frame:true,	html:'<div align="center">PYMES Afiliadas a Cr�dito Electr�nico</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">N�mero de  SIRAC</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Nombre/Razon Social</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">RFC</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Tipo Persona</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Sector Econ�mico</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Subsector</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Rama</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Clase</div>'	},
				{	border:false,frame:true,html:'<div class="formas" align="center">Estrato</div>'},
				{	border:false,frame:true,html:'<div class="formas" align="center">N�mero de folio</div>'}	
			]
		},
		{
			xtype: 'button', 
			text: 'Descargar Archivo con Errores', 
			iconCls: 'icoXls',
			hidden:true,
			id:'btnDownfileErrores',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux3');
						forma.submit();											
					}
		},
		{
			xtype: 'button', 
			text: 'Bajar Archivo',
				iconCls: 'icoXls',
			id:'btnDownfile',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.submit();											
					}
		},
		{
			xtype: 'button', 
			text: 'Bajar PDF', 
			id:'btnDownPDF',
			iconCls: 'icoPdf',
			handler: function(boton, evento) {
						var forma = Ext.getDom('formAux2');
						forma.submit();											
					}
		},
		{
			xtype: 'button', 
			text: 'Salir', 
			id:'btnSal',
			handler: function(boton, evento) {
						window.location = '26forma1bExt.jsp'												
					}
		}
		
	];
	//Contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: fpArchivoMas
	});
	
		
});