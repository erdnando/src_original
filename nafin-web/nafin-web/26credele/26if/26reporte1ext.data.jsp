<%@ page contentType="application/json;charset=UTF-8" import="   java.util.*, 
java.text.*,    
com.netro.exception.*, 
com.netro.afiliacion.*,  
	com.netro.credito.*,
net.sf.json.JSONArray,  
net.sf.json.JSONObject,  
netropology.utilerias.ElementoCatalogo,  
com.netro.pdf.ComunesPDF, 
com.netro.model.catalogos.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/26credele/26secsession_extjs.jspf"%>
<% 

String informacion	= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String facultad		  = (request.getAttribute("facultad")==null)?request.getParameter("facultad"):request.getAttribute("facultad").toString();
String producto		  = (request.getAttribute("producto")==null)?request.getParameter("producto"):request.getAttribute("producto").toString();
String sFacultad	  = (request.getAttribute("cveFacultad")==null)?request.getParameter("cveFacultad"):request.getAttribute("cveFacultad").toString();
String estatus      = (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
ResultSet rs        = null;
String query = "";	
//int i = 0, reg = 0;
AccesoDB con = new AccesoDB();
String resultado="";

if(informacion.equals("Consulta")){ 
  
  RepOperEstatus paginador = new RepOperEstatus();
  paginador.setEstatus(estatus);
  paginador.setStrTipoUsuario(strTipoUsuario);
  paginador.setIc_if(iNoCliente);
  CQueryHelperRegExtJS queryHelper	= new CQueryHelperRegExtJS(paginador); 
  
  Registros registros = queryHelper.doSearch();
  HashMap datos;
  List reg=new ArrayList();
  while (registros.next()) {
    datos = new HashMap();
    datos.put("INTERMEDIARIO_F",registros.getString("cg_razon_social")==null?"":registros.getString("cg_razon_social"));
    datos.put("CLIENTE",registros.getString("nombrePyme"));
    datos.put("NUMERO_SIRAC",registros.getString("claveSirac"));
    datos.put("NUMERO_DOCTO",registros.getString("numDocto"));
    datos.put("NUMERO_SOL",registros.getString("numSolicitud"));
    datos.put("FECHA_EMISION",registros.getString("fechaETC"));
    datos.put("FECHA_VENC_DESC",registros.getString("fechaDscto"));
    datos.put("MONEDA",registros.getString("nombreMoneda"));
    datos.put("TIPO_CREDITO",registros.getString("descTipoCred"));
    datos.put("MONTO_DOCTO",registros.getString("importeDocto"));
    datos.put("MONTO_DESCONTAR",registros.getString("importeDscto"));
    datos.put("FOLIO",registros.getString("Folio"));
    datos.put("NUMPRESTAMO",registros.getString("numPrestamo"));
    reg.add(datos);
    //i++; reg++;
  }
  System.err.println("strTipoUsuario " + strTipoUsuario);
  con.cierraStatement();
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
  jsonObj.put("registros", reg);
  resultado = jsonObj.toString();
}

if(informacion.equals("PDF")){
 int i = 0;
 int reg = 0;
  try {
   // String estatus = (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
    String hint = "";
    con.conexionDB();
  
    //>>CREA REPORTE PDF
    CreaArchivo archivo = new CreaArchivo();
    String nombreArchivo = archivo.nombreArchivo()+".pdf";
    ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
    //<<CREA REPORTE PDF
  
    String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
    String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    String diaActual    = fechaActual.substring(0,2);
    String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
    String anioActual   = fechaActual.substring(6,10);
    String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                    (session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
                     (String)session.getAttribute("sesExterno"),
                                    (String) session.getAttribute("strNombre"),
                                       (String) session.getAttribute("strNombreUsuario"),
                     (String)session.getAttribute("strLogo"),strDirectorioPublicacion);
  
    pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
    int columnas=11;
    if("NAFIN".equals(strTipoUsuario)){
    columnas++;
    }
    SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
    pdfDoc.addText("\n\nUsuario: "+strNombreUsuario,"formas",ComunesPDF.LEFT);
    pdfDoc.addText("\nFecha: "+formatoHora2.format(new java.util.Date()),"formas",ComunesPDF.LEFT);  
    
    if("T".equals(estatus)||"2".equals(estatus)){
      pdfDoc.setTable(4,100,new float[]{20f,20f,.2f,59.8f});
      pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("En Proceso","formas",ComunesPDF.CENTER);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,1);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);
      pdfDoc.addTable();
    
    
    
      pdfDoc.setTable(columnas);
    
      if("NAFIN".equals(strTipoUsuario)){
      pdfDoc.setCell("Intermediario Financiero","celda01rep",ComunesPDF.CENTER);
      }
      pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Sirac","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Solicitud","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emisión","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Tipo de Crédito","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
      
      i = 0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp ci p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "SP.ig_numero_docto as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,com_interfase CI "+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_solic_portal= CI.ic_solic_portal "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 2 "+
          //" and trunc(SP.df_carga) = trunc(SYSDATE) "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) "+
          " and CI.ic_error_proceso is NULL ";
    
      if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
        }
      rs = con.queryDB(query);
        while (rs.next()) {
        if("NAFIN".equals(strTipoUsuario)){
         pdfDoc.setCell((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"),"formasrep",ComunesPDF.CENTER);
        }
        pdfDoc.setCell((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numDocto")==null)?"":rs.getString("numDocto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDocto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDscto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("Folio")==null)?"":rs.getString("Folio"),"formasrep",ComunesPDF.CENTER);
        i++;
        reg++;
      } //while
      con.cierraStatement();
      if(i == 0) {  
        pdfDoc.setCell("No se encontró ningún registro","celda01rep",ComunesPDF.CENTER,columnas,1,1);
      } else {
        pdfDoc.setCell("Total en Proceso","formasrep",ComunesPDF.CENTER,2,1,1);
        pdfDoc.setCell(""+i,"formasrep",ComunesPDF.CENTER,columnas-2,1,1);
      }
      pdfDoc.addTable(); 
      }
     if("T".equals(estatus)||"D".equals(estatus)){
    
      pdfDoc.setTable(4,100,new float[]{20f,20f,.2f,59.8f});
      pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Detenidas \n Contacte a su ejecutivo de cuenta","formas",ComunesPDF.CENTER);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,1);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);
      pdfDoc.addTable();
    
      pdfDoc.setTable(columnas);
    
      if("NAFIN".equals(strTipoUsuario)){
      pdfDoc.setCell("Intermediario Financiero","celda01rep",ComunesPDF.CENTER);
      }
      pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Sirac","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Solicitud","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emisión","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Tipo de Crédito","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
      i = 0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp ci p tc m cif)*/";
      query = "select " + hint +
			"P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
			"SP.ig_numero_docto as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
			"TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
			"TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
			"SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
			" ,SP.ig_numero_prestamo as numPrestamo,  CIF.cg_razon_social as cg_razon_social"+
			" , '26credele\26if\26reporte1.jsp' as pantalla"+
			" from com_solic_portal SP"+
			" ,com_interfase CI "+
			" ,comcat_pyme P"+
			" , comcat_tipo_credito TC"+
			" , comcat_moneda M "+
			" ,comcat_if CIF "+
			" where SP.ic_tipo_credito = TC.ic_tipo_credito "+
			" and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
			" and SP.ic_solic_portal= CI.ic_solic_portal "+
			" and SP.ic_if= CIF.ic_if "+
			" and SP.ic_moneda = M.ic_moneda(+) "+
			" and SP.ic_estatus_solic = 2 "+
			" and CI.ic_error_proceso is NOT NULL ";
    
       if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
        }
      rs = con.queryDB(query);
        while (rs.next()) {
        if("NAFIN".equals(strTipoUsuario)){
          pdfDoc.setCell((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"),"formasrep",ComunesPDF.CENTER);
        }
        pdfDoc.setCell((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numDocto")==null)?"":rs.getString("numDocto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDocto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDscto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("Folio")==null)?"":rs.getString("Folio"),"formasrep",ComunesPDF.CENTER);
        i++;
        reg++;
      } //while
      con.cierraStatement();
      if(i == 0) {
        pdfDoc.setCell("No se encontró ningún registro","celda01rep",ComunesPDF.CENTER,columnas,1,1);
      } else {
        pdfDoc.setCell("Total en Proceso","formasrep",ComunesPDF.CENTER,2,1,1);
        pdfDoc.setCell(""+i,"formasrep",ComunesPDF.CENTER,columnas-2,1,1);
      }
      pdfDoc.addTable();
      }
    
    if("T".equals(estatus)||"3".equals(estatus)){
    
      pdfDoc.setTable(4,100,new float[]{20f,20f,.2f,59.8f});
      pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Operadas","formas",ComunesPDF.CENTER);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,1);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);
      pdfDoc.addTable();
    
        columnas=12;
      if("NAFIN".equals(strTipoUsuario)){
      columnas++;
      }
    
      pdfDoc.setTable(columnas);
    
        if("NAFIN".equals(strTipoUsuario)){
      pdfDoc.setCell("Intermediario Financiero","celda01rep",ComunesPDF.CENTER);
        }
      pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Sirac","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Solicitud","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emisión","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Tipo de Crédito","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Num. Préstamo","celda01rep",ComunesPDF.CENTER);
      i=0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_05_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "SP.ig_numero_docto as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo ,CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_tipo_credito TC"+
          " ,comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 3 "+
          " AND sp.df_operacion >= trunc(SYSDATE) "+
          " AND sp.df_operacion < trunc(SYSDATE+1) ";
      if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
      }
      rs = con.queryDB(query);
      while (rs.next()) {
         if("NAFIN".equals(strTipoUsuario)){
          pdfDoc.setCell((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"),"formasrep",ComunesPDF.CENTER);
        }
        pdfDoc.setCell((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numDocto")==null)?"":rs.getString("numDocto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDocto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDscto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("Folio")==null)?"":rs.getString("Folio"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numPrestamo")==null)?"":rs.getString("numPrestamo"),"formasrep",ComunesPDF.CENTER);
        i++;
        reg++;
      }// while
      con.cierraStatement();
      if(i == 0) {
        pdfDoc.setCell("No se encontró ningún registro","celda01rep",ComunesPDF.CENTER,columnas,1,1);
      } else {
        pdfDoc.setCell("Total Operadas","formasrep",ComunesPDF.CENTER,2,1,1);
        pdfDoc.setCell(""+i,"formasrep",ComunesPDF.CENTER,columnas-2,1,1);
      }
      pdfDoc.addTable();
      }
      if("T".equals(estatus)||"4".equals(estatus)){
    
      pdfDoc.setTable(4,100,new float[]{20f,20f,.2f,59.8f});
      pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Rechazadas","formas",ComunesPDF.CENTER);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,1);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);
      pdfDoc.addTable();
    
        columnas=11;
      if("NAFIN".equals(strTipoUsuario)){
      columnas++;
      }
    
      pdfDoc.setTable(columnas);
    
      if("NAFIN".equals(strTipoUsuario)){
      pdfDoc.setCell("Intermediario Financiero","celda01rep",ComunesPDF.CENTER);
      }
    
      pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Sirac","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Solicitud","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emisión","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Tipo de Crédito","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
      
      i=0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          " SP.ig_numero_docto as numDocto, SP.ic_solic_esp as numSolicitud , TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          " TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          " TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          " SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          ", SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_if CIF "+
          " ,comcat_tipo_credito TC "+
          " ,comcat_moneda M "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_estatus_solic = 4 "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) ";
    
      if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
        }
    
      rs = con.queryDB(query);
      while (rs.next()) {
         if("NAFIN".equals(strTipoUsuario)){
          pdfDoc.setCell((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"),"formasrep",ComunesPDF.CENTER);
        }
        pdfDoc.setCell((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numDocto")==null)?"":rs.getString("numDocto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDocto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDscto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("Folio")==null)?"":rs.getString("Folio"),"formasrep",ComunesPDF.CENTER);
        i++;
        reg++;
      }// while
      con.cierraStatement();
    
      if(i == 0) {
        pdfDoc.setCell("No se encontró ningún registro","celda01rep",ComunesPDF.CENTER,columnas,1,1);
      } else {
        pdfDoc.setCell("Total Rechazadas","formasrep",ComunesPDF.CENTER,2,1,1);
        pdfDoc.setCell(""+i,"formasrep",ComunesPDF.CENTER,columnas-2,1,1);
      }
      pdfDoc.addTable();
    
      }
      if("T".equals(estatus)||"12".equals(estatus)){
      pdfDoc.setTable(4,100,new float[]{20f,20f,.2f,59.8f});
      pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Bloqueadas","formas",ComunesPDF.CENTER);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,1);
      pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);
      pdfDoc.addTable();
    
      pdfDoc.setTable(columnas);
    
      if("NAFIN".equals(strTipoUsuario)){
      pdfDoc.setCell("Intermediario Financiero","celda01rep",ComunesPDF.CENTER);
      }
      pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Sirac","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Número de Solicitud","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Emisión","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Tipo de Crédito","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
      pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
    
      i=0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "SP.ig_numero_docto as numDocto, SP.ic_solic_esp as numSolicitud,  TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 12 ";
          //" and trunc(SP.df_carga) = trunc(SYSDATE) "+
      if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
        }
      rs = con.queryDB(query);
        while (rs.next()) {
         if("NAFIN".equals(strTipoUsuario)){
         pdfDoc.setCell((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"),"formasrep",ComunesPDF.CENTER);
        }
        pdfDoc.setCell((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numDocto")==null)?"":rs.getString("numDocto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred"),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDocto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell("$"+Comunes.formatoDecimal(rs.getString("importeDscto"),2,false),"formasrep",ComunesPDF.CENTER);
        pdfDoc.setCell((rs.getString("Folio")==null)?"":rs.getString("Folio"),"formasrep",ComunesPDF.CENTER);
        i++;
        reg++;
      } //while
      con.cierraStatement();
      if(i == 0) {
        pdfDoc.setCell("No se encontró ningún registro","celda01rep",ComunesPDF.CENTER,columnas,1,1);
      } else {
        pdfDoc.setCell("Total Bloqueadas","formasrep",ComunesPDF.CENTER,2,1,1);
        pdfDoc.setCell(""+i,"formasrep",ComunesPDF.CENTER,columnas-2,1,1);
      }
      pdfDoc.addTable();
      }
      pdfDoc.endDocument();
      //response.sendRedirect(strDirecVirtualTemp+nombreArchivo);
      
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
      resultado = jsonObj.toString();
      con.cierraStatement();
    } catch(Exception e) { out.println(e); }
    finally {
      if (con.hayConexionAbierta()) con.cierraConexionDB();
    }
} 



else if(informacion.equals("XLS")){  
  int i = 0, reg = 0;
  try {
    StringBuffer	contenidoArchivo	= new StringBuffer("");
    String 			nombreArchivo		= "";
    con.conexionDB();
    String hint = "";
    
    if("T".equals(estatus)||"2".equals(estatus)){
  
    contenidoArchivo.append("Estatus,En Proceso\n");
      if("NAFIN".equals(strTipoUsuario)){
    contenidoArchivo.append("Intermediario Financiero,");
    }
    contenidoArchivo.append(
      "Nombre de Cliente,Número de Sirac,Número de Documento,Número de Solicitud,Fecha de Emisión,Fecha de Vencimiento del Descuento,"+
      "Moneda,Tipo de Crédito,Monto del Documento,Monto a Descontar,Folio\n"
    );
  
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp ci p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,com_interfase CI "+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_solic_portal= CI.ic_solic_portal "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 2 "+
          //" and trunc(SP.df_carga) = trunc(SYSDATE) "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) "+
          " and CI.ic_error_proceso is NULL ";
      if("IF".equals(strTipoUsuario)){	
        query = query+" and SP.ic_if = " + iNoCliente;
      }	
    rs = con.queryDB(query);
    while (rs.next()) {	
	     if("NAFIN".equals(strTipoUsuario)){
       contenidoArchivo.append((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social").replace(',',' ')+",");
       }
      contenidoArchivo.append(""+
        ((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme").replace(',',' '))+
        ","+ ((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac").replace(',',' '))+
        ","+ ((rs.getString("numDocto")==null)?"":rs.getString("numDocto").replace(',',' '))+
        ","+ ((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud").replace(',',' '))+
        ","+ ((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC").replace(',',' '))+
        ","+ ((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto").replace(',',' '))+
        ","+ ((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").replace(',',' '))+
        ","+ ((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred").replace(',',' '))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDocto"),2,false))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDscto"),2,false))+
        ","+ ((rs.getString("Folio")==null)?"":rs.getString("Folio").replace(',',' '))+"\n"
      );
      i++;
      reg++;
    } //while
    con.cierraStatement();
    if(i == 0) {
      contenidoArchivo.append("No se encontró ningún registro\n");
    } else { 		
      contenidoArchivo.append("Total en proceso,"+i+"\n"); 
    }
}

if("T".equals(estatus)||"D".equals(estatus)){

	contenidoArchivo.append("Estatus,Detenidas Contacte a su ejecutivo de cuenta\n");
	if("NAFIN".equals(strTipoUsuario)){
	contenidoArchivo.append("Intermediario Financiero,");
	}
	contenidoArchivo.append(
		"Nombre de Cliente,Número de Sirac,Número de Documento,Número de Solicitud,Fecha de Emisión,Fecha de Vencimiento del Descuento,"+
		"Moneda,Tipo de Crédito,Monto del Documento,Monto a Descontar,Folio\n"
	);

      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp ci p tc m cif)*/";

      query = "select " + hint +
			"P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
			"TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
			"TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
			"TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
			"SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
			" ,SP.ig_numero_prestamo as numPrestamo,  CIF.cg_razon_social as cg_razon_social"+
			" , '26credele\26if\26reporte1.jsp' as pantalla"+
			" from com_solic_portal SP"+
			" ,com_interfase CI "+
			" ,comcat_pyme P"+
			" , comcat_tipo_credito TC"+
			" , comcat_moneda M "+
			" ,comcat_if CIF "+
			" where SP.ic_tipo_credito = TC.ic_tipo_credito "+
			" and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
			" and SP.ic_solic_portal= CI.ic_solic_portal "+
			" and SP.ic_if= CIF.ic_if "+
			" and SP.ic_moneda = M.ic_moneda(+) "+
			" and SP.ic_estatus_solic = 2 "+
			//" and trunc(SP.df_carga) = trunc(SYSDATE) "+
			" and CI.ic_error_proceso is NOT NULL ";
		if("IF".equals(strTipoUsuario)){	
			query = query+" and SP.ic_if = " + iNoCliente;
		}	
    rs = con.queryDB(query);
      while (rs.next()) {	
         if("NAFIN".equals(strTipoUsuario)){
       contenidoArchivo.append((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social").replace(',',' ')+",");
       } 
      contenidoArchivo.append(""+
        ((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme").replace(',',' '))+
        ","+ ((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac").replace(',',' '))+
        ","+ ((rs.getString("numDocto")==null)?"":rs.getString("numDocto").replace(',',' '))+
        ","+ ((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud").replace(',',' '))+
        ","+ ((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC").replace(',',' '))+
        ","+ ((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto").replace(',',' '))+
        ","+ ((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").replace(',',' '))+
        ","+ ((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred").replace(',',' '))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDocto"),2,false))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDscto"),2,false))+
        ","+ ((rs.getString("Folio")==null)?"":rs.getString("Folio").replace(',',' '))+"\n"
      );
      i++;
      reg++;
    } //while
    con.cierraStatement();
    if(i == 0) {
      contenidoArchivo.append("No se encontró ningún registro\n");
    } else { 		
      contenidoArchivo.append("Total en proceso,"+i+"\n"); 
    }
  }//fin Estatus Detenidas
  
  if("T".equals(estatus)||"3".equals(estatus)){
    contenidoArchivo.append("Estatus,Operadas\n");
    if("NAFIN".equals(strTipoUsuario)){
    contenidoArchivo.append("Intermediario Financiero,");
    }
    contenidoArchivo.append(
      "Nombre de Cliente,Número de Sirac,Número de Documento,Numero de Solicitud, Fecha de Emisión,Fecha de Vencimiento del Descuento,"+
      "Moneda,Tipo de Crédito,Monto del Documento,Monto a Descontar,Folio,Número de Prestamo\n"
    );
    i=0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_05_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo ,CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_tipo_credito TC"+
          " ,comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 3 "+
          " AND sp.df_operacion >= trunc(SYSDATE) "+
          " AND sp.df_operacion < trunc(SYSDATE+1) ";
      if("IF".equals(strTipoUsuario)){
          query = query+" and SP.ic_if = " + iNoCliente;
      }
    rs = con.queryDB(query);
    while (rs.next()) {
         if("NAFIN".equals(strTipoUsuario)){
       contenidoArchivo.append((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social").replace(',',' ')+",");
       }
      contenidoArchivo.append(
        ((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme").replace(',',' '))+
        ","+ ((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac").replace(',',' '))+
        ","+ ((rs.getString("numDocto")==null)?"":rs.getString("numDocto").replace(',',' '))+
        ","+ ((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud").replace(',',' '))+
        ","+ ((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC").replace(',',' '))+
        ","+ ((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto").replace(',',' '))+
        ","+ ((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").replace(',',' '))+
        ","+ ((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred").replace(',',' '))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDocto"),2,false))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDscto"),2,false))+
        ","+ ((rs.getString("Folio")==null)?"":rs.getString("Folio").replace(',',' '))+
        ","+ ((rs.getString("numPrestamo")==null)?"":rs.getString("numPrestamo").replace(',',' '))+"\n"
      );		 
      i++;
      reg++;
    }// while
    con.cierraStatement();
    if(i == 0) { 
      contenidoArchivo.append("No se encontró ningún registro\n");
    } else {
      contenidoArchivo.append("Total Operadas,"+i+"\n"); 
    } 
    }
    if("T".equals(estatus)||"4".equals(estatus)){
      contenidoArchivo.append("Estatus,Rechazadas\n");
    if("NAFIN".equals(strTipoUsuario)){
    contenidoArchivo.append("Intermediario Financiero,");
    }
    contenidoArchivo.append(
      "Nombre de Cliente,Número de Sirac,Número de Documento,Numero de Solicitud, Fecha de Emisión,Fecha de Vencimiento del Descuento,"+
      "Moneda,Tipo de Crédito,Monto del Documento,Monto a Descontar,Folio\n"
    );  
    i=0;
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          " TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud , TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          " TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          " TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          " SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          ", SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_if CIF "+
          " ,comcat_tipo_credito TC "+
          " ,comcat_moneda M "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_estatus_solic = 4 "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) ";
      if("IF".equals(strTipoUsuario)){	
        query = query+" and SP.ic_if = " + iNoCliente;
      }		
        
    rs = con.queryDB(query);
    while (rs.next()) { 
          if("NAFIN".equals(strTipoUsuario)){
       contenidoArchivo.append((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social").replace(',',' ')+",");
       }
      contenidoArchivo.append(
        ((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme").replace(',',' '))+
        ","+ ((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac").replace(',',' '))+
        ","+ ((rs.getString("numDocto")==null)?"":rs.getString("numDocto").replace(',',' '))+
        ","+ ((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud").replace(',',' '))+
        ","+ ((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC").replace(',',' '))+
        ","+ ((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto").replace(',',' '))+
        ","+ ((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").replace(',',' '))+
        ","+ ((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred").replace(',',' '))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDocto"),2,false))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDscto"),2,false))+
        ","+ ((rs.getString("Folio")==null)?"":rs.getString("Folio").replace(',',' '))+
        ","+ ((rs.getString("numPrestamo")==null)?"":rs.getString("numPrestamo").replace(',',' '))+"\n"
      );	
      i++;
      reg++;
    }// while
    con.cierraStatement();
    if(i == 0) { 
      contenidoArchivo.append("No se encontró ningún registro\n");
    } else {
      contenidoArchivo.append("Total Rechazadas,"+i+"\n"); 
    } 
    } 
  if("T".equals(estatus)||"12".equals(estatus)){
      i=0;
    contenidoArchivo.append("Estatus,Bloqueadas\n");
    if("NAFIN".equals(strTipoUsuario)){
    contenidoArchivo.append("Intermediario Financiero,");
    }
    contenidoArchivo.append(
      "Nombre de Cliente,Número de Sirac,Número de Documento,Numero de Solicitud, Fecha de Emisión,Fecha de Vencimiento del Descuento,"+
      "Moneda,Tipo de Crédito,Monto del Documento,Monto a Descontar,Folio\n"
    );
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud,  TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 12 ";
      if("IF".equals(strTipoUsuario)){	
        query = query+" and SP.ic_if = " + iNoCliente;
      }		
    rs = con.queryDB(query);
      while (rs.next()) {	
        if("NAFIN".equals(strTipoUsuario)){
       contenidoArchivo.append((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social").replace(',',' ')+",");
       }
      contenidoArchivo.append(""+
        ((rs.getString("nombrePyme")==null)?"":rs.getString("nombrePyme").replace(',',' '))+
        ","+ ((rs.getString("claveSirac")==null)?"":rs.getString("claveSirac").replace(',',' '))+
        ","+ ((rs.getString("numDocto")==null)?"":rs.getString("numDocto").replace(',',' '))+
        ","+ ((rs.getString("numSolicitud")==null)?"":rs.getString("numSolicitud").replace(',',' '))+
        ","+ ((rs.getString("fechaETC")==null)?"":rs.getString("fechaETC").replace(',',' '))+
        ","+ ((rs.getString("fechaDscto")==null)?"":rs.getString("fechaDscto").replace(',',' '))+
        ","+ ((rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").replace(',',' '))+
        ","+ ((rs.getString("descTipoCred")==null)?"":rs.getString("descTipoCred").replace(',',' '))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDocto"),2,false))+
        ","+ (Comunes.formatoDecimal(rs.getString("importeDscto"),2,false))+
        ","+ ((rs.getString("Folio")==null)?"":rs.getString("Folio").replace(',',' '))+"\n"
      );
      i++;
      reg++;
    } //while
    con.cierraStatement();
    if(i == 0) {
      contenidoArchivo.append("No se encontró ningún registro\n");
    } else { 		
      contenidoArchivo.append("Total bloqueadas,"+i+"\n"); 
    }
  }

      CreaArchivo archivo = new CreaArchivo();
      if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")){
            out.println("Error en la generación del archivo");
      }
      else{
         nombreArchivo = archivo.nombre;
      }	  
      
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
    resultado = jsonObj.toString();
  }catch(Exception e) { out.println(e); }
  finally {
    if (con.hayConexionAbierta()) con.cierraConexionDB();
  }
}
%>
<%= resultado%>
