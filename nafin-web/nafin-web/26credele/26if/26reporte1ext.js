Ext.onReady(function(){  
		
	var strTipoUsuario = Ext.getDom('strTipoUsuario').value;
	
  var procesarGenerarPDF =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
         Ext.getCmp('btnPDF').setIconClass('icoPdf');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
  var procesarGenerarXLS =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
         Ext.getCmp('btnXLS').setIconClass('icoXls');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
/******************************************************************************/
  var fechaHoy = new Date();
  var horaMomento = 0, meridiano = 'AM';
  horaMomento = Number(fechaHoy.getHours());
  meridiano = Number(fechaHoy.getHours())==12?'PM':'AM';
  
  if(Number(fechaHoy.getHours())>12){
    horaMomento = Number(fechaHoy.getHours())==24?00:Number(fechaHoy.getHours())-12;    
  }
  horaMomento+=':'+fechaHoy.getMinutes()+':'+fechaHoy.getSeconds()+' '+meridiano;
/******************************************************************************/

  var procesarConsultaDataProceso = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridProceso');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
						
				gridProceso.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
				Ext.getCmp('footerProceso').setValue('Total Proceso: ' + store.getTotalCount());
				el.unmask();
			  if(!Ext.getCmp('btnPDF').isVisible()){
				 Ext.getCmp('btnPDF').show();
			  }
			  if(!Ext.getCmp('btnXLS').isVisible()){
				 Ext.getCmp('btnXLS').show();
			  }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  
  var procesarConsultaDataEstatus = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridEstatus');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				
				gridEstatus.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
        Ext.getCmp('footerEstatus').setValue('Total Estatus: ' + store.getTotalCount());
				el.unmask();
        if(!Ext.getCmp('btnPDF').isVisible()){
          Ext.getCmp('btnPDF').show();
        }
        if(!Ext.getCmp('btnXLS').isVisible()){
          Ext.getCmp('btnXLS').show();
        }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  
  var procesarConsultaDataOperadas = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridOperadas');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
						
				gridOperadas.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
        Ext.getCmp('footerOperadas').setValue('Total Operadas: ' + store.getTotalCount());
				el.unmask();
        if(!Ext.getCmp('btnPDF').isVisible()){
          Ext.getCmp('btnPDF').show();
        }
        if(!Ext.getCmp('btnXLS').isVisible()){
          Ext.getCmp('btnXLS').show();
        }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  
  var procesarConsultaDataRechazadas = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridRechazadas');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
						
				gridRechazadas.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
        Ext.getCmp('footerRechazadas').setValue('Total Rechazadas: ' + store.getTotalCount());
				el.unmask();
        if(!Ext.getCmp('btnPDF').isVisible()){
          Ext.getCmp('btnPDF').show();
        }
        if(!Ext.getCmp('btnXLS').isVisible()){
          Ext.getCmp('btnXLS').show();
        }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  
  var procesarConsultaDataBloqueadas = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridBloqueadas');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
						
				gridBloqueadas.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
        Ext.getCmp('footerBloqueadas').setValue('Total Bloqueadas: ' + store.getTotalCount());
				el.unmask();
        if(!Ext.getCmp('btnPDF').isVisible()){
          Ext.getCmp('btnPDF').show();
        }
        if(!Ext.getCmp('btnXLS').isVisible()){
          Ext.getCmp('btnXLS').show();
        }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  
  var procesarConsultaDataDetenidas = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridDetenidas');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
							
				gridDetenidas.getColumnModel().setHidden(0, strTipoUsuario=='NAFIN'?false:true);
				
			 	Ext.getCmp('footerDetenidas').setValue('Total Detenidas: ' + store.getTotalCount());
					el.unmask();
			  if(!Ext.getCmp('btnPDF').isVisible()){
				 Ext.getCmp('btnPDF').show();
			  }
			  if(!Ext.getCmp('btnXLS').isVisible()){
				 Ext.getCmp('btnXLS').show();
			  }
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}  
  
  var catalogoEstatus = new Ext.data.ArrayStore({
		fields: ['clave','descripcion'],
		data:[['2', 'En Proceso'],['D', 'Detenidas'],['3', 'Operadas'],['4', 'Rechazadas'],['12', 'Bloqueadas']]
	});
  
	var consultaDataProceso = new Ext.data.GroupingStore({
		root : 'registros',
		url : '26reporte1ext.data.jsp',
		baseParams: { informacion: 'Consulta' },
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'CLIENTE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NUMERO_SOL'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_DESC'},
			{name: 'MONEDA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DOCTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'FOLIO'},
			{name: 'INTERMEDIARIO_F'},
         {name: 'NUMPRESTAMO'}
      ]
		}),
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaDataProceso,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaDataProceso(null, null, null);
				}
			}
		}
	});
  
	var consultaDataDetenidas  = new Ext.data.GroupingStore({
		root : 'registros',
		url : '26reporte1ext.data.jsp',
		baseParams: { informacion: 'Consulta', estatus: 'D' },
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'CLIENTE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NUMERO_SOL'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_DESC'},
			{name: 'MONEDA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DOCTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'FOLIO'},
			{name: 'INTERMEDIARIO_F'},
         {name: 'NUMPRESTAMO'}
      ]
		}),
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetenidas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaDataDetenidas(null, null, null);
				}
			}
		}
	});
  
	var consultaDataOperadas  = new Ext.data.GroupingStore({
		root : 'registros',
		url : '26reporte1ext.data.jsp',
		baseParams: { informacion: 'Consulta' },
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'CLIENTE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NUMERO_SOL'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_DESC'},
			{name: 'MONEDA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DOCTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'FOLIO'},
			{name: 'INTERMEDIARIO_F'},
         {name: 'NUMPRESTAMO'}
      ]
		}),
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaDataOperadas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaDataOperadas(null, null, null);
				}
			}
		}
	});
  
	var consultaDataRechazadas  = new Ext.data.GroupingStore({
		root : 'registros',
		url : '26reporte1ext.data.jsp',
		baseParams: { informacion: 'Consulta' },
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'CLIENTE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NUMERO_SOL'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_DESC'},
			{name: 'MONEDA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DOCTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'FOLIO'},
			{name: 'INTERMEDIARIO_F'},
         {name: 'NUMPRESTAMO'}
      ]
		}),
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaDataRechazadas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaDataRechazadas(null, null, null);
				}
			}
		}
	});
  
	var consultaDataBloqueadas  = new Ext.data.GroupingStore({
		root : 'registros',
		url : '26reporte1ext.data.jsp',
		baseParams: { informacion: 'Consulta' },
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'CLIENTE'},
			{name: 'NUMERO_SIRAC'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NUMERO_SOL'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_DESC'},
			{name: 'MONEDA'},
			{name: 'TIPO_CREDITO'},
			{name: 'MONTO_DOCTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'FOLIO'},
			{name: 'INTERMEDIARIO_F'},
         {name: 'NUMPRESTAMO'}
      ]
		}),
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarConsultaDataBloqueadas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaDataBloqueadas(null, null, null);
				}
			}
		}
	});
  /******   GRID *****/
  var gridProceso = new Ext.grid.GridPanel({
		store: consultaDataProceso,
      id: 'gridProceso',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 940,
		title: '<span style="font-weight:normal">Estatus: </span><b>En Proceso</b>',
		frame: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero', dataIndex: 'INTERMEDIARIO_F',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'N�mero de Sirac', tooltip: 'N�mero de Sirac', dataIndex: 'NUMERO_SIRAC',
				sortable: true,	width: 100,	resizable: true, hidden: false
			},{
				header : 'N�mero de Documento', tooltip: 'N�mero de Documento',	dataIndex : 'NUMERO_DOCTO',
				sortable : true, width : 100, hidden : false 
			},{
				header : 'N�mero de Solicitud', tooltip: 'N�mero de Solicitud',	dataIndex : 'NUMERO_SOL', 
        sortable : true, width : 100, hidden : false, renderer : function(val){ if(val==null || val=="")return "&nbsp;"; }
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_EMISION', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Fecha de Vencimiento del Descuento', tooltip: 'Fecha de Vencimiento del Descuento',	dataIndex : 'FECHA_VENC_DESC', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO', 
        sortable : true, width : 100, hidden : false, align :  'right'
			},{
				header : 'Monto del Documento', tooltip: 'Monto del Documento',	dataIndex : 'MONTO_DOCTO', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DESCONTAR', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Folio', tooltip: 'Folio',	dataIndex : 'FOLIO', 
        sortable : true, width : 100, hidden : false
			}
		],
    bbar:[{
      xtype: 'displayfield',
      id: 'footerProceso',
      value: ''
    }]
  }); 
  
  var gridDetenidas = new Ext.grid.GridPanel({
		store: consultaDataDetenidas,
    id: 'gridDetenidas',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 940,
		title: '<span style="font-weight:normal">Estatus: </span><b>Detenidas / CONTACTE A SU EJECUTIVO DE CUENTA</b>',
		frame: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero', dataIndex: 'INTERMEDIARIO_F',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'N�mero de Sirac', tooltip: 'N�mero de Sirac', dataIndex: 'NUMERO_SIRAC',
				sortable: true,	width: 100,	resizable: true, hidden: false
			},{
				header : 'N�mero de Documento', tooltip: 'N�mero de Documento',	dataIndex : 'NUMERO_DOCTO',
				sortable : true, width : 100, hidden : false 
			},{
				header : 'N�mero de Solicitud', tooltip: 'N�mero de Solicitud',	dataIndex : 'NUMERO_SOL', 
            sortable : true, width : 100, hidden : false, renderer : function(val){ if(val==null || val=="")return "&nbsp;"; }
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_EMISION', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Fecha de Vencimiento del Descuento', tooltip: 'Fecha de Vencimiento del Descuento',	dataIndex : 'FECHA_VENC_DESC', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Monto del Documento', tooltip: 'Monto del Documento',	dataIndex : 'MONTO_DOCTO', 
            sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DESCONTAR', 
            sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Folio', tooltip: 'Folio',	dataIndex : 'FOLIO', 
            sortable : true, width : 100, hidden : false
			}
		],
    bbar:[{
      xtype: 'displayfield',
      id: 'footerDetenidas',
      value: ''
    }]
  }); 
  
  var gridOperadas = new Ext.grid.GridPanel({
		store: consultaDataOperadas,
    id: 'gridOperadas',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 940,
		title: '<span style="font-weight:normal">Estatus: </span><b>Operadas</b>',
		frame: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero', dataIndex: 'INTERMEDIARIO_F',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'N�mero de Sirac', tooltip: 'N�mero de Sirac', dataIndex: 'NUMERO_SIRAC',
				sortable: true,	width: 100,	resizable: true, hidden: false
			},{
				header : 'N�mero de Documento', tooltip: 'N�mero de Documento',	dataIndex : 'NUMERO_DOCTO',
				sortable : true, width : 100, hidden : false 
			},{
				header : 'N�mero de Solicitud', tooltip: 'N�mero de Solicitud',	dataIndex : 'NUMERO_SOL', 
            sortable : true, width : 100, hidden : false, renderer : function(val){ if(val==null || val=="")return "&nbsp;"; }
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_EMISION', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Fecha de Vencimiento del Descuento', tooltip: 'Fecha de Vencimiento del Descuento',	dataIndex : 'FECHA_VENC_DESC', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'Monto del Documento', tooltip: 'Monto del Documento',	dataIndex : 'MONTO_DOCTO', 
            sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DESCONTAR', 
            sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Folio', tooltip: 'Folio',	dataIndex : 'FOLIO', 
            sortable : true, width : 100, hidden : false
			},{
				header : 'N�mero de Prestamo', tooltip: 'N�mero de Prestamo',	dataIndex : 'NUMPRESTAMO', 
            sortable : true, width : 100, hidden : false
			}
		],
    bbar:[{
      xtype: 'displayfield',
      id: 'footerOperadas',
      value: ''
    }]
  });  
  
  var gridRechazadas = new Ext.grid.GridPanel({
		store: consultaDataRechazadas,
    id: 'gridRechazadas',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 940,
		title: '<span style="font-weight:normal">Estatus: </span>Rechazadas',
		frame: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero', dataIndex: 'INTERMEDIARIO_F',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'N�mero de Sirac', tooltip: 'N�mero de Sirac', dataIndex: 'NUMERO_SIRAC',
				sortable: true,	width: 100,	resizable: true, hidden: false
			},{
				header : 'N�mero de Documento', tooltip: 'N�mero de Documento',	dataIndex : 'NUMERO_DOCTO',
				sortable : true, width : 100, hidden : false 
			},{
				header : 'N�mero de Solicitud', tooltip: 'N�mero de Solicitud',	dataIndex : 'NUMERO_SOL', 
        sortable : true, width : 100, hidden : false, renderer : function(val){ if(val==null || val=="")return "&nbsp;"; }
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_EMISION', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Fecha de Vencimiento del Descuento', tooltip: 'Fecha de Vencimiento del Descuento',	dataIndex : 'FECHA_VENC_DESC', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Monto del Documento', tooltip: 'Monto del Documento',	dataIndex : 'MONTO_DOCTO', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DESCONTAR', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Folio', tooltip: 'Folio',	dataIndex : 'FOLIO', 
        sortable : true, width : 100, hidden : false
			}
		],
    bbar:[{
      xtype: 'displayfield',
      id: 'footerRechazadas',
      value: ''
    }]
  }); 
  
  var gridBloqueadas = new Ext.grid.GridPanel({
		store: consultaDataBloqueadas,
    id: 'gridBloqueadas',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 940,
		title: '<span style="font-weight:normal">Estatus: </span>Bloqueadas',
		frame: true,
		columns: [{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero', dataIndex: 'INTERMEDIARIO_F',
				sortable: true,	width: 150,	resizable: true, hidden: false
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente', dataIndex: 'CLIENTE',
				sortable: true,	width: 200,	resizable: true, hidden: false
			},{
				header: 'N�mero de Sirac', tooltip: 'N�mero de Sirac', dataIndex: 'NUMERO_SIRAC',
				sortable: true,	width: 100,	resizable: true, hidden: false
			},{
				header : 'N�mero de Documento', tooltip: 'N�mero de Documento',	dataIndex : 'NUMERO_DOCTO',
				sortable : true, width : 100, hidden : false 
			},{
				header : 'N�mero de Solicitud', tooltip: 'N�mero de Solicitud',	dataIndex : 'NUMERO_SOL', 
        sortable : true, width : 100, hidden : false, renderer : function(val){ if(val==null || val=="")return "&nbsp;"; }
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_EMISION', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Fecha de Vencimiento del Descuento', tooltip: 'Fecha de Vencimiento del Descuento',	dataIndex : 'FECHA_VENC_DESC', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Tipo de Cr�dito', tooltip: 'Tipo de Cr�dito',	dataIndex : 'TIPO_CREDITO', 
        sortable : true, width : 100, hidden : false
			},{
				header : 'Monto del Documento', tooltip: 'Monto del Documento',	dataIndex : 'MONTO_DOCTO', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DESCONTAR', 
        sortable : true, width : 100, hidden : false, align :  'right', renderer : Ext.util.Format.usMoney
			},{
				header : 'Folio', tooltip: 'Folio',	dataIndex : 'FOLIO', 
        sortable : true, width : 100, hidden : false
			}
		],
    bbar:[{
      xtype: 'displayfield',
      id: 'footerBloqueadas',
      value: ''
    }]
  }); 
  /****** FIN DE GRID *******/  
  var elementosForma = [{
    xtype: 'label',
    html: '<b>Usuario:</b> <span style="padding:5px">' + Ext.getDom('strNombreUsuario').value +"</span>",
    id: 'lblUsuario'
  },{
    xtype:'label',
    html: '<div style="margin:3px 0 5px 0px;padding:5px;"><b>Fecha:</b> <span style="padding:5px;">' + fechaHoy.getDate()+'/'+(fechaHoy.getMonth()+1)+'/'+fechaHoy.getFullYear()+' '+horaMomento + "</span>",
    id: 'lblFecha'
  },{
    xtype: 'combo',
    hiddenName: 'estatus',
    name: 'estatus',
    id: 'id_estatus',
    allowBlank: false,
    fieldLabel: 'Estatus',
    mode: 'local',
    displayField: 'descripcion',
    valueField: 'clave',
    emptyText: '- Seleccione -',
    forceSeleccion: true,
    triggerAction: 'all',
    typeAhead: true,
    minChars: 1,
    selectOnFocus:true,
    anchor: '50%',
    store: catalogoEstatus,
    listeners:{
      select: function(combo){
        var fp = Ext.getCmp('forma');
        gridProceso.hide();
        gridDetenidas.hide();
        gridOperadas.hide();
        gridRechazadas.hide();
        gridBloqueadas.hide();   
        Ext.getCmp('btnPDF').hide();
        Ext.getCmp('btnXLS').hide();  
        
        if(combo.getValue()=='2'){
          consultaDataProceso.load({params: Ext.apply(fp.getForm().getValues(),{})});  
          gridProceso.show();
        }else if(combo.getValue()=='D'){ 
          consultaDataDetenidas.load({params: Ext.apply(fp.getForm().getValues(),{})}); 
          gridDetenidas.show();
        }else if(combo.getValue()=='3'){  
          consultaDataOperadas.load({params: {estatus:'3'}});  
          gridOperadas.show();
        }else if(combo.getValue()=='4'){  
          consultaDataRechazadas.load({params: {estatus:'4'}});  
          gridRechazadas.show();
        }else if(combo.getValue()=='12'){  
          consultaDataBloqueadas.load({params: {estatus:'12'}});  
          gridBloqueadas.show();
        }
      }
    }
  }];
  
  var fp = {
		xtype: 'form',
		id: 'forma',
		style: 'margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 450,
    labelWidth: 50,
		titleCollapse: false,
    title: '<div style="text-align:center">Reporte de Operaciones por Estatus</div>',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
    buttons:[{
      text: 'Imprimir',
			id:   'btnPDF',
			iconCls: 'icoPdf',
			formBind: true,
      hidden: true,
      handler: function(boton, evento) {
         Ext.getCmp('btnPDF').setIconClass('loading-indicator');
         var fp = Ext.getCmp('forma');		
			Ext.Ajax.request({
            url: '26reporte1ext.data.jsp',
            params:Ext.apply(fp.getForm().getValues(),{
            informacion: 'PDF'  
         }),
         callback: procesarGenerarPDF
        });
      }
    },{
      text: 'Generar Archivo',
			id:   'btnXLS',
			iconCls: 'icoXls',
			formBind: true,
         hidden: true,
         handler: function(boton, evento) {
            var fp = Ext.getCmp('forma');		
            Ext.getCmp('btnXLS').setIconClass('loading-indicator');
				Ext.Ajax.request({
               url: '26reporte1ext.data.jsp',
               params:Ext.apply(fp.getForm().getValues(),{
               informacion: 'XLS'  
            }),
            callback: procesarGenerarXLS
        });
      }
    }],
		items:[elementosForma]
	};//FIN DE LA FORMA
  
  var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
      NE.util.getEspaciador(10),
      gridProceso,
      NE.util.getEspaciador(10),
      gridDetenidas,
      NE.util.getEspaciador(10),
      gridOperadas,
      NE.util.getEspaciador(10),
      gridRechazadas,
      NE.util.getEspaciador(10),
      gridBloqueadas
		]
	});  
});