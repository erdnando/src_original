Ext.onReady(function(){


//-----------------Handlers--------------------
var procesarConsultaData = function(store,arrRegistros,opts){
			Ext.getCmp('btnBajarPDF').hide();
			Ext.getCmp('btnBajarArchivo').hide();
			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnGenerarArchivo').disable();
					
				}
			}
		}
function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesaConsulta(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('descarga');
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			verDescarga.setVisible(true);
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
function procesaConsultaAmort(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			if(Ext.util.JSON.decode(response.responseText).Totales>0){
				Ext.getCmp('descargaPDF').enable();
				Ext.getCmp('descargaCSV').enable();
				storeAmort.loadData(Ext.util.JSON.decode(response.responseText).registros);
				var btnBajarArchivo = Ext.getCmp('descargaCSV');
				btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
			
			var btnBajarArchivo2 = Ext.getCmp('descargaPDF');
			btnBajarArchivo2.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivoPDF;
				forma.submit();
			});
			}
			else{
				//NE.util.mostrarConnError(response,opts);
				var el = gridTabla.getGridEl();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('descargaPDF').disable();
				Ext.getCmp('descargaCSV').disable();
				
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarTabla = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		Ext.getCmp('descargaPDF').disable();
		Ext.getCmp('descargaCSV').disable();
		verTabla.show();
		verTabla.setTitle('<center>N�mero de Prestamo: '+registro.get('NUMPRESTAMO')+'</center>');
		Ext.Ajax.request({
							url: '26consulta2ext.data.jsp',
							params: Ext.apply({informacion:'tablaAmort',numPrestamo:registro.get('NUMPRESTAMO') }),
							callback: procesaConsultaAmort
						});
	}
	var procesarDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		
					Ext.Ajax.request({
							url: '26consulta2ext.data.jsp',
							params: Ext.apply({informacion:'generaOrigen',claveSolicitud:registro.get('NUMFOLIO') }),
							callback: procesarArchivoSuccess
						});
	}
var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//-----------------Stores----------------------

var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '26consulta2ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'NOMBREPYME'},
				{name: 'CLAVESIRAC'},
				{name: 'NUMDOCTO'},
				{name: 'PERIODICIDAD_PAGO'},
				{name: 'FECHAETC'},
				{name: 'FECHADSCTO'},
				{name: 'NOMBREMONEDA'},
				{name: 'DESCTIPOCRED'},
				{name: 'IMPORTEDOCTO'},
				{name: 'IMPORTEDSCTO'},
				{name: 'NUMFOLIO'},
				{name: 'FECHAOPERACION'},
				{name: 'NUMPRESTAMO'},
				{name: 'NUMACUSE'},
				{name: 'IC_TASAUF'},
				{name: 'CG_RMUF'},
				{name: 'FG_STUF'},
				{name: 'ESTATUS'},
				{name: 'IC_TABLA_AMORT'},
				{name: 'ESTATUS_SOLI'},
				{name: 'FECHAENVIOIF'},
				{name: 'CODIGOAPLICACION'},
				{name: 'ERRORDESCRIPCION'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
var storeAmort = new Ext.data.JsonStore({
		fields: [
			{name:'NUM_AMOR'},{name:'FECHA_AMOR'},{name:'TIPO_CUOTA'},{name:'MONTO_AMOR'}			
		],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

var catalogoEstatus = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '26consulta2ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEstatus'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo, 
		load: function(){Ext.getCmp('estatus').setValue('3');}
		}
	});

//--------------Componentes-------------------
var dt = new Date();

var gridTabla = new Ext.grid.GridPanel({
				id: 'grid2',
				hidden: false,
				header:true,
				title: '<center>Tabla de Amortizaci�n</center>',
				store: storeAmort,
				style: 'margin:0 auto;',
				columns:[
					{
						
						header:'N�mero de Amortizaci�n',
						tooltip: 'N�mero de Amortizaci�n',
						sortable: true,
						dataIndex: 'NUM_AMOR',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Fecha de Amortizaci�n',
						tooltip: 'Fecha de Amortizaci�n',
						sortable: true,
						dataIndex: 'FECHA_AMOR',
						width: 140,
						align: 'center'
						},
						{
						header: 'Tipo de Cuota',
						tooltip: 'Tipo de Cuota',
						sortable: true,
						dataIndex: 'TIPO_CUOTA',
						width: 140,
						align: 'center'
						},
						{
						header: 'Monto de Amortizaci�n',
						tooltip: 'Monto de Amortizaci�n',
						sortable: true,
						dataIndex: 'MONTO_AMOR',
						width: 140,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 300,
				width: 600,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					autoScroll:true,
					buttonAlign: 'left',
					
					emptyMsg: "No hay registros.",
					items: ['->','-',{
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Descargar Archivo',
			  id:'descargaCSV',
			  hidden: false,
			  formBind: true
			 },
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Imprimir',
			  id:'descargaPDF',
			  hidden: false,
			  formBind: true
			 }	,
								'-']
				}
				});

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Nombre del Cliente',
						tooltip: 'Nombre del Cliente',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'N�mero de Sirac',
						tooltip: 'N�mero de Sirac',
						sortable: true,
						dataIndex: 'CLAVESIRAC',
						width: 150,
						align: 'center'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMDOCTO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Periodicidad de Pago de Capital',
						tooltip: 'Periodicidad de Pago de Capital',
						sortable: true,
						dataIndex: 'PERIODICIDAD_PAGO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha Emisi�n T�tulo de Cr�dito',
						tooltip: 'Fecha Emisi�n T�tulo de Cr�dito',
						sortable: true,
						dataIndex: 'FECHAETC',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Fecha Vencimiento Descuento',
						tooltip: 'Fecha Vencimiento Descuento',
						sortable: true,
						dataIndex: 'FECHADSCTO',
						width: 130
						},{
						
						header:'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},
						{
						
						header: 'Tipo de Cr�dito',
						tooltip: 'Tipo de Cr�dito',
						sortable: true,
						dataIndex: 'DESCTIPOCRED',
						width: 150,
						align: 'center'
						},
						{
						header: 'Monto del Documento',
						tooltip: 'Monto del Documento',
						sortable: true,
						dataIndex: 'IMPORTEDOCTO',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Monto del Descuento',
						tooltip: 'Monto del Descuento',
						sortable: true,
						dataIndex: 'IMPORTEDSCTO',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'N�mero de Folio',
						tooltip: 'N�mero de Folio',
						sortable: true,
						dataIndex: 'NUMFOLIO',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Fecha de Operaci�n',
						tooltip: 'Fecha de Operaci�n',
						sortable: true,
						dataIndex: 'FECHAOPERACION',
						width: 130
						},
						{
						header: 'N�mero de Prestamo',
						tooltip: 'N�mero de Prestamo',
						sortable: true,
						dataIndex: 'NUMPRESTAMO',
						width: 130,
						align: 'center'
						},
						{
						header: 'N�mero de Acuse',
						tooltip: 'N�mero de Acuse',
						sortable: true,
						dataIndex: 'NUMACUSE',
						width: 130,
						align: 'right'
						},
						{
						header: 'Tasa Usuario Final',
						tooltip: 'Tasa Usuario Final',
						sortable: true,
						dataIndex: 'IC_TASAUF',
						width: 150,
						align: 'center'
						},
						{
						header: 'Relaci�n Matem�tica',
						tooltip: 'Relaci�n Matem�tica',
						sortable: true,
						dataIndex: 'CG_RMUF',
						width: 150,
						align: 'center'
						},
						{
						header: 'Sobre Tasa Usuario Final',
						tooltip: 'Sobre Tasa Usuario Final',
						sortable: true,
						dataIndex: 'FG_STUF',
						width: 150,
						align: 'center'
						},
						{
						header: 'Estatus',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS',
						width: 150,
						align: 'center'
						},
						{
						xtype: 'actioncolumn',
						header: 'Tabla de Amortizaci�n',
						tooltip: 'Tabla de Amortizaci�n',
						sortable: true,
						dataIndex: 'IC_TABLA_AMORT',
						width: 150,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
									 if(registro.get('IC_TABLA_AMORT')==5){
							       return 'iconoLupa';	
									 //alert();
									 }
									 else{
									 return '';
									 }
								 }
								 ,handler: procesarTabla
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'Ver Archivo Origen',
						tooltip: 'Ver Archivo Origen',
						sortable: true,
						dataIndex: 'ESTATUS_SOLI',
						width: 150,
						align: 'center',
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarDetalle
					      }]
						},
						{
						header: 'Fecha de Env�o del IF',
						tooltip: 'Fecha de Env�o del IF',
						sortable: true,
						dataIndex: 'FECHAENVIOIF',
						width: 150,
						align: 'center'
						},
						{
						header: 'C�digo de Error',
						tooltip: 'C�digo de Error',
						sortable: true,
						dataIndex: 'CODIGOAPLICACION',
						width: 150,
						align: 'center'
						},
						{
						header: 'Descripci�n',
						tooltip: 'Descripci�n',
						sortable: true,
						dataIndex: 'ERRORDESCRIPCION',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}

				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
				
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar PDF',
									id: 'btnGenerarPDF',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '26consulta2ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoPDF',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize}),
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '26consulta2ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-']
				}
		});

var elementosForma=[
			{
				xtype: 'textfield',
				name: 'acuse',
				id: 'acuse',
				fieldLabel: 'No. de Acuse',
				anchor:'74%',
				maxLength: 22
			},{
				xtype: 'numberfield',
				name: 'numFolio',
				id: 'numFolio',
				anchor:'74%',
				fieldLabel: 'N�mero de Folio'							
			},{
				xtype: 'numberfield',
				name: 'nomPrestamo',
				id: 'nomPrestamo',
				anchor:'74%',
				fieldLabel: 'N�mero de Prestamo'
				
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha Envio de IF de',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'datefield',
						name: 'fechaIF',
						id: 'fechaIF',
						startDay: 0,
						width: 100,
						allowBlank: false,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'fechaIF2',
						margins: '0 20 0 0',  //necesario para mostrar el icono de error
						listener:{
								blur:{ fn:function(){
									Ext.getCmp('fechaIF2').focus();
								}
							}
						}
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 24
					},
					{
						xtype: 'datefield',
						name: 'fechaIF2',
						id: 'fechaIF2',
						startDay: 1,
						width: 100,
						allowBlank: false,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'fechaIF',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Operaci�n de',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'datefield',
						name: 'fechaOper',
						id: 'fechaOper',
						startDay: 0,
						width: 100,
						value:  dt,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'fechaOper2',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 24
					},
					{
						xtype: 'datefield',
						name: 'fechaOper2',
						id: 'fechaOper2',
						startDay: 1,
						width: 100,
						value: dt,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'fechaOper',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype: 'combo',
				name: 'estatus',
				id:	'estatus',
				fieldLabel: 'Estatus',
				emptyText: 'Seleccione estatus',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'Hestatus',
				forceSelection : false,
				anchor:'74%',
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: catalogoEstatus,
				tpl : NE.util.templateMensajeCargaCombo
			}

]

var panelTablaAmor = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		width: 'auto',
		title: '',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'tablaAmor',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		items: [gridTabla]
  });

var panelDetalles = new Ext.form.FormPanel
  ({
	
		hidden: false,
		height: 'auto',
		width: 'auto',
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 160,
		defaultType: 'textfield',
		frame: true,
		id: 'forma5',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		
		monitorValid: true,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  align: 'center',
			  text: 'Descargar Archivo',
			  id:'descarga',
			  hidden: false,
			  formBind: true
			 }
		  ]
  });
  
var verTabla = new Ext.Window({
			title: 'Descarga Archivo',
			width: 650,
			height: 400,
			maximizable: false,
			modal: true,
			id:'ventana2',
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelTablaAmor]
});

var verDescarga = new Ext.Window({
			title: 'Descarga Archivo',
			width: 200,
			id:'ventana1',
			height: 100,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [panelDetalles]
});
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{			
						
						Ext.getCmp('btnGenerarPDF').enable();
						Ext.getCmp('btnGenerarArchivo').enable();
						Ext.getCmp('btnBajarPDF').hide();
						Ext.getCmp('btnBajarArchivo').hide();
						var dt2 = new Date(Ext.getCmp('fechaIF').getValue()).add(Date.DAY, 8);
						if(dt2>Ext.getCmp('fechaIF2').getValue()){
							//Funcion ajax
							grid.setVisible(true);
							grid.show();
							consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
						}else{
							Ext.MessageBox.alert('Error','El rango de fechas de Envio del IF no debe de ser mayor a 7 d�as');
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});
//--------------Principal---------------------

var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [fp,NE.util.getEspaciador(30),grid,verDescarga,verTabla]
  });
  catalogoEstatus.load();
  Ext.getCmp('fechaIF').focus();
});