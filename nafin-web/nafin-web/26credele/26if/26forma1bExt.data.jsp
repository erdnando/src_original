<%@ page contentType="text/html; charset=UTF-8"
import=" com.jspsmart.upload.*, 
com.netro.exception.*,
netropology.utilerias.*,
net.sf.json.JSONArray,  
net.sf.json.JSONObject,
java.util.*, java.text.*, 
com.netro.afiliacion.*,
com.netro.pdf.ComunesPDF,
com.jspsmart.upload.SmartUpload"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/26credele/26secsession_extjs.jspf"%>
<%    
String resultadoArch="";
String sError="", name="", value="", sSinError="",sConError="",totRegSiracBien="",
totRegSiracMal="",sArchivoErrores="",sPymesOK="",nombreArchivoPDF="";
String nombreArchivo="";
JSONArray jsonArr = new JSONArray();
boolean bTodoOK=false;
String NoIf ="";
String informacion		= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String resultadoRes="";
if(informacion.equals("Proceso"))
{
try {
Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
sPymesOK = (request.getParameter("sPymesOK")==null)?"":request.getParameter("sPymesOK");
sArchivoErrores = (request.getParameter("sArchivoErrores")==null)?"":request.getParameter("sArchivoErrores");
			NoIf = (request.getParameter("NoIf")==null)?"":request.getParameter("NoIf");
	
	Hashtable hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sPymesOK, "", "", "SI", NoIf, iNoUsuario, strTipoUsuario);
	String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();
	CreaArchivo archivo = new CreaArchivo();

	StringBuffer contenidoArchivo = new StringBuffer("PYMES Afiliadas a Crédito Electrónico\nNúmero de Cliente SIRAC,Nombre Completo o Razón Social,RFC,Tipo Persona,Sector Económico,Subsector,Rama,Clase,Estrato,Número de Folio"+(strTipoUsuario.equals("NAFIN")?",Número de Cliente TROYA":"")+"\n");	
	/////////////////////////////////////////////
									//---------------Hacer PDF-----------------//
									
									 nombreArchivoPDF = Comunes.cadenaAleatoria(16) + ".pdf";
									ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivoPDF);
									pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
									session.getAttribute("iNoNafinElectronico").toString(),
									(String)session.getAttribute("sesExterno"),
									(String) session.getAttribute("strNombre"),
									(String) session.getAttribute("strNombreUsuario"),
									(String)session.getAttribute("strLogo"),
									(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
									
									String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
									String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
									String diaActual    = fechaActual.substring(0,2);
									String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
									String anioActual   = fechaActual.substring(6,10);
									String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
									
									pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
									 pdfDoc.addText("Resumen de carga", "formas",ComunesPDF.CENTER);
									 pdfDoc.addText("PYMES Afiliadas a Crédito Electrónico", "formas",ComunesPDF.CENTER);
			//////////FIN CABECERA PDF
			////Columnas titulo
			pdfDoc.setTable(10, 100);
											pdfDoc.setCell("Número de Cliente SIRAC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Nombre Completo o Razón Social","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Tipo Persona","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Sector Económico","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Subsector","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Rama","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Clase","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Estrato","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Número de folio","celda01",ComunesPDF.CENTER);
											
											
	
	Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
				for(int i=0; i<vDatosPyme.size(); i++) {
					Vector vPyme = (Vector)vDatosPyme.get(i);	
					JSONObject jsonObj = new JSONObject();
										jsonObj.put("SIRAC",vPyme.get(0));
										jsonObj.put("NOMBRERAZON",(vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()));
										jsonObj.put("RFC",vPyme.get(5));
										jsonObj.put("TIPO_PER",(vPyme.get(6).toString().equals("F")?"Física":"Moral"));
										jsonObj.put("SECTOR",vPyme.get(7));
										jsonObj.put("SUBSECTOR",vPyme.get(8));
										jsonObj.put("RAMA",vPyme.get(9));
										jsonObj.put("CLASE",vPyme.get(10));
										jsonObj.put("ESTRATO",vPyme.get(11));
										jsonObj.put("FOLIO",vPyme.get(12));	
										
										//celdas pdf
											pdfDoc.setCell(vPyme.get(0).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell((vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(5).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell((vPyme.get(6).toString().equals("F")?"Física":"Moral"),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(7).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(8).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(9).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(10).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(11).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(12).toString(),"formas",ComunesPDF.CENTER);
											
											
                       jsonArr.add(jsonObj);    
                 		contenidoArchivo.append(vPyme.get(0).toString()+","+(vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString())+","+vPyme.get(5).toString()+","+(vPyme.get(6).toString().equals("F")?"Física":"Moral")+","+vPyme.get(7).toString().replace(',',' ')+","+vPyme.get(8).toString().replace(',',' ')+","+vPyme.get(9).toString().replace(',',' ')+","+vPyme.get(10).toString().replace(',',' ')+","+vPyme.get(11).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?vPyme.get(13).toString():"")+"\n");
				}
							pdfDoc.addTable();
							pdfDoc.endDocument();
							
					String nombreArchivoErr="";
				 if(!sArchivoErrores.equals("")) {
					
						if (archivo.make(sArchivoErrores, strDirectorioTemp, ".csv"))
							nombreArchivoErr = archivo.nombre;	
					 
				}
				if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
						nombreArchivo = archivo.nombre;
				resultadoArch="{\"success\": true,\"urlArchivoError\":\""+strDirecVirtualTemp + nombreArchivoErr+"\", \"total\": \"" + 
											jsonArr.size() + "\", \"registros\": " +
												jsonArr.toString()
												+",\"urlArchivo\":\""+strDirecVirtualTemp + nombreArchivo+"\",\"urlPDF\":\""+
												strDirecVirtualTemp + nombreArchivoPDF+"\""+"}";  
				
					
} catch(NafinException ne){	
	
		System.out.println(ne.getMsgError());

} catch(Exception e) { out.println(e.getMessage()); }
}
else
{
try {
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	SmartUpload myUpload=new SmartUpload();	
	
	myUpload.initialize(pageContext);
	myUpload.setTotalMaxFileSize(2097152);
	myUpload.upload();
	
	Enumeration en = myUpload.getRequest().getParameterNames();
	Hashtable hiddens=new Hashtable();
	while (en.hasMoreElements()) {
		name = en.nextElement().toString().trim();
		value = myUpload.getRequest().getParameter(name);
		hiddens.put(name,value);
	}
	String TipoPYME = hiddens.get("TipoPYME").toString();
	NoIf = hiddens.get("NoIf").toString();
	
	boolean bOkArchivo = true;
	if(myUpload.getSize() > 2097152) {
		sError = "Error, el Archivo es muy Grande, excede el L&iacute;mite que es de 2 MB. \n";
		bOkArchivo = false;
		resultadoArch="{\"success\": false, \"sError\":"+sError+"}";
	}
	int numFiles = 0;
	numFiles = myUpload.save(strDirectorioTemp);
	if(bOkArchivo && numFiles>0) {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		String Ruta = strDirectorioTemp + myFile.getFileName();
		java.io.File fArchivo = new java.io.File(Ruta);
		StringBuffer sbDatosPymes = new StringBuffer();
		String linea="";
		BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(fArchivo)));
		while((linea=br.readLine())!=null) {
			sbDatosPymes.append(linea.trim()+"\n");
		}
		Hashtable hAfiliaCE = BeanAfiliacion.afiliaPymeCE(sbDatosPymes.toString(), "", "", "", NoIf, iNoUsuario, strTipoUsuario);
		bTodoOK = new Boolean(hAfiliaCE.get("bTodoOK").toString()).booleanValue();

		if(bTodoOK)
		{
		StringBuffer contenidoArchivo = new StringBuffer("PYMES Afiliadas a Crédito Electrónico\nNúmero de Cliente SIRAC,Nombre Completo o Razón Social,RFC,Tipo Persona,Sector Económico,Subsector,Rama,Clase,Estrato,Número de Folio"+(strTipoUsuario.equals("NAFIN")?",Número de Cliente TROYA":"")+"\n");
		///////////////////////////////////////////
									//---------------Hacer PDF-----------------//
									
									 nombreArchivoPDF = Comunes.cadenaAleatoria(16) + ".pdf";
									ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivoPDF);
									pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
									session.getAttribute("iNoNafinElectronico").toString(),
									(String)session.getAttribute("sesExterno"),
									(String) session.getAttribute("strNombre"),
									(String) session.getAttribute("strNombreUsuario"),
									(String)session.getAttribute("strLogo"),
									(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
									
									String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
									String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
									String diaActual    = fechaActual.substring(0,2);
									String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
									String anioActual   = fechaActual.substring(6,10);
									String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
									
									pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
									 pdfDoc.addText("Resumen de carga", "formas",ComunesPDF.CENTER);
									 pdfDoc.addText("PYMES Afiliadas a Crédito Electrónico", "formas",ComunesPDF.CENTER);
			//////////FIN CABECERA PDF
			////Columnas titulo
			pdfDoc.setTable(10, 100);
											pdfDoc.setCell("Número de Cliente SIRAC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Nombre Completo o Razón Social","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Tipo Persona","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Sector Económico","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Subsector","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Rama","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Clase","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Estrato","celda01",ComunesPDF.CENTER);
											pdfDoc.setCell("Número de folio","celda01",ComunesPDF.CENTER);
											
		
		
		String sPymesDadasAlta = hAfiliaCE.get("sNoPymesAlta").toString();	
             
       Vector vDatosPyme = BeanAfiliacion.getPymeAfiliadasCE(sPymesDadasAlta);
		 	
				for(int i=0; i<vDatosPyme.size(); i++) {
					Vector vPyme = (Vector)vDatosPyme.get(i);
                          JSONObject jsonObj = new JSONObject();
										jsonObj.put("SIRAC",vPyme.get(0));
										jsonObj.put("NOMBRERAZON",(vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()));
										jsonObj.put("RFC",vPyme.get(5));
										jsonObj.put("TIPO_PER",(vPyme.get(6).toString().equals("F")?"Física":"Moral"));
										jsonObj.put("SECTOR",vPyme.get(7));
										jsonObj.put("SUBSECTOR",vPyme.get(8));
										jsonObj.put("RAMA",vPyme.get(9));
										jsonObj.put("CLASE",vPyme.get(10));
										jsonObj.put("ESTRATO",vPyme.get(11));
										jsonObj.put("FOLIO",vPyme.get(12));	
											if("NAFIN".equals(strTipoUsuario)){
												jsonObj.put("TROYA",vPyme.get(13));
											}
											
										//celdas pdf
											pdfDoc.setCell(vPyme.get(0).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell((vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString()),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(5).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell((vPyme.get(6).toString().equals("F")?"Física":"Moral"),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(7).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(8).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(9).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(10).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(11).toString(),"formas",ComunesPDF.CENTER);
											pdfDoc.setCell(vPyme.get(12).toString(),"formas",ComunesPDF.CENTER);
											
                       jsonArr.add(jsonObj);    
                  String NomCompleto = (vPyme.get(6).toString().equals("F")?vPyme.get(1).toString()+" "+vPyme.get(2).toString()+" "+vPyme.get(3).toString():vPyme.get(4).toString());
					   NomCompleto = NomCompleto.replace(',',' ');	
						contenidoArchivo.append(vPyme.get(0).toString()+","+NomCompleto+","+vPyme.get(5).toString()+","+(vPyme.get(6).toString().equals("F")?"Física":"Moral")+","+vPyme.get(7).toString().replace(',',' ')+","+vPyme.get(8).toString().replace(',',' ')+","+vPyme.get(9).toString().replace(',',' ')+","+vPyme.get(10).toString().replace(',',' ')+","+vPyme.get(11).toString()+","+vPyme.get(12).toString()+","+(strTipoUsuario.equals("NAFIN")?vPyme.get(13).toString():"")+"\n");
				} // for  	
					
						pdfDoc.addTable();
							pdfDoc.endDocument();
							
					CreaArchivo archivo = new CreaArchivo();
					if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
						nombreArchivo = archivo.nombre;	
                      
				resultadoArch="{\"success\": true,\"urlArchivo\":\""+strDirecVirtualTemp + nombreArchivo+"\", \"total\": \"" + 
											jsonArr.size() + "\", \"registros\": " +
												jsonArr.toString()+"}";      
             
			}
			//Error con el archivo
			else
			{
				resultadoArch="{\"success\":false"+"}";
				sSinError = hAfiliaCE.get("sSinError").toString();
				sSinError=sSinError.replaceAll("\n","\\\\"+"n");
				sSinError=sSinError.replaceAll("\"","\\\\\"");
				sConError = hAfiliaCE.get("sConError").toString();
				sConError=sConError.replaceAll("\n","\\\\"+"n");
				sConError=sConError.replaceAll("\"","\\\\\"");
				totRegSiracBien=hAfiliaCE.get("iTotalRegBien").toString();
            totRegSiracMal=hAfiliaCE.get("iTotalRegMal").toString();
				if(sSinError.length() > 0) { 
                      sArchivoErrores=hAfiliaCE.get("sArchivoErrores").toString();
                       sPymesOK=hAfiliaCE.get("sPymesOK").toString();
							sArchivoErrores=sArchivoErrores.replaceAll("\n","\\\\"+"n");
							sPymesOK=sPymesOK.replaceAll("\n","\\\\"+"n");
                      }                             
			}
	}
}catch(Exception e) { System.out.println("eeror en alta masiva"); }}%>
<%  if(informacion.equals("")){%>
{"success": true,
"TodOk": <%=  (bTodoOK)?"true":"false"%>,
"sError":"<%=  sError%>",
"NoIf":"<%=  NoIf%>",
"sArchivoErrores":"<%=sArchivoErrores%>",
"sPymesOK":"<%=sPymesOK%>",
"sSinError":"<%=  sSinError%>",
"sConError":"<%=  sConError%>",
"totRegSiracBien":"<%=  totRegSiracBien.toString()%>",
"totRegSiracMal":"<%=  totRegSiracMal.toString()%>",
"urlArchivo":"<%=  strDirecVirtualTemp + nombreArchivo%>", 
"urlPDF":"<%=strDirecVirtualTemp + nombreArchivoPDF%>",
"registros":<%=  jsonArr.toString()%>} 
<%  }else{%>
<%=resultadoArch%>
<%  }%>
