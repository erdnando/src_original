<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.pdf.*,
	com.netro.afiliacion.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/26credele/26secsession_extjs.jspf"%>
<%@ include file="../certificado.jspf" %>

<% 
String  resultado = null;
String  operacion = request.getParameter("operacion")==null?"":request.getParameter("operacion");
String  informacion= request.getParameter("informacion")==null?"":request.getParameter("informacion");
String	fecha_actual		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String	txt_fecha_oper_de 	= (request.getParameter("txt_fecha_oper_de")==null)?fecha_actual:request.getParameter("txt_fecha_oper_de").trim();
String	txt_fecha_oper_a 	= (request.getParameter("txt_fecha_oper_a")==null)?fecha_actual:request.getParameter("txt_fecha_oper_a").trim();
String	hidAction 			= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction").trim();
//String	condicion = new String();
List		lVarBind		= new ArrayList();
List		renglones		= new ArrayList();
AccesoDB	con				= new AccesoDB();
int			registros		= 0;

if(informacion.equals("Consulta")){
  StringBuffer qrySentencia = new StringBuffer();
  
  try {
    con.conexionDB();
    qrySentencia.append(" SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp)*/");
    qrySentencia.append("           TO_CHAR (sp.df_operacion, 'dd/mm/yyyy') fecha_operacion,");
    qrySentencia.append("           COUNT (1) operaciones, SUM (sp.fn_importe_dscto) monto,");
    qrySentencia.append("           mon.cd_nombre moneda, " );
    qrySentencia.append("           DECODE (sp.cs_tipo_plazo, 'C', 'Cr�dito', 'Factoraje'), sp.cs_tipo_plazo," );
    qrySentencia.append("           sp.ic_moneda, sp.df_operacion");
    qrySentencia.append("    FROM com_solic_portal sp, comcat_moneda mon");
    qrySentencia.append("    WHERE sp.ic_moneda = mon.ic_moneda(+)");
    qrySentencia.append("      AND sp.ic_if = ?");
    qrySentencia.append("      AND sp.ic_estatus_solic IN (?, ?, ?)");
    qrySentencia.append("      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')");
    qrySentencia.append("      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)");
    qrySentencia.append(" GROUP BY TO_CHAR (sp.df_operacion, 'dd/mm/yyyy'), " );
    qrySentencia.append("           mon.cd_nombre, DECODE (cs_tipo_plazo, 'C', 'Cr�dito', 'Factoraje'), cs_tipo_plazo, sp.ic_moneda, sp.df_operacion");
    qrySentencia.append(" ORDER BY sp.df_operacion ");
    
    lVarBind = new ArrayList();
    lVarBind.add(new Integer(iNoCliente));
    lVarBind.add(new Integer(3));
    lVarBind.add(new Integer(5));
    lVarBind.add(new Integer(6));
    lVarBind.add(txt_fecha_oper_de);
    lVarBind.add(txt_fecha_oper_a);
    renglones = con.consultaDB(qrySentencia.toString(), lVarBind, false);
      
    List reg = new ArrayList();
    HashMap datos = new HashMap();
    for(int i=0;i<renglones.size();i++) {
      List lDatos = (ArrayList)renglones.get(i);
      String rs_fecha_operacion 	= lDatos.get(0).toString();
  //				String rs_acuse 			= lDatos.get(1).toString();
      String rs_num_oper 			= lDatos.get(1).toString();
      String rs_monto 			= lDatos.get(2).toString();
      String rs_moneda 			= lDatos.get(3).toString();
      String rs_tipo_plazo 		= lDatos.get(4).toString();
      String rs_cve_tipo_plazo 	= lDatos.get(5).toString();
      String rs_cve_moneda 		= lDatos.get(6).toString();
      
      qrySentencia = new StringBuffer();      
      qrySentencia.append(" SELECT COUNT (1)");
      qrySentencia.append("   FROM comrel_constancia_deposito");
      qrySentencia.append(" WHERE ic_if = ?");
      qrySentencia.append("    AND ic_moneda = ?");
      qrySentencia.append("    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')");
      qrySentencia.append("    AND df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)");
      qrySentencia.append("    AND ic_usuario IS NOT NULL");
        
      lVarBind = new ArrayList();
      lVarBind.add(new Integer(iNoCliente));
      lVarBind.add(new Integer(rs_cve_moneda));
      lVarBind.add(rs_fecha_operacion);
      lVarBind.add(rs_fecha_operacion);
      int existe = con.existeDB(qrySentencia.toString(), lVarBind);
      
      datos = new HashMap();
      datos.put("FENVIO_OPER",rs_fecha_operacion);
      datos.put("NUM_OPER",rs_num_oper);
      datos.put("MONTO",rs_monto);
      datos.put("MONEDA",rs_moneda);
      datos.put("CVE_MONEDA",rs_cve_moneda);
      datos.put("TIPO",rs_tipo_plazo);
      datos.put("TIPO_PLAZO",rs_cve_tipo_plazo);
      datos.put("CONSTANCIA",rs_num_oper);
      datos.put("EXISTE",Integer.toString(existe));
      reg.add(datos);
    }
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("registros", reg);
    resultado = jsonObj.toString();	
  } catch(Exception e) {
    e.printStackTrace();
    out.print(e);
  } finally {
    if (con.hayConexionAbierta())
      con.cierraConexionDB();
  }
}
else if(informacion.equals("textoSuperiorVer")){
  String lsAcuse 				= (request.getParameter("hid_acuse") == null) ? "" : request.getParameter("hid_acuse");
  String lsFechaOperacion 	= (request.getParameter("hid_fecha_oper") == null) ? "" : request.getParameter("hid_fecha_oper");
  String lsImporte 			= (request.getParameter("hid_monto") == null)? "0":request.getParameter("hid_monto");
  String lsTipoOperacion		= (request.getParameter("hid_tipo_plazo") == null)? "":request.getParameter("hid_tipo_plazo");
  String lsMoneda				= (request.getParameter("hid_moneda") == null)? "":request.getParameter("hid_moneda");
  String lsNomMoneda			= (request.getParameter("hid_nom_moneda") == null)? "":request.getParameter("hid_nom_moneda");
  
  if ( lsImporte.equals(""))
    lsImporte = "0.0";
  double total = Double.parseDouble(lsImporte);
  
  String		qrySentencia	= "";
  double 		mtoTotal 		= 0;
  
  StringBuffer seguridadPKI = new StringBuffer();
  String		seguridad		= "";
  String    titulo = "";
  
  try {
    con.conexionDB();
    qrySentencia = 
		" SELECT TO_CHAR (df_convenio, 'dd') dia,"   +
		"        DECODE (TO_CHAR (df_convenio, 'mm'),"   +
		"                '01', 'Enero',"   +
		"                '02', 'Febrero',"   +
		"                '03', 'Marzo',"   +
		"                '04', 'Abril',"   +
		"                '05', 'Mayo',"   +
		"                '06', 'Junio',"   +
		"                '07', 'Julio',"   +
		"                '08', 'Agosto',"   +
		"                '09', 'Septiembre',"   +
		"                '10', 'Octubre',"   +
		"                '11', 'Noviembre',"   +
		"                '12', 'Diciembre'"   +
		"               ) mes,"   +
		"        TO_CHAR (df_convenio, 'YYYY') anyo"   +
		"   FROM comrel_producto_if"   +
		"  WHERE ic_if = ?"   +
		"    AND ic_producto_nafin = ?"  ;
    lVarBind = new ArrayList();
    lVarBind.add(new Integer(iNoCliente));
    lVarBind.add(new Integer(0));
    renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
    String rs_fecha_contrato = "ERROR: Falta Parametrizar Fecha de Contrato";
    if(renglones.size()>0) {
      rs_fecha_contrato = renglones.get(0)+" de "+renglones.get(1)+" de "+renglones.get(2);
    }
    
    qrySentencia = 
      " SELECT per.cg_nombre || ' ' || per.cg_ap_paterno || ' ' || per.cg_ap_materno,"   +
      "        per.cg_puesto"   +
      "   FROM com_personal_facultado per"   +
      "  WHERE per.ic_if = ?"   +
      "    AND per.ic_producto_nafin = ?"   +
      "    AND per.cs_habilitado = ?";
    lVarBind = new ArrayList();
    lVarBind.add(new Integer(iNoCliente));
    lVarBind.add(new Integer(0));
    lVarBind.add("S");
    renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
    String rs_nombre 	= "ERROR: Falta Parametrizar Personal Facultado";
    String rs_puesto 	= "";
    if(renglones.size()>0) {
      rs_nombre 	= renglones.get(0).toString();
      rs_puesto 	= renglones.get(1).toString();
    }
    
    if ("F".equals(lsTipoOperacion)) {
      titulo = "FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO";
      seguridadPKI.append("FORMATO DE CONSTANCIA DE DEPOSITO DE \nDOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO");
    } else {
      titulo = "FORMATO DE CERTIFICADO DE DEPOSITO DE TITULOS DE CREDITO EN ADMINISTRACI�N";
      seguridadPKI.append("FORMATO DE CERTIFICADO DE DEPOSITO DE \nTITULOS DE CREDITO EN ADMINISTRACI�N");
    }
    if ("F".equals(lsTipoOperacion)) {
		seguridad += "De conformidad con el convenio que "+strNombre+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a "+
      "honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.<br /><br />"+
			"La informaci�n del documento en "+lsNomMoneda+" por un importe de $ "+Comunes.formatoDecimal(new Double(lsImporte),2)+" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.<br /><br />"+
			"Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.<br /><br />"+
			"En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.<br /><br />"+
			"As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.<br /><br />";
		 
		seguridadPKI.append("De conformidad con el convenio que "+strNombre+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios\n \n");
		seguridadPKI.append(" y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.\n \n");
		seguridadPKI.append("La informaci�n del documento en "+lsNomMoneda+" por un importe de $ "+Comunes.formatoDecimal(new Double(lsImporte),2)+" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.\n \n");
		seguridadPKI.append("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.\n \n");
		seguridadPKI.append("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.\n \n");
		seguridadPKI.append("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.\n \n");      
	} else {
		seguridad += 
			"Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.<br /><br />"+
			"Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.<br /><br />"+
			"El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.<br /><br />";
    
    seguridadPKI.append("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n \n");
		seguridadPKI.append("Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n \n");
		seguridadPKI.append("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.\n \n");
	} 
  
  seguridad+="<br><br><div style='margin:0 0 0 250px;text-align:left;border-bottom:0px solid #000; width:800px'><b style='float:left;border:1px;width:130px'>Importe Total:</b><span style='float:left;text-align:center;width:200px;margin-left:10px;border-bottom:1px solid'>&nbsp;&nbsp;$&nbsp;"+Comunes.formatoDecimal(total,2,true)+"</span></div>";
  seguridad+="<br><br><div style='margin:0 0 0 250px;text-align:left;border-bottom:0px solid #000; width:800px'><b style='float:left;border:1px;width:130px'>Fecha de Contrato:</b><span style='float:left;text-align:center;width:200px;margin-left:10px;border-bottom:1px solid'>&nbsp;&nbsp;&nbsp;"+rs_fecha_contrato+"</span></div>";
  
  seguridadPKI.append("\n Importe Total: $"+Comunes.formatoDecimal(total,2,true));
  seguridadPKI.append("\n Fecha de Contrato: "+rs_fecha_contrato);
  
  if ("F".equals(lsTipoOperacion)) {
    seguridad+="<br><br><div style='margin:0 0 0 250px;text-align:left;border-bottom:0px solid #000; width:800px'><b style='float:left;border:1px;width:130px'>Fecha del Descuento:</b><span style='float:left;text-align:center;width:200px;margin-left:10px;border-bottom:1px solid'>&nbsp;&nbsp;&nbsp;"+lsFechaOperacion+"</span></div>";  
    seguridadPKI.append("\n Fecha del Descuento: "+lsFechaOperacion);
  }
  
  seguridadPKI.append("\n \n \n");
  seguridadPKI.append("Nombre del Emisor|");
  seguridadPKI.append("Beneficiario|");
  seguridadPKI.append("Clase de Documento|");
  seguridadPKI.append("Lugar de Emisi�n|");
  seguridadPKI.append("Fecha de Emisi�n|");
  seguridadPKI.append("Domicilio de Pago|");
  seguridadPKI.append("Tasa de Interes|");
  seguridadPKI.append("N�mero de Documento|");
  seguridadPKI.append("Fecha de Vencimiento|");
  seguridadPKI.append("Valor Nomina|");
  seguridadPKI.append("N�mero de Pr�stamo|");
  seguridadPKI.append("Amortizaci�n|");
  seguridadPKI.append("Domicilio Pago\n");
  
  /* PKI SECURITY */
    qrySentencia = 
          " SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp pym ce t m)*/"   +
          "           sp.ic_solic_portal,"   +
          "           sp.ig_numero_prestamo, sp.fn_importe_dscto importetotal,"   +
          "           ce.cd_descripcion emisor, sp.cg_lugar_firma lugaremision,"   +
          "           TO_CHAR (sp.df_etc, 'dd/mm/yyyy') fechaemision, sp.cg_domicilio_pago,"   +
          "           TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy') fechavenc,"   +
          "           sp.ig_numero_docto,"   +
          "              TO_CHAR (sp.df_operacion, 'dd')"   +
          "           || ' de '"   +
          "           || DECODE (TO_CHAR (sp.df_operacion, 'mm'),"   +
          "                      '01', 'Enero',"   +
          "                      '02', 'Febrero',"   +
          "                      '03', 'Marzo',"   +
          "                      '04', 'Abril',"   +
          "                      '05', 'Mayo',"   +
          "                      '06', 'Junio',"   +
          "                      '07', 'Julio',"   +
          "                      '08', 'Agosto',"   +
          "                      '09', 'Septiembre',"   +
          "                      '10', 'Octubre',"   +
          "                      '11', 'Noviembre',"   +
          "                      '12', 'Diciembre'"   +
          "                     )"   +
          "           || ' de '"   +
          "           || TO_CHAR (sp.df_operacion, 'YYYY') AS fechaoper,"   +
          "           sp.in_numero_amort, t.cd_nombre tasaif, pym.cg_razon_social nompyme,"   +
          "           cs_tipo_plazo, m.cd_nombre moneda"   +
          "     FROM com_solic_portal sp,"   +
          "          comcat_pyme pym,"   +
          "          comcat_emisor ce,"   +
          "          comcat_tasa t,"   +
          "          comcat_moneda m"   +
          "    WHERE sp.ic_emisor = ce.ic_emisor(+)"   +
          "      AND sp.ic_tasaif = t.ic_tasa"   +
          "      AND sp.ig_clave_sirac = pym.in_numero_sirac(+)"   +
          "      AND sp.ic_moneda = m.ic_moneda(+)"   +
          "      AND sp.ic_if = ?"   +
          "      AND sp.ic_moneda = ?"   +
          "      AND sp.ic_estatus_solic IN (?, ?, ?)"   +
      //		"      AND sp.cc_acuse = ?"   +
          "      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
          "      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
          " ORDER BY 1"  ;
        lVarBind = new ArrayList();
        lVarBind.add(new Integer(iNoCliente));
        lVarBind.add(new Integer(lsMoneda));
        lVarBind.add(new Integer(3));
        lVarBind.add(new Integer(5));
        lVarBind.add(new Integer(6));
      //	lVarBind.add(lsAcuse);
        lVarBind.add(lsFechaOperacion);
        lVarBind.add(lsFechaOperacion);
        renglones = con.consultaDB(qrySentencia, lVarBind, false);
        List reg = new ArrayList();
        HashMap datos;
        for(int i=0;i<renglones.size();i++) { 
          List lDatos = (ArrayList)renglones.get(i);
          String rs_solic_portal		= lDatos.get( 0).toString();
          String rs_numero_prestamo	= lDatos.get( 1).toString();
          String rs_importe_total		= lDatos.get( 2).toString();
          String rs_emisor			= lDatos.get( 3).toString();
          String rs_lugar_emision		= lDatos.get( 4).toString();
          String rs_fecha_emision		= lDatos.get( 5).toString();
          String rs_domicilio_pago	= lDatos.get( 6).toString();
          String rs_fecha_venc		= lDatos.get( 7).toString();
          String rs_numero_docto		= lDatos.get( 8).toString();
          String rs_fecha_oper		= lDatos.get( 9).toString();
          String rs_numero_amort		= lDatos.get(10).toString();
          String rs_tasa_if			= lDatos.get(11).toString();
          String rs_nom_pyme			= lDatos.get(12).toString();
          String rs_tipo_plazo		= lDatos.get(13).toString();
          String rs_moneda			= lDatos.get(14).toString();
          
          seguridadPKI.append(rs_emisor+"|");
          seguridadPKI.append(strNombre+"|");
          seguridadPKI.append("PAGARE|");
          seguridadPKI.append(rs_lugar_emision+"|");
          seguridadPKI.append(rs_fecha_emision+"|");
          seguridadPKI.append(rs_domicilio_pago+"|");
          seguridadPKI.append(rs_tasa_if+"|");
          seguridadPKI.append(rs_numero_docto+"|");
          seguridadPKI.append(rs_fecha_venc+"|");
          seguridadPKI.append("$"+Comunes.formatoDecimal(rs_importe_total,2,true)+"|");
          seguridadPKI.append(rs_numero_prestamo+"|");
          seguridadPKI.append(rs_numero_amort+"|");
          seguridadPKI.append(rs_domicilio_pago+"\n");
        }
  /* END SECURITY */
  seguridadPKI.append("\n \n"+lsNomMoneda+" \nTotal:"+registros+" \n$"+Comunes.formatoDecimal(mtoTotal,2,true));
  seguridadPKI.append("\n \n"+lsFechaOperacion+" \n"+strNombre+" \n"+rs_puesto+" "+rs_nombre);
  
  String piePagina = lsFechaOperacion+"<br />"+strNombre+"<br />"+rs_puesto+" "+rs_nombre;
  
  reg = new ArrayList();
  datos = new HashMap();
  String texto_firmado = seguridadPKI.toString();
  
  System.out.println("\n\n\n\n" + texto_firmado + "\n\n\n");
  
  datos.put("TEXTO_FIRMADO",texto_firmado);
  datos.put("TITULO",titulo);
  datos.put("TEXTO",seguridad);
  datos.put("PIE_PAGINA",piePagina);
  reg.add(datos);
  
  resultado = new String();       
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
  jsonObj.put("registros",reg);
  
  resultado = jsonObj.toString();
  
  }catch(Exception e) {
    e.printStackTrace();
    out.print(e);
  } finally {
    if (con.hayConexionAbierta())
      con.cierraConexionDB();
  }
}

else if(informacion.equals("ConsultaDataGridCertVer")){
  String existe 				= (request.getParameter("existe") == null) ? "" : request.getParameter("existe");
  String lsAcuse 				= (request.getParameter("hid_acuse") == null) ? "" : request.getParameter("hid_acuse");
  String lsFechaOperacion 	= (request.getParameter("hid_fecha_oper") == null) ? "" : request.getParameter("hid_fecha_oper");
  String lsImporte 			= (request.getParameter("hid_monto") == null)? "0":request.getParameter("hid_monto");
  String lsTipoOperacion		= (request.getParameter("hid_tipo_plazo") == null)? "":request.getParameter("hid_tipo_plazo");
  String lsMoneda				= (request.getParameter("hid_moneda") == null)? "":request.getParameter("hid_moneda");
  String lsNomMoneda			= (request.getParameter("hid_nom_moneda") == null)? "":request.getParameter("hid_nom_moneda");
  
  
  if ( lsImporte.equals(""))
    lsImporte = "0.0";
  double total = Double.parseDouble(lsImporte);
  
  String		qrySentencia	= "";
  double 		mtoTotal 		= 0;
  
  String		seguridad		= "";
  String    titulo = "";
  
  try {
    con.conexionDB();
    if(true){
      qrySentencia = 
        " SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp pym ce t m)*/"   +
        "           sp.ic_solic_portal,"   +
        "           sp.ig_numero_prestamo, sp.fn_importe_dscto importetotal,"   +
        "           ce.cd_descripcion emisor, sp.cg_lugar_firma lugaremision,"   +
        "           TO_CHAR (sp.df_etc, 'dd/mm/yyyy') fechaemision, sp.cg_domicilio_pago,"   +
        "           TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy') fechavenc,"   +
        "           sp.ig_numero_docto,"   +
        "              TO_CHAR (sp.df_operacion, 'dd')"   +
        "           || ' de '"   +
        "           || DECODE (TO_CHAR (sp.df_operacion, 'mm'),"   +
        "                      '01', 'Enero',"   +
        "                      '02', 'Febrero',"   +
        "                      '03', 'Marzo',"   +
        "                      '04', 'Abril',"   +
        "                      '05', 'Mayo',"   +
        "                      '06', 'Junio',"   +
        "                      '07', 'Julio',"   +
        "                      '08', 'Agosto',"   +
        "                      '09', 'Septiembre',"   +
        "                      '10', 'Octubre',"   +
        "                      '11', 'Noviembre',"   +
        "                      '12', 'Diciembre'"   +
        "                     )"   +
        "           || ' de '"   +
        "           || TO_CHAR (sp.df_operacion, 'YYYY') AS fechaoper,"   +
        "           sp.in_numero_amort, t.cd_nombre tasaif, pym.cg_razon_social nompyme,"   +
        "           cs_tipo_plazo, m.cd_nombre moneda"   +
        "     FROM com_solic_portal sp,"   +
        "          comcat_pyme pym,"   +
        "          comcat_emisor ce,"   +
        "          comcat_tasa t,"   +
        "          comcat_moneda m"   +
        "    WHERE sp.ic_emisor = ce.ic_emisor(+)"   +
        "      AND sp.ic_tasaif = t.ic_tasa"   +
        "      AND sp.ig_clave_sirac = pym.in_numero_sirac(+)"   +
        "      AND sp.ic_moneda = m.ic_moneda(+)"   +
        "      AND sp.ic_if = ?"   +
        "      AND sp.ic_moneda = ?"   +
        "      AND sp.ic_estatus_solic IN (?, ?, ?)"   +
    //		"      AND sp.cc_acuse = ?"   +
        "      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
        "      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
        " ORDER BY 1"  ;
      lVarBind = new ArrayList();
      lVarBind.add(new Integer(iNoCliente));
      lVarBind.add(new Integer(lsMoneda));
      lVarBind.add(new Integer(3));
      lVarBind.add(new Integer(5));
      lVarBind.add(new Integer(6));
    //	lVarBind.add(lsAcuse);
      lVarBind.add(lsFechaOperacion);
      lVarBind.add(lsFechaOperacion);
      renglones = con.consultaDB(qrySentencia, lVarBind, false);
      List reg = new ArrayList();
      HashMap datos;
      for(int i=0;i<renglones.size();i++) {
        List lDatos = (ArrayList)renglones.get(i);
        String rs_solic_portal		= lDatos.get( 0).toString();
        String rs_numero_prestamo	= lDatos.get( 1).toString();
        String rs_importe_total		= lDatos.get( 2).toString();
        String rs_emisor			= lDatos.get( 3).toString();
        String rs_lugar_emision		= lDatos.get( 4).toString();
        String rs_fecha_emision		= lDatos.get( 5).toString();
        String rs_domicilio_pago	= lDatos.get( 6).toString();
        String rs_fecha_venc		= lDatos.get( 7).toString();
        String rs_numero_docto		= lDatos.get( 8).toString();
        String rs_fecha_oper		= lDatos.get( 9).toString();
        String rs_numero_amort		= lDatos.get(10).toString();
        String rs_tasa_if			= lDatos.get(11).toString();
        String rs_nom_pyme			= lDatos.get(12).toString();
        String rs_tipo_plazo		= lDatos.get(13).toString();
        String rs_moneda			= lDatos.get(14).toString();
        mtoTotal += Double.parseDouble(rs_importe_total);
        
        datos = new HashMap();
        datos.put("RS_EMISOR",rs_emisor);
        datos.put("STRNOMBRE",strNombre);
        datos.put("PAGARE","PAGARE");
        datos.put("RS_LUGAR_EMISION",rs_lugar_emision);
        datos.put("RS_FECHA_EMISION",rs_fecha_emision);
        datos.put("RS_DOMICILIO_PAGO",rs_domicilio_pago);
        datos.put("RS_TASA_IF",rs_tasa_if);
        datos.put("RS_NUMERO_DOCTO",rs_numero_docto);
        datos.put("RS_FECHA_VENC",rs_fecha_venc);
        datos.put("RS_IMPORTE_TOTAL","$ " + Comunes.formatoDecimal(rs_importe_total,2,true));
        datos.put("RS_NUMERO_PRESTAMO",rs_numero_prestamo);
        datos.put("RS_NUMERO_AMORT",rs_numero_amort);
        datos.put("RS_DOMICILIO_PAGO",rs_domicilio_pago);
        reg.add(datos);
        
        registros++;
      }
      if(operacion.equals("TotalesVer")){
        datos = new HashMap();
        reg = new ArrayList();
        datos.put("TOTAL",Integer.toString(registros));
        datos.put("TOTAL_VALOR_NOM", "$ " + Comunes.formatoDecimal(mtoTotal,2,true));
        reg.add(datos);
      }
      resultado = new String();       
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("registros",reg);
      resultado = jsonObj.toString();
    }if(Integer.parseInt(existe)>0){
      String ic_if 				   = iNoCliente; //(request.getParameter("ic_if")==null)?iNoCliente:request.getParameter("ic_if").trim();
      String nombre_if 			   = strNombre;//(request.getParameter("nombre_if")==null)?"":request.getParameter("nombre_if").trim();
      String lsFechaContrato		= (request.getParameter("hid_fecha_contrado") == null)? "":request.getParameter("hid_fecha_contrado");
      String lsNombre				= (request.getParameter("hid_nombre") == null)? "":request.getParameter("hid_nombre");
      String lsPuesto				= (request.getParameter("hid_puesto") == null)? "":request.getParameter("hid_puesto");
      
      if ( lsImporte.equals(""))
        lsImporte = "0.0";
      boolean		bOK 			= true;
      
      CreaArchivo archivo = new CreaArchivo();
      String nombreArchivo = archivo.nombreArchivo()+".pdf";
      String contenidoArchivo="";
      ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
      qrySentencia = 
        " SELECT TO_CHAR (df_convenio, 'dd') dia,"   +
        "        DECODE (TO_CHAR (df_convenio, 'mm'),"   +
        "                '01', 'Enero',"   +
        "                '02', 'Febrero',"   +
        "                '03', 'Marzo',"   +
        "                '04', 'Abril',"   +
        "                '05', 'Mayo',"   +
        "                '06', 'Junio',"   +
        "                '07', 'Julio',"   +
        "                '08', 'Agosto',"   +
        "                '09', 'Septiembre',"   +
        "                '10', 'Octubre',"   +
        "                '11', 'Noviembre',"   +
        "                '12', 'Diciembre'"   +
        "               ) mes,"   +
        "        TO_CHAR (df_convenio, 'YYYY') anyo"   +
        "   FROM comrel_producto_if"   +
        "  WHERE ic_if = ?"   +
        "    AND ic_producto_nafin = ?"  ;
        lVarBind = new ArrayList();
        lVarBind.add(new Integer(iNoCliente));
        lVarBind.add(new Integer(0));
        renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
        String rs_fecha_contrato = "ERROR: Falta Parametrizar Fecha de Contrato";
        if(renglones.size()>0) {
          rs_fecha_contrato = renglones.get(0)+" de "+renglones.get(1)+" de "+renglones.get(2);
        }
        
        qrySentencia = 
          " SELECT per.cg_nombre || ' ' || per.cg_ap_paterno || ' ' || per.cg_ap_materno,"   +
          "        per.cg_puesto"   +
          "   FROM com_personal_facultado per"   +
          "  WHERE per.ic_if = ?"   +
          "    AND per.ic_producto_nafin = ?"   +
          "    AND per.cs_habilitado = ?";
        lVarBind = new ArrayList();
        lVarBind.add(new Integer(iNoCliente));
        lVarBind.add(new Integer(0));
        lVarBind.add("S");
        renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
        String rs_nombre 	= "ERROR: Falta Parametrizar Personal Facultado";
        String rs_puesto 	= "";
        if(renglones.size()>0) {
          rs_nombre 	= renglones.get(0).toString();
          rs_puesto 	= renglones.get(1).toString();
        }
        
        String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
        String diaActual    = fechaActual.substring(0,2);
        String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
        String anioActual   = fechaActual.substring(6,10);
        String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
          pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                          (session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
                          (String)session.getAttribute("sesExterno"),
                          (String) session.getAttribute("strNombre"),
                          (String) session.getAttribute("strNombreUsuario"),
                          (String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));
        
        pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
        
        pdfDoc.addText("\n ");
        if ("F".equals(lsTipoOperacion)) {
          pdfDoc.addText("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO","formasrepB",ComunesPDF.CENTER);
        } else {
          pdfDoc.addText("FORMATO DE CERTIFICADO DE DEPOSITO DE\nTITULOS DE CREDITO EN ADMINISTRACI�N","formasrepB",ComunesPDF.CENTER);
        }
        pdfDoc.addText("\n","formasb",ComunesPDF.CENTER);
        
        if ("F".equals(lsTipoOperacion)) {
          pdfDoc.addText("De conformidad con el convenio que "+nombre_if+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.\n \n", "formasrep");
          pdfDoc.addText("La informaci�n del documento en "+lsNomMoneda+" por un importe de $ "+Comunes.formatoDecimal(new Double(lsImporte),2)+" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.\n \n", "formasrep");
          pdfDoc.addText("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.\n \n", "formasrep");
          pdfDoc.addText("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.\n \n", "formasrep");
          pdfDoc.addText("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.\n \n", "formasrep");
        } else {
          pdfDoc.addText("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n \n", "formasrep");
          pdfDoc.addText("Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n \n", "formasrep");
          pdfDoc.addText("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.\n \n", "formasrep");
        }
        
        pdfDoc.setTable(2, 50);
        pdfDoc.setCell("Importe Total:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
        pdfDoc.setCell("$ "+Comunes.formatoDecimal(total,2),"formasrep", ComunesPDF.LEFT, 1, 1, 0);
        pdfDoc.setCell("Fecha de Contrato:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
        pdfDoc.setCell(rs_fecha_contrato,"formasrep", ComunesPDF.LEFT, 1, 1, 0);
        if ("F".equals(lsTipoOperacion)) {
          pdfDoc.setCell("Fecha del Descuento:","formasB", ComunesPDF.RIGHT, 1, 1, 0);
          pdfDoc.setCell(lsFechaOperacion,"formas", ComunesPDF.LEFT, 1, 1, 0);
        }
        pdfDoc.addTable();
        
        pdfDoc.setLTable(14);
        pdfDoc.setLCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Beneficiario","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Clase de Documento","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Lugar de Emisi�n","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Fecha de Emisi�n","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Domicilio de Pago","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Tasa de Interes","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("N�mero de Documento","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Fecha de Operaci�n","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Fecha de Vencimiento","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Valor Nominal","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Amortizaci�n","celda01rep",ComunesPDF.CENTER);
        pdfDoc.setLCell("Domicilio Pago","celda01rep",ComunesPDF.CENTER);
        
        registros = 0;
        qrySentencia = 
          " SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp pym ce t m)*/"   +
          "           sp.ic_solic_portal,"   +
          "           sp.ig_numero_prestamo, sp.fn_importe_dscto importetotal,"   +
          "           ce.cd_descripcion emisor, sp.cg_lugar_firma lugaremision,"   +
          "           TO_CHAR (sp.df_etc, 'dd/mm/yyyy') fechaemision, sp.cg_domicilio_pago,"   +
          "           TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy') fechavenc,"   +
          "           sp.ig_numero_docto,"   +
      	/*	"              TO_CHAR (sp.df_operacion, 'dd')"   +
          "           || ' de '"   +
          "           || DECODE (TO_CHAR (sp.df_operacion, 'mm'),"   +
          "                      '01', 'Enero',"   +
          "                      '02', 'Febrero',"   +
          "                      '03', 'Marzo',"   +
          "                      '04', 'Abril',"   +
          "                      '05', 'Mayo',"   +
          "                      '06', 'Junio',"   +
          "                      '07', 'Julio',"   +
          "                      '08', 'Agosto',"   +
          "                      '09', 'Septiembre',"   +
          "                      '10', 'Octubre',"   +
          "                      '11', 'Noviembre',"   +
          "                      '12', 'Diciembre'"   +
          "                     )"   +
          "           || ' de '"   +
          "           || TO_CHAR (sp.df_operacion, 'YYYY') AS fechaoper,"   +*/
		"           TO_CHAR (sp.df_operacion, 'dd/mm/yyyy') AS fechaoper,"   +
          "           sp.in_numero_amort, t.cd_nombre tasaif, pym.cg_razon_social nompyme,"   +
          "           cs_tipo_plazo, m.cd_nombre moneda"   +
          "     FROM com_solic_portal sp,"   +
          "          comcat_pyme pym,"   +
          "          comcat_emisor ce,"   +
          "          comcat_tasa t,"   +
          "          comcat_moneda m"   +
          "    WHERE sp.ic_emisor = ce.ic_emisor(+)"   +
          "      AND sp.ic_tasaif = t.ic_tasa"   +
          "      AND sp.ig_clave_sirac = pym.in_numero_sirac(+)"   +
          "      AND sp.ic_moneda = m.ic_moneda(+)"   +
          "      AND sp.ic_if = ?"   +
          "      AND sp.ic_moneda = ?"   +
          "      AND sp.ic_estatus_solic IN (?, ?, ?)"   +
      //		"      AND sp.cc_acuse = ?"   +
          "      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
          "      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
          " ORDER BY 1"  ;
        lVarBind = new ArrayList();
        lVarBind.add(new Integer(iNoCliente));
        lVarBind.add(new Integer(lsMoneda));
        lVarBind.add(new Integer(3));
        lVarBind.add(new Integer(5));
        lVarBind.add(new Integer(6));
      //	lVarBind.add(lsAcuse);
        lVarBind.add(lsFechaOperacion);
        lVarBind.add(lsFechaOperacion);
        renglones = con.consultaDB(qrySentencia, lVarBind, false);
        for(int i=0;i<renglones.size();i++) {
          List lDatos = (ArrayList)renglones.get(i);
          String rs_solic_portal		= lDatos.get( 0).toString();
          String rs_numero_prestamo	= lDatos.get( 1).toString();
          String rs_importe_total		= lDatos.get( 2).toString();
          String rs_emisor			= lDatos.get( 3).toString();
          String rs_lugar_emision		= lDatos.get( 4).toString();
          String rs_fecha_emision		= lDatos.get( 5).toString();
          String rs_domicilio_pago	= lDatos.get( 6).toString();
          String rs_fecha_venc		= lDatos.get( 7).toString();
          String rs_numero_docto		= lDatos.get( 8).toString();
          String rs_fecha_oper		= lDatos.get( 9).toString();
          String rs_numero_amort		= lDatos.get(10).toString();
          String rs_tasa_if			= lDatos.get(11).toString();
          String rs_nom_pyme			= lDatos.get(12).toString();
          String rs_tipo_plazo		= lDatos.get(13).toString();
          String rs_moneda			= lDatos.get(14).toString();
          mtoTotal += Double.parseDouble(rs_importe_total);
          
          pdfDoc.setLCell(rs_emisor,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(nombre_if,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell("PAGARE","formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_lugar_emision,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_fecha_emision,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_tasa_if,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_numero_docto,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_fecha_oper,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell(rs_fecha_venc,"formasrep",ComunesPDF.LEFT);
          pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_importe_total,2),"formasrep",ComunesPDF.RIGHT);
          pdfDoc.setLCell(rs_numero_prestamo,"formasrep",ComunesPDF.RIGHT);
          pdfDoc.setLCell(rs_numero_amort,"formasrep",ComunesPDF.CENTER);
          pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
          registros++;
        }//for(int i=0;i<renglones.size();i++)
        
        if(renglones.size()>0) {
          pdfDoc.setLCell("Total:","celda01rep",ComunesPDF.CENTER);
          pdfDoc.setLCell(String.valueOf(registros),"formasrep",ComunesPDF.CENTER);
          pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,7);
          pdfDoc.setLCell("$ "+Comunes.formatoDecimal(mtoTotal,2),"formasrep",ComunesPDF.RIGHT);
          pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,3);
          
        }
        pdfDoc.addLTable();
        
        pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
        pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
        pdfDoc.addText(nombre_if, "formasrepB", ComunesPDF.CENTER);
        pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
        pdfDoc.addText(rs_puesto, "formasrepB", ComunesPDF.CENTER);
        pdfDoc.addText(rs_nombre, "formasrepB", ComunesPDF.CENTER);
        pdfDoc.endDocument();
        
        List reg = new ArrayList();
        HashMap datos = new HashMap();
        
        datos.put("URL_ARCHIVO",strDirecVirtualTemp+nombreArchivo);
        reg.add(datos);
        
        resultado = new String();       
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("registros", reg);
        resultado = jsonObj.toString();
    }
  }catch(Exception e) {
    e.printStackTrace();
    out.print(e);
  } finally {
    if (con.hayConexionAbierta())
      con.cierraConexionDB();
  }
}
else if(informacion.equals("Confirmar")){
  String lsAcuse 				= (request.getParameter("hid_acuse") == null) ? "" : request.getParameter("hid_acuse");
  String lsFechaOperacion 	= (request.getParameter("hid_fecha_oper") == null) ? "" : request.getParameter("hid_fecha_oper");
  String lsImporte 			= (request.getParameter("hid_monto") == null)? "0":request.getParameter("hid_monto");
  String lsTipoOperacion		= (request.getParameter("hid_tipo_plazo") == null)? "":request.getParameter("hid_tipo_plazo");
  String lsMoneda				= (request.getParameter("hid_moneda") == null)? "":request.getParameter("hid_moneda");
  String lsNomMoneda			= (request.getParameter("hid_nom_moneda") == null)? "":request.getParameter("hid_nom_moneda");
  String lsFechaContrato		= (request.getParameter("hid_fecha_contrado") == null)? "":request.getParameter("hid_fecha_contrado");
  String lsNombre				= (request.getParameter("hid_nombre") == null)? "":request.getParameter("hid_nombre");
  String lsPuesto				= (request.getParameter("hid_puesto") == null)? "":request.getParameter("hid_puesto");
  String respuesta = "";
  String error = "";
  
  if ( lsImporte.equals(""))
    lsImporte = "0.0";
  double total = Double.parseDouble(lsImporte);
  
  String		qrySentencia	= "";
  double 		mtoTotal 		= 0;
  boolean		bOK 			= true;
  
  //	De Seguridad
  String 	pkcs7 = (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
  String 	serial = "";
  String 	folioCert = "";
  String 	externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
  char 	getReceipt = 'Y';
  String 	recibo_e = "500";
  
  String		seguridad		= "";
  
  try {
	con.conexionDB();
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
  //if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
  if (!strSerial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
  //if(true){
	
		folioCert = acuse.toString();
		netropology.utilerias.Seguridad s = new netropology.utilerias.Seguridad();
		
		if (s.autenticar(folioCert, strSerial, pkcs7, externContent, getReceipt)) {
    //if(true){
			recibo_e = s.getAcuse();
		
      qrySentencia = 
        " SELECT COUNT (1)"   +
        "   FROM comrel_constancia_deposito"   +
        "  WHERE ic_if = ?"   +
        "    AND ic_moneda = ?"   +
        "    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
        "    AND df_operacion < TO_DATE (?, 'dd/mm/yyyy') + 1"  ;
      lVarBind = new ArrayList();
      lVarBind.add(new Integer(iNoCliente));
      lVarBind.add(new Integer(lsMoneda));
      lVarBind.add(lsFechaOperacion);
      lVarBind.add(lsFechaOperacion);
      int existe = con.existeDB(qrySentencia, lVarBind);
      
      if(existe == 0) {
        qrySentencia = 
          " INSERT INTO comrel_constancia_deposito"   +
          "             (ic_if, df_operacion, ic_moneda, ic_usuario, cg_nombre_usuario)"   +
          "      VALUES (?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?)"  ;
          lVarBind = new ArrayList();
          lVarBind.add(new Integer(iNoCliente));
          lVarBind.add(lsFechaOperacion);
          lVarBind.add(new Integer(lsMoneda));
          lVarBind.add(iNoUsuario);
          lVarBind.add(strNombre);
        } else {
          qrySentencia = 
            " UPDATE comrel_constancia_deposito"   +
            "    SET df_confirma_if = SYSDATE,"   +
            "        ic_usuario = ?,"   +
            "        cg_nombre_usuario = ?"   +
            "  WHERE ic_if = ?"   +
            "    AND ic_moneda = ?"   +
            "    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
            "    AND df_operacion < TO_DATE (?, 'dd/mm/yyyy') + 1"  ;
          lVarBind = new ArrayList();
          lVarBind.add(iNoUsuario);
          lVarBind.add(strNombre);
          lVarBind.add(new Integer(iNoCliente));
          lVarBind.add(new Integer(lsMoneda));
          lVarBind.add(lsFechaOperacion);
          lVarBind.add(lsFechaOperacion);
        }
        registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
          
        if("F".equals(hidAction)) {
          respuesta = "Se ha enviado�a Nacional Financiera, S.N.C. su aceptaci�n de la Constancia de Dep�sito se�alada";
          error="No";
        }
      }else { //autenticaci�n fallida 
        respuesta = s.mostrarError();
        error="Si";
      }
    }
  }catch(Exception e) {
    bOK = false;
    e.printStackTrace();
    out.print(e);
  } finally {
    con.terminaTransaccion(bOK);
    if (con.hayConexionAbierta())
      con.cierraConexionDB();
  } 
  
  resultado = new String();       
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
  jsonObj.put("error",error);
  jsonObj.put("confirmar",respuesta);
  resultado = jsonObj.toString();
}

else if( informacion.equals("VerPdfConfirmacion") ){
  // String fecha_actual		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String ic_if 				= iNoCliente; //(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
String nombre_if 			= strNombre;//(request.getParameter("nombre_if")==null)?"":request.getParameter("nombre_if").trim();
//String txt_fecha_oper_de 	= (request.getParameter("txt_fecha_oper_de")==null)?fecha_actual:request.getParameter("txt_fecha_oper_de").trim();
//String txt_fecha_oper_a 	= (request.getParameter("txt_fecha_oper_a")==null)?fecha_actual:request.getParameter("txt_fecha_oper_a").trim();
//String lsAcuse 				= (request.getParameter("hid_acuse") == null) ? "" : request.getParameter("hid_acuse");
String lsFechaOperacion 	= (request.getParameter("hid_fecha_oper") == null) ? "" : request.getParameter("hid_fecha_oper");
String lsImporte 			= (request.getParameter("hid_monto") == null)? "0":request.getParameter("hid_monto");
String lsTipoOperacion		= (request.getParameter("hid_tipo_plazo") == null)? "":request.getParameter("hid_tipo_plazo");
String lsMoneda				= (request.getParameter("hid_moneda") == null)? "":request.getParameter("hid_moneda");
String lsNomMoneda			= (request.getParameter("hid_nom_moneda") == null)? "":request.getParameter("hid_nom_moneda");
String lsFechaContrato		= (request.getParameter("hid_fecha_contrado") == null)? "":request.getParameter("hid_fecha_contrado");
String lsNombre				= (request.getParameter("hid_nombre") == null)? "":request.getParameter("hid_nombre");
String lsPuesto				= (request.getParameter("hid_puesto") == null)? "":request.getParameter("hid_puesto");
//String hidAction			= (request.getParameter("hidAction") == null) ? "" : request.getParameter("hidAction");

if ( lsImporte.equals(""))
	lsImporte = "0.0";
double total = Double.parseDouble(lsImporte);

String		qrySentencia	= "";
String		condicion		= "";
lVarBind		= new ArrayList();
renglones		= new ArrayList();
con				= new AccesoDB();
registros		= 0;
double 		mtoTotal 		= 0;
boolean		bOK 			= true;

CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = archivo.nombreArchivo()+".pdf";
String contenidoArchivo="";
ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

try {
	con.conexionDB();
	
	qrySentencia = 
		" SELECT TO_CHAR (df_convenio, 'dd') dia,"   +
		"        DECODE (TO_CHAR (df_convenio, 'mm'),"   +
		"                '01', 'Enero',"   +
		"                '02', 'Febrero',"   +
		"                '03', 'Marzo',"   +
		"                '04', 'Abril',"   +
		"                '05', 'Mayo',"   +
		"                '06', 'Junio',"   +
		"                '07', 'Julio',"   +
		"                '08', 'Agosto',"   +
		"                '09', 'Septiembre',"   +
		"                '10', 'Octubre',"   +
		"                '11', 'Noviembre',"   +
		"                '12', 'Diciembre'"   +
		"               ) mes,"   +
		"        TO_CHAR (df_convenio, 'YYYY') anyo"   +
		"   FROM comrel_producto_if"   +
		"  WHERE ic_if = ?"   +
		"    AND ic_producto_nafin = ?"  ;
	lVarBind = new ArrayList();
	lVarBind.add(new Integer(iNoCliente));
	lVarBind.add(new Integer(0));
	renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
	String rs_fecha_contrato = "ERROR: Falta Parametrizar Fecha de Contrato";
	if(renglones.size()>0) {
		rs_fecha_contrato = renglones.get(0)+" de "+renglones.get(1)+" de "+renglones.get(2);
	}
	
	qrySentencia = 
		" SELECT per.cg_nombre || ' ' || per.cg_ap_paterno || ' ' || per.cg_ap_materno,"   +
		"        per.cg_puesto"   +
		"   FROM com_personal_facultado per"   +
		"  WHERE per.ic_if = ?"   +
		"    AND per.ic_producto_nafin = ?"   +
		"    AND per.cs_habilitado = ?";
	lVarBind = new ArrayList();
	lVarBind.add(new Integer(iNoCliente));
	lVarBind.add(new Integer(0));
	lVarBind.add("S");
	renglones = con.consultaRegDB(qrySentencia, lVarBind, false);
	String rs_nombre 	= "ERROR: Falta Parametrizar Personal Facultado";
	String rs_puesto 	= "";
	if(renglones.size()>0) {
		rs_nombre 	= renglones.get(0).toString();
		rs_puesto 	= renglones.get(1).toString();
	}
	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
										(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
										(String)session.getAttribute("sesExterno"),
										(String) session.getAttribute("strNombre"),
										(String) session.getAttribute("strNombreUsuario"),
										(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.addText("\n ");
	if ("F".equals(lsTipoOperacion)) {
		pdfDoc.addText("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO","formasrepB",ComunesPDF.CENTER);
	} else {
		pdfDoc.addText("FORMATO DE CERTIFICADO DE DEPOSITO DE\nTITULOS DE CREDITO EN ADMINISTRACI�N","formasrepB",ComunesPDF.CENTER);
	}
	pdfDoc.addText("\n","formasb",ComunesPDF.CENTER);
	
	if ("F".equals(lsTipoOperacion)) {
		pdfDoc.addText("De conformidad con el convenio que "+nombre_if+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.\n \n", "formasrep");
		pdfDoc.addText("La informaci�n del documento en "+lsNomMoneda+" por un importe de $ "+Comunes.formatoDecimal(new Double(lsImporte),2)+" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.\n \n", "formasrep");
		pdfDoc.addText("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.\n \n", "formasrep");
		pdfDoc.addText("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.\n \n", "formasrep");
		pdfDoc.addText("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.\n \n", "formasrep");
	} else {
		pdfDoc.addText("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n \n", "formasrep");
		pdfDoc.addText("Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n \n", "formasrep");
		pdfDoc.addText("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.\n \n", "formasrep");
	}
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Importe Total:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.setCell("$ "+Comunes.formatoDecimal(total,2),"formasrep", ComunesPDF.LEFT, 1, 1, 0);
	pdfDoc.setCell("Fecha de Contrato:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
	pdfDoc.setCell(rs_fecha_contrato,"formasrep", ComunesPDF.LEFT, 1, 1, 0);
	if ("F".equals(lsTipoOperacion)) {
		pdfDoc.setCell("Fecha del Descuento:","formasB", ComunesPDF.RIGHT, 1, 1, 0);
		pdfDoc.setCell(lsFechaOperacion,"formas", ComunesPDF.LEFT, 1, 1, 0);
	}
	pdfDoc.addTable();
	
	pdfDoc.setLTable(14);
	pdfDoc.setLCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Beneficiario","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Clase de Documento","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Lugar de Emisi�n","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Fecha de Emisi�n","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Domicilio de Pago","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Tasa de Interes","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("N�mero de Documento","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Fecha de Operaci�n","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Fecha de Vencimiento","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Valor Nominal","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Amortizaci�n","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setLCell("Domicilio Pago","celda01rep",ComunesPDF.CENTER);
	
	registros = 0;
	qrySentencia = 
		" SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp pym ce t m)*/"   +
		"           sp.ic_solic_portal,"   +
		"           sp.ig_numero_prestamo, sp.fn_importe_dscto importetotal,"   +
		"           ce.cd_descripcion emisor, sp.cg_lugar_firma lugaremision,"   +
		"           TO_CHAR (sp.df_etc, 'dd/mm/yyyy') fechaemision, sp.cg_domicilio_pago,"   +
		"           TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy') fechavenc,"   +
		"           sp.ig_numero_docto,"   +
/*		"              TO_CHAR (sp.df_operacion, 'dd')"   +
		"           || ' de '"   +
		"           || DECODE (TO_CHAR (sp.df_operacion, 'mm'),"   +
		"                      '01', 'Enero',"   +
		"                      '02', 'Febrero',"   +
		"                      '03', 'Marzo',"   +
		"                      '04', 'Abril',"   +
		"                      '05', 'Mayo',"   +
		"                      '06', 'Junio',"   +
		"                      '07', 'Julio',"   +
		"                      '08', 'Agosto',"   +
		"                      '09', 'Septiembre',"   +
		"                      '10', 'Octubre',"   +
		"                      '11', 'Noviembre',"   +
		"                      '12', 'Diciembre'"   +
		"                     )"   +
		"           || ' de '"   +
		"           || TO_CHAR (sp.df_operacion, 'YYYY') AS fechaoper,"   +*/
		"           TO_CHAR (sp.df_operacion, 'dd/mm/yyyy') AS fechaoper,"   +
		"           sp.in_numero_amort, t.cd_nombre tasaif, pym.cg_razon_social nompyme,"   +
		"           cs_tipo_plazo, m.cd_nombre moneda"   +
		"     FROM com_solic_portal sp,"   +
		"          comcat_pyme pym,"   +
		"          comcat_emisor ce,"   +
		"          comcat_tasa t,"   +
		"          comcat_moneda m"   +
		"    WHERE sp.ic_emisor = ce.ic_emisor(+)"   +
		"      AND sp.ic_tasaif = t.ic_tasa"   +
		"      AND sp.ig_clave_sirac = pym.in_numero_sirac(+)"   +
		"      AND sp.ic_moneda = m.ic_moneda(+)"   +
		"      AND sp.ic_if = ?"   +
		"      AND sp.ic_moneda = ?"   +
		"      AND sp.ic_estatus_solic IN (?, ?, ?)"   +
//		"      AND sp.cc_acuse = ?"   +
		"      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
		"      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
		" ORDER BY 1"  ;
	lVarBind = new ArrayList();
	lVarBind.add(new Integer(iNoCliente));
	lVarBind.add(new Integer(lsMoneda));
	lVarBind.add(new Integer(3));
	lVarBind.add(new Integer(5));
	lVarBind.add(new Integer(6));
//	lVarBind.add(lsAcuse);
	lVarBind.add(lsFechaOperacion);
	lVarBind.add(lsFechaOperacion);
	renglones = con.consultaDB(qrySentencia, lVarBind, false);
	for(int i=0;i<renglones.size();i++) {
		List lDatos = (ArrayList)renglones.get(i);
		String rs_solic_portal		= lDatos.get( 0).toString();
		String rs_numero_prestamo	= lDatos.get( 1).toString();
		String rs_importe_total		= lDatos.get( 2).toString();
		String rs_emisor			= lDatos.get( 3).toString();
		String rs_lugar_emision		= lDatos.get( 4).toString();
		String rs_fecha_emision		= lDatos.get( 5).toString();
		String rs_domicilio_pago	= lDatos.get( 6).toString();
		String rs_fecha_venc		= lDatos.get( 7).toString();
		String rs_numero_docto		= lDatos.get( 8).toString();
		String rs_fecha_oper		= lDatos.get( 9).toString();
		String rs_numero_amort		= lDatos.get(10).toString();
		String rs_tasa_if			= lDatos.get(11).toString();
		String rs_nom_pyme			= lDatos.get(12).toString();
		String rs_tipo_plazo		= lDatos.get(13).toString();
		String rs_moneda			= lDatos.get(14).toString();
		mtoTotal += Double.parseDouble(rs_importe_total);
		
		pdfDoc.setLCell(rs_emisor,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(nombre_if,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell("PAGARE","formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_lugar_emision,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_fecha_emision,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_tasa_if,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_numero_docto,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_fecha_oper,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell(rs_fecha_venc,"formasrep",ComunesPDF.LEFT);
		pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_importe_total,2),"formasrep",ComunesPDF.RIGHT);
		pdfDoc.setLCell(rs_numero_prestamo,"formasrep",ComunesPDF.RIGHT);
		pdfDoc.setLCell(rs_numero_amort,"formasrep",ComunesPDF.CENTER);
		pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
		registros++;
	}//for(int i=0;i<renglones.size();i++)
	
	if(renglones.size()>0) {
		pdfDoc.setLCell("Total:","celda01rep",ComunesPDF.CENTER);
		pdfDoc.setLCell(String.valueOf(registros),"formasrep",ComunesPDF.CENTER);
		pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,7);
		pdfDoc.setLCell("$ "+Comunes.formatoDecimal(mtoTotal,2),"formasrep",ComunesPDF.RIGHT);
		pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,3);
		
	}
	pdfDoc.addLTable();
	
	pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
	pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
	pdfDoc.addText(nombre_if, "formasrepB", ComunesPDF.CENTER);
	pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
	pdfDoc.addText(rs_puesto, "formasrepB", ComunesPDF.CENTER);
	pdfDoc.addText(rs_nombre, "formasrepB", ComunesPDF.CENTER);
	
	pdfDoc.endDocument();
   } catch(Exception e) {
	bOK = false;
	e.printStackTrace();
	out.print(e);
} finally {
	con.terminaTransaccion(bOK);
	if (con.hayConexionAbierta())
		con.cierraConexionDB();
}

  
  resultado = new String();       
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
  jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
  resultado = jsonObj.toString();

}
%>
<%= resultado%>
