<%@ page import=" com.jspsmart.upload.*, 
com.netro.exception.*,
netropology.utilerias.*,
net.sf.json.JSONObject,
java.util.*,
java.text.*,
com.netro.descuento.*,
com.netro.procesos.*,
com.netro.xls.*,
com.netro.zip.*,
com.jspsmart.upload.SmartUpload"
contentType="text/html; charset=UTF-8"
errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/26credele/26secsession_extjs.jspf"%>
<%
String msgError = "0";
boolean ok_file = true;
AccesoDB con = new AccesoDB();
String ic_proc_solic = "0";
//JSONObject jsonObj		= new JSONObject();
//String infoRegresar = "";
boolean success	= true;
try {

	AutorizacionSolicitud AutSolBean = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	SmartUpload myUpload=new SmartUpload();

	con.conexionDB();

	myUpload.initialize(pageContext);
	myUpload.setTotalMaxFileSize(2097152);
	myUpload.upload();

	int 	numFiles			= 0;
	int 	total				= 0;
	String 	name				= "", 
			value 				= "";
	String 				qrySentencia	= "";
	PreparedStatement	ps				= null;
	ResultSet			rs				= null;
	if(ok_file) { 
		numFiles = myUpload.save(strDirectorioTemp); 
	}
	Enumeration en = myUpload.getRequest().getParameterNames();
	Hashtable hParametros = new Hashtable();
	while (en.hasMoreElements()) {
		name = en.nextElement().toString().trim();
		value = myUpload.getRequest().getParameter(name);
		hParametros.put(name,value);
	}
	String 	TipoPlazo 		= hParametros.get("TipoPlazo").toString();
	String 	TipoArchivo 	= hParametros.get("TipoArchivo").toString();
	String 	NoIf 			= iNoCliente;
	String 	TipoCartera 	= hParametros.get("TipoCartera").toString();
	String 	desc_intermediario 	= hParametros.get("desc_intermediario").toString();	
	boolean lbOK 			= true, 
			bBotonProcesar 	= false;

	Vector vecErrores = new Vector();
	Vector vecProcs   = new Vector();
	
	if(ok_file) {
		/* * * * * * * * * * * Procesamiento del Archivo * * * * * * * * * */
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		String 	ruta 			= strDirectorioTemp+myFile.getFileName();
		String 	nombre 			= myFile.getFileName();
		String 	ext 			= nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length()).toUpperCase();
		Vector 	lovResultado 	= new Vector();
		List 	lArchivos		= new ArrayList();
		if("ZIP".equals(ext)) {
			lArchivos = ComunesZIP.descomprimir(ruta, strDirectorioTemp);
		} else {
			lArchivos.add(nombre);
		}
		for(int i=0;i<lArchivos.size();i++) {
			nombre 	= lArchivos.get(i).toString();
			ext 	= nombre.substring(nombre.lastIndexOf('.') + 1, nombre.length()).toUpperCase();
			if(!("TXT".equals(ext)||"XLS".equals(ext))) {
				throw new Exception("Error, el formato del Archivo no es el correcto. ("+nombre+") ");
/*				error.append("Error, el formato del Archivo no es el correcto. \n");
				ok_file = false;
				continue;*/
			}//if(!("TXT".equals(ext)||"XLS".equals(ext)))
			
			List lDatos = AutSolBean.getDatosCargaMasivaCE(strDirectorioTemp, nombre, ext);
			ic_proc_solic = "0";
			
			qrySentencia = 
				" SELECT seq_comtmp_proc_solic_portal.NEXTVAL"   +
				"   FROM DUAL"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				ic_proc_solic = rs.getString(1)==null?"1":rs.getString(1);
			}
			rs.close();ps.close();
			
			for(int r = 0;r<lDatos.size();r++) {
				String regOK = "S";
				String error = "";
				List lRegistro = (List)lDatos.get(r);
/*System.out.println("_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-");
System.out.println("lRegistro: "+lRegistro);
System.out.println("lRegistro: "+lRegistro.size());*/
				if(lRegistro.size()==0||lRegistro.size()<26) {
					regOK = "N";
					error = "El registro no coincide con el layout. ";
				}//if(lRegistro.size()==0||lRegistro.size()<26)
				
				int longitud = lRegistro.size();

				if("C".equals(TipoPlazo)){
					int longitud2 = longitud-29;
					longitud2 = (longitud2/4);
					longitud2 = (longitud2*4)-lRegistro.size();
					//if( lRegistro.size() > 29  && ( (( lRegistro.size() - 29 ) % 4) != 1 ) ){
					if( (longitud > 29  && ( ((longitud-29)%4) != 0  && longitud2!=1 )) ){
						throw new Exception("Error, el registro no coincide con el layout. ("+nombre+") ");
					}
				}else{
					int longitud2 = longitud-28;
					longitud2 = (longitud2/4);
					longitud2 = (longitud2*4)-lRegistro.size();
					//if( lRegistro.size() > 28  && ( (( lRegistro.size() - 28 ) % 4) != 1 ) ){
					if( (longitud > 28  && ( ((longitud-28)%4) != 0  && longitud2!=1 )) ){
						throw new Exception("Error, el registro no coincide con el layout. ("+nombre+") ");
					}
				}
				
				int indice = 0;
				int size = 0;

				if(ok_file) {
					String claveSirac		= (lRegistro.size()>= ++size)?lRegistro.get( indice).toString():"";//1-0
					String noSucBanco		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//2-1
					String moneda			= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//3-2
					String numDocto			= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//4-3
					String importeDocto		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//5-4
					String importeDscto		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//6-5
					String emisor 			= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//7-6
					String tipoCredito		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//8-7
					String ppc				= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//9-8
					String ppi 				= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//10-9
					String bieneServi 		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//11-10
					String destCredito	 	= ("C".equals(TipoPlazo) && lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//12-11
					String claseDocto 		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";//12-11 o 13-12
					String domicilioPago 	= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String tasaInteresUF 	= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String relMatUF 		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String sobreTasaUF		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String tasaInteresI		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String numAmort			= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String tablaAmort 		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String fppc				= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String fppi				= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String diaPago			= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String fechVencDocto	= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String fechVencDscto	= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String fetc				= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String lugarFirma		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String amortAjuste	 	= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					String tipoRenta		= (lRegistro.size()>= ++size)?lRegistro.get( ++indice).toString():"";
					
					
					
					// eliminamos las comillas de todos los campos y las comas de los numericos
					claveSirac		= Comunes.strtr(Comunes.quitaComitasSimples(claveSirac),"\"","").trim();
					noSucBanco		= Comunes.strtr(Comunes.quitaComitasSimples(noSucBanco),"\"","").trim();
					moneda			= Comunes.strtr(Comunes.quitaComitasSimples(moneda),"\"","").trim();
					numDocto		= Comunes.strtr(Comunes.quitaComitasSimples(numDocto),"\"","").trim();
					importeDocto	= Comunes.strtr(Comunes.quitaComitasSimples(importeDocto),"\"","").trim();
					importeDocto	= Comunes.strtr(Comunes.quitaComitasSimples(importeDocto),",","").trim();
					importeDscto	= Comunes.strtr(Comunes.quitaComitasSimples(importeDscto),"\"","").trim();
					importeDscto	= Comunes.strtr(Comunes.quitaComitasSimples(importeDscto),",","").trim();
					emisor 			= Comunes.strtr(Comunes.quitaComitasSimples(emisor),"\"","").trim();
					tipoCredito		= Comunes.strtr(Comunes.quitaComitasSimples(tipoCredito),"\"","").trim();
					ppc				= Comunes.strtr(Comunes.quitaComitasSimples(ppc),"\"","").trim();
					ppi 			= Comunes.strtr(Comunes.quitaComitasSimples(ppi),"\"","").trim();
					bieneServi 		= Comunes.strtr(Comunes.quitaComitasSimples(bieneServi),"\"","").trim();
					claseDocto 		= Comunes.strtr(Comunes.quitaComitasSimples(claseDocto),"\"","").trim();
					domicilioPago 	= Comunes.strtr(Comunes.quitaComitasSimples(domicilioPago),"\"","").trim();
					tasaInteresUF 	= Comunes.strtr(Comunes.quitaComitasSimples(tasaInteresUF),"\"","").trim();
					relMatUF 		= Comunes.strtr(Comunes.quitaComitasSimples(relMatUF),"\"","").trim();
					sobreTasaUF		= Comunes.strtr(Comunes.quitaComitasSimples(sobreTasaUF),"\"","").trim();
					tasaInteresI	= Comunes.strtr(Comunes.quitaComitasSimples(tasaInteresI),"\"","").trim();
					numAmort		= Comunes.strtr(Comunes.quitaComitasSimples(numAmort),"\"","").trim();
					tablaAmort 		= Comunes.strtr(Comunes.quitaComitasSimples(tablaAmort),"\"","").trim();
					fppc			= Comunes.strtr(Comunes.quitaComitasSimples(fppc),"\"","").trim();
					fppi			= Comunes.strtr(Comunes.quitaComitasSimples(fppi),"\"","").trim();
					diaPago			= Comunes.strtr(Comunes.quitaComitasSimples(diaPago),"\"","").trim();
					fechVencDocto	= Comunes.strtr(Comunes.quitaComitasSimples(fechVencDocto),"\"","").trim();
					fechVencDscto	= Comunes.strtr(Comunes.quitaComitasSimples(fechVencDscto),"\"","").trim();
					fetc			= Comunes.strtr(Comunes.quitaComitasSimples(fetc),"\"","").trim();
					lugarFirma		= Comunes.strtr(Comunes.quitaComitasSimples(lugarFirma),"\"","").trim();
					amortAjuste	 	= Comunes.strtr(Comunes.quitaComitasSimples(amortAjuste),"\"","").trim();
					tipoRenta		= Comunes.strtr(Comunes.quitaComitasSimples(tipoRenta),"\"","").trim();
					destCredito 	= Comunes.strtr(Comunes.quitaComitasSimples(destCredito),"\"","").trim();

					AutSolBean.setDatosCargaMasivaCE(
						claveSirac,		noSucBanco,		moneda,			numDocto,		importeDocto,
						importeDscto,	emisor,			tipoCredito,	ppc,			ppi,
						bieneServi,		claseDocto,		domicilioPago,	tasaInteresUF,	relMatUF,
						sobreTasaUF,	tasaInteresI,	numAmort,		tablaAmort,		fppc,
						fppi,			diaPago,		fechVencDocto,	fechVencDscto,	fetc,
						lugarFirma,		amortAjuste,	tipoRenta, 	destCredito, TipoPlazo,
						ic_proc_solic,	lRegistro, 
						regOK,			error);
					
				}//if(ok_file)
			}//for(int r = 0;r<lDatos.size();r++)

			List lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_proc_solic));
			lVarBind.add(new Integer(NoIf));
			lVarBind.add(TipoPlazo);
			new CicloBackGroundSP("sp_carga_masiva_credele", lVarBind).start();
		}//for(int i=0;i<lArchivos.size();i++)
	}//if(ok_file)
} catch(SecurityException se) {
	se.printStackTrace();
	success = false;
	msgError = "Error. El Archivo es muy Grande; excede el L�mite que es de 2 MB.<br>Exception:"+se; 
} catch(Exception e) {
	e.printStackTrace();
	msgError = e.getMessage();
	success = false;
} finally {
	if(con.hayConexionAbierta()){
			con.cierraConexionDB();
	}
	//jsonObj.put("msg", msgError);
	//jsonObj.put("ic_proc_solic", ic_proc_solic);
	//jsonObj.put("success", new Boolean(success));
	//infoRegresar = jsonObj.toString();
}
%>
{
	"success":	<%=(success)?"true":"false"%>,
	"ic_proc_solic":	'<%=ic_proc_solic%>',
	"msg":	'<%=msgError%>'
}
