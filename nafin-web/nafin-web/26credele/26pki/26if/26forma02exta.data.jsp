<%@ page
	import="
		java.util.*,		
		java.text.*,
		java.math.BigDecimal,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf"%>

<%
	ParametrosRequest req = null;
	JSONObject jsonObj = new JSONObject();
	if (ServletFileUpload.isMultipartContent(request)) {

		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();

		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		String fechaLimite	= (req.getParameter("fechaLimite") == null)? "" : req.getParameter("fechaLimite");
		String ImporteDocto	= (req.getParameter("_importDocto_") == null)? "" : req.getParameter("_importDocto_");

		String itemArchivo = "";
		List contenidoArchivo = new ArrayList();
		BigDecimal impAmort = new BigDecimal("0.00");
		BigDecimal impRecor = new BigDecimal("0.00");
		try{
			int carga = 0;
			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoInput = fItem.getInputStream();

			String ruta = strDirectorioTemp + itemArchivo;
			//contenidoArchivo = this.contenidoArchivo(ruta);
			contenidoArchivo = this.contenidoArchivo(archivoInput);
			carga = contenidoArchivo.size();
			int i = 0;
			List nuevoContenido = new ArrayList();
			Iterator itera = contenidoArchivo.iterator();
			while(itera.hasNext()){
				List linea = (List)itera.next();
				i = Integer.parseInt((String)linea.get(0));
				String fVenc = (String)linea.get(1);
				String tipAmrt = (String)linea.get(2);
				String imporAmt = (String)linea.get(3);

				if(Comunes.restaFechas(fVenc, fechaLimite) >= 0){
					tipAmrt = "R";
				}

				HashMap hm = new HashMap();
				hm.put("NO_AMORT",new Integer(i));
				hm.put("FECHA_VENC",	fVenc);
				hm.put("TIPO_AMORT",	tipAmrt);

				if(tipAmrt.equalsIgnoreCase("I")){
					imporAmt = "0";
				}
				impAmort = impAmort.add(new BigDecimal(imporAmt));

				if(tipAmrt.equalsIgnoreCase("R")){
					impRecor = impRecor.add(new BigDecimal(imporAmt));
				}

				hm.put("IMPORTE",		imporAmt);
				nuevoContenido.add(hm);
			}
			if (nuevoContenido != null){
				String impCap = Comunes.formatoDecimal(new BigDecimal(ImporteDocto.replaceAll(",", "")).subtract(impAmort).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString(), 2);
				JSONArray jsObjArr = new JSONArray();
				jsObjArr = JSONArray.fromObject(nuevoContenido);
				jsonObj.put("impAmortCap", (impAmort.setScale(2, BigDecimal.ROUND_HALF_UP)).toPlainString());
				jsonObj.put("importeAmorRec", (impRecor.setScale(2, BigDecimal.ROUND_HALF_UP)).toPlainString());
				jsonObj.put("importeCapturar", impCap);
				jsonObj.put("nuevoContenido", jsObjArr);
			}
			jsonObj.put("carga",new Integer(carga));

		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}

%>
<%!
	//private List contenidoArchivo(String rutaArchivo) throws AppException{
	private List contenidoArchivo(InputStream archivo) throws AppException{
		List contenidoArchivo = new ArrayList();
		String linea = "";
		String numeroAmortizacion = "";
		String fechaVencimiento = "";
		String tipoAmortizacion = "";
		String importeAmortizacion = "";
		
		try{
			//InputStream inputStream = new FileInputStream(rutaArchivo);
			BufferedReader breader = new BufferedReader(new InputStreamReader(archivo));
			
			while((linea = breader.readLine()) != null){
				StringTokenizer tokens = new StringTokenizer(linea, "|");
				
				if(tokens.countTokens() == 4){
					numeroAmortizacion 	= tokens.nextToken("|");
					fechaVencimiento 		= tokens.nextToken("|");
					tipoAmortizacion 		= tokens.nextToken("|");
					importeAmortizacion = tokens.nextToken("|");
					
					List contenidoLinea = new ArrayList();
					contenidoLinea.add(numeroAmortizacion == null?"":numeroAmortizacion.trim());
					contenidoLinea.add(fechaVencimiento == null?"":fechaVencimiento.trim());
					contenidoLinea.add(tipoAmortizacion == null?"":tipoAmortizacion.trim());
					contenidoLinea.add(importeAmortizacion == null?"":importeAmortizacion.trim());
					contenidoArchivo.add(contenidoLinea);
				}else{
					List contenidoLinea = new ArrayList();
					contenidoLinea.add("0");
					contenidoLinea.add(linea);
					contenidoArchivo.add(contenidoLinea);
				}
			}
			//inputStream.close();
		}catch(Exception e){
			System.out.println("::: Error leyendo archivo de carga :: "+e.toString());
			e.printStackTrace();
			throw new AppException("Error al leer el archivo...");
		}finally{}
		return contenidoArchivo;
	}
%>
{"success": true,"infoRegresar":<%=jsonObj.toString()%>}
