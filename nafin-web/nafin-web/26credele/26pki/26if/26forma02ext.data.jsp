<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		java.text.*,
		java.math.*,
		com.netro.exception.*,
		netropology.utilerias.Seguridad,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoCredCons,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoTasa,
		com.netro.distribuidores.*,
		com.netro.afiliacion.*,
		com.netro.descuento.*,
		com.netro.credito.*,
		com.netro.procesos.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf"%>
<%
///////
String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{

	JSONObject jsonObj= new JSONObject();
	boolean success	= true;
	String msg			= "";

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	
	try{
		manejoServicio.getEstadoDelServicioIfCE(iNoCliente);
	}catch(NafinException e){
		String codError = e.getCodError();
		if (codError.equals("DSCT0048")){
			msg = "Lo sentimos esta usted fuera de los rangos de horarios establecidos.";
		}else if(codError.equals("HORA0009")){
			msg = "Problemas al revisar el horario de servicio del IF de Credito Electronico.";
		}else{
			success = false;
		}
	}
	

	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	int tipoPiso = BeanAfiliacion.getTipoPiso(iNoCliente);
	jsonObj.put("tipoCartera", new Integer(tipoPiso));

	CargaCredito BeanCargaCredito = ServiceLocator.getInstance().lookup("CargaCreditoEJB", CargaCredito.class);

	List plaz = new ArrayList();
	String tipo = "";
	int plazos=0;
	plaz = BeanCargaCredito.getPlazo(iNoCliente);
	Iterator iter = plaz.iterator();
	while (iter.hasNext()){
		tipo = (String)iter.next();
		plazos++;
	}
	jsonObj.put("tipo", tipo);
	jsonObj.put("plazos", new Integer(plazos) );
	
	String mensajeBloqueo = (strTipoUsuario.equals("IF") && autSolEJB.esIfBloqueado(iNoCliente,"0"))?
			"Las operaciones enviadas estarán detenidas por el Usuario Administrador":
			"";
	jsonObj.put("mensajeBloqueo", mensajeBloqueo);

	jsonObj.put("NoIf", iNoCliente);
	jsonObj.put("msg", msg );
	jsonObj.put("success", new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoMonedaDist")){

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoCredito")){

	String TipoCartera	=	(request.getParameter("TipoCartera")!=null)?request.getParameter("TipoCartera"):"";
	String TipoPlazo	=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";

	CatalogoCredCons cat = new CatalogoCredCons();
	cat.setCampoClave("ic_tipo_credito");
	cat.setCampoDescripcion("ic_tipo_credito||' - '||t.cd_descripcion");
	cat.setHabilitado("S");
	cat.setIntermediario(iNoCliente);
	cat.setPiso(TipoCartera);
	cat.setPlazo(TipoPlazo);
	cat.setOrden("t.ic_tipo_credito");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoNombreEmisor")){

	String TipoCartera=	(request.getParameter("TipoCartera")!=null)?request.getParameter("TipoCartera"):"";
	String TipoPlazo	=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";
	String TipoCredito	=	(request.getParameter("TipoCredito")!=null)?request.getParameter("TipoCredito"):"";

	CatalogoCredCons cat = new CatalogoCredCons();
	cat.setCampoClave("ic_emisor");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setIntermediario(iNoCliente);
	cat.setPiso(TipoCartera);
	cat.setPlazo(TipoPlazo);
	cat.setCredito(TipoCredito);
	cat.setOrden("cd_descripcion");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTasaBaseI")){

	String TipoCartera=	(request.getParameter("TipoCartera")!=null)?request.getParameter("TipoCartera"):"";
	String TipoPlazo	=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";
	String TipoCredito=	(request.getParameter("TipoCredito")!=null)?request.getParameter("TipoCredito"):"";
	String emisor		=	(request.getParameter("emisor")!=null)?request.getParameter("emisor"):"";

	CatalogoCredCons cat = new CatalogoCredCons();
	cat.setCampoClave("ic_tasa");
	cat.setCampoDescripcion("ic_tasa||' - '||t.cd_nombre");
	cat.setIntermediario(iNoCliente);
	cat.setPiso(TipoCartera);
	cat.setPlazo(TipoPlazo);
	cat.setCredito(TipoCredito);
	cat.setEmisor(emisor);
	cat.setHabilitado("S");
	cat.setOrden("t.ic_tasa");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoAmortizacion")){

	String TipoPlazo	=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";
	String TipoCartera=	(request.getParameter("TipoCartera")!=null)?request.getParameter("TipoCartera"):"";
	String TipoCredito=	(request.getParameter("TipoCredito")!=null)?request.getParameter("TipoCredito"):"";
	String emisor		=	(request.getParameter("emisor")!=null)?request.getParameter("emisor"):"";
	String tasaI		=	(request.getParameter("tasaI")!=null)?request.getParameter("tasaI"):"";

	CatalogoCredCons cat = new CatalogoCredCons();
	cat.setCampoClave("ic_tabla_amort");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setIntermediario(iNoCliente);
	cat.setPiso(TipoCartera);
	cat.setPlazo(TipoPlazo);
	cat.setCredito(TipoCredito);
	cat.setEmisor(emisor);
	cat.setTasa(tasaI);
	cat.setOrden("t.ic_tabla_amort");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoCapital")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_periodicidad");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_periodicidad");
	cat.setOrden("ic_periodicidad");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoIntereses")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_periodicidad");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_periodicidad");
	cat.setOrden("ic_periodicidad");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoDocumento")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_clase_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_clase_docto");
	cat.setOrden("ic_clase_docto");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTasa")){

	CatalogoTasa cat = new CatalogoTasa();
	cat.setCampoClave("ic_tasa");
	cat.setCampoDescripcion("ic_tasa||' - '||cd_nombre");
	cat.setHabilitado("S");
	cat.setOrden("ic_tasa");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("obtieneNombre")){

	String clienteSirac	=	(request.getParameter("clienteSirac")!=null)?request.getParameter("clienteSirac"):"";

	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	
	String nombre = BeanAfiliacion.getNombrePymeSirac(clienteSirac);

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("Nombre", nombre);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("plazosBaseOperacion")){

	String TipoPlazo		=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";
	String TipoCartera	=	(request.getParameter("TipoCartera")!=null)?request.getParameter("TipoCartera"):"";
	String TipoCredito	=	(request.getParameter("TipoCredito")!=null)?request.getParameter("TipoCredito"):"";
	String emisor			=	(request.getParameter("emisor")!=null)?request.getParameter("emisor"):"";
	String tasaI			=	(request.getParameter("tasaI")!=null)?request.getParameter("tasaI"):"";
	String Amortiza		=	(request.getParameter("Amortiza")!=null)?request.getParameter("Amortiza"):"";
	String iPeriodicidad	=	(request.getParameter("ppcapital")!=null)?request.getParameter("ppcapital"):"";
	String iTipoRenta		=	(request.getParameter("tipoRenta")!=null)?request.getParameter("tipoRenta"):"";

	CargaCredito BeanCargaCredito = ServiceLocator.getInstance().lookup("CargaCreditoEJB", CargaCredito.class);
	Hashtable datos  = new Hashtable();
	datos = BeanCargaCredito.getPlazoBaseOperacion(TipoPlazo,TipoCartera,iNoCliente,tasaI, TipoCredito,	Amortiza, emisor, iPeriodicidad, iTipoRenta);

	String valida		= (String)datos.get("Valida");
	String FecVenDMax	= (String)datos.get("FecVenDMax");
	String plazoMax	= (String)datos.get("plazoMax");
	boolean codigo		=  "true".equals((String)datos.get("puedeUsarCodigoBaseOperacionDinamica"))?true:false;
	String FechaActual = (new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("Valida", valida);
	jsonObj.put("FechaActual", FechaActual);
	jsonObj.put("FecVenDMax", FecVenDMax);
	jsonObj.put("plazoMax", plazoMax);
	jsonObj.put("puedeUsarCodigoBO", new Boolean(codigo));
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("NoAmortizaciones")){

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	JSONObject jsonObj = new JSONObject();

	String TipoPlazo		=	(request.getParameter("TipoPlazo")!=null)?request.getParameter("TipoPlazo"):"";
	String tasaI			=	(request.getParameter("tasaI")!=null)?request.getParameter("tasaI"):"";
	String TipoCredito	=	(request.getParameter("TipoCredito")!=null)?request.getParameter("TipoCredito"):"";
	String Amortiza		=	(request.getParameter("Amortiza")!=null)?request.getParameter("Amortiza"):"";
	String emisor			=	(request.getParameter("emisor")!=null)?request.getParameter("emisor"):"";
	String moneda			=	(request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";

	if("0".equals(emisor)){
		emisor = "";
	}
	String fechaLimite = autSolEJB.getFechaMaxRecortado(TipoPlazo, iNoCliente, tasaI, TipoCredito,Amortiza, emisor, moneda);

	jsonObj.put("fechaLimite", fechaLimite);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Continuar")){

	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	String NoIf = iNoCliente;
//pendiente. . . String Plazo = (request.getParameter("Plazo")==null)?"":request.getParameter("Plazo").trim();
	String TipoPlazo                   = (request.getParameter("TipoPlazo")                   ==null)?("NAFIN".equals(strTipoUsuario)?"F":"C"):request.getParameter("TipoPlazo").trim();
	String NoClienteSIRAC              = (request.getParameter("NoClienteSIRAC")              ==null)?"":request.getParameter("NoClienteSIRAC").trim();
	String Nombre                      = (request.getParameter("Nombre")                      ==null)?"":request.getParameter("Nombre").trim();
	String NoSucBanco                  = (request.getParameter("NoSucBanco")                  ==null)?"":request.getParameter("NoSucBanco").trim();
	String Moneda                      = (request.getParameter("Moneda")                      ==null)?"":request.getParameter("Moneda").trim();
	String NoDocumento                 = (request.getParameter("NoDocumento")                 ==null)?"":request.getParameter("NoDocumento").trim();
	String ImporteDocto                = (request.getParameter("importeDocto_")               ==null)?"":request.getParameter("importeDocto_").trim();
	String ImporteDscto                = (request.getParameter("ImporteDscto")                ==null)?"":request.getParameter("ImporteDscto").trim();
	String Emisor                      = (request.getParameter("Emisor_")                     ==null)?"":request.getParameter("Emisor_").trim();
	String TipoCredito                 = (request.getParameter("TipoCredito_")                ==null)?"":request.getParameter("TipoCredito_").trim();
	String PPCapital                   = (request.getParameter("PPCapital")                   ==null)?"":request.getParameter("PPCapital").trim();
	String PPIntereses                 = (request.getParameter("PPIntereses")                 ==null)?"":request.getParameter("PPIntereses").trim();
	String BienesServicios             = (request.getParameter("BienesServicios")             ==null)?"":request.getParameter("BienesServicios").trim();
	String ClaseDocto                  = (request.getParameter("ClaseDocto_")                 ==null)?"":request.getParameter("ClaseDocto_").trim();
	String DomicilioPago               = (request.getParameter("DomicilioPago")               ==null)?"":request.getParameter("DomicilioPago").trim();
	String TasaUF                      = (request.getParameter("TasaUF_")                     ==null)?"":request.getParameter("TasaUF_").trim();
	String RelMatUF                    = (request.getParameter("RelMatUF_")                   ==null)?"":request.getParameter("RelMatUF_").trim();
	String SobreTasaUF                 = (request.getParameter("SobreTasaUF_")                ==null)?"":request.getParameter("SobreTasaUF_").trim();
	String TasaI                       = (request.getParameter("TasaI")                       ==null)?"":request.getParameter("TasaI").trim();
	String RelMatI                     = (request.getParameter("RelMatI")                     ==null)?"":request.getParameter("RelMatI").trim();
	String SobreTasaI                  = (request.getParameter("SobreTasaI")                  ==null)?"":request.getParameter("SobreTasaI").trim();
	String NoAmortizaciones            = (request.getParameter("NoAmortizaciones_")           ==null)?"0":request.getParameter("NoAmortizaciones_").trim();
	String Amortizacion                = (request.getParameter("Amortizacion_")               ==null)?"":request.getParameter("Amortizacion_").trim();
	String FechaPPC                    = (request.getParameter("FechaPPC_")                   ==null)?"":request.getParameter("FechaPPC_").trim();
	String DiaPago                     = (request.getParameter("DiaPago_")                    ==null)?"":request.getParameter("DiaPago_").trim();
	String FechaPPI                    = (request.getParameter("FechaPPI_")                   ==null)?"":request.getParameter("FechaPPI_").trim();
	String FechaVencDocto              = (request.getParameter("FechaVencDocto_")             ==null)?"":request.getParameter("FechaVencDocto_").trim();
	String FechaVencDscto              = (request.getParameter("FechaVencDscto_")             ==null)?"":request.getParameter("FechaVencDscto_").trim();
	String FechaEmisionTC              = (request.getParameter("FechaEmisionTC_")             ==null)?"":request.getParameter("FechaEmisionTC_").trim();
	String LugarFirma                  = (request.getParameter("LugarFirma_")                 ==null)?"":request.getParameter("LugarFirma_").trim();
	String AmortizacionAjuste          = (request.getParameter("AmortizacionAjuste_")         ==null)?"":request.getParameter("AmortizacionAjuste_").trim();
	String ImporAmortAjuste            = (request.getParameter("ImporAmortAjuste_")           ==null)?"":request.getParameter("ImporAmortAjuste_").trim();
	String FechaAmortAjuste            = (request.getParameter("FechaAmortAjuste_")           ==null)?"":request.getParameter("FechaAmortAjuste_").trim();
	String TipoRenta                   = (request.getParameter("TipoRenta")                   ==null)?"":request.getParameter("TipoRenta").trim();
	String codigoBaseOperacionDinamica = (request.getParameter("codigoBaseOperacionDinamica") ==null)?"":request.getParameter("codigoBaseOperacionDinamica").trim();
	String destinoCredito              = (request.getParameter("DestinoCredito")              ==null)?"":request.getParameter("DestinoCredito").trim();

	if("0".equals(Emisor)){
		Emisor = "";
	}

	String icAmort[];
	String fvencAmort[];
	String tipoAmort[];
	String montoAmort[];

	icAmort    = request.getParameterValues("icAmort");
	fvencAmort = request.getParameterValues("fvencAmort");
	tipoAmort  = request.getParameterValues("tipoAmort");
	montoAmort = request.getParameterValues("montoAmort");

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	java.util.Date fechaHoy = new java.util.Date();
	String diaHoy  = new java.text.SimpleDateFormat("dd").format(fechaHoy);
	int mesAct     = Integer.parseInt(new java.text.SimpleDateFormat("MM").format(fechaHoy));
	String anioAct = new java.text.SimpleDateFormat("yyyy").format(fechaHoy);
	mesAct--;
	String nombre_personal  = "";
	String paterno_personal = "";
	String materno_personal = "";
	String puesto_personal  = "";

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	int numAmort = Integer.parseInt(NoAmortizaciones);
	int i = 0;
	int numAmortRec = 0;
	
	if(tipoAmort!=null){
		for(i=0;i<tipoAmort.length;i++){
			if("R".equals(tipoAmort[i])&&Double.parseDouble(montoAmort[i])>0)
				numAmortRec++;
		}
	}

	int    numDoctosMN  = 0;
	int    numDoctosUSD = 0;
	double totMontoMN   = 0;
	double totMontoUSD  = 0;

	Hashtable hsres = autSolEJB.ohProcesaOperacSolicTmp(TipoPlazo,NoIf, NoClienteSIRAC, Nombre,
											NoSucBanco, Moneda, NoDocumento, ImporteDocto,
											ImporteDscto,Emisor, TipoCredito,
											PPCapital, PPIntereses, BienesServicios,
											ClaseDocto, DomicilioPago, TasaUF, RelMatUF,
											SobreTasaUF, TasaI, RelMatI, SobreTasaI, 
											NoAmortizaciones, Amortizacion, FechaPPC,
											DiaPago, FechaPPI, FechaVencDocto,
											FechaVencDscto, FechaEmisionTC, LugarFirma,
											AmortizacionAjuste, ImporAmortAjuste, 
											FechaAmortAjuste,TipoRenta,numAmortRec,
											fvencAmort,tipoAmort,montoAmort,"","","","","", // Nota: El campo producto debe ir vacio, de lo
																											// contrario se afectará como se lee la parametrizacion
																											// del campo com_base_op_credito.cs_base_op_dinamica
											codigoBaseOperacionDinamica, destinoCredito);
	String       msgError      = (String)hsres.get("lsbError");
	String       icProcSolic   = (String)hsres.get("NoProcSolic");
	StringBuffer seguridad     = new StringBuffer("");
	String       fechaConvenio = "";
	String       lugarFirma    = "";

	seguridad.append(
		"Nombre del Cliente|Numero Sirac|Numero de Documento|Fecha Emision titulo de Credito|Fecha de vencimiento dscto|Moneda|Tipo de credito|Tasa Intermediario Financiero|"+
		"Numero de Amortizaciones|Tabla Amortizacion|Monto del Documento|Monto del Descuento\n"
	);
	
	List newReg = new ArrayList();
	List newReg_b = new ArrayList();
	
	if ( "".equals(icProcSolic)||icProcSolic==null) icProcSolic = "0";
	Vector lovDatos = autSolEJB.getDoctosCargadosTmp(Integer.parseInt(icProcSolic));
	Vector lovRegistro = null;
	for(i = 0; i < lovDatos.size(); i++) {
		HashMap hm = new HashMap();
		HashMap hmB = new HashMap();
		lovRegistro = (Vector) lovDatos.elementAt(i);
		hm.put("NOMBRE_CLI",		lovRegistro.elementAt(0));
		hm.put("NO_SIRAC",		lovRegistro.elementAt(1));
		hm.put("NO_DOCTO",		lovRegistro.elementAt(3));
		hm.put("FECHA_EMISION",	lovRegistro.elementAt(8));
		hm.put("FECHA_VENC",		lovRegistro.elementAt(7));
		hm.put("MONEDA",			lovRegistro.elementAt(2));
		hm.put("TIPO_CREDITO",	lovRegistro.elementAt(6));
		hm.put("TASA_IF",			lovRegistro.elementAt(10));
		hm.put("NO_AMORTIZA",	lovRegistro.elementAt(11));
		hm.put("TABLA_AMORTIZA",lovRegistro.elementAt(9));
		hm.put("MONTO_DOCTO",	"$ "+lovRegistro.elementAt(4));
		hm.put("MONTO_DESC",		"$ "+lovRegistro.elementAt(5));

		hmB.put("NOMBRE_EMISOR",lovRegistro.elementAt(("C").equals(TipoPlazo)?0:12));
		hmB.put("CLASE_DOCTO",	lovRegistro.elementAt(13));
		hmB.put("LUGAR",			lovRegistro.elementAt(14));
		hmB.put("FECHA_EMISION",lovRegistro.elementAt(8));
		hmB.put("DOMICILIO",		lovRegistro.elementAt(15));
		hmB.put("TASA_INTERES",	lovRegistro.elementAt(10));
		hmB.put("NO_DOCTO",		lovRegistro.elementAt(3));
		hmB.put("FECHA_VENC",	lovRegistro.elementAt(7));
		hmB.put("MONTO_OPERA",	"$ "+lovRegistro.elementAt(5));
		hmB.put("DEST_CREDITO",	lovRegistro.elementAt(25));
		newReg.add(hm);
		newReg_b.add(hmB);

		seguridad.append(
			lovRegistro.elementAt(0)+"|"+lovRegistro.elementAt(1)+"|"+lovRegistro.elementAt(3)+"|"+
			lovRegistro.elementAt(8)+"|"+lovRegistro.elementAt(7)+"|"+lovRegistro.elementAt(2)+"|"+
			lovRegistro.elementAt(6)+"|"+lovRegistro.elementAt(10)+"|"+lovRegistro.elementAt(11)+"|"+
			lovRegistro.elementAt(9)+"|"+lovRegistro.elementAt(4)+"|"+lovRegistro.elementAt(5)+"\\n"
		);
		if(i==0){
			fechaConvenio	= (String)lovRegistro.get(18);
			lugarFirma		= (String)lovRegistro.get(14);
		}
		if("1".equals(lovRegistro.elementAt(19).toString())){
			numDoctosMN++;
			totMontoMN += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}else if("54".equals(lovRegistro.elementAt(19).toString())){
			numDoctosUSD++;
			totMontoUSD += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}
		
		puesto_personal = (String)lovRegistro.get(21);
		nombre_personal = (String)lovRegistro.get(22);
		paterno_personal = (String)lovRegistro.get(23);
		materno_personal = (String)lovRegistro.get(24);
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("icProcSolic", icProcSolic);
	jsonObj.put("strNombre", strNombre);
	jsonObj.put("fechaConvenio", fechaConvenio);
	jsonObj.put("diaHoy", diaHoy);
	jsonObj.put("mesAct", meses[mesAct]);
	jsonObj.put("anioAct", anioAct);

	if (newReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(newReg);
		jsonObj.put("registros", jsObjArray);
		jsonObj.put("txtFirmar", seguridad.toString());
	}

	if (newReg_b != null){
		JSONArray jsObjArray_b = new JSONArray();
		jsObjArray_b = JSONArray.fromObject(newReg_b);
		jsonObj.put("registros_b", jsObjArray_b);
	}

	jsonObj.put("numDoctosMN", new Integer(numDoctosMN));
	jsonObj.put("totMontoMN", new Double(totMontoMN));
	jsonObj.put("numDoctosUSD",new Integer(numDoctosUSD));
	jsonObj.put("totMontoUSD",new Double( totMontoUSD));

	jsonObj.put("puesto_personal", puesto_personal);
	jsonObj.put("nombre_personal", nombre_personal);
	jsonObj.put("paterno_personal", paterno_personal);
	jsonObj.put("materno_personal", materno_personal);
	
	jsonObj.put("msg", msgError);
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Confirmar")){

	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	JSONObject jsonObj = new JSONObject();

	String icProcSolic = (request.getParameter("icProcSolic")==null)?"":request.getParameter("icProcSolic");
	Vector lovDatos 	= null;
	Vector lovRegistro  = null;
	int icSolicPortal = 0, iNoSolicCarga = 0;
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm:ss");
	BigDecimal bdMtoDoctos=new BigDecimal("0.0");
	BigDecimal bdMtoDsctos=new BigDecimal("0.0");

	String _acuse = "500", mensajeFirma="";
	String pkcs7							=	request.getParameter("pkcs7");
	String externContent					=	request.getParameter("textoFirmado");
	//String serial							=	"";
	String folioCert						=	"";
	char getReceipt						=	'Y';
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
	
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			mensajeFirma = "<b>La autentificaci&oacute;n se llev&oacute; a cabo con éxito<br>Recibo:"+_acuse+"</b>";
			if ( icProcSolic.equals("")) icProcSolic = "0";
			lovDatos = autSolEJB.ovTransmitirDoctos( Integer.parseInt(icProcSolic), acuse.toString());
			if ( lovDatos.size() > 0){
				iNoSolicCarga = ((Integer) lovDatos.elementAt(0)).intValue();
				bdMtoDoctos = (BigDecimal) lovDatos.elementAt(1);
				bdMtoDsctos = (BigDecimal) lovDatos.elementAt(2);
			} //if
		}else{
			throw new NafinException("GRAL0021");
		}
	}
	List newReg = new ArrayList();
	lovDatos = autSolEJB.getDoctosCargados(acuse.toString(),"","","","","","");
	for(int i = 0; i < lovDatos.size(); i++) {
		HashMap hm = new HashMap();
		lovRegistro = (Vector) lovDatos.elementAt(i);
		hm.put("FOLIO_OPERA",lovRegistro.elementAt(9));
		hm.put("NOMBRE_CLI",lovRegistro.elementAt(0));
		hm.put("NO_SIRAC",lovRegistro.elementAt(1));
		hm.put("NO_DOCTO",lovRegistro.elementAt(3));
		hm.put("FECHA_EMISION",lovRegistro.elementAt(8));
		hm.put("FECHA_VENC",lovRegistro.elementAt(7));
		hm.put("MONEDA",lovRegistro.elementAt(2));
		hm.put("TIPO_CREDITO",lovRegistro.elementAt(6));
		hm.put("TASA_IF",lovRegistro.elementAt(15));
		hm.put("NO_AMORTIZA",lovRegistro.elementAt(14));
		hm.put("TABLA_AMORTIZA",lovRegistro.elementAt(16));
		hm.put("MONTO_DOCTO","$ "+lovRegistro.elementAt(4));
		hm.put("MONTO_DESC",	"$ "+lovRegistro.elementAt(5));
		newReg.add(hm);
	}
	if (newReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(newReg);
		jsonObj.put("registros", jsObjArray);
	}
	jsonObj.put("acuse", acuse.toString());
	jsonObj.put("fechaCarga", sdf.format(new java.util.Date()));
	jsonObj.put("horaCarga", sdf2.format(new java.util.Date()));
	jsonObj.put("usuario", strNombreUsuario);
	jsonObj.put("iNoSolicCarga", new Integer(iNoSolicCarga));
	jsonObj.put("bdMtoDoctos", Comunes.formatoMN(bdMtoDoctos.toPlainString()));
	jsonObj.put("bdMtoDsctos", Comunes.formatoMN(bdMtoDsctos.toPlainString()));
	jsonObj.put("mensajeFirma", mensajeFirma);
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("Asistencia")){

	JSONObject jsonObj = new JSONObject();
	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	String TipoCartera = (request.getParameter("TipoCartera")==null)?"":request.getParameter("TipoCartera").trim();

	Vector vecFilas 	= null;
	Vector vecColumnas 	= null;
	int i = 0;
	List newReg = new ArrayList();
	vecFilas = autSolEJB.sGetDatosBO(iNoCliente,TipoCartera);
	for (i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector) vecFilas.get(i);
		HashMap hm = new HashMap();
		String TipoPlazo		= (String) vecColumnas.elementAt(0);
		String TipoCred		= (String) vecColumnas.elementAt(1);
		String Emisor			= (String) vecColumnas.elementAt(2);
		String Tasa 	 		= (String) vecColumnas.elementAt(3);
		String TipoAmort 		= (String) vecColumnas.elementAt(4);
		String PMin 	 		= (String) vecColumnas.elementAt(5);
		String PMax 	 		= (String) vecColumnas.elementAt(6);
		String Periodicidad 	= (String) vecColumnas.elementAt(7);
		String TipoInteres 	= (String) vecColumnas.elementAt(8);
		String TipoRenta 		= (String) vecColumnas.elementAt(9);
		hm.put("TIPO_PLAZO",TipoPlazo);
		hm.put("TIPO_CREDITO",TipoCred);
		hm.put("EMISOR",Emisor);
		hm.put("TASA",Tasa);
		hm.put("TIPO_AMORT",TipoAmort);
		hm.put("PMIN",PMin);
		hm.put("PMAX",PMax);
		hm.put("PERIODICIDAD",Periodicidad);
		hm.put("TIPO_INTERES",TipoInteres);
		hm.put("TIPO_RENTA",TipoRenta);
		newReg.add(hm);
	}
	if (newReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(newReg);
		jsonObj.put("registros", jsObjArray);
	}

	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>