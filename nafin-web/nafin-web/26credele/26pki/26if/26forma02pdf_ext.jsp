<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<% 

JSONObject jsonObj = new JSONObject();

String acuse			= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String iNoSolicCarga	= (request.getParameter("iNoSolicCarga")==null)?"":request.getParameter("iNoSolicCarga");
String bdMtoDoctos	= (request.getParameter("bdMtoDoctos")==null)?"":request.getParameter("bdMtoDoctos");
String bdMtoDsctos	= (request.getParameter("bdMtoDsctos")==null)?"":request.getParameter("bdMtoDsctos");
String fechaCarga		= (request.getParameter("fechaCarga")==null)?"":request.getParameter("fechaCarga");
String horaCarga		= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario			= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");
Vector lovDatos 	= null;
Vector lovRegistro  = null;

try {

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Resumen de Solicitudes Cargadas","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("No. de Acuse:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(acuse,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("No. total de operaciones registradas:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(iNoSolicCarga,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los documentos:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(bdMtoDoctos,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los descuentos:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(bdMtoDsctos,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Fecha de carga:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Hora de carga:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Usuario:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(usuario,"formas",ComunesPDF.RIGHT);
	pdfDoc.addTable();

	pdfDoc.setTable(13, 100);
	pdfDoc.setCell("Folio de Operación","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre del Cliente","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Sirac","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Emisión Título de Crédito","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Vencimiento Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de Crédito","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tasa Intermediario Financiero","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Amortizaciones","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tabla Amortización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Descuento","celda01",ComunesPDF.CENTER);

	String folio="",	cliente="", sirac="", noDocto="", fec_emision="", fec_venc, moneda="",tipoCred, tasaIf="", noAmort="", tabAmort="", montoDocto="",montoDesc="";
	
	lovDatos = autSolEJB.getDoctosCargados(acuse,"","","","","","");
	for(int i = 0; i < lovDatos.size(); i++){
		lovRegistro = (Vector) lovDatos.elementAt(i);
		folio=(lovRegistro.elementAt(9)).toString();
		cliente=(lovRegistro.elementAt(0)).toString();
		sirac=(lovRegistro.elementAt(1)).toString();
		noDocto=(lovRegistro.elementAt(3)).toString();
		fec_emision=(lovRegistro.elementAt(8)).toString();
		fec_venc=(lovRegistro.elementAt(7)).toString();
		moneda=(lovRegistro.elementAt(2)).toString();
		tipoCred=(lovRegistro.elementAt(6)).toString();
		tasaIf=(lovRegistro.elementAt(15)).toString();
		noAmort=(lovRegistro.elementAt(14)).toString();
		tabAmort=(lovRegistro.elementAt(16)).toString();
		montoDocto=(lovRegistro.elementAt(4)).toString();
		montoDesc=(lovRegistro.elementAt(5)).toString();
		pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(cliente,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(noDocto,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_emision,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_venc,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tipoCred,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tasaIf,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(noAmort,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tabAmort,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDocto,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDesc,"formas",ComunesPDF.CENTER);
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>