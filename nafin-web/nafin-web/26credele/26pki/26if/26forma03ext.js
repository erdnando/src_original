Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   

	//------------------------------ EXTRA - Function �S ----------------------------------------
	var strEsquemaExtJS = Ext.getDom('hidStrEsquemaExtJS').value;
	var valInicio		=	{plazos:null}
	var barra 			=	{ic_proc_solic:null}
	var transmiteSolic=	{textoFirmar:null}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDFPreAcuse =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGeneraPdfPreAcuse');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfPreAcuse');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fpCarga.setVisible(false);
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(opts.params.operacion == "Cancelar"){
				Ext.Msg.alert("Mensaje de p�gina web.",	"Operaci�n Cancelada",
					function(){
						window.location = '26forma03ext.jsp';
					}
				);
			}else{
				//Ext.Msg.alert("Mensaje de p�gina web.",	infoR.mensajeFirma);
				Ext.getCmp('disMsjFirma').body.update('<div class="formas" align="center">'+infoR.mensajeFirma+'</div>');
				Ext.getCmp('disAcuse').body.update('<div class="formas">'+infoR.acuse+'</div>');	//align="right" 
				Ext.getCmp('disTotOpera').body.update('<div class="formas">'+infoR.iNoSolicCarga+'</div>');
				Ext.getCmp('disImpDocto').body.update('<div class="formas">'+infoR.bdMtoDoctos+'</div>');
				Ext.getCmp('disImpDesc').body.update('<div class="formas">'+infoR.bdMtoDsctos+'</div>');
				Ext.getCmp('disFechaCarga').body.update('<div class="formas">'+infoR.fechaCarga+'</div>');
				Ext.getCmp('disHoraCarga').body.update('<div class="formas">'+infoR.horaCarga+'</div>');
				Ext.getCmp('disUsuario').body.update('<div class="formas">'+infoR.usuario+'<div>');
				fpAcuse.setVisible(true);

				if (infoR.registros != undefined && infoR.registros.length > 0){
					gridAcuseData.loadData(infoR.registros);
					Ext.getCmp('gridAcuse').getGridEl().unmask();
				}else{
					Ext.getCmp('gridAcuse').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}

				Ext.getCmp('btnGenerarPDF').setHandler( function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url : '26forma03pdf_ext.jsp',
						params:{
									acuse: infoR.acuse,
									aux_acuse:infoR.aux_acuse,
									iNoSolicCarga:infoR.iNoSolicCarga,
									bdMtoDoctos:infoR.bdMtoDoctos,
									bdMtoDsctos:infoR.bdMtoDsctos,
									fechaCarga:infoR.fechaCarga,
									horaActual:infoR.horaCarga,
									usuario:infoR.usuario
								},
						callback: procesarSuccessFailureGenerarPDF
					});
				});
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValoresPreAcuse(opts, success, response) {
		pnl.el.unmask();
		gridResumenData.loadData("");
		gridResumen_bData.loadData("");
		gridAcuseData.loadData("");
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.setVisible(false);
			fpCarga.setVisible(true);
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('totalOpera').body.update('<div class="formas" align="right"> '+infoR.totalOpera+'</div>');
			Ext.getCmp('montoImpDocto').body.update('<div class="formas" align="right"> '+infoR.montoDocto+'</div>');
			Ext.getCmp('montoImpDesc').body.update('<div class="formas" align="right"> '+infoR.montoDesc+'</div>');

			if (parseFloat(infoR.totalOpera) > 0){
				Ext.getCmp('btnConfirmarResumen').enable();
			}else{
				Ext.getCmp('btnConfirmarResumen').disable();
			}

			var miHtml = ''+
					'<div align="justify">'+
						'<br>De conformidad con el convenio que este Banco ('+infoR.strNombre+'), celebr&oacute; con Nacional Financiera, S.N.C., y en virtud del descuento electr&oacutenico de (los) documento(s) electr&oacute;nico(s), '+
						'en este acto cedemos a Nacional Financiera, S.N.C., los derechos de cr&eacute;dito que se derivan de los mismos.<br><br>'+
						'Asimismo, bajo protesta de decir verdad, hacemos constar en este acto que para todos los efectos legales a que haya lugar nos asumimos como depositarios sin derecho a honorarios y asumiendo la responsabilidad '+
						'civil y penal correspondiente al car&aacute;cter de depositario judicial, del (los) documento(s) electr&oacute;nico(s) en que se consignan los derechos de cr&eacute;dito derivados de las operaciones celebradas con los '+
						'clientes que se describen en el cuadro anexo en donde se especifica la fecha y lugar de presentaci&oacute;n, importe total y nombre de la empresa acreditada.<br><br>'+
						'La informaci&oacute;n del (los) '+infoR.numDoctosMN+' documento(s) electr&oacute;nico(s) en Moneda Nacional por un importe de $'+Ext.util.Format.number(infoR.totMontoMN,'0.00')+' y '+infoR.numDoctosUSD+' documento(s) electr&oacute;nico(s) en D&oacute;lares por un importe de $'+Ext.util.Format.number(infoR.totMontoUSD,'0.00')+' citado(s), '+
						'est&aacute; registrada en el sistema denominado Nafin Electr&oacute;nico a disposici&oacute;n de Nacional Financiera, S.N.C., cuando &eacute;sta la requiera.<br><br>'+
						'En t&eacute;rminos del art&iacute;culo 2044 del C&oacute;digo Civil para el Distrito Federal, la cesi&oacute;n a que se refiere el primer p&aacute;rrafo la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, '+
						'por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidaci&oacute;n total.<br><br>'+
						'Asimismo y en t&eacute;rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del (los) documento(s) electr&oacute;nico(s) que se listan a continuaci&oacute;n y que amparan los derechos '+
						'de cr&eacute;dito a ejercer los derechos de cobro consignados en el (los) documento(s) electr&oacute;nico(s) se�alados y vigilar que conserven su valor y dem&aacute;s derechos que les correspondan.'+
						'<br><br>'+
						'<table border="0">'+
							'<tr>'+
								'<td class="formas" nowrap>Nombre del Intermediario Financiero:</td>'+
								'<td class="formas" nowrap>'+infoR.strNombre+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Fecha del Convenio de Cr&eacute;dito Electr�nico:</td>'+
								'<td class="formas" nowrap>'+infoR.fechaConvenio+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Fecha de Operaci&oacute;n:</td>'+
								'<td class="formas" nowrap>M&eacute;xico D.F. a '+infoR.diaHoy+' de '+infoR.mesAct+' de '+infoR.anioAct+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Nota:�La Fecha de Operaci&oacute;n es Informativa.</td>'+
								'<td class="formas" nowrap>&nbsp;</td>'+
							'</tr>'+
						'</table>'+
					'</div>';

			if (infoR.registros != undefined && infoR.registros.length > 0){
				gridResumenData.loadData(infoR.registros);
				Ext.getCmp('gridResumen').getGridEl().unmask();
				/*var totDesc = 0;
				gridResumenData.each(function(registro){
					if (	Ext.isEmpty(registro.get('MONTO_DESC'))	){
						registro.get('MONTO_DESC') = 0;
					}
					totDesc += parseFloat(registro.get('MONTO_DESC'));
				});
				var regTot = totalesData.getAt(0);
				regTot.set('TOTAL_MONTO',	totDesc );*/

			}else{
				Ext.getCmp('gridResumen').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}

			if (infoR.registros_b != undefined && infoR.registros.length > 0){
				var gridColumnMod = Ext.getCmp('gridResumen_b').getColumnModel();
				gridResumen_bData.loadData(infoR.registros_b);
				
				if(Ext.getCmp('tipoPlazo').getValue()=="C"){
					gridColumnMod.setHidden(6,false);
				}else{
					gridColumnMod.setHidden(6,true);
				}
				
				Ext.getCmp('gridResumen_b').getGridEl().unmask();
			}else{
				Ext.getCmp('gridResumen_b').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}

			var regTot = totalesData.getAt(0);
			regTot.set('TOTAL_MONTO',	infoR.montoDesc );

			Ext.getCmp('msjTipoPlazoRes_b').body.update(miHtml);
			Ext.getCmp('msjTipoPlazoRes_c').body.update('<div class="titulos" align="center">Facultado por <strong>'+infoR.strNombre+'</strong><br>'+
															infoR.puesto_personal+'-'+infoR.nombre_personal+'&nbsp;'+infoR.paterno_personal+'&nbsp;'+infoR.materno_personal+'</div>');

			if (infoR.txtFirmar != undefined){
				transmiteSolic.textoFirmar = infoR.txtFirmar;
			}

			Ext.getCmp('btnGeneraPdfPreAcuse').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '26forma03pre_pdf_ext.jsp',
					params:{	ic_proc_solic:	barra.ic_proc_solic, tipoPlazo: Ext.getCmp('tipoPlazo').getValue()},
					callback: procesarSuccessFailureGenerarPDFPreAcuse
				});
			});
			
			/**///Esta parte del boton no esta especificado en los archivos originales...
			/*if (!Ext.isEmpty(infoR.nombreArchivoErr)){
				Ext.getCmp('btnDescargaErrores').setVisible(true);
			}*/
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaErroresCarga(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.hide();
			fpErrores.show();
			var infoR = Ext.util.JSON.decode(response.responseText);
			//Ext.getCmp('mensajeBloqueo').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');

		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function makeActualiza() {
		return function() {
					Ext.Ajax.request({
						url: 		'26forma03ext.data.jsp',
						params: 	{
							informacion:		'ActualizarAvance',
							proceso: 			barra.ic_proc_solic
						},
						callback: 				procesaActualizarAvance
					});
		}
	}

	function timeActualizaAvance(){
		setTimeout(makeActualiza(), 1250);
	}

	function procesaActualizarAvance(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var respuesta = Ext.util.JSON.decode(response.responseText);
			var estatusProceso				= respuesta.estatusProceso;

			if(estatusProceso == "PR"){
				timeActualizaAvance();
			}else if( estatusProceso == "OK" || estatusProceso=="ER" ){
					aviso.hide();
					if (estatusProceso == "OK"){
						Ext.Ajax.request({
							url: 		'26forma03ext.data.jsp',
							params: 	{
								informacion:		'transmitirSolicitudes',
								ic_proc_solic: 	barra.ic_proc_solic
							},
							callback: 				procesaValoresPreAcuse
						});
	
					}else if (estatusProceso == "ER"){
						Ext.Ajax.request({
							url: 		'26forma03ext.data.jsp',
							params: 	{
								informacion:		'obtieneErrores',
								ic_proc_solic: 	barra.ic_proc_solic
							},
							callback: 				procesaObtieneErrores
						});
					}
			}else{
				timeActualizaAvance();
			}

		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfAsistencia =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfAsistencia');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAsistencia');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAsistencia(opts, success, response) {
		pnl.el.unmask();
		gridAsistenciaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);

			var elementosAsistencia = [
				{
					xtype:'panel',	style:'margin:0 auto;',	frame:false,	border:false,	html:'<div align="center"><b>Para hacer m�s �gil el registro de operaciones, se muestran las caracter�sticas con las que puede solicitar su cr�dito.</b></div>'
				},{
					xtype:'displayfield',	value:''
				},{
					xtype:'grid',	id:'gridAsistencia',	store:gridAsistenciaData,	stripeRows:true,	loadMask:true,	height:580,	width:850,	anchor:'100%',	hidden:false, frame:false,
					columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	title:'Datos con los que debe realizar el registro de su operaci�n',
					columns: [
						{header:'Tipo Plazo',	tooltip:'Tipo Plazo', dataIndex:'TIPO_PLAZO',	width:100,	align:'center', resizable:true, hideable:false, sortable:true},
						{header:'Tipo Cr�dito',tooltip:'Tipo Cr�dito',	dataIndex:'TIPO_CREDITO',width:150,	align:'center', resizable:true,	sortable:true},
						{header:'Nombre del Emisor',	tooltip:'Nombre del Emisor',	dataIndex:'EMISOR',	width:200,	align:'center', resizable:true},
						{header:'Tasa Base Intermediario',	tooltip:'Tasa Base Intermediario',	dataIndex:'TASA',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo Amortizaci�n',	tooltip:'Tipo Amortizaci�n',	dataIndex:'TIPO_AMORT',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Plazo m�nimo',	tooltip:'Plazo m�nimo',	dataIndex:'PMIN',	width:110,	align:'center', sortable:true,	resizable:true},
						{header:'Plazo m�ximo',	tooltip:'Plazo m�ximo',	dataIndex:'PMAX',	width:110,	align:'center', sortable:true,	resizable:true},
						{header:'Periodicidad de Pago al Capital',	tooltip:'Periodicidad de Pago al Capital',	dataIndex:'PERIODICIDAD',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo de Inter�s',	tooltip:'Tipo de Inter�s',	dataIndex:'TIPO_INTERES',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo Renta',	tooltip:'Tipo Renta',	dataIndex:'TIPO_RENTA',	width:150,	align:'center', sortable:true,	resizable:true}
					]
				},{
					xtype:'displayfield',	value:''
				},{
					xtype:'panel', layout:'table',	layoutConfig: {columns:3}, border:false,
					items: [{
						border:false,	html:'<div align="center"><b>Si la informaci�n que requiere para la captura de su cr�dito no se encuentra en este recuadro, contacte a su ejecutivo.</b></div>',
						colspan: 3
					},{
						border:false,	html: '<div align="right"><b>Para mayor informaci�n consulte el Manual de Operaci�n</b></div>',
						colspan: 2, width:500
					},{
						xtype:'button', id:'btnManualOpera', 	iconCls:'icoPdf', height:'auto', width:30,
							handler: function() {
											var forma = Ext.getDom('formAux');
											forma.action = "26guiaoperacion.pdf";
											forma.submit();
							}
					 }]
				}
			];
		
			var fpAsistencia = new Ext.form.FormPanel({
				id: 'formaAsistencia',
				width: 850,
				heigth:'auto',
				style: 'margin:0 auto;',
				frame: false,
				border:false,
				bodyStyle: 'padding: 6px',
				labelWidth: 150,
				items: elementosAsistencia
			});
			new Ext.Window({
				modal: true,
				resizable: true,
				x: 100, y:75,
				width: 870,
				height: 750,
				layout:'fit',
				autoScroll:true,
				id: 'winAsistencia',
				closeAction: 'destroy',
				items: [fpAsistencia],
				title: 'Asistencia en la Operaci�n',
				bbar: {xtype: 'toolbar', buttons: ['->','-',{xtype:'button',	text:'Generar PDF ',	id:'btnPdfAsistencia',	disabled:true, iconCls:'icoPdf',
						handler: function(boton, event){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '26forma_ayuda_pdf_ext.jsp',
											params: {TipoCartera:Ext.getCmp('_tipoCartera').getValue()},
											callback: procesarGenerarPdfAsistencia
										});
						}
						},{xtype:'button',text:'Bajar PDF',id:'btnBajarPdfAsistencia',hidden:true}]}
			}).show();

			if (infoR.registros != undefined && infoR.registros.length > 0){
				gridAsistenciaData.loadData(infoR.registros);
				Ext.getCmp('btnPdfAsistencia').setDisabled(false);
				Ext.getCmp('gridAsistencia').getGridEl().unmask();
			}else{
				Ext.getCmp('gridAsistencia').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnPdfAsistencia').setDisabled(true);
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaObtieneErrores(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp("sinErr").setValue('');
			Ext.getCmp("conErr").setValue('');

				/**///Despues de cargar el archivo en caso de que se direccione con el estatusProceso = "ER"
				/*if(opts.params.TipoPlazo == "F"){
					Ext.getDom('plazoFactoraje').checked = true;
					Ext.getCmp('radioGp').items.items[0].checked = true;
					Ext.getCmp('tipoPlazo').setValue("F");
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
				}else if(opts.params.TipoPlazo == "C"){
					Ext.getDom('plazoCredito').checked = true;
					Ext.getCmp('radioGp').items.items[1].checked = true;
					Ext.getCmp('tipoPlazo').setValue("C");
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
				}
				if(opts.params.TipoArchivo == "E"){
					Ext.getDom('radioE').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[0].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('E');
				}else if(opts.params.TipoArchivo == "T"){
					Ext.getDom('radioT').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[1].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('T');
				}else if(opts.params.TipoArchivo == "Z"){
					Ext.getDom('radioZ').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[2].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('Z');
				}
				*/
				Ext.getCmp('form_file').reset();
				Ext.getCmp('form_file').validate();

				Ext.getCmp('panelErrores').show();
				Ext.getCmp('btnRegresar').show();
				if(infoR.sSinError != undefined){
					if (	!Ext.isEmpty(infoR.sSinError)	){
						Ext.getCmp("sinErr").setValue(infoR.sSinError);
						Ext.getCmp('btnProcesar').setHandler( function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							pnl.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: 		'26forma03ext.data.jsp',
								params: 	{
									informacion:		'transmitirSolicitudes',
									ic_proc_solic: 	barra.ic_proc_solic
								},
								callback: 				procesaValoresPreAcuse
							});
						});
						Ext.getCmp('btnProcesar').enable();
					}
				}
				if(infoR.sConError != undefined){
					Ext.getCmp("conErr").setValue(infoR.sConError);
				}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		///-------------->>>>>>>>>>>>> Quitar esta l�nea..... es linea t
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('mensajeBloqueo').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			Ext.getCmp('mensajeBloqueo_b').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			Ext.getCmp('mensajeBloqueo_c').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			Ext.getCmp('_tipoCartera').setValue(infoR.tipoCartera);
			Ext.getCmp('_desc_intermediario').setValue(infoR.desc_intermediario);
			valInicio.plazos = infoR.plazos;
			if (infoR.plazos == "1"){
				Ext.getCmp('tipoPlazo').setValue(infoR.tipo);
				Ext.getCmp('radioGrupoPlazo').hide();
				if(Ext.getCmp('tipoPlazo').getValue() == "F"){
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
				}else if(Ext.getCmp('tipoPlazo').getValue() == "C"){
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
				}
			}else{
				Ext.getCmp('radioGrupoPlazo').show();
			}
			
			if(opts.params.inicio == "26forma3c.jsp"){
				/**///Despues de cargar el archivo en caso de que se direccione con el estatusProceso = "ER"
				if(opts.params.TipoPlazo == "F"){
					Ext.getDom('plazoFactoraje').checked = true;
					Ext.getCmp('radioGp').items.items[0].checked = true;
					Ext.getCmp('tipoPlazo').setValue("F");
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
				}else if(opts.params.TipoPlazo == "C"){
					Ext.getDom('plazoCredito').checked = true;
					Ext.getCmp('radioGp').items.items[1].checked = true;
					Ext.getCmp('tipoPlazo').setValue("C");
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
				}
				if(opts.params.TipoArchivo == "E"){
					Ext.getDom('radioE').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[0].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('E');
				}else if(opts.params.TipoArchivo == "T"){
					Ext.getDom('radioT').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[1].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('T');
				}else if(opts.params.TipoArchivo == "Z"){
					Ext.getDom('radioZ').checked = true;
					Ext.getCmp('radioGpArchivos').items.items[2].checked = true;
					Ext.getCmp('_tipoArchivo').setValue('Z');
				}
				
				barra.ic_proc_solic = opts.params.ic_proc_solic;
				Ext.getCmp('panelErrores').show();
				Ext.getCmp('btnRegresar').show();

				if(infoR.sSinError != undefined){
					if (	!Ext.isEmpty(infoR.sSinError)	){
						Ext.getCmp("sinErr").setValue(infoR.sSinError);
						Ext.getCmp('btnProcesar').setHandler( function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						pnl.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: 		'26forma03ext.data.jsp',
								params: 	{
									informacion:		'transmitirSolicitudes',
									ic_proc_solic: 	barra.ic_proc_solic
								},
								callback: 				procesaValoresPreAcuse
							});
						});
						Ext.getCmp('btnProcesar').enable();
					}
				}
				if(infoR.sConError != undefined){
					Ext.getCmp("conErr").setValue(infoR.sConError);
				}
			}

		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

//* - * - * - * - *STORE�S - * - * - * - * - * -
	var dataLayout_a = [
		{'NUMERO':'1','DESCRIPCION':'Clave de Sirac', 'TIPO_DATO':'N�merico',	'LONGITUD':'12','OBLIGATORIO':'S�'},
		{'NUMERO':'2','DESCRIPCION':'N�mero de Sucursal del Banco',	'TIPO_DATO':'N�merico',	'LONGITUD':'10','OBLIGATORIO':'No'},
		{'NUMERO':'3','DESCRIPCION':'Clave de la Moneda','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'4','DESCRIPCION':'N�mero de Documento','TIPO_DATO':'Num�rico',	'LONGITUD':'10','OBLIGATORIO':'S� / Este n�mero no puede venir repetido en el archivo de carga.'},
		{'NUMERO':'5','DESCRIPCION':'Importe de Documento','TIPO_DATO':'Num�rico',	'LONGITUD':'19,2','OBLIGATORIO':'S� / Mayor a cero.'},
		{'NUMERO':'6','DESCRIPCION':'Importe de Descuento','TIPO_DATO':'Num�rico',	'LONGITUD':'19,2','OBLIGATORIO':'S� /Mayor a cero y menor al Importe de Documento'},
		{'NUMERO':'7','DESCRIPCION':'Clave del Emisor','TIPO_DATO':'Num�rico',	'LONGITUD':'5','OBLIGATORIO':'No'},
		{'NUMERO':'8','DESCRIPCION':'Clave del Tipo de Cr�dito','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'9','DESCRIPCION':'Clave de la Periodicidad del Pago de Capital','TIPO_DATO':'Num�rico',	'LONGITUD':'2','OBLIGATORIO':'S�'},
		{'NUMERO':'10','DESCRIPCION':'Clave de la Periodicidad del Pago de Intereses','TIPO_DATO':'Num�rico',	'LONGITUD':'2','OBLIGATORIO':'S�'},
		{'NUMERO':'11','DESCRIPCION':'Descripci�n de los Bienes y Servicios','TIPO_DATO':'Texto',	'LONGITUD':'60','OBLIGATORIO':'S� / S�lo se consideran los primeros 60 caracteres.'},
		{'NUMERO':'12','DESCRIPCION':'Clave de la Clase de Documento','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'13','DESCRIPCION':'Domicilio de Pago','TIPO_DATO':'Texto',	'LONGITUD':'60','OBLIGATORIO':'S� / S�lo se consideran los primeros 60 caractares'},
		{'NUMERO':'14','DESCRIPCION':'Clave de la Tasa de Inter�s del Usuario Final ','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':"No / Obligatorio cuando Tipo Plazo = 'C' (Cr�dito)."},
		{'NUMERO':'15','DESCRIPCION':'Relaci�n Matem�tica del Usuario Final','TIPO_DATO':'Texto',	'LONGITUD':'1','OBLIGATORIO':"No / Obligatorio cuando Tipo Plazo = 'C' (Cr�dito) y el valor que debe traer es: '+'."},
		{'NUMERO':'16','DESCRIPCION':'Sobretasa del Usuario Final','TIPO_DATO':'Num�rico',	'LONGITUD':'2,4','OBLIGATORIO':"No / Obligatorio cuando Tipo Plazo = 'C' (Cr�dito)"},
		{'NUMERO':'17','DESCRIPCION':'Clave de la Tasa de Inter�s del Intermediario','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'18','DESCRIPCION':'N�mero de Amortizaciones','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'19','DESCRIPCION':'Clave del Tipo de Amortizaci�n','TIPO_DATO':'Num�rico',	'LONGITUD':'3','OBLIGATORIO':'S�'},
		{'NUMERO':'20','DESCRIPCION':'Fecha de Primer Pago de Capital','TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'S� / Debe ser mayor o igual a la fecha de Hoy'},
		{'NUMERO':'21','DESCRIPCION':'Fecha de Primer Pago de Inter�s','TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'S� / Debe ser mayor o igual a la fecha de Hoy'},
		{'NUMERO':'22','DESCRIPCION':'D�a de Pago','TIPO_DATO':'Num�rico',	'LONGITUD':'2','OBLIGATORIO':'No'},
		{'NUMERO':'23','DESCRIPCION':'Fecha de Vencimiento del Documento','TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'Debe ser mayor o igual a la fecha de Hoy.'},
		{'NUMERO':'24','DESCRIPCION':'Fecha de Vencimiento del Descuento','TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'S� / Debe ser mayor o igual a la fecha de Hoy.'},
		{'NUMERO':'25','DESCRIPCION':'Fecha de Emisi�n del T�tulo de Cr�dito','TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'S�'},
		{'NUMERO':'26','DESCRIPCION':'Lugar de Firma','TIPO_DATO':'Texto',	'LONGITUD':'50','OBLIGATORIO':'S� / S�lo se toman en cuenta los primeros 50 caracteres.'},
		{'NUMERO':'27','DESCRIPCION':'Tipo de Amortizaci�n de Ajuste','TIPO_DATO':'Texto',	'LONGITUD':'1','OBLIGATORIO':'No / Es obligatorio cuando el Tipo de Amortizaci�n es "PAGOS IGUALES CAP. MAS INTS."'+" ( clave 3 ) \nen caso contrario es ignorado. Valores permitidos: 'P' (primero) y 'U' (ultimo).\n"},	
		{'NUMERO':'28','DESCRIPCION':'Tipo de Renta','TIPO_DATO':'Texto',	'LONGITUD':'1','OBLIGATORIO':'No / Es obligatorio cuando el Tipo de Amortizaci�n es "PLAN DE PAGOS" \n(clave 5) en caso contrario este valor es ignorado.'+"Valores permitidos: 'S' (Si) y 'N' (No).\n"}		
	];

	var dataLayout_b = [
		{'NUMERO':'29','DESCRIPCION':'N�mero de Amortizaci�n', 'TIPO_DATO':'N�merico',	'LONGITUD':'3','OBLIGATORIO':'Es obligatorio cuando el Tipo de Amortizaci�n es "PLAN DE PAGOS" \n(clave 5) � "PAGOS IGUALES CAP. MAS INTS." (clave 3), en caso contrario este valor es ignorado.'},
		{'NUMERO':'30','DESCRIPCION':'Importe de Amortizaci�n', 'TIPO_DATO':'N�merico',	'LONGITUD':'18,2','OBLIGATORIO':'Es obligatorio cuando el Tipo de Amortizaci�n es "PLAN DE PAGOS" \n(clave 5) � "PAGOS IGUALES CAP. MAS INTS." (clave 3), en caso contrario este valor es ignorado.'},
		{'NUMERO':'31','DESCRIPCION':'Fecha de Amortizaci�n', 'TIPO_DATO':'Fecha con formato dd/mm/aaaa',	'LONGITUD':'10','OBLIGATORIO':'Es obligatorio cuando el Tipo de Amortizaci�n es "PLAN DE PAGOS" \n(clave 5) � "PAGOS IGUALES CAP. MAS INTS." (clave 3), en caso contrario este valor es ignorado.'},
		{'NUMERO':'32','DESCRIPCION':'Tipo de Amortizaci�n', 'TIPO_DATO':'Texto',	'LONGITUD':'1','OBLIGATORIO':'Es obligatorio cuando el Tipo de Amortizaci�n es "PLAN DE PAGOS" \n(clave 5) � "PAGOS IGUALES CAP. MAS INTS."' +"(clave 3), en caso contrario este valor es ignorado / \nValores permitidos: 'A' (Capital e Intereses), 'I' (Intereses) y 'R' (Recortada)"}
	];
	
	var dataLayout_c = [
		{'NUMERO':'�ltimo campo','DESCRIPCION':'C�digo de Base de Operaci�n', 'TIPO_DATO':'Num�rico',	'LONGITUD':'6','OBLIGATORIO':'Opcional'}
	];
	
	var gridLayoutData_c = new Ext.data.JsonStore({
		fields:[ {name:'NUMERO'},{name:'DESCRIPCION'},{name:'TIPO_DATO'},{name:'LONGITUD'},{name:'OBLIGATORIO'}],
		data:  dataLayout_c,
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var gridLayoutData_b = new Ext.data.JsonStore({
		fields:[ {name:'NUMERO'},	{name:'DESCRIPCION'},	{name:'TIPO_DATO'},	{name:'LONGITUD'},	{name:'OBLIGATORIO'}],
		data:  dataLayout_b,
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var gridLayoutData_a = new Ext.data.JsonStore({
		fields:[ {name:'NUMERO'},	{name:'DESCRIPCION'},	{name:'TIPO_DATO'},	{name:'LONGITUD'},	{name:'OBLIGATORIO'}],
		data:  dataLayout_a,
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError,
												load: function(store,arrayReg, opt){
												
													if(Ext.getCmp('tipoPlazo') && Ext.getCmp('tipoPlazo').getValue()=='C'){
														var record = Ext.data.Record.create([
																{name:'NUMERO'},
																{name:'DESCRIPCION'},
																{name:'TIPO_DATO'},
																{name:'LONGITUD'},
																{name:'OBLIGATORIO'}
															]);
														store.insert(11, new record({NUMERO:'11', DESCRIPCION:'Destino del Cr�dito', TIPO_DATO:'Texto', LONGITUD:'400', OBLIGATORIO:'Si'}));
													}
												}
											}
	});

	var gridAsistenciaData = new Ext.data.JsonStore({
		fields:[{name:'TIPO_PLAZO'},
					{name: 'TIPO_CREDITO'},
					{name: 'EMISOR'},
					{name: 'TASA'},
					{name: 'TIPO_AMORT'},
					{name: 'PMIN'},
					{name: 'PMAX'},
					{name: 'PERIODICIDAD'},
					{name: 'TIPO_INTERES'},
					{name: 'TIPO_RENTA'}
		],
		data:  [
					{'TIPO_PLAZO':'','TIPO_CREDITO':'', 'EMISOR':'',	'TASA':'','TIPO_AMORT':'','PMIN':'','PMAX':'','PERIODICIDAD':'','TIPO_INTERES':'','TIPO_RENTA':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridResumenData = new Ext.data.JsonStore({
		fields:[{name:'NOMBRE_CLI'},
					{name: 'NO_SIRAC'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_EMISION'},
					{name: 'FECHA_VENC'},
					{name: 'MONEDA'},
					{name: 'TIPO_CREDITO'},
					{name: 'TASA_IF'},
					{name: 'NO_AMORTIZA'},
					{name: 'TABLA_AMORTIZA'},
					{name: 'MONTO_DOCTO'},
					{name: 'MONTO_DESC'}	],
		data:  [
					{'NOMBRE_CLI':'','NO_SIRAC':'', 'NO_DOCTO':'',	'FECHA_EMISION':'','FECHA_VENC':'','MONEDA':'',
					'TIPO_CREDITO':'','TASA_IF':'','NO_AMORTIZA':'','TABLA_AMORTIZA':'','MONTO_DOCTO':'','MONTO_DESC':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridResumen_bData = new Ext.data.JsonStore({
		fields:[{name:'NOMBRE_EMISOR'},
					{name: 'CLASE_DOCTO'},
					{name: 'LUGAR'},
					{name: 'FECHA_EMISION'},
					{name: 'DOMICILIO'},
					{name: 'TASA_INTERES'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_VENC'},
					{name: 'MONTO_OPERA'},
					{name: 'DEST_CREDITO'}
		],
		data:  [
					{'NOMBRE_EMISOR':'','CLASE_DOCTO':'', 'LUGAR':'',	'FECHA_EMISION':'','DOMICILIO':'','DEST_CREDITO':'','TASA_INTERES':'','NO_DOCTO':'','FECHA_VENC':'','MONTO_OPERA':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [	{name: 'TOTAL'},	{name: 'TOTAL_MONTO'}	],
		data:[{'TOTAL':'Total:','TOTAL_MONTO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAcuseData = new Ext.data.JsonStore({
		fields:[ {name:'FOLIO_OPERA'},
					{name:'NOMBRE_CLI'},
					{name: 'NO_SIRAC'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_EMISION'},
					{name: 'FECHA_VENC'},
					{name: 'MONEDA'},
					{name: 'TIPO_CREDITO'},
					{name: 'TASA_IF'},
					{name: 'NO_AMORTIZA'},
					{name: 'TABLA_AMORTIZA'},
					{name: 'MONTO_DOCTO', type:'float'},
					{name: 'MONTO_DESC', type:'float'}	],
		data:  [
					{'FOLIO_OPERA':'','NOMBRE_CLI':'','NO_SIRAC':'', 'NO_DOCTO':'',	'FECHA_EMISION':'','FECHA_VENC':'','MONEDA':'',
					'TIPO_CREDITO':'','TASA_IF':'','NO_AMORTIZA':'','TABLA_AMORTIZA':'','MONTO_DOCTO':'','MONTO_DESC':''}	],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

//* - * - * - * - *COMPONENT�S - * - * - * - * - * -

	var elementosAcuse = [
		{
			xtype:'panel',	id:'mensajeBloqueo_c',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype:'displayfield', value:''
		},{
			xtype: 'panel',layout:'table',	width:600,	border:true,	frame:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:200,	height:35,	bodyStyle:'padding:6px'},
			items:[
				{	width:600,	id:'disMsjFirma',	colspan:2,	border:false,	height:45,	html:'&nbsp;'	},
				{	width:600,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Resumen de Solicitudes Cargadas</div>'	},
				{	html:'<div class="formas" align="right">No. de Acuse:</div>'	},
				{	width:600,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">No. total de operaciones registradas:</div>'	},
				{	width:600,id:'disTotOpera',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Monto total del importe de los documentos:</div>'	},
				{	width:600,id:'disImpDocto',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Monto total del importe de los descuentos:</div>'	},
				{	width:600,id:'disImpDesc',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Fecha de carga:</div>'	},
				{	width:600,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Hora de carga:</div>'	},
				{	width:600,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="right">Usuario:</div>'	},
				{	width:600,id:'disUsuario',html: '&nbsp;'	}
			]
		},{
			xtype:'displayfield', value:''
		},{
			xtype:'grid',	id:'gridAcuse',	store:gridAcuseData,	stripeRows:true,	loadMask:true,	height:400,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Folio Operaci�n',	tooltip:'Folio Operaci�n', dataIndex:'FOLIO_OPERA',	width:100,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'Nombre del Cliente',		tooltip:'Nombre del Cliente', dataIndex:'NOMBRE_CLI',		width:100,	align:'center', resizable:true, sortable:true},
				{header:'N�mero Sirac',tooltip:'N�mero Sirac',	dataIndex:'NO_SIRAC',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de documento',	tooltip:'N�mero de documento',	dataIndex:'NO_DOCTO',	width:120,	align:'center', resizable:true},
				{header:'Fecha Emisi�n T�tulo de Cr�dito',	tooltip:'Fecha Emisi�n T�tulo de Cr�dito',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center', sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Fecha Vencimiento Descuento',	tooltip:'Fecha Vencimiento Descuento', dataIndex:'FECHA_VENC',
					width:120,	align:'center', sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tipo de cr�dito',		tooltip:'Tipo de cr�dito',	dataIndex:'TIPO_CREDITO',	width:150,	align:'center', sortable:true},
				{header:'Tasa Intermediario Financiero',	tooltip:'Tasa Intermediario Financiero',	dataIndex:'TASA_IF',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de Amortizaciones',	tooltip:'N�mero de Amortizaciones',	dataIndex:'NO_AMORTIZA',	width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Tabla de amortizaci�n',		tooltip:'Tabla de amortizaci�n',		dataIndex:'TABLA_AMORTIZA',width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Monto del Documento',		tooltip:'Monto del Documento',		dataIndex:'MONTO_DOCTO',	width:130,	align:'right', resizable:true,	sortable:true,	renderer:Ext.util.Format.numberRenderer('$0,0.00')},
				{header:'Monto del Descuento',		tooltip:'Monto del Descuento',		dataIndex:'MONTO_DESC',		width:130,	align:'right', resizable:true,	sortable:true,	renderer:Ext.util.Format.numberRenderer('$0,0.00')}
			],
			bbar:{
				buttonAlign:"center",
				items: [
					'-',' ',
					{
						text:'Descargar Archivo con Errores', iconCls:'icoTxt',	id:'btnDescargaErrores', hidden:true
					},' ','-',' ',{
						text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf'
					},{
						text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true
					},' ','-',' ',{
						xtype:'button',	text:'Salir',	id:'btnSalir', iconCls:'icoLimpiar',
						handler: function(boton, evento){
							window.location = '26forma03ext.jsp';
						}
					},' ','-'
				]
			}
		}
	];

	var fpAcuse = new Ext.form.FormPanel({
		id: 'formaAcuse',
		width: 900,
		heigth:'auto',
		style: 'margin:0 auto;',
		frame: false,
		border:false,
		hidden:true,
		bodyStyle: 'padding: 4px',
		labelWidth: 150,
		items: elementosAcuse,
		buttonAlign: 'center'
	});

	
	var fnConfirmarResumenCallback = function(vpkcs7, vtextoFirmar, vic_proc_solic){
		if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}
		pnl.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '26forma03ext.data.jsp',
			params: Ext.apply({	informacion:'ConfirmarSolicitud',
										operacion:	'Generar',
										pkcs7: vpkcs7,
										textoFirmado: vtextoFirmar,
										icProcSolic: vic_proc_solic
									}),
			callback: procesarConfirmar
		});
	}
	
	var elementosCarga = [
		{
			xtype:'panel',	id:'mensajeBloqueo_b',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype: 'panel',layout:'table',	width:550,	border:true,	layoutConfig:{ columns:2 },	style:'margin:0 auto;',
			defaults: {frame:false, border: true,width:350, height: 30,bodyStyle:'padding:2px'},
			items:[
				{	frame:true,	width:550,	colspan:2,	html:'<div align="center">Resumen de solicitudes Cargadas</div>'	},
				{	html:'<div align="left" class="formas">No. total de operaciones registradas</div>'	},
				{	width:200,	id:'totalOpera',	html:'<div align="right">0</div>'	},
				{	html:'<div align="left" class="formas">Monto total del importe de los documentos</div>'	},
				{	width:200,	id:'montoImpDocto',	html:'&nbsp;Aqui va ir importeDocto'	},
				{	html:'<div align="left" class="formas">Monto total del importe de los descuentos</div>'	},
				{	width:200,	id:'montoImpDesc',	html:'&nbsp;Aqui va ir importeDesc'	},
				{	height:115,	width:550,	colspan:2,
					html:'<div class="formas">Declaro bajo protesta de decir verdad que soy tenedor y propietario de los documentos'+ 
							'que aqu&iacute; se describen en forma electr&oacute;nica. Dicha declaraci&oacute;n tendr&aacute; plena validez para todos los efectos legales conducentes.<br><br>'+
							'Con fundamento en lo dispuesto por los art&iacute;culos 1205 y 1298-A del C&oacute;digo de Comercio, la informaci&oacute;n e instrucciones que el Intermediario y Nafin '+
							'se transmitan o comuniquen mutuamente mediante el sistema, tendr&aacute;n pleno valor probatorio y fuerza legal para acreditar la operaci&oacute;n realizada,'+
							'el importe de la misma, su naturaleza, as&iacute; como las caracter&iacute;sticas y alcance de sus instrucciones.</div>'
				}
			]
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'grid',	id:'gridResumen',	store:gridResumenData,	stripeRows:true,	loadMask:true,	height:400,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Nombre del Cliente',		tooltip:'Nombre del Cliente', dataIndex:'NOMBRE_CLI',		width:100,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'N�mero de Sirac',tooltip:'N�mero de Sirac',	dataIndex:'NO_SIRAC',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de documento',	tooltip:'N�mero de documento',	dataIndex:'NO_DOCTO',	width:120,	align:'center', resizable:true},
				{header:'Fecha Emisi�n del T�tulo de Cr�dito',	tooltip:'Fecha Emisi�n del T�tulo de Cr�dito',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center', sortable:true,	resizable:true},	//,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
				{header:'Fecha Vencimiento Descuento',	tooltip:'Fecha Vencimiento Descuento', dataIndex:'FECHA_VENC',
					width:120,	align:'center', sortable:true,	resizable:true},	//,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
				{header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tipo de cr�dito',		tooltip:'Tipo de cr�dito',	dataIndex:'TIPO_CREDITO',	width:150,	align:'center', sortable:true},
				{header:'Tasa Intermediario Financiero',	tooltip:'Tasa Intermediario Financiero',	dataIndex:'TASA_IF',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de Amortizaciones',	tooltip:'N�mero de Amortizaciones',	dataIndex:'NO_AMORTIZA',	width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Tabla de amortizaci�n',		tooltip:'Tabla de amortizaci�n',		dataIndex:'TABLA_AMORTIZA',width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Monto del Documento',		tooltip:'Monto del Documento',		dataIndex:'MONTO_DOCTO',	width:130,	align:'right', resizable:true,	sortable:true},
				{header:'Monto del Descuento',		tooltip:'Monto del Descuento',		dataIndex:'MONTO_DESC',		width:130,	align:'right', resizable:true,	sortable:true}
			],
			bbar:{
				buttonAlign:"center",
				items: [
					'-',' ',
					{
						xtype: 'button',	text:'Transmitir Solicitudes',	id:'btnConfirmarResumen', iconCls:'icoAceptar',
						handler: function(boton, evento) {
										
							NE.util.obtenerPKCS7(fnConfirmarResumenCallback, transmiteSolic.textoFirmar, barra.ic_proc_solic);
										
						}
					},' ','-',' ',{
						xtype:'button', id:'btnGeneraPdfPreAcuse', text:'Generar PDF', iconCls:'icoPdf'
					},{
						xtype:'button', id:'btnBajarPdfPreAcuse', text:'Bajar PDF', hidden:true
					},' ','-',' ',{
						xtype:'button',	text:'Cancelar',	id:'btnCancelarResumen', iconCls:'icoRechazar',
						handler: function(boton, evento) {
							Ext.Msg.confirm("Mensaje de p�gina web.","�Esta usted seguro de cancelar la operaci�n?",
								function(res){
									if(res=='no' || res == 'NO'){
										return;
									}else{
										pnl.el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url : '26forma03ext.data.jsp',
											params: Ext.apply({	informacion:'ConfirmarSolicitud',
																		operacion:	'Cancelar',
																		icProcSolic:barra.ic_proc_solic
																	}),
											callback: procesarConfirmar
										});
									}
							});
						}
					},' ','-'
				]
			}
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes',	border:false,	html:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes_b',	border:false,	html:'&nbsp;Algo'
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'grid',	id:'gridResumen_b',	store:gridResumen_bData,	stripeRows:true,	loadMask:true,	height:400,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Nombre del Emisor',	tooltip:'Nombre del Emisor', dataIndex:'NOMBRE_EMISOR',	width:150,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'Clase de Documento',	tooltip:'Clase de Documento',	dataIndex:'CLASE_DOCTO',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'Lugar de Emisi�n',	tooltip:'Lugar de Emisi�n',	dataIndex:'LUGAR',	width:120,	align:'center', resizable:true},
				{header:'Fecha de Emisi�n',	tooltip:'Fecha de Emisi�n',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center',sortable:true,	resizable:true},	//,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
				{header:'Domicilio de Pago',	tooltip:'Domicilio de Pago',	dataIndex:'DOMICILIO',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Destino del Cr�dito',	tooltip:'Destino del Cr�dito',	dataIndex:'DEST_CREDITO',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tasa de Interes',		tooltip:'Tasa de Interes',	dataIndex:'TASA_INTERES',	width:150,	align:'center', sortable:true},
				{header:'N�mero de Documento',	tooltip:'N�mero de Documento',	dataIndex:'NO_DOCTO',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'Fecha de Vencimiento',	tooltip:'Fecha de Vencimiento', dataIndex:'FECHA_VENC',
					width:120,	align:'center',	sortable:true,	resizable:true},	//,	renderer:Ext.util.Format.dateRenderer('d/m/Y')
				{header:'Importe de la Operaci�n',	tooltip:'Importe de la Operaci�n',	dataIndex:'MONTO_OPERA',	width:120,	align:'right', resizable:true,	sortable:true}
			]
		},{
			xtype:'grid',	id:'gridTotales',	store:totalesData,	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	anchor:'100%',
			width:880,	height:50,	title:'Totales', hidden:false,	frame:true, header:false,	style:'margin:0 auto;',
			columns: [
				{
					header: '',	dataIndex: 'TOTAL',	align:'right',	width:700, menuDisabled:true
				},{
					header: '',	dataIndex: 'TOTAL_MONTO',	width:180,	align:'center',	menuDisabled:true
				}
			]
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes_c',	border:false,	html:'&nbsp;Facultado por'
		}
	];
	
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
		hidden: (strEsquemaExtJS=='true')?true:false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Individual',
				id: 'btnCons1',					
				handler: function() {
					window.location = '26forma02ext.jsp';					
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Masiva</h1></b>',
				id: 'btnCons2',					
				handler: function() {
					window.location = '26forma03ext.jsp';					
				}
			}
		]
	};

	var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 900,
		heigth:'auto',
		style: 'margin:0 auto;',
		frame: false,
		border:false,
		hidden:true,
		bodyStyle: 'padding: 4px',
		labelWidth: 150,
		items: elementosCarga,
		buttonAlign: 'center'
	});

	var aviso = new Ext.Window({
		resizable: false,
		x: 330, y:200,
		width: 200,
		height: 100,
		layout:'fit',
		id: 'winAviso',
		closeAction: 'hide',
		items: [{
			xtype:'panel',
			html:'<div class="formas" align="center"><br>Microsoft Excel 97<br>Microsoft Excel 2000<br>Microsoft Excel XP<br></div>'
		}],
		title: 'Versiones v�lidas de Excel'
	});
	
	var elementosForma = [
		{
			xtype:'panel',	id:'mensajeBloqueo',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype:'hidden',	id:'_tipoCartera', name:'TipoCartera',	value:''
		},{
			xtype:'hidden',	id:'_desc_intermediario', name:'desc_intermediario',	value:''
		},{
			xtype:'hidden',	id:'tipoPlazo', name:'TipoPlazo',	value:''
		},{
			xtype:'hidden',	id:'_tipoArchivo', name:'TipoArchivo',	value:''
		},{
			xtype:'panel', defaults: {layout:'form',width:600,	bodyStyle:'padding:5px 5px 0'},
			items:[
				{
					id:'radioGrupoPlazo',	defaults: {	msgTarget: 'side',anchor:'-20'},
					items:[
					{
						xtype:'radiogroup',	id:'radioGp',	anchor:'60%',	style:'margin:0 auto;',	columns:2,
						items:[
							{boxLabel: 'Factoraje', id:'plazoFactoraje',	name: 'TipoPlazoAux', inputValue: 'F', checked: false,
								listeners:{
									check:	function(radio){
													//recargar(0);
													if (radio.checked){
														Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
														Ext.getCmp('tipoPlazo').setValue('F');
														//gridLayoutData_a.removeAt(11);
														gridLayoutData_a.loadData(dataLayout_a);
														gridLayoutData_b.loadData(dataLayout_b);
													}
												}
								}
							},{boxLabel: 'Cr�dito', id:'plazoCredito',	name:'TipoPlazoAux', inputValue:'C',	checked:false,
								listeners:{
									check:	function(radio){
													//recargar(0);
													if (radio.checked){
														Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
														Ext.getCmp('tipoPlazo').setValue('C');
														
														var record = Ext.data.Record.create([
															{name:'NUMERO'},
															{name:'DESCRIPCION'},
															{name:'TIPO_DATO'},
															{name:'LONGITUD'},
															{name:'OBLIGATORIO'}
														]);
														
														//gridLayoutData_a.insert(11, new record({NUMERO:'12', DESCRIPCION:'Destino del Cr�dito', TIPO_DATO:'Texto', LONGITUD:'200', OBLIGATORIO:'Si'}));
														gridLayoutData_a.loadData(dataLayout_a);
														gridLayoutData_b.loadData(dataLayout_b);
													}
												}
								}
							}
						]
					}
					]
				}
			]
		},{
			xtype:'panel', defaults: {layout:'form',width:600,	bodyStyle:'padding:5px 5px 0'},
			items:[
				{
					id:'panelRadios',	defaults: {	msgTarget: 'side',anchor:'-20'},
					items:[
					{
						xtype:'radiogroup',	id:'radioGpArchivos',	anchor:'60%',	style:'margin:0 auto;',	columns:3,
						items:[
							{boxLabel:'Excel',	id:'radioE',	name:'TipoArchivoA', inputValue:'E', checked:false,
								listeners:{
									check:function(radio){
												//recargar(0);
												if (radio.checked){
													Ext.getCmp('form_file').regex = /^.+\.([xX][lL][sS])$/;
													Ext.getCmp('form_file').regexText = 'El archivo debe tener extensi�n .xls';
													Ext.getCmp('_tipoArchivo').setValue('E');
													Ext.getCmp('form_file').validate();
													aviso.show();
												}
											},
									focus:function(radio){
												aviso.show();
											}
								}
							},{boxLabel:'TXT',	id:'radioT',	name:'TipoArchivoA', inputValue:'T',	checked:false,
								listeners:{
									check:	function(radio){
													//recargar(0);
													if (radio.checked){
														Ext.getCmp('form_file').regex = /^.+\.([tT][xX][tT])$/;
														Ext.getCmp('form_file').regexText = 'El archivo debe tener extensi�n .txt';
														Ext.getCmp('_tipoArchivo').setValue('T');
														Ext.getCmp('form_file').validate();
														aviso.hide();
													}
												}
								}
							},{boxLabel:'ZIP', id:'radioZ',	name:'TipoArchivoA', inputValue:'Z',	checked:false,
								listeners:{
									check:	function(radio){
													//recargar(0);
													if (radio.checked){
														Ext.getCmp('form_file').regex = /^.+\.([zZ][iI][pP])$/;
														Ext.getCmp('form_file').regexText = 'El archivo debe tener extensi�n .zip';
														Ext.getCmp('_tipoArchivo').setValue('Z');
														Ext.getCmp('form_file').validate();
														aviso.hide();
													}
												}
								}
							}
						]
					}
					]
				}
			]
		},{
			xtype:'panel',	layout:'column',	anchor:'100%',	defaults: {	bodyStyle:'padding:6px'	}, border:false, 	style:'margin:0 auto;', width: 600,
			items:[
				{
					xtype: 'button',	columnWidth: .05,	autoWidth: true,	autoHeight: true,	iconCls: 'icoAyuda',
					handler: function() {
						if(Ext.getCmp('tipoPlazo').getValue()!=''){
							if (!Ext.getCmp('panelLayout').isVisible()){
								Ext.getCmp('panelLayout').show();
								Ext.EventManager.onWindowResize(fp.doLayout(), fp);
							}
						}else{
							Ext.MessageBox.alert('Aviso','Seleccione un tipo de plazo');
						}
					}
				},{
					xtype:'panel',	id:'pnlArchivo',	columnWidth:.90,	anchor:'100%',	layout:'form',	fileUpload:true,	labelWidth:2,	border:false,
					items: [
						{
							xtype:'panel', layout:'form', labelAlign:'top', anchor:'100%',defaults: {bodyStyle:'padding:5px',	msgTarget:'side',	anchor:'-20'},
							items:[
								{
									xtype: 'fileuploadfield',
									id: 'form_file',
									anchor: '95%',
									allowBlank: true,
									fieldLabel:'Ruta del Archivo de Origen',
									blankText:	'Debe seleccionar una ruta de archivo.',
									emptyText: 'Seleccione un archivo',
									name: 'txtarchivo',
									//buttonCfg: {iconCls: 'upload-icon'},
									buttonText: 'Examinar...'
								}
							],
							buttons:[
								{
									text:'Continuar',	id:'btnContinuar',	iconCls:'icoContinuar',	//formBind:true,	
									handler: function(boton, evento) {
													Ext.getCmp('panelLayout').hide();
													Ext.getCmp('panelErrores').hide();
													Ext.getCmp('btnProcesar').disable();
													if(Ext.isEmpty(Ext.getCmp('tipoPlazo').getValue())){
														if(!Ext.getCmp('plazoFactoraje').checked&&!Ext.getCmp('plazoCredito').checked){
															Ext.Msg.alert('Mensaje de p�gina web.','Seleccione un tipo de plazo.');
															return;
														}
													}
													if(Ext.getCmp('tipoPlazo').getValue()=="N"){
														Ext.Msg.alert('Mensaje de p�gina web.','No existen bases de operaci�n registradas.');
														return;
													}
													if(!Ext.getCmp('radioGpArchivos').items.items[0].checked&&!Ext.getCmp('radioGpArchivos').items.items[1].checked&&!Ext.getCmp('radioGpArchivos').items.items[2].checked){
														Ext.Msg.alert('Mensaje de p�gina web.','Seleccione un tipo de archivo.');
														return;
													} else {
														if(Ext.isEmpty(Ext.getCmp('form_file').getValue())){
															 Ext.Msg.alert('Mensaje de p�gina web.','Inserte la ruta del archivo',
																function(){
																	Ext.getCmp('form_file').focus();
																	return;
																}
															 );
															 return;
														 } else {
															var Archivo = Ext.getCmp('form_file').getValue();
															var ext = 'xls';
															if(Ext.getCmp('radioGpArchivos').items.items[0].checked){
																var tipo = 5;
																ext = 'xls';
															}
															if(Ext.getCmp('radioGpArchivos').items.items[1].checked){
																var tipo = 1;
																ext = 'txt';
															}
															if(Ext.getCmp('radioGpArchivos').items.items[2].checked){
																var tipo = 7;
																ext = 'zip';
															}
															if(!formatoValido(Archivo, tipo)) {
																 Ext.Msg.alert('Mensaje de p�gina web.','El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado. Debe tener extensi�n '+ext,
																	function(){
																		Ext.getCmp('form_file').focus();
																		Ext.getCmp('form_file').selectText();
																	}
																 );
																 return;
															}
														 }
													}

													// * - * - * - * - * Carga de archivo�s

													fp.getForm().submit({
														url: '26forma03file.data.jsp', waitMsg: 'Enviando datos...',	waitTitle:'Procesando registros',
														success:function(form, action){
																if (action.result.msg != "0"){
																	Ext.Msg.alert("Mensaje de p�gina web.",action.result.msg,
																		function(){
																			window.location = '26forma03ext.jsp';
																		}
																	);
																	return;
																}
																barra.ic_proc_solic = action.result.ic_proc_solic;
																pnl.el.mask('Enviando...', 'x-mask-loading');
																Ext.Ajax.request({
																	url: 		'26forma03ext.data.jsp',
																	params: 	{
																		informacion:		'ActualizarAvance',
																		proceso: 			barra.ic_proc_solic
																	},
																	callback: 				procesaActualizarAvance
																});
														},
														failure: function(form,action){
																		var json = action.response.responseText;
																		var mensaje = Ext.util.JSON.decode(json).msg;
																		Ext.Msg.alert("Error",mensaje,
																			function(){
																				window.location = '26forma03ext.jsp';
																			}
																		);
														}
														//NE.util.mostrarSubmitError
													});

									} //fin handler
								},{
									text:'Regresar',	id:'btnRegresar',	iconCls:'icoLimpiar', hidden:true,
									handler: function(boton, evento) {window.location = '26forma03ext.jsp';}
								}
							]
						}
					]
				}
			]
		},{
			xtype:'panel', id:'panelLayout',	border:true, heigth:700,	anchor:'100%',	autoScroll:true, frame:false,
			title: '<div align="center">Layout Registro Masivo de Operaciones</div>',
			items:[
				{
					xtype:'grid',	id:'gridLayout',	store:gridLayoutData_a,	stripeRows:true,	loadMask:true,	height:630,	width:675,	anchor:'100%',	frame:false,
					columLines:true,	enableColumnMove:false, style:'margin:0 auto;',
					columns: [
						{header:'No.',		tooltip:'No. de Campo', dataIndex:'NUMERO', width:70,	align:'center', resizable:false, menuDisabled:true,
							renderer: function(value, metadata, record, rowindex, colindex, store) {
												if(Ext.getCmp('tipoPlazo').getValue()=='C' && rowindex > 10){
													return Number(value)+1
												}else{
													return value;
												}
								}
						},
						{header:'Descripci�n',		tooltip:'Descripci�n',	dataIndex:'DESCRIPCION',	width : 150,	align:'center', resizable:true,	menuDisabled:true,
							renderer: function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return value;
							}
						},
						{header:'Tipo',		tooltip:'Tipo',	dataIndex:'TIPO_DATO',	width : 100,align:'center', resizable:false,menuDisabled:true},
						{header:'Longitud M�xima',	tooltip:'Longitud M�xima',	dataIndex:'LONGITUD', width : 90,align:'center', resizable:false, menuDisabled:true},
						{header:'Obligatorio / Observaciones',	tooltip:'Obligatorio / Observaciones', dataIndex:'OBLIGATORIO', width:950, align:'left', resizable:true, menuDisabled:true}
					]
				},{
					xtype:'grid',	id:'gridLayout_b',	store:gridLayoutData_b,	stripeRows:true,	loadMask:true,	height:230,	width:675,	anchor:'100%',	frame:false,
					columLines:true,	enableColumnMove:false,	style:'margin:0 auto;',
					title:'<div>'+
						'Amortizaciones por grupos de 4 campos, comenzando con el campo no. 29.<br>'+
						'S&oacute;lo son requeridos cuando el Tipo de Amortizaci&oacute;n es "PLAN DE PAGOS" (clave 5) � "PAGOS IGUALES CAP. MAS INTS."'+
						'(clave 3), en caso	contrario el contenido de las amortizaciones es ignorado.<br>'+
						'Para "PLAN DE PAGOS", el n&uacute;mero de grupos debe coincidir con el especificado en el campo no. 18 (N&uacute;mero de Amortizaciones),'+
						'y para "PAGOS IGUALES CAP. MAS INTS.", s&oacute;lo se requiere un grupo, la cual debe corresponder a la primera'+ "('P') o &uacute;ltima('U') amortizaci&oacute;n, seg&uacute;n se"+
						'especifique en el campo no. 27 (Tipo de Amortizaci&oacute;n de Ajuste).'+
						'</div>',
					columns: [
						{header:'No.',		tooltip:'No. de Campo', dataIndex:'NUMERO', width:70,	align:'center', resizable:false, menuDisabled:true,
							renderer: function(value, metadata, record, rowindex, colindex, store) {
												metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
												if(Ext.getCmp('tipoPlazo').getValue()=='C'){
												return Number(value)+1
												}else{
													return value;
												}
								}
						},
						{header:'Descripci�n',		tooltip:'Descripci�n',	dataIndex:'DESCRIPCION',	width : 150,	align:'center', resizable:true,	menuDisabled:true,
							renderer: function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return value;
							}
						},
						{header:'Tipo',		tooltip:'Tipo',	dataIndex:'TIPO_DATO',	width : 100,align:'center', resizable:false,menuDisabled:true},
						{header:'Longitud M�xima',	tooltip:'Longitud M�xima',	dataIndex:'LONGITUD', width : 90,align:'center', resizable:false, menuDisabled:true},
						{header:'Obligatorio / Observaciones',tooltip:'Obligatorio / Observaciones', dataIndex:'OBLIGATORIO', width:1200, align:'left', resizable:true, menuDisabled:true}
					]
				},{
					xtype:'grid',	id:'gridLayout_c',	store:gridLayoutData_c,	stripeRows:true,	loadMask:true,	height:40,	width:675,	anchor:'100%',	frame:false,
					columLines:true,	enableColumnMove:false, style:'margin:0 auto;', header:false,
					columns: [
						{header:'',		tooltip:'No. de Campo', dataIndex:'NUMERO', width:70,	align:'center', resizable:false, menuDisabled:true},
						{header:'',		tooltip:'Descripci�n',	dataIndex:'DESCRIPCION',	width : 150,	align:'center', resizable:true,	menuDisabled:true,
							renderer: function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return value;
							}
						},
						{header:'',		tooltip:'Tipo',	dataIndex:'TIPO_DATO',	width : 100,align:'center', resizable:false,menuDisabled:true},
						{header:'',	tooltip:'Longitud M�xima',	dataIndex:'LONGITUD', width : 90,align:'center', resizable:false, menuDisabled:true},
						{header:'',tooltip:'Obligatorio / Observaciones', dataIndex:'OBLIGATORIO', width:250, align:'center', resizable:false, menuDisabled:true}
					]
				}
			],
			tools: [
				{
					id: 'close',
					handler: function(evento, toolEl, panel, tc) {
						panel.hide();
					}
				}
			],
			bbar:{
				items:[
					'->','-',' ',
					{
						iconCls:'rechazar',
						handler: function(){Ext.getCmp('panelLayout').hide();}
					}
				]
			}
		},{
			xtype:'panel', id:'panelErrores',	layout:'form', anchor:'100%',defaults: {bodyStyle:'padding:5px',	msgTarget:'side',	anchor:'-20'}, labelWidth:10, buttonAlign:"center",
			items:[
				{
				xtype:'compositefield',	combineErrors:false,	msgTarget:'side',
				items: [
							{
								xtype:'displayfield',	value:'Documentos sin Errores',	width:300
							},{
								xtype: 'displayfield',	value: 'Detalle de Documentos con Errores'
							}
				]},
				{
				xtype:'compositefield',	combineErrors:false,	msgTarget:'side',
				items: [
					{
						xtype:'textarea',	name:'_sinErr',	id:'sinErr',	readOnly:true,	anchor:'100%',	width:300,	height:300,	value:''
					},{
						xtype:'textarea',	name:'_conErr',	id:'conErr',	readOnly:true,	width:300,	height:300,	anchor:'100%',	value:''
					}
				]}
			],
			buttons:[
				{
					text:'Procesar', id:'btnProcesar', iconCls:'icoAceptar', disabled:true
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		heigth:'auto',
		style: 'margin:0 auto;',
		fileUpload:true,
		frame: true,
		border:true,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: false,
		tbar:[
			'->',
			{
				text:'Asistencia en la Operaci�n', iconCls:'icoAyuda',
				handler: function(boton, evento) {
								var ventana = Ext.getCmp('winAsistencia');
								if (ventana) {
									ventana.destroy();
								}
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '26forma03ext.data.jsp',
									params: {informacion: "Asistencia", TipoCartera:Ext.getCmp('_tipoCartera').getValue()},
									callback: procesaAsistencia
								});
				} //fin handler
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			fp, fpCarga,	fpAcuse,
			NE.util.getEspaciador(10)
		]
	});
	Ext.getCmp('panelLayout').hide();
	Ext.getCmp('panelErrores').hide();
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '26forma03ext.data.jsp',
		params: {informacion:"valoresIniciales"},
		callback: procesaValoresIniciales
	});

});