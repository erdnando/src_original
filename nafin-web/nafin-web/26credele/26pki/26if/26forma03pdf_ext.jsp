<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.anticipos.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<% 

JSONObject jsonObj = new JSONObject();

String acuse			= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String aux_acuse		= (request.getParameter("aux_acuse")==null)?"":request.getParameter("aux_acuse");
String iNoSolicCarga	= (request.getParameter("iNoSolicCarga")==null)?"":request.getParameter("iNoSolicCarga");
String bdMtoDoctos	= (request.getParameter("bdMtoDoctos")==null)?"":request.getParameter("bdMtoDoctos");
String bdMtoDsctos	= (request.getParameter("bdMtoDsctos")==null)?"":request.getParameter("bdMtoDsctos");
String fechaCarga		= (request.getParameter("fechaCarga")==null)?"":request.getParameter("fechaCarga");
String horaCarga		= (request.getParameter("horaActual")==null)?"":request.getParameter("horaActual");
String usuario			= (request.getParameter("usuario")==null)?"":request.getParameter("usuario");
Vector lovDatos 	= null;
Vector lovRegistro  = null;

try {

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String mensajeBloqueo = (strTipoUsuario.equals("IF") && autSolEJB.esIfBloqueado(iNoCliente,"0"))?
			"Las operaciones enviadas estarán detenidas por el Usuario Administrador":
			"";
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	//pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	pdfDoc.addText(mensajeBloqueo,"formas",ComunesPDF.CENTER);

	pdfDoc.setTable(2, 35);
	pdfDoc.setCell("Resumen de Solicitudes Cargadas","formasmenB",ComunesPDF.CENTER,2);
	pdfDoc.setCell("No. de Acuse:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(acuse,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("No. total de operaciones registradas:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(iNoSolicCarga,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los documentos:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(bdMtoDoctos,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los descuentos:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(bdMtoDsctos,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("Fecha de carga:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(fechaCarga,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("Hora de carga:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(horaCarga,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell("Usuario:","formasMini",ComunesPDF.RIGHT);
	pdfDoc.setCell(usuario,"formasMini",ComunesPDF.RIGHT);
	pdfDoc.addTable();

	pdfDoc.setTable(13, 100);
	pdfDoc.setCell("Folio de Operación","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre del Cliente","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Sirac","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Documento","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Emisión Título de Crédito","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Vencimiento Descuento","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de Crédito","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Tasa Intermediario Financiero","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Amortizaciones","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Tabla Amortización","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Documento","formasmenB",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Descuento","formasmenB",ComunesPDF.CENTER);

	String folio="",	cliente="", sirac="", noDocto="", fec_emision="", fec_venc, moneda="",tipoCred, tasaIf="", noAmort="", tabAmort="", montoDocto="",montoDesc="";
	
	lovDatos = autSolEJB.getDoctosCargados(aux_acuse,"","","","","","");
	for(int i = 0; i < lovDatos.size(); i++){
		lovRegistro = (Vector) lovDatos.elementAt(i);
		folio=(lovRegistro.elementAt(9)).toString();
		cliente=(lovRegistro.elementAt(0)).toString();
		sirac=(lovRegistro.elementAt(1)).toString();
		noDocto=(lovRegistro.elementAt(3)).toString();
		fec_emision=(lovRegistro.elementAt(8)).toString();
		fec_venc=(lovRegistro.elementAt(7)).toString();
		moneda=(lovRegistro.elementAt(2)).toString();
		tipoCred=(lovRegistro.elementAt(6)).toString();
		tasaIf=(lovRegistro.elementAt(15)).toString();
		noAmort=(lovRegistro.elementAt(14)).toString();
		tabAmort=(lovRegistro.elementAt(16)).toString();
		montoDocto=(lovRegistro.elementAt(4)).toString();
		montoDesc=(lovRegistro.elementAt(5)).toString();
		
		pdfDoc.setCell(folio,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(cliente,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(sirac,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(noDocto,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_emision,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_venc,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(moneda,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(tipoCred,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(tasaIf,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(noAmort,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell(tabAmort,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDocto,"formasMini",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDesc,"formasMini",ComunesPDF.CENTER);
	}
	if(lovDatos.size() < 1){
		pdfDoc.setCell("No se encontró ningún registro","formasMini",ComunesPDF.CENTER,13);
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>