<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<% 

JSONObject jsonObj = new JSONObject();

String TipoCartera = (request.getParameter("TipoCartera")==null)?"":request.getParameter("TipoCartera").trim();

try {

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	Vector vecFilas 	= null;
	Vector vecColumnas 	= null;
	int i = 0;

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	vecFilas = autSolEJB.sGetDatosBO(iNoCliente,TipoCartera);

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual_a.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual   = fechaActual_a.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

	pdfDoc.addText("\n\nPara hacer más ágil el registro de operaciones, se muestran las características con las que puede solicitar su crédito.\n\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Datos con los que debe realizar el registro de su operación","formas",ComunesPDF.LEFT);

	pdfDoc.setTable(10,100);
	for (i=0;i<vecFilas.size();i++){
		if(i==0){
			pdfDoc.setCell("Tipo Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Crédito","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre del Emisor","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa Base\nIntermediario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Amortización","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo\nMínimo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo\nMáximo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Periodicidad de \nPago a Capital","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Interés","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Renta","celda01",ComunesPDF.CENTER);
		}	
		vecColumnas = (Vector) vecFilas.get(i);
			
		String TipoPlazo = (String) vecColumnas.elementAt(0);
		String TipoCred  = (String) vecColumnas.elementAt(1);
		String Emisor 	 = (String) vecColumnas.elementAt(2);
		String Tasa 	 = (String) vecColumnas.elementAt(3);
		String TipoAmort = (String) vecColumnas.elementAt(4);
		String PMin 	 = (String) vecColumnas.elementAt(5);
		String PMax 	 = (String) vecColumnas.elementAt(6);
		String Periodicidad = (String) vecColumnas.elementAt(7);
		String TipoInteres 	 = (String) vecColumnas.elementAt(8);
		String TipoRenta 	 = (String) vecColumnas.elementAt(9);

		pdfDoc.setCell(TipoPlazo+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(TipoCred+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(Emisor+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(Tasa+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(TipoAmort+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(PMin+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(PMax+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(Periodicidad+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(TipoInteres+" ","formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(TipoRenta+" ","formasmen",ComunesPDF.CENTER);
	}
	pdfDoc.addTable();
	if(i==0){
		pdfDoc.addText("No existen registros parametrizados","formas",ComunesPDF.RIGHT);	
	}
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>