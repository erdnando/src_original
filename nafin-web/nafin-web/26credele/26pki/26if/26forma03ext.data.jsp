<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		netropology.utilerias.Seguridad,
		com.netro.distribuidores.*, 
		com.netro.afiliacion.*,
		com.netro.descuento.*,
		com.netro.credito.*,
		com.netro.procesos.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf"%>
<%

String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";

if (informacion.equals("valoresIniciales"))	{
	
	//session.removeAttribute("datosProceso");
	JSONObject jsonObj = new JSONObject();

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	ManejoServicio manejoServicio =ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	CargaCredito BeanCargaCredito = ServiceLocator.getInstance().lookup("CargaCreditoEJB", CargaCredito.class);

	ParametrosDescuento paramBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	String nombreIf  = paramBean.getNombreIF(iNoCliente);
	jsonObj.put("desc_intermediario", nombreIf);

	int tipoPiso = BeanAfiliacion.getTipoPiso(iNoCliente);
	jsonObj.put("tipoCartera", new Integer(tipoPiso));

	List plaz = new ArrayList();
	String tipo = "";
	int plazos=0;
	plaz = BeanCargaCredito.getPlazo(iNoCliente);
	Iterator iter = plaz.iterator();
	while (iter.hasNext()){
		tipo = (String)iter.next();
		plazos++;
	}
	jsonObj.put("tipo", tipo);
	jsonObj.put("plazos", new Integer(plazos) );
	
	String mensajeBloqueo = (strTipoUsuario.equals("IF") && autSolEJB.esIfBloqueado(iNoCliente,"0"))?
			"Las operaciones enviadas estarán detenidas por el Usuario Administrador":
			"";
	jsonObj.put("mensajeBloqueo", mensajeBloqueo);
	jsonObj.put("NoIf", iNoCliente);
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ActualizarAvance")){

	JSONObject jsonObj = new JSONObject();

	String proceso 			= (request.getParameter("proceso")			== null)?"0":request.getParameter("proceso");

	AccesoDB con 			= new AccesoDB();
	String	qrySentencia= "";
	PreparedStatement	ps	= null;
	ResultSet			rs	= null;
	boolean			bOk	= true;

	String estatusProceso = "PR";
	int	 NumProcesados  = 0;

	try {
		con.conexionDB();
		qrySentencia = 
			" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0)"   +
			"   FROM bit_carga_credele"   +
			"  WHERE ic_proc_solic = ?"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(proceso));
		rs = ps.executeQuery();
		if(rs.next()) {
			estatusProceso = rs.getString(1)==null?"PR":rs.getString(1);
			NumProcesados  = rs.getInt(2);
		}
		rs.close();ps.close();

		jsonObj.put("estatusProceso", estatusProceso);

	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("obtieneErrores")){

	JSONObject jsonObj = new JSONObject();
	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	String ic_proc_solic = (request.getParameter("ic_proc_solic")	== null)?"0":request.getParameter("ic_proc_solic");

	StringBuffer sSinError = new StringBuffer();
	StringBuffer sConError = new StringBuffer();

	List lDetalle = autSolEJB.getDetalleCarga(ic_proc_solic);
	List lRegOK		= (ArrayList)lDetalle.get(0);
	List lRegError	= (ArrayList)lDetalle.get(1);

	for(int i=0;i<lRegOK.size();i++) {
		sSinError.append(lRegOK.get(i).toString()+"\n");
	} //for(int i=0;i<lRegOK.size();i++)

	//System.out.println("sSinError.toString() ES - - - - "+sSinError.toString());

	for(int i=0;i<lRegError.size();i++) {
		sConError.append(lRegError.get(i).toString()+"\n");
	} //for(int i=0;i<lRegOK.size();i++)
	jsonObj.put("sSinError", sSinError.toString());
	jsonObj.put("sConError", sConError.toString());
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("transmitirSolicitudes")){

	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	JSONObject jsonObj = new JSONObject();
	String ic_proc_solic = (request.getParameter("ic_proc_solic")==null)?"":request.getParameter("ic_proc_solic");
	//String nombreArchivoErr = (request.getParameter("nombreArchivoErr")==null)?"":request.getParameter("nombreArchivoErr");

	StringBuffer seguridad	=new StringBuffer("");
	String fechaConvenio		= "";
	String lugarFirma			= "";

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	java.util.Date fechaHoy = new java.util.Date();
	String	diaHoy	= new java.text.SimpleDateFormat("dd").format(fechaHoy);
	int		mesAct	= Integer.parseInt(new java.text.SimpleDateFormat("MM").format(fechaHoy));
	String	anioAct	= new java.text.SimpleDateFormat("yyyy").format(fechaHoy);
			mesAct--;

	String 	nombre_personal	= "";
	String 	paterno_personal	= "";
	String 	materno_personal	= "";
	String 	puesto_personal	= "";

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	List lTotales = autSolEJB.getTotalesCargaM(ic_proc_solic);
	String totalOpera = lTotales.get(0).toString();
	String montoDocto = Comunes.formatoMN(lTotales.get(1).toString());
	String montoDesc  = Comunes.formatoMN(lTotales.get(2).toString());

	jsonObj.put("totalOpera",	totalOpera);
	jsonObj.put("montoDocto",	montoDocto);
	jsonObj.put("montoDesc",	montoDesc);

	int		numDoctosMN		= 0;
	int		numDoctosUSD	= 0;
	double	totMontoMN		= 0;
	double	totMontoUSD		= 0;

	HashMap hm = new HashMap();

	seguridad.append(
		"Nombre del Cliente|Numero Sirac|Numero de Documento|Fecha Emision titulo de Credito|Fecha de vencimiento dscto|Moneda|Tipo de credito|Tasa Intermediario Financiero|"+
		"Numero de Amortizaciones|Tabla Amortizacion|Monto del Documento|Monto del Descuento\n"	);

	List newReg = new ArrayList();
	List newReg_b = new ArrayList();

	if ( ic_proc_solic.equals("")) ic_proc_solic = "0";
	Vector lovDatos = autSolEJB.getDoctosCargadosTmp(Integer.parseInt(ic_proc_solic));
	Vector lovRegistro = null;
	for(int i = 0; i < lovDatos.size(); i++) {
		hm = new HashMap();
		lovRegistro = (Vector) lovDatos.elementAt(i);
		hm.put("NOMBRE_CLI",		lovRegistro.elementAt(0));
		hm.put("NO_SIRAC",		lovRegistro.elementAt(1));
		hm.put("NO_DOCTO",		lovRegistro.elementAt(3));
		hm.put("FECHA_EMISION",	lovRegistro.elementAt(8));
		hm.put("FECHA_VENC",		lovRegistro.elementAt(7));
		hm.put("MONEDA",			lovRegistro.elementAt(2));
		hm.put("TIPO_CREDITO",	lovRegistro.elementAt(6));
		hm.put("TASA_IF",			lovRegistro.elementAt(10));
		hm.put("NO_AMORTIZA",	lovRegistro.elementAt(11));
		hm.put("TABLA_AMORTIZA",lovRegistro.elementAt(9));
		hm.put("MONTO_DOCTO",	"$ "+lovRegistro.elementAt(4));
		hm.put("MONTO_DESC",		"$ "+lovRegistro.elementAt(5));
		newReg.add(hm);
		seguridad.append(
			lovRegistro.elementAt(0)+"|"+lovRegistro.elementAt(1)+"|"+lovRegistro.elementAt(3)+"|"+
			lovRegistro.elementAt(8)+"|"+lovRegistro.elementAt(7)+"|"+lovRegistro.elementAt(2)+"|"+
			lovRegistro.elementAt(6)+"|"+lovRegistro.elementAt(10)+"|"+lovRegistro.elementAt(11)+"|"+
			lovRegistro.elementAt(9)+"|"+lovRegistro.elementAt(4)+"|"+lovRegistro.elementAt(5)+"\n"
		);
		if(i==0){
			fechaConvenio	= (String)lovRegistro.get(18);
			lugarFirma		= (String)lovRegistro.get(14);
		}
		if("1".equals(lovRegistro.elementAt(19).toString())){
			numDoctosMN++;
			totMontoMN += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}else if("54".equals(lovRegistro.elementAt(19).toString())){
			numDoctosUSD++;
			totMontoUSD += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}

		puesto_personal = (String)lovRegistro.get(21);
		nombre_personal = (String)lovRegistro.get(22);
		paterno_personal = (String)lovRegistro.get(23);
		materno_personal = (String)lovRegistro.get(24);
	}

	for(int i = 0; i < lovDatos.size(); i++) {
		hm = new HashMap();
		lovRegistro = (Vector) lovDatos.elementAt(i);
		hm.put("NOMBRE_EMISOR",	lovRegistro.elementAt(12));
		hm.put("CLASE_DOCTO",	lovRegistro.elementAt(13));
		hm.put("LUGAR",			lovRegistro.elementAt(14));
		hm.put("FECHA_EMISION",	lovRegistro.elementAt(8));
		hm.put("DOMICILIO",		lovRegistro.elementAt(15));
		hm.put("TASA_INTERES",	lovRegistro.elementAt(10));
		hm.put("NO_DOCTO",		lovRegistro.elementAt(3));
		hm.put("FECHA_VENC",		lovRegistro.elementAt(7));
		hm.put("MONTO_OPERA",	"$ "+lovRegistro.elementAt(5));
		hm.put("DEST_CREDITO",	lovRegistro.elementAt(25));
		newReg_b.add(hm);
	}
	jsonObj.put("strNombre", strNombre);
	jsonObj.put("fechaConvenio", fechaConvenio);
	jsonObj.put("diaHoy", diaHoy);
	jsonObj.put("mesAct", meses[mesAct]);
	jsonObj.put("anioAct", anioAct);

	if (newReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(newReg);
		jsonObj.put("registros", jsObjArray);
		jsonObj.put("txtFirmar", seguridad.toString());
	}

	if (newReg_b != null){
		JSONArray jsObjArray_b = new JSONArray();
		jsObjArray_b = JSONArray.fromObject(newReg_b);
		jsonObj.put("registros_b", jsObjArray_b);
	}

	jsonObj.put("numDoctosMN", new Integer(numDoctosMN));
	jsonObj.put("totMontoMN", new Double(totMontoMN));
	jsonObj.put("numDoctosUSD",new Integer(numDoctosUSD));
	jsonObj.put("totMontoUSD",new Double( totMontoUSD));

	jsonObj.put("puesto_personal", puesto_personal);
	jsonObj.put("nombre_personal", nombre_personal);
	jsonObj.put("paterno_personal", paterno_personal);
	jsonObj.put("materno_personal", materno_personal);
	//jsonObj.put("nombreArchivoErr", strDirecVirtualTemp + nombreArchivoErr);	//Esta parte no esta especificada en los archivo originales

	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();	
	
}else if (informacion.equals("Asistencia")){

	JSONObject jsonObj = new JSONObject();
	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	String TipoCartera = (request.getParameter("TipoCartera")==null)?"":request.getParameter("TipoCartera").trim();

	Vector vecFilas 	= null;
	Vector vecColumnas 	= null;
	int i = 0;
	List newReg = new ArrayList();
	vecFilas = autSolEJB.sGetDatosBO(iNoCliente,TipoCartera);
	for (i=0;i<vecFilas.size();i++){
		vecColumnas = (Vector) vecFilas.get(i);
		HashMap hm = new HashMap();
		String TipoPlazo		= (String) vecColumnas.elementAt(0);
		String TipoCred		= (String) vecColumnas.elementAt(1);
		String Emisor			= (String) vecColumnas.elementAt(2);
		String Tasa 	 		= (String) vecColumnas.elementAt(3);
		String TipoAmort 		= (String) vecColumnas.elementAt(4);
		String PMin 	 		= (String) vecColumnas.elementAt(5);
		String PMax 	 		= (String) vecColumnas.elementAt(6);
		String Periodicidad 	= (String) vecColumnas.elementAt(7);
		String TipoInteres 	= (String) vecColumnas.elementAt(8);
		String TipoRenta 		= (String) vecColumnas.elementAt(9);
		hm.put("TIPO_PLAZO",TipoPlazo);
		hm.put("TIPO_CREDITO",TipoCred);
		hm.put("EMISOR",Emisor);
		hm.put("TASA",Tasa);
		hm.put("TIPO_AMORT",TipoAmort);
		hm.put("PMIN",PMin);
		hm.put("PMAX",PMax);
		hm.put("PERIODICIDAD",Periodicidad);
		hm.put("TIPO_INTERES",TipoInteres);
		hm.put("TIPO_RENTA",TipoRenta);
		newReg.add(hm);
	}
	if (newReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(newReg);
		jsonObj.put("registros", jsObjArray);
	}

	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ConfirmarSolicitud")){

	ManejoServicio manejoServicio = ServiceLocator.getInstance().lookup("ManejoServicioEJB", ManejoServicio.class);
	manejoServicio.getEstadoDelServicioIfCE(iNoCliente);

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	JSONObject jsonObj = new JSONObject();
	String icProcSolic = (request.getParameter("icProcSolic")==null)?"":request.getParameter("icProcSolic");
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");

	int icSolicPortal = 0, iNoSolicCarga = 0;
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdf2=new SimpleDateFormat("hh:mm:ss");
	BigDecimal bdMtoDoctos=new BigDecimal("0.0");
	BigDecimal bdMtoDsctos=new BigDecimal("0.0");
	
	Vector lovDatos 	= null;
	Vector lovRegistro  = null;

	String _acuse = "500", mensajeFirma="";
	String pkcs7							=	request.getParameter("pkcs7");
	String externContent					=	request.getParameter("textoFirmado");
	String folioCert						=	"";
	char getReceipt						=	'Y';
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
	if(operacion.equals("Generar")) {
		//seguimos
		if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = acuse.toString();
			Seguridad s = new Seguridad();
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				mensajeFirma = "<b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo:"+_acuse+"</b>";
				if ( icProcSolic.equals("")) icProcSolic = "0";
				lovDatos = autSolEJB.ovTransmitirDoctos( Integer.parseInt(icProcSolic), acuse.toString());

				if ( lovDatos.size() > 0){
					iNoSolicCarga = ((Integer) lovDatos.elementAt(0)).intValue();
					bdMtoDoctos = (BigDecimal) lovDatos.elementAt(1);
					bdMtoDsctos = (BigDecimal) lovDatos.elementAt(2);
				} //if
			}else{
				throw new NafinException("GRAL0021");
			}
		}else{
			throw new NafinException("GRAL0021");
		}
		List newReg = new ArrayList();
		lovDatos = autSolEJB.getDoctosCargados(acuse.toString(),"","","","","","");
		for(int i = 0; i < lovDatos.size(); i++) {
			HashMap hm = new HashMap();
			lovRegistro = (Vector) lovDatos.elementAt(i);
			hm.put("FOLIO_OPERA",	lovRegistro.elementAt(9));
			hm.put("NOMBRE_CLI",		lovRegistro.elementAt(0));
			hm.put("NO_SIRAC",		lovRegistro.elementAt(1));
			hm.put("NO_DOCTO",		lovRegistro.elementAt(3));
			hm.put("FECHA_EMISION",	lovRegistro.elementAt(8));
			hm.put("FECHA_VENC",		lovRegistro.elementAt(7));
			hm.put("MONEDA",			lovRegistro.elementAt(2));
			hm.put("TIPO_CREDITO",	lovRegistro.elementAt(6));
			hm.put("TASA_IF",			lovRegistro.elementAt(15));
			hm.put("NO_AMORTIZA",	lovRegistro.elementAt(14));
			hm.put("TABLA_AMORTIZA",lovRegistro.elementAt(16));
			hm.put("MONTO_DOCTO",	lovRegistro.elementAt(4));
			hm.put("MONTO_DESC",		lovRegistro.elementAt(5));
			newReg.add(hm);
		}
		if (newReg != null){
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(newReg);
			jsonObj.put("registros", jsObjArray);
		}
		jsonObj.put("aux_acuse", acuse.toString());
		jsonObj.put("acuse", acuse.formatear());
		jsonObj.put("fechaCarga", sdf.format(new java.util.Date()));
		jsonObj.put("horaCarga", sdf2.format(new java.util.Date()));
		jsonObj.put("usuario", strNombreUsuario);
		jsonObj.put("iNoSolicCarga", new Integer(iNoSolicCarga));
		jsonObj.put("bdMtoDoctos", Comunes.formatoMN(bdMtoDoctos.toPlainString()));
		jsonObj.put("bdMtoDsctos", Comunes.formatoMN(bdMtoDsctos.toPlainString()));
		jsonObj.put("mensajeFirma", mensajeFirma);
	}
	if(operacion.equals("Cancelar") || operacion.equals("Generar")){
		autSolEJB.borrarDoctosCargadosTmp( Integer.parseInt(icProcSolic));
	}
	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>