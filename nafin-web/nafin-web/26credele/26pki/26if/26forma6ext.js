Ext.onReady(function() {
  
  var existeGlobal = '0';
  var existeGlobal2= '0';
  var pkcs7 = '';
  var textoFirmado = '';
  var hid_fecha_oper,hid_monto,hid_tipo_plazo,hid_moneda,existe;  
  var bandera = '';
  
  var procesarConfirmarCertificado = function(result, request){
		var res = new Object();
		res = Ext.decode(result.responseText);
		//btnVerConstancia
    //btnAceptarConstancia
    if(res.error=='No'){
      Ext.Msg.alert('Confirmar',res.confirmar);		
      Ext.getCmp('btnVerConstancia').show();
      Ext.getCmp('btnAceptarConstancia').hide();
    }else{
      Ext.Msg.show({
        title: 'La autentificaci�n no se llev� a cabo',
        msg: res.confirmar,
        buttons: Ext.Msg.OK,
        icon: Ext.Msg.ERROR
      });	
      Ext.getCmp('btnVerConstancia').hide();
      Ext.getCmp('btnAceptarConstancia').show();
    }
    
	}
 
  var procesarConsultaDataGrid =  function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
  var procesaGridArchivo =  function(opts, success, response) {
		if (parseInt(existeGlobal)>0) {
      var forma = Ext.getDom('formAux');
      forma.action = opts.getAt(0).get('URL_ARCHIVO');//Ext.util.JSON.decode(response.responseText).urlArchivo;
      forma.submit();  
		}
   }
  
  var procesaVerPdfConfirmacion =  function(opts, success, response){
      Ext.getCmp('btnVerConstancia').setIconClass('icoPdf');
      var forma = Ext.getDom('formAux');
      forma.action =  Ext.util.JSON.decode(opts.responseText).urlArchivo;
      forma.submit();  
   }
  
  
  var procesaConsultaTextoSuperiorVer = function(opts, success, response) {
    textoFirmado = opts.getAt(0).get('TEXTO_FIRMADO');
    Ext.getCmp('textoSuperiorVer').setText('<div style="background:#fff;margin:20px;margin-bottom:0px;padding:10px;border:#99bce8 1px solid;height:260px">'+opts.getAt(0).get('TEXTO')+'</div>',false);
    Ext.getCmp('winFormatoVer').setTitle('<div style="text-align:center;text-transform:uppercase;font-size:14px">'+opts.getAt(0).get('TITULO')+'</div>');
    Ext.getCmp('piePaginaVer').setText('<div style="font-size:12px;font-weight:bold;color:#31556F;text-align:center">'+opts.getAt(0).get('PIE_PAGINA')+'</div>',false);
	}
  
  var consultaTextoSuperiorVer = new Ext.data.JsonStore({
    root: 'registros',
    url: '26forma6ext.data.jsp',
    baseParams:{
      informacion: 'textoSuperiorVer'
    },
    fields: [
      {name: 'TITULO'},
      {name: 'TEXTO_FIRMADO'},
      {name: 'TEXTO'},
      {name: 'PIE_PAGINA'},
      {name: 'URL_ARCHIVO'}
    ],
    totalProperty: 'total',
    messageProperty: 'msg',
    autoLoad: false,
    listeners:{
      load: procesaConsultaTextoSuperiorVer
    }
  });
  
  var consultaDataGridCertVer = new Ext.data.JsonStore({
    root: 'registros',
    url: '26forma6ext.data.jsp',
    baseParams:{
      informacion: 'ConsultaDataGridCertVer'
    },
    fields: [
      {name: 'RS_EMISOR'},
      {name: 'STRNOMBRE'},
      {name: 'PAGARE'},
      {name: 'RS_LUGAR_EMISION'},
      {name: 'RS_FECHA_EMISION'},
      {name: 'RS_DOMICILIO_PAGO'},
      {name: 'RS_TASA_IF'},
      {name: 'RS_NUMERO_DOCTO'},
      {name: 'RS_FECHA_VENC'},
      {name: 'RS_IMPORTE_TOTAL'},
      {name: 'RS_NUMERO_PRESTAMO'},
      {name: 'RS_NUMERO_AMORT'},
      {name: 'RS_DOMICILIO_PAGO'},
      {name: 'URL_ARCHIVO'}
    ],
    totalProperty: 'total',
    messageProperty: 'msg',
    autoLoad: false,
    listeners:{
      load: procesaGridArchivo
    }
  });
  var consultaDataGridTotalVer = new Ext.data.JsonStore({
    root: 'registros',
    url: '26forma6ext.data.jsp',
    baseParams:{
      informacion: 'ConsultaDataGridCertVer',
      operacion: 'TotalesVer'
    },
    fields: [
      {name: 'TOTAL'},
      {name: 'TOTAL_VALOR_NOM'}
    ],
    totalProperty: 'total',
    messageProperty: 'msg',
    autoLoad: false
  });
  
  var consultaDataGrid = new Ext.data.JsonStore({
    root: 'registros',
    url: '26forma6ext.data.jsp',
    baseParams:{
      informacion: 'Consulta'
    },
    fields: [
      {name: 'FENVIO_OPER'},
      {name: 'NUM_OPER'},
      {name: 'MONTO'},
      {name: 'MONEDA'},
      {name: 'TIPO'},
      {name: 'CONSTANCIA'},
      {name: 'CVE_MONEDA'},
      {name: 'TIPO_PLAZO'},
      {name: 'EXISTE'}
    ],
    totalProperty: 'total',
    messageProperty: 'msg',
    autoLoad: false,
    listeners: {
      load: procesarConsultaDataGrid
    }
  });
  
  var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 5, align: 'center'},
				{header: 'Constancia', colspan: 1, align: 'center'}
			]
		]
	});
  
  var grid = new Ext.grid.EditorGridPanel({
    id:'grid', store: consultaDataGrid, title: '&nbsp;', plugins:grupos,
    style: 'margin:0 auto;', width: 710, height: 400, hidden:true, loadMask: true,
		columns: [{
      header:'Fecha de envio de operaciones', dataIndex:'FENVIO_OPER', sorteable: false,
      width:145, align:'center', resizable:true, hidden:false, menuDisabled: true    
    },{
      header:'', dataIndex:'TIPO_PLAZO', sorteable: false,
      width:145, align:'center', resizable:true, hidden:true  , menuDisabled: true    
    },{
      header:'', dataIndex:'CVE_MONEDA', sorteable: false,
      width:145, align:'center', resizable:true, hidden:true , menuDisabled: true     
    },{
      header:'Numero de operaciones', dataIndex:'NUM_OPER', sorteable: false,
      width:110, align:'center', resizable:true, hidden:false  , menuDisabled: true    
    },{
      header:'Monto', dataIndex:'MONTO', sorteable: false,
      width:100, align:'right', resizable:true, hidden:false, renderer: Ext.util.Format.usMoney  , menuDisabled: true  
    },{
      header:'Moneda', dataIndex:'MONEDA', sorteable: false,
      width:130, align:'center', resizable:true, hidden:false  , menuDisabled: true    
    },{
      header:'Tipo', dataIndex:'TIPO', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false, menuDisabled: true      
    },{
      xtype:'actioncolumn', header:'Ver&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Transmitir', dataIndex:'EXISTE', sorteable: false,
      width:120, align:'center', resizable:true, hidden:false, menuDisabled: true,
      renderer: function(value){
       existeGlobal2 = value;
      },
      items: [{ 
       getClass: function(value){
        return parseInt(existeGlobal2)==0?'icoBuscar':'';
       },
        handler: function(grid, rowIdx, colIds){
          var reg = grid.getStore().getAt(rowIdx);          
          var cmpForma = Ext.getCmp('forma');      
          estatus_solicitud = reg.get('IC_ESTATUS');
          ic_solicitud = reg.get('IC_SOLIC_ESP');
          
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaTextoSuperiorVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'textoSuperiorVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'V',
              existe: 0,
              operacion: 'Documento'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridCertVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'V',
              existe: 0,
              operacion: 'Documento'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridTotalVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              operacion:'TotalesVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'V',
              existe: 0
              //operacion: 'Documento'
            })
          });
          
          existeGlobal=0;
            Ext.getCmp('btnVerConstancia').hide();
            winFormatoVer.show();
            Ext.getCmp('btnAceptarConstancia').hide();
          
        }				
			},{
       getClass: function(value){
        return '';
       }		
			},{ 
        hidden:true,
        id:'MostrarPopUp',
        getClass: function(value){
          return parseInt(existeGlobal2)==0?'validarCuenta':'';
        },
        handler: function(grid, rowIdx, colIds){
          var reg = grid.getStore().getAt(rowIdx); 
          var cmpForma = Ext.getCmp('forma'); 
          existeGlobal = reg.get('EXISTE');
          hid_fecha_oper=reg.get('FENVIO_OPER');
          hid_monto=reg.get('MONTO');
          hid_tipo_plazo=reg.get('TIPO_PLAZO');
          hid_moneda=reg.get('CVE_MONEDA');
          hid_nom_moneda=reg.get('MONEDA');
          existe= 0;
          
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaTextoSuperiorVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'textoSuperiorVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: 0,
              operacion: 'ArchivoPDF'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridCertVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: 0,
              operacion: 'ArchivoPDF'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridTotalVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              operacion:'TotalesVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: 0
              //operacion: 'ArchivoPDF'
            })
          });
            existeGlobal=0;
            Ext.getCmp('btnVerConstancia').hide();
            winFormatoVer.show();
            Ext.getCmp('btnAceptarConstancia').show();

        }				
			},{
        hidden:true,
        id:'ImprimePDF',
        getClass: function(value){
          return parseInt(existeGlobal2)==0?'':'icoPdf';
       },
        handler: function(grid, rowIdx, colIds){
          var reg = grid.getStore().getAt(rowIdx); 
          var cmpForma = Ext.getCmp('forma'); 
          existeGlobal = reg.get('EXISTE');
          hid_fecha_oper=reg.get('FENVIO_OPER');
          hid_monto=reg.get('MONTO');
          hid_tipo_plazo=reg.get('TIPO_PLAZO');
          hid_moneda=reg.get('CVE_MONEDA');
          hid_nom_moneda=reg.get('MONEDA');
          existe= reg.get('EXISTE');
          
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaTextoSuperiorVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'textoSuperiorVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: reg.get('EXISTE'),
              operacion: 'ArchivoPDF'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridCertVer.load({
						params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: reg.get('EXISTE'),
              operacion: 'ArchivoPDF'
            })
          });
          paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
          consultaDataGridTotalVer.load({
				params: Ext.apply(paramSubmit,{
              informacion:'ConsultaDataGridCertVer',
              operacion:'TotalesVer',
              hid_fecha_oper:reg.get('FENVIO_OPER'),
              hid_monto: reg.get('MONTO'),
              hid_tipo_plazo:reg.get('TIPO_PLAZO'),
              hid_moneda:reg.get('CVE_MONEDA'),
              hid_nom_moneda: reg.get('MONEDA'),
              hid_acuse: '',
              hidAction: 'T',
              existe: reg.get('EXISTE'),
              operacion: 'ArchivoPDF'
            })
          });
          
          if(parseInt(existeGlobal)==0){
            Ext.getCmp('btnVerConstancia').hide();
            winFormatoVer.show();
            Ext.getCmp('btnAceptarConstancia').show();
          }
        }				
			}]
    }]
  });
  
  var gridCertVer = new Ext.grid.EditorGridPanel({
    id:'id_gridCertVer', store: consultaDataGridCertVer, title: '&nbsp;',
    style: 'margin:0 auto;', width: 830, height: 200,
		columns: [{
      header:'Nombre del Emisor', dataIndex:'RS_EMISOR', sorteable: false,
      width:145, align:'center', resizable:true, hidden:false      
    },{
      header:'Beneficiario', dataIndex:'STRNOMBRE', sorteable: false,
      width:110, align:'center', resizable:true, hidden:false      
    },{
      header:'Clase de documento', dataIndex:'PAGARE', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Lugar de Emisi�n', dataIndex:'RS_LUGAR_EMISION', sorteable: false,
      width:130, align:'center', resizable:true, hidden:false      
    },{
      header:'Fecha de Emisi�n', dataIndex:'RS_FECHA_EMISION', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Domicilio de Pago', dataIndex:'RS_DOMICILIO_PAGO', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Tasa de Inter�s', dataIndex:'RS_TASA_IF', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'N�mero de Documento', dataIndex:'RS_NUMERO_DOCTO', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Fecha de Vencimiento', dataIndex:'RS_FECHA_VENC', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Valor Nominal', dataIndex:'RS_IMPORTE_TOTAL', sorteable: false,
      width:100, align:'right', resizable:true, hidden:false   
    },{
      header:'N�mero de Pr�stamo', dataIndex:'RS_NUMERO_PRESTAMO', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Amortizaci�n', dataIndex:'RS_NUMERO_AMORT', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    },{
      header:'Domicilio Pago', dataIndex:'RS_DOMICILIO_PAGO', sorteable: false,
      width:100, align:'center', resizable:true, hidden:false      
    }]
  });
  
  var gridTotalesVer = new Ext.grid.EditorGridPanel({
    id:'gridTotalesVer', store: consultaDataGridTotalVer, title: '<div style="text-align:center">Totales</div>',
    style: 'margin:0 auto;', width: 425, height: 76,
		columns: [{
      header:'N�mero de Documento', dataIndex:'TOTAL', sorteable: false,
      width:210, align:'center', resizable:true, hidden:false,
      renderer: function(value){ return '<b>'+value+'</b>'; }
    },{
      header:'Valor Nominal', dataIndex:'TOTAL_VALOR_NOM', sorteable: false,
      width:210, align:'center', resizable:true, hidden:false,
      renderer: function(value){ return '<b>'+value+'</b>'; }      
    }]
  });
  
  var elementosForma = [{
    xtype: 'compositefield',
    fieldLabel: 'Fecha de env�o de operaciones',
    combineErrors: false,
    msgTarget: 'side',
    width: 500,
    items:[{
      xtype: 'datefield',
      name: 'txt_fecha_oper_de',
      id: 'txt_fecha_oper_de',
      startDay:0,
      width:100,
      msgTarget:'side',
      vtype:'rangofecha',
      //campoFinFecha:'',
      margin: '0 20 0 0',
      formBind: true,
      value: new Date(),
      allowBlank  :  false
    },{
      xtype: 'displayfield',
      value: 'a:',
      width: 20
		},{
      xtype: 'datefield',
      name:'txt_fecha_oper_a',
      id:'txt_fecha_oper_a',
      startDay:1,
      width:100,
      msgTarget:'side',
      vtype:'rangofecha',
      //campoFinFecha:'',
      margin: '0 20 0 0',
      formBind: true,
      value: new Date(),
      allowBlank  :  false
    }]
  }];
  
  var textoSuperiorVer = {
    xtype: 'label',
    id:'textoSuperiorVer',
    html: ''
  }
  var piePaginaVer = {
    xtype: 'label',
    id:'piePaginaVer',
    html: ''
  }
  /****************************************************************************/
  
   var miPanel = new Ext.Panel({  
      width   :  890,   
      height   : 600,  
      autoScroll:   true,
      items:   [
         textoSuperiorVer,
         gridCertVer,
         gridTotalesVer,
         piePaginaVer
      ]
   })
  
  
  var confirmarAcuse= function(vpkcs7,  vtextoFirmado, vhid_fecha_oper, vhid_monto ,vhid_tipo_plazo, vhid_moneda, vhid_nom_moneda, vexiste ){
  
	if (Ext.isEmpty(vpkcs7)) {  
    
		Ext.Msg.show({
			title:'Firmar',
            msg:'Error en el proceso de Firmado. \n No se puede continuar',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
          });
          return false;	//Error en la firma. Termina...
        }else  {
						
			Ext.Ajax.request({
				url: '26forma6ext.data.jsp',
			  params: {
				informacion:'Confirmar',
				hidAction: 'F',
				hid_fecha_oper:vhid_fecha_oper,
				hid_monto: vhid_monto,
				hid_tipo_plazo: vhid_tipo_plazo,
				hid_moneda: vhid_moneda,
				hid_nom_moneda: vhid_nom_moneda,
				hid_acuse: '',
				existe: vexiste,
				Pkcs7:vpkcs7,
				TextoFirmado: vtextoFirmado
			  },
			success: procesarConfirmarCertificado,
			failure: function(){
				Ext.Msg.show({
					title: 'Firmar',
					msg: 'Problemas para confirmar...',
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.ERROR
				});
			}
        });
	}
    
  }
  
  
  
  var winFormatoVer = new Ext.Window({
    title   :  '',
    id      :  'winFormatoVer',
    closable:  false,
    width   :  900,
    height  :  650,
    modal   :  true,
    autoScroll:   false,
    html    : '<div id="cancelado">&nbsp;</div>',
    items:[miPanel], 
    bbar:['-','->',{
      xtype: 'button',
      text: 'Cerrar',
      iconCls: 'cancelar',
      handler: function(){
        winFormatoVer.hide();
        
        if(bandera == 'recargarGrid'){
           var cmpForma = Ext.getCmp('forma');
            paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
            consultaDataGrid.load({
               params: Ext.apply(paramSubmit,{
                  operacion: 'Generar'							
               })
            })
         }
      }
    },{
      xtype: 'button',
      id: 'btnAceptarConstancia',
      text: 'Aceptar Constancia',
      hidden:true,
      iconCls: 'validarCuenta',
      handler: function(){
         bandera = 'recargarGrid';
		
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmado, hid_fecha_oper, hid_monto , hid_tipo_plazo, hid_moneda, hid_nom_moneda, existe     );
				
      }
    },{
      xtype: 'button',
      id: 'btnVerConstancia',
      text: 'Ver',
      hidden:true,
      iconCls: 'icoPdf',
      handler: function(boton){
         boton.setIconClass('loading-indicator');
        Ext.Ajax.request({
          url: '26forma6ext.data.jsp',
          params: {
              //informacion:'textoSuperiorVer',
              informacion: 'VerPdfConfirmacion',
              hid_fecha_oper:hid_fecha_oper,
              hid_monto: hid_monto,
              hid_tipo_plazo: hid_tipo_plazo,
              hid_moneda: hid_moneda,
              hid_nom_moneda: hid_nom_moneda,
              hid_acuse: '',
              hidAction: 'T',
              existe: existe
          },
          success: procesaVerPdfConfirmacion
        });
      }
    }]    
  });
	var fp = {
		xtype: 'form',
		id: 'forma',
		style: 'margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 450,
      labelWidth: 170,
		titleCollapse: false,
      title: 'Constancia de Deposito',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
      monitorValid   :  true,
		items:[elementosForma],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
            formBind:true,
				handler: function(boton, evento) {
               Ext.getCmp('grid').show();
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar'							
					})
				});
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '26forma6ext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid
		]
	});
});
