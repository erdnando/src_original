<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	netropology.utilerias.*,
	com.netro.anticipos.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/26credele/26secsession_extjs.jspf" %>
<% 

JSONObject jsonObj = new JSONObject();

String ic_proc_solic	= (request.getParameter("ic_proc_solic")==null)?"":request.getParameter("ic_proc_solic");
String tipoPlazo	= (request.getParameter("tipoPlazo")==null)?"":request.getParameter("tipoPlazo");

Vector lovDatos 	= null;
Vector lovRegistro  = null;

try {

	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	StringBuffer cad = new StringBuffer();
	String mensajeBloqueo = (strTipoUsuario.equals("IF") && autSolEJB.esIfBloqueado(iNoCliente,"0"))?
			"Las operaciones enviadas estarán detenidas por el Usuario Administrador":
			"";
	String meses_a[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	java.util.Date fechaHoy = new java.util.Date();
	String	diaHoy	= new java.text.SimpleDateFormat("dd").format(fechaHoy);
	int		mesAct	= Integer.parseInt(new java.text.SimpleDateFormat("MM").format(fechaHoy));
	String	anioAct	= new java.text.SimpleDateFormat("yyyy").format(fechaHoy);
			mesAct--;
	String			fechaConvenio	= "";
	String			lugarFirma		= "";

	int		numDoctosMN		= 0;
	int		numDoctosUSD	= 0;
	double	totMontoMN		= 0;
	double	totMontoUSD		= 0;

	String 	nombre_personal	= "";
	String 	paterno_personal	= "";
	String 	materno_personal	= "";
	String 	puesto_personal	= "";

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual_a	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual		= fechaActual_a.substring(0,2);
	String mesActual		= meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
	String anioActual		= fechaActual_a.substring(6,10);
	String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

	List lTotales = autSolEJB.getTotalesCargaM(ic_proc_solic);

	cad.append("Declaro bajo protesta de decir verdad que soy tenedor y propietario de los documentos ");
	cad.append("que aquí se describen en forma electrónica. Dicha declaración tendrá plena validez para todos los efectos legales conducentes.\n\n");
	cad.append("Con fundamento en lo dispuesto por los artículos 1205 y 1298-A del Código de Comercio, la información e instrucciones que el Intermediario y Nafin ");
	cad.append("se transmitan o comuniquen mutuamente mediante el sistema, tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada,");
	cad.append("el importe de la misma, su naturaleza, así; como las características y alcance de sus instrucciones.");

	pdfDoc.addText(mensajeBloqueo,"formas",ComunesPDF.CENTER);

	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Resumen de Solicitudes Cargadas","celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("No. total de operaciones registradas:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(lTotales.get(0).toString(),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los documentos:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMN(lTotales.get(1).toString()),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Monto total del importe de los descuentos:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(Comunes.formatoMN(lTotales.get(2).toString()),"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(cad.toString(),"formas",ComunesPDF.CENTER,2);
	pdfDoc.addTable();

	String montoTotDesc = lTotales.get(2).toString();
	
	pdfDoc.setTable(12, 100);
	pdfDoc.setCell("Nombre del Cliente","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Sirac","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Emisión Título de Crédito","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Vencimiento Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de Crédito","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tasa Intermediario Financiero","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Amortizaciones","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tabla Amortización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto del Descuento","celda01",ComunesPDF.CENTER);

	String cliente="", sirac="", noDocto="", fec_emision="", fec_venc, moneda="",tipoCred, tasaIf="", noAmort="", tabAmort="", montoDocto="",montoDesc="";

	if ( ic_proc_solic.equals("")) ic_proc_solic = "0";
		lovDatos = autSolEJB.getDoctosCargadosTmp(Integer.parseInt(ic_proc_solic));
	for(int i = 0; i < lovDatos.size(); i++){
		lovRegistro = (Vector) lovDatos.elementAt(i);
		cliente=(lovRegistro.elementAt(0)).toString();
		sirac=(lovRegistro.elementAt(1)).toString();
		noDocto=(lovRegistro.elementAt(3)).toString();
		fec_emision=(lovRegistro.elementAt(8)).toString();
		fec_venc=(lovRegistro.elementAt(7)).toString();
		moneda=(lovRegistro.elementAt(2)).toString();
		tipoCred=(lovRegistro.elementAt(6)).toString();
		tasaIf=(lovRegistro.elementAt(10)).toString();
		noAmort=(lovRegistro.elementAt(11)).toString();
		tabAmort=(lovRegistro.elementAt(9)).toString();
		montoDocto=(lovRegistro.elementAt(4)).toString();
		montoDesc=(lovRegistro.elementAt(5)).toString();
		pdfDoc.setCell(cliente,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(noDocto,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_emision,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fec_venc,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tipoCred,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tasaIf,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(noAmort,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(tabAmort,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDocto,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+montoDesc,"formas",ComunesPDF.CENTER);

		if(i==0){
			fechaConvenio	= (String)lovRegistro.get(18);
			lugarFirma		= (String)lovRegistro.get(14);
		}
		if("1".equals(lovRegistro.elementAt(19).toString())){
			numDoctosMN++;
			totMontoMN += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}else if("54".equals(lovRegistro.elementAt(19).toString())){
			numDoctosUSD++;
			totMontoUSD += ((Double)lovRegistro.elementAt(20)).doubleValue();
		}

		puesto_personal = (String)lovRegistro.get(21);
		nombre_personal = (String)lovRegistro.get(22);
		paterno_personal = (String)lovRegistro.get(23);
		materno_personal = (String)lovRegistro.get(24);
	}
	if(lovDatos.size() < 1){
		pdfDoc.setCell("No se encontró ningún registro","celda01",ComunesPDF.CENTER,12);
	}
	pdfDoc.addTable();
	
	pdfDoc.addText("","formas",ComunesPDF.CENTER);
	pdfDoc.addText("","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Formato de constancia de depósito de operaciones\nCrédito Electrónico","formas",ComunesPDF.CENTER);
	pdfDoc.setTable(1,100);
	//pdfDoc.setCell("Formato de constancia de depósito de operaciones<br>Crédito Electrónico", "celda01");
	pdfDoc.setCell(
						"De conformidad con el convenio que este Banco ("+strNombre+"), celebró con Nacional Financiera, S.N.C., y en virtud del descuento electrónico de (los) documento(s) electrónico(s),"+
						"en este acto cedemos a Nacional Financiera, S.N.C., los derechos de crédito que se derivan de los mismos.\n\n"+
						"Asimismo, bajo protesta de decir verdad, hacemos constar en este acto que para todos los efectos legales a que haya lugar nos asumimos como depositarios sin derecho a honorarios y asumiendo la responsabilidad "+
						"civil y penal correspondiente al carácter de depositario judicial, del (los) documento(s) electrónico(s) en que se consignan los derechos de crédito derivados de las operaciones celebradas con los "+
						"clientes que se describen en el cuadro anexo en donde se especifica la fecha y lugar de presentación, importe total y nombre de la empresa acreditada.\n\n"+
						"La información del (los) "+numDoctosMN+" documento(s) electrónica(s) en Moneda Nacional por un importe de $"+Comunes.formatoDecimal(totMontoMN,2)+" y "+numDoctosUSD+" documento(s) electrónico(s) en Dólares por un importe de $"+Comunes.formatoDecimal(totMontoUSD,2)+" citado(s), "+
						"está registrada en el sistema denominado Nafin Electrónico a disposición de Nacional Financiera, S.N.C., cuando ésta la requiera.\n\n"+
						"En términos del artículo 2044 del Código Civil para el Distrito Federal, la cesión a que se refiere el primer párrafo la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, "+
						"por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidación total.\n\n"+
						"Asimismo y en términos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del (los) documento(s) electrónico(s) que se listan a continuación y que amparan los derechos "+
						"de crédito a ejercer los derechos de cobro consignados en el (los) documento(s) electrónico(s) señalados y vigilar que conserven su valor y demás derechos que les correspondan."
	, "formas");
	pdfDoc.addTable();
	
	pdfDoc.setTable(2, 50);
	pdfDoc.setCell("Nombre del Intermediario Financiero","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(strNombre,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha del Convenio de Crédito Electrónico:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(fechaConvenio,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Fecha de Operación:","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(lugarFirma+" a "+diaHoy+" de "+meses_a[mesAct]+" de "+anioAct,"formas",ComunesPDF.RIGHT);
	pdfDoc.setCell("Nota: La Fecha de Operación es Informativa.","formas",ComunesPDF.CENTER,2);
	pdfDoc.addTable();

	String emisor="", claseDocto="", lugar="", fechaEmision="", domicilio="",tasa="",numDocto="", fechaVto="", impOpera="", destCredito = "";
	if("C".equals(tipoPlazo))
		pdfDoc.setTable(10, 100);
	else
		pdfDoc.setTable(9, 100);
	pdfDoc.setCell("Nombre del Emisor","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Clase de Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Lugar de Emisión","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Emisión","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Domicilio de pago","celda01",ComunesPDF.CENTER);

	if("C".equals(tipoPlazo))
		pdfDoc.setCell("Destino del Crédito","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tasa de Interes","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Importe de la Operación","celda01",ComunesPDF.CENTER);
	
	for(int i = 0; i < lovDatos.size(); i++) {
		lovRegistro = (Vector) lovDatos.elementAt(i);
		emisor=(lovRegistro.elementAt(12)).toString();
		claseDocto=(lovRegistro.elementAt(13)).toString();
		lugar=(lovRegistro.elementAt(14)).toString();
		fechaEmision=(lovRegistro.elementAt(8)).toString();
		domicilio=(lovRegistro.elementAt(15)).toString();
		tasa=(lovRegistro.elementAt(10)).toString();
		numDocto=(lovRegistro.elementAt(3)).toString();
		fechaVto=(lovRegistro.elementAt(7)).toString();
		impOpera=(lovRegistro.elementAt(5)).toString();
		destCredito=(lovRegistro.elementAt(25)).toString();
		
		pdfDoc.setCell(emisor,			"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(claseDocto,		"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lugar,			"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fechaEmision,	"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(domicilio,		"formas",ComunesPDF.CENTER);
		
		if("C".equals(tipoPlazo))
			pdfDoc.setCell(destCredito,		"formas",ComunesPDF.CENTER);
		
		pdfDoc.setCell(tasa,				"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(noDocto,			"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fechaVto,		"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(impOpera,		"formas",ComunesPDF.CENTER);
	}
	pdfDoc.setCell("Total",		"celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(Comunes.formatoMN(montoTotDesc),		"formas",ComunesPDF.LEFT,8);
	pdfDoc.addTable();

	pdfDoc.addText("Facultado por "+strNombre,"formas",ComunesPDF.CENTER);
	pdfDoc.addText(puesto_personal+" - "+nombre_personal+" "+paterno_personal+" "+materno_personal,"formas",ComunesPDF.CENTER);
	
	pdfDoc.endDocument();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>