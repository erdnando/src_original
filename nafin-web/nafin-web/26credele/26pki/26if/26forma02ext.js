Ext.onReady(function() {

   //--------------------------------- OVERRIDES ------------------------------------
   /*
      Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del campo de fecha.
      Version original: http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
   */
   Ext.override(Ext.Element, {
      getWidth : function(contentWidth){
         var me = this,
         dom    = me.dom,
         hidden = Ext.isIE && me.isStyle('display', 'none'),
         //w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
         w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
         w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
         return w < 0 ? 0 : w;
      }
   });   
	
	//------------------------------ EXTRA - Function �S ----------------------------------------
	var strEsquemaExtJS = Ext.getDom('hidStrEsquemaExtJS').value;
	var valid = true;
	function verificaPanel(id){
		var myPanel = Ext.getCmp(id);
		//var valid = true;
		if (!valid){
			return;
		}
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != undefined){
				if (panelItem.xtype == 'panel')	{	//El metodo isValid() no aplica para los tipos de objetos label.
					if(!verificaPanel(panelItem.id)) {
						return false;
					}
				}else{
					if (!panelItem.isValid())	{
						valid = false;
						return false;
					}
				}
			}
		});
		return valid;
	}

	var plazosBO = {valida:"", FechaActual:"",FecVenDMax:"",puedeUsarCodigoBO:null}
	var noAmortiza = {fechaLimite:null, capitalInt:false}
	var valInicio = {tipoCartera:null, plazos:null}
	var continua = {textoFirmar:null,icProcSolic:null}

	var procesarGenerarPdfAsistencia =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfAsistencia');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAsistencia');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaAsistencia(opts, success, response) {
		pnl.el.unmask();
		gridAsistenciaData.loadData('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);

			var elementosAsistencia = [
				{
					xtype:'panel',	style:'margin:0 auto;',	frame:false,	border:false,	html:'<div align="center"><b>Para hacer m�s �gil el registro de operaciones, se muestran las caracter�sticas con las que puede solicitar su cr�dito.</b></div>'
				},{
					xtype:'displayfield',	value:''
				},{
					xtype:'grid',	id:'gridAsistencia',	store:gridAsistenciaData,	stripeRows:true,	loadMask:true,	height:580,	width:850,	anchor:'100%',	hidden:false, frame:false,
					columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	title:'Datos con los que debe realizar el registro de su operaci�n',
					columns: [
						{header:'Tipo Plazo',	tooltip:'Tipo Plazo', dataIndex:'TIPO_PLAZO',	width:100,	align:'center', resizable:true, hideable:false, sortable:true},
						{header:'Tipo Cr�dito',tooltip:'Tipo Cr�dito',	dataIndex:'TIPO_CREDITO',width:150,	align:'center', resizable:true,	sortable:true},
						{header:'Nombre del Emisor',	tooltip:'Nombre del Emisor',	dataIndex:'EMISOR',	width:200,	align:'center', resizable:true},
						{header:'Tasa Base Intermediario',	tooltip:'Tasa Base Intermediario',	dataIndex:'TASA',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo Amortizaci�n',	tooltip:'Tipo Amortizaci�n',	dataIndex:'TIPO_AMORT',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Plazo m�nimo',	tooltip:'Plazo m�nimo',	dataIndex:'PMIN',	width:110,	align:'center', sortable:true,	resizable:true},
						{header:'Plazo m�ximo',	tooltip:'Plazo m�ximo',	dataIndex:'PMAX',	width:110,	align:'center', sortable:true,	resizable:true},
						{header:'Periodicidad de Pago al Capital',	tooltip:'Periodicidad de Pago al Capital',	dataIndex:'PERIODICIDAD',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo de Inter�s',	tooltip:'Tipo de Inter�s',	dataIndex:'TIPO_INTERES',	width:150,	align:'center', sortable:true,	resizable:true},
						{header:'Tipo Renta',	tooltip:'Tipo Renta',	dataIndex:'TIPO_RENTA',	width:150,	align:'center', sortable:true,	resizable:true}
					]
				},{
					xtype:'displayfield',	value:''
				},{
					xtype:'panel', layout:'table',	layoutConfig: {columns:3}, border:false,
					items: [{
						border:false,	html:'<div align="center"><b>Si la informaci�n que requiere para la captura de su cr�dito no se encuentra en este recuadro, contacte a su ejecutivo.</b></div>',
						colspan: 3
					},{
						border:false,	html: '<div align="right"><b>Para mayor informaci�n consulte el Manual de Operaci�n</b></div>',
						colspan: 2, width:500
					},{
						xtype:'button', id:'btnManualOpera', 	iconCls:'icoPdf', height:'auto', width:30,
							handler: function() {
											var forma = Ext.getDom('formAux');
											forma.action = "26guiaoperacion.pdf";
											forma.submit();
							}
					 }]
				}
			];
		
			var fpAsistencia = new Ext.form.FormPanel({
				id: 'formaAsistencia',
				width: 850,
				heigth:'auto',
				style: 'margin:0 auto;',
				frame: false,
				border:false,
				bodyStyle: 'padding: 6px',
				labelWidth: 150,
				items: elementosAsistencia
			});
			new Ext.Window({
				modal: true,
				resizable: true,
				x: 100, y:75,
				width: 870,
				height: 750,
				layout:'fit',
				autoScroll:true,
				id: 'winAsistencia',
				closeAction: 'destroy',
				items: [fpAsistencia],
				title: 'Asistencia en la Operaci�n',
				bbar: {xtype: 'toolbar', buttons: ['->','-',{xtype:'button',	text:'Generar PDF ',	id:'btnPdfAsistencia',	disabled:true, iconCls:'icoPdf',
						handler: function(boton, event){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '26forma_ayuda_pdf_ext.jsp',
											params: {TipoCartera:valInicio.tipoCartera},
											callback: procesarGenerarPdfAsistencia
										});
						}
						},{xtype:'button',text:'Bajar PDF',id:'btnBajarPdfAsistencia',hidden:true}]}
			}).show();

			if (infoR.registros != undefined && infoR.registros.length > 0){
				gridAsistenciaData.loadData(infoR.registros);
				Ext.getCmp('btnPdfAsistencia').setDisabled(false);
				Ext.getCmp('gridAsistencia').getGridEl().unmask();
			}else{
				Ext.getCmp('gridAsistencia').getGridEl().mask('No existen registros parametrizados', 'x-mask');
				Ext.getCmp('btnPdfAsistencia').setDisabled(true);
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdf');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarConfirmar(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fpIndividual_b.setVisible(false);
			var infoR = Ext.util.JSON.decode(response.responseText);
			//Ext.Msg.alert("Mensaje de p�gina web.",	infoR.mensajeFirma);
			
			Ext.getCmp('disMsjFirma').body.update('<div class="formas" align="center">'+infoR.mensajeFirma+'</div>');
			Ext.getCmp('disAcuse').body.update('<div class="formas">'+infoR.acuse+'</div>');	//align="right" 
			Ext.getCmp('disTotOpera').body.update('<div class="formas">'+infoR.iNoSolicCarga+'</div>');
			Ext.getCmp('disImpDocto').body.update('<div class="formas">'+infoR.bdMtoDoctos+'</div>');
			Ext.getCmp('disImpDesc').body.update('<div class="formas">'+infoR.bdMtoDsctos+'</div>');
			Ext.getCmp('disFechaCarga').body.update('<div class="formas">'+infoR.fechaCarga+'</div>');
			Ext.getCmp('disHoraCarga').body.update('<div class="formas">'+infoR.horaCarga+'</div>');
			Ext.getCmp('disUsuario').body.update('<div class="formas">'+infoR.usuario+'<div>');
			fpIndividual_c.setVisible(true);

			if (infoR.registros != undefined && infoR.registros.length > 0){
				gridAcuseData.loadData(infoR.registros);
				Ext.getCmp('gridAcuse').getGridEl().unmask();
			}else{
				Ext.getCmp('gridAcuse').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}

			Ext.getCmp('btnGenerarPDF').setHandler( function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url : '26forma02pdf_ext.jsp',
					params:{
								acuse: infoR.acuse,
								iNoSolicCarga:infoR.iNoSolicCarga,
								bdMtoDoctos:infoR.bdMtoDoctos,
								bdMtoDsctos:infoR.bdMtoDsctos,
								fechaCarga:infoR.fechaCarga,
								horaActual:infoR.horaCarga,
								usuario:infoR.usuario
							},
					callback: procesarSuccessFailureGenerarPDF
				});
			});

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaContinuar(opts, success, response) {
		pnl.el.unmask();
		gridResumenData.loadData("");
		gridResumen_bData.loadData("");
		gridAcuseData.loadData("");
		fpIndividual_b.setVisible(true);
		fpIndividual.setVisible(false);
		fpIndividual_a.setVisible(false);
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('montoImpDocto').body.update('<div class="formas" align="right"> $ '+Ext.getCmp('importeDocto').getValue()+'</div>');
			Ext.getCmp('montoImpDesc').body.update('<div class="formas" align="right"> $ '+Ext.getCmp('importeDesc').getValue()+'</div>');
			
			var miHtml = ''+
					'<div align="justify">'+
						'<br>De conformidad con el convenio que este Banco ('+infoR.strNombre+'), celebr&oacute; con Nacional Financiera, S.N.C., y en virtud del descuento electr&oacutenico de (los) documento(s) electr&oacute;nico(s), '+
						'en este acto cedemos a Nacional Financiera, S.N.C., los derechos de cr&eacute;dito que se derivan de los mismos.<br><br>'+
						'Asimismo, bajo protesta de decir verdad, hacemos constar en este acto que para todos los efectos legales a que haya lugar nos asumimos como depositarios sin derecho a honorarios y asumiendo la responsabilidad '+
						'civil y penal correspondiente al car&aacute;cter de depositario judicial, del (los) documento(s) electr&oacute;nico(s) en que se consignan los derechos de cr&eacute;dito derivados de las operaciones celebradas con los '+
						'clientes que se describen en el cuadro anexo en donde se especifica la fecha y lugar de presentaci&oacute;n, importe total y nombre de la empresa acreditada.<br><br>'+
						'La informaci&oacute;n del (los) '+infoR.numDoctosMN+' documento(s) electr&oacute;nico(s) en Moneda Nacional por un importe de $'+Ext.util.Format.number(infoR.totMontoMN,'0.00')+' y '+infoR.numDoctosUSD+' documento(s) electr&oacute;nico(s) en D&oacute;lares por un importe de $'+Ext.util.Format.number(infoR.totMontoUSD,'0.00')+' citado(s), '+
						'est&aacute; registrada en el sistema denominado Nafin Electr&oacute;nico a disposici&oacute;n de Nacional Financiera, S.N.C., cuando &eacute;sta la requiera.<br><br>'+
						'En t&eacute;rminos del art&iacute;culo 2044 del C&oacute;digo Civil para el Distrito Federal, la cesi&oacute;n a que se refiere el primer p&aacute;rrafo la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, '+
						'por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidaci&oacute;n total.<br><br>'+
						'Asimismo y en t&eacute;rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del (los) documento(s) electr&oacute;nico(s) que se listan a continuaci&oacute;n y que amparan los derechos '+
						'de cr&eacute;dito a ejercer los derechos de cobro consignados en el (los) documento(s) electr&oacute;nico(s) se�alados y vigilar que conserven su valor y dem&aacute;s derechos que les correspondan.'+
						'<br><br>'+
						'<table border="0">'+
							'<tr>'+
								'<td class="formas" nowrap>Nombre del Intermediario Financiero:</td>'+
								'<td class="formas" nowrap>'+infoR.strNombre+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Fecha del Convenio de Cr&eacute;dito Electr�nico:</td>'+
								'<td class="formas" nowrap>'+infoR.fechaConvenio+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Fecha de Operaci&oacute;n:</td>'+
								'<td class="formas" nowrap>M&eacute;xico D.F. a '+infoR.diaHoy+' de '+infoR.mesAct+' de '+infoR.anioAct+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td class="formas" nowrap>Nota:�La Fecha de Operaci&oacute;n es Informativa.</td>'+
								'<td class="formas" nowrap>&nbsp;</td>'+
							'</tr>'+
						'</table>'+
					'</div>';

			if (infoR.registros != undefined && infoR.registros.length > 0){
				gridResumenData.loadData(infoR.registros);
				Ext.getCmp('gridResumen').getGridEl().unmask();
			}else{
				Ext.getCmp('gridResumen').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}

			if (infoR.registros_b != undefined && infoR.registros.length > 0){
				var gridColumnMod = Ext.getCmp('gridResumen_b').getColumnModel();
				gridResumen_bData.loadData(infoR.registros_b);
				Ext.getCmp('gridResumen_b').getGridEl().unmask();
				
				if(Ext.getCmp('tipoPlazo').getValue()=="C"){
					gridColumnMod.setHidden(6,false);
				}else{
					gridColumnMod.setHidden(6,true);
				}
				
			}else{
				Ext.getCmp('gridResumen_b').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
			}

			Ext.getCmp('msjTipoPlazoRes_b').body.update(miHtml);
			Ext.getCmp('msjTipoPlazoRes_c').body.update('<div class="titulos" align="center">Facultado por <strong>'+infoR.strNombre+'</strong><br>'+
															infoR.puesto_personal+'-'+infoR.nombre_personal+'&nbsp;'+infoR.paterno_personal+'&nbsp;'+infoR.materno_personal+'</div>');

			var regTot = totalesData.getAt(0);
			regTot.set('TOTAL_MONTO',	Ext.getCmp('importeDesc').getValue() );

			if(!Ext.isEmpty(infoR.msg)){
				Ext.Msg.alert("Mensaje de p�gina web",infoR.msg,
					function(){
						if(opts.params.deFormulario == 'forma2'){
							fpIndividual.setVisible(true);
						}else{
							fpIndividual_a.setVisible(true);
						}
						fpIndividual_b.setVisible(false);
				});
			}
			if (infoR.txtFirmar != undefined){
				continua.textoFirmar = infoR.txtFirmar;
			}
			continua.icProcSolic = infoR.icProcSolic;
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function enviar(plazos){
		if(!Ext.getCmp('sobreTasaUF').isValid()){
			return;
		}

		var action = "26forma2b";
		var TasaUF			= Ext.getCmp('tasaUF').getValue();
		var RelMatUF		= Ext.getCmp('relMatUF').getValue();
		var SobreTasaUF	= Ext.getCmp('sobreTasaUF').getValue();
		var Amortizacion	= Ext.getCmp('Amortizacion').getValue();
		var FechaPPC		= Ext.util.Format.date(Ext.getCmp('fechaPPC').getValue(),'d/m/Y');
		var DiaPago			= Ext.getCmp('diaPago').getValue();
		var FechaPPI		= Ext.util.Format.date(Ext.getCmp('fechaPPI').getValue(),'d/m/Y');
		var FechaVencDscto= Ext.util.Format.date(Ext.getCmp('fechaVencDscto').getValue(),'d/m/Y');
		var PPCapital		= Ext.getCmp('capital').getValue();
		var PPIntereses	= Ext.getCmp('intereses').getValue();

		if(Amortizacion=="5"){
			if(!Ext.getCmp('radioSiAmortiza').checked&&!Ext.getCmp('radioNoAmortiza').checked){
				//Ext.getCmp('radioGpAmortiza').markInvalid("El tipo de renta es obligatorio para amortizaciones de plan de pagos");
				Ext.Msg.alert("Mensaje de p�gina web","El tipo de renta es obligatorio para amortizaciones de plan de pagos");
				return;
			}
			if(Ext.getCmp('radioNoAmortiza').checked){		// tipo de renta N
				if ( Ext.isEmpty(Ext.getCmp('noAmortizaciones').getValue()) ){
						Ext.Msg.alert("Mensaje de p�gina web","El numero de amortizaciones para plan de pagos debe ser mayor a uno");
						return;					
				}else{
					if(Ext.getCmp('noAmortizaciones').getValue()=="1"){
						Ext.Msg.alert("Mensaje de p�gina web","El numero de amortizaciones para plan de pagos debe ser mayor a uno");
						return;
					}
				}
			}
			if(Ext.getCmp('radioSiAmortiza').checked){	// tipo de renta S
				//if (!Ext.isEmpty(FechaPPI)){
				var dia_fppi = 	FechaPPI.substring(0,2);
				var dia_fven = 	FechaVencDscto.substring(0,2);
				if(dia_fppi != dia_fven){
					Ext.Msg.alert("Mensaje de p�gina web","Error en pagos tipo renta, los d�as de pago deben ser iguales para todas las amortizaciones");
					return;
				}
				//}
				if(PPCapital=="14" || PPIntereses=="14"){
					Ext.Msg.alert("Mensaje de p�gina web","No se puede seleccionar periodicidad de pago catorcenal para esa tabla de amortizacion y tipo de renta");
					return;
				}
			}
		}

		if(Amortizacion=="5"&&Ext.getCmp('radioNoAmortiza').checked){
			action = "26forma2a";

		}else{

			if(Ext.isEmpty(FechaPPC)){
				Ext.Msg.alert("Mensaje de p�gina web","Campo obligatorio....<br>Fecha de Primer Pago de capital");
				return;
			}
			if(Ext.isEmpty(DiaPago)){
				Ext.Msg.alert("Mensaje de p�gina web","Campo obligatorio....<br>Dia de Pago");
				return;
			}
			if(Ext.isEmpty(FechaPPI)){
				Ext.Msg.alert("Mensaje de p�gina web","Campo obligatorio....<br>Fecha de Primer Pago de interes");
				return;
			}
			if(Ext.isEmpty(FechaVencDscto)){
				Ext.Msg.alert("Mensaje de p�gina web","Campo obligatorio....<br>Fecha de Vencimiento de descuento");
				return;
			}
		}
		if(plazos=="1"){
			if(Ext.getCmp('tipoPlazo').getValue()=="C"){
				if(Ext.isEmpty(TasaUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La tasa base para el usuario final es obligatoria para operaciones de credito");
					return;
				}
				if(Ext.isEmpty(RelMatUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La relacion matematica para el usuario final es obligatoria para operaciones de credito");
					return;
				}
				if(Ext.isEmpty(SobreTasaUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La sobre tasa para el usuario final es obligatoria para operaciones de credito");
					return;
				}
			}
		} else {
			if(Ext.getCmp('tipoPlazo').getValue()=="C"){
				if(Ext.isEmpty(TasaUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La tasa base para el usuario final es obligatoria para operaciones de credito");
					return;
				}
				if(Ext.isEmpty(RelMatUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La relacion matematica para el usuario final es obligatoria para operaciones de credito");
					return;
				}
				if(Ext.isEmpty(SobreTasaUF)){
					Ext.Msg.alert("Mensaje de p�gina web","La sobre tasa para el usuario final es obligatoria para operaciones de credito");
					return;
				}
			}
		}
		if(Ext.isEmpty(Ext.getCmp('importeDocto').getValue())  ) {
			Ext.Msg.alert("","El Importe del Documento debe ser mayor a cero");
			return;
		}else{
			if(parseFloat(Ext.getCmp('importeDocto').getValue()) <= 0){
				Ext.Msg.alert("","El Importe del Documento debe ser mayor a cero");
				return;
			}
		}
		if(Ext.isEmpty(Ext.getCmp('importeDesc').getValue())  ) {
			Ext.Msg.alert("Mensaje de p�gina web","El Importe del Descuento debe ser mayor a cero");
			return;
		}else{
			if(parseFloat(Ext.getCmp('importeDesc').getValue()) <= 0){
				Ext.Msg.alert("Mensaje de p�gina web","El Importe del Descuento debe ser mayor a cero");
				return;
			}			
		}

		valid = true;
		if(!Ext.getCmp('formaIndividual').getForm().isValid()){
			return;
		}
		/*if(!verificaPanel('formaIndividual')) {
			return;
		}*/

		//--REALIZA PETICION AJAX PARA MOSTRAR LA INFORMACION CONTENIDA EN EL JSP 24forma2b.jsp o el 24forma2a.jsp
		if (action == "26forma2a"){
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '26forma02ext.data.jsp',
				params: {informacion: "NoAmortizaciones", TipoPlazo:Ext.getCmp('tipoPlazo').getValue(),	TipoCredito:Ext.getCmp('tipoCredito').getValue(),
							emisor:Ext.getCmp('emisor').getValue(), Amortiza:Ext.getCmp('Amortizacion').getValue(),	tasaI:Ext.getCmp('tasaBaseI').getValue()},
				callback: procesaNoAmortizaciones
			});
		}else{
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '26forma02ext.data.jsp',
				params: Ext.apply(fpIndividual.getForm().getValues(),{informacion:'Continuar', deFormulario:'forma2'}),
				callback: procesaContinuar
			});
		}
	}

	function validaPlazo(nomObj,i) {
		if(!esVacio(eval("f."+nomObj+".value"))) {
			var resultado = datecomp("<%=FechaVencDMax%>",eval("f."+nomObj+"["+i+"].value"));
			if (resultado==2) {
				Ext.Msg.alert("El plazo m�ximo definido en la base de operaci�n es de <%=plazoMax%> d�as");
				eval("f."+nomObj+"["+i+"].value=\"\"");
				return;
			}
		}
	}

	function calculaMontos(){
		var elementos = amortizaData.getCount();
		var i = 0;
		var montoAmort;
		var impAmortCap		= 0;
		var importeCapturar	= Ext.getCmp('importeDocto').getValue();
		var importeAmorRec	= 0;
		var reg = amortizaData.data.items;
		var  valor =true;
		var importeTotalAmor	= 0;
		
		if(elementos==1){
				montoAmort = reg[0].data.IMPORTE;
				//f.montoAm.value = montoAmort;
				if(reg[0].data.TIPO_AMORT=="I"){
					reg[0].data.IMPORTE = "0";
					montoAmort = 0;
				}
				if(roundOff(importeCapturar,2) - montoAmort <0){
					reg[0].data.IMPORTE = "";
					Ext.Msg.alert("Mensaje de p�gina web.","La suma de montos excede al importe del documento ");
					valor =false;
					//return;
				}
				if(reg[0].data.TIPO_AMORT=="R"){
					importeAmorRec += montoAmort;
				}			
				impAmortCap += montoAmort;		
				importeCapturar -= montoAmort;
				Ext.getCmp('impAmortCap').setValue(Ext.util.Format.number(impAmortCap, '0.00'));
				Ext.getCmp('importeCapturar').setValue(Ext.util.Format.number(importeCapturar, '0.00'));
				
				importeTotalAmor  += parseFloat(montoAmort); 
				
		}else{
			for(i=0;i<elementos;i++){
				montoAmort = reg[i].data.IMPORTE;
				if(reg[i].data.TIPO_AMORT == "I"){
					reg[i].data.IMPORTE="0";
					montoAmort = 0;
				}
				if(roundOff(importeCapturar,2) - montoAmort <0){
					reg[i].data.IMPORTE="";
					Ext.Msg.alert("","La suma de montos excede al importe del documento ");
					valor =false;
				//	return;
				}
				if(reg[i].data.TIPO_AMORT=="R"){
					importeAmorRec += montoAmort;
				}
				impAmortCap		+= montoAmort;
				importeCapturar -= montoAmort;
				importeTotalAmor  += parseFloat(montoAmort); 
			}
		}
		
		if( Ext.getCmp('importeDocto').getValue()!=roundOff(importeTotalAmor,2)){
			Ext.Msg.alert("","La suma de los montos debe de ser igual al Importe del documento  ");
			valor =false;
		}
		
		Ext.getCmp('importeAmorRec').setValue(Ext.util.Format.number(importeAmorRec, '0.00'));
		Ext.getCmp('impAmortCap').setValue(Ext.util.Format.number(impAmortCap, '0.00'));
		Ext.getCmp('importeCapturar').setValue(Ext.util.Format.number(importeCapturar, '0.00'));
			
		amortizaData.commitChanges();
		return valor;
	}
	
	function detTipoAmort(indice){
		var elementos = amortizaData.getCount();
		var fvencAmort;
		var tipoAmort;
		var i = 0;
		var capitalInt = false;
		var FechaLimite	= noAmortiza.fechaLimite;
		var reg = amortizaData.data.items;
		if(elementos==1){
			Ext.Msg.alert("Mensaje de p�gina web.","Deben capturarse por lo menos dos amortizaciones en plan de pagos");
			return;
		}else{
			fvencAmort = Ext.util.Format.date(reg[indice].data.FECHA_VENC,'d/m/Y');
		}
		var comp = datecomp(fvencAmort,FechaLimite);
		if(comp==2||comp==0){
			reg[indice].data.TIPO_AMORT = "R";
		}else if(reg[indice].data.TIPO_AMORT=="R"){
			reg[indice].data.TIPO_AMORT = "I";
		}
		for(i=0;i<indice;i++){
			if(reg[indice].data.TIPO_AMORT == "A"){
				capitalInt = true;
			}
		}
		calculaMontos();
		return true;
	}

	function detTipoAmortXFecha(indice){
		var elementos = amortizaData.getCount();
		var fvencAmort;
		//var tipoAmort;
		var FechaLimite	= noAmortiza.fechaLimite;
		var reg = amortizaData.data.items;
		fvencAmort = Ext.util.Format.date(reg[indice].data.FECHA_VENC,'d/m/Y');
		//tipoAmort	= reg[indice].data.TIPO_AMORT;

		var comp = datecomp(fvencAmort,FechaLimite);
		if(comp==2||comp==0){
			reg[indice].data.TIPO_AMORT = "R";
			reg[indice].data.IMPORTE = "0";
			amortizaData.commitChanges();
			//var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex("IMPORTE");
			//Ext.getCmp('gridEditor').startEditing(indice, col);
		}else{
			detTipoAmortXIndice(indice);
		}
		calculaMontos();
	}

	function detTipoAmortXIndice(indice){
		var elementos = amortizaData.getCount();
		var reg = amortizaData.data.items;
		var i = 0;
		var capitalInt	= false;
		if(elementos==1){
			reg[indice].data.TIPO_AMORT = "I";
		}else{
			for(i=0;i<indice;i++){
				var tipoAmort = reg[indice].data.TIPO_AMORT;
				if(tipoAmort=="A"){
					capitalInt = true;
				}
			}
		}
		calculaMontos();
	}

	function enviar_aceptar(plazos){
		
		if(	!Ext.isEmpty(Ext.getCmp('form_file').getValue())	){
			// Aqui se realiza el submit para la carga del archivo...
			//pnl.el.mask('Procesando...', 'x-mask-loading');
			fpIndividual_a.getForm().submit({
				url: '26forma02exta.data.jsp',
				waitMsg: 'Enviando datos...',
				waitTitle:'Procesando registros',
				success: function(form, action) {
					if (action.result.infoRegresar != undefined){
						var infoR = action.result.infoRegresar;
						var numAmort=Ext.getCmp('noAmortizaciones').getValue();
						if(infoR.carga != 0 && numAmort != infoR.carga){
							Ext.Msg.alert("Mensaje de p�gina web.","El contenido del archivo no coincide con el n�mero de amortizaciones.");
							return;
						}
						if(infoR.nuevoContenido != undefined){
							amortizaData.loadData(infoR.nuevoContenido);
						}
						Ext.getCmp('importeAmorRec').setValue(Ext.util.Format.number(infoR.importeAmorRec, '0.00'));
						Ext.getCmp('impAmortCap').setValue(Ext.util.Format.number(infoR.impAmortCap, '0.00'));
						Ext.getCmp('importeCapturar').setValue(Ext.util.Format.number(infoR.importeCapturar, '0.00'));
					}
					Ext.getCmp('form_file').setValue("");
					pnl.el.unmask();
				},
				failure: NE.util.mostrarSubmitError
			})
		}else{
			//calculaMontos();
			var elementos = amortizaData.getCount();
			var interes		= false;
			var capitalInt	= false;
			var FechaPPC		= "";
			var FechaPPI		= "";
			var FechaVencDscto= "";
			var DiaPago			= "";
			var reg = amortizaData.data.items;
		
			if(calculaMontos()==true){
			
				if(elementos==1){
					Ext.Msg.alert("Mensaje de p�gina web","No se puede capturar solo una amortizaci�n para plan de pagos");
					return;
				}else{
							
					for(i=0;i<elementos;i++){
						var tipoAmort	= reg[i].data.TIPO_AMORT;//f.tipoAmort[i][f.tipoAmort[i].selectedIndex].value;
						var montoAmort	= reg[i].data.IMPORTE;//f.montoAmort[i].value;
						fvencAmort	= Ext.util.Format.date(reg[i].data.FECHA_VENC,'d/m/Y');//f.fvencAmort[i].value;					
						if(i>0){
							var dif = datecomp(fvencAmort,fvencAmortAnt);
							if(dif==2||dif==0){
								Ext.Msg.alert("Mensaje de p�gina web","Las fechas de vencimiento de la amortizaci�n deben estar en orden ascendente");
								return;
							}
						}
						if(Ext.isEmpty(fvencAmort)){
							Ext.Msg.alert("Mensaje de p�gina web","Favor de capturar la Fecha de vencimiento de la amortizaci�n "+(i+1),
								function(){
									var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex("FECHA_VENC");
									Ext.getCmp('gridEditor').startEditing(i, col);
									return;
							});
							//f.fvencAmort[i].focus();
							return;
						}
						if(Ext.isEmpty(tipoAmort)){
							Ext.Msg.alert("Mensaje de p�gina web","Favor de capturar el tipo de la amortizacion "+(i+1),
								function(){
									var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex("TIPO_AMORT");
									Ext.getCmp('gridEditor').startEditing(i, col);
									return;
							});
							//f.tipoAmort[i].focus();
							return;
						}
						if(Ext.isEmpty(montoAmort)){
							Ext.Msg.alert("Mensaje de p�gina web","Favor de capturar el importe de la amortizacion "+(i+1),
								function(){
									var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex("IMPORTE");
									Ext.getCmp('gridEditor').startEditing(i, col);
									return;
							});
							//f.montoAmort[i].focus();
							return;
						}
						if(tipoAmort=="A"){
							capitalInt = true;
							if(FechaPPC=="")
								FechaPPC = fvencAmort;
							if(FechaPPI=="")
								FechaPPI = fvencAmort;
							FechaVencDscto = fvencAmort;
							if(parseFloat(montoAmort)==0){
								Ext.Msg.alert("Mensaje de p�gina web","El importe del pago de capital no puede ser cero",
									function(){
										var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex("IMPORTE");
										Ext.getCmp('gridEditor').startEditing(i, col);
										return;
								});
								//f.montoAmort[i].focus();
								return;
							}
	
						}else if(tipoAmort=="I"){
							interes = true;
							if(FechaPPI=="")
								FechaPPI = fvencAmort;
						}else if(tipoAmort=="R"){
							if(interes||capitalInt){
								Ext.Msg.alert("Mensaje de p�gina web","No se pueden capturar amortizaciones recortadas despues de haber capturado intereses o capital");
								return;
							}
						}
						fvencAmortAnt = fvencAmort;					
					}
				}
			
			
			if(!capitalInt){
				Ext.Msg.alert("Mensaje de p�gina web","Debe capturar por lo menos una amortizacion de capital e interes");
				return;
			}
			if(Ext.getCmp('importeCapturar').getValue()>0){
				Ext.Msg.alert("Mensaje de p�gina web","El importe de las amortizaciones capturadas debe ser igual al importe del documento");
				return;
			}
			if(FechaPPC!=""){
				DiaPago = FechaPPC.substring(0,2);
			}
			Ext.getCmp('fechaPPC').setValue(FechaPPC);
			Ext.getCmp('fechaPPI').setValue(FechaPPI);
			Ext.getCmp('diaPago').setValue(DiaPago);
			Ext.getCmp('fechaVencDscto').setValue(FechaVencDscto);

			//Realizamos peticion Ajax para irnos a la forma2b.jsp
			var icAmort = [];
			var fvencAmort = [];
			var tipoAmort = [];
			var montoAmort = [];
		
			Ext.each(reg, function(item,index,arrItem){
				icAmort.push(item.data.NO_AMORT);
				fvencAmort.push(Ext.util.Format.date(item.data.FECHA_VENC,'d/m/Y'));
				tipoAmort.push(item.data.TIPO_AMORT);
				montoAmort.push(item.data.IMPORTE);			
			});
			
					
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '26forma02ext.data.jsp',
				params: Ext.apply(fpIndividual.getForm().getValues(),{
					informacion: 'Continuar', deFormulario:'forma2a',
					icAmort:		icAmort,
					fvencAmort:	fvencAmort,
					tipoAmort:	tipoAmort,
					montoAmort:	montoAmort}),
				callback: procesaContinuar
			});
			
			}
		}
	}

	function desHabilitaCampo(field){
		if (	Ext.getCmp('Amortizacion').getValue()=="5"	&&	Ext.getCmp('tipoRenta').getValue()=="N"	){
			field.setValue("");
		}
	}

	function menorHoy(field) {
		if(!Ext.isEmpty(field.getValue())){
			var valFecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
			var resultado = datecomp(plazosBO.FechaActual, valFecha );
			if (resultado==1) {
				if(field.id=="fechaPPI"){
					Ext.Msg.alert("","La fecha de primer pago de interes no puede ser anterior a la fecha actual.");
				}else{
					Ext.Msg.alert("","La fecha de Vencimiento no puede ser anterior a la fecha actual.");
				}
				field.setValue("");
				return;
			}
		}
	}

	function validaPlazo(field) {
		if( plazosBO.valida == "S"){
			if(	!Ext.isEmpty( field.getValue() )	){
				var valFecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
				var resultado = datecomp(plazosBO.FecVenDMax,valFecha);
				if (resultado==2) {
					if(field.id=="fechaPPC"){
						Ext.Msg.alert("","La fecha de primer pago de capital debe ser menor a "+ plazosBO.FecVenDMax,
							function(){
								field.setValue("");
								field.focus();
								return;
							});
					}else if(field.id=="fechaPPI"){
						Ext.Msg.alert("","La fecha de primer pago de inter�s debe ser menor a: "+ plazosBO.FecVenDMax,
							function(){
								field.setValue("");
								field.focus();
								return;
							});
					}else{
						Ext.Msg.alert("","El plazo m�ximo definido en la base de operaci�n es de "+ plazosBO.FecVenDMax + " d�as",
							function(){
								field.setValue("");
								field.focus();
								return;
							});
					}
				}
			}
		}
	}

	function validaPlazo_b(value) {
		var valid = true;
		if(	!Ext.isEmpty( value )	){
			var valFecha = Ext.util.Format.date(value,'d/m/Y');
			var resultado = datecomp(plazosBO.FecVenDMax,valFecha);
			if (resultado==2) {
				Ext.Msg.alert("El plazo m�ximo definido en la base de operaci�n es de "+plazosBO.FecVenDMax+" d�as");
				valid = false;
			}else{
				valid = true;
			}
		}
		return valid;
	}

	function diaPago() {
		if ( !Ext.isEmpty(Ext.getCmp('fechaPPC').getValue())	){
			//var dia = Ext.util.Format(Ext.getCmp('fechaPPC').getValue(),'d/m/Y');
			var dia = Ext.util.Format.substr(Ext.util.Format.date(Ext.getCmp('fechaPPC').getValue(),'d/m/Y'),0,2);
			Ext.getCmp('diaPago').setValue(dia);
		}
	}

	function actualizaCombos(){
		var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
		var storeCbo;
		if ( Ext.isEmpty(Ext.getCmp('tipoPlazo').getValue()) ){
			storeCbo = Ext.getCmp('tipoCredito').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('emisor').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('tasaBaseI').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('Amortizacion').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);	

			Ext.getCmp('tipoCredito').setValue("");
			Ext.getCmp('emisor').setValue("");
			Ext.getCmp('tasaBaseI').setValue("");
			Ext.getCmp('Amortizacion').setValue("");

		}else if ( Ext.isEmpty(Ext.getCmp('tipoCredito').getValue()) ){
			storeCbo = Ext.getCmp('emisor').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de cr�dito',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('tasaBaseI').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de cr�dito',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('Amortizacion').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de cr�dito',	loadMsg: null	})	);

			Ext.getCmp('emisor').setValue("");
			Ext.getCmp('tasaBaseI').setValue("");
			Ext.getCmp('Amortizacion').setValue("");
			
		}else if ( Ext.isEmpty(Ext.getCmp('emisor').getValue()) ){
			storeCbo = Ext.getCmp('tasaBaseI').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un emisor',	loadMsg: null	})	);

			storeCbo = Ext.getCmp('Amortizacion').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un emisor',	loadMsg: null	})	);

			Ext.getCmp('tasaBaseI').setValue("");
			storeCbo.removeAll();
			Ext.getCmp('Amortizacion').setValue("");

		}else if ( Ext.isEmpty(Ext.getCmp('tasaBaseI').getValue()) ){
			storeCbo = Ext.getCmp('Amortizacion').getStore();
			storeCbo.removeAll();
			storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de tasa',	loadMsg: null	})	);
			Ext.getCmp('Amortizacion').setValue("");
		}
		Ext.getCmp('codigoBase').hide();
		Ext.getCmp('msgFechaMax').hide();
		Ext.getCmp('radioSiAmortiza').checked = false;
		Ext.getCmp('radioNoAmortiza').checked = false;
		Ext.getCmp('radioGpAmortiza').hide();
		Ext.getCmp('fechaVencDocto').hide();
		Ext.getCmp('fechaVencDscto').hide();
		Ext.getCmp('fechaEmisionTC').hide();
		Ext.getCmp('fechaPPC').hide();
		Ext.getCmp('diaPago').hide();
		Ext.getCmp('fechaPPI').hide();
		Ext.getCmp('lugarFirma').hide();
		Ext.getCmp('btnContinuar').hide();
		Ext.getCmp('btnCancelar').hide();
		Ext.getCmp('imporAmortAjuste').hide();
		Ext.getCmp('fechaAmortAjuste').hide();
		Ext.getCmp('amortizacionAjuste').hide();

		Ext.getCmp('codigoBase').reset();
		Ext.getCmp('msgFechaMax').body.update('&nbsp;');
		Ext.getCmp('tipoRenta').reset();
		Ext.getCmp('fechaVencDocto').reset();
		Ext.getCmp('fechaVencDscto').reset();
		Ext.getCmp('fechaEmisionTC').reset();
		Ext.getCmp('fechaPPC').reset();
		Ext.getCmp('diaPago').reset();
		Ext.getCmp('fechaPPI').reset();
		Ext.getCmp('lugarFirma').reset();
		Ext.getCmp('imporAmortAjuste').reset();
		Ext.getCmp('fechaAmortAjuste').reset();
		Ext.getCmp('amortizacionAjuste').reset();
		plazosBO.valida="";
		plazosBO.FechaActual="";
		plazosBO.FecVenDMax="";
		plazosBO.puedeUsarCodigoBO=null;
		Ext.EventManager.onWindowResize(fpIndividual.doLayout(), fpIndividual);
	}

	function recargar(operacion){
		if(operacion==0){
			Ext.getCmp('tipoCredito').setValue("");
			Ext.getCmp('tipoCredito').store.removeAll();
			Ext.getCmp('emisor').setValue("");
			Ext.getCmp('emisor').store.removeAll();
			Ext.getCmp('tasaBaseI').setValue("");
			Ext.getCmp('tasaBaseI').store.removeAll();
			Ext.getCmp('Amortizacion').setValue("");
			Ext.getCmp('Amortizacion').store.removeAll();
		}
		if(operacion==2){
			Ext.getCmp('Amortizacion').setValue("");
			Ext.getCmp('Amortizacion').store.removeAll();
		}
	}
	// Termina funciones extras -------------------------------------------------
	
	//var form = {tipoPlazo:""}
	/*********   HANDLER�S Y FUNCTIONS   **************/

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (!Ext.isEmpty(infoR.msg)){
				Ext.Msg.alert('Aviso', infoR.msg, 
					function(){
						pnl.removeAll();
						pnl.doLayout();
						return;
				});
				return;
			}
			Ext.getCmp('mensajeBloqueo').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			Ext.getCmp('mensajeBloqueo_b').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			Ext.getCmp('mensajeBloqueo_c').body.update('<div align="center" class="formas"><b>'+infoR.mensajeBloqueo+'</b></div>');
			valInicio.tipoCartera = infoR.tipoCartera;
			valInicio.plazos = infoR.plazos;
			if (infoR.plazos == "1"){
				Ext.getCmp('tipoPlazo').setValue(infoR.tipo);
				Ext.getCmp('radioGrupoPlazo').hide();
				
				if(Ext.getCmp('tipoPlazo').getValue() == "F"){
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
					Ext.getCmp('DestinoCredito1').allowBlank = true;
					Ext.getCmp('DestinoCredito1').hide();
				}else if(Ext.getCmp('tipoPlazo').getValue() == "C"){
					Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
					Ext.getCmp('DestinoCredito1').allowBlank = false;
					Ext.getCmp('DestinoCredito1').show();
				}
				Ext.getCmp('tipoCredito').setValue("");
				Ext.getCmp('tipoCredito').store.removeAll();
				Ext.getCmp('tipoCredito').store.reload({params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue()}});
				actualizaCombos();
			}else{
				if (infoR.plazos == "0"){
					Ext.getCmp('tipoPlazo').setValue("N");
				}
				Ext.getCmp('radioGrupoPlazo').show();
			}
			if ( Ext.isEmpty(Ext.getCmp('tipoPlazo').getValue()) ){
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				var storeCbo;
				storeCbo = Ext.getCmp('tipoCredito').getStore();
				storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

				reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				storeCbo = Ext.getCmp('emisor').getStore();
				storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

				reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				storeCbo = Ext.getCmp('tasaBaseI').getStore();
				storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

				reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				storeCbo = Ext.getCmp('Amortizacion').getStore();
				storeCbo.insert(	0,new reg({	clave: "",	descripcion: 'Seleccione un tipo de plazo',	loadMsg: null	})	);

				Ext.getCmp('tipoCredito').setValue("");
				Ext.getCmp('emisor').setValue("");
				Ext.getCmp('tasaBaseI').setValue("");
				Ext.getCmp('Amortizacion').setValue("");
			}
			catalogoCapital.load();
			catalogoIntereses.load();
			catalogoDocumento.load();
			catalogoTasa.load();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
		catalogoMonedaData.load();
	}

	function procesaNoAmortizaciones(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			noAmortiza.fechaLimite = infoR.fechaLimite;
			Ext.getCmp('fechaLimite_').setValue(infoR.fechaLimite);
			Ext.getCmp('montoDocto').setValue(Ext.getCmp('importeDocto').getValue());
			Ext.getCmp('mensajeAmort').body.update('<div align="center" class="formas">Nota: La fecha l�mite para considerar como amortizaci�n recortada es el '+infoR.fechaLimite+'</div>');
			Ext.getCmp('importDocto').body.update('<div class="formas" align="right"> $ '+Ext.getCmp('importeDocto').getValue()+'</div>');
			Ext.getCmp('importDesc').body.update('<div class="formas" align="right"> $ '+Ext.getCmp('importeDesc').getValue()+'</div>');
			Ext.getCmp('importeCapturar').setValue(Ext.getCmp('importeDocto').getValue());
			amortizaData.loadData('');

			var reg = Ext.data.Record.create(['NO_AMORT','FECHA_VENC', 'TIPO_AMORT',	'IMPORTE']);
			var temp=Ext.getCmp('noAmortizaciones').getValue();
			for(var i=0;i<temp;i++){
				amortizaData.add(	new reg({NO_AMORT:(i+1),FECHA_VENC: '',TIPO_AMORT: '',IMPORTE: ''}) );
			}
			fpIndividual.setVisible(false);
			fpIndividual_a.setVisible(true);
			Ext.EventManager.onWindowResize(fpIndividual_a.doLayout(), fpIndividual_a);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaObtieneNombre(opts, success, response) {
		fpIndividual.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('nombre').setValue(infoR.Nombre);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaPlazosBaseOperacion(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			plazosBO.valida = infoR.Valida;
			plazosBO.FecVenDMax = infoR.FecVenDMax;
			plazosBO.FechaActual = infoR.FechaActual;
			plazosBO.puedeUsarCodigoBO = infoR.puedeUsarCodigoBO;
			Ext.getCmp('msgFechaMaxB').body.update('<div><b>La fecha m�xima de vencimiento del descuento es: '+infoR.FecVenDMax+'<br>&nbsp;</b><br></div>');
			if (infoR.puedeUsarCodigoBO){
				Ext.getCmp('codigoBase').show();
			}
			if(infoR.Valida == "S"){
				Ext.getCmp('msgFechaMax').show();
				Ext.getCmp('msgFechaMax').body.update('<div><b>La fecha m�xima de vencimiento del descuento es: '+infoR.FecVenDMax+'</b></div>');
			}
			if(opts.params.Amortiza == "5" && Ext.getCmp('tipoRenta').getValue() != "N"){
				Ext.getCmp('radioGpAmortiza').reset();
				Ext.getCmp('radioGpAmortiza').show();
			}
			if(opts.params.Amortiza == "5" && Ext.getCmp('tipoRenta').getValue() == "N"){
				Ext.getCmp('radioGpAmortiza').reset();
				Ext.getCmp('radioGpAmortiza').show();
				Ext.getCmp('fechaVencDocto').show();
				Ext.getCmp('lugarFirma').show();
				Ext.getCmp('fechaEmisionTC').show();
			}else if(	!(opts.params.Amortiza == "5" && Ext.isEmpty(Ext.getCmp('tipoRenta').getValue()))	){
				Ext.getCmp('fechaPPC').show();
				Ext.getCmp('diaPago').show();
				Ext.getCmp('fechaPPI').show();
				Ext.getCmp('fechaVencDocto').show();
				Ext.getCmp('fechaVencDscto').show();
				Ext.getCmp('fechaEmisionTC').show();
				Ext.getCmp('lugarFirma').show();
				Ext.getCmp('btnContinuar').show();
				Ext.getCmp('btnCancelar').show();
				if(opts.params.Amortiza == "3"){
					Ext.getCmp('imporAmortAjuste').show();
					Ext.getCmp('fechaAmortAjuste').show();
					Ext.getCmp('amortizacionAjuste').show();
				}
			}
			Ext.EventManager.onWindowResize(fpIndividual.doLayout(), fpIndividual);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function muestraOcultaDatosComplementarios(){
		Ext.getCmp('codigoBase').hide();
		Ext.getCmp('fechaVencDocto').hide();
		Ext.getCmp('fechaVencDscto').hide();
		Ext.getCmp('fechaEmisionTC').hide();
		Ext.getCmp('fechaPPC').hide();
		Ext.getCmp('diaPago').hide();
		Ext.getCmp('fechaPPI').hide();
		Ext.getCmp('lugarFirma').hide();
		Ext.getCmp('imporAmortAjuste').hide();
		Ext.getCmp('fechaAmortAjuste').hide();
		Ext.getCmp('amortizacionAjuste').hide();

		Ext.getCmp('codigoBase').reset();
		Ext.getCmp('fechaVencDocto').reset();
		Ext.getCmp('fechaVencDscto').reset();
		Ext.getCmp('fechaEmisionTC').reset();
		Ext.getCmp('fechaPPC').reset();
		Ext.getCmp('diaPago').reset();
		Ext.getCmp('fechaPPI').reset();
		Ext.getCmp('lugarFirma').reset();
		Ext.getCmp('imporAmortAjuste').reset();
		Ext.getCmp('fechaAmortAjuste').reset();
		Ext.getCmp('amortizacionAjuste').reset();

		if(!Ext.isEmpty(Ext.getCmp('Amortizacion').getValue())){
			if(plazosBO.puedeUsarCodigoBO){
				Ext.getCmp('codigoBase').show();
			}
			if(Ext.getCmp('Amortizacion').getValue() == "5" && Ext.getCmp('tipoRenta').getValue() == "N"){
				Ext.getCmp('fechaVencDocto').show();
				Ext.getCmp('lugarFirma').show();
				Ext.getCmp('fechaEmisionTC').show();
			}else if(	!(Ext.getCmp('Amortizacion').getValue() == "5" && Ext.isEmpty(Ext.getCmp('tipoRenta').getValue()))	){
				Ext.getCmp('fechaPPC').show();
				Ext.getCmp('diaPago').show();
				Ext.getCmp('fechaPPI').show();
				Ext.getCmp('fechaVencDocto').show();
				Ext.getCmp('fechaVencDscto').show();
				Ext.getCmp('fechaEmisionTC').show();
				Ext.getCmp('lugarFirma').show();
				if( Ext.getCmp('Amortizacion').getValue() == "3"){
					Ext.getCmp('imporAmortAjuste').show();
					Ext.getCmp('fechaAmortAjuste').show();
					Ext.getCmp('amortizacionAjuste').show();
				}
			}
		}
		Ext.getCmp('btnContinuar').show();
		Ext.getCmp('btnCancelar').show();
		Ext.EventManager.onWindowResize(fpIndividual.doLayout(), fpIndividual);
	}
	/*********   STORE�S   **************/

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoMonedaDist'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	//var procesaCatalogoTipoCred = function(store, arrRegistros, opts) {	}
	var procesaCatalogoTipoCredito = function(store, arrRegistros, opts) {
		if (store.getTotalCount() < 1){
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'No existen datos',	loadMsg: null	})	);
			Ext.getCmp('tipoCredito').setValue("");
		}
	}

	var catalogoTipoCredito = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTipoCredito'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	load:procesaCatalogoTipoCredito,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}	//load: procesaCatalogoTipoCred,	
	});

	var procesaCatNombreEmisor = function(store, arrRegistros, opts) {
		//if (store.getTotalCount() < 1){
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "0",	descripcion: 'Sin emisor',	loadMsg: null	})	);
		//}
	}

	var catalogoNombreEmisor = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoNombreEmisor'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	load:procesaCatNombreEmisor, exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoTasaBaseI = function(store, arrRegistros, opts) {
		if (store.getTotalCount() < 1){
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'No existen datos',	loadMsg: null	})	);
		}
	}

	var catalogoTasaBaseI = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTasaBaseI'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	load:procesaCatalogoTasaBaseI,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var procesaCatalogoAmortiza = function(store, arrRegistros, opts) {
		if (store.getTotalCount() < 1){
			var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
			store.insert(	0,new reg({	clave: "",	descripcion: 'No existen datos',	loadMsg: null	})	);
		}
	}

	var catalogoAmortizacion = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoAmortizacion'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	load:procesaCatalogoAmortiza,	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoCapital = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoCapital'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoIntereses = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoIntereses'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});
	var catalogoDocumento = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoDocumento'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var catalogoTasa = new Ext.data.JsonStore({
		root : 'registros',	fields : ['clave', 'descripcion', 'loadMsg'],	url : '26forma02ext.data.jsp',
		baseParams: {	informacion: 'CatalogoTasa'	},	totalProperty : 'total',	autoLoad: false,
		listeners: {	exception: NE.util.mostrarDataProxyError,	beforeload: NE.util.initMensajeCargaCombo	}
	});

	var relMatData = new Ext.data.ArrayStore({	fields: ['clave', 'descripcion'],	data : [['+','+']]	});

	var amortAjusteData = new Ext.data.ArrayStore({	fields: ['clave', 'descripcion'],	data : [['P','Primera'],['U','�ltima']]	});

	var tipoAmortData = new Ext.data.ArrayStore({	fields: ['clave', 'descripcion'],	data : [['R','Recortada'],['I','Interes'],['A','Capital e Intereses']]	});
	
	gridLayoutData = new Ext.data.JsonStore({
		fields:[{name:'NUMERO'},
					{name: 'CAMPO'},
					{name: 'TIPO_DATO'},
					{name: 'LONGITUD'},
					{name: 'DESCRIPCION'}],
		data:  [
					{'NUMERO':'1','CAMPO':'N�mero Amortizaci�n', 'TIPO_DATO':'N�merico',	'LONGITUD':'5','DESCRIPCION':'N�mero Consecutivo de la Amortizaci�n'},
					{'NUMERO':'2','CAMPO':'Fecha de Vencimiento','TIPO_DATO':'Fecha',		'LONGITUD':'10','DESCRIPCION':'Fecha de Vencimiento del Cr�dito'},
					{'NUMERO':'3','CAMPO':'Tipo de Amortizaci�n','TIPO_DATO':'Num�rico',	'LONGITUD':'10','DESCRIPCION':'Amortizaci�n que aplicar� para ese pago'},
					{'NUMERO':'4','CAMPO':'Importe Amortizaci�n','TIPO_DATO':'Num�rico',	'LONGITUD':'10','DESCRIPCION':'Monto de la Amortizaci�n'}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var amortizaData = new Ext.data.JsonStore({
		fields:[ {name:'NO_AMORT'},
					{name: 'FECHA_VENC', type:'date', dateFormat:'d/m/Y'},
					{name: 'TIPO_AMORT'},
					{name: 'IMPORTE'},
		],
		data:  [	{'NO_AMORT':'','FECHA_VENC':'', 'TIPO_AMORT':'',	'IMPORTE':''}	],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridResumenData = new Ext.data.JsonStore({
		fields:[{name:'NOMBRE_CLI'},
					{name: 'NO_SIRAC'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_EMISION',	type:'date', dateFormat:'d/m/Y'},
					{name: 'FECHA_VENC',	type:'date', dateFormat:'d/m/Y'},
					{name: 'MONEDA'},
					{name: 'TIPO_CREDITO'},
					{name: 'TASA_IF'},
					{name: 'NO_AMORTIZA'},
					{name: 'TABLA_AMORTIZA'},
					{name: 'MONTO_DOCTO'},
					{name: 'MONTO_DESC'}	],
		data:  [
					{'NOMBRE_CLI':'','NO_SIRAC':'', 'NO_DOCTO':'',	'FECHA_EMISION':'','FECHA_VENC':'','MONEDA':'',
					'TIPO_CREDITO':'','TASA_IF':'','NO_AMORTIZA':'','TABLA_AMORTIZA':'','MONTO_DOCTO':'','MONTO_DESC':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridResumen_bData = new Ext.data.JsonStore({
		fields:[{name:'NOMBRE_EMISOR'},
					{name: 'CLASE_DOCTO'},
					{name: 'LUGAR'},
					{name: 'FECHA_EMISION',	type:'date', dateFormat:'d/m/Y'},
					{name: 'DOMICILIO'},
					{name: 'TASA_INTERES'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_VENC',	type:'date', dateFormat:'d/m/Y'},
					{name: 'MONTO_OPERA'},
					{name: 'DEST_CREDITO'}
		],
		data:  [
					{'NOMBRE_EMISOR':'','CLASE_DOCTO':'', 'LUGAR':'',	'FECHA_EMISION':'','DOMICILIO':'','DEST_CREDITO':'','TASA_INTERES':'','NO_DOCTO':'','FECHA_VENC':'','MONTO_OPERA':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var totalesData = new Ext.data.JsonStore({
		fields: [	{name: 'TOTAL'},	{name: 'TOTAL_MONTO'}	],
		data:[{'TOTAL':'Total:','TOTAL_MONTO':''}],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAcuseData = new Ext.data.JsonStore({
		fields:[ {name:'FOLIO_OPERA'},
					{name:'NOMBRE_CLI'},
					{name: 'NO_SIRAC'},
					{name: 'NO_DOCTO'},
					{name: 'FECHA_EMISION'},
					{name: 'FECHA_VENC'},
					{name: 'MONEDA'},
					{name: 'TIPO_CREDITO'},
					{name: 'TASA_IF'},
					{name: 'NO_AMORTIZA'},
					{name: 'TABLA_AMORTIZA'},
					{name: 'MONTO_DOCTO'},
					{name: 'MONTO_DESC'}	],
		data:  [
					{'FOLIO_OPERA':'','NOMBRE_CLI':'','NO_SIRAC':'', 'NO_DOCTO':'',	'FECHA_EMISION':'','FECHA_VENC':'','MONEDA':'',
					'TIPO_CREDITO':'','TASA_IF':'','NO_AMORTIZA':'','TABLA_AMORTIZA':'','MONTO_DOCTO':'','MONTO_DESC':''}	],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAsistenciaData = new Ext.data.JsonStore({
		fields:[{name:'TIPO_PLAZO'},
					{name: 'TIPO_CREDITO'},
					{name: 'EMISOR'},
					{name: 'TASA'},
					{name: 'TIPO_AMORT'},
					{name: 'PMIN'},
					{name: 'PMAX'},
					{name: 'PERIODICIDAD'},
					{name: 'TIPO_INTERES'},
					{name: 'TIPO_RENTA'}
		],
		data:  [
					{'TIPO_PLAZO':'','TIPO_CREDITO':'', 'EMISOR':'',	'TASA':'','TIPO_AMORT':'','PMIN':'','PMAX':'','PERIODICIDAD':'','TIPO_INTERES':'','TIPO_RENTA':''}
		],
		autoLoad: true,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	/*********   COMPONENTES   **************/
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [	[	{header: '<div align="center">Layout para la generaci�n de la Tabla de Amortizaci�n de Plan de Pagos<br>'+
									'Archivo de extensi�n TXT y los campos deben estar separados por pipes "|"</div>', 
						colspan:5, align:'center'}	]	]
	});

	var elementosIndividual_c = [
		{
			xtype:'panel',	id:'mensajeBloqueo_c',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype:'displayfield', value:''
		},{
			xtype: 'panel',layout:'table',	width:600,	border:true,	frame:false,	layoutConfig:{ columns: 2 },	style: 'margin:0 auto;',
			defaults: {frame:false, border: true,width:200,	height:35,	bodyStyle:'padding:6px'},
			items:[
				{	width:600,	id:'disMsjFirma',	colspan:2,	border:false,	height:45,	html:'&nbsp;'	},
				{	width:600,	colspan:2,	border:false,	frame:true,	html:'<div align="center">Resumen de Solicitudes Cargadas</div>'	},
				{	html:'<div class="formas" align="right">No. de Acuse:</div>'	},
				{	width:600,id:'disAcuse',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">No. total de operaciones registradas:</div>'	},
				{	width:600,id:'disTotOpera',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Monto total del importe de los documentos:</div>'	},
				{	width:600,id:'disImpDocto',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Monto total del importe de los descuentos:</div>'	},
				{	width:600,id:'disImpDesc',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Fecha de carga:</div>'	},
				{	width:600,id:'disFechaCarga',	html:'&nbsp;'	},
				{	html:'<div class="formas" align="right">Hora de carga:</div>'	},
				{	width:600,id:'disHoraCarga',	html:'&nbsp;'},
				{	html:'<div class="formas" align="right">Usuario:</div>'	},
				{	width:600,id:'disUsuario',html: '&nbsp;'	}
			]
		},{
			xtype:'displayfield', value:''
		},{
			xtype:'grid',	id:'gridAcuse',	store:gridAcuseData,	stripeRows:true,	loadMask:true,	height:200,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Folio Operaci�n',	tooltip:'Folio Operaci�n', dataIndex:'FOLIO_OPERA',	width:100,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'Nombre del Cliente',		tooltip:'Nombre del Cliente', dataIndex:'NOMBRE_CLI',		width:100,	align:'center', resizable:true, sortable:true},
				{header:'N�mero de Cliente Sirac',tooltip:'N�mero de Cliente Sirac',	dataIndex:'NO_SIRAC',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de documento',	tooltip:'N�mero de documento',	dataIndex:'NO_DOCTO',	width:120,	align:'center', resizable:true},
				{header:'Fecha Emisi�n del T�tulo de Cr�dito',	tooltip:'Fecha Emisi�n del T�tulo de Cr�dito',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center', sortable:true,	resizable:true},
				{header:'Fecha de Vencimiento del Descuento',	tooltip:'Fecha de Vencimiento del Descuento', dataIndex:'FECHA_VENC',
					width:120,	align:'center', sortable:true,	resizable:true},
				{header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tipo de cr�dito',		tooltip:'Tipo de cr�dito',	dataIndex:'TIPO_CREDITO',	width:150,	align:'center', sortable:true},
				{header:'Tasa Intermediario Financiero',	tooltip:'Tasa Intermediario Financiero',	dataIndex:'TASA_IF',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de Amortizaciones',	tooltip:'N�mero de Amortizaciones',	dataIndex:'NO_AMORTIZA',	width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Tabla Amortizaci�n',		tooltip:'Tabla Amortizaci�n',		dataIndex:'TABLA_AMORTIZA',width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Monto del Documento',		tooltip:'Monto del Documento',		dataIndex:'MONTO_DOCTO',	width:130,	align:'right', resizable:true,	sortable:true},
				{header:'Monto del Descuento',		tooltip:'Monto del Descuento',		dataIndex:'MONTO_DESC',		width:130,	align:'right', resizable:true,	sortable:true}
			],
			bbar:{
				buttonAlign:"center",
				items: [
					'-',' ',
					{
						text:'Generar Pdf',	id:'btnGenerarPDF',	iconCls:'icoPdf'
					},{
						text:'Bajar Pdf',	id:'btnBajarPdf',	hidden:true
					},' ','-',' ',{
						xtype:'button',	text:'Salir',	id:'btnSalir', iconCls:'icoLimpiar',
						handler: function(boton, evento){
							window.location = '26forma02ext.jsp';
						}
					},' ','-'
				]
			}
		}
	];
	
	var fpIndividual_c = new Ext.form.FormPanel({
		id: 'formaIndividual_c',
		width: 900,
		heigth:'auto',
		style: 'margin:0 auto;',
		frame: false,
		border:false,
		hidden:true,
		bodyStyle: 'padding: 4px',
		labelWidth: 150,
		items: elementosIndividual_c,
		buttonAlign: 'center'
	});

	
	var fnConfirmarResumenCallback = function(vpkcs7, vtextoFirmar, vicProcSolic){
		if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}
		pnl.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '26forma02ext.data.jsp',
			params: Ext.apply({	informacion: 'Confirmar',
										pkcs7: vpkcs7,
										textoFirmado: vtextoFirmar,
										icProcSolic: vicProcSolic
									}),
			callback: procesarConfirmar
		});
	}
	
	var elementosIndividual_b = [
		{
			xtype:'panel',	id:'mensajeBloqueo_b',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype: 'panel',layout:'table',	width:550,	border:true,	layoutConfig:{ columns:2 },	style:'margin:0 auto;',
			defaults: {frame:false, border: true,width:350, height: 30,bodyStyle:'padding:2px'},
			items:[
				{	frame:true,	width:550,	colspan:2,	html:'<div align="center">Resumen de solicitudes</div>'	},
				{	html:'<div align="left" class="formas">No. total de operaciones registradas</div>'	},
				{	width:200,	html:'<div align="right">1</div>'	},
				{	html:'<div align="left" class="formas">Monto total del importe de los documentos</div>'	},
				{	width:200,	id:'montoImpDocto',	html:'&nbsp;Aqui va ir importeDocto'	},
				{	html:'<div align="left" class="formas">Monto total del importe de los descuentos</div>'	},
				{	width:200,	id:'montoImpDesc',	html:'&nbsp;Aqui va ir importeDesc'	},
				{	height:115,	width:550,	colspan:2,
					html:'<div class="formas">Declaro bajo protesta de decir verdad que soy tenedor y propietario de los documentos'+ 
							'que aqu&iacute; se describen en forma electr&oacute;nica. Dicha declaraci&oacute;n tendr&aacute; plena validez para todos los efectos legales conducentes.<br><br>'+
							'Con fundamento en lo dispuesto por los art&iacute;culos 1205 y 1298-A del C&oacute;digo de Comercio, la informaci&oacute;n e instrucciones que el Intermediario y Nafin '+
							'se transmitan o comuniquen mutuamente mediante el sistema, tendr&aacute;n pleno valor probatorio y fuerza legal para acreditar la operaci&oacute;n realizada,'+
							'el importe de la misma, su naturaleza, as&iacute; como las caracter&iacute;sticas y alcance de sus instrucciones.</div>'
				}
			]
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'grid',	id:'gridResumen',	store:gridResumenData,	stripeRows:true,	loadMask:true,	height:200,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Nombre del Cliente',		tooltip:'Nombre del Cliente', dataIndex:'NOMBRE_CLI',		width:100,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'N�mero de Cliente Sirac',tooltip:'N�mero de Cliente Sirac',	dataIndex:'NO_SIRAC',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de documento',	tooltip:'N�mero de documento',	dataIndex:'NO_DOCTO',	width:120,	align:'center', resizable:true},
				{header:'Fecha Emisi�n del T�tulo de Cr�dito',	tooltip:'Fecha Emisi�n del T�tulo de Cr�dito',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center', sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Fecha de Vencimiento del Descuento',	tooltip:'Fecha de Vencimiento del Descuento', dataIndex:'FECHA_VENC',
					width:120,	align:'center', sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Moneda',	tooltip:'Moneda',	dataIndex:'MONEDA',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tipo de cr�dito',		tooltip:'Tipo de cr�dito',	dataIndex:'TIPO_CREDITO',	width:150,	align:'center', sortable:true},
				{header:'Tasa Intermediario Financiero',	tooltip:'Tasa Intermediario Financiero',	dataIndex:'TASA_IF',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'N�mero de Amortizaciones',	tooltip:'N�mero de Amortizaciones',	dataIndex:'NO_AMORTIZA',	width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Tabla de amortizaci�n',		tooltip:'Tabla de amortizaci�n',		dataIndex:'TABLA_AMORTIZA',width:150,	align:'center',resizable:true,	sortable:true},
				{header:'Importe del Documento',		tooltip:'Importe del Documento',		dataIndex:'MONTO_DOCTO',	width:130,	align:'right', resizable:true,	sortable:true},
				{header:'Importe del Descuento',		tooltip:'Importe del Descuento',		dataIndex:'MONTO_DESC',		width:130,	align:'right', resizable:true,	sortable:true}
			],
			bbar:{
				buttonAlign:"center",
				items: [
					'-',' ',
					{
						xtype: 'button',	text:'Confirmar',	id:'btnConfirmarResumen', iconCls:'icoAceptar',
						handler: function(boton, evento) {
								
							NE.util.obtenerPKCS7(fnConfirmarResumenCallback, continua.textoFirmar, continua.icProcSolic);
							
						}
					},' ','-',' ',{
						xtype:'button',	text:'Cancelar',	id:'btnCancelarResumen', iconCls:'icoRechazar',
						handler: function(boton, evento) {
							Ext.Msg.confirm("Mensaje de p�gina web.","�Esta usted seguro de cancelar la operaci�n?",
								function(res){
									if(res=='no' || res == 'NO'){
										return;
									}else{
										window.location = '26forma02ext.jsp';
									}
							});
						}
					},' ','-'
				]
			}
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes',	border:false,	html:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes_b',	border:false,	html:'&nbsp;Algo'
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'grid',	id:'gridResumen_b',	store:gridResumen_bData,	stripeRows:true,	loadMask:true,	height:200,	width:880,	anchor:'100%',	hidden:false, frame:true,
			columLines:true,	enableColumnMove:false, style:'margin:0 auto;',	header:false,
			columns: [
				{header:'Nombre del Emisor',	tooltip:'Nombre del Emisor', dataIndex:'NOMBRE_EMISOR',	width:150,	align:'center', resizable:true, hideable:false, sortable:true},
				{header:'Clase de Documento',	tooltip:'Clase de Documento',	dataIndex:'CLASE_DOCTO',width:150,	align:'center', resizable:true,	sortable:true},
				{header:'Lugar de Emisi�n',	tooltip:'Lugar de Emisi�n',	dataIndex:'LUGAR',	width:120,	align:'center', resizable:true},
				{header:'Fecha de Emisi�n',	tooltip:'Fecha de Emisi�n',	dataIndex:'FECHA_EMISION',
					width:120,	align:'center',sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Domicilio de Pago',	tooltip:'Domicilio de Pago',	dataIndex:'DOMICILIO',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Destino del Cr�dito',	tooltip:'Destino del Cr�dito',	dataIndex:'DEST_CREDITO',	width:150,	align:'center', resizable:true, sortable:true},
				{header:'Tasa de Interes',		tooltip:'Tasa de Interes',	dataIndex:'TASA_INTERES',	width:150,	align:'center', sortable:true},
				{header:'N�mero de Documento',	tooltip:'N�mero de Documento',	dataIndex:'NO_DOCTO',	width:150,	align:'center', resizable:true,	sortable:true},
				{header:'Fecha de Vencimiento',	tooltip:'Fecha de Vencimiento', dataIndex:'FECHA_VENC',
					width:120,	align:'center',	sortable:true,	resizable:true,	renderer:Ext.util.Format.dateRenderer('d/m/Y')},
				{header:'Importe de la Operaci�n',	tooltip:'Importe de la Operaci�n',	dataIndex:'MONTO_OPERA',	width:120,	align:'right', resizable:true,	sortable:true}
			]
		},{
			xtype:'grid',	id:'gridTotales',	store:totalesData,	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	anchor:'100%',
			width:880,	height:50,	title:'Totales', hidden:false,	frame:true, header:false,	style:'margin:0 auto;',
			columns: [
				{
					header: '',	dataIndex: 'TOTAL',	align:'right',	width:700, menuDisabled:true
				},{
					header: '',	dataIndex: 'TOTAL_MONTO',	width:180,	align:'center',	renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
				}
			]
		},{
			xtype:'displayfield',	value:''
		},{
			xtype:'panel', id:'msjTipoPlazoRes_c',	border:false,	html:'&nbsp;Facultado por'
		}
	];

	var fpIndividual_b = new Ext.form.FormPanel({
		id: 'formaIndividual_b',
		width: 900,
		heigth:'auto',
		style: 'margin:0 auto;',
		frame: false,
		border:false,
		hidden:true,
		bodyStyle: 'padding: 4px',
		labelWidth: 150,
		items: elementosIndividual_b,
		buttonAlign: 'center'
	});

	var elementosIndividual_a = [
		{
			xtype:'panel',	style:'margin:0 auto;',	id:'msgFechaMaxB',	border:false,	html:'fecha descuento&nbsp;'
		},{
			xtype:'hidden',	id:'montoDocto',	name:'_importDocto_',	value:''
		},{
			xtype:'hidden',	id:'fechaLimite_', name:'fechaLimite',	value:''
		},{
			xtype:'panel',	layout:'column',	anchor:'100%',	defaults: {	bodyStyle:'padding:6px'	}, border:false, 	style:'margin:0 auto;', //width: 600,
			items:[
				{
					xtype: 'button',	columnWidth: .05,	autoWidth: true,	autoHeight: true,	iconCls: 'icoAyuda',
					handler: function() {
						if (!Ext.getCmp('gridLayout').isVisible()){
							Ext.getCmp('gridLayout').show();
							Ext.EventManager.onWindowResize(fpIndividual_a.doLayout(), fpIndividual_a);
						}
					}
				},{
					xtype:'panel',	id:'pnlArchivo',	columnWidth:.95,	anchor:'100%',	layout:'form',	fileUpload:true,	labelWidth:2,	border:false,
					defaults: {bodyStyle:'padding:5px',	msgTarget:'side',	anchor:'-20'},
					items: [
						{
							xtype: 'fileuploadfield',
							id: 'form_file',
							anchor: '90%',
							allowBlank: true,
							blankText:	'Debe seleccionar una ruta de archivo.',
							emptyText: 'Seleccione un archivo',
							name: 'txtarchivo',
							regex:	/^.+\.([tT][xX][tT])$/,
							regexText:'El archivo debe tener extensi�n .txt',
							buttonText: 'Examinar...'
						}
					]
				}
			]
		},{
			xtype:'grid',	id:'gridLayout',	store:gridLayoutData,	stripeRows:true,	loadMask:true,	height:180,	width:690,	anchor:'100%',	hidden:true, frame:false,
			title: '<div align="center">Estructura de Archivo <br>Carga Masiva de Operaciones</div>',	plugins: grupos,	columLines:true,	enableColumnMove:false, style:'margin:0 auto;',
			columns: [
				{header:'N�m.',		tooltip:'No. de Campo', dataIndex:'NUMERO', width:70,	align:'center', resizable:false, menuDisabled:true},
				{header:'Campo',		tooltip:'Campo',	dataIndex:'CAMPO',	width : 150,	align:'center', resizable:true,	menuDisabled:true},
				{header:'Tipo',		tooltip:'Tipo',	dataIndex:'TIPO_DATO',	width : 100,align:'center', resizable:false,menuDisabled:true},
				{header:'Longitud',	tooltip:'Longitud',	dataIndex:'LONGITUD', width : 90,align:'center', resizable:false, menuDisabled:true},
				{header:'Descripci�n',tooltip:'Descripci�n', dataIndex:'DESCRIPCION', width:270, align:'center', resizable:false, menuDisabled:true}
			],
			tools: [
				{
					id: 'close',
					handler: function(evento, toolEl, panel, tc) {
						panel.hide();
					}
				}
			]
		},{
			xtype:'displayfield', value:''
		},{
			xtype:'editorgrid',	id:'gridEditor',enableColumnMove:false ,store: amortizaData,	clicksToEdit:1,	columLines:true,  hidden:false,
			stripeRows:true,	loadMask:true,	height:300,	width:690,	style:'margin:0 auto;',	frame:false,
			title:'<div align="center">Amortizaciones</div>',
			columns: [
				{
					header:'N�mero de <br>Amortizaci�n',	tooltip:'N�mero de Amortizaci�n', hideable:false,	dataIndex:'NO_AMORT',	sortable:false,	width:100,	align:'center',menuDisabled:true
				},{
					header:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	tooltip:'Fecha de Vencimiento:<br/>dd/mm/aaaa',	dataIndex:'FECHA_VENC',
					sortable:false,	width:180,	hidden:false, hideable:false,	align:'center',	menuDisabled:true,
					editor: {xtype:'datefield',	id:'fechaVenc', name:'fechaVenc_',	allowBlank:true,	startDay:0,	width:100,
								msgTarget:'side',	margins:'0 20 0 0'},	//,	minValue:
					renderer:function(value,metadata,registro, rowIndex, colIndex){
									return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
									//return NE.util.colorCampoEdit(value,metadata,registro);
								}
				},{
					header: 'Tipo de Amortizaci�n',	tooltip: 'Tipo de Amortizaci�n',	dataIndex: 'TIPO_AMORT',	sortable:false,	resizable:true,	width:160,	align:'center', menuDisabled:true,
					editor: {xtype : 'combo', id:0,	mode:'local',	displayField:'descripcion',	valueField:'clave',	forceSelection:true,	triggerAction:'all',
								typeAhead:true,	minChars:1,	store:tipoAmortData,	lazyInit:false,	lazyRender:true,	value:'0',
								listeners:{ //focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
												select:{ fn:function (comboField, record, index) {
																	comboField.fireEvent('blur');
																}
														 }
								}},
					renderer:function(value,metadata,registro, rowIndex, colIndex){
										if(!Ext.isEmpty(value)){
											var dato = tipoAmortData.findExact("clave", value);
											if (dato != -1 ) {
												var reg = tipoAmortData.getAt(dato);
												value = reg.get('descripcion');
											}
										}else{
												value = 'Seleccionar . . .';
										}
									return NE.util.colorCampoEdit(value,metadata,registro);
								}
				},{
					header:'Importe de la <br>Amortizaci�n',	tooltip:'Importe de la Amortizaci�n',	dataIndex:'IMPORTE',	sortable:false,	width:200,	hidden:false, hideable:false,	align:'right',menuDisabled:true,
					editor: {xtype:'numberfield', id:0, name:'montoAmort_',maxLength:11, minValue:0},
					renderer:function(value,metadata,registro, rowIndex, colIndex){
									return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00'),metadata,registro);
								}
				}
			],
			bbar:{
				items:[
					'->',{xtype: 'tbspacer', width: 5},'-',{xtype: 'tbspacer', width: 5},
					{
						xtype:'displayfield', value:'Importe Amortizaciones Capturadas: $&nbsp;'
					},{
						xtype:'textfield',	id:'impAmortCap', name:'impAmortCap_', width:'200', readOnly:true, value:0,	maskRe:/[0-9.]/,
						listeners:{
							afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
							focus:	function(field){field.fireEvent('blur');},
							change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
						}
					}
				]
			}
		},{
			xtype:'displayfield', value:''
		},{
			xtype: 'panel',layout:'table',	width:400,	border:true,	layoutConfig:{ columns:2 },	style:'margin:0 auto;',
			defaults: {frame:false, border: true,width:200, height: 30,bodyStyle:'padding:2px'},
			items:[
				{	html:'<div align="right">Importe del Documento:</div>'	},
				{	id:'importDocto',	html:'&nbsp;'	},
				{	html:'<div align="right">Importe del Descuento:</div>'	},
				{	id:'importDesc',	html:'&nbsp;'	},
				{	height:40,	html:'<div align="right">Importe Amortizaciones Recortadas:</div>'	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20, height: 40,
					items:[{xtype:'textfield', id:'importeAmorRec',	name:'importeAmorRec_',	readOnly:true, value:0, maskRe:/[0-9.]/,
								listeners:{
									afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
									change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
								}
							}]},
				{	height:40,	html:'<div align="right">Importe por Capturar</div>'	},
				{	bodyStyle:'padding:8px', layout:'form',labelWidth:20, height: 40,
					items:[{xtype:'textfield', id:'importeCapturar',	name:'importeCapturar_',	readOnly:true, value:0,	maskRe:/[0-9.]/,
								listeners:{
									afterrender: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  },
									change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
								}
					}]},
				{	height:40,	id:'mensajeAmort',	width:400,	colspan:2,	html:'&nbsp;'	}
			]
		}
	];
	
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',		
      frame:     	false,
      border:    	false,
		hidden: (strEsquemaExtJS=='true')?true:false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Individual</h1></b>',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '26forma02ext.jsp';					
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Masiva',
				id: 'btnCons2',					
				handler: function() {
					window.location = '26forma03ext.jsp';					
				}
			}
		]
	};

	var fpIndividual_a = new Ext.form.FormPanel({
		id: 'formaIndividual_a',
		width: 700,
		heigth:'auto',
		style: 'margin:0 auto;',
		frame: false,
		border:false,
		hidden:true,
		fileUpload:true,
		bodyStyle: 'padding: 4px',
		labelWidth: 150,
		items: elementosIndividual_a,
		buttonAlign: 'center',
		buttons:[
			{
				text:'Aceptar',	id:'btnAceptar',	iconCls:'aceptar',	//formBind:true,
				handler: function(boton, evento) {
								enviar_aceptar(valInicio.plazos);
				} //fin handler
			},{
				text:'Cancelar',	id:'btnCancelar_a',	iconCls:'borrar',	handler:function() {fpIndividual.show(); fpIndividual_a.hide();}
			}
		]
	});

	Ext.getCmp('gridEditor').on('afteredit', function(e){
		var col = Ext.getCmp('gridEditor').getColumnModel().findColumnIndex(e.field);
		if (e.field == 'FECHA_VENC') {
			detTipoAmortXFecha(e.row);
			if (!validaPlazo_b(e.value)){
				return;
			}
		}
		if (e.field == 'TIPO_AMORT') {
			detTipoAmort(e.row);
		}
		if (e.field == 'IMPORTE') {
			if(parseFloat(e.value)<0){
				Ext.Msg.alert("Mensaje de p�gina web.","El valor del monto capturado no puede ser menor que cero",
				function(){
					var reg = amortizaData.data.items;
					reg[e.row].data.IMPORTE = "";
					amortizaData.commitChanges();
					Ext.getCmp('gridEditor').startEditing(e.row, col);
					return;
				});
				return;
			}
			calculaMontos();
		}
	});

	var elementosIndividual = [
		{
			xtype:'panel',	id:'mensajeBloqueo',	style:'margin:0 auto;',	frame:false,	border:false,	html:''
		},{
			xtype:'hidden',	id:'tipoPlazo', name:'TipoPlazo',	value:''
//		},{
		},{
			xtype:'panel',
			id:	'formInd',
			frame: false,
			//style: ' margin:0 auto;',
			bodyStyle: 'padding: 6px',
			border:false,
			defaults: {	msgTarget: 'side',layout:'hbox'},	//, style: 'margin:0 auto;'
			//monitorValid: true,
			items:[
				{
					xtype:'panel', defaults: {layout:'form',width:840,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							id:'radioGrupoPlazo',	defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
							{
								xtype:'radiogroup',	id:'radioGp',	anchor:'50%',	style:'margin:0 auto;',	columns:2,
								items:[
									{boxLabel: 'Factoraje', name: 'TipoPlazoAux', inputValue: 'F', checked: false,
										listeners:{
											check:	function(radio){
															recargar(0);
															if (radio.checked){
																Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Documentos Descontados para Operaciones de <br>Factoraje Electr&oacute;nico<br></div>');
																Ext.getCmp('tipoPlazo').setValue('F');
																Ext.getCmp('tipoCredito').setValue("");
																Ext.getCmp('tipoCredito').store.removeAll();
																Ext.getCmp('tipoCredito').store.reload({params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue()}});
																Ext.getCmp('DestinoCredito1').allowBlank = true;
																Ext.getCmp('DestinoCredito1').hide();
															}
															actualizaCombos();
														}
										}
									},{boxLabel: 'Cr�dito', name: 'TipoPlazoAux', inputValue: 'C',checked: false,
										listeners:{
											check:	function(radio){
															recargar(0);
															if (radio.checked){
																Ext.getCmp('msjTipoPlazoRes').body.update('<div class="titulos" align="center">Formato de Constancia de Dep&oacute;sito de Operaciones <br>Cr&eacute;dito Electr&oacute;nico<br></div>');
																Ext.getCmp('tipoCredito').setValue("");
																Ext.getCmp('tipoCredito').store.removeAll();
																Ext.getCmp('tipoPlazo').setValue('C');
																Ext.getCmp('tipoCredito').store.reload({params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue()}});
																Ext.getCmp('DestinoCredito1').allowBlank = false;
																Ext.getCmp('DestinoCredito1').show();
															}
															actualizaCombos();
														}
										}
									}
								]
							}
							]
						}
					]
				},
				{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							id:'panIno',	defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{	xtype:'textfield',fieldLabel:'* No. Cliente Sirac',name:'NoClienteSIRAC',width:250,maxLength:12,anchor:'70%', maskRe:/[0-9]/,allowBlank:false,
									listeners:{
										change:function(field){
												if (	!Ext.isEmpty(field.getValue()) ){
													fpIndividual.el.mask('Enviando...', 'x-mask-loading');
													Ext.Ajax.request({
														url: '26forma02ext.data.jsp',
														params: {informacion:"obtieneNombre", clienteSirac: field.getValue()},
														callback: procesaObtieneNombre
													});
												}
										}
									}
							}]
						},{
							id:'panEspacio', defaults: {	msgTarget: 'side',anchor:'-20'}
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'textfield',	id:'nombre',	fieldLabel:'Nombre',	name:'Nombre',	maxLength:100, regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{	xtype:'numberfield',	fieldLabel:'No. Sucursal Banco', name:'NoSucBanco',	maxLength:10,	width:250,	anchor:'70%'	}]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	name:'Moneda',	hiddenName:'Moneda',	fieldLabel:'* Moneda',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, allowBlank:false,
									triggerAction : 'all',	typeAhead: true,	minChars : 1,	store: catalogoMonedaData,	tpl : NE.util.templateMensajeCargaCombo
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{xtype:'numberfield',	name:'NoDocumento',	fieldLabel:'* No. de Documento', maxLength:10,	width:250,	anchor:'70%', allowBlank:false}]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},	//labelWidth:160,
							items:[{	xtype:'textfield',fieldLabel:'* Importe de Documento $',id:'importeDocto',name:'importeDocto_',maxLength:22,maskRe:/[0-9.]/,allowBlank:false,
										vtype:'rangoValor',	campoInicioValor:'importeDesc',	vtypeText:'El Importe de Descuento no puede ser mayor que el importe Documento.',
										listeners:{
											change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
										}
							}]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},	//labelWidth:160,	
							items:[
								{	xtype:'textfield',id:'importeDesc',	fieldLabel:'* Importe de Descuento $',name:'ImporteDscto',maxLength:22,vtype:'rangoValor',allowBlank:false,
									vtypeText:'El Importe de Descuento no puede ser mayor que el importe Documento.',	maskRe:/[0-9.]/,	campoFinValor:'importeDocto',
										listeners:{
											change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
											/*change:function(field){
												
													if (	field.getValue() > Ext.getCmp('importeDocto').getValue() ){
														field.markInvalid("El Importe de Descuento no puede ser mayor que el importe Documento.");
														field.focus();
														return;
													}
											}*/
										}
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget:'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'tipoCredito',	name:'TipoCredito_',	hiddenName:'TipoCredito_',	fieldLabel:	'* Tipo de Cr�dito',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185,	allowBlank:false,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoTipoCredito,	tpl:NE.util.templateMensajeCargaCombo,
									listeners:{
										select:function(cbo){
											if( !Ext.isEmpty(cbo.getValue()) ){
												recargar(2);
												actualizaCombos();
												Ext.getCmp('emisor').store.removeAll();
												Ext.getCmp('emisor').setValue("");
												Ext.getCmp('emisor').store.reload( {params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue(), TipoCredito:cbo.getValue()}} );
											}
										}
									}
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'emisor',	name:'Emisor_',	hiddenName:'Emisor_',	fieldLabel:	'Nombre del Emisor',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoNombreEmisor,	tpl:NE.util.templateMensajeCargaCombo,
									listeners:{
										select:function(cbo){
											if( !Ext.isEmpty(cbo.getValue()) ){
												recargar(2);
												Ext.getCmp('tasaBaseI').store.removeAll();
												Ext.getCmp('tasaBaseI').setValue("");
												Ext.getCmp('tasaBaseI').store.reload( {params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue(), 
																												TipoCredito:Ext.getCmp('tipoCredito').getValue(), emisor: cbo.getValue()}} );
												actualizaCombos();
											}
										},
										change:function(cbo){
												//actualizaCombos();
										}
									}
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},	//labelWidth:130, 
							items:[
								{
									xtype: 'compositefield',	fieldLabel:'Periodicidad de Pago',	combineErrors: false,	msgTarget: 'side',
									items: [
										{
											xtype: 'displayfield',	value:'* Capital:',	width:55
										},{
											xtype:'combo',	id:'capital',	name:'PPCapital',	hiddenName:'PPCapital',	emptyText:'Seleccionar . . .',
											mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:165,	allowBlank:false,
											triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoCapital,	tpl:NE.util.templateMensajeCargaCombo
										}
									]
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'intereses',	name:'PPIntereses',	hiddenName:'PPIntereses',	fieldLabel:	'* Intereses',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185,	allowBlank:false,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoIntereses,	tpl:NE.util.templateMensajeCargaCombo
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:	'textfield',	id:'bienesyServ',	fieldLabel:	'* Descripci�n de Bienes y Servicios',	name:'BienesServicios',	maxLength:60, allowBlank:false,
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'claseDocto',	name:'ClaseDocto_',	hiddenName:'ClaseDocto_',	fieldLabel:	'* Clase de Documento',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185, allowBlank:false,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoDocumento,	tpl:NE.util.templateMensajeCargaCombo
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:820,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'textfield',	fieldLabel:'* Domicilio de Pago',	name:'DomicilioPago',	maxLength:60, allowBlank:false, anchor:'97%',
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:820,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'textfield',	fieldLabel:'* Destino del Cr�dito',	name:'DestinoCredito', id:'DestinoCredito1',	maxLength:400, allowBlank:false, anchor:'97%', hidden: true,
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',border:false,frame:false,width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							title:'Usuario Final',	defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'tasaUF',	name:'TasaUF_',	hiddenName:'TasaUF_',	fieldLabel:	'Tasa Base',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoTasa,	tpl:NE.util.templateMensajeCargaCombo
								}
							]
						},{
							title:'&nbsp;'
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'relMatUF',	name:'RelMatUF_',	hiddenName:'RelMatUF_',	fieldLabel:	'Relaci�n Matem�tica',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185, lazyRender:true,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:relMatData//,	tpl:NE.util.templateMensajeCargaCombo
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'textfield',	fieldLabel:	'Sobretasa',	id:'sobreTasaUF',	name:'SobreTasaUF_',	maxLength:7, anchor:'70%',maskRe:/[0-9.]/,
									regex: /^[\d]{1,2}[.]{1}[\d]{1,4}?$/, regexText:'El n�mero es muy grande.\nPor favor escriba un n�mero menor o igual a 2 enteros y 4 decimales.',
									listeners:{
										change: function(field){
													var dato = Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0.0000');
													field.setValue(dato.substring(0,7));
										}
									}
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',border:false,frame:false,width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							title:'Intermediario Financiero',defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'tasaBaseI',	name:'TasaI',	hiddenName:'TasaI',	fieldLabel:	'* Tasa Base',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185, allowBlank:false,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoTasaBaseI,	tpl:NE.util.templateMensajeCargaCombo,
									listeners:{
										select:function(cbo){
											if( !Ext.isEmpty(cbo.getValue()) ){
												recargar(2);
												actualizaCombos();
												Ext.getCmp('Amortizacion').store.removeAll();
												Ext.getCmp('Amortizacion').setValue("");
												Ext.getCmp('Amortizacion').store.reload( {params:{TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue(), 
																												TipoCredito:Ext.getCmp('tipoCredito').getValue(), emisor: Ext.getCmp('emisor').getValue(),
																												tasaI:cbo.getValue()}} );
											}
										}
									}
								}
							]
						},{
							title:'&nbsp;'
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',border:false,frame:false,width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							title:'Datos Complementarios del Prestamo:',defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'numberfield',fieldLabel:'*N�mero de Amortizaciones',id:'noAmortizaciones',name:'NoAmortizaciones_',maxLength:3,anchor:'70%',allowBlank:false
								}
							]
						},{
							title:'&nbsp;',	defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'radiogroup',	id:'radioGpAmortiza',	fieldLabel:'Tipo de Renta',	anchor:'70%',	columns:2,	hidden:true,
									items:[
										{boxLabel:'Si', id:'radioSiAmortiza',	name:'TipoRenta_', inputValue:'S', checked:false,
											listeners:{
												check:	function(radio){
																if (radio.checked){
																	Ext.getCmp('tipoRenta').setValue("S");
																}
																muestraOcultaDatosComplementarios();
															}
											}
										},{boxLabel:'No',	id:'radioNoAmortiza',	name:'TipoRenta_', inputValue:'N',checked:false,
											listeners:{
												check:	function(radio){
																if (radio.checked){
																	Ext.getCmp('tipoRenta').setValue("N");
																}
																muestraOcultaDatosComplementarios();
															}
											}
										}
									]
								},{
									xtype:'hidden', name:'TipoRenta',	id:'tipoRenta', value:""
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'Amortizacion',	name:'Amortizacion_',	hiddenName:'Amortizacion_',	fieldLabel:	'* Tabla de Amortizaci�n',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185, allowBlank:false,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:catalogoAmortizacion,	tpl:NE.util.templateMensajeCargaCombo,
									listeners:{
										select:function(cbo){
											if( !Ext.isEmpty(cbo.getValue()) ){
												actualizaCombos();
												if (!Ext.isEmpty(Ext.getCmp('Amortizacion')) ) {
													if (Ext.getCmp('Amortizacion').getValue() == "5" && Ext.getCmp('tipoRenta').getValue()=="N"){
														
													}
												}
												if(!Ext.isEmpty(Ext.getCmp('tipoPlazo'))&&!Ext.isEmpty(Ext.getCmp('tipoCredito'))&&!Ext.isEmpty(Ext.getCmp('emisor'))&&
													!Ext.isEmpty(Ext.getCmp('tasaBaseI'))&&!Ext.isEmpty(Ext.getCmp('Amortizacion'))){
													pnl.el.mask('Enviando...', 'x-mask-loading');
													Ext.Ajax.request({
														url: '26forma02ext.data.jsp',
														params: {informacion:"plazosBaseOperacion", TipoCartera:valInicio.tipoCartera,TipoPlazo:Ext.getCmp('tipoPlazo').getValue(),
																	TipoCredito:Ext.getCmp('tipoCredito').getValue(), emisor:Ext.getCmp('emisor').getValue(), Amortiza:cbo.getValue(),
																	tasaI:Ext.getCmp('tasaBaseI').getValue(),ppcapital:Ext.getCmp('capital').getValue(), tipoRenta:Ext.getCmp('tipoRenta').getValue()},
														callback: procesaPlazosBaseOperacion
													});
												}
											}
										}
									}
								}
							]
						},{
							id:'msgFechaMax',	hidden:false,	html:'&nbsp;'
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{	xtype:'numberfield',	id:'codigoBase',	fieldLabel:'C�digo Base de Operaci�n Din�mica',	name:'codigoBaseOperacionDinamica',	
									width:250,	maxLength:6,	anchor:'70%',	hidden:true,	enableKeyEvents:true,
									listeners:{
										blur:	function(field){
													desHabilitaCampo(field);
										},
										keyup:function(field){
													desHabilitaCampo(field);
										}
									}
							}]
						},{
							id:'panEspacio', defaults: {	msgTarget: 'side',anchor:'-20'}
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'datefield',id:'fechaPPC',	fieldLabel:'* Fecha de primer pago de capital (dd/mm/aaaa)',	name:'FechaPPC_', 
									startDay: 0, width:100, anchor:'70%',	hidden:true,	enableKeyEvents:true,	//allowBlank:false,	
									listeners:{
										blur:	function(field){
													desHabilitaCampo(field);
													diaPago();
													validaPlazo(field);
										},
										keyup:function(field){
													desHabilitaCampo(field);
										}
									}
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{	xtype:'textfield',	fieldLabel:'* D�a de pago',	id:'diaPago',	name:'DiaPago_',	//allowBlank:false,
										maxLength:2,	width:50,	anchor:'55%',	hidden:true,enableKeyEvents:true, regex:/^[0-9]*$/,
									listeners:{
										blur:	function(field){
													desHabilitaCampo(field);
										},
										keyup:function(field){
													desHabilitaCampo(field);
										}
									}
							}]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							id:'prueba',	defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'datefield',id:'fechaPPI',	fieldLabel:'* Fecha de primer pago de interes (dd/mm/aaaa)',	name:'FechaPPI_', 
									startDay: 0, width:100, anchor:'70%',	hidden:true,	enableKeyEvents:true,	//allowBlank:false,	
									listeners:{
										blur:	function(field){
													desHabilitaCampo(field);
													validaPlazo(field);
													menorHoy(field);
										},
										keyup:function(field){
													desHabilitaCampo(field);
										}
									}
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{
									xtype: 'compositefield',	fieldLabel:'', id:'fechaVencDocto',
									combineErrors: false,	msgTarget: 'side', anchor:'100%',	hidden:true,
									items: [
										{
											xtype:'datefield',	fieldLabel:'Fecha de vencimiento del docto. (dd/mm/aaaa)',	name:'FechaVencDocto_', 
											startDay: 0, width:100, msgTarget:'side',margins: '0 20 0 0',
											listeners:{
												blur:	function(field){
															menorHoy(field);
												}
											}
										},{
											xtype:'displayfield',	value: '(hasta 20 a�os)', width: 150
										}
									]
							}]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{
									xtype:'datefield',	id:'fechaVencDscto',	fieldLabel:'* Fecha de vencimiento del descuento (dd/mm/aaaa)',	name:'FechaVencDscto_', 
									startDay: 0, width:100,anchor:'70%',		hidden:true,	enableKeyEvents:true,	//allowBlank:false,
									listeners:{
										blur:	function(field){
													desHabilitaCampo(field);
													menorHoy(field);
													validaPlazo(field);
										},
										keyup:function(field){
													desHabilitaCampo(field);
										}
									}
							}]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'datefield',	id:'fechaEmisionTC',	fieldLabel:'* Fecha de emisi�n del t�tulo de cr�dito (dd/mm/aaaa)',	name:'FechaEmisionTC_', startDay: 0, width:100, anchor:'70%',	hidden:true,allowBlank:false
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget:'side',anchor:'-20'},
							items:[
								{
									xtype:'textfield', maxLength:50, id:'lugarFirma',	name:'LugarFirma_', fieldLabel:'* Lugar de Firma',	hidden:true,	allowBlank:false,
									regexText:"El campo no puede contener ninguno de los siguientes caracteres: <br><br>                    ' \ * � % # + � ~ [ ] { } � ? � ! ",
									regex: /^[a-zA-Z0-9������������;,&\.\(\)\-\/\s]*$/
								}
							]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'combo',	id:'amortizacionAjuste',	name:'AmortizacionAjuste_',	hiddenName:'AmortizacionAjuste_',	fieldLabel:	'Amortizaci�n de ajuste',	emptyText:'Seleccionar . . .',
									mode: 'local',	valueField:'clave',	displayField:'descripcion',	forceSelection:false, width:185, lazyRender:true,
									triggerAction:'all',	typeAhead:true,	minChars:1,	store:amortAjusteData,	hidden:true
								}
							]
						}
					]
				},{
					xtype:'panel',	defaults: {xtype:'panel',layout:'form',width:420,	bodyStyle:'padding:5px 5px 0'},
					items:[
						{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[{	xtype:'textfield',	fieldLabel:'Importe amortizaci�n de ajuste $',	id:'imporAmortAjuste',	name:'ImporAmortAjuste_',	maxLength:21,	hidden:true, maskRe:/[0-9.]/,
								listeners:{
									change: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '000.00'));  }
								}
							}]
						},{
							defaults: {	msgTarget: 'side',anchor:'-20'},
							items:[
								{
									xtype:'datefield',	id:'fechaAmortAjuste',	fieldLabel:'Fecha amortizaci�n de ajuste (dd/mm/aaaa)',	name:'FechaAmortAjuste_', startDay: 0, width:100,anchor:'70%',	hidden:true
								}
							]
						}
					]
				}
			]
		}
	];

	var fpIndividual = new Ext.form.FormPanel({
		id: 'formaIndividual',
		width: 935,
		heigth:'auto',
		style: 'margin:0 auto;',
		//frame: false,
		frame: true,
		border:true,
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosIndividual,
		monitorValid: false,
		buttonAlign: 'center',
		buttons:[
			{
				text:'Continuar',	id:'btnContinuar',	iconCls:'icoContinuar',	hidden:true,	//formBind:true,	
				handler: function(boton, evento) {
								enviar(valInicio.plazos);
				} //fin handler
			},{
				text:'Cancelar',	id:'btnCancelar',	iconCls:'borrar',	hidden:true,	handler:function() {window.location = '26forma02ext.jsp';}
			}
		],
		tbar:[
			'->',
			{
				text:'Asistencia en la Operaci�n', iconCls:'icoAyuda',
				handler: function(boton, evento) {
								var ventana = Ext.getCmp('winAsistencia');
								if (ventana) {
									ventana.destroy();
								}
								pnl.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '26forma02ext.data.jsp',
									params: {informacion: "Asistencia", TipoCartera:valInicio.tipoCartera},
									callback: procesaAsistencia
								});
				} //fin handler
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			NE.util.getEspaciador(10),
			fpBotones,
			fpIndividual,fpIndividual_a,fpIndividual_b,fpIndividual_c,
			NE.util.getEspaciador(10)
		]
	});
	
	pnl.el.mask('Enviando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '26forma02ext.data.jsp',
		params: {informacion: "valoresIniciales"},
		callback: procesaValoresIniciales
	});

});