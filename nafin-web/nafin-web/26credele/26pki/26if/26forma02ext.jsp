<!DOCTYPE html>
<%@ page import= " java.text.*, java.util.* " contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/26credele/26secsession.jspf"%>
<%@ include file="../certificado.jspf" %>

<html>
<head>
<title>.:: N@fin Electrónico :: Financiamiento a Distribuidores ::.</title>

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%@ include file="/00utils/componente_firma.jspf" %>

<script type="text/javascript" src="26forma02ext.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.js"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>
<%if(esEsquemaExtJS){%>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
<%}%>
	<form id='formAux' name="formAux" target='_new'></form>
	<input type="hidden" id="hidStrEsquemaExtJS" value="<%=esEsquemaExtJS%>">

</body>
</html>