var showPanelLayoutAyuda;

Ext.onReady(function() {

/******************************************************************************
 *	Regresa la pantalla a su condici�n inicial											*
 ******************************************************************************/	
	function cancelar_regresar(){
		Ext.getCmp('forma').getForm().reset();	
		Ext.getCmp('formaGrid').getForm().reset();
		Ext.getCmp('formaGrid').hide();
		Ext.getCmp('formaRegistro').hide();
		Ext.getCmp('formaCambioEstatus').hide();
		Ext.getCmp('gridRegistro').hide();
		Ext.getCmp('forma').show();
		Ext.getCmp('btnConfirmar').show();
		Ext.getCmp('btnImprimir').hide();
		
	}

/******************************************************************************
 * Descarga archivo PDF																			*
 ******************************************************************************/
	function descargaArchivo() {
		var archivo = Ext.getCmp('file_name1').getValue();				
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};	
		Ext.getCmp('btnImprimir').setIconClass('icoPdf');
		fc.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		fc.getForm().getEl().dom.submit();

	}	

/******************************************************************************
 *	Procesos para obtener la ventana de ayuda											   *
 ******************************************************************************/
	var procesaVentanaAyuda = function(opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if(!Ext.isEmpty(resp.tablaHtml)){
				Ext.getCmp("labelLayoutAyuda").setText(resp.tablaHtml,false);
			}			
		}else{
			//NE.util.mostrarConnError(response,opts);
			alert('Error');
		}
	}
	
	var ventanaAyuda = function(respuesta ){
		Ext.Ajax.request({
			url:'26forma03ext.data.jsp',
			params: { informacion:'ventanaAyuda' },
			callback: procesaVentanaAyuda
		});	
	}
	
/******************************************************************************
 *	Muestran y ocultan la ventana de ayuda		    									   *
 ******************************************************************************/
	showPanelLayoutAyuda = function(){
		Ext.getCmp('panelLayoutAyuda').show();
	}
	
	var hidePanelLayoutAyuda = function(){
		Ext.getCmp('panelLayoutAyuda').hide();
	}
	
/******************************************************************************
 *	Sube el archivo para ser procesado													   *
 ******************************************************************************/
	function subeArchivo(){
		Ext.getCmp('forma').getForm().submit({
			clientValidation: true,
			url: '26forma03ext.data.jsp?informacion=SubirArchivo',
			waitMsg: 'Cargando archivo...',
			waitTitle: 'Por favor espere',
			success: function(form, action) {
				 procesaArchivo(null,  true,  action.response );
			},
			failure: function(form, action) {
			 	 action.response.status = 200;
				 Ext.MessageBox.alert('Error...', 'Error al subir el archivo. Por favor int�ntelo de nuevo');
			}
		});	
	}

/******************************************************************************
 *	Valida el archivo que se subi� previamente										   *
 ******************************************************************************/
	var procesaArchivo = function(opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('nombre_archivo1').setValue(resp.NOMBRE_ARCHIVO);
			//valido el archivo, lo hago mandando a crear el grid
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'ValidaArchivo',
					nombre_archivo: Ext.getCmp('nombre_archivo1').getValue()
				})
			});
		}else{
			Ext.MessageBox.alert('Error...', 'Error al subir el archivo. Por favor int�ntelo de nuevo');
			Ext.getCmp('forma').getForm().reset();
		}
	}

/******************************************************************************
 *	Muestra el resultado del cambio de estatus de las solicitudes procesadas   *
 ******************************************************************************/
	var procesaCambioEstatus = function(store, records, response){
		var jsonData = Ext.util.JSON.decode(response.responseText);
		if (jsonData != null){
			if(jsonData.success == true){
				
				Ext.getCmp('forma').hide();
				Ext.getCmp('formaRegistro').hide();
				Ext.getCmp('formaCambioEstatus').show();
				Ext.getCmp('btnConfirmar').hide();
				Ext.getCmp('btnImprimir').show();
				
				Ext.getCmp('acuse1').setValue(jsonData.acuse);
				Ext.getCmp('solicitudes_procesadas1').setValue(jsonData.solicProcesadas);
				Ext.getCmp('fecha_proceso1').setValue(jsonData.fecha);
				Ext.getCmp('hora_proceso1').setValue(jsonData.hora);
				Ext.getCmp('usuario1').setValue(jsonData.iNoUsuario);
				Ext.getCmp('file_name1').setValue(jsonData.fileName);
			} else {
				Ext.MessageBox.alert('Error...', 'Error realizar el cambio de estatus. Por favor int�ntelo de nuevo');
			}
		} 
  }; 
	
/************************************************************************************************************
 *	Procesa las solicitudes correctas. La funci�n obtiene los valores del grid de solicitudes sin errores		*
 * y los env�a en formato JSON para ser actualizados y llenar el gridRegistros										*
 ************************************************************************************************************/
	function getJsonOfStore(store, info){
		var datar = new Array();
      var jsonDataEncode = "";
		var records = store.getRange();
      for (var i = 0; i < records.length; i++) {
			datar.push(records[i].data);
      }
		jsonDataEncode = Ext.util.JSON.encode(datar);
		if(info == 'ProcesaCambios'){
			consultaRegistro.load({
				params: Ext.apply({ 
					informacion: 'ProcesaCambios',
					datos: jsonDataEncode
				})
			});	
		} else if(info == 'CambioEstatus'){
			Ext.Ajax.request({
				url: '26forma03ext.data.jsp',
				params: Ext.apply({
					informacion: 'CambioEstatus',	
					datos: jsonDataEncode
				}),							
				callback: procesaCambioEstatus
			});		
		}
      
	}	

/******************************************************************************
 *	C�digo para generar el Grid de Solicitudes procesadas SIN ERRORES   		   *
 ******************************************************************************/
 	var procesarConsultaDataSE = function(store, arrRegistros, opts) 	{
		var fg = Ext.getCmp('formaGrid');
		fg.el.unmask();				
		var gridConsultaSE = Ext.getCmp('gridConsultaSE');	
		var el = gridConsultaSE.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
		}
	}	

	
	//---------- Se crea el store del grid  de solicitudes sin errorres -------
	var consultaDataSE = new Ext.data.JsonStore({
		root : 'registros',
		url : '26forma03ext.data.jsp',
		baseParams: {
			informacion: 'ValidaArchivoSE'		
		},
		fields: [
			{name: 'LINEA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataSE,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataSE(null, null, null);					
				}
			}
		}					
	});

	//---------- Se crea el grid de solicitudes sin errores----------
	var gridConsultaSE = new Ext.grid.EditorGridPanel({	
		store: consultaDataSE,
		id: 'gridConsultaSE',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: false,
		columns: [	
			{
				header: "Linea",
				dataIndex: 'LINEA', 
				width: 400
			}
		],	
		displayInfo: true,		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 800,
		align: 'center',
		frame: false
	});	


	
/******************************************************************************
 *	C�digo para generar el Grid de Solicitudes procesadas CON ERRORES   		   *
 ******************************************************************************/
 	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();				
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			//Se llena el GRID sin errores
			consultaDataSE.load({
				params: Ext.apply({ 
					informacion: 'ValidaArchivoSE',
					nombre_archivo: Ext.getCmp('nombre_archivo1').getValue()
				})
			});		
			var jsonData = store.reader.jsonData;
			Ext.getCmp('formaGrid').show();
			if(store.getTotalCount() > 0 || jsonData.total_sin_errores > 0) {
				if(jsonData.total_sin_errores > 0){					
					Ext.getCmp('btnProcesar').enable();	
					Ext.getCmp('btnRegresar').disable();
				} else {
					Ext.getCmp('btnProcesar').disable();
					Ext.getCmp('btnRegresar').enable();
				}
				if(jsonData.total_con_errores < 1){
					el.mask('No se encontraron errores en las solicitudes', 'x-mask');
				}
				Ext.getCmp('total_sin_errores1').setValue(jsonData.total_sin_errores);
				Ext.getCmp('total_con_errores1').setValue(jsonData.total_con_errores);
			} else {	
				Ext.getCmp('btnProcesar').disable();
				Ext.getCmp('btnRegresar').enable();
			}
		}
	}	

	
	//---------- Se crea el store del grid de solicitudes con errores ----------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '26forma03ext.data.jsp',
		baseParams: {
			informacion: 'ValidaArchivo'		
		},
		fields: [
			{name: 'LINEA_CON_ERROR'},
			{name: 'ERROR'},
			{name: 'SOLUCION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}					
	});

	//---------- Se crea el grid  de solicitudes con errores----------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		title: 'Solicitudes con error',
		hidden: false,
		columns: [	
			{
				header: "L�nea",
				dataIndex: 'LINEA_CON_ERROR',
				resizable: true,
				align: 'center',
				width: 119
			},{
				header: "Error",
				dataIndex: 'ERROR',
				resizable: true,
				width: 310
			},{
				header: "Soluci�n",
				dataIndex: 'SOLUCION',
				resizable: true,
				width: 339
			}
		],	
		displayInfo: true,		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 800,
		align: 'center',
		frame: false
	});	


/******************************************************************************
 *	C�digo para generar el Grid de Registro de solicitudes por procesar		   *
 ******************************************************************************/
 	var procesarConsultaRegistro = function(store, arrRegistros, opts) 	{
		var fg = Ext.getCmp('formaGrid');
		fg.el.unmask();				
		var gridRegistro = Ext.getCmp('gridRegistro');	
		var el = gridRegistro.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0) {
				Ext.getCmp('formaGrid').hide();
				Ext.getCmp('formaRegistro').show();
				Ext.getCmp('gridRegistro').show();
				Ext.getCmp('total1').setValue(jsonData.total);
			}
		}
	}	

	//---------- Se crea el store del grid de solicitudes por procesar----------
	var consultaRegistro = new Ext.data.JsonStore({
		root : 'registros',
		url : '26forma03ext.data.jsp',
		baseParams: {
			informacion: 'ProcesaCambios'		
		},
		fields: [
			{name: 'FOLIO'},
			{name: 'PRESTAMO'},
			{name: 'FECHA_OPERACION'},
			{name: 'VECTOR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaRegistro,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistro(null, null, null);					
				}
			}
		}					
	});

	//---------- Se crea el grid Registro----------
	var gridRegistro = new Ext.grid.EditorGridPanel({	
		store: consultaRegistro,
		id: 'gridRegistro',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: "N�mero de Folio",
				dataIndex: 'FOLIO',
				resizable: true,
				width: 199,
				align: 'center'
				
			},{
				header: "N�mero de Pr�stamo",
				dataIndex: 'PRESTAMO',
				resizable: true,
				width: 199,
				align: 'center'
			},{
				header: "Fecha de Operaci�n",
				dataIndex: 'FECHA_OPERACION',
				resizable: true,
				width: 199,
				align: 'center'
			},{
				header: "Campo oculto",
				dataIndex: 'VECTOR',
				resizable: true,
				hidden: true
			}
		],	
		displayInfo: true,		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 600,
		align: 'center',
		frame: false,
		bbar: {
			buttonAlign: 'right',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir archivo PDF ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					hidden: true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						descargaArchivo();
					}
				},{
					xtype: 'button',
					text: 'Confirmar cambio de estatus',
					iconCls: 'icoAceptar',
					id: 'btnConfirmar',
					hidden: false,
					handler: function(boton, evento) {
						getJsonOfStore(consultaDataSE, 'CambioEstatus');				
					}
				},'-',{
					xtype: 'button',
					text: 'Salir',
					iconCls: 'icoRegresar',
					id: 'btnSalir',
					tooltip: 'Regresa a la pantalla inicial',
					handler: function(boton, evento) {
						cancelar_regresar();
					}
				}				
			]
		}
	});

/******************************************************************************
 *	Elementos para mostrar la ventana de ayuda    									   *
 ******************************************************************************/	
	var elementosLayoutAyuda = [
		{
			xtype: 'label',
			id: 'labelLayoutAyuda',
			name: 'labelLayoutAyuda',
			html: "" ,
			cls: 'x-form-item',
			style: {
				width: '100%',
				marginBottom: '10px', 
				textAlign: 'center',
				color: '#ff0000'
			}
		}
	];

/******************************************************************************
 *	Elementos para mostrar los resultados de la validaci�n del archivo	  	   *
 ******************************************************************************/
	var elementosPanel = [
		{			
			xtype:'panel',  
			id: 'panel_gridConsulta',  
			hidden: false,
			border: false,	
			layout: 'table',	
			width: 800,	
			layoutConfig: { columns: 1 },
			items: gridConsulta
		},{			
			xtype:'panel',  
			id: 'panel_gridConsultaSE',  
			hidden: true,
			border: false,	
			layout: 'table',	
			width: 800,	
			layoutConfig: { columns: 1 },
			items: gridConsultaSE
		},{			
			xtype: 'panel',  
			id: 'pane1_totales',  
			hidden: false,	
			layout: 'table',	
			width: 800,	
			layoutConfig: {columns: 8},
			items: [
				{	width: 150,	
					frame: false,	
					border: false,	
					html: '<div class="formas" align="right"><br><b> Total Solicitudes sin errores:</b> <br>&nbsp;</div>'	
				},{	
					width: 80,	
					xtype: 'textfield',			
					id: 'total_sin_errores1',	
					name: 'total_sin_errores',
					readOnly: true
				},{	
					width: 100,	
					frame: false,	
					border: false,	
					html: '<div class="formas" align="right"><br> Total con errores: <br>&nbsp;</div>'	
				},{	
					width: 80,	
					xtype: 'textfield',			
					id: 'total_con_errores1',	
					name: 'total_con_errores',
					readOnly: true
				},{	
					width: 170,	
					frame: false,	
					border: false,	
					html: '<div class="formas" align="right"><br> &nbsp;</div>'	
				},{
					xtype: 'button',
					text: 'Procesar',
					iconCls: 'icoAceptar',
					id: 'btnProcesar',
					handler: function(boton, evento) {
						//Obtengo las solicitudes correctas del grid y las convierto a JSON
						getJsonOfStore(consultaDataSE, 'ProcesaCambios');				
					}
				},{	
					width: 10,	
					frame: false,	
					border: false,	
					html: '<div class="formas" align="right"><br> &nbsp;</div>'	
				},{
					xtype: 'button',
					text: 'Regresar',
					iconCls: 'icoRegresar',
					id: 'btnRegresar',
					//tooltip: 'Confirmar cambio de estatus',
					handler: function(boton, evento) {
						cancelar_regresar();			
					}
				}
			]
		}
	];	

/******************************************************************************
 *	Elementos panel de Registro de solicitudes procesadas	  						   *
 ******************************************************************************/
	var elementosRegistro = [
		{			
			xtype: 'panel',  
			id: 'pane1_registro',  
			hidden: false,	
			layout: 'table',	
			width: 600,	
			layoutConfig: {columns: 2},
			items: [
				{	width: 300,	
					frame: false,	
					border: false,	
					html: '<div class="formas" align="right"><br> N�mero de Solicitudes Procesadas: <br>&nbsp;</div>'	
				},{	
					width: 150,	
					xtype: 'textfield',			
					id: 'total1',	
					name: 'total',
					readOnly: true
				}
			]
		}
	];	

/******************************************************************************
 *	Elementos operaciones que sufrieron cambio de estatus							   *
 ******************************************************************************/
	var elementosCambioEstatus = [
		{	
			width: 300,	
			fieldLabel: 'No. Acuse',			
			id: 'acuse1',	
			name: 'acuse',
			readOnly: true
		},{	
			width: 300,	
			fieldLabel: 'No. de Solicitudes Procesadas',			
			id: 'solicitudes_procesadas1',	
			name: 'solicitudes_procesadas',
			readOnly: true					
		},{	
			width: 300,	
			fieldLabel: 'Fecha de Proceso',		
			id: 'fecha_proceso1',	
			name: 'fecha_proceso',
			readOnly: true					
		},{	
			width: 300,	
			fieldLabel: 'Hora de Proceso',		
			id: 'hora_proceso1',	
			name: 'hora_proceso',
			readOnly: true					
		},{	
			width: 300,	
			fieldLabel: 'Usuario',					
			id: 'usuario1',	
			name: 'usuario',
			readOnly: true					
		},{	
			width: 300,	
			fieldLabel: 'Archivo',					
			id: 'file_name1',	
			name: 'file_name',
			hidden: true					
		}
	];

/******************************************************************************
 *	Elementos de la forma principal														   *
 ******************************************************************************/
	var elementosForma = [
		{
		  xtype: 'fileuploadfield',
		  id: 'archivo',
		  name: 'archivo',
		  emptyText: 'Seleccione...',
		  fieldLabel: "Ruta del Archivo de Origen", 
		  buttonText: null,
		  buttonCfg: { iconCls: 'upload-icon' },
		  anchor: '-20'
		},{
			xtype: 'panel',
			anchor: '100%',
			style: { marginTop: '10px' },
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Continuar',
					iconCls: 'icoContinuar',
					id: 'btnCargaArchivo',
					tooltip: 'Continuar con la carga del archivo',
					handler: function(boton, evento) {
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([xX][lL][sS])$/;
						
						var nombreArchivo = archivo.getValue();
						if( !myRegexTXT.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n xls.");
							return;
						}
						// Cargar archivo
						subeArchivo();
					}
				}
			]
		},{	
			xtype: 'textfield',								
			hidden: true,			
			id: 'nombre_archivo1',	
			name: 'nombre_archivo'					
		}
	];

/******************************************************************************
 *	Panel para mostrar la ventana de ayuda     	 									   *
 ******************************************************************************/	
	var panelLayoutAyuda = {
		xtype: 'panel',
		id: 'panelLayoutAyuda',
		hidden: true,
		width: 650,
		title: 'Ayuda',
		frame: true,
		style: 'margin: 0 auto',
		bodyStyle: 'padding:10px',
		items: elementosLayoutAyuda,
		tools: [{
			id: 'close',
			handler: function(event, toolEl, panel){
				hidePanelLayoutAyuda();
			}
    	}],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}	
 
/********************************************************************************************
 *	Form Panel para ver el grid de solicitudes y los resultados de la validaci�n del archivo *
 ********************************************************************************************/	
	var fg = new Ext.form.FormPanel({
		id: 'formaGrid',
		frame	: false,
		border: false,
		width: 800,		
		hidden: true,
		autoHeight: true,				
		layout : 'form',
		style: 'margin:0 auto;',	
		items: elementosPanel
	});
 
/******************************************************************************
 *	Form Panel para ver el grid de solicitudes por procesar					  		*
 ******************************************************************************/	
	var fr = new Ext.form.FormPanel({
		id: 'formaRegistro',
		title: 'Resumen de solicitudes por procesar ', 
		frame	: false,
		border: true,
		width: 600,		
		hidden: true,
		autoHeight: true,				
		layout : 'form',
		style: 'margin:0 auto;',	
		items: elementosRegistro
	});
	
/******************************************************************************
 *	Form Panel para ver el grid de solicitudes con cambio de estatus  	  		*
 ******************************************************************************/	
 	var fc = new Ext.form.FormPanel({
		id: 'formaCambioEstatus',
		frame	: true,
		border: false,
		title: 'Operaciones que sufrieron cambio de estatus',
		width: 600,		
		autoHeight: true,			
		layout: 'form',
		labelWidth: 180,
		hidden: true,
		style: 'margin:0 auto;',
      defaults: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: 
			elementosCambioEstatus/**,
			buttons: [{
				text: 'Imprimir',					
				tooltip:	'Imprimir archivo PDF ',
				iconCls: 'icoPdf',
				id: 'btnImprimir',
				handler: function(boton, evento) {
					
				}
			}]	*/	
	});
	
/******************************************************************************
 *	Form Panel principal     																	*
 ******************************************************************************/	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title: 'Registro de Operaciones Carga Individual',
		width: 800,		
		autoHeight: true,
		frame: true,
		collapsible: true,
		titleCollapse: true,
		fileUpload: true,
		hidden: false,
		style: 'margin: 0 auto',
		bodyStyle: 'padding:10px',
		labelWidth: 190,
		defaultType: 'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutAyuda();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false
	});
	
/******************************************************************************
 *	Contenedor Principal    								      							*
 ******************************************************************************/	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		height: 'auto',
		disabled: false,
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10),
			fg,
			fr,
			fc,
			gridRegistro,
			panelLayoutAyuda
			
		]
	});
	
	ventanaAyuda(null);
	
});

/**
Regresa la pantalla a su condici�n inicial
Procesos para obtener la ventana de ayuda
Muestran y ocultan la ventana de ayuda
Sube el archivo para ser procesado
Valida el archivo que se subi� previamente
Muestra el resultado del cambio de estatus de las solicitudes procesadas
Procesa las solicitudes correctas
C�digo para generar el Grid de Solicitudes procesadas SIN ERRORES
C�digo para generar el Grid de Solicitudes procesadas CON ERRORES
C�digo para generar el Grid de Registro de solicitudes por procesar
Elementos para mostrar la ventana de ayuda
Elementos para mostrar los resultados de la validaci�n del archivo
Elementos panel de Registro de solicitudes por procesar
Elementos de la forma principal
Panel para mostrar la ventana de ayuda
Form Panel para ver el grid de solicitudes y los resultados de la validaci�n del archivo
Form Panel para ver el grid de solicitudes procesadas
Form Panel principal
Contenedor Principal
*/