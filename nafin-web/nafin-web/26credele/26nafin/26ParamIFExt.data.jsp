<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	org.apache.commons.logging.Log,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	com.netro.exception.*,
	javax.naming.Context,
	com.netro.model.catalogos.CatalogoSimple,
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="../26secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String sFideCorElec_s = (request.getParameter("sFideCorElec_s")!=null)?request.getParameter("sFideCorElec_s"):"S";
String sFideCorElec_n = (request.getParameter("sFideCorElec_n")!=null)?request.getParameter("sFideCorElec_n"):"N";
String sFideCorElec ="N";

log.debug("sFideCorElec_s  "+sFideCorElec_s);
log.debug("sFideCorElec_n  "+sFideCorElec_n); 


if(sFideCorElec_s.equals("true")) { sFideCorElec = "S"; }

log.debug("sFideCorElec  "+sFideCorElec); 


ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);	


String infoRegresar ="";
JSONObject jsonObj = new JSONObject();


if (informacion.equals("catIFsData")){
	
	CatalogoIF_Desc cat = new CatalogoIF_Desc();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setOrden("cg_razon_social");	
	infoRegresar = cat.getJSONElementos();

} else if (informacion.equals("Guardar")){
	
	boolean success			= true;
	String msg = "";

	try{
		Hashtable alParamIF = new Hashtable();
		alParamIF.put("FISO_CORTES_CREDELEC",sFideCorElec);
		
		BeanParamDscto.setParametrosIF(ic_if, alParamIF, 0,iNoUsuario);
		msg = "Se guardo con éxito la parametrización del IF.";

	}catch(NafinException ne){
		ne.printStackTrace();
		msg = ne.getMsgError();
		success = false;
	}

	jsonObj.put("msg",					msg							);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_GRABAR"		);
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();




} else if (informacion.equals("Comsultar")){
	
	String sParamFisoCortes= new String("");
	Hashtable alParamIF =	alParamIF = BeanParamDscto.getParametrosIF(ic_if, 0);
	if (alParamIF!=null) {
		sParamFisoCortes	= alParamIF.get("FISO_CORTES_CREDELEC")==null||alParamIF.get("FISO_CORTES_CREDELEC").equals("")?"N":(String)alParamIF.get("FISO_CORTES_CREDELEC");		
	}


	jsonObj.put("sParamFisoCortes",	sParamFisoCortes		);
	jsonObj.put("success",	new Boolean(true)		);
	infoRegresar = jsonObj.toString();
	
}

	
%>	

<%= infoRegresar %>

<%!

	public HashMap  getPlantillaCorreos ( String operacion ) throws AppException { 
		log.info("getPlantillaCorreos  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();		
		List  lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;
		HashMap	registros = new HashMap();	
		int i =0;
		try{
		
			con.conexionDB();
			
			qrySentencia 	= new StringBuffer();	
			qrySentencia.append(" Select   "+
				" FROM  COMCAT_PARAMETRO_IF p , COMCAT_OPERACIONES o  "+
				" WHERE  p.IC_OPERACIONES = o.IC_OPERACIONES " +
				" AND  p.IC_OPERACIONES = ? "); 
				lVarBind		= new ArrayList(); 
				lVarBind.add(operacion);	
			
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();			
			if(rs.next()){
				registros.put("CG_DESTINATARIO", rs.getString("CG_DESTINATARIO")==null?"":rs.getString("CG_DESTINATARIO"));
				registros.put("CG_CCDESTINATARIO", rs.getString("CG_CCDESTINATARIO")==null?"":rs.getString("CG_CCDESTINATARIO"));
				registros.put("CG_ASUNTO", rs.getString("CG_ASUNTO")==null?"":rs.getString("CG_ASUNTO"));
				registros.put("CG_CUERPO_CORREO", rs.getString("CG_CUERPO_CORREO")==null?"":rs.getString("CG_CUERPO_CORREO"));		
				registros.put("OP_DESCRIPCION", rs.getString("OP_DESCRIPCION")==null?"":rs.getString("OP_DESCRIPCION"));	
				i++;
			}
			rs.close();
			ps.close();
			
			if(i==0)  {	
				qrySentencia 	= new StringBuffer();	
				qrySentencia.append(" Select o.CG_DESCRIPCION AS OP_DESCRIPCION  "+
					" FROM  COMCAT_OPERACIONES o  "+
					" WHERE o.IC_OPERACIONES = ? ");  
					lVarBind		= new ArrayList(); 
					lVarBind.add(operacion);	
				
				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();			
				if(rs.next()){
					registros.put("OP_DESCRIPCION", rs.getString("OP_DESCRIPCION")==null?"":rs.getString("OP_DESCRIPCION"));						
				}
				rs.close();
				ps.close();			
			}
			
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			log.error(" getPlantillaCorreos  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("   getPlantillaCorreos (S) ");
			}
		} 
		return registros;
		
	}
	
%>