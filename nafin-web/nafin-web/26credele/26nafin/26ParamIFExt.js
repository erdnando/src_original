Ext.onReady(function(){
	
	
	
	
	var procesarGuardar = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			Ext.MessageBox.alert('Mensaje','La informaci�n se guard� correctamente',
				function(){ 					
					window.location = '26ParamIFExt.jsp';
			}); 
								
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var procesarConsulta = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
						
			var  forma =   Ext.ComponentQuery.query('#forma')[0];	
			
			var  sFideCorElec_s = forma.query('#sFideCorElec_s')[0];
			var  sFideCorElec_n=  forma.query('#sFideCorElec_n')[0];
						
			if(jsonData.sParamFisoCortes	=='S'){
				sFideCorElec_s.setValue(true)	
				sFideCorElec_n.setValue(false)	
			}else {
				sFideCorElec_n.setValue(true);
				sFideCorElec_s.setValue(false)	
			}
			
			forma.show();
						
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	
	var catIFsData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '26ParamIFExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIFsData'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	var fpCombo = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'formaCombo',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: ' ',
      width: 490,
		//height: 520,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 80
		},
		layout: 'anchor',			
		items	:  [
			{
				xtype: 'combo',
				fieldLabel: 'Nombre IF',
				itemId: 'ic_if1',
				name: 'ic_if',
				hiddenName: 'ic_if',
				forceSelection: true,
				hidden: false, 
				width: 450,
				store: catIFsData,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave',						
				listeners: {
					select: function(obj, newVal, oldVal){						
						
						var  fpCombo =   Ext.ComponentQuery.query('#formaCombo')[0];
						
						Ext.Ajax.request({
							url: '26ParamIFExt.data.jsp',
							params: {							
								informacion: 'Comsultar',							
								ic_if  : fpCombo.query('#ic_if1')[0].getValue()
							},
							callback: procesarConsulta
						});
						
						
						
					}
				}
			}		
		]	
	});
	
	
	
	var fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'forma',
      width:			530,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,		
		hidden: true,
		labelWidth:		100,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items	:  [		
			{
				xtype:			'panel',
				title:			'<div align="center">Par�metros por IF</div>',
				layout:			'table',
				anchor:			'65%',
				frame:			false,
				border:true,
				style:			'margin:0 auto;',
				layoutConfig:	{ columns: 3 },
				defaults:		{frame:true,	border:true,	height:50,	bodyStyle:'padding:5px'},
				items	:  [
					{	html:'<div align="center">1</div>'},{html:'IF Fideicomiso (Cortes-Cr�dito Electr�nico)',width:200},
					{
						layout:		'form',
						labelWidth:	1,
						width:		300,
						items:[
							{
								xtype:	'radiogroup',
								columns:	[50, 50],
								items:[
									{boxLabel: 'SI',	id:'sFideCorElec_s',	name:'sFideCorElec',	inputValue:'S'},
									{boxLabel: 'NO',	id:'sFideCorElec_n',	name:'sFideCorElec',	inputValue:'N'}
								]
							}
						]
					}
				]
			}
		],
			buttons: [		
			{
				text: 'Grabar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(){				
				
					var  fpCombo =   Ext.ComponentQuery.query('#formaCombo')[0];
					var ic_if =	fpCombo.query('#ic_if1')[0];
					
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
				
					
					if(Ext.isEmpty(  ic_if.getValue()  )){
						ic_if.markInvalid('Debe seleccionar una operaci�n');
						ic_if.focus();
						return;
					}
					
					
					Ext.Ajax.request({
						url: '26ParamIFExt.data.jsp',
						params: {							
							informacion: 'Guardar',
							ic_if: fpCombo.query('#ic_if1')[0].getValue(),
							sFideCorElec_s: forma.query('#sFideCorElec_s')[0].getValue(),
							sFideCorElec_n: forma.query('#sFideCorElec_n')[0].getValue()
							
						
						},
						callback: procesarGuardar
					});															
				}
			}			
		]	
	});
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [	
			fpCombo,
			NE.util.getEspaciador(20),
			fp
		]
	});
	
	
	catIFsData.load(); 
	
});