<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.xls.*,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*,
		com.netro.descuento.*,
		com.netro.descuento.AutorizacionSolicitudBean,
		com.netro.credito.*"
	errorPage= "/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../26secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion") == null)?"":request.getParameter("informacion");
String nombreArchivo = (request.getParameter("nombre_archivo") == null)?"":request.getParameter("nombre_archivo");
String datosGrid = (request.getParameter("datos")==null)?"":request.getParameter("datos");
String totalSinError = (request.getParameter("totalSinError")==null)?"":request.getParameter("totalSinError");

String urlArchivo="", consulta = "", infoRegresar = "";
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();
JSONArray registrosCE = new JSONArray();
HashMap datos = new HashMap();
Vector datosVec = null;
Vector registrosDatos = new Vector();
boolean success = true;

log.debug("informacion = <"+informacion+">");

if(informacion.equals("ventanaAyuda")){
	success = true;
	String tablaHtml =
				"<table width='590' cellpadding='3' cellspacing='0' border='0' bordercolor='#A5B8BF'> "+
				"<tr><td colsan=2 align='Center'><span class='titulos'>Carga de Archivo para el proceso SIRAC - Crédito Electrónico<br></span></td></tr> "+
				"<tr><td colsan=2>&nbsp;</td></tr> "+
				"<tr> "+
				"<td ><img src='/../nafin/00utils/gif/ayuda_proceso_sirac.jpeg' alt='Imagen_Ayuda'  border='0'>	</td> "+
				"<td align='center' class='formas' > "+
				"<table width='150' cellpadding='3' cellspacing='0' border='1' bordercolor='#A5B8BF'> "+
				"<tr> "+
				"<td align='Center'><span class='titulos'>Versiones v&aacute;lidas<br> "+
				"de Excel</span><br> "+
				"<span class='formas'> Microsoft Excel 97<br> "+
				"Microsoft Excel 2000<br> "+
				"Microsoft Excel XP</span> "+
				"</td> "+
				"</tr> "+
				"</table> "+
				"</td> "+
				"</tr> "+	
				"<tr><td colsan=2>&nbsp;</td></tr> "+
				"</table> "+
				"<table width='600' cellpadding='3' cellspacing='0' border='1' bordercolor='#A5B8BF'> "+
				"<tr> "+
				"<td class='celda01' align='left'><strong>Número de Campo</strong></td>  "+
				"<td class='celda01' align='Center'><strong>Descripción</strong></td> "+	
				"<td class='celda01' align='Center'><strong>Tipo de Dato</strong></td> "+	
				"<td class='celda01' align='Center'><strong>Longitud</strong></td> "+						
				"</tr> "+					  							 
				"<tr> "+
				"<td class='formas' align='left'><font color='#0000FF'><strong>1</strong></font></td> "+
				"<td class='formas' align='left'>&nbsp;<strong>Fila 1:</strong> Esta  fila es utilizada para definir los títulos que desee para cada "+
				"columna.</td> "+
				"<td  class='formas' align='left'>&nbsp;</td> "+
				"<td  class='formas' align='left'>&nbsp;</td> "+
				"</tr> "+
				"<tr> "+
				"<td  class='formas' align='left'><font color='#0000FF'><strong>2</strong></font></td> "+
				"<td  class='formas' align='left'>&nbsp;<strong>Número de folio:</strong> Capturar el número de folio de la operación.</td> "+
				"<td  class='formas' align='left'>Numérico</td> "+
				"<td  class='formas' align='left'>9</td> "+
				"</tr> "+
				"<tr> "+
				"<td  class='formas' align='left'><font color='#0000FF'><strong>3</strong></font></td> "+
				"<td  class='formas' align='left'>&nbsp;<strong>Número de Prestamo:</strong> Capturar el número de préstamo</td> "+
				"<td  class='formas' align='left'>Numérico</td> "+
				"<td  class='formas' align='left'>12</td> "+
				"</tr> "+
				"<tr> "+
				"<td  class='formas' align='left'><font color='#0000FF'><strong>4</strong></font></td> "+
				"<td  class='formas' align='left'>&nbsp;<strong>Fecha de operación:</strong> A fin de que el sistema reconozca de manera correcta este campo,  "+
				"primero deberá formatear la columna a tipo de texto, mediante las opciones: Formato, Celdas, Texto y posteriormente capturar las fechas con  "+
				"el formato: dd/mm/aaaa; por ejemplo 25/04/2006 y la fecha deberá quedar alineada a la izquierda de la columna.</td> "+
				"<td  class='formas' align='left'>Date</td> "+
				"<td  class='formas' align='left'>10</td> "+
				"</tr> "+
				"</table>";	
				
	jsonObj.put("tablaHtml", tablaHtml);
	jsonObj.put("success", new Boolean(success));				
	infoRegresar = jsonObj.toString();		
	
} else if(informacion.equals("SubirArchivo")){
	
	JSONObject resultado = new JSONObject();
	success = true;
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
	} catch(Exception e) {
		success = false;
		log.warn("SubirArchivo(Exception): Cargar en memoria el contenido del archivo: " + e);
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		//myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));	
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));	
		resultado.put("NOMBRE_ARCHIVO", myFile.getFileName().replaceAll("\\s+"," "));
	}catch(Exception e){
		success = false;
		log.warn("SubirArchivo(Exception): Guardar Archivo en Disco: " + e);
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("ValidaArchivo") || informacion.equals("ValidaArchivoSE")){

	success = true;
	JSONObject resultado = new JSONObject();
	Map mResultado	= null;
	List lDatos = null;
	List listaCorrectos = null;
	List listaErrores = null;
	//String rutaArchivo = strDirectorioTemp + nombreArchivo;
	String rutaArchivo = strDirectorioTemp + iNoUsuario + "." + nombreArchivo	;
	ComunesXLS xls = new ComunesXLS();
	// Obtener instancia del EJB
	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	//Se procesa el archivo	
	lDatos = xls.LeerXLS(rutaArchivo, false);
	mResultado	= autSolEJB.ovProcesarArchivoSiracCE(lDatos);
	listaCorrectos = (List)mResultado.get("correctos");
	listaErrores = (List)mResultado.get("errores");	

	if(informacion.equals("ValidaArchivo")){
		for(int i=0;i<listaErrores.size();i++){
			datos = new HashMap();
			Map mDetErr = (Map)listaErrores.get(i);
			//Modifico el link para mostrar la ayuda
			String solucion = (""+mDetErr.get("solucion")).replaceAll("Ayuda", "");		
			datos.put("LINEA_CON_ERROR", mDetErr.get("linea"));
			datos.put("ERROR", mDetErr.get("error"));
			datos.put("SOLUCION", solucion);
			registros.add(datos);
		}
		resultado.put("total", ""+registros.size());
		resultado.put("registros", registros.toString());
		resultado.put("total_sin_errores", ""+listaCorrectos.size());
		resultado.put("total_con_errores", ""+listaErrores.size());
		
	} else if(informacion.equals("ValidaArchivoSE")){
	
		for(int i=0;i<listaCorrectos.size();i++){
			datos = new HashMap();
			datos.put("LINEA", listaCorrectos.get(i));
			registros.add(datos);
		}
		resultado.put("total", ""+registros.size());
		resultado.put("registros", registros.toString());
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();
		
} else if(informacion.equals("ProcesaCambios")){
	
	success = true;
	JSONObject resultado = new JSONObject();
	String datosArray[] = null;
	String datosMod="";
	
	datosGrid = datosGrid.replace('[',' ').replace(']',' ').replace('{',' ').replace('}',' ').replace('"',' ');
	datosGrid = datosGrid.replaceAll("LINEA :", " ");
	for (int x=0; x < datosGrid.length(); x++) {
		if (datosGrid.charAt(x) != ' ')
		datosMod += datosGrid.charAt(x);
	}
	datosArray = datosMod.split(",");
	for(int i=0; i<datosArray.length; i++){
		List lReg = Comunes.explode("|",datosArray[i]);
		String folio = (String)lReg.get(0);
		String prestamo = (String)lReg.get(1);
		String fecOpe = (String)lReg.get(2);
		folio = ""+Integer.parseInt(folio,16);
		prestamo = ""+Integer.parseInt(prestamo,8);
		datos = new HashMap();
		datos.put("VECTOR",datosArray[i]);
		datos.put("FOLIO",folio);
		datos.put("PRESTAMO",prestamo);
		datos.put("FECHA_OPERACION",fecOpe);
		registros.add(datos);
	}
	resultado.put("total", ""+registros.size());
	resultado.put("registros", registros.toString());
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();	
	
} else if(informacion.equals("CambioEstatus")){

	success = true;
	JSONObject resultado = new JSONObject();
	String datosArray[] = null;
	String datosMod="";
	List datosArchivo = null;
	
	AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	
	datosGrid = datosGrid.replace('[',' ').replace(']',' ').replace('{',' ').replace('}',' ').replace('"',' ');
	datosGrid = datosGrid.replaceAll("LINEA :", " ");
	for (int x=0; x < datosGrid.length(); x++) {
		if (datosGrid.charAt(x) != ' ')
		datosMod += datosGrid.charAt(x);
	}
	datosArray = datosMod.split(",");	
	for(int i=0; i<datosArray.length; i++){	
		List lReg = Comunes.explode("|",datosArray[i]);
		String folio = (String)lReg.get(0);
		String prestamo = (String)lReg.get(1);
		String fecOpe = (String)lReg.get(2);
		folio = ""+Integer.parseInt(folio,16);
		prestamo = ""+Integer.parseInt(prestamo,8);
		datosVec = new Vector();
		datosVec.add(folio);
		datosVec.add(prestamo);
		datosVec.add(fecOpe);
		registrosDatos.addElement(datosVec);		
	}
	try{
		Map mAcuse = autSolEJB.confirmaSiracCredEle(datosArray,iNoUsuario);
		resultado.put("acuse", ""+ mAcuse.get("acuse"));
		resultado.put("fecha", mAcuse.get("fecha"));
		resultado.put("hora", mAcuse.get("hora"));
		resultado.put("iNoUsuario", iNoUsuario+"-"+strNombreUsuario);
		resultado.put("solicProcesadas", "" + datosArray.length);
	
		//Envío datos para generar el PDF
		GeneraPDFCreditoElectronico generaPdf = new GeneraPDFCreditoElectronico();
		generaPdf.setAcuse("" + mAcuse.get("acuse"));
		generaPdf.setFecha("" + mAcuse.get("fecha"));
		generaPdf.setHora("" + mAcuse.get("hora"));
		generaPdf.setUsuario(iNoUsuario+"-"+strNombreUsuario);
		generaPdf.setSolicProcesadas("" + datosArray.length);
		String fileName = generaPdf.generaPdfSolicitudesProcesadas(registrosDatos, request, strDirectorioTemp);
		resultado.put("fileName", strDirecVirtualTemp+fileName);
		
	} catch(Exception e){
		log.warn("Error en 26orma03ext.data.jsp informacion = CambioEstatus: " + e);
		success = false;
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();		
}
%>
<%=infoRegresar%>