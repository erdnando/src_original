Ext.onReady(function() {
	
	var hidCifrasControl = Ext.getDom('hidCifrasControl').value;
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var columna= 4;
	var txttotdoc = Ext.getDom('txttotdoc').value;
	var txtmtodoc = Ext.getDom('txtmtodoc').value;
	var txtmtomindoc = Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc = Ext.getDom('txtmtomaxdoc').value;
	var txttotdocdo = Ext.getDom('txttotdocdo').value;
	var txtmtodocdo = Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo = Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo = Ext.getDom('txtmtomaxdocdo').value;
	var tipoCargaE  = "";
      	var textoFirmar; 
        var mesPago;
        var anioPago;
        var tipoCalen;
        var noRegistros;
        
	
		//para mostrar los archivos con errores
	
	
        var catalogoMes = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [
            [1, 'Enero'], [2, 'Febrero'],[3, 'Marzo'],
            [4, 'Abril'],[5, 'Mayo'],[6, 'Junio'],
            [7, 'Julio'],[8, 'Agosto'],[9, 'Septiembre'],
            [10, 'Octubre'],[11, 'Noviembre'],[12, 'Diciembre'],
            [13, '1er Semestre'],[14, '2do Semestre']
        ]
    });
    
    /*var catalogoAnio = new Ext.data.ArrayStore({
        fields: [
            'idAnio',
            'anio'
        ],
				
        data: [[2010,2010],[2011,2011],[2012,2012],[2013,2013],[2014,2014],[2015,2015],[2016,2016],[2017,2017]]
				
    });*/
    
    var dt= Date();
var anio =Ext.util.Format.date(dt,'Y');
anio--;
var mesP =Ext.util.Format.date(dt,'m');
mesP--;
if(mesP<1){
mesP=12;
anio--;
}
	
           var fpConfirmar = new Ext.Container({
		layout: 'table',
		id: 'fpConfirmar',
		hidden: true,
		layoutConfig: {
			columns: 3
		},
		width:	'300',
                align: 'center',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Confirmar documentos',
			id: 'btnConfirma',
			iconCls: 'icoAceptar',
			handler: procesarConfirmar,	
			hidden: true
		},		
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',
			hidden: true,
			handler: function() {
			window.location = '40CargaCalExt.jsp?idMenu=40ALTACALENDARIO';
			}
		}
                /*,
		//estos son para el acuseABR
		{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDFAcuse',
				hidden: true
			},			
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDFAcuse',
				hidden: true
			},
			//'-',
			{
				xtype: 'button',
				text: 'Generar Archivo',
				id: 'btnGenerarCSVAcuse',
				hidden: true
			},			
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				id: 'btnBajarCSVAcuse',
				hidden: true
			}*/
                        ,
			//'-',
			
			{
				text: 'Salir',
				xtype: 'button',
				id: 'btnSalir',
                                iconCls: 'icoLimpiar',
				hidden: true,
				handler: function() {						
					window.location = '40CargaCalExt.jsp?idMenu=40ALTACALENDARIO';
				}
			}
			
	]
	});
        
        
	var procesarSuccessValidaCarga = function(store, arrRegistros, opts) {
            //console.log("procesarSuccessValidaCarga ini");
		var jsonData = store.reader.jsonData;
		var error_enc = jsonData.ERROR_ENC;
		var mensajeCorreccion = jsonData.MENSAJE_CORRECCION;
		var confirmaCorreccion = jsonData.CONFIRMA_CORRECCION;

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var fp = Ext.getCmp('formaCarga');

		mesPago = Ext.getCmp("cboMes").getValue();
		anioPago = jsonData.ANIO;
		tipoCalen = Ext.getCmp("tipoCal").getValue().value;

		fp.el.unmask();
		
		//Valido si es Correccion
		if(tipoCalen === 'C') {
                    //console.log("Es correccion");
                    if(mensajeCorreccion!=='') {
                        Ext.MessageBox.alert('Mensaje', mensajeCorreccion, function(){
                            window.location = '40CargaCalExt.jsp?idMenu=40ALTACALENDARIO';
                        });
                        
                    } else {
                        if(confirmaCorreccion!=='') {
                            var btnBox = Ext.MessageBox; 
                            btnBox.buttonText={
                            cancel: 'Cancelar',
                            no: 'Cancelar',
                            ok: 'Aceptar',
                            yes: 'Aceptar'
                            };
                            Ext.MessageBox.confirm('Mensaje',confirmaCorreccion,function(msb){
                                if(msb=='yes'){
                                    btnBox=null;
                                    muestraGridPreAcuse(store, arrRegistros, opts);
				} else {
                                    btnBox=null;
                                    window.location = '40CargaCalExt.jsp?idMenu=40ALTACALENDARIO';
                                }
                            });

                        } else {
                            //Debug: Si entra aqui es porque pas� algo raro y se tiene que mostrar algo en pantalla
                            muestraGridPreAcuse(store, arrRegistros, opts);
                        }
                    }
		} else {
                    //console.log("Mando a construir el grid.");
                    muestraGridPreAcuse(store, arrRegistros, opts);
		}
                //console.log("procesarSuccessValidaCarga fin");

	}

    function muestraGridPreAcuse(store, arrRegistros, opts){
        //console.log("muestraGridPreAcuse ini");
        var jsonData = store.reader.jsonData;
        var proceso = jsonData.NUMERO_PROCESO; 
        var error_enc = jsonData.ERROR_ENC; 
        var archivo = jsonData.ARCHIVO_ERRORES;
        var limiteLineaCredito  = jsonData.limiteLineaCredito;
        var btnConfirmaCarga = jsonData.btnConfirmaCarga;

        var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
        var fp = Ext.getCmp('formaCarga');

        mesPago = Ext.getCmp("cboMes").getValue();
        anioPago = jsonData.ANIO;
        tipoCalen = Ext.getCmp("tipoCal").getValue().value;
        if (arrRegistros != null) {
            if (!gridResulCarga.isVisible()) {
                gridResulCarga.show();
            }

            var el = gridResulCarga.getGridEl();
            if(store.getTotalCount() > 0) {
                var registrosCo = jsonData.TOTALDOCS;
                noRegistros = registrosCo;
                if(registrosCo>0){
                    fpConfirmar.show();
                    var btnConfirma = Ext.getCmp('btnConfirma');
                    btnConfirma.show();
                    var btnCancelar = Ext.getCmp('btnCancelar');
                    btnCancelar.show();
                    gridTotales.show();
                    gridLayout.hide();
                    fpCarga.hide();
                }
                var cm = gridResulCarga.getColumnModel();
                var errorE = jsonData.TOTALDOCE;
                //se llena el grid de Totales 
                var cifras = [
                    [jsonData.TOTALDOCS, 
                    jsonData.MES,
                    jsonData.ANIO,
                    jsonData.TOTALDOCE,
                    jsonData.MESER,
                    jsonData.ANIOER]
                ];
                resumenTotalesData.loadData(cifras);
	
                //se muestra el mensaje bajo los totales en caso de haber
                var table ='<table align="left" width="700" class="formas" cellspacing="0" cellpadding="3">'+
                        '<tr><td width="275" colspan="4" >Errores vs cifras de control:</td> </tr>'+
                        '<tr>'+
                            '<td valign="middle" align="left" class="formas" width="275" colspan="4">'+
                                '<textarea name="conerrores" cols="100" wrap="hard" rows="6" class="formas">'+error_enc+'</textarea>'+
                            '</td>'+
                        '</tr>'+
                    '</table>';
                var fpError = new Ext.Container({
                    layout: 'table',
                    id: 'fpError',
                    width: 'auto',
                    heigth: 'auto',
                    style: 'margin:0 auto;',
                    items: [{ 
                        xtype: 'label',  
                        html: table, 
                        cls: 'x-form-item', 
                        style: { 
                            width: '100%', 
                            textAlign: 'left'
                        } 
                    }]
                });
        
                contenedorPrincipalCmp.remove(fpError);

                if(error_enc!='') {
                    contenedorPrincipalCmp.add(fpError);
                }
                contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
                contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
                contenedorPrincipalCmp.doLayout();

                if(errorE==0){
                    Ext.MessageBox.alert("Mensaje","EL ARCHIVO SE PROCESO CORRECTAMENTE");
                }else{
                    Ext.MessageBox.alert("Mensaje","EL ARCHIVO SE PROCESO PERO EXISTEN ALGUNOS ERRORES");
                }

                textoFirmar = "Total de Registros Correctos: "+  jsonData.TOTALDOCS+"\n" +
                "Mes de pago: "+jsonData.MES+"\n"+
                "A�o de Pago: "+jsonData.ANIO+"\n";

            } else {
                el.mask('No se encontr� ning�n registro', 'x-mask');
            }
        }
        //console.log("muestraGridPreAcuse fin");
    }

		//para mostrar TOTALES 
	var resumenTotalesData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'TOTALDOCS'},
			  {name: 'MES'},
                           {name: 'ANIO'},
				{name: 'TOTALDOCE'},
				{name: 'MESER'},
				{name: 'ANIOER'}
		  ]
	 });
	 
	 	var gruposT = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Documentos sin errores', colspan: 3, align: 'center'},
				{header: 'Documentos con errores', colspan: 3, align: 'center'}					
			]
		]
	});
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',
		store: resumenTotalesData,	
		margins: '20 0 0 0',
		align: 'center',
		title: '',
		plugins: gruposT,	
		hidden: true,
		columns: [	
			{
				header: 'Total documentos',
				dataIndex: 'TOTALDOCS',
				width: 157,
				align: 'center'			
			},	
			{
				header: 'Mes de Pago',
				dataIndex: 'MES',
				width: 157,
                              	align: 'center'
			},
			{
				header: 'A�o de Pago',
				dataIndex: 'ANIO',
				width: 157,
				align: 'center' 		
			},	
			{
				header: 'Total documentos',
				dataIndex: 'TOTALDOCE',
				width: 157,
				align: 'center'			
			},	
			{
				header: 'Mes Error',
				dataIndex: 'MESER',
				width: 157,
				align: 'center'		
			},
			{
				header: 'A�o Error',
				dataIndex: 'ANIOER',
				width: 157,
				align: 'center' 	
			}	
		],
		height: 100,	
		width : 943,
		frame: false
	});
	
	
	//Resultados del proceso de carga
		var resultadosCargaData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '40CargaCalExt.data.jsp',
		baseParams: {
			informacion: 'ValidaCargaArchivo'		
		},								
		fields: [			
			{name: 'DESCRIP_SERROR' , mapping: 'DESCRIP_SERROR'},			
			{name: 'FECHA_E_SERROR', mapping: 'FECHA_E_SERROR'},
                        {name: 'FECHA_R_Error', mapping:'FECHA_R_Error'},
			{name: 'FECHA_A_SERROR', mapping: 'FECHA_A_SERROR' },
                        {name: 'FECHA_D_SERROR', mapping: 'FECHA_D_SERROR' },                        
			{name: 'NUDOCTO_CERROR',  mapping: 'NUDOCTO_CERROR'},			
			{name: 'CERROR',  mapping: 'CERROR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarSuccessValidaCarga,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSuccessValidaCarga(null, null, null);						
				}
			}
		}
	});
	
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Documentos sin errores', colspan: columna, align: 'center'},
				{header: 'Documentos con errores', colspan: 2, align: 'center'}					
			]
		]
	});
	
	
	var gridResulCarga = new Ext.grid.EditorGridPanel({	
		id: 'gridResulCarga',
		store: resultadosCargaData,	
		margins: '20 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		plugins: grupos,	
		title: 'Resultados del proceso de carga',		
		hidden: true,
		columns: [	
			{
				header: 'Numero de Intermediario',
				dataIndex: 'DESCRIP_SERROR',
				width: 117,
				align: 'center'	,
				resizable: true	
			},	
			{
				header: 'FECHA LIM EXP',
				dataIndex: 'FECHA_E_SERROR',
				width: 117,
				resizable: true	,
				align: 'center'
			},
                        {
                                header: 'FECHA RESULTADO',
                                dataIndex: 'FECHA_R_Error',
                                width:117,
                                aling: 'center',
                                realizable: true
                        },
			{
				header: 'FECHA LIM ACL',
				dataIndex: 'FECHA_A_SERROR',
				width: 117,
				align: 'center',
				resizable: true
			},
                        {
				header: 'FECHA DICTAMEN',
				dataIndex: 'FECHA_D_SERROR',
				width: 117,
				align: 'center',
				resizable: true
			},
			//Documentos con errores
			{							
				header : 'Numero de Intermediario',			
				dataIndex : 'NUDOCTO_CERROR',
				width : 235,
				resizable: true	,
				align: 'center'				
			}	,
			{
				header: 'Error',
				dataIndex: 'CERROR',
				width: 352,
				resizable: true	,
				align: 'left'			
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		align: 'center',
		frame: false
	});
	
		//STORES////////////////////////////////////////////////////////////////////////
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBLIGATORIO'}
	  ]
	});
	//HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////
	var infoLayout = [];
	if(tipoCargaE == 'O'){
            infoLayout = [
				['1','Cve SIAG','Num�rico','8','Si'],
				['2','Mes de Pago','Num�rico','2','Si'],
				['3','A�o de Pago.','Num�rico','4','Si'],
				['4','Fecha L�mite de Entrega de Expedientes','Fecha','dd/mm/aaaa','Si'],
                                ['5','Fecha L�mite de Resultados de Supervisi�n','Fecha','dd/mm/aaaa','Si'],
                                ['6','Fecha L�mite de Entrega de Aclaraciones','Fecha','dd/mm/aaaa','Si'],
                                ['7','Fecha Limite de Dictamen','Fecha','dd/mm/aaaa','Si'],
				['8','N�mero de Autorizaci�n','Alfanum�rico','15','No'],
                                ['9',' Relaci�n de Garant�as','Alfanum�rico','250','Si']
			];
        }else{
              infoLayout = [
				['1','Cve SIAG','Num�rico','8','Si'],
				['2','Mes de Pago','Num�rico','2','Si'],
				['3','A�o de Pago.','Num�rico','4','Si'],
				['4','Fecha L�mite de Entrega de Expedientes','Fecha','dd/mm/aaaa','Si'],
                                ['5','Fecha L�mite de Resultados de Supervisi�n','Fecha','dd/mm/aaaa','Si'],
                                ['6','Fecha L�mite de Entrega de Aclaraciones','Fecha','dd/mm/aaaa','Si'],
                                ['7','Fecha Limite de Dictamen','Fecha','dd/mm/aaaa','Si'],
				['8','N�mero de Autorizaci�n','Alfanum�rico','15','Si'],
                                ['9',' Relaci�n de Garant�as','Alfanum�rico','250','Si']
			];
        }
        
        

	storeLayoutData.loadData(infoLayout);
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No. de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'DESC',
				width : 200,
				sortable : true
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 200,
				sortable : true
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 200,
				sortable : true
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 830,
		height: 470,
		frame: true
	});
	
	
	var confirmar = function(pkcs7, vtextoFirmar){
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}				
		Ext.Ajax.request({
			url : '40CargaCalExt.data.jsp',
			params : {
				pkcs7: pkcs7,
				textoFirmado: vtextoFirmar,
				informacion: 'ACUSE',						
				mesPago:mesPago,
				anioPago:anioPago,
				tipoCarga: tipoCalen,
				noRegistros: noRegistros
			},
			callback: procesarSuccessFailureGuardar
		});	
	}
	
	
	var myValidFn = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		}
		Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
		});
                
                
       function procesarConfirmar(opts, success, response) {
	   
	   NE.util.obtenerPKCS7(confirmar, textoFirmar );						
	}    
        
        var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				var btnConfirma = Ext.getCmp('btnConfirma');
				var btnCancelar = Ext.getCmp('btnCancelar');
				btnConfirma.hide();
				btnCancelar.hide();
				gridCifrasControl.hide();
                                gridResulCarga.hide();
                                gridTotales.hide();

				var acuseCifras = [
					['N�mero de Acuse', jsondeAcuse.acuse],
                                        ['N�mero de Registros Correctos ', noRegistros],
					['Fecha ', jsondeAcuse.fecha],
					['Hora ', jsondeAcuse.hora],
					['Usuario ', jsondeAcuse.usuario]
                                        
				];				
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				
				
				
				var btnSalir = Ext.getCmp('btnSalir');
				btnSalir.show();
					
			}						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
        
        var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
           var procesarAnioIni = function(store, arrRegistros, opts) {
		alert('ya entro');
                if (arrRegistros != null) {
			Ext.ComponentQuery.query('#claveAnioI')[0].setValue(arrRegistros[0]);
		}
	}
        
       var catalogoAnio = new Ext.data.JsonStore({
                id		: 'catAnio',
		root 		: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 		: '40CargaCalExt.data.jsp',
		baseParams	: { informacion: 'Consulta_Anio'},
		autoLoad		: false,
		listeners	:
		{
			//load		: procesarIF,
			exception: NE.util.mostrarDataProxyError
		}
	});
         
        var gridCifrasControl = new Ext.grid.GridPanel({
	id: 'gridCifrasControl',
	store: storeCifrasData,
	margins: '20 0 0 0',
	style: 'margin:0 auto;',
	hideHeaders : true,
	hidden: true,
	align: 'center',
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 200,
			sortable : true
		},
		{
			header : 'Informacion',			
			dataIndex : 'informacion',
			width : 350,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 560,
	style: 'margin:0 auto;',
	autoHeight : true,
	//title: '',
	frame: true
	});
		
	var elementosFormaCarga = [	
		{
			xtype:	'panel',
			layout:	'column',
			width: 750,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
					
					if (!gridLayout.isVisible()) {
						tipoCargaE = Ext.getCmp("tipoCal").getValue().value;
                                                if(tipoCargaE == 'O'){
                                                    infoLayout = [
                                                        ['1','Cve SIAG','Num�rico','8','Si'],
                                                        ['2','Mes de Pago','Num�rico','2','Si'],
                                                        ['3','A�o de Pago.','Num�rico','4','Si'],
                                                        ['4','Fecha L�mite de Entrega de Expedientes','Fecha','dd/mm/aaaa','Si'],
                                                        ['5','Fecha L�mite de Entrega de Resultados de Supervisi�n','Fecha','dd/mm/aaaa','Si'],
                                                        ['6','Fecha L�mite de Entrega de Aclaraciones','Fecha','dd/mm/aaaa','Si'],
                                                        ['7','Fecha Limite de Dictamen','Fecha','dd/mm/aaaa','Si'],
                                                        ['8','N�mero de Autorizaci�n','Alfanum�rico','15','No'],
                                                        ['9',' Relaci�n de Garant�as','Alfanum�rico','250','Si']
                                                    ];
                                                }else{
                                                    infoLayout = [
                                                        ['1','Cve SIAG','Num�rico','8','Si'],
                                                        ['2','Mes de Pago','Num�rico','2','Si'],
                                                        ['3','A�o de Pago.','Num�rico','4','Si'],
                                                        ['4','Fecha L�mite de Entrega de Expedientes','Fecha','dd/mm/aaaa','Si'],
                                                        ['5','Fecha L�mite de Entrega de Resultados de Supervisi�n','Fecha','dd/mm/aaaa','Si'],
                                                        ['6','Fecha L�mite de Entrega de Aclaraciones','Fecha','dd/mm/aaaa','Si'],
                                                        ['7','Fecha Limite de Dictamen','Fecha','dd/mm/aaaa','Si'],
                                                        ['8','N�mero de Autorizaci�n','Alfanum�rico','15','Si'],
                                                        ['9',' Relaci�n de Garant�as','Alfanum�rico','250','Si']
                                                ];
                                            }
                                            storeLayoutData.loadData(infoLayout);                                             
                                            gridLayout.show();
                                            }else{
						gridLayout.hide();
                                            }					
					}
				},
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .90,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 85,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: '',
									width: 350,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '100%',
									vtype: 'archivotxt'
								}						
							]
						},
                                                {
			xtype: 'compositefield',
			id:'mesAnio',
			columns: 3,
			items:[
				{
					xtype: 'combo',
					value: mesP,
					editable:false,
					fieldLabel: 'Mes de Pago',
					displayField: 'displayText',
					valueField: 'myId',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'Hmes',
					store: catalogoMes,
					id: 'cboMes',
					mode: 'local',
                                        width: 90,
					hiddenName: 'Hmes',
					hidden: false,
					emptyText: 'Seleccionar Mes'
		},
                  {
					xtype: 'displayfield',
					value: 'A�o:',
					width: 20
				},                
                {	
					xtype: 'combo',
					displayField: 'descripcion',
					editable:false,
					valueField: 'clave',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					name:'claveAnioI',
					id: 'cboAnio',
                                        itemId:  'claveAnioI',
                                        forceSelection:true,
                                        width: 75,
					//store: catalogoAnio,
					mode: 'local',
					hiddenName: 'A�o',
					hidden: false,
                                        queryMode:				'local',
					emptyText: 'Seleccionar A�o',
                                        store: catalogoAnio
                                        
                },    
				
                                {
							xtype: 'radiogroup',
							allowBlank: false,
							fieldLabel: '',
							columns: 3,
                                                        id:'tipoCal',
                                                        name: 'tipoCal',
                                                        width: 320,
                                                        default: {
								msgTarget: 'side'
                                                                ,
								anchor: '-10'
							},
							items: [
								{
									boxLabel: 'Ordinaria',
									name: 'tipoCal',
                                                                        id: 'tipoCalO',
									inputValue: 'O',
                                                                        value: 'O',
                                                                        checked: true
								},
								{
									boxLabel: 'Extemporanea',
									name: 'tipoCal',
                                                                        id: 'tipoCalE',
									inputValue: 'E',
                                                                        value: 'E'
								},
								{
									boxLabel: 'Semestral',
									name: 'tipoCal',
                                                                        id: 'tipoCalS',
									inputValue: 'S',
                                                                        value: 'S'
								},
								{
									boxLabel: 'Correcci&oacute;n',
									name: 'tipoCal',
									id: 'tipoCalC',
									inputValue: 'C',
									value: 'C'
								}
							],
						listeners: {
                                                    change: {
                                                        fn: function(field, newValue, oldValue, options) {
                                                                tipoCargaE = newValue.value;
                                                                gridLayout.hide();
                                                            }
                                                    }
                                                }
                                                
                                                }
                                
                                
                                
                                ]
			 		
		}
                 
					]
				}	
			]
		}	
	];
        
   
	
	var fpCarga = new Ext.form.FormPanel({
	  id: 'formaCarga',
		width: 720,
		title: 'Ubicaci�n del archivo de datos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-10'
		},		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:  function(){
					var archivo = Ext.getCmp("archivo");
                                        var cboAnioC = Ext.getCmp("cboAnio");
					if(Ext.isEmpty(cboAnioC.getValue())){
                                            cboAnioC.markInvalid('El A�o es requerido.');	
                                            return;
                                        }
                                        
                                        if (Ext.isEmpty(archivo.getValue()) ) {
						archivo.markInvalid('El valor de la Ruta es requerida.');	
						return;
                                                
					}else {							 
						fpCarga.getForm().submit({
						url: '40CargaCalfile.data.jsp',									
						waitMsg: 'Enviando datos...',
						waitTitle :'Por favor, espere',		
							success: function(form, action) {								
								var resp = action.result;
								var mArchivo =resp.archivo;
								var error_tam = resp.error_tam;																		
								if(!resp.codificacionValida){
									new NE.cespcial.AvisoCodificacionArchivo({codificacion:resp.codificacionArchivo}).show();
								}else if(mArchivo!='') {	
								var cboMes = Ext.getCmp("cboMes").getValue();
                                                                var cboAnio   = Ext.getCmp("cboAnio").getValue();
                                                                fpCarga.el.mask('Procesando...', 'x-mask-loading');	
									resultadosCargaData.load({
										params: {
											informacion: 'ValidaCargaArchivo',
											archivo:mArchivo,
											mes:cboMes,
                                                                                        cboAnio:cboAnio,
                                                                                        tipoCal:Ext.getCmp("tipoCal").getValue().value
										}
									});															
								}	else{
									Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
									return;
								}
							}
							,failure: NE.util.mostrarSubmitError
						})												
					}	
				}
			}
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [			
			NE.util.getEspaciador(20),
			fpCarga,
                        NE.util.getEspaciador(20),
			gridCifrasControl,
                        NE.util.getEspaciador(20),
                        fpConfirmar,
			NE.util.getEspaciador(20),
			gridResulCarga,			
			gridTotales,
			gridLayout,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoAnio.load();
	
	
} );