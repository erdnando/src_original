<%@ page contentType="application/json;charset=UTF-8" import="
        org.apache.commons.logging.Log,
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.nafin.supervision.*,
        com.netro.distribuidores.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	java.io.*,	
	java.text.*, 
	java.math.*,
	net.sf.json.*" errorPage="/00utils/error_extjs.jsp, /00utils/error_extjs_fileupload.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%@ include file="../../certificado.jspf"%>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String hidCifrasControl  = (request.getParameter("hidCifrasControl") != null) ? request.getParameter("hidCifrasControl") : "N";
String numDoctoCE = "";	
String numDoctoSE = "";
String fechaESE = "";
String fechaRSE = "";
String fechaASE = "";
String fechaDSE = "";
String MESE  = "";

String infoRegresar ="";
String rutaArchivoTemporal = null;
ParametrosRequest req = null;
StringBuffer contenidoArchivo=new StringBuffer("");
JSONObject 	resultado	= new JSONObject();
//DECLARACION DE VARIABLES
int numFiles=0, i=0, nd=1, comp_fecha=0, no_pyme=0, pipesporlinea=0, ic_proc_docto=0,
lineadocs=1;
boolean ok=true, ok_enc=true,	ok_fechas=true;
String ln	=	"",  numero_docto	=	"",
fn_monto	=	"", 
dscto_especial =	"", 
arma_error="", tipo_Pago ="";
String ses_cg_razon_social=strNombre;

Vector vecColumnas 		= null;
Vector vecFilasSE		= null;
Vector vecColumnasSE	= null;
Vector vecFilasCE 		= new Vector();
Vector vecColumnasCE	= null;
Vector vecFilas         = new Vector();
VectorTokenizer vt=null; 
Vector vecdat=null; 
Vector ig_nums_doctos=new Vector(); 
String ses_ic_epo=iNoCliente;
double monto_mn_ok = 0,  monto_dolar_ok=0,  monto_mn_er=0,  monto_dolar_er=0;	
BigDecimal monto_mn=new BigDecimal("0.0");	BigDecimal monto_dolar=new BigDecimal("0.0");
StringBuffer error=new StringBuffer();  /* documentos Malos */
StringBuffer bueno=new StringBuffer(); 	/* documentos buenos */
StringBuffer error_enc=new StringBuffer();  /* Enc Malos */
StringBuffer seguridad=new StringBuffer("");
StringBuffer bueno_det=new StringBuffer();  /* Detalle Bueno */	
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

String noIntermediario = "";
String mesPagoG    = "";;
String anioPagoG  = "";
String fecha_limite_E = "";
String fecha_limite_R = "";
String fecha_limite_A = "";
String fecha_limite_D = "";
String no_Autorizacion	= "";
String rel_garantias   = "";

try {
	Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
	//ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	if(informacion.equals("ValidaCargaArchivo")) {
		ic_proc_docto=0;
		//parametrizaciones

		String cboAnio  = (request.getParameter("cboAnio") != null) ? request.getParameter("cboAnio") : "0";
		String mes      = (request.getParameter("mes")     != null) ? request.getParameter("mes")     : "0";
		String tipoCal  = (request.getParameter("tipoCal") != null) ? request.getParameter("tipoCal") : "0";

		log.trace("Anio: " + cboAnio+". Mes: " + mes + ".tipo_cal: " + tipoCal);

		String query="";
		ResultSet rs=null;
		Vector diames_inhabil=new Vector();
		java.util.Date FechaHoy=new java.util.Date();
                Calendar dia_fv = new GregorianCalendar();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy"); 
		java.util.Date fechaDocto=null;                 
		java.util.Date fechaVenc=null;
		java.util.Date fechaContr=null;
		int total_campos=0, c1=0, c2=0, c3=0, c4=0, c5=0;
                boolean sin_campos=false;
		int noCampo[]=new int[6];
                String tipoDato[]=new String[6];
		int longitud[]=new int[6];
                String nombreCampo[]=new String[6];
                
                //ABR:: Variables para cuando es tipoCal='C'
                String mensajeCorreccion = ""; //Se envía cuando es corrección y el archivo tiene más de un registro. O cuando el IF no tiene un calendario cargado.
                String confirmaCorreccion = ""; //Se envía cuando es corrección y tiene un calendario ordinario o extraordinario.
                String tipoCalendarioCargado = ""; //Consulta a la BD si hay un calendario cargado para el IF
                Integer cveSiag = 0;
                Integer mesInt = Integer.parseInt(mes);
                Integer anioInt = Integer.parseInt(cboAnio);

		/* Obtiene los campos dinamicos definidos por la epo. */
		int v=1; String los_campos="", los_valores="";

		//recupero el nombre del archivo
		String archivo  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
		String rutaArchivo = strDirectorioTemp + archivo;
		java.io.File ft = new java.io.File(rutaArchivo);
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea= "";
		int noLinea = 0;

		String csCarcaterEspecial = "N";

		while((linea=br.readLine())!=null){
			linea = linea.replaceAll("\"", ""); 
			csCarcaterEspecial = "N";
			vtd = new VectorTokenizer(linea,"|");
			vecdet = vtd.getValuesVector();
			arma_error = "";
			ok=true;
			ok_fechas=true;
			noLinea++;
			pipesporlinea=0; linea=linea.trim();
			if(linea.length()>0) {
                            for(int n=0; n<linea.length(); n++) {
                                ln=linea.substring(n,n+1);
                                if(ln.equals("|"))
                                pipesporlinea++;
                            }

                            if(Comunes.tieneCaracterControlNoPermitidoEnXML1(linea)){
                                csCarcaterEspecial = "S";
                                arma_error = "Se detectaron caracteres de control no permitidos (excluyendo tabulador (\\t) salto de línea (\\n) y retorno de carro (\\r))";
                                ok = false;
                            }

                            try {
				vt=new VectorTokenizer(linea,"|");
				vecdat=vt.getValuesVector();

				noIntermediario = (vecdat.size()>=1)?(String)vecdat.get(0):"";
				mesPagoG    = (vecdat.size()>=2)?(String)vecdat.get(1):"";
				anioPagoG  = (vecdat.size()>=3)?(String)vecdat.get(2):"";                                
				fecha_limite_E = (vecdat.size()>=4)?(String)vecdat.get(3):"";
                                fecha_limite_R = (vecdat.size()>=4)?(String)vecdat.get(4):"";
				fecha_limite_A = (vecdat.size()>=5)?(String)vecdat.get(5):"";
				fecha_limite_D = (vecdat.size()>=6)?(String)vecdat.get(6):"";
				no_Autorizacion = (vecdat.size()>=7)?(String)vecdat.get(7):"";
				rel_garantias = (vecdat.size()>=8)?(String)vecdat.get(8):"";
				vecdat.removeAllElements();
				noIntermediario = Comunes.quitaComitasSimples(noIntermediario);
				mesPagoG = Comunes.quitaComitasSimples(mesPagoG);
				anioPagoG = Comunes.quitaComitasSimples(anioPagoG);
				fecha_limite_E = Comunes.quitaComitasSimples(fecha_limite_E);
                                fecha_limite_R = Comunes.quitaComitasSimples(fecha_limite_R);
				fecha_limite_A = Comunes.quitaComitasSimples(fecha_limite_A);
				fecha_limite_D = Comunes.quitaComitasSimples(fecha_limite_D);
				no_Autorizacion = Comunes.quitaComitasSimples(no_Autorizacion);
				rel_garantias = Comunes.quitaComitasSimples(rel_garantias);

				log.trace("noIntermediario:"+noIntermediario);
				log.trace("mesPagoG:"+mesPagoG);
				log.trace(" anioPagoG:"+anioPagoG);
				log.trace(" fecha_limite_E:"+fecha_limite_E);
                                log.trace(" fecha_limite_R:"+fecha_limite_R);
				log.trace(" fecha_limite_A:"+fecha_limite_A);
				log.trace(" fecha_limite_D:"+fecha_limite_D);
				log.trace(" no_Autorizacion:"+no_Autorizacion);
				//System.out.println(" rel_garantias:"+rel_garantias);

                            } catch(ArrayIndexOutOfBoundsException aioobe) {
				arma_error = "El layout es incorrecto";
				log.error("Error  aioobe  "+aioobe);
				ok = false;
                            } catch(Exception e){
				log.error("Error  "+e);
				arma_error = "El layout es incorrecto";
				ok = false;
                            }

                            log.trace("arma_error("+ok+").::"+arma_error+"::.");
                            try{
				if("".equals(arma_error)){
					if(!"S".equals(tipoCal)&&(
                                            noIntermediario.trim().equals("")||
                                            mesPagoG.trim().equals("")||
                                            anioPagoG.trim().equals("")||
                                            fecha_limite_E.trim().equals("")||
                                            fecha_limite_R.trim().equals("")||
                                            fecha_limite_A.trim().equals("")||
                                            fecha_limite_D.trim().equals("")||
                                            rel_garantias.trim().equals(""))
                                            ) {
						arma_error = "El layout del archivo de calendario es incorrecto";
						ok = false;
					} else if("S".equals(tipoCal)&&(
                                            noIntermediario.trim().equals("")||
                                            mesPagoG.trim().equals("")||
                                            anioPagoG.trim().equals("")||
                                            fecha_limite_E.trim().equals("")||
                                            fecha_limite_D.trim().equals("")||
                                            rel_garantias.trim().equals(""))
                                            ) {
						arma_error = "El layout del archivo de calendario es incorrecto";
						ok = false;
                                        }
                                        log.trace("arma_error("+ok+").::"+arma_error+"::.");
					if(!Comunes.esNumero(noIntermediario)){
						arma_error = "La clave SIAG debe ser númerica";
						log.debug("entro aqui a val de interm");
						ok = false;
					}else if(!Comunes.esNumero(mesPagoG)){
						arma_error = "El mes de pago debe ser númerico";
						ok = false;
					}else if(!Comunes.esNumero(anioPagoG)){
						arma_error = "El año de pago debe ser númerico";
						ok = false; 
					}else if(!mesPagoG.equals(mes)){
						arma_error = "El mes de pago dentro del calendario en la linea: "+noLinea+", debe ser igual al capturado en pantalla";
						ok = false;
					}else if(!anioPagoG.equals(cboAnio)){
						arma_error = "El año de pago dentro del calendario en la linea: "+noLinea+", debe ser igual al capturado en pantalla";
						ok = false;
					}else if(!Comunes.esFechaValida(fecha_limite_E, "dd/MM/yyyy")){
						arma_error = "La fecha limite de entrega de Expediente no es valida";
						ok = false;
					} else if(!"S".equals(tipoCal)&&!Comunes.esFechaValida(fecha_limite_R, "dd/MM/yyyy")) {
						arma_error = "La fecha limite de entrega de Resultados no es valida";
						ok = false;
                                        } else if(!"S".equals(tipoCal)&&!Comunes.esFechaValida(fecha_limite_A, "dd/MM/yyyy")) {
						arma_error = "La fecha limite de entrega de Aclaraciones no es valida";
						ok = false;
					} else if(!Comunes.esFechaValida(fecha_limite_D, "dd/MM/yyyy")) {
						arma_error = "La fecha limite de dictamen no es valida";
						ok = false;
					}/*else if(rel_garantias.length()>4000){
						arma_error = "El tamaño de la columna de la relacion de las garantias no puede ser mayor a 4000";
						ok = false;
					}*/
					else {
                                            arma_error = supervision.validaCalendario(noIntermediario, fecha_limite_E, fecha_limite_R,fecha_limite_A, fecha_limite_D, no_Autorizacion, rel_garantias, mes, cboAnio, tipoCal);
					}
				}
                                log.trace("arma_error("+ok+").::"+arma_error+"::.");
				if(!"".equals(arma_error)) {
                                    ok = false;
				} else {
                                    if(!supervision.insertaCalendario(noIntermediario, fecha_limite_E, fecha_limite_R, fecha_limite_A, fecha_limite_D, no_Autorizacion, rel_garantias, mes, cboAnio, tipoCal)){
                                        ok = false;
                                    }
				}
				// Totales 
				if(ok)  {
                                
				}
                                //log.trace("ok:"+ok);
                            } catch(NafinException ne) {
				arma_error = ne.getMsgError();
				ok = false;
                            } catch(Exception e) {
				arma_error = "El layout es incorrecto"+e;
				ok = false;
                            }
                            //log.trace("ok:"+ok);
                            ig_nums_doctos.addElement(numero_docto);
                            lineadocs++;
                            if(ok==false) {
				vecColumnasCE = new Vector();
				vecColumnasCE.add(noIntermediario);
				vecColumnasCE.add(arma_error);
				vecColumnasCE.add(mesPagoG);
				vecFilasCE.add(vecColumnasCE);

				String addError = "";
				if("S".equals(csCarcaterEspecial)){
					addError = ","+arma_error;
				}
				contenidoArchivo.append(noIntermediario+","+mesPagoG+","+anioPagoG+","+fecha_limite_E +","+fecha_limite_R+","+fecha_limite_A +","+fecha_limite_D +","+no_Autorizacion+","+rel_garantias+"\n");
                            }//if(ok==false)
                    } //if de linea > 0
		} //while fin de archivo !=null
		lineadocs=0;
                
                //log.trace("vecFilas1:"+vecFilas);
		vecFilas = supervision.getCalInsertado(noIntermediario, mes,cboAnio,tipoCal);
                //log.trace("vecFilas2:"+vecFilas);
                
		vecFilasSE = new Vector();
		String referencia="";

		for(i=0;i<vecFilas.size();i++){
			vecColumnas = (Vector)vecFilas.get(i);
			vecFilasSE.add(vecColumnas);
			/* El Tipo de Moneda */
		} // for
                log.trace("vecFilasSE:"+vecFilasSE);

		String errorCE = "";

                log.trace("vecFilasSE:"+vecFilasSE.size());
                log.trace("vecFilasCE:"+vecFilasCE.size());
		for(int x=0;x<vecFilasSE.size()||x<vecFilasCE.size();x++) {
                    errorCE = "";
                    numDoctoSE              = "";
                    fechaESE                = "";
                    fechaASE                = "";
                    fechaRSE                = "";
                    fechaDSE                = "";
                    MESE                    = "";
                    numDoctoCE              = "";
                    
                    if(x<vecFilasSE.size()) {
                        vecColumnasSE           = (Vector)vecFilasSE.get(x);
                        log.trace("vecColumnasSE:"+vecColumnasSE);
                        numDoctoSE              = (String)vecColumnasSE.get(1);
                        fechaESE                = (String)vecColumnasSE.get(2);
                        fechaRSE                = (String)vecColumnasSE.get(3);
                        fechaASE                = (String)vecColumnasSE.get(4);
                        fechaDSE                = (String)vecColumnasSE.get(5);
                        MESE                    = (String)vecColumnasSE.get(7);
                    }
                    
                    if(x<vecFilasCE.size()) {
                        vecColumnasCE   = (Vector)vecFilasCE.get(x);
                        log.trace("vecColumnasCE:"+vecColumnasCE);
                        numDoctoCE      = (String)vecColumnasCE.get(0);
                        errorCE         = (String)vecColumnasCE.get(1);
                        MESE            = (String)vecColumnasCE.get(2);
                    }
                    
                    datos.put("DESCRIP_SERROR",numDoctoSE);
                    datos.put("FECHA_E_SERROR",fechaESE);
                    datos.put("FECHA_R_Error", fechaRSE);
                    datos.put("FECHA_A_SERROR",fechaASE);
                    datos.put("FECHA_D_SERROR",fechaDSE);
                    datos.put("NUDOCTO_CERROR",numDoctoCE);
                    datos.put("CERROR",errorCE);
                    registros.add(datos);
		}//FOR
		
		String btnConfirmaCarga  ="N";
		//if ((ok_enc) && (rtxttotdoc > 0 || rtxttotdocdol >0)) {
			btnConfirmaCarga ="S";
		//}

                //ABR:: Si el tipo de calendario es Corrección y el archivo tiene más de una línea, se notifica al usuario que no procede la carga del archivo
                if("C".equals(tipoCal) && noLinea>1) {
                    //Es correccion y tiene mas de una linea
                    mensajeCorreccion = "Solo se puede corregir un calendario a la vez.";
                    btnConfirmaCarga ="N";
                    vecFilasCE = new Vector();
                } else if("C".equals(tipoCal) && noLinea==1){
                    //Valido si el IF tiene un calendario ordinario, extraordinario o no tiene calendario para el periodo seleccionado
                    cveSiag = Integer.parseInt(noIntermediario);
                    tipoCalendarioCargado = supervision.validaTipoDeCalendarioACorregir(cveSiag, mesInt, anioInt);
                    if("C".equals(tipoCalendarioCargado)){
                        mensajeCorreccion = "Ya existe un calendario de Correcci&oacute;n para el periodo " + (mesInt<10?"0"+mesInt:mesInt) + "-" + anioInt;
                        btnConfirmaCarga ="N";
                        vecFilasCE = new Vector();
                    } else if("O".equals(tipoCalendarioCargado)){
                        confirmaCorreccion = "¿Est&aacute; seguro de reemplazar el archivo de tipo calendario ORDINARIO correspondiente al periodo " + (mesInt<10?"0"+mesInt:mesInt) + "-" + anioInt +"?";
                    } else if("E".equals(tipoCalendarioCargado)){
                        confirmaCorreccion = "¿Est&aacute; seguro de reemplazar el archivo de tipo calendario EXTEMPORANEO correspondiente al periodo " + (mesInt<10?"0"+mesInt:mesInt) + "-" + anioInt +"?";
                    } else {
                        mensajeCorreccion = "No es posble realizar una correcci&oacute;n porque no existe un calendario para el periodo " + (mesInt<10?"0"+mesInt:mesInt) + "-" + anioInt;
                        btnConfirmaCarga ="N";
                    vecFilasCE = new Vector();
                    }
                }

		String consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);
		resultado.put("NUMERO_PROCESO", String.valueOf(ic_proc_docto));	
		resultado.put("TOTALDOCS",String.valueOf(vecFilasSE.size()));
		resultado.put("MES",MESE);
		resultado.put("ANIO",cboAnio);
		resultado.put("TOTALDOCE", String.valueOf(vecFilasCE.size()));
		resultado.put("ERROR_ENC", error_enc.toString());	
		resultado.put("ARCHIVO_ERRORES", contenidoArchivo.toString());
		resultado.put("MESER",MESE);
		resultado.put("ANIOER",cboAnio);
		resultado.put("btnConfirmaCarga", btnConfirmaCarga);
		resultado.put("MENSAJE_CORRECCION", mensajeCorreccion);
		resultado.put("CONFIRMA_CORRECCION", confirmaCorreccion);

		infoRegresar = resultado.toString();

	} else if(informacion.equals("ACUSE")) {
        
	Seguridad s = new Seguridad();
	Acuse		acuse	= null;
	String		_acuse	= "";
	String folioCert = "";
	char getReceipt = 'Y';
	AccesoDB con = new AccesoDB();
	try {
            con.conexionDB();
            acuse = new Acuse(Acuse.ACUSE_EPO,"4","com",con);
	} catch(Exception e) {
		throw new NafinException("SIST001");
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}

	String externContent = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	String lineabien 		= (request.getParameter("lineabien")==null)?"0":request.getParameter("lineabien").trim();
	String lineaerror 		= (request.getParameter("lineaerror")==null)?"0":request.getParameter("lineaerror").trim();
	String linea	 		= (request.getParameter("linea")==null)?"":request.getParameter("linea").trim();
	String icDoctos	 		= (request.getParameter("icDoctos")==null)?"":request.getParameter("icDoctos").trim();
        String anioPago  = (request.getParameter("anioPago") != null) ? request.getParameter("anioPago") : "0";
        String mesPago  = (request.getParameter("mesPago") != null) ? request.getParameter("mesPago") : "0";
        String tipoCarga  = (request.getParameter("tipoCarga") != null) ? request.getParameter("tipoCarga") : "0";
        String noRegistros  = (request.getParameter("noRegistros") != null) ? request.getParameter("noRegistros") : "0";
        
        
	String strPreacuse	 		= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim();
	String usuario = 	strLogin+" - "+strNombreUsuario;
		
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
    
            folioCert = acuse.toString();
            
            log.trace("_serial "+_serial);
            log.trace("pkcs7 "+pkcs7);
            log.trace("externContent "+externContent);
            log.trace("getReceipt "+getReceipt);
            
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			_acuse = s.getAcuse();			
			
			//actualizar los documentos correctos
                        if("C".equals(tipoCarga)){
                            supervision.updateCalendarioCorreccion(mesPago, anioPago, tipoCarga,acuse.toString(),strLogin, noRegistros);
                        } else {
                            supervision.updateCalendario(mesPago, anioPago, tipoCarga,acuse.toString(),strLogin, noRegistros);
                        }
		}
	
		resultado.put("success", new Boolean(true));
		resultado.put("acuse",acuse.formatear().toString());
		resultado.put("fecha",fechaHoy);	
		resultado.put("hora",horaActual);	
		resultado.put("usuario",usuario.toString());
		resultado.put("recibo",acuse.toString());
		infoRegresar = resultado.toString();	
	
	}
    } else if(informacion.equals("Consulta_Anio")){
		List reg = supervision.getCatalogoAnioI();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		log.trace("infoRegresar ** inicial "+infoRegresar);

    }
        
        


%>
<%
} catch(Exception ex) {
    log.error("La Exception: "+ex.toString()); 
}
%>
<%=infoRegresar%>