<%response.addHeader("X-UA-Compatible", "IE=edge");%>
<!DOCTYPE html>
<%@ page import="java.util.*,
        java.text.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%
        
        String noArchivos = (request.getParameter("noArchivos")!=null)?request.getParameter("noArchivos"):"";
        String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
        String mes = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
        String tipoInformacion = (request.getParameter("tipoInformacion")!=null)?request.getParameter("tipoInformacion"):"";
        
        System.out.println("noArchivos "+ noArchivos);
        
        
	
	String tipoCarga = (request.getParameter("tipoCarga")!=null)?request.getParameter("tipoCarga"):"";
	String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
	String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"P";
	String accionDetalle = (request.getParameter("accionDetalle")!=null)?request.getParameter("accionDetalle"):"N";
	String producto = (request.getParameter("producto")!=null)?request.getParameter("producto"):"";
	
try{
	
	CargaDocDist cargaDocto = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
	
        //cargaDocto.operaDistribuidores(iNoCliente);

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
	
	String hidFechaActual=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
	
	//para ver si hay campos adicionales 
	String campos ="N";
	
	String  mensajeEPO  ="EPO";
	if(application.getInitParameter("EpoCemex").equals(iNoCliente)){
		mensajeEPO ="EpoCemex";
	}



%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="40CargaCalPreAcuse.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	
        <div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	
	<%@ include file="/01principal/01nafin/pie.jspf"%>

<form id='formParametros' name="formParametros">	
	
	<input type="hidden" id="hidFechaActual" name="hidFechaActual" value="<%=hidFechaActual%>"/>
		
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="accionDetalle" name="accionDetalle" value="<%=accionDetalle%>"/>
	<input type="hidden" id="mensajeEPO" name="mensajeEPO" value="<%=mensajeEPO%>"/>
	<input type="hidden" id="tipoCarga" name="tipoCarga" value="<%=tipoCarga%>"/>
	<input type="hidden" id="producto" name="producto" value="<%=producto%>"/>	
        
        <input type="hidden" id="noArchivos" name="noArchivos" value="<%=noArchivos%>"/>
	<input type="hidden" id="anio" name="anio" value="<%=anio%>"/>
	<input type="hidden" id="mes" name="mes" value="<%=mes%>"/>
	<input type="hidden" id="tipoInformacion" name="tipoInformacion" value="<%=tipoInformacion%>"/>	
        
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
<%
}catch(Exception e){
	out.println(" Error: "+e);
}
%>