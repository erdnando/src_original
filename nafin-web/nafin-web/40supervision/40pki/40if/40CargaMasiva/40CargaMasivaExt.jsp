<%response.addHeader("X-UA-Compatible", "IE=edge");%>
<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
        java.text.*,
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="\nafin\00utils\valida.js?&lt;%=session.getId()%>"></script>
<script type="text/javascript" src="40CargaMasiva2.js?&lt;%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../../certificado.jspf" %>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>
        
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
        
     
<form id='formAux' name="formAux" target="blank"></form>
<form id='formParametros' name="formParametros"></form>

</body>
</html>
