<%response.addHeader("X-UA-Compatible", "IE=edge");%>
<%@ page import="
        org.apache.commons.logging.Log,
        java.util.*,
	javax.naming.*,	
        java.text.*,
	java.sql.*,
        javax.ejb.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
        com.nafin.supervision.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
    String user = (String)session.getAttribute("username");
    String fechaActual = null;
    List anioList = new ArrayList(); 
    String primerA = (String)(request.getAttribute("primerA")!=null?request.getAttribute("primerA"):"0");
    log.trace("primerA "+primerA);
%>
 
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script src="/nafin/00utils/00js/uploadFile/js/jquery-1.12.0.min.js"></script>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>

        <style>
            #dropzone {
                    background: white;
                    width: 100%;
                    line-height: 80px;
                    height: 80px;
                    text-align: center;
                    font-weight: bold;
                    color:#999999;
                    border-style: dashed;
                    border-width: 4px;
                    border-color: #e2e2e2;
                    font-size:18px;
                    font-weight:bold;
                    margin-bottom:10px;
                }
                #dropzone.in {
                    height: 200px;
                    line-height: 200px;
                    font-size: larger;
                     color:#555555;
                }
                #dropzone.hover {
                    background: #eeeeee;
                }
                #dropzone.fade {
                    -webkit-transition: all 0.3s ease-out;
                    -moz-transition: all 0.3s ease-out;
                    -ms-transition: all 0.3s ease-out;
                    -o-transition: all 0.3s ease-out;
                    transition: all 0.3s ease-out;
                    opacity: 1;
                }
                
                  #zoneExt {
                    background: #FFFFFF;                    
                }
                
                
                        </style>
        <script language="JavaScript">
          function doShowUploadStats() {
              document.SearchForm.action = "/docman/reportdoc/ReportDocShowPage.jsp";
              document.SearchForm.elements['Action'].value = 'Buscar'
              document.SearchForm.submit();
          }
          
          function preAcuse() {
              document.SearchForm.noArchivos.value = document.fileupload.noArchivos.value;
              document.SearchForm.anio.value = document.fileupload.anio.value;
              document.SearchForm.mes.value = document.fileupload.mes.value;
              document.SearchForm.tipoInformacion.value = document.fileupload.tipoInformacion.value;
              if($('#primerA').val()=="0"){
                alert("No ha cargado ningun archivo");                  
                return false;
              }
              if($('#archivosC').val() != document.SearchForm.noArchivos.value){
                alert("No coinciden el numero de archivos cargados con los capturados");
                return false;
              } 
              document.SearchForm.action = "/nafin/40supervision/40pki/40if/40CargaMasiva/40PreAcuseExt.data.jsp";
                document.SearchForm.elements['Action'].value = 'Buscar'
                document.SearchForm.target = "_parent";
                document.SearchForm.submit();
              
          }
          
          function activaCampos(tipo){
            $('#noArchivos').removeAttr('disabled');
            $('#anio').removeAttr('disabled');
            $('#mes').removeAttr('disabled');
            $('#tipoInformacion').removeAttr('disabled');
            if(tipo == "0"){
                $('#primerA').val("0");
                $('#nombreA').val("");
                $('#archivosC').val("0");
                
            }    
          }
          
          function verificarExtemporanea() {
            var f = document.fileupload;
            var el = document.getElementById("num_acuerdo");
            //alert("Entro verificarExtemporanea");
            if(f.tipoInformacion.options[f.tipoInformacion.selectedIndex].value == "E") {
                //alert("MuestraExtemporane");
                el.style.display = 'block';
            } else {
               el.style.display = 'none';
            }
          }
          
          function Valida_Texto(myfield, e, tipovalida){
            var key;
            var keychar;
            var cadena_valida;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;

            keychar = String.fromCharCode(key);

                if (tipovalida.toString().toLowerCase()=='r') //acepta sólo reales positivos
                        cadena_valida = "0123456789.";
                else if (tipovalida.toString().toLowerCase()=='e') //acepta sólo enteros positivos y letras
                        cadena_valida = "kgjwopbvcñkgjfabcdefghijklmnñopqrstuvwxyz1234567890 ";
                else if (tipovalida.toString().toLowerCase()=='f') //acepta sólo enteros positivos y letras
                        cadena_valida = "0123456789/";
                else if (tipovalida.toString().toLowerCase()=='h') //acepta sólo enteros positivos y letras
                        cadena_valida = "0123456789:.";
                else if (tipovalida.toString().toLowerCase()=='sior') //acepta sólo enteros positivos y letras
                        cadena_valida = "0123456789-";
                else if (tipovalida.toString().toLowerCase()=='n') //acepta sólo enteros positivos y letras
                        cadena_valida = "0123456789";
                else if (tipovalida.toString().toLowerCase()=='np') //acepta sólo enteros positivos y letras
                        cadena_valida = "0123456789-dm";
                else if (tipovalida.toString().toLowerCase()=='fac') //caracteres validos para una factura
                        cadena_valida = "0123456789-kgjwopbvcñkgjfabcdefghijklmnñopqrstuvwxyz";
                else if(tipovalida.toString().toLowerCase()=='obs') {
                    var regExprEsAlfaNumerico = /^([a-z]|[0-9]|[_#&@ñáéíóú.:,-^\s*$])+$/i;
                    if (regExprEsAlfaNumerico.test(keychar.toLowerCase()) || key==8 || key==13 || key==0)
                        return true;
                    else
                        return false;
                }
  // --solo numeros

	  if ((cadena_valida).indexOf(keychar.toLowerCase()) > -1 || key==8 || key==13 || key==0)
	    return true;
	  else
	    return false;

   }
   
   function desactiva(){
    alert("ya netro");
   }
          
          
          
        </script>
		<!-- Bootstrap styles -->
		<!--link rel="stylesheet" href="/docman/js/uploadFile/css/bootstrap.min.css"-->
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="/nafin/00utils/00js/uploadFile/css/jquery.fileupload.css">
		<link rel="stylesheet" href="/nafin/00utils/00js/uploadFile/css/jquery.fileupload-ui.css">
                <link rel="stylesheet" href="/nafin/00utils/00js/uploadFile/css/bootstrapEsp.min.css">
        <!--script src="/nafin/00utils/00js/uploadFile/js/jquery-1.12.0.min.js"></script-->
        	
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="/nafin/00utils/00js/uploadFile/js/vendor/jquery.ui.widget.js"></script>
		<script src="/nafin/00utils/00js/uploadFile/js/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="/nafin/00utils/00js/uploadFile/js/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="/nafin/00utils/00js/uploadFile/js/canvas-to-blob.min.js"></script>
        <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
        <script src="/nafin/00utils/00js/uploadFile/js/bootstrap.min.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload.js"></script>
        <!-- The File Upload processing plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-image.js"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-audio.js"></script>
        <!-- The File Upload video preview plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-video.js"></script>
        <!-- The File Upload validation plugin -->
        <script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-validate.js"></script>
		<script src="/nafin/00utils/00js/uploadFile/js/jquery.fileupload-ui.js"></script>
                <script src="/nafin/00utils/00js/uploadFiles.js"></script>
    <!--/head-->
    <!--body-->
        <%
	 	
	 	try {
			fechaActual = Fecha.getFechaActual();
                        Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
                        anioList = supervision.getCatalogoAnioI();
	 %>

</head>

	<div id="Contcentral">
	<form id="SearchForm" class="SearchForm" name="SearchForm" method="post">
            <input type="HIDDEN" name="Action" value=""/>
            <input type="HIDDEN" name="opcion" value="F"/>
            <input type="HIDDEN" name="dataFormat" value="table"/>
            <input type="HIDDEN" name="usuario" value="<%=user%>"/>
            <input type="HIDDEN" name="FechaIni" value="<%= fechaActual %>"/>
            <input type="HIDDEN" name="FechaFin" value="<%= fechaActual %>"/>
            
            <input type="HIDDEN" name="noArchivos" value=""/>
            <input type="HIDDEN" name="anio" value=""/>
            <input type="HIDDEN" name="mes" value=""/>
            <input type="HIDDEN" name="tipoInformacion" value=""/>          
            
        </form>
         
        <form name="QueryForm" method="post" class="form-inline">
            <input type="HIDDEN" name="Action" value=""/>
            <% 
   
    if(user == null)
        user = "Guest";
%>
        </form>
        <table cellpadding="2" cellspacing="" border="0" width="100%" style="">
            <tbody>
                <tr>
                    <td valign="top" style="color:#254d8b;font-size:12px;" bgcolor="#bbd2eb" height="20px" >
                         <div style="color:#254d8b;font-size:14px;"  ALIGN="center" bgcolor="#92b9d9">
                             <b> Carga Masiva de Expedientes</b>
                         </div>
                 </td>
                 </tr>
              <tr>
                    <td valign="top" style="color:#254d8b;font-size:12px;" bgcolor="White" borderColor="Gray">           
		<form id="fileupload" name="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data" class="form-horizontal">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <div class="zoneExt">
        
        <noscript></noscript>
		<p>
			DATOS DE CARGA
		</p>
                <div class="form-group">
		<label class="col-sm-2" style="width:210px;">
			Numero de Archivos:
		</label>
                <div class="col-sm-4" style="width:15px;">
		<input type="text" name="noArchivos" id="noArchivos" size="5px" onkeypress="return Valida_Texto(this,event,'n');">
                </div>
                </div>
                <input type="hidden" name="primerA" id="primerA" value="0">
                <input type="hidden" name="archivosC" id="archivosC" value="0">
                <input type="hidden" name="nombreA" id="nombreA" value="">
                <div class="form-group">
		<label class="col-sm-2" style="width:210px;">
			A&ntilde;o:
		</label>
                <div class="col-sm-4">
		<select name="anio" id="anio">
<%              for(int i=0;i<anioList.size();i++){
                    ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
                    elementoCatalogo = (ElementoCatalogo)anioList.get(i);%>
                    <option value="<%=elementoCatalogo.getClave()%>"><%=elementoCatalogo.getDescripcion()%></option>
<%              }%>
                </select>
                </div>
                </div>
                
                <div class="form-group">
		<label class="col-sm-2" style="width:210px;">
			Mes:
		</label>
                <div class="col-sm-4">
		<select name="mes" id="mes">
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                    <option value="13">1er Semestre</option>
                    <option value="14">2do Semestre</option>
                </select>
                </div>
                </div>
                
                 <div class="form-group">
		<label class="col-sm-2" style="width:210px;">
			Tipo de Informacion:
		</label>
                <div class="col-sm-4">
		<select name="tipoInformacion" id="tipoInformacion" onchange="javascript:verificarExtemporanea();">
                    <option value="P">Carga Parcial</option>
                    <option value="F">Carga Final</option>
                    <option value="A">Carga Aclaraciones</option>
                    <option value="E">Carga Extemporanea</option>
                    <option value="S">Carga Semestral</option>
                </select>
                </div>
                </div>
                
                <div class="form-group" id="num_acuerdo" style="display: none;">
                    <label class="col-sm-2" style="width:210px;">Numero de Acuerdo:</label>
                    <div class="col-sm-4" style="width:15px;">
                        <input type="text" name="noAcuerdo" id="noAcuerdo" size="5px" onkeypress="return Valida_Texto(this,event,'obs');">
                    </div>
                </div>
                
                <br>
		
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="row fileupload-buttonbar">
            <div class="col-lg-7" align="center">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success btn-sm fileinput-button">
                    <span>Agregar archivos...</span>
                    <input type="file" name="files[]" multiple >
                </span>
                <button type="submit" class="btn btn-primary btn-sm start" onclick="activaCampos(1)">
                    <span>Cargar</span>
                </button>
                <button type="reset" class="btn btn-warning btn-sm cancel" onclick="activaCampos(0)">
                    <span>Cancelar</span>
                </button>
                <button type="button" class="btn btn-primary btn-sm" onclick="preAcuse()">
                    <span>Continuar Carga</span>
                </button>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <div id="dropzone" class="fade"><span>Arrastra tus archivos aqu&iacute;</span></div>
        <table role="presentation" class="table table-striped table-condensed"><tbody class="files"></tbody></table>
        
            
    </form>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td> 
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary btn-sm start" disabled style="display:none">
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning btn-sm cancel">
                    <span>Eliminar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% 
               if (file.success) { %}
                <div><span class="label label-success">Exito</span> {%=file.message%}</div>
                
            {% } else { %}
                <div><span class="label label-danger">Error</span> {%=file.message%}</div>
            {%  }  %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger btn-sm delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning btn-sm cancel" style="display:none">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Eliminar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
    <br>
                        <br/>
                    </td>
                </tr>
            </tbody>
        </table>
        <br/>
        <% }catch(Exception e){ %>
			<br>
			&nbsp;&nbsp;&nbsp;<span style="color:red;">Ocurri&oacute; un error al mostrar la p&aacute;gina.</span>
		<% e.printStackTrace(); %>
	<%	} %>
        
    