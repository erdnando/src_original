
Ext.onReady(function() {
	
	//Variables globales
	
	var proceso  = Ext.getDom('proceso').value;
	var accionDetalle = Ext.getDom('accionDetalle').value;
	var tipoCarga = Ext.getDom('tipoCarga').value;
	
        var anio = Ext.getDom('anio').value;
        var noArchivos = Ext.getDom('noArchivos').value;
        var mes = Ext.getDom('mes').value;
        var tipoInformacion = Ext.getDom('tipoInformacion').value;
	
	
	var procesarCancelar = function(store, arrRegistros, opts) 	{
		document.location.href = "40CargaMasivaExt.jsp?idMenu=40MASIVASUPERV";
		
		}
		
		
	var confirmar = function(pkcs7, vtextoFirmar, vmes, vanio, vtipoInformacion, vrelacionGar, vnoArchivos){
		if (Ext.isEmpty(pkcs7) || pkcs7 == 'error:noMatchingCert') {
			Ext.getCmp("btnConfirmaAcuse").enable();	
			Ext.Msg.show({
					title:'Firmar',
					msg:'Error en el proceso de Firmado. \nNo se puede continuar',
					buttons: Ext.Msg.OK,
					icon: Ext.Msg.ERROR
				});
				return false;
		}else  {
			Ext.getCmp("btnConfirmaAcuse").disable();	
			pnl.add(acusePanel);                        
			pnl.doLayout();                        
			var regreso = "";
            
			Ext.getCmp('gridAcuse').getStore().load({
			params:{
				informacion: 'ConfirmarCertificado', 
				proceso: 'actualizaOperarDescuentoAutomaticoAcuse',
				pkcs7:	pkcs7,
				TextoFirmado: vtextoFirmar,
				mes: vmes,
				anio: vanio,
				tipoInformacion: vtipoInformacion,
				relacionGar    : vrelacionGar,
				noArchivos     : vnoArchivos
			},
			callback: function(records, operation, success){
					if(success){                                                            
					} else {                                                         
						Ext.Msg.show({
							title:'Carga archivos',
							msg:'Error en el proceso de carga de archivos intente cargarlos mas tarde.',
							buttons: Ext.Msg.OK,
							icon: Ext.Msg.ERROR
						});
						acusePanel.hide();                                                           
					}
				}
			});                        
			gridDetallesTotales.hide();
			gridCaptura.hide();
			fpPreAcuse.hide();
			fpCarga.hide();                       
			return;
		}
	}
	
	var procesarConfirmaAcuse =  function(opts, success, response) {
		var  gridDetallesTotales = Ext.getCmp('gridDetallesTotales');
		
		var mes = 0;
                var mes_adic = 0;
		var anio=0;
		var tipoCarga =0;
		var noArchivos =0;
		var relaGarantia=0;		
		
		var store = gridDetallesTotales.getStore();
			store.each(function(record) {
				mes = record.data['MES_PAGO'];
				anio = record.data['ANIO_PAGO'];
                                tipoCarga = record.data['TIPO_CARGA'];
				noArchivos = record.data['NUMERO_ARC'];
				relacionGar = record.data['RELACION_GAR'];
		});
                
                mes_adic = mes;
                if(mes==="13") {
                    mes_adic="1er.Sem.";
                } else if(mes==="14") {
                    mes_adic="2do. Sem.";
                }
                
		var textoFirmar = "Carga de Archivos "+
				" \n No. total de documentos cargados "+noArchivos+
				" \n TipoCarga "+ tipoCarga  +
				" \n Mes "+mes_adic+
				" \n A�o "+anio +"\n"+
                                " \n Relaci�n de Garantias "+relacionGar +"\n"+
				" Para todos los efectos legales al transmitir este 	MENSAJE DE DATOS, me obligo a no modificar ni cancelar o restringir el (los) DOCUMENTO(S) que aqui se menciona(n). \n";
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar,  mes, anio, tipoInformacion, relacionGar, noArchivos);	
		
		
	}
        
        function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fpCarga.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpCarga.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
        
         function procesarRegreso(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
		}else {
			//var infoR = Ext.util.JSON.decode(response.responseText);
			var mensajer = response.mensaje;				
                        
                        NE.util.mostrarConnError(response,opts);
		}
	}
        
        var acusePanel = new Ext.Panel({
			id: 'acusePanel',
			title: 'Recibo n�mero ',
			style: 'margin: 0 auto;',
			width: 600,
			items: [
				{
					xtype: 'fieldset',
					border: true,
					style: 'margin: 0px',
					items: [
						new Ext.grid.EditorGridPanel({
							id: 'gridAcuse',
							store: new Ext.data.JsonStore({
								root: 'registros',
								url:'40CargaMas01Ext.data.jsp',
								fields:[
									{	name: 'TITULO'	            },
									{	name: 'VALOR'	   			}
								],
								totalProperty : 'total',
								messageProperty: 'msg',
								autoLoad: false
							}),
							loadMask:true,	
							frame: false,
							border: true,
							style: 'margin: 10px auto', 
							stripeRows: true,
							columnLines: true,
							enableColumnMove: false,
							enableColumnHide: false,
							height: 170,
                                                        enableColLock: false,
                                                        flex:2,
							columns:[
								{
									dataIndex: 'TITULO',
									sortable: false,
									hideable: false,
									width: 248,
									align: 'left',
									hidden: false,
									renderer: function(v){
										return '<b>' + v + '</b>';
									}
								},
								{
									dataIndex: 'VALOR',
									sortable: false,
									hideable: false,
									width: 310,
									align: 'left',
									hidden: false,
                                                                        scrollable:true,
                                                                        renderer: function(value, metadata, registro, rowindex, colindex, store) {
                                                                            if(registro.get('TITULO') =='Folio de Operacion' ){
                                                                                Ext.getCmp('acuseF').setValue(registro.get('VALOR'))
                                                                                return value;
                                                                            }else {
                                                                                return value;	
                                                                        }
                                                                    }
								}
							]
						})							
					],
                                       buttons:[
                                                {
                                                    text: 'Imprimir PDF', id: 'btnImprimirPdfAcuse', iconCls: 'icoImprimir',
                                                    handler: function(boton, evento) {
                                                    Ext.Ajax.request({
							url: '40CargaMas01Ext.data.jsp',
							params: Ext.apply(fpCarga.getForm().getValues(),{
								informacion: 'GenerarArchivoAcuse',
								acuseF:Ext.getCmp('acuseF').getValue()
							}),
							callback: procesarArchivos
                                                    });
                                                    }//, handler: fnDescargaAcusePdf
                                                },
                                                
                                                {
                                                    text: 'Salir', id: 'btnSalirAcuse', iconCls: 'icoCancelar', handler: function(btn){
                                                    window.location = '40CargaMasivaExt.jsp?idMenu=40MASIVASUPERV';
                                                }   
                                            }
                                        ]
				}
			]
		});
	
	var procesarConsultaDetalle = function(store, arrRegistros, opts) 	{
								
		if (arrRegistros != null) {
			if (!gridDetallesTotales.isVisible()) {
				gridDetallesTotales.show();
			}		
			var jsonData = store.reader.jsonData;
                        
                        noArchivos = jsonData.noArchivosI;			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridDetallesTotales.getColumnModel();	
						
			var btnCancelarP = Ext.getCmp('btnCancelarP');	
			var btnConfirmaAcuse = Ext.getCmp('btnConfirmaAcuse');
			btnCancelarP.show();
			//para habilitar los botones  de  Confirmar Detalle
								
			if(jsonData.habConfirma =='SI' ){ 	btnConfirmaAcuse.show();   	}
					
				
			var el = gridDetallesTotales.getGridEl();		
			if(store.getTotalCount() > 0) {
				el.unmask();	
			} else {								
				el.mask('Para ver los totales favor de capturar detalle de Descuento y/o Factoraje', 'x-mask');				
			}			
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		if (arrRegistros != null) {
			if (!gridCaptura.isVisible()) {
				gridCaptura.show();
			}		
		
			var jsonData = store.reader.jsonData;	
                 
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCaptura.getColumnModel();
                        
                      
				
			
			
			
			
			var el = gridCaptura.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
        
             
		//para los totales del grid Detalles 
	var totalesDetallesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '40CargaMas01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesDetalle'	
		},								
		fields: [			
			{name: 'TIPO_CARGA'},
			{name: 'MES_PAGO'},
			{name: 'ANIO_PAGO'},
			{name: 'NOMBRE_ARC'},
                        {name: 'NUMERO_ARC'},
                        {name: 'RELACION_GAR'},
		],
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {
			load: procesarConsultaDetalle,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaDetalle(null, null, null);						
				}
			}
		}			
	});
	
	//esto esta en duda como manejarlo
	var gruposM = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Archivos Cargados', colspan: 4, align: 'center'}	
			]
		]
	});
	
	var gridDetallesTotales = new Ext.grid.EditorGridPanel({	
  	id: 'gridDetallesTotales',		
		store: totalesDetallesData,
		plugins: gruposM,
		hidden: true,		
		columns: [	
			{
				header: 'Mes de Pago.',
				dataIndex: 'MES_PAGO',
				width: 150,
				align: 'center',
                                renderer: function(value, p, record) {
                                    if((record.get('MES_PAGO'))==='13') {
                                        return '1er. Sem.';
                                    } else if((record.get('MES_PAGO'))==='14') {
                                        return '2do. Sem.';
                                    } else {
                                        return record.get('MES_PAGO');
                                    }
                                }
			},
			{
				header: 'A�o de Pago.',
				dataIndex: 'ANIO_PAGO',
				width: 150,
				align: 'center'			
			},
			{
				header: 'Tipo de Carga.',
				dataIndex: 'TIPO_CARGA',
				width: 150,
				align: 'center' 
			},
			{
				header: 'N�mero Archivos',
				dataIndex: 'NUMERO_ARC',
				width: 150,
				align: 'center'
			},
                        {
				header: 'Relaci�n Garantias',
				dataIndex: 'RELACION_GAR',
				width: 150,
				align: 'LEFT'
			}	
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 150,
		width: 620,	
		style: 'margin:0 auto;',
		frame: true
	});	
	


	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '40CargaMas01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'CONSECUTIVO'},		
			{name: 'MES_PAGO'},
			{name: 'ANIO_PAGO'},
			{name: 'TIPO_CARGA'},			
			{name: 'NOMBRE_ARC'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
				
	var gridCaptura = new Ext.grid.EditorGridPanel({
		id: 'gridCaptura',
		title: 'Carga de Documentos',		
		clicksToEdit: 1,	
		style: 'margin:0 auto;',
		columns: [
		{							
			header : 'Mes de Pago',
			tooltip: 'Mes de Pago',
			dataIndex : 'MES_PAGO',
			width : 150,
			align: 'center',
			sortable : false,
                        renderer: function(value, p, record) {
                            if((record.get('MES_PAGO'))==='13') {
                                return '1er. Sem.';
                            } else if((record.get('MES_PAGO'))==='14') {
                                return '2do. Sem.';
                            } else {
                                return record.get('MES_PAGO');
                            }
                        }
		},
		{							
			header : 'A�o de Pago',
			tooltip: 'A�o de Pago',
			dataIndex : 'ANIO_PAGO',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Tipo de Carga',
			tooltip: 'Tipo de Carga',
			dataIndex : 'TIPO_CARGA',
			width : 150,
			align: 'center',
			sortable : false
		},
		{							
			header : 'Nombre de Archivo',
			tooltip: 'Nombre de Archivo',
			dataIndex : 'NOMBRE_ARC',
			width : 150,
			align: 'left',
			sortable : false
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		}	
		],
		displayInfo: true,
		store: consultaData,
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 150,
		width: 615,
		frame: false
		});
		
	
	//Contenedor para el PreAcuse
	var fpPreAcuse = new Ext.Container({
		layout: 'table',
		id: 'fpPreAcuse',
		//hidden: true,
		layoutConfig: {
			columns: 10
		},
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
                align: 'center',
		items: [	
		{
			xtype: 'button',
			text: 'Transmitir Registros',
			id: 'btnConfirmaAcuse',
			iconCls: 'icoAceptar',
			handler: procesarConfirmaAcuse,	
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelarP',
			hidden: true,
			handler:procesarCancelar
		}		
	]
	});
	
        
        var elementosForma =[
		{ 	xtype: 'textfield', hidden: true, id: 'acuseF', 	value: '' }
	];

	var fpCarga = new Ext.form.FormPanel({
		id: 'forma',
		hidden: true,
		width: 885,
		title: 'Criterios de Captura',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
                items: elementosForma,
		monitorValid: true	
	})
	
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
                renderHidden: 	true,
		width: 949,
		items: [
			fpCarga,
			NE.util.getEspaciador(20),
			gridDetallesTotales,	
			NE.util.getEspaciador(20),
			fpPreAcuse,			
			NE.util.getEspaciador(20),				
			gridCaptura,			
			NE.util.getEspaciador(20)		
			
		]
	});
		
	//Cargando PreAcuse 
	if(proceso!==''){
	
	totalesDetallesData.load({ 		
			params: { 		
				informacion: 'ResumenTotalesDetalle', 
				proceso:proceso,
				accionDetalle:accionDetalle,
                                mes: mes,
                                anio: anio,
                                tipoInformacion: tipoInformacion
			} 		
		});
		
		consultaData.load({  	
			params: { 		
				informacion: 'Consulta', 		
				proceso:proceso,
				pantalla: 'Captura',
                                mes: mes,
                                anio: anio,
                                tipoInformacion: tipoInformacion
			}  		
		});						
	
	}
            
	
} );