<%response.addHeader("X-UA-Compatible", "IE=edge");%>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
        com.nafin.supervision.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf" %>
<%@ include file="../../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String proceso	= (request.getParameter("proceso")==null)?"":request.getParameter("proceso");
String pantalla	= (request.getParameter("pantalla")==null)?"":request.getParameter("pantalla");
//esta parte es para la pantalla de las linea  de Credito
String acuse	= (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String mes	= (request.getParameter("mes")==null)?"":request.getParameter("mes");
String anio	= (request.getParameter("anio")==null)?"":request.getParameter("anio");
String noArchivos	= (request.getParameter("noArchivos")==null)?"":request.getParameter("noArchivos");
String tipoInformacion	= (request.getParameter("tipoInformacion")==null)?"":request.getParameter("tipoInformacion");
String relacionGar = (request.getParameter("relacionGar")==null)?"":request.getParameter("relacionGar");
String acuseF = (request.getParameter("acuseF")==null)?"":request.getParameter("acuseF");
boolean actualizacionExitosa 		= false;

String ses_ic_epo =  iNoCliente;
Calendar fechaEmisPlazo = new GregorianCalendar();

String msgError ="", 
infoRegresar ="", 
DescripcionEstatus ="";
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();	
	
	HashMap  registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();	
	JSONObject 	resultado	= new JSONObject();
	
Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);

ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);

Vector nombresCampo = new Vector(5);

int hayCamposAdicionales = nombresCampo.size();
int numeroCampos=  nombresCampo.size();

/************************************************************************************/
// Catalogos  que se Usan 
/***********************************************************************************/
 if (informacion.equals("CatalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


} else  if (  
		(informacion.equals("Consulta") || informacion.equals("ResumenTotales")) 
		&&  ( pantalla.equals("PreAcuseDespuesdeDetalle") ||  pantalla.equals("Captura") )  
		) {	
	//carga
	int mesPago = 0;
        int anioPago = 0;
        int noArchivosP = 0;
        String tipoCarga = "";
        String nombreArc = "";
        Vector	vecFilas  = supervision.getDoctoPreInsertados(mes,anio,tipoInformacion, iNoUsuario);
        //Vector	vecFilas  = supervision.getDoctoPreInsertados(mes,anio,tipoInformacion, iNoCliente);
	int numElementos = vecFilas.size();	
	for(int i=0;i<vecFilas.size();i++){
		
		
		Vector vecColumnas = (Vector)vecFilas.get(i);
		mesPago = Integer.parseInt(vecColumnas.get(0).toString()); 
                anioPago = Integer.parseInt(vecColumnas.get(1).toString()); 
                tipoCarga = vecColumnas.get(2).toString();
                noArchivosP = Integer.parseInt(vecColumnas.get(3).toString());
                nombreArc = vecColumnas.get(4).toString();
			
     
			
			
			datos = new HashMap();
		
		datos.put("TIPO_CARGA", tipoCarga);			
		datos.put("MES_PAGO", String.valueOf(mesPago));	
		datos.put("ANIO_PAGO", String.valueOf(anioPago) );
		datos.put("NOMBRE_ARC", nombreArc);
						
			if(datos.size()>0){
				registros.add(datos);	
			}			
	} //for
				
	
	
	
	if (informacion.equals("Consulta")) {
		//carga
                String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
		resultado = JSONObject.fromObject(consulta);
		for(int i=0;i<nombresCampo.size();i++){
			Vector detalleCampo = (Vector) nombresCampo.get(i);
			String nombreCampo = (String) detalleCampo.get(0);
			String numCampo = ("Campo"+i);				
			resultado.put(numCampo, nombreCampo);				
		}		
		
		resultado.put("numElementos", String.valueOf(numElementos));
		resultado.put("pantalla", pantalla);	
		resultado.put("strPerfil", strPerfil);
		infoRegresar = resultado.toString();
	
	
	}else if(informacion.equals("ResumenTotales") ) {
		
		 infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registrosT\": " + registrosTotales.toString()+"}";
	
	}
	
/************************************************************************************/
//  Consulta cuando la EPO Opera Credito Cuenta Corriente
/***********************************************************************************/		
	
} else if (informacion.equals("Eliminar")) {	


	//cargaDocto.cancelaCarga(proceso,hidID,ses_ic_epo);

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  Modificación  de la tabla temporal
/***********************************************************************************/	
}else  if (informacion.equals("ResumenTotalesDetalle")  ) {	
	System.out.println("entro carga");
	int		auxDM	= 0, 	totalDocDM 	= 0, 	totalDocConvDM	= 0,	totalDocDMUSD		= 0, 	totalDocCC = 0, 	totalDocConvCC = 0, 	totalDocCCUSD		= 0;
	int mesPago = 0;
        int anioPago = 0;
        int noArchivosP = 0;
        String tipoCarga = "";
        String nombreArc = "";
        double	montoTotalDM		= 0 , 	montoTotalConvDM	= 0, 	montoCDescDM		= 0, 	montoCDescConvDM	= 0,  montoCDescDMUSD		= 0,   montoCDescCC		= 0,
	montoCDescConvCC	= 0, montoCDescCCUSD		= 0, montoTotalDMUSD		= 0, montoTotalCC		= 0, 	montoTotalConvCC	= 0, 	montoTotalCCUSD 	= 0,
	interesTotalDM 		= 0, 	interesTotalDMUSD 	= 0, interesTotalCC 		= 0, interesTotalCCUSD 	= 0;
	//String tipoCambio =  cargaDocto.getTipoCambio(ses_ic_epo);	
	
	JSONArray registrosT = new JSONArray();	
	String tipo_Credito ="";
	Vector 	vecFilas = supervision.getDoctoPreInsertados(mes,anio,tipoInformacion,iNoUsuario);	
		/*
                F.ARCT_MES_PAGO ARCT_MES_PAGO,\n" + 
                                                            "       F.ARCT_ANIO_PAGO ARCT_ANIO_PAGO,\n" + 
                                                            "       F.ARCT_TIPO_CARGA ARCT_TIPO_CARGA,\n" + 
                                                            "       F.ARCT_NO_ARCHIVOS ARCT_NO_ARCHIVOS,\n" + 
                                                            "       F.ARCT_NOMBRE ARCT_NOMBRE,\n" + 
                                                            "       F.ARCT_EXTENSION ARCT_EXTENSION\n" +
                
                */
	int noArchivosI = 0;
        String relGarantias = "";
        for(int i=0;i<vecFilas.size();i++){
		Vector vecColumnas = (Vector)vecFilas.get(i);
		mesPago = Integer.parseInt(vecColumnas.get(0).toString()); 
                anioPago = Integer.parseInt(vecColumnas.get(1).toString()); 
                tipoCarga = vecColumnas.get(2).toString();
                noArchivosP = Integer.parseInt(vecColumnas.get(3).toString());
                nombreArc = vecColumnas.get(4).toString();
                relGarantias += vecColumnas.get(6).toString();
                noArchivosI++;
	}
			
		
	
		datos = new HashMap();
		tipo_Credito ="Descuento";
		datos.put("TIPO_CARGA", tipoCarga);			
		datos.put("MES_PAGO", String.valueOf(mesPago));	
		datos.put("ANIO_PAGO", String.valueOf(anioPago) );
		datos.put("NOMBRE_ARC", nombreArc);
                datos.put("NUMERO_ARC", noArchivosI);
		datos.put("RELACION_GAR", relGarantias);	
	
	if(datos.size()>0){
		registrosT.add(datos);
	}
	
	//esto es para saber si se habilita la el boton de confirmar  en el preacuse
	String habConfirma  = "NO";
	
	habConfirma= "SI";
	
	String consulta =  "{\"success\": true, \"total\": \"" + registrosT.size() + "\", \"registros\": " + registrosT.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	
	resultado.put("habConfirma", habConfirma);
        resultado.put("noArchivosI",noArchivosI); 
	
	infoRegresar = resultado.toString();


/************************************************************************************/
// Monitor de las Lineas de Credito
/***********************************************************************************/

}  else if(informacion.equals("ConfirmarCertificado")){
	
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
							
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("pkcs7");
		String folioCert = "";
		String externContent = "";
		char getReceipt = 'Y';
		String cc_acuse = "";
		JSONObject jsonObj   =  new JSONObject();
                
        externContent = (request.getParameter("TextoFirmado") != null) ? request.getParameter("TextoFirmado") : "";
        pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
        String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
        String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
        String usuario = 	strLogin+" - "+strNombreUsuario;
						
		// ----- Autentificación -----
		Seguridad s = new Seguridad();
		Acuse acuse2 = new Acuse(Acuse.ACUSE_IF,"1");
		//if(true){
        //if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse2.toString();
			
            System.out.println("_serial "+_serial);
            System.out.println("pkcs7 "+pkcs7);
            System.out.println("externContent "+externContent);
            System.out.println("getReceipt "+getReceipt);
            
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				System.out.println("folio Cert "+folioCert);
				cc_acuse = s.getAcuse();
				
				String 	mensaje 		= 	null;
				String 	icoMensaje 	= 	null;
				String	cveUsuario	=	iNoCliente;
				
                                
                                String rootPath = getServletConfig().getServletContext().getRealPath("/");
                                
				actualizacionExitosa = supervision.confirmaCarga(mes,  anio,  tipoInformacion,  noArchivos, strLogin ,  relacionGar,  cc_acuse, rootPath, iNoCliente);
                                
					
				//actualizacionExitosa 	= 	parametros.actualizaOperarDescuentoAutomatico(operarDsctoAut, clavePyme, strLogin,"S");					
				
				
																
				if(actualizacionExitosa){
					mensaje = "Los Datos se han guardado con éxito ...";
					icoMensaje = "INFO";	
				}else{
					mensaje = "Ocurrio un problema al guardar la parametrización ...";
					icoMensaje = "ERROR";		
				}
				
                                System.out.println("entro aqui a lista 1");
                                
				List listaHoraFecha = new ArrayList();
                                listaHoraFecha.add(fechaHoy);
                                listaHoraFecha.add(horaActual);
				
				String usuario_autorizacion = strLogin + " - " + strNombreUsuario;
				HashMap hm = null;
				List lista = new ArrayList();
								                                   
				String tipoInf = "";
				if(tipoInformacion.equals("P"))
					tipoInf = "Carga Parcial";
				else if(tipoInformacion.equals("F"))
					tipoInf = "Carga Final";
				else if(tipoInformacion.equals("A"))
					tipoInf = "Carga Aclaraciones";
				else if(tipoInformacion.equals("E"))	
					tipoInf = "Carga Extemporanea";
				
				 System.out.println("entro aqui a lista 2 "+relacionGar);
				
                                hm = new HashMap();
				hm.put(	"TITULO", 				"Tipo de Carga"	);
				hm.put(	"VALOR", 				tipoInf	);
				lista.add(hm);
                                
                                hm = new HashMap();
				hm.put(	"TITULO", 				"No. Total de Archivos transmitidos"	);
				hm.put(	"VALOR", 				noArchivos	);
				lista.add(hm);
                                
                                hm = new HashMap();
				hm.put(	"TITULO", 				"No. Credito de Garantia"	);
				hm.put(	"VALOR", 				relacionGar	);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Folio de Operacion"		);
				hm.put(	"VALOR", 				cc_acuse					);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Fecha y Hora de carga"	);
				hm.put(	"VALOR", 				listaHoraFecha.get(0) + " " + listaHoraFecha.get(1)	);
				lista.add(hm);
				
				hm = new HashMap();
				hm.put(	"TITULO", 				"Usuario"	);
				hm.put(	"VALOR", 				usuario_autorizacion			);
				lista.add(hm);
				
				jsonObj.put("success",   new Boolean(actualizacionExitosa));
				jsonObj.put("mensaje",   mensaje);
				jsonObj.put("icoMensaje", icoMensaje);
				jsonObj.put("registros", lista);
			
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("mensaje", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
				jsonObj.put("cc_acuse", cc_acuse);
			}
		}
                infoRegresar = jsonObj.toString();
                System.out.println(infoRegresar);
	}
        
        
        else if (informacion.equals("GenerarArchivoAcuse")){
        //acuse
            JSONObject jsonObj   =  new JSONObject();
            SupervisionConsulta paginador = new SupervisionConsulta();
            paginador.setPantalla("Acuse");	
            paginador.setAcuse(acuseF);
            CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
            String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
            jsonObj.put("success", new Boolean(true)); 
            jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
            infoRegresar = jsonObj.toString();

        }


%>
<%=infoRegresar%>



