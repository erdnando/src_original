var getPdf;
Ext.onReady(function() {

//--------------- VARIABLES GLOBALES --------------------

	var dt        = Date();
	var anio      = Ext.util.Format.date(dt,'Y');

//--------------- FUNCIONES Y PROCESOS --------------------

	//Genera archivos pdf a partir del folio de operaci�n en el grid Resultados de la consulta.
	getPdf = function(folioOperacion) {
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '40consultaSupervisionExt.data.jsp',
			params: Ext.apply({
				informacion: 'PDF_CARGA_DE_DOCUMENTOS',
				folioOperacion: folioOperacion
			}),
			callback: descargaArchivo
		});
	};

	//Descarga archivos pdf generados a partir del folio de operaci�n en el grid Resultados de la consulta.
	function descargaArchivo(opts, success, response) {
		var fp = Ext.getCmp('contenedorPrincipal');
		fp.el.unmask();
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.mensajeErr !== '') {
				Ext.Msg.alert('Error...', infoR.mensajeErr);
			} else {
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fpCarga.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fpCarga.getForm().getEl().dom.submit();
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Funci�n para descargar archivos del visor de detalles.
	function procesaDetalles(nombreArchivo, acuse) {
		Ext.Ajax.request({
			url: '40consultaSupervisionExt.data.jsp',
			params: Ext.apply({
				informacion:   'DESCARGA_ARCHIVOS_VISOR',
				nombreArchivo: nombreArchivo,
				acuse:         acuse
			}),
			callback: descargaArchivo
		});
	}

	//Proceso del grid Resultados de la Consulta
	var procesarSuccessValidaCarga = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('formaCarga');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridResulCarga.isVisible()) {
				gridResulCarga.show();
			}
			var el = gridResulCarga.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	//Proceso del grid Detalle Doctos
	var procesarConsDetalleDoctosData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('formaCarga');
		fp.el.unmask();
		if(arrRegistros != null){
			var gridDetalleDoctos = Ext.getCmp('gridDetalleDoctos');
			if (!gridDetalleDoctos.isVisible()) {
				gridDetalleDoctos.show();
			}
			var el = gridDetalleDoctos.getGridEl();
			if(store.getTotalCount() <= 0){
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	};

	//Proceso del grid Avisos de Supervisi�n
	var procesarConsDetalleAvisosData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('formaCarga');
		fp.el.unmask();
		if(arrRegistros != null){
			var gridDetalleAvisos = Ext.getCmp('gridDetalleAvisos');
			if (!gridDetalleAvisos.isVisible()) {
				gridDetalleAvisos.show();
			}
			var el = gridDetalleAvisos.getGridEl();
			if(store.getTotalCount() <= 0){
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	};

	// Visor que muestra el detalle de los documentos cargados.
	var verDetalle = function(grid, rowIndex, colIndex, item, event) {
        console.log(item);
		var registro = grid.getStore().getAt(rowIndex);
		consDetalleDoctosData.load({
			params: {
				informacion:    'CONSULTA_ARCHIVOS_CARGADOS',
				mes:            registro.get('MES'),
				anio:           registro.get('ANIO'),
				interFin:       registro.get('CVE_SIAG'),
				tipoCarga:      registro.get('TIPO_CARGA'),
				acuse:          registro.get('FOLIO_OPERACION'),
                                garantia:       Ext.getCmp('txtGarantia').getValue()
			}
		});
		var verDetalle = Ext.getCmp('verDetalle');
		if(verDetalle){
			verDetalle.show();
		}else{
			verDetalle = new Ext.Window({
				id:              'verDetalle',
				layout:          'fit',
				closeAction:     'hide',
				width:           525,
				height:          400,
				resizable:       false,
				closable:        false,
				modal:           true,
				items: [
					gridDetalleDoctos
				],
				bbar: {
					xtype:       'toolbar',
					buttonAlign: 'center',
					buttons: [{
						xtype:   'button',
						text:    'Cerrar',
						iconCls: 'icoLimpiar', 
						id:      'btnCerraP',
						handler: function(){
							Ext.getCmp('verDetalle').hide();
						}
					}]
				}
			}).show().setTitle('Detalle Doctos');
		}
	};

	// Visor que muestra el detalle de los avisos de supervisi�n.
	var verDetalleAvisos = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		consDetalleAvisosData.load({
			params: {
				informacion:    'CONSULTA_AVISOS_SUP',
				mes:            registro.get('MES'),
				anio:           registro.get('ANIO'),
				interFin:       registro.get('CVE_SIAG'),
				tipoCalendario: registro.get('TIPO_CALENDARIO'),
				consAviso:      registro.get('CONSULTA_AVISO_SUP')
			}
		});
		var verDetalleAvisos = Ext.getCmp('verDetalleAvisos');
		if(verDetalleAvisos){
			verDetalleAvisos.show();
		}else{
			verDetalleAvisos = new Ext.Window({
				id:              'verDetalleAvisos',
				layout:          'fit',
				closeAction:     'hide',
				width:           525,
				height:          400,
				resizable:       false,
				closable:        false,
				modal:           true,
				items: [
					gridDetalleAvisos
				],
				bbar: {
					xtype:       'toolbar',
					buttonAlign: 'center',
					buttons: [{
						xtype:   'button',
						text:    'Cerrar',
						iconCls: 'icoLimpiar', 
						id:      'btnCerraDet',
						handler: function(){
							Ext.getCmp('verDetalleAvisos').hide();
						}
					}]
				}
			}).show().setTitle('Avisos de supervisi�n');
		}
	};
	
	var confirmar = function(pkcs7, vtextoFirmar, vidArchivo, vnombreArchivo, vcveSiag, vcalMes, vcalAnio, vtipoCalendario ){
		if (Ext.isEmpty(pkcs7)) {
			Ext.MessageBox.alert('Error', 'Error en la firma. Termina el proceso');
			return; //Error en la firma. Termina el proceso...
		} else {
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '40consultaSupervisionExt.data.jsp',
				params : {
					pkcs7:          pkcs7,
					textoFirmado:   vtextoFirmar,
					idArchivo:      vidArchivo,
					nombreArchivo:  vnombreArchivo,
					interFin:       vcveSiag,
					calMes:         vcalMes,
					calAnio:        vcalAnio,
					tipoCalendario: vtipoCalendario,
					informacion:    'DESCARGA_ARCHIVOS_SUPERVISION'
				},
				callback: descargaArchivo
			});
		}
	}

	//Antes de descargar un archivo, se tiene que obtener el folio del certificado digital
	function procesaFirma(idArchivo, nombreArchivo, cveSiag, calMes, calAnio, tipoCalendario) {

		var textoFirmar = '';
		if(idArchivo === 'DICTAMEN_SUPERVISION') {
			textoFirmar = 'Est� descargando el archivo Dictamen de Supervisi�n';
		} else if(idArchivo === 'RESULTADOS_SUPERVISION') {
			textoFirmar = 'Est� descargando el archivo Resultados de Supervisi�n';
		} else {
			textoFirmar = '';
		}
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar, idArchivo, nombreArchivo, cveSiag, calMes, calAnio, tipoCalendario );	
		
	}

//--------------- STORES --------------------
	var catalogoAnio = new Ext.data.JsonStore({
		id:       'catAnio',
		root:     'registros',
		url:      '40consultaSupervisionExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_ANIO'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoMes = new Ext.data.JsonStore({
		id:       'catMes',
		root:     'registros',
		url:      '40consultaSupervisionExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_MES'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var resultadosCargaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '40consultaSupervisionExt.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_RESULTADOS'
		},
		fields: [
			{name: 'MES_PAGO',               mapping: 'MES_PAGO'              },
			{name: 'TIPO_CARGA',             mapping: 'TIPO_CARGA'            },
			{name: 'TIPO_CALENDARIO',        mapping: 'TIPO_CALENDARIO'       },
			{name: 'FOLIO_OPERACION',        mapping: 'FOLIO_OPERACION'       },
			{name: 'NO_EXPEDIENTES',         mapping: 'NO_EXPEDIENTES'        },
			{name: 'FECHA_HORA',             mapping: 'FECHA_HORA'            },
			{name: 'FECHA_LIMITE',           mapping: 'FECHA_LIMITE'          },
			{name: 'MES',                    mapping: 'MES'                   },
			{name: 'ANIO',                   mapping: 'ANIO'                  },
			{name: 'CVE_SIAG',               mapping: 'CVE_SIAG'              },
			{name: 'ARCHIVOS_CARGADOS',      mapping: 'ARCHIVOS_CARGADOS'     },
			{name: 'AVISO_SUPERVISION',      mapping: 'AVISO_SUPERVISION'     },
			{name: 'RESULTADOS_SUPERVISION', mapping: 'RESULTADOS_SUPERVISION'},
			{name: 'DICTAMEN_SUPERVISION',   mapping: 'DICTAMEN_SUPERVISION'  },
			{name: 'CONSULTA_AVISO_SUP',     mapping: 'CONSULTA_AVISO_SUP'    },
			{name: 'CAL_DOC_SOLICITUD',      mapping: 'CAL_DOC_SOLICITUD'     },
			{name: 'CAL_DOC_DICTAMEN',       mapping: 'CAL_DOC_DICTAMEN'      },
			{name: 'PROGRESO',       mapping: 'PROGRESO'      },
			{name: 'PROGRESOCOMPLETO',       mapping: 'PROGRESOCOMPLETO'      }
                        
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarSuccessValidaCarga,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					var fp = Ext.getCmp('formaCarga');
					fp.el.unmask();
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarSuccessValidaCarga(null, null, null);
				}
			}
		}
	});

	var consDetalleDoctosData = new Ext.data.JsonStore({
		root: 'registros',
		url: '40consultaSupervisionExt.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_ARCHIVOS_CARGADOS'
		},
		fields: [
			{ name: 'NOMBRE_ARCHIVO'},
			{ name: 'RELACION_GAR'  },
			{ name: 'ACUSE'         },
			{ name: 'CARGADOS'      }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalleDoctosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalleDoctosData(null, null, null);
				}
			}
		}
	});

	var consDetalleAvisosData = new Ext.data.JsonStore({
		root: 'registros',
		url: '40consultaSupervisionExt.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_AVISOS_SUP'
		},
		fields: [
			{ name: 'CLAVE_SIAG'      },
			{ name: 'MES_PAGO'        },
			{ name: 'ANIO_PAGO'       },
			{ name: 'FECHA_LIMITE_EXP'},
			{ name: 'FECHA_LIMITE_ACL'},
			{ name: 'FECHA_LIMITE_DIC'},
			{ name: 'TIPO_CALENDARIO' },
			{ name: 'NUM_GARANTIAS'   },
			{ name: 'REL_GARANTIAS'   }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalleAvisosData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalleAvisosData(null, null, null);
				}
			}
		}
	});

//--------------- GRID DETALLE AVISOS--------------------
	var gridDetalleAvisos = {
		xtype:         'grid',
		id:            'gridDetalleAvisos',
		title:         '',
		store:         consDetalleAvisosData,
		height:        300,
		width:         450,
		hidden:        false,
		stripeRows:    true,
		loadMask:      true,
		frame:         true,
		columns: [{
			header:    'Clave SIAG',
			tooltip:   'Clave SIAG',
			dataIndex: 'CLAVE_SIAG',
			align:     'center',
			width:     90
		},{
			header:    'A�o de pago del calendario',
			tooltip:   'A�o de pago del calendario',
			dataIndex: 'ANIO_PAGO',
			align:     'center',
			width:     190
		},{
			header:    'Mes de pago del calendario',
			tooltip:   'Mes de pago del calendario',
			dataIndex: 'MES_PAGO',
			align:     'center',
			width:     190,
                        renderer:   function(value, p, record) {
                                        if((record.get('MES_PAGO'))==='13') {
                                            return '1er. Sem.';
                                        } else if((record.get('MES_PAGO'))==='14') {
                                            return '2do. Sem.';
                                        } else {
                                            return record.get('MES_PAGO');
                                        }
                                    }
		},{
			header:    'Fecha l�mite de entrega del expediente',
			tooltip:   'Fecha l�mite de entrega del expediente',
			dataIndex: 'FECHA_LIMITE_EXP',
			align:     'center',
			width:     220
		},{
			header:    'Fecha l�mite de entrega de aclaraciones',
			tooltip:   'Fecha l�mite de entrega de aclaraciones',
			dataIndex: 'FECHA_LIMITE_ACL',
			align:     'center',
			width:     230
		},{
			header:    'Fecha l�mite de entrega de dictamen',
			tooltip:   'Fecha l�mite de entrega de dictamen',
			dataIndex: 'FECHA_LIMITE_DIC',
			align:     'center',
			width:     210
		},{
			header:    'Tipo de calendario',
			tooltip:   'Tipo de calendario',
			dataIndex: 'TIPO_CALENDARIO',
			align:     'center',
			width:     150
		},{
			header:    'REL_GARANTIAS',
			dataIndex: 'REL_GARANTIAS',
			hidden:    true,
			width:     150
		},{
			xtype:     'actioncolumn',
			header:    'Relaci�n de garant�as',
			dataIndex: 'NUM_GARANTIAS',
			align:     'center',
			width:     150,
			renderer: function(value, metadata, registro, rowindex, colindex, store) {
				return registro.get('NUM_GARANTIAS');
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					if(registro.get('NUM_GARANTIAS') !== '') {
						return 'iconoLupa';
					}
				},
				handler: function(grid, rowIndex, colIndex, item, event) {
					var registro = grid.getStore().getAt(rowIndex);
					Ext.Ajax.request({
						url: '40consultaSupervisionExt.data.jsp',
						params: Ext.apply({
							informacion: 'PDF_AVISO_DE_SUPERVISION',
							interFin:    registro.get('CLAVE_SIAG'),
							cboAnio:     registro.get('ANIO_PAGO'),
							cboMes:      registro.get('MES_PAGO'),
							relGarant:   registro.get('REL_GARANTIAS')
						}),
						callback: descargaArchivo
					});
				}
			}]
		}]
	};

//--------------- GRID DETALLE DOCUMENTOS CARGADOS --------------------

	var gridDetalleDoctos = {
		xtype:         'grid',
		id:            'gridDetalleDoctos',
		title:         '',
		store:         consDetalleDoctosData,
		height:        360,
		width:         320,
		hidden:        false,
		stripeRows:    true,
		loadMask:      true,
		frame:         true,
		columns: [{
			header:    'Nombre Archivo',
			tooltip:   'Nombre Archivo',
			dataIndex: 'NOMBRE_ARCHIVO',
			align:     'left',
			width:     200,
			sortable:  true,
			resizable: true
		},{
			header:    'Relacion de Garantias',
			tooltip:   'Relacion de Garantias',
			dataIndex: 'RELACION_GAR',
			align:     'left',
			width:     200,
			sortable:  true,
			resizable: true
		},{
			header:    'ACUSE',
			tooltip:   'acuse',
			dataIndex: 'ACUSE',
			hidden:    true
		},{
			xtype:     'actioncolumn',
			header:    'Archivo',
			tooltip:   'Archivo Cargado',
			dataIndex: 'CARGADOS',
			align:     'center',
			width:     95,
			sortable:  true,
			resizable: true,
			hideable:  false,
			hidden:    false,
			renderer:  function(value, metadata, record, rowindex, colindex, store) {
				return value+'&nbsp;&nbsp;';
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					this.items[0].tooltip = 'Ver';
					return 'iconoLupa';
				},
				handler:function(grid, rowIndex, colIndex, item, event) {
					var registro = grid.getStore().getAt(rowIndex);
					var nombreArchivo = registro.get('NOMBRE_ARCHIVO');
					var acuse = registro.get('ACUSE');
					procesaDetalles(nombreArchivo, acuse);
				}
			}]
		}]
	};

//--------------- GRID RESULTADOS DE LA CONSULTA --------------------
	var gridResulCarga = new Ext.grid.EditorGridPanel({
		id:            'gridResulCarga',
		margins:       '20 0 0',
		style:         'margin:0 auto;',
		align:         'center',
		title:         'Resultados de Consulta',
		emptyMsg:      'No hay registros.',
		store:         resultadosCargaData,
		clicksToEdit:  1,
		height:        200,
		width:         943,
		frame:         false,
		hidden:        true,
		displayInfo:   true,
		loadMask:      true,
		stripeRows:    true,
		columns: [{
			header:    'Mes de pago de garant�a',
			dataIndex: 'MES_PAGO',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Tipo de carga',
			dataIndex: 'TIPO_CARGA',
			align:     'center',
			width:     115,
			resizable: true
		},{
			header:    'Tipo de calendario',
			dataIndex: 'TIPO_CALENDARIO',
			align:     'center',
			width:     120,
			resizable: true
		},{
			header:    'Folio de la operaci�n',
			dataIndex: 'FOLIO_OPERACION',
			align:     'center',
			width:     130,
			resizable: true,
			renderer: function(value, p, record) {
				return '<a href="#" onclick="javascript:getPdf('+value+');"><font color="blue">'+ value+'<\/font><\/a>';
			}
		},{
			header:    'No. de expedientes recibidos',
			dataIndex: 'NO_EXPEDIENTES',
			align:     'center',
			width:     160,
			resizable: true
		},{
			header:    'Fecha y hora de la operaci�n',
			dataIndex: 'FECHA_HORA',
			align:     'center',
			width:     160,
			resizable: true
		},{
			header:    'Fecha l�mite de recepci�n',
			dataIndex: 'FECHA_LIMITE',
			align:     'center',
			width:     155,
			resizable: true
		},{
			header:    'A�o',
			dataIndex: 'ANIO',
			align:     'center',
			width:     70,
			resizable: true
		},{
			header:    'Mes',
			dataIndex: 'MES',
			align:     'center',
			width:     70,
			resizable: true,
                        renderer:   function(value, p, record) {
                                        if((record.get('MES'))==='13') {
                                            return '1er. Sem.';
                                        } else if((record.get('MES'))==='14') {
                                            return '2do. Sem.';
                                        } else {
                                            return record.get('MES');
                                        }
                                    }
		},{
			header:    'CVE SIAG',
			dataIndex: 'CVE_SIAG',
			align:     'center',
			width:     90,
			resizable: true
		},{
			header:    'ARCHIVOS_CARGADOS',
			dataIndex: 'ARCHIVOS_CARGADOS',
			hidden:    true
		},{
			header:    'AVISO_SUPERVISION',
			dataIndex: 'AVISO_SUPERVISION',
			hidden:    true
		},{
			header:    'RESULTADOS_SUPERVISION',
			dataIndex: 'RESULTADOS_SUPERVISION',
			hidden:    true
		},{
			header:    'DICTAMEN_SUPERVISION',
			dataIndex: 'DICTAMEN_SUPERVISION',
			hidden:    true
		},{
			header:    'CONSULTA_AVISO_SUP',
			dataIndex: 'CONSULTA_AVISO_SUP',
			hidden:    true
		},{
			header:    'CAL_DOC_SOLICITUD',
			dataIndex: 'CAL_DOC_SOLICITUD',
			hidden:    true
		},{
			header:    'CAL_DOC_DICTAMEN',
			dataIndex: 'CAL_DOC_DICTAMEN',
			hidden:    true
		},{
			xtype:     'actioncolumn',
			header:    'Archivos Cargados',
			tooltip:   'Archivos Cargados',
			align:     'center',
			width:     150,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					if(registro.get('ARCHIVOS_CARGADOS')>0) {
						return 'iconoLupa';
					}
				},
				handler: verDetalle
			}]
		},{
			xtype:     'actioncolumn',
			header:    'Avisos Supervisi�n',
			tooltip:   'Avisos Supervisi�n',
			align:     'center',
			width:     150,
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					return 'iconoLupa';
				},
				handler: verDetalleAvisos
			}]
		},{
			xtype:     'actioncolumn',
			header:    'Resultados Supervisi�n',
			tooltip:   'Resultados Supervisi�n',
			align:     'center',
			width:     150,
			renderer: function(value, metadata, registro, rowindex, colindex, store) {
                                if(registro.get('TIPO_CALENDARIO') === 'Semestral') {
                                        return 'No Aplica';
                                } else if(registro.get('RESULTADOS_SUPERVISION') === '') {
					return 'Pendiente';
				} else {
					return '';
				}
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					if(registro.get('RESULTADOS_SUPERVISION') !== '') {
						return 'iconoLupa';
					}
				},
				handler:function(grid, rowIndex, colIndex, item, event) {
					var registro       = grid.getStore().getAt(rowIndex);
					var nombreArchivo  = registro.get('CAL_DOC_SOLICITUD');
					var cveSiag        = registro.get('CVE_SIAG');
					var calMes         = registro.get('MES');
					var calAnio        = registro.get('ANIO');
					var tipoCalendario = registro.get('TIPO_CALENDARIO');
					procesaFirma('RESULTADOS_SUPERVISION', nombreArchivo, cveSiag, calMes, calAnio, tipoCalendario);
				}
			}]
		},{
			xtype:     'actioncolumn',
			header:    'Dictamen Supervisi�n',
			tooltip:   'Dictamen Supervisi�n',
			align:     'center',
			width:     150,
			renderer: function(value, metadata, registro, rowindex, colindex, store) {
				if(registro.get('DICTAMEN_SUPERVISION') === '') {
					return 'Pendiente';
				} else {
					return '';
				}
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
					if(registro.get('DICTAMEN_SUPERVISION') !== '') {
						return 'iconoLupa';
					}
				},
				handler:function(grid, rowIndex, colIndex, item, event) {
					var registro = grid.getStore().getAt(rowIndex);
					var nombreArchivo = registro.get('CAL_DOC_DICTAMEN');
					var cveSiag = registro.get('CVE_SIAG');
					var calMes = registro.get('MES');
					var calAnio = registro.get('ANIO');
					var tipoCalendario = registro.get('TIPO_CALENDARIO');
					procesaFirma('DICTAMEN_SUPERVISION', nombreArchivo, cveSiag, calMes, calAnio, tipoCalendario);
				}
			}]
		},
                {
			xtype:     'actioncolumn',
			header:    'Garantias',
			tooltip:   'Garant�as',
			align:     'center',
			width:     150,
			renderer: function(value, metadata, registro, rowindex, colindex, store) {                        
                            var informacion = registro.get("PROGRESOCOMPLETO");
                            var data = [
                                ['Complementos', informacion.relacionComplementos.join("|"),informacion.totalComplementos],
                                ['Garant�as', informacion.relacionGarantias.join("|") ,informacion.totalGarantias]
                            ];                           
                            storeGarantiasComplementos.loadData(data);                            
                            return registro.get("PROGRESO") + '&nbsp;';
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
                                    return 'iconoLupa';	
				},
				handler:function(grid, rowIndex, colIndex, item, event) {
					var verDetalleCarga = Ext.getCmp('verDetalleCarga');
		                    if (verDetalleCarga) {
                                        verDetalleCarga.show();
                                    }
                                    else {
                                        var verDetalleCarga = new Ext.Window( {
                                            id : 'verDetalleCarga', layout : 'fit', 
                                            //closeAction:     'hide',
                                            width : 450,
                                            height : 250,
                                            resizable : false,
                                            closable : true,
                                            modal : true,
                                            items :  [
                                                {
                                                    xtype:         'grid',
                                                    id:            'gridDetalleGarantias',
                                                    title:         '',
                                                    store:         storeGarantiasComplementos,
                                                    height:        150,
                                                    width:         150,
                                                    hidden:        false,
                                                    stripeRows:    true,
                                                    loadMask:      false,
                                                    frame:         false,
                                                    columns: [
                                                    {
                                                            header:    'Tipo',
                                                            tooltip:   'Tipo',
                                                            dataIndex: 'tipo',
                                                            align:     'left',
                                                            width:     100,
                                                            height:    400,
                                                            sortable:  true,
                                                            resizable: true
                                                    },
                                                    {
                                                            header:    'Relacion',
                                                            tooltip:   'Relacion',
                                                            dataIndex: 'relacion',
                                                            align:     'left',
                                                            width:     200,
                                                            height:    400,
                                                            sortable:  true,
                                                            resizable: true
                                                    },
                                                    {
                                                            header:    'Total',
                                                            tooltip:   'Total',
                                                            dataIndex: 'total',
                                                            align:     'left',
                                                            width:     100,
                                                            sortable:  true,
                                                            resizable: true
                                                    }
                                                    ]
                                                }                                            
                                            ],
                                            bbar :  {
                                                xtype : 'toolbar',
                                                buttonAlign : 'center',
                                                buttons : [
                                                    {
                                                        xtype : 'button',
                                                        text : 'Cerrar',
                                                        iconCls : 'icoLimpiar',
                                                        id : 'btnCerraP',
                                                        handler : function () {
                                                            Ext.getCmp('verDetalleCarga').hide();
                                                        }
                                                    }
                                                ]
                                            }
                                        }).show().setTitle('Detalle carga de garant�as');
                                    }
				}
			}]
		}
                ]
	});
        
//***********************************       
var gc = Ext.data.Record.create([    
    {name: 'tipo'},
    {name: 'relacion'},
    {name: 'total'}
]);

var storeGarantiasComplementos = new Ext.data.Store({
    reader: new Ext.data.ArrayReader(
        {
            idIndex: 0
        },
        gc
    )
});






//--------------- FORM PRINCIPAL --------------------

	var fpCarga = new Ext.form.FormPanel({
		id:                     'formaCarga',
		title:                  'Criterios de Consulta',
		style:                  'margin:0 auto;',
		bodyStyle:              'padding: 6px',
		defaultType:            'textfield',
		width:                  500,
		labelWidth:             100,
		frame:                  true,
		defaults: {
			msgTarget:          'side'
		},
		items: [{
			xtype:              'compositefield',
			id:                 'mesAnio',
			columns:            4,
			combineErrors:      false,
			items:[{
				xtype:          'combo',
				fieldLabel:     'A�o',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hanio',
				id:             'cboAnio',
				mode:           'local',
				hiddenName:     'A�o',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				margins:        '0 10 0 0',
				value:          anio,
				store:          catalogoAnio,
				width:          135,
				minChars:       1,
				allowBlank:     false,
				hidden:         false,
				forceSelection: true,
				typeAhead:      true,
				editable:       false,
				listeners: {
					select: {
						fn: function(combo) {
							var cmbMes = Ext.getCmp('cboMes');
							cmbMes.setValue('');
							cmbMes.store.load({
								params: {
									cboAnio:combo.getValue()
								}
							});
						}
					}
				}
			},{
				xtype:          'displayfield',
				value:          '&nbsp;&nbsp;&nbsp; Mes:',
				width:          60
			},{
				xtype:          'combo',
				msgTarget:      'side',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hmes',
				id:             'cboMes',
				mode:           'local',
				hiddenName:     'Mes',
				emptyText:      'Seleccione...',
				store:          catalogoMes,
				minChars:       1,
				width:          135,
				editable:       false,
				typeAhead:      true,
				forceSelection: true
			},{
				xtype:          'displayfield',
				value:          '&nbsp;',
				width:          15
			}]
		},
                {
			xtype:              'compositefield',
			id:                 'busquedaGarantia',
			columns:            2,
			combineErrors:      false,
                        items:[
                            {
				xtype:          'textfield',
                                fieldLabel:     'N�mero de garant�a',
				msgTarget:      'side',
				displayField:   'descripcion',
				name:           'grantia',
				id:             'txtGarantia',
				mode:           'local',
				hiddenName:     'txtGarantia',
				emptyText:      'Ej. A120',
				minChars:       1,
				width:          135,
				editable:       true,
				typeAhead:      true,
				forceSelection: true
			}
                        ]
                }
                ],
		buttons: [{
			xtype:              'button',
			text:               'Consultar',
			id:                 'btnBuscar',
			iconCls:            'icoBuscar',
			width:              75,
			handler: function(boton, evento) {
				var errores = 0;
				if(Ext.isEmpty(Ext.getCmp('cboAnio').getValue())) {
					Ext.getCmp('cboAnio').markInvalid('Debe seleccionar un a�o');
					errores++;
				}
				if(Ext.isEmpty(Ext.getCmp('cboMes').getValue())) {
					Ext.getCmp('cboMes').markInvalid('Debe seleccionar un mes');
					errores++;
				}
				if(errores > 0) {
					return;
				}
				fpCarga.el.mask('Procesando...', 'x-mask-loading');
				resultadosCargaData.load({
					params: {
						informacion: 'CONSULTA_RESULTADOS',
						mes:         Ext.getCmp('cboMes').getValue(),
						cboAnio:     Ext.getCmp('cboAnio').getValue(),
                                                txtGarantia:     Ext.getCmp('txtGarantia').getValue()
					}
				});
			},
			style: {
				marginBottom: '10px'
			}
		},{
			xtype:              'button',
			text:               'Limpiar',
			id:                 'btnLimpiar',
			iconCls:            'icoLimpiar',
			width:              75,
			handler: function(boton, evento) {
				Ext.getCmp('cboMes').setValue('');
				Ext.getCmp('cboAnio').setValue(anio);
                                Ext.getCmp('txtGarantia').setValue('');
				Ext.getCmp('gridResulCarga').hide();
			},
			style: {
				marginBottom: '10px'
			}
		}]
	});

	var pnl = new Ext.Container({
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin:0 auto;',
		width:   949,
		items: [
			NE.util.getEspaciador(20),
			fpCarga,
			NE.util.getEspaciador(20),
			gridResulCarga,
			NE.util.getEspaciador(20)
		]
	});
        


//--------------- CARGA INICIAL --------------------
	catalogoAnio.load();
	catalogoMes.load({
		params: {
			informacion: 'CATALOGO_MES',
			cboAnio:     anio
		}
	});

});