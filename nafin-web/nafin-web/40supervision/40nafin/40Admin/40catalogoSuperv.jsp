<!DOCTYPE  HTML> 
<%@ page 
	contentType=
		"text/html;charset=windows-1252"
	import=
		"  java.text.*, 
			java.util.*, 		
			com.netro.exception.*, 
			com.netro.dispersion.*"			
	errorPage=
		"/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession.jspf" %>
<html>
<head>
	<title>Nafi@net </title>
	<%@ include file="/extjs.jspf" %>
	  <%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
		<script type="text/javascript" src="40catalogoSuperv.js?<%=session.getId()%>"></script>
        <style>
            .x-grid3-row-checker, .x-grid3-hd-checker {
                width: 35%;
            }
        </style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>

<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>

<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>