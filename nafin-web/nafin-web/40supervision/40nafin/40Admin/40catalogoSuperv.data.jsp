<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.seguridadbean.*,
	com.netro.electronica.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.electronica.*,
	netropology.utilerias.*,
	com.netro.seguridad.*, 
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	com.netro.cotizador.*,
        com.nafin.supervision.*,
	com.netro.model.catalogos.*,  
        org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"  
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="";

OperacionElectronica opeElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
String claveIF		          = (request.getParameter("cbo_if")!=null?request.getParameter("cbo_if"):"");
String claveIF2                 = (request.getParameter("claveIF")!=null?request.getParameter("claveIF"):"");

Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);

if(informacion.equals("CatalogoIF")){
	
	List lista = new ArrayList();
	//lista.add("NB");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setCampoLlave("cs_tipo");
	cat.setValoresCondicionIn(lista);
    cat.setCondicionesAdicionales("IC_IF_SIAG IS NOT NULL");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
}else if(informacion.equals("CatalogoPromo")){
	JSONArray jsObjArray = new JSONArray();
        List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
        //List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("EJE PROMO", "N");
        List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveIF2, "I");
        if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
                        if(loginUsuarioOPOP!=null){
                            Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);                        
                             log.info(usuarioEpo.getPerfil());
                             
                            if("IF SUP GARANT".equals(usuarioEpo.getPerfil())){
                                String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
                                hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
                                hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
                                lstCatalogoEjecOP.add(hmData);
                            }
                        }
		}
   }
             
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
   		
}else if(informacion.equals("CatalogoEjecutivo")){
	JSONArray jsObjArray = new JSONArray();
        List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
      
        List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("EJEC SUPGARANT", "N");
                
        if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
                        if(loginUsuarioOPOP!=null){
                            Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
                            if("EJEC SUPGARANT".equals(usuarioEpo.getPerfil())){
                                String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
                                hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
                                hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
                                lstCatalogoEjecOP.add(hmData);
                            }
                        }
		}
   }
             
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
		
}else if(informacion.equals("catalogoSupervisor")){
JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
   List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUP SEG", "N");
                
   if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);		                                        
			lstCatalogoEjecOP.add(hmData);
		}
   }
                                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
		
} else if(informacion.equals("catalogoIFOperador")){
	
JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
	
	String[] listaIF	  = request.getParameterValues("listaIF");	
	List listsIF 	= Arrays.asList(listaIF);
		System.err.println("IF: "+listsIF);
	String operador = (request.getParameter("operador") != null) ? request.getParameter("operador") : "";
	String autorizadorUno = (request.getParameter("autorizadorUno") != null) ? request.getParameter("autorizadorUno") : "";

		ArrayList 	listaUsuarios 	= new ArrayList();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		Iterator itIFS = listsIF.iterator();
		String miPerfil="";

		while(itIFS.hasNext()){
			List cuentas = utilUsr.getUsuariosxAfiliado( (String)itIFS.next(), "I");			
			Iterator itCuentas = cuentas.iterator();
			while (itCuentas.hasNext()) {
				netropology.utilerias.usuarios.Usuario oUsuario = null;
				String login = (String) itCuentas.next();
                                oUsuario = oUtilUsr.getUsuario(login);
				if("IF LI".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				}
			}
		}
		
   List usuariosLI = listaUsuarios;
	
	List listaAux =null;
	
		listaAux= listaUsuarios;
		if(listaAux!=null && listaAux.size()>0){
			HashMap hmData = null;
			for(int i=0;i<listaAux.size();i++){
				hmData = new HashMap();
				String loginUsuarioOPOP = (String)listaAux.get(i);
                              	Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
				String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();
				if(operador.equals("")){
					hmData.put("clave", loginUsuarioOPOP);
					hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
					lstCatalogoEjecOP.add(hmData);
				} else {
					if(autorizadorUno.equals("")){
						if(loginUsuarioOPOP.equals(operador)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					} else {
						if(loginUsuarioOPOP.equals(operador) || loginUsuarioOPOP.equals(autorizadorUno)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					}
				}
			}
		}
	 
    if(!autorizadorUno.equals("")){
		HashMap hmData = new HashMap();
		hmData.put("clave", "0");
		hmData.put("descripcion", "No Aplica");
		lstCatalogoEjecOP.add(hmData);
	 }  
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
    
    
}else if(informacion.equals("catalogoepo") )	{

	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setBancofondeo("1");
	infoRegresar = catalogo.getJSONElementos();

     
	 
} else if(informacion.equals("CargarDatos")){
	
	
	String consulta = "";
	JSONObject resultado = new JSONObject();

	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cmp = (request.getParameter("cmp") != null) ? request.getParameter("cmp") : "";
	
	CargaIFOE  carga = new CargaIFOE();
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( carga );
	
	carga.setCve_if(cve_if);
	carga.setValida(cmp);
	
	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		 
	}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
	}
	
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();



	
}else if(informacion.equals("guardar")) {
  JSONObject 		jsonObj = new JSONObject();
  String claveIFcb	= (request.getParameter("claveIF")	!=null)	?	request.getParameter("claveIF")	:	"";
  String correoNaf	= (request.getParameter("correoNaf")	!=null)	?	request.getParameter("correoNaf")	:	"";
  String correoEpo	= (request.getParameter("correoEpo")	!=null)	?	request.getParameter("correoEpo")	:	"";
  String cveSIAG        = (request.getParameter("cveSIAG")	!=null)	?	request.getParameter("cveSIAG")	:	"";
  String principalNaf = (request.getParameter("principalNaf")	!=null)	?	request.getParameter("principalNaf")	:	"";
  String principalEpo = (request.getParameter("principalEpo")	!=null)	?	request.getParameter("principalEpo")	:	"";
  String cN;
  String cE;
  if (correoNaf.equals("")){
     cN=null;
  } else {
     cN = correoNaf.substring(0,correoNaf.length()-1);
  }
  if (correoEpo.equals("")){
     cE = null;
  } else {
     cE = correoEpo.substring(0,correoEpo.length()-1); 
  }  
  try {
     
     supervision.setParametrosDeSupervision(claveIFcb,cN,cE, cveSIAG, principalNaf, principalEpo);
    
  jsonObj.put("success"	,  new Boolean(true));
  jsonObj.put("guardar"	,  new Boolean(true));
  } catch (Exception e){
    System.err.println("eRROr::"+e);
    jsonObj.put("success"	,  new Boolean(false));
    jsonObj.put("guardar"	,  new Boolean(false));
  }
  
  infoRegresar = jsonObj.toString(); 
  
}else if(informacion.equals("dispersion")) {
	List 				registros = new ArrayList();
	List 				tarifaNac = new ArrayList();
	List 				tarifaDol = new ArrayList();
	HashMap			beanResult= new HashMap();
	JSONObject 		jsonObj = new JSONObject();
	jsonObj.put("borrar"	,  new Boolean(false)); 
    beanResult.put("claveIF"		,claveIF);
	Iterator 	it      = null;
	String    v1      ="";
	String    v2      ="";
	String    v	      ="";  
	int sizeTarifaNac =0;
        String cveSIAG = "";
	UtilUsr 		utilUsr 			= new UtilUsr();
  HashMap parametrosDispersion = supervision.getParametrosDeSupervision(claveIF);
  HashMap correoNafin = new HashMap();
  List cN = new ArrayList();
  int sizeCuentasNafin = 0;
  cveSIAG = (String)parametrosDispersion.get("CVE_SIAG");
  String contactoPrincipal = (String)parametrosDispersion.get("SOCIO_PRINCIPAL");
  String ejecutivoPrincipal = (String)parametrosDispersion.get("EJECUTIVO_PRINCIPAL");
  String [] cadena = (String[])parametrosDispersion.get("CUENTAS_SOCIOS");
  try {
    for(int i=0;i<cadena.length;i++){
      correoNafin = new HashMap();
      Usuario usuarioEpo = utilUsr.getUsuario(cadena[i]);
      String nombre = cadena[i] +" - "+ usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
      correoNafin.put("clave",cadena[i]);
      correoNafin.put("descripcion",cadena[i]);
      if (cadena[i].equals(contactoPrincipal)){
            correoNafin.put("principal","S");
        }
      else{
        correoNafin.put("principal","N");
      }
      correoNafin.put("valor",nombre);
      cN.add(correoNafin);
      sizeCuentasNafin++;
      }
  } catch(Exception e){
    log.debug("No hay correos");
  }
  String sizeCN = String.valueOf(sizeCuentasNafin);
  beanResult.put("sizeCN",sizeCN); 
  beanResult.put("CUENTAS_SOCIOS",cN);
  
  HashMap correoEpo = new HashMap();
  List cE = new ArrayList();
  int sizeCuentasEpo = 0;
  String [] cadenaEpo = (String[])parametrosDispersion.get("CUENTAS_EJECUTIVOS");
  try {
    for(int i=0;i<cadenaEpo.length;i++){
      if(cadenaEpo[i]!=null){ 
        Usuario usuarioEpo = utilUsr.getUsuario(cadenaEpo[i]);
        String nombre = cadenaEpo[i] +" - "+ usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
        correoEpo = new HashMap();
        correoEpo.put("clave",cadenaEpo[i]);
        correoEpo.put("descripcion",cadenaEpo[i]);
      if (cadenaEpo[i].equals(ejecutivoPrincipal)){
        correoEpo.put("principal","S");
        }
      else{
        correoEpo.put("principal","N");
      }
        correoEpo.put("valor",nombre);
        cE.add(correoEpo);
        sizeCuentasEpo++;
      }
      }
  } catch(Exception e){
  log.debug("No hay correos");
  }
  try{
    if (cveSIAG == null || cveSIAG.length()<1){ 
        CatalogoSimple cat = new CatalogoSimple();
        cat.setTabla("comcat_if");
        cat.setCampoClave("IC_IF_SIAG");
        cat.setCampoDescripcion("cg_razon_social"); 
        cat.setCondicionesAdicionales("IC_IF = " + claveIF);
        List listaIF = cat.getListaElementos();
        cveSIAG = ((ElementoCatalogo)listaIF.get(0)).getClave();
    }
  }
  catch(Exception e){
    log.error("Error al buscar la clave SIAG "+e);
  }
  beanResult.put("cveSIAG",cveSIAG);
  String sizeCE = String.valueOf(sizeCuentasEpo);
  beanResult.put("sizeCE",sizeCE); 
  beanResult.put("CUENTAS_EJECUTIVOS",cE);
  
	registros.add(beanResult);
        
        jsonObj.put("success"	,  new Boolean(true)); 
	jsonObj.put("registros", registros);
    
  infoRegresar = jsonObj.toString();
}


%>

<%=infoRegresar%>