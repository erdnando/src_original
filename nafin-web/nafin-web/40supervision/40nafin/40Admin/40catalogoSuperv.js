Ext.onReady(function(){

  var sizeCN;
  var sizeCE;
  var mN;
  var dA;
  var cargado=0;
  var fila1=false;
  var fila2=false;
  var fila3=false;
  var fila4=false;
  var fila5=false;
  var fila6=false;
  	
    var consultaData = new Ext.data.JsonStore ({		
		root:'registros',	
		url:'40catalogoSuperv.data.jsp',	
		totalProperty: 'total',		
		fields: [	
			{name: 'nombre_producto'}, 
			{name: 'ic_producto'}, 					
			{name: 'dispersion'},
			{name: 'dispNoNeg'},
			{name: 'producto'}
		],
		messageProperty: 'msg',
		autoLoad: false,
		listeners: { exception: NE.util.mostrarDataProxyError	}		
	});
	
	
	
//-----------------------------respuestaGuardar------------------------------
function respuestaGuardar(opts, success, response) {
  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      Ext.Msg.show({
          title: 'Guardar',
          msg: 'La informaci�n se almacen� con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){
               //window.location  = "/nafin/15cadenas/15mantenimiento/15dispersion/15tarifaepoEXT.jsp "; 
              }
          });
      Ext.Ajax.request({
      url: '40catalogoSuperv.data.jsp',
      params: Ext.apply(fp.getForm().getValues(), {
        informacion: 'dispersion'
        }),
      callback:procesarInformacion 								
      });
     fila1=false;
     fila2=false;
     fila3=false;
     fila4=false;
     fila5=false;
     fila6=false;
     dA=false;
     mN=false;
      
  } else {
    NE.util.mostrarConnError(response,opts);
     fila1=false;
     fila2=false;
     fila3=false;
     fila4=false;
     fila5=false;
     fila6=false;
  }
};
//---------------------------Fin respuestaGuardar----------------------------

//*******************************************************  procesarGuardar
var procesarGuardar = function() {
  cargado = 1;
  var sN = "";
  var cveSIAG = Ext.getCmp('cveSIAG').getValue();
  var contactoPrincipalNaf = "";
  var contactoPrincipalEpo = "";
  if (!Ext.getCmp('cveSIAG').isValid()){
        alert('Debe Capturar Clave SIAG');
        return;
    }
  /** Validar contacto principal seleccionado */
    var arrContactoPrincipalNafin = smContactosNafin.getSelections();
    if (arrContactoPrincipalNafin.length == 0) {
        alert("Debes seleccionar un Contacto principal");
        return;
    }
    else{
        contactoPrincipalNaf = arrContactoPrincipalNafin[0].data.clave;
    }
    var arrContactoPrincipalEpo = smContactosEpo.getSelections();
    if (arrContactoPrincipalEpo.length == 0) {
        alert("Debes seleccionar un Ejecutivo principal");
        return;
    }
    else{
        contactoPrincipalEpo = arrContactoPrincipalEpo[0].data.clave;
    }
  /** */
  
  var grid = Ext.getCmp('gridNafin');
  var store = grid.getStore();  
  store.each(function(record) {
    sN = sN + record.data['descripcion']+",";
  });
  
  var sEp = "";
  var gridEp = Ext.getCmp('gridEpo');
  var storeEp = gridEp.getStore();  
  storeEp.each(function(record) {
    sEp = sEp + record.data['descripcion']+",";
  });
  if (cargado == 1){
  Ext.Ajax.request({
    url: '40catalogoSuperv.data.jsp',
    params: {
      informacion: 'guardar',
      claveIF : Ext.getCmp('_cbo_if').getValue(),
      correoNaf: sN,
      correoEpo: sEp,
      cveSIAG  : cveSIAG,
      principalNaf: contactoPrincipalNaf, 
      principalEpo: contactoPrincipalEpo
    },
    callback: respuestaGuardar
    });
  }
}
//*******************************************************  procesarGuardar

var catalogoPromo = new Ext.data.JsonStore({
		id: 'catalogoPromo',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '40catalogoSuperv.data.jsp',
		baseParams: {
			informacion: 'CatalogoPromo'
                        ,claveIF : Ext.getCmp('cbo_if')!=null?Ext.getCmp('cbo_if').getValue():""                        
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	})



var catalogoEjecutivo = new Ext.data.JsonStore({
		id: 'catalogoEjecutivo',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '40catalogoSuperv.data.jsp',
		baseParams: {
			informacion: 'CatalogoEjecutivo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
			//beforeload: NE.util.initMensajeCargaCombo
		}		
	})




//-----------------------------procesarInformacion------------------------------
	function procesarInformacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
       sizeCN          =info.registros[0].sizeCN;
       sizeCE          =info.registros[0].sizeCE;
       var cveSIAG     = info.registros[0].cveSIAG;
      
       Ext.getCmp('cveSIAG').setValue(cveSIAG);
			
      //Store Correo Nafin
      var tamCN = parseInt(sizeCN,10);
      if (tamCN > 0){
        var miData = new Array();
        for ( x =0 ; x < tamCN; x++){
          var registro = [];
          registro.push(info.registros[0].CUENTAS_SOCIOS[x].descripcion);
          registro.push(info.registros[0].CUENTAS_SOCIOS[x].clave);
          registro.push(info.registros[0].CUENTAS_SOCIOS[x].valor);
          registro.push(info.registros[0].CUENTAS_SOCIOS[x].principal);
          miData.push(registro);
          }
        var grid = Ext.getCmp('gridNafin');
        var store = grid.getStore();
        store.loadData(miData);
      } else {
          var grid = Ext.getCmp('gridNafin');
          var store = grid.getStore();
          store.loadData('');
      } // FIN Store Correo Nafin

      //Store Correo Epo
      var tamCE = parseInt(sizeCE,10);
      if (tamCE > 0){
        var miData = new Array();
        for ( x =0 ; x < tamCE; x++){
          var registro = [];
          registro.push(info.registros[0].CUENTAS_EJECUTIVOS[x].descripcion);
          registro.push(info.registros[0].CUENTAS_EJECUTIVOS[x].clave);
          registro.push(info.registros[0].CUENTAS_EJECUTIVOS[x].valor);
          registro.push(info.registros[0].CUENTAS_EJECUTIVOS[x].principal);
          miData.push(registro);
          }       
          var grid = Ext.getCmp('gridEpo');
          var store = grid.getStore();
          store.loadData(miData);
      } else {
          var grid = Ext.getCmp('gridEpo');
          var store = grid.getStore();
          store.loadData('');
      } // FIN Store Correo Epo
      
      
      
      //catalogoPromo.load();
      Ext.getCmp('cbo_user_promo').clearValue();
      Ext.getCmp('cbo_ejec_promo').clearValue();
      
      var misIF = Ext.getCmp('_cbo_if').getValue();

      catalogoPromo.load({params		:{
											informacion	:'CatalogoPromo',
											claveIF		:misIF
										},
										callback : function(record,options,success){
											//fpDatos.el.unmask();
										}
									});
      
      catalogoEjecutivo.load();
    if (info.borrar){
      Ext.Msg.show({
          title: 'Eliminar',
          msg: 'La informaci�n se elimin� con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){}
          });
    }
		} else {
				NE.util.mostrarConnError(response,opts);			
		}
	}
//---------------------------Fin procesarInformacion2----------------------------

//-------------------------Store contactosNafin---------------------------------
	var contactosNafin = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' },
              {name : 'valor' },
              {name : 'principal'}
              ]
		});
//--------------------------Fin Store contactosNafin----------------------------


//-------------------------Store correoEpo---------------------------------
	var correoEpo = new Ext.data.ArrayStore({
     autoDestroy: true,
     storeId: 'myStore',
     idIndex: 0, 
		 fields		: [
              {name : 'descripcion'},
              {name : 'clave' },
              {name : 'valor' },
              {name : 'principal'}
              ]
		});
//--------------------------Fin Store correoEpo----------------------------

var catalogoIF = new Ext.data.JsonStore({
	   id				: 'catIFData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '40catalogoSuperv.data.jsp',
		baseParams	: { informacion: 'CatalogoIF'},
		autoLoad		: false,
		listeners	:
		{
			//load		: procesarIF,
			exception: NE.util.mostrarDataProxyError
		}
	});

// ------------------ CheckboxSelectionModel Contactos NAFIN -----------------
	var smContactosNafin = new Ext.grid.CheckboxSelectionModel({
		header: 'Principal',
        singleSelect: true,
		id:'chkContactosNafin',
		tooltip: 'Contacto principal',
		width: 70
	});

// ------------------ CheckboxSelectionModel EJECUTIVOS ----------------------
	var smContactosEpo = new Ext.grid.CheckboxSelectionModel({
		header: 'Principal',
        singleSelect: true,
		id:'chksmContactosEpo',
		tooltip: 'Contacto principal',
		width: 70
	});    

// ---------------- GRID Contactos NAFIN ---------------------------------
var gridNafin = new Ext.grid.GridPanel({
		id: 'gridNafin',
		store: contactosNafin,
		height: 150,
		width: 475,
        sm: smContactosNafin,
        enableColumnHide: false,
		columns: [
                smContactosNafin,
				{
				header: 'Clave y Nombre',
				//tooltip: 'Descripci�n',
				width : 400 ,
				dataIndex: 'valor',
				sortable: false,
				resizable: false,
                hideable:false,
				align: 'left'
			}
		],
    listeners : {
        cellclick: function(grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex);  // Get the Record
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                    var data = record.get(fieldName);
                    Ext.getCmp('validaNafin').setValue(rowIndex);
                },
        afterrender: function() {
            var me = this;
            contactosNafin.on('load', function(){
                var data = me.getStore().data.items;
                var recs = [];
                Ext.each(data, function(item, index){
                    if (item.data.principal == "S") {
                        recs.push(index);
                    }
                });
                me.getSelectionModel().selectRows(recs);
            })
        }
        
    }
	});

// ---------------- GRID EJECUTIVOS  ---------------------------------
var gridEpo = new Ext.grid.GridPanel({
		id: 'gridEpo',
		store: correoEpo,
		height: 150,
		width: 475,
        sm: smContactosEpo,
        enableColumnHide: false,
		columns: [
                smContactosEpo,
				{
				header: 'Clave y Nombre',
				//tooltip: 'Descripci�n',
				width : 400 ,
				dataIndex: 'valor',
				sortable: false,
				resizable: false,
                hideable:false,
				align: 'left'
			}
		],listeners : {
        cellclick: function(grid, rowIndex, columnIndex, e) {
                    var record = grid.getStore().getAt(rowIndex);  // Get the Record
                    var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
                    var data = record.get(fieldName);                
                    Ext.getCmp('validaEpo').setValue(rowIndex);
                },
        afterrender: function() {
            var me = this;
            correoEpo.on('load', function(){
                var data = me.getStore().data.items;
                var recs = [];
                Ext.each(data, function(item, index){
                    if (item.data.principal == "S") {
                        recs.push(index);
                    }
                });
                me.getSelectionModel().selectRows(recs);
            })
        }                
    }
	});
//



//-------------------------------Elementos Forma--------------------------------
	var  elementosForma =  [
		{
		xtype				: 'combo',
		name				: '_cbo_if',
		hiddenName		: 'cbo_if',
		id					: '_cbo_if',	
		fieldLabel		: 'I. F. ', 
		mode				: 'local',	
		emptyText		: 'Seleccione un IF',
		forceSelection  : true,	
		triggerAction 	: 'all',	
		typeAhead		: true,
		minChars 		: 1,
		tpl				: NE.util.templateMensajeCargaCombo,
		store 			: catalogoIF,
		displayField	: 'descripcion',
		valueField 		: 'clave',
		listeners		: {
			select		: {
				fn			: function(combo) {
								
                          
                Ext.getCmp('es10').show();
                Ext.getCmp('es11').show();
                Ext.getCmp('es12').show();
                Ext.getCmp('es13').show();                
                Ext.getCmp('es14').show();
                Ext.getCmp('es15').show();
                Ext.getCmp('es16').show();
                Ext.getCmp('esC1').show();
				
						Ext.Ajax.request({
							url: '40catalogoSuperv.data.jsp',
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'dispersion'
							}),
							callback:procesarInformacion 								
						});
												
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(), {
								informacion: 'ConsultaProducto'
								
							})						
						});
						
				
					}
				}
			}
		},		
		{
		xtype: 'compositefield',
		combineErrors: false,
		msgTarget: 'side',
    hidden : true,
    id:'es10',
		items: [
			{
			xtype: 'displayfield',
			value:'',
			width : 200
			},{
			xtype: 'button',
			text:'Guardar',
			iconCls:'icoGuardar',
			id: 'btnGuardar',
			handler: function() {
                            procesarGuardar();
				}//fin handlear guardar
			},{
			xtype: 'button',
			text:'Limpiar',
			iconCls:'icoLimpiar',
			id: 'btnLimpiar',
			handler: function() {
                        window.location  = "/nafin/40supervision/40nafin/40Admin/40catalogoSuperv.jsp";
				}//fin handlear limpiar
			}]
		},
                 {  xtype: 'compositefield',
                        combineErrors: false,
                        //msgTarget: 'side',
                        hidden : true,
                        id:'esC1',
		items: [
                 
                 {
                    xtype: 'textfield',
                    id: 'cveSIAG',
                    width : 100,
                    fieldLabel: 'No. SIAG',
                    allowBlank: false,
                    readOnly: true,
                    disabled: true
                    }]
                 },
                
                
                {
                        xtype: 'displayfield',
			value:'Contactos',
			width : 200,
                        hidden : true,
                        id:'es11'
                },
                {
                        xtype: 'compositefield',
                        combineErrors: false,
                        //msgTarget: 'side',
                        hidden : true,
                        id:'es12',
		items: [
                    {
			xtype				: 'combo',
                        id					: 'cbo_user_promo',
			hiddenName 		: 'cbo_user_promo',
			fieldLabel		: 'Contacto',
			emptyText: 'Seleccione...',
			width				: 425,
			forceSelection	: true,
			msgTarget: 'side',
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	: 'descripcion',
			store				: catalogoPromo
                    },{
                        xtype     : 'displayfield',
                        id : 'disp',
                        width : 10
                    },{
                        xtype: 'button',
                        text:'Agregar',
                        iconCls:'icoAgregar',
                        id: 'agregaNafin',
                        width : 100,
                        handler: function() {
                            var contacto = Ext.getCmp('cbo_user_promo').getValue();
                            var descCont = Ext.getCmp('cbo_user_promo').getRawValue();
                            if (Ext.getCmp('cbo_user_promo').getValue() !="" ){
                                var grid = Ext.getCmp('gridNafin');
                                var store = grid.getStore();
                                var cmp=0; 
                                var uno=1;
                                var cuenta=0;
                                store.each(function(record) {
                                    cuenta++;
                                });
                            if (store.getCount()== 0){
                                TaskLocation = Ext.data.Record.create([
                                {name: "clave", type: "string"},
                                {name: "descripcion", type: "string"},
                                {name: "valor", type: "string"},
                                {name: "principal", type: "string"}
                                ]);
                                var record = new TaskLocation({
                                    //clave : Ext.id(),
                                    clave : contacto,
                                    descripcion: contacto,
                                    valor: descCont,
                                    principal : 'N'
                                });    
                                store.add(record);
                                store.commitChanges();
                                uno++;
                            } else {
                                store.each(function(record) {
                                var existe = record.data['descripcion'];
                                var nuevo  =  contacto;
                            if (nuevo != existe){           
                                cmp=0;
                            }else {
                                cmp++;
                                uno++;
                            }
                            }); }
                            if (cmp > 0){ 
                             Ext.Msg.show({
                                title: 'Contactos',
                                msg: 'El contacto ya existe en la lista,<br>por lo que no sera agregada nuevamente',
                                modal: true,
                                icon: Ext.Msg.WARNING,
                                buttons: Ext.Msg.OK,
                                fn: function (){
                                    var contacto1 = Ext.getCmp('cbo_user_promo');		
                                    contacto1.focus();
                                }
                            });
      } else { }//nuevamodificacion
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"},
              {name: "valor", type: "string"},
              {name: "principal", type: "string"}
        ]);
        var record = new TaskLocation({
            //clave : Ext.id(),
            clave : contacto,
            descripcion: contacto,
            valor: descCont,
            principal: 'N'
        });    
        store.add(record);
        store.commitChanges();
      } else { 
          if(cuenta==0) {} else { 
          Ext.Msg.show({
          title: 'Contactos',
          msg: 'El contacto ya existe en la lista,<br>por lo que no sera agregada nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var correo = Ext.getCmp('cbo_user_promo');		
                  correo.focus();
              }
          }); }
            } //nuevamodificacion
        } else{          
          if (contacto == "" ){
              Ext.Msg.show({
              title: 'Contactos',
              msg: 'Debe especificar algun contacto',
              modal: true,
              icon: Ext.Msg.WARNING,
              buttons: Ext.Msg.OK,
              fn: function (){
                  var correo = Ext.getCmp('cbo_user_promo');		
                      correo.focus();
                  }
              });
              }
           }// FIN ELSE
          }//FIN HANDLER
        },{
        xtype: 'button',
        text:'Quitar',
        iconCls:'icoCancelar',
        id: 'quitaNafin',
        width : 100,
        handler: function() {
   
      var grid = Ext.getCmp('gridNafin');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaNafin').getValue();
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
       if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaNafin').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Contactos',
          msg: 'Debe especificar un contacto',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }
    } else {
      Ext.Msg.show({
          title: 'Contactos',
          msg: 'No hay ningun contacto valido que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      }//fin else 
      }//fin handler
        },{
        xtype : 'textfield',
        id : 'validaNafin',
        msgTarget: 'side',
        width : 50,
        hidden: true
        }
			]
  },{
    xtype: 'compositefield',
    hidden : true,
    id:'es13',
		items: [{
            xtype: 'displayfield',
            width : 175
        },gridNafin
      ]
  },{
    xtype: 'displayfield',
    value:'Ejecutivos',
    width : 200,
    hidden : true,
    id:'es14'
  },{
    xtype: 'compositefield',
    hidden : true,
    combineErrors: false,
    id: 'es15',
		items: [
        {
						xtype			: 'combo',
						id			: 'cbo_ejec_promo',
						hiddenName 		: 'cbo_ejec_promo',
						fieldLabel		: 'Ejecutivo',
						emptyText: 'Seleccione...',
						width			: 425,
						forceSelection	: true,
						msgTarget: 'side',
						triggerAction	: 'all',
						mode			: 'local',
						valueField		: 'clave',
						displayField	: 'descripcion',
						store			: catalogoEjecutivo
					},
        {
        xtype     : 'displayfield',
        id : 'disp',
        width : 10
        },{
        xtype: 'button',
        text:'Agregar',
        iconCls:'icoAgregar',
        id: 'agregaEpp',
        width : 100,
        handler: function() {
        var ejecutivo = Ext.getCmp('cbo_ejec_promo').getValue();
        var descEject = Ext.getCmp('cbo_ejec_promo').getRawValue();
     
     if (Ext.getCmp('cbo_ejec_promo').getValue() !="" ){
     var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var cmp=0; 
      var uno=1;
      var cuenta=0;
      store.each(function(record) {
        cuenta++;
      });
      
      if (store.getCount()== 0){
        TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"},
              {name: "valor", type: "string"},
              {name: "principal", type: "string"}
        ]);
        var record = new TaskLocation({
            //clave : Ext.id(),
            clave : ejecutivo,
            descripcion: ejecutivo,
            valor: descEject,
            principal: 'N'
        });    
        store.add(record);
        store.commitChanges();
        uno++;
      } else {
      store.each(function(record) {
        var existe = record.data['descripcion'];
        var nuevo  =  ejecutivo;
        if (nuevo != existe){           
            cmp=0;
        }else {
          cmp++;
          uno++;
        }
      }); }
      if (cmp > 0){ 
      Ext.Msg.show({
          title: 'Ejecutivos',
          msg: 'El ejecutivo seleccionado ya existe en la lista,<br>por lo que no sera agregado nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var ejecutivo1 = Ext.getCmp('cbo_ejec_promo');		
                  ejecutivo1.focus();
              }
          });
      } else {}//nuevamodificacion1
      if (uno == 1 & cmp == 0 ){
       TaskLocation = Ext.data.Record.create([
              {name: "clave", type: "string"},
              {name: "descripcion", type: "string"},
              {name: "valor", type: "string"},
              {name: "principal", type: "string"}
        ]);
        var record = new TaskLocation({
            //clave : Ext.id(),
            clave : ejecutivo,
            descripcion: ejecutivo,
            valor: descEject,
            principal: 'N'
        });    
        store.add(record);
        store.commitChanges();
      } else { if(cuenta==0) {} else { 
          Ext.Msg.show({
          title: 'Ejecutivos',
          msg: 'El ejecutivo seleccionado ya existe en la lista,<br>por lo que no sera agregado nuevamente',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
               var ejecutivo1 = Ext.getCmp('cbo_ejec_promo');		
                  ejecutivo1.focus();
              }
          }); }
          }//nuevamodificacion2
     } else {
        if (ejecutivo == "" ){
          Ext.Msg.show({
          title: 'Ejecutivos',
          msg: 'Debe especificar algun ejecutivo',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK,
          fn: function (){
              var ejecutivo1 = Ext.getCmp('cbo_ejec_promo');		
                  ejecutivo1.focus();
              }
          });
          }
      }// FIN ELSE
      }//FIN HANDLER
        },{
        xtype: 'button',
        text:'Quitar',
        iconCls:'icoCancelar',
        id: 'quitaEpo',
        width : 100,
        handler: function() {
      var grid = Ext.getCmp('gridEpo');
      var store = grid.getStore();
      var indice = Ext.getCmp('validaEpo').getValue(); 
      var filas=0;
      store.each(function(record) {
        filas++;
      });
     if(filas>0){ 
      if (indice !=""){
        var rec = grid.getStore().getAt(indice);
        store.remove(rec);
        store.commitChanges();
        Ext.getCmp('validaEpo').setValue("");        
      } else {
          Ext.Msg.show({
          title: 'Ejecutivos',
          msg: 'Debe especificar un ejecutivo',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
      } 
    } else {
      Ext.Msg.show({
          title: 'Ejecutivos',
          msg: 'No hay ningun ejecutivo valido que se pueda borrar.',
          modal: true,
          icon: Ext.Msg.WARNING,
          buttons: Ext.Msg.OK
          });
        }//fin else
      }//fin handler
        },{
        xtype : 'textfield',
        id : 'validaEpo',
        msgTarget: 'side',
        width : 50,
        hidden: true
        }
			]
  },
  {
    xtype: 'compositefield',
    hidden : true,
    id:'es16',
		items: [{
      xtype: 'displayfield',
			width : 175 
      },gridEpo
      ]
    }		 
]
//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id					:'formcon',
		width				: 750,
		//height			: 100,
		title				:'Supervisi�n',
		//layout			:'form',
		frame				:true,
		collapsible		:true,
		titleCollapse	:false,
		style				:'margin:0 auto;',
		bodyStyle		:'padding: 6px',
		labelWidth		:50,
		defaults			:{
							msgTarget: 'side',
							anchor: '-20'
							},
		items				:[ 
							elementosForma
							]	
})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 949,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp
					]
})//-----------------------------Fin Contenedor Principal-------------------------

catalogoIF.load();
//catalogoPromo.load();
//catalogoEjecutivo.load();

})//-----------------------------------------------Fin Ext.onReady(function(){}