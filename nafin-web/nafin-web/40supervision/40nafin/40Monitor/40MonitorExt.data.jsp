<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.electronica.*,
	com.netro.exception.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	javax.naming.*,
	com.nafin.supervision.*,
	com.netro.model.catalogos.*,
	org.apache.commons.logging.Log,
        netropology.utilerias.usuarios.Usuario,
        netropology.utilerias.usuarios.UtilUsr"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String informacion  = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion    = (request.getParameter("operacion")   != null) ? request.getParameter("operacion")   : "";
String tipoArchivo  = (request.getParameter("tipo")        != null) ? request.getParameter("tipo")        : "";


String interFin     = (request.getParameter("interFin")    != null) ? request.getParameter("interFin")    : "0";
String cboEjec      = (request.getParameter("cboEjec")     != null) ? request.getParameter("cboEjec")     : "0";
String cboAnio      = (request.getParameter("cboAnio")     != null) ? request.getParameter("cboAnio")     : "0";
String cboMes       = (request.getParameter("cboMes")      != null) ? request.getParameter("cboMes")      : "0";
String clavesiag       = (request.getParameter("clavesiag")      != null) ? request.getParameter("clavesiag") : "0";
String id_tipoInfo       = (request.getParameter("id_tipoInfo")      != null) ? request.getParameter("id_tipoInfo") : "0";
String infoRegresar = "";
int start = 0;
int limit = 0;
System.out.println("#### <"+clavesiag );
Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
System.out.println("###%<"+clavesiag+"><"+interFin+"><"+cboAnio+"><"+cboMes+">");
if("CATALOGO_IF".equals(informacion)) {
	
	CatalogoIcCveSiag cat = new CatalogoIcCveSiag();
	cat.setCampoClave("A.IC_CVE_SIAG");
	cat.setCampoDescripcion("B.CG_RAZON_SOCIAL");
	cat.setOrden("B.CG_RAZON_SOCIAL");
        System.out.println("##########"+cat.getCampoClave());
	infoRegresar = cat.getJSONElementos();

} else if("CATALOGO_EJECUTIVO".equals(informacion)) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setTabla("BIT_SUPERVISION");
	catalogo.setDistinc(true);
	catalogo.setCampoClave("BIT_EJECUTIVO_LOGIN");
	catalogo.setCampoDescripcion("BIT_EJECUTIVO_NOMBRE");
	catalogo.setOrden("BIT_EJECUTIVO_NOMBRE");
	infoRegresar = catalogo.getJSONElementos();	

} else if("CATALOGO_ANIO".equals(informacion)) {

	List reg = supervision.getCatalogoAnioI();
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", reg);
	infoRegresar = jsonObj.toString();

} else if("CATALOGO_MES".equals(informacion)) {

	List lista = new ArrayList();
	CatalogoSupCalendarioIf cat = new CatalogoSupCalendarioIf();
        cat.setEsResultados("S");
	cat.setCalAnio(Integer.parseInt(cboAnio));
	infoRegresar = cat.getJSONElementos();

}else if("CONSULTA_RESULTADOS_SIAG".equals(informacion)) {
        System.out.println("#### Iniciando "+clavesiag);
	JSONArray  registros = new JSONArray();
	JSONObject resultado = new JSONObject();

	List<Map> listaResultados = supervision.obtieneResultadoSIAG(clavesiag);
	if(!listaResultados.isEmpty()) {
		for(Map<String, String> datos : listaResultados) {
			registros.add(datos);
		}
	}

	String consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	System.out.println("####JSON "+consulta);
        resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();

}
else if("CONSULTA_BITACORA".equals(informacion) || "DESCARGA_ARCHIVO".equals(informacion)) {
        System.out.println("###%#$$&  "+clavesiag+", "+interFin+", "+cboAnio+", "+cboMes);
        
	JSONObject resultado = new JSONObject();
	BitacoraSupervisionM paginador = new BitacoraSupervisionM();
        paginador.setClaveSIAG(!clavesiag.isEmpty()?Integer.parseInt(clavesiag):0);        
	paginador.setInterFin(Integer.parseInt("".equals(interFin)?"0":interFin));
	paginador.setEjecutivo(cboEjec);
	paginador.setAnio(Integer.parseInt("".equals(cboAnio)?"0":cboAnio));
	paginador.setMes(Integer.parseInt("".equals(cboMes)?"0":cboMes));
        paginador.setTipoDocumento(id_tipoInfo);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);        

	if("CONSULTA_BITACORA".equals(informacion)) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if("GENERAR".equals(operacion)){
				queryHelper.executePKQuery(request);
			}
			Registros registros = queryHelper.getPageResultSet(request,start,limit);
			
                         while(registros.next()){                         
                            String claveSiag = registros.getString("IC_SIAG");
                            String mes = registros.getString("MES");
                            String anio = registros.getString("ANIO");                            
                            String tipo = registros.getString("BIT_TIPO"); 
                            //System.out.println("#####Tipo  "+tipo);                                                       

                            String numgt = supervision.obtenerTotalGarantias(mes, anio, claveSiag, tipo);
                            registros.setObject("NGR", numgt);
                            
                            //Obtengo el color del semaforo
                            String semaforoRes = supervision.obtieneSemaforoMonitordeSupervision(registros.getString("BIT_FEC_PUB_RES_SUP"), registros.getString("CAL_FEC_LIM_ENT_R"), "RESOLUCION");
                            registros.setObject("SEMAFORO_RES", semaforoRes);
                            String semaforoDic = supervision.obtieneSemaforoMonitordeSupervision(registros.getString("BIT_FEC_PUB_DIC"), registros.getString("CAL_FEC_LIM_ENT_D"), "DICTAMEN");
                            registros.setObject("SEMAFORO_DIC", semaforoDic);
 
                        }

                        String consulta = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta);
			infoRegresar = resultado.toString();
                        //System.out.println(supervision.totalgarantias());
		}catch(Exception e){
			throw new AppException("Error en la paginación",e);
		}
	} else if("DESCARGA_ARCHIVO".equals(informacion)) {
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = resultado.toString();
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo " + tipoArchivo, e);
		}
	}

}
%>
<%=infoRegresar%>