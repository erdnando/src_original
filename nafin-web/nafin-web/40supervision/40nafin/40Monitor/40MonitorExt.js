Ext.onReady(function() {

	//Proceso del grid Resultados de la Consulta
	var procesarSuccessConsulta = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('formaConsulta');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
			var el = gridConsulta.getGridEl();
			if(store.getTotalCount() > 0) {
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();
				el.unmask();
			} else {
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	// Funci�n para descargar los archivos pdf y xls del grid
	function mostrarArchivo(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaConsulta.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaConsulta.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirCSV.setIconClass('icoXls');
		btnImprimirPDF.setIconClass('icoPdf');
		btnImprimirCSV.enable();
		btnImprimirPDF.enable();
    }
    
       

//--------------- STORES --------------------

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '40MonitorExt.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_BITACORA'
		},
		fields: [
            {name: 'MESANIOG',                mapping: 'MESANIOG'               },
            {name: 'BIT',                 mapping: 'BIT'         },
            {name: 'IC_SIAG',                 mapping: 'IC_SIAG'              }, 
            {name: 'NOMBRE_IF',               mapping: 'NOMBRE_IF'              },
            {name: 'BIT_EJECUTIVO_NOMBRE',    mapping: 'BIT_EJECUTIVO_NOMBRE'   },			
            {name: 'NGR',                     mapping: 'NGR'   },
            {name: 'CAL_FEC_LIM_ENT_R',       mapping: 'CAL_FEC_LIM_ENT_R'        },
            {name: 'BIT_FEC_PUB_RES_SUP',     mapping: 'BIT_FEC_PUB_RES_SUP'    },	
            {name: 'CAL_FEC_LIM_ENT_D',       mapping: 'CAL_FEC_LIM_ENT_D'    },
            {name: 'BIT_FEC_PUB_DIC',         mapping: 'BIT_FEC_PUB_DIC'        },
            {name: 'SEMAFORO_RES',         mapping: 'SEMAFORO_RES'        },
            {name: 'SEMAFORO_DIC',         mapping: 'SEMAFORO_DIC'        }
            
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarSuccessConsulta,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					var fp = Ext.getCmp('formaConsulta');
					fp.el.unmask();
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarSuccessConsulta(null, null, null);
				}
			}
		}
    });
    
   

	var catalogoIF = new Ext.data.JsonStore({
		id:         'catIFData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40MonitorExt.data.jsp',
		baseParams: { informacion: 'CATALOGO_IF'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
    });
    
    var consultaSIAG = new Ext.data.JsonStore({
		id:         'catIFData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40MonitorExt.data.jsp',
		baseParams: { informacion: 'CONSULTA_RESULTADOS_SIAG'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoEjecutivo = new Ext.data.JsonStore({
		id:         'catEjecData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40MonitorExt.data.jsp',
		baseParams: { informacion: 'CATALOGO_EJECUTIVO'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoAnio = new Ext.data.JsonStore({
		id:       'catAnio',
		root:     'registros',
		url:      '40MonitorExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_ANIO'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoMes = new Ext.data.JsonStore({
		id:       'catMes',
		root:     'registros',
		url:      '40MonitorExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_MES'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
    });
    var det_tipo = new Ext.data.JsonStore({
		id:         'catEjecData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40MonitorExt.data.jsp',
		baseParams: { informacion: 'CATALOGO_EJECUTIVO'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
    });

    var mySimpleStore = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [['R', 'Resultados de Supervisi�n'], ['D', 'Dictamen de Supervisi�n']]
    });
/*
    var mySimpleStore = Ext.create('Ext.data.Store', {
		fields: ['clave', 'descripcion'],
		data:[
			{clave: 'R', descripcion:'Resultados de Supervisi�n'},
			{clave: 'D', descripcion:'Dictamen de Supervisi�n'}
		],
		autoload: true
	});  
*/       
	 	var gruposT = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: ' ', colspan :6, align:'center'},
                {header: 'Resultados de Supervisi�n', colspan:3, align:'center'},
                {header: 'Dictamen de Supervisi�n', colspan: 3, align:'center'}					
			]
		]
	});
        
//--------------- GRID DE CONSULTA --------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		margins: '20 0 0 0',
		align: 'center',
		title: 'Resultado de Consulta',
		plugins: gruposT,
		height: 350,
		width: 943,
		displayInfo: true,
		loadMask: true,
		hidden: true,
		columns: [{
			header: 'Mes y A�o de Garant�as<br> Pagadas',
			dataIndex: 'MESANIOG',
			align: 'center',
			width: 150
		},{
			header: 'Tipo de Calendario',
			dataIndex: 'BIT',
			align: 'center',
			width: 150
		},{
			header:    'Clave SIAG',
			dataIndex: 'IC_SIAG',
			align:     'center',
			width:     150
		},{
			header:    'Nombre del Intermediario',
			dataIndex: 'NOMBRE_IF',
			align:     'center',
			width:     150
		},{
			header:    'Ejecutivo Responsable',
			dataIndex: 'BIT_EJECUTIVO_NOMBRE',
			align:     'center',
			width:     150
		},{
			header:    'N�mero de Garant�as',
			dataIndex: 'NGR',
			align:     'center',
			width:     150
		},{
			header:    'Fecha Programada',
			dataIndex: 'CAL_FEC_LIM_ENT_R',
			align:     'center',
			width:     150
		},{
			header:    'Fecha Real',
			dataIndex: 'BIT_FEC_PUB_RES_SUP',
			align:     'center',
			resizable: true
		},{
			//xtype:     'actioncolumn',
                        header:    'Avance de Cumplimiento',
			dataIndex: '',
			align:     'center',
			sortable:  false,
			resizable: false,
			width:     150,
                        renderer:  function (valor, columna, registro){
                            if(registro.get('SEMAFORO_RES') === 'VERDE') {
                                return '<img src="/nafin/00utils/gif/semaforo_verde.png" alt="Ver.1." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else if(registro.get('SEMAFORO_RES') === 'AMARILLO'){
                                return '<img src="/nafin/00utils/gif/semaforo_amarillo.png" alt="Ver.2." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else if(registro.get('SEMAFORO_RES') === 'ROJO'){
                                return '<img src="/nafin/00utils/gif/semaforo_rojo.png" alt="Ver.3." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else {
                                return '';
                            }
			}
			/*items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
                                    if(record.get('SEMAFORO_RES') === 'VERDE') {
                                        this.items[0].tooltip = 'VERDE';
                                        return  'icoPdf';//'semaforoVerde';//
                                    } else if(record.get('SEMAFORO_RES') === 'AMARILLO'){
                                        this.items[0].tooltip = 'AMARILLO';
                                        return 'icoWhiteCode';//'semaforoAmarillo';//
                                    } else if(record.get('SEMAFORO_RES') === 'ROJO'){
                                        this.items[0].tooltip = 'ROJO';
                                        return 'icoXls';//'semaforoRojo';//
                                    }
                                }
                            }]*/
		},{
			header:    'Fecha Programada',
			dataIndex: 'CAL_FEC_LIM_ENT_D',
			align:     'center',
			width:     150
		},{
			header:    'Fecha Real',
			dataIndex: 'BIT_FEC_PUB_DIC',
			align:     'center',
			width:     150
		},{
			//xtype:     'actioncolumn',
                        header:    'Avance de Cumplimiento',
			dataIndex: '',
			align:     'center',
			sortable:  false,
			resizable: false,
			width:     150,
                        renderer:  function (valor, columna, registro){
                            if(registro.get('SEMAFORO_DIC') === 'VERDE') {
                                return '<img src="/nafin/00utils/gif/semaforo_verde.png" alt="Ver.1." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else if(registro.get('SEMAFORO_DIC') === 'AMARILLO'){
                                return '<img src="/nafin/00utils/gif/semaforo_amarillo.png" alt="Ver.2." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else if(registro.get('SEMAFORO_DIC') === 'ROJO'){
                                return '<img src="/nafin/00utils/gif/semaforo_rojo.png" alt="Ver.3." style="border-style:none;height: 16px;width: 16px;font-size: 14px" />';
                            } else {
                                return '';
                            }
			}
			/*items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
                                    if(record.get('SEMAFORO_DIC') === 'VERDE') {
                                        this.items[0].tooltip = 'VERDE';
                                        return 'icoPdf';//'semaforoVerde';//
                                    } else if(record.get('SEMAFORO_DIC') === 'AMARILLO'){
                                        this.items[0].tooltip = 'AMARILLO';
                                        return 'icoWhiteCode';//'semaforoAmarillo';//
                                    } else if(record.get('SEMAFORO_DIC') === 'ROJO'){
                                        this.items[0].tooltip = 'ROJO';
                                        return 'icoXls';//'semaforoRojo';//
                                    }
				}
			}]*/
		}],
		frame: true,
		bbar: {
			xtype:           'paging',
			buttonAlign:     'left',
			id:              'barraPaginacion',
			displayMsg:      '{0} - {1} de {2}',
			emptyMsg:        'No se encontro ning�n registro',
			pageSize:        15,
			displayInfo:     true,
			store:           consultaData,
			items: ['->','-',{
				xtype:       'button',
				text:        'Descargar Archivo',
				id:          'btnGenerarArchivo',
				iconCls:     'icoXls',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url:'40MonitorExt.data.jsp',
						params: Ext.apply(formaConsulta.getForm().getValues(),{
						informacion: 'DESCARGA_ARCHIVO',
						tipo:        'XLS',
						interFin:    Ext.getCmp('cboIf').getValue(),
						cboEjec:     Ext.getCmp('cboEjec').getValue(),
						cboAnio:     Ext.getCmp('cboAnio').getValue(),
						cboMes:      Ext.getCmp('cboMes').getValue()
					}),
					callback: mostrarArchivo
					});
				}
			},'-',{
				xtype:       'button',
				text:        'Imprimir',
				iconCls:     'icoPdf',
				id:          'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '40MonitorExt.data.jsp',
						params: Ext.apply(formaConsulta.getForm().getValues(),{
							informacion: 'DESCARGA_ARCHIVO',
							tipo:        'PDF',
							interFin:    Ext.getCmp('cboIf').getValue(),
							cboEjec:     Ext.getCmp('cboEjec').getValue(),
							cboAnio:     Ext.getCmp('cboAnio').getValue(),
							cboMes:      Ext.getCmp('cboMes').getValue()
						}),
						callback: mostrarArchivo
					});
				}
			},'-',{
				xtype:       'button',
				text:        'Salir',
				id:          'btnSalirGrid',
				handler: function(boton, evento) {
                                        Ext.getCmp('clavesiag').setValue('');
					Ext.getCmp('cboIf').setValue('');
					Ext.getCmp('cboEjec').setValue('');
					Ext.getCmp('cboAnio').setValue('');
					Ext.getCmp('cboMes').setValue('');
                                        Ext.getCmp('id_tipoInfo').setValue('');
					Ext.getCmp('gridConsulta').hide();
				}
			}]
		}
	});

//--------------- FORM PRINCIPAL --------------------

	var formaConsulta = new Ext.form.FormPanel({
		id:                     'formaConsulta',
		title:                  'Criterios de Consulta',
		style:                  'margin:0 auto;',
		bodyStyle:              'padding: 6px',
		defaultType:            'textfield',
		width:                  500,
		labelWidth:             100,
		frame:                  true,
		defaults: {
			msgTarget:          'side'
		},
		items: [            
            {
                xtype:              'compositefield',
                id:                 'busquedaGarantia',
                columns:            2,
                combineErrors:      false,
                items:[
                    {
                        xtype:          'textfield',
                        fieldLabel:     'Clave SIAG',
                        msgTarget:      'side',
                        displayField:   'descripcion',
                        name:           'clavesiag',
                        id:             'clavesiag',
                        mode:           'local',
                        hiddenName:     'ClaveSIAG',
                        store:          'consultaSIAG',                       
                        minChars:       1,
                        width:          135,
                        editable:       true,
                        typeAhead:      true,
                        forceSelection: true
                    }
                ]
            }
            ,{
			xtype:              'combo',
			name:               'cboIf',
			hiddenName:         'cboIf',
			id:                 'cboIf',
			fieldLabel:         'Nombre del Intermediario',
			mode:               'local',
			emptyText:          'Seleccione...',
			displayField:       'descripcion',
			valueField:         'clave',
			triggerAction:      'all',
			tpl:                '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',
			store:              catalogoIF,
			minChars:           1,
			width:              348,
			forceSelection:     true,
			typeAhead:          true
		},{
			xtype:              'compositefield',
			id:                 'mesAnio',
			columns:            4,
			combineErrors:      false,
			items:[{
				xtype:          'combo',
				fieldLabel:     'A�o',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hanio',
				id:             'cboAnio',
				mode:           'local',
				hiddenName:     'A�o',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				margins:        '0 10 0 0',
				store:          catalogoAnio,
				width:          135,
				minChars:       1,
				allowBlank:     true,
				forceSelection: true,
				typeAhead:      true,
				editable:       false,
				listeners: {
					select: {
						fn: function(combo) {
							var cmbMes = Ext.getCmp('cboMes');
							cmbMes.setValue('');
							cmbMes.store.load({
								params: {
									cboAnio:  combo.getValue(),
									interFin: Ext.getCmp('cboIf').getValue()
								}
							});
						}
					}
				}
			},{
				xtype:          'displayfield',
				value:          '&nbsp;&nbsp;&nbsp; Mes:',
				width:          60
			},{
				xtype:          'combo',
				msgTarget:      'side',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hmes',
				id:             'cboMes',
				mode:           'local',
				hiddenName:     'Mes',
				emptyText:      'Seleccione...',
				store:          catalogoMes,
				minChars:       1,
				width:          135,
				editable:       false,
				typeAhead:      true,
				forceSelection: true
			},{
				xtype:          'displayfield',
				value:          '&nbsp;',
				width:          15
			}]
        },
        {
			xtype:              'combo',
			name:               'cboEjec',
			hiddenName:         'cboEjec',
			id:                 'cboEjec',
			fieldLabel:         'Nombre del Ejecutivo',
			mode:               'local',
			emptyText:          'Seleccione...',
			displayField:       'descripcion',
			valueField:         'clave',
			triggerAction:      'all',
			tpl:                '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',
			store:              catalogoEjecutivo,
			minChars:           1,
			width:              348,
			forceSelection:     true,
			typeAhead:          true
		},        
                {
            
                    xtype: 'combo',
                    editable:false,
                    fieldLabel: 'Tipo de Documento',
                    displayField: 'displayText',
                    valueField: 'myId',
                    triggerAction: 'all',
                    typeAhead: true,
                    minChars: 1,
                    name:'n_tipoInfo',
                    store: mySimpleStore,
                    id: 'id_tipoInfo',
                    mode: 'local',
                    width: 250,
                    hidden: false,
                    emptyText: 'Seleccione...'
                }
    ],
		buttons: [{
			xtype:              'button',
			text:               'Consultar',
			id:                 'btnBuscar',
			iconCls:            'icoBuscar',
			width:              75,
			handler: function(boton, evento) {
				formaConsulta.el.mask('Cargando...', 'x-mask-loading');
                                consultaData.load({
                                    params: Ext.apply({
					informacion: 'CONSULTA_BITACORA',
					operacion: 'GENERAR',
					start:0,
					limit:15,
                                        clavesiag:   Ext.getCmp('clavesiag').getValue(),
                                        interFin:    Ext.getCmp('cboIf').getValue(),
                                        cboEjec:     Ext.getCmp('cboEjec').getValue(),
                                        cboAnio:     Ext.getCmp('cboAnio').getValue(),
                                        cboMes:      Ext.getCmp('cboMes').getValue(),
                                        id_tipoInfo: Ext.getCmp('id_tipoInfo').getValue()
                                    })
                                });

			},
			style: {
				marginBottom: '10px'
			}
		},{
			xtype:              'button',
			text:               'Limpiar',
			id:                 'btnLimpiar',
			iconCls:            'icoLimpiar',
			width:              75,
			handler: function(boton, evento) {
                            Ext.getCmp('cboIf').setValue('');
                            Ext.getCmp('clavesiag').setValue('');
                            Ext.getCmp('cboEjec').setValue('');
                            Ext.getCmp('cboAnio').setValue('');
                            Ext.getCmp('cboMes').setValue('');
                            Ext.getCmp('id_tipoInfo').setValue('');
                            Ext.getCmp('gridConsulta').hide();
			},
			style: {
				marginBottom: '10px'
			}
		}]
	});

	var pnl = new Ext.Container({
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin:0 auto;',
		width:   949,
		items: [
			NE.util.getEspaciador(20),
			formaConsulta,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});

//--------------- CARGA INICIAL --------------------
    
    catalogoIF.load();
    catalogoEjecutivo.load();
    catalogoAnio.load();
    det_tipo.load();

});
