Ext.onReady(function() {
	
	var tipoCarga = Ext.getDom('tipoCarga').value;
	var columna= 4;
	var tipoCargaE  = "";
      	var textoFirmar; 
        var mesPago;
        var anioPago;
        var tipoCalen;
        var noRegistros;
        
	
		//para mostrar los archivos con errores
	
	
        var catalogoMes = new Ext.data.ArrayStore({
        fields: [
            'myId',
            'displayText'
        ],
        data: [[1, 'Enero'], [2, 'Febrero'],[3, 'Marzo'],
				[4, 'Abril'],[5, 'Mayo'],[6, 'Junio'],[7, 'Julio'],
				[8, 'Agosto'],[9, 'Septiembre'],[10, 'Octubre'],
				[11, 'Noviembre'],[12, 'Diciembre']]
    });
    
  
    
      var catalogoAnio = new Ext.data.JsonStore({
                id		: 'catAnio',
		root 		: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 		: '40ConsultaArchivosExt.data.jsp',
		baseParams	: { informacion: 'Consulta_Anio'},
		autoLoad		: false,
		listeners	:
		{
			//load		: procesarIF,
			exception: NE.util.mostrarDataProxyError
		}
	});
    
    var dt= Date();
var anio =Ext.util.Format.date(dt,'Y');
anio--;
var mesP =Ext.util.Format.date(dt,'m');
mesP--;
if(mesP<1){
mesP=12;
anio--;
}
	
       
        
	var procesarSuccessValidaCarga = function(store, arrRegistros, opts) {
	        
                var jsonData = store.reader.jsonData;		
		var mesPago = jsonData.MES_PAGO; 
		var tipoCarga = jsonData.TIPO_CARGA; 
		var folioOperacion = jsonData.FOLIO_OPERACION;		
		var noExpediente  = jsonData.NO_EXPEDIENTES;	
                var fechaHora  = jsonData.FECHA_HORA;	
                var fechaLimite  = jsonData.FECHA_LIMITE;	
             
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var fp = Ext.getCmp('formaCarga');
               
                mesPago = Ext.getCmp("cboMes").getValue();;
		anioPago = jsonData.ANIO;
                            
            
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridResulCarga.isVisible()) {
				gridResulCarga.show();
			}
			
			var el = gridResulCarga.getGridEl();			
			if(store.getTotalCount() > 0) {	
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
        
        var catalogoIF = new Ext.data.JsonStore({
	   id				: 'catIFData',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '40ConsultaArchivosExt.data.jsp',
		baseParams	: { informacion: 'CatalogoIF'},
		autoLoad		: false,
		listeners	:
		{
			//load		: procesarIF,
			exception: NE.util.mostrarDataProxyError
		}
	});
      
	 	
	//Resultados del proceso de carga
		var resultadosCargaData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '40ConsultaArchivosExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaArchivos'		
		},								
		fields: [			
			{name: 'MES_PAGO' , mapping: 'MES_PAGO'},			
			{name: 'TIPO_CARGA', mapping: 'TIPO_CARGA'},
			{name: 'FOLIO_OPERACION', mapping: 'FOLIO_OPERACION' },
                        {name: 'NO_EXPEDIENTES', mapping: 'NO_EXPEDIENTES' },                        
			{name: 'FECHA_HORA',  mapping: 'FECHA_HORA'},			
                        {name: 'FECHA_LIMITE',  mapping: 'FECHA_LIMITE'},
                        {name: 'ARCHIVOS_CARGADOS',  mapping: 'ARCHIVOS_CARGADOS'},
                        {name: 'MES',  mapping: 'MES'},
                        {name: 'ANIO',  mapping: 'ANIO'},
                        {name: 'INTER_FIN',  mapping: 'INTER_FIN'},
                        {name: 'CVE_SIAG',  mapping: 'CVE_SIAG'}
                        
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarSuccessValidaCarga,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSuccessValidaCarga(null, null, null);						
				}
			}
		}
	});
	
	
        var verDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var mes = registro.get('MES');  
		var anio = registro.get('ANIO');  
		var interFin = registro.get('INTER_FIN');
                var tipoCarga = registro.get('TIPO_CARGA');
                var acuse = registro.get('FOLIO_OPERACION');
            
              
		
		consDetalleDoctosData.load({
			params: {
				informacion: 'obtenArchivos',	
				mes:mes,
				anio:anio,
				interFin:interFin,
				tipoCarga:tipoCarga,
                                acuse: acuse
			}			
		});
		
		var verDetalle = Ext.getCmp('verDetalle');
		if(verDetalle){
			verDetalle.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'verDetalle',
				closeAction: 'hide',
				items: [					
					gridDetalleDoctos				
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: [
						{	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('verDetalle').hide();} }
													
					]
				}
			}).show().setTitle('Detalle Doctos');
		}		
	}
        
          var procesarConsDetalleDoctosDataData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('formaCarga');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridDetalleDoctos = Ext.getCmp('gridDetalleDoctos');
			if (!gridDetalleDoctos.isVisible()) {
				gridDetalleDoctos.show();
			}	
			var el = gridDetalleDoctos.getGridEl();			
			var info = store.reader.jsonData;				
		
			
			if(store.getTotalCount()>0){	
                                el.unmask();
			}else{				
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
        
        var consDetalleDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '40ConsultaArchivosExt.data.jsp',
		baseParams: {
			informacion: 'obtenArchivos'
		},		
		fields: [				
			{ name: 'NOMBRE_ARCHIVO'},
			{ name: 'RELACION_GAR'},
                        { name: 'ACUSE'},
                        { name: 'CARGADOS'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalleDoctosDataData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalleDoctosDataData(null, null, null);					
				}
			}
		}					
	});
        
      
        
        var gridDetalleDoctos = {		
		xtype: 'grid',
		store: consDetalleDoctosData,
		id: 'gridDetalleDoctos',		
		hidden: false,
		title: '',	
		columns: [	
			{
				header: 'Nombre Archivo',
				tooltip: 'Nombre Archivo',
				dataIndex: 'NOMBRE_ARCHIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'			
			},
			{
				header: 'Relacion de Garantias',
				tooltip: 'Relacion de Garantias',
				dataIndex: 'RELACION_GAR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'			
			},
                        {
				header: 'ACUSE',
				tooltip: 'acuse',
				dataIndex: 'ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
                                hidden: true
			},
                        {
				xtype:	'actioncolumn',
				header: 'Archivo', 
                                tooltip: 'Archivo Cargado',
				dataIndex: 'CARGADOS',	
                                align: 'center',	
                                sortable: true,	
                                resizable: true,	
                                width: 120,	
                                hideable: false, 
                                hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							var nombreArchivo = registro.get('NOMBRE_ARCHIVO');
                                                        var acuse = registro.get('ACUSE');
							procesaDetalles(nombreArchivo, acuse);	
						}
					}
				]
			}
                        ],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 320,		
		frame: true
	}
	
	
	var gridResulCarga = new Ext.grid.EditorGridPanel({	
		id: 'gridResulCarga',
		store: resultadosCargaData,	
		margins: '20 0 0',
		style: 'margin:0 auto;',
		align: 'center',
		//plugins: grupos,	
		title: 'Resultados de Consulta',		
		hidden: true,
		columns: [	
			{
				header: 'Mes de Pago de Garantia',
				dataIndex: 'MES_PAGO',
				width: 117,
				align: 'center'	,
				resizable: true	
			},	
			{
				header: 'Tipo de Carga',
				dataIndex: 'TIPO_CARGA',
				width: 117,
				resizable: true	,
				align: 'center'
			},			
			{
				header: 'Folio de la Operacion',
				dataIndex: 'FOLIO_OPERACION',
				width: 117,
				align: 'center',
				resizable: true
			},
                        {
				header: 'No. de Expedientes recibidos',
				dataIndex: 'NO_EXPEDIENTES',
				width: 117,
				align: 'center',
				resizable: true
			},
			//Documentos con errores
			{							
				header : 'Fecha y hora de la operacion',			
				dataIndex : 'FECHA_HORA',
				width : 117,
				resizable: true	,
				align: 'center'				
			}	,
			{
				header: 'Fecha Limite de Recepcion',
				dataIndex: 'FECHA_LIMITE',
				width: 117,
				resizable: true	,
				align: 'center'			
			},
                        
                        {
				header: 'Mes',
				dataIndex: 'MES',
				width: 117,
				resizable: true	,
				align: 'center'
			},
                        {
				header: 'A�o',
				dataIndex: 'ANIO',
				width: 117,
				resizable: true	,
				align: 'center'
			},
                        {
				header: 'CVE SIAG',
				dataIndex: 'CVE_SIAG',
				width: 117,
				resizable: true	,
				align: 'center'
			},
                        {
				header: 'Intermediario Financiero',
				dataIndex: 'INTER_FIN',
				width: 117,
				resizable: true	,
				align: 'center',
                                hidden: true
			},
                        
                        {
				header: 'Archivos Cargados',
				dataIndex: 'ARCHIVOS_CARGADOS',
				width: 80,
				resizable: true	,
				align: 'center',
                                hidden: true 
			},
                        {
			xtype:	'actioncolumn',
			header: 'Archivos Cargados',
			tooltip: 'Estatus',					
			align: 'center',				
			width: 150,
			/*renderer: function(value, metadata, record, rowindex, colindex, store) {
				return (record.get('CD_DESCRIPCION'));
			},*/
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('ARCHIVOS_CARGADOS')>0) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
					}
					,handler:	verDetalle  
				}
			]
                    }
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		align: 'center',
		frame: false
	});
	
			       
        function procesaDetalles(nombreArchivo, acuse) {
			formAux.action = NE.appWebContextRoot+'/supervision/VerArchivosS';
			formAux.nombreArchivo.value=nombreArchivo;
                        formAux.acuse.value=acuse;
			formAux.submit();
		
	}
		
	var elementosFormaCarga = [	
		{
			xtype:	'panel',
			layout:	'column',
			width: 350,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .75,
					anchor: '100%',
					layout: 'form',
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [                                        
                                          {
			xtype: 'compositefield',
			id:'intermediarioFin',
			columns: 3,
			items:[
				{
                                xtype				: 'combo',
                                name				: '_cbo_if',
                                hiddenName		: 'cbo_if',
                                id					: '_cbo_if',	
                                fieldLabel		: 'I. F. ', 
                                mode				: 'local',	
                                emptyText		: 'Seleccione un IF',
                                forceSelection  : true,	
                                triggerAction 	: 'all',	
                                typeAhead		: true,
                                minChars 		: 1,
                                tpl				: NE.util.templateMensajeCargaCombo,
                                store 			: catalogoIF,
                                displayField	: 'descripcion',
                                valueField 		: 'clave',
                                width : 300
                                }                                
                                ]
			 		
		},                      
                                                {
			xtype: 'compositefield',
			id:'mesAnio',
			columns: 3,
			items:[
				{
					xtype: 'combo',
					value: mesP,
					editable:false,
					fieldLabel: 'Mes de Pago',
					displayField: 'displayText',
					valueField: 'myId',
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
                                        forceSelection  : true,	
					name:'Hmes',
					store: catalogoMes,
					id: 'cboMes',
					mode: 'local',
                                        width: 111,
					hiddenName: 'Mes',
					hidden: false,
					emptyText: 'Seleccionar Mes',
                                        allowBlank: false
		},
                {
					xtype: 'displayfield',
					value: 'A�o:',
					width: 30
				},
                
                {	
					xtype: 'combo',
					displayField: 'descripcion',
					editable:false,
					valueField: 'clave',
					triggerAction: 'all',
                                        forceSelection  : true,	
					typeAhead: true,
					minChars: 1,
					name:'Hanio',
					id: 'cboAnio',
                                        width: 150,
					store: catalogoAnio,
					mode: 'local',
					hiddenName: 'A�o',
					hidden: false,
					emptyText: 'Seleccionar A�o',
                                        allowBlank: false,
                                        msgTarget: 'side',
                                        margins: '0 10 0 0' 
				}
                                
                                ]
			 		
		}
                 
					]
				}	
			]
		}	
	];
        
     
	
	var fpCarga = new Ext.form.FormPanel({
	  id: 'formaCarga',
		width: 600,
		title: 'Alta de Garantias - Consulta de Resultados',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		//fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
						xtype: 	'button',
						text: 	'Buscar',
						id: 		'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var cboMes = Ext.getCmp("cboMes").getValue();
                                                        var cboAnio   = Ext.getCmp("cboAnio").getValue();
                                                        var interFin  = Ext.getCmp("_cbo_if").getValue();
                                                        if(Ext.isEmpty(interFin)){
                                                            Ext.getCmp("_cbo_if").markInvalid('Debe seleccionar un intermediario financiero');	
                                                            return;
                                                        }
                                                        if(Ext.isEmpty(cboAnio)){
                                                            Ext.getCmp("cboAnio").markInvalid('Debe seleccionar un a�o');	
                                                            return;
                                                        }
                                                        
                                                        
                                                        fpCarga.el.mask('Cargando...', 'x-mask-loading');
							resultadosCargaData.load({
										params: {
											informacion: 'ConsultaArchivos',
											mes:cboMes,
                                                                                        cboAnio:cboAnio,
                                                                                        interFin:interFin
										}
									});			
						},
						style: { 
							  marginBottom:  '10px' 
						} 
		}
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [			
			NE.util.getEspaciador(20),
			fpCarga,
                        NE.util.getEspaciador(20),			
                        gridResulCarga,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoIF.load();
	
	catalogoAnio.load();
} );