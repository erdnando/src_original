<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.seguridadbean.*,
	com.netro.electronica.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.electronica.*,
	netropology.utilerias.*,
	com.netro.seguridad.*, 
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	com.netro.cotizador.*,
	com.nafin.supervision.*,
	com.netro.model.catalogos.*,  
	org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar = "";

OperacionElectronica opeElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
String claveIF          = (request.getParameter("cbo_if")!=null?request.getParameter("cbo_if"):"");
String claveIF2                 = (request.getParameter("claveIF")!=null?request.getParameter("claveIF"):"");

Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);

Vector vecColumnas   = null;
Vector vecFilasSE    = null;
Vector vecColumnasSE = null;
Vector vecFilasCE    = new Vector();
Vector vecColumnasCE = null;
Vector vecFilas      = new Vector();

if(informacion.equals("CatalogoIF")){

	List lista = new ArrayList();
	CatalogoIcCveSiag cat = new CatalogoIcCveSiag();
	cat.setCampoClave("A.IC_CVE_SIAG");
	cat.setCampoDescripcion("B.CG_RAZON_SOCIAL");
	cat.setOrden("B.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoPromo")){
	JSONArray jsObjArray = new JSONArray();
        List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
        //List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("EJE PROMO", "N");
        List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveIF2, "I");
        if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
                        if(loginUsuarioOPOP!=null){
                            Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);                        
                             System.out.println(usuarioEpo.getPerfil());
                            if("IF SUP GARANT".equals(usuarioEpo.getPerfil())){
                                String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
                                hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
                                hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
                                lstCatalogoEjecOP.add(hmData);
                            }
                        }
		}
   }
             
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
   		
} else if(informacion.equals("CatalogoEjecutivo")){
	JSONArray jsObjArray = new JSONArray();
        List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
        //List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUP SEG", "N");
        
        //List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("EJEC SUPGARANT", "N");
       List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("", "N");
        //"SUP SEG", "N"
        //"EJE PROMO", "N"
                
        if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
                        if(loginUsuarioOPOP!=null){
                            Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
                            if("EJEC SUPGARANT".equals(usuarioEpo.getPerfil())){
                                String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
                                hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
                                hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
                                lstCatalogoEjecOP.add(hmData);
                            }
                        }
		}
   }
             
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
		
} else if(informacion.equals("catalogoSupervisor")){
JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
   List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUP SEG", "N");
                
   if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);		                                        
			lstCatalogoEjecOP.add(hmData);
		}
   }
                                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
		
} else if(informacion.equals("catalogoIFOperador")){
	
JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
	
	String[] listaIF	  = request.getParameterValues("listaIF");	
	List listsIF 	= Arrays.asList(listaIF);
		System.err.println("IF: "+listsIF);
	String operador = (request.getParameter("operador") != null) ? request.getParameter("operador") : "";
	String autorizadorUno = (request.getParameter("autorizadorUno") != null) ? request.getParameter("autorizadorUno") : "";

		ArrayList 	listaUsuarios 	= new ArrayList();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		Iterator itIFS = listsIF.iterator();
		String miPerfil="";

		while(itIFS.hasNext()){
			List cuentas = utilUsr.getUsuariosxAfiliado( (String)itIFS.next(), "I");			
			Iterator itCuentas = cuentas.iterator();
			while (itCuentas.hasNext()) {
				netropology.utilerias.usuarios.Usuario oUsuario = null;
				String login = (String) itCuentas.next();
                                oUsuario = oUtilUsr.getUsuario(login);
				if("IF LI".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				}
			}
		}
		
   List usuariosLI = listaUsuarios;
	
	List listaAux =null;
	
		listaAux= listaUsuarios;
		if(listaAux!=null && listaAux.size()>0){
			HashMap hmData = null;
			for(int i=0;i<listaAux.size();i++){
				hmData = new HashMap();
				String loginUsuarioOPOP = (String)listaAux.get(i);
                              	Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
				String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();
				if(operador.equals("")){
					hmData.put("clave", loginUsuarioOPOP);
					hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
					lstCatalogoEjecOP.add(hmData);
				} else {
					if(autorizadorUno.equals("")){
						if(loginUsuarioOPOP.equals(operador)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					} else {
						if(loginUsuarioOPOP.equals(operador) || loginUsuarioOPOP.equals(autorizadorUno)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					}
				}
			}
		}
	 
    if(!autorizadorUno.equals("")){
		HashMap hmData = new HashMap();
		hmData.put("clave", "0");
		hmData.put("descripcion", "No Aplica");
		lstCatalogoEjecOP.add(hmData);
	 }  
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
    
    
} else if(informacion.equals("catalogoepo") ){

	JSONObject 					jsonObj 	= new JSONObject();
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setBancofondeo("1");
	infoRegresar = catalogo.getJSONElementos();

     
	 
} else if(informacion.equals("CargarDatos")){
	
	
	String consulta = "";
	JSONObject resultado = new JSONObject();

	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cmp = (request.getParameter("cmp") != null) ? request.getParameter("cmp") : "";
	
	CargaIFOE  carga = new CargaIFOE();
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( carga );
	
	carga.setCve_if(cve_if);
	carga.setValida(cmp);
	
	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		 
	}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
	}
	
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();

} else if(informacion.equals("ConsultaArchivos")) {

		String cboAnio   = (request.getParameter("cboAnio") != null) ? request.getParameter("cboAnio") : "0";
		String mes       = (request.getParameter("mes") != null) ? request.getParameter("mes") : "0";
		String interFin  = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "0";
		String strAforo  = "0";
		String query     = "";
		ResultSet rs     = null;

		Vector diames_inhabil = new Vector();
		HashMap datos         = new HashMap();
		JSONArray registros   = new JSONArray();
		JSONObject resultado  = new JSONObject();

		//recupero el nombre del archivo
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea = "";

		vecFilas                = supervision.getConsultaArchivosR(interFin,mes,cboAnio);
		vecFilasSE              = new Vector();
		String referencia       = "";
		String mesPago          = "";
		String tipoCarga        = "";
		String folioOperacion   = "";
		String noExpedientes    = "";
		String fechaHora        = "";
		String fechaLimite      = "";
		String archivosCargados = "";
		String cveSIAG          = "";

		for(int i=0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			vecFilasSE.add(vecColumnas);
		}

		String errorCE = "";

		for(int x=0;x<vecFilasSE.size();x++){
			mesPago          = "";
			tipoCarga        = "";
			folioOperacion   = "";
			noExpedientes    = "";
			fechaHora        = "";
			fechaLimite      = "";
			archivosCargados = "0";

			if(x<vecFilasSE.size()){
				vecColumnasSE  = (Vector)vecFilasSE.get(x);
				mesPago        = (String)vecColumnasSE.get(0);
				tipoCarga      = (String)vecColumnasSE.get(1);
				folioOperacion = (String)vecColumnasSE.get(2);
				noExpedientes  = (String)vecColumnasSE.get(3);
				fechaHora      = (String)vecColumnasSE.get(4);
				fechaLimite    = (String)vecColumnasSE.get(5);
				cveSIAG        = (String)vecColumnasSE.get(6);
				if(noExpedientes!=null&&!noExpedientes.equals(""))
					archivosCargados = "1";
			}

			datos.put("MES_PAGO",          mesPago          );
			datos.put("TIPO_CARGA",        tipoCarga        );
			datos.put("FOLIO_OPERACION",   folioOperacion   );
			datos.put("NO_EXPEDIENTES",    noExpedientes    );
			datos.put("FECHA_HORA",        fechaHora        );
			datos.put("FECHA_LIMITE",      fechaLimite      );
			datos.put("ARCHIVOS_CARGADOS", archivosCargados );
			datos.put("INTER_FIN",         interFin         );
			datos.put("CVE_SIAG",          cveSIAG          );
			datos.put("MES",               mes              );
			datos.put("ANIO",              cboAnio          );
			registros.add(datos);
		}

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);
		infoRegresar = resultado.toString();

	} else if(informacion.equals("obtenArchivos")) {

		String    anio           = (request.getParameter("anio")     != null) ? request.getParameter("anio")     : "0";
		String    mes            = (request.getParameter("mes")      != null) ? request.getParameter("mes")      : "0";
		String    interFin       = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "0";
		String    acuse          = (request.getParameter("acuse")    != null) ? request.getParameter("acuse")    : "";
		String    strAforo       = "0";
		String    query          = "";
		ResultSet rs             = null;
		Vector    diames_inhabil = new Vector();

		HashMap    datos     = new HashMap();
		JSONArray  registros = new JSONArray();
		JSONObject resultado = new JSONObject();

		//recupero el nombre del archivo
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea= "";

		vecFilas = supervision.getObtenArchivosR(interFin,mes,anio, acuse, "-1");
		vecFilasSE = new Vector();
		String referencia  = "";
		String nomArchivo  = "";
		String relacionGar = "";

		for(int i=0;i<vecFilas.size();i++)	{
			vecColumnas = (Vector)vecFilas.get(i);
			vecFilasSE.add(vecColumnas);
		}

		String errorCE = "";

		for(int x=0;x<vecFilasSE.size();x++){
			nomArchivo = "";
			relacionGar = "";
			if(x<vecFilasSE.size()){
				vecColumnasSE = (Vector)vecFilasSE.get(x);
				nomArchivo    = (String)vecColumnasSE.get(0);
				relacionGar   = (String)vecColumnasSE.get(1);
			}

			datos.put("NOMBRE_ARCHIVO", nomArchivo );
			datos.put("RELACION_GAR",   relacionGar);
			datos.put("INTER_FIN",      interFin   );
			datos.put("MES",            mes        );
			datos.put("ANIO",           anio       );
			datos.put("ACUSE",          acuse      );
			registros.add(datos);
		}

		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);

		infoRegresar = resultado.toString();

	} else if(informacion.equals("Consulta_Anio")){
		List reg = supervision.getCatalogoAnioI();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		System.out.println("infoRegresar ** inicial "+infoRegresar);
	}
        
        else if(informacion.equals("DescargaArch")) {
	//String strDirectorioTemp	=	(request.getParameter("directorio")!=null)?request.getParameter("directorio"):"";	
	String nombreArchivo	=	(request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";	
	String acuse	=	(request.getParameter("clave_carga")!=null)?request.getParameter("clave_carga"):"";	
	
		//String nombreArchivo = Comunes.cadenaAleatoria(16)+".txt";
		JSONObject jsonObj = new JSONObject();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nombreArchivo", nombreArchivo);
		jsonObj.put("acuse", acuse);
		
	infoRegresar = jsonObj.toString();
	}
%>

<%=infoRegresar%>