<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		org.apache.commons.logging.Log,
		com.nafin.supervision.*,
		netropology.utilerias.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 1024*1024*80; // 80 MB   
%>
<%
	ParametrosRequest req = null;
	JSONObject jsonObj = new JSONObject();
	String itemArchivo = "";
	String rutaArchivo = "";
	String mensajeError =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	String estadoSiguiente = "";
	String rutaArchivoTemporal="";
	
	String cargaAcuse = "";
	String cargaFechaAcuse = "";
	String cargaHoraAcuse = "";
	
	Boolean success = true;
	boolean continua = true;
	
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
	if (ServletFileUpload.isMultipartContent(request)) {
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		// Definir el tamaño máximo que pueden tener los archivos
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		// Parsear request
		
		try {
		
			req = new ParametrosRequest(upload.parseRequest(request));
			
			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName().toUpperCase();
                        System.out.println("*********Modificacion Archivo(itemArchivo)  "+itemArchivo);
			
			rutaArchivo = PATH_FILE+itemArchivo;
			int tamanio			= (int)fItem.getSize();
		
			nombreArchivo= itemArchivo;
			rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
                        
			File archivoUpload  = new File(rutaArchivoTemporal);
			if(archivoUpload.exists()){
				archivoUpload.delete();
			}
			fItem.write(new File(rutaArchivoTemporal));
			
			
		} catch(Throwable e) {			
				
				if( e instanceof SizeLimitExceededException  ) {
					
					log.error("CargaPlantilla.subirArchivo(Exception): El Archivo es muy Grande, excede el límite", e);
					mensajeError = "El archivo que intenta cargar sobrepasa los "+ ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) +" Mb permitidos. Favor de verificarlo";
					estadoSiguiente = "VALIDACIONES_SIN_EXITO";
					continua = false;
					
				} else{
					
					success	= false;
					continua = false;
					log.error("CargaPlantilla.subirArchivo(Exception): Guardar Archivo en Disco");
					throw new AppException("Ocurrió un error al subir el archivo", e);
				}
				
		}
		
	}
	if(!mensajeError.equals("")){
		nombreArchivo="";
	}
	
	if(continua==true){
		estadoSiguiente = "MOSTRAR_PDF_VIEW";
	}

%>


{
	"success": <%=success.toString()%>,
	"nombreArchivo":	'<%=nombreArchivo%>',
	"estadoSiguiente":	'<%=estadoSiguiente%>',
	"rutaArchivoTemporal":	'<%=strDirecVirtualTemp+nombreArchivo%>',
	"mensajeError":	'<%=mensajeError%>'
}