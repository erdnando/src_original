Ext.onReady(function() {

	//Proceso del grid Resultados de la Consulta
	var procesarSuccessConsulta = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('formaConsulta');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
			var el = gridConsulta.getGridEl();
			if(store.getTotalCount() > 0) {
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();
				el.unmask();
			} else {
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	// Funci�n para descargar los archivos pdf y xls del grid
	function mostrarArchivo(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaConsulta.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaConsulta.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirCSV.setIconClass('icoXls');
		btnImprimirPDF.setIconClass('icoPdf');
		btnImprimirCSV.enable();
		btnImprimirPDF.enable();
	}

//--------------- STORES --------------------

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '40consultaSupervisionBitacoraExt.data.jsp',
		baseParams: {
			informacion: 'CONSULTA_BITACORA'
		},
		fields: [
			{name: 'IC_IF',                   mapping: 'IC_IF'                  },
			{name: 'NOMBRE_IF',               mapping: 'NOMBRE_IF'              },
			{name: 'BIT_EJECUTIVO_NOMBRE',    mapping: 'BIT_EJECUTIVO_NOMBRE'   },
			{name: 'BIT_FEC_PUB_CAL',         mapping: 'BIT_FEC_PUB_CAL'        },
			{name: 'BIT_FEC_PUB_AVI',         mapping: 'BIT_FEC_PUB_AVI'        },			
                        {name: 'BIT_FEC_CON_AVI',         mapping: 'BIT_FEC_CON_AVI'        },
			{name: 'BIT_FEC_LIM_ENT_EXP',     mapping: 'BIT_FEC_LIM_ENT_EXP'    },
			{name: 'BIT_FEC_ENT_EXP',         mapping: 'BIT_FEC_ENT_EXP'        },
                        {name: 'NGR',                     mapping: 'NGR'                    },                        			
                        {name: 'BIT_FEC_PUB_RES_SUP',     mapping: 'BIT_FEC_PUB_RES_SUP'    },
			{name: 'BIT_FEC_CON_RES_SUP',     mapping: 'BIT_FEC_CON_RES_SUP'    },
			{name: 'BIT_FOLIO_CON_RES',       mapping: 'BIT_FOLIO_CON_RES'      },
			{name: 'BIT_FEC_LIM_ENT_INF_ACL', mapping: 'BIT_FEC_LIM_ENT_INF_ACL'},
                        {name: 'BIT_FEC_ENT_INF_ACL',     mapping: 'BIT_FEC_ENT_INF_ACL'    },
                        {name: 'BIT_FEC_PUB_DIC',         mapping: 'BIT_FEC_PUB_DIC'        },
			{name: 'BIT_FEC_CON_RES_DIC',     mapping: 'BIT_FEC_CON_RES_DIC'    },
			{name: 'BIT_FOLIO_CON_DIC',       mapping: 'BIT_FOLIO_CON_DIC'      },
                        {name: 'BIT_TIPO',                mapping: 'BIT_TIPO'      }
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarSuccessConsulta,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					var fp = Ext.getCmp('formaConsulta');
					fp.el.unmask();
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarSuccessConsulta(null, null, null);
				}
			}
		}
	});

	var catalogoIF = new Ext.data.JsonStore({
		id:         'catIFData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40consultaSupervisionBitacoraExt.data.jsp',
		baseParams: { informacion: 'CATALOGO_IF'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoEjecutivo = new Ext.data.JsonStore({
		id:         'catEjecData',
		root:       'registros',
		fields:     ['clave', 'descripcion', 'loadMsg'],
		url:        '40consultaSupervisionBitacoraExt.data.jsp',
		baseParams: { informacion: 'CATALOGO_EJECUTIVO'},
		autoLoad:   false,
		listeners:  {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoAnio = new Ext.data.JsonStore({
		id:       'catAnio',
		root:     'registros',
		url:      '40consultaSupervisionBitacoraExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_ANIO'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

	var catalogoMes = new Ext.data.JsonStore({
		id:       'catMes',
		root:     'registros',
		url:      '40consultaSupervisionBitacoraExt.data.jsp',
		autoLoad: false,
		fields: ['clave', 'descripcion', 'loadMsg'],
		baseParams: {
			informacion: 'CATALOGO_MES'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

//--------------- GRID DE CONSULTA --------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id:            'gridConsulta',
		margins:       '20 0 0',
		style:         'margin:0 auto;',
		align:         'center',
		title:         'Resultados de Consulta',
		emptyMsg:      'No hay registros.',
		store:         consultaData,
		clicksToEdit:  1,
		height:        350,
		width:         943,
		frame:         false,
		hidden:        true,
		displayInfo:   true,
		loadMask:      true,
		stripeRows:    true,
		columns: [{
			header:    'Clave del IF',
			dataIndex: 'IC_IF',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Nombre del IF',
			dataIndex: 'NOMBRE_IF',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Nombre del ejecutivo',
			dataIndex: 'BIT_EJECUTIVO_NOMBRE',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Fecha de Publicaci�n <br> del Calendario',
			dataIndex: 'BIT_FEC_PUB_CAL',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Fecha de Notificaci�n <br> de Avisos de Supervisi�n',
			dataIndex: 'BIT_FEC_PUB_AVI',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Fecha de Consulta IF <br> de Avisos de Supervisi�n',
			dataIndex: 'BIT_FEC_CON_AVI',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Fecha L�mite de Entrega <br> de Expediente',
			dataIndex: 'BIT_FEC_LIM_ENT_EXP',
			align:     'center',
			width:     150,
			resizable: true
		},{
                        header:    'Fecha de Entrega <br> de Expediente',
			dataIndex: 'BIT_FEC_ENT_EXP',
			align:     'center',
			width:     150,
			resizable: true
                },{
                        header:    'N�m. de Garant�as <br> Recibidas',
			dataIndex: 'NGR',
			align:     'center',
			width:     150,
			resizable: true
                },{
			header:    'Fecha de Publicaci�n de <br> Resultado de Supervisi�n',
			dataIndex: 'BIT_FEC_PUB_RES_SUP',
			align:     'center',
			width:     150,
			resizable: true,
			renderer: function(value, p, record) {
                                if((record.get('BIT_TIPO'))==='S') {
                                    return 'No aplica';
                                } else {
                                    return record.get('BIT_FEC_PUB_RES_SUP');
                                }
			}
		},{
			header:    'Fecha de Consulta IF de <br> Resultado de Supervisi�n',
			dataIndex: 'BIT_FEC_CON_RES_SUP',
			align:     'center',
			width:     150,
			resizable: true,
			renderer: function(value, p, record) {
                                if((record.get('BIT_TIPO'))==='S') {
                                    return 'No aplica';
                                } else {
                                    return record.get('BIT_FEC_CON_RES_SUP');
                                }
			}
		},{
			header:    'Folio Consulta Resultado <br> de Supervisi�n',
			dataIndex: 'BIT_FOLIO_CON_RES',
			align:     'center',
			width:     150,
			resizable: true,
			renderer: function(value, p, record) {
                                if((record.get('BIT_TIPO'))==='S') {
                                    return 'No aplica';
                                } else {
                                    return record.get('BIT_FOLIO_CON_RES');
                                }
			}
		},{
			header:    'Fecha L�mite de Entrega <br> de Informaci�n Aclaraci�n',
			dataIndex: 'BIT_FEC_LIM_ENT_INF_ACL',
			align:     'center',
			width:     150,
			resizable: true,
			renderer: function(value, p, record) {
                                if((record.get('BIT_TIPO'))==='S') {
                                    return 'No aplica';
                                } else {
                                    return record.get('BIT_FEC_LIM_ENT_INF_ACL');
                                }
			}
		},{
                        header:    'Fecha de Expedientes de <br> Aclaraciones',
			dataIndex: 'BIT_FEC_ENT_INF_ACL',
			align:     'center',
			width:     150,
			resizable: true,
			renderer: function(value, p, record) {
                                if((record.get('BIT_TIPO'))==='S') {
                                    return 'No aplica';
                                } else {
                                    return record.get('BIT_FEC_ENT_INF_ACL');
                                }
			}
                },{
			header:    'Fecha de Publicaci�n <br> de Dictamen',
			dataIndex: 'BIT_FEC_PUB_DIC',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Fecha de Consulta IF <br> del Dictamen Supervisi�n',
			dataIndex: 'BIT_FEC_CON_RES_DIC',
			align:     'center',
			width:     150,
			resizable: true
		},{
			header:    'Folio Consulta Dictamen <br> de Supervisi�n',
			dataIndex: 'BIT_FOLIO_CON_DIC',
			align:     'center',
			width:     150,
			resizable: true
		}],
		bbar: {
			xtype:           'paging',
			buttonAlign:     'left',
			id:              'barraPaginacion',
			displayMsg:      '{0} - {1} de {2}',
			emptyMsg:        'No se encontro ning�n registro',
			pageSize:        15,
			displayInfo:     true,
			store:           consultaData,
			items: ['->','-',{
				xtype:       'button',
				text:        'Descargar Archivo',
				id:          'btnGenerarArchivo',
				iconCls:     'icoXls',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url:'40consultaSupervisionBitacoraExt.data.jsp',
						params: Ext.apply(formaConsulta.getForm().getValues(),{
						informacion: 'DESCARGA_ARCHIVO',
						tipo:        'XLS',
						interFin:    Ext.getCmp('cboIf').getValue(),
						cboEjec:     Ext.getCmp('cboEjec').getValue(),
						cboAnio:     Ext.getCmp('cboAnio').getValue(),
						cboMes:      Ext.getCmp('cboMes').getValue()
					}),
					callback: mostrarArchivo
					});
				}
			},'-',{
				xtype:       'button',
				text:        'Imprimir',
				iconCls:     'icoPdf',
				id:          'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '40consultaSupervisionBitacoraExt.data.jsp',
						params: Ext.apply(formaConsulta.getForm().getValues(),{
							informacion: 'DESCARGA_ARCHIVO',
							tipo:        'PDF',
							interFin:    Ext.getCmp('cboIf').getValue(),
							cboEjec:     Ext.getCmp('cboEjec').getValue(),
							cboAnio:     Ext.getCmp('cboAnio').getValue(),
							cboMes:      Ext.getCmp('cboMes').getValue()
						}),
						callback: mostrarArchivo
					});
				}
			},'-',{
				xtype:       'button',
				text:        'Salir',
				id:          'btnSalirGrid',
				handler: function(boton, evento) {
					Ext.getCmp('cboIf').setValue('');
					Ext.getCmp('cboEjec').setValue('');
					Ext.getCmp('cboAnio').setValue('');
					Ext.getCmp('cboMes').setValue('');
					Ext.getCmp('gridConsulta').hide();
				}
			}]
		}
	});

//--------------- FORM PRINCIPAL --------------------

	var formaConsulta = new Ext.form.FormPanel({
		id:                     'formaConsulta',
		title:                  'Criterios de Consulta',
		style:                  'margin:0 auto;',
		bodyStyle:              'padding: 6px',
		defaultType:            'textfield',
		width:                  500,
		labelWidth:             100,
		frame:                  true,
		defaults: {
			msgTarget:          'side'
		},
		items: [{
			xtype:              'combo',
			name:               'cboIf',
			hiddenName:         'cboIf',
			id:                 'cboIf',
			fieldLabel:         'Nombre del Intermediario',
			mode:               'local',
			emptyText:          'Seleccione...',
			displayField:       'descripcion',
			valueField:         'clave',
			triggerAction:      'all',
			tpl:                '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',
			store:              catalogoIF,
			minChars:           1,
			width:              348,
			forceSelection:     true,
			typeAhead:          true
		},{
			xtype:              'combo',
			name:               'cboEjec',
			hiddenName:         'cboEjec',
			id:                 'cboEjec',
			fieldLabel:         'Nombre del Ejecutivo',
			mode:               'local',
			emptyText:          'Seleccione...',
			displayField:       'descripcion',
			valueField:         'clave',
			triggerAction:      'all',
			tpl:                '<tpl for=".">' +
                '<tpl if="!Ext.isEmpty(loadMsg)">'+
                '<div class="loading-indicator">{loadMsg}</div>'+
                '</tpl>'+
                '<tpl if="Ext.isEmpty(loadMsg)">'+
                '<div class="x-combo-list-item">' +
                '<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
                '</div></tpl></tpl>',
			store:              catalogoEjecutivo,
			minChars:           1,
			width:              348,
			forceSelection:     true,
			typeAhead:          true
		},{
			xtype:              'compositefield',
			id:                 'mesAnio',
			columns:            4,
			combineErrors:      false,
			items:[{
				xtype:          'combo',
				fieldLabel:     'A�o',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hanio',
				id:             'cboAnio',
				mode:           'local',
				hiddenName:     'A�o',
				emptyText:      'Seleccione...',
				msgTarget:      'side',
				margins:        '0 10 0 0',
				store:          catalogoAnio,
				width:          135,
				minChars:       1,
				allowBlank:     true,
				forceSelection: true,
				typeAhead:      true,
				editable:       false,
				listeners: {
					select: {
						fn: function(combo) {
							var cmbMes = Ext.getCmp('cboMes');
							cmbMes.setValue('');
							cmbMes.store.load({
								params: {
									cboAnio:  combo.getValue(),
									interFin: Ext.getCmp('cboIf').getValue()
								}
							});
						}
					}
				}
			},{
				xtype:          'displayfield',
				value:          '&nbsp;&nbsp;&nbsp; Mes:',
				width:          60
			},{
				xtype:          'combo',
				msgTarget:      'side',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				name:           'Hmes',
				id:             'cboMes',
				mode:           'local',
				hiddenName:     'Mes',
				emptyText:      'Seleccione...',
				store:          catalogoMes,
				minChars:       1,
				width:          135,
				editable:       false,
				typeAhead:      true,
				forceSelection: true
			},{
				xtype:          'displayfield',
				value:          '&nbsp;',
				width:          15
			}]
		}],
		buttons: [{
			xtype:              'button',
			text:               'Consultar',
			id:                 'btnBuscar',
			iconCls:            'icoBuscar',
			width:              75,
			handler: function(boton, evento) {
				formaConsulta.el.mask('Cargando...', 'x-mask-loading');
				consultaData.load({
					params: {
						informacion: 'CONSULTA_BITACORA',
						operacion:   'GENERAR',
						interFin:    Ext.getCmp('cboIf').getValue(),
						cboEjec:     Ext.getCmp('cboEjec').getValue(),
						cboAnio:     Ext.getCmp('cboAnio').getValue(),
						cboMes:      Ext.getCmp('cboMes').getValue(),
						start:       0,
						limit:       15
					}
				});
			},
			style: {
				marginBottom: '10px'
			}
		},{
			xtype:              'button',
			text:               'Limpiar',
			id:                 'btnLimpiar',
			iconCls:            'icoLimpiar',
			width:              75,
			handler: function(boton, evento) {
				Ext.getCmp('cboIf').setValue('');
				Ext.getCmp('cboEjec').setValue('');
				Ext.getCmp('cboAnio').setValue('');
				Ext.getCmp('cboMes').setValue('');
				Ext.getCmp('gridConsulta').hide();
			},
			style: {
				marginBottom: '10px'
			}
		}]
	});

	var pnl = new Ext.Container({
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin:0 auto;',
		width:   949,
		items: [
			NE.util.getEspaciador(20),
			formaConsulta,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
//--------------- CARGA INICIAL --------------------
	catalogoIF.load();
	catalogoEjecutivo.load();
	catalogoAnio.load();

});