<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.supervision.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf" %>  

<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>


<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.debug("informacion: "+informacion);

String tipoAfiliado = (request.getParameter("tipoAfiliado") != null) ? request.getParameter("tipoAfiliado") : "";
String perfil = (request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";

Integer icCveSiag = (request.getParameter("icCveSiag") != null) ? Integer.parseInt(request.getParameter("icCveSiag")):0;
String calTipo = (request.getParameter("calTipo") != null) ? request.getParameter("calTipo") : "";
Integer calAnio = (request.getParameter("calAnio") != null) ? Integer.parseInt(request.getParameter("calAnio")):0;
Integer calMes = (request.getParameter("calMes") != null) ? Integer.parseInt(request.getParameter("calMes")):0;
String tipoInfo = (request.getParameter("tipoInfo") != null) ? request.getParameter("tipoInfo") : "";

if (informacion.equals("servGetCatalogoIF")){   //<------------------------------------------------------
	CatalogoIcCveSiag cat = new CatalogoIcCveSiag();
	cat.setCampoClave("a.IC_CVE_SIAG");
	cat.setCampoDescripcion("b.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("servGetCatalogoAnio")){   //<------------------------------------------------------
	
	CatalogoAnioMesSupCalendarioIf cat = new CatalogoAnioMesSupCalendarioIf();
	cat.setCampoClave("a.CAL_ANIO");
	cat.setCampoDescripcion("a.CAL_ANIO");
	cat.setIcCveSiag(icCveSiag);
	cat.setCalTipo(calTipo);
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("servGetCatalogoMes")){   //<------------------------------------------------------
	CatalogoAnioMesSupCalendarioIf cat = new CatalogoAnioMesSupCalendarioIf();
	cat.setCampoClave("a.CAL_MES");
	cat.setCampoDescripcion("a.CAL_MES");
	cat.setIcCveSiag(icCveSiag);
	cat.setCalTipo(calTipo);
	cat.setCalAnio(calAnio);
	List elementos = cat.getListaElementos();
	
	String []meses = {"","Enero","Febrero","Marzo","Abril",
                                "Mayo","Junio","Julio","Agosto",
                                "Septiembre", "Octubre","Noviembre","Diciembre",
                                "1er Semestre", "2do Semestre"};
	for(Object obj: elementos){
		ElementoCatalogo elementoCatalogo = (ElementoCatalogo)obj;
		elementoCatalogo.setDescripcion(meses[Integer.parseInt(elementoCatalogo.getDescripcion())]);
	}
	
	infoRegresar = cat.getJSONElementos(elementos);

} else if (informacion.equals("servSetGuardar")){ 
	Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB",Supervision.class);
	String cargaAcuse = "";
	String cargaFechaAcuse = "";
	String cargaHoraAcuse = "";
	String mensajeError = "";
	String estadoSiguiente = "";
	String nombreArchivo = (request.getParameter("nombreArchivo") != null) ? request.getParameter("nombreArchivo") : "";

	if(("R".equals(tipoInfo) || "D".equals(tipoInfo))
            && supervision.existeDoctosResultDictamenSup(Long.parseLong(String.valueOf(icCveSiag)),
                            calAnio, calMes, calTipo, tipoInfo)){
			
                String info = "";
                if("R".equals(tipoInfo)){
                    info = "Resultados de Supervisión";
                } else if("D".equals(tipoInfo)){
                    info = "Dictamen de Supervisión";
                } else if("A".equals(tipoInfo)){
                    info = "Corrección de Resultados de Supervisión";
                } else if("B".equals(tipoInfo)){
                    info = "Corrección de Dictamen de Supervisión";
                }
		mensajeError = "El archivo de "+ info +" del IF del mes y "+
				"año seleccionado ya se encuentra cargado. Favor de verificarlo.";
		
		estadoSiguiente = "VALIDACIONES_SIN_EXITO";
			
	} else if(!supervision.isWebServiceDisponible()){
		
		mensajeError = "El archivo no se pudo cargar, el servicio no esta disponible.";
		estadoSiguiente = "VALIDACIONES_SIN_EXITO";
		
	} else {
	
		Map<String,String> mResult = supervision.cargaDoctosResultDictamenSup(Long.parseLong(String.valueOf(icCveSiag)), 
				calAnio, calMes, calTipo, tipoInfo, strLogin, nombreArchivo, strDirectorioTemp);
				
		if("true".equals(mResult.get("CARGA_EXITO"))){
			cargaAcuse = mResult.get("CARGA_ACUSE");
			cargaFechaAcuse = mResult.get("CARGA_FECHA_ACUSE");
			cargaHoraAcuse = mResult.get("CARGA_HORA_ACUSE");
			estadoSiguiente = "MOSTRAR_ACUSE_CARGA";
		}else{
                    String info = "";
                    if("R".equals(tipoInfo)){
                        info = "Resultados de Supervisión";
                    } else if("D".equals(tipoInfo)){
                        info = "Dictamen de Supervisión";
                    } else if("A".equals(tipoInfo)){
                        info = "Corrección de Resultados de Supervisión";
                    } else if("B".equals(tipoInfo)){
                        info = "Corrección de Dictamen de Supervisión";
                    }
			mensajeError = "El archivo de "+ info +" no se cargo via webservice ";
			estadoSiguiente = "VALIDACIONES_SIN_EXITO";
		}
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("cargaAcuse", cargaAcuse);
	jsonObj.put("cargaFechaAcuse", cargaFechaAcuse);
	jsonObj.put("cargaHoraAcuse", cargaHoraAcuse);
	jsonObj.put("cargaUsuario", strLogin+"-"+strNombreUsuario);
	jsonObj.put("mensajeError", mensajeError);
	jsonObj.put("estadoSiguiente", estadoSiguiente);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("VALIDA_ARCHIVO_EXISTENTE")) {

    Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB",Supervision.class);
    String mensajeCorreccion = ""; //Mensaje que indica que por alguna causa no se puede continuar con la carga.
    String confirmaCorreccion = ""; //Mensaje que solicita la confirmación para continuar con la carga.
    String estadoSiguiente = "";
    String mensajeError = "";

    if("A".equals(tipoInfo)) {
        //Correccion de resultados de supervision
        //Paso 1. Valida si ya existe un archivo de Resultados de supervision para el IF y periodo seleccionados
        if(supervision.existeDoctosResultDictamenSup(Long.parseLong(String.valueOf(icCveSiag)), calAnio, calMes, calTipo, "R")){
            confirmaCorreccion = "¿Est&aacute; seguro de reemplazar el archivo de Resultado de Supervisi&oacute;n correspondiente al periodo "+ (calMes<10?"0"+calMes:calMes) + "-" + calAnio +"?";
            estadoSiguiente = "PROCESAR_ARCHIVO";
	} else {
            mensajeCorreccion = "No es posible realizar una corrección de Resultados de Supervisi&oacute;n debido a que no se encuentra un Resultado de Supervisi&oacute;n previamente cargado.";
            estadoSiguiente = "VALIDACIONES_SIN_EXITO";
        }

    } else if("B".equals(tipoInfo)) {
        //Correccion de dictamen de supervision
        //Paso 1. Valida si ya existe un archivo de dictamen de supervision para el IF y periodo seleccionados
        if(supervision.existeDoctosResultDictamenSup(Long.parseLong(String.valueOf(icCveSiag)), calAnio, calMes, calTipo, "D")){
            confirmaCorreccion = "¿Est&aacute; seguro de reemplazar el archivo de Dictamen de Supervisi&oacute;n correspondiente al periodo "+ (calMes<10?"0"+calMes:calMes) + "-" + calAnio +"?";
            estadoSiguiente = "PROCESAR_ARCHIVO";
	} else {
            mensajeCorreccion = "No es posible realizar una corrección de Dictamen de Supervisi&oacute;n debido a que no se encuentra un Dictamen de Supervisi&oacute;n previamente cargado.";
            estadoSiguiente = "VALIDACIONES_SIN_EXITO";
        }
    
    } else {
        mensajeError = "Opcion no valida.";
        estadoSiguiente = "VALIDACIONES_SIN_EXITO";
    }
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("confirmaCorreccion", confirmaCorreccion);
    jsonObj.put("mensajeCorreccion", mensajeCorreccion);
    jsonObj.put("mensajeError", mensajeError);
    jsonObj.put("estadoSiguiente", estadoSiguiente);
    jsonObj.put("success", new Boolean(true));
    infoRegresar = jsonObj.toString();

}

log.debug("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>