<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.electronica.*,
	com.netro.exception.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	javax.naming.*,
	java.io.FileOutputStream,
	java.io.OutputStream,
	com.nafin.supervision.*,
	com.netro.model.catalogos.*,
	org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/40supervision/040secsession_extjs.jspf"%>
<%! private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar = "";

Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);

if("CATALOGO_IF".equals(informacion)) {

	List lista = new ArrayList();
	CatalogoIcCveSiag cat = new CatalogoIcCveSiag();
	cat.setCampoClave("A.IC_CVE_SIAG");
	cat.setCampoDescripcion("B.CG_RAZON_SOCIAL");
	cat.setOrden("B.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();

} else if("CATALOGO_ANIO".equals(informacion)) {

	List reg = supervision.getCatalogoAnioI();
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", reg);
	infoRegresar = jsonObj.toString();

} else if("CATALOGO_MES".equals(informacion)) {

	String interFin = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "0";
	String cboAnio  = (request.getParameter("cboAnio")  != null) ? request.getParameter("cboAnio")  : "0";

	List lista = new ArrayList();
	CatalogoSupCalendarioIf cat = new CatalogoSupCalendarioIf();
	cat.setCalAnio(Integer.parseInt(cboAnio));
	cat.setIcCveSiag(Integer.parseInt("".equals(interFin)?"0":interFin));
	infoRegresar = cat.getJSONElementos();

} else if("CONSULTA_RESULTADOS".equals(informacion)) {

	String interFin = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "0";
	String cboMes   = (request.getParameter("mes")      != null) ? request.getParameter("mes")      : "0";
	String cboAnio  = (request.getParameter("cboAnio")  != null) ? request.getParameter("cboAnio")  : "0";
        String txtGarantia = (request.getParameter("txtGarantia") != null) ? request.getParameter("txtGarantia") : "-1";

	JSONArray  registros = new JSONArray();
	JSONObject resultado = new JSONObject();

	List<Map> listaResultados = supervision.obtieneResultadosDeSupervision(interFin, cboMes, cboAnio, txtGarantia, true);
	if(!listaResultados.isEmpty()) {
		for(Map<String, String> datos : listaResultados) {
			registros.add(datos);
		}
	}

	String consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();

} else if("CONSULTA_ARCHIVOS_CARGADOS".equals(informacion)) {

		String    anio       = (request.getParameter("anio")     != null) ? request.getParameter("anio")     : "0";
		String    mes        = (request.getParameter("mes")      != null) ? request.getParameter("mes")      : "0";
		String    interFin   = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "0";
		String    acuse      = (request.getParameter("acuse")    != null) ? request.getParameter("acuse")    : "";
                String    garantia      = (request.getParameter("garantia")    != null) ? request.getParameter("garantia")    : "-1";

		HashMap    datos     = new HashMap();
		JSONArray  registros = new JSONArray();
		JSONObject resultado = new JSONObject();

		Vector vecFilasSE    = new Vector();
		Vector vecColumnas   = null;
		Vector vecColumnasSE = null;
		String nomArchivo    = "";
		String relacionGar   = "";

		Vector vecFilas = supervision.getObtenArchivosR(interFin, mes, anio, acuse, garantia);
		for(int i=0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			vecFilasSE.add(vecColumnas);
		}

		for(int x=0;x<vecFilasSE.size();x++) {
			nomArchivo = "";
			relacionGar = "";
			if(x<vecFilasSE.size()) {
				vecColumnasSE = (Vector)vecFilasSE.get(x);
				nomArchivo    = (String)vecColumnasSE.get(0);
				relacionGar   = (String)vecColumnasSE.get(1);
			}
			datos.put("NOMBRE_ARCHIVO", nomArchivo );
			datos.put("RELACION_GAR",   relacionGar);
			datos.put("INTER_FIN",      interFin   );
			datos.put("MES",            mes        );
			datos.put("ANIO",           anio       );
			datos.put("ACUSE",          acuse      );
			registros.add(datos);
		}

		String consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);
		infoRegresar = resultado.toString();

} else if("CONSULTA_AVISOS_SUP".equals(informacion)) {

	String     interFin  = (request.getParameter("interFin") != null) ? request.getParameter("interFin") : "";
	String     anio      = (request.getParameter("anio")     != null) ? request.getParameter("anio")     : "";
	String     mes       = (request.getParameter("mes")      != null) ? request.getParameter("mes")      : "";
	String     consAviso = (request.getParameter("consAviso")!= null) ? request.getParameter("consAviso"): "";
	JSONArray  registros = new JSONArray();
	JSONObject resultado = new JSONObject();

	List<Map> listaResultados = supervision.obtieneAvisosDeSupervision(interFin, anio, mes);
	if(!listaResultados.isEmpty()) {
		for(Map<String, String> datos : listaResultados) {
			registros.add(datos);
		}
	}

	String consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();

} else if("PDF_CARGA_DE_DOCUMENTOS".equals(informacion)) {

	String folioOperacion = (request.getParameter("folioOperacion") != null) ? request.getParameter("folioOperacion") : "";
	JSONObject jsonObj = new JSONObject();
	ResultadosSupervision paginador = new ResultadosSupervision();
	paginador.setFolioOperacion(folioOperacion);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	jsonObj.put("mensajeErr", "");
	infoRegresar = jsonObj.toString();

} else if("PDF_AVISO_DE_SUPERVISION".equals(informacion)) {

	String interFin  = (request.getParameter("interFin")  != null) ? request.getParameter("interFin") : "0";
	String cboMes    = (request.getParameter("cboMes")    != null) ? request.getParameter("cboMes")   : "0";
	String cboAnio   = (request.getParameter("cboAnio")   != null) ? request.getParameter("cboAnio")  : "0";
	String relGarant = (request.getParameter("relGarant") != null) ? request.getParameter("relGarant") : "";

	JSONObject jsonObj = new JSONObject();
	AvisoSupervision paginador = new AvisoSupervision();
	paginador.setInterFin(Integer.parseInt("".equals(interFin)?"0":interFin));;
	paginador.setAnio(Integer.parseInt("".equals(cboAnio)?"0":cboAnio));
	paginador.setMes(Integer.parseInt("".equals(cboMes)?"0":cboMes));
	paginador.setRelGarant(relGarant);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "XLS");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	jsonObj.put("mensajeErr", "");
	infoRegresar = jsonObj.toString();

} else if("DESCARGA_ARCHIVOS_SUPERVISION".equals(informacion)) {

	String     nombreArchivo = (request.getParameter("nombreArchivo") != null) ? request.getParameter("nombreArchivo") : "";
	String     acuse         = (request.getParameter("acuse")         != null) ? request.getParameter("acuse")         : "";
	JSONObject jsonObj       = new JSONObject();
	String     mensajeErr    = "";

	if(!supervision.isWebServiceDisponible()){
		jsonObj.put("success", new Boolean(true));
		mensajeErr = "El archivo no se pudo descargar, el servicio no esta disponible.";
	} else{
		try {
			ArchivosCargaValue archivosValue = null;
			archivosValue = supervision.getArchivo(nombreArchivo, acuse);
			byte[] x = archivosValue.getARCHIVO_ESP();
			OutputStream outF = new FileOutputStream(strDirectorioTemp + nombreArchivo);
			outF.write(x);
			outF.close();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		} catch(Exception e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al descargar el archivo. ", e);
		}
	}
	jsonObj.put("mensajeErr", mensajeErr);
	infoRegresar = jsonObj.toString();

}
%>

<%=infoRegresar%>