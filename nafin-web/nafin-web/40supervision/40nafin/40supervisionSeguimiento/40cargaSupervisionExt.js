Ext.onReady(function() {
	var nombreArchivo = '';

    /*
     * Cuando se va a cargar una correccion de dictamen de supervision,
     * se valida que el nombre del archivo contenga el nombre del periodo
     * TRUE: El nombre es valido
     * FALSE: No es valido.
     */
    function validaNombreArchivo (archivo, mes, anio) {
        console.log("validaNombreArchivo<"+archivo+"-"+mes+"-"+anio+">");
        var nombreValido = false;
        var periodo = '';
        var n = 0;
        if(mes==='1'){
            periodo = "_ENERO" + anio;
            n = archivo.indexOf(periodo);
        } else if(mes==='2'){
            periodo = "_FEBRERO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='3'){
            periodo = "_MARZO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='4'){
            periodo = "_ABRIL" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='5'){
            periodo = "_MAYO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='6'){
            periodo = "_JUNIO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='7'){
            periodo = "_JULIO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='8'){
            periodo = "_AGOSTO" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='9'){
            periodo = "_SEPTIEMBRE" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='10'){
            periodo = "_OCTUBRE" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='11'){
            periodo = "_NOVIEMBRE" + anio;
            n = archivo.indexOf(periodo);
        }  else if(mes==='12'){
            periodo = "_DICIEMBRE" + anio;
            n = archivo.indexOf(periodo);
        } 
        
        if(n!==-1){
            nombreValido = true;
        }
        return nombreValido;
    }
	
	var fnArchivoPdfXlsxZip = function(nombArch){
		var myRegex = /^.+\.([Pp][dD][Ff])$/;
		var myRegexZip = /^.+\.([Zz][Ii][Pp])$/;
		var myRegexXlsx = /^.+\.([Xx][Ll][Ss][Xx])$/;
		
		return (myRegex.test(nombArch) || myRegexZip.test(nombArch) || myRegexXlsx.test(nombArch));
	};
	
	Ext.apply(Ext.form.VTypes, {
		archivoPdfXlsxZip: function(v) {
			
			return fnArchivoPdfXlsxZip(v);
		},
		archivoPdfXlsxZipText: 'Solo se permite cargar archivos en formato PDF, XLSX y ZIP Favor de verificarlo.'
	});
	
	var fnPDFViewer = function(rutaArchivo){
		
		var showPDF = Ext.create('Ext.window.Window', {
			title: 'Contenido PDF',
			modal   : true,
			closeAction: 'destroy',
			items: { 
				 xtype: 'component',
				 html: '<iframe src="../../../00tmp/40supervision/'+rutaArchivo+'" width="600" height="500"></iframe>'
				 //html: '<iframe src="' +NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params)+'" width="600" height="500"></iframe>'
			  }
		});
		
		showPDF.show();
	};
	
	var procesarSuccessCargaArchivo = function(options, success, response) {
		main.el.unmask();
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			
			fnCargaArchivo(resp.estadoSiguiente, resp);
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	};
	
	var procesarSuccessValidaCargaArchivo = function(options, success, response) {
		main.el.unmask();
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
                
			var resp = Ext.JSON.decode(response.responseText);
                        var confirmaCorreccion = resp.confirmaCorreccion;
			var mensajeCorreccion = resp.mensajeCorreccion;
                        
                        if(mensajeCorreccion!=='') {
                            Ext.MessageBox.alert('Mensaje', mensajeCorreccion, function(){
                                fnCargaArchivo(resp.estadoSiguiente, resp);//Ya no hace nada
                            });
                        } else {
                            if(confirmaCorreccion!=='') {
                                var btnBox = Ext.MessageBox; 
                                btnBox.buttonText={
                                    cancel: 'Cancelar',
                                    no: 'Cancelar',
                                    ok: 'Aceptar',
                                    yes: 'Aceptar'
                                };
                                Ext.MessageBox.confirm('Mensaje',confirmaCorreccion,function(msb){
                                    if(msb=='yes'){
                                        btnBox=null;
                                        fnCargaArchivo(resp.estadoSiguiente, resp);//Continua con la carga del archivo
                                    } else {
                                        btnBox=null;
                                        fnCargaArchivo("VALIDACIONES_SIN_EXITO", null);//Ya no hace nada
                                    }
                                });
                            } else {
                                fnCargaArchivo(resp.estadoSiguiente, resp);//Ya no hace nada
                            }
                        }
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	};
	
	var fnEventSelectIFtipoCal = function(obj, newVal, oldVal) {
		var  forma =   Ext.ComponentQuery.query('#fpCargaArchivos1')[0];
		var valorIF = forma.query('#id_if')[0].getValue();
		var valorTipoCal = forma.query('#id_tipoCalendario')[0].getValue();
                
		forma.query('#id_anio')[0].reset();
		forma.query('#id_mes')[0].reset();
                
		if(valorIF!==null && valorIF!=='' && valorTipoCal!==null && valorTipoCal!==''){
                    storeCatAnio.load({
                        params:{
                            icCveSiag: valorIF,
                            calTipo: valorTipoCal
                        }
                    });
		}                
                if(valorTipoCal==='S') {
                    //alert(valorTipoCal);
                    //var combo = Ext.getCmp('id_tipoInfo');
                    //combo.store.remove(combo.store.getById('R'));
                    
                }
	};
	
	var fnEventSelectIFtipoCalAnio = function(obj, newVal, oldVal) {
		var  forma =   Ext.ComponentQuery.query('#fpCargaArchivos1')[0];
		var valorIF = forma.query('#id_if')[0].getValue();
		var valorTipoCal = forma.query('#id_tipoCalendario')[0].getValue();
		var valorAnio = forma.query('#id_anio')[0].getValue();

		forma.query('#id_mes')[0].reset();
		
		if(valorIF!==null && valorIF!=='' && valorTipoCal!==null && valorTipoCal!=='' && 
				valorAnio!==null && valorAnio!==''){

			storeCatMes.load({
				params:{
					icCveSiag: valorIF,
					calTipo: valorTipoCal,
					calAnio:valorAnio
				}
			});
		}
	};
	
	var procesaCargaArchivo = function(opts, success, action) {
		
		if (success === true && action.result.success === true){
			var resp = 	action.result;
			fnCargaArchivo(resp.estadoSiguiente,resp);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
		
	};
	
	var fnCargaArchivo = function(estado, respuesta){
                console.log("fnCargaArchivo ini " + estado);
		var forma =   Ext.ComponentQuery.query('#fpCargaArchivos1')[0];
		var botonPDFView = forma.query('#btnPDFView')[0];
		var fieldArchCarga = forma.query('#id_archivoCarga')[0];
                var valorIF = forma.query('#id_if')[0].getValue();
		var valorTipoCal = forma.query('#id_tipoCalendario')[0].getValue();
		var valorAnio = forma.query('#id_anio')[0].getValue();
		var valorMes = forma.query('#id_mes')[0].getValue();
		var valorTipoInfo = forma.query('#id_tipoInfo')[0].getValue();
		
		botonPDFView.hide();
		if(estado === "SUBIR_ARCHIVO"){
			nombreArchivo = '';
			var params = forma.getForm().getValues();
			
			forma.getForm().submit({
				clientValidation: false,
				url: '40cargaSupervisionExtCargaArch.jsp',
				params:		params,
				waitMsg: 'Enviando datos...',
				waitTitle:'Cargando archivo',
				success: function(form, action) {
					procesaCargaArchivo(null,  true,  action );
				}
				,failure: NE.util.mostrarSubmitError
			});
		
		}else if(estado === "VALIDACIONES_SIN_EXITO"){
			nombreArchivo = '';
			fieldArchCarga.setRawValue(nombreArchivo);
                        if(null!=respuesta && respuesta.mensajeError!==''){
                            Ext.Msg.alert(
				'Mensaje',
				respuesta.mensajeError
                            );
                        }
		
		}else if(estado === "MOSTRAR_PDF_VIEW"){
			nombreArchivo = respuesta.nombreArchivo;
			fieldArchCarga.setRawValue(nombreArchivo);
			
			var myRegex = /^.+\.([Pp][dD][Ff])$/;
			if(myRegex.test(nombreArchivo) ){
				botonPDFView.show(nombreArchivo);
			}
		
		}else if(estado === "PROCESAR_ARCHIVO"){
			main.el.mask('Cargando Archivo...', 'x-mask-loading');
			
			Ext.Ajax.request({
				url: '40cargaSupervisionExt.data.jsp',
				params : {
					informacion: 'servSetGuardar',
					icCveSiag: valorIF,
					calTipo: valorTipoCal,
					calAnio: valorAnio,
					calMes: valorMes,
					tipoInfo: valorTipoInfo,
					nombreArchivo: nombreArchivo
				},
				callback: procesarSuccessCargaArchivo
			});	
		}else if(estado === "MOSTRAR_ACUSE_CARGA"){
			
			var dataAcuse = [
					['N�mero de Carga', respuesta.cargaAcuse],
					['Fecha de Carga', respuesta.cargaFechaAcuse],
					['Hora de Carga', respuesta.cargaHoraAcuse],
					['Usuario de Carga', respuesta.cargaUsuario]
				];
			storeAcuse.loadData(dataAcuse);
			
			
			var pFormData = (Ext.ComponentQuery.query('#fpCargaArchivos1')[0]).getForm().getValues();
			
			var dataDetAcuse = [
					[
						fpCargaArchivos.query('#id_if')[0].getRawValue(),
						fpCargaArchivos.query('#id_tipoCalendario')[0].getRawValue(),
						pFormData.n_anio, pFormData.n_mes,
						fpCargaArchivos.query('#id_tipoInfo')[0].getRawValue(),
						nombreArchivo]
				];
			storeDetAcuse.loadData(dataDetAcuse);
			
			fpCargaArchivos.hide();
			gpAcuse.show();
			gpDetalleAcuse.show();

		} else if(estado === "VALIDA_ARCHIVO_EXISTENTE") {

			main.el.mask('Cargando Archivo...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '40cargaSupervisionExt.data.jsp',
				params : {
					informacion: 'VALIDA_ARCHIVO_EXISTENTE',
					icCveSiag: valorIF,
					calTipo: valorTipoCal,
					calAnio: valorAnio,
					calMes: valorMes,
					tipoInfo: valorTipoInfo,
					nombreArchivo: nombreArchivo
				},
				callback: procesarSuccessValidaCargaArchivo
			})

                }
                console.log("fnCargaArchivo fin");
	};
	
	var fnMostrarAyuda = function(){
		var descripcion =['<table width="350" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite seleccionar el archivo que desea cargar. Los tipos de archivos que admite son PDF, XLSX y ZIP.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		Ext.create('Ext.window.Window',{								
			id:'ventanaAyuda',
			modal: true,
			width: 400,
			title: 'Ayuda',
			autoHeight: true,
			resizable: false,
			constrain: true,
			closable:true,
			autoScroll:true,
			closeAction: 'destroy',
			html: descripcion.join('')
		}).show();
	};
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});
	
	var  storeCatIntermediario = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '40cargaSupervisionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'servGetCatalogoIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});
	
	var storeCatTipoCalendario = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		data:[
			{clave: 'O', descripcion:'Ordinario'},
			{clave: 'E', descripcion:'Extemporaneo'},
                        {clave: 'S', descripcion:'Semestral'}
		],
		autoload: true
	});
	
	var storeCatAnio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '40cargaSupervisionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'servGetCatalogoAnio'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var storeCatMes = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '40cargaSupervisionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'servGetCatalogoMes'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var storeCatTipoInformacion = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		data:[
			{clave: 'R', descripcion:'Resultados de Supervisi�n'},
			{clave: 'D', descripcion:'Dictamen de Supervisi�n'},
			{clave: 'A', descripcion:'Correcci�n de Resultados de Supervisi�n'},
			{clave: 'B', descripcion:'Correcci�n de Dictamen de Supervisi�n'}
		],
		autoload: true
	});
	
	var storeAcuse = Ext.create('Ext.data.ArrayStore', {
		fields: [
		  {name: 'ETIQUETA'},
		  {name: 'DESCRIPCION'}
		],
		autoLoad: false
	});
	
	var gpAcuse = Ext.create('Ext.grid.Panel',{
		id: 'gpAcuse', width: 445, frame: true, align: 'center', style: 'margin:0 auto;', hideHeaders: true,
		border: true, hidden: true, loadMask: true, stripeRows: true, autoHeight: true, columnLines: true,
		title: 'Acuse Carga Documentaci�n', store: storeAcuse,
		columns:           [
			{
				width: 150, align:     'left', sortable: true, resizable: false,
				dataIndex: 'ETIQUETA'
			},{
				width: 280,align: 'left',
				dataIndex: 'DESCRIPCION'
			}
		]
		
	});
	
	var storeDetAcuse = Ext.create('Ext.data.ArrayStore', {
		fields: [
		  {name: 'DET_ACUSE_NOMBRE_IF'},
		  {name: 'DET_ACUSE_TIPO_CAL'},
		  {name: 'DET_ACUSE_ANIO'},
		  {name: 'DET_ACUSE_MES'},
		  {name: 'DET_ACUSE_TIPO_INFO'},
		  {name: 'DET_ACUSE_NOMBRE_ARCH'}
		],
		autoLoad: false
	});
	
	var gpDetalleAcuse = Ext.create('Ext.grid.Panel',{
		id: 'gpDetalleAcuse', width: 900, height: 120, frame: true, align: 'center', style: 'margin:0 auto;',
		border: true, hidden: true, loadMask: true, stripeRows: true, columnLines: false, rowLines: false,
		title: 'Carga de Documentos', store: storeDetAcuse,
		columns:           [
			{
				width: 250, align: 'center', sortable: false, resizable: true,
				header: 'Nombre del IF', dataIndex: 'DET_ACUSE_NOMBRE_IF'
			},{
				width: 120,align: 'center', sortable: false, resizable: true,
				header: 'Tipo de Calendario', dataIndex: 'DET_ACUSE_TIPO_CAL'
			},{
				width: 70,align: 'center', sortable: false, resizable: true,
				header: 'A�o', dataIndex: 'DET_ACUSE_ANIO'
			},{
				width: 70, align: 'center', sortable: false, resizable: true,
				header: 'Mes', dataIndex: 'DET_ACUSE_MES'
			},{
				width: 150,align: 'center', sortable: false, resizable: true,
				header: 'Tipo de Informaci�n', dataIndex: 'DET_ACUSE_TIPO_INFO'
			},{
				width: 225,align: 'center', sortable: false, resizable: true,
				header: 'Archivo Cargado', dataIndex: 'DET_ACUSE_NOMBRE_ARCH'
			}
		],
		dockedItems: [{
				xtype: 'toolbar',
				dock: 'bottom',
				items: ['->','-',
					{
						text: 'Salir',
//						iconCls: 'icoSalir',
						scope: this,
						handler: function(btn){
							window.location = '40cargaSupervisionExt.jsp';
						}
					}
				]
			}
		]
	});
	
	var fpCargaArchivos = Ext.create('Ext.form.Panel',{
		name: 'fpCargaArchivos',
		id: 'fpCargaArchivos1',
		frame: true,
		width: 515,
		style: 'margin: 0 auto;',
		title: 'Carga de Archivo',
		fileUpload: true,
		items: [
			{
				xtype: 'combo', fieldLabel: 'Nombre del Intermediario', hidden: false, width: 500,
				itemId: 'id_if', name: 'n_if', hiddenName: 'n_if',
				forceSelection: true,emptyText: 'Seleccione...', queryMode: 'local', allowBlank: false,
				store: storeCatIntermediario, displayField: 'descripcion', valueField: 'clave',
				listeners:{
					select: fnEventSelectIFtipoCal
				}
			},
			{
				xtype: 'combo', fieldLabel: 'Tipo Calendario', hidden: false, width: 500,
				itemId: 'id_tipoCalendario', name: 'n_tipoCalendario', hiddenName: 'n_tipoCalendario',
				forceSelection: true,emptyText: 'Seleccione...', queryMode: 'local', allowBlank: false,
				store: storeCatTipoCalendario, displayField: 'descripcion', valueField: 'clave',
				listeners:{
					select: fnEventSelectIFtipoCal
				}
			},
			{
				xtype: 'fieldcontainer', fieldLabel: 'A�o', layout: 'hbox',
				defaults:{hideLabel: true},
				items:[
					{
						xtype: 'combo', hidden: false, width: 150, id:'id_anio',
						itemId: 'id_anio', name: 'n_anio', hiddenName: 'n_anio',
						forceSelection: true,emptyText: 'Seleccione...', queryMode: 'local', allowBlank: false,
						store: storeCatAnio, displayField: 'descripcion', valueField: 'clave',
						listeners:{
							select: fnEventSelectIFtipoCalAnio
						}
					},
					{
						xtype: 'displayfield', value: 'Mes:', margin: '0 35 0 35'
					},
					{
						xtype: 'combo',hidden: false, width: 150, id:'id_mes',
						itemId: 'id_mes', name: 'n_mes', hiddenName: 'n_mes',
						forceSelection: true,emptyText: 'Seleccione...', queryMode: 'local', allowBlank: false,
						store: storeCatMes, displayField: 'descripcion', valueField: 'clave'
					}
				]
			},
			{
				xtype: 'combo', fieldLabel: 'Tipo de Informaci�n', hidden: false, width: 500,
				itemId: 'id_tipoInfo', name: 'n_tipoInfo', hiddenName: 'n_tipoInfo',
				forceSelection: true,emptyText: 'Seleccione...', queryMode: 'local', allowBlank: true,
				store: storeCatTipoInformacion, displayField: 'descripcion', valueField: 'clave'
			},
			{
				xtype: 'fieldcontainer', layout: 'hbox',
				defaults:{hideLabel: true},
				items:[
					{	xtype: 'button', id: 'btnAyuda', iconCls: 'icoAyuda', handler: function(){fnMostrarAyuda();} 	},
					{
						xtype: 'displayfield', value: 'Carga de Archivo', margin: '0 10 0 10'
					},
					{
						xtype: 'filefield',  width: 350,
						name: 'n_archivoCarga', id: 'id_archivoCarga', emptyText: 'Ruta de Archivo...',
						allowBlank: false, buttonText: 'Examinar...',
						vtype: 	'archivoPdfXlsxZip',
						listeners: {
							change: function(fld, value){
								fld.setRawValue(value.substring(value.lastIndexOf("\\")+1));
								if(fld.isValid()){
									fnCargaArchivo('SUBIR_ARCHIVO',null);
								}
							}
						}
					},
					{
						xtype: 'button', id: 'btnPDFView', iconCls: 'icoBuscar', hidden:true,
						handler: function(){fnPDFViewer(nombreArchivo);}
					}
				]
			}
		],
		monitorValid: true,
		buttons: [
			{
				name: 'btnGuardar',
				id: 'btnGuardar',
				text: 'Guardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(){
					var  formaCrit =   Ext.ComponentQuery.query('#fpCargaArchivos1')[0];
					if(formaCrit.isValid()){
                                            var valorTipoInfo = formaCrit.query('#id_tipoInfo')[0].getValue();
                                            var valorAnio = formaCrit.query('#id_anio')[0].getValue();
                                            var valorMes = formaCrit.query('#id_mes')[0].getValue();
                                            var valorTipoCal = formaCrit.query('#id_tipoCalendario')[0].getValue();
                                            
                                            //ABR:: Para cualquier tipo de carga valido que el nombre del archivo corresponda con el periodo seleccionado.
                                            var validaArchivo = true;
                                            if(valorTipoCal==="S" && (valorTipoInfo==="A"||valorTipoInfo==="R")) {
                                                //alert(valorTipoCal);alert(valorTipoInfo);
                                                Ext.MessageBox.alert('Mensaje', 'No se pueden cargar Resultados para un calendario Semestral.', function(){
                                                    fnCargaArchivo("VALIDACIONES_SIN_EXITO", null);//Ya no hace nada
                                                });
                                                return false;
                                            }
                                            console.log("Valido aechivo");
                                            if(!validaNombreArchivo(nombreArchivo, valorMes, valorAnio)) {
                                                validaArchivo = false;
                                                var nombreTmp = "";
                                                if(valorTipoInfo==="R" || valorTipoInfo==="A"){
                                                    nombreTmp = "Resultado de supervision";
                                                } else if(valorTipoInfo==="D" || valorTipoInfo==="B") {
                                                    nombreTmp = "Dictamen de Supervisi�n";
                                                }
                                                console.log("validacion de archivo false");
                                                Ext.MessageBox.alert('Mensaje', 'El nombre del archivo de '+nombreTmp+' que intenta cargar' +
                                                ' no corresponde al periodo seleccionado ' + valorMes + '-' + valorAnio + '.', function(){
                                                    fnCargaArchivo("VALIDACIONES_SIN_EXITO", null);//Ya no hace nada
                                                });
                                            }
                                            
                                            if(validaArchivo==true) {
                                                console.log("Valido archivo true");
                                                if(valorTipoInfo==='D' || valorTipoInfo==='R') {
                                                    //ABR:: Si es tipo Dictamen de Supervisi�n o Resultados de Supervisi�n continua con el proceso de carga de archivo.
                                                    fnCargaArchivo('PROCESAR_ARCHIVO',null);
                                                } else {
                                                    fnCargaArchivo('VALIDA_ARCHIVO_EXISTENTE',null);
                                                }
                                            }

					}
				}
			},
			{
				name: 'btnLimpiar',
				id: 'btnLimpiar1',
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function(btn){
					fpCargaArchivos.getForm().reset();

				}
			}
		]
		
	});

	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 940,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			NE.util.getEspaciador(20),
			fpCargaArchivos,
			NE.util.getEspaciador(20),
			gpAcuse,
			NE.util.getEspaciador(20),
			gpDetalleAcuse
		]
	});
	
	storeCatIntermediario.load();

});