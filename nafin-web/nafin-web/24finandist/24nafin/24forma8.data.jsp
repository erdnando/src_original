<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String ic_moneda = (request.getParameter("ic_moneda") == null)?"1":(String)request.getParameter("ic_moneda");
String ic_pyme = (request.getParameter("ic_pyme") == null)?"":(String)request.getParameter("ic_pyme");
String combo = (request.getParameter("combo") == null)?"":(String)request.getParameter("combo");

String in_PlazoDias = (request.getParameter("in_PlazoDias") == null)?"":(String)request.getParameter("in_PlazoDias");
String pRelMat = (request.getParameter("pRelMat") == null)?"":(String)request.getParameter("pRelMat");
String pPuntos = (request.getParameter("pPuntos") == null)?"":(String)request.getParameter("pPuntos");
String clavesTasa 		= (request.getParameter("clavesTasa") == null) ? "" : request.getParameter("clavesTasa");

String pAccion = (request.getParameter("pAccion") == null)?"":(String)request.getParameter("pAccion");

if(ic_moneda.equals("") )  { ic_moneda = "1"; };

String Claves_a="",Valores="",Puntos="", lsMoneda="", lsTipoTasa="", lsCveTasa="", lsPlazo="", lsValor="", lsRelMat="", lsPuntos="", lsTasaPiso="", lsFecha = "";	

double tasaAplicar = 0;
int liRegistros  = 0, totalLineas  =0;
Vector lovTasas 	= null;
String existe 	= "N";
	

String infoRegresar ="", consulta ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

CapturaTasas  BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class); 


	
if (informacion.equals("valoresIniciales") ) {
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("combo","INICIALIZA");		
	infoRegresar = jsonObj.toString(); 	
	
}  else  if (informacion.equals("catalogoEPO") ) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTipoCredito("C"); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoIF") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveEpo(ic_epo); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoPYME") ) {

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cp.cg_razon_social"); 
	cat.setClaveEpo(ic_epo); 
	cat.setIntermediario(ic_if); 
	cat.setTipoCredito("C"); 	  	
	cat.setOrden("cp.cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoMoneda") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

} else if (informacion.equals("catalogoPlazo") ) {

	String  existeplazo = "N";
	String selected = "",icPlazo = "",inPlazoDias = "";
	Vector vecFilas = BeanTasas.getComboPlazo(4,ic_epo);
	for(int i=0;i<vecFilas.size();i++) {
		existeplazo= "S";
		Vector vecColumnas = (Vector)vecFilas.get(i);
		icPlazo = (String)vecColumnas.get(1);
		inPlazoDias = (String)vecColumnas.get(1);
		
		datos = new HashMap();
		datos.put("clave", icPlazo);
		datos.put("descripcion", inPlazoDias);
		registros.add(datos);
		
	}	
	if(existeplazo.equals("N") ) {
		datos = new HashMap();
		datos.put("clave", "0");
		datos.put("descripcion", "Plazos no disponibles para operar");
		registros.add(datos);
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("existeplazo",existeplazo);
	infoRegresar = jsonObj.toString(); 	
	
} else if (informacion.equals("validaPuntos") ) {
	String maxpuntos = "";
	try {
		maxpuntos = BeanTasas.getPuntosMaximosxIf("4",ic_if);
	} catch(NafinException ne) {
		mensaje = ne.getMsgError();
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("maxpuntos",maxpuntos);	
	jsonObj.put("mensaje",mensaje);	
	jsonObj.put("ic_if",ic_if);	
	jsonObj.put("combo",combo);		
	infoRegresar = jsonObj.toString(); 	
	
} else if (informacion.equals("validaTotaldeTasas") 
		|| informacion.equals("validaDatosMoneda")   )  {		
		
	totalLineas = BeanTasas.getTotalLineas("4",ic_epo,ic_if,ic_pyme,"54");
	lovTasas = BeanTasas.ovgetTasasxPyme(4,ic_pyme,ic_if,ic_moneda);			
	liRegistros = lovTasas.size();

		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("totalLineas",String.valueOf(totalLineas));	
	jsonObj.put("liRegistros",String.valueOf(liRegistros));	
   jsonObj.put("mensaje",mensaje);		
	jsonObj.put("combo",combo);	
	infoRegresar = jsonObj.toString(); 	
 

} else if (informacion.equals("Consultar") ) {

	lovTasas = BeanTasas.ovgetTasasxPyme(4,ic_pyme,ic_if,ic_moneda);			
	liRegistros = lovTasas.size();			
	if (liRegistros==0) {
		lovTasas = BeanTasas.getTasasDispxPyme("4",ic_pyme,ic_if,ic_moneda,in_PlazoDias);		
		liRegistros = lovTasas.size();
		existe = "S";
	}
		
	for (int i=0; i<liRegistros; i++) {
		Vector lovDatosTasa = (Vector)lovTasas.get(i);
		lsCveTasa	= lovDatosTasa.get(2).toString();
		lsMoneda	= lovDatosTasa.get(0).toString();
		lsTipoTasa	= lovDatosTasa.get(1).toString();
		lsPlazo 	= lovDatosTasa.get(3).toString();
		lsValor 	= lovDatosTasa.get(4).toString();
		if (existe.equals("N") ) {
			lsFecha 	= lovDatosTasa.get(8).toString();
		}
		if (existe.equals("S")) {
			if ("+".equals(pRelMat))
				tasaAplicar = Double.parseDouble(lsValor) + Double.parseDouble(pPuntos);
			else
				if ("-".equals(pRelMat))
					tasaAplicar = Double.parseDouble(lsValor) - Double.parseDouble(pPuntos);
				else
					if ("*".equals(pRelMat))
						tasaAplicar = Double.parseDouble(lsValor) * Double.parseDouble(pPuntos);
					else 
						tasaAplicar = Double.parseDouble(lsValor) / Double.parseDouble(pPuntos);
			lsRelMat = pRelMat;
			lsPuntos = pPuntos;
			lsTasaPiso = ""+tasaAplicar;
		} else{
			lsRelMat = lovDatosTasa.get(5).toString();
			lsPuntos = lovDatosTasa.get(6).toString();
			lsTasaPiso = lovDatosTasa.get(7).toString();
		}
		if (i==0) {
			Claves_a = lsCveTasa;
			Valores = lsValor;
		}
		else {
			Claves_a += ","+lsCveTasa;
			Valores += ","+lsValor;			
		}
		
		datos = new HashMap();
		datos.put("IC_MONEDA",ic_moneda);
		datos.put("MONEDA",lsMoneda);
		datos.put("TIPO_TASA",lsTipoTasa);
		datos.put("PLAZO",lsPlazo);
		datos.put("VALOR",Comunes.formatoDecimal(Double.parseDouble(lsValor),2));
		datos.put("REL_MAT",lsRelMat);
		datos.put("PUNTOS_ADICIONALES",Comunes.formatoDecimal(Double.parseDouble(lsPuntos),2));
		datos.put("TASA_APLICAR",Comunes.formatoDecimal(Double.parseDouble(lsTasaPiso),2));			
		datos.put("FECHA_ACTUALIZACION",lsFecha);			
		registros.add(datos);		
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("existe",existe);	
	jsonObj.put("clavesTasa",Claves_a);	
	jsonObj.put("pAccion",pAccion);	
	infoRegresar = jsonObj.toString(); 		

} else if (informacion.equals("Agregar_Modificar_Tasas")  ) {

	boolean OK = false;	
	try {
	
		OK = BeanTasas.setTasasxPyme(ic_pyme,ic_if,pAccion,4,clavesTasa,pRelMat,pPuntos);

	} catch(NafinException ne) {
		mensaje = ne.getMsgError();	
	}	

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje",mensaje);		
	infoRegresar = jsonObj.toString(); 	
 
} 


%>
<%=infoRegresar%>