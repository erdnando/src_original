Ext.onReady(function() {

	var hayRegistros=0;

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('NUM_ACUSE_CARGA');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
		
			var btnArchivoPDF = Ext.getCmp('btnArchivoPDF');
			btnArchivoPDF.setIconClass('icoPdf');
			var btnArchivoCSV = Ext.getCmp('btnArchivoCSV');
			btnArchivoCSV.setIconClass('icoXls');
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,	
		width:	'100',
		heigth:	'auto',		
		items: [
			{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var fp = Ext.getCmp("forma");	
						var barraPaginacion1 = Ext.getCmp("barraPaginacion1");
						var barraPaginacion2 = Ext.getCmp("barraPaginacion2");								
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '24consulta1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF',
								start: barraPaginacion1.cursor,
								limit: barraPaginacion1.pageSize,
								start2: barraPaginacion2.cursor,
								limit2: barraPaginacion2.pageSize,
								tipo_credito:Ext.getCmp("tipo_credito1").getValue()
							}),
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'displayfield',
					value: ' ',					 
					width: 30
				},
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {
						
						var fp = Ext.getCmp("forma");	
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV',
								tipo_credito:Ext.getCmp("tipo_credito1").getValue()
							}),	
							callback: procesarDescargaArchivos
						});						
					}
				}
		]
	});
	
	//************************VER Cambios del documento**************************
	
		//------Consulta  procesarConsultaData_1   ------------------------
	var procesarConsDetalle = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridDetalle = Ext.getCmp('gridDetalle');	
		var el = gridDetalle.getGridEl();	
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridDetalle.getColumnModel();
		
			gridDetalle.setTitle('<div align="center"><b> </div>');
		
			if(store.getTotalCount() > 0) {
				hayRegistros++;
				el.unmask();					
			} else {						
				el.mask('No se encontr� ning�n cambio para este documento', 'x-mask');				
			}
		}
	}
	
	var consDetalles   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'consDetalles',
			tipoConsulta:'Detalle'
		},		
		fields: [			
			{name: 'FECHA_CAMBIO'},
			{name: 'CAMBIO_ESTATUS'},
			{name: 'F_EMISION_ANTERIOR'},
			{name: 'F_EMISION_NUEVA'},			
			{name: 'MONTO_ANTERIOR'},
			{name: 'MONTO_NUEVO'},
			{name: 'F_VENCIMIENTO_ANTERIOR'},
			{name: 'F_VENCIMIENTO_NUEVO'},
			{name: 'MODALIDAD_ANTERIOR'},
			{name: 'MODALIDAD_NUEVO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalle,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalle(null, null, null);					
				}
			}
		}		
	});
	
	var gridDetalle =[{
		id: 'gridDetalle',		
		xtype:'grid',
		title:'<div align="center"><b> Procesado .... </div> ',	
		store: consDetalles,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Fecha del Cambio',
				tooltip: 'Fecha del Cambio',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				width: 150,
				align: 'center'				
			},
			{
				header: 'Cambio Estatus',
				tooltip: 'Cambio Estatus ',
				dataIndex: 'CAMBIO_ESTATUS',
				sortable: true,
				width: 150,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Emisi�n Anterior',
				tooltip: 'Fecha Emisi�n Anterior',
				dataIndex: 'F_EMISION_ANTERIOR',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Fecha Emision Nueva',
				tooltip: 'Fecha Emision Nueva',
				dataIndex: 'F_EMISION_NUEVA',
				sortable: true,
				width: 150,
				align: 'center'			
			}, 			
			{
				header: 'Monto Anterior',
				tooltip: 'Monto Anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable: true,
				width: 150,
				align: 'right',				
				renderer:function(value,metadata,registro){                                
					if(registro.data['MONTO_ANTERIOR'] ==0) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');;
					}
				}					
			},
			{
				header: 'Monto Nuevo',
				tooltip: 'Monto Nuevo',
				dataIndex: 'MONTO_NUEVO',
				sortable: true,
				width: 150,
				align: 'right',
				renderer:function(value,metadata,registro){                                
				if(registro.data['MONTO_NUEVO'] ==0) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');;
					}
				}			
			},
			{
				header: 'Fecha Vencimiento Anterior',
				tooltip: 'Fecha Vencimiento Anterior',
				dataIndex: 'F_VENCIMIENTO_ANTERIOR',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Fecha Vencimiento Nuevo',
				tooltip: 'Fecha Vencimiento Nuevo',
				dataIndex: 'F_VENCIMIENTO_NUEVO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Modalidad de Plazo Anterior',
				tooltip: 'Modalidad de Plazo Anterior',
				dataIndex: 'MODALIDAD_ANTERIOR',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Modalidad de Plazo Nuevo',
				tooltip: 'Modalidad de Plazo Nuevo',
				dataIndex: 'MODALIDAD_NUEVO',
				sortable: true,
				width: 150,
				align: 'center'			
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 680,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	var verDetalleCambios= function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ventana = Ext.getCmp('VerDetalle');	
		
		consDetalles.load({
			params: Ext.apply(fp.getForm().getValues(),{					
				informacion: 'consDetalles',					
				tipoConsulta:'Detalle',
				ic_documento:registro.get('IC_DOCUMENTO')
			})
		});	
							
		if (ventana) {
			ventana.show();			
		} else {
			new Ext.Window({	
				width: 710,	
				id:'VerDetalle',				
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,	
				items: [		
					gridDetalle
				],
				title: 'Cambios del documento',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Salir ',								
							iconCls: 'icoLimpiar', 	
							id: 'btnSalir_1', 
							handler: function(){
								Ext.getCmp('VerDetalle').destroy();	
							}					
						}
					]
				}		
			}).show();		
		}
	}
		

		
		
//------Consulta  procesarConsultaData_2   ------------------------
	var procesarConsultaData_2 = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta_2 = Ext.getCmp('gridConsulta_2');	
		var el = gridConsulta_2.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta_2.isVisible()) {
				gridConsulta_2.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta_2.getColumnModel();
			if( Ext.getCmp('mostrarBotones').getValue()=='S'  ||  store.getTotalCount() > 0 ) {
				Ext.getCmp('fpBotones').show();	
			}	else  if( Ext.getCmp('mostrarBotones').getValue()=='N'  &&   store.getTotalCount() == 0 ) {
				Ext.getCmp('fpBotones').hide();	
			}
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('barraPaginacion2').show();	
				Ext.getCmp('gridTotales_2').show();	
				el.unmask();							
				consTotales_2.load({
					params: Ext.apply(fp.getForm().getValues(),{					
					informacion: 'ConsTotales_2',
						tipoConsulta:'2'
					})
				});	
				hayRegistros++;
			} else {		
				Ext.getCmp('barraPaginacion2').hide();	
				Ext.getCmp('gridTotales_2').hide();					
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}  
		
		if(hayRegistros==0)  {
			Ext.getCmp('fpBotones').hide();	
		}else {
			Ext.getCmp('fpBotones').show();	
		}
		
	}
	
	
	var consTotales_2   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'ConsTotales_2',
			tipoConsulta:'2'
		},		
		fields: [			
			{	name: 'MONEDA'},
			{	name: 'TOTAL_DOCTOS'},			
			{	name: 'TOTAL_MONTO'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);					
				}
			}
		}		
	});	
	
	var gridTotales_2 = new Ext.grid.EditorGridPanel({	
		store: consTotales_2,
		id: 'gridTotales_2',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '<div align="center"><b> </div>',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'TOTAL_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false		
	});	
	
	var enviaParametros =  function(store, options){
		var ic_estatus_docto = Ext.getCmp('forma').getForm().findField("ic_estatus_docto1").getValue();
		var tipo_credito = Ext.getCmp('forma').getForm().findField("tipo_credito1").getValue();		
		Ext.apply(options.params, {ic_estatus_docto: ic_estatus_docto,tipo_credito:tipo_credito });
	}
	
			
	var consultaData_2   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'Consultar_2',
			tipoConsulta:'2'				
		},		
		fields: [			
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_PYME'},
			{	name: 'NUM_ACUSE_CARGA'},
			{	name: 'NUM_DOCTO_INICIAL'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'FECHA_EMISION'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'PLAZO_DOCTO'},
			{	name: 'MONEDA'},
			{	name: 'MONTO'},
			{	name: 'PLAZO_DESC_DIAS'},
			{	name: 'PORCENTAJE_DESC'},
			{	name: 'MONTO_DESCUENTO'},			
			{	name: 'TIPO_CONVENIO'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODALIDAD_PLAZO'},
			{	name: 'ESTATUS'},
			{	name: 'TIPO_CREDITO'},
			{	name: 'NUM_DOCTO_FINAL'},
			{	name: 'PLAZO_FINAL'},
			{	name: 'FECHA_VENC_FINAL'},
			{	name: 'FECHA_OPERA_FINAL'},			
			{	name: 'NOMBRE_IF'},
			{	name: 'REFERENCIA_TASA_INT'},
			{	name: 'VALOR_TASA_INT'},
			{	name: 'MONTO_INTERES'},
			{	name: 'TIPO_COBRO_INT'},
			{	name: 'IC_MONEDA'},
			{	name: 'IC_MONEDA_LINEA'},
			{	name: 'CG_VENTACARTERA'},
			{	name: 'BINS'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
			{	name: 'COMISION_APLICABLE'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
			{	name: 'MONTO_COMISION'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)
			{	name: 'MONTO_DEPOSITAR'},//FODEA-013-2014 MOD(SE AGREGARON LAS NUEVAS COLUMNAS AL GRID)			
			{ name : 'OPERA_TARJETA'},
			{ 	name : 'NUM_TC'},
			{ 	name : 'TIPOPAGO'},
			{ 	name : 'MUESTRA_VISOR'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: { 
			beforeLoad: enviaParametros,		 	
			load: procesarConsultaData_2,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData_2(null, null, null);					
				}
			}
		}		
	});	

	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Documento', colspan: 19, align: 'center'},
					{header: 'Datos del Financiamiento', colspan: 11, align: 'center'}					
				]
			]
	});
	
	var gridConsulta_2 = new Ext.grid.EditorGridPanel({	
		store: consultaData_2,
		id: 'gridConsulta_2',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '<div align="center"><b>&nbsp; </div>',
		clicksToEdit: 1,
		hidden: true,	
		plugins: grupos,	
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				width:     180,
				xtype:     'actioncolumn',
				header:    'N�m. acuse carga',
				tooltip:   'N�m. acuse carga',
				dataIndex: 'NUM_ACUSE_CARGA',
				align:    'center',
				sortable:  true,
				resizable: true,
				renderer: function(value, metadata, record, rowindex, colindex, store){
					return (record.get('NUM_ACUSE_CARGA'));
				},
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('NUM_ACUSE_CARGA') != '' && registro.get('MUESTRA_VISOR') == 'S'){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						} else{
							return value;
						}
					},
					handler: function(gridConsulta_2, rowIndex, colIndex){
						var rec = consultaData_2.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'NUM_DOCTO_INICIAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Moneda.',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Plazo para descuento en dias.',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'PLAZO_DESC_DIAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},			
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVENIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  value;
					}
				}				
			},
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');;
					}
				}	
			},
			{
				header: 'Monto valuado en Pesos',
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				width: 150,			
				resizable: true,								
				align: 'right',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');;
					}
				}				
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					if(registro.data['CG_VENTACARTERA'] =='S') {
						return '';
					}else {
						return  value;
					}
				}
			},
			{
				header: 'Tipo de Pago',
				tooltip: 'Tipo de Pago',
				dataIndex: 'TIPOPAGO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'							
			},			
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'						
			},
			{
				xtype: 'actioncolumn',
				header: 'Cambios del documento',
				tooltip: 'Cambios del documento',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';										
						}
						,handler: verDetalleCambios
					}
				]				
			},
			{
				header: 'Tipo de cr�dito',
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'						
			},
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'NUM_DOCTO_FINAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'						
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					var valor =  registro.get('MONTO')-  registro.get('MONTO_DESCUENTO');
					return Ext.util.Format.number(valor,'$0,0.00');		
				}
			},
			{
				header: '% Comisi�n<BR>Aplicable de<br> Terceros',
				tooltip: '% Comisi�n Aplicable de Terceros',
				dataIndex: 'COMISION_APLICABLE',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center',//OPERA_TARJETA
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					if ( (registro.get('ESTATUS') != 'Operada TC')){//registro.get('OPERA_TARJETA') == 'N') ||
						 return value = 'N/A';
					}else{
						return Ext.util.Format.number(value,'0.00%');
					}
				}
			},
			{
				header: 'Monto Comisi�n<br>de Terceros<br>(IVA Incluido)',//'Monto Comisi�n<br>Terceros',
				tooltip: 'Monto Comisi�n de Terceros (IVA Incluido)',//'Monto Comisi�n Terceros',
				dataIndex: 'MONTO_COMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
					if ( (registro.get('ESTATUS') != 'Operada TC')){//registro.get('OPERA_TARJETA') == 'N') ||
						return value = 'N/A';
					}else{
						return '<div align=right >'+Ext.util.Format.number(value,'$0,000.00')+'</div>';
					}
				}
			},
			{
				header: 'Monto a Depositar<br>por Operaci�n',
				tooltip: 'Monto a Depositar por Operaci�n',
				dataIndex: 'MONTO_DEPOSITAR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
					if ( (registro.get('ESTATUS') != 'Operada TC')){//registro.get('OPERA_TARJETA') == 'N') ||
						return value = 'N/A';
					}else{
						return '<div align=right >'+Ext.util.Format.number(value,'$0,000.00')+'</div>';
					}
				}
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO_FINAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') == 'Operada TC'){
						return value = 'N/A';
					}else{
						return value;
					}
				}						
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_FINAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') == 'Operada TC'){
						return value = 'N/A';
					}else{
						return value;
					}
				}						
			},
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERA_FINAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'						
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre del Producto',//"Bin's",
				tooltip: 'Nombre del Producto',//"Bin's",
				dataIndex: 'BINS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') != 'Operada TC'){
						value = 'N/A';
					}
					return value;
				}
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, registro, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					//return value;
								if (registro.get('ESTATUS') == 'Operada TC'){
									value = 'N/A';
								}
							return value;
						}
				
			},
			{
				header: 'Valor tasa de inter�s',
				tooltip: 'Valor tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									if (registro.get('ESTATUS') == 'Operada TC'){
											value = 'N/A';
											return value;
									}else{
										return Ext.util.Format.number(value, '0.00%');
									}
									
								}
			},
			{
				header: 'Monto de intereses',
				tooltip: 'Monto de intereses',
				dataIndex: 'MONTO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
									//if (registro.get('ESTATUS') == 'Negociable'){
									//	value = '';
									//}
									if (registro.get('ESTATUS') == 'Operada TC'){
											value = 'N/A';
											return value;
									}else{
										return Ext.util.Format.number(value, '$0,0.00');
									}
								}
			},
			{
				header: 'Tipo de cobro inter�s',
				tooltip: 'Tipo de cobro inter�s',
				dataIndex: 'TIPO_COBRO_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'					
			},
			{
				header:'N�mero Tarjeta<br>de Cr�dito ',
				tooltip:'Fecha de<br>Registro',
				dataIndex:'NUM_TC',
				sortable : true,	width : 200,	align: 'center', hidden: false,
				renderer:	function(value, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get('ESTATUS') != 'Operada TC'){
						value = 'N/A';
					}else {
						if(value != ''){
							value = 'XXXX-XXXX-XXXX-'+value;
						}
						
					}
					return value;
				}
			}	
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion2',
			displayInfo: true,
			store: consultaData_2,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros."
		}
		});
			
							

	//------Consulta  procesarConsultaData_1   ------------------------
	var procesarConsultaData_1 = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta_1 = Ext.getCmp('gridConsulta_1');	
		var el = gridConsulta_1.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta_1.isVisible()) {
				gridConsulta_1.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta_1.getColumnModel();
			
			if(jsonData.tipo_credito=='D')  {
				gridConsulta_1.setTitle('<div align="center"><b>Datos Documento Inicial </div>');
			}else  {
				gridConsulta_1.setTitle('<div align="center"><b>Documento </div>');
			}
		
			if(store.getTotalCount() > 0) {		
				Ext.getCmp('barraPaginacion1').show();	
				Ext.getCmp('gridTotales_1').show();	
				Ext.getCmp('mostrarBotones').setValue('S');	
				Ext.getCmp('fpBotones').show();	
				el.unmask();	
				consTotales_1.load({
					params: Ext.apply(fp.getForm().getValues(),{					
						informacion: 'consTotales_1'						
					})
				});
				
			} else {		
				Ext.getCmp('gridTotales_1').hide();					
				Ext.getCmp('barraPaginacion1').hide();	
				Ext.getCmp('mostrarBotones').setValue('N');	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	
	var consTotales_1   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'consTotales_1',
			tipoConsulta:'1'
		},		
		fields: [			
			{	name: 'MONEDA'},
			{	name: 'TOTAL_DOCTOS'},			
			{	name: 'TOTAL_MONTO'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);					
				}
			}
		}		
	});	
	
	var gridTotales_1 = new Ext.grid.EditorGridPanel({	
		store: consTotales_1,
		id: 'gridTotales_1',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '<div align="center"><b> </div>',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'TOTAL_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false		
	});	
				
			
	var consultaData_1   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'Consultar_1',
			tipoConsulta:'1'
		},		
		fields: [			
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_PYME'},
			{	name: 'NUM_ACUSE_CARGA'},
			{	name: 'NUM_DOCTO_INICIAL'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'FECHA_EMISION'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'PLAZO_DOCTO'},
			{	name: 'MONEDA'},
			{	name: 'MONTO'},
			{	name: 'PLAZO_DESC_DIAS'},
			{	name: 'PORCENTAJE_DESC'},
			{	name: 'MONTO_DESCUENTO'},
			{	name: 'TIPO_CONVENIO'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODALIDAD_PLAZO'},
			{	name: 'ESTATUS'},			
			{	name: 'IC_DOCUMENTO'},
			{	name: 'IC_MONEDA'},
			{	name: 'IC_MONEDA_LINEA'},
			{	name: 'CG_VENTACARTERA'},
			{	name: 'TIPOPAGO'},
			{ 	name : 'MUESTRA_VISOR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData_1,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData_1(null, null, null);					
				}
			}
		}		
	});	

	
	var gridConsulta_1 = new Ext.grid.EditorGridPanel({	
		store: consultaData_1,
		id: 'gridConsulta_1',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '<div align="center"><b>Documento </div>',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				width:     180,
				xtype:     'actioncolumn',
				header:    'N�m. acuse carga',
				tooltip:   'N�m. acuse carga',
				dataIndex: 'NUM_ACUSE_CARGA',
				align:     'center',
				sortable:  true,
				resizable: true,
				renderer: function(value, metadata, record, rowindex, colindex, store){
					return (record.get('NUM_ACUSE_CARGA'));
				},
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('NUM_ACUSE_CARGA') != '' && registro.get('MUESTRA_VISOR') == 'S'){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						} else{
							return value;
						}
					},
					handler: function(gridConsulta_1, rowIndex, colIndex){
						var rec = consultaData_1.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'NUM_DOCTO_INICIAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha de publicaci�n',
				tooltip: 'Fecha de publicaci�n',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Fecha vencimiento',
				tooltip: 'Fecha vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Moneda.',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Plazo para descuento en dias.',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'PLAZO_DESC_DIAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Tipo conv.',
				tooltip: 'Tipo conv.',
				dataIndex: 'TIPO_CONVENIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  value;
					}
				}				
			},
			{
				header: 'Tipo cambio',
				tooltip: 'Tipo cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');
					}
				}	
			},
			{
				header: 'Monto valuado en Pesos',
				tooltip: 'Monto valuado en Pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				width: 150,			
				resizable: true,								
				align: 'right',
				renderer:function(value,metadata,registro){                                
					if(registro.data['IC_MONEDA'] ==registro.data['IC_MONEDA_LINEA']) {
						return '';
					}else {
						return  Ext.util.Format.number(value, '$0,0.00');
					}
				}				
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){                                
					if(registro.data['CG_VENTACARTERA'] =='S') {
						return '';
					}else {
						return  value;
					}
				}				
			},
			{
				header: 'Tipo de Pago',
				tooltip: 'Tipo de Pago',
				dataIndex: 'TIPOPAGO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'							
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'						
			},
			{
				xtype: 'actioncolumn',
				header: 'Cambios del documento',
				tooltip: 'Cambios del documento',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';										
						}
						,handler: verDetalleCambios
					}
				]				
			}			
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion1',
			displayInfo: true,
			store: consultaData_1,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros."			
		}
	});
			
			
	//*********************Criterios de Busqueda ************************
	
	 var dataTipoPago = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['','Seleccionar'],
			['1','Financiamiento con intereses '],
			['2','Meses sin intereses '],
			['3','Tarjeta de Cr�dito']
		]		
	 });
	 
	 
	var catEPOData = new Ext.data.JsonStore({
		id: 'catEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catEPOData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catPymeData = new Ext.data.JsonStore({
		id: 'catPymeData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catPymeData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catIFData = new Ext.data.JsonStore({
		id: 'catIFData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catIFData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catBINSData = new Ext.data.JsonStore({
		id: 'catBINSData',
		root : 'registros',
		fields : ['CLAVE', 'DESCRIPCION', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catalogoBINS'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catEstatusData = new Ext.data.JsonStore({
		id: 'catEstatusData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catEstatusData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:function(v,C){
			},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catTipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['D','Descuento y/o Factoraje'],
			['C','Cr�dito en Cuenta Corriente']			
		 ]
	});
	
	var catCobroInteresData = new Ext.data.JsonStore({
		id: 'catCobroInteresData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catCobroInteresData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catMonedaData = new Ext.data.JsonStore({
		id: 'catMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catMonedaData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catModalidadData = new Ext.data.JsonStore({
		id: 'catModalidadData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta1.data.jsp',
		baseParams: {
			informacion: 'catModalidadData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var  elementosForma  = [
		{
			xtype: 'compositefield',  
			fieldLabel: 'EPO',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_epo',
					id: 'ic_epo1',
					fieldLabel: 'EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_epo',
					emptyText: 'Seleccione EPO...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catEPOData,
					width: 270,
					tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
					setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
					}, 
					listeners: {
						select:{ 
							fn:function (combo) {
							
								Ext.getCmp('ic_pyme1').setValue('');	
								Ext.getCmp('ic_if1').setValue('');									
					
								catIFData.load({
									params: {
										ic_epo:combo.getValue()
									}				
								});	
								catPymeData.load({
									params: {
										ic_epo:combo.getValue()
									}				
								});							
							}
						}
					}					
				},
				{
					xtype: 'displayfield',
					value: 'N�mero de documento final',
					width: 100
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'N�mero de documento final',
					name: 'ic_documento',
					id: 'ic_documento1',
					allowBlank: true,
					maxLength: 12,
					width: 100,
					msgTarget: 'side',				
					margins: '0 20 0 0'
				}		
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Distribuidor',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: 'Distribuidor',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pyme',
					emptyText: 'Seleccione Distribuidor...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catPymeData,
					width: 270,
					tpl:'<tpl for=".">' +
					'<tpl if="!Ext.isEmpty(loadMsg)">'+
					'<div class="loading-indicator">{loadMsg}</div>'+
					'</tpl>'+
					'<tpl if="Ext.isEmpty(loadMsg)">'+
					'<div class="x-combo-list-item">' +
					'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
					'</div></tpl></tpl>',
					setNewEmptyText: function(emptyTextMsg){
					this.emptyText = emptyTextMsg;
					this.setValue('');
					this.applyEmptyText();
					this.clearInvalid();
					}
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de cr�dito',
					width: 100
				},
				{
					xtype: 'combo',	
					name: 'tipo_credito',	
					id: 'tipo_credito1',	
					fieldLabel: '', 
					mode: 'local',	
					hiddenName : 'tipo_credito',	
					emptyText: 'Seleccione tipo de cr�dito ... ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catTipoCreditoData,	
					displayField : 'descripcion',	
					valueField : 'clave',
					width: 250,
					listeners:{
						select:	function(combo){				
							
							if(combo.getValue() == 'D' ){
								Ext.getCmp('ic_tipo_Pago1').hide(); 
								Ext.getCmp('lbic_tipo_Pago1').hide(); 
								
							}else if(combo.getValue() == 'C' ){
								Ext.getCmp('ic_tipo_Pago1').show(); 
								Ext.getCmp('lbic_tipo_Pago1').show(); 
							}
						
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'IF',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_if',
					id: 'ic_if1',
					fieldLabel: 'IF',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_if',
					emptyText: 'Seleccione IF...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catIFData,//catBINSData
					width: 270,
					tpl:'<tpl for=".">' +
						'<tpl if="!Ext.isEmpty(loadMsg)">'+
						'<div class="loading-indicator">{loadMsg}</div>'+
						'</tpl>'+
						'<tpl if="Ext.isEmpty(loadMsg)">'+
						'<div class="x-combo-list-item">' +
						'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
						'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
						this.emptyText = emptyTextMsg;
						this.setValue('');
						this.applyEmptyText();
						this.clearInvalid();
					},
					listeners: {
						select:{ 
							fn:function (combo) {
								catBINSData.load({
									params: {
										ic_if:combo.getValue()
									}				
								});	
							}
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de Pago',
					width: 100,
					id: 'lbic_tipo_Pago1'
				},
				{
					xtype: 'combo',
					name: 'ic_tipo_Pago',
					id: 'ic_tipo_Pago1',
					fieldLabel: 'Tipo de Pago',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_tipo_Pago',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',					
					store : dataTipoPago				
				}		
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre del Producto',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'combo',
					name: 'catBins',
					id: 'cboBins',
					fieldLabel: 'Nombre del Producto',//"Bin's",
					mode: 'local', 
					displayField : 'DESCRIPCION',
					valueField : 'CLAVE',
					hiddenName : 'catBins',
					emptyText: 'Seleccione ...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catBINSData,
					width: 270,
					anchor:'48%'//,
					//tpl : NE.util.templateMensajeCargaCombo
				},
		
				{
					xtype: 'displayfield',
					value: 'Fecha de operaci�n',
					width: 100
				},
				{
					xtype: 'datefield',
					name: 'fecha_seleccion_de',
					id: 'fecha_seleccion_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fecha_seleccion_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_seleccion_a',
					id: 'fecha_seleccion_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_seleccion_de',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_estatus_docto',
					id: 'ic_estatus_docto1',
					fieldLabel: 'IF',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_estatus_docto',
					emptyText: 'Seleccione estatus...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catEstatusData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'displayfield',
					value: 'Monto',
					width: 100
				},
				{
					xtype: 'textfield',
					name: 'monto_credito_de',
					id: 'monto_credito_de',
					fieldLabel: 'Monto entre',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				},
				{
					xtype: 'displayfield',
					value: ' a',					 
					width: 20
				},				
				{
					xtype: 'textfield',
					name: 'monto_credito_a',
					id: 'monto_credito_a',
					fieldLabel: 'Monto entre',				
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				}	
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto1',
					fieldLabel: 'N�mero de documento inicial',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	
					width: 255,
					msgTarget: 'side',						
					margins: '0 20 0 0'					
				},
				{
					xtype: 'displayfield',
					value: 'Fecha vencimiento docto. final',
					width: 100
				},						
				
				{
					xtype: 'datefield',
					name: 'fecha_vto_credito_de',
					id: 'fecha_vto_credito_de',
					fieldLabel: 'Fecha vencimiento docto. final',	
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vto_credito_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vto_credito_a',
					id: 'fecha_vto_credito_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_vto_credito_a',
					margins: '0 20 0 0'					
				}					
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha vencimiento docto',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'datefield',
					name: 'fecha_vto_de',
					id: 'fecha_vto_de',
					fieldLabel: 'Fecha vencimiento docto',	
					allowBlank: true,
					startDay: 0,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vto_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vto_a',
					id: 'fecha_vto_a',
					allowBlank: true,
					startDay: 1,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_vto_de',
					margins: '0 20 0 0'					
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de cobro de inter�s',
					width: 100
				},
				{
					xtype: 'combo',
					name: 'ic_tipo_cobro_int',
					id: 'ic_tipo_cobro_int1',
					fieldLabel: 'IF',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_tipo_cobro_int',
					emptyText: 'Seleccione tipo cobro de inter�s ...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catCobroInteresData,
					width: 250,
					tpl : NE.util.templateMensajeCargaCombo
				}
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_de',
					id: 'fecha_publicacion_de',
					fieldLabel: 'Fecha de publicaci�n',	
					allowBlank: true,
					startDay: 0,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_publicacion_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_publicacion_a',
					id: 'fecha_publicacion_a',
					allowBlank: true,
					startDay: 1,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_publicacion_de',
					margins: '0 20 0 0'					
				},
				{
					xtype: 'displayfield',
					value: 'Fecha emisi�n docto.',
					width: 100
				},			
				
				{
					xtype: 'datefield',
					name: 'fecha_emision_de',
					id: 'fecha_emision_de',
					fieldLabel: 'Fecha de publicaci�n',	
					allowBlank: true,
					startDay: 0,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_emision_a',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_emision_a',
					id: 'fecha_emision_a',
					allowBlank: true,
					startDay: 1,
					width: 105,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'fecha_emision_de',
					margins: '0 20 0 0'					
				}					
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione Moneda...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catMonedaData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				},
				{
					xtype: 'displayfield',
					value: 'N�m. acuse de carga',
					width: 100
				},
				{
					xtype: 'textfield',
					fieldLabel: 'N�m. acuse de carga',
					name: 'cc_acuse',
					id: 'cc_acuse',
					allowBlank: true,
					maxLength: 17,
					width: 120,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}		
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de',
					fieldLabel: 'Monto',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				},
				{
					xtype: 'displayfield',
					value: ' a',					 
					width: 20
				},				
				{
					xtype: 'textfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a',
					fieldLabel: 'Monto entre',				
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				}	
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype			: 'checkbox',
					id				: 'monto_con_descuento1',
					name			: 'monto_con_descuento',
					hiddenName	: 'convenio',
					fieldLabel	: '',
					tabIndex		: 29,
					enable 		: true
				},
				{
					xtype: 'displayfield',
					value: ' Monto % de descuento',					 
					width: 150
				},	
				
				{
					xtype			: 'checkbox',
					id				: 'chk_proc_desc1',
					name			: 'chk_proc_desc',
					hiddenName	: 'convenio',
					fieldLabel	: '',
					tabIndex		: 29,
					enable 		: true
				},
				{
					xtype: 'displayfield',
					value: ' Informaci�n procesada en descuento autom�tico',					 
					width: 300
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype			: 'checkbox',
					id				: 'solo_cambio1',
					name			: 'solo_cambio',
					hiddenName	: 'convenio',
					fieldLabel	: '',
					tabIndex		: 29,
					enable 		: true
				},
				{
					xtype: 'displayfield',
					value: ' Solo documentos modificados',					 
					width: 200
				}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Modalidad del plazo',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'modo_plazo',
					id: 'modo_plazo1',
					fieldLabel: 'EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'modo_plazo',
					emptyText: 'Seleccione modalidad de plazo...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,			
					store : catModalidadData,
					width: 270,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'mostrarBotones', 	value: '' }
	];
	
		var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		monitorValid: true,
		title: 'Informacion de Documentos y Cr�ditos',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {
				
					var fecha_seleccion_de  = Ext.getCmp('fecha_seleccion_de');
					var fecha_seleccion_a  = Ext.getCmp('fecha_seleccion_a');
					
					if(!Ext.isEmpty(fecha_seleccion_de.getValue()) || !Ext.isEmpty(fecha_seleccion_a.getValue())){
						if(Ext.isEmpty(fecha_seleccion_de.getValue())){
							fecha_seleccion_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_seleccion_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_seleccion_a.getValue())){
							fecha_seleccion_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_seleccion_a.focus();
							return;
						}
					}
					
					var monto_credito_de  = Ext.getCmp('monto_credito_de');
					var monto_credito_a  = Ext.getCmp('monto_credito_a');
					
					if(!Ext.isEmpty(monto_credito_de.getValue()) || !Ext.isEmpty(monto_credito_a.getValue())){
						if(Ext.isEmpty(monto_credito_de.getValue())){
							monto_credito_de.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							monto_credito_de.focus();
							return;
						}
						if(Ext.isEmpty(monto_credito_a.getValue())){
							monto_credito_a.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							monto_credito_a.focus();
							return;
						}
					}
					if(!Ext.isEmpty(monto_credito_de.getValue()) && !Ext.isEmpty(monto_credito_a.getValue())){
						if(parseFloat(monto_credito_de.getValue().replace(/,/g,''))>parseFloat(monto_credito_a.getValue().replace(/,/g,''))){					
							console.dir(parseFloat(monto_credito_de.getValue()));
							console.dir(parseFloat(monto_credito_a.getValue()));
							monto_credito_de.markInvalid('El monto del credito a no puede ser mayor que el monto del cr�dito de');
							monto_credito_de.focus();
							return;
						}
					}	
					
					var fecha_vto_credito_de  = Ext.getCmp('fecha_vto_credito_de');
					var fecha_vto_credito_a  = Ext.getCmp('fecha_vto_credito_a');
					
					if(!Ext.isEmpty(fecha_vto_credito_de.getValue()) || !Ext.isEmpty(fecha_vto_credito_a.getValue())){
						if(Ext.isEmpty(fecha_vto_credito_de.getValue())){
							fecha_vto_credito_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_vto_credito_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_vto_credito_a.getValue())){
							fecha_vto_credito_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_vto_credito_a.focus();
							return;
						}
					}
					
					var fecha_vto_de  = Ext.getCmp('fecha_vto_de');
					var fecha_vto_a  = Ext.getCmp('fecha_vto_a');
					
					if(!Ext.isEmpty(fecha_vto_de.getValue()) || !Ext.isEmpty(fecha_vto_a.getValue())){
						if(Ext.isEmpty(fecha_vto_de.getValue())){
							fecha_vto_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_vto_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_vto_a.getValue())){
							fecha_vto_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_vto_a.focus();
							return;
						}
					}
					
					var fecha_publicacion_de  = Ext.getCmp('fecha_publicacion_de');
					var fecha_publicacion_a  = Ext.getCmp('fecha_publicacion_a');
					
					if(!Ext.isEmpty(fecha_publicacion_de.getValue()) || !Ext.isEmpty(fecha_publicacion_a.getValue())){
						if(Ext.isEmpty(fecha_publicacion_de.getValue())){
							fecha_publicacion_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_publicacion_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_publicacion_a.getValue())){
							fecha_publicacion_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_publicacion_a.focus();
							return;
						}
					}
					
					var fecha_emision_de  = Ext.getCmp('fecha_emision_de');
					var fecha_emision_a  = Ext.getCmp('fecha_emision_a');
					
					if(!Ext.isEmpty(fecha_emision_de.getValue()) || !Ext.isEmpty(fecha_emision_a.getValue())){
						if(Ext.isEmpty(fecha_emision_de.getValue())){
							fecha_emision_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_emision_de.focus();
							return;
						}
						if(Ext.isEmpty(fecha_emision_a.getValue())){
							fecha_emision_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fecha_emision_a.focus();
							return;
						}
					}
					
					
					
					var fn_monto_de  = Ext.getCmp('fn_monto_de');
					var fn_monto_a  = Ext.getCmp('fn_monto_a');
					
					if(!Ext.isEmpty(fn_monto_de.getValue()) || !Ext.isEmpty(fn_monto_a.getValue())){
						if(Ext.isEmpty(fn_monto_de.getValue())){
							fn_monto_de.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							fn_monto_de.focus();
							return;
						}
						if(Ext.isEmpty(fn_monto_a.getValue())){
							fn_monto_a.markInvalid('Debe capturar ambos montos o dejarlas en blanco');
							fn_monto_a.focus();
							return;
						}
					}
					if(!Ext.isEmpty(fn_monto_de.getValue()) && !Ext.isEmpty(fn_monto_a.getValue())){
						if(parseFloat(fn_monto_de.getValue())>parseFloat(fn_monto_a.getValue())){					
							fn_monto_de.markInvalid('El monto  a no puede ser mayor que el monto de');
							fn_monto_de.focus();
							return;
						}
					}				 		
					 
					hayRegistros =0;
					
					fp.el.mask('Procesando...', 'x-mask-loading');	
					consultaData_1.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar_1',
							tipoConsulta:'1',
							start:0,
							limit:15						
						})
					});		
						
					fp.el.mask('Procesando...', 'x-mask-loading');	
					consultaData_2.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar_2',
							tipoConsulta:'2',
							start:0,
							limit:15						
						})
					});	
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24consulta1ext.jsp';					
				}
			}
		]
	
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta_1,
			gridTotales_1,
			NE.util.getEspaciador(20),
			gridConsulta_2,
			gridTotales_2,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
			
		]
	});
	
	
	catEPOData.load();
	catEstatusData.load();
	catCobroInteresData.load();
	catMonedaData.load();
	catModalidadData.load();
	catIFData.load();
	
});