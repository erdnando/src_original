var showPanelLayoutNotificacion;

Ext.onReady(function() {

	//----------------------Descargar Archivos--------------------------
	function descargaArchivo() {
		var archivo = Ext.getCmp('ruta_archivo1').getValue();				
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};	
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoTxt');
		panelFormaCargaArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		panelFormaCargaArchivo.getForm().getEl().dom.submit();

	}

/************************************************************
 *		Grid que se muestra cuando se pulsa el bot�n Grabar	*
 ************************************************************/
 	var procesarConsultaGuardar = function(store, arrRegistros, opts) {
		var gridConsultaGuardar = Ext.getCmp('gridConsultaGuardar');	
		var el = gridConsultaGuardar.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaGuardar.getColumnModel();
			if(store.getTotalCount() < 1) {
				Ext.getCmp('btnIGuardarSolicitudesSinErrores').setIconClass('x-status-valid');
				Ext.getCmp('panelFormaCargaArchivo').hide();
				//el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('total_archivos1').setValue(jsonData.totalDoctos);
				Ext.getCmp('gridConsultaGuardar').setTitle('No se encontr� ning�n registro');
				Ext.getCmp("formaResumen").show();	
			} else{
				Ext.getCmp('btnIGuardarSolicitudesSinErrores').setIconClass('x-status-valid');
				Ext.getCmp('panelFormaCargaArchivo').hide();
				Ext.getCmp('total_archivos1').setValue(jsonData.totalDoctos);
				Ext.getCmp('gridConsultaGuardar').setTitle('Resumen');
				Ext.getCmp("formaResumen").show();	
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
 	//-------------------- Se crea el store para el Grid -----------------
	var consultaGuardar = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24proceso01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGuardar'
		},		
		fields: [
			{	name: 'NUMERO_CREDITO'},
			{	name: 'NUMERO_PRESTAMO'}, 
			{	name: 'FECHA_OPERACION'},
			{	name: 'ESTATUS_CREDITO'},
			{	name: 'CAUSA_RECHAZO'} 
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaGuardar,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaGuardar(null, null, null);					
				}
			}
		}	
	});
	
   //-------------------- Se crea el Grid --------------------
	var gridConsultaGuardar = new Ext.grid.EditorGridPanel({	
		store: consultaGuardar,
		id: 'gridConsultaGuardar',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',
		columns: [	
			{
				header: 'Numero de cr�dito ',
				tooltip: 'Numero de cr�dito ',
				dataIndex: 'NUMERO_CREDITO',
				sortable: true,			
				resizable: true,
				width: 159,
				align: 'center'				
			},{
				header: 'N�mero de pr�stamo ',
				tooltip: 'N�mero de pr�stamo ',
				dataIndex: 'NUMERO_PRESTAMO',
				sortable: true,		
				resizable: true,
				width: 159,
				align: 'center'				
			},{
				header: 'Fecha de operaci�n ',
				tooltip: 'Fecha de operaci�n ',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,	
				resizable: true,	
				width: 159,
				align: 'center'
			},{
				header: 'Estatus cr�dito ',
				tooltip: 'Estatus cr�dito ',
				dataIndex: 'ESTATUS_CREDITO',
				sortable: true,			
				resizable: true,
				width: 159,
				align: 'center'				
			},{
				header: 'Causa rechazo',
				tooltip: 'Causa rechazo ',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,		
				resizable: true,	
				width: 159,
				align: 'center'
			}			
		],				
		title: 'Resumen ',
		displayInfo: true,	
		emptyMsg: "No hay registros.",		
		loadMask: true,
		flex: 1,
		stripeRows: true,
		autoHeight: true,
		width: 800,
		//height: 200,
		align: 'center',
		frame: false
	});	

	
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         // Ocultar mascara del panel de resultados de la validacion
         var element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
		}
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(estado == "INICIALIZACION"){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url:'24proceso01ext.data.jsp',
				params: 	{
					informacion:'CargaArchivo.inicializacion'
				},
				callback: procesaCargaArchivo
			});
			
		} else if(estado == "MOSTRAR_AVISO"){

			if(!Ext.isEmpty(respuesta.aviso)){
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
			}
		} else if(estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"){
 
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.getForm().clearInvalid();
			panelFormaCargaArchivo.show();
		
			// Definir el texto del layout de carga
			if(!Ext.isEmpty(respuesta.reinstalacionesLayout)){
				Ext.getCmp("labelLayoutCarga").setText(respuesta.reinstalacionesLayout,false);
			}
 
		} else if(estado == "SUBIR_ARCHIVO"){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: true,
				url: '24proceso01ext.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg: 'Subiendo archivo...',
				waitTitle: 'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(estado == "REALIZAR_VALIDACION"){
 
			showWindowAvanceValidacion();
			Ext.Ajax.request({
				url: '24proceso01ext.data.jsp',
				params: {
					informacion: 'CargaArchivo.realizarValidacionEInsercion',
					fileName: respuesta.fileName
				},
				callback: procesaCargaArchivo
			});
		
		} else if(estado == "ESPERAR_DECISION_VALIDACION_INSERCION"){
		
			Ext.getCmp("formaResumen").hide();	
 			
			var totalRegistros = respuesta.totalRegistros;   
			var numeroRegistrosSinErrores	= respuesta.numeroRegistrosSinErrores;
			var numeroRegistrosConErrores	= respuesta.numeroRegistrosConErrores;
			var cargaExistosa = respuesta.cargaExistosa;

			// SETUP PARAMETROS VALIDACION
			
			// Obtengo el lsProcSirac
			Ext.getCmp('ls_proc_sirac1').setValue(respuesta.lsProcSirac);			

			// Recordar el total de registros
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
 
         // Inicializar panel de resultados
			Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
 
			// Determinar si se mostrar� el bot�n de continuar carga
			var botonAceptarCarga = Ext.getCmp('botonAceptarCarga');
			// Habilitar boton aceptar carga
			//botonAceptarCarga.enable();
 
			// MOSTRAR COMPONENTES
			
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var registrosSinErroresData = Ext.StoreMgr.key('registrosSinErroresDataStore');
         var registrosConErroresData = Ext.StoreMgr.key('registrosConErroresDataStore');
         registrosSinErroresData.removeAll();
         registrosConErroresData.removeAll();
 
         // Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").show();
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         var panelFormaCargaArchivo = Ext.getCmp('panelFormaCargaArchivo');
         var botonesResultadosValidacion = Ext.getCmp('botonesResultadosValidacion');
         var labelOperacionExitosa = Ext.getCmp('labelOperacionExitosa');
         
         if(cargaExistosa){
         	
         	panelFormaCargaArchivo.hide();
         	tabPanelResultadosValidacion.unhideTabStripItem(0);
         	tabPanelResultadosValidacion.setActiveTab(0);
				registrosSinErroresData.loadData(respuesta.registrosSinErroresDataArray);
				Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
				// Limpio la ruta del archivo a descargar
				Ext.getCmp('ruta_archivo1').setValue('');
				
				tabPanelResultadosValidacion.hideTabStripItem(1);
				botonesResultadosValidacion.show();
				
				//labelOperacionExitosa.show();
				
			} else {
				
				panelFormaCargaArchivo.show();
				tabPanelResultadosValidacion.unhideTabStripItem(1);
				tabPanelResultadosValidacion.setActiveTab(1);
				registrosConErroresData.loadData(respuesta.registrosConErroresDataArray);
				Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
				// Obtengo la ruta del archivo a descargar
				Ext.getCmp('ruta_archivo1').setValue(respuesta.urlArchivoDescargar);
				
				tabPanelResultadosValidacion.hideTabStripItem(0);
				botonesResultadosValidacion.hide();
				
         }
 
      } else if(estado == "ESPERAR_DECISION"){
      	
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
      	return;
 
		} else if(estado == "FIN"){
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma = Ext.getDom('formAux');
			forma.action = "24proceso01ext.jsp";
			forma.target = "_self";
			forma.submit();
		}
		return;
	}
 
	//---------------------------- 3. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelLayoutNotificacion = function(){
		Ext.getCmp('panelLayoutNotificacion').hide();
		Ext.getCmp('espaciadorPanelLayoutNotificacion').hide();
	}
	
	showPanelLayoutNotificacion = function(){
		Ext.getCmp('panelLayoutNotificacion').show();
		Ext.getCmp('espaciadorPanelLayoutNotificacion').show();
	}
	                             
	var elementosLayoutNotificacion = [
		{
			xtype: 'label',
			id: 'labelLayoutCarga',
			name: 'labelLayoutCarga',
			html: "" ,
			cls: 'x-form-item',
			style: {
				width: '100%',
				marginBottom: '10px', 
				textAlign: 'center',
				color: '#ff0000'
			}
		}
	];
	
	var panelLayoutNotificacion = {
		xtype: 'panel',
		id: 'panelLayoutNotificacion',
		hidden: true,
		width: 600,
		title: 'Descripci�n del Layout de Carga',
		frame: true,
		style: 'margin: 0 auto',
		bodyStyle: 'padding:10px',
		items: elementosLayoutNotificacion,
		tools: [
			{
				id: 'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutNotificacion();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelLayoutNotificacion = {
		xtype: 'box',
		id: 'espaciadorPanelLayoutNotificacion',
		hidden: true,
		height: 10,
		width: 800
	}
	
	//------------------------- 5. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarConsultaRegistrosSinErrores = function(store, registros, opts){
		var gridRegistrosSinErrores = Ext.getCmp('gridRegistrosSinErrores');
		if (registros != null) {
			var el = gridRegistrosSinErrores.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaRegistrosConErrores = function(store, registros, opts){
		
		var gridRegistrosConErrores = Ext.getCmp('gridRegistrosConErrores');
		
		if (registros != null) {
			var el = gridRegistrosConErrores.getGridEl();		
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
 
	var registrosSinErroresData = new Ext.data.ArrayStore({
		
		storeId: 'registrosSinErroresDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_SOLICITUD', mapping: 0												 },
			{ name: 'NUMERO_PRESTAMO' , mapping: 1 											 },
			{ name: 'FECHA_OPERACION' , mapping: 2 											 },
			{ name: 'ESTATUS_CREDITO' , mapping: 3, type: 'date', dateFormat: 'd/m/Y'},
			{ name: 'CAUSA_RECHAZO'   , mapping: 4												 }
		],
		idIndex: 0,
		autoLoad: false,
		listeners: {
			load: procesarConsultaRegistrosSinErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosSinErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var registrosConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_CREDITO',  mapping: 0 },
			{ name: 'DESCRIPCION', 		mapping: 1 },
			{ name: 'CREDITO_NE', 		mapping: 2 },
			{ name: 'CREDITO_NAFIN', 	mapping: 3 },
			{ name: 'FECHA_OPERACION',	mapping: 4 }
		],
		//idIndex: 0,
		autoLoad: false,
		listeners: {
			load: procesarConsultaRegistrosConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosConErrores(null, null, null);						
				}
			}
		}
		
	});
 
	var elementosResultadosValidacion = [
		{
			xtype: 'label',
			id: 'labelOperacionExitosa',
			cls: 'x-form-item',
			style: 'font-weight:bold;text-align:center;margin:2px;',
			html: "La operaci�n se complet� con �xito.",
			hidden:	true
		},
		{ 
			xtype: 'tabpanel',
			id: 'tabPanelResultadosValidacion',
			activeTab: 0,
			plain: true,
			height: 400,
			//bodyStyle:		'padding: 10px;',
			items: [
				{
					xtype: 'container',
					title: 'Resumen', //'Registros sin Errores',
					layout: 'vbox',
               layoutConfig: {
						align: 'stretch',
						pack: 'start'
					},
					items: [
						{
							store: registrosSinErroresData,
							xtype: 'grid',
							id: 'gridRegistrosSinErrores',
							stripeRows: true,
							loadMask: true,	
							frame: false,
							flex: 1,
							columns: [
								{
									header: 'Numero de solicitud',
									tooltip: 'Numero de solicitud',
									dataIndex: 'NUMERO_SOLICITUD',
									sortable: true,
									resizable: true,
									width: 130
								},
								{
									header: 'Numero de prestamo',
									tooltip: 'Numero de prestamo',
									dataIndex: 'NUMERO_PRESTAMO',
									sortable: true,
									resizable: true,
									width: 160
								},
								{
									header: 'Fecha de operaci�n',
									tooltip: 'Fecha de opercaci�n',
									dataIndex: 'FECHA_OPERACION',
									sortable: true,
									resizable: true,
									renderer: Ext.util.Format.dateRenderer('d/m/Y'),
									width: 150
								},
								{
									header: 'Estatus cr�dito',
									tooltip: 'Estatus cr�dito',
									dataIndex: 'ESTATUS_CREDITO',
									sortable: true,
									resizable: true,
									width: 150
									
								},
								{
									header: 'Causa de rechazo',
									tooltip: 'Causa de rechazo',
									dataIndex: 'CAUSA_RECHAZO',
									sortable: true,
									resizable: true,
									width: 155
								}
							]					
						},
						{
							xtype: 'container',
							layout: 'form',
							labelWidth: 200,
							style: 'padding:8px;',
							height: 35,
							//width:		400,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 'textfield',
									fieldLabel: 'Total de Folios Recibidos',
									name: 'numeroRegistrosSinErrores',
									id: 'numeroRegistrosSinErrores',
									readOnly: true,
									hidden: false,
									maxLength: 6, // Maximo 6 	
									msgTarget: 'side',
									anchor: '-220',
									style: {
										textAlign: 'right'
									}
								}								
							]
						}
					]
				},
				{
					xtype: 'container',
					title: 'Errores Generados', //'Registros con Errores',
					layout: 'vbox',
               layoutConfig: {
						align: 'stretch',
						pack: 'start'
					},
					items: [
						{
							store: registrosConErroresData,
							xtype: 'grid',
							id: 'gridRegistrosConErrores',
							stripeRows: true,
							loadMask: true,	
							//frame: false,
							flex: 1,
							columns: [
								{
									header: 'N�mero de cr�dito',
									tooltip: 'N�mero de cr�dito',
									dataIndex: 'NUMERO_CREDITO',
									sortable: true,
									resizable: true,
									width: 130
								},
								{
									header: 'Descripci�n',
									tooltip: 'Descripci�n',
									dataIndex: 'DESCRIPCION',
									sortable: true,
									resizable: true,
									width: 160
								},
								{
									header: 'Cr�dito NE',
									tooltip: 'Cr�dito NE',
									dataIndex: 'CREDITO_NE',
									sortable: true,
									resizable: true,
									width: 150
								},
								{
									header: 'Cr�dito Nafin',
									tooltip: 'Cr�dito Nafin',
									dataIndex: 'CREDITO_NAFIIN',
									sortable: true,
									resizable: true,
									width: 150
								},
								{
									header: 'Fecha de operaci�n',
									tooltip: 'Fecha de operaci�n',
									dataIndex: 'FECHA_OPERACION',
									sortable: true,
									resizable: true,
									width: 155
								}
							],
							bbar: {
								items: [
									'->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',					
									tooltip:	'Generar archivo TXT',
									iconCls: 'icoTxt', 
									id: 'btnGenerarArchivo',
									handler: function(boton, evento) {
										boton.setIconClass('loading-indicator');
										descargaArchivo();
									}
								},
								'-',
								{
									xtype: 'button',
									text: 'Guardar solicitudes sin errores',					
									tooltip:	'Guardar solicitudes sin errores ',
									iconCls: 'icoAceptar', 
									id: 'btnIGuardarSolicitudesSinErrores',
									handler: function(boton, evento) {
										boton.setIconClass('loading-indicator');
										Ext.getCmp('panelResultadosValidacion').hide();
										consultaGuardar.load({
											params: Ext.apply({
												informacion: 'consultaGuardar',
												procSirac: Ext.getCmp('ls_proc_sirac1').getValue()	
											})
										});
									}
								}				
							]
							}
						},
						{
							xtype: 'container',
							layout: 'form',
							labelWidth: 200,
							style: "padding:8px;",
							height: 70,
							//width:		400,
							hidden: true,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 'textfield',
									fieldLabel: 'Total de Registros',
									name: 'numeroRegistrosConErrores',
									id: 'numeroRegistrosConErrores',
									readOnly: true,
									hidden: false,
									maxLength: 6,	// Maximo 6
									msgTarget: 'side',
									anchor: '-220',
									style: {
									  textAlign: 'right'
								   }
								}
							]
						}
					]
				}
			]
		},
		{
			xtype: 'container',
			id: 'botonesResultadosValidacion',
			anchor: '100%',
			style: 'margin: 0px;padding-top:15px;',
			renderHidden: false,
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			items: [
				{
					xtype: 'button',
					minWidth: 128,				// Nota: Se usa minWidth debido a un raro bug en IE7 que ocasiona no se actualicen las posiciones de los botones
					text: 'Regresar',
					iconCls: 'icoAceptar',
					id: 'botonAceptarCarga',
					margins: ' 3',
					handler: function(boton, evento) {
						cargaArchivo("FIN",null);
					}
				}
			] 
		},
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype: 'hidden',  
        name: 'panelResultadosValidacion.totalRegistros',
        id: 'panelResultadosValidacion.totalRegistros'
      },
		{    
		  xtype:	'textfield',
		  hidden: true,
        name: 'ruta_archivo',
        id: 'ruta_archivo1'
      },
		{   
		  xtype:	'textfield',
		  hidden: true,
        name: 'ls_proc_sirac',
        id: 'ls_proc_sirac1'
      }
	];
	
	var panelResultadosValidacion = {
		title: 'Resultados', //'Resultado de la Validaci�n',
		hidden: true,
		xtype: 'panel',
		id: 'panelResultadosValidacion',
		width: 800, //949,
		frame: true,
		style: 'margin:  0 auto',
		bodyStyle: 'padding: 10px',
		items: elementosResultadosValidacion,
		layout: 'anchor'
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 'box',
		id: 'espaciadorPanelResultadosValidacion',
		hidden: true,
		height: 10,
		width: 800
	}
	
	//---------------------------- PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceValidacion = [
		{
			xtype: 'label',	
			cls: 'x-form-item',
			style: {
				textAlign: 'left',
				paddingBottom: '15px'
			},
			text: 'Revisando solicitudes...'
		},
		{
			xtype: 'progress',
			id: 'barrarAvanceValidacion',
			name: 'barrarAvanceValidacion',
			cls: 'center-align',
			value: 0.00,
			text: '0.00%'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype: 'panel',
		id: 'panelAvanceValidacion',
		frame: true,
		width: 500,
		height: 134,
		style: 'margin: 0 auto',
		bodyStyle: 'padding: 20px',
		items: elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion = Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval: 200,
            increment: 15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title: 'Estatus del Proceso',
				layout: 'fit',
				width: 300,
				resizable: false,
				id: 'windowAvanceValidacion',
				modal: true,
				closable: false,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
 
	//-------------------------- 1. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 'fileuploadfield',
		  id: 'archivo',
		  name: 'archivo',
		  emptyText: 'Seleccione...',
		  fieldLabel: "Ruta del Archivo de Origen", 
		  buttonText: null,
		  buttonCfg: {
			  iconCls: 'upload-icon'
		  },
		  anchor: '-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 'panel',
			anchor: '100%',
			style: {
				marginTop: '10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Continuar',
					iconCls: 'icoContinuar',
					id: 'botonContinuarCargaArchivo',				
					handler: function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						
						var nombreArchivo = archivo.getValue();
						if( !myRegexTXT.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n txt.");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 'panelFormaCargaArchivo',
		width: 800,
		autoHeight: true,
		title: 'Carga de Archivo',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		fileUpload: true,
		hidden: true,
		renderHidden:	true,
		style: 'margin: 0 auto',
		bodyStyle: 'padding:10px',
		labelWidth: 190,
		defaultType: 'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutNotificacion();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaCargaArchivo,
		monitorValid: false
	});
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 'label',
			id: 'labelAviso',
			cls: 'x-form-item',
			style: 'font-weight:normal;text-align:center;margin:15px;color:black;',
			html: ''
		}
	];
	
	var panelAvisos = {
		xtype: 'panel',
		id: 'panelAvisos',
		hidden: true,
		width: 800,
		title: 'Avisos',
		frame: true,
		style: 'margin: 0 auto',
		bodyStyle: 'padding:10px',
		items: elementosPanelAvisos
	}

/******************************************************************************
 * El siguiente c�digo es para mostrar el grid de los archivos procesados		*
 ******************************************************************************/ 
	var elementosForma = [		
		{			
			xtype:'panel',  
			id: 'panelResumen',  
			hidden: false,	
			layout:'table', 
			border: false,
			width:800,	
			layoutConfig:{ columns: 1 },
			items: [
				{			
				xtype:'panel',  id: 'pane1_0',  hidden:false,	layout:'table',	width:800,	layoutConfig:{ columns: 1 },
				defaults: {align:'left', bodyStyle:'padding:0px,'},
				items: gridConsultaGuardar
			},{			
					xtype:'panel',  id: 'pane1_1',  hidden:false, border: false, layout:'table',	width:800,	layoutConfig:{ columns: 2 },
					defaults: {align:'center', bodyStyle:'padding:2px,'},
					items: [
						{	
							width:700,	
							frame:false,	
							border:false,	
							html:'<div class="formas" align="right"><br> Total de Documentos Recibidos: <br>&nbsp;</div>'
						},{	
							xtype: 'textfield',
							width: 100,
							fieldLabel: '',
							id: 'total_archivos1',	
							name: 'total_archivos',
							readOnly: true
						}
					]
				}	
			],
			buttons: [{
            text: 'Regresar',
				iconCls: 'icoAceptar',
				id: 'btnRegresar',
				margins: ' 3',
				handler: function(boton, evento) {
					cargaArchivo("FIN",null);
				}
			}]
		}
	];	
		
	var formaResumen = new Ext.form.FormPanel({
		id: 'formaResumen',
		frame	: false,
		border: false,
		width: 800,		
		autoHeight	: true,
		hidden: true,			
		layout : 'form',
		style: 'margin:0 auto;',
      defaults: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: elementosForma
	});
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		disabled: false,
		items: [
			panelAvisos,
			panelFormaCargaArchivo,
			espaciadorPanelLayoutNotificacion,
			panelLayoutNotificacion,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion,
			//gridConsultaGuardar,
			formaResumen
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);
 
});