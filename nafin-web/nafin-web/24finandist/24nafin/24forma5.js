Ext.onReady(function() {

	/***** Varables globales *****/
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	var fm = Ext.form;
	var jsonDataEncode;
	
	//**************Inicializa Valores **************************************
	function procesaConsultaValores(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				Ext.getCmp('btnGuardar').enable();		
		
				if(jsonData.accion=='G') {
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
					function(){ 					
						Ext.Ajax.request({
							url: '24forma5.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{  
								informacion: "ConsultaValores",
								ic_if:jsonData.ic_if
							}),
							callback: procesaConsultaValores
						});	
					});
				}else  if(jsonData.accion=='C') {
				
					Ext.getCmp('cg_tipo_sol').setValue(jsonData.cg_tipo_sol);
					Ext.getCmp('fn_limite_puntos').setValue(jsonData.fn_limite_puntos); 
				
					Ext.getCmp('cg_linea_c').setValue(jsonData.cg_linea_c);
									  
					/***** INICIO Fodea 13-2014 *****/
					if( jsonData.cg_opera_tarjeta =='S' ){
						Ext.getCmp('id_cg_opera_tarjeta').setValue(true);
						Ext.getCmp('gridConsultaBins').show();
					} else {
						Ext.getCmp('id_cg_opera_tarjeta').setValue(false);
						Ext.getCmp('gridConsultaBins').hide();
					}
					/***** Consulto el grid *****/
					consultaBinsIF.load({
						params: Ext.apply({
							informacion: 'consultaBinsIF', 
							ic_if: Ext.getCmp('ic_if1').getValue()
						})
					});
					/***** FIN Fodea 13-2014 *****/
					
					// INI F000-2015
					if( jsonData.notifica_doctos =='S' ){
						Ext.getCmp('notifica_doctos_id').setValue('S');
						Ext.getCmp('email_notifica_doctos_id').setValue(jsonData.email_notifica_doctos);
						Ext.getCmp('seccion_1_2').show();
					} else{
						Ext.getCmp('notifica_doctos_id').setValue('N');
						Ext.getCmp('email_notifica_doctos_id').setValue('');
						Ext.getCmp('seccion_1_2').hide();
					}
					//FIN F000-2015

				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}		
		
	/**********     Valida el campo Correo(s) de Notificaci�n .     **********/
	function validaEmail(){
		var errores = 0;
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var correo = Ext.getCmp('email_notifica_doctos_id').getValue();
		// Verifico el tama�o del campo
		if(correo.length == 0){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('El campo es obligatorio');
			return false;
		}
		if(correo.length > 400){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('El tama�o m�ximo es de 400');
			return false;
		}
		// Verifico el formato de los correos
		var vecCorreos = correo.split(',');
		for(var i = 0; i < vecCorreos.length; i++){
			if (!expr.test(vecCorreos[i])){
				errores++;
			}
		}
		if(errores > 0){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
			return false;
		}
		return true;
	}

	/**********		Obtiene los datos del grid y los convierte en array  		**********/
	function getJsonOfStore(store){
		var exito = true;
		var datar = new Array();
		jsonDataEncode = '';
		var records = store.getRange();
		for (var i = 0; i < records.length; i++) {
			if(records[i].data.DESCRIPCION == ''){
				Ext.MessageBox.alert('Mensaje','El campo Descripci�n  es obligatorio.',
					function(){
						gridConsultaBins.startEditing(i, 1);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.CODIGO_BIN == ''){
				Ext.MessageBox.alert('Mensaje','El campo C�digo del Producto  es obligatorio.',
					function(){
						gridConsultaBins.startEditing(i, 2);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.ESTATUS == ''){
				Ext.MessageBox.alert('Mensaje','El campo Estatus  es obligatorio.',
					function(){
						gridConsultaBins.startEditing(i, 3);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.ESTATUS == 'INACTIVO' && records[i].data.DESC_BAJA == ''){
				Ext.MessageBox.alert('Mensaje','Para Inactivar el Producto es obligatorio capturar las causas de rechazo.',
					function(){
						gridConsultaBins.startEditing(i, 4);
						return;
					}
				);
				exito = false;
				break;
			} else if(records[i].data.ESTATUS == 'ACTIVO' && records[i].data.DESC_BAJA != ''){
				Ext.MessageBox.alert('Mensaje','Si el estatus es ACTIVO el campo Causas de la Baja debe estar vac�o.',
					function(){
						gridConsultaBins.startEditing(i, 4);
						return;
					}
				);
				exito = false;
				break;
			} else{
				datar.push(records[i].data);
			}
		}
		if(exito == true){
			jsonDataEncode = Ext.util.JSON.encode(datar);
			Ext.Ajax.request({
				url:					'24forma5.data.jsp',
				params: 				Ext.apply({
					informacion:	'Valida_Grid',
					datos: 			jsonDataEncode
				}),			
				callback: procesaRespuestaValidacion
			});		
		}

	}
	
	function getJsonOfStore1(store){
		var datar = new Array();
		jsonDataEncode = '';
		var records = store.getRange();
		for (var i = 0; i < records.length; i++) {
			datar.push(records[i].data);
		}
		jsonDataEncode = Ext.util.JSON.encode(datar);		
	}	
	
	/**********		Despues de validar el grid, determina el paso siguiente  		**********/ 
	var procesaRespuestaValidacion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			procesarGuardar();
		} else {
			Ext.Msg.alert('Mensaje', Ext.util.JSON.decode(response.responseText).msg );
			return;
		}
	}	
	
	/*****  Funci�n para guardar los datos *****/
	function procesarGuardar(){	
		
		if(Ext.getCmp('cg_tipo_sol').getValue()==null) {
			Ext.getCmp('cg_tipo_sol').markInvalid('El campo es obligatorio');
			return;	
		}
		
		if(Ext.isEmpty(Ext.getCmp('fn_limite_puntos').getValue() )){ 
			Ext.getCmp('fn_limite_puntos').markInvalid('El campo es obligatorio');
			return;
		}	
		
		Ext.getCmp('btnGuardar').disable();		
		fp.el.mask('Procesando...', 'x-mask-loading');
		
		if( Ext.getCmp('id_cg_opera_tarjeta').getValue() == true ){
			
			Ext.Ajax.request({
				url:					'24forma5.data.jsp',
				params:				Ext.apply(fp.getForm().getValues(),{  
				informacion:		'Guardar_Datos',
				cg_opera_tarjeta:	Ext.getCmp('id_cg_opera_tarjeta').getValue(),
				datos: 				jsonDataEncode
				}),
				callback: procesaConsultaValores
			});
		
		} else{
		
			Ext.Ajax.request({
				url: 				'24forma5.data.jsp',
				params:			Ext.apply(fp.getForm().getValues(),{  
				informacion:	'Guardar_Datos'
				}),
				callback: procesaConsultaValores
			});	
		
		}		
	}

	// *******************Criterios de Captura ***********************
	 var procesarCatalogoIF = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){	
			var ic_if = Ext.getCmp('ic_if1');
			Ext.getCmp('ic_if1').setValue('');
			if(ic_if.getValue() == ''){
				var newRecord = new recordType({
					clave: '',
					descripcion: 'Seleccionar ....'
				});							
				store.insert(0,newRecord);
				store.commitChanges();
				ic_if.setValue('');			
			}						
		}
  };
	
	var catalogoIF = new Ext.data.JsonStore({
		id:		'catalogoIF',
		root:		'registros',
		fields:	['clave', 'descripcion', 'loadMsg'],
		url:		'24forma5.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty :	'total',
		autoLoad: 			false,		
		listeners: {	
		load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});

	/***** C�digo para generar el grid Bins del IF *****/
	var procesarConsultaBinsIF = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();				
		var gridConsultaBins = Ext.getCmp('gridConsultaBins');	
		var el = gridConsultaBins.getGridEl();	
		var jsonData = store.reader.jsonData;
		el.unmask();
	}
	
	/***** Se crea el store del grid *****/
	var consultaBinsIF = new Ext.data.JsonStore({
		root:			'registros',
		url:			'24forma5.data.jsp',
		baseParams: {
			informacion: 'consultaBinsIF'		
		},
		fields: [
			{name: 'BIN',         type: 'string'},
			{name: 'IC_IF',       type: 'string'},
			{name: 'MONEDA',      type: 'string'},
			{name: 'DESCRIPCION', type: 'string'},
			{name: 'CODIGO_BIN',  type: 'string'},
			{name: 'ESTATUS',     type: 'string'},
			{name: 'DESC_BAJA',   type: 'string'}

		],		
		totalProperty:		'total',
		messageProperty:	'msg',
		autoLoad:			false,
		listeners: {
			load: procesarConsultaBinsIF,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaBinsIF(null, null, null);					
				}
			}
		}					
	});

	/***** Se crea el renderer del campo Moneda *****/
	function renderValorMoneda(value, metaData, record, rowIndex, colIndex, store){
		if (record.data.MONEDA == '54')
			return 'DOLAR AMERICANO';
		else
			return 'MONEDA NACIONAL';
	}

	/***** Se crea el grid Captura *****/
	var gridConsultaBins = new Ext.grid.EditorGridPanel({
		width:           600,
		height:          200,
		clicksToEdit:    1,
		flex:            1,
		store:           consultaBinsIF,
		id:              'gridConsultaBins',
		title:           'Productos del IF',
		margins:         '20 20 20 20',
		style:           'margin:0 auto;',
		align:           'center',
		hidden:          true,
		displayInfo:     true,
		loadMask:        true,
		stripeRows:      true,
		frame:           true,
		border:          true,
		tbar: [{
			xtype:       'button',
			text:        'Agregar',
			iconCls:     'icoAgregar',
			handler: function(){
				var BinsIF = gridConsultaBins.getStore().recordType;
				var nuevoReg = new BinsIF({
					BIN:         '',
					IC_IF:       '',
					MONEDA:      '1',
					DESCRIPCION: '',
					CODIGO_BIN:  '',
					ESTATUS:     'ACTIVO',
					DESC_BAJA:   ''
				});
				gridConsultaBins.stopEditing();
				consultaBinsIF.insert(0, nuevoReg);
				gridConsultaBins.startEditing(0,0);
			}
		}],
		columns: [
			{
				width:     150,
				header:    'Moneda',
				dataIndex: 'MONEDA',
				align:     'center',
				resizable: false,
				renderer:  renderValorMoneda
			},{
				width:     150,
				header:    'Descripci�n',
				dataIndex: 'DESCRIPCION',
				align:     'left',
				resizable: true,
				editor:    new fm.TextField({
					maxLength:  30,
					allowBlank: false
				})
			},{
				width:     150,
				header:    'C�digo del Producto',
				dataIndex: 'CODIGO_BIN',
				align:     'center',
				resizable: true,
				editor:    new fm.NumberField({
					maxLength:  6,
					allowBlank: false
				})
			},{
				width:     133,
				header:    'Estatus',
				dataIndex: 'ESTATUS',
				align:     'center',
				resizable: false,
				editor:    new fm.ComboBox({
					queryMode:      'local',
					triggerAction:  'all',
					forceSelection: true,
					allowBlank:     false,
					store: [
						['ACTIVO',   'ACTIVO' ],
						['INACTIVO','INACTIVO']
					]
				})
			},{
				width:     150,
				header:    'Causas de la Baja',
				dataIndex: 'DESC_BAJA',
				align:     'left',
				resizable: true,
				editor:    new fm.TextArea({
					maxLength:  250,
					allowBlank: true
				})
			}
		]
	});

	var elementosForma = [
		{
			width:			400,	
			minChars:		1,	
			xtype:			'combo',	
			name:				'ic_if',	
			id:				'ic_if1',	
			fieldLabel:		'&nbsp;&nbsp;IF', 
			mode:				'local',	
			hiddenName:		'ic_if',	
			emptyText:		'Seleccione ... ',
			triggerAction: 'all',	
			displayField:	'descripcion',	
			valueField:		'clave',	
			forceSelection:true,	
			typeAhead: 		true,		
			store:			catalogoIF,
			listeners: {
				select: {
					fn: function(combo) {
					
						if(combo.getValue()!=''){
						
							Ext.getCmp("titulos_1").show();
							Ext.getCmp("titulos_2").show();
							Ext.getCmp("seccion_1").show();
							Ext.getCmp("seccion_1_1").show();
							Ext.getCmp("titulos_3").show();
							Ext.getCmp("seccion_2").show();
							Ext.getCmp("titulos_4").show();
							Ext.getCmp("seccion_3").show();
							
							Ext.getCmp("titulos_5").show(); //F020-2015
							Ext.getCmp("seccion_4").show();//F020-2015
							
							Ext.getCmp("gridConsultaBins").hide();
							Ext.getCmp("btnGuardar").show();
							
							fp.el.mask('Procesando...', 'x-mask-loading');		
							Ext.Ajax.request({
								url: '24forma5.data.jsp',
								params: {
									informacion: 'ConsultaValores',
									ic_if:combo.getValue()
								},
								callback: procesaConsultaValores
							});	
							
						}else  {
						
							Ext.getCmp("titulos_1").hide();
							Ext.getCmp("titulos_2").hide();
							Ext.getCmp("seccion_1").hide();
							Ext.getCmp("seccion_1_1").hide();
							Ext.getCmp("seccion_1_2").hide();
							Ext.getCmp("titulos_3").hide();
							Ext.getCmp("seccion_2").hide();
							Ext.getCmp("titulos_4").hide();
							Ext.getCmp("seccion_3").hide();
							Ext.getCmp("gridConsultaBins").hide();	
							Ext.getCmp("btnGuardar").hide();
							
							Ext.getCmp("titulos_5").hide(); //F020-2015
							Ext.getCmp("seccion_4").hide();//F020-2015
						}
						
					}
				}
			}	
		},			
		{			
			xtype:'panel',	 id: 'titulos_1', hidden:true, layout:'table',	width:600,	layoutConfig:{   columns: 3 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'3',	border:true,	html:'<div align="center"><b>Secci�n 1: Esquemas de operaci�n</b	><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',  id: 'titulos_2',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Nombre del par�metro <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Tipo de dato <br><br>&nbsp;</div>'	},
				{	width:300,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Valores<br><br>&nbsp;</div>'	}							
			]
		},
		{			
			xtype:'panel',  id: 'seccion_1',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de liquidaci�n de cr�dito (Dep�sito o cargo) <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'cg_tipo_sol',	name:'cg_tipo_sol',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Deposito del Acreditado a IF     ',	name : 'cg_tipo_sol', inputValue: 'I' },
							{	boxLabel	: 'Cargo de IF a la Cuenta del Acreditado ',	name : 'cg_tipo_sol',  inputValue: 'A' }							
						]															
				}				
			]
		},		
		//INI F000-2015
		{			
			xtype:'panel',  id: 'seccion_1_1',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Activar Notificaci�n doctos.<br>"Seleccionada PyME" por e-mail <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'notifica_doctos_id',	name:'notifica_doctos',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[  
							{ boxLabel: 'SI', name:'notifica_doctos', inputValue: 'S' },
							{ boxLabel: 'NO', name:'notifica_doctos', inputValue: 'N' }
						],
						listeners:     {
							change:     {
								fn:      function(){
									var notifica_doctos = (Ext.getCmp('notifica_doctos_id')).getValue();
									if(notifica_doctos.getGroupValue() == 'S'){
										Ext.getCmp('seccion_1_2').show();
									} else if(notifica_doctos.getGroupValue() == 'N'){
										Ext.getCmp('email_notifica_doctos_id').setValue('');
										Ext.getCmp('seccion_1_2').hide();
									}
								}
							}
						}
				}
			]
		},
		{
			xtype:'panel',  id: 'seccion_1_2',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Correo(s) de Notificaci�n <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	
					width:      230,
					xtype:      'textarea',
					id:         'email_notifica_doctos_id',
					name:       'email_notifica_doctos',
					anchor:     '-20',
					msgTarget:  'side',
					listeners:     {
						blur:       {
							fn: function(objTxt) {
								if(!validaEmail()){
									return;
								}
							}
						}
					}
				}
			]
		},
		//FIN F000-2015
		{			
			xtype:'panel',	 id: 'titulos_3',  hidden:true, layout:'table',	width:600,	layoutConfig:{   columns: 2 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'2',	border:true,	html:'<div align="center"><b>Secci�n 2: L�mites de puntos por Tasa</b	><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',  id: 'seccion_2', 	 hidden:true, layout:'table',	width:600,	layoutConfig:{ columns: 2 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> L�mite <br><br>&nbsp;</div>'	},				
				
				{	
					width:			150,				
					xtype:			'bigdecimal',
					fieldLabel:		'',	
					id:				'fn_limite_puntos',	
					name:				'fn_limite_puntos'	,		
					maxText:			'El tama�o m�ximo del campo es de 3 enteros 2 decimales',				
					maxValue: 		'999.99', 
					msgTarget: 		'side',
					anchor:			'-20',						
					format:			'000.00',
					allowDecimals: true,
					allowNegative: false,							
					hidden: 			false,	
					allowBlank:		true				
				}				
			]
		},{	/***** INICIO Fodea 13-2014 *****/		
			xtype:'panel',	 id: 'titulos_4',  hidden:true, layout:'table',	width:600,	layoutConfig:{   columns: 2 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'2',	border:true,	html:'<div align="center"><b>Secci�n 3: Tarjetas de Cr�dito</b><br>&nbsp;</div>'	}
			]
		},{			
			xtype:'panel',  id: 'seccion_3', 	 hidden:true, layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tarjeta de Cr�dito <br><br>&nbsp;</div>'	},				
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{
					width:			250,
					labelWidth:		1,
					border:			false,
					defaultType:	'checkbox',
					align:			'left',
					id:				'id_opera_tarjeta_cred', 
					name:				'opera_tarjeta_cred',
					style:{ width: '80%', marginLeft: '10px' },
					items: [{
						width:		300,
						fieldLabel:	'',
						boxLabel:	'Opera Tarjeta de Cr�dito',
						id:			'id_cg_opera_tarjeta',
						name:			'cg_opera_tarjeta',
						align:		'left',
						value:		'1',
						listeners: {
							check: function(){
								if( Ext.getCmp('id_cg_opera_tarjeta').getValue() == true ){
									 Ext.getCmp('gridConsultaBins').show();
								} else{
									Ext.getCmp('gridConsultaBins').hide();
								}
							}	
						}
					}]
				}				
			]
		}	/***** FIN Fodea 13-2014 *****/
	];
	
	var elementosForma_2 = [
		/***** INICIO Fodea 20-2015 *****/		
		{	xtype:'panel',	 id: 'titulos_5',  hidden:true, layout:'table',	width:600,	layoutConfig:{   columns: 2 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'2',	border:true,	html:'<div align="center"><b>Secci�n 4: L�nea de Cr�dito </b><br>&nbsp;</div>'	}
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_4',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Validar el periodo de financiamiento de los documentos con respecto a la vigencia de la L�nea de Cr�dito <br><br>&nbsp;</div>'	},	 			
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'cg_linea_c',	name:'cg_linea_c',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items:  
						[  
							{	boxLabel	: 'SI',	name : 'cg_linea_c', inputValue: 'S' },
							{	boxLabel	: 'NO ',	name : 'cg_linea_c',  inputValue: 'N' }							
						]															
				}				
			]
		}		
	];
	
	var fp = new Ext.form.FormPanel({
		width:			600,	
		id:				'forma',
		title:			'Par�metros por IF',				
		layout:			'form',
		style:			'margin:0 auto;',
		frame:			false,	
		autoHeight:		true,
		monitorValid:	true,	
      defaults: 		{ xtype: 'textfield', msgTarget: 	'side'},	
		items: 			[elementosForma,	gridConsultaBins, elementosForma_2 ],	
		buttons: 				
		[{
			text:			'Guardar',
			id:			'btnGuardar',
			iconCls:		'icoAceptar',
			formBind:	true,
			hidden:		true,
			handler:		function(boton, evento) {
				//INI F000-2015
				var notifica_doctos = (Ext.getCmp('notifica_doctos_id')).getValue();
				if(notifica_doctos.getGroupValue() == 'S'){
					if(!validaEmail()){
						return;
					}
				}
				//FIN F000-2015
				if( Ext.getCmp('id_cg_opera_tarjeta').getValue() == true ){
					getJsonOfStore(consultaBinsIF);
				} else{
					procesarGuardar();
				}
			}
		}]
	});

	var pnl = new Ext.Container({
		width:	949,	
		id:		'contenedorPrincipal',
		applyTo:	'areaContenido',	
		style:	'margin:0 auto;',
		items:	[
			NE.util.getEspaciador(20),
			fp			
		]
	});

	jsonDataEncode = '';
	catalogoIF.load();
});
