Ext.onReady(function() {

//------------------------------------------------------------------------------
	var procesarConsultaTasas = function(store, arrRegistros, opts) {

		pnl.el.unmask();
		var el = gridParamsEpo.getGridEl();
			
		if (arrRegistros != null) {
			cboSeccion = Ext.getCmp('cboSeccion_');
			var record = cboSeccion.findRecord(cboSeccion.valueField, cboSeccion.getValue());		
			var valSeccion = record.get(cboSeccion.displayField);
			
			gridParamsEpo.setTitle('<p align="left">'+valSeccion+'<p/>');
			gridParamsEpo.show();	
			if (!gridParamsEpo.isVisible()) {
				gridParamsEpo.show();				
			}
			
			if(store.getTotalCount() > 0) {	
				el.unmask();			
			
			}else{
				el.mask('No se encontr� informaci�n', 'x-mask');		
			}
		}
	}

//------------------------------------------------------------------------------
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'storeCatEpoData_',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta8ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
			/*
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial=='S'){
							iniRequest(store);
						}
					}else if(store.getTotalCount()<1 ){
						cveEpoOriginal = Ext.getDom("hidCboEpo").value;
						validaContratoEpoPyme(cveEpoOriginal);
					}
			}
			*/
		}
	});
	
	var storeParamsData = new Ext.data.JsonStore({
		root : 'registros',
		url : '24consulta8ext.data.jsp',
		baseParams: {
			informacion: 'consultaParametros'
		},
		fields: [
			{name:'NOMBREPARAM'},
			{name:'TIPODATO'},
			{name:'VALORES'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTasas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaTasas(null, null, null);						
				}
			}
		}
	});

//------------------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'combo',
				name: 'cboEpo',
				id: 'cboEpo_',
				fieldLabel: 'EPO',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cboEpo',
				emptyText: 'Seleccionar',
				width: 580,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : storeCatEpoData,
				listeners:{
					select : function(cboIf, record, index ) {
						var cboSeccion = Ext.getCmp('cboSeccion_');
						if(cboSeccion.getValue()!=""){
							pnl.el.unmask();
							pnl.el.mask('Consultando...', 'x-mask-loading');
							
							storeParamsData.load({
								params: Ext.apply(fp.getForm().getValues())
							});
						}
					}
				},
				tpl : NE.util.getTemplateMensajeCargaCombo('{descripcion}')
			},
			{
				xtype: 'combo',
				name: 'cboSeccion',
				id: 'cboSeccion_',
				fieldLabel: 'Secci�n',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cboSeccion',
				emptyText: 'Seleccionar',
				width: 580,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : new Ext.data.ArrayStore({
					fields: [
						{name: 'clave'},
						{name: 'descripcion'}
					],
					data:[['1','Secci�n 1: Esquema de operaci�n'], ['2','Secci�n 2: Par�metros']]
				}),
				value: '1',
				listeners:{
					select : function(cboIf, record, index ) {
						var cboEpo = Ext.getCmp('cboEpo_');
						if(cboEpo.getValue()!=""){
							pnl.el.unmask();
							pnl.el.mask('Consultando...', 'x-mask-loading');
							
							storeParamsData.load({
								params: Ext.apply(fp.getForm().getValues())
							});
						}
					}
				}
				//tpl : NE.util.getTemplateMensajeCargaCombo('{descripcion}')
			}
		],
		monitorValid: true
	});

	var gridParamsEpo = new Ext.grid.GridPanel({
		id: 'gridParamsEpo1',
		store: storeParamsData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title:' ',	
		hidden: true,
		columns: [		
			{
				header : '<center>Nombre del param�tro</center>',
				tooltip: 'Nombre del param�tro',
				dataIndex : 'NOMBREPARAM',
				sortable : true,
				width : 300,
				align: 'left'
			},
			{
				header : 'Tipo de dato',
				tooltip: 'Tipo de dato',
				dataIndex : 'TIPODATO',
				sortable : true,
				width : 100,
				align: 'center'
			},
			{
				header : '<center>Valores</center>',
				tooltip: 'Valores',
				dataIndex : 'VALORES',
				sortable : true,
				width : 200,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		autoHeight: true,
		width: 600,
		//bodyStyle:'text-align:left',
		frame: true
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridParamsEpo
		]
	});
	
});