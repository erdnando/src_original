Ext.onReady(function() {
//------------------------------------------------------------------------------
//-----------------------------HANDLER's-----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function hideGrids(){ //funcin para ocultar todos los grids
		gridConsulta.hide();
		gridConsultaDocto1259.hide();
		gridConsultaDoctoD3.hide();
		gridConsultaDoctoD11.hide();
		gridConsultaDoctoD20.hide();
		gridConsultaDoctoD22.hide();
    gridConsultaDoctoD32.hide();
	}
	
	function totalRegistros(store,grid){ //funcion para habilitar e inhabilitar los
	//botones delos grids y lanzar la consulta de totales
		var cbo = Ext.getCmp('cmbCatalogoEstatus');
		if(store>0){
			Ext.getCmp('btnArchivoPDF0').enable();
			Ext.getCmp('btnArchivoCSV0').enable();
			Ext.getCmp('btnArchivoPDF00').enable();
			Ext.getCmp('btnArchivoCSV00').enable();
			Ext.getCmp('btnArchivoPDF000').enable();
			Ext.getCmp('btnArchivoCSV000').enable();
			Ext.getCmp('btnArchivoPDF0000').enable();
			Ext.getCmp('btnArchivoCSV0000').enable();
			Ext.getCmp('btnArchivoPDF00000').enable();
			Ext.getCmp('btnArchivoCSV00000').enable();
			Ext.getCmp('btnArchivoPDF000000').enable();
			Ext.getCmp('btnArchivoCSV000000').enable();
		
			consultaTotalesData.load({
				params:	Ext.apply(fp.getForm().getValues(),
				{
					informacion : 'ResumenTotales'
				}
			)});
			grid.unmask();					
		}else{
			Ext.getCmp('btnArchivoPDF0').disable();
			Ext.getCmp('btnArchivoCSV0').disable();
			Ext.getCmp('btnArchivoPDF00').disable();
			Ext.getCmp('btnArchivoCSV00').disable();
			Ext.getCmp('btnArchivoPDF000').disable();
			Ext.getCmp('btnArchivoCSV000').disable();
			Ext.getCmp('btnArchivoPDF0000').disable();
			Ext.getCmp('btnArchivoCSV0000').disable();
			Ext.getCmp('btnArchivoPDF00000').disable();
			Ext.getCmp('btnArchivoCSV00000').disable();
			Ext.getCmp('btnArchivoPDF000000').disable();
			Ext.getCmp('btnArchivoCSV000000').disable();
			grid.mask('No se encontr� ning�n registro', 'x-mask');				
		}
	}

	var procesarVerDetalles = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var claveDoc = registro.get('IC_DOCUMENTO'); //parametro para la consulta detalles
		consultaDataDetalles.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultaDetalles',
				claveDocumento: claveDoc		
			})
		});
		var aux = Ext.getCmp('winDetalles').show().setTitle('Detalles del documento : '+claveDoc);
	}
	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		if (arrRegistros != null ) {
			if (!gridTotales.isVisible() ) {
       if (Ext.getCmp('gridTotalesMio').isVisible()){
          Ext.getCmp('gridTotales').hide();
       } else {
				gridTotales.show();
        }
			}
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
    if (Ext.getCmp('gridTotalesMio').isVisible()){
        Ext.getCmp('gridTotales').hide();
    } 
	}

	var procesarConsultaDataEstatusSolic = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS VALORES C1,C2,C3, C4 Y D3
	*/
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsulta');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
		}
	}

	var procesarConsultaDataEstatusDocto1259	 = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS VALORES D1,D2,D5 Y D9
	*/
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDocto1259');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
		}
	}
	
	var procesarConsultaDataEstatusDoctoD3 = function(store, arrRegistros, opts) 	{
	//*	PARA LOS VALORES D3
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDoctoD3');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
		}
	}

	var procesarConsultaDataEstatusDoctoD11  = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS VALORES D11
	*/
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDoctoD11');	
		var el = gridConsultaShow.getGridEl();	
				hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
		}
	}
	
	var porcesarConsultaDataEstatusDoctoD20 = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS VALORES D20
	*/
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDoctoD20');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
		}
	}
	
	var procesarConsultaDataEstatusDoctoD22 = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS VALORES D22
	*/
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDoctoD22');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
		if (arrRegistros != null) {
			if (!gridConsultaShow.isVisible()) {
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
			var jsonData = store.reader.jsonData;	
			totalRegistros(store.getTotalCount(),el);	
			}
	}


//-------------------
var procesarConsultaDataEstatusDoctoD32 = function(store, arrRegistros, opts) 	{
	//D32
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaShow = Ext.getCmp('gridConsultaDoctoD32');	
		var el = gridConsultaShow.getGridEl();	
			hideGrids();
      
      if (Ext.getCmp('gridTotales').isVisible()){
        Ext.getCmp('gridTotales').hide();
      }
      
		if (arrRegistros != null) {
    Ext.getCmp('gridTotales').hide();
    Ext.getCmp('gridTotalesMio').show();
    gridTotales.hide();
			if (!gridConsultaShow.isVisible()) {
      gridTotales.hide();
				gridConsultaShow.show().setTitle('Estatus : '+Ext.getCmp('cmbcatalogoEstatus').getRawValue());
			}	
		//	var jsonData = store.reader.jsonData;	
      
      if(store.getTotalCount() > 0) {					
				el.unmask();
        totalRegistros(store.getTotalCount(),el);	
			//	Ext.getCmp('btnGenerarCSV').enable();				
			} else {		
				Ext.getCmp('btnArchivoCSV0000').disable();	
        Ext.getCmp('btnArchivoPDF0000').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');	
        Ext.getCmp('gridTotales').hide();
        Ext.getCmp('gridTotalesMio').hide();
        gridTotales.hide();
			}
      
			//totalRegistros(store.getTotalCount(),el);	ok
			}
      
  gridTotales.hide();    
	}
//------------------

	
	var procesarConsultaDataDetalles = function(store, arrRegistros, opts) 	{
	/*
	*	PARA LOS DETALLES DE CABIOS POR NUMERO DE DOCUMENTO
	*/
	var gridConsultaDetalles = Ext.getCmp('gridDetalles');	
		if (arrRegistros != null) {
			var el = gridConsultaDetalles.getGridEl();
			el.unmask();
			if (!gridConsultaDetalles.isVisible()) {
				gridConsultaDetalles.show();
			}
			if(store.getTotalCount()>0){
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------

	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24reporte01ext.data.jsp',
			baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var consultaTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'		
		},
		fields: [
			{name: 'MONEDA'},			
			{name: 'TOTAL_DOCTOS'},
			{name: 'FN_MONTO_TOTAL'},
			{name: 'MONTO_CREDITO_TOTAL'},
			{name: 'MONTO_VALUADO_TOTAL'},
			{name: 'IC_MONEDA'},
      {name: 'MONTO_VALUADO_TOTAL_MIO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});

var consultaDataEstatusSolic   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
			{name : 'NOMBRE_EPO'},//1
			{name : 'NOMBRE_DIST'},//2
			{name : 'CC_ACUSE'},//3
			{name : 'IG_NUMERO_DOCTO'},//4
			{name : 'DF_FECHA_EMISION'},//5
			{name : 'DF_FECHA_VENC'},//6
			{name : 'DF_CARGA'},//7
			{name : 'IG_PLAZO_DOCTO'},//8
			{name : 'MONEDA'},//9
			{name : 'FN_MONTO', type: 'float'},//10
			{name : 'IG_PLAZO_DESCUENTO'},//11
			{name : 'FN_PORC_DESCUENTO', type :'float'},//12
			{name : 'motoporcdesc', type:' float'},//13
			{name : 'FN_tipoConversion'},//14 data a mostrar
			{name : 'TIPO_CONVERSION'},//14
			{name : 'FN_tipoCambio'},//14 data a mostrar
			{name : 'TIPO_CAMBIO', type:'float'},//15
			{name : 'FN_montoValuado'},//16 
			{name : 'MODO_PLAZO'},//17
			{name : 'IC_DOCUMENTO'},//18
			{name : 'MONTO_CREDITO'},//19
			{name : 'IG_PLAZO_CREDITO'},//20
			{name : 'FECHA_VENC_CREDITO'},//21
			{name : 'FECHA_SELECCION'},//22
			{name : 'NOMBRE_IF'},//23
			{name : 'REFERENCIA_INT'},//24
			{name : 'VALOR_TASA_INT'},//25
			{name : 'MONTO_TASA_INT'},//26
			{name : 'MONTO_TOTAL_CAPITAL_INTER'},//27
			{name : 'MONEDA_LINEA'},//extra para calcular tipo convesion
			{name : 'IC_MONEDA'},//estra para calcular tipo conversion
			{name : 'CG_VENTACARTERA'}//if (bandeVentaCartera.equals("S") ) {	modoPlazo ="";   }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusSolic,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusSolic(null, null, null);					
				}
			}
		}		
	});
	
	var consultarDataEstatusDocto1259 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
			{name : 'NOMBRE_EPO'},//1
			{name : 'NOMBRE_DIST'},//2
			{name : 'CC_ACUSE'},//3
			{name : 'IG_NUMERO_DOCTO'},//4
			{name : 'DF_FECHA_EMISION'},//5
			{name : 'DF_FECHA_VENC'},//6
			{name : 'DF_CARGA'},//7
			{name : 'IG_PLAZO_DOCTO'},//8
			{name : 'MONEDA'},//9
			{name : 'FN_MONTO', type: 'float'},//10
			{name : 'IG_PLAZO_DESCUENTO'},//11
			{name : 'FN_PORC_DESCUENTO', type :'float'},//12
			{name : 'FN_MONTO_DESCUENTO'},//13
			{name : 'FN_tipoConversion'},//14 data a mostrar
			{name : 'TIPO_CONVERSION'},//14
			{name : 'FN_tipoCambio'},//15 data a mostrar
			{name : 'TIPO_CAMBIO'},//15
			{name : 'FN_montoValuado'},//17 data a mostrar
			{name : 'totalMontoValuado'},//17
			{name : 'MODO_PLAZO'},//17
			
			{name : 'IC_DOCUMENTO'},
			{name : 'IC_MONEDA'},//estra para calcular tipo conversion
			{name : 'CG_VENTACARTERA'}//if (bandeVentaCartera.equals("S") ) {	modoPlazo ="";   }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusDocto1259,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusDocto1259(null, null, null);					
				}
			}
		}		
	});
	

	var consultaDataEstatusDoctoD3 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
		{name : 'NOMBRE_EPO'},//1
		{name : 'NOMBRE_DIST'},//2
		{name : 'CC_ACUSE'},//3
		{name : 'IG_NUMERO_DOCTO'},//4
		{name : 'DF_FECHA_EMISION'},//5
		{name : 'DF_FECHA_VENC'},//6
		{name : 'DF_CARGA'},//7
		{name : 'IG_PLAZO_DOCTO'},//8
		{name : 'MONEDA'},//9
		{name : 'FN_MONTO'},//10
		{name : 'IG_PLAZO_DESCUENTO'},//11
		{name : 'FN_PORC_DESCUENTO'}//12
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusDoctoD3,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusDoctoD3(null, null, null);					
				}
			}
		}		
	});
	
	var consultarDataEstatusDoctoD11 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
		{name : 'NOMBRE_EPO'},//1
			{name : 'NOMBRE_DIST'},//2
			{name : 'CC_ACUSE'},//3
			{name : 'IG_NUMERO_DOCTO'},//4
			{name : 'DF_FECHA_EMISION'},//5
			{name : 'DF_FECHA_VENC'},//6
			{name : 'DF_CARGA'},//7
			{name : 'IG_PLAZO_DOCTO'},//8
			{name : 'MONEDA'},//9
			{name : 'FN_MONTO', type: 'float'},//10
			{name : 'IG_PLAZO_DESCUENTO'},//11
			{name : 'FN_PORC_DESCUENTO', type :'float'},//12
			{name : 'motoporcdesc', type:' float'},//13
			{name : 'FN_tipoConversion'},//14 data a mostrar
			{name : 'TIPO_CONVERSION'},//14
			{name : 'FN_tipoCambio'},//14 data a mostrar
			{name : 'TIPO_CAMBIO', type:'float'},//15
			{name : 'FN_montoValuado'},//16 
			{name : 'MODO_PLAZO'},//17
			{name : 'IC_DOCUMENTO'},//18
			{name : 'MONTO_CREDITO'},//19
			{name : 'IG_PLAZO_CREDITO'},//20
			{name : 'FECHA_VENC_CREDITO'},//21
			{name : 'FECHA_SELECCION'},//22
			{name : 'NOMBRE_IF'},//23
			{name : 'REFERENCIA_INT'},//24
			{name : 'VALOR_TASA_INT'},//25
			{name : 'MONTO_TASA_INT'},//26
			{name : 'MONTO_TOTAL_CAPITAL_INTER'},//27
			{name : 'CG_NUMERO_PAGO'},
			
			
			{name : 'MONEDA_LINEA'},//extra para calcular tipo convesion
			{name : 'IC_MONEDA'},//estra para calcular tipo conversion
			{name : 'CG_VENTACARTERA'}//if (bandeVentaCartera.equals("S") ) {	modoPlazo ="";   }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusDoctoD11,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusDoctoD11(null, null, null);					
				}
			}
		}		
	});
	
	var consultaDataEstatusDoctoD20 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
		{name : 'NOMBRE_EPO'},//1
		{name : 'NOMBRE_DIST'},//2
		{name : 'CC_ACUSE'},//3
		{name : 'IG_NUMERO_DOCTO'},//4
		{name : 'DF_FECHA_EMISION'},//5
		{name : 'DF_FECHA_VENC'},//6
		{name : 'DF_CARGA'},//7
		{name : 'IG_PLAZO_DOCTO'},//8
		{name : 'MONEDA'},//9
		{name : 'FN_MONTO', type: 'float'},//10
		{name : 'IG_PLAZO_DESCUENTO'},//11
		{name : 'FN_PORC_DESCUENTO', type :'float'},//12
		{name : 'FN_MONTO_porc_DESC'},//13
		{name : 'FN_tipoConversion'},//14 data a mostrar
		{name : 'TIPO_CONVERSION'},//14
		{name : 'FN_tipoCambio'},//14 data a mostrar
		{name : 'TIPO_CAMBIO'},//15
		{name : 'FN_montoValuado'},//
		{name : 'MODO_PLAZO'},//17
		{name : 'IC_DOCUMENTO'},//18
		{name : 'MONTO_CREDITO'},//19
		{name : 'IG_PLAZO_CREDITO'},//20
		{name : 'FECHA_VENC_CREDITO'},//21
		{name : 'FECHA_SELECCION'},//22
		{name : 'NOMBRE_IF'},//23
		{name : 'REFERENCIA_INT'},//24
		{name : 'VALOR_TASA_INT'},//25
		{name : 'MONTO_TASA_INT'},//26
		{name : 'MONTO_TOTAL_CAPITAL_INTER'},//27
		{name : 'FECHA_CAMBIO'},//28
		{name : 'CT_CAMBIO_MOTIVO'},//29
		
		{name : 'MONEDA_LINEA'},//extra para calcular tipo convesion
		{name : 'IC_MONEDA'},//estra para calcular tipo conversion
		{name : 'CG_VENTACARTERA'}//if (bandeVentaCartera.equals("S") ) {	modoPlazo ="";   }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: porcesarConsultaDataEstatusDoctoD20,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					porcesarConsultaDataEstatusDoctoD20(null, null, null);					
				}
			}
		}		
	});
	
	var consultaDataEstatusDoctoD22 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
			{name : 'NOMBRE_EPO'},//1
			{name : 'NOMBRE_DIST'},//2
			{name : 'CC_ACUSE'},//3
			{name : 'IG_NUMERO_DOCTO'},//4
			{name : 'DF_FECHA_EMISION'},//5
			{name : 'DF_FECHA_VENC'},//6
			{name : 'DF_CARGA'},//7
			{name : 'IG_PLAZO_DOCTO'},//8
			{name : 'MONEDA'},//9
			{name : 'FN_MONTO', type: 'float'},//10
			{name : 'IG_PLAZO_DESCUENTO'},//11
			{name : 'FN_PORC_DESCUENTO', type :'float'},//12
			{name : 'motoporcdesc', type:' float'},//13
			{name : 'FN_tipoConversion'},//14 data a mostrar
			{name : 'TIPO_CONVERSION'},//14
			{name : 'FN_tipoCambio'},//14 data a mostrar
			{name : 'TIPO_CAMBIO', type:'float'},//15
			{name : 'FN_montoValuado'},//16 
			{name : 'MODO_PLAZO'},//17
			{name : 'IC_DOCUMENTO'},//18
			{name : 'MONTO_CREDITO'},//19
			{name : 'IG_PLAZO_CREDITO'},//20
			{name : 'FECHA_VENC_CREDITO'},//21
			{name : 'FECHA_SELECCION'},//22
			{name : 'NOMBRE_IF'},//23
			{name : 'REFERENCIA_INT'},//24
			{name : 'VALOR_TASA_INT'},//25
			{name : 'MONTO_TASA_INT'},//26
			{name : 'MONTO_TOTAL_CAPITAL_INTER'},//27
			{name : 'CG_NUMERO_PAGO'},
			{name : 'MONEDA_LINEA'},//extra para calcular tipo convesion
			{name : 'IC_MONEDA'},//estra para calcular tipo conversion
			{name : 'CG_VENTACARTERA'}//if (bandeVentaCartera.equals("S") ) {	modoPlazo ="";   }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusDoctoD22,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusDoctoD22(null, null, null);					
				}
			}
		}		
	});

////-------------- /* /* /* */*                                            /* /* /* /* /* /*/ */* /* * /*/ */ */ */ * */*/
  var consultarDataEstatusDoctoD32 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			operacion: 'Generar'
		},
		fields :[
      {name : 'NOMBRE_EPO'},//1
      {name : 'NOMBRE_DIST'},//2
      {name : 'CC_ACUSE'},//3
		{name : 'IG_NUMERO_DOCTO'},//4
		{name : 'DF_FECHA_EMISION'},//5
		{name : 'DF_FECHA_VENC'},//6
		{name : 'DF_CARGA'},//7
		{name : 'IG_PLAZO_DOCTO'},//8
		{name : 'MONEDA'},//9
		{name : 'FN_MONTO', type: 'float'},//10
		{name : 'IG_PLAZO_DESCUENTO'},//11
		{name : 'FN_PORC_DESCUENTO', type :'float'},//12
		//		{name : 'motoporcdesc', type:' float'},//13
		{name : 'MODO_PLAZO'},//17
		{name : 'IC_DOCUMENTO'},//18
		{name : 'MONTO_CREDITO'},//19
		//----nuevos1----
		{name : 'COMISION_APLICABLE'},
		{name : 'MON_COMISION'},
		{name : 'MON_DEPOSITAR'},
      //----fin nuevos1--
		{name : 'IG_PLAZO_CREDITO'},//20
		{name : 'FECHA_VENC_CREDITO'},//21
		{name : 'FECHA_SELECCION'},//22
		{name : 'NOMBRE_IF'},//23
		{name : 'CG_CODIGO_BIN'},
		{name : 'BIN'},
		//---nuveos2
		{name : 'ORDEN_ENVIADO'},
		{name : 'ID_OPERACION'},
		{name : 'CODIGO_AUTORIZA'},
		{name : 'FECHA_REGISTRO'},
		{name : 'NUM_TARJETA'}
      //FIN NUEVOS2
    ],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEstatusDoctoD32,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataEstatusDoctoD32(null, null, null);					
				}
			}
		}		
	});
//---------------------------------------------
///-------------------
	
	var consultaDataDetalles = new Ext.data.JsonStore({
		root : 'registros',
		url : '24reporte01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalles'
		},
		fields:[
			{name:'FECH_CAMBIO'},//1
			{name:'CD_DESCRIPCION'},//2
			{name:'FECH_EMI_ANT'},//3
			{name:'FECH_EMI_NEW'},//4
			{name:'FN_MONTO_ANTERIOR'},//5
			{name:'FN_MONTO_NUEVO'},//6
			{name:'FECH_VENC_ANT'},//7
			{name:'FECH_VENC_NEW'},//8
			{name:'MODO_PLAZO_ANTERIOR'},//9
			{name:'MODO_PLAZO_NUEVO'}//10
			//{name:'CT_CAMBIO_MOTIVO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetalles,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataDetalles(null, null, null);					
				}
			}
		}					
	});	
			
//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------

	var estatus= Ext.getCmp('catalogoEstatus');
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		store: consultaDataEstatusSolic,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus ',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'motoporcdesc',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.FN_MONTO * record.data.FN_PORC_DESCUENTO)/100);///////Verificar si es correcto
				}
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'FN_tipoConversion',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CONVERSION;///Verificar si es correcto		
					}
				}
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'FN_tipoCambio',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CAMBIO;///Verificar si es correcto		
					}
				}	
			},//16
			{
				header: 'MONEDA_LINEA',
				tooltip: 'MONEDA_LINEA',
				dataIndex: 'MONEDA_LINEA',//Extra para calcular monto valuado
				hidden: true
			},
			{
				header :'IC_MONEDA',//Extra para calcular monto valuado
				tooltip : 'IC_MONEDA',
				dataIndex : 'IC_MONEDA',
				hidden: true
			},
			{
				header: 'Monto valuado',
				tootip : 'Monto valuado',
				dataIndex:'FN_montoValuado',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return Ext.util.Format.usMoney((record.data.FN_MONTO-record.data.motoporcdesc)*record.data.TIPO_CAMBIO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',//17
				renderer: function(v,params,record){
					if(record.data.CG_VENTACARTERA=='S'){
							return " ";
					}else{
						return (record.data.MODO_PLAZO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//18
			{
				header: 'N�m.docto. final',
				tooltip: 'N�m.docto. final',
				dataIndex: 'IC_DOCUMENTO',//18
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//19
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',//19
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},//20
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',//20,
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//21
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',//21
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//22
			{
				header: 'Fecha de Operaci�n Distribuidor',
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'FECHA_SELECCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//23
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//24
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',//24
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//25
			{
				header: 'Valor de tasa de inter�s',
				tooltip: 'Valor de tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//26
			{
				header: 'Monto de inter�s',
				tooltip: 'Monto de inter�s',
				dataIndex: 'MONTO_TASA_INT',//26
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//27
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL_INTER',//27
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.MONTO_CREDITO + record.data.MONTO_TASA_INT));///Verificar si es correcto
				}
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
		buttonAlign: 'left',
			items: [	'->','-',								
					{
						xtype:	'button',
						text:		'Generar PDF',
						id: 		'btnArchivoPDF0',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoPDF',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					},
					{
						xtype:	'button',
						text:		'Generar CSV',
						id: 		'btnArchivoCSV0',
						iconCls:	'icoXls',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoCSV',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}
		});
	
	var gridConsultaDocto1259 = new Ext.grid.EditorGridPanel({
		store: consultarDataEstatusDocto1259,
		id: 'gridConsultaDocto1259',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus: ',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'FN_MONTO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.FN_MONTO * record.data.FN_PORC_DESCUENTO)/100);///////Verificar si es correcto
				}
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'FN_tipoConversion',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.TIPO_CAMBIO==1){
						return "  ";
					}else{
						return record.data.TIPO_CONVERSION;///Verificar si es correcto		
					}
				}
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'FN_tipoCambio',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.TIPO_CAMBIO==1){
						return "  ";
					}else{
						return Ext.util.Format.usMoney(record.data.TIPO_CAMBIO);
					}
				}	
			},//16
			{
				header: 'Monto valuado',
				tooltip: 'Monto valuado',
				dataIndex: 'FN_montoValuado',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					if(record.data.TIPO_CAMBIO=='1'){
						return "  ";
					}else{
						return Ext.util.Format.usMoney((record.data.FN_MONTO-record.data.motoporcdesc)*record.data.TIPO_CAMBIO);///verificar si es correcto	
					}
				}
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',//17
				renderer: function(v,params,record){
					if(record.data.CG_VENTACARTERA=='S'){
							return " ";
					}else{
						return (record.data.MODO_PLAZO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},{
				//Cambios del documento
				xtype: 'actioncolumn',
				header: 'Cambios del documento',
				tooltip: 'Cambios del documento',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalles';
							return 'iconoLupa';//return 'icoModificar';										
						},
						handler: procesarVerDetalles
					}
				]
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			buttonAlign: 'left',
			items: [	'->','-',								
				{
					xtype:	'button',
					text:		'Generar PDF',
					id: 		'btnArchivoPDF00',
					iconCls:	'icoPdf',
					handler: function(boton, evento) {
					
						Ext.Ajax.request({
							url: '24reporte01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								operacion: 'ArchivoPDF',
								informacion : 'Consultar'
							}),
							callback: procesarDescargaArchivos
						});	
					}
				},
				{
					xtype:	'button',
					text:		'Generar CSV',
					id: 		'btnArchivoCSV00',
					iconCls:	'icoXls',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '24reporte01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								operacion: 'ArchivoCSV',
								informacion : 'Consultar'
							}),
							callback: procesarDescargaArchivos
						});	
					}
				}
			]
		}
	});
		
	var gridConsultaDoctoD3 = new Ext.grid.EditorGridPanel({
		store: consultaDataEstatusDoctoD3,
		id: 'gridConsultaDoctoD3',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus: Seleccionada Pyme',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//16
			{
				header: 'Monto valuado',
				tooltip: 'Monto valuado',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//18
			{
				header: 'N�m.docto. final',
				tooltip: 'N�m.docto. final',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//19
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},//20
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//21
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//22
			{
				header: 'Fecha de Operaci�n Distribuidor',
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//23
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//24
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//25
			{
				header: 'Valor de tasa de inter�s',
				tooltip: 'Valor de tasa de inter�s',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//26
			{
				header: 'Monto de inter�s',
				tooltip: 'Monto de inter�s',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//27
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
			bbar: {
			buttonAlign: 'left',
			items: [	'->','-',								
					{
						xtype:	'button',
						text:		'Generar PDF',
						id: 		'btnArchivoPDF000',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoPDF',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					},
					{
						xtype:	'button',
						text:		'Generar CSV',
						id: 		'btnArchivoCSV000',
						iconCls:	'icoXls',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoCSV',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}
		});
		
	var gridConsultaDoctoD11 = new Ext.grid.EditorGridPanel({
		store: consultarDataEstatusDoctoD11,
		id: 'gridConsultaDoctoD11',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus: Operado Pagada',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'motoporcdesc',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
							return Ext.util.Format.usMoney((record.data.FN_MONTO * record.data.FN_PORC_DESCUENTO)/100);///////Verificar si es correcto
				}
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'FN_tipoConversion',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CONVERSION;///Verificar si es correcto		
					}
				}
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'FN_tipoCambio',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CAMBIO;///Verificar si es correcto		
					}
				}	
			},//16
			{
				header: 'MONEDA_LINEA',
				tooltip: 'MONEDA_LINEA',
				dataIndex: 'MONEDA_LINEA',//Extra para calcular monto valuado
				hidden: true
			},
			{
				header :'IC_MONEDA',//Extra para calcular monto valuado
				tooltip : 'IC_MONEDA',
				dataIndex : 'IC_MONEDA',
				hidden: true
			},
			{
				header: 'Monto valuado',
				tootip : 'Monto valuado',
				dataIndex:'FN_montoValuado',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
							return "  ";
					}else{
						return Ext.util.Format.usMoney((record.data.FN_MONTO-record.data.motoporcdesc)*record.data.TIPO_CAMBIO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',//17
				renderer: function(v,params,record){
					if(record.data.CG_VENTACARTERA=='S'){
							return " ";
					}else{
						return (record.data.MODO_PLAZO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//18
			{
				header: 'N�m.docto. final',
				tooltip: 'N�m.docto. final',
				dataIndex: 'IC_DOCUMENTO',//18
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//19
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',//19
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},//20
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',//20,
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//21
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',//21
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//22
			{
				header: 'Fecha de Operaci�n Distribuidor',
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'FECHA_SELECCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//23
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//24
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',//24
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//25
			{
				header: 'Valor de tasa de inter�s',
				tooltip: 'Valor de tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//26
			{
				header: 'Monto de inter�s',
				tooltip: 'Monto de inter�s',
				dataIndex: 'MONTO_TASA_INT',//26
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//27
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL_INTER',//27
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.MONTO_CREDITO + record.data.MONTO_TASA_INT));///Verificar si es correcto
				}
			},//28
			{
				header: 'N�mero de pago',
				tooltip: 'N�mero de pago',
				dataIndex: 'CG_NUMERO_PAGO',//28
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			buttonAlign: 'left',
			items: [	'->','-',								
					{
						xtype:	'button',
						text:		'Generar PDF',
						id: 		'btnArchivoPDF0000',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoPDF',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					},
					{
						xtype:	'button',
						text:		'Generar CSV',
						id: 		'btnArchivoCSV0000',
						iconCls:	'icoXls',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoCSV',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}
		});	
		
	var gridConsultaDoctoD20 = new Ext.grid.EditorGridPanel({
		store: consultaDataEstatusDoctoD20,
		id: 'gridConsultaDoctoD20',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus: Rechazado IF',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'FN_MONTO_porc_DESC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
							return Ext.util.Format.usMoney((record.data.FN_MONTO * record.data.FN_PORC_DESCUENTO)/100);///////Verificar si es correcto
				}
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'FN_tipoConversion',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CONVERSION;///Verificar si es correcto		
					}
				}
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'FN_tipoCambio',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CAMBIO;///Verificar si es correcto		
					}
				}	
			},//16
			{
				header: 'MONEDA_LINEA',
				tooltip: 'MONEDA_LINEA',
				dataIndex: 'MONEDA_LINEA',//Extra para calcular monto valuado
				hidden: true
			},
			{
				header :'IC_MONEDA',//Extra para calcular monto valuado
				tooltip : 'IC_MONEDA',
				dataIndex : 'IC_MONEDA',
				hidden: true
			},
			{
				header: 'Monto valuado',
				tooltip: 'Monto valuado',
				dataIndex: 'FN_montoValuado',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return Ext.util.Format.usMoney(record.data.montoValuado);///Verificar si es correcto		
					}
				}
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',//17
				renderer: function(v,params,record){
					if(record.data.CG_VENTACARTERA=='S'){
							return " ";
					}else{
						return (record.data.MODO_PLAZO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//18
			{
				header: 'N�m.docto. final',
				tooltip: 'N�m.docto. final',
				dataIndex: 'IC_DOCUMENTO',//18
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//19
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',//19
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},//20
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',//20,
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//21
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',//21
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//22
			{
				header: 'Fecha de Operaci�n Distribuidor',
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'FECHA_SELECCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//23
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//24
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',//24
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//25
			{
				header: 'Valor de tasa de inter�s',
				tooltip: 'Valor de tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//26
			{
				header: 'Monto de inter�s',
				tooltip: 'Monto de inter�s',
				dataIndex: 'MONTO_TASA_INT',//26
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//27
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL_INTER',//27
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.MONTO_CREDITO + record.data.MONTO_TASA_INT));///Verificar si es correcto
				}
				
			},//28
			{
				header: 'Fecha de rechazo',
				tooltip: 'Fecha de rechazo',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//29
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			buttonAlign: 'left',
			items: [	'->','-',								
				{
					xtype:	'button',
					text:		'Generar PDF',
					id: 		'btnArchivoPDF00000',
					iconCls:	'icoPdf',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '24reporte01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								operacion: 'ArchivoPDF',
								informacion : 'Consultar'
							}),
							callback: procesarDescargaArchivos
						});	
					}
				},
				{
					xtype:	'button',
					text:		'Generar CSV',
					id: 		'btnArchivoCSV00000',
					iconCls:	'icoXls',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '24reporte01ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								operacion: 'ArchivoCSV',
								informacion : 'Consultar'
							}),
							callback: procesarDescargaArchivos
						});	
					}
				}
			]
		}	
		});	
		
	var gridConsultaDoctoD22 = new Ext.grid.EditorGridPanel({
		store: consultaDataEstatusDoctoD22,
		id: 'gridConsultaDoctoD22',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Estatus: Pendiente de pago IF',
		clicksToEdit: 1,
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//1
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//2
			{
				header: 'N�m. Acuse de Carga',
				tooltip: 'N�m. Acuse de Carga',
				dataIndex: 'CC_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//3			
			{
				header: 'N�m. docto. inicial',
				tooltip: 'N�m. docto. inicial',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//5
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//6
			{
				header: 'Fehca de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//7
			{
				header: 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex: 'DF_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,			
				align: 'center'				
			},//8
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//9
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//10
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//11
			{
				header: 'Plazo para descuento en dias',
				tooltip: 'Plazo para descuento en dias',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//12
			{
				header: '%  de descuento',
				tooltip: '% de descuento',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//13
			{
				header: 'Monto % de descuento',
				tooltip: 'Monto % de descuento',
				dataIndex: 'motoporcdesc',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.FN_MONTO * record.data.FN_PORC_DESCUENTO)/100);///////Verificar si es correcto
				}
			},//14
			{
				header: 'Tipo de conv',
				tooltip: 'Tipo de conv',
				dataIndex: 'FN_tipoConversion',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CONVERSION;///Verificar si es correcto		
					}
				}
			},//15
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'FN_tipoCambio',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return record.data.TIPO_CAMBIO;///Verificar si es correcto		
					}
				}	
			},//16
			{
				header: 'MONEDA_LINEA',
				tooltip: 'MONEDA_LINEA',
				dataIndex: 'MONEDA_LINEA',//Extra para calcular monto valuado
				hidden: true
			},
			{
				header :'IC_MONEDA',//Extra para calcular monto valuado
				tooltip : 'IC_MONEDA',
				dataIndex : 'IC_MONEDA',
				hidden: true
			},
			{
				header: 'Monto valuado',
				tootip : 'Monto valuado',
				dataIndex:'FN_montoValuado',
				renderer: function(v,params,record){
					if(record.data.IC_MONEDA==record.data.MONEDA_LINEA){
						return "  ";
					}else{
						return Ext.util.Format.usMoney((record.data.FN_MONTO-record.data.motoporcdesc)*record.data.TIPO_CAMBIO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'
			},//17
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODO_PLAZO',//17
				renderer: function(v,params,record){
					if(record.data.CG_VENTACARTERA=='S'){
						return " ";
					}else{
						return (record.data.MODO_PLAZO);
					}
				},
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//18
			{
				header: 'N�m.docto. final',
				tooltip: 'N�m.docto. final',
				dataIndex: 'IC_DOCUMENTO',//18
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//19
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_CREDITO',//19
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},//20
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'IG_PLAZO_CREDITO',//20,
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//21
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',//21
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//22
			{
				header: 'Fecha de Operaci�n Distribuidor',
				tooltip: 'Fecha de Operaci�n Distribuidor',
				dataIndex: 'FECHA_SELECCION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},//23
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//24
			{
				header: 'Referencia de tasa de inter�s',
				tooltip: 'Referencia de tasa de inter�s',
				dataIndex: 'REFERENCIA_INT',//24
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},//25
			{
				header: 'Valor de tasa de inter�s',
				tooltip: 'Valor de tasa de inter�s',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},//26
			{
				header: 'Monto de inter�s',
				tooltip: 'Monto de inter�s',
				dataIndex: 'MONTO_TASA_INT',//26
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},//27
			{
				header: 'Monto Total de Capital e Inter�s',
				tooltip: 'Monto Total de Capital e Inter�s',
				dataIndex: 'MONTO_TOTAL_CAPITAL_INTER',//27
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: function(v, params, record){
					return Ext.util.Format.usMoney((record.data.MONTO_CREDITO + record.data.MONTO_TASA_INT));///Verificar si es correcto
				}
			},//28
			{
				header: 'N�mero de pago de inter�s relacionado',
				tooltip: 'N�mero de pago de inter�s relacionado',
				dataIndex: 'CG_NUMERO_PAGO',//28
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
			bbar: {
				buttonAlign: 'left',
				items: [	'->','-',								
					{
						xtype:	'button',
						text:		'Generar PDF',
						id: 		'btnArchivoPDF000000',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoPDF',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					},
					{
						xtype:	'button',
						text:		'Generar CSV',
						id: 		'btnArchivoCSV000000',
						iconCls:	'icoXls',
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '24reporte01ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{							
									operacion: 'ArchivoCSV',
									informacion : 'Consultar'
								}),
								callback: procesarDescargaArchivos
							});	
						}
					}
				]
			}
			
		});	
//------------------- ----------------------------------------------------------*/ */* /* */ */ ** /* /* */ */ */ */ */ **/*
var gridConsultaDoctoD32 = new Ext.grid.EditorGridPanel({
store: consultarDataEstatusDoctoD32,
id: 'gridConsultaDoctoD32',
margins: '20 0 0 0',		
style: 'margin:0 auto;',
title: 'Estatus: Operada TC',
clicksToEdit: 1,
hidden: true,
columns: [
{
  header: 'EPO',
  tooltip: 'EPO',
  dataIndex: 'NOMBRE_EPO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'left'	
},//1
{
  header: 'Distribuidor',
  tooltip: 'Distribuidor',
  dataIndex: 'NOMBRE_DIST',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'left'	
},//2
{
  header: 'N�mero de Acuse de Carga',
  tooltip: 'N�mero de Acuse de Carga',
  dataIndex: 'CC_ACUSE',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},//3			
{
  header: 'N�mero de Documento Inicial',
  tooltip: 'N�mero de Documento Inicial',
  dataIndex: 'IG_NUMERO_DOCTO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},//5
{
  header: 'Fecha de Emisi�n',
  tooltip: 'Fecha de Emisi�n',
  dataIndex: 'DF_FECHA_EMISION',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'  
},//6
{
  header: 'Fecha de Vencimiento',
  tooltip: 'Fecha de Vencimiento',
  dataIndex: 'DF_FECHA_VENC',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center' ,
  renderer: function(v,params,record){
    return "N/A"	
  }
},//7
{
  header: 'Fecha de Publicaci�n',
  tooltip: 'Fecha de Publicaci�n',
  dataIndex: 'DF_CARGA',
  sortable: true,
  width: 150,			
  resizable: true,			
  align: 'center'  
},//8
{
  header: 'Plazo del Documento.',
  tooltip: 'Plazo del Documento.',
  dataIndex: 'IG_PLAZO_DOCTO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
    return "N/A"	
  }
},//9
{
  header: 'Moneda',
  tooltip: 'Moneda',
  dataIndex: 'MONEDA',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},//10
{
  header: 'Monto',
  tooltip: 'Monto Inicial del Documento',
  dataIndex: 'FN_MONTO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer:function(value){
    return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
  }
},//11
{
  header: 'Plazo para descuento en dias',
  tooltip: 'Plazo para descuento en dias',
  dataIndex: 'IG_PLAZO_DESCUENTO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},//12
{
  header: '%  de descuento',
  tooltip: '% de descuento Documento Inicial',
  dataIndex: 'FN_PORC_DESCUENTO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: Ext.util.Format.numberRenderer('0.00%')
},//13
{
  header: 'Monto % de descuento',
  tooltip: 'Monto % de descuento Documento inicial',
  dataIndex: '',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
      var montoPorDescuento = ( ((record.data.FN_MONTO) *  (record.data.FN_PORC_DESCUENTO)) /100 );
       return "<div align='right'>"+Ext.util.Format.usMoney(montoPorDescuento)+"</div>"
  }
},{
  header: 'Modalidad de plazo',
  tooltip: 'Modalidad de plazo',
  dataIndex: 'MODO_PLAZO',//17
  renderer: function(v,params,record){
    if(record.data.CG_VENTACARTERA=='S'){
        return " ";
    }else{
      return (record.data.MODO_PLAZO);
    }
  },
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},//18
{
  header: 'N�mero de Documento Final: ',
  tooltip: 'N�mero de Documento Final: ',
  dataIndex: 'IC_DOCUMENTO',//18
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	
},{
  header: 'Monto',
  tooltip: 'Monto',
  dataIndex: '',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'	,
  renderer: function(v,params,record){
      var montoPorDescuento = ( ((record.data.FN_MONTO) *  (record.data.FN_PORC_DESCUENTO)) /100 );
      var montoDos = ( (record.data.FN_MONTO) - montoPorDescuento );
       return "<div align='right'>"+Ext.util.Format.usMoney(montoDos)+"</div>"
  }
},
//----nuevos1
{
  header: '% Comision Aplicable de Terceros',
  tooltip: '% Comision Aplicable de Terceros',
  dataIndex: 'COMISION_APLICABLE',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: Ext.util.Format.numberRenderer('0.00%')
},{
  header: 'Monto Comisi�n de Terceros (IVA Incluido)',
  tooltip: 'Monto Comisi�n de Terceros (IVA Incluido)',
  dataIndex: 'MON_COMISION',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
      var montoPorDescuento = ( ((record.data.FN_MONTO) *  (record.data.FN_PORC_DESCUENTO)) /100 );
      var montoDos = ( (record.data.FN_MONTO) - montoPorDescuento );
      var montoComisTerc = ( ((montoDos) * (record.data.COMISION_APLICABLE) ) /100);
       return "<div align='right'>"+Ext.util.Format.usMoney(montoComisTerc)+"</div>"
  }
},{
  header: 'Monto a Depositar por Operaci�n',
  tooltip: 'Monto a Depositar por Operaci�n',
  dataIndex: '',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
    var montoPorDescuento = ( ((record.data.FN_MONTO) *  (record.data.FN_PORC_DESCUENTO)) /100 );
    var montoDos = ( (record.data.FN_MONTO) - montoPorDescuento );
    var montoComisTerc = ( ((montoDos) * (record.data.COMISION_APLICABLE) ) /100);
    var montoDeposOper = montoDos - montoComisTerc;
       return "<div align='right'>"+Ext.util.Format.usMoney(montoDeposOper)+"</div>"    
   /*if(record.data.MODO_PLAZO=='Pronto pago'){ 
      var montodos = ( (record.data.FN_MONTO) - ( (record.data.FN_MONTO) * ( (record.data.COMISION_APLICABLE) / 100   ) ) )
      var montodepositar= ( montodos - ( montodos * ( (record.data.COMISION_APLICABLE) / 100   ) ) )  
       return "<div align='right'>"+Ext.util.Format.usMoney(montodepositar)+"</div>"
   }else{
      var montocomision = ((record.data.FN_MONTO)* ( (record.data.COMISION_APLICABLE) / 100   ) )//(record.data.COMISION_APLICABLE))
      return "<div align='right'>"+Ext.util.Format.usMoney((record.data.FN_MONTO) - montocomision )+"</div>"
   }*/
  }
}//----fin nuevos1
,//20
{
  header: 'Plazo',
  tooltip: 'Plazo',
  dataIndex: 'IG_PLAZO_CREDITO',//20,
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
    return "N/A"	
  }
},//21
{
  header: 'Fecha de vencimiento',
  tooltip: 'Fecha de vencimiento',
  dataIndex: 'FECHA_VENC_CREDITO',//21
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
    return "N/A"	
  }
},//22
{
  header: 'Fecha de Operaci�n Distribuidor',
  tooltip: 'Fecha de Operaci�n Distribuidor',
  dataIndex: 'FECHA_SELECCION',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'  
},//23
{
  header: 'IF',
  tooltip: 'IF',
  dataIndex: 'NOMBRE_IF',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'left'	
},//24
{                                                   
  header: 'Nombre del Producto',
  tooltip: 'Nombre del Producto',
  dataIndex: '',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
  renderer: function(v,params,record){
		return "<div align='left'>"+record.data.CG_CODIGO_BIN+"-"+record.data.BIN+"</div>"
  }
},//25 , 
{                                                 
  header: 'ID Orden Enviado',
  tooltip: 'ID Orden Enviado',
  dataIndex: 'ORDEN_ENVIADO',
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'//,
//  renderer: Ext.util.Format.numberRenderer('0.0000%')
},//26
{
  header: 'Respuesta de Operaci�n',
  tooltip: 'Respuesta de Operaci�n',
  dataIndex: 'ID_OPERACION',//26
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'//,
//  renderer: Ext.util.Format.numberRenderer('$0,0.00')
},//27
{
  header: 'C�digo de Autorizaci�n',
  tooltip: 'C�digo de Autorizaci�n',
  dataIndex: 'CODIGO_AUTORIZA',//27
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center'/*,
  renderer: function(v, params, record){
    return Ext.util.Format.usMoney((record.data.MONTO_CREDITO + record.data.MONTO_TASA_INT));///Verificar si es correcto
  }*/
},
{
  header: 'N�mero Tarjeta de Cr�dito',
  tooltip: 'N�mero Tarjeta de Cr�dito',
  dataIndex: 'NUM_TARJETA',//27
  sortable: true,
  width: 150,			
  resizable: true,				
  align: 'center',
   renderer: function(v,params,record){
    return "<div align='right'>"+"XXXX-XXXX-XXXX-"+v+"</div>";
  }
}                                       //----------------------fin nuevos2                         

],			
displayInfo: true,		
emptyMsg: "No hay registros.",		
loadMask: true,
stripeRows: true,
height: 400,
width: 900,
align: 'center',
frame: false,
bbar: {
buttonAlign: 'left',
items: [	'->','-',								
    {
      xtype:	'button',
      text:		'Generar PDF',
      id: 		'btnArchivoPDF0000',
      iconCls:	'icoPdf',
      handler: function(boton, evento) {
      
      boton.setIconClass('loading-indicator');
      boton.setDisabled(true);
            
        Ext.Ajax.request({
          url: '24reporte01ext.data.jsp',
          params: Ext.apply(fp.getForm().getValues(),{							
            operacion: 'ArchivoPDF',
            informacion : 'Consultar'
          }),
          success : function(response) {
              boton.setIconClass('icoPdf');     
              boton.setDisabled(false);
            },
          callback: procesarDescargaArchivos
        });	
      }
    },
    {
      xtype:	'button',
      text:		'Generar CSV',
      id: 		'btnArchivoCSV0000',
      iconCls:	'icoXls',
      handler: function(boton, evento) {
      boton.setIconClass('loading-indicator');
      boton.setDisabled(true);
        Ext.Ajax.request({
          url: '24reporte01ext.data.jsp',
          params: Ext.apply(fp.getForm().getValues(),{							
            operacion: 'ArchivoCSV',
            informacion : 'Consultar'
          }),
          success : function(response) {
              boton.setIconClass('icoXls');     
              boton.setDisabled(false);
            },
          callback: procesarDescargaArchivos
        });	
      }
    }
  ]
}
});	  

//------------------- grid totales mio   
		var gridTotalesMio = new Ext.grid.GridPanel({
		id: 'gridTotalesMio',				
		store: consultaTotalesData,	
		style: 'margin:0 auto;',
		title:'Totales ',
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',//1
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'N�m. Documentos',//2
				dataIndex: 'TOTAL_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documentos',//3
				dataIndex: 'FN_MONTO_TOTAL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
      {
      	header: 'Total Monto a Depositar por Operaci�n',//4
				dataIndex: 'MONTO_VALUADO_TOTAL_MIO',
				width: 230,
				align: 'right',
				hidden: false,
        renderer: function(v, params, record){          
						return "<div align='right'>"+Ext.util.Format.usMoney(record.data.MONTO_VALUADO_TOTAL_MIO)+"</div>"; 
				}
      }
		],
		height: 150,
		frame: true
	});
//***************************grid totales mio

//-------------------    
		var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaTotalesData,	
		style: 'margin:0 auto;',
		title:'Totales ',
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',//1
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'N�m. Documentos',//2
				dataIndex: 'TOTAL_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documentos',//3
				dataIndex: 'FN_MONTO_TOTAL',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Valuado',//5
				dataIndex: 'MONTO_VALUADO_TOTAL',
				width: 150,
				align: 'right',
				renderer: function(v, params, record){
					if(record.data.IC_MONEDA=='1'){
						return "";
					}else{
						return Ext.util.Format.usMoney((record.data.MONTO_VALUADO_TOTAL) );///Verificar si es correcto}
					}
				}
			},
			{
				header: 'Monto Total Creditos',//4
				dataIndex: 'MONTO_CREDITO_TOTAL',
				id: 'col5',
        width: 150,
				align: 'right',
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}/*
      {
      	header: 'Total Monto a Depositar',//4
				dataIndex: 'MONTO_VALUADO_TOTAL',
				width: 150,
				align: 'right',
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			
      }*/
		],
		height: 150,
		frame: true
	});
	
	var gridDetalles = new Ext.grid.GridPanel({
		id: 'gridDetalles',
		store: consultaDataDetalles,	
		style: 'margin:0 auto;',
		clicksToEdit	:1,
		hidden: true,		
		columns: [	
			{
				header: 'Fecha del Cambio',//1
				dataIndex: 'FECH_CAMBIO',
				width: 150,
				align: 'center'				
			},						
			{
				header: 'Cambio Estatus',//2
				dataIndex: 'CD_DESCRIPCION',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Fecha Emision Anterior',//3
				dataIndex: 'FECH_EMI_ANT',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Fecha Emision Nueva',//4
				dataIndex: 'FECH_EMI_NEW',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Monto Anterior',//5
				dataIndex: 'FN_MONTO_ANTERIOR',
				width: 150,
				align: 'center',
					hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Nuevo',//6
				dataIndex: 'FN_MONTO_NUEVO',
				width: 150,
				align: 'center',
					hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha Vencimiento Anterior',//7
				dataIndex: 'FECH_VENC_ANT',
				width: 150,
				align: 'center',
				hidden: false
			},
			{
				header: 'Fecha Vencimiento Nuevo',//8
				dataIndex: 'FECH_VENC_NEW',
				width: 150,
				align: 'center',
				hidden: false				
			},
			{
				header: 'Modalidad de Plazo Anterior',//8
				dataIndex: 'MODO_PLAZO_ANTERIOR',
				width: 150,
				align: 'center',
					hidden: false
			},
			{
				header: 'Modalidad de Plazo Nuevo',//10
				dataIndex: 'MODO_PLAZO_NUEVO',
				width: 150,
				align: 'center',
					hidden: false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 850,
		align: 'center',
		frame: false
	});

	var  elementosForma  = [
		{//catalogoEstatus
			xtype :'combo',
			name : 'catalogoEstatus',
			id : 'cmbcatalogoEstatus',
			fieldLabel : 'Estatus',
			mode : 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'catalogoEstatus',
			emptyText : 'Selecione estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				'select': function(cbo, record, index) {
        Ext.getCmp('gridTotalesMio').hide();
					var fp = Ext.getCmp('forma');
					gridTotales.hide();
					hideGrids();
					if(!Ext.isEmpty(cbo.getValue())){
						fp.el.mask('Cargando...', 'x-mask-loading');
						if(cbo.getValue()=='C1' || cbo.getValue()=='C2' || cbo.getValue()=='C3' ||cbo.getValue()=='C4' ||cbo.getValue()=='D3'||cbo.getValue()=='D24'){//MOD +(||cbo.getValue()=='D24')
						//c1,c2,c3,c4
							consultaDataEstatusSolic.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						}else if(cbo.getValue()=='D1' ||cbo.getValue()=='D2' ||cbo.getValue()=='D5' || cbo.getValue()=='D9'){
							consultarDataEstatusDocto1259.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						}else if(cbo.getValue()=='D11'  ){
						
							consultarDataEstatusDoctoD11.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						}else if(cbo.getValue()=='D20' ){
						
							consultaDataEstatusDoctoD20.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						}else if(cbo.getValue()=='D22' ){
							consultaDataEstatusDoctoD22.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						} else if(cbo.getValue()=='D32'  ){
						
							consultarDataEstatusDoctoD32.load({
								params:	Ext.apply(fp.getForm().getValues(),
								{
									informacion : 'Consultar',
									operacion: 'Generar'
								}
							)});
						}
					}
				}  
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma
		});
		
	var windowDetalles = new Ext.Window({//Window para mostrar los detalles del documento
		id: 'winDetalles',
		width: 900,
		height: '500',
		modal: true,
		closeAction: 'hide',
		title: 'Detalles ',
		items:[
			NE.util.getEspaciador(20),	
			gridDetalles
		],
		bbar: {
			xtype: 'toolbar',	buttonAlign:'center',	
			buttons: ['-',
				{	xtype: 'button',	text: 'Cerrar',	iconCls: 'icoLimpiar', 	id: 'btnCerrar',
					handler: function(){																		
						Ext.getCmp('winDetalles').hide();
					} 
				}
			]
		}
	});

//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,gridConsultaDocto1259,gridConsultaDoctoD3,gridConsultaDoctoD11,
			gridConsultaDoctoD20,gridConsultaDoctoD22,gridConsultaDoctoD32,
			NE.util.getEspaciador(20),gridTotales,NE.util.getEspaciador(20),gridTotalesMio
			
		]
	});	

	catalogoEstatus.load();

});