<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.distribuidores.ConsTasasGeneralesNafin,
	com.netro.anticipos.CapturaTasasBean,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String tipoCredito = request.getParameter("tipo_credito") == null?"":(String)request.getParameter("tipo_credito");
String icIf = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String icEpo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String icPyme = request.getParameter("ic_pyme") == null?"":(String)request.getParameter("ic_pyme");
String opcionConsulta = request.getParameter("opcion_consulta") == null?"":(String)request.getParameter("opcion_consulta");
String tituloGrid = request.getParameter("titulo_grid") == null?"":(String)request.getParameter("titulo_grid");
String gridVisible = request.getParameter("grid_visible") == null?"":(String)request.getParameter("grid_visible");
String lsMoneda = "";
String lsTipoTasa = "";
String lsCveTasa = "";
String lsPlazo = "";
String lsValor = "";
String lsRelMat = "";
String lsPuntos = "";
String lsTasaPiso = "";
String lsFecha = "";
int opcion = 0;

CapturaTasasBean beanTasas = new CapturaTasasBean();
ConsTasasGeneralesNafin paginador = new ConsTasasGeneralesNafin();

String infoRegresar = "", consulta = "", totales = "", mensaje = "", nombreArchivo = "", encabezado = "";
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();
Vector lovTasas = null;
Vector vecFilas = null;
Vector vecColumnas = null;
HashMap datos = new HashMap();
List listaParametrosTasas = new ArrayList();

if (informacion.equals("catalogoTipoCredito") ) {

	consulta="{\"success\": true, \"total\": \"2\", \"registros\": [{\"clave\":\"D\",\"descripcion\":\"Descuento y/o factoraje\"},"+
				"{\"clave\":\"C\",\"descripcion\":\"Crédito en cuenta corriente\"}]}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();   	

} else if (informacion.equals("catalogoIF") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();		

} else if (informacion.equals("catalogoEPO") ) {

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTipoCredito(tipoCredito); 
	cat.setClaveIf(icIf);
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoDistribuidor") ) {

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	if(!icIf.equals("")){
		cat.setCampoClave("cpe.ic_pyme");
		cat.setCampoDescripcion("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social"); 
		cat.setIcIF(icIf); 
		cat.setClaveEpo(icEpo); 
		cat.setTipoCredito("C"); 
		cat.setOrden("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social");  
	}
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("consultaGeneral")) { 	//Tasas fijadas por Nafin en el esquema general

	vecFilas = beanTasas.ovgetTasas(4);
	if( vecFilas.size()>0){
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				lsMoneda = vecColumnas.get(0).toString();
				lsTipoTasa = vecColumnas.get(1).toString();
				lsCveTasa = vecColumnas.get(2).toString();
				lsPlazo = vecColumnas.get(3).toString();
				lsValor = vecColumnas.get(4).toString();
				lsRelMat = vecColumnas.get(5).toString();
				lsPuntos = vecColumnas.get(6).toString();
				lsTasaPiso = vecColumnas.get(7).toString();
				lsFecha = vecColumnas.get(8).toString();
				datos = new HashMap();
				datos.put("MONEDA", lsMoneda);
				datos.put("TIPO_TASA", lsTipoTasa);
				datos.put("CVE_TASA", lsCveTasa);
				datos.put("PLAZO", lsPlazo);
				datos.put("VALOR", lsValor);
				datos.put("REL_MAT", lsRelMat);
				datos.put("PUNTOS", lsPuntos);
				datos.put("TASA_PISO", lsTasaPiso);
				datos.put("FECHA", lsFecha);
				registros.add(datos);
			} 
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();
		
} else if (informacion.equals("consultaPorEsquema")) {	

	vecFilas = null;
	opcion = 0;
	if(tipoCredito.equals("D") && !icEpo.equals("") || tipoCredito.equals("C") && !icEpo.equals("") && !icPyme.equals("")){
		if(tipoCredito.equals("D")){
			vecFilas = beanTasas.ovgetTasasxEpo(4,icEpo,icIf,"1, 54","");
			if( vecFilas.size()>0){
				for (int i=0; i<vecFilas.size(); i++) {
					vecColumnas = (Vector)vecFilas.get(i);
					lsMoneda = vecColumnas.get(0).toString();
					lsTipoTasa = vecColumnas.get(1).toString();
					lsCveTasa = vecColumnas.get(2).toString();
					lsPlazo = vecColumnas.get(3).toString();
					lsValor = vecColumnas.get(4).toString();
					lsRelMat = vecColumnas.get(5).toString();
					lsPuntos = vecColumnas.get(6).toString();
					lsTasaPiso = vecColumnas.get(7).toString();
					lsFecha = vecColumnas.get(8).toString();
					datos = new HashMap();
					datos.put("MONEDA", lsMoneda);
					datos.put("TIPO_TASA", lsTipoTasa);
					datos.put("CVE_TASA", lsCveTasa);
					datos.put("PLAZO", lsPlazo);
					datos.put("VALOR", lsValor);
					datos.put("REL_MAT", lsRelMat);
					datos.put("PUNTOS", lsPuntos);
					datos.put("TASA_PISO", lsTasaPiso);
					datos.put("FECHA", lsFecha);
					registros.add(datos);
				} 
				encabezado = tituloGrid;
				opcion = 1;
			}	
			
		} else if(tipoCredito.equals("C")){
		
			vecFilas = beanTasas.ovgetTasasxPyme(4,icPyme,icIf,"1,54");
			if( vecFilas.size()>0){
				for (int i=0; i<vecFilas.size(); i++) {
					vecColumnas = (Vector)vecFilas.get(i);
					lsMoneda = vecColumnas.get(0).toString();
					lsTipoTasa = vecColumnas.get(1).toString();
					lsCveTasa = vecColumnas.get(2).toString();
					lsPlazo = vecColumnas.get(3).toString();
					lsValor = vecColumnas.get(4).toString();
					lsRelMat = vecColumnas.get(5).toString();
					lsPuntos = vecColumnas.get(6).toString();
					lsTasaPiso = vecColumnas.get(7).toString();
					lsFecha = vecColumnas.get(8).toString();
					datos = new HashMap();
					datos.put("MONEDA", lsMoneda);
					datos.put("TIPO_TASA", lsTipoTasa);
					datos.put("CVE_TASA", lsCveTasa);
					datos.put("PLAZO", lsPlazo);
					datos.put("VALOR", lsValor);
					datos.put("REL_MAT", lsRelMat);
					datos.put("PUNTOS", lsPuntos);
					datos.put("TASA_PISO", lsTasaPiso);
					datos.put("FECHA", lsFecha);
					registros.add(datos);
				}
				opcion = 2;
			} else{

				vecFilas = null;			
				vecFilas = beanTasas.ovgetTasas(4);
				if( vecFilas.size()>0){
					for (int i=0; i<vecFilas.size(); i++) {
						vecColumnas = (Vector)vecFilas.get(i);
						lsMoneda = vecColumnas.get(0).toString();
						lsTipoTasa = vecColumnas.get(1).toString();
						lsCveTasa = vecColumnas.get(2).toString();
						lsPlazo = vecColumnas.get(3).toString();
						lsValor = vecColumnas.get(4).toString();
						lsRelMat = vecColumnas.get(5).toString();
						lsPuntos = vecColumnas.get(6).toString();
						lsTasaPiso = vecColumnas.get(7).toString();
						lsFecha = vecColumnas.get(8).toString();
						datos = new HashMap();
						datos.put("MONEDA", lsMoneda);
						datos.put("TIPO_TASA", lsTipoTasa);
						datos.put("CVE_TASA", lsCveTasa);
						datos.put("PLAZO", lsPlazo);
						datos.put("VALOR", lsValor);
						datos.put("REL_MAT", lsRelMat);
						datos.put("PUNTOS", lsPuntos);
						datos.put("TASA_PISO", lsTasaPiso);
						datos.put("FECHA", lsFecha);
						registros.add(datos);
					} 
				}	
				opcion = 3;
			}
			encabezado = "Esquema de tasas para operaciones con crédito en cuenta corriente";
		}
	}
	consulta =  "{\"success\": true,\"encabezado\":\""+encabezado+"\",\"opcion\":\""+opcion+"\", \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("imprimir") || informacion.equals("generarArchivo")) { 	//Genera el archivo PDF y CSV

	try{
		opcion = Integer.parseInt(opcionConsulta);
	} catch(Exception e){
		opcion = 0;
	}
	//--------------	Realiza la consulta por esquema		-----------------------
	if(opcion == 1){
		vecFilas = beanTasas.ovgetTasasxEpo(4,icEpo,icIf,"1, 54","");
		if( vecFilas.size()>0){
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				listaParametrosTasas.add(vecColumnas);
			} 	
		}
	} else if(opcion == 2){
		vecFilas = beanTasas.ovgetTasasxPyme(4,icPyme,icIf,"1,54");
		if( vecFilas.size()>0){
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				listaParametrosTasas.add(vecColumnas);
			}
		}	
	} else if(opcion == 3){
		vecFilas = beanTasas.ovgetTasas(4);
		if( vecFilas.size()>0){
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				listaParametrosTasas.add(vecColumnas);
			} 
		}		
	}
	//-------------------------------------------------------------------------	
	
	paginador.setEncabezado(tituloGrid);
	paginador.setListaParametrosTasas(listaParametrosTasas);	
	paginador.setGridPorEsquemaVisible(gridVisible);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if(informacion.equals("imprimir")){
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
	} else if(informacion.equals("generarArchivo")){
		try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}	
	}	
	infoRegresar = jsonObj.toString();		
} 
%>
<%=infoRegresar%>
