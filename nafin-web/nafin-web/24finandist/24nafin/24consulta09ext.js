Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

/******************************************************************************
 *	Proceso para llenar el cat�logo IF														*
 ******************************************************************************/
	var procesarCatalogoIF = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_if = Ext.getCmp('ic_if1');
			Ext.getCmp('ic_if1').setValue("");
			if(ic_if.getValue()==""){
				var newRecord = new recordType({
					clave:"",
					descripcion:"Seleccionar ...."
				});							
				store.insert(0,newRecord);
				store.commitChanges();
				ic_if.setValue("");			
			}						
		}
  };

	//-------------------- Se crea el store para el combo IF --------------------  
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta09ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
		load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
/******************************************************************************
 *	Consulta para obtener los valores y EPO												*
 ******************************************************************************/
	function procesaConsultaValores(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) { 
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				if(jsonData.accion=='G') {
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
				function(){ 					
					Ext.Ajax.request({
						url: '24consulta09ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: "consultaValores",
							ic_if:jsonData.ic_if
						}),
						callback: procesaConsultaValores
					});
				});
				} else if(jsonData.accion=='OK'){
					Ext.getCmp('if_tipoliq').setValue(jsonData.if_tipoliq);
					Ext.getCmp('if_desc').setValue(jsonData.if_desc);
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
/******************************************************************************
 *	C�digo para generar el Grid																*
 ******************************************************************************/
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
						
			if(store.getTotalCount() > 0) {	
				Ext.getCmp("titulos_2").show();
				Ext.getCmp("seccion_2").show();
				Ext.getCmp("seccion_3").show();
				Ext.getCmp("seccion_4").hide();
				el.unmask();				
			} else {			
				Ext.getCmp("titulos_2").hide();
				Ext.getCmp("seccion_2").hide();
				Ext.getCmp("seccion_3").hide();
				Ext.getCmp("seccion_4").show();
				//el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
 
	//-------------------- Se crea el data store para el Grid --------------------
	var consultaData = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta09ext.data.jsp',
		baseParams: {
			informacion: 'autorizacionTasasInteres'
		},		
		fields: [
			{	name: 'EPO_DESC'},
			{	name: 'VALORES'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}	
		
	});	
	
   //-------------------- Se crea el Grid --------------------
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',
		hidden: true,		
		columns: [	
			{
				header: 'EPO ',
				tooltip: 'EPO',
				dataIndex: 'EPO_DESC',
				sortable: true,
				width: 295,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Valores',
				tooltip: 'Valores',
				dataIndex: 'VALORES',
				sortable: true,
				width: 295,			
				resizable: true,	
				align: 'left'
			}
		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 600,
		align: 'center',
		frame: false
	});

/******************************************************************************
 *	Elementos de la forma principal							   							*
 ******************************************************************************/
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'ic_if',	
			id: 'ic_if1',	
			fieldLabel: '&nbsp;&nbsp;IF', 
			mode: 'local',	
			hiddenName : 'ic_if',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogoIF,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,			
			listeners: {
				select: {
					fn: function(combo) {
					
						if(combo.getValue()!=''){
						
							Ext.getCmp("titulos_0").show();
							Ext.getCmp("titulos_1").show();
							Ext.getCmp("seccion_1").show();
							
							fp.el.mask('Procesando...', 'x-mask-loading');		
							Ext.Ajax.request({
								url: '24consulta09ext.data.jsp',
								params: {
									informacion: "consultaValores",
									ic_if:combo.getValue()
								},
								callback: procesaConsultaValores
							});	
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{ 
								})
								});
						}else  {
							Ext.getCmp("titulos_0").hide();
							Ext.getCmp("titulos_1").hide();
							Ext.getCmp("titulos_2").hide();
							Ext.getCmp("seccion_1").hide();
							Ext.getCmp("seccion_2").hide();
							Ext.getCmp("seccion_3").hide();	
							Ext.getCmp("seccion_4").hide();
							
						}
					}
				}
			}	
		},{			
			xtype:'panel',	 
			id: 'titulos_0', 
			hidden: true, 
			layout: 'table', 
			border: false,	
			width: 600,	
			layoutConfig:{ columns: 3 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	
					width: 600,
					frame: true,	
					colspan: '3',	
					border: false,	
					html: '<div align="center"><b>Secci�n 1: Esquemas de operaci�n</b	><br>&nbsp;</div>'	
				}
			]
		},{			
			xtype: 'panel',  
			id: 'titulos_1',  
			hidden: true,	
			layout: 'table', 
			border: true,  
			width: 600,	
			layoutConfig:{ columns: 3 },
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [
				{	width: 200,	frame: false,	border: false,	html: '<div class="formas" align="center"><br> <b> Nombre del par�metro <br>&nbsp;</div>'	},
				{	width: 150,	frame: false,	border: false,	html: '<div class="formas" align="center"><br> <b> Tipo de dato <br>&nbsp;</div>'	},
				{	width: 250,	frame: false,	border: false,	html: '<div class="formas" align="center"><br> <b> Valores<br>&nbsp;</div>'	}							
			]
		},{			
			xtype: 'panel',  
			id: 'seccion_1',  
			hidden: true,	
			layout: 'table', 
			border: true,  
			width: 600,	
			layoutConfig: { columns:3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	
					width: 200,	
					frame: false,	
					border: false,	
					html: '<div align="left"><br> Tipo de liquidaci�n de cr�dito (Dep�sito o cargo) <br><br></div>'	
				},{	
					width: 150,	
					frame: false,	
					border: false,	
					html: '<div align="center"><br> C <br><br>&nbsp;</div>'	
				},{	
					width: 250, 
					xtype: 'displayfield', 
					id: 'if_tipoliq', 
					name: 'if_tipoliq'
				}	
			]
		},{			
			xtype: 'panel',	 
			id: 'titulos_2', 
			hidden: true, 
			layout: 'table', 
			border: false,	
			width: 600,	
			layoutConfig: { columns: 3 },
			defaults: { align: 'center', bodyStyle: 'padding:2px,'},
			items: [ 
				{	
					width: 600,
					frame: true,	
					colspan: '3',	
					border: false,	
					html: '<div align="center"><b>Secci�n 2: Autorizaci�n para establecer las tasas de inter�s por EPO </b><br>&nbsp;</div>'	
				}
			]
		},{			
			xtype: 'panel',  
			id: 'seccion_2',  
			hidden: true, 
			border: false, 
			layout: 'table',	
			width: 600,	
			layoutConfig: { columns: 3 },
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [
				{	
					width: 600,	
					xtype: 'textfield',	
					msgTarget: 'side',	
					id: 'if_desc',	
					name: 'if_desc',
					readOnly: true
				}	
			]
		},{			
			xtype: 'panel',  
			id: 'seccion_3',  
			hidden: true, 
			border: false, 
			layout: 'table',	
			width: 600,	
			layoutConfig: { columns: 3 },
			items: gridConsulta
		},{			
			xtype: 'panel',  
			id: 'seccion_4',  
			hidden: true, 
			border: true, 
			layout: 'table',	
			width: 600,	
			layoutConfig: { columns: 3 },
			defaults: {align: 'center', bodyStyle: 'padding:2px,'},
			items: [
				{	
					width: 600,	
					frame: false,	
					border: false,	
					html: '<div align="center"><b>No se encontraron EPOs para establecer tasas de inter�s</b><br>&nbsp;</div>'	
				}			
			]
		}
	];

/******************************************************************************
 *	Form Panel principal    																	*
 ******************************************************************************/	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame	: false,
		width: 600,		
		autoHeight	: true,
		title: ' Par�metros por IF',				
		layout : 'form',
		style: 'margin:0 auto;',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: elementosForma
	});

/******************************************************************************
 *	Contenedor Principal          															*
 ******************************************************************************/	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)			
		]
	});

/******************************************************************************
 *	Inicializar parametros         															*
 ******************************************************************************/
	catalogoIF.load();
	
});