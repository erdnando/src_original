Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
	//---------------------- Descargar Archivos --------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(Ext.getCmp('estatus1').getValue()=='2'){
				Ext.getCmp('btnImprimir_2').setIconClass('icoPdf');
				Ext.getCmp('btnGenerarCSV_2').setIconClass('icoXls');
			} else if(Ext.getCmp('estatus1').getValue()=='21'){
				Ext.getCmp('btnImprimir_21').setIconClass('icoPdf');
				Ext.getCmp('btnGenerarCSV_21').setIconClass('icoXls');
			} else if(Ext.getCmp('estatus1').getValue()=='4' || Ext.getCmp('estatus1').getValue()=='22' || Ext.getCmp('estatus1').getValue()=='24'){
				Ext.getCmp('btnImprimir').setIconClass('icoPdf');
				Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			} else if(Ext.getCmp('estatus1').getValue()=='23' || Ext.getCmp('estatus1').getValue()=='34'){
				Ext.getCmp('btnImprimir_22').setIconClass('icoPdf');
				Ext.getCmp('btnGenerarCSV_22').setIconClass('icoXls');
			}
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

/*****************************************************************************
 * 							C�digo para el combo Estatus								  *
 *****************************************************************************/
  	var procesarCatalogoEstatus = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var estatus = Ext.getCmp('estatus1');
				Ext.getCmp('estatus1').setValue("");
				if(estatus.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:""
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					estatus.setValue("");			
				}	
			} 
		}
  };	
  
	//----------- Se crea el store para el combo Estatus -----------------
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: true,		
		listeners: {	
		//load: procesarCatalogoEstatus,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
/*****************************************************************************
 * C�digo para el grid cuando se selecciona en el combo estatus la opci�n 22 *
 *****************************************************************************/	
	var procesarConsultaData_22 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var gridConsultaData_22 = Ext.getCmp('gridConsultaData_22');	
		var el = gridConsultaData_22.getGridEl();	
		if (arrRegistros != null) {
			fp.el.unmask();
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaData_22.getColumnModel();
			
			if(store.getTotalCount() < 1) {
				
				Ext.getCmp('gridConsultaData_22').setTitle(jsonData.tituloGrid);
				Ext.getCmp('gridConsultaData_22').show();
				Ext.getCmp('btnImprimir_22').disable();
				Ext.getCmp('btnGenerarCSV_22').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('gridConsultaDataComun').hide();
				Ext.getCmp('gridConsultaData_21').hide();				
			} else{
				
				el.unmask();
				Ext.getCmp('gridConsultaData_22').setTitle(jsonData.tituloGrid);
				Ext.getCmp('btnImprimir_22').enable();
				Ext.getCmp('btnGenerarCSV_22').enable();				
				Ext.getCmp('gridConsultaData_22').show();
				fp.el.mask('Procesando...', 'x-mask-loading');
				consultaTotalBis.load({
					params: Ext.apply({
						informacion: 'consultaTotal',
						status: Ext.getCmp('estatus1').getValue()
					})
				});
				
			}
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//-------------------- Se crea el store para el Grid -----------------
	var consultaData_22 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaData_22'
		},		
		fields: [
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_DIST'},
			{	name: 'CC_ACUSE'},
			{	name: 'IG_NUMERO_DOCTO'},
			{	name: 'DF_FECHA_EMISION'},
			{	name: 'DF_FECHA_VENC'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'IG_PLAZO_DOCTO'},
			{	name: 'FN_MONTO'},		
			{	name: 'MONEDA'},
			{	name: 'IG_PLAZO_DESCUENTO'},
			{	name: 'FN_PORC_DESCUENTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'TIPO_CONVERSION'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'TIPO_CREDITO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODO_PLAZO'},
			{	name: 'IC_DOCUMENTO'},
			{	name: 'MONTO_CREDITO'},
			{	name: 'IG_PLAZO_CREDITO'},
			{	name: 'FECHA_VENC_CREDITO'},
			{	name: 'NOMBRE_IF'},
			{	name: 'REFERENCIA_INT'},
			{	name: 'VALOR_TASA_INT'},
			{	name: 'VALOR_TASA_INT'},
			{	name: 'MONTO_TASA_INT'},
			{	name: 'MONTO_CAPITAL_INT'},
			{	name: 'CAUSA'},
			{	name: 'FECHA_CAMBIO'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaData_22,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaData_22(null, null, null);					
				}
			}
		}	
	});
		
   //-------------------- Se crea el Grid --------------------
	var gridConsultaData_22 = new Ext.grid.EditorGridPanel({	
		store: consultaData_22,
		id: 'gridConsultaData_22',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'EPO ',
				tooltip: 'EPO ',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,			
				resizable: true,	
				align: 'left'				
			},{
				header: 'Distribuidor ',
				tooltip: 'Distribuidor ',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'N�mero de Acuse de carga ',
				tooltip: 'N�mero de Acuse de carga ',
				dataIndex: 'CC_ACUSE',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'N�mero de documento inicial ',
				tooltip: 'N�mero de documento inicial ',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Fecha de emisi�n ',
				tooltip: 'Fecha de emisi�n ',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,		
				resizable: true,
				align: 'center'
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'Fecha de publicaci�n ',
				tooltip: 'Fecha de publicaci�n ',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Plazo de documento ',
				tooltip: 'Plazo de documento ',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: 'Moneda ',
				tooltip: 'Moneda ',
				dataIndex: 'MONEDA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'FN_MONTO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Plazo para descuento en d�as  ',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Porcentaje de descuento',
				tooltip: 'Porcentaje de descuento ',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'
			},{
				header: 'Monto porcentaje de descuento ',
				tooltip: 'Monto porcentaje de descuento ',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,		
				resizable: true,	
				align: 'center'			
			},{
				header: 'Modalidad de plazo ',
				tooltip: 'Modalidad de plazo ',
				dataIndex: 'MODO_PLAZO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'N�mero de documento final ',
				tooltip: 'N�mero de documento final ',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Tipo de cr�dito ',
				tooltip: 'Tipo de cr�dito ',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'IF ',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,		
				resizable: true,	
				align: 'left'				
			},{
				header: 'Monto documento Final ',
				tooltip: 'Monto documento Final ',
				dataIndex: 'FN_MONTO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},{
				header: 'Plazo ',
				tooltip: 'Plazo ',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Referencia de tasa de inter�s ',
				tooltip: 'Referencia de  tasa de inter�s ',
				dataIndex: 'REFERENCIA_INT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Valor  Tasa de Inter�s ',
				tooltip: 'Valor  Tasa de Inter�s ',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,		
				resizable: true,
				align: 'center'
			},{
				header: 'Monto de  Inter�s ',
				tooltip: 'Monto de  Inter�s ',
				dataIndex: 'MONTO_TASA_INT',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},{
				header: 'Monto total de capital',
				tooltip: 'Monto total de capital ',
				dataIndex: 'MONTO_CAPITAL_INT',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},{
				header: 'Fecha de  rechazo',
				tooltip: 'Fecha de  rechazo ',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Causa',
				tooltip: 'Causa ',
				dataIndex: 'CAUSA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: ' ',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		//autoHeight: true,
		width: 800,
		height: 300,
		hidden: true,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV_22',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({
								informacion: 'generarArchivo',
								status: Ext.getCmp('estatus1').getValue()			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir_22',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({  //fp.getForm().getValues(),
								informacion: 'imprimir',
								status: Ext.getCmp('estatus1').getValue()
							}),						
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});	

/*****************************************************************************
 * C�digo para el grid cuando se selecciona en el combo estatus la opci�n 2  *
 *****************************************************************************/	
	var procesarConsultaData_2 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var gridConsultaData_2 = Ext.getCmp('gridConsultaData_2');	
		var el = gridConsultaData_2.getGridEl();	
		if (arrRegistros != null) {
			fp.el.unmask();
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaData_2.getColumnModel();
			if(store.getTotalCount() < 1) {	
				Ext.getCmp('gridConsultaData_2').setTitle(jsonData.tituloGrid);
				Ext.getCmp('gridConsultaData_2').show();
				Ext.getCmp('btnImprimir_2').disable();
				Ext.getCmp('btnGenerarCSV_2').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('gridConsultaDataComun').hide();
				Ext.getCmp('gridConsultaData_21').hide();				
			} else{	
				el.unmask();
				Ext.getCmp('gridConsultaData_2').setTitle(jsonData.tituloGrid);
				Ext.getCmp('btnImprimir_2').enable();
				Ext.getCmp('btnGenerarCSV_2').enable();				
				Ext.getCmp('gridConsultaData_2').show();
				fp.el.mask('Procesando...', 'x-mask-loading');
				consultaTotalBis.load({
					params: Ext.apply({
						informacion: 'consultaTotal',
						status: Ext.getCmp('estatus1').getValue()
					})
				});
				
			}
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//-------------------- Se crea el store para el Grid -----------------
	var consultaData_2 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaData_2'
		},		
		fields: [
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_DIST'},
			{	name: 'CC_ACUSE'},
			{	name: 'IG_NUMERO_DOCTO'},
			{	name: 'DF_FECHA_EMISION'},
			{	name: 'DF_FECHA_VENC'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'IG_PLAZO_DOCTO'},
			{	name: 'FN_MONTO'},		
			{	name: 'MONEDA'},
			{	name: 'IG_PLAZO_DESCUENTO'},
			{	name: 'FN_PORC_DESCUENTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'TIPO_CONVERSION'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODO_PLAZO'},
			{	name: 'IC_DOCUMENTO'},
			{	name: 'MONTO_CREDITO'},
			{	name: 'IG_PLAZO_CREDITO'},
			{	name: 'FECHA_VENC_CREDITO'},
			{	name: 'NOMBRE_IF'},
			{	name: 'REFERENCIA_INT'},
			{	name: 'VALOR_TASA_INT'},
			{	name: 'MONTO_TASA_INT'},
			{	name: 'MONTO_CAPITAL_INT '},
			{	name: 'FECHA_CAMBIO'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaData_2,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaData_2(null, null, null);					
				}
			}
		}	
	});
		
   //-------------------- Se crea el Grid --------------------
	var gridConsultaData_2 = new Ext.grid.EditorGridPanel({	
		store: consultaData_2,
		id: 'gridConsultaData_2',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'EPO ',
				tooltip: 'EPO ',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,			
				resizable: true,	
				align: 'center'				
			},{
				header: 'Distribuidor ',
				tooltip: 'Distribuidor ',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'N�mero de Acuse de Carga ',
				tooltip: 'N�mero de Acuse de Carga ',
				dataIndex: 'CC_ACUSE',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'N�mero de documento inicial ',
				tooltip: 'N�mero de documento inicial ',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Fecha de emisi�n ',
				tooltip: 'Fecha de emisi�n ',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,		
				resizable: true,
				align: 'center'
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'Fecha de publicaci�n ',
				tooltip: 'Fecha de publicaci�n ',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Plazo docto ',
				tooltip: 'Plazo docto ',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'FN_MONTO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Moneda ',
				tooltip: 'Moneda ',
				dataIndex: 'MONEDA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Plazo para descuento en d�as ',
				tooltip: 'Plazo para descuento en d�as ',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: '% de descuento ',
				tooltip: '% de descuento ',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto % de descuento ',
				tooltip: 'Monto % de descuento ',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,		
				resizable: true,	
				align: 'center'			
			},{
				header: 'Tipo conv. ',
				tooltip: 'Tipo conv. ',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Tipo cambio ',
				tooltip: 'Tipo cambio ',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto valuado en pesos ',
				tooltip: 'Monto valuado en pesos ',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,		
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Modalidad de plazo ',
				tooltip: 'Modalidad de plazo ',
				dataIndex: 'MODO_PLAZO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'N�mero de documento final ',
				tooltip: 'N�mero de documento final ',
				dataIndex: 'IC_DOCUMENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Plazo ',
				tooltip: 'Plazo ',
				dataIndex: 'IG_PLAZO_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'IF ',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Referencia tasa de inter�s ',
				tooltip: 'Referencia tasa de inter�s ',
				dataIndex: 'REFERENCIA_INT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Valor de inter�s ',
				tooltip: 'Valor de inter�s ',
				dataIndex: 'VALOR_TASA_INT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto de intereses ',
				tooltip: 'Monto de intereses ',
				dataIndex: 'MONTO_TASA_INT',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},{
				header: 'Monto total de capital e intereses ',
				tooltip: 'Monto total de capital e intereses ',
				dataIndex: 'MONTO_CAPITAL_INT',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Fecha de rechazo ',
				tooltip: 'Fecha de rechazo ',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: ' ',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		//autoHeight: true,
		width: 800,
		height: 300,
		hidden: true,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV_2',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({
								informacion: 'generarArchivo',
								status: Ext.getCmp('estatus1').getValue()			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir_2',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({  //fp.getForm().getValues(),
								informacion: 'imprimir',
								status: Ext.getCmp('estatus1').getValue()
							}),						
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});	 
	
/*****************************************************************************
 * C�digo para el grid cuando se selecciona en el combo estatus la opcion 21 *
 *****************************************************************************/
	var procesarConsultaData_21 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var gridConsultaData_21 = Ext.getCmp('gridConsultaData_21');	
		var el = gridConsultaData_21.getGridEl();	
		if (arrRegistros != null) {
			fp.el.unmask();
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaData_21.getColumnModel();
			if(store.getTotalCount() < 1) {	
				Ext.getCmp('gridConsultaData_21').setTitle(jsonData.tituloGrid);
				Ext.getCmp('btnImprimir_21').disable();
				Ext.getCmp('btnGenerarCSV_21').disable();
				Ext.getCmp('gridConsultaData_21').show();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('gridConsultaDataComun').hide();
				Ext.getCmp('gridConsultaData_2').hide();		
			} else{
				el.unmask();
				Ext.getCmp('gridConsultaData_21').setTitle(jsonData.tituloGrid);
				Ext.getCmp('btnImprimir_21').enable();
				Ext.getCmp('btnGenerarCSV_21').enable();
				Ext.getCmp('gridConsultaData_21').show();
				fp.el.mask('Procesando...', 'x-mask-loading');
				consultaTotal.load({
					params: Ext.apply({
						informacion: 'consultaTotal',
						status: Ext.getCmp('estatus1').getValue()
					})
				});	
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//-------------------- Se crea el store para el Grid -----------------
	var consultaData_21 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaData_21'
		},		
		fields: [
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_DIST'},
			{	name: 'CC_ACUSE'},
			{	name: 'IG_NUMERO_DOCTO'},
			{	name: 'DF_FECHA_EMISION'},
			{	name: 'DF_FECHA_VENC'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'IG_PLAZO_DOCTO'},
			{	name: 'FN_MONTO'},
			{	name: 'MONEDA'},
			{	name: 'IG_PLAZO_DESCUENTO'},
			{	name: 'FN_PORC_DESCUENTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'TIPO_CONVERSION'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODO_PLAZO'},
			{	name: 'FECHA_CAMBIO'},
			{	name: 'FECHA_EMISION_ANT'},
			{	name: 'FECHA_VENC_ANT'},
			{	name: 'FN_MONTO_ANTERIOR'},
			{	name: 'MODO_PLAZO_ANT'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaData_21,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaData_21(null, null, null);					
				}
			}
		}	
	});
		
   //-------------------- Se crea el Grid --------------------
	var gridConsultaData_21 = new Ext.grid.EditorGridPanel({	
		store: consultaData_21,
		id: 'gridConsultaData_21',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'EPO ',
				tooltip: 'EPO ',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,			
				resizable: true,	
				align: 'center'				
			},{
				header: 'Distribuidor ',
				tooltip: 'Distribuidor ',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'N�mero de Acuse de Carga ',
				tooltip: 'N�mero de Acuse de Carga ',
				dataIndex: 'CC_ACUSE',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'N�mero de documento inicial ',
				tooltip: 'N�mero de documento inicial ',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Fecha de emisi�n ',
				tooltip: 'Fecha de emisi�n ',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,		
				resizable: true,
				align: 'center'
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'Fecha de publicaci�n ',
				tooltip: 'Fecha de publicaci�n ',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Plazo docto ',
				tooltip: 'Plazo docto ',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'FN_MONTO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Moneda ',
				tooltip: 'Moneda ',
				dataIndex: 'MONEDA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Plazo para descuento en d�as ',
				tooltip: 'Plazo para descuento en d�as ',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: '% de descuento ',
				tooltip: '% de descuento ',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto % de descuento ',
				tooltip: 'Monto % de descuento ',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Tipo conv. ',
				tooltip: 'Tipo conv. ',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Tipo cambio ',
				tooltip: 'Tipo cambio ',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto valuado en pesos ',
				tooltip: 'Monto valuado en pesos ',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Modalidad de plazo ',
				tooltip: 'Modalidad de plazo ',
				dataIndex: 'MODO_PLAZO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Fecha de Cambio ',
				tooltip: 'Fecha de Cambio ',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Anterior Fecha de emisi�n ',
				tooltip: 'Anterior Fecha de emisi�n ',
				dataIndex: 'FECHA_EMISION_ANT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Anterior Fecha de vencimiento ',
				tooltip: 'Anterior Fecha de vencimiento ',
				dataIndex: 'FECHA_VENC_ANT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Anterior monto del documento ',
				tooltip: 'Anterior monto del documento ',
				dataIndex: 'FN_MONTO_ANTERIOR',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: 'Anterior modalidad de plazo ',
				tooltip: 'Anterior modalidad de plazo ',
				dataIndex: 'MODO_PLAZO_ANT',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: ' ',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		//autoHeight: true,
		width: 800,
		height: 300,
		hidden:true,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV_21',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({
								informacion: 'generarArchivo',
								status: Ext.getCmp('estatus1').getValue()			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir_21',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({  //fp.getForm().getValues(),
								informacion: 'imprimir',
								status: Ext.getCmp('estatus1').getValue()
							}),						
							callback: descargaArchivo
						});
					}
				}				
			]
		}		
	});	 
 
/*****************************************************************************
 * C�digo para el grid cuando se selecciona en el combo estatus las opciones *
 *	4, 22 o 24. Ya que ienen los mismos campos y se utiliza el mismo grid     *
 * para desplegar los datos																  *
 *****************************************************************************/
	var procesarConsultaDataComun = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var gridConsultaDataComun = Ext.getCmp('gridConsultaDataComun');	
		var el = gridConsultaDataComun.getGridEl();	
		if (arrRegistros != null) {
			fp.el.unmask();
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaDataComun.getColumnModel();
			if(store.getTotalCount() < 1) {
				Ext.getCmp('gridConsultaDataComun').setTitle(jsonData.tituloGrid);
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('gridConsultaDataComun').show();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('gridConsultaData_2').hide();
				Ext.getCmp('gridConsultaData_21').hide();		
			} else{
				el.unmask();
				Ext.getCmp('gridConsultaDataComun').show();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();
				Ext.getCmp('gridConsultaDataComun').setTitle(jsonData.tituloGrid);
				fp.el.mask('Procesando...', 'x-mask-loading');
				consultaTotal.load({
					params: Ext.apply({
						informacion: 'consultaTotal',
						status: Ext.getCmp('estatus1').getValue()
					})
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//-------------------- Se crea el store para el Grid -----------------
	var consultaDataComun = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaData_4_22_24'
		},		
		fields: [
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_DIST'},
			{	name: 'CC_ACUSE'},
			{	name: 'IG_NUMERO_DOCTO'},
			{	name: 'DF_FECHA_EMISION'},
			{	name: 'DF_FECHA_VENC'},
			{	name: 'FECHA_PUBLICACION'},
			{	name: 'IG_PLAZO_DOCTO'},			
			{	name: 'FN_MONTO'},		
			{	name: 'MONEDA'},
			{	name: 'IG_PLAZO_DESCUENTO'},
			{	name: 'FN_PORC_DESCUENTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'TIPO_CONVERSION'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'}, 
			{	name: 'MODO_PLAZO'},
			{	name: 'FECHA_CAMBIO'},
			{	name: 'CAUSA'} //Campo vacio para la opcion 22
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaDataComun,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaDataComun(null, null, null);					
				}
			}
		}	
	});

   //-------------------- Se crea el Grid --------------------
	var gridConsultaDataComun = new Ext.grid.EditorGridPanel({	
		store: consultaDataComun,
		id: 'gridConsultaDataComun',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'EPO ',
				tooltip: 'EPO ',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,			
				resizable: true,
				align: 'center'				
			},{
				header: 'Distribuidor ',
				tooltip: 'Distribuidor ',
				dataIndex: 'NOMBRE_DIST',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: 'N�mero de Acuse de Carga ',
				tooltip: 'N�mero de Acuse de Carga ',
				dataIndex: 'CC_ACUSE',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'N�mero de documento inicial ',
				tooltip: 'N�mero de documento inicial ',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,			
				resizable: true,
				align: 'center'				
			},{
				header: 'Fecha de emisi�n ',
				tooltip: 'Fecha de emisi�n ',
				dataIndex: 'DF_FECHA_EMISION',
				sortable: true,		
				resizable: true,	
				align: 'center'
			},{
				header: 'Fecha de vencimiento ',
				tooltip: 'Fecha de vencimiento ',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,	
				resizable: true,	
				align: 'center'
			},{
				header: 'Fecha de publicaci�n ',
				tooltip: 'Fecha de publicaci�n ',
				dataIndex: 'FECHA_PUBLICACION',
				sortable: true,			
				resizable: true,
				align: 'center'			
			},{
				header: 'Plazo docto ',
				tooltip: 'Plazo docto ',
				dataIndex: 'IG_PLAZO_DOCTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'FN_MONTO',
				sortable: true,		
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Moneda ',
				tooltip: 'Moneda ',
				dataIndex: 'MONEDA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Plazo para descuento en d�as ',
				tooltip: 'Plazo para descuento en d�as ',
				dataIndex: 'IG_PLAZO_DESCUENTO',
				sortable: true,		
				resizable: true,
				align: 'center'				
			},{
				header: '% de descuento ',
				tooltip: '% de descuento ',
				dataIndex: 'FN_PORC_DESCUENTO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto % de descuento ',
				tooltip: 'Monto % de descuento ',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,		
				resizable: true,	
				align: 'center'
			},{
				header: 'Tipo conv ',
				tooltip: 'Tipo conv ',
				dataIndex: 'TIPO_CONVERSION',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Tipo cambio ',
				tooltip: 'Tipo cambio ',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Monto valuado en pesos ',
				tooltip: 'Monto valuado en pesos ',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,		
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Modalidad de plazo ',
				tooltip: 'Modalidad de plazo ',
				dataIndex: 'MODO_PLAZO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Fecha de Cambio ',
				tooltip: 'Fecha de Cambio ',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			},{
				header: 'Causa ',
				tooltip: 'Causa ',
				dataIndex: 'CAUSA',
				sortable: true,		
				resizable: true,	
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: ' ',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		//autoHeight: true,
		width: 800,
		height: 300,
		hidden: true,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo ',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({
								informacion: 'generarArchivo',
								status: Ext.getCmp('estatus1').getValue()			
							}),							
							callback: descargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24reporte02ext.data.jsp',
							params: Ext.apply({  //fp.getForm().getValues(),
								informacion: 'imprimir',
								status: Ext.getCmp('estatus1').getValue()
							}),						
							callback: descargaArchivo
						});
					}
				}				
			]
		}
	});	
	 
/*****************************************************************************
 * C�digo para el grid de totales cuando se selecciona en el combo           *
 * la opciones 4, 21, 22, y 24														     *
 *****************************************************************************/
	var procesarConsultaTotal = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaTotal = Ext.getCmp('gridConsultaTotal');	
		var el = gridConsultaTotal.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaTotal.getColumnModel();
			if(store.getTotalCount() < 1) {	
				Ext.getCmp('gridConsultaTotal').hide();
			} else{
				Ext.getCmp('gridConsultaTotal').show();
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	//-------------------- Se crea el store para el Grid -----------------
	var consultaTotal = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaTotal'
		},		
		fields: [
			{	name: 'TOTALES'},
			{	name: 'TOTAL_DOCTOS'},
			{	name: 'TOTAL_MONTO'},
			{	name: 'TOTAL_MONTO_VALUADO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaTotal,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaTotal(null, null, null);					
				}
			}
		}	
	});
	
	//-------------------- Se crea el Grid --------------------
	var gridConsultaTotal = new Ext.grid.EditorGridPanel({	
		store: consultaTotal,
		id: 'gridConsultaTotal',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'Totales ',
				tooltip: 'Totales ',
				dataIndex: 'TOTALES',
				sortable: true,			
				resizable: true,
				width: 199,
				align: 'center'				
			},{
				header: 'Doctos ',
				tooltip: 'Doctos ',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,			
				resizable: true,
				width: 199,
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'TOTAL_MONTO',
				sortable: true,		
				resizable: true,
				width: 199,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Monto Valuado ',
				tooltip: 'Monto Valuado ',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				sortable: true,	
				resizable: true,
				width: 199,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],			
		displayInfo: true,	
		//title: ' ',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 800,
		//height: 300,
		hidden: true,
		align: 'center',
		frame: false
	});	
 	 
/*****************************************************************************
 * C�digo para el grid de totales cuando se selecciona en el combo           *
 * la opcion 2														                       *
 *****************************************************************************/
	var procesarConsultaTotalBis = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaTotalBis = Ext.getCmp('gridConsultaTotalBis');	
		var el = gridConsultaTotalBis.getGridEl();	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaTotalBis.getColumnModel();
			if(store.getTotalCount() < 1) {	
				Ext.getCmp('gridConsultaTotalBis').hide();
			} else{
				Ext.getCmp('gridConsultaTotalBis').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	//-------------------- Se crea el store para el Grid -----------------
	var consultaTotalBis = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24reporte02ext.data.jsp',
		baseParams: {
			informacion: 'consultaTotal'
		},		
		fields: [
			{	name: 'TOTALES'},
			{	name: 'TOTAL_DOCTOS'},
			{	name: 'TOTAL_MONTO'},
			{	name: 'TOTAL_MONTO_VALUADO'},
			{	name: 'TOTAL_MONTO_CREDITO'},
			{	name: 'TOTAL_MONTO_DESCUENTO'},
			{	name: 'TOTAL_MONTO_INTERESES'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaTotalBis,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaTotalBis(null, null, null);					
				}
			}
		}	
	});
	
	//-------------------- Se crea el Grid --------------------
	var gridConsultaTotalBis = new Ext.grid.EditorGridPanel({	
		store: consultaTotalBis,
		id: 'gridConsultaTotalBis',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'Totales ',
				tooltip: 'Totales ',
				dataIndex: 'TOTALES',
				sortable: true,			
				resizable: true,
				width: 114,
				align: 'center'				
			},{
				header: 'Doctos ',
				tooltip: 'Doctos ',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,			
				resizable: true,
				width: 114,
				align: 'center'				
			},{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'TOTAL_MONTO',
				sortable: true,		
				resizable: true,
				width: 114,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},{
				header: 'Monto Valuado ',
				tooltip: 'Monto Valuado ',
				dataIndex: 'TOTAL_MONTO_VALUADO',
				sortable: true,	
				resizable: true,
				width: 114,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto cr�dito ',
				tooltip: 'Monto cr�dito ',
				dataIndex: 'TOTAL_MONTO_CREDITO',
				sortable: true,	
				resizable: true,
				width: 114,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto descuento ',
				tooltip: 'Monto descuento ',
				dataIndex: 'TOTAL_MONTO_DESCUENTO',
				sortable: true,	
				resizable: true,
				width: 114,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto intereses ',
				tooltip: 'Monto intereses ',
				dataIndex: 'TOTAL_MONTO_INTERESES',
				sortable: true,	
				resizable: true,
				width: 114,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],			
		displayInfo: true,	
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		width: 800,
		hidden: true,
		align: 'center',
		frame: false
	});	
 
/*****************************************************************************
 * 							Elementos y paneles para la forma						  *
 *****************************************************************************/	
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'estatus',	
			id: 'estatus1',	
			fieldLabel: '&nbsp;&nbsp;Estatus', 
			mode: 'local',		
			emptyText: 'Seleccione ... ',
			forceSelection: true,	
			triggerAction: 'all',	
			typeAhead: true,
			minChars: 1,	
			store: catalogoEstatus,	
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 500,			
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!=''){
							if(combo.getValue()=='4' || combo.getValue()=='22' || combo.getValue()=='24'){
								Ext.getCmp("gridConsultaData_2").hide();
								Ext.getCmp("gridConsultaData_22").hide();
								Ext.getCmp("gridConsultaData_21").hide();
								Ext.getCmp('gridConsultaTotal').hide();
								Ext.getCmp('gridConsultaTotalBis').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consultaDataComun.load({
									params: {
										status:combo.getValue()
									}				
								});
							} else if(combo.getValue()=='2'){
								
								//Ext.getCmp("noHayDatos").hide();
								Ext.getCmp("gridConsultaDataComun").hide();
								Ext.getCmp("gridConsultaData_22").hide();
								Ext.getCmp("gridConsultaData_21").hide();		
								Ext.getCmp('gridConsultaTotal').hide();
								Ext.getCmp('gridConsultaTotalBis').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consultaData_2.load({
									params: {
										status:combo.getValue()
									}				
								});
								
							} else if(combo.getValue()=='23' || combo.getValue()=='34'){
								
								//Ext.getCmp("noHayDatos").hide();
								Ext.getCmp("gridConsultaDataComun").hide();
								Ext.getCmp("gridConsultaData_21").hide();	
								Ext.getCmp("gridConsultaData_2").hide();
								Ext.getCmp('gridConsultaTotal').hide();
								Ext.getCmp('gridConsultaTotalBis').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consultaData_22.load({
									params: {
										status:combo.getValue()
									}				
								});
								
							} else if(combo.getValue()=='21'){
								//Ext.getCmp("noHayDatos").hide();
								Ext.getCmp("gridConsultaDataComun").hide();
								Ext.getCmp("gridConsultaData_2").hide();
								Ext.getCmp("gridConsultaData_22").hide();
								Ext.getCmp('gridConsultaTotal').hide();
								Ext.getCmp('gridConsultaTotalBis').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consultaData_21.load({
									params: {
										status:combo.getValue()
									}				
								});
							}
						}
					}
				}
			}
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame: true,
		title: 'Criterios de b�squeda',
		width: 800,		
		border: false,				
		layout: 'form',		
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaultType: 'textfield',
		labelWidth: 150,
      defaults: {  msgTarget: 'side'
		},		
		items: elementosForma
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsultaData_2,
			gridConsultaData_22,
			gridConsultaData_21,
			gridConsultaDataComun,
			gridConsultaTotal,
			gridConsultaTotalBis
		]
	});

	catalogoEstatus.load();
});	
