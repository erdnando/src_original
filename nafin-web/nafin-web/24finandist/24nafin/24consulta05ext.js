Ext.onReady(function() {
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // A�adimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}

 
//------------------------------------------------------------------------------
//-----------------------------HANDLER's----------------------------------------
//------------------------------------------------------------------------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
		Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
				
			}	
			
			var jsonData = store.reader.jsonData;	
				
			if(store.getTotalCount() > 0) {//////////Verifica que existan registros
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				consultaTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'
					})
				});
				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
			
//------------------------------------------------------------------------------
//------------------------------STORE's------------------------------------------
//------------------------------------------------------------------------------

	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta05ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
			{	name: 'TIPO_MONEDA'},
			{	name: 'NO_DOCUMENTOS'},	
			{	name: 'MONTO_TOTAL'},				
			{	name: 'MONTO_TOTAL_INTERES'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta05ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'NUM_CREDITO'},
			{	name: 'EPO'},	
			{	name: 'DISTRIBUIDOR'},	
			{	name: 'INTERMEDIARIO'},	
			{	name: 'TIPO_CREDITO'},	
			{	name: 'RESPONSABLE_INTERES'},			
			{	name: 'TIPO_COBRANZA'},
			{  name: 'FECHA_OPERACION' },
			{	name: 'TIPO_MONEDA'},
			{	name: 'MONTO'},
			{	name: 'REF_TASA'},
			{	name: 'TASA_INTERES'},
			{	name: 'PLAZO'},
			{	name: 'FCH_VENCIMIENTO'},
			{	name: 'MONTO_INTERES'},
			{	name: 'TIPO_COBRO_INTE'},
			{	name: 'ESTATUS_SOLI'},
			{	name: 'NUM_PAGO'},
			{	name: 'FECHA_PAGO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var catalogoTipoInter = new Ext.data.JsonStore({
		id : 'catalogoTipoInter',
		root : 'registros',
		fields :['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
			baseParams :{
				informacion : 'catalogoTipoInter'
			},
		totalProperty : 'total',
		autoLoad : false,
		listeners : {
			exception : NE.util.mostrarDataProxyError
		}
	});
	var procesarCatalogoEstatus= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var catalogo = Ext.getCmp('cmbCatalogoTipoEstatus');
			if(catalogo.getValue()==''){
				catalogo.setValue(records[0].data['clave']);
			}
		}
	}
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
			baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEstatus,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
			baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
			baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo

		}
	});


	//Catalogo Tipo de Cr�dito (sus elementos son fijo)
	var catalogoTC = new Ext.data.JsonStore({
		id: 'catalogoTC',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta05ext.data.jsp',
			baseParams: {
			informacion: 'catalogoTC'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});

//------------------------------------------------------------------------------	
//----------------------------------COMPONENTES---------------------------------
//------------------------------------------------------------------------------
	var panelIzqCriterios= {
		xtype:             'fieldset',
		id:                'panelIzqCriterios',
		border:            false,
		columnWidth:       0.5,
		items: [{//Numero de documento final
			anchor:         '90%',
			xtype:          'numberfield',
			fieldLabel:     'N�mero de documento final',
			name:           'numCredito',
			id:             'txtNumCredito',
			msgTarget:      'side',
			allowBlank:     true,
			maxLength:      12,
			tabIndex:       1
		},{
		//catalogoTipoCredito
			anchor:         '90%',
			xtype:          'combo',
			name:           'catalogoTipoCredito',
			id:             'cmbCatalogoTipoCredito',
			fieldLabel:     'Tipo de cr�dito',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			hiddenName:     'catalogoTipoCredito',
			emptyText:      'Selecione tipo de dr�dito',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			tabIndex:       3,
			store:          catalogoTC,
			tpl:            NE.util.templateMensajeCargaCombo
		},{//catalogoTipoMoneda
			anchor:         '90%',
			xtype:          'combo',
			name:           'catalogoTipoMoneda',
			id:             'cmbCatalogoTipoMoneda',
			fieldLabel:     'Moneda',
			mode:           'local', 
			displayField:   'descripcion',
			valueField:     'clave',
			hiddenName:     'catalogoTipoMoneda',
			emptyText:      'Seleccione...',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			tabIndex:       5,
			store:          catalogoMoneda,
			tpl:            NE.util.templateMensajeCargaCombo
		},{//Monto
			xtype:               'compositefield',
			fieldLabel:          'Monto',
			msgTarget:           'side',
			combineErrors:       false,
			items: [{
				xtype:            'numberfield',
				name:             'montoMin',//'campoInicioValor',
				id:               'txtMontoMin',//'',campoInicioValor
				allowBlank:       true,
				maxLength:        9,
				width:            90,
				msgTarget:        'side',
				vtype:            'rangoValor',
				campoFinValor:    'txtMontoMax',
				tabIndex:         7,
				margins:          '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:            'displayfield',
				value:            'a',
				width:            22
			},{
				xtype:            'numberfield',
				name:             'montoMax',//'',campoFinValor
				id:               'txtMontoMax',//'',campoFinValor
				allowBlank:       true,
				maxLength:        9,
				width:            90,
				vtype:            'rangoValor',
				msgTarget:        'side',
				campoInicioValor: 'txtMontoMin',//'',campoInicioValor
				tabIndex:         8,
				margins:          '0 20 0 0' //necesario para mostrar el icono de error
			}]
		},{
			xtype:               'compositefield',
			fieldLabel:          'Fecha de vencimiento',
			combineErrors:       false,
			
			msgTarget:           'side',
			items: [{
				xtype:            'datefield',
				name:             'fechaVencimientoMin',
				id:               'txtFechaVencimientoMin',
				allowBlank:       true,
				startDay:         0,
				vtype:            'rangofecha',
				width:            90,
				msgTarget:        'side',
				campoFinFecha:    'txtFechaVencimientoMax',
				tabIndex:         11,
				margins:          '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:            'displayfield',
				value:            'al',
				width:            22//,margins:{top:0, right:10, bottom:0, left:-15}
			},{
				xtype:            'datefield',
				name:             'fechaVencimientoMax',
				id:               'txtFechaVencimientoMax',
				allowBlank:       true,
				startDay:         1,
				vtype:            'rangofecha',
				width:            90,
				msgTarget:        'side',
				campoInicioFecha: 'txtFechaVencimientoMin',
				tabIndex:         12,
				margins:          '0 20 0 0'  //necesario para mostrar el icono de error
			}]
		}]
	};

	var panelDerCriterios = {
		xtype:                  'fieldset',
		id:                     'panelDerCriterios',
		border:                 false,
		columnWidth:            0.5,
		items: [{//catalogoTipoCobroInter
			xtype:               'combo',
			name:                'catalogoTipoCobroInter',
			id:                  'cmbCatalogoTipoCobroInter',
			fieldLabel:          'Tipo de cobro de intereses',
			anchor:              '95%',
			mode:                'local',
			displayField:        'descripcion',
			valueField:          'clave',
			hiddenName:          'catalogoTipoCobroInter',
			emptyText:           'Selecione tipo cobro de interes',
			forceSelection:      true,
			triggerAction:       'all',
			typeAhead:           true,
			minChars:            1,
			store:               catalogoTipoInter,
			tabIndex:            2,
			tpl:                 NE.util.templateMensajeCargaCombo
		},{//catalogoIF
			xtype:               'combo',
			name:                'catalogoIF',
			id:                  'cmbCatalogoIF',
			fieldLabel:          'IF',
			mode:                'local',
			hiddenName:          'catalogoIF',
			emptyText:           'Seleccione ... ',
			forceSelection:      true,
			triggerAction:       'all',
			typeAhead:           true,
			minChars:            1,
			allowBlank:          false,
			store:               catalogoIF,
			displayField:        'descripcion',
			valueField:          'clave',
			msgTarget:           'side',
			anchor:              '95%',
			tabIndex:            4
		},{//catalogoTipoEstatus
			xtype:               'combo',
			name:                'catalogoTipoEstatus',
			id:                  'cmbCatalogoTipoEstatus',
			fieldLabel:          'Tipo de estatus',
			anchor:              '95%',
			mode:                'local', 
			displayField:        'descripcion',
			valueField:          'clave',
			hiddenName:          'catalogoTipoEstatus',
			forceSelection:      true,
			triggerAction:       'all',
			typeAhead:           true,
			minChars:            1,
			store:               catalogoEstatus,
			tabIndex:            6,
			tpl:                 NE.util.templateMensajeCargaCombo
		},{
			xtype:               'compositefield',
			fieldLabel:          'Fecha de pago',
			combineErrors:       false,
			msgTarget:           'side',
			items: [{
				xtype:            'datefield',
				name:             'fechaPagoMin',
				id:               'txtFechaPagoMin',
				allowBlank:       true,
				startDay:         0,
				vtype:            'rangofecha',
				width:            98,
				msgTarget:        'side',
				campoFinFecha:    'txtFechaPagoMax',
				tabIndex:         9,
				margins:          '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:            'displayfield',
				value:            'al',
				width:            22//,margins:{top:0, right:10, bottom:0, left:-10}
			},{
				xtype:            'datefield',
				name:             'fechaPagoMax',
				id:               'txtFechaPagoMax',
				allowBlank:       true,
				startDay:         1,
				vtype:            'rangofecha',
				width:            98,
				tabIndex:         10,
				msgTarget:        'side',
				campoInicioFecha: 'txtFechaPagoMin',
				margins:          '0 20 0 0'  //necesario para mostrar el icono de error
			}]
		}]
	};

	var elementosForma = [panelIzqCriterios, panelDerCriterios];

	var fp = new Ext.form.FormPanel({
		width:          800,
		labelWidth:     120,
		id:             'forma',
		title:          'Criterios de Busqueda',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		layout:         'column',
		frame:          true,
		collapsible:    true,
		titleCollapse:  false,
		items:[elementosForma],
		buttons: [
			{
				text:     'Buscar',
				id:       'Buscar',
				iconCls:  'icoBuscar',
				formBind: true,	
				tabIndex: 13,
				handler:  function(boton, evento){

					var catalogoIF = Ext.getCmp('cmbCatalogoIF');

					if(Ext.isEmpty(catalogoIF.getValue())){
						Ext.getCmp('cmbCatalogoIF').markInvalid('Seleccione un IF','side');
						return;
					}

					// El siguiente IF es solo para validar el formato de las fechas
					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}

					var montoMin = Ext.getCmp('txtMontoMin');
					var montoMax = Ext.getCmp('txtMontoMax');

					if(!Ext.isEmpty(montoMin.getValue()) || !Ext.isEmpty(montoMax.getValue())){
						if(Ext.isEmpty(montoMin.getValue())){
							montoMin.markInvalid('Debe capturar ambos Montos o dejarlos en blanco');
							montoMin.focus();
							return;
						}
						if(Ext.isEmpty(montoMax.getValue())){
							montoMax.markInvalid('Debe capturar ambos Montos o dejarlos en blanco');
							montoMax.focus();
							return;
						}
						
					}
					if(!Ext.isEmpty(montoMin.getValue()) && !Ext.isEmpty(montoMax.getValue()) ){
						
						if((montoMin.getValue())>(montoMax.getValue())){ 
							montoMax.markInvalid('El segundo monto debe de ser mayor');
							montoMax.focus();
							return;
						}
					}

					var fechaPagoMin = Ext.getCmp('txtFechaPagoMin');
					var fechaPagoMax = Ext.getCmp('txtFechaPagoMax');

					if(!Ext.isEmpty(fechaPagoMin.getValue()) || !Ext.isEmpty(fechaPagoMax.getValue())){
						if(Ext.isEmpty(fechaPagoMin.getValue())){
							fechaPagoMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaPagoMin.focus();
							return;
						}
						if(Ext.isEmpty(fechaPagoMax.getValue())){
							fechaPagoMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaPagoMax.focus();
							return;
						}
					}
					if(!Ext.isEmpty(fechaPagoMin.getValue()) && !Ext.isEmpty(fechaPagoMax.getValue())){

						if((fechaPagoMin.getValue()) > (fechaPagoMax.getValue())){
							fechaPagoMax.markInvalid('La segunda fecha debe de ser mayor');
							fechaPagoMax.focus();
							return;
						}
						//validaciondel formato
						var fechaPagoMin_ = Ext.util.Format.date(fechaPagoMin.getValue(),'d/m/Y');
						var fechaPagoMax_ = Ext.util.Format.date(fechaPagoMax.getValue(),'d/m/Y');

						if(!Ext.isEmpty(fechaPagoMin.getValue())){
							if(!isdate(fechaPagoMin_)) { 
								fechaPagoMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
								fechaPagoMin.focus();
								return;
							}

						}
						if( !Ext.isEmpty(fechaPagoMax.getValue())){
							if(!isdate(fechaPagoMax_)) { 
								fechaPagoMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
								fechaPagoMax.focus();
								return;
							}
							
						}
					
					}

					var fechaVencimientoMin = Ext.getCmp('txtFechaVencimientoMin');
					var fechaVencimientoMax = Ext.getCmp('txtFechaVencimientoMax');

					if(!Ext.isEmpty(fechaVencimientoMin.getValue()) || !Ext.isEmpty(fechaVencimientoMax.getValue())){
						if(Ext.isEmpty(fechaVencimientoMin.getValue())){
							fechaVencimientoMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaVencimientoMin.focus();
							return;
						}
						if(Ext.isEmpty(fechaVencimientoMax.getValue())){
							fechaVencimientoMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaVencimientoMax.focus();
							return;
						}
					}

					if(!Ext.isEmpty(fechaVencimientoMin.getValue()) && !Ext.isEmpty(fechaVencimientoMax.getValue())){

						if((fechaVencimientoMin.getValue()) > (fechaVencimientoMax.getValue())){
							fechaVencimientoMax.markInvalid('La segunda fecha debe de ser mayor');
							fechaVencimientoMax.focus();
							return;
						}
						//validaciondel formato
						var fechaVencimientoMin_ = Ext.util.Format.date(fechaVencimientoMin.getValue(),'d/m/Y');
						var fechaVencimientoMax_ = Ext.util.Format.date(fechaVencimientoMax.getValue(),'d/m/Y');
						
						if(!Ext.isEmpty(fechaVencimientoMin.getValue())){
							if(!isdate(fechaVencimientoMin_)) { 
								fechaVencimientoMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
								fechaVencimientoMin.focus();
								return;
							}
						}
						if( !Ext.isEmpty(fechaVencimientoMax.getValue())){
							if(!isdate(fechaVencimientoMax_)) { 
								fechaVencimientoMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
								fechaVencimientoMax.focus();
								return;
							}
						}
					}

					Ext.getCmp('gridConsulta').hide();	
					Ext.getCmp('gridTotales').hide();	
					fp.el.mask('Procesando...', 'x-mask-loading');
					consultaData.load({

						params: Ext.apply(fp.getForm().getValues(),{
							operacion : 'Generar',
							informacion : 'Consultar',
							start:0,
							limit:15
						})
					}); 
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				tabIndex: 14,
				handler: function() {
					window.location = '24consulta05ext.jsp';
				}
			}
		]
	});

	//grid de resultados de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({
		store:        consultaData,
		id:           'gridConsulta',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		title:        'Consulta',
		clicksToEdit: 1,
		hidden:       true,
		columns: [{
			header:    'Num. de documento final',
			tooltip:   'N�mero de documento final',
			dataIndex: 'NUM_CREDITO',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center'
		},{
			header:    'EPO',
			tooltip:   'EPO',
			dataIndex: 'EPO',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Dist.',
			tooltip:   'Dist.',
			dataIndex: 'DISTRIBUIDOR',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'IF',
			tooltip:   'IF',
			dataIndex: 'INTERMEDIARIO',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Tipo de cr�dito',
			tooltip:   'Tipo de cr�dito',
			dataIndex: 'TIPO_CREDITO',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Resp. pago de inter�s',
			tooltip:   'Resp. pago de inter�s',
			dataIndex: 'RESPONSABLE_INTERES',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Tipo de cobranza',
			tooltip:   'Tipo de cobranza',
			dataIndex: 'TIPO_COBRANZA',
			sortable:  true,
			width:     100,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Fecha de operaci�n',
			tooltip:   'Fecha de operaci�n',
			dataIndex: 'FECHA_OPERACION',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center'/*,
			renderer:  Ext.util.Format.dateRenderer('d/m/Y')*/
		},{
			header:    'Moneda',
			tooltip:   'Moneda',
			dataIndex: 'TIPO_MONEDA',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Monto',
			tooltip:   'Monto',
			dataIndex: 'MONTO',
			sortable:  true,
			width:     100,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="right">$'+ formato_numero(value, 2, '.', ',')+'</div>';
			}
		},{
			header:    'Referencia tasa',
			tooltip:   'Referencia tasa',
			dataIndex: 'REF_TASA',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Tasa de inter�s',
			tooltip:   'Tasa de inter�s',
			dataIndex: 'TASA_INTERES',
			sortable:  true,
			width:     100,
			resizable: true,
			align:     'center',
			renderer:  Ext.util.Format.numberRenderer('0.00%')
		},{
			header:    'Plazo',
			tooltip:   'Plazo',
			dataIndex: 'PLAZO',
			sortable:  true,
			width:     50,
			resizable: true,
			align:     'center'
		},{
			header:    'Fecha de vencimiento',
			tooltip:   'Fecha de vencimiento',
			dataIndex: 'FCH_VENCIMIENTO',
			sortable:  true,
			width:     120,
			resizable: true,
			align:     'center'/*,
			renderer:  Ext.util.Format.dateRenderer('d/m/Y')*/
		},{
			header:    'Monto inter�s',
			tooltip:   'Monto inter�s',
			dataIndex: 'MONTO_INTERES',
			sortable:  true,
			width:     80,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="right">$'+ formato_numero(value, 2, '.', ',')+'</div>';
			}
		},{
			header:    'Tipo de cobro de inter�s',
			tooltip:   'Tipo de cobro de inter�s',
			dataIndex: 'TIPO_COBRO_INTE',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			renderer:  function(value){
				return '<div align="left">'+ value+'</div>';
			}
		},{
			header:    'Estatus',
			tooltip:   'Estatus',
			dataIndex: 'ESTATUS_SOLI',
			sortable:  true,
			width:     60,
			resizable: true,
			align:     'center'
		},{
			header:    'Num. pago',
			tooltip:   'Num pago',
			dataIndex: 'NUM_PAGO',
			sortable:  true,
			width:     70,
			height :   50,
			resizable: true,
			align:     'center'
		},{
			header:    'Fecha de cambio de estatus',
			tooltip:   'Fecha de cambio de estatus',
			dataIndex: 'FECHA_PAGO',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center'/*,
			renderer:  Ext.util.Format.dateRenderer('d/m/Y')*/
		}],
		displayInfo:  true,
		emptyMsg:     'No hay registros.',
		loadMask:     true,
		stripeRows:   true,
		height:       400,
		width:        800,
		align:        'center',
		frame:        false,
		bbar: {
			xtype:         'paging',
			pageSize:      15,
			buttonAlign:   'left',
			id:            'barraPaginacion',
			displayInfo:   true,
			store:         consultaData,
			displayMsg:    '{0} - {1} de {2}',
			emptyMsg:      'No hay registros.',
			items: ['->','-',
				{
					xtype:   'button',
					text:    'Generar Archivo',
					tooltip: 'Generar Archivo',
					iconCls: 'icoXls',
					id:      'btnArchivoCSV',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'
							}),	
							callback: procesarDescargaArchivos
						});
					}
				},{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime todos los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnArchivoPDF',
					handler: function(boton, evento){
						boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: 0,
								limit: 0
							}),
							callback: procesarDescargaArchivos
						});
					}
				}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 25/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
							Ext.Ajax.request({
							url: '24consulta05ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize
							}),
							callback: procesarDescargaArchivos
						});
					}
				}
*/
			]
		}
	});

	//grid de Totales
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales ',	
		align: 'center',
		hidden: true,//true,
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Tipo de Moneda ',
				dataIndex: 'TIPO_MONEDA',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer:function(value){
							return '<div align="left">'+ value+'</div>';
				 }
			},
			{
				header: 'N�mero de documentos ',
				tooltip: 'N�mero de documentos ',
				dataIndex: 'NO_DOCUMENTOS',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center'	
			},				
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer: function (value){
					Ext.util.Format.numberRenderer('$0,0.00');
					return '<div align="right">$'+formato_numero(value, '2', '.', ',')+'</div>';	
				}
			},
			{
				header: 'Monto inter�s',
				tooltip: 'Monto inter�s',
				dataIndex: 'MONTO_TOTAL_INTERES',
				sortable: true,
				width: 220,			
				resizable: true,				
				align: 'center',
				renderer:function (value){
					Ext.util.Format.numberRenderer('$0,0.00');
					return	 '<div align="right">$'+formato_numero(value, '2', '.', ',')+'</div>';	
				}	
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 800,
		frame: true	
	});
	
//------------------------------------------------------------------------------	
//-----------------------------CONTENEDOR_PRINCIPAL-----------------------------
//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 900,		
		height: 'auto',
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});

	catalogoIF.load();
	catalogoMoneda.load();
	catalogoEstatus.load();
	catalogoTipoInter.load();
	catalogoTC.load();
	  
});	
