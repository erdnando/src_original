<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String tipo_credito = request.getParameter("tipo_credito") == null?"1":(String)request.getParameter("tipo_credito");
String tipocred = request.getParameter("tipocred") == null?"":(String)request.getParameter("tipocred");
String respago = request.getParameter("respago") == null?"":(String)request.getParameter("respago");
String tipocob = request.getParameter("tipocob") == null?"":(String)request.getParameter("tipocob");
String pgointeres = request.getParameter("pgointeres") == null?"":(String)request.getParameter("pgointeres");
String txdiasmin = request.getParameter("txdiasmin") == null?"":(String)request.getParameter("txdiasmin");
String txplazomax = request.getParameter("txplazomax") == null?"":(String)request.getParameter("txplazomax");
String txtDiasFechaVenc = request.getParameter("txtDiasFechaVenc") == null?"":(String)request.getParameter("txtDiasFechaVenc");
String rconversion = request.getParameter("rconversion") == null?"":(String)request.getParameter("rconversion");
String rcontrol = request.getParameter("rcontrol") == null?"":(String)request.getParameter("rcontrol");
String txtplazominlc = request.getParameter("txtplazominlc") == null?"":(String)request.getParameter("txtplazominlc");
String txtproxvenc = request.getParameter("txtproxvenc") == null?"":(String)request.getParameter("txtproxvenc");
String txtEnvCorreo = request.getParameter("txtEnvCorreo") == null?"":(String)request.getParameter("txtEnvCorreo");
if(txtDiasFechaVenc.equals(""))  { txtDiasFechaVenc = "0"; } 
if(txtplazominlc.equals(""))     { txtplazominlc  = "0"; } 
if(txtproxvenc.equals(""))       { txtproxvenc  = "0"; } 

if(tipocred.equals("C"))       {    respago="";  tipocob ="";    }

String infoRegresar ="",   mensaje ="";

JSONObject jsonObj = new JSONObject();
Vector vecParametros = new Vector(); 	

ParametrosDist  BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	
if (informacion.equals("valoresIniciales") ) {

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("tipo_credito", tipo_credito);	

	if (tipo_credito.equals("1") ) {
	
		vecParametros = BeanParametros.getParamGral();
		tipocred = (vecParametros.get(0).toString().trim()==null)?"":vecParametros.get(0).toString().trim();
		respago = (vecParametros.get(1).toString().trim()==null)?"":vecParametros.get(1).toString().trim();
		tipocob = (vecParametros.get(2).toString().trim()==null)?"":vecParametros.get(2).toString().trim();
		pgointeres = (vecParametros.get(3).toString().trim()==null)?"":vecParametros.get(3).toString().trim();		
			
		jsonObj.put("tipocred", tipocred);
		jsonObj.put("respago", respago);
		jsonObj.put("tipocob", tipocob);
		jsonObj.put("pgointeres", pgointeres);

	}else  if (tipo_credito.equals("2") ) {
	
		vecParametros = BeanParametros.getParamGrala();	
		
		txdiasmin 		= (vecParametros.get(0).toString().trim()==null)?"":vecParametros.get(0).toString().trim();
		txplazomax		= (vecParametros.get(1).toString().trim()==null)?"":vecParametros.get(1).toString().trim();
		rconversion 	= (vecParametros.get(2).toString().trim()==null)?"":vecParametros.get(2).toString().trim();
		rcontrol		= (vecParametros.get(3).toString().trim()==null)?"":vecParametros.get(3).toString().trim();
		txtplazominlc	= (vecParametros.get(4).toString().trim()==null)?"":vecParametros.get(4).toString().trim();
		txtproxvenc 	= (vecParametros.get(5).toString().trim()==null)?"":vecParametros.get(5).toString().trim();
		txtDiasFechaVenc = (vecParametros.get(6).toString().trim()==null)?"":vecParametros.get(6).toString().trim();
		txtEnvCorreo = (vecParametros.get(7).toString().trim()==null)?"":vecParametros.get(7).toString().trim();
				
		jsonObj.put("txdiasmin", txdiasmin);
		jsonObj.put("txplazomax", txplazomax);
		jsonObj.put("rconversion", rconversion);
		jsonObj.put("rcontrol", rcontrol);
		jsonObj.put("txtplazominlc", txtplazominlc);
		jsonObj.put("txtproxvenc", txtproxvenc);
		jsonObj.put("txtDiasFechaVenc", txtDiasFechaVenc);
		jsonObj.put("txtEnvCorreo", txtEnvCorreo);			
	}

	infoRegresar = jsonObj.toString();	

}else  if (informacion.equals("Guardar_Datos") ) {

	int  actualiza = 0;
	if (tipo_credito.equals("1") ) {
	
		actualiza = BeanParametros.vmodificaParametros(tipocred,respago,tipocob,Integer.parseInt(pgointeres) );
		if (actualiza==1){
			mensaje = "Los Parámetros se actualizaron satisfactoriamente";		
		}	
		
	}else  if (tipo_credito.equals("2") ) {
	
		actualiza = BeanParametros.vmodificaParamGrales(Integer.parseInt(txdiasmin), Integer.parseInt(txplazomax),rconversion,rcontrol,txtplazominlc, txtproxvenc, txtDiasFechaVenc,txtEnvCorreo);
		if (actualiza==1){
			mensaje = "Los Parámetros se actualizaron satisfactoriamente";		
		}	
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("tipo_credito", tipo_credito);
	jsonObj.put("mensaje", mensaje);
	jsonObj.put("Guardar_Datos", "S");
	infoRegresar = jsonObj.toString();	
	
}

%>

<%=infoRegresar%>