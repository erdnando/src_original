<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/24finandist/24secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<%
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String operacion = request.getParameter("operacion") == null?"":(String)request.getParameter("operacion");
	String claveEpo = request.getParameter("epo") == null?"":(String)request.getParameter("epo");
	String clavePyme = request.getParameter("ic_pyme") == null?"":(String)request.getParameter("ic_pyme");
	String tipoCredito = request.getParameter("tipo_credito") == null?"":(String)request.getParameter("tipo_credito");
	String modePlazo = request.getParameter("modalidadPlazo") == null?"":(String)request.getParameter("modalidadPlazo");
	String icCambioEstatus = request.getParameter("tipoEstatus") == null?"":(String)request.getParameter("tipoEstatus");
	String dcFechaCambioDe = request.getParameter("fecha_seleccion_de") == null?"":(String)request.getParameter("fecha_seleccion_de");
	String dcFechaCambioA = request.getParameter("fecha_seleccion_a") == null?"":(String)request.getParameter("fecha_seleccion_a");
	String claveIf = request.getParameter("if") == null?"":(String)request.getParameter("if");
	String numeroDocto = request.getParameter("documento") == null?"":(String)request.getParameter("documento");
	String fechaVtoDe = request.getParameter("fecha_vto_de") == null?"":(String)request.getParameter("fecha_vto_de");
	String fechaVtoA = request.getParameter("fecha_vto_a") == null?"":(String)request.getParameter("fecha_vto_a");
	String fechaPublicacionDe = request.getParameter("fecha_publicacion_de") == null?"":(String)request.getParameter("fecha_publicacion_de");
	String fechaPublicacionA = request.getParameter("fecha_publicacion_a") == null?"":(String)request.getParameter("fecha_publicacion_a");
	String moneda = request.getParameter("ic_moneda") == null?"":(String)request.getParameter("ic_moneda");
	String montoDe = request.getParameter("fn_monto_de") == null?"":(String)request.getParameter("fn_monto_de");
	String montoA = request.getParameter("fn_monto_a") == null?"":(String)request.getParameter("fn_monto_a");
	String montoDescuento = request.getParameter("monto_con_descuento") == null?"":(String)request.getParameter("monto_con_descuento");
	String icDocumento = request.getParameter("ic_documento") == null?"":(String)request.getParameter("ic_documento");
	String tipo_Consulta = request.getParameter("tipoConsulta") == null?"":(String)request.getParameter("tipoConsulta");
	String infoRegresar ="", consulta="", usuario="";
	
	if(informacion.equals("GenerarArchivoCSV") || informacion.equals("PDF_Todo"))  { tipo_Consulta = "1";   }
	ConsNoOperNafinDist paginador = new ConsNoOperNafinDist();
	paginador.setClaveEpo(claveEpo);
	paginador.setClaveIf(claveIf);
	paginador.setClavePyme(clavePyme);
	paginador.setTipoCredito(tipoCredito);
	paginador.setModePlazo(modePlazo);
	paginador.setIcCambioEstatus(icCambioEstatus);
	paginador.setDcFechaCambioDe(dcFechaCambioDe);
	paginador.setDcFechaCambioA(dcFechaCambioA);
	paginador.setNumeroDocto(numeroDocto);
	paginador.setFechaVtoDe(fechaVtoDe);
	paginador.setFechaVtoA(fechaVtoA);
	paginador.setFechaPublicacionDe(fechaPublicacionDe);
	paginador.setFechaPublicacionA(fechaPublicacionA);
	paginador.setMoneda(moneda);
	paginador.setMontoDe(montoDe);
	paginador.setMontoA(montoA);
	paginador.setIc_documento(icDocumento);
	paginador.setTipoConsulta(tipo_Consulta);
	paginador.setMontoDescuento(montoDescuento);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	JSONObject jsonObj = new JSONObject();

	if (informacion.equals("catEPOData")){

		CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}else if (informacion.equals("catPymeData")){

		CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
		cat.setCampoClave("cp.ic_pyme");
		cat.setCampoDescripcion("cp.cg_razon_social");   
		cat.setClaveEpo(claveEpo);	
		cat.setOrden("cp.cg_razon_social");  	
		infoRegresar = cat.getJSONElementos();
	}else  if (informacion.equals("catIFData")){
		
		CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");   
		cat.setClaveEpo(claveEpo);	
		cat.setOrden("cg_razon_social");  	
		infoRegresar = cat.getJSONElementos();
	}else  if (informacion.equals("catEstatusData")){
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_cambio_estatus");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_cambio_estatus");		
		catalogo.setOrden("1");
		catalogo.setValoresCondicionIn("23,34,4,2,24", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	}else  if (informacion.equals("catCobroInteresData")){
		
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_cobro_interes");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_tipo_cobro_interes");		
		catalogo.setOrden("ic_tipo_cobro_interes");
		catalogo.setValoresCondicionIn("1, 2", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	}else  if (informacion.equals("catMonedaData")){
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");		
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
	}else  if (informacion.equals("catModalidadData")){
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_financiamiento");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_tipo_financiamiento");		
		catalogo.setOrden("ic_tipo_financiamiento");	
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("Consultar_1")  || informacion.equals("consTotales_1")  ||  informacion.equals("GenerarArchivoCSV") ||  informacion.equals("PDF_Todo") || informacion.equals("PDF") || informacion.equals("consDetalles") ){

		int start = 0;
		int limit = 0;
		if (informacion.equals("Consultar_1")){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		JSONObject resultado = new JSONObject();
		try {
			if (operacion.equals("Generar")) {
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			Registros registros = queryHelper.getPageResultSet(request,start,limit);
			String consulta2 = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta2);
			infoRegresar = resultado.toString();
		} catch(Exception e){
			throw new AppException("Error en la paginacion", e);
		}
	}
	else if(informacion.equals("consTotales_1") ) {
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar = queryHelper.getJSONResultCount(request);	
	} else if(informacion.equals("GenerarArchivoCSV") || informacion.equals("PDF_Todo")) {
		try{
				String tipo = "CSV";
				if(informacion.equals("PDF_Todo")){
					tipo = "PDF";
				}
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipo);
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				jsonObj.put("success", new Boolean(false));
				throw new AppException("Error al generar el archivo ", e);
			}
	} else if(informacion.equals("consDetalles")) {
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("tipo_credito", tipoCredito);
			infoRegresar = jsonObj.toString();	
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}	
	}
/*
	Fodea 011-2015
	La opción de generar el PDF por paginación queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el método por si se requiere nuevamente 
	Fecha: 24/06/2015
	BY: Agustín Bautista Ruiz
*/
/*
	else if(informacion.equals("PDF")) {
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
				JSONObject jsonObj3 = new JSONObject();
				jsonObj3.put("success", new Boolean(true));
				jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj3.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
	}*/
}
%>
<%=infoRegresar%>