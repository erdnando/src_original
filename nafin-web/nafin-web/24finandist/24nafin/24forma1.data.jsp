<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String df_fecha_operacion_de = request.getParameter("df_fecha_operacion_de") == null?"":(String)request.getParameter("df_fecha_operacion_de");
String df_fecha_operacion_a = request.getParameter("df_fecha_operacion_a") == null?"":(String)request.getParameter("df_fecha_operacion_a");
String tipoCredito = request.getParameter("tipoCredito") == null?"":(String)request.getParameter("tipoCredito");
String ic_tipo_cobro_int = request.getParameter("ic_tipo_cobro_int") == null?"":(String)request.getParameter("ic_tipo_cobro_int");
String ic_documento = request.getParameter("ic_documento") == null?"":(String)request.getParameter("ic_documento");
String ig_numero_docto = request.getParameter("ig_numero_docto") == null?"":(String)request.getParameter("ig_numero_docto");
String ic_moneda = request.getParameter("ic_moneda") == null?"":(String)request.getParameter("ic_moneda");
String ic_estatus_solic = request.getParameter("ic_estatus_solic") == null?"1":(String)request.getParameter("ic_estatus_solic");
String inDocumentos = request.getParameter("inDocumentos") == null?"":(String)request.getParameter("inDocumentos");
String pantalla = request.getParameter("pantalla") == null?"N":(String)request.getParameter("pantalla");
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());	

String infoRegresar ="", consulta ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
	
	
//totales 
	int 	totalDoctosMN 		= 0;
	int		totalDoctosUSD		= 0;
	double	totalMontoMN 		= 0;
	double	totalMontoUSD		= 0;
	double	totalMontoValuadoMN	= 0;
	double	totalMontoValuadoUSD= 0;
	double	totalMontoDescMN	= 0;
	double	totalMontoDescUSD	= 0;
	double	totalMontoTasaIntMN = 0;
	double	totalMontoTasaIntUSD= 0;
	
	

AutorizacionSolic  autSolic = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class); 

Cons_AutoSolic paginador = new Cons_AutoSolic (); 

if (informacion.equals("valoresIniciales") ) {

jsonObj.put("success", new Boolean(true));	
jsonObj.put("fechaHoy",fechaHoy);	
infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("catIFData") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	

} else  if (informacion.equals("catMonedaData") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("cd_nombre");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("catCobroInteresData") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_cobro_interes");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_tipo_cobro_interes");		
	catalogo.setOrden("cd_descripcion");
	catalogo.setValoresCondicionIn("1,2", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


} else  if (informacion.equals("Consultar")  ||   informacion.equals("ConsTotales")    ) {

	paginador.setIc_if(ic_if);
	paginador.setDf_fecha_operacion_de(df_fecha_operacion_de);
	paginador.setDf_fecha_operacion_a(df_fecha_operacion_a);
	paginador.setTipoCredito(tipoCredito);
	paginador.setIc_tipo_cobro_int(ic_tipo_cobro_int);
	paginador.setIc_documento(ic_documento);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_estatus_solic(ic_estatus_solic);
	paginador.setInDocumentos(inDocumentos);
	paginador.setPantalla("Consulta");
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try {
	
		Registros reg	=	queryHelper.doSearch();
		
		while(reg.next()){
		
			String tipoCambio = (reg.getString("TIPO_CAMBIO") == null) ? "" : reg.getString("TIPO_CAMBIO");	
			String monedaLinea   = (reg.getString("MONEDA_LINEA") == null) ? "" : reg.getString("MONEDA_LINEA");	 
			String icMoneda   = (reg.getString("IC_MONEDA") == null) ? "" : reg.getString("IC_MONEDA");	
			String  tipoConversion  = (reg.getString("TIPO_CONV") == null) ? "" : reg.getString("TIPO_CONV");	
			String  montoValuado  = (reg.getString("MONTO_VALUADO") == null) ? "" : reg.getString("MONTO_VALUADO");	
			String  plazoDescuento  = (reg.getString("PLAZO_DESCUENTO") == null) ? "" : reg.getString("PLAZO_DESCUENTO");
			String  porcDescuento  = (reg.getString("PORCENTAJE_DESCUENTO") == null) ? "" : reg.getString("PORCENTAJE_DESCUENTO");				
			montoValuado = (icMoneda.equals(monedaLinea))?"0":montoValuado;				
			String monto   = (reg.getString("MONTO") == null) ? "0" : reg.getString("MONTO");	
			String montoDescuento   = (reg.getString("MONTO_DESCONTAR") == null) ? "0" : reg.getString("MONTO_DESCONTAR");
			String montoTasaInteres   = (reg.getString("MONTO_INTERES") == null) ? "0" : reg.getString("MONTO_INTERES");
				
			if (informacion.equals("Consultar") )  {
				reg.setObject("TIPO_CONV",(icMoneda.equals(monedaLinea))?"":tipoConversion);	
				reg.setObject("TIPO_CAMBIO",(icMoneda.equals(monedaLinea))?"":(tipoCambio.equals("1"))?"":""+tipoCambio);
				reg.setObject("MONTO_VALUADO",(icMoneda.equals(monedaLinea))?"&nbsp;":"$&nbsp;"+montoValuado);
				reg.setObject("PLAZO_DESCUENTO",(plazoDescuento.equals("0"))?"":plazoDescuento);
				reg.setObject("PORCENTAJE_DESCUENTO",(porcDescuento.equals("0"))?"":porcDescuento);
			}
			
			if (informacion.equals("ConsTotales") )  {
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN 		+= Double.parseDouble(monto);
					totalMontoValuadoMN	+= Double.parseDouble(montoValuado);
					totalMontoDescMN	+= Double.parseDouble(montoDescuento);
					totalMontoTasaIntMN += Double.parseDouble(montoTasaInteres);
				}else if("54".equals(icMoneda)){
					totalDoctosUSD ++;
					totalMontoUSD		+= Double.parseDouble(monto);
					if(!icMoneda.equals(monedaLinea))
						totalMontoValuadoUSD+= Double.parseDouble(montoValuado);
					totalMontoDescUSD	+= Double.parseDouble(montoDescuento);
					totalMontoTasaIntUSD+= Double.parseDouble(montoTasaInteres); 
				}	
			}				
				
				
		}	//while(reg.next()){
		
		if (informacion.equals("Consultar") )  {
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);			
		}
		
		if (informacion.equals("ConsTotales") )  {
		
			if(totalDoctosMN>0){
				datos = new HashMap();
				datos.put("MONEDA", "MONEDA NACIONAL");
				datos.put("TOTAL", String.valueOf(totalDoctosMN));
				datos.put("MONTO",  Double.toString (totalMontoMN));			
				datos.put("MONTO_DESCONTAR",  Double.toString (totalMontoDescMN) );			
				datos.put("MONTO_VALUADO",  Double.toString (totalMontoValuadoMN));			
				datos.put("MONTO_INTERES",  Double.toString (totalMontoTasaIntMN));		
				registros.add(datos);
			}
			if(totalDoctosUSD>0){
				datos = new HashMap();
				datos.put("MONEDA", "DOLAR AMERICANO");
				datos.put("TOTAL", String.valueOf(totalDoctosUSD));
				datos.put("MONTO",  Double.toString (totalMontoUSD));			
				datos.put("MONTO_DESCONTAR",  Double.toString (totalMontoDescUSD));			
				datos.put("MONTO_VALUADO",  Double.toString (totalMontoValuadoUSD));			
				datos.put("MONTO_INTERES",  Double.toString (totalMontoTasaIntUSD));		
				registros.add(datos);
			}
			
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
		}
		
		
	}	catch(Exception e) {
		throw new AppException("Error en la consulta y totales  ", e);
	}
	
	jsonObj.put("pantalla", pantalla);
	infoRegresar = jsonObj.toString();  
	
} else  if (informacion.equals("ConsDetalle")    ) {

	paginador.setIc_documento(ic_documento);
	paginador.setPantalla("Detalle");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try {
	
		Registros reg	=	queryHelper.doSearch();
		
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);	
		
	}	catch(Exception e) {
		throw new AppException("Error en la ConsDetalle ", e);
	}
	infoRegresar = jsonObj.toString();  
	

}else if (informacion.equals("Generar_Reporte_SIRAC") )  {

	try{
	
		autSolic.generaReporteSirac(inDocumentos);
		
		jsonObj.put("success", new Boolean(true));
		
	}	catch(Exception e) {
		throw new AppException("Error la generación del reporte Sirac  ", e);
	}

	jsonObj.put("inDocumentos", inDocumentos);
	infoRegresar = jsonObj.toString(); 


}else if (informacion.equals("generar_Archivo_SIRAC") )  {

	try {
		
		Vector  vecFilas = autSolic.getDatosArchivoSirac(inDocumentos);
		String nombreArchivo = paginador.generar_Archivo_SIRAC(vecFilas, strDirectorioTemp) ;
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}
	infoRegresar = jsonObj.toString(); 	
	
	
	
}else if (informacion.equals("Imprimir_Reporte_SIRAC") )  {

	try {
		
		Vector  vecFilas = autSolic.getDatosArchivoSirac(inDocumentos);
		String nombreArchivo = paginador.imprimir_Reporte_SIRAC(request, vecFilas, strDirectorioTemp) ;
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	infoRegresar = jsonObj.toString(); 	
	

}





%>

<%=infoRegresar%>