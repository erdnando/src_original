Ext.onReady(function() {

	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);

	//----------------------Descargar Archivos--------------------------
	function descargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// Comprueba si es Grid por esquema es visible o no
	function verificaGridPorEsquema(){
		var esVisible;
		if (gridConsultaPorEsquema.isVisible()) {
		 esVisible='ok'	
		}else{
		 esVisible='ko'	
		}	
	return esVisible;
	}
	
	/************************************************************
	*				C�digo para el combo Tipo de cr�dito				*
	*************************************************************/	
  	var procesarCatalogoTipoCredito = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var tipo_credito = Ext.getCmp('tipo_credito1');
				Ext.getCmp('tipo_credito1').setValue("");
				if(tipo_credito.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					tipo_credito.setValue("");			
				}	
			} 
		}
  };
	//-------------------- Se crea el store para el combo Tipo de Credito -------  
	var catalogoTipoCredito = new Ext.data.JsonStore({
		id: 'catalogoTipoCredito',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoCredito'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
		load: procesarCatalogoTipoCredito,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	 		
	/************************************************************
	*				C�digo para el combo IF              				*
	*************************************************************/		
	var procesarCatalogoIF = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ic_if = Ext.getCmp('ic_if1');
				Ext.getCmp('ic_if1').setValue("");
				if(ic_if.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					ic_if.setValue("");			
				}	
			} 
		}
  };
	
	//--------------- Se crea el store para el combo box "IF"----------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoIF,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});	
			
	/************************************************************
	 *				C�digo para el combo EPO             				*
	 *************************************************************/	
	var procesarCatalogoEPO = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ic_epo = Ext.getCmp('ic_epo1');
				Ext.getCmp('ic_epo1').setValue("");
				if(ic_epo.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					ic_epo.setValue("");			
				}	
			} 
		}
  };
 
	//--------------- Se crea el store para el combo box EPO----------------
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	/************************************************************
	 *				C�digo para el combo Distribuidor (ic_pyme)		*
	 ************************************************************/	  
	var procesarCatalogoDistribuidor = function(store, records, response){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var ic_pyme = Ext.getCmp('ic_pyme1');
				Ext.getCmp('ic_pyme1').setValue("");
				if(ic_pyme.getValue()==""){
					var newRecord = new recordType({
						clave:"",
						descripcion:"Seleccione..."
					});							
					store.insert(0,newRecord);
					store.commitChanges();
					ic_pyme.setValue("");			
				}	
			}
		}
  };
	
	//--------------- Se crea el store para el combo box Distribuidor------------
	var catalogoDistribuidor = new Ext.data.JsonStore({
		id: 'catalogoDistribuidor',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoDistribuidor'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {	
			load: procesarCatalogoDistribuidor,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});	
	
	/************************************************
	*		Empieza el c�digo para el GRID General		*
	*************************************************/
	var procesarConsultaGeneral = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaGeneral = Ext.getCmp('gridConsultaGeneral');	
		var el = gridConsultaGeneral.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsultaGeneral.isVisible()) {
				gridConsultaGeneral.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaGeneral.getColumnModel();
						
			if(store.getTotalCount() < 1) {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	//-------------------- Se crea el store para el Grid General-----------------
	var consultaGeneral = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'consultaGeneral'
		},		
		fields: [
			{	name: 'MONEDA'},
			{	name: 'TIPO_TASA'},
			{	name: 'CVE_TASA'},
			{	name: 'PLAZO'},
			{	name: 'VALOR'},
			{	name: 'REL_MAT'},
			{	name: 'PUNTOS'},
			{	name: 'TASA_PISO'},			
			{	name: 'FECHA'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaGeneral,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaGeneral(null, null, null);					
				}
			}
		}	
	});	
		
   //-------------------- Se crea el Grid General--------------------
	var gridConsultaGeneral = new Ext.grid.EditorGridPanel({	
		store: consultaGeneral,
		id: 'gridConsultaGeneral',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',		
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,			
				resizable: true,	
				width: 120,	
				align: 'center'				
			},{
				header: 'Tipo tasa',
				tooltip: 'Tipo tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,		
				resizable: true,	
				width: 130,
				align: 'center'				
			},{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,	
				resizable: true,	
				width: 85,
				align: 'center'
			},{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,			
				resizable: true,
				width: 75,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')				
			},{
				header: 'Rel. mat.',
				tooltip: 'Rel. mat.',
				dataIndex: 'REL_MAT',
				sortable: true,		
				resizable: true,
				width: 75,	
				align: 'center'
			},{
				header: 'Puntos adicionales',
				tooltip: 'Puntos adicionales',
				dataIndex: 'PUNTOS',
				sortable: true,	
				resizable: true,	
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},{
				header: 'Tasa a aplicar',
				tooltip: 'Tasa a aplicar',
				dataIndex: 'TASA_PISO',
				sortable: true,			
				resizable: true,
				width: 90,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')				
			},{
				header: 'Fecha �ltima actualizaci�n',
				tooltip: 'Fecha �ltima actualizaci�n',
				dataIndex: 'FECHA',
				sortable: true,		
				resizable: true,	
				width: 120,
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: 'Tasas fijadas por Nafin en el esquema general', //titulo,
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		autoHeight: true,
		collapsible: true,
		width: 800,
		align: 'center',
		frame: false
	});	
	
	/******************************************************
	 *		Empieza el c�digo para el GRID por Esquema		*
	 ******************************************************/
	var procesarConsultaPorEsquema = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsultaPorEsquema = Ext.getCmp('gridConsultaPorEsquema');	
		var el = gridConsultaPorEsquema.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsultaPorEsquema.isVisible()) {
				gridConsultaPorEsquema.show();
			}	
			var jsonData = store.reader.jsonData;	
			var cm = gridConsultaPorEsquema.getColumnModel();	
			if(store.getTotalCount() < 1) {	
				Ext.getCmp('noHayDatos').show();
				Ext.getCmp('gridConsultaPorEsquema').hide();		
			} else{
				Ext.getCmp('noHayDatos').hide();
				Ext.getCmp('gridConsultaPorEsquema').show();
				Ext.getCmp('titulo_grid1').setValue(jsonData.encabezado);
				Ext.getCmp('gridConsultaPorEsquema').setTitle(jsonData.encabezado);
			}
			Ext.getCmp('opcion_consulta1').setValue(jsonData.opcion);
		}
	}
	//-------------------- Se crea el store para el Grid por Esquema-------------
	var consultaPorEsquema = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta03ext.data.jsp',
		baseParams: {
			informacion: 'consultaPorEsquema'
		},		
		fields: [
			{	name: 'MONEDA'},
			{	name: 'TIPO_TASA'},
			{	name: 'CVE_TASA'},
			{	name: 'PLAZO'},
			{	name: 'VALOR'},
			{	name: 'REL_MAT'},
			{	name: 'PUNTOS'},
			{	name: 'TASA_PISO'},			
			{	name: 'FECHA'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		listeners: {
			load: procesarConsultaPorEsquema,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaPorEsquema(null, null, null);					
				}
			}
		}	
	});	

   //-------------------- Se crea el Grid Por esquema--------------------
	var gridConsultaPorEsquema = new Ext.grid.EditorGridPanel({	
		store: consultaPorEsquema,
		id: 'gridConsultaPorEsquema',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',	
		hidden: true,
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,			
				resizable: true,	
				width: 120,	
				align: 'center'				
			},{
				header: 'Tipo tasa',
				tooltip: 'Tipo tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,		
				resizable: true,	
				width: 130,
				align: 'center'				
			},{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,	
				resizable: true,	
				width: 85,
				align: 'center'
			},{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,			
				resizable: true,
				width: 73,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')				
			},{
				header: 'Rel. mat.',
				tooltip: 'Rel. mat.',
				dataIndex: 'REL_MAT',
				sortable: true,		
				resizable: true,
				width: 75,	
				align: 'center'				
			},{
				header: 'Puntos adicionales',
				tooltip: 'Puntos adicionales',
				dataIndex: 'PUNTOS',
				sortable: true,	
				resizable: true,	
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},{
				header: 'Tasa a aplicar',
				tooltip: 'Tasa a aplicar',
				dataIndex: 'TASA_PISO',
				sortable: true,			
				resizable: true,
				width: 90,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')				
			},{
				header: 'Fecha �ltima actualizaci�n',
				tooltip: 'Fecha �ltima actualizaci�n',
				dataIndex: 'FECHA',
				sortable: true,		
				resizable: true,	
				width: 120,
				align: 'center'				
			}			
		],			
		displayInfo: true,	
		title: ' ', 
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		//height: 180,
		autoHeight: true,
		width: 800,
		align: 'center',
		frame: false
	});	
	
	/*********************************************
	 *		Se crean los elementos de la forma		*
	 *********************************************/	
	var elementosForma = [
		{			
			xtype:'panel',	 
			id: 'tutulo',  
			layout:'table',	
			width:800,	
			layoutConfig:{columns: 1}, 
			border: false,
			defaults:{ align:'center', bodyStyle:'padding:2px,' },
			items: [ 
				{	
					width: 800,
					frame: true,	
					border:false,	
					html:'<div align="left"><b>Si desea consultar tasas por esquema de financiamiento, seleccione:</b	><br>&nbsp;</div>'	
				}
			]
		},		
		{
			xtype: 'combo',	
			name: 'tipo_credito',	
			id: 'tipo_credito1',	
			fieldLabel: '&nbsp;&nbsp;Tipo de cr�dito', 
			mode: 'local',	
			hiddenName : 'tipo_credito',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogoTipoCredito,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,			
			listeners: {
				select: {
					fn: function(combo) {
					Ext.getCmp("ic_epo1").hide();
						if(combo.getValue()!=''){
							Ext.getCmp("ic_epo1").hide();
							Ext.getCmp("ic_pyme1").hide();
							Ext.getCmp('gridConsultaPorEsquema').hide();
							Ext.getCmp('noHayDatos').hide();
						}else  {
							Ext.getCmp("ic_epo1").hide();
							Ext.getCmp("ic_pyme1").hide();
						}
					}
				}
			}	
		},{
			xtype: 'combo',	
			name: 'ic_if',	
			id: 'ic_if1',	
			fieldLabel: '&nbsp;&nbsp;IF', 
			mode: 'local',	
			hiddenName: 'ic_if',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction: 'all',	
			typeAhead: true,
			minChars: 1,	
			store: catalogoIF,	
			hidden: false,
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 400,			
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!= '' ){
							Ext.getCmp("ic_epo1").setValue("");
							Ext.getCmp('gridConsultaPorEsquema').hide();
							Ext.getCmp('noHayDatos').hide();
							catalogoEPO.load({
								params: {
									ic_if:combo.getValue(),
									tipo_credito:Ext.getCmp("tipo_credito1").getValue()
								}				
							});
							Ext.getCmp("ic_epo1").show();
							Ext.getCmp("ic_pyme1").hide();
						} else{
							Ext.getCmp("ic_epo1").hide();
							Ext.getCmp("ic_pyme1").hide();
						}
					}
				}
			}	
		},{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			hiddenName: 'ic_epo',
			fieldLabel: '&nbsp;&nbsp;EPO',
			mode: 'local', 
			autoLoad: false,
			hidden: true,
			displayField: 'descripcion',
			valueField: 'clave',		
			emptyText: 'Seleccione ...',					
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,			
			store: catalogoEPO,
			width: 400,		
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!= ''){
	
							if( Ext.getCmp('tipo_credito1').getValue() == 'C'){	//Habilito el combo Distribuidores
								Ext.getCmp('gridConsultaPorEsquema').hide();
								Ext.getCmp('noHayDatos').hide();
								Ext.getCmp('ic_pyme1').setValue("");
								catalogoDistribuidor.load({
									params: {
										ic_epo:combo.getValue(),
										ic_if:Ext.getCmp("ic_if1").getValue()
									}				
								});	
								Ext.getCmp('ic_pyme1').show();
							} else if(Ext.getCmp('tipo_credito1').getValue() == 'D'){ //Mando a llamar al grid
								Ext.getCmp('gridConsultaPorEsquema').hide();
								Ext.getCmp('noHayDatos').hide();
								Ext.getCmp('ic_pyme1').hide();
								fp.el.mask('Procesando...', 'x-mask-loading');
								consultaPorEsquema.load({
									params: {
										tipo_credito:Ext.getCmp("tipo_credito1").getValue(),
										ic_epo:combo.getValue(),
										ic_if:Ext.getCmp("ic_if1").getValue(),
										ic_pyme:'',
										titulo_grid:combo.getRawValue()
									}				
								});
							}
						} else{
							Ext.getCmp("ic_pyme1").hide();
							Ext.getCmp('gridConsultaPorEsquema').hide();
							Ext.getCmp('noHayDatos').hide();
						}
					}
				}
			}	
		},{
			xtype: 'combo',	
			name: 'ic_pyme',	
			id: 'ic_pyme1',	
			fieldLabel: '&nbsp;&nbsp;Distribuidor', 
			mode: 'local',	
			hiddenName: 'ic_pyme',	
			emptyText: 'Seleccione ... ',
			forceSelection: true,	
			triggerAction: 'all',	
			typeAhead: true,
			minChars : 1,	
			store: catalogoDistribuidor,	
			hidden: true,
			displayField: 'descripcion',	
			valueField: 'clave',
			width: 400,			
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()!= ''){
							fp.el.mask('Procesando...', 'x-mask-loading');
							consultaPorEsquema.load({
								params: {
									tipo_credito:Ext.getCmp("tipo_credito1").getValue(),
									ic_epo:Ext.getCmp("ic_epo1").getValue(),
									ic_if:Ext.getCmp("ic_if1").getValue(),
									ic_pyme:combo.getValue(),
									titulo_grid:Ext.getCmp("ic_epo1").getRawValue()
								}				
							});						
						} 
					}
				}
			}	
		},{	
			width: 150,	
			fieldLabel: 'Opci�n',									
			hidden: true,						
			//format: '000.00',
			id: 'opcion_consulta1',	
			name: 'opcion_consulta'					
		},{	
			width: 150,	
			fieldLabel: 'Encabezado',									
			hidden: true,			
			id: 'titulo_grid1',	
			name: 'titulo_grid'					
		},{				
			xtype: 'panel',	 
			id: 'noHayDatos', 
			hidden: true, 
			layout:'table',	
			width: 800,	
			layoutConfig: {columns: 3}, 
			border: false,
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	
					width: 800,
					frame: false,	
					colspan:'3',	
					border:true,	
					html:'<div align="center"><b>No se encontraron tasas</b	><br>&nbsp;</div>'	
				}
			]
		}, 
		NE.util.getEspaciador(10),
		gridConsultaPorEsquema
	];
 
 	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame	: false,
		border: false,
		width: 800,		
		autoHeight	: true,			
		layout : 'form',
		style: 'margin:0 auto;',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: 
			elementosForma,
			buttons: [{
				text: 'Generar Archivo',					
				tooltip:	'Generar Archivo ',
				iconCls: 'icoXls',
				id: 'btnGenerarCSV',
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					var esVisible = verificaGridPorEsquema();
					Ext.Ajax.request({
						url: '24consulta03ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'generarArchivo',	
							grid_visible: esVisible
						}),							
						callback: descargaArchivo
					});
				}
			},{
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var esVisible = verificaGridPorEsquema();
						Ext.Ajax.request({
							url: '24consulta03ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'imprimir',
								grid_visible: esVisible
							}),						
							callback: descargaArchivo
						});
					}
				}
			]
			
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			gridConsultaGeneral,
			NE.util.getEspaciador(10),
			fp/**,
			NE.util.getEspaciador(10),
			gridConsultaPorEsquema*/
		]
	});
	
	consultaGeneral.load();
	catalogoTipoCredito.load();
	catalogoIF.load();
});	