<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_epo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String ic_pyme = request.getParameter("ic_pyme") == null?"":(String)request.getParameter("ic_pyme");
String ic_linea = request.getParameter("ic_linea") == null?"":(String)request.getParameter("ic_linea");

LineaCredito  lineaCredito = ServiceLocator.getInstance().lookup("LineaCreditoEJB", LineaCredito.class); 

String infoRegresar ="", consulta ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
	

if (informacion.equals("catalogoEPO") ) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTipoCredito("C"); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoIF") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveEpo(ic_epo); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	
	
} else if (informacion.equals("catalogoPYME") ) {

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setCampoClave("cpe.ic_pyme");
	cat.setCampoDescripcion("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social"); 
	cat.setIcIF(ic_if); 
	cat.setClaveEpo(ic_epo); 
	cat.setTipoCredito("C"); 
	cat.setOrden("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoEstatusAsignar") ) {
	
	Vector  comboEstatus =  lineaCredito.getVecEstatus("N");
	
	for(int i=0;i<comboEstatus.size();i++)  	{	
		Vector vecColumnas = (Vector)comboEstatus.get(i);		
		datos = new HashMap();
		datos.put("clave", (String)vecColumnas.get(0));
		datos.put("descripcion", (String)vecColumnas.get(1));
		registros.add(datos);
	}
	
	datos = new HashMap();
	datos.put("clave", "0");
	datos.put("descripcion", "No asignar estatus");
	registros.add(datos);
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString(); 		
			
} else if (informacion.equals("Consultar") ) {

	Vector vecFilas = lineaCredito.consultaNafin(ic_epo,ic_if,ic_pyme,ic_linea,"4");
	for(int i=0;i<vecFilas.size();i++)  	{	
		Vector vecColumnas = (Vector)vecFilas.get(i);
		String folioSolicitud 	= (String)vecColumnas.get(0);
		String nombreEPO 		= (String)vecColumnas.get(1);
		String nombreIF		= (String)vecColumnas.get(2);
		String nombrePYME		= (String)vecColumnas.get(3);
		String tipoSolicitud 	= (String)vecColumnas.get(4);
		String fechaSolicitud 	= (String)vecColumnas.get(5);
		String montoAut		= (String)vecColumnas.get(7);
		String bancoServicio	= (String)vecColumnas.get(8);
		String numeroCuenta	= (String)vecColumnas.get(9);
		String nombreProducto 	= (String)vecColumnas.get(11);
		String cveProducto  	= (String)vecColumnas.get(12);
		String fechaAutIF		= (String)vecColumnas.get(13);
		String numeroCuentaIF	= (String)vecColumnas.get(14);
		String montoSol		= (String)vecColumnas.get(18);			
				
		datos = new HashMap();
		datos.put("PRODUCTO", nombreProducto);
		datos.put("FOLIO_SOLICITUD", folioSolicitud);
		datos.put("NOMBRE_IF", nombreIF);
		datos.put("NOMBRE_PYME", nombrePYME);
		datos.put("NOMBRE_EPO", nombreEPO);
		datos.put("TIPO_SOLICITUD", tipoSolicitud);
		datos.put("FECHA_SOLICITUD", fechaSolicitud);
		datos.put("FECHA_AUTORIZACION_IF", fechaAutIF);
		datos.put("MONTO_AUTORIZADO", montoAut);
		datos.put("SALDO_DISPONIBLE", montoSol);
		datos.put("BANCO_SERVICIO", bancoServicio);
		datos.put("DISTRIBUIDOR_CUENTA", numeroCuenta);
		datos.put("IF_CUENTA", numeroCuentaIF);
		datos.put("IC_ESTATUS", "6");
		datos.put("CAUSA_RECHAZO", "");		
		registros.add(datos);
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString(); 		

} else if (informacion.equals("TerminaCaptura") ) {

	String folioSolicitud [] 	= request.getParameterValues("folioSolicitud");
   String ic_estatus [] 	= request.getParameterValues("ic_estatus");
	String huboCambios [] 	= request.getParameterValues("huboCambios");
	String elementos = (request.getParameter("elementos") == null)?"0":(String)request.getParameter("elementos");
	String causasRechazo [] 	= request.getParameterValues("causasRechazo");
	
	try {
		lineaCredito.AutorizaNafin(folioSolicitud,ic_estatus,huboCambios,elementos,causasRechazo,"4");
		mensaje = "Autorizaciones llevadas a cabo con exito";
	} catch(NafinException ne) {
		mensaje = ne.getMsgError();
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();		

}

%>
<%=infoRegresar%>