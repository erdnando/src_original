<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion  = request.getParameter("informacion")  == null?"":(String)request.getParameter("informacion");
String tipo_credito = request.getParameter("tipo_credito") == null?"":(String)request.getParameter("tipo_credito");
String ic_epo       = request.getParameter("ic_epo")       == null?"":(String)request.getParameter("ic_epo");

String infoRegresar  = "",   mensaje = "", qtipofin = "",  autoriza = "", tcobranza = ""; 
JSONObject jsonObj   = new JSONObject();
Vector vecParametros = new Vector(); 	

ParametrosDist  BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 


//****************Sección 1: Esquemas de operación********************

String tipocred			= request.getParameter("tipocred")				== null?"" :(String)request.getParameter("tipocred");
String firma_mancomunada= request.getParameter("firma_mancomunada")	== null?"" :(String)request.getParameter("firma_mancomunada"); //Fodea 32-2014
String ventaCartera		= request.getParameter("ventaCartera")			== null?"N":(String)request.getParameter("ventaCartera");
String respago				= request.getParameter("respago")				== null?"" :(String)request.getParameter("respago");
String tipocob				= request.getParameter("tipocob")				== null?"" :(String)request.getParameter("tipocob");
String pgointeres			= request.getParameter("pgointeres")			== null?"" :(String)request.getParameter("pgointeres");
String negociableabaja	= request.getParameter("negociableabaja")		== null?"" :(String)request.getParameter("negociableabaja");  //Fodea 13-2014
String bajocontrato	= request.getParameter("bajocontrato")		== null?"" :(String)request.getParameter("bajocontrato");  //Fodea 13-2014

String modplazos2			= request.getParameter("modplazos2")			== null?"" :(String)request.getParameter("modplazos2");
String modplazo1			= request.getParameter("modplazo1")				== null?"" :(String)request.getParameter("modplazo1");
String modplazo2			= request.getParameter("modplazo2")				== null?"" :(String)request.getParameter("modplazo2");
String modplazo3			= request.getParameter("modplazo3")				== null?"" :(String)request.getParameter("modplazo3");
String modplazo4			= request.getParameter("modplazo4")				== null?"" :(String)request.getParameter("modplazo4");
String DesAutomaticoEPO	= request.getParameter("DesAutomaticoEPO")	== null?"" :(String)request.getParameter("DesAutomaticoEPO");
String noNegociables		= request.getParameter("noNegociables")		== null?"" :(String)request.getParameter("noNegociables");
String lineaCredito		= request.getParameter("lineaCredito")			== null?"" :(String)request.getParameter("lineaCredito");
String tipoPago			= request.getParameter("tipoPago")				== null?"" :(String)request.getParameter("tipoPago"); //Fodea 13-2014
String mesesIntereses			= request.getParameter("mesesIntereses")				== null?"" :(String)request.getParameter("mesesIntereses"); //Fodea 09-2014 
String cmbMeses			= request.getParameter("cmbMeses")				== null?"" :(String)request.getParameter("cmbMeses"); //Fodea 09-2014 


if(modplazo1.equals("on")) { modplazo1 ="1";	}
if(modplazo2.equals("on")) {modplazo2 ="2";	}
if(modplazo3.equals("on")) {modplazo3 ="3";	}
if(modplazo4.equals("on")) {modplazo4 ="5";	}
if(modplazos2.equals(""))  {modplazos2 = modplazo1 +modplazo2 +modplazo3+modplazo4;  }
if(tipocred.equals("C"))   {ventaCartera ="N";			}
if(lineaCredito.equals("")){lineaCredito ="N";			}
if(tipocred.equals("A"))   {DesAutomaticoEPO	= "N";   }
if(tipocred.equals("F"))   { 
	pgointeres 			= "1";  
	respago 				= ""; 	
	tipocob				= "";
	modplazos2		 	= "";   
	DesAutomaticoEPO 	= "N";
	noNegociables		= "N";   
	lineaCredito		= "N";
	ventaCartera 		= "N";
}
int  actualiza =	0;


//*****************Sección 2: Parámetros ********************
String txdiasmin					= request.getParameter("txdiasmin")					== null?"" :(String)request.getParameter("txdiasmin");
String txplazomax					= request.getParameter("txplazomax")				== null?"" :(String)request.getParameter("txplazomax");
String txtDiasFechaVenc			= request.getParameter("txtDiasFechaVenc")		== null?"" :(String)request.getParameter("txtDiasFechaVenc");
String rconversion				= request.getParameter("rconversion")				== null?"" :(String)request.getParameter("rconversion");
String rcontrol					= request.getParameter("rcontrol")					== null?"" :(String)request.getParameter("rcontrol");
String txplazo						= request.getParameter("txplazo")					== null?"" :(String)request.getParameter("txplazo");
String txporcentaje				= request.getParameter("txporcentaje")				== null?"" :(String)request.getParameter("txporcentaje");
String txproxvenc					= request.getParameter("txproxvenc")				== null?"" :(String)request.getParameter("txproxvenc");
String rad_fecha_porc_desc		= request.getParameter("rad_fecha_porc_desc")	== null?"" :(String)request.getParameter("rad_fecha_porc_desc");
String txtEnvCorreo				= request.getParameter("txtEnvCorreo")				== null?"" :(String)request.getParameter("txtEnvCorreo");
String nombreEPO					= request.getParameter("nombreEPO") 				== null?"" :(String)request.getParameter("nombreEPO");
String diasoperacion				= request.getParameter("diasoperacion") 			== null?"0":(String)request.getParameter("diasoperacion"); 			//ABR
String descAdquirienteBanco	= request.getParameter("descAdquirienteBanco")	== null?"0":(String)request.getParameter("descAdquirienteBanco");	//ABR
String noAfiliacionComer      = request.getParameter("no_afiliacion_comer") 	== null?"" :(String)request.getParameter("no_afiliacion_comer");
if(txdiasmin.equals("")) 			{ txdiasmin ="0"; 		}
if(txplazomax.equals(""))			{ txplazomax ="0"; 		}
if(txtDiasFechaVenc.equals("")) 	{ txtDiasFechaVenc ="0";}
if(txplazo.equals("")) 				{ txplazo ="0"; 			}
if(txporcentaje.equals("")) 		{ txporcentaje ="0"; 	}
if(txproxvenc.equals("")) 			{ txproxvenc ="0"; 		}
  
		 
//Sección 3: Parámetros Generales EPO - IF*
String elementos = request.getParameter("elementos") == null?"0":(String)request.getParameter("elementos");
if(elementos.equals("")) { elementos ="0"; }
String porCobranza [] 	= request.getParameterValues("porCobranza");
String montoLinea [] 	= request.getParameterValues("montoLinea");
String porGarantia [] 	= request.getParameterValues("porGarantia");
String ic_ifs [] 			= request.getParameterValues("ic_ifs");



if (informacion.equals("catalogoEPO") ) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");   
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("Consultar_Seccion1") ) {
	
	vecParametros = BeanParametros.getParamEpo(ic_epo);
	
	tipocred         = (vecParametros.get(0).toString().trim()==null)?"":vecParametros.get(0).toString().trim();
	respago          = (vecParametros.get(1).toString().trim()==null)?"":vecParametros.get(1).toString().trim();
	tipocob          = (vecParametros.get(2).toString().trim()==null)?"":vecParametros.get(2).toString().trim();
	pgointeres       = (vecParametros.get(3).toString().trim()==null)?"":vecParametros.get(3).toString().trim();		
	qtipofin         = (vecParametros.get(4).toString().trim()==null)?"":vecParametros.get(4).toString().trim();
	negociableabaja  = (vecParametros.get(7).toString().trim()==null)?"":vecParametros.get(7).toString().trim();
	tipoPago         = (vecParametros.get(8).toString().trim()==null)?"":vecParametros.get(8).toString().trim();
	firma_mancomunada = (vecParametros.get(9).toString().trim()==null)?"":vecParametros.get(9).toString().trim();
	bajocontrato     = (vecParametros.get(10).toString().trim()==null)?"":vecParametros.get(10).toString().trim();
        
	Vector parametros = (Vector) vecParametros.elementAt(5);  	
  	if (parametros.size()==4){ 
		for (int i = 0; i <parametros.size(); i++) { 
			DesAutomaticoEPO = (String)parametros.get(0); 
			noNegociables    = (String)parametros.get(1);			
			ventaCartera     = (String)parametros.get(2);
			lineaCredito     = (String)parametros.get(3);       
		}
  }  
	Vector   desauto = (Vector) vecParametros.elementAt(6);   
   if (desauto.size()>0){
		for (int i = 0; i <desauto.size(); i++) {  
			String   valor = (String)desauto.get(i);
         if (valor.equals("1")) {             
				modplazo1 	= valor;
         }else if (valor.equals("2")) {   
				modplazo2 	= valor;
         }else if (valor.equals("3")) {   
				modplazo3 	= valor;
         }else if (valor.equals("5")) {    
				modplazo4 	= valor;   
         }
      }
    } //if
       
	if(DesAutomaticoEPO.equals("")) { DesAutomaticoEPO ="N"; }
	if(noNegociables.equals("")) { noNegociables ="N"; }	
	if(tipocred.equals("A")) {  modplazos2 =  modplazo1 +modplazo2 +modplazo3+modplazo4;   }
	
	jsonObj.put("success", 				new Boolean(true)	);	
	jsonObj.put("accion", 				"C"					);
	jsonObj.put("tipocred",				tipocred				);	
	jsonObj.put("firma_mancomunada",	firma_mancomunada ); // Fodea 32-2014
	jsonObj.put("ventaCartera",		ventaCartera		);
	jsonObj.put("respago",				respago				);
	jsonObj.put("tipocob",				tipocob				);
	jsonObj.put("pgointeres",			pgointeres			);
	jsonObj.put("negociableabaja",	negociableabaja	);
	jsonObj.put("tipoPago",				tipoPago				); // Fodea 13-2014
	jsonObj.put("modplazos2",			modplazos2			);
	jsonObj.put("modplazo1",			modplazo1			);
	jsonObj.put("modplazo2",			modplazo2			);
	jsonObj.put("modplazo3",			modplazo3			);
	jsonObj.put("modplazo4",			modplazo4			);
	jsonObj.put("DesAutomaticoEPO",	DesAutomaticoEPO	);
	jsonObj.put("noNegociables", 		noNegociables		);
	jsonObj.put("lineaCredito",		lineaCredito		);
	jsonObj.put("bajocontrato", bajocontrato);
        infoRegresar = jsonObj.toString();	

}else if (informacion.equals("Modidicar_Seccion1") ) {
	
	try {
		if(tipoPago.equals("TC")){
			pgointeres = "0";
		}
		actualiza = BeanParametros.vmodificaParamEpo(tipocred,respago,tipocob, Integer.parseInt(pgointeres),ic_epo,modplazos2,4,DesAutomaticoEPO,noNegociables,lineaCredito,ventaCartera, negociableabaja, tipoPago, firma_mancomunada,  mesesIntereses,   cmbMeses, bajocontrato );  

		mensaje ="Los Parámetros se actualizaron satisfactoriamente";
	}catch(Exception e){
		mensaje  = "Error: "+e;
		log.error(mensaje);
	}	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion","G");	
	jsonObj.put("mensaje",mensaje);	
	infoRegresar = jsonObj.toString();	


} else if(informacion.equals("Consultar_Seccion2")){

	String descuentoAutomatico = "";
	if(!ic_epo.equals("")) {
		descuentoAutomatico = BeanParametros.DesAutomaticoEpo(ic_epo,"PUB_EPO_DESC_AUTOMATICO");
		ventaCartera = BeanParametros.DesAutomaticoEpo(ic_epo,"PUB_EPO_VENTA_CARTERA");
		mesesIntereses = BeanParametros.DesAutomaticoEpo(ic_epo,"CS_MESES_SIN_INTERESES");		
		noAfiliacionComer = BeanParametros.getAfiliadoComercio(ic_epo);
		if(descuentoAutomatico.equals("S") && tipocred.equals("C") ){
			rad_fecha_porc_desc = "";
			txplazo = "0";
			txporcentaje = "0";
		}
	}

	//Sección 2
	vecParametros        = BeanParametros.getParamEpoa(ic_epo);
	txdiasmin            = (vecParametros.get(0) ==null)?"": vecParametros.get(0).toString().trim();
	txplazomax           = (vecParametros.get(1) ==null)?"": vecParametros.get(1).toString().trim();
	rconversion          = (vecParametros.get(2) ==null)?"": vecParametros.get(2).toString().trim();
	rcontrol             = (vecParametros.get(3) ==null)?"": vecParametros.get(3).toString().trim();
	txplazo              = (vecParametros.get(4) ==null)?"": vecParametros.get(4).toString().trim();
	txporcentaje         = (vecParametros.get(5) ==null)?"": vecParametros.get(5).toString().trim();
	nombreEPO            = (vecParametros.get(6) ==null)?"": vecParametros.get(6).toString().trim();
	tipocred             = (vecParametros.get(7) ==null)?"": vecParametros.get(7).toString().trim();
	tipocob              = (vecParametros.get(8) ==null)?"": vecParametros.get(8).toString().trim();
	txproxvenc           = (vecParametros.get(9) ==null)?"": vecParametros.get(9).toString().trim();
	rad_fecha_porc_desc  = (vecParametros.get(10)==null)?"": vecParametros.get(10).toString().trim();
	txtDiasFechaVenc     = (vecParametros.get(11)==null)?"": vecParametros.get(11).toString().trim();
	txtEnvCorreo         = (vecParametros.get(12)==null)?"": vecParametros.get(12).toString().trim();
	diasoperacion        = (vecParametros.get(13)==null)?"": vecParametros.get(13).toString().trim();
	tipoPago             = (vecParametros.get(14)==null)?"": vecParametros.get(14).toString().trim(); // Fodea 13-2014
	descAdquirienteBanco = (vecParametros.get(15)==null)?"": vecParametros.get(15).toString().trim(); // Fodea 13-2014
	cmbMeses  				= (vecParametros.get(16)==null)?"": vecParametros.get(16).toString().trim(); // Fodea 13-2014
	

	jsonObj.put("success",              new Boolean(true)    );
	jsonObj.put("accion",               "C"                  );
	jsonObj.put("txdiasmin",            txdiasmin            );
	jsonObj.put("txplazomax",           txplazomax           );
	jsonObj.put("txtDiasFechaVenc",     txtDiasFechaVenc     );
	jsonObj.put("rconversion",          rconversion          );
	jsonObj.put("rcontrol",             rcontrol             );
	jsonObj.put("txplazo",              txplazo              );
	jsonObj.put("txporcentaje",         txporcentaje         );
	jsonObj.put("txproxvenc",           txproxvenc           );
	jsonObj.put("rad_fecha_porc_desc",  rad_fecha_porc_desc  );
	jsonObj.put("txtEnvCorreo",         txtEnvCorreo         );
	jsonObj.put("nombreEPO",            nombreEPO            );
	jsonObj.put("tipocred",             tipocred             );
	jsonObj.put("descuentoAutomatico",  descuentoAutomatico  );
	jsonObj.put("ventaCartera",         ventaCartera         );
	jsonObj.put("diasoperacion",        diasoperacion        );
	jsonObj.put("descAdquirienteBanco", descAdquirienteBanco ); // Fodea 13-2014
	jsonObj.put("tipoPago",             tipoPago             ); // Fodea 13-2014
	jsonObj.put("noAfiliacionComer",    noAfiliacionComer    );
	jsonObj.put("cmbMeses",    cmbMeses    );
	jsonObj.put("mesesIntereses",    mesesIntereses    ); 
	

	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("Consultar_Seccion3")){

	JSONArray registros = new JSONArray();
	HashMap datos = new HashMap();
	JSONObject 	resultado	= new JSONObject();

	vecParametros = BeanParametros.getParamEpoa(ic_epo);	
	tipocob =(vecParametros.get(8)==null)?"":vecParametros.get(8).toString().trim();				
	
	//Sección 3
	Vector vecVectores = BeanParametros.getParamEpo_IF(ic_epo);
	for(int i=0;i<vecVectores.size();i++){
		Vector vecParamEpoIf = (Vector)vecVectores.elementAt(i);
		String qdescIF       = (vecParamEpoIf.get(0).toString().trim()==null)?"":vecParamEpoIf.get(0).toString().trim();
		String qorcentdel    = (vecParamEpoIf.get(1).toString().trim()==null)?"":vecParamEpoIf.get(1).toString().trim();
		String qbloqueo      = (vecParamEpoIf.get(2).toString().trim()==null)?"":vecParamEpoIf.get(2).toString().trim();
		String qporcgarant   = (vecParamEpoIf.get(3).toString().trim()==null)?"0":vecParamEpoIf.get(3).toString().trim();
		if(qporcgarant.equals("") || qporcgarant==null)qporcgarant="0";
		String mtoautoriza   = (vecParamEpoIf.get(4).toString().trim()==null)?"0":vecParamEpoIf.get(4).toString().trim();
		if(mtoautoriza.equals("") || mtoautoriza==null)mtoautoriza="0";
		String qhidIF        = (vecParamEpoIf.get(5).toString().trim()==null)?"":vecParamEpoIf.get(5).toString().trim();	 			

		double total = Double.parseDouble(mtoautoriza) *(Double.parseDouble(qporcgarant)/100);					

		String mntogarantia = Double.toString(total);
		
		datos = new HashMap();
		datos.put("IC_IF",					qhidIF		);
		datos.put("NOMBRE_IF",				qdescIF		);
		datos.put("PORCENTAJE_COBRANZA",	qorcentdel	);
		datos.put("MONTO_LINEA",			qbloqueo		);
		datos.put("PORCENTAJE_GARANTIA",	qporcgarant	);
		datos.put("MONTO_GARANTIA",		mntogarantia);
		datos.put("TIPO_COBRANZA",			tipocob		);
		datos.put("MONTO_AUTORIZA",		mtoautoriza	);
		registros.add(datos);
		
	}
	
	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);	
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("Modidicar_Seccion2") ) {

	double tasaDescuento = 0; 
	try{
		if(diasoperacion.equals("")){
			diasoperacion = "0";
		}
		if(!descAdquirienteBanco.equals("")){
			tasaDescuento = Double.parseDouble(descAdquirienteBanco);
		}
		
		BeanParametros.vmodificaParamEpoa(
		Integer.parseInt(txdiasmin) , Integer.parseInt(txplazomax) , rconversion,
		rcontrol, Integer.parseInt(txplazo) , Double.parseDouble((String)txporcentaje)  ,
		ic_epo, Integer.parseInt(txproxvenc)  , rad_fecha_porc_desc, 
		txtDiasFechaVenc, txtEnvCorreo,  Integer.parseInt(diasoperacion),
		tasaDescuento, tipocred, tipoPago, iNoUsuario, strNombreUsuario, noAfiliacionComer, mesesIntereses, cmbMeses); 
	
		BeanParametros.vmodificaEpoIF(porCobranza,montoLinea,porGarantia,ic_ifs,Integer.parseInt(elementos),ic_epo);
		
		mensaje ="Los Parámetros se actualizaron satisfactoriamente";
		
	}catch(Exception e){
		mensaje  = "Error: "+e;
	}	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",  "G");
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();	

 		
} else if (informacion.equals("Actualiza_Tasa") ) {

	CapturaTasas  BeanTasas = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class); 
	
	try{
		
		BeanTasas.actualizaParamTasas(ic_epo,"4");	
		
		mensaje ="Las Tasas se actualizaron satisfactoriamente";
	
	}catch(Exception e){
		mensaje  = "Error: "+e;
	}	
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensaje",mensaje);
	infoRegresar = jsonObj.toString();	
	
}

%>
<%=infoRegresar%>