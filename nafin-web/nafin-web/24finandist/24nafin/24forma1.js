Ext.onReady(function() {



	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var consultaDetalleData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'ConsDetalle'
		},		
		fields: [
			{	name: 'NO_FOLIO'},
			{	name: 'MONEDA'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'SALDO_INICIAL'},
			{	name: 'SALDO_DOCUMENTOS_FINANCIADOS'},
			{	name: 'MONTO_DISPONIBLE'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);								
				}
			}
		}		
	});
	
	var gridDetalle =[{
		id: 'gridDetalle',		
		xtype:'grid',
		title:'Informaci�n de L�nea',
		store: consultaDetalleData,
		style: 'margin:0 auto;',
		columns:[	
			{
				header: 'No. de Folio',
				tooltip: 'No. de Folio',
				dataIndex: 'NO_FOLIO',
				sortable: true,
				width: 150,
				align: 'center'
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,
				align: 'left'
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,
				align: 'center'
			},
			{
				header: 'Saldo Inicial',
				tooltip: 'Saldo Inicial',
				dataIndex: 'SALDO_INICIAL',
				sortable: true,				
				align: 'right',
				width: 150,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo de Documentos Financiados',
				tooltip: 'Saldo de Documentos Financiados',
				dataIndex: 'SALDO_DOCUMENTOS_FINANCIADOS',
				sortable: true,				
				align: 'right',
				width: 150,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Disponible',
				tooltip: 'Monto Disponible',
				dataIndex: 'MONTO_DISPONIBLE',
				sortable: true,				
				align: 'right',
				width: 150,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	
	var verDetalleEPO= function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		var ventana = Ext.getCmp('verDetalle');	
		var  ic_documento  = registro.get('NUMERO_DOCTO_FINAL');
		
			
		consultaDetalleData.load({
			params: { 
			ic_documento:ic_documento
			}
		});
					
					
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({	
				width: 710,	
				id:'verDetalle',				
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,	
				items: [					
					gridDetalle
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Salir ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrar', 
							handler: function(){								
								Ext.getCmp('verDetalle').destroy();									
							} 
						}					
					]
				}
			});
		}
	}
	
	//**** procesar Reporte  SIrac******************++
	
	function procesarGenerar_Reporte_SIRAC(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var 	jsonData = Ext.util.JSON.decode(response.responseText);
			
			
			Ext.getCmp('btnImprReporteSIRAC').show();
			Ext.getCmp('btnImprGenerarReporteSIRAC').show();
			Ext.getCmp('btnReporteSIRAC').hide();						
			Ext.getCmp('inDocumentos').setValue(jsonData.inDocumentos);
			
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{  	
				inDocumentos:jsonData.inDocumentos,
				ic_estatus_solic:'2',
				pantalla:'S'
				})
			});
					
			consTotalesData.load({
				params: Ext.apply(fp.getForm().getValues(),{  
				inDocumentos:jsonData.inDocumentos,
				ic_estatus_solic:'2'
			})
			});
					
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function generar_Reporte_SIRAC() {
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		
		var inDocumentos=''; 
		var coma= 0;
		
		store.each(function(record) {
			var numRegistro = store.indexOf(record);			
			if(record.data['SELECCIONAR']=='true' ||  record.data['SELECCIONAR']==true  ){	
				if(coma == 0){
					inDocumentos += record.data['NUMERO_DOCTO_FINAL'];
				}else{
					inDocumentos += "," + record.data['NUMERO_DOCTO_FINAL'];
				}
				coma++;										
			}
		});
				
		if(inDocumentos =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un registro para continuar');
			return false;	
			
		}else  {			
			var btnReporteSIRAC = Ext.getCmp('btnReporteSIRAC');
			btnReporteSIRAC.setHandler(
				function(boton, evento) {											
					Ext.Ajax.request({
						url: '24forma1.data.jsp',
						params: {
							informacion: 'Generar_Reporte_SIRAC',
							inDocumentos:inDocumentos									
						}
						,callback: procesarGenerar_Reporte_SIRAC
					});
				}
			);		
		}		
	
	}
		

	var consTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'ConsTotales'
		},		
		fields: [			
			{	name: 'MONEDA'},
			{	name: 'TOTAL'},			
			{	name: 'MONTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MONTO_INTERES'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);									
				}
			}
		}		
	});
	

	var gridTotales= new Ext.grid.EditorGridPanel({	
		store: consTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',
		clicksToEdit: 1,		
		hidden: true,		
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Total Registros',
				tooltip: 'Total Registros',
				dataIndex: 'TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},				
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 100,
		width: 900,
		align: 'center',
		frame: false	
	});


			
 //*******************Consulta *************************************
 
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.pantalla =='N') {
				Ext.getCmp('btnImprReporteSIRAC').hide();
				Ext.getCmp('btnImprGenerarReporteSIRAC').hide();
				Ext.getCmp('btnReporteSIRAC').show();
			}
			
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				if(jsonData.pantalla =='N') {
					Ext.getCmp('btnReporteSIRAC').enable();								
				}
				Ext.getCmp('gridTotales').show();	
			} else {		
				if(jsonData.pantalla =='N') { 
					Ext.getCmp('btnReporteSIRAC').disable();					
				}
				Ext.getCmp('gridTotales').hide();
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_DISTRIBUIDOR'},
			{	name: 'NUMERO_INICIAL'},
			{	name: 'FECHA_EMISION'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'PLAZO_DOCTO'},
			{	name: 'MONEDA'},
			{	name: 'MONTO'},
			{	name: 'PLAZO_DESCUENTO'},
			{	name: 'PORCENTAJE_DESCUENTO'},
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'TIPO_CONV'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'MONTO_VALUADO'},
			{	name: 'MODALIDAD_PLAZO'},
			{	name: 'TIPO_COBRANZA'},
			{	name: 'ESTATUS'},
			{	name: 'TIPO_CREDITO'},
			{  name: 'CG_TIPO_CREDITO'},			
			{	name: 'NUMERO_DOCTO_FINAL'},			
			{	name: 'PLAZO'},
			{	name: 'FECHA_VENC_CREDITO'},
			{	name: 'FECHA_OPERACION'},
			{	name: 'NOMBRE_IF'},
			{	name: 'REFERENCIA_TASA'},
			{	name: 'VALOR_TASA'},
			{	name: 'MONTO_INTERES'},
			{	name: 'TIPO_COBRO_INTERES'},
			{	name: 'SELECCIONAR'}				
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
	
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Datos Documento Inicial', colspan: 17, align: 'center'},
					{header: 'Datos Documento Final', colspan: 12, align: 'center'}					
				]
			]
	});
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		plugins: gruposConsulta,
		hidden: true,		
		columns: [	
			{
				xtype: 'actioncolumn',
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CG_TIPO_CREDITO') =='D' ) {						
								this.items[0].tooltip = 'Ver';
								return 'icoBuscar';							
							}
						}
						,handler: verDetalleEPO
					}
				]						
			},
			{
				header: 'Distribuidor',
				tooltip: 'Distribuidor',
				dataIndex: 'NOMBRE_DISTRIBUIDOR',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de documento inicial',
				tooltip: 'N�mero de documento inicial',
				dataIndex: 'NUMERO_INICIAL',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'				
			},
			{
				header: 'Fecha de emisi�n',
				tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'				
			},
			{
				header: 'Feha de vencimiento',
				tooltip: 'Feha de vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'				
			},
			{
				header: 'Plazo docto.',
				tooltip: 'Plazo docto.',
				dataIndex: 'PLAZO_DOCTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'				
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left'				
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Plazo para descuento en d�as',
				tooltip: 'Plazo para descuento en d�as',
				dataIndex: 'PLAZO_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'							
			},
			{
				header: '% de descuento',
				tooltip: '% de descuento',
				dataIndex: 'PORCENTAJE_DESCUENTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto a descontar',
				tooltip: 'Monto a descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Tipo conv',
				tooltip: 'Tipo conv',
				dataIndex: 'TIPO_CONV',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'								
			},
			{
				header: 'Tipo de cambio',
				tooltip: 'Tipo de cambio',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'								
			},
			{
				header: 'Monto valuado en pesos',
				tooltip: 'Monto valuado en pesos',
				dataIndex: 'MONTO_VALUADO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Modalidad de plazo',
				tooltip: 'Modalidad de plazo',
				dataIndex: 'MODALIDAD_PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left'								
			},
			{
				header: 'Tipo de cobranza',
				tooltip: 'Tipo de cobranza',
				dataIndex: 'TIPO_COBRANZA',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left'								
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left'								
			},
			{
				header: 'Tipo de cr�dito',
				tooltip: 'Tipo de cr�dito',
				dataIndex: 'TIPO_CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'left'								
			},
			{
				header: 'N�mero de documento final',
				tooltip: 'N�mero de documento final',
				dataIndex: 'NUMERO_DOCTO_FINAL',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'								
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,	
				align: 'center'				
			},
			{
				header: 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENC_CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'Fecha de operaci�n',
				tooltip: 'Fecha de operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'				
			},
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Referencia tasa de inter�s',
				tooltip: 'Referencia tasa de inter�s',
				dataIndex: 'REFERENCIA_TASA',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'left'				
			},
			{
				header: 'Valor tasa inter�s',
				tooltip: 'Valor tasa inter�s',
				dataIndex: 'VALOR_TASA',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex: 'MONTO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Tipo de cobro inter�s',
				tooltip: 'Tipo de cobro inter�s',
				dataIndex: 'TIPO_COBRO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'left'								
			},
			{
				xtype: 'checkcolumn',
				header: 'Seleccionar',
				tooltip: 'Seleccionar',
				dataIndex: 'SELECCIONAR',
				sortable: true,
				width: 150,			
				resizable: true,									
				align: 'center'								
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Reporte SIRAC',					
					tooltip:	'Generar Reporte SIRAC',
					iconCls: 'icoXls',
					id: 'btnReporteSIRAC',
					handler: generar_Reporte_SIRAC						
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo SIRAC',					
					tooltip:	'Generar Archivo SIRAC',
					iconCls: 'icoXls',
					hidden:true,
					id: 'btnImprGenerarReporteSIRAC',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '24forma1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'generar_Archivo_SIRAC'
							}),
							callback: procesarDescargaArchivos
						});						
					}				
				}	, 
				'-',
				{
					xtype: 'button',
					text: 'Imprimir Reporte SIRAC',					
					tooltip:	'Imprimir Reporte SIRAC',
					iconCls: 'icoPdf',
					hidden:true,
					id: 'btnImprReporteSIRAC',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '24forma1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Imprimir_Reporte_SIRAC'
							}),
							callback: procesarDescargaArchivos
						});						
					}			
				}			
			]
		}
	});




 //*******************Criterios de Busqueda *************************************
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				Ext.getCmp('df_fecha_operacion_de').setValue(jsonData.fechaHoy);	
				Ext.getCmp('df_fecha_operacion_a').setValue(jsonData.fechaHoy);	
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	

	var catCobroInteresData = new Ext.data.JsonStore({
		id: 'catCobroInteresData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'catCobroInteresData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catMonedaData = new Ext.data.JsonStore({
		id: 'catMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'catMonedaData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catIFData = new Ext.data.JsonStore({
		id: 'catIFData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma1.data.jsp',
		baseParams: {
			informacion: 'catIFData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catTipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['D','Descuento y/o Factoraje'],
			['C','Credito en Cuenta Corriente']			
		 ]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			hiddenName : 'ic_if',
			fieldLabel: 'IF',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_operacion_de',
					id: 'df_fecha_operacion_de',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'df_fecha_operacion_a',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_operacion_a',
					id: 'df_fecha_operacion_a',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'df_fecha_operacion_de',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de cr�dito',
			combineErrors: false,
			msgTarget: 'side',
			width: 700,
			items: [			
				{
					xtype: 'combo',	
					name: 'tipoCredito',	
					id: 'tipoCredito1',	
					fieldLabel: 'Tipo de cr�dito', 
					mode: 'local',	
					hiddenName : 'tipoCredito',	
					emptyText: 'Seleccione ... ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catTipoCreditoData,	
					displayField : 'descripcion',	
					valueField : 'clave',
					width: 200
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de cobro de inter�s',
					width: 150
				},
				{
					xtype: 'combo',
					name: 'ic_tipo_cobro_int',
					id: 'ic_tipo_cobro_int1',
					hiddenName : 'ic_tipo_cobro_int',
					fieldLabel: 'Tipo de cobro de inter�s',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',		
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,	
					autoLoad: false,
					store : catCobroInteresData,
					tpl : NE.util.templateMensajeCargaCombo
				}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de documento final',
			combineErrors: false,
			msgTarget: 'side',
			width: 350,
			items: [	
				{
					xtype: 'numberfield',
					fieldLabel: 'N�mero de documento final',
					name: 'ic_documento',
					id: 'ic_documento1',
					allowBlank: true,
					maxLength: 15,
					width: 200,
					msgTarget: 'side',
					margins: '0 20 0 0'			
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de documento inicial',
			combineErrors: false,
			msgTarget: 'side',
			width: 350,
			items: [	
				{
					xtype: 'textfield',
					fieldLabel: 'N�mero de documento inicial',	
					name: 'ig_numero_docto',
					id: 'ig_numero_docto',
					allowBlank: true,
					maxLength: 15,
					width: 200,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 350,
			items: [		
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',		
					emptyText: 'Seleccione...',					
					forceSelection : true,
					autoLoad: false,
					triggerAction : 'all',
					width: 200,
					typeAhead: true,
					minChars : 1,							
					store : catMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}	
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'inDocumentos', 	value: '' }
	];
		
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		monitorValid: true,
		title: ' Autorizaci�n de solicitudes',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {
				
					var df_fecha_operacion_de = Ext.getCmp('df_fecha_operacion_de');
					var df_fecha_operacion_a = Ext.getCmp('df_fecha_operacion_a');
					
					if(!Ext.isEmpty(df_fecha_operacion_de.getValue()) || !Ext.isEmpty(df_fecha_operacion_a.getValue())){
						if(Ext.isEmpty(df_fecha_operacion_de.getValue())){
							df_fecha_operacion_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_operacion_de.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_operacion_a.getValue())){
							df_fecha_operacion_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_operacion_a.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{ 
						inDocumentos:''
						})
					});
					
					consTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{  
						inDocumentos: ''
						
						})
					});
					
					
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '24forma1ext.jsp';					
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});
	
	catIFData.load();
	catCobroInteresData.load();
	catMonedaData.load();
	
	
	
	Ext.Ajax.request({
		url: '24forma1.data.jsp',
		params: {
			informacion: "valoresIniciales"		
		},
		callback: procesaValoresIniciales
	});
	

});
