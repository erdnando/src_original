<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.threads.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/24finandist/24secsession_extjs.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String operacion = request.getParameter("operacion") == null?"":(String)request.getParameter("operacion");

String ic_epo = request.getParameter("ic_epo") == null?"":(String)request.getParameter("ic_epo");
String ic_documento = request.getParameter("ic_documento") == null?"":(String)request.getParameter("ic_documento");
String ic_pyme = request.getParameter("ic_pyme") == null?"":(String)request.getParameter("ic_pyme");
String tipo_credito = request.getParameter("tipo_credito") == null?"":(String)request.getParameter("tipo_credito");
String ic_if = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String fecha_seleccion_de = request.getParameter("fecha_seleccion_de") == null?"":(String)request.getParameter("fecha_seleccion_de");
String fecha_seleccion_a = request.getParameter("fecha_seleccion_a") == null?"":(String)request.getParameter("fecha_seleccion_a");
String ic_estatus_docto = request.getParameter("ic_estatus_docto") == null?"":(String)request.getParameter("ic_estatus_docto");
String monto_credito_de = request.getParameter("monto_credito_de") == null?"":(String)request.getParameter("monto_credito_de");
String monto_credito_a = request.getParameter("monto_credito_a") == null?"":(String)request.getParameter("monto_credito_a");
String ig_numero_docto = request.getParameter("ig_numero_docto") == null?"":(String)request.getParameter("ig_numero_docto");
String fecha_vto_credito_de = request.getParameter("fecha_vto_credito_de") == null?"":(String)request.getParameter("fecha_vto_credito_de");
String fecha_vto_credito_a = request.getParameter("fecha_vto_credito_a") == null?"":(String)request.getParameter("fecha_vto_credito_a");
String fecha_vto_de = request.getParameter("fecha_vto_de") == null?"":(String)request.getParameter("fecha_vto_de");
String fecha_vto_a = request.getParameter("fecha_vto_a") == null?"":(String)request.getParameter("fecha_vto_a");
String ic_tipo_cobro_int = request.getParameter("ic_tipo_cobro_int") == null?"":(String)request.getParameter("ic_tipo_cobro_int");
String fecha_publicacion_de = request.getParameter("fecha_publicacion_de") == null?"":(String)request.getParameter("fecha_publicacion_de");
String fecha_publicacion_a = request.getParameter("fecha_publicacion_a") == null?"":(String)request.getParameter("fecha_publicacion_a");
String fecha_emision_de = request.getParameter("fecha_emision_de") == null?"":(String)request.getParameter("fecha_emision_de");
String fecha_emision_a = request.getParameter("fecha_emision_a") == null?"":(String)request.getParameter("fecha_emision_a");
String ic_moneda = request.getParameter("ic_moneda") == null?"":(String)request.getParameter("ic_moneda");
String cc_acuse = request.getParameter("cc_acuse") == null?"":(String)request.getParameter("cc_acuse");
String fn_monto_de = request.getParameter("fn_monto_de") == null?"":(String)request.getParameter("fn_monto_de");
String fn_monto_a = request.getParameter("fn_monto_a") == null?"":(String)request.getParameter("fn_monto_a");
String monto_con_descuento = request.getParameter("monto_con_descuento") == null?"":(String)request.getParameter("monto_con_descuento");
String chk_proc_desc = request.getParameter("chk_proc_desc") == null?"":(String)request.getParameter("chk_proc_desc");
String solo_cambio = request.getParameter("solo_cambio") == null?"":(String)request.getParameter("solo_cambio");
String modo_plazo = request.getParameter("modo_plazo") == null?"":(String)request.getParameter("modo_plazo");
String tipoConsulta = request.getParameter("tipoConsulta") == null?"":(String)request.getParameter("tipoConsulta");
String pantalla = request.getParameter("pantalla") == null?"":(String)request.getParameter("pantalla");
String ic_tipo_Pago = request.getParameter("ic_tipo_Pago") == null?"":(String)request.getParameter("ic_tipo_Pago");

String bins  = request.getParameter("catBins") == null?"":(String)request.getParameter("catBins");

if(monto_con_descuento.equals("on") )  {    monto_con_descuento = "S";  } else  {     monto_con_descuento = "";   }
if(chk_proc_desc.equals("on") )  {    chk_proc_desc = "S";  } else  {     chk_proc_desc = "";   }
if(solo_cambio.equals("on") )  {    solo_cambio = "S";  }  else  {     solo_cambio = "";   }

//Estos if son redundancia
if(informacion.equals("ArchivoCSV") )  { tipoConsulta = "1";   } // Este if se cambia por el sigueinte
if(informacion.equals("Imprimir_CSV_1") || informacion.equals("Imprimir_CSV_2")){
	tipoConsulta = "1";
}
if(informacion.equals("Imprimir_PDF_1") || informacion.equals("Imprimir_PDF_2")){
	tipoConsulta = "1";
}

	InfDocCredNafinDist paginador1 = new InfDocCredNafinDist();
	paginador1.setIc_epo(ic_epo);
	paginador1.setIc_documento(ic_documento);
	paginador1.setIc_pyme(ic_pyme);
	paginador1.setTipo_credito(tipo_credito);
	paginador1.setIc_if(ic_if);
	paginador1.setFecha_seleccion_de(fecha_seleccion_de);
	paginador1.setFecha_seleccion_a(fecha_seleccion_a);
	paginador1.setIc_estatus(ic_estatus_docto);
	paginador1.setMonto_credito_de(monto_credito_de);
	paginador1.setMonto_credito_a(monto_credito_a);
	paginador1.setIg_numero_docto(ig_numero_docto);
	paginador1.setFecha_vto_credito_de(fecha_vto_credito_de);
	paginador1.setFecha_vto_credito_a(fecha_vto_credito_a);
	paginador1.setFecha_vto_de(fecha_vto_de);
	paginador1.setFecha_vto_a(fecha_vto_a);
	paginador1.setIc_tipo_cobro_int(ic_tipo_cobro_int);
	paginador1.setFecha_publicacion_de(fecha_publicacion_de);
	paginador1.setFecha_publicacion_a(fecha_publicacion_a);
	paginador1.setFecha_emision_de(fecha_emision_de);
	paginador1.setFecha_emision_a(fecha_emision_a);
	paginador1.setIc_moneda(ic_moneda);
	paginador1.setCc_acuse(cc_acuse);
	paginador1.setFn_monto_de(fn_monto_de);
	paginador1.setFn_monto_a(fn_monto_a);
	paginador1.setMonto_con_descuento(monto_con_descuento);
	paginador1.setChk_proc_desc(chk_proc_desc);
	paginador1.setSolo_cambio(solo_cambio);
	paginador1.setModo_plazo(modo_plazo);
	paginador1.setTipoConsulta(tipoConsulta);
	paginador1.setIc_tipo_Pago(ic_tipo_Pago); 
	//CQueryHelperRegExtJS queryHelper1 = new CQueryHelperRegExtJS(paginador1);
	CQueryHelperThreadRegExtJS queryHelper1 = new CQueryHelperThreadRegExtJS(paginador1);
	queryHelper1.setMultiplesPaginadoresXPagina(true);

	InfDocCredNafinDist2 paginador2 = new InfDocCredNafinDist2();
	paginador2.setIc_epo(ic_epo);
	paginador2.setIc_documento(ic_documento);
	paginador2.setIc_pyme(ic_pyme);
	paginador2.setTipo_credito(tipo_credito);
	paginador2.setIc_if(ic_if);
	paginador2.setFecha_seleccion_de(fecha_seleccion_de);
	paginador2.setFecha_seleccion_a(fecha_seleccion_a);
	paginador2.setIc_estatus(ic_estatus_docto);
	paginador2.setMonto_credito_de(monto_credito_de);
	paginador2.setMonto_credito_a(monto_credito_a);
	paginador2.setIg_numero_docto(ig_numero_docto);
	paginador2.setFecha_vto_credito_de(fecha_vto_credito_de);
	paginador2.setFecha_vto_credito_a(fecha_vto_credito_a);
	paginador2.setFecha_vto_de(fecha_vto_de);
	paginador2.setFecha_vto_a(fecha_vto_a);
	paginador2.setIc_tipo_cobro_int(ic_tipo_cobro_int);
	paginador2.setFecha_publicacion_de(fecha_publicacion_de);
	paginador2.setFecha_publicacion_a(fecha_publicacion_a);
	paginador2.setFecha_emision_de(fecha_emision_de);
	paginador2.setFecha_emision_a(fecha_emision_a);
	paginador2.setIc_moneda(ic_moneda);
	paginador2.setCc_acuse(cc_acuse);
	paginador2.setFn_monto_de(fn_monto_de);
	paginador2.setFn_monto_a(fn_monto_a);
	paginador2.setMonto_con_descuento(monto_con_descuento);
	paginador2.setChk_proc_desc(chk_proc_desc);
	paginador2.setSolo_cambio(solo_cambio);
	paginador2.setModo_plazo(modo_plazo);
	paginador2.setTipoConsulta(tipoConsulta);
	paginador2.setBins(bins);//FODEA-013-2014
	paginador2.setIc_tipo_Pago(ic_tipo_Pago); 
	//CQueryHelperRegExtJS queryHelper2 = new CQueryHelperRegExtJS(paginador2);
	CQueryHelperThreadRegExtJS queryHelper2 = new CQueryHelperThreadRegExtJS(paginador2);
	queryHelper2.setMultiplesPaginadoresXPagina(true);

String infoRegresar = "", consulta = "";
JSONObject jsonObj = new JSONObject();
int  start= 0, limit =0,  start2= 0, limit2 =0;

if (informacion.equals("catEPOData")){

	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");   
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

}else  if (informacion.equals("catPymeData")){
	
	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	cat.setCampoClave("cp.ic_pyme");
	cat.setCampoDescripcion("cp.cg_razon_social");   
	cat.setClaveEpo(ic_epo);	
	cat.setOrden("cp.cg_razon_social");  	
	infoRegresar = cat.getJSONElementos();

}else  if (informacion.equals("catIFData")){

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");   
	cat.setClaveEpo(ic_epo);	
	cat.setOrden("cg_razon_social");  	
	infoRegresar = cat.getJSONElementos();


}else  if (informacion.equals("catEstatusData")){
	
	List catEstatus= paginador1.catEstatusData();
	jsonObj.put("registros", catEstatus);	
   infoRegresar = jsonObj.toString();	
	System.err.println("Información: "+informacion+"\nInfoRegresar: "+ infoRegresar);
}else if(informacion.equals("catalogoBINS")){
		Registros catBins= paginador1.catBINSData();
		jsonObj.put("success", new Boolean(true));	
		
		jsonObj.put("registros", catBins.getJSONData() );	
		infoRegresar = jsonObj.toString();
		System.err.println("Información: "+informacion+"\nInfoRegresar: "+ infoRegresar);
}else  if (informacion.equals("catCobroInteresData")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_cobro_interes");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_tipo_cobro_interes");
	catalogo.setOrden("ic_tipo_cobro_interes");
	catalogo.setValoresCondicionIn("1, 2", Integer.class);
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("catMonedaData")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else  if (informacion.equals("catModalidadData")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_tipo_financiamiento");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_tipo_financiamiento");		
	catalogo.setOrden("ic_tipo_financiamiento");	
	infoRegresar = catalogo.getJSONElementos();
	
	
} else if (informacion.equals("Consultar_1") || informacion.equals("consTotales_1") || informacion.equals("consDetalles") ){

	if (informacion.equals("Consultar_1")) {
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}

	if (informacion.equals("Consultar_1") ) {
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper1.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper1.getJSONPageResultSet(request,start,limit);
			jsonObj = JSONObject.fromObject(consulta);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	}else  if(informacion.equals("consTotales_1") ) {
		consulta = queryHelper1.getJSONResultCount(request); 
		jsonObj = JSONObject.fromObject(consulta);

	}else if(informacion.equals("consDetalles")) {

	try {
			Registros reg	=	queryHelper1.doSearch();
			
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la consulta", e);
		}	
	}
	
	jsonObj.put("tipo_credito", tipo_credito);
	infoRegresar = jsonObj.toString();	


} else if( informacion.equals("Consultar_2") || informacion.equals("ConsTotales_2")){

	if (informacion.equals("Consultar_2")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));	
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}
	}

	if (informacion.equals("Consultar_2")){
		try {
			if (operacion.equals("Generar")) { // Nueva consulta
				queryHelper2.executePKQuery(request); // Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper2.getJSONPageResultSet(request,start,limit);
			jsonObj = JSONObject.fromObject(consulta);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	} else if(informacion.equals("ConsTotales_2")){

		consulta = queryHelper2.getJSONResultCount(request);
		jsonObj = JSONObject.fromObject(consulta);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Imprimir_CSV_1")){

	String nombreArchivo = "";
	try{
		nombreArchivo = queryHelper1.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo CSV. ", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Imprimir_CSV_2")){

	String nombreArchivo = "";
	try{
		nombreArchivo = queryHelper2.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo CSV. ", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Imprimir_PDF_1")){

	String estatusArchivo = request.getParameter("estatusArchivo");
	String porcentaje = "";
	String nombreArchivo = "";
	try{
		if(estatusArchivo.equals("INICIO")){
			session.removeAttribute("24consulta1ThreadCreateFiles");
			ThreadCreateFiles threadCreateFiles = queryHelper1.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
			session.setAttribute("24consulta1ThreadCreateFiles", threadCreateFiles);
			estatusArchivo = "PROCESANDO";
		} else if(estatusArchivo.equals("PROCESANDO")){
			ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("24consulta1ThreadCreateFiles");
			System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
			porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
			if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
				nombreArchivo = threadCreateFiles.getNombreArchivo();
				estatusArchivo = "FINAL";
			}else if(threadCreateFiles.hasError()){
				estatusArchivo = "ERROR";
			}
		}
		if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %")){
			porcentaje = "Procesando...";
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("estatusArchivo", estatusArchivo);
		jsonObj.put("porcentaje", porcentaje);

	} catch(Throwable e){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF. ", e);
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Imprimir_PDF_2")){

	String estatusArchivo = request.getParameter("estatusArchivo");
	String porcentaje = "";
	String nombreArchivo = "";
	try{
		if(estatusArchivo.equals("INICIO")){
			session.removeAttribute("24consulta1ThreadCreateFiles");
			ThreadCreateFiles threadCreateFiles = queryHelper2.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
			session.setAttribute("24consulta1ThreadCreateFiles", threadCreateFiles);
			estatusArchivo = "PROCESANDO";
		} else if(estatusArchivo.equals("PROCESANDO")){
			ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("24consulta1ThreadCreateFiles");
			System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
			porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
			if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
				nombreArchivo = threadCreateFiles.getNombreArchivo();
				estatusArchivo = "FINAL";
			}else if(threadCreateFiles.hasError()){
				estatusArchivo = "ERROR";
			}
		}
		if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %")){
			porcentaje = "Procesando...";
		}
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("estatusArchivo", estatusArchivo);
		jsonObj.put("porcentaje", porcentaje);

	} catch(Throwable e){
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF. ", e);
	}
	infoRegresar = jsonObj.toString();

} 
/***** si no se utiliza el siguiente bloque de código comentado en otras páginas, entonces es obsoleto y se puede eliminar *****/
else if(informacion.equals("ArchivoPDF")){

	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));	
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		start2 = Integer.parseInt(request.getParameter("start2"));
		limit2 = Integer.parseInt(request.getParameter("limit2"));	
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	InfDocCredNafinDistDescarga descarga = new InfDocCredNafinDistDescarga();	
	descarga.setTipo_credito(tipo_credito);

	Registros reg1 = queryHelper1.getPageResultSet(request,start,limit);
	Registros reg2 = queryHelper2.getPageResultSet(request,start2,limit2);

	try {
			String nombreArchivo = descarga.crearPageCustomFile(request, reg1 , reg2 , strDirectorioTemp );
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
		infoRegresar = jsonObj.toString();

} else if(informacion.equals("ArchivoCSV")){

	InfDocCredNafinDistDescarga descarga = new InfDocCredNafinDistDescarga();
	descarga.setTipo_credito(tipo_credito);

	Registros reg1 = queryHelper1.doSearch();
	Registros reg2 = queryHelper2.doSearch();

	try {
			String nombreArchivo = descarga.crearCustomFile(request, reg1 , reg2 , strDirectorioTemp );
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}		
		infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>