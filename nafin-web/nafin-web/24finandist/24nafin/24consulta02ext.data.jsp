<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.distribuidores.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):""; 
String tipoCredito = request.getParameter("tipo_credito") == null?"":(String)request.getParameter("tipo_credito");
//tipoCreditoRespaldo: Toma el valor de un campo oculto y no del bombo box para evitar problemas con la paginación
String tipoCreditoRespaldo = request.getParameter("tipo_cred_respaldo") == null?"":(String)request.getParameter("tipo_cred_respaldo");
String tipoSolicitud = request.getParameter("tipo_solicitud") == null?"":(String)request.getParameter("tipo_solicitud");
String icEpo = request.getParameter("epo") == null?"":(String)request.getParameter("epo");
String icIf = request.getParameter("ic_if") == null?"":(String)request.getParameter("ic_if");
String cliente = request.getParameter("cliente") == null?"":(String)request.getParameter("cliente");
String estatus = request.getParameter("estatus") == null?"":(String)request.getParameter("estatus");
String fechaDe = request.getParameter("fecha_autorizacion_de") == null?"":(String)request.getParameter("fecha_autorizacion_de");
String fechaA = request.getParameter("fecha_autorizacion_a") == null?"":(String)request.getParameter("fecha_autorizacion_a");
int  start= 0, limit =0;

InfLinCredNafinDist paginador = new InfLinCredNafinDist();
CatalogoSimple catalogo = new CatalogoSimple();

String infoRegresar = "", consulta = "", totales = "", mensaje = "";
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();
HashMap datos = new HashMap();

	if(fechaDe.equals("De fecha ...")){
		fechaDe = "";
	}
	if(fechaA.equals("A fecha ...")){
		fechaA = "";
	}

if (informacion.equals("catalogoTipoCredito") ) {
	
	consulta="{\"success\": true, \"total\": \"2\", \"registros\": [{\"clave\":\"D\",\"descripcion\":\"Descuento y/o factoraje\"},{\"clave\":\"C\",\"descripcion\":\"Crédito en cuenta corriente\"}]}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();   	

} else if (informacion.equals("catalogoTipoSolicitud") ) {
	
	consulta="{\"success\": true, \"total\": \"3\", \"registros\": [{\"clave\":\"I\",\"descripcion\":\"Inicial\"},{\"clave\":\"A\",\"descripcion\":\"Ampliación\"},{\"clave\":\"R\",\"descripcion\":\"Renovación\"}]}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();   	

} else if (informacion.equals("catalogoEPO") ) {
	
	CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setTipoCredito(tipoCredito); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoIF") ) {

	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveEpo(icEpo); 
	cat.setOrden("cg_razon_social");  
	infoRegresar = cat.getJSONElementos();		

}  else if (informacion.equals("catalogoCliente") ) {

	CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
	if(!icIf.equals("")){
		cat.setCampoClave("cpe.ic_pyme");
		cat.setCampoDescripcion("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social"); 
		cat.setIcIF(icIf); 
		cat.setClaveEpo(icEpo); 
		cat.setTipoCredito("C"); 
		cat.setOrden("CPE.cg_pyme_epo_interno||' '||CP.cg_razon_social");  
	}
	infoRegresar = cat.getJSONElementos();	

} else if (informacion.equals("catalogoEstatus") ) {
	
	catalogo.setTabla("comcat_estatus_linea");
	catalogo.setDistinc(false);
	catalogo.setCampoClave("ic_estatus_linea");
	catalogo.setCampoDescripcion("cd_descripcion");
	if(tipoCredito.equals("D")){
		catalogo.setValoresCondicionIn("3,11", Integer.class);
	} else if(tipoCredito.equals("C")){
		catalogo.setValoresCondicionIn("1,2,3,4,5,6", Integer.class);
	}
	catalogo.setOrden("cd_descripcion");
	infoRegresar = catalogo.getJSONElementos();	

} else if (informacion.equals("consultar") || informacion.equals("consultarTotales") || informacion.equals("imprimir") || informacion.equals("imprimirTodo") || informacion.equals("generarArchivo") ) {
	
	paginador.setTipo_creditoM(tipoCredito);
	paginador.setTipo_solicM(tipoSolicitud);
	paginador.setIc_epoM(icEpo);
	paginador.setIc_ifM(icIf);
	paginador.setIc_pymeM(cliente);
	paginador.setIc_estatus_lineaM(estatus);
	paginador.setFecha_auto_deM(fechaDe);
	paginador.setFecha_auto_aM(fechaA);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if (informacion.equals("consultar") || informacion.equals("consultarTotales") || informacion.equals("imprimir") || informacion.equals("imprimirTodo")){

		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		if(informacion.equals("consultar")) {
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request);
				}	
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				throw new AppException("(Informacion: consultar) Error en la paginacion", e);
			}
			jsonObj = JSONObject.fromObject(consulta);
		} else if(informacion.equals("consultarTotales")){
			try{
				consulta = queryHelper.getJSONResultCount(request); 
				jsonObj = JSONObject.fromObject(consulta);
			} catch(Exception e){
				log.warn("Error en consulta totales: " + e);
			}

		} else if( informacion.equals("imprimirTodo") ) {
			paginador.setTipo_creditoM(tipoCreditoRespaldo);
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				jsonObj.put("success", new Boolean(false));
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
/*
	Fodea 011-2015
	La opción de generar el PDF por paginación queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el método por si se requiere nuevamente 
	Fecha: 23/06/2015
	BY: Agustín Bautista Ruiz
*/
/*
		else if( informacion.equals("imprimir") ) {
			paginador.setTipo_creditoM(tipoCreditoRespaldo);
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
		}
*/
	} else if( informacion.equals("generarArchivo") ) {
		paginador.setTipo_creditoM(tipoCreditoRespaldo);
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>
