Ext.onReady(function() {

	var tipocred = ''; //Fodea 13-2014
	var tipoPago = ''; //Fodea 13-2014
	var recordType = Ext.data.Record.create([
		{name:'clave'},
		{name:'descripcion'}
	]);
	
	var porCobranza = [];
	var montoLinea = [];
	var porGarantia = [];
	var ic_ifs = [];
	var elementos=0;
	
	var limpia		=  function(){
		porCobranza	= [];
		montoLinea	= [];
		porGarantia	= [];
		ic_ifs		= [];
		elementos	= 0;
	}
	
	
	var storeMeses = new Ext.data.SimpleStore({
		fields: ['clave', 'afiliacion'],
		data : [
			['2','2 Meses'],
			['3','3 Meses'],
			['4','4 Meses'],
			['5','5 Meses'],
			['6','6 Meses'],
			['7','7 Meses'],
			['8','8 Meses'],
			['9','9 Meses'],
			['10','10 Meses'],
			['11','11 Meses'],
			['12','12 Meses']			
		]
	});
	
	var validaCampos =  function(registro) {
		
		var total  = 0;
		
		mtoautoriza =  registro.data['MONTO_AUTORIZA'];
		porcgar =  registro.data['PORCENTAJE_GARANTIA'];
		
		if(registro.data['PORCENTAJE_COBRANZA']>100){
			Ext.MessageBox.alert('Mensaje','El porcentaje no puede ser mayor a 100 ');
		}
		if(registro.data['PORCENTAJE_GARANTIA']>100){
			Ext.MessageBox.alert('Mensaje','El porcentaje no puede ser mayor a 100 ');
		}
		if(registro.data['PORCENTAJE_GARANTIA']!=''){
			total = mtoautoriza * (porcgar/100);
			registro.data['MONTO_GARANTIA']=total;
			registro.commit();
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			
			var el = gridConsulta.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({
		root : 'registros',
		url : '24forma4.data.jsp',
		baseParams: {
			informacion: 'Consultar_Seccion3'
		},
		fields: [
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'PORCENTAJE_COBRANZA'},
			{	name: 'MONTO_LINEA'},
			{	name: 'PORCENTAJE_GARANTIA'},
			{	name: 'MONTO_GARANTIA'},
			{	name: 'TIPO_COBRANZA'},
			{	name: 'MONTO_AUTORIZA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: '',
		clicksToEdit: 1,
		columns: [
			
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Porcentaje por cobranza delegada',
				tooltip: 'Porcentaje por cobranza delegada',
				dataIndex: 'PORCENTAJE_COBRANZA',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'center',
					editor: {
					xtype : 'numberfield',
					maxValue : 999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					var accion=registro.data['TIPO_COBRANZA'];
					if(accion=='D' ){
						return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00%'),metadata,registro);
					}else  {
						return 'N/A';
					}
				}
			},
			{
				header: 'Bloqueo de l�nea por monto vencido mayor a', // es es editable
				tooltip: 'Bloqueo de l�nea por monto vencido mayor a',
				dataIndex: 'MONTO_LINEA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(Ext.util.Format.number(value,'$0,0.00'),metadata,registro);
				}
			},
			{
				header: 'Porcentaje de garant�a', // es es editable 
				tooltip: 'Porcentaje de garant�a',
				dataIndex: 'PORCENTAJE_GARANTIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(Ext.util.Format.number(value,'0.00%'),metadata,registro);
				}
			},
			{
				header: 'Monto garant�a l�quida', // es es editable 
				tooltip: 'Monto garant�a l�quida',
				dataIndex: 'MONTO_GARANTIA',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		displayInfo: true,
		emptyMsg: "No hay registros.",
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 700,
		align: 'center',
		frame: false,
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables
				var record = e.record;
				var campo= e.field;
				if(campo == 'PORCENTAJE_COBRANZA'){
					var accion=record.data['TIPO_COBRANZA'];
					if(accion=='D' ){
						return true;
					}else {
						return false;
					}
				}
			},
			afteredit : function(e){
				var record = e.record;
				var campo= e.field;
				if(campo == 'PORCENTAJE_COBRANZA'  ||  campo == 'MONTO_LINEA' ||  campo == 'PORCENTAJE_GARANTIA' ){
				
					validaCampos(record);
				
				}
			}
		}
	});
	
	// ***************** Actualiza Tasa ************
	function procesaActualiza_Tasa(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
			
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje).getDialog().setZIndex(1000000);
	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// *****************Parametros Secci�n 2 **************
	function procesaValoresParametros_Seccion2(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				
				// Proceso de Guardar 
				if(jsonData.accion =='G'){
					
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
						function(){
							fp.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: '24forma4.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar_Seccion2'
								}),
							callback: procesaValoresParametros_Seccion2
							});
						});
						
						
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar_Seccion3'
							})
						});
						
				// consultar 
				}else if(jsonData.accion =='C'){
					
					Ext.getCmp('txdiasmin1').setValue(jsonData.txdiasmin);
					Ext.getCmp('txplazomax1').setValue(jsonData.txplazomax);
					Ext.getCmp('txtDiasFechaVenc1').setValue(jsonData.txtDiasFechaVenc);
					Ext.getCmp('id_rconversion').setValue(jsonData.rconversion);
					Ext.getCmp('id_rcontrol').setValue(jsonData.rcontrol);
					Ext.getCmp('txplazo1').setValue(jsonData.txplazo);
					Ext.getCmp('txporcentaje1').setValue(jsonData.txporcentaje);
					Ext.getCmp('txproxvenc1').setValue(jsonData.txproxvenc);
					Ext.getCmp('id_rad_fecha_porc_desc').setValue(jsonData.rad_fecha_porc_desc);
					Ext.getCmp('id_txtEnvCorreo').setValue(jsonData.txtEnvCorreo);
					Ext.getCmp('id_diasoperacion').setValue(jsonData.diasoperacion);
					Ext.getCmp('nombreEPO').update(jsonData.nombreEPO);
					Ext.getCmp('id_descAdquirienteBanco').setValue(jsonData.descAdquirienteBanco); //Fodea 13-2014.
					Ext.getCmp('id_no_afiliacion_comer').setValue(jsonData.noAfiliacionComer);
					Ext.getCmp('btnGuardar').show();
					tipocred = jsonData.tipocred; //Fodea 13-2014. En la seccion 2 me indica si es modalidad 2 al momento de guardar
					tipoPago = jsonData.tipoPago; //Fodea 13-2014. En la seccion 2 me indica si el tipo de pago es H o TC al momento de guardar

					//INI 14/10/2014
					Ext.getCmp('txdiasmin1').enable();
					Ext.getCmp('txplazomax1').enable();
					Ext.getCmp('btnAcTasa').enable();
					Ext.getCmp('txtDiasFechaVenc1').enable();
					Ext.getCmp('id_rconversion').items.items[0].enable();
					Ext.getCmp('id_rconversion').items.items[1].enable();
					//FIN 14/10/2014

					if( jsonData.tipocred=='D' ||  jsonData.tipocred=='A' ){
						//seccci�n 3
						Ext.getCmp('titulos_5').setVisible(true);
						Ext.getCmp('titulos_6').setVisible(true);
						Ext.getCmp('titulos_7').setVisible(true);
						Ext.getCmp('seccion_16_1').setVisible(false); //Fodea 13-2014.
						Ext.getCmp('seccion_21').setVisible(false);
						
						Ext.getCmp('seccion_16_2').setVisible(false);
						Ext.getCmp('id_no_afiliacion_comer').setValue('');

					} else if(jsonData.tipocred=='C'){  // Modalidad 2 (Riesgo Distribuidor) AGUSTIN

						//Seccion 2. Este componente solo se muestra cuando es modalidad 2
						Ext.getCmp('seccion_21').setVisible(true);
						/***** INICIO Fodea 13-2014 *****/
						if(jsonData.tipoPago == 'H' || jsonData.tipoPago == 'TC'){
							Ext.getCmp('seccion_16_1').setVisible(true);
							Ext.getCmp('seccion_16_2').setVisible(true);
						} else{
							Ext.getCmp('seccion_16_1').setVisible(false);
							Ext.getCmp('seccion_16_2').setVisible(false);
							Ext.getCmp('id_no_afiliacion_comer').setValue('');
						}
						/***** FIN Fodea 13-2014 *****/
						
						//INI 14/10/2014
						if(jsonData.tipoPago == 'TC'){
							Ext.getCmp('txdiasmin1').setValue('');
							Ext.getCmp('txplazomax1').setValue('');
							Ext.getCmp('txtDiasFechaVenc1').setValue('');
							Ext.getCmp('id_rconversion').setValue('');
							Ext.getCmp('txdiasmin1').disable();
							Ext.getCmp('txplazomax1').disable();
							Ext.getCmp('btnAcTasa').disable();
							Ext.getCmp('txtDiasFechaVenc1').disable();
							Ext.getCmp('id_rconversion').items.items[0].disable();
							Ext.getCmp('id_rconversion').items.items[1].disable();
						}
						//FIN 14/10/2014
						
						
						//Fodea 09-2015 
						var id_tipoCred =	(Ext.getCmp('id_tipocred')).getValue();
						var id_tipoPago = (Ext.getCmp('id_tipoPago')).getValue(); 
																		
						
						if( (  jsonData.tipocred=='C' &&    jsonData.tipoPago== 'H' )  || 						 
								 (jsonData.tipocred=='C' &&    jsonData.tipoPago== 'LC' )  
							 ){							 
		
							Ext.getCmp('seccion_13_1').setVisible(true);
							Ext.getCmp('id_mesesIntereses').setValue(jsonData.mesesIntereses);
							Ext.getCmp('cmbMeses1').setValue(jsonData.cmbMeses);
							
							if(jsonData.mesesIntereses=='S') {						
								Ext.getCmp('seccion_13_2').setVisible(true); 
							
							}else  {
								Ext.getCmp('seccion_13_2').setVisible(false);
							}						
						}

					} else{
						//seccci�n 3
						Ext.getCmp('titulos_5').setVisible(false);		
						Ext.getCmp('titulos_6').setVisible(false);	
						Ext.getCmp('titulos_7').setVisible(false);
						Ext.getCmp('seccion_16_1').setVisible(false); //Fodea 13-2014.
						Ext.getCmp('seccion_21').setVisible(false);
						Ext.getCmp('seccion_16_2').setVisible(false);
						Ext.getCmp('id_no_afiliacion_comer').setValue('');
					}
					
					if( jsonData.descuentoAutomatico=='S' ||  jsonData.ventaCartera=='S' ){
						Ext.getCmp('txplazo1').disable();
						Ext.getCmp('txporcentaje1').disable();
					} else{
						Ext.getCmp('txplazo1').enable();
						Ext.getCmp('txporcentaje1').enable();
					}
					
					if( jsonData.tipocred=='C'  && jsonData.descuentoAutomatico=='S' ){
						Ext.getCmp('id_rad_fecha_porc_desc').disable();
					}else  {
						Ext.getCmp('id_rad_fecha_porc_desc').enable();
					}
				}
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// *****************Parametros Secci�n 1 **************

	function procesaValoresParametros_Seccion1(opts, success, response){
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				
				// Proceso de Guardar
				if(jsonData.accion =='G'){
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
					function(){
					
						fp.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: '24forma4.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar_Seccion1'
								}),
							callback: procesaValoresParametros_Seccion1
							});
					});
					
				// consultar 
				} else if(jsonData.accion =='C'){
					
					Ext.getCmp('id_tipocred').setValue(jsonData.tipocred);
					Ext.getCmp('id_firma_mancomunada').setValue(jsonData.firma_mancomunada); //Fodea 32-2014
					Ext.getCmp('id_pgointeres').setValue(jsonData.pgointeres);
					Ext.getCmp('id_ventaCartera').setValue(jsonData.ventaCartera);
					Ext.getCmp('id_respago').setValue(jsonData.respago);
					Ext.getCmp('id_tipocob').setValue(jsonData.tipocob);
					Ext.getCmp('id_pgointeres').setValue(jsonData.pgointeres);
					Ext.getCmp('id_DesAutomaticoEPO').setValue(jsonData.DesAutomaticoEPO);
					Ext.getCmp('id_noNegociables').setValue(jsonData.noNegociables);
					Ext.getCmp('id_lineaCredito').setValue(jsonData.lineaCredito);
					Ext.getCmp('id_DesAutomaticoEPO').setValue(jsonData.DesAutomaticoEPO);
					Ext.getCmp('id_negociableabaja').setValue(jsonData.negociableabaja);
					Ext.getCmp('id_tipoPago').setValue(jsonData.tipoPago); //Fodea 13-2014
                                        
                                        Ext.getCmp('id_operbajocontrato').setValue(jsonData.bajocontrato);
					
					Ext.getCmp('titulos_1').setVisible(true);
					Ext.getCmp('titulos_2').setVisible(true);
					Ext.getCmp('titulos_3').setVisible(false);
					Ext.getCmp('seccion_1').setVisible(true);
					Ext.getCmp('seccion_1_1').setVisible(true); //Fodea 32-2014
					Ext.getCmp('seccion_2').setVisible(false);
					Ext.getCmp('seccion_3').setVisible(false);
					Ext.getCmp('seccion_4').setVisible(false);
					Ext.getCmp('seccion_5').setVisible(false);
					Ext.getCmp('seccion_6').setVisible(false);
					Ext.getCmp('seccion_7').setVisible(false);
					Ext.getCmp('seccion_8').setVisible(false);
					Ext.getCmp('seccion_9').setVisible(false);
					Ext.getCmp('seccion_10').setVisible(false);
					Ext.getCmp('seccion_20').setVisible(true);
					Ext.getCmp('seccion_27').setVisible(true);
                                        
					Ext.getCmp("id_modplazo_2").items.items[0].setValue(false);
					Ext.getCmp("id_modplazo_2").items.items[1].setValue(false);
					Ext.getCmp("id_modplazo_2").items.items[2].setValue(false);
					Ext.getCmp("id_modplazo_2").items.items[3].setValue(false);
				  	
					if( (  jsonData.tipocred=='D'   &&  jsonData.ventaCartera=='N')   ||    jsonData.tipocred=='C'  )  {
						
						Ext.getCmp('id_respago').items.items[1].enable();
						
						Ext.getCmp('id_pgointeres').items.items[0].enable();
						Ext.getCmp('id_pgointeres').items.items[1].enable();
						
						Ext.getCmp('id_modplazo_2').items.items[0].enable();
						Ext.getCmp('id_modplazo_2').items.items[1].enable();
						Ext.getCmp('id_modplazo_2').items.items[2].enable();
						Ext.getCmp('id_modplazo_2').items.items[3].enable();
						
						if(jsonData.modplazo1!='')  {Ext.getCmp("id_modplazo_2").items.items[0].setValue(true);  }
						if(jsonData.modplazo2!='')  {Ext.getCmp("id_modplazo_2").items.items[1].setValue(true);  }
						if(jsonData.modplazo3!='')  {Ext.getCmp("id_modplazo_2").items.items[2].setValue(true);  }
						if(jsonData.modplazo4!='')  {Ext.getCmp("id_modplazo_2").items.items[3].setValue(true);  }
						
						Ext.getCmp('seccion_9').setVisible(true);
						
					}
					
					if(  jsonData.tipocred=='D'  ||    jsonData.tipocred=='C'  )  {
						if(jsonData.DesAutomaticoEPO == 'S'){
							Ext.getCmp('seccion_10').setVisible(true);
						}
					}
							
					if(jsonData.tipocred=='D')  {  // Modalidad 1 (Riesgo Empresa de Primer Orden ) 
						
						Ext.getCmp('seccion_1_2').setVisible(false);	//Fodea 13-2014
						Ext.getCmp('seccion_2').setVisible(true);
						Ext.getCmp('seccion_3').setVisible(true);
						Ext.getCmp('seccion_4').setVisible(true);
						Ext.getCmp('seccion_5').setVisible(true);
						Ext.getCmp('seccion_6').setVisible(false);
						Ext.getCmp('seccion_7').setVisible(true);
						Ext.getCmp('seccion_9').setVisible(true);
						
						if(jsonData.ventaCartera=='S') {
							
							Ext.getCmp('id_respago').items.items[1].disable();
							Ext.getCmp('id_pgointeres').items.items[1].enable();//Ext.getCmp('id_pgointeres').items.items[1].disable();
							Ext.getCmp('id_modplazo_2').items.items[0].disable();
							Ext.getCmp("id_modplazo_2").items.items[1].setValue(true);
							Ext.getCmp('id_modplazo_2').items.items[2].disable();
							Ext.getCmp('id_modplazo_2').items.items[3].disable();
							Ext.getCmp('seccion_8').setVisible(true);		
						}
					
					} else if(jsonData.tipocred=='C')  {  // Modalidad 2 (Riesgo Distribuidor)
						Ext.getCmp('seccion_5').setVisible(true);
						Ext.getCmp('seccion_7').setVisible(true);
						Ext.getCmp('seccion_9').setVisible(true);
						Ext.getCmp('seccion_6').setVisible(false);
						if(jsonData.pgointeres==2) {
							Ext.getCmp('seccion_8').setVisible(true);
						}
						/***** INICIO Fodea 13-2014 *****/
						Ext.getCmp('seccion_1_2').setVisible(true);
						if(jsonData.tipoPago == 'TC'){
							Ext.getCmp('id_modplazo_2').items.items[0].enable();
							Ext.getCmp('id_modplazo_2').items.items[1].enable();
							Ext.getCmp('id_modplazo_2').items.items[2].enable();
							Ext.getCmp('id_modplazo_2').items.items[3].disable();
							//INI 14/10/2014
							Ext.getCmp('id_pgointeres').setValue('');
							Ext.getCmp('id_pgointeres').items.items[0].disable();
							Ext.getCmp('id_pgointeres').items.items[1].disable();
							//FIN 14/10/2014
						}
						/***** FIN Fodea 13-2014 *****/
						
					} else if(jsonData.tipocred=='A'){  // Ambos
						
						Ext.getCmp('seccion_1_2').setVisible(false); //Fodea 13-2014
						Ext.getCmp('seccion_3').setVisible(true);
						Ext.getCmp('seccion_4').setVisible(true);
						Ext.getCmp('seccion_5').setVisible(true);
						Ext.getCmp('seccion_6').setVisible(true);
						Ext.getCmp('seccion_7').setVisible(false);
						Ext.getCmp('seccion_9').setVisible(true);
						Ext.getCmp('id_modplazos2').setValue(jsonData.modplazos2);
						Ext.getCmp('id_pgointeres').setValue(jsonData.pgointeres);
						//Ext.getCmp('id_pgointeres').items.items[1].disable();
											
					}else if(jsonData.tipocred=='')  {  //  vacio 
					
						Ext.getCmp('seccion_1_2').setVisible(false); //Fodea 13-2014
						Ext.getCmp('seccion_5').setVisible(true);
						Ext.getCmp('seccion_6').setVisible(true);
						Ext.getCmp('seccion_9').setVisible(true);
						Ext.getCmp('id_modplazos2').setValue('');
						
					}
					
				}
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//  funci�n para el boton de Guardar
	function procesarGuardar() {
			
		var tipo_credito1 =(Ext.getCmp('tipo_credito1')).getValue();
		var ic_epo1 =	(Ext.getCmp('ic_epo1'));
		
		if(tipo_credito1 ==1 )  {    //Secci�n 1: Esquemas de operaci�n
		
			if(Ext.isEmpty(ic_epo1.getValue())){ 
				ic_epo1.markInvalid('Debe seleccionar una EPO');
				return;
			}
			
			if(Ext.isEmpty(ic_epo1.getValue())){ 
				ic_epo1.markInvalid('Debe seleccionar una EPO');
				return;
			}
			
			var id_tipocred	= (Ext.getCmp('id_tipocred')).getValue();
			var id_respago		= (Ext.getCmp('id_respago')).getValue();
			var id_pgointeres	= (Ext.getCmp('id_pgointeres')).getValue();
			var id_tipoPago	= (Ext.getCmp('id_tipoPago')).getValue(); //Fodea 13-2014
			
			if(id_tipocred ==null){
				//Ext.MessageBox.alert('Mensaje','Debe de seleccionar un Tipo de cr�dito a operar' );
				Ext.MessageBox.alert('Mensaje','Seleccione los parametros correspondientes' );
				return;
			}
			
			/*****INICIO Fodea 20-2014. Se agrega la validaci�n sobre los checkbox y los radiobuttons *****/
			if( id_tipocred.getGroupValue() == 'C'	 || id_tipocred.getGroupValue() == 'D' )  {
				if( (Ext.getCmp('id_modplazo_2').items.items[0].getValue() == false &&
					 Ext.getCmp('id_modplazo_2').items.items[1].getValue() == false  &&
					 Ext.getCmp('id_modplazo_2').items.items[2].getValue() == false &&
					 Ext.getCmp('id_modplazo_2').items.items[3].getValue() == false)){
					Ext.MessageBox.alert('Mensaje','Debe de seleccionar al menos una Modalidad de Plazo.' );
					return;
				}
			}
			/*****FIN Fodea 20-2014 *****/
			
			/***** INICIO Fodea 13-2014 *****/
			if( id_tipocred.getGroupValue() == 'C' && (id_tipoPago == '' || id_tipoPago == null) ){
				Ext.MessageBox.alert('Mensaje','Debe seleccionar al menos un tipo de pago' );
				return;
			}
			/***** FIN Fodea 13-2014 *****/
			
			if( id_tipocred.getGroupValue() == 'A' || id_tipocred.getGroupValue() == 'D'){
				if( id_respago == null ||  id_pgointeres == null  ){
					Ext.MessageBox.alert('Mensaje','Seleccione los parametros correspondientes' );
					return;
				}
			}

			if( id_tipocred.getGroupValue() == 'C' && id_tipoPago.getGroupValue() != 'TC'){
				if(id_pgointeres == null  ){
					Ext.MessageBox.alert('Mensaje','Seleccione los parametros correspondientes' );
					return;
				}
			}

			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '24forma4.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Modidicar_Seccion1'
				}),
				callback: procesaValoresParametros_Seccion1
			});
			
		} else if(tipo_credito1 == 2){ // Secci�n 2: Par�metros

			var txdiasmin1 =	(Ext.getCmp('txdiasmin1'));
			var txplazomax1 =	(Ext.getCmp('txplazomax1'));

			if(!Ext.isEmpty(txdiasmin1.getValue())){
				if(txdiasmin1.getValue()<=0 ){
					txdiasmin1.markInvalid('Los d�as m�nimos tienen que ser mayores a cero');
					return;
				}
			}

			if(!Ext.isEmpty(txplazomax1.getValue())){
				if(txplazomax1.getValue()<=0 ){ 
					txplazomax1.markInvalid('Los d�as m�nimos tienen que ser mayores a cero');
					return;
				}
			}

			if(!Ext.isEmpty(txplazomax1.getValue())){
				if(parseInt(txdiasmin1.getValue()) > parseInt(txplazomax1.getValue())){
					Ext.MessageBox.alert('Mensaje','Los d�as m�nimos no pueden ser mayores al plazo m�ximo');
					return;
				}
			}

			/***** INICIO Fodea 13-2014 *****/
			if(tipocred == 'C' && (tipoPago == 'H' || tipoPago == 'TC')){
				if(Ext.getCmp('id_descAdquirienteBanco').getValue() == '' || Ext.getCmp('id_descAdquirienteBanco').getValue() == 0){
					Ext.MessageBox.alert('Mensaje','No ha capturado la Tasa de descuento de Banco Adquirente Moneda Nacional');
					return;
				}
			}
			/***** FIN Fodea 13-2014 *****/

			if(tipocred == 'C' && (tipoPago != 'LC')){
				if(Ext.getCmp('id_no_afiliacion_comer').getValue() == ''){
					Ext.MessageBox.alert('Mensaje','No ha capturado el N�mero de Afiliaci�n de Comercio.');
					return;
				}
			}

				//F09-201
				if(Ext.getCmp('seccion_13_1').isVisible()){
					var id_mesesIntereses = (Ext.getCmp('id_mesesIntereses')).getValue(); 								
					if( id_mesesIntereses.getGroupValue()=='S' ){				
						if(Ext.getCmp('cmbMeses1').getValue() == ''){
							Ext.MessageBox.alert('Mensaje','Seleccione un Plazo m�ximo de financiamiento para operaciones a meses sin intereses ');
							return;
						}
					}
				}


			var  gridConsulta = Ext.getCmp('gridConsulta');
			var store = gridConsulta.getStore();
			limpia(); //limpia las variables
			
			store.each(function(record) {
				porCobranza.push(record.data['PORCENTAJE_COBRANZA']);
				montoLinea.push(record.data['MONTO_LINEA']);
				porGarantia.push(record.data['PORCENTAJE_GARANTIA']);
				ic_ifs.push(record.data['IC_IF']);
				elementos++;
			});
			
			fp.el.mask('Procesando...', 'x-mask-loading');	
			Ext.Ajax.request({
				url: '24forma4.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:	'Modidicar_Seccion2',
					porCobranza:	porCobranza,
					montoLinea:		montoLinea,
					porGarantia:	porGarantia,
					ic_ifs:			ic_ifs,
					tipoPago:		tipoPago, //Fodea 13-2014
					tipocred:		tipocred, //Fodea 13-2014
					elementos:		elementos 
				}),
				callback: procesaValoresParametros_Seccion2
			});
		}
	}
	
	// Funcion para la Ayuda 	
	function mostrarAyuda(parametro){
		var titulo = '';
		var descripcion =[];
		
		if(parametro==1) {
			titulo = "Plazo m�nimo del financiamiento";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Plazo comercial m�nimo que le otorgue la EPO a la pyme cliente o distribuidor. (A)'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		}else  if(parametro==2) {
			titulo = "Plazo m�ximo del financiamiento";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Plazo total considerando el plazo comercial m�nimo m�s el plazo adicional que en su caso se le otorgue a la pyme con el financiamiento (C=A+B). '  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		}else  if(parametro==3) {
			titulo = "D�as M�ximos para Ampliar la Fecha de Vencimiento";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Plazo adicional que el distribuidor o cliente puede tomar posterior al vencimiento del plazo comercial (B). '  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		}else  if(parametro==4) {
			titulo = "Tipo de conversi�n";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Establece si existe conversi�n del tipo de cambio peso-d�lar. '  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		}else  if(parametro==5) {
			titulo = "Operar con pantalla de cifras de control";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Activar pantalla en la cual el sistema valida las cifras globales con los documentos publicados. '  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
						
		}else  if(parametro==6) {
			titulo = "Plazo m�ximo para tomar el descuento, Porcentaje de descuento que otorga la EPO";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Establece en la modalidad de pronto pago el par�metro general de plazo y porcentaje para descuento.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];		
		
		}else  if(parametro==7) {
			titulo = "Aviso de pr�ximos vencimientos en:";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Establece el(os) d�as para que se notifiquen los avisos de pr�ximos vencimientos.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];				
		
		}else  if(parametro==8) {
			titulo = "Fecha para porcentaje de descuento:";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Define la fecha a partir de la cual el sistema calcula el porcentaje de descuento para la modalidad de pronto pago. '  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];	
		
		} else  if(parametro==9) {
			titulo = "Env�o de Correo Electr�nico para Aceptaci�n de Documentos";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Se establece para el env�o de correo electr�nico a la pyme cliente o distribuidor cuando la operaci�n se realiza por descuento autom�tico, ejemplo: ABC-CEMEX.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];			
		} else  if(parametro==10) {
			titulo = "D�as h�biles de Operaci�n Distribuidor";		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Indica que los clientes y/o distribuidores solo puedan ver y en su caso seleccionar para financiamiento los documentos que vencen dentro de los pr�ximos d�as h�blies ingresados'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];			
		} else  if(parametro==11) { /*****Fodea 13-2014 *****/
			titulo = 'Tasa de descuento de Banco Adquirente Moneda Nacional';		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR> Tasa negociada del Banco Adquirente con la Cadena Productiva para operaciones que se realicen con Tarjetas de Cr�dito.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];		
		
		} else if(parametro==12){

			titulo = 'N�mero de Afiliaci�n de Comercio';		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>El campo N�mero de Afiliaci�n de Comercio es el denominado StoreID del contrato de Servicio de Adquirencia de NetPay.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];		
		
		} else if(parametro==13){

			titulo = 'Meses sin intereses';		
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Establece si la EPO puede publicar documentos financiables a meses sin intereses'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}

		new Ext.Window({
			width:			520,
			id:				'ventanaAyuda',
			closeAction:	'hide',
			modal:			true,
			autoHeight:		true,
			resizable:		false,
			constrain:		true,
			closable:		false,	
			autoScroll:		true,
			html: descripcion.join(''),
			bbar: {
				xtype: 'toolbar',	buttonAlign:'center',
				buttons: ['->',
					{	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('ventanaAyuda').destroy();} },
					'-'
				]
			}
			}).show().setTitle(titulo);
	}
			
		
		
	// *******************Criterios de Captura ***********************
		
	var procesarCatalogoEPO = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_epo1 = Ext.getCmp('ic_epo1');
			Ext.getCmp('ic_epo1').setValue("");
			if(ic_epo1.getValue()==""){
				var newRecord = new recordType({
					clave:"",
					descripcion:"Seleccionar EPO"
				});
				store.insert(0,newRecord);
				store.commitChanges();
				ic_epo1.setValue("");
			}
		}
  };
  
	var catTipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Secci�n 1: Esquemas de operaci�n'],
			['2','Secci�n 2: Par�metros']
		 ]
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma4.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
		load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [
		{
			width:			400,
			xtype:			'combo',
			name:				'tipo_credito',
			id:				'tipo_credito1',
			fieldLabel: 	'&nbsp;&nbsp;Secci�n',
			mode:				'local',
			hiddenName:		'tipo_credito',
			emptyText:		'Seleccione ... ',
			triggerAction:	'all',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			value:			1,
			minChars:		1,
			store:			catTipoCreditoData,
			listeners: {
				select: {
					fn: function(combo) {
					
						var ic_epo1 =	(Ext.getCmp('ic_epo1'));
						
						//seccci�n 2 
						Ext.getCmp('titulos_3').setVisible(false);
						Ext.getCmp('titulos_4').setVisible(false);
						Ext.getCmp('seccion_11').setVisible(false);
						Ext.getCmp('seccion_12').setVisible(false);
						Ext.getCmp('seccion_13').setVisible(false);
						Ext.getCmp('seccion_14').setVisible(false);
						Ext.getCmp('seccion_15').setVisible(false);
						Ext.getCmp('seccion_16').setVisible(false);
						Ext.getCmp('seccion_16_1').setVisible(false); //Fodea 13-2014
						Ext.getCmp('seccion_16_2').setVisible(false);
						Ext.getCmp('seccion_17').setVisible(false);
						Ext.getCmp('seccion_18').setVisible(false);
						Ext.getCmp('seccion_19').setVisible(false);
						Ext.getCmp('seccion_21').setVisible(false);
						//seccci�n 3
						Ext.getCmp('titulos_5').setVisible(false);
						Ext.getCmp('titulos_6').setVisible(false);
						Ext.getCmp('titulos_7').setVisible(false);
						Ext.getCmp('seccion_13_1').setVisible(false);
						Ext.getCmp('seccion_13_2').setVisible(false); 
						
						if(combo.getValue()==1)  { 
						
							if(ic_epo1.getValue()!=''){
							
								fp.el.mask('Procesando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '24forma4.data.jsp',
									params: {
										informacion: 'Consultar_Seccion1',
										ic_epo:ic_epo1.getValue()
									},
									callback: procesaValoresParametros_Seccion1
								});
							
							} else{
								
								Ext.getCmp('titulos_1').setVisible(true);
								Ext.getCmp('titulos_2').setVisible(true);
								Ext.getCmp('titulos_3').setVisible(false);
								Ext.getCmp('seccion_1').setVisible(true);
								Ext.getCmp('seccion_1_1').setVisible(true); //Fodea 32-2014
								Ext.getCmp('seccion_3').setVisible(true);
								Ext.getCmp('seccion_4').setVisible(true);
								Ext.getCmp('seccion_5').setVisible(true);
								Ext.getCmp('seccion_6').setVisible(true);
								Ext.getCmp('seccion_20').setVisible(true);
                                                                Ext.getCmp('seccion_27').setVisible(true);
							}
							
						} else if(combo.getValue()==2){ 
							
							if(ic_epo1.getValue()!=''){
							
								fp.el.mask('Procesando...', 'x-mask-loading');	
								Ext.Ajax.request({
									url: '24forma4.data.jsp',
									params: {
										informacion: "Consultar_Seccion2",
										ic_epo:ic_epo1.getValue()
									},
									callback: procesaValoresParametros_Seccion2
								});
								
								
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'Consultar_Seccion3'
									})
								});
						
							}
								
								Ext.getCmp('titulos_1').setVisible(false);
								Ext.getCmp('titulos_2').setVisible(false);
								Ext.getCmp('seccion_1').setVisible(false);
								Ext.getCmp('seccion_1_1').setVisible(false); //Fodea 32-2014
								Ext.getCmp('seccion_1_2').setVisible(false);
								Ext.getCmp('seccion_2').setVisible(false);
								Ext.getCmp('seccion_3').setVisible(false);
								Ext.getCmp('seccion_4').setVisible(false);
								Ext.getCmp('seccion_5').setVisible(false);
								Ext.getCmp('seccion_6').setVisible(false);
								Ext.getCmp('seccion_7').setVisible(false);
								Ext.getCmp('seccion_8').setVisible(false);
								Ext.getCmp('seccion_9').setVisible(false);
								Ext.getCmp('seccion_10').setVisible(false);
								Ext.getCmp('seccion_20').setVisible(false);
								Ext.getCmp('seccion_27').setVisible(false);
                                                                Ext.getCmp('btnGuardar').hide();
								
								//seccci�n 2 
								Ext.getCmp('titulos_3').setVisible(true);
								Ext.getCmp('titulos_4').setVisible(true);
								Ext.getCmp('seccion_11').setVisible(true);
								Ext.getCmp('seccion_12').setVisible(true);
								Ext.getCmp('seccion_13').setVisible(true);
								Ext.getCmp('seccion_14').setVisible(true);
								Ext.getCmp('seccion_15').setVisible(true);
								Ext.getCmp('seccion_16').setVisible(true);
								Ext.getCmp('seccion_16_1').setVisible(true); // Fodea 13-2014
								Ext.getCmp('seccion_16_2').setVisible(false); // Fodea 13-2014
								Ext.getCmp('seccion_17').setVisible(true);
								Ext.getCmp('seccion_18').setVisible(true);
								Ext.getCmp('seccion_19').setVisible(true);
								//Ext.getCmp('seccion_21').setVisible(true);
								
								//seccci�n 3
								Ext.getCmp('titulos_5').setVisible(false);
								Ext.getCmp('titulos_6').setVisible(false);
								Ext.getCmp('titulos_7').setVisible(false);
							  
						}
					}
				}
			}
		},
		{
			xtype: 'combo',	
			name: 'ic_epo',	
			id: 'ic_epo1',	
			fieldLabel: '&nbsp;&nbsp;EPO', 
			mode: 'local',	
			hiddenName : 'ic_epo',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogoEPO,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,			
			listeners: {
				select: {
					fn: function(combo) {
					
						if (combo.getValue() != ''){
							
							var tipo_credito1 =	(Ext.getCmp('tipo_credito1'));
							
							if(tipo_credito1.getValue() ==1 )  {	
							
								fp.el.mask('Procesando...', 'x-mask-loading');	
								Ext.Ajax.request({
									url: '24forma4.data.jsp',
									params: {
										informacion: "Consultar_Seccion1",
										ic_epo:combo.getValue()									
									},
									callback: procesaValoresParametros_Seccion1
								});	
								
							} else{
							
								fp.el.mask('Procesando...', 'x-mask-loading');	
								Ext.Ajax.request({
									url: '24forma4.data.jsp',
									params: {
										informacion: "Consultar_Seccion2",
										ic_epo:combo.getValue()
									},
									callback: procesaValoresParametros_Seccion2
								});
							
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'Consultar_Seccion3'
									})
								});
								
								Ext.getCmp('titulos_1').setVisible(false);
								Ext.getCmp('titulos_2').setVisible(false);
								Ext.getCmp('seccion_1').setVisible(false);
								Ext.getCmp('seccion_2').setVisible(false);
								Ext.getCmp('seccion_3').setVisible(false);
								Ext.getCmp('seccion_4').setVisible(false);
								Ext.getCmp('seccion_5').setVisible(false);
								Ext.getCmp('seccion_6').setVisible(false);
								Ext.getCmp('seccion_7').setVisible(false);
								Ext.getCmp('seccion_8').setVisible(false);
								Ext.getCmp('seccion_9').setVisible(false);
								Ext.getCmp('seccion_10').setVisible(false);
								Ext.getCmp('btnGuardar').hide();
								
								//seccci�n 2 
								Ext.getCmp('titulos_3').setVisible(true);
								Ext.getCmp('titulos_4').setVisible(true);
								Ext.getCmp('seccion_11').setVisible(true);
								Ext.getCmp('seccion_12').setVisible(true);
								Ext.getCmp('seccion_13').setVisible(true);
								Ext.getCmp('seccion_14').setVisible(true);
								Ext.getCmp('seccion_15').setVisible(true);
								Ext.getCmp('seccion_16').setVisible(true);
								Ext.getCmp('seccion_16_1').setVisible(true); // Fodea 13-2014
								Ext.getCmp('seccion_16_2').setVisible(false);
								Ext.getCmp('seccion_17').setVisible(true);
								Ext.getCmp('seccion_18').setVisible(true);
								Ext.getCmp('seccion_19').setVisible(true);
								
								//seccci�n 3
								Ext.getCmp('titulos_5').setVisible(true);
								Ext.getCmp('titulos_6').setVisible(true);
								Ext.getCmp('titulos_7').setVisible(true);
															
							}
						
						}else  {
							window.location = '24forma4ext.jsp';	
						}
					}
				}
			}
		},						
		//*****************Secci�n 1: Esquemas de operaci�n ********************
		{			
			xtype:'panel',	 id: 'titulos_1',  layout:'table',	width:700,	layoutConfig:{   columns: 3 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:700,frame:true,	colspan:'3',	border:true,	html:'<div align="center"><b>Secci�n 1: Esquemas de operaci�n</b	><br>&nbsp;</div>'	}
			]
		},		
		{			
			xtype:'panel',  id: 'titulos_2',  	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Nombre del par�metro <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Tipo de dato <br><br>&nbsp;</div>'	},
				{	width:300,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Valores<br><br>&nbsp;</div>'	}							
			]
		},
		{			
			xtype:'panel',  id: 'seccion_1', 	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de cr�dito a operar <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_tipocred',	name:'tipocred',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Modalidad 1 (Riesgo Empresa de Primer Orden )',	name : 'tipocred', inputValue: 'D' },
						{	boxLabel	: 'Modalidad 2 (Riesgo Distribuidor)',					name : 'tipocred', inputValue: 'C' }, 
						{	boxLabel	: 'Ambos',														name : 'tipocred', inputValue: 'A' },
						{	boxLabel	: 'Factoraje con recurso Propio' ,						name : 'tipocred', inputValue: 'F' }
					],
					listeners	: {
						change:{
							fn:function(){
							
								var id_tipocred		= (Ext.getCmp('id_tipocred')).getValue();
								var id_ventaCartera	= (Ext.getCmp('id_ventaCartera')).getValue();
								var id_tipocob			= (Ext.getCmp('id_tipocob')).getValue();
								var id_noNegociables	= (Ext.getCmp('id_noNegociables')).getValue();	
								var id_pgointeres		= (Ext.getCmp('id_pgointeres')).getValue();	
								var id_respago			= (Ext.getCmp('id_respago')).getValue();	
								
								if(id_tipocred.getGroupValue() == 'D'){
								
									/***** INICIO Fodea 13-2014 *****/
									Ext.getCmp('id_modplazo_2').items.items[0].enable();
									Ext.getCmp('id_modplazo_2').items.items[1].enable();
									Ext.getCmp('id_modplazo_2').items.items[2].enable();
									Ext.getCmp('id_modplazo_2').items.items[3].enable();
									Ext.getCmp('seccion_1_2').setVisible(false);
									/***** FIN Fodea 13-2014 *****/
									
									Ext.getCmp('seccion_2').setVisible(true);
									Ext.getCmp('seccion_3').setVisible(true);
									Ext.getCmp('seccion_4').setVisible(true);
									Ext.getCmp('seccion_5').setVisible(true);
									Ext.getCmp('seccion_6').setVisible(false);
									Ext.getCmp('seccion_7').setVisible(true);
									Ext.getCmp('seccion_8').setVisible(false);
									Ext.getCmp('seccion_9').setVisible(true);
									
									/***** INICIO Fodea 20-2014 *****/
									Ext.getCmp('id_modplazos2').items.items[0].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[1].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[2].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[3].setValue(false);
									/***** FIN Fodea 20-2014 *****/
																	
									if ( id_ventaCartera ==null ) {   					     Ext.getCmp('id_ventaCartera').setValue('N'); 										
									} else  if(id_ventaCartera.getGroupValue()=='N' ) {   Ext.getCmp('id_ventaCartera').setValue('N'); 	}
									
									if ( id_tipocob ==null ) {                            Ext.getCmp('id_tipocob').setValue('D');   
									} else  if(id_tipocob.getGroupValue()=='D') {       	Ext.getCmp('id_tipocob').setValue('D');		}
																	
									if ( id_noNegociables ==null ) {                      Ext.getCmp('id_noNegociables').setValue('N');	
									} else  if(id_noNegociables.getGroupValue()=='N' ) {	Ext.getCmp('id_noNegociables').setValue('N');	   }								
																					
								
									if ( id_respago ==null ) {                      Ext.getCmp('id_respago').setValue('E');	
									}else  if(id_respago.getGroupValue()=='E' ) {	Ext.getCmp('id_respago').setValue('E');	   }								
									
									//INI 14/10/2014
									Ext.getCmp('id_pgointeres').items.items[0].enable();
									Ext.getCmp('id_pgointeres').items.items[1].enable();
									//FIN 14/10/2014

									Ext.getCmp('id_mesesIntereses').setValue('');
									Ext.getCmp('cmbMeses1').setValue(''); 
									
								} else if(id_tipocred.getGroupValue() == 'C'){
								
									Ext.getCmp('seccion_2').setVisible(false);	
									Ext.getCmp('seccion_3').setVisible(false);	
									Ext.getCmp('seccion_4').setVisible(false);
									Ext.getCmp('seccion_5').setVisible(true);
									Ext.getCmp('seccion_6').setVisible(false);
									Ext.getCmp('seccion_7').setVisible(true);
									Ext.getCmp('seccion_8').setVisible(false);	
									Ext.getCmp('seccion_9').setVisible(true);										
									Ext.getCmp('seccion_10').setVisible(false);
									Ext.getCmp('id_pgointeres').items.items[0].enable(); //14/10/2014
									Ext.getCmp('id_pgointeres').items.items[1].enable(); //14/10/2014
									
									/***** INICIO Fodea 13-2014 *****/
									Ext.getCmp('seccion_1_2').setVisible(true);
									
									var id_tipoPago =	(Ext.getCmp('id_tipoPago')).getValue();
									// Se supone que todas las EPOs ya vienen por default con LC,
									// pero por si las dudas cuando es nulo le asigna el valor LC
									if(id_tipoPago == null){
										Ext.getCmp('id_tipoPago').setValue('LC');
									}
									if( id_tipoPago.getGroupValue() == 'TC' ){
										Ext.getCmp('id_modplazo_2').items.items[0].enable();
										Ext.getCmp('id_modplazo_2').items.items[1].enable();
										Ext.getCmp('id_modplazo_2').items.items[2].enable();
										Ext.getCmp('id_modplazo_2').items.items[3].disable();
										Ext.getCmp('id_pgointeres').items.items[0].disable(); //14/10/2014
										Ext.getCmp('id_pgointeres').items.items[1].disable(); //14/10/2014
										Ext.getCmp('id_pgointeres').setValue(''); //14/10/2014
									} else {	
										Ext.getCmp('id_modplazo_2').items.items[0].enable();
										Ext.getCmp('id_modplazo_2').items.items[1].enable();
										Ext.getCmp('id_modplazo_2').items.items[2].enable();
										Ext.getCmp('id_modplazo_2').items.items[3].enable();
										Ext.getCmp('id_pgointeres').items.items[0].enable(); //14/10/2014
										Ext.getCmp('id_pgointeres').items.items[1].enable(); //14/10/2014
									}
									/***** FIN Fodea 13-2014 *****/
									
									/***** INICIO Fodea 20-2014 *****/
									Ext.getCmp('id_modplazos2').items.items[0].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[1].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[2].setValue(false);
									Ext.getCmp('id_modplazos2').items.items[3].setValue(false);
									/***** FIN Fodea 20-2014 *****/								
									
									//INI 14/10/2014
									if(id_tipoPago == null || id_tipoPago.getGroupValue() != 'TC'){
										if ( id_pgointeres == null ){
											Ext.getCmp('id_pgointeres').setValue('1');
										} else if(id_pgointeres.getGroupValue()=='1'){
											Ext.getCmp('id_pgointeres').setValue('1');
										} 
									}
									//FIN 14/10/2014

									if ( id_pgointeres != null ){
										if(id_pgointeres.getGroupValue()=='2' ) {
											Ext.getCmp('seccion_8').setVisible(true);
										}
									}		

								}else if(id_tipocred.getGroupValue() == 'A'   ){
									
									Ext.getCmp('seccion_1_2').setVisible(false); // Fodea 13-2014
									Ext.getCmp('seccion_2').setVisible(false);
									Ext.getCmp('seccion_3').setVisible(true);
									Ext.getCmp('seccion_4').setVisible(true);
									Ext.getCmp('seccion_5').setVisible(true);
									Ext.getCmp('seccion_6').setVisible(true);
									Ext.getCmp('seccion_7').setVisible(false); // Fodea 13-2014
									Ext.getCmp('seccion_9').setVisible(true);
									Ext.getCmp('seccion_10').setVisible(false);

									/***** INICIO Fodea 20-2014 *****/
									Ext.getCmp("id_modplazo_2").items.items[0].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[1].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[2].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[3].setValue(false);
									/***** FIN Fodea 20-2014 *****/
									
									//INI 14/10/2014
									Ext.getCmp('id_pgointeres').items.items[0].enable();
									Ext.getCmp('id_pgointeres').items.items[1].enable();
									//FIN 14/10/2014
									//Ext.getCmp('id_pgointeres').setValue('');
									Ext.getCmp('id_tipocob').setValue('D');
									
									Ext.getCmp('id_mesesIntereses').setValue('');
									Ext.getCmp('cmbMeses1').setValue('');
								
								}else if(id_tipocred.getGroupValue() == 'F'   ){
								
									Ext.getCmp('seccion_1_2').setVisible(false); // Fodea 13-2014
									Ext.getCmp('seccion_2').setVisible(false);	
									Ext.getCmp('seccion_3').setVisible(false);	
									Ext.getCmp('seccion_4').setVisible(false);
									Ext.getCmp('seccion_5').setVisible(false);	
									Ext.getCmp('seccion_6').setVisible(false);					
									Ext.getCmp('seccion_7').setVisible(false);
									Ext.getCmp('seccion_8').setVisible(false);
									Ext.getCmp('seccion_9').setVisible(false);
									Ext.getCmp('seccion_10').setVisible(false);
									
									/***** INICIO Fodea 20-2014 *****/
									Ext.getCmp("id_modplazo_2").items.items[0].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[1].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[2].setValue(false);
									Ext.getCmp("id_modplazo_2").items.items[3].setValue(false);
									/***** FIN Fodea 20-2014 *****/									
									
									Ext.getCmp('id_mesesIntereses').setValue('');
									Ext.getCmp('cmbMeses1').setValue('');

								}								
							}
						}
					}
				}				
			]
		},
		{ /***** INICIO Fodea 32-2014 *****/
			xtype:'panel',  id:'seccion_1_1', layout:'table', width:700, layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{ width:200, frame:false, border:false, html:'<div class="formas" align="left"><br> Publicaci�n de Firma Mancomunada <br><br>&nbsp;</div>' },
				{ width:150, frame:false, border:false, html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'},
				{ width:350, xtype:'radiogroup', style: { width:'80%', marginLeft: '10px' }, align:'center', id:'id_firma_mancomunada', name:'firma_mancomunada', cls:'x-check-group-alt', columns:[300, 300, 300],
					items:
					[
						{ boxLabel:'SI', name:'firma_mancomunada', inputValue: 'S' },
						{ boxLabel:'NO', name:'firma_mancomunada', inputValue: 'N' }
					]
				}
			]
		}, /***** FIN Fodea 32-2014 *****/
		{ /***** INICIO Fodea 13-2014 *****/
			xtype:'panel',  id: 'seccion_1_2', hidden: false, layout:'table', width:700, layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de pago <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'					},
				{	width:350,	xtype:'radiogroup', style: { width: '80%', marginLeft: '10px'  }, align:'center', id:'id_tipoPago', name:'tipoPago', cls:'x-check-group-alt', columns:[300, 300, 300],
				items:
					[  
						{	boxLabel: 'H�brido (Con TC y/o L�nea de Cr�dito)',	name: 'tipoPago', inputValue: 'H'  },
						{	boxLabel: 'Tarjeta de Cr�dito',							name: 'tipoPago', inputValue: 'TC' },
						{	boxLabel: 'L�nea de Cr�dito',								name: 'tipoPago', inputValue: 'LC' }
					],
					listeners: {
						change:{
							fn:function(){
							
								var id_tipoCred =	(Ext.getCmp('id_tipocred')).getValue();
								var id_tipoPago = (Ext.getCmp('id_tipoPago')).getValue(); 
								
								if( id_tipoCred.getGroupValue()=='C' && id_tipoPago.getGroupValue()=='H' ){
									
									Ext.getCmp('id_modplazo_2').items.items[0].enable();
									Ext.getCmp('id_modplazo_2').items.items[1].enable();
									Ext.getCmp('id_modplazo_2').items.items[2].enable();
									Ext.getCmp('id_modplazo_2').items.items[3].enable();

								} else if( id_tipoCred.getGroupValue()=='C' &&  id_tipoPago.getGroupValue()=='TC' ){
									
									Ext.getCmp('id_modplazo_2').items.items[0].enable();
									Ext.getCmp('id_modplazo_2').items.items[1].enable();
									Ext.getCmp('id_modplazo_2').items.items[2].enable();
									Ext.getCmp('id_modplazo_2').items.items[3].disable();
									
									Ext.getCmp('id_modplazo_2').items.items[1].setValue(false);
									Ext.getCmp('id_modplazo_2').items.items[2].setValue(false);
									Ext.getCmp('id_modplazo_2').items.items[3].setValue(false);

								} else if( id_tipoCred.getGroupValue()=='C' &&  id_tipoPago.getGroupValue()=='LC' ){
									
									Ext.getCmp('id_modplazo_2').items.items[0].enable();
									Ext.getCmp('id_modplazo_2').items.items[1].enable();
									Ext.getCmp('id_modplazo_2').items.items[2].enable();
									Ext.getCmp('id_modplazo_2').items.items[3].enable();
									
								}
								//INI 14/10/2014
								if(id_tipoPago.getGroupValue()=='TC'){
									Ext.getCmp('id_pgointeres').items.items[0].disable();
									Ext.getCmp('id_pgointeres').items.items[1].disable();
									Ext.getCmp('id_pgointeres').setValue('');
								} else{
									Ext.getCmp('id_pgointeres').items.items[0].enable();
									Ext.getCmp('id_pgointeres').items.items[1].enable();
								}
								//FIN 14/10/2014
								
								if( id_tipoCred.getGroupValue()!='C' ||  id_tipoPago.getGroupValue()!='H' ){
									Ext.getCmp('id_mesesIntereses').setValue('N');
									Ext.getCmp('cmbMeses1').setValue(''); 								
								}
								
								
								
							}
						}
					}
				}
			]
		}, /***** FIN Fodea 13-2014 *****/
		{		
			xtype:'panel',  id: 'seccion_2', hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Venta de Cartera <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_ventaCartera',	name:'ventaCartera',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'ventaCartera', inputValue: 'S' },
						{	boxLabel	: 'NO ',	name : 'ventaCartera',  inputValue: 'N' }						
					],
					listeners	: {
						change:{
							fn:function(){
							
								var id_ventaCartera =	(Ext.getCmp('id_ventaCartera')).getValue();							
								var id_respago =	(Ext.getCmp('id_respago')).getValue();	
								var id_pgointeres =	(Ext.getCmp('id_pgointeres')).getValue();	
								var id_DesAutomaticoEPO =	(Ext.getCmp('id_DesAutomaticoEPO')).getValue();	
								var id_tipocred =	(Ext.getCmp('id_tipocred')).getValue();
							
								if(id_tipocred.getGroupValue()=='D')  { 
									
									if(id_ventaCartera.getGroupValue() == 'S' ){	
									
										if(id_respago.getGroupValue()=='E') { 
											Ext.getCmp('id_respago').setValue('E');
											Ext.getCmp('id_respago').items.items[1].disable();		
										}
										
										
										Ext.getCmp('id_pgointeres').setValue('1');
										
										Ext.getCmp('id_pgointeres').items.items[1].enable();//Ext.getCmp('id_pgointeres').items.items[1].disable();	
																		
										Ext.getCmp('seccion_7').setVisible(true);
										Ext.getCmp('id_modplazo_2').items.items[0].disable();																		
										Ext.getCmp('id_modplazo_2').items.items[2].disable();
										Ext.getCmp('id_modplazo_2').items.items[3].disable();									
										Ext.getCmp("id_modplazo_2").items.items[0].setValue(false); 
										Ext.getCmp("id_modplazo_2").items.items[1].setValue(true); 
										Ext.getCmp("id_modplazo_2").items.items[2].setValue(false); 
										Ext.getCmp("id_modplazo_2").items.items[3].setValue(false); 
										
										Ext.getCmp('seccion_8').setVisible(true);
										if ( id_DesAutomaticoEPO ==null ) {     Ext.getCmp('id_DesAutomaticoEPO').setValue('N'); 
										}else 	if(id_DesAutomaticoEPO.getGroupValue()=='N') {   Ext.getCmp('id_DesAutomaticoEPO').setValue('N');	  }						
										
																	
									} else  if(id_ventaCartera.getGroupValue() == 'N' ){		
									
										Ext.getCmp('id_respago').items.items[1].enable();
										Ext.getCmp('id_pgointeres').items.items[1].enable();										
										Ext.getCmp('id_modplazo_2').items.items[0].enable();									
										Ext.getCmp("id_modplazo_2").items.items[1].enable(); 									
										Ext.getCmp('id_modplazo_2').items.items[2].enable();
										Ext.getCmp('id_modplazo_2').items.items[3].enable();
										Ext.getCmp('seccion_8').setVisible(false);
										Ext.getCmp('id_DesAutomaticoEPO').setValue('N');
										if(Ext.getCmp("id_modplazo_2").items.items[0].getValue() ==true) { Ext.getCmp("id_modplazo_2").items.items[0].setValue(true);  }
										if(Ext.getCmp("id_modplazo_2").items.items[1].getValue() ==true) { Ext.getCmp("id_modplazo_2").items.items[1].setValue(true);  }
										if(Ext.getCmp("id_modplazo_2").items.items[2].getValue() ==true) { Ext.getCmp("id_modplazo_2").items.items[2].setValue(true);  }
										if(Ext.getCmp("id_modplazo_2").items.items[3].getValue() ==true) { Ext.getCmp("id_modplazo_2").items.items[3].setValue(true);  }
																			
									}
								
								}
							}
						}
					}
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_3', 	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Responsabilidad de pago de inter�s (S�lo si opera DM) <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_respago',	name:'respago',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'EPO',	name : 'respago', inputValue: 'E' },
						{	boxLabel	: 'Distribuidor ',				name : 'respago',  inputValue: 'D' }						
					]	
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_4', 	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de cobranza (S�lo si opera DM) <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_tipocob',	name:'tipocob',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Delegada',	name : 'tipocob', inputValue: 'D' },
						{	boxLabel	: 'No delegada', name : 'tipocob',  inputValue: 'N' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_5', 	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Esquema de pago de intereses <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_pgointeres',	name:'pgointeres',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Anticipado',	name : 'pgointeres', inputValue: '1' },
						{	boxLabel	: 'Al vencimiento', name : 'pgointeres',  inputValue: '2' }						
					],
					listeners	: {
						change:{
							fn:function(){			
							
								var id_pgointeres =	(Ext.getCmp('id_pgointeres')).getValue();	
								var id_DesAutomaticoEPO =	(Ext.getCmp('id_DesAutomaticoEPO')).getValue();	
								var id_tipocred =	(Ext.getCmp('id_tipocred')).getValue();	
									
									
								if(id_tipocred.getGroupValue() =='C' ){
									//INI 14/10/2014
									if(id_pgointeres != null){
										if(id_pgointeres.getGroupValue() == 2){
											Ext.getCmp('seccion_8').setVisible(true);
											if (id_DesAutomaticoEPO == null) { Ext.getCmp('id_DesAutomaticoEPO').setValue('N'); }
											else if ( id_DesAutomaticoEPO.getGroupValue() =='N' ) { Ext.getCmp('id_DesAutomaticoEPO').setValue('N'); }
										} else if(id_pgointeres.getGroupValue() == 1 ){
											Ext.getCmp('seccion_8').setVisible(false);
											Ext.getCmp('id_DesAutomaticoEPO').setValue('N');
										}
									}
									//FIN 14/10/2014
								}
								
							}
						}
					}
					
				}				
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_6', 	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Modalidad de Plazo <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_modplazos2',	name:'modplazos2',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel: 'Pronto Pago',				name: 'modplazos2',	inputValue: '1' },
						{	boxLabel: 'Plazo ampliado',			name: 'modplazos2',	inputValue: '2' },
						{	boxLabel: 'Plazo mixto',				name: 'modplazos2',	inputValue: '3' },
						{	boxLabel: 'Plazo ampliado PEMEX ',	name: 'modplazos2',	inputValue: '5' }						
					]					
				}				
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_7', hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Modalidad de Plazo <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{
					xtype: 'fieldset',
					width:500,
					border	: false,	
					labelWidth			: 30,
					defaultType: 'checkbox', 
					style: {  width: '80%', 	marginLeft: '10px'  },
					align:'left',
					id:'id_modplazo_2',	name:'modplazo_2',					
						items: [ 
						{
							 fieldLabel: '',
							 boxLabel: 'Pronto Pago',
							 name: 'modplazo1',
							 width:300,
							 align:'left',
							 value:'1'
						}, 
						{
							 fieldLabel: '',
							 labelSeparator: '',
							 boxLabel: 'Plazo ampliado',
							 name: 'modplazo2',
							 width:300,
							 align:'left',
							 value:'2'
						}, 
						{							 
							fieldLabel: '',
							labelSeparator: '',
							boxLabel: 'Plazo mixto',
							name: 'modplazo3',
							width:300,
							align:'left',
							value:'3'
						},
						{
							 fieldLabel: '',
							 labelSeparator: '',
							 boxLabel: 'Plazo ampliado PEMEX',
							 name: 'modplazo4',
							width:300,
							align:'left',
							value:'5'
						}
					]
				}		  
			]
       },
		 {			
			xtype:'panel',  id: 'seccion_8',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Descuento Autom�tico <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_DesAutomaticoEPO',	name:'DesAutomaticoEPO',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'DesAutomaticoEPO', inputValue: 'S' },
						{	boxLabel	: 'NO', name : 'DesAutomaticoEPO',  inputValue: 'N' }						
					],
					listeners	: {
						change:{
							fn:function(){
								var id_DesAutomaticoEPO =	(Ext.getCmp('id_DesAutomaticoEPO')).getValue();
								var id_lineaCredito =	(Ext.getCmp('id_lineaCredito')).getValue();
								
								if(id_DesAutomaticoEPO.getGroupValue() == 'S'){
									Ext.getCmp('seccion_10').setVisible(true);	
									
									if(id_lineaCredito == null ){	 Ext.getCmp('id_lineaCredito').setValue('N');  }
									else  if(id_lineaCredito.getGroupValue() == 'N'){	Ext.getCmp('id_lineaCredito').setValue('N');  }
									
									
									
								}else  if(id_DesAutomaticoEPO.getGroupValue() == 'N'){
									Ext.getCmp('seccion_10').setVisible(false);
									if(id_lineaCredito == null ){ Ext.getCmp('id_lineaCredito').setValue('');	  }	
									else  if(id_lineaCredito.getGroupValue() == ''){ Ext.getCmp('id_lineaCredito').setValue('');	  }									
								}
							}
						}
					}
				}				
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_9',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Publica documentos No Negociables <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_noNegociables',	name:'noNegociables',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'noNegociables', inputValue: 'S' },
						{	boxLabel	: 'NO', name : 'noNegociables',  inputValue: 'N' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_10',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Validar l�mite de la l�nea de cr�dito en la publicaci�n de documentos <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_lineaCredito',	name:'lineaCredito',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'lineaCredito', inputValue: 'S' },
						{	boxLabel	: 'NO', name : 'lineaCredito',  inputValue: 'N' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_20',  hidden: false,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Retornar documentos de Negociable a Baja <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_negociableabaja',	name:'negociableabaja',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'negociableabaja', inputValue: 'S' },
						{	boxLabel	: 'NO',  name : 'negociableabaja', inputValue: 'N' }						
					]					
				}				
			]
		},
                //*****************NUEVO PARAMETRO JAGE ********************
                {			
			xtype:'panel',  id: 'seccion_27',  hidden: false,	layout:'table',	width:700,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Opera bajo contrato de financiamiento a clientes y distribuidores sin cesi�n de derechos <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_operbajocontrato',	name:'operbajocontrato',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'bajocontrato', inputValue: 'S' },
						{	boxLabel	: 'NO',  name : 'bajocontrato', inputValue: 'N' }						
					]					
				}				
			]
		},
                
		//*****************Secci�n 2: Par�metros ********************
		
		{			
			xtype:'panel',	 id: 'titulos_3',  hidden: true,  layout:'table',	width:700,	layoutConfig:{   columns: 4 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:700,frame:true,	colspan:'4',	border:true,	html:'<div align="center"><b>Secci�n 2: Par�metros</b	><br>&nbsp;</div>'	}
			]
		},		
		{			
			xtype:'panel',  id: 'titulos_4',   hidden: true, 	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Nombre del par�metro <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Tipo de dato <br><br>&nbsp;</div>'	},
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Valores<br><br>&nbsp;</div>'	}							
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_11',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda1',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('1');  		} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Plazo m�nimo del financiamiento <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false , 
					items: [	 
						{
							width: 100,	
							xtype: 'numberfield',
							fieldLabel: '',
							name: 'txdiasmin',
							id: 'txdiasmin1',
							allowBlank: true,
							maxLength: 2,					
							msgTarget: 'side',
							margins: '0 20 0 0',
							listeners	: {
								blur:function(){	
									var txdiasmin1 =	Ext.getCmp('txdiasmin1');
									var txplazomax1 =	Ext.getCmp('txplazomax1');
									if(!Ext.isEmpty(txplazomax1.getValue())){ 		
										if(parseInt(txdiasmin1.getValue()) > parseInt(txplazomax1.getValue())  ){
											Ext.MessageBox.alert('Mensaje','Los d�as m�nimos no pueden ser mayores al plazo m�ximo');											  
											return;	
										}else  {
											if(Ext.isEmpty(txplazomax1.getValue())){ 
												Ext.MessageBox.alert('Mensaje','Debe capturar el plazo m�ximo');											  
												return;
											}
										}
									}									
								}							
							}
						}	
					]
				}							
			]
		},
		{			
			xtype:'panel',  id: 'seccion_12',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns:6},
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda2',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('2');  		} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Plazo m�ximo del financiamiento <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width:100,	xtype: 'numberfield',	fieldLabel: '',	name: 'txplazomax', 	id: 'txplazomax1',allowBlank: true, 	maxLength: 3,	msgTarget: 'side', 	margins: '0 20 0 0',
					listeners	: {
						blur:function(){	
							var txdiasmin1 =	Ext.getCmp('txdiasmin1');
							var txplazomax1 =	Ext.getCmp('txplazomax1');
							if(!Ext.isEmpty(txplazomax1.getValue())){ 		
								if(parseInt(txdiasmin1.getValue()) > parseInt(txplazomax1.getValue())  ){
									Ext.MessageBox.alert('Mensaje','Los d�as m�nimos no pueden ser mayores al plazo m�ximo');											  
									return;	
								}
							}									
						}							
					}
				}	,					
				{ 	width:20,  xtype: 'displayfield', 	value: '&nbsp;&nbsp;&nbsp;'	},	
				{	width:30, text: 'Actualizar Tasa', 		id: 'btnAcTasa',		iconCls: 'icoAceptar', 	xtype: 'button' ,
					handler: function(){
						
						fp.el.mask('Procesando...', 'x-mask-loading');	
						Ext.Ajax.request({
							url: '24forma4.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: "Actualiza_Tasa"
							}),
							callback: procesaActualiza_Tasa
						});		
					} 
				}		
			]
		},
		{			
			xtype:'panel',  id: 'seccion_13',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda3',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('3');  		} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>D�as M�ximos para Ampliar la Fecha de Vencimiento <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false , 
					items: [	 
						{
							width: 100,	
							xtype: 'numberfield',
							fieldLabel: '',
							name: 'txtDiasFechaVenc',
							id: 'txtDiasFechaVenc1',
							allowBlank: true,
							maxLength: 3,					
							msgTarget: 'side',
							margins: '0 20 0 0'
						}	
					]
				}							
			]
		},		
		{			
			xtype:'panel',  id: 'seccion_13_1',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	xtype: 'button',		id: 'btnAyuda3_1',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('13');  		} 	},												
			
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Publicar documentos financiables a meses sin intereses <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_mesesIntereses',	name:'mesesIntereses',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'SI',	name : 'mesesIntereses', inputValue: 'S' },
						{	boxLabel	: 'NO', name : 'mesesIntereses',  inputValue: 'N' }						
					],
					listeners: {
						change:{
							fn:function(){
								if(Ext.getCmp('seccion_13_1').isVisible()){
									var id_mesesIntereses = (Ext.getCmp('id_mesesIntereses')).getValue(); 
									
									if( id_mesesIntereses.getGroupValue()=='S' ){	
										Ext.getCmp('seccion_13_2').show();									
										
									}else {								
										Ext.getCmp('seccion_13_2').hide();
										Ext.getCmp('cmbMeses1').setValue(''); 
									}
								}
							}
						}
					}
				}				
			]
		},			
		{			
			xtype:'panel',  id: 'seccion_13_2',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>   <br><br>&nbsp;</div>'	},					
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Plazo m�ximo de financiamiento para operaciones a meses sin intereses<br><br>&nbsp;</div>'	},
				{
					xtype				: 'combo',
					id					: 'cmbMeses1',
					name				: 'cmbMeses',
					hiddenName 		:'cmbMeses', 
					fieldLabel		: '',
					width				: 150,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'afiliacion',					
					store				: storeMeses				
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_14',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	xtype: 'button',		id: 'btnAyuda4',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('4');  		} 	},												
			
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Tipo de conversi�n <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_rconversion',	name:'rconversion',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Sin Conversi�n',	name : 'rconversion', inputValue: 'N' },
						{	boxLabel	: 'D�lar-Peso', name : 'rconversion',  inputValue: 'P' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_15',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	xtype: 'button',		id: 'btnAyuda5',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('5');  		} 	},												
			
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Operar con pantalla de cifras de control <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_rcontrol',	name:'rcontrol',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Si mostrar pantalla cifras de control',	name : 'rcontrol', inputValue: 'S' },
						{	boxLabel	: 'No mostrar pantalla de cifras de control', name : 'rcontrol',  inputValue: 'N' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_16',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 8 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda6',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('6');  		} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Plazo m�ximo para tomar el descuento, Porcentaje de descuento que otorga la EPO <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 35,  xtype: 'displayfield', 	value: 'Plazo' 	 	},
				{ 	width: 50,	xtype: 'numberfield', 	fieldLabel: '',  name: 'txplazo', 	id: 'txplazo1', 	allowBlank: true, 		maxLength: 3,	msgTarget: 'side',  	margins: '0 20 0 0' 	},
				{ 	width: 70,  xtype: 'displayfield', 		value: 'Porcentaje' 	},
				{ 	width: 50,	xtype: 'numberfield', fieldLabel: '', 	name: 'txporcentaje', 	id: 'txporcentaje1', 	allowBlank: true,	maxLength: 3, 	msgTarget: 'side', margins: '0 20 0 0'  },																			
				{ 	width: 25,  xtype: 'displayfield', 		value: ' &nbsp; %' 	}
			]
		},{
			xtype:'panel',  id: 'seccion_16_1',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 5 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	xtype: 'button', id: 'btnAyuda6_1', iconCls: 'icoAyuda', handler: function(){ mostrarAyuda('11'); } },
				{	width: 250, frame:false, border:false, html:'<div class="formas" align="left" ><br>Tasa de descuento de Banco Adquirente y EPO Moneda Nacional:<br>&nbsp;</div>' },
				{	width: 100, frame:false, border:false, html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 100, frame:false, border:false, xtype:'numberfield', fieldLabel:'', name:'descAdquirienteBanco', id:'id_descAdquirienteBanco', allowBlank:true, minValue:1, maxValue:100, allowDecimals:true, msgTarget:'side', margins:'0 20 0 0' },
				{ width:100,  xtype: 'displayfield', 	value: ' &nbsp; %' }
			]
		},{
			xtype:'panel',  id: 'seccion_16_2',  hidden: true, layout:'table', width:700, layoutConfig:{ columns: 5 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{ xtype: 'button', id: 'btnAyuda6_2', iconCls: 'icoAyuda', handler: function(){ mostrarAyuda('12'); } },
				{ width: 250, frame:false, border:false, html:'<div class="formas" align="left" ><br> N�mero de Afiliaci�n de Comercio: <br>&nbsp;</div>' },
				{ width: 100, frame:false, border:false, html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>' },
				{ width: 120, frame:false, border:false, xtype:'textfield', fieldLabel:'', name:'no_afiliacion_comer', id:'id_no_afiliacion_comer', allowBlank:true, maxLength:16, msgTarget:'side', margins:'0 20 0 0' },
				{ width: 80,  xtype: 'displayfield',  value: ' &nbsp;' }
			]
		},{			
			xtype:'panel',  id: 'seccion_17',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 5 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda7',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('7');  		} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Aviso de pr�ximos vencimientos en: <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false ,  	xtype: 'numberfield', 	fieldLabel: '',  	name: 'txproxvenc', 		id: 'txproxvenc1', allowBlank: true, maxLength: 2, 	msgTarget: 'side', 	margins: '0 20 0 0' 		},						
				{ 	width:100,  xtype: 'displayfield', 	value: 'd�as h�biles'	}												
			]
		},
		{			
			xtype:'panel',  id: 'seccion_18',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	xtype: 'button',		id: 'btnAyuda8',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('8');  		} 	},												
			
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Fecha para porcentaje de descuento <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_rad_fecha_porc_desc',	name:'rad_fecha_porc_desc',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Fecha de Emisi�n',	name : 'rad_fecha_porc_desc', inputValue: 'E' },
						{	boxLabel	: 'Fecha de Publicaci�n', name : 'rad_fecha_porc_desc',  inputValue: 'P' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_19',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [		
				{	xtype: 'button',		id: 'btnAyuda9',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('9');  		} 	},	
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Env�o de Correo Electr�nico para Aceptaci�n de Documentos: <br><br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:350,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'id_txtEnvCorreo',	name:'txtEnvCorreo',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
					[  
						{	boxLabel	: 'Si',	name : 'txtEnvCorreo', inputValue: 'S' },
						{	boxLabel	: 'No', name : 'txtEnvCorreo',  inputValue: 'N' }						
					]					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_21',  hidden: true,	layout:'table',	width:700,	layoutConfig:{ columns: 5 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [	
				{	xtype: 'button',		id: 'btnAyuda10',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('10');  	} 	},												
				{	width:250,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> D�as h�biles de Operaci�n Distribuidor <br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br>&nbsp;</div>'	},
				{	width:100,	frame:false,	border:false ,  	xtype: 'numberfield', 	fieldLabel: '',  	name: 'diasoperacion', 		id: 'id_diasoperacion', allowBlank: true, maxLength: 3, 	msgTarget: 'side', 	margins: '0 20 0 0' 		},						
				{ 	width:100,  xtype: 'displayfield', 	value: 'd�as h�biles'	}												
			]
		},	
		// *******************Secci�n 3: Par�metros Generales EPO - IF*****************
		{			
			xtype:'panel',	 id: 'titulos_5',  hidden: true,  layout:'table',	width:700,	layoutConfig:{   columns: 5 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:700,frame:true,	colspan:'4',	border:true,	html:'<div align="center"><b>Secci�n 3: Par�metros Generales EPO - IF</b	><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',	 id: 'titulos_6',  hidden: true,  layout:'table',	width:700,	layoutConfig:{   columns: 5 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{ 	xtype: 'displayfield', 		id: 'nombreEPO', 		style: 'text-align:left;', 		fieldLabel: '', 	text: '-' 	}	
			]
		},
		{			
			xtype:'panel',  id: 'titulos_7',   hidden: true, 	layout:'table',	width:700,	layoutConfig:{ columns: 5 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ gridConsulta 	]
		}			
	];
		
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: false,
		width: 700,		
		autoHeight	: true,
		title: ' Par�metros por EPO',				
		layout		: 'form',
		style: 'margin:0 auto;',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: elementosForma,		
		buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',				
				iconCls: 'icoAceptar',
				formBind: true,	
				handler: procesarGuardar
			}		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20)				
		]
	});
	
	catalogoEPO.load();
	
	
});
