Ext.onReady(function() {



	//**************Inicializa Valores **************************************
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				Ext.getCmp('btnGuardar').enable();		
				
				if(jsonData.Guardar_Datos=='S') {
					Ext.MessageBox.alert('Mensaje',jsonData.mensaje,
					function(){ 					
						Ext.Ajax.request({
							url: '24forma3.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{  
								informacion: "valoresIniciales",
								tipo_credito:jsonData.tipo_credito
							}),
							callback: procesaValoresIniciales
						});	
					});
				}else  {
				
					if(jsonData.tipo_credito==1)  {
					
						Ext.getCmp("titulos_2").hide(); 	Ext.getCmp("seccion_5").hide();
						Ext.getCmp("seccion_6").hide(); 	Ext.getCmp("seccion_7").hide();
						Ext.getCmp("seccion_8").hide(); 	Ext.getCmp("seccion_9").hide();
						Ext.getCmp("seccion_10").hide(); Ext.getCmp("seccion_11").hide();
						Ext.getCmp("seccion_12").hide(); Ext.getCmp("tipo_credito1").setValue('1');
						
						
						Ext.getCmp("titulos_1").show();	Ext.getCmp("seccion_1").show();
						Ext.getCmp("seccion_2").show();	Ext.getCmp("seccion_3").show();
						Ext.getCmp("seccion_4").show();	
												
						Ext.getCmp('tipocred').setValue(jsonData.tipocred);				
						Ext.getCmp('respago').setValue(jsonData.respago);
						Ext.getCmp('tipocob').setValue(jsonData.tipocob);
						Ext.getCmp('pgointeres').setValue(jsonData.pgointeres);	
						
						Ext.getCmp('hidtipocob').setValue(jsonData.tipocob);	
						Ext.getCmp('hidrespago').setValue(jsonData.respago);	
												
						if(jsonData.respago == 'D'){
							Ext.getCmp('pgointeres').items.items[1].disable();
						}						
						if(jsonData.tipocred == 'C'){
							Ext.getCmp('seccion_2').setVisible(false);
							Ext.getCmp('seccion_3').setVisible(false);
							Ext.getCmp('pgointeres').items.items[1].enable();							
								
						} else if(jsonData.tipocred == 'D' || jsonData.tipocred == 'A'){
							Ext.getCmp('seccion_2').setVisible(true);
							Ext.getCmp('seccion_3').setVisible(true);
							Ext.getCmp('seccion_4').setVisible(true);									
						}
						
						
					}else if(jsonData.tipo_credito==2)  {
					
						Ext.getCmp("titulos_1").hide();	Ext.getCmp("seccion_1").hide();
						Ext.getCmp("seccion_2").hide();	Ext.getCmp("seccion_3").hide();
						Ext.getCmp("seccion_4").hide();					
								
						Ext.getCmp("titulos_2").show();	Ext.getCmp("seccion_5").show();
						Ext.getCmp("seccion_6").show();	Ext.getCmp("seccion_7").show();
						Ext.getCmp("seccion_8").show();	Ext.getCmp("seccion_9").show();
						Ext.getCmp("seccion_10").show();	Ext.getCmp("seccion_11").show();
						Ext.getCmp("seccion_12").show();				
						
						Ext.getCmp('txdiasmin').setValue(jsonData.txdiasmin);	
						Ext.getCmp('txplazomax').setValue(jsonData.txplazomax);	
						Ext.getCmp('rconversion').setValue(jsonData.rconversion);	
						Ext.getCmp('rcontrol').setValue(jsonData.rcontrol);	
						Ext.getCmp('txtplazominlc').setValue(jsonData.txtplazominlc);	
						Ext.getCmp('txtproxvenc').setValue(jsonData.txtproxvenc);
						Ext.getCmp('txtDiasFechaVenc').setValue(jsonData.txtDiasFechaVenc);
						Ext.getCmp('txtEnvCorreo').setValue(jsonData.txtEnvCorreo);
						
					}				
				}	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	// ***********************++Funci�n para guardar los datos  *********************
	function procesarGuardar() {	
	
		var tipo_credito1 =(Ext.getCmp('tipo_credito1')).getValue();
		var txdiasmin =	(Ext.getCmp('txdiasmin'));
		var txplazomax =	(Ext.getCmp('txplazomax'));	
		
		if(tipo_credito1 ==2)  {
		
			if(Ext.isEmpty(txdiasmin.getValue())){ 
				txdiasmin.markInvalid('El campo es obligatorio');
				return true;
			}		
			if(Ext.isEmpty(txplazomax.getValue())){ 
				txplazomax.markInvalid('El campo es obligatorio');
				return true;
			}	
			
			var txplazomax = Ext.getCmp('txplazomax').getValue();
			var txdiasmin = Ext.getCmp('txdiasmin').getValue();
			if(parseInt(txdiasmin) > parseInt(txplazomax) ){
				Ext.getCmp('txplazomax').markInvalid('Los d�as minimos no pueden ser mayores al plazo m�ximo ');		
				return true;	
			}
		}
		
		Ext.getCmp('btnGuardar').disable();		
		fp.el.mask('Procesando...', 'x-mask-loading');			
		Ext.Ajax.request({
			url: '24forma3.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{  
			informacion: "Guardar_Datos"
			}),
			callback: procesaValoresIniciales
		});
			
	}
				
	
	// *******************Criterios de Captura ***********************
	var catTipoCreditoData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Secci�n 1: Esquemas de operaci�n'],
			['2','Secci�n 2: Par�metros']			
		 ]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'tipo_credito',	
			id: 'tipo_credito1',	
			fieldLabel: '&nbsp;&nbsp;Secci�n', 
			mode: 'local',	
			hiddenName : 'tipo_credito',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catTipoCreditoData,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,
			value:1,
			listeners: {
				select: {
					fn: function(combo) {
						if(combo.getValue()==1)  {
						
						Ext.getCmp("titulos_1").show();	Ext.getCmp("seccion_1").show();
						Ext.getCmp("seccion_2").show();	Ext.getCmp("seccion_3").show();
						Ext.getCmp("seccion_4").show(); 					
						
						Ext.getCmp("titulos_2").hide();	Ext.getCmp("seccion_5").hide();
						Ext.getCmp("seccion_6").hide();	Ext.getCmp("seccion_7").hide();
						Ext.getCmp("seccion_8").hide();	Ext.getCmp("seccion_9").hide();
						Ext.getCmp("seccion_10").hide(); Ext.getCmp("seccion_11").hide();
						Ext.getCmp("seccion_12").hide();				
					
						}else if(combo.getValue()==2)  {
						
							Ext.getCmp("titulos_1").hide();	Ext.getCmp("seccion_1").hide();
							Ext.getCmp("seccion_2").hide();	Ext.getCmp("seccion_3").hide();
							Ext.getCmp("seccion_4").hide();					
							
							Ext.getCmp("titulos_2").show();	Ext.getCmp("seccion_5").show();
							Ext.getCmp("seccion_6").show();	Ext.getCmp("seccion_7").show();
							Ext.getCmp("seccion_8").show();	Ext.getCmp("seccion_9").show();
							Ext.getCmp("seccion_10").show();	Ext.getCmp("seccion_11").show();
							Ext.getCmp("seccion_12").show();						
					
						}
						fp.el.mask('Procesando...', 'x-mask-loading');	
						Ext.Ajax.request({
							url: '24forma3.data.jsp',
							params: {
								informacion: "valoresIniciales",
								tipo_credito:combo.getValue()
							},
							callback: procesaValoresIniciales
						});
						
					}
				}
			}	
		},			
		{			
			xtype:'panel',	 id: 'titulos_1', layout:'table',	width:600,	layoutConfig:{   columns: 3 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'3',	border:true,	html:'<div align="center"><b>Secci�n 1: Esquemas de operaci�n</b	><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',	 id: 'titulos_2', layout:'table',	width:600,	layoutConfig:{   columns: 3 },
			defaults: { align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'3',	border:true,	html:'<div align="center"><b>Secci�n 2: Par�metros</b	><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',  id: 'titulos_3', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Nombre del par�metro <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Tipo de dato <br><br>&nbsp;</div>'	},
				{	width:300,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> <b> Valores<br><br>&nbsp;</div>'	}							
			]
		},
		
			//**********************Datos de Seccion 1
		{			
			xtype:'panel',  id: 'seccion_1', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de cr�dito a operar <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'center',	id:'tipocred',	name:'tipocred',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Descuento y/o Factoraje     ',	name : 'tipocred', inputValue: 'D' },
							{	boxLabel	: 'Cr�dito en cuenta corriente ',	name : 'tipocred',  inputValue: 'C' }, 
							{	boxLabel	: 'Ambos'                       ,	name : 'tipocred', inputValue: 'A' }
						],
						listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('tipocred')).getValue();
								if(valor.getGroupValue() == 'C'){
									Ext.getCmp('seccion_2').setVisible(false);
									Ext.getCmp('seccion_3').setVisible(false);									
									Ext.getCmp('pgointeres').items.items[1].enable();
									
								} else if(valor.getGroupValue() == 'D' || valor.getGroupValue() == 'A'){
								
									Ext.getCmp('seccion_2').setVisible(true);
									Ext.getCmp('seccion_3').setVisible(true);
									Ext.getCmp('seccion_4').setVisible(true);
									Ext.getCmp('tipocob').setValue(Ext.getCmp('hidtipocob').getValue());												
									Ext.getCmp('respago').setValue(Ext.getCmp('hidrespago').getValue());	
									
								} 
							}
						}
					}					
					
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_2', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Responsabilidad de pago de inter�s (S�lo si opera DM) <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'respago',	name:'respago',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'EPO     ',	name : 'respago', inputValue: 'E' },
							{	boxLabel	: 'Distribuidor ',	name : 'respago', inputValue: 'D' }							
						],
						listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('respago')).getValue();								
								if(valor.getGroupValue() == 'D'){
									
									Ext.getCmp('pgointeres').setValue('1');	
									Ext.getCmp('pgointeres').items.items[1].disable();
									
								} else if(valor.getGroupValue() == 'E' ){
									
									Ext.getCmp('pgointeres').items.items[1].enable();
									
								} 
							}
						}
					}
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_3', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de cobranza(S�lo si opera DM) <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'tipocob',	name:'tipocob',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Delegada     ',	name : 'tipocob', inputValue: 'D' },
							{	boxLabel	: 'No delegada ',	name : 'tipocob', inputValue: 'N' }							
						]
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_4', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Esquema de pago de intereses <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br>  <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'pgointeres',	name:'pgointeres',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Anticipado     ', name : 'pgointeres', inputValue: '1' },
							{	boxLabel	: 'Al vencimiento ', name : 'pgointeres', inputValue: '2' }							
						]
				}				
			]
		},
		
		//**********************Datos de Seccion 2 
		
		{			
			xtype:'panel',  id: 'seccion_5', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo m�nimo del financiamiento <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 150,	xtype:'numberfield',	msgTarget:'side',	id:'txdiasmin',	name:'txdiasmin',	maxLength	: 3 , msgTarget: 'side', margins: '0 20 0 0', allowBlank: true 	}		
			]
		},
		{			
			xtype:'panel',  id: 'seccion_6', 	layout:'table', allowBlank: true, msgTarget: 'side',   margins		: '0 20 0 0', width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo m�ximo del financiamiento <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 150,	xtype:'numberfield',	  id:'txplazomax',  name:'txplazomax',	margins	: '0 20 0 0', maxLength	: 3,
					listeners:{
						blur: function(field){ 								
							if(field.getValue() != '') {	
								
								var txplazomax = Ext.getCmp('txplazomax').getValue();
								var txdiasmin = Ext.getCmp('txdiasmin').getValue();
								
								if(parseInt(txdiasmin) > parseInt(txplazomax) ){
									 Ext.getCmp('txplazomax').markInvalid('Los d�as minimos no pueden ser mayores al plazo m�ximo ');		
									return;	
								}																	
							}
						}
					}	
				}		
			]
		},
		{			
			xtype:'panel',  id: 'seccion_7', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> D�as M�ximos para Ampliar la Fecha de Vencimiento <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 150,	xtype:'numberfield',	msgTarget:'side',	id:'txtDiasFechaVenc',	name:'txtDiasFechaVenc',	maxLength	: 3  }		
			]
		},
		{			
			xtype:'panel',  id: 'seccion_8', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//1
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de conversi�n <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C  <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'rconversion',	name:'rconversion',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Sin conversi�n     ',	name : 'rconversion', inputValue: 'N' },
							{	boxLabel	: 'Dolar-Peso ',	name : 'rconversion', inputValue: 'P' }							
						]
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_9', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Operar con pantalla de cifras de control <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'rcontrol',	name:'rcontrol',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Si mostrar pantalla cifras de control     ',	name : 'rcontrol', inputValue: 'N' },
							{	boxLabel	: 'No mostrar pantalla de cifras de control ',	name : 'rcontrol', inputValue: 'P' }							
						]
				}				
			]
		},
		{			
			xtype:'panel',  id: 'seccion_10', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo m�nimo para la l�nea de cr�dito <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 150,	xtype:'numberfield',	msgTarget:'side',	id:'txtplazominlc',	name:'txtplazominlc',	maxLength	: 5	}		
			]
		},
		{			
			xtype:'panel',  id: 'seccion_11', 	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Aviso de pr�ximos vencimientos en: <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> N <br><br>&nbsp;</div>'	},
				{	width: 100,	xtype:'numberfield',	msgTarget:'side',	id:'txtproxvenc',	name:'txtproxvenc',	maxLength	: 2	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> d�as h�biles <br><br>&nbsp;</div>'	}
			]
		},
		{			
			xtype:'panel',  id: 'seccion_12', 	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [				
				{	width:200,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Env�o de Correo Electr�nico para Aceptaci�n de Documentos: <br><br>&nbsp;</div>'	},
				{	width:150,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> C <br><br>&nbsp;</div>'	},
				{	width:250,	xtype:'radiogroup',	style: {  width: '80%', 	marginLeft: '10px'  },	align:'left',	id:'txtEnvCorreo',	name:'txtEnvCorreo',	cls:'x-check-group-alt',	columns:[300, 300, 300],
					items: 
						[  
							{	boxLabel	: 'Si',	name : 'txtEnvCorreo', inputValue: 'S' },
							{	boxLabel	: 'No',	name : 'txtEnvCorreo', inputValue: 'N' }							
						]
				}				
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'hidtipocob', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'hidrespago', 	value: '' }
	];
		
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: false,
		width: 600,		
		autoHeight	: true,
		title: ' Par�metros Generales',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		titleCollapse: false,
     defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,		
		items: elementosForma,		
		buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoAceptar',
				formBind: true	,
				handler: procesarGuardar
			}		
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)			
		]
	});
	


	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '24forma3.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
});
