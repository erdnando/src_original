<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*,
		com.netro.descuento.*,
		com.netro.anticipos.ProcesoSNAnticipoBean"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/24finandist/24secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") == null)?"":request.getParameter("informacion");
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String procSirac = (request.getParameter("procSirac") == null)?"":request.getParameter("procSirac");
String esPedido="", esNumPrestamo="", esFechaOperacion="", esEstatusAntic="", esCausaRechazo="", nombreArchivo = "";
String urlArchivo="", consulta = "", infoRegresar = "";
int totalSolicitudesRecibidas=0;

JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();
HashMap datos = new HashMap();

log.debug("informacion = <"+informacion+">");

if (informacion.equals("CargaArchivo.inicializacion")){
	
	JSONObject resultado = new JSONObject();
	boolean success = true;
	boolean hayAviso = false;
	String estadoSiguiente = null;
	String aviso = null;
 
	if(!hayAviso){
		String reinstalacionesLayout = 
"		<table width=\"100%\" > "+
"		<tr> "+
"		<td class=\"titulos\" align=\"center\"> "+
"				<span class=\"titulos\"> "+
"					Estructura de Archivo<br>Carga masiva "+
"				</span> "+
"			</td> "+
"		</tr> "+
"		<tr> "+
"			<td>&nbsp;</td> "+
"		</tr> "+
"		<tr> "+
"			<td colspan=\"2\" align=\"center\" > "+
"				<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" > "+
"					<tr> "+
"						<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" > "+
"							Layout SIRAC - NAFINET<br> "+
"							Archivo de extensi&oacute;n TXT; los campos deben de estar separados por pipes (\"|\") "+ 
"						</td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td> "+
"						<td class=\"celda01\" align=\"center\">Nombre del Campo</td> "+
"						<td class=\"celda01\" align=\"center\">Tipo</td> "+
"						<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td> "+
"						<td class=\"celda01\" align=\"center\">Obligatorio</td> "+
"						<td class=\"celda01\" align=\"center\">Observaciones</td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							Número de documento final  "+
"						</td> "+
"						<td class=\"formas\" align=\"center\">NUMBER</td> "+
"						<td class=\"formas\" align=\"center\">6</td> "+ 
"						<td class=\"formas\" align=\"center\">S&iacute;</td> "+
"						<td class=\"formas\" align=\"center\">&nbsp;</td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							N&uacute;mero de Pr&eacute;stamo  "+
"						</td> "+
"						<td class=\"formas\" align=\"center\">NUMBER</td> "+
"						<td class=\"formas\" align=\"center\">8</td> "+
"						<td class=\"formas\" align=\"center\">S&iacute;</td> "+
"						<td class=\"formas\" align=\"center\"> &nbsp </td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							Fecha de Operaci&oacute;n  "+
"						</td> "+
"						<td class=\"formas\" align=\"center\">DATE</td> "+
"						<td class=\"formas\" align=\"center\">8</td> "+
"						<td class=\"formas\" align=\"center\">S&iacute;</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							Fecha con formato: DD/MM/AAAA.  "+
"						</td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							Tipo Motivo  "+
"						</td> "+
"						<td class=\"formas\" align=\"center\">CHAR</td> "+
"						<td class=\"formas\" align=\"center\">1</td> "+
"						<td class=\"formas\" align=\"center\">S&iacute;</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							&nbsp "+
"						</td> "+
"					</tr> "+
"					<tr> "+
"						<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							Causa del rechazo  "+
"						</td> "+
"						<td class=\"formas\" align=\"center\">CHAR</td> "+
"						<td class=\"formas\" align=\"center\">15</td> "+
"						<td class=\"formas\" align=\"center\">S&iacute;</td> "+
"						<td class=\"formas\" align=\"center\"> "+
"							&nbsp "+
"						</td> "+
"					</tr>	 "+
"			</table> "+
"			</td> "+
"		</tr> "+
"		<tr> "+
"			<td>&nbsp;</td> "+
"		</tr> "+
"		</table>";	
		resultado.put("reinstalacionesLayout", reinstalacionesLayout);
	}
	
	if(!hayAviso){
		estadoSiguiente = "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", new Boolean(success));
	resultado.put("estadoSiguiente",	estadoSiguiente);
	resultado.put("aviso", aviso);
	infoRegresar = resultado.toString();
 
} else if (informacion.equals("CargaArchivo.subirArchivo")){	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	JSONObject resultado = new JSONObject();
	boolean success = true;
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
	} catch(Exception e) {
		success = false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));	
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," "));
	}catch(Exception e){
		success = false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "REALIZAR_VALIDACION");
	// Enviar resultado de la operacion
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();
	
} else if (informacion.equals("CargaArchivo.realizarValidacionEInsercion")) {
	
	JSONObject resultado	= new JSONObject();
	boolean success = true;

	String fileName = (request.getParameter("fileName") == null)?"":request.getParameter("fileName");
	String rutaArchivo = strDirectorioTemp + iNoUsuario + "." + fileName	;
	
	// Obtener instancia del EJB de ProcesoSNSolicitud
	ProcesoSNAnticipoBean beanProceso = new ProcesoSNAnticipoBean();
		
	// Preprocesar Archivo
	int ctaLineas = 0;
	java.io.File lfArchivo = new java.io.File(rutaArchivo);
	BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lfArchivo)));
	String lsLinea = "";
	StringBuffer lsbDocumentos = new StringBuffer();
	while( (lsLinea = br.readLine()) != null ) {
		lsLinea = lsLinea.replace('\'',' ');
		lsbDocumentos.append(lsLinea.trim()+"\n");
		ctaLineas++;
	}//while linea
	//Obtenemos el número de proceso para insertar en la tabla temporal para cada docto.
	String lsProcSirac = beanProceso.sgetNumaxProcesoSirac();
	//Realizar Proceso de creditos
	Hashtable lohResultados = beanProceso.ohprocesarCreditos(lsbDocumentos.toString(), lsProcSirac);
	boolean lbOk = new Boolean(lohResultados.get("bTodosOk").toString()).booleanValue();

	// Leer Resultado de la Validacion
	String totalRegistros	= String.valueOf(ctaLineas);
	
	//  Leer Resultado de la Validacion ( Registros sin Errores )
	JSONArray registrosSinErroresDataArray	= new JSONArray();
	String registrosSinErrores = "0";
	String numeroRegistrosSinErrores = "0";

	if(lbOk){
		
		int ctaRegistros = 0;
		//Borramos de la tabla temporal ya que no se utilizará al no haber errores
		boolean bOkBorra = beanProceso.bborrarAnticiposSiracTmp(lsProcSirac);		
		//Actualizamos y mostramos los pedidos cargados masivamente
		boolean bOkActualiza = false;
		Vector lvFolio = (Vector)lohResultados.get("lovFolio");
		Vector lvNumeroPrestamo = (Vector) lohResultados.get("lovNumeroPrestamo");
		Vector lvFecha = (Vector) lohResultados.get("lovFecha");
		Vector lovTipo = (Vector)lohResultados.get("lovTipo");
		Vector lovCausa = (Vector)lohResultados.get("lovCausa");
		
		for (int i=0; i<lvFolio.size(); i++) {
 
			esPedido = lvFolio.get(i).toString();
			esNumPrestamo = lvNumeroPrestamo.get(i).toString();
			esFechaOperacion = lvFecha.get(i).toString();
			esEstatusAntic = (lovTipo.get(i).toString().equals("O"))?"3":"4";
			esCausaRechazo = lovCausa.get(i).toString();
			bOkActualiza = beanProceso.bactualizarCredito(esEstatusAntic, esFechaOperacion, esNumPrestamo, esCausaRechazo, esPedido);
			
			JSONArray registro = new JSONArray();	
			registro.add(esPedido);
			registro.add(esNumPrestamo);
			registro.add(esFechaOperacion);
			registro.add(esEstatusAntic);
			registro.add(esCausaRechazo);
			
			registrosSinErroresDataArray.add(registro);
			
		}
		
		// Total de Solicitudes exitosas recibidas:
		registrosSinErrores = String.valueOf(lvFolio.size());
		numeroRegistrosSinErrores = Comunes.formatoDecimal(lvFolio.size(),0);
		
	}
	
	//  Leer Resultado de la Validacion ( Registros con Errores )
	JSONArray registrosConErroresDataArray = new JSONArray();
	String registrosConErrores = "0";
	String numeroRegistrosConErrores = "0";
 
	if(!lbOk){
 
		StringBuffer buffer = new StringBuffer();
		int cuentaMensajes = 0;
		char lastChar = 0;
		char previousChar	= 0;
		
		//Se crea el archivo	
		String contArchivo = lohResultados.get("lsbContArchivo").toString();
		nombreArchivo = "";
		CreaArchivo archivo = new CreaArchivo();
		if (archivo.make(contArchivo, strDirectorioTemp, ".txt")){
			nombreArchivo = archivo.nombre;
		} else {
			log.warn("Error en la generacion del archivo");
		}
		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		Vector error = (Vector)lohResultados.get("lsbError");
		Vector regOk = (Vector)lohResultados.get("lsbCorrecto");
		int itera = error.size()>=regOk.size()?error.size():regOk.size();
		Vector aux = null;
		for (int i=0;i<itera;i++) {
			JSONArray registro = new JSONArray();
			if (i<error.size()) {
				aux = (Vector)error.get(i);
				registro.add(""+aux.get(0));
				registro.add(""+aux.get(1));
			} else{
				registro.add(" ");
				registro.add(" ");
			}
			if (i<regOk.size()) {
				aux = (Vector)regOk.get(i);	
				registro.add(""+aux.get(0));
				registro.add(""+aux.get(1));
				registro.add(""+aux.get(2));
			} else{
				registro.add(" ");
				registro.add(" ");
				registro.add(" ");				
			}
			registrosConErroresDataArray.add(registro);
		}
			
		// Total de Solicitudes exitosas recibidas:
		registrosConErrores = "0";
		numeroRegistrosConErrores = "0";
 
	}
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	JSONArray erroresVsCifrasControlDataArray = new JSONArray();
	// Determinar si se mostrará el boton continuar carga	
	Boolean 	 cargaExistosa = lbOk?new Boolean(true):new Boolean(false);
 
	// Enviar resultado general de la validación/inserción
	resultado.put("totalRegistros", totalRegistros);
	
	resultado.put("registrosSinErroresDataArray", registrosSinErroresDataArray);
	resultado.put("numeroRegistrosSinErrores", numeroRegistrosSinErrores);
	
	resultado.put("registrosConErroresDataArray", registrosConErroresDataArray);
	resultado.put("numeroRegistrosConErrores", numeroRegistrosConErrores);
	
	resultado.put("erroresVsCifrasControlDataArray", erroresVsCifrasControlDataArray);
	resultado.put("cargaExistosa", cargaExistosa);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION_INSERCION" );
	// Enviar resultado de la operacion
	resultado.put("success", new Boolean(success));
 
	// Envio la ruta del archivo a descargar
 	resultado.put("urlArchivoDescargar",  urlArchivo);
	
	// Envio el lsProcSirac
 	resultado.put("lsProcSirac",  lsProcSirac);
	
	infoRegresar = resultado.toString();
 
} else if (informacion.equals("consultaGuardar")) { 

	ProcesoSNAnticipoBean beanProceso = new ProcesoSNAnticipoBean();
	Vector lovRegistro;
	Vector lovAnticipos = beanProceso.ovgetCreditosProcesados(procSirac);
	if( lovAnticipos.size()>0){
			for (int i=0; i<lovAnticipos.size(); i++) {
				lovRegistro = (Vector)lovAnticipos.get(i);
				esPedido = lovRegistro.get(0).toString();
				esNumPrestamo = lovRegistro.get(1).toString();
				esFechaOperacion = lovRegistro.get(2).toString();
				esEstatusAntic = lovRegistro.get(3).toString();
				esCausaRechazo = lovRegistro.get(4).toString();
				datos = new HashMap();
				datos.put("NUMERO_CREDITO", esPedido);
				datos.put("NUMERO_PRESTAMO", esNumPrestamo);
				datos.put("FECHA_OPERACION", esFechaOperacion);
				datos.put("ESTATUS_CREDITO", esEstatusAntic.equals("3")?"O":"R");
				datos.put("CAUSA_RECHAZO", esCausaRechazo);
				registros.add(datos);
			} 
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"totalDoctos\": \"" + lovAnticipos.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();
		
} else if (informacion.equals("CargaArchivo.fin"))	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("24proceso01ext.jsp"); 
 
} else {
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
}
 
%>
<%=infoRegresar%>