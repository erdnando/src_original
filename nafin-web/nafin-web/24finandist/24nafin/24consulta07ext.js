Ext.onReady(function() {

	//--------------------Consulta para el Grid (Seccion 1)--------------------
	var procesarConsultaSeccion1 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridSeccion1 = Ext.getCmp('gridSeccion1');	
		var el = gridSeccion1.getGridEl();	
		if (arrRegistros != null) {
			if (!gridSeccion1.isVisible()) {
				gridSeccion1.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridSeccion1.getColumnModel();
						
			if(store.getTotalCount() < 1) {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	//--------------------Consulta para el Grid (Seccion 2)--------------------
	var procesarConsultaSeccion2 = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridSeccion2 = Ext.getCmp('gridSeccion2');	
		var el = gridSeccion1.getGridEl();	
		if (arrRegistros != null) {
			if (!gridSeccion2.isVisible()) {
				gridSeccion2.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridSeccion1.getColumnModel();
						
			if(store.getTotalCount() < 1) {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	//-------------------- Se crea el data store para el Grid (Seccion 1)--------------------
	var consultaSeccion1 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta07ext.data.jsp',
		baseParams: {
			informacion: 'esquemasOperacion'
		},		
		fields: [
			{	name: 'NOMBRE_PARAMETRO'},
			{	name: 'TIPO_DATO'},
			{	name: 'VALORES'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaSeccion1,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaSeccion1(null, null, null);					
				}
			}
		}	
		
	});	
		
	//-------------------- Se crea el data store para el Grid (Seccion 2)--------------------
	var consultaSeccion2 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24consulta07ext.data.jsp',
		baseParams: {
			informacion: 'parametros'
		},		
		fields: [
			{	name: 'NOMBRE_PARAMETRO'},
			{	name: 'TIPO_DATO'},
			{	name: 'VALORES'}		
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaSeccion2,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama a procesarConsultaSeccion1 para desbloquear los componentes.
					procesarConsultaSeccion2(null, null, null);					
				}
			}
		}	
		
	});	
	
	//-------------------- Se crea el store para el combo box--------------------  
	var catalogo = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Secci�n 1: Esquemas de operaci�n'],
			['2','Secci�n 2: Par�metros']			
		 ]
	});
	
   //-------------------- Se crea el Grid (seccion 1)--------------------
	var gridSeccion1 = new Ext.grid.EditorGridPanel({	
		store: consultaSeccion1,
		id: 'gridSeccion1',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',
		hidden: true,		
		columns: [	
			{
				header: 'Nombre del par�metro',
				tooltip: 'Nombre del par�metro',
				dataIndex: 'NOMBRE_PARAMETRO',
				sortable: true,
				width: 299,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Tipo de dato',
				tooltip: 'Tipo de dato',
				dataIndex: 'TIPO_DATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Valores',
				tooltip: 'valores',
				dataIndex: 'VALORES',
				sortable: true,
				width: 197,			
				resizable: true,	
				align: 'left'
			}
		],			
		displayInfo: true,	
		title: 'Secci�n 1: Esquemas de operaci�n',
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 600,
		align: 'center',
		frame: false
	});	
		
   //-------------------- Se crea el Grid (seccion 2)--------------------
	var gridSeccion2 = new Ext.grid.EditorGridPanel({	
		store: consultaSeccion2,
		id: 'gridSeccion2',
		margins: '0 0 0 0',		
		style: 'margin:0 auto;',
		hidden: true,		
		columns: [	
			{
				header: 'Nombre del par�metro',
				tooltip: 'Nombre del par�metro',
				dataIndex: 'NOMBRE_PARAMETRO',
				sortable: true,
				width: 299,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Tipo de dato',
				tooltip: 'Tipo de dato',
				dataIndex: 'TIPO_DATO',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Valores',
				tooltip: 'valores',
				dataIndex: 'VALORES',
				sortable: true,
				width: 197,			
				resizable: true,	
				align: 'left'
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		title: 'Secci�n 2: Par�metros EPO - IF',
		loadMask: true,
		stripeRows: true,
		height: 200,
		width: 600,
		align: 'center',
		frame: false
	});	
	
	//-------------------- Se crean los elementos de la forma(combo y p�neles)  --------------------
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'seccion',	
			id: 'seccionUno',	
			fieldLabel: '&nbsp;&nbsp;Secci�n', 
			mode: 'local',	
			hiddenName : 'seccion',	
			emptyText: 'Seleccione ... ',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			minChars : 1,	
			store : catalogo,	
			displayField : 'descripcion',	
			valueField : 'clave',
			width: 400,	
			value: 1,
			listeners: {
				select: {
					fn: function(combo) {
					
						if(combo.getValue()=='1'){
						
							Ext.getCmp("pane1_1").show();
							Ext.getCmp("panel_seccion_1").show();
							Ext.getCmp("panel_seccion_2").hide();
						} else if(combo.getValue()=='2'){
							Ext.getCmp("pane1_1").hide();
							Ext.getCmp("panel_seccion_1").hide();
							Ext.getCmp("panel_seccion_2").show();
						} else  {
							Ext.getCmp("pane1_1").hide();
							Ext.getCmp("panel_seccion_1").hide();
							Ext.getCmp("panel_seccion_2").hide();
							
						}
					}
				}
			}	
		},	
		{			
			xtype:'panel',  id: 'pane1_1',  hidden:false,	layout:'table',	width:600,	layoutConfig:{ columns: 1 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	width:600,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>  Par�metros con los que una EPO al afiliarse operar�a en forma predeterminada <br>&nbsp;</div>'	}
			]
		},		
		{			
			xtype:'panel',  id: 'panel_seccion_1',  hidden:false,	layout:'table',	width:600,	layoutConfig:{ columns: 1 },
			items: gridSeccion1
		},
		{			
			xtype:'panel',  id: 'panel_seccion_2',  hidden:true,	layout:'table',	width:600,	layoutConfig:{ columns: 1 },
			items: gridSeccion2
		}
	];	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame	: false,
		width: 600,		
		autoHeight	: true,
		title: ' Par�metros Generales',				
		layout : 'form',
		style: 'margin:0 auto;',
      defaults		: {   xtype: 'textfield', msgTarget: 	'side'},
		monitorValid: true,		
		items: elementosForma
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),			
			NE.util.getEspaciador(20)			
		]
	});
	
	consultaSeccion1.load();
	consultaSeccion2.load();
	
});