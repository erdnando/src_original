Ext.onReady(function() {

	
	function procesaAgregarModificar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			if (jsonData != null){				
				if(jsonData.mensaje=='') {
					window.location = '24forma7ext.jsp';	
				}		
					
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
		
	var proComfirmar = function() {
	
		Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de confirmar tasas?', function(resp){
				if(resp =='yes'){		
				
					fp.el.mask('Procesando...', 'x-mask-loading');	
					Ext.getCmp('btnConfirmarTasas').disable();	
					Ext.Ajax.request({
						url: '24forma7.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{ 
							informacion: "Agregar_Modificar_Tasas",
							pAccion :"GA"
						}),		
						callback: procesaAgregarModificar
					});
				}
			});
			
	}
	
	
	//**Agregar 
	
	var procAgregar = function() {
	
		var maxpuntos = Ext.getCmp('maxpuntos').getValue();
		var pPuntos1 = Ext.getCmp('pPuntos1');
		var pRelMat1  = Ext.getCmp('pRelMat1');
		
		var ic_epo1  = Ext.getCmp('ic_epo1');
		var ic_if1  = Ext.getCmp('ic_if1');
		var ic_moneda1  = Ext.getCmp('ic_moneda1');
		
		if (Ext.isEmpty(ic_epo1.getValue()) ){
			ic_epo1.markInvalid('Este campo es obigatorio');
			return;
		}	
		if (Ext.isEmpty(ic_if1.getValue()) ){
			ic_if1.markInvalid('Este campo es obigatorio');
			return;
		}	
		if (Ext.isEmpty(ic_moneda1.getValue()) ){
			ic_moneda1.markInvalid('Este campo es obigatorio');
			return;
		}	
		
		if (Ext.isEmpty(pRelMat1.getValue()) ){
			pRelMat1.markInvalid('Este campo es obigatorio');
			return;
		}			
		
		if (Ext.isEmpty(pPuntos1.getValue()) ){
			pPuntos1.markInvalid('Este campo es obigatorio');
			return;
		}	
		
		if(maxpuntos<pPuntos1.getValue()) {
			pPuntos1.markInvalid('El n�mero rebasa los puntos autorizados');	
			return;
		}
		
		fp.el.mask('Procesando...', 'x-mask-loading');	
			consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{ 
			pAccion :"GA"
			}) 
		});	
		
	}
	
	// ************Para Modificar*****************++
	var procGuardar = function() {
	
		var maxpuntos = Ext.getCmp('maxpuntos').getValue();
		var pPuntos1 = Ext.getCmp('pPuntos1');
		if (Ext.isEmpty(pPuntos1.getValue()) ){
			pPuntos1.markInvalid('Este campo es obigatorio');
			return;
		}	
		
		if(maxpuntos<pPuntos1.getValue()) {
			pPuntos1.markInvalid('El n�mero rebasa los puntos autorizados');	
			return;
		}
		
		fp.el.mask('Procesando....', 'x-mask-loading');		
		Ext.getCmp('btnGuardar').disable();				
		Ext.Ajax.request({
			url: '24forma7.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{ 
				informacion: "Agregar_Modificar_Tasas",
				pAccion :"GM"
			}),		
			callback: procesaAgregarModificar
		});
	
	
	
	}
	
	
	
	// Mostrar  la pantalla de Agregar o Modificar 
	var procModificar = function() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		
		Ext.getCmp('ic_epo1').hide();
		Ext.getCmp('ic_if1').hide();
		Ext.getCmp('ic_moneda1').hide();
		Ext.getCmp('gridConsulta').hide();
		
		Ext.getCmp('lblMoneda').show();
		Ext.getCmp('pRelMat_1').show();
		Ext.getCmp('pPuntos_1').show();
		Ext.getCmp('lblFechaActualizacion').show();
		Ext.getCmp('btnGuardar').show();
		Ext.getCmp('btnCancelar').show();
		
		store.each(function(record) {	
			Ext.getCmp('lblMoneda').update(record.data['MONEDA']);
			Ext.getCmp('pRelMat1').setValue(record.data['REL_MAT']);	
			Ext.getCmp('pPuntos1').setValue(record.data['PUNTOS_ADICIONALES']);	
			Ext.getCmp('lblFechaActualizacion').update(record.data['FECHA_ACTUALIZACION']);			
		});
			
	}
	
	
	 // *********************************Consulta ***********************
	 
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
			Ext.getCmp('btnModificar').hide();
			Ext.getCmp('btnConfirmarTasas').hide();
			
			Ext.getCmp('btnGuardar').hide();
			Ext.getCmp('btnCancelar').hide();
			Ext.getCmp('btnAgregar').hide();
			Ext.getCmp('btnLimpiar').hide();
			Ext.getCmp('ic_epo1').show();
			Ext.getCmp('ic_if1').show();
			Ext.getCmp('clavesTasa').setValue(jsonData.clavesTasa);
			
			
			if(jsonData.existe=='S')  {
				Ext.getCmp('btnConfirmarTasas').show();
			}else  {
				Ext.getCmp('btnModificar').show();

			}
							
			if(jsonData.pAccion!='GA') {
				Ext.getCmp('lblMoneda').hide();
				Ext.getCmp('pRelMat_1').hide();
				Ext.getCmp('pPuntos_1').hide();
				Ext.getCmp('lblFechaActualizacion').hide();
				Ext.getCmp('btnGuardar').hide();
				Ext.getCmp('btnCancelar').hide();
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_ACTUALIZACION'), false);			
			}else  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_ACTUALIZACION'), true);					
			}
			
					
			if(store.getTotalCount() > 0) {			
				el.unmask();				
				
			} else {			
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '24forma7.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'IC_MONEDA'},	
			{	name: 'MONEDA'},	
			{	name: 'TIPO_TASA'},	
			{	name: 'PLAZO'},	
			{	name: 'VALOR'},	
			{	name: 'REL_MAT'},	
			{	name: 'PUNTOS_ADICIONALES'},	
			{	name: 'TASA_APLICAR'},	
			{	name: 'FECHA_ACTUALIZACION'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		hidden: true,	
		clicksToEdit: 1,			
		columns: [				
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left'				
			},
			{
				header: 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00')
			},
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat',
				dataIndex: 'REL_MAT',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{			
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PUNTOS_ADICIONALES',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00')
			},
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00')
			},
			{
				header: 'Fecha de �ltima actualizaci�n',
				tooltip: 'Fecha de �ltima actualizaci�n',
				dataIndex: 'FECHA_ACTUALIZACION',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Confirmar Tasas',					
					tooltip:	'Confirmar Tasas',
					iconCls: 'icoAceptar',
					hidden: true,	
					id: 'btnConfirmarTasas',
					handler: proComfirmar
				},				
				{
					xtype: 'button',
					text: 'Modificar',					
					tooltip:	'Modificar',
					iconCls: 'icoAceptar',
					hidden: true,	
					id: 'btnModificar',
					handler: procModificar
					
				}		
			]
		}				
	});
	 
	 
	
	
	//***************Criterios de busqueda  ****************************
	
	function procesoValidaDatosCombos(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				var fp = Ext.getCmp('forma');
				fp.el.unmask();			
				var jsonData = Ext.util.JSON.decode(response.responseText);
								
				 Ext.getCmp('gridConsulta').hide();
				 
				 Ext.getCmp('pRelMat_1').hide();
				 Ext.getCmp('pPuntos_1').hide();
				 Ext.getCmp('btnAgregar').hide();
				Ext.getCmp('btnLimpiar').hide();
							
				 	
				if(jsonData.combo=='IF') {	 
				
					Ext.getCmp('maxpuntos').setValue(jsonData.maxpuntos);
					if(jsonData.mensaje !='')  {						 
					
						Ext.getCmp('lblMensaje').update(jsonData.mensaje);	
						Ext.getCmp('lblMensaje').show();	
						Ext.getCmp('ic_epo1').hide();						
						Ext.getCmp('ic_if1').hide();
						Ext.getCmp('ic_moneda1').hide();
						Ext.getCmp('gridConsulta').hide();
					
					}else  {					
							
						if(jsonData.totalLineas!=0) {				
							Ext.getCmp('ic_moneda1').show();									
						}else  if(jsonData.totalLineas ==0 ) {				
							Ext.getCmp('ic_moneda1').hide();	
							
						}
						if(jsonData.liRegistros==0) {								
							Ext.getCmp('pRelMat_1').show();
							Ext.getCmp('pPuntos_1').show();
							Ext.getCmp('btnAgregar').show();
							Ext.getCmp('btnLimpiar').show();
							
						}else  {
							fp.el.mask('Procesando...', 'x-mask-loading');	
							consultaData.load({
								params: Ext.apply(fp.getForm().getValues(),{ }) 
							});						
						}
										
					}		
					
				}else  if(jsonData.combo=='MONEDA') {	
				
					if(jsonData.liRegistros ==  0 ) {
					
						Ext.getCmp('pRelMat_1').show();
						Ext.getCmp('pPuntos_1').show();
						Ext.getCmp('btnAgregar').show();
						Ext.getCmp('btnLimpiar').show();
						
					}else  {
					
						fp.el.mask('Procesando...', 'x-mask-loading');	
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{ }) 
						});
					
					}				 
				
				}
					
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma7.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	 
  
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma7.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	// Catalogo 
	var procesarCatMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda = Ext.getCmp('ic_moneda1');					
			if(ic_moneda.getValue()==''){
				ic_moneda.setValue('1');
			}
		}
	}  
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '24forma7.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {		
			load: procesarCatMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catRelMat = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['+','+'],
			['-','-'],		
			['*','*'],			
			['/','/']		
		 ]
	});
	
		
	
	var elementosForma  =[	
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						Ext.getCmp('ic_if1').setValue('');							
						Ext.getCmp('ic_moneda1').hide();	
						Ext.getCmp('gridConsulta').hide();
						Ext.getCmp('pRelMat_1').hide();
						Ext.getCmp('pPuntos_1').hide();
						Ext.getCmp('btnAgregar').hide();
						Ext.getCmp('btnLimpiar').hide();
						Ext.getCmp('ic_if1').show();	
						catalogoIF.load({
							params: {
								ic_epo:combo.getValue()
							}				
						});					
					}
				}
			}			
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			hiddenName : 'ic_if',
			fieldLabel: 'IF',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoIF,
			hidden: true,	
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
											
						fp.el.mask('Procesando...', 'x-mask-loading');								
						Ext.getCmp('ic_moneda1').hide();	
						Ext.getCmp('gridConsulta').hide();
						Ext.getCmp('pRelMat_1').hide();
						Ext.getCmp('pPuntos_1').hide();	
						Ext.getCmp('ic_moneda1').setValue('1');
						
						Ext.Ajax.request({
							url: '24forma7.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: "validaPuntos",
								ic_if:combo.getValue(),
								combo: "IF"
							}),
							callback: procesoValidaDatosCombos
						});					
						
					}
				}
			}				
		},
		{
			xtype: 'combo',
			name: 'ic_moneda',
			id: 'ic_moneda1',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,	
			hidden: true,	
			store : catalogoMoneda,			
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						fp.el.mask('Procesando...', 'x-mask-loading');	
						Ext.getCmp('gridConsulta').hide();
						var ic_epo = Ext.getCmp('ic_epo1').getValue();
						var ic_if = Ext.getCmp('ic_if1').getValue();						
						
						Ext.getCmp('pRelMat1').setValue('');
						Ext.getCmp('pPuntos1').setValue('');	
									
						
						Ext.Ajax.request({
							url: '24forma7.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: "validaDatosMoneda",
								ic_moneda:combo.getValue(),								
								ic_epo:ic_epo,
								ic_if:ic_if,
								combo: "MONEDA"
							}),
							callback: procesoValidaDatosCombos
						});	
						
						
						
					}
				}
			}				
		},
		{
			xtype: 'displayfield',
			hidden: true,	
			id: 'lblMoneda',
			style: 'text-align:left;',
			fieldLabel: 'Moneda',
			text: '-',
			width: 300
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Relaci�n Mat.',
			combineErrors: false,
			msgTarget: 'side',
			id: 'pRelMat_1',
			hidden: true,	
			width: 300,
			items: [	
				{
					xtype: 'combo',	
					name: 'pRelMat',	
					id: 'pRelMat1',	
					fieldLabel: 'Relaci�n Mat.', 
					mode: 'local',	
					hiddenName : 'pRelMat',	
					emptyText: 'Seleccione ... ',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					minChars : 1,	
					store : catRelMat,	
					displayField : 'descripcion',	
					valueField : 'clave',						
					width: 300,
					value:'+'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Puntos Adicionales',
			combineErrors: false,
			msgTarget: 'side',
			id: 'pPuntos_1',
			hidden: true,	
			width: 300,
			items: [	
				{
					xtype: 'numberfield',
					fieldLabel: 'Puntos Adicionales',	
					name: 'pPuntos',
					id: 'pPuntos1',
					allowBlank: true,
					maxLength: 7,
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 								
							if(field.getValue() != '') {	
							
								var maxpuntos = Ext.getCmp('maxpuntos').getValue();
								var pPuntos1 = Ext.getCmp('pPuntos1');
								if(maxpuntos<field.getValue()) {
									pPuntos1.markInvalid('El n�mero rebasa los puntos autorizados');	
									return;
								}
							}							
						}
					}
				}	
			]
		},
		{
			xtype: 'displayfield',
			hidden: true,	
			id: 'lblFechaActualizacion',
			style: 'text-align:left;',
			fieldLabel: 'Fecha �ltima actualizaci�n',
			text: '-',
			width: 300
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'maxpuntos', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'clavesTasa', 	value: '' }
			
	];
		
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Tasas por EPO',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [ elementosForma,		
			{
				width		: 800,	
				xtype: 'label',
				id: 'lblMensaje',	
				align: 'center',
				style: 'text-align:center;',
				text: ''
			}		
		],
		buttons: [		
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				hidden: true,					
				handler: procGuardar			
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'icoLimpiar',
				hidden: true,	
				handler: function() {
					
					fp.el.mask('Procesando...', 'x-mask-loading');	
					consultaData.load({ 	params: Ext.apply(fp.getForm().getValues(),{ 	}) 	});						
					
				}
			},
			{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'icoGuardar',
				hidden: true,				
				handler: procAgregar			
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				hidden: true,	
				handler: function() {
				
					window.location = '24forma7ext.jsp';
					
				}
			}
		]
	});
	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),	
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoEPO.load();
	catalogoMoneda.load();
	
});
