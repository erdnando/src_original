<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	org.apache.commons.logging.Log,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/24finandist/24secsession_extjs.jspf" %>
		<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String cg_tipo_sol = (request.getParameter("cg_tipo_sol")==null)?"":request.getParameter("cg_tipo_sol");
	String accion = "", if_tipoliq="", if_desc="", consulta="", infoRegresar ="";  
	JSONObject jsonObj = new JSONObject();
	Vector vecParametros = new Vector();
	Vector vecColumnas = null;
	Vector vecFilas = null;
	
	JSONArray registros = new JSONArray();
	HashMap datos = new HashMap();
	
	ParametrosDist  beanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	
	if (informacion.equals("catalogoIF") ) { //-->En este if se llena el combo
	
		CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");   
		cat.setOrden("cg_razon_social");  
		infoRegresar = cat.getJSONElementos();	
	
	} else if(informacion.equals("consultaValores")) {//--> Obtengo los valores de a colunna "Valores" de la seccion 1
		vecParametros = beanParametros.getParamIF(ic_if);
		if_desc = (String)vecParametros.get(0); //--> cg_razon_social
		if_tipoliq = (String)vecParametros.get(1); //-->cg_tipo_liquidacion
		
		if(if_tipoliq.equals("I")){
			if_tipoliq = "Depósito del Acreditado a IF";
		} else if(if_tipoliq.equals("A")){
			if_tipoliq = "Cargo de IF a la Cuenta del Acreditado";
		}
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("if_desc", if_desc);	
		jsonObj.put("if_tipoliq", if_tipoliq);
		jsonObj.put("accion", "OK");
		
		infoRegresar = jsonObj.toString();	
		
	} else if(informacion.equals("autorizacionTasasInteres")){
			
		vecFilas =beanParametros.getEposxIf(ic_if);
		if( vecFilas.size()>0){
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				String ic_epo = (String)vecColumnas.get(0);
				String epo_desc = (String)vecColumnas.get(1);
				String rad_value	= (String)vecColumnas.get(2);
	
				if (rad_value.equals("S")) {
					rad_value = "SI AUTORIZADO";
				} else if(rad_value.equals("N")) {
					rad_value = "NO AUTORIZADO";
				} else {
					rad_value = "";
				}
				datos = new HashMap();
				datos.put("IC_EPO", ic_epo);
				datos.put("EPO_DESC", epo_desc);
				datos.put("VALORES", rad_value);
				registros.add(datos);
			} 
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString();  	
	}
%>
<%=infoRegresar%>